<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027962966</ID>
<ANCIEN_ID>JG_L_2013_09_000000367396</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/96/29/CETATEXT000027962966.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème SSR, 17/09/2013, 367396</TITRE>
<DATE_DEC>2013-09-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>367396</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème SSR</FORMATION>
<TYPE_REC>Contentieux des pensions</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELVOLVE, DELVOLVE</AVOCATS>
<RAPPORTEUR>M. Frédéric Dieu</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Bertrand Dacosta</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2013:367396.20130917</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi du ministre de l'économie et des finances, enregistré le 3 avril 2013 au secrétariat du contentieux du Conseil d'Etat ; le ministre demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 12MA01386 du 12 février 2013 par lequel la cour administrative d'appel de Marseille a rejeté son recours tendant, à titre principal, à l'annulation de l'ordonnance n° 1200516 du 26 mars 2012 par laquelle le juge des référés du tribunal administratif de Nîmes a condamné l'Etat à verser à Mme A...la somme provisionnelle de 20 000 euros au titre des pensions d'orphelins de ses enfants Louis, Victor et Pierre et, à titre subsidiaire, au sursis à l'exécution de cette ordonnance ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son recours ;  <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution, notamment son Préambule et son article 61-1 ; <br/>
<br/>
              Vu l'ordonnance n° 58-1067 du 7 novembre 1958 ; <br/>
<br/>
              Vu la Convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, ensemble son premier protocole additionnel ;<br/>
<br/>
              Vu le code des pensions civiles et militaires de retraite ;<br/>
<br/>
              Vu le code de la sécurité sociale ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Dieu, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de M. Bertrand Dacosta, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Delvolvé, Delvolvé, avocat de Mme A...;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis au juge des référés que l'époux de Mme A...est décédé en décembre 2006 et qu'elle bénéficie d'une pension de réversion ; que, par courrier du 2 février 2012, la direction régionale des finances publiques de la région Languedoc-Roussillon a refusé à Mme A... le paiement des pensions temporaires d'orphelin qu'elle estimait lui être dues au titre de ses trois enfants ; que, par l'arrêt attaqué, la cour administrative d'appel de Marseille a rejeté l'appel du ministre du budget, des comptes publics et de la réforme de l'Etat contre l'ordonnance du 26 mars 2012 par laquelle le juge des référés du tribunal administratif de Nîmes a condamné l'Etat à verser à Mme A...une provision de 20 000 euros au titre de ces pensions ;<br/>
<br/>
              Sur la question prioritaire de constitutionnalité :<br/>
<br/>
              2. Considérant qu'aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...) " ; qu'il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              3. Considérant qu'à l'appui de sa question prioritaire de constitutionnalité, Mme A...soutient que les dispositions combinées de l'article L. 89 du code des pensions civiles et militaires de retraite et de l'article L. 553-3 du code de la sécurité sociale, telles qu'appliquées par le ministre, méconnaissent le principe d'égalité garanti par l'article 6 de la Déclaration des droits de l'homme et du citoyen ainsi que le droit de toute personne à mener une vie familiale normale garanti par le dixième alinéa du préambule de la Constitution de 1946 ;<br/>
<br/>
              4. Considérant qu'aux termes de l'article L. 40 du code des pensions civiles et militaires de retraite : " Chaque orphelin a droit jusqu'à l'âge de vingt et un ans à une pension égale à 10 % de la pension obtenue par le fonctionnaire ou qu'il aurait pu obtenir au jour de son décès, et augmentée, le cas échéant, de 10 % de la rente d'invalidité dont il bénéficiait ou aurait pu bénéficier, sans que le total des émoluments attribués aux conjoints survivants ou divorcés et aux orphelins puisse excéder le montant de la pension et, éventuellement, de la rente d'invalidité attribuées ou qui auraient été attribuées au fonctionnaire. S'il y a excédent, il est procédé à la réduction temporaire des pensions des orphelins " ; qu'aux termes de l'article L. 89 du même code : " Est interdit du chef d'un même enfant, le cumul de plusieurs accessoires de traitement, solde, salaire et pension servis par l'Etat, les collectivités publiques et les organismes de prévoyance collectifs ou obligatoires, aux intéressés ou à leur conjoint, dans les conditions prévues à l'article L. 553-3 du code de la sécurité sociale. Cette interdiction ne s'applique pas à la majoration de pension prévue à l'article L. 18 " ; que l'article L. 553-3 du code de la sécurité sociale dispose : " Lorsqu'un même enfant ouvre droit aux prestations familiales et à une majoration de l'une quelconque des allocations ci-après énumérées : ... 4° retraites ou pensions attribuées par l'Etat, les collectivités publiques ou les organismes de prévoyance obligatoire, les prestations familiales sont perçues en priorité et excluent, à due concurrence, lesdites majorations " ; <br/>
<br/>
              5. Considérant que, pour l'application de ces dispositions, la pension temporaire d'orphelin à laquelle l'enfant ouvre droit doit être regardée comme un accessoire de la pension de réversion perçue par le conjoint du fonctionnaire décédé, qui ne peut être cumulé avec d'autres accessoires tels que les prestations familiales ; que, par suite, les prestations familiales sont dues par priorité pour chacun des enfants et excluent à due concurrence le paiement de la pension temporaire d'orphelin pour chacun des enfants ouvrant droit à des prestations familiales ; <br/>
<br/>
              6. Considérant, d'une part, que les dispositions combinées de l'article L. 89 du code des pensions civiles et militaires de retraite et de l'article L. 553-3 du code de la sécurité sociale ne sauraient être regardées, au seul motif qu'elles excluent, à due concurrence du montant des prestations familiales auxquelles un orphelin est susceptible d'ouvrir droit, le versement d'une pension temporaire d'orphelin, comme faisant obstacle à l'exercice d'une vie familiale normale ; que, d'autre part, tous les orphelins se trouvent dans une situation identique pour l'application de ces dispositions ; que, par suite, la question soulevée, qui n'est pas nouvelle, ne présente pas un caractère sérieux ; qu'ainsi, sans qu'il soit besoin de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée, le moyen tiré de ce que les dispositions précitées des articles L. 89 du code des pensions civiles et militaires de l'Etat et L. 553-3 du code de la sécurité sociale portent atteinte aux droits et libertés garantis par la Constitution doit être écarté ;<br/>
<br/>
              Sur le pourvoi :<br/>
<br/>
              7. Considérant qu'ainsi qu'il a été dit ci-dessus, il résulte des dispositions combinées de l'article L. 89 du code des pensions civiles et militaires de retraite et de l'article L. 553-3 du code de la sécurité sociale, que les prestations familiales auxquelles ouvre droit un enfant orphelin excluent, à due concurrence, le versement d'une pension temporaire d'orphelin ; que, eu égard aux conditions de leur mise en cause, analysées ci-dessus, ces dispositions ne méconnaissent pas les stipulations des articles 8 et 14 de la Convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et de l'article 1er de son premier protocole additionnel ; qu'en jugeant qu'elles ne faisaient pas obstacle au cumul des prestations familiales et des pensions temporaires d'orphelin, la cour administrative d'appel de Marseille a commis une erreur de droit ; que, dès lors, le ministre de l'économie et des finances est fondé à demander l'annulation de l'arrêt attaqué ; <br/>
<br/>
              8. Considérant que, dans les circonstances de l'espèce, il y a lieu, en application de l'article L. 821-2 du code de justice administrative, de régler l'affaire au titre de la procédure de référé engagée ; <br/>
<br/>
              9. Considérant qu'aux termes de l'article R. 541-1 du code de justice administrative : " Le juge des référés peut, même en l'absence d'une demande au fond, accorder une provision au créancier qui l'a saisi lorsque l'existence de l'obligation n'est pas sérieusement contestable. Il peut, même d'office, subordonner le versement de la provision à la constitution d'une garantie. " ;<br/>
<br/>
              10. Considérant que Mme A...ne conteste pas que le montant des prestations familiales qui lui sont servies au titre de ses trois enfants est supérieur au montant des pensions temporaires d'orphelin auxquels ils sont susceptibles d'ouvrir droit ; que, par suite, le ministre a pu légalement lui refuser le paiement de l'intégralité de ces pensions ; que, dès lors, celui-ci est fondé à soutenir que c'est à tort que, par l'ordonnance attaquée, le juge des référés du tribunal administratif de Nîmes a jugé que l'obligation dont se prévalait Mme A...à l'encontre de l'Etat n'était pas sérieusement contestable et lui a accordé une provision de 20 000 euros ; que cette ordonnance doit ainsi être annulée ;<br/>
<br/>
              11. Considérant que l'annulation de l'ordonnance du juge des référés, par la présente décision, prive d'objet les conclusions du ministre tendant à ce qu'il soit sursis à son exécution ;<br/>
<br/>
              12. Considérant qu'il résulte de ce qui précède que la demande de Mme A...doit être rejetée, y compris ses conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative et de l'article 37 de la loi du 10 juillet 1991 ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par MmeA....<br/>
Article 2 : L'arrêt de la cour administrative de Marseille du 12 février 2013 et l'ordonnance du juge des référés du tribunal administratif de Nîmes du 26 mars 2012 sont annulés.<br/>
Article 3 : Il n'y pas lieu de statuer sur les conclusions du ministre de l'économie et des finances tendant à ce qu'il soit sursis à l'exécution de cette ordonnance.<br/>
Article 4 : La demande présentée par Mme A...devant le tribunal administratif de Nîmes et ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative et de l'article 37 de la loi du 10 juillet 1991 sont rejetées.<br/>
Article 5 : La présente décision sera notifiée au ministre de l'économie et des finances et à Mme B... A....<br/>
Copie en sera adressée pour information au Conseil constitutionnel et au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">48-02-01-09-02 PENSIONS. PENSIONS CIVILES ET MILITAIRES DE RETRAITE. QUESTIONS COMMUNES. AYANTS-CAUSE. ORPHELINS. - PENSION TEMPORAIRE D'ORPHELIN (ART. L. 40 DU CODE DES PENSIONS CIVILES ET MILITAIRES DE RETRAITE) - CARACTÈRE D'ACCESSOIRE DE LA PENSION DE RÉVERSION PERÇUE PAR LE CONJOINT DU FONCTIONNAIRE DÉCÉDÉ - EXISTENCE - CONSÉQUENCE - IMPOSSIBILITÉ DE CUMUL AVEC D'AUTRES ACCESSOIRES DE CETTE PENSION (ART. L. 89 DU MÊME CODE) - CAS DES PRESTATIONS FAMILIALES - PERCEPTION EN PRIORITÉ DE CES PRESTATIONS ET EXCLUSION À DUE CONCURRENCE DU PAIEMENT DE LA PENSION TEMPORAIRE D'ORPHELIN (ART. L. 533 DU CODE DE LA SÉCURITÉ SOCIALE) [RJ1].
</SCT>
<ANA ID="9A"> 48-02-01-09-02 Pour l'application des dispositions des articles L. 89 du code des pensions civiles et militaires de retraite et L. 553 du code de la sécurité sociale, la pension temporaire d'orphelin à laquelle l'enfant d'un fonctionnaire décédé ouvre droit en vertu de l'article L. 40 du code des pensions civiles et militaires de retraite doit être regardée comme un accessoire de la pension de réversion perçue par le conjoint du fonctionnaire décédé, qui ne peut être cumulé avec d'autres accessoires tels que les prestations familiales. Par suite, les prestations familiales sont dues par priorité pour chacun des enfants et excluent à due concurrence le paiement de la pension temporaire d'orphelin pour chacun des enfants ouvrant droit à des prestations familiales.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Solution abandonnée par CE, Section, 27 juillet 2015, Ministre de l'économie et des finances c/ Mme Taino, n° 375042, p. 301.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
