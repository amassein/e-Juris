<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031596621</ID>
<ANCIEN_ID>JG_L_2015_12_000000386817</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/59/66/CETATEXT000031596621.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème SSR, 09/12/2015, 386817</TITRE>
<DATE_DEC>2015-12-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>386817</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>BROUCHOT</AVOCATS>
<RAPPORTEUR>Mme Sophie-Caroline de Margerie</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier Domino</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2015:386817.20151209</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Par une requête, enregistrée au greffe du tribunal administratif de Montpellier le 10 juin 2014, M. D...et Mme B...A...ont demandé à ce tribunal d'annuler pour excès de pouvoir la décision implicite par laquelle le ministre de l'intérieur a rejeté leur demande tendant, d'une part, à rétablir les dispositions de la circulaire du ministre de l'intérieur du 11 mai 1990 exigeant des ressortissants français mineurs quittant seuls le territoire français avec leur carte nationale d'identité ou un passeport périmé de détenir une autorisation parentale de sortie du territoire, dispositions abrogées par la circulaire n° INTD1237286C du 20 novembre 2012 relative à la décision judiciaire d'interdiction de sortie du territoire et à la mesure administrative conservatoire d'opposition à la sortie du territoire des mineurs, d'autre part, à la réparation du préjudice moral qu'ils estiment avoir subi du fait du départ de leur fille mineure vers la Syrie, de condamner l'Etat à leur verser à chacun la somme de 50 000 euros en réparation de ce préjudice et de mettre à la charge de l'Etat le versement de la somme de 4 000 euros à la SCP Blanquer-Girard Basile-Jauvin Croizier, leur avocat, sur le fondement des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
              Par une ordonnance n° 1402762 du 15 décembre 2014, enregistrée au secrétariat du contentieux du Conseil d'Etat le 30 décembre 2014, le président du tribunal administratif de Montpellier a transmis cette requête au Conseil d'Etat en application de l'article R. 351-3 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - la convention internationale relative aux droits de l'enfant ;<br/>
              - le règlement (CE) n° 562/2006 du Parlement européen et du Conseil du 15 mars 2006 ;<br/>
              - le code civil ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie-Caroline de Margerie, conseiller d'Etat, <br/>
<br/>
              - les conclusions de M. Xavier Domino, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Brouchot, avocat de M. C...et de Mme A...; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Sur les conclusions à fins d'annulation :<br/>
<br/>
              1.	Considérant que, par une lettre du 31 mars 2014, M. C...et Mme A... ont demandé au ministre de l'intérieur d'instituer un dispositif exigeant des ressortissants français mineurs d'être munis d'une autorisation de leurs parents pour quitter seuls le territoire français, dès lors que la circulaire interministérielle du 20 novembre 2012 relative aux décisions judiciaires d'interdiction de sortie du territoire et aux mesures administratives conservatoires d'opposition à la sortie du territoire des mineurs a abrogé les dispositions de la circulaire du ministre de l'intérieur du 11 mai 1990 exigeant des ressortissants français mineurs quittant seuls le territoire français avec leur carte nationale d'identité ou un passeport périmé de détenir une autorisation parentale de sortie du territoire ; qu'ils demandent l'annulation pour excès de pouvoir du rejet implicite opposé à leur demande ;<br/>
<br/>
              2.	Considérant, d'une part, qu'aux termes de l'article 371-1 du code civil : " L'autorité parentale est un ensemble de droits et de devoirs ayant pour finalité l'intérêt de l'enfant. / Elle appartient aux parents jusqu'à la majorité ou l'émancipation de l'enfant pour le protéger dans sa sécurité, sa santé et sa moralité, pour assurer son éducation et permettre son développement, dans le respect dû à sa personne. / Les parents associent l'enfant aux décisions qui le concernent, selon son âge et son degré de maturité " ; qu'aux termes de l'article 371-3 du même code : " L'enfant ne peut, sans permission des père et mère, quitter la maison familiale et il ne peut en être retiré que dans les cas de nécessité que détermine la loi " ; que ces dispositions n'imposent pas aux autorités compétentes d'instituer un dispositif général exigeant des ressortissants français mineurs d'être munis d'une autorisation de leurs parents pour quitter seuls le territoire français ; <br/>
<br/>
              3.	Considérant, d'autre part, que si le règlement (CE) n° 562/2006 du Parlement européen et du Conseil du 15 mars 2006 établissant un code communautaire relatif au régime de franchissement des frontières par les personnes recommande aux garde-frontières d'accorder une attention particulière aux mineurs et de vérifier, de manière approfondie, les documents de voyage et les autres documents présentés par les mineurs voyageant non accompagnés, ces dispositions ne sont pas méconnues par la circulaire, qui prescrit à ces autorités de vérifier, dans tous les cas, outre la validité du titre de voyage, que le mineur ne fait pas l'objet d'une interdiction judiciaire de sortie du territoire ou d'une opposition à sortie du territoire ; <br/>
<br/>
              4.	Considérant qu'il résulte de ce qui précède que la décision attaquée n'a méconnu ni les dispositions du code civil citées ci-dessus, ni le règlement du 15 mars 2006, ni l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, ni l'article 3, paragraphe 1, de la convention internationale relative aux droits de l'enfant ; que, dès lors, les requérants ne sont pas fondés, par les dispositions et stipulations qu'ils invoquent, à demander l'annulation de cette décision ; <br/>
<br/>
              Sur les conclusions à fins d'indemnisation :<br/>
<br/>
              5.	Considérant que les requérants demandent la réparation du préjudice qu'ils estiment avoir subi du fait du départ de leur fille mineure, alors âgée de 17 ans, le 11 mars 2014 à 14h45, sur un vol qui a quitté l'aéroport de Marseille Provence à destination d'Istanbul, d'où, selon eux, elle aurait rejoint la Syrie ;<br/>
<br/>
              6.	Considérant, en premier lieu, que les conclusions tendant à la réparation du préjudice moral qu'ils estiment avoir subi en raison de l'illégalité fautive de la circulaire ne peuvent, par voie de conséquence de ce qui a été dit ci-dessus et en tout état de cause, qu'être rejetées ;<br/>
<br/>
              7.	Considérant, en second lieu, qu'il n'est pas contesté que la jeune fille, qui était en possession d'une passeport en cours de validité et d'un billet d'avion à son nom, remplissait les conditions légales de sortie du territoire à destination de la Turquie ; qu'il résulte de l'instruction et notamment du rapport de consultation des traces de connexion au fichier des personnes recherchées, produit par la direction centrale de la police judiciaire, que les fonctionnaires en charge du contrôle des frontières à l'aéroport de Marseille Provence ont, d'une part, vérifié la conformité du nom figurant sur la carte d'embarquement de la jeune fille avec celui figurant sur son passeport et, d'autre part, consulté à 14h02 le fichier national des personnes recherchées pour s'assurer qu'elle ne faisait pas l'objet d'une interdiction judiciaire de sortie du territoire ou d'une opposition à sortie du territoire ; que, par suite, ils n'ont commis aucune faute dans l'exécution de leur mission de surveillance de nature à engager la responsabilité de l'Etat ; qu'en conséquence, les requérants ne sont pas fondés à demander une réparation sur ce fondement ;<br/>
<br/>
              Sur les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              8.	Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante, la somme que demandent les requérants au titre des frais avancés par eux et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
           --------------<br/>
<br/>
Article 1er : La requête de M. C...et Mme A...est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M.D..., à Mme B...A...et au ministre de l'intérieur.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">35 FAMILLE. - REFUS D'INSTITUER UN DISPOSITIF D'AUTORISATION PARENTALE DE SORTIE DU TERRITOIRE POUR LES MINEURS - MÉCONNAISSANCE DES ARTICLES 371-1 ET 371-3 C. CIV., DU RÈGLEMENT (CE) N° 562/2006, DE L'ART. 8 CEDH OU DE L'ARTICLE 3§1 DE LA CONVENTION INTERNATIONALE RELATIVE AUX DROITS DE L'ENFANT - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">49-05 POLICE. POLICES SPÉCIALES. - SORTIE DU TERRITOIRE - 1) REFUS D'INSTITUER UN DISPOSITIF D'AUTORISATION PARENTALE DE SORTIE DU TERRITOIRE POUR LES MINEURS - MÉCONNAISSANCE DES ARTICLES 371-1 ET 371-3 C. CIV., DU RÈGLEMENT (CE) N° 562/2006, DE L'ART. 8 CEDH OU DE L'ARTICLE 3§1 DE LA CONVENTION INTERNATIONALE RELATIVE AUX DROITS DE L'ENFANT - ABSENCE - 2) RESPONSABILITÉ DES SERVICES DE POLICE DANS LEUR MISSION DE SURVEILLANCE DE LA SORTIE DU TERRITOIRE - RÉGIME - FAUTE SIMPLE [RJ1].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">60-02-03-01 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RESPONSABILITÉ EN RAISON DES DIFFÉRENTES ACTIVITÉS DES SERVICES PUBLICS. SERVICES DE POLICE. SERVICES DE L'ETAT. - SURVEILLANCE DE LA SORTIE DU TERRITOIRE - RÉGIME - FAUTE SIMPLE [RJ1].
</SCT>
<ANA ID="9A"> 35 Ni les articles 371-1 et 371-3 du code civil, ni le règlement (CE) n° 562/2006 du 15 mars 2006 établissant un code communautaire relatif au régime de franchissement des frontières, ni l'article 8 de la convention européenne des droits de l'homme et des libertés fondamentales (Convention EDH), ni l'article 3, paragraphe 1 de la convention internationale relative aux droits de l'enfant n'imposent aux autorités compétentes d'instituer un dispositif général exigeant des ressortissants français mineurs d'être munis d'une autorisation de leurs parents pour quitter le territoire français.</ANA>
<ANA ID="9B"> 49-05 1) Ni les articles 371-1 et 371-3 du code civil, ni le règlement (CE) n° 562/2006 du 15 mars 2006 établissant un code communautaire relatif au régime de franchissement des frontières, ni l'article 8 de la convention européenne des droits de l'homme et des libertés fondamentales (Convention EDH), ni l'article 3, paragraphe 1 de la convention internationale relative aux droits de l'enfant n'imposent aux autorités compétentes d'instituer un dispositif général exigeant des ressortissants français mineurs d'être munis d'une autorisation de leurs parents pour quitter le territoire français.,,,2) Une faute commise par les services chargés du contrôle des frontières dans leur mission de surveillance de la sortie du territoire est de nature à engager la responsabilité de l'Etat.</ANA>
<ANA ID="9C"> 60-02-03-01 Une faute commise par les services chargés du contrôle des frontières dans leur mission de surveillance de la sortie du territoire est de nature à engager la responsabilité de l'Etat.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Ab. jur. CE, 9 février 1979, de La Villéon, n° 09137, p. 57 ; CE, 26 juin 1985, Mme Garagnon, n° 45560, p. 209.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
