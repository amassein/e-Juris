<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000007841893</ID>
<ANCIEN_ID>JGXLX1994X11X0000021359</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/07/84/18/CETATEXT000007841893.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'Etat, 4 SS, du 2 novembre 1994, 121359, mentionné aux tables du recueil Lebon</TITRE>
<DATE_DEC>1994-11-02</DATE_DEC>
<JURIDICTION>Conseil d'Etat</JURIDICTION>
<NUMERO>121359</NUMERO>
<SOLUTION>Annulation renvoi</SOLUTION>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4 SS</FORMATION>
<TYPE_REC>Recours en cassation</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Massot</PRESIDENT>
<AVOCATS/>
<RAPPORTEUR>M. Stasse</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Schwartz</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>    Vu la requête sommaire et le mémoire complémentaire enregistrés les 28 novembre 1990 et 3 janvier 1991, présentés pour M. Michel X... domicilié ... ; M. X... demande l'annulation d'une décision du 18 juillet 1990 par laquelle la section disciplinaire du Conseil national de l'ordre des médecins lui a infligé la sanction du blâme ;<br/>    Vu les autres pièces du dossier ;<br/>    Vu la loi n° 88-828 du 20 juillet 1988 portant amnistie ;<br/>    Vu le décret n° 79-506 du 28 juin 1979 portant code de déontologie médicale ;<br/>    Vu le décret n° 91-1266 du 19 décembre 1991 ;<br/>    Vu l'ordonnance n° 45-1708 du 31 juillet 1945, le décret n° 53-934 du 30 septembre 1953 et la loi n° 87-1127 du 31 décembre 1987 ;<br/>    Après avoir entendu en audience publique :<br/>    - le rapport de M. Stasse, Maître des Requêtes,<br/>    - les observations de la SCP de Chaisemartin, Courjon, avocat de M. Michel X... et de la SCP Peignot, Garreau, avocat du conseil national de l'ordre des médecins,<br/>    - les conclusions de M. Schwartz, Commissaire du gouvernement ;<br/>
<br/>    Sans qu'il soit besoin d'examiner les autres moyens de la requête :<br/>    Considérant qu'aux termes de l'article 83 du code de déontologie médicale susvisé, "nul ne peut être à la fois, sauf en cas d'urgence, médecin contrôleur et médecin traitant d'un même malade" ;<br/>    Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. X... a prodigué des soins à une patiente en 1984 alors qu'il avait effectué un contrôle auprès de cette même patiente en 1983, en méconnaissance des dispositions précitées de l'article 83 du code de déontologie médicale ; qu'à supposer même que M. X... ait méconnu une seconde fois lesdites dispositions en effectuant un contrôle auprès de cette même patiente en 1987, son comportement fautif ne peut être regardé comme ayant revêtu un caractère systématique, contraire à l'honneur et à la probité ; que par suite, en estimant que M. X... ne pouvait bénéficier de l'amnistie instituée par la loi susvisée du 20 juillet 1988, la section disciplinaire du Conseil national de l'ordre des médecins a fait une application inexacte de ladite loi ;<br/>    Sur les conclusions de M. X... tendant à l'application de l'article 1er du décret susvisé du 2 septembre 1988 :<br/>    Considérant que le décret n° 88-907 du 2 septembre 1988 ayant été abrogé par le décret n° 91-1266 du 19 décembre 1991 portant application de la loi n° 91-647 du 10 juillet 1991, les conclusions de M. X... doivent être regardées comme demandant la condamnation de la Caisse primaire d'assurance maladie du Loiret sur le fondement de l'article 75-I de ladite loi ;<br/>    Considérant que les conclusions de M. X... tendent à ce que la Caisse primaire d'assurance maladie du Loiret soit condamnée à lui verser une somme exposée par lui et non comprise dans les dépens ; que ces conclusions, dirigées contre une caisse primaire d'assurance maladie qui n'est pas partie à la présente instance ne peuvent qu'être rejetées ;<br/>Article 1er : La décision du 18 juillet 1990 de la section disciplinaire du Conseil national de l'Ordre des médecins est annulée.<br/>Article 2 : Les conclusions de M. X... tendant à l'application de l'article 1er du décret du 2 septembre 1988 sont rejetées.<br/>Article 3 : L'affaire est renvoyée devant la section disciplinaire du Conseil national de l'Ordre des médecins.<br/>Article 3 : La présente décision sera notifiée à M. X..., à l'Ordre des médecins et auministre d'Etat, ministre des affaires sociales, de la santé et de la ville.<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8AA" TYPE="PRINCIPAL">07-01-01-02-01 AMNISTIE, GRACE ET REHABILITATION - AMNISTIE - BENEFICE DE L'AMNISTIE - AMNISTIE DES SANCTIONS DISCIPLINAIRES OU PROFESSIONNELLES - FAITS NON CONTRAIRES A LA PROBITE, AUX BONNES MOEURS OU A L'HONNEUR -Médecins - Cumul occasionnel des fonctions de médecin contrôleur et de médecin traitant.</SCT>
<SCT ID="8BA" TYPE="PRINCIPAL">55-04-02-04-02-01 PROFESSIONS - CHARGES ET OFFICES - DISCIPLINE PROFESSIONNELLE - SANCTIONS - AMNISTIE - FAITS NON CONTRAIRES A LA PROBITE, AUX BONNES MOEURS OU A L'HONNEUR - MEDECINS -Cumul occasionnel des fonctions de médecin contrôleur et de médecin traitant.</SCT>
<ANA ID="9AA">07-01-01-02-01, 55-04-02-04-02-01        Médecin ayant, en méconnaissance de l'article 83 du code de déontologie médicale selon lequel "nul ne peut être à la fois, sauf en cas d'urgence, médecin contrôleur et médecin traitant d'un même malade", prodigué des soins à une patiente alors qu'il avait effectué un contrôle auprès d'elle l'année précédente. A supposer même que ce médecin ait méconnu une seconde fois ces dispositions en effectuant un contrôle auprès de cette même patiente trois ans plus tard, son comportement fautif ne peut être regardé comme ayant revêtu un caractère systématique, contraire à l'honneur et à la probité.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS>
<LIEN cidtexte="" datesignatexte="" id="" naturetexte="" nortexte="" num="" numtexte="" sens="source" typelien="CITATION">Code de déontologie médicale 83</LIEN>
<LIEN cidtexte="" datesignatexte="" id="" naturetexte="" nortexte="" num="" numtexte="" sens="source" typelien="CITATION">Décret 79-506 1979-06-28 art. 83</LIEN>
<LIEN cidtexte="" datesignatexte="" id="" naturetexte="" nortexte="" num="" numtexte="" sens="source" typelien="CITATION">Décret 88-907 1988-09-02 art. 1</LIEN>
<LIEN cidtexte="" datesignatexte="" id="" naturetexte="" nortexte="" num="" numtexte="" sens="source" typelien="CITATION">Décret 91-1266 1991-12-19</LIEN>
<LIEN cidtexte="" datesignatexte="" id="" naturetexte="" nortexte="" num="" numtexte="" sens="source" typelien="CITATION">Loi 88-828 1988-07-20</LIEN>
<LIEN cidtexte="" datesignatexte="" id="" naturetexte="" nortexte="" num="" numtexte="" sens="source" typelien="CITATION">Loi 91-647 1991-07-10 art. 75</LIEN>
</LIENS>
</TEXTE_JURI_ADMIN>
