<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037183337</ID>
<ANCIEN_ID>JG_L_2018_07_000000407865</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/18/33/CETATEXT000037183337.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 11/07/2018, 407865, Publié au recueil Lebon</TITRE>
<DATE_DEC>2018-07-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>407865</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS ; SCP FABIANI, LUC-THALER, PINATEL</AVOCATS>
<RAPPORTEUR>M. François Lelièvre</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier Henrard</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:407865.20180711</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La commune d'Isola et le syndicat mixte pour l'aménagement et l'exploitation de la station d'Isola 2000 ont demandé à la cour administrative d'appel de Marseille d'enjoindre à la société d'aménagement d'Isola (SAI) 2000 d'exécuter son arrêt n° 12MA01668 du 7 juillet 2014 et la décision n° 384280 du 14 mars 2016 du Conseil d'Etat statuant au contentieux annulant partiellement cet arrêt, de porter le taux de l'astreinte à 10 000 euros par jour de retard et de procéder à la liquidation de l'astreinte provisoire. <br/>
<br/>
              Par un arrêt n° 16MA02502 du 12 décembre 2016, la cour administrative d'appel de Marseille a rejeté cette requête.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 13 février, 15 mai 2017 et 25 janvier 2018 au secrétariat du contentieux du Conseil d'Etat, la commune d'Isola et le syndicat mixte pour l'aménagement et l'exploitation de la station d'Isola 2000 demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur requête ;<br/>
<br/>
              3°) de mettre à la charge de la SAI 2000 la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la loi n° 80-539 du 16 juillet 1980 ;<br/>
              - la loi n° 95-125 du 8 février 1995 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. François Lelièvre, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Olivier Henrard, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de la commune d'Isola et du syndicat mixte pour l'aménagement et l'exploitation de la station d'Isola 2000 et à la SCP Fabiani, Luc-Thaler, Pinatel, avocat de la société d'aménagement d'Isola 2000.<br/>
<br/>
<br/>
<br/>Sur le cadre juridique applicable :<br/>
<br/>
              1. Considérant que si, en principe, il n'appartient pas au juge administratif d'intervenir dans l'exécution d'un contrat administratif en adressant des injonctions à ceux qui ont contracté avec l'administration lorsque celle-ci dispose à l'égard de ces derniers des pouvoirs nécessaires pour assurer l'exécution du contrat, il en va autrement quand l'administration ne peut user de moyens de contrainte à l'encontre de son cocontractant qu'en vertu d'une décision juridictionnelle, notamment après l'expiration des relations contractuelles ; qu'en pareille hypothèse, le juge du contrat est en droit de prononcer, à l'encontre du cocontractant de l'administration, une condamnation, éventuellement sous astreinte à une obligation de faire ; que la demande adressée en 2007 par la commune d'Isola et par le syndicat mixte pour l'aménagement et l'exploitation de la station d'Isola 2000 au tribunal administratif de Nice tendait précisément à obtenir, à la suite de la résiliation pour un motif d'intérêt général, le 6 mars 2001, de la convention d'aménagement conclue en 1992 par le syndicat mixte avec la société d'aménagement et de promotion de la station d'Isola (SAPSI), aux droits de laquelle est venue la société d'aménagement d'Isola 2000 (SAI 2000), la restitution, en application de l'article 20 de cette convention, des parcelles qui lui avaient été cédées par la commune ;<br/>
<br/>
              2. Considérant que le tribunal administratif de Nice, par un jugement du 9 mars 2012, et la cour administrative d'appel de Marseille, par un arrêt du 7 juillet 2014, confirmé sur ce point par la décision du 14 mars 2016 du Conseil d'Etat, statuant au contentieux, ont prononcé à l'encontre de la SAI 2000 une injonction de restituer lesdits terrains assortie d'une astreinte, en leur qualité de juge du contrat ; que la commune d'Isola et le syndicat mixte pour l'aménagement et l'exploitation de la station d'Isola 2000 ont, par la suite, saisi la cour administrative d'appel de Marseille pour lui demander d'assurer l'exécution de ces décisions ; que leurs conclusions doivent être regardées comme tendant à ce que le juge de l'exécution assure l'exécution effective des obligations de faire assorties d'une astreinte que le juge du contrat avait prescrites ;<br/>
<br/>
              3. Considérant que les dispositions du livre IX du code de justice administrative ne s'appliquent qu'aux injonctions et astreintes que, depuis la loi n° 80-539 du 16 juillet 1980 et la loi n° 95-125 du 8 février 1995, les juridictions administratives peuvent prononcer à l'encontre d'une personne morale de droit public ou d'un organisme privé chargé de la gestion d'un service public ; qu'elles ne sont, en revanche, pas applicables lorsque le juge du contrat, saisi par l'administration en vue de prononcer une obligation de faire à l'encontre de l'ancien cocontractant de l'administration, fait application du principe général selon lequel les juges ont la faculté de prononcer une injonction assortie d'une astreinte en vue de l'exécution de leurs décisions ;<br/>
<br/>
              Sur la juridiction compétente pour prononcer des mesures d'exécution : <br/>
<br/>
              4. Considérant que la juridiction compétente pour connaître d'une demande d'exécution du jugement d'un tribunal administratif est le tribunal qui a rendu cette décision ou, en cas d'appel, la juridiction d'appel, alors même que cette dernière aurait rejeté l'appel formé devant elle ; que la seule circonstance qu'un jugement ou un arrêt ait fait l'objet d'un pourvoi en cassation est sans incidence sur la compétence du tribunal administratif ou de la cour administrative d'appel pour prononcer les mesures qu'implique l'exécution de ce jugement ou de cet arrêt ; que, toutefois, il en va différemment dans l'hypothèse où un jugement ou un arrêt a fait l'objet d'un pourvoi en cassation et où le Conseil d'Etat règle l'affaire au fond, y compris lorsque le jugement ou l'arrêt n'a fait l'objet que d'une annulation partielle ; <br/>
<br/>
              5. Considérant que l'arrêt du 7 juillet 2014 de la cour administrative d'appel de Marseille a été annulé partiellement par la décision du 14 mars 2016 du Conseil d'Etat, statuant au contentieux ; que le Conseil d'Etat ayant, par cette même décision, réglé l'affaire au fond dans la mesure de la cassation prononcée, il est seul compétent pour prononcer les mesures qu'implique l'exécution de sa décision et de la partie du dispositif de l'arrêt de la cour qui est devenue définitive ; qu'il suit de là que la commune d'Isola et le syndicat mixte pour l'aménagement et l'exploitation de la station d'Isola 2000 sont fondés, sans qu'il soit besoin d'examiner le moyen de leur pourvoi, à demander l'annulation de l'arrêt attaqué par lequel la cour administrative d'appel de Marseille a statué sur leurs conclusions à fin d'exécution de son arrêt du 7 juillet 2014 ;<br/>
<br/>
              6. Considérant qu'il appartient au Conseil d'Etat de statuer, comme juge de l'exécution, sur les conclusions présentées par la commune d'Isola et le syndicat mixte pour l'aménagement et l'exploitation de la station d'Isola 2000 devant la cour administrative d'appel de Marseille ;<br/>
<br/>
<br/>
<br/>
<br/>
              Sur la portée de la chose jugée par le tribunal administratif de Nice, la cour administrative d'appel de Marseille et le Conseil d'Etat :<br/>
<br/>
              7. Considérant que la chose jugée résulte, en l'espèce, de la combinaison de l'article 1er du jugement du 9 mars 2012 du tribunal administratif de Nice, de la partie, devenue définitive, de l'arrêt du 7 juillet 2014 de la cour administrative d'appel de Marseille et de la décision du 14 mars 2016 du Conseil d'Etat statuant au contentieux ; qu'il résulte des dispositifs de ces décisions successives, d'une part, qu'il est enjoint à la SAI 2000 de procéder à la restitution à la commune d'Isola des parcelles lui appartenant, sous réserve du paiement à cette dernière société d'une somme de 2 196 617 euros actualisée selon l'indice du coût de la construction de l'Institut national de la statistique et des études économiques (INSEE) à la date du transfert de propriété ; que, d'autre part, cette injonction est assortie, à l'encontre de la SAI 2000, d'une astreinte de 1 000 euros par jour de retard à compter d'un délai de trois mois suivant la notification du jugement dans l'hypothèse où cette société s'opposerait à la restitution des parcelles ; que, par ailleurs, la SAI 2000 a droit, en exécution de l'arrêt de la cour, au versement de la plus-value apportée aux terrains sur lesquels un golf d'altitude de 18 trous et un circuit de glace ont été construits par l'aménageur, injonction étant faite au syndicat mixte pour l'aménagement et l'exploitation de la station d'Isola 2000 de saisir le service des Domaines pour qu'il évalue cette plus-value et, à défaut d'accord amiable sur cette base, à la partie la plus diligente de saisir le juge de l'expropriation ; qu'enfin, la SAI 2000 doit verser à la commune d'Isola et au syndicat mixte pour l'aménagement et l'exploitation de la station d'Isola 2000 la somme de 2 250 000 euros dont sera déduite une indemnité correspondant au prix de la cession de la parcelle AC n° 86 intervenue le 15 septembre 1970, augmenté pour tenir compte de l'évolution de l'indice du coût de la construction de l'INSEE entre cette date et le 28 novembre 2006 ;<br/>
<br/>
              8. Considérant que, contrairement à ce qui est soutenu par la SAI 2000, la restitution à la commune des parcelles restant la propriété de l'aménageur, ordonnée par l'article 1er du jugement du 9 mars 2012 du tribunal administratif de Nice, est subordonnée uniquement au paiement simultané de la somme de 2 196 617 euros actualisée ; qu'il résulte, en effet, de l'article 2 de l'arrêt du 7 juillet 2014 de la cour administrative d'appel de Marseille, qui n'a pas affecté l'article 1er du jugement frappé d'appel, que la détermination du montant de la plus-value apportée à certains terrains par l'aménageur, qui n'est pas enserrée dans le délai de trois mois fixé pour les restitutions et est assortie de modalités qui ne pourront être effectives que dans un délai excédant cette durée de trois mois, n'est pas une condition préalable pour que cette restitution intervienne ; que la restitution ainsi visée concerne également les parcelles ayant fait l'objet de la plus-value ; <br/>
<br/>
              Sur les conclusions tendant à la liquidation de l'astreinte :<br/>
<br/>
              9. Considérant que, dans les circonstances particulières rappelées au point 5, le Conseil d'Etat, statuant au contentieux, est compétent pour statuer sur les conclusions tendant à la liquidation de l'astreinte prononcée par le jugement du 9 mars 2012 du tribunal administratif de Nice tel qu'il a été réformé par l'arrêt de la cour ; <br/>
<br/>
              10. Considérant qu'ainsi qu'il a été dit au point 7, une astreinte provisoire de 1 000 euros par jour de retard a été prononcée à l'encontre de la SAI 2000, dans un délai de trois mois à compter de la notification du jugement du tribunal administratif, dans l'hypothèse où la restitution des parcelles ne serait pas intervenue du seul fait de cette société ; que le syndicat mixte pour l'aménagement et l'exploitation de la station d'Isola 2000 et la commune d'Isola demandent la liquidation de l'astreinte sur la période comprise entre le 22 janvier 2015 et la date de la décision du Conseil d'Etat à intervenir ; <br/>
<br/>
              11. Considérant qu'il résulte de l'instruction que le 22 janvier 2015, le syndicat mixte pour l'aménagement et l'exploitation de la station d'Isola 2000 et la commune d'Isola ont demandé à la SAI 2000 de comparaître devant un notaire afin de procéder au transfert de propriété des parcelles litigieuses moyennant le paiement des sommes dues en application de l'arrêt du 7 juillet 2014 de la cour administrative d'appel de Marseille ; que la SAI 2000 a refusé d'accéder à cette demande tant que ne lui serait pas versé le montant de la plus-value apportée aux terrains sur lesquels le golf d'altitude de 18 trous et le circuit de glace ont été construits par l'aménageur ; que, par un mémoire enregistré le 4 septembre 2015, la SAI 2000 a, conformément à l'article 2 de l'arrêt de la cour administrative d'appel, saisi le juge de l'expropriation aux fins de fixer l'indemnité au titre de la plus-value apportée par les travaux qu'elle avait réalisés sur certaines parcelles ; que par un jugement du 22 juin 2017, le juge de l'expropriation du tribunal de grande instance de Nice s'est déclaré incompétent au motif du caractère d'ordre public des règles relatives à la compétence du juge de l'expropriation, auxquelles l'article 20 de la convention de 1992 n'a pu déroger ; que, par un arrêt avant-dire droit du 5 juillet 2018, la cour d'appel d'Aix-en-Provence, saisie d'un appel contre ce jugement, a estimé que le litige ressortissait à la compétence des juridictions administratives et a renvoyé au tribunal des conflits le soin de décider de la question de compétence ainsi soulevée ;  <br/>
<br/>
              12. Considérant qu'ainsi qu'il a été indiqué au point 8, la restitution des parcelles n'était pas subordonnée à la fixation et au versement du montant de la plus-value apportée à certains terrains par l'aménageur ; que, toutefois, eu égard à la difficulté de déterminer la portée exacte de la chose jugée sur ce point par les différentes décisions juridictionnelles, laquelle est précisée par les points 7 et 8 de la présente décision, l'inexécution de l'injonction ne peut être regardée, dans les circonstances particulière de l'espèce, comme intervenue du seul fait de la SAI 2000 ; que, dans ces conditions, il n'y a pas lieu de procéder à la liquidation de l'astreinte prononcée à l'encontre de la SAI 2000 par le tribunal administratif de Nice ; <br/>
<br/>
              Sur l'édiction de nouvelles mesures d'exécution : <br/>
<br/>
              13. Considérant que, pour assurer l'exécution des mesures prescrites par le juge du contrat, telles qu'explicitées au point 8, il y a lieu d'enjoindre à la SAI 2000 de signer l'acte procédant au transfert de propriété des parcelles lui appartenant au bénéfice de la commune d'Isola, sous réserve du paiement simultané à cette dernière société d'une somme de 2 196 617 euros actualisée selon l'indice du coût de la construction de l'INSEE à la date du transfert de propriété ; que la restitution devra intervenir dans un délai de trois mois à compter de la notification de la présente décision ;<br/>
<br/>
              14. Considérant que la commune d'Isola et le syndicat mixte pour l'aménagement et l'exploitation de la station d'Isola 2000 demandent que le taux de l'astreinte prononcée par le tribunal administratif dans son jugement du 9 mars 2012 soit, à l'avenir, porté à 10 000 euros par jour de retard ; que ces conclusions doivent être regardées comme tendant à ce que la mesure d'injonction édictée au point précédent soit assortie d'une astreinte d'un tel montant ; que dans les circonstances de l'espèce, il y a lieu de prononcer contre la SAI 2000, à défaut pour elle de justifier de l'exécution de la présente décision dans le délai fixé au point précédent, une astreinte de 1 000 euros par jour de retard jusqu'à la date à laquelle cette décision aura reçu exécution ;<br/>
<br/>
              Sur les frais liés au litige :<br/>
<br/>
              15. Considérant que, dans les circonstances de l'espèce, il n'y a pas lieu de faire application des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 12 décembre 2016 de la cour administrative d'appel de Marseille est annulé.<br/>
Article 2 : Il est enjoint à la SAI 2000 de signer l'acte procédant au transfert de propriété des parcelles lui appartenant au bénéfice de la commune d'Isola, sous réserve du paiement simultané à cette dernière société d'une somme de 2 196 617 euros actualisée selon l'indice du coût de la construction de l'INSEE à la date du transfert de propriété, dans un délai de trois mois à compter de la notification de la présente décision.<br/>
Article 3 : Une astreinte de 1 000 euros par jour est prononcée à l'encontre de la SAI 2000 s'il n'est pas justifié de l'exécution de la présente décision dans le délai mentionné à l'article 2 ci-dessus. La SAI 2000 communiquera à la section du rapport et des études copies des actes justifiant des mesures prises pour exécuter la présente décision.<br/>
Article 4 : Le surplus des conclusions de la requête et les conclusions présentées par la SAI 2000 au titre de l'article L. 761-1 du code de justice administrative sont rejetés.<br/>
Article 5 : La présente décision sera notifiée à la commune d'Isola, au syndicat mixte pour l'aménagement et l'exploitation de la station d'Isola 2000 et à la société d'aménagement d'Isola 2000.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">17-05 COMPÉTENCE. COMPÉTENCE À L'INTÉRIEUR DE LA JURIDICTION ADMINISTRATIVE. - JURIDICTION COMPÉTENTE POUR CONNAÎTRE D'UNE DEMANDE D'EXÉCUTION D'UNE DÉCISION RENDUE PAR LE JUGE DU CONTRAT - 1) PRINCIPE - COMPÉTENCE DU TA AYANT RENDU CETTE DÉCISION OU, EN CAS D'APPEL, DE LA JURIDICTION D'APPEL [RJ3] - 2) CIRCONSTANCE QU'UN POURVOI A ÉTÉ FORMÉ DEVANT LE CONSEIL D'ETAT - CIRCONSTANCE SANS INCIDENCE, SAUF SI LE CONSEIL D'ETAT RÈGLE L'AFFAIRE AU FOND [RJ3], MÊME PARTIELLEMENT - COMPÉTENCE DU CONSEIL D'ETAT, DANS CETTE DERNIÈRE HYPOTHÈSE, POUR LIQUIDER L'ASTREINTE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">39-08-03-02 MARCHÉS ET CONTRATS ADMINISTRATIFS. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. POUVOIRS ET OBLIGATIONS DU JUGE. POUVOIRS DU JUGE DU CONTRAT. - 1) COMPÉTENCE DU JUGE DU CONTRAT POUR PRONONCER UNE INJONCTION ASSORTIE D'UNE ASTREINTE - EXISTENCE [RJ1] - APPLICABILITÉ DU LIVRE IX DU CJA - ABSENCE [RJ2] - 2) JURIDICTION COMPÉTENTE POUR CONNAÎTRE D'UNE DEMANDE D'EXÉCUTION D'UNE DÉCISION RENDUE PAR LE JUGE DU CONTRAT - A) PRINCIPE - COMPÉTENCE DU TA AYANT RENDU CETTE DÉCISION OU, EN CAS D'APPEL, DE LA JURIDICTION D'APPEL [RJ3] - B) CIRCONSTANCE QU'UN POURVOI A ÉTÉ FORMÉ DEVANT LE CONSEIL D'ETAT - CIRCONSTANCE SANS INCIDENCE, SAUF SI LE CONSEIL D'ETAT RÈGLE L'AFFAIRE AU FOND [RJ3], MÊME PARTIELLEMENT - COMPÉTENCE DU CONSEIL D'ETAT, DANS CETTE DERNIÈRE HYPOTHÈSE, POUR LIQUIDER L'ASTREINTE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-06-07 PROCÉDURE. JUGEMENTS. EXÉCUTION DES JUGEMENTS. - JURIDICTION COMPÉTENTE POUR CONNAÎTRE D'UNE DEMANDE D'EXÉCUTION D'UNE DÉCISION RENDUE PAR LE JUGE DU CONTRAT - 1) PRINCIPE - COMPÉTENCE DU TA AYANT RENDU CETTE DÉCISION OU, EN CAS D'APPEL, DE LA JURIDICTION D'APPEL [RJ3] - 2) CIRCONSTANCE QU'UN POURVOI A ÉTÉ FORMÉ DEVANT LE CONSEIL D'ETAT - CIRCONSTANCE SANS INCIDENCE, SAUF SI LE CONSEIL D'ETAT RÈGLE L'AFFAIRE AU FOND [RJ3], MÊME PARTIELLEMENT - COMPÉTENCE DU CONSEIL D'ETAT, DANS CETTE DERNIÈRE HYPOTHÈSE, POUR LIQUIDER L'ASTREINTE.
</SCT>
<ANA ID="9A"> 17-05 1) La juridiction compétente pour connaître d'une demande d'exécution du jugement d'un tribunal administratif est le tribunal qui a rendu cette décision ou, en cas d'appel, la juridiction d'appel, alors même que cette dernière aurait rejeté l'appel formé devant elle.... ,,2) La seule circonstance qu'un jugement ou un arrêt ait fait l'objet d'un pourvoi en cassation est sans incidence sur la compétence du tribunal administratif ou de la cour administrative d'appel pour prononcer les mesures qu'implique l'exécution de ce jugement ou de cet arrêt. Toutefois, il en va différemment dans l'hypothèse où un jugement ou un arrêt a fait l'objet d'un pourvoi en cassation et où le Conseil d'Etat règle l'affaire au fond, y compris lorsque le jugement ou l'arrêt n'a fait l'objet que d'une annulation partielle. Dans cette dernière hypothèse, le Conseil d'Etat statuant au contentieux est également compétent pour statuer sur les conclusions tendant à la liquidation de l'astreinte prononcée par un jugement ou un arrêt.</ANA>
<ANA ID="9B"> 39-08-03-02 1) Les dispositions du livre IX du code de justice administrative ne s'appliquent qu'aux injonctions et astreintes que, depuis la loi n° 80-539 du 16 juillet 1980 et la loi n° 95-125 du 8 février 1995, les juridictions administratives peuvent prononcer à l'encontre d'une personne morale de droit public ou d'un organisme privé chargé de la gestion d'un service public. Elles ne sont, en revanche, pas applicables lorsque le juge du contrat, saisi par l'administration en vue de prononcer une obligation de faire à l'encontre de l'ancien cocontractant de l'administration, fait application du principe général selon lequel les juges ont la faculté de prononcer une injonction assortie d'une astreinte en vue de l'exécution de leurs décisions.,,,2) a) La juridiction compétente pour connaître d'une demande d'exécution du jugement d'un tribunal administratif est le tribunal qui a rendu cette décision ou, en cas d'appel, la juridiction d'appel, alors même que cette dernière aurait rejeté l'appel formé devant elle. La seule circonstance qu'un jugement ou un arrêt ait fait l'objet d'un pourvoi en cassation est sans incidence sur la compétence du tribunal administratif ou de la cour administrative d'appel pour prononcer les mesures qu'implique l'exécution de ce jugement ou de cet arrêt. b) Toutefois, il en va différemment dans l'hypothèse où un jugement ou un arrêt a fait l'objet d'un pourvoi en cassation et où le Conseil d'Etat règle l'affaire au fond, y compris lorsque le jugement ou l'arrêt n'a fait l'objet que d'une annulation partielle. Dans cette dernière hypothèse, le Conseil d'Etat statuant au contentieux est également compétent pour statuer sur les conclusions tendant à la liquidation de l'astreinte prononcée par un jugement ou un arrêt.</ANA>
<ANA ID="9C"> 54-06-07 1) La juridiction compétente pour connaître d'une demande d'exécution du jugement d'un tribunal administratif est le tribunal qui a rendu cette décision ou, en cas d'appel, la juridiction d'appel, alors même que cette dernière aurait rejeté l'appel formé devant elle.... ,,2) La seule circonstance qu'un jugement ou un arrêt ait fait l'objet d'un pourvoi en cassation est sans incidence sur la compétence du tribunal administratif ou de la cour administrative d'appel pour prononcer les mesures qu'implique l'exécution de ce jugement ou de cet arrêt. Toutefois, il en va différemment dans l'hypothèse où un jugement ou un arrêt a fait l'objet d'un pourvoi en cassation et où le Conseil d'Etat règle l'affaire au fond, y compris lorsque le jugement ou l'arrêt n'a fait l'objet que d'une annulation partielle. Dans cette dernière hypothèse, le Conseil d'Etat statuant au contentieux est également compétent pour statuer sur les conclusions tendant à la liquidation de l'astreinte prononcée par un jugement ou un arrêt.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Section, 17 mars 1956, OPHLM du département de la Seine, n° 37656, p. 343 ; CE, 15 juin 2018, ADEME, n° 418493, à mentionner aux Tables., ,[RJ2] Rappr., s'agissant de contraventions de grande voirie, CE, 5 février 2014, Voies navigables de France, n° 364561, p. 19.,,[RJ3] Rappr., sur le fondement des articles L. 911-4 et L. 911-5 du CJA, CE, 24 février 2016,,, n° 391296, T. pp. 693-894.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
