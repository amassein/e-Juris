<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032103989</ID>
<ANCIEN_ID>JG_L_2016_02_000000391296</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/10/39/CETATEXT000032103989.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère SSJS, 24/02/2016, 391296</TITRE>
<DATE_DEC>2016-02-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>391296</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère SSJS</FORMATION>
<TYPE_REC>Action en astreinte</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Frédéric Puigserver</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2016:391296.20160224</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
Procédure contentieuse antérieure<br/>
<br/>
              Mme A...B...a demandé au tribunal administratif de Montpellier d'annuler pour excès de pouvoir l'arrêté du 10 novembre 2009 par lequel le président de la communauté d'agglomération de Montpellier a exercé le droit de préemption sur les parcelles cadastrées SM 22, 27 et le lot de copropriété n° 2 de la parcelle SM 25 à Montpellier. Par une ordonnance n° 0905557 du 31 mars 2010, le président de la 1ère chambre du tribunal administratif de Montpellier a constaté qu'il n'y avait plus lieu de statuer sur sa demande. <br/>
<br/>
              Mme B...a demandé au tribunal administratif de Montpellier d'annuler pour excès de pouvoir l'arrêté du président de la communauté d'agglomération de Montpellier du 29 décembre 2009 en tant qu'il préempte les parcelles cadastrées SM 22, 27 et le lot de copropriété n° 2 de la parcelle cadastrée SM 25. Par un jugement n° 1000502 du 18 novembre 2010, le tribunal administratif de Montpellier a annulé l'arrêté du 29 décembre 2009 dans cette mesure.<br/>
<br/>
              Par un arrêt n°s 10MA02144, 11MA03477, 11MA00254 du 9 février 2012, la cour administrative d'appel de Marseille a :<br/>
              - rejeté l'appel formé par la communauté d'agglomération de Montpellier contre le jugement du tribunal administratif de Montpellier du 18 novembre 2010 ;<br/>
              - annulé, à la demande de la communauté d'agglomération de Montpellier, l'ordonnance du président de la 1ère chambre du tribunal administratif de Montpellier du 31 mars 2010 et rejeté la demande de MmeB..., présentée à ce tribunal, tendant à l'annulation de la décision du 10 novembre 2009. <br/>
<br/>
              Par une décision n° 358438 du 17 juin 2014, le Conseil d'Etat, statuant au contentieux, à la demande de MmeB..., a annulé l'arrêt de la cour administrative d'appel de Marseille du 9 février 2012 en tant qu'il annule l'article 1er de la décision de la communauté d'agglomération de Montpellier du 29 décembre 2009 abrogeant la décision du 10 novembre 2009.<br/>
Procédure devant le Conseil d'Etat<br/>
<br/>
              Par une requête, enregistrée le 24 juin 2015 au secrétariat du contentieux du Conseil d'Etat, Mme B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'enjoindre à la communauté d'agglomération de Montpellier de proposer la rétrocession du bien préempté à la congrégation des Petites soeurs des pauvres dans un délai de quinze jours suivant la décision à intervenir et, en cas de refus de cette congrégation, de lui proposer la rétrocession du bien dans un délai de quinze jours suivant ce refus, sous astreinte de 1 000 euros par jour de retard passé chacun de ces délais ;<br/>
<br/>
              2°) de mettre à la charge de la communauté d'agglomération de Montpellier la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Puigserver, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 911-4 du code de justice administrative : " En cas d'inexécution d'un jugement ou d'un arrêt, la partie intéressée peut demander au tribunal administratif ou à la cour administrative d'appel qui a rendu la décision d'en assurer l'exécution. / Toutefois, en cas d'inexécution d'un jugement frappé d'appel, la demande d'exécution est adressée à la juridiction d'appel. / Si le jugement ou l'arrêt dont l'exécution est demandée n'a pas défini les mesures d'exécution, la juridiction saisie procède à cette définition. Elle peut fixer un délai d'exécution et prononcer une astreinte (...) ". Aux termes de l'article L. 911-5 du même code : " En cas d'inexécution d'une décision rendue par une juridiction administrative, le Conseil d'Etat peut, même d'office, prononcer une astreinte contre les personnes morales de droit public ou les organismes de droit privé chargés de la gestion d'un service public pour assurer l'exécution de cette décision. / Les dispositions du premier alinéa ne sont pas applicables dans les cas prévus aux articles L. 911-3 et L. 911-4 (...) ". Enfin, aux termes de l'article R. 931-3 du code de justice administrative : " Il peut être demandé au Conseil d'Etat de prononcer une astreinte pour assurer l'exécution d'une décision rendue par le Conseil d'Etat ou par une juridiction administrative spéciale (...) ".<br/>
<br/>
              2. Il résulte de ces dispositions que la juridiction compétente pour connaître d'une demande d'exécution du jugement d'un tribunal administratif est le tribunal qui a rendu cette décision ou, en cas d'appel, la juridiction d'appel, alors même que cette dernière aurait rejeté l'appel formé devant elle. En revanche, la seule circonstance qu'un jugement ou un arrêt ait fait l'objet d'un pourvoi en cassation, sans que le Conseil d'Etat règle l'affaire au fond, est sans incidence sur la compétence du tribunal administratif ou de la cour administrative d'appel pour prononcer les mesures qu'implique l'exécution de ce jugement ou de cet arrêt. <br/>
<br/>
              3. La demande présentée par Mme B...tend à l'exécution du jugement du 18 novembre 2010 par lequel le tribunal administratif de Montpellier a annulé pour excès de pouvoir l'arrêté du président de la communauté d'agglomération de Montpellier du 29 décembre 2009 en tant que, par son article 2, il préemptait les parcelles cadastrées SM 22, 27 et le lot de copropriété n° 2 de la parcelle cadastrée SM 25 à Montpellier. Si l'arrêt du 9 février 2012 par lequel la cour administrative d'appel de Marseille a rejeté l'appel formé par la communauté d'agglomération de Montpellier contre ce jugement a fait l'objet d'un pourvoi en cassation, le Conseil d'Etat, par sa décision du 17 juin 2014, s'est borné à annuler cet arrêt en tant qu'il annulait l'article 1er de l'arrêté du 29 décembre 2009 abrogeant une précédente décision de préemption portant sur les mêmes biens, sans procéder à un règlement de l'affaire au fond après cassation. La décision du Conseil d'Etat est ainsi sans incidence sur la compétence de la cour administrative d'appel de Marseille pour connaître de la demande de MmeB.... Dès lors, le Conseil d'Etat n'est pas compétent pour en connaître et il y a lieu de l'attribuer à la cour administrative d'appel de Marseille. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'affaire est attribuée à la cour administrative d'appel de Marseille.<br/>
Article 2 : La présente décision sera notifiée à Mme A...B..., à la communauté d'agglomération de Montpellier, à la congrégation des Petites soeurs des pauvres et à la présidente de la cour administrative d'appel de Marseille.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">17-05 COMPÉTENCE. COMPÉTENCE À L'INTÉRIEUR DE LA JURIDICTION ADMINISTRATIVE. - DEMANDE D'EXÉCUTION D'UN JUGEMENT DE TA - 1) COMPÉTENCE DU TA AYANT RENDU CETTE DÉCISION OU, EN CAS D'APPEL, JURIDICTION D'APPEL - 2) CIRCONSTANCE QU'UN POURVOI EN CASSATION A ÉTÉ FORMÉ DEVANT LE CONSEIL D'ETAT - CIRCONSTANCE SANS INCIDENCE, SAUF SI LE CONSEIL D'ETAT RÈGLE L'AFFAIRE AU FOND.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-06-07 PROCÉDURE. JUGEMENTS. EXÉCUTION DES JUGEMENTS. - JURIDICTION COMPÉTENTE POUR CONNAÎTRE D'UNE DEMANDE D'EXÉCUTION D'UN JUGEMENT DE TA - 1) COMPÉTENCE DU TA AYANT RENDU CETTE DÉCISION OU, EN CAS D'APPEL, JURIDICTION D'APPEL - 2) CIRCONSTANCE QU'UN POURVOI EN CASSATION A ÉTÉ FORMÉ DEVANT LE CONSEIL D'ETAT - CIRCONSTANCE SANS INCIDENCE, SAUF SI LE CONSEIL D'ETAT RÈGLE L'AFFAIRE AU FOND.
</SCT>
<ANA ID="9A"> 17-05 1) Il résulte des articles L. 911-4 et L. 911-5 du code de justice administrative que la juridiction compétente pour connaître d'une demande d'exécution du jugement d'un tribunal administratif est le tribunal qui a rendu cette décision ou, en cas d'appel, la juridiction d'appel, alors même que cette dernière aurait rejeté l'appel formé devant elle.... ,,2) La seule circonstance qu'un jugement ou un arrêt ait fait l'objet d'un pourvoi en cassation est sans incidence sur la compétence du tribunal administratif ou de la cour administrative d'appel pour prononcer les mesures qu'implique l'exécution de ce jugement ou de cet arrêt. Il en va différemment dans l'hypothèse où le Conseil d'Etat règle l'affaire au fond.</ANA>
<ANA ID="9B"> 54-06-07 1) Il résulte des articles L. 911-4 et L. 911-5 du code de justice administrative que la juridiction compétente pour connaître d'une demande d'exécution du jugement d'un tribunal administratif est le tribunal qui a rendu cette décision ou, en cas d'appel, la juridiction d'appel, alors même que cette dernière aurait rejeté l'appel formé devant elle.... ,,2) La seule circonstance qu'un jugement ou un arrêt ait fait l'objet d'un pourvoi en cassation est sans incidence sur la compétence du tribunal administratif ou de la cour administrative d'appel pour prononcer les mesures qu'implique l'exécution de ce jugement ou de cet arrêt. Il en va différemment dans l'hypothèse où le Conseil d'Etat règle l'affaire au fond.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
