<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034455166</ID>
<ANCIEN_ID>JG_L_2017_04_000000405164</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/45/51/CETATEXT000034455166.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 21/04/2017, 405164, Publié au recueil Lebon</TITRE>
<DATE_DEC>2017-04-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>405164</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP JEAN-PHILIPPE CASTON</AVOCATS>
<RAPPORTEUR>Mme Cécile Barrois de Sarigny</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier Domino</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:405164.20170421</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Le préfet de la Seine-Maritime a demandé au juge des référés du tribunal administratif de Rouen, statuant sur le fondement de l'article L. 521-3 du code de justice administrative, d'ordonner l'expulsion du dispositif d'accueil temporaire-service de l'asile de Caudebec-lès-Elbeuf, au besoin avec le concours de la force publique, de M. et Mme A...B.... <br/>
<br/>
              Par une ordonnance n° 1603362 du 3 novembre 2016, le juge des référés a rejeté cette demande. <br/>
<br/>
              Par un pourvoi, enregistré le 21 novembre 2016 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'intérieur demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette ordonnance ; <br/>
<br/>
              2°) statuant en référé, de faire droit à la demande d'expulsion de M. et Mme B.... <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'action sociale et des familles ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le code des procédures civiles d'exécution ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - la loi n° 2015-925 du 29 juillet 2015 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Cécile Barrois de Sarigny, maître des requêtes, <br/>
<br/>
              - les conclusions de M. Xavier Domino, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Jean-Philippe Caston, avocat de M. et Mme B...; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant, d'une part, que selon l'article L. 744-5 du code de l'entrée et du séjour des étrangers et du droit d'asile, issu de la loi du 29 juillet 2015 relative à la réforme du droit d'asile, les lieux d'hébergement pour demandeurs d'asile " accueillent les demandeurs d'asile pendant la durée d'instruction de leur demande d'asile ou jusqu'à leur transfert effectif vers un autre Etat européen. Cette mission prend fin à l'expiration du délai de recours contre la décision de l'Office français de protection des réfugiés et apatrides ou à la date de la notification de la décision de la Cour nationale du droit d'asile ou à la date du transfert effectif vers un autre Etat, si sa demande relève de la compétence de cet Etat. (...) / Un décret en Conseil d'Etat détermine les conditions dans lesquelles les personnes s'étant vu reconnaître la qualité de réfugié ou accorder le bénéfice de la protection subsidiaire et les personnes ayant fait l'objet d'une décision de rejet définitive peuvent être maintenues dans un lieu d'hébergement mentionné au même article L. 744-3 à titre exceptionnel et temporaire. / Lorsque, après une décision de rejet définitive, le délai de maintien dans un lieu d'hébergement mentionné audit article L. 744-3 prend fin, l'autorité administrative compétente peut, après mise en demeure restée infructueuse, demander en justice qu'il soit enjoint à cet occupant sans titre d'évacuer ce lieu. / Le quatrième alinéa du présent article est applicable aux personnes qui ont un comportement violent ou commettent des manquements graves au règlement du lieu d'hébergement. / La demande est portée devant le président du tribunal administratif, qui statue sur le fondement de l'article L. 521-3 du code de justice administrative et dont l'ordonnance est immédiatement exécutoire " ; <br/>
<br/>
              2.	Considérant, d'autre part, qu'aux termes de l'article L. 521-3 du code de justice administrative : " En cas d'urgence et sur simple requête qui sera recevable même en l'absence de décision administrative préalable, le juge des référés peut ordonner toutes autres mesures utiles sans faire obstacle à l'exécution d'aucune décision administrative " ; <br/>
<br/>
              3.	Considérant qu'il résulte de ces dispositions que, saisi par le préfet d'une demande tendant à ce que soit ordonnée l'expulsion d'un lieu d'hébergement pour demandeurs d'asile d'un demandeur d'asile dont la demande a été définitivement rejetée, le juge des référés du tribunal administratif y fait droit dès lors que la demande d'expulsion ne se heurte à aucune contestation sérieuse et que la libération des lieux présente un caractère d'urgence et d'utilité ;<br/>
<br/>
              4.	Considérant que, par l'ordonnance attaquée, le juge des référés du tribunal administratif de Rouen a rejeté la demande présentée par le préfet de la Seine-Maritime sur le fondement de l'article L. 744-5 du code de l'entrée et du séjour des étrangers et du droit d'asile, tendant à l'expulsion immédiate, au besoin avec le concours de la force publique, de M. et Mme B..., bénéficiaires d'un hébergement au sein du dispositif d'accueil temporaire-service de l'asile de Caudebec-lès-Elbeuf ; que le juge des référés s'est fondé sur les dispositions de l'article L. 412-6 du code des procédures civiles d'exécution pour rejeter la demande du préfet ;<br/>
<br/>
              5.	Considérant qu'aux termes de l'article L. 412-6 du code des procédures civiles d'exécution, dans sa rédaction en vigueur à la date de l'ordonnance attaquée : " Nonobstant toute décision d'expulsion passée en force de chose jugée et malgré l'expiration des délais accordés en vertu de l'article L. 412-3, il est sursis à toute mesure d'expulsion non exécutée à la date du 1er novembre de chaque année jusqu'au 31 mars de l'année suivante, à moins que le relogement des intéressés soit assuré dans des conditions suffisantes respectant l'unité et les besoins de la famille. / Toutefois, le juge peut supprimer le bénéfice du sursis prévu au premier alinéa lorsque les personnes dont l'expulsion a été ordonnée sont entrées dans les locaux par voie de fait " ; <br/>
<br/>
              6.	Considérant que ces dispositions du code des procédures civiles d'exécution ne sont pas applicables, en l'absence de disposition législative expresse, à la procédure d'expulsion des personnes se maintenant dans un lieu d'hébergement pour demandeurs d'asile organisée par l'article L. 744-5 du code de l'entrée et du séjour des étrangers et du droit d'asile ; que, par suite, en retenant que les dispositions de l'article L. 412-6 du code des procédures civiles d'exécution étaient applicables et faisaient obstacle à la demande d'expulsion présentée par le préfet de la Seine-Maritime, le juge des référés a commis une erreur de droit ; que le ministre de l'intérieur est, dès lors et sans qu'il soit besoin d'examiner l'autre moyen de son pourvoi, fondé à demander l'annulation de l'ordonnance qu'il attaque ; <br/>
<br/>
              7.	Considérant qu'il y a lieu, dans les circonstances de l'espèce et par application des dispositions de l'article L. 821-2 du code de justice administrative, de régler l'affaire au titre de la procédure de référé engagée ;<br/>
<br/>
              8.	Considérant qu'il résulte de l'instruction que la demande d'asile présentée par M. et MmeB..., de nationalité albanaise, hébergés au sein du dispositif d'accueil temporaire-service de l'asile de Caudebec-lès-Elbeuf, a été rejetée par décision du directeur général de l'Office français de protection des réfugiés et apatrides le 26 décembre 2014 ; que les recours dirigés contre ces refus ont été rejetés par la Cour nationale du droit d'asile le 28 janvier 2016 ; que la demande de réexamen ensuite présentée par les intéressés a été rejetée comme irrecevable par l'Office français de protection des réfugiés et apatrides le 25 mars 2016, puis par la Cour nationale du droit d'asile le 4 juillet 2016 ; qu'après que les intéressés ont été informés de la fin de leur prise en charge par le gestionnaire du centre de Caudebec-lès-Elbeuf, le préfet de la Seine-Maritime les a mis en demeure de quitter les lieux par lettre du 29 avril 2016 ; que le préfet a, le 20 octobre 2016, saisi le juge des référés en vue d'ordonner leur expulsion ; <br/>
<br/>
              9.	Considérant, en premier lieu, qu'il résulte de l'instruction que M. et Mme B... se maintiennent dans un lieu d'hébergement pour demandeurs d'asile alors que leur demande d'asile a été définitivement rejetée ; que la mesure d'expulsion ne se heurte donc, à cet égard, à aucune contestation sérieuse ; qu'en outre, si les requérants soutiennent que le signataire de la mise en demeure qui leur a été adressée le 26 avril 2016 aurait été incompétent pour ce faire, il résulte de l'instruction que M. C...bénéficiait d'une délégation de signature l'habilitant à signer cet acte au nom du préfet de la Seine-Maritime ; que la contestation nouée à cet égard ne présente pas davantage de caractère sérieux ; <br/>
<br/>
              10.	Considérant, en deuxième lieu, qu'ainsi qu'il a été dit au point 6, les dispositions de l'article L. 412-6 du code des procédures d'exécution ne font pas obstacle au prononcé de l'expulsion demandée par le préfet sur le fondement de l'article L. 744-5 du code de l'entrée et du séjour des étrangers et du droit d'asile ; <br/>
<br/>
              11.	Considérant, en troisième lieu, que la libération des lieux par les intéressés présente, eu égard aux besoins d'accueil des demandeurs d'asile et au nombre de places disponibles dans les lieux d'hébergement pour demandeurs d'asile en Seine-Maritime, un caractère d'urgence et d'utilité que la circonstance que les intéressés soient parents de deux enfants nés en 2006 et 2014 ne remet pas en cause ; <br/>
<br/>
              12.	Considérant qu'il résulte de ce qui précède qu'il y a lieu d'ordonner la libération par M. et Mme B...des lieux qu'ils occupent dans le centre d'accueil temporaire-service de l'asile de Caudebec-lès-Elbeuf, au besoin avec le concours de la force publique ;   <br/>
<br/>
              13.	Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de l'Etat, qui n'est pas dans la présence instance la partie perdante, au titre de l'article 37 de la loi du 10 juillet 1991 ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'ordonnance du 3 novembre 2016 du juge des référés du tribunal administratif de Rouen est annulée. <br/>
<br/>
Article 2 : Il est enjoint à M. et Mme B...de libérer des lieux qu'ils occupent dans le centre d'accueil temporaire-service de l'asile de Caudebec-lès-Elbeuf. <br/>
<br/>
Article 3 : Le préfet de la Seine-Maritime est autorisé à procéder, dans un délai de huit jours à compter de la notification de la présente décision, avec le concours de la force publique, à l'expulsion de M. et MmeB.... <br/>
<br/>
Article 4 : Les conclusions de la requête de M. et Mme B...au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 sont rejetées.<br/>
<br/>
Article 5 : La présente décision sera notifiée au ministre de l'intérieur et à M. et Mme A...B.... <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">095-02-06-02 - HÉBERGEMENT - DEMANDEUR D'ASILE DONT LA DEMANDE A ÉTÉ DÉFINITIVEMENT REJETÉE - DEMANDE TENDANT À CE QUE SON EXPULSION SOIT ORDONNÉE - 1) COMPÉTENCE DU JUGE ADMINISTRATIF - EXISTENCE (SOL. IMPL.) [RJ1] - 2) CONDITIONS POUR ORDONNER L'EXPULSION EN RÉFÉRÉ MESURES UTILES - A) PRINCIPE - B) CONDITION TENANT AU RESPECT DE LA TRÊVE HIVERNALE (ART. L. 412-6 DU CPCE) - ABSENCE - C) CONDITION TENANT À L'ABSENCE DE CONTESTATION SÉRIEUSE - I) REJET DÉFINITIF DE LA DEMANDE D'ASILE - CONDITION REMPLIE À L'ÉGARD DU DROIT D'ASILE - II) MOYEN TIRÉ DE CE QUE LE SIGNATAIRE DE LA MISE EN DEMEURE NE BÉNÉFICIAIT PAS D'UNE DÉLÉGATION DE SIGNATURE - OPÉRANCE - APPRÉCIATION AU TITRE DE CETTE CONDITION (SOL. IMPL.) - D) CONDITIONS D'URGENCE ET D'UTILITÉ - CONDITIONS REMPLIES EN PRINCIPE EU ÉGARD AUX BESOINS D'ACCUEIL DES DEMANDEURS D'ASILE ET AUX PLACES DISPONIBLES [RJ2].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">17-03-01-01 COMPÉTENCE. RÉPARTITION DES COMPÉTENCES ENTRE LES DEUX ORDRES DE JURIDICTION. COMPÉTENCE DÉTERMINÉE PAR DES TEXTES SPÉCIAUX. ATTRIBUTIONS LÉGALES DE COMPÉTENCE AU PROFIT DES JURIDICTIONS ADMINISTRATIVES. - DEMANDE TENDANT À CE QUE SOIT ORDONNÉE L'EXPULSION D'UN LIEU D'HÉBERGEMENT D'UN DEMANDEUR D'ASILE DONT LA DEMANDE A ÉTÉ DÉFINITIVEMENT REJETÉE (SOL. IMPL.) [RJ1].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-035-04-03 PROCÉDURE. PROCÉDURES INSTITUÉES PAR LA LOI DU 30 JUIN 2000. RÉFÉRÉ TENDANT AU PRONONCÉ DE TOUTES MESURES UTILES (ART. L. 521-3 DU CODE DE JUSTICE ADMINISTRATIVE). CONDITIONS D'OCTROI DE LA MESURE DEMANDÉE. - CONDITIONS D'OCTROI DE LA MESURE D'EXPULSION D'UN LIEU D'HÉBERGEMENT POUR DEMANDEURS D'ASILE D'UN DEMANDEUR D'ASILE DONT LA DEMANDE A ÉTÉ DÉFINITIVEMENT REJETÉE - 1) PRINCIPE - 2) CONDITION TENANT AU RESPECT DE LA TRÊVE HIVERNALE (ART. L. 412-6 DU CPCE) - ABSENCE - 3) CONDITION TENANT À L'ABSENCE DE CONTESTATION SÉRIEUSE - A) REJET DÉFINITIF DE LA DEMANDE D'ASILE - CONDITION REMPLIE À L'ÉGARD DU DROIT D'ASILE - B) MOYEN TIRÉ DE CE QUE LE SIGNATAIRE DE LA MISE EN DEMEURE NE BÉNÉFICIAIT PAS D'UNE DÉLÉGATION DE SIGNATURE - OPÉRANCE - APPRÉCIATION AU TITRE DE CETTE CONDITION (SOL. IMPL.) - 4) CONDITIONS D'URGENCE ET D'UTILITÉ - CONDITIONS REMPLIES EN PRINCIPE EU ÉGARD AUX BESOINS D'ACCUEIL DES DEMANDEURS D'ASILE ET AUX PLACES DISPONIBLES [RJ2].
</SCT>
<ANA ID="9A"> 095-02-06-02 1) Le juge administratif est compétent pour ordonner l'expulsion d'un lieu d'hébergement pour demandeurs d'asile mentionné à l'article L. 744-3 du code de l'entrée et du séjour des étrangers et du droit d'asile d'un demandeur d'asile dont la demande a été définitivement rejetée.,,,2) a) Saisi par le préfet d'une demande tendant à ce que soit ordonnée l'expulsion d'un lieu d'hébergement pour demandeurs d'asile d'un demandeur d'asile dont la demande a été définitivement rejetée, le juge des référés du tribunal administratif y fait droit dès lors que la demande d'expulsion ne se heurte à aucune contestation sérieuse et que la libération des lieux présente un caractère d'urgence et d'utilité.,,,b) L'article L. 412-6 du code des procédures civiles d'exécution (CPCE) n'est pas applicable, en l'absence de disposition législative expresse, à la procédure d'expulsion des personnes se maintenant dans un lieu d'hébergement pour demandeurs d'asile organisée par l'article L. 744-5 du code de l'entrée et du séjour des étrangers et du droit d'asile.,,,c) i) Dès lors que leur demande d'asile a été définitivement rejetée, la mesure d'expulsion des demandeurs ne se heurte, à l'égard du droit d'asile, à aucune contestation sérieuse.,,,ii) Le moyen tiré de ce que le signataire de la mise en demeure ne bénéficiait pas d'une délégation de signature l'habilitant à signer cet acte est opérant devant le juge du référé mesures utiles saisi de la demande d'expulsion. Il revient à ce dernier d'examiner ce moyen au titre de la condition d'absence de contestation sérieuse prévue par l'article L. 521-3 du code de justice administrative.,,,d) La libération des lieux par les intéressés présente, eu égard aux besoins d'accueil des demandeurs d'asile et au nombre de places disponibles dans les lieux d'hébergement pour demandeurs d'asile dans le département, un caractère d'urgence et d'utilité que la circonstance que les intéressés soient parents de deux enfants de trois et onze ans ne remet pas en cause.</ANA>
<ANA ID="9B"> 17-03-01-01 Il résulte de l'article L. 744-5 code de l'entrée et du séjour des étrangers et du droit d'asile que le juge administratif est compétent pour ordonner l'expulsion d'un lieu d'hébergement pour demandeurs d'asile mentionné à l'article L. 744-3 de ce code dont la demande a été définitivement rejetée.</ANA>
<ANA ID="9C"> 54-035-04-03 1) Saisi par le préfet d'une demande tendant à ce que soit ordonnée l'expulsion d'un lieu d'hébergement pour demandeurs d'asile d'un demandeur d'asile dont la demande a été définitivement rejetée, le juge des référés du tribunal administratif y fait droit dès lors que la demande d'expulsion ne se heurte à aucune contestation sérieuse et que la libération des lieux présente un caractère d'urgence et d'utilité.,,,2) L'article L. 412-6 du code des procédures civiles d'exécution (CPCE) n'est pas applicable, en l'absence de disposition législative expresse, à la procédure d'expulsion des personnes se maintenant dans un lieu d'hébergement pour demandeurs d'asile organisée par l'article L. 744-5 du code de l'entrée et du séjour des étrangers et du droit d'asile.,,,3) a) Dès lors que leur demande d'asile a été définitivement rejetée, la mesure d'expulsion des demandeurs ne se heurte, à l'égard du droit d'asile, à aucune contestation sérieuse.,,,b) Le moyen tiré de ce que le signataire de la mise en demeure ne bénéficiait pas d'une délégation de signature l'habilitant à signer cet acte est opérant devant le juge du référé-mesures utiles saisi de la demande d'expulsion. Il revient à ce dernier d'examiner ce moyen au titre de la condition d'absence de contestation sérieuse prévue par l'article L. 521-3 du code de justice administrative.,,,4) La libération des lieux par les intéressés présente, eu égard aux besoins d'accueil des demandeurs d'asile et au nombre de places disponibles dans les lieux d'hébergement pour demandeurs d'asile dans le département, un caractère d'urgence et d'utilité que la circonstance que les intéressés soient parents de deux enfants de trois et onze ans ne remet pas en cause.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Comp., dans l'état antérieur des textes, CE, 11 mai 2015, M. et Mme,, n° 384957, T. p. 603.,,[RJ2]Comp., en présence de circonstances exceptionnelles, décision du même jour, Ministre de l'intérieur c/ Mme,, n° 406065, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
