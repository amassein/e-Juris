<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025115818</ID>
<ANCIEN_ID>JG_L_2011_12_000000324310</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/11/58/CETATEXT000025115818.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 1ère sous-sections réunies, 30/12/2011, 324310</TITRE>
<DATE_DEC>2011-12-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>324310</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 1ère sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Jacques Arrighi de Casanova</PRESIDENT>
<AVOCATS>SCP GASCHIGNARD ; SCP PEIGNOT, GARREAU, BAUER-VIOLAS</AVOCATS>
<RAPPORTEUR>M. Marc Pichon de Vendeuil</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Cyril Roger-Lacan</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2011:324310.20111230</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et les mémoires complémentaires, enregistrés les 21 janvier, 21 avril et 20 novembre 2009 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. Gérard B, demeurant ... ; M. B demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 07VE00843 du 20 novembre 2008 par lequel la cour administrative d'appel de Versailles a rejeté sa requête tendant, d'une part, à l'annulation du jugement n° 0405752 du 31 janvier 2007 du tribunal administratif de Versailles rejetant sa demande d'annulation de l'arrêté du maire de Neuilly-sur-Seine du 24 août 2007 refusant de lui délivrer un permis de construire un immeuble d'habitation et de surélever un immeuble existant sur un terrain situé 17/19 boulevard Vital-Bouhot à Neuilly-sur-Seine, d'autre part, à l'annulation pour excès de pouvoir de cet arrêté ;<br/>
<br/>
              2°) de mettre à la charge de la commune de Neuilly-sur-Seine le versement de la somme de 4000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 14 décembre 2011, présentée pour M. B ;<br/>
<br/>
              Vu le code de l'environnement ;<br/>
<br/>
              Vu le code de l'urbanisme ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Marc Pichon de Vendeuil, chargé des fonctions de Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Gaschignard, avocat de M. B et de la SCP Peignot, Garreau, Bauer-Violas, avocat de la commune de Neuilly-sur-Seine, <br/>
<br/>
              - les conclusions de M. Cyril Roger-Lacan, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Gaschignard, avocat de M. B et à la SCP Peignot, Garreau, Bauer-Violas, avocat de la commune de Neuilly-sur-Seine ;<br/>
<br/>
<br/>
<br/>
<br/>
              Sans qu'il soit besoin d'examiner les autres moyens du pourvoi ;<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la décision de refus de permis de construire opposée à M. B est fondée sur l'incompatibilité de son projet avec la réglementation annexée au plan de prévention des risques d'inondation (PPRI) de la Seine dans les Hauts-de-Seine, approuvé par un arrêté préfectoral du 9 janvier 2004, au motif que le terrain d'implantation est situé en zone rouge de ce plan où les constructions nouvelles à usage d'habitation sont proscrites et l'extension de constructions est limitée à 20% de la surface hors oeuvre nette préexistante ; que le requérant a soutenu, tant en première instance qu'en appel, que ce terrain n'était pas submersible car situé à une cote supérieure à la cote de référence retenue par le plan, soit la cote maximale atteinte par le niveau des eaux lors de la crue centennale de 1910 ; que, dès lors, en affirmant qu'il n'était pas allégué par le requérant que la cote maximale de crue à laquelle il faisait référence correspondait à celle de la crue de 1910, la cour administrative d'appel de Versailles a dénaturé ses écritures ; qu'il suit de là que M. B est fondé à demander l'annulation de l'arrêt attaqué ;<br/>
<br/>
              Considérant que, dans les circonstances de l'espèce, il y a lieu, par application de l'article L. 821-2 du code de justice administrative, de régler l'affaire au fond ;<br/>
<br/>
              Sur la compétence de l'auteur de la décision attaquée :<br/>
<br/>
              Considérant que, s'agissant du moyen tiré de l'incompétence de l'auteur de l'acte attaqué à raison du caractère illisible de sa signature, M. B n'apporte aucun élément de nature à remettre en cause l'appréciation portée par le tribunal administratif de Versailles sur son argumentation de première instance ; qu'il y a lieu, dès lors, d'écarter ce moyen par adoption des motifs retenus par les premiers juges ;<br/>
<br/>
              Sur l'exception d'illégalité de l'arrêté préfectoral du 9 janvier 2004 approuvant le plan de prévention des risques d'inondation de la Seine dans les Hauts-de-Seine :<br/>
<br/>
              Considérant qu'à l'appui de ses conclusions dirigées contre la décision du 3 août 2004, M. B excipe de l'illégalité du plan de prévention des risques d'inondation de la Seine dans les Hauts-de-Seine qui, contrairement à ce qui est soutenu, est opposable à la décision attaquée ;<br/>
<br/>
              En ce qui concerne la légalité externe de cet arrêté :<br/>
<br/>
              Considérant, d'une part, qu'aux termes de l'article L. 600-1 du code de l'urbanisme : " L'illégalité pour vice de forme ou de procédure d'un schéma directeur, d'un schéma de cohérence territoriale, d'un plan d'occupation des sols, d'un plan local d'urbanisme, d'une carte communale ou d'un document d'urbanisme en tenant lieu ne peut être invoquée par voie d'exception, après l'expiration d'un délai de six mois à compter de la prise d'effet du document en cause. / Les dispositions de l'alinéa précédent sont également applicables à l'acte prescrivant l'élaboration ou la révision d'un document d'urbanisme ou créant une zone d'aménagement concerté. / Les deux alinéas précédents ne sont pas applicables lorsque le vice de forme concerne : / (...) / - soit la méconnaissance substantielle ou la violation des règles de l'enquête publique sur les schémas de cohérence territoriale, les plans locaux d'urbanisme et les cartes communales ; / - soit l'absence du rapport de présentation ou des documents graphiques. " ;<br/>
<br/>
              Considérant, d'autre part, qu'aux termes de l'article L. 562-1 du code de l'environnement, dans sa rédaction alors en vigueur : " I. - L'Etat élabore et met en application des plans de prévention des risques naturels prévisibles tels que les inondations, les mouvements de terrain, les avalanches, les incendies de forêt, les séismes, les éruptions volcaniques, les tempêtes ou les cyclones. / II. - Ces plans ont pour objet, en tant que de besoin : / 1° De délimiter les zones exposées aux risques, dites "zones de danger", en tenant compte de la nature et de l'intensité du risque encouru, d'y interdire tout type de construction, d'ouvrage, d'aménagement ou d'exploitation agricole, forestière, artisanale, commerciale ou industrielle ou, dans le cas où des constructions, ouvrages, aménagements ou exploitations agricoles, forestières, artisanales, commerciales ou industrielles pourraient y être autorisés, prescrire les conditions dans lesquelles ils doivent être réalisés, utilisés ou exploités ; 2° De délimiter les zones, dites "zones de précaution", qui ne sont pas directement exposées aux risques mais où des constructions, des ouvrages, des aménagements ou des exploitations agricoles, forestières, artisanales, commerciales ou industrielles pourraient aggraver des risques ou en provoquer de nouveaux et y prévoir des mesures d'interdiction ou des prescriptions telles que prévues au 1° ; 3° De définir les mesures de prévention, de protection et de sauvegarde qui doivent être prises, dans les zones mentionnées au 1° et au 2°, par les collectivités publiques dans le cadre de leurs compétences, ainsi que celles qui peuvent incomber aux particuliers ; 4° De définir, dans les zones mentionnées au 1° et au 2°, les mesures relatives à l'aménagement, l'utilisation ou l'exploitation des constructions, des ouvrages, des espaces mis en culture ou plantés existants à la date de l'approbation du plan qui doivent être prises par les propriétaires, exploitants ou utilisateurs (...) " ; qu'aux termes de l'article L. 562-4 du même code : " Le plan de prévention des risques naturels prévisibles approuvé vaut servitude d'utilité publique. Il est annexé au plan d'occupation des sols, conformément à l'article L. 126-1 du code de l'urbanisme. " ;<br/>
<br/>
              Considérant qu'il résulte de la combinaison de ces dispositions que les plans de prévention des risques naturels prévisibles constituent des documents qui, élaborés à l'initiative de l'Etat, ont pour objet de définir des zones exposées à des risques naturels à l'intérieur desquelles s'appliquent des contraintes d'urbanisme importantes et ont ainsi pour effet de déterminer des prévisions et règles opposables aux personnes publiques ou privées au titre de la délivrance des autorisations d'urbanisme qu'elles sollicitent ; que, par suite, les plans de prévention des risques naturels constituent des documents d'urbanisme tenant lieu de plan d'occupation des sols ou de plan local d'urbanisme, au sens des dispositions de l'article L. 600-1 du code de l'urbanisme ;<br/>
<br/>
              Considérant qu'il suit de là que M. B n'était plus recevable le 10 avril 2007, date d'enregistrement de sa requête au greffe de la cour administrative d'appel de Versailles, à se prévaloir pour la première fois, par voie d'exception, de l'illégalité du PPRI de la Seine dans les Hauts-de-Seine approuvé le 9 janvier 2004 en invoquant des moyens tirés respectivement de l'illégalité de l'arrêté préfectoral du 29 avril 1998 prescrivant l'élaboration de ce plan à raison de l'irrégularité de ses modalités d'affichage et de l'insuffisance de la consultation menée auprès des communes concernées, et de l'insuffisance de l'affichage de l'avis d'ouverture d'enquête publique ;<br/>
<br/>
              Considérant, en revanche, qu'en vertu des cinquième et sixième alinéas de l'article L. 600-1 du code de l'urbanisme, M. B était recevable à invoquer les vices substantiels dont aurait été entachée, selon lui, la procédure d'enquête publique préalable à ce plan, ainsi que l'absence de rapport de présentation ; <br/>
<br/>
              Considérant toutefois, d'une part, que le moyen tiré de ce que le rapport de présentation du plan ferait défaut manque en fait ; <br/>
<br/>
              Considérant, d'autre part, que M. B soutient que la procédure d'enquête publique s'est déroulée en méconnaissance des conditions prévues aux articles L. 123-1 et suivants du code de l'environnement ; que, si, en vertu des dispositions de l'article L. 562-3 du code de l'environnement, dans sa rédaction issue de la loi du 30 juillet 2003, l'enquête publique préalable à l'adoption d'un plan de prévention des risques naturels prévisibles doit être organisée dans les conditions prévues aux articles L. 123-1 et suivants du même code, il résulte des dispositions de l'article L. 562-7 de ce code que les conditions d'application de l'article L. 562-3 devaient être précisées par un décret en Conseil d'Etat, qui n'a été édicté que le 4 janvier 2005 et qui a précisé que ses dispositions ne seraient applicables qu'aux enquêtes publiques prescrites par un arrêté d'ouverture pris postérieurement au dernier jour du dernier mois suivant sa publication ; que, par suite, le requérant n'est pas fondé à soutenir que l'irrégularité de l'enquête publique aurait vicié la procédure ; <br/>
<br/>
              En ce qui concerne la légalité interne :<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier que le PPRI litigieux a entendu préserver l'état des berges du fleuve et des îles, à la fois en raison des risques d'inondations auxquels elles sont exposées et en vue de conserver des zones d'expansion des crues ; qu'à cette fin, ces catégories de terrain sont classées en " zone rouge " du PPRI, définie par ce document comme " zone à forts aléas et zone à préserver pour la capacité de stockage de la crue quel que soit le niveau d'aléas " et couvrant notamment les " espaces non encore urbanisés, insérés dans le tissus urbain, ainsi que des berges du fleuve (...) " ; qu'en zone rouge, les constructions nouvelles à usage d'habitation sont interdites, les extensions des constructions existantes limitées tandis qu'est prohibée toute construction sur une marge de recul de 30 mètres par rapport aux berges ;<br/>
<br/>
              Considérant, en premier lieu, que la zone rouge ainsi définie ne méconnaît pas les prescriptions des dispositions du 1° et du 2° du II de l'article L. 562-1 du code de l'environnement rappelées ci-dessus, celles-ci ne faisant pas obstacle, le cas échéant, à ce qu'une même zone d'un plan regroupe des secteurs correspondant à une " zone de danger " et à une " zone de précaution " au sens de la loi, à condition qu'il soit justifié, comme en l'espèce, qu'ils obéissent aux mêmes prescriptions ou interdictions prévues par le plan ; <br/>
<br/>
              Considérant, en deuxième lieu, que le terrain de M. B est situé sur les berges de l'île de la Jatte ; que s'il ressort de la carte des aléas que la partie de l'île où il est localisé n'a pas été submergée lors de la crue de 1910, les parties sud-ouest et nord-ouest ont en revanche été couvertes par plus de 2 mètres d'eau ; que, d'une part, une marge d'incertitude s'attache nécessairement aux prévisions quant aux inondations qui résulteraient d'un événement de même ampleur, eu égard en particulier aux changements de circonstances intervenus depuis 1910 ; que, d'autre part, il ressort de la note de présentation qu'en cas de nouvelle crue centennale, les berges de la Seine seraient soumises à un très fort courant ; qu'enfin la préservation de la capacité des champs d'expansion des crues, qui permet de limiter leur impact en aval, présente un caractère d'intérêt général et justifie que puissent être déclarées inconstructibles ou enserrées dans des règles de constructibilité limitée, des zones ne présentant pas un niveau d'aléa fort ; qu'ainsi, le moyen tiré de ce que le classement en zone rouge du terrain appartenant à M. B serait entaché d'erreur manifeste d'appréciation doit être écarté ;<br/>
<br/>
              Considérant, en dernier lieu, que M. B soutient que le plan litigieux méconnaît le principe d'égalité au motif qu'ont été classés dans des zones différentes des terrains situés à une même hauteur d'eau selon qu'ils sont situés à plus ou moins trente mètres des berges de la Seine ; que, contrairement à ce qui est soutenu, un tel critère de distinction, qui est adapté à l'objectif de prévention des risques d'inondation poursuivi par le plan et qui n'est pas entaché de disproportion manifeste, ne méconnaît pas le principe d'égalité ;<br/>
<br/>
              Considérant qu'il résulte de ce qui précède que M. B n'est pas fondé à se plaindre de ce que, par le jugement attaqué, le tribunal administratif de Versailles a rejeté sa demande ;<br/>
<br/>
              Sur les conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              Considérant que ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de la commune de Neuilly-sur-Seine, qui n'est pas la partie perdante dans la présente instance ; qu'il y a lieu, en revanche, dans les circonstances de l'espèce, de mettre à la charge de M. B le versement à la commune de Neuilly-sur-Seine de la somme de 2 000 euros au même titre ;<br/>
<br/>
<br/>			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : L'arrêt du 20 novembre 2008 de la cour administrative d'appel de Versailles est annulé.<br/>
<br/>
Article 2 : La requête présentée par M. B devant la cour administrative d'appel de Versailles est rejetée.<br/>
<br/>
		Article 3 : Le surplus des conclusions du pourvoi de M. B est rejeté.<br/>
<br/>
Article 4 : M. B versera à la commune de Neuilly-sur-Seine une somme de 2 000 euros en application de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 5 : La présente décision sera notifiée à M. Gérard B et au maire de Neuilly-sur- Seine.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-07-01-04-04-01 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. QUESTIONS GÉNÉRALES. MOYENS. EXCEPTION D'ILLÉGALITÉ. IRRECEVABILITÉ. - CONTESTATION PAR LA VOIE DE L'EXCEPTION DE L'ILLÉGALITÉ D'UN DOCUMENT D'URBANISME TENANT LIEU DE POS OU DE PLU APRÈS L'EXPIRATION D'UN DÉLAI DE SIX MOIS À COMPTER DE SA PRISE D'EFFET (ART. L. 600-1 DU CODE DE L'URBANISME) - NOTION DE DOCUMENT D'URBANISME TENANT LIEU DE POS OU DE PLU - PLAN DE PRÉVENTION DES RISQUES NATURELS PRÉVISIBLES - INCLUSION.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">68-01 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PLANS D'AMÉNAGEMENT ET D'URBANISME. - PLANS DE PRÉVENTION DES RISQUES NATURELS PRÉVISIBLES - CARACTÈRE DE DOCUMENTS D'URBANISME TENANT LIEU DE POS OU DE PLU AU SENS DES DISPOSITIONS DE L'ARTICLE L. 600-1 DU CODE DE L'URBANISME - EXISTENCE - CONSÉQUENCE - IMPOSSIBILITÉ D'EXCIPER DE LEUR ILLÉGALITÉ POUR VICE DE FORME OU DE PROCÉDURE APRÈS L'EXPIRATION D'UN DÉLAI DE SIX MOIS À COMPTER DE LEUR PRISE D'EFFET.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">68-01-01-01 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PLANS D'AMÉNAGEMENT ET D'URBANISME. PLANS D'OCCUPATION DES SOLS ET PLANS LOCAUX D'URBANISME. LÉGALITÉ DES PLANS. - ABSENCE, EN RAISON D'UN VICE DE FORME OU DE PROCÉDURE - IMPOSSIBILITÉ D'EN EXCIPER APRÈS L'EXPIRATION D'UN DÉLAI DE SIX MOIS À COMPTER DE LA PRISE D'EFFET DU DOCUMENT (ART. L. 600-1 DU CODE DE L'URBANISME - CHAMP D'APPLICATION DE CETTE RÈGLE - PLANS DE PRÉVENTION DES RISQUES NATURELS PRÉVISIBLES - INCLUSION.
</SCT>
<ANA ID="9A"> 54-07-01-04-04-01 Les plans de prévention des risques naturels prévisibles constituent des documents d'urbanisme tenant lieu de plan d'occupation des sols (POS) ou de plan local d'urbanisme (PLU) au sens des dispositions de l'article L. 600-1 du code de l'urbanisme. Par suite et sous réserve des dispositions des quatrième à sixième alinéas de cet article, leur illégalité pour vice de forme ou de procédure ne peut être invoquée par voie d'exception après l'expiration d'un délai de six mois à compter de leur prise d'effet.</ANA>
<ANA ID="9B"> 68-01 Les plans de prévention des risques naturels prévisibles constituent des documents d'urbanisme tenant lieu de plan d'occupation des sols (POS) ou de plan local d'urbanisme (PLU) au sens des dispositions de l'article L. 600-1 du code de l'urbanisme. Par suite et sous réserve des dispositions des quatrième à sixième alinéas de cet article, leur illégalité pour vice de forme ou de procédure ne peut être invoquée par voie d'exception après l'expiration d'un délai de six mois à compter de leur prise d'effet.</ANA>
<ANA ID="9C"> 68-01-01-01 Les plans de prévention des risques naturels prévisibles constituent des documents d'urbanisme tenant lieu de plan d'occupation des sols ou de plan local d'urbanisme au sens des dispositions de l'article L. 600-1 du code de l'urbanisme. Par suite et sous réserve des dispositions des quatrième à sixième alinéas de cet article, leur illégalité pour vice de forme ou de procédure ne peut être invoquée par voie d'exception après l'expiration d'un délai de six mois à compter de leur prise d'effet.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
