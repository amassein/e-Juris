<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036771642</ID>
<ANCIEN_ID>JG_L_2018_04_000000407032</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/77/16/CETATEXT000036771642.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 04/04/2018, 407032</TITRE>
<DATE_DEC>2018-04-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>407032</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Contentieux des pensions</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ODENT, POULET ; CORLAY</AVOCATS>
<RAPPORTEUR>M. Marc Pichon de Vendeuil</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:407032.20180404</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Poitiers d'annuler la décision du 25 janvier 2012 par laquelle le directeur de la Caisse nationale de retraites des agents des collectivités locales (CNRACL) a rejeté sa demande de validation des services accomplis en tant qu'agent non titulaire. Par un jugement n° 1201318 du 21 mai 2014, le tribunal administratif de Poitiers a fait droit à sa demande.<br/>
<br/>
              Par un arrêt n° 14BX02019 du 21 novembre 2016, la cour administrative d'appel de Bordeaux a, sur appel formé par la CNRACL, annulé le jugement du tribunal administratif de Poitiers et rejeté la demande de M.A....<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 20 janvier et 20 avril 2017 au secrétariat du contentieux du Conseil d'Etat, M. A...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel formé par la CNRACL ;<br/>
<br/>
              3°) de mettre à la charge de la CNRACL la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code des pensions civiles et militaires de retraite ;<br/>
              - le décret n° 2003-1306 du 26 décembre 2003 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Marc Firoud, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Corlay, avocat de M. A...et à la SCP Odent, Poulet, avocat de la Caisse des dépôts et consignations.<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis au juge du fond que M.A..., agent contractuel de la commune de La Flotte (Charente-Maritime), a été titularisé, à compter du 1er octobre 2009, en qualité d'adjoint technique territorial de 2ème classe ; qu'il a, le 16 octobre 2009, déposé auprès de son employeur une demande de validation des services accomplis en tant qu'agent contractuel en vue de la constitution de ses droits à pension de retraite ; que la commune n'a transmis cette demande que le 17 janvier 2012 à la Caisse nationale de retraites des agents des collectivités locales (CNRACL) ; que celle-ci a rejeté la demande par une décision du 25 janvier 2012 au motif que le délai de deux ans dont disposait l'agent à compter de sa titularisation était expiré ; que, par un jugement du 21 mai 2014, le tribunal administratif de Poitiers a annulé cette décision à la demande de M. A...; que, par un arrêt du 21 novembre 2016, contre lequel M. A...se pourvoit en cassation, la cour administrative d'appel de Bordeaux a annulé ce jugement et rejeté sa demande ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article R. 811-1 du code de justice administrative : " (...) le tribunal administratif statue en premier et dernier ressort :/ (...) 7° Sur les litiges en matière de pensions " ; <br/>
<br/>
              3. Considérant que le recours juridictionnel exercé contre une décision par laquelle le directeur de la CNRACL rejette une demande de validation de services entre dans la catégorie des litiges relatifs aux pensions pour lesquels, en application des dispositions du 7° de l'article R. 811-1 précité du code de justice administrative, le tribunal administratif statue en premier et dernier ressort ; qu'il suit de là que les conclusions de la CNRACL tendant à l'annulation du jugement du tribunal administratif de Poitiers du 21 mai 2014, présentées devant la cour administrative d'appel de Bordeaux, revêtaient le caractère d'un pourvoi en cassation ; que, par suite, en statuant sur ces conclusions, la cour administrative d'appel de Bordeaux a méconnu les règles régissant sa compétence ; que, dès lors et sans qu'il soit besoin d'examiner les moyens du pourvoi de M.A..., son arrêt doit être annulé ;<br/>
<br/>
              4. Considérant qu'il appartient au Conseil d'Etat de statuer, en tant que juge de cassation, sur le pourvoi formé par la CNRACL contre le jugement du 21 mai 2014 du tribunal administratif de Poitiers ; <br/>
<br/>
              5. Considérant que si M. A...fait valoir que la CNRACL a fait droit à sa demande de validation de services par une décision du 11 juin 2014, devenue définitive selon lui le 19 novembre 2015, il ressort des pièces du dossier soumis aux juges du fond que celle-ci n'a été motivée que par le souci de se conformer au jugement du 21 mai 2014, qui était immédiatement exécutoire, et ne prive donc pas d'objet les conclusions de la CNRACL tendant à l'annulation de ce jugement ; que, par suite, les conclusions à fin de non-lieu de M. A...ne peuvent qu'être rejetées ; <br/>
<br/>
              6. Considérant qu'aux termes de l'article 8 du décret du 26 décembre 2003 relatif au régime des retraites des fonctionnaires affiliés à la Caisse nationale de retraites des agents des collectivités locales : " Les services pris en compte dans la constitution du droit à pension sont :/ (...) 2° Les périodes de services dûment validées pour les fonctionnaires titularisés au plus tard le 1er janvier 2013. Est admise à validation toute période de services, quelle qu'en soit la durée, effectués en qualité d'agent non titulaire auprès de l'un des employeurs mentionnés aux 1°, 2° et 3° de l'article L. 86-1 du code des pensions civiles et militaires de retraite (...) " ; qu'aux termes du I de l'article 50 du même décret, dans sa rédaction applicable au litige : " La validation des services visés à l'article 8 doit être demandée dans les deux années qui suivent la date de la notification de la titularisation (...) " ; <br/>
<br/>
              7. Considérant que le respect du délai de deux ans prévu par le I de l'article 50 du décret du 26 décembre 2003 cité au point précédent, durant lequel un fonctionnaire territorial peut, à compter de la notification de sa titularisation, demander la validation de ses services accomplis antérieurement en qualité d'agent non titulaire, s'apprécie à la date de réception de cette demande par son employeur ou, lorsqu'elle est saisie directement par l'agent, par la CNRACL ; <br/>
<br/>
              8. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. A...a été titularisé à compter du 1er octobre 2009 par une décision notifiée le 30 septembre 2009 ; que, le 16 octobre 2009, M. A...a saisi son employeur, la commune de La Flotte, d'une demande de validation de ses services accomplis en qualité d'agent non titulaire ; que la commune n'a transmis cette demande à la CNRACL que le 17 janvier 2012 ; qu'il résulte de ce qui vient d'être dit que le tribunal administratif de Poitiers n'a pas commis d'erreur de droit en jugeant qu'en dépit de la date figurant sur le formulaire de demande et du retard mis par la commune à transmettre celui-ci à la CNRACL, M. A...devait être regardé comme ayant déposé sa demande dans le délai de deux ans prévu par le I de l'article 50 du décret du 26 décembre 2003 ; <br/>
<br/>
              9. Considérant qu'il résulte de ce qui précède que la CNRACL n'est pas fondée à demander l'annulation du jugement du tribunal administratif de Poitiers du 21 mai 2014 ; <br/>
<br/>
              10. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la Caisse des dépôts et consignations la somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative pour l'ensemble de la procédure ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 21 novembre 2016 de la cour administrative d'appel de Bordeaux est annulé.<br/>
Article 2 : Le pourvoi de la CNRACL contre le jugement du tribunal administratif de Poitiers du 21 mai 2014 est rejeté.<br/>
Article 3 : La Caisse des dépôts et consignations versera à M. A...une somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative pour l'ensemble de la procédure.<br/>
Article 4 : La présente décision sera notifiée à M. B...A...et à la Caisse des dépôts et consignations.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">48-02-02-02 PENSIONS. PENSIONS CIVILES ET MILITAIRES DE RETRAITE. PENSIONS CIVILES. CONDITIONS D'OUVERTURE DU DROIT À PENSION. - CONSTITUTION DU DROIT À PENSION DES FONCTIONNAIRES AFFILIÉS À LA CNRACL - PRISE EN COMPTE DES PÉRIODES DE SERVICES EFFECTUÉS EN QUALITÉ D'AGENT NON TITULAIRE - CONDITION - VALIDATION DANS UN DÉLAI DE DEUX ANS SUIVANT LA DATE DE NOTIFICATION DE LA TITULARISATION - COMPUTATION DU DÉLAI DE DEUX ANS.
</SCT>
<ANA ID="9A"> 48-02-02-02 Le respect du délai de deux ans prévu par le I de l'article 50 du décret n° 2003-1306 du 26 décembre 2003, dans sa rédaction antérieure au décret n° 2015-788, durant lequel un fonctionnaire territorial peut, à compter de la notification de sa titularisation, demander la validation de ses services accomplis antérieurement en qualité d'agent non titulaire, s'apprécie à la date de réception de cette demande par son employeur ou, lorsqu'elle est saisie directement, par la Caisse nationale de retraite des agents des collectivités territoriales (CNRACL).</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
