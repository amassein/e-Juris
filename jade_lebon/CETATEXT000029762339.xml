<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029762339</ID>
<ANCIEN_ID>JG_L_2014_11_000000374450</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/76/23/CETATEXT000029762339.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème / 2ème SSR, 14/11/2014, 374450</TITRE>
<DATE_DEC>2014-11-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>374450</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème / 2ème SSR</FORMATION>
<TYPE_REC>Contentieux des pensions</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MASSE-DESSEN, THOUVENIN, COUDRAY</AVOCATS>
<RAPPORTEUR>Mme Charline Nicolas</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Bertrand Dacosta</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:374450.20141114</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu l'ordonnance n° 11PA03141 du 13 décembre 2013, enregistrée le 7 janvier 2014 au secrétariat du contentieux du Conseil d'Etat, par laquelle le président de la cour administrative d'appel de Paris a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, le pourvoi présenté à cette cour par le ministre de l'éducation nationale, de la jeunesse et de la vie associative ; <br/>
<br/>
              Vu le pourvoi, enregistré le 11 juillet 2011 au greffe de la cour administrative d'appel de Paris, présenté par le ministre de l'éducation nationale, de la jeunesse et de la vie associative ; le ministre demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler les articles 1er et 2 du jugement n° 0901248/5-2 du 12 mai 2011 par lesquels le tribunal administratif de Paris a, d'une part, annulé sa décision du 30 juin 2008 refusant à M. B...A...tout droit à pension civile et le rétablissant dans ses droits à pension au regard du régime général de sécurité sociale pour la période durant laquelle son traitement était soumis aux retenues pour pension civile, ainsi que celle du 29 octobre 2008 rejetant le recours gracieux de M. A...contre cette décision, et, d'autre part, lui a enjoint de réexaminer la situation administrative de M. A...au regard de ses services effectifs ouvrant droit à pension de retraite civile en vertu de l'article L. 5 du code des pensions civiles et militaires de retraite ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de première instance de M. A... ;<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code des pensions civiles et militaires de retraite ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Charline Nicolas, auditeur,  <br/>
<br/>
              - les conclusions de M. Bertrand Dacosta, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Masse-Dessen, Thouvenin, Coudray, avocat de M. A...;<br/>
<br/>
<br/>
<br/>1. Considérant que, d'une part, aux termes du 1° de l'article L. 4 du code des pensions civiles et militaires de retraite, dans sa rédaction applicable aux faits de l'espèce, le droit à pension est acquis : " Aux fonctionnaires après quinze années accomplies de services civils et militaires effectifs " ; qu'aux termes de l'article L. 5 du même code : " Les services pris en compte dans la constitution du droit à pension sont : 1° Les services accomplis par les fonctionnaires titulaires et stagiaires mentionnés à l'article 2 de la loi n° 83-634 du 13 juillet 1983 précitée ; 2° Les services militaires (...) " ; que, d'autre part, l'article L. 11 du même code dispose que : " Les services pris en compte dans la liquidation de la pension sont : 1° Pour les fonctionnaires civils, les services énumérés à l'article L. 5 (...) " ; qu'enfin, aux termes de l'article L. 12 du même code " Aux services effectifs s'ajoutent, dans les conditions déterminées par un décret en Conseil d'Etat, les bonifications ci-après : a) Bonification de dépaysement pour les services civils rendus hors d'Europe (...) " ;<br/>
<br/>
              2. Considérant qu'il résulte de ces dispositions que, si des bonifications peuvent venir s'ajouter aux services effectifs accomplis par l'agent pour le calcul du montant de sa pension au moment de sa liquidation, seuls les services effectifs mentionnés aux articles L. 4 et L. 5 du code des pensions civiles et militaires sont pris en compte pour la constitution du droit à pension lui-même ; qu'en conséquence, les durées calculées au titre de la bonification de dépaysement pour les services civils rendus hors d'Europe, prévue au a) de l'article L. 12, ne peuvent s'ajouter à la durée de services effectifs pour déterminer si un droit à pension est ouvert ; qu'il résulte de ce qui précède que le tribunal administratif de Paris a commis une erreur de droit en annulant la décision du 30 juin 2008 et, par voie de conséquence, la décision du 29 octobre 2008 rejetant le recours gracieux de M.A..., au motif que les bonifications pour dépaysement auxquelles il a droit à raison des missions accomplies hors d'Europe n'avaient pas été prises en compte par l'administration dans le calcul des services effectifs pour la constitution de son droit à pension ; que dès lors, le ministre de l'éducation nationale est fondé à demander l'annulation du jugement du tribunal administratif de Paris du 12 mai 2011 ;<br/>
<br/>
              3. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'il soit fait droit aux conclusions présentées sur le fondement de ces dispositions par M. A...;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Paris du 12 mai 2011 est annulé.<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Paris.<br/>
Article 3 : Les conclusions présentées par M. A...au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à la ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche et à M. B...A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">48-02-01-04-02 PENSIONS. PENSIONS CIVILES ET MILITAIRES DE RETRAITE. QUESTIONS COMMUNES. LIQUIDATION DES PENSIONS. SERVICES EFFECTIFS. - PRISE EN COMPTE DES BONIFICATIONS DANS LE CALCUL DE LA DURÉE DE SERVICES EFFECTIFS OUVANT DROIT À PENSION - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">48-02-01-04-03 PENSIONS. PENSIONS CIVILES ET MILITAIRES DE RETRAITE. QUESTIONS COMMUNES. LIQUIDATION DES PENSIONS. BONIFICATIONS. - BONIFICATIONS S'AJOUTANT AUX SERVICES EFFECTIVEMENT ACCOMPLIS PAR L'AGENT - NOTION - PRISE EN COMPTE POUR LE CALCUL DE LA DURÉE DE SERVICES EFFECTIFS OUVANT DROIT À PENSION - ABSENCE - PRISE EN COMPTE POUR LA LIQUIDATION DU MONTANT DE LA PENSION - EXISTENCE.
</SCT>
<ANA ID="9A"> 48-02-01-04-02 Si des bonifications, prévues notamment par l'article L. 12 du code des pensions civiles et miliaires de retraite, peuvent venir s'ajouter aux services effectifs accomplis par l'agent pour le calcul du montant de sa pension au moment de sa liquidation, seuls les services effectifs mentionnés aux articles L. 4 et L. 5 du code des pensions civiles et militaires sont pris en compte pour la constitution du droit à pension lui-même. En l'espèce, les durées calculées au titre de la bonification de dépaysement pour les services civils rendus hors d'Europe, prévue au a) de l'article L. 12, ne peuvent s'ajouter à la durée de services effectifs pour déterminer si un droit à pension est ouvert.</ANA>
<ANA ID="9B"> 48-02-01-04-03 Si des bonifications, prévues notamment par l'article L. 12 du code des pensions civiles et miliaires de retraite, peuvent venir s'ajouter aux services effectifs accomplis par l'agent pour le calcul du montant de sa pension au moment de sa liquidation, seuls les services effectifs mentionnés aux articles L. 4 et L. 5 du code des pensions civiles et militaires sont pris en compte pour la constitution du droit à pension lui-même. En l'espèce, les durées calculées au titre de la bonification de dépaysement pour les services civils rendus hors d'Europe, prévue au a) de l'article L. 12, ne peuvent s'ajouter à la durée de services effectifs pour déterminer si un droit à pension est ouvert.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
