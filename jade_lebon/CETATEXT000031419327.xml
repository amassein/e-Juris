<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031419327</ID>
<ANCIEN_ID>JG_L_2015_11_000000373896</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/41/93/CETATEXT000031419327.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère / 6ème SSR, 02/11/2015, 373896</TITRE>
<DATE_DEC>2015-11-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>373896</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère / 6ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ORTSCHEIDT ; SCP DIDIER, PINET</AVOCATS>
<RAPPORTEUR>M. Frédéric Puigserver</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Jean Lessi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:373896.20151102</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
Procédure contentieuse antérieure<br/>
<br/>
              Mme A...B...a demandé au tribunal administratif de Nancy d'annuler la décision implicite par laquelle le maire de la commune de Neuves-Maisons a refusé de prendre les mesures permettant la conservation et l'entretien de la parcelle cadastrée AB 874. Par un jugement n° 1100434 du 10 juillet 2012, le tribunal administratif de Nancy a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 12NC01558 du 10 octobre 2013, la cour administrative d'appel de Nancy, saisie par MmeB..., a annulé ce jugement ainsi que la décision implicite du maire de la commune de Neuves-Maisons, et a enjoint à la commune de prendre, dans un délai de trois mois suivant la notification de l'arrêt, les mesures de nature à aménager la parcelle en cause afin de la rendre conforme à son affectation à la circulation piétonne.<br/>
<br/>
Procédure devant le Conseil d'Etat<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 10 décembre 2013 et 8 janvier 2014 au secrétariat du contentieux du Conseil d'Etat, la commune de Neuves-Maisons demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt de la cour administrative d'appel de Nancy du 10 octobre 2013 ;<br/>
<br/>
              2°) de mettre à la charge de Mme B...la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des collectivités territoriales ; <br/>
              - le code général de la propriété des personnes publiques ;<br/>
              - le code de la voirie routière ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Puigserver, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Jean Lessi, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Didier, Pinet, avocat de la commune de Neuves-Maisons, et à la SCP Ortscheidt, avocat de Mme B... ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond qu'à la suite du partage, intervenu en 2007, d'une parcelle auparavant détenue par une indivision, la commune de Neuves-Maisons est devenue propriétaire d'un immeuble situé à l'un des angles du carrefour constitué par les deux rues principales de la commune ; que la commune ayant ensuite procédé, en 2009, à la démolition du bâtiment, le terrain, délimité sur deux côtés par d'autres bâtiments, et sur les deux autres côtés, sans obstacle organisé au franchissement, par les trottoirs qui bordent la voie publique, est resté vide ; que le maire de la commune a laissé sans réponse la demande, faite en 2010, de MmeB..., propriétaire de l'un des bâtiments bordant le terrain communal et y ayant un accès, tendant à ce que soient entrepris des travaux de conservation et d'entretien de ce terrain ; <br/>
<br/>
              2. Considérant qu'en vertu de l'article L. 2111-14 du code général de la propriété des personnes publiques, le domaine public routier communal comprend l'ensemble des biens appartenant à la commune et affectés aux besoins de la circulation terrestre, à l'exception des voies ferrées ; que, selon l'article L. 2111-2 du même code, font également partie du domaine public communal les biens de la commune qui, concourant à l'utilisation d'un bien appartenant au domaine public, en constituent un accessoire indissociable ;<br/>
<br/>
              3. Considérant qu'il ressort des énonciations de l'arrêt attaqué que, pour qualifier la parcelle litigieuse de dépendance du domaine public communal, la cour, d'une part, après avoir relevé que cette parcelle, propriété de la commune, était située à l'intersection de deux voies communales, dans le prolongement des trottoirs bordant ces voies, sans obstacle majeur à la circulation des piétons, en a déduit que cette parcelle était affectée aux besoins de la circulation terrestre ; que, s'il lui appartenait de se prononcer sur l'existence, l'étendue et les limites du domaine public routier communal, la cour, en statuant ainsi, sans rechercher si la commune avait affecté la parcelle en cause aux besoins de la circulation terrestre, a commis une erreur de droit ; que la cour a, d'autre part, jugé que la parcelle litigieuse constituait l'accessoire d'une dépendance du domaine public routier ; que, toutefois, en ne recherchant pas si cette parcelle était indissociable du bien relevant du domaine public dont elle était supposée être l'accessoire, la cour a méconnu les dispositions de l'article L. 2111-2 du code général de la propriété des personnes publiques ; que, par suite et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, son arrêt doit être annulé ;<br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ; <br/>
<br/>
              5. Considérant qu'aux termes de l'article L. 2111-1 du code général de la propriété des personnes publiques : " Sous réserve de dispositions législatives spéciales, le domaine public d'une personne publique (...) est constitué des biens lui appartenant qui sont soit affectés à l'usage direct du public, soit affectés à un service public pourvu qu'en ce cas ils fassent l'objet d'un aménagement indispensable à l'exécution des missions de ce service public " ;<br/>
<br/>
              6. Considérant qu'il ressort des pièces du dossier que si la parcelle litigieuse était accessible au public, elle ne pouvait être regardée comme affectée par la commune aux besoins de la circulation terrestre ; qu'ainsi, elle ne relevait pas, comme telle, en application de l'article L. 2111-14 du code général de la propriété des personnes publiques, du domaine public routier communal ; qu'en outre, il ne ressort pas des pièces du dossier, en dépit de la circonstance que des piétons aient pu de manière occasionnelle la traverser pour accéder aux bâtiments mitoyens, que la commune ait affecté cette parcelle à l'usage direct du public ; qu'elle n'a pas davantage été affectée à un service public ni fait l'objet d'un quelconque aménagement à cette fin ; qu'elle n'entrait pas, dès lors, dans les prévisions de l'article L. 2111-1 du même code ; que, de même, il ne ressort pas de ces pièces, notamment en raison de la configuration des lieux, qu'elle constituait un accessoire indissociable d'un bien appartenant au domaine public de la commune, au sens des dispositions de l'article L. 2111-2 du code ; qu'il suit de là que la parcelle litigieuse ne constituait pas une dépendance du domaine public de la commune mais une dépendance de son domaine privé ; que la contestation du refus du maire de prendre, à la demande d'un propriétaire riverain, des mesures permettant la conservation et l'entretien de cette parcelle, qui n'affecte ni le périmètre, ni la consistance du domaine privé communal, ne met en cause que des rapports de droit privé et relève donc de la compétence des juridictions de l'ordre judiciaire ; que, par suite, il y a lieu d'annuler le jugement du tribunal administratif de Nancy et de rejeter la demande de Mme B...comme portée devant un ordre de juridiction incompétent pour en connaître ;<br/>
<br/>
              7. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de Mme B...la somme de 3 000 euros à verser à la commune de Neuves-Maisons ; que les dispositions des articles L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la commune de Neuves-Maisons qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nancy du 10 octobre 2013 et le jugement du tribunal administratif de Nancy du 10 juillet 2012 sont annulés. <br/>
Article 2 : Les conclusions de la demande de Mme B...devant le tribunal administratif de Nancy tendant à l'annulation de la décision implicite par laquelle le maire de la commune de Neuves-Maisons a refusé de prendre les mesures permettant la conservation et l'entretien de la parcelle cadastrée AB 874 sont rejetées comme portées devant un ordre de juridiction incompétent pour en connaître.<br/>
Article 3 : Mme B...versera à la commune de Neuves-Maisons une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions de Mme B...présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à la commune de Neuves-Maisons et à Mme A...B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">24-01-01-01-01-01 DOMAINE. DOMAINE PUBLIC. CONSISTANCE ET DÉLIMITATION. DOMAINE PUBLIC ARTIFICIEL. BIENS FAISANT PARTIE DU DOMAINE PUBLIC ARTIFICIEL. AMÉNAGEMENT SPÉCIAL ET AFFECTATION AU SERVICE PUBLIC OU À L'USAGE DU PUBLIC. - AFFECTATION À L'USAGE DIRECT DU PUBLIC - CONDITION - INTENTION D'AFFECTER - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">24-01-01-01-01-02 DOMAINE. DOMAINE PUBLIC. CONSISTANCE ET DÉLIMITATION. DOMAINE PUBLIC ARTIFICIEL. BIENS FAISANT PARTIE DU DOMAINE PUBLIC ARTIFICIEL. VOIES PUBLIQUES ET LEURS DÉPENDANCES. - DOMAINE PUBLIC ROUTIER COMMUNAL (ART. L. 2111-14 DU CG3P) - CONDITION - AFFECTATION AUX BESOINS DE LA CIRCULATION TERRESTRE - EXISTENCE.
</SCT>
<ANA ID="9A"> 24-01-01-01-01-01 Une parcelle communale ne peut être regardée comme affectée à l'usage direct du public en l'absence d'intention de la commune de l'y affecter. Ainsi, une parcelle communale située à l'intersection de deux voies communales, dans le prolongement des trottoirs bordant ces voies, sans obstacle majeur à la circulation des piétons, et que des piétons ont pu, de manière occasionnelle, traverser pour accéder aux bâtiments mitoyens, n'est pas affectée à l'usage direct du public s'il ne ressort pas des pièces du dossier que la commune a procédé à une telle affectation.</ANA>
<ANA ID="9B"> 24-01-01-01-01-02 L'appartenance d'une parcelle au domaine public routier communal implique une affectation aux besoins de la circulation terrestre.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
