<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042545455</ID>
<ANCIEN_ID>JG_L_2020_11_000000428156</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/54/54/CETATEXT000042545455.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 20/11/2020, 428156</TITRE>
<DATE_DEC>2020-11-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>428156</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>CARBONNIER ; SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>M. Thomas Pez-Lavergne</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Mireille Le Corre</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:428156.20201120</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              L'association Trans'Cub, M. D... B..., M. F... C... et M. A... E... ont demandé au tribunal administratif de Bordeaux, d'une part, d'annuler les délibérations du conseil de la communauté urbaine de Bordeaux du 8 juillet 2011 et du 21 décembre 2012 ainsi que la décision du président de la communauté urbaine de Bordeaux du 18 avril 2013 refusant de retirer ces délibérations et, d'autre part, de constater l'illégalité des clauses tarifaires résultant des délibérations de la communauté urbaine de Bordeaux du 22 décembre 200 6 et du 10 juillet 2009. Par un jugement n° 1302295 du 9 mai 2016, le tribunal administratif de Bordeaux a rejeté leur demande.<br/>
<br/>
              Par un arrêt n° 16BX02303 du 18 décembre 2018, la cour administrative d'appel de Bordeaux a rejeté l'appel qu'ils ont formé contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 18 février et 20 mai 2019 et 10 juillet 2020 au secrétariat du contentieux du Conseil d'Etat, l'association Trans'Cub, M. B..., M. C... et M. E... demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel ;<br/>
<br/>
              3°) de mettre à la charge de Bordeaux Métropole la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des collectivités territoriales ;<br/>
              - la loi n° 93-122 du 29 janvier 1993 ;<br/>
              - la loi n° 95-101 du 2 février 1995 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Thomas Pez-Lavergne, maître des requêtes en service extraordinaire, <br/>
<br/>
              - les conclusions de Mme Mireille Le Corre, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Carbonnier, avocat de l'association Trans'cub, de M. B..., de M. C... et de M. E... et à la SCP Foussard, Froger, avocat de la Bordeaux Métropole ;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des énonciations de l'arrêt attaqué que, par une délibération du 20 décembre 1991, la communauté urbaine de Bordeaux, devenue Bordeaux Métropole, a concédé le service public de l'eau potable et de l'assainissement à la société Lyonnaise des Eaux pour une durée de trente ans à compter du 1er janvier 1992. Le 22 décembre 2006, le conseil communautaire a approuvé l'avenant n° 7 à la convention de délégation de service public dont l'objet est notamment d'augmenter les investissements à la charge du délégataire de 146 à 302 millions d'euros sur la durée du contrat. Par une délibération du 10 juillet 2009, le conseil communautaire a approuvé un avenant n° 8 ayant notamment pour objet d'imposer au délégataire le remplacement sur le réseau de distribution des branchements en plomb pour un montant prévisionnel de 78 millions d'euros et de fixer une indemnité de retour tenant compte de l'absence d'amortissement total de ces travaux à l'échéance de la délégation. Le 8 juillet 2011, le conseil communautaire a adopté une nouvelle délibération qui a, notamment, approuvé des créations de postes en prévision de la reprise du service en régie à l'issue du contrat. Enfin, par une délibération du 21 décembre 2012, le conseil communautaire a approuvé un avenant n° 9 à la convention envisageant les modalités de transition vers un nouveau modèle d'exploitation du service sans modifier la date d'échéance de la délégation, telle que prévue initialement, fixée au 31 décembre 2021. L'association Trans'Cub, M. B..., M. C... et M. E... ont adressé au président de la communauté urbaine, le 21 février 2013, un recours gracieux dirigé contre les quatre délibérations mentionnées précédemment. Après le rejet de leur demande par une décision du 18 avril 2013, ils ont saisi le tribunal administratif de Bordeaux d'une demande tendant à l'annulation pour excès de pouvoir des délibérations du 8 juillet 2011 et du 21 décembre 2012 ainsi que de la décision refusant de retirer ces délibérations et celles des 22 décembre 2006 et 10 juillet 2009. Par un jugement du 9 mai 2016, le tribunal administratif a rejeté leur demande. Les requérants se pourvoient en cassation contre l'arrêt de la cour administrative d'appel de Bordeaux du 18 décembre 2018 qui a rejeté l'appel qu'ils avaient formé contre ce jugement.<br/>
<br/>
              2. En vertu de la décision n° 358994 du 4 avril 2014 du Conseil d'Etat, statuant au contentieux, la contestation de la validité des contrats administratifs par les tiers doit faire l'objet d'un recours de pleine juridiction dans les conditions définies par cette décision. Toutefois, cette décision a jugé que le recours ainsi défini ne trouve à s'appliquer qu'à l'encontre des contrats signés à compter du 4 avril 2014, date de sa lecture, la contestation des contrats signés antérieurement à cette date continuant d'être appréciée au regard des règles applicables avant cette décision. Dans le cas où est contestée la validité d'un avenant à un contrat, la détermination du régime de la contestation est fonction de la date de signature de l'avenant, un avenant signé après le 4 avril 2014 devant être contesté dans les conditions prévues par la décision n° 358994 quand bien même il modifie un contrat signé antérieurement à cette date.<br/>
<br/>
              3. En l'espèce, les délibérations du 22 décembre 2006, du 10 juillet 2009 et du 21 décembre 2012 sont relatives à des avenants au contrat de concession antérieurs au 4 avril 2014. Eu égard à la date de conclusion de ces avenants, elles constituent, avec la décision refusant de les retirer, des actes détachables du contrat de concession susceptibles de faire l'objet d'un recours pour excès de pouvoir.<br/>
<br/>
              Sur le pourvoi en tant qu'il concerne les délibérations des 22 décembre 2006 et 10 juillet 2009 :<br/>
<br/>
              4. D'une part, une délibération approuvant un contrat de concession et autorisant le maire à le signer est dépourvue de caractère réglementaire, sans qu'ait à cet égard d'incidence la circonstance que le contrat approuvé comporte des clauses revêtues d'un caractère réglementaire. D'autre part, une telle délibération crée des droits au profit du concessionnaire. <br/>
<br/>
              5. Dès lors, la cour administrative d'appel, qui ne s'est pas méprise sur la portée de ces délibérations, n'a pas commis d'erreur de droit en retenant que la délibération du conseil communautaire du 22 décembre 2006 approuvant l'avenant n° 7 au contrat de concession et celle du 10 juillet 2009 approuvant l'avenant n° 8 étaient dépourvues de caractère réglementaire et avaient créé des droits au profit de la société concessionnaire et en déduisant, d'une part, que l'autorité compétente de Bordeaux Métropole était tenue de rejeter la demande dont elle avait été saisie de retirer ces délibérations au-delà du délai de retrait de quatre mois et, d'autre part, que les moyens excipant de leur illégalité étaient inopérants à l'encontre de la décision du 18 avril 2013 du président de la communauté urbaine de Bordeaux refusant de procéder à leur retrait.<br/>
<br/>
              Sur le pourvoi en tant qu'il concerne la délibération du 21 décembre 2012 :<br/>
<br/>
              6. Aux termes de l'article 40 de la loi du 29 janvier 1993 relative à la prévention de la corruption et à la transparence de la vie économique et des procédures publiques, dans sa rédaction postérieure à la loi du 2 février 1995 relative au renforcement de la protection de l'environnement, codifié à l'article L. 1411-2 du code général des collectivités territoriales et dont la substance est désormais reprise aux articles L. 3114-7 et L. 3114-8 du code de la commande publique : " Les conventions de délégation de service public doivent être limitées dans leur durée. Celle-ci est déterminée par la collectivité en fonction des prestations demandées au délégataire. Lorsque les installations sont à la charge du délégataire, la convention de délégation tient compte, pour la détermination de sa durée, de la nature et du montant de l'investissement à réaliser et ne peut dans ce cas dépasser la durée normale d'amortissement des installations mises en oeuvre. Dans le domaine de l'eau potable, de l'assainissement, des ordures ménagères et autres déchets, les délégations de service public ne peuvent avoir une durée supérieure à vingt ans sauf examen préalable par le directeur départemental des finances publiques, à l'initiative de l'autorité délégante, des justificatifs de dépassement de cette durée. Les conclusions de cet examen sont communiquées aux membres de l'assemblée délibérante compétente avant toute délibération relative à la délégation ".<br/>
<br/>
              7. Ces dispositions répondent à un impératif d'ordre public qui est de garantir, par une remise en concurrence périodique, la liberté d'accès des opérateurs économiques aux contrats de délégation de service public et la transparence des procédures de passation. Un tel motif d'intérêt général ne saurait, pas plus que la nécessité d'assurer l'égalité de tous les opérateurs économiques délégataires de service public au regard des exigences de la loi, entraîner la nullité des contrats de délégation de service public conclus antérieurement à l'entrée en vigueur de la loi du 29 janvier 1993 pour des durées incompatibles avec les dispositions de son article 40, ni contraindre les parties à de tels contrats à modifier leur durée. Il implique en revanche, non seulement qu'aucune stipulation relative à la durée du contrat, convenue entre les parties après la date d'entrée en vigueur de la loi, ne peut méconnaître les exigences prévues par son article 40, mais en outre que les clauses d'une convention de délégation de service public qui auraient pour effet de permettre son exécution pour une durée restant à courir, à compter de la date d'entrée en vigueur de la loi, excédant la durée maximale autorisée par la loi, ne peuvent plus être régulièrement mises en oeuvre au-delà de la date à laquelle cette durée maximale est atteinte.<br/>
<br/>
              8. Il ressort des pièces du dossier soumis aux juges du fond que la communauté urbaine de Bordeaux a approuvé, le 21 décembre 2012, un avenant n° 9 au traité de concession ayant notamment pour objet de consacrer le principe d'une maîtrise d'ouvrage communautaire sur certains investissements structurants, d'encadrer et de planifier la transition vers un nouveau mode d'exploitation du service en précisant les conditions financières de sortie de la concession et de valider une nouvelle grille de tarification du service de l'eau applicable aux usagers.<br/>
<br/>
              9. Il ressort des énonciations de l'arrêt attaqué que, pour écarter comme inopérant le moyen, invoqué à l'appui du recours pour excès de pouvoir formé contre la délibération du conseil communautaire du 21 décembre 2012, tiré de ce que l'avenant approuvé par cette délibération méconnaissait la portée des dispositions de l'article 40 de la loi du 29 janvier 1993, la cour a retenu que cette délibération ne pouvait être regardée comme ayant eu pour objet de prolonger l'exécution du contrat de concession au-delà du délai de vingt ans prévu par la loi. En statuant ainsi, alors que cette délibération, qui a d'ailleurs été adoptée après consultation, à l'initiative de la collectivité, du directeur départemental des finances publiques, telle qu'elle est requise par les dispositions législatives citées au point 6, impliquait, par les modifications qu'elle approuvait, la poursuite de l'exécution du contrat au-delà du mois de février 2015, c'est-à-dire au-delà de la date à laquelle la délégation de service public aurait dû en principe prendre fin en application de la loi du 29 janvier 1993 modifiée par la loi du 2 février 1995, la cour administrative d'appel de Bordeaux a méconnu la portée de cette délibération. <br/>
<br/>
              10. Si Bordeaux Métropole, venue aux droits de la communauté urbaine de Bordeaux, demande que soit substitué au motif retenu par la cour un motif tiré de ce que des tiers à un contrat administratif ne peuvent former un recours pour excès de pouvoir contre une décision refusant de mettre fin à l'exécution du contrat, qui peut faire l'objet uniquement d'un recours de plein contentieux devant le juge du contrat, il ressort des pièces du dossier soumis aux juges du fond que la requête présentée devant eux tendait, en tout état de cause, non à ce qu'il soit mis fin au contrat liant Bordeaux Métropole et la société Lyonnaise des eaux, mais à l'annulation de la délibération décidant de la poursuite de l'exécution du contrat au-delà de la durée maximale de vingt ans à compter de l'entrée en vigueur de la loi du 2 février 1995. Par suite, ce motif ne peut être substitué aux motifs retenus par l'arrêt attaqué.<br/>
<br/>
              11. Il résulte de tout ce qui précède, sans qu'il soit besoin de se prononcer sur l'autre moyen du pourvoi dirigé contre cette partie de l'arrêt attaqué, que cet arrêt doit être annulé en tant qu'il statue sur les conclusions dirigées contre la délibération du 21 décembre 2012.<br/>
<br/>
              12. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de Bordeaux Métropole la somme globale de 3 000 euros à verser aux requérants au titre des dispositions de l'article L. 761-1 du code de justice administrative. Ces mêmes dispositions font en revanche obstacle à ce que soit mise à la charge de ces derniers, qui ne sont pas, dans la présente instance, la partie perdante, la somme que demande au même titre Bordeaux Métropole.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 18 décembre 2018 de la cour administrative d'appel de Bordeaux est annulé en tant qu'il statue sur les conclusions dirigées contre la délibération du 21 décembre 2012 de la communauté urbaine de Bordeaux.<br/>
Article 2 : L'affaire est renvoyée, dans la mesure de la cassation ainsi prononcée, à la cour administrative d'appel de Bordeaux.<br/>
Article 3 : Bordeaux Métropole versera la somme globale de 3 000 euros aux quatre requérants au titre de l'article L. 761-1 du code de justice administrative. Ses conclusions présentées au titre des mêmes dispositions sont rejetées.<br/>
Article 4 : Le surplus des conclusions du pourvoi de l'association Trans'Cub et autres est rejeté.<br/>
Article 5 : La présente décision sera notifiée à l'association Trans'Cub, représentant unique pour l'ensemble des requérants, et à Bordeaux Métropole.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-08-01-03 MARCHÉS ET CONTRATS ADMINISTRATIFS. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. RECEVABILITÉ. - RECOURS TARN-ET-GARONNE [RJ1] - APPLICATION AU RECOURS CONTRE UN AVENANT À UN CONTRAT SIGNÉ ANTÉRIEUREMENT À LA DATE DE LECTURE DE CETTE DÉCISION - EXISTENCE, LORSQUE LA SIGNATURE DE L'AVENANT EST POSTÉRIEURE À CETTE DATE.
</SCT>
<ANA ID="9A"> 39-08-01-03 En vertu de la décision n° 358994 du 4 avril 2014 du Conseil d'Etat, statuant au contentieux, la contestation de la validité des contrats administratifs par les tiers doit faire l'objet d'un recours de pleine juridiction dans les conditions définies par cette décision.... ,,Toutefois, cette décision a jugé que le recours ainsi défini ne trouve à s'appliquer qu'à l'encontre des contrats signés à compter du 4 avril 2014, date de sa lecture, la contestation des contrats signés antérieurement à cette date continuant d'être appréciée au regard des règles applicables avant cette décision.... ,,Dans le cas où est contestée la validité d'un avenant à un contrat, la détermination du régime de la contestation est fonction de la date de signature de l'avenant, un avenant signé après le 4 avril 2014 devant être contesté dans les conditions prévues par la décision n° 358994 quand bien même il modifie un contrat signé antérieurement à cette date.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Assemblée, 4 avril 2014, Département de Tarn-et-Garonne, n° 358994, p. 70.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
