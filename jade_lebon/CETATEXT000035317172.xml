<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035317172</ID>
<ANCIEN_ID>JG_L_2017_05_000000400678</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/31/71/CETATEXT000035317172.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 1ère chambres réunies, 19/05/2017, 400678</TITRE>
<DATE_DEC>2017-05-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>400678</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP POTIER DE LA VARDE, BUK LAMENT, ROBILLOT ; SCP MEIER-BOURDEAU, LECUYER</AVOCATS>
<RAPPORTEUR>M. Cyrille Beaufils</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Louis Dutheillet de Lamothe</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:400678.20170519</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Collet a demandé au tribunal administratif de Rouen d'annuler pour excès de pouvoir l'arrêté du 4 juillet 2011 par lequel le préfet de la Seine-Maritime a approuvé le plan de prévention des risques technologiques autour de l'établissement Revima à Caudebec-en-Caux et Saint-Wandrille-Rançon. Par un jugement n° 1102497 du 26 juin 2014, le tribunal administratif de Rouen a fait droit à sa demande.<br/>
<br/>
              Par un arrêt n° 14DA01464 du 7 avril 2016, la cour administrative d'appel de Douai a, sur appel du ministre de l'écologie, du développement durable et de l'énergie, d'une part, annulé ce jugement et, d'autre part, annulé l'arrêté du 4 juillet 2011. <br/>
<br/>
              Par un pourvoi et un mémoire en réplique, enregistrés les 14 juin 2016 et 16 mars 2017 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'environnement, de l'énergie et de la mer demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de première instance de la société Collet.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'environnement ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Cyrille Beaufils, auditeur,  <br/>
<br/>
              - les conclusions de M. Louis Dutheillet de Lamothe, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Potier de la Varde, Buk Lament, Robillot, avocat de la société Collet, et à la SCP Meier-Bourdeau, Lecuyer, avocat de la société Revima ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 4 juillet 2011, le préfet de la Seine-Maritime a établi le plan de prévention des risques technologiques autour de l'établissement de la société Revima implanté sur le territoire des communes de Caudebec-en-Caux et Saint-Wandrille-Rançon où elle exerce une activité spécialisée dans la maintenance de matériels aéronautiques et la chaudronnerie ; que si les risques identifiés par ce document au sein de l'établissement résultent principalement d'une installation stockant et employant des substances très toxiques classées sous la rubrique 1111.1c de la nomenclature des installations classées pour la protection de l'environnement, alors applicable, soumise au régime des autorisations susceptibles de donner lieu à servitudes d'utilité publique dit " Seveso seuil haut " et, accessoirement, d'une installation consistant en une cuve de stockage de kérosène d'une capacité de 50 mètres cubes, relevant du régime de la déclaration au titre de la rubrique 1432 de la nomenclature alors applicable, le plan prescrit des mesures de protection, consistant en des servitudes liées à l'usage du sol ou au renforcement du bâti, uniquement contre les risques résultant de cette dernière installation ; que la société Collet, riveraine de la partie du site où se trouve cette installation de stockage de kérosène, a demandé au tribunal administratif de Rouen d'annuler l'arrêté du 4 juillet 2011 ; que, par un arrêt du 7 avril 2016 contre lequel le ministre chargé de l'environnement se pourvoit en cassation, la cour administrative d'appel de Douai a annulé le jugement du tribunal administratif de Rouen du 26 juin 2014 ainsi que cet arrêté, au motif que l'installation justifiant les mesures litigieuses ne remplissait pas à elle seule ou par addition avec d'autres installations du même site la condition posée par le code de l'environnement pour l'édiction d'un plan de prévention des risques technologiques ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 515-15 du code de l'environnement, dans sa rédaction applicable à l'arrêté en litige : " L'Etat élabore et met en oeuvre des plans de prévention des risques technologiques qui ont pour objet de délimiter les effets d'accidents susceptibles de survenir dans les installations figurant sur la liste prévue au IV de l'article L. 515-8 et qui y figuraient au 31 juillet 2003, et pouvant entraîner des effets sur la salubrité, la santé et la sécurité publiques directement ou par pollution du milieu. / (...) / Ces plans délimitent un périmètre d'exposition aux risques en tenant compte de la nature et de l'intensité des risques technologiques décrits dans les études de dangers et des mesures de prévention mises en oeuvre " ; que l'article L. 515-16 du même code, dans sa rédaction applicable, énumère les mesures que les plans de prévention des risques technologiques peuvent définir à l'intérieur du périmètre d'exposition aux risques, en fonction du type de risques, de leur gravité, de leur probabilité et de leur cinétique ; qu'au nombre de celles-ci figure notamment, au I de cet article, la délimitation des zones dans lesquelles la réalisation d'aménagements ou d'ouvrages ainsi que les constructions nouvelles et l'extension des constructions existantes sont interdites ou subordonnées au respect de prescriptions relatives à la construction, à l'utilisation ou à l'exploitation ; qu'en vertu de l'article L. 515-23 du même code : " Le plan de prévention des risques technologiques approuvé vaut servitude d'utilité publique. Il est porté à la connaissance des maires des communes situées dans le périmètre du plan en application de l'article L. 121-2 du code de l'urbanisme. Il est annexé aux plans locaux d'urbanisme, conformément à l'article L. 126-1 du même code " ; <br/>
<br/>
              3 Considérant qu'aux termes du IV de l'article L. 515-8 du code de l'environnement dans sa rédaction alors en vigueur, relatif aux installations susceptibles de donner lieu à des servitudes d'utilité publique, auquel renvoient les dispositions citées ci-dessus de l'article L. 515-15 du même code : " Un décret en Conseil d'Etat, pris après avis du Conseil supérieur de la prévention des risques technologiques, fixe la liste des catégories, et éventuellement les seuils de capacité, des installations dans le voisinage desquelles ces servitudes peuvent être instituées ", ces installations classées étant définies au I du même article comme celles susceptibles de créer, par danger d'explosion ou d'émanation de produits nocifs, des risques très importants pour la santé ou la sécurité des populations voisines et pour l'environnement ; que la liste ainsi prévue a été incorporée au tableau constituant l'annexe I de la nomenclature des installations classées pour la protection de l'environnement par le décret du 10 août 2005 modifiant cette nomenclature, désormais incorporée dans la colonne " A " de l'annexe à l'article R. 511-9 du code de l'environnement, les substances ou préparations concernées étant repérées par les lettres " AS " ; que l'article R. 511-10 du même code, dans sa rédaction en vigueur à la date de l'arrêté litigieux, dispose enfin que : " I.- La liste prévue au IV de l'article L. 515-8, incorporée à l'annexe de l'article R. 511-9, comporte également l'ensemble des installations d'un même établissement relevant d'un même exploitant sur un même site au sens de l'article R. 512-13, dès lors que l'addition des substances ou préparations susceptibles d'être présentes dans cet établissement satisfait la condition énoncée ci-après : qx/Qx &gt; 1 / 1° Pour les substances ou préparations visées par les rubriques 11.. comportant un seuil AS de la nomenclature annexée à l'article R. 511-9 à l'exclusion des rubriques 1171, 1172 et 1173 ; / 2° Pour les substances ou préparations visées par les rubriques 1171, 1172 et 1173 ; / 3° Pour les substances ou préparations visées par les rubriques 12.., 13.. et 14.. comportant un seuil AS et 2255. / II.- Dans la formule mentionnée au I : / "qx" désigne la quantité de la substance ou de la préparation x susceptible d'être présente dans l'établissement ; / "Qx" désigne la quantité seuil AS dans la rubrique visant le stockage de la substance ou de la préparation x " ; <br/>
<br/>
              4. Considérant qu'il résulte des dispositions combinées des articles L. 515-15 et L. 515-8 du code de l'environnement citées aux points 2 et 3 que les plans de prévention des risques technologiques ont pour objet de délimiter les effets d'accidents susceptibles de survenir dans les installations classées pour la protection de l'environnement présentant des dangers particulièrement importants pour la sécurité et la santé des populations voisines et pour l'environnement qui figurent dans la liste prévue au IV de l'article L. 515-8 ; que l'article R. 511-10, cité au point 3, conduit à inclure dans cette liste l'ensemble des installations utilisant des substances ou préparations relevant d'une rubrique comportant un seuil " AS " mentionnée à cet article susceptibles d'être présentes dans un même établissement relevant d'un même exploitant sur un même site, dès lors que la condition qu'il précise portant sur la somme pondérée des quantités des divers types de substances ou préparations rapportées à leur seuil " AS " est satisfaite ; que, pour établir le plan de prévention des risques technologiques, le représentant de l'Etat doit alors prendre en compte les effets d'accidents susceptibles de survenir dans les installations figurant sur cette liste, en relation avec tout autre facteur de nature à interagir avec elles et en particulier les effets dit " dominos " dus aux interactions entre les différentes installations de l'établissement ; <br/>
<br/>
              5. Considérant que la cour administrative d'appel de Douai a relevé, par des constatations non arguées de dénaturation, que l'établissement de la société Revima exploitait sur un même site une installation stockant et employant des substances très toxiques classées sous la rubrique 1111.1c de la nomenclature des installations classées pour la protection de l'environnement et une installation consistant en une cuve de stockage de kérosène d'une capacité de 50 mètres cubes relevant du régime de la déclaration au titre de la rubrique 1432 de la nomenclature ; que ces deux rubriques comportent un seuil " AS " ; qu'il résulte dès lors de ce qui a été dit au point 4 que ces installations doivent toutes deux être prises en compte pour l'application de la formule qui figure à l'article R. 511-10 du code de l'environnement ; que satisfaisant à la condition fixée par cet article, elles figurent toutes deux sur la liste des installations prévues au IV de l'article L. 515-8 du code de l'environnement ; qu'ainsi, en jugeant que la seconde de ces installations devait être prise en considération isolément, dès lors que le plan de prévention des risques technologiques attaqué prévoyait des servitudes destinées à protéger des risques résultant de sa seule exploitation, ce dont elle a déduit qu'elle ne figurait pas sur la liste du IV de l'article L. 515-8 du code de l'environnement, la cour administrative d'appel a commis une erreur de droit ; <br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que le ministre chargé de l'environnement est fondé à demander l'annulation de l'arrêt qu'il attaque ; <br/>
<br/>
              7. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 7 avril 2016 de la cour administrative d'appel de Douai est annulé.<br/>
<br/>
      Article 2 : L'affaire est renvoyée à la cour administrative de Douai.<br/>
<br/>
Article 3 : Les conclusions présentées par la société Collet au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
<br/>
Article 4 : La présente décision sera notifiée au ministre d'Etat, ministre de la transition écologique et solidaire et à la société Collet.<br/>
      Copie en sera adressée à la société Revima.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">44-02-02-005-01-05 NATURE ET ENVIRONNEMENT. INSTALLATIONS CLASSÉES POUR LA PROTECTION DE L'ENVIRONNEMENT. RÉGIME JURIDIQUE. ACTES AFFECTANT LE RÉGIME JURIDIQUE DES INSTALLATIONS. CLASSEMENT. - 1) APPRÉCIATION DU DÉPASSEMENT DU SEUIL DE SUBSTANCES OU PRÉPARATIONS DANGEREUSES - PRISE EN COMPTE DE L'ENSEMBLE DES INSTALLATIONS UTILISANT CES SUBSTANCES OU PRÉPARATIONS SUSCEPTIBLES D'ÊTRE PRÉSENTES DANS UN MÊME ÉTABLISSEMENT RELEVANT D'UN MÊME EXPLOITANT SUR UN MÊME SITE - 2) ETABLISSEMENT DU PPRT VALANT SERVITUDE D'UTILITÉ PUBLIQUE - PRISE EN COMPTE DE L'ENSEMBLE DES INSTALLATIONS.
</SCT>
<ANA ID="9A"> 44-02-02-005-01-05 1) Il résulte des dispositions combinées des articles L. 515-15 et L. 515-8 du code de l'environnement que les plans de prévention des risques technologiques (PPRT) ont pour objet de délimiter les effets d'accidents susceptibles de survenir dans les installations classées pour la protection de l'environnement présentant des dangers particulièrement importants pour la sécurité et la santé des populations voisines et pour l'environnement qui figurent dans la liste prévue au IV de l'article L. 515-8. L'article R. 511-10 du même code conduit à inclure dans cette liste l'ensemble des installations utilisant des substances ou préparations relevant d'une rubrique comportant un seuil AS mentionnée à cet article susceptibles d'être présentes dans un même établissement relevant d'un même exploitant sur un même site, dès lors que la condition qu'il précise portant sur la somme pondérée des quantités des divers types de substances ou préparations rapportées à leur seuil AS est satisfaite.,,,2) Pour établir le PPRT, le représentant de l'Etat doit alors prendre en compte les effets d'accidents susceptibles de survenir dans les installations figurant sur cette liste, en relation avec tout autre facteur de nature à interagir avec elles et en particulier les effets dit dominos dus aux interactions entre les différentes installations de l'établissement.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
