<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034013214</ID>
<ANCIEN_ID>JG_L_2017_02_000000394801</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/01/32/CETATEXT000034013214.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 4ème chambres réunies, 07/02/2017, 394801</TITRE>
<DATE_DEC>2017-02-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>394801</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 4ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ODENT, POULET ; SCP SEVAUX, MATHONNET</AVOCATS>
<RAPPORTEUR>M. Guillaume Leforestier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Laurence Marion</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:394801.20170207</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Châlons-en-Champagne de condamner le centre hospitalier universitaire de Reims ou l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales (ONIAM) à réparer les conséquences dommageables de sa contamination par le virus de l'hépatite C lors de la transplantation rénale réalisée au CHU de Reims. Par un jugement n° 0801674 du 10 janvier 2013, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 13NC00344 du 26 février 2015, la cour administrative d'appel de Nancy a annulé ce jugement en tant qu'il rejetait les conclusions indemnitaires de M. B... dirigées contre l'ONIAM et a invité M. B... à chiffrer ses prétentions. Par un second arrêt du 24 septembre 2015, la cour administrative d'appel de Nancy a mis à la charge de l'ONIAM le versement à M. B...de la somme de 353 028 euros au titre des préjudices subis.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 24 novembre 2015 et 24 février 2016 au secrétariat du contentieux du Conseil d'Etat, l'ONIAM demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de M. B...la somme de 2 800 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - le code de la santé publique ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Guillaume Leforestier, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Laurence Marion, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Sevaux, Mathonnet, avocat de l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales et à la SCP Odent, Poulet, avocat de M. B....<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que l'arrêt du 24 septembre 2015 de la cour administrative d'appel de Nancy contre lequel l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales (ONIAM) se pourvoit en cassation met à la charge de l'office, au titre des dispositions du II de l'article L. 1142-1 du code de la santé publique, le versement d'indemnités d'un montant total de 353 028 euros en réparation des conséquences de la contamination de M. B...par le virus de l'hépatite C à l'occasion d'une greffe de rein ; que le pourvoi critique l'arrêt en tant qu'il alloue à l'intéressé une somme de 202 610 euros au titre des pertes de revenus qu'il a subies à compter du 25 septembre 2015 ; que, pour justifier ce montant, la cour a énoncé que la " perte de revenus annuelle évaluée à 8 808 euros sera, compte tenu de l'âge du requérant à la date du présent arrêt, convertie en un capital de 202 610 euros, calculé sur la base du barème de capitalisation de 2013 reposant sur la table de mortalité de 2008 pour les hommes, publiée par l'Institut national de la statistique et des études économiques et d'un taux d'intérêt de 1,2 % " ; qu'il résulte nécessairement de ces motifs que l'indemnité a été calculée sur la base d'une perte de revenus annuelle de 8 808 euros pendant toute la période correspondant à l'espérance de vie de l'intéressé ;<br/>
<br/>
              2. Considérant que, compte tenu de la proximité relative du départ à la retraite de M.B..., qui était âgé de 52 ans à la date de l'arrêt attaqué, il appartenait à la cour d'indemniser de manière distincte la perte de revenus qu'il subirait jusqu'à l'âge auquel, en l'absence de contamination, il aurait pris sa retraite ainsi que le préjudice patrimonial qu'il subirait, le cas échéant, au cours de la période ultérieure, en raison notamment d'une perte éventuelle de droits à pension ; que l'âge auquel l'intéressé aurait pris sa retraite est, en principe, celui auquel il aurait pu prétendre à une pension à taux plein, à moins que l'instruction ne fasse ressortir qu'il l'aurait prise à un âge différent ; qu'il suit de là qu'en mettant à la charge de l'ONIAM le versement d'une indemnité calculée, sur l'ensemble de la période correspondant à l'espérance de vie de l'intéressé, à partir de la perte de revenus professionnels de 8 808 euros par an constatée à la date de son arrêt, la cour a méconnu le principe de réparation intégrale du préjudice, sans perte ni profit pour la victime, et entaché son arrêt d'erreur de droit ; que, par suite, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, l'ONIAM est fondé à demander l'annulation de cet arrêt en tant qu'il statue sur les conclusions de M. B...tendant à l'indemnisation de ses pertes de revenus à compter du 25 septembre 2015 ; <br/>
<br/>
              3. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par l'ONIAM au titre de l'article L. 761-1 du code de justice administrative ; que ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'ONIAM, qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nancy du 24 septembre 2015 est annulé en tant qu'il statue sur les conclusions de M. B...tendant à l'indemnisation de ses pertes de revenus à compter du 25 septembre 2015.<br/>
<br/>
Article 2 : L'affaire est renvoyée, dans la limite de la cassation prononcée, à la cour administrative d'appel de Nancy.<br/>
<br/>
Article 3 : Le surplus des conclusions du pourvoi de l'ONIAM et les conclusions de M. B... au titre de l'article L. 761-1 du code de justice administrative sont rejetés. <br/>
<br/>
Article 4 : La présente décision sera notifiée à l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales et à M. A...B....<br/>
Copie en sera adressée au centre hospitalier universitaire de Reims, à la caisse primaire d'assurance maladie de Seine-et-Marne et à l'Agence de la biomédecine.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">60-04-03-02-01-01 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RÉPARATION. ÉVALUATION DU PRÉJUDICE. PRÉJUDICE MATÉRIEL. PERTE DE REVENUS. PERTE DE REVENUS SUBIE PAR LA VICTIME D'UN ACCIDENT. - INDEMNITÉ VERSÉE SOUS LA FORME D'UN CAPITAL - CAS D'UNE PERSONNE PROCHE DE L'ÂGE DE LA RETRAITE - CALCUL D'UN CAPITAL POUR LA PERTE DE REVENU JUSQU'À L'ÂGE DE LA RETRAITE PRÉVISIBLE ET CALCUL D'UN CAPITAL CORRESPONDANT À D'ÉVENTUELLES PERTES DE DROITS À PENSION.
</SCT>
<ANA ID="9A"> 60-04-03-02-01-01 Indemnisation sous la forme d'un capital d'une perte de revenus subie par une personne contrainte de diminuer son activité professionnelle à la suite d'une contamination par le virus de l'hépatite C à l'hôpital.... ,,Compte tenu de la proximité relative du départ à la retraite de la victime, qui était âgée de 52 ans à la date de l'arrêt attaqué, il appartenait à la cour d'indemniser de manière distincte la perte de revenus qu'elle subirait jusqu'à l'âge auquel, en l'absence de la contamination, elle aurait pris sa retraite ainsi que le préjudice patrimonial qu'elle subirait, le cas échéant, au cours de la période ultérieure, en raison notamment d'une perte éventuelle de droits à pension. L'âge auquel l'intéressé aurait pris sa retraite est, en principe, celui auquel il aurait pu prétendre à une pension à taux plein, sauf si l'instruction fait ressortir qu'il l'aurait prise à un âge différent.... ,,Erreur de droit à avoir calculé le montant de l'indemnité à partir de la perte de revenus professionnels annuelle, constatée à la date de son arrêt, appliquée à l'ensemble de la période correspondant à l'espérance de vie de l'intéressé.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
