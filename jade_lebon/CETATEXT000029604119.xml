<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029604119</ID>
<ANCIEN_ID>JG_L_2014_10_000000362235</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/60/41/CETATEXT000029604119.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème et 5ème sous-sections réunies, 15/10/2014, 362235</TITRE>
<DATE_DEC>2014-10-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>362235</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème et 5ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GATINEAU, FATTACCINI ; SCP BORE, SALVE DE BRUNETON</AVOCATS>
<RAPPORTEUR>Mme Florence Chaltiel-Terral</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Keller</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:362235.20141015</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 28 août et 28 novembre 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour la société Gefco, dont le siège est 77-81 rue des Lilas d'Espagne, BP 313, à Courbevoie Cedex (92402) ; la société Gefco demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 11DA00087-11DA00100 du 21 juin 2012 de la cour administrative d'appel de Douai en tant qu'il a rejeté sa requête tendant, d'une part, à l'annulation du jugement n° 0800097-0801635 du 9 novembre 2010 par lequel le tribunal administratif de Lille a annulé la décision du 11 janvier 2008 du ministre de l'écologie, du développement et de l'aménagement durables, réformant la décision du 20 mai 2007 de l'inspecteur du travail autorisant le licenciement de M. A...B..., a accordé l'autorisation de licencier ce dernier et retiré sa décision implicite ayant rejeté son recours hiérarchique, et, d'autre part, au rejet de la demande de première instance de M. B...;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
              3°) de mettre à la charge de M. B...la somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code du travail ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Florence Chaltiel-Terral, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Rémi Keller, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gatineau, Fattaccini, avocat de la société Gefco et à la SCP Boré, Salve de Bruneton, avocat de M. B... ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par une décision du 11 janvier 2008, le ministre de l'écologie, du développement et de l'aménagement durables a substitué sa décision à celle de l'inspecteur du travail en date du 20 mai 2007 qui avait autorisé le licenciement de M.B..., et a accordé l'autorisation de licencier ce dernier ; que la société Gefco se pourvoit en cassation contre l'arrêt du 21 juin 2012 par lequel la cour administrative d'appel de Douai a rejeté son appel contre le jugement du tribunal administratif de Lille du 9 novembre 2010 annulant la décision du ministre à la demande de M.B... ; <br/>
<br/>
              2. Considérant qu'en vertu des dispositions du code du travail, le licenciement des salariés légalement investis de fonctions représentatives, qui bénéficient d'une protection exceptionnelle dans l'intérêt de l'ensemble des travailleurs qu'ils représentent, ne peut intervenir que sur autorisation de l'inspecteur du travail ; que, lorsque leur licenciement est envisagé, celui-ci ne doit pas être en rapport avec les fonctions représentatives normalement exercées ou avec leur appartenance syndicale ; que dans le cas où la demande de licenciement est motivée par un acte ou un comportement du salarié qui, ne méconnaissant pas les obligations découlant pour lui de son contrat de travail, ne constitue pas une faute, il appartient à l'inspecteur du travail, et le cas échéant au ministre, de rechercher, sous le contrôle du juge de l'excès de pouvoir, si les faits en cause sont établis et de nature, compte tenu de leur répercussion sur le fonctionnement de l'entreprise, à rendre impossible le maintien du salarié dans l'entreprise, eu égard à la nature de ses fonctions et à l'ensemble des règles applicables au contrat de travail de l'intéressé ;<br/>
<br/>
              3. Considérant que pour rejeter l'appel de la société Gefco contre le jugement du tribunal administratif de Lille annulant la décision du ministre qui avait autorisé le licenciement de M. B..., la cour, après avoir relevé que la demande présentée par l'employeur était motivée non pas par les fautes que le salarié aurait commises dans l'exécution de son contrat de travail mais par des agissements survenus en dehors de l'exécution de ce contrat et rendant impossible le maintien de l'intéressé dans l'entreprise, a jugé que, si l'intéressé avait adressé à une jeune salariée de l'entreprise des appels téléphoniques et de nombreux courriels au contenu déplacé et insultant alors qu'elle lui avait expressément demandé, à plusieurs reprises, de cesser de l'importuner, et s'il avait persisté dans son comportement, allant jusqu'à importuner l'intéressée à son domicile, ces agissements, alors même qu'ils avaient pu affecter psychologiquement la salariée en cause, n'étaient pas, à eux seuls, de nature à rendre impossible le maintien de l'intéressé dans l'entreprise ; qu'en statuant ainsi la cour a, eu égard notamment à la nature et au caractère répété des agissements en cause, à leurs répercussions sur la salariée concernée, et aux antécédents de l'intéressé, inexactement qualifié les faits qui lui étaient soumis ; qu'ainsi, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, la société requérante est fondée à demander l'annulation de l'arrêt attaqué ; <br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de M. B...une somme de 3 000 euros au titre des frais exposés par la société Gefco et non compris dans les dépens ; qu'en revanche, les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de la société Gefco, qui n'est pas, dans la présente instance, la partie perdante, la somme que M. B...demande au titre de ces dispositions ; <br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Douai du 21 juin 2012 est annulé.<br/>
<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Douai.<br/>
<br/>
Article 3 : M. B...versera à la société Gefco la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : Les conclusions de M. B...tendant à l'application de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 5 : La présente décision sera notifiée à la société Gefco et à M. A...B.... <br/>
Copie en sera adressée pour information à la ministre de l'écologie, du développement durable et de l'énergie. <br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-08-02-02-01-02 PROCÉDURE. VOIES DE RECOURS. CASSATION. CONTRÔLE DU JUGE DE CASSATION. BIEN-FONDÉ. QUALIFICATION JURIDIQUE DES FAITS. - CAUSE DU LICENCIEMENT D'UN SALARIÉ PROTÉGÉ - CAS OÙ DES AGISSEMENTS DU SALARIÉ INTERVENUS EN DEHORS DE L'EXÉCUTION DE SON CONTRAT DE TRAVAIL RENDENT IMPOSSIBLE SON MAINTIEN DANS L'ENTREPRISE.
</SCT>
<ANA ID="9A"> 54-08-02-02-01-02 Le licenciement d'un salarié protégé peut être fondé sur un acte ou un comportement du salarié qui, ne méconnaissant pas les obligations découlant pour lui de son contrat de travail, ne constitue pas une faute, si les faits en cause sont établis et de nature, compte tenu de leur répercussion sur le fonctionnement de l'entreprise, à rendre impossible le maintien du salarié dans l'entreprise, eu égard à la nature de ses fonctions et à l'ensemble des règles applicables au contrat de travail de l'intéressé. Le juge exerce un contrôle de qualification juridique sur le point de savoir si les faits sont de nature à justifier le licenciement.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
