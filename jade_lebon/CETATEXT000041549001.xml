<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041549001</ID>
<ANCIEN_ID>JG_L_2020_02_000000423972</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/54/90/CETATEXT000041549001.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 6ème chambres réunies, 05/02/2020, 423972</TITRE>
<DATE_DEC>2020-02-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>423972</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GATINEAU, FATTACCINI ; SCP GOUZ-FITOUSSI ; SCP MEIER-BOURDEAU, LECUYER</AVOCATS>
<RAPPORTEUR>M. Florian Roussel</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2020:423972.20200205</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. C... F..., M. B... F... et M. E... F... ont demandé au tribunal administratif de Paris de condamner l'Etat à leur verser la somme totale de 732 963,74 euros en réparation des préjudices qu'ils estiment avoir subi en raison du décès de Mme A... F.... Par un jugement n° 1216363 du 14 février 2014, le tribunal administratif a condamné l'Etat à leur verser une somme globale de 1 667 euros en leur qualité d'ayants droit de Mme F..., à verser à M. C... F... une somme de 10 292 euros, à M. B... F... une somme de 5 000 euros et à M. E... F... une somme de 5 000 euros.<br/>
<br/>
              Par un arrêt n° 14PA02023 du 10 juillet 2018, la cour administrative d'appel de Paris a, sur appel de M. F... et autres, annulé ce jugement et condamné la commune de Courbevoie à verser à M. C... F..., M. B... F... et M. E... F..., en leur qualité d'ayants droit de Mme A... D..., la somme de 4 000 euros et à verser à M. C... F... la somme de 9 828,74 euros et à MM. B... et M. E... F... la somme de 2 000 euros chacun.<br/>
<br/>
              Par un pourvoi et deux mémoires complémentaires, enregistrés le 7 septembre 2018 et les 18 avril et 3 octobre 2019 au secrétariat du contentieux du Conseil d'Etat, la commune de Courbevoie demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de la " mettre hors de cause " et, subsidiairement, de rejeter l'appel de M. F... et autres ;<br/>
<br/>
              3°) de mettre à la charge des appelants la somme globale de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des collectivités territoriales,<br/>
              - le code de la santé publique,<br/>
              - le code de la défense,<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Florian Roussel, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gatineau, Fattaccini, avocat de la commune de Courbevoie, à la SCP Gouz-Fitoussi, avocat de M. C... F... et autres et à la SCP Meier-Bourdeau, Lecuyer, avocat du préfet de police.<br/>
<br/>
              Vu la note en délibéré, enregistrée le 23 janvier 2020, présentée par le préfet de police.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, le 23 mars 2009, Mme A... F... ayant été victime d'une détresse respiratoire à son domicile à Courbevoie (Hauts-de-Seine), son époux a appelé le numéro 18 et a été mis en relation avec la brigade des sapeurs-pompiers de Paris, laquelle a dépêché sur place une équipe dont l'intervention n'a pas permis d'éviter son décès. Estimant que ce décès était dû à des fautes commises lors de l'intervention des secours, l'époux de la défunte et ses deux fils ont obtenu du tribunal administratif de Paris, par un jugement du 14 février 2014, la condamnation de l'État à réparer divers préjudices. Sur leur appel, la cour administrative d'appel de Paris a, par un arrêt du 10 juillet 2018, annulé ce jugement et condamné, non plus l'Etat mais seulement la commune de Courbevoie à leur verser, en leur qualité d'ayants droit, la somme de 4 000 euros, à l'époux de la défunte la somme de 9 828,74 euros et à ses deux fils la somme de 2 000 euros chacun. La commune de Courbevoie se pourvoit en cassation contre cet arrêt, contre lequel M. F... et autres forment un pourvoi incident.<br/>
<br/>
              2. D'une part, l'article L.2212-1 du code général des collectivités territoriales prévoit que : " Le maire est chargé (...) de la police municipale (...) " et son article L. 2212-2 dispose que : " La police municipale a pour objet d'assurer le bon ordre, la sûreté, la sécurité et la salubrité publiques. Elle comprend notamment: (...) 5° Le soin de (...) pourvoir d'urgence à toutes les mesures d'assistance et de secours (...) ". Aux termes de l'article L. 2216-2 de ce code : " (...) les communes sont civilement responsables des dommages qui résultent de l'exercice des attributions de police municipale, quel que soit le statut des agents qui y concourent. Toutefois, au cas où le dommage résulte, en tout ou partie, de la faute d'un agent ou du mauvais fonctionnement d'un service ne relevant pas de la commune, la responsabilité de celle-ci est atténuée à due concurrence. / La responsabilité de la personne morale autre que la commune dont relève l'agent ou le service concerné ne peut être engagée que si cette personne morale a été mise en cause, soit par la commune, soit par la victime du dommage. S'il n'en a pas été ainsi, la commune demeure seule et définitivement responsable du dommage. " Il résulte de ces dispositions que, si la responsabilité de la commune, à laquelle incombe notamment le soin de pourvoir d'urgence à toutes les mesures d'assistance et de secours, est susceptible d'être engagée par toute faute commise dans l'exercice de ces attributions, celles des fautes qui seraient commises, dans cet exercice, par un service relevant d'une autre personne morale que la commune, sont de nature à atténuer la responsabilité de cette dernière, sous réserve que la commune ou les victimes du dommage aient mis en cause la responsabilité de ce service devant le juge administratif.<br/>
<br/>
              3. D'autre part, aux termes de l'article L. 2521-3 du code général des collectivités territoriales : " Le préfet de police de Paris est chargé du secours et de la défense contre l'incendie dans les départements des Hauts-de-Seine, de la Seine-Saint-Denis et du Val-de-Marne (...) ". Aux termes de l'article R. 2521-2 de ce code : " La brigade de sapeurs-pompiers de Paris assure sa mission dans les départements des Hauts-de-Seine, de la Seine-Saint-Denis et du Val-de-Marne. / A cet effet, elle est à la disposition du préfet de police de Paris. "  Aux termes de l'article R. 1321-19 : " La brigade de sapeurs-pompiers de Paris, placée pour emploi sous l'autorité du préfet de police, est chargée de la prévention, de la protection et de la lutte contre les incendies, à Paris et dans les départements des Hauts-de-Seine, de la Seine-Saint-Denis, du Val-de-Marne (...)./ Elle concourt, avec les autres services et professionnels concernés, (...) aux secours d'urgence dans les limites territoriales mentionnées à l'alinéa précédent. " Enfin, aux termes de l'article R. 3222-13 du code de la défense : " La brigade de sapeurs-pompiers de Paris est une unité militaire de sapeurs-pompiers de l'armée de terre appartenant à l'arme du génie (...) ". Lorsqu'il assure, dans une commune du département des Hauts-de-Seine, de la Seine-Saint-Denis ou du Val-de-Marne, les missions qui lui sont attribuées par ces dispositions, le préfet de police y exerce des missions de police municipale prévues par les dispositions de l'article L. 2212-2 du code général des collectivités territoriales. La responsabilité de l'Etat peut, par suite, être recherchée pour les fautes éventuellement commises dans l'exercice de ces missions dans les conditions fixées par l'article L. 2216-2 du même code.<br/>
<br/>
              4. Il résulte de ce qui précède qu'en jugeant que la responsabilité de la commune de Courbevoie était seule susceptible d'être engagée à raison des agissements fautifs imputés à la brigade de sapeurs-pompiers de Paris à l'occasion de son intervention sur son territoire, alors que cette intervention relevait d'une des missions de police municipale mentionnées à l'article L. 2212-2 du code général des collectivités territoriales et que les victimes du dommage avaient mis en cause, tant devant le tribunal que devant la cour, l'Etat, la cour a commis une erreur de droit. Par suite, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, son arrêt doit être annulé.<br/>
<br/>
              5. L'annulation de cet arrêt rend sans objet le pourvoi incident de M. F... et autres. Il n'y a donc pas lieu d'y statuer.<br/>
<br/>
              6. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de la commune de Courbevoie, qui n'est pas dans la présente instance la partie perdante, les sommes que demandent à ce titre l'Etat et M. F... et autres. Dans les circonstances de l'espèce, il n'y a pas lieu de faire droit aux conclusions présentées, au même titre, par la commune de Courbevoie.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Paris du 10 juillet 2018 est annulé.<br/>
<br/>
Article 2 : Il n'y a pas lieu de statuer sur le pourvoi incident de M. F... et autres.<br/>
<br/>
		Article 3 : L'affaire est renvoyée à la cour administrative d'appel de Paris.<br/>
<br/>
Article 4 : Les conclusions présentées par les parties au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 5 : La présente décision sera notifiée à la commune de Courbevoie, à M. C... F..., premier requérant dénommé, et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">49-02-03 POLICE. AUTORITÉS DÉTENTRICES DES POUVOIRS DE POLICE GÉNÉRALE. PRÉFETS. - FAUTE DANS L'EXERCICE PAR LE PRÉFET DE POLICE DE SA MISSION DE SECOURS ET DE DÉFENSE CONTRE L'INCENDIE DANS UNE COMMUNE DE LA PETITE COURONNE (ART. L. 2521-3 DU CGCT) - POSSIBILITÉ D'ENGAGER LA RESPONSABILITÉ DE L'ETAT (ART. L. 2216-2 DU CGCT) - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">60-03-02-02-01 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. PROBLÈMES D'IMPUTABILITÉ. PERSONNES RESPONSABLES. ÉTAT OU AUTRES COLLECTIVITÉS PUBLIQUES. ÉTAT OU COMMUNE. - FAUTE DANS L'EXERCICE PAR LE PRÉFET DE POLICE DE SA MISSION DE SECOURS ET DE DÉFENSE CONTRE L'INCENDIE DANS UNE COMMUNE DE LA PETITE COURONNE (ART. L. 2521-3 DU CGCT) - POSSIBILITÉ D'ENGAGER LA RESPONSABILITÉ DE L'ETAT (ART. L. 2216-2 DU CGCT) - EXISTENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">60-04-02-02 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RÉPARATION. CAUSES EXONÉRATOIRES DE RESPONSABILITÉ. FAIT DU TIERS. - RESPONSABILITÉ DE LA COMMUNE POUR FAUTE DANS L'EXERCICE DE SES MISSIONS D'ASSISTANCE ET DE SECOURS (5° DE L'ART. L. 2212-2 DU CGCT) - 1) FAUTES COMMISES PAR UNE AUTRE PERSONNE MORALE - FAITS DE NATURE À ATTÉNUER LA RESPONSABILITÉ DE LA COMMUNE - EXISTENCE, SOUS RÉSERVE QUE LA RESPONSABILITÉ DE CETTE PERSONNE AIT ÉTÉ MISE EN CAUSE - 2) ILLUSTRATION - FAUTE COMMISE PAR LE PRÉFET DE POLICE DANS L'EXERCICE DE SA MISSION DE SECOURS ET DE DÉFENSE CONTRE L'INCENDIE DANS UNE COMMUNE DE LA PETITE COURONNE (ART. L. 2521-3 DU CGCT).
</SCT>
<ANA ID="9A"> 49-02-03 Lorsqu'il assure, dans une commune du département des Hauts-de-Seine, de la Seine-Saint-Denis ou du Val-de-Marne, les missions qui lui sont attribuées par les articles L. 2521-3, R. 2521-2 et R. 1321-19 du code général des collectivités territoriales (CGCT), le préfet de police y exerce des missions de police municipale prévues par les dispositions de l'article L. 2212-2 de ce code. La responsabilité de l'Etat peut, par suite, être recherchée pour les fautes éventuellement commises dans l'exercice de ces missions dans les conditions fixées par l'article L. 2216-2 du même code.</ANA>
<ANA ID="9B"> 60-03-02-02-01 Lorsqu'il assure, dans une commune du département des Hauts-de-Seine, de la Seine-Saint-Denis ou du Val-de-Marne, les missions qui lui sont attribuées par les articles L. 2521-3, R. 2521-2 et R. 1321-19 du code général des collectivités territoriales (CGCT), le préfet de police y exerce des missions de police municipale prévues par les dispositions de l'article L. 2212-2 de ce code. La responsabilité de l'Etat peut, par suite, être recherchée pour les fautes éventuellement commises dans l'exercice de ces missions dans les conditions fixées par l'article L. 2216-2 du même code.</ANA>
<ANA ID="9C"> 60-04-02-02 1) Il résulte de l'article L.2212-1, du 5° de l'article L. 2212-2 et de l'article L. 2216-2 du code général des collectivités territoriales (CGCT) que, si la responsabilité de la commune, à laquelle incombe notamment le soin de pourvoir d'urgence à toutes les mesures d'assistance et de secours, est susceptible d'être engagée par toute faute commise dans l'exercice de ces attributions, celles des fautes qui seraient commises, dans cet exercice, par un service relevant d'une autre personne morale que la commune, sont de nature à atténuer la responsabilité de cette dernière, sous réserve que la commune ou les victimes du dommage aient mis en cause la responsabilité de ce service devant le juge administratif.,,,2) Lorsqu'il assure, dans une commune du département des Hauts-de-Seine, de la Seine-Saint-Denis ou du Val-de-Marne, les missions qui lui sont attribuées par les articles L. 2521-3, R. 2521-2 et R. 1321-19 du CGCT, le préfet de police y exerce des missions de police municipale prévues par les dispositions de l'article L. 2212-2 de ce code. La responsabilité de l'Etat peut, par suite, être recherchée pour les fautes éventuellement commises dans l'exercice de ces missions dans les conditions fixées par l'article L. 2216-2 du même code.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
