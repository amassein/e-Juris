<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034879209</ID>
<ANCIEN_ID>JG_L_2017_06_000000399446</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/87/92/CETATEXT000034879209.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 07/06/2017, 399446</TITRE>
<DATE_DEC>2017-06-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>399446</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Isabelle Lemesle</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:399446.20170607</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, un mémoire en réplique et un nouveau mémoire, enregistrés le 3 mai 2016, le 13 janvier 2017 et le 4 avril 2017 au secrétariat du contentieux du Conseil d'Etat, M. A...B...demande au Conseil d'Etat d'annuler pour excès de pouvoir la décision n°16003396 du 17 mars 2016 par laquelle la présidente de la Commission nationale de l'informatique et des libertés l'a informé de sa décision de clôturer sa plainte relative à l'exercice de son droit d'accès aux informations le concernant ainsi que sa mère décédée et sa soeur, auprès de la mutuelle d'assurance des instituteurs de France (MAIF).<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code civil ;<br/>
              - le code de la santé publique ;<br/>
              - la loi n°78-17 du 6 janvier 1978 ;<br/>
              - loi n°2002-303 du 4 mars 2002 ;<br/>
              - le décret n°2005-1309 du 20 octobre 2005;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Isabelle Lemesle, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier qu'à la suite d'un accident de circulation, une procédure judiciaire a été engagée afin de déterminer la réparation du préjudice subi par Mme B..., laquelle est entre-temps décédée. M. A...B..., son fils, a demandé à la mutuelle d'assurance des instituteurs de France (MAIF), par une lettre du 19 octobre 2015, de lui donner accès aux traitements informatisés concernant les suites de cet accident et comportant des informations concernant sa mère, sa soeur ou lui-même. Le 18 décembre 2015, la MAIF lui a transmis par courriel un tableau résumant sur huit pages la teneur des courriers, courriels et appels téléphoniques relatifs à ce sinistre, avec leur date et le nom des intervenants, échangés entre le 27 février 2007, date du sinistre, et le 20 octobre 2015. M. B...estimant qu'il n'avait pas été répondu à sa demande a adressé une plainte auprès de la Commission nationale de l'informatique et des libertés le 2 février 2016, que sa présidente a clôturée, par une lettre du 17 mars 2016, au motif que le droit d'accès conféré aux personnes physiques par l'article 39 de la loi du 6 janvier 1978 est un droit personnel qui ne se transmet pas aux héritiers. M. B...demande l'annulation de cette décision.<br/>
<br/>
              2. Aux termes du dernier alinéa de l'article 2 de la loi du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés : " La personne concernée par un traitement de données à caractère personnel est celle à laquelle se rapportent les données qui font l'objet du traitement ". Aux termes de l'article 39 de cette même loi : " I. Toute personne physique justifiant de son identité a le droit d'interroger le responsable d'un traitement de données à caractère personnel en vue d'obtenir : / (...) 4° La communication, sous une forme accessible, des données à caractère personnel qui la concernent ainsi que de toute information disponible quant à l'origine de celles-ci (...) ". Il résulte de ces dispositions que la communication de données à caractère personnel n'est possible qu'à la personne concernée par ces données. Par suite, la seule qualité d'ayant droit d'une personne à laquelle se rapportent des données ne confère pas la qualité de " personne concernée " par leur traitement au sens des articles 2 et 39 de la loi du 6 janvier 1978.<br/>
<br/>
              3. Toutefois, lorsque la victime d'un dommage décède, son droit à la réparation de ce dommage, entré dans son patrimoine, est transmis à ses héritiers, saisis de plein droit des biens, droits et actions du défunt en application du premier alinéa de l'article 724 du code civil. Par suite, lorsque la victime a engagé une action en réparation avant son décès ou lorsque ses héritiers ont ultérieurement eux-mêmes engagé une telle action, ces derniers doivent être regardés comme des " personnes concernées " au sens des articles 2 et 39 de la loi du 6 janvier 1978 pour l'exercice de leur droit d'accès aux données à caractère personnel concernant le défunt, dans la mesure nécessaire à l'établissement du préjudice que ce dernier  a subi en vue de sa réparation et pour les seuls besoins de l'instance engagée.<br/>
<br/>
              4. Il suit de là que la présidente de la Commission nationale de l'informatique et des libertés a commis une erreur de droit en clôturant la plainte ouverte à l'encontre de la MAIF au motif qu'en sa qualité d'ayant droit et de mandataire de sa soeur, également ayant droit, M. B...ne pouvait être regardé comme " une personne concernée ", au sens des articles 2 et 39 de la loi du 6 janvier 1978, par les données relatives à l'accident dont sa mère a été la victime, alors que le droit à réparation du dommage subi par cette dernière leur avait été transféré en leur qualité d'héritiers. <br/>
<br/>
              5. Il résulte de ce qui précède que M. B...est fondé à demander l'annulation de la décision qu'il attaque, sans qu'il soit besoin d'examiner les autres moyens de sa requête.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : La décision de la présidente de la Commission nationale de l'informatique et des libertés du 17 mars 2016 est annulée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. A...B...et à la Commission nationale de l'informatique et des libertés.<br/>
Copie en sera adressée au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">26-07-05 DROITS CIVILS ET INDIVIDUELS. - AYANT-DROIT D'UNE PERSONNE À LAQUELLE SE RAPPORTENT DES DONNÉES À CARACTÈRE PERSONNEL  - 1) PERSONNE CONCERNÉE (ART. 2 ET 39 DE LA LOI DU 6 JANVIER 1978) - ABSENCE EN PRINCIPE [RJ1] - 2) EXCEPTION - HÉRITIERS DE LA VICTIME D'UN DOMMAGE AYANT ENGAGÉ UNE ACTION EN RÉPARATION AVANT SON DÉCÈS OU AYANT EUX-MÊMES ENGAGÉ ULTÉRIEUREMENT UNE TELLE ACTION [RJ2].
</SCT>
<ANA ID="9A"> 26-07-05 1) Il résulte des dispositions des articles 2 et 39 de la loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés, que la communication de données à caractère personnel n'est possible qu'à la personne concernée par ces données. Par suite, la seule qualité d'ayant droit d'une personne à laquelle se rapportent des données ne confère pas la qualité de personne concernée par leur traitement au sens de ces dispositions.,,,2) Toutefois, lorsque la victime d'un dommage décède, son droit à la réparation de ce dommage, entré dans son patrimoine, est transmis à ses héritiers, saisis de plein droit des biens, droits et actions du défunt en application du premier alinéa de l'article 724 du code civil. Par suite, lorsque la victime a engagé une action en réparation avant son décès ou lorsque ses héritiers ont ultérieurement eux-mêmes engagé une telle action, ces derniers doivent être regardés comme des personnes concernées au sens des articles 2 et 39 de la loi du 6 janvier 1978 pour l'exercice de leur droit d'accès aux données à caractère personnel concernant le défunt, dans la mesure nécessaire à l'établissement du préjudice que ce dernier a subi en vue de sa réparation et pour les seuls besoins de l'instance engagée.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 8 juin 2016, Mme et MM.,, n° 386525, p. 235.,  ,[RJ2] Cf., sur la transmission du droit à réparation aux héritiers, CE, Section, 29 mars 2000, Assistance publique - Hôpitaux de Paris, n° 195662, p.147 ; CE, 27 mai 2015,,, n° 368440, T. pp. 714-815-866-873. Rappr. CE, 29 juin 2011, Ministre du budget, des comptes publics et de la réforme de l'Etat c/ Mme,et autres, n° 339147, T. pp. 937-939.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
