<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036550284</ID>
<ANCIEN_ID>JG_L_2018_01_000000401796</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/55/02/CETATEXT000036550284.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 26/01/2018, 401796</TITRE>
<DATE_DEC>2018-01-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>401796</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Marie Gautier-Melleray</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:401796.20180126</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et deux mémoires en réplique, enregistrés le 25 juillet 2016 et les 22 mai  et 10 août 2017 au secrétariat du contentieux du Conseil d'Etat, Mme A...C...demande au Conseil d'Etat d'annuler pour excès de pouvoir le décret du 13 juillet 2016 portant nomination de la directrice générale de l'Ecole nationale supérieure des métiers de l'image et du son.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son article 13 ;<br/>
              - la loi modifiée n°96-1093 du 16 décembre 1996, notamment son article 90 ;<br/>
              - le décret modifié n°98-371 du 13 mai 1998 ;<br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie Gautier-Melleray, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier que, pour pourvoir au poste de directeur général de l'Ecole nationale supérieure des métiers de l'image et du son (la fémis), le ministère de la culture et de la communication a diffusé dans la presse spécialisée en avril 2016 un appel aux candidatures. Les onze candidatures soumises avant le 25 avril 2016, date limite de dépôt des candidatures, ont été examinées par un comité de recrutement qui a, lors de sa réunion du 4 mai 2016, choisi d'en auditionner trois, dont Mme D...et MmeC.... A l'issue de ces auditions le 26 mai 2016, le comité de recrutement a donné un avis favorable à la candidature de Mme D...et un avis défavorable aux deux autres candidatures. Par un avis du 7 juillet 2016, le conseil d'administration de la fémis a donné un avis favorable à la candidature de MmeD.... Mme C... demande l'annulation pour excès de pouvoir du décret du 13 juillet 2016 par lequel le Président de la République a nommé Mme D...directrice générale de l'Ecole nationale supérieure des métiers de l'image et du son.<br/>
<br/>
              2. En premier lieu, aux termes de l'article 13 de la Constitution du 4 octobre 1958, " Le Président de la République signe les ordonnances et les décrets délibérés en Conseil des ministres. /Il nomme aux emplois civils et militaires de l'Etat (...) ". L'article 90 de la loi du16 décembre 1996 relative à l'emploi dans la fonction publique et à diverses mesures d'ordre statutaire, tel que modifié par la loi du 26 juillet 2005 portant diverses mesures de transpositions du droit communautaire à la fonction publique, dispose que : " I. - Il est créé un établissement public à caractère industriel et commercial appelé " École nationale supérieure des métiers de l'image et du son ". (...) / II. - L'établissement public est administré par un conseil d'administration et dirigé par un directeur général. (...) / Le président du conseil d'administration et le directeur général sont nommés par décret ". <br/>
<br/>
              3. Dès  lors, d'une part, que l'article 90 de la loi du 16 décembre 1996 précité prévoit que la nomination du directeur général de l'établissement public " École nationale supérieure des métiers de l'image et du son " a lieu " par décret " et que, d'autre part, cette expression doit être interprétée comme réservant cette compétence au Président de la République compte tenu des termes de l'article 13 de la Constitution précité, le moyen tiré de ce que le décret attaqué aurait été pris par une autorité incompétente ne peut qu'être écarté alors même que l'article 11 du décret du 13 mai 1998 portant statut de l'Ecole nationale supérieure des métiers de l'image et du son précise que " Le directeur est nommé pour une durée de trois ans renouvelable deux fois par arrêté du ministre chargé de la culture, après avis du conseil d'administration (...) ".<br/>
<br/>
              4. En deuxième lieu, il ressort des pièces du dossier que  l'avis de vacance du poste de directeur général de l'établissement public en cause a été publié au mois d'avril 2016 dans deux hebdomadaires spécialisés, " Ecran Total " et " Le film français ", et diffusés sur le site internet de recherche d'emploi Talents.fr ainsi que sur le site internet de cet établissement public. Il suit de là que le moyen tiré de ce que le décret attaqué serait entaché d'irrégularité faute pour l'avis de vacance du poste d'avoir bénéficié d'une publicité suffisante manque en fait.<br/>
<br/>
              5. En troisième lieu, il ressort des pièces du dossier que la nomination du directeur général de l'établissement public " École nationale supérieure des métiers de l'image et du son " a été précédée d'un examen des candidatures par un comité de recrutement, chargé de donner un avis à l'autorité de nomination. Ce comité était présidé par le président du conseil d'administration de cet établissement public et réunissait deux professionnels, également membres de ce conseil d'administration, un représentant du secrétariat général du ministère de la culture et de la communication et deux représentants du Centre national du cinéma. S'il n'est pas contesté que certains des membres siégeant au sein de ce comité connaissaient préalablement la candidate finalement retenue pour l'avoir côtoyée, pour l'un, alors qu'elle exerçait au sein du groupe Canal + et, pour trois autres, dans le cadre du conseil d'administration de la fémis au sein duquel elle siégeait en qualité de personnalité qualifiée, leur présence au sein de l'organe consultatif chargé d'examiner des candidatures ne peut être regardée par elle-même comme caractérisant un défaut d'impartialité au seul motif qu'ils avaient auparavant entretenu des relations de nature professionnelle avec cette candidate. Il ne ressort au demeurant pas des pièces du dossier que l'avis émis par le comité soit intervenu pour des motifs étrangers à la valeur respective des candidats révélant de la part de ses membres un manque d'impartialité.<br/>
<br/>
              6. En quatrième lieu, le moyen tiré de ce que la procédure suivie pour la nomination contestée aurait méconnu les dispositifs de contrôle et de déontologie mis en place au sein du ministère de la culture et de la communication n'est pas assorti des précisions permettant d'en apprécier le bien fondé. <br/>
<br/>
              7. En cinquième lieu, et alors qu'aucune disposition n'interdit à un membre du conseil d'administration de l'École nationale supérieure des métiers de l'image et du son de se porter candidat au poste de directeur général de cet établissement public, la circonstance que la candidate retenue  pour ce poste était membre du conseil d'administration ne caractérise, par elle-même, aucune rupture d'égalité entre les candidats.<br/>
<br/>
              8. En sixième et dernier lieu, il ne ressort pas des pièces du dossier que, compte tenu des qualifications et de l'expérience professionnelle de MmeD..., diplômée de l'Ecole des hautes études commerciales de Paris et qui a exercé depuis 1995 diverses fonctions en lien avec le cinéma, notamment celle de directrice du pôle thématique cinéma et celle de directrice du cinéma du groupe Canal +, sa nomination en qualité de directeur général de la fémis soit entachée d'erreur manifeste d'appréciation. <br/>
<br/>
              9. Il résulte de tout ce qui précède que Mme C...n'est pas fondée à demander l'annulation du décret qu'elle attaque.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
       --------------<br/>
<br/>
Article 1er : La requête de Mme C...est rejetée.<br/>
Article 2 : La présente décision sera notifiée à MoniqueC..., au Président de la République, à la ministre de la culture et au Premier ministre. <br/>
Copie en sera adressée à Mme B...D....<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-04-03-07 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. PRINCIPES GÉNÉRAUX DU DROIT. PRINCIPES INTÉRESSANT L'ACTION ADMINISTRATIVE. - PRINCIPE D'IMPARTIALITÉ - EXAMEN DES CANDIDATURES PAR UN COMITÉ DE RECRUTEMENT CHARGÉ DE DONNER UN AVIS À L'AUTORITÉ DE NOMINATION - PRÉSENCE AU SEIN DE CE COMITÉ DE MEMBRES AYANT ENTRETENU DES RELATIONS PROFESSIONNELLES AVEC LA CANDIDATE RETENUE - CIRCONSTANCE DE NATURE À CARACTÉRISER UN DÉFAUT D'IMPARTIALITÉ - ABSENCE, PAR ELLE-MÊME [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">09-05 ARTS ET LETTRES. CINÉMA. - RECRUTEMENT DU DIRECTEUR GÉNÉRAL DE LA FÉMIS - EXAMEN DES CANDIDATURES PAR UN COMITÉ DE RECRUTEMENT CHARGÉ DE DONNER UN AVIS À L'AUTORITÉ DE NOMINATION - PRÉSENCE AU SEIN DE CE COMITÉ DE MEMBRES AYANT ENTRETENU DES RELATIONS PROFESSIONNELLES AVEC LA CANDIDATE RETENUE - CIRCONSTANCE DE NATURE À CARACTÉRISER UN DÉFAUT D'IMPARTIALITÉ - ABSENCE, PAR ELLE-MÊME [RJ1].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">36-03-03-007 FONCTIONNAIRES ET AGENTS PUBLICS. ENTRÉE EN SERVICE. NOMINATIONS. CONDITIONS DE NOMINATION. - EXAMEN DES CANDIDATURES PAR UN COMITÉ DE RECRUTEMENT CHARGÉ DE DONNER UN AVIS À L'AUTORITÉ DE NOMINATION - PRÉSENCE AU SEIN DE CE COMITÉ DE MEMBRES AYANT ENTRETENU DES RELATIONS PROFESSIONNELLES AVEC LA CANDIDATE RETENUE - CIRCONSTANCE DE NATURE À CARACTÉRISER UN DÉFAUT D'IMPARTIALITÉ - ABSENCE, PAR ELLE-MÊME [RJ1].
</SCT>
<ANA ID="9A"> 01-04-03-07 Recrutement du directeur général de l'établissement public « École nationale supérieure des métiers de l'image et du son » (la fémis) précédée d'un examen des candidatures par un comité de recrutement, chargé de donner un avis à l'autorité de nomination.,,,La présence au sein de cet organe consultatif de membres ayant entretenu des relations de nature professionnelle avec la candidate finalement retenue ne peut être regardée par elle-même comme caractérisant un défaut d'impartialité.</ANA>
<ANA ID="9B"> 09-05 Recrutement du directeur général de l'établissement public « École nationale supérieure des métiers de l'image et du son » (la fémis) précédée d'un examen des candidatures par un comité de recrutement, chargé de donner un avis à l'autorité de nomination.,,,La présence au sein de cet organe consultatif de membres ayant entretenu des relations de nature professionnelle avec la candidate finalement retenue ne peut être regardée par elle-même comme caractérisant un défaut d'impartialité.</ANA>
<ANA ID="9C"> 36-03-03-007 Recrutement du directeur général de l'établissement public « École nationale supérieure des métiers de l'image et du son » (la fémis) précédée d'un examen des candidatures par un comité de recrutement, chargé de donner un avis à l'autorité de nomination.,,,La présence au sein de cet organe consultatif de membres ayant entretenu des relations de nature professionnelle avec la candidate finalement retenue ne peut être regardée par elle-même comme caractérisant un défaut d'impartialité.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., s'agissant d'un jury, CE, 4 novembre 1994,,, n° 1511127, T. p. 993 ; s'agissant du recrutement d'un professeur d'université, CE, 23 avril 1997,,, n° 167862, T. p. 840.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
