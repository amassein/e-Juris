<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043074296</ID>
<ANCIEN_ID>JG_L_2021_01_000000445958</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/07/42/CETATEXT000043074296.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 5ème chambres réunies, 27/01/2021, 445958</TITRE>
<DATE_DEC>2021-01-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>445958</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DE NERVO, POUPET ; SCP ZRIBI, TEXIER</AVOCATS>
<RAPPORTEUR>Mme Coralie Albumazard</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier  Fuchs</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:445958.20210127</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme E... C... a demandé au juge des référés du tribunal administratif de Marseille, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, à titre principal, d'une part, d'ordonner au directeur de l'Office français de l'immigration et de l'intégration (OFII) d'indiquer le centre d'accueil pour demandeurs d'asile ou le centre d'hébergement susceptible de l'accueillir avec ses deux fils, dans un délai de cinq jours sous astreinte de 100 euros par jour de retard, d'autre part, d'ordonner au directeur de l'Office de lui verser l'allocation pour demandeur d'asile en sa qualité de représentante de son fils mineur B..., en tenant compte du nombre de personnes composant le foyer, et, à titre subsidiaire, à défaut pour l'Office d'indiquer un centre d'accueil, de lui verser un montant additionnel destiné à couvrir les frais de logement du foyer, sous les mêmes conditions de délai et d'astreinte.<br/>
<br/>
              Par une ordonnance n° 2007657 du 20 octobre 2020, le juge des référés du tribunal administratif de Marseille a enjoint à l'Office français de l'immigration et de l'intégration de faire bénéficier Mme C... et ses deux fils B... et Wisdom de l'hébergement inclus dans les conditions matérielles d'accueil, ainsi que du versement de l'allocation pour demandeur d'asile pour trois personnes, dans un délai de 48 heures à compter de la notification de l'ordonnance, sous astreinte de 100 euros par jour de retard, et a rejeté le surplus des conclusions de sa demande.<br/>
<br/>
              Par une requête et un mémoire complémentaire, enregistrés les 4 novembre 2020 et 5 janvier 2021 au secrétariat du contentieux du Conseil d'Etat, l'Office français de l'immigration et de l'intégration demande au Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'annuler cette ordonnance en tant qu'elle lui enjoint de verser à Mme C... l'allocation pour demandeur d'asile pour trois personnes.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la directive 2013/33/UE du Parlement européen et du Conseil du 26 juin 2013 établissant des normes pour l'accueil des personnes demandant la protection internationale ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme A... D..., maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Olivier Fuchs, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP de Nervo, Poupet, avocat de l'Office français de l'immigration et de l'intégration et à la SCP Zribi et Texier, avocat de Mme C... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ".<br/>
<br/>
              2. Il résulte de l'instruction que Mme C..., ressortissante du Nigéria, née en 1995, a présenté en 2017 une demande d'asile qui a été définitivement rejetée par la Cour nationale du droit d'asile le 29 août 2019. Le 4 octobre 2019, Mme C... a présenté une demande d'asile au nom de son fils B..., né le 3 novembre 2017 à Marseille, que le préfet des Bouches-du-Rhône a enregistrée comme une première demande. Une attestation de demandeur d'asile valable jusqu'au 18 septembre 2020 et en cours de renouvellement a été délivrée au nom de cet enfant. Toutefois, l'Office français de l'immigration et de l'intégration (OFII) a refusé de le faire bénéficier des conditions matérielles d'accueil. Saisi par Mme C..., le juge des référés du tribunal administratif de Marseille a, par une ordonnance du 20 octobre 2020, enjoint à l'Office, sur le fondement de l'article L. 521-2 du code de justice administrative, de faire bénéficier Mme C... au nom de son fils B..., dans un délai de 48 heures, des conditions matérielles d'accueil et, par conséquent, de mettre à sa disposition un hébergement adapté à sa situation et de lui verser, pour le compte de son fils B..., l'allocation pour demandeur d'asile d'un montant forfaitaire calculé sur la base d'un foyer constitué de trois personnes. L'Office fait appel de cette ordonnance en tant qu'elle lui enjoint de verser une allocation pour demandeur d'asile tenant compte de l'ensemble de la famille et non du seul enfant demandeur d'asile.<br/>
<br/>
              Sur les interventions :<br/>
<br/>
              3. La Cimade justifie d'un intérêt suffisant au maintien de l'ordonnance attaquée. Ainsi, son intervention en défense est recevable.<br/>
<br/>
              4. Le ministre de l'intérieur justifie d'un intérêt suffisant à l'annulation de l'ordonnance attaquée. Ainsi, son intervention au soutien de l'appel de l'Office est recevable.<br/>
<br/>
              Sur l'urgence :<br/>
<br/>
              5. Il résulte de l'instruction que Mme C... est dépourvue de toute ressource. Par suite, la condition d'urgence prévue par les dispositions de l'article L. 521-2 du code de justice administrative est, en l'espèce, remplie.<br/>
<br/>
              Sur l'atteinte grave et manifestement illégale à une liberté fondamentale :<br/>
<br/>
              6. D'une part, aux termes de l'article L. 744-1 du code de l'entrée et du séjour des étrangers et du droit d'asile : " Les conditions matérielles d'accueil du demandeur d'asile, au sens de la directive 2013/33/UE du Parlement européen et du Conseil, du 26 juin 2013, établissant des normes pour l'accueil des personnes demandant la protection internationale, sont proposées à chaque demandeur d'asile par l'Office français de l'immigration et de l'intégration après l'enregistrement de la demande d'asile par l'autorité administrative compétente, en application du présent chapitre. Les conditions matérielles d'accueil comprennent les prestations et l'allocation prévues au présent chapitre. / (...) ". Aux termes de l'article L. 744-9 du même code : " Le demandeur d'asile qui a accepté les conditions matérielles d'accueil proposées en application de l'article L. 744-1 bénéficie d'une allocation pour demandeur d'asile s'il satisfait à des conditions d'âge et de ressources, dont le versement est ordonné par l'Office français de l'immigration et de l'intégration. (...) / Un décret définit le barème de l'allocation pour demandeur d'asile, en prenant en compte les ressources de l'intéressé, son mode d'hébergement et, le cas échéant, les prestations offertes par son lieu d'hébergement. Le barème de l'allocation pour demandeur d'asile prend en compte le nombre d'adultes et d'enfants composant la famille du demandeur d'asile et accompagnant celui-ci. (...) ". En application de l'article D. 744-17 du même code : " Sont admis au bénéfice de l'allocation pour demandeur d'asile : / 1° Les demandeurs d'asile qui ont accepté les conditions matérielles d'accueil proposées par l'Office français de l'immigration et de l'intégration en application de l'article L. 744-1 et qui sont titulaires de l'attestation de demande d'asile délivrée en application de l'article L. 741-1 ; (...) ". Aux termes de l'article D. 744-18 du même code : " Pour bénéficier de l'allocation pour demandeur d'asile, les personnes mentionnées aux 1° et 2° de l'article D. 744-17 doivent être âgées de dix-huit ans révolus ". Aux termes de l'article D. 744-25 du même code : " Au sein du foyer, le bénéficiaire de l'allocation est celui qui a déposé la demande. Toutefois, le bénéficiaire peut être désigné d'un commun accord (...) ". Enfin, en application de l'article D. 744-26 du même code : " En application du cinquième alinéa de l'article L. 744-9, l'allocation pour demandeur d'asile est composée d'un montant forfaitaire, dont le niveau varie en fonction du nombre de personnes composant le foyer, et, le cas échéant, d'un montant additionnel destiné à couvrir les frais d'hébergement ou de logement du demandeur ". <br/>
<br/>
              7. L'article L. 744-8 du même code prévoit, par ailleurs, que le bénéfice des conditions matérielles d'accueil peut être refusé, notamment, " si le demandeur présente une demande de réexamen de sa demande d'asile (...) ". En outre, aux termes de l'article D. 744-37 du même code, le bénéfice de l'allocation pour demandeur d'asile peut être refusé par l'OFII, notamment, en cas de fraude. Il résulte toutefois du point 5 de l'article 20 de la directive 2013/33/UE du Parlement européen et du Conseil du 26 juin 2013 établissant des normes pour l'accueil des personnes demandant la protection internationale qu'un tel refus ne peut être pris qu'au terme d'un examen au cas par cas, fondé sur la situation particulière de la personne concernée, en particulier dans le cas des personnes vulnérables mentionnées à l'article 21 de cette directive, lequel vise notamment les mineurs. <br/>
<br/>
              8. D'autre part, aux termes du premier alinéa de l'article L. 741-1 du même code : " Tout étranger présent sur le territoire français et souhaitant demander l'asile se présente en personne à l'autorité administrative compétente, qui enregistre sa demande et procède à la détermination de l'Etat responsable en application du règlement (UE) n° 604/2013 du Parlement européen et du Conseil, du 26 juin 2013, établissant les critères et mécanismes de détermination de l'Etat membre responsable de l'examen d'une demande de protection internationale introduite dans l'un des Etats membres par un ressortissant de pays tiers ou un apatride, ou en application d'engagements identiques à ceux prévus par le même règlement, dans des conditions fixées par décret en Conseil d'Etat. (...) / Lorsque la demande d'asile est présentée par un étranger qui se trouve en France accompagné de ses enfants mineurs, la demande est regardée comme présentée en son nom et en celui de ses enfants. Lorsqu'il est statué sur la demande de chacun des parents, la décision accordant la protection la plus étendue est réputée prise également au bénéfice des enfants. Cette décision n'est pas opposable aux enfants qui établissent que la personne qui a présenté la demande n'était pas en droit de le faire (...) / L'étranger est tenu de coopérer avec l'autorité administrative compétente en vue d'établir son identité, sa ou ses nationalités, sa situation familiale, son parcours depuis son pays d'origine ainsi que, le cas échéant, ses demandes d'asile antérieures. Il présente tous documents d'identité ou de voyage dont il dispose (...) ".<br/>
<br/>
              9. Enfin, aux termes de l'article L. 723-15 du même code : " Constitue une demande de réexamen une demande d'asile présentée après qu'une décision définitive a été prise sur une demande antérieure, y compris lorsque le demandeur avait explicitement retiré sa demande antérieure, lorsque l'office a pris une décision définitive de clôture en application de l'article L. 723-13 ou lorsque le demandeur a quitté le territoire, même pour rejoindre son pays d'origine. (...) / Si des éléments nouveaux sont présentés par le demandeur d'asile alors que la procédure concernant sa demande est en cours, ils sont examinés, dans le cadre de cette procédure, par l'office si celui-ci n'a pas encore statué ou par la Cour nationale du droit d'asile si celle-ci est saisie ".<br/>
<br/>
              10. Il résulte de ces dispositions qu'il appartient à l'étranger présent sur le territoire français et souhaitant demander l'asile de présenter une demande en son nom et, le cas échéant, en celui de ses enfants mineurs qui l'accompagnent. En cas de naissance ou d'entrée en France d'un enfant mineur postérieurement à l'enregistrement de sa demande, l'étranger est tenu, tant que l'Office français de protection des réfugiés et apatrides ou, en cas de recours, la Cour nationale du droit d'asile, ne s'est pas prononcé, d'en informer cette autorité administrative ou cette juridiction. La décision rendue par l'office ou, en cas de recours, par la Cour nationale du droit d'asile, est réputée l'être à l'égard du demandeur et de ses enfants mineurs, sauf dans le cas où le mineur établit que la personne qui a présenté la demande n'était pas en droit de le faire. <br/>
<br/>
              11. Ces dispositions ne font pas obstacle à ce que les parents d'un enfant né après l'enregistrement de leur demande d'asile présentent, postérieurement au rejet définitif de leur propre demande, une demande au nom de leur enfant. Il résulte toutefois de ce qui a été dit au point précédent que la demande ainsi présentée au nom du mineur doit alors être regardée, dans tous les cas, comme une demande de réexamen au sens de l'article L. 723-15 du code de l'entrée et du séjour des étrangers et du droit d'asile.<br/>
<br/>
              12. La demande ainsi présentée au nom du mineur présentant le caractère d'une demande de réexamen, le bénéfice des conditions matérielles d'accueil peut être refusé à la famille, conformément aux dispositions de l'article L. 744-8, sous réserve d'un examen au cas par cas tenant notamment compte de la présence au sein de la famille du mineur concerné. Lorsque l'Office français de l'immigration et de l'intégration décide de proposer à la famille les conditions matérielles d'accueil et que les parents les acceptent, il est tenu, jusqu'à ce qu'il ait été statué sur cette demande, d'héberger la famille et de verser aux parents l'allocation pour demandeur d'asile, le montant de cette dernière étant calculé, en application des dispositions des articles L. 744-9 et D. 744-26 du code de l'entrée et du séjour des étrangers et du droit d'asile précité, en fonction du nombre de personnes composant le foyer du demandeur d'asile.<br/>
<br/>
              13. Il résulte de ce qui précède que l'Office français de l'immigration et de l'intégration, dont l'appel se borne à demander l'annulation de l'ordonnance du juge des référés du tribunal administratif de Marseille en tant qu'elle a déterminé le montant de l'allocation pour demandeur d'asile qu'elle a ordonné de verser à Mme C..., n'est pas fondé à soutenir que c'est à tort que, par cette ordonnance, le juge des référés lui a enjoint de verser à cette dernière, en sa qualité de représentante légale de son fils mineur B..., l'allocation pour demandeur d'asile en tenant compte des trois personnes composant le foyer du demandeur.<br/>
<br/>
              14. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Office français de l'immigration et de l'intégration le versement à Mme C... de la somme de 2 500 euros qu'elle demande au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les interventions de la Cimade et du ministre de l'intérieur sont admises.<br/>
Article 2 : La requête de l'Office français de l'immigration et de l'intégration est rejetée.<br/>
Article 3 : L'Office français de l'immigration et de l'intégration versera la somme de 2 500 euros à Mme C... en application de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à l'Office français de l'immigration et de l'intégration, à Mme E... C..., à la Cimade et au ministre de l'intérieur. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">095-02 - DEMANDE PRÉSENTÉE PAR L'ÉTRANGER PARENT D'ENFANTS MINEURS - 1) ENFANTS NÉS OU PRÉSENTS SUR LE TERRITOIRE À LA DATE DE L'ENREGISTREMENT DE LA DEMANDE - FACULTÉ DE PRÉSENTER UNE DEMANDE EN LEUR NOM - EXISTENCE - 2) ENFANTS NÉS OU ENTRÉS EN FRANCE APRÈS L'ENREGISTREMENT DE LA DEMANDE - A) OBLIGATION, POUR LE DEMANDEUR, D'EN INFORMER L'OFPRA OU LA CNDA AVANT LEUR DÉCISION - EXISTENCE - B) DÉCISION RÉPUTÉE RENDUE À L'ÉGARD DE CES ENFANTS - EXISTENCE - C) FACULTÉ DE PRÉSENTER UNE DEMANDE AU NOM DE CES ENFANTS APRÈS LE REJET DÉFINITIF DE LA DEMANDE DES PARENTS - I) EXISTENCE, LA DEMANDE DEVANT ALORS ÊTRE REGARDÉE COMME UNE DEMANDE DE RÉEXAMEN [RJ2] - II) CONSÉQUENCE - POSSIBILITÉ DE REFUSER LE BÉNÉFICE DES CONDITIONS MATÉRIELLES D'ACCUEIL - EXISTENCE, SOUS RÉSERVE D'UN EXAMEN AU CAS PAR CAS - III) DÉCISION DE L'OFII D'OCTROYER CES CONDITIONS - OCTROI AU BÉNÉFICE DE L'ENSEMBLE DU FOYER [RJ3].
</SCT>
<ANA ID="9A"> 095-02 1) Il résulte de l'article L. 741-1 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA) qu'il appartient à l'étranger présent sur le territoire français et souhaitant demander l'asile de présenter une demande en son nom et, le cas échéant, en celui de ses enfants mineurs qui l'accompagnent.... ,,2) a) En cas de naissance ou d'entrée en France d'un enfant mineur postérieurement à l'enregistrement de sa demande, l'étranger est tenu, tant que l'Office français de protection des réfugiés et apatrides (OFPRA) ou, en cas de recours, la Cour nationale du droit d'asile (CNDA), ne s'est pas prononcé, d'en informer cette autorité administrative ou cette juridiction.... ,,b) La décision rendue par l'OFPRA ou, en cas de recours, par la CNDA, est réputée l'être à l'égard du demandeur et de ses enfants mineurs, sauf dans le cas où le mineur établit que la personne qui a présenté la demande n'était pas en droit de le faire.... ,,c) i) Ces dispositions ne font pas obstacle à ce que les parents d'un enfant né après l'enregistrement de leur demande d'asile présentent, postérieurement au rejet définitif de leur propre demande, une demande au nom de leur enfant. Il résulte toutefois de ce qui a été dit au point précédent que la demande ainsi présentée au nom du mineur doit alors être regardée, dans tous les cas, comme une demande de réexamen au sens de l'article L. 723-15 du CESEDA.,,,ii) La demande ainsi présentée au nom du mineur présentant le caractère d'une demande de réexamen, le bénéfice des conditions matérielles d'accueil peut être refusé à la famille, conformément à l'article L. 744-8, sous réserve d'un examen au cas par cas tenant notamment compte de la présence au sein de la famille du mineur concerné.... ,,iii) Lorsque l'Office français de l'immigration et de l'intégration (OFII) décide de proposer à la famille les conditions matérielles d'accueil et que les parents les acceptent, il est tenu, jusqu'à ce qu'il ait été statué sur cette demande, d'héberger la famille et de verser aux parents l'allocation pour demandeur d'asile (ADA), le montant de cette dernière étant calculé, en application des articles L. 744-9 et D. 744-26 du CESEDA, en fonction du nombre de personnes composant le foyer du demandeur d'asile.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 6 novembre 2019, Office français de protection des réfugiés et apatrides c/ Mme,, n° 422017, T. p. 578.,,[RJ2] Ab. jur., s'agissant de la faculté, pour l'enfant né après le rejet de la demande d'asile de ses parents, de présenter une demande qui lui est propre, CE, juge des référés, 20 décembre 2019, Office français de l'immigration et de l'intégration c/ M.,et M.,, n° 436700, T. p. 577.,,[RJ3] Cf., sur l'obligation pour l'OFII d'héberger l'enfant avec ses parents et de verser l'ADA, CE, juge des référés, 20 décembre 2019, Office français de l'immigration et de l'intégration c/ M.,et M.,, n° 436700, T. p. 577.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
