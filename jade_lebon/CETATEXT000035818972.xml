<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035818972</ID>
<ANCIEN_ID>JG_L_2017_10_000000402322</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/81/89/CETATEXT000035818972.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 06/10/2017, 402322</TITRE>
<DATE_DEC>2017-10-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>402322</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MARLANGE, DE LA BURGADE ; SCP LEVIS</AVOCATS>
<RAPPORTEUR>M. Thomas Odinot</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier Henrard</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:402322.20171006</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Omnium thermique des grands ensembles (ci-après Omnitherm) a demandé au tribunal administratif de Grenoble d'annuler le titre de recettes du 12 juillet 2013 d'un montant de 625 753,18 euros émis pour la commune de Valence, de la décharger de cette somme et de mettre à la charge de la commune la somme de 15 000 euros au titre des frais non compris dans les dépens. Par un jugement n° 1304912 du 17 juillet 2015, le tribunal administratif de Grenoble a annulé le titre de recettes du 12 juillet 2013 et déchargé la société Omnitherm de la somme de 625 753,18 euros et rejeté le surplus des conclusions des parties.<br/>
<br/>
              Par un arrêt n° 15LY03127 du 23 juin 2016, la cour administrative d'appel de Lyon a rejeté l'appel formé par la commune de Valence contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 10 août et 27 octobre 2016 et 30 janvier 2017 au secrétariat du contentieux du Conseil d'Etat, la commune de Valence demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de la société Omnitherm la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'environnement ;<br/>
              - le code général des collectivités territoriales ;<br/>
              - le code de justice administrative ; <br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Thomas Odinot, auditeur,  <br/>
<br/>
              - les conclusions de M. Olivier Henrard, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Marlange, de la Burgade, avocat de la commune de Valence et à la SCP Lévis, avocat de la société Omnium thermique des grands ensembles.<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des énonciations de l'arrêt attaqué que la commune de Valence a délégué en 1968 le service public du chauffage urbain de la zone à urbaniser en priorité (ZUP) de Valence à la SCET - Bureau d'Etudes Techniques pour l'Urbanisme et l'Equipement (BETURE), aux droits de laquelle est venue la société Omnitherm ; que la commune a émis, le 12 juillet 2013, un titre de recettes à l'encontre de la société pour un montant de 625 753,18 euros, correspondant au produit, majoré des intérêts, de la cession des quotas de dioxyde de carbone excédentaires qui avaient été attribués à la société dans le cadre du premier plan national d'allocation des quotas établi pour la période 2005-2007 ; que, par un jugement du 17 juillet 2015, le tribunal administratif de Grenoble a annulé ce titre de recettes et a déchargé la société de l'obligation de payer cette somme ; que, par un arrêt du 23 juin 2016 contre lequel la commune de Valence se pourvoit en cassation, la cour administrative d'appel de Lyon a rejeté l'appel formé par la commune ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 229-7 du code de l'environnement : " (...) Pour chaque installation bénéficiant de l'autorisation d'émettre des gaz à effet de serre, l'Etat affecte à l'exploitant, pour une période déterminée, des quotas d'émission et lui délivre chaque année, au cours de cette période, une part des quotas qui lui ont été ainsi affectés. (...) A l'issue de chacune des années civiles de la période d'affectation, l'exploitant restitue à l'Etat sous peine des sanctions prévues à l'article L. 229-18 un nombre de quotas égal au total des émissions de gaz à effet de serre de ses installations, que ces quotas aient été délivrés ou qu'ils aient été acquis en vertu de l'article L. 229-15 " ; qu'en vertu de l'article L. 229-15 du même code :  " I - Les quotas d'émission de gaz à effet de serre délivrés aux exploitants d'installations autorisées à émettre ces gaz ou aux exploitants d'aéronef sont des biens meubles exclusivement matérialisés par une inscription au compte de leur détenteur dans le registre national mentionné à l'article L. 229-16. Ils sont négociables, transmissibles par virement de compte à compte et confèrent des droits identiques à leurs détenteurs. Ils peuvent être cédés dès leur délivrance sous réserve des dispositions du II de l'article L. 229-12 et de l'article L. 229-18 (...) II. - Les quotas d'émission peuvent être acquis, détenus et cédés par tout exploitant d'une installation au titre de laquelle a été délivrée par un Etat membre de la Communauté européenne une autorisation d'émettre des gaz à effet de serre, par toute personne physique ressortissante d'un Etat membre de la Communauté européenne, par toute personne morale y ayant son siège et par les Etats membres eux-mêmes " ;<br/>
<br/>
              3. Considérant, en premier lieu, que les dispositions législatives citées ci-dessus établissent un mécanisme d'attribution par l'Etat de quotas d'émission de dioxyde de carbone aux exploitants des installations concernées ainsi qu'un système d'échange des quotas d'émission excédentaires ; qu'elles impliquent nécessairement que, dans le cadre d'une concession de service public et quelles que soient les clauses du contrat sur ce point, les quotas appartiennent à l'exploitant concessionnaire auquel ils ont été attribués ; qu'ainsi, en relevant que, eu égard au régime spécifique auquel ils sont soumis, les quotas excédentaires d'émission de gaz à effet de serre délivrés à un délégataire de service public exploitant une installation autorisée à émettre ces gaz ne relèvent pas de la catégorie des biens de retour et que la commune de Valence ne pouvait par suite se prévaloir d'un droit de propriété sur les quotas cédés par la société Omnitherm, la cour administrative d'appel de Lyon, qui a suffisamment motivé son arrêt, n'a commis ni erreur de droit, ni erreur de qualification juridique ;  <br/>
<br/>
              4. Considérant, en deuxième lieu, que, c'est sans dénaturer les faits qui lui étaient soumis ni commettre d'erreur de droit, que la cour a jugé que la commune n'était pas fondée à demander à la société Omnitherm le versement d'une somme au titre d'un enrichissement sans cause dès lors que l'enrichissement de la société trouvait sa cause dans la cession de quotas dont elle était propriétaire ; que, par ailleurs, elle n'a en tout état de cause pas davantage dénaturé les faits ou commis d'erreur de droit en relevant qu'aucune disposition législative ou réglementaire ni aucune stipulation contractuelle n'imposaient à la société Omnitherm d'informer la collectivité de la cession des quotas ni ne faisaient obstacle à ce qu'elle en perçoive la contrepartie et en en déduisant que la société n'avait pas méconnu le principe de loyauté contractuelle ;<br/>
<br/>
              5. Considérant, en troisième lieu, que la cour n'a pas commis d'erreur de droit ou, comme le soutient aussi le pourvoi, d'erreur de qualification juridique en jugeant que la commune ne pouvait en tout état de cause invoquer utilement la rupture de l'équilibre du contrat de délégation de service public du chauffage urbain qui résulterait, selon elle, de la cession de quotas par le délégataire pour obtenir unilatéralement de celui-ci le reversement du produit de cette cession en émettant un titre de recettes ;  <br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède que le pourvoi de la commune de Valence ne peut qu'être rejeté ;<br/>
<br/>
              7. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la société Omnitherm qui n'est pas, dans la présente instance, la partie perdante ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de Valence, la somme de 4 000 euros au titre des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la commune de Valence est rejeté.<br/>
Article 2 : La commune de Valence versera à la société Omnitherm la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la commune de Valence et à la société Omnitherm.<br/>
Copie en sera adressée au ministre d'Etat, ministre de la transition écologique et solidaire et au ministre de l'économie et des finances. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39 MARCHÉS ET CONTRATS ADMINISTRATIFS. - DÉLÉGATION DE SERVICE PUBLIC - QUOTAS D'ÉMISSION DE GAZ À EFFET DE SERRE DÉLIVRÉS AU DÉLÉGATAIRE (ART. L. 229-7 ET L. 229-15 DU CODE DE L'ENVIRONNEMENT) - BIENS DITS DE RETOUR [RJ1] - EXCLUSION.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">44-008-02-01 NATURE ET ENVIRONNEMENT. - ATTRIBUTION DES QUOTAS D'ÉMISSION DE GAZ À EFFET DE SERRE À UN DÉLÉGATAIRE DE SERVICE PUBLIC- BIENS DITS DE RETOUR [RJ1] - EXCLUSION.
</SCT>
<ANA ID="9A"> 39 Les dispositions des articles L. 229-7 et L. 229-15 du code de l'environnement établissent un mécanisme d'attribution par l'Etat de quotas d'émission de dioxyde de carbone aux exploitants des installations concernées ainsi qu'un système d'échange des quotas d'émission excédentaires et impliquent nécessairement que, dans le cadre d'une concession de service public et quelles que soient les clauses du contrat sur ce point, les quotas appartiennent à l'exploitant concessionnaire auquel ils ont été attribués. Par suite, les quotas excédentaires d'émission de gaz à effet de serre délivrés à un délégataire de service public exploitant une installation autorisée à émettre ces gaz ne relèvent pas de la catégorie des biens de retour.</ANA>
<ANA ID="9B"> 44-008-02-01 Les dispositions des articles L. 229-7 et L. 229-15 du code de l'environnement établissent un mécanisme d'attribution par l'Etat de quotas d'émission de dioxyde de carbone aux exploitants des installations concernées ainsi qu'un système d'échange des quotas d'émission excédentaires et impliquent nécessairement que, dans le cadre d'une concession de service public et quelles que soient les clauses du contrat sur ce point, les quotas appartiennent à l'exploitant concessionnaire auquel ils ont été attribués. Par suite, les quotas excédentaires d'émission de gaz à effet de serre délivrés à un délégataire de service public exploitant une installation autorisée à émettre ces gaz ne relèvent pas de la catégorie des biens de retour.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Assemblée, 21 décembre 2012, Commune de Douai, n°342788, p. 477.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
