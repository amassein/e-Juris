<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043240942</ID>
<ANCIEN_ID>JG_L_2021_02_000000447326</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/24/09/CETATEXT000043240942.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème - 10ème chambres réunies, 24/02/2021, 447326</TITRE>
<DATE_DEC>2021-02-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>447326</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème - 10ème chambres réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>M. Nicolas Agnoux</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Céline  Guibé</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:447326.20210224</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par un jugement n° 1802640 du 3 décembre 2020, enregistré le 7 décembre 2020 au secrétariat du contentieux du Conseil d'Etat, le tribunal administratif de Dijon, avant de statuer sur la demande de la société Thevenin et Ducrot Distribution tendant à l'annulation de la décision du ministre d'Etat, ministre de la transition écologique et solidaire en date du 28 juin 2018 prononçant le retrait de ses certificats d'économie d'énergie, a décidé, par application des dispositions de l'article L. 113-1 du code de justice administrative, de transmettre le dossier de cette demande au Conseil d'Etat, en soumettant à son examen la question de savoir si, dans l'hypothèse où des certificats ont été obtenus par fraude de leur premier détenteur, l'administration peut se fonder sur cette fraude pour prononcer le retrait du volume correspondant inscrit sur le compte de la société détentrice de ces certificats, alors même qu'aucun élément ne permet de considérer que cette dernière était en mesure d'en connaître le caractère frauduleux lors de leur inscription sur son compte et, en cas de réponse positive à la question précédente, s'il y a lieu d'élargir les conditions d'application de la solution dégagée par la décision du Conseil d'Etat nos 407149, 407198 du 5 février 2018 et de considérer que, lorsque l'administration constate que les certificats d'économie d'énergie inscrits au compte d'une société ont été obtenus par fraude de leur détenteur initial, elle doit apprécier, sous le contrôle restreint du juge, l'opportunité de procéder ou non au "retrait" de ces certificats du compte de cette société, compte tenu notamment de la gravité de la fraude et des atteintes aux divers intérêts publics ou privés en présence susceptibles de résulter soit du maintien de ces certificats soit de leur retrait.<br/>
<br/>
              Des observations, enregistrées le 2 février 2021, ont été présentées par la ministre de la transition écologique.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'énergie ;<br/>
              - le code des relations entre le public et l'administration ;<br/>
              - le code de justice administrative, notamment son article L. 113-1, et le décret n° 2020-1406 du 18 novembre 2020 ; <br/>
<br/>
              Vu les notes en délibéré, enregistrées les 5 et 10 février 2021, présentées par la ministre de la transition écologique ;<br/>
<br/>
<br/>
<br/>
               Après avoir entendu en séance publique :<br/>
<br/>
- le rapport de M. Nicolas Agnoux, maître des requêtes, <br/>
- les conclusions de Mme B... A..., rapporteure publique ;<br/>
              - La parole ayant été donnée, après les conclusions, à la SCP Foussard, Froger, avocat de la société Thevenin et Ducrot Distribution ;<br/>
    REND L'AVIS SUIVANT :<br/>
<br/>
<br/>1. D'une part, l'article L. 221-1 du code de l'énergie dispose que les personnes soumises à des obligations d'économies d'énergie peuvent se libérer de ces obligations soit en réalisant, directement ou indirectement, des économies d'énergie, soit en acquérant des certificats d'économies d'énergie. Aux termes de l'article L. 221-7 du même code : " Le ministre chargé de l'énergie ou, en son nom, un organisme habilité à cet effet peut délivrer des certificats d'économies d'énergie aux personnes éligibles lorsque leur action, additionnelle par rapport à leur activité habituelle, permet la réalisation d'économies d'énergie sur le territoire national d'un volume supérieur à un seuil fixé par arrêté du ministre chargé de l'énergie (...) ". Aux termes de l'article L. 221-8 du même code : " Les certificats d'économies d'énergie sont des biens meubles négociables, dont l'unité de compte est le kilowattheure d'énergie finale économisé. Ils peuvent être détenus, acquis ou cédés par toute personne mentionnée aux 1° à 6° de l'article L. 221-7 ou par toute autre personne morale (...) ". Selon l'article L. 221-10 du même code : " Les certificats d'économies d'énergie sont exclusivement matérialisés par leur inscription au registre national des certificats d'économies d'énergie, accessible au public et destiné à tenir la comptabilité des certificats obtenus, acquis ou restitués à l'Etat (...) ". L'article R. 221-22 précise que " La demande de certificats d'économies d'énergie est adressée au ministre chargé de l'énergie. / (...) Tout demandeur de certificats d'économies d'énergie détient un compte auprès du registre national des certificats d'économies d'énergie (...) ". Enfin, aux termes de l'article R. 221-26 : " L'Etat peut, en application de l'article L. 221-10, charger un délégataire de la mission consistant à mettre en place et à tenir un registre national des certificats d'économies d'énergie, sur lequel sont consignées de manière informatisée et sécurisée toutes les opérations de délivrance, d'annulation ou de transaction portant sur des certificats d'économies d'énergie./ Cette mission comprend :/ (...) 2° L'enregistrement de toutes les opérations correspondant à ces comptes (...) :/ c) L'annulation, sur instruction du ministre chargé de l'énergie, des certificats d'économies d'énergie figurant sur un compte (...) ". <br/>
<br/>
              2. L'article L. 222-1 du code de l'énergie dispose que : " Dans les conditions définies aux articles suivants, le ministre chargé de l'énergie peut sanctionner les manquements aux dispositions du chapitre Ier du présent titre ou aux dispositions réglementaires prises pour leur application. " Aux termes de l'article L. 222-2 du même code, dans sa rédaction applicable au litige : " Le ministre met l'intéressé en demeure de se conformer à ses obligations dans un délai déterminé. (...) / Lorsque l'intéressé ne se conforme pas dans les délais fixés à cette mise en demeure, le ministre chargé de l'énergie peut : / 1° Prononcer à son encontre une sanction pécuniaire (...) ; / 2° Le priver de la possibilité d'obtenir des certificats d'économies d'énergie (...) ; / 3° Annuler des certificats d'économies d'énergie de l'intéressé, d'un volume égal à celui concerné par le manquement ; / 4° Suspendre ou rejeter les demandes de certificats d'économies d'énergie faites par l'intéressé ". Enfin, aux termes de l'article L. 222-8 du même code : " Le fait de se faire délivrer indûment, par quelque moyen frauduleux que ce soit, un certificat d'économies d'énergie est puni des peines prévues aux articles 441-6 et 441-10 du code pénal (...)./ Les peines encourues par les personnes morales responsables de l'infraction définie au présent article sont celles prévues à l'article 441-12 du code pénal ".<br/>
<br/>
              3. D'autre part, selon l'article L. 241-2 du code des relations entre le public et l'administration, " Par dérogation aux dispositions du présent titre, un acte administratif unilatéral obtenu par fraude peut être à tout moment abrogé ou retiré ". <br/>
<br/>
              4. Il résulte des dispositions du code de l'énergie citées aux points 1 et 2 ci-dessus qu'en définissant, aux articles L. 222-2 et L. 222-8 du code de l'énergie, les sanctions administratives et pénales auxquelles s'expose l'auteur d'un manquement aux dispositions législatives et règlementaires relatives aux certificats d'économies d'énergie, le législateur a déterminé l'ensemble des conséquences légales susceptibles d'être tirées d'un tel manquement.<br/>
<br/>
              5. Par suite, lorsque le ministre chargé de l'énergie établit que des certificats d'économies d'énergie ont été obtenus de manière frauduleuse par leur premier détenteur, il peut prononcer à l'encontre de celui-ci, dans les conditions et selon la procédure prévues au code de l'énergie, les sanctions mentionnées à l'article L. 222-2 de ce code et notamment, en application du 3° de cet article, l'annulation des certificats d'économie d'énergie qu'il détient, pour un volume égal à celui concerné par la fraude. Mais ces dispositions particulières font obstacle à ce que le ministre puisse, indépendamment de leur mise en oeuvre, prononcer le retrait de la décision d'octroi des certificats sur le fondement des dispositions générales de l'article L. 241-2 du code des relations entre le public et l'administration citées au point 3 et à ce qu'il procède à l'annulation de ces certificats en conséquence de ce retrait. <br/>
<br/>
              6. Il s'ensuit qu'en l'absence de toute disposition du code de l'énergie l'y habilitant, le ministre chargé de l'énergie ne peut, dans l'hypothèse où des certificats d'économie d'énergie acquis de manière frauduleuse par leur premier détenteur ont été cédés à un tiers, faire procéder à l'annulation des certificats litigieux dans le compte du nouveau détenteur.<br/>
<br/>
              7. Compte tenu de la réponse apportée à la première question soulevée par le tribunal, la seconde question est privée d'objet.<br/>
<br/>
<br/>
<br/>
<br/>Le présent avis sera notifié au tribunal administratif de Dijon, à la société Thévenin et Ducrot Distribution et à la ministre de la transition écologique.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">29-06 ENERGIE. MARCHÉ DE L'ÉNERGIE. - CERTIFICATS D'ÉCONOMIES D'ÉNERGIE - 1) RÈGLES PARTICULIÈRES DE RÉPRESSION PRÉVUES AU CODE DE L'ÉNERGIE - A) CARACTÈRE EXCLUSIF - EXISTENCE - B) POSSIBILITÉ DE SANCTIONNER LE PREMIER DÉTENTEUR POUR FRAUDE - EXISTENCE, NOTAMMENT EN PRONONÇANT L'ANNULATION DES CERTIFICATS POUR UN MÊME VOLUME QUE CELUI CONCERNÉ PAR LA FRAUDE - 2) APPLICABILITÉ DES RÈGLES GÉNÉRALES DE RETRAIT DES DÉCISIONS OBTENUES PAR FRAUDE (ART. L. 241-2 DU CRPA) - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">44-05 NATURE ET ENVIRONNEMENT. DIVERS RÉGIMES PROTECTEURS DE L`ENVIRONNEMENT. - CERTIFICATS D'ÉCONOMIES D'ÉNERGIE - 1) RÈGLES PARTICULIÈRES DE RÉPRESSION PRÉVUES AU CODE DE L'ÉNERGIE - A) CARACTÈRE EXCLUSIF - EXISTENCE - B) POSSIBILITÉ DE SANCTIONNER LE PREMIER DÉTENTEUR POUR FRAUDE - EXISTENCE, NOTAMMENT EN PRONONÇANT L'ANNULATION DES CERTIFICATS POUR UN MÊME VOLUME QUE CELUI CONCERNÉ PAR LA FRAUDE - 2) APPLICABILITÉ DES RÈGLES GÉNÉRALES DE RETRAIT DES DÉCISIONS OBTENUES PAR FRAUDE (ART. L. 241-2 DU CRPA) - ABSENCE.
</SCT>
<ANA ID="9A"> 29-06 1) a) En définissant, aux articles L. 222-2 et L. 222-8 du code de l'énergie, les sanctions administratives et pénales auxquelles s'expose l'auteur d'un manquement aux dispositions législatives et règlementaires relatives aux certificats d'économies d'énergie, le législateur a déterminé l'ensemble des conséquences légales susceptibles d'être tirées d'un tel manquement.,,,b) Lorsque le ministre chargé de l'énergie établit que des certificats d'économies d'énergie ont été obtenus de manière frauduleuse par leur premier détenteur, il peut prononcer à l'encontre de celui-ci, dans les conditions et selon la procédure prévues au code de l'énergie, les sanctions mentionnées à l'article L. 222-2 de ce code.,,,Il peut notamment, en application du 3° de cet article, prononcer l'annulation des certificats d'économie d'énergie de ce premier détenteur, pour un volume égal à celui concerné par la fraude.,,,2) Ces dispositions particulières font obstacle à ce que le ministre puisse, indépendamment de leur mise en oeuvre, prononcer le retrait de la décision d'octroi des certificats sur le fondement des dispositions générales de l'article L. 241-2 du code des relations entre le public et l'administration (CRPA) et à ce qu'il procède à l'annulation de ces certificats en conséquence de ce retrait.,,,Par suite, en l'absence de toute disposition du code de l'énergie l'y habilitant, le ministre chargé de l'énergie ne peut, dans l'hypothèse où des certificats d'économie d'énergie acquis de manière frauduleuse par leur premier détenteur ont été cédés à un tiers, faire procéder à l'annulation des certificats litigieux dans le compte du nouveau détenteur.</ANA>
<ANA ID="9B"> 44-05 1) a) En définissant, aux articles L. 222-2 et L. 222-8 du code de l'énergie, les sanctions administratives et pénales auxquelles s'expose l'auteur d'un manquement aux dispositions législatives et règlementaires relatives aux certificats d'économies d'énergie, le législateur a déterminé l'ensemble des conséquences légales susceptibles d'être tirées d'un tel manquement.,,,b) Lorsque le ministre chargé de l'énergie établit que des certificats d'économies d'énergie ont été obtenus de manière frauduleuse par leur premier détenteur, il peut prononcer à l'encontre de celui-ci, dans les conditions et selon la procédure prévues au code de l'énergie, les sanctions mentionnées à l'article L. 222-2 de ce code.,,,Il peut notamment, en application du 3° de cet article, prononcer l'annulation des certificats d'économie d'énergie de ce premier détenteur, pour un volume égal à celui concerné par la fraude.,,,2) Ces dispositions particulières font obstacle à ce que le ministre puisse, indépendamment de leur mise en oeuvre, prononcer le retrait de la décision d'octroi des certificats sur le fondement des dispositions générales de l'article L. 241-2 du code des relations entre le public et l'administration (CRPA) et à ce qu'il procède à l'annulation de ces certificats en conséquence de ce retrait.,,,Par suite, en l'absence de toute disposition du code de l'énergie l'y habilitant, le ministre chargé de l'énergie ne peut, dans l'hypothèse où des certificats d'économie d'énergie acquis de manière frauduleuse par leur premier détenteur ont été cédés à un tiers, faire procéder à l'annulation des certificats litigieux dans le compte du nouveau détenteur.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
