<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026768137</ID>
<ANCIEN_ID>JG_L_2012_12_000000353496</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/76/81/CETATEXT000026768137.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 5ème SSR, 12/12/2012, 353496</TITRE>
<DATE_DEC>2012-12-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>353496</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 5ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Esther de Moustier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Gaëlle Dumortier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2012:353496.20121212</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu l'ordonnance du 11 octobre 2011, enregistrée le 20 octobre 2011 au secrétariat du contentieux du Conseil d'Etat, par laquelle le président du tribunal administratif de Bordeaux a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, la requête présentée à ce tribunal par la société Davalex ;<br/>
<br/>
              Vu la requête, enregistrée le 9 septembre 2011 au greffe du tribunal administratif de Bordeaux et le mémoire complémentaire, enregistré le 13 janvier 2012 au secrétariat du contentieux du Conseil d'Etat, présentés par la société Davalex, dont le siège est situé au lieu dit Les Tabernottes, au Poteau d'Yvrac (33370), représentée par son président ; la société demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 28 juillet 2011 par laquelle la Commission nationale d'aménagement commercial a accordé à la société Distribution Casino France l'autorisation préalable requise en vue de la création d'un supermarché à l'enseigne " Casino Supermarché " d'une surface de vente de 2 000 m², à Montussan (Gironde) ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat et de la société Distribution Casino France la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu les notes en délibéré, enregistrées le 22 novembre 2012, présentées par la société Distribution Casino France ;<br/>
<br/>
              Vu le code de commerce ;<br/>
<br/>
              Vu le code de l'urbanisme ;<br/>
<br/>
              Vu la loi n° 73-1193 du 27 décembre 1973 ; <br/>
<br/>
              Vu la loi n° 2010-788 du 12 juillet 2010 ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Esther de Moustier, Auditeur, <br/>
<br/>
              - les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
<br/>
<br/>
<br/>Sur la légalité de la décision de la Commission nationale d'aménagement commercial :<br/>
<br/>
              En ce qui concerne la procédure suivie devant la commission nationale :<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier que la demande d'autorisation dont la commission nationale était saisie par la société Distribution Casino France, en vue d'être autorisée à créer un supermarché à Montussan (Gironde), comportait des indications relatives à la zone de chalandise du projet et à l'impact du projet sur les flux de circulation ainsi qu'une description de la desserte du projet par les transports en commun ; que ces informations étaient suffisamment précises pour permettre à la commission nationale de statuer en connaissance de cause ; qu'ainsi le moyen tiré d'une insuffisance du dossier de demande d'autorisation doit être écarté ; <br/>
<br/>
              En ce qui concerne le respect des critères de l'article L. 752-6 du code de commerce :<br/>
<br/>
              2. Considérant qu'il appartient aux commissions d'aménagement commercial, lorsqu'elles se prononcent sur un projet d'exploitation commerciale soumis à autorisation en application de l'article L. 752-1 du code de commerce, d'apprécier la conformité de ce projet aux objectifs prévus à l'article 1er de la loi du 27 décembre 1973 et à l'article L. 750-1 du code de commerce, au vu des critères d'évaluation mentionnés à l'article L. 752-6 du même code ; que l'autorisation ne peut être refusée que si, eu égard à ses effets, le projet compromet la réalisation de ces objectifs ;<br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier que le projet contesté, qui n'est pas implanté sur des terrains à vocation agricole ou viticole, permettra de compléter l'offre de produits alimentaires proposée dans la zone de chalandise et sera notamment complémentaire de celle des magasins du centre bourg de Montussan ; que si le site d'implantation du projet est situé à 1,8 kilomètres du centre bourg, il se trouve à proximité immédiate d'un important axe routier ainsi que d'habitations individuelles et d'entreprises ; que l'impact du projet sur les conditions de circulation automobile aux abords du site d'implantation sera limité et que le site est desservi par une ligne de bus du réseau " Transgironde " ; que le projet sera doté d'un système de climatisation permettant d'optimiser la consommation d'énergie, de dispositifs permettant de limiter les différentes formes de pollution, d'assurer un traitement adéquat des déchets ainsi que des eaux de ruissèlement et s'insérera de façon satisfaisante dans le paysage environnant ; qu'il ne résulte d'aucune disposition législative ou règlementaire que des panneaux photovoltaïques doivent nécessairement être installés ; que par suite, et contrairement à ce que soutient la société Davalex, la Commission nationale d'aménagement commercial n'a pas fait une inexacte application des dispositions de l'article L. 752-6 du code de commerce en accordant l'autorisation contestée ;<br/>
<br/>
              En ce qui concerne la compatibilité du projet avec le schéma directeur de l'aire métropolitaine bordelaise valant schéma de cohérence territoriale :<br/>
<br/>
              4. Considérant qu'aux termes de l'article L. 122-1 du code de l'urbanisme, en vigueur jusqu'à son abrogation dans les conditions définies par l'article 17 de la loi du 12 juillet 2010 : " Les schémas de cohérence territoriale (...) présentent le projet d'aménagement et de développement durable retenu, qui fixe les objectifs des politiques publiques d'urbanisme en matière d'habitat, de développement économique, de loisirs, de déplacements des personnes et des marchandises, de stationnement des véhicules et de régulation du trafic automobile. / Pour mettre en oeuvre le projet d'aménagement et de développement durable retenu, ils fixent, dans le respect des équilibres résultant des principes énoncés aux articles L. 110 et L. 121-1, les orientations générales de l'organisation de l'espace et de la restructuration des espaces urbanisés et déterminent les grands équilibres entre les espaces urbains et à urbaniser et les espaces naturels et agricoles ou forestiers. Ils apprécient les incidences prévisibles de ces orientations sur l'environnement. / A ce titre, ils définissent notamment les objectifs relatifs (...) à l'équipement commercial et artisanal, aux localisations préférentielles des commerces, (...) / Ils peuvent comprendre un document d'aménagement commercial défini dans les conditions prévues au II de l'article L. 752-1 du code de commerce. (...) " ; que selon cette disposition : " Les schémas prévus au chapitre II du titre II du livre Ier du code de l'urbanisme peuvent définir des zones d'aménagement commercial. / Ces zones sont définies en considération des exigences d'aménagement du territoire, de protection de l'environnement ou de qualité de l'urbanisme spécifiques à certaines parties du territoire couvert par le schéma. Leur délimitation ne peut reposer sur l'analyse de l'offre commerciale existante ni sur une mesure de l'impact sur cette dernière de nouveaux projets de commerces. / La définition des zones figure dans un document d'aménagement commercial qui est intégré au schéma de cohérence territoriale par délibération de l'établissement public prévu à l'article L. 122-4 du code de l'urbanisme. (...) " ; <br/>
<br/>
              5. Considérant qu'aux termes de l'article L. 122-18 du code de l'urbanisme : " Les schémas directeurs approuvés avant l'entrée en vigueur de la loi n° 2000-1208 du 13 décembre 2000 relative à la solidarité et au renouvellement urbains sont soumis au régime juridique des schémas de cohérence territoriale (...). Ils demeurent.applicables jusqu'à leur prochaine révision et ont les mêmes effets que les schémas de cohérence territoriale) (... " ; qu'il résulte de ces dispositions que le schéma directeur de l'aire métropolitaine bordelaise, adopté le 26 juillet 2001 et toujours en vigueur à la date de la décision attaquée, doit être regardé comme ayant la valeur d'un schéma de cohérence territoriale régi par les dispositions de l'article L. 122-1 du code de l'urbanisme ;<br/>
<br/>
              6. Considérant qu'il résulte des dispositions de l'article L. 122-1 du code de l'urbanisme qu'à l'exception des cas limitativement prévus par la loi dans lesquels les schémas de cohérence territoriale peuvent contenir des normes prescriptives, ceux-ci, avec lesquels les autorisations délivrées par les commissions d'aménagement commercial doivent être compatibles en vertu de ce même article, doivent se borner à fixer des orientations et des objectifs ; qu'en matière d'aménagement commercial, s'il ne leur appartient pas, sous réserve des dispositions applicables aux zones d'aménagement commercial, d'interdire par des dispositions impératives certaines opérations de création ou d'extension relevant des qualifications et procédures prévues au titre V du livre VII du code de commerce, ils peuvent fixer des orientations générales et des objectifs d'implantations préférentielles des activités commerciales, définis en considération des exigences d'aménagement du territoire, de protection de l'environnement ou de qualité de l'urbanisme ; que si de tels objectifs peuvent être pour partie exprimés sous forme quantitative, il appartient aux commissions d'aménagement commercial non de vérifier la conformité des projets d'exploitation commerciale qui leur sont soumis aux énonciations des schémas de cohérence territoriale mais d'apprécier la compatibilité de ces projets avec les orientations générales et les objectifs qu'ils définissent ; <br/>
<br/>
              7. Considérant que si le schéma directeur de l'aire métropolitaine bordelaise valant schéma de cohérence territoriale comporte une disposition qui prévoit " l'arrêt de toute implantation nouvelle de supermarchés de plus de 1 000 m² de surface alimentaire et galeries marchandes créés ex-nihilo " - disposition qui ne saurait être regardée comme impérative - il prévoit également " le développement des commerces de proximité " dans les zones périphériques " pour assurer un équilibre des services commerciaux par secteur " ; que si le projet contesté vise à créer un supermarché d'une surface de 2 000 m² dans la commune de Montussan, la seule circonstance que la surface de vente dépasse le seuil de 1 000 m² mentionné par le schéma directeur n'implique pas qu'il doive être regardé comme incompatible avec ce schéma ; qu'il ressort des pièces du dossier, notamment de l'avis donné par le président du Syndicat du schéma directeur de l'agglomération bordelaise, que cette commune se trouve dans une zone périphérique caractérisée par un déficit de grandes surfaces qui entraine un report des consommateurs vers des grandes surfaces plus éloignées, de nature à justifier qu'y soit implanté un supermarché d'une surface de 2 000 m² afin d'améliorer l'offre de commerce de proximité et de rééquilibrer ainsi les services commerciaux ; que, dès lors, le moyen tiré de ce que le projet contesté serait incompatible avec le schéma directeur valant schéma de cohérence territoriale doit être écarté ;<br/>
<br/>
              8. Considérant qu'il résulte de tout ce qui précède que la société Davalex n'est pas fondée à demander l'annulation de la décision attaquée ; <br/>
<br/>
              Sur les conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              9. Considérant que ces dispositions font obstacle à ce qu'il soit fait droit à la demande présentée à ce titre par la société Davalex ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à ce titre à la charge de la société Davalex la somme de 3 000 euros à verser à la société Distribution Casino France au titre de ces dispositions ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la société Davalex est rejetée.<br/>
Article 2 : La société Davalex versera à la société Distribution Casino France la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la société Davalex, à la société Distribution Casino France et à la Commission nationale d'aménagement commercial.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">14-02-01-05-03 COMMERCE, INDUSTRIE, INTERVENTION ÉCONOMIQUE DE LA PUISSANCE PUBLIQUE. RÉGLEMENTATION DES ACTIVITÉS ÉCONOMIQUES. ACTIVITÉS SOUMISES À RÉGLEMENTATION. AMÉNAGEMENT COMMERCIAL. RÈGLES DE FOND. - OBLIGATION DE COMPATIBILITÉ DES AUTORISATIONS D'AMÉNAGEMENT COMMERCIAL AVEC LES ORIENTATIONS ET OBJECTIFS ÉNONCÉS PAR LES SCOT - EXISTENCE - OBLIGATION DE CONFORMITÉ AUX OBJECTIFS EXPRIMÉS SOUS FORME QUANTITATIVE DANS LES SCOT - ABSENCE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">68-01-006-02 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PLANS D'AMÉNAGEMENT ET D'URBANISME. SCHÉMAS DE COHÉRENCE TERRITORIALE. EFFETS. - OBLIGATION DE COMPATIBILITÉ DES AUTORISATIONS D'AMÉNAGEMENT COMMERCIAL AVEC LES ORIENTATIONS ET OBJECTIFS ÉNONCÉS PAR LES SCOT - EXISTENCE - OBLIGATION DE CONFORMITÉ AUX OBJECTIFS EXPRIMÉS SOUS FORME QUANTITATIVE DANS LES SCOT - ABSENCE [RJ1].
</SCT>
<ANA ID="9A"> 14-02-01-05-03 Il résulte des dispositions de l'article L. 122-1 du code de l'urbanisme qu'à l'exception des cas limitativement prévus par la loi dans lesquels les schémas de cohérence territoriale (SCOT) peuvent contenir des normes prescriptives, ceux-ci, avec lesquels les autorisations délivrées par les commissions d'aménagement commercial doivent être compatibles en vertu de ce même article, doivent se borner à fixer des orientations et des objectifs. En matière d'aménagement commercial, s'il ne leur appartient pas, sous réserve des dispositions applicables aux zones d'aménagement commercial, d'interdire par des dispositions impératives certaines opérations de création ou d'extension relevant des qualifications et procédures prévues au titre V du livre VII du code de commerce, ils peuvent fixer des orientations générales et des objectifs d'implantations préférentielles des activités commerciales, définis en considération des exigences d'aménagement du territoire, de protection de l'environnement ou de qualité de l'urbanisme. Si de tels objectifs peuvent être pour partie exprimés sous forme quantitative, il appartient aux commissions d'aménagement commercial non de vérifier la conformité des projets d'exploitation commerciale qui leur sont soumis aux énonciations des SCOT mais d'apprécier la compatibilité de ces projets avec les orientations générales et les objectifs qu'ils définissent.</ANA>
<ANA ID="9B"> 68-01-006-02 Il résulte des dispositions de l'article L. 122-1 du code de l'urbanisme qu'à l'exception des cas limitativement prévus par la loi dans lesquels les schémas de cohérence territoriale (SCOT) peuvent contenir des normes prescriptives, ceux-ci, avec lesquels les autorisations délivrées par les commissions d'aménagement commercial doivent être compatibles en vertu de ce même article, doivent se borner à fixer des orientations et des objectifs. En matière d'aménagement commercial, s'il ne leur appartient pas, sous réserve des dispositions applicables aux zones d'aménagement commercial, d'interdire par des dispositions impératives certaines opérations de création ou d'extension relevant des qualifications et procédures prévues au titre V du livre VII du code de commerce, ils peuvent fixer des orientations générales et des objectifs d'implantations préférentielles des activités commerciales, définis en considération des exigences d'aménagement du territoire, de protection de l'environnement ou de qualité de l'urbanisme. Si de tels objectifs peuvent être pour partie exprimés sous forme quantitative, il appartient aux commissions d'aménagement commercial non de vérifier la conformité des projets d'exploitation commerciale qui leur sont soumis aux énonciations des SCOT mais d'apprécier la compatibilité de ces projets avec les orientations générales et les objectifs qu'ils définissent.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 11 juillet 2012, SAS Sodigor, n° 353880, inédite au Recueil.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
