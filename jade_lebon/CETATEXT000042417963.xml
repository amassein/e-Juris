<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042417963</ID>
<ANCIEN_ID>JG_L_2020_10_000000425459</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/41/79/CETATEXT000042417963.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 1ère chambres réunies, 09/10/2020, 425459</TITRE>
<DATE_DEC>2020-10-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>425459</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MATUCHANSKY, POUPOT, VALDELIEVRE ; SCP FABIANI, LUC-THALER, PINATEL</AVOCATS>
<RAPPORTEUR>Mme Céline Roux</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Raphaël Chambon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:425459.20201009</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une première décision du 19 juillet 2016, la section disciplinaire de l'université de Poitiers, saisie par le président de l'université, a infligé à M. A..., professeur des universités, la sanction d'interdiction d'exercer toute fonction d'enseignement dans tout établissement public d'enseignement supérieur pendant cinq ans, assortie de la privation de la moitié de son traitement.<br/>
<br/>
              Par une deuxième décision du 16 juin 2017, la section disciplinaire a infligé à M. A... la sanction d'interdiction d'exercer toute fonction d'enseignement dans tout établissement public d'enseignement supérieur pendant trois ans, assortie de la privation de la totalité de son traitement.<br/>
<br/>
              Par une décision du 10 juillet 2018, le Conseil national de l'enseignement supérieur et de la recherche (CNESER), statuant en formation disciplinaire, a rejeté les appels formés par M. A... contre ces deux décisions et, statuant sur les appels incidents formés par l'université de Poitiers, a prononcé la révocation de M. A....<br/>
<br/>
              Par un pourvoi, un nouveau mémoire, un mémoire en réplique et deux autres mémoires enregistrés les 19 novembre 2018, 22 mars, 11 juin, 23 septembre 2019 et 14 septembre 2020 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette décision ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses appels et de rejeter les appels incidents de l'université ;<br/>
<br/>
              3°) de mettre à la charge de l'université de Poitiers la somme de 3 000 euros à verser à la SCP Matuchansky, Poupot, Valdelievre, son avocat, au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'éducation ;<br/>
              - la loi n° 78-17 du 6 janvier 1978 ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 23 septembre 2020, présentée par l'université de Poitiers ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 28 septembre 2020, présentée par M. A... ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Céline Roux, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Raphaël Chambon, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Matuchansky, Poupot, Valdelièvre, avocat de M. A... et à la SCP Fabiani, Luc-Thaler, Pinatel, avocat de l'université de Poitiers ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que le président de l'université de Poitiers a engagé le 30 mars 2016 des poursuites disciplinaires contre M. A..., professeur des universités. Par une décision du 19 juillet 2016, la section disciplinaire de l'université a infligé à M. A... la sanction d'interdiction d'exercer toute fonction d'enseignement dans tout établissement public d'enseignement supérieur pendant une durée de cinq ans, assortie de la privation de la moitié de son traitement, à raison de différents faits commis au cours des années 2014, 2015 et 2016. Ultérieurement, le président de l'université a engagé de nouvelles poursuites disciplinaires contre M. A... devant la section disciplinaire, à raison de faits commis postérieurement à la décision du 19 juillet 2016. Par une seconde décision du 16 juillet 2017, la section disciplinaire a infligé à l'intéressé la sanction d'interdiction d'exercer toute fonction d'enseignement dans tout établissement public d'enseignement supérieur pendant trois ans, assortie de la privation de la totalité de son traitement. Par une décision du 10 juillet 2018, contre laquelle M. A... se pourvoit en cassation, le Conseil national de l'enseignement supérieur et de la recherche (CNESER), joignant les appels formés par M. A... contre ces deux décisions, a rejeté ces appels et, statuant sur les appels incidents de l'université de Poitiers, a prononcé à l'encontre de M. A... la sanction de la révocation.<br/>
<br/>
              Sur l'instruction des affaires en appel :<br/>
<br/>
              2. Aux termes de l'article R. 232-36 du code de l'éducation : " Le président du Conseil national de l'enseignement supérieur et de la recherche statuant en matière disciplinaire désigne pour chaque affaire une commission d'instruction (...) ". Aux termes de l'article R. 232-37 du même code : " La commission d'instruction entend la personne déférée et instruit l'affaire par tous les moyens qu'elle juge propres à l'éclairer et en fait un rapport écrit comprenant l'exposé des faits et moyens des parties. Ce rapport est transmis au président dans un délai qu'il a préalablement fixé et qui ne peut être supérieur à trois mois. Toutefois, le président peut ordonner un supplément d'instruction s'il estime que l'affaire n'est pas en état d'être jugée. (...) / Dans le cas où la juridiction est saisie de nouveaux éléments, le président ordonne la réouverture de l'instruction qui se déroule selon les formes prescrites à l'alinéa précédent du présent article ". Il ressort des pièces du dossier des juges du fond que, d'une part, l'appel et l'appel incident formés contre la décision du 19 juillet 2016 de la section disciplinaire de l'université de Poitiers et, d'autre part, l'appel et l'appel incident formés contre la décision du 16 juillet 2017 de cette même section disciplinaire ont été respectivement confiés à deux commissions d'instruction qui ont établi des rapports distincts sur chacune des deux affaires.<br/>
<br/>
              3. En premier lieu, le délai maximal de trois mois au terme duquel le rapport écrit de la commission d'instruction doit, en principe, être transmis au président du CNESER n'est pas prescrit à peine d'irrégularité de la procédure. Par suite, M. A... n'est, en tout état de cause, pas fondé à soutenir que la procédure serait irrégulière au motif que les pièces du dossier ne permettraient pas de vérifier que la durée de l'instruction n'a pas excédé ce délai. <br/>
<br/>
              4. En deuxième lieu, en désignant une commission à la composition identique pour l'instruction des deux appels de M. A..., circonstance qui n'impliquait par elle-même aucun préjugement des affaires, le président du CNESER n'a méconnu ni le principe d'impartialité, ni les dispositions citées au point 2. <br/>
<br/>
              5. En troisième lieu, il ressort des pièces du dossier que, dans son rapport, la commission d'instruction chargée d'instruire l'appel et l'appel incident formés contre la décision du 19 juillet 2016 de la section disciplinaire de l'université de Poitiers a examiné les quatre griefs dont la juridiction disciplinaire était saisie dans le cadre de cette procédure, contrairement à ce qui est soutenu. <br/>
<br/>
              6. En quatrième lieu, le CNESER, qui a exposé de manière détaillée les divers faits reprochés à M. A..., et en a examiné la matérialité, n'a pas entaché sa décision d'insuffisance de motivation en se référant à l'instruction. <br/>
<br/>
              Sur la jonction des procédures disciplinaires devant le CNESER :<br/>
<br/>
              7. Rien ne s'oppose à ce que le CNESER, saisi de différentes procédures disciplinaires, en particulier lorsqu'elles sont relatives à des poursuites disciplinaires engagées contre un même enseignant-chercheur, use de la faculté dont il dispose de joindre ces procédures pour statuer par une seule décision se prononçant alors sur l'ensemble du comportement professionnel de l'intéressé, dès lors que chaque affaire a été instruite conformément aux dispositions des articles R. 232-36 et R. 232-37 du code de l'éducation.<br/>
<br/>
              8. Lorsqu'il décide de faire usage de cette faculté de joindre plusieurs affaires, le CNESER n'est pas tenu d'en informer préalablement les parties afin de les mettre en mesure de présenter des observations sur la jonction. Par suite, le moyen tiré de ce que le caractère contradictoire de la procédure aurait été méconnu faute pour le CNESER d'avoir, à l'issue de l'instruction, informé M. A... de la jonction de ses deux appels et des appels incidents de l'université de Poitiers ne peut qu'être écarté.<br/>
<br/>
              9. Par ailleurs, s'il résulte des principes généraux du droit disciplinaire qu'une sanction infligée en première instance par une juridiction disciplinaire ne peut être aggravée par le juge d'appel sur le recours de la personne frappée par la sanction, il résulte du dossier des juges du fond que le CNESER était saisi, pour chacun des appels formés par M. A..., d'appels incidents formés par l'université de Poitiers. Par suite, les moyens tirés de ce que le CNESER aurait statué au-delà de ce dont il était saisi, en méconnaissance des principes du droit disciplinaire, ou méconnu le principe de neutralité de la jonction des requêtes ne peuvent qu'être écartés. <br/>
<br/>
              Sur la décision attaquée en ce qu'elle se prononce sur des moyens dirigés contre la décision du 19 juillet 2016 :<br/>
<br/>
              10. En premier lieu, pour juger que M. A... adoptait habituellement un comportement menaçant et agressif à l'égard des personnels de l'administration de l'université, la section disciplinaire de l'université de Poitiers s'est fondée sur différents faits exposés dans les poursuites et a également relevé que M. A... avait, au cours de la procédure devant elle, porté des accusations infondées contre la secrétaire de la juridiction disciplinaire afin d'exercer une pression sur le fonctionnement de la juridiction disciplinaire. En relevant par des constatations souveraines exemptes de dénaturation, pour écarter le moyen d'appel tiré par M. A... de ce que la référence à ces derniers faits entacherait d'irrégularité la procédure suivie devant la section disciplinaire de l'université, que les faits en cause avaient été constatés et avaient donné lieu à un échange contradictoire au cours de l'instruction, le CNESER n'a ni porté atteinte aux droits de la défense ou au principe d'impartialité, ni entaché sa décision d'erreur de droit.<br/>
<br/>
              11. En second lieu, pour juger que M. A... avait adopté un comportement inapproprié à l'égard de jeunes étudiantes, en particulier sur des réseaux sociaux, la section disciplinaire de l'université de Poitiers s'est fondée sur des témoignages non anonymes ainsi que sur des témoignages d'étudiantes recueillis de façon anonyme par la commission chargée d'instruire la plainte. En jugeant, pour écarter le moyen d'appel tiré par M. A... de l'atteinte aux droits de la défense, que la section disciplinaire avait pu, sans entacher d'irrégularité la procédure suivie devant elle, procéder à l'anonymisation de certains des témoignages produits devant elle par des étudiantes afin de les protéger de risques de représailles, le CNESER n'a entaché sa décision ni d'insuffisance de motivation ni d'erreur de droit, dès lors que l'ensemble des pièces et témoignages ainsi anonymisés ont été soumis au débat contradictoire et que leur teneur était confortée par des éléments non anonymisés versés au dossier.<br/>
<br/>
              Sur la décision attaquée en ce qu'elle se prononce sur des moyens dirigés contre la décision du 16 juin 2017 :<br/>
<br/>
              12. En premier lieu, pour écarter le moyen tiré par M. A... de ce que le principe d'impartialité des juridictions aurait été méconnu devant la section disciplinaire de l'université de Poitiers au motif que la présidente de cette section disciplinaire avait exercé des fonctions d'adjointe aux relations internationales au sein d'une unité de formation et de recherche de l'université, le CNESER s'est fondé sur la circonstance que l'intéressée, laquelle ne pouvait d'ailleurs être regardée à ce titre comme appartenant à l'équipe de direction de l'université, avait démissionné de ces fonctions en décembre 2015, à une date antérieure à l'engagement des poursuites contre M. A.... En statuant ainsi, le CNESER n'a pas entaché sa décision d'erreur de droit.<br/>
<br/>
              13. En second lieu, si M. A... soutenait à l'appui de son appel que la section disciplinaire de l'université avait entaché sa décision d'irrégularité en ne vérifiant pas si le traitement de données à caractère personnel mis en oeuvre par l'université, au cours de l'enquête administrative ayant permis de l'identifier comme étant l'auteur des messages électroniques anonymes ou usant d'une fausse identité adressés à des étudiants ou des collègues après le prononcé de la première sanction disciplinaire, satisfaisait aux exigences de la loi du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés, le CNESER a retenu que la juridiction disciplinaire n'était pas tenue de procéder à une telle vérification pour apprécier la valeur probante des éléments produits devant elle, dès lors que l'université avait été autorisée à les recueillir par une ordonnance du président du tribunal de grande instance de Poitiers. En écartant ainsi la contestation dont il était saisi, le CNESER, dont la décision est suffisamment motivée sur ce point, n'a pas entaché sa décision d'erreur de droit. <br/>
<br/>
              Sur les faits retenus par la décision attaquée et sur la sanction qu'elle inflige :<br/>
<br/>
              14. En premier lieu, en jugeant que M. A... était l'auteur des messages électroniques anonymes ou usurpant l'identité de tiers, adressés en octobre et décembre 2016 à des témoins ou à des membres de l'administration de l'université afin de les intimider et les dissuader de témoigner dans le cadre de l'instruction de la procédure d'appel formé contre la décision du 19 juin 2016, le CNESER a porté sur les faits qui lui étaient soumis une appréciation souveraine exempte de dénaturation. Statuant sur les poursuites disciplinaires dont il était saisi, il ne saurait être regardé comme ayant porté atteinte au principe de la présomption d'innocence.<br/>
<br/>
              15. En deuxième lieu, si le CNESER a rappelé, par une mention qui présente un caractère surabondant, que M. A... avait fait l'objet d'une sanction en 2007 pour avoir exercé des pressions sur certaines de ses étudiantes, il ressort des termes de l'ensemble de la décision attaquée qu'il ne s'est fondé, pour apprécier la sanction à prononcer, que sur les seuls faits ayant motivé l'engagement, par l'université de Poitiers, des deux procédures disciplinaires ayant conduit aux décisions de la section disciplinaire des 19 juillet 2016 et 16 juin 2017. Par suite, le moyen tiré de ce que le CNESER aurait commis une erreur de droit en se fondant sur des faits ayant donné lieu à une précédente sanction effacée de son dossier administratif ne peut, en tout état de cause, qu'être écarté. <br/>
<br/>
              16. En troisième lieu, si le caractère fautif des faits reprochés à un enseignant-chercheur est susceptible de faire l'objet d'un contrôle de qualification juridique de la part du juge de cassation, l'appréciation du caractère proportionné de la sanction au regard de la gravité des fautes commises relève, pour sa part, de l'appréciation des juges du fond et n'est susceptible d'être remise en cause par le juge de cassation que dans le cas où la solution qu'ils ont retenue quant au choix de la sanction est hors de proportion avec les fautes commises.<br/>
<br/>
              17. Il ressort des énonciations de la décision attaquée que le CNESER a jugé que M. A... avait méconnu le devoir d'exemplarité qui s'imposait à lui et fait la preuve de son incapacité à exercer ses fonctions de professeur des universités, en cherchant à entretenir avec des étudiantes, contre leur volonté, des relations intimes et inappropriées, en adoptant un comportement brutal et arbitraire à l'encontre d'étudiants, en adressant des messages électroniques diffamatoires ou insultants à ses collègues, puis, après avoir été sanctionné par la décision du 19 juillet 2016 à raison de ces faits, en tentant d'intimider et de dissuader plusieurs de ses collègues et étudiantes de témoigner dans le cadre de l'instance d'appel, en recourant à des messages anonymes ou adressés sous une fausse identité. En infligeant la sanction de révocation à M. A... à raison de l'ensemble de ces faits fautifs, le CNESER n'a pas prononcé une sanction hors de proportion avec les fautes commises.<br/>
<br/>
              Sur l'exactitude des mentions de la décision attaquée :<br/>
<br/>
              18. Aux termes de l'article R. 232-41 du code de l'éducation : " La décision est prononcée en séance publique. (...) Elle est signée par le président de la séance et par le secrétaire. (...) ". Si M. A... soutient qu'à l'issue de la séance du 10 juillet 2018, le CNESER aurait annoncé lui infliger une autre sanction que la sanction de révocation, les mentions de la décision du 10 juillet 2018, laquelle lui inflige la sanction de la révocation, font foi jusqu'à preuve du contraire, laquelle n'est pas rapportée par M. A....<br/>
<br/>
              19. Il résulte de tout ce qui précède que M. A... n'est pas fondé à demander l'annulation de la décision qu'il attaque. Son pourvoi doit, en conséquence, être rejeté, y compris les conclusions présentées au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de M. A... la somme que l'université de Poitiers demande au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. A... est rejeté.<br/>
Article 2 : Les conclusions de l'université de Poitiers présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à M. B... A... et à l'université de Poitiers.<br/>
Copie en sera adressée à la ministre de l'enseignement supérieur, de la recherche et de l'innovation.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">30-01-01-01-03 ENSEIGNEMENT ET RECHERCHE. QUESTIONS GÉNÉRALES. ORGANISATION SCOLAIRE ET UNIVERSITAIRE. ORGANISMES CONSULTATIFS NATIONAUX. CONSEIL NATIONAL DE L'ENSEIGNEMENT SUPÉRIEUR ET DE LA RECHERCHE. - JONCTION DES REQUÊTES - FACULTÉ OUVERTE AU CNESER SAISI DE DIFFÉRENTES PROCÉDURES DISCIPLINAIRES RELATIVES À DES POURSUITES ENGAGÉES CONTRE UN MÊME ENSEIGNANT-CHERCHEUR - 1) EXISTENCE [RJ1], SANS OBLIGATION PRÉALABLE D'EN INFORMER LES PARTIES - 2) MISE EN OEUVRE - PRONONCÉ D'UNE SANCTION UNIQUE AU REGARD DES FAITS À L'ORIGINE DES DEUX PROCÉDURES DISCIPLINAIRES - MÉCONNAISSANCE DU PRINCIPE DE NEUTRALITÉ DE LA JONCTION [RJ1] - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-04-03 PROCÉDURE. INSTRUCTION. CARACTÈRE CONTRADICTOIRE DE LA PROCÉDURE. - JONCTION DES REQUÊTES - FACULTÉ OUVERTE AU CNESER SAISI DE DIFFÉRENTES PROCÉDURES DISCIPLINAIRES RELATIVES À DES POURSUITES ENGAGÉES CONTRE UN MÊME ENSEIGNANT-CHERCHEUR [RJ1] - OBLIGATION D'EN INFORMER PRÉALABLEMENT LES PARTIES - ABSENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-07-01 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. QUESTIONS GÉNÉRALES. - JONCTION DES REQUÊTES - FACULTÉ OUVERTE AU CNESER SAISI DE DIFFÉRENTES PROCÉDURES DISCIPLINAIRES RELATIVES À DES POURSUITES ENGAGÉES CONTRE UN MÊME ENSEIGNANT-CHERCHEUR - 1) EXISTENCE [RJ1], SANS OBLIGATION PRÉALABLE D'EN INFORMER LES PARTIES - 2) MISE EN OEUVRE - PRONONCÉ D'UNE SANCTION UNIQUE AU REGARD DES FAITS À L'ORIGINE DES DEUX PROCÉDURES DISCIPLINAIRES - MÉCONNAISSANCE DU PRINCIPE DE NEUTRALITÉ DE LA JONCTION [RJ1] - ABSENCE.
</SCT>
<ANA ID="9A"> 30-01-01-01-03 1) Rien ne s'oppose à ce que le Conseil national de l'enseignement supérieur et de la recherche (CNESER), saisi de différentes procédures disciplinaires, en particulier lorsqu'elles sont relatives à des poursuites disciplinaires engagées contre un même enseignant-chercheur, use de la faculté dont il dispose de joindre ces procédures pour statuer par une seule décision se prononçant alors sur l'ensemble du comportement professionnel de l'intéressé, dès lors que chaque affaire a été instruite conformément aux dispositions des articles R. 232-36 et R. 232-37 du code de l'éducation. Lorsqu'il décide de faire usage de cette faculté de joindre plusieurs affaires, le CNESER n'est pas tenu d'en informer préalablement les parties afin de les mettre en mesure de présenter des observations sur la jonction.,,,2) Par suite, le moyen tiré de ce que le CNSER, en substituant aux deux interdictions temporaires d'exercice prononcées par les premiers juges, une sanction unique de révocation, aurait méconnu le principe de neutralité de la jonction des requêtes ne peut qu'être écarté.</ANA>
<ANA ID="9B"> 54-04-03 Rien ne s'oppose à ce que le Conseil national de l'enseignement supérieur et de la recherche (CNESER), saisi de différentes procédures disciplinaires, en particulier lorsqu'elles sont relatives à des poursuites disciplinaires engagées contre un même enseignant-chercheur, use de la faculté dont il dispose de joindre ces procédures pour statuer par une seule décision se prononçant alors sur l'ensemble du comportement professionnel de l'intéressé, dès lors que chaque affaire a été instruite conformément aux dispositions des articles R. 232-36 et R. 232-37 du code de l'éducation.,,,Lorsqu'il décide de faire usage de cette faculté de joindre plusieurs affaires, le CNESER n'est pas tenu d'en informer préalablement les parties afin de les mettre en mesure de présenter des observations sur la jonction.</ANA>
<ANA ID="9C"> 54-07-01 1) Rien ne s'oppose à ce que le Conseil national de l'enseignement supérieur et de la recherche (CNESER), saisi de différentes procédures disciplinaires, en particulier lorsqu'elles sont relatives à des poursuites disciplinaires engagées contre un même enseignant-chercheur, use de la faculté dont il dispose de joindre ces procédures pour statuer par une seule décision se prononçant alors sur l'ensemble du comportement professionnel de l'intéressé, dès lors que chaque affaire a été instruite conformément aux dispositions des articles R. 232-36 et R. 232-37 du code de l'éducation. Lorsqu'il décide de faire usage de cette faculté de joindre plusieurs affaires, le CNESER n'est pas tenu d'en informer préalablement les parties afin de les mettre en mesure de présenter des observations sur la jonction.,,,2) Par suite, le moyen tiré de ce que le CNSER, en substituant aux deux interdictions temporaires d'exercice prononcées par les premiers juges, une sanction unique de révocation, aurait méconnu le principe de neutralité de la jonction des requêtes ne peut qu'être écarté.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., sur la reconnaissance au juge administratif d'une telle faculté, CE, Section, 23 octobre 2015, Ministre délégué auprès du Ministre de l'économie et des finances, chargé du budget c/ M. Chehboun, n°s 370251 373530, p. 359.,,[RJ2] Cf., sur le principe de neutralité de la jonction des requêtes, CE, 28 janvier 1987, Comité de défense des espaces verts, n° 39145, inédite au Recueil ; CE, 27 juillet 2005, Belin, n° 228554, T. pp. 1042-1058-1061.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
