<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027377273</ID>
<ANCIEN_ID>JG_L_2013_04_000000357373</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/37/72/CETATEXT000027377273.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème et 7ème sous-sections réunies, 29/04/2013, 357373</TITRE>
<DATE_DEC>2013-04-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>357373</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème et 7ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Yves Doutriaux</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Damien Botteghi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:357373.20130429</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu l'ordonnance n° 1202949/5-2 du 27 février 2012, enregistrée au secrétariat du contentieux du Conseil d'Etat le 6 mars 2012, par laquelle le président du tribunal administratif de Paris a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, la requête de la Fédération des syndicats solidaires, unitaires et démocratiques des activités postales et de télécommunications (Fédération SUD-PTT) ; <br/>
<br/>
              Vu la requête, enregistrée au greffe du tribunal administratif de Paris le 14 février 2012, présentée par la Fédération des syndicats solidaires, unitaires et démocratiques des activités postales et de télécommunications (Fédération SUD-PTT), dont le siège est au 25-27, rue des Envierges à Paris (75020), représenté par sa secrétaire générale en exercice ; la Fédération des syndicats solidaires, unitaires et démocratiques des activités postales et de télécommunications demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision n° 356-04 du 22 décembre 2011 par laquelle le président directeur général du groupe La Poste a fixé la liste des organisations syndicales représentatives de l'ensemble du personnel habilitées à désigner leurs représentants au sein du conseil d'orientation et de gestion des activités sociales à La Poste ainsi que la répartition des sièges au sein de ce conseil ; <br/>
<br/>
              2°) de mettre à la charge de La Poste le versement de la somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 20 mars 2013, présentée par la Fédération des syndicats solidaires, unitaires et démocratiques des activités postales et de télécommunications ;<br/>
<br/>
              Vu la loi n° 83-634 du 13 juillet 1983 ;<br/>
<br/>
              Vu la loi n° 90-568 du 2 juillet 1990 ;<br/>
<br/>
              Vu la loi n° 2010-123 du 9 février 2010 ;<br/>
<br/>
              Vu la loi n° 2010-751 du 5 juillet 2010 ;<br/>
<br/>
              Vu le décret n° 2011-1063 du 7 septembre 2011 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Yves Doutriaux, Conseiller d'Etat,<br/>
<br/>
              - les conclusions de M. Damien Botteghi, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'aux termes de l'article 33-1 de la loi du 2 juillet 1990 relative à l'organisation du service public de la poste et de France Télécom, modifiée par la loi du 5 juillet 2010 relative à la rénovation du dialogue social et comportant diverses dispositions relatives à la fonction publique : " Il est créé au sein de La Poste un conseil d'orientation et de gestion des activités sociales en charge de définir la politique et d'assurer la gestion et le contrôle des activités sociales relevant de la société./ Chaque conseil d'orientation et de gestion des activités sociales comprend huit représentants désignés par La Poste, huit représentants désignés par les organisations syndicales représentatives, huit représentants désignés par les associations de personnel à caractère national./(...)./ Le président de La Poste ou son représentant est de droit président des conseils d'orientation et de gestion des activités sociales de La Poste.(...)/. La convention constitutive du conseil d'orientation et de gestion est soumise à l'approbation du ministre chargé des postes et fixe les modalités d'application du présent article " ; que l'article 5 de cette convention, dont la modification a été approuvée par un arrêté du 22 novembre 2011 du ministre chargé des postes, stipule que le président du conseil d'administration de La Poste répartit les sièges entre les huit représentants désignés par les organisations syndicales représentatives en tenant compte du nombre de voix obtenues par les organisations syndicales représentatives à l'élection des représentants du personnel au comité technique national ;<br/>
<br/>
              2.	Considérant que le syndicat requérant demande l'annulation de la décision du 22 décembre 2011 par laquelle le président directeur général du groupe La Poste a fixé la liste des organisations syndicales représentatives de l'ensemble du personnel habilitées à désigner leurs représentants au sein du conseil d'orientation et de gestion des activités sociales à La Poste ainsi que la répartition des sièges au sein de ce conseil ; <br/>
<br/>
              3.	Considérant qu'en vertu de l'article 1er de la loi du 9 février 2010 relative à l'entreprise publique La Poste et aux activités postales, qui a modifié la loi du 2 juillet 1990 relative à l'organisation du service public de la poste et à France Télécom, la personne morale de droit public La Poste a été transformée à compter du 1er mars 2010 en une société anonyme dénommée La Poste ; que, si, selon l'article 2 de la même loi, La Poste et ses filiales constituent un groupe public qui remplit des missions de service public et d'intérêt général énumérées par cet article et si les corps de fonctionnaires de La Poste sont rattachés à cette société anonyme, le même article prévoit aussi que ce groupe public assure d'autres activités selon les règles de droit commun tandis que l'article 31 de la loi du 2 juillet 1990 prévoit que La Poste emploie des agents contractuels sous le régime des conventions collectives ; que, selon l'article 33-1 de la loi du 2 juillet 1990 cité ci-dessus, le conseil d'orientation et de gestion des activités sociales (COGAS) définit la politique et assure la gestion et le contrôle des activités sociales relevant de la société dont l'objet, selon le préambule de la convention constitutive COGAS prévue par cet article, est de " contribuer à améliorer le bien-être des personnels " et de " développer leur accompagnement social " ; que ces activités ne relèvent pas de l'organisation du service public ; que, dès lors, le litige soulevé par la requête de la Fédération des syndicats solidaires, unitaires et démocratiques des activités postales et de télécommunications n'est pas au nombre de ceux dont il appartient à la juridiction administrative de connaître ;<br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la  Fédération des syndicats solidaires, unitaires et démocratiques des activités postales et de télécommunications la somme de 3 000 euros à verser à La Poste, au titre des dispositions de l'article L. 761-1 du code de justice administrative ; que ces dispositions font, en revanche, obstacle à ce qu'une somme soit mise à ce titre à la charge de La Poste, qui n'est pas la partie perdante dans la présente instance ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la Fédération des syndicats solidaires, unitaires et démocratiques des activités postales et de télécommunications est rejetée comme portée devant une juridiction incompétente pour en connaître.<br/>
<br/>
Article 2 : La Fédération des syndicats solidaires, unitaires et démocratiques des activités postales et de télécommunications versera à La Poste une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée à la Fédération des syndicats solidaires, unitaires et démocratiques des activités postales et de télécommunications et à La Poste.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">17-03-02-07-02 COMPÉTENCE. RÉPARTITION DES COMPÉTENCES ENTRE LES DEUX ORDRES DE JURIDICTION. COMPÉTENCE DÉTERMINÉE PAR UN CRITÈRE JURISPRUDENTIEL. PROBLÈMES PARTICULIERS POSÉS PAR CERTAINES CATÉGORIES DE SERVICES PUBLICS. SERVICE PUBLIC INDUSTRIEL ET COMMERCIAL. - LA POSTE - DÉTERMINATION DES ORGANISATIONS REPRÉSENTATIVES HABILITÉES À DÉSIGNER DES MEMBRES AU SEIN DU COGAS ET RÉPARTITION DES SIÈGES ENTRE CES ORGANISATIONS - DÉCISION RELEVANT DE L'ORGANISATION DU SERVICE PUBLIC - ABSENCE - CONSÉQUENCE - INCOMPÉTENCE DE LA JURIDICTION ADMINISTRATIVE POUR CONNAÎTRE DE LA REQUÊTE TENDANT À SON ANNULATION [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">51-01-03 POSTES ET COMMUNICATIONS ÉLECTRONIQUES. POSTES. PERSONNEL DE LA POSTE. - COGAS - DÉTERMINATION DES ORGANISATIONS REPRÉSENTATIVES HABILITÉES À DÉSIGNER DES MEMBRES ET RÉPARTITION DES SIÈGES ENTRE CES ORGANISATIONS  - DÉCISION RELEVANT DE L'ORGANISATION DU SERVICE PUBLIC - ABSENCE - CONSÉQUENCE - INCOMPÉTENCE DE LA JURIDICTION ADMINISTRATIVE POUR CONNAÎTRE DE LA REQUÊTE TENDANT À SON ANNULATION [RJ1].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">66-05-01 TRAVAIL ET EMPLOI. SYNDICATS. REPRÉSENTATIVITÉ. - LA POSTE - DÉTERMINATION DES ORGANISATIONS REPRÉSENTATIVES HABILITÉES À DÉSIGNER DES MEMBRES AU SEIN DU COGAS ET RÉPARTITION DES SIÈGES ENTRE CES ORGANISATIONS - DÉCISION RELEVANT DE L'ORGANISATION DU SERVICE PUBLIC - ABSENCE - CONSÉQUENCE - INCOMPÉTENCE DE LA JURIDICTION ADMINISTRATIVE POUR CONNAÎTRE DE LA REQUÊTE TENDANT À SON ANNULATION [RJ1].
</SCT>
<ANA ID="9A"> 17-03-02-07-02 Le conseil d'orientation et de gestion des activités sociales (COGAS) de La Poste définit la politique et assure la gestion et le contrôle des activités sociales relevant de La Poste dont l'objet, selon le préambule de la convention constitutive du COGAS prévue par cet article, est de  contribuer à améliorer le bien-être des personnels  et de  développer leur accompagnement social . Ces activités ne relèvent pas de l'organisation du service public. Dès lors, le litige relatif à l'annulation de la décision par laquelle le président-directeur général du groupe La Poste a fixé la liste des organisations syndicales représentatives de l'ensemble du personnel habilitées à désigner leurs représentants au sein du COGAS de La Poste ainsi que la répartition des sièges au sein de ce conseil n'est pas au nombre de ceux dont il appartient à la juridiction administrative de connaître.</ANA>
<ANA ID="9B"> 51-01-03 Le conseil d'orientation et de gestion des activités sociales (COGAS) de La Poste définit la politique et assure la gestion et le contrôle des activités sociales relevant de La Poste dont l'objet, selon le préambule de la convention constitutive du COGAS prévue par cet article, est de  contribuer à améliorer le bien-être des personnels  et de  développer leur accompagnement social . Ces activités ne relèvent pas de l'organisation du service public. Dès lors, le litige relatif à l'annulation de la décision par laquelle le président-directeur général du groupe La Poste a fixé la liste des organisations syndicales représentatives de l'ensemble du personnel habilitées à désigner leurs représentants au sein du COGAS de La Poste ainsi que la répartition des sièges au sein de ce conseil n'est pas au nombre de ceux dont il appartient à la juridiction administrative de connaître.</ANA>
<ANA ID="9C"> 66-05-01 Le conseil d'orientation et de gestion des activités sociales (COGAS) de La Poste définit la politique et assure la gestion et le contrôle des activités sociales relevant de La Poste dont l'objet, selon le préambule de la convention constitutive du COGAS prévue par cet article, est de  contribuer à améliorer le bien-être des personnels  et de  développer leur accompagnement social . Ces activités ne relèvent pas de l'organisation du service public. Dès lors, le litige relatif à l'annulation de la décision par laquelle le président-directeur général du groupe La Poste a fixé la liste des organisations syndicales représentatives de l'ensemble du personnel habilitées à désigner leurs représentants au sein du COGAS de La Poste ainsi que la répartition des sièges au sein de ce conseil n'est pas au nombre de ceux dont il appartient à la juridiction administrative de connaître.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., s'agissant du critère de l'organisation du service public, TC, 15 janvier 1968, Compagnie Air France c/ Epoux Barbier, n° 1908, p. 789. Rappr., s'agissant des conventions collectives, TC, 15 décembre 2008, M. Kim c/ Etablissement français du sang, n° 3652, T. pp. 647-950 ; s'agissant d'un accord d'entreprise, TC, 15 décembre 2008, M. Voisin c/ RATP, n° 3662, p. 563 ; s'agissant de la désignation des organisations syndicales représentatives habilitées à participer aux instances de dialogue social et à la négociation des conventions collectives, décision du même jour, Fédération des syndicats solidaires, unitaires et démocratiques des activités postales de télécommunications (Fédération SUD-PTT), n°s 357372-357418, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
