<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032698980</ID>
<ANCIEN_ID>JG_L_2016_06_000000386558</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/69/89/CETATEXT000032698980.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 08/06/2016, 386558</TITRE>
<DATE_DEC>2016-06-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>386558</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>Mme Sophie-Caroline de Margerie</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier Domino</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:386558.20160608</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Paris d'annuler la décision du 27 novembre 2013 par laquelle le ministre de l'intérieur a rejeté sa demande d'admission sur le territoire au titre de l'asile. Par un jugement n° 1316982/8 du 2 décembre 2013, le magistrat délégué par le président du tribunal administratif de Paris a annulé cette décision.<br/>
<br/>
              Par un arrêt n° 14PA00176 du 21 octobre 2014, la cour administrative d'appel de Paris a, sur appel du ministre de l'intérieur, annulé ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 18 décembre 2014 et 18 mars 2015 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel formé par le ministre de l'intérieur ;  <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention de Genève du 28 juillet 1951 relative au statut des réfugiés ;<br/>
              - la directive 2005/85/CE du Conseil du 1er décembre 2005 ;<br/>
              - le code de l'entrée et du séjour des étrangers en France et du droit d'asile ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie- Caroline de Margerie, conseiller d'Etat, <br/>
<br/>
              - les conclusions de M. Xavier Domino, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de M. A...; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M.A..., ressortissant sri-lankais, a fait l'objet d'une décision du 27 novembre 2013, prise après avis de l'Office français de protection des réfugiés et apatrides, du ministre de l'intérieur rejetant comme manifestement infondée la demande d'admission au séjour au titre de l'asile présentée par ce dernier lors de son arrivée à l'aéroport de Roissy ; qu'il se pourvoit contre l'arrêt de la cour administrative d'appel de Paris qui, statuant sur appel du ministre de l'intérieur, a annulé le jugement du 2 décembre 2013 par lequel le tribunal administratif de Paris avait annulé cette décision ;<br/>
<br/>
              2.	Considérant qu'aux termes de l'article 10 de la directive 2005/85/CE du Conseil du 1er décembre 2005 relative à des normes minimales concernant la procédure d'octroi et de retrait du statut de réfugié dans les Etats membres, applicable à la date de la décision contestée : " 1. En ce qui concerne les procédures prévues au chapitre III, les États membres veillent à ce que tous les demandeurs d'asile bénéficient des garanties suivantes : / a) ils sont informés, dans une langue dont il est raisonnable de supposer qu'ils la comprennent, de la procédure à suivre et de leurs droits et obligations au cours de la procédure ainsi que des conséquences que pourrait avoir le non-respect de leurs obligations ou le refus de coopérer avec les autorités. Ils sont informés du calendrier, ainsi que des moyens dont ils disposent pour remplir leur obligation de présenter les éléments visés à l'article 4 de la directive 2004/83/CE. Ces informations leur sont communiquées à temps pour leur permettre d'exercer les droits garantis par la présente directive et de se conformer aux obligations décrites à l'article 11 ; / b) ils bénéficient, en tant que de besoin, des services d'un interprète pour présenter leurs arguments aux autorités compétentes (...) c) la possibilité de communiquer avec le HCR ou toute autre organisation agissant au nom du HCR sur le territoire de l'État membre en vertu d'un accord conclu avec ce dernier ne leur est pas refusée " ; qu'aux termes de l'article 15 de la même directive : " 1. Les États membres accordent aux demandeurs d'asile la possibilité effective de consulter, à leurs frais, un conseil juridique ou un autre conseiller reconnu comme tel ou autorisé à cette fin en vertu du droit national sur des questions touchant à leur demande d'asile (...) " ; qu'aux termes de l'article 21 de la même directive : " 1. Les États membres autorisent le HCR : a) à avoir accès aux demandeurs d'asile, y compris ceux qui sont placés en rétention ou dans des zones de transit aéroportuaire ou portuaire ; / b) à avoir accès aux informations concernant chaque demande d'asile, l'état d'avancement de la procédure et les décisions prises, sous réserve que le demandeur d'asile y consente ; c) à donner son avis, dans l'accomplissement de la mission de surveillance que lui confère l'article 35 de la convention de Genève de 1951, à toute autorité compétente en ce qui concerne chaque demande d'asile et à tout stade de la procédure " ;<br/>
<br/>
              3.	Considérant qu'aux termes de l'article L. 221-1 du code de l'entrée et du séjour des étrangers et du droit d'asile, dans sa rédaction en vigueur à la date de la décision contestée : " L'étranger qui arrive en France par la voie (...) aérienne et qui, soit n'est pas autorisé à entrer sur le territoire français, soit demande son admission au titre de l'asile, peut être maintenu dans une zone d'attente située dans (...) un aéroport, pendant le temps strictement nécessaire à son départ et, s'il est demandeur d'asile, à un examen tendant à déterminer si sa demande n'est pas manifestement infondée (...) " ; qu'aux termes de l'article L. 221-4 du même code, dans sa rédaction en vigueur à la date de la décision contestée : " L'étranger maintenu en zone d'attente est informé, dans les meilleurs délais, qu'il peut demander l'assistance d'un interprète et d'un médecin, communiquer avec un conseil ou toute personne de son choix... " ; qu'aux termes de l'article R. 213-2 du même code, dans sa rédaction en vigueur à la date de la décision contestée : " Lorsque l'étranger qui se présente à la frontière demande à bénéficier du droit d'asile, il est informé sans délai, dans une langue dont il est raisonnable de penser qu'il la comprend, de la procédure de demande d'asile, de ses droits et obligations au cours de cette procédure, des conséquences que pourrait avoir le non-respect de ses obligations ou le refus de coopérer avec les autorités et des moyens dont il dispose pour l'aider à présenter sa demande. / La décision de refus d'entrée ne peut être prise qu'après consultation de l'Office français de protection des réfugiés et apatrides, qui procède à l'audition de l'étranger. / Lorsque l'audition du demandeur d'asile nécessite l'assistance d'un interprète, sa rétribution est prise en charge par l'Etat. / Cette audition fait l'objet d'un rapport écrit (...) " ; <br/>
<br/>
              4.	Considérant qu'il résulte des dispositions de l'article R. 213-2 du code de l'entrée et du séjour des étrangers et du droit d'asile, qui ont assuré la transposition de l'article 10 de la directive 2005/85/CE, que l'étranger qui se présente à la frontière et demande à bénéficier du droit d'asile doit être informé du déroulement de la procédure dont il fait l'objet et des moyens dont il dispose pour satisfaire à son obligation de justifier du bien-fondé de sa demande ; que ces dispositions impliquent notamment que l'étranger soit informé de la possibilité de communiquer avec un représentant du Haut Commissariat des Nations unies pour les Réfugiés (HCR) ; que dès lors, en jugeant que l'administration ne devait pas justifier avoir informé le requérant de la possibilité de communiquer avec un représentant du HCR, la cour administrative d'appel de Paris a commis une erreur de droit ;  <br/>
<br/>
              5.	Considérant qu'il résulte de ce qui précède que, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, M. A...est fondé à demander l'annulation de l'arrêt qu'il attaque ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à M. A...au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Paris du 21 octobre 2014 est annulé.<br/>
<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Paris.<br/>
<br/>
Article 3 : L'Etat versera à M. A...une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : Le présent arrêt sera notifié à M. B...A...et au ministre de l'intérieur.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">095-02-01 - INFORMATION DE L'ÉTRANGER SUR LE DÉROULEMENT DE LA PROCÉDURE ET LES MOYENS DONT IL DISPOSE POUR JUSTIFIER DU BIEN-FONDÉ DE SA DEMANDE - OBLIGATION DE L'INFORMER DE LA POSSIBILITÉ DE COMMUNIQUER AVEC UN REPRÉSENTANT DU HCR - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">15-05-045-05 COMMUNAUTÉS EUROPÉENNES ET UNION EUROPÉENNE. RÈGLES APPLICABLES. - DEMANDE D'ASILE À LA FRONTIÈRE - INFORMATION DE L'ÉTRANGER SUR LE DÉROULEMENT DE LA PROCÉDURE ET LES MOYENS DONT IL DISPOSE POUR JUSTIFIER DU BIEN-FONDÉ DE SA DEMANDE - OBLIGATION DE L'INFORMER DE LA POSSIBILITÉ DE COMMUNIQUER AVEC UN REPRÉSENTANT DU HCR - EXISTENCE.
</SCT>
<ANA ID="9A"> 095-02-01 Il résulte des dispositions de l'article R. 213-2 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA), qui ont assuré la transposition de l'article 10 de la directive 2005/85/CE, que l'étranger qui se présente à la frontière et demande à bénéficier du droit d'asile doit être informé du déroulement de la procédure dont il fait l'objet et des moyens dont il dispose pour satisfaire à son obligation de justifier du bien-fondé de sa demande. Ces dispositions impliquent notamment que l'étranger soit informé de la possibilité de communiquer avec un représentant du Haut Commissariat des Nations unies pour les Réfugiés (HCR).</ANA>
<ANA ID="9B"> 15-05-045-05 Il résulte des dispositions de l'article R. 213-2 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA), qui ont assuré la transposition de l'article 10 de la directive 2005/85/CE, que l'étranger qui se présente à la frontière et demande à bénéficier du droit d'asile doit être informé du déroulement de la procédure dont il fait l'objet et des moyens dont il dispose pour satisfaire à son obligation de justifier du bien-fondé de sa demande. Ces dispositions impliquent notamment que l'étranger soit informé de la possibilité de communiquer avec un représentant du Haut Commissariat des Nations unies pour les Réfugiés (HCR).</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
