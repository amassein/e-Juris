<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038633890</ID>
<ANCIEN_ID>JG_L_2019_06_000000419770</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/63/38/CETATEXT000038633890.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 6ème chambres réunies, 17/06/2019, 419770, Publié au recueil Lebon</TITRE>
<DATE_DEC>2019-06-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>419770</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 6ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR>M. Florian Roussel</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2019:419770.20190617</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A...B...a demandé au tribunal administratif de Versailles d'annuler la décision du 10 décembre 2015 par laquelle la caisse d'allocations familiales de l'Essonne ne lui a accordé qu'une remise partielle de 1 257,49 euros sur une dette totale d'un montant de 2 095,82 euros au titre d'un indu d'aide personnalisée au logement. Par une ordonnance n° 1600288 du 16 mars 2018, la présidente du tribunal a donné acte de son désistement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 11 avril et 27 juillet 2018 et 27 mai 2019 au secrétariat du contentieux du Conseil d'Etat, Mme B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros à verser à la SCP Matuchansky, Poupot, Valdelièvre en application des articles 37 de la loi du 10 juillet 1991 et L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              - loi n° 91-647 du 10 juillet 1991 ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Florian Roussel, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Matuchansky, Poupot, Valdelièvre, avocat de MmeB... ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article R. 612-5-1 du code de justice administrative : " Lorsque l'état du dossier permet de s'interroger sur l'intérêt que la requête conserve pour son auteur, le président de la formation de jugement ou, au Conseil d'Etat, le président de la chambre chargée de l'instruction, peut inviter le requérant à confirmer expressément le maintien de ses conclusions. La demande qui lui est adressée mentionne que, à défaut de réception de cette confirmation à l'expiration du délai fixé, qui ne peut être inférieur à un mois, il sera réputé s'être désisté de l'ensemble de ses conclusions ".<br/>
<br/>
              2. Il ressort des pièces du dossier soumis au juge du fond que Mme B... a demandé au tribunal administratif de Versailles d'annuler la décision du 10 décembre 2015 par laquelle la caisse d'allocations familiales de l'Essonne ne lui a accordé qu'une remise partielle de 1 257,49 euros sur une dette d'un montant de 2 095,82 euros au titre d'un indu d'aide personnalisée au logement. Par un courrier du 2 février 2018, le tribunal lui a demandé de confirmer expressément le maintien de ses conclusions dans un délai d'un mois. Mme B...n'ayant pas répondu à cette lettre du 2 février 2018, la présidente du tribunal, se fondant sur les dispositions précitées de l'article R. 612-5-1 du code de justice administrative, a regardé la requérante comme s'étant désistée de ses conclusions et a donné acte de ce désistement par une ordonnance du 16 mars 2018 contre laquelle Mme B...se pourvoit en cassation. <br/>
<br/>
              3. A l'occasion de la contestation de l'ordonnance donnant acte d'un désistement par application de l'article R. 612-5-1 du code de justice administrative en l'absence de réponse du requérant à la demande de confirmation de ses conclusions dans le délai qui lui a été imparti, il incombe au juge de cassation, saisi de moyens en ce sens, de vérifier que l'intéressé a reçu la demande de confirmation du maintien de ses conclusions, que cette demande laissait au requérant un délai d'au moins un mois pour y répondre et l'informait des conséquences d'un défaut de réponse dans ce délai et que le requérant s'est abstenu de répondre en temps utile. Si les motifs pour lesquels le signataire de l'ordonnance, auquel il incombe de veiller à une bonne administration de la justice, estime que l'état du dossier permet de s'interroger sur l'intérêt que la requête conserve pour son auteur ne peuvent en principe être utilement discutés devant le juge de cassation, il appartient néanmoins à ce dernier de censurer l'ordonnance qui lui est déférée dans le cas où il juge, au vu de l'ensemble des circonstances de l'espèce, qu'il a été fait un usage abusif de la faculté ouverte par l'article R. 612-5-1 du code de justice administrative. <br/>
<br/>
              4. En l'espèce, il ressort des pièces du dossier du juge du fond que, postérieurement à l'introduction de sa requête, qui tendait à obtenir la remise d'un indu d'aide personnalisée au logement et relevait des dispositions des articles R. 772-5 et suivants du code de justice administrative relatives aux contentieux sociaux, Mme B...a produit trois nouveaux mémoires par lesquels elle attirait l'attention de la juridiction sur l'urgence de sa situation et demandait que l'affaire soit jugée dans les meilleurs délais. Dans son mémoire en défense, la caisse d'allocations familiales de l'Essonne concluait au rejet de la demande, sans faire état d'aucun élément laissant penser qu'elle envisageait de revenir sur sa position. Enfin, la lettre par laquelle le vice-président du tribunal demandait à l'intéressée de confirmer expressément le maintien de ses conclusions a été retournée au tribunal avec la mention selon laquelle ce courrier n'avait pu lui être remis à son adresse et n'avait pas été réclamé au bureau de poste dans le délai de quinze jours qui lui était laissé à cette fin. Compte tenu de l'ensemble de ces circonstances, l'auteur de l'ordonnance attaquée n'a pu, sans faire un usage abusif de la faculté ouverte par l'article R. 612-5-1 du code de justice administrative, regarder l'absence de réponse de Mme B...à ce courrier comme traduisant une renonciation de sa part à l'instance introduite. Par suite, sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, l'ordonnance donnant acte de son désistement doit être annulée.<br/>
<br/>
              5. Mme B...a obtenu le bénéfice de l'aide juridictionnelle. Par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. Il y a lieu, dans les circonstances de l'espèce, et sous réserve que la SCP Matuchansky, Poupot, Valdelièvre, avocat de MmeB..., renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à cette société.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du tribunal administratif de Versailles du 16 mars 2018 est annulée.<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Versailles.<br/>
Article 3 : L'Etat versera à la SCP Matuchansky, Poupot, Valdelièvre une somme de 3 000 euros en application de l'article L. 761-1 du code de justice administrative et du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat. <br/>
Article 4 : La présente décision sera notifiée à Mme A...B...et à la ministre de la cohésion des territoires et des relations avec les collectivités territoriales.<br/>
Copie en sera adressée à la caisse d'allocations familiales de l'Essonne.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-05-04-03 PROCÉDURE. INCIDENTS. DÉSISTEMENT. DÉSISTEMENT D'OFFICE. - DÉSISTEMENT D'OFFICE D'UN REQUÉRANT N'AYANT PAS RÉPONDU, À L'EXPIRATION DU DÉLAI IMPARTI, À UNE DEMANDE DU JUGE LUI DEMANDANT DE CONFIRMER LE MAINTIEN DE SES CONCLUSIONS (ART. R. 612-5-1 DU CJA) - CONTESTATION D'UNE ORDONNANCE PRENANT ACTE D'UN TEL DÉSISTEMENT - CONTRÔLE DU JUGE - 1) RÉGULARITÉ FORMELLE DE LA DEMANDE ADRESSÉE AU REQUÉRANT - EXISTENCE [RJ1] - 2) BIEN-FONDÉ - CONTRÔLE DES SEULS ABUS DE L'USAGE DE CETTE FACULTÉ [RJ2].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-08 PROCÉDURE. VOIES DE RECOURS. - DÉSISTEMENT D'OFFICE D'UN REQUÉRANT N'AYANT PAS RÉPONDU, À L'EXPIRATION DU DÉLAI IMPARTI, À UNE DEMANDE DU JUGE LUI DEMANDANT DE CONFIRMER LE MAINTIEN DE SES CONCLUSIONS (ART. R. 612-5-1 DU CJA) - CONTESTATION D'UNE ORDONNANCE PRENANT ACTE D'UN TEL DÉSISTEMENT - CONTRÔLE DU JUGE - 1) RÉGULARITÉ FORMELLE DE LA DEMANDE ADRESSÉE AU REQUÉRANT - EXISTENCE [RJ1] - 2) BIEN-FONDÉ - CONTRÔLE DES SEULS ABUS DE L'USAGE DE CETTE FACULTÉ [RJ2].
</SCT>
<ANA ID="9A"> 54-05-04-03 1) A l'occasion de la contestation de l'ordonnance donnant acte d'un désistement par application de l'article R. 612-5-1 du code de justice administrative (CJA) en l'absence de réponse du requérant à la demande de confirmation de ses conclusions dans le délai qui lui a été imparti, il incombe au juge de cassation, saisi de moyens en ce sens, de vérifier que l'intéressé a reçu la demande de confirmation du maintien de ses conclusions, que cette demande laissait au requérant un délai d'au moins un mois pour y répondre et l'informait des conséquences d'un défaut de réponse dans ce délai et que le requérant s'est abstenu de répondre en temps utile.... ,,2) Si les motifs pour lesquels le signataire de l'ordonnance, auquel il incombe de veiller à une bonne administration de la justice, estime que l'état du dossier permet de s'interroger sur l'intérêt que la requête conserve pour son auteur ne peuvent en principe être utilement discutés devant le juge de cassation, il appartient néanmoins à ce dernier de censurer l'ordonnance qui lui est déférée dans le cas où il juge, au vu de l'ensemble des circonstances de l'espèce, qu'il a été fait un usage abusif de la faculté ouverte par l'article R. 612-5-1 du CJA.</ANA>
<ANA ID="9B"> 54-08 1) A l'occasion de la contestation de l'ordonnance donnant acte d'un désistement par application de l'article R. 612-5-1 du code de justice administrative (CJA) en l'absence de réponse du requérant à la demande de confirmation de ses conclusions dans le délai qui lui a été imparti, il incombe au juge de cassation, saisi de moyens en ce sens, de vérifier que l'intéressé a reçu la demande de confirmation du maintien de ses conclusions, que cette demande laissait au requérant un délai d'au moins un mois pour y répondre et l'informait des conséquences d'un défaut de réponse dans ce délai et que le requérant s'est abstenu de répondre en temps utile.... ,,2) Si les motifs pour lesquels le signataire de l'ordonnance, auquel il incombe de veiller à une bonne administration de la justice, estime que l'état du dossier permet de s'interroger sur l'intérêt que la requête conserve pour son auteur ne peuvent en principe être utilement discutés devant le juge de cassation, il appartient néanmoins à ce dernier de censurer l'ordonnance qui lui est déférée dans le cas où il juge, au vu de l'ensemble des circonstances de l'espèce, qu'il a été fait un usage abusif de la faculté ouverte par l'article R. 612-5-1 du CJA.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., sur ce point, CE, 19 mars 2018, SAS Roset, n°s 410389 410395, T. pp. 840-842-863,,[RJ2] Ab. jur., s'agissant de l'absence complet de contrôle sur ce point, CE, 19 mars 2018, SAS Roset, n°s 410389 410395, T. pp. 840-842-863. Rappr., s'agissant de la nature du contrôle, CE, Section, 5 octobre 2018, SA Finamur, n° 412560, p. 370.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
