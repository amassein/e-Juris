<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039451884</ID>
<ANCIEN_ID>JG_L_2019_12_000000416798</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/45/18/CETATEXT000039451884.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 4ème chambres réunies, 04/12/2019, 416798, Publié au recueil Lebon</TITRE>
<DATE_DEC>2019-12-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>416798</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Cécile Chaduteau-Monplaisir</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie Sirinelli</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:416798.20191204</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés le 22 décembre 2017 et le 16 juillet 2018 au secrétariat du contentieux du Conseil d'Etat, la fédération des entreprises de la beauté demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 9 décembre 2017 par laquelle le directeur général de l'Agence nationale de sécurité du médicament et des produits de santé (ANSM) a implicitement rejeté sa demande du 6 octobre 2017 d'abroger la recommandation intitulée " Concentration de phénoxyéthanol dans les produits cosmétiques - point d'information " ;<br/>
<br/>
              2°) d'enjoindre au directeur général de l'ANSM d'abroger cette recommandation et de la retirer du site Internet et de toutes les publications de l'ANSM, dans un délai d'une semaine à compter de la décision à intervenir, sous astreinte de 2 000 euros par jour de retard ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le règlement (CE) n°1223/2009 du 30 novembre 2009 ; <br/>
              - le code de santé publique ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Cécile Chaduteau-Monplaisir, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Marie Sirinelli, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. L'article L. 5312-4 du code de santé publique prévoit que " (...) dans tous les cas où l'intérêt de la santé publique l'exige les autorités sanitaires informent, si nécessaire, l'opinion publique et les professionnels de santé par tout moyen et notamment par la diffusion de messages sanitaires ou d'avis de rappel de produit sur tout support approprié ".<br/>
<br/>
              2. Il ressort des pièces du dossier que, sur le fondement de ces dispositions, par un " point d'information " consacré à la " concentration de phénoxyéthanol dans les produits cosmétiques ", publié sur son site internet à compter du 26 novembre 2012, l'Agence nationale de sécurité du médicament et des produits de santé a présenté la synthèse d'un rapport d'évaluation du risque lié à l'exposition au phénoxyéthanol, en préconisant, pour les enfants de moins de trois ans, de ne pas utiliser le phénoxyéthanol dans les produits cosmétiques destinés au siège et de restreindre sa concentration dans tous les autres types de produits à 0,4 %. Cette recommandation est depuis lors restée accessible sur ce site, complétée depuis le 7 décembre 2016 par une mise à jour faisant mention de l'avis rendu le 6 octobre 2016 par le comité scientifique européen pour la sécurité des consommateurs et renvoyant au site de la Commission européenne. La fédération des entreprises de la beauté doit être regardée comme demandant au Conseil d'Etat, d'une part, d'annuler pour excès de pouvoir la décision du 9 décembre 2017 par laquelle le directeur général de l'agence a implicitement rejeté sa demande du 6 octobre 2017 d'abroger cette recommandation et de porter son abrogation à la connaissance du public et, d'autre part, d'enjoindre au directeur général de l'agence de l'abroger et de la retirer du site internet et de toutes les publications de l'agence.<br/>
<br/>
              3. Alors même qu'elle est, par elle-même, dépourvue d'effets juridiques, cette recommandation, prise par une autorité administrative, consultable sur internet et relayée par les associations de défense des consommateurs, a eu pour objet d'influer de manière significative sur les comportements des fabricants et des distributeurs des produits cosmétiques destinés aux enfants de moins de trois ans, ainsi que sur les comportements de consommation des personnes responsables de ces enfants, et est également de nature à produire des effets notables. L'annulation pour excès de pouvoir du refus d'abroger un tel acte implique que l'autorité compétente non seulement procède à l'abrogation de cet acte mais aussi, eu égard à sa nature et à ses effets, en tire les conséquences pertinentes quant à la publicité qui lui est donnée.<br/>
<br/>
              4. Aux termes de son article premier, le règlement (CE) n° 1223/2009 du Parlement européen et du Conseil du 30 novembre 2009 relatif aux produits cosmétiques " établit des règles auxquelles doit satisfaire tout produit cosmétique mis à disposition sur le marché, afin de garantir le fonctionnement du marché intérieur et d'assurer un niveau élevé de protection de la santé humaine ". Ses considérants (4) et (36) précisent qu'il harmonise de manière exhaustive les règles en vigueur dans la Communauté et que les mesures prises par la Commission et les Etats membres concernant la protection de la santé humaine reposent sur le principe de précaution. Aux termes de son article 9 : " Les États membres ne refusent pas, n'interdisent pas et ne restreignent pas, pour des raisons concernant les exigences contenues dans le présent règlement, la mise à disposition sur le marché des produits cosmétiques qui répondent aux prescriptions du présent règlement ". En vertu des dispositions combinées de l'article 14 et de l'annexe V de ce règlement, les produits cosmétiques ne peuvent contenir de phénoxyéthanol, classé comme agent conservateur au rang 29 de cette annexe, que si la concentration de cette substance dans le produit ne dépasse pas 1 %, sans autre restriction liée notamment à l'âge ou à la zone corporelle d'utilisation. Enfin, l'article 27 du même règlement comporte une " clause de sauvegarde ", permettant aux autorités compétentes des Etats membres de prendre des mesures provisoires lorsqu'un produit cosmétique présente ou pourrait présenter un risque grave pour la santé humaine, sous réserve de communiquer immédiatement ces mesures à la Commission et de les abroger si la Commission ne les estime pas justifiées.<br/>
<br/>
              5. S'il ressort des pièces du dossier que la recommandation litigieuse n'a pas été adoptée par l'Agence nationale de sécurité des médicaments et des produits de santé sur le fondement de l'article 27 du règlement du 30 novembre 2009, il en ressort également que, par une décision du 13 mars 2019, l'Agence nationale de sécurité du médicament et des produits de santé a souhaité mettre en oeuvre la " clause de sauvegarde " instituée par cet article. Elle a, à ce titre, soumis, à titre conservatoire, la mise à disposition sur le marché des produits cosmétiques non rincés contenant du phénoxyéthanol, à l'exclusion des déodorants, des produits de coiffage et des produits de maquillage, à la mention sur leur étiquetage de ce que ces produits ne peuvent être utilisés sur le siège des enfants de 3 ans ou moins. Eu égard, d'une part, à l'objet et aux motifs de cette décision, par lesquels l'agence indique notamment se fonder sur les conclusions de l'évaluation menée par le comité scientifique spécialisé temporaire qu'elle a réuni en vue d'apprécier l'opportunité de maintenir sa recommandation antérieure, lequel a conclu à l'élargissement de cette recommandation aux produits cosmétiques utilisés sur le siège des enfants de trois ans ou moins même sans y être destinés et, au contraire, à son absence de maintien pour les autres produits cosmétiques destinés à ces enfants, et, d'autre part, au " point d'information " qui accompagne cette décision depuis sa publication sur le site internet de l'agence le 20 mars 2019, indiquant que, sur la base de cet avis, elle a pris à titre provisoire une décision de police sanitaire et que " pour les autres produits destinés aux enfants de 3 ans ou moins, la concentration de 1 % en phénoxyéthanol est applicable, conformément au règlement européen relatif aux produits cosmétiques ", la décision du 13 mars 2019 doit être regardée comme constituant le dernier état des mesures prises par l'Agence nationale de sécurité du médicament et des produits de santé en cette matière et comme ayant ainsi remplacé, à compter de sa publication, la recommandation litigieuse par laquelle l'agence préconisait, pour les enfants de moins de trois ans, de ne pas utiliser le phénoxyéthanol dans les produits cosmétiques destinés au siège et de restreindre sa concentration dans tous les autres types de produits à 0,4 %. Par suite, les conclusions de la requête à fin d'annulation de la décision attaquée en tant qu'elle refuse d'abroger cette recommandation et à fin d'injonction de l'abroger ont perdu leur objet. Il n'y a, par suite, pas lieu d'y statuer.   <br/>
<br/>
              6. Toutefois, ainsi que la fédération requérante le fait valoir, il ressort des pièces du dossier que la recommandation litigieuse figure toujours sur le site internet de l'Agence nationale de sécurité du médicament et des produits de santé, sans que cette mise en ligne s'accompagne de mentions permettant, lors de sa consultation, d'être informé qu'elle n'est plus en vigueur. <br/>
<br/>
              7. Dans ces conditions, il y a lieu d'annuler la décision attaquée en tant qu'elle refuse de prendre les mesures permettant de porter à la connaissance du public l'abrogation de la recommandation en litige et d'enjoindre à l'Agence nationale de sécurité du médicament et des produits de santé, dans un délai de deux semaines à compter de la présente décision, sauf à ce qu'elle mette fin à la mise en ligne de cette recommandation, de prendre les mesures nécessaires pour l'accompagner de mentions propres à permettre, lors de sa consultation, d'être informé qu'elle n'est plus en vigueur. Il n'y a pas lieu d'assortir cette injonction d'une astreinte.<br/>
<br/>
              8. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre de mettre à la charge de l'Etat la somme que demande la fédération requérante au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : Il n'y a pas lieu de statuer sur les conclusions de la fédération des entreprises de la beauté à fin d'annulation de la décision du directeur général de l'Agence nationale de sécurité du médicament et des produits de santé du 9 décembre 2017 en tant qu'elle refuse d'abroger la recommandation intitulée " Concentration de phénoxyéthanol dans les produits cosmétiques - point d'information " et à fin d'injonction de l'abroger.<br/>
Article 2 : La décision du directeur général de l'Agence nationale de sécurité du médicament et des produits de santé du 9 décembre 2017 est annulée en tant qu'elle refuse de prendre les mesures permettant de porter à la connaissance du public l'abrogation de la recommandation intitulée " Concentration de phénoxyéthanol dans les produits cosmétiques - point d'information ".<br/>
Article 3 : Il est enjoint à l'Agence nationale de sécurité du médicament et des produits de santé, dans un délai de deux semaines à compter de la présente décision, sauf à ce qu'elle mette fin à la mise en ligne de cette recommandation, de prendre les mesures nécessaires pour l'accompagner de mentions propres à permettre, lors de sa consultation, d'être informé qu'elle n'est plus en vigueur.<br/>
Article 4 : Les conclusions de la fédération des entreprises de la beauté présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à la fédération des entreprises de la beauté et à l'Agence nationale de sécurité du médicament et des produits de santé.<br/>
Copie en sera adressée à la ministre des solidarités et de la santé et à la section du rapport et des études.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-01-05-02-02 ACTES LÉGISLATIFS ET ADMINISTRATIFS. DIFFÉRENTES CATÉGORIES D'ACTES. ACTES ADMINISTRATIFS - NOTION. ACTES À CARACTÈRE DE DÉCISION. ACTES NE PRÉSENTANT PAS CE CARACTÈRE. - REFUS D'ABROGER UNE RECOMMANDATION DE L'ANSM PRÉCONISANT DES RESTRICTIONS D'EMPLOI D'UNE SUBSTANCE DANS LES PRODUITS COSMÉTIQUES - ACTE SUSCEPTIBLE DE FAIRE L'OBJET D'UN RECOURS POUR EXCÈS DE POUVOIR - EXISTENCE [RJ1] - CONSÉQUENCES NÉCESSAIRES DE L'ANNULATION - INJONCTION D'ABROGER ET D'EN TIRER LES CONSÉQUENCES EN MATIÈRE DE PUBLICITÉ.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-01-01-01 PROCÉDURE. INTRODUCTION DE L'INSTANCE. DÉCISIONS POUVANT OU NON FAIRE L'OBJET D'UN RECOURS. ACTES CONSTITUANT DES DÉCISIONS SUSCEPTIBLES DE RECOURS. - REFUS D'ABROGER UNE RECOMMANDATION DE L'ANSM PRÉCONISANT DES RESTRICTIONS D'EMPLOI D'UNE SUBSTANCE DANS LES PRODUITS COSMÉTIQUES [RJ1] - CONSÉQUENCES NÉCESSAIRES DE L'ANNULATION - INJONCTION D'ABROGER ET D'EN TIRER LES CONSÉQUENCES EN MATIÈRE DE PUBLICITÉ.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-06-07-005 PROCÉDURE. JUGEMENTS. EXÉCUTION DES JUGEMENTS. EFFETS D'UNE ANNULATION. - REFUS D'ABROGER UNE RECOMMANDATION DE L'ANSM PRÉCONISANT DES RESTRICTIONS D'EMPLOI D'UNE SUBSTANCE DANS LES PRODUITS COSMÉTIQUES [RJ1] - CONSÉQUENCES NÉCESSAIRES DE L'ANNULATION - INJONCTION D'ABROGER ET D'EN TIRER LES CONSÉQUENCES EN MATIÈRE DE PUBLICITÉ.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">61-041 SANTÉ PUBLIQUE. - REFUS D'ABROGER UNE RECOMMANDATION DE L'ANSM PRÉCONISANT DES RESTRICTIONS D'EMPLOI D'UNE SUBSTANCE DANS LES PRODUITS COSMÉTIQUES - ACTE SUSCEPTIBLE DE FAIRE L'OBJET D'UN RECOURS POUR EXCÈS DE POUVOIR - EXISTENCE [RJ1] - CONSÉQUENCES NÉCESSAIRES DE L'ANNULATION - INJONCTION D'ABROGER ET D'EN TIRER LES CONSÉQUENCES EN MATIÈRE DE PUBLICITÉ.
</SCT>
<ANA ID="9A"> 01-01-05-02-02 Recommandation de l'Agence nationale de sécurité du médicament et des produits de santé (ANSM) préconisant, pour les enfants de moins de trois ans, de ne pas utiliser le phénoxyéthanol dans les produits cosmétiques destinés au siège et de restreindre sa concentration dans tous les autres types de produits à 0,4 %.,,,Alors même qu'elle est, par elle-même, dépourvue d'effets juridiques, cette recommandation, prise par une autorité administrative, consultable sur internet et relayée par les associations de défense des consommateurs, a eu pour objet d'influer de manière significative sur les comportements des fabricants et des distributeurs des produits cosmétiques destinés aux enfants de moins de trois ans, ainsi que sur les comportements de consommation des personnes responsables de ces enfants, et est également de nature à produire des effets notables.... ,,L'annulation pour excès de pouvoir du refus d'abroger un tel acte implique que l'autorité compétente non seulement procède à l'abrogation de cet acte mais aussi, eu égard à sa nature et à ses effets, en tire les conséquences pertinentes quant à la publicité qui lui est donnée.</ANA>
<ANA ID="9B"> 54-01-01-01 Recommandation de l'Agence nationale de sécurité du médicament et des produits de santé (ANSM) préconisant, pour les enfants de moins de trois ans, de ne pas utiliser le phénoxyéthanol dans les produits cosmétiques destinés au siège et de restreindre sa concentration dans tous les autres types de produits à 0,4 %.,,,Alors même qu'elle est, par elle-même, dépourvue d'effets juridiques, cette recommandation, prise par une autorité administrative, consultable sur internet et relayée par les associations de défense des consommateurs, a eu pour objet d'influer de manière significative sur les comportements des fabricants et des distributeurs des produits cosmétiques destinés aux enfants de moins de trois ans, ainsi que sur les comportements de consommation des personnes responsables de ces enfants, et est également de nature à produire des effets notables.... ,,L'annulation pour excès de pouvoir du refus d'abroger un tel acte implique que l'autorité compétente non seulement procède à l'abrogation de cet acte mais aussi, eu égard à sa nature et à ses effets, en tire les conséquences pertinentes quant à la publicité qui lui est donnée.</ANA>
<ANA ID="9C"> 54-06-07-005 Recommandation de l'Agence nationale de sécurité du médicament et des produits de santé (ANSM) préconisant, pour les enfants de moins de trois ans, de ne pas utiliser le phénoxyéthanol dans les produits cosmétiques destinés au siège et de restreindre sa concentration dans tous les autres types de produits à 0,4 %.,,,Alors même qu'elle est, par elle-même, dépourvue d'effets juridiques, cette recommandation, prise par une autorité administrative, consultable sur internet et relayée par les associations de défense des consommateurs, a eu pour objet d'influer de manière significative sur les comportements des fabricants et des distributeurs des produits cosmétiques destinés aux enfants de moins de trois ans, ainsi que sur les comportements de consommation des personnes responsables de ces enfants, et est également de nature à produire des effets notables.... ,,L'annulation pour excès de pouvoir du refus d'abroger un tel acte implique que l'autorité compétente non seulement procède à l'abrogation de cet acte mais aussi, eu égard à sa nature et à ses effets, en tire les conséquences pertinentes quant à la publicité qui lui est donnée.</ANA>
<ANA ID="9D"> 61-041 Recommandation de l'Agence nationale de sécurité du médicament et des produits de santé (ANSM) préconisant, pour les enfants de moins de trois ans, de ne pas utiliser le phénoxyéthanol dans les produits cosmétiques destinés au siège et de restreindre sa concentration dans tous les autres types de produits à 0,4 %.,,,Alors même qu'elle est, par elle-même, dépourvue d'effets juridiques, cette recommandation, prise par une autorité administrative, consultable sur internet et relayée par les associations de défense des consommateurs, a eu pour objet d'influer de manière significative sur les comportements des fabricants et des distributeurs des produits cosmétiques destinés aux enfants de moins de trois ans, ainsi que sur les comportements de consommation des personnes responsables de ces enfants, et est également de nature à produire des effets notables.... ,,L'annulation pour excès de pouvoir du refus d'abroger un tel acte implique que l'autorité compétente non seulement procède à l'abrogation de cet acte mais aussi, eu égard à sa nature et à ses effets, en tire les conséquences pertinentes quant à la publicité qui lui est donnée.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., s'agissant d'une recommandation de l'ANSM, CE, 21 octobre 2019, Association française de l'industrie pharmaceutique pour une automédication responsable, n°s 419996 419997, à publier au Recueil ; s'agissant du refus d'une autorité de régulation d'abroger un acte de droit souple, CE, Section, 13 juillet 2016, Société GDF Suez, n° 388150, p. 384.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
