<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032950542</ID>
<ANCIEN_ID>JG_L_2016_07_000000383412</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/95/05/CETATEXT000032950542.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 22/07/2016, 383412</TITRE>
<DATE_DEC>2016-07-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>383412</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SEVAUX, MATHONNET ; SCP LE GRIEL</AVOCATS>
<RAPPORTEUR>M. Christian Fournier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:383412.20160722</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B... A...a demandé au tribunal administratif de Nancy d'annuler pour excès de pouvoir la décision du 13 février 2012 par laquelle l'évêque de Metz a mis fin à ses fonctions de secrétaire général d'évêché et la décision du 22 février 2012 par laquelle le ministre de l'intérieur a mis fin au versement de son traitement. Par un jugement n° 1201796 et n° 1201798 du 12 février 2013, le tribunal administratif de Nancy a rejeté sa demande. Par un arrêt n° 13NC00691 du 18 juin 2014, la cour administrative d'appel de Nancy a rejeté l'appel formé par M. A... contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 4 août et 4 novembre 2014 et 11 janvier 2016 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'arrêt n° 13NC00691 du 18 juin 2014 de la cour administrative d'appel de Nancy ; <br/>
<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la loi du 31 mars 1873 relative au statut des fonctionnaires d'Empire ;<br/>
              - la loi d'Alsace-Lorraine du 23 décembre 1873 ;<br/>
              - le décret n° 86-83 du 17 janvier 1986 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Christian Fournier, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Sevaux, Mathonnet, avocat de M. A...et à la SCP Le Griel, avocat de l'Evéché de Metz ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 11 juillet 2016, présentée par M. A... ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. A... a été recruté par l'évêque de Metz, le 1er janvier 1999, sur un emploi rémunéré par l'Etat, pour exercer en qualité d'agent de la mense épiscopale de Metz dans des fonctions de " secrétaire des évêchés " ; qu'à compter du  1er septembre 2005,  il a exercé des fonctions  de " secrétaire général " puis a été affecté sur le poste de directeur adjoint de la bibliothèque diocésaine de Metz à compter du 9 octobre 2006 ; que par une décision du 13 février 2012, l'évêque de Metz a mis fin à ses fonctions et, par une décision du 22 février 2012, le ministre de l'intérieur a mis fin au versement de son traitement ; que par un jugement du 12 février 2013, le tribunal administratif de Nancy a rejeté sa demande tendant à l'annulation de ces deux décisions ; que M. A... se pourvoit en cassation contre l'arrêt du 18 juin 2014 par lequel la cour administrative d'appel de Nancy a rejeté l'appel qu'il avait formé contre ce jugement ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article 1er de la loi d'Empire du 31 mars 1873 relative au statut des fonctionnaires d'Empire, maintenue en vigueur dans les départements du Bas Rhin, du Haut Rhin et de Moselle : " Est fonctionnaire d'Empire, au sens de la présente loi, tout fonctionnaire nommé par l'Empereur, ou tenu, aux termes de la Constitution de l'Empire, d'obéir aux ordres de l'Empereur " ; qu'aux termes de l'article premier de la loi d'Alsace-Lorraine du 23 décembre 1873 relative au statut des fonctionnaires et des maîtres de l'enseignement : " La loi d'Empire ci-annexée du 31 mars 1873 relative au statut des fonctionnaires d'Empire est introduite en Alsace-Lorraine. / Elle règle le statut des fonctionnaires d'Alsace-Lorraine qui touchent des émoluments sur le Trésor d'Alsace-Lorraine, ainsi que les maîtres et maîtresses de l'enseignement public. (...) " ; <br/>
<br/>
              3. Considérant, en premier lieu, que les agents publics rémunérés par l'Etat et employés par la mense épiscopale de Metz, établissement public du culte en vertu du droit local maintenu en vigueur, sont recrutés par l'évêque et chargés d'administrer, sous sa direction, les biens du diocèse ; qu'eu égard à leurs conditions de recrutement et d'emploi, sous l'autorité de l'évêque, ils ne sont pas régis par les dispositions de la loi du 31 mars 1873 qui ne s'appliquent qu'à un " fonctionnaires nommé par l'Empereur ou tenu (...) d'obéir à ses ordres " ; que ce motif, qui est d'ordre public et n'appelle l'appréciation d'aucune circonstance de fait, doit être substitué au motif retenu par l'arrêt attaqué, dont il justifie, sur ce point, le dispositif ; que, par suite, le moyen soulevé par M. A... à l'encontre de l'arrêt, en tant qu'il a jugé qu'il ne pouvait se prévaloir des dispositions de l'article 86 de la loi du 31 mars 1873, ne peut qu'être écarté ;<br/>
<br/>
              4. Considérant, en deuxième lieu, que le décret du 17 janvier 1986 relatif aux dispositions applicables aux agents non titulaires de l'Etat pris pour l'application de l'article 7 de la loi n° 84-16 du 11 janvier 1984 portant dispositions statutaires relatives à la fonction publique de l'Etat, pose les règles applicables aux agents non titulaires de l'Etat et de ses établissements publics à caractère administratif ;  que la mense épiscopale de Metz, qui a le statut, ainsi qu'il vient d'être dit, d'établissement public du culte, doit être regardée, pour l'application du décret du 17 janvier 1986, comme un établissement public de l'Etat à caractère administratif  ; qu'il en résulte que les agents publics  de la mense épiscopale sont régis par les dispositions de ce décret et que le pouvoir disciplinaire de l'évêque s'exerce dans ce cadre ; <br/>
<br/>
              5. Considérant qu'il appartient au juge de l'excès de pouvoir, saisi de moyens en ce sens, de rechercher si les faits reprochés à un agent public ayant fait l'objet d'une sanction disciplinaire constituent des fautes de nature à justifier une sanction et si la sanction retenue est proportionnée à la gravité de ces fautes ; que la constatation et la caractérisation des faits reprochés à l'agent relèvent, dès lors qu'elles sont exemptes de dénaturation, du pouvoir souverain des juges du fond ; que le caractère fautif de ces faits est susceptible de faire l'objet d'un contrôle de qualification juridique de la part du juge de cassation ; que l'appréciation du caractère proportionné de la sanction au regard de la gravité des fautes commises relève, pour sa part, de l'appréciation des juges du fond et n'est susceptible d'être remise en cause par le juge de cassation que dans le cas où la solution qu'ils ont retenue quant au choix, par l'administration, de la sanction est hors de proportion avec les fautes commises  ; <br/>
<br/>
              6. Considérant qu'en regardant comme établi que M. A... avait refusé de reconnaître l'autorité hiérarchique du directeur de la bibliothèque diocésaine et d'exécuter les tâches qui lui étaient confiées par ce dernier alors qu'elles correspondaient à sa fiche de poste, ne respectait pas les instructions de son supérieur hiérarchique et rencontrait des difficultés relationnelles, la cour administrative d'appel a porté sur les faits de l'espèce une appréciation souveraine exempte de dénaturation ; qu'elle n'a commis aucune erreur de qualification juridique des faits en jugeant que les agissements imputés à M. A... étaient de nature à justifier une sanction disciplinaire ; que la cour administrative d'appel en a déduit que la mesure de fin de fonctions prononcée à l'encontre de M. A... n'était pas disproportionnée à la gravité des fautes commises ; qu'en portant une telle appréciation, la cour administrative d'appel a retenu une solution quant au choix, par l'évêque de Metz, de la sanction qui n'est pas hors de proportion avec les fautes commises ;<br/>
<br/>
              7. Considérant, en troisième lieu, que le licenciement de M. A... par l'évêque de Metz impliquait l'interruption du versement de son traitement par le ministre de l'intérieur ; que, par suite, M. A... n'est pas fondé à soutenir que la cour administrative d'appel a commis une erreur droit en jugeant que ce ministre était tenu, ainsi qu'il l'a fait par sa décision du 22 février 2012, de mettre fin au versement de son traitement à compter de son licenciement ; que le moyen tiré de ce que le ministre de l'intérieur était tenu de poursuivre le paiement de son traitement jusqu'au terme de son congé de maladie est nouveau en cassation et est, par suite, inopérant ;<br/>
<br/>
              8. Considérant qu'il résulte de tout ce qui précède que M. A... n'est pas fondé à demander l'annulation de l'arrêt, qui est suffisamment motivé, qu'il l'attaque ;<br/>
<br/>
              9. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de M. A... le versement à la mense épiscopale de Metz de la somme qu'elle demande au titre des dispositions de l'article L. 761-1 du code de justice administrative ; que ces dispositions font obstacle à ce que soit mise à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante, la somme que M. A... demande sur leur fondement.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. A... est rejeté.<br/>
Article 2 : Les conclusions présentées par la mense épiscopale de Metz au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à M. A..., à la mense épiscopale de Metz et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">06-04 ALSACE-MOSELLE. ENSEIGNEMENT ET CULTES. - AGENTS DES MENSES ÉPISCOPALES - AGENTS PUBLICS RÉGIS PAR LE DÉCRET N° 86-83 DU 17 JANVIER 1986.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">21-04 CULTES. RÉGIME CONCORDATAIRE D'ALSACE-MOSELLE. - AGENTS DES MENSES ÉPISCOPALES - AGENTS PUBLICS RÉGIS PAR LE DÉCRET N° 86-83 DU 17 JANVIER 1986.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">36-12-01 FONCTIONNAIRES ET AGENTS PUBLICS. AGENTS CONTRACTUELS ET TEMPORAIRES. NATURE DU CONTRAT. - AGENTS DES MENSES ÉPISCOPALES D'ALSACE ET DE MOSELLE - AGENTS PUBLICS RÉGIS PAR LE DÉCRET N° 86-83 DU 17 JANVIER 1986.
</SCT>
<ANA ID="9A"> 06-04 Les agents publics rémunérés par l'Etat et employés par les menses épiscopales sont recrutés par l'évêque et chargés d'administrer, sous sa direction, les biens du diocèse. Eu égard à leur condition de recrutement et d'emploi, sous l'autorité de l'évêque, ces agents ne sont pas des fonctionnaires. En revanche, la mense épiscopale, qui a le statut d'établissement public du culte en vertu du droit local maintenu en vigueur, doit être regardée, pour l'application du décret n° 86-83 du 17 janvier 1986 relatif aux dispositions applicables aux agents non titulaires de l'Etat pris pour l'application de l'article 7 de la loi n° 84-16 du 11 janvier 1984 portant dispositions statutaires relatives à la fonction publique de l'Etat, comme un établissement public de l'Etat à caractère administratif. Il en résulte que les agents publics de la mense épiscopale sont régis par les dispositions de ce décret et que le pouvoir disciplinaire de l'évêque s'exerce dans ce cadre.</ANA>
<ANA ID="9B"> 21-04 Les agents publics rémunérés par l'Etat et employés par les menses épiscopales sont recrutés par l'évêque et chargés d'administrer, sous sa direction, les biens du diocèse. Eu égard à leur condition de recrutement et d'emploi, sous l'autorité de l'évêque, ces agents ne sont pas des fonctionnaires. En revanche, la mense épiscopale, qui a le statut d'établissement public du culte en vertu du droit local maintenu en vigueur, doit être regardée, pour l'application du décret n° 86-83 du 17 janvier 1986 relatif aux dispositions applicables aux agents non titulaires de l'Etat pris pour l'application de l'article 7 de la loi n° 84-16 du 11 janvier 1984 portant dispositions statutaires relatives à la fonction publique de l'Etat, comme un établissement public de l'Etat à caractère administratif. Il en résulte que les agents publics de la mense épiscopale sont régis par les dispositions de ce décret et que le pouvoir disciplinaire de l'évêque s'exerce dans ce cadre.</ANA>
<ANA ID="9C"> 36-12-01 Les agents publics rémunérés par l'Etat et employés par les menses épiscopales sont recrutés par l'évêque et chargés d'administrer, sous sa direction, les biens du diocèse. Eu égard à leur condition de recrutement et d'emploi, sous l'autorité de l'évêque, ces agents ne sont pas des fonctionnaires. En revanche, la mense épiscopale, qui a le statut d'établissement public du culte en vertu du droit local maintenu en vigueur, doit être regardée, pour l'application du décret n° 86-83 du 17 janvier 1986 relatif aux dispositions applicables aux agents non titulaires de l'Etat pris pour l'application de l'article 7 de la loi n° 84-16 du 11 janvier 1984 portant dispositions statutaires relatives à la fonction publique de l'Etat, comme un établissement public de l'Etat à caractère administratif. Il en résulte que les agents publics de la mense épiscopale sont régis par les dispositions de ce décret et que le pouvoir disciplinaire de l'évêque s'exerce dans ce cadre.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
