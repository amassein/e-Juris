<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044271056</ID>
<ANCIEN_ID>JG_L_2021_10_000000441708</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/27/10/CETATEXT000044271056.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 28/10/2021, 441708</TITRE>
<DATE_DEC>2021-10-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>441708</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>RIDOUX</AVOCATS>
<RAPPORTEUR>M. Paul Bernard</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie Roussel</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:441708.20211028</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. C... L... a demandé au tribunal administratif de Nantes d'annuler pour excès de pouvoir l'arrêté du 29 juin 2018 par lequel le préfet de la Loire-Atlantique a refusé de lui délivrer un titre de séjour, lui a fait obligation de quitter le territoire français dans un délai de trente jours et a fixé le pays de destination. <br/>
<br/>
              Par un jugement n° 1812417 du 24 mai 2019, le tribunal administratif a annulé cet arrêté et a enjoint au préfet de délivrer à M. L... un certificat de résidence portant la mention " vie privée et familiale " dans le délai de deux mois à compter de la notification du jugement. <br/>
<br/>
              Par un arrêt n° 19NT02135 du 29 juillet 2019, la cour administrative d'appel de Nantes a annulé ce jugement et rejeté la demande d'annulation présentée par M. L... devant le tribunal administratif. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un nouveau mémoire, enregistrés les 8 juillet et 8 octobre 2020 et le 4 octobre 2021 au secrétariat du contentieux du Conseil d'Etat, M. L... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros à verser à Me Ridoux, son avocat, au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - l'accord franco-algérien du 27 décembre 1968 relatif à la circulation, à l'emploi et au séjour des ressortissants algériens et de leurs familles ;<br/>
              - la convention internationale relative aux droits de l'enfant signée à New York le 26 janvier 1990 ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Paul Bernard, maître des requêtes, <br/>
<br/>
              - les conclusions de Mme Sophie Roussel, rapporteure publique,<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à Me Ridoux, avocat de M. L... ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. C... L..., ressortissant algérien, a présenté une demande de renouvellement du certificat de résidence dont il bénéficiait en qualité de parent d'enfant français. Par un arrêté du 29 juin 2018, le préfet de la Loire-Atlantique a rejeté sa demande au motif de la menace à l'ordre public que constitue sa présence en France, lui a fait obligation de quitter le territoire français dans un délai de trente jours et a désigné le pays de destination. Par un jugement du 24 mai 2019, le tribunal administratif de Nantes a annulé cet arrêté et enjoint au préfet de la Loire-Atlantique de délivrer à M. L... un certificat de résidence portant la mention " vie privée et familiale ". Par un arrêt du 19 décembre 2019, contre lequel M. L... se pourvoit en cassation, la cour administrative d'appel de Nantes a annulé ce jugement et rejeté la demande tendant à l'annulation pour excès de pouvoir de l'arrêté du 29 juin 2018. <br/>
<br/>
              2. D'une part, aux termes de l'article 6 de l'accord franco-algérien du 27 décembre 1968 : " (...) Le certificat de résidence d'un an portant la mention "vie privée et familiale est délivré de plein droit : (...) 4. Au ressortissant algérien ascendant direct d'un enfant français mineur résidant en France, à la condition qu'il exerce même partiellement l'autorité parentale à l'égard de cet enfant ou qu'il subvienne effectivement à ses besoins. (...) ". Ces stipulations ne privent pas l'autorité compétente du pouvoir qui lui appartient de refuser à un ressortissant algérien la délivrance du certificat de résidence d'un an lorsque sa présence en France constitue une menace pour l'ordre public.<br/>
<br/>
              3. D'autre part, aux termes de l'article L. 312-1 du code de l'entrée et du séjour des étrangers et du droit d'asile, désormais repris à l'article L. 432-14 : " Dans chaque département, est instituée une commission du titre de séjour (...) ". Aux termes de l'article L. 312-2 du même code, devenu l'article L. 432-13 : " La commission est saisie par l'autorité administrative lorsque celle-ci envisage de refuser de délivrer ou de renouveler une carte de séjour temporaire à un étranger mentionné à l'article L. 313-11 (...) ". Ces dispositions s'appliquent aux ressortissants algériens dont la situation est examinée sur le fondement du 4 de l'article 6 de l'accord franco-algérien régissant, comme celles, de portée équivalente en dépit des différences tenant au détail des conditions requises, du 6° de l'article L. 313-11 du code de l'entrée et du séjour des étrangers et du droit d'asile alors en vigueur, la délivrance de plein droit du titre de séjour portant la mention " vie privée et familiale " aux parents d'un enfant français mineur résidant en France. Si le préfet n'est tenu de saisir la commission que du cas des seuls étrangers qui remplissent effectivement les conditions prévues par ces textes auxquels il envisage de refuser le titre de séjour sollicité et non de celui de tous les étrangers qui s'en prévalent, la circonstance que la présence de l'étranger constituerait une menace à l'ordre public ne le dispense pas de son obligation de saisine de la commission.<br/>
<br/>
              4. Pour rejeter la demande présentée par M. L... tendant à l'annulation pour excès de pouvoir de l'arrêté du 29 juin 2018, la cour administrative d'appel de Nantes a jugé que le préfet de la Loire-Atlantique avait pu légalement se fonder sur la menace à l'ordre public que constituait la présence en France de M. L... pour refuser le renouvellement du certificat de résidence dont l'intéressé bénéficiait en qualité de parent d'enfant français. Toutefois, en en déduisant, sans rechercher si l'intéressé remplissait effectivement les conditions de l'article 6 de l'accord franco-algérien du 27 décembre 1968, que le préfet n'était pas tenu de soumettre le cas de M. L... à la commission du titre de séjour, alors qu'une telle circonstance n'était pas de nature à y faire obstacle, la cour a commis une erreur de droit. <br/>
<br/>
              5. Il résulte de ce qui précède que, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, M. L... est fondé à demander l'annulation de l'arrêt qu'il attaque. <br/>
<br/>
              6. M. L... a obtenu le bénéfice de l'aide juridictionnelle. Par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. Il y a lieu, dans les circonstances de l'espèce, et sous réserve qu'il renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à Me Ridoux. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 19 décembre 2019 de la cour administrative d'appel de Nantes est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nantes. <br/>
Article 3 : L'Etat versera à Me Ridoux, avocat de M. L..., une somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991, sous réserve qu'il renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
Article 4 : La présente décision sera notifiée à M. C... L... et au ministre de l'intérieur.<br/>
              Délibéré à l'issue de la séance du 8 octobre 2021 où siégeaient : M. Jacques-Henri Stahl, président adjoint de la section du contentieux, présidant ; M. H... J..., M. Olivier Japiot, présidents de chambre ; Mme A... K..., M. D... G..., M. E... N..., M. F... M..., M. Jean-Yves Ollier, conseillers d'Etat et M. Paul Bernard, maître des requêtes-rapporteur. <br/>
<br/>
              Rendu le 28 octobre 2021. <br/>
<br/>
<br/>
                 Le Président : <br/>
                 Signé : M. Jacques-Henri Stahl<br/>
 		Le rapporteur : <br/>
      Signé : M. Paul Bernard<br/>
                 La secrétaire :<br/>
                 Signé : Mme I... B...<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">335-01-01-02 ÉTRANGERS. - SÉJOUR DES ÉTRANGERS. - TEXTES APPLICABLES. - CONVENTIONS INTERNATIONALES. - ACCORD FRANCO-ALGÉRIEN DU 27 DÉCEMBRE 1968 - DEMANDE DE CERTIFICAT DE RÉSIDENCE D'UN AN (ART. 6) - REFUS ENVISAGÉ EN APPLICATION DE LA RÉSERVE D'ORDRE PUBLIC [RJ1] - OBLIGATION POUR LE PRÉFET DE SAISIR AU PRÉALABLE LA COMMISSION DU TITRE DE SÉJOUR LORSQUE LE DEMANDEUR REMPLIT LES CONDITIONS PRÉVUES PAR LES TEXTES [RJ2] - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">335-01-03-02 ÉTRANGERS. - SÉJOUR DES ÉTRANGERS. - REFUS DE SÉJOUR. - PROCÉDURE. - CERTIFICAT DE RÉSIDENCE D'UN AN DEMANDÉ SUR LE FONDEMENT DE L'ARTICLE 6 DE L'ACCORD FRANCO-ALGÉRIEN DU 27 DÉCEMBRE 1968 - REFUS ENVISAGÉ EN APPLICATION DE LA RÉSERVE D'ORDRE PUBLIC [RJ1] - OBLIGATION DU PRÉFET DE SAISIR AU PRÉALABLE LA COMMISSION DU TITRE DE SÉJOUR LORSQUE LE DEMANDEUR REMPLIT LES CONDITIONS PRÉVUES PAR LES TEXTES [RJ2] - EXISTENCE.
</SCT>
<ANA ID="9A"> 335-01-01-02 L'article 6 de l'accord franco-algérien du 27 décembre 1968 ne prive pas l'autorité compétente du pouvoir qui lui appartient de refuser à un ressortissant algérien la délivrance du certificat de résidence d'un an lorsque sa présence en France constitue une menace pour l'ordre public.......Les articles L. 312-1 et L. 312-2 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA), devenus les articles L. 432-13 et L. 432-14 de ce code, s'appliquent aux ressortissants algériens dont la situation est examinée sur le fondement du 4 de l'article 6 de l'accord franco-algérien régissant, comme le 6° de l'article L. 313-11 du CESEDA alors en vigueur, de portée équivalente en dépit des différences tenant au détail des conditions requises, la délivrance de plein droit du titre de séjour portant la mention vie privée et familiale aux parents d'un enfant français mineur résidant en France. Si le préfet n'est tenu de saisir la commission du titre de séjour que du cas des seuls étrangers qui remplissent effectivement les conditions prévues par ces textes auxquels il envisage de refuser le titre de séjour sollicité et non de celui de tous les étrangers qui s'en prévalent, la circonstance que la présence de l'étranger constituerait une menace à l'ordre public ne le dispense pas de son obligation de saisine de la commission.</ANA>
<ANA ID="9B"> 335-01-03-02 L'article 6 de l'accord franco-algérien du 27 décembre 1968 ne prive pas l'autorité compétente du pouvoir qui lui appartient de refuser à un ressortissant algérien la délivrance du certificat de résidence d'un an lorsque sa présence en France constitue une menace pour l'ordre public.......Les articles L. 312-1 et L. 312-2 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA), devenus les articles L. 432-13 et L. 432-14 de ce code, s'appliquent aux ressortissants algériens dont la situation est examinée sur le fondement du 4 de l'article 6 de l'accord franco-algérien régissant, comme le 6° de l'article L. 313-11 du CESEDA alors en vigueur, de portée équivalente en dépit des différences tenant au détail des conditions requises, la délivrance de plein droit du titre de séjour portant la mention vie privée et familiale aux parents d'un enfant français mineur résidant en France. Si le préfet n'est tenu de saisir la commission du titre de séjour que du cas des seuls étrangers qui remplissent effectivement les conditions prévues par ces textes auxquels il envisage de refuser le titre de séjour sollicité et non de celui de tous les étrangers qui s'en prévalent, la circonstance que la présence de l'étranger constituerait une menace à l'ordre public ne le dispense pas de son obligation de saisine de la commission.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., sur l'applicabilité de la réserve d'ordre public aux ressortissants algériens, CE, Ass., 29 juin 1990, Groupe d'information et de soutien des travailleurs immigrés (GISTI), n° 78519, p. 171 ; CE, 11 juillet 2018, M. Boukenna, n° 409090, T. pp. 715-716...[RJ2] Cf., qui réserve l'obligation de saisir la commission au cas où l'intéressé satisfait aux conditions posées par les textes, CE, Section, 27 mai 1994, Oncul, n° 118879, p. 268.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
