<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032103879</ID>
<ANCIEN_ID>JG_L_2016_02_000000378257</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/10/38/CETATEXT000032103879.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère / 6ème SSR, 24/02/2016, 378257</TITRE>
<DATE_DEC>2016-02-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>378257</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère / 6ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>BALAT</AVOCATS>
<RAPPORTEUR>M. Marc Thoumelou</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Jean Lessi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2016:378257.20160224</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B...A...a demandé au tribunal administratif de Melun de condamner Pôle emploi à lui verser une somme de 3 000 euros, assortie des intérêts au taux légal, en réparation du préjudice qu'elle estime avoir subi du fait de la mesure de radiation de la liste des demandeurs d'emploi dont elle a fait l'objet en 2011. Par un jugement n° 1206896 du 10 février 2014, le magistrat désigné par le président du tribunal administratif de Melun a rejeté sa demande.<br/>
<br/>
              Par une ordonnance n° 14PA01013 du 16 avril 2014, enregistrée le 22 avril 2014 au secrétariat du contentieux du Conseil d'Etat, le président de la cour administrative d'appel de Paris a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, le pourvoi, enregistré le 6 mars 2014 au greffe de cette cour, présenté par MmeA.... Par ce pourvoi et par un mémoire complémentaire, enregistré le 17 juillet 2014 au secrétariat du contentieux du Conseil d'Etat, Mme A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement du tribunal administratif de Melun du 10 février 2014 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de Pôle emploi la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code du travail ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Marc Thoumelou, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Jean Lessi, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Balat, avocat de Mme A...;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis au juge du fond que, par une décision du 12 octobre 2011, confirmée le 25 octobre 2011 à la suite du recours administratif préalable obligatoire formé par l'intéressée, le directeur de l'agence de Pôle emploi de Saint-Maur-des-Fossés a radié Mme A...de la liste des demandeurs d'emploi pour deux mois à compter du 13 septembre 2011, au motif qu'elle ne s'était pas présentée, à cette date, à l'entretien de suivi mensuel auquel elle avait été convoquée dans le cadre de son projet personnalisé d'accès à l'emploi ; que, le 30 janvier 2012, le même directeur a retiré cette décision ; que Mme A...a demandé au tribunal administratif de Melun de condamner Pôle emploi à l'indemniser des préjudices qu'elle estime avoir subis du fait de sa radiation ;<br/>
<br/>
              2. Considérant que le 3° de l'article L. 5412-1 du code du travail prévoit notamment la radiation de la liste des demandeurs d'emploi de la personne qui, " sans motif légitime : / a) Refuse d'élaborer ou d'actualiser le projet personnalisé d'accès à l'emploi (...) ; / b) Refuse de suivre une action de formation ou d'aide à la recherche d'emploi proposée par l'un des services ou organismes mentionnés à l'article L. 5311-2 et s'inscrivant dans le cadre du projet personnalisé d'accès à l'emploi ; / c) Refuse de répondre à toute convocation des services et organismes mentionnés à l'article L. 5311-2 ou mandatés par ces services et organismes (...) " ; que l'article R. 5412-1 du même code donne compétence au directeur régional de Pôle emploi pour procéder à cette radiation, dont la durée, selon l'article R. 5412-5 de ce code, est de quinze jours lorsque sont constatés pour la première fois les manquements mentionnés au b du 3° de l'article L. 5412-1, pouvant, en cas de manquements répétés, être portée à une durée comprise entre un et six mois consécutifs, et de deux mois lorsque sont constatés pour la première fois les manquements mentionnés aux a et c du même 3°, pouvant, en cas de manquements répétés, être portée à une durée comprise entre deux et six mois consécutifs ;<br/>
<br/>
              3. Considérant qu'il résulte de ces dispositions que la radiation d'une personne de la liste des demandeurs d'emploi prononcée sur le fondement du 3° de l'article L. 5412-1 du code du travail a le caractère d'une sanction que l'administration inflige à un administré ; que, par suite, elle ne peut légalement prendre effet avant la notification à l'intéressé de la décision initiale par laquelle le directeur régional de Pôle emploi la prononce ; <br/>
<br/>
              4. Considérant, dès lors, qu'en jugeant que la radiation de Mme A..., prononcée le 12 octobre 2011 avec effet à compter du 13 septembre 2011, n'était entachée d'aucune rétroactivité illégale au motif qu'elle présentait le caractère d'une simple mesure recognitive, le tribunal a commis une erreur de droit ; <br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, Mme A...est fondée à demander l'annulation du jugement qu'elle attaque ;<br/>
<br/>
              6. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de Pôle emploi une somme de 3 000 euros à verser à Mme A... au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Melun du 10 février 2014 est annulé.<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Melun.<br/>
Article 3 : Pôle emploi versera une somme de 3 000 euros à Mme A...au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à Mme B...A...et à Pôle emploi.<br/>
Copie en sera adressée à la ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">17-05-012 COMPÉTENCE. COMPÉTENCE À L'INTÉRIEUR DE LA JURIDICTION ADMINISTRATIVE. COMPÉTENCE EN PREMIER ET DERNIER RESSORT DES TRIBUNAUX ADMINISTRATIFS. - INCLUSION - LITIGE RELATIF À UNE RADIATION DE LA LISTE DES DEMANDEURS D'EMPLOI, Y COMPRIS S'AGISSANT DE CONCLUSIONS INDEMNITAIRES (1° DE L'ART. R. 811-1 DU CJA) (SOL.IMPL.).
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">59-02-02 RÉPRESSION. DOMAINE DE LA RÉPRESSION ADMINISTRATIVE RÉGIME DE LA SANCTION ADMINISTRATIVE. - RADIATION DE LA LISTE DES DEMANDEURS D'EMPLOI - NATURE DE SANCTION - EXISTENCE [RJ1] - CONSÉQUENCE - PRISE D'EFFET À DATE DE LA NOTIFICATION.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">66-10-02 TRAVAIL ET EMPLOI. POLITIQUES DE L'EMPLOI. INDEMNISATION DES TRAVAILLEURS PRIVÉS D'EMPLOI. - RADIATION DE LA LISTE DES DEMANDEURS D'EMPLOI - NATURE DE SANCTION - EXISTENCE [RJ1] - CONSÉQUENCE - PRISE D'EFFET À DATE DE LA NOTIFICATION.
</SCT>
<ANA ID="9A"> 17-05-012 Les tribunaux administratifs connaissent en premier et dernier ressort des litiges relatifs à une radiation de la liste des demandeurs d'emploi, y compris s'agissant de conclusions indemnitaires, en application du 1° de l'article R. 811-1 du code de justice administrative (CJA).</ANA>
<ANA ID="9B"> 59-02-02 La radiation d'une personne de la liste des demandeurs d'emploi prononcée sur le fondement du 3° de l'article L. 5412-1 du code du travail a le caractère d'une sanction que l'administration inflige à un administré. Par suite, elle ne peut légalement prendre effet avant la notification à l'intéressé de la décision initiale par laquelle le directeur régional de Pôle emploi la prononce.</ANA>
<ANA ID="9C"> 66-10-02 La radiation d'une personne de la liste des demandeurs d'emploi prononcée sur le fondement du 3° de l'article L. 5412-1 du code du travail a le caractère d'une sanction que l'administration inflige à un administré. Par suite, elle ne peut légalement prendre effet avant la notification à l'intéressé de la décision initiale par laquelle le directeur régional de Pôle emploi la prononce.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Cf. décision du même jour, M.,, n° 382688, à mentionner aux Tables. Rappr., s'agissant de l'exclusion du revenu de remplacement, CE, 23 février 2011, M.,, n° 332837, T. p. 760.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
