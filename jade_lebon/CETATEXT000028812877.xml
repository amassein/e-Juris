<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028812877</ID>
<ANCIEN_ID>JG_L_2014_03_000000362135</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/81/28/CETATEXT000028812877.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème et 5ème sous-sections réunies, 31/03/2014, 362135</TITRE>
<DATE_DEC>2014-03-31</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>362135</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème et 5ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BORE, SALVE DE BRUNETON ; SCP BARTHELEMY, MATUCHANSKY, VEXLIARD</AVOCATS>
<RAPPORTEUR>M. Christophe Eoche-Duval</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Gaëlle Dumortier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:362135.20140331</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 23 août 2012 au secrétariat du contentieux du Conseil d'Etat, présenté pour M. A...D..., demeurant ... ; M. D...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision n° 11203 du 21 juin 2012 par laquelle la chambre disciplinaire nationale de l'ordre des médecins a annulé la décision du 13 janvier 2011 de la chambre disciplinaire de première instance de l'ordre des médecins de Rhône-Alpes rejetant la plainte de M. et MmeC..., lui a infligé la sanction de l'interdiction d'exercer la médecine pendant deux mois, assortie d'un sursis d'un mois, et a décidé que cette sanction sera exécutée du 1er au 31 octobre 2012 inclus ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de M. et Mme C...une somme de 3 000 euros en application de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de la santé publique ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Christophe Eoche-Duval, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boré, Salve de Bruneton, avocat de M. D...et à la SCP Barthélemy, Matuchansky, Vexliard, avocat du Conseil national de l'ordre des médecins ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes du premier alinéa de l'article L. 4124-2 du code de la santé publique : " Les médecins (...) chargés d'un service public et inscrits au tableau de l'ordre ne peuvent être traduits devant la chambre disciplinaire de première instance, à l'occasion des actes de leur fonction publique, que par le ministre chargé de la santé, le représentant de l'Etat dans le département, le directeur général de l'agence régionale de santé, le procureur de la République, le conseil national ou le conseil départemental au tableau duquel le praticien est inscrit. " ; <br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis au juge du fond que M.D..., médecin du service départemental d'incendie et de secours, est intervenu dans le cadre d'une opération de secours au domicile d'une personne qui n'a pu être réanimée ; qu'un médecin du SAMU, également appelé lors de cette intervention, a établi et signé un certificat de décès comportant la mention " obstacle médico-légal " ; qu'ayant dû quitter les lieux, il a laissé le soin à son confrère de le compléter ; que celui-ci a établi et signé un nouveau certificat ne comportant plus la mention portée par son confrère, sans lui en avoir référé ; que la suppression de cette mention a eu pour conséquence de faire disparaître l'obligation d'informer le procureur de la République et la possibilité de réaliser une autopsie permettant aux parents de la victime de connaître la cause du décès ; que la chambre disciplinaire de première instance de l'ordre des médecins de Rhône-Alpes a rejeté la plainte de M. et MmeC..., parents de la personne décédée, transmise par le conseil départemental sans s'y associer, comme n'étant pas présentée par une des autorités habilitées à poursuivre un médecin chargé d'une mission de service public à l'occasion des actes non détachables commis dans leur fonction publique ; que, par la décision attaquée, la chambre disciplinaire nationale de l'ordre des médecins a, au contraire, jugé la plainte recevable et a infligé une sanction à M. D...;<br/>
<br/>
              3. Considérant que, M. D...étant intervenu en sa qualité de médecin du service départemental d'incendie et de secours dans le cadre d'une intervention d'urgence requise de ce service, l'acte qui lui est reproché, quelle qu'en soit la gravité, a été commis dans l'exercice de ses fonctions publiques, au sens des dispositions de l'article L. 4124-2 du code de la santé publique ; que, par suite, la chambre disciplinaire nationale a commis une erreur de droit en jugeant recevable une plainte qui n'émanait pas d'une des autorités mentionnées par les dispositions précitées du code de la santé publique ; que, dès lors, sa décision doit être annulée ;<br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              5. Considérant qu'il résulte de ce qui vient être dit que M. et Mme  C...ne sont pas fondés à soutenir que c'est à tort que la chambre disciplinaire de première instance de l'ordre des médecins de Rhône-Alpes a rejeté leur plainte comme irrecevable ; <br/>
<br/>
              6. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de M. D...qui n'est pas, dans la présente instance, la partie perdante ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par M. D...au titre des ces mêmes dispositions ;<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : La décision de la chambre disciplinaire nationale de l'ordre des médecins du 21 juin 2012 est annulée.<br/>
Article 2 : La requête présentée par M. et Mme C...devant la chambre disciplinaire nationale de l'ordre des médecins est rejetée.<br/>
Article 3 : Le surplus des conclusions du pourvoi de M. D...est rejeté.<br/>
Article 4 : La présente décision sera notifiée à M. A...D...et au Conseil national de l'ordre des médecins. <br/>
Copie en sera adressé à M. et Mme B...C....<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">55-04-01-01 PROFESSIONS, CHARGES ET OFFICES. DISCIPLINE PROFESSIONNELLE. PROCÉDURE DEVANT LES JURIDICTIONS ORDINALES. INTRODUCTION DE L'INSTANCE. - RECEVABILITÉ - PLAINTE PORTANT SUR DES MANQUEMENTS COMMIS PAR UN PRATICIEN HOSPITALIER DANS L'EXERCICE DE SES FONCTIONS PUBLIQUES - RECEVABILITÉ LIMITÉE AUX PLAINTES INTRODUITES PAR LES PERSONNES VISÉES À L'ARTICLE L. 4124-2 DU CSP - INCIDENCE, SUR LA QUALIFICATION DE MANQUEMENT COMMIS DANS L'EXERCICE DES FONCTIONS PUBLIQUES AU SENS DE CET ARTICLE, DE LA GRAVITÉ DE CE MANQUEMENT - ABSENCE.
</SCT>
<ANA ID="9A"> 55-04-01-01 Dès lors qu'un praticien est intervenu dans le cadre d'une intervention requise de ce service, l'acte qui lui est reproché, quelle qu'en soit la gravité, a été commis dans l'exercice de ses fonctions publiques, au sens des dispositions de l'article L. 4124-2 du code de la santé publique.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
