<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026856826</ID>
<ANCIEN_ID>JG_L_2012_12_000000349059</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/85/68/CETATEXT000026856826.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème SSR, 28/12/2012, 349059</TITRE>
<DATE_DEC>2012-12-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>349059</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BORE ET SALVE DE BRUNETON</AVOCATS>
<RAPPORTEUR>M. Maxime Boutron</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2012:349059.20121228</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 6 mai et 2 août 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour l'association U Levante, dont le siège est situé RN 193, E Muchjelline à Corte (20250) ; l'association demande au Conseil d'Etat : <br/>
<br/>
               1°) d'annuler l'article 3 de l'arrêt n° 09MA00437 du 3 mars 2011 par lequel la cour administrative d'appel de Marseille, après avoir annulé pour excès de pouvoir l'arrêté n° 07-1333 du 19 septembre 2007 du préfet de la Corse-du-Sud portant transfert de la servitude de passage des piétons sur le littoral de la commune de Pianottoli-Caldarello, de Cala di Fornellu à la tour de Caldarello en tant qu'il suspendait la servitude de passage existant sur les parcelles D 65 et D 66 dans l'anse de Chevanu et réformé le jugement n°s 0800498-0800921 du 4 décembre 2008 du tribunal administratif de Bastia en ce qu'il avait de contraire, a rejeté le surplus des conclusions de sa requête tendant à l'annulation de l'intégralité de cet arrêté ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit intégralement à son appel ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'urbanisme ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Maxime Boutron, Auditeur, <br/>
<br/>
              - les observations de la SCP Boré et Salve de Bruneton, avocat de l'Association U Levante,<br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Boré et Salve de Bruneton, avocat de l'Association U Levante ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par arrêté n° 07-1333 du 19 septembre 2007 portant transfert de la servitude de passage des piétons sur le littoral de la commune de Pianottoli-Caldarello, de Cala di Fornellu à la tour de Caldarello, le préfet de la Corse-du-Sud a, au vu de l'avis favorable du commissaire enquêteur en date du 14 mai 2007, transféré cette servitude à l'intérieur de certaines parcelles, et notamment des parcelles cadastrées D 755 et D 756, et l'a suspendue pour d'autres parcelles ; que, par jugement du 4 décembre 2008, le tribunal administratif de Bastia a partiellement fait droit aux conclusions de l'association U Levante tendant à l'annulation pour excès de pouvoir de cet arrêté ; que la cour administrative d'appel de Marseille, après avoir partiellement annulé d'autres dispositions de cet arrêté et réformé le jugement en ce qu'il avait de contraire, a, par l'article 3 de l'arrêt du 3 mars 2011 contre lequel l'association se pourvoit en cassation, rejeté le surplus des conclusions de sa requête ;<br/>
<br/>
              2. Considérant, en premier lieu, qu'aux termes de l'article de L. 160-6 du code de l'urbanisme : " Les propriétés privées riveraines du domaine public maritime sont grevées sur une bande de trois mètres de largeur d'une servitude destinée à assurer exclusivement le passage des piétons. / L'autorité administrative peut, par décision motivée prise après avis du ou des conseils municipaux intéressés et au vu du résultat d'une enquête publique effectuée comme en matière d'expropriation : / a) Modifier le tracé ou les caractéristiques de la servitude, afin, d'une part, d'assurer, compte tenu notamment de la présence d'obstacles de toute nature, la continuité du cheminement des piétons ou leur libre accès au rivage de la mer, d'autre part, de tenir compte des chemins ou règles locales préexistants ; le tracé modifié peut grever exceptionnellement des propriétés non riveraines du domaine public maritime ; / b) A titre exceptionnel, la suspendre. / Sauf dans le cas où l'institution de la servitude est le seul moyen d'assurer la continuité du cheminement des piétons ou leur libre accès au rivage de la mer, la servitude instituée aux alinéas 1 et 2 ci-dessus ne peut grever les terrains situés à moins de quinze mètres des bâtiments à usage d'habitation édifiés avant le 1er janvier 1976 (...) " ; qu'aux termes de l'article R. 160-18 du même code : " Le commissaire enquêteur ou la commission d'enquête peut décider de procéder à une visite des lieux. Dans ce cas, le commissaire enquêteur ou le président de la commission avise le maire et convoque sur place les propriétaires intéressés ainsi que les représentants des administrations ; après les avoir entendus, il dresse procès-verbal de la réunion " ; qu'aux termes de l'article R. 160-19 du même code : " Si le commissaire enquêteur ou la commission d'enquête propose de rectifier le tracé ou les caractéristiques de la servitude qui ont été soumis à enquête, et si ces rectifications tendent à appliquer la servitude à de nouveaux terrains, les propriétaires de ces terrains en sont avisés par lettre. Un avis au public est, en outre, affiché à la mairie. Un délai de quinze jours au moins, en sus de celui fixé par l'arrêté prescrivant l'ouverture de l'enquête, est accordé à toute personne intéressée pour prendre connaissance à la mairie des rectifications proposées et présenter des observations. (...) " ;<br/>
<br/>
              3. Considérant qu'il résulte de la combinaison de ces dispositions que les propriétaires intéressés mentionnés à l'article R. 160-18 du code de l'urbanisme, devant être convoqués sur place par le commissaire enquêteur lorsque celui-ci décide de procéder à une visite des lieux, s'entendent des seuls propriétaires des parcelles concernées soit par les modifications envisagées par l'autorité administrative du tracé ou des caractéristiques de la servitude soit par la suspension de celle-ci ;   <br/>
<br/>
              4. Considérant que la cour a, par une appréciation souveraine, relevé que tous les propriétaires des parcelles concernées par les modifications projetées avaient été préalablement avertis des visites effectuées sur leurs parcelles par le commissaire-enquêteur ; qu'en jugeant, pour écarter le moyen tiré de l'irrégularité de la procédure d'enquête publique, que la circonstance que tous les propriétaires des terrains du littoral emprunté par la servitude de passage n'avaient pas été convoqués était sans incidence sur le respect de la procédure prévue par les dispositions précitées, quand bien même le commissaire-enquêteur s'était rendu sur leurs parcelles au cours de ses opérations de visite générale du site, la cour, qui n'a pas dénaturé les écritures de la requérante, n'a pas commis d'erreur de droit ;  <br/>
<br/>
              5. Considérant, en second lieu, que la cour a jugé qu'en assurant la continuité du cheminement des piétons par le transfert du tracé de la servitude de passage existante au droit des parcelles D 755 et D 756, le préfet de la Corse-du-Sud avait usé, dans la stricte mesure nécessaire au respect des objectifs fixés par la loi, de la faculté offerte par les dispositions de l'article L. 160-6 du code de l'urbanisme de grever exceptionnellement de la servitude des propriétés non riveraines du domaine public maritime ; qu'elle a estimé, d'une part, qu'il ressortait des pièces du dossier, et notamment des éléments contenus dans le rapport du commissaire-enquêteur et des observations de celui-ci, que la présence d'un amas rocheux, empêchant l'accès au littoral, rendait nécessaire, contrairement à ce que soutenait l'association requérante, le passage du tracé de la servitude à l'intérieur de la parcelle D 755 ; qu'en statuant ainsi, la cour n'a pas dénaturé ces pièces et a suffisamment répondu au moyen dont elle était saisie ;<br/>
<br/>
              6. Considérant que la cour a, d'autre part, relevé que la maison d'habitation présente sur cette parcelle avait été construite avant le 1er janvier 1976 et que le commissaire-enquêteur et la direction départementale de l'équipement indiquaient, dans leurs rapports respectifs, que la distance mesurée sur le terrain entre cette maison et l'emprise de la servitude faisant l'objet du transfert serait comprise entre 12 et 13 mètres ; qu'en estimant que, si l'association requérante faisait valoir que la distance entre la construction et la limite du domaine public maritime était d'environ 30 mètres, elle ne l'établissait pas par la seule production d'un cliché photogrammétrique de l'Institut géographique national et d'une image satellite,  la cour n'a dénaturé ni les écritures de la requérante, dès lors que, si celle-ci se fondait aussi sur l'étude d'un géomètre-expert, ce rapport avait été élaboré sur la base de ce même cliché, ni les pièces du dossier ;<br/>
<br/>
              7. Considérant qu'il résulte de tout ce qui précède que le pourvoi de l'association U Levante doit être rejeté, y compris ses conclusions présentées sur le fondement de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : Le pourvoi de l'association U Levante est rejeté.<br/>
<br/>
Article 2 : La présente décision sera notifiée à l'association U Levante et à la ministre de l'égalité des territoires et du logement.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">26-04-01-01-03 DROITS CIVILS ET INDIVIDUELS. DROIT DE PROPRIÉTÉ. SERVITUDES. INSTITUTION DES SERVITUDES. SERVITUDES DE PASSAGE SUR LE LITTORAL. - SERVITUDE DE PASSAGE DES PIÉTONS (ART. L. 160-6 DU CODE DE L'URBANISME) - PROCÉDURE DE MODIFICATION OU DE SUSPENSION - CONVOCATION SUR PLACE DES PROPRIÉTAIRES INTÉRESSÉS LORS DE LA VISITE DES LIEUX PAR LE COMMISSAIRE ENQUÊTEUR (ART. R. 160-18 DU CODE DE L'URBANISME) - NOTION DE PROPRIÉTAIRES INTÉRESSÉS - PROPRIÉTAIRES DES PARCELLES CONCERNÉES SOIT PAR LES MODIFICATIONS ENVISAGÉES PAR L'AUTORITÉ ADMINISTRATIVE SOIT PAR LA SUSPENSION DE LA SERVITUDE - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">68-001-01-02-03 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. RÈGLES GÉNÉRALES D'UTILISATION DU SOL. RÈGLES GÉNÉRALES DE L'URBANISME. PRESCRIPTIONS D'AMÉNAGEMENT ET D'URBANISME. RÉGIME ISSU DE LA LOI DU 3 JANVIER 1986 SUR LE LITTORAL. - SERVITUDE DE PASSAGE DES PIÉTONS (ART. L. 160-6 DU CODE DE L'URBANISME) - PROCÉDURE DE MODIFICATION OU DE SUSPENSION - CONVOCATION SUR PLACE DES PROPRIÉTAIRES INTÉRESSÉS LORS DE LA VISITE DES LIEUX PAR LE COMMISSAIRE ENQUÊTEUR (ART. R. 160-18 DU CODE DE L'URBANISME) - NOTION DE PROPRIÉTAIRES INTÉRESSÉS - PROPRIÉTAIRES DES PARCELLES CONCERNÉES SOIT PAR LES MODIFICATIONS ENVISAGÉES PAR L'AUTORITÉ ADMINISTRATIVE SOIT PAR LA SUSPENSION DE LA SERVITUDE - EXISTENCE.
</SCT>
<ANA ID="9A"> 26-04-01-01-03 Il résulte de la combinaison des dispositions des articles L. 160-6, R. 160-18 et R. 160-19 du code de l'urbanisme relatives à la procédure d'enquête publique effectuée en matière de modification ou de suspension d'une servitude de passage sur le littoral que les propriétaires intéressés mentionnés à l'article R. 160-18 du code de l'urbanisme, qui doivent être convoqués sur place par le commissaire enquêteur lorsque celui-ci décide de procéder à une visite des lieux, s'entendent des seuls propriétaires des parcelles concernées soit par les modifications du tracé ou des caractéristiques de la servitude envisagées par l'autorité administrative soit par la suspension de la servitude.</ANA>
<ANA ID="9B"> 68-001-01-02-03 Il résulte de la combinaison des dispositions des articles L. 160-6, R. 160-18 et R. 160-19 du code de l'urbanisme relatives à la procédure d'enquête publique effectuée en matière de modification ou de suspension d'une servitude de passage sur le littoral que les propriétaires intéressés mentionnés à l'article R. 160-18 du code de l'urbanisme, qui doivent être convoqués sur place par le commissaire enquêteur lorsque celui-ci décide de procéder à une visite des lieux, s'entendent des seuls propriétaires des parcelles concernées soit par les modifications du tracé ou des caractéristiques de la servitude envisagées par l'autorité administrative soit par la suspension de la servitude.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
