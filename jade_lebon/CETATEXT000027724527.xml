<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027724527</ID>
<ANCIEN_ID>JG_L_2013_07_000000359420</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/72/45/CETATEXT000027724527.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 17/07/2013, 359420</TITRE>
<DATE_DEC>2013-07-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>359420</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>M. Gaël Raimbault</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:359420.20130717</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 15 mai et 16 août 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour la fondation Ellen Poidatz, dont le siège est 1, rue Ellen Poidtaz à Saint-Fargeau-Ponthierry (77310), représentée par son président ; la fondation requérante demande au Conseil d'Etat d'annuler l'ordonnance n° 12PA01017 du 16 mars 2012 par laquelle le président de la huitième chambre de la cour administrative d'appel de Paris a rejeté sa requête tendant, d'une part, à l'annulation de l'ordonnance n° 1201458 du 16 février 2012 par laquelle le président de la première chambre du tribunal administratif de Melun a rejeté sa demande tendant à l'annulation de la décision du 12 décembre 2011 par laquelle l'inspecteur du travail de la septième section d'inspection de l'unité territoriale de Seine-et-Marne a déclaré Mme A...inapte au port de charges de plus de 15 kg et à la manutention de personnes et, d'autre part, à ce que sa requête présentée devant le tribunal soit jugée recevable et l'affaire renvoyée devant le tribunal ou, à titre subsidiaire, à ce que l'affaire soit évoquée et la décision du 12 décembre 2011 annulée ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution, notamment son Préambule ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
      Vu le code de justice administrative ;		<br/>
<br/>
               Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Gaël Raimbault, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
- les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de la fondation Ellen Poidatz ;<br/>
<br/>
<br/>1. Considérant, d'une part, qu'aux termes de l'article R. 411-2 du code de justice administrative : " Lorsque la contribution pour l'aide juridique prévue à l'article 1635 bis Q du code général des impôts est due et n'a pas été acquittée, la requête est irrecevable. / Cette irrecevabilité est susceptible d'être couverte après l'expiration du délai de recours (...) / Par exception au premier alinéa de l'article R. 612-1, la juridiction peut rejeter d'office une requête entachée d'une telle irrecevabilité sans demande de régularisation préalable, lorsque l'obligation d'acquitter la contribution ou, à défaut, de justifier du dépôt d'une demande d'aide juridictionnelle est mentionnée dans la notification de la décision attaquée ou lorsque la requête est introduite par un avocat " ; que, d'autre part, l'article R. 222-1 du même code dispose que : " Les présidents de tribunal administratif et de cour administrative d'appel, le vice-président du tribunal administratif de Paris et les présidents de formation de jugement des tribunaux et des cours peuvent, par ordonnance : / (...) 4° Rejeter les requêtes manifestement irrecevables, lorsque la juridiction n'est pas tenue d'inviter leur auteur à les régulariser ou qu'elles n'ont pas été régularisées à l'expiration du délai imparti par une demande en ce sens (...) " ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par une décision du 12 décembre 2011, notifiée le 15 décembre suivant, l'inspecteur du travail a reconnu une salariée de la fondation Ellen Poidatz inapte au port de charges lourdes et à la manutention de personnes ; que la fondation a demandé l'annulation de cette décision au tribunal administratif de Melun ; que le 16 février 2012, le président de la première chambre de ce tribunal a rejeté cette demande par ordonnance, sur le fondement du 4° de l'article R. 222-1 du code de justice administrative, au motif que, faute que la contribution pour l'aide juridique ait été acquittée, elle était manifestement irrecevable ; que cette ordonnance a été confirmée par une ordonnance du président de la huitième chambre de la cour administrative d'appel de Paris, contre laquelle la fondation se pourvoit en cassation ;<br/>
<br/>
              3. Considérant que, pour rejeter l'appel de la fondation Ellen Poidatz, le président de la huitième chambre de la cour administrative d'appel de Paris s'est borné, après avoir cité les dispositions de l'article R. 411-2 du code de justice administrative, à relever que la demande de la fondation, qui tendait à l'annulation d'une décision notifiée le 15 décembre 2011 et avait été enregistrée au greffe du tribunal administratif de Melun le 11 février 2012, avait été introduite par l'intermédiaire d'un avocat et ne comportait pas la contribution prévue à l'article 1635 bis Q du code général des impôts ; qu'il en a déduit que le président de la première chambre du tribunal administratif de Melun avait à bon droit, par une ordonnance du 16 février 2012, d'office et sans inviter la requérante à régulariser sa demande, rejeté sa demande comme entachée d'une irrecevabilité manifeste ; qu'en adoptant une telle motivation, alors que la fondation soutenait que sa demande ne pouvait être regardée comme irrecevable le 16 février 2012, date à laquelle le délai de recours n'était pas expiré, il n'a pas mis le juge de cassation à même d'exercer son contrôle ; que la fondation requérante est dès lors fondée à soutenir que l'ordonnance est insuffisamment motivée et à en demander l'annulation pour ce motif ; <br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              5. Considérant, en premier lieu, qu'en créant, par l'article 54 de la loi du 29 juillet 2011 de finances rectificative pour 2011, la contribution pour l'aide juridique perçue, sauf exception, pour toute instance introduite en matière civile, commerciale, prud'homale, sociale ou rurale devant une juridiction judiciaire ou introduite devant une juridiction administrative, le législateur a entendu établir une solidarité financière entre les justiciables dans le but d'intérêt général d'assurer le financement de la réforme de la garde à vue résultant de la loi du 14 avril 2011 et, en particulier, le coût résultant, au titre de l'aide juridique, de l'intervention de l'avocat au cours de la garde à vue ; qu'eu égard à l'objet de la contribution et à la généralité de l'obligation posée par la loi fiscale, aucune irrecevabilité résultant du défaut de versement de la contribution pour l'aide juridique ne peut être relevée d'office par les juridictions sans que le requérant ait été préalablement mis en mesure, directement ou par l'intermédiaire de son avocat, professionnel averti, de respecter la formalité exigée ; que la circonstance qu'il n'est pas interdit à une juridiction de procéder à une demande de régularisation ne méconnaît pas le principe d'égalité devant la justice ; que, dès lors, les dispositions de l'article R. 411-2 du code de justice administrative ne méconnaissent ni le principe d'égalité, ni le droit à un recours effectif garanti par l'article 16 de la Déclaration des droits de l'homme et du citoyen, ni les stipulations des articles 6 paragraphe 1, 13 et 14 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              6. Considérant, en deuxième lieu, qu'il résulte des dispositions de l'article R. 411-2 du code de justice administrative qu'une requête pour laquelle la contribution pour l'aide juridique est due et n'a pas été acquittée est irrecevable et que la juridiction peut la rejeter d'office sans demande de régularisation préalable, lorsqu'elle est introduite par un avocat ; que la circonstance que cette irrecevabilité est susceptible d'être couverte en cours d'instance ne fait pas obstacle à ce qu'une requête, introduite par un avocat et pour laquelle la contribution n'a pas été acquittée, soit regardée comme entachée d'une irrecevabilité manifeste ; que, par suite, le président de la première chambre du tribunal administratif de Melun était compétent pour rejeter la requête par ordonnance, sur le fondement des dispositions du 4° de l'article R. 222-1 du code de justice administrative ;<br/>
<br/>
              7. Considérant, en troisième lieu, qu'il ne résulte d'aucune disposition ni d'aucun principe qu'une requête entachée d'une telle irrecevabilité ne pourrait être rejetée avant l'expiration du délai de recours ; que, par suite, le président de la première chambre du tribunal administratif de Melun n'a pas commis d'irrégularité en rejetant la requête de la fondation dès le 16 février 2012, avant l'expiration du délai de deux mois courant à compter de la notification de la décision attaquée ;<br/>
<br/>
              8. Considérant, en quatrième lieu, qu'il résulte des dispositions de l'article R. 411-2 du code de justice administrative que le président de la première chambre du tribunal administratif de Melun n'était pas tenu d'inviter l'auteur de la requête à la régulariser ; que la fondation requérante ne peut utilement se prévaloir des  recommandations adressées par le secrétaire général du Conseil d'Etat aux présidents de tribunaux administratifs pour la mise en oeuvre des dispositions de cet article ; que, par suite, le président de la première chambre du tribunal administratif de Melun, qui ne s'est pas mépris sur son office, a pu régulièrement rejeter la requête sans inviter préalablement son auteur à la régulariser ;<br/>
<br/>
              9. Considérant qu'il résulte de tout ce qui précède que la fondation Ellen Poidatz n'est pas fondée à demander l'annulation de l'ordonnance du président de la première chambre du tribunal administratif de Melun du 16 février 2012 ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'ordonnance du président de la huitième chambre de la cour administrative d'appel de Paris du 16 mars 2012 est annulée.<br/>
<br/>
Article 2 : L'appel de la fondation Ellen Poidatz est rejeté.<br/>
Article 3 : La présente décision sera notifiée à la fondation Ellen Poidatz et au ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-01-08-05 PROCÉDURE. INTRODUCTION DE L'INSTANCE. FORMES DE LA REQUÊTE. DROIT DE TIMBRE. - CONTRIBUTION POUR L'AIDE JURIDIQUE - ABSENCE D'ACQUITTEMENT ALORS QU'ELLE EST DUE - CONSÉQUENCE - IRRECEVABILITÉ - CAS DANS LEQUEL LA REQUÊTE EST INTRODUITE PAR UN AVOCAT - POSSIBILITÉ DE REJET D'OFFICE SANS DEMANDE DE RÉGULARISATION PRÉALABLE - EXISTENCE - CIRCONSTANCE QUE CETTE IRRECEVABILITÉ SOIT SUSCEPTIBLE D'ÊTRE COUVERTE EN COURS D'INSTANCE - INCIDENCE - ABSENCE - POSSIBILITÉ DE REJETER CETTE REQUÊTE SANS ATTENDRE L'EXPIRATION DU DÉLAI DE RECOURS - EXISTENCE.
</SCT>
<ANA ID="9A"> 54-01-08-05 Il résulte des dispositions de l'article R. 411-2 du code de justice administrative qu'une requête pour laquelle la contribution pour l'aide juridique est due et n'a pas été acquittée est irrecevable et que la juridiction peut la rejeter d'office sans demande de régularisation préalable, lorsqu'elle est introduite par un avocat. La circonstance que cette irrecevabilité est susceptible d'être couverte en cours d'instance ne fait pas obstacle à ce qu'une requête, introduite par un avocat et pour laquelle la contribution n'a pas été acquittée, soit regardée comme entachée d'une irrecevabilité manifeste. Il ne résulte d'aucune disposition ni d'aucun principe qu'une requête entachée d'une telle irrecevabilité ne pourrait être rejetée avant l'expiration du délai de recours.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
