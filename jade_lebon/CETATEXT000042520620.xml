<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042520620</ID>
<ANCIEN_ID>JG_L_2020_11_000000428582</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/52/06/CETATEXT000042520620.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 13/11/2020, 428582</TITRE>
<DATE_DEC>2020-11-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>428582</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>CABINET MUNIER-APAIRE ; SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>M. Réda Wadjinny-Green</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Domingo</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:428582.20201113</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
           M. B... A... a demandé à la Cour nationale du droit d'asile d'annuler la décision du 2 août 2018 par laquelle l'Office français de protection des réfugiés et apatrides lui a retiré le bénéfice de la protection subsidiaire qui lui avait été accordé le 3 mars 2005. Par une décision n° 18042120 du 4 janvier 2019, la Cour nationale du droit d'asile a rejeté sa demande. <br/>
<br/>
           Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique enregistrés les 4 mars, 4 juin et 10 octobre 2019 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat :<br/>
<br/>
           1°) d'annuler cette décision ; <br/>
<br/>
           2°) de renvoyer l'affaire devant la Cour nationale du droit d'asile ou, réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
           3°) de mettre à la charge de l'Office français de protection des réfugiés et apatrides la somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
           Vu les autres pièces du dossier ;<br/>
<br/>
           Vu :<br/>
           - la convention de Genève du 28 juillet 1951 relative au statut des réfugiés et le protocole signé à New York le 31 janvier 1967 ; <br/>
- le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
- le code de justice administrative ;<br/>
<br/>
<br/>
    Après avoir entendu en séance publique :<br/>
<br/>
    - le rapport de M. Réda Wadjinny-Green, auditeur,  <br/>
<br/>
    - les conclusions de M. Laurent Domingo, rapporteur public ;<br/>
<br/>
           La parole ayant été donnée, avant et après les conclusions, au cabinet Munier-Apaire, avocat de M. B... A... et à la SCP Foussard, Froger, avocat de l'Office français de protection des réfugiés et apatrides ;<br/>
<br/>
           Vu la note en délibéré, enregistrée le 23 octobre 2020, présentée par M. A... ; <br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Par une décision du 4 janvier 2019, la Cour nationale du droit d'asile a rejeté le recours formé par M. A... contre la décision du 2 août 2018 par laquelle l'Office français de protection des réfugiés et apatrides (OFPRA) lui a retiré le bénéfice de la protection subsidiaire sur le fondement de l'article L. 712-3 du code de l'entrée et du séjour des étrangers et du droit d'asile, au motif qu'il existait des raisons sérieuses de penser qu'il avait commis un crime grave. M. A... se pourvoit en cassation contre la décision de la Cour nationale du droit d'asile. <br/>
<br/>
              2. En premier lieu, il ressort des pièces de la procédure que le moyen tiré de ce que la minute de la décision attaquée ne serait pas signée manque en fait. <br/>
<br/>
              3. En deuxième lieu, si l'article R. 733-10 du code de l'entrée et du séjour des étrangers et du droit d'asile prévoit que les mémoires et pièces produits par l'OFPRA dans le cadre de la procédure sont communiqués au requérant et si, en l'espèce, le mémoire en défense produit par l'Office devant la Cour nationale du droit d'asile le 7 décembre 2018 n'a pas été communiqué à M. A..., il ressort des pièces de la procédure suivie devant la Cour nationale du droit d'asile que ce mémoire, eu égard à sa teneur et au contenu du dossier de l'Office versé à la procédure, n'apportait aucun élément nouveau ne figurant pas déjà au dossier. Dès lors, le défaut de communication de ce mémoire ne peut être regardé, dans les circonstances de l'espèce, comme ayant pu préjudicier aux droits de M. A... ni, par suite, comme entachant la procédure d'irrégularité. <br/>
<br/>
              4. En troisième lieu, l'article L. 712-2 du code de l'entrée et du séjour des étrangers et du droit d'asile dispose que : " La protection subsidiaire n'est pas accordée à une personne s'il existe des raisons sérieuses de penser : (...) b) Qu'elle a commis un crime grave ". Aux termes de l'article L. 712-3 du même code : " L'office met également fin à tout moment, de sa propre initiative ou à la demande de l'autorité administrative, au bénéfice de la protection subsidiaire lorsque : / 1° Son bénéficiaire aurait dû être exclu de cette protection pour l'un des motifs prévus à l'article L. 712-2 ".<br/>
<br/>
              5. Pour statuer sur l'application de la clause d'exclusion prévue par ces dispositions, la Cour nationale du droit d'asile a relevé que M. A..., de nationalité albanaise, avait été reconnu coupable de détention, offre ou cession, transport et acquisition non autorisés de stupéfiants et condamné, par un jugement du 2 octobre 2014 du tribunal correctionnel de Brive-la-Gaillarde, à une peine d'emprisonnement de trois ans dont un an avec sursis, assortie d'une mise à l'épreuve de deux ans. Elle a retenu qu'au nombre des faits constatés par le juge pénal se trouve une organisation très active en relation avec de nombreux groupes d'Albanais, impliquant le transport de stupéfiants dans plusieurs pays européens, la dissimulation d'importantes sommes d'argent et la couverture de ces activités par des contrats de travail de complaisance, les complices de M. A... s'étant en outre livrés à du trafic de munitions et de matériel informatique et téléphonique. Elle a relevé que, sur appel formé par les complices de M. A..., la cour d'appel de Limoges a confirmé dans un arrêt du 11 décembre 2015 que les transports de stupéfiants aux Pays-Bas s'effectuaient sous la " haute surveillance " de M. A.... Eu égard au rôle de premier plan joué par M. A... dans ce trafic de stupéfiants d'ampleur transnationale et à la gravité de ces faits, qui sont punis, ainsi qu'elle a pu sans erreur de droit le relever, d'une peine de dix ans d'emprisonnement et 7 500 000 euros d'amende, la Cour nationale du droit d'asile, qui n'est pas liée dans son appréciation par la qualification donnée aux faits par les dispositions pénales de droit français, n'a pas inexactement qualifié ces faits en jugeant qu'il existait des raisons sérieuses de penser que M. A... s'était rendu coupable d'un crime grave au sens et pour l'application du b) de l'article L. 712-2 du code de l'entrée et du séjour des étrangers et du droit d'asile. <br/>
<br/>
              6. En quatrième lieu, si M. A... soutient que la Cour a insuffisamment motivé sa décision et commis une erreur de droit en se bornant à relever qu'il ne l'avait pas convaincue d'une véritable prise de conscience de la gravité de ses actes, il ressort des énonciations de la décision attaquée que, pour statuer sur sa demande, la Cour a également tenu compte des allégations du requérant quant aux changements dans les conditions de sa vie professionnelle, sociale et familiale, relevant en outre qu'il n'a pas récidivé ni commis de nouvelle infraction depuis sa dernière condamnation en 2014. En statuant ainsi, la Cour, qui a suffisamment motivé sa décision, s'est livrée, sans erreur de droit, à une appréciation souveraine des faits de l'espèce. <br/>
<br/>
              7. Il résulte de tout ce qui précède que le pourvoi de M. A... doit être rejeté, y compris ses conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le pourvoi de M. A... est rejeté. <br/>
Article 2 : La présente décision sera notifiée à M. B... A... et à l'Office français de protection des réfugiés et apatrides. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">095-04-01-02-02 - COMMISSION D'UN CRIME GRAVE - 1) CONTRÔLE DU JUGE DE CASSATION - QUALIFICATION JURIDIQUE [RJ1] - 2) ESPÈCE - REQUÉRANT AYANT JOUÉ UN RÔLE DE PREMIER PLAN DANS UN TRAFIC DE STUPÉFIANTS D'AMPLEUR TRANSNATIONALE - EXISTENCE, NONOBSTANT LA QUALIFICATION DÉLICTUELLE DES FAITS EN CAUSE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-08-02-02-01-02 PROCÉDURE. VOIES DE RECOURS. CASSATION. CONTRÔLE DU JUGE DE CASSATION. BIEN-FONDÉ. QUALIFICATION JURIDIQUE DES FAITS. - COMMISSION D'UN CRIME GRAVE JUSTIFIANT L'EXCLUSION DE LA PROTECTION SUBSIDIAIRE (B) DE L'ART. L. 712-2 DU CESEDA) [RJ1].
</SCT>
<ANA ID="9A"> 095-04-01-02-02 1) Le juge de cassation exerce un contrôle de qualification juridique des faits sur la commission d'un crime grave justifiant l'exclusion de la protection subsidiaire en application du b) de l'article L. 712-2 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA).... ,,2) Requérant reconnu coupable de détention, offre ou cession, transport et acquisition non autorisés de stupéfiants et condamné à une peine d'emprisonnement de trois ans dont un an avec sursis, assortie d'une mise à l'épreuve de deux ans.... ,,Juge pénal ayant constaté l'existence d'une organisation très active en relation avec de nombreux groupes de même nationalité que le requérant, impliquant le transport de stupéfiants dans plusieurs pays européens, la dissimulation d'importantes sommes d'argent et la couverture de ces activités par des contrats de travail de complaisance, les complices de l'intéressé s'étant en outre livrés à du trafic de munitions et de matériel informatique et téléphonique. Juge pénal ayant relevé que les transports de stupéfiants aux Pays-Bas s'effectuaient sous la haute surveillance de l'intéressé.... ,,Eu égard au rôle de premier plan joué par celui-ci dans ce trafic de stupéfiants d'ampleur transnationale et à la gravité de ces faits, qui sont punis d'une peine de dix ans d'emprisonnement et 7 500 000 euros d'amende, la Cour nationale du droit d'asile (CNDA), qui n'est pas liée dans son appréciation par la qualification donnée aux faits par les dispositions pénales de droit français, n'a pas inexactement qualifié ces faits en jugeant qu'il existait des raisons sérieuses de penser que le requérant s'était rendu coupable d'un crime grave au sens et pour l'application du b) de l'article L. 712-2 du CESEDA.</ANA>
<ANA ID="9B"> 54-08-02-02-01-02 Le juge de cassation exerce un contrôle de qualification juridique des faits sur la commission d'un crime grave justifiant l'exclusion de la protection subsidiaire en application du b) de l'article L. 712-2 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA).</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., s'agissant de la clause d'exclusion de l'asile prévue au 1° de l'article L. 711-6 du CESEDA, CE, 17 avril 2019, Office français de protection des réfugiés et apatrides, n° 419722, T. pp. 580-961 ; s'agissant des clauses d'exclusion prévues à l'article 1 F de la Convention de Genève, CE, 9 novembre 2016, Office français de protection des réfugiés et apatrides, n° 388830, p. 465 ; CE, 11 avril 2018, M.,, n° 410897, p. 112.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
