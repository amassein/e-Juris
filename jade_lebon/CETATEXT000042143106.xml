<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042143106</ID>
<ANCIEN_ID>JG_L_2020_07_000000430601</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/14/31/CETATEXT000042143106.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 22/07/2020, 430601</TITRE>
<DATE_DEC>2020-07-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>430601</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUVENIN, COUDRAY, GREVY</AVOCATS>
<RAPPORTEUR>M. Arno Klarsfeld</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:430601.20200722</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
      M. C... et Mme D... A... ont demandé à la Cour nationale du droit d'asile d'annuler les décisions du 25 octobre 2017 par lesquelles l'Office français de protection des réfugiés et apatrides a rejeté leur demande d'asile. Par une décision n°18002676,18002677 du 17 décembre 2018, la Cour nationale du droit d'asile a rejeté leur demande.<br/>
<br/>
      Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 9 mai et 8 août 2019 au secrétariat du contentieux du Conseil d'Etat, M. B... et Mme A... demandent au Conseil d'Etat :<br/>
<br/>
      1°) d'annuler cette décision ;<br/>
<br/>
      2°) réglant l'affaire au fond, de leur reconnaitre la qualité de réfugié ou, à défaut, de leur accorder le bénéfice de la protection subsidiaire ;<br/>
<br/>
      3°) de mettre à la charge de l'Office français de protection des réfugiés et apatrides la somme de 3 500 euros à verser à la SCP Thouvenin, Coudray, Grevy, leur avocat, au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
<br/>
      Vu les autres pièces du dossier ;<br/>
<br/>
      Vu :<br/>
      - la convention de Genève du 28 juillet 1951 relative au statut des réfugiés et le protocole signé à New York le 31 janvier 1967 relatif au statut des réfugiés ;<br/>
      - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
      - la loi n° 91-647 du 10 juillet 1991 ;<br/>
      - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 ;<br/>
<br/>
<br/>
<br/>
      Après avoir entendu en séance publique :<br/>
<br/>
      - le rapport de M. Arno Klarsfeld, conseiller d'Etat,  <br/>
<br/>
      - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
      La parole ayant été donnée, avant et après les conclusions, à la SCP Thouvenin, Coudray, Grevy, avocat de M. C... et de Mme D... A... ;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. L'article 1er de la convention de Genève du 28 juillet 1951 précise, au 2° de son paragraphe A, que doit être considérée comme réfugiée toute personne qui " craignant avec raison d'être persécutée du fait de sa race, de sa religion, de sa nationalité, de son appartenance à un certain groupe social ou de ses opinions politiques, se trouve hors du pays dont elle a la nationalité et qui ne peut, ou, du fait de cette crainte, ne veut se réclamer de la protection de ce pays ". Par ailleurs, aux termes de l'article L. 712-1 du code de l'entrée et du séjour des étrangers et du droit d'asile : " Le bénéfice de la protection subsidiaire est accordé à toute personne qui ne remplit pas les conditions pour se voir reconnaître la qualité de réfugié et pour laquelle il existe des motifs sérieux et avérés de croire qu'elle courrait dans son pays un risque réel de subir l'une des atteintes graves suivantes : a) La peine de mort ou une exécution ; b) La torture ou des peines ou traitements inhumains ou dégradants ; c) S'agissant d'un civil, une menace grave et individuelle contre sa vie ou sa personne en raison d'une violence qui peut s'étendre à des personnes sans considération de leur situation personnelle et résultant d'une situation de conflit armé interne ou international ".<br/>
<br/>
              2. M. B... et Mme A..., de nationalité iranienne, ont demandé à la Cour nationale du droit d'asile d'annuler les décisions du 25 octobre 2017 par lesquelles l'Office français de protection des réfugiés et apatrides a refusé de leur reconnaître la qualité de réfugié et de leur accorder le bénéfice de la protection subsidiaire. Ils se pourvoient en cassation contre la décision du 17 décembre 2018 par laquelle la Cour nationale du droit d'asile a rejeté leur demande.<br/>
<br/>
              3. En premier lieu, l'article R. 733-31 du code de l'entrée et du séjour des étrangers et du droit d'asile dispose : " Les décisions de la cour sont lues en audience publique. Leur sens est affiché dans les locaux de la cour le jour de leur lecture ".<br/>
<br/>
              4. M. B... et Mme A... font valoir que le sens de la décision de la Cour qui a été affiché dans les locaux de celle-ci le jour de sa lecture, conformément à ce que prévoit l'article R. 733-31, portait la mention " statut de réfugié " alors qu'ils ont ultérieurement reçu notification d'une décision rejetant leur demande. Il ressort toutefois des pièces du dossier de la procédure que la décision de rejet qui leur a été notifiée est en tous points conforme à la minute signée de la décision rendue, qui seule fait foi. L'erreur entachant la mesure administrative d'affichage du sens de cette décision est sans incidence sur sa régularité. <br/>
<br/>
              5. En second lieu, aux termes de l'article L. 733-1 du code de l'entrée et du séjour des étrangers et du droit d'asile, les requérants devant la Cour nationale du droit d'asile peuvent se faire assister d'un interprète. Aux termes de l'article R. 733-5 du même code : " Le requérant est entendu à l'audience dans la langue qu'il a, en application de l'article L. 741-2-1, indiquée à l'autorité administrative lors de l'enregistrement de sa demande d'asile. A défaut de choix de sa part lors de l'enregistrement ou dans le cas où sa demande ne peut être satisfaite, il est entendu dans une langue dont il a une connaissance suffisante. Lorsque le requérant conteste la langue dans laquelle il a été entendu par l'office, il indique dans le délai de recours la langue dans laquelle il souhaite être entendu. Dans ce cas, le requérant est entendu dans cette langue. Lorsque sa demande ne peut être satisfaite, il est entendu dans une langue dont il est raisonnable de penser qu'il la comprend ".<br/>
<br/>
              6. Il ressort des pièces du dossier de la procédure que M. B... et Mme A... ont bénéficié devant la Cour de l'assistance d'un interprète en langue farsi. S'ils font valoir que cet interprète s'exprimait en " dari ", variante du farsi parlée en Afghanistan, alors qu'ils avaient demandé à être entendus en " farsi iranien ", il ne ressort pas des pièces du dossier de la Cour qu'ils aient fait état de difficultés de compréhension de leur part ou de la part de l'interprète lors de l'audience, ni à l'appui d'une note en délibéré, alors qu'il était raisonnable de penser qu'ils comprenaient cette variante de leur langue et pouvaient se faire comprendre, dans leur langue, par l'interprète auquel ils s'adressaient en farsi iranien. <br/>
<br/>
              7. En troisième lieu, c'est par une appréciation souveraine exempte de dénaturation que la Cour a jugé que les pièces au dossier et les déclarations de M. B... et Mme A... ne permettaient pas d'établir la réalité des craintes alléguées. <br/>
<br/>
              8. Il résulte de tout ce qui précède que M. B... et Mme A... ne sont pas fondés à demander l'annulation de la décision qu'ils attaquent. Leur pourvoi doit, dès lors, être rejeté, y compris les conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
                               D E C I D E :<br/>
                               --------------<br/>
<br/>
Article 1er : Le pourvoi de M. B... et Mme A... est rejeté.<br/>
Article 2 : La présente décision sera notifiée à M. C... et Mme D... A... ainsi qu'à l'Office français de protection des réfugiés et apatrides.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">095-08-04 - ERREUR ENTACHANT LA MESURE D'AFFICHAGE DU SENS DE LA DÉCISION - ABSENCE D'INCIDENCE SUR LA RÉGULARITÉ DE CETTE DÉCISION [RJ1].
</SCT>
<ANA ID="9A"> 095-08-04 L'erreur entachant la mesure administrative d'affichage du sens d'une décision de la Cour, prévue par l'article R. 733-31 du code de l'entrée et du séjour des étrangers et du droit d'asile, est sans incidence sur sa régularité.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., s'agissant de l'affichage du jugement d'un tribunal administratif après la lecture, CE, 9 mars 1983, Association SOS Défense, n° 42301, T. pp. 727-827. Comp., s'agissant d'une divergence entre le dispositif d'un jugement de reconduite à la frontière lu à l'audience publique et celui du jugement notifié, CE, 9 février 2004, Préfet de police c/,, n° 254913, T. p. 727.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
