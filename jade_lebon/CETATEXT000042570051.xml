<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042570051</ID>
<ANCIEN_ID>JG_L_2020_11_000000428732</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/57/00/CETATEXT000042570051.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 4ème chambres réunies, 25/11/2020, 428732</TITRE>
<DATE_DEC>2020-11-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>428732</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DE NERVO, POUPET</AVOCATS>
<RAPPORTEUR>M. Thibaut Félix</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie Sirinelli</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:428732.20201125</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société par actions simplifiée (SAS) Ambulances santé assistance a demandé au tribunal administratif d'Orléans d'annuler l'arrêté du 17 mars 2015 par lequel le directeur général de l'Agence régionale de santé du Centre-Val de Loire a retiré l'agrément et l'autorisation de mise en service de deux véhicules sanitaires de catégorie C dont elle était titulaire. Par un jugement n° 1501722 du 9 février 2017, le tribunal administratif d'Orléans a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 17NT01067 du 11 janvier 2019, la cour administrative d'appel de Nantes a rejeté l'appel formé par la SAS Ambulances santé assistance contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 11 mars et 7 juin 2019 au secrétariat du contentieux du Conseil d'Etat, la SAS Ambulances santé assistance demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la santé publique ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Thibaut Félix, auditeur,  <br/>
<br/>
              - les conclusions de Mme Marie Sirinelli, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP de Nervo, Poupet, avocat de la société Ambulances sante assistance ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 9 décembre 2013, le directeur général de l'agence régionale de santé (ARS) de la région Centre-Val de Loire a délivré à la société Ambulances santé assistance deux autorisations de mise en service de véhicules de transports sanitaires de catégorie C, ou ambulances, et un agrément à ce titre. Un contrôle de l'activité de l'entreprise mené par l'ARS, qui a donné lieu à un rapport de contrôle du 12 janvier 2015, a montré que les deux autorisations de mise en service avaient été utilisées par la société pour les besoins exclusifs de départements autres que l'Eure-et-Loir, notamment en Ile-de-France. Par un arrêté du 17 mars 2015, l'ARS a retiré à la société Ambulances santé assistance son agrément et les autorisations de mise en service des deux ambulances. Le tribunal administratif d'Orléans a rejeté, par un jugement du 9 février 2017, la demande de la société tendant à l'annulation de cet arrêté. La société se pourvoit en cassation contre l'arrêt du 11 janvier 2019 par lequel la cour administrative d'appel de Nantes a rejeté son appel contre ce jugement.<br/>
<br/>
              2. En premier lieu, l'article R. 6313-6 du code de la santé publique prévoit que le sous-comité des transports sanitaires du comité départemental de l'aide médicale urgente, de la permanence des soins et des transports sanitaires " donne un avis préalable au retrait par le directeur général de l'agence régionale de santé de l'agrément nécessaire aux transports sanitaires mentionné à l'article L. 6312-2. / Cet avis est donné au vu du rapport du médecin désigné par le directeur général de l'agence régionale de santé et des observations de l'intéressé (...) ". <br/>
<br/>
              3. Il ressort des pièces du dossier soumis aux juges du fond que la décision attaquée a été précédée de la consultation du sous-comité des transports sanitaires, réuni le 13 février 2015, et que la société a été représentée à cette réunion par son conseil, qui avait également présenté des observations écrites le 10 février 2015. Par suite, la cour administrative d'appel de Nantes n'a pas dénaturé les pièces du dossier en jugeant que le sous-comité des transports sanitaires avait été régulièrement consulté le 13 février 2015, dans le respect du principe des droits de la défense. <br/>
<br/>
              4. En deuxième lieu, aux termes de l'article L. 6312-2 du code de la santé publique : " Toute personne effectuant un transport sanitaire doit avoir été préalablement agréée par le directeur général de l'agence régionale de santé. Le refus d'agrément doit être motivé ". En vertu des articles R. 6312-6 et suivants de ce code, applicables aux transports sanitaires terrestres, l'agrément suppose la disposition des personnels nécessaires pour garantir la présence à bord de tout véhicule en service d'un équipage ayant certaines qualifications ainsi que de véhicules spécialement adaptés au transport sanitaire qui répondent à certaines normes et dont le titulaire de l'agrément a un usage exclusif. L'article R. 6312-11 du même code prévoit que l'agrément est délivré pour l'accomplissement des transports sanitaires des malades, blessés ou parturientes effectués, dans tous les cas, au titre de l'aide médicale urgente et, au surplus, le cas échéant, aux transports effectués sur prescription médicale. Dans ce dernier cas, le titulaire de l'agrément doit disposer des personnels, des véhicules et des installations matérielles précisées par l'article R. 6312-13 du même code. A ce titre, en particulier, il doit disposer d'au moins deux véhicules des catégories " ambulance de secours et de soins d'urgence ", " ambulance " ou " véhicule sanitaire léger ", dont l'un au moins relevant de la première ou de la deuxième de ces catégories. Ses installations doivent comprendre, en vertu de l'arrêté du ministre chargé de la santé du 10 février 2009 applicable au litige, un local sur le territoire pour lequel l'agrément est donné, le cas échéant commun à plusieurs entreprises de transports sanitaires agréées, destiné à l'accueil des patients ou de leur famille, un ou des locaux permettant d'assurer la désinfection et l'entretien courant des véhicules et la maintenance du matériel et une ou des aires destinées au stationnement des véhicules inscrits au dossier d'agrément. Enfin, les articles R. 6312-18 et R. 6312-19 de ce code prévoient, pour garantir la continuité de prise en charge des patients, qu'une garde des transports sanitaires est assurée sur l'ensemble du territoire départemental, à laquelle les entreprises de transports sanitaires agréées pour l'accomplissement des transports effectués sur prescription médicale sont tenues de participer, en fonction de leurs moyens matériels et humains.<br/>
<br/>
              5. Par ailleurs, aux termes de l'article L. 6312-4 du même code, dans sa rédaction applicable au litige : " Dans chaque département, la mise en service par les personnes mentionnées à l'article L. 6312-2 de véhicules affectés aux transports sanitaires terrestres est soumise à l'autorisation du directeur général de l'agence régionale de santé. / Aucune autorisation n'est délivrée si le nombre de véhicules déjà en service égale ou excède un nombre fixé en fonction des besoins sanitaires de la population. / Le retrait de l'agrément peut être prononcé à l'encontre de toute personne qui a mis ou maintenu en service un véhicule sans autorisation. / Les dispositions du présent article ne sont pas applicables aux véhicules exclusivement affectés aux transports sanitaires effectués dans le cadre de l'aide médicale urgente ". En vertu des articles R. 6312-29 et R. 6312-30 du même code, le nombre théorique de véhicules est fixé dans chaque département par le directeur général de l'agence régionale de santé en fonction, d'une part, de l'indice national des besoins de transports sanitaires de la population exprimé en nombre de véhicules par habitant, qui est arrêté par le ministre chargé de la santé et qui varie selon la taille des communes et, d'autre part, d'une possible majoration ou minoration pour prendre en compte, en particulier, les caractéristiques démographiques, géographiques ou d'équipement sanitaire du département et la situation locale de la concurrence dans le secteur des transports sanitaires. Les articles R. 6312-33 et suivants du même code prévoient qu'au moins une fois par an, si le nombre de véhicules fixé en fonction des besoins sanitaires de la population est supérieur au nombre des véhicules déjà autorisés, le directeur général de l'agence régionale de santé détermine et rend publics le nombre d'autorisations nouvelles de mise en service qui peuvent être attribuées et les priorités d'attribution pour assurer  la meilleure distribution des moyens de transport sanitaire dans le département ainsi que le délai de réception des demandes d'autorisation, qui doit être d'au moins un mois. Les autorisations sont attribuées selon les priorités ainsi rendues publiques et en fonction de la situation locale de la concurrence, le choix étant opéré par tirage au sort si plusieurs demandes satisfont également à ces critères ". Enfin, aux termes de l'article R. 6312-40 du même code : " Les personnes bénéficiant d'autorisations de mise en service et dont la demande d'agrément a été rejetée disposent d'un délai imparti par le directeur général de l'agence régionale de santé, d'au moins deux mois, pour réunir les conditions qui faisaient défaut pour l'obtention de l'agrément et déposer une nouvelle demande. En cas de nouveau refus, les autorisations de mise en service des véhicules pour l'utilisation desquels l'agrément était demandé deviennent caduques ".<br/>
<br/>
              6. Il résulte de ces dispositions que toute personne effectuant un transport sanitaire terrestre doit disposer, d'une part, d'une autorisation de mise en service des véhicules utilisés, sauf s'ils sont exclusivement affectés aux transports sanitaires effectués dans le cadre de l'aide médicale urgente et, d'autre part, d'un agrément, tous deux délivrés par le directeur général de l'agence régionale de santé. <br/>
<br/>
              7. L'autorisation de mise en service de chaque véhicule a pour objet de permettre une régulation territoriale de l'offre de soins. Elle est délivrée au niveau départemental, de telle sorte que l'offre de transports sanitaires terrestres soit adaptée, dans chaque département, aux besoins de la population. Il s'ensuit que chaque véhicule doit, en principe, être basé et utilisé dans le département dans lequel il a été autorisé. Le fait pour un transporteur sanitaire de mettre en service ou d'utiliser, de façon habituelle, un véhicule pour des transports sanitaires effectués dans leur totalité dans d'autres départements que celui dans lequel il a été autorisé constitue une méconnaissance des obligations qui lui incombent, de nature à justifier l'abrogation de l'autorisation de mise en service de ce véhicule.  <br/>
<br/>
              8. L'agrément du transporteur sanitaire a pour objet de veiller à la qualité et à la sécurité du transport sanitaire compte tenu, notamment, des moyens dont il dispose pour remplir sa mission. Si aucune disposition du code de la santé publique ne fait obstacle à ce qu'un transporteur sanitaire soit agréé simultanément au titre de plusieurs départements, c'est à la condition, toutefois, s'agissant des transports effectués sur prescription médicale, qu'il dispose dans chacun de ces départements des personnels, des véhicules et des installations exigés par les dispositions de l'article R. 6312-13 du code de la santé publique. Lorsqu'il ne dispose plus des moyens en personnel et en matériel lui permettant de respecter ses obligations, notamment son obligation de participation au service de garde, et qu'il cesse ainsi de remplir les conditions de cet agrément dans tout ou partie des départements considérés, il incombe au directeur général de l'agence régionale de santé, selon le cas, d'abroger cet acte ou d'en modifier la portée. <br/>
<br/>
              9. Par suite, la cour administrative d'appel de Nantes n'a pas commis d'erreur de droit en jugeant que l'agrément dont bénéficiait la société Ambulances santé assistance, délivré le 9 décembre 2013 par le directeur général de l'agence régionale de santé du Centre-Val de Loire, en même temps que l'autorisation de mise en service de deux ambulances en Eure-et-Loir, supposait que la société dispose, dans ce département, des moyens exigés par les dispositions de l'article R. 6312-13 du code de la santé publique et que l'utilisation des deux ambulances au bénéfice exclusif de la population d'autres départements justifiait l'abrogation tant des autorisations de mise en service des véhicules que de l'agrément de la société. <br/>
<br/>
              10. Il résulte de tout ce qui précède que la société Ambulances santé assistance n'est pas fondée à demander l'annulation de l'arrêt de la cour administrative d'appel de Nantes qu'elle attaque. Dès lors, ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative doivent être également rejetées.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la société Ambulances santé assistance est rejeté.<br/>
Article 2 : La présente décision sera notifiée à la société par actions simplifiée Ambulances santé assistance et au ministre des solidarités et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">61-01-02 SANTÉ PUBLIQUE. PROTECTION GÉNÉRALE DE LA SANTÉ PUBLIQUE. TRANSPORTS SANITAIRES. - EXIGENCE D'UNE AUTORISATION DE MISE EN SERVICE DES VÉHICULES (ART. L. 6312-4 DU CSP) ET D'UN AGRÉMENT (ART. L 6312-2 DU CSP) - PORTÉE TERRITORIALE - 1) AUTORISATION - A) PORTÉE DÉPARTEMENTALE - CONSÉQUENCE - INTERDICTION D'UTILISER DE FAÇON HABITUELLE UN VÉHICULE POUR DES TRANSPORTS SANITAIRES EFFECTUÉS DANS LEUR TOTALITÉ DANS D'AUTRES DÉPARTEMENTS - B) MÉCONNAISSANCE DE CETTE INTERDICTION JUSTIFIANT L'ABROGATION DE L'AUTORISATION - 2) AGRÉMENT - A) POSSIBILITÉ D'AGRÉER AU TITRE DE PLUSIEURS DÉPARTEMENTS - EXISTENCE, À LA CONDITION QUE LE TRANSPORTEUR DISPOSE DANS CHACUN DE CES DÉPARTEMENTS DES MOYENS EXIGÉS PAR L'ARTICLE R. 6312-13 DU CSP -  B) CAS OÙ LE TRANSPORTEUR CESSE DE REMPLIR CES CONDITIONS DANS TOUT OU PARTIE DES DÉPARTEMENTS - DG DE L'ARS TENU D'ABROGER L'AGRÉMENT [RJ1] OU D'EN MODIFIER LA PORTÉE.
</SCT>
<ANA ID="9A"> 61-01-02 Il résulte des articles L. 6312-2, L. 6312-4, R. 6312-1 et suivants du code de la santé publique (CSP) que toute personne effectuant un transport sanitaire terrestre doit disposer, d'une part, d'une autorisation de mise en service des véhicules utilisés, sauf s'ils sont exclusivement affectés aux transports sanitaires effectués dans le cadre de l'aide médicale urgente et, d'autre part, d'un agrément, tous deux délivrés par le directeur général de l'agence régionale de santé (ARS).... ,,1) a) L'autorisation de mise en service de chaque véhicule a pour objet de permettre une régulation territoriale de l'offre de soins. Elle est délivrée au niveau départemental, de telle sorte que l'offre de transports sanitaires terrestres soit adaptée, dans chaque département, aux besoins de la population.,, ...b) Il s'ensuit que chaque véhicule doit, en principe, être basé et utilisé dans le département dans lequel il a été autorisé. Le fait pour un transporteur sanitaire de mettre en service ou d'utiliser, de façon habituelle, un véhicule pour des transports sanitaires effectués dans leur totalité dans d'autres départements que celui dans lequel il a été autorisé constitue une méconnaissance des obligations qui lui incombent, de nature à justifier l'abrogation de l'autorisation de mise en service de ce véhicule....  ,,2) a) L'agrément du transporteur sanitaire a pour objet de veiller à la qualité et à la sécurité du transport sanitaire compte tenu, notamment, des moyens dont il dispose pour remplir sa mission. Si aucune disposition du code de la santé publique ne fait obstacle à ce qu'un transporteur sanitaire soit agréé simultanément au titre de plusieurs départements, c'est à la condition, toutefois, s'agissant des transports effectués sur prescription médicale, qu'il dispose dans chacun de ces départements des personnels, des véhicules et des installations exigés par les dispositions de l'article R. 6312-13 du CSP.,,,b) Lorsqu'il ne dispose plus des moyens en personnel et en matériel lui permettant de respecter ses obligations, notamment son obligation de participation au service de garde, et qu'il cesse ainsi de remplir les conditions de cet agrément dans tout ou partie des départements considérés, il incombe au directeur général (DG) de l'ARS, selon le cas, d'abroger cet acte ou d'en modifier la portée.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 5 février 2020, Société Les Taxis Hurié, n° 426225, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
