<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038071585</ID>
<ANCIEN_ID>JG_L_2019_01_000000424118</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/07/15/CETATEXT000038071585.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème et 1ère chambres réunies, 28/01/2019, 424118</TITRE>
<DATE_DEC>2019-01-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>424118</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème et 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARADUC, DUHAMEL, RAMEIX</AVOCATS>
<RAPPORTEUR>Mme Sara-Lou Gerber</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:424118.20190128</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              1° M. G...U..., M. AA...Y..., Mme L...V..., Mme F...P..., M. Z...R..., M. H...M..., M. X... T..., M. AC...E...K..., M. W... S..., M. J... B..., M. E...O..., M AB... I..., M. G...AD...C...et M. G... A...D...ont demandé au juge des référés du tribunal administratif de Marseille, sur le fondement des dispositions de l'article L. 521-1 du code de justice administrative, de suspendre l'arrêté du 4 juillet 2018 par lequel le directeur général de l'agence régionale de santé Provence-Alpes-Côte d'Azur a prononcé la dissolution du conseil départemental des Bouches-du-Rhône de l'ordre des médecins. Par une ordonnance n° 1806112 du 28 août 2018, le juge des référés a donné acte du désistement de M. A...D...et suspendu l'exécution de cet arrêté. <br/>
<br/>
              Par un pourvoi, un mémoire en réplique et un nouveau mémoire, enregistrés les 12 septembre et 5 novembre 2018 et le 18 janvier 2019 au secrétariat du contentieux du Conseil d'Etat sous le n° 424118, la ministre des solidarités et de la santé demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette ordonnance en tant qu'elle suspend l'arrêté contesté ;<br/>
<br/>
              2°) statuant en référé, de rejeter la demande de M. U...et autres.<br/>
<br/>
              2° M. G...A...D...et Mme Q...N...ont demandé au juge des référés du tribunal administratif de Marseille, sur le fondement des dispositions de l'article L. 521-1 du code de justice administrative, de suspendre l'exécution du même arrêté du 4 juillet 2018 du directeur général de l'agence régionale de santé Provence-Alpes-Côte d'Azur. Par une ordonnance n° 1806253 du 28 août 2018, le juge des référés a suspendu l'exécution de cet arrêté.<br/>
<br/>
              Par un pourvoi, enregistré le 12 septembre 2018 au secrétariat du contentieux du Conseil d'Etat sous le n° 424120, la ministre des solidarités et de la santé demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette ordonnance en tant qu'elle suspend l'arrêté contesté ; <br/>
<br/>
              2°) statuant en référé, de rejeter la demande de M. A...D...et autre.<br/>
<br/>
<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - le code de la santé publique ;<br/>
              - le décret n° 2017-1418 du 29 septembre 2017 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sara-Lou Gerber, auditeur,  <br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Baraduc, Duhamel, Rameix, avocat de M. U...et autres et à la SCP Gaschignard, avocat de M. A... D... et de Mme N...;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 25 janvier 2019, présentée par M. U... et autres ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ".<br/>
<br/>
              Sur le droit applicable :<br/>
<br/>
              2. L'article L. 4121-2 du code de la santé publique prévoit que l'ordre des médecins veille : " (...) au maintien des principes de moralité, de probité, de compétence et de dévouement indispensables à l'exercice de la médecine (...) et à l'observation, par tous leurs membres, des devoirs professionnels, ainsi que des règles édictées par le code de déontologie prévu à l'article L. 4127-1 (...) ". L'article L. 4123-1 du même code dispose à ce titre que : " Le conseil départemental de l'ordre exerce, dans le cadre départemental et sous le contrôle du conseil national, les attributions générales de l'ordre, énumérées à l'article L. 4121-2 (...) ". Il résulte de ces dispositions et de l'ensemble des missions confiées aux instances ordinales par les dispositions du titre deuxième du livre premier de la quatrième partie du code de la santé publique que le législateur a entendu faire de l'organisation et du contrôle de la profession médicale un service public, auquel concourent, alors même qu'ils ne constituent pas des établissements publics, le Conseil national de l'ordre et, dans le périmètre de leurs ressorts territoriaux, les conseils régionaux et départementaux. <br/>
<br/>
              3. A ce titre l'article L. 4123-10 du code de la santé publique dispose que : " Lorsque, par leur fait, les membres d'un conseil départemental mettent celui-ci dans l'impossibilité de fonctionner, le directeur général de l'agence régionale de santé, sur proposition du Conseil national de l'ordre, peut, par arrêté, prononcer la dissolution du conseil départemental. En cas de dissolution du conseil départemental ou en cas de démission de tous ses membres, il nomme, sur proposition du Conseil national de l'ordre, une délégation de trois à cinq membres suivant l'importance numérique du conseil. Cette délégation assure les fonctions du conseil départemental jusqu'à l'élection d'un nouveau conseil organisée sans délai par le Conseil national ". Ces dispositions permettent ainsi au directeur général de l'agence régionale de santé compétent, sur proposition du Conseil national de l'ordre, de prononcer la dissolution d'un conseil départemental lorsque, par le fait de ses membres, la continuité du service public ou le bon exercice des missions qu'il comporte sont gravement compromis.<br/>
<br/>
              Sur les pourvois :<br/>
<br/>
              4. Il ressort des pièces du dossier soumis au juge des référés du tribunal administratif de Marseille que, par un arrêté du 4 juillet 2018, le directeur général de l'agence régionale de santé Provence-Alpes-Côte d'Azur a, en application des dispositions citées ci-dessus de l'article L. 4123-10 du code de la santé publique, prononcé la dissolution du conseil départemental des Bouches-du-Rhône de l'ordre des médecins et nommé une délégation de cinq membres. Par deux ordonnances du 28 août 2018, le juge des référés du tribunal administratif de Marseille, statuant sur le fondement des dispositions citées ci-dessus de l'article L. 521-1 du code de justice administrative, a, à la demande de plusieurs conseillers ordinaux, suspendu l'exécution de cet arrêté. Par deux pourvois qu'il y a lieu de joindre, la ministre des solidarités et de la santé demande l'annulation de ces deux ordonnances.<br/>
<br/>
              5. La ministre des solidarités et de la santé est fondée à soutenir que le juge des référés a dénaturé les pièces du dossier qui lui était soumis en estimant qu'était propre à créer un doute sérieux quant à la légalité de l'arrêté contesté le moyen tiré de ce que ce conseil départemental n'était pas dans l'impossibilité de fonctionner, au sens des dispositions de l'article L. 4123-10 du code de la santé publique, alors qu'il ressortait de ce dossier que le conseil départemental des Bouches-du-Rhône de l'ordre des médecins était notoirement traversé de violents conflits compromettant gravement sa légitimité, que vingt-et-un des quarante six membres titulaires et suppléants avaient démissionné en contestant la légitimité des autres membres à exercer leurs fonctions, que le conseil départemental avait, en raison de ce conflit, porté plainte contre quatre de ses propres membres et que, après plusieurs tentatives de conciliation conduites sur place par des délégations du Conseil national de l'ordre des médecins, une ultime mission de conciliation effectuée par le Conseil national avait échoué.<br/>
<br/>
              6. La ministre des solidarités et de la santé est, par suite, fondée, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, à demander l'annulation des ordonnances qu'elle attaque, en tant qu'elles suspendent l'arrêté contesté.<br/>
<br/>
              7. Dans les circonstances de l'espèce, il y a lieu de régler, dans cette mesure, les affaires au titre des procédures de référé engagées, d'une part par M. U...et autres et, d'autre part, par M. A...D...et MmeN..., en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              8. Le Conseil national de l'ordre des médecins, sur la proposition duquel a été pris l'arrêté contesté, justifie d'un intérêt suffisant au maintien de celui-ci. Ses deux interventions sont, par suite, recevables.<br/>
<br/>
              9. Les requérants soutiennent que l'arrêté qu'ils contestent est entaché : <br/>
              - d'illégalité externe en ce que la proposition du Conseil national de l'ordre des médecins sur laquelle il est fondé a été prise sur une procédure irrégulière ;<br/>
              - d'erreur de droit, en ce que le directeur général de l'agence régionale de santé s'est estimé lié par la proposition du Conseil national de l'ordre des médecins ;<br/>
              - d'inexacte appréciation des faits et de méconnaissance des dispositions de l'article L. 4123-10 du code de la santé publique, en ce qu'il retient que le conseil départemental était dans l'impossibilité de fonctionner ;<br/>
              - de détournement de pouvoir.<br/>
<br/>
              10. Aucun de ces moyens ne paraît, en l'état de l'instruction, de nature à créer un doute sérieux quant à la légalité de l'arrêté contesté.<br/>
<br/>
              11. Il résulte de tout ce qui précède que, sans qu'il y ait lieu d'apprécier la condition d'urgence prévue à l'article L. 521-1 du code de justice administrative cité au point 1 et sans qu'il soit non plus besoin de statuer sur la fin de non-recevoir soulevée par l'agence régionale de santé de Provence-Alpes-Côte d'Azur, les requérants ne sont pas fondés à demander la suspension de l'exécution de l'arrêté qu'ils contestent. Par voie de conséquence, leurs conclusions présentées au titre de l'article L. 761-1 du code de justice administrative doivent être également rejetées.<br/>
<br/>
              Sur les effets de la présente décision :<br/>
<br/>
              12. Le rejet, par la présente décision, des demandes de suspension introduites, devant le tribunal administratif de Marseille, par M. U...et autres et par M. A...D...et MmeN..., a pour effet de mettre fin, à compter de sa date de lecture, à la suspension prononcée le 29 août 2018 par le juge des référés du tribunal administratif de Marseille. Si les dispositions de l'article L. 4123-10 du code de la santé publique citées ci-dessus permettent à la délégation nommée par le directeur général de l'agence régionale de santé d'assurer l'ensemble des fonctions du conseil départemental jusqu'à l'élection d'un nouveau conseil départemental, sans se limiter à la seule expédition des affaires courantes, elles ne confèrent pas, pour autant, aux membres de cette délégation la qualité de membres titulaires du conseil départemental, au sens des dispositions du IV de l'article L. 4124-11 du même code, appelés, à ce titre, à élire les membres du conseil régional de l'ordre.<br/>
<br/>
              13. Or il résulte de l'instruction que le renouvellement de l'ensemble des membres du conseil régional de l'ordre des médecins de Provence-Alpes-Côte d'Azur-Corse doit en principe s'effectuer à la date du 7 février 2019 et que dix des trente-deux membres de ce conseil doivent, lors de ce scrutin, être élus par les membres titulaires du conseil départemental des Bouches-du-Rhône. En effet, en application des dispositions de l'article D. 4132-2 du code de la santé publique, aux termes duquel " Le conseil régional ou interrégional de l'ordre des médecins est composé de (...) seize binômes de titulaires selon que le nombre de médecins inscrits aux derniers tableaux publiés des conseils départementaux constituant la région ou l'interrégion est (...) 4° Supérieur à 25 000. / Chaque conseil départemental élit un binôme de titulaires, le représentant au conseil régional ou interrégional. Les sièges restants sont répartis par le conseil national de l'ordre compte tenu du nombre de médecins de la région ou de l'interrégion inscrits au tableau mentionné au premier alinéa  ", le Conseil national de l'ordre des médecins a fixé à cinq le nombre de binômes de titulaires représentant le conseil départemental des Bouches-du-Rhône au sein du conseil régional de Provence-Alpes-Côte d'Azur-Corse.<br/>
<br/>
              14. Par suite, si la participation à ce scrutin des membres titulaires du conseil départemental des Bouches-du-Rhône est, à raison de l'exercice des fonctions de ce conseil départemental par la délégation mentionnée au point 4, impossible à la date à laquelle les autres conseils départementaux de la région Provence-Alpes-Côte d'Azur-Corse éliront leurs représentants, le conseil régional nouvellement élu ne comportera de membres représentant le conseil départemental des Bouches du Rhône que lorsque ceux de ses membres qui doivent être élus par les membres titulaires de ce conseil départemental l'auront été, après l'élection d'un nouveau conseil départemental conformément aux dispositions citées au point 3.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Les ordonnances n° 1806112 et 1806253 du 28 août 2018 du juge des référés du tribunal administratif de Marseille sont annulées.<br/>
Article 2 : Les interventions du Conseil national de l'ordre des médecins devant le tribunal administratif de Marseille sont admises.<br/>
Article 3 : Les demandes présentées par M. U...et autres et par M. A... D... et Mme N... devant le juge des référés du tribunal administratif de Marseille sont rejetées. <br/>
Article 4 : Les conclusions présentées par les requérants devant le tribunal administratif de Marseille et devant le Conseil d'Etat au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à M. U..., premier requérant dénommé pour la requête enregistrée sous le n° 424118, à M. A... D..., à Mme N... et à la ministre des solidarités et de la santé.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-04-03-07-01 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. PRINCIPES GÉNÉRAUX DU DROIT. PRINCIPES INTÉRESSANT L'ACTION ADMINISTRATIVE. CONTINUITÉ DU SERVICE PUBLIC. - DISSOLUTION, PAR LE DIRECTEUR GÉNÉRAL DE L'ARS, D'UN CONSEIL DÉPARTEMENTAL DE L'ORDRE DES MÉDECINS (ART. L. 4123-10 DU CSP) - FACULTÉ OUVERTE LORSQUE, PAR LE FAIT DE SES MEMBRES, LA CONTINUITÉ DU SERVICE PUBLIC [RJ1] OU LE BON EXERCICE DES MISSIONS SONT GRAVEMENT COMPROMIS.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">55-01-02-01 PROFESSIONS, CHARGES ET OFFICES. ORDRES PROFESSIONNELS - ORGANISATION ET ATTRIBUTIONS NON DISCIPLINAIRES. QUESTIONS PROPRES À CHAQUE ORDRE PROFESSIONNEL. ORDRE DES MÉDECINS. - DISSOLUTION PAR LE DIRECTEUR GÉNÉRAL DE L'ARS, SUR PROPOSITION DU CONSEIL NATIONAL, D'UN CONSEIL DÉPARTEMENTAL (ART. L. 4123-10 DU CSP) - 1) PRINCIPE - FACULTÉ OUVERTE LORSQUE, PAR LE FAIT DE SES MEMBRES, LA CONTINUITÉ DU SERVICE PUBLIC [RJ1] OU LE BON EXERCICE DES MISSIONS SONT GRAVEMENT COMPROMIS - 2) ESPÈCE - ARRÊTÉ DISSOLVANT UN CONSEIL DÉPARTEMENTAL ET NOMMANT UNE DÉLÉGATION - A) SUSPENSION, PAR UN JRTA, DE CET ARRÊTÉ - DÉNATURATION À AVOIR JUGÉ SÉRIEUX LE MOYEN TIRÉ DE CE QUE CE CONSEIL N'ÉTAIT PAS DANS L'IMPOSSIBILITÉ DE FONCTIONNER - B) REJET DES DEMANDES DE SUSPENSION, ALORS QUE LE RENOUVELLEMENT DU CONSEIL RÉGIONAL EST IMMINENT - I) FONCTIONNEMENT DU CONSEIL DÉPARTEMENTAL - COMPÉTENCE DE LA DÉLÉGATION POUR ASSURER L'ENSEMBLE DE SES FONCTIONS - EXISTENCE - QUALITÉ DES MEMBRES DE LA DÉLÉGATION POUR ÉLIRE LES MEMBRES DU CONSEIL RÉGIONAL - ABSENCE - II) FONCTIONNEMENT DU CONSEIL RÉGIONAL - POSSIBILITÉ, POUR LUI, DE SE RÉUNIR AVANT QUE LES MEMBRES ÉLUS PAR LE CONSEIL DÉPARTEMENTAL DISSOUT NE L'AIENT ÉTÉ - EXISTENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">61-09-02-01 SANTÉ PUBLIQUE. ADMINISTRATION DE LA SANTÉ. - DISSOLUTION, PAR LE DIRECTEUR GÉNÉRAL, D'UN CONSEIL DÉPARTEMENTAL DE L'ORDRE DES MÉDECINS (ART. L. 4123-10 DU CSP) - FACULTÉ OUVERTE LORSQUE, PAR LE FAIT DE SES MEMBRES, LA CONTINUITÉ DU SERVICE PUBLIC [RJ1] OU LE BON EXERCICE DES MISSIONS SONT GRAVEMENT COMPROMIS.
</SCT>
<ANA ID="9A"> 01-04-03-07-01 Il résulte des articles L. 4121-2 et 4123-1 du code de la santé publique (CSP) et de l'ensemble des missions confiées aux instances ordinales par le titre deuxième du livre premier de la quatrième partie de ce code que le législateur a entendu faire de l'organisation et du contrôle de la profession médicale un service public, auquel concourent, alors même qu'ils ne constituent pas des établissements publics, le Conseil national de l'ordre et, dans le périmètre de leurs ressorts territoriaux, les conseils régionaux et départementaux.,,A ce titre, l'article L. 4123-10 du CSP permet au directeur général de l'agence régionale de santé (ARS) compétent, sur proposition du Conseil national de l'ordre, de prononcer la dissolution d'un conseil départemental lorsque, par le fait de ses membres, la continuité du service public ou le bon exercice des missions qu'il comporte sont gravement compromis.</ANA>
<ANA ID="9B"> 55-01-02-01 1) Il résulte des articles L. 4121-2 et 4123-1 du code de la santé publique (CSP) et de l'ensemble des missions confiées aux instances ordinales par le titre deuxième du livre premier de la quatrième partie de ce code que le législateur a entendu faire de l'organisation et du contrôle de la profession médicale un service public, auquel concourent, alors même qu'ils ne constituent pas des établissements publics, le Conseil national de l'ordre et, dans le périmètre de leurs ressorts territoriaux, les conseils régionaux et départementaux.,,A ce titre, l'article L. 4123-10 du CSP permet au directeur général de l'agence régionale de santé (ARS) compétent, sur proposition du Conseil national de l'ordre, de prononcer la dissolution d'un conseil départemental lorsque, par le fait de ses membres, la continuité du service public ou le bon exercice des missions qu'il comporte sont gravement compromis.,,2) Arrêté du 4 juillet 2018 par lequel un directeur général d'ARS a, en application de l'article L. 4123-10 du CSP, prononcé la dissolution d'un conseil départemental de l'ordre des médecins et nommé une délégation de cinq membres.... ...a) Juge des référés d'un tribunal administratif (JRTA) ayant suspendu l'exécution de cet arrêté. Le ministre est fondé à soutenir que le juge des référés a dénaturé les pièces du dossier qui lui était soumis en estimant qu'était propre à créer un doute sérieux quant à la légalité de cet arrêté le moyen tiré de ce que ce conseil départemental n'était pas dans l'impossibilité de fonctionner, au sens de l'article L. 4123-10 du CSP, alors qu'il ressortait de ce dossier que le conseil départemental était notoirement traversé de violents conflits compromettant gravement sa légitimité, que vingt-et-un des quarante six membres titulaires et suppléants avaient démissionné en contestant la légitimité des autres membres à exercer leurs fonctions, que le conseil départemental avait, en raison de ce conflit, porté plainte contre quatre de ses propres membres et que, après plusieurs tentatives de conciliation conduites sur place par des délégations du Conseil national de l'ordre des médecins, une ultime mission de conciliation effectuée par le Conseil national avait échoué.... ...b) Rejet des demandes de suspension introduites devant le JRTA, ayant pour effet de mettre fin à la suspension prononcée par le JRTA, alors que le renouvellement de l'ensemble des membres du conseil régional doit s'effectuer à la date du 7 février 2019 et que dix des trente-deux membres de ce conseil doivent, lors de ce scrutin, être élus par les membres titulaires du conseil départemental dissout.,,i) Si l'article L. 4123-10 du CSP permet à la délégation nommée par le directeur général de l'ARS d'assurer l'ensemble des fonctions du conseil départemental jusqu'à l'élection d'un nouveau conseil départemental, sans se limiter à la seule expédition des affaires courantes, elles ne confèrent pas, pour autant, aux membres de cette délégation la qualité de membres titulaires du conseil départemental, au sens du IV de l'article L. 4124-11 du même code, appelés, à ce titre, à élire les membres du conseil régional de l'ordre.,,ii) Si la participation à ce scrutin des membres titulaires du conseil départemental est, à raison de l'exercice des fonctions de ce conseil départemental par la délégation, impossible à la date à laquelle les autres conseils départementaux de la région éliront leurs représentants, le conseil régional nouvellement élu ne comportera de membres représentant ce conseil départemental que lorsque ceux de ses membres qui doivent être élus par les membres titulaires de ce conseil départemental l'auront été, après l'élection d'un nouveau conseil départemental.</ANA>
<ANA ID="9C"> 61-09-02-01 Il résulte des articles L. 4121-2 et 4123-1 du code de la santé publique (CSP) et de l'ensemble des missions confiées aux instances ordinales par le titre deuxième du livre premier de la quatrième partie de ce code que le législateur a entendu faire de l'organisation et du contrôle de la profession médicale un service public, auquel concourent, alors même qu'ils ne constituent pas des établissements publics, le Conseil national de l'ordre et, dans le périmètre de leurs ressorts territoriaux, les conseils régionaux et départementaux.,,A ce titre, l'article L. 4123-10 du CSP permet au directeur général de l'agence régionale de santé (ARS) compétent, sur proposition du Conseil national de l'ordre, de prononcer la dissolution d'un conseil départemental lorsque, par le fait de ses membres, la continuité du service public ou le bon exercice des missions qu'il comporte sont gravement compromis.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. sur le concours apporté par l'ordre des médecins au fonctionnement du service public, CE, Assemblée, 2 avril 1943,,, n° 72210, p. 86.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
