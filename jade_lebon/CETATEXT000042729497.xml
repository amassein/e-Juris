<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042729497</ID>
<ANCIEN_ID>JG_L_2020_12_000000439800</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/72/94/CETATEXT000042729497.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 22/12/2020, 439800</TITRE>
<DATE_DEC>2020-12-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>439800</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Réda Wadjinny-Green</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Domingo</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:439800.20201222</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>1°) Sous le numéro 439800, par une ordonnance n° 2005459 du 25 mars 2020, enregistrée le 25 mars 2020 au secrétariat du contentieux du Conseil d'Etat, le président du tribunal administratif de Paris a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code dejustice administrative, la requête enregistrée le 18 mars 2020 au greffe de ce tribunal, présentée par M. B... A.... Par cette requête, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2020-260 du 16 mars 2020 ;<br/>
              2°) d'enjoindre à l'Etat de retirer ce décret sous astreinte de 500 euros par jour de retard ; <br/>
              3°) de mettre à la charge de l'Etat une somme de 1 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              2°) Sous le numéro 439818, par une requête enregistrée le 23 mars 2020 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2020-260 du 16 mars 2020 ;<br/>
              2°) d'enjoindre à l'Etat de retirer ce décret sous astreinte de 500 euros par jour de retard ; <br/>
              3°) de mettre à la charge de l'Etat une somme de 1 000 euros au titre de l'article L. 761-1 du code de justice administrative.  <br/>
<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              3°) Sous le numéro 439855, par une ordonnance n° 2000684 du 27 mars 2020, enregistrée le 30 mars 2020 au secrétariat du contentieux du Conseil d'Etat, le président du tribunal administratif de Pau a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, la requête et le mémoire enregistrés le 17 mars 2020 au greffe de ce tribunal, présentés par M. A.... Par cette requête, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2020-260 du 16 mars 2020 ;<br/>
              2°) d'enjoindre à l'Etat de retirer ce décret sous astreinte de 500 euros par jour de retard ; <br/>
              3°) de mettre à la charge de l'Etat une somme de 1 000 euros au titre de l'article L. 761-1 du code de justice administrative.  <br/>
<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu :<br/>
              - la Constitution ; <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et son protocole n°4 ;<br/>
              - le code de la santé publique ;<br/>
              - le décret n° 2020-260 du 16 mars 2020 ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Réda Wadjinny-Green, auditeur-rapporteur,  <br/>
<br/>
              - les conclusions de M. Laurent Domingo, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1.	Les requêtes enregistrées sous les n°s 439800, 439818 et 439855 tendent à l'annulation pour excès de pouvoir du même décret. Il y a lieu de les joindre pour statuer par une seule décision.<br/>
<br/>
              2.	L'émergence d'un nouveau coronavirus (covid-19), de caractère pathogène et particulièrement contagieux et sa propagation sur le territoire français ont conduit le ministre des solidarités et de la santé à prendre, par plusieurs arrêtés à compter du 4 mars 2020, des mesures sur le fondement des dispositions de l'article L. 3131-1 du code de la santé publique. Par un décret du 16 mars 2020 motivé par les circonstances exceptionnelles découlant de l'épidémie de covid-19, le Premier ministre a interdit le déplacement de toute personne hors de son domicile, sous réserve d'exceptions limitativement énumérées et devant être dûment justifiées, à compter du 17 mars à 12h, sans préjudice de mesures plus strictes susceptibles d'être arrêtées par le représentant de l'Etat dans le département. M. A... demande l'annulation pour excès de pouvoir de ce décret.<br/>
<br/>
              3.	En premier lieu, le Premier ministre peut, en vertu de ses pouvoirs propres, édicter des mesures de police applicables à l'ensemble du territoire, en particulier en cas d'épidémie, comme celle de covid-19 que traversait la France à la date des décisions attaquées. Ces mesures, qui peuvent limiter l'exercice des droits et libertés fondamentaux, comme la liberté d'aller et venir, doivent, dans cette mesure, être nécessaires, adaptées et proportionnées à l'objectif de sauvegarde de la santé publique qu'elles poursuivent. Dès lors, le moyen tiré de ce que le Premier ministre n'était pas compétent pour prendre le décret attaqué afin de lutter contre la propagation du virus covid-19 doit être écarté.<br/>
<br/>
              4.	En deuxième lieu, eu égard, d'une part, aux circonstances exceptionnelles dans lesquelles a été adopté le décret attaqué, caractérisées par une augmentation rapide de la circulation du virus, une possible saturation, à brève échéance, des structures hospitalières à l'échelle nationale, qui a conduit au transfert de patients entre régions et vers des pays voisins ainsi qu'à la déprogrammation d'hospitalisations non urgentes, des difficultés dans le traitement des chaines de contamination et pour le respect des gestes barrières en raison de l'insuffisance du nombre de tests, qui ne permettait pas d'identifier les personnes asymptomatiques, et de la pénurie de masques chirurgicaux et FFP2 et, d'autre part, aux dérogations prévues pour les déplacements répondant à des besoins de première nécessité et au caractère strictement circonscrit dans le temps de l'interdiction de déplacement, une telle interdiction applicable à l'ensemble du territoire national ne présentait pas, à la date à laquelle elle a été édictée et au regard de l'objectif de protection de la santé publique poursuivi, un caractère disproportionné, en dépit des disparités observées entre départements en termes de prévalence de l'épidémie et malgré la gravité de l'atteinte ainsi portée à la liberté d'aller et venir et au droit au respect de la vie privée et familiale. <br/>
<br/>
              5.	En troisième lieu, il résulte de ce qui a été dit au point précédent que le requérant n'est pas fondé à soutenir que les dispositions attaquées, qui poursuivaient un objectif de santé publique, seraient entachées de détournement de pouvoir. <br/>
<br/>
              6.	Il résulte de ce qui précède que les requêtes de M. A... doivent être rejetées, y compris les conclusions tendant au prononcé d'une injonction et d'une astreinte et les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les requêtes de M. A... sont rejetées.<br/>
Article 2 : La présente décision sera notifiée à M. B... A... et au ministre des solidarités et de la santé. Copie en sera adressée au Premier ministre et au ministre de l'intérieur. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-02-02-01-02 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - COMPÉTENCE. RÉPARTITION DES COMPÉTENCES ENTRE AUTORITÉS DISPOSANT DU POUVOIR RÉGLEMENTAIRE. AUTORITÉS DISPOSANT DU POUVOIR RÉGLEMENTAIRE. PREMIER MINISTRE. - CAS D'ÉPIDÉMIE AVÉRÉE - COMPÉTENCE DU PREMIER MINISTRE, EN VERTU DE SES POUVOIRS PROPRES [RJ1], POUR PRENDRE DES MESURES APPLICABLES À L'ENSEMBLE DU TERRITOIRE AFIN DE LUTTER CONTRE LA PROPAGATION DU VIRUS [RJ2].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">61-01-01-02 SANTÉ PUBLIQUE. PROTECTION GÉNÉRALE DE LA SANTÉ PUBLIQUE. POLICE ET RÉGLEMENTATION SANITAIRE. LUTTE CONTRE LES ÉPIDÉMIES. - CAS D'ÉPIDÉMIE AVÉRÉE - COMPÉTENCE DU PREMIER MINISTRE, EN VERTU DE SES POUVOIRS PROPRES [RJ1], POUR PRENDRE DES MESURES APPLICABLES À L'ENSEMBLE DU TERRITOIRE AFIN DE LUTTER CONTRE LA PROPAGATION DU VIRUS [RJ2].
</SCT>
<ANA ID="9A"> 01-02-02-01-02 Décret n° 2020-260 du 16 mars 2020, pris avant l'entrée en vigueur de la loi n° 2020-290 du 23 mars créant le régime d'état d'urgence sanitaire, instaurant un premier confinement afin de lutter contre l'épidémie de covid-19 et interdisant le déplacement de toute personne hors de son domicile, sous réserve d'exceptions limitativement énumérées et devant être dûment justifiées... ,,Le Premier ministre peut, en vertu de ses pouvoirs propres, édicter des mesures de police applicables à l'ensemble du territoire, en particulier en cas d'épidémie, comme celle de covid-19 que traversait la France à la date du décret attaqué. Ces mesures, qui peuvent limiter l'exercice des droits et libertés fondamentaux, comme la liberté d'aller et venir, doivent, dans cette mesure, être nécessaires, adaptées et proportionnées à l'objectif de sauvegarde de la santé publique qu'elles poursuivent.... ,,Dès lors, le Premier ministre était compétent pour prendre le décret attaqué afin de lutter contre la propagation du virus covid-19.</ANA>
<ANA ID="9B"> 61-01-01-02 Décret n° 2020-260 du 16 mars 2020, pris avant l'entrée en vigueur de la loi n° 2020-290 du 23 mars créant le régime d'état d'urgence sanitaire, instaurant un premier confinement afin de lutter contre l'épidémie de covid-19 et interdisant le déplacement de toute personne hors de son domicile, sous réserve d'exceptions limitativement énumérées et devant être dûment justifiées... ,,Le Premier ministre peut, en vertu de ses pouvoirs propres, édicter des mesures de police applicables à l'ensemble du territoire, en particulier en cas d'épidémie, comme celle de covid-19 que traversait la France à la date du décret attaqué. Ces mesures, qui peuvent limiter l'exercice des droits et libertés fondamentaux, comme la liberté d'aller et venir, doivent, dans cette mesure, être nécessaires, adaptées et proportionnées à l'objectif de sauvegarde de la santé publique qu'elles poursuivent.... ,,Dès lors, le Premier ministre était compétent pour prendre le décret attaqué afin de lutter contre la propagation du virus covid-19.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 8 août 1919, Labonne, n° 56377, p. 737.,,[RJ2] Cf. CE, juge des référés, 22 mars 2020, Syndicat Jeunes Médecins, n° 439674, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
