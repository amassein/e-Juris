<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034993700</ID>
<ANCIEN_ID>JG_L_2017_06_000000400366</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/99/37/CETATEXT000034993700.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 22/06/2017, 400366</TITRE>
<DATE_DEC>2017-06-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>400366</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FABIANI, LUC-THALER, PINATEL</AVOCATS>
<RAPPORTEUR>M. Yves Doutriaux</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Béatrice Bourgeois-Machureau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:400366.20170622</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé à la Cour nationale du droit d'asile d'annuler la décision du 18 septembre 2013 par laquelle le directeur général de l'Office français de protection des réfugiés et apatrides a rejeté sa demande d'asile, et de lui reconnaître la qualité de réfugié ou, à défaut, de lui accorder la protection subsidiaire.<br/>
<br/>
              Par une décision n° 13027358 du 26 janvier 2016, la Cour nationale du droit d'asile a rejeté cette demande.<br/>
<br/>
              Par un pourvoi, enregistré le 3 juin 2016 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette décision ;<br/>
<br/>
               2°) de renvoyer l'affaire à la Cour nationale du droit d'asile ; <br/>
<br/>
              3°) de mettre à la charge de l'Office français de protection des réfugiés et apatrides la somme de 4 000 euros à verser à la SCP Fabiani, Luc-Thaler, Pinatel, son avocat, au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - la convention de Genève du 28 juillet 1951 relative au statut des réfugiés et le protocole signé à New-York le 31 janvier 1967 ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Yves Doutriaux, conseiller d'Etat,<br/>
<br/>
              - les conclusions de Mme Béatrice Bourgeois-Machureau, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Fabiani, Luc-Thaler, Pinatel, avocat de M. B...; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article L. 733-5 du code de l'entrée et du séjour des étrangers et du droit d'asile : " Saisie d'un recours contre une décision du directeur général de l'Office français de protection des réfugiés et apatrides, la Cour nationale du droit d'asile statue, en qualité de juge de plein contentieux, sur le droit du requérant à une protection au titre de l'asile au vu des circonstances de fait dont elle a connaissance au moment où elle se prononce. / La cour ne peut annuler une décision du directeur général de l'office et lui renvoyer l'examen de la demande d'asile que lorsqu'elle juge que l'office a pris cette décision sans procéder à un examen individuel de la demande ou en se dispensant, en dehors des cas prévus par la loi, d'un entretien personnel avec le demandeur et qu'elle n'est pas en mesure de prendre immédiatement une décision positive sur la demande de protection au vu des éléments établis devant elle " ; qu'en application de ces dispositions, le moyen tiré de ce que l'entretien personnel du demandeur d'asile à l'Office se serait déroulé dans de mauvaises conditions n'est pas de nature à justifier que la Cour nationale du droit d'asile annule une décision du directeur général de l'Office et lui renvoie l'examen de la demande d'asile ; qu'en revanche, il revient à la Cour de procéder à cette annulation et à ce renvoi si elle juge que le demandeur a été dans l'impossibilité de se faire comprendre lors de cet entretien, faute d'avoir pu bénéficier du concours d'un interprète dans la langue qu'il a choisie dans sa demande d'asile ou dans une autre langue dont il a une connaissance suffisante, et que ce défaut d'interprétariat est imputable à l'Office ; que, par suite, en jugeant inopérant le moyen tiré de ce que M. B...n'avait pu s'exprimer dans une langue qu'il comprenait lors de son entretien avec un agent de l'Office sans rechercher si le défaut d'interprétariat allégué était imputable à l'Office, la Cour a commis une erreur de droit ; que, par suite, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, sa décision doit être annulée ;<br/>
<br/>
              2. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Office français de protection des réfugiés et apatrides le versement d'une somme de 3 000 euros au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991, à verser à la SCP Fabiani, Luc-Thaler, Pinatel, avocat de M.B..., sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La décision de la Cour nationale du droit d'asile du 26 janvier 2016 est annulée.<br/>
Article 2 : L'affaire est renvoyée devant la Cour nationale du droit d'asile.<br/>
Article 3 : L'Office français de protection des réfugiés et apatrides versera à la SCP Fabiani, Luc-Thaler, Pinatel, avocat de M.B..., une somme de 3 000 euros au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
Article 4 : La présente décision sera notifiée à M. A...B...et à l'Office français de protection des réfugiés et apatrides.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">095-08-05-02 - MOYEN TIRÉ DE CE QUE L'ENTRETIEN PERSONNEL DU DEMANDEUR D'ASILE À L'OFFICE SE SERAIT DÉROULÉ DANS DE MAUVAISES CONDITIONS - MOYEN DE NATURE À JUSTIFIER L'ANNULATION DE LA DÉCISION DE L'OFPRA - ABSENCE, SAUF SI LE DEMANDEUR A ÉTÉ DANS L'IMPOSSIBILITÉ DE SE FAIRE COMPRENDRE, FAUTE POUR L'OFPRA DE LUI AVOIR PERMIS DE BÉNÉFICIER DU CONCOURS D'UN INTERPRÈTE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-07-03 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. POUVOIRS DU JUGE DE PLEIN CONTENTIEUX. - RECOURS DIRIGÉ CONTRE UNE DÉCISION DU DIRECTEUR GÉNÉRAL DE L'OFPRA REFUSANT DE RECONNAÎTRE LA QUALITÉ DE RÉFUGIÉ OU D'ACCORDER LA PROTECTION SUBSIDIAIRE - OFFICE DU JUGE - MOYEN TIRÉ DE CE QUE L'ENTRETIEN PERSONNEL DU DEMANDEUR D'ASILE À L'OFFICE SE SERAIT DÉROULÉ DANS DE MAUVAISES CONDITIONS - MOYEN DE NATURE À JUSTIFIER L'ANNULATION DE LA DÉCISION DE L'OFPRA - ABSENCE, SAUF SI LE DEMANDEUR A ÉTÉ DANS L'IMPOSSIBILITÉ DE SE FAIRE COMPRENDRE, FAUTE POUR L'OFPRA DE LUI AVOIR PERMIS DE BÉNÉFICIER DU CONCOURS D'UN INTERPRÈTE [RJ1].
</SCT>
<ANA ID="9A"> 095-08-05-02 En application de l'article L. 733-5 du code de l'entrée et du séjour des étrangers et du droit d'asile, le moyen tiré de ce que l'entretien personnel du demandeur d'asile à l'Office français de protection des réfugiés et apatrides (OFPRA) se serait déroulé dans de mauvaises conditions n'est pas de nature à justifier que la Cour nationale du droit d'asile annule une décision du directeur général de l'Office et lui renvoie l'examen de la demande d'asile. En revanche, il revient à la Cour de procéder à cette annulation et à ce renvoi si elle juge que le demandeur a été dans l'impossibilité de se faire comprendre lors de cet entretien, faute d'avoir pu bénéficier du concours d'un interprète dans la langue qu'il a choisie dans sa demande d'asile ou dans une autre langue dont il a une connaissance suffisante, et que ce défaut d'interprétariat est imputable à l'Office.</ANA>
<ANA ID="9B"> 54-07-03 En application de l'article L. 733-5 du code de l'entrée et du séjour des étrangers et du droit d'asile, le moyen tiré de ce que l'entretien personnel du demandeur d'asile à l'Office français de protection des réfugiés et apatrides (OFPRA) se serait déroulé dans de mauvaises conditions n'est pas de nature à justifier que la Cour nationale du droit d'asile annule une décision du directeur général de l'Office et lui renvoie l'examen de la demande d'asile. En revanche, il revient à la Cour de procéder à cette annulation et à ce renvoi si elle juge que le demandeur a été dans l'impossibilité de se faire comprendre lors de cet entretien, faute d'avoir pu bénéficier du concours d'un interprète dans la langue qu'il a choisie dans sa demande d'asile ou dans une autre langue dont il a une connaissance suffisante, et que ce défaut d'interprétariat est imputable à l'Office.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Rappr. CE, 10 octobre 2013, OFPRA c/ M.,, n° 362798, 362799, p. 254 ; CE, 27 février 2015, OFPRA c/ M.,, n° 380489, T. pp. 561-835.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
