<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044172602</ID>
<ANCIEN_ID>JG_L_2021_10_000000442182</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/17/26/CETATEXT000044172602.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 4ème chambres réunies, 06/10/2021, 442182, Publié au recueil Lebon</TITRE>
<DATE_DEC>2021-10-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>442182</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GASCHIGNARD ; SCP SEVAUX, MATHONNET</AVOCATS>
<RAPPORTEUR>Mme Bénédicte Fauvarque-Cosson</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Villette</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:442182.20211006</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A... D..., Mme E... B... épouse D..., M. C... D... et M. F... D... ont demandé au tribunal administratif de Toulon d'annuler pour excès de pouvoir l'arrêté du 9 juin 2017 par lequel le maire de Saint-Cyr-sur-Mer a accordé à la société civile immobilière Marésias un permis de construire pour la réalisation de travaux d'augmentation de la surface de plancher de 40 m² sur un logement, des modifications des façades, toitures et aménagement du terrain et la création de 5 places de stationnement, ainsi que le rejet de leur recours gracieux. Par un jugement n° 1704275 du 10 avril 2020, le tribunal administratif de Toulon a fait droit à cette demande.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 24 juillet et 26 octobre 2020 et le 3 septembre 2021 au secrétariat du contentieux du Conseil d'Etat, la société Marésias demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) de mettre à la charge des consorts D... la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'urbanisme ;<br/>
              - la loi n° 2018-1021 du 23 novembre 2018 ;<br/>
               - l'ordonnance n° 2020-305 du 25 mars 2020 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Bénédicte Fauvarque-Cosson, conseillère d'Etat,  <br/>
<br/>
              - les conclusions de M. Vincent Villette, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Gaschignard, avocat de la société Maresias et à la SCP Sevaux, Mathonnet, avocat de M. et Mme D..., de M. C... D... et de M. F... D... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la société Marésias a acquis, en août 2012, une villa située à Saint-Cyr-sur-Mer, édifiée au titre d'un permis de construire délivré le 15 mai 1962. Le 18 mars 2014, le maire de Saint-Cyr-sur-Mer a décidé de ne pas s'opposer à la déclaration de travaux déposée par la société en vue de la réhabilitation de la construction existante, cette réhabilitation comportant l'agrandissement des ouvertures de la façade sud, la réfection de la toiture et du garage. Par un arrêté du 9 juin 2017, le maire de Saint-Cyr-sur-Mer a délivré à la même société un permis de construire pour la réalisation de travaux d'augmentation de la surface de plancher de 40 m², la modification des façades, toitures et aménagements du terrain, ainsi que pour la création de 5 places de stationnement. Par un jugement du 10 avril 2020, contre lequel la société Marésias se pourvoit en cassation, le tribunal administratif de Toulon, saisi par les consorts D..., voisins immédiats du projet, a annulé cet arrêté ainsi que le rejet du recours gracieux des requérants.<br/>
<br/>
              2. Lorsqu'une construction a été édifiée sans autorisation en méconnaissance des prescriptions légales alors applicables, il appartient au propriétaire qui envisage d'y faire de nouveaux travaux de présenter une demande d'autorisation d'urbanisme portant sur l'ensemble du bâtiment. De même, lorsqu'une construction a été édifiée sans respecter la déclaration préalable déposée ou le permis de construire obtenu ou a fait l'objet de transformations sans les autorisations d'urbanisme requises, il appartient au propriétaire qui envisage d'y faire de nouveaux travaux de présenter une demande d'autorisation d'urbanisme portant sur l'ensemble des éléments de la construction qui ont eu ou auront pour effet de modifier le bâtiment tel qu'il avait été initialement approuvé. Il en va ainsi même dans le cas où les éléments de construction résultant de ces travaux ne prennent pas directement appui sur une partie de l'édifice réalisée sans autorisation. Dans l'hypothèse où l'autorité administrative est saisie d'une demande qui ne satisfait pas à cette exigence, elle doit inviter son auteur à présenter une demande portant sur l'ensemble des éléments devant être soumis à son autorisation. Cette invitation, qui a pour seul objet d'informer le pétitionnaire de la procédure à suivre s'il entend poursuivre son projet, n'a pas à précéder le refus que l'administration doit opposer à une demande portant sur les seuls nouveaux travaux envisagés.<br/>
<br/>
              3. En premier lieu, pour juger que le maire de Saint-Cyr-sur-Mer n'avait pu légalement délivrer le 9 juin 2017 le permis de construire sollicité faute que le pétitionnaire ait présenté une demande de permis de construire portant sur l'ensemble des éléments de la construction qui avaient eu ou allaient avoir pour effet de modifier le bâtiment tel qu'il avait été initialement autorisé par le permis de construire délivré en 1962, le tribunal administratif de Toulon a tout d'abord relevé, sans que son jugement soit contesté sur ce point, que le garage accolé à la maison n'avait pas été autorisé par le permis de construire initial, que la toiture de la maison, initialement prévue en terrasse, avait été transformée en une toiture à pans inclinés et que les ouvertures de la façade nord du bâtiment avaient été modifiées. Il a ensuite jugé que la décision de non opposition du 18 mars 2014 ne pouvait être regardée comme ayant eu pour effet d'autoriser l'implantation du garage et d'autoriser les transformations de la toiture et des ouvertures de la façade nord dès lors que le dossier de déclaration de travaux ne portait que sur la réhabilitation projetée du garage et de la toiture, sans comporter aucun élément relatif aux travaux antérieurs dont ils étaient issus, et comprenait seulement des plans et photographies de l'existant, n'incluant d'ailleurs pas de plan de la façade nord. En statuant ainsi, le tribunal administratif de Toulon a suffisamment motivé son jugement au regard de l'argumentation opposée en défense et a souverainement apprécié, sans les dénaturer, les pièces du dossier qui lui était soumis.<br/>
<br/>
              4. En second lieu, aux termes de l'article L. 600-5 du code de l'urbanisme : " Sans préjudice de la mise en œuvre de l'article L. 600-5-1, le juge administratif qui, saisi de conclusions dirigées contre un permis de construire, de démolir ou d'aménager ou contre une décision de non-opposition à déclaration préalable, estime, après avoir constaté que les autres moyens ne sont pas fondés, qu'un vice n'affectant qu'une partie du projet peut être régularisé, limite à cette partie la portée de l'annulation qu'il prononce et, le cas échéant, fixe le délai dans lequel le titulaire de l'autorisation pourra en demander la régularisation, même après l'achèvement des travaux. Le refus par le juge de faire droit à une demande d'annulation partielle est motivé. " Aux termes de l'article L. 600-5-1 du code de l'urbanisme : " Sans préjudice de la mise en œuvre de l'article L. 600-5, le juge administratif qui, saisi de conclusions dirigées contre un permis de construire, de démolir ou d'aménager ou contre une décision de non-opposition à déclaration préalable estime, après avoir constaté que les autres moyens ne sont pas fondés, qu'un vice entraînant l'illégalité de cet acte est susceptible d'être régularisé, sursoit à statuer, après avoir invité les parties à présenter leurs observations, jusqu'à l'expiration du délai qu'il fixe pour cette régularisation, même après l'achèvement des travaux. Si une mesure de régularisation est notifiée dans ce délai au juge, celui-ci statue après avoir invité les parties à présenter leurs observations. Le refus par le juge de faire droit à une demande de sursis à statuer est motivé. "<br/>
<br/>
              5. Il résulte de ces dispositions, éclairées par les travaux parlementaires ayant conduit à l'adoption de la loi du 23 novembre 2018 portant évolution du logement, de l'aménagement et du numérique, que lorsque le ou les vices affectant la légalité de l'autorisation d'urbanisme dont l'annulation est demandée sont susceptibles d'être régularisés, le juge doit, en application de l'article L. 600-5-1 du code de l'urbanisme, surseoir à statuer sur les conclusions dont il est saisi contre cette autorisation, sauf à ce qu'il fasse le choix de recourir à l'article L. 600-5 du code de l'urbanisme, si les conditions posées par cet article sont réunies, ou que le bénéficiaire de l'autorisation lui ait indiqué qu'il ne souhaitait pas bénéficier d'une mesure de régularisation. Un vice entachant le bien-fondé de l'autorisation d'urbanisme est susceptible d'être régularisé, même si cette régularisation implique de revoir l'économie générale du projet en cause, dès lors que les règles d'urbanisme en vigueur à la date à laquelle le juge statue permettent une mesure de régularisation qui n'implique pas d'apporter à ce projet un bouleversement tel qu'il en changerait la nature même. <br/>
<br/>
              6. Toutefois, lorsque l'autorité administrative, saisie dans les conditions mentionnées au point 2 d'une demande ne portant pas sur l'ensemble des éléments qui devaient lui être soumis, a illégalement accordé l'autorisation de construire qui lui était demandée au lieu de refuser de la délivrer et de se borner à inviter le pétitionnaire à présenter une nouvelle demande portant sur l'ensemble des éléments ayant modifié ou modifiant la construction par rapport à ce qui avait été initialement autorisé, cette illégalité ne peut être regardée comme un vice susceptible de faire l'objet d'une mesure de régularisation en application de l'article L. 600-5-1 du code de l'urbanisme ou d'une annulation partielle en application de l'article L. 600-5 du même code. Par suite, la société Marésias n'est pas fondée à soutenir que le tribunal administratif de Toulon a commis une erreur de droit ou dénaturé les faits de l'espèce en jugeant que, compte tenu du motif d'annulation qu'il retenait, il n'y avait pas lieu pour lui de mettre en œuvre ces dispositions.<br/>
<br/>
              7. Il résulte de tout ce qui précède la société Marésias n'est pas fondée à demander l'annulation du jugement qu'elle attaque.<br/>
<br/>
              8. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge des consorts D... qui ne sont pas, dans la présente instance, la partie perdante. Dans les circonstances de l'espèce il y a lieu de mettre à la charge de la société Marésias une somme de 3 000 euros à verser aux consorts D... au titre des mêmes dispositions.<br/>
<br/>
<br/>
<br/>
<br/>
              D E C I D E :<br/>
              --------------<br/>
Article 1er : Le pourvoi de la société Marésias est rejeté.<br/>
Article 2 : La société Marésias versera aux consorts D... une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la société civile immobilière Marésias et à M. A... D..., pour l'ensemble des défendeurs.<br/>
Copie en sera adressée à la commune de Saint-Cyr-sur-Mer.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">68-03-01-01 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. - PERMIS DE CONSTRUIRE. - TRAVAUX SOUMIS AU PERMIS. - PRÉSENTENT CE CARACTÈRE. - TRAVAUX PORTANT SUR UNE CONSTRUCTION IRRÉGULIÈREMENT ÉDIFIÉE OU TRANSFORMÉE - 1) A) OBLIGATION DE DEMANDER L'AUTORISATION DES TRAVAUX PASSÉS IRRÉGULIERS EN MÊME TEMPS QUE DES NOUVEAUX TRAVAUX ENVISAGÉS - EXISTENCE [RJ1] - B) OFFICE DE L'ADMINISTRATION - 2) AUTORISATION DÉLIVRÉE EN MÉCONNAISSANCE DE CE PRINCIPE - POSSIBILITÉ, POUR LE JUGE, DE PRONONCER UN SURSIS À STATUER EN VUE DE LA RÉGULARISATION D'UNE AUTORISATION D'URBANISME (ART. L. 600-5-1 DU CODE DE L'URBANISME) OU UNE ANNULATION PARTIELLE (ART. L. 600-5) - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">68-06-04 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. - RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. - POUVOIRS DU JUGE. - SURSIS À STATUER EN VUE DE LA RÉGULARISATION D'UNE AUTORISATION D'URBANISME (ART. L. 600-5-1 DU CODE DE L'URBANISME) OU ANNULATION PARTIELLE (ART. L. 600-5) - CHAMP D'APPLICATION - EXCLUSION - AUTORISATION DÉLIVRÉE EN MÉCONNAISSANCE DE L'OBLIGATION, POUR LE PÉTITIONNAIRE, DE PRÉSENTER UNE DEMANDE PORTANT SUR LES TRAVAUX IRRÉGULIÈREMENT EFFECTUÉS EN MÊME TEMPS QUE LES NOUVEAUX TRAVAUX ENVISAGÉS SUR LE MÊME BÂTIMENT [RJ2].
</SCT>
<ANA ID="9A"> 68-03-01-01 1) a) Lorsqu'une construction a été édifiée sans autorisation en méconnaissance des prescriptions légales alors applicables, il appartient au propriétaire qui envisage d'y faire de nouveaux travaux de présenter une demande d'autorisation d'urbanisme portant sur l'ensemble du bâtiment. De même, lorsqu'une construction a été édifiée sans respecter la déclaration préalable déposée ou le permis de construire obtenu ou a fait l'objet de transformations sans les autorisations d'urbanisme requises, il appartient au propriétaire qui envisage d'y faire de nouveaux travaux de présenter une demande d'autorisation d'urbanisme portant sur l'ensemble des éléments de la construction qui ont eu ou auront pour effet de modifier le bâtiment tel qu'il avait été initialement approuvé. Il en va ainsi même dans le cas où les éléments de construction résultant de ces travaux ne prennent pas directement appui sur une partie de l'édifice réalisée sans autorisation. ......b) Dans l'hypothèse où l'autorité administrative est saisie d'une demande qui ne satisfait pas à cette exigence, elle doit inviter son auteur à présenter une demande portant sur l'ensemble des éléments devant être soumis à son autorisation. Cette invitation, qui a pour seul objet d'informer le pétitionnaire de la procédure à suivre s'il entend poursuivre son projet, n'a pas à précéder le refus que l'administration doit opposer à une demande portant sur les seuls nouveaux travaux envisagés.......2) Lorsque l'autorité administrative, saisie dans les conditions mentionnées au point précédent d'une demande ne portant pas sur l'ensemble des éléments qui devaient lui être soumis, a illégalement accordé l'autorisation de construire qui lui était demandée au lieu de refuser de la délivrer et de se borner à inviter le pétitionnaire à présenter une nouvelle demande portant sur l'ensemble des éléments ayant modifié ou modifiant la construction par rapport à ce qui avait été initialement autorisé, cette illégalité ne peut être regardée comme un vice susceptible de faire l'objet d'une mesure de régularisation en application de l'article L. 600-5-1 du code de l'urbanisme ou d'une annulation partielle en application de l'article L. 600-5 du même code.</ANA>
<ANA ID="9B"> 68-06-04 Lorsque l'autorité administrative, saisie d'une demande relative à des travaux projetés sur une construction irrégulière édifiée ou modifiée qui ne porte pas sur l'ensemble des éléments de la construction qui ont eu ou auront pour effet de modifier le bâtiment tel qu'il avait été initialement approuvé, a illégalement accordé l'autorisation de construire qui lui était demandée au lieu de refuser de la délivrer et de se borner à inviter le pétitionnaire à présenter une nouvelle demande portant sur l'ensemble des éléments qu'il aurait lui dû soumettre, cette illégalité ne peut être regardée comme un vice susceptible de faire l'objet d'une mesure de régularisation en application de l'article L. 600-5-1 du code de l'urbanisme ou d'une annulation partielle en application de l'article L. 600-5 du même code.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 9 juillet 1986, Thalamy, n° 51172, p. 201 ; CE, 13 décembre 2013, Mme Carn et autres, n° 349081, T. pp. 879-882....[RJ2] Cf., sur cette obligation, CE, 9 juillet 1986, Thalamy, n° 51172, p. 201 ; CE, 13 décembre 2013, Mme Carn et autres, n° 349081, T. pp. 879-882.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
