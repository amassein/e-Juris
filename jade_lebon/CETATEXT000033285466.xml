<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033285466</ID>
<ANCIEN_ID>JG_L_2016_10_000000395494</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/28/54/CETATEXT000033285466.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 20/10/2016, 395494</TITRE>
<DATE_DEC>2016-10-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>395494</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Interprétation</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROUSSEAU, TAPIE ; SCP LEVIS</AVOCATS>
<RAPPORTEUR>M. Paul Bernard</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier Domino</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:395494.20161020</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              La SARL Oxial a demandé au tribunal administratif de Dijon d'annuler les arrêtés du 6 août 2013 par lequel le maire de Dijon a retiré les décisions du 31 janvier 2013 par lesquelles il l'avait tacitement autorisée à implanter des dispositifs de publicité lumineuse au 13 boulevard Machureau, au 2 rue de l'Ecluse, au 31 rue Arthur Deroye, au 1 boulevard Pascal et au 1 avenue du Mont Blanc, sur le territoire de la commune. <br/>
<br/>
              Par cinq jugements n° 1400336, 1400338, 1400339, 1400340 et 1400342 du 30 octobre 2014, le tribunal administratif de Dijon a annulé ces arrêtés.<br/>
<br/>
              Par un arrêt n° 14LY03954, 14LY03955, 14LY03957, 14LY03958 et 14LY03959 du 22 octobre 2015, la cour administrative d'appel de Lyon a rejeté les appels formés par la commune de Dijon contre ces jugements. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et deux mémoires en réplique, enregistrés le 22 décembre 2015 et les 15 mars, 13 juillet et 5 octobre 2016 au secrétariat du contentieux du Conseil d'Etat, la commune de Dijon demande au Conseil d'Etat :<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de la SARL Oxial une somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'environnement ;<br/>
              - le code de la route ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Paul Bernard, maître des requêtes, <br/>
<br/>
              - les conclusions de M. Xavier Domino, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Levis, avocat de la commune de Dijon, et à la SCP Rousseau, Tapie, avocat de la société Oxial. <br/>
<br/>
              Vu la note en délibéré, enregistrée le 7 octobre 2016, présentée par la société Oxial ; <br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la société Oxial a déposé des demandes d'autorisation pour installer cinq dispositifs de publicité lumineuse sur le territoire de la commune de Dijon ; que ces demandes ont été rejetées par des décisions des 1er juin, 8 juin et 30 juillet 2012 ; qu'elle a déposé, pour les mêmes emplacements, de nouvelles demandes d'autorisation préalable le 30 novembre 2012 ; que la société, estimant qu'elle détenait des autorisations tacites, a installé les dispositifs publicitaires lumineux ; que, par un arrêté du 6 août 2013, le maire de Dijon a confirmé les refus opposés à la société Oxial en indiquant retirer les autorisations tacites nées le 31 janvier 2013 ; que, par cinq jugements du 30 octobre 2014, le tribunal administratif de Dijon a annulé ces arrêtés en jugeant que la société détenait des autorisations tacites d'affichage devenues définitives à la suite de ces demandes du 30 novembre 2012 ; que, par un arrêt du 22 octobre 2015, la cour administrative d'appel de Lyon, d'une part, a estimé que les demandes du 30 novembre 2012 avaient en réalité le caractère de recours gracieux contre les refus des 1er juin, 8 juin et 30 juillet 2012 et que le tribunal administratif avait à tort jugé que la société détenait des autorisations tacites mais, d'autre part, a jugé que les refus opposés par la commune de Dijon étaient illégaux et a rejeté les appels formés par la commune de Dijon contre les jugements du tribunal administratif ; que la commune de Dijon se pourvoit en cassation contre cet arrêt ; <br/>
<br/>
              Sur les conclusions à fin de non-lieu présentées par la société Oxial : <br/>
<br/>
              2.	Considérant que la circonstance que la commune de Dijon n'aurait pas répondu à un courrier de la société Oxial du 23 octobre 2015 confirmant ses précédentes demandes à la suite de l'arrêt attaqué ne saurait avoir pour effet de priver d'objet le pourvoi de la commune de Dijon ; que, par suite, les conclusions à fin de non-lieu présentées par la société Oxial ne peuvent qu'être rejetées ;<br/>
<br/>
              Sur le pourvoi :<br/>
<br/>
              3.	Considérant qu'aux termes de l'article L. 581-3 du code de l'environnement : " Constitue une publicité, à l'exclusion des enseignes et des préenseignes, toute inscription, forme ou image, destinée à informer le public ou à attirer son attention, les dispositifs dont le principal objet est de recevoir lesdites inscriptions, formes ou images étant assimilées à des publicités (...) " qu'aux termes de l'article R. 581-34 du même code, issu de l'article 8 du décret du 30 janvier 2012 relatif à la publicité extérieure, aux enseignes et aux préenseignes : " La publicité lumineuse est la publicité à la réalisation de laquelle participe une source lumineuse spécialement prévue à cet effet. / La publicité lumineuse ne peut être autorisée à l'intérieur des agglomérations de moins de 10 000 habitants ne faisant pas partie d'une unité urbaine de plus de 100 000 habitants. / A l'intérieur des agglomérations de plus de 10 000 habitants et dans celles de moins de 10 000 habitants faisant partie d'une unité urbaine de plus de 100 000 habitants, ainsi qu'à l'intérieur de l'emprise des aéroports et des gares ferroviaires situés hors agglomération, la publicité lumineuse apposée sur un mur, scellée au sol ou installée directement sur le sol ne peut avoir une surface unitaire excédant 8 mètres carrés, ni s'élever à plus de 6 mètres au-dessus du niveau du sol (...) " ; qu'il résulte de ces dispositions législatives et réglementaires que, pour calculer la surface unitaire, il convient de prendre en compte, non pas la seule surface de la publicité lumineuse apposée sur le dispositif publicitaire mais le dispositif lui-même dont le  principal objet est de recevoir cette publicité, c'est-à-dire la surface du panneau litigieux tout entier ; <br/>
<br/>
              4.	Considérant qu'en ne prenant en compte que la seule surface de la publicité lumineuse pour juger que la surface unitaire n'excède pas la surface maximale autorisée de 8 mètres carrés, la cour administrative d'appel de Lyon a commis une erreur de droit ; que, par suite, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, la commune de Dijon est fondée à demander l'annulation de l'arrêt qu'elle attaque ;<br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              5.	Considérant qu'il n'y a pas lieu de mettre à la charge de la SARL Oxial une somme à verser à la commune de Dijon au titre de ces dispositions ; que celles-ci font obstacle à ce qu'une somme soit mise à la charge de la commune de Dijon qui n'est pas, dans la présente instance, la partie perdante ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'arrêt du 22 octobre 2015 de la cour administrative d'appel de Lyon est annulé.  <br/>
<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Lyon.<br/>
<br/>
Article 3 : Les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 5 : La présente décision sera notifiée à la commune de Dijon et à la SARL Oxial. Copie en sera adressée à la ministre de l'environnement, de l'énergie et de la mer, chargée des relations internationales sur le climat.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">02-01-04-02-05 AFFICHAGE ET PUBLICITÉ. AFFICHAGE. RÉGIME DE LA LOI DU 29 DÉCEMBRE 1979. DISPOSITIONS APPLICABLES À LA PUBLICITÉ. DISPOSITIONS PARTICULIÈRES APPLICABLES À CERTAINS MODES D'EXERCICE DE LA PUBLICITÉ. - LIMITATION DE LA SURFACE UNITAIRE DES PUBLICITÉS LUMINEUSES (ART. R. 581-34 DU C. ENVIR.) - NOTION DE SURFACE UNITAIRE - ENSEMBLE DU PANNEAU DONT L'OBJET EST DE RECEVOIR LA PUBLICITÉ LUMINEUSE [RJ1].
</SCT>
<ANA ID="9A"> 02-01-04-02-05 Limite de 8 m² fixée par l'article R. 581-34 du code de l'environnement pour la surface unitaire d'une publicité lumineuse. Pour calculer cette surface unitaire, il convient de prendre en compte, non pas la seule surface de la publicité lumineuse apposée sur le dispositif publicitaire mais le dispositif lui-même dont le principal objet est de recevoir cette publicité, c'est-à-dire la surface du panneau litigieux tout entier.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 6 octobre 1999, Société Sopremo, n° 169570, T. pp. 623-963.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
