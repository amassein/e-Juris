<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042575685</ID>
<ANCIEN_ID>JG_L_2020_11_000000430510</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/57/56/CETATEXT000042575685.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 27/11/2020, 430510</TITRE>
<DATE_DEC>2020-11-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>430510</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BAUER-VIOLAS, FESCHOTTE-DESBOIS, SEBAGH ; SCP MELKA - PRIGENT</AVOCATS>
<RAPPORTEUR>Mme Pauline Berne</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Gabrielle Merloz</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:430510.20201127</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Mme A... C... a demandé au tribunal administratif de Montreuil d'annuler le titre de recettes émis le 29 juin 2017 pour le compte de la commune de Bagnolet, l'avis de sommes à payer qui lui a été adressé le 9 août 2017 par la commune de Bagnolet pour assurer le recouvrement de la somme de 34 557, 44 euros, et de prononcer la décharge de cette créance. Par un jugement n ° 1708984 du 7 décembre 2018, le tribunal administratif de Montreuil a rejeté sa demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 6 mai et 22 juillet 2019 au secrétariat du contentieux du Conseil d'Etat, Mme C... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, d'annuler le titre de recettes émis le 29 juin 2017 par la commune de Bagnolet et l'avis de sommes à payer émis le 9 août 2017 pour assurer le recouvrement de la somme de 34 557, 44 euros ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Bagnolet la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code du travail ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Pauline Berne, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Marie-Gabrielle Merloz, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Bauer-Violas, Feschotte-Desbois, Sebagh, avocat de Mme B...-crèvecoeur et à la SCP Melka - Prigent, avocat de la commune de Bagnolet ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que Mme C... a été recrutée par la commune de Bagnolet en qualité d'agent non titulaire pour une durée d'un an à compter du 1er juillet 2013. Après que son contrat a pris fin à son terme, le 30 juin 2014, elle a été informée par décision du 23 juillet 2014 qu'elle pourrait bénéficier du versement de l'allocation d'aide au retour à l'emploi pour une période de 730 jours, soit jusqu'en juillet 2016. Mais par un courrier du 3 mars 2017, le maire de Bagnolet l'informait qu'elle avait perçu, à tort, ces allocations depuis le 11 juillet 2015 pour un montant de 34 557, 44 euros. Il émettait, le 29 juin 2017, un titre de recettes à hauteur de ce montant et lui adressait, le 9 août 2017, l'avis à payer correspondant à ce trop-perçu d'indemnités chômage. Mme C... se pourvoit en cassation contre le jugement du 7 décembre 2018 par lequel le tribunal administratif de Montreuil a rejeté sa demande tendant à l'annulation du titre de recettes et de l'avis de sommes à payer. <br/>
<br/>
              2. Aux termes de l'article R. 772-5 du code de justice administrative : " Sont présentées, instruites et jugées selon les dispositions du présent code, sous réserve des dispositions du présent chapitre, les requêtes relatives aux prestations, allocations ou droits attribués au titre de l'aide ou de l'action sociale, du logement ou en faveur des travailleurs privés d'emploi (...) ". Aux termes de l'article R. 772-9 du même code : " La procédure contradictoire peut être poursuivie à l'audience sur les éléments de fait qui conditionnent l'attribution de la prestation ou de l'allocation ou la reconnaissance du droit, objet de la requête. / L'instruction est close soit après que les parties ou leurs mandataires ont formulé leurs observations orales, soit, si ces parties sont absentes ou ne sont pas représentées, après appel de leur affaire à l'audience. Toutefois, afin de permettre aux parties de verser des pièces complémentaires, le juge peut décider de différer la clôture de l'instruction à une date postérieure dont il les avise par tous moyens. / L'instruction fait l'objet d'une réouverture en cas de renvoi à une autre audience ".<br/>
<br/>
              3. Il résulte de ces dispositions, qui dérogent aux règles de droit commun de la procédure administrative contentieuse, que la procédure contradictoire peut, eu égard aux spécificités de l'office du juge en matière de contentieux sociaux, être poursuivie au cours de l'audience sur les éléments de fait qui conditionnent l'attribution de la prestation ou de l'allocation, objet de la requête, et que le juge peut décider de différer la clôture à une date postérieure à l'audience pour permettre  aux parties de verser des pièces complémentaires.<br/>
<br/>
              4. Les visas du jugement indiquant que l'instruction avait été close après l'appel de l'affaire l'audience, alors que l'avocat de Mme C... était présent et a formulé des observations orales lors de l'examen de l'affaire, il s'ensuit que ce jugement attaqué a été pris en méconnaissance des dispositions du deuxième alinéa de l'article R. 772-9 précitées et est, ainsi, entaché d'irrégularité. Par suite, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, la requérante est fondée à demander l'annulation de ce jugement. <br/>
<br/>
              5. Il résulte de ce qui précède que le jugement du 7 décembre 2018 du tribunal administratif de Montreuil doit être annulé.<br/>
<br/>
              6. Mme C... a obtenu le bénéfice de l'aide juridictionnelle Par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. Il y a lieu, dans les circonstances de l'espèce, et sous réserve que la SCP Bauer-Violas, Feschotte-Desbois, Sebagh, avocat de Mme C..., renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de la commune de Bagnolet la somme de 2 000 euros à verser à la SCP Bauer-Violas, Feschotte-Desbois, Sebagh.  <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le jugement du 7 décembre 2018 du tribunal administratif de Montreuil est annulé. <br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Montreuil.<br/>
Article 3 : La commune de Bagnolet versera à la SCP Bauer-Violas, Feschotte-Desbois, Sebagh une somme de 2 000 euros en application de l'article L. 761-1 du code de justice administrative et du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991, sous réserve que cette SCP renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
Article 4 : La présente décision sera notifiée à Mme A... C... et à la commune de Bagnolet.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">04-04 AIDE SOCIALE. CONTENTIEUX DE L'AIDE SOCIALE ET DE LA TARIFICATION. - PROCÉDURE APPLICABLE AUX CONTENTIEUX SOCIAUX (ART. R. 772-5 ET S. DU CJA) - CLÔTURE DE L'INSTRUCTION CONTRADICTOIRE (ART. R. 772-9, 2E AL. DU CJA) - CAS OÙ LES PARTIES SONT PRÉSENTES OU REPRÉSENTÉES À L'AUDIENCE - CLÔTURE À L'ISSUE DES OBSERVATIONS ORALES [RJ1] - CONSÉQUENCE - CLÔTURE APRÈS L'APPEL DE L'AFFAIRE, ALORS QUE L'AVOCAT DE LA REQUÉRANTE A FORMULÉ DES OBSERVATIONS - IRRÉGULARITÉ.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-04-01-05 PROCÉDURE. INSTRUCTION. POUVOIRS GÉNÉRAUX D'INSTRUCTION DU JUGE. CLÔTURE DE L'INSTRUCTION. - PROCÉDURE APPLICABLE AUX CONTENTIEUX SOCIAUX (ART. R. 772-5 ET S. DU CJA) - CAS OÙ LES PARTIES SONT PRÉSENTES OU REPRÉSENTÉES À L'AUDIENCE - CLÔTURE À L'ISSUE DES OBSERVATIONS ORALES (ART. R. 772-9, 2E AL. DU CJA) [RJ1] - CONSÉQUENCE - CLÔTURE APRÈS L'APPEL DE L'AFFAIRE, ALORS QUE L'AVOCAT DE LA REQUÉRANTE A FORMULÉ DES OBSERVATIONS - IRRÉGULARITÉ.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-06-01 PROCÉDURE. JUGEMENTS. RÈGLES GÉNÉRALES DE PROCÉDURE. - PROCÉDURE APPLICABLE AUX CONTENTIEUX SOCIAUX (ART. R. 772-5 ET S. DU CJA) - CLÔTURE DE L'INSTRUCTION CONTRADICTOIRE (ART. R. 772-9, 2E AL. DU CJA) - CAS OÙ LES PARTIES SONT PRÉSENTES OU REPRÉSENTÉES À L'AUDIENCE - CLÔTURE À L'ISSUE DES OBSERVATIONS ORALES [RJ1] - CONSÉQUENCE - CLÔTURE APRÈS L'APPEL DE L'AFFAIRE À L'AUDIENCE, ALORS QUE L'AVOCAT DE LA REQUÉRANTE A FORMULÉ DES OBSERVATIONS [RJ1] - IRRÉGULARITÉ.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">54-06-04-01 PROCÉDURE. JUGEMENTS. RÉDACTION DES JUGEMENTS. VISAS. - PROCÉDURE APPLICABLE AUX CONTENTIEUX SOCIAUX (ART. R. 772-5 ET S. DU CJA) - CLÔTURE DE L'INSTRUCTION CONTRADICTOIRE (ART. R. 772-9, 2E AL. DU CJA) - CAS OÙ LES PARTIES SONT PRÉSENTES OU REPRÉSENTÉES À L'AUDIENCE - CLÔTURE À L'ISSUE DES OBSERVATIONS ORALES [RJ1] - VISAS INDIQUANT UNE CLÔTURE APRÈS L'APPEL DE L'AFFAIRE À L'AUDIENCE, ALORS QUE L'AVOCAT DE LA REQUÉRANTE A FORMULÉ DES OBSERVATIONS  - IRRÉGULARITÉ.
</SCT>
<ANA ID="9A"> 04-04 Il résulte des articles R. 772-5 et R. 772-9 du code de justice administrative (CJA), qui dérogent aux règles de droit commun de la procédure administrative contentieuse, que la procédure contradictoire peut, eu égard aux spécificités de l'office du juge en matière de contentieux sociaux, être poursuivie au cours de l'audience sur les éléments de fait qui conditionnent l'attribution de la prestation ou de l'allocation, objet de la requête, et que le juge peut décider de différer la clôture à une date postérieure à l'audience pour permettre aux parties de verser des pièces complémentaires.,,,Les visas du jugement indiquant que l'instruction avait été close après l'appel de l'affaire l'audience, alors que l'avocat de la requérante était présent et a formulé des observations orales lors de l'examen de l'affaire, il s'ensuit que ce jugement a été pris en méconnaissance du deuxième alinéa de l'article R. 772-9 du CJA et est, ainsi, entaché d'irrégularité.</ANA>
<ANA ID="9B"> 54-04-01-05 Il résulte des articles R. 772-5 et R. 772-9 du code de justice administrative (CJA), qui dérogent aux règles de droit commun de la procédure administrative contentieuse, que la procédure contradictoire peut, eu égard aux spécificités de l'office du juge en matière de contentieux sociaux, être poursuivie au cours de l'audience sur les éléments de fait qui conditionnent l'attribution de la prestation ou de l'allocation, objet de la requête, et que le juge peut décider de différer la clôture à une date postérieure à l'audience pour permettre aux parties de verser des pièces complémentaires.,,,Les visas du jugement indiquant que l'instruction avait été close après l'appel de l'affaire l'audience, alors que l'avocat de la requérante était présent et a formulé des observations orales lors de l'examen de l'affaire, il s'ensuit que ce jugement a été pris en méconnaissance du deuxième alinéa de l'article R. 772-9 du CJA et est, ainsi, entaché d'irrégularité.</ANA>
<ANA ID="9C"> 54-06-01 Il résulte des articles R. 772-5 et R. 772-9 du code de justice administrative (CJA), qui dérogent aux règles de droit commun de la procédure administrative contentieuse, que la procédure contradictoire peut, eu égard aux spécificités de l'office du juge en matière de contentieux sociaux, être poursuivie au cours de l'audience sur les éléments de fait qui conditionnent l'attribution de la prestation ou de l'allocation, objet de la requête, et que le juge peut décider de différer la clôture à une date postérieure à l'audience pour permettre aux parties de verser des pièces complémentaires.,,,Les visas du jugement indiquant que l'instruction avait été close après l'appel de l'affaire l'audience, alors que l'avocat de la requérante était présent et a formulé des observations orales lors de l'examen de l'affaire, il s'ensuit que ce jugement a été pris en méconnaissance du deuxième alinéa de l'article R. 772-9 du CJA et est, ainsi, entaché d'irrégularité.</ANA>
<ANA ID="9D"> 54-06-04-01 Il résulte des articles R. 772-5 et R. 772-9 du code de justice administrative (CJA), qui dérogent aux règles de droit commun de la procédure administrative contentieuse, que la procédure contradictoire peut, eu égard aux spécificités de l'office du juge en matière de contentieux sociaux, être poursuivie au cours de l'audience sur les éléments de fait qui conditionnent l'attribution de la prestation ou de l'allocation, objet de la requête, et que le juge peut décider de différer la clôture à une date postérieure à l'audience pour permettre aux parties de verser des pièces complémentaires.,,,Les visas du jugement indiquant que l'instruction avait été close après l'appel de l'affaire l'audience, alors que l'avocat de la requérante était présent et a formulé des observations orales lors de l'examen de l'affaire, il s'ensuit que ce jugement a été pris en méconnaissance du deuxième alinéa de l'article R. 772-9 du CJA et est, ainsi, entaché d'irrégularité.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 2 octobre 2017, Mme,, n° 399578, p. 308.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
