<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036560965</ID>
<ANCIEN_ID>JG_L_2018_01_000000397611</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/56/09/CETATEXT000036560965.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 26/01/2018, 397611, Publié au recueil Lebon</TITRE>
<DATE_DEC>2018-01-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>397611</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO ; SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>M. Jacques Reiller</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:397611.20180126</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...et MmeC..., épouse B...ont demandé à la Cour nationale du droit d'asile d'annuler les décisions du 28 novembre 2014 par lesquelles le directeur général de l'Office français de protection des réfugiés et apatrides (OFPRA) a rejeté leurs demandes d'admission au bénéfice de l'asile ou, à défaut, de la protection subsidiaire. Par la décision jointe n° 15000468, 15000469 du 25 juin 2015, la Cour nationale du droit d'asile a rejeté leurs recours.<br/>
<br/>
              M. et Mme B...ont demandé à la Cour nationale du droit d'asile d'annuler les décisions du 10 août 2015 par lesquelles le directeur général de l'Office français de protection des réfugiés et apatrides a rejeté comme irrecevables leurs nouvelles demandes d'admission au bénéfice de l'asile ou, à défaut, de la protection subsidiaire. <br/>
<br/>
              Par deux mémoires distincts, ils ont aussi demandé à la Cour, sur le fondement de l'article 61-1 de la Constitution et des articles 23-1 et 23-2 de l'ordonnance n° 58-1067 du 7 novembre 1958, de transmettre au Conseil d'Etat la question prioritaire de constitutionnalité relative à la conformité aux droits et libertés garantis par la Constitution, de l'article L. 723-16 du code de l'entrée et du séjour des étrangers et du droit d'asile.<br/>
<br/>
              Par une décision n° 15025487, 15025488 du 7 janvier 2016, la Cour nationale du droit d'asile a rejeté leurs recours et refusé de transmettre au Conseil d'Etat la question prioritaire de constitutionnalité soulevée par eux.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 3 mars et 1er juin 2016 au secrétariat du contentieux du Conseil d'Etat, M et Mme B...demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette décision du 7 janvier 2016 ;<br/>
<br/>
              2°) de saisir la Cour de justice de l'Union européenne (CJUE) d'une question préjudicielle en appréciation de validité sur le point de savoir si l'article 40 paragraphe 3 de la directive 2013/32/UE du Parlement européen et du Conseil relative à des procédures communes pour l'octroi et le retrait de la protection internationale est conforme au droit de l'Union et de surseoir à statuer jusqu'à l'intervention de la décision de la CJUE, ou, à titre subsidiaire, de saisir la CJUE d'une question préjudicielle en interprétation de ses dispositions ;<br/>
<br/>
              3°) de mettre à la charge de l'OFPRA la somme de 3 500 euros au titre des articles L. 761- 1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule et son article 61-1 ;<br/>
              - le traité sur le fonctionnement de l'Union européenne ;<br/>
              - la Charte des droits fondamentaux de l'Union européenne ;<br/>
              - la convention de Genève du 28 juillet 1951 relative aux réfugiés ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - la directive n° 2013/32/UE du Parlement européen et du Conseil ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;  <br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jacques Reiller, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de M. et Mme B...et à la SCP Foussard, Froger, avocat de l'Office français de protection des refugies et apatrides ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Par la décision contestée du 7 janvier 2016, la Cour nationale du droit d'asile a jugé qu'il n'y avait pas lieu de transmettre au Conseil d'Etat la question prioritaire de constitutionnalité relative à la conformité aux droits et libertés garantis par la Constitution, de l'article L. 723-16 du code de l'entrée et du séjour des étrangers et du droit d'asile et notamment de son dernier alinéa, soulevée par M. et Mme B... à l'appui de leurs recours dirigés contre les décisions du 10 août 2015 par lesquelles le directeur général de l'Office français de protection des réfugiés et apatrides a rejeté comme irrecevables leurs nouvelles demandes d'admission au bénéfice de l'asile ou, à défaut, de la protection subsidiaire. Par la même décision, la Cour a rejeté ces recours, après avoir estimé qu'il n'y avait pas lieu de saisir la Cour de justice de l'Union européenne d'une question préjudicielle relative à l'article 40 paragraphe 3 de la directive 2013/32/UE du Parlement européen et du Conseil relative à des procédures communes pour l'octroi et le retrait de la protection internationale. M. et Mme B...se pourvoient en cassation contre cette décision du 7 janvier 2016 en présentant, par un mémoire distinct et motivé, une contestation du refus de transmission par la Cour de la question prioritaire de constitutionnalité qu'ils avaient soulevée.<br/>
<br/>
              2. Aux termes de l'article 33 de la directive 2013/32/UE du 26 juin 2013 relative à des procédures communes pour l'octroi et le retrait de la protection internationale : " (...) 2. Les États membres peuvent considérer une demande de  protection internationale comme irrecevable uniquement lorsque (...) d) la demande concernée est une demande ultérieure, dans laquelle n'apparaissent ou ne sont présentés par le demandeur aucun élément ou fait nouveau relatifs à l'examen visant à déterminer si le demandeur remplit les conditions requises  pour prétendre au statut de bénéficiaire d'une protection  internationale en vertu de la directive 2011/95/UE ". Aux termes de l'article 40 de cette directive : " (...) 2. Afin de prendre une décision sur la recevabilité d'une demande de protection internationale en vertu de l'article 33,  paragraphe 2, point d), une demande de protection internationale ultérieure est tout d'abord soumise à un examen préliminaire visant à déterminer si des éléments ou des faits nouveaux sont apparus ou ont été présentés par le demandeur, qui se rapportent à l'examen visant à déterminer si le demandeur  remplit les conditions requises pour prétendre au statut de  bénéficiaire d'une protection internationale en vertu de la directive 2011/95/UE. / 3. Si l'examen préliminaire visé au paragraphe 2 aboutit à la conclusion que des éléments ou des faits nouveaux sont apparus ou ont été présentés par le demandeur et qu'ils augmentent de manière significative la probabilité que le demandeur remplisse les conditions requises pour prétendre au statut de bénéficiaire d'une protection internationale en vertu de la directive 2011/95/UE, l'examen de la demande est poursuivi conformément au chapitre II. Les États membres peuvent également prévoir d'autres raisons de poursuivre l'examen d'une demande ultérieure. / 4. Les États membres peuvent prévoir de ne poursuivre l'examen de la demande que si le demandeur concerné a été,  sans faute de sa part, dans l'incapacité de faire valoir, au cours de la précédente procédure, les situations exposées aux paragraphes 2 et 3 du présent article, en particulier en exerçant son droit à un recours effectif en vertu de l'article 46. / 5. Lorsque l'examen d'une demande ultérieure n'est pas poursuivi en vertu du présent article, ladite demande est considérée comme irrecevable conformément à l'article 33, paragraphe 2, point d) ".<br/>
<br/>
              3. Aux termes de l'article L. 723-11 du code de l'entrée et du séjour des étrangers et du droit d'asile introduit par la loi du 29 juillet 2015 : " L'office peut prendre une décision d'irrecevabilité écrite et motivée, sans vérifier si les conditions d'octroi de l'asile sont réunies, dans les cas suivants : (...) 3° En cas de demande de réexamen lorsque, à l'issue d'un examen préliminaire effectué selon la procédure définie à l'article L. 723-16, il apparaît que cette demande ne répond pas aux conditions prévues au même article  ; (...) L'office conserve la faculté d'examiner la demande présentée par un étranger persécuté en raison de son action en faveur de la liberté ou qui sollicite la protection pour un autre motif ". Aux termes de l'article L. 723-15 du même code : " Constitue une demande de réexamen une demande d'asile présentée après qu'une décision définitive a été prise sur une demande antérieure, y compris lorsque le demandeur avait explicitement retiré sa demande antérieure, lorsque l'office a pris une décision définitive de clôture en application de l'article L. 723-13 ou lorsque le demandeur a quitté le territoire, même pour rejoindre son pays d'origine. (...) ". Aux termes de l'article L. 723-16 de ce code : " A l'appui de sa demande de réexamen, le demandeur indique par écrit les faits et produit tout élément susceptible de justifier un nouvel examen de sa demande d'asile. / L'office procède à un examen préliminaire des faits ou des éléments nouveaux présentés par le demandeur intervenus après la décision définitive prise sur une demande antérieure ou dont il est avéré qu'il n'a pu en avoir connaissance qu'après cette décision. / Lors de l'examen préliminaire, l'office peut ne pas procéder à un entretien. /  Lorsque, à la suite de cet examen préliminaire, l'office conclut que ces faits ou éléments nouveaux n'augmentent pas de manière significative la probabilité que le demandeur justifie des conditions requises pour prétendre à une protection, il peut prendre une décision d'irrecevabilité ". Enfin, aux termes de l'article L. 731-2 de ce code : " La Cour nationale du droit d'asile statue en formation collégiale, dans un délai de cinq mois à compter de sa saisine. Toutefois, sans préjudice de l'application de l'article L. 733-2, lorsque la décision de l'office a été prise en application des articles L. 723-2 ou L. 723-11, le président de la Cour nationale du droit d'asile ou le président de formation de jugement qu'il désigne à cette fin statue dans un délai de cinq semaines à compter de sa saisine. De sa propre initiative ou à la demande du requérant, le président de la cour ou le président de formation de jugement désigné à cette fin peut, à tout moment de la procédure, renvoyer à la formation collégiale la demande s'il estime que celle-ci ne relève pas de l'un des cas prévus aux mêmes articles L. 723-2 et L. 723-11 ou qu'elle soulève une difficulté sérieuse. La cour statue alors dans les conditions prévues à la première phrase du présent alinéa ".<br/>
<br/>
              Sur le refus de transmission de  la question prioritaire de constitutionnalité soulevée devant la Cour nationale du droit d'asile :<br/>
<br/>
              4. Les dispositions de l'article 23-2 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel prévoient que lorsqu'une juridiction relevant du Conseil d'Etat est saisie de moyens contestant la conformité d'une disposition législative aux droits et libertés garantis par la Constitution, elle transmet au Conseil d'Etat la question de constitutionnalité ainsi posée à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question ne soit pas dépourvue de caractère sérieux.  <br/>
<br/>
              5. Aux termes de l'article 88-1 de la Constitution : " La République participe à l'Union européenne constituée d'États qui ont choisi librement d'exercer en commun certaines de leurs compétences en vertu du traité sur l'Union européenne et du traité sur le fonctionnement de l'Union européenne, tels qu'ils résultent du traité signé à Lisbonne le 13 décembre 2007 ". En l'absence de mise en cause d'une règle ou d'un principe inhérent à l'identité constitutionnelle de la France, le Conseil constitutionnel juge qu'il n'est pas compétent pour contrôler la conformité aux droits et libertés que la Constitution garantit de dispositions législatives qui se bornent à tirer les conséquences nécessaires de dispositions inconditionnelles et précises d'une directive de l'Union européenne et qu'en ce cas, il n'appartient qu'au juge de l'Union européenne, saisi le cas échéant à titre préjudiciel, de contrôler le respect par cette directive des droits fondamentaux garantis par l'article 6 du Traité sur l'Union européenne. En l'absence de mise en cause, à l'occasion d'une question prioritaire de constitutionnalité soulevée sur des dispositions législatives se bornant à tirer les conséquences nécessaires de dispositions précises et inconditionnelles d'une directive de l'Union européenne, d'une règle ou d'un principe inhérent à l'identité constitutionnelle de la France, une telle question n'est pas au nombre de celles qu'il appartient au Conseil d'Etat de transmettre au Conseil constitutionnel sur le fondement de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel.<br/>
<br/>
              6. Les dispositions contestées par les requérants de l'article L. 723-16 du code de l'entrée et du séjour des étrangers et du droit d'asile se bornent à assurer la transposition en droit interne des dispositions précises et inconditionnelles des paragraphes 2 et 3 de l'article 40 de la directive n° 2013/32/UE du Parlement européen et du Conseil du 26 juin 2013. <br/>
<br/>
              7. D'une part, en invoquant l'objectif d'intelligibilité et d'accessibilité de la loi, la question prioritaire de constitutionnalité soulevée ne met en cause aucune règle ni aucun principe inhérent à l'identité constitutionnelle de la France. D'autre part, si les requérants invoquent également l'alinéa 4 du Préambule de la Constitution de 1946, aux termes duquel " tout homme persécuté en raison de son action en faveur de la liberté a droit d'asile sur les territoires de la République ", ils ne sont en tout état de cause pas fondés à soutenir que les dispositions contestées le mettent en cause dès lors que le dernier alinéa de l'article L. 723-11 du code de l'entrée et du séjour des étrangers et du droit d'asile, cité au point 3, prévoit que lorsque l'Office se prononce sur une demande de réexamen selon la procédure définie à l'article L. 723-16, celui-ci " conserve la faculté d'examiner la demande présentée par un étranger persécuté en raison de son action en faveur de la liberté (...) ".<br/>
<br/>
              8. Il s'ensuit qu'en regardant la question dont elle était saisie comme dépourvue de caractère sérieux, la Cour nationale du droit d'asile n'a pas inexactement qualifié la question de la conformité à la Constitution des dispositions de l'article L. 723-16 du code de l'entrée et du séjour des étrangers et du droit d'asile.<br/>
<br/>
              Sur les moyens mettant en cause la validité de la directive du 26 juin 2013 :<br/>
<br/>
              9. Aux termes du point 36 de la directive 2013/32/UE susvisée : " Lorsqu'un demandeur présente une demande ultérieure sans apporter de nouvelles preuves ou de nouveaux arguments, il serait disproportionné d'obliger les États-membres à entreprendre une nouvelle procédure d'examen complet. Les États membres devraient, en l'espèce, pouvoir rejeter une demande comme étant irrecevable conformément au principe de l'autorité de la chose jugée ".<br/>
<br/>
              10. Il résulte clairement des dispositions des articles 33 et 40 de la directive du 26 juin 2013 citées au point 2, dont l'interprétation est confortée par le point 36 cité ci-dessus, que l'examen préliminaire de recevabilité d'une demande de réexamen doit porter successivement sur le caractère nouveau des faits et des éléments de preuve présentés à son appui ainsi que sur leur valeur probante afin de déterminer s'ils sont ou non de nature à modifier l'appréciation du bien-fondé de la demande de protection.<br/>
<br/>
              11. En premier lieu, ces dispositions prévoient ainsi qu'une demande de réexamen est soumise à un examen préliminaire permettant d'en apprécier la recevabilité. Contrairement à ce qui est soutenu, l'article 40 de cette directive a pu, sans être entaché ni de contradiction ni d'ambiguïté, successivement prévoir, à son paragraphe 2, que cet examen visait à déterminer si des éléments de preuve ou des faits nouveaux étaient apparus ou avaient été présentés depuis le rejet de la demande antérieure puis, à son paragraphe 3, que la demande ultérieure n'était recevable, exigeant dès lors un examen au fond, que dans l'hypothèse où il apparaissait, au terme de l'examen préliminaire, que les éléments de preuve ou faits nouveaux augmentaient de manière significative la probabilité que le demandeur remplisse les conditions requises pour prétendre à une protection. En subordonnant ainsi la recevabilité d'une demande de réexamen à deux conditions cumulatives, la première à l'existence d'éléments de preuve ou des faits nouveaux et la seconde à leur valeur probante, les dispositions contestées ne méconnaissent ni le principe de sécurité juridique ni, par voie de conséquence, le droit d'asile garanti par l'article 18 de la Charte des droits fondamentaux de l'Union européenne et les articles 67 et 78 du traité sur le fonctionnement de l'Union européenne.<br/>
<br/>
              12. Il s'ensuit que la Cour nationale du droit d'asile n'a pas entaché sa décision d'erreur de droit en écartant les moyens mettant en cause la validité de la directive du 26 juin 2013 sans saisir la Cour de justice de l'Union européenne d'une question préjudicielle.<br/>
<br/>
              13. En second lieu, les requérants ne peuvent utilement soulever, pour la première fois en cassation, le moyen tiré de l'invalidité des dispositions litigieuses de la directive du 26 juin 2013 au regard des articles 41, 47 et 48 de la Charte des droits fondamentaux de l'Union européenne pour soutenir que la Cour nationale du droit d'asile aurait entaché sa décision d'erreur de droit en ne renvoyant pas à la Cour de justice de l'Union européenne une question en appréciation de validité sur ce point.<br/>
<br/>
              Sur le moyen mettant en cause l'interprétation de la directive du 26 juin 2013 retenue par la Cour nationale du droit d'asile :<br/>
<br/>
              14. Ainsi qu'il a été dit au point 10, il résulte clairement des dispositions des articles 33 et 40 de la directive du 26 juin 2013 précitées que l'examen préliminaire de recevabilité d'une demande de réexamen doit porter successivement sur le caractère nouveau des faits et des éléments de preuve présentés à son appui et sur leur valeur probante afin de déterminer s'ils sont ou non de nature à modifier l'appréciation du bien-fondé de la demande de protection.<br/>
<br/>
              15. Il s'ensuit que, contrairement à ce qui est soutenu, la Cour nationale du droit d'asile, en donnant cette interprétation aux dispositions contestées des articles 33 et 40 de la directive sans saisir, ainsi qu'il a été dit, la Cour de justice de l'Union européenne d'une question sur ce point, n'a pas entaché sa décision d'erreur de droit. <br/>
<br/>
              Sur les moyens articulés à l'encontre de l'article L. 723-16 du code de l'entrée et du séjour des étrangers et du droit d'asile :<br/>
<br/>
              16. En premier lieu, il ressort des termes de la décision attaquée que la Cour nationale du droit d'asile a procédé, à la lumière des exigences découlant de la directive du 26 juin 2013 qu'elles ont pour objet de transposer, à une interprétation des dispositions de l'article L. 723-16 du code de l'entrée et du séjour des étrangers et du droit d'asile exempte de toute erreur de droit en subordonnant la recevabilité d'une demande de réexamen, d'une part, à la présentation de faits nouveaux intervenus ou révélés postérieurement au rejet de la demande antérieure ou d'éléments de preuve nouveaux et, d'autre part, au constat que leur valeur probante est de nature à modifier l'appréciation du bien fondé de la demande de protection au regard de la situation personnelle du demandeur et de la situation dans son pays d'origine.<br/>
<br/>
              17. En deuxième lieu, la Cour nationale du droit d'asile n'a pas entaché sa décision d'erreur de droit en écartant le moyen tiré de ce que les dispositions contestées de cet article L. 723-16 auraient pour effet de limiter le contrôle juridictionnel exercé par le juge de l'asile sur les décisions d'irrecevabilité après avoir relevé que lorsque celui-ci annule une décision d'irrecevabilité au motif qu'elle a été prise en dehors des cas prévus par la loi, soit il accorde la protection, soit, il renvoie l'examen de la demande à l'Office de protection des réfugiés et apatrides auquel il incombe, le cas échéant, d'organiser un entretien personnel avec le demandeur.<br/>
<br/>
              18. En troisième lieu, contrairement à ce qui est soutenu, la décision attaquée n'est pas entachée d'insuffisance de motivation faute d'avoir répondu aux moyens tirés de la méconnaissance, par l'article L. 723-16 du code de l'entrée et du séjour des étrangers et du droit d'asile des articles 2, 3, 8 et 18 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales qui n'étaient pas soulevés devant la Cour. S'agissant du moyen tiré de la méconnaissance du principe de sécurité juridique tel qu'interprété par la jurisprudence de la Cour européenne des droits de l'homme, la décision attaquée doit être regardée comme y ayant suffisamment répondu en écartant le moyen tiré de la violation du principe général de sécurité juridique. <br/>
<br/>
              19. En quatrième lieu, la Cour nationale du droit d'asile n'a pas entaché sa décision d'erreur de droit en jugeant que les requérants ne pouvaient utilement invoquer le moyen tiré de la violation d'un " principe du bénéfice du doute ", qu'ils estimaient être le corollaire du principe de non-refoulement garanti par l'article 33 de la convention de Genève, après avoir relevé que l'examen des demandes de réexamen prévu à l'article L. 723-16, ne porte pas sur l'éventuel refoulement du demandeur d'asile.<br/>
<br/>
              Sur les autres moyens :<br/>
<br/>
              20. En premier lieu, dès lors qu'elle s'était fondée pour rejeter la première demande des requérants, sur le manque de crédibilité des persécutions alléguées et non sur l'appartenance de Mme B... à la minorité ashkalie, la Cour nationale du droit d'asile n'a pas entaché sa décision d'erreur de droit en jugeant, par une appréciation souveraine exempte de dénaturation, que les éléments nouveaux apportés sur ce point à l'appui de leur demande de réexamen n'étaient pas de nature à la rendre recevable. <br/>
<br/>
              21. En second lieu, en considérant, au terme d'une appréciation souveraine, que les témoignages et éléments nouveaux produits par les requérants, étaient insuffisamment précis et probants pour augmenter de manière significative la probabilité  qu'ils  justifient des conditions requises pour prétendre à une protection, la Cour n'a pas dénaturé les faits et pièces du dossier. Il s'ensuit qu'elle n'a pas davantage entaché sa décision d'erreur de droit en en déduisant que l'Office avait pu légalement, en application du troisième alinéa de l'article L. 723-16 du code de l'entrée et du séjour des étrangers et du droit d'asile, rejeter la demande de réexamen des demandeurs pour irrecevabilité sans les convoquer à un entretien préalable.<br/>
<br/>
              22. Il résulte de tout ce qui précède que M. et Mme B...ne sont pas fondés à demander l'annulation de la décision du 7 janvier 2016 de la Cour nationale du droit d'asile. Leur pourvoi doit donc être rejeté, y compris les conclusions présentées au titre des dispositions des articles L. 761- 1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le pourvoi de M. et Mme B...est rejeté.<br/>
Article 2 : La présente décision sera notifiée à M. A...B...et MmeC..., épouseB..., et à l'Office français de protection des réfugiés et apatrides.<br/>
Copie en sera adressée au ministre d'Etat, ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">095-02-08 - NOUVELLE DEMANDE D'ADMISSION AU BÉNÉFICE DE L'ASILE - EXAMEN PRÉLIMINAIRE DE RECEVABILITÉ (ART. 33 ET 40 DE LA DIRECTIVE 2013/32/UE ET L. 723-16 DU CESEDA - CONDITIONS - FAITS NOUVEAUX INTERVENUS OU RÉVÉLÉS POSTÉRIEUREMENT AU REJET DE LA DEMANDE ANTÉRIEURE OU ÉLÉMENTS DE PREUVE NOUVEAUX [RJ1] - VALEUR PROBANTE DE NATURE À MODIFIER L'APPRÉCIATION DU BIEN FONDÉ DE LA DEMANDE DE PROTECTION AU REGARD DE LA SITUATION PERSONNELLE DU DEMANDEUR ET DE LA SITUATION DANS SON PAYS D'ORIGINE.
</SCT>
<ANA ID="9A"> 095-02-08 L'article L. 723-16 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA), interprété à la lumière des articles 33 et 40 de la directive 2013/32/UE du 26 juin 2013 relative à des procédures communes pour l'octroi et le retrait de la protection internationale, qu'il a pour objet de transposer, subordonne la recevabilité d'une demande de réexamen, d'une part, à la présentation de faits nouveaux intervenus ou révélés postérieurement au rejet de la demande antérieure ou d'éléments de preuve nouveaux et, d'autre part, au constat que leur valeur probante est de nature à modifier l'appréciation du bien-fondé de la demande de protection au regard de la situation personnelle du demandeur et de la situation de son pays d'origine.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Comp. CE, Section, 27 janvier 1995, Mme,, n° 129428, p.51 ; CE, 2 décembre 1998,,, n° 178752, T. pp. 962-1138.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
