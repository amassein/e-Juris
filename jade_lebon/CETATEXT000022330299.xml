<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000022330299</ID>
<ANCIEN_ID>JG_L_2010_05_000000306643</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/22/33/02/CETATEXT000022330299.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème et 8ème sous-sections réunies, 18/05/2010, 306643, Publié au recueil Lebon</TITRE>
<DATE_DEC>2010-05-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>306643</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème et 8ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Vigouroux</PRESIDENT>
<AVOCATS>SCP LYON-CAEN, FABIANI, THIRIEZ</AVOCATS>
<RAPPORTEUR>Mme Anne  Egerszegi</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Geffray Edouard</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2010:306643.20100518</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le mémoire, enregistré le 1er mars 2010 au secrétariat du contentieux du Conseil d'Etat, présenté pour la COMMUNE DE DUNKERQUE, représentée par son maire, en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958 ; la COMMUNE DE DUNKERQUE demande au Conseil d'Etat, à l'appui de son pourvoi tendant à l'annulation de l'arrêt du 12 avril 2007 de la cour administrative d'appel de Douai, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution, de l'article L. 2113-3 du code général des collectivités territoriales et de l'article 123 de la loi n° 2004-809 du 13 août 2004 modifiant l'article L. 2113-2 du même code ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution, notamment ses articles 61-1 et 72-1 ;<br/>
<br/>
              Vu l'ordonnance n° 58-1067 du 7 novembre 1958 ; <br/>
<br/>
              Vu les articles L. 2113-2 et L. 2113-3 du code général des collectivités territoriales ; <br/>
<br/>
              Vu l'article 123 de la loi n°2004-809 du 13 août 2004 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Anne Egerszegi, Maître des Requêtes,<br/>
<br/>
              - les observations de la SCP Lyon-Caen, Fabiani, Thiriez, avocat de la COMMUNE DE DUNKERQUE,<br/>
<br/>
              - les conclusions de M. Edouard Geffray, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Lyon-Caen, Fabiani, Thiriez, avocat de la COMMUNE DE DUNKERQUE ;<br/>
<br/>
<br/>
<br/>Considérant qu'aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé, y compris pour la première fois en cassation, à l'occasion d'une instance devant le Conseil d'Etat (...) " ; qu'il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et qu'elle soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              Considérant que les articles L. 2113-2 et L. 2113-3 du code général des collectivités territoriales organisent une procédure de consultation des électeurs sur les projets de fusion de communes avant décision du préfet ; que le litige soulevé par le pourvoi de la COMMUNE DE DUNKERQUE porte sur les décisions des 17 et 28 décembre 2004 par lesquelles le préfet du Nord a refusé de prononcer sa fusion avec deux autres communes en se fondant, pour la première des deux décisions contestées, sur ce que la consultation des personnes inscrites sur les listes électorales municipales, organisée le 5 décembre 2004, n'avait pas dégagé une majorité de suffrages favorables répondant aux exigences de l'article L. 2113-3 du code et, pour la seconde, sur l'entrée en vigueur, le 1er janvier 2005, des nouvelles dispositions de l'article L. 2113-2 du même code, issues de l'article 123 de la loi n° 2004-809 du 13 août 2004 relative aux libertés et responsabilités locales, rendant obligatoire la consultation des électeurs sur les projets de fusion ; que, dans ces conditions, l'article L. 2113-2 du code général des collectivités territoriales, dans sa rédaction issue de l'article 123 de la loi du 13 août 2004, et l'article L. 2113-3 du même code sont applicables au litige au sens et pour l'application de l'article 23-5 de l'ordonnance du 7 novembre 1958 ; que ces dispositions n'ont pas déjà été déclarées conformes à la Constitution par le Conseil constitutionnel ; que le moyen tiré de ce qu'elles portent atteinte aux droits et libertés garantis par la Constitution, et notamment aux dispositions du dernier alinéa de l'article 72-1 de la Constitution dans sa rédaction issue de la loi constitutionnelle n° 2003-276 du 28 mars 2003 ainsi qu'au principe de libre administration des collectivités territoriales, soulève une question présentant un caractère sérieux ; qu'ainsi, il y a lieu de renvoyer au Conseil constitutionnel la question de la conformité à la Constitution de l'article L. 2113-3 du code général des collectivités territoriales et de l'article L. 2113-2 du même code dans sa rédaction issue de l'article 123 de la loi n° 2004-809 du 13 août 2004 ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La question de la conformité à la Constitution de l'article L. 2113-3 du code général des collectivités territoriales et de l'article L. 2113-2 du même code dans sa rédaction issue de l'article 123 de la loi n° 2004-809 du 13 août 2004 est renvoyée au Conseil constitutionnel.<br/>
Article 2 : Il est sursis à statuer sur le pourvoi de la COMMUNE DE DUNKERQUE jusqu'à ce que le Conseil constitutionnel ait tranché la question de constitutionnalité ainsi soulevée. <br/>
Article 3 : La présente décision sera notifiée à la COMMUNE DE DUNKERQUE, au ministre de l'intérieur, de l'outre-mer et des collectivités territoriales et au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-10-05-01-02 PROCÉDURE. - ARTICLES LÉGISLATIFS LIÉS, SUR LESQUELS SE FONDENT LES DÉCISIONS CONTESTÉES, ALORS QUE LES DISPOSITIONS DE L'UN DE CES DEUX ARTICLES N'ENTRAIENT EN VIGUEUR QUE QUELQUES JOURS PLUS TARD.
</SCT>
<ANA ID="9A"> 54-10-05-01-02 Litige relatif à deux décisions d'un préfet refusant de prononcer la fusion d'une commune avec deux autres communes. Les articles L. 2113-2 et L. 2113-3 du code général des collectivités territoriales (CGCT) organisent une procédure de consultation des électeurs sur les projets de fusion de communes avant décision du préfet. Question prioritaire de constitutionnalité portant sur ces deux articles. Ces deux textes sont applicables au litige au sens et pour l'application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958, dès lors que le préfet s'est fondé sur ces deux articles pour refuser de prononcer la fusion des communes demanderesses, alors même que les dispositions de l'article L. 2113-2 du CGCT, dans leur version issue de l'article 123 de la loi n° 2004-809 du 13 août 2004 dont le préfet a fait application, n'entraient en vigueur que quelques jours jours plus tard.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
