<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025449353</ID>
<ANCIEN_ID>JG_L_2012_03_000000354159</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/44/93/CETATEXT000025449353.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème et 2ème sous-sections réunies, 01/03/2012, 354159, Publié au recueil Lebon</TITRE>
<DATE_DEC>2012-03-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>354159</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème et 2ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Jacques Arrighi de Casanova</PRESIDENT>
<AVOCATS>SPINOSI ; SCP DELAPORTE, BRIARD, TRICHET</AVOCATS>
<RAPPORTEUR>M. Nicolas Polge</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Bertrand Dacosta</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:354159.20120301</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 18 novembre et 5 décembre 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour le DEPARTEMENT DE LA CORSE DU SUD, représenté par le président du conseil général ; le département demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 1100957 du 3 novembre 2011 du juge des référés du tribunal administratif de Bastia statuant, en application de l'article L. 551-1 du code de justice administrative, sur la demande de la société des Autocars Roger Ceccaldi, en ce qu'elle a annulé la procédure de passation d'un marché de services de transport scolaire entre Vico et Ajaccio, ayant donné lieu à la décision de la commission d'appel d'offres du 13 octobre 2011, et a mis à la charge du département la contribution pour l'aide juridique ainsi qu'une somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
              2°) statuant en référé, de rejeter la demande de la société des Autocars Roger Ceccaldi ;<br/>
<br/>
              3°) de mettre à la charge de la société des Autocars Roger Ceccaldi le versement de la somme acquittée par le département au titre de la contribution pour l'aide juridique ainsi que celui d'une somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 15 février 2012, présentée pour la société des Autocars Roger Ceccaldi ;<br/>
<br/>
              Vu le code de l'éducation ;<br/>
<br/>
              Vu le code des marchés publics ;<br/>
<br/>
              Vu le code du travail ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Nicolas Polge, Maître des Requêtes,  <br/>
<br/>
              - les observations de Me Spinosi, avocat du DEPARTEMENT DE LA CORSE DU SUD et de la SCP Delaporte, Briard, Trichet, avocat de la société des Autocars Roger Ceccaldi, <br/>
<br/>
              - les conclusions de M. Bertrand Dacosta, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à Me Spinosi, avocat du DEPARTEMENT DE LA CORSE DU SUD et à la SCP Delaporte, Briard, Trichet, avocat de la société des Autocars Roger Ceccaldi ;<br/>
<br/>
<br/>
<br/>Considérant qu'aux termes de l'article 53 du code des marchés publics : <br/>
" I.- Pour attribuer le marché au candidat qui a présenté l'offre économiquement la plus avantageuse, le pouvoir adjudicateur se fonde : / 1° Soit sur une pluralité de critères non discriminatoires et liés à l'objet du marché (...) 2° Soit, compte tenu de l'objet du marché, sur un seul critère, qui est celui du prix. / (...) III.- Les offres inappropriées, irrégulières et inacceptables sont éliminées. Les autres offres sont classées par ordre décroissant. L'offre la mieux classée est retenue (...) "  ; qu'aux termes de l'article 55 du même code : " Si une offre paraît anormalement basse, le pouvoir adjudicateur peut la rejeter par décision motivée après avoir demandé par écrit les précisions qu'il juge utiles et vérifié les justifications fournies (...) " ; <br/>
<br/>
              Considérant que pour annuler, par l'ordonnance attaquée, la procédure ayant donné lieu à la décision de la commission d'appel d'offres du 13 octobre 2011 attribuant à la société des Autocars de l'Ile de Beauté le lot n° 130, relatif à la ligne entre Vico et Ajaccio, du marché départemental ayant pour objet le service des transports scolaires en Corse du Sud, le juge des référés du tribunal administratif de Bastia s'est fondé sur ce que le département avait retenu une offre d'un montant anormalement bas, dans des conditions de nature à fausser l'égalité entre les entreprises candidates ; que, pour statuer ainsi, il a estimé que le salarié de la société des Autocars Roger Ceccaldi affecté à cette ligne devait être repris par le nouvel attributaire, en application de l'accord du 7 juillet 2009 sur la garantie d'emploi et la poursuite des relations de travail en cas de changement de prestataire dans les transports interurbains de voyageurs, étendu par arrêté interministériel du 22 juillet 2010, et en a déduit que le coût de cette reprise devait nécessairement s'intégrer au prix de l'offre soumise par les candidats ;<br/>
<br/>
              Considérant, toutefois, que si le coût correspondant à la reprise de salariés imposée par les dispositions du code du travail ou par un accord collectif étendu constitue un élément essentiel du marché, dont la connaissance permet aux candidats d'apprécier les charges du cocontractant et d'élaborer utilement une offre, le prix de cette offre ne doit pas nécessairement assurer la couverture intégrale de ce coût, compte tenu des possibilités pour l'entreprise de le compenser, notamment par le redéploiement des effectifs en son sein ou, si l'exécution de ce marché n'assure pas un emploi à temps plein des salariés concernés, de la possibilité de leur donner d'autres missions et donc de n'imputer, pour le calcul du prix de l'offre, qu'un coût salarial correspondant aux heures effectives de travail requises par la seule exécution du marché ; qu'en ne tenant pas compte de ces possibilités, le juge des référés du tribunal administratif de Bastia a commis une erreur de droit ; que, par suite, le DEPARTEMENT DE LA CORSE DU SUD est fondé à demander pour ce motif, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, l'annulation des articles 1er, 2 et 3 de l'ordonnance attaquée ;<br/>
<br/>
              Considérant que dans les circonstances de l'espèce, il y a lieu, en application des dispositions de l'article L. 821-2 du code de justice administrative, de régler, dans cette mesure, l'affaire au titre de la procédure de référé engagée par la société des Autocars Roger Ceccaldi ;<br/>
<br/>
              Considérant, en premier lieu, que, pour demander l'annulation de la procédure ayant donné lieu à la décision de la commission d'appel d'offres du 13 octobre 2011, la société des Autocars Roger Ceccaldi ne peut utilement invoquer les manquements aux obligations de publicité et de mise en concurrence qui auraient affecté tant la procédure ayant conduit à déclarer infructueux un premier appel d'offres publié le 28 février 2011 que la procédure négociée consécutive déclarée sans suite le 29 juillet 2011, le département ayant entièrement repris une nouvelle procédure et publié à cette fin un nouvel appel d'offres le 19 août 2011 ; <br/>
<br/>
              Considérant, en deuxième lieu, qu'il ne résulte ni du constat d'huissier produit par la société des Autocars Roger Ceccaldi, qui n'est pas relatif aux prestations et aux équipements de la société des Autocars de l'Ile de Beauté mais à ceux d'une autre entreprise, qui n'était pas candidate, ni de ses allégations relatives à des manquements de cette société dans l'exécution de précédents marchés, qui ne sont pas établies et sont contestées par le département, que la candidature de la société retenue aurait dû être écartée comme ne présentant pas les niveaux des capacités professionnelles, techniques et financières requis par les règles de la consultation ;<br/>
<br/>
              Considérant, en troisième lieu, qu'il résulte de l'instruction que le véhicule proposé par l'offre retenue présente une capacité de cinquante-sept passagers, conformément à l'annexe 1 au cahier des clauses techniques particulières soumis à la consultation, qui prévoit un effectif de quarante-cinq élèves à transporter ; que si la société des Autocars Roger Ceccaldi soutient que cette définition des besoins du pouvoir adjudicateur serait erronée, son allégation n'est, en tout état de cause, pas établie ; <br/>
<br/>
              Considérant, en quatrième lieu, qu'il résulte clairement des stipulations de l'accord du 7 juillet 2009 étendu par l'arrêté du 22 juillet 2010 qu'aucune d'elles, contrairement à ce que soutient la société des Autocars Roger Ceccaldi, n'interdit que les salariés affectés à un marché de transport routier de voyageurs soient, après leur reprise par un nouveau prestataire, affectés à d'autres tâches au sein de l'entreprise, dans le respect des clauses particulières attachées à leur contrat dans l'entreprise sortante et sous réserve du maintien de leurs éléments de rémunération, conformément au paragraphe 2.4. de l'article 2 de cet accord ;  que, dès lors, le coût de leur reprise par un nouveau prestataire, s'il doit être intégralement supporté par ce dernier, ne peut être imputé au nouveau marché qu'à raison de leur affectation à ce marché ; qu'il résulte en l'espèce de l'instruction que, si le conducteur affecté par l'entreprise sortante à la ligne Vico-Ajaccio est employé à temps complet par cette entreprise, au-delà de la durée de travail requise par le service de cette ligne, l'offre retenue par la commission d'appel d'offres ne prévoit, pour l'exécution du nouveau marché, que 4 h 12 mn de service du conducteur, comprenant les coupures, prises et fins de service, pour chacun des 175 jours prévisionnels de fonctionnement de la ligne dans l'année ; qu'à supposer, par conséquent, que le conducteur affecté par l'entreprise sortante à la ligne correspondant au marché en cause remplisse les conditions fixées par l'accord étendu du 7 juillet 2009 pour être repris par le nouveau prestataire et reste affecté au service de cette ligne, le coût salarial total que représente cette reprise n'est à rattacher au nouveau marché que dans cette mesure ; qu'eu égard à cette circonstance, et compte tenu des autres coûts que devra supporter le nouveau prestataire, il ne résulte pas de l'instruction qu'en ne rejetant pas l'offre retenue comme anormalement basse et susceptible de rendre difficile l'exécution du marché, le pouvoir adjudicateur aurait entaché sa décision d'une erreur manifeste d'appréciation ni, dès lors, méconnu le principe d'égalité entre les candidats et manqué à ses obligations de mise en concurrence ;<br/>
<br/>
              Considérant, en dernier lieu, que si les dispositions de l'article R. 213-6 du code de l'éducation imposent, comme le prévoyait le règlement de la consultation, de conclure par périodes entières correspondant à une ou plusieurs années scolaires les conventions relatives à l'exécution de services de transports scolaires, ces dispositions n'ont ni pour objet ni pour effet de prohiber les stipulations prévoyant, en vue d'assurer la continuité du service public, la reprise de ce service en cours d'année scolaire, lorsqu'il a été interrompu ou que, comme en l'espèce, un nouveau contrat n'a pu être conclu à temps pour l'exécution de ce service ; qu'une offre de services de transports scolaires dont l'exécution ne commencera nécessairement, du fait du retard pris dans la passation du marché, qu'à partir d'une date postérieure à la rentrée scolaire ne méconnaît donc pas ces dispositions et n'est pas inacceptable ; que la société des Autocars Roger Ceccaldi n'est, par suite, pas fondée à soutenir que le département aurait méconnu ses obligations de mise en concurrence en retenant une telle offre ;<br/>
<br/>
              Considérant qu'il résulte de ce qui précède que la société des Autocars Roger Ceccaldi n'est pas fondée à demander l'annulation de la procédure de passation du marché de transports scolaires entre Vico et Ajaccio ayant conduit à la décision de la commission d'appel d'offres du 13 octobre 2011 ; que sa demande doit donc être rejetée ;<br/>
<br/>
              Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge du DEPARTEMENT DE LA CORSE DU SUD, qui n'est pas, dans la présente instance, la partie perdante, la somme que la société des Autocars Roger Ceccaldi demande au titre des frais exposés par elle et non compris dans les dépens ; qu'il y a lieu, en revanche, de faire droit aux conclusions du département tendant, d'une part, à ce que la société lui verse la somme de 3 000 euros au titre des mêmes dispositions et, d'autre part, à ce que soit également mise à la charge de cette société la contribution pour l'aide juridique acquittée par le département ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les articles 1er, 2 et 3 de l'ordonnance du juge des référés du tribunal administratif de Bastia du 3 novembre 2011 sont annulés.<br/>
Article 2 : La demande de la société des Autocars Roger Ceccaldi tendant à l'annulation de la procédure de passation du marché du DEPARTEMENT DE LA CORSE DU SUD pour les transports scolaires entre Vico et Ajaccio est rejetée.<br/>
Article 3 : La contribution pour l'aide juridique acquittée par le DEPARTEMENT DE LA CORSE DU SUD est mise à la charge de la société des Autocars Roger Ceccaldi.<br/>
Article 4 : La société des Autocars Roger Ceccaldi versera au DEPARTEMENT DE LA CORSE DU SUD une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 5 : La présente décision sera notifiée au DEPARTEMENT DE LA CORSE DU SUD et à la société des Autocars Roger Ceccaldi. <br/>
Copie en sera adressée à la société des Autocars de l'Île de Beauté.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-08-015-01 MARCHÉS ET CONTRATS ADMINISTRATIFS. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. PROCÉDURES D'URGENCE. - MOYEN TIRÉ DE CE QUE LE POUVOIR ADJUDICATEUR AURAIT DÛ REJETER UNE OFFRE ANORMALEMENT BASSE (ART. 55 DU CMP) - 1) OPÉRANCE - EXISTENCE (SOL. IMPL.) - CONTRÔLE DU JUGE SUR LE REFUS DE REJETER POUR CE MOTIF UNE OFFRE - CONTRÔLE RESTREINT [RJ1].
</SCT>
<ANA ID="9A"> 39-08-015-01 1) Est utilement invocable dans le cadre du référé précontractuel le moyen tiré de ce que le pouvoir adjudicateur aurait dû, en application de l'article 55 du code des marchés publics (CMP), rejeter une offre anormalement basse. 2) Le juge du référé précontractuel exerce sur un tel refus un contrôle limité à l'erreur manifeste d'appréciation.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., sur l'existence d'un tel degré de contrôle par le juge de plein contentieux du référé précontractuel, CE, 21 mai 2010, Commune d'Ajaccio, n° 333737, T. p. 849. Rappr., sur le contrôle restreint exercé en excès de pouvoir sur la décision de la personne publique d'écarter une offre anormalement basse, CE, 15 avril 1996, Commune de Poindimie, n° 133171, inédite au Recueil ; CE, 29 janvier 2003, Département d'Ille-et-Vilaine, n° 208096, inédite au Recueil.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
