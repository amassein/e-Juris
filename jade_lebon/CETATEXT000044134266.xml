<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044134266</ID>
<ANCIEN_ID>JG_L_2021_09_000000435323</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/13/42/CETATEXT000044134266.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 6ème chambres réunies, 29/09/2021, 435323, Publié au recueil Lebon</TITRE>
<DATE_DEC>2021-09-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>435323</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 6ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>M. Olivier Rousselle</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:435323.20210929</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. B... D..., agissant en son nom propre et au nom de ses enfants E... et C..., a demandé au tribunal administratif d'Orléans de condamner l'Etat à l'indemniser des préjudices qu'il estime avoir subis à la suite d'une vaccination obligatoire. Par un jugement n°1500510 du 30 mai 2017, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un arrêt n°17NT03250 du 5 juillet 2019, la cour administrative d'appel de Nantes a rejeté l'appel formé par M. D... contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 14 octobre 2019 et les 15 janvier et 16 décembre 2020 au secrétariat du contentieux du Conseil d'Etat, M. D..., agissant en son nom propre et au nom de ses enfants, demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel.<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L.761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la santé publique ;<br/>
              - le code de justice administrative <br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Rousselle, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteure publique.<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de M. D... ;<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que M. D..., vacciné en 1994 et 1995 contre le virus de l'hépatite B, à titre obligatoire, pendant son service militaire, a souffert à partir de septembre 1995 de divers troubles qu'il a attribués à cette vaccination, en lien avec une myofasciite à macrophages par ailleurs diagnostiquée en 1997. Il a bénéficié pour ce motif, à partir de 2001, d'une pension militaire d'invalidité. Le ministre de la défense a toutefois rejeté sa demande d'indemnisation des préjudices non indemnisés par cette pension, par une décision du 17 mars 2015. Par un jugement du 30 mai 2017, le tribunal administratif d'Orléans a rejeté la demande de M. D... tendant à la condamnation de l'Etat à lui verser 58 000 euros au titre de ses préjudices propres et 10 000 euros au titre des préjudices de ses deux enfants mineurs. A... se pourvoit en cassation contre l'arrêt du 5 juillet 2019 par lequel la cour administrative d'appel de Nantes a rejeté son appel formé contre ce jugement.<br/>
<br/>
              2. Il résulte des termes mêmes de l'arrêt attaqué que, pour rejeter l'appel de M. D..., la cour administrative d'appel a estimé, en se fondant sur les travaux de l'Académie nationale de médecine, du Haut conseil de santé publique, de l'Académie nationale de pharmacie et de l'Organisation mondiale de la santé consacrés aux liens susceptibles d'exister entre l'administration de vaccins contenant des adjuvants aluminiques et le développement de différents symptômes constitués de lésions histologiques de myofasciite à macrophages, de fatigue chronique, de douleurs articulaires et musculaires et de troubles cognitifs, qu'aucun lien de causalité n'avait, à la date de son arrêt, été scientifiquement établi.<br/>
<br/>
              3. Toutefois, en statuant ainsi, alors qu'elle était saisie d'un litige individuel portant sur les conséquences pour la personne concernée d'une vaccination présentant un caractère obligatoire, la cour a commis une erreur de droit. En effet, pour écarter toute responsabilité de la puissance publique, il appartenait à la cour, non pas de rechercher si le lien de causalité entre l'administration d'adjuvants aluminiques et les différents symptômes attribués à la myofasciite à macrophages était ou non établi, mais de s'assurer, au vu du dernier état des connaissances scientifiques en débat devant elle, qu'il n'y avait aucune probabilité qu'un tel lien existe.<br/>
<br/>
              4. Il appartenait ensuite à la cour, après avoir procédé à la recherche mentionnée au point précédent, soit, s'il en était ressorti, en l'état des connaissances scientifiques en débat devant elle, qu'il n'y avait aucune probabilité qu'un tel lien existe, de rejeter l'appel de M. D..., soit, dans l'hypothèse inverse, de procéder à l'examen des circonstances de l'espèce et de ne retenir alors l'existence d'un lien de causalité entre les vaccinations obligatoires subies par l'intéressé et les symptômes qu'il avait ressentis que si ceux-ci étaient apparus, postérieurement à la vaccination, dans un délai normal pour ce type d'affection, ou s'étaient aggravés à un rythme et une ampleur qui n'étaient pas prévisibles au vu de son état de santé antérieur ou de ses antécédents et, par ailleurs, qu'il ne ressortait pas du dossier qu'ils pouvaient être regardés comme résultant d'une autre cause que ces vaccinations.<br/>
<br/>
              5. Mais il résulte de ce qui a été dit au point 3 que M. D... est fondé, sans qu'il soit besoin de se prononcer sur les autres moyens de son pourvoi, à demander l'annulation de l'arrêt qu'il attaque.<br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 3000 euros à verser à M. D... au titre de l'article L.761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				  --------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nantes du 5 juillet 2019 est annulé.<br/>
<br/>
		Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nantes.<br/>
<br/>
Article 3 : L'Etat versera à M. D... la somme de 3000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à M. B... D..., à la ministre des armées et au ministre de la santé et des solidarités.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">60-02-01-03 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. - RESPONSABILITÉ EN RAISON DES DIFFÉRENTES ACTIVITÉS DES SERVICES PUBLICS. - SERVICE PUBLIC DE SANTÉ. - SERVICE DES VACCINATIONS. - INDEMNISATION DES DOMMAGES IMPUTABLES AUX VACCINATIONS OBLIGATOIRES - CONDITIONS [RJ1] - 1) EXISTENCE D'UNE PROBABILITÉ NON NULLE QU'UN LIEN DE CAUSALITÉ EXISTE ENTRE L'ADMINISTRATION DU VACCIN ET LES SYMPTÔMES ATTRIBUÉS À L'AFFECTION [RJ2] - 2) RECONNAISSANCE, AU CAS D'ESPÈCE, D'UN TEL LIEN - CONDITION - FAISCEAU D'INDICES.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">61-01 SANTÉ PUBLIQUE. - PROTECTION GÉNÉRALE DE LA SANTÉ PUBLIQUE. - INDEMNISATION DES DOMMAGES IMPUTABLES AUX VACCINATIONS OBLIGATOIRES - CONDITIONS [RJ1] - 1) EXISTENCE D'UNE PROBABILITÉ NON NULLE QU'UN LIEN DE CAUSALITÉ EXISTE ENTRE L'ADMINISTRATION DU VACCIN ET LES SYMPTÔMES ATTRIBUÉS À L'AFFECTION [RJ2] - 2) RECONNAISSANCE, AU CAS D'ESPÈCE, D'UN TEL LIEN - CONDITION - FAISCEAU D'INDICES.
</SCT>
<ANA ID="9A"> 60-02-01-03 1) Saisi d'un litige individuel portant sur les conséquences pour la personne concernée d'une vaccination présentant un caractère obligatoire, il appartient au juge, pour écarter toute responsabilité de la puissance publique, non pas de rechercher si le lien de causalité entre l'administration du vaccin et les différents symptômes attribués à l'affection dont souffre l'intéressé est ou non établi, mais de s'assurer, au vu du dernier état des connaissances scientifiques en débat devant le juge, qu'il n'y a aucune probabilité qu'un tel lien existe.......2) Il appartient ensuite au juge, après avoir procédé à la recherche mentionnée au point précédent, soit, s'il en était ressorti, en l'état des connaissances scientifiques en débat devant lui, qu'il n'y a aucune probabilité qu'un tel lien existe, de rejeter la demande indemnitaire, soit, dans l'hypothèse inverse, de procéder à l'examen des circonstances de l'espèce et de ne retenir alors l'existence d'un lien de causalité entre les vaccinations obligatoires subies par l'intéressé et les symptômes qu'il avait ressentis que si ceux-ci étaient apparus, postérieurement à la vaccination, dans un délai normal pour ce type d'affection, ou s'étaient aggravés à un rythme et une ampleur qui n'étaient pas prévisibles au vu de son état de santé antérieur ou de ses antécédents et, par ailleurs, qu'il ne ressortait pas du dossier qu'ils pouvaient être regardés comme résultant d'une autre cause que ces vaccinations.</ANA>
<ANA ID="9B"> 61-01 1) Saisi d'un litige individuel portant sur les conséquences pour la personne concernée d'une vaccination présentant un caractère obligatoire, il appartient au juge, pour écarter toute responsabilité de la puissance publique, non pas de rechercher si le lien de causalité entre l'administration du vaccin et les différents symptômes attribués à l'affection dont souffre l'intéressé est ou non établi, mais de s'assurer, au vu du dernier état des connaissances scientifiques en débat devant le juge, qu'il n'y a aucune probabilité qu'un tel lien existe.......2) Il appartient ensuite au juge, après avoir procédé à la recherche mentionnée au point précédent, soit, s'il en était ressorti, en l'état des connaissances scientifiques en débat devant lui, qu'il n'y a aucune probabilité qu'un tel lien existe, de rejeter la demande indemnitaire, soit, dans l'hypothèse inverse, de procéder à l'examen des circonstances de l'espèce et de ne retenir alors l'existence d'un lien de causalité entre les vaccinations obligatoires subies par l'intéressé et les symptômes qu'il avait ressentis que si ceux-ci étaient apparus, postérieurement à la vaccination, dans un délai normal pour ce type d'affection, ou s'étaient aggravés à un rythme et une ampleur qui n'étaient pas prévisibles au vu de son état de santé antérieur ou de ses antécédents et, par ailleurs, qu'il ne ressortait pas du dossier qu'ils pouvaient être regardés comme résultant d'une autre cause que ces vaccinations.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., en précisant, CE, 9 mars 2007, Mme Schwartz, n° 267635, p. 118. Rapp., s'agissant de l'imputablité au service d'une maladie survenue à la suite d'une vaccination, CE, 21 novembre 2011, Ville de Paris et Landry, n°s 344561 356462, p. 386...[RJ2] Comp., qui tient compte, s'agissant d'une décision de mise sur le marché d'un vaccin, de l'absence de lien de causalité scientifiquement avéré, CE, 6 mai 2019, M. Baudelet de Livois et autres, n° 415694, p. 163.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
