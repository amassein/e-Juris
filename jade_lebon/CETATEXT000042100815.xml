<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042100815</ID>
<ANCIEN_ID>JG_L_2020_07_000000427002</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/10/08/CETATEXT000042100815.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 08/07/2020, 427002</TITRE>
<DATE_DEC>2020-07-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>427002</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELAMARRE, JEHANNIN ; SCP BOUTET-HOURDEAUX</AVOCATS>
<RAPPORTEUR>M. Christian Fournier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Cytermann</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:427002.20200708</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. C... D... a demandé au tribunal administratif de Lyon d'annuler la délibération du conseil municipal de la commune de Messimy-sur-Saône du 7 septembre 2012 par laquelle il a attribué la protection fonctionnelle à M. du B..., ancien maire. Par un jugement n° 1301517 du 31 décembre 2015, le tribunal administratif de Lyon a rejeté sa demande.<br/>
<br/>
              Par un arrêt n°16LY00879 du 15 mai 2018, la cour administrative d'appel de Lyon a rejeté l'appel formé par M. D... contre ce jugement.  <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 11 janvier et 7 mars 2019 au secrétariat du contentieux du Conseil d'Etat, M. D... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Messimy-sur-Saône la somme de 3 500 euros à verser à la SCP Delamarre et Jehannin, avocat de M. D..., au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des collectivités territoriales ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 modifiée ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Christian Fournier, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Laurent Cytermann, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Delamarre, Jéhannin, avocat de M. D... et à la SCP Boutet-Hourdeaux, avocat de la commune de Messimy-sur-Saône ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. C... D..., qui réside sur le territoire de la commune de Messimy-sur-Saône, a assigné devant le tribunal de grande instance de Bourg-en-Bresse pour des faits d'entraves discriminatoires M. E... A... B..., maire de cette commune entre 2002 et 2008. Par une délibération du 7 septembre 2012, le conseil municipal de la commune de Messimy-sur-Saône, estimant que les faits qui lui étaient reprochés n'étaient pas détachables de l'exercice de son mandat, a accordé à M. A... B... le bénéfice de la protection fonctionnelle et décidé de prendre en charge les honoraires de son avocat. M. D... se pourvoit en cassation contre l'arrêt du 15 mai 2018 de la cour administrative d'appel de Lyon qui a rejeté son appel contre le jugement du 31 décembre 2015 par lequel le tribunal administratif de Lyon a rejeté sa demande tendant à l'annulation de cette délibération.<br/>
<br/>
              2. Lorsqu'un agent public est mis en cause par un tiers à raison de ses fonctions, il incombe à la collectivité publique dont il dépend de lui accorder sa protection dans le cadre d'une instance civile non seulement en le couvrant des condamnations civiles prononcées contre lui mais aussi en prenant en charge l'ensemble des frais de cette instance, dans la mesure où une faute personnelle détachable du service ne lui est pas imputable ; de même, il lui incombe de lui accorder sa protection dans le cas où il fait l'objet de poursuites pénales, sauf s'il a commis une faute personnelle, et, à moins qu'un motif d'intérêt général ne s'y oppose, de le protéger contre les menaces, violences, voies de fait, injures, diffamations ou outrages dont il est l'objet. Ce principe général du droit a d'ailleurs été expressément réaffirmé par la loi, notamment en ce qui concerne les fonctionnaires et agents non titulaires, par l'article 11 de la loi du 13 juillet 1983 portant statut général de la fonction publique et par les articles L. 2123-34, L. 2123-35, L. 3123-28, L. 3123-29, L. 4135-28 et L. 4135-29 du code général des collectivités territoriales, s'agissant des exécutifs des collectivités territoriales. Cette protection s'applique à tous les agents publics, quel que soit le mode d'accès à leurs fonctions.<br/>
<br/>
              3. En premier lieu, il ressort des pièces du dossier soumis aux juges du fond que la délibération du 7 septembre 2012 de la commune de Messimy-sur-Saône avait pour objet d'assurer la prise en charge des frais que M. A... B... était susceptible d'engager pour assurer sa défense devant les juridictions civiles. Il résulte de ce qui a été dit au point 2 qu'en jugeant que l'attribution de la protection par la collectivité publique constitue une obligation lorsque l'agent fait l'objet de poursuites pénales ou d'une action civile, en l'absence de faute personnelle qui lui est imputable, la cour n'a pas commis d'erreur de droit. <br/>
<br/>
              4. En deuxième lieu, c'est sans commettre d'erreur de droit que la cour a jugé que la commune pouvait légalement accorder sa protection sans qu'une demande écrite formalisée lui soit adressée par le bénéficiaire. <br/>
<br/>
              5. En troisième lieu, en relevant que M. D... n'apportait aucun élément sérieux à l'appui de ses allégations relatives au caractère détachable des fautes pour lesquelles M. A... B... a été assigné, la cour n'a ni inversé la charge de la preuve ni commis d'erreur de droit.<br/>
<br/>
              6. Il résulte de tout ce qui précède que M. D... n'est pas fondé à demander l'annulation de l'arrêt qu'il attaque. <br/>
<br/>
              7. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'il soit fait droit aux conclusions présentées à ce titre par M. D.... Il y a lieu, en revanche, dans les circonstances de l'espèce, de mettre à la charge de M. D... la somme de 1 500 euros à verser à la commune de Messimy-sur-Saône, au titre des mêmes dispositions. <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le pourvoi de M. D... est rejeté. <br/>
Article 2 : M. D... versera à la commune de Messimy-sur-Saône une somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 3 : La présente décision sera notifiée à M. C... D... et à la commune de Messimy-sur-Saône.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-04-03-07-04 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. PRINCIPES GÉNÉRAUX DU DROIT. PRINCIPES INTÉRESSANT L'ACTION ADMINISTRATIVE. GARANTIES DIVERSES ACCORDÉES AUX AGENTS PUBLICS. - PROTECTION FONCTIONNELLE - ETENDUE - PRISE EN CHARGE DES FRAIS DE L'INSTANCE CIVILE - INCLUSION [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">36-07-10-005 FONCTIONNAIRES ET AGENTS PUBLICS. STATUTS, DROITS, OBLIGATIONS ET GARANTIES. GARANTIES ET AVANTAGES DIVERS. PROTECTION CONTRE LES ATTAQUES. - PROTECTION FONCTIONNELLE - ETENDUE - PRISE EN CHARGE DES FRAIS DE L'INSTANCE CIVILE - INCLUSION [RJ1].
</SCT>
<ANA ID="9A"> 01-04-03-07-04 Lorsqu'un agent public est mis en cause par un tiers à raison de ses fonctions, il incombe à la collectivité publique dont il dépend de lui accorder sa protection dans le cadre d'une instance civile non seulement en le couvrant des condamnations civiles prononcées contre lui mais aussi en prenant en charge l'ensemble des frais de cette instance, dans la mesure où une faute personnelle détachable du service ne lui est pas imputable.</ANA>
<ANA ID="9B"> 36-07-10-005 Lorsqu'un agent public est mis en cause par un tiers à raison de ses fonctions, il incombe à la collectivité publique dont il dépend de lui accorder sa protection dans le cadre d'une instance civile non seulement en le couvrant des condamnations civiles prononcées contre lui mais aussi en prenant en charge l'ensemble des frais de cette instance, dans la mesure où une faute personnelle détachable du service ne lui est pas imputable.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., sur l'obligation de couvrir le fonctionnaire des condamnations civiles prononcées contre lui, CE, Section, 26 avril 1963, Centre hospitalier de Besançon, n° 42763, p. 243.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
