<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037534175</ID>
<ANCIEN_ID>JG_L_2018_10_000000421292</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/53/41/CETATEXT000037534175.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 26/10/2018, 421292</TITRE>
<DATE_DEC>2018-10-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>421292</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Christelle Thomas</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:421292.20181026</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au juge des référés du tribunal administratif de Lille, sur le fondement de l'article R. 541-1 du code de justice administrative, de condamner l'Etat à lui verser une provision de 3 576,78 euros au titre des arriérés de salaires dus pour son emploi aux ateliers et au service général du centre pénitentiaire de Maubeuge (Nord).<br/>
<br/>
              Par une ordonnance n° 1801346 du 22 mai 2018, le juge des référés du tribunal administratif de Lille a condamné l'Etat à lui verser une somme de 3 502,68 euros à titre de provision et rejeté le surplus de sa demande.<br/>
<br/>
              Par un pourvoi et un nouveau mémoire, enregistrés les 7 juin et 15 octobre 2018 au secrétariat du contentieux du Conseil d'Etat, le garde des sceaux, ministre de la justice, demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance en tant qu'elle condamne l'Etat à verser une provision à M. A...;<br/>
<br/>
              2°) statuant en référé, de rejeter la demande M. A...;<br/>
<br/>
              3°) à titre subsidiaire, de procéder à l'homologation du protocole transactionnel conclu entre M. A...et l'administration pénitentiaire.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code civil ;<br/>
              - le code de procédure pénale ;<br/>
              - le code des relations entre le public et l'administration, notamment son article L. 423 1 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Christelle Thomas, maître des requêtes en service extraordinaire ;  <br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge des référés que M.A..., alors détenu au centre pénitentiaire de Maubeuge (Nord), a été affecté aux ateliers et au service général du centre pénitentiaire de janvier 2015 à janvier 2017. Par un courrier en date du 20 octobre 2017, il a formé un recours auprès du directeur interrégional des services pénitentiaires des Hauts-De-France aux fins d'être indemnisé du préjudice résultant du mode de calcul erroné de sa rémunération sur cette période. Par un courrier du 22 janvier 2018, l'administration pénitentiaire a reconnu que M. A...était créancier d'une somme de 3 502,68 euros. Ce dernier a signé le 5 février 2018 un protocole transactionnel par lequel il a accepté la somme proposée par l'administration pénitentiaire et déclaré, en contrepartie, renoncer à tout recours contre le ministère de la justice ayant le même objet. Toutefois, en l'absence de versement de la somme réclamée, M. A...a saisi, le 14 février 2018, le juge des référés du tribunal administratif de Lille, sur le fondement de l'article R 541-1 du code de justice administrative, d'une demande tendant à ce que l'Etat soit condamné à lui verser la somme de 3 576,78 euros à titre de provision. Par une ordonnance du 22 mai 2018 contre laquelle la garde des sceaux, ministre de la justice, se pourvoit en cassation, le juge des référés du tribunal administratif de Lille a condamné l'Etat à verser une provision de 3 502,68 euros au titre des arriérés de salaire dus à M. A...et rejeté le surplus de sa demande.<br/>
<br/>
              2. D'une part, l'article 2044 du code civil dispose que : " La transaction est un contrat par lequel les parties, par des concessions réciproques, terminent une contestation née, ou préviennent une contestation à naître ". Aux termes de l'article 2052 dudit code : " La transaction fait obstacle à l'introduction ou à la poursuite entre les parties d'une action en justice ayant le même objet ". L'article L. 423-1 du code des relations entre le public et l'administration dispose que : " Ainsi que le prévoit l'article 2044 du code civil et sous réserve qu'elle porte sur un objet licite et contienne des concessions réciproques et équilibrées, il peut être recouru à une transaction pour terminer une contestation née ou prévenir une contestation à naître avec l'administration. La transaction est formalisée par un contrat écrit ".  D'autre part, l'article 6  du code civil dispose que : " On ne peut déroger, par des conventions particulières, aux lois qui intéressent l'ordre public [...] ".<br/>
<br/>
              3. Il résulte de ces dispositions que l'administration peut, afin de prévenir ou d'éteindre un litige, légalement conclure avec un particulier un protocole transactionnel, sous réserve de la licéité de l'objet de ce dernier, de l'existence de concessions réciproques et équilibrées entre les parties et du respect de l'ordre public. <br/>
<br/>
              4. Par suite, en jugeant, pour écarter la fin de non-recevoir opposée par la ministre de la justice à la demande de M.A..., qu'un protocole transactionnel comportant un engagement à renoncer à engager une action en justice ne saurait, en aucun cas, faire obstacle à l'exercice d'un recours juridictionnel, le juge des référés du tribunal administratif de Lille a commis une erreur de droit.<br/>
<br/>
              4. Toutefois, aux termes de l'article 717-3 du code de procédure pénale : " (...) La rémunération du travail des personnes détenues ne peut être inférieure à un taux horaire fixé par décret et indexé sur le salaire minimum de croissance défini à l'article L. 3231-2 du code du travail. Ce taux peut varier en fonction du régime sous lequel les personnes détenues sont employées ". En application des dispositions combinées des articles 717-3 et D. 432-1 du même code, la rémunération du travail effectué au sein des établissements pénitentiaires ne peut être inférieure à des taux horaires qui varient suivant la nature des activités exercées par la personne détenue.<br/>
<br/>
              5. Ces dispositions réglant entièrement les conditions de la rémunération du travail des personnes détenues et excluant pour leur application toute recherche de concessions réciproques et équilibrées entre les parties, le protocole transactionnel conclu le 5 février 2018, qui règle un litige n'ayant pas pour objet de réparer un préjudice mais exclusivement d'assurer le versement des salaires légalement dus à M.A..., ne saurait faire obstacle à la saisine du  juge des référés, sur le fondement de l'article R. 541-1 du code de justice administrative. Ce motif, qui répond à un moyen d'ordre public soulevé par le juge de cassation et dont l'examen n'implique l'appréciation d'aucune circonstance de fait, doit être substitué au motif juridiquement erroné retenu par l'ordonnance attaquée, dont il justifie légalement le dispositif.<br/>
<br/>
              6. Il résulte de ce qui précède que la garde des sceaux, ministre de la justice, n'est pas fondée à demander l'annulation de l'ordonnance attaquée.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
          --------------<br/>
<br/>
Article 1er : Le pourvoi du garde des sceaux, ministre de la justice, est rejeté.<br/>
Article 2 : La présente décision sera notifiée à la garde des sceaux, ministre de la justice, et à M. B... A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">37-07-01 JURIDICTIONS ADMINISTRATIVES ET JUDICIAIRES. - TRANSACTION CONCLUE PAR L'ADMINISTRATION (ART. L. 423-1 DU CRPA) - 1) PRINCIPE - NÉCESSITÉ DE CONCESSIONS RÉCIPROQUES ET ÉQUILIBRÉES ENTRE LES PARTIES - 2) APPLICATION - POSSIBILITÉ DE CONCLURE UNE TRANSACTION AU SUJET D'UN LITIGE SALARIAL DONT LE RÈGLEMENT DÉCOULE DE LA SEULE APPLICATION DE LA RÈGLE DE DROIT - ABSENCE.
</SCT>
<ANA ID="9A"> 37-07-01 1) Il résulte des articles 6, 2044 et 2052 du code civil, et de l'article L. 423-1 du code des relations entre le public et l'administration (CRPA) que l'administration peut, afin de prévenir ou d'éteindre un litige, légalement conclure avec un particulier un protocole transactionnel, sous réserve de la licéité de l'objet de ce dernier, de l'existence de concessions réciproques et équilibrées entre les parties et du respect de l'ordre public.... ,,2) Détenu ayant conclu une transaction avec l'administration pénitentiaire au sujet du calcul erroné de sa rémunération pour le travail effectué en établissement pénitentiaire et renonçant, en contrepartie de la somme proposée par l'administration, à tout recours contre le ministère de la justice ayant le même objet.... ,,Les articles 717-3 et D. 432-1 du code de procédure pénale (CPP) réglant entièrement les conditions de la rémunération du travail des personnes détenues et excluant pour leur application toute recherche de concessions réciproques et équilibrées entre les parties, un tel protocole transactionnel, qui règle un litige n'ayant pas pour objet de réparer un préjudice mais exclusivement d'assurer le versement des salaires légalement dus à l'intéressé, ne saurait faire obstacle à la saisine du juge des référés, sur le fondement de l'article R. 541-1 du code de justice administrative (CJA), tendant au versement, à titre de provision, de la somme en cause.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
