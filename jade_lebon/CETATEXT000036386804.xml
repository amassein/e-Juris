<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036386804</ID>
<ANCIEN_ID>JG_L_2017_12_000000411820</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/38/68/CETATEXT000036386804.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 28/12/2017, 411820</TITRE>
<DATE_DEC>2017-12-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>411820</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Cécile Barrois de Sarigny</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CECHR:2017:411820.20171228</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par un jugement n° 1700794 du 24 mai 2017, enregistré le 23 juin 2017 au secrétariat du contentieux du Conseil d'Etat, le tribunal administratif de Caen, avant de statuer sur la demande de M. A...B..., tendant à l'annulation de l'arrêté du 11 avril 2017 par lequel le préfet de la Manche l'a assigné à résidence dans le département de la Manche pour une durée de six mois maximum en raison de l'engagement d'une procédure de détermination de l'Etat responsable de sa demande d'asile, a décidé, par application des dispositions de l'article L. 113-1 du code de justice administrative, de transmettre le dossier de cette demande au Conseil d'Etat, en soumettant à son examen la question de savoir si la procédure prévue au III de l'article  L. 512-1 du code de l'entrée et du séjour des étrangers et du droit d'asile s'applique en cas de recours formé à l'encontre d'une décision d'assignation à résidence prise sur le fondement de l'article L. 742-2 du même code.<br/>
<br/>
              ..................................................................<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le règlement (UE) n° 604/2013 du Parlement européen et du Conseil du 26 juin 2013 ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ; <br/>
              - le code de justice administrative, notamment son article L. 113-1 ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Cécile Barrois de Sarigny, maître des requêtes, <br/>
<br/>
              - les conclusions de M. Guillaume Odinet, rapporteur public,<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              REND L'AVIS SUIVANT<br/>
<br/>
<br/>
              1. Les articles 20 et suivants du règlement (UE) n° 604/2013 du Parlement européen et du Conseil du 26 juin 2013 établissant les critères et mécanismes de détermination de l'État membre responsable de l'examen d'une demande de protection internationale introduite dans l'un des États membres par un ressortissant de pays tiers ou un apatride fixent les règles selon lesquelles sont organisées les procédures de prise en charge ou de reprise en charge d'un demandeur d'asile par l'Etat membre responsable de l'examen de sa demande d'asile. Ces articles déterminent notamment les conditions dans lesquelles l'Etat sur le territoire duquel se trouve le demandeur d'asile requiert de l'Etat qu'il estime responsable de l'examen de la demande de prendre ou de reprendre en charge le demandeur d'asile.<br/>
<br/>
              2. Pour la mise en oeuvre de ces dispositions, les articles L. 742-1 et suivants du code de l'entrée et du séjour des étrangers et du droit d'asile organisent la procédure de détermination de l'Etat responsable de la demande d'asile. <br/>
<br/>
              Dans ce cadre, l'article L. 742-2 prévoit que l'autorité administrative peut, aux fins de mise en oeuvre de la procédure de détermination de l'Etat responsable de l'examen de la demande d'asile et du traitement rapide et du suivi efficace de cette demande, assigner à résidence le demandeur dès le début de la procédure. A cet égard, l'article L. 742-2 dispose que : " La décision d'assignation à résidence est motivée. Elle peut être prise pour une durée maximale de six mois et renouvelée une fois dans la même limite de durée, par une décision également motivée. / Le demandeur astreint à résider dans les lieux qui lui sont fixés doit se présenter aux convocations de l'autorité administrative, répondre aux demandes d'information et se rendre aux entretiens prévus dans le cadre de la procédure de détermination de l'Etat responsable de l'examen de sa demande d'asile. L'autorité administrative peut prescrire à l'étranger la remise de son passeport ou de tout document justificatif de son identité, dans les conditions prévues à l'article L. 611-2 ". <br/>
<br/>
              Si, par la suite, une décision de transfert vers un autre Etat membre est prise sur le fondement de l'article L. 742-3 du même code, l'étranger qui en est l'objet peut, en vertu de l'article L. 742-4, être placé en rétention ou faire l'objet d'une autre mesure d'assignation à résidence, en application cette fois des dispositions de l'article L. 561-2 du même code. L'article L. 742-4 précise alors que la décision de transfert et la décision d'assignation à résidence peuvent faire l'objet d'un recours dans les quarante huit heures devant le président du tribunal administratif, qui statue selon la procédure et dans le délai prévu au III de l'article L. 512-1 du même code, c'est-à-dire dans les soixante douze heures.<br/>
<br/>
              3. Il résulte des termes mêmes des dispositions du code de l'entrée et du séjour des étrangers et du droit d'asile que, s'agissant des différentes mesures d'assignation à résidence susceptibles d'être prononcées dans le cadre de la procédure de détermination de l'Etat responsable de l'examen d'une demande d'asile, le législateur n'a prévu la mise en oeuvre de la procédure particulière de recours du III de l'article L. 512-1 que pour la contestation des mesures d'assignation à résidence prises en vertu de l'article L. 742-4 après l'intervention des décisions de transfert. En revanche, pour les mesures d'assignation à résidence susceptibles d'être prises en début de procédure, sur le fondement de l'article L. 742-2, avant l'intervention des décisions de transfert, aucune disposition ne rend applicable la procédure particulière du III de l'article L. 512-1. Il s'ensuit que les recours dirigés contre ces mesures d'assignation à résidence prises en vertu de l'article L. 742-2 doivent être jugés selon les règles de droit commun applicables devant les tribunaux administratifs. <br/>
<br/>
<br/>
<br/>
              Le présent avis sera notifié au tribunal administratif de Caen, à M. A...B...et au ministre d'Etat, ministre de l'intérieur. <br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">095-02-03 - RECOURS DIRIGÉ CONTRE UNE MESURE D'ASSIGNATION À RÉSIDENCE PRONONCÉE AUX FINS DE MISE EN OEUVRE DE LA PROCÉDURE DE DÉTERMINATION DE L'ETAT RESPONSABLE DE L'EXAMEN DE LA DEMANDE D'ASILE (ART. L. 742-2 DU CESEDA) - PROCÉDURE APPLICABLE - PROCÉDURE DE DROIT COMMUN DEVANT LES TRIBUNAUX ADMINISTRATIFS.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">15-05-045-05 COMMUNAUTÉS EUROPÉENNES ET UNION EUROPÉENNE. RÈGLES APPLICABLES. - RECOURS DIRIGÉ CONTRE UNE MESURE D'ASSIGNATION À RÉSIDENCE PRONONCÉE AUX FINS DE MISE EN OEUVRE DE LA PROCÉDURE DE DÉTERMINATION DE L'ETAT RESPONSABLE DE L'EXAMEN DE LA DEMANDE D'ASILE PRÉVUE PAR LE RÈGLEMENT DUBLIN III (ART. L. 742-2 DU CESEDA) - PROCÉDURE APPLICABLE - PROCÉDURE DE DROIT COMMUN DEVANT LES TRIBUNAUX ADMINISTRATIFS.
</SCT>
<ANA ID="9A"> 095-02-03 Les recours dirigés contre les mesures d'assignation à résidence prononcées sur le fondement de l'article L. 742-2 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA), aux fins de mise en oeuvre de la procédure de détermination de l'Etat responsable de l'examen de la demande d'asile, doivent être jugés selon les règles de droit commun applicables devant les tribunaux administratifs, à la différence des recours dirigés contre les mesures d'assignation à résidence prises après l'intervention des décisions de transfert, sur le fondement de l'article L. 742-4 de ce code, qui doivent être jugés selon la procédure particulière prévue au III de l'article L. 512-1 du même code [RJ1].</ANA>
<ANA ID="9B"> 15-05-045-05 Les recours dirigés contre les mesures d'assignation à résidence prononcées sur le fondement de l'article L. 742-2 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA), aux fins de mise en oeuvre de la procédure de détermination de l'Etat responsable de l'examen de la demande d'asile, doivent être jugés selon les règles de droit commun applicables devant les tribunaux administratifs, à la différence des recours dirigés contre les mesures d'assignation à résidence prises après l'intervention des décisions de transfert, sur le fondement de l'article L. 742-4 de ce code, qui doivent être jugés selon la procédure particulière prévue au III de l'article L. 512-1 du même code [RJ1].</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., sur l'application de la procédure prévue au III de l'article L. 512-1 du CESEDA au recours dirigé contre une mesure d'assignation à résidence prise en vue de l'exécution d'une décision de transfert, CE, juge des référés, 4 mars 2015, M.,, n° 388180, p. 79 ; CE, juge des référés, 8 novembre 2017, M.,, n° 415178, aux Tables sur un autre point.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
