<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038815766</ID>
<ANCIEN_ID>JG_L_2019_07_000000409340</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/81/57/CETATEXT000038815766.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 6ème chambres réunies, 24/07/2019, 409340</TITRE>
<DATE_DEC>2019-07-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>409340</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Jean-Dominique Langlais</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2019:409340.20190724</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une décision du 4 avril 2018, le Conseil d'Etat, statuant au contentieux sur requête du Syndicat des cadres de la sécurité intérieure tendant à l'annulation pour excès de pouvoir de l'article 1er du décret n° 2017-109 du 30 janvier 2017 modifiant le décret n° 2002-1279 du 23 octobre 2002 portant dérogations aux garanties minimales de durée de travail et de repos applicables aux personnels de la police nationale, a sursis à statuer jusqu'à ce que la Cour de justice de l'Union européenne se soit prononcée sur les questions suivantes :<br/>
<br/>
              1°) Les dispositions des articles 6 et 16 de la directive 2003/88/CE du 4 novembre 2003 concernant certains aspects de l'aménagement du temps de travail doivent-elles être interprétées comme imposant une période de référence définie de manière glissante ou comme laissant aux Etats membres le choix de lui conférer un caractère glissant ou fixe '<br/>
<br/>
              2°) Dans l'hypothèse où ces dispositions devraient être interprétées comme imposant une période de référence glissante, la possibilité ouverte par l'article 17 de déroger au b de l'article 16 est-elle susceptible de concerner, non seulement la durée de la période de référence, mais aussi son caractère glissant '<br/>
<br/>
              Par un arrêt n° C-254/18 du 11 avril 2019, la Cour de justice de l'Union européenne s'est prononcée sur ces questions.<br/>
<br/>
              Par un mémoire en réponse à un supplément d'instruction, enregistré le 11 juin 2019, le Syndicat des cadres de la sécurité intérieure maintient les conclusions de sa requête. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier, y compris celles visées par la décision du Conseil d'Etat, statuant au contentieux du 4 avril 2018 ;<br/>
<br/>
              Vu : <br/>
              - le traité sur le fonctionnement de l'Union européenne ;<br/>
              - la directive 2003/88/CE du Parlement européen et du Conseil du 4 novembre 2003 ;<br/>
              - le décret n° 2000-815 du 25 août 2000 ;<br/>
              - le décret n° 2002-1279 du 23 octobre 2002 ;<br/>
              - l'arrêt n° C-254/18 du 11 avril 2019 de la Cour de justice de l'Union européenne ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Langlais, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
              Vu la note en délibéré, enregistrée le 16 juillet 2019, présentée par le ministre de l'intérieur ; <br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du I de l'article 3 du décret du 25 août 2000 relatif à l'aménagement et à la réduction du temps de travail dans la fonction publique de l'Etat et dans la magistrature : " I.- L'organisation du travail doit respecter les garanties minimales ci-après définies. / La durée hebdomadaire du travail effectif, heures supplémentaires comprises, ne peut excéder ni quarante-huit heures au cours d'une même semaine, ni quarante-quatre heures en moyenne sur une période quelconque de douze semaines consécutives (...). / II.- Il ne peut être dérogé aux règles énoncées au I que dans les cas et conditions ci-après : / a) Lorsque l'objet même du service public en cause l'exige en permanence, notamment pour la protection des personnes et des biens, par décret en Conseil d'Etat, pris après avis du comité d'hygiène et de sécurité le cas échéant, du comité technique ministériel et du Conseil supérieur de la fonction publique, qui détermine les contreparties accordées aux catégories d'agents concernés (...) ". Aux termes du premier alinéa de l'article 1er du décret du 23 octobre 2002 portant dérogations aux garanties minimales de durée de travail et de repos applicables aux personnels de la police nationale : " Pour l'organisation du travail des fonctionnaires actifs des services de la police nationale, il est dérogé aux garanties minimales mentionnées au I de l'article 3 du décret du 25 août 2000 susvisé, lorsque les tâches de sécurité et de paix publiques, de police judiciaire et de renseignement et d'information, qui leur sont confiées, l'exigent ". L'article 1er du décret attaqué, dont le syndicat requérant demande l'annulation pour excès de pouvoir, ajoute à l'article 1er du décret du 23 octobre 2002 les dispositions suivantes : " Cette dérogation doit toutefois respecter les conditions suivantes : / 1° La durée hebdomadaire de travail mesurée, pour chaque période de sept jours, heures supplémentaires comprises, ne peut excéder quarante-huit heures en moyenne sur une période d'un semestre de l'année civile. (...) ".<br/>
<br/>
              2. Aux termes de l'article 6 de la directive 2003/88/CE du 4 novembre 2003 concernant certains aspects de l'aménagement du temps de travail : " Les Etats membres prennent les mesures nécessaires pour que, en fonction des impératifs de protection de la sécurité et de la santé des travailleurs :/ (...) b) la durée moyenne de travail pour chaque période de sept jours n'excède pas quarante-huit heures, y compris les heures supplémentaires ". Aux termes de son article 16 : " Les États membres peuvent prévoir :/ (...) b) pour l'application de l'article 6 (durée maximale hebdomadaire de travail), une période de référence ne dépassant pas quatre mois... ". Aux termes du c du paragraphe 3 de l'article 17 de la même directive, il peut être dérogé à l'article 16 " pour les activités de garde, de surveillance et de permanence caractérisées par la nécessité d'assurer la protection des biens et des personnes " et " pour les activités caractérisées par la nécessité d'assurer la continuité du service ". Enfin, aux termes de l'article 19 de cette directive : " La faculté de déroger à l'article 16, point b), prévue à l'article 17, paragraphe 3 (...) ne peut avoir pour effet l'établissement d'une période de référence dépassant six mois ".<br/>
<br/>
              3. Le syndicat requérant soutient que le décret attaqué méconnaît les dispositions combinées des articles 6 et 16 de la directive cités au point précédent en retenant, pour le calcul de la durée hebdomadaire moyenne de travail des fonctionnaires actifs de la police nationale, une période de référence fixe correspondant à un semestre de l'année civile.<br/>
<br/>
              4. Dans l'arrêt du 11 avril 2019 par lequel elle s'est prononcée sur les questions dont le Conseil d'Etat, statuant au contentieux l'avait saisie à titre préjudiciel, la Cour de justice de l'Union européenne a dit pour droit que l'article 6, sous b), l'article 16, sous b), et l'article 19, premier alinéa, de la directive 2003/88/CE précitée doivent être interprétés en ce sens qu'ils ne s'opposent pas à une réglementation nationale qui prévoit, aux fins du calcul de la durée moyenne hebdomadaire de travail, des périodes de référence qui commencent et se terminent à des dates calendaires fixes, pourvu que cette règlementation comporte des mécanismes permettant d'assurer que la durée moyenne maximale hebdomadaire de travail de quarante-huit heures est respectée au cours de chaque période de six mois à cheval sur deux périodes de référence fixes successives.<br/>
<br/>
              5. Il résulte de l'arrêt de la cour que, dès lors que les autorités françaises ont fixé la durée moyenne maximale de travail des fonctionnaires actifs de la police nationale au plafond de 48 heures hebdomadaires prévu par l'article 6 de la directive et étendu à six mois la période de référence utilisée pour le calcul de cette moyenne, en application de ses articles 17 et 19, seule l'utilisation de périodes de référence glissantes permet de garantir que la durée moyenne maximale hebdomadaire de travail de 48 heures est respectée au cours de toute période de six mois. Il suit de là que les dispositions de l'article 1er du décret attaqué doivent être annulées en tant qu'elles prévoient que la période de référence de six mois qu'elles définissent est une période fixe coïncidant avec un semestre de l'année civile.<br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 3 000 euros à verser au Syndicat des cadres de la sécurité intérieure au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Au troisième alinéa (1°) de l'article 1er du décret du 23 octobre 2002, tel qu'il résulte de l'article 1er du 30 janvier 2017, les mots " de l'année civile " sont annulés.<br/>
Article 2 : L'Etat versera au Syndicat des cadres de la sécurité intérieure la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée au Syndicat des cadres de la sécurité intérieure, au Premier ministre, au ministre de l'intérieur, au ministre de l'action et des comptes publics et au président de la Cour de justice de l'Union européenne.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">15-05-085 COMMUNAUTÉS EUROPÉENNES ET UNION EUROPÉENNE. RÈGLES APPLICABLES. - ENCADREMENT PAR LA DIRECTIVE 2003/88/CE DE LA DURÉE HEBDOMADAIRE DU TRAVAIL - CALCUL POUR LES FONCTIONNAIRES DE LA POLICE NATIONALE DE LA DURÉE MOYENNE HEBDOMADAIRE REPOSANT SUR DES PÉRIODES DE RÉFÉRENCE FIXES - CONTRARIÉTÉ AVEC LA DIRECTIVE - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">36-07-02-002 FONCTIONNAIRES ET AGENTS PUBLICS. STATUTS, DROITS, OBLIGATIONS ET GARANTIES. STATUTS SPÉCIAUX. PERSONNELS DE POLICE (VOIR : POLICE ADMINISTRATIVE). - FONCTIONNAIRES DE LA POLICE NATIONALE - ENCADREMENT PAR LA DIRECTIVE 2003/88/CE DE LA DURÉE HEBDOMADAIRE DU TRAVAIL - CALCUL DE LA DURÉE MOYENNE HEBDOMADAIRE REPOSANT SUR DES PÉRIODES DE RÉFÉRENCE FIXES - CONTRARIÉTÉ AVEC LA DIRECTIVE - EXISTENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">49-025 POLICE. PERSONNELS DE POLICE. - FONCTIONNAIRES DE LA POLICE NATIONALE - ENCADREMENT PAR LA DIRECTIVE 2003/88/CE DE LA DURÉE HEBDOMADAIRE DU TRAVAIL - CALCUL DE LA DURÉE MOYENNE HEBDOMADAIRE REPOSANT SUR DES PÉRIODES DE RÉFÉRENCE FIXES - CONTRARIÉTÉ AVEC LA DIRECTIVE - EXISTENCE.
</SCT>
<ANA ID="9A"> 15-05-085 Arrêt n° C-254/18 du 11 avril 2019 de la Cour de justice de l'Union européenne (CJUE) disant pour droit que l'article 6, sous b), l'article 16, sous b), et l'article 19, premier alinéa, de la directive 2003/88/CE du 4 novembre 2003 doivent être interprétés en ce sens qu'ils ne s'opposent pas à une réglementation nationale qui prévoit, aux fins du calcul de la durée moyenne hebdomadaire de travail, des périodes de référence qui commencent et se terminent à des dates calendaires fixes, pourvu que cette règlementation comporte des mécanismes permettant d'assurer que la durée moyenne maximale hebdomadaire de travail de quarante-huit heures est respectée au cours de chaque période de six mois à cheval sur deux périodes de référence fixes successives.... ,,Il résulte de l'arrêt de la cour que, dès lors que les autorités françaises ont fixé la durée moyenne maximale de travail des fonctionnaires actifs de la police nationale au plafond de 48 heures hebdomadaires prévu par l'article 6 de la directive et étendu à six mois la période de référence utilisée pour le calcul de cette moyenne, en application de ses articles 17 et 19, seule l'utilisation de périodes de références glissantes permet de garantir que la durée moyenne maximale hebdomadaire de travail de 48 heures est respectée au cours de toute période de six mois. Il suit de là que les dispositions de l'article 1er du décret n° 2017-109 du 30 janvier 2017 attaqué doivent être annulées en tant qu'elles prévoient que la période de référence de six mois qu'elles définissent est une période fixe coïncidant avec un semestre de l'année civile.</ANA>
<ANA ID="9B"> 36-07-02-002 Arrêt n° C-254/18 du 11 avril 2019 de la Cour de justice de l'Union européenne (CJUE) disant pour droit que l'article 6, sous b), l'article 16, sous b), et l'article 19, premier alinéa, de la directive 2003/88/CE du 4 novembre 2003 doivent être interprétés en ce sens qu'ils ne s'opposent pas à une réglementation nationale qui prévoit, aux fins du calcul de la durée moyenne hebdomadaire de travail, des périodes de référence qui commencent et se terminent à des dates calendaires fixes, pourvu que cette règlementation comporte des mécanismes permettant d'assurer que la durée moyenne maximale hebdomadaire de travail de quarante-huit heures est respectée au cours de chaque période de six mois à cheval sur deux périodes de référence fixes successives.... ,,Il résulte de l'arrêt de la cour que, dès lors que les autorités françaises ont fixé la durée moyenne maximale de travail des fonctionnaires actifs de la police nationale au plafond de 48 heures hebdomadaires prévu par l'article 6 de la directive et étendu à six mois la période de référence utilisée pour le calcul de cette moyenne, en application de ses articles 17 et 19, seule l'utilisation de périodes de références glissantes permet de garantir que la durée moyenne maximale hebdomadaire de travail de 48 heures est respectée au cours de toute période de six mois. Il suit de là que les dispositions de l'article 1er du décret n° 2017-109 du 30 janvier 2017 attaqué doivent être annulées en tant qu'elles prévoient que la période de référence de six mois qu'elles définissent est une période fixe coïncidant avec un semestre de l'année civile.</ANA>
<ANA ID="9C"> 49-025 Arrêt n° C-254/18 du 11 avril 2019 de la Cour de justice de l'Union européenne (CJUE) disant pour droit que l'article 6, sous b), l'article 16, sous b), et l'article 19, premier alinéa, de la directive 2003/88/CE du 4 novembre 2003 doivent être interprétés en ce sens qu'ils ne s'opposent pas à une réglementation nationale qui prévoit, aux fins du calcul de la durée moyenne hebdomadaire de travail, des périodes de référence qui commencent et se terminent à des dates calendaires fixes, pourvu que cette règlementation comporte des mécanismes permettant d'assurer que la durée moyenne maximale hebdomadaire de travail de quarante-huit heures est respectée au cours de chaque période de six mois à cheval sur deux périodes de référence fixes successives.... ,,Il résulte de l'arrêt de la cour que, dès lors que les autorités françaises ont fixé la durée moyenne maximale de travail des fonctionnaires actifs de la police nationale au plafond de 48 heures hebdomadaires prévu par l'article 6 de la directive et étendu à six mois la période de référence utilisée pour le calcul de cette moyenne, en application de ses articles 17 et 19, seule l'utilisation de périodes de références glissantes permet de garantir que la durée moyenne maximale hebdomadaire de travail de 48 heures est respectée au cours de toute période de six mois. Il suit de là que les dispositions de l'article 1er du décret n° 2017-109 du 30 janvier 2017 attaqué doivent être annulées en tant qu'elles prévoient que la période de référence de six mois qu'elles définissent est une période fixe coïncidant avec un semestre de l'année civile.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
