<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025386932</ID>
<ANCIEN_ID>JG_L_2012_02_000000351617</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/38/69/CETATEXT000025386932.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 13/02/2012, 351617, Publié au recueil Lebon</TITRE>
<DATE_DEC>2012-02-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>351617</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO ; SCP DELAPORTE, BRIARD, TRICHET</AVOCATS>
<RAPPORTEUR>M. Gaël Raimbault</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Maud Vialettes</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:351617.20120213</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 5 et 19 août 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour l'ASSOCIATION SOCIETE PROTECTRICE DES ANIMAUX DE VANNES, dont le siège est zone industrielle du Prat, avenue Edouard-Michelin à Vannes (56000), représentée par son président ; l'association demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 1102410 du 21 juillet 2011 par laquelle le juge des référés du tribunal administratif de Rennes, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, a rejeté sa demande tendant à la suspension de l'exécution de l'arrêté du 7 mars 2011 par lequel le maire de Theix (Morbihan) a retiré le permis de construire tacite qui lui avait été accordé le 8 décembre 2010 pour un projet de refuge pour animaux domestiques ;<br/>
<br/>
              2°) statuant en référé, de faire droit à sa demande de suspension ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Theix le versement de la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
              Vu le code de l'urbanisme, modifié notamment par la loi n° 2006-872 du 13 juillet 2006 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Gaël Raimbault, chargé des fonctions de Maître des requêtes,  <br/>
<br/>
              - les observations de la SCP Rocheteau, Uzan-Sarano, avocat de l'ASSOCIATION SOCIETE PROTECTRICE DES ANIMAUX DE VANNES et de la SCP Delaporte, Briard, Trichet, avocat de la commune de Theix, <br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Rocheteau, Uzan-Sarano, avocat de l'ASSOCIATION SOCIETE PROTECTRICE DES ANIMAUX DE VANNES et à la SCP Delaporte, Briard, Trichet, avocat de la commune de Theix ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant, d'une part, qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie, et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ;<br/>
<br/>
              Considérant, d'autre part, qu'aux termes du second alinéa de l'article L. 424-5 du code de l'urbanisme : " Le permis de construire, d'aménager ou de démolir, tacite ou explicite, ne peut être retiré que s'il est illégal et dans le délai de trois mois suivant la date de cette décision " ; que, compte tenu de l'objectif de sécurité juridique poursuivi par le législateur, qui ressort des travaux préparatoires de la loi du 13 juillet 2006 dont ces dispositions sont issues, l'autorité compétente ne peut rapporter un permis de construire, d'aménager ou de démolir, tacite ou explicite, que si la décision de retrait est notifiée au bénéficiaire du permis avant l'expiration du délai de trois mois suivant la date à laquelle ce permis a été accordé ; <br/>
<br/>
              Considérant qu'à l'appui de sa demande de suspension de l'exécution de l'arrêté signé le 7 mars 2011 par lequel le maire de Theix a retiré le permis de construire tacite qui lui avait été accordé, l'association requérante faisait valoir que ce retrait était illégal, faute de lui avoir été notifié avant l'expiration du délai fixé par l'article L. 424-5 du code de l'urbanisme ; que, pour juger que ce moyen n'était pas de nature à faire naître un doute sérieux quant à la légalité de l'arrêté attaqué, le juge des référés du tribunal administratif de Rennes a relevé que la signature de cet arrêté était antérieure à l'expiration de ce délai et que la date de sa notification était sans incidence sur sa légalité ; qu'il résulte de ce qui vient d'être dit qu'il a, ce faisant, commis une erreur de droit ; que, par suite, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, l'association est fondée à demander l'annulation de l'ordonnance attaquée ;<br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au titre de la procédure de référé engagée ;<br/>
<br/>
              Considérant que, pour justifier l'urgence qui s'attacherait à la suspension de la décision de retrait du permis de construire qui lui avait été accordé, l'ASSOCIATION SOCIETE PROTECTRICE DES ANIMAUX DE VANNES se borne à invoquer le caractère inadapté et insuffisant de ses installations actuelles ; que ces éléments ne permettent pas de caractériser une situation d'urgence ; que, par suite, cette association n'est pas fondée à demander la suspension de l'exécution de cette décision ;<br/>
<br/>
              Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'il soit fait droit aux conclusions présentées sur leur fondement par l'association requérante ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par la commune de Theix au même titre ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance n° 1102410 du 21 juillet 2011 du juge des référés du tribunal administratif de Rennes est annulée.<br/>
Article 2 : La demande présentée par l'ASSOCIATION SOCIETE PROTECTRICE DES ANIMAUX DE VANNES au juge des référés du tribunal administratif de Rennes, ainsi que les conclusions présentées par les parties au titre de l'article L. 761-1 du code de justice administrative, sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à l'ASSOCIATION SOCIETE PROTECTRICE DES ANIMAUX DE VANNES et à la commune de Theix.<br/>
Copie en sera adressée pour information à la ministre de l'écologie, du développement durable, des transports et du logement.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-09-01 ACTES LÉGISLATIFS ET ADMINISTRATIFS. DISPARITION DE L'ACTE. RETRAIT. - RETRAIT D'UN PERMIS DE CONSTRUIRE, D'AMÉNAGER OU DE DÉMOLIR - DÉLAI DE TROIS MOIS PRÉVU PAR L'ARTICLE L. 424-5 DU CODE DE L'URBANISME - RETRAIT DEVANT ÊTRE NOTIFIÉ DANS LE DÉLAI DE TROIS MOIS [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">68 ACTES LÉGISLATIFS ET ADMINISTRATIFS. DISPARITION DE L'ACTE. RETRAIT. - RETRAIT D'UN PERMIS DE CONSTRUIRE, D'AMÉNAGER OU DE DÉMOLIR - DÉLAI DE TROIS MOIS PRÉVU PAR L'ARTICLE L. 424-5 DU CODE DE L'URBANISME - RETRAIT DEVANT ÊTRE NOTIFIÉ DANS LE DÉLAI DE TROIS MOIS [RJ1].
</SCT>
<ANA ID="9A"> 01-09-01 Aux termes du second alinéa de l'article L. 424-5 du code de l'urbanisme : « Le permis de construire, d'aménager ou de démolir, tacite ou explicite, ne peut être retiré que s'il est illégal et dans le délai de trois mois suivant la date de cette décision ».,,Compte tenu de l'objectif de sécurité juridique poursuivi par le législateur, qui ressort des travaux préparatoires de la loi n° 2006-872 du 13 juillet 2006 dont ces dispositions sont issues, l'autorité compétente ne peut rapporter un permis de construire, d'aménager ou de démolir, tacite ou explicite, que si la décision de retrait est notifiée au bénéficiaire du permis avant l'expiration du délai de trois mois suivant la date à laquelle ce permis a été accordé.</ANA>
<ANA ID="9B"> 68 Aux termes du second alinéa de l'article L. 424-5 du code de l'urbanisme : « Le permis de construire, d'aménager ou de démolir, tacite ou explicite, ne peut être retiré que s'il est illégal et dans le délai de trois mois suivant la date de cette décision ».,,Compte tenu de l'objectif de sécurité juridique poursuivi par le législateur, qui ressort des travaux préparatoires de la loi n° 2006-872 du 13 juillet 2006 dont ces dispositions sont issues, l'autorité compétente ne peut rapporter un permis de construire, d'aménager ou de démolir, tacite ou explicite, que si la décision de retrait est notifiée au bénéficiaire du permis avant l'expiration du délai de trois mois suivant la date à laquelle ce permis a été accordé.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., pour la jurisprudence antérieure à l'article L. 424-5 du code de l'urbanisme, CE, Assemblée, 1er juin 1973, Ministre de l'équipement et du logement contre Epoux Roulin, n° 85804, p. 390 ; 13 novembre 1981, Baumert, n° 24945, p. 412 ; 26 janvier 2005, Filippi, n° 260188, T. pp.725-1139. Comp., pour l'application du régime issu de la jurisprudence Ternon, CE, Section, 21 décembre 2007, Société Bretim,  n° 285515, p. 519.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
