<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031240699</ID>
<ANCIEN_ID>JG_L_2015_09_000000372624</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/24/06/CETATEXT000031240699.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Section, 25/09/2015, 372624, Publié au recueil Lebon</TITRE>
<DATE_DEC>2015-09-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>372624</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Section</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>M. Frédéric Dieu</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESEC:2015:372624.20150925</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante : <br/>
<br/>
              Mme B... a demandé au tribunal administratif de Paris, d'une part, d'annuler la décision du 23 août 2011 par laquelle le directeur régional adjoint des entreprises, de la concurrence, de la consommation, du travail et de l'emploi, responsable de l'unité territoriale de Paris, a décidé son changement d'affectation et, d'autre part, d'enjoindre au ministre du travail, de l'emploi et de la santé, de la réintégrer dans son ancienne affectation, dans un délai de quinze jours, sous astreinte de 500 euros par jour de retard.<br/>
              Par un jugement n° 1116493/5-2 du 28 mars 2013, le tribunal administratif de Paris a rejeté sa demande.<br/>
<br/>
              Mme B... a contesté ce jugement devant la cour administrative d'appel de Paris, dont le président, par une ordonnance n° 13PA02006 du 27 septembre 2013, a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, le pourvoi présenté le 27 mai 2013 à cette cour par Mme B....<br/>
<br/>
              Par ce pourvoi et deux mémoires complémentaires, enregistrés les 28 février et 12 mars 2014 au secrétariat du contentieux du Conseil d'Etat, Mme B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement du 28 mars 2013 du tribunal administratif de Paris ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande de première instance ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement de la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ainsi que le remboursement de la somme de 35 euros qu'elle a acquittée au titre de l'article R. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Dieu, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de Mme B... ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 21 septembre 2015, présentée par Mme B... ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes du dernier alinéa de l'article R. 741-2 du code de justice administrative : " La décision fait apparaître la date de l'audience et la date à laquelle elle a été prononcée " ; que le jugement attaqué, qui comporte une erreur en ce qui concerne la date de lecture et mentionne trois dates d'audience différentes, méconnaît ces exigences ; que, par suite et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, il est irrégulier et doit être annulé ;<br/>
<br/>
              2. Considérant qu'il y a lieu de régler l'affaire au fond par application de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              3. Considérant que les mesures prises à l'égard d'agents publics qui, compte tenu de leurs effets, ne peuvent être regardées comme leur faisant grief, constituent de simples mesures d'ordre intérieur insusceptibles de recours ; qu'il en va ainsi des mesures qui, tout en modifiant leur affectation ou les tâches qu'ils ont à accomplir, ne portent pas atteinte aux droits et prérogatives qu'ils tiennent de leur statut ou à l'exercice de leurs droits et libertés fondamentaux, ni n'emportent perte de responsabilités ou de rémunération ; que le recours contre de telles mesures, à moins qu'elles ne traduisent une discrimination, est irrecevable ; <br/>
<br/>
              4. Considérant qu'il ressort des pièces du dossier que Mme B..., contrôleur du travail en fonction à la section d'inspection du travail C... de la direction régionale des entreprises, de la concurrence, de la consommation, du travail et de l'emploi d'Ile de France, a été affectée, par la décision contestée du 23 août 2013, à la section ... de cette direction ; que la mesure a été prise, dans l'intérêt du service, en vue de mettre fin à des difficultés relationnelles entre Mme B... et plusieurs de ses collègues ;<br/>
<br/>
              5. Considérant, en premier lieu, que ce changement d'affectation, qui ne présente pas le caractère d'une sanction disciplinaire déguisée et dont il n'est ni démontré ni même soutenu qu'il traduirait une discrimination, n'a entraîné pour Mme B... ni diminution de ses responsabilités ni perte de rémunération ; qu'en second lieu, il est intervenu au sein de la même commune et sans que soit porté atteinte aux droits statutaires ou aux droits et libertés fondamentaux de la requérante ; que, par suite, et alors même que cette mesure de changement d'affectation a été prise pour des motifs tenant au comportement de celle-ci, elle présente le caractère d'une mesure d'ordre intérieur, qui ne fait pas grief et n'est donc pas susceptible de faire l'objet d'un recours pour excès de pouvoir ; que, dès lors, la demande de Mme B... est irrecevable et doit être rejetée, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ; qu'il y a lieu enfin de laisser à la charge de cette dernière la contribution pour l'aide juridique qu'elle a acquittée au titre de l'article R. 761-1 du code de justice administrative dans sa rédaction applicable à la présente affaire ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Paris du 28 mars 2013 est annulé.<br/>
Article 2 : La demande présentée par Mme B... devant le tribunal administratif de Paris ainsi que ses conclusions présentées au titre des articles L. 761-1 et R 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à Mme A... B...et au ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-13-01-02-01 FONCTIONNAIRES ET AGENTS PUBLICS. CONTENTIEUX DE LA FONCTION PUBLIQUE. CONTENTIEUX DE L'ANNULATION. INTRODUCTION DE L'INSTANCE. DÉCISIONS SUSCEPTIBLES DE RECOURS. - ABSENCE (MESURE D'ORDRE INTÉRIEUR) - CHANGEMENT D'AFFECTATION OU DES TÂCHES D'UN AGENT PUBLIC - 1) INCLUSION -  CONDITIONS - 2) INCIDENCE D'UN MOTIF TENANT AU COMPORTEMENT DE L'AGENT - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-01-01-02-03 PROCÉDURE. INTRODUCTION DE L'INSTANCE. DÉCISIONS POUVANT OU NON FAIRE L'OBJET D'UN RECOURS. ACTES NE CONSTITUANT PAS DES DÉCISIONS SUSCEPTIBLES DE RECOURS. MESURES D'ORDRE INTÉRIEUR. - CHANGEMENT D'AFFECTATION OU DES TÂCHES D'UN AGENT PUBLIC - 1) INCLUSION [RJ1] - CONDITIONS - 2) INCIDENCE D'UN MOTIF TENANT AU COMPORTEMENT DE L'AGENT - ABSENCE.
</SCT>
<ANA ID="9A"> 36-13-01-02-01 Les mesures prises à l'égard d'agents publics qui, compte tenu de leurs effets, ne peuvent être regardées comme leur faisant grief, constituent de simples mesures d'ordre intérieur insusceptibles de recours. Il en va ainsi des mesures qui, tout en modifiant leur affectation ou les tâches qu'ils ont à accomplir, ne portent pas atteinte aux droits et prérogatives qu'ils tiennent de leur statut ou à l'exercice de leurs droits et libertés fondamentaux, ni n'emportent perte de responsabilités ou de rémunération.... ,,Le recours contre une telle mesure, à moins qu'elle ne traduise une discrimination [RJ2], est irrecevable, alors même que la mesure de changement d'affectation aurait été prise pour des motifs tenant au comportement de l'agent public concerné.</ANA>
<ANA ID="9B"> 54-01-01-02-03 Les mesures prises à l'égard d'agents publics qui, compte tenu de leurs effets, ne peuvent être regardées comme leur faisant grief, constituent de simples mesures d'ordre intérieur insusceptibles de recours. Il en va ainsi des mesures qui, tout en modifiant leur affectation ou les tâches qu'ils ont à accomplir, ne portent pas atteinte aux droits et prérogatives qu'ils tiennent de leur statut ou à l'exercice de leurs droits et libertés fondamentaux, ni n'emportent perte de responsabilités ou de rémunération.... ,,Le recours contre une telle mesure, à moins qu'elle ne traduise une discrimination [RJ2], est irrecevable, alors même que la mesure de changement d'affectation aurait été prise pour des motifs tenant au comportement de l'agent public concerné.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Cf. CE, 8 mars 1999, Mme Butler, n° 171341, T. pp. 843-936.,  ,[RJ2]Cf. CE, 15 avril 2015, Pôle emploi, n° 373893, p. 146.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
