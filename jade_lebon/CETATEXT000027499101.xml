<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027499101</ID>
<ANCIEN_ID>JG_L_2013_06_000000342673</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/49/91/CETATEXT000027499101.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère / 6ème SSR, 03/06/2013, 342673</TITRE>
<DATE_DEC>2013-06-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>342673</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère / 6ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GASCHIGNARD ; SCP CELICE, BLANCPAIN, SOLTNER</AVOCATS>
<RAPPORTEUR>Mme Chrystelle Naudan-Carastro</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Maud Vialettes</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2013:342673.20130603</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire, le mémoire complémentaire et le nouveau mémoire, enregistrés les 23 août 2010, 24 novembre 2010 et 31 janvier 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour la commune de Lamastre, représentée par son maire ; la commune de Lamastre demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 08LY00643 du 22 juin 2010 par lequel la cour administrative d'appel de Lyon a rejeté sa requête tendant à l'annulation du jugement n° 0507681 du 24 janvier 2008 en tant que, par ce jugement, le tribunal administratif de Lyon, à la demande de la SCI Tracoulon et de M. A...B..., a, d'une part, annulé l'arrêté du 3 juin 2005 par lequel son maire a refusé de délivrer un permis de construire à cette société ainsi que la décision implicite par laquelle il a rejeté le recours gracieux de ces derniers, d'autre part, enjoint à son maire de se prononcer à nouveau sur la demande de permis de construire ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de la SCI Tracoulon et de M. A...B...la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu la note en délibéré, enregistrée le 23 mai 2013, présentée pour la SCI Tracoulon et M. A...B... ;<br/>
              Vu la loi n° 2000-321 du 12 avril 2000 ;<br/>
<br/>
              Vu le code de l'urbanisme ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Chrystelle Naudan-Carastro, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gaschignard, avocat de la commune de Lamastre, et à la SCP Célice, Blancpain, Soltner, avocat de la société Tracoulon et de M. A...B... ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. A...B...a entrepris sans permis de construire divers travaux, destinés à l'aménagement de cinq gîtes dans deux bâtiments existants à usage d'habitation, situés au lieu-dit Tracoulon sur le territoire de la commune de Lamastre (Ardèche), accroissant leur surface et modifiant leurs façades par le percement de nouvelles ouvertures ; que, par un jugement du 25 mai 2005, le tribunal correctionnel de Privas a condamné M. A...B...à la démolition de l'ensemble des travaux réalisés irrégulièrement ; que, saisi par la SCI Tracoulon, ayant pour gérant M. A... B..., d'une demande de permis de construire de régularisation en vu de créer " deux logements saisonniers en sus de trois logements existants ", aux fins d'occupation personnelle, de changer l'affectation de certains locaux, de créer des surfaces nouvelles et de modifier les façades, le maire de la commune de Lamastre a estimé que cette demande visait à l'aménagement de gîtes locatifs saisonniers et l'a rejetée par arrêté du 3 juin 2005, alors que le jugement du 25 mai 2005 du tribunal correctionnel de Privas n'était pas devenu définitif, au motif que le règlement du plan d'occupation des sols n'autorisait pas une telle destination et que le système d'assainissement prévu était de nature à porter atteinte à la salubrité publique ; que la commune de Lamastre se pourvoit en cassation contre l'arrêt de la cour administrative d'appel de Lyon du 22 juin 2010 confirmant le jugement du 24 janvier 2008 par lequel le tribunal administratif de Lyon a annulé cet arrêté et la décision implicite rejetant le recours gracieux de la société et de son gérant ; <br/>
<br/>
              2. Considérant qu'un permis de construire n'a d'autre objet que d'autoriser la construction d'immeubles conformes aux plans et indications fournis par le pétitionnaire ; que la circonstance que ces plans et indications pourraient ne pas être respectés ou que ces immeubles risqueraient d'être ultérieurement transformés ou affectés à un usage non conforme aux documents et aux règles générales d'urbanisme n'est pas, par elle-même, sauf le cas d'éléments établissant l'existence d'une fraude à la date de la délivrance du permis, de nature à affecter la légalité de celui-ci ;<br/>
<br/>
              3. Considérant toutefois qu'il ressort des pièces du dossier soumis aux juges du fond qu'une visite sur les lieux menée par la sous-préfète de Tournon-sur-Rhône, le 27 août 2004, avait permis de constater que les cinq logements construits sans autorisation dans les deux bâtiments en cause étaient utilisés comme des gîtes destinés à la location et qu'un sixième gîte avait été aménagé, portant la capacité d'accueil à 43 personnes ; qu'à la date du dépôt de sa demande de permis de construire par la SCI Tracoulon, le site Internet de cette dernière proposait ces gîtes à la location et des réservations avaient déjà été enregistrées pour l'été 2006 ; que si cette société soutenait qu'elle avait demandé à la société qui hébergeait ce site de le supprimer, elle n'apportait aucun élément à l'appui de ses allégations ; que, par suite, en estimant que la demande de permis de régularisation, portant sur la création de deux logements saisonniers et le réaménagement de trois logements existants en vue d'une occupation personnelle, n'était pas entachée de fraude, la cour administrative d'appel de Lyon a dénaturé les pièces du dossier ; que, par voie de conséquence et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, son arrêt doit être annulé ;<br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              5. Considérant que, pour annuler les décisions litigieuses, le tribunal administratif de Lyon a estimé, d'une part, que si le projet devait être regardé comme tendant à la réalisation de plusieurs gîtes, ceux-ci avaient la nature de constructions à usage d'habitation autorisées par le règlement du plan d'occupation des sols et, d'autre part, que le projet ne présentait pas de risque pour la salubrité publique ; qu'il a ainsi jugé qu'aucun des deux motifs invoqués par le maire de la commune de Lamastre n'était de nature à justifier légalement le refus du permis de construire sollicité ;<br/>
<br/>
              6. Considérant, en premier lieu, qu'il résulte de ce qui précède que le maire de la commune de Lamastre a pu légalement, pour se prononcer sur la demande de permis de construire qui lui était soumise, retenir que le projet ne consistait pas en une création de deux logements saisonniers et un réaménagement de trois logements existants, en vue d'une occupation personnelle, comme le mentionnait le dossier de demande, mais en la réalisation de gîtes destinés à la location ;<br/>
<br/>
              7. Considérant qu'en vertu de l'article ND 1 du règlement du plan d'occupation des sols de la commune de Lamastre, modifié en 2001 pour supprimer la mention des chambres d'hôtes, auberges rurales, gîtes ruraux, gîtes d'étape et gîtes équestres, sont autorisés en zone ND : " (...) l'aménagement et l'extension mesurée des constructions à vocation d'habitation existantes (...) " ; que des gîtes destinés à la location ne peuvent être regardés comme des habitations pour l'application de ces dispositions ; que, par suite, c'est à tort que le tribunal administratif de Lyon a jugé que les aménagements et extensions pour lesquels la SCI Tracoulon sollicitait un permis de construire devaient être regardés comme des constructions à usage d'habitation et qu'en conséquence, le maire de la commune de Lamastre n'avait pu légalement rejeter sa demande au motif qu'ils ne relevaient pas des dispositions de l'article ND 1 du règlement du plan d'occupation des sols ; <br/>
<br/>
              8. Considérant, en second lieu, qu'aux termes des dispositions de l'article R. 111-2 du code de l'urbanisme, dans sa rédaction applicable à la date des décisions litigieuses : " Le permis de construire peut être refusé ou n'être accordé que sous réserve de l'observation de prescriptions spéciales si les constructions, par leur situation ou leurs dimensions, sont de nature à porter atteinte à la salubrité ou à la sécurité publique. Il en est de même si les constructions projetées, par leur implantation à proximité d'autres installations, leurs caractéristiques ou leur situation, sont de nature à porter atteinte à la salubrité ou à la sécurité publique " ; <br/>
<br/>
              9. Considérant qu'il ressort des pièces du dossier qu'une étude réalisée par un premier bureau d'études en mars 2001, confirmée par un rapport d'un second bureau d'études en juin 2002, a conclu, sous réserve du respect de certaines prescriptions, à la faisabilité d'un dispositif d'assainissement autonome pour cinq habitations regroupées sur le terrain d'assiette du projet ; que l'ingénieur d'études sanitaires de la direction départementale des affaires sanitaires et sociales a émis le 6 avril 2005 un avis favorable sur le projet ; que si la commune de Lamastre fait valoir que le dispositif épurateur prévu par ce projet est situé à l'ouest des constructions, et non dans la partie est du terrain d'assiette du projet, comme le recommandait le second bureau d'études, il ne résulte pas de ce seul choix que l'assainissement autonome retenu présenterait des risques pour la salubrité publique ; que si la commune soutient également que le dossier qui lui a été soumis ne précise pas le mode d'évacuation des eaux usées du snack-bar présent sur le site, ce dernier, qui ne se situe pas dans l'un des deux bâtiments concernés par le projet, n'avait pas à être pris en compte pour l'attribution du permis de construire litigieux ; qu'il ne ressort pas des pièces du dossier que le système d'assainissement autonome qu'inclut le projet ne permettrait pas d'assurer un assainissement satisfaisant des constructions ; que, par suite, c'est à tort que le maire de la commune de Lamastre a estimé que le projet méconnaissait les dispositions de l'article R. 111-2 du code de l'urbanisme ;<br/>
<br/>
              10. Considérant toutefois qu'il résulte de l'instruction que le maire aurait pris les mêmes décisions s'il s'était fondé uniquement sur le premier motif de rejet de la demande de permis de construire, tiré de la méconnaissance des dispositions de l'article ND 1 du règlement du plan d'occupation des sols ; que, par suite, c'est à tort que le tribunal administratif s'est fondé sur l'illégalité des motifs du refus de permis de construire pour annuler les décisions du maire de la commune de Lamastre ; <br/>
<br/>
              11. Mais considérant qu'il appartient au Conseil d'Etat, saisi de l'ensemble du litige par l'effet dévolutif de l'appel, d'examiner les autres moyens soulevés par M. A...B...et la SCI Tracoulon devant le tribunal administratif et la cour administrative d'appel ;<br/>
<br/>
              Sur les moyens de légalité externe :<br/>
<br/>
              12. Considérant, en premier lieu, que si les dispositions de l'article L. 421-2-5 du code de l'urbanisme, alors applicable, prévoyaient que le conseil municipal de la commune désigne un autre de ses membres pour délivrer le permis de construire lorsque le maire est intéressé à sa délivrance, le maire de la commune de Lamastre ne pouvait être regardé comme intéressé, au sens de ces dispositions, à la délivrance du permis de construire sollicité ; qu'il ne ressort pas des pièces du dossier qu'il aurait méconnu l'obligation de neutralité qui s'imposait à lui ; que, par suite, M. A...B...et la SCI Tracoulon ne sont pas fondés à soutenir que le maire ne pouvait statuer lui-même sur leur demande sans entacher d'illégalité le refus qui leur a été opposé ;  <br/>
<br/>
              13. Considérant, en deuxième lieu, qu'aux termes du second alinéa de l'article 4 de la loi du 12 avril 2000 relative aux droits des citoyens dans leurs relations avec les administrations : " Toute décision prise par l'une des autorités administratives mentionnées à l'article 1er comporte, outre la signature de son auteur, la mention, en caractères lisibles, du prénom, du nom et de la qualité de celui-ci " ; <br/>
<br/>
              14. Considérant que, si l'arrêté du 3 juin 2005 ne comporte pas, en méconnaissance de ces dispositions, l'indication du prénom et du nom de son signataire, il ressort des pièces du dossier, notamment de la circonstance que le requérant avait été destinataire de plusieurs autres arrêtés du maire comportant ces indications, que le maire de la commune pouvait être identifié comme étant également l'autorité signataire de l'arrêté du 3 juin 2005 ; que, dès lors, la méconnaissance des dispositions de l'article 4 de la loi du 12 avril 2000 n'a pas, dans les circonstances de l'espèce, revêtu un caractère substantiel justifiant l'annulation de la décision attaquée ; <br/>
<br/>
              Sur l'exception d'illégalité du plan d'occupation des sols révisé le 29 novembre 2001 :<br/>
<br/>
              15. Considérant que l'article R. 123-11 du code de l'urbanisme, dans sa rédaction applicable à la procédure de mise à l'enquête publique de la révision du plan d'occupation des sols de la commune, prévoit que le plan d'occupation des sols rendu public est soumis par le maire à enquête publique, qu'un arrêté du maire précise notamment l'objet de l'enquête, sa date d'ouverture et sa durée, le nom du commissaire enquêteur, ainsi que les jours et heures, et les lieux où le public pourra prendre connaissance du dossier et formuler ses observations et que : " Un avis portant ces indications à la connaissance du public est, par les soins du maire, publié en caractères apparents dans deux journaux diffusés dans le département, quinze jours au moins avant le début de l'enquête et rappelé de même dans les huit premiers jours de celle-ci. Il est publié par voie d'affiches et éventuellement par tous autres procédés dans la ou les communes membres concernées. (...) " ; <br/>
<br/>
              16. Considérant que s'il appartient au maire de procéder à la publicité de l'ouverture de l'enquête publique dans les conditions fixées par les dispositions précitées, la méconnaissance de ces dispositions n'est de nature à vicier la procédure et donc à entraîner l'illégalité de la révision du plan d'occupation des sols approuvée à l'issue de l'enquête publique que si elle a pu avoir pour effet de nuire à l'information de l'ensemble des personnes intéressées ou si elle a été de nature à exercer une influence sur les résultats de l'enquête et, par suite, sur la décision de l'autorité administrative ;<br/>
<br/>
              17. Considérant que si la SCI Tracoulon et M. A...B...font valoir que l'avis d'ouverture de l'enquête publique n'a été affiché qu'en mairie, du 6 août au 5 octobre 2011, il ressort des pièces du dossier que cet avis a par ailleurs fait l'objet d'une publication dans deux journaux, " Le Dauphiné libéré " et " L'Echo-le Valentinois ", diffusés dans tout le département ; que, dans ces conditions, l'absence d'affichage multiple de cet avis n'a pas, dans les circonstances de l'espèce, et eu égard notamment à la taille de la commune de Lamastre, entaché d'irrégularité la procédure de révision de son plan d'occupation des sols ; <br/>
<br/>
              18. Considérant, en deuxième lieu, qu'il appartient aux auteurs d'un plan d'occupation des sols de déterminer le parti d'aménagement à retenir pour le territoire concerné par le plan, en tenant compte de la situation existante et des perspectives d'avenir, et de fixer en conséquence le zonage et les possibilités de construction ; qu'il ressort des pièces du dossier que les parcelles 542, 544, 545 et 546 sont comprises dans un ensemble boisé plus vaste, de sorte que leur classement, depuis 1983, en zone ND vise à préserver le paysage et le milieu naturel ; qu'en maintenant le classement de ces parcelles en zone ND, le conseil municipal n'a pas commis d'erreur manifeste d'appréciation ;<br/>
<br/>
              19. Considérant, en troisième lieu, que si la SCI Tracoulon et M. A...B...soutiennent que les auteurs du plan d'occupation des sols ont entaché ce dernier d'un détournement de pouvoir en supprimant à son article ND 1 la référence aux chambres d'hôtes, auberges rurales, gîtes ruraux, gîtes d'étape et gîtes équestres, le détournement de pouvoir allégué n'est pas établi ; que le moyen tiré de ce que la nouvelle rédaction de cet article méconnaîtrait le principe d'impartialité n'est pas assorti des précisions suffisantes pour permettre d'en apprécier le bien-fondé ;<br/>
<br/>
              20. Considérant, en quatrième lieu, que la décision litigieuse ne se fonde pas sur les dispositions de l'article ND 4 du plan d'occupation des sols de la commune, selon lesquelles : " Le système d'assainissement autonome adapté des eaux usées provenant des établissements autres que des maisons individuelles d'habitation devra faire l'objet d'une étude particulière aux frais du pétitionnaire " ; que, par suite, la SCI Tracoulon et M. A...B...ne peuvent utilement exciper de leur illégalité ;<br/>
<br/>
              Sur l'autre moyen de légalité interne :<br/>
<br/>
              21. Considérant que le détournement de pouvoir allégué à l'encontre du refus de permis de construire n'est pas établi ;<br/>
<br/>
              22. Considérant qu'il résulte de tout ce qui précède que la SCI Tracoulon et M. A... B...ne sont pas fondés à demander l'annulation de l'arrêté du 3 juin 2005 par lequel le maire de la commune de Lamastre a refusé de délivrer un permis de construire à la SCI Tracoulon ni de la décision implicite par laquelle il a rejeté leur recours gracieux ; que leurs conclusions à fin d'injonction ne peuvent, par suite, qu'être rejetées ;<br/>
<br/>
              23. Considérant que, dans les circonstances de l'espèce, il y a lieu de mettre à la charge de la SCI Tracoulon et M. A...B...le versement à la commune de Lamastre d'une somme de 3 000 euros chacun, au titre des différentes instances, en application de l'article L. 761-1 du code de justice administrative ; que ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de la commune de Lamastre ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Lyon du 22 juin 2010  est annulé.<br/>
Article 2 : Les articles 1er, 2 et 5 du jugement du tribunal administratif de Lyon du 24 janvier 2008 sont annulés.<br/>
Article 3 : Les conclusions de la demande présentée par la SCI Tracoulon et M. A...B...devant le tribunal administratif de Lyon tendant à ce que soient annulés l'arrêté du 3 juin 2005 et la décision implicite du maire de Lamastre rejetant le recours gracieux dirigé contre cet arrêté et à ce qu'il soit enjoint au maire de se prononcer à nouveau sur la demande de permis de construire sollicitée ainsi que leurs conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La SCI Tracoulon et M. A...B...verseront chacun une somme de 3 000 euros à la commune de Lamastre au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 5 : La présente décision sera notifiée à la commune de Lamastre, à la SCI Tracoulon et à M. C...A...B....<br/>
Copie en sera adressée à la ministre de l'égalité des territoires et du logement.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">68-03 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PERMIS DE CONSTRUIRE. - OBJET D'UN PERMIS DE CONSTRUIRE - AUTORISATION DE CONSTRUCTIONS CONFORMES AUX PLANS ET INDICATIONS FOURNIS - CONSÉQUENCES - CIRCONSTANCE QUE CES PLANS POURRAIENT NE PAS ÊTRE RESPECTÉS OU QUE LES IMMEUBLES RISQUENT D'ÊTRE ULTÉRIEUREMENT TRANSFORMÉS OU AFFECTÉS À UN USAGE NON CONFORME - INCIDENCE SUR LA LÉGALITÉ DU PERMIS - ABSENCE, EN PRINCIPE - EXCEPTION - FRAUDE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">68-03-03 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PERMIS DE CONSTRUIRE. LÉGALITÉ INTERNE DU PERMIS DE CONSTRUIRE. - CAS D'UN PERMIS DE RÉGULARISATION DEMANDÉ ALORS QUE LA DÉMOLITION A ÉTÉ ORDONNÉE PAR UNE DÉCISION DE JUSTICE N'ÉTANT PAS DEVENUE DÉFINITIVE [RJ1] - OBJET D'UN PERMIS DE CONSTRUIRE - AUTORISATION DE CONSTRUCTIONS CONFORMES AUX PLANS ET INDICATIONS FOURNIS - CONSÉQUENCES - CIRCONSTANCE QUE CES PLANS POURRAIENT NE PAS ÊTRE RESPECTÉS OU QUE LES IMMEUBLES RISQUENT D'ÊTRE ULTÉRIEUREMENT TRANSFORMÉS OU AFFECTÉS À UN USAGE NON CONFORME - INCIDENCE SUR LA LÉGALITÉ DU PERMIS - ABSENCE, EN PRINCIPE [RJ2] - EXCEPTION - FRAUDE.
</SCT>
<ANA ID="9A"> 68-03 Un permis de construire n'a d'autre objet que d'autoriser la construction d'immeubles conformes aux plans et indications fournis par le pétitionnaire. La circonstance que ces plans et indications pourraient ne pas être respectés ou que ces immeubles risqueraient d'être ultérieurement transformés ou affectés à un usage non conforme aux documents et aux règles générales d'urbanisme n'est pas, par elle-même, sauf le cas d'éléments établissant l'existence d'une fraude à la date de la délivrance du permis, de nature à affecter la légalité de celui-ci.</ANA>
<ANA ID="9B"> 68-03-03 Hypothèse d'une demande de permis de construire visant à régulariser l'édification antérieurement opérée d'un ouvrage dont la démolition a été ordonnée par une décision de justice n'étant pas devenue définitive. Un permis de construire n'a d'autre objet que d'autoriser la construction d'immeubles conformes aux plans et indications fournis par le pétitionnaire. La circonstance que ces plans et indications pourraient ne pas être respectés ou que ces immeubles risqueraient d'être ultérieurement transformés ou affectés à un usage non conforme aux documents et aux règles générales d'urbanisme n'est pas, par elle-même, sauf le cas d'éléments établissant l'existence d'une fraude à la date de la délivrance du permis, de nature à affecter la légalité de celui-ci.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Comp., dans le cas d'une décision de justice devenue définitive, CE, 8 juillet 1996, Piccinini, n°123437, p. 271.,,[RJ2] Cf. CE, Section, 18 mars 1983, Mme Siefert, n° 35255, p. 130.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
