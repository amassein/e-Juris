<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033551448</ID>
<ANCIEN_ID>JG_L_2016_12_000000388979</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/55/14/CETATEXT000033551448.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Section, 02/12/2016, 388979, Publié au recueil Lebon</TITRE>
<DATE_DEC>2016-12-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>388979</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Section</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>Mme Isabelle Lemesle</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESEC:2016:388979.20161202</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Nantes d'annuler pour excès de pouvoir la décision du 21 août 2013 par laquelle le maire du Mans (Sarthe) a refusé de lui communiquer la liste électorale de la commune et d'enjoindre à la commune du Mans de procéder à cette communication dans les quinze jours suivant la notification du jugement, sous astreinte de 100 euros par jour de retard. Par un jugement n° 1309977 du 22 octobre 2014, le tribunal administratif de Nantes a rejeté cette demande.<br/>
<br/>
              Par une ordonnance n° 15NT00765 du 16 mars 2015, enregistrée le 27 mars 2015 au secrétariat du contentieux du Conseil d'Etat, le président de la cour administrative d'appel de Nantes a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, le pourvoi, enregistré le 27 février 2015 au greffe de cette cour, présenté par M. B...A.... Par ce pourvoi et par deux nouveaux mémoires, enregistrés le 18 août 2015 et le 1er avril 2016 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de la commune du Mans la somme de 3 000 euros à verser à la SCP Piwnica-Molinié, avocat de M.A..., au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la loi organique n°2016-1046 du 1er août 2016 ;<br/>
              - la loi organique n° 2016-1047 du 1er août 2016 ;<br/>
              - le code électoral, notamment ses articles L. 28 et R. 16 ;<br/>
              - la loi n° 91-647 du 10 juillet 1991, notamment son article 37 ;<br/>
              - la loi n°2016-1048 du 1er aout 2016 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Isabelle Lemesle, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat de M. A...;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 28 du code électoral, dans sa version issue de la loi n° 88-227 du 11 mars 1988 : " Les listes électorales sont réunies en un registre et conservées dans les archives de la commune. / Tout électeur, tout candidat et tout parti ou groupement politique peut prendre communication et copie de la liste électorale ". Aux termes du dernier alinéa de l'article R. 16 du même code : " Tout électeur peut prendre communication et copie de la liste électorale et des tableaux rectificatifs à la mairie, ou à la préfecture pour l'ensemble des communes du département à la condition de s'engager à ne pas en faire un usage purement commercial ".<br/>
<br/>
              2. Ces dispositions, qui ont pour objet de concourir à la libre expression du suffrage, ouvrent au profit de tout électeur, régulièrement inscrit sur une liste électorale, le droit de prendre communication et copie de la liste électorale d'une commune. La demande doit être adressée à la mairie. Si elle porte sur plusieurs communes d'un département, elle peut l'être à la préfecture de ce département. Afin d'éviter toute exploitation commerciale des données personnelles que comporte une liste électorale, sur laquelle figurent le nom, la date et le lieu de naissance, l'adresse du domicile ou du lieu de résidence des personnes inscrites, ainsi que la nationalité s'agissant des électeurs ressortissants d'un Etat membre de l'Union européenne autre que la France, le pouvoir réglementaire a subordonné l'exercice du droit d'accès à l'engagement, de la part du demandeur, de ne pas en faire un usage commercial. S'il existe, au vu des éléments dont elle dispose et nonobstant l'engagement pris par le demandeur, des raisons sérieuses de penser que l'usage des listes électorales risque de revêtir, en tout ou partie, un caractère commercial, l'autorité compétente peut rejeter la demande de communication de la ou des listes électorales dont elle est saisie. Il lui est loisible de solliciter du demandeur qu'il produise tout élément d'information de nature à lui permettre de s'assurer de la sincérité de son engagement de ne faire de la liste électorale qu'un usage conforme aux dispositions des articles L. 28 et R. 16 du code électoral. L'absence de réponse à une telle demande peut être prise en compte parmi d'autres éléments, par l'autorité compétente afin d'apprécier, sous le contrôle du juge, les suites qu'il convient de réserver à la demande dont elle est saisie.<br/>
<br/>
              3. En l'espèce, il ressort des pièces du dossier soumis aux juges du fond que le maire du Mans, au vu des éléments dont il disposait qui l'ont conduit à nourrir des raisons sérieuses de penser que M.A..., en dépit de l'engagement qu'il avait pris, risquait de faire un usage commercial des informations figurant sur la liste électorale de la commune, lui a demandé de lui fournir des précisions sur l'usage qu'il entendait effectivement réserver à ces informations. Celui-ci n'ayant pas donné suite à cette demande, le maire du Mans a, par une décision du 21 août 2013, refusé de lui donner communication de la liste électorale de la commune. Après avoir relevé que M. A...exerce, à Cholet où il est électeur, une activité de " conseil juridique au soutien des entreprises ", qu'il a déjà sollicité la communication de la liste électorale de la commune d'Angers et qu'il s'est abstenu de fournir toute explication sur les motifs de sa demande, le tribunal administratif a jugé de façon suffisamment motivée et sans commettre d'erreur de droit que, dans les circonstances de l'espèce, le maire du Mans a pu légalement estimer qu'il existait des raisons sérieuses de penser que l'usage des listes électorales par l'intéressé risquait, en dépit de l'engagement pris par celui-ci, de revêtir, au moins en partie, un caractère commercial et refuser, pour ce motif, de faire droit à sa demande de communication.<br/>
<br/>
              4. Il résulte de ce qui précède que M. A...n'est pas fondé à demander l'annulation du jugement qu'il attaque. Par suite, les dispositions de l'article L. 761-1 du code de justice administrative et celles de l'article 37 de la loi du 10 juillet 1991 font obstacle à ce qu'il soit fait droit aux conclusions présentées sur leur fondement par la SCP Piwnica-Molinié, avocat de M.A.... Les conclusions présentées par la commune du Mans au titre de l'article L. 761-1 du code de justice administrative, présentées sans le ministère d'un avocat au Conseil d'Etat, ne sont pas recevables et doivent, par suite, être rejetées.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le pourvoi de M. A...est rejeté.<br/>
Article 2 : Les conclusions présentées par la commune du Mans au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à M. B...A..., à la commune du Mans et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">26-06-03 DROITS CIVILS ET INDIVIDUELS. ACCÈS AUX DOCUMENTS ADMINISTRATIFS. DROIT D'ACCÈS ET DE VÉRIFICATION SUR UN FONDEMENT AUTRE QUE CELUI DES LOIS DU 17 JUILLET 1978 ET DU 6 JANVIER 1978. - DROIT D'ACCÈS AUX LISTES ÉLECTORALES (ART. L. 28 DU CODE ÉLECTORAL) - 1) CONSISTANCE - DROIT OUVERT À TOUT ÉLECTEUR POUR TOUTE LISTE ÉLECTORALE - 2) INTERDICTION D'EXPLOITATION COMMERCIALE - A) OBLIGATION POUR LE DEMANDEUR DE S'ENGAGER EN CE SENS - EXISTENCE - B) FACULTÉ DE SOLLICITER DU DEMANDEUR DES PRÉCISIONS SUR SES INTENTIONS - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">28-005 ÉLECTIONS ET RÉFÉRENDUM. DISPOSITIONS GÉNÉRALES APPLICABLES AUX ÉLECTIONS. - DROIT D'ACCÈS AUX LISTES ÉLECTORALES (ART. L. 28 DU CODE ÉLECTORAL) - 1) CONSISTANCE - DROIT OUVERT À TOUT ÉLECTEUR POUR TOUTE LISTE ÉLECTORALE - 2) INTERDICTION D'EXPLOITATION COMMERCIALE - A) OBLIGATION POUR LE DEMANDEUR DE S'ENGAGER EN CE SENS - EXISTENCE - B) FACULTÉ DE SOLLICITER DU DEMANDEUR DES PRÉCISIONS SUR SES INTENTIONS - EXISTENCE.
</SCT>
<ANA ID="9A"> 26-06-03 1) Les dispositions des articles L. 28 et R. 16 du code électoral, qui ont pour objet de concourir à la libre expression du suffrage, ouvrent au profit de tout électeur, régulièrement inscrit sur une liste électorale, le droit de prendre communication et copie de la liste électorale d'une commune. La demande doit être adressée à la mairie. Si elle porte sur plusieurs communes d'un département, elle peut l'être à la préfecture de ce département.... ,,2) a) Afin d'éviter toute exploitation commerciale des données personnelles que comporte une liste électorale, le pouvoir réglementaire a subordonné l'exercice du droit d'accès à l'engagement, de la part du demandeur, de ne pas en faire un usage commercial.... ,,b) S'il existe, au vu des éléments dont elle dispose et nonobstant l'engagement pris par le demandeur, des raisons sérieuses de penser que l'usage des listes électorales risque de revêtir, en tout ou partie, un caractère commercial, l'autorité compétente peut rejeter la demande de communication de la ou des listes électorales dont elle est saisie. Il lui est loisible de solliciter du demandeur qu'il produise tout élément d'information de nature à lui permettre de s'assurer de la sincérité de son engagement de ne faire de la liste électorale qu'un usage conforme aux dispositions des articles L. 28 et R. 16 du code électoral. L'absence de réponse à une telle demande peut être prise en compte parmi d'autres éléments, par l'autorité compétente afin d'apprécier, sous le contrôle du juge, les suites qu'il convient de réserver à la demande dont elle est saisie.</ANA>
<ANA ID="9B"> 28-005 1) Les dispositions des articles L. 28 et R. 16 du code électoral, qui ont pour objet de concourir à la libre expression du suffrage, ouvrent au profit de tout électeur, régulièrement inscrit sur une liste électorale, le droit de prendre communication et copie de la liste électorale d'une commune. La demande doit être adressée à la mairie. Si elle porte sur plusieurs communes d'un département, elle peut l'être à la préfecture de ce département.... ,,2) a) Afin d'éviter toute exploitation commerciale des données personnelles que comporte une liste électorale, le pouvoir réglementaire a subordonné l'exercice du droit d'accès à l'engagement, de la part du demandeur, de ne pas en faire un usage commercial.... ,,b) S'il existe, au vu des éléments dont elle dispose et nonobstant l'engagement pris par le demandeur, des raisons sérieuses de penser que l'usage des listes électorales risque de revêtir, en tout ou partie, un caractère commercial, l'autorité compétente peut rejeter la demande de communication de la ou des listes électorales dont elle est saisie. Il lui est loisible de solliciter du demandeur qu'il produise tout élément d'information de nature à lui permettre de s'assurer de la sincérité de son engagement de ne faire de la liste électorale qu'un usage conforme aux dispositions des articles L. 28 et R. 16 du code électoral. L'absence de réponse à une telle demande peut être prise en compte parmi d'autres éléments, par l'autorité compétente afin d'apprécier, sous le contrôle du juge, les suites qu'il convient de réserver à la demande dont elle est saisie.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
