<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036610546</ID>
<ANCIEN_ID>JG_L_2018_02_000000414845</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/61/05/CETATEXT000036610546.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 4ème chambres réunies, 09/02/2018, 414845</TITRE>
<DATE_DEC>2018-02-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>414845</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>LE PRADO ; SCP BARADUC, DUHAMEL, RAMEIX</AVOCATS>
<RAPPORTEUR>Mme Dorothée Pradines</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Charles Touboul</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:414845.20180209</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              La société Ysy Médical a demandé au juge des référés du tribunal administratif de Nîmes, sur le fondement de l'article L. 521-1 du code de justice administrative, de suspendre l'exécution de la décision du 1er mars 2017 par laquelle la directrice générale adjointe de l'Agence nationale de sécurité du médicament et des produits de santé a suspendu la mise sur le marché, la distribution, et l'exportation des dispositifs médicaux Ysy ER, Ysy EST, Ysy Evolution 2 et Ysy Evolution 4 qu'elle fabrique mis sur le marché après le 7 mars 2014 et ordonné le retrait de ces produits auprès des clients et la suspension de leur utilisation dans un délai de six mois, ainsi que de la décision du 22 juin 2017 rejetant son recours gracieux, jusqu'à ce qu'il soit statué au fond sur leur légalité. Par une ordonnance n° 1702661 du 21 septembre 2017, rectifiée par une ordonnance du 27 septembre 2017, le juge des référés du tribunal administratif de Nîmes a suspendu l'exécution de ces décisions en tant qu'elles ont pour effet le rappel et la suspension de l'utilisation des dispositifs médicaux en cause déjà mis sur le marché.<br/>
<br/>
              1° Sous le n° 414845, par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 4 et 19 octobre 2017 au secrétariat du contentieux du Conseil d'Etat, l'Agence nationale de sécurité du médicament et des produits de santé demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'ordonnance rectifiée du juge des référés du tribunal administratif de Nîmes ;<br/>
<br/>
              2°) de mettre à la charge de la société Ysy Médical la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              2° Sous le n° 415128, par une requête, enregistrée le 19 octobre 2017, l'Agence nationale de sécurité du médicament et des produits de santé demande au Conseil d'État d'ordonner le sursis à exécution de l'ordonnance rectifiée du juge des référés du tribunal administratif de Nîmes, jusqu'à ce qu'il soit statué sur son pourvoi tendant à l'annulation de cette ordonnance.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la santé publique ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Dorothée Pradines, auditeur,<br/>
<br/>
              - les conclusions de M. Charles Touboul, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Le Prado, avocat de l'Agence nationale de sécurité du médicament et des produits de santé et à la SCP Baraduc, Duhamel, Rameix, avocat de la société Société Ysy Médical.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge des référés que, par une décision du 1er mars 2017, prise sur le fondement des pouvoirs que lui donne l'article L. 5322-2 du code de la santé publique et confirmée sur recours gracieux de la société le 22 juin suivant, la directrice générale adjointe de l'Agence nationale de sécurité du médicament et des produits de santé a suspendu la mise sur le marché, la distribution et l'exportation des dispositifs médicaux Ysy ER, Ysy Est, Ysy Evolution 2 et Ysy Evolution 4, fabriqués par Ysy Médical, mis sur le marché après le 7 mars 2014, et a ordonné le retrait de ces produits auprès des clients dans un délai de six mois ainsi que la suspension de leur utilisation à l'issue de ce délai. Par une ordonnance du 21 septembre 2017, rectifiée par une ordonnance du 27 septembre 2017, le juge des référés du tribunal administratif de Nîmes a suspendu l'exécution de ces décisions en tant seulement qu'elles ont pour effet le rappel et la suspension de l'utilisation des dispositifs médicaux en cause, mis sur le marché entre le 7 mars 2014 et la date de notification de la décision en litige. <br/>
<br/>
              2. Le pourvoi par lequel l'Agence nationale de sécurité du médicament et des produits de santé demande l'annulation de cette ordonnance et sa requête tendant à ce qu'il soit sursis à l'exécution de cette ordonnance présentent à juger les mêmes questions. Il y a lieu de les joindre pour y statuer par une seule décision.<br/>
<br/>
              Sur la recevabilité du pourvoi en cassation :<br/>
<br/>
              3. Aux termes de l'article L. 5322-2 du code de la santé publique : " Le directeur général de l'agence prend, au nom de l'Etat, les décisions qui relèvent, en ce qui concerne les produits mentionnés à l'article L. 5311-1, de la compétence de celle-ci en vertu des dispositions du présent code, (...) ainsi que des mesures réglementaires prises pour l'application de ces dispositions. / Les décisions prises par le directeur général en application du présent article ne sont susceptibles d'aucun recours hiérarchique. Toutefois, en cas de menace grave pour la santé publique, le ministre chargé de la santé peut s'opposer, par arrêté motivé, à la décision du directeur général et lui demander de procéder, dans le délai de trente jours, à un nouvel examen du dossier ayant servi de fondement à ladite décision. Cette opposition est suspensive de l'application de cette décision. (...) ". Aux termes de l'article L. 5311-1 du même code : " I.- L'Agence nationale de sécurité du médicament et des produits de santé est un établissement public de l'Etat, placé sous la tutelle du ministre chargé de la santé. / II.- (...) L'agence participe à l'application des lois et règlements et prend, dans les cas prévus par des dispositions particulières, des décisions relatives à l'évaluation, aux essais, à la fabrication, à la préparation, à l'importation, à l'exportation, à la distribution en gros, au courtage, au conditionnement, à la conservation, à l'exploitation, à la mise sur le marché, à la publicité, à la mise en service ou à l'utilisation des produits à finalité sanitaire destinés à l'homme et des produits à finalité cosmétique, et notamment : / (...) / 3° Les biomatériaux et les dispositifs médicaux ; (...) ". <br/>
<br/>
              4. Il résulte de ces dispositions que si le directeur général de l'Agence nationale de sécurité du médicament et des produits de santé prend, au nom de l'État, les décisions qui relèvent de la compétence de l'agence, il dispose, à cet effet, d'une grande autonomie et n'est pas subordonné au contrôle hiérarchique du ministre chargé de la santé, qui n'a pas le pouvoir de réformer ses décisions. Ce dernier ne peut que s'opposer provisoirement à sa décision, en cas de menace grave pour la santé publique, en lui demandant de procéder à un réexamen du dossier. Par suite, le directeur général de l'Agence nationale de sécurité du médicament et des produits de santé doit être regardé comme ayant qualité pour représenter l'Etat devant les juridictions administratives, et en particulier devant le Conseil d'Etat, afin d'assurer la défense de ces décisions, sans qu'y fasse notamment obstacle l'article R. 432-4 du code de justice administrative selon lequel seul le ministre intéressé est habilité à représenter l'Etat devant le Conseil d'Etat. <br/>
<br/>
              5. Il ressort des pièces du dossier soumis au juge des référés que la décision attaquée, qui est relative à la mise sur le marché, la distribution, l'exportation et l'utilisation de dispositifs médicaux, a été prise en application des dispositions combinées des articles L. 5322-2 et L. 5311-1 du code de la santé publique. Par suite, le directeur général de l'Agence nationale de sécurité du médicament et des produits de santé a qualité pour se pourvoir en cassation au nom de l'État contre l'ordonnance du juge des référés du tribunal administratif de Nîmes ayant partiellement fait droit à la demande de suspension de l'exécution de sa décision formée par la société Ysy Médical.<br/>
<br/>
              Sur les moyens du pourvoi :<br/>
<br/>
              6. Aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ".<br/>
<br/>
              7. En premier lieu, l'urgence justifie que soit prononcée la suspension d'un acte administratif lorsque l'exécution de celui-ci porte atteinte, de manière suffisamment grave et immédiate, à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre. Il appartient au juge des référés d'apprécier concrètement, compte tenu des justifications fournies par le requérant, si les effets de l'acte litigieux sont de nature à caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, l'exécution de la décision soit suspendue. Il lui appartient également, l'urgence s'appréciant objectivement et compte tenu de l'ensemble des circonstances de chaque espèce, de faire apparaître dans sa décision tous les éléments qui, eu égard notamment à l'argumentation des parties, l'ont conduit à considérer que la suspension demandée revêtait un caractère d'urgence.<br/>
<br/>
              8. Il ressort des pièces du dossier soumis au juge des référés que l'obligation de rappel des produits vendus par la société Ysy Médical depuis le 7 mars 2014, soit depuis près de trois ans à la date de la décision attaquée, devait nécessairement entraîner des conséquences financières particulièrement lourdes pour cette société. Si l'évaluation de son expert comptable faisant état d'un coût de plus de 850 000 euros était sommaire, le juge des référés n'a pas dénaturé les pièces du dossier en estimant que la décision litigieuse était, eu égard à la taille de la société en cause, dont le total du bilan s'élevait au 31 décembre 2016 à 1 089 048 euros, de nature à porter une atteinte grave et immédiate à ses intérêts économiques. En jugeant que la condition d'urgence posée par l'article L. 521-1 du code de justice administrative était, en l'espèce, remplie, eu égard à cette atteinte grave et immédiate à la situation de la société et dès lors que l'Agence nationale de sécurité du médicament et des produits de santé ne faisait état d'aucun risque avéré pour la santé publique, quand bien même l'obligation de rappel pouvait être regardée comme partiellement imputable à la société qui avait continué, durant cette période, à commercialiser ces produits en dépit de leur absence de certification, le juge des référés du tribunal administratif de Nîmes a porté sur les faits de l'espèce une appréciation souveraine exempte de dénaturation et n'a pas commis d'erreur de droit. <br/>
<br/>
              9. En deuxième lieu, aux termes de l'article L. 5312-2 du code de la santé publique : " (...) lorsqu'un produit ou groupe de produits mentionné à l'article L. 5311-1 est mis sur le marché, mis en service ou utilisé sans avoir obtenu l'autorisation, l'enregistrement ou la certification préalable exigé par les dispositions législatives ou réglementaires applicables à ce produit ou groupe de produits, l'agence peut suspendre, jusqu'à la mise en conformité du produit ou groupe de produits au regard de la législation et de la réglementation en vigueur, les essais, la fabrication, la préparation, l'importation, l'exploitation, l'exportation, la distribution en gros, le conditionnement, la conservation, la mise sur le marché à titre gratuit ou onéreux, la détention en vue de la vente ou de la distribution à titre gratuit, la publicité, la mise en service, l'utilisation, la prescription, la délivrance ou l'administration de ce produit ou groupe de produits (...) ". Aux termes de l'article L. 5312-3 du même code : " Dans les cas mentionnés aux articles L. 5312-1 et L. 5312-2, (...) l'agence peut enjoindre [à] la personne physique ou morale responsable de la mise sur le marché, de la mise en service ou de l'utilisation de procéder au retrait du produit ou groupe de produits en tout lieu où il se trouve, à sa destruction lorsque celle-ci constitue le seul moyen de faire cesser le danger, et ordonner la diffusion de mises en garde ou de précautions d'emploi. Ces mesures sont à la charge de cette personne (...) ".<br/>
<br/>
              10. Le juge des référés a jugé qu'en l'état de l'instruction, si les moyens soulevés n'étaient pas propres à créer un doute sérieux quant à la légalité de la décision en tant qu'elle suspendait la mise sur le marché, la distribution et l'exportation des dispositifs médicaux de la société Ysy Médical, le moyen tiré de l'erreur d'appréciation commise par l'Agence nationale de sécurité du médicament et des produits de santé était en revanche, en l'absence de risque démontré pour la santé publique, propre à créer un tel doute quant à la légalité de la décision en tant qu'elle suspendait l'utilisation des dispositifs médicaux mis sur le marché depuis le 7 mars 2014 et en ordonnait le rappel. En statuant ainsi, alors que " l'absence de danger avéré " était soulignée par les termes mêmes de la décision litigieuse et que l'Agence nationale de sécurité du médicament et des produits de santé se bornait à soutenir devant le juge des référés, comme elle le fait, du reste, dans le présent pourvoi, que la suspension de la mise sur le marché des produits litigieux, faute de certificat CE en cours de validité, impliquait nécessairement l'injonction de rappel des produits déjà mis sur le marché, alors que dans une telle situation l'Agence " peut enjoindre " le retrait du produit en application de l'article L. 5312-3 du code de la santé publique, le juge des référés n'a pas, eu égard à son office, commis d'erreur de droit ni dénaturé les pièces du dossier.<br/>
<br/>
              11. Il résulte de ce qui précède que l'Agence nationale de sécurité du médicament et des produits de santé n'est pas fondée à demander l'annulation de l'ordonnance du juge des référés du tribunal administratif de Nîmes qu'elle attaque.<br/>
<br/>
              Sur les frais liés au litige :<br/>
<br/>
              12. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la société Ysy Médical, qui n'est pas, dans la présente instance, la partie perdante. Par ailleurs, en application de l'article L. 5322-2 du code de la santé publique, les décisions prises par le directeur général de l'Agence nationale de sécurité du médicament et des produits de santé dans l'exercice des pouvoirs qu'il tient du code de la santé publique le sont au nom de l'État. Par suite, les conclusions de la société Ysy Médical, qui tendent à ce qu'une somme soit mise à la charge de cette agence au titre des dispositions de l'article L. 761-1 du  code de justice administrative, sont mal dirigées et ne peuvent, dès lors, qu'être rejetées.<br/>
<br/>
              Sur la requête à fin de sursis à exécution :<br/>
<br/>
              13. Le pourvoi formé par le directeur général de l'Agence nationale de sécurité du médicament et des produits de santé contre l'ordonnance du 21 septembre 2017, rectifiée par une ordonnance du 27 septembre 2017, du juge des référés du tribunal administratif de Nîmes étant rejeté par la présente décision, les conclusions à fin de sursis de cette même ordonnance sont devenues sans objet.<br/>
<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : Le pourvoi n° 414845 de l'Agence nationale de sécurité du médicament et des produits de santé est rejeté.<br/>
Article 2 : Il n'y a pas lieu de statuer sur les conclusions de la requête n° 415128 de l'Agence nationale de sécurité du médicament et des produits de santé.<br/>
Article 3 : Les conclusions de la société Ysy Médical présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à l'Agence nationale de sécurité du médicament et des produits de santé et à la société Ysy Médical.<br/>
Copie en sera adressée à la ministre des solidarités et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-01-05 PROCÉDURE. INTRODUCTION DE L'INSTANCE. QUALITÉ POUR AGIR. - DIRECTEUR GÉNÉRAL DE L'ANSM AU NOM DE L'ETAT - EXISTENCE, Y COMPRIS DEVANT LE CONSEIL D'ETAT [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-06-05-11 PROCÉDURE. JUGEMENTS. FRAIS ET DÉPENS. REMBOURSEMENT DES FRAIS NON COMPRIS DANS LES DÉPENS. - CONCLUSIONS TENDANT À CE QU'UNE SOMME SOIT MISE À CE TITRE À LA CHARGE DE L'AGENCE NATIONALE DE SÉCURITÉ DU MÉDICAMENT ET DES PRODUITS DE SANTÉ (ANSM) - IRRECEVABILITÉ [RJ2].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">61-10 SANTÉ PUBLIQUE. - ANSM - QUALITÉ POUR AGIR DU DIRECTEUR GÉNÉRAL AU NOM DE L'ETAT DEVANT LES JURIDICTIONS ADMINISTRATIVES - EXISTENCE, Y COMPRIS DEVANT LE CONSEIL D'ETAT [RJ1].
</SCT>
<ANA ID="9A"> 54-01-05 Il résulte des articles L. 5322-2 et L. 5311-1 du code de la santé publique (CSP) que si le directeur général de l'Agence nationale de sécurité du médicament et des produits de santé (ANSM) prend, au nom de l'État, les décisions qui relèvent de la compétence de l'agence, il dispose, à cet effet, d'une grande autonomie et n'est pas subordonné au contrôle hiérarchique du ministre chargé de la santé, qui n'a pas le pouvoir de réformer ses décisions. Ce dernier ne peut que s'opposer provisoirement à sa décision, en cas de menace grave pour la santé publique, en lui demandant de procéder à un réexamen du dossier. Par suite, le directeur général de l'ANSM doit être regardé comme ayant qualité pour représenter l'Etat devant les juridictions administratives, et en particulier devant le Conseil d'Etat, afin d'assurer la défense de ces décisions, sans qu'y fasse notamment obstacle l'article R. 432-4 du code de justice administrative (CJA) selon lequel seul le ministre intéressé est habilité à représenter l'Etat devant le Conseil d'Etat.</ANA>
<ANA ID="9B"> 54-06-05-11 En application de l'article L. 5322 2 du code de la santé publique (CSP), les décisions prises par le directeur général de l'Agence nationale de sécurité du médicament et des produits de santé (ANSM) dans l'exercice des pouvoirs qu'il tient de ce code le sont au nom de l'État. Par suite, les conclusions tendant à ce qu'une somme soit mise à la charge de cette agence au titre des dispositions de l'article L. 761-1 du  code de justice administrative (CJA) sont mal dirigées et ne peuvent, dès lors, qu'être rejetées.</ANA>
<ANA ID="9C"> 61-10 Il résulte des articles L. 5322-2 et L. 5311-1 du code de la santé publique (CSP) que si le directeur général de l'Agence nationale de sécurité du médicament et des produits de santé (ANSM) prend, au nom de l'État, les décisions qui relèvent de la compétence de l'agence, il dispose, à cet effet, d'une grande autonomie et n'est pas subordonné au contrôle hiérarchique du ministre chargé de la santé, qui n'a pas le pouvoir de réformer ses décisions. Ce dernier ne peut que s'opposer provisoirement à sa décision, en cas de menace grave pour la santé publique, en lui demandant de procéder à un réexamen du dossier. Par suite, le directeur général de l'ANSM doit être regardé comme ayant qualité pour représenter l'Etat devant les juridictions administratives, et en particulier devant le Conseil d'Etat, afin d'assurer la défense de ces décisions, sans qu'y fasse notamment obstacle l'article R. 432-4 du code de justice administrative (CJA) selon lequel seul le ministre intéressé est habilité à représenter l'Etat devant le Conseil d'Etat.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., s'agissant du directeur de Agence de biomédecine, CE, 23 décembre 2014, Agence de biomédecine, n° 360958, T. pp. 496-872-874 (sol. impl.) ; s'agissant de la Commission des opérations en bourse, CE, 9 novembre 1993, Commission des opérations de bourse, n° 143973, T. pp. 624-782-783-934-944-955 ; CE, Assemblée, 23 février 2001, Commission des opérations de bourse, n° 204425, p. 80; s'agissant de la Commission nationale des comptes de campagnes et des financements politiques, CE, Section, 26 juillet 1996, Elections municipales de Tonneins, n° 177534 , p. 307., ,[RJ2] Rappr., s'agissant de l'Agence française de sécurité sanitaire des produits de santé, CE, 27 juin 2008, Société Coating Industries, n° 299284, p. 870.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
