<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000024736720</ID>
<ANCIEN_ID>JG_L_2011_10_000000346634</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/24/73/67/CETATEXT000024736720.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 26/10/2011, 346634</TITRE>
<DATE_DEC>2011-10-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>346634</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ODENT, POULET ; SCP DELAPORTE, BRIARD, TRICHET</AVOCATS>
<RAPPORTEUR>M. Jean Lessi</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Claire Landais</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2011:346634.20111026</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 11 février 2011 au secrétariat du contentieux du Conseil d'Etat, présentée pour la SOCIETE GLOBAL CARRIBEAN NETWORK(GCN), dont le siège est Tour Sécid, Place de la Rénovation à Pointe-à-Pitre (97110) ; la société GCN demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision n° 1000703 du 13 janvier 2011 par laquelle le tribunal administratif de Basse-Terre a autorisé la société Outremer Télécom, sur le fondement de l'article L. 4143-1 du code général des collectivités territoriales, à saisir, en lieu et place de la région Guadeloupe, le juge administratif du contrat afin que soient sanctionnés les divers manquements contractuels relevés à son encontre et que soit prononcée, le cas échéant, la déchéance de la délégation de service public dont elle est titulaire ;<br/>
<br/>
              2°) de refuser l'autorisation sollicitée par la société Outremer Télécom ; <br/>
<br/>
              3°) de mettre à la charge de la société Outremer Télécom le versement de la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des collectivités territoriales ;<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean Lessi, Auditeur,  <br/>
<br/>
              - les observations de la SCP Odent, Poulet, avocat de la SOCIETE GLOBAL CARRIBEAN NETWORK et de la SCP Delaporte, Briard, Trichet, avocat de la société Outremer Télécom, <br/>
<br/>
              - les conclusions de Mme Claire Landais, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Odent, Poulet, avocat de la SOCIETE GLOBAL CARRIBEAN NETWORK et à la SCP Delaporte, Briard, Trichet, avocat de la société Outremer Télécom ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant qu'aux termes de l'article L. 4143-1 du code général des collectivités territoriales : " Tout contribuable inscrit au rôle de la région a le droit d'exercer, tant en demande qu'en défense, à ses frais et risques, avec l'autorisation du tribunal administratif, les actions qu'il croit appartenir à la région et que celle-ci, préalablement appelée à en délibérer, a refusé ou négligé d'exercer (...) " ; qu'il résulte de ces dispositions qu'un contribuable ne peut saisir le tribunal administratif d'une demande d'autorisation en vue d'exercer une action en justice au nom de la région que si celle-ci, au préalable, a été appelée à en délibérer ; qu'à cette fin, le contribuable doit indiquer dans la demande qu'il adresse au président du conseil régional la nature de l'action envisagée, afin que le conseil régional soit en mesure de se prononcer sur l'intérêt, pour la collectivité, de l'action en cause, ainsi que sur ses chances de succès ;<br/>
<br/>
              Considérant que, par une convention de délégation de service public passée en novembre 2004 entre la région Guadeloupe et la SOCIETE GLOBAL CARRIBEAN NETWORK (GCN), la région a concédé notamment la réalisation et l'exploitation d'un câble sous-marin en fibres optiques reliant la Guadeloupe à Porto-Rico, ainsi que les îles Saint-Martin et Saint-Barthélemy ; que la demande dont la société Outremer Télécom (OMT) avait saisi la région, avant d'obtenir, par une décision du 13 janvier 2011 du tribunal administratif de Basse-Terre, l'autorisation de plaider contestée, tendait à ce que fussent " sanctionnés les divers manquements contractuels relevés à l'encontre " du délégataire " le cas échéant en prononçant la déchéance " de celui-ci ; <br/>
<br/>
              Considérant que cette demande n'était pas formulée dans des termes suffisamment précis pour identifier la nature de l'action en justice que la société OMT demandait à la région d'exercer ; que celle-ci ne pouvant, dans ces conditions, être regardée comme ayant été valablement saisie d'une demande tendant à ce qu'elle engage elle-même l'action, comme l'exigent les dispositions de l'article L. 4143-1, l'autorisation de plaider ainsi sollicitée ne pouvait être accordée à la société OMT ; que cette société ne saurait régulariser sa demande en précisant, pour la première fois devant le Conseil d'Etat, qu'elle tendait en réalité à introduire une action en responsabilité contractuelle ; qu'au demeurant, si elle soutient que la délégation de service public a été exécutée dans des conditions préjudiciables aux intérêts de la région et reproche à celle-ci ne pas avoir pris les mesures commandées par la situation, il résulte de l'instruction que la région a diligenté une mission d'audit technique, financier et juridique, puis signé, à la suite du rapport d'audit, plusieurs avenants tarifaires destinés à réduire les tarifs et organisé des comités de suivi sur les mises en conformité d'ordre technique, organisationnel et financier et, enfin, a mis en demeure, le 3 février 2011, la société GCN de tirer toutes les conséquences du rapport d'audit ; qu'ainsi, la région ne pouvait, en tout état de cause, être regardée comme ayant fait preuve de négligence dans la défense des intérêts de la collectivité ;<br/>
<br/>
              Considérant qu'il résulte de ce qui précède que la société GCN est fondée à demander l'annulation de la décision du 13 janvier 2011 par laquelle le tribunal administratif de Basse-Terre a délivré à la société OMT l'autorisation qu'elle sollicitait ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société OMT le versement à la société GCN de la somme de  3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ; que ces dispositions font en revanche obstacle à ce qu'une somme soit mise à ce titre à la charge de la société GCN ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La décision du tribunal administratif de Basse-Terre du 13 janvier 2011 est annulée.<br/>
Article 2 : La demande d'autorisation de plaider présentée par la société Outremer Télécom est rejetée.<br/>
Article 3 : La société Outremer Télécom versera à la SOCIETE GLOBAL CARRIBEAN NETWORK la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions de la société Outremer Télécom présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à la SOCIETE GLOBAL CARRIBEAN NETWORK, à la société Outremer Télécom et à la région Guadeloupe.<br/>
Copie en sera adressée pour information au ministre de l'intérieur, de l'outre-mer, des collectivités territoriales et de l'immigration.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">135-02-05-01-03 COLLECTIVITÉS TERRITORIALES. COMMUNE. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. EXERCICE PAR UN CONTRIBUABLE DES ACTIONS APPARTENANT À LA COMMUNE. PROCÉDURE DEVANT LE CONSEIL D'ETAT. - 1) CARACTÈRE INSUFFISAMMENT PRÉCIS DE LA DEMANDE À LA COLLECTIVITÉ - MOYEN D'ORDRE PUBLIC À SOULEVER D'OFFICE PAR LE JUGE - EXISTENCE - 2) OBLIGATION DE NOTIFICATION PRÉALABLE AUX PARTIES (ART. R. 611-7 DU CJA) - EXISTENCE, S'AGISSANT D'UN CAS OÙ LE RECOURS CONCERNE UNE AUTORISATION ACCORDÉE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-04-03-02 PROCÉDURE. INSTRUCTION. CARACTÈRE CONTRADICTOIRE DE LA PROCÉDURE. COMMUNICATION DES MOYENS D'ORDRE PUBLIC. - AUTORISATIONS DE PLAIDER - CARACTÈRE INSUFFISAMMENT PRÉCIS DE LA DEMANDE À LA COLLECTIVITÉ - MOYEN D'ORDRE PUBLIC À SOULEVER D'OFFICE PAR LE JUGE - EXISTENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-07-01-04-01-02 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. QUESTIONS GÉNÉRALES. MOYENS. MOYENS D'ORDRE PUBLIC À SOULEVER D'OFFICE. EXISTENCE. - AUTORISATIONS DE PLAIDER - CARACTÈRE INSUFFISAMMENT PRÉCIS DE LA DEMANDE À LA COLLECTIVITÉ - MOYEN D'ORDRE PUBLIC À SOULEVER D'OFFICE PAR LE JUGE - EXISTENCE.
</SCT>
<ANA ID="9A"> 135-02-05-01-03 1) Le Conseil d'Etat, juge de plein contentieux de l'autorisation de plaider, vérifie d'office si la demande présentée à la collectivité était suffisamment précise.... ...2) Lorsque le recours concerne une autorisation accordée par le tribunal administratif, il appartient au Conseil d'Etat d'informer préalablement les parties en application de l'article R. 611-7 du code de justice administrative (CJA), de ce que sa décision est susceptible d'être fondée sur un tel moyen.</ANA>
<ANA ID="9B"> 54-04-03-02 Le Conseil d'Etat, juge de plein contentieux de l'autorisation de plaider, vérifie d'office si la demande présentée à la collectivité était suffisamment précise.</ANA>
<ANA ID="9C"> 54-07-01-04-01-02 Le Conseil d'Etat, juge de plein contentieux de l'autorisation de plaider, vérifie d'office si la demande présentée à la collectivité était suffisamment précise.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., s'agissant de l'obligation pour une cour administrative d'appel de notifier le moyen tiré de ce que le motif retenu par les premiers juges était inopérant, CE, 3 août 2011, Mme Craeye, n°326754, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
