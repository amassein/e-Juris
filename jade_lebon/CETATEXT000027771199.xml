<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027771199</ID>
<ANCIEN_ID>JG_L_2013_07_000000357703</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/77/11/CETATEXT000027771199.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 4ème sous-sections réunies, 25/07/2013, 357703</TITRE>
<DATE_DEC>2013-07-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>357703</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 4ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROGER, SEVAUX, MATHONNET ; SCP DIDIER, PINET</AVOCATS>
<RAPPORTEUR>Mme Domitille Duval-Arnould</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:357703.20130725</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 19 mars 2012 et 19 juin 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales (ONIAM), dont le siège est Tour Gallieni 2, 36 avenue Charles de Gaulle à Bagnolet (93175) ; l'ONIAM demande au Conseil d'Etat :<br/>
<br/>
              1°)  d'annuler l'arrêt n° 10PA04685 du 19 janvier 2012 de la cour administrative d'appel de Paris en ce qu'il a confirmé le jugement n° 0711036-0906160 du 18 juin 2010 du tribunal administratif de Paris limitant, malgré le versement par l'office à M. A... de la somme de 700 952,44 euros en réparation des préjudices subis à la suite d'une faute commise par l'Hôpital Broussais, la condamnation de l'Assistance publique - Hôpitaux de Paris (AP-HP) au remboursement d'une somme de 467 491,37 euros et la pénalité due au titre de l'article L. 1142-15 du code de la santé publique à 30 000 euros ;<br/>
<br/>
              2°) réglant l'affaire au fond, de condamner l'AP-HP à lui rembourser les sommes de 700 952,44 euros au titre des indemnités allouées et 1 900 euros au titre des frais d'expertise et à lui verser la somme de 105 142,87 euros à titre de pénalité civile ;<br/>
<br/>
               3°) de mettre à la charge de l'AP-HP le versement d'une somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de la santé publique ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Domitille Duval-Arnould, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Roger, Sevaux, Mathonnet, avocat de l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales et à la SCP Didier, Pinet, avocat de l'Assistance publique - Hôpitaux de Paris ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond qu'après avoir subi une intervention de chirurgie cardiaque, M. A...a été transféré, à compter du 17 janvier 2002, dans le service de rééducation cardiaque de l'hôpital Broussais, dépendant de l'Assistance Publique - Hôpitaux de Paris (AP-HP), et est demeuré atteint, à l'issue des soins reçus, d'une hémiplégie et d'une aphasie ; qu'il a saisi la commission régionale de conciliation et d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales de Paris qui a estimé, le 13 mars 2007, que la responsabilité de l'AP-HP était engagée en raison de fautes commises dans la prise en charge du patient ; qu'en l'absence d'offre d'indemnisation de l'AP-HP, l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales (ONIAM) s'est substitué à cet établissement et a versé aux consortsA..., le 5 septembre 2008, une indemnité transactionnelle de 700 952,44 euros comprenant un capital au titre des préjudices futurs de M. A...non couverts par des prestations ; que, le 7 février 2009, M. A...est décédé ; que, le 3 avril 2009, l'ONIAM, subrogé dans les droits des consortsA..., a saisi le tribunal administratif de Paris d'une demande tendant notamment à la condamnation de l'AP-HP à lui verser la somme de 700 952,44 euros, augmentée des frais d'expertise et de la pénalité prévue à l'article L. 1141-15 du code de la santé publique ; que, par un jugement du 18 juin 2010, le tribunal administratif de Paris a retenu la responsabilité de l'AP-HP, évalué les préjudices subis par M. A...jusqu'à son décès ainsi que les préjudices de son épouse et de sa fille, alloué en conséquence à l'ONIAM des indemnités d'un montant total de 467 491,37 euros, ainsi que les frais d'expertise et une pénalité de 10 000 euros ; que, par un arrêt du 19 janvier 2002 à l'encontre duquel l'ONIAM se pourvoit en cassation, la cour administrative d'appel de Paris a porté la pénalité à 30 000 euros mais confirmé le jugement en tant qu'il fixe les indemnités à la charge de l'AP-HP ; <br/>
<br/>
              2. Considérant que l'article L. 1142-1 du code de la santé publique prévoit que les conséquences dommageables d'actes de prévention, de diagnostic ou de soins sont réparées par le professionnel ou l'établissement de santé dont la responsabilité est engagée ; que les articles L. 1142-4 à L. 1142-8 et R. 1142-13 à R. 1142-18 de ce code organisent une procédure de règlement amiable confiée aux commissions régionales de conciliation et d'indemnisation (CRCI) ; qu'en vertu des dispositions de l'article L. 1142-14, si la CRCI, saisie par la victime ou ses ayants droit, estime que la responsabilité du professionnel ou de l'établissement de santé est engagée, l'assureur qui garantit la responsabilité civile de celui-ci adresse aux intéressés une offre d'indemnisation ; qu'aux termes de l'article L. 1142-15, dans sa rédaction applicable au litige : " En cas de silence ou de refus explicite de la part de l'assureur de faire une offre (...), l'office institué à l'article L. 1142-22 est substitué à l'assureur. / (...) / L'acceptation de l'offre de l'office vaut transaction au sens de l'article 2044 du code civil. (...) / Sauf dans le cas où le délai de validité de la couverture d'assurance garantie par les dispositions du cinquième alinéa de l'article L. 251-2 du code des assurances est expiré, l'office est subrogé, à concurrence des sommes versées, dans les droits de la victime contre la personne responsable du dommage ou, le cas échéant, son assureur. Il peut en outre obtenir remboursement des frais d'expertise. / (...) / Lorsque l'office transige avec la victime, ou ses ayants droit, en application du présent article, cette transaction est opposable à l'assureur ou, le cas échéant, au responsable des dommages sauf le droit pour ceux-ci de contester devant le juge le principe de la responsabilité ou le montant des sommes réclamées. Quelle que soit la décision du juge, le montant des indemnités allouées à la victime lui reste acquis " ; <br/>
<br/>
              3. Considérant qu'en application de ces dispositions, il incombe au juge, saisi d'une action de l'ONIAM subrogé, à l'issue d'une transaction, dans les droits d'une victime à concurrence des sommes qu'il lui a versées, de déterminer si la responsabilité du professionnel ou de l'établissement de santé est engagée et, dans l'affirmative, d'évaluer les préjudices subis afin de fixer le montant des indemnités dues à l'office ; que lorsqu'il procède à cette évaluation, le juge n'est pas lié par le contenu de la transaction intervenue entre l'ONIAM et la victime ;  <br/>
<br/>
              4. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond qu'à l'appui de son appel contre le jugement du 18 juin 2010, l'ONIAM a reproché au tribunal administratif de Paris d'avoir fixé le montant de l'indemnité mise à la charge de l'AP-HP en fonction des préjudices subis par M. A...jusqu'à son décès, alors qu'il avait lui-même versé à la victime un capital dont le montant avait été fixé en fonction de son espérance de vie à la date de la transaction ; qu'en écartant cette contestation au motif que le choix fait par l'office d'indemniser les préjudices futurs par l'octroi d'un capital plutôt que d'une rente ne saurait s'imposer ni à l'AP-HP ni au juge, la cour administrative d'appel n'a pas commis d'erreur de droit ; <br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que l'ONIAM n'est pas fondé à demander l'annulation de l'arrêt attaqué ; que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'ONIAM qui n'est pas, dans la présente instance, la partie perdante ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par l'AP-HP à ce titre ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
		Article 1er : Le pourvoi de l'ONIAM est rejeté.<br/>
<br/>
Article 2 : Les conclusions de l'AP-HP présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 3 : La présente décision sera notifiée à l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales et à l'Assistance Publique-Hôpitaux de Paris. <br/>
Copie en sera adressée pour information à la caisse régionale d'assurance maladie d'Ile de France, à la caisse primaire d'assurance maladie des Hauts de Seine et à la caisse nationale d'assurance vieillesse. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">60-02-01-01-02 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RESPONSABILITÉ EN RAISON DES DIFFÉRENTES ACTIVITÉS DES SERVICES PUBLICS. SERVICE PUBLIC DE SANTÉ. ÉTABLISSEMENTS PUBLICS D'HOSPITALISATION. RESPONSABILITÉ POUR FAUTE MÉDICALE : ACTES MÉDICAUX. - ACTION EN RÉPARATION DES DOMMAGES SUBIS DU FAIT D'ACTES DE PRÉVENTION, DE DIAGNOSTIC OU DE SOINS - SUBROGATION, À L'ISSUE D'UNE TRANSACTION, DE L'ONIAM DANS LES DROITS D'UNE VICTIME À CONCURRENCE DES SOMMES VERSÉES (ART. L. 1142-15 DU CSP) - OFFICE DU JUGE - DÉTERMINATION DE L'ENGAGEMENT DE LA RESPONSABILITÉ DU PROFESSIONNEL OU DE L'ÉTABLISSEMENT DE SANTÉ - EVALUATION, DANS LE CAS OÙ CETTE RESPONSABILITÉ EST ENGAGÉE, DES PRÉJUDICES SUBIS - EXISTENCE - JUGE LIÉ PAR LE CONTENU DE LA TRANSACTION ENTRE L'ONIAM ET LA VICTIME - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">60-05-03 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RECOURS OUVERTS AUX DÉBITEURS DE L'INDEMNITÉ, AUX ASSUREURS DE LA VICTIME ET AUX CAISSES DE SÉCURITÉ SOCIALE. SUBROGATION. - ACTION EN RÉPARATION DES DOMMAGES SUBIS DU FAIT D'ACTES DE PRÉVENTION, DE DIAGNOSTIC OU DE SOINS - SUBROGATION, À L'ISSUE D'UNE TRANSACTION, DE L'ONIAM DANS LES DROITS D'UNE VICTIME À CONCURRENCE DES SOMMES VERSÉES (ART. L. 1142-15 DU CSP) - OFFICE DU JUGE - DÉTERMINATION DE L'ENGAGEMENT DE LA RESPONSABILITÉ DU PROFESSIONNEL OU DE L'ÉTABLISSEMENT DE SANTÉ - EVALUATION, DANS LE CAS OÙ CETTE RESPONSABILITÉ EST ENGAGÉE, DES PRÉJUDICES SUBIS - EXISTENCE - JUGE LIÉ PAR LE CONTENU DE LA TRANSACTION ENTRE L'ONIAM ET LA VICTIME - ABSENCE.
</SCT>
<ANA ID="9A"> 60-02-01-01-02 Il incombe au juge, saisi d'une action de l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales (ONIAM) subrogé en vertu de l'article L. 1142-15 du code de la santé publique (CSP), à l'issue d'une transaction, dans les droits d'une victime à concurrence des sommes qu'il lui a versées, de déterminer si la responsabilité du professionnel ou de l'établissement de santé est engagée et, dans l'affirmative, d'évaluer les préjudices subis afin de fixer le montant des indemnités dues à l'office. Lorsqu'il procède à cette évaluation, le juge n'est pas lié par le contenu de la transaction intervenue entre l'ONIAM et la victime.</ANA>
<ANA ID="9B"> 60-05-03 Il incombe au juge, saisi d'une action de l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales (ONIAM) subrogé en vertu de l'article L. 1142-15 du code de la santé publique (CSP), à l'issue d'une transaction, dans les droits d'une victime à concurrence des sommes qu'il lui a versées, de déterminer si la responsabilité du professionnel ou de l'établissement de santé est engagée et, dans l'affirmative, d'évaluer les préjudices subis afin de fixer le montant des indemnités dues à l'office. Lorsqu'il procède à cette évaluation, le juge n'est pas lié par le contenu de la transaction intervenue entre l'ONIAM et la victime.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
