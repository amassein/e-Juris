<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037220702</ID>
<ANCIEN_ID>JG_L_2018_07_000000411156</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/22/07/CETATEXT000037220702.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 6ème chambres réunies, 18/07/2018, 411156</TITRE>
<DATE_DEC>2018-07-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>411156</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 6ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Florian Roussel</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Laurence Marion</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:411156.20180718</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. I...O..., Mme F...O..., M. D...O...et Mme L...O..., Mme N...H..., épouseO..., en son nom personnel et au nom de son fils mineur, C..., M. E...H..., Mme B...-R...H..., M. M...H..., Mme P... J...et le Fonds de garantie des victimes des actes de terrorisme et d'autres infractions (FGTI) ont demandé au tribunal administratif de Nîmes de condamner l'Etat à les indemniser des préjudices subis du fait de l'assassinat de M. A...O...le 15 mars 2012. Par un jugement n° 1400420,150005 du 12 juillet 2016, le tribunal administratif de Nîmes a rejeté la demande de M. I...O..., M. D...O...et Mme L...O...et condamné l'Etat à verser à Mme N...H...épouse O...la somme de 20 000 euros, dont 10 000 euros en qualité de représentante légale de son fils mineur, à Mme B...-R... H...la somme de 21 836,27 euros, à M. E...H...la somme de 2 000 euros et au FGTI la somme de 5 000 euros.<br/>
<br/>
              Par un arrêt n° 16MA03663 du 4 avril 2017, la cour administrative d'appel de Marseille a, sur appel du ministre de l'intérieur, annulé ce jugement et rejeté les demandes des consorts O...et du FGTI.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 2 juin et 4 septembre 2017 au secrétariat du contentieux du Conseil d'Etat, Mme N...H..., épouseO..., en son nom personnel et au nom de son fils mineur, C..., M. I...O..., Mme F...O..., M. D...O...et Mme L...O..., M. E...H..., Mme B... -R... H...et M. M...H..., et Mme P...J...demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Florian Roussel, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Laurence Marion, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat de MmeH..., de M. I...O..., de Mme F...O..., de Mme L...O..., de M. D...O..., de Mme B...-véroniqueH..., de M. E...H..., de M. M...H...et de MmeJ....<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. A...O..., caporal-chef affecté au 17ème régiment du génie parachutiste, a été assassiné le 15 mars 2012 à Montauban alors qu'il était dans la rue en tenue militaire, devant le guichet automatique d'une banque ; qu'estimant que des fautes avaient été commises par les services de renseignement dans la surveillance de M. K...G..., son assassin, les parents de M. O..., ses frère et soeur, ainsi que la veuve de M.O..., agissant en son nom propre et en sa qualité de représentante légale de son fils mineur, les parents, la grand-mère et le frère de celle-ci ont adressé à l'Etat des demandes d'indemnisation préalable, qui ont été rejetées ; que le fonds de garantie des actes de terrorisme et d'autres infractions (FGTI) a également saisi l'Etat d'une demande tendant au remboursement des sommes versées aux proches de la victime ; que, par un jugement du 12 juillet 2016, le tribunal administratif de Nîmes a estimé que la responsabilité de l'Etat était engagée à raison des carences commises par les services de renseignement dans l'exercice de leur mission de prévention des actions terroristes et de surveillance des individus radicaux, du fait de l'absence de toute mesure de surveillance de M. K... G...à son retour du Pakistan en 2011 ; qu'ayant évalué à un tiers la perte de chance de prévenir le décès d'A... O...ayant résulté de la faute ainsi commise, le tribunal a retenu qu'il y avait lieu de mettre à la charge de l'Etat la réparation du tiers des préjudices des requérants ; que les premiers juges ont condamné l'Etat à verser à la veuve de la victime la somme de 20 000 euros, dont 10 000 euros en qualité de représentante légale de son fils mineur, à la belle-mère de la victime la somme de 21 836,27 euros, à son beau-père la somme de 2 000 euros et au FGTI la somme de 5 000 euros ; qu'ils ont en revanche rejeté les conclusions indemnitaires des autres membres de la familleQ...'A...O... ; que saisie par le ministre de l'intérieur, la cour administrative d'appel de Marseille a, par un arrêt du 4 avril 2017, annulé le jugement et rejeté la demande des consorts O...et du FGTI au motif que les services de l'Etat n'avaient pas commis de faute lourde en lien avec le décès d'A...O... ; que les consorts O...se pourvoient en cassation contre cet arrêt ;<br/>
<br/>
              2. Considérant, en premier lieu, qu'en jugeant que seule une faute lourde était de nature à engager la responsabilité de l'Etat à l'égard des victimes d'acte de terrorisme à raison des carences des services de renseignement dans la surveillance d'un individu ou d'un groupe d'individus, la cour n'a pas commis d'erreur de droit ;<br/>
<br/>
              3. Considérant, en second lieu, qu'il ressort des pièces du dossier soumis aux juges du fond que M. K...G..., de nationalité française, était suivi par les services de renseignement de Toulouse depuis 2006 en raison de ses fréquentations au sein du milieu de l'islam radical ; qu'à la suite d'un voyage en Afghanistan à la fin de l'année 2010, il a fait l'objet, au cours du premier semestre 2011, d'une enquête approfondie de ces services, qui ont notamment procédé à une quarantaine de filatures ainsi qu'au contrôle de ses communications téléphoniques et électroniques ; qu'au retour d'un séjour au Pakistan effectué au cours de l'été 2011, Mohamed G...a été entendu au siège de la direction centrale du renseignement intérieur le 14 novembre 2011 ; que, postérieurement à cet entretien, il n'a plus fait l'objet de mesures de surveillance particulière avant l'attentat commis à l'encontre d'A...O... ;<br/>
<br/>
              4. Considérant que la cour a relevé que l'enquête dont Mohamed G...avait fait l'objet au premier semestre 2011, si elle avait mis en évidence le profil radicalisé de l'intéressé et son comportement méfiant, n'avait pas permis de recueillir des indices suffisamment sérieux d'infraction en lien avec des actes terroristes, de nature à justifier l'ouverture d'une information judiciaire à l'encontre de l'intéressé ; que si la cour a constaté qu'au cours de l'audition du 14 novembre 2011, les agents de la direction centrale du renseignement intérieur, induits en erreur par l'attitude dissimulatrice de MohamedG..., n'étaient pas parvenus à mettre en évidence son appartenance à un réseau djihadiste et l'existence de risques suffisamment avérés de préparation d'actes terroristes, elle a retenu que ni cette méprise sur la dangerosité de l'intéressé ni l'absence de reprise des mesures de surveillance qui en est résulté ne caractérisaient, eu égard aux moyens matériels dont disposaient les services de renseignement et aux difficultés particulières inhérentes à la prévention de ce type d'attentat terroriste, l'existence d'une faute lourde ; qu'en statuant ainsi, la cour, qui a suffisamment motivé son arrêt, n'a, eu égard à ses appréciations souveraines exemptes de dénaturation, pas commis d'erreur de qualification juridique ;<br/>
<br/>
              5. Considérant que les consorts O...ne sont par suite pas fondés à demander l'annulation de l'arrêt qu'ils attaquent ; que leurs conclusions présentées au titre de l'article L. 761-1 du code de justice administrative doivent, par voie de conséquence, être rejetées ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de Mme H...et autres est rejeté.<br/>
Article 2 : La présente décision sera notifiée à Mme N...H..., épouseO..., premier requérant dénommé, au fonds de garantie des victimes des actes de terrorisme et d'autres infractions, au ministre d'Etat, ministre de l'intérieur et à la ministre des armées. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">60-01-02-02-03 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. FAITS SUSCEPTIBLES OU NON D'OUVRIR UNE ACTION EN RESPONSABILITÉ. FONDEMENT DE LA RESPONSABILITÉ. RESPONSABILITÉ POUR FAUTE. APPLICATION D'UN RÉGIME DE FAUTE LOURDE. - RESPONSABILITÉ DE L'ETAT À L'ÉGARD DES VICTIMES D'ACTES DE TERRORISME À RAISON DES CARENCES DES SERVICES DE RENSEIGNEMENT.
</SCT>
<ANA ID="9A"> 60-01-02-02-03 Seule une faute lourde est de nature à engager la responsabilité de l'Etat à l'égard des victimes d'actes de terrorisme à raison des carences des services de renseignement dans la surveillance d'un individu ou d'un groupe d'individus.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
