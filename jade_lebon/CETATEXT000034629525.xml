<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034629525</ID>
<ANCIEN_ID>JG_L_2017_05_000000407999</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/62/95/CETATEXT000034629525.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème - 10ème chambres réunies, 09/05/2017, 407999, Publié au recueil Lebon</TITRE>
<DATE_DEC>2017-05-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>407999</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème - 10ème chambres réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Marie-Gabrielle Merloz</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Astrid Nicolazo de Barmon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:407999.20170509</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B..., à l'appui de sa demande tendant à la réduction des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales auxquelles il a été assujetti au titre des années 2011 à 2013, a produit un mémoire, enregistré le 12 janvier 2017 au greffe du tribunal administratif de Grenoble, en application de l'article 23-1 de l'ordonnance n° 58-1067 du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel, par lequel il soulève deux questions prioritaires de constitutionnalité mettant en cause la conformité aux droits et libertés garantis par la Constitution des dispositions combinées, d'une part, de l'article 109 du code général des impôts et du 2° du 7 de l'article 158 du même code et, d'autre part, de l'article 109 du code général des impôts, du 2° du 7 de l'article 158 du même code et de l'article L. 136-6 du code de la sécurité sociale.<br/>
<br/>
              Par une ordonnance n° 1700212 QPC du 14 février 2017, enregistrée le 15 février 2017 au secrétariat du contentieux du Conseil d'Etat, le président de la 4ème chambre du tribunal administratif de Grenoble a décidé, par application des dispositions de l'article 23-2 de l'ordonnance n° 58-1067 du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel, de transmettre au Conseil d'Etat la question prioritaire de constitutionnalité mettant en cause la conformité aux droits et libertés garantis par la Constitution des dispositions combinées de l'article 109 du code général des impôts, du 2° du 7 de l'article 158 du même code et de l'article L. 136-6 du code de la sécurité sociale.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule et son article 61-1 ; <br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code général des impôts et le livre des procédures fiscales ; <br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie-Gabrielle Merloz, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Marie-Astrid Nicolazo de Barmon, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il résulte des dispositions de l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que, lorsqu'une juridiction relevant du Conseil d'Etat a transmis à ce dernier, en application de l'article 23-2 de cette même ordonnance, la question de la conformité à la Constitution d'une disposition législative, le Conseil constitutionnel est saisi de cette question de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              2. D'une part, aux termes du 7 de l'article 158 du code général des impôts : " Le montant des revenus et charges énumérés ci-après, retenu pour le calcul de l'impôt selon les modalités prévues à l'article 197, est multiplié par 1,25. Ces dispositions s'appliquent : / (...) 2° Aux revenus distribués mentionnés aux c à e de l'article 111, aux bénéfices ou revenus mentionnés à l'article 123 bis et aux revenus distribués mentionnés à l'article 109 résultant d'une rectification des résultats de la société distributrice (...). ". Aux termes de l'article 109 du même code : " 1. Sont considérés comme revenus distribués : (...) / 2° Toutes les sommes ou valeurs mises à la disposition des associés, actionnaires ou porteurs de parts et non prélevées sur les bénéfices (...). ".<br/>
<br/>
              3. D'autre part, aux termes du I de l'article L. 136-6 du code de la sécurité sociale relatif à la contribution sociale sur les revenus du patrimoine, aux dispositions duquel renvoient directement ou indirectement les articles 1600-0 C, 1600-0 F bis et 1600-0 G du code général des impôts relatifs à la contribution sociale généralisée, aux prélèvements sociaux et à la contribution au remboursement de la dette sociale : " Les personnes physiques fiscalement domiciliées en France au sens de l'article 4 B du code général des impôts sont assujetties à une contribution sur les revenus du patrimoine assise sur le montant net retenu pour l'établissement de l'impôt sur le revenu (...) :/ (...) c) Des revenus de capitaux mobiliers (...). ".<br/>
<br/>
              4. Par une ordonnance du 14 février 2017, le président de la 4ème chambre du tribunal administratif de Grenoble a décidé, par application des dispositions de l'article 23-2 de l'ordonnance du 7 novembre 1958, de transmettre au Conseil d'Etat la question prioritaire de constitutionnalité mettant en cause la conformité aux droits et libertés garantis par la Constitution des dispositions combinées de l'article 109 du code général des impôts, du 2° du 7 de l'article 158 du même code et de l'article L. 136-6 du code de la sécurité sociale. Si le ministre produit devant le juge de cassation une décision du 23 février 2017 par laquelle l'administration fiscale a prononcé le dégrèvement de la somme de 2 844 euros correspondant à l'application de la majoration de 25 % de l'assiette des contributions sociales en vertu de ces dispositions, le tribunal administratif de Grenoble n'a pas, à la date de la présente décision, prononcé de non-lieu à statuer. Dès lors, la question prioritaire de constitutionnalité transmise par le président de la 4ème chambre du tribunal administratif de Grenoble n'a pas perdu son objet. Il y a donc lieu, pour le Conseil d'Etat, de statuer sur le renvoi de cette question au Conseil constitutionnel. <br/>
<br/>
              5. En premier lieu, les dispositions combinées du 2° du 1 de l'article 109 du code général des impôts, du 2° du 7 de l'article 158 du même code et du c du I de l'article L. 136-6 du code de la sécurité sociale sont applicables au litige dont est saisi le tribunal administratif de Grenoble. <br/>
<br/>
              6. En deuxième lieu, dans sa décision n° 2016-610 QPC du 10 février 2017, après avoir relevé, au paragraphe 11, qu'il ressortait des travaux préparatoires de la loi du 30 décembre 2005 de finances pour 2006, dont l'article 76 a institué la majoration de l'assiette prévue au 2° du 7 de l'article 158 du code général des impôts, " que, pour l'établissement des contributions sociales, cette majoration de l'assiette des revenus en cause n'est justifiée ni par une telle contrepartie, ni par l'objectif de valeur constitutionnelle de lutte contre la fraude et l'évasion fiscales, ni par aucun autre motif ", le Conseil constitutionnel a déclaré conforme à la Constitution le c du paragraphe I de l'article L. 136-6 du code de la sécurité sociale, dans sa rédaction résultant de la loi n° 2008-1425 du 27 décembre 2008 de finances pour 2009, sous la réserve, énoncée au paragraphe 12 de sa décision, que ces dispositions " ne sauraient, sans méconnaître le principe d'égalité devant les charges publiques, être interprétées comme permettant l'application du coefficient multiplicateur de 1,25 prévu au premier alinéa du 7 de l'article 158 du code général des impôts pour l'établissement des contributions sociales assises sur les rémunérations et avantages occultes mentionnés au c de l'article 111 du même code. ". Cette réserve d'interprétation, par laquelle le Conseil constitutionnel a exclu l'application du coefficient multiplicateur de 1,25 pour l'établissement des contributions sociales assises sur les rémunérations et avantages occultes mentionnés au c de l'article 111 du même code, ne trouve à s'appliquer, en vertu de ses termes mêmes, qu'au cas de ces rémunérations et avantages occultes.<br/>
<br/>
              7. Le litige dont est saisi le tribunal administratif de Grenoble porte sur l'application du coefficient multiplicateur de 1,25 prévu au premier alinéa du 7 de l'article 158 du code général des impôts pour l'établissement des contributions sociales assises sur les revenus distribués mentionnés à l'article 109 du même code, résultant d'une rectification des résultats de la société distributrice. Il résulte de ce qui est dit au point 6 ci-dessus que les dispositions combinées du 2° du 1 de l'article 109 du code général des impôts, du 2° du 7 de l'article 158 du même code et du c du I de l'article L. 136-6 du code de la sécurité sociale dont il a ainsi été fait application, qui ne sont pas couvertes par la réserve d'interprétation prononcée par le Conseil constitutionnel dans sa décision n° 2016-610 QPC du 10 février 2017, ne peuvent être regardées comme ayant déjà été déclarées conformes à la Constitution dans les motifs et le dispositif de cette décision. <br/>
<br/>
              8. En dernier lieu, il résulte des termes mêmes de la décision n° 2016-610 QPC du 10 février 2017, et en particulier de ce que le Conseil constitutionnel a jugé au paragraphe 11 cité au point 6 ci-dessus, que le moyen tiré de ce que les dispositions litigieuses méconnaissent les principes d'égalité devant la loi et devant les charges publiques garantis par les articles 6 et 13 de la Déclaration des droits de l'homme et du citoyen soulève une question présentant un caractère sérieux. <br/>
<br/>
              9. Il résulte de tout ce qui précède qu'il y a lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La question de la conformité à la Constitution des dispositions combinées du 2° du 1 de l'article 109 du code général des impôts, du 2° du 7 de l'article 158 du même code et du c du I de l'article L. 136-6 du code de la sécurité sociale est renvoyée au Conseil constitutionnel.<br/>
Article 2 : La présente décision sera notifiée à M. A...B...et au ministre de l'économie et des finances.<br/>
Copie en sera adressée au Premier ministre et au tribunal administratif de Grenoble.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-10-05-02-02 PROCÉDURE. - CAS OÙ DES DISPOSITIONS ANALOGUES MAIS DISTINCTES DE CELLES CRITIQUÉES ONT DÉJÀ ÉTÉ DÉCLARÉES CONFORMES À LA CONSTITUTION AVEC UNE RÉSERVE D'INTERPRÉTATION.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-10-05-04-01 PROCÉDURE. - PRÉCÉDENTE DÉCISION DU CONSEIL CONSTITUTIONNEL DÉCLARANT CONFORMES À LA CONSTITUTION AVEC UNE RÉSERVE D'INTERPRÉTATION DES DISPOSITIONS ANALOGUES À CELLES CRITIQUÉES - MOTIFS DE CETTE PRÉCÉDENTE DÉCISION RÉVÉLANT LE CARACTÈRE SÉRIEUX DE LA QUESTION[RJ1].
</SCT>
<ANA ID="9A"> 54-10-05-02-02 Des dispositions législatives ne peuvent être regardées comme ayant été déclarées conformes à la Constitution par une précédente décision du Conseil constitutionnel déclarant des dispositions analogues mais distinctes conformes à la Constitution avec une réserve d'interprétation.</ANA>
<ANA ID="9B"> 54-10-05-04-01 Cas où des dispositions législatives ne peuvent être regardées comme ayant été déclarées conformes à la Constitution par une précédente décision du Conseil constitutionnel déclarant des dispositions analogues mais distinctes conformes à la Constitution avec une réserve d'interprétation. Les motifs retenus par cette précédente décision montrent le caractère sérieux de la question prioritaire de constitutionnalité invoquée. Renvoi au Conseil constitutionnel.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr. Cons. const. 7 juillet 2017, n° 2017-643/650 QPC.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
