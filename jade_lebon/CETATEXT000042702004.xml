<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042702004</ID>
<ANCIEN_ID>JG_L_2020_12_000000435097</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/70/20/CETATEXT000042702004.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 18/12/2020, 435097</TITRE>
<DATE_DEC>2020-12-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>435097</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DE NERVO, POUPET</AVOCATS>
<RAPPORTEUR>M. Didier Ribes</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Mireille Le Corre</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:435097.20201218</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B... A... a demandé au tribunal administratif de Rouen, d'une part, d'annuler pour excès de pouvoir l'arrêté du 5 août 2016 par lequel le préfet de la Seine-Maritime a refusé de lui délivrer un titre de séjour et la décision implicite de rejet de son recours gracieux et, d'autre part, d'enjoindre au préfet de lui délivrer une carte de séjour temporaire mention " vie privée et familiale " valable un an ou, à titre subsidiaire, de lui délivrer une autorisation provisoire de séjour et de réexaminer sa situation dans un délai d'un mois à compter du jugement et sous astreinte de cent euros par jours de retard. Par un jugement n° 1700194 du 12 avril 2018, le tribunal administratif de Rouen a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 18DA02128 du 7 mai 2019, la cour administrative d'appel de Douai a rejeté l'appel formé par M. A... contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 4 octobre 2019 et 6 janvier 2020 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros à verser à la SCP de Nervo, Poupet, avocat de M. A..., au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - l'accord franco-algérien du 27 décembre 1968 modifié relatif à la circulation, à l'emploi et au séjour des ressortissants algériens et de leurs familles ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le code pénal ;<br/>
              - le code de procédure pénale ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Didier Ribes, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Mireille Le Corre, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP de Nervo, Poupet, avocat de M. A... ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. A... a été condamné, par jugement du 11 mars 2013 du tribunal correctionnel de Toulouse, à une peine de deux mois d'emprisonnement avec sursis, assortie d'une peine complémentaire d'interdiction du territoire français pendant deux ans, pour soustraction à l'exécution d'une mesure de reconduite à la frontière. M. A... n'a pas quitté le territoire français postérieurement à sa condamnation. Par un arrêté du 5 août 2016, le préfet de la Seine-Maritime a refusé de délivrer à M. A... un titre de séjour en qualité de parent d'enfant français en raison de l'interdiction judiciaire du territoire prononcée à son encontre. Il a également rejeté, par une décision implicite, le recours gracieux formé par M. A... contre cet arrêté. Par un jugement du 12 avril 2018, le tribunal administratif de Rouen a rejeté la demande de M. A... tendant à l'annulation de ces deux décisions. Par un arrêt du 7 mai 2019, contre lequel M. A... se pourvoit en cassation, la cour administrative d'appel de Douai a rejeté son appel contre ce jugement.<br/>
<br/>
              2. Aux termes de l'article 131-30 du code pénal, dans sa rédaction applicable au litige : " Lorsqu'elle est prévue par la loi, la peine d'interdiction du territoire français peut être prononcée, à titre définitif ou pour une durée de dix ans au plus, à l'encontre de tout étranger coupable d'un crime ou d'un délit. / L'interdiction du territoire entraîne de plein droit la reconduite du condamné à la frontière, le cas échéant, à l'expiration de sa peine d'emprisonnement ou de réclusion. / Lorsque l'interdiction du territoire accompagne une peine privative de liberté sans sursis, son application est suspendue pendant le délai d'exécution de la peine. Elle reprend, pour la durée fixée par la décision de condamnation, à compter du jour où la privation de liberté a pris fin. / (...) ". L'article 708 du code de procédure pénale dispose : " L'exécution de la ou des peines prononcées à la requête du ministère public a lieu lorsque la décision est devenue définitive. / Toutefois, le délai d'appel accordé au procureur général par les articles 505 et 548 ne fait point obstacle à l'exécution de la peine, quelle que soit sa nature. / (...) ".<br/>
<br/>
              3. Il résulte des dispositions précitées que, sauf lorsqu'elle accompagne une peine privative de liberté sans sursis, une peine complémentaire d'interdiction temporaire du territoire s'exécute à compter du jour où le jugement la prononçant devient définitif ou à compter de son prononcé s'il est assorti de l'exécution provisoire, sans que le maintien de l'intéressé sur le territoire français, en méconnaissance de cette interdiction,  fasse obstacle à ce que l'exécution soit complète au terme de la durée d'interdiction fixée par le jugement. A cette date, cette peine ne peut justifier légalement un refus de titre de séjour.<br/>
<br/>
              4. Il suit de là qu'en jugeant que la peine complémentaire d'interdiction du territoire ne pouvait être regardée comme exécutée tant que M. A... n'avait pas quitté le territoire et en en déduisant que le préfet était tenu de refuser le titre de séjour que celui-ci sollicitait dès lors qu'il n'était pas établi que M. A... avait quitté le territoire national en exécution de l'interdiction prononcée à son encontre, la cour administrative d'appel de Douai a entaché son arrêt d'une erreur de droit.<br/>
<br/>
              5. Il résulte de ce qui précède que M. A... est fondé à demander l'annulation de l'arrêt qu'il attaque. Le requérant ayant obtenu le bénéfice de l'aide juridictionnelle, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. Il y a lieu, dans les circonstances de l'espèce, et sous réserve que la SCP de Nervo, Poupet, avocat de M. A..., renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à la SCP de Nervo, Poupet.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 7 mai 2019 de la cour administrative d'appel de Douai est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Douai.<br/>
Article 3 : L'Etat versera à la SCP de Nervo, Poupet, avocat de M. A..., une somme de 3 000 euros en application des dispositions de l'article L. 761-1 du code de justice administrative et du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
Article 4 : La présente décision sera notifiée à M. B... A... et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">335-01-03-04 ÉTRANGERS. SÉJOUR DES ÉTRANGERS. REFUS DE SÉJOUR. MOTIFS. - PEINE COMPLÉMENTAIRE D'ITF (ART. 131-30 DU CODE PÉNAL) DONT LA DURÉE EST EXPIRÉE - PEINE POUVANT JUSTIFIER UN REFUS DE TITRE DE SÉJOUR - ABSENCE, ALORS MÊME QUE L'ITF N'A PAS ÉTÉ EXÉCUTÉE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">59-01-02-03 RÉPRESSION. DOMAINE DE LA RÉPRESSION PÉNALE. DROIT PÉNAL. PEINES. - PEINE COMPLÉMENTAIRE D'ITF (ART. 131-30 DU CODE PÉNAL) DONT LA DURÉE EST EXPIRÉE - PEINE POUVANT JUSTIFIER UN REFUS DE TITRE DE SÉJOUR - ABSENCE, ALORS MÊME QUE L'ITF N'A PAS ÉTÉ EXÉCUTÉE [RJ1].
</SCT>
<ANA ID="9A"> 335-01-03-04 Il résulte des articles 131-30 du code pénal et 708 du code de procédure pénale que, sauf lorsqu'elle accompagne une peine privative de liberté sans sursis, une peine complémentaire d'interdiction temporaire du territoire français (ITF) s'exécute à compter du jour où le jugement la prononçant devient définitif ou à compter de son prononcé s'il est assorti de l'exécution provisoire, sans que le maintien de l'intéressé sur le territoire français, en méconnaissance de cette interdiction, fasse obstacle à ce que l'exécution soit complète au terme de la durée d'interdiction fixée par le jugement. A cette date, cette peine ne peut justifier légalement un refus de titre de séjour.,,,Il suit de là qu'un refus de titre de séjour ne peut légalement se fonder sur une ITF dont la durée est expirée, alors même que l'intéressé s'est maintenu irrégulièrement sur le territoire français.</ANA>
<ANA ID="9B"> 59-01-02-03 Il résulte des articles 131-30 du code pénal et 708 du code de procédure pénale que, sauf lorsqu'elle accompagne une peine privative de liberté sans sursis, une peine complémentaire d'interdiction temporaire du territoire français (ITF) s'exécute à compter du jour où le jugement la prononçant devient définitif ou à compter de son prononcé s'il est assorti de l'exécution provisoire, sans que le maintien de l'intéressé sur le territoire français, en méconnaissance de cette interdiction, fasse obstacle à ce que l'exécution soit complète au terme de la durée d'interdiction fixée par le jugement. A cette date, cette peine ne peut justifier légalement un refus de titre de séjour.,,,Il suit de là qu'un refus de titre de séjour ne peut légalement se fonder sur une ITF dont la durée est expirée, alors même que l'intéressé s'est maintenu irrégulièrement sur le territoire français.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., sur le caractère temporaire de l'ITF, CE, Section, 28 juillet 2000, Préfet de police c/,, n° 210367, p. 340.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
