<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032189018</ID>
<ANCIEN_ID>JG_L_2016_03_000000389023</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/18/90/CETATEXT000032189018.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème / 5ème SSR, 09/03/2016, 389023</TITRE>
<DATE_DEC>2016-03-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>389023</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème / 5ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MATUCHANSKY, VEXLIARD, POUPOT</AVOCATS>
<RAPPORTEUR>M. Jean-François de Montgolfier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Gaëlle Dumortier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2016:389023.20160309</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire complémentaire, enregistrés au secrétariat du contentieux du Conseil d'Etat les 27 mars et 28 juillet 2015, M. A...B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 13 mars 2015 par laquelle le Conseil national de l'ordre des pédicures-podologues a rejeté son recours contre la décision du 16 décembre 2014 par laquelle le conseil régional de Provence-Alpes-Côte d'Azur de l'ordre des pédicures-podologues a refusé de l'inscrire au tableau de cet ordre ;<br/>
<br/>
              2°) d'enjoindre au Conseil national de l'ordre des pédicures-podologues de l'inscrire au tableau de cet ordre dans un délai de quinze jours à compter de la notification de la présente décision ;<br/>
<br/>
              3°) d'assortir cette injonction d'une astreinte de 500 euros par jour de retard ;<br/>
<br/>
              4°) de mettre à la charge du conseil national la somme de 2 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la santé publique ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-François de Montgolfier, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Barthélemy, Matuchansky, Vexliard, Poupot, avocat du Conseil national de l'ordre des pédicures-podologues ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article L. 4322-2 du code de la santé publique : " Sont tenues de se faire enregistrer auprès du service ou de l'organisme désigné à cette fin par le ministre chargé de la santé les personnes ayant obtenu un titre de formation ou une autorisation requis pour l'exercice de la profession de pédicure-podologue, avant leur entrée dans la profession, ainsi que celles qui ne l'exerçant pas ont obtenu leur titre de formation depuis moins de trois ans. / L'enregistrement de ces personnes est réalisé après vérification des pièces justificatives attestant de leur identité et de leur titre de formation ou de leur autorisation. Elles informent le même service ou organisme de tout changement de résidence ou de situation professionnelle. (...) / Nul ne peut exercer la profession de pédicure-podologue si ses diplômes, certificats, titres ou autorisation n'ont été enregistrés conformément au premier alinéa et s'il n'est inscrit au tableau tenu par l'ordre " ; qu'en application de ces dispositions, toute personne souhaitant exercer en France la profession de pédicure-podologue doit, d'une part, se faire enregistrer auprès de l'autorité administrative compétente qui vérifie ses titres de formation, d'autre part, demander à l'ordre des pédicures-podologues son inscription au tableau ;<br/>
<br/>
              2. Considérant, d'une part, que, en ce qui concerne les titres et formations, l'article L. 4322-3 du code de la santé publique dispose que les études de pédicure-podologue sont sanctionnées en France par un diplôme d'Etat ; qu'en outre il résulte des dispositions combinées des articles L. 4322-4 et R. 4322-14 du code de la santé publique que le préfet de région  " (...) peut, après avis d'une commission composée notamment de professionnels, autoriser individuellement à exercer la profession de pédicure-podologue les ressortissants d'un Etat membre de l'Union européenne ou d'un autre Etat partie à l'accord sur l'Espace économique européen, qui ont suivi avec succès un cycle d'études postsecondaires et qui, sans posséder le diplôme prévu à l'article L. 4322-3, sont titulaires : / 1° D'un titre de formation délivré par un Etat, membre ou partie, et requis par l'autorité compétente d'un Etat, membre ou partie, qui réglemente l'accès à cette profession ou son exercice, et permettant d'exercer légalement ces fonctions dans cet Etat ; / 2° Ou, lorsque les intéressés ont exercé dans un Etat, membre ou partie, qui ne réglemente pas l'accès à cette profession ou son exercice, d'un titre de formation délivré par un Etat, membre ou partie, attestant de la préparation à l'exercice de la profession, accompagné d'une attestation justifiant, dans cet Etat, de son exercice à temps plein pendant deux ans au cours des dix dernières années ou à temps partiel pendant une durée correspondante au cours de la même période. Cette condition n'est pas applicable lorsque la formation conduisant à cette profession est réglementée ; / 3° Ou d'un titre de formation délivré par un Etat tiers et reconnu dans un Etat, membre ou partie, autre que la France, permettant d'y exercer légalement la profession (...) " ; <br/>
<br/>
              3. Considérant, d'autre part, que, en ce qui concerne l'inscription au tableau de l'ordre, l'article L. 4311-16 du même code, rendu applicable aux pédicures-podologues par le neuvième alinéa de l'article L. 4322-2, dispose que le conseil de l'ordre compétent " refuse l'inscription au tableau de l'ordre si le demandeur ne remplit pas les conditions de compétence, de moralité et d'indépendance exigées pour l'exercice de la profession " ; que le I de l'article R. 4112-2 du même code, rendu applicable aux pédicures-podologues par son article R. 4323-1, précise que le conseil refuse l'inscription  " si le demandeur est dans l'un des trois cas suivants : / (...) / 2° Il est établi, dans les conditions fixées au II, qu'il ne remplit pas les conditions nécessaires de compétence (...) " ; qu'aux termes du II de ce même article, applicable aux conseils régionaux ou interrégionaux des pédicures-podologues : " II.-En cas de doute sérieux sur la compétence professionnelle du demandeur, le conseil départemental saisit, par une décision non susceptible de recours, le conseil régional ou interrégional qui diligente une expertise. Le rapport d'expertise est établi dans les conditions prévues aux II, III, IV, VI et VII de l'article R. 4124-3-5 et il est transmis au conseil départemental. / S'il est constaté, au vu du rapport d'expertise, une insuffisance professionnelle rendant dangereux l'exercice de la profession, le conseil départemental refuse l'inscription et précise les obligations de formation du praticien. La notification de cette décision mentionne qu'une nouvelle demande d'inscription ne pourra être acceptée sans que le praticien ait au préalable justifié avoir rempli les obligations de formation fixées par la décision du conseil départemental " ;<br/>
<br/>
              4. Considérant qu'il résulte, en premier lieu, de l'ensemble des dispositions citées ci-dessus qu'il n'appartient ni au conseil régional ou interrégional de l'ordre des pédicures-podologues, saisi d'une demande d'inscription au tableau de l'ordre, ni au Conseil national de cet ordre, saisi d'un recours formé contre une décision d'un conseil régional ou interrégional, de remettre en cause la décision individuelle d'autorisation d'exercer délivrée par le préfet en application de l'article L. 4322-4 du code de la santé publique ; qu'en décidant qu'il lui appartenait de vérifier si, en raison de circonstances postérieures à la décision du préfet délivrant l'autorisation d'exercice, les motifs de droit et de fait ayant justifié cette décision avaient, à la date où il statuait sur l'inscription au tableau, conservé leur valeur, le Conseil national de l'ordre des pédicures-podologues a entaché sa décision d'une erreur de droit ; <br/>
<br/>
              5. Considérant qu'il résulte, en second lieu, de ces mêmes dispositions que si le conseil compétent de l'ordre des pédicures-podologues envisage de refuser l'inscription au tableau d'un praticien pour un motif relatif à la compétence du demandeur, il ne peut le faire au seul motif d'un doute sérieux, mais doit, dans une telle hypothèse, diligenter une expertise ; que si cette expertise le conduit à constater une insuffisance professionnelle dangereuse pour la santé, il refuse l'inscription ; que, dès lors, en refusant l'inscription de M. B...au tableau de l'ordre des pédicures-podologues au seul motif que des informations transmises par le Gouvernement belge au Gouvernement français " ne [permettaient] pas à l'ordre d'évaluer la formation et de s'assurer de la compétence du demandeur ", le Conseil national de l'ordre a également entaché sa décision d'une erreur de droit ; <br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède que M. B...est fondé à demander l'annulation de la décision de l'ordre des pédicures-podologues refusant son inscription au tableau ; que, toutefois, cette annulation implique seulement que le Conseil national de l'ordre des pédicures-podologues se prononce à nouveau sur la demande d'inscription de M. B...selon les modalités rappelées ci-dessus ; que, par suite, les conclusions tendant à ce qu'il soit enjoint à cet ordre d'inscrire M. B...au tableau et ses conclusions aux fins d'astreinte ne peuvent qu'être rejetées ;<br/>
<br/>
              7. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de M.B..., qui n'est pas la partie perdante dans la présente instance ; que, dans les circonstances de l'espèce, il y a lieu de mettre à la charge du Conseil national de l'ordre des pédicures-podologues la somme de 500 euros à verser à M. B...au titre de ces mêmes dispositions ;<br/>
<br/>
<br/>
<br/>
              				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : La décision du 13 mars 2015 du Conseil national de l'ordre des pédicures-podologues est annulée.<br/>
Article 2 : Le Conseil national de l'ordre des pédicures-podologues versera à M. B...la somme de 500 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : Le surplus des conclusions de M. B...est rejeté.<br/>
Article 4 : Les conclusions présentées par le Conseil national de l'ordre des pédicures-podologues au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à M. A...B...et au Conseil national de l'ordre des pédicures-podologues.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">55-02-036 PROFESSIONS, CHARGES ET OFFICES. ACCÈS AUX PROFESSIONS. - INSCRIPTION AU TABLEAU DE L'ORDRE - 1) FACULTÉ DU CONSEIL COMPÉTENT DE REMETTRE EN CAUSE LA DÉCISION INDIVIDUELLE D'AUTORISATION D'EXERCER DÉLIVRÉE PAR LE PRÉFET - ABSENCE - 2) OFFICE DU CONSEIL COMPÉTENT LORSQU'IL ENVISAGE DE REFUSER L'INSCRIPTION POUR UN MOTIF RELATIF À LA COMPÉTENCE DU DEMANDEUR.
</SCT>
<ANA ID="9A"> 55-02-036 1) Il résulte des articles L. 4322-2, L. 4322-3, L. 4322-4, L. 4311-6, R. 4322-14, R. 4323-1 et R. 4112-2 du code de la santé publique qu'il n'appartient ni au conseil régional ou interrégional de l'ordre des pédicures-podologues, saisi d'une demande d'inscription au tableau de l'ordre, ni au Conseil national de cet ordre, saisi d'un recours formé contre une décision d'un conseil régional ou interrégional, de remettre en cause la décision individuelle d'autorisation d'exercer délivrée par le préfet en application de l'article L. 4322-4. Il n'appartient donc pas au conseil compétent de vérifier si, en raison de circonstances postérieures à la décision du préfet délivrant l'autorisation d'exercice, les motifs de droit et de fait ayant justifié cette décision ont, à la date où il statue sur l'inscription au tableau, conservé leur valeur.,,,2) Il résulte des mêmes dispositions que si le conseil compétent de l'ordre des pédicures-podologues envisage de refuser l'inscription au tableau d'un praticien pour un motif  relatif à la compétence du demandeur, il ne peut le faire au seul motif d'un doute sérieux, mais doit, dans une telle hypothèse, diligenter une expertise. Si cette expertise le conduit à constater une insuffisance professionnelle dangereuse pour la santé, il refuse l'inscription.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
