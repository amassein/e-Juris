<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043328514</ID>
<ANCIEN_ID>JG_L_2021_04_000000438163</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/32/85/CETATEXT000043328514.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 6ème chambres réunies, 02/04/2021, 438163</TITRE>
<DATE_DEC>2021-04-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>438163</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP HEMERY, THOMAS-RAQUIN, LE GUERER</AVOCATS>
<RAPPORTEUR>M. Joachim Bendavid</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:438163.20210402</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un nouveau mémoire, enregistrés les 1er février et 10 août 2020 au secrétariat du contentieux du Conseil d'Etat, M. A... B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 14 janvier 2019 par laquelle le Conseil national de l'ordre des masseurs-kinésithérapeutes a annulé la décision du 14 novembre 2019 du conseil régional de l'ordre des Pays de la Loire ayant autorisé son inscription au tableau de l'ordre et rejeté sa demande d'inscription au tableau ; <br/>
<br/>
              2°) de mettre à la charge du Conseil national de l'ordre des masseurs-kinésithérapeutes la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu : <br/>
              - le code de la santé publique ;<br/>
              - le code des relations entre le public et l'administration ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Joachim Bendavid, auditeur,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Hémery, Thomas-Raquin, Le Guerer, avocat du Conseil national de l'ordre des masseurs-kinésithérapeutes ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier que M. B..., masseur-kinésithérapeute, a demandé son inscription au tableau du conseil départemental de Maine-et-Loire de son ordre, à la suite du transfert de sa résidence professionnelle dans ce département. Par une décision du 11 septembre 2019, le conseil départemental a rejeté sa demande au motif qu'il ne remplissait pas la condition de moralité requise pour l'exercice de la profession. Toutefois, par une décision du 14 novembre 2019, le conseil régional des Pays de la Loire a, sur la demande de M. B..., annulé cette décision et autorisé son inscription au tableau de l'ordre. Sur saisine du Conseil national de l'ordre des masseurs-kinésithérapeutes, la formation restreinte de ce Conseil national a, le 14 janvier 2020, annulé la décision du conseil régional du 14 novembre 2019 et rejeté la demande d'inscription de M. B.... Ce dernier demande l'annulation pour excès de pouvoir de la décision de la formation restreinte du Conseil national.<br/>
<br/>
              Sur la régularité de la saisine de la formation restreinte du Conseil national de l'ordre des masseurs kinésithérapeutes : <br/>
<br/>
              2. Aux termes de l'article L. 4321-14 du code de la santé publique : " L'ordre des masseurs-kinésithérapeutes veille au maintien des principes de moralité, de probité et de compétence indispensables à l'exercice de la masso-kinésithérapie et à l'observation, par tous ses membres, des droits, devoirs et obligations professionnels (...) ". <br/>
<br/>
              3. Aux termes du I de l'article L. 4321-17-1 du même code : " Le conseil régional ou interrégional, placé sous le contrôle du Conseil national, remplit, sur le plan régional, les attributions générales de l'ordre définies à l'article L.4321-14. (...) / Il exerce dans les régions ou les inter régions les attributions mentionnées à l'article L.4112-4 (...) ". Selon cet article L. 4112-4, rendu applicable aux masseurs-kinésithérapeutes par l'article L. 4321-19 du même code, " Les décisions du conseil départemental rendues sur les demandes d'inscription au tableau peuvent être frappées d'appel devant le conseil régional, par le médecin, le chirurgien-dentiste ou la sage-femme demandeur, s'il s'agit d'un refus d'inscription, par le conseil national s'il s'agit d'une décision d'inscription. (...) / Les décisions du conseil régional en matière d'inscription au tableau sont notifiées sans délai par le conseil régional au médecin, chirurgien-dentiste ou sage-femme qui en est l'objet, au conseil départemental et au conseil national de l'ordre. / Le délai d'appel, tant devant le conseil régional que devant le conseil national, est de trente jours à compter, soit de la notification de la décision expresse frappée d'appel, soit du jour où est acquise la décision implicite de rejet du conseil départemental. / Faute pour les personnes intéressées d'avoir régulièrement frappé d'appel une décision d'inscription, le conseil national peut, dans un délai de trois mois à compter de l'expiration du délai d'appel, retirer cette décision lorsque celle-ci repose sur une inexactitude matérielle ou une erreur manifeste d'appréciation des conditions auxquelles est subordonnée l'inscription ". Enfin, le II de l'article L. 4321-17-1 du même code dispose que : " Les décisions des conseils régionaux ou interrégionaux en matière d'inscription au tableau (...) peuvent faire l'objet d'un recours hiérarchique devant le Conseil national. Le Conseil national peut déléguer ses pouvoirs à une formation restreinte qui se prononce en son nom ".<br/>
<br/>
              4. Il résulte de l'ensemble des dispositions citées au point 3 que, lorsqu'un conseil régional ou interrégional de l'ordre des masseurs-kinésithérapeutes prend, sur recours contre une décision d'un conseil départemental de l'ordre, une décision autorisant l'inscription d'un praticien au tableau de l'ordre, le Conseil national de l'ordre, auquel cette décision doit être notifiée, peut, dans un délai de trente jours suivant cette notification, se saisir de cette décision pour statuer sur le bien-fondé de la demande d'inscription au tableau de l'ordre. Il peut en outre, en vertu du dernier alinéa de l'article L. 4112-4, s'il ne s'est pas saisi ou n'a pas été saisi d'un recours hiérarchique dans le délai de trente jours, retirer une décision d'autorisation dans les trois mois qui suivent l'expiration du délai de trente jours, si cette décision repose sur une inexactitude matérielle ou est entachée d'une erreur manifeste d'appréciation.<br/>
<br/>
              5. En premier lieu, il ressort des pièces du dossier que la saisine de la formation restreinte du Conseil national de l'ordre à la suite de laquelle a été prise la décision litigieuse résulte de l'exercice, par le Conseil national, du pouvoir, tel que défini au point 4, de réformation des décisions d'autorisation d'inscription prises par les conseils régionaux ou interrégionaux de l'ordre. M. B... n'est, par suite, pas fondé à soutenir qu'une telle saisine serait irrégulière et de nature à entacher la décision litigieuse d'irrégularité.<br/>
<br/>
              6. En second lieu, aux termes de l'article R. 4112-5 du code de la santé publique, applicable aux masseurs-kinésithérapeutes en vertu de l'article R. 4323-1 du même code : " (...) Si le recours est présenté par le conseil national, il est accompagné de la délibération décidant de former un recours contre la décision d'inscription (...) ". Ces dispositions, qui régissent le recours en appel du Conseil national devant le conseil régional ou interrégional, sont rendues applicables au recours devant le Conseil national par l'article R. 4112-5-1 du même code. Il en résulte que la décision du Conseil national de l'ordre des masseurs-kinésithérapeutes d'exercer, dans les conditions indiquées au point 4, son pouvoir de réformation d'une décision d'un conseil régional ou interrégional autorisant l'inscription au tableau de l'ordre, doit faire l'objet d'une délibération du Conseil national.<br/>
<br/>
              7. A ce titre, si la décision de saisir la formation restreinte du Conseil national de l'ordre de la décision du 14 novembre 2019 du conseil régional des Pays de la Loire n'a, dans un premier temps, fait l'objet que d'une consultation par voie électronique des membres du Conseil national, il ressort des pièces du dossier que cette consultation électronique a été confirmée par une délibération du Conseil national de l'ordre des 11 - 12 décembre 2019. Par suite, le requérant n'est, en tout état de cause, pas fondé à soutenir que la décision attaquée a été prise sur une saisine du Conseil national entachée d'irrégularité.<br/>
<br/>
              Sur les autres moyens de légalité externe :<br/>
<br/>
              8. En premier lieu, il résulte des dispositions du II de l'article L. 4321-17-1 du code de la santé publique et de celles de l'article 46 du règlement intérieur du Conseil national de l'ordre des masseurs-kinésithérapeutes, qui prévoit que la formation restreinte du Conseil national de l'ordre est compétente pour examiner les recours formés contre les décisions des conseils régionaux en matière d'inscription au tableau, que le moyen tiré de ce que la décision litigieuse aurait été prise par une autorité incompétente doit être écarté.<br/>
<br/>
              9. En deuxième lieu, il ressort des pièces du dossier que la décision attaquée comporte la signature du président de la formation restreinte et la mention en caractères lisibles de son prénom, de son nom et de sa qualité. Par suite, le moyen tiré de ce que la décision attaquée, faute de comporter ces mentions, méconnaîtrait l'article L. 212-1 du code des relations entre le public et l'administration, doit être écarté. <br/>
<br/>
              10. En troisième lieu, si M. B... soutient que la décision attaquée est intervenue sans que les observations écrites du Conseil national de l'ordre lui aient été communiquées et sans que la décision les vise, il ne ressort, en tout état de cause, pas des pièces du dossier que le Conseil national aurait présenté de telles observations dans le cadre de la procédure. Par ailleurs, contrairement à ce que soutient le requérant, il ne résulte d'aucun texte ni d'aucun principe que la décision attaquée devrait, à peine d'irrégularité, comporter le visa de ses propres observations en défense. Enfin, il est constant que M. B... a été informé dès le 26 novembre 2019 de la possibilité de consulter, au siège du Conseil national de l'ordre, le dossier sur lequel s'est fondée la commission restreinte. S'il soutient qu'il se trouvait dans l'impossibilité de se déplacer en raison d'un mouvement de grève affectant les services de transport, il ressort toutefois des pièces du dossier qu'il n'a demandé la communication électronique du dossier que par courrier du 26 décembre 2019 et que le Conseil national de l'ordre, qui n'y était au demeurant pas tenu, a procédé à cette communication le 7 janvier 2020, soit sept jours avant que la formation restreinte ne statue. Par suite, le requérant n'est pas fondé à soutenir qu'il n'aurait pas été en mesure de préparer utilement sa défense. <br/>
<br/>
              Sur la légalité interne de la décision attaquée : <br/>
<br/>
              11. En premier lieu, en se fondant, pour estimer que la condition de moralité requise pour l'exercice de la profession de masseur-kinésithérapeute n'était pas remplie par M. B... et rejeter, par suite, sa demande d'inscription au tableau de l'ordre, sur le fait que celui-ci avait déclaré sur l'honneur en juillet 2019, dans son dossier de demande d'inscription, qu'il ne faisait l'objet d'aucune instance disciplinaire susceptible d'avoir une influence sur son inscription au tableau, alors qu'il se savait faire l'objet, au même moment, de poursuites devant la chambre disciplinaire de première instance d'Ile-de-France, lesquelles ont donné lieu le 20 novembre 2019 à une sanction d'interdiction d'exercer la profession pendant douze mois pour des faits d'agressions sexuelles sur une consoeur et plusieurs patientes, d'encaissement frauduleux de chèques et de facturation d'actes non effectués, le Conseil national de l'ordre a fait une exacte application des dispositions de l'article L. 4321-14  du code de la santé publique. Sont à cet égard sans incidence sur le caractère trompeur de la déclaration faite sur l'honneur, tant la circonstance que M. B... a fait appel de cette sanction que l'allégation selon laquelle, faute d'accusé de réception du dossier de sa demande d'inscription, il lui aurait été prétendument loisible de la compléter à tout moment en signalant l'existence des poursuites en cause.<br/>
<br/>
              12. En second lieu, M. B... ne peut sérieusement soutenir que la décision du 18 novembre 2019 par laquelle le président du conseil départemental de l'ordre de Maine-et-Loire a procédé à son inscription au tableau de l'ordre des masseurs-kinésithérapeutes afin d'exécuter la décision du 14 novembre 2019 du conseil régional des Pays de la Loire avant qu'elle ne soit ultérieurement annulée par la décision attaquée ferait obstacle à ce que le Conseil national de l'ordre rejette sa demande d'inscription au tableau.<br/>
<br/>
              13. Il résulte de tout ce qui précède que M. B... n'est pas fondé à demander l'annulation pour excès de pouvoir de la décision qu'il attaque. Sa requête doit par suite être rejetée, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à sa charge la somme que demande, au même titre, le Conseil national de l'ordre des masseurs-kinésithérapeutes.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
		Article 1er : La requête de M. B... est rejetée. <br/>
<br/>
Article 2 : Les conclusions du Conseil national de l'ordre des masseurs-kinésithérapeutes présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
<br/>
Article 3 : La présente décision sera notifiée à M. A... B... et au Conseil national de l'ordre des masseurs-kinésithérapeutes.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">55-01-02-018 PROFESSIONS, CHARGES ET OFFICES. ORDRES PROFESSIONNELS - ORGANISATION ET ATTRIBUTIONS NON DISCIPLINAIRES. QUESTIONS PROPRES À CHAQUE ORDRE PROFESSIONNEL. - INSCRIPTION AU TABLEAU - POUVOIRS DU CONSEIL NATIONAL - 1) EVOCATION D'UNE AUTORISATION DÉLIVRÉE PAR UN CONSEIL RÉGIONAL OU INTERRÉGIONAL SAISI D'UN RECOURS CONTRE UNE DÉCISION DU CONSEIL DÉPARTEMENTAL - EXISTENCE, DANS UN DÉLAI DE 30 JOURS - 2) RETRAIT D'UNE AUTORISATION POUR INEXACTITUDE MATÉRIELLE OU ERREUR MANIFESTE D'APPRÉCIATION - EXISTENCE, DANS UN DÉLAI DE 3 MOIS SUIVANT L'EXPIRATION DE CES 30 JOURS.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">55-03-035 PROFESSIONS, CHARGES ET OFFICES. CONDITIONS D'EXERCICE DES PROFESSIONS. - INSCRIPTION AU TABLEAU - POUVOIRS DU CONSEIL NATIONAL - 1) EVOCATION D'UNE AUTORISATION DÉLIVRÉE PAR UN CONSEIL RÉGIONAL OU INTERRÉGIONAL SAISI D'UN RECOURS CONTRE UNE DÉCISION DU CONSEIL DÉPARTEMENTAL - EXISTENCE, DANS UN DÉLAI DE 30 JOURS - 2) RETRAIT D'UNE AUTORISATION POUR INEXACTITUDE MATÉRIELLE OU ERREUR MANIFESTE D'APPRÉCIATION - EXISTENCE, DANS UN DÉLAI DE 3 MOIS SUIVANT L'EXPIRATION DE CES 30 JOURS.
</SCT>
<ANA ID="9A"> 55-01-02-018 1) Il résulte de la combinaison des articles L. 4112-4, L. 4321-17-1 et L. 4321-19 du code de la santé publique (CSP) que, lorsqu'un conseil régional ou interrégional de l'ordre des masseurs-kinésithérapeutes prend, sur recours contre une décision d'un conseil départemental de l'ordre, une décision autorisant l'inscription d'un praticien au tableau de l'ordre, le Conseil national de l'ordre, auquel cette décision doit être notifiée, peut, dans un délai de trente jours suivant cette notification, se saisir de cette décision pour statuer sur le bien-fondé de la demande d'inscription au tableau de l'ordre.... ...2) Il peut en outre, en vertu du dernier alinéa de l'article L. 4112-4, s'il ne s'est pas saisi ou n'a pas été saisi d'un recours hiérarchique dans le délai de trente jours, retirer une décision d'autorisation dans les trois mois qui suivent l'expiration du délai de trente jours, si cette décision repose sur une inexactitude matérielle ou est entachée d'une erreur manifeste d'appréciation.</ANA>
<ANA ID="9B"> 55-03-035 1) Il résulte de la combinaison des articles L. 4112-4, L. 4321-17-1 et L. 4321-19 du code de la santé publique (CSP) que, lorsqu'un conseil régional ou interrégional de l'ordre des masseurs-kinésithérapeutes prend, sur recours contre une décision d'un conseil départemental de l'ordre, une décision autorisant l'inscription d'un praticien au tableau de l'ordre, le Conseil national de l'ordre, auquel cette décision doit être notifiée, peut, dans un délai de trente jours suivant cette notification, se saisir de cette décision pour statuer sur le bien-fondé de la demande d'inscription au tableau de l'ordre.... ...2) Il peut en outre, en vertu du dernier alinéa de l'article L. 4112-4, s'il ne s'est pas saisi ou n'a pas été saisi d'un recours hiérarchique dans le délai de trente jours, retirer une décision d'autorisation dans les trois mois qui suivent l'expiration du délai de trente jours, si cette décision repose sur une inexactitude matérielle ou est entachée d'une erreur manifeste d'appréciation.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
