<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000023663321</ID>
<ANCIEN_ID>JG_L_2011_03_000000344197</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/23/66/33/CETATEXT000023663321.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème et 2ème sous-sections réunies, 04/03/2011, 344197</TITRE>
<DATE_DEC>2011-03-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>344197</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème et 2ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DEFRENOIS, LEVIS</AVOCATS>
<RAPPORTEUR>M. Nicolas  Polge</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CESSR:2011:344197.20110304</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 5 et 19 novembre 2010 au secrétariat du contentieux du Conseil d'Etat, présentés pour la REGION REUNION, représentée par le président du conseil régional ; la REGION REUNION demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 1000936 du 20 octobre 2010 par laquelle le juge des référés du tribunal administratif de Saint-Denis-de-la-Réunion, statuant en application de l'article L. 551-1 du code de justice administrative, a, à la demande de la société FMC Antilles, annulé la procédure de passation du marché d'entretien ménager du campus de l'Océan indien conduite par la REGION REUNION et enjoint à celle-ci, si elle persiste dans son intention de conclure un marché ayant le même objet, de relancer une procédure d'appel d'offres ;<br/>
<br/>
              2°) statuant au titre de la procédure de référé engagée, de rejeter la requête de la société FMC Antilles ;<br/>
<br/>
              3°) de mettre à la charge de la société FMC Antilles le versement de la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code des marchés publics ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Nicolas Polge, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Defrenois, Levis, avocat de la REGION REUNION, <br/>
<br/>
      - les conclusions de M. Nicolas Boulouis, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Defrenois, Levis, avocat de la REGION REUNION ;<br/>
<br/>
<br/>
<br/>Considérant qu'aux termes du premier alinéa du I de l'article 52 du code des marchés publics, applicable à la sélection des candidatures : " Avant de procéder à l'examen des candidatures, le pouvoir adjudicateur qui constate que des pièces dont la production était réclamée sont absentes ou incomplètes peut demander à tous les candidats concernés de compléter leur dossier de candidature dans un délai identique pour tous et qui ne saurait être supérieur à dix jours. (...) Il en informe les autres candidats qui ont la possibilité de compléter leur candidature dans le même délai. " ; qu'aux termes du troisième alinéa : " Les candidatures (...) sont examinées au regard des niveaux de capacités professionnelles, techniques et financières mentionnées dans l'avis d'appel public à la concurrence (...). Les candidatures qui ne satisfont pas à ces niveaux de capacité sont éliminées. " ; qu'aux termes de l'article 53, qui régit l'attribution du marché : " I.- Pour attribuer le marché au candidat qui a présenté l'offre économiquement la plus avantageuse, le pouvoir adjudicateur se fonde : / 1° Soit sur une pluralité de critères non discriminatoires et liés à l'objet du marché, notamment la qualité, le prix, la valeur technique (...). / III.- Les offres inappropriées, irrégulières et inacceptables sont éliminées (...) " ; qu'aux termes de l'article 59, applicable en cas d'appel d'offres ouvert : " I. Il ne peut y avoir de négociation avec les candidats. Il est seulement possible de demander aux candidats de préciser ou de compléter la teneur de leur offre. (...) III.- Lorsque aucune candidature ou aucune offre n'a été remise ou lorsqu'il n'a été proposé que des offres inappropriées (...) ou des offres irrégulières ou inacceptables au sens du 1° du I de l'article 35, l'appel d'offres est déclaré sans suite ou infructueux (...) " ; qu'aux termes de la deuxième phrase du 1° du I de l'article 35 : " Une offre irrégulière est une offre qui, tout en apportant une réponse au besoin du pouvoir adjudicateur, est incomplète ou ne respecte pas les exigences formulées dans l'avis d'appel public à la concurrence ou dans les documents de la consultation. " ;<br/>
<br/>
              Considérant que les dispositions citées ci-dessus distinguent la phase de sélection des candidatures à un marché public, au cours de laquelle elles permettent au pouvoir adjudicateur, avant l'examen des candidatures, et dans les conditions fixées au I de l'article 52 du code des marchés publics, de demander aux candidats de compléter leur dossier de candidature, de la phase d'attribution du marché au candidat qui a présenté l'offre économiquement la plus avantageuse, après élimination, notamment, en application du III de l'article 53, des offres que leur teneur, incomplète, rend irrégulières ;<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis au juge des référés du tribunal administratif de Saint-Denis-de-la-Réunion que par une lettre du 30 juillet 2010 la société assistant la REGION REUNION dans la conduite de la procédure d'attribution, à la suite d'un appel d'offres ouvert, du marché d'entretien ménager du campus de l'Océan indien, a indiqué à la société FMC Antilles, candidate aux trois lots du marché, d'une part, que sa candidature était complète, et, d'autre part, qu'une autre entreprise ayant été sollicitée pour compléter son dossier de candidature, il lui était également possible de compléter le sien, conformément aux dispositions du I de l'article 52 du code des marchés publics ; que par une lettre du 16 septembre 2010, le président du conseil régional de la Réunion a informé la société FMC Antilles du rejet de ses offres pour les trois lots du marché, au motif de leur caractère incomplet et, par suite, irrégulier au regard des dispositions du 1° du I de l'article 35 du code des marchés publics ; qu'en déduisant des seuls termes de ces courriers que la REGION REUNION avait invité d'autres entreprises que la société FMC Antilles à compléter leur offre, après avoir indiqué à celle-ci que la sienne était complète et sans lui demander, avant d'éliminer son offre, de donner des précisions complémentaires, alors qu'il résulte de cette pièce qu'elle ne se rapportait qu'à la vérification des candidatures conformément aux dispositions du I de l'article 52 du code des marchés publics et non à l'examen des offres dans les conditions prévues aux articles 53 et 59 du même code, le juge des référés du tribunal administratif, qui n'a ainsi distingué ni entre le contenu du dossier de candidature et la teneur de l'offre, ni entre la phase de sélection des candidatures et celle de jugement des offres, a commis une erreur de droit ; que la REGION REUNION est par suite fondée, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, à demander pour ce motif l'annulation de son ordonnance ;<br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au titre de la procédure de référé engagée, en application de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              Considérant que les dispositions du I de l'article 52 du code des marchés publics, citées plus haut, qui régissent la sélection des candidatures, si elles permettent au pouvoir adjudicateur qui constate que des pièces dont la production était réclamée sont absentes ou incomplètes de demander à tous les candidats concernés, avant l'examen des candidatures, dans les conditions fixées à cet article, de compléter leur dossier de candidature, ainsi qu'il a été dit plus haut, elles ne l'autorisent pas à leur demander de compléter la teneur de leur offre ;<br/>
<br/>
              Considérant , également, qu'il ressort de la lettre du 30 juillet 2010 que le dossier de candidature présenté par la société FMC Antilles a été regardé, avant l'examen des candidatures, comme complet ; que la lettre du 16 septembre 2010 motive l'élimination de l'offre de la société FMC Antilles, après examen des offres, non par le caractère incomplet du dossier de candidature, mais par le caractère incomplet, et donc irrégulier, de cette offre, en raison de " l'absence du mémoire technique nécessaire au jugement de la valeur technique de l'offre ", requis par les dispositions du 2° du IV et du 2° du V du règlement de consultation pour permettre au pouvoir adjudicateur d'apprécier la valeur technique de l'offre conformément aux critères indiqués dans les documents de la consultation ; que, par suite, la société FMC Antilles, qui ne conteste pas l'appréciation portée sur le caractère complet de son dossier de candidature, ni, d'ailleurs, l'appréciation portée sur la régularité de son offre, ne peut utilement soutenir que la REGION REUNION aurait méconnu, dans la mise en oeuvre des dispositions du I de l'article 52 du code des marchés publics, le principe d'égalité de traitement des candidats en éliminant son offre au stade du jugement des offres alors qu'avant l'examen des candidatures elle ne l'avait pas invitée, sur le fondement de ces dispositions, à compléter son dossier ainsi qu'elle l'avait fait à l'égard d'un autre candidat ; <br/>
<br/>
              Considérant que la demande de la société FMC Antilles tendant à l'annulation des décisions de la REGION REUNION relatives à la passation du marché d'entretien ménager du campus de l'Océan indien doit dès lors être rejetée, de même que sa demande tendant à ce qu'il soit enjoint à la région de lancer un nouvel appel d'offres ;<br/>
<br/>
              Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mis à la charge de la REGION REUNION qui n'est pas, dans la présente instance, la partie perdante, le versement de la somme que demandait la société FMC Antilles devant le juge des référés du tribunal administratif de Saint-Denis-de-la-Réunion au titre des frais exposés par elle et non compris dans les dépens ; qu'il y a lieu en revanche, dans les circonstances de l'espèce, sur le fondement des mêmes dispositions, de mettre à la charge de la société FMC Antilles le versement à la REGION REUNION de la somme de 4 500 euros au titre des frais exposés par celle-ci tant en première instance qu'en cassation, et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
     --------------<br/>
Article 1er : L'ordonnance du juge des référés du tribunal administratif de Saint-Denis-de-la-Réunion du 20 octobre 2010 est annulée.<br/>
<br/>
Article 2 : Les demandes présentées par la société FMC Antilles au juge des référés du tribunal administratif de Saint-Denis-de-la-Réunion sont rejetées.<br/>
<br/>
Article 3 : La société FMC Antilles versera la somme de 4 500 euros à la REGION REUNION au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la REGION REUNION et à la société FMC Antilles.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-02-005 MARCHÉS ET CONTRATS ADMINISTRATIFS. FORMATION DES CONTRATS ET MARCHÉS. FORMALITÉS DE PUBLICITÉ ET DE MISE EN CONCURRENCE. - SUPPRESSION DU SYSTÈME DE LA « DOUBLE ENVELOPPE » DANS LES APPELS D'OFFRES OUVERTS (DÉCRET DU 19 DÉCEMBRE 2008) - CONSÉQUENCE SUR LES POSSIBILITÉS DE RÉGULARISATION DES OFFRES - ABSENCE.
</SCT>
<ANA ID="9A"> 39-02-005 Bien que le décret n° 2008-1355 du 19 décembre 2008 ait supprimé l'exigence d'une « double enveloppe » contenant d'un côté le dossier de candidature et de l'autre l'offre du candidat, les articles 52 et 53 du code des marchés publics continuent de distinguer la phase de sélection des candidatures, au cours de laquelle elles permettent au pouvoir adjudicateur de demander aux candidats de compléter leur dossier de candidature, de la phase d'attribution du marché au candidat qui a présenté l'offre économiquement la plus avantageuse, après élimination des offres irrégulières. Dans ces conditions, il y a toujours lieu de distinguer, au sein de l'enveloppe unique, entre les éléments relatifs à la candidature, qui seuls peuvent être complétés à la demande du pouvoir adjudicateur, en vertu du I de l'article 52, avant l'examen des candidatures, et ceux relatifs à l'offre du candidat.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
