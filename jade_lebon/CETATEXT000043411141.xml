<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043411141</ID>
<ANCIEN_ID>JG_L_2021_04_000000437179</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/41/11/CETATEXT000043411141.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 4ème chambres réunies, 21/04/2021, 437179</TITRE>
<DATE_DEC>2021-04-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>437179</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 4ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELAMARRE, JEHANNIN ; RIDOUX</AVOCATS>
<RAPPORTEUR>Mme Manon Chonavel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Villette</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:437179.20210421</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme D... C... a demandé au tribunal administratif de Bordeaux d'annuler la décision du 23 novembre 2018 par laquelle le président du conseil départemental de la Gironde a confirmé un indu de revenu de solidarité active de 4 605,79 euros, de la décharger de cette somme et d'enjoindre que lui soient restituées les sommes recouvrées au titre de cet indu. Par un jugement n° 1901396 du 27 septembre 2019, le tribunal administratif de Bordeaux a annulé la décision du 23 novembre 2018, déchargé Mme C... de l'obligation de payer la part de l'indu qui n'avait pas encore été recouvrée à la date du jugement et rejeté le surplus des conclusions de la demande de Mme C....<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 27 décembre 2019 et 25 février 2020 au secrétariat du contentieux du Conseil d'Etat, Mme C... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement en tant qu'il ne fait pas entièrement droit aux conclusions de sa demande de première instance ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit au surplus des conclusions de sa demande ; <br/>
<br/>
              3°) de mettre à la charge du département de la Gironde, de la caisse d'allocations familiales de la Gironde et de l'Etat la somme de 3 000 euros à verser à la SCP Delamarre et Jéhannin, son avocat, au titre des articles 37 de la loi du 10 juillet 1991 et L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'action sociale et des familles ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme B... E..., auditrice,  <br/>
<br/>
              - les conclusions de M. Vincent Villette, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Delamarre, Jéhannin, avocat de Mme C... et à Maître Ridoux, avocat du département de la Gironde ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond qu'à la suite d'omissions dans ses déclarations trimestrielles de ressources, Mme D... C... s'est vu réclamer le 2 décembre 2017 par la caisse d'allocations familiales de la Gironde le remboursement d'un indu de revenu de solidarité active de 4 605,79 euros. Le recours administratif préalable exercé par Mme C... contre cette décision devant le président du conseil départemental de la Gironde a été rejeté par une décision du 23 novembre 2018, signée de M. A... F..., chef du service Insertion et dispositif RSA. Le tribunal administratif de Bordeaux a annulé la décision du 23 novembre 2018 au motif qu'il n'était pas justifié par le département de la Gironde de la délégation de signature accordée à M. F... par le président du conseil départemental de la Gironde, déchargé Mme C... de l'obligation de payer la part de l'indu qui n'avait pas encore été recouvrée et rejeté le surplus des conclusions de sa demande, tendant à ce qu'il soit ordonné que les sommes déjà recouvrées lui soient restituées. Mme C... se pourvoit en cassation contre ce jugement, en tant qu'il ne fait pas entièrement droit à ses conclusions de première instance. Le département de la Gironde forme un pourvoi incident contre le même jugement et conclut à son entière annulation.<br/>
<br/>
              Sur le pourvoi principal : <br/>
<br/>
              2. En cas d'annulation par le juge administratif, saisi d'un recours dirigé contre celle-ci, d'une décision qui, remettant en cause des paiements déjà effectués, ordonne la récupération d'un indu de revenu de solidarité active ou d'aide exceptionnelle de fin d'année, il est loisible à l'administration, si elle s'y croit fondée et si, en particulier, aucune règle de prescription n'y fait obstacle, de reprendre régulièrement et dans le respect de l'autorité de la chose jugée, sous le contrôle du juge, une nouvelle décision. Lorsque tout ou partie de l'indu d'allocation de revenu de solidarité active ou d'aide exceptionnelle de fin d'année a été recouvré avant que le caractère suspensif du recours n'y fasse obstacle, il appartient au juge, s'il est saisi de conclusions tendant à ce qu'il soit enjoint à l'administration de rembourser la somme déjà recouvrée ou s'il décide de prescrire cette mesure d'office, de déterminer le délai dans lequel l'administration, en exécution de sa décision, doit procéder à ce remboursement, sauf à régulariser sa décision de récupération si celle-ci n'a été annulée que pour un vice de légalité externe.<br/>
<br/>
              3. Par suite, le tribunal administratif a commis une erreur de droit en jugeant que, compte tenu du motif de l'annulation qu'il prononçait, tenant à l'incompétence de l'auteur de la décision du 23 novembre 2018 confirmant l'indu réclamé à Mme C..., cette annulation n'impliquait pas le remboursement des sommes retenues par l'organisme payeur, alors qu'il résulte de ce qui a été dit au point précédent qu'il lui appartenait au contraire de déterminer le délai dans lequel l'administration, en exécution de son jugement, devait procéder à ce remboursement, sauf à régulariser la procédure par une nouvelle décision. <br/>
<br/>
              4. Il en résulte que Mme C... est fondée à demander pour ce motif l'annulation du jugement du tribunal administratif de Bordeaux du 27 septembre 2019 qu'elle attaque, en tant qu'il rejette le surplus des conclusions de sa demande de première instance.<br/>
<br/>
              Sur le pourvoi incident :<br/>
<br/>
              5. Il ressort des énonciations du jugement attaqué que, pour faire droit aux conclusions de Mme C... tendant à l'annulation de la décision du 23 novembre 2018 rejetant son recours administratif préalable, le tribunal administratif s'est fondé sur la circonstance qu'il n'était pas justifié par le département de la Gironde, défendeur à l'instance, de l'existence d'une délégation de signature pour signer les décisions de remise de dette de revenu de solidarité active accordée par le président du conseil départemental à M. F..., signataire de la décision, dont la compétence était contestée par Mme C.... En accueillant le moyen d'incompétence soulevé par la requérante au seul vu de l'absence de justification par le département, qui n'avait pas produit à l'instance, de l'existence d'une délégation de signature au profit du signataire de la décision, le tribunal administratif de Bordeaux a commis une erreur de droit. <br/>
<br/>
              6. Il en résulte que le département de la Gironde est fondé à demander pour ce motif l'annulation du jugement du 27 septembre 2019 du tribunal administratif de Bordeaux en tant qu'il fait partiellement droit aux conclusions de Mme C....<br/>
<br/>
              Sur les frais du litige :<br/>
<br/>
              7. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par Mme C... au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du 27 septembre 2019 du tribunal administratif de Bordeaux est annulé.<br/>
Article 2 :  L'affaire est renvoyée au tribunal administratif de Bordeaux.<br/>
Article 3 : Les conclusions présentées par Mme C... au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à Mme D... C... et au département de la Gironde.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">04-04 AIDE SOCIALE. CONTENTIEUX DE L'AIDE SOCIALE ET DE LA TARIFICATION. - RSA - RECOURS DIRIGÉ CONTRE LA DÉCISION DE RÉCUPÉRATION D'UN INDU - OFFICE DU JUGE EN CAS D'ANNULATION DE CETTE DÉCISION POUR VICE DE RÉGULARITÉ [RJ1] - FACULTÉ POUR L'ADMINISTRATION DE RÉGULARISER - EXISTENCE, Y COMPRIS EN CAS DE VICE D'INCOMPÉTENCE - CONSÉQUENCE - INJONCTION DE REVERSEMENT SUR DEMANDE OU D'OFFICE, SAUF POUR L'ADMINISTRATION À RÉGULARISER.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-06-07-008 PROCÉDURE. JUGEMENTS. EXÉCUTION DES JUGEMENTS. PRESCRIPTION D'UNE MESURE D'EXÉCUTION. - ANNULATION POUR VICE DE RÉGULARITÉ DE LA DÉCISION DE RÉCUPÉRATION D'UN INDU DE RSA [RJ1] - FACULTÉ POUR L'ADMINISTRATION DE RÉGULARISER - EXISTENCE, Y COMPRIS EN CAS DE VICE D'INCOMPÉTENCE - CONSÉQUENCE - OFFICE DU JUGE - INJONCTION DE REVERSEMENT SUR DEMANDE OU D'OFFICE, SAUF POUR L'ADMINISTRATION À RÉGULARISER.
</SCT>
<ANA ID="9A"> 04-04 En cas d'annulation par le juge administratif, saisi d'un recours dirigé contre celle-ci, d'une décision qui, remettant en cause des paiements déjà effectués, ordonne la récupération d'un indu de revenu de solidarité active ou d'aide exceptionnelle de fin d'année, il est loisible à l'administration, si elle s'y croit fondée et si, en particulier, aucune règle de prescription n'y fait obstacle, de reprendre régulièrement et dans le respect de l'autorité de la chose jugée, sous le contrôle du juge, une nouvelle décision.,,,Lorsque tout ou partie de l'indu d'allocation de revenu de solidarité active (RSA) ou d'aide exceptionnelle de fin d'année a été recouvré avant que le caractère suspensif du recours n'y fasse obstacle, il appartient au juge, s'il est saisi de conclusions tendant à ce qu'il soit enjoint à l'administration de rembourser la somme déjà recouvrée ou s'il décide de prescrire cette mesure d'office, de déterminer le délai dans lequel l'administration, en exécution de sa décision, doit procéder à ce remboursement, sauf à régulariser sa décision de récupération si celle-ci n'a été annulée que pour un vice de légalité externe.</ANA>
<ANA ID="9B"> 54-06-07-008 En cas d'annulation par le juge administratif, saisi d'un recours dirigé contre celle-ci, d'une décision qui, remettant en cause des paiements déjà effectués, ordonne la récupération d'un indu de revenu de solidarité active ou d'aide exceptionnelle de fin d'année, il est loisible à l'administration, si elle s'y croit fondée et si, en particulier, aucune règle de prescription n'y fait obstacle, de reprendre régulièrement et dans le respect de l'autorité de la chose jugée, sous le contrôle du juge, une nouvelle décision.,,,Lorsque tout ou partie de l'indu d'allocation de revenu de solidarité active (RSA) ou d'aide exceptionnelle de fin d'année a été recouvré avant que le caractère suspensif du recours n'y fasse obstacle, il appartient au juge, s'il est saisi de conclusions tendant à ce qu'il soit enjoint à l'administration de rembourser la somme déjà recouvrée ou s'il décide de prescrire cette mesure d'office, de déterminer le délai dans lequel l'administration, en exécution de sa décision, doit procéder à ce remboursement, sauf à régulariser sa décision de récupération si celle-ci n'a été annulée que pour un vice de légalité externe.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., en précisant, CE, Section, 16 décembre 2016, Mme,, n° 389642, p. 555.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
