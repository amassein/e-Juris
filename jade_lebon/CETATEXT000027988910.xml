<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027988910</ID>
<ANCIEN_ID>JG_L_2013_09_000000351782</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/98/89/CETATEXT000027988910.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème / 4ème SSR, 23/09/2013, 351782</TITRE>
<DATE_DEC>2013-09-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>351782</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème / 4ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP POTIER DE LA VARDE, BUK LAMENT</AVOCATS>
<RAPPORTEUR>Mme Marie Gautier-Melleray</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Fabienne Lambolez</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:351782.20130923</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi, enregistré le 9 août 2011 au secrétariat du contentieux du Conseil d'Etat, présenté par le ministre de l'intérieur, de l'outre-mer, des collectivités territoriales et de l'immigration ; le ministre demande au Conseil d'Etat d'annuler le jugement n° 1100059 du 9 juin 2011 par lequel le tribunal administratif de Nouvelle-Calédonie a condamné l'Etat à verser à M. A... B...une indemnité mensuelle de 20 000 francs CFP pour la période du 23 juillet 2004 au 21 mars 2006, en réparation du préjudice subi du fait du refus de concours de la force publique ; <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi organique n° 99-209 et la loi n°99-210 du 19 mars 1999 ;<br/>
<br/>
              Vu la loi n° 68-1250 du 31 décembre 1968 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie Gautier-Melleray, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Fabienne Lambolez, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Potier de la Varde, Buk Lament, avocat de M. B... ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond qu'à la demande de M.B..., le tribunal de première instance de Nouméa a, par un jugement du 6 mars 2000 confirmé par un arrêt du 2 novembre 2000 de la cour d'appel de Nouméa, ordonné l'expulsion des occupants d'un terrain lui appartenant dans la commune de Mont-Dore ; que le haut-commissaire de la République en Nouvelle-Calédonie a refusé de faire droit à la demande de concours de la force publique présentée le 11 janvier 2001 par M. B...pour l'exécution de cet arrêt ; que par jugements des 29 novembre 2001 et 22 juillet 2004, le tribunal administratif de Nouvelle-Calédonie a condamné l'Etat à verser à l'intéressé une indemnité en réparation du préjudice subi, du fait de ce refus, entre le 11 mars 2001 et le 29 novembre 2001 et entre le 30 novembre 2001 et le 22 juillet 2004 ; que, le 21 mars 2006, il a été procédé avec le concours de la force publique à l'expulsion des occupants ; que ceux-ci s'étant réinstallés sur le terrain, M. B... a présenté le 6 novembre 2006 une nouvelle demande de concours de la force publique à laquelle le haut-commissaire a refusé de faire droit ; que, par le jugement attaqué du 9 juin 2011, le tribunal administratif a condamné l'Etat à réparer les préjudices subis par M. B... entre le 23 juillet 2004 et le 21 mars 2006 en raison du refus de lui accorder le concours de la force publique ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article 1er de la loi du 31 décembre 1968 relative à la prescription des créances sur l'Etat, les départements, les communes et les établissements publics, applicable en Nouvelle-Calédonie en vertu de son article 11 : " Sont prescrites, au profit de l'État, des départements et des communes, sans préjudice des déchéances particulières édictées par la loi et sous réserve des dispositions de la présente loi, toutes créances qui n'ont pas été payées dans un délai de quatre ans à partir du premier jour de l'année suivant celle au cours de laquelle les droits ont été acquis " ; qu'aux termes de l'article 2 de la même loi " La prescription est interrompue par : / Toute demande de paiement ou toute réclamation écrite adressée par un créancier à l'autorité administrative, dès lors que la demande ou la réclamation a trait au fait générateur, à l'existence, au montant ou au paiement de la créance (...) " ;<br/>
<br/>
              3. Considérant que le fait générateur de la créance détenue par M. B... à raison des préjudices subis entre le 23 juillet 2004 et le 21 mars 2006 est le refus de l'Etat de lui accorder le concours de la force publique durant cette période ; que la lettre du 6 novembre 2006 par laquelle M. B... a demandé au haut-commissaire de lui accorder le concours de la force publique après la réinstallation des occupants sans titre précédemment expulsés ne concernait pas le refus qui avait été opposé à la demande de concours présentée le 11 janvier 2001 et ne contenait aucune réclamation tendant à la réparation des préjudices qui en étaient résultés pour l'intéressé ; que, dans ces conditions, le tribunal administratif a entaché son jugement d'une erreur de qualification juridique en voyant dans le courrier du 6 novembre 2006 une demande ayant trait au fait générateur, à l'existence, au montant ou au paiement de la créance indemnitaire résultant de ce refus, de nature à interrompre la prescription par application des dispositions précitées de l'article 2 de la loi du 31 décembre 1968 ; que, par suite, sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, son jugement doit être annulé en tant que, par ses articles 1er à 3, il fait droit à la demande de M. B...au titre de la période comprise entre le 23 juillet 2004 et le 21 mars 2006 ;<br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler, dans cette mesure, l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              5. Considérant que les délais de prescription relatifs aux indemnités nées d'un refus de concours de la force publique pour l'exécution d'un jugement d'expulsion commencent à courir, pour les créances nées au cours de chacune des années en cause, le 1er janvier de l'année suivante ; que les créances relatives aux préjudices subis par M. B...au cours des années 2004, 2005 et 2006 ont donc été atteintes par la prescription, respectivement, le 1er janvier 2009, le 1er janvier 2010 et le 1er janvier 2011 ; que l'ensemble de ces créances étaient donc prescrites lorsque l'intéressé a introduit sa demande devant le tribunal administratif de Nouvelle Calédonie le 2 mars 2011 ; que, par suite, sa demande doit être rejetée ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Les articles 1er à 3 du jugement du tribunal administratif de Nouvelle-Calédonie du 9 juin 2011 sont annulés.<br/>
Article 2 : La  demande présentée par M. B...devant le tribunal administratif de Nouvelle-Calédonie est rejetée.<br/>
Article 3 : La présente décision sera notifiée au ministre des outre-mer et à M. A... B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">18-04-02-05 COMPTABILITÉ PUBLIQUE ET BUDGET. DETTES DES COLLECTIVITÉS PUBLIQUES - PRESCRIPTION QUADRIENNALE. RÉGIME DE LA LOI DU 31 DÉCEMBRE 1968. INTERRUPTION DU COURS DU DÉLAI. - DEMANDE DE PAIEMENT OU RÉCLAMATION AYANT TRAIT AU FAIT GÉNÉRATEUR, À L'EXISTENCE, AU MONTANT OU AU PAIEMENT DE LA CRÉANCE - NOTION - 1) CONTRÔLE DU JUGE DE CASSATION - CONTRÔLE DE LA QUALIFICATION JURIDIQUE - 2) EXCLUSION - CRÉANCE NÉE D'UN REFUS DE CONCOURS DE LA FORCE PUBLIQUE POUR UNE EXPULSION - NOUVELLE DEMANDE DE CONCOURS APRÈS EXPULSION SUIVIE D'UNE RÉINSTALLATION DES MÊMES OCCUPANTS.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">37-05-01 JURIDICTIONS ADMINISTRATIVES ET JUDICIAIRES. EXÉCUTION DES JUGEMENTS. CONCOURS DE LA FORCE PUBLIQUE. - CRÉANCE NÉE D'UN REFUS DE CONCOURS POUR UNE EXPULSION - NOUVELLE DEMANDE DE CONCOURS APRÈS EXPULSION SUIVIE D'UNE RÉINSTALLATION DES MÊMES OCCUPANTS - CARACTÈRE DE DEMANDE AYANT TRAIT AU FAIT GÉNÉRATEUR DE LA CRÉANCE DE NATURE À INTERROMPRE LA PRESCRIPTION QUADRIENNALE - ABSENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-08-02-02-01-02 PROCÉDURE. VOIES DE RECOURS. CASSATION. CONTRÔLE DU JUGE DE CASSATION. BIEN-FONDÉ. QUALIFICATION JURIDIQUE DES FAITS. - PRESCRIPTION QUADRIENNALE - NOTION DE DEMANDE DE PAIEMENT OU RÉCLAMATION AYANT TRAIT AU FAIT GÉNÉRATEUR, À L'EXISTENCE, AU MONTANT OU AU PAIEMENT DE LA CRÉANCE DE NATURE À INTERROMPRE LE COURS DU DÉLAI.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">60-02-03-01-03 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RESPONSABILITÉ EN RAISON DES DIFFÉRENTES ACTIVITÉS DES SERVICES PUBLICS. SERVICES DE POLICE. SERVICES DE L'ETAT. EXÉCUTION DES DÉCISIONS DE JUSTICE. - REFUS DE CONCOURS DE LA FORCE PUBLIQUE - NOUVELLE DEMANDE DE CONCOURS APRÈS EXPULSION SUIVIE D'UNE RÉINSTALLATION DES MÊMES OCCUPANTS - CARACTÈRE DE DEMANDE AYANT TRAIT AU FAIT GÉNÉRATEUR DE LA CRÉANCE NÉE DU PREMIER REFUS - ABSENCE - CONSÉQUENCE - INTERRUPTION DE LA PRESCRIPTION QUADRIENNALE - ABSENCE.
</SCT>
<ANA ID="9A"> 18-04-02-05 1) Le juge de cassation contrôle au titre de la qualification juridique la notion de demande ou de réclamation ayant trait au fait générateur, à l'existence, au montant ou au paiement d'une créance indemnitaire de nature à interrompre la prescription quadriennale par application des dispositions de l'article 2 de la loi n° 68-1250 du 31 décembre 1968.,,,2) Une nouvelle demande de concours de la force publique formée après la réinstallation d'occupants sans titre précédemment expulsés n'a pas trait au fait générateur de la créance née du refus initialement opposé à la première demande de concours et n'interrompt donc pas le cours du délai de prescription de cette créance.</ANA>
<ANA ID="9B"> 37-05-01 Une nouvelle demande de concours de la force publique formée après la réinstallation d'occupants sans titre précédemment expulsés n'a pas trait au fait générateur de la créance née du refus initialement opposé à la première demande de concours et n'interrompt donc pas le cours du délai de prescription de cette créance.</ANA>
<ANA ID="9C"> 54-08-02-02-01-02 Le juge de cassation contrôle au titre de la qualification juridique la notion de demande ou de réclamation ayant trait au fait générateur, à l'existence, au montant ou au paiement d'une créance indemnitaire de nature à interrompre la prescription quadriennale par application des dispositions de l'article 2 de la loi n° 68-1250 du 31 décembre 1968.</ANA>
<ANA ID="9D"> 60-02-03-01-03 Une nouvelle demande de concours de la force publique formée après la réinstallation d'occupants sans titre précédemment expulsés n'a pas trait au fait générateur de la créance née du refus initialement opposé à la première demande de concours et n'interrompt donc pas le cours du délai de prescription de cette créance.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
