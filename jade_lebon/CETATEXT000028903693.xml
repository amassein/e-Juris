<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028903693</ID>
<ANCIEN_ID>JG_L_2014_05_000000370830</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/90/36/CETATEXT000028903693.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème / 4ème SSR, 05/05/2014, 370830</TITRE>
<DATE_DEC>2014-05-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>370830</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème / 4ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS</AVOCATS>
<RAPPORTEUR>M. Gérald Bégranger</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:370830.20140505</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu 1°, sous le n° 370830, la requête, enregistrée le 2 août 2013 au secrétariat du contentieux du Conseil d'Etat, présentée pour la société Mercedes-Benz France, dont le siège est Parc de Rocquencourt à Rocquencourt (78150), représentée par son président ; la société Mercedes-Benz France demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 26 juillet 2013 par laquelle le ministre de l'écologie, du développement durable et de l'énergie a refusé, pour une durée de six mois au plus, l'immatriculation des véhicules du constructeur Daimler correspondant aux types suivants :<br/>
              - type 230, à l'exception de la version SZBBA200, dont la réception communautaire, enregistrée sous le numéro e1*98/14*0169*19, a fait l'objet d'une extension le 6 juin 2013 ;<br/>
              - type 245 G, à l'exception des versions de la variante Y2GBM2, dont la réception communautaire, enregistrée sous le numéro e1*2001/116*0470*04, a fait l'objet d'une extension le 3 juin 2013 ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu 2°, sous le n° 373573, l'ordonnance du président du tribunal administratif de Versailles du 22 novembre 2013, enregistrée le 28 novembre 2013 au secrétariat du contentieux du Conseil d'Etat et renvoyant au Conseil d'Etat, par application des dispositions de l'article R. 341-2 du code de justice administrative, la requête présentée par la société Mercedes-Benz France à ce tribunal ;<br/>
<br/>
              Vu la requête, enregistrée le 8 juillet 2013 au greffe du tribunal administratif de Versailles, présentée pour la société Mercedes-Benz France, dont le siège est Parc de Rocquencourt à Rocquencourt (78150) ; la société Mercedes-Benz France demande :<br/>
<br/>
              1°) l'annulation pour excès de pouvoir de la décision, révélée par les courriers électroniques des 19 juin et 2 juillet 2013 adressés par l'Organisme technique central (OTC) et les services du ministre de l'écologie, du développement durable et de l'énergie, par laquelle ce ministre a refusé de lui communiquer les codes nationaux d'identification des types de véhicules 230 et 245G ayant fait l'objet des réceptions communautaires enregistrées sous les numéros, respectivement, e1*98/14*0169*19 et e1*2001/116*0470*04 ;<br/>
<br/>
              2°) qu'il soit enjoint à ce même ministre et à l'OTC de lui communiquer ces codes dans un délai de deux jours à compter de la notification de la présente décision, sous astreinte de 1000 euros par jour de retard ;<br/>
<br/>
              3°) que soit mise à la charge de l'Etat une somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu le traité sur l'Union européenne ;<br/>
<br/>
              Vu le traité sur le fonctionnement de l'Union européenne ;<br/>
<br/>
              Vu la directive 2006/40/CE du Parlement européen et du Conseil du 17 mai 2006 ;<br/>
<br/>
              Vu la directive 2007/46/CE du Parlement européen et du Conseil du 5 septembre 2007 ;<br/>
<br/>
              Vu le code de la route ;<br/>
<br/>
              Vu l'arrêté du 21 décembre 2007 relatif à la réception des véhicules automobiles en ce qui concerne les systèmes de climatisation ;<br/>
<br/>
              Vu l'arrêté du 9 février 2009 relatif aux modalités d'immatriculation des véhicules ;<br/>
<br/>
              Vu l'arrêté du 4 mai 2009 relatif à la réception des véhicules à moteur, de leurs remorques et des systèmes et équipements destinés à ces véhicules en application de la directive 2007/46/CE ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Gérald Bégranger, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de la société Mercedes-Benz France ;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier que la société Daimler AG a, en 2011 et 2012, obtenu du Kraftfahrt-Bundesamt (KBA), autorité allemande compétente en matière de réception des véhicules automobiles, des réceptions CE pour des types de véhicules correspondant aux modèles de classes A, B, CLA et SL ; que ces véhicules devaient initialement être équipés d'un système de climatisation utilisant le gaz réfrigérant R1234yf, dont le potentiel de réchauffement planétaire (PRP) n'excède pas 150, à la place du gaz R134a, qui équipait les anciens modèles et dont le PRP est de 1300 ; qu'à la suite d'essais qui auraient révélé la dangerosité, pour l'usager du véhicule, du nouveau gaz de réfrigération dans certaines circonstances, la société Daimler AG a décidé d'installer sur ces véhicules un système de climatisation utilisant le gaz R134a ; qu'à sa demande, le KBA lui a accordé, au titre de la modification d'une spécification technique, les 3 et 6 juin 2013, l'extension de la réception CE du type 245 G, enregistrée sous le numéro d'autorisation e1*2001/116*0470*04 et correspondant aux véhicules de classes A, B et CLA, et celle de la réception CE du type 230, enregistrée sous le numéro d'autorisation 1*98/14*0169*19 et correspondant aux véhicules de classe SL ; que la société Mercedes-Benz France a sollicité des autorités françaises l'attribution des codes nationaux d'identification de ces deux types de véhicules ; que l'Organisme technique central (OTC) a refusé les 19 juin et 2 juillet 2013 l'attribution de ces codes, faisant ainsi obstacle à la mise sur le marché français de plusieurs milliers de véhicules ; que la société ayant demandé au tribunal administratif de Versailles d'annuler cette décision et de suspendre son exécution, le juge des référés du tribunal a fait droit à la demande de suspension par une ordonnance du 25 juillet 2013 ; que, par une décision du 26 juillet 2013, le ministre de l'écologie, du développement durable et de l'énergie a, sur le fondement de l'article R. 321-14 du code de la route, refusé d'immatriculer les véhicules des types 245 G, à l'exception de la version SZBBA200, et 230, à l'exception des versions de la variante Y2GBM2 ; qu'à la demande de la société Mercedes-Benz France, le juge des référés du Conseil d'Etat a suspendu l'exécution de cette décision par une ordonnance du 27 août 2013; que la société demande au Conseil d'Etat d'annuler la décision du 26 juillet 2013 ; que, par ailleurs, par une ordonnance du 22 novembre 2013, le président du tribunal administratif de Versailles a transmis au Conseil d'Etat sa demande d'annulation de la décision des 19 juin et 2 juillet 2013 ; <br/>
<br/>
              2. Considérant que les requêtes visées ci-dessus présentent à juger les mêmes questions ; qu'il y a lieu de les joindre pour statuer par une seule décision ; <br/>
<br/>
              Sur les conclusions dirigées contre la décision du ministre de l'écologie, du développement durable et de l'énergie du 26 juillet 2013 :<br/>
<br/>
              3. Considérant que la directive 2007/46/CE du Parlement européen et du Conseil du 5 septembre 2007 établissant un cadre pour la réception des véhicules à moteur, de leurs remorques et des systèmes, des composants et des entités techniques destinés à ces véhicules a remplacé les systèmes de réception de la très grande majorité des véhicules à moteur propres à chaque Etat membre par une procédure de réception harmonisée au sein de l'Union européenne, dite " réception CE par type " ; que l'objectif principal de cette directive est de garantir que les nouveaux véhicules mis sur le marché européen présentent des garanties élevées en matière de sécurité et de protection de l'environnement ; qu'en application de l'article 4 de la directive, les Etats membres ne réceptionnent que les véhicules conformes aux exigences de la directive et ne peuvent interdire, restreindre ou entraver l'immatriculation, la vente ou la circulation sur route de véhicules qui répondent à ces exigences pour des motifs liés à des aspects de leur construction ou de leur fonctionnement couverts par ce texte ; <br/>
<br/>
              4. Considérant qu'aux termes de l'article R. 321-14 du code de la route, qui transpose les dispositions du paragraphe 1 de l'article 29 de la directive 2007/46/CE, qui instituent une clause de sauvegarde : " S'il est établi que des véhicules, systèmes ou équipements d'un type ayant fait l'objet d'une réception CE compromettent gravement la sécurité routière ou nuisent gravement à l'environnement ou à la santé publique alors qu'ils sont accompagnés d'un certificat de conformité en cours de validité ou qu'ils portent une marque de réception valide, le ministre chargé des transports peut, pour une durée de six mois au maximum, refuser d'immatriculer ces véhicules ou interdire la vente ou la mise en service de ces véhicules, systèmes ou équipements. Il en informe immédiatement le constructeur et les autorités compétentes en matière de réception des autres Etats et la Commission européenne en motivant sa décision. La décision doit également être notifiée au constructeur intéressé et indiquer les voies et délais de recours " ;<br/>
<br/>
              5. Considérant, par ailleurs, que le paragraphe 4 de l'article 5 de la directive 2006/40/CE du Parlement européen et du Conseil du 17 mai 2006 concernant les émissions provenant des systèmes de climatisation des véhicules à moteur et modifiant la directive 70/156/CEE du Conseil prévoit qu'à compter du 1er janvier 2011 les Etats membres n'accordent plus la réception CE d'un type de véhicule équipé d'un système de climatisation conçu pour contenir des gaz à effet de serre fluorés dont le potentiel de réchauffement planétaire est supérieur à 150 ; que la mise en oeuvre de cette interdiction a été reportée au 1er janvier 2013 par la Commission européenne en raison des difficultés rencontrées par les constructeurs européens d'automobiles pour s'approvisionner en gaz R1234yf ;<br/>
<br/>
              6. Considérant qu'il ressort des pièces du dossier que, pour refuser, par la décision attaquée, qui présente un caractère réglementaire, l'immatriculation des véhicules en cause, le ministre de l'écologie, du développement durable et de l'énergie a fait valoir sa volonté de s'opposer au détournement, par le constructeur Daimler AG, de la procédure de réception CE prévue par la directive 2007/46/CE, d'empêcher la méconnaissance des dispositions du paragraphe 4 de l'article 5 de la directive 2006/40/CE  et de prévenir la distorsion de concurrence entre les constructeurs automobiles présents sur le marché européen pouvant résulter de l'utilisation, pour le système de climatisation des véhicules, du gaz R134a, moins coûteux que le gaz R1234yf ;<br/>
<br/>
              7. Considérant, toutefois, que de tels motifs ne sont pas au nombre de ceux qui sont prévus par les dispositions de l'article R. 321-14 du code de la route et ne pouvaient justifier légalement la décision attaquée ; qu'en particulier, le ministre ne peut se borner à invoquer une méconnaissance des dispositions du paragraphe 4 de l'article 5 de la directive 2006/40/CE, sans établir qu'elle est de nature à nuire gravement à l'environnement ou à la santé publique ;<br/>
<br/>
              8. Considérant que le ministre fait, il est vrai, aussi valoir, pour justifier la décision attaquée, la nécessité d'éviter que les véhicules en cause ne nuisent gravement à l'environnement ; qu'il soutient à cet égard que chaque véhicule dont le système de climatisation utilise le gaz R134a émettrait, sur une durée d'utilisation moyenne estimée à douze années, près de 629 kg équivalent CO2 de plus que s'il utilisait le gaz R1234yf ; que, toutefois, il ressort des pièces du dossier que moins de 6% des véhicules correspondant à de nouveaux modèles immatriculés en France en 2013 étaient équipés d'un système de climatisation utilisant un gaz répondant aux exigences prévues au paragraphe 4 de l'article 5 de la directive 2006/40/CE et que les véhicules produits par Daimler AG en cause ne représentent qu'une très faible part du parc automobile français ; que, dans ces conditions, les éléments relevés par le ministre de l'écologie, du développement durable et de l'énergie ne suffisent pas à caractériser une atteinte grave à l'environnement qui justifierait légalement l'usage de la faculté que lui donnent les dispositions précitées de l'article R. 321-14 du code de la route de refuser d'immatriculer des véhicules pendant une période maximale de six mois ; qu'ainsi le ministre a commis sur ce point, pour l'application de l'article R. 321-14 du code de la route, une erreur d'appréciation ;<br/>
<br/>
              9. Considérant qu'il résulte de ce qui précède, et sans qu'il soit besoin d'examiner les autres moyens de sa requête, que la société Mercedes-Benz France est fondée à demander l'annulation de la décision attaquée du 26 juillet 2013 ;<br/>
<br/>
              Sur les conclusions dirigées contre la décision révélée par les courriers électroniques des 19 juin et 2 juillet 2013 :<br/>
<br/>
              10. Considérant qu'il ressort des pièces du dossier que, par deux courriers électroniques des 19 juin et 2 juillet 2013, l'OTC a, sur instruction du ministre de l'écologie, du développement durable et de l'énergie, refusé de communiquer à la société Mercedes-Benz France les codes nationaux d'identification des types de véhicules 245G et 230 ; que ces deux courriers électroniques révèlent l'existence d'une décision du ministre de refus d'attribution de ces codes à la société requérante ; <br/>
<br/>
              11. Considérant qu'il résulte des dispositions combinées des articles R. 321-9, R. 321-11 et R. 322-1 du code de la route, des articles 1er et 2 de l'arrêté du 9 février 2009 relatif aux modalités d'immatriculation des véhicules et des articles 23 et 24 de l'arrêté du 4 mai 2009 relatif à la réception des véhicules à moteur, de leurs remorques et des systèmes et équipements destinés à ces véhicules en application de la directive 2007/46/CE que l'OTC est chargé de communiquer aux constructeurs de véhicules et aux services du ministère de l'intérieur chargés de l'immatriculation des véhicules le code national d'identification du type (CNIT) ; que le propriétaire d'un véhicule à moteur qui souhaite le mettre en circulation pour la première fois, doit, pour obtenir un certificat d'immatriculation, présenter aux services compétents du ministère de l'intérieur le certificat de conformité de ce véhicule, établi par le constructeur et comportant ce code ; qu'il en résulte que le refus de communiquer les codes nationaux d'identification des types de véhicules au constructeur a pour effet de faire obstacle à la livraison et à la mise en circulation sur le territoire français des véhicules correspondants ; que, par suite, la fin de non-recevoir opposée par le ministre de l'écologie, du développement durable et de l'énergie, tirée de ce que la requête de la société Mercedes-Benz France ne serait pas dirigée contre une décision administrative lui faisant grief ne peut qu'être écartée ;<br/>
<br/>
              12. Considérant qu'il ressort des pièces du dossier que le ministre a pris la décision ainsi révélée pour des motifs identiques à ceux de la décision du 26 juillet 2013 ; que, dès lors, et sans qu'il soit besoin d'examiner les autres moyens de la requête, cette décision est, pour les motifs exposés ci-dessus, entachée d'illégalité ; que, par suite, la société Mercedes-Benz France est également fondée à en demander l'annulation ; qu'il y a lieu d'ordonner au ministre de l'écologie, du développement durable et de l'énergie de lui délivrer les codes d'identification des types de véhicules en cause ; <br/>
<br/>
              Sur les conclusions de la société Mercedes-Benz France présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              13. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 6 000 euros à verser à la société Mercedes-Benz France au titre de ces dispositions ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : La décision du 26 juillet 2013 par laquelle le ministre de l'écologie, du développement durable et de l'énergie a refusé d'immatriculer sur le territoire français les véhicules du constructeur Daimler bénéficiant des réceptions communautaires e1*98/14*0169*19 (type 230), à l'exception de la version SZBBA200, et e1*2001/116*0470*04 (type 245G), à l'exception des versions de la variante Y2GBM2, est annulée.<br/>
<br/>
Article 2 : La décision révélée par les courriers électroniques des 19 juin et 2 juillet 2013 par laquelle le ministre de l'écologie, du développement durable et de l'énergie a refusé de communiquer à la société Mercedes-Benz France les codes nationaux d'identification des types de véhicules 230 et 245G ayant les réceptions communautaires, respectivement, e1*98/14*0169*19 et e1*2001/116*0470*04, est annulée.<br/>
<br/>
Article 3 : Il est enjoint au ministre de l'écologie, du développement durable et de l'énergie de délivrer à la société Mercedes-Benz-France les codes d'identification des types de véhicules en cause.<br/>
<br/>
Article 4 : L'Etat versera à la société Mercedes-Benz France la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 5 : La présente décision sera notifiée à la société Mercedes-Benz France et au ministre de l'écologie, du développement durable et de l'énergie.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-01-06-01-01 ACTES LÉGISLATIFS ET ADMINISTRATIFS. DIFFÉRENTES CATÉGORIES D'ACTES. ACTES ADMINISTRATIFS - CLASSIFICATION. ACTES RÉGLEMENTAIRES. PRÉSENTENT CE CARACTÈRE. - REFUS D'IMMATRICULATION DE VÉHICULES AYANT FAIT L'OBJET D'UNE RÉCEPTION CE EN CAS DE RISQUE GRAVE POUR LA SÉCURITÉ ROUTIÈRE, L'ENVIRONNEMENT OU LA SANTÉ PUBLIQUE (ART. R. 321-14 DU CODE DE LA ROUTE).
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">49-05 POLICE. POLICES SPÉCIALES. - REFUS D'IMMATRICULATION DE VÉHICULES AYANT FAIT L'OBJET D'UNE RÉCEPTION CE EN CAS DE RISQUE GRAVE POUR LA SÉCURITÉ ROUTIÈRE, L'ENVIRONNEMENT OU LA SANTÉ PUBLIQUE (ART. R. 321-14 DU CODE DE LA ROUTE) - 1) NATURE DE L'ACTE - ACTE RÉGLEMENTAIRE - 2) CONTRÔLE DU JUGE DE L'EXCÈS DE POUVOIR SUR LES MOTIFS LE JUSTIFIANT - CONTRÔLE NORMAL.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-07-02-03 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. CONTRÔLE DU JUGE DE L'EXCÈS DE POUVOIR. APPRÉCIATIONS SOUMISES À UN CONTRÔLE NORMAL. - MOTIFS D'UN REFUS D'IMMATRICULATION DE VÉHICULES AYANT FAIT L'OBJET D'UNE RÉCEPTION CE EN CAS DE RISQUE GRAVE POUR LA SÉCURITÉ ROUTIÈRE, L'ENVIRONNEMENT OU LA SANTÉ PUBLIQUE (ART. R. 321-14 DU CODE DE LA ROUTE).
</SCT>
<ANA ID="9A"> 01-01-06-01-01 La décision par laquelle le ministre chargé des transports refuse d'immatriculer des véhicules ayant fait l'objet d'une réception CE qui compromettent gravement la sécurité routière ou nuisent gravement à l'environnement ou à la santé publique, pour six mois au plus, sur le fondement de l'article R. 321-14 du code de la route, présente un caractère réglementaire.</ANA>
<ANA ID="9B"> 49-05 1) La décision par laquelle le ministre chargé des transports refuse d'immatriculer des véhicules ayant fait l'objet d'une réception CE qui compromettent gravement la sécurité routière ou nuisent gravement à l'environnement ou à la santé publique, pour six mois au plus, sur le fondement de l'article R. 321-14 du code de la route, présente un caractère réglementaire.,,,2) Le juge de l'excès de pouvoir exerce un contrôle normal sur les motifs retenus par le ministre chargé des transports pour faire usage de cette faculté.</ANA>
<ANA ID="9C"> 54-07-02-03 Le juge de l'excès de pouvoir exerce un contrôle normal sur les motifs retenus par le ministre chargé des transports pour faire usage de la faculté que lui donnent les dispositions de l'article R. 321-14 du code de la route de refuser d'immatriculer des véhicules ayant fait l'objet d'une réception CE qui compromettent gravement la sécurité routière ou nuisent gravement à l'environnement ou à la santé publique pendant une période maximale de six mois.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
