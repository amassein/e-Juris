<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028934612</ID>
<ANCIEN_ID>JG_L_2014_05_000000355988</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/93/46/CETATEXT000028934612.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère / 6ème SSR, 14/05/2014, 355988</TITRE>
<DATE_DEC>2014-05-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>355988</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère / 6ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>FOUSSARD</AVOCATS>
<RAPPORTEUR>Mme Julia Beurton</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:355988.20140514</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu l'ordonnance n° 1105890 du 3 janvier 2012, enregistrée le 19 janvier 2012 au secrétariat du contentieux du Conseil d'Etat, par laquelle le président du tribunal administratif de Grenoble a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, la requête présentée devant ce tribunal par la commune de Vienne ;<br/>
<br/>
              Vu la requête, enregistrée le 2 novembre 2011 au greffe du tribunal administratif de Grenoble, présentée par la commune de Vienne, représentée par son maire ; la commune demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté du 3 octobre 2011 du ministre des solidarités et de la cohésion sociale portant fusion des caisses d'allocations familiales de Grenoble et de Vienne ; <br/>
<br/>
              2°)  de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution, notamment les articles 34 et 37 ;<br/>
<br/>
              Vu le code de la sécurité sociale ;<br/>
<br/>
              Vu le code du travail ;<br/>
<br/>
              Vu la loi n° 2000-321 du 12 avril 2000 ;<br/>
<br/>
              Vu le décret du 31 octobre 2002 ;<br/>
<br/>
              Vu le décret n° 2005-850 du 27 juillet 2005 ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Julia Beurton, Auditeur,  <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Foussard, avocat de la caisse nationale des allocations familiales ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article R. 212-1 du code de la sécurité sociale : " La circonscription et le siège des caisses d'allocations familiales sont fixés par arrêté du ministre chargé de la sécurité sociale, compte tenu des circonscriptions territoriales des caisses primaires d'assurance maladie " ; que, sur ce fondement, le ministre des solidarités et de la cohésion sociale a, par arrêté du 3 octobre 2011, créé la caisse d'allocations familiales de l'Isère, dont la circonscription correspond à l'ensemble du département, dissous les caisses de Grenoble et de Vienne et transféré leurs biens, droits et obligations à la caisse nouvellement créée ; que la commune de Vienne demande l'annulation pour excès de pouvoir de cet arrêté ; <br/>
<br/>
              Sur l'intervention de la caisse nationale des allocations familiales :<br/>
<br/>
              2. Considérant que la caisse nationale des allocations familiales a intérêt au maintien de l'arrêté attaqué ; qu'ainsi, son intervention est recevable ; <br/>
<br/>
              Sur la légalité externe de l'arrêté attaqué :<br/>
<br/>
              En ce qui concerne la compétence du pouvoir réglementaire :<br/>
<br/>
              3. Considérant qu'aux termes de l'article 34 de la Constitution : " (...) La loi détermine les principes fondamentaux (...) de la sécurité sociale (...) " ; que si l'administration des organismes de sécurité sociale, dotés de la personnalité morale, par des représentants des employeurs et des salariés constitue l'un des principes fondamentaux de la sécurité sociale, ce principe doit être apprécié dans le cadre des limitations de portée générale qui y ont été introduites pour permettre certaines interventions jugées nécessaires de la puissance publique, en raison du caractère des activités assumées par ces organismes ; qu'en particulier, il appartient au pouvoir réglementaire de déterminer l'organisation administrative de ce service public et de délimiter les circonscriptions territoriales des organismes de sécurité sociale ; que, par suite, contrairement à ce que soutient la commune requérante, le pouvoir réglementaire était compétent pour prévoir la possibilité de déterminer, par arrêté ministériel, la circonscription et le siège des caisses d'allocations familiales et de procéder, le cas échéant, à la fusion de deux ou plusieurs caisses ; <br/>
<br/>
              En ce qui concerne la compétence du signataire de l'arrêté attaqué et les mentions de l'arrêté :<br/>
<br/>
              4. Considérant, en premier lieu, qu'il résulte des dispositions du décret du 27 juillet 2005 relatif aux délégations de signature des membres du Gouvernement que le directeur de la sécurité sociale avait, en vertu de sa nomination par un décret du 31 octobre 2002 publié au Journal officiel de la République française le 3 novembre suivant, qualité pour signer l'arrêté attaqué au nom du ministre des solidarités et de la cohésion sociale ; qu'ainsi, le moyen tiré de l'incompétence de son signataire doit être écarté ;<br/>
<br/>
              5. Considérant, en second lieu, que les dispositions du second alinéa de l'article 4 de la loi du 12 avril 2000 relative aux droits des citoyens dans leurs relations avec les administrations, aux termes desquelles : " Toute décision prise par l'une des autorités administratives mentionnées à l'article 1er comporte, outre la signature de son auteur, la mention, en caractères lisibles, du prénom, du nom et de la qualité de celui-ci ", ne peuvent être utilement invoquées à l'encontre d'un acte réglementaire ; que, par suite, le moyen tiré de ce que l'arrêté attaqué serait illégal au motif que le nom de son signataire est précédé de la seule initiale de son prénom ne peut qu'être écarté ; <br/>
<br/>
              En ce qui concerne la régularité de la procédure :<br/>
<br/>
              6. Considérant, en premier lieu, qu'aux termes des deuxième et troisième alinéas de l'article L. 217-3 du code de la sécurité sociale, applicable aux directeurs des organismes régionaux et locaux de sécurité sociale : " Le directeur de la caisse nationale nomme le directeur ou l'agent comptable après concertation avec le président du conseil d'administration de l'organisme concerné et après avis du comité des carrières institué à l'article L. 217-5. (...) / Le directeur de la caisse nationale peut mettre fin aux fonctions des directeurs et des agents comptables (...) après avoir recueilli l'avis du président du conseil d'administration de l'organisme concerné et sous les garanties, notamment de reclassement, prévues par la convention collective (...) " ; qu'aux termes de l'article L. 217-5 du même code : " Il est institué auprès de l'Union des caisses nationales, visée à l'article L. 224-5, un comité des carrières des agents de direction. / (...) Le comité des carrières émet un avis motivé sur les nominations des directeurs et des agents comptables dans les conditions prévues à l'article L. 217-3. / Dans le respect des dispositions réglementaires et conventionnelles en vigueur, le comité a pour mission de veiller à l'évolution des carrières des directeurs et des autres agents de direction et notamment à la mobilité des directeurs entre les caisses et entre les différentes branches et organismes de recouvrement du régime général (...) " ; qu'aux termes du premier alinéa de l'article R. 217-11 du même code : " (...) le directeur général ou le directeur de la caisse nationale ou de l'agence centrale (...) qui envisage, pour un motif autre que disciplinaire, de prendre une décision de cessation de fonction d'un directeur ou d'un agent comptable d'un organisme local ou régional de la branche dont relève l'organisme national, recueille préalablement l'avis du président du conseil ou du conseil d'administration de la caisse intéressée et en informe (...) le président du comité des carrières " ;<br/>
<br/>
              7. Considérant, d'une part, qu'en application de ces dispositions, si le comité des carrières doit être saisi pour émettre un avis motivé avant la nomination du directeur d'une caisse d'allocations familiales, afin d'éclairer le directeur de la caisse nationale des allocations familiales compétent pour procéder à cette nomination, sa consultation n'est, en revanche, pas requise lorsqu'il est envisagé de mettre fin aux fonctions d'un directeur ; que, par suite, le moyen tiré de ce que l'arrêté attaqué aurait été pris au terme d'une procédure irrégulière faute de consultation de ce comité doit, en tout état de cause, être écarté ; <br/>
<br/>
              8. Considérant, d'autre part, que l'arrêté attaqué n'a pas pour objet de mettre fin aux fonctions du directeur de la caisse d'allocations familiales de Vienne ; que la commune de Vienne ne peut, par conséquent, utilement soutenir que l'arrêté attaqué aurait dû être précédé de la consultation du président du conseil d'administration de la caisse d'allocations familiales de Vienne ;  <br/>
<br/>
              9. Considérant, en second lieu, qu'aux termes de l'article L. 2323-19 du code du travail : " Le comité d'entreprise est informé et consulté sur les modifications de l'organisation économique ou juridique de l'entreprise, notamment en cas de fusion (...) / L'employeur indique les motifs des modifications projetées et consulte le comité d'entreprise sur les mesures envisagées à l'égard des salariés lorsque ces modifications comportent des conséquences pour ceux-ci (...) " ; qu'en application de ces dispositions, la caisse d'allocations familiales de Vienne, fusionnée avec celle de Grenoble, devait être mise en mesure de saisir son comité d'entreprise, afin de le consulter préalablement à l'adoption de l'arrêté attaqué sur les modalités de la fusion envisagée, en lui laissant un délai suffisant pour se prononcer ; <br/>
<br/>
              10. Considérant qu'il ressort des pièces du dossier que le directeur de la caisse d'allocations familiales de Vienne a été mis à même de procéder à la consultation du comité d'entreprise de la caisse sur la fusion envisagée des caisses d'allocations familiales de Grenoble et de Vienne ; que le projet de fusion a d'ailleurs été débattu au cours de plusieurs réunions du comité d'entreprise entre janvier et août 2011 ; que, par suite, le moyen tiré de ce que le comité d'entreprise de la caisse d'allocations familiales de Vienne n'a pas été consulté préalablement à l'édiction de l'arrêté attaqué doit être écarté ; <br/>
<br/>
              En ce qui concerne la légalité interne de l'arrêté attaqué :<br/>
<br/>
              11. Considérant que la fusion des caisses d'allocations familiales de Grenoble et de Vienne répond à la volonté d'améliorer l'efficacité du réseau des caisses d'allocations familiales dans le département et s'accompagne, au demeurant, du maintien d'une antenne locale de la caisse d'allocations familiales sur la commune de Vienne afin d'assurer les services de proximité utiles aux allocataires ; que, contrairement à ce qu'affirme la commune requérante, il ne ressort pas des pièces du dossier que le ministre ait commis une erreur manifeste d'appréciation en procédant à cette fusion ;<br/>
<br/>
              12. Considérant qu'il résulte de tout ce qui précède, sans qu'il soit besoin de se prononcer sur la recevabilité de sa requête, que la commune de Vienne n'est pas fondée à demander l'annulation de l'arrêté qu'elle attaque ; que ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent, par conséquent, qu'être rejetées ; <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
 Article 1er : L'intervention de la caisse nationale des allocations familiales est admise.<br/>
<br/>
 Article 2 : La requête de la commune de Vienne est rejetée.<br/>
<br/>
Article 3 : La présente décision sera notifiée à la commune de Vienne, à la caisse nationale des allocations familiales et à la ministre des affaires sociales et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">62-01-01-01-03-01 SÉCURITÉ SOCIALE. ORGANISATION DE LA SÉCURITÉ SOCIALE. RÉGIME DE SALARIÉS. RÉGIME GÉNÉRAL. ALLOCATIONS FAMILIALES. CAISSES D'ALLOCATIONS FAMILIALES. - COMPÉTENCE DU POUVOIR RÉGLEMENTAIRE POUR PRÉVOIR LA POSSIBILITÉ DE DÉTERMINER, PAR ARRÊTÉ MINISTÉRIEL, LA CIRCONSCRIPTION ET LE SIÈGE DES CAISSES D'ALLOCATIONS FAMILIALES ET DE PROCÉDER, LE CAS ÉCHÉANT, À LA FUSION DE DEUX OU PLUSIEURS CAISSES - EXISTENCE [RJ1].
</SCT>
<ANA ID="9A"> 62-01-01-01-03-01 Si l'administration des organismes de sécurité sociale, dotés de la personnalité morale, par des représentants des employeurs et des salariés, constitue l'un des principes fondamentaux de la sécurité sociale, ce principe doit être apprécié dans le cadre des limitations de portée générale qui y ont été introduites pour permettre certaines interventions jugées nécessaires de la puissance publique, en raison du caractère des activités assumées par ces organismes. En particulier, il appartient au pouvoir réglementaire de déterminer l'organisation administrative de ce service public et de délimiter les circonscriptions territoriales des organismes de sécurité sociale. Par suite, le pouvoir réglementaire est compétent pour prévoir la possibilité de déterminer, par arrêté ministériel, la circonscription et le siège des caisses d'allocations familiales et procéder, le cas échéant, à la fusion de deux ou plusieurs caisses.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., pour les URSSAF, décision du même jour, CE, 14 mai 2014, Fédération nationale CGT des personnels des organismes sociaux, n° 354634, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
