<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041688020</ID>
<ANCIEN_ID>JG_L_2020_03_000000423443</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/68/80/CETATEXT000041688020.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 04/03/2020, 423443</TITRE>
<DATE_DEC>2020-03-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>423443</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, TEXIDOR, PERIER ; HAAS</AVOCATS>
<RAPPORTEUR>M. Thomas Pez-Lavergne</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Mireille Le Corre</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:423443.20200304</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
Procédure contentieuse antérieure  <br/>
<br/>
              La société Savima a demandé au tribunal administratif de la Guadeloupe d'annuler le titre de recettes n° 212477 émis par l'ordonnateur du centre hospitalier de Capesterre-Belle-Eau et rendu exécutoire le 4 décembre 2012, par lequel elle a été faite débitrice de la somme de 446 207,09 euros correspondant au montant de l'avance forfaitaire qui lui avait été versée pour l'exécution en sa qualité de sous-traitante agréée du lot 4-4 du marché de conception-réalisation du nouvel hôpital local. Par un jugement n° 1300102 du 19 novembre 2015, le tribunal administratif de la Guadeloupe a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 16BX00626 du 21 juin 2018, la cour administrative d'appel de Bordeaux a rejeté l'appel formé par la société Savima contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 21 août et 21 novembre 2018 et le 15 octobre 2019 au secrétariat du contentieux du Conseil d'Etat, la société Savima demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge du centre hospitalier de Capesterrre-Belle-Eau la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code civil ;<br/>
              - le code des marchés publics ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Thomas Pez-Lavergne, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Mireille Le Corre, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Texidor, Perier, avocat de la société Savima et à Me Haas, avocat du centre hospitalier de Capesterre-Belle-Eau ;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, dans le cadre d'un marché de conception-réalisation passé entre le centre hospitalier de la commune de Capesterre-Belle-Eau (Guadeloupe) et la société Alfa Bâtiment, agissant en qualité de mandataire solidaire d'un groupement conjoint d'entreprises, pour la construction d'un nouvel hôpital local, le pouvoir adjudicateur a, par un acte spécial du 16 décembre 2008, accepté la société Savima en qualité de sous-traitant pour l'exécution d'une partie du lot 4-4 " Menuiserie extérieure brise soleil " et agréé ses conditions de paiement pour un montant maximum de 2 056 253,86 euros HT. Conformément à sa demande, cette société a obtenu une avance forfaitaire de 20 % du montant des travaux sous-traités, soit la somme de 446 207,09 euros TTC. A la suite de la cession partielle, au profit de la société Saint Landry, des actifs de la société Alfa Bâtiment relatifs au chantier du nouvel hôpital, mise en redressement judiciaire, le centre hospitalier a constaté l'absence de reprise du chantier. Par un courrier du 31 août 2011, le directeur du centre hospitalier a informé la société Savima de la résiliation du marché aux torts de la société Saint Landry, survenue le 10 juin 2011. Par un titre de recettes émis et rendu exécutoire le 4 décembre 2012, le centre hospitalier a réclamé à la société Savima la somme de 446 207,09 euros TTC, correspondant au remboursement de l'avance forfaitaire sur travaux qui lui avait été versée. Par un arrêt du 21 juin 2018 contre lequel la société Savima se pourvoit en cassation, la cour administrative d'appel de Bordeaux a rejeté l'appel que la société avait formé contre le jugement du tribunal administratif de la Guadeloupe rejetant sa demande tendant à l'annulation de ce titre exécutoire.<br/>
<br/>
              2. Les avances accordées et versées au titulaire d'un marché sur le fondement des dispositions de l'article 87 du code des marchés publics, applicable au litige, ont pour objet de lui fournir une trésorerie suffisante destinée à assurer le préfinancement de l'exécution des prestations qui lui ont été confiées. Le principe et les modalités de leur remboursement sont prévus par les dispositions de l'article 88 de ce code, dont la substance a été reprise aux articles R. 2191-11 et R. 2191-12 du code de la commande publique, qui permettent au maître d'ouvrage d'imputer le remboursement des avances par précompte sur les sommes dues au titulaire du marché à titre d'acomptes, de règlement partiel définitif ou de solde. L'article 115 du même code, dont la substance a été reprise aux articles R. 2193-17 et suivants du code de la commande publique, prévoit que ces dispositions s'appliquent aux sous-traitants bénéficiaires du paiement direct. Il résulte de la combinaison de ces dispositions que lorsque le marché est résilié avant que l'avance puisse être remboursée par précompte sur les prestations dues, le maître d'ouvrage peut obtenir le remboursement de l'avance versée au titulaire du marché ou à son sous-traitant sous réserve des dépenses qu'ils ont exposées et qui correspondent à des prestations prévues au marché et effectivement réalisées. En cas de résiliation pour faute du marché, le remboursement de l'avance par le sous-traitant ne fait pas obstacle à ce que celui-ci engage une action contre le titulaire du marché et lui demande, le cas échéant, réparation du préjudice que cette résiliation lui a causé à raison des dépenses engagées en vue de l'exécution de prestations prévues initialement au marché.<br/>
<br/>
              3. Il ressort des énonciations de l'arrêt attaqué que la cour a tout d'abord estimé que le maître d'ouvrage ne pouvait en l'espèce obtenir le remboursement de l'avance qu'il avait versée à la société sous-traitante par précompte sur les sommes dues au sous-traitant, sur le fondement des dispositions des articles 88 et 115 du code des marchés publics alors applicable et de l'article 6.3 du cahier des clauses administratives particulières, dès lors que cette société n'avait pas exécuté, ne serait-ce que partiellement, les prestations qui lui avaient été confiées. La cour administrative d'appel a ensuite estimé que, dans les circonstances de l'espèce, les conditions de la répétition d'un indu n'étaient pas réunies mais que le centre hospitalier pouvait, pour émettre le titre de recettes en litige, se fonder sur la théorie de l'enrichissement sans cause. Ce faisant, la cour a commis une erreur de droit dès lors que, ainsi qu'il a été dit au point 2, le fondement du remboursement des avances par le sous-traitant, à raison d'une absence totale ou partielle de réalisation de ses prestations, repose sur les articles 88 et 115 du code des marchés publics applicable au litige alors même que le marché résilié n'aurait pas été exécuté.<br/>
<br/>
              4. Il résulte de ce qui précède que la société Savima est fondée à demander l'annulation de l'arrêt attaqué.<br/>
<br/>
              5.  Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du centre hospitalier de Capesterre-Belle-Eau la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. En revanche, les mêmes dispositions font obstacle à ce que soit mise à la charge de la société Savima une somme à ce titre.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 21 juin 2018 de la cour administrative d'appel de Bordeaux est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Bordeaux. <br/>
Article 3 : Le centre hospitalier de Capesterre-Belle-Eau versera à la société Savima la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions présentées par le centre hospitalier de Capesterre-Belle-Eau au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à la société Savima et au centre hospitalier de Capesterre-Belle-Eau.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-04-02-02 MARCHÉS ET CONTRATS ADMINISTRATIFS. FIN DES CONTRATS. RÉSILIATION. EFFETS. - REMBOURSEMENT DES AVANCES VERSÉES - 1) POSSIBILITÉ D'OBTENIR LE REMBOURSEMENT AUPRÈS DU TITULAIRE DU MARCHÉ ET DU SOUS-TRAITANT BÉNÉFICIAIRE DU PAIEMENT DIRECT, SOUS RÉSERVE DES DÉPENSES EXPOSÉES PAR EUX - 2) RÉSILIATION POUR FAUTE - RECOURS POSSIBLE DU SOUS-TRAITANT CONTRE LE TITULAIRE DU MARCHÉ.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">39-05-01 MARCHÉS ET CONTRATS ADMINISTRATIFS. EXÉCUTION FINANCIÈRE DU CONTRAT. RÉMUNÉRATION DU CO-CONTRACTANT. - AVANCES - REMBOURSEMENT - 1) CAS GÉNÉRAL - A) IMPUTATION PAR LE MAÎTRE D'OUVRAGE SUR LES SOMMES DUES AU TITULAIRE DU MARCHÉ - B) APPLICATION AUX SOUS-TRAITANTS BÉNÉFICIAIRES DU PAIEMENT DIRECT - 2) CAS DU MARCHÉ RÉSILIÉ AVANT QUE L'AVANCE PUISSE ÊTRE REMBOURSÉE PAR PRÉCOMPTE - A) POSSIBILITÉ D'OBTENIR REMBOURSEMENT AUPRÈS DU TITULAIRE OU DU SOUS-TRAITANT, SOUS RÉSERVE DES DÉPENSES EXPOSÉES PAR EUX - B) RÉSILIATION POUR FAUTE - RECOURS POSSIBLE DU SOUS-TRAITANT CONTRE LE TITULAIRE DU MARCHÉ.
</SCT>
<ANA ID="9A"> 39-04-02-02 Les avances accordées et versées au titulaire d'un marché sur le fondement de l'article 87 du code des marchés publics (CMP) ont pour objet de lui fournir une trésorerie suffisante destinée à assurer le préfinancement de l'exécution des prestations qui lui ont été confiées. Le principe et les modalités de leur remboursement sont prévus par les dispositions de l'article 88 de ce code, dont la substance a été reprise aux articles R. 2191-11 et R. 2191-12 du code de la commande publique (CCP), qui permettent au maître d'ouvrage d'imputer le remboursement des avances par précompte sur les sommes dues au titulaire du marché à titre d'acomptes, de règlement partiel définitif ou de solde. L'article 115 du CMP, dont la substance a été reprise aux articles R. 2193-17 et suivants du CCP, prévoit que ces dispositions s'appliquent aux sous-traitants bénéficiaires du paiement direct.... ,,1) Il résulte de la combinaison de ces articles que, lorsque le marché est résilié avant que l'avance puisse être remboursée par précompte sur les prestations dues, le maître d'ouvrage peut obtenir le remboursement de l'avance versée au titulaire du marché ou à son sous-traitant sous réserve des dépenses qu'ils ont exposées et qui correspondent à des prestations prévues au marché et effectivement réalisées.... ,,2) En cas de résiliation pour faute du marché, le remboursement de l'avance par le sous-traitant ne fait pas obstacle à ce que celui-ci engage une action contre le titulaire du marché et lui demande, le cas échéant, réparation du préjudice que cette résiliation lui a causé à raison des dépenses engagées en vue de l'exécution de prestations prévues initialement au marché.</ANA>
<ANA ID="9B"> 39-05-01 Les avances accordées et versées au titulaire d'un marché sur le fondement de l'article 87 du code des marchés publics (CMP) ont pour objet de lui fournir une trésorerie suffisante destinée à assurer le préfinancement de l'exécution des prestations qui lui ont été confiées.... ,,1) a) Le principe et les modalités de leur remboursement sont prévus par les dispositions de l'article 88 de ce code, dont la substance a été reprise aux articles R. 2191-11 et R. 2191-12 du code de la commande publique (CCP), qui permettent au maître d'ouvrage d'imputer le remboursement des avances par précompte sur les sommes dues au titulaire du marché à titre d'acomptes, de règlement partiel définitif ou de solde.... ,,b) L'article 115 du CMP, dont la substance a été reprise aux articles R. 2193-17 et suivants du CCP, prévoit que ces dispositions s'appliquent aux sous-traitants bénéficiaires du paiement direct.... ,,2) a) Il résulte de la combinaison de ces articles que, lorsque le marché est résilié avant que l'avance puisse être remboursée par précompte sur les prestations dues, le maître d'ouvrage peut obtenir le remboursement de l'avance versée au titulaire du marché ou à son sous-traitant sous réserve des dépenses qu'ils ont exposées et qui correspondent à des prestations prévues au marché et effectivement réalisées.... ,,b) En cas de résiliation pour faute du marché, le remboursement de l'avance par le sous-traitant ne fait pas obstacle à ce que celui-ci engage une action contre le titulaire du marché et lui demande, le cas échéant, réparation du préjudice que cette résiliation lui a causé à raison des dépenses engagées en vue de l'exécution de prestations prévues initialement au marché.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
