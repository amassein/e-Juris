<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000024615271</ID>
<ANCIEN_ID>JG_L_2011_09_000000336249</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/24/61/52/CETATEXT000024615271.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 30/09/2011, 336249</TITRE>
<DATE_DEC>2011-09-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>336249</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Christian Vigouroux</PRESIDENT>
<AVOCATS>SCP CELICE, BLANCPAIN, SOLTNER ; SCP FABIANI, LUC-THALER</AVOCATS>
<RAPPORTEUR>M. Rémi Decout-Paolini</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Claire Landais</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2011:336249.20110930</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 4 février et 28 avril 2010 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. et Mme Christian B, demeurant ... ; M. et Mme B demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 08MA02704 du 4 décembre 2009 par lequel la cour administrative d'appel de Marseille, à la demande de M. et Mme A, d'une part, a annulé le jugement n° 0507889 du 13 mars 2008 par lequel le tribunal administratif de Marseille a annulé, à la demande des auteurs du pourvoi, l'arrêté du 24 mars 2005 par lequel le maire de Gignac-la-Nerthe (13180) a accordé à M. et Mme A un permis de construire une maison à usage d'habitation, d'autre part, a rejeté la demande à fin d'annulation de ce permis présentée devant le tribunal administratif de Marseille ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de M. et Mme A ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Gignac-la-Nerthe et de <br/>
M. et Mme A le versement de la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'urbanisme ;<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Rémi Decout-Paolini, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Célice, Blancpain, Soltner, avocat de M. et Mme B et de la SCP Fabiani, Luc-Thaler, avocat de M. et Mme A, <br/>
<br/>
              - les conclusions de Mme Claire Landais, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Célice, Blancpain, Soltner, avocat de M. et Mme B et à la SCP Fabiani, Luc-Thaler, avocat de M. et Mme A ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant qu'aux termes de l'article UB 7 du règlement du plan d'occupation des sols de la commune de Gignac-la-Nerthe, relatif à l'implantation des constructions par rapport aux limites séparatives : " 1. Implantation par rapport aux limites séparatives aboutissant aux voies. / En bordure des voies, les constructions doivent être édifiées en ordre continu d'une limite séparative à l'autre. / (...). 2. Implantation par rapport aux limites de fonds de propriété. Sauf création de la servitude prévue à l'article L. 451-1 du code de l'urbanisme, la distance comptée horizontalement de tout point d'une construction au point le plus proche d'une limite séparative n'aboutissant pas aux voies doit être au moins égale à la moitié de la différence d'altitude entre ces deux points, sans être inférieure à trois mètres (3 m). " ; que, pour l'application de ces dispositions, les limites séparatives s'entendent comme les limites entre la propriété constituant le terrain d'assiette de la construction et la ou les propriétés qui la jouxtent ; que la limite entre deux propriétés situées en bordure d'une même voie doit être regardée comme une limite séparative aboutissant à cette voie ; que la circonstance qu'une telle limite séparative soit constituée de plusieurs segments de droite faisant angle entre eux est sans influence sur sa qualification de limite séparative aboutissant aux voies ;<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que le terrain d'assiette de la construction litigieuse envisagée par M. et Mme A, constitué des parcelles n° 23 et 24, présente six côtés, dont l'un borde le chemin du Billard et l'autre jouxte au nord-ouest la parcelle n° 22, également propriété des époux A ; que ce terrain est entouré, sur ses quatre autres côtés, par la propriété de M. et Mme B, constituée de la parcelle n° 27 ; qu'ainsi, pour l'application des dispositions de l'article UB 7 du règlement du plan d'occupation des sols de la commune, le terrain d'assiette de la construction en litige comporte exclusivement deux limites séparatives aboutissant à la voie publique, dont l'une est formée de ces quatre côtés, et ne comporte donc pas de limite de fond de propriété ; que, pour annuler le jugement du tribunal administratif de Marseille qui, à la demande de M. et Mme B, avait annulé l'arrêté du 24 mars 2005 par lequel le maire de la commune de Gignac-la-Nerthe avait accordé à M. et Mme A le permis de construire en litige, la cour administrative d'appel de Marseille a jugé que c'est à tort que le tribunal avait fait application des dispositions du paragraphe 2 de l'article UB 7 du règlement du POS, dès lors que ces dispositions ne sont pas applicables au projet ; qu'il résulte de ce qui a été dit ci-dessus que M. et Mme B ne sont pas fondés à soutenir qu'en statuant ainsi la cour aurait commis une erreur de droit ; que dès lors qu'ils ne pouvaient utilement soutenir en défense que le projet de construction méconnaissait les dispositions du paragraphe 2 de l'article UB 7 en ce qu'il n'était pas implanté à plus de 3 mètres de la limite nord-est entre les propriétés, la cour n'était pas tenue de répondre à ce moyen ; qu'il résulte de tout ce qui précède que le pourvoi de M. et Mme B ne peut qu'être rejeté ;<br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, sur le fondement des dispositions de l'article L. 761-1 du code de justice administrative, de mettre à la charge de M. et Mme B le versement à M. et Mme A de la somme de 3 000 euros ; que ces dispositions font obstacle à ce que soit mis à la charge de M. et Mme A, qui ne sont pas, dans la présente instance, la partie perdante, le versement à M. et Mme B d'une somme au titre des frais exposés par ces derniers et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. et Mme B est rejeté.<br/>
Article 2 : M. et Mme B verseront à M. et Mme A une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à M. et Mme Christian B, à M. et Mme Patrick A et à la commune de Gignac-la-Nerthe.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">68-01-01-02-02-07 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PLANS D'AMÉNAGEMENT ET D'URBANISME. PLANS D'OCCUPATION DES SOLS ET PLANS LOCAUX D'URBANISME. APPLICATION DES RÈGLES FIXÉES PAR LES POS OU LES PLU. RÈGLES DE FOND. IMPLANTATION DES CONSTRUCTIONS PAR RAPPORT AUX LIMITES SÉPARATIVES. - NOTION DE LIMITE DE FOND DE PARCELLE - EXCLUSION - PARCELLE QUADRILATÈRE BORDANT UNE VOIE NE COMPORTANT QUE DEUX LIMITES SÉPARATIVES AVEC LES PROPRIÉTÉS VOISINES [RJ1].
</SCT>
<ANA ID="9A"> 68-01-01-02-02-07 Une parcelle quadrilatère bordant la voie publique qui ne comporte que deux limites séparatives avec les propriétés voisines ne comporte pas de limite de fond de propriété. En conséquence, les règles du plan d'occupation des sols (POS) relatives aux limites de fond de propriété lui sont inapplicables.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr. CE, 24 janvier 1994, Association Robertsauvienne pour la promotion de l'environnement et autres, n°s 132209,132225,132226,132486,132573, T. pp. 1238-1258. Comp. 11 mars 1987, Commune du Pouliguen c/ Bronner et autres, n° 69894, inédite au Recueil ; CE, 26 janvier 1994, Ville de Rennes, n° 133683, T. pp. 1238-1258.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
