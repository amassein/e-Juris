<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038351083</ID>
<ANCIEN_ID>JG_L_2019_04_000000412701</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/35/10/CETATEXT000038351083.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 05/04/2019, 412701</TITRE>
<DATE_DEC>2019-04-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>412701</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GASCHIGNARD</AVOCATS>
<RAPPORTEUR>Mme Pauline Berne</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:412701.20190405</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              La commune de Tinchebray-Bocage a demandé au tribunal administratif de Caen d'annuler pour excès de pouvoir la décision du 12 juin 2015 de la préfète de l'Orne fixant le montant de dotation nationale de péréquation qui lui était attribué pour l'année 2015. Par un jugement n° 1502304 du 24 mars 2016, le tribunal administratif de Caen a fait droit à cette demande.<br/>
<br/>
              Par un arrêt n° 16NT01707 du 24 mai 2017, la cour administrative d'appel de Nantes a rejeté l'appel formé par le ministre de l'intérieur contre ce jugement.<br/>
<br/>
              Par un pourvoi enregistré le 24 juillet 2017 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'intérieur demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des collectivités territoriales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Pauline Berne, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gaschignard, avocat de la commune de Tinchebray-Bocage ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la commune nouvelle de Tinchebray-Bocage, créée au 1er janvier 2015 à la suite du regroupement de sept communes, a demandé au tribunal administratif de Caen d'annuler pour excès de pouvoir la décision du 12 juin 2015 de la préfète de l'Orne fixant le montant de dotation nationale de péréquation qui lui était attribué pour l'année 2015 au motif qu'il lui avait fait application, à tort selon elle, de la règle de plafonnement prévue par le VI de l'article L. 2334-14-1 du code général des collectivités territoriales. Par un jugement du 24 mars 2016, le tribunal a fait droit à cette demande. Par un arrêt du 24 mai 2017, la cour administrative d'appel de Nantes a rejeté l'appel formé par le ministre de l'intérieur contre ce jugement. Le ministre se pourvoit en cassation contre cet arrêt.<br/>
<br/>
              2. Aux termes de l'article L. 2334-14-1 du code général des collectivités territoriales, dans sa version applicable au litige : " I. La dotation nationale de péréquation comprend une part principale et une majoration. / II. Cette dotation est répartie entre les communes dans les conditions précisées aux III, IV, V et VI (...) / VI. A compter de 2012, l'attribution au titre de la part principale ou de la part majoration de la dotation nationale de péréquation revenant à une commune éligible ne peut être ni inférieure à 90 %, ni supérieure à 120 % du montant perçu l'année précédente ". Aux termes de l'article L. 2113-22 du même code : " Les communes nouvelles sont éligibles aux dotations de péréquation communale dans les conditions de droit commun. (...) Au cours des trois années suivant leur création, les communes nouvelles créées au plus tard le 1er janvier 2016 et regroupant soit une population inférieure ou égale à 10 000 habitants, soit toutes les communes membres d'un ou de plusieurs établissements publics de coopération intercommunale à fiscalité propre, perçoivent des attributions au titre des deux parts de la dotation nationale de péréquation, de la dotation de solidarité urbaine et de cohésion sociale et de la dotation de solidarité rurale au moins égales aux attributions perçues au titre de chacune de ces dotations par les anciennes communes l'année précédant la création de la commune nouvelle ". <br/>
<br/>
              3. Il résulte de ces dispositions que les communes nouvelles créées au plus tard le 1er janvier 2016 et regroupant soit une population inférieure ou égale à 10 000 habitants, soit toutes les communes membres d'un ou de plusieurs établissements publics de coopération intercommunale à fiscalité propre, sont éligibles aux dotations de péréquation communale dans les conditions de droit commun fixées notamment par l'article L. 2334-14-1 du code général des collectivités territoriales. Par conséquent, elles sont en principe soumises au dispositif d'encadrement de l'évolution de la dotation nationale de péréquation d'une année à l'autre prévu par le VI de cet article. Toutefois, d'une part, ce dispositif ne peut trouver à s'appliquer aux communes nouvelles l'année de leur création, dès lors que celles-ci n'ont pu, en tant que telles, percevoir de dotation de péréquation l'année précédente et, d'autre part, ce dispositif ne saurait avoir pour effet de priver les communes nouvelles de la garantie du maintien des dotations au cours des trois années suivant leur création prévue par l'article L. 2113-22 du code général des collectivités territoriales.<br/>
<br/>
              4. Par suite, en jugeant que les dispositions de l'article L. 2113-22 du code général des collectivités territoriales excluaient toute application des dispositions du VI de l'article L. 2334-14-1 aux communes nouvelles pendant les trois ans qui suivent leur création, la cour a commis une erreur de droit. <br/>
<br/>
              5. Toutefois, le motif énoncé au point 3, qui répond à un moyen invoqué devant les juges du fond et dont l'examen n'implique l'appréciation d'aucune circonstance de fait, doit être substitué au motif erroné en droit retenu par l'arrêt attaqué, dont il justifie le dispositif dès lors qu'il implique qu'en 2015, année de sa création, la commune nouvelle de Tinchebray-Bocage n'était pas soumise au plafond de 120 % prévu par l'article L. 2334-14-1 du code général des collectivités territoriales. <br/>
<br/>
              6. Il résulte de tout ce qui précède que le ministre de l'intérieur n'est pas fondé à demander l'annulation de l'arrêt qu'il attaque. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement d'une somme de 3 000 euros à la commune de Tinchebray-Bocage en application de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le pourvoi du ministre de l'intérieur est rejeté.<br/>
Article 2 : L'Etat versera à la commune de Tinchebray-Bocage une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée au ministre de l'intérieur et à la commune de Tinchebray-Bocage.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">135-02-04-03-03 COLLECTIVITÉS TERRITORIALES. COMMUNE. FINANCES COMMUNALES. RECETTES. DOTATIONS. - DOTATION DE NATIONALE DE PÉRÉQUATION (ART. L. 2334-14-1 DU CGCT) - DISPOSITIF D'ENCADREMENT DE L'ÉVOLUTION ANNUELLE (VI DE CET ARTICLE) - APPLICATION AUX COMMUNES NOUVELLES (ART. L. 2113-22 DU CGCT) -  MODALITÉS.
</SCT>
<ANA ID="9A"> 135-02-04-03-03 Il résulte des articles L. 2334-14-1 et L. 2113-22 du code général des collectivités territoriales (CGCT) que les communes nouvelles créées au plus tard le 1er janvier 2016 et regroupant soit une population inférieure ou égale à 10 000 habitants, soit toutes les communes membres d'un ou de plusieurs établissements publics de coopération intercommunale à fiscalité propre, sont éligibles aux dotations de péréquation communale dans les conditions de droit commun fixées notamment par l'article L. 2334-14-1. Par conséquent, elles sont en principe soumises au dispositif d'encadrement de l'évolution de la dotation nationale de péréquation d'une année à l'autre prévu par le VI de cet article. Toutefois, d'une part, ce dispositif ne peut trouver à s'appliquer aux communes nouvelles l'année de leur création, dès lors que celles-ci n'ont pu, en tant que telles, percevoir de dotation de péréquation l'année précédente et, d'autre part, ce dispositif ne saurait avoir pour effet de priver les communes nouvelles de la garantie du maintien des dotations au cours des trois années suivant leur création prévue par l'article L. 2113-22.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
