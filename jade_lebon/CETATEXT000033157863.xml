<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033157863</ID>
<ANCIEN_ID>JG_L_2016_09_000000398231</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/15/78/CETATEXT000033157863.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 21/09/2016, 398231</TITRE>
<DATE_DEC>2016-09-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>398231</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ ; SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>M. François Lelièvre</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:398231.20160921</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société de transports urbains du Valenciennois (TUV) a demandé au juge des référés du tribunal administratif de Lille, sur le fondement des dispositions de l'article L. 521-1 du code de justice administrative, de suspendre l'exécution de la délibération du 16 décembre 2015 par laquelle le comité syndical du syndicat intercommunal pour les transports urbains de la région de Valenciennes (SITURV) a approuvé l'annulation du protocole d'accord qu'il avait conclu avec la société TUV le 21 janvier 2014. Par une ordonnance n° 1601325 du 10 mars 2016, le juge des référés du tribunal administratif de Lille a fait droit à cette demande.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 25 mars, 31 mars et 9 juin 2016 au secrétariat du contentieux du Conseil d'Etat, le SITURV demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ; <br/>
<br/>
              2°) réglant l'affaire en référé, de rejeter la demande de la société TUV ;<br/>
<br/>
              3°) de mettre à la charge de la société TUV la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code civil ;<br/>
              - le code général des collectivités territoriales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. François Lelièvre, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat du syndicat intercommunal pour les transports urbains de la région de Valenciennes et à la SCP Foussard, Froger, avocat de la société de transports urbains du Valenciennois ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ; que la condition d'urgence à laquelle est subordonné le prononcé d'une mesure de suspension doit être regardée comme remplie lorsque la décision administrative contestée préjudicie de manière suffisamment grave et immédiate à un intérêt public, à la situation du  requérant ou aux intérêts qu'il entend défendre ; qu'il en va ainsi, alors même que cette décision n'aurait un objet ou des répercussions que purement financiers et que, en cas d'annulation, ses effets pourraient être effacés par une réparation pécuniaire ;<br/>
<br/>
              2. Considérant que par l'ordonnance attaquée et à la demande de la société des transports urbains du Valenciennois (TUV), le juge des référés du tribunal administratif de Lille a, sur le fondement des dispositions de l'article L. 521-1 du code de justice administrative, suspendu l'exécution de la délibération du 16 décembre 2015 par laquelle le comité syndical du syndicat intercommunal pour les transports urbains de la région de Valenciennes (SITURV) a approuvé l'annulation du protocole d'accord transactionnel qu'il avait conclu avec la société TUV le 21 janvier 2014 à la suite de sa décision de substituer une ligne de tramway au service de trolleybus dont il avait confié l'exploitation à cette société par une convention de délégation de service public conclue le 18 novembre 2009 ; <br/>
<br/>
              3. Considérant, en premier lieu, que le juge des référés, statuant sur le fondement des dispositions de l'article L. 521-1 du code de justice administrative, qui a suffisamment motivé son ordonnance, n'a pas entaché celle-ci d'irrégularité en se fondant sur des éléments apportés par la société TUV au cours de l'audience publique sans rouvrir l'instruction à l'issue de celle-ci ;<br/>
<br/>
              4. Considérant, en deuxième lieu, que pour juger que la condition d'urgence était remplie, le juge des référés ne s'est pas fondé sur la circonstance que l'annulation, par la délibération attaquée, du contrat de transaction aurait préjudicié de manière suffisamment grave et immédiate à un intérêt public, compte tenu de l'autorité de la chose jugée qui s'attache aux transactions en vertu des dispositions de l'article 2052 du code civil, mais sur les répercussions financières de la délibération attaquée pour la société TUV ; que, par suite, le moyen tiré de ce qu'en se fondant sur une telle circonstance, le juge des référés aurait commis une erreur de droit est inopérant ;<br/>
<br/>
              5. Considérant, en troisième lieu, qu'en raison de l'autonomie juridique et financière dont une société dispose comme toute personne morale, il n'appartient pas, en principe, au juge des référés, lorsqu'une décision a des répercussions financières sur une société, de tenir compte des capacités financières de ses actionnaires ou de son appartenance à un groupe pour apprécier si cette décision préjudicie de manière suffisamment grave et immédiate à sa situation ; que devant le tribunal administratif, le SITURV se bornait à faire valoir que la société TUV appartenait à un groupe doté d'une solide assise financière ; qu'il ne soutenait pas que cette filiale serait fictive ou que son patrimoine se serait confondu avec celui de sa maison-mère ; qu'il suit de là qu'en estimant que la délibération attaquée portait atteinte de manière suffisamment grave et immédiate aux intérêts, notamment financiers, de la société TUV sans prendre en compte son appartenance à un groupe, le juge des référés n'a pas commis d'erreur de droit ; <br/>
<br/>
              6. Considérant, en dernier lieu, qu'en jugeant que le moyen tiré de ce que le SITURV n'était pas compétent pour prononcer l'annulation unilatérale du protocole d'accord transactionnel du 21 janvier 2014 était propre, en l'état de l'instruction, à créer un doute sérieux quant à la légalité de la délibération du 16 décembre 2015 du comité syndical du SITURV, le juge des référés n'a pas entaché son ordonnance d'erreur de droit ;<br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que le pourvoi du SITURV doit être rejeté, y compris les conclusions présentées sur le fondement de l'article L. 761-1 du code de justice administrative ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du SITURV le versement d'une somme de 3 000 euros à la société TUV au titre des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi du syndicat intercommunal pour les transports urbains de la région de Valenciennes est rejeté.<br/>
Article 2 : Le syndicat intercommunal pour les transports urbains de la région de Valenciennes versera la somme de 3 000 euros à la société des transports urbains du Valenciennois en application des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée au syndicat intercommunal pour les transports urbains de la région de Valenciennes et à la société des transports urbains du Valenciennois.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-035-02-03-02 PROCÉDURE. PROCÉDURES INSTITUÉES PAR LA LOI DU 30 JUIN 2000. RÉFÉRÉ SUSPENSION (ART. L. 521-1 DU CODE DE JUSTICE ADMINISTRATIVE). CONDITIONS D'OCTROI DE LA SUSPENSION DEMANDÉE. URGENCE. - APPRÉCIATION DES CONSÉQUENCES FINANCIÈRES D'UNE DÉCISION SUR UNE SOCIÉTÉ - PRISE EN COMPTE DES CAPACITÉS DE SES ACTIONNAIRES OU DE SON APPARTENANCE À UN GROUPE - ABSENCE EN PRINCIPE.
</SCT>
<ANA ID="9A"> 54-035-02-03-02 En raison de l'autonomie juridique et financière dont une société dispose comme toute personne morale, il n'appartient pas, en principe, au juge des référés, lorsqu'une décision a des répercussions financières sur une société, de tenir compte des capacités financières de ses actionnaires ou de son appartenance à un groupe pour apprécier si cette décision préjudicie de manière suffisamment grave et immédiate à sa situation.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
