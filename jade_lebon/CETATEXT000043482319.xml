<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043482319</ID>
<ANCIEN_ID>JG_L_2021_05_000000445305</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/48/23/CETATEXT000043482319.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 05/05/2021, 445305, Publié au recueil Lebon</TITRE>
<DATE_DEC>2021-05-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>445305</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELAMARRE, JEHANNIN</AVOCATS>
<RAPPORTEUR>M. David Moreau</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Domingo</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:445305.20210505</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
      Par une protestation et un mémoire en réplique, enregistrés les 13 octobre et 16 décembre 2020 au secrétariat du contentieux du Conseil d'Etat, Mme E... F..., Mme G... H..., Mme C... D... et M. A... B... demandent au Conseil d'Etat :<br/>
<br/>
      1°) d'annuler les opérations électorales qui se sont déroulées dans les bureaux de vote nos 28, 29, 37, 43, 44 et 50 de la commune de Nouméa, nos 27 et 29 de la commune de Lifou et n° 5 de la commune du Mont-Dore pour la consultation sur l'accession à la pleine souveraineté de la Nouvelle-Calédonie qui s'est tenue le 4 octobre 2020 ;<br/>
<br/>
      2°) de modifier en conséquence la décision de la commission de contrôle de l'organisation et du déroulement de la consultation sur l'accession à la pleine souveraineté de la Nouvelle-Calédonie arrêtant le résultat de cette consultation ;<br/>
<br/>
      3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
      Vu les autres pièces du dossier ;<br/>
<br/>
      Vu :<br/>
      - la Constitution, notamment ses articles 76 et 77 ; <br/>
      - l'accord sur la Nouvelle-Calédonie signé à Nouméa le 5 mai 1998;<br/>
      - la loi n° 99-209 organique du 19 mars 1999 ;<br/>
      - le code électoral ;<br/>
      - le décret n° 2020-776 du 24 juin 2020 ;<br/>
      - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
       Après avoir entendu en séance publique :<br/>
<br/>
       - le rapport de M. David Moreau, maître des requêtes,  <br/>
<br/>
       - les conclusions de M. Laurent Domingo, rapporteur public ;<br/>
<br/>
       La parole ayant été donnée, après les conclusions, à la SCP Delamarre, Jéhannin, avocat, de Mme E... F..., de Mme G... H..., de Mme C... D... et de M. A... B... ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article 77 de la Constitution : " Après approbation de l'accord lors de la consultation prévue à l'article 76, la loi organique, prise après avis de l'assemblée délibérante de la Nouvelle-Calédonie, détermine, pour assurer l'évolution de la Nouvelle-Calédonie dans le respect des orientations définies par cet accord et selon les modalités nécessaires à sa mise en oeuvre : (...) les conditions et les délais dans lesquels les populations intéressées de la Nouvelle-Calédonie seront amenées à se prononcer sur l'accession à la pleine souveraineté ". <br/>
<br/>
              2. Aux termes de l'article 216 de la loi organique du 19 mars 1999 relative à la Nouvelle-Calédonie : " I. - La consultation sur l'accession à la pleine souveraineté prévue par l'article 77 de la Constitution est organisée conformément aux dispositions du présent titre. II. - Les électeurs sont convoqués par décret en conseil des ministres, après consultation du gouvernement et du congrès de la Nouvelle-Calédonie (...) ". Aux termes de l'article 217 de la même loi organique : " La consultation est organisée au cours du mandat du congrès qui commencera en 2014 (...) / Si la majorité des suffrages exprimés conclut au rejet de l'accession à la pleine souveraineté, une deuxième consultation sur la même question peut être organisée à la demande écrite du tiers des membres du congrès, adressée au haut-commissaire et déposée à partir du sixième mois suivant le scrutin. La nouvelle consultation a lieu dans les dix-huit mois suivant la saisine du haut-commissaire à une date fixée dans les conditions prévues au II de l'article 216. / (...) Si, lors de la deuxième consultation, la majorité des suffrages exprimés conclut à nouveau au rejet de l'accession à la pleine souveraineté, une troisième consultation peut être organisée dans les conditions prévues aux deuxième et troisième alinéas du présent article (...) ". Aux termes du III de l'article 219 : " Il est institué une commission de contrôle de l'organisation et du déroulement de la consultation (...) / La commission de contrôle a pour mission de veiller à la régularité et à la sincérité de la consultation. / A cet effet, elle est chargée : (...) 3° De veiller à la régularité de la composition des bureaux de vote, ainsi qu'à celle des opérations de vote, de dépouillement des bulletins et de dénombrement des suffrages et de garantir aux électeurs le libre exercice de leurs droits ; 4° De procéder au recensement général des votes ainsi qu'à la proclamation des résultats ". Aux termes de l'article 220 : " La régularité de la consultation peut être contestée par tout électeur admis à y participer et par le haut-commissaire devant le Conseil d'Etat statuant au contentieux. Les recours sont déposés soit au secrétariat du contentieux du Conseil d'Etat, soit auprès du haut-commissaire dans les dix jours suivant la proclamation des résultats ". <br/>
<br/>
              3. L'article 1er du décret du 24 juin 2020 portant convocation des électeurs et organisation de la consultation sur l'accession à la pleine souveraineté de la Nouvelle-Calédonie, pris sur le fondement des dispositions de la loi organique du 19 mars 1999 mettant en oeuvre l'article 77 de la Constitution, a fixé au 4 octobre 2020 la date de la deuxième consultation sur l'accession à la pleine souveraineté de la Nouvelle-Calédonie. En vertu de l'article 2 de ce décret, les électeurs avaient à répondre par " oui " ou par " non " à la question : " Voulez-vous que la Nouvelle-Calédonie accède à la pleine souveraineté et devienne indépendante ' ". A l'issue de la consultation du 4 octobre 2020, le " non " a obtenu 81 503 voix, soit 53,26 % des suffrages exprimés, et le " oui " 71 533 voix, soit 46,74 % des suffrages exprimés.<br/>
<br/>
              4. Par leur protestation, Mme F... et autres ne demandent pas l'annulation du résultat de la consultation dans son ensemble, mais se bornent à demander l'annulation des opérations électorales qui se sont déroulées dans les bureaux de vote nos 28, 29, 37, 43, 44 et 50 de la commune de Nouméa, nos 27 et 29 de la commune de Lifou et n° 5 de la commune du Mont-Dore, au motif que des irrégularités y auraient entaché le déroulement du scrutin. L'annulation des votes exprimés dans ces bureaux de vote conduirait à ramener les suffrages exprimés en faveur du " non " à 78 616 voix et ceux en faveur du " oui " à 66 241 voix, soit 54,27% pour le " non " et 45,73% pour le " oui ", sans inverser le résultat de la consultation. Dans ces conditions, la protestation qui, sans tendre à l'inversion ou l'annulation du résultat de la consultation, recherche seulement la modification du décompte des voix, auquel ne s'attache, dans son détail, aucune conséquence juridique, n'est pas recevable.<br/>
<br/>
              5. Il résulte de ce qui précède que la protestation de Mme F... et autres ne peut qu'être rejetée, y compris les conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
     D E C I D E :<br/>
Article 1er : La protestation de Mme F... et autres est rejetée.<br/>
Article 2 : La présente décision sera notifiée à Mme F..., première dénommée des protestataires, au groupe Union nationale pour l'indépendance du Congrès, au groupe Calédonie ensemble du Congrès, au groupement " Les Loyalistes 1 ", au groupement " Les Loyalistes 2 ", au Parti travailliste, au groupe l'UC-FLNKS et Nationalistes du Congrès et au ministre des outre-mer.<br/>
Copie en sera adressée au Premier ministre, au gouvernement de la Nouvelle-Calédonie et au haut-commissaire de la République en Nouvelle-Calédonie.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">28-024-02 ÉLECTIONS ET RÉFÉRENDUM. RÉFÉRENDUM. RÉFÉRENDUMS LOCAUX. - CONSULTATION SUR L'ACCESSION À LA PLEINE SOUVERAINETÉ DE LA NOUVELLE-CALÉDONIE (LO DU 19 MARS 1999) - PROTESTATION DEMANDANT L'ANNULATION DES OPÉRATIONS ÉLECTORALES DANS CERTAINS BUREAUX DE VOTE -  RECEVABILITÉ - ABSENCE, LA PROTESTATION ÉTANT INSUSCEPTIBLE DE CONDUIRE À INVERSER LE RÉSULTAT DE LA CONTESTATION [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">28-08-01 ÉLECTIONS ET RÉFÉRENDUM. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. INTRODUCTION DE L'INSTANCE. - CONSULTATION SUR L'ACCESSION À LA PLEINE SOUVERAINETÉ DE LA NOUVELLE-CALÉDONIE (LO DU 19 MARS 1999) - PROTESTATION DEMANDANT L'ANNULATION DES OPÉRATIONS ÉLECTORALES DANS CERTAINS BUREAUX DE VOTE -  RECEVABILITÉ - ABSENCE, LA PROTESTATION ÉTANT INSUSCEPTIBLE DE CONDUIRE À INVERSER LE RÉSULTAT DE LA CONTESTATION [RJ1].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">46-01-02-01 OUTRE-MER. DROIT APPLICABLE. STATUTS. NOUVELLE-CALÉDONIE. - CONSULTATION SUR L'ACCESSION À LA PLEINE SOUVERAINETÉ DE LA NOUVELLE-CALÉDONIE (LO DU 19 MARS 1999) - PROTESTATION DEMANDANT L'ANNULATION DES OPÉRATIONS ÉLECTORALES DANS CERTAINS BUREAUX DE VOTE -  RECEVABILITÉ - ABSENCE, LA PROTESTATION ÉTANT INSUSCEPTIBLE DE CONDUIRE À INVERSER LE RÉSULTAT DE LA CONTESTATION [RJ1].
</SCT>
<ANA ID="9A"> 28-024-02 Consultation du 4 octobre 2020 sur l'accession à la pleine souveraineté de la Nouvelle-Calédonie, en application de la loi organique n° 99-209 du 19 mars 1999 mettant en oeuvre l'article 77 de la Constitution. Le non a obtenu 81 503 voix, soit 53,26 % des suffrages exprimés, et le oui 71 533 voix, soit 46,74 % des suffrages exprimés.,,,Par leur protestation, les requérants ne demandent pas l'annulation du résultat de la consultation dans son ensemble, mais se bornent à demander l'annulation des opérations électorales qui se sont déroulées dans certains bureaux de vote, au motif que des irrégularités y auraient entaché le déroulement du scrutin.,,,L'annulation des votes exprimés dans ces bureaux de vote conduirait à ramener les suffrages exprimés en faveur du non à 78 616 voix et ceux en faveur du oui à 66 241 voix, soit 54,27% pour le non et 45,73% pour le oui, sans inverser le résultat de la consultation.,,,Dans ces conditions, la protestation qui, sans tendre à l'inversion ou l'annulation du résultat de la consultation, recherche seulement la modification du décompte des voix, auquel ne s'attache, dans son détail, aucune conséquence juridique, n'est pas recevable.</ANA>
<ANA ID="9B"> 28-08-01 Consultation du 4 octobre 2020 sur l'accession à la pleine souveraineté de la Nouvelle-Calédonie, en application de la loi organique n° 99-209 du 19 mars 1999 mettant en oeuvre l'article 77 de la Constitution. Le non a obtenu 81 503 voix, soit 53,26 % des suffrages exprimés, et le oui 71 533 voix, soit 46,74 % des suffrages exprimés.,,,Par leur protestation, les requérants ne demandent pas l'annulation du résultat de la consultation dans son ensemble, mais se bornent à demander l'annulation des opérations électorales qui se sont déroulées dans certains bureaux de vote, au motif que des irrégularités y auraient entaché le déroulement du scrutin.,,,L'annulation des votes exprimés dans ces bureaux de vote conduirait à ramener les suffrages exprimés en faveur du non à 78 616 voix et ceux en faveur du oui à 66 241 voix, soit 54,27% pour le non et 45,73% pour le oui, sans inverser le résultat de la consultation.... ,,Dans ces conditions, la protestation qui, sans tendre à l'inversion ou l'annulation du résultat de la consultation, recherche seulement la modification du décompte des voix, auquel ne s'attache, dans son détail, aucune conséquence juridique, n'est pas recevable.</ANA>
<ANA ID="9C"> 46-01-02-01 Consultation du 4 octobre 2020 sur l'accession à la pleine souveraineté de la Nouvelle-Calédonie, en application de la loi organique n° 99-209 du 19 mars 1999 mettant en oeuvre l'article 77 de la Constitution. Le non a obtenu 81 503 voix, soit 53,26 % des suffrages exprimés, et le oui 71 533 voix, soit 46,74 % des suffrages exprimés.,,,Par leur protestation, les requérants ne demandent pas l'annulation du résultat de la consultation dans son ensemble, mais se bornent à demander l'annulation des opérations électorales qui se sont déroulées dans certains bureaux de vote, au motif que des irrégularités y auraient entaché le déroulement du scrutin.,,,L'annulation des votes exprimés dans ces bureaux de vote conduirait à ramener les suffrages exprimés en faveur du non à 78 616 voix et ceux en faveur du oui à 66 241 voix, soit 54,27% pour le non et 45,73% pour le oui, sans inverser le résultat de la consultation.... ,,Dans ces conditions, la protestation qui, sans tendre à l'inversion ou l'annulation du résultat de la consultation, recherche seulement la modification du décompte des voix, auquel ne s'attache, dans son détail, aucune conséquence juridique, n'est pas recevable.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., sur les modalités d'appréciation des irrégularités susceptibles d'affecter le résultat d'une consultation organisée le sur le fondement du troisième alinéa de l'article 72-1 de la Constitution, CE, Assemblée, 17 octobre 2003, Consultation des électeurs de Corse, n°s 258487 258626, p. 428-1 ; sur l'irrecevabilité d'une protestation ne tendant pas à l'annulation de l'élection ou la réformation des résultats, CE, Section, 17 octobre 1986, Elections cantonnales de Sevran, n°s 70266 70386, p. 233.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
