<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025449347</ID>
<ANCIEN_ID>JG_L_2012_03_000000338450</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/44/93/CETATEXT000025449347.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème et 2ème sous-sections réunies, 01/03/2012, 338450, Publié au recueil Lebon</TITRE>
<DATE_DEC>2012-03-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>338450</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème et 2ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Jacques Arrighi de Casanova</PRESIDENT>
<AVOCATS>SCP BLANC, ROUSSEAU ; SCP MONOD, COLIN</AVOCATS>
<RAPPORTEUR>Mme Cécile Chaduteau-Monplaisir</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Bertrand Dacosta</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:338450.20120301</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu l'ordonnance n° 10LY00556 du 30 mars 2010, enregistrée au secrétariat du contentieux du Conseil d'Etat le 7 avril 2010, par laquelle le président de la cour administrative d'appel de Lyon a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, le pourvoi présenté à cette cour par Mme Chantal A, demeurant ... ; <br/>
<br/>
              Vu le pourvoi, enregistré au greffe de la cour administrative d'appel de Lyon le 1er mars 2010, et les mémoires complémentaires, enregistrés les 1er juin et 6 juillet 2010 au secrétariat du contentieux du Conseil d'Etat, présentés pour Mme A; Mme  demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement n° 0708568 du 30 décembre 2009 du tribunal administratif de Lyon en tant qu'il a rejeté sa demande tendant à l'annulation de la décision du 18 juin 2007 du maire de la commune de Vénissieux rejetant le recours gracieux qu'elle a formé contre la décision du 8 juin 2007 lui refusant de reconnaître comme imputable au service l'accident dont elle a été victime le 24 mai 2005 et de transmettre, à ce titre, un dossier de demande d'allocation temporaire d'invalidité à la Caisse des dépôts et consignations ;<br/>
<br/>
              2°) de mettre la somme de 3 000 euros à la charge de la commune de Vénissieux en application de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Cécile Chaduteau-Monplaisir, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Blanc, Rousseau, avocat de Mme A et de la SCP Monod, Colin, avocat de la commune de Vénissieux, <br/>
<br/>
              - les conclusions de M. Bertrand Dacosta, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Blanc, Rousseau, avocat de Mme A et à la SCP Monod, Colin, avocat de la commune de Vénissieux, <br/>
<br/>
<br/>
<br/>
              Sur la fin de non-recevoir opposée par la commune de Vénissieux : <br/>
<br/>
              Considérant, d'une part, que le délai de recours en cassation est de deux mois en vertu des dispositions de l'article R. 821-1 du code de justice administrative, ce délai courant à compter de la notification de la décision juridictionnelle attaquée ; que le requérant qui, dans ce délai, a contesté tant la régularité de cette décision que son bien-fondé et a ainsi invoqué les deux causes juridiques susceptibles de fonder un pourvoi en cassation, est recevable à développer, après l'expiration de ce délai, tout moyen de cassation se rattachant à l'une ou l'autre de ces causes ; <br/>
<br/>
              Considérant, d'autre part, qu'il résulte de l'article R. 612-1 du code de justice administrative que, dans le cas où la notification de la décision attaquée ne mentionnait pas l'obligation de recourir, conformément aux dispositions de l'article R. 821-3 du même code, au ministère d'un avocat au Conseil d'Etat et à la Cour de cassation, le Conseil d'Etat doit inviter l'auteur du pourvoi à le régulariser ; que tel est en particulier le cas lorsque, par suite des indications erronées portées sur la notification d'un jugement rendu en premier et dernier ressort, un requérant a formé, dans les deux mois de cette notification, un appel motivé devant la cour administrative d'appel et que le président de celle-ci a transmis son recours au Conseil d'Etat en application de l'article R. 351-2 de ce code ; qu'en pareille hypothèse, le requérant doit être regardé comme ayant régulièrement formé un pourvoi en cassation contre ce jugement, sous réserve qu'il ait donné suite à l'invitation à faire régulariser son pourvoi par un avocat au Conseil d'Etat et à la Cour de cassation ; que le délai de deux mois à l'issue duquel il n'est plus recevable à invoquer une cause juridique distincte court alors à compter de la réception de cette demande de régularisation ;<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis au juge du fond que Mme A, agent de la commune de Vénissieux, a demandé la reconnaissance de l'imputabilité au service de l'accident dont elle a été victime le 24 mai 2005 en vue d'obtenir l'attribution d'une allocation temporaire d'invalidité ; que le maire de Vénissieux a rejeté ces demandes par décisions des 8 et 18 juin 2007 ; que, par le jugement attaqué du 30 décembre 2009, le tribunal administratif de Lyon, après avoir annulé la première de ces décisions, a rejeté les conclusions de Mme A dirigées contre la seconde ; <br/>
<br/>
              Considérant que la notification de ce jugement, rendu en premier et dernier ressort, a été reçue par Mme A le 7 janvier 2010, avec l'indication erronée d'une voie de recours devant la cour administrative d'appel de Lyon et que l'intéressée a saisi la cour, le 1er mars 2010, d'un recours motivé contre ce jugement ; qu'à la suite de sa transmission au Conseil d'Etat par ordonnance du président de cette cour, le 30 mars suivant, son pourvoi a été régularisé par la présentation, le 1er juin 2010, d'un mémoire signé par un avocat au Conseil d'Etat et à la Cour de cassation et mettant en cause tant la régularité du jugement attaqué que son bien-fondé ; que, dès lors que ce mémoire a été enregistré dans les deux mois suivant la réception, le 11 mai 2010, de la demande de régularisation adressée à la requérante conformément aux dispositions de l'article R. 612-1 du code de justice administrative, la commune de Vénissieux n'est pas fondée à soutenir que, faute de l'avoir fait dans les deux mois suivant la notification de ce jugement, Mme A ne serait plus recevable à en contester la régularité ; <br/>
<br/>
              Sur la régularité du jugement attaqué :<br/>
<br/>
              Considérant qu'aux termes de l'article R. 741-2 du code de justice administrative : " La décision mentionne que l'audience a été publique (...) " ; qu'il ne ressort d'aucune des mentions du jugement attaqué que l'audience du tribunal administratif de Lyon au cours de laquelle la demande Mme A a été examinée a été publique ; qu'ainsi ce jugement ne fait pas la preuve que la procédure à l'issue de laquelle il a été prononcé a été régulière ; que, dès lors, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, ce jugement doit être annulé ;  <br/>
<br/>
              Sur les conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              Considérant que ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de Mme A, qui n'est pas, dans la présente instance, la partie perdante ; qu'en revanche, il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de Vénissieux  la somme de 3 000 euros à verser à Mme A au titre des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du 30 décembre 2009 du tribunal administratif de Lyon est annulé. <br/>
<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Lyon.<br/>
<br/>
Article 3 : La commune de Vénissieux versera à Mme A la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : Les conclusions présentées par la commune de Vénissieux au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 5 : La présente décision sera notifiée à Mme Chantal A et à la commune de Vénissieux. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-08-02-004-03 PROCÉDURE. VOIES DE RECOURS. CASSATION. RECEVABILITÉ. RECEVABILITÉ DES MOYENS. - RECEVABILITÉ APRÈS L'EXPIRATION DU DÉLAI DE RECOURS EN CASSATION - MOYEN SE RATTACHANT À LA MÊME CAUSE JURIDIQUE QU'UN MOYEN SOULEVÉ DANS LE DÉLAI DE RECOURS - 1) NOTION DE CAUSE JURIDIQUE [RJ1] - 2) CAS DE REQUÊTE D'APPEL CONTRE UN JUGEMENT RENDU EN PREMIER ET DERNIER RESSORT, À LA SUITE D'INDICATIONS ERRONÉES DANS LA NOTIFICATION - TRANSMISSION AU JUGE DE CASSATION ET RÉGULARISATION -  CONSÉQUENCES - REQUALIFICATION EN MOYENS DE CASSATION DES MOYENS FIGURANT DANS LA REQUÊTE D'APPEL - POINT DE DÉPART DU DÉLAI POUR INVOQUER UN MOYEN RELEVANT D'UNE CAUSE JURIDIQUE DISTINCTE - RÉCEPTION DE LA DEMANDE DE RÉGULARISATION [RJ2].
</SCT>
<ANA ID="9A"> 54-08-02-004-03 1) La régularité d'une décision, d'une part, et son bien-fondé, de l'autre constituent les deux causes juridiques susceptibles de fonder un pourvoi en cassation.,,2) Requérant ayant formé un appel contre un jugement rendu en premier et dernier ressort par un tribunal administratif, à la suite d'indications erronées figurant sur la notification. Le recours a été transmis par ordonnance au Conseil d'Etat et régularisé par un avocat au Conseil d'Etat et à la Cour de cassation. En pareille hypothèse, le délai de deux mois à l'issue duquel le requérant, qui doit être regardé comme ayant régulièrement formé un pourvoi en cassation contre le jugement, n'est plus recevable à invoquer une cause juridique distincte, court à compter de la réception par celui-ci de la demande de régularisation adressée par le Conseil d'Etat.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Section, 20 février 1953, Société Intercopie, p. 88. Rappr., pour la notion de cause juridique devant les juridictions disciplinaires d'appel, CE, 27 juin 2011, Conseil départemental de Paris de l'Ordre des chirurgiens-dentistes, n° 339568, à mentionner aux Tables.,,[RJ2] Cf. CE, 30 avril 2009, Bouvier d'Yvoire, n° 316389, p. 178, qui doit dès lors s'entendre comme concernant seulement la question de l'existence de moyens dans le délai de recours et la requalification en moyens de cassation des moyens figurant dans la requête d'appel.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
