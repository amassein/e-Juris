<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031350144</ID>
<ANCIEN_ID>JG_L_2015_10_000000382633</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/35/01/CETATEXT000031350144.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème / 5ème SSR, 21/10/2015, 382633, Publié au recueil Lebon</TITRE>
<DATE_DEC>2015-10-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>382633</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème / 5ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MASSE-DESSEN, THOUVENIN, COUDRAY ; SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>M. David Moreau</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Gaëlle Dumortier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:382633.20151021</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La délégation unique du personnel de la clinique Vauban 2020, le syndicat CFDT santé sociaux de la Seine-Saint-Denis et Mme A...E...ont demandé au tribunal administratif de Montreuil d'annuler pour excès de pouvoir la décision du 26 juillet 2013 par laquelle le directeur régional des entreprises, de la concurrence, de la consommation, du travail et de l'emploi d'Ile de France a homologué le plan de sauvegarde de l'emploi relatif à la société Vauban 2020. Par un jugement nos 1309825-1310102-1311272 du 20 décembre 2013, le tribunal administratif a annulé cette décision.<br/>
<br/>
              Par un arrêt nos 14VE00587, 14VE00271 du 13 mai 2014, la cour administrative d'appel de Versailles a, sur appel de la société Vauban 2020 et du ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social, annulé ce jugement et rejeté les demandes de la délégation unique du personnel de la clinique Vauban 2020, du syndicat CFDT santé sociaux de la Seine-Saint-Denis et de MmeE....<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 15 juillet 2014, 15 octobre 2014 et 24 mars 2015 au secrétariat du contentieux du Conseil d'Etat, le syndicat CFDT santé sociaux de la Seine-Saint-Denis, la délégation unique du personnel de la clinique Vauban 2020 et Mme E...demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de la société Vauban 2020 et du ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social ;<br/>
<br/>
              3°) de mettre à la charge solidaire de MeD..., en qualité de liquidateur judiciaire de la société Vauban 2020, de MeB..., en qualité d'administrateur judiciaire de cette société et de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de commerce ;<br/>
              - le code du travail ;<br/>
              - l'ordonnance n° 2014-326 du 12 mars 2014 ;<br/>
              - l'ordonnance n° 2014-699 du 26 juin 2014 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. David Moreau, maître des requêtes, <br/>
<br/>
              - les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Masse-Dessen, Thouvenin, Coudray, avocat du syndicat CFDT santé sociaux de la Seine-Saint-Denis, de la délégation unique du personnel de la clinique Vauban 2020 et de Mme E...et à la SCP Lyon-Caen, Thiriez, avocat de Me D...et de Me B...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que, par un jugement du 20 décembre 2013, le tribunal administratif de Montreuil a, à la demande de la délégation unique du personnel de la clinique Vauban 2020, du syndicat CFDT santé sociaux de la Seine-Saint-Denis et de Mme E...annulé la décision du 26 juillet 2013 par laquelle le directeur régional des entreprises, de la concurrence, de la consommation, du travail et de l'emploi d'Ile de France a homologué le plan de sauvegarde de l'emploi relatif à la société Vauban 2020, société placée en liquidation judiciaire puis ayant fait l'objet d'un plan de cession arrêté par un jugement du tribunal de commerce de Bobigny du 9 juillet 2013 ; que sur les appels formés, d'une part, par Me D...et MeB..., agissant respectivement en qualité de liquidateur et d'administrateur judiciaire de la société Vauban 2020, et, d'autre part, par le ministre chargé du travail, la cour administrative d'appel de Versailles a, par un arrêt du 13 mai 2014, annulé ce jugement du tribunal administratif de Montreuil et rejeté les demandes de première instance ; que le syndicat CFDT sociaux santé de la Seine-Saint-Denis et autres se pourvoient en cassation contre cet arrêt ;<br/>
<br/>
              2. Considérant que, après avoir censuré le motif retenu par le tribunal administratif pour annuler la décision d'homologation du plan de sauvegarde de l'emploi relatif à la société Vauban 2020, il appartenait à la cour administrative d'appel de Versailles, saisie par l'effet dévolutif de l'appel, de se prononcer sur l'ensemble des moyens soulevés tant devant le tribunal administratif que devant elle par les demandeurs de première instance ; qu'il ressort des pièces du dossier soumis aux juges du fond que, dans un mémoire en défense enregistré au greffe de la cour administrative d'appel de Versailles le 7 avril 2014, le syndicat CFDT santé sociaux de la Seine-Saint-Denis et autres soutenaient que la délégation unique du personnel n'avait pas rendu l'avis sur l'opération projetée et ses modalités d'application prévu par le 1° du I de l'article L. 1233-30 et par l'article L. 2325-15 du code du travail ; que la cour n'a pas répondu à ce moyen, qu'elle n'a d'ailleurs pas analysé dans les visas de sa décision ; que ce moyen n'était pas inopérant ; qu'elle a ainsi entaché son arrêt d'irrégularité ; que, dès lors, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, le syndicat CFDT santé sociaux de la Seine-Saint-Denis et autres sont fondés à en demander l'annulation ;<br/>
<br/>
              3. Considérant que, le délai de trois mois imparti à la cour administrative d'appel pour statuer par les dispositions de l'article L. 1235-7-1 du code du travail étant expiré, il y a lieu pour le Conseil d'Etat, en application des mêmes dispositions, de statuer immédiatement sur les appels de MeD..., de Me B...et du ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social contre le jugement du 20 décembre 2013 du tribunal administratif de Montreuil ;<br/>
<br/>
              4. Considérant, d'une part, qu'aux termes de l'article L. 1233-61 du code du travail : " Dans les entreprises d'au moins cinquante salariés, lorsque le projet de licenciement concerne au moins dix salariés dans une même période de trente jours, l'employeur établit et met en oeuvre un plan de sauvegarde de l'emploi pour éviter les licenciements ou en limiter le nombre. (...) " ; que les articles L. 1233-24-1 et L. 1233-24-4 du même code prévoient que le contenu de ce plan de sauvegarde de l'emploi peut être déterminé par un accord collectif d'entreprise et qu'à défaut d'accord, il est fixé par un document élaboré unilatéralement par l'employeur ; qu'enfin, aux termes de l'article L. 1233-57-3 du même code, dans sa rédaction en vigueur à la date de la décision litigieuse : " En l'absence d'accord collectif (...) l'autorité administrative homologue le document élaboré par l'employeur mentionné à l'article L. 1233-24-4, après avoir vérifié (...) la régularité de la procédure d'information et de consultation du comité d'entreprise (...) " ;<br/>
<br/>
              5. Considérant, d'autre part, qu'il résulte des dispositions de l'article L. 1233-28 du code du travail que l'employeur qui envisage de procéder à un licenciement collectif pour motif économique d'au moins dix salariés dans une même période de trente jours doit réunir et consulter, selon le cas, le comité d'entreprise ou les délégués du personnel ; qu'à ce titre, le I de l'article L. 1233-30 du même code dispose, s'agissant des entreprises ou établissements qui emploient habituellement au moins cinquante salariés, que : " (...) l'employeur réunit et consulte le comité d'entreprise sur : / 1° L'opération projetée et ses modalités d'application, conformément à l'article L. 2323-15 ; / 2° Le projet de licenciement collectif : le nombre de suppressions d'emploi, les catégories professionnelles concernées, les critères d'ordre et le calendrier prévisionnel des licenciements, les mesures sociales d'accompagnement prévues par le plan de sauvegarde de l'emploi. (...) Le comité d'entreprise tient au moins deux réunions espacées d'au moins quinze jours " ; qu'aux termes de l'article L. 1233-31 : " L'employeur adresse au représentants du personnel, avec la convocation à la première réunion, tous renseignements utiles sur le projet de licenciement collectif. / Il indique: / 1° La ou les raisons économiques, financières ou techniques du projet de licenciement ; (...) " ; que l'article L. 1233-32 dispose que, dans les entreprises de plus de cinquante salariés, l'employeur adresse " outre les renseignements prévus à l'article L. 1233-31 (...) le plan de sauvegarde de l'emploi (...) " ; que l'article L. 2323-15 dispose que : " Le comité d'entreprise est saisi en temps utile des projets de restructuration et de compression des effectifs. / Il émet un avis sur l'opération projetée et ses modalités d'application dans les conditions et délais prévus à l'article L. 1233-30, lorsqu'elle est soumise à l'obligation d'établir un plan de sauvegarde de l'emploi. (...) " ; qu'aux termes des deux premiers alinéas de l'article L. 1233-34 : " Dans les entreprises d'au moins cinquante salariés, lorsque le projet de licenciement concerne au moins dix salariés dans une même période de trente jours, le comité d'entreprise peut recourir à l'assistance d'un expert-comptable en application de l'article L. 2325-35. Le comité prend sa décision lors de la première réunion prévue à l'article L. 1233-30 (...) / L'expert-comptable peut être assisté par un expert technique dans les conditions prévues à l'article L. 2325-41 " ; qu'enfin, aux termes du premier alinéa de l'article L. 1233-35 : " L'expert désigné par le comité d'entreprise demande à l'employeur, au plus tard dans les dix jours à compter de sa désignation, toutes les informations qu'il juge nécessaires à la réalisation de sa mission. L'employeur répond à cette demande dans les huit jours. Le cas échéant, l'expert demande, dans les dix jours, des informations complémentaires à l'employeur, qui répond à cette demande dans les huit jours à compter de la date à laquelle la demande de l'expert est formulée " ;<br/>
<br/>
              6. Considérant qu'il résulte de l'ensemble des dispositions citées ci-dessus que, lorsqu'elle est saisie par un employeur d'une demande d'homologation d'un document élaboré en application de l'article L. 1233-24-4 du code du travail et fixant le contenu d'un plan de sauvegarde de l'emploi, il appartient à l'administration de s'assurer, sous le contrôle du juge de l'excès de pouvoir, que la procédure d'information et de consultation du comité d'entreprise a été régulière ; qu'elle ne peut légalement accorder l'homologation demandée que si le comité a été mis à même d'émettre régulièrement un avis, d'une part sur l'opération projetée et ses modalités d'application et, d'autre part, sur le projet de licenciement collectif et le plan de sauvegarde de l'emploi ; qu'il appartient en particulier à ce titre à l'administration de s'assurer que l'employeur a adressé au comité d'entreprise, avec la convocation à sa première réunion, ainsi que, le cas échéant, en réponse à des demandes exprimées par le comité, tous les éléments utiles pour qu'il formule ses deux avis en toute connaissance de cause ; que lorsque l'assistance d'un expert comptable a été demandée selon les modalités prévues par l'article L. 1233-34 du code du travail, l'administration doit également s'assurer que celui-ci a pu exercer sa mission dans des conditions permettant au comité d'entreprise de formuler ses avis en toute connaissance de cause ;<br/>
<br/>
              7. Considérant qu'il ressort des pièces du dossier, notamment du compte-rendu de la réunion qui s'est tenue le 15 juillet 2013, que la délégation unique du personnel a demandé l'assistance d'un expert comptable aux frais de l'employeur prévue par les articles L. 1233-34 et L. 2325-35 du code du travail ; que ces dispositions étaient applicables à la date des faits alors même qu'avant leur modification par l'ordonnance du 12 mars 2014 sur la prévention des difficultés des entreprises et des procédures collectives et l'ordonnance du 26 juin 2014 sur la simplification et l'adaptation du code du travail, l'article L. 1233-58, qui fixe les règles de consultation du comité d'entreprise en cas de procédure collective, n'y renvoyait pas expressément ; qu'il ressort des pièces du dossier, notamment des attestations produites par le cabinet Syndex, que l'administrateur judiciaire a refusé le financement de cette expertise ; que, la délégation unique du personnel ayant ainsi été privée du droit qu'elle tenait des dispositions précitées des articles L. 1233-34 et L. 2325-35, elle ne pouvait en principe être regardée comme ayant été mise à même de formuler ses avis en toute connaissance de cause ; <br/>
<br/>
              8. Mais considérant qu'il ressort des pièces du dossier que le plan de cession et le nombre de licenciements avaient, en l'espèce, déjà été arrêtés par le jugement du 9 juillet 2013 du tribunal de commerce de Bobigny lorsque la délégation unique du personnel a souhaité recourir à l'assistance d'un expert-comptable pour la suite de la procédure d'information et de consultation des représentants du personnel ; que, par ailleurs, celui qu'elle a alors désigné à ses frais, dans le cadre des dispositions de l'article L. 2325-41 du code du travail, a été associé à cette procédure, notamment lors de la réunion du 18 juillet 2013 à laquelle il était présent ; qu'enfin, si cet expert-comptable n'est pas intervenu au titre d'une mission d'assistance financée par l'employeur, il ressort des pièces du dossier que cette circonstance ne l'a pas empêché d'exercer utilement sa mission et, en particulier, n'a pas fait obstacle à ce qu'il dispose des documents nécessaires à cette fin ; que, dans ces conditions, le refus de l'administrateur judiciaire de prendre en charge l'expertise comptable sollicitée par la délégation unique du personnel ne peut être regardé comme ayant été, en l'espèce, de nature à empêcher cette dernière de formuler ses avis en toute connaissance de cause ; que, par suite, le ministre chargé du travail, Me D...et Me B...sont fondés à soutenir que c'est à tort que le tribunal administratif de Montreuil s'est fondé sur le refus opposé à la délégation unique du personnel du concours d'un expert-comptable dans les conditions prévues par les articles L. 1233-34 et L. 2325-35 du code du travail pour annuler la décision du 26 juillet 2013 du directeur régional des entreprises, de la concurrence, de la consommation, du travail et de l'emploi d'Ile de France ;<br/>
<br/>
              9. Considérant toutefois qu'il appartient au Conseil d'Etat, saisi par l'effet dévolutif de l'appel, d'examiner les autres moyens soulevés par le syndicat CFDT santé sociaux de la Seine-Saint-Denis, la délégation unique du personnel de la clinique Vauban 2020 et Mme E... tant en première instance qu'en appel ;<br/>
<br/>
              10. Considérant, en premier lieu, qu'il ressort des pièces du dossier que la délégation unique du personnel a été consultée et a rendu un avis sur les offres de reprise avant le prononcé du jugement du tribunal de commerce de Bobigny du 9 juillet 2013 arrêtant le plan de cession ; qu'il ressort par ailleurs du procès-verbal de la réunion qui s'est tenue le 15 juillet 2013 à 15 h qu'elle a été consultée et a émis un avis sur le plan de cession arrêté par le tribunal de commerce, notamment sur le projet de licenciement de 36 salariés pour motif économique ; que, par suite, le moyen tiré de ce que l'administrateur judiciaire n'aurait pas informé et consulté la délégation unique du personnel sur l'opération de réduction d'effectifs projetée et ses modalités d'application conformément aux exigences du 1° du I de l'article L. 1233-30 du code du travail doit être écarté ;<br/>
<br/>
              11. Considérant, en deuxième lieu, qu'aux termes de l'article L. 642-5 du code de commerce dans sa version en vigueur à la date des faits : " Après avoir recueilli l'avis du ministère public et entendu ou dûment appelé le débiteur, le liquidateur, l'administrateur lorsqu'il en a été désigné, les représentants du comité d'entreprise ou, à défaut, des délégués du personnel et les contrôleurs, le tribunal retient l'offre qui permet dans les meilleures conditions d'assurer le plus durablement l'emploi attaché à l'ensemble cédé, le paiement des créanciers et qui présente les meilleures garanties d'exécution. Il arrête un ou plusieurs plans de cession. (...) / Lorsque le plan prévoit des licenciements pour motif économique, il ne peut être arrêté par le tribunal qu'après que la procédure prévue à l'article L. 1233-58 du code du travail a été mise en oeuvre, à l'exception du 6° du I et des trois premiers alinéas du II de cet article (...) " ; que ces dispositions, qui fixent une règle de procédure relative aux conditions dans lesquelles le tribunal de commerce doit rendre le jugement arrêtant le plan de cession, ne sauraient être utilement invoquées à l'appui d'un recours dirigé contre une décision de l'administration homologuant un plan de sauvegarde de l'emploi ;<br/>
<br/>
              12. Considérant, en troisième lieu, qu'aux termes de l'article L. 2325-15 du code du travail : " L'ordre du jour des réunions du comité d'entreprise est arrêté par l'employeur et le secrétaire. / Toutefois, lorsque sont en cause des consultations rendues obligatoires par une disposition législative, réglementaire ou par un accord collectif de travail, elles y sont inscrites de plein droit par l'employeur ou le secrétaire " ; qu'à la supposer même établie, la circonstance que l'ordre du jour des deux réunions de la délégation unique du personnel du 15 juillet 2013 aurait été fixé unilatéralement par l'employeur est sans incidence sur la régularité de la procédure de licenciement, dès lors que la consultation de la délégation unique du personnel sur le projet de licenciement collectif pour motif économique devait être inscrite de plein droit à l'ordre du jour de cette réunion en application des dispositions citées ci-dessus du I de l'article L. 1233-30 du code du travail ; <br/>
<br/>
              13. Considérant, en quatrième lieu, que les demandeurs ne sont pas fondés à soutenir que la présence de l'avocat de l'administrateur judiciaire lors des réunions des 15 et 18 juillet 2013 aurait entaché d'irrégularité la procédure d'information et de consultation de la délégation unique du personnel dès lors qu'il n'est ni établi ni même allégué que cette présence a pu exercer une influence sur les membres de la délégation ;<br/>
<br/>
              14. Considérant, en cinquième lieu, qu'il n'est, en tout état de cause, pas établi que le procès-verbal des réunions des 15 et 18 juillet 2013, qui a d'ailleurs été signé par la secrétaire de la délégation unique du personnel, aurait été dressé uniquement par l'administrateur judiciaire ;<br/>
<br/>
              15. Considérant, en dernier lieu, qu'aux termes du troisième alinéa de l'article L. 642-5 du code de commerce : " Le jugement qui arrête le plan en rend les dispositions applicables à tous " ; qu'aux termes du deuxième alinéa de l'article R. 642-3 du même code : " Lorsque le plan de cession prévoit des licenciements pour motif économique, le liquidateur, ou l'administrateur lorsqu'il en a été désigné, produit à l'audience les documents mentionnés à l'article R. 631-36. Le jugement arrêtant le plan indique le nombre de salariés dont le licenciement est autorisé ainsi que les activités et catégories professionnelles concernées " ; qu'il résulte de ces dispositions que les catégories professionnelles déterminées par le jugement qui arrête le plan de cession et fixe le nombre de licenciements s'imposent au liquidateur ou à l'administrateur judiciaire pour le choix des salariés à licencier, ainsi qu'à l'autorité administrative chargée d'homologuer le document unilatéral de l'employeur déterminant le contenu du plan de sauvegarde de l'emploi ; qu'il ressort des pièces du dossier que les catégories professionnelles retenues dans le document unilatéral de l'employeur ayant fait l'objet de la décision d'homologation litigieuse sont identiques à celles qui ont été fixées par le jugement du tribunal de commerce de Bobigny du 9 juillet 2013 ; que, par suite, les demandeurs ne sauraient utilement en contester le bien-fondé ;<br/>
<br/>
              16. Considérant qu'il résulte de tout ce qui précède que, sans qu'il soit besoin de statuer sur la recevabilité des demandes du syndicat CFDT santé sociaux de la Seine-Saint-Denis, de la délégation unique du personnel de la clinique Vauban 2020 et de Mme E..., le ministre chargé du travail, Me B...et Me D...sont fondés à soutenir que c'est à tort que, par son jugement du 20 décembre 2013, le tribunal administratif de Montreuil a annulé la décision du 26 juillet 2013 du directeur régional des entreprises, de la concurrence, de la consommation, du travail et de l'emploi de la région Ile-de-France ;<br/>
<br/>
              17. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat, de Me B... et de MeD..., qui ne sont pas les parties perdantes dans la présente instance ; que, dans les circonstances de l'espèce, il n'y a pas lieu de faire droit aux conclusions présentées au titre des mêmes dispositions par Me B...et Me D...;<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : L'arrêt du 13 mai 2014 de la cour administrative d'appel de Versailles et le jugement du 20 décembre 2013 du tribunal administratif de Montreuil sont annulés.<br/>
Article 2 : Les demandes du syndicat CFDT santé sociaux de la Seine-Saint-Denis, de la délégation unique du personnel de la clinique Vauban 2020 et de Mme E...présentées devant le tribunal administratif de Montreuil sont rejetées.<br/>
Article 3 : Les conclusions des parties présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée au syndicat CFDT santé sociaux de la Seine-Saint-Denis, à la délégation unique du personnel de la clinique Vauban 2020, à Mme A...E..., à la ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social, à Me C...D...et à Me F...B....<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">66-07 TRAVAIL ET EMPLOI. LICENCIEMENTS. - VALIDATION OU HOMOLOGATION ADMINISTRATIVE DES PSE (LOI DU 14 JUIN 2013) - CONSULTATION DU COMITÉ D'ENTREPRISE - ASSISTANCE D'UN EXPERT-COMPTABLE - CONTRÔLE DE L'AUTORITÉ ADMINISTRATIVE [RJ1] - CAS OÙ L'EMPLOYEUR REFUSE DE FINANCER L'EXPERT-COMPTABLE.
</SCT>
<ANA ID="9A"> 66-07 Contrôle de la régularité de la consultation du comité d'entreprise lors de l'élaboration d'un plan de sauvegarde de l'emploi (PSE). Lorsque l'assistance d'un expert-comptable a été demandée selon les modalités prévues par l'article L. 1233-34 du code du travail, l'administration doit s'assurer que celui-ci a pu exercer sa mission dans des conditions permettant au comité d'entreprise de formuler ses avis en toute connaissance de cause.... ,,En l'espèce, l'administrateur judiciaire désigné dans le cadre d'une procédure de liquidation avait refusé de prendre à sa charge l'assistance d'un expert-comptable et, par suite, la délégation unique du personnel ne pouvait pas, en principe, être regardée comme ayant été mise à même de formuler ses avis en toute connaissance de cause. Cependant, dès lors que le plan de cession et le nombre des licenciements avaient déjà été arrêtés par le tribunal de commerce dans le cadre de la procédure de liquidation judiciaire, que la délégation unique du personnel a désigné à ses frais un expert-comptable qui a été associé à la procédure, et que la circonstance que celui-ci n'avait pas été pris en charge par l'administrateur judiciaire ne l'a pas empêché d'exercer utilement sa mission et, en particulier, n'a pas fait obstacle à ce qu'il dispose des documents nécessaires à cette fin, le refus de l'administrateur judiciaire ne peut être regardé comme ayant été, en l'espèce, de nature à empêcher la délégation unique du personnel de formuler ses avis en toute connaissance de cause.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Cf. CE, 21 octobre 2015, Comité d'entreprise de la société Norbert Dentressangle Silo et autres, n° 385683, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
