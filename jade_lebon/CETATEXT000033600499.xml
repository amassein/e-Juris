<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033600499</ID>
<ANCIEN_ID>JG_L_2016_12_000000384292</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/60/04/CETATEXT000033600499.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 13/12/2016, 384292</TITRE>
<DATE_DEC>2016-12-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>384292</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MONOD, COLIN, STOCLET ; SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>M. Christian Fournier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:384292.20161213</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Le syndicat des cadres techniques de la ville de Paris, M. I... B..., M. E... F..., M. J... D..., Mme G...K..., M. H... C...et M. J... A...ont demandé au tribunal administratif de Paris d'annuler la délibération DRH n° 2008-27 des 7 et 8 juillet 2008 par laquelle le conseil de Paris, siégeant en formation de conseil municipal, a modifié le statut particulier des architectes-voyers de la commune de Paris.<br/>
<br/>
              Par un jugement n° 0816551/5-3 du 12 juillet 2011, le tribunal administratif a annulé l'article 4 de cette délibération et rejeté le surplus de leur demande.<br/>
<br/>
              Par un arrêt n° 12PA04593 du 3 juillet 2014, la cour administrative d'appel de Paris, saisie de l'appel du syndicat des cadres techniques de la ville de Paris et autres et d'un appel incident de la ville de Paris, a annulé le jugement litigieux et rejeté la demande de première instance du syndicat des cadres techniques de la ville de Paris et autres ainsi que le surplus des conclusions de leur requête d'appel.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 8 septembre et 4 décembre 2014 et le 15 octobre 2015 au secrétariat du contentieux du Conseil d'Etat, le syndicat des cadres techniques de la ville de Paris et autres demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel ;<br/>
<br/>
              3°) de mettre à la charge de la ville de Paris la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la loi n° 84-53 du 26 janvier 1984 ;<br/>
              - le décret n° 94-415 du 24 mai 1994 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Christian Fournier, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Monod, Colin, Stoclet, avocat du syndicat des cadres techniques de la ville de Paris, de M. I...B..., de M. E...F..., de M. J...D..., de Mme G...K..., de M. H... C...et de M. J...A...et à la SCP Foussard, Froger, avocat de la ville de Paris ;<br/>
<br/>
              Vu la note en délibéré présentée le 28 novembre 2016 par le syndicat des cadres techniques de la ville de Paris et autres.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par une délibération DRH n° 2008-27 des 7 et 8 juillet 2008, le conseil de Paris, siégeant en formation de conseil municipal, a modifié la délibération DRH n° 2006-36-1° des 10 et 11 juillet 2006 fixant le statut particulier du corps des architectes-voyers de la commune de Paris afin, d'une part, de créer au sein de ce corps les spécialités " architecte-voyer " et " paysagiste " et, d'autre part, d'intégrer dans ce corps et dans cette spécialité, sur leur demande et après avis de la commission administrative paritaire, les agents non titulaires de la commune de Paris exerçant les fonctions de " paysagiste " depuis au moins trois ans et justifiant de certains diplômes. Le syndicat des cadres techniques de la ville de Paris, M. I...B..., M. E...F..., M. J... D..., Mme G...K..., M. H...C...et M. J...A...ont demandé au tribunal administratif de Paris d'annuler la délibération des 7 et 8 juillet 2008. Par un jugement du 12 juillet 2011, le tribunal administratif a annulé l'article 4 de la délibération litigieuse fixant les conditions d'avancement au grade d'architecte-voyer en chef applicables aux agents relevant de la spécialité " paysagiste ". Le syndicat des cadres techniques de la ville de Paris et autres ont relevé appel de ce jugement, en tant qu'il leur faisait grief, devant la cour administrative de Paris, laquelle a été saisie en outre d'un appel incident de la ville de Paris. Par un arrêt du 3 juillet 2014, la cour administrative d'appel de Paris a annulé ce jugement et rejeté la demande de première instance du syndicat des cadres techniques de la ville de Paris et autres ainsi que le surplus des conclusions de leur requête d'appel. Le syndicat des cadres techniques de la ville de Paris et autres se pourvoient en cassation contre cet arrêt.<br/>
<br/>
              2. Aux termes de l'article 4 de la loi du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale, dans sa rédaction applicable au litige : " Les fonctionnaires territoriaux appartiennent à des cadres d'emplois régis par des statuts particuliers, communs aux fonctionnaires des communes, des départements, des régions et de leurs établissements publics / Ces statuts particuliers ont un caractère national. / Un cadre d'emplois regroupe les fonctionnaires soumis au même statut particulier, titulaires d'un grade leur donnant vocation à occuper un ensemble d'emplois. Chaque titulaire d'un grade a vocation à occuper certains des emplois correspondant à ce grade (...) ". Aux termes de l'article 118 de cette même loi : " I - La commune et le département de Paris, ainsi que leurs établissements publics, disposent de fonctionnaires organisés en corps. Les personnels de ces collectivités et établissements sont soumis à un statut fixé par décret en Conseil d'Etat, qui peut déroger aux dispositions de la présente loi. Ce statut peut être commun à l'ensemble des collectivités et établissements mentionnés ci-dessus ou à certains d'entre eux. / (...) / II - Lorsqu'un emploi de la commune, du département de Paris ou de leurs établissements publics est équivalent à un emploi de la fonction publique de l'Etat, le statut particulier de l'emploi de ces collectivités et établissements et la rémunération qui lui est afférente sont fixés par référence à l'emploi de l'Etat. / Lorsqu'un emploi des collectivités ou établissements mentionnés à l'alinéa précédent est équivalent à un emploi de la fonction publique territoriale, le statut particulier de l'emploi de ces collectivités et établissements et la rémunération qui lui est afférente sont fixés par référence à l'emploi territorial. / (...) / Les statuts particuliers et les rémunérations des emplois définis comme ne relevant d'aucune des catégories d'emplois mentionnés ci-dessus sont déterminés dans des conditions fixées par décret en Conseil d'Etat. (...) ". Aux termes de l'article 28 du décret du 24 mai 1994 portant dispositions statutaires relatives aux personnels des administrations parisiennes: " L'organe délibérant de l'administration parisienne concernée, ou le conseil de Paris pour les corps communs à plusieurs administrations parisiennes, détermine par délibération, après avis du Conseil supérieur des administrations parisiennes, la référence des emplois des administrations parisiennes qui sont équivalents à un emploi de la fonction publique de l'Etat, de la fonction publique territoriale ou de la fonction publique hospitalière. / Les statuts particuliers et les rémunérations des emplois des administrations parisiennes mentionnés à l'alinéa précédent sont fixés par référence à ceux de l'emploi équivalent ".<br/>
<br/>
              3. Il résulte de l'ensemble de ces dispositions que l'organe délibérant de l'administration parisienne concernée, ou le conseil de Paris pour les corps communs à plusieurs administrations parisiennes, est tenu, pour fixer les statuts particuliers et les rémunérations des emplois des administrations parisiennes, de rechercher s'il existe des emplois équivalents relevant de la fonction publique de l'Etat, de la fonction publique territoriale ou de la fonction publique hospitalière et, dans cette hypothèse, de se référer aux statuts particuliers régissant ces emplois. En cas de modification du statut particulier d'un corps de fonctionnaires d'une administration parisienne, l'organe délibérant concerné doit prendre en compte les règles prévues par le statut particulier du corps ou du cadre d'emplois de la fonction publique de l'Etat, de la fonction publique territoriale ou de la fonction publique hospitalière lorsque celui-ci sert de cadre de référence.<br/>
<br/>
              4. Il ressort des pièces du dossier soumis aux juges du fond que le syndicat des cadres techniques de la ville de Paris et autres ont soutenu devant la cour administrative d'appel que les règles statutaires régissant les emplois que les architectes-voyers de la commune de Paris ont vocation à occuper devaient être fixées par référence aux statuts particuliers du corps des architectes et urbanistes de l'Etat ou du cadre d'emplois des ingénieurs territoriaux et que la délibération n° 2008-27 des 7 et 8 juillet 2008 du conseil de Paris s'écartait des règles fixées par ces statuts particuliers, en méconnaissance des dispositions citées au point 3, notamment en créant au sein du corps des architectes-voyers de la commune de Paris les spécialités " architecte-voyer " et " paysagiste ". En écartant ce moyen au motif que le corps des architectes et urbanistes de l'Etat et le cadre d'emplois des ingénieurs territoriaux ne comportaient pas de spécialité " paysagiste " et en en déduisant qu'il n'existait pas d'emploi équivalent que les fonctionnaires appartenant à ces corps ou cadre d'emplois auraient vocation à occuper sans rechercher si les emplois du corps des architectes-voyers de la commune de Paris, avant la modification statutaire envisagée, pouvaient être regardés comme équivalents à ceux du corps des architectes et urbanistes de l'Etat ou à ceux du cadre d'emplois des ingénieurs territoriaux, la cour administrative d'appel a commis une erreur de droit. Dès lors et sans qu'il soit besoin d'examiner les autres moyens de leur pourvoi, le syndicat des cadres techniques de la ville de Paris et autres sont fondés à demander l'annulation de cet arrêt.<br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la ville de Paris le versement au syndicat des cadres techniques de la ville de Paris et autres une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. Ces dispositions font obstacle à ce qu'une somme soit mise au même titre à la charge du syndicat des cadres techniques de la ville de Paris et autres, qui ne sont pas la partie perdante dans la présente instance.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative de Paris du 3 juillet 2014 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Paris.<br/>
<br/>
Article 3 : La ville de Paris versera au syndicat des cadres techniques de la ville de Paris et autres la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. Les conclusions présentées par la ville de Paris au titre des mêmes dispositions sont rejetées.<br/>
<br/>
Article 4 : La présente décision sera notifiée au syndicat des cadres techniques de la ville de Paris, à M. I... B..., à M. E... F..., à M. J... D..., à Mme G...K..., à M. H... C..., à M. J... A...et à la ville de Paris.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">135-06-01-01 COLLECTIVITÉS TERRITORIALES. DISPOSITIONS PARTICULIÈRES À CERTAINES COLLECTIVITÉS. COLLECTIVITÉS DE LA RÉGION ILE-DE-FRANCE. DISPOSITIONS PARTICULIÈRES À PARIS. - STATUTS PARTICULIERS DES EMPLOIS DES ADMINISTRATIONS PARISIENNES - 1) PRINCIPE - FIXATION PAR RÉFÉRENCE AUX EMPLOIS ÉQUIVALENTS DE LA FONCTION PUBLIQUE DE L'ETAT, DE LA FONCTION PUBLIQUE TERRITORIALE OU DE LA FONCTION PUBLIQUE HOSPITALIÈRE, Y COMPRIS EN CAS DE MODIFICATION D'UN STATUT PARTICULIER - 2) ESPÈCE - NÉCESSITÉ DE RECHERCHER L'EXISTENCE D'EMPLOIS ÉQUIVALENTS AVANT LA MODIFICATION STATUTAIRE ENVISAGÉE - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">36-02-02-01 FONCTIONNAIRES ET AGENTS PUBLICS. CADRES ET EMPLOIS. CRÉATION, TRANSFORMATION OU SUPPRESSION DE CORPS, DE CADRES D'EMPLOIS, GRADES ET EMPLOIS. RÉFORME STATUTAIRE. - STATUTS PARTICULIERS DES EMPLOIS DES ADMINISTRATIONS PARISIENNES - 1) PRINCIPE - FIXATION PAR RÉFÉRENCE AUX EMPLOIS ÉQUIVALENTS DE LA FONCTION PUBLIQUE DE L'ETAT, DE LA FONCTION PUBLIQUE TERRITORIALE OU DE LA FONCTION PUBLIQUE HOSPITALIÈRE, Y COMPRIS EN CAS DE MODIFICATION D'UN STATUT PARTICULIER - 2) ESPÈCE - NÉCESSITÉ DE RECHERCHER L'EXISTENCE D'EMPLOIS ÉQUIVALENTS AVANT LA MODIFICATION STATUTAIRE ENVISAGÉE - EXISTENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">36-07-02-03 FONCTIONNAIRES ET AGENTS PUBLICS. STATUTS, DROITS, OBLIGATIONS ET GARANTIES. STATUTS SPÉCIAUX. FONCTIONNAIRES ET AGENTS DE LA VILLE DE PARIS. - STATUTS PARTICULIERS DES EMPLOIS DES ADMINISTRATIONS PARISIENNES - 1) PRINCIPE - FIXATION PAR RÉFÉRENCE AUX EMPLOIS ÉQUIVALENTS DE LA FONCTION PUBLIQUE DE L'ETAT, DE LA FONCTION PUBLIQUE TERRITORIALE OU DE LA FONCTION PUBLIQUE HOSPITALIÈRE, Y COMPRIS EN CAS DE MODIFICATION D'UN STATUT PARTICULIER - 2) ESPÈCE - NÉCESSITÉ DE RECHERCHER L'EXISTENCE D'EMPLOIS ÉQUIVALENTS AVANT LA MODIFICATION STATUTAIRE ENVISAGÉE - EXISTENCE.
</SCT>
<ANA ID="9A"> 135-06-01-01 1) Il résulte des dispositions des articles 4 et 118 de la loi n° 84-53 du 26 janvier 1984 et de l'article 28 du décret n° 94-415 du 24 mai 1994 que l'organe délibérant de l'administration parisienne concernée, ou le conseil de Paris pour les corps communs à plusieurs administrations parisiennes, est tenu, pour fixer les statuts particuliers et les rémunérations des emplois des administrations parisiennes, de rechercher s'il existe des emplois équivalents relevant de la fonction publique de l'Etat, de la fonction publique territoriale ou de la fonction publique hospitalière et, dans cette hypothèse, de se référer aux statuts particuliers régissant ces emplois. En cas de modification du statut particulier d'un corps de fonctionnaires d'une administration parisienne, l'organe délibérant concerné doit prendre en compte les règles prévues par le statut particulier du corps ou cadre d'emplois de la fonction publique de l'Etat, de la fonction publique territoriale ou de la fonction publique hospitalière lorsque celui-ci sert de cadre de référence.... ,,2) Requérants soutenant que les règles statutaires régissant les emplois que les architectes-voyers de la ville de Paris ont vocation à occuper devaient être fixées par référence aux statuts particuliers du corps des architectes et urbanistes de l'Etat ou du cadre d'emplois des ingénieurs territoriaux, et que la délibération du conseil de Paris comportant la modification statutaire envisagée s'écartait des règles fixées par ces statuts particuliers en créant au sein du corps des architectes-voyers de la ville de Paris les spécialités architecte-voyer et paysagiste. Erreur de droit commise par la cour à avoir écarté ce moyen en déduisant de la circonstance qu'il n'existait pas de spécialité paysagiste dans le corps des architectes et urbanistes de l'Etat ou dans le cadre d'emplois des ingénieurs territoriaux le fait qu'il n'existait pas d'emploi équivalent que les fonctionnaires appartenant à ces corps ou cadre d'emplois auraient vocation à occuper, sans rechercher si les emplois du corps des architectes-voyers de la ville de Paris pouvaient, avant la modification statutaire envisagée, être regardés comme équivalents à ceux du corps des architectes et urbanistes de la Ville de Paris ou à ceux du cadre d'emplois des ingénieurs territoriaux.</ANA>
<ANA ID="9B"> 36-02-02-01 1) Il résulte des dispositions des articles 4 et 118 de la loi n° 84-53 du 26 janvier 1984 et de l'article 28 du décret n° 94-415 du 24 mai 1994 que l'organe délibérant de l'administration parisienne concernée, ou le conseil de Paris pour les corps communs à plusieurs administrations parisiennes, est tenu, pour fixer les statuts particuliers et les rémunérations des emplois des administrations parisiennes, de rechercher s'il existe des emplois équivalents relevant de la fonction publique de l'Etat, de la fonction publique territoriale ou de la fonction publique hospitalière et, dans cette hypothèse, de se référer aux statuts particuliers régissant ces emplois. En cas de modification du statut particulier d'un corps de fonctionnaires d'une administration parisienne, l'organe délibérant concerné doit prendre en compte les règles prévues par le statut particulier du corps ou cadre d'emplois de la fonction publique de l'Etat, de la fonction publique territoriale ou de la fonction publique hospitalière lorsque celui-ci sert de cadre de référence.... ,,2) Requérants soutenant que les règles statutaires régissant les emplois que les architectes-voyers de la ville de Paris ont vocation à occuper devaient être fixées par référence aux statuts particuliers du corps des architectes et urbanistes de l'Etat ou du cadre d'emplois des ingénieurs territoriaux, et que la délibération du conseil de Paris comportant la modification statutaire envisagée s'écartait des règles fixées par ces statuts particuliers en créant au sein du corps des architectes-voyers de la ville de Paris les spécialités architecte-voyer et paysagiste. Erreur de droit commise par la cour à avoir écarté ce moyen en déduisant de la circonstance qu'il n'existait pas de spécialité paysagiste dans le corps des architectes et urbanistes de l'Etat ou dans le cadre d'emplois des ingénieurs territoriaux le fait qu'il n'existait pas d'emploi équivalent que les fonctionnaires appartenant à ces corps ou cadre d'emplois auraient vocation à occuper, sans rechercher si les emplois du corps des architectes-voyers de la ville de Paris pouvaient, avant la modification statutaire envisagée, être regardés comme équivalents à ceux du corps des architectes et urbanistes de la Ville de Paris ou à ceux du cadre d'emplois des ingénieurs territoriaux.</ANA>
<ANA ID="9C"> 36-07-02-03 1) Il résulte des dispositions des articles 4 et 118 de la loi n° 84-53 du 26 janvier 1984 et de l'article 28 du décret n° 94-415 du 24 mai 1994 que l'organe délibérant de l'administration parisienne concernée, ou le conseil de Paris pour les corps communs à plusieurs administrations parisiennes, est tenu, pour fixer les statuts particuliers et les rémunérations des emplois des administrations parisiennes, de rechercher s'il existe des emplois équivalents relevant de la fonction publique de l'Etat, de la fonction publique territoriale ou de la fonction publique hospitalière et, dans cette hypothèse, de se référer aux statuts particuliers régissant ces emplois. En cas de modification du statut particulier d'un corps de fonctionnaires d'une administration parisienne, l'organe délibérant concerné doit prendre en compte les règles prévues par le statut particulier du corps ou cadre d'emplois de la fonction publique de l'Etat, de la fonction publique territoriale ou de la fonction publique hospitalière lorsque celui-ci sert de cadre de référence.... ,,2) Requérants soutenant que les règles statutaires régissant les emplois que les architectes-voyers de la ville de Paris ont vocation à occuper devaient être fixées par référence aux statuts particuliers du corps des architectes et urbanistes de l'Etat ou du cadre d'emplois des ingénieurs territoriaux, et que la délibération du conseil de Paris comportant la modification statutaire envisagée s'écartait des règles fixées par ces statuts particuliers en créant au sein du corps des architectes-voyers de la ville de Paris les spécialités architecte-voyer et paysagiste. Erreur de droit commise par la cour à avoir écarté ce moyen en déduisant de la circonstance qu'il n'existait pas de spécialité paysagiste dans le corps des architectes et urbanistes de l'Etat ou dans le cadre d'emplois des ingénieurs territoriaux le fait qu'il n'existait pas d'emploi équivalent que les fonctionnaires appartenant à ces corps ou cadre d'emplois auraient vocation à occuper, sans rechercher si les emplois du corps des architectes-voyers de la ville de Paris pouvaient, avant la modification statutaire envisagée, être regardés comme équivalents à ceux du corps des architectes et urbanistes de la Ville de Paris ou à ceux du cadre d'emplois des ingénieurs territoriaux.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
