<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028938247</ID>
<ANCIEN_ID>JG_L_2007_05_000000267700</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/93/82/CETATEXT000028938247.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème / 8ème SSR, 30/05/2007, 267700</TITRE>
<DATE_DEC>2007-05-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>267700</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème / 8ème SSR</FORMATION>
<TYPE_REC>Contentieux des pensions</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LAUGIER, CASTON ; SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>M. Gilles  Bardou</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Séners</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2007:267700.20070530</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 18 mai et 20 septembre 2004 au secrétariat du contentieux du Conseil d'Etat, présentés pour les HOPITAUX UNIVERSITAIRES DE STRASBOURG, dont le siège est 1, Place de l'Hôpital à Strasbourg (67000) ; les HOPITAUX UNIVERSITAIRES DE STRASBOURG demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement du 12 mars 2004 par lequel le tribunal administratif de Strasbourg, faisant partiellement droit à la demande présentée par M. A... B..., a, d'une part, annulé la décision du 22 février 2001 du directeur des HOPITAUX UNIVERSITAIRES DE STRASBOURG refusant la révision de la pension de l'intéressé, et, d'autre part, leur a enjoint de modifier, à compter du 1er janvier 1997, l'assiette de ladite pension conformément aux indications énoncées dans les motifs de ce jugement ;<br/>
<br/>
              2°) statuant au fond, de rejeter la demande présentée par M. B...devant le tribunal administratif de Strasbourg ;<br/>
<br/>
              3°) de mettre à la charge de M. B...la somme de 300 euros en application de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
                          Vu les autres pièces du dossier ;<br/>
               Vu le code de la sécurité sociale ; <br/>
<br/>
              Vu la loi locale du 31 mars 1873 portant statut des fonctionnaires ; <br/>
<br/>
              Vu la loi communale locale du 6 juin 1895 ;<br/>
<br/>
              Vu la loi du 22 juillet 1923 relative au statut des fonctionnaires d'Alsace et de Lorraine ; <br/>
<br/>
              Vu le décret n° 83-1025 du 28 novembre 1983 concernant les relations entre l'administration et les usagers ;<br/>
<br/>
              Vu le règlement des pensions et des allocations aux survivants des employés titulaires et auxiliaires de la ville de Strasbourg ;<br/>
              Vu le code de justice administrative ; <br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Gilles Bardou, Conseiller d'Etat,<br/>
<br/>
              - les observations de la SCP Waquet, Farge, Hazan, avocat des HOPITAUX UNIVERSITAIRES DE STRASBOURG et de la SCP Laugier, Caston, avocat de M. A...B...,<br/>
<br/>
              - les conclusions de M. François Séners, Commissaire du gouvernement ;<br/>
<br/>
<br/>
<br/>Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. B..., employé des HOPITAUX UNIVERSITAIRES DE STRASBOURG relevant du régime de droit local applicable aux fonctionnaires territoriaux dans les départements du Haut-Rhin, du Bas-Rhin et de la Moselle, a été admis par une décision du 13 juin 1989 à faire valoir ses droits à la retraite à compter du 10 septembre 1989 ; que, le 24 janvier 2001, M.  B...a saisi le directeur général des HOPITAUX UNIVERSITAIRES DE STRASBOURG d'une demande tendant à ce que soient inclus, dans les bases de liquidation de sa pension de retraite, divers avantages et éléments accessoires de rémunération dont il avait bénéficié pendant ses années de service ; que par une décision en date du 22 février 2001, le directeur général des HOPITAUX UNIVERSITAIRES DE STRASBOURG a rejeté cette demande ; que les HOPITAUX UNIVERSITAIRES DE STRASBOURG se pourvoient en cassation contre le jugement du 12 mars 2004 du tribunal administratif de Strasbourg en tant que celui-ci, après avoir accueilli pour la période antérieure au 1er janvier 1997 l'exception de prescription quadriennale opposée par le directeur général des HOPITAUX UNIVERSITAIRES DE STRASBOURG, d'une part, a annulé la décision du 22 février 2001 de cette autorité en tant qu'elle refuse la révision de la pension de M. B...à compter du 1er janvier 1997, et, d'autre part, a enjoint aux HOPITAUX UNIVERSITAIRES DE STRASBOURG de modifier, à compter de cette date, l'assiette de la pension concédée à l'intéressé "en intégrant dans sa liquidation les versements à caractère d'accessoire régulier de la rémunération visés dans sa requête et non contestés par le défendeur, perçus par M. B...lorsqu'il était en activité" ; que, par ailleurs, M.  B...forme un pourvoi incident contre le même jugement en tant qu'il a rejeté ses conclusions relatives au paiement trimestriel à l'avance de sa pension ;<br/>
<br/>
              Sur le pourvoi principal :<br/>
<br/>
              Sans qu'il soit besoin d'examiner les autres moyens du pourvoi ;<br/>
<br/>
              Considérant qu'aux termes de l'article 9 du règlement des pensions et des allocations aux survivants des employés titulaires et auxiliaires de la ville de Strasbourg, pris en application de la loi municipale locale du 6 juin 1895 et rendu applicable aux agents des HOPITAUX UNIVERSITAIRES DE STRASBOURG ayant opté pour le statut de droit local par une délibération du 19 novembre 1935 de la commission administrative de cet établissement : "Le montant de la pension est calculé sur la base de la totalité des émoluments de service imputables pour la pension que l'employé touchait en dernier lieu" ; qu'en l'absence de toute précision dans ce règlement sur la portée à donner à cette disposition, il y a lieu de se référer aux dispositions similaires de la loi du 31 mars 1873 relative au statut des fonctionnaires d'Empire, étendue aux fonctionnaires d'Alsace et de Moselle par la loi du 23 décembre 1873 et dont le bénéfice a pu être conservé par les agents nommés après le 11 novembre 1918 grâce au droit d'option ouvert par la loi du 22 juillet 1923 ; <br/>
<br/>
              Considérant qu'aux termes de l'article 42 de la loi locale du 31 mars 1873 : "La pension est calculée sur l'ensemble des émoluments de service que le fonctionnaire touchait en dernier lieu, et ce, selon les règles ci-après: / 1° L'indemnité de logement entre en compte selon les dispositions spéciales édictées à cet effet ; si le budget de l'Empire prévoit expressément la somme pour laquelle le droit du fonctionnaire à un logement gratuit doit entrer dans le calcul, on prendra cette somme ; / 2° Les suppléments attachés à certaines fonctions ou à certains postes, les indemnités de vie chère ou autres, s'il n'en est pas décidé autrement dans le budget, entrent en compte lorsqu'ils figurent sous les divers chapitres des traitements ; (...) 4° Les revenus qui, de leur nature, sont susceptibles d'augmentation ou de diminution n'entrent dans le calcul que s'ils sont expressément alloués ou désignés dans le budget de l'Empire comme susceptibles de compter pour la pension (...)"; que, pour l'application de ces dispositions, les avantages et suppléments de rémunération pris en compte pour le calcul de la pension en raison de "dispositions spéciales" ou de leur mention à cet effet dans le "budget" ou "le budget de l'Empire" s'entendent désormais de ceux qui, en vertu d'une disposition de la loi nationale et, par suite, grâce aux dotations budgétaires correspondantes, sont inclus dans les bases de liquidation de la pension de retraite des fonctionnaires relevant du cadre national ; que, par suite, en faisant droit à la demande de révision de M. B...par simple application des dispositions de l'article 9 du règlement précité sans rechercher si les avantages et éléments accessoires de rémunération en cause étaient ou non inclus dans les bases de liquidation de la pension des fonctionnaires relevant du cadre national, le tribunal administratif de Strasbourg a commis une erreur de droit ; que son jugement doit dans cette mesure être annulé ;<br/>
<br/>
              Sur le pourvoi incident de M.B... :<br/>
<br/>
              Considérant que, par la voie du pourvoi incident, M.  B...demande l'annulation du jugement du tribunal administratif en tant que celui-ci a rejeté ses conclusions tendant à ce que sa pension de retraite lui soit désormais payée trimestriellement et par avance et non mensuellement ; qu'alors même qu'il porte sur les modalités du versement de la pension et non sur les conditions de sa liquidation, le pourvoi de M. B...se rattache au même litige relatif à sa pension que celui que soulève le pourvoi principal ; qu'il est par suite recevable ;<br/>
<br/>
              Considérant qu'aux termes de l'article 12 du règlement des pensions et des allocations aux survivants des employés titulaires et auxiliaires de la ville de Strasbourg : "Les pensions sont payées trimestriellement à l'avance" ; qu'il ne ressort pas des pièces du dossier et qu'il n'est pas soutenu que cette disposition aurait été modifiée en ce qui concerne les personnels des HOPITAUX UNIVERSITAIRES DE STRASBOURG ;<br/>
<br/>
              Considérant que, pour rejeter la demande de M.B..., le tribunal s'est fondé non sur les dispositions précitées mais sur celles de l'article D. 357-26 du code de la sécurité sociale aux termes duquel "Le service des arrérages de pensions dues au titre du régime local a lieu mensuellement et d'avance" ; qu'il résulte toutefois des dispositions des articles L. 357-1 et suivants du code de la sécurité sociale que le chapitre VII du titre  V du livre III de ce code n'est applicable qu'aux assurés relevant du code local des assurances sociales du 19 juillet 1911 (assurance des ouvriers) et de la loi du 20 décembre 1911 sur l'assurance des employés dans les départements du Haut-Rhin, du Bas-Rhin et de la Moselle ; que les employés municipaux de la Ville de Strasbourg et des HOPITAUX UNIVERSITAIRES DE STRASBOURG sous statut local ne relèvent pas de ces lois mais des dispositions de  l'article 12 précité du règlement des pensions et des allocations aux survivants des employés titulaires et auxiliaires de la ville de Strasbourg, pris en application de la loi municipale locale du 6 juin 1895 ; que, dès lors, M.  B...est fondé à soutenir que le tribunal administratif de Strasbourg a commis une erreur de droit en écartant sa demande sur ce point ; <br/>
<br/>
              Considérant qu'il résulte de ce qui précède que le jugement du 12 mars 2004 du tribunal administratif de Strasbourg doit être entièrement annulé ;<br/>
<br/>
              Considérant qu'il y a lieu, en application de l'article L. 821-2 du code de justice administrative, de régler l'affaire au fond ; <br/>
<br/>
              Sur la liquidation de la pension :<br/>
<br/>
              Sans qu'il soit besoin de statuer sur la recevabilité de la demande de révision ;<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier que M.  B...a saisi le directeur général des HOPITAUX UNIVERSITAIRES DE STRASBOURG d'une demande tendant à ce que soient incluses, dans les bases de liquidation de sa pension de retraite, l'indemnité de résidence, l'indemnité de difficultés administratives et l'avantage en nature correspondant à un logement de fonction, évalué à 10 % du traitement, dont il avait bénéficié pendant ses années de service ; que les dispositions applicables, interprétées comme il a été dit ci-dessus, font obstacle à leur prise en compte dès lors que ces avantages et indemnités ne sont pas inclus dans les bases de liquidation de la pension des fonctionnaires relevant du cadre national ; que, par suite, M. B...n'est pas fondé à contester le refus opposé à sa demande par les HOPITAUX UNIVERSITAIRES DE STRASBOURG ; que ses conclusions à fin d'injonction doivent par voie de conséquence être rejetées ;<br/>
<br/>
              Sur le paiement de la pension :<br/>
<br/>
              Considérant que, pour les raisons précédemment indiquées, la décision du 22 février 2001 du directeur des HOPITAUX UNIVERSITAIRES DE STRASBOURG refusant que la pension de M. B...soit payée trimestriellement et à l'avance méconnaît les dispositions précitées du règlement des pensions et des allocations aux survivants des employés titulaires et auxiliaires de la ville de Strasbourg et doit être, pour ce motif, annulée ;<br/>
<br/>
              Considérant qu'aux termes de l'article L. 911-1 du code de justice administrative: "Lorsque sa décision implique nécessairement qu'une personne morale de droit public (...) prenne une mesure d'exécution dans un sens déterminé, la juridiction, saisie de conclusions en ce sens, prescrit, par la même décision, cette mesure assortie, le cas échéant, d'un délai d'exécution"; qu'il y a lieu, en application de ces dispositions, de prescrire aux HOPITAUX UNIVERSITAIRES DE STRASBOURG de verser la pension de M. B...trimestriellement et à l'avance à compter de la prochaine échéance de versement ; <br/>
<br/>
              Sur les conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              Considérant que ces dispositions font obstacle à ce que soit mise à la charge de M.B..., qui n'est pas la partie perdante dans la présente instance, la somme que demandent les HOPITAUX UNIVERSITAIRES DE STRASBOURG au titre des frais exposés par eux et non compris dans les dépens ; qu'il y a lieu en revanche de mettre à la charge des HOPITAUX UNIVERSITAIRES DE STRASBOURG une somme de  1 000 euros au titre des frais de même nature exposés par M. B...; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Strasbourg en date du 12 mars 2004 est annulé.<br/>
<br/>
Article 2 : La décision du 22 février 2001 du directeur des HOPITAUX UNIVERSITAIRES DE STRASBOURG refusant à M. B...le versement de sa pension trimestriellement et par avance est annulée. <br/>
Article 3 : Il est enjoint au directeur général des HOPITAUX UNIVERSITAIRES DE STRASBOURG de procéder au versement trimestriel et par avance de la pension servie à M.  B...à compter de la prochaine échéance de versement.<br/>
Article 4 : Les HOPITAUX UNIVERSITAIRES DE STRASBOURG verseront à M. B...la somme de 1 000 euros en application des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 5 : Le surplus des conclusions présentées par M. B...devant le tribunal administratif de Strasbourg est rejeté.<br/>
<br/>
Article 6  : Les conclusions présentées par les HOPITAUX UNIVERSITAIRES DE STRASBOURG sur le fondement de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 7 : La présente décision sera notifiée aux HOPITAUX UNIVERSITAIRES DE STRASBOURG et à M. A...B...et au ministre de la santé, de la jeunesse et des sports.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">48-02-04 PENSIONS. PENSIONS CIVILES ET MILITAIRES DE RETRAITE. CONTENTIEUX DES PENSIONS CIVILES ET MILITAIRES DE RETRAITE. - VOIES DE RECOURS - CASSATION - POURVOI PRINCIPAL PORTANT SUR LES CONDITIONS DE LIQUIDATION D'UNE PENSION - POURVOI INCIDENT PORTANT SUR LES MODALITÉS DU VERSEMENT DE LA PENSION - LITIGE DISTINCT - ABSENCE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-08-02-004-01 PROCÉDURE. VOIES DE RECOURS. CASSATION. RECEVABILITÉ. RECEVABILITÉ DES POURVOIS. - CONTENTIEUX DES PENSIONS - POURVOI PRINCIPAL PORTANT SUR LES CONDITIONS DE LIQUIDATION D'UNE PENSION - POURVOI INCIDENT PORTANT SUR LES MODALITÉS DU VERSEMENT DE LA PENSION - LITIGE DISTINCT - ABSENCE [RJ1].
</SCT>
<ANA ID="9A"> 48-02-04 Pourvoi principal tendant à l'annulation du jugement par lequel le tribunal administratif, faisant partiellement droit à la demande du pensionné, a annulé la décision de l'autorité administrative refusant la révision de la pension de l'intéressé. Par la voie du pourvoi incident, celui-ci demande l'annulation du jugement du tribunal administratif en tant qu'il a rejeté ses conclusions tendant à ce que sa pension de retraite lui soit désormais payée trimestriellement et par avance et non mensuellement. Ce pourvoi est recevable dès lors qu'il se rattache au même litige, relatif à la pension, que celui que soulève le pourvoi principal, alors même qu'il porte sur les modalités du versement de la pension et non sur les conditions de sa liquidation.</ANA>
<ANA ID="9B"> 54-08-02-004-01 Pourvoi principal tendant à l'annulation du jugement par lequel le tribunal administratif, faisant partiellement droit à la demande du pensionné, a annulé la décision de l'autorité administrative refusant la révision de la pension de l'intéressé. Par la voie du pourvoi incident, celui-ci demande l'annulation du jugement du tribunal administratif en tant qu'il a rejeté ses conclusions tendant à ce que sa pension de retraite lui soit désormais payée trimestriellement et par avance et non mensuellement. Ce pourvoi est recevable dès lors qu'il se rattache au même litige, relatif à la pension, que celui que soulève le pourvoi principal, alors même qu'il porte sur les modalités du versement de la pension et non sur les conditions de sa liquidation.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Comp. 12 décembre 2003, Ministre de la défense c/ M. Nollevaux, n° 249310, T. p. 881.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
