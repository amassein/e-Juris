<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034808217</ID>
<ANCIEN_ID>JG_L_2017_05_000000395321</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/80/82/CETATEXT000034808217.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 1ère chambres réunies, 24/05/2017, 395321</TITRE>
<DATE_DEC>2017-05-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>395321</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SEVAUX, MATHONNET</AVOCATS>
<RAPPORTEUR>M. Cyrille Beaufils</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:395321.20170524</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              1° Sous le n° 395321, par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 15 décembre 2015, 16 mars 2016 et 24 mars 2017 au secrétariat du contentieux du Conseil d'Etat, le syndicat de la magistrature et le syndicat des avocats de France demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2015-1272 du 13 octobre 2015 pris pour l'application des articles 41-1-1 du code de procédure pénale et L. 132-10-1 du code de la sécurité intérieure ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              2° Sous le n° 395509, par un requête sommaire et un mémoire complémentaire, enregistrés les 16 décembre 2015 et 16 mars 2016 au secrétariat du contentieux du Conseil d'Etat, le syndicat national des magistrats Force ouvrière demande au Conseil d'Etat d'annuler pour excès de pouvoir ce même décret. <br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment ses articles 61-1 et 62 ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code de procédure pénale ;<br/>
              - le code de la sécurité intérieure ;<br/>
              - l'arrêté du 8 décembre 2014 du garde des sceaux, ministre de la justice portant création d'une commission permanente d'études au ministère de la justice et d'une commission permanente d'études de service déconcentré placée auprès de chaque premier président de cour d'appel ;<br/>
              - la décision du 27 juin 2016 par laquelle le Conseil d'Etat statuant au contentieux a renvoyé au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par le syndicat de la magistrature et le syndicat des avocats de France ;<br/>
              -	la décision n° 2016-569 QPC du 23 septembre 2016 du Conseil constitutionnel statuant sur la question prioritaire de constitutionnalité soulevée par le syndicat de la magistrature et le syndicat des avocats de France ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Cyrille Beaufils, auditeur,  <br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Sevaux, Mathonnet, avocat du syndicat de la magistrature et autre ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que la requête du syndicat de la magistrature et du syndicat des avocats de France ainsi que celle du syndicat national des magistrats Force ouvrière sont dirigées contre le décret du 13 octobre 2015, pris pour l'application, d'une part, de l'article 41-1-1 du code de procédure pénale, qui permet, dans les conditions qu'il prévoit, aux officiers de police judiciaire de proposer une transaction pénale aux auteurs de certains délits, et, d'autre part, de l'article L. 132-10-1 du code de la sécurité intérieure, relatif au conseil départemental de prévention de la délinquance, dispositions issues de la loi du 15 août 2014 relative à l'individualisation des peines et renforçant l'efficacité des sanctions pénales ; qu'il y a lieu de les joindre pour statuer par une seule décision ;<br/>
<br/>
              Sur la légalité externe du décret attaqué :<br/>
<br/>
              2. Considérant, d'une part, que lorsque, comme en l'espèce, un décret doit être pris en Conseil d'Etat, le texte retenu par le Gouvernement ne peut être différent à la fois du projet qu'il a soumis au Conseil d'Etat et du texte adopté par ce dernier ; qu'il ressort des pièces versées au dossier par le Premier ministre que le décret attaqué ne comporte aucune disposition différant à la fois de celles qui figuraient dans le projet du Gouvernement et de celles qui ont été adoptées par la section de l'intérieur du Conseil d'Etat ; que, dès lors, le moyen tiré de la méconnaissance des règles qui gouvernent l'examen par le Conseil d'Etat des projets de décret doit être écarté ;<br/>
<br/>
              3. Considérant, d'autre part, que l'article 2 de l'arrêté du 8 décembre 2014 du garde des sceaux, ministre de la justice portant création d'une commission permanente d'études au ministère de la justice et d'une commission permanente d'études de service déconcentré placée auprès de chaque premier président de cour d'appel prévoit que cette commission permanente d'études " est chargée de donner un avis sur les problèmes concernant le statut des magistrats de l'ordre judiciaire et des fonctionnaires des services judiciaires, les structures judiciaires et les conditions de fonctionnement et d'équipement des juridictions. / Elle peut, en outre, être consultée sur les projets de textes législatifs et réglementaires élaborés à l'initiative du ministère de la justice et ayant une incidence directe sur le fonctionnement des juridictions. " ; que ni ces dispositions ni aucune autre disposition législative ou réglementaire n'imposaient de consulter cette commission préalablement à l'édiction du décret attaqué ; que, par suite et en tout état de cause, le moyen tiré de ce que ce décret serait intervenu au terme d'une procédure irrégulière, faute d'avoir été précédé de la consultation de cette commission, doit être écarté ;<br/>
<br/>
              Sur la légalité interne du décret attaqué :<br/>
<br/>
              En ce qui concerne le 1° de l'article 1er : <br/>
<br/>
              4. Considérant, qu'aux termes de l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales : " 1. Toute personne a droit à ce que sa cause soit entendue équitablement, publiquement et dans un délai raisonnable, par un tribunal indépendant et impartial, établi par la loi, qui décidera, soit des contestations sur ses droits et obligations de caractère civil, soit du bien-fondé de toute accusation en matière pénale dirigée contre elle. (...). / (...) / 3. Tout accusé a droit notamment à : / a. être informé, dans le plus court délai, dans une langue qu'il comprend et d'une manière détaillée, de la nature et de la cause de l'accusation portée contre lui ; / b. disposer du temps et des facilités nécessaires à la préparation de sa défense ; / c. se défendre lui-même ou avoir l'assistance d'un défenseur de son choix et, s'il n'a pas les moyens de rémunérer un défenseur, pouvoir être assisté gratuitement par un avocat d'office, lorsque les intérêts de la justice l'exigent ; / d. interroger ou faire interroger les témoins à charge et obtenir la convocation et l'interrogation des témoins à décharge dans les mêmes conditions que les témoins à charge ; / e. se faire assister gratuitement d'un interprète, s'il ne comprend pas ou ne parle pas la langue employée à l'audience. " ;<br/>
<br/>
              5. Considérant que le 1° de l'article 1er du décret attaqué ajoute au code de procédure pénale une section 1 bis intitulée " de la transaction proposée par un officier de police judiciaire ", composée des articles R. 15-33-37-1 à R. 15-33-37-6, qui précise les modalités de mise en oeuvre de cette transaction pénale instituée par l'article 41-1-1 du code de procédure pénale dans sa rédaction issue de la loi du 15 août 2014 relative à l'individualisation des peines et renforçant l'efficacité des sanctions pénales ; que ces dispositions prévoient ainsi, à l'article R. 15-33-37-1, que l'officier de police judiciaire qui propose une transaction à une personne physique ou morale doit demander l'autorisation au procureur de la République, en lui indiquant le montant de l'amende qu'il envisage ainsi que, le cas échéant, les modalités de réparation du dommage causé ; qu'en vertu du nouvel article R. 15-32-37-2, aucune transaction ne peut être proposée pendant la durée d'une garde à vue et en vertu du nouvel article R. 15-33-37-4, si la personne accepte l'amende transactionnelle proposée, l'officier de police judiciaire peut la soumettre, avant l'homologation de la transaction par le président du tribunal de grande instance ou le juge par lui désigné, à l'obligation de consigner une somme d'argent égale au montant de l'amende ; que l'article R. 15-33-37-5 issu également du 1° de l'article 1 du décret contesté prévoit que la consignation vaut paiement de l'amende transactionnelle si la transaction est homologuée, la somme consignée étant restituée à la personne en cas de refus d'homologation, sauf si la juridiction de jugement, saisie le cas échéant en raison des faits qui avaient fait l'objet de la transaction, inflige une amende d'un montant au moins égale à la somme consignée ; qu'enfin, les dispositions du nouvel article R. 15-33-37-6 issues également du 1° du 1 du décret contesté prévoient que l'exécution de la transaction ne fait pas échec au droit de la partie civile de délivrer citation directe devant le tribunal correctionnel qui, composé d'un seul magistrat, ne statue alors que sur les seuls intérêts civils ;<br/>
<br/>
              6. Considérant que le deuxième alinéa de l'article R. 15-33-37-5, introduit dans le code de procédure pénale par le décret attaqué, prévoit, ainsi qu'il a été dit, que la consignation vaut paiement de l'amende transactionnelle si la transaction est homologuée ; que la transaction homologuée revêt ainsi un caractère exécutoire, dès lors qu'elle prive, dans tous les cas, l'auteur de l'infraction de la possibilité de refuser d'acquitter la somme due après l'homologation ; que cette procédure de transaction pénale doit reposer sur l'accord libre et non équivoque de l'auteur des faits, qui implique, notamment, la complète connaissance, par l'intéressé, de la nature des faits reprochés et de leur qualification juridique ; que, toutefois, ni l'article 41-1-1 du code de procédure pénale ni les dispositions de ce code introduites par le 1° de l'article 1er du décret attaqué ne prévoient que la personne à qui est proposée la transaction soit dûment informée de la nature des faits reprochés ainsi que de leur qualification juridique ; qu'il résulte de ce qui précède que le 1° de l'article 1er du décret méconnaît le droit à un procès équitable garanti par l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; que, sans qu'il soit besoin d'examiner les autres moyens dirigés contre ces dispositions, qui sont divisibles des autres dispositions du décret attaqué, les syndicats requérants sont fondés à en demander l'annulation ;<br/>
<br/>
              En ce qui concerne l'article 3 : <br/>
<br/>
              7. Considérant que l'article 3 du décret attaqué insère, dans le code de la sécurité intérieure, un article R. 132-6-1 ; que le I de cet article détermine la composition et le fonctionnement de l'état-major de sécurité et de la cellule de coordination opérationnelle des forces de sécurité intérieure chargés, en vertu de l'article L. 132-10-1 du code de sécurité intérieure, d'animer et de coordonner, sur leur territoire, les actions conduites par l'administration pénitentiaire, les autres services de l'Etat, les collectivités territoriales, les associations et les autres personnes publiques ou privées, en vue de favoriser l'exécution des peines et de prévenir la récidive ; que le II de cet article précise que le procureur de la République communique à ces instances une synthèse de son rapport annuel de politique pénale ; que le III de cet article fixe, à son premier alinéa, les modalités de désignation des personnes condamnées sortant de détention dont l'organisation du suivi et du contrôle en milieu ouvert est confiée à ces instances, en application du 3° de l'article L. 132-10-1, et prévoit, à ses deuxième et troisième alinéas, les conditions dans lesquelles des informations sont échangées, dans ce cadre, entre ces mêmes instances et les juridictions de l'application des peines ainsi que le service pénitentiaire d'insertion et de probation en application du 4° du même article ; <br/>
<br/>
              8. Considérant que, par sa décision n° 2016-569 QPC du 23 septembre 2016, le Conseil constitutionnel a déclaré contraires à la Constitution les mots : " et peuvent se voir transmettre par ces mêmes juridictions et ce même service toute information que ceux-ci jugent utile au bon déroulement du suivi et du contrôle de ces personnes " figurant au 4° du paragraphe I de l'article L. 132-10-1 du code de la sécurité intérieure ; que cette abrogation a pour effet de priver de base légale les deuxième et troisième alinéas du III de l'article R. 132 6-1 ajouté au code de la sécurité intérieure par l'article 3 du décret attaqué, pris pour l'application de ces dispositions ; que ces dispositions, qui sont divisibles des autres dispositions du décret attaqué, doivent par suite être annulées ;<br/>
<br/>
              9. Considérant, en revanche, d'une part, qu'aux termes de l'article L. 132-10-1 du code de la sécurité intérieure : " (...) Dans le cadre de leurs attributions, l'état-major de sécurité et la cellule de coordination opérationnelle des forces de sécurité intérieure : / (...) 3° Organisent les modalités du suivi et du contrôle en milieu ouvert, par les services et personnes publiques ou privées mentionnés au premier alinéa du présent I, des personnes condamnées sortant de détention, désignées par l'autorité judiciaire compte tenu de leur personnalité, de leur situation matérielle, familiale et sociale ainsi que des circonstances de la commission des faits ; (...) " ; qu'aux termes du premier alinéa de l'article 712-1 du code de procédure pénale : " Le juge de l'application des peines et le tribunal de l'application des peines constituent les juridictions de l'application des peines du premier degré qui sont chargées, dans les conditions prévues par la loi, de fixer les principales modalités de l'exécution des peines privatives de liberté ou de certaines peines restrictives de liberté, en orientant et en contrôlant les conditions de leur application. " ; qu'eu égard, notamment, à l'objet du dispositif de suivi et de contrôle en milieu ouvert de personnes condamnées sortant de détention prévu par l'article L. 132-10-1 précité, qui vise à associer à ce suivi des instances locales de coordination impliquées dans la prévention de la délinquance, dans lesquelles siège le procureur de la République, le premier alinéa du III de l'article R. 132-6-1 ajouté au code de la sécurité intérieure par l'article 3 du décret attaqué ne méconnaît ni ces dispositions ni celles de l'article 712-1 du code de procédure pénale précité relatives à la compétence des juridictions de l'application des peines en prévoyant que les personnes faisant l'objet de ce suivi sont désignées par le procureur de la République, après avis favorable du juge de l'application des peines ; que, contrairement à ce qui est soutenu, l'avis rendu par le juge de l'application des peines sur la désignation de ces personnes ne saurait, par lui-même, porter atteinte à l'impartialité de ce juge lorsqu'il aura de nouveau à connaître des modalités de l'application des peines des intéressés ;<br/>
<br/>
              10. Considérant, d'autre part, que le syndicat national des magistrats Force ouvrière soutient qu'en soumettant l'application des peines au contrôle de l'état-major de sécurité et de la cellule de coordination opérationnelle des forces de sécurité intérieure, les mêmes dispositions du premier alinéa du III de l'article R. 132-6-1 ajouté au code de la sécurité intérieure par l'article 3 du décret attaqué méconnaissent la séparation des pouvoirs et le principe constitutionnel d'indépendance de la magistrature ; que toutefois, le législateur ayant prévu, par les dispositions précitées de l'article L. 132-10-1 du code de la sécurité intérieure, que, dans le cadre de leurs attributions, les instances précitées " organisent les modalités du suivi et du contrôle en milieu ouvert (...) des personnes condamnées sortant de détention, désignées par l'autorité judiciaire ", la conformité de ces dispositions à ces principes ne saurait être contestée devant le Conseil d'Etat, statuant au contentieux, en dehors de la procédure prévue à l'article 61-1 de la Constitution ; que le syndicat requérant n'indique pas en quoi les dispositions du décret attaqué les auraient, par elles-mêmes, méconnus ; <br/>
<br/>
              11. Considérant qu'il résulte de ce qui précède que les requérants sont seulement fondés à demander l'annulation du 1° de l'article 1er du décret attaqué et des deuxième et troisième alinéas du III de l'article R. 132-6-1 du code de la sécurité intérieure introduit par l'article 3 de ce même décret ; <br/>
<br/>
<br/>
<br/>
              Sur l'application des dispositions de l'article L. 761-1 du code de justice administrative : <br/>
<br/>
              12. Considérant qu'il y a lieu de faire application de ces dispositions et de mettre à la charge de l'Etat à ce titre le versement au syndicat de la magistrature et au syndicat des avocats de France d'une somme de 1 000 euros chacun ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le 1° de l'article 1er du décret n° 2015-1272 du 13 octobre 2015 pris pour l'application des articles 41-1-1 du code de procédure pénale et L. 132-10-1 du code de la sécurité intérieure, et les deuxième et troisième alinéas du III de l'article R. 132-6-1 du code de la sécurité intérieure introduits par l'article 3 de ce même décret sont annulés. <br/>
<br/>
Article 2 : L'Etat versera au syndicat de la magistrature et au syndicat des avocats de France une somme de 1 000 euros chacun en application de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : Le surplus des conclusions des requêtes du syndicat de la magistrature et du syndicat des avocats de France et du syndicat national des magistrats Force ouvrière est rejeté. <br/>
<br/>
Article 4 : La présente décision sera notifiée au syndicat de la magistrature, au syndicat des avocats de France et au syndicat national des magistrats Force ouvrière. <br/>
Copie en sera adressée au Premier ministre, au ministre d'Etat, ministre de l'intérieur, au ministre d'Etat, garde des sceaux, ministre de la justice et à la ministre des outre-mer. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-04-01-02 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. TRAITÉS ET DROIT DÉRIVÉ. CONVENTION EUROPÉENNE DES DROITS DE L`HOMME (VOIR : DROITS CIVILS ET INDIVIDUELS). - DROIT AU PROCÈS ÉQUITABLE (ART. 6) - MÉCONNAISSANCE - EXISTENCE - DISPOSITIF DE TRANSACTION PÉNALE NE PRÉVOYANT PAS D'INFORMATION DE LA PERSONNE À QUI LA TRANSACTION EST PROPOSÉE SUR LA NATURE DES FAITS REPROCHÉS ET LEUR QUALIFICATION JURIDIQUE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">26-055-01-06-02 DROITS CIVILS ET INDIVIDUELS. CONVENTION EUROPÉENNE DES DROITS DE L'HOMME. DROITS GARANTIS PAR LA CONVENTION. DROIT À UN PROCÈS ÉQUITABLE (ART. 6). VIOLATION. - EXISTENCE - TRANSACTION PÉNALE - DISPOSITIF NE PRÉVOYANT PAS D'INFORMATION DE LA PERSONNE À QUI LA TRANSACTION EST PROPOSÉE SUR LA NATURE DES FAITS REPROCHÉS ET LEUR QUALIFICATION JURIDIQUE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">37-05-02 JURIDICTIONS ADMINISTRATIVES ET JUDICIAIRES. EXÉCUTION DES JUGEMENTS. EXÉCUTION DES PEINES. - SUIVI ET CONTRÔLE EN MILIEU OUVERT DES PERSONNES CONDAMNÉES SORTANT DE DÉTENTION (ART. L. 132-10-1 DU CODE DE LA SÉCURITÉ INTÉRIEURE) - DÉSIGNATION DES PERSONNES FAISANT L'OBJET DE CE SUIVI - AUTORITÉ COMPÉTENTE - PROCUREUR DE LA RÉPUBLIQUE, APRÈS AVIS FAVORABLE DU JUGE DE L'APPLICATION DES PEINES - 1) MÉCONNAISSANCE DE LA COMPÉTENCE DES JURIDICTION DE L'APPLICATION DES PEINES - ABSENCE - 2) ATTEINTE À L'IMPARTIALITÉ DU JUGE DE L'APPLICATION DES PEINES LORSQU'IL AURA À CONNAÎTRE À NOUVEAU DE L'APPLICATION DES PEINES DES INTÉRESSÉS - ABSENCE.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">59-01-01 RÉPRESSION. DOMAINE DE LA RÉPRESSION PÉNALE. PROCÉDURE PÉNALE. - TRANSACTION PÉNALE - GARANTIES À RESPECTER - DROIT À UN PROCÈS ÉQUITABLE (ART. 6 DE LA CONV. EDH) [RJ1] - CONSÉQUENCE - ILLÉGALITÉ D'UN DISPOSITIF DE TRANSACTION PÉNALE NE PRÉVOYANT PAS D'INFORMATION À LA PERSONNE À QUI LA TRANSACTION EST PROPOSÉE SUR LA NATURE DES FAITS REPROCHÉS ET LEUR QUALIFICATION JURIDIQUE.
</SCT>
<ANA ID="9A"> 01-04-01-02 Transaction pénale proposée par un officier de police judiciaire (art. 41-1-1 du code de procédure pénale).... ,,La transaction pénale homologuée par le président du tribunal de grande instance ou le juge par lui désigné revêt un caractère exécutoire. Cette procédure de transaction doit reposer sur l'accord libre et non équivoque de l'auteur des faits qui implique, notamment, la complète connaissance, par l'intéressé de la nature des faits reprochés et de leur qualification juridique. Méconnaît le droit à un procès équitable garanti par l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales (conv. EDH) le dispositif de transaction pénale qui ne prévoit pas que la personne à qui est proposée la transaction est dûment informée de la nature des faits reprochés ainsi que de leur qualification juridique.</ANA>
<ANA ID="9B"> 26-055-01-06-02 Transaction pénale proposée par un officier de police judiciaire (art. 41-1-1 du code de procédure pénale).... ,,La transaction pénale homologuée par le président du tribunal de grande instance ou le juge par lui désigné revêt un caractère exécutoire. Cette procédure de transaction doit reposer sur l'accord libre et non équivoque de l'auteur des faits qui implique, notamment, la complète connaissance, par l'intéressé de la nature des faits reprochés et de leur qualification juridique. Méconnaît le droit à un procès équitable garanti par l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales le dispositif de transaction pénale qui ne prévoit pas que la personne à qui est proposée la transaction est dûment informée de la nature des faits reprochés ainsi que de leur qualification juridique.</ANA>
<ANA ID="9C"> 37-05-02 1) Eu égard, notamment, à l'objet du dispositif de suivi et de contrôle en milieu ouvert de personnes condamnées sortant de détention prévu par l'article L. 132-10-1 du code de la sécurité intérieure, qui vise à associer à ce suivi des instances locales de coordination impliquées dans la prévention de la délinquance, dans lesquelles siège le procureur de la République, le premier alinéa du III de l'article R 132-6-1 issu du décret n° 2015-1272 attaqué ne méconnaît ni ces dispositions ni celles de l'article 712-1 du code de procédure pénale relatives à la compétence des juridictions de l'application des peines en prévoyant que les personnes faisant l'objet de ce suivi sont désignées par le procureur de la République, après avis favorable du juge de l'application des peines.... ,,2) L'avis rendu par le juge de l'application des peines sur la désignation de ces personnes ne saurait, par lui-même, porter atteinte à l'impartialité de ce juge lorsqu'il aura de nouveau à connaître des modalités de l'application des peines des intéressés.</ANA>
<ANA ID="9D"> 59-01-01 Transaction pénale proposée par un officier de police judiciaire (art. 41-1-1 du code de procédure pénale).... ,,La transaction pénale homologuée par le président du tribunal de grande instance ou le juge par lui désigné revêt un caractère exécutoire. Cette procédure de transaction doit reposer sur l'accord libre et non équivoque de l'auteur des faits qui implique, notamment, la complète connaissance, par l'intéressé de la nature des faits reprochés et de leur qualification juridique. Méconnaît le droit à un procès équitable garanti par l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales (conv. EDH) le dispositif de transaction pénale qui ne prévoit pas que la personne à qui est proposée la transaction est dûment informée de la nature des faits reprochés ainsi que de leur qualification juridique.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., sur les garanties constitutionnelles à respecter, CE, Assemblée, 7 juillet 2006, France Nature Environnement, n° 283178, p. 328.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
