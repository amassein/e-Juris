<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029709176</ID>
<ANCIEN_ID>JG_L_2014_11_000000369658</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/70/91/CETATEXT000029709176.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème et 9ème sous-sections réunies, 05/11/2014, 369658</TITRE>
<DATE_DEC>2014-11-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>369658</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème et 9ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>M. Jacques Reiller</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Delphine Hedary</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:369658.20141105</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 25 juin et 25 septembre 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. B... A..., demeurant ...; M. A...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler la décision n° 12012125 du 12 mars 2013 par laquelle la Cour nationale du droit d'asile a rejeté sa demande tendant à l'annulation de la décision du 20 avril 2012 du directeur général de l'Office français de protection des réfugiés et apatrides refusant de lui reconnaître la qualité de réfugié ou, à défaut, de lui accorder le bénéfice de la protection subsidiaire ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement à la SCP Waquet-Farge-Hazan, son avocat, de la somme de 2 500 euros au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991, moyennant renonciation à percevoir la part contributive de l'Etat ;<br/>
<br/>
<br/>
              Vu l'arrêt attaqué ;<br/>
<br/>
              Vu les pièces dont il résulte que le pourvoi a été communiqué à l'Office français de protection des réfugiés et apatrides, qui n'a pas produit de mémoire ; <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention de Genève du 28 juillet 1951 relative au statut des réfugiés et le protocole signé à New York le 31 janvier 1967 ;<br/>
<br/>
              Vu la loi n° 91-647 du 10 juillet 1991 ; <br/>
<br/>
              Vu le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jacques Reiller, conseiller d'Etat,<br/>
<br/>
              - les conclusions de Mme Delphine Hedary, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de M.A... ;<br/>
<br/>
<br/>
<br/>
<br/>1.	Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par une décision en date du 2 avril 2008, la Cour nationale du droit d'asile a rejeté un premier recours introduit par M.A..., ressortissant turc, dirigé contre la décision du 30 juillet 2007 du directeur général de l'Office français de protection des réfugiés et apatrides refusant de lui reconnaître le statut de réfugié ou, à défaut, de lui accorder le bénéfice de la protection subsidiaire ; que, saisi d'une nouvelle demande de l'intéressé, le directeur général de l'OFPRA l'a rejetée par une décision en date du 20 avril 2012 ; que M. A...se pourvoit contre la décision du 12 mars 2013 par laquelle la Cour nationale du droit d'asile, après avoir reconnu l'existence d'un fait nouveau, postérieur à sa précédente décision juridictionnelle, susceptible de justifier les craintes de persécutions que le demandeur invoque en cas de retour dans son pays d'origine, a rejeté sa demande tendant à l'annulation de la décision de l'Office français des réfugiés rejetant, à nouveau, sa demande d'asile; <br/>
<br/>
              2. Considérant qu'en vertu du 2° du A de l'article 1er de la convention de Genève du 28 juillet 1951 modifié par l'article 1er du 2 du protocole signé le 31 janvier 1967 à New York, la qualité de réfugié est notamment reconnue " à toute personne qui, craignant avec raison d'être persécutée du fait de sa race, de sa religion, de sa nationalité, de son appartenance à un certain groupe social ou de ses opinions politiques, se trouve hors du pays dont elle a la nationalité et qui ne peut, ou du fait de cette crainte, ne veut se réclamer de la protection de ce pays (...) " ; qu'aux termes de l'article L. 712-1 du code de l'entrée et du séjour des étrangers et du droit d'asile : " Sous réserve des dispositions de l'article L. 712-2, le bénéfice de la protection subsidiaire est accordé à toute personne qui ne remplit pas les conditions pour se voir reconnaître la qualité de réfugié mentionnées à l'article L. 711-1 et qui établit qu'elle est exposée dans son pays à l'une des menaces graves suivantes :/ a) la peine de mort ; b) la torture ou des peines ou traitements inhumains ou dégradants ; c) s'agissant d'un civil, une menace grave, directe et individuelle contre sa vie ou sa personne en raison d'une violence généralisée résultant d'une situation de conflit armé interne ou international " ;<br/>
<br/>
              3. Considérant que, s'il est loisible à l'autorité administrative d'adresser aux autorités du pays d'origine d'un ressortissant étranger en situation irrégulière tout élément en vue de son identification pour assurer la mise en oeuvre d'une mesure d'éloignement prise à son encontre, la transmission à ces autorités, après qu'une demande d'asile a été définitivement rejetée, d'informations relatives au contenu de cette demande constitue un fait nouveau justifiant un nouvel examen à la demande d'asile ; que, lors de ce nouvel examen, la demande d'admission au statut de réfugié ou, le cas échéant, d'octroi de la protection subsidiaire est appréciée, compte tenu notamment du pays d'origine du demandeur, de la nature de l'information et des conditions dans lesquelles elle a été transmise ainsi que des risques encourus ; <br/>
<br/>
              4. Considérant, en premier lieu, qu'il ressort des pièces du dossier soumis aux juges du fond que le préfet d'Eure-et-Loir a, le 10 avril 2012, transmis au consulat de Turquie un procès-verbal d'audition, recueilli par un officier de police judiciaire dans le cadre d'une procédure de vérification d'identité, dans lequel M. A...affirmait, d'une part avoir fui son pays en raison de ses activités politiques et de son refus d'accomplir son service militaire, d'autre part avoir précédemment introduit une demande d'asile en France ; que la Cour nationale du droit d'asile a estimé qu'une telle transmission constituait un fait nouveau, postérieur à sa première décision, de nature à justifier un nouvel examen de la situation de l'intéressé ; qu'en revanche, elle a estimé, après avoir relevé que ni l'engagement politique allégué en faveur de la cause kurde, ni les craintes énoncées en raison de son refus d'effectuer le service militaire ne peuvent être tenus pour établis, que les déclarations de M.A..., consignées dans le procès-verbal litigieux, qui ne comportent la mention d'aucun engagement militant personnel ni en Turquie ni en France, apparaissaient très générales et peu personnalisées s'agissant des motifs politiques supposés de son départ de Turquie en 2007 ; qu'en déduisant de ces constatations de faits exemptes de dénaturation, qu'en l'espèce, la méconnaissance de la garantie de confidentialité, alors même qu'elle a permis aux autorités turques de prendre connaissance de la demande d'asile précédemment rejetée du requérant en France, n'a pas créé à elle seule les conditions d'une exposition à des persécutions au sens des stipulations de la convention de Genève ou à l'une des menaces graves visées par la loi, la Cour nationale du droit d'asile, qui a suffisamment motivé sa décision, n'a pas commis d'erreur de droit ni inexactement qualifié les faits de l'espèce; <br/>
<br/>
              5. Considérant, en deuxième lieu, que la Cour nationale du droit d'asile a relevé, d'une part, que les déclarations de M. A...sont apparues vagues et peu crédibles s'agissant de l'engagement politique qu'il allègue en faveur de la cause kurde ; que, si la Cour a indiqué qu'il s'est exprimé en des termes confus et anachroniques au sujet des formations politiques au sein desquelles il affirme avoir milité, elle ne s'est pas limitée à cette constatation et s'est également fondée, pour rejeter sa demande, sur l'imprécision de sa description du contenu de ses activités et des discours qu'il aurait prononcés, sur le caractère peu spontané de ses déclarations relatives aux recherches dont il ferait l'objet en Turquie, ainsi que sur l'absence de caractère probant des documents produits et présentés comme étant un acte d'accusation en date du 20 juin 2007, un mandat d'arrêt en date du 4 juillet 2007 et un procès-verbal de perquisition établi le même jour ; que, d'autre part, la Cour a estimé que ses déclarations faisant état d'arrestations et de placements en garde à vue de proches, qui auraient eu lieu en 2011 et 2012, soit postérieurement à sa précédente décision en date du 2 avril 2008, étaient sommaires et peu vraisemblables et, partant, ne permettaient pas de tenir ces faits pour établis ; qu'en se livrant à ces appréciations souveraines, la Cour a suffisamment motivé sa décision, eu égard aux moyens invoqués devant elle, et n'a pas dénaturé les faits de l'espèce ni les pièces du dossier qui lui était soumis ;<br/>
<br/>
              6. Considérant, en troisième lieu, que la Cour a estimé, après avoir qualifié de schématiques et sommaires les propos tenus par M. A...s'agissant des craintes suscitées par son refus d'effectuer son service militaire, qu'il n'en ressortait pas que cet acte d'insoumission aurait été dicté par l'un des motifs énoncés à l'article 1er A 2 de la convention de Genève ou par un motif de conscience ; qu'elle a également indiqué qu'il ne résultait pas de l'instruction que son refus d'accomplir son service militaire l'exposerait à l'une des menaces graves visées à l'article L. 712-1 du code de l'entrée et du séjour des étrangers et du droit d'asile en cas de retour dans son pays d'origine ; qu'en se livrant à cette appréciation souveraine, la Cour n'a pas dénaturé les faits de l'espèce ni les pièces du dossier qui lui était soumis ; <br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que M. A...n'est pas fondé à demander l'annulation de la décision qu'il attaque ; <br/>
<br/>
              Sur les conclusions tendant  à l'application de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              8. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que l'Etat, qui n'est pas la partie perdante dans la présente instance, soit condamné à payer la somme que la SCP Waquet, Farge, Hazan, avocat de M. A... demande en application des dispositions de l'article 37 de la loi du 10 juillet 1991 relative à l'aide juridique, pour les frais que le requérant aurait exposés s'il n'avait pas bénéficié de l'aide juridictionnelle ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. A...est rejeté. <br/>
Article 2 : La présente décision sera notifiée à Monsieur B...A...et à l'Office français de protection des réfugiés et apatrides.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">095-01-01 - CONFIDENTIALITÉ DES INFORMATIONS RELATIVES À UNE DEMANDE D'ASILE - TRANSMISSION, APRÈS LE REJET D'UNE DEMANDE D'ASILE, D'INFORMATIONS SUR LE CONTENU DE CETTE DEMANDE AUX AUTORITÉS DU PAYS D'ORIGINE - 1) FAIT NOUVEAU OUVRANT DROIT À RÉEXAMEN DE LA DEMANDE - EXISTENCE - 2) CONSÉQUENCE - RÉEXAMEN DE LA DEMANDE AU REGARD DU PAYS D'ORIGINE, DE LA NATURE DE L'INFORMATION ET DES CONDITIONS DANS LESQUELLES ELLE A ÉTÉ TRANSMISE AINSI QUE DES RISQUES ENCOURUS.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">095-08-08-01-01-01 - TRANSMISSION, APRÈS LE REJET D'UNE DEMANDE D'ASILE, D'INFORMATIONS SUR LE CONTENU DE CETTE DEMANDE AUX AUTORITÉS DU PAYS D'ORIGINE - 1) FAIT NOUVEAU - EXISTENCE - 2) CONSÉQUENCE - RÉEXAMEN AU REGARD DU PAYS D'ORIGINE, DE LA NATURE DE L'INFORMATION ET DES CONDITIONS DANS LESQUELLES ELLE A ÉTÉ TRANSMISE AINSI QUE DES RISQUES ENCOURUS.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">095-08-08-02-02 - TRANSMISSION, APRÈS LE REJET D'UNE DEMANDE D'ASILE, D'INFORMATIONS SUR LE CONTENU DE CETTE DEMANDE AUX AUTORITÉS DU PAYS D'ORIGINE - 1) FAIT NOUVEAU - EXISTENCE - 2) CONSÉQUENCE - RÉEXAMEN AU REGARD DU PAYS D'ORIGINE, DE LA NATURE DE L'INFORMATION ET DES CONDITIONS DANS LESQUELLES ELLE A ÉTÉ TRANSMISE AINSI QUE DES RISQUES ENCOURUS.
</SCT>
<ANA ID="9A"> 095-01-01 1) S'il est loisible à l'autorité administrative d'adresser aux autorités du pays d'origine d'un ressortissant étranger en situation irrégulière tout élément en vue de son identification pour assurer la mise en oeuvre d'une mesure d'éloignement prise à son encontre, la transmission à ces autorités, après qu'une demande d'asile a été définitivement rejetée, d'informations relatives au contenu de cette demande constitue un fait nouveau justifiant un nouvel examen à la demande d'asile.,,,2) Lors de ce nouvel examen, la demande d'admission au statut de réfugié ou, le cas échéant, d'octroi de la protection subsidiaire est appréciée, compte tenu notamment du pays d'origine du demandeur, de la nature de l'information et des conditions dans lesquelles elle a été transmise ainsi que des risques encourus.</ANA>
<ANA ID="9B"> 095-08-08-01-01-01 1) S'il est loisible à l'autorité administrative d'adresser aux autorités du pays d'origine d'un ressortissant étranger en situation irrégulière tout élément en vue de son identification pour assurer la mise en oeuvre d'une mesure d'éloignement prise à son encontre, la transmission à ces autorités, après qu'une demande d'asile a été définitivement rejetée, d'informations relatives au contenu de cette demande constitue un fait nouveau justifiant un nouvel examen à la demande d'asile.,,,2) Lors de ce nouvel examen, la demande d'admission au statut de réfugié ou, le cas échéant, d'octroi de la protection subsidiaire est appréciée, compte tenu notamment du pays d'origine du demandeur, de la nature de l'information et des conditions dans lesquelles elle a été transmise ainsi que des risques encourus.</ANA>
<ANA ID="9C"> 095-08-08-02-02 1) S'il est loisible à l'autorité administrative d'adresser aux autorités du pays d'origine d'un ressortissant étranger en situation irrégulière tout élément en vue de son identification pour assurer la mise en oeuvre d'une mesure d'éloignement prise à son encontre, la transmission à ces autorités, après qu'une demande d'asile a été définitivement rejetée, d'informations relatives au contenu de cette demande constitue un fait nouveau justifiant un nouvel examen à la demande d'asile.,,,2) Lors de ce nouvel examen, la demande d'admission au statut de réfugié ou, le cas échéant, d'octroi de la protection subsidiaire est appréciée, compte tenu notamment du pays d'origine du demandeur, de la nature de l'information et des conditions dans lesquelles elle a été transmise ainsi que des risques encourus.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
