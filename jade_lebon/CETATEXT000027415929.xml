<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027415929</ID>
<ANCIEN_ID>JG_L_2013_05_000000337120</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/41/59/CETATEXT000027415929.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème / 10ème SSR, 17/05/2013, 337120</TITRE>
<DATE_DEC>2013-05-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>337120</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème / 10ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARTHELEMY, MATUCHANSKY, VEXLIARD ; SCP CELICE, BLANCPAIN, SOLTNER</AVOCATS>
<RAPPORTEUR>Mme Séverine Larere</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Claire Legras</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2013:337120.20130517</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 1er mars et 31 mai 2010 au secrétariat du contentieux du Conseil d'Etat, présentés pour la société Isère Développement Environnement (IDE), dont le siège est 36 rue des Vingt Toises à Saint-Martin-le-Vinoux (38950) ; la société IDE demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 07LY02785 de la cour administrative d'appel de Lyon du 31 décembre 2009 en tant qu'après avoir annulé le jugement n° 0404907 du 16 octobre 2007 par lequel le tribunal administratif de Grenoble avait rejeté sa demande tendant à la condamnation de la commune de Renage (Isère) à lui verser la somme de 129 000 euros, avec les intérêts au taux légal majoré de 5 points à compter du 25 juin 2004, en restitution de la participation financière mise à sa charge par l'autorisation de lotir qui lui a été délivrée le 13 février 2001, il a limité le montant de cette restitution à la somme de 23 611,14 euros, avec intérêts au taux légal majoré de 5 points à compter du 25 juin 2004, et a rejeté le surplus des conclusions de sa requête ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel et de condamner la commune de Renage à lui verser la somme de 129 000 euros avec intérêts au taux légal capitalisés ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Renage la somme de 4 000 euros en application de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'urbanisme ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Séverine Larere, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de Mme Claire Legras, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Barthélemy, Matuchansky, Vexliard, avocat de la société Isère Développement Environnement et à la SCP Célice, Blancpain, Soltner, avocat de la commune de Renage ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par arrêté du 13 février 2001, la société Isère Développement Environnement (IDE) a été autorisée à lotir en quatre lots, dénommés " Le Clos des Vergers ", un terrain de 3 975 m² situé sur le territoire de la commune de Renage ; que, conformément au programme de travaux annexé à cet arrêté, elle a réalisé, à ses frais, des travaux d'équipement et de viabilité consistant notamment en la création d'une voie de desserte, dénommée " voie A ",  la pose de poteaux d'éclairage public ainsi que la pose d'un collecteur pour les eaux usées ; qu'estimant que ces équipements excédaient les besoins propres de son lotissement, elle a demandé à la commune de Renage, par courrier du 24 juin 2004, de lui rembourser la somme de 129 000 euros correspondant au coût de leur réalisation ; qu'après le rejet implicite opposé par la commune, elle a saisi le tribunal administratif de Grenoble qui, par jugement du 16 octobre 2007, a rejeté sa demande ; qu'elle se pourvoit en cassation contre l'arrêt de la cour administrative d'appel de Lyon du 31 décembre 2009 en tant qu'après avoir annulé le jugement du tribunal administratif de Grenoble, il n'a que partiellement fait droit à ses conclusions d'appel en limitant à la somme de 23 611,14 euros le montant que la commune de Renage était condamnée à lui verser ; que cette commune forme, de son côté, un pourvoi incident à l'encontre du même arrêt en tant qu'il a reconnu le droit à répétition de la société IDE ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 332-30 du code de l'urbanisme : " Les taxes et contributions de toute nature qui sont obtenues ou imposées en violation des dispositions des articles L. 311-4 et L. 332-6 sont réputées sans cause ; les sommes versées ou celles qui correspondent au coût de prestations fournies sont sujettes à répétition. L'action en répétition se prescrit par cinq ans à compter du dernier versement ou de l'obtention des prestations indûment exigées.(...) " ; que selon l'article L. 332-6, dans sa rédaction applicable à la date de l'arrêté du 13 février 2001 :  "Les bénéficiaires d'autorisations de construire ne peuvent être tenus que des obligations suivantes : / (...) ;/ 3° La réalisation des équipements propres mentionnés à l'article L. 332-15 / (...) " ; qu'enfin cet article prévoit que : " L'autorité qui délivre l'autorisation de construire, d'aménager, ou de lotir exige, en tant que de besoin, du bénéficiaire de celle-ci la réalisation et le financement de tous travaux nécessaires à la viabilité et à l'équipement de la construction, du terrain aménagé ou du lotissement, notamment en ce qui concerne la voirie, l'alimentation en eau, gaz et électricité, les réseaux de télécommunication, l'évacuation et le traitement des eaux et matières usées, l'éclairage, les aires de stationnement, les espaces collectifs, les aires de jeux et les espaces plantés.(...)" ; qu'il résulte des dispositions de ces deux derniers articles que seul peut être mis à la charge du bénéficiaire d'une autorisation de lotir le coût des équipements propres à son lotissement ; que dès lors que des équipements excèdent, par leurs caractéristiques et leurs dimensions, les seuls besoins constatés et simultanés d'un ou, le cas échéant, plusieurs lotissements et ne peuvent, par suite, être regardés comme des équipements propres au sens de l'article L. 332-15, leur coût ne peut être, même pour partie, supporté par le lotisseur ;<br/>
<br/>
              3. Considérant, d'une part, qu'il résulte des termes mêmes de l'article L. 332-30 du code de l'urbanisme que l'action en répétition de l'indu qu'il prévoit concerne non seulement les taxes et contributions imposées aux constructeurs en violation des dispositions des articles L. 311-4 et L. 332-6 du code de l'urbanisme mais également celles qui sont obtenues d'eux, avec leur accord, en méconnaissance des mêmes textes ; que c'est, par suite, sans erreur de droit que la cour a jugé que le coût des équipements que le pétitionnaire a intégrés de sa propre initiative dans les pièces techniques de son dossier, et qui sont au demeurant devenus une prescription de l'autorisation qui lui a été délivrée, pouvait faire l'objet d'une action en répétition de l'indu en application de l'article L. 332-30 du code de l'urbanisme ;<br/>
<br/>
              4. Considérant, d'autre part, qu'il ressort des constatations effectuées par les juges du fond que la voie de desserte dénommée " voie A ", dont la création était prévue par le plan d'occupation des sols de la commune, et la canalisation d'eau usée située sous cette voie excédaient les besoins du seul lotissement réalisé par la société IDE et avaient vocation, dès l'origine, à desservir une zone plus large ; que la cour n'a, par suite, pas commis d'erreur de qualification juridique des faits en estimant, implicitement mais nécessairement, que la " voie A " et la canalisation d'évacuation des eaux usées située sous cette voie ne pouvaient être regardées comme des équipements propres au lotissement de la société IDE ;<br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que la commune de Renage n'est pas fondée à demander l'annulation de l'arrêt attaqué en tant qu'il a reconnu le droit à répétition de la société IDE ;<br/>
<br/>
              6. Considérant, en revanche, qu'il résulte de qui a été dit plus haut que la cour a commis une erreur de droit en jugeant que, dans l'hypothèse où le programme des travaux annexé à l'autorisation de lotir comporte des équipements destinés, à la fois, à la viabilité de l'opération et à la desserte d'un secteur plus vaste, il y avait lieu de rechercher dans quelle proportion ils excédaient les besoins propres et de limiter la répétition au coût des prestations excédentaires ; que, par suite, et sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, la société IDE est fondée à demander l'annulation de l'arrêt attaqué en tant qu'il a rejeté le surplus de ses conclusions d'appel ; <br/>
<br/>
              7. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la société IDE, qui n'a pas la qualité de partie perdante dans la présente instance ; qu'il y a lieu, en revanche, de mettre à la charge de la commune de Renage une somme de 2 000 euros à verser à la société IDE en application de ces mêmes dispositions ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Lyon est annulé en tant qu'il a rejeté le surplus des conclusions d'appel de la société IDE.<br/>
Article 2 : Le pourvoi incident de la commune de Renage et ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetés.<br/>
Article 3 : L'affaire est renvoyée, dans la mesure de la cassation prononcée à l'article 1er, à la cour administrative d'appel de Lyon.<br/>
Article 4 : La commune de Renage versera une somme de 2 000 euros à la société IDE au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 5 : La présente décision sera notifiée à la société Isère Développement Environnement et à la commune de Renage.<br/>
Copie en sera adressée à la ministre de l'égalité des territoires et du logement.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">68-02-04-02 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PROCÉDURES D'INTERVENTION FONCIÈRE. LOTISSEMENTS. AUTORISATION DE LOTIR. - PARTICIPATION DES LOTISSEURS - COÛT POUVANT ÊTRE MIS À LA CHARGE DU BÉNÉFICIAIRE DE L'AUTORISATION DE LOTIR ( ART. L. 332-6, 3° ET ART. L. 332-15 DU CODE DE L'URBANISME) - COÛT DES SEULS ÉQUIPEMENTS PROPRES AU LOTISSEMENT [RJ1] - FACULTÉ DE LUI FAIRE SUPPORTER, MÊME EN PARTIE, LE COÛT DES ÉQUIPEMENTS EXCÉDANT LES SEULS BESOINS CONSTATÉS ET SIMULTANÉS D'UN OU, LE CAS ÉCHÉANT, PLUSIEURS LOTISSEMENTS - ABSENCE.
</SCT>
<ANA ID="9A"> 68-02-04-02 Il résulte des dispositions du 3° de l'article L. 332-6 et de l'article L. 332-15 du code de l'urbanisme que seul peut être mis à la charge du bénéficiaire d'une autorisation de lotir le coût des équipements propres à son lotissement. Dès lors que des équipements excèdent, par leurs caractéristiques et leurs dimensions, les seuls besoins constatés et simultanés d'un ou, le cas échéant, plusieurs lotissements et ne peuvent, par suite, être regardés comme des équipements propres au sens de l'article L. 332-15, leur coût ne peut être, même pour partie, supporté par le lotisseur.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Section, 18 mars 1983, M. et Mme Plunian, n° 34130, p. 128.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
