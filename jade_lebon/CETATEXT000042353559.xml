<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042353559</ID>
<ANCIEN_ID>JG_L_2020_09_000000427435</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/35/35/CETATEXT000042353559.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 1ère chambres réunies, 21/09/2020, 427435</TITRE>
<DATE_DEC>2020-09-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>427435</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GASCHIGNARD ; SCP THOUVENIN, COUDRAY, GREVY ; SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR>Mme Yaël Treille</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:427435.20200921</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Mme D... B... a porté plainte contre Mme C... E... devant la chambre disciplinaire de première instance des Pays de la Loire de l'ordre des médecins. Par une décision du 11 octobre 2016, la chambre disciplinaire de première instance a rejeté cette plainte.<br/>
<br/>
              Par une décision du 27 novembre 2018, la chambre disciplinaire nationale de l'ordre des médecins a rejeté l'appel formé par Mme B... contre cette décision. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 28 janvier et 29 avril 2019 au secrétariat du contentieux du Conseil d'Etat, Mme B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette décision ; <br/>
<br/>
              2°) de mettre à la charge de Mme E... la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la santé publique ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Yaël Treille, auditeur,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gaschignard, avocat de Mme B... et à la SCP Thouvenin, Coudray, Grevy, avocat du conseil départemental de Loire-Atlantique de l'ordre des médecins ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond qu'après le décès de son père, M. A..., le 2 novembre 2014, Mme B... a demandé à Mme E..., médecin traitant de M. A..., la communication du dossier médical du défunt dans la perspective de faire valoir ses droits successoraux alors que le testament rédigé par son père le 18 septembre précédent était revenu sur les dispositions successorales antérieurement prises par ce dernier. Mme E..., après avoir pris l'avis du conseil départemental de Loire-Atlantique de l'ordre des médecins, a refusé de communiquer le dossier médical de M. A... à Mme B... au motif que son patient lui avait indiqué oralement, à deux reprises, sa volonté que ses enfants ne puissent pas avoir accès aux informations médicales le concernant. Saisi par Mme B... d'une plainte contre Mme E..., le conseil départemental de Loire-Atlantique de l'ordre des médecins a transmis, sans s'y associer, la plainte de la requérante à la chambre disciplinaire de première instance des Pays de la Loire de l'ordre des médecins, après une réunion de conciliation infructueuse entre la praticienne et la plaignante. Par une décision du 11 octobre 2016, la chambre disciplinaire de première instance des Pays de la Loire de l'ordre des médecins a rejeté la plainte de Mme B..., en jugeant que le refus de Mme E... de communiquer le dossier médical de son patient à ses ayants droit n'était pas constitutif d'un manquement à ses obligations déontologiques. Par une décision du 27 novembre 2018, contre laquelle Mme B... se pourvoit en cassation, la chambre disciplinaire nationale de l'ordre des médecins a rejeté l'appel formé par Mme B... contre la décision de la chambre disciplinaire de première instance. <br/>
<br/>
              2. Aux termes du 6ème alinéa de l'article L. 1111-7 du code de la santé publique dans sa rédaction applicable aux faits de l'espèce : " En cas de décès du malade, l'accès des ayants droit à son dossier médical s'effectue dans les conditions prévues par le dernier alinéa de l'article L. 1110-4 ". Aux termes de l'article L. 1110-4 du même code dans sa rédaction applicable aux faits de l'espèce, toute personne malade " a droit au respect de sa vie privée et au secret des informations la concernant. / Excepté dans les cas de dérogation expressément prévus par la loi, ce secret (...) s'impose à tous les professionnels intervenant dans le système de santé. (...) / Le secret médical ne fait pas obstacle à ce que les informations concernant une personne décédée soient délivrées à ses ayants droit, son concubin ou son partenaire lié par un pacte civil de solidarité, dans la mesure où elles leur sont nécessaires pour leur permettre de connaître les causes de la mort, de défendre la mémoire du défunt ou de faire valoir leurs droits, sauf volonté contraire exprimée par la personne avant son décès (...) ". Aux termes de l'article R. 4127-2 du même code : " Le médecin, au service de l'individu et de la santé publique, exerce sa mission dans le respect de la vie humaine, de la personne et de sa dignité. Le respect dû à la personne ne cesse pas de s'imposer après la mort ".<br/>
<br/>
              3. Il résulte des dispositions précitées des articles L. 1110-4 et L. 1111-7 du code de la santé publique que le respect du secret qui s'attache aux informations médicales concernant la santé d'une personne ne cesse pas de s'imposer après sa mort et que le législateur n'a entendu, par dérogation, autoriser la communication aux ayants droit d'une personne décédée que des seules informations qui leur sont nécessaires pour connaître les causes de la mort, défendre la mémoire du défunt ou faire valoir leurs droits, à la condition que la personne concernée n'ait pas exprimé de volonté contraire avant son décès. En cas de litige sur ce point, lorsqu'une telle volonté n'a pas été clairement exprimée par écrit, il revient à chaque partie d'apporter les éléments de preuve circonstanciés dont elle dispose afin de permettre au juge de former sa conviction pour déterminer si la personne concernée, avant son décès, avait exprimé de façon claire et non équivoque sa volonté libre et éclairée de s'opposer à la communication à ses ayants droit des informations visées à l'article L. 1110-4 du code de la santé publique. <br/>
<br/>
              4. En jugeant au cas d'espèce qu'il ressortait de l'ensemble des éléments produits par les parties devant elle que M. A... avait exprimé auprès de Mme E... la volonté que ses enfants ne puissent pas avoir accès aux informations médicales le concernant, la chambre disciplinaire nationale de l'ordre des médecins a porté sur les faits qui lui étaient soumis une appréciation souveraine qui est exempte de dénaturation. En en déduisant que Mme E... n'avait pas, dès lors, méconnu les obligations déontologiques fixées à l'article R. 4127-2 du code de la santé publique, elle n'a pas commis d'erreur de droit.<br/>
<br/>
              5. Il résulte de tout ce qui précède que Mme B... n'est pas fondée à demander l'annulation de la décision de la chambre disciplinaire nationale de l'ordre des médecins du 27 novembre 2018. Son pourvoi doit donc être rejeté, y compris ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
              6. Dans les circonstances de l'espèce, il n'y a pas lieu de mettre à la charge de Mme B... le versement d'une somme au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de Mme B... est rejeté.<br/>
Article 2 : Les conclusions du conseil départemental de Loire-Atlantique de l'ordre des médecins présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 3 : La présente décision sera notifiée à Mme D... B..., à Mme C... E... et au conseil départemental de Loire-Atlantique de l'ordre des médecins. <br/>
Copie en sera adressée au Conseil national de l'ordre des médecins. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">26-03-10 DROITS CIVILS ET INDIVIDUELS. LIBERTÉS PUBLIQUES ET LIBERTÉS DE LA PERSONNE. SECRET DE LA VIE PRIVÉE. - SECRET MÉDICAL (ART. L. 1110-4 DU CSP) - VOLONTÉ D'UN DÉFUNT DE S'OPPOSER À LA COMMUNICATION À SES AYANTS DROIT D'ÉLÉMENTS DE SON DOSSIER MÉDICAL - 1) MODALITÉS D'ÉTABLISSEMENT DE LA PREUVE DEVANT LE JUGE DU FOND - 2) CONTRÔLE DU JUGE DE CASSATION - DÉNATURATION.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-08-02-02-01-04 PROCÉDURE. VOIES DE RECOURS. CASSATION. CONTRÔLE DU JUGE DE CASSATION. BIEN-FONDÉ. DÉNATURATION. - VOLONTÉ D'UN DÉFUNT DE S'OPPOSER À LA COMMUNICATION À SES AYANTS DROIT D'ÉLÉMENTS DE SON DOSSIER MÉDICAL (ART L. 1110-4 DU CSP).
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">55-03-01-02 PROFESSIONS, CHARGES ET OFFICES. CONDITIONS D'EXERCICE DES PROFESSIONS. MÉDECINS. RÈGLES DIVERSES S'IMPOSANT AUX MÉDECINS DANS L'EXERCICE DE LEUR PROFESSION. - SECRET MÉDICAL (ART. L. 1110-4 DU CSP) - VOLONTÉ D'UN DÉFUNT DE S'OPPOSER À LA COMMUNICATION À SES AYANTS DROIT D'ÉLÉMENTS DE SON DOSSIER MÉDICAL - 1) MODALITÉS D'ÉTABLISSEMENT DE LA PREUVE DEVANT LE JUGE DU FOND - 2) CONTRÔLE DU JUGE DE CASSATION - DÉNATURATION.
</SCT>
<ANA ID="9A"> 26-03-10 1) Il résulte des articles L. 1110-4 et L. 1111-7 du code de la santé publique (CSP) que le respect du secret qui s'attache aux informations médicales concernant la santé d'une personne ne cesse pas de s'imposer après sa mort et que le législateur n'a entendu, par dérogation, autoriser la communication aux ayants droit d'une personne décédée que des seules informations qui leur sont nécessaires pour connaître les causes de la mort, défendre la mémoire du défunt ou faire valoir leurs droits, à la condition que la personne concernée n'ait pas exprimé de volonté contraire avant son décès. En cas de litige sur ce point, lorsqu'une telle volonté n'a pas été clairement exprimée par écrit, il revient à chaque partie d'apporter les éléments de preuve circonstanciés dont elle dispose afin de permettre au juge de former sa conviction pour déterminer si la personne concernée, avant son décès, avait exprimé de façon claire et non équivoque sa volonté libre et éclairée de s'opposer à la communication à ses ayants droit des informations visées à l'article L. 1110-4 du CSP.,,,2) Le juge de cassation laisse à l'appréciation souveraine des juges du fond, sous réserve de dénaturation, le point de savoir s'il résulte de l'ensemble des éléments produits que le défunt avait exprimé la volonté de s'opposer à cette communication.</ANA>
<ANA ID="9B"> 54-08-02-02-01-04 Le juge de cassation laisse à l'appréciation souveraine des juges du fond, sous réserve de dénaturation, le point de savoir s'il résulte de l'ensemble des éléments produits qu'un défunt avait exprimé la volonté de s'opposer à la communication à ses ayants droit des informations visées à l'article L. 1110-4 du code de la santé publique (CSP).</ANA>
<ANA ID="9C"> 55-03-01-02 1) Il résulte des articles L. 1110-4 et L. 1111-7 du code de la santé publique (CSP) que le respect du secret qui s'attache aux informations médicales concernant la santé d'une personne ne cesse pas de s'imposer après sa mort et que le législateur n'a entendu, par dérogation, autoriser la communication aux ayants droit d'une personne décédée que des seules informations qui leur sont nécessaires pour connaître les causes de la mort, défendre la mémoire du défunt ou faire valoir leurs droits, à la condition que la personne concernée n'ait pas exprimé de volonté contraire avant son décès. En cas de litige sur ce point, lorsqu'une telle volonté n'a pas été clairement exprimée par écrit, il revient à chaque partie d'apporter les éléments de preuve circonstanciés dont elle dispose afin de permettre au juge de former sa conviction pour déterminer si la personne concernée, avant son décès, avait exprimé de façon claire et non équivoque sa volonté libre et éclairée de s'opposer à la communication à ses ayants droit des informations visées à l'article L. 1110-4 du CSP.,,,2) Le juge de cassation laisse à l'appréciation souveraine des juges du fond, sous réserve de dénaturation, le point de savoir s'il résulte de l'ensemble des éléments produits que le défunt avait exprimé la volonté de s'opposer à cette communication.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
