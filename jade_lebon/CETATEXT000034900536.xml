<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034900536</ID>
<ANCIEN_ID>JG_L_2017_06_000000399748</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/90/05/CETATEXT000034900536.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 09/06/2017, 399748</TITRE>
<DATE_DEC>2017-06-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>399748</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DE NERVO, POUPET</AVOCATS>
<RAPPORTEUR>M. Grégory Rzepski</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier Henrard</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:399748.20170609</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Le syndicat national pénitentiaire Force-Ouvrière (SNP-FO) a demandé au tribunal administratif de Lille, d'une part, d'annuler la répartition des sièges des représentants du personnel au sein de la commission administrative paritaire interrégionale du corps d'encadrement et d'application du personnel de surveillance des services pénitentiaires de Nord-Pas-de-Calais, Picardie, Haute-Normandie effectuée le 19 janvier 2015, ensemble l'arrêté du directeur interrégional des services pénitentiaires du 2 février 2015 portant désignation de ces représentants et, d'autre part, d'enjoindre à ce directeur de mettre les organisations syndicales ayant obtenu des sièges au sein de cette commission en mesure de procéder à leur répartition, dans un délai de deux mois à compter de la notification du jugement. Par un jugement n° 1502443 du 2 novembre 2015, le tribunal administratif de Lille a fait droit à ces demandes.<br/>
<br/>
              Par un arrêt n°s 15DA01932, 15DA01933 du 15 mars 2016, la cour administrative d'appel de Douai a, sur recours du garde des sceaux, ministre de la justice, annulé ce jugement et rejeté la demande du SNP-FO.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 12 mai, 9 août 2016 et 30 mars 2017 au secrétariat du contentieux du Conseil d'Etat, le SNP-FO demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le décret n° 82-451 du 28 mai 1982 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Grégory Rzepski, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Olivier Henrard, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP de Nervo, Poupet, avocat du syndicat national pénitentiaire Force-Ouvrière.  <br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article 21 du décret du 28 mai 1982 relatif aux commissions administratives paritaires : " Les représentants du personnel au sein des commissions administratives paritaires sont élus au bulletin secret à la proportionnelle. La désignation des membres titulaires est effectuée de la manière indiquée au présent article. / a) Nombre total de sièges de représentants titulaires attribués à chaque liste / Chaque liste a droit à autant de sièges de représentants titulaires que le nombre de voix recueillies par elle contient de fois le quotient électoral. / Les sièges de représentants titulaires restant éventuellement à pourvoir sont attribués suivant la règle de la plus forte moyenne. / b) Fixation des grades dans lesquels les listes ont des représentants titulaires / La liste ayant droit au plus grand nombre de sièges choisit les sièges de titulaires qu'elle souhaite se voir attribuer sous réserve de ne pas empêcher par son choix une autre liste d'obtenir le nombre de sièges auxquels elle a droit dans les grades pour lesquels elle avait présenté des candidats. Elle ne peut toutefois choisir d'emblée plus d'un siège dans chacun des grades pour lesquels elle a présenté des candidats que dans le cas où aucune liste n'a présenté de candidats pour le ou les grades considérés. / Les autres listes exercent ensuite leur choix successivement dans l'ordre décroissant du nombre de sièges auxquels elles peuvent prétendre, dans les mêmes conditions et sous les mêmes réserves (...) / Lorsque la procédure prévue ci-dessus n'a pas permis à une ou plusieurs listes de pourvoir tous les sièges auxquels elle aurait pu prétendre, ces sièges sont attribués à la liste qui, pour les grades dont les représentants restent à désigner, a obtenu le plus grand nombre de suffrages (...) " ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que des élections se sont tenues le 4 décembre 2014 pour désigner les représentants du personnel à la commission administrative paritaire interrégionale compétente pour le corps d'encadrement et d'application du personnel de surveillance à la direction interrégionale des services pénitentiaires de Nord-Pas-de-Calais, Picardie, Haute-Normandie ; que deux sièges étaient à pourvoir pour le grade de major pénitentiaire, pour lequel seul le syndicat national pénitentiaire Force-Ouvrière (SNP-FO) avait présenté des candidats, deux pour celui de premier surveillant, deux pour celui de surveillant brigadier et trois pour celui de surveillant ; que les listes présentées par l'Union fédérale de l'administration pénitentiaire des surveillants (UFAP) et par le syndicat national pénitentiaire Force-Ouvrière (SNP-FO) ont obtenu respectivement quatre et trois sièges ; que celles de la Confédération générale du travail (CGT) et du Syndicat pénitentiaire des surveillants (SPS) ont obtenu chacune un siège ; qu'au cours d'une réunion de répartition des sièges qui s'est tenue le 16 décembre 2014, l'UFAP a fait connaître son intention d'occuper un siège dans chacun des trois grades pour lesquels elle avait présenté des candidats, avant que le SNP-FO n'indique qu'il souhaitait en occuper un dans  le grade de premier surveillant, un dans  le grade de surveillant brigadier et un dans le grade de major pénitentiaire ; que le directeur interrégional des services pénitentiaires de Lille a alors décidé de lever la séance au motif que  le choix du SNP-FO de n'occuper qu'un seul des sièges dans le grade de major avait pour effet de contraindre l'UFAP à renoncer à pourvoir le quatrième siège auquel elle pouvait prétendre, sauf à ce que la CGT et le SPS ne puissent obtenir les sièges auxquels ils avaient droit dans les grades pour lesquels ils avaient présenté des candidats ; qu'au cours d'une seconde réunion consacrée à la répartition des sièges, le 19 janvier 2015, le directeur interrégional des services pénitentiaires a invité le SNP-FO à occuper deux sièges dans le grade de major afin de ne pas priver les autres listes de leur droit d'obtenir le nombre de sièges auxquels elles avaient droit ; qu'il a ensuite, par un arrêté du 2 février 2015, attribué à la liste de ce syndicat, outre ces deux sièges, un siège dans le grade de brigadier ; qu'il a attribué à la liste de l'UFAP deux sièges dans le grade de premier surveillant et un siège dans les grades de surveillant et de brigadier ; qu'il a enfin attribué à chacune des deux autres listes un siège dans le grade de surveillant ; que, par un jugement du 2 novembre 2015, le tribunal administratif de Lille a, à la demande du SNP-FO, annulé cet arrêté ; que le SNP-FO se pourvoit en cassation contre l'arrêt du 15 mars 2016 par lequel la cour administrative d'appel de Douai a, sur appel du garde des sceaux, ministre de la justice, annulé ce jugement et rejeté sa demande ; <br/>
<br/>
              3. Considérant, en premier lieu, qu'en jugeant que l'administration avait pu légalement procéder ainsi qu'il a été dit au point précédent, dès lors que, en choisissant de n'occuper qu'un siège dans le grade de major, pour lequel il avait seul présenté des candidats, le syndicat requérant empêchait nécessairement une autre liste d'obtenir un siège auquel elle avait droit dans un grade pour lequel elle avait présenté des candidats, la cour, qui ne s'est pas méprise sur la portée des écritures du syndicat requérant, n'a pas commis d'erreur de droit ;     <br/>
<br/>
              4. Considérant, en second lieu, que la cour n'a pas commis d'erreur de droit en écartant l'application des dispositions précitées du troisième alinéa du b de l'article 21 du décret du 28 mai 1982 dès lors que, ainsi qu'il a été dit, elle a jugé que la mise en oeuvre de la procédure prévue par les deux premiers alinéas du b de cet article permettait de pourvoir tous les sièges ;<br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que le SNP-FO n'est pas fondé à demander l'annulation de l'arrêt qu'il attaque ; que ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi du syndicat national pénitentiaire Force-Ouvrière est rejeté.<br/>
Article 2 : La présente décision sera notifiée au syndicat national pénitentiaire Force-Ouvrière et au ministre d'Etat, garde des sceaux, ministre de la justice. <br/>
Copie en sera adressée au syndicat CGT pénitentiaire UR de Lille, au syndicat pénitentiaire des surveillants non gradés et au syndicat Union Régionale UFAP de Lille.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-07-05-02 FONCTIONNAIRES ET AGENTS PUBLICS. STATUTS, DROITS, OBLIGATIONS ET GARANTIES. COMMISSIONS ADMINISTRATIVES PARITAIRES. COMPOSITION. - OPÉRATIONS ÉLECTORALES - MODALITÉS DU CHOIX, PAR UNE LISTE, DES SIÈGES AUXQUELS ELLE A DROIT DANS LES GRADES POUR LESQUELS ELLE AVAIT PRÉSENTÉ DES CANDIDATS - 1) PRINCIPE [RJ1] - 2) ESPÈCE.
</SCT>
<ANA ID="9A"> 36-07-05-02 1) Il résulte des dispositions de l'article 21 du décret n° 82-451 du 28 mai 1982 relatif aux commissions administratives paritaires que le choix, par une liste, des sièges de titulaires qu'elle souhaite se voir attribuer doit être exercé sans empêcher une autre liste, ayant recueilli un plus grand nombre ou un moins grand nombre de voix, d'obtenir un siège auquel elle a droit dans un grade pour lequel elle a présenté des candidats....  ,,2) L'administration peut demander à la liste arrivée deuxième, qui a présenté des candidats dans tous les grades, de pourvoir deux sièges dans un même grade dès lors qu'à défaut, elle empêcherait la liste arrivée première, qui n'avait pas présenté de candidats dans tous les grades, de pourvoir le dernier siège auquel elle avait droit.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 9 novembre 1988, Lange et Aurand, n° 86327, T. p. 804.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
