<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029985893</ID>
<ANCIEN_ID>JG_L_2014_12_000000361514</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/98/58/CETATEXT000029985893.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème / 1ère SSR, 23/12/2014, 361514</TITRE>
<DATE_DEC>2014-12-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>361514</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème / 1ère SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LE BRET-DESACHE ; SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>M. Didier Ribes</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:361514.20141223</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 31 juillet et 30 octobre 2012 au secrétariat du contentieux du Conseil d'État, présentés pour la Société hydroélectrique du Pont du Gouffre, dont le siège social est 12, traverse du Daval à la Bresse (88250), représentée par son co-gérant en exercice ; la Société hydroélectrique du Pont du Gouffre demande au Conseil d'État :<br/>
<br/>
              1°) d'annuler l'arrêt n° 11NC01016 du 4 juin 2012 par lequel la cour administrative d'appel de Nancy a rejeté sa requête tendant, d'une part, à l'annulation du jugement n° 0901488 du 19 avril 2011 par lequel le tribunal administratif de Nancy a annulé l'arrêté du 20 octobre 2008 par lequel le préfet des Vosges l'a autorisée à réhabiliter et à exploiter la centrale hydroélectrique du Pont du Gouffre pendant une durée de 30 ans, d'autre part, au rejet de la demande présentée par la Fédération des Vosges pour la pêche et la protection du milieu aquatique devant ledit tribunal ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de la Fédération des Vosges pour la pêche et la protection du milieu aquatique une somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'énergie ;<br/>
<br/>
              Vu le code de l'environnement ;<br/>
<br/>
              Vu la loi du 16 octobre 1919 relative à l'énergie hydroélectrique modifiée notamment par la loi n° 80-531 du 15 juillet 1980 ;<br/>
<br/>
              Vu le décret n° 99-1138 du 27 décembre 1999 ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Didier Ribes, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Le Bret-Desaché, avocat de la Société hydroélectrique du Pont du Gouffre et à la SCP Waquet, Farge, Hazan, avocat de la Fédération des Vosges pour la pêche et la protection du milieu aquatique ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des énonciations de l'arrêt attaqué et des pièces du dossier soumis aux juges du fond que la Société hydroélectrique du Pont du Gouffre a acquis en 2005 la centrale hydroélectrique du Pont du Gouffre, située sur le territoire de la commune de Cornimont (Vosges), autorisée par arrêté du préfet des Vosges du 14 juillet 1860 pour une puissance de 82 kW et non exploitée depuis 1964 ; que, par application des dispositions du dernier alinéa de l'article 18 de la loi du 16 octobre 1919 relative à l'utilisation de l'énergie hydraulique et de celles de l'article R. 214-84 du code de l'environnement, l'exploitation de cette centrale est demeurée autorisée dans cette mesure au titre de la police de l'énergie et de la police de l'eau, sans limitation de durée ; que le ruisseau du Ventron sur lequel est implanté cet ouvrage a été inscrit par un décret du 27 décembre 1999 sur la liste des cours d'eau réservés en application de l'article 2 de la loi du 16 octobre 1919 modifiée relative à l'utilisation de l'énergie hydraulique ; que, par un courrier du 12 janvier 2006, la Société hydroélectrique du Pont du Gouffre a demandé au préfet des Vosges l'autorisation de réhabiliter les installations de la centrale et de procéder à divers travaux afin de porter sa puissance de 82 à 207 kW ; que, par un arrêté du 20 octobre 2008, le préfet lui a accordé cette autorisation pour une durée de trente ans ; que, par un jugement du 19 avril 2011, le tribunal administratif de Nancy a, à la demande de la Fédération des Vosges pour la pêche et la protection du milieu aquatique, annulé l'arrêté préfectoral ; que par un arrêt du 4 juin 2012 contre lequel la Société hydroélectrique du Pont du Gouffre se pourvoit en cassation, la cour administrative d'appel de Nancy a rejeté sa requête dirigée contre le jugement du tribunal administratif ;  <br/>
<br/>
              2. Considérant qu'aux termes de l'alinéa 5 de l'article 2 de la loi du 16 octobre 1919 relative à l'utilisation de l'énergie hydraulique, dans sa rédaction applicable au litige : " Sur certains cours d'eau ou sections de cours d'eau dont la liste sera fixée par décret en Conseil d'État, aucune autorisation ou concession ne sera donnée pour des entreprises hydrauliques nouvelles. Pour les entreprises existantes, régulièrement installées à la date de la promulgation de la loi n° 80-531 du 15 juillet 1980, ou visées à l'article 27 de ladite loi, une concession ou une autorisation pourra être accordée sous réserve que la hauteur du barrage ne soit pas modifiée " ;<br/>
<br/>
              3. Considérant qu'au sens de ces dispositions, l'entreprise doit s'entendre des installations matérielles et non de la personne physique ou morale bénéficiaire de l'autorisation ou de la concession ;<br/>
<br/>
              4. Considérant que ces dispositions, éclairées par les travaux préparatoires de la loi du 15 juillet 1980 relative aux économies d'énergie et à l'utilisation de la chaleur dont elles sont issues, s'opposent à la création d'obstacles nouveaux à la continuité écologique des cours d'eau ou sections de cours réservés au titre de la protection de l'environnement, que ces obstacles affectent le régime hydrologique, la circulation des espèces vivantes et l'accès à leur habitat ou l'écoulement des sédiments ; que ces dispositions interdisent la création de toute installation hydraulique nouvelle sur les cours d'eau ou sections de cours d'eau réservés au titre de la protection de l'environnement ; qu'enfin, elles font également obstacle aux modifications d'une installation hydraulique existante ayant pour effet de créer un obstacle nouveau ou de modifier l'écoulement du cours d'eau réservé dans des conditions portant atteinte à la continuité écologique ; que de telles modifications doivent être regardées comme portant création d'une entreprise nouvelle au sens de l'article 2 de la loi du 16 octobre 1919 et sont, en conséquence, interdites ; <br/>
<br/>
              5. Considérant qu'exception faite du cas où la hauteur de chute est modifiée, ces mêmes dispositions ne s'opposent pas, en revanche, à ce que soient réalisées des modifications substantielles des installations hydrauliques existantes légalement autorisées sur ces cours d'eau, y compris lorsque ces modifications permettent d'augmenter leur puissance, et dès lors qu'elles n'ont pas pour effet de créer un obstacle nouveau à la continuité écologique des cours d'eau réservés ; <br/>
<br/>
              6. Considérant que la cour a relevé que les travaux de réhabilitation de la centrale hydroélectrique du Pont du Gouffre, consistant notamment en l'élargissement du canal d'amenée, le remplacement d'une grande portion de ce canal par une conduite forcée et la construction d'un nouveau bâtiment en aval du bâtiment existant, s'ils ne rehaussaient pas la crête du barrage, aboutissaient à accroître le débit d'eau prélevé et à presque tripler la puissance nominale de l'installation ; qu'elle en a déduit que ces travaux, eu égard à leur importance, devaient être regardés comme entraînant la création d'une entreprise nouvelle au sens des dispositions de l'article 2 de la loi du 16 octobre 1919 ; qu'en se fondant, d'une part, sur le critère de l'augmentation de la puissance pour apprécier si les modifications de l'installation étaient conformes à l'article 2 de la loi du 16 octobre 1919 et en retenant, d'autre part, la qualification d'entreprise nouvelle sans rechercher si les travaux dont elle relevait l'importance étaient, par leur nature ou leurs effets, susceptibles de créer un obstacle nouveau ou de modifier l'écoulement du ruisseau du Ventron dans des conditions portant atteinte à la continuité écologique, la cour a entaché son arrêt d'une double erreur de droit ; que par suite et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, la Société hydroélectrique du Pont du Gouffre est fondée, pour ce motif, à demander l'annulation de l'arrêt qu'elle attaque ;<br/>
<br/>
              7. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la Société hydroélectrique du Pont du Gouffre qui n'est pas, dans la présente instance, la partie perdante ; qu'en revanche, il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la Fédération des Vosges pour la pêche et la protection du milieu aquatique la somme de 2 000 euros à verser à la Société hydroélectrique du Pont du Gouffre au titre des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nancy du 4 juin 2012 est annulé.<br/>
<br/>
      Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nancy.<br/>
<br/>
Article 3 : La Fédération des Vosges pour la pêche et la protection du milieu aquatique versera à la Société hydroélectrique du Pont du Gouffre une somme de 2 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : Les conclusions de la Fédération des Vosges pour la pêche et la protection du milieu aquatique présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 5 : La présente décision sera notifiée à la Société hydroélectrique du Pont du Gouffre, à la Fédération des Vosges pour la pêche et la protection du milieu aquatique et à la ministre de l'écologie, du développement durable et de l'énergie.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">29-02-02 ENERGIE. ÉNERGIE HYDRAULIQUE. - DISPOSITIONS ENCADRANT LES  ENTREPRISES  HYDRAULIQUES AUTORISÉES SUR LES COURS D'EAU RÉSERVÉS (ART. 2, AL. 5 DE LA LOI DU 16 OCTOBRE 1919) - 1) NOTION D'ENTREPRISE  - INSTALLATIONS MATÉRIELLES - 2) OBJET DE CE RÉGIME - EVITER LA CRÉATION D'OBSTACLES NOUVEAUX À LA CONTINUITÉ ÉCOLOGIQUE DE CES COURS D'EAU - 3) IMPLICATIONS - A) INSTALLATIONS HYDRAULIQUES NOUVELLES SUR CES COURS D'EAU - INTERDICTION - B) MODIFICATION D'UNE INSTALLATION HYDRAULIQUE EXISTANTE - POSSIBILITÉ, Y COMPRIS EN CAS DE MODIFICATION SUBSTANTIELLE CONDUISANT À UNE AUGMENTATION DE LA PUISSANCE DE L'INSTALLATION - CONDITIONS - ABSENCE DE MODIFICATION DE LA HAUTEUR DE CHUTE, DE CRÉATION D'UN OBSTACLE NOUVEAU ET DE MODIFICATION DE L'ÉCOULEMENT DANS DES CONDITIONS PORTANT ATTEINTE À LA CONTINUITÉ ÉCOLOGIQUE.
</SCT>
<ANA ID="9A"> 29-02-02 1) Au sens des dispositions du 5e alinéa de l'article 2 de la loi du 16 octobre 1919 relative à l'utilisation de l'énergie hydraulique, dans sa rédaction issue de la loi n° 80-531 du 15 juillet 1980,  l'entreprise  doit s'entendre des installations matérielles et non de la personne physique ou morale bénéficiaire de l'autorisation ou de la concession.,,,2) Ces dispositions éclairées par les travaux préparatoires de la loi du 15 juillet 1980 dont elles sont issues, s'opposent à la création d'obstacles nouveaux à la continuité écologique des cours d'eau ou sections de cours réservés au titre de la protection de l'environnement, que ces obstacles affectent le régime hydrologique, la circulation des espèces vivantes et l'accès à leur habitat ou l'écoulement des sédiments.... ,,3) a) Ces dispositions interdisent la création de toute installation hydraulique nouvelle sur les cours d'eau ou sections de cours d'eau réservés au titre de la protection de l'environnement.,,,b) Elles font également obstacle aux modifications d'une installation hydraulique existante ayant pour effet de créer un obstacle nouveau ou de modifier l'écoulement du cours d'eau réservé dans des conditions portant atteinte à la continuité écologique. De telles modifications doivent être regardées comme portant création d'une entreprise nouvelle au sens de l'article 2 de la loi du 16 octobre 1919 et sont, en conséquence, interdites.... ,,Exception faite du cas où la hauteur de chute est modifiée, ces mêmes dispositions ne s'opposent pas, en revanche, à ce que soient réalisées des modifications substantielles des installations hydrauliques existantes légalement autorisées sur ces cours d'eau, y compris lorsque ces modifications permettent d'augmenter leur puissance, et dès lors qu'elles n'ont pas pour effet de créer un obstacle nouveau à la continuité écologique des cours d'eau réservés.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
