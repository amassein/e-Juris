<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037461547</ID>
<ANCIEN_ID>JG_L_2018_10_000000405939</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/46/15/CETATEXT000037461547.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 03/10/2018, 405939</TITRE>
<DATE_DEC>2018-10-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>405939</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Isabelle Lemesle</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:405939.20181003</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés au secrétariat du contentieux du Conseil d'Etat les 13 décembre 2016, 11 janvier 2017 et 12 avril 2017, M. A...B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir les décisions des 9 juin et 4 octobre 2016 par laquelle la présidente de la Commission nationale de l'informatique et des libertés (CNIL) a clôturé sa plainte relative à la demande de rectification de la graphie de nom dans les fichiers de la société ADL Partner ;<br/>
<br/>
              2°) d'enjoindre à la CNIL d'édicter une ligne de conduite prescrivant que les outils informatiques utilisés par les responsables de traitement de données permettent l'utilisation des minuscules et des majuscules dans le libellé des dénominations.<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu :<br/>
              - la loi du 6 fructidor an II ;<br/>
              - la loi n° 78-17 du 6 janvier 1978 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Isabelle Lemesle, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du 2° de l'article 11 de la loi du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés, la Commission nationale de l'informatique et des libertés (CNIL) : " veille à ce que les traitements de données à caractère personnel soient mis en oeuvre conformément aux dispositions de la présente loi. A ce titre : / (...) c) Elle reçoit les réclamations, pétitions et plaintes relatives à la mise en oeuvre des traitements de données à caractère personnel et informe leurs auteurs des suites données à celles-ci ". Aux termes du I de l'article 45 de la même loi, dans sa rédaction applicable à la date de la décision attaquée : " I. - Lorsque le responsable d'un traitement ne respecte pas les obligations découlant de la présente loi, le président de la Commission nationale de l'informatique et des libertés peut le mettre en demeure de faire cesser le manquement constaté dans un délai qu'il fixe. En cas d'extrême urgence, ce délai peut être ramené à vingt-quatre heures. /Si le responsable du traitement se conforme à la mise en demeure qui lui est adressée, le président de la commission prononce la clôture de la procédure. / Dans le cas contraire, la formation restreinte de la commission peut prononcer, après une procédure contradictoire, les sanctions suivantes : / 1° Un avertissement ;/ 2° Une sanction pécuniaire, dans les conditions prévues à l'article 47, à l'exception des cas où le traitement est mis en oeuvre par l'Etat ; / 3° Une injonction de cesser le traitement, lorsque celui-ci relève de l'article 22, ou un retrait de l'autorisation accordée en application de l'article 25. Lorsque le manquement constaté ne peut faire l'objet d'une mise en conformité dans le cadre d'une mise en demeure, la formation restreinte peut prononcer, sans mise en demeure préalable et après une procédure contradictoire, les sanctions prévues au présent I (...) ". Il résulte de ces dispositions qu'il appartient à la CNIL de procéder, lorsqu'elle est saisie d'une plainte ou d'une réclamation tendant à la mise en oeuvre de ses pouvoirs, à l'examen des faits qui  sont à l'origine de la plainte ou de la réclamation et de décider des suites à lui donner. Elle dispose, à cet effet, d'un large pouvoir d'appréciation et peut tenir compte de la gravité des manquements allégués au regard de la législation ou de la réglementation qu'elle est chargée de faire appliquer, du sérieux des indices relatifs à ces faits, de la date à laquelle ils ont été commis, du contexte dans lequel ils l'ont été et, plus généralement, de l'ensemble des intérêts généraux dont elle a la charge.<br/>
<br/>
              2. L'auteur d'une plainte peut déférer au juge de l'excès de pouvoir le refus de la CNIL d'engager à l'encontre de la personne visée par la plainte une procédure sur le fondement du I de l'article 45 de la loi du 6 janvier 1978 précité, y compris lorsque la CNIL procède à des mesures d'instruction ou constate l'existence d'un manquement aux dispositions de cette loi. Il appartient au juge de censurer ce refus en cas d'erreur de fait ou de droit, d'erreur manifeste d'appréciation ou de détournement de pouvoir. <br/>
<br/>
              3. Aux termes du I de l'article 40 de la loi du 6 janvier 1978 : " Toute personne physique justifiant de son identité peut exiger du responsable d'un traitement que soient, selon les cas, rectifiées, complétées, mises à jour, verrouillées ou effacées les données à caractère personnel la concernant, qui sont inexactes, incomplètes, équivoques, périmées, ou dont la collecte, l'utilisation, la communication ou la conservation est interdite ". Lorsque l'auteur de la plainte se fonde sur la méconnaissance des droits qu'il tient de cet article, notamment du droit de rectification de ses données personnelles, le pouvoir d'appréciation de la CNIL pour décider des suites à y donner s'exerce, eu égard à la nature des droits individuels en cause, sous l'entier contrôle du juge de l'excès de pouvoir.<br/>
<br/>
              4. Il ressort des pièces du dossier que M. B...a souscrit en octobre 2014 un abonnement à un magazine auprès de la société ADL Partner, à laquelle il a, par divers courriers ultérieurs, demandé que soit corrigée la graphie de son nom de famille dans les fichiers de cette société, afin que la particule de celui-ci apparaisse en lettres minuscules et non plus en lettres majuscules. Après le refus opposé à sa demande de rectification, il a saisi la CNIL, le 16 juin 2015, d'une plainte à l'encontre de la société ADL Partner. Il demande l'annulation pour excès de pouvoir des décisions des 9 juin et 4 octobre 2016 l'informant de la clôture de sa plainte. <br/>
<br/>
              5. En premier lieu, la CNIL a correctement interprété le courrier du 16 juin 2015 en l'analysant comme une plainte contre la société ADL Partner à raison du refus opposé à la demande de correction de la graphie de son patronyme, dont elle était saisie.<br/>
<br/>
              6. En deuxième lieu, la CNIL ne s'est pas méprise sur l'étendue de ses compétences en affirmant qu'il ne lui appartient pas de veiller au respect des dispositions de l'article 1er de la loi du 6 fructidor an II aux termes duquel : " Aucun citoyen ne pourra porter de nom, ni de prénom autres que ceux exprimés dans son acte de naissance... " et de l'article 4 de la même loi qui dispose qu'il " est expressément défendu à tous fonctionnaires publics de désigner les citoyens dans les actes autrement que par le nom de famille, les prénoms portés en l'acte de naissance (...) dans les expéditions et extraits qu'ils délivreront à l'avenir ". <br/>
<br/>
              7. En troisième et dernier lieu, la CNIL a fait une exacte application des dispositions précitées de l'article 40 de la loi du 6 janvier 1978 en considérant que la graphie en lettres majuscules de la particule du patronyme de M. B...n'entachait pas d'inexactitude ses données personnelles et n'entraînait aucun risque de confusion ou d'erreur sur la personne et en décidant, par suite, de ne pas engager de procédure sur le fondement du I de l'article 45 de la loi du 6 janvier 1978 précité.<br/>
<br/>
              8. Il résulte de ce qui précède que M. B...n'est pas fondé à demander l'annulation pour excès de pouvoir des décisions des 9 juin et 4 octobre 2016 qui sont suffisamment motivées. Sa requête doit donc être rejetée y compris, par voie de conséquence, ses conclusions à fin d'injonction.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : La requête de M. B...est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. A...B...et à la Commission nationale de l'informatique et des libertés.   <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">26-07-05-02 DROITS CIVILS ET INDIVIDUELS. - PLAINTE FONDÉE SUR LA MÉCONNAISSANCE DES DROITS TIRÉS DU I DE L'ARTICLE 40 DE LA LOI DU 6 JANVIER 1978, NOTAMMENT DU DROIT DE RECTIFICATION DES DONNÉES À CARACTÈRE PERSONNEL - CONTRÔLE DU JUGE DE L'EXCÈS DE POUVOIR SUR L'APPRÉCIATION DE LA CNIL DES SUITES À Y DONNER - CONTRÔLE ENTIER [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-07-02-03 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. CONTRÔLE DU JUGE DE L'EXCÈS DE POUVOIR. APPRÉCIATIONS SOUMISES À UN CONTRÔLE NORMAL. - APPRÉCIATION DE LA CNIL DES SUITES À DONNER À UNE PLAINTE FONDÉE SUR LA MÉCONNAISSANCE DES DROITS TIRÉS DU I DE L'ARTICLE 40 DE LA LOI DU 6 JANVIER 1978, NOTAMMENT DU DROIT DE RECTIFICATION DES DONNÉES À CARACTÈRE PERSONNEL [RJ1].
</SCT>
<ANA ID="9A"> 26-07-05-02 Lorsque l'auteur de la plainte se fonde sur la méconnaissance des droits qu'il tient du I de l'article 40 de la loi n° 78-17 du 6 janvier 1978, notamment le droit de rectification de ses données personnelles, le pouvoir d'appréciation de la CNIL pour décider des suites à y donner s'exerce, eu égard à la nature des droits individuels en cause, sous l'entier contrôle du juge de l'excès de pouvoir.</ANA>
<ANA ID="9B"> 54-07-02-03 Lorsque l'auteur de la plainte se fonde sur la méconnaissance des droits qu'il tient du I de l'article 40 de la loi n° 78-17 du 6 janvier 1978, notamment le droit de rectification de ses données personnelles, le pouvoir d'appréciation de la CNIL pour décider des suites à y donner s'exerce, eu égard à la nature des droits individuels en cause, sous l'entier contrôle du juge de l'excès de pouvoir.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr. CE, Ass., 24 février 2017, Mme,, M.,, M.,et M.,, n°s 391000 393769 399999 401258, p. 59.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
