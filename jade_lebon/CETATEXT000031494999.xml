<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031494999</ID>
<ANCIEN_ID>JG_L_2015_11_000000372111</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/49/49/CETATEXT000031494999.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème / 9ème SSR, 18/11/2015, 372111</TITRE>
<DATE_DEC>2015-11-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>372111</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème / 9ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Isabelle Lemesle</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:372111.20151118</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              1° Sous le n° 372111, par une requête et deux mémoires en réplique, enregistrés le 11 septembre 2013, le 15 mai 2014 et le 14 janvier 2015 au secrétariat du contentieux du Conseil d'Etat, Mme B...D...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 12 juillet 2013 par laquelle le ministre de l'intérieur a rejeté sa demande tendant à l'abrogation de l'article 5 du décret n° 55-1397 du 22 octobre 1955 instituant la carte nationale d'identité ;<br/>
<br/>
              2°) à titre subsidiaire, d'enjoindre au ministre de l'intérieur soit d'abroger ces dispositions, soit de reprendre l'instruction de la demande d'abrogation de ces dispositions, dans les deux cas dans un délai de deux mois à compter de la décision du Conseil d'Etat et sous une astreinte de 200 euros par jour de retard ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              2° Sous le n° 372310, par une requête et deux mémoires en réplique, enregistrés le 20 septembre 2013, le 15 mai 2014 et le 14 janvier 2015 au secrétariat du contentieux du Conseil d'Etat, Mme A...C...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite par laquelle le ministre de l'intérieur a rejeté sa demande tendant à l'abrogation de l'article 5 du même décret ;<br/>
<br/>
              2°) à titre subsidiaire, d'enjoindre au ministre de l'intérieur soit d'abroger ces dispositions, soit de reprendre l'instruction de la demande d'abrogation de ces dispositions, dans les deux cas dans un délai de deux mois à compter de la décision du Conseil d'Etat et sous une astreinte de 200 euros par jour de retard ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu :<br/>
              - la directive 95/46/CE du Parlement européen et du Conseil du 24 octobre 1995 ;<br/>
              - la loi n° 78-17 du 6 janvier 1978, modifiée notamment par la loi n° 2004-801 du 6 août 2004 ;<br/>
              - la loi n° 2012-410 du 27 mars 2012 ;<br/>
              - le décret n° 55-1397 du 22 octobre 1955, notamment son article 5 modifié par le décret n° 99-973 du 25 novembre 1999 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Isabelle Lemesle, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que par deux lettres des 22 avril et 23 mai 2013, Mmes D...et C...ont demandé au Premier ministre d'abroger l'article 5 du décret du 22 octobre 1955 instituant la carte nationale d'identité ; que leurs demandes ont été rejetées par une lettre du 12 juillet 2013 adressée par le ministre de l'intérieur à MmeD..., d'une part et par une décision implicite, née du silence gardé sur la demande de MmeC..., d'autre part ; que, par deux requêtes qu'il y a lieu de joindre, Mmes D...et C...demandent l'annulation de ces décisions et qu'il soit enjoint au ministre de l'intérieur, sous astreinte, soit d'abroger ces dispositions, soit de reprendre l'instruction de leurs demandes d'abrogation ;<br/>
<br/>
              Sur les conclusions tendant à l'annulation des décisions attaquées :<br/>
<br/>
              En ce qui concerne le champ d'application de la loi du 6 janvier 1978 :<br/>
<br/>
              2. Considérant qu'aux termes de l'article 5 du décret du 22 octobre 1955 instituant la carte nationale d'identité, dans sa rédaction issue de l'article 1er du décret du 25 novembre 1999, antérieure à la transposition, par la loi du 6 août 2004, de la directive du 24 octobre 1995 relative à la protection des personnes physiques à l'égard du traitement des données à caractère personnel et à la libre circulation de ces données : " Lors de la constitution du dossier de demande de carte nationale d'identité, il est procédé au relevé d'une empreinte digitale de l'intéressé. Conservée au dossier par le service gestionnaire de la carte, l'empreinte digitale ne peut être utilisée qu'en vue : / 1° De la détection des tentatives d'obtention ou d'utilisation frauduleuse d'un titre d'identité ; / 2° De l'identification certaine d'une personne dans le cadre d'une procédure judiciaire " ; <br/>
<br/>
              3. Considérant qu'il résulte des dispositions de l'article 2 de la loi du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés, dans sa rédaction issue de la loi du 6 août 2004, que cette loi s'applique y compris aux traitements non automatisés de données à caractère personnel, entendus comme toute opération ou ensemble d'opérations portant sur de telles données, quel que soit le procédé utilisé, et notamment la collecte, l'enregistrement, l'organisation, la conservation, l'adaptation ou la modification, l'extraction, la consultation, l'utilisation, la communication par transmission, diffusion ou toute autre forme de mise à disposition, le rapprochement ou l'interconnexion, ainsi que le verrouillage, l'effacement ou la destruction ; que, par suite, la collecte, la conservation et la consultation des empreintes digitales effectuées sur le fondement de l'article 5 du décret du 22 octobre 1955, sous la responsabilité du ministre de l'intérieur, entrent dans le champ d'application de la loi du 6 janvier 1978, nonobstant la circonstance que ces fichiers ne sont pas numérisés et qu'ils ne sont constitués et conservés qu'au seul niveau des préfectures, pour l'arrondissement du chef lieu d'un département, ou des sous-préfectures, et des consulats ;<br/>
<br/>
              En ce qui concerne la durée de conservation des empreintes digitales :<br/>
<br/>
              4. Considérant qu'aux termes de l'article 6 de la même loi : " Un traitement ne peut porter que sur des données à caractère personnel qui satisfont aux conditions suivantes : (...)/5° Elles sont conservées sous une forme permettant l'identification des personnes concernées pendant une durée qui n'excède pas la durée nécessaire aux finalités pour lesquelles elles sont collectées et traitées " ; <br/>
<br/>
              5. Considérant que les empreintes digitales collectées sur le fondement de l'article 5 du décret du 22 octobre 1955 figurent, en application de l'article 2 de la loi du 27 mars 2012 relative à la protection de l'identité, au nombre des données du composant électronique sécurisé de la carte nationale d'identité ; qu'en revanche, elles ne font pas partie des données mémorisées dans le système permettant la fabrication et la gestion informatisée des cartes nationales d'identité sécurisées dont la liste est fixée par l'article 8 du décret du 22 octobre 1955 ; que, par suite, la durée de conservation fixée par l'article 9 de ce décret ne leur est pas applicable ; que la circonstance qu'un décret du 18 décembre 2013 a porté, d'une part, à quinze ans pour les personnes majeures et à dix ans pour les personnes mineures la durée de validité de la carte nationale d'identité et, d'autre part, à cinq ans la faculté de solliciter le renouvellement d'un titre périmé ne peut être regardée comme fondant légalement les durées de conservation des empreintes digitales dont le ministre de l'intérieur indique qu'elles sont de vingt ans pour les personnes majeures et de quinze ans pour les personnes mineures ;<br/>
<br/>
              6. Considérant que, faute de dispositions expresses la régissant, la durée de conservation des empreintes digitales relevées sur le fondement de l'article 5 du décret du 22 octobre 1955 est illimitée ; qu'une telle durée de conservation ne peut être regardée comme nécessaire aux finalités du fichier, eu égard à la durée de validité de la carte nationale d'identité et au délai dans lequel tout détenteur d'une carte nationale d'identité périmée peut en solliciter le renouvellement ; que, dès lors, Mmes D...et C...sont fondées à demander l'annulation des décisions rejetant leurs demandes tendant à l'abrogation de l'article 5 du décret du 22 octobre 1955, sans qu'il soit besoin d'examiner les autres moyens de leurs requêtes ;<br/>
<br/>
              Sur les conclusions à fin d'injonction :<br/>
<br/>
              7. Considérant qu'aux termes de l'article L. 911-1 du code de justice administrative : " Lorsque sa décision implique nécessairement qu'une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public prenne une mesure d'exécution dans un sens déterminé, la juridiction, saisie de conclusions en ce sens, prescrit, par la même décision, cette mesure assortie, le cas échéant, d'un délai d'exécution " ; <br/>
<br/>
              8. Considérant que si l'annulation des décisions du Premier ministre refusant d'abroger l'article 5 du décret du 22 octobre 1955 implique normalement que le juge lui enjoigne d'y procéder, il ressort des pièces du dossier que le traitement que ces dispositions autorisent est nécessaire à l'authentification de l'identité des personnes qui possèdent une carte nationale d'identité ; qu'aucun des autres moyens soulevés par les requérants n'aurait été de nature à justifier l'annulation prononcée par la présente décision ; qu'il appartient au Premier ministre, s'il entend maintenir dans ce traitement la collecte des empreintes digitales, de le mettre en conformité avec l'article 6 de la loi du 6 janvier 1978, conformément à la procédure qu'elle prévoit ;<br/>
<br/>
              9. Considérant que, dans ces conditions, il y a lieu de fixer à huit mois à compter de la notification de la présente décision le délai au terme duquel, s'il n'a pas pris le décret en Conseil d'Etat nécessaire, après avis de la Commission nationale de l'informatique et des libertés, à cette mise en conformité, le Premier ministre devra prononcer l'abrogation des dispositions de l'article 5 du décret du 20 octobre 1955 ; que, dans les circonstances de l'espèce, il n'y pas lieu d'assortir cette injonction d'une astreinte ;<br/>
<br/>
              Sur les conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              10. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 1 500 euros à verser, d'une part, à Mme D...et, d'autre part, à MmeC..., en application de ces dispositions ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : La décision du 12 juillet 2013 par laquelle le ministre de l'intérieur a refusé de proposer l'abrogation de l'article 5 du décret du 22 octobre 1955 instituant la carte nationale d'identité est annulée.<br/>
Article 2 : La décision implicite de rejet née du silence gardé par le Premier ministre sur la demande d'abrogation présentée aux mêmes fins par Mme C...est annulée.<br/>
Article 3 : Il est enjoint au Premier ministre de prendre les mesures définies aux points 8 et 9.<br/>
Article 4 : L'Etat versera à Mmes D...etC..., chacune, la somme de 1 500 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 5 : La présente décision sera notifiée à Mme B...D..., à Mme A...C..., au Premier ministre et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">26-01-04 DROITS CIVILS ET INDIVIDUELS. ÉTAT DES PERSONNES. QUESTIONS DIVERSES RELATIVES À L`ÉTAT DES PERSONNES. - CARTE NATIONALE D'IDENTITÉ - COLLECTE, CONSERVATION ET CONSULTATION DES EMPREINTES DIGITALES - 1) CHAMP D'APPLICATION DE LA LOI DU 6 JANVIER 1978 - INCLUSION - 2) DURÉE DE CONSERVATION ILLIMITÉE FAUTE DE DISPOSITIONS EXPRESSES LA RÉGISSANT - ILLÉGALITÉ.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">26-07-01-01-02 DROITS CIVILS ET INDIVIDUELS. - TRAITEMENTS NON AUTOMATISÉS DE DONNÉES À CARACTÈRE PERSONNEL SOUMIS À LA LOI DU 6 JANVIER 1978 - 1) NOTION - 2) COLLECTE, CONSERVATION ET CONSULTATION DES EMPREINTES DIGITALES RELEVÉES LORS D'UNE DEMANDE DE CARTE NATIONALE D'IDENTITÉ - INCLUSION.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">26-07-01-02-04 DROITS CIVILS ET INDIVIDUELS. - DURÉE DE CONSERVATION DES EMPREINTES DIGITALES RELEVÉES LORS D'UNE DEMANDE DE CARTE NATIONALE D'IDENTITÉ - DURÉE ILLIMITÉE FAUTE DE DISPOSITIONS EXPRESSES LA RÉGISSANT - ILLÉGALITÉ.
</SCT>
<ANA ID="9A"> 26-01-04 1) La collecte, la conservation et la consultation des empreintes digitales effectuées sur le fondement de l'article 5 du décret n° 55-1397 du 22 octobre 1955 instituant la carte nationale d'identité, sous la responsabilité du ministre de l'intérieur, entrent dans le champ d'application de la loi n° 78-17 du 6 janvier 1978, nonobstant la circonstance que ces fichiers ne sont pas numérisés et qu'ils ne sont constitués et conservés qu'au seul niveau des préfectures, pour l'arrondissement du chef-lieu d'un département, ou des sous-préfectures, et des consulats, dès lors qu'en vertu de l'article 2 de cette loi, dans sa rédaction issue de la loi n° 2004-801 du 6 août 2004, celle-ci s'applique y compris aux traitements non automatisés de données à caractère personnel.,,,2) Faute de dispositions expresses la régissant, la durée de conservation des empreintes digitales relevées sur le fondement de l'article 5 du décret du 22 octobre 1955 est illimitée. Une telle durée de conservation ne peut être regardée comme nécessaire aux finalités du fichier, eu égard à la durée de validité de la carte nationale d'identité et au délai dans lequel tout détenteur d'une carte nationale d'identité périmée peut en solliciter le renouvellement. Elle est donc illégale.</ANA>
<ANA ID="9B"> 26-07-01-01-02 1) Il résulte des dispositions de l'article 2 de la loi n° 78-17 du 6 janvier 1978, dans sa rédaction issue de la loi n° 2004-801 du 6 août 2004, que cette loi s'applique y compris aux traitements non automatisés de données à caractère personnel, entendus comme toute opération ou ensemble d'opérations portant sur de telles données, quel que soit le procédé utilisé, et notamment la collecte, l'enregistrement, l'organisation, la conservation, l'adaptation ou la modification, l'extraction, la consultation, l'utilisation, la communication par transmission, diffusion ou toute autre forme de mise à disposition, le rapprochement ou l'interconnexion, ainsi que le verrouillage, l'effacement ou la destruction.,,,2) Par suite, la collecte, la conservation et la consultation des empreintes digitales effectuées sur le fondement de l'article 5 du décret n° 55-1397 du 22 octobre 1955 instituant la carte nationale d'identité, sous la responsabilité du ministre de l'intérieur, entrent dans le champ d'application de la loi du 6 janvier 1978, nonobstant la circonstance que ces fichiers ne sont pas numérisés et qu'ils ne sont constitués et conservés qu'au seul niveau des préfectures, pour l'arrondissement du chef lieu d'un département, ou des sous-préfectures, et des consulats.</ANA>
<ANA ID="9C"> 26-07-01-02-04 Faute de dispositions expresses la régissant, la durée de conservation des empreintes digitales relevées sur le fondement de l'article 5 du décret n° 55-1397 du 22 octobre 1955 instituant la carte nationale d'identité est illimitée. Une telle durée de conservation ne peut être regardée comme nécessaire aux finalités du fichier, eu égard à la durée de validité de la carte nationale d'identité et au délai dans lequel tout détenteur d'une carte nationale d'identité périmée peut en solliciter le renouvellement. Elle est donc illégale.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
