<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041569377</ID>
<ANCIEN_ID>JG_L_2020_02_000000418880</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/56/93/CETATEXT000041569377.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 5ème chambres réunies, 12/02/2020, 418880</TITRE>
<DATE_DEC>2020-02-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>418880</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>M. Didier Ribes</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Stéphane Hoynck</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2020:418880.20200212</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              1° Sous le n° 418880, par une requête, enregistrée le 9 mars 2018 au secrétariat du contentieux du Conseil d'État, M. C... B... demande au Conseil d'État :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté du 16 janvier 2018 par lequel le garde des sceaux, ministre de la justice a déclaré vacant l'office notarial dont M. D... A... était titulaire jusqu'au 28 juin 2017 et a ouvert la procédure de candidature prévue à l'article 56 du décret n° 73-609 du 5 juillet 1973 modifié ;<br/>
<br/>
              2°) de mettre à la charge de l'État une somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              2° Sous le n° 419938, par une ordonnance n° 1802209 du 17 avril 2018, enregistrée le 18 avril 2018 au secrétariat du contentieux du Conseil d'État, le président du tribunal administratif de Paris a transmis au Conseil d'État, en application de l'article R. 351-2 du code de justice administrative, la requête présentée à ce tribunal par M. C... B....<br/>
<br/>
              Par cette requête et un mémoire complémentaire, enregistrés au greffe du tribunal administratif de Paris les 12 et 19 février 2018, M. B... demande au Conseil d'État :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le même arrêté ;<br/>
<br/>
              2°) de mettre à la charge de l'État une somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - le décret n° 73-609 du 5 juillet 1973 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Didier Ribes, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Stéphane Hoynck, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de M. B... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Les requêtes visées ci-dessus sont dirigées contre la même décision. Il y a lieu de les joindre pour statuer par une seule décision.<br/>
<br/>
              2. Aux termes de l'article 56 du décret du 5 juillet 1973 relatif à la formation professionnelle dans le notariat et aux conditions d'accès aux fonctions de notaire, dans sa rédaction applicable au litige : " Lorsqu'il n'a pas été ou qu'il ne peut être pourvu par l'exercice du droit de présentation à un office de notaire dépourvu de titulaire, cet office est déclaré vacant par arrêté du garde des sceaux, ministre de la justice. / Cet arrêté ouvre la procédure de candidature aux date et heure qu'il précise. / L'article 49 du présent décret est applicable. / Les candidatures sont enregistrées dans les formes et accompagnées des pièces mentionnées à l'article 51 du présent décret. / La candidature doit être accompagnée d'un engagement de payer l'indemnité fixée par le garde des sceaux, ministre de la justice. Lorsque le candidat doit contracter un emprunt, la demande est accompagnée des éléments permettant d'apprécier ses possibilités financières au regard des engagements contractés. / (...) ". <br/>
<br/>
              3. Sur le fondement de ces dispositions, le garde des sceaux, ministre de la justice a, par un arrêté du 16 janvier 2018, déclaré vacant l'office notarial dont M. D... A... était titulaire jusqu'au 28 juin 2017 et a ouvert la procédure de candidature.<br/>
<br/>
              4. Si l'arrêté par lequel, en application des dispositions de l'article 52 de la loi du 6 août 2015 pour la croissance, l'activité et l'égalité des chances économiques, les ministres de la justice et de l'économie fixent conjointement, sur proposition de l'Autorité de la concurrence, les zones dans lesquelles les notaires peuvent librement s'installer ainsi que le nombre d'offices à créer dans ces zones pour les deux années à venir, est relatif à l'organisation du service public notarial, la décision par laquelle le garde des sceaux, ministre de la justice, déclare vacant un office existant et ouvre la procédure de candidature à la nomination dans cet office, qui concerne le fonctionnement du service public notarial mais n'a pas, par elle-même, pour objet d'assurer son organisation, est dépourvue de caractère réglementaire. Dès lors, elle n'entre pas dans le champ de l'article R. 311-1 du code de justice administrative. Par suite, le Conseil d'Etat n'est pas compétent pour connaître en premier et dernier ressort des conclusions de M. B... tendant à l'annulation pour excès de pouvoir de l'arrêté du 16 janvier 2018 par lequel le garde des sceaux, ministre de la justice, a déclaré vacant l'office notarial dont M. D... A... était titulaire à Paris jusqu'au 28 juin 2017 et ouvert la procédure de candidature. Il y a lieu, en application de l'article R. 351-1 du code de justice administrative, d'en attribuer le jugement au tribunal administratif de Paris, compétent pour en connaître en vertu de l'article R. 312-10 du même code.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement des requêtes de M. B... est attribué au tribunal administratif de Paris.<br/>
Article 2 : La présente décision sera notifiée à M. C... B..., à la garde des sceaux, ministre de la justice et à M. D... A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-01-06-01-02 ACTES LÉGISLATIFS ET ADMINISTRATIFS. DIFFÉRENTES CATÉGORIES D'ACTES. ACTES ADMINISTRATIFS - CLASSIFICATION. ACTES RÉGLEMENTAIRES. NE PRÉSENTENT PAS CE CARACTÈRE. - DÉCISION MINISTÉRIELLE DÉCLARANT VACANT UN OFFICE NOTARIAL EXISTANT ET OUVRANT LA PROCÉDURE DE CANDIDATURE À LA NOMINATION DANS CET OFFICE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">17-05-01-01-01 COMPÉTENCE. COMPÉTENCE À L'INTÉRIEUR DE LA JURIDICTION ADMINISTRATIVE. COMPÉTENCE EN PREMIER RESSORT DES TRIBUNAUX ADMINISTRATIFS. COMPÉTENCE MATÉRIELLE. ACTES NON RÉGLEMENTAIRES. - INCLUSION - DÉCISION MINISTÉRIELLE DÉCLARANT VACANT UN OFFICE EXISTANT ET OUVRANT LA PROCÉDURE DE CANDIDATURE À LA NOMINATION DANS CET OFFICE [RJ1].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">17-05-02-04 COMPÉTENCE. COMPÉTENCE À L'INTÉRIEUR DE LA JURIDICTION ADMINISTRATIVE. COMPÉTENCE DU CONSEIL D'ETAT EN PREMIER ET DERNIER RESSORT. ACTES RÉGLEMENTAIRES DES MINISTRES. - EXCLUSION - DÉCISION MINISTÉRIELLE DÉCLARANT VACANT UN OFFICE EXISTANT ET OUVRANT LA PROCÉDURE DE CANDIDATURE À LA NOMINATION DANS CET OFFICE [RJ1].
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">55-03-05-03 PROFESSIONS, CHARGES ET OFFICES. CONDITIONS D'EXERCICE DES PROFESSIONS. PROFESSIONS S'EXERÇANT DANS LE CADRE D'UNE CHARGE OU D'UN OFFICE. NOTAIRES. - DÉCISION MINISTÉRIELLE DÉCLARANT VACANT UN OFFICE EXISTANT ET OUVRANT LA PROCÉDURE DE CANDIDATURE À LA NOMINATION DANS CET OFFICE - ACTE NE PRÉSENTANT PAS UN CARACTÈRE RÉGLEMENTAIRE [RJ1].
</SCT>
<ANA ID="9A"> 01-01-06-01-02 Si l'arrêté par lequel, en application des dispositions de l'article 52 de la loi n° 2015-990 du 6 août 2015, les ministres de la justice et de l'économie fixent conjointement, sur proposition de l'Autorité de la concurrence, les zones dans lesquelles les notaires peuvent librement s'installer ainsi que le nombre d'offices à créer dans ces zones pour les deux années à venir, est relatif à l'organisation du service public notarial, la décision par laquelle le garde des sceaux, ministre de la justice, déclare vacant un office existant et ouvre la procédure de candidature à la nomination dans cet office, qui concerne le fonctionnement du service public notarial mais n'a pas, par elle-même, pour objet d'assurer son organisation, est dépourvue de caractère réglementaire.</ANA>
<ANA ID="9B"> 17-05-01-01-01 Si l'arrêté par lequel, en application des dispositions de l'article 52 de la loi n° 2015-990 du 6 août 2015, les ministres de la justice et de l'économie fixent conjointement, sur proposition de l'Autorité de la concurrence, les zones dans lesquelles les notaires peuvent librement s'installer ainsi que le nombre d'offices à créer dans ces zones pour les deux années à venir, est relatif à l'organisation du service public notarial, la décision par laquelle le garde des sceaux, ministre de la justice, déclare vacant un office existant et ouvre la procédure de candidature à la nomination dans cet office, qui concerne le fonctionnement du service public notarial mais n'a pas, par elle-même, pour objet d'assurer son organisation, est dépourvue de caractère réglementaire.,,,Dès lors, elle n'entre pas dans le champ de l'article R. 311-1 du code de justice administrative. Par suite, le Conseil d'Etat n'est pas compétent pour connaître en premier et dernier ressort des conclusions tendant à l'annulation pour excès de pouvoir de l'arrêté par lequel le garde des sceaux, ministre de la justice, a déclaré vacant l'office notarial et ouvert la procédure de candidature. Il y a lieu, en application de l'article R. 351-1 du code de justice administrative, d'en attribuer le jugement au tribunal administratif de Paris, compétent pour en connaître en vertu de l'article R. 312-10 du même code.</ANA>
<ANA ID="9C"> 17-05-02-04 Si l'arrêté par lequel, en application des dispositions de l'article 52 de la loi n° 2015-990 du 6 août 2015, les ministres de la justice et de l'économie fixent conjointement, sur proposition de l'Autorité de la concurrence, les zones dans lesquelles les notaires peuvent librement s'installer ainsi que le nombre d'offices à créer dans ces zones pour les deux années à venir, est relatif à l'organisation du service public notarial, la décision par laquelle le garde des sceaux, ministre de la justice, déclare vacant un office existant et ouvre la procédure de candidature à la nomination dans cet office, qui concerne le fonctionnement du service public notarial mais n'a pas, par elle-même, pour objet d'assurer son organisation, est dépourvue de caractère réglementaire.,,,Dès lors, elle n'entre pas dans le champ de l'article R. 311-1 du code de justice administrative. Par suite, le Conseil d'Etat n'est pas compétent pour connaître en premier et dernier ressort des conclusions tendant à l'annulation pour excès de pouvoir de l'arrêté par lequel le garde des sceaux, ministre de la justice, a déclaré vacant l'office notarial et ouvert la procédure de candidature. Il y a lieu, en application de l'article R. 351-1 du code de justice administrative, d'en attribuer le jugement au tribunal administratif de Paris, compétent pour en connaître en vertu de l'article R. 312-10 du même code.</ANA>
<ANA ID="9D"> 55-03-05-03 Si l'arrêté par lequel, en application des dispositions de l'article 52 de la loi n° 2015-990 du 6 août 2015, les ministres de la justice et de l'économie fixent conjointement, sur proposition de l'Autorité de la concurrence, les zones dans lesquelles les notaires peuvent librement s'installer ainsi que le nombre d'offices à créer dans ces zones pour les deux années à venir, est relatif à l'organisation du service public notarial, la décision par laquelle le garde des sceaux, ministre de la justice, déclare vacant un office existant et ouvre la procédure de candidature à la nomination dans cet office, qui concerne le fonctionnement du service public notarial mais n'a pas, par elle-même, pour objet d'assurer son organisation, est dépourvue de caractère réglementaire.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., s'agissant de la décision ministérielle créant un nouvel office ou se prononçant sur l'ouverture d'un bureau annexe à un office existant, CE, 28 décembre 2018, M. Laffon et autres, n° 409441, T. p. 658.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
