<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037070271</ID>
<ANCIEN_ID>JG_L_2018_06_000000410721</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/07/02/CETATEXT000037070271.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 5ème chambres réunies, 14/06/2018, 410721</TITRE>
<DATE_DEC>2018-06-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>410721</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUVENIN, COUDRAY, GREVY</AVOCATS>
<RAPPORTEUR>Mme Laurence Franceschini</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Louis Dutheillet de Lamothe</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:410721.20180614</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A...B...a demandé au tribunal administratif de Lyon d'annuler pour excès de pouvoir les décisions du 20 août 2015 par lesquels le préfet de l'Ardèche a constaté que son certificat de résidence algérien était périmé et le lui a retiré, lui a fait obligation de quitter le territoire français, a fixé un délai de départ volontaire de trente jours, a désigné un pays de destination et l'a astreinte à se présenter régulièrement aux services de police. Par un jugement n° 1510998 du 30 juin 2016, le tribunal administratif de Lyon a fait droit à cette demande.<br/>
<br/>
              Par un arrêt n° 16LY02276 du 26 janvier 2017, la cour administrative d'appel de Lyon a, sur appel du préfet de l'Ardèche, annulé ce jugement et rejeté la demande présentée par Mme B...devant le tribunal administratif de Lyon. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un nouveau mémoire, enregistrés respectivement le 22 mai 2017, le 10 août 2017 et le 3 novembre 2017 au secrétariat du contentieux du Conseil d'Etat, Mme B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel du préfet de l'Ardèche ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - l'accord franco-algérien du 27 décembre 1968 modifié ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Laurence Franceschini, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Louis Dutheillet de Lamothe, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Thouvenin, Coudray, Grevy, avocat de MmeB....<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1.	Il ressort des pièces du dossier soumis aux juges du fond que Mme B..., ressortissante algérienne née en 1983, est entrée en France une première fois en novembre 2001 en compagnie de sa mère et d'un frère pour rejoindre son père dans le cadre d'un regroupement familial et a obtenu, à ce titre, un certificat de résidence valable du 1er février 2002 au 10 février 2012. Elle a obtenu le renouvellement de son certificat de résidence, du 11 février 2012 au 10 février 2022. Cependant, estimant que le certificat de résidence dont elle disposait était périmé, le préfet de l'Ardèche, par des décisions du 20 août 2015, lui a demandé de restituer ce document, lui a fait obligation de quitter le territoire français, a fixé un délai de départ volontaire de trente jours, a désigné le pays à destination duquel elle serait renvoyée en cas d'exécution forcée de la mesure et l'a astreinte à se présenter aux services de police pour justifier des diligences accomplies pour préparer son départ. Mme B...se pourvoit en cassation contre l'arrêt du 26 janvier 2017 par lequel la cour administrative d'appel de Lyon a, sur appel du préfet de l'Ardèche, annulé le jugement du tribunal administratif de Lyon du 30 juin 2016 qui avait annulé, à la demande de MmeB..., l'ensemble des décisions litigieuses et rejeté la demande de l'intéressée.<br/>
<br/>
              2. Aux termes de l'article 8 de l'accord franco-algérien du 27 décembre 1968 : " Le certificat de résidence d'un ressortissant algérien qui aura quitté le territoire français pendant une période de plus de trois ans consécutifs est périmé. Toutefois, il lui sera possible de demander la prolongation de la période visée au premier alinéa, soit avant son départ de France, soit par l'intermédiaire des Ambassades et Consulats français. ". En application de ces stipulations, un certificat de résidence n'est périmé qu'en cas d'absence du territoire français pendant une période de plus de trois années consécutives, qui n'est interrompue par aucun séjour en France ou par des retours qui, étant purement ponctuels, ne permettent pas de regarder l'intéressé comme ayant interrompu son absence du territoire national. <br/>
<br/>
              3. Il ressort des énonciations de l'arrêt attaqué que, pour juger que le préfet de l'Ardèche avait pu légalement regarder le certificat de résidence de Mme B...comme périmé, la cour administrative d'appel, après avoir relevé qu'elle avait quitté la France pour résider "de manière stable et continue" en Algérie depuis au moins la mi-décembre 2009, a jugé que les séjours effectués depuis lors et jusqu'en août 2015 par l'intéressée sur le territoire français, notamment pendant environ un mois en 2013 pour l'accouchement de son second enfant, "ne sauraient être regardés comme valant fixation en France du centre de ses intérêts personnels ni, par conséquent, interruption de la période de plus de trois années consécutives à compter de décembre 2009 qu'elle a passé hors du territoire national". Il résulte de ce qui a été dit au point 2 que, en soumettant ainsi l'absence de péremption du certificat de résidence d'un ressortissant algérien ayant quitté le territoire national à un nouveau transfert en France du centre de ses intérêts personnels avant l'expiration du délai de trois ans, la cour a commis une erreur de droit et méconnu les stipulations précitées. Par suite, son arrêt doit être annulé.<br/>
<br/>
              4. Mme B...a obtenu le bénéfice de l'aide juridictionnelle ; par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ; il y a lieu, dans les circonstances de l'espèce, et sous réserve que la SCP Masse-Dessen, Thouvenin, Coudray, avocat de Mme B...renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de l'Etat  la somme de 3 000 euros à verser à cette SCP.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Lyon du 26 janvier 2017 est annulé.<br/>
<br/>
Article 2 : L'affaire est renvoyée devant la cour administrative d'appel de Lyon.<br/>
<br/>
Article 3 : L'Etat versera à la SCP Masse-Dessen, Thouvenin, Coudray, avocat de Mme B..., la somme de 3 000 euros au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991, sous réserve que cette SCP renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
<br/>
Article 4 : La présente décision sera notifiée à Mme B...et au ministre d'Etat, ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">335-01-01-02 ÉTRANGERS. SÉJOUR DES ÉTRANGERS. TEXTES APPLICABLES. CONVENTIONS INTERNATIONALES. - ACCORD FRANCO-ALGÉRIEN DU 27 DÉCEMBRE 1968 - CERTIFICAT DE RÉSIDENCE - CONDITION DE PÉREMPTION - ABSENCE DU TERRITOIRE FRANÇAIS PENDANT UNE PÉRIODE DE PLUS DE TROIS ANS CONSÉCUTIFS - SÉJOURS PONCTUELS EN FRANCE - INTERRUPTION DE LA PÉRIODE - ABSENCE.
</SCT>
<ANA ID="9A"> 335-01-01-02 En application de l'article 8 de l'accord franco-algérien du 27 décembre 1968, un certificat de résidence n'est périmé qu'en cas d'absence du territoire français pendant une période de plus de trois années consécutives, qui n'est interrompue par aucun séjour en France ou par des retours qui, étant purement ponctuels, ne permettent pas de regarder l'intéressé comme ayant interrompu son absence du territoire national.... ,,Commet une erreur de droit la cour qui subordonne la validité du certificat de résidence d'un ressortissant algérien ayant quitté le territoire national à un nouveau transfert en France du centre de ses intérêts personnels avant l'expiration du délai de trois ans.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
