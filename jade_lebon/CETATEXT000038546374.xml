<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038546374</ID>
<ANCIEN_ID>JG_L_2019_06_000000415040</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/54/63/CETATEXT000038546374.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Section, 03/06/2019, 415040, Publié au recueil Lebon</TITRE>
<DATE_DEC>2019-06-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>415040</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Section</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>M. Arnaud Skzryerbak</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESEC:2019:415040.20190603</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Lyon d'annuler la décision prise le 29 septembre 2015 par Pôle emploi de récupérer un indu d'allocation de solidarité spécifique d'un montant de 5 627,82 euros, ainsi que la décision du 18 janvier 2016 rejetant sa demande de remise gracieuse de cet indu. Par un jugement n° 1601102 du 4 avril 2017, le tribunal administratif de Lyon a rejeté sa demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 16 octobre 2017 et 15 janvier 2018 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) de mettre à la charge de Pôle emploi la somme de 3 500 euros à verser à la SCP Rocheteau, Uzan-Sarano, son avocat, au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code du travail ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Arnaud Skzryerbak, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de M. B...;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que, par un courrier du 29 septembre 2015, le directeur de l'agence Pôle emploi de Saint-Chamond a fait connaître à M. B...sa décision de récupérer un indu d'allocation de solidarité spécifique d'un montant de 5 627,82 euros au titre des mois de mars 2013 à septembre 2014 et que, par décision du 18 janvier 2016, le directeur de l'agence Pôle emploi de Saint-Etienne a rejeté la demande de remise gracieuse de cet indu que l'intéressé avait formée le 26 octobre 2015. Par un jugement du 4 avril 2017, contre lequel M. B... se pourvoit en cassation, le tribunal administratif de Lyon a rejeté sa demande tendant à l'annulation de ces décisions.<br/>
<br/>
              Sur le jugement attaqué, en tant qu'il statue sur la décision de récupération de l'indu :<br/>
<br/>
              2. Le tribunal administratif de Lyon a rejeté les conclusions de M. B...tendant à l'annulation de la décision de récupération de l'indu de 5 627,82 euros comme irrecevables, au motif qu'il n'avait pas formé le recours administratif préalable qu'exigent les dispositions de l'article R. 5426-19 du code du travail. M. B...ne critique l'irrecevabilité qui lui a été ainsi opposée par aucun des moyens de son pourvoi. Par suite, ses conclusions tendant à l'annulation du jugement du tribunal administratif de Lyon en tant qu'il statue sur la décision de récupération de l'indu doivent être rejetées.<br/>
<br/>
              Sur le jugement attaqué, en tant qu'il statue sur le refus de remise gracieuse :<br/>
<br/>
              3. Lorsqu'il statue sur un recours dirigé contre une décision refusant ou ne faisant que partiellement droit à une demande de remise gracieuse d'un indu d'une prestation ou d'une allocation versée au titre de l'aide ou de l'action sociale, du logement ou en faveur des travailleurs privés d'emploi, il appartient au juge administratif, eu égard tant à la finalité de son intervention qu'à sa qualité de juge de plein contentieux, non de se prononcer sur les éventuels vices propres de la décision attaquée, mais d'examiner si une remise gracieuse totale ou partielle est susceptible d'être accordée, en se prononçant lui-même sur la demande au regard des dispositions applicables et des circonstances de fait dont il est justifié par l'une et l'autre parties à la date de sa propre décision.<br/>
<br/>
              4. Il résulte des termes du jugement attaqué que le tribunal administratif de Lyon a estimé que la demande formée par M. B... devant lui, dirigée contre le refus de remise gracieuse de l'indu d'allocation de solidarité spécifique qui lui était réclamé, relevait du contentieux de l'excès de pouvoir. En statuant ainsi, le tribunal a méconnu son office.<br/>
<br/>
              5. Il résulte de ce qui précède, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que le jugement attaqué doit être annulé en tant qu'il statue sur le refus de remise gracieuse opposé à M.B....<br/>
<br/>
              6. Dans les circonstances de l'espèce, il y a lieu de régler, dans cette mesure, l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              Sur la remise gracieuse sollicitée par M.B... :<br/>
<br/>
              7. Sur le fondement de l'article L. 5426-8-3 du code du travail, Pôle emploi est autorisé " à différer ou à abandonner la mise en recouvrement des allocations, aides, ainsi que de toute autre prestation indûment versées (...) pour le compte de l'État (...) ". Pour l'application de ces dispositions, il y a lieu de rechercher si la situation de précarité du débiteur et sa bonne foi justifient que lui soit accordée une remise ou une réduction de dette.<br/>
<br/>
              8. D'une part, M.B..., qui se borne à une simple allégation, ne conteste pas sérieusement que, ainsi que le fait valoir Pôle emploi, il n'a pas déclaré, lors de ses actualisations mensuelles de 2013 et 2014, les périodes au cours desquelles il a travaillé, en méconnaissance des articles L. 5411-2, R. 5411-6 et R. 5411-7 du code du travail. <br/>
<br/>
              9. D'autre part, en réponse à la mesure d'instruction diligentée par la 1ère chambre de la section du contentieux, M. B... a justifié être toujours inscrit comme demandeur d'emploi et percevoir l'allocation de solidarité spécifique, au titre de laquelle il a perçu 411 euros en février 2019. Si, en vertu de l'article R. 5423-1 du code du travail, le bénéfice de cette allocation est subordonné à la condition que le demandeur d'emploi justifie, à la date de sa demande, de ressources mensuelles inférieures à un plafond correspondant à 70 fois le montant journalier de l'allocation pour une personne seule et 110 fois le même montant pour un couple, il ne résulte pas de l'instruction que la situation de précarité de M. B... serait telle qu'il y aurait lieu de lui accorder, en dépit de son manquement délibéré à ses obligations déclaratives, une remise gracieuse totale ou partielle de l'indu qui lui est réclamé.<br/>
<br/>
              10. Si M. B...fait valoir que la somme qui lui est réclamée est couverte par la prescription, un tel moyen peut être utilement soulevé à l'encontre d'une décision de récupération d'indu mais non à l'encontre du rejet d'une demande de remise gracieuse. De même, il résulte de ce qui a été dit au point 3 que M. B...ne peut utilement soutenir que la décision rejetant sa demande de remise gracieuse a été prise par une autorité incompétente, n'est pas signée et ne comporte pas les nom et prénom de son auteur, ni qu'elle est insuffisamment motivée.<br/>
<br/>
              11. Il résulte de ce qui précède que la demande de M. B... doit être rejetée.<br/>
<br/>
              Sur les frais liés à l'instance :<br/>
<br/>
              12. Les dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 font obstacle à ce qu'il soit fait droit aux conclusions présentées sur le fondement de ces dispositions par la SCP Rocheteau, Uzan-Sarano, avocat de M. B....<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le jugement du tribunal administratif de Lyon du 4 avril 2017 est annulé en tant qu'il statue sur le refus de remise gracieuse opposé à M.B....<br/>
Article 2 : La demande de remise gracieuse présentée par M. B...est rejetée.<br/>
Article 3 : Les conclusions de la SCP Rocheteau, Uzan-Sarano, avocat de M.B..., présentées au titre des dispositions de l'article 37 de la loi du 10 juillet 1991 sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à M. A... B...et à Pôle emploi.<br/>
Copie en sera adressée à la ministre du travail et à la ministre des solidarités et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">04-04 AIDE SOCIALE. CONTENTIEUX DE L'AIDE SOCIALE ET DE LA TARIFICATION. - RECOURS DIRIGÉ CONTRE UNE DÉCISION DE L'ADMINISTRATION REFUSANT OU NE FAISANT QUE PARTIELLEMENT DROIT À UNE DEMANDE DE REMISE GRACIEUSE EN MATIÈRE D'AIDE OU D'ACTION SOCIALE, DE LOGEMENT OU AU TITRE DES DISPOSITIONS EN FAVEUR DES TRAVAILLEURS PRIVÉS D'EMPLOI - RECOURS DE PLEIN CONTENTIEUX - OFFICE DU JUGE - EXAMEN LIMITÉ AUX DROITS DE L'INTÉRESSÉ SANS EXAMEN D'ÉVENTUELS VICES PROPRES DE LA DÉCISION [RJ1] - JUGE STATUANT AU REGARD DES DISPOSITIONS APPLICABLES ET DES CIRCONSTANCES DE FAIT À LA DATE DE SA PROPRE DÉCISION.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-02-02-01 PROCÉDURE. DIVERSES SORTES DE RECOURS. RECOURS DE PLEIN CONTENTIEUX. RECOURS AYANT CE CARACTÈRE. - RECOURS DIRIGÉ CONTRE UNE DÉCISION DE L'ADMINISTRATION REFUSANT OU NE FAISANT QUE PARTIELLEMENT DROIT À UNE DEMANDE DE REMISE GRACIEUSE EN MATIÈRE D'AIDE OU D'ACTION SOCIALE, DE LOGEMENT OU AU TITRE DES DISPOSITIONS EN FAVEUR DES TRAVAILLEURS PRIVÉS D'EMPLOI - OFFICE DU JUGE - EXAMEN LIMITÉ AUX DROITS DE L'INTÉRESSÉ SANS EXAMEN D'ÉVENTUELS VICES PROPRES DE LA DÉCISION [RJ1] - JUGE STATUANT AU REGARD DES DISPOSITIONS APPLICABLES ET DES CIRCONSTANCES DE FAIT À LA DATE DE SA PROPRE DÉCISION.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-07-01-04-03 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. QUESTIONS GÉNÉRALES. MOYENS. MOYENS INOPÉRANTS. - MOYENS SOULEVÉS CONTRE UNE DÉCISION DE L'ADMINISTRATION REFUSANT OU NE FAISANT QUE PARTIELLEMENT DROIT À UNE DEMANDE DE REMISE GRACIEUSE EN MATIÈRE D'AIDE OU D'ACTION SOCIALE, DE LOGEMENT OU AU TITRE DES DISPOSITIONS EN FAVEUR DES TRAVAILLEURS PRIVÉS D'EMPLOI, TIRÉS DES VICES PROPRES DE LA DÉCISION [RJ1].
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">54-07-03 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. POUVOIRS DU JUGE DE PLEIN CONTENTIEUX. - RECOURS DIRIGÉ CONTRE UNE DÉCISION DE L'ADMINISTRATION REFUSANT OU NE FAISANT QUE PARTIELLEMENT DROIT À UNE DEMANDE DE REMISE GRACIEUSE EN MATIÈRE D'AIDE OU D'ACTION SOCIALE, DE LOGEMENT OU AU TITRE DES DISPOSITIONS EN FAVEUR DES TRAVAILLEURS PRIVÉS D'EMPLOI - OFFICE DU JUGE - EXAMEN LIMITÉ AUX DROITS DE L'INTÉRESSÉ SANS EXAMEN D'ÉVENTUELS VICES PROPRES DE LA DÉCISION [RJ1] - JUGE STATUANT AU REGARD DES DISPOSITIONS APPLICABLES ET DES CIRCONSTANCES DE FAIT À LA DATE DE SA PROPRE DÉCISION.
</SCT>
<ANA ID="9A"> 04-04 Lorsqu'il statue sur un recours dirigé contre une décision refusant ou ne faisant que partiellement droit à une demande de remise gracieuse d'un indu d'une prestation ou d'une allocation versée au titre de l'aide ou de l'action sociale, du logement ou en faveur des travailleurs privés d'emploi, il appartient au juge administratif, eu égard tant à la finalité de son intervention qu'à sa qualité de juge de plein contentieux, non de se prononcer sur les éventuels vices propres de la décision attaquée, mais d'examiner si une remise gracieuse totale ou partielle est susceptible d'être accordée, en se prononçant lui-même sur la demande au regard des dispositions applicables et des circonstances de fait dont il est justifié par l'une et l'autre parties à la date de sa propre décision.</ANA>
<ANA ID="9B"> 54-02-02-01 Lorsqu'il statue sur un recours dirigé contre une décision refusant ou ne faisant que partiellement droit à une demande de remise gracieuse d'un indu d'une prestation ou d'une allocation versée au titre de l'aide ou de l'action sociale, du logement ou en faveur des travailleurs privés d'emploi, il appartient au juge administratif, eu égard tant à la finalité de son intervention qu'à sa qualité de juge de plein contentieux, non de se prononcer sur les éventuels vices propres de la décision attaquée, mais d'examiner si une remise gracieuse totale ou partielle est susceptible d'être accordée, en se prononçant lui-même sur la demande au regard des dispositions applicables et des circonstances de fait dont il est justifié par l'une et l'autre parties à la date de sa propre décision.</ANA>
<ANA ID="9C"> 54-07-01-04-03 Lorsqu'il statue sur un recours dirigé contre une décision refusant ou ne faisant que partiellement droit à une demande de remise gracieuse d'un indu d'une prestation ou d'une allocation versée au titre de l'aide ou de l'action sociale, du logement ou en faveur des travailleurs privés d'emploi, il appartient au juge administratif, eu égard tant à la finalité de son intervention qu'à sa qualité de juge de plein contentieux, non de se prononcer sur les éventuels vices propres de la décision attaquée, mais d'examiner si une remise gracieuse totale ou partielle est susceptible d'être accordée, en se prononçant lui-même sur la demande au regard des dispositions applicables et des circonstances de fait dont il est justifié par l'une et l'autre parties à la date de sa propre décision.</ANA>
<ANA ID="9D"> 54-07-03 Lorsqu'il statue sur un recours dirigé contre une décision refusant ou ne faisant que partiellement droit à une demande de remise gracieuse d'un indu d'une prestation ou d'une allocation versée au titre de l'aide ou de l'action sociale, du logement ou en faveur des travailleurs privés d'emploi, il appartient au juge administratif, eu égard tant à la finalité de son intervention qu'à sa qualité de juge de plein contentieux, non de se prononcer sur les éventuels vices propres de la décision attaquée, mais d'examiner si une remise gracieuse totale ou partielle est susceptible d'être accordée, en se prononçant lui-même sur la demande au regard des dispositions applicables et des circonstances de fait dont il est justifié par l'une et l'autre parties à la date de sa propre décision.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., s'agissant du recours contre une décision refusant ou ne faisant que partiellement droit à une demande de remise gracieuse d'un indu de RSA, CE, 9 mars 2016, Mme,, n° 381272, p. 53.,Rappr., s'agissant du recours dirigé contre une décision de l'administration déterminant les droits d'une personne en matière d'aide ou d'action sociale, de logement ou au titre des dispositions en faveur des travailleurs privés d'emploi, sans remettre en cause des versements déjà effectués, CE, Section, décision du même jour, Mme,, n° 423001, à publier au Recueil et CE, Section, décision du même jour, M.,, n° 422873, à publier au Recueil ; s'agissant du recours contre une décision refusant une prise en charge par le service de l'ASE, CE, Section, décision du même jour, Département de l'Oise, n° 419903, à publier au Recueil.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
