<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028135658</ID>
<ANCIEN_ID>JG_L_2013_10_000000346569</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/13/56/CETATEXT000028135658.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 1ère sous-sections réunies, 29/10/2013, 346569, Publié au recueil Lebon</TITRE>
<DATE_DEC>2013-10-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>346569</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 1ère sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Didier Ribes</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier de Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:346569.20131029</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU> Vu la requête, enregistrée le 9 février 2011 au secrétariat du contentieux du Conseil d'État, présentée par M. A...B..., demeurant ... ; M. B...demande au Conseil d'Etat d'annuler pour excès de pouvoir la décision du 9 décembre 2010 du Conseil supérieur de la magistrature, en ce qu'elle émet un avis non conforme à sa nomination en qualité de vice-président placé auprès du premier président de la cour d'appel de Saint-Denis de La Réunion ;<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution ;<br/>
<br/>
              Vu l'ordonnance n° 58-1270 du 22 décembre 1958 ;<br/>
<br/>
              Vu la loi n° 79-587 du 11 juillet 1979 ; <br/>
<br/>
              Vu le décret n° 93-21 du 7 janvier 1993 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Didier Ribes, Maître des Requêtes, <br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que le Conseil supérieur de la magistrature a, le 9 décembre 2010, donné un avis non conforme à la nomination de M.B..., vice-président au tribunal de grande instance de Saint-Pierre (La Réunion), en qualité de vice-président placé auprès du premier président de la cour d'appel de Saint-Denis ; que M. B...demande l'annulation pour excès de pouvoir de cet avis, dont il estime qu'il s'agit d'une décision ;<br/>
<br/>
              Sur la fin de non recevoir opposée par le ministre :<br/>
<br/>
              2. Considérant qu'aux termes de l'article 64 de la Constitution : " Le Président de la République est garant de l'indépendance de l'autorité judiciaire. Il est assisté par le Conseil supérieur de la magistrature. (...) " ; que le quatrième alinéa de l'article 65 de la Constitution dispose que " La formation du Conseil supérieur de la magistrature compétente à l'égard des magistrats du siège fait des propositions pour les nominations des magistrats du siège à la Cour de cassation, pour celles de premier président de cour d'appel et pour celles de président de tribunal de grande instance. Les autres magistrats du siège sont nommés sur son avis conforme " ; que l'article 28 de l'ordonnance du 22 décembre 1958 portant loi organique relative au statut de la magistrature dispose que " Les décrets de nomination aux fonctions de président d'un tribunal de grande instance ou d'un tribunal de première instance ou de conseiller référendaire à la Cour de cassation sont pris par le Président de la République sur proposition de la formation compétente du Conseil supérieur de la magistrature. / Les décrets portant promotion de grade ou nomination aux fonctions de magistrat autres que celles mentionnées à l'alinéa précédent sont pris par le Président de la République sur proposition du garde des sceaux, ministre de la justice, après avis conforme de la formation compétente du Conseil supérieur de la magistrature pour ce qui concerne les magistrats du siège (...) " ;<br/>
<br/>
              3. Considérant qu'il appartient au Conseil supérieur de la magistrature, dans l'exercice de sa mission constitutionnelle, de se prononcer, par un avis conforme, sur les nominations des magistrats du siège pour lesquelles il n'est pas chargé de formuler des propositions ; qu'en cas de refus du Conseil supérieur de la magistrature de donner son accord à une nomination, proposée par le ministre de la justice, aux fonctions de magistrat relevant du deuxième alinéa de l'article 28 de l'ordonnance du 22 décembre 1958, un tel avis non conforme fait obstacle à ce que le Président de la République prononce cette nomination et n'implique pas nécessairement qu'il prenne un décret pour en tirer les conséquences ; que, par suite, l'avis non conforme du Conseil supérieur de la magistrature sur la nomination d'un magistrat du siège constitue un acte faisant grief qui peut être déféré au juge de l'excès de pouvoir ; que, dès lors, la fin de non-recevoir tirée par le ministre de la justice de l'irrecevabilité de la contestation directe de cet avis doit être écartée ;<br/>
<br/>
              Sur les conclusions de M. B...tendant à ce que certaines pièces du dossier soient écartées des débats soumis au Conseil d'Etat :<br/>
<br/>
              4. Considérant qu'il appartient au juge de l'excès de pouvoir de former sa conviction sur les points en litige au vu des éléments versés au dossier par les parties ; que le cas échéant, il revient au juge, avant de se prononcer sur une requête assortie d'allégations sérieuses non démenties par les éléments produits par l'administration en défense, de mettre en oeuvre ses pouvoirs généraux d'instruction des requêtes et de prendre toute mesure propre à lui procurer, par les voies de droit, les éléments de nature à lui permettre de former sa conviction, en particulier en exigeant de l'administration compétente qu'elle lui fasse connaître, alors même qu'elle ne serait soumise par aucun texte à une obligation de motivation, les raisons de fait et de droit qui l'ont conduite à prendre la décision attaquée ; qu'il n'y a donc pas lieu, contrairement à ce que demande le requérant, d'écarter des débats les éléments versés au dossier, à la suite du supplément d'instruction réalisé par le Conseil d'Etat, par le président de la formation du Conseil supérieur de la magistrature compétente pour les magistrats du siège, qui éclairent les motifs pour lesquels la nomination de M. B...au poste qu'il sollicitait a donné lieu à un avis négatif ;<br/>
<br/>
              Sur les conclusions à fin d'annulation de la décision attaquée :<br/>
<br/>
              5. Considérant, en premier lieu, que l'avis défavorable donné par le Conseil supérieur de la magistrature à une proposition de nomination d'un magistrat du siège n'est pas au nombre des décisions individuelles refusant à l'intéressé un avantage auquel il a droit qui, en application de l'article 1er de la loi du 11 juillet 1979, doivent être motivées ; que par suite, le moyen tiré du défaut de motivation de la décision attaquée doit être écarté ;<br/>
<br/>
              6. Considérant, en deuxième lieu, qu'aux termes de l'article 29 de l'ordonnance du 22 décembre 1958 : " Dans toute la mesure compatible avec le fonctionnement du service et des particularités de l'organisation judiciaire, les nominations des magistrats tiennent compte de leur situation familiale. (...) " ; qu'il ne ressort pas des pièces du dossier, notamment des motifs de l'avis défavorable communiqués par le Conseil supérieur de la magistrature, que l'avis litigieux serait intervenu sans qu'ait été prise en compte la situation familiale de l'intéressé ;<br/>
<br/>
              7. Considérant qu'il ressort des pièces du dossier que, comme le soutient M. B..., son parcours professionnel et ses évaluations étaient de nature à le qualifier pour le poste concerné ; que, toutefois, le Conseil supérieur de la magistrature s'attache, au titre de sa mission générale d'avis sur les nominations des magistrats du siège, à promouvoir la mutation ou la promotion en métropole des magistrats en poste dans les départements et collectivités d'outre-mer afin d'assurer le bon fonctionnement des juridictions, tout en prenant en compte les impératifs liés à la situation personnelle du magistrat ou aux considérations de bonne administration de la justice ; que la nomination du requérant dans les fonctions souhaitées lui aurait permis, après deux années d'exercice des fonctions, par l'effet des dispositions de l'article 3-1 de l'ordonnance du 22 décembre 1958, d'être à nouveau nommé sur place, pour la troisième fois consécutive dans une juridiction outre-mer ; que la nomination dans les fonctions souhaitées aurait ainsi été de nature à compromettre durablement, en ce qui le concerne, l'objectif de mobilité géographique s'appliquant à l'ensemble des magistrats judiciaires et concourant à garantir leur indépendance ; qu'il ne ressort pas des pièces du dossier que la nomination de M. B...au poste souhaité était rendue impérative par les besoins du service ; que dès lors, au vu de l'ensemble de ces considérations, et alors même que son affectation actuelle conduit le requérant, compte tenu de son choix de résidence et de ses contraintes familiales, à des temps de trajet importants, l'avis défavorable porté par le Conseil supérieur de la magistrature n'est pas entaché d'une erreur manifeste d'appréciation ;<br/>
<br/>
              8. Considérant, en troisième lieu, qu'il ne ressort pas non plus des pièces du dossier que l'avis défavorable attaqué aurait été rendu en méconnaissance du principe d'égalité, alors même que plusieurs candidatures de M. B...à des postes du ressort de la cour d'appel de Saint-Denis auraient été successivement écartées, dans certains cas au profit de magistrats justifiant d'une ancienneté inférieure à la sienne ;<br/>
<br/>
              9. Considérant, en dernier lieu, que le détournement de pouvoir allégué n'est pas établi ;<br/>
<br/>
              10. Considérant qu'il résulte de tout ce qui précède que M. B...n'est pas fondé à demander l'annulation de la décision attaquée ;<br/>
<br/>
<br/>
<br/>
<br/>	                      D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : La requête de M. B...est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. A...B...et à la garde des sceaux, ministre de la justice.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">37-04-02-005 JURIDICTIONS ADMINISTRATIVES ET JUDICIAIRES. MAGISTRATS ET AUXILIAIRES DE LA JUSTICE. MAGISTRATS DE L'ORDRE JUDICIAIRE. NOMINATION. - AVIS NON CONFORME ÉMIS PAR LE CONSEIL SUPÉRIEUR DE LA MAGISTRATURE À LA NOMINATION D'UN MAGISTRAT DU SIÈGE PROPOSÉE PAR LE MINISTRE DE LA JUSTICE (ART. 28 DE L'ORDONNANCE DU 22 DÉCEMBRE 1958) - CARACTÈRE D'ACTE FAISANT GRIEF - EXISTENCE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-01-01-01-01 PROCÉDURE. INTRODUCTION DE L'INSTANCE. DÉCISIONS POUVANT OU NON FAIRE L'OBJET D'UN RECOURS. ACTES CONSTITUANT DES DÉCISIONS SUSCEPTIBLES DE RECOURS. AVIS ET PROPOSITIONS. - AVIS NON CONFORME ÉMIS PAR LE CONSEIL SUPÉRIEUR DE LA MAGISTRATURE À LA NOMINATION D'UN MAGISTRAT DU SIÈGE PROPOSÉE PAR LE MINISTRE DE LA JUSTICE (ART. 28 DE L'ORDONNANCE DU 22 DÉCEMBRE 1958) [RJ1].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-04-01 PROCÉDURE. INSTRUCTION. POUVOIRS GÉNÉRAUX D'INSTRUCTION DU JUGE. - EXCÈS DE POUVOIR - FORMATION DE LA CONVICTION DU JUGE - MISE EN &#140;UVRE DES POUVOIRS D'INSTRUCTION DU JUGE [RJ2] - POSSIBILITÉ D'EXIGER DE L'ADMINISTRATION QU'ELLE FASSE CONNAÎTRE LES MOTIFS DE SA DÉCISION - EXISTENCE, MÊME EN L'ABSENCE DE TEXTE PRÉVOYANT UNE OBLIGATION DE MOTIVATION DE LA DÉCISION [RJ3].
</SCT>
<ANA ID="9A"> 37-04-02-005 L'avis non conforme émis par le Conseil supérieur de la magistrature sur la nomination d'un magistrat du siège proposée par le ministre de la justice, qui fait obstacle à ce que le Président de la République prononce cette nomination et n'implique pas nécessairement qu'il prenne un décret pour en tirer les conséquences, constitue un acte faisant grief qui peut être déféré au juge de l'excès de pouvoir.</ANA>
<ANA ID="9B"> 54-01-01-01-01 L'avis non conforme émis par le Conseil supérieur de la magistrature sur la nomination d'un magistrat du siège proposée par le ministre de la justice, qui fait obstacle à ce que le Président de la République prononce cette nomination et n'implique pas nécessairement qu'il prenne un décret pour en tirer les conséquences, constitue un acte faisant grief qui peut être déféré au juge de l'excès de pouvoir.</ANA>
<ANA ID="9C"> 54-04-01 Il appartient au juge de l'excès de pouvoir de former sa conviction sur les points en litige au vu des éléments versés au dossier par les parties. Le cas échéant, il revient au juge, avant de se prononcer sur une requête assortie d'allégations sérieuses non démenties par les éléments produits par l'administration en défense, de mettre en oeuvre ses pouvoirs généraux d'instruction des requêtes et de prendre toute mesure propre à lui procurer, par les voies de droit, les éléments de nature à lui permettre de former sa conviction, en particulier en exigeant de l'administration compétente qu'elle lui fasse connaître, alors même qu'elle ne serait soumise par aucun texte à une obligation de motivation, les raisons de fait et de droit qui l'ont conduite à prendre la décision attaquée.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Ab. jur. CE, 22 janvier 2003, Pezzati, n°s 225069 230523, T. pp. 848-897. Rappr. Section, 30 décembre 2003, Mme Mocko, n°243943, p. 535.,,[RJ2] Cf. CE, 26 novembre 2012, Mme Cordière, n° 354108, à publier au Recueil.,,[RJ3] Cf. CE, Section, 26 janvier 1968, Société Maison Genestal, n° 69765, p. 62.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
