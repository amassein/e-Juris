<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025528980</ID>
<ANCIEN_ID>JG_L_2012_03_000000354279</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/52/89/CETATEXT000025528980.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème et 2ème sous-sections réunies, 12/03/2012, 354279</TITRE>
<DATE_DEC>2012-03-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>354279</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème et 2ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Christian Vigouroux</PRESIDENT>
<AVOCATS>SCP LYON-CAEN, THIRIEZ ; SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Nicolas Polge</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Boulouis</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:354279.20120312</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 23 novembre et 8 décembre 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour la COMMUNAUTE D'AGGLOMERATION DU PAYS DE MONTBELIARD (PAYS DE MONTBELIARD AGGLOMERATION), dont le siège est 8 avenue des Alliés à Montbéliard (25208), représentée par son président ; la communauté d'agglomération demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 1101505 du 8 novembre 2011 par laquelle le juge des référés du tribunal administratif de Besançon, statuant en application de l'article L. 551-5 du code de justice administrative, sur la demande de la société Ginger Groupe Ingénierie Europe, lui a enjoint de suspendre l'exécution de toute décision relative à la procédure engagée le 6 mai 2011 en vue de l'attribution d'un marché de maîtrise d'oeuvre pour la réalisation d'une ligne de transport en commun de Valentigney à Montbéliard et de faire procéder dans un délai de dix jours à compter de la notification de cette ordonnance, sous astreinte de cent euros par jour de retard, à un nouvel examen des candidatures reçues par le jury de concours, après avoir retiré des dossiers des candidats leur note de motivation, sauf à renoncer à la procédure ;<br/>
<br/>
              2°) statuant en référé, de rejeter la demande de la société Ginger Groupe Ingénierie Europe ;<br/>
<br/>
              3°) de mettre à la charge de la société Ginger Groupe Ingenierie Europe la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code des marchés publics ;<br/>
<br/>
              Vu l'arrêté du 28 août 2006 fixant la liste des renseignements et des documents pouvant être demandés aux candidats aux marchés passés par les pouvoirs adjudicateurs ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Nicolas Polge, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Lyon-Caen, Thiriez, avocat de la COMMUNAUTE D'AGGLOMERATION DU PAYS DE MONTBELIARD (PAYS DE MONTBELIARD AGGLOMERATION) et de la SCP Piwnica, Molinié, avocat de la société Ginger Groupe Ingenierie Europe, <br/>
<br/>
              - les conclusions de M. Nicolas Boulouis, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Lyon-Caen, Thiriez, avocat de la COMMUNAUTE D'AGGLOMERATION DU PAYS DE MONTBELIARD (PAYS DE MONTBELIARD AGGLOMERATION) et à la SCP Piwnica, Molinié, avocat de la société Ginger Groupe Ingenierie Europe ; <br/>
<br/>
<br/>
<br/>
              Considérant qu'aux termes de l'article L. 551-5 du code de justice administrative : " Le président du tribunal administratif, ou le magistrat qu'il délègue, peut être saisi en cas de manquement aux obligations de publicité et de mise en concurrence auxquelles est soumise la passation par les entités adjudicatrices de contrats administratifs ayant pour objet l'exécution de travaux, la livraison de fournitures ou la prestation de services, avec une contrepartie économique constituée par un prix ou un droit d'exploitation (...). / Le juge est saisi avant la conclusion du contrat " ; qu'aux termes de l'article L. 551-6 : " Le juge peut ordonner à l'auteur du manquement de se conformer à ses obligations en lui fixant un délai à cette fin. Il peut lui enjoindre de suspendre l'exécution de toute décision se rapportant à la passation du contrat. Il peut, en outre, prononcer une astreinte provisoire courant à l'expiration des délais impartis (...) " ; qu'aux termes de l'article L. 551-7 : " Le juge peut toutefois, en considération de l'ensemble des intérêts susceptibles d'être lésés et notamment de l'intérêt public, écarter les mesures énoncées au premier alinéa de l'article L. 551-6 lorsque leurs conséquences négatives pourraient l'emporter sur leurs avantages. " ;<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis au juge des référés du tribunal administratif de Besançon, comme d'ailleurs des visas de son ordonnance, que la COMMUNAUTE D'AGGLOMERATION DU PAYS DE MONTBELIARD a demandé à ce juge, en défense à la demande de la société Ginger Groupe Ingénierie Europe tendant à l'annulation de la procédure de passation engagée et à ce qu'il lui soit enjoint de la reprendre dans des conditions régulières, de faire application des dispositions de l'article L. 551-7 du code de justice administrative permettant au juge, bien qu'il ait relevé un manquement aux obligations de publicité et de mise en concurrence susceptible de léser le requérant, de ne pas prononcer de mesures à l'encontre de l'entité adjudicatrice ; que la COMMUNAUTE D'AGGLOMERATION DU PAYS DE MONTBELIARD soutenait à l'appui de cette demande que les conséquences négatives de telles mesures pouvaient l'emporter sur leurs avantages ; que le juge des référés a toutefois enjoint sous astreinte à la communauté d'agglomération de suspendre l'exécution de toute décision relative à la procédure et de faire procéder à un nouvel examen des candidatures reçues par le jury, sans répondre à ce moyen ; que son ordonnance est, ainsi, entachée d'une insuffisance de motivation ; que la COMMUNAUTE D'AGGLOMERATION DU PAYS DE MONTBELIARD est par suite fondée, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, à en demander pour ce motif l'annulation ;<br/>
<br/>
              Considérant que, dans les circonstances de l'espèce, il y a lieu, en application des dispositions de l'article L. 821-2 du code de justice administrative, de régler l'affaire au titre de la procédure de référé engagée par la société Ginger Groupe Ingénierie Europe ;<br/>
<br/>
              Considérant qu'il résulte de l'instruction qu'après avoir fait procéder à un nouvel examen des candidatures reçues par le jury, conformément à l'ordonnance du juge des référés du tribunal administratif de Besançon du 8 novembre 2011, l'entité adjudicatrice a conduit la procédure de passation du contrat à son terme et conclu le marché ; qu'il n'y a dès lors plus lieu de statuer sur la demande de la société Ginger Groupe Ingénierie Europe ; <br/>
<br/>
              Considérant que, dans les circonstances de l'espèce, il y a lieu de rejeter les conclusions de la COMMUNAUTE D'AGGLOMERATION DU PAYS DE MONTBELIARD et de la société Ginger Groupe Ingénierie Europe présentées en application des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : L'ordonnance du juge des référés du tribunal administratif de Besançon du 8 novembre 2011 est annulée.<br/>
<br/>
Article 2 : Il n'y a pas lieu de statuer sur la demande présentée par la société Ginger Groupe Ingénierie Europe devant le juge des référés du tribunal administratif de Besançon.<br/>
<br/>
Article 3 : Les conclusions de la COMMUNAUTE D'AGGLOMERATION DU PAYS DE MONTBELIARD et de la société Ginger Groupe Ingénierie Europe tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la COMMUNAUTE D'AGGLOMERATION DU PAYS DE MONTBELIARD (PAYS DE MONTBELIARD AGGLOMERATION) et à la société Ginger Groupe Ingénierie Europe.<br/>
.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-08-015-01 MARCHÉS ET CONTRATS ADMINISTRATIFS. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. PROCÉDURES D'URGENCE. - DEMANDE D'APPLICATION DES DISPOSITIONS DE L'ARTICLE L. 551-7 DU CJA PAR LE DÉFENDEUR - OBLIGATION POUR LE JUGE DE MOTIVER SON REFUS D'Y FAIRE DROIT AVANT DE FAIRE USAGE DE L'ARTICLE L. 551-6.
</SCT>
<ANA ID="9A"> 39-08-015-01 Lorsque, dans une instance de référé précontractuel, le défendeur demande au juge de faire application des dispositions de l'article L. 551-7 du code de justice administrative, qui lui permettent de ne pas sanctionner sur le fondement de l'article L. 551-6 un manquement aux obligations de publicité et de mise en concurrence, le juge est tenu, avant de prononcer une mesure sur le fondement de ce dernier article, de motiver son refus de faire jouer l'article L. 551-7.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
