<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030866716</ID>
<ANCIEN_ID>JG_L_2015_07_000000386068</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/86/67/CETATEXT000030866716.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème / 9ème SSR, 10/07/2015, 386068</TITRE>
<DATE_DEC>2015-07-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>386068</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème / 9ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>Mme Isabelle Lemesle</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:386068.20150710</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. E...B...a demandé au tribunal administratif de Caen d'annuler les opérations électorales qui se sont déroulées le 10 septembre 2014 en vue de la désignation des conseillers communautaires de la commune de Pont-Hébert au sein de la communauté d'agglomération " Saint-Lô Agglo ". <br/>
<br/>
              Par un jugement n° 1401758 du 13 novembre 2014, le tribunal administratif de Caen a annulé cette élection.<br/>
<br/>
              1° Sous le n° 386068, par un recours, enregistré le 28 novembre 2014 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'intérieur demande l'annulation de ce jugement.<br/>
<br/>
<br/>
<br/>
              2° Sous le n° 386403, par une requête enregistrée au secrétariat du contentieux du Conseil d'Etat le 12 décembre 2014, la commune de Pont-Hébert et son maire, M. C... A..., demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le même jugement ;<br/>
<br/>
              2°) de rejeter la protestation de M.B... ;<br/>
<br/>
              3°) de mettre la somme de 1 000 euros à la charge de M. B...au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu :<br/>
              - la Constitution, notamment son article 62 ;<br/>
              - le code électoral ;<br/>
              - le code général des collectivités territoriales ;<br/>
              - la décision du Conseil constitutionnel n° 2014-405 QPC du 20 juin 2014 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Isabelle Lemesle, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Foussard, Froger, avocat de M. C...A...et de la commune de Pont-Hébert ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que le recours du ministre de l'intérieur, d'une part, et la requête de la commune de Pont-Hébert et de M. C...A..., d'autre part, sont relatifs à la même élection ; qu'il y a lieu de les joindre pour statuer par une seule décision ;<br/>
<br/>
              Sur la recevabilité du recours du ministre et des conclusions de la commune :<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 5211-2 du code général des collectivités territoriales : " Les dispositions du chapitre II du titre II du livre Ier de la deuxième partie relatives aux maires et adjoints sont applicables au président et aux membres de l'organe délibérant des établissements publics de coopération intercommunale, en tant qu'elles ne sont pas contraires aux dispositions du présent titre " ; qu'aux termes de l'article L. 2122-13 du même code : " L'élection du maire et des adjoints peut être arguée de nullité dans les conditions, formes et délais prescrits pour les réclamations contre les élections du conseil municipal " ; qu'aux termes de l'article L. 248 du code électoral : " Tout électeur et tout éligible a le droit d'arguer de nullité les opérations électorales de la commune devant le tribunal administratif./ Le préfet, s'il estime que les conditions et les formes légalement prescrites n'ont pas été remplies, peut également déférer les opérations électorales au tribunal administratif " ; qu'en vertu de l'article L. 250 du même code, le recours au Conseil d'Etat contre la décision du tribunal administratif est ouvert soit au préfet, soit aux parties intéressées ;<br/>
<br/>
              3. Considérant qu'il résulte de l'ensemble de ces dispositions qu'en l'absence de dispositions contraires, les protestations dirigées contre les opérations électorales par lesquelles un conseil municipal désigne les délégués de la commune à l'assemblée d'un établissement public de coopération intercommunale doivent être formées dans les conditions, formes et délais prescrits pour les réclamations contre les élections du conseil municipal et qu'il en va de même pour l'appel d'un jugement statuant sur de telles protestations ; qu'il s'ensuit, en premier lieu, que le préfet a seul qualité pour agir au nom de l'Etat en appel devant le Conseil d'Etat, soit qu'il ait déféré les opérations électorales aux premiers juges, soit que ces derniers en aient rectifié ou annulé les résultats et, en second lieu, qu'une commune ne saurait avoir la qualité de partie devant le juge de l'élection saisi d'une contestation relative à l'élection de conseillers communautaires, ni pour faire appel d'un jugement annulant les opérations électorales par lesquelles un conseil municipal désigne ses délégués ; <br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède que le recours n° 386068 du ministre de l'intérieur et les conclusions de la requête n° 386403, en tant qu'elles sont présentées par la commune de Pont-Hébert, tendant à l'annulation du jugement du 13 novembre 2014 du tribunal administratif de Caen, ne sont pas recevables ; que, toutefois, cette requête est recevable en tant qu'elle émane de M.A..., dont l'élection en qualité de conseiller communautaire a été annulée ;<br/>
<br/>
              Sur le fond :<br/>
<br/>
              5. Considérant qu'aux termes des deux derniers alinéas de l'article 62 de la Constitution : " Une disposition déclarée inconstitutionnelle sur le fondement de l'article 61-1 est abrogée à compter de la publication de la décision du Conseil constitutionnel ou d'une date ultérieure fixée par cette décision. Le Conseil constitutionnel détermine les conditions et limites dans lesquelles les effets que la disposition a produits sont susceptibles d'être remis en cause. / Les décisions du Conseil constitutionnel ne sont susceptibles d'aucun recours. Elles s'imposent aux pouvoirs publics et à toutes les autorités administratives et juridictionnelles. " ;<br/>
<br/>
              6. Considérant que, par une décision n° 2014-405 QPC du 20 juin 2014, le Conseil Constitutionnel a déclaré contraire à la Constitution le deuxième alinéa du I de l'article L. 5211-6-1 du code général des collectivités territoriales qui prévoyait, dans sa version issue de la loi n° 2013-403 du 17 mai 2013, que, pour les communautés d'agglomération, le nombre et la répartition des sièges pouvaient être fixés par accord des deux tiers des conseils municipaux représentant la moitié de la population ou de la moitié des conseils municipaux représentant les deux tiers de la population, " en tenant compte de la population de chaque commune " et en assurant à chacune d'entre elles au moins un siège, sans qu'aucune ne puisse disposer de plus de la moitié des sièges ; que, s'agissant des effets dans le temps de cette déclaration d'inconstitutionnalité, il a notamment décidé que la désignation de tous les délégués communautaires par stricte application des règles de proportionnalité entre le nombre de représentants et la population de chaque commune serait applicable pour les communautés d'agglomération au sein desquelles le conseil municipal d'au moins une des communes membres serait, postérieurement à la date de la publication de sa décision, partiellement ou intégralement renouvelé ;<br/>
<br/>
              7. Considérant que des élections municipales partielles se sont déroulées le 5 octobre 2014 dans la commune du Mesnil Opac, également membre de " Saint-Lô Agglo ", à la suite de démissions intervenues en avril et juin 2014 ; qu'en application de la décision du Conseil Constitutionnel mentionnée au point 6, le préfet de la Manche a pris le 29 août 2014 un nouvel arrêté, fondé sur les dispositions restant en vigueur de l'article L. 5211-6-1 du code général des collectivités territoriales, ramenant à 108 le nombre total de conseillers communautaires et à deux le nombre de représentants de la commune de Pont-Hébert ; que la délibération du 10 septembre 2014 du conseil municipal de Pont-Hébert portant désignation de ses deux membres au conseil de la communauté d'agglomération a été prise sur le fondement de l'article L. 5211-6-2 du code général des collectivités territoriales, dans sa rédaction issue de la loi n° 2013-403 du 17 mai 2013 ; que, s'il est vrai que ces dernières dispositions ne sont applicables, entre deux renouvellements généraux des conseils municipaux, qu'à des cas dans lesquels la désignation contestée n'entrait pas de façon expresse, leur utilisation était, en l'absence de dispositions particulières applicables à une telle situation, les seules à même de respecter l'autorité de la chose jugée par le Conseil constitutionnel ; <br/>
<br/>
              8. Considérant qu'il s'ensuit que c'est à tort que, par le jugement attaqué, le tribunal administratif de Caen a annulé l'élection des conseillers communautaires de la commune de Pont-Hébert au sein du conseil communautaire de " Saint-Lô Agglo " du 10 septembre 2014 au motif qu'il n'appartenait qu'au seul législateur de définir les modalités selon lesquelles de telles élections devaient se dérouler, en particulier s'agissant du mode de scrutin ;<br/>
<br/>
              9. Considérant toutefois qu'il appartient au Conseil d'Etat, saisi par l'effet dévolutif de l'appel, d'examiner les autres griefs soulevés par M. B...dans sa protestation devant le tribunal ;<br/>
<br/>
              10. Considérant qu'il résulte de l'instruction que la délibération du 10 septembre 2014 du conseil municipal de Pont-Hébert " portant élection des conseillers communautaires consécutive à la recomposition du conseil communautaire à la suite de la décision du Conseil Constitutionnel n° 2014-405 QPC " mentionne l'élection de " deux titulaires " au " scrutin de liste à un tour à bulletin secret " ; que toutefois, M. B... soutient, en produisant une attestation d'un conseiller municipal, ce qui est contesté par M.A..., qui produit trois attestations en défense, que le secret du scrutin aurait été méconnu, le vote s'étant déroulé sans urne, sans isoloir, sans enveloppe, un bulletin blanc étant posé devant chacun des 19 conseillers municipaux et le maire ayant remis à chacun d'entre eux un bulletin comportant les noms de M. A...et de MmeD... ;<br/>
<br/>
              11. Considérant qu'en vertu des dispositions combinées de l'article L. 5211-2 du code général des collectivités territoriales, citées au point 2, et des articles L. 2122-7 et L. 2122-7-1, applicables à l'élection du maire et des adjoints, l'élection des membres du conseil municipal au conseil d'une communauté d'agglomération sur le fondement de l'article L. 5211-6-2 du code général des collectivités territoriales se fait au scrutin secret ;<br/>
<br/>
              12. Considérant que, si aucune disposition législative ou réglementaire ni aucun principe n'impose la présence d'un isoloir, d'une urne, ni la mise sous enveloppe des bulletins pour les désignations opérées au sein du conseil municipal, et si l'utilisation de bulletins portant certains noms inscrits à l'avance ne constitue pas, en elle-même, une atteinte au secret du vote, celui-ci n'a en l'espèce pas été assuré dès lors qu'il résulte de l'instruction que les conseillers municipaux qui souhaitaient s'écarter de ces bulletins ont dû inscrire leur choix de manière manuscrite au vu des autres membres du conseil municipal et du public éventuel ; que, dans les circonstances de l'espèce, cette irrégularité a été de nature à altérer la sincérité du scrutin ; <br/>
<br/>
              13. Considérant qu'il résulte de ce qui précède que M. A...n'est pas fondé à se plaindre de ce que, par le jugement attaqué, le tribunal administratif a annulé l'élection des délégués de la commune de Pont-Hébert à la communauté d'agglomération " Saint-Lô Agglo " à laquelle il a été procédé le 10 septembre 2014 ;<br/>
<br/>
              14. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de M. B...qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le recours n° 386068 et la requête n° 386403 sont rejetés.<br/>
Article 2 : Les conclusions de M. B...et de M. A...tendant à l'application de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à la commune de Pont-Hébert, à M. C... A...et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">28-08-01-01 ÉLECTIONS ET RÉFÉRENDUM. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. INTRODUCTION DE L'INSTANCE. QUALITÉ DU REQUÉRANT. - ELECTIONS MUNICIPALES ET COMMUNAUTAIRES - 1) QUALITÉ POUR AGIR AU NOM DE L'ETAT - PRÉFET - EXISTENCE - AUTRE AUTORITÉ DE L'ETAT - ABSENCE - 2) QUALITÉ POUR AGIR DE LA COMMUNE - ABSENCE.
</SCT>
<ANA ID="9A"> 28-08-01-01 Contestation des élections municipales et communautaires.,,,1) Le préfet a seul qualité pour agir au nom de l'Etat en appel devant le Conseil d'Etat, soit qu'il ait déféré les opérations électorales aux premiers juges, soit que ces derniers en aient rectifié ou annulé les résultats... ,,2) Une commune ne saurait avoir la qualité de partie devant le juge de l'élection saisi d'une telle contestation, ni pour faire appel d'un jugement annulant les opérations électorales.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
