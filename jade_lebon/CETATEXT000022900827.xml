<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000022900827</ID>
<ANCIEN_ID>JG_L_2010_10_000000340849</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/22/90/08/CETATEXT000022900827.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème et 7ème sous-sections réunies, 08/10/2010, 340849, Publié au recueil Lebon</TITRE>
<DATE_DEC>2010-10-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>340849</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème et 7ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Martin</PRESIDENT>
<AVOCATS/>
<RAPPORTEUR>Mme Catherine  Chadelat</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Lenica Frédéric</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2010:340849.20101008</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le mémoire, enregistré le 9 juillet 2010 au secrétariat du contentieux du Conseil d'Etat, présenté par le GROUPEMENT DE FAIT BRIGADE SUD DE NICE, domicilié ... et M. Gilles A, demeurant à la même adresse, en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958 ; le GROUPEMENT DE FAIT BRIGADE SUD DE NICE et M. A demandent au Conseil d'Etat, à l'appui de leur requête tendant à l'annulation du décret du 28 avril 2010 portant dissolution du GROUPEMENT DE FAIT BRIGADE SUD DE NICE, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des articles 4 et 6 de la loi n° 2006-784 du 5 juillet 2006 relative à la prévention des violences lors de manifestations sportives insérant au code du sport les articles L. 332-18 à L. 332-21 ; <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution, notamment son article 61-1 ;<br/>
<br/>
              Vu l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
<br/>
              Vu le code du sport, notamment son article L. 332-18 ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Catherine Chadelat, Conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Frédéric Lenica, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant qu'aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...) " ; qu'il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              Considérant qu'aux termes de l'article L. 332-18 du code du sport, issu de l'article 4 de la loi du 5 juillet 2006 et modifié par la loi du 2 mars 2010, applicable à la date du décret attaqué : " Peut être dissous ou suspendu d'activité pendant douze mois au plus par décret, après avis de la commission nationale consultative de prévention des violences lors des manifestations sportives, toute association ou groupement de fait ayant pour objet le soutien à une association sportive mentionnée à l'article L. 122-1, dont des membres ont commis en réunion, en relation ou à l'occasion d'une manifestation sportive, des actes répétés ou un acte d'une particulière gravité et qui sont constitutifs de dégradations de biens, de violence sur des personnes ou d'incitation à la haine ou à la discrimination contre des personnes à raison de leur origine, de leur orientation sexuelle, de leur sexe ou de leur appartenance, vraie ou supposée, à une ethnie, une nation, une race ou une religion déterminée. / Les représentants des associations ou groupements de fait (...) peuvent présenter leurs observations à la commission (...) " ; que les articles L. 332-19, L. 332-20 et L. 332-21 du même code déterminent les sanctions pénales applicables aux personnes participant au maintien ou à la reconstitution d'une association ou d'un groupement dissous en application de l'article L. 332-18 ; <br/>
<br/>
              Considérant, d'une part, que les dispositions de l'article L. 332-18 du code du sport, qui constituent le fondement du décret attaqué par lequel le Premier ministre a prononcé la dissolution du GROUPEMENT DE FAIT BRIGADE SUD DE NICE, sont applicables au litige dont est saisi le Conseil d'Etat au sens et pour l'application de l'article 23-5 de l'ordonnance du 7 novembre 1958 et n'ont pas déjà été déclarées conformes à la Constitution par une décision du Conseil constitutionnel ; qu'en revanche, les dispositions des articles L. 332-19, L. 332-20 et L. 332-21 du même code, dont le décret attaqué ne fait pas application, ne sont pas applicables à ce litige ; <br/>
<br/>
              Considérant, d'autre part, que l'article L. 332-18 du code du sport permet au Premier ministre, après avis de la commission nationale consultative de prévention des violences lors des manifestations sportives, de prononcer la dissolution ou de suspendre l'activité, pendant douze mois au plus, d'une association ou d'un groupement de fait dont l'objet est de soutenir une association sportive et dont des membres ont commis en réunion, en relation ou à l'occasion d'une manifestation sportive, des actes graves ou répétés de dégradations de biens, de violence sur des personnes ou d'incitation à la haine ou à la discrimination ; qu'eu égard aux motifs susceptibles de conduire, sous le contrôle du juge de l'excès de pouvoir, au prononcé de la dissolution ou de la suspension d'activité de ces associations ou groupements de fait ainsi qu'aux conditions de mise en oeuvre de ces mesures, les dispositions de l'article L. 332-18, qui permettent le prononcé de mesures qui présentent le caractère de mesure de police administrative, répondent à la nécessité de sauvegarder l'ordre public, compte tenu de la gravité des troubles qui lui sont portés par les membres de certains groupements et associations de soutien des associations sportives, et ne portent pas d'atteinte excessive au principe de la liberté d'association qui est au nombre des principes fondamentaux reconnus par les lois de la République ; que ces dispositions n'emportent, par ailleurs, aucune atteinte à la liberté individuelle ou à la séparation des pouvoirs ; que le GROUPEMENT DE FAIT BRIGADE SUD DE NICE et M. A ne sont, par suite, pas fondés à soutenir que l'article L. 332-18 du code du sport porterait atteinte aux garanties constitutionnelles de la liberté d'association, de la liberté individuelle ou de la séparation des pouvoirs, ou méconnaîtrait les principes de légalité des délits et des peines ou de la personnalité des peines ; qu'ainsi, la question de constitutionnalité soulevée, qui n'est pas nouvelle, ne présente pas un caractère sérieux ; <br/>
<br/>
              Considérant qu'il résulte de ce qui précède, sans qu'il soit besoin de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée, que le moyen tiré de ce que les articles L. 332-18 et suivants du code du sport portent atteinte aux droits et libertés garantis par la Constitution doit être écarté ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par le GROUPEMENT DE FAIT BRIGADE SUD DE NICE et M. A.<br/>
<br/>
Article 2 : La présente décision sera notifiée au GROUPEMENT DE FAIT BRIGADE SUD DE NICE, à M. Gilles A, au Premier ministre, au ministre de l'intérieur, de l'outre-mer et des collectivités territoriales et à la ministre de la santé et des sports. Copie en sera adressée au Conseil constitutionnel.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-10-05-04-02 PROCÉDURE. - DISPOSITION DU CODE DU SPORT AUTORISANT LA DISSOLUTION OU LA SUSPENSION DE L'ACTIVITÉ DE GROUPES DE SUPPORTERS DONT DES MEMBRES ONT COMMIS DES ACTES RÉPRÉHENSIBLES (ART. L. 332-18 DU CODE DU SPORT) - LIBERTÉ D'ASSOCIATION.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">63-05 SPECTACLES, SPORTS ET JEUX. SPORTS. - GROUPES DE SUPPORTERS - DISSOLUTION OU SUSPENSION DE L'ACTIVITÉ DE GROUPES DE SUPPORTERS DONT DES MEMBRES ONT COMMIS DES ACTES RÉPRÉHENSIBLES (ART. L. 332-18 DU CODE DU SPORT) - QUESTION PRIORITAIRE DE CONSTITUTIONNALITÉ - LIBERTÉ D'ASSOCIATION - QUESTION JUGÉE NON SÉRIEUSE - REFUS DE RENVOI AU CONSEIL CONSTITUTIONNEL.
</SCT>
<ANA ID="9A"> 54-10-05-04-02 L'article L. 332-18 du code du sport - issu de l'article 4 de la loi n° 2006-784 du 5 juillet 2006 et modifié par la loi n° 2010-201 du 2 mars 2010 - permet au Premier ministre la dissolution ou la suspension de l'activité d'une association ou d'un groupement de fait dont l'objet est de soutenir une association sportive et dont des membres ont commis en réunion, en relation ou à l'occasion d'une manifestation sportive, des actes graves ou répétés de dégradations de biens, de violence sur des personnes ou d'incitation à la haine ou à la discrimination. Ces dispositions ne portent pas d'atteinte excessive au principe de la liberté d'association, principe fondamental reconnu par les lois de la République, eu égard aux motifs susceptibles de fonder cette mesure de police administrative ainsi qu'aux conditions de sa mise en oeuvre, et alors qu'elle répond à la nécessité de sauvegarder l'ordre public, compte tenu de la gravité des troubles qui lui sont portés par les membres de certains groupements et associations de soutien des associations sportives.</ANA>
<ANA ID="9B"> 63-05 L'article L. 332-18 du code du sport - issu de l'article 4 de la loi n° 2006-784 du 5 juillet 2006 et modifié par la loi n° 2010-201 du 2 mars 2010 - permet au Premier ministre la dissolution ou la suspension de l'activité d'une association ou d'un groupement de fait dont l'objet est de soutenir une association sportive et dont des membres ont commis en réunion, en relation ou à l'occasion d'une manifestation sportive, des actes graves ou répétés de dégradations de biens, de violence sur des personnes ou d'incitation à la haine ou à la discrimination. Ces dispositions ne portent pas d'atteinte excessive au principe de la liberté d'association, principe fondamental reconnu par les lois de la République, eu égard aux motifs susceptibles de fonder cette mesure de police administrative ainsi qu'aux conditions de sa mise en oeuvre, et alors qu'elle répond à la nécessité de sauvegarder l'ordre public, compte tenu de la gravité des troubles qui lui sont portés par les membres de certains groupements et associations de soutien des associations sportives.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
