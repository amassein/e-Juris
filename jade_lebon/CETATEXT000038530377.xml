<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038530377</ID>
<ANCIEN_ID>JG_L_2019_05_000000417406</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/53/03/CETATEXT000038530377.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 4ème chambres réunies, 29/05/2019, 417406</TITRE>
<DATE_DEC>2019-05-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>417406</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 4ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP POTIER DE LA VARDE, BUK LAMENT, ROBILLOT ; SCP SEVAUX, MATHONNET</AVOCATS>
<RAPPORTEUR>M. Jean-Luc Nevache</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Charles Touboul</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:417406.20190529</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Strasbourg, d'une part, d'annuler la décision du 17 janvier 2014 par laquelle le président du conseil général du Bas-Rhin a rejeté sa demande de prise en charge en tant que jeune majeur au titre de l'aide sociale à l'enfance ainsi que la décision du 20 mars 2014 rejetant son recours gracieux et la décision du 30 avril 2014 rejetant sa demande de réexamen et, d'autre part, d'enjoindre au président du conseil général du Bas-Rhin de procéder au réexamen de sa demande dans un délai de 8 jours à compter de la notification du jugement, sous astreinte de 150 euros par jour de retard. <br/>
<br/>
              Par un jugement n° 1402708 du 7 décembre 2016, le tribunal administratif de Strasbourg a annulé les décisions des 17 janvier et 20 mars 2014, enjoint au président du conseil départemental du Bas-Rhin de réexaminer la demande de M. A...dans le délai d'un mois à compter de la notification du jugement et rejeté le surplus des conclusions de la demande de M. A....<br/>
<br/>
              Par une ordonnance n° 17NC00276 du 16 janvier 2018, enregistrée le 19 janvier suivant au secrétariat du contentieux du Conseil d'Etat, le président de la cour administrative d'appel de Nancy a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, le pourvoi, enregistré le 7 février 2017 au greffe de cette même cour, présenté par le département du Bas-Rhin. <br/>
<br/>
              Par ce pourvoi, deux nouveaux mémoires et deux mémoires en réplique, enregistrés les 2 mars et 4 juin 2018 et les 24 janvier et 6 mai 2019, le département du Bas-Rhin demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande présentée par M. A...devant le tribunal administratif de Strasbourg ;<br/>
<br/>
              3°) de mettre à la charge de M. A...la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'action sociale et des familles ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Luc Nevache, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Charles Touboul, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Potier de la Varde, Buk Lament, Robillot, avocat du département du Bas-Rhin et à la SCP Sevaux, Mathonnet, avocat de M. A...;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 10 mai 2019, présentée par le département du Bas-Rhin ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M.A..., ressortissant pakistanais, est entré sur le territoire français le 28 mai 2013 alors qu'il était mineur. Sur décision judicaire, il a été pris en charge par le service de l'aide sociale à l'enfance du département du Bas-Rhin jusqu'au 5 février 2014, date de sa majorité. A l'approche de celle-ci, il a demandé à bénéficier d'une poursuite de prise en charge par le service de l'aide sociale à l'enfance du département dans le cadre du dispositif " contrat jeune majeur " mis en place en application des dispositions du sixième alinéa de l'article L. 222-5 du code de l'action sociale et des familles. Par une décision du 17 janvier 2014, suivie d'un rejet du recours gracieux de l'intéressé le 20 mars 2014 et d'un refus de reconsidérer cette position le 30 avril 2014, le président du conseil général du Bas-Rhin a rejeté la demande de M.A.... Sur recours de l'intéressé, le tribunal administratif de Strasbourg a, par un jugement du 7 décembre 2016, annulé les deux premières de ces décisions, enjoint au président du conseil départemental du Bas-Rhin de réexaminer la demande de M.A..., mis les frais de l'instance à la charge du département du Bas-Rhin et rejeté le surplus des conclusions de M.A.... Le département du Bas-Rhin se pourvoit en cassation contre ce jugement. Eu égard aux moyens qu'il invoque, il doit être regardé comme demandant l'annulation de ce jugement en tant qu'il annule les décisions des 17 janvier et 20 mars 2014 et qu'il enjoint au président du conseil départemental de réexaminer la demande de M.A....<br/>
<br/>
              Sur le pourvoi du département du Bas-Rhin :<br/>
<br/>
              2. D'une part, aux termes de l'article L. 111-2 du code de l'action sociale et des familles : " Les personnes de nationalité étrangère bénéficient dans les conditions propres à chacune de ces prestations : / 1° Des prestations d'aide sociale à l'enfance (...) ". L'article L. 221-1 du même code prévoit que : " Le service de l'aide sociale à l'enfance est un service non personnalisé du département chargé des missions suivantes : / 1° Apporter un soutien matériel, éducatif et psychologique tant aux mineurs et à leur famille ou à tout détenteur de l'autorité parentale, confrontés à des difficultés risquant de mettre en danger la santé, la sécurité, la moralité de ces mineurs ou de compromettre gravement leur éducation ou leur développement physique, affectif, intellectuel et social, qu'aux mineurs émancipés et majeurs de moins de vingt et un ans confrontés à des difficultés familiales, sociales et éducatives susceptibles de compromettre gravement leur équilibre (...) ". L'article L. 222-5 du même code, dans sa rédaction applicable à la date des décisions litigieuses, détermine les personnes susceptibles, sur décision du président du conseil général, d'être prises en charge par le service de l'aide sociale à l'enfance, parmi lesquelles, au titre du 1° de cet article, les mineurs qui ne peuvent demeurer provisoirement dans leur milieu de vie habituel et dont la situation requiert un accueil à temps complet ou partiel et, au titre de son 3°, les mineurs confiés au service par le juge des enfants parce que leur protection l'exige. Aux termes du sixième alinéa de cet article : " Peuvent être également pris en charge à titre temporaire par le service chargé de l'aide sociale à l'enfance les mineurs émancipés et les majeurs âgés de moins de vingt et un ans qui éprouvent des difficultés d'insertion sociale faute de ressources ou d'un soutien familial suffisants ". Enfin, aux termes du dernier alinéa de l'article R. 221-2 du même code, dans sa rédaction applicable à la date de la décision attaquée : " S'agissant de mineurs émancipés ou de majeurs âgés de moins de vingt et un ans, le président du conseil général ne peut agir que sur demande des intéressés et lorsque ces derniers éprouvent des difficultés d'insertion sociale faute de ressources ou d'un soutien familial suffisants ".<br/>
<br/>
              3. Il résulte de ces dispositions que le président du conseil général dispose d'un large pouvoir d'appréciation pour accorder ou maintenir, en fonction de critères qu'il lui appartient de déterminer, la prise en charge par le service de l'aide sociale à l'enfance d'un jeune majeur de moins de vingt et un ans éprouvant des difficultés d'insertion sociale faute de ressources ou d'un soutien familial suffisants. <br/>
<br/>
              4. D'autre part, l'article L. 111-1 du code de l'action sociale et des familles dispose que, sous réserve des dispositions particulières applicables à certaines personnes, notamment de nationalité étrangère, " toute personne résidant en France bénéficie, si elle remplit les conditions légales d'attribution, des formes de l'aide sociale telles qu'elles sont définies par le présent code ".  Aux termes de l'article L. 111-4 de ce code : " L'admission à une prestation d'aide sociale est prononcée au vu des conditions d'attribution telles qu'elles résultent des dispositions législatives ou réglementaires et, pour les prestations légales relevant de la compétence du département ou pour les prestations que le département crée de sa propre initiative, au vu des conditions d'attribution telles qu'elles résultent des dispositions du règlement départemental d'aide sociale mentionné à l'article L. 121-3 ". L'article L. 121-3 du même code dispose, dans sa rédaction en vigueur à la date des décisions litigieuses, que : " Dans les conditions définies par la législation et la réglementation sociales, le conseil général adopte un règlement départemental d'aide sociale définissant les règles selon lesquelles sont accordées les prestations d'aide sociale relevant du département " et l'article L. 121-4  de ce code, dans sa rédaction applicable à la même date, précise que : " Le conseil général peut décider de conditions et de montants plus favorables que ceux prévus par les lois et règlements applicables aux prestations mentionnées à l'article L. 121-1 ", c'est-à-dire les prestations légales d'aide sociale à la charge du département. Enfin, l'article L. 223-1 du même code précise, s'agissant de l'aide sociale à l'enfance, que : " L'attribution d'une ou plusieurs prestations prévues au présent titre est précédée d'une évaluation de la situation prenant en compte l'état du mineur, la situation de la famille et les aides auxquelles elle peut faire appel dans son environnement ".<br/>
<br/>
              5. Il résulte de ces dispositions que le département a l'obligation de verser celles des prestations d'aide sociale que la loi met à sa charge à toute personne en remplissant les conditions légales. Lorsque les conditions d'attribution ou les montants des prestations sont déterminées par les lois et décrets qui les régissent, le règlement départemental d'aide sociale ne peut édicter que des dispositions plus favorables. En l'absence de conditions ou montants précisément fixés par les lois et décrets, si le règlement départemental d'aide sociale peut préciser les critères au vu desquels il doit être procédé à l'évaluation de la situation des demandeurs, il ne peut fixer de condition nouvelle conduisant à écarter par principe du bénéfice des prestations des personnes qui entrent dans le champ des dispositions législatives applicables. Enfin, pour les prestations d'aide sociale qu'il crée de sa propre initiative, le département définit, par le règlement départemental d'aide sociale, les règles selon lesquelles ces prestations sont accordées. <br/>
<br/>
              6. Si, compte tenu de l'objet de la mesure considérée, l'existence et la durée de sa prise en charge antérieure par le service de l'aide sociale à l'enfance sont au nombre des critères sur lesquels un département peut légalement se fonder pour accorder ou maintenir la prise en charge par le service de l'aide sociale à l'enfance d'un jeune majeur de moins de vingt et un ans éprouvant des difficultés d'insertion sociale faute de ressources ou d'un soutien familial suffisants, le président du conseil général ne pouvait légalement refuser cette prise en charge à M. A...au seul motif, et sans procéder à l'évaluation de sa situation, qu'il ne remplissait pas la condition fixée par le règlement d'aide sociale du Bas-Rhin imposant d'" avoir bénéficié d'une prise en charge antérieure par le service de l'aide sociale à l'enfance au cours de sa minorité pendant un an au moins ". Par suite, le tribunal administratif de Strasbourg, qui ne s'est pas mépris sur la portée de la décision du 17 janvier 2014 en relevant que le président du conseil général n'avait pas procédé à l'appréciation particulière de la situation de M. A...mais s'était fondé sur la seule circonstance qu'il n'avait pas été pris en charge pendant au moins un an avant sa majorité, n'a pas commis d'erreur de droit en jugeant cette décision illégale. <br/>
<br/>
              7. Il ressort, en revanche, des pièces du dossier soumis aux juges du fond que le recours gracieux présenté le 31 janvier 2014 par M. A...contre la décision du 17 janvier 2014 a fait l'objet d'un examen, le 18 mars 2014, par la commission départementale de recours et que la décision de rejet du 20 mars 2014 repose sur le double motif que, d'une part, il ne remplissait pas la condition de durée minimale d'une année de prise en charge avant sa majorité par l'aide sociale à l'enfance et, d'autre part, il ne justifiait pas d'un engagement effectif dans une formation et sa situation administrative ne garantissait pas qu'un tel engagement puisse aboutir dans un délai de six mois. Par suite, le département du Bas-Rhin est fondé à soutenir que le tribunal a commis une erreur de droit et s'est mépris sur la portée de la décision du 20 mars 2014 en l'annulant par voie de conséquence de l'annulation de la décision du 17 janvier 2014, sans rechercher s'il aurait pris la même décision s'il s'était fondé sur le seul motif de l'absence d'engagement effectif de M. A...dans une formation.<br/>
<br/>
              8. Il résulte de ce qui précède que le département du Bas-Rhin n'est fondé à demander l'annulation du jugement attaqué qu'en tant seulement qu'il a annulé la décision du 20 mars 2014 du président du conseil général et lui a enjoint de réexaminer la demande de M. A... dans le délai d'un mois à compter de la notification du jugement.<br/>
<br/>
              9. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond dans la mesure de l'annulation prononcée, en application des dispositions de l'article L. 821-2 du code de justice administrative.  <br/>
<br/>
              Sur la décision du président du conseil général du Bas-Rhin du 20 mars 2014 :<br/>
<br/>
              10. Ainsi qu'il a été dit précédemment, la décision par laquelle le président du conseil général du Bas-Rhin a, le 20 mars 2014, rejeté le recours gracieux de M. A...contre le refus de le prendre en charge au titre de l'aide sociale à l'enfance en qualité de jeune majeur se fonde sur deux motifs. Le premier, tiré de ce qu'il ne satisfaisait pas à la condition d'une durée de prise en charge de douze mois avant sa majorité, doit, conformément à ce qui a été dit au point 6, être tenu pour illégal.<br/>
<br/>
              11. Il résulte, par ailleurs, de l'instruction que M. A...a été admis à compter du 1er avril 2014 en formation de carreleur-mosaïste dans le cadre d'un certificat d'aptitude professionnelle à modalités pédagogiques adaptées dans le lycée où il avait précédemment été scolarisé en module d'apprentissage en langue française, après une période d'attente faute de places disponibles. Le président du conseil général du Bas-Rhin, saisi d'une demande de réexamen de sa situation par l'intéressé, a néanmoins, le 30 avril 2014, refusé à nouveau sa prise en charge au titre de l'aide sociale à l'enfance en se fondant sur le seul motif qu'il ne satisfaisait pas à la condition de prise en charge par l'aide sociale à l'enfance pendant une année au moins avant sa majorité. Dans ces conditions, il ne résulte pas de l'instruction que le président du conseil général du Bas-Rhin aurait pris, le 20 mars 2014, la même décision en se fondant uniquement sur l'autre motif de refus, tiré de l'absence d'engagement effectif de l'intéressé dans une formation. <br/>
<br/>
              12. Il résulte de tout ce qui précède que M. A...est fondé à demander l'annulation de la décision du 20 mars 2014 par laquelle le président du conseil général du Bas-Rhin a rejeté son recours gracieux contre le refus de sa prise en charge par l'aide sociale à l'enfance en qualité de jeune majeur. <br/>
<br/>
              Sur les conclusions à fin d'injonction :<br/>
<br/>
              13. Il résulte de l'instruction que M.A..., qui est né le 5 février 1996 et a désormais plus de vingt et un ans, ne satisfait plus, à la date de la présente décision, à la condition d'âge posée à l'article L. 222-5 du code de l'action sociale et des familles. Il n'est ainsi plus susceptible de bénéficier d'une prise en charge par un service de l'aide sociale à l'enfance. Ses conclusions tendant à ce qu'il soit enjoint au président du conseil départemental du Bas-Rhin de réexaminer sa situation ne peuvent, par suite, qu'être rejetées.<br/>
<br/>
              Sur les frais liés au litige :<br/>
<br/>
              14. Les dispositions de l'article L. 761-1 du code de justice administrative et de l'article 37 de la loi du 10 juillet 1991 font obstacle à ce qu'une somme soit mise à la charge de M.A..., qui n'est pas la partie perdante, ou de l'Etat, qui n'est pas partie au présent litige.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Sont annulés l'article 1er du jugement du tribunal administratif de Strasbourg du 7 décembre 2016 en tant qu'il annule la décision du président du conseil général du Bas-Rhin du 20 mars 2014 et l'article 2 du même jugement.<br/>
Article 2 : La décision du président du conseil général du Bas-Rhin du 20 mars 2014 est annulée.<br/>
Article 3 : Les conclusions à fin d'injonction présentées par M. A...devant le tribunal administratif de Strasbourg sont rejetées.<br/>
Article 4 : Les conclusions de la SCP Sevaux, Mathonnet présentées au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 sont rejetées.<br/>
Article 5 : Le surplus des conclusions du pourvoi du département du Bas-Rhin est rejeté.<br/>
Article 6 : La présente décision sera notifiée au département du Bas-Rhin et à M. B...A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-02-02-01-045 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - COMPÉTENCE. RÉPARTITION DES COMPÉTENCES ENTRE AUTORITÉS DISPOSANT DU POUVOIR RÉGLEMENTAIRE. AUTORITÉS DISPOSANT DU POUVOIR RÉGLEMENTAIRE. ORGANES DÉLIBÉRANTS DES COLLECTIVITÉS TERRITORIALES. - CONSEIL DÉPARTEMENTAL - COMPÉTENCE DU RÈGLEMENT DÉPARTEMENTAL D'AIDE SOCIALE POUR PRÉCISER LES CONDITIONS D'ATTRIBUTION ET LE MONTANT DES PRESTATIONS VERSÉES PAR LE DÉPARTEMENT [RJ1] - CONDITIONS.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">04-01-01 AIDE SOCIALE. ORGANISATION DE L'AIDE SOCIALE. COMPÉTENCES DU DÉPARTEMENT. - COMPÉTENCE DU RÈGLEMENT DÉPARTEMENTAL D'AIDE SOCIALE POUR PRÉCISER LES CONDITIONS D'ATTRIBUTION ET LE MONTANT DES PRESTATIONS VERSÉES PAR LE DÉPARTEMENT [RJ1] - CONDITIONS.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">04-02-02 AIDE SOCIALE. DIFFÉRENTES FORMES D'AIDE SOCIALE. AIDE SOCIALE À L'ENFANCE. - PRISE EN CHARGE DES JEUNES MAJEURS DE MOINS DE VINGT-ET-UN ANS ÉPROUVANT DES DIFFICULTÉS D'INSERTION SOCIALE (6E ET 7E AL. DE L'ART. L. 222-5 DU CASF) - EXAMEN DES DEMANDES - 1) POSSIBILITÉ DE PRENDRE EN COMPTE L'EXISTENCE ET LA DURÉE DE LA PRISE EN CHARGE ANTÉRIEURE DE L'INTÉRESSÉ PAR LE SERVICE DE L'AIDE SOCIALE À L'ENFANCE (ASE) - EXISTENCE - 2) ILLÉGALITÉ D'UN REFUS FONDÉ SUR CE SEUL MOTIF [RJ2].
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">135-03-02-01-01 COLLECTIVITÉS TERRITORIALES. DÉPARTEMENT. ATTRIBUTIONS. COMPÉTENCES TRANSFÉRÉES. ACTION SOCIALE. - COMPÉTENCE DU RÈGLEMENT DÉPARTEMENTAL D'AIDE SOCIALE POUR PRÉCISER LES CONDITIONS D'ATTRIBUTION ET LE MONTANT DES PRESTATIONS VERSÉES PAR LE DÉPARTEMENT [RJ1] - CONDITIONS.
</SCT>
<ANA ID="9A"> 01-02-02-01-045 Il résulte des articles L. 111-1, L. 111-4, L. 121-3, L. 121-4 et L. 223-1 du code de l'action sociale et des familles (CASF) que le département a l'obligation de verser celles des prestations d'aide sociale que la loi met à sa charge à toute personne en remplissant les conditions légales. Lorsque les conditions d'attribution ou les montants des prestations sont déterminées par les lois et décrets qui les régissent, le règlement départemental d'aide sociale ne peut édicter que des dispositions plus favorables. En l'absence de conditions ou montants précisément fixés par les lois et décrets, si le règlement départemental d'aide sociale peut préciser les critères au vu desquels il doit être procédé à l'évaluation de la situation des demandeurs, il ne peut fixer de condition nouvelle conduisant à écarter par principe du bénéfice des prestations des personnes qui entrent dans le champ des dispositions législatives applicables. Enfin, pour les prestations d'aide sociale qu'il crée de sa propre initiative, le département définit, par le règlement départemental d'aide sociale, les règles selon lesquelles ces prestations sont accordées.</ANA>
<ANA ID="9B"> 04-01-01 Il résulte des articles L. 111-1, L. 111-4, L. 121-3, L. 121-4 et L. 223-1 du code de l'action sociale et des familles (CASF) que le département a l'obligation de verser celles des prestations d'aide sociale que la loi met à sa charge à toute personne en remplissant les conditions légales. Lorsque les conditions d'attribution ou les montants des prestations sont déterminées par les lois et décrets qui les régissent, le règlement départemental d'aide sociale ne peut édicter que des dispositions plus favorables. En l'absence de conditions ou montants précisément fixés par les lois et décrets, si le règlement départemental d'aide sociale peut préciser les critères au vu desquels il doit être procédé à l'évaluation de la situation des demandeurs, il ne peut fixer de condition nouvelle conduisant à écarter par principe du bénéfice des prestations des personnes qui entrent dans le champ des dispositions législatives applicables. Enfin, pour les prestations d'aide sociale qu'il crée de sa propre initiative, le département définit, par le règlement départemental d'aide sociale, les règles selon lesquelles ces prestations sont accordées.</ANA>
<ANA ID="9C"> 04-02-02 1) Si, compte tenu de l'objet de la mesure considérée, l'existence et la durée de sa prise en charge antérieure par le service de l'aide sociale à l'enfance sont au nombre des critères sur lesquels un département peut légalement se fonder pour accorder ou maintenir la prise en charge par le service de l'aide sociale à l'enfance d'un jeune majeur de moins de vingt et un ans éprouvant des difficultés d'insertion sociale faute de ressources ou d'un soutien familial suffisants, 2) le président du conseil général ne pouvait légalement refuser cette prise en charge à l'intéressé au seul motif, et sans procéder à l'évaluation de sa situation, qu'il ne remplissait pas la condition fixée par le règlement d'aide sociale du département imposant d' avoir bénéficié d'une prise en charge antérieure par le service de l'aide sociale à l'enfance au cours de sa minorité pendant un an au moins.</ANA>
<ANA ID="9D"> 135-03-02-01-01 Il résulte des articles L. 111-1, L. 111-4, L. 121-3, L. 121-4 et L. 223-1 du code de l'action sociale et des familles (CASF) que le département a l'obligation de verser celles des prestations d'aide sociale que la loi met à sa charge à toute personne en remplissant les conditions légales. Lorsque les conditions d'attribution ou les montants des prestations sont déterminées par les lois et décrets qui les régissent, le règlement départemental d'aide sociale ne peut édicter que des dispositions plus favorables. En l'absence de conditions ou montants précisément fixés par les lois et décrets, si le règlement départemental d'aide sociale peut préciser les critères au vu desquels il doit être procédé à l'évaluation de la situation des demandeurs, il ne peut fixer de condition nouvelle conduisant à écarter par principe du bénéfice des prestations des personnes qui entrent dans le champ des dispositions législatives applicables. Enfin, pour les prestations d'aide sociale qu'il crée de sa propre initiative, le département définit, par le règlement départemental d'aide sociale, les règles selon lesquelles ces prestations sont accordées.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 4 novembre 1994, Département de la Dordogne, n° 100354, p. 486.,,[RJ2] Rappr. décision du même jour, CE, Département de l'Isère, n° 417467, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
