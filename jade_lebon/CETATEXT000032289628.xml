<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032289628</ID>
<ANCIEN_ID>JG_L_2016_03_000000389158</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/28/96/CETATEXT000032289628.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème / 5ème SSR, 23/03/2016, 389158</TITRE>
<DATE_DEC>2016-03-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>389158</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème / 5ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GATINEAU, FATTACCINI</AVOCATS>
<RAPPORTEUR>M. Benjamin de Maillard</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2016:389158.20160323</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Le syndicat CGT des personnels des éclaireuses, éclaireurs de France a demandé au tribunal administratif de Montreuil d'annuler pour excès de pouvoir la décision du 29 avril 2014 par laquelle le directeur régional des entreprises, de la concurrence, de la consommation, du travail et de l'emploi d'Ile-de-France a homologué le document unilatéral portant plan de sauvegarde de l'emploi de l'association Eclaireuses, Eclaireurs de France. Par un jugement nos 1405860, 1405862, 1405863, 1405867, 1405868, 1405869, 1405871, 1405892, 1405894, 1405963, 1405964 du 22 septembre 2014 le tribunal a rejeté sa demande. <br/>
<br/>
              Par un arrêt n° 14VE03183 du 3 février 2015, la cour administrative d'appel de Versailles a, sur appel du syndicat CGT des personnels des éclaireuses, éclaireurs de France, annulé ce jugement et la décision du 29 avril 2014.  <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 1er avril et 4 mai 2015 au secrétariat du contentieux du Conseil d'Etat, l'association Eclaireuses, Eclaireurs de France demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel du syndicat CGT des personnels des éclaireuses, éclaireurs de France ;<br/>
<br/>
              3°) de mettre à la charge du syndicat CGT des personnels des éclaireuses, éclaireurs de France la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code du travail ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Benjamin de Maillard, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gatineau, Fattaccini, avocat de l'association Eclaireuses, Eclaireurs de France ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 1233-61 du code du travail : " Dans les entreprises d'au moins cinquante salariés, lorsque le projet de licenciement concerne au moins dix salariés dans une même période de trente jours, l'employeur établit et met en oeuvre un plan de sauvegarde de l'emploi pour éviter les licenciements ou en limiter le nombre. (...) " ; que les articles L. 1233-24-1 et L. 1233-24-4 du même code prévoient que le contenu de ce plan de sauvegarde de l'emploi peut être déterminé par un accord collectif d'entreprise et qu'à défaut d'accord, il est fixé par un document élaboré unilatéralement par l'employeur ; qu'enfin, aux termes de l'article L. 1233-57-3 du même code, dans sa rédaction en vigueur à la date de la décision litigieuse : " En l'absence d'accord collectif (...) l'autorité administrative homologue le document élaboré par l'employeur mentionné à l'article L. 1233-24-4, après avoir vérifié (...) la régularité de la procédure d'information et de consultation du comité d'entreprise (...) et le respect par le plan de sauvegarde de l'emploi des articles L. 1233-61 à L. 1233-63 en fonction des critères suivants : / 1° Les moyens dont disposent l'entreprise, l'unité économique et sociale et le groupe ; / 2° Les mesures d'accompagnement prévues au regard de l'importance du projet de licenciement ; / 3° Les efforts de formation et d'adaptation tels que mentionnés aux articles L. 1233-4 et L. 6321-1 " ; que, lorsqu'elle est saisie par un employeur d'une demande d'homologation d'un document élaboré en application de l'article L. 1233-24-4 du code du travail et fixant le contenu d'un plan de sauvegarde de l'emploi, il appartient à l'administration de s'assurer, sous le contrôle du juge de l'excès de pouvoir, que la procédure d'information et de consultation du comité d'entreprise a été régulière ; qu'elle ne peut légalement accorder l'homologation demandée que si le comité a été mis à même d'émettre régulièrement un avis, d'une part sur l'opération projetée et ses modalités d'application et, d'autre part, sur le projet de licenciement collectif et le plan de sauvegarde de l'emploi ; <br/>
<br/>
              2. Considérant, par ailleurs, qu'aux termes de l'article L. 1233-57-6 du code du travail : " L'administration peut, à tout moment en cours de procédure, faire toute observation ou proposition à l'employeur concernant le déroulement de la procédure ou les mesures sociales prévues à l'article L. 1233-32. Elle envoie simultanément copie de ses observations au comité d'entreprise ou, à défaut, aux délégués du personnel et, lorsque la négociation de l'accord visé à l'article L. 1233-24-1 est engagée, aux organisations syndicales représentatives dans l'entreprise. / L'employeur répond à ces observations et adresse copie de sa réponse aux représentants du personnel et, le cas échéant, aux organisations syndicales " ; que l'obligation qui incombe à l'administration d'envoyer copie au comité d'entreprise des observations qu'elle adresse à l'employeur sur le fondement de ces dispositions vise à ce que le comité d'entreprise, saisi en vertu des dispositions rappelées au point 2 ci-dessus, dispose de tous les éléments utiles pour formuler ses deux avis en toute connaissance de cause ; que le respect de cette obligation doit, par suite, être pris en compte dans l'appréciation globale de la régularité de la procédure d'information et de consultation du comité d'entreprise à laquelle doit se livrer l'administration à la date où elle statue sur la demande d'homologation ;<br/>
<br/>
              3. Considérant qu'il résulte des termes de l'arrêt attaqué que, pour annuler la décision d'homologation du 29 avril 2014 du directeur régional des entreprises, de la concurrence, de la consommation, du travail et de l'emploi d'Ile-de-France relative au plan de sauvegarde de l'emploi de l'association Eclaireuses, Eclaireurs de France, la cour administrative d'appel de Versailles a retenu que l'absence d'envoi par l'administration au comité d'entreprise d'un courrier du 11 février 2014 que celle-ci avait adressé à l'employeur et qui comportait des observations au sens des dispositions citées ci-dessus de l'article L. 1233-57-6 du code du travail, méconnaissait le respect d'une garantie et entachait par elle-même d'irrégularité la procédure d'information et de consultation du comité d'entreprise ; qu'il résulte de ce qui a été dit ci-dessus qu'en se prononçant ainsi, sans procéder à une appréciation globale de la régularité de la procédure d'information et de consultation du comité d'entreprise, la cour administrative d'appel de Versailles a commis une erreur de droit ; que, par suite, et sans qu'il soit besoin de statuer sur la recevabilité du pourvoi du ministre, son arrêt doit être annulé ; <br/>
<br/>
              4. Considérant que le délai de trois mois imparti à la cour administrative d'appel pour statuer par les dispositions de l'article L. 1235-7-1 du code du travail étant expiré, il y a lieu pour le Conseil d'Etat, en application des mêmes dispositions, de statuer immédiatement sur l'appel formé par le syndicat CGT des personnels des éclaireuses, éclaireurs de France contre le jugement du 22 septembre 2014 du tribunal administratif de Montreuil ;<br/>
<br/>
              Sur le moyen tiré de l'absence de communication au comité d'entreprise du courrier du 11 février 2014 :<br/>
<br/>
              5. Considérant qu'aux termes de l'article L. 1233-57-5 du code du travail : " Toute demande tendant, avant transmission de la demande de validation ou d'homologation, à ce qu'il soit enjoint à l'employeur de fournir les éléments d'information relatifs à la procédure en cours ou de se conformer à une règle de procédure prévue par les textes législatifs, les conventions collectives ou un accord collectif est adressée à l'autorité administrative. Celle-ci se prononce dans un délai de cinq jours " ; que les décisions par lesquelles, sur le fondement de ces dispositions, l'administration enjoint à l'employeur de fournir les éléments d'information ou de se conformer à une règle de procédure ne revêtent pas le caractère d'observations ou de propositions au sens des dispositions de l'article L. 1233-57-6 du même code ; que l'administration n'est, par suite, pas tenue d'en envoyer copie au comité d'entreprise ;<br/>
<br/>
              6. Considérant qu'il ressort des pièces du dossier que le courrier du 11 février 2014 adressé par le directeur de l'unité territoriale de Seine-Saint-Denis à l'association Eclaireuses, Eclaireurs de France faisait suite à une demande d'injonction formulée le 4 février précédent par plusieurs organisations syndicales ; que si l'administration pouvait encore adresser une injonction à l'employeur malgré l'expiration du délai de cinq jours imparti par les dispositions de l'article L. 1233-57-5 du code du travail, ce courrier doit toutefois, eu égard à son contenu et à son absence de caractère impératif, être regardé comme ayant le caractère d'observations au sens des dispositions de l'article L. 1233-57-6 du même code ;<br/>
<br/>
              7. Considérant que s'il est constant que, ainsi qu'il a déjà été dit ci-dessus, l'administration n'a pas, comme l'article L. 1233-57-6 du code du travail lui en faisait pourtant obligation, envoyé au comité d'entreprise une copie de son courrier d'observations, il ressort des pièces du dossier que l'employeur l'a néanmoins communiqué aux organisations syndicales représentées au sein de l'association, en même temps que la réponse qu'il y apportait ; qu'il ressort également des pièces du dossier que les délégués syndicaux destinataires de ce document ont pris part aux réunions du comité d'entreprise, dans des conditions permettant à celui-ci de tenir compte des éléments ainsi transmis ; que, par suite, la procédure d'information et de consultation du comité d'entreprise n'a pas été entachée d'irrégularité ; <br/>
<br/>
              Sur les autres moyens d'appel :<br/>
<br/>
              8. Considérant que le syndicat requérant reprend en appel les autres moyens qu'il avait invoqués en première instance ; qu'il y a lieu, par adoption des motifs retenus par le tribunal administratif de Montreuil, d'écarter ces autres moyens ; <br/>
<br/>
              9. Considérant qu'il résulte de tout ce qui précède que le syndicat CGT des personnels des éclaireuses, éclaireurs de France n'est pas fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Montreuil a rejeté sa demande ; <br/>
<br/>
              10. Considérant que, l'association Eclaireuses, Eclaireurs de France n'étant pas, dans la présente instance, la partie perdante, les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'il soit fait droit aux conclusions présentées à ce titre en appel par le syndicat CGT des personnels des éclaireuses, éclaireurs de France ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge du syndicat CGT des personnels des éclaireuses, éclaireurs de France les sommes que demande l'association Eclaireuses, Eclaireurs de France, tant en appel qu'en cassation, au titre des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>
<br/>
      D E C I D E :<br/>
      --------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Versailles du 3 février 2015 est annulé.<br/>
Article 2 : La requête présentée par le syndicat CGT des personnels des éclaireuses, éclaireurs de France devant la cour administrative d'appel de Versailles est rejetée. <br/>
Article 3 : Le surplus des conclusions de l'association Eclaireuses, Eclaireurs de France présentées, devant la cour administrative d'appel et devant le Conseil d'Etat, au titre de l'article L. 761-1 du code de justice administrative est rejeté.<br/>
Article 4 : La présente décision sera notifiée à l'association Eclaireuses, Eclaireurs de France, au syndicat CGT des personnels des éclaireuses, éclaireurs de France et à la ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">66-07 TRAVAIL ET EMPLOI. LICENCIEMENTS. - VALIDATION OU HOMOLOGATION ADMINISTRATIVE DES PSE (LOI N° 2013-504 DU 14 JUIN 2013) - OBSERVATIONS FAITES PAR L'ADMINISTRATION SUR LE FONDEMENT DE L'ART. L. 1233-57-6 DU CODE DU TRAVAIL - OBLIGATION DE L'ADMINISTRATION D'EN ADRESSER COPIE AU COMITÉ D'ENTREPRISE - MODALITÉS D'APPRÉCIATION PAR L'ADMINISTRATION DU RESPECT DE CETTE OBLIGATION.
</SCT>
<ANA ID="9A"> 66-07 L'obligation qui incombe à l'administration d'envoyer copie au comité d'entreprise des observations qu'elle adresse à l'employeur sur le fondement de l'article L. 1233-57-6 du code du travail vise à ce que le comité d'entreprise dispose de tous les éléments utiles pour formuler ses deux avis en toute connaissance de cause. Le respect de cette obligation doit, par suite, être pris en compte dans l'appréciation globale de la régularité de la procédure d'information et de consultation du comité d'entreprise à laquelle doit se livrer l'administration à la date où elle statue sur la demande d'homologation.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
