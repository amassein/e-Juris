<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025580441</ID>
<ANCIEN_ID>JG_L_2012_03_000000336459</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/58/04/CETATEXT000025580441.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème et 8ème sous-sections réunies, 26/03/2012, 336459, Publié au recueil Lebon</TITRE>
<DATE_DEC>2012-03-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>336459</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème et 8ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN ; SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Christian Fournier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:336459.20120326</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 10 février et 10 mai 2010 au secrétariat du contentieux du Conseil d'Etat, présentés pour la COMMUNE DE VERGEZE, représentée par son maire ; la commune demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 08MA01766 du 10 décembre 2009 par lequel la cour administrative d'appel de Marseille, faisant droit à la requête des sociétés Nestlé Waters, Nestlé Waters France et Nestlé Waters Supply Sud, a annulé, d'une part, le jugement n° 0630061 du 11 janvier 2008 du tribunal administratif de Nîmes rejetant leur demande d'annulation de la délibération du 25 octobre 2006 par laquelle son conseil municipal a modifié la dénomination du lieu-dit "Les Bouillens" en "Source Perrier - Les Bouillens" et d'autre part, cette délibération ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel des sociétés Nestlé Waters, Nestlé Waters France et Nestlé Waters Supply Sud ; <br/>
<br/>
              3°) de mettre à la charge des sociétés Nestlé Waters France et Nestlé Waters Supply Sud le versement d'une somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 13 mars 2012, présentée pour la société Nestlé Waters et autres ;<br/>
<br/>
              Vu le code général des collectivités territoriales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Christian Fournier, Maître des Requêtes, <br/>
<br/>
              - les observations de la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la COMMUNE DE VERGEZE et de l'association pour la défense de la source Perrier et de la SCP Piwnica, Molinié, avocat de la société Nestlé Waters et autres, <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la COMMUNE DE VERGEZE et de l'association pour la défense de la source Perrier et à la SCP Piwnica, Molinié, avocat de la société Nestlé Waters et autres, <br/>
<br/>
<br/>
<br/>Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par une délibération du 25 octobre 2006, le conseil municipal de la COMMUNE DE VERGEZE a modifié la dénomination du lieu-dit "Les Bouillens" en "Source Perrier - Les Bouillens" ; que, par un jugement du 11 janvier 2008, le tribunal administratif de Nîmes a rejeté la demande des sociétés Nestlé Waters, Nestlé Waters France et Nestlé Waters Supply Sud tendant à l'annulation de cette délibération ; que par l'arrêt attaqué du 10 décembre 2009, la cour administrative d'appel de Marseille a annulé ce jugement et cette délibération ;<br/>
<br/>
              Sur l'intervention de l'association pour la défense de la source Perrier :<br/>
<br/>
              Considérant que l'association pour la défense de la source Perrier a intérêt à l'annulation de l'arrêt attaqué ; qu'ainsi son intervention est recevable ;<br/>
<br/>
              Sur le bien-fondé de l'arrêt attaqué :<br/>
<br/>
              Sans qu'il soit besoin d'examiner les autres moyens du pourvoi ;<br/>
<br/>
              Considérant qu'aux termes du premier alinéa de l'article L. 2121-29 du code général des collectivités territoriales : " Le conseil municipal règle par ses délibérations les affaires de la commune " ; <br/>
<br/>
              Considérant que le nom d'un lieu-dit situé sur le territoire d'une commune trouve généralement son origine dans la géographie ou la topographie, est hérité de l'histoire ou est forgé par les usages ; qu'aucun texte législatif ou réglementaire ne prévoit qu'il appartient au conseil municipal de la commune ou à une autre autorité administrative d'attribuer un nom à un lieu-dit ou de modifier un nom existant ; que, toutefois, en application des dispositions de l'article L. 2121-29 du code général des collectivités territoriales rappelées ci-dessus, le conseil municipal est compétent, dans le cas où un intérêt public local le justifie, pour décider de modifier le nom d'un lieu-dit situé sur le territoire de la commune ; que, par suite, en jugeant que la délibération du conseil municipal de Vergèze du 25 octobre 2006 décidant de modifier le nom du lieu-dit "Les Bouillens" était entachée d'incompétence, sans rechercher si un intérêt public communal permettait au conseil municipal de procéder à un tel changement sur le fondement des dispositions de l'article L.2121-29 du code général des collectivités territoriales sans méconnaître sa compétence, la cour administrative d'appel de Marseille a commis une erreur de droit ; que la COMMUNE DE VERGEZE est donc fondée à demander l'annulation de l'arrêt qu'elle attaque ;<br/>
<br/>
              Sur les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge des sociétés Nestlé Waters France et Nestlé Waters Supply Sud la somme globale de 3 000 euros à verser à la COMMUNE DE VERGEZE au titre de ces dispositions ; que ces mêmes dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de la commune qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'intervention de l'association pour la défense de la source Perrier est admise.<br/>
Article 2 : L'arrêt du 10 décembre 2009 de la cour administrative d'appel de Marseille est annulé.<br/>
Article 3 : L'affaire est renvoyée devant la cour administrative d'appel de Marseille.<br/>
Article 4 : Les sociétés Nestlé Waters France et Nestlé Waters Supply Sud verseront à la COMMUNE DE VERGEZE une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 5 : Les conclusions des sociétés Nestlé Waters, Nestlé Waters France et Nestlé Waters Supply Sud présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 6 : La présente décision sera notifiée à la COMMUNE DE VERGEZE, à la société Nestlé Waters, à la société Nestlé Waters France, à la société Nestlé Waters Supply Sud et à l'association pour la défense de la source Perrier.<br/>
Copie pour information en sera adressée au ministre de l'intérieur, de l'outre-mer, des collectivités territoriales et de l'immigration.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-02 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - COMPÉTENCE. - TEXTE LÉGISLATIF OU RÉGLEMENTAIRE PRÉVOYANT QU'IL APPARTIENT À UNE AUTORITÉ ADMINISTRATIVE D'ATTRIBUER UN NOM À UN LIEU-DIT OU DE MODIFIER UN NOM EXISTANT - ABSENCE - COMPÉTENCE DU CONSEIL MUNICIPAL POUR MODIFIER LE NOM D'UN LIEU-DIT SITUÉ SUR LE TERRITOIRE DE LA COMMUNE, DANS LE CAS OÙ UN INTÉRÊT PUBLIC LOCAL LE JUSTIFIE - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">135-02-01-02-01-02-02 COLLECTIVITÉS TERRITORIALES. COMMUNE. ORGANISATION DE LA COMMUNE. ORGANES DE LA COMMUNE. CONSEIL MUNICIPAL. ATTRIBUTIONS. DÉCISIONS RELEVANT DE LA COMPÉTENCE DU CONSEIL MUNICIPAL. - MODIFICATION DU NOM D'UN LIEU-DIT SITUÉ SUR LE TERRITOIRE DE LA COMMUNE, DANS LE CAS OÙ UN INTÉRÊT PUBLIC LOCAL LE JUSTIFIE - INCLUSION.
</SCT>
<ANA ID="9A"> 01-02 Le nom d'un lieu-dit situé sur le territoire d'une commune trouve généralement son origine dans la géographie ou la topographie, est hérité de l'histoire ou est forgé par les usages. Aucun texte législatif ou réglementaire ne prévoit qu'il appartient au conseil municipal de la commune ou à une autre autorité administrative d'attribuer un nom à un lieu-dit ou de modifier un nom existant. Toutefois, en application des dispositions de l'article L. 2121-29 du code général des collectivités territoriales (CGCT), selon lesquelles le conseil municipal règle par ses délibérations les affaires de la commune, le conseil municipal est compétent, dans le cas où un intérêt public local le justifie, pour décider de modifier le nom d'un lieu-dit situé sur le territoire de la commune.</ANA>
<ANA ID="9B"> 135-02-01-02-01-02-02 Le nom d'un lieu-dit situé sur le territoire d'une commune trouve généralement son origine dans la géographie ou la topographie, est hérité de l'histoire ou est forgé par les usages. Aucun texte législatif ou réglementaire ne prévoit qu'il appartient au conseil municipal de la commune ou à une autre autorité administrative d'attribuer un nom à un lieu-dit ou de modifier un nom existant. Toutefois, en application des dispositions de l'article L. 2121-29 du code général des collectivités territoriales (CGCT), selon lesquelles le conseil municipal règle par ses délibérations les affaires de la commune, le conseil municipal est compétent, dans le cas où un intérêt public local le justifie, pour décider de modifier le nom d'un lieu-dit situé sur le territoire de la commune.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
