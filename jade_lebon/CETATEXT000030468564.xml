<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030468564</ID>
<ANCIEN_ID>JG_L_2015_04_000000386912</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/46/85/CETATEXT000030468564.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème / 2ème SSR, 10/04/2015, 386912</TITRE>
<DATE_DEC>2015-04-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>386912</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème / 2ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GASCHIGNARD ; SCP RICHARD ; SCP POTIER DE LA VARDE, BUK LAMENT</AVOCATS>
<RAPPORTEUR>Mme Natacha Chicot</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Bertrand Dacosta</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:386912.20150410</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              La société TAT a demandé au juge des référés du tribunal administratif de Nouvelle-Calédonie, sur le fondement de l'article L. 551-24 du code de justice administrative, d'annuler la procédure de passation du marché de missions de service médical d'urgence par hélicoptère (SMUH) lancée par le centre hospitalier territorial de Nouvelle-Calédonie. Par une ordonnance n° 1400464 du 18 décembre 2014, le juge des référés du tribunal administratif de Nouvelle-Calédonie a annulé cette procédure de passation.<br/>
<br/>
              1° Sous le n° 386912, par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 5 et 20 janvier et 20 mars 2015 au secrétariat du contentieux du Conseil d'Etat, le centre hospitalier territorial de Nouvelle-Calédonie demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance du juge des référés du tribunal administratif de Nouvelle-Calédonie ;<br/>
<br/>
              2°) de mettre à la charge de la société TAT le versement de la somme de 4 000 euros en application des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              2° Sous le n° 386920, par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 5 et 21 janvier et 18 mars 2015 au secrétariat du contentieux du Conseil d'Etat, la société Hélicocéan demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la même ordonnance ; <br/>
<br/>
              2°) statuant en référé, de rejeter la demande de la société TAT ; <br/>
<br/>
              3°) de mettre à la charge de la société TAT le versement de la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - le code de commerce ;<br/>
              - la délibération du congrès n° 136/CP du 1er mars 1967 modifiée portant réglementation des marchés publics en Nouvelle-Calédonie ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Natacha Chicot, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Bertrand Dacosta, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Richard, avocat du centre hospitalier territorial de Nouvelle-Calédonie, à la SCP Gaschignard, avocat de la société TAT, et à la SCP Potier de la Varde, Buk Lament, avocat de la société Hélicocéan ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 551-24 du code de justice administrative : " En Nouvelle-Calédonie, (...) le président du tribunal administratif, ou le magistrat qu'il délègue, peut être saisi en cas de manquement aux obligations de publicité et de mise en concurrence auxquelles est soumise la passation des marchés et contrats publics en vertu de dispositions applicables localement. / Les personnes habilitées à agir sont celles qui ont un intérêt à conclure le contrat et qui sont susceptibles d'être lésées par ce manquement, ainsi que le haut-commissaire de la République dans le cas où le contrat est conclu ou doit être conclu par une collectivité territoriale ou un établissement public local. / Le président du tribunal administratif peut être saisi avant la conclusion du contrat. Il peut ordonner à l'auteur du manquement de se conformer à ses obligations et suspendre la passation du contrat ou l'exécution de toute décision qui s'y rapporte. Il peut également annuler ces décisions et supprimer les clauses ou prescriptions destinées à figurer dans le contrat et qui méconnaissent lesdites obligations. Dès qu'il est saisi, il peut enjoindre de différer la signature du contrat jusqu'au terme de la procédure et pour une durée maximum de vingt jours. / Le président du tribunal administratif ou son délégué statue en premier et dernier ressort en la forme des référés " ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis au juge des référés du tribunal administratif de Nouvelle-Calédonie que, par un avis d'appel public à la concurrence du 4 août 2014, le centre hospitalier territorial de Nouvelle-Calédonie a lancé une procédure d'appel d'offres ouvert pour la passation d'un marché de missions de service médical d'urgence par hélicoptère ; que, saisi par la société TAT, candidate à la conclusion du contrat, sur le fondement des dispositions précitées de l'article L. 551-24 du code de justice administrative, le juge des référés du tribunal administratif de Nouvelle-Calédonie a, par une première ordonnance du 5 décembre 2014, enjoint au centre hospitalier de suspendre la procédure de passation du marché litigieux jusqu'au 22 décembre 2014 puis, par une seconde ordonnance du 18 décembre 2014, annulé la procédure de passation engagée ; que, par des pourvois qu'il y a lieu de joindre, le centre hospitalier territorial de Nouvelle-Calédonie et la société Hélicocéan, autre candidate à l'obtention du marché, se pourvoient en cassation contre cette dernière ordonnance ;<br/>
<br/>
              Sur les conclusions à fins de non-lieu en cassation :<br/>
<br/>
              3. Considérant qu'en vertu de l'article 25 de la délibération du congrès n° 136/CP du 1er mars 1967 portant réglementation des marchés publics en Nouvelle-Calédonie, dans sa version applicable au litige, l'avis d'appel d'offres précise " le délai pendant lequel les candidats resteront engagés par les offres " ;<br/>
<br/>
              4. Considérant que la société TAT soutient que le délai de validité des offres, fixé à cent-vingt jours en application de l'article 3 du règlement de la consultation, ayant expiré depuis le 10 février 2015, les offres présentées par les candidats à l'obtention du marché litigieux sont nécessairement caduques et que les conclusions du centre hospitalier territorial de Nouvelle-Calédonie et de la société Hélicocéan en cassation sont, par suite, sans objet ;<br/>
<br/>
              5. Considérant que si la personne publique doit, sous peine d'irrégularité de la procédure de passation, choisir l'attributaire d'un marché dans le délai de validité des offres, elle peut toujours solliciter de l'ensemble des candidats une prorogation ou un renouvellement de ce délai ; que lorsque ce délai est arrivé ou arrive à expiration avant l'examen des offres en raison, comme c'est le cas en l'espèce, d'une procédure devant le juge du référé précontractuel, la personne publique peut poursuivre la procédure de passation du marché avec les candidats qui acceptent la prorogation ou le renouvellement du délai de validité de leur offre ; qu'il s'ensuit que le litige, contrairement à ce que soutient la société TAT, n'est pas devenu sans objet du seul fait de l'expiration du délai de validité des offres après l'intervention de l'ordonnance du juge des référés du tribunal administratif de Nouvelle-Calédonie annulant la procédure de passation du marché ; qu'il en résulte que le présent litige conserve son objet ;<br/>
<br/>
              Sur les pourvois : <br/>
<br/>
              6. Considérant qu'en vertu de l'article 25 de la délibération du congrès de Nouvelle-Calédonie du 1er mars 1967, dans sa version applicable au litige, l'avis d'appel d'offres précise, le cas échéant, si les concurrents ont la possibilité de présenter des variantes au projet de l'administration ; qu'aux termes de l'article 27-2 de la même délibération, la commission d'appel d'offres se fonde, pour établir le classement des offres, " sur une pluralité de critères non discriminatoires et liés à l'objet du marché " ;<br/>
<br/>
              7. Considérant qu'il ressort des pièces du dossier soumis au juge des référés que le centre hospitalier territorial de Nouvelle-Calédonie exige l'affectation au service médical d'urgence par hélicoptère de deux appareils " biturbine de catégorie A exploité en classe de performance 1 " ; que l'hélicoptère de remplacement qui peut être utilisé à d'autres fins pendant la durée du contrat, doit toutefois être mis à disposition du service dans un délai de deux heures en cas de panne de l'appareil principal voire de quatre heures en application du m) de l'article 4-1 du règlement de la consultation ; que, par l'ordonnance attaquée, le juge des référés a jugé que ces modalités d'utilisation du second appareil présentent un caractère discriminatoire et portent, par suite, atteinte au principe d'égalité de traitement des candidats au motif qu'elles sont de nature à favoriser la société Hélicocéan en lui permettant de proposer un prix plus compétitif que les autres candidats, par un usage de l'appareil de remplacement au transport public de voyageur pour lequel elle est l'unique détentrice d'une licence d'exploitation sur le territoire de la Nouvelle-Calédonie ; <br/>
<br/>
              8. Considérant qu'en statuant ainsi, sans rechercher si cette possibilité laissée aux candidats correspond aux besoins de la personne publique et peut être justifiée par l'objet du marché, le juge des référés a commis une erreur de droit ; qu'ainsi et sans qu'il soit besoin d'examiner les autres moyens des pourvois, le centre hospitalier territorial de Nouvelle-Calédonie et la société Hélicocéan sont fondés, par ce moyen qui n'est pas nouveau en cassation, à demander l'annulation de l'ordonnance attaquée ;<br/>
              9. Considérant qu'il y a lieu, en application des dispositions de l'article L. 821-2 du code de justice administrative, de régler l'affaire au titre de la procédure de référé engagée par la société TAT ;<br/>
<br/>
              10. Considérant, en premier lieu, qu'aux termes de l'article 27-2 de la délibération du 1er mars 1967, dans sa version applicable au litige : " La commission d'appel d'offres arrête la liste des soumissionnaires admis à concourir, élimine les offres inappropriées, irrégulières ou inacceptables, procède au classement des offres par ordre décroissant et propose d'attribuer le marché au candidat dont l'offre correspond le mieux aux besoins exprimés. / Elle se fonde sur une pluralité de critères non discriminatoires et liés à l'objet du marché. Ces critères peuvent porter notamment sur le prix des prestations, le coût d'utilisation, la valeur technique ou le délai d'exécution. D'autres critères peuvent être pris en compte s'ils sont justifiés par l'objet du marché. / Chacun des critères retenus fait l'objet d'une pondération. / Les critères ainsi que leur pondération sont indiqués dans le règlement particulier d'appel d'offres. " ;<br/>
<br/>
              11. Considérant que la personne publique définit librement la méthode de notation pour la mise en oeuvre de chacun des critères de sélection des offres qu'elle a définis et rendus publics ; que, toutefois, une méthode de notation est entachée d'irrégularité si, en méconnaissance des principes fondamentaux d'égalité de traitement des candidats et de transparence des procédures, elle est par elle-même de nature à priver de leur portée les critères de sélection ou à neutraliser leur pondération et est, de ce fait, susceptible de conduire, pour la mise en oeuvre de chaque critère, à ce que la meilleure note ne soit pas attribuée à la meilleure offre ou, au regard de l'ensemble des critères pondérés, à ce que l'offre économiquement la plus avantageuse ne soit pas choisie ; qu'il en va ainsi alors même que la personne publique, qui n'y est pas tenue, aurait rendu publique, dans l'avis d'appel à concurrence ou les documents de la consultation, une telle méthode de notation ;<br/>
<br/>
              12. Considérant qu'il résulte de l'instruction, notamment de l'acte d'engagement, que le montant de l'offre présentée par les candidats comprend en principe un prix forfaitaire mensuel de mise à disposition durant le jour aéronautique assorti du prix de la minute de vol ; que l'offre de base est elle-même divisée en deux tranches d'exploitation annuelle, la première jusqu'à trois cent cinquante heures de vol, la seconde à partir de trois cent cinquante heures de vol ; qu'enfin, le règlement de la consultation prévoit, en son article 5, que pour la mise en oeuvre du critère prix qui représente 30 % de la note finale, chaque offre sera notée sur la base d'un volume horaire de quatre cents heures ; <br/>
<br/>
              13. Considérant qu'il résulte de ce qui précède, d'une part, que contrairement à ce que soutient la société TAT, les informations transmises aux candidats quant au volume horaire des prestations attendues ont été suffisamment précises, compte tenu des fluctuations inhérentes à ce type d'activités, pour permettre à ces derniers d'élaborer une offre sans qu'il soit par ailleurs établi que l'absence d'estimations complémentaires favorise, à supposer qu'il soit candidat, l'ancien attributaire du marché ; que si la société TAT soutient, d'autre part, que la méthode de notation ne permettrait pas la prise en compte d'un prix forfaitaire mensuel de mise à disposition différent une fois le seuil des trois cent cinquante heures de vol dépassé, cette seule circonstance n'est pas de nature à établir, alors que le centre hospitalier n'a pas encore à ce stade procédé à l'examen des offres, que cette méthode serait par elle-même de nature à priver de leur portée les critères de sélection ou à neutraliser leur pondération ; qu'il appartient en effet à la personne publique, avant l'examen des offres, de définir une méthode de simulation de l'exécution des prestations sur une période annuelle pour permettre la comparabilité des offres ; que, conformément aux principes rappelés au point 11, cette méthode n'a pas à être rendue publique dans l'avis d'appel à concurrence ou les documents de la consultation ; qu'il s'ensuit que la société TAT n'est pas fondée à soutenir que les critères et la méthode de notation relatifs à l'attribution du marché litigieux seraient irréguliers ;<br/>
<br/>
              14. Considérant, en deuxième lieu, que, comme il a été dit au point 7, l'appareil de remplacement pouvait être utilisé à des fins commerciales sous réserve de sa disponibilité dans un délai maximal de quatre heures en cas de panne de l'appareil principal ; qu'à supposer même que la société Hélicocéan, unique détentrice d'une licence d'exploitation pour le transport public de personnes sur le territoire de la Nouvelle-Calédonie à la date de la passation du marché litigieux, serait, de fait, la seule à pouvoir bénéficier pleinement de cette souplesse autorisée par le centre hospitalier, il résulte de l'instruction, d'une part, que la nécessité pour ce dernier de pouvoir disposer d'un appareil de remplacement pour assurer la continuité du service n'est pas contestée et, d'autre part, que la mobilisation permanente de deux appareils n'est pas justifiée dès lors qu'elle entraînerait un surcoût pour la personne publique sans rapport avec la réalité de ses besoins ; qu'il s'ensuit que la société requérante n'est pas fondée à soutenir que la possibilité laissée aux candidats d'exploiter le second appareil à des fins commerciales serait contraire au principe d'égalité de traitement des candidats ; <br/>
<br/>
              15. Considérant, enfin, qu'il résulte des dispositions précitées de l'article L. 551-24 du code de justice administrative que la méconnaissance éventuelle des dispositions de l'article L. 420-2 du code de commerce prohibant les abus de position dominante, qui n'est au demeurant pas applicable en Nouvelle-Calédonie, n'est pas au nombre des manquements dont peut être saisi le juge des référés précontractuels ; que le moyen tiré de ce que la possibilité laissée aux candidats d'exploiter le second appareil à des fins commerciales serait susceptible de placer la société Hélicocéan en situation d'abuser de sa situation dominante est, par suite, inopérant ;<br/>
<br/>
              16. Considérant qu'il résulte de tout ce qui précède que, sans qu'il soit besoin de statuer sur les fins de non-recevoir opposées par le centre hospitalier territorial de Nouvelle-Calédonie et la société Hélicocéan, la demande présentée par la société TAT doit être rejetée ; <br/>
<br/>
              17. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge du centre hospitalier territorial de Nouvelle-Calédonie et de la société Hélicocéan qui ne sont pas, dans la présente instance, les parties perdantes ; qu'il y a lieu, en revanche, sur le fondement des mêmes dispositions, de mettre à la charge de la société TAT la somme de 4 500 euros à verser, d'une part, au centre hospitalier territorial de Nouvelle-Calédonie et, d'autre part, à la société Hélicocéan au titre des frais exposés par eux et non compris dans les dépens devant le juge des référés du tribunal administratif de Nouvelle-Calédonie et le Conseil d'Etat ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du juge des référés du tribunal administratif de Nouvelle-Calédonie du 18 décembre 2014 est annulée.<br/>
Article 2 : La demande présentée par la société TAT devant le juge des référés du tribunal administratif de Nouvelle-Calédonie ainsi que ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La société TAT versera au centre hospitalier territorial de Nouvelle-Calédonie et à la société Hélicocéan la somme de 4 500 euros chacun en application de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée au centre hospitalier territorial de Nouvelle-Calédonie et aux sociétés TAT et Hélicocéan.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-02-005 MARCHÉS ET CONTRATS ADMINISTRATIFS. FORMATION DES CONTRATS ET MARCHÉS. FORMALITÉS DE PUBLICITÉ ET DE MISE EN CONCURRENCE. - EXPIRATION DU DÉLAI DE VALIDITÉ DES OFFRES - POSSIBILITÉ DE SOLLICITER DES CANDIDATS UNE PROROGATION OU UN RENOUVELLEMENT DU DÉLAI - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">39-08-015-01 MARCHÉS ET CONTRATS ADMINISTRATIFS. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. PROCÉDURES D'URGENCE. - EXPIRATION DU DÉLAI DE VALIDITÉ DES OFFRES APRÈS UNE SAISINE DU JUGE DU RÉFÉRÉ PRÉCONTRACTUEL - NON LIEU - ABSENCE, LA PERSONNE PUBLIQUE POUVANT TOUJOURS SOLLICITER DES CANDIDATS UNE PROROGATION DU DÉLAI.
</SCT>
<ANA ID="9A"> 39-02-005 Si la personne publique doit, sous peine d'irrégularité de la procédure de passation, choisir l'attributaire d'un marché dans le délai de validité des offres, elle peut toujours solliciter de l'ensemble des candidats une prorogation ou un renouvellement de ce délai.</ANA>
<ANA ID="9B"> 39-08-015-01 Si la personne publique doit, sous peine d'irrégularité de la procédure de passation, choisir l'attributaire d'un marché dans le délai de validité des offres, elle peut toujours solliciter de l'ensemble des candidats une prorogation ou un renouvellement de ce délai.... ,,Lorsque ce délai est arrivé ou arrive à expiration avant l'examen des offres en raison d'une procédure devant le juge du référé précontractuel, la personne publique peut poursuivre la procédure de passation du marché avec les candidats qui acceptent la prorogation ou le renouvellement du délai de validité de leur offre. En l'espèce, ordonnance du juge du référé précontractuel ayant annulé une procédure de passation d'un marché. Le recours contre cette ordonnance n'est pas devenu sans objet du seul fait de l'expiration du délai de validité des offres après l'intervention de cette ordonnance.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
