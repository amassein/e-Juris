<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000024081916</ID>
<ANCIEN_ID>JG_L_2011_05_000000339406</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/24/08/19/CETATEXT000024081916.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème / 2ème SSR, 23/05/2011, 339406</TITRE>
<DATE_DEC>2011-05-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>339406</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème / 2ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN ; SCP LAUGIER, CASTON</AVOCATS>
<RAPPORTEUR>M. Laurent Cytermann</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Boulouis</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2011:339406.20110523</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 11 mai et 10 août 2010 au secrétariat du contentieux du Conseil d'Etat, présentés pour la COMMUNE D'AJACCIO, représentée par son maire ; la commune demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 07MA04835 du 11 mars 2010 par lequel la cour administrative d'appel de Marseille a, à la demande de la société Esa, annulé le jugement du 18 octobre 2007 du tribunal administratif de Bastia et la décision par laquelle le maire de la COMMUNE D'AJACCIO avait attribué à la société LRS le marché relatif à la refonte de la signalisation directionnelle de la commune ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la requête de la société Esa ;<br/>
<br/>
              3°) de mettre à la charge de la société Esa le versement d'une somme de 5 000 euros sur le fondement de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 5 mai 2011, présentée pour la COMMUNE D'AJACCIO ;<br/>
<br/>
              Vu le code des marchés publics annexé au décret n° 2004-15 du 7 janvier 2004 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Laurent Cytermann, chargé des fonctions de Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Waquet, Farge, Hazan, avocat de la COMMUNE D'AJACCIO et de la SCP Laugier, Caston, avocat de la société Esa, <br/>
<br/>
              - les conclusions de M. Nicolas Boulouis, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Waquet, Farge, Hazan, avocat de la COMMUNE D'AJACCIO et à la SCP Laugier, Caston, avocat de la société Esa ;<br/>
<br/>
<br/>
              Considérant qu'il résulte des pièces du dossier soumis aux juges du fond que la COMMUNE D'AJACCIO a engagé en 2005 une procédure d'appel d'offres ouvert pour la passation d'un marché de travaux relatif à la refonte de sa signalisation directionnelle ; que, selon l'article 4 du règlement de consultation, les offres étaient appréciées à raison de 40 % pour la valeur technique, de 40 % pour le prix et de 20 % pour les délais de fabrication et de pose ; que ce règlement prévoyait en outre, en application de l'article 49 du code des marchés publics, que " les candidats fourniront avec leur offre des échantillons gratuits de panneaux et de mâts et de signalisation directionnelle, conformes aux produits proposés dans leur offre " ; que la société LRS, dont l'offre a été retenue par la commission d'appel d'offres, a reçu notification de la décision d'attribution du marché le 2 février 2006 ; qu'en réponse à la demande de la société Esa, la COMMUNE D'AJACCIO a fait connaître qu'elle avait apprécié la valeur technique des offres en se fondant à hauteur de deux tiers sur les échantillons et à hauteur d'un tiers sur les mémoires techniques ; que la société Esa, concurrente évincée, a saisi le tribunal administratif de Bastia d'une demande d'annulation de la décision d'attribution du marché ; que par l'arrêt attaqué du 11 mars 2010, la cour administrative d'appel de Marseille a annulé le jugement du tribunal administratif, qui avait rejeté la demande de la société Esa, ainsi que la décision d'attribution du marché ; <br/>
<br/>
              Considérant qu'aux termes du II de l'article 53 du code des marchés publics, dans sa version applicable au marché en cause : " Pour attribuer le marché au candidat qui a présenté l'offre économiquement la plus avantageuse, la personne publique se fonde sur divers critères variables selon l'objet du marché, notamment le coût d'utilisation, la valeur technique de l'offre, son caractère innovant, ses performances en matière de protection de l'environnement, le délai d'exécution, les qualités esthétiques et fonctionnelles, le service après-vente et l'assistance technique, la date et le délai de livraison, le prix des prestations. (...) / Les critères sont définis dans l'avis d'appel public à la concurrence ou dans le règlement de la consultation. Ces critères sont pondérés ou à défaut hiérarchisés (...) " ; que ces dispositions imposent au pouvoir adjudicateur d'informer les candidats des critères de sélection des offres ainsi que de leur pondération ou hiérarchisation ; que si le pouvoir adjudicateur décide, pour mettre en oeuvre ces critères de sélection des offres, de faire usage de sous-critères également pondérés ou hiérarchisés, il doit porter à la connaissance des candidats la pondération ou la hiérarchisation de ces sous-critères dès lors que, eu égard à leur nature et à l'importance de cette pondération ou hiérarchisation, ils sont susceptibles d'exercer une influence sur la présentation des offres par les candidats ainsi que sur leur sélection et doivent en conséquence être eux-mêmes regardés comme des critères de sélection ; que le pouvoir adjudicateur n'est en revanche pas tenu d'informer les candidats de la méthode de notation des offres ;<br/>
<br/>
              Considérant que, pour annuler la décision d'attribution du marché, la cour administrative d'appel de Marseille s'est fondée sur la circonstance que l'appréciation des échantillons était constitutive d'un sous-critère du critère de la valeur technique, non prévu par les documents de la consultation, que la COMMUNE D'AJACCIO n'avait dès lors pu régulièrement prendre en considération ; qu'en retenant ce motif, sans rechercher si la prise en compte des échantillons révélait un critère distinct de celui de la valeur technique et n'était pas constitutive d'une simple méthode de notation des offres pour l'appréciation du critère de la valeur technique, la cour a commis une erreur de droit ; qu'il en résulte que, sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, son arrêt doit être annulé ;<br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              Considérant que la société Esa, qui a participé au groupement Signature/Esa dont l'offre a été écartée par la COMMUNE D'AJACCIO, a intérêt à l'annulation de la décision litigieuse ; que la circonstance qu'elle n'ait pas engagé de référé précontractuel est sans incidence sur sa recevabilité à contester la décision de conclure le marché ;<br/>
<br/>
              Considérant que le règlement de la consultation d'un marché est obligatoire dans toutes ses mentions ; que l'administration ne peut en conséquence attribuer le marché à un candidat qui ne respecterait pas une des prescriptions imposées par le règlement ; qu'en l'espèce, l'article 4 du règlement de consultation prévoit que " le candidat proposera en outre un planning de réalisation faisant apparaître les différentes phases du chantier. " ; qu'il résulte des pièces du dossier que la société LRS, attributaire du marché, n'a pas produit ce planning ; que la commune a ainsi attribué le marché à un candidat qui ne respectait pas une des prescriptions imposées par son règlement ; que par suite, et sans qu'il soit besoin d'examiner les autres moyens de la requête, la société Esa est fondée à soutenir que c'est à tort que, par son jugement du 18 octobre 2007, le tribunal administratif de Bastia a rejeté sa demande ;<br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la COMMUNE D'AJACCIO le versement à la société Esa de la somme de 3 000 euros en application des dispositions de l'article L. 761-1 du code de justice administrative ; que ces mêmes dispositions font obstacle à ce qu'il soit fait droit aux conclusions présentées au même titre par la COMMUNE D'AJACCIO ;<br/>
<br/>
<br/>
     D E C I D E :<br/>
     --------------<br/>
Article 1er : L'arrêt n° 07MA04835 de la cour administrative d'appel de Marseille du 11 mars 2010 est annulé.<br/>
<br/>
Article 2 : Le jugement n° 0600348 du tribunal administratif de Bastia du 18 octobre 2007 est annulé.<br/>
<br/>
Article 3 : La décision du 2 février 2006 de la COMMUNE D'AJACCIO attribuant le marché relatif à la refonte de la signalisation directionnelle à la société LRS est annulée.<br/>
<br/>
Article 4 : La COMMUNE D'AJACCIO versera à la société Esa la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 5 : Le surplus des conclusions de la COMMUNE D'AJACCIO est rejeté.<br/>
<br/>
Article 6 : La présente décision sera notifiée à la COMMUNE D'AJACCIO, à la société Esa et à la société L.R.S.<br/>
''<br/>
''<br/>
''<br/>
''<br/>
N° 339406- 2 -<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-02-005 MARCHÉS ET CONTRATS ADMINISTRATIFS. FORMATION DES CONTRATS ET MARCHÉS. FORMALITÉS DE PUBLICITÉ ET DE MISE EN CONCURRENCE. - OBLIGATION D'INFORMER LES CANDIDATS SUR LES CRITÈRES D'ATTRIBUTION DU MARCHÉ ET LEURS CONDITIONS DE MISE EN &#140;UVRE DÈS L'ENGAGEMENT DE LA PROCÉDURE [RJ1] - PORTÉE - OBLIGATION D'INDIQUER LA MÉTHODE D'ÉVALUATION DES OFFRES AU REGARD DE CHACUN DE CES CRITÈRES - ABSENCE [RJ2] - PRISE EN COMPTE D'ÉCHANTILLONS.
</SCT>
<ANA ID="9A"> 39-02-005 La prise en compte d'échantillons demandée par le pouvoir adjudicateur en application de l'article 49 du code des marchés publics peut révéler un critère distinct de celui de la valeur technique ou être constitutive d'une simple méthode de notation des offres pour l'appréciation du critère de la valeur technique.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Section, 30 janvier 2009, Agence nationale pour l'emploi, n° 290236, p. 3.,,[RJ2] Cf. CE, 31 mars 2010, Collectivité territoriale de Corse, n° 334279, T. p. 848.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
