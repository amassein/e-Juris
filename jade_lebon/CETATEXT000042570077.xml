<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042570077</ID>
<ANCIEN_ID>JG_L_2020_11_000000442411</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/57/00/CETATEXT000042570077.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème chambre, 25/11/2020, 442411</TITRE>
<DATE_DEC>2020-11-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>442411</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Laurent Roulaud</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:442411.20201125</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme D... B... a demandé au tribunal administratif d'Amiens d'annuler l'élection du maire et des adjoints de la commune de Rouy-le-Grand (Oise) qui s'est déroulée le 28 mai 2020. Par une ordonnance n° 20001783 du 21 juillet 2020, le tribunal administratif d'Amiens a rejeté sa protestation.<br/>
<br/>
              Par une requête et deux mémoires en réplique enregistrés les 4 août, 15 septembre et 7 octobre 2020 au secrétariat du contentieux du Conseil d'Etat, Mme B... demande au Conseil d'Etat d'annuler cette ordonnance. Elle soutient :<br/>
              - que sa protestation n'était pas tardive, dès lors qu'elle l'a envoyée par la Poste dans un délai utile pour qu'elle soit enregistrée avant l'expiration du délai, que la délibération par laquelle le maire et les adjoints ont été élus a été prise à huis-clos et n'a pas fait l'objet de l'affichage requis ;<br/>
              - que l'élection elle-même est entachée d'irrégularités liées notamment au huis-clos ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code électoral ;<br/>
              - le code général des collectivités territoriales ;<br/>
              - la loi n° 2020-290 du 23 mars 2020 ; <br/>
              - l'ordonnance n° 2020-305 du 25 mars 2020 ;<br/>
              - l'ordonnance n° 2020-306 du 25 mars 2020 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Laurent Roulaud, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. D'une part, l'article R. 119 du code électoral dispose que : " Les réclamations contre les opérations électorales doivent être consignées au procès-verbal, sinon être déposées, à peine d'irrecevabilité, au plus tard à dix-huit heures le cinquième jour qui suit l'élection, à la sous-préfecture ou à la préfecture. Elles sont immédiatement adressées au préfet qui les fait enregistrer au greffe du tribunal administratif. / Les protestations peuvent également être déposées directement au greffe du tribunal administratif dans le même délai. " Aux termes de l'article L. 2122-12 du code général des collectivités territoriales : " Les élections du maire et des adjoints sont rendues publiques, par voie d'affiche, dans les vingt-quatre heures. " Aux termes de l'article L. 2122-13 du même code : " L'élection du maire et des adjoints peut être arguée de nullité dans les conditions, formes et délais prescrits pour les réclamations contre les élections du conseil municipal. " L'article R. 2122-1 du même code prévoit l'affichage des nominations a lieu à la porte de la mairie. Aux termes de l'article D. 2122-2 du même code : " Le délai de cinq jours dans lequel, conformément à l'article L. 2122-13, l'élection du maire et des adjoints peut être arguée de nullité court à partir de vingt-quatre heures après l'élection. " <br/>
<br/>
              2. D'autre part, il résulte des dispositions du 1° du II de l'article 1er de l'ordonnance n° 2020-306 du 25 mars 2020 relative à la prorogation des délais échus pendant la période d'urgence sanitaire et à l'adaptation des procédures pendant cette même période que les dispositions générales relatives à la prorogation des délais édictées par son titre Ier, et notamment celles de son article 2, ne sont pas applicables aux délais et mesures concernant les élections régies par le code électoral et les consultations auxquelles ce code est rendu applicable. Si, aux termes du I de l'article 15 de l'ordonnance n° 2020-305 du même jour portant adaptation des règles applicables devant les juridictions de l'ordre administratif, les dispositions de l'article 2 de l'ordonnance n° 2020-306 sont applicables aux procédures devant les juridictions de l'ordre administratif, le II du même article déroge à cette règle pour les réclamations et recours mentionnés à l'article R. 119 du code électoral contre les opérations électorales du premier tour des élections municipales. Il résulte de la combinaison de l'ensemble de ces dispositions et de celles du code général des collectivités territoriales citées au point 1 que le délai de recours contre l'élection du maire et des adjoints organisée à la suite du premier tour des élections municipales du 15 mars 2020 expirait au terme du délai de droit commun de cinq jours courant à partir de vingt-quatre heures après l'élection.<br/>
<br/>
              3. Il résulte de l'instruction que la protestation formée par Mme B... contre l'élection du maire et des adjoints de la commune de Rouy-le-Grand (Oise), qui s'est déroulée le 28 mai 2020, n'a été enregistrée au greffe du tribunal administratif d'Amiens que le 4 juin 2020, alors que le délai fixé par les dispositions de l'article D. 2122-2 du code général des collectivités territoriales expirait le 3 juin à 18 heures. Si la requérante fait valoir que le conseil municipal s'est réuni à huis-clos et soutient qu'il n'a pas été procédé à l'affichage des nominations avant le début du mois de juin, elle n'apporte pas, en tout état de cause, à l'appui de cette dernière affirmation, d'éléments suffisamment probants. Par ailleurs, sa protestation ayant été postée le 2 juin, cet envoi n'a pas été effectué, compte tenu du délai normal d'acheminement du courrier, en temps utile pour parvenir au greffe du tribunal administratif avant l'expiration du délai imparti. Mme B..., n'est, dès lors, pas fondée à soutenir que c'est à tort que, par l'ordonnance attaquée, le président du tribunal administratif d'Amiens a rejeté sa protestation comme tardive et, par suite, irrecevable.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de Mme B... est rejetée.<br/>
Article 2 : La présente décision sera notifiée à Mme D... B..., à M. C... A..., premier défendeur dénommé, et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">28-08-01-02 ÉLECTIONS ET RÉFÉRENDUM. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. INTRODUCTION DE L'INSTANCE. DÉLAIS. - ELECTIONS MUNICIPALES DE 2020 - ELECTIONS DU MAIRE ET DES ADJOINTS ORGANISÉES À LA SUITE DU PREMIER TOUR (15 MARS) - PROLONGATION DU DÉLAI DE CONTESTATION PAR L'ORDONNANCE DU 25 MARS 2020 - ABSENCE [RJ1].
</SCT>
<ANA ID="9A"> 28-08-01-02 Il résulte de la combinaison, d'une part du 1° du II de l'article 1er de l'ordonnance n° 2020-306 du 25 mars 2020 et du 3° du II de l'article 15 de l'ordonnance n° 2020-305 du même jour, d'autre part des articles L. 2122-12, L. 2122-13, R. 2122-1 et D. 2122-2 du code général des collectivités territoriales (CGCT) et de l'article R. 119 du code électoral que le délai de recours contre l'élection du maire et des adjoints organisée à la suite du premier tour des élections municipales du 15 mars 2020 expire au terme du délai de droit commun de cinq jours courant à partir de vingt-quatre heures après l'élection.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Comp., s'agissant de l'élection du conseil municipal, CE, 15 juillet 2020, Elections municipales et communautaires de Saint-Sulpice-sur-Risle, n° 440055, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
