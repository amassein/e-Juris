<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041569420</ID>
<ANCIEN_ID>JG_L_2020_02_000000425138</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/56/94/CETATEXT000041569420.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 5ème chambres réunies, 12/02/2020, 425138</TITRE>
<DATE_DEC>2020-02-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>425138</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Didier Ribes</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Stéphane Hoynck</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2020:425138.20200212</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              1° Sous le n° 425138, par une requête et un mémoire en réplique, enregistrés le 30 octobre 2018 et le 23 août 2019 au secrétariat du contentieux du Conseil d'État, M. B... A... demande au Conseil d'État d'annuler pour excès de pouvoir le décret n° 2018-767 du 31 août 2018 relatif au financement des mandataires judiciaires à la protection des majeurs en tant qu'il modifie l'article R. 471-5-3 du code de l'action sociale et des familles. <br/>
<br/>
<br/>
              2° Sous le n° 425163, par une requête sommaire et un mémoire complémentaire, enregistrés les 31 octobre 2018 et 31 janvier 2019 au secrétariat du contentieux du Conseil d'Etat, la Fédération nationale des associations tutélaires, l'Union nationale des associations familiales et l'Union nationale des associations de parents, de personnes handicapées mentales et de leurs amis demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté du 31 août 2018 de la ministre des solidarités et de la santé et du ministre de l'action et des comptes publics relatif à la détermination du coût des mesures de protection exercées par les mandataires judiciaires à la protection des majeurs ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              3° Sous le n° 425164, par une requête sommaire et un mémoire complémentaire, enregistrés les 31 octobre 2018 et 31 janvier 2019 au secrétariat du contentieux du Conseil d'Etat, la Fédération nationale des associations tutélaires, l'Union nationale des associations familiales et l'Union nationale des associations de parents, de personnes handicapées mentales et de leurs amis demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le même décret n° 2018-767 du 31 août 2018 ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son Préambule ;<br/>
              - le code de l'action sociale et des familles ;<br/>
              - le code civil ;<br/>
              - le code de procédure civile ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Didier Ribes, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Stéphane Hoynck, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rousseau, Tapie, avocat de la Fédération nationale des associations tutélaires et autres ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Les requêtes de M. A... contre le décret du 31 août 2018 relatif au financement des mandataires judiciaires à la protection des majeurs , d'une part, et de la Fédération nationale des associations tutélaires, de l'Union nationale des associations familiales et de l'Union nationale des associations de parents, de personnes handicapées mentales et de leurs amis contre ce même décret et l'arrêté pris le même jour par la ministre des solidarités et de la santé et le ministre de l'action et des comptes publics relatif à la détermination du coût des mesures de protection exercées par les mandataires judiciaires à la protection des majeurs, d'autre part, soulèvent des questions similaires. Il y a lieu de les joindre pour statuer par une seule décision. <br/>
<br/>
              Sur la légalité externe du décret et de l'arrêté attaqués :<br/>
<br/>
              2. En premier lieu, s'il était loisible au Gouvernement de demander à l'Inspection générale des affaires sociales de lui remettre un rapport relatif aux mesures faisant l'objet du décret et de l'arrêté litigieux, ces derniers pouvaient légalement être adoptés avant la remise de ce rapport. Par suite, les associations requérantes ne sont pas fondées à soutenir que le décret et l'arrêté attaqués auraient été pris au terme d'une procédure irrégulière au motif qu'ils auraient été publiés avant la remise de ce rapport.<br/>
<br/>
              3. En deuxième lieu, aux termes de l'article L. 312-3 du code de l'action sociale et des familles, dans sa rédaction applicable à la date du décret et de l'arrêté attaqués : " (...) La section sociale du comité national de l'organisation sanitaire et sociale est consultée par le ministre chargé des affaires sociales sur les problèmes généraux relatifs à l'organisation des établissements et services mentionnés à l'article L. 312-1, notamment sur les questions concernant leur fonctionnement administratif et financier. " Le décret et l'arrêté attaqués ont pour objet de déterminer les modalités du financement de l'exercice à titre individuel de l'activité de mandataire judiciaire à la protection des majeurs et de celle de délégué aux prestations familiales, qui ne sont pas des établissements et services sociaux ou médico-sociaux mentionnés à l'article L. 312-1 du code de l'action sociale et des familles. La circonstance, invoquée par les associations requérantes, que le I de l'article R. 471-5-5 du code de l'action sociale et des familles, créé par le décret attaqué, prévoit que la participation financière du majeur protégé à la mesure de protection est versée, non au mandataire judiciaire, mais à l'établissement de santé, à l'établissement social ou médico-social ou au groupement de coopération sociale ou médico-sociale dans le cas où le mandataire agit en tant que préposé d'un tel établissement ou groupement, ne permet pas de regarder les dispositions du décret et de l'arrêté attaqués comme portant sur des questions concernant le fonctionnement administratif et financier des établissements et services sociaux ou médico-sociaux au sens des dispositions précitées de l'article L. 312-3 du code de l'action sociale et des familles. Par suite, le moyen tiré du défaut de consultation du comité national de l'organisation sanitaire et sociale doit être écarté.<br/>
<br/>
              Sur la légalité interne du décret et de l'arrêté attaqués :<br/>
<br/>
              4. En premier lieu, l'article L. 472-3 du code de l'action sociale et des familles prévoit que : " La rémunération des personnes physiques mandataires judiciaires à la protection des majeurs est déterminée en fonction d'indicateurs liés, en particulier, à la charge de travail résultant de l'exécution des mesures de protection dont elles ont la charge ". L'article R. 471-5-1 du même code précise que le coût des mesures de protection juridique du majeur protégé est déterminé par un arrêté des ministres chargés de la famille et du budget en fonction d'indicateurs permettant d'évaluer la charge de travail du mandataire et tenant à la nature des missions du mandataire judiciaire, au lieu de vie de la personne protégée ainsi qu'aux ressources et au patrimoine de cette personne, calculés selon les modalités prévues à l'article R. 471-5-2. Pour calculer le coût mensuel des mesures de protection exercées par un mandataire judiciaire, l'arrêté attaqué applique à un coût de référence un coefficient propre à chacun des indicateurs énoncés à l'article R. 471-5-1. En particulier, l'arrêté prévoit que le coefficient relatif aux ressources et au patrimoine augmente lorsque le montant des ressources augmente.<br/>
<br/>
              5. D'une part, contrairement à ce qui est soutenu, il ne résulte pas des dispositions de l'article L. 472-3 du code de l'action sociale et des familles que la charge de travail doive être l'indicateur prépondérant pour déterminer la rémunération des mandataires judiciaires à la protection des majeurs. Par suite, les associations requérantes ne sont pas fondées à soutenir qu'en prévoyant que le coefficient fondé sur le montant des ressources pourra avoir, lorsque les ressources du majeur protégé dépassent un certain niveau, un effet multiplicateur plus élevé que celui du coefficient fondé sur la nature des missions, l'arrêté attaqué méconnaîtrait l'article L. 472-3 du code de l'action sociale et des familles.<br/>
<br/>
              6. D'autre part, il ne ressort pas des pièces des dossiers qu'en ayant retenu, pour apprécier le coût de la mesure de protection exercée par le mandataire judiciaire, des critères tenant à la nature des missions du mandataire judiciaire, au lieu de vie de la personne protégée ainsi qu'aux ressources et au patrimoine de cette personne et en les ayant affectés des coefficients retenus, le décret et l'arrêté attaqués soient entachés d'une erreur manifeste d'appréciation.<br/>
<br/>
              7. En deuxième lieu, l'article 1254-1 du code de procédure civile prévoit que le greffier en chef peut solliciter, aux frais de la personne protégée lorsque ses ressources le permettent, l'assistance d'un huissier pour l'examen de vérification des comptes. Par ailleurs, le quatrième alinéa de l'article 419 du code civil prévoit qu'en complément de l'indemnité de base prévue par le code de l'action sociale et des familles, le juge des tutelles ou le conseil de famille peut, après avis du procureur de la République, autoriser le versement au mandataire judiciaire d'une indemnité complémentaire, à la charge de la personne protégée, pour l'accomplissement d'un acte ou d'une série d'actes requis par la mesure de protection et impliquant des diligences particulièrement longues ou complexes, lorsque les sommes perçues au titre de l'indemnité de base s'avèrent manifestement insuffisantes.<br/>
<br/>
              8. Aucun texte ni aucun principe n'impose, pour déterminer le montant de la participation du majeur protégé au coût des mesures de protection juridique, de déduire de son revenu les charges spécifiques susceptibles de lui être imposées en application des dispositions citées au point précédent. Par suite, le moyen tiré de ce que l'article R. 471-5-2 du code de l'action sociale et des familles, dans sa rédaction issue du décret attaqué, et l'arrêté pris pour son application seraient entachés d'illégalité en ce qu'ils ne prennent pas en compte de tels frais dans la détermination du montant de cette participation ne peut qu'être écarté.  <br/>
<br/>
              9. En dernier lieu, l'article R. 471-5-3 du code de l'action sociale et des familles dans sa rédaction issue de l'article 1er du décret attaqué, dispose que " la participation de la personne au financement du coût de sa mesure est calculée sur la base du montant annuel des ressources dont a bénéficié la personne protégée l'année précédente. / Le coût des mesures mentionné à l'article L. 471-5 du présent code n'est pas à la charge de la personne protégée lorsque le montant des ressources annuelles de l'année précédente mentionné à l'article R. 471-5-2 est inférieur ou égal au montant annuel de l'allocation aux adultes handicapés mentionnée à l'article L. 821-1 du code de la sécurité sociale en vigueur au 1er janvier de l'année précédente. / Dans le cas contraire, la participation de la personne est calculée selon les taux suivants : / 1° 0,6 % pour la tranche des revenus annuels égale ou inférieure au montant annuel de l'allocation aux adultes handicapés ; / 2° 8,5 % pour la tranche des revenus annuels supérieure au montant annuel de l'allocation aux adultes handicapés et inférieure ou égale au montant brut annuel du salaire minimum interprofessionnel de croissance ; / 3° 20 % pour la tranche des revenus annuels supérieure au montant brut annuel du salaire minimum interprofessionnel de croissance et inférieure ou égale au même montant majoré de 150 % ; / 4° 3 % pour la tranche des revenus annuels supérieure au montant brut annuel du salaire minimum interprofessionnel de croissance majoré de 150 % et inférieure ou égale à six fois le montant brut annuel du salaire minimum interprofessionnel de croissance. " Il résulte de ces dispositions que les majeurs protégés dont les ressources sont inférieures ou égales au montant de l'allocation aux adultes handicapés, qui s'élevait à un maximum de 819 euros par mois au 1er avril 2018, sont exonérés de toute participation au financement de la mesure de protection les concernant et que, en revanche, un prélèvement de 0,6 % est appliqué à l'intégralité de la tranche de revenus correspondant au montant de cette allocation dès que les ressources du majeur protégé excèdent ce montant.<br/>
<br/>
              10. Les requérants soutiennent que ces dispositions contestées de l'article R. 471-5-3 précité produisent des effets de seuil se traduisant par une réduction du revenu disponible des majeurs protégés dont les ressources prises en compte pour le calcul de cette participation dépassent de peu le seuil correspondant au montant maximum de l'allocation aux adultes handicapés, par rapport à ceux dont les ressources sont inférieures ou égales à ce seuil. Ils indiquent, sans être contredits sur ce point, que le montant mis à la charge d'un majeur protégé, lorsque ses ressources excèdent d'un euro ce seuil, est de l'ordre de cinq euros par mois.<br/>
<br/>
              11. Les dispositions contestées prennent ainsi pour seuil à partir duquel est calculée la participation de la personne protégée au financement du coût de sa protection juridique, le montant maximum annuel de l'allocation aux adultes handicapés. Compte tenu de la modicité des ressources des intéressés, le seuil étant en dessous de l'indicateur de pauvreté relative, des conséquences de l'application du taux de 0,6 % sur le montant des revenus annuels des intéressés dès lors qu'ils dépassent ce seuil, en l'absence de tout mécanisme de lissage, la différence de traitement qui en résulte, selon que les personnes protégées sont juste en dessous ou juste au-dessus de ce seuil, est manifestement disproportionnée au regard de l'objet de la mesure, lequel est de les faire participer au financement de leur protection juridique en fonction de leurs ressources. <br/>
<br/>
              12. Il résulte de tout ce qui précède que M. A..., d'une part, la Fédération nationale des associations titulaires et autres, d'autre part, sont seulement fondés à demander l'annulation du 1° de l'article R. 471-5-3 du code de l'action sociale et des familles, dans sa rédaction issue de l'article 1er du décret attaqué, qui dispose qu'est mis à la charge du majeur protégé un prélèvement de 0,6 % applicable à l'intégralité de la tranche de revenus correspondant au montant annuel de l'allocation aux adultes handicapés lorsque ses ressources excèdent ce montant. <br/>
<br/>
              13. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'État le versement à la Fédération nationale des associations titulaires et autres de la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le 1° de l'article R. 471-5-3 du code de l'action sociale et des familles est annulé.<br/>
Article 2 : L'État versera à la Fédération nationale des associations tutélaires et autres une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La requête n° 425163 et les surplus des conclusions des requêtes nos 425138 et 425164 sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à M. B... A..., à la Fédération nationale des associations tutélaires, première dénommée, pour l'ensemble des associations requérantes et à la ministre des solidarités et de la santé.<br/>
Copie en sera adressée au Premier ministre et au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">26-01-04 DROITS CIVILS ET INDIVIDUELS. ÉTAT DES PERSONNES. QUESTIONS DIVERSES RELATIVES À L`ÉTAT DES PERSONNES. - CAPACITÉ DES PERSONNES - MESURE DE PROTECTION DES PERSONNES MAJEURES - PARTICIPATION DES PERSONNES PROTÉGÉES AU FINANCEMENT DE LA MESURE - FIXATION D'UN SEUIL, CORRESPONDANT AU MONTANT MAXIMUM ANNUEL DE L'AAH, EN-DEÇÀ DUQUEL LA PERSONNE PROTÉGÉE EST EXONÉRÉE, ET AU-DELÀ DUQUEL UN PRÉLÈVEMENT DE 0,6 % EST APPLIQUÉ À L'INTÉGRALITÉ DE LA TRANCHE DE REVENUS CORRESPONDANT AU MONTANT DE CETTE ALLOCATION - DIFFÉRENCE DE TRAITEMENT MANIFESTEMENT DISPROPORTIONNÉE SELON QUE LES PERSONNES PROTÉGÉES SONT JUSTE EN DESSOUS OU JUSTE AU-DESSUS DE CE SEUIL.
</SCT>
<ANA ID="9A"> 26-01-04 Il résulte de l'article R. 471-5-3 du code de l'action sociale et des familles (CASF), dans sa rédaction issue de l'article 1er du décret n° 2018-767 du 31 août 2018 attaqué, que les majeurs protégés dont les ressources sont inférieures ou égales au montant de l'allocation aux adultes handicapés (AAH), qui s'élevait à un maximum de 819 euros par mois au 1er avril 2018, sont exonérés de toute participation au financement de la mesure de protection les concernant et que, en revanche, un prélèvement de 0,6 % est appliqué à l'intégralité de la tranche de revenus correspondant au montant de cette allocation dès que les ressources du majeur protégé excèdent ce montant.,,,Les requérants soutiennent que ces dispositions produisent des effets de seuil se traduisant par une réduction du revenu disponible des majeurs protégés dont les ressources prises en compte pour le calcul de cette participation dépassent de peu le seuil correspondant au montant maximum de l'AAH, par rapport à ceux dont les ressources sont inférieures ou égales à ce seuil. Ils indiquent, sans être contredits sur ce point, que le montant mis à la charge d'un majeur protégé, lorsque ses ressources excèdent d'un euro ce seuil, est de l'ordre de cinq euros par mois.,,,Les dispositions contestées prennent ainsi pour seuil à partir duquel est calculée la participation de la personne protégée au financement du coût de sa protection juridique, le montant maximum annuel de l'AAH. Compte tenu de la modicité des ressources des intéressés, le seuil étant en dessous de l'indicateur de pauvreté relative, des conséquences de l'application du taux de 0,6% sur le montant des revenus annuels des intéressés dès lors qu'ils dépassent ce seuil, en l'absence de tout mécanisme de lissage, la différence de traitement qui en résulte selon que les personnes protégées sont juste en dessous ou juste au-dessus de ce seuil, est manifestement disproportionnée au regard de l'objet de la mesure, lequel est de les faire participer au financement de leur protection juridique en fonction de leurs ressources.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
