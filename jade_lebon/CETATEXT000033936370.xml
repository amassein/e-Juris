<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033936370</ID>
<ANCIEN_ID>JG_L_2017_01_000000399793</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/93/63/CETATEXT000033936370.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 27/01/2017, 399793</TITRE>
<DATE_DEC>2017-01-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>399793</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>Mme Marie-Anne Lévêque</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:399793.20170127</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Mme B...A...a demandé au tribunal administratif de Paris d'annuler la décision du 22 avril 2010 par laquelle le commandant des forces françaises stationnées au Cap-Vert a procédé à son licenciement pour insuffisance professionnelle, d'enjoindre au ministre de la défense de lui accorder un rappel de salaires au titre de la période du 1er juin 2001 au 22 avril 2010, de l'indemniser des pathologies dont elle est atteinte conformément aux dispositions prévues par le code du travail et le code de la sécurité sociale, de lui accorder une pension d'invalidité, de condamner l'Etat à lui verser une somme de 200 000 euros en réparation du préjudice matériel et moral et des troubles dans les conditions d'existence subis du fait des souffrances physiques et morales endurées et de son incapacité permanente à exercer la profession de secrétaire comptable et de rectifier l'indication figurant sur les contrats de travail qu'elle a signés les 1er juin 2001 et 15 décembre 2002 avec la direction du commissariat de la marine du Cap-Vert et qui ne mentionnent que sa nationalité sénégalaise alors qu'elle dispose également de la nationalité française. Par un jugement n° 1220978 du 5 décembre 2013, le tribunal administratif de Paris a rejeté ses demandes.<br/>
<br/>
              Par un arrêt n° 14PA00546 du 31 décembre 2015, la cour administrative d'appel de Paris a, sur appel de MmeA..., annulé ce jugement et rejeté les demandes comme portées devant une juridiction incompétente pour en connaître. <br/>
<br/>
              Par un pourvoi, enregistré le 16 mai 2016 au secrétariat du contentieux du Conseil d'Etat, Mme A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement à la SCP Rocheteau et Uzan-Sarano de la somme de 3 000 euros au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ; <br/>
              - la loi n° 2000-321 du 12 avril 2000 ; <br/>
              - le décret n° 67-290 du 28 mars 1967 ;<br/>
              - le décret n° 69-697 du 18 juin 1969 ;<br/>
              - l'arrêté du 14 décembre 1995 portant application aux agents contractuels du ministère de la défense en service dans les postes permanents à l'étranger du décret n° 67-290 du 28 mars 1967 et du décret n° 69-697 du 18 juin 1969 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie-Anne Lévêque, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public. <br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de MmeA....<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des énonciations de l'arrêt attaqué qu'ayant souscrit un contrat à durée déterminée de six mois à compter du 1er décembre 2000 à la direction du commissariat de la marine au Cap-Vert, puis les deux contrats en litige, l'un à durée déterminée, conclu à compter du 1er juin 2001, l'autre à durée indéterminée, conclu à compter du 1er décembre 2002, MmeA..., ressortissante française possédant également la nationalité sénégalaise, a été recrutée en qualité de secrétaire comptable au sein du commandement des " Forces françaises du Cap-Vert ", devenues " Eléments français au Sénégal " depuis le 1er août 2011 ; que, par une décision du 22 avril 2010, le général, commandant les forces françaises stationnées au Cap-Vert, a prononcé son licenciement pour insuffisance professionnelle ; que, par un arrêt du 31 décembre 2015, la cour administrative d'appel de Paris a, sur appel de MmeA..., annulé le jugement du 5 décembre 2013 par lequel le tribunal administratif de Paris a rejeté comme non fondée sa demande tendant notamment à l'annulation de cette décision et à l'indemnisation de divers chefs de préjudices, mais a rejeté à son tour cette demande au motif qu'elle ne relevait pas de la compétence de la juridiction administrative française ; que Mme A...se pourvoit en cassation contre cet arrêt ;<br/>
<br/>
<br/>
              2. Considérant que le juge administratif, juge d'attribution en matière de contrat international de travail, est compétent pour connaître des litiges nés de l'exécution ou de la rupture de contrats conclus par les services de l'Etat à l'étranger pour le recrutement sur place d'agents publics non statutaires lorsqu'ils sont régis par la loi française ;<br/>
<br/>
              3. Considérant qu'aux termes du V de l'article 34 de la loi du 12 avril 2000 relative aux droits des citoyens dans leurs relations avec les administrations : " Lorsque les nécessités du service le justifient, les services de l'Etat à l'étranger peuvent, dans le respect des conventions internationales, faire appel à des personnels contractuels recrutés sur place, sur des contrats de travail soumis au droit local, pour exercer des fonctions concourant au fonctionnement desdits services " ; qu'il résulte de ces dispositions que l'autorité compétente peut, en l'absence de dispositions législatives ou réglementaires contraires, procéder, dans ses services situés à l'étranger, à des recrutements sur place d'agents, le cas échéant de nationalité française, sur des contrats de droit privé soumis au droit local, dès lors que ces agents sont amenés à concourir au fonctionnement desdits services et que ces recrutements répondent aux nécessités du service ; <br/>
<br/>
              4. Considérant, toutefois, qu'aux termes de l'article 1er du décret du 18 juin 1969 portant fixation du statut des agents contractuels de l'Etat et des établissements publics de l'Etat à caractère administratif, de nationalité française, en service à l'étranger : " Les dispositions du présent décret sont applicables aux agents contractuels de nationalité française relevant de l'Etat et des établissements publics de l'Etat à caractère administratif en service à l'étranger. / (...) Des arrêtés conjoints du ministre de l'économie et des finances, du ministre des affaires étrangères et du secrétaire d'Etat auprès du Premier ministre, chargé de la fonction publique, pris sur proposition du ministre intéressé, définiront pour chaque ministère les emplois et préciseront en tant que de besoin les pays étrangers auxquels les dispositions du présent décret sont applicables. / Les emplois susvisés peuvent être confiés soit à des agents non titulaires, soit à des agents titulaires (...) " ; qu'il résulte de ces dispositions que le décret du 18 juin 1969 est applicable aux contrats conclus avec des ressortissants français pour pourvoir les emplois qu'il vise ; que les litiges relatifs à ces contrats relèvent, dès lors, de la compétence de la juridiction administrative ; qu'est sans incidence la circonstance que le ressortissant français avec lequel un contrat a été conclu possède par ailleurs une autre nationalité ;<br/>
<br/>
              5. Considérant que, pour écarter la compétence de la juridiction administrative, la cour administrative d'appel de Paris a jugé, après avoir relevé que Mme A...avait la nationalité française, que les dispositions du décret du 18 juin 1969 ne s'opposaient pas à ce que son contrat de travail ne leur soit pas soumis, dès lors que l'intéressée l'avait conclu en qualité de ressortissante sénégalaise ; qu'elle a ainsi entaché son arrêt d'erreur de droit ; que, par suite, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, son arrêt doit être annulé ; <br/>
<br/>
              6. Considérant que Mme A...a obtenu le bénéfice de l'aide juridictionnelle ; que, par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ; qu'il y a lieu, dans les circonstances de l'espèce, et sous réserve que la SCP Rocheteau et Uzan-Sarano, avocat de MmeA..., renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de l'Etat la somme de 1 500 euros à verser à la SCP Rocheteau et Uzan-Sarano ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Paris du 31 décembre 2015 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Paris. <br/>
Article 3 : L'Etat versera à la SCP Rocheteau et Uzan-Sarano, avocat de MmeA..., une somme de 1 500 euros en application des dispositions du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
Article 4 : La présente décision sera notifiée à Mme B...A..., au ministre de la défense et  <br/>
à la ministre de la fonction publique.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">17-03-02-04-01 COMPÉTENCE. RÉPARTITION DES COMPÉTENCES ENTRE LES DEUX ORDRES DE JURIDICTION. COMPÉTENCE DÉTERMINÉE PAR UN CRITÈRE JURISPRUDENTIEL. PERSONNEL. AGENTS DE DROIT PUBLIC. - RESSORTISSANT FRANÇAIS AYANT CONCLU UN CONTRAT POUR POURVOIR UN EMPLOI EN SERVICE À L'ÉTRANGER VISÉ PAR LE DÉCRET DU 18 JUIN 1969, ALORS MÊME QU'IL POSSÈDERAIT UNE AUTRE NATIONALITÉ.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">36-12 FONCTIONNAIRES ET AGENTS PUBLICS. AGENTS CONTRACTUELS ET TEMPORAIRES. - DÉCRET DU 18 JUIN 1969 PORTANT FIXATION DU STATUT DES AGENTS CONTRACTUELS DE L'ETAT ET DES ÉTABLISSEMENTS PUBLICS DE L'ETAT À CARACTÈRE ADMINISTRATIF, DE NATIONALITÉ FRANÇAISE, EN SERVICE À L'ÉTRANGER - CHAMP D'APPLICATION - INCLUSION - RESSORTISSANT FRANÇAIS AYANT CONCLU UN CONTRAT POUR POURVOIR UN EMPLOI VISÉ PAR LE DÉCRET, ALORS MÊME QU'IL POSSÈDERAIT UNE AUTRE NATIONALITÉ.
</SCT>
<ANA ID="9A"> 17-03-02-04-01 Le décret n° 69-697 du 18 juin 1969 du 18 juin 1969 portant fixation du statut des agents contractuels de l'Etat et des établissements publics de l'Etat à caractère administratif, de nationalité française, en service à l'étranger est applicable aux contrats conclus avec des ressortissants français pour pourvoir les emplois qu'il vise. Les litiges relatifs à ces contrats relèvent, dès lors, de la compétence de la juridiction administrative. Est sans incidence la circonstance que le ressortissant français avec lequel un contrat a été conclu possède par ailleurs une autre nationalité.</ANA>
<ANA ID="9B"> 36-12 Le décret n° 69-697 du 18 juin 1969 du 18 juin 1969 portant fixation du statut des agents contractuels de l'Etat et des établissements publics de l'Etat à caractère administratif, de nationalité française, en service à l'étranger est applicable aux contrats conclus avec des ressortissants français pour pourvoir les emplois qu'il vise. Les litiges relatifs à ces contrats relèvent, dès lors, de la compétence de la juridiction administrative. Est sans incidence la circonstance que le ressortissant français avec lequel un contrat a été conclu possède par ailleurs une autre nationalité.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
