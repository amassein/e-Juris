<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030223883</ID>
<ANCIEN_ID>JG_L_2015_02_000000373673</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/22/38/CETATEXT000030223883.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème / 5ème SSR, 11/02/2015, 373673</TITRE>
<DATE_DEC>2015-02-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>373673</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème / 5ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Pauline Pannier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Keller</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:373673.20150211</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 3 décembre 2013 au secrétariat du contentieux du Conseil d'Etat, présentée par la SA Aubert France, dont le siège est 4 rue de la Ferme, à Cernay (68700), représentée par son président directeur général en exercice ; la société demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite résultant du silence gardé par la Commission nationale d'aménagement commercial sur son recours dirigé contre la décision du 23 septembre 2010 par laquelle la commission départementale d'aménagement commercial de l'Essonne a autorisé la SCI GVM à étendre un ensemble commercial par la création d'un magasin de 2 222 m² de surface de vente à Fleury-Mérogis (Essonne) ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement de la somme de 5 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de commerce ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Pauline Pannier, auditeur,  <br/>
<br/>
              - les conclusions de M. Rémi Keller, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que, par décision du 23 septembre 2010, la commission départementale d'aménagement commercial de l'Essonne a accordé à la SCI GVM l'autorisation préalable requise en vue d'étendre un ensemble commercial par la création d'un magasin de 2 222 m² de surface de vente à Fleury-Mérogis ; que la SA Aubert France a formé contre cette décision, devant la Commission nationale d'aménagement commercial, un recours qui a été rejeté par une décision implicite du 12 mars 2011 valant autorisation du projet ; que, par décision du 23 mars 2011, la commission nationale a retiré cette décision du 12 mars 2011 et rejeté la demande d'autorisation ; que, saisi par la SCI GVM, le Conseil d'Etat, statuant au contentieux par une décision du 23 octobre 2013, a annulé la décision du 23 mars 2011 ; que l'annulation de ce retrait a eu pour effet de faire revivre la décision implicite du 12 mars 2011, dont la SA Aubert France demande l'annulation pour excès de pouvoir ;<br/>
<br/>
              Sur la fin de non-recevoir opposée par la SCI GVM :<br/>
<br/>
              2. Considérant qu'aux termes de l'article R. 421-3 du code de justice administrative : " (...) l'intéressé n'est forclos qu'après un délai de deux mois à compter du jour de la notification d'une décision expresse de rejet : (...) / 2° Dans le contentieux de l'excès de pouvoir, si la mesure sollicitée ne peut être prise que par décision ou sur avis des assemblées locales ou de tous autres organismes collégiaux " ; qu'il résulte de ces dispositions que les décisions implicites de la commission nationale, qui est un organisme collégial, ne font pas courir le délai de recours contentieux ; que la requête de la SA Aubert tendant à l'annulation de la décision du 12 mars 2011, qui, en tout état de cause, a été présentée dans le délai de deux mois à compter de la notification de la décision du 23 octobre 2013 par laquelle le Conseil d'Etat, statuant au contentieux, avait annulé la décision du 23 mars 2011, n'est pas tardive ; <br/>
<br/>
              Sur la légalité de la décision attaquée :<br/>
<br/>
              3. Considérant qu'il résulte des dispositions de l'article R. 752-51 du code de commerce qu'il incombe au commissaire du Gouvernement de recueillir et de présenter à la commission nationale les avis de l'ensemble des ministres intéressés avant d'exprimer son propre avis ; qu'il ressort des pièces du dossier que la décision attaquée est intervenue sans que les avis des ministres aient été présentés aux membres de la commission ; que, par suite, sans qu'il soit besoin d'examiner les autres moyens de la requête, la SA Aubert France est fondée à en demander l'annulation ; <br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              4. Considérant que ces dispositions font obstacle à ce que soit mise à la charge de la SA Aubert France, qui n'est pas la partie perdante dans la présente instance, la somme que demande la SCI GVM au titre des frais exposés et non compris dans les dépens ; qu'en revanche, il y a lieu de mettre à la charge de l'Etat le versement à la SA Aubert France de la somme de 2 000 euros au titre de ces dispositions ;<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : La décision du 12 mars 2011 de la Commission nationale d'aménagement commercial est annulée.<br/>
Article 2 : L'Etat versera la somme de 2 000 euros à la SA Aubert France au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : Les conclusions de la SCI GVM présentées sur le fondement des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à la SA Aubert France, à la SCI GVM et à la Commission nationale d'aménagement commercial.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">14-02-01-05-02 COMMERCE, INDUSTRIE, INTERVENTION ÉCONOMIQUE DE LA PUISSANCE PUBLIQUE. RÉGLEMENTATION DES ACTIVITÉS ÉCONOMIQUES. ACTIVITÉS SOUMISES À RÉGLEMENTATION. AMÉNAGEMENT COMMERCIAL. PROCÉDURE. - RAPO EXERCÉ AUPRÈS DE LA CNAC CONTRE UNE AUTORISATION DE LA CDAC - 1) EFFET DU SILENCE DE LA CNAC - DÉCISION IMPLICITE DE REJET DU RECOURS SE SUBSTITUANT À LA DÉCISION DE LA CDAC [RJ1]- 2) CNAC RETIRANT ENSUITE SA DÉCISION IMPLICITE POUR REFUSER EXPLICITEMENT D'AUTORISER LE PROJET - ANNULATION DE CETTE DÉCISION EXPLICITE [RJ2] À LA DEMANDE DU PÉTITIONNAIRE - EFFETS - PROJET BÉNÉFICIANT À NOUVEAU DE L'AUTORISATION IMPLICITE DE LA CNAC.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-06-07-005 PROCÉDURE. JUGEMENTS. EXÉCUTION DES JUGEMENTS. EFFETS D'UNE ANNULATION. - RAPO EXERCÉ AUPRÈS DE LA CNAC CONTRE UNE AUTORISATION DE LA CDAC - 1) EFFET DU SILENCE DE LA CNAC - DÉCISION IMPLICITE DE REJET DU RECOURS SE SUBSTITUANT À LA DÉCISION DE LA CDAC [RJ1]- 2) CNAC RETIRANT ENSUITE SA DÉCISION IMPLICITE POUR REFUSER EXPLICITEMENT D'AUTORISER LE PROJET - ANNULATION DE CETTE DÉCISION EXPLICITE [RJ2] À LA DEMANDE DU PÉTITIONNAIRE - EFFETS - PROJET BÉNÉFICIANT À NOUVEAU DE L'AUTORISATION IMPLICITE DE LA CNAC.
</SCT>
<ANA ID="9A"> 14-02-01-05-02 1) Lorsque la Commission nationale d'aménagement commerciale (CNAC) est saisie d'un recours administratif préalable obligatoire contre une décision d'autorisation de la commission départementale d'aménagement commerciale (CDAC), son silence pendant 4 mois fait naître une décision implicite de rejet du recours se substituant à la décision de la CDAC et valant nouvelle autorisation du projet.... ,,2) Si la CNAC retire ensuite cette décision implicite pour y substituer une décision explicite faisant droit au recours administratif et refusant d'autoriser le projet, l'annulation par le juge de l'excès de pouvoir de cette décision explicite de la CDAC à la demande du pétitionnaire a pour effet de faire revivre la décision implicite autorisant le projet, qui peut également faire l'objet d'un recours pour excès de pouvoir.</ANA>
<ANA ID="9B"> 54-06-07-005 1) Lorsque la Commission nationale d'aménagement commerciale (CNAC) est saisie d'un recours administratif préalable obligatoire contre une décision d'autorisation de la commission départementale d'aménagement commerciale (CDAC), son silence pendant 4 mois fait naître une décision implicite de rejet du recours se substituant à la décision de la CDAC et valant nouvelle autorisation du projet.... ,,2) Si la CNAC retire ensuite cette décision implicite pour y substituer une décision explicite faisant droit au recours administratif et refusant d'autoriser le projet, l'annulation par le juge de l'excès de pouvoir de cette décision explicite de la CDAC à la demande du pétitionnaire a pour effet de faire revivre la décision implicite autorisant le projet, qui peut également faire l'objet d'un recours pour excès de pouvoir.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Cf. CE, 4 juillet 2012, Association de défense des consommateurs du centre-ville de Reims, n°s 352933 et autres, p. 264., ,[RJ2]Comp., pour une hypothèse où le requérant n'a pas intérêt à agir contre une décision de la CNAC en tant qu'elle retire une précédente décision, CE, 23 septembre 2013, Confédération pour les entrepreneurs et la préservation du Pays du Bassin d'Arcachon, n° 359270, T pp. 473-474-787.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
