<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037525345</ID>
<ANCIEN_ID>JG_L_2018_10_000000419229</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/52/53/CETATEXT000037525345.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 24/10/2018, 419229</TITRE>
<DATE_DEC>2018-10-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>419229</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Louise Bréhier</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CECHR:2018:419229.20181024</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par un jugement n° 1800550 du 22 mars 2018, enregistré le 23 mars 2018 au secrétariat du contentieux du Conseil d'Etat, le magistrat désigné par le président du tribunal administratif de Châlons-en-Champagne, avant de statuer sur la demande de M. B...A...tendant à l'annulation de l'arrêté du 15 mars 2018 par lequel le préfet de la Marne l'a assigné à résidence dans le département de la Marne, a décidé, par application des dispositions de l'article L. 113-1 du code de justice administrative, de transmettre le dossier de cette demande au Conseil d'Etat, en soumettant à son examen la question de savoir si, au regard des dispositions de l'article L. 742-4 du code de l'entrée et du séjour des étrangers et du droit d'asile, le recours formé contre un arrêté d'assignation à résidence pris sur le fondement de l'article L. 561-1 de ce code, notifié concomitamment à la décision portant transfert de l'étranger, relève de la compétence d'un magistrat désigné ou d'une formation collégiale, ainsi que la question de savoir quels sont les délais de recours applicables et, le cas échéant, ceux dans lesquels la juridiction doit se prononcer. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le règlement (UE) n° 604/2013 du Parlement européen et du Conseil du 26 juin 2013 ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ; <br/>
              - la loi n° 2015-925 du 29 juillet 2015 ;<br/>
              - le code de justice administrative, notamment son article L. 113-1 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Louise Bréhier, auditrice, <br/>
<br/>
              - les conclusions de M. Guillaume Odinet, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              REND L'AVIS SUIVANT<br/>
<br/>
<br/>
              1. Les articles 20 et suivants du règlement (UE) n° 604/2013 du Parlement européen et du Conseil du 26 juin 2013 établissant les critères et mécanismes de détermination de l'État membre responsable de l'examen d'une demande de protection internationale introduite dans l'un des États membres par un ressortissant de pays tiers ou un apatride fixent les règles selon lesquelles sont organisées les procédures de prise en charge ou de reprise en charge d'un demandeur d'asile par l'Etat membre responsable de l'examen de sa demande d'asile. Ces articles déterminent notamment les conditions dans lesquelles l'Etat sur le territoire duquel se trouve le demandeur d'asile requiert de l'Etat qu'il estime responsable de l'examen de la demande la prise ou la reprise en charge du demandeur d'asile.<br/>
<br/>
              2. Pour la mise en oeuvre de ces dispositions, les articles L. 742-1 et suivants du code de l'entrée et du séjour des étrangers et du droit d'asile organisent la procédure de détermination de l'Etat responsable de la demande d'asile. <br/>
<br/>
              Dans ce cadre, l'article L. 742-3 prévoit que l'étranger dont l'examen de la demande d'asile relève de la responsabilité d'un autre Etat peut faire l'objet d'un transfert vers l'Etat responsable de cet examen. Selon l'article L. 742-5, tel qu'issu de la loi du 29 juillet 2015 relative à la réforme du droit d'asile, l'article L. 551-1 du code de l'entrée et du séjour des étrangers et du droit d'asile, relatif au placement en rétention, et l'article L. 561-2, relatif aux mesures d'assignation à résidence pouvant être prononcées en lieu et place d'un placement en rétention, sont applicables à l'étranger faisant l'objet d'une décision de transfert dès la notification de cette décision. <br/>
<br/>
              L'assignation à résidence prévue à l'article L. 561-2, qui est susceptible d'être prononcée à l'égard d'étrangers qui ne peuvent quitter immédiatement le territoire français mais dont l'éloignement demeure une perspective raisonnable, peut être décidée pour une durée maximale de quarante cinq jours, renouvelable une fois. L'article L. 561-2 mentionne, parmi les cas pouvant conduire au prononcé d'une mesure d'assignation à résidence régie par cet article, le cas de l'étranger qui a fait l'objet d'une décision de transfert en application de l'article L. 742-3. <br/>
<br/>
              Ce cas est, par ailleurs, également mentionné à l'article L. 561-1, s'agissant des mesures d'assignation à résidence régies par cet article, lesquelles sont susceptibles d'être décidées, pour une durée maximale de six mois renouvelable une fois, à l'égard des étrangers qui justifient être dans l'impossibilité de quitter le territoire français ou ne peuvent ni regagner leur pays d'origine ni se rendre dans aucun autre pays.<br/>
<br/>
              3. Pour ce qui concerne la procédure applicable au jugement des recours dirigés contre les décisions de transfert, l'article L. 742-4 du code de l'entrée et du séjour des étrangers et du droit d'asile, issu de la loi du 29 juillet 2015 relative à la réforme de l'asile, distingue selon qu'est ou non intervenue une décision de placement en rétention ou d'assignation à résidence. <br/>
<br/>
              Lorsque la décision de transfert n'est pas assortie d'une telle mesure, le I de l'article L. 742-4 prévoit que l'étranger peut demander l'annulation de la décision de transfert dans un délai de quinze jours à compter de la notification de la décision et qu'il est statué sur cette demande par le président du tribunal administratif, ou le magistrat qu'il délègue, dans un délai de quinze jours. <br/>
<br/>
              En revanche, lorsque la décision de transfert est accompagnée d'une décision de placement en rétention en application de l'article L. 551-1 ou d'une assignation à résidence en application de l'article L. 561-2, notifiées ensemble, le II de l'article L. 742-4 prévoit que l'étranger dispose d'un délai de recours de quarante-huit heures contre l'ensemble de ces décisions. Il appartient alors au président du tribunal administratif, ou au magistrat qu'il délègue, de statuer sur le recours de l'étranger dans un délai de soixante-douze heures et selon la procédure prévue au III de l'article L. 512-1. Il en va de même lorsque la décision de placement en rétention ou d'assignation à résidence intervient au cours de l'instance relative au recours dirigé contre la mesure de transfert. <br/>
<br/>
              4. En adoptant les dispositions du II de l'article L. 742-4 du code de l'entrée et du séjour des étrangers et du droit d'asile, le législateur a organisé une procédure spéciale conduisant le juge administratif à statuer rapidement sur la légalité des mesures d'éloignement que sont les décisions de transfert lorsque les étrangers concernés sont placés en rétention ou assignés à résidence, sans entendre distinguer selon que la mesure d'assignation à résidence a été prise sur le fondement de l'article L. 561-2, normalement applicable, ou se présente comme ayant été prise en application de l'article L. 561-1. <br/>
<br/>
              Ainsi, quand bien même la mesure d'assignation à résidence assortissant une décision de transfert se présenterait comme prise sur le fondement de l'article L. 561-1, la contestation d'une telle mesure, notifiée avec une décision de transfert, doit être faite avant l'expiration du délai de recours de quarante-huit heures et doit être jugée selon la procédure et dans le délai prévus au III de l'article L. 512-1. <br/>
<br/>
<br/>
<br/>
<br/>
              Le présent avis sera notifié au tribunal administratif de Châlons-en-Champagne, à M. A...et au ministre de l'intérieur. <br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">095-02-03 - ASSIGNATION À RÉSIDENCE PRONONCÉE SUR LE FONDEMENT D'UNE DÉCISION DE TRANSFERT (ART. L. 561-2 CESEDA) - RECOURS - PROCÉDURE APPLICABLE - DÉLAI DE RECOURS DE 48H, DÉTERMINÉ AU II DE L'ART. L. 742-4 DU CESEDA, ET PROCÉDURE PRÉVUE AU III DE L'ARTICLE L. 512-1 DU CESEDA, Y COMPRIS LORSQUE LA MESURE D'ASSIGNATION À RÉSIDENCE SE PRÉSENTE COMME AYANT ÉTÉ PRISE EN APPLICATION DE L'ARTICLE L. 561-1 DE CE CODE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">335-01-04-01 ÉTRANGERS. SÉJOUR DES ÉTRANGERS. RESTRICTIONS APPORTÉES AU SÉJOUR. ASSIGNATION À RÉSIDENCE. - RECOURS DIRIGÉ CONTRE UNE ASSIGNATION À RÉSIDENCE PRONONCÉE SUR LE FONDEMENT D'UNE DÉCISION DE TRANSFERT (ART. L. 561-2 CESEDA) - PROCÉDURE APPLICABLE - DÉLAI DE RECOURS DE 48H, DÉTERMINÉ AU II DE L'ART. L. 742-4 DU CESEDA, ET PROCÉDURE PRÉVUE AU III DE L'ARTICLE L. 512-1 DU CESEDA, Y COMPRIS LORSQUE LA MESURE D'ASSIGNATION À RÉSIDENCE SE PRÉSENTE COMME AYANT ÉTÉ PRISE EN APPLICATION DE L'ARTICLE L. 561-1 DE CE CODE.
</SCT>
<ANA ID="9A"> 095-02-03 En adoptant les dispositions du II de l'article L. 742-4 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA), le législateur a organisé une procédure spéciale conduisant le juge administratif à statuer rapidement sur la légalité des mesures d'éloignement que sont les décisions de transfert lorsque les étrangers concernés sont placés en rétention ou assignés à résidence, sans entendre distinguer selon que la mesure d'assignation à résidence a été prise sur le fondement de l'article L. 561-2, normalement applicable, ou se présente comme ayant été prise en application de l'article L. 561-1.... ...Ainsi, quand bien même la mesure d'assignation à résidence assortissant une décision de transfert se présenterait comme prise sur le fondement de l'article L. 561-1, la contestation d'une telle mesure, notifiée avec une décision de transfert, doit être faite avant l'expiration du délai de recours de quarante-huit heures et doit être jugée selon la procédure et dans le délai prévus au III de l'article L. 512-1.</ANA>
<ANA ID="9B"> 335-01-04-01 En adoptant les dispositions du II de l'article L. 742-4 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA), le législateur a organisé une procédure spéciale conduisant le juge administratif à statuer rapidement sur la légalité des mesures d'éloignement que sont les décisions de transfert lorsque les étrangers concernés sont placés en rétention ou assignés à résidence, sans entendre distinguer selon que la mesure d'assignation à résidence a été prise sur le fondement de l'article L. 561-2, normalement applicable, ou se présente comme ayant été prise en application de l'article L. 561-1.... ...Ainsi, quand bien même la mesure d'assignation à résidence assortissant une décision de transfert se présenterait comme prise sur le fondement de l'article L. 561-1, la contestation d'une telle mesure, notifiée avec une décision de transfert, doit être faite avant l'expiration du délai de recours de quarante-huit heures et doit être jugée selon la procédure et dans le délai prévus au III de l'article L. 512-1.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
