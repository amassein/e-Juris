<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031978214</ID>
<ANCIEN_ID>JG_L_2016_02_000000380935</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/97/82/CETATEXT000031978214.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème / 4ème SSR, 03/02/2016, 380935</TITRE>
<DATE_DEC>2016-02-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>380935</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème / 4ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUIN-PALAT, BOUCARD</AVOCATS>
<RAPPORTEUR>Mme Manon Perrière</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Laurence Marion</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2016:380935.20160203</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Rouen de condamner l'Etat à réparer, au titre de la période comprise entre le 1er janvier 2006 et le 30 juin 2011, les préjudices ayant résulté pour lui du refus du concours de la force publique en vue d'exécuter un jugement du 3 avril 1991 du tribunal de grande instance d'Evreux. Par un jugement n° 1102939 du 21 janvier 2014, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par une ordonnance n° 14DA00428 du 22 mai 2014, enregistrée au secrétariat du contentieux du Conseil d'Etat le 4 juin 2014, le président de la cour administrative d'appel de Douai a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, le pourvoi formé contre ce jugement par M.A....<br/>
<br/>
              Par un pourvoi, enregistré le 10 mars 2014 au greffe de la cour administrative d'appel de Douai, un mémoire complémentaire et un mémoire en réplique, enregistrés le 5 septembre 2014 et le 11 janvier 2016 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - le code civil ;<br/>
<br/>
              - la loi n° 91-650 du 9 juillet 1991 ;<br/>
<br/>
              - l'ordonnance n° 2006-936 du 21 avril 2006 ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Manon Perrière, auditeur,  <br/>
<br/>
              - les conclusions de Mme Laurence Marion, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Thouin-Palat, Boucard, avocat de M.A... ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article 16 de la loi du 9 juillet 1991 portant réforme des procédures civiles d'exécution, applicable au litige porté devant les juges du fond : " L'Etat est tenu de prêter son concours à l'exécution des jugements et des autres titres exécutoires. Le refus de l'Etat de prêter son concours ouvre droit à réparation " ; qu'aux termes de l'article 61 de la même loi : " Sauf disposition spéciale, l'expulsion ou l'évacuation d'un immeuble ou d'un lieu habité ne peut être poursuivie qu'en vertu d'une décision de justice ou d'un procès-verbal de conciliation exécutoire et après signification d'un commandement d'avoir à libérer les locaux. (...) " ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article 2210 du code civil, dans sa rédaction issue de l'article 2 de l'ordonnance du 21 avril 2006 réformant la saisie immobilière et dont les dispositions sont désormais codifiées à l'article L. 322-13 du code des procédures civiles d'exécution : " Le jugement d'adjudication constitue un titre d'expulsion à l'encontre du saisi " ; que l'article 25 de cette ordonnance prévoit que ces dispositions entreront en vigueur à la date d'adoption du décret prévu pour son application et au plus tard le 1er janvier 2007 ; qu'il résulte de ces dispositions et de celles de l'article 2 du code civil que la modification apportée par l'ordonnance du 21 avril 2006 à l'article 2210 du code civil n'a eu ni pour objet ni pour effet de conférer la portée d'un titre d'expulsion aux décisions par lesquelles le juge judiciaire a prononcé, avant le 1er janvier 2007, l'adjudication d'un bien vendu sur saisie immobilière ;<br/>
<br/>
              3. Considérant que, pour rejeter la demande de M.A..., tendant à la condamnation de l'Etat à réparer les préjudices ayant résulté pour lui du rejet par le préfet de l'Eure d'une réquisition de la force publique présentée le 23 juin 1992, pour l'exécution d'une décision du 3 avril 1991 de la chambre des saisies immobilières du tribunal de grande instance d'Evreux prononçant à son profit l'adjudication d'un immeuble situé sur le territoire de la commune de Saint-Denis-du-Behan, le tribunal administratif de Rouen a estimé que cette décision ne constituait pas un titre exécutoire, au sens des dispositions de l'article 16 de la loi du 9 juillet 1991 ; que c'est par une appréciation souveraine, exempte de dénaturation, que le tribunal a déduit des termes de la décision du 3 avril 1991 qu'elle ne revêtait pas une telle portée ; que cette décision ayant été rendue sous le régime antérieur à la modification de l'article 2210 du code civil par l'article 2 de l'ordonnance du 21 avril 2006, il résulte de ce qui vient d'être dit au point 2 que le tribunal n'a pas non plus méconnu les dispositions issues de cette ordonnance ; que si M. A... fait valoir qu'il ressortait des pièces du dossier soumis au juge du fond que, par des ordonnances rendues les 13 janvier 1993 et 5 août 2011, le juge des référés du tribunal de grande instance d'Evreux avait affirmé que la décision du 3 avril 1991 faisait obligation aux occupants de quitter les lieux, cette circonstance ne pouvait pas, par elle-même, conférer à cette décision la portée d'un jugement exécutoire ordonnant l'expulsion ; qu'à supposer même que l'ordonnance du 13 janvier 1993 puisse être regardée comme ayant une telle portée, en ce qu'elle rappelle aux occupants sans titre du logement qu'ils doivent quitter les lieux, il est constant que M. A...n'avait pas demandé le concours de la force publique après cette date et que sa demande indemnitaire était présentée au titre du refus qui lui avait été opposé en 1992  ; <br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède que le pourvoi de M. A...doit être rejeté, y compris ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
		Article 1er : Le pourvoi de M. A...est rejeté.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. B...A...et au ministre de l'intérieur.<br/>
		Copie en sera adressée à la garde des sceaux, ministre de la justice.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">37-05-01 JURIDICTIONS ADMINISTRATIVES ET JUDICIAIRES. EXÉCUTION DES JUGEMENTS. CONCOURS DE LA FORCE PUBLIQUE. - POSSIBILITÉ DE LE DEMANDER SUR LE FONDEMENT D'UN JUGEMENT D'ADJUDICATION JUDICIAIRE - DISPOSITIONS ISSUES DE L'ORDONNANCE DU 21 AVRIL 2006 PRÉVOYANT QUE LE JUGEMENT D'ADJUDICATION CONSTITUE UN TITRE D'EXPULSION - APPLICATION DANS LE TEMPS - APPLICATION AUX SEULS JUGEMENTS POSTÉRIEURS AU 1ER JANVIER 2007 [RJ1].
</SCT>
<ANA ID="9A"> 37-05-01 L'article 2210 du code civil, dans sa rédaction issue de l'article 2 de l'ordonnance n° 2006-936 du 21 avril 2006, et dont les dispositions sont désormais codifiées à l'article L. 332-13 du code des procédures civiles d'exécution, prévoit que le jugement d'adjudication constitue un titre d'expulsion à l'encontre du saisi.,,,Il résulte de l'article 25 de l'ordonnance du 21 avril 2006 et de l'article 2 du code civil que la modification ainsi apportée à l'article 2210 du code civil n'a eu ni pour objet ni pour effet de conférer la portée d'un titre d'expulsion aux décisions par lesquelles le juge judiciaire a prononcé, avant le 1er janvier 2007, l'adjudication d'un bien vendu sur saisie immobilière.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Cf., pour l'état du droit antérieur à l'ordonnance du 21 avril 2006, CE, 29 octobre 2007, Ministre de l'intérieur, de la sécurité intérieure et des libertés locales c/ SARL Immobjectifs France, n° 279147, T. p. 930.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
