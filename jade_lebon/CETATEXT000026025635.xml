<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026025635</ID>
<ANCIEN_ID>JG_L_2012_06_000000335398</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/02/56/CETATEXT000026025635.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 4ème sous-sections réunies, 15/06/2012, 335398</TITRE>
<DATE_DEC>2012-06-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>335398</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 4ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOUTET ; SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>Mme Marie Gautier-Melleray</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:335398.20120615</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 8 janvier et 7 avril 2010 au secrétariat du contentieux du Conseil d'Etat, présentés pour l'ETABLISSEMENT PUBLIC LOCAL D'ENSEIGNEMENT AGRICOLE (EPLEA) DE LAVAUR, dont le siège est au Domaine de Flamarens à Lavaur (81500) ; l'établissement demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 08BX01155 du 29 octobre 2009 par lequel la cour administrative d'appel de Bordeaux, sur requête de Mme Sylvie A, a annulé le jugement n° 0403826 du 6 février 2008 du tribunal administratif de Toulouse prononçant l'annulation partielle de la décision du 3 septembre 2004 de la directrice de l'établissement résiliant son contrat de travail et a prononcé l'annulation totale de cette décision ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de Mme A et de faire droit à son appel incident ;<br/>
<br/>
              3°) de mettre à la charge de Mme A la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi n° 84-16 du 11 janvier 1984 ; <br/>
<br/>
              Vu la loi n° 91-647 du 10 juillet 1991 ; <br/>
<br/>
              Vu le décret n° 86-83 du 17 janvier 1986 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie Gautier-Melleray, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les observations à la SCP Boutet, avocat de l'ETABLISSEMENT PUBLIC LOCAL DE L'ENSEIGNEMENT AGRICOLE DE LAVAUR et à la SCP Lyon-Caen, Thiriez, avocat Mme Bernard-Ferrero, <br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Boutet, avocat de l'ETABLISSEMENT PUBLIC LOCAL DE L'ENSEIGNEMENT AGRICOLE DE LAVAUR et à la SCP Lyon-Caen, Thiriez, avocat Mme Bernard-Ferrero ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que Mme A, initialement recrutée en qualité de formateur au lycée professionnel agricole de Lavaur par un contrat à durée déterminée à temps complet, a conclu le 25 juillet 1994 avec cet établissement un contrat à durée indéterminée ; qu'elle a fait l'objet, les 12 février 1999 et 14 mars 2001, de décisions de licenciement qui ont toutes deux été annulées par des jugements du tribunal administratif de Toulouse en date, respectivement, du 10 octobre 2000 et du 28 novembre 2003, devenus définitifs ; que, devant exécuter ce second jugement, la directrice de l'ETABLISSEMENT PUBLIC LOCAL D'ENSEIGNEMENT AGRICOLE (EPLEA) DE LAVAUR, qui a succédé au lycée professionnel agricole, a proposé à Mme A sa réintégration sur un autre poste d'ingénierie de formation et la modification de son contrat en contrat d'une durée d'un an ; que, au motif que l'intéressée avait refusé cette proposition, la directrice a prononcé, le 3 septembre 2004, la résiliation de son contrat à compter du 3 novembre de la même année ; que, par un jugement du 6 février 2008, le tribunal administratif de Toulouse a annulé cette décision uniquement en tant qu'elle prévoyait sa prise d'effet au 3 novembre 2004 ; que, saisie de conclusions de Mme A tendant à l'annulation totale de la décision litigieuse et de conclusions incidentes de l'établissement dirigées contre le jugement dans la seule mesure de cette annulation partielle, la cour administrative d'appel de Bordeaux, par l'arrêt du 29 octobre 2009 contre lequel l'EPLEA DE LAVAUR se pourvoit en cassation, a fait entièrement droit aux conclusions de Mme A ;<br/>
<br/>
              Sans qu'il soit besoin d'examiner les autres moyens du pourvoi ;<br/>
<br/>
              Considérant que, devant la cour administrative d'appel, l'établissement faisait valoir que Mme A s'était opposée à tout engagement à durée déterminée et que cette circonstance était à elle seule de nature à justifier son licenciement ; qu'en ne répondant pas à ce moyen, qui n'était pas inopérant, la cour a entaché son arrêt d'une irrégularité de nature à en justifier l'annulation ;<br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative en statuant sur l'appel de Mme A et sur l'appel incident de l'EPLEA DE LAVAUR ;<br/>
<br/>
              Sur l'appel de Mme A :<br/>
<br/>
              Considérant, d'une part, que, sauf s'il présente un caractère fictif ou frauduleux, le contrat de recrutement d'un agent contractuel de droit public crée des droits au profit de celui-ci ; que, lorsque le contrat est entaché d'une irrégularité, notamment parce qu'il méconnaît une disposition législative ou réglementaire applicable à la catégorie d'agents dont relève l'agent contractuel en cause, l'administration est tenue de proposer à celui-ci une régularisation de son contrat afin que son exécution puisse se poursuivre régulièrement ; que si le contrat ne peut être régularisé, il appartient à l'administration, dans la limite des droits résultant du contrat initial, de proposer à l'agent un emploi de niveau équivalent, ou, à défaut d'un tel emploi et si l'intéressé le demande, tout autre emploi, afin de régulariser sa situation ; que, si l'intéressé refuse la régularisation de son contrat ou si la régularisation de sa situation, dans les conditions précisées ci-dessus, est impossible, l'administration est tenue de le licencier ;<br/>
<br/>
              Considérant, d'autre part, qu'aux termes de l'article 4 de la loi du 11 janvier 1984 portant dispositions statutaires relatives à la fonction publique de l'Etat, dans sa rédaction en vigueur à la date de la décision de licenciement contestée : " Par dérogation au principe énoncé à l'article 3 du titre Ier du statut général, des agents contractuels peuvent être recrutés dans les cas suivants : (...) / 2° Pour les emplois de catégorie A et, dans les représentations de l'Etat à l'étranger, des autres catégories, lorsque la nature des fonctions ou les besoins des services le justifient. / Les agents ainsi recrutés sont engagés par des contrats d'une durée maximale de trois ans qui ne peuvent être renouvelés que par reconduction expresse " ;<br/>
<br/>
              Considérant qu'au regard de ces dispositions, qui lui étaient alors applicables, le contrat du 25 juillet 1994 liant Mme A à l'établissement était entaché d'irrégularité en tant qu'il était conclu pour une durée indéterminée ; que l'établissement était en conséquence tenu d'en proposer la régularisation, qui impliquait nécessairement sa transformation en contrat à durée déterminée, dans la mesure où le maintien de l'intéressée sur son emploi demeurait par ailleurs possible dans le respect des autres prescriptions législatives et réglementaires relatives aux agents contractuels, telles que celles tenant à la nature des fonctions et aux besoins du service ; qu'à défaut, il était tenu de lui proposer, dans les conditions précisées ci-dessus, un autre emploi dans la mesure où il en existait qu'il fût possible de pourvoir par un contrat à durée déterminée et dans le respect de ces mêmes prescriptions ; que le refus de l'intéressée de consentir à une modification de son contrat nécessaire à la poursuite régulière de son exécution ou d'occuper le ou les seuls emplois qui pouvaient lui être régulièrement proposés mettait l'établissement dans l'obligation de prononcer son licenciement ;<br/>
<br/>
              Considérant, en premier lieu, que la décision de licenciement notifiée à Mme A mentionne qu'elle est fondée sur l'irrégularité du contrat à durée indéterminée au regard des dispositions législatives applicables et sur le refus de l'intéressée de conclure un contrat à durée déterminée ; que le moyen tiré de ce que cette décision ne serait pas motivée manque ainsi en fait ;<br/>
<br/>
              Considérant, en second lieu, qu'il résulte de ce qui précède que, contrairement à ce que soutient Mme A, l'administration n'a pas subordonné sa réintégration en exécution du jugement du 28 novembre 2003 à une condition illégale en lui demandant de conclure un contrat à durée déterminée ; qu'il est constant que l'intéressée a refusé que ses relations avec l'établissement se poursuivent dans le cadre d'un tel contrat, quelle qu'en fût la durée ; que, ce faisant, elle s'est opposée tant à la régularisation nécessaire du contrat du 25 juillet 1994 qu'à la conclusion de tout autre contrat lui permettant d'occuper un emploi dans l'établissement ; que la directrice de ce dernier se trouvait, du fait de ce refus, dans l'obligation de la licencier ; que, dès lors, Mme A ne saurait soutenir qu'elle a fait l'objet d'une sanction déguisée, ni invoquer le bénéfice du congé pour grave maladie institué par l'article 13 du décret du 17 janvier 1986 relatif aux dispositions générales applicables aux agents non titulaires de l'Etat ;<br/>
<br/>
              Sur l'appel incident de l'EPLEA DE LAVAUR :<br/>
<br/>
              Considérant qu'aux termes de l'article 46 du même décret : " L'agent recruté pour une durée indéterminée ainsi que l'agent qui, engagé à terme fixe, est licencié avant le temps fixé, a droit à un préavis qui est de : (...) deux mois pour ceux qui ont au moins deux ans de services. (...) " ; que ces dispositions sont applicables au licenciement de Mme A, alors même que le contrat du 25 juillet 1994 aurait prévu une durée de préavis différente ; que, dès lors, la décision de licenciement du 3 septembre 2004 a légalement fixé sa date d'effet au 3 novembre de la même année ;<br/>
<br/>
              Considérant qu'il résulte de tout ce qui précède que l'EPLEA DE LAVAUR est fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Toulouse a annulé la décision de licenciement du 3 septembre 2004 en tant qu'elle prévoyait sa prise d'effet au 3 novembre de la même année ; qu'en revanche, Mme A n'est pas fondée à soutenir que c'est à tort qu'il n'en a pas prononcé l'annulation totale ; <br/>
<br/>
              Considérant que l'EPLEA DE LAVAUR n'étant pas, dans la présente instance, la partie perdante, les conclusions présentées par Mme A et par son avocat au titre  des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ne peuvent qu'être rejetées ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de mettre les frais exposés par l'établissement à la charge de Mme A ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 29 octobre 2009 de la cour administrative d'appel de Bordeaux est annulé.<br/>
<br/>
Article 2 : L'article 1er du jugement du 6 février 2008 du tribunal administratif de Toulouse est annulé.<br/>
<br/>
Article 3 : La requête de Mme A devant la cour administrative d'appel de Bordeaux et ses conclusions devant le tribunal administratif de Toulouse tendant à l'annulation de la décision de licenciement du 3 septembre 2004, en tant qu'elle prévoit sa prise d'effet au 3 novembre de la même année, sont rejetées.<br/>
<br/>
Article 4 : Les conclusions présentées par les parties au titre de l'article L. 761-1 du code de justice administrative et de l'article 37 de la loi du 10 juillet 1991 sont rejetées.<br/>
<br/>
Article 5 : La présente décision sera notifiée à l'ETABLISSEMENT PUBLIC LOCAL D'ENSEIGNEMENT AGRICOLE DE LAVAUR et à Mme Sylvie A.<br/>
Copie en sera adressée pour information au ministre de l'agriculture et de l'agroalimentaire.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-12-03-01 FONCTIONNAIRES ET AGENTS PUBLICS. AGENTS CONTRACTUELS ET TEMPORAIRES. FIN DU CONTRAT. LICENCIEMENT. - DURÉE DU PRÉAVIS APPLICABLE AU LICENCIEMENT DES AGENTS CONTRACTUELS DE L'ETAT (ART. 46 DU DÉCRET DU 17 JANVIER 1986) - DURÉE APPLICABLE MÊME DANS LE CAS OÙ LE CONTRAT PRÉVOIT UNE DURÉE DIFFÉRENTE.
</SCT>
<ANA ID="9A"> 36-12-03-01 La durée de préavis de licenciement applicable au licenciement d'un agent contractuel de l'Etat est celle résultant de l'application de l'article 46 du décret n° 86-83 du 17 janvier 1986 relatif aux dispositions générales applicables aux agents non titulaires de l'Etat pris pour l'application de l'article 7 de la loi n° 84-16 du 11 janvier 1984 portant dispositions statutaires relatives à la fonction publique de l'Etat, quand bien même son contrat prévoirait une durée différente.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
