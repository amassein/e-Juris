<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038713934</ID>
<ANCIEN_ID>JG_L_2019_07_000000419287</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/71/39/CETATEXT000038713934.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 6ème chambres réunies, 01/07/2019, 419287</TITRE>
<DATE_DEC>2019-07-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>419287</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP POTIER DE LA VARDE, BUK LAMENT, ROBILLOT ; SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>Mme Louise Cadin</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:419287.20190701</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Le syndicat national des discothèques et lieux de loisirs, l'association " Club des discothèques pointe de Bretagne " et les sociétés " Guear ", " Club et spectacles Artero ", " Le 29 ", " Le Platinum " et " Nazka ", ont demandé au tribunal administratif de Rennes d'annuler la décision du sous-préfet de Morlaix du 30 septembre 2013 accordant à Mme B... l'autorisation d'exploiter un dancing-discothèque avec licence IV au 83 quai Tabarly à Brest. <br/>
<br/>
              Par un jugement n° 1304493 du 13 juillet 2016, le tribunal administratif a annulé la décision du 30 septembre 2013 du sous-préfet de Morlaix. <br/>
<br/>
              Par un arrêt n° 16NT03091 du 26 janvier 2018, la cour administrative d'appel de Nantes a, sur la requête de MmeB..., annulé le jugement du 30 septembre 2013 du tribunal administratif de Rennes et rejeté la demande présentée devant ce tribunal par le syndicat national des discothèques et lieux de loisirs. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et deux nouveau mémoires, enregistrés les 26 mars 2018, 27 juin 2018, 6 mai 2019 et 6 juin 2019, au secrétariat du contentieux du Conseil d'Etat, la société " Club et spectacles Artero ", la société Guear et l'association " Club des discothèques Pointe de Bretagne " demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de MmeB... ;<br/>
<br/>
              3°) de mettre à la charge de Mme B...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la santé publique ;<br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Louise Cadin, auditrice,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Potier de la Varde, Buk Lament, Robillot, avocat de la société " Club et spectacles Artero " et autres et à la SCP Lyon-Caen, Thiriez, avocat de MmeB....<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que, par une décision du 30 septembre 2013, le sous-préfet de Morlaix a accordé à Mme A... B...l'autorisation de créer et d'exploiter une discothèque au 83 quai Tabarly à Brest. Le tribunal administratif de Rennes a annulé cette décision. La société " Club et spectacles Artero " et autres se pourvoient en cassation contre l'arrêt par lequel la cour administrative d'appel de Nantes a annulé ce jugement et rejeté la requête dirigée contre l'arrêté attaqué. <br/>
<br/>
              2. Aux termes de l'article L. 3335-1 du code de la santé publique : " Le représentant de l'Etat dans le département peut prendre des arrêtés pour déterminer sans préjudice des droits acquis, les distances auxquelles les débits de boissons à consommer sur place ne peuvent être établis autour des édifices et établissements suivants dont l'énumération est limitative : (...) 7° Casernes, camps, arsenaux et tous bâtiments occupés par le personnel des armées de terre, de mer et de l'air ; (...) / Ces distances sont calculées selon la ligne droite au sol reliant les accès les plus rapprochés de l'établissement protégé et du débit de boissons. Dans ce calcul, la dénivellation en dessus et au-dessous du sol, selon que le débit est installé dans un édifice en hauteur ou dans une infrastructure en sous-sol, doit être prise en ligne de compte. / L'intérieur des édifices et établissements en cause est compris dans les zones de protection ainsi déterminées (...) ". Il résulte de ces dispositions, éclairées par les travaux préparatoires de la loi n°2007-1787 du 20 décembre 2007 relative à la simplification du droit, que la distance entre un établissement protégé et un débit de boissons se mesure sur les voies de circulation ouvertes au public, suivant l'axe de ces dernières, entre et à l'aplomb des portes d'entrée ou de sortie les plus rapprochées de l'établissement protégé et du débit de boissons, la distance obtenue étant augmentée de la longueur de la ligne droite au sol entre les portes d'accès et l'axe de la voie et, le cas échéant, de la différence de hauteur entre le niveau du sol et celui du débit de boissons. <br/>
<br/>
              3. Il ressort des pièces du dossier soumis aux juges du fond que, par application des dispositions de l'article L. 49 du code des débits de boisson, reprises aujourd'hui par les dispositions citées ci-dessus de l'article L. 3335-1 du code de la santé publique, le préfet du Finistère a pris un arrêté du 20 septembre 1991 portant interdiction de créer ou de transférer des débits de boissons à consommer sur place autour de certains édifices et établissements. Pour la création et le transfert de débits de boissons, cet arrêté prescrit, dans les villes de plus de 10 000 habitants, au nombre desquelles figure Brest, une distance minimale à respecter de 200 mètres autour des " casernes, camps, arsenaux et tous bâtiments occupés par le personnel des armées de terre et de mer ".<br/>
<br/>
              4. En premier lieu, pour retenir que l'établissement dont l'exploitation a été autorisée par la décision attaquée n'est pas situé à une distance inférieure à 200 mètres des bâtiments protégés sur le fondement des dispositions citées ci-dessus du code de la santé publique, la cour administrative d'appel a appliqué les règles énoncées au point 2 ci-dessus. Les requérantes ne sont, par suite, pas fondées à soutenir qu'elle aurait commis une erreur de droit en retenant une méthode de calcul qui ne serait pas conforme  aux prescriptions de l'article L. 3335-1 du code de la santé publique.<br/>
<br/>
              5. En deuxième lieu, en relevant que la distance ainsi calculée entre l'établissement et, d'une part, la porte de l'héliport de la marine et, d'autre part, l'immeuble abritant le service historique de la défense était supérieure à 200 mètres, la cour administrative d'appel a suffisamment motivé sa décision. Elle n'était, en particulier, pas tenue de se prononcer sur la distance entre l'établissement et le club historique de la marine dont il ressortait tant des pièces du dossier qui lui était soumis que du jugement du tribunal administratif de Rennes, non contesté sur ce point, qu'il se situe dans le même immeuble que le service historique de la défense sur lequel elle se prononçait. Elle n'était pas davantage tenue de se prononcer sur la distance entre l'établissement et la porte Surcouf de l'arsenal militaire de Brest, dont il ressortait du jugement, non contesté sur ce point, qu'elle était plus éloignée de l'établissement que les bâtiments sur lesquels elle se prononçait, le jugement la situant à plus de 200 mètres de l'établissement alors qu'il faisait application d'une méthode conduisant à des mesures inférieures à celles calculées suivant la méthode qu'elle retenait. <br/>
<br/>
              6. Il résulte de tout ce qui précède que les requérantes ne sont pas fondées à demander l'annulation de l'arrêt litigieux. Par suite, les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de Mme B..., qui n'est pas, dans la présence instance, la partie perdante. Il y a en revanche lieu, dans les circonstances de l'espèce, de mettre à la charge des sociétés requérantes la somme de 3 000 euros à verser à Mme B....<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le pourvoi de la société " Club et spectacles Artero " et autres est rejeté.<br/>
Article 2 : La société " Club et spectacles Artero " et autres verseront la somme de 3 000 euros à Mme B.... <br/>
Article 3 : La présente décision sera notifiée à la société " Clubs et spectacles Artero ", première dénommée, pour l'ensemble des requérants, au ministre de l'intérieur et à Mme A...B.... <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">49-05-04 POLICE. POLICES SPÉCIALES. POLICE DES DÉBITS DE BOISSONS. - DISTANCE D'IMPLANTATION DES DÉBITS DE BOISSON PAR RAPPORT AUX ÉTABLISSEMENTS PROTÉGÉS (ART. L. 3335-1 DU CSP) - MODALITÉS DE CALCUL [RJ1].
</SCT>
<ANA ID="9A"> 49-05-04 Il résulte des dispositions de l'article L. 3335-1 du code de la santé publique (CSP), éclairées par les travaux préparatoires de la loi n° 2007-1787 du 20 décembre 2007, que la distance entre un établissement protégé et un débit de boissons se mesure sur les voies de circulation ouvertes au public, suivant l'axe de ces dernières, entre et à l'aplomb des portes d'entrée ou de sortie les plus rapprochées de l'établissement protégé et du débit de boissons, la distance obtenue étant augmentée de la longueur de la ligne droite au sol entre les portes d'accès et l'axe de la voie et, le cas échéant, de la différence de hauteur entre le niveau du sol et celui du débit de boissons.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., sous l'empire de la législation antérieure, CE, 10 juillet 1995, SARL La locomotive, n° 148976, T. p. 947.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
