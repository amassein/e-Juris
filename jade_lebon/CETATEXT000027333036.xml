<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027333036</ID>
<ANCIEN_ID>JG_L_2013_04_000000360598</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/33/30/CETATEXT000027333036.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème et 2ème sous-sections réunies, 19/04/2013, 360598</TITRE>
<DATE_DEC>2013-04-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>360598</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème et 2ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ ; SCP BORE, SALVE DE BRUNETON ; SCP PEIGNOT, GARREAU, BAUER-VIOLAS</AVOCATS>
<RAPPORTEUR>M. François Lelièvre</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:360598.20130419</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 29 juin et 1er octobre 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour le centre hospitalier d'Alès-Cévennes, dont le siège est 811 avenue du docteur Jean Goubert, BP 20139 à Alès Cedex (30103) ; le centre hospitalier demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 11MA03953 du 18 juin 2012 par lequel la cour administrative d'appel de Marseille a rejeté sa requête tendant à l'annulation du jugement n° 1102343 du 22 septembre 2011 par lequel le tribunal administratif de Nîmes a rejeté sa demande tendant à la récusation de M. B...A..., désigné à la demande de la société Sogea Sud et de la société Richard Satem comme expert par ordonnance n° 1101256 du 22 juin 2011 du juge des référés du tribunal administratif, avec mission notamment de déterminer les causes des retards du chantier de construction du nouvel hôpital d'Alès ; <br/>
<br/>
              2°) de mettre à la charge des sociétés Sogea Sud et Richard Satem le versement de la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ainsi que la somme de 35 euros acquittée au titre de la contribution pour l'aide juridique ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. François Lelièvre, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Lyon-Caen, Thiriez, avocat du centre hospitalier d'Alès-Cévennes, de la SCP Peignot, Garreau, Bauer-Violas, avocat de la société Sogea Sud et de la société Richard Satem, et de la SCP Boré, Salve de Bruneton, avocat de M.A...,<br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Lyon-Caen, Thiriez, avocat du centre hospitalier d'Alès-Cévennes, à la SCP Peignot, Garreau, Bauer-Violas, avocat de la société Sogea Sud et de la société Richard Satem, et à la SCP Boré, Salve de Bruneton, avocat de M. A...;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 721-1 du code de justice administrative : " La récusation d'un membre de la juridiction est prononcée, à la demande d'une partie, s'il existe une raison sérieuse de mettre en doute son impartialité. " ; qu'aux termes de l'article R. 621-2 du même code : " Les experts ou sapiteurs mentionnés à l'article R. 621-2 peuvent être récusés pour les mêmes causes que les juges. " ; qu'aux termes de l'article R. 621-6-4 : " Si l'expert acquiesce à la demande de récusation, il est aussitôt remplacé. / Dans le cas contraire, la juridiction, par une décision non motivée, se prononce sur la demande, après audience publique dont l'expert et les parties sont avertis. " ;<br/>
<br/>
              2. Considérant, en premier lieu, que si les dispositions de l'article R. 621-6-4 du code de justice administrative n'ont pas écarté l'application de la règle générale de motivation des décisions juridictionnelles, rappelée à l'article L. 9 de ce code, elles permettent au juge, pour tenir compte des exigences d'une bonne administration de la justice ainsi que des particularités qui s'attachent à une demande de récusation, laquelle est notamment susceptible, selon la teneur de l'argumentation du requérant, de porter atteinte à la vie privée de l'expert ou de mettre en cause sa probité ou sa réputation professionnelle, d'adapter la motivation de sa décision, au regard de ces considérations, en se limitant, le cas échéant à énoncer qu'il y a lieu, ou qu'il n'y a pas lieu, de faire droit à la demande ; que ces dispositions n'imposent pas au juge d'expliciter dans sa décision les raisons pour lesquelles il estime devoir user de cette faculté de limiter ainsi la motivation de sa décision ; que, par suite, la cour administrative d'appel n'a pas commis d'erreur de droit en estimant que le jugement du tribunal administratif de Nîmes du 22 septembre 2011, lequel indiquait qu'il n'y avait pas lieu dans les circonstances de l'espèce de faire droit à la demande de récusation, était suffisamment motivé ;<br/>
<br/>
              3. Considérant, en deuxième lieu, que l'action en récusation d'un expert ne porte ni sur des droits et obligations de caractère civil, ni sur une accusation en matière pénale, au sens du paragraphe 1 de l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; que la décision statuant sur cette action n'entrant pas dans le champ d'application de ces stipulations, le moyen tiré de ce que l'arrêt attaqué les aurait méconnues ne peut qu'être écarté ;<br/>
<br/>
              4. Considérant, en dernier lieu, qu'il appartient au juge, saisi d'un moyen mettant en doute l'impartialité d'un expert, de rechercher si, eu égard à leur nature, à leur intensité, à leur date et à leur durée, les relations directes ou indirectes entre cet expert et l'une ou plusieurs des parties au litige sont de nature à susciter un doute sur son impartialité ; qu'en particulier, doivent en principe être regardées comme suscitant un tel doute les relations professionnelles s'étant nouées ou poursuivies durant la période de l'expertise ;<br/>
<br/>
              5. Considérant que le centre hospitalier a fait valoir devant les juges du fond que la société BEC Construction SA, dont M. A...était alors le directeur, avait participé en 2002 à un groupement d'entreprise avec la société Sogea, partie au litige faisant l'objet de l'expertise, en vue de l'attribution d'un marché ; que toutefois, eu égard tant à l'ancienneté des faits en cause à la date de la désignation de M. A...en tant qu'expert, le 22 juin 2011, qu'à la nature et à l'intensité des relations alléguées, dès lors qu'il ressort des pièces du dossier soumis aux juges du fond que M. A... n'était plus dirigeant de la société BEC Construction SA lors de la période d'exécution du marché, la cour n'a pas inexactement qualifié les éléments soumis à son examen en estimant, par une décision suffisamment motivée, que son parcours professionnel dans le secteur du bâtiment et des travaux publics ne révélait aucun élément actuel qui ferait obstacle à ce qu'il accomplisse la mission confiée par le juge des référés ; <br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que le pourvoi du centre hospitalier d'Alès-Cévennes doit être rejeté, y compris les conclusions qu'il présente au titre des dispositions de l'article L. 761-1 du code de justice administrative ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à sa charge la somme de 3 000 euros à verser, d'une part, à M. A...et, d'autre part, aux sociétés SNC Sogea Sud et Richard Satem, au même titre ;<br/>
<br/>
              7. Considérant que, dans les circonstances de l'espèce, il y a lieu de laisser la contribution pour l'aide juridique à la charge du centre hospitalier d'Alès-Cévennes ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi du centre hospitalier d'Alès-Cévennes est rejeté.<br/>
Article 2 : Le centre hospitalier d'Alès-Cévennes versera la somme de 3 000 euros, d'une part, à M. A... et, d'autre part, aux sociétés SNC Sogea Sud et Richard Satem, en application des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée au centre hospitalier d'Alès-Cévennes, à la SNC Sogea Sud, à la SA Richard Satem et à M. B...A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-04-02-02 PROCÉDURE. INSTRUCTION. MOYENS D'INVESTIGATION. EXPERTISE. - 1) MISE EN DOUTE DE L'IMPARTIALITÉ D'UN EXPERT - CRITÈRES D'APPRÉCIATION PAR LE JUGE - 2) ESPÈCE - EXPERT AYANT ÉTÉ DIRECTEUR D'UNE SOCIÉTÉ AYANT PARTICIPÉ À UN GROUPEMENT D'ENTREPRISES AVEC UNE SOCIÉTÉ PARTIE AU LITIGE FAISANT L'OBJET DE L'EXPERTISE, EN VUE DE L'ATTRIBUTION D'UN MARCHÉ - POSSIBILITÉ POUR L'EXPERT D'ACCOMPLIR SA MISSION - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-05-02 PROCÉDURE. INCIDENTS. RÉCUSATION. - 1) MISE EN DOUTE DE L'IMPARTIALITÉ D'UN EXPERT - CRITÈRES D'APPRÉCIATION PAR LE JUGE - 2) ESPÈCE - EXPERT AYANT ÉTÉ DIRECTEUR D'UNE SOCIÉTÉ AYANT PARTICIPÉ À UN GROUPEMENT D'ENTREPRISES AVEC UNE SOCIÉTÉ PARTIE AU LITIGE FAISANT L'OBJET DE L'EXPERTISE, EN VUE DE L'ATTRIBUTION D'UN MARCHÉ - POSSIBILITÉ POUR L'EXPERT D'ACCOMPLIR SA MISSION - EXISTENCE.
</SCT>
<ANA ID="9A"> 54-04-02-02 1) Il appartient au juge, saisi d'un moyen mettant en doute l'impartialité d'un expert, de rechercher si, eu égard à leur nature, à leur intensité, à leur date et à leur durée, les relations directes ou indirectes entre cet expert et l'une ou plusieurs des parties au litige sont de nature à susciter un doute sur son impartialité. En particulier, doivent en principe être regardées comme suscitant un tel doute les relations professionnelles s'étant nouées ou poursuivies durant la période de l'expertise.,,,2) En l'espèce, expert ayant été par le passé directeur d'une société ayant participé à un groupement d'entreprises avec une société partie au litige faisant l'objet de l'expertise, en vue de l'attribution d'un marché. Eu égard à l'ancienneté des faits en cause à la date de la désignation de l'expert, et à la nature et à l'intensité des relations alléguées dès lors que l'expert n'était plus dirigeant de la société lors de la période d'exécution du marché, absence d'obstacle à ce qu'il accomplisse sa mission.</ANA>
<ANA ID="9B"> 54-05-02 1) Il appartient au juge, saisi d'un moyen mettant en doute l'impartialité d'un expert, de rechercher si, eu égard à leur nature, à leur intensité, à leur date et à leur durée, les relations directes ou indirectes entre cet expert et l'une ou plusieurs des parties au litige sont de nature à susciter un doute sur son impartialité. En particulier, doivent en principe être regardées comme suscitant un tel doute les relations professionnelles s'étant nouées ou poursuivies durant la période de l'expertise.,,,2) En l'espèce, expert ayant été par le passé directeur d'une société ayant participé à un groupement d'entreprises avec une société partie au litige faisant l'objet de l'expertise, en vue de l'attribution d'un marché. Eu égard à l'ancienneté des faits en cause à la date de la désignation de l'expert, et à la nature et à l'intensité des relations alléguées dès lors qu'il ressort des pièces du dossier soumis aux juges du fond que l'expert n'était plus dirigeant de la société lors de la période d'exécution du marché, absence d'obstacle à ce qu'il accomplisse sa mission.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
