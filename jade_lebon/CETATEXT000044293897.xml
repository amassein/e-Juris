<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044293897</ID>
<ANCIEN_ID>JG_L_2021_11_000000448092</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/29/38/CETATEXT000044293897.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 05/11/2021, 448092</TITRE>
<DATE_DEC>2021-11-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>448092</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>CABINET ROUSSEAU ET TAPIE</AVOCATS>
<RAPPORTEUR>M. Guillaume Leforestier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Marc Pichon de Vendeuil</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:448092.20211105</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. N... B... a demandé au tribunal administratif de Nantes d'annuler la décision implicite par laquelle le ministre de la défense a rejeté son recours administratif préalable obligatoire du 1er avril 2016 contre le titre de perception du 16 février 2016 mettant à sa charge un trop perçu de solde de 13 400 euros et le titre d'annulation du 17 juin 2016 ramenant ce montant à 13 068,40 euros et, subsidiairement, de condamner l'Etat à lui verser une indemnité égale au montant réclamé ou à lui en accorder la remise gracieuse.<br/>
<br/>
              Il a, en outre, demandé au tribunal administratif d'annuler la décision implicite par laquelle le ministre de la défense a rejeté son recours administratif préalable obligatoire du 10 août 2016 contre la décision du centre expert des ressources humaines de Nancy du 23 mai 2016 lui réclamant un trop-perçu de solde d'un montant de 13 068,40 euros, subsidiairement de condamner l'Etat à lui verser une indemnité égale au montant réclamé ou à lui en accorder la remise gracieuse, et de condamner l'Etat à lui verser une somme de 3 000 euros en réparation de son préjudice moral et des troubles dans les conditions d'existence qu'il estime avoir subis.<br/>
<br/>
              Par un jugement n°s 1609319, 1701258 du 8 janvier 2019, le tribunal a annulé le titre de perception du 16 février 2016 et le titre d'annulation du 17 juin 2016 en tant qu'ils mettaient à la charge de M. B... une somme supérieure à 9 028,22 euros, condamné l'Etat à lui verser la somme de 1 500 euros en réparation de ses préjudices et rejeté le surplus de ses conclusions.<br/>
<br/>
              Par un arrêt n° 19NT00991 du 23 octobre 2020, la cour administrative d'appel de Nantes a, sur appel de M. B..., annulé les mêmes titres et déchargé l'intéressé de l'obligation de payer la somme totale de 13 068,40 euros.<br/>
<br/>
              Par un pourvoi, enregistré le 22 décembre 2020 au secrétariat du contentieux du Conseil d'Etat, la ministre des armées demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la requête de M. B....<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la défense ;<br/>
              - la loi n° 2000-321 du 12 avril 2000 ;<br/>
              - la loi n° 2011-1978 du 28 décembre 2011 ;<br/>
              - le décret n° 2012-1246 du 7 novembre 2012 ; <br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Guillaume Leforestier, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Marc Pichon de Vendeuil, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, au cabinet Rousseau et Tapie, avocat de M. B... ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. B..., militaire radié des contrôles au terme de son contrat d'engagement le 27 avril 2013, a été avisé par un courrier du 10 juin 2015 du centre expert des ressources humaines et de la solde de Nancy, relevant du ministère de la défense, de l'existence d'un trop-versé de solde d'un montant de 13 399,85 euros et de l'émission d'un titre de perception destiné à recouvrer cette somme. Par un titre de perception émis le 16 février 2016, le directeur régional des finances publiques des Pays de La Loire a mis à la charge de M. B... une somme de 13 400 euros. L'intéressé a formé, auprès du comptable public, une réclamation contre ce titre le 1er avril 2016. Par courrier du 23 mai 2016, le centre expert des ressources humaines et de la solde de Nancy a informé M. B... qu'il y avait lieu de minorer la somme mise à sa charge et un titre d'annulation a été émis le 17 juin 2016, ramenant la somme réclamée à 13 068,40 euros. Par un jugement du 8 janvier 2019, le tribunal administratif de Nantes a ramené à 9 028,22 euros la somme due par M. B.... La ministre des armées se pourvoit en cassation contre l'arrêt par lequel la cour administrative d'appel de Nantes a, sur appel de M. B..., annulé les titres de perception et d'annulation des 16 février et 17 juin 2016 et déchargé l'intéressé de l'obligation de payer la somme de 13 068,40 euros au motif que cette somme était prescrite.<br/>
<br/>
              Sur le pourvoi :<br/>
<br/>
              2. Aux termes de l'article 37-1 de la loi du 12 avril 2000 relative aux droits des citoyens dans leurs relations avec les administrations, dans sa rédaction issue du I de l'article 94 de la loi de finances rectificative du 28 décembre 2011: " Les créances résultant de paiements indus effectués par les personnes publiques en matière de rémunération de leurs agents peuvent être répétées dans un délai de deux années à compter du premier jour du mois suivant celui de la date de mise en paiement du versement erroné, y compris lorsque ces créances ont pour origine une décision créatrice de droits irrégulière devenue définitive (...) ".<br/>
<br/>
              3. En vertu de l'article L. 4211-1 du code de la défense, la réserve militaire, qui a pour objet " de renforcer les capacités des forces armées et formations rattachées dont elle est une des composantes pour la protection du territoire national, comme dans le cadre des opérations extérieures, d'entretenir l'esprit de défense et de contribuer au maintien du lien entre la Nation et son armée ", est notamment constituée d'une réserve opérationnelle comprenant les anciens militaires soumis à l'obligation de disponibilité. Aux termes de l'article L. 4231-1 du même code : " Sont soumis à l'obligation de disponibilité :/ (...) / 2° Les anciens militaires de carrière ou sous contrat et les personnes qui ont accompli un volontariat dans les armées, dans la limite de cinq ans à compter de la fin de leur lien au service ". Aux termes de l'article R. 4231-1 du même code : " L'autorité militaire est tenue de notifier par écrit à tout ancien militaire la durée de sa disponibilité, les sujétions qui en découlent ainsi que, le cas échéant, son unité et son lieu d'affectation ". Aux termes de l'article R. 4231-3 de ce code : " Les anciens militaires soumis à l'obligation de disponibilité sont tenus d'avertir l'autorité militaire de tout changement dans leur situation personnelle susceptible d'affecter l'accomplissement de cette obligation ".<br/>
<br/>
              4. Il résulte des dispositions citées au point 3 que la circonstance qu'un ancien militaire n'aurait pas reçu la notification prévue à l'article R. 4231-1 du code de la défense ne saurait faire obstacle ni à l'obligation de disponibilité instituée par le 2° de l'article L. 4231-1 du même code, ni aux devoirs qui en découlent, et notamment à la nécessité d'avertir l'autorité militaire de tout changement dans sa situation personnelle susceptible d'affecter l'accomplissement de cette obligation. Par suite, en jugeant que M. B... n'était pas tenu de faire connaître son changement d'adresse à la ministre des armées, faute pour cette dernière de lui avoir notifié la durée de sa disponibilité et les sujétions qui découlaient de l'obligation de disponibilité, pour en déduire que le courrier adressé par l'administration à l'ancienne adresse de l'intéressé n'avait pas pu avoir pour effet d'interrompre la prescription biennale applicable aux rémunérations en vertu de l'article 37-1 de la loi du 12 avril 2000, la cour administrative d'appel de Nantes a commis une erreur de droit. La ministre des armées est, par suite, fondée à demander l'annulation de l'arrêt qu'elle attaque.<br/>
<br/>
<br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              Sur les conclusions aux fins d'annulation et de décharge :<br/>
<br/>
              6. En premier lieu, aux termes de l'article 24 du décret du 7 novembre 2012, applicables aux titres de perception : " (...) Toute créance liquidée faisant l'objet d'une déclaration ou d'un ordre de recouvrer indique les bases de la liquidation. (...) ". Ces dispositions imposent à la personne publique qui émet un état exécutoire d'indiquer, soit dans le titre lui-même, soit par référence à un document joint à l'état exécutoire ou précédemment adressé au débiteur, les bases de la liquidation. Il résulte de l'instruction que les titres de perception et d'annulation litigieux mentionnent les prestations et indemnités au titre desquelles il est procédé à la répétition de l'indu, ainsi que les périodes concernées. Ils se réfèrent aux décomptes notifiés à M. B... par le centre expert des ressources humaines et de la solde respectivement les 10 juin 2015 et 23 mai 2016, qui détaillent les montants en cause pour chaque composante de la rémunération et sont assortis de tableaux annexés retraçant mois par mois les droits, versements et retenues et montants de trop-versé en résultant. Par suite, le moyen tiré du défaut de motivation des titres de perception doit être écarté.<br/>
<br/>
              7. En deuxième lieu, il résulte des dispositions de l'article 37-1 de la loi du 12 avril 2000, citées au point 2, qu'une somme indûment versée par une personne publique à l'un de ses agents au titre de sa rémunération peut, en principe, être répétée dans un délai de deux ans à compter du premier jour du mois suivant celui de sa date de mise en paiement, sans que puisse y faire obstacle la circonstance que la décision créatrice de droits qui en constitue le fondement ne peut plus être retirée. Sauf dispositions spéciales, les règles fixées par l'article 37-1 de la loi du 12 avril 2000 sont applicables à l'ensemble des sommes indûment versées par des personnes publiques à leurs agents à titre de rémunération, y compris les avances et, faute d'avoir été précomptées sur la rémunération, les contributions ou cotisations sociales.<br/>
<br/>
              8. Compte tenu de l'obligation qui pesait sur M. B..., en vertu de l'article R. 4231-3 du code de la défense, d'avertir l'autorité militaire de tout changement dans sa situation personnelle, le délai de prescription de deux ans mentionné au point 7 a été valablement interrompu par la notification d'indu faite le 10 juin 2015 à la dernière adresse connue de cette autorité, qui était en l'espèce l'adresse communiquée par l'intéressé lors de sa radiation des contrôles. Il s'ensuit que seules les sommes perçues à tort par M. B... avant le 1er juin 2013 étaient prescrites.<br/>
<br/>
              9. En troisième lieu, il résulte de l'instruction, en particulier des pièces et éléments détaillés de calcul produits par la ministre des armées et non sérieusement contestés par M. B..., que, compte tenu notamment de la prescription des sommes indûment versées avant le 1er juin 2013, celui-ci reste redevable d'une somme totale de 9 028,22 euros correspondant à des trop-versés de 467,70 euros au titre de l'indemnité pour temps d'activité et d'obligations professionnelles complémentaires, de 10 857 euros au titre de l'indemnité de résidence à l'étranger et de 21,93 euros au titre du complément forfaitaire de l'indemnité pour charges militaires, ainsi qu'à des moins-versés de 815,79 euros au titre des cotisations sociales, de 0,86 euro au titre du supplément familial de solde et de 1 501,76 euros au titre des indemnités pour charges militaires. M. B... n'est, par suite, pas fondé à soutenir que les titres de perception litigieux seraient erronés. Contrairement à ce qui est soutenu, il ne résulte pas davantage de l'instruction que les sommes faisant l'objet des titres contestés n'auraient pas été effectivement versées à l'intéressé.<br/>
<br/>
              10. Il résulte de ce qui précède que M. B... n'est pas fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif a annulé les titres de perception et d'annulation des 16 février et 17 juin 2016 en tant seulement qu'ils mettaient à sa charge une somme supérieure à 9 028,22 euros.<br/>
<br/>
              Sur les conclusions indemnitaires :<br/>
<br/>
              11. Comme il a été dit aux points 8 et 9, si l'administration a demandé le reversement de sommes indûment versées avant le 1er juin 2013, elle pouvait légalement demander le reversement d'une somme totale de 9 028,22 euros indûment versée à M. B.... Si ce dernier fait valoir qu'il a dû acquitter des impôts sur les sommes lorsqu'elles lui ont été versées et met en avant sa situation financière personnelle, il n'établit pas l'existence d'un préjudice financier qu'il aurait subi à raison des reversements qui lui ont été demandés. Il ne résulte pas de l'instruction que le préjudice moral et les troubles dans les conditions d'existence qu'il invoque justifieraient une appréciation plus importante que celle retenue par le tribunal administratif de Nantes. Il s'ensuit que M. B... n'est pas fondé à demander la réformation du jugement en tant qu'il a statué sur ces conclusions indemnitaires.<br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              12. Les dispositions de l'article L. 761-1 font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 23 octobre 2020 de la cour administrative d'appel de Nantes est annulé.<br/>
Article 2 : L'appel formé par M. B... devant la cour administrative d'appel de Nantes est rejeté.<br/>
Article 3 : Les conclusions présentées par M. B... devant le Conseil d'Etat au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à la ministre des armées et à M. N... B....<br/>
              Délibéré à l'issue de la séance du 13 octobre 2021 où siégeaient : M. Jacques-Henri Stahl, président adjoint de la section du contentieux, présidant ; M. I... K..., M. Olivier Japiot, présidents de chambre ; M. J... M..., Mme A... L..., M. E... H..., M. F... O..., M. Jean-Yves Ollier, conseillers d'Etat et M. Guillaume Leforestier, maître des requêtes-rapporteur. <br/>
<br/>
              Rendu le 5 novembre 2021. <br/>
<br/>
                                   Le président : <br/>
                                   Signé : M. P... D...<br/>
<br/>
Le rapporteur : <br/>
Signé : M. Guillaume Leforestier<br/>
<br/>
                                   La secrétaire :<br/>
                                   Signé : Mme G... C... <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">08-01-01-08-03 ARMÉES ET DÉFENSE. - PERSONNELS MILITAIRES ET CIVILS DE LA DÉFENSE. - QUESTIONS COMMUNES À L'ENSEMBLE DES PERSONNELS MILITAIRES. - OBLIGATION DE DISPONIBILITÉ (ART. L. 4231-1 ET S. DU CODE DE LA DÉFENSE) - CIRCONSTANCE QU'UN ANCIEN MILITAIRE N'AURAIT PAS REÇU LA NOTIFICATION PRÉVUE À L'ARTICLE R. 4231-1 DU CODE DE LA DÉFENSE - CIRCONSTANCE SANS INCIDENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">08-01-02-04 ARMÉES ET DÉFENSE. - PERSONNELS MILITAIRES ET CIVILS DE LA DÉFENSE. - QUESTIONS PARTICULIÈRES À CERTAINS PERSONNELS MILITAIRES. - RÉSERVISTES. - OBLIGATION DE DISPONIBILITÉ (ART. L. 4231-1 ET S. DU CODE DE LA DÉFENSE) - CIRCONSTANCE QU'UN ANCIEN MILITAIRE N'AURAIT PAS REÇU LA NOTIFICATION PRÉVUE À L'ARTICLE R. 4231-1 DU CODE DE LA DÉFENSE - CIRCONSTANCE SANS INCIDENCE.
</SCT>
<ANA ID="9A"> 08-01-01-08-03 En vertu de l'article L. 4211-1 du code de la défense, la réserve militaire est notamment constituée d'une réserve opérationnelle comprenant les anciens militaires soumis à l'obligation de disponibilité.......La circonstance qu'un ancien militaire n'aurait pas reçu la notification, prévue à l'article R. 4231-1 du code de la défense, de la durée de sa disponibilité, des sujétions qui en découlent ainsi que, le cas échéant, de son unité et de son lieu d'affectation, ne saurait faire obstacle ni à l'obligation de disponibilité instituée par le 2° de l'article L. 4231-1 du même code pour les anciens militaires, ni aux devoirs qui en découlent, et notamment à la nécessité, prévue à l'article R. 4231-3, d'avertir l'autorité militaire de tout changement dans sa situation personnelle susceptible d'affecter l'accomplissement de cette obligation.</ANA>
<ANA ID="9B"> 08-01-02-04 En vertu de l'article L. 4211-1 du code de la défense, la réserve militaire est notamment constituée d'une réserve opérationnelle comprenant les anciens militaires soumis à l'obligation de disponibilité.......La circonstance qu'un ancien militaire n'aurait pas reçu la notification, prévue à l'article R. 4231-1 du code de la défense, de la durée de sa disponibilité, des sujétions qui en découlent ainsi que, le cas échéant, de son unité et de son lieu d'affectation, ne saurait faire obstacle ni à l'obligation de disponibilité instituée par le 2° de l'article L. 4231-1 du même code pour les anciens militaires, ni aux devoirs qui en découlent, et notamment à la nécessité, prévue à l'article R. 4231-3, d'avertir l'autorité militaire de tout changement dans sa situation personnelle susceptible d'affecter l'accomplissement de cette obligation.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
