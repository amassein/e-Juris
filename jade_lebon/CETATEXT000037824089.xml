<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037824089</ID>
<ANCIEN_ID>JG_L_2018_12_000000400311</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/82/40/CETATEXT000037824089.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 6ème chambres réunies, 17/12/2018, 400311</TITRE>
<DATE_DEC>2018-12-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>400311</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN ; SCP MEIER-BOURDEAU, LECUYER</AVOCATS>
<RAPPORTEUR>M. Guillaume Leforestier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2018:400311.20181217</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              L'association Sainte-Thérèse Préservée, M. A...M..., M. D...J..., M. P...I..., Mme K...E..., Mme L...O..., M. Q...G..., M. Q...H..., Mme N...F...et M. C...B...ont demandé au tribunal administratif de Pau d'annuler pour excès de pouvoir, d'une part, l'arrêté du 18 février 2014 par lequel le maire de Ciboure (Pyrénées-Atlantiques) a délivré aux sociétés La Foncière du Pays basque et Clairsienne un permis de construire d'un ensemble immobilier, la décision du 2 mai 2014 par laquelle le maire de Ciboure a rejeté leur recours gracieux contre ce permis et l'arrêté du 9 avril 2015 par lequel le maire de Ciboure a délivré à ces sociétés un permis de construire modificatif pour ce même ensemble, ainsi que, d'autre part, l'arrêté du 22 avril 2014 par lequel le préfet des Pyrénées-Atlantiques a autorisé la société La Foncière du Pays basque à défricher des parcelles d'une superficie de 39 455 m² sur le territoire de la commune de Ciboure et la décision modificative du 19 juin 2015 ramenant cette superficie à 35 496 m². Par un jugement n° 1401303 du 29 mars 2016, le tribunal administratif a fait droit à leurs demandes.<br/>
<br/>
              Par un arrêt n° 16BX01833 du 22 juin 2017, la cour administrative d'appel de Bordeaux a, sur appel de la société Clairsienne, annulé ce jugement en tant qu'il avait annulé les autorisations de défrichement des 22 avril 2014 et 19 juin 2015 et rejeté les conclusions de l'association Sainte-Thérèse Préservée et autres dirigées contre ces décisions.<br/>
<br/>
              1° Sous le n° 400311, par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 1er juin, 1er septembre 2016 et 27 octobre 2017 au secrétariat du contentieux du Conseil d'Etat, la société Clairsienne demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement du 29 mars 2016 du tribunal administratif de Pau en tant qu'il a annulé les permis de construire qui lui ont été accordés par le maire de Ciboure ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter les demandes de l'association Sainte-Thérèse Préservée et autres ;<br/>
<br/>
              3°) de mettre à la charge de l'association Sainte-Thérèse Préservée et autres une somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              2° Sous le n° 413655, par un pourvoi sommaire, un pourvoi rectificatif, un mémoire complémentaire et un mémoire en réplique, enregistrés les 23 août, 31 août, 23 novembre 2017 et 8 octobre 2018 au secrétariat du contentieux du Conseil d'Etat, l'association Sainte-Thérèse Préservée, M. A...M..., M. P...I..., Mme K...E..., M. Q...G..., M. Q...H..., Mme N...F..., M. C...B...et M. D... J...demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 16BX01833 du 22 juin 2017 de la cour administrative d'appel de Bordeaux ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat et de la société Clairsienne la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
<br/>
              - le code de l'urbanisme ;<br/>
<br/>
              - le code de l'environnement ;<br/>
<br/>
              - le code forestier ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Guillaume Leforestier, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la société Clairsienne SA, à la SCP Meier-Bourdeau, Lecuyer, avocat de l'association Sainte Thérèse Préservée, de M.M..., de M. I..., de MmeE..., de M.G..., de M.H..., de MmeF..., de M. B...et de M.J....<br/>
<br/>
<br/>
              Vu la note en délibéré, enregistrée le 30 novembre 2018, présentée par la société Clairsienne ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces des dossiers soumis aux juges du fond que la société La Foncière du Pays Basque et la société Clairsienne envisageaient l'aménagement d'un ensemble de logements et de commerces sur des parcelles situées sur le territoire de la commune de Ciboure (Pyrénées-Atlantiques) ; que le maire de Ciboure leur a délivré un permis de construire le 18 février 2014 puis un permis modificatif le 9 avril 2015 ; que la société La Foncière du Pays Basque a obtenu, le 22 avril 2014, du préfet des Pyrénées-Atlantiques une autorisation de défrichement, modifiée par une nouvelle autorisation délivrée le 19 juin 2015 ; qu'à la demande de l'association Sainte-Thérèse Préservée et autres, le tribunal administratif de Pau, par un jugement du 29 mars 2016, a annulé l'ensemble de ces décisions ; que ce jugement a été rendu en premier et dernier ressort en ce qui concerne les conclusions dirigées contre les permis de construire et sous réserve d'appel en ce qui concerne les conclusions dirigées contre les autorisations de défrichement ; que la société Clairsienne se pourvoit en cassation contre ce jugement en tant qu'il annule les permis de construire ; que la société ayant par ailleurs fait appel du jugement en tant qu'il annulait les autorisations de défrichement, la cour administrative d'appel de Bordeaux, par un arrêt du 22 juin 2017, a annulé le jugement du tribunal administratif dans cette mesure et rejeté la demande d'annulation des autorisations de défrichement ; que l'association Sainte-Thérèse Préservée et autres se pourvoit en cassation contre cet arrêt ; qu'il y a lieu de joindre ces pourvois pour statuer par une seule décision ;<br/>
<br/>
              Sur les conclusions dirigées contre l'arrêt de la cour administrative d'appel de Bordeaux :<br/>
<br/>
              En ce qui concerne la recevabilité de l'appel de la société Clairsienne :<br/>
<br/>
              2. Considérant que la personne qui, devant le tribunal administratif, est régulièrement intervenue en défense à un recours pour excès de pouvoir est recevable à interjeter appel du jugement rendu sur ce recours contrairement aux conclusions de son intervention lorsqu'elle aurait eu qualité, à défaut d'intervention de sa part, pour former tierce opposition contre le jugement faisant droit au recours ; qu'aux termes de l'article R. 832-1 du code de justice administrative : " Toute personne peut former tierce opposition à une décision juridictionnelle qui préjudicie à ses droits, dès lors que ni elle ni ceux qu'elle représente n'ont été présents ou régulièrement appelés dans l'instance ayant abouti à cette décision " ;<br/>
<br/>
              3. Considérant qu'aux termes de l'article L. 341-7 du code forestier, dans sa rédaction applicable au litige porté devant les juges du fond, dont les dispositions étaient rappelées à l'article L. 425-6 du code de l'urbanisme : " Lorsque la réalisation d'une opération ou de travaux soumis à une autorisation administrative, à l'exception de celles prévues au titre Ier et au chapitre V du titre V du livre V du code de l'environnement, nécessite également l'obtention d'une autorisation de défrichement, celle-ci doit être obtenue préalablement à la délivrance de cette autorisation administrative " ; qu'il résulte de ces dispositions que si la société La Foncière du Pays basque était seule titulaire des autorisations de défrichement des 22 avril 2014 et 19 juin 2015, la délivrance de ces autorisations conditionnait celle du permis de construire sollicité par la société Clairsienne ; qu'ainsi, l'annulation par le tribunal administratif de Pau des autorisations de défrichement préjudiciait aux droits de la société Clairsienne ; que, par suite, la cour administrative de Bordeaux n'a pas commis d'erreur de droit en regardant comme recevable l'appel de la société dirigé contre le jugement du 29 mars 2016 en tant qu'il annulait les autorisations de défrichement litigieuses ;<br/>
<br/>
              En ce qui concerne le bien-fondé de l'arrêt attaqué :<br/>
<br/>
              Quant à la dispense d'étude d'impact :<br/>
<br/>
              4. Considérant qu'aux termes des deux premiers alinéas de l'article L. 122-1 du code de l'environnement, dans sa rédaction alors en vigueur : " I. - Les projets de travaux, d'ouvrages ou d'aménagements publics et privés qui, par leur nature, leurs dimensions ou leur localisation sont susceptibles d'avoir des incidences notables sur l'environnement ou la santé humaine sont précédés d'une étude d'impact. / Ces projets sont soumis à étude d'impact en fonction de critères et de seuils définis par voie réglementaire et, pour certains d'entre eux, après un examen au cas par cas effectué par l'autorité administrative de l'Etat compétente en matière d'environnement " ; qu'aux termes du I de l'article R. 122-3 du même code, dans sa rédaction alors en vigueur : " Pour les projets relevant d'un examen au cas par cas en application de l'article R. 122-2, l'autorité administrative de l'Etat compétente en matière d'environnement, définie à l'article R. 122-6, examine, au regard des informations fournies par le pétitionnaire ou le maître d'ouvrage, si le projet doit faire l'objet d'une étude d'impact " ; qu'il ressort des pièces des dossiers soumis aux juges du fond que le préfet des Pyrénées Atlantiques a, par une décision du 7 juin 2013, dispensé d'étude d'impact les opérations de défrichement litigieuses ; <br/>
<br/>
              5. Considérant qu'en jugeant que le préfet des Pyrénées Atlantiques s'était notamment fondé, pour estimer que le projet envisagé n'était pas susceptible d'impacts notables sur l'environnement, sur la circonstance que le terrain d'assiette était situé en site inscrit et en zone de protection du patrimoine architectural urbain et paysager, la cour a porté sur les faits et pièces du dossier une appréciation souveraine exempte de dénaturation ;<br/>
<br/>
              6. Considérant qu'il ressort des motifs de l'arrêt attaqué que la cour ne s'est pas fondée, pour statuer sur la légalité de la dispense d'étude d'impact, sur la circonstance que les parcelles d'assiette du projet étaient situées en zone à urbaniser du plan local d'urbanisme ; qu'est, par suite, inopérant le moyen tiré de ce que les juges d'appel auraient dénaturé les pièces du dossier ou commis une erreur de qualification juridique en s'abstenant de prendre en compte l'annulation du plan local d'urbanisme par le tribunal administratif de Pau ; <br/>
<br/>
              7. Considérant que le moyen tiré de ce que les dispositions des articles L. 341-5 et L. 341-6 du code forestier imposaient de prendre en compte, pour apprécier à nouveau la nécessité d'une étude d'impact préalable à la délivrance de l'autorisation de défrichement modificative, non seulement la superficie des parcelles à défricher mais aussi la nature des bois concernés est nouveau en cassation est n'est pas d'ordre public ; qu'il est, par suite, inopérant ; <br/>
<br/>
              Quant aux compensations auxquelles les autorisations de défrichement ont été subordonnées :<br/>
<br/>
              8. Considérant que l'article L. 341-6 du code forestier, dans sa rédaction en vigueur à la date de la première autorisation de défrichement, disposait que la délivrance de l'autorisation de défrichement pouvait être subordonnée, notamment, à la conservation sur le terrain de réserves boisées suffisantes, à l'exécution de travaux de reboisement sur d'autres terrains ou au versement d'une indemnité destinée à financer des opérations de boisement ou à plusieurs de ces conditions ; que les dispositions du même code, dans leur rédaction en vigueur à la date de la seconde autorisation, faisaient de la définition de telles compensations une obligation ; qu'il ressort des pièces des dossiers soumis aux juges du fond que l'autorisation de défrichement délivrée le 22 avril 2014 était subordonnée au financement d'opérations de boisement et d'aménagement en espaces boisés naturels, au titre des mesures compensatoires prévues par ces dispositions ; que les modalités d'exécution de ces opérations étaient définies dans une convention annexée à l'autorisation, qui avait été passée le 14 mars 2014 entre la société La Foncière du Pays basque et la société Clairsienne, d'une part, et la commune de Ciboure, d'autre part ; que, par une délibération du 25 mars 2015, le conseil municipal de Ciboure a retiré la délibération du 3 mars 2014 par laquelle il avait autorisé le maire à signer cette convention ; que l'autorisation de défrichement initiale a fait l'objet d'une décision modificative le 19 juin 2015, afin notamment de substituer aux conditions tenant au financement d'opérations de boisement et d'aménagement en espaces boisés naturels le versement d'une indemnité au Fonds stratégique de la forêt et du bois ;<br/>
<br/>
              9. Considérant qu'en se fondant, pour écarter comme inopérants les moyens tirés d'éventuelles irrégularités ayant affecté la définition des mesures de compensation prévues par l'autorisation de défrichement initiale du 22 avril 2014, sur la circonstance que l'autorisation modificative du 19 juin 2015 avait substitué à ces mesures d'autres mesures de compensation,, la cour administrative d'appel de Bordeaux n'a pas commis d'erreur de droit ; qu'était également inopérant, pour le même motif, le moyen tiré de ce que le retrait, par une délibération du 25 mars 2015, de la délibération du conseil municipal de Ciboure du 3 mars 2014 avait rendu illégale l'autorisation de défrichement initiale ; que ce motif, qui n'emporte l'appréciation d'aucune circonstance de fait, doit être substitué au motif retenu par l'arrêt attaqué, dont il justifie le dispositif ; <br/>
<br/>
              10. Considérant qu'il résulte de tout ce qui précède que l'association Sainte-Thérèse Préservée et autres ne sont pas fondés à demander l'annulation de l'arrêt qu'ils attaquent ;<br/>
<br/>
              Sur les conclusions dirigées contre le jugement du tribunal administratif de Pau :<br/>
<br/>
              11. Considérant que, pour annuler les permis de construire litigieux, le tribunal a notamment jugé que ces décisions devaient être annulées par voie de conséquence de l'annulation des autorisations de défrichement des 22 avril 2014 et 22 juin 2015 ; que par son arrêt du 22 juin 2017, la cour administrative d'appel de Bordeaux a censuré ce jugement en tant qu'il prononçait l'annulation de ces autorisations de défrichement ; que le pourvoi formé contre cet arrêt étant rejeté pour les motifs indiqués ci-dessus, ce motif d'annulation des permis de construire doit, par suite, être censuré ; qu'il appartient au juge de cassation, dans cette situation, d'examiner les moyens dirigés contre les autres motifs d'annulation retenus par le jugement, afin de censurer le cas échéant ceux qui seraient erronés ;  <br/>
<br/>
              12. Considérant qu'il revient au juge de l'excès de pouvoir d'exercer un plein contrôle sur les motifs pour lesquels l'autorité administrative décide de dispenser un projet de l'étude d'impact prévue à l'article L. 122-1 du code de l'environnement ; que si le tribunal administratif s'est limité, dans le jugement attaqué, à examiner si la décision de dispense d'étude d'impact préalable à la délivrance des permis de construire litigieux était entachée d'une erreur manifeste d'appréciation, la société Clairsienne ne saurait utilement invoquer l'erreur ainsi commise par le tribunal quant à l'intensité de son contrôle dès lors que, celui-ci ayant prononcé l'annulation des décisions litigieuses en exerçant un contrôle restreint de leurs motifs, il aurait nécessairement procédé de même en exerçant un entier contrôle ;  <br/>
<br/>
              13. Considérant qu'il ressort des pièces des dossiers soumis aux juges du fond que l'opération d'aménagement litigieuse nécessite le défrichement de 3,55 hectares ; que si les superficies concernées par le défrichement sont envahies de ronciers et d'espèces invasives, ces plantes ont prospéré au sein d'essences telles que le robinier faux acacia, le cyprès, le pin ou le chêne pédonculé ; que le " procès-verbal de reconnaissance des bois à défricher " établi par les services de l'Etat le 2 avril 2014, postérieurement à l'avis de l'architecte des bâtiments de France, mentionne que seuls " les boisements de type ripisylve " et " les arbres remarquables et sains existants en périphérie du projet " seront conservés ; qu'en estimant au vu de ces éléments que les permis de construire litigieux méconnaissaient les dispositions par lesquelles le règlement de la ZPPAUP couvrant une partie des terrains d'assiette oblige les aménagements à " permettre la conservation et la plantation d'arbres de haut jet ", le tribunal a porté sur les pièces du dossier une appréciation souveraine exempte de dénaturation ; <br/>
<br/>
              14. Considérant qu'en retenant qu'une étude d'impact aurait dû être réalisée en application des articles L. 122-1 et R. 122-3 du code de l'environnement au vu de l'artificialisation de la majeure partie du terrain d'assiette, de son exposition au risque sismique et de son inclusion dans le périmètre d'un site inscrit et d'une ZPPAUP, de la présence d'espèces hygrophiles au-delà de la ripisylve qui suggère d'importantes quantités d'eau pouvant constituer une zone humide, de l'augmentation du trafic routier d'environ mille véhicules par jour et des nuisances sonores associées qui résulteraient du projet, le tribunal n'a pas commis d'erreur de droit et a porté sur les faits et pièces du dossier une appréciation souveraine exempte de dénaturation ;<br/>
<br/>
              15. Considérant qu'il résulte de ce qui précède que la société Clairsienne n'est pas fondée à demander l'annulation du jugement qu'elle attaque, en tant qu'il annule les permis de construire litigieux ;<br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              16. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par l'association Sainte-Thérèse Préservée et autres et la société Clairsienne au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Les pourvois de la société Clairsienne et de l'association Sainte-Thérèse Préservée et autres sont rejetés.<br/>
Article 2 : La présente décision sera notifiée à la société Clairsienne, à l'association Sainte-Thérèse Préservée, M. A...M..., M. P...I..., Mme K...E..., M. Q...G..., M. Q...H..., Mme N...F..., M. C...B...et M. D...J....<br/>
Copie en sera adressée au ministre de l'agriculture et de l'alimentation, à la commune de Ciboure et à la société La Foncière du Pays basque. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">03-06-02-02 AGRICULTURE ET FORÊTS. BOIS ET FORÊTS. PROTECTION DES BOIS ET FORÊTS. AUTORISATION DE DÉFRICHEMENT. - AUTORISATION MODIFICATIVE SUBSTITUANT AUX MESURES DE COMPENSATION PRÉVUES PAR L'AUTORISATION INITIALE D'AUTRES MESURES DE COMPENSATION (ART. L. 341-6 DU CODE FORESTIER) - INOPÉRANCE DES MOYENS TIRÉS D'IRRÉGULARITÉS AYANT AFFECTÉ LA DÉFINITION DES MESURES DE COMPENSATION PRÉVUES PAR L'AUTORISATION INITIALE - EXISTENCE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-07-01-04-03 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. QUESTIONS GÉNÉRALES. MOYENS. MOYENS INOPÉRANTS. - MOYEN TIRÉ D'IRRÉGULARITÉS AYANT AFFECTÉ LA DÉFINITION DES MESURES DE COMPENSATION PRÉVUES PAR UNE AUTORISATION DE DÉFRICHEMENT (ART. L. 341-6 DU CODE FORESTIER), LORSQU'UNE AUTORISATION MODIFICATIVE LEUR EN A SUBSTITUÉ D'AUTRES.
</SCT>
<ANA ID="9A"> 03-06-02-02 En se fondant, pour écarter comme inopérants les moyens tirés d'éventuelles irrégularités ayant affecté la définition des mesures de compensation prévues par l'autorisation de défrichement initiale, sur la circonstance que l'autorisation modificative a substitué à ces mesures d'autres mesures de compensation, une cour ne commet pas d'erreur de droit.</ANA>
<ANA ID="9B"> 54-07-01-04-03 En se fondant, pour écarter comme inopérants les moyens tirés d'éventuelles irrégularités ayant affecté la définition des mesures de compensation prévues par l'autorisation de défrichement initiale, sur la circonstance que l'autorisation modificative a substitué à ces mesures d'autres mesures de compensation, une cour ne commet pas d'erreur de droit.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., s'agissant d'un permis de construire, CE, 9 décembre 1994, SARL Séri, n° 116447, T. p. 1261 ; CE, 2 février 2004, SCI La Fontaine de Villiers, n° 238315, T. p. 914.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
