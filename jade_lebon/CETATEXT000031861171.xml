<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031861171</ID>
<ANCIEN_ID>JG_L_2015_12_000000375777</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/86/11/CETATEXT000031861171.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère / 6ème SSR, 30/12/2015, 375777</TITRE>
<DATE_DEC>2015-12-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>375777</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère / 6ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>HAAS</AVOCATS>
<RAPPORTEUR>M. Frédéric Puigserver</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Jean Lessi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:375777.20151230</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 25 février et 16 septembre 2014 au secrétariat du contentieux du Conseil d'Etat, la société Mylan demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du comité économique des produits de santé du 28 novembre 2013 en tant qu'elle concerne le prix de la spécialité Diosmectite Mylan 3 g ;<br/>
<br/>
              2°) d'enjoindre au comité économique des produits de santé, d'une part, de lui communiquer l'avenant à la convention du 7 juin 2010, conclu le 29 octobre 2013, signé par les deux parties et, d'autre part, de faire publier au Journal officiel de la République française les arrêtés d'inscription sur la liste des spécialités remboursables aux assurés sociaux ainsi que les avis sur les taux de prise en charge pour l'assuré et de prix résultant de l'avenant à la convention conclu le 29 octobre 2013, dans un délai de quinze jours à compter de la notification de la décision à intervenir, sous astreinte de 1 000 euros par jour de retard ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 8 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la sécurité sociale ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Puigserver, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Jean Lessi, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Haas, avocat de la société Mylan ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 162-16-4 du code de la sécurité sociale : " Les prix de vente au public de chacun des médicaments mentionnés au premier alinéa de l'article L. 162-17 est fixé par convention entre l'entreprise exploitant le médicament et le Comité économique des produits de santé conformément à l'article L. 162-17-4 ou, à défaut, par décision du comité, sauf opposition conjointe des ministres concernés qui arrêtent dans ce cas le prix dans un délai de quinze jours après la décision du comité (...) " ; qu'il résulte des dispositions de l'article R. 163-8 du même code que l'entreprise exploitant un médicament, en même temps qu'elle demande au ministre chargé de la sécurité sociale l'inscription de la spécialité en cause sur la liste des médicaments remboursables prévue à l'article L. 162-17, adresse au comité économique des produits de santé (CEPS) une proposition de fixation de son prix par convention ; que, selon l'article R. 163-9 du même code, les décisions relatives à la fixation du prix du médicament, comme celles relatives à son inscription sur la liste des médicaments remboursables, sont prises et communiquées à l'entreprise dans un délai de cent quatre-vingts jours à compter de la réception de la demande mentionnée à l'article R. 163-8 ;<br/>
<br/>
              Sur la fin de non-recevoir opposée par le CEPS : <br/>
<br/>
              2. Considérant que les délibérations prises par le CEPS dans le cadre des échanges avec une société exploitant un médicament, en vue de la fixation du prix de ce dernier, purement préparatoires soit à une convention de prix entre la société et le comité soit à, défaut, à une décision unilatérale de fixation du prix par le comité ou, au terme du délai mentionné à l'article R. 163-9 du code de la sécurité sociale, à une décision implicite de refus, ne sont pas susceptibles de faire l'objet d'un recours contentieux ; <br/>
<br/>
              3. Considérant, toutefois, qu'en l'espèce, il ressort des pièces du dossier que, par une demande reçue par le CEPS le 8 mars 2013, la société Mylan a proposé de fixer à un niveau correspondant à une décote de 10 % par rapport au prix du médicament princeps le prix de vente au public de la spécialité générique Diosmectite Mylan 3 g, dont elle a simultanément sollicité l'inscription sur la liste des médicaments remboursables ; qu'alors qu'il avait initialement répondu à cette demande par une proposition de prix correspondant à une décote de 28 % par rapport au prix du médicament princeps, le CEPS est ensuite revenu sur cette proposition, par une délibération du 28 novembre 2013, prise après plusieurs échanges avec la société et communiquée à celle-ci par un courrier du président du comité du 11 décembre 2013 ; que, par cette délibération, le comité a indiqué qu'il ne pourrait envisager de fixer qu'un prix correspondant  à une décote de 60 % ; qu'il ressort des termes mêmes du courrier du 11 décembre 2013 que le CEPS a, par la délibération du 28 novembre 2013, postérieure à la naissance, le 8 septembre précédent, d'une décision implicite de refus, mis fin aux échanges avec la société Mylan, rejeté la demande de fixation du prix au niveau précédemment accepté par celle-ci et refusé tout prix autre que celui qui correspond à une décote de 60 % ; que le comité s'est d'ailleurs abstenu ensuite de faire usage du pouvoir dont il dispose, sous réserve de l'inscription concomitante, par l'autorité ministérielle, du médicament sur la liste des médicaments remboursables, de fixer unilatéralement le prix de vente au public de la spécialité en cause ; que, dans ces circonstances particulières, la délibération du 28 novembre 2013 doit être regardée non comme une mesure préparatoire mais, contrairement à ce qui est soutenu en défense, comme une décision de rejet de la demande de la société Mylan susceptible de faire l'objet d'un recours pour excès de pouvoir ;<br/>
<br/>
              Sur la légalité de la décision attaquée :<br/>
<br/>
              4. Considérant, en premier lieu, que le courrier du président du CEPS indique que la décision du comité repose sur l'" avantage insuffisant pour l'assurance maladie " qu'apporterait un prix supérieur à celui qui correspond à une décote de 60 % par rapport au prix fabricant hors taxe du médicament princeps ; que le projet de convention joint à ce courrier mentionne les dispositions du code de la sécurité sociale dont le comité a entendu faire application ; que la société Mylan a ainsi été informée des motifs de fait et de droit justifiant la décision du comité ; que, par suite, le moyen tiré du défaut de motivation de la décision attaquée manque en fait ;<br/>
<br/>
              5. Considérant, en deuxième lieu, que la décision de fixer le prix d'un médicament remboursable, qu'elle soit prise sous la forme d'une convention ou, à défaut, sous la forme d'une décision unilatérale du CEPS, revêt un caractère réglementaire ; que la seule circonstance que, au cours du processus de négociation ouvert en vue de la fixation du prix, la société Mylan ait accepté une proposition du CEPS, en lui retournant, par un courrier du 29 octobre 2013, un projet de convention signé, n'a, contrairement à ce que soutient la société requérante, pas eu comme conséquence, en l'absence de suite donnée par le comité, l'adoption d'une décision de fixation de prix ; que la société ne tenant aucun droit de la précédente proposition du comité, celui-ci pouvait, sans avoir à justifier d'un changement de circonstances, revenir sur cette proposition sans porter atteinte à des droits acquis ; que le moyen tiré de ce que la décision attaquée serait entachée d'erreur de droit pour ce motif doit être écarté ;<br/>
<br/>
              6. Considérant, en troisième lieu, que le CEPS s'est donné pour lignes directrices, à compter du début de l'année 2012, de fixer le prix d'une première spécialité générique à un prix correspondant à une décote de 60 % par rapport au prix du médicament princeps, sauf circonstances particulières justifiant une moindre décote, tenant en particulier aux conditions de fabrication de la spécialité, à son faible niveau de prix lié à son ancienneté ou à la faible taille du marché ; que, sans contester la légalité de ces lignes directrices, qui ont été rendues publiques, la société Mylan soutient que l'antériorité du médicament princeps et les coûts de production de sa spécialité, liés en particulier au prix d'acquisition du principe actif, justifiaient la fixation d'une décote  inférieure à 60 % ;<br/>
<br/>
              7. Considérant, toutefois, que la société requérante se borne, à l'appui de ses allégations relatives aux coûts de production auxquels elle est exposée, à produire une unique facture émanant d'une société présentée comme son fournisseur de diosmectite, portant au demeurant sur des volumes non précisés ; qu'il ne ressort ni de ces éléments ni des autres pièces du dossier qu'en excluant de fixer un prix autre que celui qui correspond à une décote de 60 % par rapport au prix du médicament princeps, le CEPS aurait entaché sa décision d'erreur manifeste d'appréciation ;  <br/>
<br/>
              8. Considérant, en dernier lieu, qu'il résulte de leur lettre même que les dispositions de l'article L. 162-38 du code de la sécurité sociale selon lesquelles la fixation des prix et des marges des produits pris en charge par les régimes obligatoires de sécurité sociale " tient compte de l'évolution des charges, des revenus et du volume d'activité des praticiens ou entreprises concernés " ne s'appliquent, en ce qui concerne le CEPS, qu'à la fixation du prix des produits mentionnés à l'article L. 165-1, qui ne comprennent pas les médicaments ; qu'en mentionnant, à son deuxième alinéa, que le prix de vente au public des médicaments comprend les marges prévues par la décision mentionnée à l'article L. 162-38, l'article L. 162-16-4 du même code n'a pas rendu applicables à la fixation du prix des médicaments par le CEPS les critères de fixation des prix mentionnés à l'article L. 162-38 ; qu'il suit de là que la société requérante ne peut utilement soutenir qu'en ne tenant pas compte de ses charges, le CEPS aurait méconnu les dispositions de l'article L. 162-38 ;<br/>
<br/>
              9. Considérant qu'il résulte de tout ce qui précède que la société Mylan n'est pas fondée à demander l'annulation de la décision qu'elle attaque ; que les conclusions qu'elle présente aux fins d'injonction et au titre des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être également rejetées ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
                           --------------<br/>
Article 1er : La requête de la société Mylan est rejetée.<br/>
Article 2 : La présente décision sera notifiée à la société Mylan et au comité économique des produits de santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-01-01 PROCÉDURE. INTRODUCTION DE L'INSTANCE. DÉCISIONS POUVANT OU NON FAIRE L'OBJET D'UN RECOURS. - DÉLIBÉRATIONS PRISES PAR LE CEPS POUR LA FIXATION DU PRIX DE VENTE DES MÉDICAMENTS [RJ1] - 1) PRINCIPE - ACTES PRÉPARATOIRES INSUSCEPTIBLES DE RECOURS - 2) EXCEPTION EN L'ESPÈCE, COMPTE TENU DES CIRCONSTANCES PARTICULIÈRES.
</SCT>
<ANA ID="9A"> 54-01-01 1) Les délibérations prises par le comité économique des produits de santé (CEPS) dans le cadre des échanges avec une société exploitant un médicament, en vue de la fixation du prix de ce dernier, purement préparatoires soit à une convention de prix entre la société et le comité soit à, défaut, à une décision unilatérale de fixation du prix par le comité ou, au terme du délai mentionné à l'article R. 163-9 du code de la sécurité sociale (CSS), à une décision implicite de refus, ne sont pas susceptibles de faire l'objet d'un recours contentieux.,,,2) Toutefois, en l'espèce, par la délibération attaquée, intervenue après plusieurs échanges avec la société et postérieurement à la naissance d'une décision implicite de refus, le CEPS a mis fin aux échanges avec la société, rejeté la demande de fixation du prix au niveau précédemment accepté par celle-ci et refusé tout prix autre que celui qu'il envisageait en dernier lieu, correspondant à une décote par rapport au médicament princeps très supérieure à celle envisagée lors des échanges initiaux. Le comité s'est d'ailleurs abstenu ensuite de faire usage du pouvoir dont il dispose, sous réserve de l'inscription concomitante, par l'autorité ministérielle, du médicament sur la liste des médicaments remboursables, de fixer unilatéralement le prix de vente au public de la spécialité en cause. Dans ces circonstances particulières, la délibération attaquée doit être regardée non comme une mesure préparatoire mais comme une décision de rejet de la demande.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Cf. CE, 20 mars 2013, Société Addmedica, n°s 356661 e. a., aux Tables sur d'autres points.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
