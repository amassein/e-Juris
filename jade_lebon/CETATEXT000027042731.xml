<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027042731</ID>
<ANCIEN_ID>JG_L_2013_02_000000348278</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/04/27/CETATEXT000027042731.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème / 4ème SSR, 06/02/2013, 348278</TITRE>
<DATE_DEC>2013-02-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>348278</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème / 4ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP HEMERY, THOMAS-RAQUIN ; SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>M. Jean-Dominique Langlais</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Fabienne Lambolez</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2013:348278.20130206</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu 1°, sous le n° 348278, le pourvoi sommaire et le mémoire complémentaire, enregistrés les 8 avril 2011 et 8 juillet 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour la commune de Gassin, représentée par son maire ; la commune de Gassin demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 09MA01528 du 10 février 2011 par lequel la cour administrative d'appel de Marseille, après avoir annulé le jugement n° 0500477 du 22 janvier 2009 du tribunal administratif de Nice rejetant la demande de la SCI L'Horizon tendant à l'annulation de l'arrêté du 1er décembre 2004 par lequel le maire de Gassin lui a refusé une autorisation d'abattage d'arbres en vue de l'aménagement d'un chemin d'accès aux bâtiments sis sur sa propriété, a annulé cet arrêté ;<br/>
<br/>
              2°) de mettre à la charge de la SCI L'Horizon le versement d'une somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
              Vu, 2° sous le n° 348279, le pourvoi sommaire et le mémoire complémentaire, enregistrés les 8 avril 2011 et 8 juillet 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour la commune de Gassin, représentée par son maire ; la commune de Gassin demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 09MA01553 du 10 février 2011 par lequel la cour administrative d'appel de Marseille, après avoir annulé le jugement n° 0500480 du 22 janvier 2009 du tribunal administratif de Nice rejetant la demande de la SCI L'Horizon tendant à l'annulation de l'arrêté du 1er décembre 2004 par lequel le maire de Gassin lui a refusé une autorisation d'abattage d'arbres en vue de l'aménagement d'un chemin autour des bâtiments sis sur sa propriété, a annulé cet arrêté ;<br/>
<br/>
              2°) de mettre à la charge de la SCI L'Horizon le versement d'une somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu le code de l'environnement ;<br/>
<br/>
              Vu le code de l'urbanisme ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Langlais, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Hémery, Thomas-Raquin, avocat de la commune de Gassin, et de la SCP Lyon-Caen, Thiriez, avocat de la SCI L'Horizon,<br/>
<br/>
              - les conclusions de Mme Fabienne Lambolez, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Hémery, Thomas-Raquin, avocat de la commune de Gassin, et à la SCP Lyon-Caen, Thiriez, avocat de la SCI L'Horizon ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que les pourvois de la commune de Gassin présentent à juger les mêmes questions ; qu'il y a lieu de les joindre pour statuer par une seule décision ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la SCI L'Horizon a acquis en 2003 une propriété située dans un espace boisé classé en application des dispositions de l'article L. 130-1 du code de l'urbanisme par le plan d'occupation des sols de Gassin, commune littorale de la presqu'île de Saint-Tropez ; qu'elle y a fait procéder en 2004 à des abattages d'arbres en vue, d'une part, d'élargir un chemin d'accès aux bâtiments sis sur cette propriété et, d'autre part, d'aménager un chemin autour de ces bâtiments ; que, ces travaux ayant été réalisés sans que soit délivrée préalablement l'autorisation prévue par l'article L. 130-1, la SCI a sollicité du maire de Gassin, à titre de régularisation, la délivrance d'autorisations d'abattage d'arbres ; que le maire a rejeté ces demandes par deux arrêtés du 1er décembre 2004 ; que les recours pour excès de pouvoir contre ces arrêtés introduits pour la SCI L'Horizon ont été rejetés par deux jugements du 22 janvier 2009 du tribunal administratif de Nice ; que la commune de Gassin se pourvoit contre les deux arrêts du 10 février 2011 par lesquels la cour administrative d'appel de Marseille, faisant droit aux appels de la SCI L'Horizon, a annulé ces jugements ainsi que les arrêtés litigieux ;<br/>
<br/>
              3. Considérant qu'aux termes de l'article L. 146-6 du code de l'urbanisme, dans sa rédaction en vigueur à la date des arrêtés attaqués : " Les documents et décisions relatifs à la vocation des zones ou à l'occupation et à l'utilisation des sols préservent les espaces terrestres et marins, sites et paysages remarquables ou caractéristiques du patrimoine naturel et culturel du littoral, et les milieux nécessaires au maintien des équilibres biologiques. Un décret fixe la liste des espaces et milieux à préserver, comportant notamment, en fonction de l'intérêt écologique qu'ils présentent, les dunes et les landes côtières, les plages et lidos, les forêts et zones boisées côtières (...) / Toutefois, des aménagements légers peuvent y être implantés lorsqu'ils sont nécessaires à leur gestion, à leur mise en valeur notamment économique ou, le cas échéant, à leur ouverture au public. Un décret définit la nature et les modalités de réalisation de ces aménagements (...) " ; que l'article R. 146-2, pris en application de ces dernières dispositions, fixe la liste des aménagements légers qui peuvent être implantés dans les espaces protégés par l'article L. 146-6 ;<br/>
<br/>
              4. Considérant qu'après avoir estimé que la propriété de la SCI L'Horizon était située dans un espace remarquable entrant dans le champ d'application des dispositions de l'article L. 146-6 du code de l'urbanisme, la cour administrative d'appel a annulé les deux arrêtés attaqués au motif, pour le premier, que l'élargissement du chemin d'accès aux bâtiments n'était pas susceptible d'entraîner une altération significative du site et, pour le second, qu'il n'était pas établi que l'abattage des arbres nécessité par l'aménagement d'un chemin autour des bâtiments porterait atteinte au caractère ou à l'intérêt de ce paysage remarquable ou qu'il serait visible à partir du littoral ; qu'en statuant ainsi, sans rechercher si les deux chemins pour la réalisation desquels  la SCI L'Horizon avait présenté ses demandes pouvaient légalement être réalisés eu égard aux dispositions de l'article R. 146-2 du même code, la cour administrative n'a pas légalement justifié ses décisions ; que, par suite, sans qu'il soit besoin d'examiner les autres moyens des pourvois, la commune de Gassin est fondée à demander l'annulation des deux arrêts qu'elle attaque ;<br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler les affaires au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              Sur l'arrêté du 1er décembre 2004 par lequel le maire de Gassin a refusé l'autorisation d'abattage d'arbres en vue de l'aménagement d'un chemin d'accès aux bâtiments : <br/>
<br/>
              En ce qui concerne la recevabilité de la requête d'appel : <br/>
<br/>
              6. Considérant que la SCI L'Horizon a produit le jugement dont elle fait appel ; qu'elle est représentée par son gérant ; qu'ainsi, les fins de non recevoir opposées par la commune de Gassin ne sauraient être accueillies ; <br/>
<br/>
              En ce qui concerne la légalité de l'arrêté attaqué : <br/>
<br/>
              7. Considérant qu'il ressort des pièces du dossier que, en application des dispositions de l'article L. 130-1 du code de l'urbanisme, la SCI L'Horizon a présenté le 12 juillet 2004 et complété le 23 septembre 2004 une demande d'autorisation d'abattage d'arbres dans un espace boisé classé par le plan d'occupation des sols, en vue de l'aménagement d'un chemin d'accès aux bâtiments sis sur sa propriété ; que l'arrêté du 1er décembre 2004 par lequel le maire de Gassin a rejeté cette demande est motivé par la circonstance que la réalisation du projet emporterait un changement d'affectation du sol de nature à compromettre la conservation et la protection de l'espace boisé classé, en méconnaissance des dispositions de l'article L. 130-1 du code de l'urbanisme, et qu'elle affecterait un espace remarquable au sens de l'article L. 146-6 du même code ; que la SCI L'Horizon fait appel du jugement du 22 janvier 2009 par lequel le tribunal administratif de Nice a rejeté sa demande tendant à l'annulation de cet arrêté ;<br/>
<br/>
              8. Considérant que la légalité du refus opposé à la SCI L'Horizon doit être appréciée au vu des mentions de la demande d'autorisation et non au vu des travaux illégalement exécutés sans autorisation dans l'espace boisé classé ; que la demande d'autorisation mentionne l'aménagement d'une voie d'accès aux bâtiments à usage d'habitation, qui sera laissée à l'état naturel et dont la largeur sera limitée à quatre mètres ;<br/>
<br/>
              9. Considérant, en premier lieu, qu'aux termes de l'article L. 130-1 du code de l'urbanisme, dans sa version en vigueur à la date des arrêtés attaqués : " Les plans locaux d'urbanisme peuvent classer comme espaces boisés, les bois, forêts, parcs à conserver, à protéger ou à créer, qu'ils relèvent ou non du régime forestier, enclos ou non, attenant ou non à des habitations. (..)./ Le classement interdit tout changement d'affectation ou tout mode d'occupation du sol de nature à compromettre la conservation, la protection ou la création des boisements./ (...) dans tout espace boisé classé, les coupes et abattages d'arbres sont soumis à autorisation préalable (...) " ; <br/>
<br/>
              10. Considérant qu'en raison de leur faible ampleur, les travaux mentionnés dans la demande d'autorisation et décrits ci-dessus  n'entraînent pas un changement d'affectation ou d'occupation du sol de nature à compromettre la conservation, la protection ou la création des boisements de l'espace boisé classé, qui serait prohibé par les dispositions de l'article L. 130-1 du code de l'urbanisme ; qu'ainsi, en se fondant sur un tel motif pour rejeter la demande d'autorisation qui lui était présentée, le maire de Gassin a fait une inexacte application de ces dispositions ;<br/>
<br/>
              11. Considérant, en second lieu, qu'il ressort des pièces du dossier que le terrain d'assiette du projet, situé dans un site inscrit par un arrêté ministériel du 15 février 1966 et dans un espace boisé classé par le plan d'occupation des sols, appartient à un ensemble boisé continu qui s'étend presque jusqu'au rivage de la presqu'île de Saint-Tropez ; que, dans ces conditions, il doit être regardé comme un espace protégé par les dispositions mentionnées ci-dessus de l'article L. 146-6 du code de l'urbanisme ;<br/>
<br/>
              12. Considérant que, si les dispositions de l'article R. 146-2 du code de l'urbanisme ne mentionnent pas, parmi les aménagements légers pouvant être implantés dans les espaces remarquables protégés par les dispositions de l'article L. 146-6, les aménagements nécessaires à la lutte contre l'incendie, elles n'ont ni pour objet ni pour effet d'interdire la réalisation de tels aménagements, à la condition qu'il s'agisse d'aménagements légers strictement nécessaires à cette fin ;<br/>
<br/>
              13. Considérant qu'il ressort des pièces du dossier que la propriété de la SCI L'Horizon est desservie par la voie carrossable dite " route de Ville Vieille " ; qu'elle comporte des bâtiments à usage d'habitation et que l'accès à ces bâtiments par les véhicules de lutte contre l'incendie depuis la route de Ville Vieille ne peut être correctement assuré par l'entrée ordinaire de la propriété, qui présente un tracé sinueux et ne se prête pas à des travaux d'aménagement ; que les travaux de faible ampleur mentionnés dans la demande d'autorisation et décrits ci-dessus n'excèdent pas ceux que le service départemental d'incendie et de secours du Var a estimé nécessaires pour permettre aux véhicules de lutte contre l'incendie d'accéder aux bâtiments, dans un secteur exposé à un risque majeur de feu de forêt ; que, dans ces conditions, ces travaux constituent des aménagements légers pouvant être légalement implantés dans l'espace remarquable protégé par les dispositions de l'article L. 146-6 du code de l'urbanisme ; qu'ainsi, en se fondant sur ces dispositions pour rejeter la demande d'autorisation qui lui était présentée, le maire de Gassin en a fait une inexacte application ;<br/>
<br/>
              14. Considérant qu'il résulte de ce qui précède que, sans qu'il soit besoin d'examiner les autres moyens de la requête, la SCI L'Horizon est fondée à soutenir que c'est à tort que, par le jugement n° 0500477 du 22 janvier 2009, le tribunal administratif de Nice a rejeté sa demande tendant à l'annulation de l'arrêté du 1er décembre 2004 par lequel le maire de Gassin a rejeté sa demande d'autorisation d'abattage d'arbres en vue de l'aménagement d'un chemin d'accès aux bâtiments sis sur sa propriété ; <br/>
<br/>
              Sur l'arrêté du 1er décembre 2004 par lequel le maire de Gassin a refusé l'autorisation d'abattage d'arbres en vue de l'aménagement d'un chemin autour des bâtiments : <br/>
<br/>
              15. Considérant qu'il ressort des pièces du dossier que, en application de l'article L. 130-1 du code de l'urbanisme, la SCI L'Horizon a présenté le 29 juillet 2004 et complété le 17 septembre 2004 une demande d'autorisation d'abattage d'arbres dans un espace boisé classé par le plan d'occupation des sols en vue de l'aménagement d'un chemin autour des bâtiments sis sur sa propriété ; que l'arrêté du 1er décembre 2004 par lequel le maire de Gassin a rejeté cette demande est motivé par la circonstance que la réalisation du projet emporterait un changement d'affectation du sol de nature à compromettre la conservation et la protection de l'espace boisé classé, en méconnaissance des dispositions de l'article L. 130-1 du code de l'urbanisme, et qu'elle affecterait un espace remarquable au sens de l'article L. 146-6 du même code ; que la SCI L'Horizon fait appel du jugement du 22 janvier 2009 par lequel le tribunal administratif de Nice a rejeté sa demande tendant à l'annulation de cet arrêté ;<br/>
<br/>
              16. Considérant qu'ainsi qu'il a été dit ci-dessus, il ressort des pièces du dossier que le terrain d'assiette du projet, situé dans un site inscrit par un arrêté ministériel du 15 février 1966 et dans un espace boisé classé par le plan d'occupation des sols, appartient à un ensemble boisé continu qui s'étend presque jusqu'au rivage de la presqu'île de Saint-Tropez ; que, dans ces conditions, il doit être regardé comme un espace protégé par les dispositions mentionnées ci-dessus de l'article L. 146-6 du code de l'urbanisme ;<br/>
<br/>
              17. Considérant que, si les dispositions de l'article R. 146-2 du code de l'urbanisme ne mentionnent pas, parmi les aménagements légers pouvant être implantés dans les espaces remarquables protégés par les dispositions de l'article L. 146-6, les aménagements nécessaires à la lutte contre l'incendie, elles n'ont ni pour objet ni pour effet d'interdire la réalisation de tels aménagements, à la condition qu'il s'agisse d'aménagements légers strictement nécessaires à cette fin ;<br/>
<br/>
              18. Considérant que, si la demande d'autorisation mentionne la réalisation d'un chemin qui sera laissé à l'état naturel et dont la largeur sera limitée à quatre mètres, elle mentionne également que les travaux nécessitent l'abattage d'une soixantaine d'arbres, en majorité des chênes lièges et pour le reste des pins maritimes sur une surface de 800 m2 ; qu'il ressort des pièces du dossier que, si le service départemental d'incendie et de secours du Var estimait que la réalisation de ce chemin pouvait présenter une utilité, il ne le jugeait pas indispensable à la protection des bâtiments contre l'incendie, se bornant à préconiser, outre l'aménagement du chemin d'accès aux bâtiments d'une largeur de quatre mètres, de simples travaux d'élagage et de débroussaillage ; que, dans ces conditions, le chemin décrit dans la demande d'autorisation ne constitue pas un aménagement léger strictement nécessaire à la protection des bâtiments contre l'incendie ; qu'ainsi, en se fondant sur les dispositions de l'article L. 146-6 du code de l'urbanisme pour rejeter la demande d'autorisation qui lui était présentée, le maire de Gassin en a fait une exacte application ; qu'il résulte de l'instruction qu'il aurait pris la même décision s'il n'avait retenu que ce seul motif ;<br/>
<br/>
              19. Considérant qu'il résulte de ce qui précède que, sans qu'il soit besoin de statuer sur les fins de non recevoir opposées par la commune de Gassin, la SCI L'Horizon n'est pas fondée à soutenir que c'est à tort que, par le jugement n° 0500480 du 22 janvier 2009, le tribunal administratif de Nice a rejeté sa demande tendant à l'annulation de l'arrêté du 1er décembre 2004 par lequel le maire de Gassin a refusé l'autorisation d'abattage d'arbres en vue de l'aménagement d'un chemin autour des bâtiments ;<br/>
<br/>
              Sur les conclusions à fin d'injonction :<br/>
<br/>
              20. Considérant que l'annulation de l'arrêté du 1er décembre 2004 par lequel le maire de Gassin a rejeté la demande d'autorisation d'abattage d'arbres en vue de l'aménagement d'un chemin d'accès aux bâtiments n'implique pas que le maire autorise les aménagements illégalement réalisés sans autorisation par la SCI L'Horizon ; qu'elle implique seulement que la demande d'autorisation concernant l'aménagement d'un chemin d'accès, telle qu'elle a été présentée, soit réexaminée ; qu'il y a lieu par suite, en application des dispositions de l'article L. 911-2 du code de justice administrative, d'enjoindre au maire de Gassin de procéder à ce réexamen dans le délai de quatre mois à compter de la notification de la présente décision ; <br/>
<br/>
              21. Considérant que, en tant qu'elle rejette les conclusions tendant à l'annulation de l'arrêté du 1er décembre 2004 par lequel le maire de Gassin a rejeté la demande d'autorisation d'abattage d'arbres en vue de l'aménagement d'un chemin autour des bâtiments, la présente décision n'implique aucune mesure d'exécution ;<br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative : <br/>
<br/>
              22. Considérant que ces dispositions font obstacle à ce qu'une somme soit mise à la charge de la SCI L'Horizon dans l'instance concernant l'arrêté du maire relatif à l'aménagement d'un chemin d'accès aux bâtiments ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de mettre dans cette instance une somme à la charge de la commune de Gassin ;<br/>
<br/>
              23. Considérant que ces mêmes dispositions font obstacle à ce qu'une somme soit mise à la charge de la commune de Gassin dans l'instance concernant l'arrêté du maire relatif à l'aménagement d'un chemin autour des bâtiments ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre dans cette instance à la charge de la SCI L'Horizon le versement à la commune de Gassin de la somme de 3 000 euros au titre de ces dispositions ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les arrêts de la cour administrative d'appel de Marseille du 10 février 2011 sont annulés.<br/>
<br/>
Article 2 : Le jugement n° 0500477 du 22 janvier 2009 du tribunal administratif de Nice est annulé.<br/>
<br/>
Article 3 : L'arrêté du 1er décembre 2004 par lequel le maire de Gassin a refusé à la SCI L'Horizon l'autorisation d'abattage d'arbres en vue de l'aménagement d'un chemin d'accès aux bâtiments est annulé. <br/>
<br/>
Article 4 : Il est enjoint au maire de Gassin de procéder, dans le délai de quatre mois à compter de la notification de la présente décision, au réexamen de la demande d'autorisation d'abattage d'arbres en vue de l'aménagement d'un chemin d'accès aux bâtiments présentée par la SCI L'Horizon.<br/>
<br/>
Article 5 : Le surplus des conclusions présentées par la SCI L'Horizon est rejeté.<br/>
<br/>
Article 6 : La SCI L'Horizon versera à la commune de Gassin la somme de 3 000 euros au titre de l'article L.761-1 du code de justice administrative.<br/>
<br/>
Article 7 : Le surplus des conclusions présentées par la commune de Gassin au titre de l'article L.761-1 du code de justice administrative est rejeté.<br/>
<br/>
Article 8 : La présente décision sera notifiée à la commune de Gassin et à la SCI L'Horizon. <br/>
Copie pour information en sera adressée à la ministre de l'égalité des territoires et du logement et à la ministre de l'écologie, du développement durable et de l'énergie.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">44-05 NATURE ET ENVIRONNEMENT. DIVERS RÉGIMES PROTECTEURS DE L`ENVIRONNEMENT. - ESPACES PROTÉGÉS SUR LESQUELS SEULS DES AMÉNAGEMENTS LÉGERS PEUVENT ÊTRE RÉALISÉS (ART. L. 146-6 ET R. 146-2 DU CODE DE L'URBANISME) - APPLICABILITÉ AUX AUTORISATIONS D'ABATTAGE D'ARBRES - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">68-04-042-01 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. AUTORISATIONS D`UTILISATION DES SOLS DIVERSES. AUTORISATIONS RELATIVES AUX ESPACES BOISÉS. AUTORISATION DE COUPE ET D'ABATTAGE D'ARBRES. - ESPACES PROTÉGÉS SUR LESQUELS SEULS DES AMÉNAGEMENTS LÉGERS PEUVENT ÊTRE RÉALISÉS (ART. L. 146-6 ET R. 146-2 DU CODE DE L'URBANISME) - 1) APPLICABILITÉ AUX AUTORISATIONS D'ABATTAGE D'ARBRES - EXISTENCE - 2) POSSIBILITÉ DE RÉALISER, SUR DES ESPACES PROTÉGÉS, DES AMÉNAGEMENTS NÉCESSAIRES À LA LUTTE CONTRE L'INCENDIE - EXISTENCE, QUAND BIEN MÊME DE TELS AMÉNAGEMENTS NE SONT PAS MENTIONNÉS À L'ARTICLE R. 146-2 - CONDITIONS.
</SCT>
<ANA ID="9A"> 44-05 Article L. 146-6 du code de l'urbanisme définissant les espaces protégés, sur lesquels seuls des aménagements légers, énumérés à l'article R. 146-2 du même code, peuvent être implantés.,,1) Les demandes d'autorisation d'abattage d'arbres ne peuvent être accordées sur des espaces protégés que si l'aménagement en vue duquel elles sont formées est autorisé par ces dispositions.,,2) Si les dispositions de l'article R. 146-2 du code de l'urbanisme ne mentionnent pas les aménagements nécessaires à la lutte contre l'incendie, elles n'ont ni pour objet ni pour effet d'interdire la réalisation de tels aménagements sur des espaces protégés, à la condition qu'il s'agisse d'aménagements légers strictement nécessaires à cette fin.</ANA>
<ANA ID="9B"> 68-04-042-01 Article L. 146-6 du code de l'urbanisme définissant les espaces protégés, sur lesquels seuls des aménagements légers, énumérés à l'article R. 146-2 du même code, peuvent être implantés.,,1) Les demandes d'autorisation d'abattage d'arbres ne peuvent être accordées sur des espaces protégés que si l'aménagement en vue duquel elles sont formées est autorisé par ces dispositions.,,2) Si les dispositions de l'article R. 146-2 du code de l'urbanisme ne mentionnent pas les aménagements nécessaires à la lutte contre l'incendie, elles n'ont ni pour objet ni pour effet d'interdire la réalisation de tels aménagements sur des espaces protégés, à la condition qu'il s'agisse d'aménagements légers strictement nécessaires à cette fin.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
