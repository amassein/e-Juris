<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043648163</ID>
<ANCIEN_ID>JG_L_2021_06_000000442464</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/64/81/CETATEXT000043648163.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 11/06/2021, 442464</TITRE>
<DATE_DEC>2021-06-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>442464</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI</AVOCATS>
<RAPPORTEUR>M. Clément Tonon</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie Roussel</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:442464.20210611</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
<br/>
              1° Sous le n° 442464, par une requête sommaire et un mémoire complémentaire, enregistrés les 4 août et 4 novembre 2020 au secrétariat du contentieux du Conseil d'Etat, la société Coopérative des Editeurs Libres et Indépendants et autres demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision n° 2020-0682 du 19 juin 2020 de l'Autorité de régulation des communications électroniques, des postes et de la distribution de la presse relative à la décision n° 2018-02 du Conseil supérieur des messageries de presse instituant une contribution exceptionnelle des éditeurs pour le financement des mesures de redressement du système collectif de distribution de la presse ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              2° Sous le n° 442775, par une requête et un mémoire en réplique, enregistrés le 13 août 2020 et le 7 janvier 2021 au secrétariat du contentieux du Conseil d'Etat, la société Marie Claire Album et autres demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision n° 2020-0682 du 19 juin 2020 de l'Autorité de régulation des communications électroniques, des postes et de la distribution de la presse relative à la décision n° 2018-02 du Conseil supérieur des messageries de presse instituant une contribution exceptionnelle des éditeurs pour le financement des mesures de redressement du système collectif de distribution de la presse ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              3° Sous le n° 446924, par une requête enregistrée le 26 novembre 2020 au secrétariat du contentieux du Conseil d'Etat, la société Financière de loisirs demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision n° 2020-0682 du 19 juin 2020 de l'Autorité de régulation des communications électroniques, des postes et de la distribution de la presse relative à la décision n° 2018-02 du Conseil supérieur des messageries de presse instituant une contribution exceptionnelle des éditeurs pour le financement des mesures de redressement du système collectif de distribution de la presse, ensemble la décision implicite de rejet de son recours gracieux tendant au retrait de ladite décision ;<br/>
<br/>
              2°) d'annuler la décision implicite de l'Autorité de régulation des communications électroniques, des postes et de la distribution de la presse rejetant sa demande d'abrogation de la décision n° 2018-02 du CSMP du 20 février 2018, rendue exécutoire par délibération de l'ARDP du 2 mars 2018 ;<br/>
<br/>
              3°) d'enjoindre à l'Autorité de régulation des communications électroniques, des postes et de la distribution de la presse d'abroger la décision n° 2018-02 du CSMP du 20 février 2018, rendue exécutoire par l'ARDP le 2 mars 2018 ;<br/>
<br/>
              4°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - la Constitution ;<br/>
              - le traité sur le fonctionnement de l'Union européenne, notamment ses articles 102 et 106 ;<br/>
              - la loi n° 47-585 du 2 avril 1947 modifiée par la loi n° 2019-1063 du 18 octobre 2019 ; <br/>
              - la loi n° 2019-1063 du 18 octobre 2019 ;<br/>
              - la décision du 28 janvier 2021 par laquelle le Conseil d'Etat, statuant au contentieux, n'a pas renvoyé au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par la société Coopérative des Editeurs Libres et Indépendants et autres ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - Le rapport de M. Clément Tonon, auditeur,<br/>
<br/>
              - Les conclusions de Mme Sophie Roussel, rapporteure publique,<br/>
<br/>
              La parole ayant été donnée après les conclusions à la SCP Spinosi, avocat de la société coopérative des éditeurs libres et indépendants et autres, et à la SCP Levis avocat de la société financière de loisirs et autres ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Les requêtes visées ci-dessus sont dirigées contre la décision n° 2020-0682 du 19 juin 2020 par laquelle l'Autorité de régulation des communications électroniques, des postes et de la distribution de la presse (ARCEP) a modifié la décision n° 2018-02 du Conseil supérieur des messageries de presse (CSMP) ayant institué une contribution exceptionnelle des éditeurs pour le financement des mesures de redressement du système collectif de distribution de la presse. La requête de la société Financière de loisirs présente en outre des conclusions tendant à l'annulation de la décision implicite de rejet de sa demande, présentée le 28 juillet 2020, d'abrogation de la décision n° 2018-02 du CSMP. Il y a lieu de les joindre pour statuer par une seule décision. <br/>
<br/>
              Sur les interventions :<br/>
<br/>
              2. Les sociétés Alternatives Economiques, FGH Invest, Fleurus Presse, Les éditions America, Les éditions Zadig, Panini France, Philo Edition, Sciences Humaines Communication, Scrineo, We Demain, Armada Concept, Diverti Editions, Editions Dipa Burda, Editions Hubert Burda Media, Editions Nuit et Jour, Grands Malades Editions, Le 15 du mois, Edicamp, CMI Publishing, Messageries Lyonnaises de Presse et MLP justifient, eu égard à la nature et l'objet du litige, d'un intérêt suffisant pour intervenir au soutien de la requête présentée sous le n° 442464. Leur intervention est, par suite, recevable.<br/>
<br/>
              3. Les sociétés Les Editions Larivière, Messageries Lyonnaises de Presse et MLP justifient, eu égard à la nature et l'objet du litige, d'un intérêt suffisant pour intervenir au soutien de la requête présentée sous le n° 442775. Leur intervention est, par suite, recevable.<br/>
<br/>
              4. Les sociétés Vagator Productions, Le.La Presse Editions, Les Editions La Vie du Rail, Beaux-Arts et Compagnie, Editions Audie, Roto Press Graphic, Editions Cité Press Impressions, Auto Hebdo Editions, Upside Down Média, Messageries Lyonnaises de Presse, MLP et le syndicat de l'Association des éditeurs de presse justifient, eu égard à la nature et l'objet du litige, d'un intérêt suffisant pour intervenir au soutien de la requête présentée sous le n° 446924. Leur intervention est, par suite, recevable.<br/>
<br/>
              Sur le cadre juridique :<br/>
<br/>
              5. D'une part aux termes de l'article 16 de la loi du 2 avril 1947 relative au statut des entreprises de groupage et de distribution des journaux et publications périodiques dans sa version issue de l'article 1er de la loi du 18 octobre 2019 relative à la modernisation de la distribution de la presse : " L'Autorité de régulation des communications électroniques, des postes et de la distribution de la presse est chargée de faire respecter les principes énoncés par la présente loi. Elle veille à la continuité territoriale et temporelle, à la neutralité et à l'efficacité économique de la distribution groupée de la presse ainsi qu'à une couverture large et équilibrée du réseau des points de vente. / Elle concourt à la modernisation de la distribution de la presse et au respect du pluralisme de la presse. " L'article 18 de la loi du 2 avril 1947, dans sa rédaction résultant de la même loi, dispose que : " Pour l'exécution des missions qui lui sont confiées par l'article 16, l'Autorité de régulation des communications électroniques, des postes et de la distribution de la presse : 1° Agrée les sociétés assurant la distribution de la presse dans le respect du cahier des charges mentionné à l'article 12 ; / 2° Est informée par chaque société agréée, deux mois avant leur entrée en vigueur, des conditions techniques, tarifaires et contractuelles de ses prestations. Dans un délai de deux mois à compter de cette transmission, elle émet un avis public sur ces conditions ou fait connaître ses observations à la société. Elle peut demander à la société de présenter une nouvelle proposition et, si nécessaire, modifier les conditions tarifaires ou suspendre leur application si elles ne respectent pas les principes de non-discrimination, d'orientation vers les coûts d'un opérateur efficace et de concurrence loyale. Elle peut également décider, pour assurer le respect de ces principes, d'un encadrement pluriannuel des tarifs de ces prestations. Elle rend publics les barèmes établis par les sociétés agréées au bénéfice de l'ensemble des clients ; / 3° Fixe les règles de répartition, entre toutes les entreprises de presse adhérant aux sociétés coopératives de groupage de presse utilisant les services des sociétés agréées de distribution de la presse, des coûts spécifiques et ne pouvant être évités induits par la distribution des quotidiens. Cette répartition s'effectue au prorata du chiffre d'affaires des entreprises de presse adhérant aux sociétés coopératives de groupage de presse ; / 4° Définit, par dérogation à l'article 3, les circonstances dans lesquelles une entreprise de presse peut, dans des zones géographiques déterminées et pour des motifs tirés de l'amélioration des conditions de desserte des points de vente, recourir à une distribution groupée sans adhérer à une société coopérative de groupage de presse ; elle précise dans ce cas les modalités de participation de l'entreprise à la répartition des coûts spécifiques mentionnés au 3° du présent article ; / 5° Est informée par les organisations professionnelles représentatives concernées de l'ouverture de leurs négociations en vue de la conclusion de l'accord interprofessionnel mentionné au 2° de l'article 5 ou d'un avenant à cet accord, reçoit communication de cet accord ou avenant et émet un avis public sur sa conformité aux principes énoncés par la présente loi. En cas de non-conformité de cet accord ou avenant ou de carence des parties dûment constatée au terme de six mois suivant l'ouverture des négociations ou, le cas échéant, suivant l'expiration de l'accord ou de l'avenant, l'autorité définit les règles d'assortiment des titres et de détermination des quantités servies aux points de vente ; / 6° Précise les règles mentionnées à l'article 14 relatives aux conditions d'implantation des points de vente et fixe, après avoir recueilli l'avis de leurs organisations professionnelles représentatives, les conditions de rémunération des diffuseurs de presse qui gèrent ces points de vente ; / 7° Rend public un schéma territorial d'orientation de la distribution de la presse prenant en compte les dépositaires centraux de presse. "<br/>
<br/>
              6. D'autre part, aux termes du 1° du V de l'article 12 de la loi du 18 octobre 2019 : " Les décisions prises par l'Autorité de régulation de la distribution de la presse et le Conseil supérieur des messageries de presse avant la date de la réunion précitée sont maintenues de plein droit jusqu'à décision contraire de l'Autorité de régulation des communications électroniques, des postes et de la distribution de la presse ". <br/>
<br/>
              7. Sur le fondement des dispositions de la loi du 2 avril 1947 dans leur rédaction antérieure à la loi 18 octobre 2019, le CSMP a, par une décision n° 2018-02 du 20 février 2018, rendue exécutoire par délibération de l'Autorité de régulation de la distribution de la presse (ARDP) n° 2018-02 du 2 mars 2018, mis à la charge des éditeurs de presse, afin de les faire participer au plan de redressement des deux messageries de presse, Presstalis et les messageries lyonnaises de Presse (MLP), une contribution exceptionnelle dont elle a défini le régime. Cette contribution a été instituée avec un taux égal à 2,25 % et une durée de dix semestres pour les éditeurs adhérents des coopératives relevant de Presstalis et 1 % et neuf semestres pour ceux relevant de MLP. Par un arrêt du 16 mai 2019, la cour d'appel de Paris, alors compétente pour connaître des recours en annulation ou en réformation des décisions du CSMP, a rejeté le recours en annulation contre cette décision. A la suite de l'engagement d'une procédure de redressement judiciaire de la société Presstalis, l'ARCEP a modifié cette décision pour substituer, à cette société, comme bénéficiaire du produit de la contribution de 2,25 %, la société de distribution de presse lui succédant.<br/>
<br/>
              Sur les conclusions à fin d'annulation de la décision n° 2020-0682 du 19 juin 2020 :<br/>
<br/>
              En ce qui concerne la légalité externe :<br/>
<br/>
              8. En premier lieu, par sa décision du 28 janvier 2021, le Conseil d'Etat, statuant au contentieux, a décidé qu'il n'y avait pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par la société Coopérative des Editeurs Libres et Indépendants et autres à l'encontre des dispositions des articles 16 et 18 de la loi du 2 avril 1947. Il s'ensuit que le moyen tiré de ce que la décision attaquée serait privée de base légale et méconnaîtrait les droits et libertés garantis par la Constitution en tant qu'elle est fondée sur ces dispositions ne peut qu'être écarté.<br/>
<br/>
              9. En deuxième lieu, il résulte des dispositions citées au point 5 qu'en confiant à l'ARCEP les missions de régulation de la distribution de la presse précédemment assurées par le CSMP et l'ARDP, le législateur ne l'a pas dotée, par ces dispositions, du pouvoir, mis en oeuvre par ces institutions dans la décision du 20 février 2018, de faire contribuer financièrement les éditeurs au redressement des messageries de presse. Par suite, l'ARCEP n'est compétente, sur le fondement de ces dispositions, ni pour instituer une telle contribution, ni pour en modifier l'économie.<br/>
<br/>
              10. Toutefois, en prévoyant, par le 1° du V de l'article 12 de la loi du 18 octobre 2019, que les décisions prises par les précédentes autorités de régulation, en particulier la décision du 20 février 2018, restaient en vigueur jusqu'à que l'ARCEP en décide autrement, le législateur a entendu lui permettre non seulement de les abroger mais également de procéder aux adaptations rendues strictement nécessaires pour leur maintien en vigueur. Par suite, en se bornant à prévoir, par la décision modificative litigieuse, que dans l'hypothèse où la société Presstalis viendrait à disparaître, la société retenue pour reprendre ses activités bénéficierait du produit de la contribution instituée en 2018 sans en modifier ni le taux, ni la durée, ni un autre élément de son régime, l'ARCEP n'a pas entaché sa décision d'incompétence.<br/>
<br/>
              11. En troisième lieu, aux termes de l'article 21 de la loi du 2 avril 1947 précitée : " Lorsque l'Autorité de régulation des communications électroniques, des postes et de la distribution de la presse envisage d'adopter des mesures ayant une incidence importante sur le marché de la distribution de la presse, elle rend publiques les mesures envisagées dans un délai raisonnable avant leur adoption et recueille les observations qui sont faites à leur sujet. L'Autorité rend public le résultat de ces consultations, sous réserve des secrets protégés par la loi. " La décision attaquée a, ainsi qu'il a été dit, pour seul objet de transférer le produit de la contribution en vigueur au bénéfice de la société reprenant les activités de distribution de la presse de la société Presstalis. Par suite, elle ne peut être regardée comme ayant une incidence importante sur le marché de la distribution de la presse et le moyen tiré de ce que l'ARCEP était tenue de mettre en oeuvre la procédure de consultation prévue par l'article 21 de la loi du 2 avril 1947 ne peut qu'être écarté.<br/>
<br/>
              12. En quatrième lieu, la décision attaquée n'a pas été prise sur le fondement de l'article 22 de la loi du 2 avril 1947 dans sa version issue de l'article 1er de la loi du 18 octobre 2019, qui permet à l'ARCEP de prendre des mesures provisoires visant à faire cesser une atteinte ou une menace d'atteinte grave et immédiate à la continuité de la distribution de la presse d'information politique et générale et prévoit une obligation de motivation des décisions prises dans ce cadre. Elle procède, en outre, à la modification de dispositions de nature réglementaire. Elle n'avait, par suite, pas à être motivée.<br/>
<br/>
              En ce qui concerne la légalité interne :<br/>
<br/>
              13. En premier lieu, ainsi qu'il a été dit ci-dessus la décision litigieuse a pour seul objet de modifier le bénéficiaire de la contribution instituée en 2018. Par suite, les sociétés requérantes ne peuvent utilement soutenir qu'elle porterait par elle-même une atteinte directe aux principes constitutionnels de liberté contractuelle, d'égalité devant les charges publiques, de liberté d'entreprendre, de respect du droit de propriété et au principe de sécurité juridique, ni qu'elle méconnaîtrait les articles 102 et 106 du traité sur le fonctionnement de l'Union européenne relatifs à l'abus de position dominante.<br/>
<br/>
              14. En deuxième lieu, la décision litigieuse a prévu, sous condition de l'approbation du plan de cession des activités de distribution de la presse de la société Presstalis par le tribunal de commerce dans le cadre du redressement judiciaire de cette société, l'attribution du produit de la contribution au cessionnaire retenu et l'affectation de ces sommes au financement de ce plan de cession. Ce faisant, alors qu'il apparaissait d'une part que dans le cadre de la procédure de redressement judiciaire avec une période d'observation prenant fin le 15 juillet 2020, une seule offre de reprise avait été présentée fin mai, précisée les 10 et 11 juin suivants, qui émanait d'un des deux actionnaires de la société Presstalis, d'autre part qu'à défaut d'acceptation de cette offre il serait procédé à la liquidation de cette société avec les conséquences qu'impliquait cette liquidation sur la diffusion de la presse d'information politique et générale, l'ARCEP s'est bornée à modifier la décision instituant la contribution exceptionnelle dans la seule mesure nécessaire à son maintien au profit de l'activité de distribution de presse assurée jusqu'alors par Presstalis et objet du plan de cession analysé dans le cadre de la procédure de redressement judiciaire. Par suite, elle n'a, contrairement à ce qui est soutenu, pas entaché sa décision d'erreur d'appréciation. <br/>
<br/>
              Sur les conclusions à fin d'annulation de la décision implicite de rejet de la demande d'abrogation de la décision n° 2018-02 du CSMP du 20 février 2018 :<br/>
<br/>
              15. Ainsi qu'il a été dit plus haut, la décision de l'ARCEP a substitué, comme bénéficiaire de la contribution exceptionnelle instaurée par la décision du CSMP, la société France Messagerie à la société Presstalis. Par suite, la société Financière de loisirs ne peut utilement soutenir, par l'unique moyen de ses conclusions, que la décision du CSMP, dont elle sollicitait l'abrogation, a été privée de fondement juridique par la disparition de la société Presstalis. Ses conclusions à fin d'annulation et, par voie de conséquence, ses conclusions à fin d'injonction doivent par suite être rejetées.<br/>
<br/>
              16. Il résulte de tout ce qui précède que les requêtes de la société Coopérative des Editeurs Libres et Indépendants et autres, de la société Marie Claire Album et autres et de la société Financière de loisirs doivent être rejetées.<br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              17. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à qu'il soit fait droit aux conclusions présentées, d'une part, par la société Coopérative des Editeurs Libres et Indépendants et autres, la société Marie Claire Album et autres et la société Financière de loisirs et, d'autre part, par les sociétés Vagator Productions, Le.La Presse Editions, Les Editions La Vie du Rail, Beaux-Arts et Compagnie, Editions Audie, Roto Press Graphic, Editions Cité Press Impressions, Auto Hebdo Editions et le syndicat de l'Association des éditeurs de presse, qui ne sont qu'intervenants, tendant à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
                       --------------<br/>
<br/>
Article 1er : Les interventions des sociétés Alternatives Economiques, FGH Invest, Fleurus Presse, Les éditions America, Les éditions Zadig, Panini France, Philo Edition, Sciences Humaines Communication, Scrineo, We Demain, Armada Concept, Diverti Editions, Editions Dipa Burda, Editions Hubert Burda Media, Editions Nuit et Jour, Grands Malades Editions, Le 15 du mois, Edicamp, Les Editions Larivière, Messageries Lyonnaises de Presse, MLP, Vagator Productions, Le.La Presse Editions, Les Editions La Vie du Rail, Beaux-Arts et Compagnie, Editions Audie, Roto Press Graphic, Editions Cité Press Impressions, Auto Hebdo Editions, Upside Down Media, CMI Publishing et du syndicat de l'Association des éditeurs de presse sont admises. <br/>
<br/>
Article 2 : Les requêtes présentées, sous le n° 442464 par la société Coopérative des Editeurs Libres et Indépendants et autres, sous le n° 442775 par la société Marie Claire Album et autres et, sous le n° 446924 par la société Financière de loisirs sont rejetées.<br/>
<br/>
Article 3 : Les conclusions présentées par les sociétés intervenantes sur le fondement de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la société Coopérative des Editeurs Libres et Indépendants, désignée représentante unique sous le n° 442464, à la société Alternatives Economiques, premier intervenant dénommé sous le n° 442464, à la société Marie Claire Album, désignée représentante unique sous le n° 442775, à la société Les Editions Larivière, à la société Mesageries Lyonnaises de Presse, à la société Financière de loisirs, à la société Vagator Productions, premier intervenant dénommé sous le n° 446924, à la société France Messagerie et à l'Autorité de régulation des communications électroniques, des postes et de la distribution de la presse. Copie en sera adressée à la ministre de la culture.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">51-005 POSTES ET COMMUNICATIONS ÉLECTRONIQUES. - CONTRIBUTION EXCEPTIONNELLE DES ÉDITEURS, INSTITUÉE PAR LE CSMP AFIN DE LES FAIRE PARTICIPER AU PLAN DE REDRESSEMENT DES SOCIÉTÉS DE DISTRIBUTION DE PRESSE - COMPÉTENCE DE L'ARCEP - 1) POUR INSTITUER UNE TELLE CONTRIBUTION OU EN MODIFIER L'ÉCONOMIE - ABSENCE - 2) POUR SUBSTITUER AU BÉNÉFICIAIRE INITIAL DE LA CONTRIBUTION LA SOCIÉTÉ DE DISTRIBUTION DE PRESSE AYANT REPRIS SES ACTIVITÉS - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">53-04 PRESSE. FONCTIONNEMENT DES ENTREPRISES DE PRESSE. - CONTRIBUTION EXCEPTIONNELLE DES ÉDITEURS, INSTITUÉE PAR LE CSMP AFIN DE LES FAIRE PARTICIPER AU PLAN DE REDRESSEMENT DES SOCIÉTÉS DE DISTRIBUTION DE PRESSE - COMPÉTENCE DE L'ARCEP - 1) POUR INSTITUER UNE TELLE CONTRIBUTION OU EN MODIFIER L'ÉCONOMIE - ABSENCE - 2) POUR SUBSTITUER AU BÉNÉFICIAIRE INITIAL DE LA CONTRIBUTION LA SOCIÉTÉ DE DISTRIBUTION DE PRESSE AYANT REPRIS SES ACTIVITÉS - EXISTENCE.
</SCT>
<ANA ID="9A"> 51-005 Décision du Conseil supérieur des messageries de presse (CSMP), rendue exécutoire par délibération de l'Autorité de régulation de la distribution de la presse (ARDP), mettant à la charge des éditeurs de presse, afin de les faire participer au plan de redressement des deux messageries de presse, Presstalis et les messageries lyonnaises de Presse (MLP), une contribution exceptionnelle dont elle a défini le régime.,,,Décision par laquelle l'Autorité de régulation des communications électroniques, des postes et de la distribution de la presse (ARCEP), à la suite de l'engagement d'une procédure de redressement judiciaire de la société Presstalis, a modifié la décision du CSMP pour substituer, à cette société, comme bénéficiaire du produit de la contribution, la société de distribution de presse lui succédant.,,,1) Il résulte des articles 16 et 18 de la loi n° 47-585 du 2 avril 1947 qu'en confiant à l'ARCEP les missions de régulation de la distribution de la presse précédemment assurées par le CSMP et l'ARDP, le législateur ne l'a pas dotée, par ces dispositions, du pouvoir, mis en oeuvre par ces institutions, de faire contribuer financièrement les éditeurs au redressement des messageries de presse.... ...Par suite, l'ARCEP n'est compétente, sur le fondement de ces dispositions, ni pour instituer une telle contribution, ni pour en modifier l'économie.,,,2) Toutefois, en prévoyant, par le 1° du V de l'article 12 de la loi n° 2019-1063 du 18 octobre 2019, que les décisions prises par les précédentes autorités de régulation restaient en vigueur jusqu'à que l'ARCEP en décide autrement, le législateur a entendu lui permettre non seulement de les abroger mais également de procéder aux adaptations rendues strictement nécessaires pour leur maintien en vigueur.... ,,Par suite, en se bornant à prévoir, par la décision modificative litigieuse, que dans l'hypothèse où la société Presstalis viendrait à disparaître, la société retenue pour reprendre ses activités bénéficierait du produit de la contribution exceptionnelle sans en modifier ni le taux, ni la durée, ni un autre élément de son régime, l'ARCEP n'a pas entaché sa décision d'incompétence.</ANA>
<ANA ID="9B"> 53-04 Décision du Conseil supérieur des messageries de presse (CSMP), rendue exécutoire par délibération de l'Autorité de régulation de la distribution de la presse (ARDP), mettant à la charge des éditeurs de presse, afin de les faire participer au plan de redressement des deux messageries de presse, Presstalis et les messageries lyonnaises de Presse (MLP), une contribution exceptionnelle dont elle a défini le régime.,,,Décision par laquelle l'Autorité de régulation des communications électroniques, des postes et de la distribution de la presse (ARCEP), à la suite de l'engagement d'une procédure de redressement judiciaire de la société Presstalis, a modifié la décision du CSMP pour substituer, à cette société, comme bénéficiaire du produit de la contribution, la société de distribution de presse lui succédant.,,,1) Il résulte des articles 16 et 18 de la loi n° 47-585 du 2 avril 1947 qu'en confiant à l'ARCEP les missions de régulation de la distribution de la presse précédemment assurées par le CSMP et l'ARDP, le législateur ne l'a pas dotée, par ces dispositions, du pouvoir, mis en oeuvre par ces institutions, de faire contribuer financièrement les éditeurs au redressement des messageries de presse.... ...Par suite, l'ARCEP n'est compétente, sur le fondement de ces dispositions, ni pour instituer une telle contribution, ni pour en modifier l'économie.,,,2) Toutefois, en prévoyant, par le 1° du V de l'article 12 de la loi n° 2019-1063 du 18 octobre 2019, que les décisions prises par les précédentes autorités de régulation restaient en vigueur jusqu'à que l'ARCEP en décide autrement, le législateur a entendu lui permettre non seulement de les abroger mais également de procéder aux adaptations rendues strictement nécessaires pour leur maintien en vigueur.... ,,Par suite, en se bornant à prévoir, par la décision modificative litigieuse, que dans l'hypothèse où la société Presstalis viendrait à disparaître, la société retenue pour reprendre ses activités bénéficierait du produit de la contribution exceptionnelle sans en modifier ni le taux, ni la durée, ni un autre élément de son régime, l'ARCEP n'a pas entaché sa décision d'incompétence.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
