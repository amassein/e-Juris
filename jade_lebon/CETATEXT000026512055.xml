<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026512055</ID>
<ANCIEN_ID>JG_L_2012_10_000000354495</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/51/20/CETATEXT000026512055.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 4ème sous-sections réunies, 19/10/2012, 354495</TITRE>
<DATE_DEC>2012-10-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>354495</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 4ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BLANC, ROUSSEAU</AVOCATS>
<RAPPORTEUR>M. Jean-Dominique Langlais</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Fabienne Lambolez</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:354495.20121019</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 1er et 13 décembre 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. Jean-Marie B, demeurant ... ; M. B demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'ordonnance n° 11VE02380 du 16 novembre 2011 par laquelle le juge des référés de la cour administrative d'appel de Versailles a rejeté son appel contre l'ordonnance n° 1101163 du 23 juin 2011 du juge des référés du tribunal administratif de Cergy-Pontoise rejetant sa demande tendant à ce que soit ordonnée une expertise aux fins de déterminer l'ensemble des préjudices résultant pour lui d'un accident de service dont il a été victime le 24 septembre 2002 ; <br/>
<br/>
              2°) statuant en référé, de faire droit à son appel ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement d'une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu les pièces dont il résulte que le pourvoi a été communiqué à la Mutuelle générale de la police qui n'a pas produit de mémoire ;<br/>
<br/>
              Vu la loi n° 83-634 du 13 juillet 1983 ;<br/>
<br/>
              Vu la loi n° 84-16 du 11 janvier 1984 ;<br/>
<br/>
              Vu la loi n° 2003-239 du 18 mars 2003 ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Langlais, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Blanc, Rousseau avocat de M. B ;<br/>
<br/>
              - les conclusions de Mme Fabienne Lambolez, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Blanc, Rousseau avocat de M. B ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes du premier alinéa de l'article R. 532-1 du code de justice administrative : " Le juge des référés peut, sur simple requête et même en l'absence de décision administrative préalable, prescrire toute mesure utile d'expertise ou d'instruction " ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis au juge des référés de la cour administrative d'appel de Versailles que M. B, brigadier de police, a subi le 24 septembre 2002, dans l'exercice de ses fonctions, des violences qui ont entraîné pour lui une contusion du coude droit et des douleurs lombaires et un placement en congé de maladie jusqu'au 18 décembre 2002, reconnus comme imputables au service ; qu'il a saisi le tribunal administratif de Cergy-Pontoise d'une demande tendant à l'annulation de la décision du 24 septembre 2010 par laquelle le préfet des Yvelines a, sur le fondement du deuxième alinéa du 2° de l'article 34 de la loi du 11 janvier 1984 relative à la fonction publique de l'Etat, refusé de prendre en charge comme imputables à cet accident de service " une lombo-sciatique gauche L5-S1 et une lombalgie droite " ; que M. B a ensuite introduit devant le juge des référés du même tribunal une demande tendant à ce qu'une expertise soit ordonnée sur le fondement de l'article R. 532-1 du code de justice administrative, aux fins, notamment, de déterminer l'ensemble des préjudices résultant pour lui de l'accident de service du 24 septembre 2002, lesquels consistent selon lui en des souffrances physiques et morales, des préjudices esthétique et d'agrément ainsi que des troubles dans ses conditions d'existence, dans le but d'engager une action tendant à la condamnation de l'Etat à l'indemniser de ces préjudices ; <br/>
<br/>
              3. Considérant que, par l'ordonnance attaquée du 16 novembre 2011, le juge des référés de la cour administrative d'appel de Versailles a confirmé l'ordonnance du 23 juin 2011 par laquelle le juge des référés du tribunal administratif de Cergy-Pontoise a rejeté cette demande d'expertise ; qu'il s'est fondé, pour ce faire, sur le motif que l'expertise demandée aurait été dépourvue du caractère d'utilité exigé par les dispositions de l'article R. 532-1 du code de justice administrative, dès lors qu'il appartenait au tribunal administratif, saisi de la demande de M. B tendant à l'annulation de la décision du 24 septembre 2010 refusant de le faire bénéficier des dispositions du deuxième alinéa du 2° de l'article 34 de la loi du 11 janvier 1984, d'ordonner, le cas échéant, toute mesure d'expertise qu'il estimerait utile ; <br/>
<br/>
              4. Considérant qu'en statuant ainsi, alors que ces dernières dispositions ont pour seul objet de mettre à la charge de l'Etat, lorsqu'un fonctionnaire est victime d'un accident de service ou d'une maladie imputable au service, le versement de l'intégralité du traitement de l'intéressé ainsi que le coût des honoraires médicaux et des frais directement entraînés par la maladie ou l'accident, et qu'il résultait des pièces soumises à son examen que l'expertise sollicitée l'était en vue d'une action future tendant à la condamnation de l'Etat à l'indemniser de préjudices distincts, le juge des référés de la cour administrative d'appel de Versailles a entaché son ordonnance d'erreur de droit ; que, par suite, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, M. B est fondé à en demander l'annulation ;<br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement à M. B d'une somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du 16 novembre 2011 du juge des référés de la cour administrative d'appel de Versailles est annulée.<br/>
<br/>
Article 2 : L'affaire est renvoyée au juge des référés de la cour administrative d'appel de Versailles.<br/>
<br/>
Article 3 : L'Etat versera à M. B la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à M. Jean-Marie B, au ministre de l'intérieur et à la Mutuelle générale de la police.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-03-011-04 PROCÉDURE. PROCÉDURES D'URGENCE. RÉFÉRÉ TENDANT AU PRONONCÉ D'UNE MESURE D'EXPERTISE OU D'INSTRUCTION. CONDITIONS. - UTILITÉ - CONDITION - MESURE PRÉSENTANT UN CARACTÈRE D'UTILITÉ DIFFÉRENT DE LA MESURE QUE POURRAIT ORDONNER LE JUGE DANS LE CADRE DE L'INSTRUCTION DE LA REQUÊTE DONT L'A ÉGALEMENT SAISI LE REQUÉRANT [RJ1] - CONDITION REMPLIE - CAS DANS LEQUEL L'EXPERTISE EST DEMANDÉE EN VUE D'UNE ACTION INDEMNITAIRE CONCERNANT DES PRÉJUDICES DISTINCTS DE CEUX QUE COUVRIRAIT LE BÉNÉFICE DE DISPOSITIONS LÉGISLATIVES, REFUSÉ À L'INTÉRESSÉ PAR UNE DÉCISION EN LITIGE.
</SCT>
<ANA ID="9A"> 54-03-011-04 Fonctionnaire ayant déposé une demande tendant à ce qu'une expertise soit ordonnée sur le fondement de l'article R. 532-1 du code de justice administrative, aux fins, notamment, de déterminer l'ensemble des préjudices résultant pour lui d'un accident de service, lesquels consistent selon lui en des souffrances physiques et morales, des préjudices esthétique et d'agrément ainsi que des troubles dans ses conditions d'existence, dans le but d'engager une action tendant à la condamnation de l'Etat à l'indemniser de ces préjudices.,,L'utilité d'une telle mesure d'expertise ne saurait être déniée au motif que l'intéressé a également saisi le juge administratif d'une requête tendant à l'annulation d'une décision refusant de le faire bénéficier des dispositions du deuxième alinéa du 2° de l'article 34 de la loi n° 84-16 du 11 janvier 1984, dès lors que ces dispositions ont pour seul objet de mettre à la charge de l'Etat, lorsqu'un fonctionnaire est victime d'un accident de service ou d'une maladie imputable au service, le versement de l'intégralité du traitement de l'intéressé ainsi que le coût des honoraires médicaux et des frais directement entraînés par la maladie ou l'accident : l'action indemnitaire envisagée concernait des préjudices distincts.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, juge des référés, 30 septembre 1998, Association 3M France, n° 199166, T. p. 1092.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
