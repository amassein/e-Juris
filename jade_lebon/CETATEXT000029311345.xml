<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029311345</ID>
<ANCIEN_ID>JG_L_2014_07_000000370306</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/31/13/CETATEXT000029311345.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème et 9ème sous-sections réunies, 30/07/2014, 370306, Publié au recueil Lebon</TITRE>
<DATE_DEC>2014-07-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>370306</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème et 9ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Romain Godet</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CESSR:2014:370306.20140730</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu 1°, sous le n° 370306, la requête sommaire et le mémoire complémentaire, enregistrés les 18 juillet et 15 octobre 2013 au secrétariat du contentieux du Conseil d'Etat, présentés par l'association dissoute " Envie de rêver ", dont le siège est 92, rue de Javel à Paris (75015), représentée par son ancien président, et par M. B...D..., demeurant ... ; les requérants demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret du 12 juillet 2013 portant dissolution d'une association et de deux groupements de fait, en tant qu'il dissout l'association requérante ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761 1 du code de justice administrative, ainsi que les dépens ;<br/>
<br/>
<br/>
              Vu 2°, sous le n° 372180, la requête, enregistrée le 13 septembre 2013 au secrétariat du contentieux du Conseil d'Etat, présentée par M. C...A..., demeurant ... ; M. A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le même décret, en tant qu'il porte dissolution des groupements de fait " Troisième voie " et " Jeunesses nationalistes révolutionnaires " ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative, ainsi que les dépens ;<br/>
<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 15 juillet 2014, présentée par le ministre de l'intérieur ;<br/>
<br/>
              Vu la Constitution, notamment son Préambule ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu le code de la sécurité intérieure, dans sa rédaction issue de l'ordonnance n° 2012-351 du 12 mars 2012 ;<br/>
<br/>
              Vu la loi du 1er juillet 1901 relative au contrat d'association ;<br/>
<br/>
              Vu la loi n° 79-587 du 11 juillet 1979 ;<br/>
<br/>
              Vu la loi n° 2000-321 du 12 avril 2000 ;<br/>
<br/>
              Vu la loi n° 2011-267 du 14 mars 2011 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Romain Godet, maître des requêtes en service extraordinaire,<br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que les requêtes de l'association " Envie de rêver " et de M. D..., d'une part, et de M.A..., d'autre part, sont dirigées contre le même décret prononçant la dissolution de l'association " Envie de rêver " et des groupements de fait " Troisième voie " et " Jeunesses nationalistes révolutionnaires " ; qu'il y a lieu de les joindre pour statuer par une seule décision ;<br/>
<br/>
              2. Considérant que le décret attaqué a été pris en application des 2° et 6° de l'article L. 212-1 du code de la sécurité intérieure, lesquels prévoient la dissolution d'associations ou groupements de fait qui " présentent, par leur forme et leur organisation militaires, le caractère de groupes de combat ou de milices privées " ou qui " soit provoquent à la discrimination, à la haine ou à la violence envers une personne ou un groupe de personnes à raison de leur origine ou de leur appartenance ou de leur non-appartenance à une ethnie, une nation, une race ou une religion déterminée, soit propagent des idées ou théories tendant à justifier ou encourager cette discrimination, cette haine ou cette violence " ;<br/>
<br/>
              Sur les moyens dirigés contre l'ensemble du décret :<br/>
<br/>
              3. Considérant, en premier lieu, que, d'après les dispositions combinées des articles 13 et 19 de la Constitution, les décrets délibérés en conseil des ministres doivent être signés par le Président de la République et contresignés par le Premier ministre et, le cas échéant, les ministres responsables ; que les ministres responsables sont ceux auxquels incombe, à titre principal, la préparation et l'application des décrets dont il s'agit ; que le ministre de la justice ne peut, eu égard à l'objet du décret attaqué, être regardé comme ayant la qualité de ministre responsable au sens de ces dispositions ; que, dès lors, le moyen tiré de ce que ce décret aurait dû être contresigné par ce ministre doit être écarté ; <br/>
<br/>
              4. Considérant, en second lieu, que l'article 102 de la loi du 14 mars 2011 a autorisé le Gouvernement, dans les conditions prévues à l'article 38 de la Constitution, à procéder par ordonnance à l'adoption de la partie législative du code de la sécurité intérieure, en vue de regrouper, notamment, les dispositions législatives relatives à la sécurité publique ; que les dispositions de l'article L. 212-1 de ce code de la sécurité intérieure, issues de l'ordonnance du 12 mars 2012, qui n'a pas été ratifiée, présentent de ce fait un caractère réglementaire ; <br/>
<br/>
              5. Considérant que le principe de la liberté d'association, tel qu'il résulte des dispositions générales de la loi du 1er juillet 1901, constitue l'un des principes fondamentaux reconnus par les lois de la République, et solennellement réaffirmés par le Préambule de la Constitution ; qu'il appartient au Gouvernement, lorsqu'il est habilité par le Parlement à intervenir dans le domaine de la loi sur le fondement de l'article 38 de la Constitution, d'opérer la conciliation nécessaire entre le respect des libertés et la sauvegarde de l'ordre public sans lequel l'exercice des libertés ne saurait être assuré ; que, eu égard aux motifs susceptibles de conduire, sous le contrôle du juge de l'excès de pouvoir, au prononcé de la dissolution d'associations ou de groupements de fait, les dispositions de l'article L. 212-1 du code de la sécurité intérieure répondent à la nécessité de sauvegarder l'ordre public, compte tenu de la gravité des troubles qui sont susceptibles de lui être portés par les associations et groupements visés par ces dispositions ; que, par ailleurs, le prononcé d'une telle dissolution, qui doit être motivé par application de la loi du 11 juillet 1979, ne peut intervenir, en vertu des dispositions de l'article 24 de la loi du 12 avril 2000, qu'au terme d'une procédure contradictoire permettant aux représentants de l'association ou du groupement de fait en cause de présenter des observations écrites et, le cas échéant, orales ; que, dans ces conditions, les dispositions de l'article L. 212-1 du code de la sécurité intérieure ne portent pas une atteinte excessive au principe de la liberté d'association ;<br/>
<br/>
              Sur la dissolution des groupements de fait " Troisième voie " et " Jeunesses nationalistes révolutionnaires " :<br/>
<br/>
              6. Considérant, en premier lieu, qu'en permettant à l'autorité administrative de prononcer la dissolution d'associations ou de groupements de fait qui, par leur objet ou leur activité réelle, se placent dans l'une des situations de fait qu'il énumère, l'article L. 212-1 du code de la sécurité intérieure tend à assurer la sauvegarde de l'ordre public tant en vue de mettre un terme à ces situations qu'en vue d'éviter leur renouvellement ; qu'à cet effet, il incrimine le " maintien ou la reconstitution d'une association ou d'un groupement dissous en application du présent article, ou l'organisation de ce maintien ou de cette reconstitution " et réprime ces infractions par l'application de sanctions pénales prévues par la section 4 du chapitre Ier du titre III du livre IV du code pénal ; qu'ainsi, une mesure de dissolution peut être prononcée, sur le fondement de cet article, à l'égard d'une association ou d'un groupement de fait dont les organes statutaires ou les dirigeants auraient prononcé la dissolution lorsque l'activité de l'entité volontairement dissoute s'est maintenue, le cas échéant dans le cadre d'un groupement de fait, consécutivement à cette dissolution et que, par suite, cette dissolution n'a eu d'autre objet que d'éviter l'application des incriminations pénales prévues par l'article L. 212-1 ; <br/>
<br/>
              7. Considérant que si les requérants font grief au décret d'avoir prononcé la dissolution, en tant que groupements de fait, de " Troisième voie " et des " Jeunesses nationalistes révolutionnaires ", alors que ces entités avaient été préalablement dissoutes par décision de leurs organes dirigeants dès le 18 juin 2013, il ressort des pièces du dossier que cette dissolution volontaire n'avait pas immédiatement mis un terme à leur activité après cette date, laquelle s'était poursuivie au cours de la procédure contradictoire préalable à l'édiction du décret ; qu'en outre, eu égard au contexte dans lequel est intervenue cette dissolution volontaire et à la date à laquelle elle a été décidée, cette dissolution doit être regardée comme n'ayant eu d'autre objet que d'éviter l'application des sanctions pénales précitées ; que, dès lors, le décret a pu légalement regarder " Troisième voie " et les " Jeunesses nationalistes révolutionnaires " comme des groupements de fait susceptibles de faire l'objet d'une mesure de dissolution sur le fondement de l'article L. 212-1 ; <br/>
<br/>
              8. Considérant, en deuxième lieu, qu'il ne ressort pas des pièces versées au dossier que " Troisième Voie " et les " Jeunesses nationalistes révolutionnaires " auraient, ensemble ou séparément, par leurs activités et notamment par leurs écrits, leurs déclarations ou leurs actions collectives, provoqué à la haine, à la discrimination ou à la violence au sens du 6° de l'article L. 212-1 du code de la sécurité intérieure ; <br/>
<br/>
              9. Considérant, toutefois, d'une part, que le groupement de fait " Jeunesses nationalistes révolutionnaires " constituait une organisation hiérarchisée, rassemblée autour de son chef, M.A..., avec comme devise : " Croire, combattre, obéir " ; que ses membres étaient recrutés selon des critères d'aptitude physique pour, le cas échéant, mener des actions de force en cas " d'affrontement " et qu'enfin, ceux-ci procédaient à des rassemblements, notamment sur la voie publique, en uniformes et en cortèges d'aspect martial ; qu'à raison de ces caractéristiques, ce groupement de fait constituait une milice privée au sens du 2° de l'article L. 212-1 du code de la sécurité intérieure ; <br/>
<br/>
              10. Considérant, d'autre part, que les " Jeunesses nationalistes révolutionnaires " et " Troisième voie " étaient, par leur organisation, leur fonctionnement et leurs activités, étroitement imbriqués dès lors que, ainsi qu'il ressort des pièces du dossier, ces entités partageaient les mêmes dirigeants et poursuivaient les mêmes objectifs et que les membres des " Jeunesses nationalistes révolutionnaires " prenaient une part active aux événements, rassemblements ou manifestations, de toute nature, suscités ou organisés par " Troisième voie " ; que cette imbrication était telle qu'elle a conduit M. A...à prononcer, en tant que dirigeant de ce groupement, la dissolution des " Jeunesses nationalistes révolutionnaires " " par voie de conséquence de la dissolution " de l'association " Troisième voie " ; qu'ainsi, ces deux entités devant être regardées comme formant ensemble une milice privée au sens du 2° de l'article L. 212-1 du code de la sécurité intérieure, le décret attaqué pouvait légalement prononcer leur dissolution sur ce fondement ;<br/>
<br/>
               11. Considérant qu'il résulte de l'instruction que le Gouvernement aurait pris la même décision s'il n'avait retenu que ce seul motif pour prononcer la dissolution de ces deux groupements ;<br/>
<br/>
              12. Considérant, en dernier lieu, qu'eu égard aux motifs sur lesquels repose la mesure de dissolution ainsi prise à l'égard de " Troisième voie " et des " Jeunesses nationalistes révolutionnaires ", le décret attaqué n'a pas méconnu les stipulations des articles 10 et 11 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; qu'en particulier, si les dissolutions critiquées constituent une restriction à l'exercice de la liberté d'expression, celle-ci est justifiée par la gravité des dangers pour l'ordre public et la sécurité publique résultant des activités des groupements en cause ;<br/>
<br/>
              13. Considérant qu'il résulte de tout ce qui précède que les requérants ne sont pas fondés à demander l'annulation du décret attaqué en tant qu'il prononce la dissolution des groupements de fait " Troisième voie " et " Jeunesses nationalistes révolutionnaires " ;<br/>
<br/>
              Sur la dissolution de l'association " Envie de rêver " :<br/>
<br/>
              14. Considérant qu'il résulte des dispositions de l'article L. 212-1 du code de la sécurité intérieure que peuvent être dissous, sur ce fondement, les associations ou groupements de fait qui par leurs agissements, et nonobstant leur objet légal ou les activités qu'ils affichent publiquement, se placent dans l'une des situations mentionnées aux 1° à 7° de cet article et contreviennent ainsi à l'ordre public ; que le décret attaqué a prononcé la dissolution de l'association " Envie de rêver " aux motifs qu'elle n'aurait " pour seule activité réelle que de permettre la tenue des réunions de 'Troisième Voie' et des 'Jeunesses Nationalistes Révolutionnaires' et de constituer ainsi l'instrument de leur propagande de haine et de discrimination envers les personnes, à raison de leur non-appartenance à la nation française ; que, dépourvue de toute autre activité, elle se confond dans l'ensemble plus vaste de ces deux structures dont elle constitue un moyen matériel de leur activité illicite " ; <br/>
<br/>
              15. Considérant, d'une part, que la circonstance que des membres ou dirigeants de " Troisième Voie " et des " Jeunesses nationalistes révolutionnaires " se réunissaient dans le local de l'association " Envie de rêver " et participaient aux activités organisées ou accueillies par celle-ci qui, si elles s'inscrivaient dans des thématiques relevant pour partie des idées véhiculées par deux groupements, n'avaient pas ce seul objet, ne saurait suffire, à elle seule, à caractériser une atteinte à l'ordre public susceptible, à raison de la qualification de milice privée, au sens du 2° de l'article L. 212-1 précité, reconnue aux groupements de faits " Troisième Voie " et " Jeunesses nationalistes révolutionnaires ", de conduire à sa dissolution sur le fondement de cette même disposition ;<br/>
<br/>
              16. Considérant, d'autre part, qu'il ne ressort pas des pièces du dossier que, lors des différents événements organisés ou accueillis par l'association " Envie de rêver " dans ses locaux, celle-ci aurait provoqué à la discrimination, à la haine ou à la violence au sens du 6° de l'article L. 212-1 ; qu'il n'est pas davantage établi que la forme, l'organisation ou les activités de cette association puissent la faire regarder elle-même comme une milice privée au sens du 2° du même article ; qu'en conséquence, c'est en méconnaissance des dispositions de cet article que le décret attaqué a prononcé la dissolution de cette association ;<br/>
<br/>
              17. Considérant qu'il résulte de tout ce qui précède que, sans qu'il soit besoin d'examiner leurs autres moyens, l'association " Envie de rêver " et M. D...sont fondés à demander l'annulation du décret attaqué en tant qu'il prononce la dissolution de cette  association ; <br/>
<br/>
              18. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme globale de 2 000 euros à verser conjointement à l'association " Envie de rêver " et à M.D..., au titre des dispositions des articles L. 761-1 et R. 761-1 du code de justice administrative ; qu'en revanche, les mêmes dispositions font obstacle à ce qu'une somme à verser à M. A...soit mise à ce titre à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante ; qu'enfin, il y a lieu de laisser la contribution à l'aide juridique à la charge de M. A...;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'article 1er du décret du 12 juillet 2013 est annulé en tant qu'il prononce la dissolution de l'association " Envie de rêver ".<br/>
Article 2 : L'Etat versera à l'association " Envie de rêver " et à M.D..., ensemble, une somme globale de 2 000 euros au titre des articles L. 761-1 et R. 761-1 du code de justice administrative.<br/>
Article 3 : La requête de M. A...est rejetée.<br/>
Article 4 : La présente décision sera notifiée à l'association " Envie de rêver ", à M. B...D..., à M. C...A..., au Premier ministre et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">10-01-04-01 ASSOCIATIONS ET FONDATIONS. QUESTIONS COMMUNES. DISSOLUTION. ASSOCIATIONS ET GROUPEMENTS DE FAIT - LOI DU 10 JANVIER 1936. - 1) ASSOCIATION OU GROUPEMENT AYANT FAIT L'OBJET D'UNE DISSOLUTION VOLONTAIRE - POSSIBILITÉ DE PRONONCER UNE DISSOLUTION SUR LE FONDEMENT DE L'ARTICLE L. 212-1 DU CODE DE LA SÉCURITÉ INTÉRIEURE - A) EXISTENCE, DÈS LORS QUE L'ACTIVITÉ S'EST MAINTENUE ET QUE LA DISSOLUTION N'A DONC EU POUR OBJET QUE D'ÉVITER LES SANCTIONS PÉNALES - B) ESPÈCE - 2) NOTION DE MILICE PRIVÉE (2° DE L'ART. L. 212-1 DU CODE DE LA SÉCURITÉ INTÉRIEURE) - A) INCLUSION - I) GROUPEMENT DOTÉ D'UNE ORGANISATION HIÉRARCHISÉE ET D'UNE DEVISE, D'UN MODE DE RECRUTEMENT ET D'ACTIVITÉS À CARACTÈRE MARTIAL -  II) GROUPEMENT ÉTROITEMENT IMBRIQUÉ PAR SON ORGANISATION, SON FONCTIONNEMENT ET SON ACTIVITÉ AVEC UN AUTRE PRÉSENTANT LE CARACTÈRE D'UNE MILICE - B) EXCLUSION - ASSOCIATION ACCUEILLANT LES RÉUNIONS DE MEMBRES DE MILICES PRIVÉES MAIS DONT LES ACTIVITÉS N'ONT PAS CE SEUL OBJET.
</SCT>
<ANA ID="9A"> 10-01-04-01 1) a) Une mesure de dissolution peut être prononcée, sur le fondement de l'article L. 212-1 du code de la sécurité intérieure, à l'égard d'une association ou d'un groupement de fait dont les organes statutaires ou les dirigeants auraient prononcé la dissolution lorsque l'activité de l'entité volontairement dissoute s'est maintenue, le cas échéant dans le cadre d'un groupement de fait, consécutivement à cette dissolution et que, par suite, cette dissolution n'a eu d'autre objet que d'éviter l'application des incriminations pénales prévues par l'article L. 212-1.,,,b) En l'espèce, la dissolution volontaire, antérieure au décret de dissolution, de  Troisième voie  et des  Jeunesses nationalistes révolutionnaires , n'avait pas immédiatement mis un terme à leur activité, laquelle s'était poursuivie au cours de la procédure contradictoire préalable à l'édiction du décret de dissolution. En outre, eu égard au contexte dans lequel est intervenue cette dissolution volontaire et à la date à laquelle elle a été décidée, elle doit être regardée comme n'ayant eu d'autre objet que d'éviter l'application des sanctions pénales. Dès lors, le décret a pu légalement regarder  Troisième voie  et les  Jeunesses nationalistes révolutionnaires  comme des groupements de fait susceptibles de faire l'objet d'une mesure de dissolution sur le fondement de l'article L. 212-1 du code de la sécurité intérieure.,,,2) a) i) Le groupement de fait  Jeunesses nationalistes révolutionnaires  constituait une organisation hiérarchisée, rassemblée autour de son chef, avec comme devise :  Croire, combattre, obéir . Ses membres étaient recrutés selon des critères d'aptitude physique pour, le cas échéant, mener des actions de force en cas  d'affrontement  et ils procédaient à des rassemblements, notamment sur la voie publique, en uniformes et en cortèges d'aspect martial. A raison de ces caractéristiques, ce groupement de fait constituait une milice privée au sens du 2° de l'article L. 212-1 du code de la sécurité intérieure.,,,ii) Les groupements de fait  Jeunesses nationalistes révolutionnaires  et  Troisième voie  étaient, par leur organisation, leur fonctionnement et leurs activités, étroitement imbriqués dès lors que ces entités partageaient les mêmes dirigeants, poursuivaient les mêmes objectifs et que les membres des  Jeunesses nationalistes révolutionnaires  prenaient une part active aux événements, rassemblements ou manifestations, de toute nature, suscités ou organisés par  Troisième voie . Cette imbrication était telle qu'elle a conduit le dirigeant des  Jeunesses nationalistes révolutionnaires  à prononcer leur dissolution  par voie de conséquence de la dissolution  de l'association  Troisième voie . Ainsi, ces deux entités doivent être regardées comme formant ensemble une milice privée au sens du 2° de l'article L. 212-1 du code de la sécurité intérieure.,,,b) La circonstance que des membres ou dirigeants de  Troisième Voie  et des  Jeunesses nationalistes révolutionnaires  se réunissaient dans le local de l'association  Envie de rêver  et participaient aux activités organisées ou accueillies par celle-ci qui, si elles s'inscrivaient dans des thématiques relevant pour partie des idées véhiculées par des deux groupements, n'avaient pas ce seul objet, ne saurait suffire, à elle seule, à caractériser une atteinte à l'ordre public susceptible, à raison de la qualification de milice privée, au sens du 2° de l'article L. 212-1, reconnue aux groupements de faits  Troisième Voie  et  Jeunesses nationalistes révolutionnaires , de conduire à la dissolution de l'association  Envie de rêver  sur le fondement de ce même article.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
