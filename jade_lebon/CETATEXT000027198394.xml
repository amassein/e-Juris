<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027198394</ID>
<ANCIEN_ID>JG_L_2013_03_000000343434</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/19/83/CETATEXT000027198394.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 4ème sous-sections réunies, 20/03/2013, 343434</TITRE>
<DATE_DEC>2013-03-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>343434</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 4ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOULLOCHE ; SCP GADIOU, CHEVALLIER ; SCP CELICE, BLANCPAIN, SOLTNER ; SCP BORE ET SALVE DE BRUNETON</AVOCATS>
<RAPPORTEUR>Mme Domitille Duval-Arnould</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:343434.20130320</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 22 septembre et 22 décembre 2010 au secrétariat du contentieux du Conseil d'Etat, présentés pour la société mutuelle d'assurances du bâtiment et des travaux publics (SMABTP), dont le siège est 114 avenue Emile Zola à Paris (75015) ; la SMABTP demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'arrêt n° 08PA02167 du 2 juillet 2010 par lequel la cour administrative d'appel de Paris a rejeté sa requête en annulation du jugement n° 0210636/6-2 du 26 février 2008 du tribunal administratif de Paris rejetant sa demande de condamnation de M. A... et des sociétés " Bureau Veritas " et " Darras et Jouanin " à lui rembourser la somme de 141 162,24 euros, versée à la société d'économie mixte d'équipement et d'aménagement du quinzième arrondissement de Paris (SEMEA XV), dans le cadre de sa garantie dommages-ouvrage, au titre de mesures provisoires et d'investigations consécutives à l'apparition des désordres affectant la façade de l'école qu'elle avait rénovée ;<br/>
<br/>
              2°) réglant l'affaire au fond, de condamner M. A...et les sociétés " Bureau Veritas " et " Darras et Jouanin " à lui verser la somme de 141 162,24 euros ainsi que la somme de 665 009,37 euros au titre des travaux de réparation, avec intérêts au taux légal et capitalisation de ces intérêts ;<br/>
<br/>
              3°) de  mettre à la charge de ces derniers la somme de 2 100 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
	Vu le code des assurances ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Domitille Duval-Arnould, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les observations de la SCP Gadiou, Chevallier, avocat de la société mutuelle d'assurances du bâtiment et des travaux publics (SMABTP), de la SCP Boré et Salve de Bruneton, avocat de la société " Bureau Veritas ", de la SCP Célice, Blancpain, Soltner, avocat de la société " Darras et Jouanin " et de la SCP Boulloche, avocat de M.A...,<br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Gadiou, Chevallier, avocat de la société mutuelle d'assurances du bâtiment et des travaux publics (SMABTP), à la SCP Boré et Salve de Bruneton, avocat de la société " Bureau veritas ", à la SCP Célice, Blancpain, Soltner, avocat de la société " Darras et Jouanin " et à la SCP Boulloche, avocat de M.A... ;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la ville de Paris a confié la rénovation d'une école élémentaire, située 50-54 rue Gutenberg, à la société d'économie mixte d'équipement et d'aménagement du quinzième arrondissement de Paris (SEMEA XV), qui a souscrit une assurance de dommages-ouvrage auprès de la société mutuelle d'assurances du bâtiment et des travaux publics (SMABTP) ; que la société " Darras et Jouanin " a été chargée, sous la maîtrise d'oeuvre de M. B...A..., architecte, et le contrôle technique de la société " Bureau Veritas ", de l'exécution des travaux, qui ont été réceptionnés le 28 juillet 1992 ; qu'à la suite de l'apparition de désordres sur les façades de l'école, la SEMEA XV a sollicité la garantie de la SMABTP, qui lui a été accordée par décision du 2 mars 1998 ; qu'après avoir pris en charge les frais engagés pour sécuriser les lieux et recherché la cause des désordres, la SMABTP a saisi le tribunal administratif de Paris d'une demande de condamnation de M. A..., de la société " Darras et Jouanin " et de la société " Bureau Veritas " à lui rembourser la somme de 141 162, 24 euros ; que cette demande a été rejetée par un jugement du 26 février 2008, confirmé par un arrêt du 2 juillet 2010 de la cour administrative d'appel de Paris, contre lequel la SMABTP se pourvoit en cassation ; <br/>
<br/>
              2. Considérant, d'une part, qu'aux termes de l'article L. 242-1 du code des assurances, l'assurance de dommages, souscrite pour son compte ou pour celui des propriétaires successifs par toute personne physique ou morale qui, agissant en qualité de propriétaire de l'ouvrage, de vendeur ou de mandataire du propriétaire de l'ouvrage, fait réaliser des travaux de bâtiment, garantit " (...) en dehors de toute recherche des responsabilités, le paiement de la totalité des travaux de réparation des dommages de la nature de ceux dont sont responsables les constructeurs au sens de l'article 1792-1 du code civil " ; qu'aux termes de l'article L. 121-12 du code des assurances, " l'assureur qui a payé l'indemnité d'assurance est subrogé, jusqu'à concurrence de cette indemnité, dans les droits et actions de l'assuré contre les tiers qui, par leur fait, ont causé le dommage ayant donné lieu à la responsabilité de l'assureur " ; qu'il résulte de ces dispositions que l'assurance de dommages est une assurance de choses bénéficiant au maître de l'ouvrage et aux propriétaires successifs ou à ceux qui sont subrogés dans leurs droits et que l'assureur qui a pris en charge la réparation de dommages ayant affecté l'ouvrage de la nature de ceux dont sont responsables les constructeurs au sens de l'article 1792-1 du code civil se trouve subrogé dans les droits et actions du propriétaire à l'encontre des constructeurs ;<br/>
<br/>
              3. Considérant qu'en rejetant la demande de la SMABTP, qui avait pris en charge, dans le cadre de la garantie dommages-ouvrage, le coût des opérations nécessaires pour  sécuriser les lieux et rechercher la cause de désordres de la nature de ceux dont sont responsables les constructeurs au sens de l'article 1792-1 du code civil, aux motifs que la mission de la SEMEA XV s'était achevée le 28 juillet 1992 lors de la réception des travaux litigieux et que seule la ville de Paris avait qualité pour mettre en jeu la responsabilité décennale des constructeurs, alors que l'assurance de dommages souscrite par la SEMEA XV bénéficiait à la ville de Paris et que la SMABTP était subrogée dans les droits de cette dernière, la cour a commis une erreur de droit ; que, par suite et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, la SMABTP est fondée à demander l'annulation de l'arrêt du 2 juillet 2010 ;<br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de M.A..., de la société " Bureau Veritas " et de la société " Darrras et Jouanin " une somme de 700 euros  chacun à verser à la SMABTP au titre des dispositions de l'article L. 761-1 du code de justice administrative ; que ces dispositions font obstacle à ce qu'une somme soit mise à la charge de la SMABTP qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 2 juillet 2010 de la cour administrative d'appel de Paris est annulé.<br/>
<br/>
		Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Paris.<br/>
<br/>
Article 3 : M.A..., la société " Bureau Veritas " et la société " Darras et Jouanin " verseront chacun à la société SMABTP une somme de 700 euros au titre de l'article L.761-1 du code de justice administrative.<br/>
<br/>
Article 4 : Les conclusions de M.A..., de la société " Bureau Veritas " et de la société " Darrras et Jouanin " présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 5 : La présente décision sera notifiée à la société SMABTP, à M. B...A..., à la société " Bureau Veritas " et à la société " Darras et Jouanin ".<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">60-05-03-02 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RECOURS OUVERTS AUX DÉBITEURS DE L'INDEMNITÉ, AUX ASSUREURS DE LA VICTIME ET AUX CAISSES DE SÉCURITÉ SOCIALE. SUBROGATION. SUBROGATION DE L'ASSUREUR. - ASSURANCE DE DOMMAGES (ART. L. 242-1 DU CODE DES ASSURANCES) - ASSURANCE DE CHOSES - EXISTENCE - BÉNÉFICIAIRES - MAÎTRE DE L'OUVRAGE OU PROPRIÉTAIRES SUCCESSIFS OU CEUX SUBROGÉS DANS LEURS DROITS [RJ1] - ASSUREUR AYANT PRIS EN CHARGE LA RÉPARATION DE DOMMAGES DE NATURE DÉCENNALE - INCLUSION.
</SCT>
<ANA ID="9A"> 60-05-03-02 Il résulte des dispositions des articles L. 242-1 et L. 121-12 du code des assurances que l'assurance de dommages souscrite pour son compte ou pour celui des propriétaires successifs par toute personne physique ou morale qui, agissant en qualité de propriétaire de l'ouvrage, de vendeur ou de mandataire du propriétaire de l'ouvrage, fait réaliser des travaux de bâtiment, est une assurance de choses bénéficiant au maître de l'ouvrage et aux propriétaires successifs ou à ceux qui sont subrogés dans leurs droits. L'assureur qui a pris en charge la réparation de dommages ayant affecté l'ouvrage de la nature de ceux dont sont responsables les constructeurs au sens de l'article 1792-1 du code civil se trouve subrogé dans les droits et actions du propriétaire à l'encontre des constructeurs.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr. Cass. civ. 1ère, 8 octobre 1996, n° 96-20275, Bull. n° 238.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
