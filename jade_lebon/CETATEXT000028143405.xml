<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028143405</ID>
<ANCIEN_ID>JG_L_2013_10_000000370393</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/14/34/CETATEXT000028143405.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème et 2ème sous-sections réunies, 25/10/2013, 370393</TITRE>
<DATE_DEC>2013-10-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>370393</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème et 2ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GASCHIGNARD ; SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>M. Frédéric Dieu</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:370393.20131025</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 22 juillet et 7 août 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour la commune de La Seyne-sur-Mer, représentée par son maire ; la commune de La Seyne-sur-Mer demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 1301578 du 4 juillet 2013 par laquelle le juge des référés du tribunal administratif de Toulon, statuant en application de l'article L. 551-13 du code de justice administrative, a, sur la requête de la société Miramar, annulé le sous-traité d'exploitation du lot n° 1 de la plage des Sablettes à La Seyne-sur-Mer conclu le 27 mai 2013 avec M. B... et décidé que cette annulation prendrait effet à l'expiration d'un délai de quatre mois à compter de la date de l'ordonnance ;<br/>
<br/>
              2°) statuant en référé, de rejeter la demande de la société Miramar ;<br/>
<br/>
              3°) de mettre à la charge de cette dernière une somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des collectivités territoriales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Dieu, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gaschignard, avocat de la commune de La Seyne-sur-Mer, et à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la société Miramar ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 551-13 du code de justice administrative : " Le président du tribunal administratif, ou le magistrat qu'il délègue, peut être saisi, une fois conclu l'un des contrats mentionnés aux articles L. 551-1 et L. 551-5, d'un recours régi par la présente section " ; qu'aux termes de l'article L. 551-18 du code de justice administrative : " Le juge prononce la nullité du contrat lorsqu'aucune des mesures de publicité requises pour sa passation n'a été prise, ou lorsque a été omise une publication au Journal officiel de l'Union européenne dans le cas où une telle publication est prescrite. / La même annulation est prononcée lorsque ont été méconnues les modalités de remise en concurrence prévues pour la passation des contrats fondés sur un accord-cadre ou un système d'acquisition dynamique. / Le juge prononce également la nullité du contrat lorsque celui-ci a été signé avant l'expiration du délai exigé après l'envoi de la décision d'attribution aux opérateurs économiques ayant présenté une candidature ou une offre ou pendant la suspension prévue à l'article L. 551-4 ou à l'article L. 551-9 si, en outre, deux conditions sont remplies : la méconnaissance de ces obligations a privé le demandeur de son droit d'exercer le recours prévu par les articles L. 551-1 et L. 551-5, et les obligations de publicité et de mise en concurrence auxquelles sa passation est soumise ont été méconnues d'une manière affectant les chances de l'auteur du recours d'obtenir le contrat " ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis au juge des référés que la commune de La Seyne-sur-Mer (Var) a lancé en octobre 2012 une procédure d'appel d'offres en vue de l'attribution d'un contrat de délégation de service public portant sur l'exploitation du lot de plage n° 1 dit de la " plage des Sablettes " ; qu'à l'issue de la phase de négociation à laquelle elle a été admise avec M. B..., la société Miramar s'est vu notifier le rejet de son offre par un courrier du 17 mai 2013 ; que, sur le fondement de l'article L. 551-1 du code de justice administrative, la société Miramar a saisi le juge des référés du tribunal administratif de Toulon d'une demande d'annulation de la procédure de passation du contrat ; que, par une ordonnance du 30 mai 2013, le juge des référés a rejeté cette demande comme irrecevable, le contrat ayant été signé le 27 mai 2013 ; que, par l'ordonnance attaquée, le juge des référés du tribunal administratif de Toulon a, à la demande de la société Miramar, annulé le contrat conclu par la commune de La Seyne-sur-Mer et M. B...sur le fondement des dispositions des articles L. 551-13 et L. 551-18 du code de justice administrative relatives au référé contractuel ;<br/>
<br/>
              3. Considérant que les cas dans lesquels le juge des référés peut annuler un contrat sont limitativement énumérés aux trois alinéas de l'article L 551-18 précité ; que s'agissant des contrats de délégation de service public, qui ne sont ni soumis à l'obligation, pour le pouvoir adjudicateur ou l'entité adjudicatrice, visée au troisième alinéa de l'article L. 551-18, de notifier aux opérateurs économiques ayant présenté une offre, avant la signature du contrat, la décision d'attribution, ni concernés par le deuxième alinéa de cet article relatif à des marchés publics fondés sur un accord cadre ou un système d'acquisition dynamique, l'annulation d'un tel contrat ne peut résulter que du constat des manquements mentionnés au premier alinéa de cet article L. 551-18, c'est-à-dire de l'absence de toutes les mesures de publicité requises pour sa passation ou d'une publication au Journal officiel de l'Union européenne dans le cas où une telle publication est prescrite ; que, par suite, les candidats à l'attribution d'un contrat de délégation de service public ne peuvent invoquer utilement, à l'appui de leurs conclusions tendant à l'annulation du contrat présentées dans le cadre d'un référé contractuel, que les manquements de l'autorité délégante à ses obligations de publicité visées au premier alinéa de l'article L. 551-18 du code de justice administrative ; <br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède qu'en jugeant que la société Miramar était fondée à demander l'annulation du contrat de délégation de service public conclu par la commune de La Seyne-sur-Mer et M. B...sur le fondement du troisième alinéa de l'article L. 551-18 du code de justice administrative, alors que ces dispositions ne sont pas applicables aux contrats de délégation de service public, le juge des référés du tribunal administratif de Toulon a commis une erreur de droit ; que, par suite, sans qu'il soit besoin d'examiner les moyens du pourvoi, l'ordonnance du juge des référés du tribunal administratif de Toulon du 4 juillet 2013 doit être annulée ;<br/>
<br/>
              5. Considérant que dans les circonstances de l'espèce, il y a lieu, en application de l'article L. 821-2 du code de justice administrative, de régler l'affaire au titre de la procédure de référé engagée par la société Miramar ; <br/>
<br/>
              6. Considérant, d'une part, qu'aux termes de l'article L. 551-14 de ce code : " Les personnes habilitées à agir sont celles qui ont un intérêt à conclure le contrat et qui sont susceptibles d'être lésées par des manquements aux obligations de publicité et de mise en concurrence auxquelles sont soumis ces contrats, ainsi que le représentant de l'Etat dans le cas des contrats passés par une collectivité territoriale ou un établissement public local. / Toutefois, le recours régi par la présente section n'est pas ouvert au demandeur ayant fait usage du recours prévu à l'article L. 551-1 ou à l'article L. 551-5 dès lors que le pouvoir adjudicateur ou l'entité adjudicatrice a respecté la suspension prévue à l'article L. 551-4 ou à l'article L. 551-9 et s'est conformé à la décision juridictionnelle rendue sur ce recours " ; qu'aux termes de l'article L. 551-15 du code de justice administrative : " Le recours régi par la présente section ne peut être exercé ni à l'égard des contrats dont la passation n'est pas soumise à une obligation de publicité préalable lorsque le pouvoir adjudicateur ou l'entité adjudicatrice a, avant la conclusion du contrat, rendu publique son intention de le conclure et observé un délai de onze jours après cette publication, ni à l'égard des contrats soumis à publicité préalable auxquels ne s'applique pas l'obligation de communiquer la décision d'attribution aux candidats non retenus lorsque le pouvoir adjudicateur ou l'entité adjudicatrice a accompli la même formalité (...) " ; qu'aux termes de l'article R. 1411-2-1 du code général des collectivités territoriales : " Pour rendre applicables les dispositions du premier alinéa de l'article L. 551-15 du code de justice administrative, l'autorité responsable de la personne publique délégante publie au Bulletin officiel d'annonces des marchés publics un avis, conforme au modèle fixé par arrêté conjoint du ministre chargé de l'intérieur et du ministre chargé de l'économie, relatif à son intention de conclure la délégation de service public. Elle doit alors respecter un délai d'au moins onze jours entre la date de publication de cet avis et la date de conclusion du contrat " ;<br/>
<br/>
              7. Considérant qu'il résulte de ces dispositions que sont seuls recevables à saisir le juge du référé contractuel d'une demande dirigée contre un contrat de délégation de service public, lequel n'est pas soumis à l'obligation de communiquer la décision d'attribution aux candidats non retenus, outre le préfet, les candidats qui n'ont pas engagé un référé précontractuel, lorsque l'autorité délégante n'a pas rendu publique son intention de conclure le contrat dans les conditions prévues par l'article R. 1411-2-1 du code général des collectivités territoriales et n'a pas observé, avant de le signer, un délai d'au moins onze jours entre la date de publication de l'avis prévu par cet article et la date de conclusion du contrat, ainsi que ceux qui ont engagé un référé précontractuel, lorsque l'autorité délégante n'a pas respecté l'obligation de suspendre la signature du contrat prévue aux articles L. 551-4 ou L. 551-9 du code de justice administrative ou ne s'est pas conformé à la décision juridictionnelle rendue sur ce référé ;<br/>
<br/>
              8. Considérant qu'il résulte de l'instruction que la commune de La Seyne-sur-Mer n'a pas rendu publique son intention de conclure le contrat de délégation de service public portant sur l'exploitation du lot de plage n° 1 dit de la " plage des Sablettes " dans les conditions prévues, pour les délégations de service public, par l'article R. 1411-2-1 du code général des collectivités territoriales ; que, par suite et nonobstant la circonstance que la commune a notifié au candidat non retenu la décision de rejeter son offre et de retenir celle de M. B... en précisant dans cette décision qu'elle respecterait un délai de suspension de seize jours minimum avant de conclure le contrat, délai minimum qu'elle n'a au demeurant pas respecté, la société Miramar est recevable à saisir le juge du référé contractuel d'une demande tendant à l'annulation de ce contrat ;<br/>
<br/>
              9. Considérant, toutefois, que la société Miramar se prévaut uniquement de manquements visés au troisième alinéa de l'article L. 551-18 du code de justice administrative ; qu'ainsi qu'il a été dit au point 3 de la présente décision, les candidats à l'attribution d'une délégation de service public ne peuvent utilement invoquer ces dispositions pour demander au juge du référé contractuel l'annulation du contrat ; que, par suite, sa demande tendant à ce que soit prononcée la nullité du contrat de délégation de service public ne peut qu'être rejetée ;<br/>
<br/>
              10. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mis à la charge de la commune de La Seyne-sur-Mer qui n'est pas, dans la présente instance, la partie perdante, le versement d'une somme au titre des frais exposés par la société Miramar et non compris dans les dépens ; qu'il y a lieu, en revanche, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par la commune de La Seyne-sur-Mer en application de ces mêmes dispositions et de mettre à la charge de la société Miramar, au titre de l'ensemble de la procédure, le versement d'une somme de 4 500 euros ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du juge des référés du tribunal administratif de Toulon du 4 juillet 2013 est annulée.<br/>
Article 2 : La demande présentée par la société Miramar devant le juge des référés du tribunal administratif de Toulon et ses conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La société Miramar versera à la commune de La Seyne-sur-Mer, pour l'ensemble de la procédure, une somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la commune de La Seyne-sur-Mer, à la société Miramar et à M. A... B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-08-015-02 MARCHÉS ET CONTRATS ADMINISTRATIFS. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. PROCÉDURES D'URGENCE. - CONTRAT DE DÉLÉGATION DE SERVICE PUBLIC - 1) PERSONNES RECEVABLES À FORMER UN RÉFÉRÉ CONTRACTUEL ET CAS D'OUVERTURE [RJ1] - 2) MANQUEMENTS UTILEMENT INVOCABLES - MANQUEMENTS DE L'AUTORITÉ DÉLÉGANTE À SES OBLIGATIONS DE PUBLICITÉ MENTIONNÉES AU PREMIER ALINÉA DE L'ARTICLE L. 551-18 DU CJA - EXISTENCE - MANQUEMENTS AUX OBLIGATIONS MENTIONNÉES AUX DEUXIÈME ET TROISIÈME ALINÉA DU MÊME ARTICLE - ABSENCE - AUTRES CAS D'ANNULATION - ABSENCE.
</SCT>
<ANA ID="9A"> 39-08-015-02 1) Il résulte des articles L. 551-14 et L. 551-15 du code de justice administrative (CJA) et de l'article R. 1411-2-1 du code général des collectivités territoriales (CGCT) que sont seuls recevables à saisir le juge du référé contractuel d'une demande dirigée contre une délégation de service public, outre le préfet, d'une part, les candidats qui n'ont pas engagé un référé précontractuel, lorsque le pouvoir adjudicateur ou l'entité adjudicatrice n'a pas rendu publique son intention de conclure le contrat dans les conditions prévues par l'article R. 1411-2-1 du CGCT ou n'a pas observé, avant de le signer, un délai d'au moins onze jours entre la date de publication de l'avis prévu par cet article et la date de conclusion du contrat, ainsi que, d'autre part, les candidats qui ont engagé un référé précontractuel, lorsque le pouvoir adjudicateur ou l'entité adjudicatrice n'a pas respecté l'obligation de suspendre la signature du contrat prévue aux articles L. 551-4 ou L. 551-9 du CJA ou ne s'est pas conformé à la décision juridictionnelle rendue sur ce référé.,,,2) Les cas dans lesquels le juge des référés peut annuler un contrat sont limitativement énumérés aux trois alinéas de l'article L 551-18 du CJA. S'agissant des contrats de délégation de service public, qui ne sont ni soumis à l'obligation, pour le pouvoir adjudicateur ou l'entité adjudicatrice, mentionnée au troisième alinéa de l'article L. 551-18, de notifier aux opérateurs économiques ayant présenté une offre, avant la signature du contrat, la décision d'attribution, ni concernés par le deuxième alinéa de cet article relatif à des marchés publics fondés sur un accord cadre ou un système d'acquisition dynamique, l'annulation d'un tel contrat ne peut résulter que du constat des manquements mentionnés au premier alinéa du même article, c'est-à-dire de l'absence de toutes les mesures de publicité requises pour sa passation ou d'une publication au Journal officiel de l'Union européenne dans le cas où une telle publication est prescrite. Par suite, les candidats à l'attribution d'un contrat de délégation de service public ne peuvent invoquer utilement que les manquements de l'autorité délégante à ses obligations de publicité visées au premier alinéa de l'article L. 551-18.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., pour les marchés à procédure adaptée, CE, 19 janvier 2011, Grand port maritime du Havre, n° 343435, p. 11.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
