<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039258843</ID>
<ANCIEN_ID>JG_L_2019_10_000000421250</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/25/88/CETATEXT000039258843.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 4ème chambres réunies, 21/10/2019, 421250</TITRE>
<DATE_DEC>2019-10-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>421250</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 4ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOULLEZ ; SCP THOUVENIN, COUDRAY, GREVY</AVOCATS>
<RAPPORTEUR>M. Damien Pons</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie Sirinelli</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:421250.20191021</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A... B... a formé opposition, devant le tribunal administratif de Bordeaux, à la contrainte délivrée le 28 avril 2017 par le directeur de Pôle emploi Nouvelle-Aquitaine afin d'obtenir le remboursement d'une somme de 5 203,63 euros qui lui avait été versée au titre de la rémunération formation Pôle emploi. Par un jugement n° 1702063 du 4 avril 2018, le tribunal administratif de Bordeaux a annulé cette contrainte.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un nouveau mémoire, enregistrés les 5 juin et 5 septembre 2018 et le 19 avril 2019 au secrétariat du contentieux du Conseil d'Etat, Pôle emploi demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) de mettre à la charge de M. B... la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code du travail ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Damien Pons, maître des requêtes en service extraordinaire, <br/>
<br/>
              - les conclusions de Mme Marie Sirinelli, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boullez, avocat de Pôle emploi Nouvelle-Aquitaine et à la SCP Thouvenin, Coudray, Grevy, avocat de M. B... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que M. B..., licencié le 10 mars 2008, a perçu la somme de 5 203,63 euros au titre de la " rémunération des formations Pôle emploi " pour la période du 16 février au 7 mai 2010. Par un arrêt du 13 janvier 2015, la cour d'appel d'Agen a déclaré nul son licenciement au motif qu'il était fondé sur l'état de santé du salarié, a ordonné sa réintégration et a condamné son employeur à lui payer notamment la somme de 392 300 euros au titre de l'indemnisation de la perte de salaire au cours des années 2008 à 2014. Le 28 avril 2017, le directeur de Pôle emploi Nouvelle-Aquitaine a délivré à M. B... une contrainte pour la récupération de la somme perçue au titre de la rémunération des formations Pôle emploi. <br/>
<br/>
              Sur le cadre juridique du litige :<br/>
<br/>
              2. En vertu de l'article L. 5312-1 du code du travail, Pôle emploi est une institution nationale publique dotée de la personnalité morale et de l'autonomie financière qui a pour mission notamment de : " 2° Accueillir, informer, orienter et accompagner les personnes, qu'elles disposent ou non d'un emploi, à la recherche d'un emploi, d'une formation ou d'un conseil professionnel, prescrire toutes actions utiles pour développer leurs compétences professionnelles et améliorer leur employabilité, favoriser leur reclassement et leur promotion professionnelle, faciliter leur mobilité géographique et professionnelle et participer aux parcours d'insertion sociale et professionnelle ". <br/>
<br/>
              3. Aux termes de l'article L. 6341-1 du même code, dans sa rédaction applicable à la date de versement de la rémunération en litige : " L'Etat, les régions, les employeurs et les organismes collecteurs paritaires agréés concourent au financement de la rémunération des stagiaires de la formation professionnelle. / L'institution mentionnée à l'article L. 5312-1 y concourt également, le cas échéant pour le compte de l'organisme mentionné à l'article L. 5427-1, notamment dans les conditions prévues à l'article L. 1233-68 ", relatif au contrat de sécurisation professionnelle. Il résulte de ces dispositions, éclairées par les travaux préparatoires de la loi du 13 février 2008 relative à la réforme de l'organisation du service public de l'emploi dont elles sont issues, que Pôle emploi a reçu compétence pour rémunérer les stagiaires de la formation professionnelle, qu'il agisse pour le compte de l'organisme gestionnaire de l'assurance chômage ou pour son propre compte. <br/>
<br/>
              4. Enfin, en vertu de l'article R. 5312-6 du même code, le conseil d'administration de Pôle emploi délibère notamment sur les mesures destinées " à favoriser l'insertion, le reclassement, la promotion professionnelle et la mobilité géographique et professionnelle des personnes, qu'elles disposent ou non d'un emploi, en application de la convention " pluriannuelle conclue entre l'Etat, l'organisme gestionnaire du régime d'assurance chômage et Pôle emploi. Sur le fondement de ces dispositions, le conseil d'administration de Pôle emploi, par une délibération du 19 décembre 2008, a fixé la nature et les conditions d'attribution des aides et mesures qu'il accorde, parmi lesquelles le versement d'une rémunération, dite " rémunération des formations Pôle emploi ", aux " demandeurs d'emploi inscrits qui suivent une action de formation conventionnée par Pôle emploi (...) et qui ne peuvent bénéficier de l'allocation d'assurance prévue à l'article L. 5422-1 du code du travail ", afin de leur assurer un revenu, d'un montant identique à celui fixé par le code du travail pour les stagiaires de la formation professionnelle suivant des stages agréés, pendant la durée de leur participation à cette action de formation.<br/>
<br/>
              Sur la compétence de la juridiction administrative :<br/>
<br/>
              5. D'une part, aux termes de l'article L. 5312-12 du même code, dans sa rédaction alors en vigueur : " Les litiges relatifs aux prestations dont le service est assuré par l'institution, pour le compte de l'organisme chargé de la gestion du régime d'assurance chômage, de l'Etat ou du Fonds de solidarité prévu à l'article L. 5423-24 sont soumis au régime contentieux qui leur était applicable antérieurement à la création de cette institution ". Il résulte de ces dispositions, éclairées par les travaux préparatoires de la loi du 13 février 2008 dont elles sont issues, que le législateur a souhaité que la réforme, qui s'est notamment caractérisée par la substitution de Pôle emploi à l'Agence nationale pour l'emploi et aux associations pour l'emploi dans l'industrie et le commerce (Assédic), reste sans incidence sur le régime juridique des prestations et sur la juridiction compétente pour connaître du droit aux prestations, notamment sur la compétence de la juridiction judiciaire s'agissant des prestations servies au titre du régime d'assurance chômage. En revanche, un litige relatif aux prestations servies au titre du régime de solidarité relève de la compétence de la juridiction administrative, dès lors que n'est pas en cause la régularité d'un acte de poursuite. De même, un litige relatif à une aide créée par Pôle emploi, établissement public à caractère administratif, dans le cadre de ses compétences propres et de sa mission de service public, telles que prévues au 2° de l'article L. 5312-1 et au 3° de l'article L. 5312-7 du code du travail, relève, sous la même réserve, de la compétence de la juridiction administrative.<br/>
<br/>
              6. D'autre part, aux termes de l'article L. 6341-11 du code du travail, qui relève d'un chapitre de ce code consacré à la rémunération des stagiaires de la formation professionnelle : " Tous les litiges auxquels peuvent donner lieu la liquidation, le versement et le remboursement des rémunérations et indemnités prévues au présent chapitre relèvent de la compétence du juge judiciaire ". <br/>
<br/>
              7. La rémunération des formations Pôle emploi constitue une aide aux demandeurs d'emploi créée par le conseil d'administration de Pôle emploi dans le cadre de ses compétences propres et de sa mission de service public, telles que prévues au 2° de l'article L. 5312-1 et au 3° de l'article L. 5312-7 du code du travail. Si les demandeurs d'emploi qui en bénéficient sont regardés comme des stagiaires de la formation professionnelle, elle n'est pas pour autant au nombre des rémunérations et indemnités prévues par le chapitre Ier du titre IV du livre III de la sixième partie du code du travail, qui sont subordonnées à l'agrément du stage par l'Etat ou les régions - ou, désormais, les opérateurs de compétence - et dont l'article L. 6341-11 prévoit que les litiges auxquels elles donnent lieu relèvent de la compétence du juge judiciaire. Par suite, la juridiction administrative est compétente pour connaître de l'opposition formée par M. B... à la contrainte délivrée le 28 avril 2017 par le directeur de Pôle emploi Nouvelle-Aquitaine afin d'obtenir le remboursement de la somme de 5 203,63 euros qui lui avait été versée au titre de la rémunération des formations Pôle emploi. <br/>
<br/>
              Sur le jugement du tribunal administratif de Bordeaux :<br/>
<br/>
              8. D'une part, aux termes de l'article L. 5426-8-2 du code du travail : " Pour le remboursement des allocations, aides, ainsi que de toute autre prestation indûment versées par Pôle emploi pour son propre compte, pour le compte de l'organisme chargé de la gestion du régime d'assurance chômage mentionné à l'article L. 5427-1, pour le compte de l'Etat ou des employeurs mentionnés à l'article L. 5424-1, le directeur général de Pôle emploi ou la personne qu'il désigne en son sein peut, dans les délais et selon les conditions fixés par voie réglementaire, et après mise en demeure, délivrer une contrainte qui, à défaut d'opposition du débiteur devant la juridiction compétente, comporte tous les effets d'un jugement et confère le bénéfice de l'hypothèque judiciaire ". Il résulte de ces dispositions que Pôle emploi peut délivrer une contrainte pour obtenir le remboursement de sommes versées indument, que le caractère indu existe dès l'origine ou que le paiement se trouve être ultérieurement indu. Ces dispositions sont applicables aux sommes versées au demandeur d'emploi au titre de la rémunération des formations Pôle emploi.<br/>
<br/>
              9. D'autre part, le salarié dont le licenciement a été postérieurement déclaré nul n'est pas, de ce seul fait, privé du droit au versement d'un revenu de remplacement. Toutefois, dans ses rapports avec Pôle emploi, ce salarié n'est pas fondé à cumuler, au titre d'une même période, un revenu de remplacement avec ses rémunérations ou avec une indemnité équivalant au montant des rémunérations dont il a été privé entre son licenciement et sa réintégration. Ainsi, la rémunération des formations Pôle emploi se révèle avoir été indûment versée s'il reçoit de son employeur le versement d'une telle indemnité pour la période considérée. <br/>
<br/>
              10. Il suit de là que le tribunal administratif de Bordeaux a commis une erreur de droit en jugeant que la rémunération des formations Pôle emploi perçue par M. B... du 16 février au 7 mai 2010 ne pouvait être regardée comme lui ayant été indument versée, alors même que son employeur avait été condamné à lui payer une indemnité correspondant à sa perte de salaire au cours des années 2008 à 2014, sans rechercher si cette indemnité compensatrice de salaires lui avait été effectivement versée. Par suite, Pôle emploi est fondé à demander l'annulation du jugement qu'il attaque. <br/>
<br/>
              Sur les frais liés au litige :<br/>
<br/>
              11. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de Pôle emploi, qui n'est pas la partie perdante dans la présente instance. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par Pôle emploi sur le fondement des mêmes dispositions. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le jugement du tribunal administratif de Bordeaux du 4 avril 2018 est annulé. <br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Bordeaux.<br/>
Article 3 : Les conclusions de Pôle emploi et de M. B... présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à Pôle emploi et à M. A... B....<br/>
Copie en sera adressée à la ministre du travail. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">17-03-01 COMPÉTENCE. RÉPARTITION DES COMPÉTENCES ENTRE LES DEUX ORDRES DE JURIDICTION. COMPÉTENCE DÉTERMINÉE PAR DES TEXTES SPÉCIAUX. - 1) PRINCIPE - LITIGES RELATIF À UNE AIDE CRÉÉE PAR PÔLE EMPLOI DANS LE CADRE DE SES COMPÉTENCES PROPRES ET DE SA MISSION DE SERVICE PUBLIC [RJ1] - COMPÉTENCE DU JUGE ADMINISTRATIF - 2) ILLUSTRATION -  LITIGE PORTANT SUR LA RÉMUNÉRATION DES FORMATIONS PÔLE EMPLOI COMPÉTENCE DU JUGE ADMINISTRATIF.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">66-10-02 TRAVAIL ET EMPLOI. POLITIQUES DE L'EMPLOI. INDEMNISATION DES TRAVAILLEURS PRIVÉS D'EMPLOI. - ORDRE DE JURIDICTION COMPÉTENT POUR CONNAÎTRE DES LITIGES RELATIFS À UNE AIDE CRÉÉE PAR PÔLE EMPLOI DANS LE CADRE DE SES COMPÉTENCES PROPRES ET DE SA MISSION DE SERVICE PUBLIC - 1) PRINCIPE - ORDRE ADMINISTRATIF [RJ1] - 2) ILLUSTRATION -  LITIGE PORTANT SUR LA RÉMUNÉRATION DES FORMATIONS PÔLE EMPLOI.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">66-11-001-01 TRAVAIL ET EMPLOI. SERVICE PUBLIC DE L'EMPLOI. - LITIGES RELATIF À UNE AIDE CRÉÉE PAR PÔLE EMPLOI DANS LE CADRE DE SES COMPÉTENCES PROPRES ET DE SA MISSION DE SERVICE PUBLIC [RJ1] - 1) PRINCIPE - COMPÉTENCE DU JUGE ADMINISTRATIF - 2) ILLUSTRATION -  LITIGE PORTANT SUR LA RÉMUNÉRATION DES FORMATIONS PÔLE EMPLOI COMPÉTENCE DU JUGE ADMINISTRATIF.
</SCT>
<ANA ID="9A"> 17-03-01 1) Il résulte de l'article L. 5312-12 du code du travail, éclairées par les travaux préparatoires de la loi n° 2008-126 du 13 février 2008 relative à la réforme de l'organisation du service public de l'emploi dont il est issue, que le législateur a souhaité que cette réforme, qui s'est notamment caractérisée par la substitution de Pôle emploi à l'Agence nationale pour l'emploi et aux associations pour l'emploi dans l'industrie et le commerce (Assédic), reste sans incidence sur le régime juridique des prestations et sur la juridiction compétente pour connaître du droit aux prestations, notamment sur la compétence de la juridiction judiciaire s'agissant des prestations servies au titre du régime d'assurance chômage. En revanche, un litige relatif aux prestations servies au titre du régime de solidarité relève de la compétence de la juridiction administrative, dès lors que n'est pas en cause la régularité d'un acte de poursuite. De même, un litige relatif à une aide créée par Pôle emploi, établissement public à caractère administratif, dans le cadre de ses compétences propres et de sa mission de service public, telles que prévues au 2° de l'article L. 5312-1 et au 3° de l'article L. 5312-7 du code du travail, relève, sous la même réserve, de la compétence de la juridiction administrative.,,,2) La rémunération des formations Pôle emploi constitue une aide aux demandeurs d'emploi créée par le conseil d'administration de Pôle emploi dans le cadre de ses compétences propres et de sa mission de service public, telles que prévues au 2° de l'article L. 5312-1 et au 3° de l'article L. 5312-7 du code du travail. Si les demandeurs d'emploi qui en bénéficient sont regardés comme des stagiaires de la formation professionnelle, elle n'est pas pour autant au nombre des rémunérations et indemnités prévues par le chapitre Ier du titre IV du livre III de la sixième partie du code du travail, qui sont subordonnées à l'agrément du stage par l'Etat ou les régions - ou, désormais, les opérateurs de compétence - et dont l'article L. 6341-11 prévoit que les litiges auxquels elles donnent lieu relèvent de la compétence du juge judiciaire. Par suite, la juridiction administrative est compétente pour connaître de l'opposition formée par le requérant à la contrainte délivrée par le directeur régional de Pôle afin d'obtenir le remboursement d'une somme qui lui avait été versée au titre de la rémunération des formations Pôle emploi.</ANA>
<ANA ID="9B"> 66-10-02 1) Il résulte de l'article L. 5312-12 du code du travail, éclairées par les travaux préparatoires de la loi n° 2008-126 du 13 février 2008 relative à la réforme de l'organisation du service public de l'emploi dont elles sont issues, que le législateur a souhaité que cette réforme, qui s'est notamment caractérisée par la substitution de Pôle emploi à l'Agence nationale pour l'emploi et aux associations pour l'emploi dans l'industrie et le commerce (Assédic), reste sans incidence sur le régime juridique des prestations et sur la juridiction compétente pour connaître du droit aux prestations, notamment sur la compétence de la juridiction judiciaire s'agissant des prestations servies au titre du régime d'assurance chômage. En revanche, un litige relatif aux prestations servies au titre du régime de solidarité relève de la compétence de la juridiction administrative, dès lors que n'est pas en cause la régularité d'un acte de poursuite. De même, un litige relatif à une aide créée par Pôle emploi, établissement public à caractère administratif, dans le cadre de ses compétences propres et de sa mission de service public, telles que prévues au 2° de l'article L. 5312-1 et au 3° de l'article L. 5312-7 du code du travail, relève, sous la même réserve, de la compétence de la juridiction administrative.,,,2) La rémunération des formations Pôle emploi constitue une aide aux demandeurs d'emploi créée par le conseil d'administration de Pôle emploi dans le cadre de ses compétences propres et de sa mission de service public, telles que prévues au 2° de l'article L. 5312-1 et au 3° de l'article L. 5312-7 du code du travail. Si les demandeurs d'emploi qui en bénéficient sont regardés comme des stagiaires de la formation professionnelle, elle n'est pas pour autant au nombre des rémunérations et indemnités prévues par le chapitre Ier du titre IV du livre III de la sixième partie du code du travail, qui sont subordonnées à l'agrément du stage par l'Etat ou les régions - ou, désormais, les opérateurs de compétence - et dont l'article L. 6341-11 prévoit que les litiges auxquels elles donnent lieu relèvent de la compétence du juge judiciaire. Par suite, la juridiction administrative est compétente pour connaître de l'opposition formée par le requérant à la contrainte délivrée par le directeur régional de Pôle afin d'obtenir le remboursement d'une somme qui lui avait été versée au titre de la rémunération des formations Pôle emploi.</ANA>
<ANA ID="9C"> 66-11-001-01 1) Il résulte de l'article L. 5312-12 du code du travail, éclairées par les travaux préparatoires de la loi n° 2008-126 du 13 février 2008 relative à la réforme de l'organisation du service public de l'emploi dont elles sont issues, que le législateur a souhaité que cette réforme, qui s'est notamment caractérisée par la substitution de Pôle emploi à l'Agence nationale pour l'emploi et aux associations pour l'emploi dans l'industrie et le commerce (Assédic), reste sans incidence sur le régime juridique des prestations et sur la juridiction compétente pour connaître du droit aux prestations, notamment sur la compétence de la juridiction judiciaire s'agissant des prestations servies au titre du régime d'assurance chômage. En revanche, un litige relatif aux prestations servies au titre du régime de solidarité relève de la compétence de la juridiction administrative, dès lors que n'est pas en cause la régularité d'un acte de poursuite. De même, un litige relatif à une aide créée par Pôle emploi, établissement public à caractère administratif, dans le cadre de ses compétences propres et de sa mission de service public, telles que prévues au 2° de l'article L. 5312-1 et au 3° de l'article L. 5312-7 du code du travail, relève, sous la même réserve, de la compétence de la juridiction administrative.,,,2) La rémunération des formations Pôle emploi constitue une aide aux demandeurs d'emploi créée par le conseil d'administration de Pôle emploi dans le cadre de ses compétences propres et de sa mission de service public, telles que prévues au 2° de l'article L. 5312-1 et au 3° de l'article L. 5312-7 du code du travail. Si les demandeurs d'emploi qui en bénéficient sont regardés comme des stagiaires de la formation professionnelle, elle n'est pas pour autant au nombre des rémunérations et indemnités prévues par le chapitre Ier du titre IV du livre III de la sixième partie du code du travail, qui sont subordonnées à l'agrément du stage par l'Etat ou les régions - ou, désormais, les opérateurs de compétence - et dont l'article L. 6341-11 prévoit que les litiges auxquels elles donnent lieu relèvent de la compétence du juge judiciaire. Par suite, la juridiction administrative est compétente pour connaître de l'opposition formée par le requérant à la contrainte délivrée par le directeur régional de Pôle afin d'obtenir le remboursement d'une somme qui lui avait été versée au titre de la rémunération des formations Pôle emploi.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., sur la compétence du juge administratif pour connaître des litiges relatifs aux prestations servies par Pôle emploi au titre du régime de solidarité CE, 26 avril 2018, M.,, n° 408049, T. pp. 604-947 ; TC, 7 avril 2014, Mme,c/ Pôle emploi Languedoc-Roussillon et Direction régionale des entreprises, de la concurrence, de la consommation, du travail et de l'emploi (DIRRECTE) Languedoc-Roussillon, n° 3946, T. pp. 574-892-893.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
