<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026141409</ID>
<ANCIEN_ID>JG_L_2012_07_000000356221</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/14/14/CETATEXT000026141409.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 04/07/2012, 356221, Publié au recueil Lebon</TITRE>
<DATE_DEC>2012-07-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>356221</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Julia Beurton</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CESSR:2012:356221.20120704</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le jugement n° 0909001 du 12 janvier 2012, enregistré le 30 janvier 2012 au secrétariat du contentieux du Conseil d'Etat, par lequel le tribunal administratif de Marseille, avant de statuer sur la demande de M. Franck A et de M. Richard B tendant à l'annulation d'une décision implicite de la commune de Marseille refusant l'abrogation de la délibération approuvant le dossier de réalisation de la zone d'aménagement concerté de Vallon Régny, a décidé, par application de l'article L. 113-1 du code de justice administrative, de transmettre le dossier de cette demande au Conseil d'Etat, en soumettant à son examen les questions suivantes : <br/>
<br/>
              1°) quelle est la nature juridique des actes mentionnés au a) et au b) de l'article R. 311-7 du code de l'urbanisme ' <br/>
<br/>
              2°) un intéressé qui conteste la légalité de l'acte approuvant le dossier de réalisation d'une zone d'aménagement concerté peut-il utilement se prévaloir de ce qu'à la date de cette approbation, les dispositions d'urbanisme applicables faisaient obstacle à la réalisation des équipements et aménagements prévus dans le dossier de réalisation '<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu le code de l'urbanisme, modifié notamment par le décret n° 2001-261 du 27 mars 2001 ;<br/>
<br/>
              Vu le code de justice administrative, notamment son article L. 113-1 ; <br/>
<br/>
               Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Julia Beurton, Auditeur, <br/>
<br/>
- les conclusions de Mme Claire Landais, rapporteur public ;<br/>
<br/>
<br/>REND L'AVIS SUIVANT :<br/>
<br/>
              I. L'article R. 311-7 du code de l'urbanisme relatif aux zones d'aménagement concerté, dans sa rédaction postérieure à l'entrée en vigueur du décret du 27 mars 2001, dispose que : " La personne publique qui a pris l'initiative de la création de la zone constitue un dossier de réalisation approuvé, sauf lorsqu'il s'agit de l'Etat, par son organe délibérant. Le dossier de réalisation comprend : / a) Le projet de programme des équipements publics à réaliser dans la zone ; lorsque celui-ci comporte des équipements dont la maîtrise d'ouvrage et le financement incombent normalement à d'autres collectivités ou établissements publics, le dossier doit comprendre les pièces faisant état de l'accord de ces personnes publiques sur le principe de la réalisation de ces équipements, les modalités de leur incorporation dans leur patrimoine et, le cas échéant, sur leur participation au financement ; / b) Le projet de programme global des constructions à réaliser dans la zone ; / c) Les modalités prévisionnelles de financement de l'opération d'aménagement, échelonnées dans le temps. / Le dossier de réalisation complète en tant que de besoin le contenu de l'étude d'impact mentionnée à l'article R. 311-2, notamment en ce qui concerne les éléments qui ne pouvaient être connus au moment de la constitution du dossier de création. / L'étude d'impact mentionnée à l'article R. 311-2 ainsi que les compléments éventuels prévus à l'alinéa précédent sont joints au dossier de toute enquête publique concernant l'opération d'aménagement réalisée dans la zone ". <br/>
<br/>
              La décision par laquelle, sur le fondement de ces dispositions, la personne publique qui a décidé la création d'une zone d'aménagement concerté en approuve le dossier de réalisation, constitue une mesure seulement préparatoire aux actes qui définiront ultérieurement les éléments constitutifs de cette zone, notamment l'acte approuvant le programme des équipements publics à réaliser à l'intérieur de la zone. Cette décision, comme la décision refusant de l'abroger, n'est donc pas au nombre des actes qui peuvent faire l'objet d'un recours pour excès de pouvoir, les illégalités qui l'affectent étant seulement susceptibles d'entacher d'irrégularité la procédure d'adoption des décisions qu'elle prépare. Il en va de même des documents constituant ce dossier de réalisation et mentionnés aux a), b) et c) de l'article R. 311-7 du code de l'urbanisme, qui sont dépourvus de tout caractère décisionnel.<br/>
<br/>
              II. En vertu de l'article R. 311-6 du code de l'urbanisme, l'aménagement et l'équipement d'une zone d'aménagement concerté " sont réalisés dans le respect des règles d'urbanisme applicables. Lorsque la commune est couverte par un plan local d'urbanisme, la réalisation de la zone d'aménagement concerté est subordonnée au respect de l'article L. 123-3 ". Il découle de ces dispositions que l'acte de création de la zone, la délibération approuvant le dossier de réalisation mentionnée à l'article R. 311-7 et la délibération approuvant le programme des équipements publics prévue à l'article R. 311-8, qui fixent seulement la nature et la consistance des aménagements à réaliser, ne sont pas tenus de respecter les dispositions du règlement du plan local d'urbanisme ou du plan d'occupation des sols en vigueur à la date de leur adoption. En revanche, il appartient aux autorités compétentes de prendre les dispositions nécessaires pour que les autorisations individuelles d'urbanisme qui ont pour objet, dans le cadre défini par les actes qui viennent d'être mentionnés, l'aménagement et l'équipement effectifs de la zone puissent, conformément aux principes de droit commun, être accordées dans le respect des règles d'urbanisme, et notamment des dispositions du règlement du plan local d'urbanisme ou du plan d'occupation des sols, applicables à la date de leur délivrance.<br/>
<br/>
              Le présent avis sera notifié au tribunal administratif de Marseille, à M. Franck A, à M. Richard B, à la commune de Marseille, à la société Marseille Aménagement et à la ministre de l'égalité des territoires et du logement.<br/>
<br/>
              Il sera publié au Journal officiel de la République française.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-01-01-02 PROCÉDURE. INTRODUCTION DE L'INSTANCE. DÉCISIONS POUVANT OU NON FAIRE L'OBJET D'UN RECOURS. ACTES NE CONSTITUANT PAS DES DÉCISIONS SUSCEPTIBLES DE RECOURS. - DÉCISION D'APPROBATION DU DOSSIER DE RÉALISATION D'UNE ZAC (ART. R. 311-7 DU CODE DE L'URBANISME) ET DES DOCUMENTS DE CE DOSSIER - ACTE PRÉPARATOIRE - DÉCISION INSUSCEPTIBLE DE RECOURS.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">68-01-01-02-015 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PLANS D'AMÉNAGEMENT ET D'URBANISME. PLANS D'OCCUPATION DES SOLS ET PLANS LOCAUX D'URBANISME. APPLICATION DES RÈGLES FIXÉES PAR LES POS OU LES PLU. OPPOSABILITÉ DU PLAN. - OPPOSABILITÉ DES RÈGLEMENTS DE PLU OU DE POS AUX APPROBATIONS DE DOSSIER DE RÉALISATION DE LA ZAC ET DU PROGRAMME DES ÉQUIPEMENTS - ABSENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">68-02-02-01-01 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PROCÉDURES D'INTERVENTION FONCIÈRE. OPÉRATIONS D'AMÉNAGEMENT URBAIN. ZONES D'AMÉNAGEMENT CONCERTÉ (ZAC). CRÉATION. - 1) DÉCISION D'APPROBATION DU DOSSIER DE RÉALISATION D'UNE ZAC (ART. R. 311-7 DU CODE DE L'URBANISME) ET DES DOCUMENTS DE CE DOSSIER - ACTE PRÉPARATOIRE - DÉCISION INSUSCEPTIBLE DE RECOURS - 2) OPPOSABILITÉ DES RÈGLEMENTS DE PLU OU DE POS AUX APPROBATIONS DE DOSSIER DE RÉALISATION DE LA ZAC ET DU PROGRAMME DES ÉQUIPEMENTS - ABSENCE.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">68-06-01-01 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. INTRODUCTION DE L'INSTANCE. DÉCISION FAISANT GRIEF. - ABSENCE - DÉCISION D'APPROBATION DU DOSSIER DE RÉALISATION D'UNE ZAC (ART. R. 311-7 DU CODE DE L'URBANISME) ET DES DOCUMENTS DE CE DOSSIER - ACTE PRÉPARATOIRE - DÉCISION INSUSCEPTIBLE DE RECOURS.
</SCT>
<ANA ID="9A"> 54-01-01-02 La décision par laquelle, sur le fondement de l'article R. 311-7 du code de l'urbanisme, dans sa rédaction postérieure à l'entrée en vigueur du décret n° 2001-261 du 27 mars 2001, la personne publique qui a décidé la création d'une zone d'aménagement concerté (ZAC) en approuve le dossier de réalisation, constitue une mesure seulement préparatoire aux actes qui définiront ultérieurement les éléments constitutifs de cette zone, notamment l'acte approuvant le programme des équipements publics à réaliser à l'intérieur de la zone. Cette décision, comme la décision refusant de l'abroger, n'est donc pas au nombre des actes qui peuvent faire l'objet d'un recours pour excès de pouvoir, les illégalités qui l'affectent étant seulement susceptibles d'entacher d'irrégularité la procédure d'adoption des décisions qu'elle prépare. Il en va de même des documents constituant ce dossier de réalisation et mentionnés aux a), b) et c) de l'article R. 311-7 du code de l'urbanisme, qui sont dépourvus de tout caractère décisionnel.</ANA>
<ANA ID="9B"> 68-01-01-02-015 Il découle de l'article R. 311-6 du code de l'urbanisme que l'acte de création de la zone d'aménagement commercial (ZAC), la délibération approuvant le dossier de réalisation mentionnée à l'article R. 311-7 et la délibération approuvant le programme des équipements publics prévue à l'article R. 311-8 du même code, qui fixent seulement la nature et la consistance des aménagements à réaliser, ne sont pas tenus de respecter les dispositions du règlement du plan local d'urbanisme (PLU) ou du plan d'occupation des sols (POS) en vigueur à la date de leur adoption. En revanche, il appartient aux autorités compétentes de prendre les dispositions nécessaires pour que les autorisations individuelles d'urbanisme qui ont pour objet, dans le cadre défini par les actes qui viennent d'être mentionnés, l'aménagement et l'équipement effectifs de la zone puissent, conformément aux principes de droit commun, être accordées dans le respect des règles d'urbanisme.</ANA>
<ANA ID="9C"> 68-02-02-01-01 1) La décision par laquelle, sur le fondement de l'article R. 311-7 du code de l'urbanisme, dans sa rédaction postérieure à l'entrée en vigueur du décret n° 2001-261 du 27 mars 2001, la personne publique qui a décidé la création d'une zone d'aménagement concerté (ZAC) en approuve le dossier de réalisation, constitue une mesure seulement préparatoire aux actes qui définiront ultérieurement les éléments constitutifs de cette zone, notamment l'acte approuvant le programme des équipements publics à réaliser à l'intérieur de la zone. Cette décision, comme la décision refusant de l'abroger, n'est donc pas au nombre des actes qui peuvent faire l'objet d'un recours pour excès de pouvoir, les illégalités qui l'affectent étant seulement susceptibles d'entacher d'irrégularité la procédure d'adoption des décisions qu'elle prépare. Il en va de même des documents constituant ce dossier de réalisation et mentionnés aux a), b) et c) de l'article R. 311-7 du code de l'urbanisme, qui sont dépourvus de tout caractère décisionnel.,,2) Il découle de l'article R. 311-6 du code de l'urbanisme que l'acte de création de la zone d'aménagement commercial (ZAC), la délibération approuvant le dossier de réalisation mentionnée à l'article R. 311-7 et la délibération approuvant le programme des équipements publics prévue à l'article R. 311-8 du même code, qui fixent seulement la nature et la consistance des aménagements à réaliser, ne sont pas tenus de respecter les dispositions du règlement du plan local d'urbanisme (PLU) ou du plan d'occupation des sols (POS) en vigueur à la date de leur adoption. En revanche, il appartient aux autorités compétentes de prendre les dispositions nécessaires pour que les autorisations individuelles d'urbanisme qui ont pour objet, dans le cadre défini par les actes qui viennent d'être mentionnés, l'aménagement et l'équipement effectifs de la zone puissent, conformément aux principes de droit commun, être accordées dans le respect des règles d'urbanisme.</ANA>
<ANA ID="9D"> 68-06-01-01 La décision par laquelle, sur le fondement de l'article R. 311-7 du code de l'urbanisme, dans sa rédaction postérieure à l'entrée en vigueur du décret n° 2001-261 du 27 mars 2001, la personne publique qui a décidé la création d'une zone d'aménagement concerté (ZAC) en approuve le dossier de réalisation, constitue une mesure seulement préparatoire aux actes qui définiront ultérieurement les éléments constitutifs de cette zone, notamment l'acte approuvant le programme des équipements publics à réaliser à l'intérieur de la zone. Cette décision, comme la décision refusant de l'abroger, n'est donc pas au nombre des actes qui peuvent faire l'objet d'un recours pour excès de pouvoir, les illégalités qui l'affectent étant seulement susceptibles d'entacher d'irrégularité la procédure d'adoption des décisions qu'elle prépare. Il en va de même des documents constituant ce dossier de réalisation et mentionnés aux a), b) et c) de l'article R. 311-7 du code de l'urbanisme, qui sont dépourvus de tout caractère décisionnel.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
