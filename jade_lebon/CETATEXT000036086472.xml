<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036086472</ID>
<ANCIEN_ID>JG_L_2017_11_000000389203</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/08/64/CETATEXT000036086472.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 5ème chambres réunies, 24/11/2017, 389203</TITRE>
<DATE_DEC>2017-11-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>389203</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FABIANI, LUC-THALER, PINATEL ; SCP MASSE-DESSEN, THOUVENIN, COUDRAY</AVOCATS>
<RAPPORTEUR>M. Jean-François de Montgolfier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:389203.20171124</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Le syndicat national des praticiens de la mutualité agricole a demandé à la cour administrative d'appel de Paris d'annuler pour excès de pouvoir l'arrêté du 24 décembre 2013 par lequel le ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social a fixé la liste des organisations syndicales reconnues représentatives dans la convention collective des praticiens-conseils de la mutualité sociale agricole. Par un arrêt n° 14PA01075 du 2 février 2015, la cour administrative d'appel de Paris a rejeté sa requête.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 3 avril et 2 juillet 2015 au secrétariat du contentieux du Conseil d'Etat, le syndicat national des praticiens de la mutualité agricole demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa requête ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu :<br/>
              - le code rural et de la pêche maritime ;<br/>
              - le code du travail ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-François de Montgolfier, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Fabiani, Luc-Thaler, Pinatel, avocat du syndicat national des praticiens de la mutualité agricole et à la SCP Masse-Dessen, Thouvenin, Coudray, avocat de la fédération générale agroalimentaire CFDT ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article L. 2122-5 du code du travail : " Dans les branches professionnelles, sont représentatives les organisations syndicales qui : / 1° Satisfont aux critères de l'article L. 2121 1 ; / 2° Disposent d'une implantation territoriale équilibrée au sein de la branche ; / 3° Ont recueilli au moins 8 % des suffrages exprimés résultant de l'addition au niveau de la branche, d'une part, des suffrages exprimés au premier tour des dernières élections des titulaires aux comités d'entreprise ou de la délégation unique du personnel ou, à défaut, des délégués du personnel, quel que soit le nombre de votants, et, d'autre part, des suffrages exprimés au scrutin concernant les entreprises de moins de onze salariés dans les conditions prévues aux articles L. 2122-10-1 et suivants. (...) " ; que l'article L. 2122-11 du code du travail prévoit que le ministre chargé du travail arrête la liste des organisations syndicales reconnues représentatives en vertu de ces dispositions ; que, sur ce fondement, le ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social a pris, le 24 décembre 2013, un arrêté fixant la liste des organisations syndicales reconnues représentatives dans la convention collective des praticiens-conseils de la mutualité sociale agricole ; que le syndicat national des praticiens de la mutualité agricole se pourvoit en cassation contre l'arrêt par lequel la cour administrative d'appel de Paris, compétente en premier et dernier ressort en vertu de l'article R. 311-2 du code de justice administrative, a rejeté sa requête tendant à l'annulation de cet arrêté ;<br/>
<br/>
              2. Considérant que l'existence de cette convention collective des praticiens-conseils de la mutualité sociale agricole, qui régit une catégorie particulière de salariés dans l'ensemble du champ professionnel des organismes de mutualité sociale agricole, résulte des dispositions de l'article L. 123-1 du code de la sécurité sociale, aux termes desquelles : " En ce qui concerne le personnel autre que les agents de direction et les agents comptables, les conditions de travail du personnel des organismes de sécurité sociale, de leurs unions ou fédérations, de leurs établissements et oeuvres sociales sont fixées par conventions collectives de travail (...). Toutefois, les dispositions de ces conventions ne deviennent applicables qu'après avoir reçu l'agrément de l'autorité compétente de l'Etat " ainsi que de celles du cinquième alinéa de l'article D. 723-147 du code rural et de la pêche maritime, aux termes desquelles : " Les conditions d'emploi des praticiens-conseils et des médecins-conseils chefs de service sont fixées, sous réserve des dispositions du présent paragraphe, par une convention collective nationale. Cette convention n'entre en vigueur qu'après avoir reçu l'agrément du ministre chargé de l'agriculture " ; <br/>
<br/>
              3. Considérant que les articles L. 2314-8, L. 2314-10, L. 2324-11 et L. 2324-12 du code du travail disposent, s'agissant des délégués et des représentants du personnel que, sauf convention ou accord collectif signé par toutes les organisations syndicales représentatives, ils sont élus, d'une part, par un collège comprenant les ouvriers et employés et, d'autre part, par un collège comprenant les ingénieurs, chefs de service, techniciens, agents de maîtrise et assimilés ; <br/>
<br/>
              4. Considérant qu'en l'absence, par suite, de dispositions permettant de mesurer, dans le champ d'application de la convention collective qui leur est légalement applicable, l'audience des différentes organisations syndicales susceptibles de représenter les praticiens-conseils de la mutualité sociale agricole, cette convention ne peut être regardée, au sens des dispositions de l'article L. 2122-5 du même code, comme constituant une " branche " pour laquelle il appartient au ministre chargé du travail de fixer, par arrêté, la liste des organisations syndicales reconnues représentatives ; <br/>
<br/>
              5. Considérant qu'en ne relevant pas d'office cette incompétence du ministre du travail pour prendre l'arrêté attaqué, la cour administrative d'appel de Paris a entaché son arrêt d'erreur de droit ; que par suite, sans qu'il soit besoin d'examiner les moyens du requérant, ce dernier est fondé à en demander l'annulation ;<br/>
<br/>
              6. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              7. Considérant qu'il résulte de ce qui a été dit au point 4 que l'arrêté attaqué est entaché d'incompétence et doit être annulé ;<br/>
<br/>
              8. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 3 000 euros à verser au syndicat national des praticiens de la mutualité agricole au titre de l'article L. 761-1 du code de justice administrative ; que ces dispositions font en revanche obstacle à ce qu'une somme soit mise au même titre à la charge du syndicat national des praticiens de la mutualité agricole, qui n'est pas la partie perdante dans la présente instance ;<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Paris du 2 février 2015 est annulé.<br/>
Article 2 : L'arrêté du 24 décembre 2013 fixant la liste des organisations syndicales reconnues représentatives dans la convention collective des praticiens-conseils de la mutualité sociale agricole est annulé.<br/>
Article 3 : L'Etat versera au syndicat national des praticiens-conseil de la mutualité agricole la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions présentées par la confédération générale agroalimentaire CFDT au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée au syndicat national des praticiens de la mutualité agricole et à la confédération générale agroalimentaire CFDT.<br/>
Copie en sera adressée à la confédération française de l'encadrement-confédération générale des cadres (CFE-CGC), à la confédération générale du travail (CGT), à la confédération française des travailleurs chrétiens (CFTC), à la confédération générale du travail-force ouvrière (CGT-FO) et à la ministre du travail.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">03-02 AGRICULTURE ET FORÊTS. PROBLÈMES SOCIAUX DE L'AGRICULTURE. - MUTUALITÉ SOCIALE AGRICOLE - PRATICIENS-CONSEILS DE LA MUTUALITÉ SOCIALE AGRICOLE RÉGIS PAR UNE CONVENTION COLLECTIVE (ART. L. 123-1 DU CSS ET D. 723-147 DU CRPM) - BRANCHE PROFESSIONNELLE AU SENS DE L'ART. L. 2122-5 DU CODE DU TRAVAIL - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">66-02 TRAVAIL ET EMPLOI. CONVENTIONS COLLECTIVES. - PRATICIENS-CONSEILS DE LA MUTUALITÉ SOCIALE AGRICOLE RÉGIS PAR UNE CONVENTION COLLECTIVE (ART. L. 123-1 DU CSS ET D. 723-147 DU CRPM) - BRANCHE PROFESSIONNELLE AU SENS DE L'ART. L. 2122-5 DU CODE DU TRAVAIL - ABSENCE.
</SCT>
<ANA ID="9A"> 03-02 Convention collective des praticiens-conseils de la mutualité sociale agricole (art. L. 123-1 du code de la sécurité sociale et D. 723-147 du code rural et de la pêche maritime).... ,,En l'absence de dispositions permettant de mesurer, dans le champ d'application de la convention collective qui leur est légalement applicable, l'audience des différentes organisations syndicales susceptibles de représenter les praticiens-conseils de la mutualité sociale agricole, cette convention ne peut être regardée, au sens des dispositions de l'article L. 2122-5 du code du travail, comme constituant une branche pour laquelle il appartient au ministre chargé du travail de fixer, par arrêté, la liste des organisations syndicales reconnues représentatives.</ANA>
<ANA ID="9B"> 66-02 Convention collective des praticiens-conseils de la mutualité sociale agricole (art. L. 123-1 du code de la sécurité sociale et D. 723-147 du code rural et de la pêche maritime).... ,,En l'absence de dispositions permettant de mesurer, dans le champ d'application de la convention collective qui leur est légalement applicable, l'audience des différentes organisations syndicales susceptibles de représenter les praticiens-conseils de la mutualité sociale agricole, cette convention ne peut être regardée, au sens des dispositions de l'article L. 2122-5 du code du travail, comme constituant une branche pour laquelle il appartient au ministre chargé du travail de fixer, par arrêté, la liste des organisations syndicales reconnues représentatives.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
