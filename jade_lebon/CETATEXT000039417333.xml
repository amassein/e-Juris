<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039417333</ID>
<ANCIEN_ID>JG_L_2019_11_000000418460</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/41/73/CETATEXT000039417333.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 22/11/2019, 418460</TITRE>
<DATE_DEC>2019-11-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>418460</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>CABINET BRIARD ; SCP NICOLAY, DE LANOUVELLE, HANNOTIN ; SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS</AVOCATS>
<RAPPORTEUR>M. François Lelièvre</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Mireille Le Corre</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:418460.20191122</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Autocars Faure a demandé au tribunal administratif de Grenoble d'annuler le marché de services réguliers de transports publics non urbains de personnes par voie terrestre conclu, pour le lot n° 10, entre le département de l'Isère et la société Cars Philibert et de condamner le département de l'Isère à lui verser une indemnité de 940 782 euros en réparation de son préjudice. Par un jugement n° 1305850 du 1er juillet 2015, le tribunal administratif de Grenoble a rejeté cette demande.<br/>
<br/>
              Par un arrêt n° 15LY03034 du 21 décembre 2017, la cour administrative d'appel de Lyon a rejeté l'appel formé par la société Autocars Faure contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 21 février, 22 mai 2018 et 26 avril 2019 au secrétariat du contentieux du Conseil d'Etat, la société Autocars Faure demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge du département de l'Isère la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code des marchés publics ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. François Lelièvre, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Mireille Le Corre, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, au Cabinet Briard, avocat de la société Autocars Faure, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la société Cars Philibert, et à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat du département de l'Isère ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que le département de l'Isère a lancé au mois de novembre 2012 une procédure d'appel d'offres ouvert pour la passation d'un marché alloti de services réguliers de transports publics non urbains de personnes par voie terrestre. L'offre présentée par un groupement d'entreprises composé des sociétés Autocars Faure et Voyages Monnet pour le lot n° 10, relatif à la ligne départementale de desserte du secteur de Grand Lemps, a été classée en seconde position. Le contrat a été conclu le 19 juillet 2013 avec la société Cars Philibert. Par un jugement du 1er juillet 2015, le tribunal administratif de Grenoble a rejeté la demande de la société Autocars Faure tendant, d'une part, à l'annulation de ce marché et, d'autre part, à la condamnation du département de l'Isère à l'indemniser du préjudice résultant de son éviction. La société Autocars Faure se pourvoit en cassation contre l'arrêt du 21 décembre 2017 par lequel la cour administrative d'appel de Lyon a rejeté son appel.<br/>
<br/>
              2. Le pouvoir adjudicateur définit librement la méthode de notation pour la mise en oeuvre de chacun des critères de sélection des offres qu'il a retenus et rendus publics. Toutefois, une méthode de notation est entachée d'irrégularité si, en méconnaissance des principes fondamentaux d'égalité de traitement des candidats et de transparence des procédures, elle est, par elle-même, de nature à priver de leur portée les critères de sélection ou à neutraliser leur pondération et est, de ce fait, susceptible de conduire, pour la mise en oeuvre de chaque critère, à ce que la meilleure note ne soit pas attribuée à la meilleure offre, ou, au regard de l'ensemble des critères pondérés, à ce que l'offre économiquement la plus avantageuse ne soit pas choisie. Il en va ainsi alors même que le pouvoir adjudicateur, qui n'y est pas tenu, aurait rendu publique, dans l'avis d'appel à concurrence ou les documents de la consultation, une telle méthode de notation.<br/>
<br/>
              3. Une méthode de notation des offres par laquelle le pouvoir adjudicateur laisse aux candidats le soin de fixer, pour l'un des critères ou sous-critères, la note qu'ils estiment devoir leur être attribuée est, par elle-même, de nature à priver de portée utile le critère ou sous-critère en cause si cette note ne peut donner lieu à vérification au stade de l'analyse des offres, quand bien même les documents de la consultation prévoiraient que le candidat attributaire qui ne respecterait pas, lors de l'exécution du marché, les engagements que cette note entend traduire pourrait, de ce fait, se voir infliger des pénalités.<br/>
<br/>
              4. Il ressort des pièces du dossier soumis aux juges du fond que le département de l'Isère a retenu trois critères de jugement des offres, à savoir le prix, la valeur technique et les garanties environnementales, pondérés respectivement à 60 %, 25 % et 15 %. La notation de l'un des deux sous-critères de la valeur technique, intitulé " niveau d'engagement du candidat en matière de notation de la qualité du service rendu sur les lignes objet du marché ", pondéré à hauteur de 20 %, dépendait exclusivement du niveau de qualité du service que le candidat s'estimait en mesure de garantir et ne résultait que de l'indication par le candidat lui-même d'une note dite " note qualité " qu'il devait s'attribuer à l'aide d'un outil de simulation. Les éléments mentionnés pour l'auto-évaluation, portant sur la propreté du véhicule, " l'ambiance générale " au sein du véhicule, la ponctualité, la conduite respectueuse du code de la route ou la qualité de l'accueil à bord du véhicule ne pouvaient faire l'objet d'une évaluation objective au stade de l'analyse des offres. La " note qualité " devait être comprise entre 7 et 9 sur 10 et la notation de ce sous-critère pouvait donc aller de 0, pour le candidat s'attribuant une " note qualité " de 7, à 25 points, pour le candidat s'attribuant une " note qualité " de 9. La cour administrative d'appel de Lyon a estimé que, par le recours à une telle méthode de notation, le département de l'Isère n'avait pas renoncé à apprécier la valeur des offres au motif, d'une part, qu'il avait précisément défini et communiqué aux candidats les modalités selon lesquelles le sous-critère en litige serait apprécié et, d'autre part, que la note attribuée aux candidats avait vocation à servir de référence pour la détermination de leur note annuelle " qualité " et le calcul d'éventuelles pénalités en cas de manquement à cet engagement. Il résulte de ce qui a été dit aux points 2 et 3 qu'en statuant ainsi, la cour administrative d'appel de Lyon a commis une erreur de droit. Il suit de là que la société Autocars Faure est fondée, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, à demander l'annulation de l'arrêt qu'elle attaque.<br/>
<br/>
              5. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mis à la charge de la société Autocars Faure, qui n'est pas la partie perdante, le versement des sommes que demandent, à ce titre, le département de l'Isère et la société Cars Philibert. En revanche, il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du département de l'Isère le versement d'une somme de 3 000 euros à la société Autocars Faure au titre des mêmes dispositions.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 21 décembre 2017 de la cour administrative d'appel de Lyon est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Lyon.<br/>
Article 3 : Le département de l'Isère versera la somme de 3 000 euros à la société Autocars Faure au titre de l'article L. 761-1 du code de justice administrative. Les conclusions présentées au même titre par le département de l'Isère et la société Cars Philibert sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à la société Autocars Faure, à la société Cars Philibert et au département de l'Isère.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-02-005 MARCHÉS ET CONTRATS ADMINISTRATIFS. FORMATION DES CONTRATS ET MARCHÉS. FORMALITÉS DE PUBLICITÉ ET DE MISE EN CONCURRENCE. - MÉTHODE DE NOTATION - RÉGULARITÉ - 1) PRINCIPES [RJ1] - 2) A) APPLICATION - MÉTHODE LAISSANT AUX CANDIDATS LE SOIN DE FIXER LEUR NOTE - IRRÉGULARITÉ DE NATURE À PRIVER DE LEUR PORTÉE LES CRITÈRES OU À NEUTRALISER LEUR PONDÉRATION SI CETTE NOTE NE PEUT DONNER LIEU À VÉRIFICATION AU STADE DE L'ANALYSE DES OFFRES - B) ESPÈCE.
</SCT>
<ANA ID="9A"> 39-02-005 1) Le pouvoir adjudicateur définit librement la méthode de notation pour la mise en oeuvre de chacun des critères de sélection des offres qu'il a retenus et rendus publics. Toutefois, une méthode de notation est entachée d'irrégularité si, en méconnaissance des principes fondamentaux d'égalité de traitement des candidats et de transparence des procédures, elle est, par elle-même, de nature à priver de leur portée les critères de sélection ou à neutraliser leur pondération et est, de ce fait, susceptible de conduire, pour la mise en oeuvre de chaque critère, à ce que la meilleure note ne soit pas attribuée à la meilleure offre, ou, au regard de l'ensemble des critères pondérés, à ce que l'offre économiquement la plus avantageuse ne soit pas choisie. Il en va ainsi alors même que le pouvoir adjudicateur, qui n'y est pas tenu, aurait rendu publique, dans l'avis d'appel à concurrence ou les documents de la consultation, une telle méthode de notation.,,,2) a) Une méthode de notation des offres par laquelle le pouvoir adjudicateur laisse aux candidats le soin de fixer, pour l'un des critères ou sous-critères, la note qu'ils estiment devoir leur être attribuée est, par elle-même, de nature à priver de portée utile le critère ou sous-critère en cause si cette note ne peut donner lieu à vérification au stade de l'analyse des offres, quand bien même les documents de la consultation prévoiraient que le candidat attributaire qui ne respecterait pas, lors de l'exécution du marché, les engagements que cette note entend traduire pourrait, de ce fait, se voir infliger des pénalités.,,,b) Procédure d'appel d'offres ouvert pour la passation d'un marché alloti de services réguliers de transports publics non urbains de personnes par voie terrestre. Pouvoir adjudicateur ayant retenu trois critères de jugement des offres, dont la valeur technique pondérée respectivement à 25 %. La notation de l'un des deux sous-critères de la valeur technique, pondéré à hauteur de 20 %, dépendait exclusivement du niveau de qualité du service que le candidat s'estimait en mesure de garantir et ne résultait que de l'indication par le candidat lui-même d'une note dite note qualité qu'il devait s'attribuer à l'aide d'un outil de simulation. Les éléments mentionnés pour l'auto-évaluation, portant sur la propreté du véhicule, l'ambiance générale au sein du véhicule, la ponctualité, la conduite respectueuse du code de la route ou la qualité de l'accueil à bord du véhicule ne pouvaient faire l'objet d'une évaluation objective au stade de l'analyse des offres.,,,Cour administrative d'appel ayant estimé que, par le recours à une telle méthode de notation, le pouvoir adjudicateur n'avait pas renoncé à apprécier la valeur des offres au motif, d'une part, qu'il avait précisément défini et communiqué aux candidats les modalités selon lesquelles le sous-critère en litige serait apprécié et, d'autre part, que la note attribuée aux candidats avait vocation à servir de référence pour la détermination de leur note annuelle qualité et le calcul d'éventuelles pénalités en cas de manquement à cet engagement.... ,,En statuant ainsi, la cour administrative d'appel a commis une erreur de droit.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 3 novembre 2011, Commune de Belleville-sur-Loire, n° 373362, p. 323.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
