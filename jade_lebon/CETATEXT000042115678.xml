<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042115678</ID>
<ANCIEN_ID>JG_L_2020_07_000000439367</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/11/56/CETATEXT000042115678.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 5ème chambres réunies, 10/07/2020, 439367</TITRE>
<DATE_DEC>2020-07-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>439367</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SEVAUX, MATHONNET ; SCP DIDIER, PINET</AVOCATS>
<RAPPORTEUR>Mme Catherine Moreau</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CECHR:2020:439367.20200710</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par un jugement n° 1913645/6-2 du 6 mars 2020, enregistré le 9 mars 2020 au secrétariat du contentieux du Conseil d'État, le tribunal administratif de Paris, avant de statuer sur la demande de l'Assistance publique - Hôpitaux de Paris tendant, d'une part, à l'annulation du titre exécutoire n° 2143 émis le 23 octobre 2018 par l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales à son encontre pour un montant de 870,67 euros et, d'autre part, à ce qu'elle soit déchargée de la somme en cause, a décidé, par application des dispositions de l'article L. 113-1 du code de justice administrative, de transmettre le dossier de cette demande au Conseil d'État, en soumettant à son examen les questions suivantes :<br/>
<br/>
              1°) Les dispositions du B du V de l'article 55 de la loi de finances rectificative pour 2010 doivent-elles être lues, dans le silence des textes, comme s'appliquant également aux établissements publics administratifs de l'Etat ' <br/>
<br/>
              2°) En cas de réponse affirmative à la première question, pour justifier de la signature du titre exécutoire, l'ordonnateur ne doit-il produire que l'ordre de recouvrer signé ou peut-il transmettre tout autre document transmis au débiteur permettant d'identifier sans ambiguïté l'identité du signataire du titre de perception en litige, telle qu'une précédente lettre signée par l'ordonnateur et jointe au titre de perception '<br/>
<br/>
              3°) En cas de réponse négative à la première question, l'ordonnateur d'un établissement public administratif de l'Etat pourrait-il se prévaloir de la jurisprudence selon laquelle les exigences de signature figurant à l'article L. 212-1 du code des relations entre le public et l'administration ne trouveraient à s'appliquer qu'à l'original des titres exécutoires, et non à leur ampliation, lequel original pourrait être produit en cours d'instance '  <br/>
<br/>
              4°) En cas de réponse négative à la première et à la troisième questions, c'est-à-dire en cas d'annulation du titre exécutoire pour vice de forme, et dans l'hypothèse où l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales a sollicité du juge la condamnation de l'établissement hospitalier à lui verser la pénalité prévue à l'article L. 1142-15 du code de la santé publique, le juge peut-il, après avoir constaté le bien-fondé de la créance, faire droit à de telles conclusions reconventionnelles '<br/>
<br/>
              Des observations, enregistrées le 21 avril 2020, ont été présentées par le ministre de l'économie et des finances et le ministre de l'action et des comptes publics.<br/>
<br/>
              Des observations, enregistrées le 23 avril 2020, ont été présentées par l'Assistance publique - Hôpitaux de Paris.<br/>
<br/>
              Des observations, enregistrées le 8 juin 2020, ont été présentées par l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code des relations entre le public et l'administration ;<br/>
              - le code de justice administrative, notamment son article L. 113-1 et l'ordonnance n° 2020-305 du 25 mars 2020 modifiée ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Catherine Moreau, conseiller d'Etat en service extraordinaire, <br/>
<br/>
              - les conclusions de M. Stéphane Hoynck, rapporteur public ;<br/>
<br/>
              - La parole ayant été donnée, avant et après les conclusions, à la SCP Didier, Pinet, avocat de l'Assistance publique - Hôpitaux de Paris et à la SCP Sevaux, Mathonnet, avocat de l'Office national d'indemnisation des accidents médicaux ;<br/>
<br/>
<br/>
REND L'AVIS SUIVANT<br/>
<br/>
<br/>
<br/>
              1. Le tribunal administratif de Paris a soumis au Conseil d'Etat les questions visées ci-dessus pour répondre au moyen soulevé par l'Assistance publique - Hôpitaux de Paris et tiré de ce que le titre exécutoire émis à son encontre par l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales, faute d'être signé, méconnaîtrait l'article L. 212-1 du code des relations entre le public et l'administration.<br/>
<br/>
              2. Aux termes du premier alinéa de l'article L. 212-1 du code des relations entre le public et l'administration : " Toute décision prise par une administration comporte la signature de son auteur, ainsi que la mention, en caractères lisibles, du prénom, du nom et de la qualité de celui-ci ". Les titres exécutoires émis par les personnes publiques doivent, en vertu de ces dispositions, être signés et comporter les prénom, nom et qualité de leur auteur.<br/>
<br/>
              3. Toutefois, il résulte des articles L. 100-1 et L. 100-3 du code des relations entre le public et l'administration que les dispositions de ce code ne s'appliquent pas, sauf exception, aux relations entre personnes morales de droit public. L'article L. 212-1 du code des relations entre le public et l'administration n'est ainsi pas applicable dans un litige opposant deux personnes publiques. Dès lors, il ne peut être utilement soutenu qu'un titre exécutoire émis par un établissement public à l'encontre d'un autre établissement public méconnaîtrait cette disposition.<br/>
<br/>
              4. Il résulte de ce qui précède qu'il n'y a pas lieu de répondre aux questions posées par le tribunal administratif de Paris. <br/>
<br/>
              Le présent avis sera notifié au tribunal administratif de Paris, à l'Assistance publique - Hôpitaux de Paris, à l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales, au ministre des solidarités et de la santé, au ministre de l'économie et des finances et au ministre de l'action et des comptes publics. <br/>
<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-03-01 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - FORME ET PROCÉDURE. QUESTIONS GÉNÉRALES. - OBLIGATION DE SIGNATURE ET DE MENTION DES PRÉNOM, NOM ET QUALITÉ DE L'AUTEUR DE LA DÉCISION (ART. L. 212-1 DU CRPA) - CHAMP D'APPLICATION - TITRES EXÉCUTOIRES - 1) PRINCIPE - INCLUSION - 2) EXCEPTION - TITRE EXÉCUTOIRE ÉMIS PAR UN ÉTABLISSEMENT PUBLIC À L'ENCONTRE D'UN AUTRE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">18-03-02-01-01 COMPTABILITÉ PUBLIQUE ET BUDGET. CRÉANCES DES COLLECTIVITÉS PUBLIQUES. RECOUVREMENT. PROCÉDURE. ÉTAT EXÉCUTOIRE. - OBLIGATION DE SIGNATURE ET DE MENTION DES PRÉNOM, NOM ET QUALITÉ DE L'AUTEUR DE LA DÉCISION (ART. L. 212-1 DU CRPA) - CHAMP D'APPLICATION - TITRES EXÉCUTOIRES - 1) PRINCIPE - INCLUSION - 2) EXCEPTION - TITRE EXÉCUTOIRE ÉMIS PAR UN ÉTABLISSEMENT PUBLIC À L'ENCONTRE D'UN AUTRE [RJ1].
</SCT>
<ANA ID="9A"> 01-03-01 1) En vertu du premier alinéa de l'article L. 212-1 du code des relations entre le public et l'administration (CRPA), les titres exécutoires émis par les personnes publiques doivent être signés et comporter les prénom, nom et qualité de leur auteur.,,,2) Toutefois, il résulte des articles L. 100-1 et L. 100-3 du CRPA que ce code ne s'applique pas, sauf exception, aux relations entre personnes morales de droit public. L'article L. 212-1 du CRPA n'est ainsi pas applicable dans un litige opposant deux personnes publiques. Dès lors, il ne peut être utilement soutenu qu'un titre exécutoire émis par un établissement public à l'encontre d'un autre établissement public méconnaîtrait cette disposition.</ANA>
<ANA ID="9B"> 18-03-02-01-01 1) En vertu du premier alinéa de l'article L. 212-1 du code des relations entre le public et l'administration (CRPA), les titres exécutoires émis par les personnes publiques doivent être signés et comporter les prénom, nom et qualité de leur auteur.,,,2) Toutefois, il résulte des articles L. 100-1 et L. 100-3 du CRPA que ce code ne s'applique pas, sauf exception, aux relations entre personnes morales de droit public. L'article L. 212-1 du CRPA n'est ainsi pas applicable dans un litige opposant deux personnes publiques. Dès lors, il ne peut être utilement soutenu qu'un titre exécutoire émis par un établissement public à l'encontre d'un autre établissement public méconnaîtrait cette disposition.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., s'agissant des décisions adressées par l'Etat à un établissement public dont il assure la tutelle, CE, 26 juillet 2011, Société Air France et autres, n°s 329818 340540, T. pp. 734-830-1176.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
