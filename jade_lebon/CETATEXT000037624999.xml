<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037624999</ID>
<ANCIEN_ID>JG_L_2018_11_000000412837</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/62/49/CETATEXT000037624999.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 19/11/2018, 412837</TITRE>
<DATE_DEC>2018-11-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>412837</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Contentieux des pensions</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARADUC, DUHAMEL, RAMEIX</AVOCATS>
<RAPPORTEUR>M. Marc Pichon de Vendeuil</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier Henrard</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:412837.20181119</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A...C...a demandé au tribunal administratif de Nantes, en premier lieu, d'annuler les titres de pension de réversion n° B 13 551304 H du 4 septembre 2013 et n° B 14 550615 C du 27 janvier 2014 en tant qu'ils ne prennent pas en compte toutes les majorations et bonifications de pension auxquelles son défunt époux pouvait prétendre et qu'ils n'appliquent pas la capitalisation des intérêts retenue pas le tribunal administratif de Poitiers dans son jugement n° 0900372 en date du 27 juin 2013 et, en deuxième lieu, d'enjoindre au ministre de la défense et à la direction générale des finances publiques de réexaminer sa situation, de procéder à une nouvelle liquidation de la pension et de lui délivrer un nouveau titre de pension.<br/>
<br/>
              Par un jugement n°s 1400950, 1404434 du 12 juillet 2017, le tribunal administratif de Nantes a, après avoir estimé qu'il n'y avait pas lieu de statuer sur les conclusions tendant à l'annulation du premier titre de pension, annulé le titre de pension n° B 14 550615 C du 24 mars 2014 en tant qu'il ne prend pas en compte les services " harkis " accomplis par M. C... du 1er août 1958 au 2 juillet 1962 et a enjoint au ministre de l'action et des comptes publics ainsi qu'à la ministre des armées de réexaminer la situation de Mme C...et de procéder, sous un délai de deux mois, à une nouvelle liquidation de sa pension en prenant en compte les services accomplis du 1er août 1958 au 2 juillet 1962.<br/>
<br/>
              Par un pourvoi, enregistré le 27 juillet 2017 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'action et des comptes publics demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement en tant, d'une part, qu'il a annulé le titre de pension de réversion de Mme C...en tant qu'il ne prend pas en compte des services accomplis par son mari et, d'autre part,  qu'il lui a enjoint de réexaminer la situation de Mme C...et de procéder à une nouvelle liquidation de sa pension ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter les demandes de MmeC....<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code des pensions civiles et militaires de retraite ;<br/>
              - la loi n° 48-1450 du 20 septembre 1948 ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - l'ordonnance n° 62-801 du 16 juillet 1962 ;<br/>
              - le décret n° 51-590 du 23 mai 1951 ;<br/>
              - le décret n° 76-1111 du 29 novembre 1976 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Marc Pichon de Vendeuil, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Olivier Henrard, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Baraduc, Duhamel, Rameix, avocat de Mme C...;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. B...C..., né le 4 mai 1900 en Algérie, a été rayé, le 22 juillet 1940, des cadres de l'armée française, au sein de laquelle il détenait le grade de sergent-chef, et a bénéficié, par arrêté du 6 février 1953, d'une pension de retraite proportionnelle ; qu'il est décédé le 10 août 1985 ; que, le 18 avril 2007, sa veuve, MmeC..., a sollicité du ministre de la défense le bénéfice d'une pension de réversion ; qu'au cours des procédures gracieuses puis contentieuses engagées par MmeC..., celle-ci a été destinataire de plusieurs titres de pension dont le dernier, émis le 24 mars 2014 sous le n° B 14 550615 C, fait état d'un indice brut 332 et d'une part payable de 50 % ; que le ministre de l'action et des comptes publics se pourvoit en cassation contre le jugement du 12 juillet 2017 par lequel le tribunal administratif de Nantes a, d'une part, annulé ce titre de pension en tant qu'il ne prend pas en compte les services accomplis par M. C...dans les formations supplétives en Algérie du 1er août 1958 au 2 juillet 1962 et, d'autre part, enjoint au ministre de l'action et des comptes publics et à la ministre des armées, chacun pour ce qui le concerne, de réexaminer la situation de Mme C... afin de procéder à une nouvelle liquidation de sa pension en prenant en compte ces services ;<br/>
<br/>
              2. Considérant, d'une part, qu'aux termes du premier alinéa de l'article L. 1 du code des pensions civiles et militaires de retraite : " La pension est une allocation pécuniaire personnelle et viagère accordée aux fonctionnaires civils et militaires et, après leur décès, à leurs ayants cause désignés par la loi, en rémunération des services qu'ils ont accomplis jusqu'à la cessation régulière de leurs fonctions " ; qu'aux termes de l'article L. 38 du même code : " Les conjoints d'un fonctionnaire civil ont droit à une pension de réversion égale à 50 % de la pension obtenue par le fonctionnaire ou qu'il aurait pu obtenir au jour de son décès (...) " ; <br/>
<br/>
              3. Considérant, d'autre part, qu'aux termes de l'article 53 de la loi du 20 septembre 1948 portant réforme du régime des pensions civiles et militaires, qui a été codifié à l'article 77 du code des pensions par le décret du 23 mai 1951 portant codification des textes législatifs concernant les pensions civiles et militaires de retraite et qui était en vigueur à la date à laquelle a été liquidée la pension de M. C...: " La pension et la rente viagère d'invalidité peuvent être révisées à tout moment en cas d'erreur ou d'omission quelle que soit la nature de celles-ci. Elles peuvent être modifiées ou supprimées si la concession en a été faite dans des conditions contraires aux prescriptions du présent code " ; qu'aux termes de l'article 2 de la loi du 26 décembre 1964 portant réforme du code des pensions civiles et militaires de retraite : " Les dispositions du code annexé à la présente loi, à l'exception de celles du titre III du livre II, ne sont applicables qu'aux fonctionnaires et militaires et à leurs ayants cause dont les droits résultant de la radiation des cadres ou du décès s'ouvriront à partir de la date d'effet de la présente loi " ; qu'ainsi, les dispositions de l'article L. 55 du code des pensions civiles et militaires de retraite, lui-même introduit par la loi du 26 décembre 1964, qui ont pour objet de limiter dans le temps la faculté de demander la révision de la pension en cas d'erreur de droit, ne sont pas applicables aux pensions concédées antérieurement à l'entrée en vigueur de la loi du 26 décembre 1964 ; <br/>
<br/>
              4. Considérant qu'il résulte de la combinaison des dispositions précitées que le caractère personnel d'une pension de retraite ne s'oppose pas à ce que le titulaire d'une pension de réversion se prévale, à l'appui d'un recours contre cette pension ou d'une demande de révision, d'une illégalité entachant le calcul de la pension de son conjoint que celui-ci n'a pas contestée, lorsque cette pension ne peut être regardée comme définitive en raison, soit de ce qu'elle est encore susceptible de recours, soit de ce qu'une demande de révision peut encore être adressée à l'administration dans les conditions posées par l'article L. 55 du code des pensions civiles et militaires de retraite pour les pensions concédées après l'entrée en vigueur de la loi du 26 décembre 1964 ; que pour les pensions concédées avant l'entrée en vigueur de cette loi, aucun délai ne peut en revanche être opposé à une demande de révision ; que par suite, aucun délai n'était opposable à la demande de révision de la pension de réversion de Mme C...dès lors que la pension de son mari lui avait été concédée avant l'entrée en vigueur de la loi du 26 décembre 1964 ; que, dans ces conditions, la demande de Mme C...pouvait notamment être fondée sur la prise en compte des dispositions de l'ordonnance du 16 juillet 1962 édictant des dispositions en faveur des personnels en service dans les makhzen d'Algérie et du décret du 29 novembre 1976 pris pour l'application de cette ordonnance ; que, dès lors, le tribunal administratif de Nantes n'a pas commis d'erreur de droit en estimant que Mme C...pouvait, à l'appui de sa demande de révision de la pension de réversion dont elle est titulaire, se prévaloir d'une illégalité entachant la pension de son conjoint en raison d'une non prise en compte des services accomplis comme " harki " en Algérie, alors même que ses recours en ce sens ont été formés au plus tôt le 6 novembre 2013 ;<br/>
<br/>
              5. Considérant, enfin, que dès lors que la demande de Mme C...ne se fondait pas sur le fait que la pension de son époux aurait encore été susceptible de recours, le ministre ne peut utilement soutenir que le principe de sécurité juridique, qui implique que ne puissent être remises en cause sans condition de délai des situations consolidées par l'effet du temps, faisait obstacle à ce que puisse être contestée indéfiniment une décision administrative individuelle qui a été notifiée à son destinataire ou dont il est établi, à défaut d'une telle notification, que celui-ci a eu connaissance ;<br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que le pourvoi du ministre de l'action et des comptes publics doit être rejeté ;<br/>
<br/>
              7. Considérant que Mme C...a obtenu le bénéfice de l'aide juridictionnelle ; que, par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ; qu'il y a lieu, dans les circonstances de l'espèce, et sous réserve que la SCP Baraduc, Duhamel, Rameix, avocat de MmeC..., renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à la SCP Baraduc, Duhamel, Rameix ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi du ministre de l'action et des comptes publics est rejeté.<br/>
Article 2 : L'Etat versera à la SCP Baraduc, Duhamel, Rameix, avocat de MmeC..., une somme de 3 000 euros en application des dispositions du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
Article 3 : La présente décision sera notifiée au ministre de l'action et des comptes publics et à Mme A...C....<br/>
Copie en sera adressée à la ministre des armées.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">48-02-01-09 PENSIONS. PENSIONS CIVILES ET MILITAIRES DE RETRAITE. QUESTIONS COMMUNES. AYANTS-CAUSE. - FACULTÉ DU TITULAIRE D'UNE PENSION DE RÉVERSION (ART. L. 38 DU CPCMR), À L'APPUI D'UN RECOURS CONTRE CETTE PENSION OU D'UNE DEMANDE DE RÉVISION, DE FAIRE ÉTAT D'UNE ILLÉGALITÉ ENTACHANT LE CALCUL DE LA PENSION DE SON CONJOINT ET QUE CELUI-CI N'A PAS CONTESTÉ - EXISTENCE - CONDITION - PENSION DU CONJOINT NE POUVANT ÊTRE REGARDÉE COMME DÉFINITIVE [RJ1] - NOTION - PENSION ENCORE SUSCEPTIBLE DE RECOURS [RJ2] - PENSION POUVANT ENCORE FAIT L'OBJET D'UNE DEMANDE DE RÉVISION, DANS LES CONDITIONS POSÉES PAR L'ARTICLE L. 55 DU CPCMR POUR LES PENSIONS CONCÉDÉES APRÈS SON ENTRÉE EN VIGUEUR (ART. 2 DE LA LOI DU 26 DÉCEMBRE 1964), OU SANS DÉLAI POUR LES PENSIONS CONCÉDÉES ANTÉRIEUREMENT (ART. 53 DE LA LOI DU 20 SEPTEMBRE 1948, CODIFIÉ À L'ART. 77 DU CPCMR PAR LE DÉCRET DU 23 MAI 1951).
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">48-02-01-10-005 PENSIONS. PENSIONS CIVILES ET MILITAIRES DE RETRAITE. QUESTIONS COMMUNES. RÉVISION DES PENSIONS ANTÉRIEUREMENT CONCÉDÉES. RÉVISION EN CAS D'ERREUR (ARTICLE L. 55 DU CODE). - FACULTÉ DU TITULAIRE D'UNE PENSION DE RÉVERSION (ART. L. 38 DU CPCMR), À L'APPUI D'UN RECOURS CONTRE CETTE PENSION OU D'UNE DEMANDE DE RÉVISION, DE FAIRE ÉTAT D'UNE ILLÉGALITÉ ENTACHANT LE CALCUL DE LA PENSION DE SON CONJOINT ET QUE CELUI-CI N'A PAS CONTESTÉ - EXISTENCE - CONDITION - PENSION DU CONJOINT NE POUVANT ÊTRE REGARDÉE COMME DÉFINITIVE [RJ1] - NOTION - PENSION ENCORE SUSCEPTIBLE DE RECOURS [RJ2] - PENSION POUVANT ENCORE FAIT L'OBJET D'UNE DEMANDE DE RÉVISION, DANS LES CONDITIONS POSÉES PAR L'ARTICLE L. 55 DU CPCMR POUR LES PENSIONS CONCÉDÉES APRÈS SON ENTRÉE EN VIGUEUR (ART. 2 DE LA LOI DU 26 DÉCEMBRE 1964), OU SANS DÉLAI POUR LES PENSIONS CONCÉDÉES ANTÉRIEUREMENT (ART. 53 DE LA LOI DU 20 SEPTEMBRE 1948, CODIFIÉ À L'ART. 77 DU CPCMR PAR LE DÉCRET DU 23 MAI 1951).
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">48-02-04 PENSIONS. PENSIONS CIVILES ET MILITAIRES DE RETRAITE. CONTENTIEUX DES PENSIONS CIVILES ET MILITAIRES DE RETRAITE. - FACULTÉ DU TITULAIRE D'UNE PENSION DE RÉVERSION (ART. L. 38 DU CPCMR), À L'APPUI D'UN RECOURS CONTRE CETTE PENSION OU D'UNE DEMANDE DE RÉVISION, DE FAIRE ÉTAT D'UNE ILLÉGALITÉ ENTACHANT LE CALCUL DE LA PENSION DE SON CONJOINT ET QUE CELUI-CI N'A PAS CONTESTÉ - EXISTENCE - CONDITION - PENSION DU CONJOINT NE POUVANT ÊTRE REGARDÉE COMME DÉFINITIVE [RJ1] - NOTION - PENSION ENCORE SUSCEPTIBLE DE RECOURS [RJ2] - PENSION POUVANT ENCORE FAIT L'OBJET D'UNE DEMANDE DE RÉVISION, DANS LES CONDITIONS POSÉES PAR L'ARTICLE L. 55 DU CPCMR POUR LES PENSIONS CONCÉDÉES APRÈS SON ENTRÉE EN VIGUEUR (ART. 2 DE LA LOI DU 26 DÉCEMBRE 1964), OU SANS DÉLAI POUR LES PENSIONS CONCÉDÉES ANTÉRIEUREMENT (ART. 53 DE LA LOI DU 20 SEPTEMBRE 1948, CODIFIÉ À L'ART. 77 DU CPCMR PAR LE DÉCRET DU 23 MAI 1951).
</SCT>
<ANA ID="9A"> 48-02-01-09 Le caractère personnel d'une pension de retraite ne s'oppose pas à ce que le titulaire d'une pension de réversion se prévale, à l'appui d'un recours contre cette pension ou d'une demande de révision, d'une illégalité entachant le calcul de la pension de son conjoint que celui-ci n'a pas contestée, lorsque cette pension ne peut être regardée comme définitive en raison, soit de ce qu'elle est encore susceptible de recours, soit de ce qu'une demande de révision peut encore être adressée à l'administration dans les conditions posées par l'article L. 55 du code des pensions civiles et militaires de retraite (CPCMR) pour les pensions concédées après l'entrée en vigueur de la loi n° 64-1339 du 26 décembre 1964. Pour les pensions concédées avant l'entrée en vigueur de cette loi, aucun délai ne peut en revanche être opposé à une demande de révision.</ANA>
<ANA ID="9B"> 48-02-01-10-005 Le caractère personnel d'une pension de retraite ne s'oppose pas à ce que le titulaire d'une pension de réversion se prévale, à l'appui d'un recours contre cette pension ou d'une demande de révision, d'une illégalité entachant le calcul de la pension de son conjoint que celui-ci n'a pas contestée, lorsque cette pension ne peut être regardée comme définitive en raison, soit de ce qu'elle est encore susceptible de recours, soit de ce qu'une demande de révision peut encore être adressée à l'administration dans les conditions posées par l'article L. 55 du code des pensions civiles et militaires de retraite (CPCMR) pour les pensions concédées après l'entrée en vigueur de la loi n° 64-1339 du 26 décembre 1964. Pour les pensions concédées avant l'entrée en vigueur de cette loi, aucun délai ne peut en revanche être opposé à une demande de révision.</ANA>
<ANA ID="9C"> 48-02-04 Le caractère personnel d'une pension de retraite ne s'oppose pas à ce que le titulaire d'une pension de réversion se prévale, à l'appui d'un recours contre cette pension ou d'une demande de révision, d'une illégalité entachant le calcul de la pension de son conjoint que celui-ci n'a pas contestée, lorsque cette pension ne peut être regardée comme définitive en raison, soit de ce qu'elle est encore susceptible de recours, soit de ce qu'une demande de révision peut encore être adressée à l'administration dans les conditions posées par l'article L. 55 du code des pensions civiles et militaires de retraite (CPCMR) pour les pensions concédées après l'entrée en vigueur de la loi n° 64-1339 du 26 décembre 1964. Pour les pensions concédées avant l'entrée en vigueur de cette loi, aucun délai ne peut en revanche être opposé à une demande de révision.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 7 mai 2014, Ministre du budget, des comptes publics et de la réforme de l'Etat c/ Mme,, n° 355961, p. 129.,,[RJ2] Comp., pour l'état antérieur de la jurisprudence, CE, 7 mai 2014, Ministre du budget, des comptes publics et de la réforme de l'Etat c/ Mme,, n° 355961, p. 129.,.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
