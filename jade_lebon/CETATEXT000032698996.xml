<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032698996</ID>
<ANCIEN_ID>JG_L_2016_06_000000389062</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/69/89/CETATEXT000032698996.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 1ère chambres réunies, 08/06/2016, 389062</TITRE>
<DATE_DEC>2016-06-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>389062</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>M. Jean-Baptiste de Froment</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier de Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:389062.20160608</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 27 mars et 26 juin 2015 et le 4 avril 2016 au secrétariat du contentieux du Conseil d'Etat, l'association " Baronnies libres sans parc " demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2015-56 du 26 janvier 2015 portant classement du parc naturel régional des Baronnies provençales (régions Rhône-Alpes et Provence-Alpes-Côte d'Azur) ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule ;<br/>
              - le code de l'environnement ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Baptiste de Froment, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de l'association " Baronnies libres sans parc " ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que l'association " Baronnies libres sans parc " demande au Conseil d'Etat d'annuler pour excès de pouvoir le décret du 26 janvier 2015 portant classement du parc naturel régional des Baronnies provençales (régions Rhône-Alpes et Provence-Alpes-Côte d'Azur) ; <br/>
<br/>
              Sur la légalité externe du décret attaqué :<br/>
<br/>
              2. Considérant, en premier lieu, que les dispositions du I de l'article R. 333-5 du code de l'environnement prévoyant que la procédure de classement ou de renouvellement de classement est engagée " par une délibération motivée du conseil régional qui prescrit l'élaboration ou la révision de la charte, détermine un périmètre d'étude et définit les modalités de l'association des collectivités territoriales concernées et de leurs groupements ainsi que celles de la concertation avec les partenaires associés ", issue d'un décret du 2 août 2005, sont entrées en vigueur postérieurement à l'adoption, en 2004, des délibérations préalables des conseils régionaux ; que, par suite, le moyen tiré de leur méconnaissance ne peut qu'être écarté ;<br/>
<br/>
              3. Considérant, en deuxième lieu, qu'aux termes de l'article R. 333-6 du code de l'environnement : " Le préfet de région définit avec le président du conseil régional, et avec le président du syndicat mixte d'aménagement et de gestion du parc en cas de révision, les modalités d'association de l'Etat à l'élaboration ou à la révision de la charte dès que la délibération prescrivant celle-ci lui a été transmise et leur communique la liste des services de l'Etat et de ses établissements publics qui y seront associés. / Il leur transmet son avis motivé sur l'opportunité du projet " ; qu'il ressort des pièces du dossier que conformément à ces dispositions, le préfet de la région Rhône-Alpes, préfet coordinateur, a transmis par lettre du 21 février 2008 son avis motivé sur l'opportunité du projet, accompagné de la liste des services de l'Etat qui y seraient associés et que ceux-ci ont été effectivement associés à l'élaboration de la charte ; que, par suite, le moyen tiré de la méconnaissance de l'article R. 333-6 du code de l'environnement ne peut qu'être écarté ; <br/>
<br/>
              4. Considérant, en troisième lieu, qu'il résulte des dispositions combinées des articles L. 414-4 et L. 122-4 et du 10° de l'article R. 122-17 du code de l'environnement que les chartes de parcs naturels régionaux font l'objet d'une évaluation environnementale ainsi que, par voie de conséquence, d'une évaluation de leurs incidences sur les sites Natura 2000 ; que, toutefois, en vertu de l'article 7 du décret du 2 mai 2012 relatif à l'évaluation de certains plans et documents ayant une incidence sur l'environnement, cette obligation n'est pas applicable aux chartes de parcs naturels régionaux dont l'élaboration ou la révision a été prescrite à la date du 1er janvier 2013 par délibération du conseil régional ; que l'élaboration de la charte du parc naturel régional des Baronnies provençales ayant été prescrite par délibérations des conseils régionaux des 16 et 17 décembre 2004, ce document n'avait pas à être soumis à évaluation environnementale, et pas davantage à une évaluation de ses incidences sur les sites Natura 2000 ; que, par suite, le moyen tiré de l'absence de cette dernière évaluation dans le dossier d'enquête publique ne peut qu'être écarté ; <br/>
<br/>
              5. Considérant, en quatrième lieu, qu'aux termes de l'article R. 333-7 du code de l'environnement : " La région ou, par délégation, le syndicat mixte d'aménagement et de gestion du parc naturel régional adresse ce projet de charte aux départements, aux communes et aux établissements publics de coopération intercommunale à fiscalité propre territorialement concernés, qui disposent d'un délai de quatre mois à compter de leur saisine pour approuver la charte. / Le conseil régional approuve ensuite la charte et, au regard des délibérations favorables recueillies et des critères de classement, détermine le périmètre finalement proposé au classement. / Le territoire d'une commune ne peut être proposé au classement lorsqu'un établissement public de coopération intercommunale à fiscalité propre dont cette commune est membre a délibéré défavorablement. Il en va de même lorsqu'une commune n'a pas approuvé la charte, alors même qu'un établissement public de coopération intercommunale à fiscalité propre dont elle est membre a délibéré favorablement " ; que, d'une part, contrairement à ce qui est soutenu, la circonstance qu'un certain nombre de communes et des établissements publics de coopération intercommunale concernés par le projet de charte ne l'ont pas approuvé ne faisait pas obstacle à ce que les conseils régionaux de Provence-Alpes Côte d'Azur et de Rhône-Alpes approuvent, ainsi qu'il l'ont fait, ce projet ; que, d'autre part, ces conseils régionaux n'ont proposé au classement que le territoire des 86 communes ayant délibéré en faveur du projet, conformément aux dispositions de l'article R. 333-7 ; que, par suite, le moyen tiré de la méconnaissance des dispositions de cet article ne peut être accueilli ; <br/>
<br/>
              6. Considérant, en cinquième lieu, qu'en vertu de l'article R. 333-8 du code de l'environnement : " Le projet de charte approuvé, accompagné des accords des collectivités territoriales et des établissements mentionnés à l'article R. 333-7, est transmis par le préfet de région, avec son avis motivé, au ministre chargé de l'environnement " ; qu'il ressort des pièces du dossier que, par lettre du 30 juillet 2014, le préfet de la région Rhône-Alpes, préfet coordonnateur, a transmis au ministre chargé de l'environnement le projet de charte accompagné de son avis motivé ; que le préfet de la région Provence-Alpes-Côte d'Azur a transmis son avis par une lettre du 11 août 2014 ; que, par suite, le moyen tiré de la méconnaissance de l'obligation prescrite par l'article R. 333-8 du code de l'environnement doit être écarté ; <br/>
<br/>
              7. Considérant, en sixième lieu, que, contrairement à ce qui est soutenu, il ressort des pièces du dossier que la liste des communes et des établissements publics de coopération intercommunale à fiscalité propre ayant approuvé la charte du parc naturel régional litigieux et les statuts du syndicat mixte de gestion du parc ont été annexés à la charte, conformément aux dispositions de l'article R. 333-3 du code de l'environnement ; <br/>
<br/>
              8. Considérant, en septième et dernier lieu, que le respect du droit de toute personne à accéder aux informations relatives à l'environnement détenues par les autorités publiques et à participer à l'élaboration des décisions publiques ayant une incidence sur l'environnement, énoncé à l'article 7 de la charte de l'environnement s'apprécie au regard des dispositions législatives qui en précisent les conditions et limites, complétées le cas échéant par les mesures d'application de ces dispositions définies par le pouvoir réglementaire ; qu'aux termes de l'article R. 123-16 du code de l'environnement : " Les jours et heures où le public pourra consulter le dossier et présenter ses observations sont fixés de manière à permettre la participation de la plus grande partie de la population, compte tenu notamment de ses horaires normaux de travail ; ils comprennent au minimum les jours et heures habituels d'ouverture au public de chacun des lieux où est déposé le dossier ; ils peuvent en outre comprendre plusieurs demi-journées prises parmi les samedis, dimanches et jours fériés " ; que ces dispositions n'imposaient pas de prévoir une permanence le soir ou le samedi pour la consultation du dossier d'enquête publique ; que l'association requérante ne fait par ailleurs état d'aucun élément précis de nature à établir que le public aurait été effectivement privé de la possibilité de consulter le dossier d'enquête publique du fait des horaires d'ouverture retenus pour cette consultation ; qu'il ressort au demeurant des pièces du dossier que la participation du public à l'enquête a été importante ; que le moyen tiré de l'irrégularité de la procédure d'enquête publique au regard de l'article R. 123-16 du code de l'environnement doit, par suite, être écarté ainsi que, par voie de conséquence, le moyen tiré de la méconnaissance des dispositions de l'article 7 de la charte de l'environnement ;<br/>
<br/>
              Sur la légalité interne du décret attaqué :<br/>
<br/>
              9. Considérant qu'aux termes de l'article L. 333-1 du code de l'environnement : " I  Les parcs naturels régionaux concourent à la politique de protection de l'environnement, d'aménagement du territoire, de développement économique et social et d'éducation et de formation du public. A cette fin, ils ont vocation à être des territoires d'expérimentation locale pour l'innovation au service du développement durable des territoires ruraux. Ils constituent un cadre privilégié des actions menées par les collectivités publiques en faveur de la préservation des paysages et du patrimoine naturel et culturel. / II. - La charte du parc détermine pour le territoire du parc naturel régional les orientations de protection, de mise en valeur et de développement et les mesures permettant de les mettre en oeuvre. Elle comporte un plan élaboré à partir d'un inventaire du patrimoine indiquant les différentes zones du parc et leur vocation. La charte détermine les orientations et les principes fondamentaux de protection des structures paysagères sur le territoire du parc. (...) / V. - L'Etat et les collectivités territoriales adhérant à la charte appliquent les orientations et les mesures de la charte dans l'exercice de leurs compétences sur le territoire du parc. Ils assurent, en conséquence, la cohérence de leurs actions et des moyens qu'ils y consacrent. (...) " ; que l'article R. 333-4 du même code dispose : " La décision de classement d'un territoire en " parc naturel régional " est fondée sur l'ensemble des critères suivants : / 1° La qualité et le caractère du territoire, de son patrimoine naturel et culturel, ainsi que de ses paysages représentant pour la ou les régions concernées un ensemble remarquable mais fragile et menacé, et comportant un intérêt reconnu au niveau national ; / 2° La cohérence et la pertinence des limites du territoire au regard de ce patrimoine et de ces paysages en tenant compte des éléments pouvant déprécier leur qualité et leur valeur ainsi que des dispositifs de protection et de mise en valeur existants ou projetés ; / 3° La qualité du projet de charte, notamment de son projet de développement fondé sur la protection et la mise en valeur du patrimoine et des paysages ; / 4° La détermination des collectivités et des établissements publics de coopération intercommunale à fiscalité propre dont l'engagement est essentiel pour mener à bien le projet ; / 5° La capacité de l'organisme chargé de l'aménagement et de la gestion du parc naturel régional à conduire le projet de façon cohérente " ; <br/>
<br/>
              10. Considérant qu'il résulte de ce qui a été dit au point 5 que le territoire d'une commune ayant refusé d'approuver le projet de charte d'un parc naturel régional ne peut être légalement inclus dans son périmètre ; qu'aucune disposition ou principe n'impose que le territoire d'un parc soit d'un seul tenant et sans enclave ; que les circonstances qu'une proportion, même notable, des communes incluses dans le périmètre d'étude du projet refusent finalement d'approuver la charte et que certaines soient enclavées ne sont pas, par elles-mêmes, de nature à faire obstacle au classement ; qu'il appartient au juge de l'excès de pouvoir d'apprécier si la décision délimitant, compte tenu de ces refus, le périmètre du parc n'est pas entachée d'une erreur manifeste d'appréciation au regard des objectifs poursuivis par la charte du parc et de l'ensemble des critères mentionnés à l'article R. 333-4 ; <br/>
<br/>
              11. Considérant, en premier lieu, que l'association requérante soutient que le territoire du parc naturel régional des Baronnies n'est pas menacé, dès lors qu'il fait déjà l'objet de plusieurs mesures de protection, telles que des zones Natura 2000, des zones naturelles d'intérêt écologique faunistique et floristique (ZNIEFF), des réserves biologiques ou un schéma départemental des espaces naturels sensibles ; que l'existence de telles mesures, qui ne concernent au demeurant qu'une partie du parc et poursuivent des objectifs qui ne se confondent pas avec ceux d'un parc naturel régional, n'est pas de nature à établir que les territoires qu'il couvre ne seraient pas " menacés " au sens des dispositions du 1° de l'article R. 333-4 du code de l'environnement citées au point 9 ; qu'il ressort en tout état de cause des pièces du dossier que ces territoires, qui recèlent un patrimoine naturel, agricole et culturel exceptionnel, et sont entourés de " villes-portes " connaissant une forte croissance démographique, sont à la fois fragiles et menacés, au sens des dispositions de cet article ; <br/>
<br/>
              12. Considérant, en second lieu, que l'association requérante soutient que le périmètre retenu ne satisferait pas au critère de cohérence énoncé au 2° de l'article R. 333-4 du code de l'environnement ; qu'il fait valoir que 44 des 130 communes retenues dans le périmètre d'études du parc, dont certaines sont enclavées, ont refusé d'approuver le projet de charte et ont donc été exclues du périmètre arrêté par le décret attaqué, comme l'autorité compétente était tenue de le faire ; que, toutefois, ainsi qu'il a été dit au point 10, ces circonstances ne sont pas par elle-même de nature à faire obstacle au classement ; que la grande majorité des communes, représentant environ les deux tiers de sa superficie, ont approuvé la charte, ainsi que 12 communautés de communes, alors même que certaines des communes membres de ces dernières ne sont pas classées, et l'ensemble des régions et départements concernés ; qu'il ressort des pièces du dossier que le périmètre retenu permet la mise en oeuvre des objectifs énoncés par la charte, qui concernent notamment la valorisation et la protection des espaces d'intérêt écologique prioritaires, des espaces patrimoniaux paysagers ou des sites géologiques à enjeux ; <br/>
<br/>
              13. Considérant qu'il résulte de ce qui vient d'être dit que le moyen tiré de ce que la décision délimitant le périmètre du parc serait entachée d'une erreur manifeste d'appréciation au regard des objectifs poursuivis par la charte du parc et de l'ensemble des critères mentionnés à l'article R. 333-4 doit être écarté ; <br/>
<br/>
              14. Considérant qu'il résulte de tout ce qui précède, sans qu'il soit besoin de statuer sur la fin de non-recevoir opposée par le ministre de l'écologie, du développement durable et de l'énergie, que l'association requérante n'est pas fondée à demander l'annulation du décret qu'elle attaque ; que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mis à la charge de l'Etat, qui n'est pas la partie perdante dans la présente instance, le versement à l'association " Baronnies libres sans parc " de la somme qu'elle demande au titre des frais exposés par elle et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête de l'association " Baronnies libres sans parc " est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à l'association " Baronnies libres sans parc ", au Premier ministre et à la ministre de l'environnement, de l'énergie et de la mer, chargée des relations internationales sur le climat. <br/>
Une copie en sera adressée pour information au syndicat mixte de gestion et d'aménagement du parc naturel régional des Baronnies.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">44-04-02-01 NATURE ET ENVIRONNEMENT. PARCS NATURELS. PARCS RÉGIONAUX. - 1) APPRÉCIATION DE LA LÉGALITÉ DE LA DÉLIBÉRATION ENGAGEANT LA PROCÉDURE DE CLASSEMENT D'UN PNR - APPRÉCIATION À LA DATE DE CETTE DÉLIBÉRATION - 2) TERRITOIRE DU PNR - A) INTERDICTION DES ENCLAVES - ABSENCE - B) CONTRÔLE DU JUGE SUR LE PÉRIMÈTRE DU PARC - CONTRÔLE RESTREINT.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-07-01-04-03 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. QUESTIONS GÉNÉRALES. MOYENS. MOYENS INOPÉRANTS. - APPRÉCIATION DE LA LÉGALITÉ D'UNE DÉLIBÉRATION ENGAGEANT LA PROCÉDURE DE CLASSEMENT D'UN PNR - APPRÉCIATION À LA DATE DE CETTE DÉLIBÉRATION - INOPÉRANCE DES MOYENS TIRÉS DE LA MÉCONNAISSANCE DE TEXTES ENTRÉS EN VIGUEUR PENDANT LA PROCÉDURE DE CLASSEMENNT.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-07-02-04 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. CONTRÔLE DU JUGE DE L'EXCÈS DE POUVOIR. APPRÉCIATIONS SOUMISES À UN CONTRÔLE RESTREINT. - DÉLIMITATION DU PÉRIMÈTRE D'UN PNR.
</SCT>
<ANA ID="9A"> 44-04-02-01 Recours contre un décret portant classement d'un parc naturel régional (PNR).... ,,1) La procédure de classement d'un PNR est engagée, en vertu de l'article R. 333-5 du code de l'environnement, par une délibération du conseil régional. Un moyen tiré de ce que cette délibération ne respecte pas un texte entré en vigueur postérieurement à son adoption, pendant la suite de la procédure de classement, est inopérant.... ,,2) Périmètre du PNR... ,,a) Le territoire d'une commune ayant refusé d'approuver le projet de charte d'un parc naturel régional ne peut être légalement, en vertu de l'article R. 333-7 du code de l'environnement, inclus dans son périmètre. En revanche, aucune disposition ou principe n'impose que le territoire d'un parc soit d'un seul tenant et sans enclave ; les circonstances qu'une proportion, même notable, des communes incluses dans le périmètre d'étude du projet refusent finalement d'approuver la charte et que certaines soient enclavées ne sont pas, par elles-mêmes, de nature à faire obstacle au classement.... ,,b) Il appartient au juge de l'excès de pouvoir d'apprécier si la décision délimitant, compte tenu de ces refus, le périmètre du parc n'est pas entachée d'une erreur manifeste d'appréciation au regard des objectifs poursuivis par la charte du parc et de l'ensemble des critères mentionnés à l'article R. 333-4 du code de l'environnement.</ANA>
<ANA ID="9B"> 54-07-01-04-03 La procédure de classement d'un parc naturel régional (PNR) est engagée, en vertu de l'article R. 333-5 du code de l'environnement, par une délibération du conseil régional. Un moyen tiré, à l'occasion d'un recours contre le décret portant classement du PNR, de ce que cette délibération ne respecte pas un texte entré en vigueur postérieurement à son adoption, pendant la suite de la procédure de classement, est inopérant.</ANA>
<ANA ID="9C"> 54-07-02-04 Il appartient au juge de l'excès de pouvoir d'apprécier si la décision délimitant, compte tenu des refus de certaines communes d'appartenir au parc national régional (PNR), le périmètre du parc n'est pas entachée d'une erreur manifeste d'appréciation au regard des objectifs poursuivis par la charte du parc et de l'ensemble des critères mentionnés à l'article R. 333-4 du code de l'environnement.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
