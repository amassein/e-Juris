<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000024815364</ID>
<ANCIEN_ID>JG_L_2011_11_000000343535</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/24/81/53/CETATEXT000024815364.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 18/11/2011, 343535</TITRE>
<DATE_DEC>2011-11-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>343535</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Philippe Martin</PRESIDENT>
<AVOCATS>SCP CELICE, BLANCPAIN, SOLTNER</AVOCATS>
<RAPPORTEUR>M. Jean Lessi</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Maud Vialettes</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 27 septembre 2010 au secrétariat du contentieux du Conseil d'Etat, présentée pour la CAISSE RSI PROFESSIONS LIBERALES PROVINCES, dont le siège est 44, boulevard de la Bastille à Paris (75578), représentée par son président et pour la CAISSE RSI PROFESSIONS LIBERALES ILE-DE-FRANCE, dont le siège est 22, rue Violet à Paris (75730), représentée par son président ; la CAISSE RSI PROFESSIONS LIBERALES PROVINCES et la CAISSE RSI PROFESSIONS LIBERALES ILE-DE-FRANCE demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2010-337 du 31 mars 2010 relatif au conseil de surveillance de l'agence régionale de santé, ainsi que la décision implicite par laquelle le ministre de la santé et des sports a rejeté leur recours gracieux formé le 27 mai 2010 contre ce décret ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement de la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 20 octobre 2011, présentée pour la CAISSE RSI PROFESSIONS LIBERALES PROVINCES et pour la CAISSE RSI PROFESSIONS LIBERALES ILE-DE-FRANCE ;<br/>
<br/>
              Vu le code de la santé publique ;<br/>
<br/>
              Vu le code de la sécurité sociale ;<br/>
<br/>
              Vu l'ordonnance n° 96-344 du 24 avril 1996 ;<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean Lessi, Auditeur,  <br/>
<br/>
              - les observations de la SCP Célice, Blancpain, Soltner, avocat de la CAISSE RSI PROFESSIONS LIBERALES PROVINCES et de la CAISSE RSI PROFESSIONS LIBERALES ILE-DE-FRANCE, <br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Célice, Blancpain, Soltner, avocat de la CAISSE RSI PROFESSIONS LIBERALES PROVINCES et de la CAISSE RSI PROFESSIONS LIBERALES ILE-DE-FRANCE ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant qu'en vertu de l'article L. 1432-1 introduit dans le code de la santé publique par l'article 118 de la loi du 21 juillet 2009 portant réforme de l'hôpital et relative aux patients, à la santé et aux territoires, les agences régionales de santé sont dotées d'un conseil de surveillance et dirigées par un directeur général ; que l'article L. 1432-3 du code de la santé publique fixe des règles relatives à la composition et au fonctionnement des conseils de surveillance des agences régionales de santé et prévoit, en particulier, que : " I. Le conseil de surveillance de l'agence régionale de santé est composé : (...) / 2° De membres des conseils et conseils d'administration des organismes locaux d'assurance maladie de son ressort dont la caisse nationale désigne les membres du conseil de l'Union nationale des caisses d'assurance maladie. (...) / III. Les modalités d'application du présent article sont fixées par décret " ; que, pour l'application de ces dispositions, est intervenu le décret n° 2010-337 du 31 mars 2010 relatif au conseil de surveillance de l'agence régionale de santé, dont la CAISSE RSI PROFESSIONS LIBERALES PROVINCES et la CAISSE RSI PROFESSIONS LIBERALES ILE-DE-FRANCE demandent l'annulation pour excès de pouvoir ;<br/>
<br/>
              Considérant qu'en vertu de l'article L. 122-1 du code de la sécurité sociale dans sa rédaction issue de l'ordonnance du 24 avril 1996 portant mesures relatives à l'organisation de la sécurité sociale, le directeur général ou le directeur de tout organisme de sécurité sociale " représente l'organisme en justice et dans tous les actes de la vie civile " ; que l'article R. 121-2 du même code dispose que : " Sous réserve des dispositions de l'article L. 122--1, les organismes sont représentés de plein droit en justice et dans tous les actes de la vie civile par leur président qui peut déléguer ses pouvoirs au directeur par mandat spécial ou général. / Les dispositions du présent article sont applicables à tous organismes à l'exception de ceux ayant le caractère d'établissement public, des organismes du régime social des indépendants, des caisses mutuelles d'assurance maladie et d'assurance vieillesse des cultes et de la caisse des Français de l'étranger " ; que s'il résulte de l'article L. 124-6 du code de la sécurité sociale que des décrets en Conseil d'Etat peuvent, pour certains régimes et en tant que de besoin, apporter les adaptations nécessaires aux dispositions de certains articles, notamment de l'article L. 122-1, aucune disposition législative ou réglementaire ne prévoit des modalités spécifiques de représentation des caisses de base du régime social des indépendants ;<br/>
<br/>
              Considérant que la requête de la CAISSE RSI PROFESSIONS LIBERALES PROVINCES et de la CAISSE RSI PROFESSIONS LIBERALES ILE-DE-FRANCE a été présentée par leurs présidents respectifs ; qu'il résulte de ce qui a été dit ci-dessus que ces autorités n'avaient pas qualité pour introduire une requête devant le Conseil d'Etat au nom de ces deux organismes, alors même qu'ils y auraient été régulièrement autorisés par une délibération de leurs conseils d'administration ; que, par suite, la requête est irrecevable et doit être rejetée, y compris ses conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la CAISSE RSI PROFESSIONS LIBERALES PROVINCES et de la CAISSE RSI PROFESSIONS LIBERALES ILE-DE-FRANCE est rejetée.<br/>
Article 2 : La présente décision sera notifiée à la CAISSE RSI PROFESSIONS LIBERALES PROVINCES, à la CAISSE RSI PROFESSIONS LIBERALES ILE-DE-FRANCE, au Premier ministre et au ministre du travail, de l'emploi et de la santé.<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-01-05-005 PROCÉDURE. INTRODUCTION DE L'INSTANCE. QUALITÉ POUR AGIR. REPRÉSENTATION DES PERSONNES MORALES. - CAISSES DE BASE DU RÉGIME SOCIAL DES INDÉPENDANTS - POSSIBILITÉ D'AUTORISER LEUR REPRÉSENTATION PAR LEUR PRÉSIDENT - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">62-05 SÉCURITÉ SOCIALE. CONTENTIEUX ET RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. - REPRÉSENTATION EN JUSTICE DES CAISSES DE BASE DU RÉGIME SOCIAL DES INDÉPENDANTS - POSSIBILITÉ D'AUTORISER LEUR REPRÉSENTATION PAR LEUR PRÉSIDENT - ABSENCE.
</SCT>
<ANA ID="9A"> 54-01-05-005 En vertu de l'article L. 122-1 du code de la sécurité sociale, tout organisme de sécurité sociale est représenté en justice par son directeur général ou son directeur. Par suite et en l'absence de toute disposition prévoyant les modalités spécifiques de représentation des caisses de base du régime social des indépendants, celles-ci ne peuvent valablement être représentées en justice par leur président, fut-il régulièrement autorisé à agir en leur nom par une délibération de leur conseil d'administration.</ANA>
<ANA ID="9B"> 62-05 En vertu de l'article L. 122-1 du code de la sécurité sociale, tout organisme de sécurité sociale est représenté en justice par son directeur général ou son directeur. Par suite et en l'absence de toute disposition prévoyant les modalités spécifiques de représentation des caisses de base du régime social des indépendants, celles-ci ne peuvent valablement être représentées en justice par leur président, fut-il régulièrement autorisé à agir en leur nom par une délibération de leur conseil d'administration.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
