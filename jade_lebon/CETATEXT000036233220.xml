<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036233220</ID>
<ANCIEN_ID>JG_L_2017_12_000000413193</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/23/32/CETATEXT000036233220.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 15/12/2017, 413193</TITRE>
<DATE_DEC>2017-12-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>413193</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>LE PRADO ; SCP DELVOLVE ET TRICHET</AVOCATS>
<RAPPORTEUR>M. François Lelièvre</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier Henrard</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:413193.20171215</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Twin Jet a demandé au juge des référés du tribunal administratif de Rennes, sur le fondement de l'article L. 551-1 du code de justice administrative, d'annuler la procédure de passation, par le syndicat mixte de l'aéroport de Lannion - Côte de granit, du contrat d'exploitation de la liaison aérienne entre les aéroports de Lannion et de Paris-Orly. <br/>
<br/>
              Par une ordonnance n° 1702973 du 24 juillet 2017, le juge des référés a fait droit à cette demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 8 et 22 août 2017 au secrétariat du contentieux du Conseil d'Etat, le syndicat mixte de l'aéroport de Lannion - Côte de granit demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) réglant l'affaire au titre de la procédure de référé, de rejeter la demande de la société Twin Jet ;<br/>
<br/>
              3°) de mettre à la charge de la société Twin Jet la somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le règlement (CE) n° 1008/2008 du Parlement européen et du Conseil du 24 septembre 2008 ;<br/>
              - le code de l'aviation civile ;<br/>
              - le code général des collectivités territoriales ;<br/>
              - le code des transports ;<br/>
              - l'ordonnance n° 2016-65 du 29 janvier 2016 ;<br/>
              - le décret n° 2005-473 du 16 mai 2005 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. François Lelièvre, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Olivier Henrard, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Delvolvé et Trichet, avocat du syndicat mixte de l'aéroport de Lannion - Côte de granit et à Me Le Prado, avocat de la société Twin Jet.<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des énonciations de l'ordonnance attaquée que le syndicat mixte de l'aéroport de Lannion - Côte de granit a lancé, sur le fondement du règlement (CE) du 24 septembre 2008 établissant des règles communes pour l'exploitation de services aériens dans la Communauté et du décret du 16 mai 2005 relatif aux règles d'attribution par l'Etat de compensations financières aux transporteurs aériens et aux exploitants d'aéroports pour leurs missions relatives au sauvetage et à la lutte contre les incendies d'aéronefs, à la sûreté, à la lutte contre le péril aviaire et aux mesures effectuées dans le cadre des contrôles environnementaux, une procédure de passation en vue de la conclusion d'une convention entre l'Etat, le syndicat mixte et le transporteur aérien retenu ayant pour objet l'exploitation, en exclusivité, de la liaison aérienne entre Lannion et Paris (Orly) et comportant une compensation versée par l'État en contrepartie du respect des obligations de service public grevant cette ligne ; que, par l'ordonnance attaquée, le juge du référé précontractuel du tribunal administratif de Rennes, saisi par la société Twin Jet et statuant en application de l'article L. 551-1 du code de justice administrative, a annulé cette procédure ;<br/>
<br/>
              Sur la compétence du juge du référé précontractuel :<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 551-1 du code de justice administrative : " Le président du tribunal administratif, ou le magistrat qu'il délègue, peut être saisi en cas de manquement aux obligations de publicité et de mise en concurrence auxquelles est soumise la passation par les pouvoirs adjudicateurs de contrats administratifs ayant pour objet l'exécution de travaux, la livraison de fournitures ou la prestation de services, avec une contrepartie économique constituée par un prix ou un droit d'exploitation, ou la délégation d'un service public (...) " ; <br/>
<br/>
              3. Considérant, d'une part, qu'aux termes de l'article L. 1411-1 du code général des collectivités territoriales : " Une délégation de service public est un contrat de concession au sens de l'ordonnance n° 2016-65 du 29 janvier 2016 relative aux contrats de concession (...) " ; qu'aux termes de l'article 5 de l'ordonnance du 29 janvier 2016 relative aux contrats de concession : " Les contrats de concession sont les contrats conclus par écrit, par lesquels une ou plusieurs autorités concédantes soumises à la présente ordonnance confient l'exécution de travaux ou la gestion d'un service à un ou plusieurs opérateurs économiques, à qui est transféré un risque lié à l'exploitation de l'ouvrage ou du service, en contrepartie soit du droit d'exploiter l'ouvrage ou le service qui fait l'objet du contrat, soit de ce droit assorti d'un prix " ; <br/>
<br/>
              4. Considérant, d'autre part, qu'aux termes de l'article 16 du règlement (CE) du 24 septembre 2008 établissant des règles communes pour l'exploitation de services aériens dans la Communauté : " 1. Un Etat membre peut (...) après en avoir informé la Commission (...) imposer une obligation de service public au titre de services aériens réguliers (...) sur une liaison à faible trafic à destination d'un aéroport situé sur son territoire, si cette liaison est considérée comme vitale pour le développement économique et social de la région desservie par l'aéroport (...) / 9. (...) si aucun transporteur aérien communautaire n'a commencé ou ne peut démontrer qu'il est sur le point de commencer des services aériens réguliers durables sur une liaison, conformément à l'obligation de service public qui a été imposée sur cette liaison, l'Etat membre concerné peut limiter l'accès des services aériens réguliers sur cette liaison à un seul transporteur aérien communautaire pour une période maximale de quatre ans, à l'issue de laquelle la situation est réexaminée (...) / 10. Le droit d'exploiter les services visés au paragraphe 9 est concédé après appel d'offres conformément à l'article 17, soit pour une seule liaison, soit, dans les cas où des raisons d'efficacité opérationnelle le justifient, pour un groupe de liaisons, à tout transporteur aérien communautaire autorisé à exploiter de tels services aériens (...) " ;<br/>
<br/>
              5. Considérant que, conformément aux dispositions du règlement (CE) du 24 septembre 2008, les Etats membres peuvent concéder par contrat, après appel d'offres, l'exploitation de liaisons aériennes non rentables à un prestataire choisi après mise en concurrence et tenu à des obligations de service public ; que ces contrats, dès lors qu'ils répondent aux critères posés à l'article 5 de l'ordonnance du 29 janvier 2016, sont des contrats de concession au sens de cette ordonnance, alors même qu'en vertu de son article 13, les contrats de concession de service de transport aérien ne sont pas soumis aux règles qu'elle fixe ; que de tels contrats, lorsqu'ils sont conclus par des personnes morales de droit public relevant du code général des collectivités territoriales, sont qualifiés, par les dispositions de l'article L. 1411-1 de ce code, de  contrats de délégation de service public et sont soumis aux règles posées à ce titre par ce code ; <br/>
<br/>
              6. Considérant qu'il ressort des pièces du dossier soumis au juge des référés que la convention litigieuse confie à son cocontractant l'exploitation, à ses risques, d'une ligne aérienne dans le respect d'obligations de service public ; que, par suite, le syndicat mixte de l'aéroport de Lannion - Côte de granit n'est pas fondé à soutenir que le juge du référé précontractuel du tribunal administratif de Rennes aurait commis une erreur de droit en qualifiant la convention litigieuse, qui remplit les critères précités de l'article 5 de l'ordonnance du 29 janvier 2016, de délégation de service public et en en déduisant que la contestation de sa passation relevait de sa compétence ;<br/>
<br/>
              7. Considérant que la circonstance, invoquée par le syndicat mixte de l'aéroport de Lannion - Côte de granit, que les contrats de concession de service de transport aérien soumis au règlement (CE) du 24 septembre 2008 sont susceptibles, le cas échéant, de donner lieu au versement d'une aide d'Etat et qu'il sont conclus à titre temporaire, est sans incidence sur cette qualification et cette compétence du juge des référés ; qu'enfin, contrairement à ce que soutient le syndicat mixte et en tout état de cause, l'article 8 du décret du 16 mai 2005 relatif aux règles d'attribution par l'Etat de compensations financières aux transporteurs aériens, qui qualifie explicitement de tels contrats de délégations de service public lorsque l'Etat y est partie, est demeuré applicable après l'entrée en vigueur du règlement (CE) du 24 septembre 2008 ; <br/>
<br/>
              Sur l'annulation de la procédure de passation :<br/>
<br/>
              8. Considérant, ainsi qu'il a été dit ci-dessus, que le contrat conclu par le syndicat mixte de l'aéroport de Lannion - Côte de granit pour l'exploitation, en exclusivité, de la liaison aérienne entre Lannion et Paris (Orly), est un contrat de concession de service de transport aérien conclu sur le fondement du règlement (CE) du 24 septembre 2008, répondant à la qualification de délégation de service public en application des dispositions combinées de l'article 5 de l'ordonnance du 29 janvier 2016 et de l'article L. 1411-1 du code général des collectivités territoriales ; que ce contrat est par suite soumis aux principes de liberté d'accès à la commande publique, d'égalité de traitement des candidats et de transparence des procédures, principes généraux du droit de la commande publique ; que, pour assurer le respect de ces principes, la personne publique doit apporter aux candidats à l'attribution du contrat, avant le dépôt de leurs offres, ainsi que le prévoit d'ailleurs le paragraphe 5 de l'article 17 du règlement (CE) du 24 septembre 2008, l'ensemble des informations et/ou documents pertinents, lesquels doivent nécessairement inclure une information sur les critères de sélection des offres ; qu'au titre de cette information, le délégant ne peut se borner à rappeler les dispositions du paragraphe 7 de l'article 17 de ce règlement qui prévoient que " la sélection parmi les offres présentées est opérée le plus rapidement possible compte tenu de l'adéquation du service et notamment des prix et des conditions qui peuvent être proposés aux usagers ainsi que du coût de la compensation requise, le cas échéant, du ou des Etats membres concernés " ;<br/>
<br/>
              9. Considérant qu'en relevant que ni l'avis de publicité, ni le dossier de la consultation adressé aux candidats admis à présenter une offre ne comportaient, au-delà du rappel des dispositions du paragraphe 7 de l'article 17, d'information suffisamment précise sur les critères de choix du délégataire et que ces critères n'avaient pas davantage été mentionnés au cours de la phase de négociation et ne pouvaient pas clairement se déduire des questions posées par le syndicat mixte, le juge du référé précontractuel s'est livré à une appréciation souveraine des faits exempte de dénaturation ;<br/>
<br/>
              10. Considérant qu'il résulte de ce qui précède que le syndicat mixte de l'aéroport Lannion - Côte de granit n'est pas fondé à demander l'annulation de l'ordonnance attaquée ;<br/>
<br/>
              11. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mis à la charge de la société Twin Jet, qui n'est pas la partie perdante, le versement des sommes que demande, à ce titre, le syndicat mixte de l'aéroport de Lannion - Côte de granit ; qu'en revanche, il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du syndicat mixte de l'aéroport de Lannion - Côte de granit le versement d'une somme de 3 000 euros à la société Twin Jet au titre des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi du syndicat mixte de l'aéroport de Lannion - Côte de granit est rejeté.<br/>
Article 2 : Le syndicat mixte de l'aéroport de Lannion - Côte de granit versera à la société Twin Jet une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée au syndicat mixte de l'aéroport de Lannion - Côte de granit et à la société Twin Jet.<br/>
Copie en sera adressée à la société Chalair Aviation.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-01-03-03 MARCHÉS ET CONTRATS ADMINISTRATIFS. NOTION DE CONTRAT ADMINISTRATIF. DIVERSES SORTES DE CONTRATS. DÉLÉGATIONS DE SERVICE PUBLIC. - CONTRATS DE CONCESSION DE SERVICE DE TRANSPORT AÉRIEN (RÈGLEMENT (CE) N° 1008/2008) CONCLUS PAR UNE PERSONNE PUBLIQUE SOUMISE AU CGCT - CONTRATS DE CONCESSION AU SENS DE L'ORDONNANCE DU 29 JANVIER 2016 - EXISTENCE, QUAND BIEN MÊME ILS SONT EXCLUS DES RÈGLES QU'ELLE FIXE - DÉLÉGATION DE SERVICE PUBLIC (ART. L. 1411-1 DU CGCT) - INCLUSION - CONSÉQUENCE - APPLICATION DES PRINCIPES GÉNÉRAUX DE LA COMMANDE PUBLIQUE - CONSÉQUENCE- OBLIGATION D'INFORMER LES CANDIDATS, AVANT LE DÉPÔT DE LEURS OFFRES, SUR LES CRITÈRES DE SÉLECTION DES OFFRES [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">39-02-02-01 MARCHÉS ET CONTRATS ADMINISTRATIFS. FORMATION DES CONTRATS ET MARCHÉS. MODE DE PASSATION DES CONTRATS. DÉLÉGATIONS DE SERVICE PUBLIC. - CONTRATS DE CONCESSION DE SERVICE DE TRANSPORT AÉRIEN (RÈGLEMENT (CE) N° 1008/2008) CONCLUS PAR UNE PERSONNE PUBLIQUE SOUMISE AU CGCT - CONTRATS DE CONCESSION AU SENS DE L'ORDONNANCE DU 29 JANVIER 2016 - EXISTENCE, QUAND BIEN MÊME ILS SONT EXCLUS DES RÈGLES QU'ELLE FIXE - DÉLÉGATION DE SERVICE PUBLIC (ART. L. 1411-1 DU CGCT) - INCLUSION - CONSÉQUENCE - APPLICATION DES PRINCIPES GÉNÉRAUX DE LA COMMANDE PUBLIQUE - CONSÉQUENCE - OBLIGATION D'INFORMER LES CANDIDATS, AVANT LE DÉPÔT DE LEURS OFFRES, SUR LES CRITÈRES DE SÉLECTION DES OFFRES [RJ1].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">65-03-02 TRANSPORTS. TRANSPORTS AÉRIENS. EXPLOITATION DES LIGNES AÉRIENNES. - CONTRATS DE CONCESSION DE SERVICE DE TRANSPORT AÉRIEN (RÈGLEMENT (CE) N° 1008/2008) CONCLUS PAR UNE PERSONNE PUBLIQUE SOUMISE AU CGCT - CONTRATS DE CONCESSION AU SENS DE L'ORDONNANCE DU 29 JANVIER 2016 - EXISTENCE, QUAND BIEN MÊME ILS SONT EXCLUS DES RÈGLES QU'ELLE FIXE - DÉLÉGATION DE SERVICE PUBLIC (ART. L. 1411-1 DU CGCT) - INCLUSION - CONSÉQUENCE - APPLICATION DES PRINCIPES GÉNÉRAUX DE LA COMMANDE PUBLIQUE - CONSÉQUENCE - OBLIGATION D'INFORMER LES CANDIDATS, AVANT LE DÉPÔT DE LEURS OFFRES, SUR LES CRITÈRES DE SÉLECTION DES OFFRES [RJ1].
</SCT>
<ANA ID="9A"> 39-01-03-03 Conformément au règlement (CE) n° 1008/2008 du 24 septembre 2008, les Etats membres peuvent concéder par contrat, après appel d'offres, l'exploitation de liaisons aériennes non rentables à un prestataire choisi après mise en concurrence et tenu à des obligations de service public. Ces contrats, dès lors qu'ils répondent aux critères posés à l'article 5 de l'ordonnance n° 2016-65 du 29 janvier 2016, sont des contrats de concession au sens de cette ordonnance, alors même qu'en vertu de son article 13, les contrats de concession de service de transport aérien ne sont pas soumis aux règles qu'elle fixe. De tels contrats, lorsqu'ils sont conclus par des personnes morales de droit public relevant du code général des collectivités territoriales (CGCT), sont qualifiés, par les dispositions de l'article L. 1411-1 de ce code, de contrats de délégation de service public et sont soumis aux règles posées à ce titre par ce code.,,,Ces contrats sont par suite soumis aux principes de liberté d'accès à la commande publique, d'égalité de traitement des candidats et de transparence des procédures, principes généraux du droit de la commande publique. Pour assurer le respect de ces principes, la personne publique doit apporter aux candidats à l'attribution d'un tel contrat, avant le dépôt de leurs offres, ainsi que le prévoit d'ailleurs le paragraphe 5 de l'article 17 du règlement (CE) du 24 septembre 2008, l'ensemble des informations et/ou documents pertinents, lesquels doivent nécessairement inclure une information sur les critères de sélection des offres.</ANA>
<ANA ID="9B"> 39-02-02-01 Conformément au règlement (CE) n° 1008/2008 du 24 septembre 2008, les Etats membres peuvent concéder par contrat, après appel d'offres, l'exploitation de liaisons aériennes non rentables à un prestataire choisi après mise en concurrence et tenu à des obligations de service public. Ces contrats, dès lors qu'ils répondent aux critères posés à l'article 5 de l'ordonnance n° 2016-65 du 29 janvier 2016, sont des contrats de concession au sens de cette ordonnance, alors même qu'en vertu de son article 13, les contrats de concession de service de transport aérien ne sont pas soumis aux règles qu'elle fixe. De tels contrats, lorsqu'ils sont conclus par des personnes morales de droit public relevant du code général des collectivités territoriales (CGCT), sont qualifiés, par les dispositions de l'article L. 1411-1 de ce code, de contrats de délégation de service public et sont soumis aux règles posées à ce titre par ce code.,,,Ces contrats sont par suite soumis aux principes de liberté d'accès à la commande publique, d'égalité de traitement des candidats et de transparence des procédures, principes généraux du droit de la commande publique. Pour assurer le respect de ces principes, la personne publique doit apporter aux candidats à l'attribution d'un tel contrat, avant le dépôt de leurs offres, ainsi que le prévoit d'ailleurs le paragraphe 5 de l'article 17 du règlement (CE) du 24 septembre 2008, l'ensemble des informations et/ou documents pertinents, lesquels doivent nécessairement inclure une information sur les critères de sélection des offres.</ANA>
<ANA ID="9C"> 65-03-02 Conformément au règlement (CE) n° 1008/2008 du 24 septembre 2008, les Etats membres peuvent concéder par contrat, après appel d'offres, l'exploitation de liaisons aériennes non rentables à un prestataire choisi après mise en concurrence et tenu à des obligations de service public. Ces contrats, dès lors qu'ils répondent aux critères posés à l'article 5 de l'ordonnance n° 2016-65 du 29 janvier 2016, sont des contrats de concession au sens de cette ordonnance, alors même qu'en vertu de son article 13, les contrats de concession de service de transport aérien ne sont pas soumis aux règles qu'elle fixe. De tels contrats, lorsqu'ils sont conclus par des personnes morales de droit public relevant du code général des collectivités territoriales (CGCT), sont qualifiés, par les dispositions de l'article L. 1411-1 de ce code, de contrats de délégation de service public et sont soumis aux règles posées à ce titre par ce code.,,,Ces contrats sont par suite soumis aux principes de liberté d'accès à la commande publique, d'égalité de traitement des candidats et de transparence des procédures, principes généraux du droit de la commande publique. Pour assurer le respect de ces principes, la personne publique doit apporter aux candidats à l'attribution d'un tel contrat, avant le dépôt de leurs offres, ainsi que le prévoit d'ailleurs le paragraphe 5 de l'article 17 du règlement (CE) du 24 septembre 2008, l'ensemble des informations et/ou documents pertinents, lesquels doivent nécessairement inclure une information sur les critères de sélection des offres.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE 23 décembre 2009, Etablissement public du musée et domaine national de Versailles, n° 328827, p. 502 ; CE, 30 juillet 2014, Société Lyonnaise des eaux France, n° 396044, T. p. 739.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
