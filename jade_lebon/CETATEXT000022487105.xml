<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000022487105</ID>
<ANCIEN_ID>JG_L_2010_07_000000338860</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/22/48/71/CETATEXT000022487105.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème et 9ème sous-sections réunies, 16/07/2010, 338860</TITRE>
<DATE_DEC>2010-07-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>338860</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème et 9ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Stirn</PRESIDENT>
<AVOCATS>SCP GASCHIGNARD ; SCP PEIGNOT, GARREAU</AVOCATS>
<RAPPORTEUR>M. Olivier  Henrard</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Hédary Delphine</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 22 avril et 11 mai 2010 au secrétariat du contentieux du Conseil d'Etat, présentés pour la SARL FRANCIMO, dont le siège social est 79, rue du Président-Wilson à Levallois-Perret (92300) ; la SARL FRANCIMO demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance du 6 avril 2010, par laquelle le juge des référés du tribunal administratif de Nice a rejeté sa demande tendant, d'une part, à la suspension du rejet implicite opposé par le maire d'Antibes à sa demande de confirmation de son permis de construire un bâtiment d'habitation situé 17, chemin des îles, à Antibes et de l'arrêté en date du 1er janvier 2010 par lequel le maire a sursis à statuer sur cette demande, d'autre part, à ce qu'il soit enjoint au maire de lui délivrer le permis litigieux ou à défaut d'instruire sa demande, dans un délai de quinze jours sous astreinte de 1 000 euros par jour de retard à compter de l'expiration de ce délai ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de la commune d'Antibes la somme de 4 000 euros en application de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'urbanisme ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Henrard, Maître des Requêtes,<br/>
<br/>
              - les observations de la SCP Gaschignard, avocat de la SARL FRANCIMO et de la SCP Peignot, Garreau, avocat de la commune d'Antibes,<br/>
<br/>
              - les conclusions de Mme Delphine Hédary, Rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Gaschignard, avocat de la SARL FRANCIMO et à la SCP Peignot, Garreau, avocat de la commune d'Antibes ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant, en premier lieu, que si les dispositions de l'article L. 600-3 du code de l'urbanisme, lorsqu'une personne autre que l'Etat, la commune ou l'établissement public de coopération intercommunale défère à un tribunal administratif une décision relative à un permis de construire ou d'aménager et assortit son recours d'une demande de suspension, impartissent au juge des référés de statuer dans un délai d'un mois sur cette demande, elles n'ont pour effet, ni de rendre irrégulier un jugement prononcé après l'expiration de ce délai, ni de faire obstacle à ce que le juge statue régulièrement au vu d'un mémoire produit au-delà de celui-ci ; qu'il suit de là que le moyen tiré de ce que l'ordonnance attaquée méconnaîtrait les dispositions de l'article L. 600-3, dès lors que le juge des référés a statué au-delà du délai d'un mois qui lui était imparti par les dispositions de cet article et a accepté de prendre en considération un mémoire produit par la commune au-delà de ce même délai, doit être écarté ;<br/>
<br/>
              Considérant, en second lieu, que selon l'article L. 111-7 du code de l'urbanisme :  Il peut être sursis à statuer sur toute demande d'autorisation concernant des travaux, constructions ou installations dans les cas prévus par les articles (...) L. 123-6 (dernier alinéa) (...) du présent code (...)  ; que le dernier alinéa de l'article L. 123-6 du même code prévoit que :  A compter de la publication de la délibération prescrivant l'élaboration d'un plan local d'urbanisme, l'autorité compétente peut décider de surseoir à statuer, dans les conditions et délai prévus à l'article L. 111-8, sur les demandes d'autorisation concernant des constructions, installations ou opérations qui seraient de nature à compromettre ou à rendre plus onéreuse l'exécution du futur plan.  ; que l'article L. 600-2 du même code dispose que :  Lorsqu'un refus opposé à une demande d'autorisation d'occuper ou d'utiliser le sol ou l'opposition à une déclaration de travaux régies par le présent code a fait l'objet d'une annulation juridictionnelle, la demande d'autorisation ou la déclaration confirmée par l'intéressé ne peut faire l'objet d'un nouveau refus ou être assortie de prescriptions spéciales sur le fondement de dispositions d'urbanisme intervenues postérieurement à la date d'intervention de la décision annulée sous réserve que l'annulation soit devenue définitive et que la confirmation de la demande ou de la déclaration soit effectuée dans les six mois suivant la notification de l'annulation au pétitionnaire.  ;<br/>
<br/>
              Considérant qu'il résulte du rapprochement des dispositions rappelées plus haut que si l'article L. 600-2 du code de l'urbanisme ne fait pas obstacle, par lui-même, à ce que la demande de permis de construire confirmée par le pétitionnaire dans les conditions qu'il prévoit fasse l'objet du sursis à statuer prévu par l'article L. 111-7 du même code, le prononcé de ce sursis ne peut être fondé, dans une telle hypothèse, sur la circonstance que la réalisation du projet de construction litigieux serait de nature à compromettre ou à rendre plus onéreuse l'exécution d'un plan local d'urbanisme intervenu postérieurement à la date de la décision de refus annulée, dès lors que cette circonstance, qui repose sur l'anticipation de l'effet que les règles futures du plan local d'urbanisme auront sur l'autorisation demandée, ou celle-ci sur leur mise en oeuvre, ne pourrait motiver un nouveau refus ou l'édiction de prescriptions spéciales portant sur le permis demandé sans méconnaître les dispositions de l'article L. 600-2 ;<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis au juge des référés que le maire d'Antibes a décidé, par l'arrêté en date du 1er février 2010 dont la suspension était demandée, de surseoir à statuer sur la confirmation, par la SARL FRANCIMO, le 8 décembre 2009, de sa demande de permis de construire portant sur un immeuble d'habitation, formulée à la suite de l'annulation, par un jugement du 26 novembre 2009 du tribunal administratif de Nice devenu définitif, du refus qui avait été opposé, le 3 août 2006, par le maire à cette demande de permis ; qu'il ressort également du dossier que le plan local d'urbanisme de la commune d'Antibes, dont l'élaboration avait été prescrite par une délibération du conseil municipal en date du 20 décembre 2002, n'avait pas été adopté à la date du refus annulé du 3 août 2006 ; qu'il suit de là que si les dispositions citées de l'article L. 600-2 du code de l'urbanisme ne faisaient pas obstacle à ce que le maire d'Antibes puisse opposer le sursis à statuer à la demande de permis de construire confirmée par le pétitionnaire, elles s'opposaient en revanche à ce que ce sursis puisse être fondé sur la circonstance que la réalisation du projet de construction litigieux serait de nature à compromettre ou à rendre plus onéreuse l'exécution du futur plan local d'urbanisme ; qu'ainsi, le juge des référés ne pouvait, sans erreur de droit, considérer que n'était pas de nature à créer un doute sérieux le moyen tiré de ce que le maire ne pouvait, comme il l'a fait, légalement décider de surseoir à statuer sur la demande de la SARL FRANCIMO au motif de la contrariété du permis de construire en cause avec les dispositions du futur plan local d'urbanisme ; qu'ainsi, la société requérante est fondée à demander l'annulation de l'ordonnance attaquée ;<br/>
<br/>
              Sur les conclusions aux fins d'application de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de la SARL FRANCIMO, qui n'est pas la partie perdante dans la présente instance, le versement de la somme que demande la commune d'Antibes au titre des frais exposés par elle et non compris dans les dépens ; qu'il n'y a pas lieu de mettre à la charge de cette commune la somme de 4 000 euros que demande la SARL FRANCIMO au même titre ;<br/>
<br/>
<br/>
<br/>				D E C I D E  :<br/>
              				--------------<br/>
<br/>
Article 1er : L'ordonnance du 6 avril 2010 du juge des référés du tribunal administratif de Nice est annulée.<br/>
<br/>
Article 2 : L'affaire est renvoyée au juge des référés du tribunal administratif de Nice.<br/>
Article 3 : Les conclusions aux fins d'application de l'article L. 761-1 du code de justice administrative présentées devant le Conseil d'Etat par la SARL FRANCIMO et par la commune d'Antibes sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à la SARL FRANCIMO et au maire d'Antibes.<br/>
Copie en sera adressée pour information au ministre d'Etat, ministre de l'écologie, de l'énergie, du développement durable et de la mer, en charge des technologies vertes et des négociations sur le climat.<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">68-03-025-01-01 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PERMIS DE CONSTRUIRE. NATURE DE LA DÉCISION. SURSIS À STATUER. MOTIFS. - PERMIS DE CONSTRUIRE ANNULÉ DÉFINITIVEMENT - MÉCANISME DE CRISTALLISATION DU DROIT EN VIGUEUR À LA DATE DU PERMIS ANNULÉ (ART. L. 600-2 DU C. URB.) - CONSÉQUENCE - IMPOSSIBILITÉ POUR L'ADMINISTRATION DE SURSEOIR À STATUER SUR LA CONFIRMATION DE LA DEMANDE DE PERMIS AU MOTIF QUE LA CONSTRUCTION SERAIT DE NATURE À COMPROMETTRE OU À RENDRE PLUS ONÉREUSE L'EXÉCUTION D'UN PLU INTERVENU POSTÉRIEUREMENT À CETTE DATE (ART. L. 111-7 ET L. 123-6 DU MÊME CODE).
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">68-06-02-01 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. PROCÉDURE D'URGENCE. RÉFÉRÉ. - DÉLAI IMPARTI AU JUGE DES RÉFÉRÉS DU TRIBUNAL ADMINISTRATIF POUR STATUER SUR UNE DEMANDE DE SUSPENSION DE L'EXÉCUTION D'UN PERMIS DE CONSTRUIRE OU D'AMÉNAGER (ART. L. 600-3 DU C. URB.) - NULLITÉ DU JUGEMENT PRONONCÉ APRÈS L'EXPIRATION DE CE DÉLAI - ABSENCE [RJ1] - POSSIBILITÉ DE TENIR COMPTE D'UN MÉMOIRE PRODUIT APRÈS CE DÉLAI - EXISTENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">68-06-05 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. EFFETS DES ANNULATIONS. - MÉCANISME DE CRISTALLISATION DU DROIT EN VIGUEUR À LA DATE DU PERMIS ANNULÉ DÉFINITIVEMENT (ART. L. 600-2 DU C. URB.) - CONSÉQUENCE - IMPOSSIBILITÉ POUR L'ADMINISTRATION DE SURSEOIR À STATUER SUR LA CONFIRMATION DE LA DEMANDE DE PERMIS AU MOTIF QUE LA CONSTRUCTION SERAIT DE NATURE À COMPROMETTRE OU À RENDRE PLUS ONÉREUSE L'EXÉCUTION D'UN PLU INTERVENU POSTÉRIEUREMENT À CETTE DATE (ART. L. 111-7 ET L. 123-6 DU MÊME CODE).
</SCT>
<ANA ID="9A"> 68-03-025-01-01 L'article L. 600-2 du code de l'urbanisme (c. urb.) prévoit que, lorsqu'un permis de construire a fait l'objet d'une annulation contentieuse définitive et que le pétitionnaire confirme sa demande de permis dans les six mois suivant la notification de l'annulation, cette demande ne peut être rejetée sur le fondement de dispositions d'urbanisme intervenues postérieurement à la date du permis annulé. Il en résulte que l'administration ne peut légalement surseoir à statuer sur la confirmation de la demande par le pétitionnaire en se fondant sur ce que la réalisation du projet de construction serait de nature à compromettre ou à rendre plus onéreuse l'exécution d'un plan local d'urbanisme (PLU) intervenu postérieurement à cette date.</ANA>
<ANA ID="9B"> 68-06-02-01 Le délai d'un mois imparti par l'article L. 600-3 du code de l'urbanisme (c. urb) au juge des référés du tribunal administratif pour statuer sur une demande de suspension de l'exécution d'un permis de construire ou d'aménager n'est pas prescrit à peine d'irrégularité de son jugement ou de son ordonnance. Le juge peut et, le cas échéant, doit tenir compte d'un mémoire produit après l'expiration de ce délai.</ANA>
<ANA ID="9C"> 68-06-05 L'article L. 600-2 du code de l'urbanisme (c. urb.) prévoit que, lorsqu'un permis de construire a fait l'objet d'une annulation contentieuse définitive et que le pétitionnaire confirme sa demande de permis dans les six mois suivant la notification de l'annulation, cette demande ne peut être rejetée sur le fondement de dispositions d'urbanisme intervenues postérieurement à la date de ce permis. Il en résulte que l'administration ne peut légalement surseoir à statuer sur la confirmation de la demande par le pétitionnaire en se fondant sur ce que la réalisation du projet de construction serait de nature à compromettre ou à rendre plus onéreuse l'exécution d'un plan local d'urbanisme (PLU) intervenu postérieurement à cette date.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., s'agissant du sursis à exécution prévu par l'ancien article L. 421-9 du code de l'urbanisme, 22 avril 1988, Comité de sauvegarde du patrimoine du pays de Montpellier, n° 78871, T. p. 596.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
