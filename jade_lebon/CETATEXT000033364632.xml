<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033364632</ID>
<ANCIEN_ID>JG_L_2016_11_000000392482</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/36/46/CETATEXT000033364632.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 6ème chambres réunies, 09/11/2016, 392482</TITRE>
<DATE_DEC>2016-11-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>392482</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ ; SCP THOUIN-PALAT, BOUCARD</AVOCATS>
<RAPPORTEUR>Mme Florence Marguerite</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Jean Lessi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:392482.20161109</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Procédure contentieuse antérieure<br/>
<br/>
              Mme A...B...a demandé au tribunal administratif de Toulouse :<br/>
              - d'annuler les décisions des 7 et 13 décembre 2011, confirmées par le président du conseil général de la Haute-Garonne le 17 janvier 2012, par lesquelles le directeur de la caisse d'allocations familiales de la Haute-Garonne lui a réclamé, d'une part, un trop-perçu d'un montant total de 8 662,65 euros au titre du revenu de solidarité active et de l'allocation de logement sociale, perçus pour la période du 1er septembre 2010 au 31 août 2011 et, d'autre part, un trop-perçu d'un montant de 228,67 euros au titre de la prime exceptionnelle de fin d'année pour 2010 ; <br/>
              - de condamner la caisse d'allocations familiales de la Haute-Garonne à lui verser une somme de 1 000 euros à titre de dommages et intérêts.<br/>
<br/>
              Par un jugement n° 1200628 du 10 juin 2015, le tribunal administratif de Toulouse a : <br/>
              - à son article 1er, rejeté ses conclusions tendant à l'annulation de la décision du directeur de la caisse d'allocations familiales de la Haute-Garonne du 7 décembre 2011 en tant qu'elle lui réclame une somme au titre de l'allocation logement sociale, comme portées devant une juridiction incompétente pour en connaître ;<br/>
              - à son article 2, dit qu'il n'y avait pas lieu de statuer sur ses conclusions tendant à l'annulation de la décision du président du conseil général de la Haute-Garonne du 17 janvier 2012 en tant qu'elle rejette sa demande de remise gracieuse d'un indu d'allocation de revenu de solidarité active pour la période du 1er octobre 2010 au 31 août 2011 dans la limite de la différence entre le montant de cet indu et le montant du revenu de solidarité active dû à Mme B... au titre de cette même période ;<br/>
              - à son article 3, renvoyé Mme B...devant le département pour le calcul du revenu de solidarité active qui lui est dû à compter du 1er octobre 2010 ;<br/>
              - à son article 4, annulé la décision du directeur de la caisse d'allocations familiales du 13 décembre 2011 relative au trop-perçu de prime exceptionnelle de fin d'année ; <br/>
              - et à son article 5, rejeté le surplus de ses conclusions.<br/>
<br/>
              Procédure devant le Conseil d'Etat<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 10 août et 10 novembre 2015 au secrétariat du contentieux du Conseil d'Etat, le département de la Haute-Garonne demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler les articles 2, 3 et 4 de ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de Mme B...;<br/>
<br/>
              3°) de mettre à la charge de Mme B...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'action sociale et des familles ; <br/>
              - le code civil ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le décret n° 2010-1631 du 23 décembre 2010 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Florence Marguerite, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Jean Lessi, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat du département de la Haute-Garonne et à la SCP Thouin-Palat, Boucard, avocat de MmeB....<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que, par deux décisions des 7 et 13 décembre 2011, le directeur de la caisse d'allocations familiales de la Haute-Garonne a réclamé à Mme B...un trop-perçu d'un montant de 7 351,10 euros, au titre du revenu de solidarité active qui lui avait été servi pour la période du 1er septembre 2010 au 31 août 2011, et un trop-perçu d'un montant de 228,67 euros, au titre de l'aide exceptionnelle de fin d'année qui lui avait été versée pour 2010, au motif que les ressources de son foyer, incluant celles de son conjoint résidant en Algérie, excédaient le montant ouvrant droit au revenu de solidarité active. Par deux décisions du 17 janvier 2012, le président du conseil général de la Haute-Garonne a, d'une part, confirmé à Mme B...la fin de son droit au revenu de solidarité active à compter du 1er octobre 2010, rejetant ainsi le recours préalable obligatoire qu'elle avait formé contre la décision du 7 décembre 2011 du directeur de la caisse d'allocations familiales de la Haute-Garonne et, d'autre part, rejeté sa demande de remise gracieuse de l'indu mis à sa charge. Par un jugement du 10 juin 2015, le tribunal administratif de Toulouse a fait partiellement droit à la requête de MmeB..., en jugeant que les revenus de son conjoint devaient être pris en considération à hauteur non du chiffre d'affaires mais seulement du résultat net de son activité commerciale. Le département de la Haute-Garonne se pourvoit en cassation contre ce jugement en tant, tout d'abord, que Mme B...a été renvoyée devant lui pour le calcul du montant de l'allocation de revenu de solidarité active qui lui est due à compter du 1er octobre 2010, en tant, ensuite, que la décision du 13 décembre 2011 du directeur de la caisse d'allocations familiales a été annulée et en tant, enfin, qu'un non-lieu a été prononcé sur les conclusions en annulation de la décision du président du conseil général rejetant la demande de remise gracieuse de l'intéressée dans la limite de la différence entre le montant de l'indu réclamé et le montant du revenu de solidarité active qui lui est dû. Par un pourvoi incident, Mme B... demande, pour sa part, l'annulation du jugement du tribunal administratif de Toulouse en tant qu'il rejette le surplus de ses conclusions relatives à ses droits au revenu de solidarité active et à l'indu mis à sa charge, ainsi que ses conclusions indemnitaires dirigées contre la caisse d'allocations familiales de la Haute-Garonne. <br/>
<br/>
              Sur les conclusions du département de la Haute-Garonne dirigées contre l'article 4 du jugement attaqué :<br/>
<br/>
              2. Aux termes de l'article 1er du décret du 23 décembre 2010 relatif aux aides exceptionnelles de fin d'année attribuées à certains allocataires du revenu de solidarité active, du revenu minimum d'insertion et de l'allocation de parent isolé : " Une aide exceptionnelle est attribuée : / 1° Aux allocataires du revenu de solidarité active qui ont droit à cette allocation au titre du mois de novembre 2010 ou, à défaut, du mois de décembre 2010, sous réserve que le montant dû au titre de ces périodes ne soit pas nul et à condition que les ressources du foyer, appréciées selon les dispositions prises en vertu de l'article L. 262-3 du code de l'action sociale et des familles, n'excèdent pas le montant forfaitaire mentionné au 2° de l'article L. 262-2 du même code (...) ". Aux termes de l'article 3 du même décret : " Les aides exceptionnelles régies par le présent décret sont à la charge de l'Etat. Elles sont versées par l'organisme débiteur de chacune des prestations mentionnées à l'article 1er ". <br/>
<br/>
              3. Il résulte de ces dispositions que l'aide exceptionnelle d'un montant de 228,67 euros, dont le recouvrement fait l'objet de la décision du 13 décembre 2011 du directeur de la caisse d'allocations familiales de la Haute-Garonne, a été servie à Mme B...pour le compte de l'Etat et non du département de la Haute-Garonne. Par suite, ce département ne justifie pas d'un intérêt à agir pour contester l'annulation de cette décision prononcée par l'article 4 du jugement attaqué. Dès lors, les conclusions du département requérant dirigées contre cet article du jugement du tribunal administratif de Toulouse du 10 juin 2015 doivent être rejetées comme irrecevables. <br/>
<br/>
              Sur le surplus des conclusions du pourvoi du département de la Haute-Garonne : <br/>
<br/>
              4. Aux termes du premier alinéa de l'article L. 262-2 du code de l'action sociale et des familles : " Toute personne résidant en France de manière stable et effective, dont le foyer dispose de ressources inférieures à un montant forfaitaire, a droit au revenu de solidarité active dans les conditions définies au présent chapitre ". L'article L. 262-4 du même code prévoit que le bénéfice du revenu de solidarité active est notamment subordonné au respect par le bénéficiaire des conditions suivantes : " 2° Etre français ou titulaire, depuis au moins cinq ans, d'un titre de séjour autorisant à travailler  (...) ". L'article L. 262-5 du même code précise que : " Pour être pris en compte au titre des droits du bénéficiaire, le conjoint, concubin ou partenaire lié par un pacte civil de solidarité du bénéficiaire doit remplir les conditions mentionnées aux 2° (...) de l'article L. 262-4 ". Il résulte de ces dispositions que, pour être pris en compte au titre des droits du bénéficiaire, son conjoint, concubin ou partenaire lié par un pacte civil de solidarité doit résider en France de manière stable et effective et, lorsqu'il est de nationalité étrangère, justifier des conditions de séjour prévues par l'article L. 262-4 ou par l'article L. 262-6 s'agissant des ressortissants d'un Etat membre de l'Union européenne, d'un autre Etat partie à l'accord sur l'Espace économique européen ou de la Confédération suisse. Dès lors, le département de la Haute-Garonne est fondé à soutenir qu'en calculant les droits de Mme B... au revenu de solidarité active sur la base du montant dû à un couple avec trois enfants, alors qu'il ressortait des pièces du dossier et qu'il n'était pas contesté que son mari, de nationalité algérienne, résidait exclusivement en Algérie, le tribunal administratif a commis une erreur de droit.<br/>
<br/>
              5. Il résulte de ce qui précède que, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, le département de la Haute-Garonne est fondé à demander l'annulation des articles 2 et 3 du jugement du tribunal administratif de Toulouse.<br/>
<br/>
              Sur le pourvoi incident de MmeB... :<br/>
<br/>
              6. Aux termes du deuxième alinéa de l'article L. 262-3 du code de l'action sociale et des familles : " L'ensemble des ressources du foyer, y compris celles qui sont mentionnées à l'article L. 132-1, est pris en compte pour le calcul du revenu de solidarité active (...) ". En outre, aux termes des deuxième et troisième alinéas de l'article L. 262-10 du même code, le droit au revenu de solidarité active " est subordonné à la condition que le foyer fasse valoir ses droits : / 1° Aux créances d'aliments qui lui sont dues au titre des obligations instituées par les articles 203, 212, 214, 255, 342 et 371-2 du code civil (...) ". Les articles 203, 212 et 214 du code civil régissent les obligations des époux, notamment leur contribution aux charges du mariage, et l'article 371-2 de ce code régit la contribution des parents à l'entretien et à l'éducation des enfants communs. <br/>
<br/>
              7. Lorsque des époux sont séparés de fait, ils ne constituent plus un foyer au sens de l'article L. 262-2 du code de l'action sociale et des familles cité au point 4 et de l'article L. 262-3 du même code cité au point précédent. En conséquence, dès lors que la séparation de fait des époux est effective, les revenus du conjoint n'ont pas à être pris en compte dans le calcul des ressources du bénéficiaire. Seules les sommes que le conjoint verse au bénéficiaire ou, le cas échéant, les prestations en nature qu'il lui sert, au titre notamment de ses obligations alimentaires, peuvent être prises en compte dans le calcul des ressources de ce dernier. Dans le cas où aucune somme ne lui est versée ou aucune prestation en nature ne lui est servie, il appartient au bénéficiaire du revenu de solidarité active de justifier avoir fait valoir ses droits aux créances d'aliments, dans les conditions prévues aux articles R. 262-46 et suivants du code de l'action sociale et des familles. <br/>
<br/>
              8. Par suite, le tribunal administratif de Toulouse a commis une erreur de droit en jugeant que l'intégralité des revenus de son conjoint devait être prise en considération dans le calcul des ressources de MmeB..., alors même que les époux auraient été séparés de fait.<br/>
<br/>
              9. En revanche, par les moyens de son pourvoi incident, tirés de l'erreur de droit qu'a commise le tribunal en prenant en considération les revenus d'un conjoint séparé de fait et de la méconnaissance du champ d'application des dispositions du code de l'action sociale et des familles relatives au calcul des ressources du foyer, Mme B...ne critique pas utilement le jugement attaqué en tant qu'il rejette ses conclusions tendant à la condamnation de la caisse d'allocations familiales de la Haute-Garonne à lui verser une somme de 1 000 euros, à titre de dommages et intérêts, au motif qu'elle ne justifiait pas du préjudice dont elle demandait ainsi l'indemnisation.<br/>
<br/>
              10. Il résulte de ce qui précède que Mme B...est fondée à demander l'annulation de l'article 5 du jugement du tribunal administratif de Toulouse en tant qu'il rejette le surplus de ses conclusions relatives à ses droits au revenu de solidarité active et à l'indu mis à sa charge.<br/>
<br/>
              11. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond, dans la mesure de la cassation prononcée, en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              12. Il résulte de l'instruction que les décisions litigieuses ont été motivées par la seule circonstance que Mme B...n'avait pas déclaré, au titre des ressources de son foyer, les revenus commerciaux perçus par son époux résidant et travaillant exclusivement en Algérie, sans qu'il soit contesté que les époux étaient effectivement séparés de fait et que son mari ne participait pas aux charges du mariage et de l'entretien des enfants. <br/>
<br/>
              13. Dès lors, et sans qu'il soit besoin d'examiner les autres moyens de la demande de MmeB..., il résulte de ce qui précède que les deux décisions du 17 janvier 2012 par lesquelles le président du conseil général de la Haute-Garonne a rejeté son recours préalable obligatoire et sa demande de remise gracieuse doivent être annulées et que Mme B...doit être rétablie dans ses droits à l'allocation de revenu de solidarité active à compter du 1er septembre 2011. Toutefois, l'état de l'instruction ne permettant pas de fixer le montant de la somme qui lui est due au titre de cette allocation, il y a lieu de la renvoyer devant le département de la Haute-Garonne pour le calcul et le versement de l'allocation de revenu de solidarité active à compter de cette date. <br/>
<br/>
              Sur les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              14. Mme B...ayant obtenu le bénéfice de l'aide juridictionnelle, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. Il y a lieu, dans les circonstances de l'espèce, et sous réserve que la SCP Thouin-Palat, Boucard, avocat de MmeB..., renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge du département de la Haute-Garonne une somme de 3 000 euros à verser à cette SCP. Les dispositions de l'article L. 761-1 du code de justice administrative font, par ailleurs, obstacle à ce qu'une somme soit mise au même titre à la charge de MmeB..., qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les articles 2 et 3 du jugement du tribunal administratif de Toulouse du 10 juin 2015, ainsi que l'article 5 de ce jugement en tant qu'il rejette le surplus des conclusions de Mme B...relatives à ses droits au revenu de solidarité active et à l'indu mis à sa charge sont annulés.<br/>
Article 2 : Le surplus des conclusions du département de la Haute-Garonne est rejeté.<br/>
Article 3 : Le surplus des conclusions du pourvoi incident de Mme B...est rejeté.<br/>
Article 4 : Les décisions du 17 janvier 2012 du président du conseil général de la Haute-Garonne sont annulées.<br/>
Article 5 : Mme B...est rétablie dans ses droits à l'allocation de revenu de solidarité active à compter du 1er septembre 2011 et est renvoyée devant le département de la Haute-Garonne pour le calcul et le versement de l'allocation de revenu de solidarité active à compter de cette date.<br/>
Article 6 : Le département de la Haute-Garonne versera à la SCP Thouin-Palat, Boucard, avocat de MmeB..., une somme de 3 000 euros en application des dispositions du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991, sous réserve que cette SCP renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
Article 7 : La présente décision sera notifiée au département de la Haute-Garonne et à Mme A...B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">04-02-06 AIDE SOCIALE. DIFFÉRENTES FORMES D'AIDE SOCIALE. REVENU MINIMUM D'INSERTION (RMI). - RSA - PRISE EN COMPTE DU CONJOINT, CONCUBIN OU PARTENAIRE LIÉ PAR UN PACTE CIVIL DE SOLIDARITÉ - 1) CONDITION DE RÉSIDENCE STABLE ET EFFECTIVE EN FRANCE - CONSÉQUENCE - ABSENCE DE PRISE EN COMPTE DU CONJOINT VIVANT EXCLUSIVEMENT À L'ÉTRANGER - 2) CAS D'ÉPOUX SÉPARÉS DE FAIT - ABSENCE DE FOYER AU SENS DE L'ART. L. 262-2 DU CASF - CONSÉQUENCE - ABSENCE DE PRISE EN COMPTE DES REVENUS DU CONJOINT - PRISE EN COMPTE DES SEULES SOMMES VERSÉES OU PRESTATIONS EN NATURE SERVIES.
</SCT>
<ANA ID="9A"> 04-02-06 1) Il résulte des articles L. 262-2, L. 262-4 et L. 262-5 du code de l'action sociale et des familles (CASF) que, pour être pris en compte au titre des droits du bénéficiaire, son conjoint, concubin ou partenaire lié par un pacte civil de solidarité doit résider en France de manière stable et effective et, lorsqu'il est de nationalité étrangère, justifier des conditions de séjour prévues par l'article L. 262-4 ou par l'article L. 262-6 s'agissant des ressortissants d'un Etat membre de l'Union européenne, d'un autre Etat partie à l'accord sur l'Espace économique européen ou de la Confédération suisse. Erreur de droit à calculer les droits d'un bénéficiaire sur la base du montant dû à un couple avec trois enfants alors que son conjoint réside exclusivement à l'étranger.... ,,2) Lorsque des époux sont séparés de fait, ils ne constituent plus un foyer au sens de l'article L. 262-2 du code de l'action sociale et des familles et de l'article L. 262-3 du même code. En conséquence, dès lors que la séparation de fait des époux est effective, les revenus du conjoint n'ont pas à être pris en compte dans le calcul des ressources du bénéficiaire. Seules les sommes que le conjoint verse au bénéficiaire ou, le cas échéant, les prestations en nature qu'il lui sert, au titre notamment de ses obligations alimentaires, peuvent être prises en compte dans le calcul des ressources de ce dernier. Dans le cas où aucune somme ne lui est versée ou aucune prestation en nature ne lui est servie, il appartient au bénéficiaire du revenu de solidarité active de justifier avoir fait valoir ses droits aux créances d'aliments, dans les conditions prévues aux articles R. 262-46 et suivants du code de l'action sociale et des familles.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
