<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043648190</ID>
<ANCIEN_ID>JG_L_2021_06_000000448948</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/64/81/CETATEXT000043648190.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 09/06/2021, 448948</TITRE>
<DATE_DEC>2021-06-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>448948</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER ; SCP BARADUC, DUHAMEL, RAMEIX</AVOCATS>
<RAPPORTEUR>M. Guillaume Leforestier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Mireille Le Corre</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:448948.20210609</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              Procédures contentieuses antérieures <br/>
<br/>
              1. La société Allo Casse Auto a demandé au juge des référés du tribunal administratif de Paris, sur le fondement de l'article L. 551-1 du code de justice administrative, d'annuler la procédure de passation lancée par la ville de Paris pour l'attribution du lot n° 1 dans le cadre de la procédure d'appel d'offres ouvert pour la conclusion de deux conventions de retrait et de destruction des véhicules abandonnés en fourrière et de lui enjoindre, pour le cas où elle souhaiterait conclure un marché ayant le même objet, d'engager une nouvelle procédure. <br/>
<br/>
              Par une ordonnance n° 2021734 du 6 janvier 2021, le juge des référés du tribunal administratif de Paris a fait droit à sa demande.<br/>
<br/>
              2. La société Euro Casse a demandé au juge des référés du tribunal administratif de Paris, sur le fondement de l'article L. 551-1 du code de justice administrative, d'annuler la même procédure pour l'attribution des lots n° 1 et n° 2, ainsi que les décisions de rejet de ses offres pour ces lots et d'attribution des mêmes lots à la société France Moteurs. Par une ordonnance n° 2021181 du 6 janvier 2021, le juge des référés du tribunal administratif de Paris a fait droit à sa demande.<br/>
<br/>
              Procédures devant le Conseil d'Etat<br/>
<br/>
              1° Sous le n° 448948, par un pourvoi sommaire et un mémoire complémentaire enregistrés les 20 janvier et 4 février 2021 au secrétariat du contentieux du Conseil d'Etat, la ville de Paris demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 2021734 du 6 janvier 2021 ;<br/>
<br/>
              2°) réglant l'affaire au titre de la procédure de référé, de rejeter la demande de la société Allo Casse Auto ;<br/>
<br/>
              3°) de mettre à la charge de la société Allo Casse Auto une somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              2° Sous le n° 448949, par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 20 janvier et 4 février 2021 au secrétariat du contentieux du Conseil d'Etat, la ville de Paris demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 2021181 du 6 janvier 2021 ;<br/>
<br/>
              2°) réglant l'affaire au titre de la procédure de référé, de rejeter la demande de la société Euro Casse ;<br/>
<br/>
              3°) de mettre à la charge de la société Euro Casse une somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - le code de la commande publique ;<br/>
              - le code de la route ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Guillaume Leforestier, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Mireille Le Corre, rapporteure publique ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Foussard, Froger, avocat de la ville de Paris, à la SCP Baraduc, Duhamel, Rameix, avocat de la société Allo Casse et à la SCP Zribi et Texier, avocat de la société Euro Casse ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Les pourvois visés ci-dessus présentent à juger les mêmes questions. Il y a lieu de les joindre pour statuer par une seule décision.<br/>
<br/>
              2. Il ressort des pièces des dossiers soumis au juge des référés que, le 26 août 2020, la ville de Paris a lancé une consultation sous forme d'appel d'offres ouvert pour l'attribution de deux contrats relatifs au retrait et à la destruction des véhicules abandonnés dans ses parcs de fourrière de Chevaleret, La Courneuve et Charléty d'une part (lot n° 1) et de Bonneuil 1 et 2 d'autre part (lot n° 2). Par deux courriels du 10 décembre 2020, la ville de Paris a informé les sociétés Allo Casse Auto et Euro Casse du rejet de leurs offres. Par deux ordonnances du 6 janvier 2021, contre lesquelles la ville de Paris se pourvoit en cassation, le juge des référés du tribunal administratif de Paris a fait droit aux demandes des sociétés Allo Casse Auto et Euro Casse tendant à l'annulation de ces procédures.<br/>
<br/>
              3. Aux termes de l'article L. 2 du code de la commande publique : " Sont des contrats de la commande publique les contrats conclus à titre onéreux par un acheteur ou une autorité concédante, pour répondre à ses besoins en matière de travaux, de fournitures ou de services, avec un ou plusieurs opérateurs économiques ". Aux termes de l'article L. 1121-1 du même code : " Un contrat de concession est un contrat par lequel une ou plusieurs autorités concédantes soumises au présent code confient l'exécution de travaux ou la gestion d'un service à un ou plusieurs opérateurs économiques, à qui est transféré un risque lié à l'exploitation de l'ouvrage ou du service, en contrepartie soit du droit d'exploiter l'ouvrage ou le service qui fait l'objet du contrat, soit de ce droit assorti d'un prix ".<br/>
<br/>
              4. Il ressort des pièces des dossiers soumis au juge des référés que les contrats pour la conclusion desquels la ville de Paris a lancé la procédure litigieuse ont pour objet de confier à leur titulaire l'enlèvement des véhicules abandonnés dans les parcs de fourrière placés sous sa responsabilité, conformément à l'article L. 325-8 du code de la route. La fréquence et le volume des enlèvements auxquels le titulaire s'engage à procéder sont fixés par les stipulations du contrat. Le service ainsi rendu par les entreprises de démolition automobile cocontractantes ne fait l'objet d'aucune rémunération sous la forme d'un prix, les stipulations des conventions projetées, qui reprennent les clauses types définies à l'article R. 325-45 du code de la route, indiquant que ces entreprises ont le droit, en contrepartie de leurs obligations, de disposer des accessoires, pièces détachées et matières ayant une valeur marchande issus des véhicules. Aucune stipulation de ces conventions ne prévoit par ailleurs de compensation, par la ville de Paris, des éventuelles pertes financières que pourrait subir son cocontractant du fait des risques inhérents à l'exploitation commerciale des produits issus de ces enlèvements. Dans ces conditions, ces conventions, qui prévoient que la rémunération du service rendu prend la forme du droit d'exploiter les véhicules abandonnés et qui transfèrent à leurs titulaires le risque inhérent à cette exploitation, présentent le caractère de concessions de service.<br/>
<br/>
              5. Pour annuler la procédure de passation litigieuse, le juge des référés du tribunal administratif de Paris a retenu qu'elle avait été conduite en méconnaissance des obligations de publicité prévues aux articles L. 2124-1, L. 2131-1 et R. 2131-16 du code de la commande publique, applicables aux seuls marchés publics. En statuant de la sorte, le juge des référés a méconnu le champ d'application de la loi. Ses ordonnances doivent être annulées pour ce motif, sans qu'il soit besoin de se prononcer sur les moyens des pourvois.<br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au titre de la procédure de référé engagée, en application de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              7. Aux termes de l'article L. 551-1 du code de justice administrative : " Le président du tribunal administratif, ou le magistrat qu'il délègue, peut être saisi en cas de manquement aux obligations de publicité et de mise en concurrence auxquelles est soumise la passation par les pouvoirs adjudicateurs de contrats administratifs ayant pour objet l'exécution de travaux, la livraison de fournitures ou la prestation de services, avec une contrepartie économique constituée par un prix ou un droit d'exploitation, la délégation d'un service public ou la sélection d'un actionnaire opérateur économique d'une société d'économie mixte à opération unique... ".<br/>
<br/>
              8. Ainsi qu'il a été dit au point 4, les conventions faisant l'objet de la procédure litigieuse sont des concessions de service. Aux termes de l'article L. 3124-4 du code de la commande publique : " Le contrat de concession est attribué au soumissionnaire qui a présenté la meilleure offre au regard de l'avantage économique global pour l'autorité concédante sur la base de plusieurs critères objectifs, précis et liés à l'objet du contrat de concession ou à ses conditions d'exécution ". Aux termes de l'article R. 3124-4 du même code : " Pour attribuer le contrat de concession, l'autorité concédante se fonde, conformément aux dispositions de l'article L. 3124-5, sur une pluralité de critères non discriminatoires. Au nombre de ces critères, peuvent figurer notamment des critères environnementaux, sociaux, relatifs à l'innovation. / Les critères et leur description sont indiqués dans l'avis de concession, dans l'invitation à présenter une offre ou dans tout autre document de la consultation ".<br/>
<br/>
              9. Il résulte de l'instruction qu'en méconnaissance des dispositions citées au point 8, aucun critère de sélection n'a été communiqué par la ville de Paris aux entreprises candidates dans le cadre de la procédure en litige. Ce manquement, qui est susceptible d'avoir lésé les sociétés requérantes, justifie l'annulation de cette procédure, sans qu'il soit besoin de se prononcer sur les autres moyens des requêtes et sans qu'il y ait lieu, dans les circonstances de l'espèce, d'enjoindre à la ville de Paris de la reprendre.<br/>
<br/>
              10. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la ville de Paris la somme de 2 000 euros à verser à la société Allo Casse Auto et à la société Euro Casse en application des dispositions de l'article L. 761-1 du code de justice administrative. Les mêmes dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de ces sociétés qui ne sont pas, dans les présentes instances, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les ordonnances du juge des référés du tribunal administratif de Paris du 6 janvier 2021 sont annulées.<br/>
Article 2 : La procédure d'attribution des contrats relatifs au retrait et à la destruction des véhicules abandonnés en fourrière, correspondant aux lots n° 1 et n° 2, est annulée.<br/>
Article 3 : La ville de Paris versera une somme de 2 000 euros chacune à la société Allo Casse Auto et à la société Euro Casse au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Le surplus des conclusions des parties est rejeté.<br/>
Article 5 : La présente décision sera notifiée à la ville de Paris, à la société Allo Casse Auto et à la société Euro Casse.<br/>
Copie sera transmise à la société France Moteurs<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">135-02-03-03 COLLECTIVITÉS TERRITORIALES. COMMUNE. ATTRIBUTIONS. SERVICES COMMUNAUX. - CONTRAT PORTANT SUR L'ENLÈVEMENT DE VÉHICULES ABANDONNÉS EN FOURRIÈRE - CONCESSION DE SERVICE - INCLUSION, DÈS LORS QUE LE TITULAIRE EST RÉMUNÉRÉ PAR LE DROIT D'EXPLOITER CES VÉHICULES ET QUE LUI EST TRANSFÉRÉ LE RISQUE INHÉRENT À CETTE EXPLOITATION - CONSÉQUENCE - APPLICABILITÉ DES OBLIGATIONS DE PUBLICITÉ PROPRES AUX MARCHÉS PUBLICS - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">39-01-03 MARCHÉS ET CONTRATS ADMINISTRATIFS. NOTION DE CONTRAT ADMINISTRATIF. DIVERSES SORTES DE CONTRATS. - CONCESSION DE SERVICE - CONTRAT PORTANT SUR L'ENLÈVEMENT DE VÉHICULES ABANDONNÉS EN FOURRIÈRE - INCLUSION, DÈS LORS QUE LE TITULAIRE EST RÉMUNÉRÉ PAR LE DROIT D'EXPLOITER CES VÉHICULES ET QUE LUI EST TRANSFÉRÉ LE RISQUE INHÉRENT À CETTE EXPLOITATION - CONSÉQUENCE - APPLICABILITÉ DES OBLIGATIONS DE PUBLICITÉ PROPRES AUX MARCHÉS PUBLICS - ABSENCE.
</SCT>
<ANA ID="9A"> 135-02-03-03 Contrats passés par une commune et ayant pour objet de confier à leur titulaire l'enlèvement des véhicules abandonnés dans les parcs de fourrière placés sous leur responsabilité, conformément à l'article L. 325-8 du code de la route.... ,,La fréquence et le volume des enlèvements auxquels le titulaire s'engage à procéder sont fixés par les stipulations du contrat. Le service ainsi rendu par les entreprises de démolition automobile cocontractantes ne fait l'objet d'aucune rémunération sous la forme d'un prix, les stipulations des conventions projetées, qui reprennent les clauses types définies à l'article R. 325-45 du code de la route, indiquant que ces entreprises ont le droit, en contrepartie de leurs obligations, de disposer des accessoires, pièces détachées et matières ayant une valeur marchande issus des véhicules. Aucune stipulation de ces conventions ne prévoit par ailleurs de compensation, par la commune, des éventuelles pertes financières que pourrait subir son cocontractant du fait des risques inhérents à l'exploitation commerciale des produits issus de ces enlèvements.... ,,Dans ces conditions, ces conventions, qui prévoient que la rémunération du service rendu prend la forme du droit d'exploiter les véhicules abandonnés et qui transfèrent à leurs titulaires le risque inhérent à cette exploitation, présentent le caractère de concessions de service.,,,Par suite, ces contrats ne sont pas soumis aux obligations de publicité prévues aux articles L. 2124-1, L. 2131-1 et R. 2131-16 du code de la commande publique, applicables aux seuls marchés publics.</ANA>
<ANA ID="9B"> 39-01-03 Contrats passés par une commune et ayant pour objet de confier à leur titulaire l'enlèvement des véhicules abandonnés dans les parcs de fourrière placés sous leur responsabilité, conformément à l'article L. 325-8 du code de la route.... ,,La fréquence et le volume des enlèvements auxquels le titulaire s'engage à procéder sont fixés par les stipulations du contrat. Le service ainsi rendu par les entreprises de démolition automobile cocontractantes ne fait l'objet d'aucune rémunération sous la forme d'un prix, les stipulations des conventions projetées, qui reprennent les clauses types définies à l'article R. 325-45 du code de la route, indiquant que ces entreprises ont le droit, en contrepartie de leurs obligations, de disposer des accessoires, pièces détachées et matières ayant une valeur marchande issus des véhicules. Aucune stipulation de ces conventions ne prévoit par ailleurs de compensation, par la commune, des éventuelles pertes financières que pourrait subir son cocontractant du fait des risques inhérents à l'exploitation commerciale des produits issus de ces enlèvements.... ,,Dans ces conditions, ces conventions, qui prévoient que la rémunération du service rendu prend la forme du droit d'exploiter les véhicules abandonnés et qui transfèrent à leurs titulaires le risque inhérent à cette exploitation, présentent le caractère de concessions de service.,,,Par suite, ces contrats ne sont pas soumis aux obligations de publicité prévues aux articles L. 2124-1, L. 2131-1 et R. 2131-16 du code de la commande publique, applicables aux seuls marchés publics.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
