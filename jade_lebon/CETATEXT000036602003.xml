<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036602003</ID>
<ANCIEN_ID>JG_L_2018_02_000000411688</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/60/20/CETATEXT000036602003.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 14/02/2018, 411688</TITRE>
<DATE_DEC>2018-02-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>411688</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Emmanuelle Petitdemange</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:411688.20180214</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Par une requête et un mémoire en réplique, enregistrés au secrétariat du contentieux du Conseil d'Etat les 20 juin et 1er août 2017, la société Arcos Dorados Martinique demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler, pour excès de pouvoir, la décision du 21 avril 2017 par laquelle le directeur interrégional des douanes et droits indirects Antilles-Guyane lui a indiqué qu'il estimait que l'activité qu'elle exerçait était une activité de transformation de biens meubles corporels entrant dans le champ de l'octroi de mer, tel que défini par les dispositions de l'article 2 de la loi du 2 juillet 2004 relative à l'octroi de mer, dans sa version issue de la loi du 29 décembre 2016 de finances rectificative pour 2016 ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code des douanes ;<br/>
              - la loi n° 2004-639 du 2 juillet 2004 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Emmanuelle Petitdemange, auditeur,  <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. La société Arcos Dorados Martinique a saisi le Conseil d'Etat d'une demande tendant à l'annulation, pour excès de pouvoir, de la décision du 21 avril 2017 par laquelle le directeur interrégional des douanes et droits indirects Antilles-Guyane, en réponse à une demande tendant à ce qu'il prenne position sur cette question, lui a indiqué qu'il estimait que l'activité qu'elle exerçait avait la nature d'une activité de transformation de biens meubles corporels entrant dans le champ de l'octroi de mer, tel que défini par les dispositions de l'article 2 de la loi du 2 juillet 2004 relative à l'octroi de mer, dans sa version issue de la loi du 29 décembre 2016 de finances rectificative pour 2016.<br/>
<br/>
              2. Aux termes du I de l'article 1er de la loi du 2 juillet 2004 : " En Guadeloupe, en Guyane, en Martinique, à Mayotte et à La Réunion, sont soumises à une taxe dénommée octroi de mer : / 1° Les importations de biens ; / 2° Les livraisons de biens effectuées à titre onéreux par les personnes qui les ont produits ". Aux termes de l'article 2 de la même loi : " Sont assujetties à l'octroi de mer les personnes qui exercent de manière indépendante, à titre exclusif ou non exclusif, une activité de production dans une collectivité mentionnée à l'article 1er, lorsque, au titre de l'année civile précédente, leur chiffre d'affaires afférent à cette activité a atteint ou dépassé 300 000 euros, quels que soient leur statut juridique et leur situation au regard des autres impôts. Sont considérées comme des activités de production les opérations de fabrication, de transformation ou de rénovation de biens meubles corporels, ainsi que les opérations agricoles et extractives.  (...) Une opération de transformation, telle que mentionnée au deuxième alinéa, est caractérisée lorsque le bien transformé se classe, dans la nomenclature figurant à l'annexe I au règlement (CEE) n° 2658/87 du Conseil du 23 juillet 1987 relatif à la nomenclature tarifaire et statistique et au tarif douanier commun, à une position tarifaire différente de celle des biens mis en oeuvre pour l'obtenir. Ce changement s'apprécie au niveau de nomenclature du système harmonisé dit "SH 4", soit les quatre premiers chiffres de la nomenclature combinée ". Aux termes de l'article 42 de la même loi : " L'octroi de mer et l'octroi de mer régional sont perçus, contrôlés et recouvrés par la direction générale des douanes et droits indirects, selon les règles, garanties, privilèges et sanctions prévus par le code des douanes. Les infractions sont constatées, réprimées et les instances instruites et jugées conformément aux dispositions du même code ". Aux termes du II de l'article 345 bis du code des douanes : " Lorsque l'administration a formellement pris position sur l'appréciation d'une situation de fait au regard d'un texte fiscal, elle ne peut constater par voie d'avis de mise en recouvrement et recouvrer les droits et taxes perçus selon les modalités du présent code en prenant une position différente ". Aux termes de l'article 357 bis du code des douanes : " Les tribunaux d'instance connaissent des contestations concernant le paiement, la garantie ou le remboursement des créances de toute nature recouvrées par l'administration des douanes et des autres affaires de douane n'entrant pas dans la compétence des juridictions répressives ".<br/>
<br/>
              3. Un acte, par lequel l'administration des douanes fait connaître à une personne physique ou morale qu'elle estime que l'activité exercée par cette personne dans une collectivité mentionnée à l'article 1er précité de la loi du 2 juillet 2004 constitue une activité entrant dans le champ de l'octroi de mer, doit être regardé comme concourant à la détermination de droits de douanes et constitue, dès lors, une " affaire de douane ", au sens de l'article 357 bis précité du code des douanes. Il suit de là que la juridiction administrative n'est pas compétente pour connaître de conclusions dirigées contre un tel acte. La requête de la société Arcos Dorados Martinique doit, par suite, être rejetée. <br/>
<br/>
              4. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise, à ce titre, à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>                   D E C I D E :<br/>
                                 --------------<br/>
<br/>
Article 1er : La requête de la société Arcos Dorados Martinique est rejetée comme portée devant un ordre de juridiction incompétent pour en connaître.<br/>
<br/>
Article 2 : La présente décision sera notifiée à la société Arcos Dorados Martinique et au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">17-03-01-02-03-01 COMPÉTENCE. RÉPARTITION DES COMPÉTENCES ENTRE LES DEUX ORDRES DE JURIDICTION. COMPÉTENCE DÉTERMINÉE PAR DES TEXTES SPÉCIAUX. ATTRIBUTIONS LÉGALES DE COMPÉTENCE AU PROFIT DES JURIDICTIONS JUDICIAIRES. COMPÉTENCE DES JURIDICTIONS JUDICIAIRES EN MATIÈRE FISCALE ET PARAFISCALE. EN MATIÈRE FISCALE. - CONTESTATIONS RELATIVES À L'ASSIETTE ET AU RECOUVREMENT DES DROITS DE DOUANE (ARTICLE 357 BIS DU CODE DES DOUANES) [RJ1] - NOTION - RECOURS DIRIGÉ CONTRE UN ACTE PAR LEQUEL L'ADMINISTRATION DES DOUANES FAIT CONNAÎTRE QU'ELLE ESTIME QUE L'ACTIVITÉ EXERCÉE PAR UNE PERSONNE ENTRE DANS LE CHAMP DE L'OCTROI DE MER - INCLUSION [RJ2].
</SCT>
<ANA ID="9A"> 17-03-01-02-03-01 Un acte, par lequel l'administration des douanes fait connaître à une personne physique ou morale qu'elle estime que l'activité exercée par cette personne dans une collectivité mentionnée à l'article 1er de la loi n° 2004-639 du 2 juillet 2004 constitue une activité entrant dans le champ de l'octroi de mer, doit être regardé comme concourant à la détermination de droits de douanes et constitue, dès lors, une « affaire de douane », au sens de l'article 357 bis du code des douanes. Il suit de là que la juridiction administrative n'est pas compétente pour connaître de conclusions dirigées contre un tel acte.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]  Rappr.TC, 1er juillet 2002, Société Pinault Bretagne et Cie, n° 3295, p. 546 ; TC, 16 mars 1998, M.,, n° 3053, p. 536., ,[RJ2] Rappr., CE, 22 décembre 2017, Société Highlands Technologies, n° 396396, T. p. 513.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
