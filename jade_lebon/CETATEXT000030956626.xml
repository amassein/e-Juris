<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030956626</ID>
<ANCIEN_ID>JG_L_2015_07_000000375794</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/95/66/CETATEXT000030956626.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème SSJS, 27/07/2015, 375794</TITRE>
<DATE_DEC>2015-07-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>375794</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème SSJS</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Anne Egerszegi</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:375794.20150727</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête enregistrée le 25 février 2014 au secrétariat du contentieux du Conseil d'Etat, la communauté de communes de la vallée du Louron demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté du Premier ministre du 24 juillet 2013 modifiant l'arrêté du 10 juillet 2013 constatant le classement de communes en zone de revitalisation rurale en tant qu'il ne comprend pas les communes d'Armenteule, Avajan, Bareilles, Bordères-Louron, Estarvielle et Mont, ainsi que la décision du 27 décembre 2013 de rejet de son recours gracieux ;<br/>
<br/>
              2°) d'enjoindre à l'Etat de constater le classement de ces communes en zone de revitalisation rurale ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts ;<br/>
              - le décret n° 2013-548 du 26 juin 2013 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Anne Egerszegi, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. D'une part, l'article L. 311-1 du code de justice administrative dispose que : " Les tribunaux administratifs sont, en premier ressort, juges de droit commun du contentieux administratif, sous réserve des compétences que l'objet du litige ou l'intérêt d'une bonne administration de la justice conduisent à attribuer à une autre juridiction administrative ". Selon le premier alinéa de l'article R. 312-1 du même code : " Lorsqu'il n'en est pas disposé autrement par les dispositions de la section 2 du présent chapitre ou par un texte spécial, le tribunal administratif territorialement compétent est celui dans le ressort duquel a légalement son siège l'autorité qui, soit en vertu de son pouvoir propre, soit par délégation, a pris la décision attaquée ou a signé le contrat litigieux (...) ". L'article R. 312-7 de ce code dispose toutefois que : " Les litiges relatifs aux déclarations d'utilité publique, au domaine public, aux affectations d'immeubles, au remembrement, à l'urbanisme et à l'habitation, au permis de construire, d'aménager ou de démolir, au classement des monuments et des sites et, de manière générale, aux décisions concernant des immeubles relèvent de la compétence du tribunal administratif dans le ressort duquel se trouvent les immeubles faisant l'objet du litige ".<br/>
<br/>
              2. D'autre part, l'article 1465 A du code général des impôts prévoit notamment que les zones de revitalisation rurale, dont le périmètre est arrêté par décret, " comprennent les communes membres d'un établissement public de coopération intercommunale à fiscalité propre, incluses dans un arrondissement ou un canton caractérisé par une très faible densité de population ou par une faible densité de population et satisfaisant à l'un des trois critères socio-économiques suivants : / a. un déclin de la population constaté sur l'ensemble de l'arrondissement ou du canton ou dans une majorité de leurs communes dont le chef-lieu ; / b. un déclin de la population active ; / c. une forte proportion d'emplois agricoles. / (...) ". L'article 8 du décret du 26 juin 2013 pris pour l'application du II de l'article 1465 A du code général des impôts relatif aux zones de revitalisation rurale dispose que : " La liste constatant le classement des communes en zone de revitalisation rurale est établie et révisée chaque année par arrêté du Premier ministre (...) ".<br/>
<br/>
              3. Une requête dirigée contre un tel arrêté du Premier ministre en tant qu'il ne constate pas le classement de certaines communes en zone de revitalisation rurale ne relève d'aucune des catégories dont il appartient au Conseil d'Etat de connaître en premier et dernier ressort en vertu de l'article R. 311-1 du code de justice administrative, dès lors notamment que cet arrêté ne revêt pas un caractère réglementaire. Il résulte des dispositions précitées de l'article R. 312-7 de ce code que le tribunal administratif dans le ressort duquel se situe les communes concernées est compétent pour connaître en premier ressort d'une telle demande.<br/>
<br/>
              4. La communauté de communes de la vallée du Louron demande au Conseil d'Etat d'annuler pour excès de pouvoir l'arrêté du 24 juillet 2013 par lequel le Premier ministre a constaté le classement de communes en zone de revitalisation rurale en tant qu'il ne mentionne pas les communes d'Armenteule, Avajan, Bareilles, Bordères-Louron, Estarvielle et Mont. Il résulte de ce qui précède qu'il y a lieu de transmettre sa requête au tribunal administratif de Pau.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le jugement des conclusions de la requête de la communauté de communes de la vallée du Louron est attribué au tribunal administratif de Pau.<br/>
<br/>
Article 2 : La présente décision sera notifiée à la communauté de communes de la vallée du Louron et au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-01-06-01-02 ACTES LÉGISLATIFS ET ADMINISTRATIFS. DIFFÉRENTES CATÉGORIES D'ACTES. ACTES ADMINISTRATIFS - CLASSIFICATION. ACTES RÉGLEMENTAIRES. NE PRÉSENTENT PAS CE CARACTÈRE. - ARRÊTÉ DU PREMIER MINISTRE RELATIF AU CLASSEMENT DE COMMUNES EN ZONE DE REVITALISATION RURALE (ART. 1465 A DU CODE GÉNÉRAL DES IMPÔTS).
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">17-05-01-01-01 COMPÉTENCE. COMPÉTENCE À L'INTÉRIEUR DE LA JURIDICTION ADMINISTRATIVE. COMPÉTENCE EN PREMIER RESSORT DES TRIBUNAUX ADMINISTRATIFS. COMPÉTENCE MATÉRIELLE. ACTES NON RÉGLEMENTAIRES. - ARTICLE R. 312-7 DU CJA - CHAMP D'APPLICATION - INCLUSION - ARRÊTÉ DU PREMIER MINISTRE RELATIF AU CLASSEMENT DE COMMUNES EN ZONE DE REVITALISATION RURALE (ART. 1465 A DU CODE GÉNÉRAL DES IMPÔTS) [RJ1].
</SCT>
<ANA ID="9A"> 01-01-06-01-02 Une requête dirigée contre un arrêté du Premier ministre relatif au classement de communes en zone de revitalisation rurale (art. 1465 A du code général des impôts) ne relève d'aucune des catégories dont il appartient au Conseil d'Etat de connaître en premier et dernier ressort en vertu de l'article R. 311-1 du code de justice administrative (CJA), dès lors notamment que cet arrêté ne revêt pas un caractère réglementaire. Il résulte des dispositions de l'article R. 312-7 de ce code que le tribunal administratif dans le ressort duquel se situe les communes concernées est compétent pour connaître en premier ressort d'une telle demande.</ANA>
<ANA ID="9B"> 17-05-01-01-01 Une requête dirigée contre un arrêté du Premier ministre relatif au classement de communes en zone de revitalisation rurale (art. 1465 A du code général des impôts) ne relève d'aucune des catégories dont il appartient au Conseil d'Etat de connaître en premier et dernier ressort en vertu de l'article R. 311-1 du code de justice administrative (CJA), dès lors notamment que cet arrêté ne revêt pas un caractère réglementaire. Il résulte des dispositions de l'article R. 312-7 de ce code que le tribunal administratif dans le ressort duquel se situe les communes concernées est compétent pour connaître en premier ressort d'une telle demande.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr. CE, 10 janvier 2001, Chambre d'agriculture des Alpes-Maritimes, n° 213832, p.2.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
