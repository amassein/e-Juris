<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028717850</ID>
<ANCIEN_ID>JG_L_2014_03_000000358111</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/71/78/CETATEXT000028717850.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 4ème sous-sections réunies, 12/03/2014, 358111</TITRE>
<DATE_DEC>2014-03-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>358111</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 4ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOUTET ; LE PRADO</AVOCATS>
<RAPPORTEUR>Mme Marie Gautier-Melleray</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Fabienne Lambolez</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:358111.20140312</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 30 mars et 29 juin 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. et Mme B...A..., demeurant... ;  M. et Mme A...demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 11NC00212 du 26 janvier 2012 par lequel la cour administrative d'appel de Nancy a rejeté leur appel contre le jugement n° 0802050 du 9 décembre 2010 du tribunal administratif de Besançon rejetant leur demande tendant à ce que le centre hospitalier universitaire de Besançon soit condamné à leur verser respectivement les sommes de 731 107,42 euros et 2 391,54 euros en réparation des préjudices ayant résulté pour eux de l'infection nosocomiale dont M. A...a été victime lors de son hospitalisation du 5 février au 1er mai 1999 ;<br/>
<br/>
               2°) réglant l'affaire au fond, de faire droit aux conclusions de leur requête d'appel ; <br/>
<br/>
              3°) de mettre à la charge du centre hospitalier universitaire de Besançon le versement d'une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de la santé publique ;<br/>
<br/>
              Vu la loi n° 2002-303 du 4 mars 2002 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie Gautier-Melleray, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Fabienne Lambolez, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boutet, avocat de M. et Mme A...et à Me Le Prado, avocat du centre hospitalier universitaire de Besançon ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. A... a été pris en charge au centre hospitalier universitaire de Besançon du 5 février au 1er mai 1999, en raison d'une pneumopathie infectieuse hypoxémiante ; que des prélèvements réalisés au cours de cette hospitalisation ont révélé la présence de germes infectieux dans l'expectoration ainsi qu'au niveau d'une escarre et du cathéter posé ; que M. et Mme A...ont recherché la responsabilité de l'établissement au titre des préjudices ayant résulté de cette infection ; que, par un jugement du 9 décembre 2010, le tribunal administratif de Besançon, tout en admettant l'existence d'une infection nosocomiale, a jugé que celle-ci ne présentait pas un lien de causalité direct avec les préjudices dont la réparation était demandée, et rejeté en conséquence les conclusions de M. et Mme A...ainsi que celles formées, aux fins de remboursement de leurs débours, par la caisse primaire d'assurance maladie de la Haute-Saône et par la société Ionis Prévoyance ; que, par un arrêt du 26 janvier 2012, la cour administrative d'appel de Nancy a rejeté les appels formés par M. et Mme A...et par la caisse primaire d'assurance maladie de la Haute-Saône, en jugeant que l'infection nosocomiale contractée par M. A...avait trouvé son origine dans une cause étrangère ; que M. et Mme A... se pourvoient en cassation contre cet arrêt ;  <br/>
<br/>
              2. Considérant que l'introduction accidentelle d'un germe microbien dans l'organisme d'un patient lors d'une hospitalisation antérieure à l'entrée en vigueur des dispositions relatives à la réparation des infections nosocomiales issues de la loi susvisée du 4 mars 2002 révèle une faute dans l'organisation ou le fonctionnement du service hospitalier et engage la responsabilité de celui-ci ; qu'il en va toutefois autrement lorsqu'il est certain que l'infection, si elle s'est déclarée à la suite d'une intervention chirurgicale, a été causée par des germes déjà présents dans l'organisme du patient avant l'hospitalisation, ou encore lorsque la preuve d'une cause étrangère est rapportée par l'établissement de santé ;  <br/>
<br/>
              3. Considérant qu'après avoir relevé que M. A...avait contracté une infection nosocomiale durant son hospitalisation alors qu'il avait été transféré en service de réanimation, la cour a estimé qu'eu égard à l'état général de l'intéressé dont les défenses immunitaires étaient particulièrement amoindries par une pneumopathie sévère, accompagnée d'un syndrome de détresse respiratoire aiguë engageant le pronostic vital, cette infection présentait un caractère imprévisible et irrésistible et en a déduit que la preuve d'une cause étrangère était rapportée par le centre hospitalier universitaire de Besançon ; qu'en statuant ainsi, alors que l'infection était consécutive aux soins dispensés à M. A...et ne résultait donc pas d'une circonstance extérieure à l'activité du centre hospitalier, la cour administrative d'appel de Nancy a commis une erreur de droit ; que, par suite, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi,  son arrêt doit être annulé ;<br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond par application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              Sur la responsabilité du centre hospitalier universitaire de Besançon : <br/>
<br/>
              5. Considérant qu'il résulte de l'instruction que l'infection nosocomiale contractée par M. A... est liée à des germes dont l'origine endogène n'a pas été établie ; que, dès lors qu'elle est consécutive aux soins reçus par M. A...alors qu'il était en réanimation, elle ne résulte pas d'une circonstance extérieure à l'activité du centre hospitalier permettant de caractériser une cause étrangère ; que sa survenue révèle donc une faute dans l'organisation et le fonctionnement du service de nature à engager la responsabilité du centre hospitalier universitaire de Besançon ;<br/>
<br/>
              Sur les préjudices subis par M. et MmeA... : <br/>
<br/>
              6. Considérant qu'il résulte également de l'instruction, et notamment des constatations de l'expert qui ne sont remises en cause par aucune autre pièce médicale, que M. A... présentait une pneumopathie sévère à évolution extrêmement rapide se transformant en syndrome de détresse respiratoire aiguë engageant son pronostic vital et qui a conduit à son transfert du service de pneumologie au service de réanimation au sein duquel il a bénéficié d'une prise en charge adaptée à la gravité de son état ; que l'infection nosocomiale décelée à l'issue de la sortie de l'intéressé du service de réanimation,  alors que son état de santé s'était amélioré, n'a pas eu de conséquences sur l'évolution de celui-ci ; que, dès lors, les préjudices invoqués par M. et Mme A...sont imputables à la pathologie pulmonaire de M. A... et ne présentent pas de lien de causalité direct avec l'infection nosocomiale mentionnée ci-dessus ; qu'il ne résulte pas de l'instruction que cette infection nosocomiale aurait privé M. A...de chances de guérison ou d'amélioration de son état de santé ; <br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède, sans qu'il y ait lieu d'ordonner une nouvelle mesure d'expertise, que M. et Mme A...ne sont pas fondés à soutenir que c'est à tort que le tribunal administratif de Besançon a rejeté leur demande ; que la caisse primaire d'assurance maladie de la Haute-Saône n'est pas davantage fondée à demander le remboursement des débours exposés au profit de M. A...ainsi qu'une indemnité forfaitaire de gestion ; <br/>
<br/>
              Sur les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              8. Considérant que ces dispositions font obstacle à ce qu'il soit fait droit aux conclusions présentées à ce titre par M et MmeA... à l'encontre du centre hospitalier universitaire de Besançon, qui n'est pas la partie perdante dans la présente instance ;  <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nancy du 26 janvier 2012 est annulé.<br/>
<br/>
Article 2 : Les conclusions de la requête d'appel de M. et Mme A...ainsi que le surplus des conclusions de leur pourvoi sont rejetés.<br/>
<br/>
Article 3 : Les conclusions de la requête d'appel de la caisse primaire d'assurance maladie de la Haute-Saône sont rejetées. <br/>
<br/>
Article 4 : La présente décision sera notifiée à  M. et Mme B...A..., au centre hospitalier universitaire de Besançon, à la caisse primaire d'assurance maladie de la Haute-Saône et à la société Ionis Prévoyance.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">60-02-01-01-01 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RESPONSABILITÉ EN RAISON DES DIFFÉRENTES ACTIVITÉS DES SERVICES PUBLICS. SERVICE PUBLIC DE SANTÉ. ÉTABLISSEMENTS PUBLICS D'HOSPITALISATION. RESPONSABILITÉ POUR FAUTE SIMPLE : ORGANISATION ET FONCTIONNEMENT DU SERVICE HOSPITALIER. - RÉGIME ANTÉRIEUR À LA LOI DU 4 MARS 2002 - PRÉSOMPTION DE FAUTE - FAUTE RÉVÉLÉE PAR L'INTRODUCTION ACCIDENTELLE DANS L'ORGANISME D'UN PATIENT D'UN GERME MICROBIEN LORS D'UNE INTERVENTION CHIRURGICALE - CAS D'ABSENCE DE FAUTE - INCLUSION - CERTITUDE D'UNE INFECTION NOSOCOMIALE ENDOGÈNE - PREUVE D'UNE CAUSE ÉTRANGÈRE RAPPORTÉE PAR L'ÉTABLISSEMENT DE SANTÉ [RJ1].
</SCT>
<ANA ID="9A"> 60-02-01-01-01 L'introduction accidentelle d'un germe microbien dans l'organisme d'un patient lors d'une hospitalisation antérieure à l'entrée en vigueur des dispositions relatives à la réparation des infections nosocomiales issues de la loi n° 2002-303 du 4 mars 2002 révèle une faute dans l'organisation ou le fonctionnement du service hospitalier et engage la responsabilité de celui-ci. Il en va toutefois autrement lorsqu'il est certain que l'infection, si elle s'est déclarée à la suite d'une intervention chirurgicale, a été causée par des germes déjà présents dans l'organisme du patient avant l'hospitalisation, ou encore lorsque la preuve d'une cause étrangère est rapportée par l'établissement de santé.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Comp. CE, 2 février 2011,,, n° 320052, T. p. 1144.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
