<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032103999</ID>
<ANCIEN_ID>JG_L_2016_02_000000394945</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/10/39/CETATEXT000032103999.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème SSR, 24/02/2016, 394945</TITRE>
<DATE_DEC>2016-02-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>394945</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUIN-PALAT, BOUCARD ; SCP BOULLOCHE</AVOCATS>
<RAPPORTEUR>M. Frédéric Dieu</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2016:394945.20160224</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société SNN a demandé au juge du référé contractuel du tribunal administratif de Rouen, sur le fondement de l'article L. 551-13 du code de justice administrative, d'annuler le contrat ayant pour objet le transport, le traitement, la valorisation et la commercialisation des mâchefers produits par l'unité de valorisation énergétique " Ecoval ", conclu entre le syndicat mixte pour l'étude et le traitement des ordures ménagères de l'Eure et la société Matériaux Baie de Seine.<br/>
<br/>
              Par une ordonnance n° 1503373 du 16 novembre 2015, le juge des référés du tribunal administratif de Rouen a annulé ce contrat.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 1er et 16 décembre 2015 et le 9 février 2016 au secrétariat du contentieux du Conseil d'Etat, le syndicat mixte pour l'étude et le traitement des ordures ménagères de l'Eure demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de rejeter la demande présentée par la société SNN ;<br/>
<br/>
              3°) de mettre à la charge de la société SNN le versement de la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code des marchés publics ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Dieu, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Thouin-Palat, Boucard, avocat du syndicat mixte pour l'étude et le traitement des ordures ménagères de l'Eure, et à la SCP Boulloche, avocat de la société SNN ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 551-13 du code de justice administrative : " Le président du tribunal administratif, ou le magistrat qu'il délègue, peut être saisi, une fois conclu l'un des contrats mentionnés aux articles L. 551-1 et L. 551-5, d'un recours régi par la présente section " ; qu'en vertu des dispositions de l'article L. 551-18 du même code, le juge du référé contractuel " prononce la nullité du contrat lorsqu'aucune des mesures de publicité requises pour sa passation n'a été prise, ou lorsque a été omise une publication au Journal officiel de l'Union européenne dans le cas où une telle publication est prescrite. / La même annulation est prononcée lorsqu'ont été méconnues les modalités de remise en concurrence prévues pour la passation des contrats fondés sur un accord-cadre ou un système d'acquisition dynamique. / Le juge prononce également la nullité du contrat lorsque celui-ci a été signé avant l'expiration du délai exigé après l'envoi de la décision d'attribution aux opérateurs économiques ayant présenté une candidature ou une offre ou pendant la suspension prévue à l'article L. 551-4 ou à l'article L. 551-9 si, en outre, deux conditions sont remplies : la méconnaissance de ces obligations a privé le demandeur de son droit d'exercer le recours prévu par les articles L. 551-1 et L. 551-5, et les obligations de publicité et de mise en concurrence auxquelles sa passation est soumise ont été méconnues d'une manière affectant les chances de l'auteur du recours d'obtenir le contrat " ; qu'enfin, aux termes de l'article L. 551-20 du même code : " Dans le cas où le contrat a été signé avant l'expiration du délai exigé après l'envoi de la décision d'attribution aux opérateurs économiques ayant présenté une candidature ou une offre ou pendant la suspension prévue à l'article L. 551-4 ou à l'article L. 551-9, le juge peut prononcer la nullité du contrat, le résilier, en réduire la durée ou imposer une pénalité financière " ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis au juge des référés du tribunal administratif de Rouen que le syndicat mixte pour l'étude et le traitement des ordures ménagères de l'Eure a lancé une procédure d'appel d'offres ouvert en vue de la passation d'un marché ayant pour objet le transport, le traitement, la valorisation et la commercialisation de mâchefers produits par l'unité de valorisation énergétique " Ecoval " située dans la commune de Guichainville ; que, le 7 septembre 2015, la commission d'appel d'offres a décidé de retenir l'offre de la société Matériaux Baie de Seine ; que la société SNN, dont l'offre a été rejetée, a saisi le juge du référé précontractuel du tribunal administratif de Rouen, sur le fondement des dispositions de l'article L. 551-1 du code de justice administrative, d'une demande tendant à l'annulation de la procédure de passation de ce contrat ; qu'ayant appris au cours de l'instance que le marché litigieux avait été signé par le syndicat, la société SNN, renonçant à ses conclusions initiales, a demandé au juge du référé contractuel du même tribunal, sur le fondement de l'article L. 551-13 du code de justice administrative, d'annuler ce marché ; que le syndicat se pourvoit contre l'ordonnance par laquelle le juge des référés a fait droit à la demande de la société SNN et annulé le contrat litigieux ;<br/>
<br/>
              3. Considérant, en premier lieu, que la minute de l'ordonnance du juge des référés du tribunal administratif de Rouen du 16 novembre 2015 comporte la signature du magistrat qui a statué et celle du greffier d'audience, conformément aux prescriptions de l'article R. 741-8 du code de justice administrative ; qu'ainsi, le moyen tiré de la méconnaissance de ces prescriptions manque en fait ;<br/>
<br/>
              4. Considérant, en deuxième lieu, que, contrairement à ce que soutient le syndicat mixte pour l'étude et le traitement des ordures ménagères, le juge des référés du tribunal administratif de Rouen s'est prononcé sur le moyen invoqué en défense par le syndicat, tiré de ce qu'aucun manquement à ses obligations de publicité et de mise en concurrence n'était susceptible d'avoir affecté les chances de la société SNN d'obtenir le contrat, dès lors que l'offre présentée par celle-ci, signée par une personne qui n'était pas mandatée ou habilitée à engager la société était irrégulière et ne pouvait qu'être rejetée ; que, par suite, le syndicat mixte pour l'étude et le traitement des ordures ménagères n'est pas fondé à soutenir que le juge des référés a méconnu la portée de ses écritures sur ce point ;<br/>
<br/>
              5. Considérant, en troisième lieu, qu'aux termes de l'article 11 du code des marchés publics : " L'acte d'engagement est la pièce signée par un candidat à un accord-cadre ou à un marché public dans laquelle le candidat présente son offre (...). Cet acte d'engagement est ensuite signé par le pouvoir adjudicateur " ; qu'aux termes du I de l'article 45 du même code, le pouvoir adjudicateur peut exiger des candidats " des documents relatifs aux pouvoirs des personnes habilitées à les engager " ; qu'aux termes du premier alinéa du I de l'article 52 du même code, applicable à la sélection des candidatures : " Avant de procéder à l'examen des candidatures, le pouvoir adjudicateur qui constate que des pièces dont la production était réclamée sont absentes ou incomplètes peut demander à tous les candidats concernés de compléter leur dossier de candidature dans un délai identique pour tous et qui ne saurait être supérieur à dix jours. Il peut demander aux candidats n'ayant pas justifié de la capacité juridique leur permettant de déposer leur candidature de régulariser leur dossier dans les mêmes conditions. Il en informe les autres candidats qui ont la possibilité de compléter leur candidature dans le même délai " ; qu'aux termes de l'article 53 du même code: " I. - Pour attribuer le marché au candidat qui a présenté l'offre économiquement la plus avantageuse, le pouvoir adjudicateur se fonde : / 1° Soit sur une pluralité de critères non discriminatoires et liés à l'objet du marché, notamment la qualité, le prix, la valeur technique (...). / III. - Les offres inappropriées, irrégulières et inacceptables sont éliminées (...) " ; qu'enfin, aux termes de l'article 59, applicable en cas d'appel d'offres ouvert : " I. Il ne peut y avoir de négociation avec les candidats. Il est seulement possible de demander aux candidats de préciser ou de compléter la teneur de leur offre (...) " ;<br/>
<br/>
              6. Considérant que les dispositions précitées de l'article 52 du code des marchés publics, qui permettent au pouvoir adjudicateur d'inviter les candidats à compléter leur dossier de candidature et à régulariser ainsi leur candidature, ne sont pas applicables à la phase d'examen et de sélection des offres ; que, dans le cadre d'une procédure d'appel d'offres, le pouvoir adjudicateur est tenu d'écarter sans l'examiner ni la classer l'offre qui est irrégulière, inappropriée ou inacceptable et ne peut, en conséquence, inviter un candidat à la régulariser ; qu'alors même qu'il aurait procédé à son examen et à son classement, il peut se prévaloir du caractère irrégulier, inapproprié ou inacceptable de l'offre présentée par l'auteur du référé pour soutenir, devant le juge du référé précontractuel, que celui-ci n'est pas susceptible d'être lésé par les manquements aux obligations de publicité et de mise en concurrence qu'il invoque ou pour soutenir, devant le juge du référé contractuel, que ces manquements n'ont pas affecté ses chances d'obtenir le contrat ; <br/>
<br/>
              7. Considérant, toutefois, qu'il résulte des dispositions de l'article 45 du code des marchés publics que la production des documents relatifs aux pouvoirs des personnes habilitées à engager un candidat est exigible au stade de l'examen des candidatures ; qu'une offre ne saurait être regardée, par elle-même, comme irrégulière, au seul motif que le pouvoir adjudicateur ne dispose pas des documents attestant que le signataire de l'acte d'engagement est habilité à représenter l'entreprise candidate ; que, lorsque l'acte d'engagement est signé par une personne qui se présente comme un responsable de cette entreprise, il est loisible au pouvoir adjudicateur, à supposer qu'il doute de la capacité du signataire à engager le candidat, de solliciter la production des documents justifiant de cette capacité ; que, par suite, le juge des référés du tribunal administratif de Rouen n'a pas commis d'erreur de droit en jugeant que le syndicat mixte pour l'étude et le traitement des ordures ménagères de l'Eure, à défaut pour lui de s'être assuré que l'acte d'engagement remis par la société SNN n'avait pas été signé par une personne ayant qualité pour engager la société, ne pouvait se prévaloir de l'irrégularité de son offre pour soutenir qu'elle ne pouvait être lésée par les manquements aux obligations de publicité et de mise en concurrence qu'elle invoquait ;<br/>
<br/>
              8. Considérant qu'il résulte de tout ce qui précède que le pourvoi du syndicat mixte pour l'étude et le traitement des ordures ménagères de l'Eure doit être rejeté, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ; qu'il y a lieu, en revanche, au même titre, de mettre à la charge du syndicat une somme de 3 000 euros à verser à la société SNN ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi du syndicat mixte pour l'étude et le traitement des ordures ménagères de l'Eure est rejeté.<br/>
Article 2 : Le syndicat mixte pour l'étude et le traitement des ordures ménagères de l'Eure versera une somme de 3 000 euros à la société SNN au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée au syndicat mixte pour l'étude et le traitement des ordures ménagères de l'Eure, à la société SNN et à la société Matériaux Baie de Seine.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-08-015-02 MARCHÉS ET CONTRATS ADMINISTRATIFS. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. PROCÉDURES D'URGENCE. - MANQUEMENTS SUSCEPTIBLES D'AVOIR AFFECTÉ LES CHANCES DU REQUÉRANT D'OBTENIR LE CONTRAT - CIRCONSTANCE QUE LE REQUÉRANT N'AVAIT PAS PRODUIT DE DOCUMENTS ATTESTANT QUE LE SIGNATAIRE DE L'ACTE D'ENGAGEMENT REMIS PAR LUI ÉTAIT HABILITÉ À LE REPRÉSENTER - CIRCONSTANCE SANS INCIDENCE SUR L'OPÉRANCE DES MOYENS DU REQUÉRANT DÈS LORS QUE LE POUVOIR ADJUDICATEUR NE S'EST PAS ASSURÉ QUE CET ACTE D'ENGAGEMENT N'AVAIT PAS ÉTÉ SIGNÉ PAR UNE PERSONNE AYANT CETTE QUALITÉ [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-03-05 PROCÉDURE. PROCÉDURES DE RÉFÉRÉ AUTRES QUE CELLES INSTITUÉES PAR LA LOI DU 30 JUIN 2000. PROCÉDURE PROPRE À LA PASSATION DES CONTRATS ET MARCHÉS. - RÉFÉRÉ CONTRACTUEL - MANQUEMENTS SUSCEPTIBLES D'AVOIR AFFECTÉ LES CHANCES DU REQUÉRANT D'OBTENIR LE CONTRAT - CIRCONSTANCE QUE LE REQUÉRANT N'AVAIT PAS PRODUIT DE DOCUMENTS ATTESTANT QUE LE SIGNATAIRE DE L'ACTE D'ENGAGEMENT REMIS PAR LUI ÉTAIT HABILITÉ À LE REPRÉSENTER - CIRCONSTANCE SANS INCIDENCE SUR L'OPÉRANCE DES MOYENS DU REQUÉRANT DÈS LORS QUE LE POUVOIR ADJUDICATEUR NE S'EST PAS ASSURÉ QUE CET ACTE D'ENGAGEMENT N'AVAIT PAS ÉTÉ SIGNÉ PAR UNE PERSONNE AYANT CETTE QUALITÉ [RJ1].
</SCT>
<ANA ID="9A"> 39-08-015-02 Il résulte de l'article 45 du code des marchés publics (CMP) que la production des documents relatifs aux pouvoirs des personnes habilitées à engager un candidat est exigible au stade de l'examen des candidatures. Une offre ne saurait donc être regardée, par elle-même, comme irrégulière, au seul motif que le pouvoir adjudicateur ne dispose pas des documents attestant que le signataire de l'acte d'engagement est habilité à représenter l'entreprise candidate. Lorsque l'acte d'engagement est signé par une personne qui se présente comme un responsable de cette entreprise, il est loisible au pouvoir adjudicateur, à supposer qu'il doute de la capacité du signataire à engager le candidat, de solliciter la production des documents justifiant de cette capacité.,,,Par suite, le pouvoir adjudicateur, à défaut pour lui de s'être assuré que l'acte d'engagement remis par la société requérante n'avait pas été signé par une personne ayant cette qualité, ne peut se prévaloir de l'irrégularité de l'offre de la société pour faire échec à un référé contractuel.</ANA>
<ANA ID="9B"> 54-03-05 Il résulte de l'article 45 du code des marchés publics (CMP) que la production des documents relatifs aux pouvoirs des personnes habilitées à engager un candidat est exigible au stade de l'examen des candidatures. Une offre ne saurait donc être regardée, par elle-même, comme irrégulière, au seul motif que le pouvoir adjudicateur ne dispose pas des documents attestant que le signataire de l'acte d'engagement est habilité à représenter l'entreprise candidate. Lorsque l'acte d'engagement est signé par une personne qui se présente comme un responsable de cette entreprise, il est loisible au pouvoir adjudicateur, à supposer qu'il doute de la capacité du signataire à engager le candidat, de solliciter la production des documents justifiant de cette capacité.,,,Par suite, le pouvoir adjudicateur, à défaut pour lui de s'être assuré que l'acte d'engagement remis par la société requérante n'avait pas été signé par une personne ayant cette qualité, ne peut se prévaloir de l'irrégularité de l'offre de la société pour faire échec à un référé contractuel.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Cf., CE, 3 décembre 2014, Département de la Loire-Atlantique et Eiffage construction Pays de la Loire, n°s 384180, 384222, T. pp. 749-790, s'agissant d'un référé précontractuel.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
