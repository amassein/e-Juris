<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032571719</ID>
<ANCIEN_ID>JG_L_2016_05_000000385505</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/57/17/CETATEXT000032571719.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 6ème chambres réunies, 20/05/2016, 385505</TITRE>
<DATE_DEC>2016-05-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>385505</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 6ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Florence Marguerite</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Jean Lessi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:385505.20160520</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...C...a demandé au tribunal administratif de Nîmes d'annuler les décisions des 26 et 29 mars 2013 par lesquelles la caisse d'allocations familiales du Gard lui a notifié la fin de ses droits au titre du revenu de solidarité active et a refusé de prendre en considération le changement qu'il avait déclaré dans la composition de son foyer, ainsi que la décision implicite par laquelle le président du conseil général du Gard a rejeté son recours gracieux. Par un jugement n° 1302261 du 26 mai 2014, le tribunal administratif de Nîmes a rejeté sa demande.<br/>
<br/>
              Par une ordonnance n° 14MA04337 du 3 novembre 2014, enregistrée, sous le n° 385505, le 4 novembre 2014 au secrétariat du contentieux du Conseil d'Etat, le président de la cour administrative d'appel de Marseille a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, le pourvoi, enregistré le 31 octobre 2014 au greffe de cette cour, présenté par M.C.... <br/>
<br/>
<br/>
              Par ce pourvoi, un nouveau mémoire et un mémoire complémentaire, enregistrés, sous le n° 388256, les 24 février et 23 mai 2015 au secrétariat du contentieux du Conseil d'Etat, M. C...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler le jugement  du tribunal administratif de Nîmes du 26 mai 2014 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge du département du Gard la somme de 3 500 euros à verser à son avocat, Me Occhipinti, au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - le code de l'action sociale et des familles ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Florence Marguerite, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Jean Lessi, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Occhipinti, avocat de M.C..., et à la SCP Piwnica, Molinié, avocat du département du Gard ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que les documents enregistrés sous le n° 388256 constituent en réalité des mémoires présentés pour M. C...et faisant suite à son pourvoi enregistré sous le n° 385505 ; que, par suite, ces documents doivent être rayés des registres du secrétariat du contentieux du Conseil d'Etat et être joints au pourvoi enregistrés sous le n° 385505, de même que le mémoire en défense du département du Gard ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 262-2 du code de l'action sociale et des familles, dans sa rédaction applicable au litige : " Toute personne résidant en France de manière stable et effective, dont le foyer dispose de ressources inférieures à un niveau garanti, a droit au revenu de solidarité active dans les conditions définies au présent chapitre. / Le revenu garanti est calculé, pour chaque foyer, en faisant la somme : / 1° D'une fraction des revenus professionnels des membres du foyer ; / 2° D'un montant forfaitaire, dont le niveau varie en fonction de la composition du foyer et du nombre d'enfants à charge. / Le revenu de solidarité active est une allocation qui porte les ressources du foyer au niveau du revenu garanti (...) " ; que, selon l'article L. 262-9 du même code, dans sa rédaction issue de la loi du 21 décembre 2011 de financement de la sécurité sociale pour 2012 : " Le montant forfaitaire mentionné au 2° de l'article L. 262-2 est majoré, pendant une période d'une durée déterminée, pour : / 1° Une personne isolée assumant la charge d'un ou de plusieurs enfants ; / (...) Est considérée comme isolée une personne veuve, divorcée, séparée ou célibataire, qui ne vit pas en couple de manière notoire et permanente et qui notamment ne met pas en commun avec un conjoint, concubin ou partenaire de pacte civil de solidarité ses ressources et ses charges (...) " ; qu'aux termes de l'article 515-8 du code civil : " Le concubinage est une union de fait, caractérisée par une vie commune présentant un caractère de stabilité et de continuité, entre deux personnes, de sexe différent ou de même sexe, qui vivent en couple " ; qu'aux termes du premier alinéa de l'article R. 262-1 du code de l'action sociale et des familles, dans sa rédaction applicable au litige : " Le montant forfaitaire mentionné au 2° de l'article L. 262-2 applicable à un foyer composé d'une seule personne est majoré de 50 % lorsque le foyer comporte deux personnes. Ce montant est ensuite majoré de 30 % pour chaque personne supplémentaire présente au foyer et à la charge de l'intéressé. Toutefois, lorsque le foyer comporte plus de deux enfants ou personnes de moins de vingt-cinq ans à charge, à l'exception du conjoint, du partenaire lié par un pacte civil de solidarité ou du concubin de l'intéressé, la majoration à laquelle ouvre droit chacun de ces enfants ou personnes est portée à 40 % à partir de la troisième personne " ; que l'article R. 262-3 du même code précise enfin que : " Pour le bénéfice du revenu de solidarité active, sont considérés comme à charge : / 1° Les enfants ouvrant droit aux prestations familiales ; / 2° Les autres enfants et personnes de moins de vingt-cinq ans qui sont à la charge effective et permanente du bénéficiaire à condition, lorsqu'ils sont arrivés au foyer après leur dix-septième anniversaire, d'avoir avec le bénéficiaire ou son conjoint, son concubin ou le partenaire lié par un pacte civil de solidarité un lien de parenté jusqu'au quatrième degré inclus (...) ". <br/>
<br/>
              3. Considérant qu'il résulte de ces dispositions que, pour le bénéfice du revenu de solidarité active, le foyer s'entend du demandeur, ainsi que, le cas échéant, de son conjoint, partenaire lié par un pacte civil de solidarité ou concubin et des enfants ou personnes de moins de vingt-cinq ans à charge qui remplissent les conditions précisées par l'article R. 262-3 du code de l'action sociale et des familles ; que, pour l'application de ces dispositions, le concubin est la personne qui mène avec le demandeur une vie de couple stable et continue ; qu'une telle vie de couple peut être établie par un faisceau d'indices concordants, au nombre desquels la circonstance que les intéressés mettent en commun leurs ressources et leurs charges ;<br/>
<br/>
              4. Considérant que, pour juger que M. C...et Mme B...formaient un foyer au sens des dispositions de l'article L. 262-2 du code de l'action sociale et des familles, le tribunal administratif de Nîmes s'est fondé exclusivement sur la communauté d'intérêts et de biens existant entre eux et a estimé que la circonstance qu'il n'existerait pas de relations maritales entre M. C...et Mme B...était dépourvue d'incidence sur l'appréciation de l'existence d'un même foyer ; qu'en statuant ainsi, le tribunal a commis une erreur de droit ;<br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que M. C...est fondé, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, à demander l'annulation du jugement qu'il attaque ;<br/>
<br/>
              6. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              7. Considérant que lorsqu'il statue sur un recours dirigé contre une décision par laquelle l'administration, sans remettre en cause des versements déjà effectués, détermine les droits d'une personne à l'allocation de revenu de solidarité active, il appartient au juge administratif, eu égard tant à la finalité de son intervention dans la reconnaissance du droit à cette prestation d'aide sociale qu'à sa qualité de juge de plein contentieux, non de se prononcer sur les éventuels vices propres de la décision attaquée, mais d'examiner les droits de l'intéressé sur lesquels l'administration s'est prononcée, en tenant compte de l'ensemble des circonstances de fait qui résultent de l'instruction ; que, par suite, la circonstance que les décisions attaquées méconnaîtraient l'article 4 de la loi du 12 avril 2000 relative aux droits des citoyens dans leurs relations avec les administrations et l'article L. 262-39 du code de l'action sociale et des familles est, en tout état de cause, sans incidence sur le présent litige, qui porte sur les droits de M. C... au revenu de solidarité active à compter du 26 mars 2013 ; <br/>
<br/>
              8. Considérant que si M. C...et MmeB..., qui ont un enfant commun, né en 2008, et vivaient depuis lors en situation de concubinage, ainsi qu'ils l'ont déclaré dans une demande d'octroi du bénéfice du revenu de solidarité active déposée en mars 2010, affirment ne plus vivre en concubinage depuis septembre 2012, ils ne font état d'aucun élément susceptible de rendre crédible cette allégation ; qu'il résulte de l'instruction que les logements dans lesquels ils affirment résider séparément se trouvent sur le même terrain qu'ils ont acquis en 2008, qu'ils les détiennent en indivision et disposent des mêmes compteurs d'électricité et d'eau ; qu'ils sont couverts par un contrat d'assurance habitation unique, conclu au nom de M. C...mais dont les cotisations sont acquittées par MmeB... ; que Mme B... déclare assumer la charge financière de leur enfant commun, né en 2008, alors que les prestations familiales sont versées à M. C...et qu'aucune demande de pension alimentaire n'a été formée ; que, dans ces conditions, M. C... et Mme B...peuvent être regardés comme menant une vie de couple stable et continue caractérisant une relation de concubinage et, par suite, comme constituant un foyer au sens des dispositions du code de l'action sociale et des familles précitées ; que les ressources de ce foyer excèdent le niveau ouvrant droit au revenu de solidarité active ;<br/>
<br/>
              9. Considérant qu'il résulte de ce qui précède, sans qu'il soit besoin d'examiner la fin de non-recevoir opposée par le département du Gard, que M. C...n'est pas fondé à demander le rétablissement de ses droits au bénéfice du revenu de solidarité active à compter du 26 mars 2013 ; que ses conclusions à fin d'injonction ne peuvent, par suite, qu'être également rejetées ;<br/>
<br/>
              10. Considérant que les dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 font obstacle à ce qu'il soit fait droit aux conclusions présentées sur le fondement de ces dispositions par Me Occhipinti, avocat de M. C... devant le Conseil d'Etat, et par l'avocat de ce dernier devant le tribunal administratif de Nîmes ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les productions enregistrées sous le n° 388256 sont rayées du registre du secrétariat du contentieux du Conseil d'Etat pour être jointes au pourvoi n° 385505.<br/>
Article 2 : Le jugement du tribunal administratif de Nîmes du 26 mai 2014 est annulé.<br/>
Article 3 : La demande présentée par M. C...devant le tribunal administratif de Nîmes est rejetée.<br/>
Article 4 : Les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative et de l'article 37 de la loi du 10 juillet 1991 sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à M. A...C...et au département du Gard.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">04-02-06 AIDE SOCIALE. DIFFÉRENTES FORMES D'AIDE SOCIALE. REVENU MINIMUM D'INSERTION (RMI). - RSA - DROIT À L'ALLOCATION EN FONCTION DES RESSOURCES DU FOYER - COMPOSITION DU FOYER - CONCUBINAGE - NOTION [RJ1].
</SCT>
<ANA ID="9A"> 04-02-06 Pour le bénéfice du revenu de solidarité active (RSA), le foyer s'entend du demandeur, ainsi que, le cas échéant, de son conjoint, partenaire lié par un pacte civil de solidarité ou concubin et des enfants ou personnes de moins de vingt-cinq ans à charge qui remplissent les conditions précisées par l'article R. 262-3 du code de l'action sociale et des familles (CASF). Pour l'application de ces dispositions, le concubin est la personne qui mène avec le demandeur une vie de couple stable et continue. Une telle vie de couple peut être établie par un faisceau d'indices concordants, au nombre desquels la circonstance que les intéressés mettent en commun leurs ressources et leurs charges.... ,,En l'espèce, un homme et une femme ayant eu un enfant en 2008 en situation de concubinage, affirment ne plus vivre en concubinage depuis 2012. Cependant, les logements dans lesquels ils affirment résider séparément se trouvent sur le même terrain, qu'ils ont acquis en 2008 et détiennent en indivision, et disposent des mêmes compteurs d'électricité et d'eau ; ils sont couverts par un contrat d'assurance habitation unique, conclu au nom du premier mais dont les cotisations sont acquittées par la seconde, qui déclare assumer la charge financière de leur enfant commun, né en 2008, alors que les prestations familiales sont versées au premier et qu'aucune demande de pension alimentaire n'a été formée. Dans ces conditions, ces deux personnes peuvent être regardés comme menant une vie de couple stable et continue caractérisant une relation de concubinage et, par suite, comme constituant un foyer au sens des dispositions du code de l'action sociale et des familles.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. avant l'intervention de la loi n° 2011-1906 du 21 décembre 2011 qui a modifié l'article L. 262-9 du CASF, CE, 12 juin 2002, M.,, n° 216066, T. p. 617.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
