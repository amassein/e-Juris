<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032047972</ID>
<ANCIEN_ID>JG_L_2016_02_000000393236</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/04/79/CETATEXT000032047972.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème / 4ème SSR, 12/02/2016, 393236</TITRE>
<DATE_DEC>2016-02-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>393236</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème / 4ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>OCCHIPINTI</AVOCATS>
<RAPPORTEUR>Mme Dominique Chelle </RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2016:393236.20160212</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante : <br/>
<br/>
              M. A...B...a demandé au juge des référés du tribunal administratif de Grenoble, sur le fondement de l'article L. 521-1 du code de justice administrative, de suspendre l'exécution de la décision du 10 juillet 2015 par laquelle le ministre de l'intérieur a constaté la perte de validité de son permis de conduire pour solde de points nul et lui a enjoint de le restituer. Par une ordonnance n° 1505055 du 26 août 2015, le juge des référés a fait droit à sa demande.  <br/>
<br/>
              Par un pourvoi, enregistré le 7 septembre 2015 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'intérieur demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de rejeter la demande de M.B....<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
<br/>
              - le code de la route ;<br/>
<br/>
              - le code de procédure pénale ; <br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Dominique Chelle, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Occhipinti, avocat de M.B... ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision (...) " ; <br/>
<br/>
              2. Considérant que pour suspendre l'exécution de la décision du 10 juillet 2015 par laquelle le ministre de l'intérieur a invalidé le permis de conduire de M. B...pour solde de points nul, le juge des référés du tribunal administratif de Grenoble a retenu comme propre à créer un doute sérieux le moyen tiré ce que l'intéressé n'avait pas été destinataire des informations prévues par les articles L. 223-3 et R. 223-3 du code de la route préalablement aux retraits de points consécutifs aux infractions des 21 septembre 2012 et 19 mars 2015 ;  <br/>
<br/>
              3. Considérant que les dispositions portant application des articles R. 49-1 et R. 49-10 du code de procédure pénale en vigueur à la date des infractions litigieuses, notamment celles des articles A. 37-15 à A. 37-18 de ce code issues de l'arrêté du 13 mai 2011 relatif aux formulaires utilisés pour la constatation et le paiement des contraventions soumises à la procédure de l'amende forfaitaire, prévoient que lorsqu'une contravention soumise à cette procédure est constatée par un procès-verbal dressé avec un appareil électronique sécurisé, sans que l'amende soit payée immédiatement entre les mains de l'agent verbalisateur, il est adressé au contrevenant un avis de contravention, qui comporte une information suffisante au regard des exigences des articles L. 223-3 et R. 223-3 du code de la route, une notice de paiement qui comprend une carte de paiement et un formulaire de requête en exonération ;  que, dès lors, le titulaire d'un permis de conduire à l'encontre duquel une infraction au code de la route est relevée au moyen d'un appareil électronique sécurisé et dont il est établi, notamment par la mention qui en est faite au système national des permis de conduire, qu'il a payé, à une date postérieure à celle de l'infraction, l'amende forfaitaire correspondant à celle-ci, a nécessairement reçu l'avis de contravention ; qu'eu égard aux mentions dont cet avis est réputé être revêtu, l'administration doit être regardée comme s'étant acquittée envers le titulaire du permis de son obligation de lui délivrer les informations requises préalablement au paiement de l'amende, à moins que l'intéressé, à qui il appartient à cette fin de produire l'avis qu'il a nécessairement reçu, ne démontre s'être vu remettre un avis inexact ou incomplet ;  <br/>
<br/>
              4. Considérant qu'il ressort des pièces du dossier soumis au juge des référés et, notamment, des mentions du relevé d'information intégral relatif au permis de conduire de M. B..., que les infractions commises les 21 septembre 2012 et 19 mars 2015, relevées par procès-verbal électronique, ont donné lieu au paiement différé par celui-ci des amendes forfaitaires ; que M. B...ne contestait pas sérieusement ces éléments ; que, dès lors, en estimant que le moyen tiré de ce que l'intéressé n'avait pas bénéficié de l'information prévue aux articles L. 223-3 et R. 223-3 du code de la route était, en l'état de l'instruction, propre à créer un doute sérieux quant à la légalité de la décision contestée, le juge des référés a commis une erreur de droit ; que son ordonnance doit, par suite, être annulée ;  <br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'affaire, en vertu de l'article L. 822-1 du code de justice administrative, de statuer sur la demande de M. B...tendant à la suspension de la décision du ministre de l'intérieur du 10 juillet 2015 ;<br/>
<br/>
              6. Considérant, d'une part, qu'il ressort des pièces du dossier que l'exécution de la décision contestée porterait une atteinte grave et immédiate à la situation de M. B...qui exerce la profession de chauffeur routier ; qu'alors que les infractions commises par ce dernier, sur une période de sept ans, ne sont pas telles par leur nature et leur fréquence que des considérations liées à la sécurité routière commanderaient de maintenir le caractère exécutoire de cette décision, la condition d'urgence fixée par l'article L. 521-1 du code de justice administrative est remplie ; <br/>
<br/>
              7. Considérant, d'autre part, que le moyen tiré de ce que l'intéressé n'a pas bénéficié, lors de la constatation de l'infraction commise le 24 juin 2010, de l'information prévue aux articles L. 223-3 et R. 223-3 du code de la route est propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité du retrait de points consécutif à cette infraction et, par suite, de la décision constatant la perte de validité du permis pour solde de points nul ; <br/>
<br/>
              8. Considérant qu'il résulte de ce qui précède que M. B...est fondé à demander la suspension de l'exécution de la décision du 10 juillet 2015 par laquelle le ministre de l'intérieur a constaté la perte de validité de son permis de conduire pour solde de points nul et lui a enjoint de le restituer ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement à M. B...de la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du 26 août 2015 du juge des référés du tribunal administratif de Grenoble est annulée.<br/>
<br/>
Article 2 : L'exécution de la décision du 10 juillet 2015 du ministre de l'intérieur est suspendue dans l'attente du jugement du recours de M. B...tendant à l'annulation de cette décision.<br/>
<br/>
Article 3 : L'Etat versera à M. B...la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée au ministre de l'intérieur et à M. A...B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">49-04-01-04 POLICE. POLICE GÉNÉRALE. CIRCULATION ET STATIONNEMENT. PERMIS DE CONDUIRE. - INFRACTION AU CODE DE LA ROUTE - PREUVE DE CE QUE L'AUTEUR DE L'INFRACTION A REÇU LES INFORMATIONS REQUISES - CAS OÙ L'INFRACTION A ÉTÉ CONSTATÉE PAR UN PROCÈS-VERBAL ÉLECTRONIQUE ET OÙ L'AMENDE A ÉTÉ PAYÉE POSTÉRIEUREMENT - PRÉSOMPTION QUE LE CONDUCTEUR A REÇU LES INFORMATIONS REQUISES [RJ1].
</SCT>
<ANA ID="9A"> 49-04-01-04 Les dispositions portant application des articles R. 49-1 et R. 49-10 du code de procédure pénale, notamment celles des articles A. 37-15 à A. 37-18 de ce code issues de l'arrêté du 13 mai 2011 relatif aux formulaires utilisés pour la constatation et le paiement des contraventions soumises à la procédure de l'amende forfaitaire, prévoient que lorsqu'une contravention soumise à cette procédure est constatée par un procès-verbal dressé avec un appareil électronique sécurisé, sans que l'amende soit payée immédiatement entre les mains de l'agent verbalisateur, il est adressé au contrevenant un avis de contravention, qui comporte une information suffisante au regard des exigences des articles L. 223-3 et R. 223-3 du code de la route, une notice de paiement qui comprend une carte de paiement et un formulaire de requête en exonération. Dès lors, le titulaire d'un permis de conduire à l'encontre duquel une infraction au code de la route est relevée au moyen d'un appareil électronique sécurisé et dont il est établi, notamment par la mention qui en est faite au système national des permis de conduire, qu'il a payé, à une date postérieure à celle de l'infraction, l'amende forfaitaire correspondant à celle-ci, a nécessairement reçu l'avis de contravention. Eu égard aux mentions dont cet avis est réputé être revêtu, l'administration doit être regardée comme s'étant acquittée envers le titulaire du permis de son obligation de lui délivrer les informations requises préalablement au paiement de l'amende, à moins que l'intéressé, à qui il appartient à cette fin de produire l'avis qu'il a nécessairement reçu, ne démontre s'être vu remettre un avis inexact ou incomplet.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., en cas d'infraction constatée par radar automatique, CE, avis, 20 novembre 2009, M.,, n° 329982, p. 468 ; en cas d'interception du véhicule, CE, 8 juin 2011, Ministre de l'intérieur, de l'outre-mer et des collectivités territoriales c/ M.,, n° 348730, p. 283 ; CE, 11 juillet 2012, Ministre de l'intérieur, de l'outre-mer, des collectivités territoriales et de l'immigration c/ M.,, n° 349137, T. p. 884.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
