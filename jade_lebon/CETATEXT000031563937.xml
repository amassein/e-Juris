<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031563937</ID>
<ANCIEN_ID>JG_L_2015_12_000000386979</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/56/39/CETATEXT000031563937.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème / 3ème SSR, 02/12/2015, 386979</TITRE>
<DATE_DEC>2015-12-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>386979</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème / 3ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>DELAMARRE ; SCP TIFFREAU, MARLANGE, DE LA BURGADE</AVOCATS>
<RAPPORTEUR>Mme Esther de Moustier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Nathalie Escaut</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:386979.20151202</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Orange a demandé au juge des référés du tribunal administratif de Lyon de suspendre, sur le fondement de l'article L. 521-1 du code de justice administrative, l'exécution de la décision du 27 octobre 2014 par laquelle le directeur de l'Ecole centrale de Lyon a rejeté sa candidature à l'appel public à la concurrence pour la signature d'une convention d'occupation du domaine public en vue de la mise en place d'une station de téléphonie mobile, jusqu'à ce qu'il soit statué au fond sur la légalité de cette décision et d'enjoindre, sur le fondement de l'article L. 911-1 du même code, au directeur de l'Ecole centrale de Lyon de cesser la procédure engagée, de la déclarer sans suite ainsi que de ne pas signer la convention avec les attributaires. Par une ordonnance n° 1409455 du 23 décembre 2014, le juge des référés du tribunal administratif de Lyon a suspendu l'exécution de la décision du 27 octobre 2014 et rejeté le surplus des conclusions de la demande. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 7 et 22 janvier 2015 au secrétariat du contentieux du Conseil d'Etat, l'Ecole centrale de Lyon demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de rejeter la demande de la société Orange ;<br/>
<br/>
              3°) de mettre à la charge de la société Orange la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
              - le code des postes et des communications électroniques ;<br/>
              - le code général de la propriété des personnes publiques ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Esther de Moustier, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Nathalie Escaut, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Delamarre, avocat de l'Ecole centrale de Lyon et à la SCP Tiffreau, Marlange, de la Burgade, avocat de la société Orange ;<br/>
<br/>
<br/>
<br/>
              1. Considérant, d'une part, qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation (...), le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ; que lorsque l'acte administratif objet du litige n'est pas susceptible de recours, cette irrecevabilité affecte tant la demande d'annulation de cet acte que la demande tendant à sa suspension ; qu'en outre, lorsqu'elle ressort des pièces du dossier soumis au juge des référés, l'irrecevabilité du recours doit être relevée, le cas échéant d'office, tant par le juge des référés qu'éventuellement par le juge de cassation ;<br/>
<br/>
              2. Considérant, d'autre part, qu'aux termes de l'article L. 46 du code des postes et des communications électroniques : " Les autorités concessionnaires ou gestionnaires du domaine public non routier, lorsqu'elles donnent accès à des exploitants de réseaux de communications électroniques, doivent le faire sous la forme de convention, dans des conditions transparentes et non discriminatoires et dans toute la mesure où cette occupation n'est pas incompatible avec son affectation ou avec les capacités disponibles. (...) " ;  <br/>
<br/>
              3. Considérant que tout tiers à une convention d'occupation du domaine public conclue sur le fondement de ces dispositions, susceptible d'être lésé dans ses intérêts de façon suffisamment directe et certaine par sa passation ou ses clauses, est recevable à former, devant le juge du contrat, un recours de pleine juridiction contestant la validité du contrat ou de certaines de ses clauses non réglementaires qui en sont divisibles ; que la légalité du choix du cocontractant ne peut être contestée qu'à l'occasion d'un tel recours, exercé dans un délai de deux mois à compter de l'accomplissement des mesures de publicité appropriées, et qui peut éventuellement être assorti  d'une demande tendant, sur le fondement de l'article L. 521-1 du code de justice administrative, à la suspension de l'exécution du contrat ;<br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède que, lorsqu'une autorité gestionnaire du domaine public non routier décide de donner accès à ce domaine à des exploitants de réseaux de communications électroniques, mais choisit de limiter le nombre de conventions simultanément conclues à cet effet, la légalité de ce choix ainsi que celle du choix des cocontractants et celle du refus simultanément opposé à un autre exploitant de réseaux de communications électroniques ne peuvent être contestées, par ce dernier, que par un recours de pleine juridiction contestant la validité du contrat ; que le candidat évincé n'est, dès lors, pas recevable à former un recours pour excès de pouvoir contre la décision par laquelle le gestionnaire du domaine public n'a pas retenu sa candidature ;<br/>
<br/>
              5. Considérant qu'il ressort des pièces du dossier soumis au juge des référés du tribunal administratif de Lyon que la société Orange et l'Ecole centrale de Lyon ont conclu, le 24 novembre 1995, une convention d'occupation du domaine public relative à l'installation d'une station de téléphonie mobile sur l'un des bâtiments de l'école, renouvelée par avenants dont le dernier arrivait à échéance le 31 décembre 2014 ; que, pour assurer le renouvellement de cette convention à compter du 1er janvier 2015, le directeur de l'Ecole centrale de Lyon a ouvert, le 7 juillet 2014, une procédure de consultation en vue de l'implantation, sur le site de l'école, d'une ou deux antennes-relais, pour une durée de neuf ans ; qu'il a, le 27 octobre 2014, rejeté l'offre présentée par la société Orange dans le cadre de cette procédure d'appel public à la concurrence et signé deux conventions d'occupation avec, respectivement, les sociétés Bouygues Telecom et Free ; que l'Ecole centrale de Lyon se pourvoit en cassation contre l'ordonnance du 23 décembre 2014 par laquelle le juge des référés du tribunal administratif de Lyon a suspendu l'exécution de la décision du 27 octobre 2014 rejetant l'offre de la société Orange ; <br/>
<br/>
              6. Considérant qu'il résulte de ce qui a été dit au point 4 que la demande de la société Orange, qui contestait uniquement la décision du 27 octobre 2014 par laquelle le directeur de l'Ecole centrale de Lyon avait rejeté sa candidature à l'appel public à la concurrence pour la signature d'une convention d'occupation du domaine public sur le fondement de l'article L. 46 du code des postes et des communications électroniques, était irrecevable ; qu'est sans incidence, à cet égard, la circonstance que la lettre par laquelle ce rejet a été notifié à cette société mentionnait, de manière erronée, que cette décision était susceptible d'être contestée par la voie du recours pour excès de pouvoir ; qu'il ne résulte de cette circonstance, qui n'a pas pour effet de priver la société requérante de tout accès à un juge, aucune atteinte disproportionnée au droit d'accès à un tribunal garanti par l'article 6, paragraphe 1, de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
<br/>
              7. Considérant qu'en ne relevant pas l'irrecevabilité de la demande de la société Orange, le juge des référés du tribunal administratif de Lyon a commis une erreur de droit ; que, dès lors et sans qu'il soit besoin d'examiner les moyens du pourvoi, l'Ecole Centrale de Lyon est fondée à en demander l'annulation ; <br/>
<br/>
              8. Considérant qu'il y a lieu, en application des dispositions de l'article L. 821-2 du code de justice administrative, de régler l'affaire au titre de la procédure de référé engagée par la société Orange et de rejeter comme irrecevable sa demande tendant à la suspension de l'exécution de la décision du 27 octobre 2014 par laquelle le directeur de l'Ecole centrale de Lyon a rejeté sa candidature à l'appel public à la concurrence pour la signature d'une convention d'occupation du domaine public en vue de la mise en place d'une station de téléphonie mobile ;  <br/>
<br/>
              9. Considérant que la présente décision n'appelle aucune mesure d'exécution ; que, dès lors, les conclusions à fin d'injonction présentées par la société Orange ne peuvent qu'être rejetées ;<br/>
<br/>
              10. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Ecole centrale de Lyon qui n'est pas, dans la présente instance, la partie perdante ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par celle-ci au titre des ces dispositions ;  <br/>
<br/>
<br/>
<br/>                    D E C I D E :<br/>
                                   --------------<br/>
<br/>
Article 1er : L'ordonnance du juge des référés du tribunal administratif de Lyon du 23 décembre 2014 est annulée.<br/>
<br/>
Article 2 : La demande de la société Orange devant le juge des référés du tribunal administratif de Lyon et ses conclusions devant le Conseil d'Etat sont rejetées.  <br/>
<br/>
Article 3 : Les conclusions du pourvoi de l'Ecole centrale de Lyon présentée au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 4 : La présente décision sera notifiée à l'Ecole centrale de Lyon et à la société anonyme Orange. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">24-01-02-01-01-02 DOMAINE. DOMAINE PUBLIC. RÉGIME. OCCUPATION. UTILISATIONS PRIVATIVES DU DOMAINE. CONTRATS ET CONCESSIONS. - CONVENTION PERMETTANT L'ACCÈS DES EXPLOITANTS DE RÉSEAUX DE COMMUNICATIONS ÉLECTRONIQUES AU DOMAINE PUBLIC NON ROUTIER (ART. L. 46 DU CPCE) - 1) RECOURS DES TIERS - RÉGIME - 2) POSSIBILITÉ DE CONTESTER LE CHOIX DU COCONTRACTANT HORS D'UN TEL RECOURS - ABSENCE [RJ2] - 3) CONSÉQUENCE DANS LE CAS OÙ LE NOMBRE DE CONVENTIONS EST LIMITÉ - IRRECEVABILITÉ DU RECOURS EN EXCÈS DE POUVOIR DU CANDIDAT ÉVINCÉ CONTRE LA DÉCISION REFUSANT SA CANDIDATURE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">39-08-01-01 MARCHÉS ET CONTRATS ADMINISTRATIFS. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. RECEVABILITÉ. RECEVABILITÉ DU RECOURS POUR EXCÈS DE POUVOIR EN MATIÈRE CONTRACTUELLE. - CONVENTION PERMETTANT L'ACCÈS DES EXPLOITANTS DE RÉSEAUX DE COMMUNICATIONS ÉLECTRONIQUES AU DOMAINE PUBLIC NON ROUTIER (ART. L. 46 DU CPCE) - 1) RECOURS DES TIERS - RÉGIME - 2) POSSIBILITÉ DE CONTESTER LE CHOIX DU COCONTRACTANT HORS D'UN TEL RECOURS - ABSENCE [RJ2] - 3) CONSÉQUENCE DANS LE CAS OÙ LE NOMBRE DE CONVENTIONS EST LIMITÉ - IRRECEVABILITÉ DU RECOURS EN EXCÈS DE POUVOIR DU CANDIDAT ÉVINCÉ CONTRE LA DÉCISION REFUSANT SA CANDIDATURE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-035-02-02 PROCÉDURE. PROCÉDURES INSTITUÉES PAR LA LOI DU 30 JUIN 2000. RÉFÉRÉ SUSPENSION (ART. L. 521-1 DU CODE DE JUSTICE ADMINISTRATIVE). RECEVABILITÉ. - ACTE NON SUSCEPTIBLE DE RECOURS - IRRECEVABILITÉ AFFECTANT LA DEMANDE DE RÉFÉRÉ COMME LE RECOURS AU FOND [RJ1].
</SCT>
<ANA ID="9A"> 24-01-02-01-01-02 1) Tout tiers à une convention d'occupation du domaine public conclue sur le fondement de l'article L. 46 du code des postes et des communications électroniques (CPCE) (accès des exploitants de réseaux de communications électroniques au domaine public non routier), susceptible d'être lésé dans ses intérêts de façon suffisamment directe et certaine par sa passation ou ses clauses, est recevable à former, devant le juge du contrat, un recours de pleine juridiction contestant la validité du contrat ou de certaines de ses clauses non réglementaires qui en sont divisibles.,,,2) La légalité du choix du cocontractant ne peut être contestée qu'à l'occasion d'un tel recours, exercé dans un délai de deux mois à compter de l'accomplissement des mesures de publicité appropriées, et qui peut éventuellement être assorti d'une demande tendant, sur le fondement de l'article L. 521-1 du code de justice administrative, à la suspension de l'exécution du contrat.,,,3) Il résulte de ce qui précède que, lorsqu'une autorité gestionnaire du domaine public non routier décide de donner accès à ce domaine à des exploitants de réseaux de communications électroniques, mais choisit de limiter le nombre de conventions simultanément conclues à cet effet, la légalité de ce choix ainsi que celle du choix des cocontractants et celle du refus simultanément opposé à un autre exploitant de réseaux de communications électroniques ne peuvent être contestées, par ce dernier, que par un recours de pleine juridiction contestant la validité du contrat. Le candidat évincé n'est, dès lors, pas recevable à former un recours pour excès de pouvoir contre la décision par laquelle le gestionnaire du domaine public n'a pas retenu sa candidature.</ANA>
<ANA ID="9B"> 39-08-01-01 1) Tout tiers à une convention d'occupation du domaine public conclue sur le fondement de l'article L. 46 du code des postes et des communications électroniques (CPCE) (accès des exploitants de réseaux de communications électroniques au domaine public non routier), susceptible d'être lésé dans ses intérêts de façon suffisamment directe et certaine par sa passation ou ses clauses, est recevable à former, devant le juge du contrat, un recours de pleine juridiction contestant la validité du contrat ou de certaines de ses clauses non réglementaires qui en sont divisibles.,,,2) La légalité du choix du cocontractant ne peut être contestée qu'à l'occasion d'un tel recours, exercé dans un délai de deux mois à compter de l'accomplissement des mesures de publicité appropriées, et qui peut éventuellement être assorti d'une demande tendant, sur le fondement de l'article L. 521-1 du code de justice administrative, à la suspension de l'exécution du contrat.,,,3) Il résulte de ce qui précède que, lorsqu'une autorité gestionnaire du domaine public non routier décide de donner accès à ce domaine à des exploitants de réseaux de communications électroniques, mais choisit de limiter le nombre de conventions simultanément conclues à cet effet, la légalité de ce choix ainsi que celle du choix des cocontractants et celle du refus simultanément opposé à un autre exploitant de réseaux de communications électroniques ne peuvent être contestées, par ce dernier, que par un recours de pleine juridiction contestant la validité du contrat. Le candidat évincé n'est, dès lors, pas recevable à former un recours pour excès de pouvoir contre la décision par laquelle le gestionnaire du domaine public n'a pas retenu sa candidature.</ANA>
<ANA ID="9C"> 54-035-02-02 Lorsque l'acte administratif objet du litige n'est pas susceptible de recours, cette irrecevabilité affecte tant la demande d'annulation de cet acte que la demande tendant à sa suspension sur le fondement de l'article L. 521-1 du code de justice administrative. En outre, lorsqu'elle ressort des pièces du dossier soumis au juge des référés, l'irrecevabilité du recours doit être relevée, le cas échéant d'office, tant par le juge des référés qu'éventuellement par le juge de cassation.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 23 février 2011, Sté Chazal, n° 339826, T. p. 1077.,,[RJ2] Cf. CE, Assemblée, 4 avril 2014, Département de Tarn-et-Garonne, n° 358994, p. 70.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
