<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000021630627</ID>
<ANCIEN_ID>JG_L_2009_12_000000304379</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/21/63/06/CETATEXT000021630627.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème et 8ème sous-sections réunies, 30/12/2009, 304379</TITRE>
<DATE_DEC>2009-12-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>304379</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème et 8ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Vigouroux</PRESIDENT>
<AVOCATS>SCP PIWNICA, MOLINIE ; SCP COUTARD, MAYER, MUNIER-APAIRE</AVOCATS>
<RAPPORTEUR>M. Frédéric  Gueudar Delahaye</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CESSR:2009:304379.20091230</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu l'ordonnance du 28 mars 2007, enregistrée le 3 avril 2007 au secrétariat du contentieux du Conseil d'Etat, par laquelle le président de la cour administrative d'appel de Paris a transmis au Conseil d'État, en application de l'article R. 351-2 du code de justice administrative, le pourvoi présenté à cette cour par l'INSTITUT DE FRANCE ;<br/>
<br/>
              Vu le pourvoi, enregistré le 1er juin 2005 au greffe de la cour administrative d'appel de Paris et le mémoire complémentaire enregistré le 3 avril 2007 au secrétariat du contentieux du Conseil d'Etat, présentés par l'INSTITUT DE FRANCE dont le siège est 23 quai de Conti à Paris (75006) ; l'INSTITUT DE FRANCE demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement du 6 avril 2005 par lequel le tribunal administratif de Paris a, à la demande de Mme Anne-Marie A, annulé les décisions du 29 janvier 2002 par lesquelles le Chancelier de l'Institut a mis fin à ses fonctions de receveur des fondations et a chargé Mme Françoise B, à titre intérimaire, des fonctions d'agent comptable des fondations ;<br/>
<br/>
              2°) de mettre à la charge de Mme A la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi du 22 avril 1905 portant fixation du budget des dépenses et des recettes de l'exercice 1905 ;<br/>
<br/>
              Vu le décret du 11 juillet 1922 approuvant le règlement général de l'Institut ;<br/>
<br/>
              Vu le décret n° 62-1587 du 29 décembre 1962 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. François Delion, Maître des Requêtes,<br/>
<br/>
              - les observations de la SCP Coutard, Mayer, Munier-Apaire, avocat de l'INSTITUT DE FRANCE et de la SCP Piwnica, Molinié, avocat de Mme Anne-Marie A,<br/>
<br/>
              - les conclusions de M. Edouard Geffray, Rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la la SCP Coutard, Mayer, Munier-Apaire, avocat de l'INSTITUT DE FRANCE et à la SCP Piwnica, Molinié, avocat de Mme A,<br/>
<br/>
<br/>
<br/>Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que Mme A a été recrutée en 1987 comme agent contractuel de l'INSTITUT DE FRANCE, personne morale de droit public présentant le caractère d'un établissement public administratif ; qu'elle a été nommée receveur des fondations, poste correspondant à celui d'agent comptable, à compter du 1er janvier 1991 ; que, par des décisions du 29 décembre 2002, le chancelier de l'Institut a exécuté les décisions du même jour  par lesquelles la commission administrative centrale de l'Institut a , d'une part, relevé Mme A de ses fonctions, au motif qu'elle ne remplissait pas les conditions statutaires prévues par le nouveau règlement financier adopté par cette commission, et l'a affectée dans les services du conseil technique de l'Institut et, d'autre part, a nommé à sa place un receveur des fondations intérimaire, jusqu'à la nomination d'un receveur titulaire qui remplirait ces conditions ; que Mme A a contesté ces décisions devant le tribunal administratif de Paris ; que celui-ci, par un jugement du 6 avril 2005 les a annulées ; que l'INSTITUT DE FRANCE se pourvoit régulièrement en cassation contre ce jugement par des conclusions qui doivent être regardées comme tendant à l'annulation de son article 1er par lequel le tribunal administratif a annulé les décisions du 29 janvier 2002  mettant fin aux fonctions de Mme A, en qualité de receveur des fondations et chargeant Mme B, à titre intérimaire, des fonctions d'agent comptable des fondations ; <br/>
<br/>
              Sans qu'il soit besoin d'examiner les autres moyens du pourvoi ;<br/>
<br/>
              Considérant qu'aux termes des articles 152 et 157 du décret du 29 décembre 1962 portant règlement général sur la comptabilité publique, applicables aux établissements publics nationaux  " (...) les modalités particulières du fonctionnement financier et comptable des établissements publics nationaux sont fixées par le règlement de l'établissement. Ce règlement peut prévoir des dérogations aux règles de comptabilité publique fixées à la présente partie " et " sauf dans le cas prévu au deuxième alinéa de l'article 16 ci-dessus, l'agent comptable est nommé par arrêté conjoint du ministre des finances et du ministre de tutelle " ; qu'aux termes de l'article 16 de ce décret : " Les comptables publics sont nommés par le ministre des finances ou avec son agrément. / L'agrément peut résulter de l'accord donné par le ministre des finances au texte réglementaire en vertu duquel la nomination est prononcée. / L'acte de nomination est publié selon les règles propres à chaque catégorie de comptables publics " ; qu'aux termes de l'article 8 du règlement sur la comptabilité des fondations et l'administration financière de l'Institut de France, approuvé par l'arrêté du ministre de l'instruction publique et des beaux-arts et du ministre des finances du 4 août 1924 : " (...) le comptable est nommé par la commission administrative centrale. Le choix de la commission peut se porter sur un agent du Trésor ; dans ce cas, la nomination est subordonnée à l'agrément du ministre des finances " ; enfin qu'aux termes de l'article 12 du règlement général de l'Institut de France, dans sa rédaction résultant du décret du 16 novembre 1953 : " La commission nomme parmi ses membres tous les trois ans dans sa première séance de l'année, le chancelier de l'Institut chargé de l'administration et de l'exécution des décisions prises par la commission (...) " ;<br/>
<br/>
              Considérant qu'il résulte de ces dispositions que la commission administrative centrale avait compétence, en application des dispositions du règlement précité, pour décider, comme elle l'a fait lors de sa réunion du 29 janvier 2002, de mettre fin aux fonctions de Mme A au poste de receveur des fondations et de nommer, en remplacement de cette dernière, un autre agent contractuel et que, par ailleurs, le chancelier de l'INSTITUT DE FRANCE avait compétence pour prendre, pour l'exécution des décisions précitées de la commission administrative centrale, les décisions contestées ; qu'ainsi en jugeant que ces décisions avaient été incompétemment signées par le chancelier, le tribunal administratif a commis une erreur de droit ; que, dès lors, l'article 1er de son jugement doit être annulé ;<br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler dans cette mesure l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              Sur les conclusions tendant à l'annulation de la décision mettant fin aux fonctions de Mme A :<br/>
<br/>
              Sans qu'il soit besoin d'examiner les autres moyens de la demande ;<br/>
<br/>
              Considérant, d'une part, qu'aucune disposition réglementaire ne faisait obstacle au maintien de Mme A dans ses fonctions ; que l'agent nommé pour la remplacer au poste de receveur des fondations ne remplissait pas plus qu'elle les conditions mises en avant par l'Institut pour justifier une mutation dans l'intérêt du service ; qu'il ressort des pièces du dossier que la mutation imposée à Mme A par la décision contestée est motivée par le comportement de l'intéressée et que cette mutation a eu pour effet une réduction sensible de ses responsabilités comme de sa rémunération ; que la décision de mutation revêt ainsi le caractère d'une mesure prise en considération de la personne ;<br/>
<br/>
              Considérant, d'autre part, qu'en vertu des dispositions de l'article 65 de la loi du 22 avril 1905, un agent public faisant l'objet d'une mesure prise en considération de sa personne doit être mis à même de demander la communication de son dossier ; que,  s'il n'est pas contesté que Mme A avait été informée de l'intention de l'Institut de mettre en oeuvre par anticipation les dispositions du projet de règlement financier et de procéder au recrutement d'un fonctionnaire pour occuper les fonctions d'agent comptable et qu'il lui avait été suggéré  de  démissionner et de présenter sa candidature sur un autre poste, il ressort des pièces du dossier que la décision de la muter a été prise alors qu'elle était en congé de maladie et n'avait pas encore donné sa réponse ; que, dès lors, dans les circonstances de l'espèce, elle ne peut être regardée comme ayant été mise à même de demander en temps utile la communication de son dossier ;<br/>
<br/>
              Considérant qu'il résulte de ce qui précède que Mme A est fondée à soutenir que la décision du 29 janvier 2002 mettant fins à ses fonctions est illégale pour avoir été prononcée sans qu'elle ait été préalablement mise à même de consulter son dossier ; qu'elle doit dès lors être annulée ;<br/>
<br/>
              Sur les conclusions tendant à l'annulation de la décision nommant Mme B dans les fonctions d'agent comptable de l'Institut :<br/>
<br/>
              Considérant que, par suite de l'annulation de la décision ayant mis fin à ses fonctions, Mme A bénéficie, en exécution d'une telle annulation, d'un droit à réintégration dans l'emploi unique de receveur des fondations, agent comptable de l'Institut, dont elle a été écartée, sans que l'INSTITUT DE FRANCE puisse y faire obstacle en lui opposant la nomination de son successeur dans ces fonctions ; qu'il en résulte que Mme A ne justifie pas d'un intérêt lui donnant qualité pour demander l'annulation de la décision du 29 janvier 2002 nommant son successeur dans les fonctions d'agent comptable de l'Institut ; que les conclusions tendant à l'annulation de cette décision doivent, par suite, être rejetées ;<br/>
<br/>
              Sur les conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              Considérant qu'il n' y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par l'INSTITUT DE FRANCE au titre des dispositions de l'article L. 761-1 du code de justice administrative ; qu'il y a lieu, en revanche, de faire application des dispositions de l'article L. 761-1 du code de justice administrative et de mettre à la charge de l'INSTITUT DE FRANCE le versement à Mme A de la somme de 3 000 euros ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1 : L'article 1er du jugement du tribunal administratif de Paris du 6 avril 2005 est annulé.<br/>
Article 2 : La décision du 29 janvier 2002, par laquelle le Chancelier de l'INSTITUT DE FRANCE a mis fin aux fonctions de Mme A en qualité de receveur des fondations est annulée.<br/>
Article 3 : Les conclusions présentées par Mme A devant le tribunal administratif de Paris tendant à l'annulation de la décision du 29 janvier 2002 par laquelle le Chancelier de l'INSTITUT DE FRANCE a nommé Mme B dans les fonctions d'agent comptable de l'Institut sont rejetées.<br/>
Article 4 : Le surplus des conclusions du pourvoi de l'INSTITUT DE FRANCE est rejeté.<br/>
Article 5 : L'INSTITUT DE FRANCE versera à Mme A une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 6 : La présente décision sera notifiée à l'INSTITUT DE FRANCE et à Mme Anne-Marie A.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-05-01-02 FONCTIONNAIRES ET AGENTS PUBLICS. POSITIONS. AFFECTATION ET MUTATION. MUTATION. - DÉCISION PRISE EN CONSIDÉRATION DE LA PERSONNE - OBLIGATION DE METTRE L'AGENT À MÊME DE DEMANDER LA COMMUNICATION DE SON DOSSIER - AGENT À QUI L'ON A SUGGÉRÉ DE DÉMISSIONNER, ET QUI EST FINALEMENT MUTÉ PENDANT UN CONGÉ MALADIE - AGENT NE POUVANT ÊTRE REGARDÉ COMME AYANT ÉTÉ MIS À MÊME DE BÉNÉFICIER DE CETTE GARANTIE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">36-07-07 FONCTIONNAIRES ET AGENTS PUBLICS. STATUTS, DROITS, OBLIGATIONS ET GARANTIES. COMMUNICATION DU DOSSIER. - DÉCISION DE MUTATION - DÉCISION PRISE EN CONSIDÉRATION DE LA PERSONNE - CAS D'UN AGENT À QUI L'ON A SUGGÉRÉ DE DÉMISSIONNER, ET QUI EST FINALEMENT MUTÉ PENDANT UN CONGÉ MALADIE - AGENT NE POUVANT ÊTRE REGARDÉ COMME AYANT ÉTÉ MIS À MÊME DE BÉNÉFICIER DE CETTE GARANTIE [RJ1].
</SCT>
<ANA ID="9A"> 36-05-01-02 Mutation imposée à l'agent, motivée par son comportement et ayant pour effet une réduction sensible de ses responsabilités comme de sa rémunération. Une telle décision de mutation revêt le caractère d'une mesure prise en considération de la personne. Par suite, en vertu des dispositions de l'article 65 de la loi du 22 avril 1905 portant fixation du budget des dépenses et des recettes de l'exercice 1905, cet agent doit être mis à même de demander la communication de son dossier. Or, en l'espèce, s'il avait été suggéré à l'agent de démissionner et de présenter sa candidature sur un autre poste, la décision de mutation a été prise alors que l'agent était en congé de maladie et n'avait pas encore donné sa réponse. Dans ces conditions, l'agent ne peut être regardé comme ayant été mis à même de demander en temps utile la communication de son dossier.</ANA>
<ANA ID="9B"> 36-07-07 Mutation imposée à l'agent, motivée par son comportement et ayant pour effet une réduction sensible de ses responsabilités comme de sa rémunération. Une telle décision de mutation revêt le caractère d'une mesure prise en considération de la personne. Par suite, en vertu des dispositions de l'article 65 de la loi du 22 avril 1905 portant fixation du budget des dépenses et des recettes de l'exercice 1905, cet agent doit être mis à même de demander la communication de son dossier. Or, en l'espèce, s'il avait été suggéré à l'agent de démissionner et de présenter sa candidature sur un autre poste, la décision de mutation a été prise alors que l'agent était en congé de maladie et n'avait pas encore donné sa réponse. Dans ces conditions, l'agent ne peut être regardé comme ayant été mis à même de demander en temps utile la communication de son dossier.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Comp. Assemblée plénière, 5 juin 1959, Sieur Dufay, n°s 5598 5599, p. 345.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
