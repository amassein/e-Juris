<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034330330</ID>
<ANCIEN_ID>JG_L_2017_03_000000391901</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/33/03/CETATEXT000034330330.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 15/03/2017, 391901</TITRE>
<DATE_DEC>2017-03-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>391901</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS</AVOCATS>
<RAPPORTEUR>M. Laurent Domingo</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:391901.20170315</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Galaxie Vendôme a demandé au tribunal administratif de Paris d'annuler pour excès de pouvoir l'arrêté du préfet de police du 5 juillet 2013 accordant un permis de stationnement à la société Bouygues Bâtiment Ile-de-France pour l'installation d'un cantonnement de chantier le long de la façade de l'hôtel Le Ritz, situé place Vendôme à Paris (75001), pour la période du 17 juin 2013 au 30 décembre 2014. Par un jugement n° 1312705 du 27 mars 2014, le tribunal administratif de Paris a fait droit à cette demande.<br/>
<br/>
              Par un arrêt n° 14PA02140 du 20 mai 2015, la cour administrative d'appel de Paris a rejeté l'appel formé par le préfet de police contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 20 juillet et 20 octobre 2015 au secrétariat du contentieux du Conseil d'Etat, le préfet de police, agissant au nom de la ville de Paris, demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de la société Galaxie Vendôme une somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la voirie routière ;<br/>
              - le code général des collectivités territoriales ;<br/>
              - le code général de la propriété des personnes publiques ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Laurent Domingo, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat du préfet de police.<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, dans le cadre des travaux de rénovation de l'hôtel Le Ritz, situé 15-17, place Vendôme à Paris, le préfet de police agissant au nom de la ville de Paris a, par un arrêté du 5 juillet 2013, accordé à la société The Ritz hotel Limited un permis de stationnement autorisant la société Bouygues Bâtiment Ile-de-France à édifier, le long de la façade de l'hôtel, un cantonnement de chantier comportant quatre-vingt quinze bungalows répartis sur cinq niveaux pour la période du 17 juin 2013 au 30 décembre 2014. Par un jugement du 27 mars 2014, le tribunal administratif de Paris a annulé cet arrêté. La cour administrative d'appel de Paris a rejeté l'appel du préfet de police dirigé contre ce jugement par un arrêt du 20 mai 2015. Le préfet de police, agissant au nom de la ville de Paris, se pourvoit en cassation contre cet arrêt.<br/>
<br/>
              2. En vertu de l'article L. 113-2 du code de la voirie routière : " En dehors des cas prévus aux articles L. 113-3 à L. 113-7 et de l'installation par l'Etat des équipements visant à améliorer la sécurité routière, l'occupation du domaine public routier n'est autorisée que si elle a fait l'objet, soit d'une permission de voirie dans le cas où elle donne lieu à emprise, soit d'un permis de stationnement dans les autres cas. Ces autorisations sont délivrées à titre précaire et révocable ". Pour l'application de ces dispositions, l'emprise sur le domaine public routier consiste en une modification de l'assiette du domaine occupé. <br/>
<br/>
              3. La cour a relevé, par une appréciation souveraine des faits non arguée de dénaturation, que l'installation du cantonnement de chantier le long de la façade de l'hôtel Le Ritz avait donné lieu à d'importants travaux de préparation, consistant notamment dans le coulage d'une dalle de béton isolée de la chaussée pavée par une feuille de polyane et sur laquelle avaient été implantés des piliers en béton, des poutres métalliques, des rails et des palissades. En jugeant que l'installation du cantonnement de chantier le long de la façade de l'hôtel Le Ritz devait être regardée, eu égard aux caractéristiques de construction, l'ampleur et la durée de cette installation, comme comportant une emprise sur le domaine public, alors qu'elle n'avait pas relevé de modification de l'assiette du domaine public, la cour a commis une erreur de droit. Par suite, le préfet de police est fondé à demander l'annulation de l'arrêt qu'il attaque.<br/>
<br/>
              4. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              5. Il ne ressort pas des pièces du dossier que l'installation du cantonnement de chantier le long de la façade de l'hôtel Le Ritz ait modifié l'assiette du domaine public routier. Dès lors, en l'absence d'emprise, l'occupation de ce domaine ne pouvait être autorisée, en application des dispositions précitées de l'article L. 113-2 du code de la voirie routière, que par un permis de stationnement et non par une permission de voirie.<br/>
<br/>
              6. Or, aux termes de l'article L. 2512-14 du code général des collectivités territoriales, dans sa version alors applicable et dont la teneur est reprise par l'actuelle rédaction de cet article : " Les pouvoirs conférés au maire par le premier alinéa de l'article L. 2213-1 et par les articles L. 2213-2 à L. 2213-6 sont, à Paris, exercés par le maire de Paris sous réserve des dispositions ci-après. / Pour les motifs d'ordre public ou liés à la sécurité des personnes et des biens ou pour assurer la protection du siège des institutions de la République et des représentations diplomatiques, le préfet de police détermine, de façon permanente ou temporaire, des sites où il réglemente les conditions de circulation et de stationnement dans certaines voies ou portions de voies, ou en réserve l'accès à certaines catégories d'usagers ou de véhicules (...) ". Aux termes de l'article L. 2213-6 du même code : " Le maire peut, moyennant le paiement de droits fixés par un tarif dûment établi, donner des permis de stationnement ou de dépôt temporaire sur la voie publique et autres lieux publics, sous réserve que cette autorisation n'entraîne aucune gêne pour la circulation et la liberté du commerce ". <br/>
<br/>
              7. En conférant ainsi au maire de Paris les pouvoirs de police en matière de circulation et de stationnement reconnus aux maires dans les communes par le premier alinéa de l'article L. 2213-1 et par les articles L. 2213-2 à L. 2213-6 du code général des collectivités territoriales, le premier alinéa de l'article L. 2512-14 précité lui a attribué la compétence pour délivrer les permis de stationnement en application de l'article L. 2213-6 du même code. Si le deuxième alinéa de l'article L. 2512-14 permet au préfet de police, à l'intérieur des périmètres des sites qu'il a déterminés, d'adopter des mesures de réglementation des conditions de circulation et de stationnement dans certaines voies ou portions de voies ou d'en réserver l'accès à certaines catégories d'usagers ou de véhicules, cette disposition ne lui attribue pas, en revanche, de compétence pour délivrer, à l'intérieur de ces périmètres, les permis individuels de stationnement prévus à l'article L. 2213-6. <br/>
<br/>
              8. Par suite, s'il est constant que le cantonnement de chantier le long de la façade de l'hôtel Le Ritz devait être édifié sur une voie comprise à l'intérieur du périmètre correspondant au site du ministère de la justice défini par l'arrêté du préfet de police du 6 mai 2002 relatif " aux sites énoncés au second alinéa de l'article L. 2512-14 du code général des collectivités territoriales ", seul le maire de Paris était compétent pour délivrer le permis de stationnement sollicité par la société The Ritz hotel Limited. <br/>
<br/>
              9. Il résulte de ce qui précède que le préfet de police, agissant au nom de la ville de Paris, n'est pas fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif a annulé son arrêté du 5 juillet 2013 par lequel il a accordé à la société The Ritz Hotel Limited un permis de stationnement autorisant la société Bouygues Bâtiment Ile-de-France à édifier un cantonnement de chantier le long de la façade de l'hôtel Le Ritz.<br/>
<br/>
              10. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la société Galaxie Vendôme qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Paris du 20 mai 2015 est annulé.<br/>
Article 2 : La requête présentée par le préfet de police devant la cour administrative d'appel de Paris est rejetée.<br/>
Article 3 : Les conclusions présentées par la ville de Paris, représentée par le préfet de police, au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à la ville de Paris, représentée par le préfet de police, à la société Galaxie Vendôme, à la société The Ritz Hotel Limited et à la société Bouygues Bâtiment Ile-de-France. <br/>
Copie en sera adressée au ministre de l'intérieur et à la ville de Paris, représentée par son maire.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">135-02-03-02-04-02-04 COLLECTIVITÉS TERRITORIALES. COMMUNE. ATTRIBUTIONS. POLICE. POLICE DE LA CIRCULATION ET DU STATIONNEMENT. RÉGLEMENTATION DU STATIONNEMENT. PERMIS DE STATIONNEMENT. - AUTORITÉ COMPÉTENTE POUR DÉLIVRER UN PERMIS DE STATIONNEMENT (ART. L. 2213-6 DU CGCT) - CAS DE LA VILLE DE PARIS - 1) PRÉFET DE POLICE - ABSENCE - MAIRE DE PARIS - EXISTENCE - 2) ESPÈCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">135-06-01-01-02 COLLECTIVITÉS TERRITORIALES. DISPOSITIONS PARTICULIÈRES À CERTAINES COLLECTIVITÉS. COLLECTIVITÉS DE LA RÉGION ILE-DE-FRANCE. DISPOSITIONS PARTICULIÈRES À PARIS. MAIRE DE PARIS. - COMPÉTENCE POUR DÉLIVRER UN PERMIS DE STATIONNEMENT (ART. L. 2213-6 DU CGCT) - 1) PRÉFET DE POLICE - ABSENCE - MAIRE DE PARIS - EXISTENCE - 2) ESPÈCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">24-01-02-01-01-01 DOMAINE. DOMAINE PUBLIC. RÉGIME. OCCUPATION. UTILISATIONS PRIVATIVES DU DOMAINE. AUTORISATIONS UNILATÉRALES. - OCCUPATION DU DOMAINE PUBLIC ROUTIER - 1) PERMISSION DE VOIRIE - CAS OÙ L'OCCUPATION DU DOMAINE PUBLIC DONNE LIEU À EMPRISE (ART. L. 113-2 DU CODE DE LA VOIRIE ROUTIÈRE) - NOTION D'EMPRISE - MODIFICATION DE L'ASSIETTE DU DOMAINE PUBLIC [RJ1] - 2) PERMIS DE STATIONNEMENT (ART. L. 2213-6 DU CGCT) - A) AUTORITÉ COMPÉTENTE POUR DÉLIVRER UN TEL PERMIS À PARIS - MAIRE DE PARIS - B) ESPÈCE.
</SCT>
<ANA ID="9A"> 135-02-03-02-04-02-04 1) En conférant au maire de Paris les pouvoirs de police en matière de circulation et de stationnement reconnus aux maires dans les communes par le premier alinéa de l'article L. 2213-1 et par les articles L. 2213-2 à L. 2213-6 du code général des collectivités territoriales (CGCT), le premier alinéa de l'article L. 2512-14 lui a attribué la compétence pour délivrer les permis de stationnement en application de l'article L. 2213-6 du même code. Si le deuxième alinéa de l'article L. 2512-14 permet au préfet de police, à l'intérieur des périmètres des sites qu'il a déterminés, d'adopter des mesures de réglementation des conditions de circulation et de stationnement dans certaines voies ou portions de voies ou d'en réserver l'accès à certaines catégories d'usagers ou de véhicules, cette disposition ne lui attribue pas, en revanche, de compétence pour délivrer les permis individuels de stationnement prévus à l'article L. 2213-6 à l'intérieur de ces périmètres.... ,,2) Par suite, s'il est constant que le cantonnement de chantier le long de la façade de l'hôtel Le Ritz devait être édifié sur une voie comprise à l'intérieur du périmètre correspondant au site du ministère de la justice défini par l'arrêté du préfet de police du 6 mai 2002 relatif aux sites énoncés au second alinéa de l'article L. 2512-14 du code général des collectivités territoriales, seul le maire de Paris était compétent pour délivrer le permis de stationnement sollicité par la société The Ritz hotel Limited.</ANA>
<ANA ID="9B"> 135-06-01-01-02 1) En conférant au maire de Paris les pouvoirs de police en matière de circulation et de stationnement reconnus aux maires dans les communes par le premier alinéa de l'article L. 2213-1 et par les articles L. 2213-2 à L. 2213-6 du code général des collectivités territoriales (CGCT), le premier alinéa de l'article L. 2512-14 lui a attribué la compétence pour délivrer les permis de stationnement en application de l'article L. 2213-6 du même code. Si le deuxième alinéa de l'article L. 2512-14 permet au préfet de police, à l'intérieur des périmètres des sites qu'il a déterminés, d'adopter des mesures de réglementation des conditions de circulation et de stationnement dans certaines voies ou portions de voies ou d'en réserver l'accès à certaines catégories d'usagers ou de véhicules, cette disposition ne lui attribue pas, en revanche, de compétence pour délivrer les permis individuels de stationnement prévus à l'article L. 2213-6 à l'intérieur de ces périmètres.... ,,2) Par suite, s'il est constant que le cantonnement de chantier le long de la façade de l'hôtel Le Ritz devait être édifié sur une voie comprise à l'intérieur du périmètre correspondant au site du ministère de la justice défini par l'arrêté du préfet de police du 6 mai 2002 relatif aux sites énoncés au second alinéa de l'article L. 2512-14 du code général des collectivités territoriales, seul le maire de Paris était compétent pour délivrer le permis de stationnement sollicité par la société The Ritz hotel Limited.</ANA>
<ANA ID="9C"> 24-01-02-01-01-01 1) Pour l'application des dispositions de l'article L. 113-2 du code la voirie routière, l'emprise sur le domaine public routier consiste en une modification de l'assiette du domaine occupé. Erreur de droit commise par une cour à avoir jugé que l'installation d'un cantonnement de chantier le long de la façade de l'hôtel Le Ritz devait être regardée, eu égard aux caractéristiques de construction, l'ampleur et la durée de cette installation, comme comportant une emprise sur le domaine public, alors qu'elle n'avait pas relevé de modification de l'assiette du domaine public.... ,,2) a) En conférant au maire de Paris les pouvoirs de police en matière de circulation et de stationnement reconnus aux maires dans les communes par le premier alinéa de l'article L. 2213-1 et par les articles L. 2213-2 à L. 2213-6 du code général des collectivités territoriales (CGCT), le premier alinéa de l'article L. 2512-14 lui a attribué la compétence pour délivrer les permis de stationnement en application de l'article L. 2213-6 du même code. Si le deuxième alinéa de l'article L. 2512-14 permet au préfet de police, à l'intérieur des périmètres des sites qu'il a déterminés, d'adopter des mesures de réglementation des conditions de circulation et de stationnement dans certaines voies ou portions de voies ou d'en réserver l'accès à certaines catégories d'usagers ou de véhicules, cette disposition ne lui attribue pas, en revanche, de compétence pour délivrer les permis individuels de stationnement prévus à l'article L. 2213-6 à l'intérieur de ces périmètres.... ,,b) Par suite, s'il est constant que le cantonnement de chantier le long de la façade de l'hôtel Le Ritz devait être édifié sur une voie comprise à l'intérieur du périmètre correspondant au site du ministère de la justice défini par l'arrêté du préfet de police du 6 mai 2002 relatif aux sites énoncés au second alinéa de l'article L. 2512-14 du code général des collectivités territoriales, seul le maire de Paris était compétent pour délivrer le permis de stationnement sollicité par la société The Ritz hotel Limited.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 15 juillet 1964, Sieur Longuefosse, n° 61100, p. 423 ; CE, 9 avril 2014, Domaine national de Chambord, n° 366483, aux Tables sur un autre point.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
