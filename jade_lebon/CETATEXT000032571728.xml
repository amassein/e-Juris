<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032571728</ID>
<ANCIEN_ID>JG_L_2016_05_000000387144</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/57/17/CETATEXT000032571728.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 20/05/2016, 387144</TITRE>
<DATE_DEC>2016-05-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>387144</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUIN-PALAT, BOUCARD</AVOCATS>
<RAPPORTEUR>M. Pierre Lombard</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:387144.20160520</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une décision du 19 juin 2012, prise en sa qualité de directeur de la publication du magazine d'information municipale " Votre Ville ", le maire de Chartres a refusé de publier dans le numéro des mois de juillet-août 2012 la tribune intitulée " La ligne jaune ", rédigée par M. A...B...et Mme D..., conseillers municipaux d'opposition ; <br/>
<br/>
              Par un jugement n° 1202304 du 8 novembre 2012, le tribunal administratif d'Orléans, saisi par M. B...et de Mme C..., a, d'une part, annulé la décision du maire de Chartres du 19 juin 2012 et, d'autre part, lui a enjoint de publier cette tribune dans la partie " tribunes de l'opposition " du numéro du bulletin municipal suivant la notification du jugement.<br/>
<br/>
              La commune de Chartres a relevé appel de ce jugement. Par un arrêt n° 13NT00014 du 14 novembre 2014, la cour administrative d'appel de Nantes a rejeté sa requête.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 15 janvier et 15 avril 2015 au secrétariat du contentieux du Conseil d'Etat, la commune de Chartres demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de M. B...et de Mme C...la somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des collectivités territoriales ;<br/>
              - la loi du 29 juillet 1881 relative à la liberté de la presse ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Pierre Lombard, auditeur,  <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Thouin-Palat, Boucard, avocat de la commune de Chartres  ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que l'article L. 2121-27-1 du code général des collectivités territoriales dispose : " Dans les communes de 3 500 habitants et plus, lorsque la commune diffuse, sous quelque forme que ce soit, un bulletin d'information générale sur les réalisations et la gestion du conseil municipal, un espace est réservé à l'expression des conseillers n'appartenant pas à la majorité municipale " ; que l'article 29 de la loi du 29 juillet 1881 sur la liberté de la presse dispose : " Toute allégation ou imputation d'un fait qui porte atteinte à l'honneur ou à la considération de la personne ou du corps auquel le fait est imputé est une diffamation. La publication directe ou par voie de reproduction de cette allégation ou de cette imputation est punissable, même si elle est faite sous forme dubitative ou si elle vise une personne ou un corps non expressément nommés, mais dont l'identification est rendue possible par les termes des discours, cris, menaces, écrits ou imprimés, placards ou affiches incriminés. / Toute expression outrageante, termes de mépris ou invective qui ne renferme l'imputation d'aucun fait est une injure " ; que l'article 42 de cette loi dispose : " Seront passibles, comme auteurs principaux des peines qui constituent la répression des crimes et délits commis par la voie de la presse, dans l'ordre ci-après, savoir : / 1° Les directeurs de publications ou éditeurs, quelles que soient leurs professions ou leurs dénominations (...) " ;<br/>
<br/>
              2. Considérant qu'il résulte des dispositions de l'article L. 2121-27-1 du code général des collectivités territoriales qu'une commune de 3 500 habitants et plus est tenue de réserver dans son bulletin d'information municipale, lorsqu'elle diffuse un tel bulletin, un espace d'expression réservé à l'opposition municipale ; que ni le conseil municipal ni le maire de la commune ne sauraient, en principe, contrôler le contenu des articles publiés, sous la responsabilité de leurs auteurs, dans cet espace ; qu'il en va toutefois autrement lorsqu'il ressort à l'évidence de son contenu qu'un tel article est de nature à engager la responsabilité pénale du directeur de la publication, notamment s'il présente un caractère manifestement outrageant, diffamatoire ou injurieux de nature à engager la responsabilité du maire, directeur de publication du bulletin municipal, sur le fondement des dispositions précitées de la loi du 29 juillet 1881 ;<br/>
<br/>
              3. Considérant, en premier lieu, qu'il résulte de ce qui précède que c'est sans commettre  d'erreur de droit que la cour administrative d'appel de Nantes a jugé que le motif tiré de ce que la tribune intitulée " La ligne jaune ", émanant de conseillers municipaux d'opposition, n'était pas en rapport direct avec les affaires de la commune de Chartres mais avait trait à un problème de politique nationale n'était pas au nombre des motifs qui pouvaient légalement justifier la décision du maire de Chartres de s'opposer à sa publication dans l'espace d'expression réservé à l'opposition municipale du bulletin d'information municipal "Votre Ville" des mois de juillet-août 2012 ;<br/>
<br/>
              4. Considérant, en second lieu, qu'il ressort des pièces du dossier soumis aux juges du fond que les auteurs de la tribune en cause y dénonçaient les conditions dans lesquelles le maire de Chartres aurait obtenu sa réélection à l'Assemblée nationale et faisaient part de leur crainte de voir des élus appartenant au Front national intégrer la prochaine équipe municipale ; que si cette tribune est rédigée sur un ton vif et polémique, la cour administrative d'appel de Nantes n'a pas inexactement qualifié les faits en jugeant qu'elle ne saurait pour autant être regardée comme présentant manifestement un caractère diffamatoire ou outrageant de nature à justifier qu'il soit fait obstacle au droit d'expression d'élus n'appartenant pas à la majorité municipale ;<br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que le pourvoi de la commune de Chartres doit être rejeté ; que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que à ce qu'une somme soit mise à la charge de M. B...et de Mme C..., qui ne sont pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la commune de Chartres est rejeté.<br/>
Article 2 : La présente décision sera notifiée à la commune de Chartres, à M. A...B...et à MmeD....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">135-02-01 COLLECTIVITÉS TERRITORIALES. COMMUNE. ORGANISATION DE LA COMMUNE. - BULLETIN MUNICIPAL D'INFORMATION DE LA COMMUNE (ART. L. 2121-27 DU CGCT) - ESPACE RÉSERVÉ À L'OPPOSITION - POSSIBILITÉ DU MAIRE OU DU CONSEIL MUNICIPAL DE S'OPPOSER À UN ARTICLE - ABSENCE - EXCEPTION - INFRACTIONS DE PRESSE. [RJ1].
</SCT>
<ANA ID="9A"> 135-02-01 Il résulte des dispositions de l'article L. 2121-27-1 du code général des collectivités territoriales (CGCT) qu'une commune de 3 500 habitants et plus est tenue de réserver dans son bulletin d'information municipale, lorsqu'elle diffuse un tel bulletin, un espace d'expression réservé à l'opposition municipale. Ni le conseil municipal ni le maire de la commune ne sauraient, en principe, contrôler le contenu des articles publiés, sous la responsabilité de leurs auteurs, dans cet espace. Il en va toutefois autrement lorsqu'il ressort manifestement de son contenu qu'un tel article est de nature à engager la responsabilité pénale du directeur de la publication, notamment s'il présente un caractère outrageant, diffamatoire ou injurieux de nature à engager la responsabilité du maire, directeur de publication du bulletin municipal, sur le fondement des dispositions de la loi du 29 juillet 1881.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 7 mai 2012, Elections cantonales de Saint-Cloud, n° 353536, p. 190.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
