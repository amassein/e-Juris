<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028052013</ID>
<ANCIEN_ID>JG_L_2013_10_000000359161</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/05/20/CETATEXT000028052013.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème et 10ème sous-sections réunies, 09/10/2013, 359161</TITRE>
<DATE_DEC>2013-10-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>359161</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème et 10ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>Mme Maïlys Lange</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Aladjidi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:359161.20131009</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 4 mai 2012 au secrétariat du contentieux du Conseil d'Etat, présentée par la société d'exercice libéral à forme anonyme MJA (Mandataires Judiciaires Associés), agissant en qualité de liquidateur judiciaire de l'Union nationale des mutuelles retraite des instituteurs et fonctionnaires de l'Education nationale et de la fonction publique, dite Mutuelle retraite de la fonction publique (MRFP), dont le siège est situé 102, rue du Faubourg Saint Denis à Paris (75010) ; la SELAFA demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 2 mars 2012 par laquelle le vice-président de l'Autorité de contrôle prudentiel (ACP) a rejeté sa demande tendant à mettre en demeure l'Union mutuelle retraites (UMR), en sa qualité de bénéficiaire du transfert du portefeuille des contrats liés à l'ancien CREF, de garantir l'exécution des condamnations financières prononcées contre la MRFP par un arrêt de la cour d'appel de Paris du 29 avril 2011 ;<br/>
<br/>
              2°) d'enjoindre à l'ACP de procéder à cette mise en demeure dans un délai d'un mois, sous astreinte de 500 euros par jour de retard au-delà de ce délai ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros sur le fondement de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 24 septembre 2013, présentée par la SELAFA MJA ;<br/>
<br/>
              Vu le code monétaire et financier ;<br/>
<br/>
              Vu le code de la mutualité ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Maïlys Lange, Auditeur,  <br/>
<br/>
              - les conclusions de M. Frédéric Aladjidi, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de l'Autorité de contrôle prudentiel ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 612-1 du code monétaire et financier, sans sa rédaction alors en vigueur : " I. - L'Autorité de contrôle prudentiel, autorité administrative indépendante, veille à la préservation de la stabilité du système financier et à la protection des clients, assurés, adhérents et bénéficiaires des personnes soumises à son contrôle. L'Autorité contrôle le respect par ces personnes des dispositions du code monétaire et financier ainsi que (...) du code de la mutualité (...). II.-Elle est chargée : (...) 2° D'exercer une surveillance permanente de la situation financière et des conditions d'exploitation des personnes mentionnées au I de l'article L. 612-2 ; elle contrôle notamment le respect de leurs exigences de solvabilité ainsi que, pour les personnes mentionnées (...) aux 1° à 3° (...) du B du I du même article, qu'elles sont en mesure de tenir à tout moment les engagements qu'elles ont pris envers leurs assurés, adhérents, bénéficiaires ou entreprises réassurées et les tiennent effectivement ; 3° De veiller au respect par les personnes soumises à son contrôle des règles destinées à assurer la protection de leur clientèle, résultant notamment de toute disposition législative et réglementaire (...). Pour l'accomplissement de ses missions, l'Autorité de contrôle prudentiel dispose, à l'égard des personnes mentionnées à l'article L. 612-2, d'un pouvoir de contrôle, du pouvoir de prendre des mesures de police administrative et d'un pouvoir de sanction. " ; qu'aux termes de l'article L. 612-2 du même code : " I. - Relèvent de la compétence de l'Autorité de contrôle prudentiel : (...) B. - Dans le secteur de l'assurance : (...) 3° Les mutuelles et unions régies par le livre II du code de la mutualité (...) " ; qu'aux termes de l'article L. 612-31 : " L'Autorité de contrôle prudentiel peut mettre en demeure toute personne soumise à son contrôle de prendre, dans un délai déterminé, toutes mesures destinées à sa mise en conformité avec les obligations au respect desquelles l'Autorité de contrôle prudentiel a pour mission de veiller. " ; qu'enfin il résulte de l'article L. 212-11 du code de la mutualité que les mutuelles et unions peuvent être autorisées " à transférer tout ou partie de leur portefeuille d'opérations, avec ses droits et obligations (...). Dans tous les cas, le nouvel assureur doit respecter les garanties concernant les activités transférées, telles que la mutuelle ou l'union les avaient établies. " ;<br/>
<br/>
              2. Considérant qu'à la suite des difficultés rencontrées par l'Union nationale des mutuelles retraite des instituteurs et fonctionnaires de l'Education nationale et de la fonction publique, dite Mutuelle retraite de la fonction publique (MRFP), trois assemblées générales extraordinaires de cette union mutualiste en date des 30 octobre 2000, 8 décembre 2001, et 11 et 12 avril 2002 ont décidé, d'une part, de baisser significativement la valeur de service du point acquis par les adhérents dans le cadre du contrat Complément retraite de la fonction publique, dit contrat CREF, proposé par la MRFP, d'autre part, de convertir le CREF en deux régimes distincts de retraite complémentaire, l'un fermé aux nouvelles adhésions reprenant la gestion des droits et obligations nés des cotisations versées avant le 31 décembre 1988, l'autre reprenant la gestion des droits et obligations nés des cotisations versées à compter du 1er janvier 1989 et des cotisations qui seraient versées à l'avenir, enfin, de transférer ce portefeuille de contrats ainsi convertis à une nouvelle union, l'Union mutualiste retraite (UMR) ; que ce transfert a été approuvé par un arrêté ministériel du 23 décembre 2002 ; <br/>
<br/>
              3. Considérant que, saisie par un certain nombre d'anciens adhérents de la MRFP d'un recours dirigé contre ces délibérations et, subsidiairement, d'une demande de condamnation de la MRFP à les indemniser du préjudice qu'ils estimaient avoir subi du fait des difficultés de cette union mutualiste, la cour d'appel de Paris a jugé, par un arrêt du 29 avril 2011, devenu irrévocable à la suite de l'arrêt de la Cour de cassation du 14 novembre 2012, que la MRFP avait manqué à son devoir d'information et de conseil et l'a condamnée à verser à ces adhérents une somme globale d'environ 5,5 millions d'euros ; que, par le même arrêt, la cour a en revanche débouté les requérants de leurs demandes tendant à l'annulation des délibérations contestées et à la condamnation solidaire de l'UMR ; qu'elle a en effet estimé, par adoption des motifs des premiers juges, d'une part, que l'UMR n'était pas intervenue dans la gestion du CREF, d'autre part, que les garanties que l'UMR devait respecter concernant les activités transférées, en application des dispositions de l'article L. 212-11 du code de la mutualité, n'étaient pas celles prévues par les contrats d'origine proposés par la MRFP, mais celles résultant des contrats modifiés par les assemblées générales extraordinaires litigieuses de cette union mutualiste ; qu'à la suite de cet arrêt, un jugement du tribunal de grande instance de Paris du 7 juillet 2011 a constaté la cessation des paiements de la MRFP, ouvert une procédure de liquidation judiciaire à son égard et désigné la société d'exercice libéral à forme anonyme Mandataires Judiciaires Associés (SELAFA MJA) en qualité de liquidateur ; que, par lettre du 12 janvier 2012, cette dernière a demandé à l'Autorité de contrôle prudentiel (ACP), sur le fondement de l'article L. 612-31 du code monétaire et financier, de mettre en demeure l'UMR, en sa qualité de bénéficiaire du transfert du portefeuille de contrats liés au CREF, de garantir l'exécution des condamnations prononcées contre la MRFP par l'arrêt de la cour d'appel de Paris ; que la SELAFA demande l'annulation de la décision du 2 mars 2012 par laquelle le vice-président de l'ACP a refusé de faire droit à cette demande  ;<br/>
<br/>
              Sur la fin de non-recevoir opposée par l'Autorité de contrôle prudentiel : <br/>
<br/>
              4. Considérant qu'il appartient à une autorité administrative indépendante investie d'une mission de régulation, qui dispose en vertu de la loi de pouvoirs de contrôle et de police, qu'elle exerce de sa propre initiative et dont l'objet consiste à assurer la sécurité d'un marché, de procéder, lorsqu'elle est saisie d'une demande tendant à la mise en oeuvre de ces pouvoirs, à l'examen des faits qui en sont à l'origine et de décider des suites à leur donner ; qu'elle dispose, à cet effet, d'un large pouvoir d'appréciation et peut tenir compte de l'ensemble des intérêts généraux dont elle a la charge ; que la décision qu'elle prend, lorsqu'elle refuse de donner suite à la demande, peut être déférée au juge de l'excès de pouvoir ; <br/>
<br/>
              5. Considérant que le pouvoir de prendre des mesures de police administrative dont l'Autorité de contrôle prudentiel dispose, notamment en vertu de l'article L. 612-31 du code monétaire et financier, et qu'elle peut exercer d'office, ont pour principal objet, s'agissant des mutuelles et unions relevant du livre II du code de la mutualité, de garantir la sécurité du marché des produits proposés par ces organismes et de veiller, comme l'indique le 2° du II de l'article L. 612-1 du code monétaire et financier, à ce qu'ils soient à tout moment en mesure de respecter les engagements qu'ils ont pris et les tiennent effectivement ; que la SELAFA MJA, agissant en qualité de liquidateur judiciaire représentant les intérêts des créanciers de la MRFP, est recevable à demander l'annulation de la décision refusant de mettre en demeure l'UMR de garantir les condamnations judiciaires dues à certains anciens adhérents de la MRFP sur le fondement de l'article L. 212-11 du code de la mutualité ;  <br/>
<br/>
              Sur la légalité de la décision attaquée :<br/>
<br/>
              6. Considérant que selon les termes mêmes de la lettre que la SELAFA MJA a adressée à l'ACP, sa demande tendait à ce que celle-ci mette en demeure l'UMR, en sa qualité de bénéficiaire du transfert du portefeuille de contrats issus de l'ancien CREF, de " garantir l'exécution des condamnations prononcées à l'encontre de la MRFP par l'arrêt de la cour d'appel de Paris du 29 avril 2011 ", une telle mise en demeure étant présentée par la SELAFA comme " la seule mesure de nature à assurer le droit des adhérents au CREF à l'effectivité des condamnations prononcées à leur profit " ; que toutefois, dès lors que la cour d'appel de Paris avait jugé que l'UMR n'avait pas à supporter les condamnations prononcées contre la MRFP, l'ACP, qui ne pouvait remettre en cause cette décision de justice, était par suite, et en tout état de cause, tenue de rejeter cette demande ;<br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que la SELAFA MJA n'est pas fondée à demander l'annulation de la décision attaquée ; que ses conclusions aux fins d'injonction ne peuvent par suite qu'être rejetées ; <br/>
<br/>
              Sur les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative : <br/>
<br/>
              8. Considérant que ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante ; que, dans les circonstances de l'espèce, il n'y a pas lieu de faire droit aux conclusions présentées par l'ACP au même titre ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la SELAFA MJA est rejetée. <br/>
Article 2 : Les conclusions présentées par l'Autorité de contrôle prudentiel au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à la SELAFA MJA, agissant en sa qualité de liquidateur judiciaire de la Mutuelle retraite de la fonction publique, et à l'Autorité de contrôle prudentiel.<br/>
Copie en sera adressée pour information au ministre de l'économie et des finances.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">14-01 COMMERCE, INDUSTRIE, INTERVENTION ÉCONOMIQUE DE LA PUISSANCE PUBLIQUE. PRINCIPES GÉNÉRAUX. - AUTORITÉ ADMINISTRATIVE INDÉPENDANTE INVESTIE D'UNE MISSION DE RÉGULATION ET DISPOSANT EN VERTU DE LA LOI DE POUVOIRS DE CONTRÔLE ET DE POLICE POUR ASSURER LA SÉCURITÉ D'UN MARCHÉ - REFUS DE METTRE EN &#140;UVRE CES POUVOIRS - NATURE - DÉCISION SUSCEPTIBLE DE RECOURS POUR EXCÈS DE POUVOIR - EXISTENCE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">52-045 POUVOIRS PUBLICS ET AUTORITÉS INDÉPENDANTES. AUTORITÉS ADMINISTRATIVES INDÉPENDANTES. - AUTORITÉ ADMINISTRATIVE INDÉPENDANTE INVESTIE D'UNE MISSION DE RÉGULATION ET DISPOSANT EN VERTU DE LA LOI DE POUVOIRS DE CONTRÔLE ET DE POLICE POUR ASSURER LA SÉCURITÉ D'UN MARCHÉ - REFUS DE METTRE EN &#140;UVRE CES POUVOIRS - NATURE - DÉCISION SUSCEPTIBLE DE RECOURS POUR EXCÈS DE POUVOIR - EXISTENCE [RJ1].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-01-01-01 PROCÉDURE. INTRODUCTION DE L'INSTANCE. DÉCISIONS POUVANT OU NON FAIRE L'OBJET D'UN RECOURS. ACTES CONSTITUANT DES DÉCISIONS SUSCEPTIBLES DE RECOURS. - AUTORITÉ ADMINISTRATIVE INDÉPENDANTE INVESTIE D'UNE MISSION DE RÉGULATION ET DISPOSANT EN VERTU DE LA LOI DE POUVOIRS DE CONTRÔLE ET DE POLICE POUR ASSURER LA SÉCURITÉ D'UN MARCHÉ - REFUS DE METTRE EN &#140;UVRE CES POUVOIRS [RJ1].
</SCT>
<ANA ID="9A"> 14-01 Il appartient à une autorité administrative indépendante investie d'une mission de régulation, qui dispose en vertu de la loi de pouvoirs de contrôle et de police, qu'elle exerce de sa propre initiative et dont l'objet consiste à assurer la sécurité d'un marché, de procéder, lorsqu'elle est saisie d'une demande tendant à la mise en oeuvre de ces pouvoirs, à l'examen des faits qui en sont à l'origine et de décider des suites à leur donner. Elle dispose, à cet effet, d'un large pouvoir d'appréciation et peut tenir compte de l'ensemble des intérêts généraux dont elle a la charge. La décision qu'elle prend, lorsqu'elle refuse de donner suite à la demande, peut être déférée au juge de l'excès de pouvoir.</ANA>
<ANA ID="9B"> 52-045 Il appartient à une autorité administrative indépendante investie d'une mission de régulation, qui dispose en vertu de la loi de pouvoirs de contrôle et de police, qu'elle exerce de sa propre initiative et dont l'objet consiste à assurer la sécurité d'un marché, de procéder, lorsqu'elle est saisie d'une demande tendant à la mise en oeuvre de ces pouvoirs, à l'examen des faits qui en sont à l'origine et de décider des suites à leur donner. Elle dispose, à cet effet, d'un large pouvoir d'appréciation et peut tenir compte de l'ensemble des intérêts généraux dont elle a la charge. La décision qu'elle prend, lorsqu'elle refuse de donner suite à la demande, peut être déférée au juge de l'excès de pouvoir.</ANA>
<ANA ID="9C"> 54-01-01-01 Il appartient à une autorité administrative indépendante investie d'une mission de régulation, qui dispose en vertu de la loi de pouvoirs de contrôle et de police, qu'elle exerce de sa propre initiative et dont l'objet consiste à assurer la sécurité d'un marché, de procéder, lorsqu'elle est saisie d'une demande tendant à la mise en oeuvre de ces pouvoirs, à l'examen des faits qui en sont à l'origine et de décider des suites à leur donner. Elle dispose, à cet effet, d'un large pouvoir d'appréciation et peut tenir compte de l'ensemble des intérêts généraux dont elle a la charge. La décision qu'elle prend, lorsqu'elle refuse de donner suite à la demande, peut être déférée au juge de l'excès de pouvoir.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr. CE, Section, 30 novembre 2007, Tinez et autres, n° 293952, p. 459.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
