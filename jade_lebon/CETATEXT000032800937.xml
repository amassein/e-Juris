<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032800937</ID>
<ANCIEN_ID>JG_L_2016_06_000000387133</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/80/09/CETATEXT000032800937.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 6ème chambres réunies, 29/06/2016, 387133</TITRE>
<DATE_DEC>2016-06-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>387133</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>LE PRADO ; SCP DELAPORTE, BRIARD</AVOCATS>
<RAPPORTEUR>M. Frédéric Puigserver</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Jean Lessi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:387133.20160629</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              1° Sous le n° 387133, par une requête sommaire, un mémoire complémentaire, un mémoire en réplique et deux autres mémoires, enregistrés les 14 janvier, 18 mars et 6 octobre 2015 et les 4 février et 5 avril 2016 au secrétariat du contentieux du Conseil d'Etat, le syndicat national des audioprothésistes - UNSAF demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté du ministre des affaires sociales et de la santé du 13 août 2014 modifiant l'arrêté du 15 février 2002 fixant la liste des marchandises dont les pharmaciens peuvent faire le commerce dans leur officine, ainsi que la décision du 19 décembre 2014 par laquelle ce ministre a rejeté son recours gracieux tendant au retrait de cet arrêté ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              2° Sous le n° 388193, par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 23 février, 27 avril et 6 octobre 2015 au secrétariat du contentieux du Conseil d'Etat, le Collège national d'audioprothèse demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté du ministre des affaires sociales et de la santé du 13 août 2014 modifiant l'arrêté du 15 février 2002 fixant la liste des marchandises dont les pharmaciens peuvent faire le commerce dans leur officine, ainsi que la décision du 19 décembre 2014 par laquelle ce ministre a rejeté son recours gracieux tendant au retrait de cet arrêté ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - le code de la santé publique ;<br/>
              - le décret n° 2005-850 du 27 juillet 2005 ;<br/>
              - le décret n° 2014-405 du 16 avril 2014 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Puigserver, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Jean Lessi, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Le Prado, avocat du syndicat national des audioprothésistes - UNSAF, à la SCP Delaporte, Briard, avocat de la société Sonalto, et à la SCP Odent, Poulet, avocat du Collège national d'audioprothèse ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 10 juin 2016, présentée pour le syndicat national des audioprothésistes - UNSAF ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que les requêtes du syndicat national des audioprothésistes - UNSAF et du Collège national d'audioprothèse sont dirigées contre le même arrêté du 13 août 2014, par lequel le ministre des affaires sociales et de la santé a modifié l'arrêté du 15 février 2002 fixant la liste des marchandises dont les pharmaciens peuvent faire le commerce dans leur officine pour y faire mention des assistants d'écoute préréglés d'une puissance maximale de 20 décibels ; qu'il y a lieu de joindre ces requêtes pour statuer par une seule décision ; <br/>
<br/>
              2. Considérant que la société Sonalto justifie d'un intérêt suffisant au maintien de l'arrêté attaqué ; qu'ainsi, son intervention dans l'instance n° 387133 est recevable ;<br/>
<br/>
              3. Considérant, en premier lieu, qu'il ressort des pièces du dossier que l'arrêté attaqué a été signé par Mme B...A..., directrice générale adjointe de la santé ; que, nommée dans ces fonctions par un décret du 7 mai 2014 publié au Journal officiel de la République française le 10 mai 2014, Mme A...avait, en vertu de l'article 1er du décret du 27 juillet 2005 relatif aux délégations de signature des membres du Gouvernement et de l'article D. 1421-1 du code de la santé publique définissant les compétences de la direction générale de la santé, qualité pour signer l'arrêté attaqué au nom du ministre des affaires sociales et de la santé ; que, par suite, le moyen tiré de l'incompétence du signataire de l'arrêté attaqué doit être écarté ;<br/>
<br/>
              4. Considérant, en deuxième lieu, d'une part, qu'aux termes de l'article L. 5125-24 du code de la santé publique : " Les pharmaciens ne peuvent faire dans leur officine le commerce de marchandises autres que celles figurant sur une liste arrêtée par le ministre chargé de la santé, sur proposition du Conseil national de l'ordre des pharmaciens. (...) " ;<br/>
<br/>
              5. Considérant, d'autre part, qu'aux termes de l'article L. 4361-1 du code de la santé publique : " Est considérée comme exerçant la profession d'audioprothésiste toute personne qui procède à l'appareillage des déficients de l'ouïe. / Cet appareillage comprend le choix, l'adaptation, la délivrance, le contrôle d'efficacité immédiate et permanente de la prothèse auditive et l'éducation prothétique du déficient de l'ouïe appareillé. / La délivrance de chaque appareil de prothèse auditive est soumise à la prescription médicale préalable et obligatoire du port d'un appareil, après examen otologique et audiométrique tonal et vocal " ; que l'article L. 4361-2 du même code subordonne l'exercice de la profession d'audioprothésiste à la détention d'un diplôme, certificat ou titre mentionné aux articles L. 4361-3 et L. 4361-4 ; que l'article L. 4361-6 de ce code prévoit que l'activité d'audioprothésiste ne peut être exercée que dans un local réservé à cet effet et aménagé ; qu'enfin, l'exercice illégal de la profession d'audioprothésiste est passible des sanctions pénales prévues à l'article L. 4363-2 du même code ;<br/>
<br/>
              6. Considérant qu'il ressort des pièces du dossier que, par l'arrêté attaqué, pris sur le fondement des dispositions précitées de l'article L. 5125-24 du code de la santé publique, le ministre chargé de la santé a précisé qu'étaient compris dans la catégorie des dispositifs médicaux à usage individuel que les pharmaciens sont autorisés à vendre dans leur officine les assistants d'écoute préréglés d'une puissance maximale de 20 décibels ; qu'eu égard, d'une part, aux propriétés de ces appareils, qui ne permettent d'autre réglage que celui du volume d'amplification et qui ne supposent pas d'adaptation individuelle, et, d'autre part, à leur faible puissance, qui ne leur permet de corriger que des déficiences auditives légères, principalement dues aux effets de l'âge et affectant les deux oreilles dans les mêmes conditions, les assistants d'écoute en cause, bien qu'ayant le caractère de dispositifs médicaux au sens des articles L. 5211-1 et R. 5211-1 du code de la santé publique, ne peuvent être regardés comme des prothèses auditives délivrées dans le cadre d'un appareillage de déficients de l'ouïe, que sont seuls habilités à effectuer, sur prescription médicale, en vertu de l'article L. 4361-1 du même code, les personnes exerçant la profession d'audioprothésiste, avec les diplômes, certificats ou titres mentionnés aux articles L. 4361-2 à L. 4361-4 de ce code, et disposant, à cette fin, des locaux prévus à l'article L. 4361-6 ; qu'ainsi, l'arrêté attaqué ne méconnaît pas les dispositions du code de la santé publique citées au point 5 ci-dessus ;<br/>
<br/>
              7. Considérant, en troisième lieu, qu'il ne ressort pas des pièces du dossier que l'arrêté attaqué serait entaché d'une erreur manifeste dans l'appréciation du risque pour la santé publique que présenterait l'utilisation des assistants d'écoute en cause, notamment en raison des retards de diagnostic et de l'aggravation de certaines pathologies auxquels celle-ci pourrait conduire ; qu'au demeurant, l'arrêté attaqué a pour seul objet de préciser que les assistants d'écoute préréglés peuvent être distribués par les pharmaciens,  lesquels sont des professionnels de santé, auxquels il incombe, chaque fois que cela paraît nécessaire, de recommander aux patients la consultation d'un spécialiste ; <br/>
<br/>
              8. Considérant, en dernier lieu, que le détournement de pouvoir allégué n'est pas établi ;<br/>
<br/>
              9. Considérant qu'il résulte de ce qui précède que le syndicat national des audioprothésistes - UNSAF et le Collège national d'audioprothèse ne sont pas fondés à demander l'annulation de l'arrêté attaqué et des décisions rejetant leurs recours gracieux tendant à son retrait ;<br/>
<br/>
              10. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'intervention de la société Sonalto dans l'instance n° 387133 est admise.<br/>
Article 2 : Les requêtes du syndicat national des audioprothésistes - UNSAF et du Collège national d'audioprothèse sont rejetées.<br/>
Article 3 : La présente décision sera notifiée au syndicat national des audioprothésistes - UNSAF, au Collège national d'audioprothèse, à la société Sonalto et à la ministre des affaires sociales et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">61-04-01-05 SANTÉ PUBLIQUE. PHARMACIE. PRODUITS PHARMACEUTIQUES. DISPOSITIFS MÉDICAUX. - DISPOSITIFS MÉDICAUX QUE LES PHARMACIENS SONT AUTORISÉS À VENDRE DANS LEUR OFFICINE - INCLUSION - ASSISTANTS D'ÉCOUTE PRÉRÉGLÉS - LÉGALITÉ.
</SCT>
<ANA ID="9A"> 61-04-01-05 Arrêté du ministre chargé de la santé précisant que sont compris dans la catégorie des dispositifs médicaux à usage individuel que les pharmaciens sont autorisés à vendre dans leur officine les assistants d'écoute préréglés d'une puissance maximale de 20 décibels.,,,Eu égard, d'une part, aux propriétés de ces appareils, qui ne permettent d'autre réglage que celui du volume d'amplification et qui ne supposent pas d'adaptation individuelle, et, d'autre part, à leur faible puissance, qui ne leur permet de corriger que des déficiences auditives légères, principalement dues aux effets de l'âge et affectant les deux oreilles dans les mêmes conditions, les assistants d'écoute en cause, bien qu'ayant le caractère de dispositifs médicaux au sens des articles L. 5211-1 et R. 5211-1 du code de la santé publique, ne peuvent être regardés comme des prothèses auditives délivrées dans le cadre d'un appareillage de déficients de l'ouïe, que sont seuls habilités à effectuer, sur prescription médicale, en vertu de l'article L. 4361-1 du même code, les personnes exerçant la profession d'audioprothésiste.,,,Absence d'erreur manifeste dans l'appréciation du risque pour la santé publique que présenterait l'utilisation des assistants d'écoute en cause, notamment en raison des retards de diagnostic et de l'aggravation de certaines pathologies auxquels celle-ci pourrait conduire. Au demeurant, l'arrêté attaqué a pour seul objet de préciser que les assistants d'écoute préréglés peuvent être distribués par les pharmaciens, lesquels sont des professionnels de santé, auxquels il incombe, chaque fois que cela paraît nécessaire, de recommander aux patients la consultation d'un spécialiste.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
