<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039274938</ID>
<ANCIEN_ID>JG_L_2019_10_000000422023</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/27/49/CETATEXT000039274938.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 6ème chambres réunies, 23/10/2019, 422023</TITRE>
<DATE_DEC>2019-10-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>422023</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 6ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GOUZ-FITOUSSI</AVOCATS>
<RAPPORTEUR>M. Jean-Dominique Langlais</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2019:422023.20191023</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B... A... a demandé au tribunal administratif de Paris de condamner l'État à lui verser une somme de 12 869,34 euros en réparation des préjudices ayant résulté pour elle de son absence de relogement. Par un jugement n° 1712617/6-2 du 10 avril 2018, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 6 juillet et 4 octobre 2018 au secrétariat du contentieux du Conseil d'Etat, Mme A... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros à verser à la SCP Gouz-Fitoussi, son avocat, au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la construction et de l'habitation ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Langlais, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gouz-Fitoussi, avocat de Mme A....<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que Mme A... a été reconnue comme prioritaire et devant être relogée en urgence par une décision du 14 août 2008 de la commission de médiation de Paris, au motif qu'elle était menacée d'expulsion sans relogement. Par un jugement du 31 mars 2010, le tribunal administratif de Paris, saisi par Mme A... sur le fondement du I de l'article L. 441-2-3-1 du code de la construction et de l'habitation, a enjoint au préfet de la région Ile-de-France, préfet de Paris d'assurer son relogement. En raison du délai mis pour l'exécution de ce jugement, Mme A... a demandé au tribunal administratif de Paris de condamner l'Etat à l'indemniser du préjudice résultant pour elle de son absence de relogement jusqu'en novembre 2015, en y incluant notamment, d'une part, des frais de stockage de ses affaires personnelles chiffrés à 11 080,74 euros, d'autre part, des frais d'hôtel évalués à 1 788,60 euros. Elle se pourvoit en cassation contre le jugement du 10 avril 2018 par lequel le tribunal administratif a rejeté sa demande.<br/>
<br/>
              2. Lorsqu'une personne a été reconnue comme prioritaire et devant être logée ou relogée d'urgence par une commission de médiation, en application des dispositions de l'article L. 441-2-3 du code de la construction et de l'habitation, la carence fautive de l'Etat à exécuter cette décision dans le délai imparti engage sa responsabilité à l'égard du seul demandeur au titre des troubles dans les conditions d'existence résultant du maintien de la situation qui a motivé la décision de la commission, que l'intéressé ait ou non fait usage du recours en injonction contre l'Etat prévu par l'article L. 441-2-3-1 du code de la construction et de l'habitation. La période de responsabilité de l'Etat court à compter de l'expiration du délai de trois ou six mois que les dispositions de l'article R. 441-16-1 du code de la construction et de l'habitation impartissent au préfet pour provoquer une offre de logement à la suite de la décision de la commission de médiation. Ces troubles doivent être appréciés en tenant notamment compte des conditions de logement qui ont perduré du fait de la carence de l'Etat, de la durée de cette carence et du nombre de personnes composant le foyer du demandeur pendant la période de responsabilité de l'Etat.<br/>
<br/>
              3. Il ressort des termes mêmes du jugement attaqué que, ayant constaté que le préfet n'avait pas proposé un relogement à Mme A... dans le délai prévu par le code de la construction et de l'habitation, le tribunal administratif de Paris a jugé que cette carence était constitutive d'une faute de nature à engager la responsabilité de l'État à son égard du 14 février 2009 jusqu'en novembre 2015, date à laquelle elle a été relogée. Il a toutefois jugé que cette faute ne lui causait aucun préjudice indemnisable, au motif qu'elle n'établissait ni même n'alléguait que les frais qu'elle invoquait au titre du stockage de ses affaires personnelles et de périodes ponctuelles de logement à l'hôtel auraient été supérieurs à ceux qu'elle aurait dû exposer au titre de ses loyers et charges en cas de relogement.<br/>
<br/>
              4. Il résulte cependant de ce qui été dit au point 2 que le tribunal administratif ne pouvait, sans erreur de droit, rejeter la demande de Mme A..., alors que le maintien de la situation qui a motivé la décision de la commission de médiation lui ouvrait droit à la réparation de ses troubles dans ses conditions d'existence. Mme A... est par suite fondée à demander l'annulation du jugement attaqué, sans qu'il soit besoin d'examiner l'autre moyen du pourvoi.<br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              6. Il résulte de ce qui a été dit ci-dessus que la responsabilité de l'Etat est engagée à raison du maintien de Mme A... dans la situation qui a motivé la décision de la commission de médiation entre le 14 février 2009 et le 16 novembre 2015, date à laquelle elle a été relogée. Au cours de cette période, son foyer doit être regardé comme composé de quatre personnes, dont trois enfants mineurs à sa charge. Compte-tenu de ses conditions de logement pendant cette période, dont l'obligation non contestée de faire stocker ses affaires personnelles et d'exposer à plusieurs reprises des frais d'hôtel révèlent la particulière précarité, il sera fait une juste appréciation des troubles qu'elle a subis dans ses conditions d'existence, en raison de la carence de l'Etat à assurer son relogement, en fixant l'indemnité qui lui est due à 9 000 euros. <br/>
<br/>
              7. Mme A... a obtenu le bénéfice de l'aide juridictionnelle. Par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 relative à l'aide juridique. Il y a lieu, dans les circonstances de l'espèce, et sous réserve que la SCP Gouz-Fitoussi, avocat de Mme A..., renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à la SCP Gouz-Fitoussi.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
		Article 1er : Le jugement du 10 avril 2018 du tribunal administratif de Paris est annulé.<br/>
<br/>
		Article 2 : L'Etat versera une indemnité de 9 000 euros à Mme A....<br/>
<br/>
Article 3 : L'Etat versera à la SCP Gouz-Fitoussi une somme de 3 000 euros en application des dispositions de l'article L. 761-1 du code de justice administrative et du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991, sous réserve que cette SCP renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
<br/>
Article 4 : La présente décision sera notifiée à Mme B... A... et à la ministre de la cohésion des territoires et des relations avec les collectivités territoriales.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">38-07-01 LOGEMENT. - RESPONSABILITÉ DE L'ETAT À RAISON DE LA CARENCE FAUTIVE À ASSURER LE LOGEMENT D'UN DEMANDEUR RECONNU PRIORITAIRE ET URGENT DANS LE DÉLAI FIXÉ PAR LE JUGE DE L'INJONCTION - EVALUATION DU PRÉJUDICE [RJ1] - 1) CIRCONSTANCE QUE LES FRAIS EXPOSÉS PAR LE DEMANDEUR EN CAS DE RELOGEMENT AURAIENT ÉTÉ SUPÉRIEURS À CEUX QU'IL A EFFECTIVEMENT EXPOSÉS - CIRCONSTANCE INOPÉRANTE - 2) ESPÈCE - OBLIGATION DE STOCKER SES AFFAIRES PERSONNELLES ET D'EXPOSER DES FRAIS D'HÔTEL RÉVÉLANT LA PARTICULIÈRE PRÉCARITÉ DES CONDITIONS DE LOGEMENT ET JUSTIFIANT UNE MAJORATION DE L'INDEMNISATION FORFAITAIRE [RJ2].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">60-02-012 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RESPONSABILITÉ EN RAISON DES DIFFÉRENTES ACTIVITÉS DES SERVICES PUBLICS. SERVICES SOCIAUX. - DALO - CARENCE FAUTIVE DE L'ETAT À ASSURER LE LOGEMENT D'UN DEMANDEUR RECONNU PRIORITAIRE ET URGENT DANS LE DÉLAI FIXÉ PAR LE JUGE DE L'INJONCTION - EVALUATION DU PRÉJUDICE [RJ1] - 1) CIRCONSTANCE QUE LES FRAIS EXPOSÉS PAR LE DEMANDEUR EN CAS DE RELOGEMENT AURAIENT ÉTÉ SUPÉRIEURS À CEUX QU'IL A EFFECTIVEMENT EXPOSÉS - CIRCONSTANCE INOPÉRANTE - 2) ESPÈCE - OBLIGATION DE STOCKER SES AFFAIRES PERSONNELLES ET D'EXPOSER DES FRAIS D'HÔTEL RÉVÉLANT LA PARTICULIÈRE PRÉCARITÉ DES CONDITIONS DE LOGEMENT ET JUSTIFIANT UNE MAJORATION DE L'INDEMNISATION FORFAITAIRE DES TCE [RJ2].
</SCT>
<ANA ID="9A"> 38-07-01 Engagement de la responsabilité de l'Etat à raison de la carence fautive à assurer le logement d'un demandeur reconnu prioritaire et urgent par une commission de médiation (art. L. 441-2-3 du code de la construction et de l'habitation - CCH), au titre des troubles dans ses conditions d'existence (TCE).,,,1) Commet une erreur de droit le tribunal qui se fonde, pour rejeter la demande d'indemnisation de la requérante, sur la circonstance qu'elle n'établissait ni même n'alléguait que les frais qu'elle invoquait au titre du stockage de ses affaires personnelles et de périodes ponctuelles de logement à l'hôtel auraient été supérieurs à ceux qu'elle aurait dû exposer au titre de ses loyers et charges en cas de relogement.,,,2) Période de responsabilité courant du 14 février 2009 au 16 novembre 2015. Foyer composé de quatre personnes, dont trois enfants mineurs à la charge de la requérante.... ,,Compte-tenu de ses conditions de logement pendant cette période, dont l'obligation non contestée de faire stocker ses affaires personnelles et d'exposer à plusieurs reprises des frais d'hôtel révèlent la particulière précarité, il sera fait une juste appréciation des troubles qu'elle a subis dans ses conditions d'existence, en raison de la carence de l'Etat à assurer son relogement, en fixant l'indemnité qui lui est due à 9 000 euros.</ANA>
<ANA ID="9B"> 60-02-012 Engagement de la responsabilité de l'Etat à raison de la carence fautive à assurer le logement d'un demandeur reconnu prioritaire et urgent par une commission de médiation (art. L. 441-2-3 du code de la construction et de l'habitation - CCH), au titre des troubles dans ses conditions d'existence (TCE).,,,1) Commet une erreur de droit le tribunal qui se fonde, pour rejeter la demande d'indemnisation de la requérante, sur la circonstance qu'elle n'établissait ni même n'alléguait que les frais qu'elle invoquait au titre du stockage de ses affaires personnelles et de périodes ponctuelles de logement à l'hôtel auraient été supérieurs à ceux qu'elle aurait dû exposer au titre de ses loyers et charges en cas de relogement.,,,2) Période de responsabilité courant du 14 février 2009 au 16 novembre 2015. Foyer composé de quatre personnes, dont trois enfants mineurs à la charge de la requérante.... ,,Compte-tenu de ses conditions de logement pendant cette période, dont l'obligation non contestée de faire stocker ses affaires personnelles et d'exposer à plusieurs reprises des frais d'hôtel révèlent la particulière précarité, il sera fait une juste appréciation des troubles qu'elle a subis dans ses conditions d'existence, en raison de la carence de l'Etat à assurer son relogement, en fixant l'indemnité qui lui est due à 9 000 euros.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., sur les règles gouvernant l'indemnisation d'un demandeur reconnu prioritaire et urgent, CE, 13 juillet 2016,,, n° 382872, T. p. 945 ; CE, 16 décembre 2016,,, n° 383111, p. 563 ; CE, 19 juillet 2017,,, n° 402172, T. pp. 664-797-804.,,[RJ2] Cf., s'agissant de l'indemnisation sur la base de 250 euros par personne et par an, CE, 28 mars 2019,,, n° 414630, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
