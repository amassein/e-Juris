<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000024448331</ID>
<ANCIEN_ID>JG_L_2011_07_000000335752</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/24/44/83/CETATEXT000024448331.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème et 3ème sous-sections réunies, 26/07/2011, 335752</TITRE>
<DATE_DEC>2011-07-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>335752</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème et 3ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PEIGNOT, GARREAU ; SCP LE GRIEL</AVOCATS>
<RAPPORTEUR>M. Alexandre Aïdara</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Olléon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2011:335752.20110726</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 20 janvier et 7 avril 2010 au secrétariat du contentieux du Conseil d'Etat, présentés pour Mme B...A..., demeurant chez..., ; Mme A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 08PA01919 du 29 juin 2009 par lequel, statuant sur la requête du préfet de police, la cour administrative d'appel de Paris a, d'une part, annulé le jugement n° 0718735 du 5 mars 2008 par lequel le tribunal administratif de Paris a annulé l'arrêté préfectoral du 21 août 2007 lui refusant la délivrance d'un titre de séjour, l'obligeant à quitter le territoire français et fixant le pays de destination, et enjoignant au préfet de police de lui délivrer un titre de séjour mention "vie privée et familiale", et, d'autre part, rejeté sa demande présentée devant le tribunal administratif de Paris ainsi que ses conclusions aux fins d'injonction et d'astreinte présentées devant la cour ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la requête du préfet de police ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 2 500 euros à verser à la SCP Le Griel, avocat de Mme A..., au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu la convention internationale relative aux droits de l'enfant, signée à New York le 26 janvier 1990 ;<br/>
<br/>
              Vu le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
<br/>
              Vu la loi n° 91-647 du 10 juillet 1991 ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Alexandre Aïdara, chargé des fonctions de Maître des Requêtes, <br/>
<br/>
              - les observations de la SCP Le Griel, avocat de Mme A..., <br/>
<br/>
      - les conclusions de M. Laurent Olléon, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Le Griel, avocat de Mme A... ; <br/>
<br/>
<br/>
<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que Mme A..., de nationalité malienne, a déclaré être entrée en France en 2002 et est mère de trois enfants mineurs, nés en France les 16 mars 2003, 26 juillet 2004 et 30 octobre 2006, qu'elle a eus avec un compatriote, titulaire d'un titre de séjour ; que le dernier de ses enfants est atteint d'une maladie hématologique congénitale symptomatique dénommée drépanocytose homozygote ; qu'elle a sollicité son admission au séjour en France sur le fondement de l'article L. 311-12 du code de l'entrée et du séjour des étrangers et du droit d'asile en qualité d'accompagnant d'un enfant malade ; que, par arrêté en date du 21 août 2007, le préfet de police a rejeté sa demande, lui faisant obligation de quitter le territoire français et fixant le pays de destination ; que Mme A... se pourvoit en cassation contre l'arrêt du 29 juin 2009 par lequel, statuant sur la requête du préfet de police, la cour administrative d'appel de Paris y a fait droit et a, d'une part, annulé le jugement du 5 mars 2008 du tribunal administratif de Paris ayant annulé cet arrêté préfectoral et, d'autre part, rejeté ses demandes ; <br/>
<br/>
              Sans qu'il soit besoin d'examiner les autres moyens du pourvoi ;<br/>
<br/>
              Considérant qu'aux termes de l'article 3-1 de la convention internationale relative aux droits de l'enfant signée à New York le 26 janvier 1990, publiée par décret le 8 octobre 1990 : " Dans toutes les décisions qui concernent les enfants, qu'elles soient le fait d'institutions publiques ou privées de protection sociale, des tribunaux, des autorités administratives ou des organes législatifs, l'intérêt supérieur de l'enfant doit être une considération primordiale " ; qu'il résulte de ces stipulations, qui peuvent être utilement invoquées à l'appui d'un recours pour excès de pouvoir, que, dans l'exercice de son pouvoir d'appréciation, l'autorité administrative doit accorder une attention primordiale à l'intérêt supérieur des enfants dans toutes les décisions les concernant ;<br/>
<br/>
              Considérant qu'après avoir censuré le motif retenu par le tribunal administratif de Paris et tiré de ce que l'arrêté du préfet de police avait méconnu les stipulations de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, la cour a écarté le moyen tiré de la violation des stipulations de l'article 3-1 de la convention internationale relative aux droits de l'enfant, aux motifs qu'il n'était pas établi que le père des trois enfants de Mme A..., dont elle a relevé qu'il était marié avec une tierce personne avec laquelle il résidait, contribuait effectivement à leur entretien et à leur éducation et qu'à la date de l'arrêté du préfet de police, rien ne s'opposait à ce que la requérante retournât dans son pays d'origine avec ses enfants ; que, pour retenir ce dernier motif, la cour s'est fondée sur le fait que le certificat médical produit par Mme A... était insuffisamment circonstancié, en particulier sur la nature des soins et traitements qui seraient nécessaires à son dernier enfant, que ce document n'établissait pas que cet enfant atteint de cette maladie ne pourrait bénéficier d'un traitement approprié dans le pays d'origine de sa mère et qu'il n'était pas de nature à remettre en cause l'avis du médecin-chef du service médical de la préfecture de police selon lequel la pathologie de cet enfant ne présentait aucune complication et justifiait une surveillance médicale pouvant être réalisée dans ce pays ; que, toutefois, il ressort des pièces du dossier soumis à la cour que, d'une part, le certificat médical, établi par un praticien hospitalier du centre de la drépanocytose de l'Hôpital Robert Debré, mentionne que l'enfant de Mme A... présente une pathologie grave évolutive nécessitant un traitement au long cours non disponible dans le pays d'origine de ses parents, que cette maladie requiert un suivi médical régulier dont l'absence ferait courir des risques de complications très graves ; que, d'autre part, si l'avis du médecin-chef du service médical de la préfecture de police admet que le défaut de prise en charge de la maladie peut entraîner des conséquences d'une exceptionnelle gravité et fait état d'une " surveillance possible au Mali ", il se borne à mentionner l'absence de tout traitement sans donner de précision sur le point de savoir si l'enfant peut avoir accès dans son pays d'origine aux soins qu'implique ce suivi médical ; qu'enfin, la requérante avait justifié à la date de l'arrêté préfectoral du traitement renouvelable administré en France à son enfant dans le cadre du traitement au long cours de cette maladie ; que dans ces conditions et eu égard à la nécessité de la présence de Mme A... auprès de son enfant malade, la cour a inexactement qualifié les faits qui lui étaient soumis en jugeant que l'arrêté du préfet de police ne pouvait être regardé comme portant atteinte à l'intérêt supérieur de l'enfant au sens des stipulations de l'article 3-1 de la convention internationale relative aux droits de l'enfant ; que, par suite, la requérante est fondée à en demander l'annulation ; <br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de faire application des dispositions de l'article L. 821-2 du code de justice administrative et de régler l'affaire au fond ;<br/>
<br/>
              Sur la requête du préfet de police : <br/>
<br/>
              Sans qu'il soit besoin de statuer sur la fin de non recevoir opposée par Mme A... ;<br/>
<br/>
              Considérant, d'une part, qu'aux termes de l'article L. 311-12 du code de l'entrée et du séjour des étrangers et du droit d'asile, dans sa rédaction applicable à la date de l'arrêté attaqué : " Sauf si sa présence constitue une menace pour l'ordre public, une autorisation provisoire de séjour peut être délivrée à l'un des parents étranger de l'étranger mineur qui remplit les conditions mentionnées au 11° de l'article L. 313-11, sous réserve qu'il justifie résider habituellement en France avec lui et subvenir à son entretien et à son éducation, sans que la condition prévue à l'article L. 311-7 soit exigée. / L'autorisation provisoire de séjour mentionnée au premier alinéa, qui ne peut être d'une durée supérieure à six mois, est délivrée par l'autorité administrative, après avis du médecin inspecteur de santé publique compétent au regard du lieu de résidence de l'intéressé ou, à Paris, du médecin, chef du service médical de la préfecture de police, dans les conditions prévues au 11° de l'article L. 313-11. Elle est renouvelable et n'autorise pas son titulaire à travailler. Toutefois, cette autorisation peut être assortie d'une autorisation provisoire de travail, sur présentation d'un contrat de travail. " ; que, pour remplir les conditions mentionnées au 11° de l'article L. 313-11 du même code dans sa rédaction applicable au litige, l'état de santé de l'étranger mineur résidant habituellement en France doit nécessiter une prise en charge médicale dont le défaut pourrait entraîner pour lui des conséquences d'une exceptionnelle gravité, sous réserve qu'il ne puisse effectivement bénéficier d'un traitement approprié dans le pays dont il est originaire ;<br/>
<br/>
              Considérant, d'autre part, qu'aux termes de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales : " 1. Toute personne a droit au respect de sa vie privée et familiale, de son domicile et de sa correspondance. / 2. Il ne peut y avoir ingérence d'une autorité publique dans l'exercice de ce droit que pour autant que cette ingérence est prévue par la loi et qu'elle constitue une mesure qui, dans une société démocratique, est nécessaire à la sécurité nationale, à la sûreté publique, au bien-être économique du pays, à la défense de l'ordre et à la prévention des infractions pénales, à la protection de la santé ou de la morale, ou à la protection des droits et libertés d'autrui " ;<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier qu'ainsi qu'il a été dit ci-dessus, le dernier enfant de Mme A... est porteur d'une pathologie grave nécessitant un traitement au long cours ; que le préfet de police reconnaît que cette maladie nécessite un suivi médical régulier et une prise en charge médicale des crises qui peuvent en résulter ; qu'il ne conteste pas que cette maladie peut engendrer des complications mettant en jeu le pronostic vital ; que s'il soutient que cette pathologie a fait l'objet d'un plan de lutte initié depuis plusieurs années au Mali et qu'elle est prise en charge, il ne ressort pas des éléments qu'il produit qu'à la date de son arrêté, cet enfant aurait pu bénéficier dans ce pays d'un suivi et d'une prise en charge appropriés ; qu'il n'est pas établi que cet enfant puisse être pris en charge par son père, qui ne subvient pas à son entretien ; que l'arrêté contesté aurait pour effet de priver cet enfant malade de la présence indispensable, eu égard à son très jeune âge, de sa mère ; que, par suite et alors même que la requérante conserve des attaches familiales dans son pays d'origine, où résident ses parents et ses frères et soeurs, le préfet de police n'est pas fondé à soutenir que le tribunal administratif de Paris a fait une inexacte application des stipulations précitées de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et que c'est à tort qu'il a annulé pour violation de ces stipulations son arrêté refusant à Mme A... l'autorisation de séjour prévue par l'article L. 311-12 du code de l'entrée et du séjour des étrangers et du droit d'asile en raison de l'atteinte disproportionnée portée au droit de l'intéressée au respect de sa vie privée et familiale ;<br/>
<br/>
              Sur les conclusions à fin d'injonction présentées par Mme A... : <br/>
<br/>
              Considérant qu'aux termes de l'article L. 911-1 du code de justice administrative : " Lorsque sa décision implique nécessairement qu'une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public prenne une mesure d'exécution dans un sens déterminé, la juridiction, saisie de conclusions en ce sens, prescrit, par la même décision, cette mesure assortie, le cas échéant, d'un délai d'exécution. " ; qu'aux termes de l'article L. 911-3 du même code : " Saisie de conclusions en ce sens, la juridiction peut assortir, dans la même décision, l'injonction prescrite en application des articles L. 911-1 et L. 911-2 d'une astreinte qu'elle prononce dans les conditions prévues au présent livre et dont elle fixe la date d'effet. " ; <br/>
<br/>
              Considérant qu'en l'absence d'un changement dans la situation de droit ou de fait de Mme A..., la présente décision implique nécessairement que, comme le demande la requérante, le préfet de police lui délivre une autorisation provisoire de séjour dans un délai d'un mois à compter de la notification de la présente décision ; qu'en revanche il n'y a pas lieu, dans les circonstances de l'espèce, d'assortir cette injonction d'une astreinte ;<br/>
<br/>
              Sur les conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative et du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991 :<br/>
<br/>
              Considérant que Mme A... a obtenu le bénéfice de l'aide juridictionnelle ; que, par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ; qu'il y a lieu, dans les circonstances de l'espèce, et sous réserve que la SCP Griel renonce à percevoir les sommes correspondant à la part contributive de l'Etat, de mettre à la charge de l'Etat le versement à la SCP Griel de la somme de 2 500 euros ;<br/>
<br/>
<br/>
<br/>
<br/>		D E C I D E :<br/>
              		--------------<br/>
<br/>
Article 1er : L'arrêt du 29 juin 2009 de la cour administrative d'appel de Paris est annulé.<br/>
Article 2 : La requête du préfet de police devant la cour administrative d'appel de Paris est rejetée.<br/>
Article 3 : Il est enjoint au ministre de l'intérieur, de l'outre-mer, des collectivités territoriales et de l'immigration de délivrer à Mme A... une autorisation provisoire de séjour sur le fondement de l'article L. 311-12 du code de l'entrée et du séjour des étrangers et du droit d'asile dans le délai d'un mois à compter de la notification de la décision à intervenir. <br/>
Article 4 : L'Etat versera à la SCP Le Griel, avocat de Mme A..., la somme de 2 500 euros en application des dispositions du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991 sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
Article 5 : Le surplus des conclusions de Mme A... présentées devant la cour administrative d'appel est rejeté.<br/>
Article 6 : La présente décision sera notifiée à Mme B... A...et au ministre de l'intérieur, de l'outre-mer, des collectivités territoriales et de l'immigration.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-01-02-02 ACTES LÉGISLATIFS ET ADMINISTRATIFS. DIFFÉRENTES CATÉGORIES D'ACTES. ACCORDS INTERNATIONAUX. APPLICATION PAR LE JUGE FRANÇAIS. - CONVENTION INTERNATIONALE RELATIVE AUX DROITS DE L'ENFANT - ARTICLE 3-1 [RJ1] - INTÉRÊT SUPÉRIEUR DE L'ENFANT - CONTRÔLE DU JUGE DE CASSATION - QUALIFICATION JURIDIQUE DES FAITS.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">01-04-01 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. TRAITÉS ET DROIT DÉRIVÉ. - CONVENTION INTERNATIONALE RELATIVE AUX DROITS DE L'ENFANT - ARTICLE 3-1 [RJ1] - INTÉRÊT SUPÉRIEUR DE L'ENFANT - CONTRÔLE DU JUGE DE CASSATION - QUALIFICATION JURIDIQUE DES FAITS.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-08-02-02-01-02 PROCÉDURE. VOIES DE RECOURS. CASSATION. CONTRÔLE DU JUGE DE CASSATION. BIEN-FONDÉ. QUALIFICATION JURIDIQUE DES FAITS. - INTÉRÊT SUPÉRIEUR DE L'ENFANT - CONVENTION INTERNATIONALE RELATIVE AUX DROITS DE L'ENFANT - ARTICLE 3-1 [RJ1].
</SCT>
<ANA ID="9A"> 01-01-02-02 Il résulte des stipulations de l'article 3-1 de la convention internationale relative aux droits de l'enfant, qui peuvent être utilement invoquées à l'appui d'un recours pour excès de pouvoir, que, dans l'exercice de son pouvoir d'appréciation, l'autorité administrative doit accorder une attention primordiale à l'intérêt supérieur des enfants dans toutes les décisions les concernant. Le juge de cassation exerce un contrôle de la qualification juridique des faits sur l'atteinte à l'intérêt supérieur de l'enfant au sens des stipulations de l'article 3-1.</ANA>
<ANA ID="9B"> 01-04-01 Il résulte des stipulations de l'article 3-1 de la convention internationale relative aux droits de l'enfant, qui peuvent être utilement invoquées à l'appui d'un recours pour excès de pouvoir, que, dans l'exercice de son pouvoir d'appréciation, l'autorité administrative doit accorder une attention primordiale à l'intérêt supérieur des enfants dans toutes les décisions les concernant. Le juge de cassation exerce un contrôle de la qualification juridique des faits sur l'atteinte à l'intérêt supérieur de l'enfant au sens des stipulations de l'article 3-1.</ANA>
<ANA ID="9C"> 54-08-02-02-01-02 Il résulte des stipulations de l'article 3-1 de la convention internationale relative aux droits de l'enfant, qui peuvent être utilement invoquées à l'appui d'un recours pour excès de pouvoir, que, dans l'exercice de son pouvoir d'appréciation, l'autorité administrative doit accorder une attention primordiale à l'intérêt supérieur des enfants dans toutes les décisions les concernant. Le juge de cassation exerce un contrôle de la qualification juridique des faits sur l'atteinte à l'intérêt supérieur de l'enfant au sens des stipulations de l'article 3-1.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 22 septembre 1997, Mlle Cinar, n° 161364, p. 319.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
