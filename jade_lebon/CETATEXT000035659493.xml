<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035659493</ID>
<ANCIEN_ID>JG_L_2017_09_000000404475</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/65/94/CETATEXT000035659493.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 5ème chambres réunies, 27/09/2017, 404475</TITRE>
<DATE_DEC>2017-09-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>404475</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Pierre-François Mourier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:404475.20170927</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, un mémoire en réplique et un nouveau mémoire, enregistrés les 17 octobre 2016, 9 juin 2017 et le 11 septembre 2017 au secrétariat du contentieux du Conseil d'Etat, M. A...B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la délibération du 4 mai 2016 du comité de sélection de l'université de Lorraine statuant sur sa candidature au poste de professeur en archéologie médiévale et patrimoine référencé 21-PR-0782 et la délibération du conseil d'administration du 31 mai 2016 déclarant le concours infructueux ainsi que la décision implicite du président de l'université rejetant son recours tendant à ce que le comité de sélection délibère à nouveau sur sa candidature ;<br/>
<br/>
              2°) d'enjoindre à l'université de Lorraine de reprendre les opérations de recrutement.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'éducation ;<br/>
              - le décret n° 84-431 du 6 juin 1984 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Pierre-François Mourier, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes des dispositions de l'article L. 952-6-1 du code de l'éducation : " (...) lorsqu'un emploi d'enseignant-chercheur est créé ou déclaré vacant, les candidatures des personnes dont la qualification est reconnue par l'instance nationale prévue à l'article L. 952-6 sont soumises à l'examen d'un comité de sélection créé par délibération du conseil académique (...). / Au vu de son avis motivé, le conseil académique ou, pour les établissements qui n'en disposent pas, le conseil d'administration, siégeant en formation restreinte aux enseignants-chercheurs et personnels assimilés de rang au moins égal à celui postulé, transmet au ministre compétent le nom du candidat dont il propose la nomination ou une liste de candidats classés par ordre de préférence (...) " ; qu'aux termes des dispositions de l'article 9-2 du décret du 6 juin 1984 fixant les dispositions statutaires communes applicables aux enseignants-chercheurs et portant statut particulier du corps des professeurs des universités et du corps des maîtres de conférences : " Le comité de sélection examine les dossiers des maîtres de conférences ou professeurs postulant à la nomination dans l'emploi par mutation et des candidats à cette nomination par détachement et par recrutement au concours parmi les personnes inscrites sur la liste de qualification aux fonctions, selon le cas, de maître de conférences ou de professeur des universités. Au vu de rapports pour chaque candidat présentés par deux de ses membres, le comité établit la liste des candidats qu'il souhaite entendre. Les motifs pour lesquels leur candidature n'a pas été retenue sont communiqués aux candidats qui en font la demande. (...) / Après avoir procédé aux auditions, le comité de sélection délibère sur les candidatures et, par un avis motivé unique portant sur l'ensemble des candidats, arrête la liste, classée par ordre de préférence, de ceux qu'il retient. Le comité de sélection se prononce à la majorité des voix des membres présents. En cas de partage des voix, le président du comité a voix prépondérante (...) " ; qu'il résulte de ces dispositions que, chaque fois que le comité de sélection, qui a la qualité de jury de concours, statue sur une candidature individuelle, soit pour décider ou non de procéder à l'audition du candidat, soit pour statuer sur sa candidature à l'issue de son audition, une éventuelle abstention doit être regardée comme traduisant un vote défavorable ; que, dans tous les cas, le président a voix prépondérante en cas de partage des voix ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier que, par une délibération du 4 mai 2016, le comité de sélection de l'université de Lorraine a décidé, après avoir pris connaissance des deux rapports consacrés à la candidature de M. B...au poste de professeur en archéologie médiévale et patrimoine, de ne pas l'auditionner ; que, M. B...étant le seul candidat, le comité de sélection n'a, en conséquence, pas établi de liste de candidats auditionnés, ni a fortiori de liste de candidats retenus, ni transmis d'avis favorable au conseil d'administration de l'université ; que le conseil d'administration de l'université, réuni en formation restreinte, a, par une délibération en date du 31 mai 2016, pris acte de cette délibération du comité de sélection ; que, dans le dernier état de ses conclusions, M. B...demande l'annulation pour excès de pouvoir de ces deux délibérations ainsi que de la décision implicite de refus opposée par le président de l'université à sa demande tendant à ce que le comité de sélection délibère à nouveau sur sa candidature ;<br/>
<br/>
              Sur les conclusions de M. B...dirigées contre la délibération du conseil d'administration de l'université de Lorraine :<br/>
<br/>
              3. Considérant que la délibération du conseil d'administration de l'université de Lorraine en date du 31 mai 2016, qui se borne à prendre acte de la décision du comité de sélection de l'université de Lorraine de ne pas auditionner le candidat et de ne pas établir de liste des candidats retenus, n'a pas le caractère d'une décision faisant grief ; que, dès lors, et sans qu'il soit besoin de statuer sur les autres fins de non-recevoir soulevées par l'université de Lorraine, les conclusions de M. B...dirigées contre cette délibération doivent être rejetées comme irrecevables ;<br/>
<br/>
              Sur les conclusions de M. B...dirigées contre la délibération du comité de sélection de l'université de Lorraine et contre la décision du président de l'université rejetant son recours :<br/>
<br/>
              4. Considérant, en premier lieu, qu'il ressort des pièces du dossier que, lors du vote au cours duquel le comité de sélection s'est prononcé sur son souhait d'entendre ou non M. B..., quatre membres sur huit ont voté en faveur de l'audition, un membre s'est abstenu et trois membres, dont le président du comité, ont voté contre l'audition ; qu'il résulte de ce qui a été dit au point 1 qu'en refusant, à l'issue de ce vote, d'auditionner le requérant, le comité de sélection de l'université de Lorraine a fait une exacte application des dispositions de l'article 9-2 du décret du 6 juin 1984 ;<br/>
<br/>
              5. Considérant, en deuxième lieu, qu'il ressort des pièces du dossier que le comité de sélection s'est fondé, pour écarter la candidature de M.B..., sur des motifs tenant à l'adéquation de sa candidature au profil du poste ; que la seule circonstance que les rapporteurs ont fait mention de ce qu'il était en fin de carrière n'est pas susceptible de faire présumer que sa candidature aurait été écartée en raison de son âge ;<br/>
<br/>
              6. Considérant, enfin, qu'en estimant que M.B..., qualifié aux fonctions de professeur des universités au titre de la 72ème section du Conseil national des universités " Epistémologie, histoire des sciences et des techniques ", n'était pas un spécialiste d'archéologie médiévale et que, malgré sa forte expérience pédagogique, son profil était éloigné des exigences du poste qui relevaient de l'archéologie médiévale, le comité de sélection n'a pas entaché sa délibération d'une erreur manifeste d'appréciation ;<br/>
<br/>
              7. Considérant qu'il résulte de tout ce qui précède que les conclusions de M. B... tendant à l'annulation de la délibération du 4 mai 2016 du comité de sélection de l'université de Lorraine et de la décision implicite de rejet de son recours tendant à ce que le président de cette université fasse délibérer à nouveau le comité de sélection sur sa candidature ne peuvent qu'être rejetées ainsi que, par voie de conséquence, ses conclusions à fin d'injonction ;<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : La requête de M. B...est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. A...B..., à l'université de Lorraine et à la ministre de l'enseignement supérieur, de la recherche et de l'innovation.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">30-02-05-01-06-01-02 ENSEIGNEMENT ET RECHERCHE. QUESTIONS PROPRES AUX DIFFÉRENTES CATÉGORIES D'ENSEIGNEMENT. ENSEIGNEMENT SUPÉRIEUR ET GRANDES ÉCOLES. UNIVERSITÉS. GESTION DES UNIVERSITÉS. GESTION DU PERSONNEL. RECRUTEMENT. - RECRUTEMENT DES ENSEIGNANTS-CHERCHEURS (ART. L. 952-6-1 DU CODE DE L'ÉDUCATION) - DÉLIBÉRATION DU COMITÉ DE SÉLECTION SUR UNE CANDIDATURE INDIVIDUELLE - COMPTABILISATION DES VOTES - 1) ABSTENTION - VOTE DÉFAVORABLE - 2) PARTAGE DES VOIX - VOIX PRÉPONDÉRANTE DU PRÉSIDENT.
</SCT>
<ANA ID="9A"> 30-02-05-01-06-01-02 1) Il résulte des articles L. 952-6-1 du code de l'éducation et 9-2 du décret n° 84-431 du 6 juin 1984 que, chaque fois que le comité de sélection, qui a la qualité de jury de concours [RJ1], statue sur une candidature individuelle, soit pour décider ou non de procéder à l'audition du candidat, soit pour statuer sur sa candidature à l'issue de son audition, une éventuelle abstention doit être regardée comme traduisant un vote défavorable.... ...2) Dans tous les cas, le président a voix prépondérante en cas de partage des voix.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 15 décembre 2010, Syndicat national de l'enseignement supérieur et autres, n°s 316927 316986, p. 494.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
