<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042659679</ID>
<ANCIEN_ID>JG_L_2020_12_000000441376</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/65/96/CETATEXT000042659679.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 10/12/2020, 441376</TITRE>
<DATE_DEC>2020-12-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>441376</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>M. Bruno Delsol</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:441376.20201210</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. C... A... B... a demandé à la Cour nationale du droit d'asile d'annuler la décision du 16 juillet 2019 par laquelle l'Office français de protection des réfugiés et apatrides (OFPRA) a rejeté sa demande d'asile et, à titre principal, de faire droit à sa demande d'asile ou, à titre subsidiaire, de renvoyer l'examen de sa demande à l'OFPRA. <br/>
<br/>
              Par une décision n° 19042072 du 3 mars 2020, la Cour nationale du droit d'asile a annulé la décision de l'OFPRA, renvoyé l'examen de la demande d'asile de M. A... B... à l'OFPRA et rejeté le surplus des conclusions de la demande de M. A... B.... <br/>
<br/>
              Par un pourvoi enregistré le 23 juin 2020 au secrétariat du contentieux du Conseil d'Etat, l'OFPRA demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette décision ;<br/>
<br/>
              2°) de renvoyer l'affaire à la Cour nationale du droit d'asile.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention de Genève du 28 juillet 1951 relative au statut des réfugiés et le protocole signé à New-York le 31 janvier 1967 ; <br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;  <br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bruno Delsol, Conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Foussard, Froger, avocat de l'office français de protection des réfugiés et apatrides ;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis à la Cour nationale du droit d'asile que M. A... B..., de nationalité somalienne, a présenté le 7 novembre 2018 une demande d'asile, qui a été rejetée comme irrecevable par une décision du 16 juillet 2019 de l'Office français de protection des réfugiés et apatrides (OFPRA) au motif qu'il bénéficie déjà d'une protection effective au titre de la protection subsidiaire qui lui a été accordée par la République de Malte, Etat membre de l'Union européenne. La Cour nationale du droit d'asile a annulé cette décision et a renvoyé à l'OFPRA l'examen de la demande de M. A... B..., par une décision du 3 mars 2020 contre laquelle l'OFPRA se pourvoit en cassation. <br/>
<br/>
              2. Aux termes de l'article L. 723-6 du code de l'entrée et du séjour des étrangers et du droit d'asile : " L'office convoque, par tout moyen garantissant la confidentialité et la réception personnelle par le demandeur, le demandeur à un entretien personnel (...) ". Aux termes de l'article L. 723-11 du même code : " L'office peut prendre une décision d'irrecevabilité écrite et motivée, sans vérifier si les conditions d'octroi de l'asile sont réunies, dans les cas suivants : / 1° Lorsque le demandeur bénéficie d'une protection effective au titre de l'asile dans un Etat membre de l'Union européenne ; (...) ". Aux termes de l'avant-dernier alinéa de ce même article : " Lors de l'entretien personnel prévu à l'article L. 723-6, le demandeur est mis à même de présenter ses observations sur l'application du motif d'irrecevabilité mentionné aux 1° ou 2° du présent article à sa situation personnelle ".<br/>
<br/>
              3. Par ailleurs, aux termes de l'article L. 733-5 du même code : " Saisie d'un recours contre une décision du directeur général de l'Office français de protection des réfugiés et apatrides, la Cour nationale du droit d'asile statue, en qualité de juge de plein contentieux, sur le droit du requérant à une protection au titre de l'asile au vu des circonstances de fait dont elle a connaissance au moment où elle se prononce. / La cour ne peut annuler une décision du directeur général de l'office et lui renvoyer l'examen de la demande d'asile que lorsqu'elle juge que l'office a pris cette décision sans procéder à un examen individuel de la demande ou en se dispensant, en dehors des cas prévus par la loi, d'un entretien personnel avec le demandeur et qu'elle n'est pas en mesure de prendre immédiatement une décision positive sur la demande de protection au vu des éléments établis devant elle. / Il en va de même lorsque la cour estime que le requérant a été dans l'impossibilité de se faire comprendre lors de l'entretien, faute d'avoir pu bénéficier du concours d'un interprète dans la langue qu'il a indiquée dans sa demande d'asile ou dans une autre langue dont il a une connaissance suffisante, et que ce défaut d'interprétariat est imputable à l'office (...) ". Il en va également de même lorsque la Cour juge que, en méconnaissance des dispositions de l'article L. 723-11 citées au point 2, l'entretien personnel du demandeur d'asile n'a pas porté sur l'application à sa situation personnelle du motif d'irrecevabilité qui lui a été opposé.<br/>
<br/>
              4. Il ressort des énonciations de la décision attaquée que, pour juger que M. A... B... n'avait pas été mis à même de présenter ses observations sur l'application à sa situation personnelle du motif d'irrecevabilité de sa demande, tiré de ce qu'il bénéficierait déjà d'une protection effective à Malte, la Cour a relevé qu'il ne lui avait pas été expressément signifié, lors de son entretien, que l'OFPRA envisageait de soulever ce motif d'irrecevabilité. En statuant ainsi alors qu'elle devait seulement rechercher si, en application des dispositions de l'article L. 723-11 citées au point 2, l'entretien avait porté sur les motifs d'irrecevabilité de sa demande, la Cour a entaché sa décision d'une erreur de droit.<br/>
<br/>
              5. Il résulte de ce qui précède, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, que l'Office français de protection des réfugiés et apatrides est fondé à demander l'annulation de la décision qu'il attaque.  <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : La décision du 3 mars 2020 de la Cour nationale du droit d'asile est annulée. <br/>
Article 2 : L'affaire est renvoyée à la Cour nationale du droit d'asile.<br/>
Article 3 : La présente décision sera notifiée à l'Office français de protection des réfugiés et apatrides et à M. C... A... B....<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">095-08-05-02 - ENTRETIEN PERSONNEL N'AYANT PAS PORTÉ SUR L'APPLICATION À LA SITUATION PERSONNELLE DU DEMANDEUR DU MOTIF D'IRRECEVABILITÉ QUI LUI A ÉTÉ OPPOSÉ - 1) MOYEN DE NATURE À JUSTIFIER L'ANNULATION DE LA DÉCISION DE L'OFPRA (ART. L. 733-5 DU CESEDA) - EXISTENCE [RJ1] - 2) ESPÈCE - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-07-03 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. POUVOIRS DU JUGE DE PLEIN CONTENTIEUX. - RECOURS DIRIGÉ CONTRE UNE DÉCISION DU DIRECTEUR GÉNÉRAL DE L'OFPRA REFUSANT DE RECONNAÎTRE LA QUALITÉ DE RÉFUGIÉ OU D'ACCORDER LA PROTECTION SUBSIDIAIRE - OFFICE DU JUGE - MOYEN TIRÉ DE CE QUE L'ENTRETIEN PERSONNEL N'A PAS PORTÉ SUR L'APPLICATION À LA SITUATION PERSONNELLE DU DEMANDEUR DU MOTIF D'IRRECEVABILITÉ QUI LUI A ÉTÉ OPPOSÉ - 1) MOYEN DE NATURE À JUSTIFIER L'ANNULATION DE LA DÉCISION DE L'OFPRA (ART. L. 733-5 DU CESEDA) - EXISTENCE [RJ1] - 2) ESPÈCE - ABSENCE.
</SCT>
<ANA ID="9A"> 095-08-05-02 Article L. 733-5 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA) prévoyant les cas dans lesquels, par exception, la Cour nationale du droit d'asile (CNDA) peut annuler la décision du directeur général (DG) de l'Office français de protection des réfugiés et apatrides (OFPRA) et lui renvoyer l'examen de la demande d'asile.,,,1) Il en va également ainsi lorsque la CNDA juge que, en méconnaissance de l'article L.723-11 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA), l'entretien personnel du demandeur d'asile n'a pas porté sur l'application à sa situation personnelle du motif d'irrecevabilité qui lui a été opposé.,,,2) La seule circonstance qu'un demandeur d'asile ne s'est pas vu expressément signifier, lors de son entretien, que l'OFPRA envisagait de soulever un motif d'irrecevabilité tiré de ce qu'il bénéficierait déjà d'une protection effective dans un autre pays, n'est pas de nature à permettre à la CNDA, qui devait seulement rechercher si, en application de l'article L.723-11 CESEDA, l'entretien avait porté sur les motifs d'irrecevabilité de sa demande, d'annuler la décision du DG de l'OFPRA au motif que le demandeur n'aurait pas été mis à même de présenter ses observations sur l'application à sa situation personnelle du motif d'irrecevabilité de sa demande.</ANA>
<ANA ID="9B"> 54-07-03 Article L. 733-5 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA) prévoyant les cas dans lesquels, par exception, la Cour nationale du droit d'asile (CNDA) peut annuler la décision du directeur général (DG) de l'Office français de protection des réfugiés et apatrides (OFPRA) et lui renvoyer l'examen de la demande d'asile.,,,1) Il en va également ainsi lorsque la CNDA juge que, en méconnaissance de l'article L.723-11 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA), l'entretien personnel du demandeur d'asile n'a pas porté sur l'application à sa situation personnelle du motif d'irrecevabilité qui lui a été opposé.,,,2) La seule circonstance qu'un demandeur d'asile ne s'est pas vu expressément signifier, lors de son entretien, que l'OFPRA envisagait de soulever un motif d'irrecevabilité tiré de ce qu'il bénéficierait déjà d'une protection effective dans un autre pays, n'est pas de nature à permettre à la CNDA, qui devait seulement rechercher si, en application de l'article L.723-11 CESEDA, l'entretien avait porté sur les motifs d'irrecevabilité de sa demande, d'annuler la décision du DG de l'OFPRA au motif que le demandeur n'aurait pas été mis à même de présenter ses observations sur l'application à sa situation personnelle du motif d'irrecevabilité de sa demande.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., avant la reprise de ces exceptions par l'article L. 733-5 du CESEDA, s'agissant du moyen tiré de l'absence d'audition du demandeur, CE, 10 octobre 2013, OFPRA c/ M.,, n°s 362798 362799, p. 254 ; s'agissant de l'impossibilité pour le demandeur de se faire comprendre lors de l'entretien, faute d'avoir pu bénéficier du concours d'un interprète, CE, 22 juin 2017, M.,, n° 400366, T. pp. 478-768 ; CJUE, 16 juillet 2020, Milkiyas Addis,c/ Bundesrepublik Deutschland, aff. C-517/17.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
