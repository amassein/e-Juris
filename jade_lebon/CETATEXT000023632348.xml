<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000023632348</ID>
<ANCIEN_ID>JG_L_2011_02_000000312592</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/23/63/23/CETATEXT000023632348.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème et 9ème sous-sections réunies, 23/02/2011, 312592</TITRE>
<DATE_DEC>2011-02-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>312592</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème et 9ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PEIGNOT, GARREAU ; SCP DE CHAISEMARTIN, COURJON</AVOCATS>
<RAPPORTEUR>M. Thierry  Carriol</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Hedary Delphine</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2011:312592.20110223</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 28 janvier et 28 avril 2008 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. B... A..., demeurant..., ; M.  A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement du 13 novembre 2007 par lequel le tribunal administratif de la Polynésie française a rejeté sa demande tendant à l'annulation de la décision implicite de la Polynésie française rejetant sa demande du 12 février 2006 tendant au versement de la seconde fraction de l'indemnité d'éloignement afférente à son second séjour dans ce territoire ;<br/>
<br/>
              2°) réglant l'affaire au fond, d'annuler cette décision implicite et de condamner la Polynésie française à lui verser l'indemnité d'éloignement sollicitée ;<br/>
<br/>
              3°) de mettre à la charge de la Polynésie française le versement d'une somme de 3 000 euros en application de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi n° 50-772 du 30 juin 1950 ;<br/>
<br/>
              Vu le décret n° 96-1028 du 27 novembre 1996 ;<br/>
<br/>
              Vu la délibération n° 98-145 APF du 10 septembre 1998 de l'assemblée de la Polynésie française ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Thierry Carriol, chargé des fonctions de Maître des Requêtes,<br/>
<br/>
              - les observations de la SCP Peignot, Garreau, avocat de M. A...et de la SCP de Chaisemartin, Courjon, avocat de la Polynésie française,<br/>
<br/>
              - les conclusions de Mme Delphine Hedary, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Peignot, Garreau, avocat de M.  A...et à la SCP de Chaisemartin, Courjon, avocat de la Polynésie française ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant qu'aux termes de l'article 2 de la loi du 30 juin 1950 fixant les conditions d'attribution des soldes et indemnités des fonctionnaires civils et militaires relevant du ministère de la France d'outre-mer : " Pour faire face aux sujétions particulières inhérentes à l'exercice de la fonction publique dans les territoires d'outre mer, les fonctionnaires civils (...) recevront : (...) / 2° Une indemnité destinée à couvrir les sujétions résultant de l'éloignement pendant le séjour et les charges afférentes au retour, accordée au personnel appelé à servir en dehors soit de la métropole, soit de son territoire, soit du pays ou territoire où il réside habituellement, qui sera déterminée pour chaque catégorie de cadres à un taux uniforme s'appliquant au traitement et majorée d'un supplément familial. Elle sera fonction de la durée du séjour et de l'éloignement et versée pour chaque séjour administratif, moitié avant le départ et moitié à l'issue du séjour (...) " ; que le décret du 27 novembre 1996 relatif à l'attribution de l'indemnité d'éloignement prévoit, à son article 4, que : " Le droit à indemnité pour les personnels qui sont affectés sans limitation de durée (...) en Polynésie Française (...) n'est ouvert que pour deux périodes de deux ans (...). / Les intéressés n'acquièrent un nouveau droit à l'indemnité pour une nouvelle affectation (...) en Polynésie Française (...) qu'après une période de services de deux ans au moins accomplie en dehors de toute collectivité ouvrant droit à indemnité " ; qu'aux termes de l'article 14 de la délibération du 10 septembre 1998 de l'assemblée de la Polynésie française relative au régime applicable aux fonctionnaires civils et militaires en position de détachement auprès du territoire de la Polynésie française et de ses établissements publics : " Pendant les deux premiers séjours éventuels de deux ans, le fonctionnaire détaché auprès du territoire bénéficie des mêmes droits, servis dans les mêmes conditions, que ceux que l'Etat accorde à ses fonctionnaires affectés en Polynésie française. / Au-delà de cette période, le fonctionnaire détaché perd tous droits en la matière, sauf, le cas échéant, à pouvoir à nouveau y prétendre dans les mêmes conditions que précédemment, en cas d'un autre détachement séparé de la fin du premier par une période minimale de deux ans passée hors du territoire " ; que ces dispositions instituent, non deux indemnités distinctes obéissant chacune à leurs règles propres, mais une indemnité unique, attribuée pour au plus deux périodes de deux ans chacune, et payable en deux fractions, chacune de ces fractions constituant pour son bénéficiaire une créance liquide et exigible à chacune des échéances prévues au 2° de l'article 2 de la loi du 30 juin 1950 pour la prise en charge des frais de déménagement et de retour ; que par suite, un agent qui continue de résider outre-mer après l'expiration des deux périodes prévues par ces textes ne peut recevoir la seconde fraction de l'indemnité d'éloignement qu'il a perçue lors de son arrivée sur le territoire ; que la circonstance que l'intéressé revienne en métropole ultérieurement, le cas échéant, après un nouveau séjour administratif, est sans incidence sur les règles d'attribution de l'indemnité d'éloignement ; que dès lors, en jugeant, après avoir relevé que M.A..., inspecteur de l'éducation nationale, avait choisi, après avoir servi pendant deux séjours administratifs, d'effectuer d'autres séjours administratifs sans quitter la Polynésie française avant la fin du dernier, qu'il ne pouvait légalement réclamer le bénéfice de la seconde fraction de l'indemnité d'éloignement afférente à son second séjour administratif, qu'il estimait lui être due, le tribunal administratif n'a pas entaché son jugement d'erreur de droit ; que, dès lors, M. A...n'est pas fondé à en demander l'annulation ;<br/>
<br/>
              Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'il soit mis à la charge de la Polynésie française qui n'est pas, dans la présente instance, la partie perdante, le versement de la somme que demande M.  A...au titre des frais exposés par lui et non compris dans les dépens ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par la Polynésie française au titre des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>				D E C I D E  :<br/>
              				--------------<br/>
<br/>
Article 1er : Le pourvoi de M. A...est rejeté.<br/>
Article 2 : Les conclusions de la Polynésie française tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à M. B...A...et au président de la Polynésie française.<br/>
Copie en sera adressée pour information au ministre de l'éducation nationale, de la jeunesse et de la vie associative.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-08-03-02 FONCTIONNAIRES ET AGENTS PUBLICS. RÉMUNÉRATION. INDEMNITÉS ET AVANTAGES DIVERS. INDEMNITÉS ALLOUÉES AUX FONCTIONNAIRES SERVANT OUTRE-MER (VOIR : OUTRE-MER). - INDEMNITÉ D'ÉLOIGNEMENT - CONDITIONS D'ATTRIBUTION - 1) INDEMNITÉ UNIQUE, ATTRIBUÉE POUR AU PLUS DEUX PÉRIODES DE DEUX ANS CHACUNE - CONSÉQUENCE - AGENT QUI CONTINUE DE RÉSIDER OUTRE-MER APRÈS L'EXPIRATION DES DEUX PÉRIODES - IMPOSSIBILITÉ DE PERCEVOIR LA SECONDE FRACTION DE L'INDEMNITÉ D'ÉLOIGNEMENT - 2) RETOUR ULTÉRIEUR EN MÉTROPOLE - CONSÉQUENCE - ABSENCE D'INCIDENCE SUR LES RÈGLES D'ATTRIBUTION DE L'INDEMNITÉ D'ÉLOIGNEMENT.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">46-01-09-06-04 OUTRE-MER. DROIT APPLICABLE. DROIT APPLICABLE AUX FONCTIONNAIRES SERVANT OUTRE-MER. RÉMUNÉRATION. INDEMNITÉ D'ÉLOIGNEMENT DES FONCTIONNAIRES SERVANT OUTRE-MER. - CONDITIONS D'ATTRIBUTION - 1) INDEMNITÉ UNIQUE, ATTRIBUÉE POUR AU PLUS DEUX PÉRIODES DE DEUX ANS CHACUNE - CONSÉQUENCE - AGENT QUI CONTINUE DE RÉSIDER OUTRE-MER APRÈS L'EXPIRATION DES DEUX PÉRIODES - IMPOSSIBILITÉ DE PERCEVOIR LA SECONDE FRACTION DE L'INDEMNITÉ D'ÉLOIGNEMENT - 2) RETOUR ULTÉRIEUR EN MÉTROPOLE - CONSÉQUENCE - ABSENCE D'INCIDENCE SUR LES RÈGLES D'ATTRIBUTION DE L'INDEMNITÉ D'ÉLOIGNEMENT.
</SCT>
<ANA ID="9A"> 36-08-03-02 1) Les dispositions de l'article 2 de la loi n° 50-772 du 30 juin 1950 fixant les conditions d'attribution des soldes et indemnités des fonctionnaires civils et militaires relevant du ministère de la France d'outre-mer, de l'article 4 du décret n° 96-1028 du 27 novembre 1996 relatif à l'attribution de l'indemnité d'éloignement et de l'article 14 de la délibération du 10 septembre 1998 de l'assemblée de la Polynésie française relative au régime applicable aux fonctionnaires civils et militaires en position de détachement auprès du territoire de la Polynésie française et de ses établissements publics instituent non pas deux indemnités distinctes obéissant chacune à leurs règles propres, mais une indemnité unique, attribuée pour au plus deux périodes de deux ans chacune, et payable en deux fractions, chacune de ces fractions constituant pour son bénéficiaire une créance liquide et exigible à chacune des échéances prévues au 2° de l' article 2 de la loi du 30 juin 1950 pour la prise en charge des frais de déménagement et de retour. Par suite, un agent qui continue de résider outre-mer après l'expiration des deux périodes prévues par ces textes ne peut recevoir la seconde fraction de l'indemnité d'éloignement qu'il a perçue lors de son arrivée sur le territoire.... ...2) La circonstance que l'intéressé revienne en métropole ultérieurement, le cas échéant, après un nouveau séjour administratif, est sans incidence sur les règles d'attribution de l'indemnité d'éloignement.</ANA>
<ANA ID="9B"> 46-01-09-06-04 1) Les dispositions de l'article 2 de la loi n° 50-772 du 30 juin 1950 fixant les conditions d'attribution des soldes et indemnités des fonctionnaires civils et militaires relevant du ministère de la France d'outre-mer, de l'article 4 du décret n° 96-1028 du 27 novembre 1996 relatif à l'attribution de l'indemnité d'éloignement et de l'article 14 de la délibération du 10 septembre 1998 de l'assemblée de la Polynésie française relative au régime applicable aux fonctionnaires civils et militaires en position de détachement auprès du territoire de la Polynésie française et de ses établissements publics instituent non pas deux indemnités distinctes obéissant chacune à leurs règles propres, mais une indemnité unique, attribuée pour au plus deux périodes de deux ans chacune, et payable en deux fractions, chacune de ces fractions constituant pour son bénéficiaire une créance liquide et exigible à chacune des échéances prévues au 2° de l' article 2 de la loi du 30 juin 1950 pour la prise en charge des frais de déménagement et de retour. Par suite, un agent qui continue de résider outre-mer après l'expiration des deux périodes prévues par ces textes ne peut recevoir la seconde fraction de l'indemnité d'éloignement qu'il a perçue lors de son arrivée sur le territoire.... ...2) La circonstance que l'intéressé revienne en métropole ultérieurement, le cas échéant, après un nouveau séjour administratif, est sans incidence sur les règles d'attribution de l'indemnité d'éloignement.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
