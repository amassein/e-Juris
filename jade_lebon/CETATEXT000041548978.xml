<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041548978</ID>
<ANCIEN_ID>JG_L_2020_02_000000419790</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/54/89/CETATEXT000041548978.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 6ème chambres réunies, 05/02/2020, 419790</TITRE>
<DATE_DEC>2020-02-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>419790</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS ; SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>M. Jean-Dominique Langlais</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2020:419790.20200205</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B... D..., Mme H... D... et M. E... G... ont demandé au tribunal administratif d'Amiens d'annuler pour excès de pouvoir l'arrêté du 28 mars 2010 par lequel le préfet de l'Oise a accordé à M. I... F... l'autorisation d'exploiter des terres d'une surface de 171 hectares et 25 centiares situées sur le territoire des communes de Nanteuil-le-Haudoin, Rouville, Baron, Levignen, Russy-Bemont et Cuvergnon dans le département de l'Oise et sur le territoire des communes de Dammarie, La Bourdinière Saint-Loup et Fresnay-le-Conte dans le département de l'Eure-et-Loir. Par un jugement n° 1001506 du 29 mai 2012, le tribunal administratif d'Amiens a annulé l'arrêté attaqué.<br/>
<br/>
              Par un arrêt n° 12DA01168 du 3 octobre 2013, la cour administrative d'appel de Douai a, sur appel de M. F..., annulé ce jugement en tant qu'il annule l'arrêté du 28 mars 2010 en tant qu'il porte sur " 115 hectares 28 ares 82 centiares " situés dans le département de l'Oise et rejeté le surplus de la requête.<br/>
<br/>
              Par une décision n° 373816 du 20 novembre 2015, le Conseil d'Etat, statuant au contentieux a, sur pourvoi de M. F..., annulé cet arrêt et renvoyé l'affaire devant la cour administrative d'appel.<br/>
<br/>
              Par un arrêt n° 15DA01850 du 6 février 2018, la cour administrative d'appel, statuant sur renvoi du Conseil d'Etat, a annulé le jugement du 29 mai 2012 en tant qu'il annule l'arrêté du 28 mars 2010 en tant qu'il autorise l'exploitation des terres situées dans le département de l'Oise et rejeté le surplus de la requête de M. F.... <br/>
<br/>
              	 Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 12 avril et 10 juillet 2018 au secrétariat du contentieux du Conseil d'Etat, M. F... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
              	3°) de mettre à la charge de M. et Mme D... et de M. G... la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code rural et de la pêche maritime ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Langlais, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de M. F... et à la SCP Rocheteau, Uzan-Sarano, avocat de M. et Mme D... et de M. G....<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Aux termes de l'article L. 331-2 du code rural et de la pêche maritime, dans sa rédaction applicable à l'espèce : " Sont soumises à autorisation préalable les opérations suivantes : / 1° Les installations, les agrandissements ou les réunions d'exploitations agricoles au bénéfice d'une exploitation agricole mise en valeur par une ou plusieurs personnes physiques ou morales, lorsque la surface totale qu'il est envisagé de mettre en valeur excède le seuil fixé par le schéma directeur départemental des structures (...) / 5° Les agrandissements ou réunions d'exploitations pour les biens dont la distance par rapport au siège de l'exploitation du demandeur est supérieure à un maximum fixé par le schéma directeur départemental  des structures, sans que ce maximum puisse être inférieur à cinq kilomètres (...)  ". Aux termes de l'article L. 331-3 du même code, dans sa rédaction applicable : " L'autorité administrative se prononce sur la demande d'autorisation en se conformant aux orientations définies par le schéma directeur départemental des structures agricoles applicable dans le département dans lequel se situe le fonds faisant l'objet de la demande. Elle doit notamment (...) / 7° Prendre en compte la structure parcellaire des exploitations concernées (...) par rapport au siège de l'exploitation (...) ".<br/>
<br/>
              2. Il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 28 mars 2010 pris après consultation du préfet d'Eure-et-Loir, le préfet de l'Oise a délivré à M. F..., sur le fondement des dispositions citées ci-dessus de l'article L. 331-2 du code rural et de la pêche maritime, une autorisation d'exploiter des terres d'une superficie d'environ cent soixante et onze hectares, réparties entre ces deux départements. Par un jugement du 29 mai 2012, le tribunal administratif d'Amiens a, à la demande de M. et Mme D... et de M. G..., propriétaires d'une partie de ces terres, annulé cet arrêté dans sa totalité. Par un arrêt du 6 février 2018, la cour administrative d'appel de Douai a, sur appel de M. F..., annulé ce jugement en tant qu'il annule l'autorisation d'exploiter les terres situées dans le département de l'Oise. <br/>
<br/>
              3. M. F... se pourvoit en cassation contre cet arrêt du 6 février 2018 en tant qu'il annule l'autorisation d'exploiter les terres situées dans le département d'Eure-et-Loir. <br/>
<br/>
              Sur la recevabilité des conclusions de première instance :<br/>
<br/>
              4. Il ressort des pièces du dossier soumis aux juges du fond que M. et Mme D... et M. G... sont propriétaires d'environ cinquante-cinq hectares de terres agricoles dans le département d'Eure-et-Loir, qui constituent une partie seulement des soixante-neuf hectares et cinquante ares de terres agricoles que le préfet de l'Oise a, par l'arrêté litigieux du 28 mars 2010, autorisé M. F... à exploiter dans ce département. Ils n'avaient, par suite, d'intérêt leur donnant qualité à agir contre l'arrêté litigieux qu'en tant qu'il autorisait l'exploitation par M. F... des terres dont ils étaient propriétaires.<br/>
<br/>
              5. M. F... est, par suite, fondé à demander l'annulation de l'arrêt qu'il attaque en tant qu'il rejette sa requête dirigée contre le jugement du 29 mai 2012 du tribunal administratif d'Amiens en ce qu'il fait droit aux conclusions de M. et Mme D... et de M. G... dirigées contre l'arrêté litigieux en ce qu'il concerne les terres dont ces derniers ne sont pas propriétaires.<br/>
<br/>
              Sur le surplus des conclusions du pourvoi : <br/>
<br/>
              6. En premier lieu, il résulte des dispositions du I de l'article L. 331-2 du code rural et de la pêche maritime, dans leur rédaction applicable à l'espèce, que les critères qu'elles fixent pour déterminer celles des opérations qui sont soumises à autorisation administrative préalable s'appliquent indépendamment de ceux, prévus par l'article L. 331-3 du même code, au vu desquels le préfet se prononce sur les demandes d'autorisation qui lui sont soumises. Ainsi, en jugeant que le critère prévu au 7° de ce dernier article, qui impose au préfet de " prendre en compte la structure parcellaire des exploitations concernées (...) par rapport au siège de l'exploitation ", était applicable à l'opération litigieuse, alors même qu'elle ne relevait pas de celles qui sont soumises à autorisation en application du critère de distance fixé par le 5° de l'article L. 331-2, la cour n'a pas commis d'erreur de droit.<br/>
<br/>
              7. En second lieu, en estimant que la distance de cent soixante kilomètres entre le siège de l'exploitation de M. F..., situé dans l'Oise, et les terres d'Eure-et-Loir dont il sollicitait l'exploitation était excessive et constituait un obstacle à une mise en valeur rationnelle, directe et personnelle des terres, la cour a porté sur les pièces du dossier qui lui était soumis une appréciation souveraine, qui, en dépit de la nature de la culture envisagée et des pratiques des anciens preneurs, est exempte de dénaturation.<br/>
<br/>
              8. Il résulte de tout ce qui précède que l'arrêt de la cour administrative d'appel de Douai doit être annulé en tant seulement qu'il rejette l'appel formé par M. F... contre le jugement du 29 mai 2012 du tribunal administratif d'Amiens en ce qu'il annule l'arrêté préfectoral du 28 mars 2010 en tant qu'il concerne les parcelles, d'une superficie de 13 ha 53 a 82 ca, situées en Eure-et-Loir et n'appartenant pas à M. et Mme D... ou à M. G....<br/>
<br/>
              9. Aux termes du second alinéa de l'article L. 821-2 du code de justice administrative : " Lorsque l'affaire fait l'objet d'un second pourvoi en cassation, le Conseil d'Etat statue définitivement sur cette affaire ". Le Conseil d'Etat étant saisi, en l'espèce, d'un second pourvoi en cassation, il lui incombe de régler l'affaire au fond, dans la mesure de la cassation prononcée. <br/>
<br/>
              10. Ainsi qu'il a été dit ci-dessus, M. et Mme D... et Mme G... ne justifient pas, en leur qualité de propriétaires de terres agricoles, d'un intérêt à agir contre l'arrêté du 28 mars 2010 en tant qu'il autorise M. F... à exploiter des terres dont ils ne sont pas propriétaires. Par suite, M. F... est fondé à demander l'annulation du jugement du 29 mai 2012 du tribunal administratif d'Amiens en ce qu'il annule l'arrêté préfectoral du 28 mars 2010 en tant qu'il concerne les terres situées en Eure-et-Loir n'appartenant pas à M. et Mme D... ou à M. G....<br/>
<br/>
              11. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de M. D..., de Mme D... et de M. G... la somme de 1 000 euros chacun à verser à M. F... au titre de l'article L. 761-1 du code de justice administrative. Il n'y a pas lieu, en revanche, de mettre à la charge de ce dernier la somme demandée au même titre par M. et Mme D... et M. G....<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 6 février 2018 de la cour administrative d'appel de Douai est annulé en tant qu'il rejette les conclusions d'appel de M. F... dirigées contre le jugement du 29 mai 2012 du tribunal administratif d'Amiens en ce qu'il annule l'arrêté du 28 mars 2010 du préfet de l'Oise en tant qu'il concerne la partie des terres situées en Eure-et-Loir, d'une contenance de 13 ha 53 a 82 ca, n'appartenant pas à M. et Mme D... ou à M. G....<br/>
<br/>
Article 2 : Le jugement du 29 mai 2012 du tribunal administratif d'Amiens est annulé en ce qu'il annule l'arrêté du 28 mars 2010 du préfet de l'Oise en tant qu'il concerne la partie des terres situées en Eure-et-Loir, d'une superficie de 13 ha 53 a 82 ca, n'appartenant pas à M. et Mme D... ou à M. G....<br/>
<br/>
Article 3 : M. D..., Mme D... et M. G... verseront, chacun, une somme de 1 000 euros à M. F... au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
		Article 4 : Le surplus des conclusions du pourvoi est rejeté.<br/>
<br/>
Article 5 : Les conclusions présentées par M. et Mme D... et M. G... au titre de l'article L. 761-1 du code de justice administrative, sont rejetées. <br/>
<br/>
Article 6 : La présente décision sera notifiée à M. I... F..., à M. A... D..., à Mme C... D... et à M. E... G....<br/>
		Copie en sera adressée au ministre de l'agriculture et de l'alimentation.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">03-03-03-01-06 AGRICULTURE ET FORÊTS. EXPLOITATIONS AGRICOLES. CUMULS ET CONTRÔLE DES STRUCTURES. CUMULS D'EXPLOITATIONS. CONTENTIEUX. - AUTORISATION D'EXPLOITER DES PARCELLES SUR DES TERRES (1° DE L'ART. L. 331-2 DU CRPM) - INTÉRÊT À AGIR DU PROPRIÉTAIRE DES TERRES - EXISTENCE, EN TANT UNIQUEMENT QUE L'AUTORISATION PORTE SUR LES PARCELLES DONT IL EST PROPRIÉTAIRE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-01-04 PROCÉDURE. INTRODUCTION DE L'INSTANCE. INTÉRÊT POUR AGIR. - AUTORISATION D'EXPLOITER DES PARCELLES SUR DES TERRES (1° DE L'ART. L. 331-2 DU CRPM) - INTÉRÊT À AGIR DU PROPRIÉTAIRE DES TERRES - EXISTENCE, EN TANT UNIQUEMENT QUE L'AUTORISATION PORTE SUR LES PARCELLES DONT IL EST PROPRIÉTAIRE.
</SCT>
<ANA ID="9A"> 03-03-03-01-06 M. et Mme A. sont propriétaires d'environ cinquante-cinq hectares de terres agricoles, qui constituent une partie seulement des soixante-neuf hectares et cinquante ares de terres agricoles que le préfet a, par l'arrêté litigieux, autorisé M. B. à exploiter dans ce département.,,,Ils n'avaient, par suite, qualité leur donnant intérêt à agir contre l'arrêté litigieux qu'en tant qu'il autorisait l'exploitation par M. B. des terres dont ils étaient propriétaires.</ANA>
<ANA ID="9B"> 54-01-04 M. et Mme A. sont propriétaires d'environ cinquante-cinq hectares de terres agricoles, qui constituent une partie seulement des soixante-neuf hectares et cinquante ares de terres agricoles que le préfet a, par l'arrêté litigieux, autorisé M. B. à exploiter dans ce département.,,,Ils n'avaient, par suite, qualité leur donnant intérêt à agir contre l'arrêté litigieux qu'en tant qu'il autorisait l'exploitation par M. B. des terres dont ils étaient propriétaires.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
