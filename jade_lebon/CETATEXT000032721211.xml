<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032721211</ID>
<ANCIEN_ID>JG_L_2016_06_000000385123</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/72/12/CETATEXT000032721211.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 4ème chambres réunies, 16/06/2016, 385123</TITRE>
<DATE_DEC>2016-06-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>385123</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>M. Olivier Rousselle</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Laurence Marion</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:385123.20160616</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. A...a demandé au tribunal administratif de Paris d'annuler la décision du 19 septembre 2012 du préfet de police refusant de procéder à l'échange de son permis de conduire sénégalais contre un permis de conduire français. Par un jugement n° 1219958/6-3 du 21 novembre 2013, le tribunal administratif a fait droit à sa demande et a enjoint au préfet de procéder à l'échange dans un délai de trois mois à compter de la date de son jugement.<br/>
<br/>
              Par un arrêt n° 14PA00262 du 31 juillet 2014, la cour administrative d'appel de Paris a rejeté la requête d'appel du ministre de l'intérieur et a enjoint au préfet de procéder à l'échange dans un délai de trois mois à compter de la date de son arrêt.<br/>
<br/>
              Par un pourvoi enregistré le 14 octobre 2014 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'intérieur demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt de la cour administrative d'appel de Paris ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses conclusions d'appel.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - l'arrêté du 12 janvier 2012 fixant les conditions de reconnaissance et d'échange des permis de conduire délivrés par les Etats n'appartenant ni à l'Union européenne, ni à l'Espace économique européen ;<br/>
<br/>
               - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Rousselle, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Laurence Marion, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de M. B...A... ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, le 7 août 2012, M.A..., ressortissant sénégalais, a sollicité l'échange de son permis de conduire sénégalais contre un permis de conduire français ; que, pour refuser cet échange par une décision du 19 septembre 2012, le préfet de police a estimé que le permis présenté n'était pas authentique ; que, par un jugement du 21 novembre 2013, le tribunal administratif a annulé cette décision à la demande de l'intéressé et enjoint au préfet de procéder à l'échange du permis litigieux ; que le ministre de l'intérieur se pourvoit en cassation contre l'arrêt par lequel la cour administrative d'appel de Paris a refusé de faire droit à son appel ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article R. 222-3 du code de la route : " Tout permis de conduire national, en cours de validité, délivré par un Etat ni membre de la Communauté européenne, ni partie à l'accord sur l'Espace économique européen, peut être reconnu en France jusqu'à l'expiration d'un délai d'un an après l'acquisition de la résidence normale de son titulaire. Pendant ce délai, il peut être échangé contre le permis français, sans que son titulaire soit tenu de subir les examens prévus au premier alinéa de l'article D. 221-3 Les conditions de cette reconnaissance et de cet échange sont définies par arrêté du ministre chargé des transports, après avis du ministre de la justice, du ministre de l'intérieur et du ministre chargé des affaires étrangères. (...) " ; <br/>
<br/>
              3. Considérant qu'aux termes de l'article 4 de l'arrêté du 12 janvier 2012  visé ci-dessus: " I. - Tout titulaire d'un permis de conduire délivré régulièrement au nom d'un Etat n'appartenant ni à l'Union européenne, ni à l'Espace économique européen doit obligatoirement demander l'échange de ce titre contre un permis de conduire français dans le délai d'un an qui suit l'acquisition de sa résidence normale en France (...) " ; qu'aux termes de l'article 7 du même arrêté : " Lorsque l'authenticité et la validité du titre sont établies lors du dépôt du dossier complet et sous réserve de satisfaire aux autres conditions prévues par le présent arrêté, le titre de conduite est échangé. / En cas de doute sur l'authenticité du titre dont l'échange est demandé, le préfet conserve le titre de conduite et fait procéder à son analyse, le cas échéant avec l'aide d'un service compétent, afin de s'assurer de son authenticité. Dans ce cas, une attestation de dépôt, sécurisée, est délivrée à son titulaire. Elle est valable pour une durée maximale de deux mois et est inscrite au fichier national du permis de conduire. Elle est retirée à l'issue de la procédure d'échange. / Si l'authenticité est confirmée, le titre de conduite peut être échangé sous réserve de satisfaire aux autres conditions. Si le caractère frauduleux est confirmé, l'échange n'a pas lieu et le titre est retiré par le préfet, qui saisit le procureur de la République en le lui transmettant. / Le préfet peut compléter son analyse en consultant l'autorité étrangère ayant délivré le titre afin de s'assurer des droits de conduite de son titulaire. Le titre de conduite est dès lors conservé par le préfet. La demande auprès des autorités étrangères est transmise, sous couvert du ministre des affaires étrangères, service de la valise diplomatique, au consulat de France compétent. Le consulat transmet au préfet la réponse de l'autorité étrangère. En l'absence de réponse dans un délai de six mois à compter de la saisine des autorités étrangères par le consulat compétent, l'échange du permis de conduire est refusé. Si l'autorité étrangère confirme l'absence de droits à conduire du titulaire, l'échange n'a pas lieu et le titre est retiré par le préfet qui saisit le procureur de la République en le lui transmettant. / Lorsque le préfet conserve le titre de conduite, une attestation de dépôt, sécurisée, est délivrée à son titulaire. Elle est valable deux mois. A l'issue de ces deux mois, une nouvelle attestation est délivrée autant de fois que nécessaire dans la limite de six mois. Elle est inscrite au fichier national du permis de conduire. Elle est retirée à l'issue de la procédure d'échange " ;<br/>
<br/>
              Sur l'arrêt attaqué en tant qu'il statue sur la demande d'annulation de la décision du 19 septembre 2012 :<br/>
<br/>
              4. Considérant que, après avoir notamment relevé que le bureau de la fraude documentaire de la direction centrale de la police aux frontières, saisi par le préfet de police, avait conclu que le permis de conduire de M. A..." présentait les caractéristiques d'un document authentique ", la cour a jugé, par une appréciation souveraine exempte de dénaturation,  qu'aucune pièce du dossier qui lui était soumis ne permettait de conclure à l'inauthenticité de ce permis ; qu'en jugeant  que la seule circonstance qu'un permis soit revêtu d'un timbre fiscal falsifié ne démontre pas qu'il soit inauthentique et en en déduisant que le préfet de police n'avait pu rejeter légalement la demande d'échange de permis dont l'avait saisi M. A... au motif que le permis qu'il présentait était inauthentique, elle n'a pas commis d'erreur de droit ;  qu'il suit de là que le ministre de l'intérieur n'est pas fondé à demander que son arrêt soit annulé en tant qu'il rejette son appel contre le jugement du 21 novembre 2013 par lequel le tribunal administratif de Paris a annulé la décision du préfet de police du 19 septembre 2012 ;  <br/>
<br/>
              Sur l'arrêt attaqué en tant qu'il statue sur les conclusions aux fins d'injonction :<br/>
<br/>
              5. Considérant qu'il résulte des dispositions citées au point 3 ci-dessus que, saisi d'une demande d'échange d'un permis de conduire étranger, le préfet doit s'assurer non seulement de l'authenticité mais également de la validité de ce permis ; qu'ainsi qu'il a été dit, il ressort des pièces du dossier soumis aux juges du fond que le permis présenté par M. A...était revêtu d'un timbre fiscal falsifié ; que, par une note verbale adressée aux autorités françaises, le ministre sénégalais des affaires étrangères avait indiqué que " si un timbre fiscal apposé sur un permis de conduire est falsifié, cela équivaut à un document sans timbre donc inachevé dans son droit de délivrance " ; que, dès lors,  en enjoignant au préfet de police de procéder à l'échange du permis de M.A..., alors qu'il ressortait des éléments qui lui étaient soumis que ce permis n'autorisait pas l'intéressé, en l'état, à conduire au Sénégal et que la condition  de validité de ce document ne pouvait donc être regardée comme remplie, la cour a commis une erreur de droit ; que son arrêt doit, par suite, être annulé en tant qu'il enjoint au préfet de procéder à l'échange du permis ; <br/>
<br/>
              6. Considérant qu'il y a lieu, dans les circonstances de l'espèce, en application des dispositions de l'article L. 821-2 du code de justice administrative, de régler l'affaire au fond  dans la mesure de la cassation prononcée, en statuant sur la demande d'injonction présentée par M.A... ;<br/>
<br/>
              7. Considérant que l'annulation de la décision du préfet de police du 19 septembre 2012 n'implique pas nécessairement qu'il lui soit enjoint d'échanger contre un permis français le permis de conduire sénégalais de M.A..., dès lors que, ainsi qu'il a été dit, ce permis sénégalais ne peut être regardé, en l'état, comme valide ; que, toutefois, si le permis de M.A..., qui doit être regardé comme authentique, n'est pas revêtu du timbre fiscal qu'exige la réglementation qui lui est applicable, une telle circonstance, qui n'est d'ailleurs pas nécessairement imputable à l'intéressé et pouvait n'être  pas connue de lui jusqu'à l'expertise réalisée par les autorités françaises à l'occasion de sa demande d'échange, ne peut conduire le préfet de police à rejeter la demande dont il est saisi sans avoir au préalable restitué son permis de conduire au demandeur en lui impartissant un délai raisonnable pour présenter un titre de conduite régularisé par les autorités de son pays ;  qu'il y a lieu d'enjoindre au  préfet de police de prendre une mesure en ce sens et de surseoir à statuer sur la demande de M. A... jusqu'à l'expiration du délai imparti, qui ne saurait être inférieur à trois mois ; <br/>
<br/>
              8. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement à M. A...de la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 31 juillet 2014 de la cour administrative d'appel de Paris est annulé en tant qu'il enjoint au préfet de police d'échanger le permis de conduire sénégalais de M. A...contre un permis français.<br/>
Article 2 : Il est enjoint au préfet de police de surseoir à statuer sur la demande d'échange présentée par M. A...en lui impartissant un délai qui ne saurait être inférieur à trois mois pour présenter un titre de conduite régularisé par les autorités de son pays.<br/>
<br/>
Article 3 : Le surplus des conclusions du pourvoi du ministre de l'intérieur et le surplus des conclusions aux fins d'injonction présentées par M. A...devant la cour administrative d'appel de Paris sont rejetés.<br/>
<br/>
Article 4 : L'Etat versera, au titre des dispositions de l'article L. 761-1 du code de justice administrative, une somme de 3 000 euros à M.A.... <br/>
<br/>
Article 5 : La présente décision sera notifiée au ministre de l'intérieur et à M. B... A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">49-04-01-04-01 POLICE. POLICE GÉNÉRALE. CIRCULATION ET STATIONNEMENT. PERMIS DE CONDUIRE. DÉLIVRANCE. - ECHANGE D'UN PERMIS DE CONDUIRE ÉTRANGER CONTRE UN PERMIS DE CONDUIRE FRANÇAIS (ART. R. 222-3 DU CODE DE LA ROUTE) - CAS D'UN PERMIS ÉTRANGER REVÊTU D'UN TIMBRE FISCAL FALSIFIÉ - 1) PERMIS INAUTHENTIQUE - ABSENCE - CONSÉQUENCES - ILLÉGALITÉ DU REFUS D'ÉCHANGE POUR CE MOTIF - 2) VALIDITÉ DU PERMIS - ABSENCE - CONSÉQUENCES - OFFICE DU PRÉFET SAISI D'UNE DEMANDE D'ÉCHANGE.
</SCT>
<ANA ID="9A"> 49-04-01-04-01 1) La seule circonstance que le permis de conduire étranger soit revêtu d'un timbre fiscal falsifié ne démontre pas qu'il soit inauthentique. La demande d'échange ne peut donc légalement être rejetée pour défaut d'authenticité dans une telle hypothèse.,,,2) Il résulte de l'article 4 de l'arrêté du 12 janvier 2012 que, saisi d'une demande d'échange d'un permis de conduire étranger, le préfet doit s'assurer non seulement de l'authenticité mais également de la validité de ce permis. Par une note verbale adressée aux autorités françaises, le ministre sénégalais des affaires étrangères avait indiqué que si un timbre fiscal apposé sur un permis de conduire est falsifié, cela équivaut à un document sans timbre donc inachevé dans son droit de délivrance. Le permis présenté en l'espèce était revêtu d'un timbre fiscal falsifié et n'autorisait donc pas l'intéressé, en l'état, à conduire au Sénégal. La condition de validité de ce document ne pouvait donc être regardée comme remplie.,,,Toutefois, si un permis de conduire étranger, regardé comme authentique, n'est pas revêtu du timbre fiscal qu'exige la réglementation qui lui est applicable, une telle circonstance, qui n'est d'ailleurs pas nécessairement imputable à l'intéressé et peut n'être pas connue de lui jusqu'à l'expertise réalisée par les autorités françaises à l'occasion de sa demande d'échange, ne peut conduire le préfet à rejeter la demande dont il est saisi sans avoir au préalable restitué son permis de conduire au demandeur en lui impartissant un délai raisonnable pour présenter un titre de conduite régularisé par les autorités de son pays.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
