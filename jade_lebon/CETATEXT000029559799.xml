<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029559799</ID>
<ANCIEN_ID>JG_L_2014_10_000000370588</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/55/97/CETATEXT000029559799.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème / 2ème SSR, 08/10/2014, 370588</TITRE>
<DATE_DEC>2014-10-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>370588</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème / 2ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ODENT, POULET ; BERTRAND</AVOCATS>
<RAPPORTEUR>M. Stéphane Bouchard</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Bertrand Dacosta</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:370588.20141008</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 26 juillet et 21 octobre 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour la commune d'Entraigues-sur-la-Sorgue (84320), représentée par son maire ; la commune d'Entraigues sur-la-Sorgue demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 10MA02835 du 27 mai 2013 par lequel la cour administrative d'appel de Marseille, sur la requête de la société L'A.C.R.A.U., a, d'une part, annulé le jugement nos 0802661 et 0901622 du 20 mai 2010 par lequel le tribunal administratif de Nîmes a rejeté les demandes de la société L'A.C.R.A.U. tendant à sa condamnation, en exécution d'un contrat signé par son maire le 16 septembre 1999 relatif à une étude de faisabilité pour un projet de zone d'aménagement concerté au lieu-dit " Le Moulin des Toiles ", à lui verser les sommes de 911,65 euros TTC, avec intérêts au taux légal à compter du 28 décembre 2003, correspondant à la note d'honoraire n° 5 du 28 novembre 2003, de 1 164,90 euros TTC, avec intérêts au taux légal à compter du 4 décembre 2004, correspondant à la note d'honoraire n° 6 du 4 novembre 2004, de 985,72 euros TTC, avec intérêts au taux légal à compter du 30 janvier 2005, correspondant à la note d'honoraire n° 7 du 30 décembre 2004 et de 469,24 euros TTC, avec intérêts au taux légal à compter du 13 avril 2007, correspondant à la note d'honoraire n° 8 du 13 mars 2007 et, d'autre part, l'a condamnée à verser à cette société les sommes en question avec intérêts au taux légal ;<br/>
<br/>
              2°) de mettre à la charge de la société L'A.C.R.A.U. le versement de la somme de 3 500 euros en application de l'article L. 761-1 du code de justice administrative ;  <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des collectivités territoriales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Stéphane Bouchard, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Bertrand Dacosta, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Odent, Poulet, avocat de la commune d'Entraigues-sur-la-Sorgue, et à Me Bertrand, avocat de la société L'A.C.R.A.U. ;<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que par un marché signé par le maire de la commune d'Entraigues-sur-la-Sorgue le 16 septembre 1999, sans autorisation préalable du conseil municipal, la société d'architecture L'A.C.R.A.U. s'est engagée à accomplir des prestations portant sur une "étude de faisabilité " en vue de la réalisation d'une zone d'aménagement concertée au lieu dit " Le Moulin des Toiles " ; que la société devait réaliser dans ce cadre trois missions pour un montant global de 28 463,77 euros ; que les deux premières missions ont été exécutées par la société et payées par la commune ; que celle-ci a, par la suite, refusé de payer quatre notes d'honoraires d'un montant total 3 531,51 euros présentées par la société au titre de la dernière mission accomplie entre 2003 et 2007 ; que la cour administrative d'appel de Marseille a, par l'arrêt attaqué du 27 mai 2013, condamné la commune à verser à la société L'A.C.R.A.U., sur un terrain contractuel, la somme de 3 531,51 euros, assortie des intérêts légaux, en paiement de ces prestations ; que la commune se pourvoit en cassation contre cet arrêt ;<br/>
<br/>
              2. Considérant que les conclusions présentées par la société L'A.C.R.A.U. devant le tribunal administratif de Marseille et transmises au tribunal administratif de Nîmes en application des articles R. 351-3 et R. 312-11 du code de justice administrative, qui tendent au règlement par la commune d'Entraigues-sur-la-Sorgue de sommes impayées relatives à l'exécution du contrat signé le 16 septembre 1999, ne revêtent pas un caractère indemnitaire au sens du 7° de l'article R. 222-13 du code de justice administrative et ne soulèvent donc pas un litige pour lequel le tribunal administratif a statué en premier et dernier ressort en vertu de l'article R. 811-1 de ce code ; que la requête de la société L'A.C.R.A.U. tendant à l'annulation du jugement du tribunal administratif de Nîmes du 20 mai 2010 présentait ainsi le caractère d'un appel sur lequel la cour administrative d'appel de Marseille était compétente pour se prononcer, contrairement à ce que soutient le pourvoi ;<br/>
<br/>
              3. Considérant que lorsque les parties soumettent au juge un litige relatif à l'exécution du contrat qui les lie, il incombe en principe à celui-ci, eu égard à l'exigence de loyauté des relations contractuelles, de faire application du contrat ; que, toutefois, dans le cas seulement où il constate une irrégularité invoquée par une partie ou relevée d'office par lui, tenant au caractère illicite du contrat ou à un vice d'une particulière gravité relatif notamment aux conditions dans lesquelles les parties ont donné leur consentement, il doit écarter le contrat et ne peut régler le litige sur le terrain contractuel ; qu'ainsi, lorsque le juge est saisi d'un litige relatif à l'exécution d'un contrat, les parties à ce contrat ne peuvent invoquer un manquement aux règles de passation, ni le juge le relever d'office, aux fins d'écarter le contrat pour le règlement du litige ; que, par exception, il en va autrement lorsque, eu égard, d'une part, à la gravité de l'illégalité et, d'autre part, aux circonstances dans lesquelles elle a été commise, le litige ne peut être réglé sur le fondement de ce contrat ;<br/>
<br/>
              4. Considérant que, dans ses énonciations, l'arrêt attaqué relève que si la signature d'un contrat par le maire sans l'autorisation du conseil municipal avait affecté le consentement de la commune, l'exigence de loyauté des relations contractuelles faisait cependant obstacle à ce que le contrat soit écarté pour régler le litige ; qu'en retenant ainsi un tel vice affectant le consentement de la personne publique et en estimant que l'exigence de loyauté des relations contractuelles faisait, par principe, obstacle à ce que le litige entre la commune et la société L'A.C.R.A.U. puisse être réglé sur un terrain autre que contractuel, la cour a commis une erreur de droit ; que par suite, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, l'arrêt attaqué doit être annulé ; <br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              6. Considérant qu'il résulte de l'instruction que, d'une part, le contrat, signé le 16 septembre 1999, a été exécuté normalement pendant plusieurs années par la commune, sans qu'elle émette d'objection, la commune ayant réglé toutes les notes d'honoraires présentées par son cocontractant à l'exception des dernières présentées  à compter de janvier 2005 ; que, d'autre part, le conseil municipal a adopté une délibération en date du  12 juillet 2001 approuvant le plan d'aménagement de zone réalisé par la société L'A.C.R.A.U., laquelle mentionnait expressément une " décision de la ville " d'engager  les études techniques confiées à cette société par le contrat litigieux ; que, dans les circonstances de l'espèce, le conseil municipal doit ainsi être regardé comme ayant donné son accord a posteriori à la conclusion du contrat en litige ; que, dès lors, eu égard à l'exigence de loyauté des relations contractuelles, l'absence d'autorisation préalable donnée par l'assemblée délibérante à la signature du contrat par le maire, ne saurait, dans les circonstances de l'espèce, eu égard au consentement ainsi donné par le conseil municipal, être regardée comme un vice d'une gravité telle que le contrat doive être écarté et que le litige opposant les parties ne doive pas être réglé sur le terrain contractuel ; <br/>
<br/>
              7. Considérant que le contrat en date du 16 septembre 1999 se borne à confier à la société L'A.C.R.A.U. des prestations de " coordination et de contrôle des différents projets qui viendront sur le site " de la zone d'aménagement concertée, sans qu'il soit fait état, contrairement à ce que prétend la commune, d'une mission plus importante de " suivi opérationnel " des projets ; que la société, en produisant les différents avis qu'elle a formulés, à la demande de la commune, sur les opérations de construction en cours sur le site justifie avoir exécuté la troisième mission, en litige, qui lui avait été contractuellement confiée et tenant en des prestations de coordination et de contrôle ; qu'il ne résulte pas de l'instruction que cette exécution aurait été imparfaite ou fautive ; que, par suite, la société est fondée à demander la condamnation de la commune à lui verser la  somme de 3 531,51 euros TTC, en paiement de prestations qu'elle a accomplies au titre de la troisième mission et non encore réglées, correspondant au total des sommes figurant sur ses notes d'honoraires nos 5 à 8  ;<br/>
<br/>
              8. Considérant que s'agissant des sommes de 911,65 euros, correspondant à la note d'honoraires n° 5, et de 1 164,90 euros, correspondant à la note d'honoraires n° 6, elles porteront intérêts au taux légal à compter de la date du 5 janvier 2005, date à laquelle, ainsi que l'atteste un courrier du maire de la commune, leur demande de paiement est parvenue à la commune ; que s'agissant des sommes de 985,72 euros, correspondant à la note d'honoraires n° 7, et de 469,24 euros, correspondant à la note d'honoraires n° 8, elles porteront intérêts au taux légal, faute de justification d'une date antérieure de réception de ces demandes en paiement, à compter de la saisine du tribunal administratif de Marseille, soit à compter du 8 août 2008 ;<br/>
<br/>
              9. Considérant qu'il résulte de tout ce qui précède que la société L'A.C.R.A.U. est fondée à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Nîmes a rejeté sa demande ;<br/>
<br/>
              10. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune d'Entraigues-sur-la-Sorgue la somme de 6 000 euros à verser à la société L'A.C.R.A.U., au titre de l'ensemble de la procédure, en application des dispositions de l'article L. 761-1 du code de justice administrative ; que ces dispositions font en revanche obstacle à ce qu'une somme soit mise à la charge de la société L'A.C.R.A.U., qui n'est pas dans la présente instance, la partie perdante ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Marseille du 27 mai 2013 et le jugement du tribunal administratif de Nîmes du 20 mai 2010 sont annulés.<br/>
Article 2 : La commune d'Entraigues-sur-la-Sorgue versera à la société L'A.C.R.A.U., d'une part, une somme totale de 3 531,51 euros TTC, portant intérêts au taux légal, pour un montant de 2 076,55 euros, à compter du 5 janvier 2005 et pour un montant de 1 454,96 euros à compter du 8 août 2008 ainsi que, d'autre part, une somme de 6 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : Les conclusions de la commune d'Entraigues-sur-la-Sorgue tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à la commune d'Entraigues-sur-la-Sorgue et à la société L'A.C.R.A.U..<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39 MARCHÉS ET CONTRATS ADMINISTRATIFS. - ABSENCE D'AUTORISATION PRÉALABLE DONNÉE PAR L'ASSEMBLÉE DÉLIBÉRANTE D'UNE COLLECTIVITÉ TERRITORIALE À LA SIGNATURE D'UN CONTRAT - CAS OÙ LA COLLECTIVITÉ PEUT ÊTRE REGARDÉE COMME AYANT DONNÉ A POSTERIORI SON ACCORD À LA CONCLUSION DU CONTRAT - VICE D'UNE PARTICULIÈRE GRAVITÉ EN MATIÈRE DE CONSENTEMENT - ABSENCE - CONSÉQUENCE - LITIGE ENTRE LES PARTIES RÉGLÉ SUR LE TERRAIN CONTRACTUEL.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">39-04-01 MARCHÉS ET CONTRATS ADMINISTRATIFS. FIN DES CONTRATS. NULLITÉ. - ABSENCE D'AUTORISATION PRÉALABLE DONNÉE PAR L'ASSEMBLÉE DÉLIBÉRANTE D'UNE COLLECTIVITÉ TERRITORIALE À LA SIGNATURE D'UN CONTRAT - CAS OÙ LA COLLECTIVITÉ PEUT ÊTRE REGARDÉE COMME AYANT DONNÉ A POSTERIORI SON ACCORD À LA CONCLUSION DU CONTRAT - VICE D'UNE PARTICULIÈRE GRAVITÉ EN MATIÈRE DE CONSENTEMENT - ABSENCE - CONSÉQUENCE - LITIGE ENTRE LES PARTIES RÉGLÉ SUR LE TERRAIN CONTRACTUEL.
</SCT>
<ANA ID="9A"> 39 Contrat exécuté normalement pendant plusieurs années par une commune, sans qu'elle émette d'objection, le conseil municipal ayant dans une de ses délibérations fait référence à une décision de la ville relative à l'objet du contrat et la commune ayant réglé toutes les notes d'honoraires présentées par son cocontractant, à l'exception des dernières qui sont l'objet du litige. Dans les circonstances de l'espèce, le conseil municipal doit ainsi être regardé comme ayant donné son accord a posteriori à la conclusion du contrat en litige et, eu égard à l'exigence de loyauté des relations contractuelles, l'absence d'autorisation préalable donnée par l'assemblée délibérante à la signature du contrat par le maire, ne saurait, eu égard au consentement ainsi donné par le conseil municipal, être regardée comme un vice d'une gravité telle que le contrat doive être écarté et que le litige opposant les parties ne doive pas être réglé sur le terrain contractuel.</ANA>
<ANA ID="9B"> 39-04-01 Contrat exécuté normalement pendant plusieurs années par une commune, sans qu'elle émette d'objection, le conseil municipal ayant dans une de ses délibérations fait référence à une décision de la ville relative à l'objet du contrat et la commune ayant réglé toutes les notes d'honoraires présentées par son cocontractant, à l'exception des dernières qui sont l'objet du litige. Dans les circonstances de l'espèce, le conseil municipal doit ainsi être regardé comme ayant donné son accord a posteriori à la conclusion du contrat en litige et, eu égard à l'exigence de loyauté des relations contractuelles, l'absence d'autorisation préalable donnée par l'assemblée délibérante à la signature du contrat par le maire, ne saurait, eu égard au consentement ainsi donné par le conseil municipal, être regardée comme un vice d'une gravité telle que le contrat doive être écarté et que le litige opposant les parties ne doive pas être réglé sur le terrain contractuel.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
