<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038101458</ID>
<ANCIEN_ID>JG_L_2019_02_000000415975</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/10/14/CETATEXT000038101458.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 06/02/2019, 415975</TITRE>
<DATE_DEC>2019-02-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>415975</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Contentieux des pensions</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>CABINET BRIARD ; SCP ODENT, POULET</AVOCATS>
<RAPPORTEUR>M. Yohann Bouquerel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier Henrard</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:415975.20190206</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A...B... a demandé au tribunal administratif de Versailles d'annuler la décision du 22 mai 2015 par laquelle la Caisse des dépôts et consignations lui a refusé le bénéfice de l'allocation temporaire d'invalidité, ainsi que la décision du 17 juin 2015 rejetant son recours gracieux, et d'enjoindre à cet établissement de lui octroyer le bénéfice de cette allocation à compter du 15 avril 2010. Par un jugement n° 1505009 du 14 juin 2016, le tribunal administratif de Versailles a rejeté sa demande. <br/>
<br/>
              Par une ordonnance n° 16VE02681 du 23 novembre 2017, enregistrée le 24 novembre 2017 au secrétariat du contentieux du Conseil d'Etat, le président de la cour administrative d'appel de Versailles a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, le pourvoi et le nouveau mémoire, enregistrés les 12 août 2016 et 23 juin 2017 au greffe de cette cour, présentés par Mme B.... Par ce pourvoi, ce mémoire et deux autres mémoires, enregistrés les 22 janvier et 27 septembre 2018 au secrétariat du contentieux du Conseil d'Etat, Mme B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses conclusions de première instance ;<br/>
<br/>
              3°) de mettre à la charge de la Caisse des dépôts et consignations la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la sécurité sociale ;<br/>
              - le décret n° 2005-442 du 2 mai 2005 ;<br/>
              - le code de justice administrative ;		<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Yohann Bouquerel, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Olivier Henrard, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, au Cabinet Briard, avocat de Mme B... et à la SCP Odent, Poulet, avocat de la Caisse des dépôts et consignations.<br/>
<br/>
              Vu la note en délibéré, enregistrée le 25 janvier 2019, présentée par Mme B... ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que Mme B..., secrétaire de mairie de la commune d'Angervilliers, a eu, le 3 février 2004, une vive altercation avec le maire. L'intéressée a été placée en arrêt de travail pour syndrome dépressif depuis cette date et maintenue dans cette position jusqu'à son départ en retraite le 15 juin 2015. Mme B... a entre-temps demandé le bénéfice d'une allocation temporaire d'invalidité. Par une décision du 22 mai 2015, la Caisse des dépôts et des consignations a rejeté sa demande, puis, le 17 juin 2015, le recours gracieux présenté contre ce refus. Madame B... a alors demandé au tribunal administratif de Versailles d'annuler ces décisions et d'enjoindre à la Caisse des dépôts et consignations de lui octroyer le bénéfice de l'allocation litigieuse à compter du 15 avril 2010. Par une ordonnance du 23 novembre 2017, le président de la cour administrative d'appel de Versailles a transmis au Conseil d'Etat le pourvoi formé par Mme B... contre le jugement du tribunal administratif de Versailles du 14 juin 2016 qui a rejeté sa demande.<br/>
<br/>
              2. Aux termes de l'article 2 du décret du 2 mai 2005 relatif à l'attribution de l'allocation temporaire d'invalidité aux fonctionnaires relevant de la fonction publique territoriale et de la fonction publique hospitalière : " L'allocation est attribuée aux fonctionnaires maintenus en activité qui justifient d'une invalidité permanente résultant / a) Soit d'un accident de service ayant entraîné une incapacité permanente d'un taux au moins égal à 10 % ; / b) Soit de l'une des maladies d'origine professionnelle énumérées par les tableaux mentionnés à l'article L. 461-2 du code de la sécurité sociale ; / c) Soit d'une maladie reconnue d'origine professionnelle dans les conditions mentionnées aux alinéas 3 et 4 de l'article L. 461-1 du code de la sécurité sociale, sous réserve des dispositions de l'article 6 du présent décret ". L'article 6 du même décret dispose que " la réalité des infirmités invoquées par le fonctionnaire, leur imputabilité au service, la reconnaissance du caractère professionnel des maladies, leurs conséquences ainsi que le taux d'invalidité qu'elles entraînent sont appréciés par la commission de réforme ", tandis que " le pouvoir de décision appartient, sous réserve de l'avis conforme de la Caisse des dépôts et consignations, à l'autorité qui a qualité pour procéder à la nomination ". <br/>
<br/>
              3. En premier lieu, il ressort des pièces du dossier soumis aux juges du fond qu'en estimant que Mme B... s'était bornée à soutenir que la décision de la Caisse des dépôts et consignations était entachée d'un vice de procédure sans assortir son moyen de précisions suffisantes pour en apprécier le bien-fondé, le tribunal administratif de Versailles n'a pas dénaturé les écritures de la requérante. Il n'a pas non plus entaché son jugement d'irrégularité en s'abstenant de solliciter de la requérante des précisions sur le moyen ainsi soulevé. Par ailleurs, contrairement à ce qui est soutenu, le tribunal n'a pas porté d'appréciation sur la régularité de la procédure suivie par la Caisse pour rejeter sa demande. Le moyen tiré de ce que le tribunal aurait commis une erreur de droit en admettant la régularité de cette procédure ne peut par suite qu'être rejeté. <br/>
<br/>
              4. En second lieu, constitue un accident de service, pour l'application de la réglementation relative à l'allocation temporaire d'invalidité, un évènement survenu à une date certaine, par le fait ou à l'occasion du service, dont il est résulté une lésion, quelle que soit la date d'apparition de celle-ci. Il ressort des pièces du dossier soumis aux juges du fond que Mme B... entretenait des relations conflictuelles depuis 2003 avec le nouveau maire de la commune d'Angervilliers. Une expertise réalisée le 15 avril 2010 à l'attention de la commission de réforme de l'Essonne indique en particulier que celle-ci souffrait d'un syndrome dépressif en lien avec ses conditions de travail avant l'altercation du 3 février 2004 avec le maire, dont les circonstances exactes ne sont au demeurant pas précisément établies. La circonstance que Mme B... a été placée en congé de maladie pour accident de service, avec effet à compter du 3 février 2004, est sans incidence sur la qualification de cet évènement au regard des dispositions relatives à l'attribution de l'allocation temporaire d'invalidité. Il résulte de ce qui précède qu'en retenant que l'invalidité permanente de Mme B..., due à son état dépressif, ne résultait pas d'un accident de service, le tribunal administratif de Versailles n'a pas inexactement qualifié les faits qui lui étaient soumis.<br/>
<br/>
              5. Il résulte de tout ce qui précède que Mme B... n'est pas fondée à demander l'annulation du jugement qu'elle attaque et que ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de Mme B... est rejeté.  <br/>
Article 2 : La présente décision sera notifiée à Mme A...B... et à la Caisse des dépôts et consignations.<br/>
Copie en sera adressée à la commune d'Angervilliers, au ministre de l'intérieur et au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-08-03-01-01 FONCTIONNAIRES ET AGENTS PUBLICS. RÉMUNÉRATION. INDEMNITÉS ET AVANTAGES DIVERS. ALLOCATION TEMPORAIRE D'INVALIDITÉ. NOTION D'ACCIDENT DE SERVICE. - 1) ACCIDENT DE SERVICE - DÉFINITION - EVÈNEMENT SURVENU À UNE DATE CERTAINE, PAR LE FAIT OU À L'OCCASION DU SERVICE, DONT IL EST RÉSULTÉ UNE LÉSION, QUELLE QUE SOIT LA DATE D'APPARITION DE CELLE-CI - 2) ESPÈCE - ALTERCATION ENTRE UN FONCTIONNAIRE D'UNE MAIRIE SOUFFRANT D'UN SYNDROME DÉPRESSIF ET LE MAIRE - ACCIDENT DE SERVICE - ABSENCE, SANS QU'AIT D'INCIDENCE LA CIRCONSTANCE QUE L'INTÉRESSÉ AIT ÉTÉ PLACÉ EN CONGÉ DE MALADIE À COMPTER DE LA DATE DE L'ALTERCATION [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">48-02-02-04-01 PENSIONS. PENSIONS CIVILES ET MILITAIRES DE RETRAITE. PENSIONS CIVILES. PENSIONS OU ALLOCATIONS POUR INVALIDITÉ. ALLOCATION TEMPORAIRE D'INVALIDITÉ PRÉVUE À L'ARTICLE 23 BIS DU STATUT GÉNÉRAL. - 1) ACCIDENT DE SERVICE - DÉFINITION - EVÈNEMENT SURVENU À UNE DATE CERTAINE, PAR LE FAIT OU À L'OCCASION DU SERVICE, DONT IL EST RÉSULTÉ UNE LÉSION, QUELLE QUE SOIT LA DATE D'APPARITION DE CELLE-CI - 2) ESPÈCE - ALTERCATION ENTRE UN FONCTIONNAIRE D'UNE MAIRIE SOUFFRANT D'UN SYNDROME DÉPRESSIF ET LE MAIRE - ACCIDENT DE SERVICE - ABSENCE, SANS QU'AIT D'INCIDENCE LA CIRCONSTANCE QUE L'INTÉRESSÉ AIT ÉTÉ PLACÉ EN CONGÉ DE MALADIE À COMPTER DE LA DATE DE L'ALTERCATION [RJ1].
</SCT>
<ANA ID="9A"> 36-08-03-01-01 1) Constitue un accident de service, pour l'application de la réglementation relative à l'allocation temporaire d'invalidité (ATI), un évènement survenu à une date certaine, par le fait ou à l'occasion du service, dont il est résulté une lésion, quelle que soit la date d'apparition de celle-ci.... ...2) Fonctionnaire d'une mairie entretenant des relations conflictuelles avec le maire. Expertise indiquant que l'intéressé souffrait d'un syndrome dépressif en lien avec ses conditions de travail avant que ne survienne une altercation avec le maire. La circonstance que l'intéressé a été placé en congé de maladie pour accident de service, avec effet à compter de cette altercation, est sans incidence sur la qualification de cet évènement au regard des dispositions relatives à l'attribution de l'allocation temporaire d'invalidité.... ...En retenant que l'invalidité permanente de l'intéressé, due à son état dépressif, ne résultait pas d'un accident de service, un tribunal administratif ne commet pas d'erreur de qualification juridique des faits.</ANA>
<ANA ID="9B"> 48-02-02-04-01 1) Constitue un accident de service, pour l'application de la réglementation relative à l'allocation temporaire d'invalidité (ATI), un évènement survenu à une date certaine, par le fait ou à l'occasion du service, dont il est résulté une lésion, quelle que soit la date d'apparition de celle-ci.... ...2) Fonctionnaire d'une mairie entretenant des relations conflictuelles avec le maire. Expertise indiquant que l'intéressé souffrait d'un syndrome dépressif en lien avec ses conditions de travail avant que ne survienne une altercation avec le maire. La circonstance que l'intéressé a été placé en congé de maladie pour accident de service, avec effet à compter de cette altercation, est sans incidence sur la qualification de cet évènement au regard des dispositions relatives à l'attribution de l'allocation temporaire d'invalidité.... ...En retenant que l'invalidité permanente de l'intéressé, due à son état dépressif, ne résultait pas d'un accident de service, un tribunal administratif ne commet pas d'erreur de qualification juridique des faits.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr. Cass. civ. 2ème, 25 mai 2005, n° 03-30.480, Bull. cuv. II, n° 132, p. 118.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
