<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036205251</ID>
<ANCIEN_ID>JG_L_2017_12_000000415207</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/20/52/CETATEXT000036205251.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 6ème chambres réunies, 13/12/2017, 415207</TITRE>
<DATE_DEC>2017-12-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>415207</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOUZIDI, BOUHANNA</AVOCATS>
<RAPPORTEUR>Mme Florence Marguerite</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Charles Touboul</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:415207.20171213</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
            Procédure contentieuse antérieure<br/>
<br/>
               M. A...B... a demandé au juge des référés du tribunal administratif de Paris, sur le fondement de l'article L. 521-2 du code de justice administrative, d'enjoindre au ministre des solidarités et de la santé de prendre des mesures ou de saisir les autorités compétentes, dans un délai de 48 heures à compter de la notification de la décision à intervenir, en vue de mettre l'ancienne formule du médicament Levothyrox à la disposition des malades en ayant besoin. <br/>
<br/>
              Par une ordonnance n° 1714601 du 22 septembre 2017, le juge des référés du tribunal administratif de Paris, statuant sur le fondement de l'article L. 522-3 du code de justice administrative, a rejeté sa demande.<br/>
<br/>
              Par une ordonnance n° 17PA03119 du 25 septembre 2017, le juge des référés de la cour administrative d'appel de Paris, statuant sur le fondement de l'article R. 522-8-1 du code de justice administrative, a rejeté le pourvoi formé par M. B...contre cette ordonnance.<br/>
<br/>
Procédure devant le Conseil d'Etat<br/>
<br/>
              1° Sous le n° 415207, par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 23 octobre et 7 novembre 2017 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'ordonnance du juge des référés du tribunal administratif de Paris du 22 septembre 2017 ;<br/>
<br/>
              2°) statuant en référé, de faire droit à sa demande d'injonction ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros à verser à la SCP Bouzidi, Bouhanna, son avocat, au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
              2° Sous le n° 415208, par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 23 octobre et 7 novembre 2017 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'ordonnance du juge des référés de la cour administrative d'appel de Paris du 25 septembre 2017 ;<br/>
<br/>
              2°) statuant en référé, de faire droit à sa demande d'injonction ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros à verser à la SCP Bouzidi, Bouhanna, son avocat, au titre des articles L 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
	Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son Préambule ;<br/>
              - le code de la santé publique ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Florence Marguerite, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Charles Touboul, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Bouzidi, Bouhanna, avocat de M.B....<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Les pourvois de M. B...présentent à juger des questions semblables. Il y a lieu de les joindre pour statuer par une seule décision.<br/>
<br/>
              2. Il ressort des pièces du dossier soumis au juge des référés que M. B... est traité, depuis plusieurs années, par la spécialité Levothyrox, fabriquée et commercialisée par les laboratoires Merck. M.B..., confronté à des effets indésirables graves à la suite de la modification de la formule de cette spécialité, a saisi le 21 septembre 2017 le juge des référés du tribunal administratif de Paris, sur le fondement de l'article L. 521-2 du code de justice administrative, en lui demandant d'enjoindre au ministre des solidarités et de la santé de prendre des mesures ou de saisir les autorités compétentes, dans un délai de 48 heures à compter de la notification de la décision à intervenir, en vue de mettre l'ancienne formule de la spécialité Levothyrox à la disposition des malades en ayant besoin. Par une ordonnance du 22 septembre 2017, le juge des référés du tribunal administratif de Paris, statuant sur le fondement de l'article L. 522-3 du code de justice administrative, a rejeté sa demande au motif qu'elle ne présentait pas un caractère d'urgence. M. B...se pourvoit en cassation contre cette ordonnance. Ayant également saisi la cour administrative d'appel de Paris d'un recours contre cette même ordonnance, M. B...se pourvoit en outre contre l'ordonnance du 25 septembre 2017 par laquelle le juge des référés de la cour administrative d'appel de Paris a rejeté son recours comme porté devant une juridiction incompétente pour en connaître.<br/>
<br/>
              Sur le pourvoi dirigé contre l'ordonnance du juge des référés du tribunal administratif de Paris :<br/>
<br/>
              3. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ". Aux termes de l'article L. 522-1 du même code : " Le juge des référés statue au terme d'une procédure contradictoire écrite ou orale. / Lorsqu'il lui est demandé de prononcer les mesures visées aux articles L. 521-1 et L. 521-2, de les modifier ou d'y mettre fin, il informe sans délai les parties de la date et de l'heure de l'audience publique. (...) ". Aux termes de l'article L. 522-3 du même code : " Lorsque la demande ne présente pas un caractère d'urgence ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée, le juge des référés peut la rejeter par une ordonnance motivée sans qu'il y ait lieu d'appliquer les deux premiers alinéas de l'article L. 522-1 ".<br/>
<br/>
              4. Pour rejeter la demande de M. B...sur le fondement de l'article L. 522-3 du code de justice administrative, au motif qu'elle ne présentait pas un caractère d'urgence justifiant, dans un délai de quarante-huit heures, le prononcé de mesures par le juge des référés saisi au titre de l'article L. 521-2 du même code, le juge des référés du tribunal administratif de Paris s'est notamment fondé sur la circonstance qu'il résultait des informations " relayées par les médias " que les laboratoires Merck avaient, à la demande du ministre chargé de la santé, annoncé qu'à la fin du mois de septembre 2017, l'ancienne formule de la spécialité Levothyrox serait de nouveau disponible en pharmacie. En rejetant ainsi sans instruction contradictoire ni audience publique la demande dont il était saisi, alors qu'il se fondait sur des éléments, justifiant un débat contradictoire, qui ne ressortaient pas de la demande du requérant ou des pièces produites à son soutien, le juge des référés a statué au terme d'une procédure irrégulière.<br/>
<br/>
              5. Il résulte de ce qui précède que M. B...est fondé à demander l'annulation de l'ordonnance qu'il attaque. Le moyen retenu suffisant à entraîner cette annulation, il n'est pas nécessaire d'examiner les autres moyens.<br/>
<br/>
              6. Dans les circonstances de l'espèce, il y a lieu, en application de l'article L. 821-2 du code de justice administrative, de régler l'affaire au titre de la procédure de référé engagée.<br/>
<br/>
              7. Aux termes de l'article L. 1110-5 du code de la santé publique : " Toute personne a, compte tenu de son état de santé et de l'urgence des interventions que celui-ci requiert, le droit de recevoir, sur l'ensemble du territoire, les traitements et les soins les plus appropriés et de bénéficier des thérapeutiques dont l'efficacité est reconnue et qui garantissent la meilleure sécurité sanitaire et le meilleur apaisement possible de la souffrance au regard des connaissances médicales avérées. Les actes de prévention, d'investigation ou de traitements et de soins ne doivent pas, en l'état des connaissances médicales, lui faire courir de risques disproportionnés par rapport au bénéfice escompté (...) ". Aux termes de l'article L. 1111-4 du même code : " Toute personne prend, avec le professionnel de santé et compte tenu des informations et des préconisations qu'il lui fournit, les décisions concernant sa santé (...) / Aucun acte médical ni aucun traitement ne peut être pratiqué sans le consentement libre et éclairé de la personne (...) ". Une carence caractérisée d'une autorité administrative dans l'usage des pouvoirs que lui confère la loi pour mettre en oeuvre le droit de toute personne de recevoir, sous réserve de son consentement libre et éclairé, les traitements et les soins les plus appropriés à son état de santé, tels qu'appréciés par le médecin, peut faire apparaître, pour l'application de l'article L. 521-2 du code de justice administrative, une atteinte grave et manifestement illégale à une liberté fondamentale lorsqu'elle risque d'entraîner une altération grave de l'état de santé de la personne intéressée. En outre, l'intervention du juge des référés dans les conditions d'urgence particulière prévues par l'article L. 521-2 du code de justice administrative est subordonnée au constat que la situation litigieuse permette de prendre utilement et à très bref délai les mesures de sauvegarde nécessaires.<br/>
<br/>
              8. Il résulte de l'instruction qu'en février 2012, à la suite d'une enquête officielle de pharmacovigilance, et pour limiter les différences de teneur en substance active selon les lots de la spécialité, l'Agence française de sécurité sanitaire des produits de santé a demandé à la société Merck Santé, titulaire des autorisations de mise sur le marché des spécialités Levothyrox, d'en restreindre les spécifications de teneur en lévothyroxine sodique dans les limites de plus ou moins 5 % de la dose déclarée sur toute la durée de vie du produit. A cette fin, la société a sollicité un changement de la formule de ses spécialités, consistant en la modification des excipients utilisés, qui a été autorisée le 27 septembre 2016 par l'Agence nationale de sécurité du médicament et des produits de santé, au vu de deux études de pharmacocinétique démontrant la bioéquivalence de l'ancienne et de la nouvelle formules. En février 2017, une lettre d'information de pharmacovigilance a été diffusée aux professionnels de santé pour les prévenir de la mise à disposition de la nouvelle formule à partir de la fin du mois de mars et recommander un suivi spécifique et un contrôle de l'équilibre thérapeutique des patients à risque, lettre complétée par d'autres éléments émanant de la société Merck Santé et par des documents d'information publiés par l'Agence. Toutefois, ainsi que le fait apparaître une enquête de pharmacovigilance portant sur la période allant de fin mars au 15 septembre 2017, qui dénombre 14 633 signalements reçus par les centres régionaux de pharmacovigilance, représentant 0,6 % des 2,6 millions de patients traités par Levothyrox, de nombreux patients ont rapporté des effets indésirables tels que fatigue, maux de tête, insomnie, vertiges, douleurs articulaires et musculaires et chute de cheveux, susceptibles de résulter des difficultés rencontrées dans le réajustement du dosage lors de la modification du traitement. <br/>
<br/>
              9. Il résulte également de l'instruction que, face à l'importance de ces effets indésirables et au risque que certains patients n'interrompent leur traitement malgré la gravité des conséquences qui pouvaient en résulter, le ministre chargé de la santé a invité la société Merck Santé à solliciter l'autorisation d'importer des unités de la spécialité Euthyrox, initialement destinée au marché allemand et correspondant à l'ancienne formule du Levothyrox. A ce titre, l'Agence nationale de sécurité du médicament et des produits de santé a autorisé la société, par des décisions des 18 septembre et 13 novembre 2017, à importer en France près de 186 000 boîtes puis un peu plus de 215 000 boîtes de 100 comprimés d'Euthyrox, en recommandant, par une lettre d'information diffusée aux professionnels de santé en octobre 2017, la prescription de cette spécialité aux seuls patients confrontés à des effets indésirables persistants malgré un équilibre hormonal et en précisant les mentions devant, dans ce cas, être portées sur l'ordonnance. En outre, l'Agence nationale de sécurité du médicament et des produits de santé a autorisé, le 16 octobre 2017, la distribution en France de la spécialité L-Thyroxin Henning de la société Sanofi, contenant d'autres excipients, et délivré le 20 septembre 2017 une autorisation de mise sur le marché à l'entreprise Unipharma en vue de la commercialisation prochaine de la spécialité Thyrofix, générique d'Euthyrox. Ces dispositions ont été accompagnées de la poursuite de l'enquête de pharmacovigilance, de mesures d'information et de la mise en place d'un " numéro vert " pour répondre aux interrogations des patients. <br/>
<br/>
              10. Compte tenu des mesures indiquées au point 9, et alors que M. B...ne soutient pas avoir actuellement des difficultés à se procurer le médicament que lui a prescrit son médecin et ne peut utilement, à l'appui de sa demande présentée au titre de l'article L. 521-2 du code de justice administrative, critiquer le caractère temporaire des mesures prises pour permettre l'importation parallèle de la spécialité Euthyrox, l'instruction ne fait pas apparaître, en l'état, une carence caractérisée du ministre chargé de la santé dans l'usage des pouvoirs que lui confère la loi, portant une atteinte grave et manifestement illégale à une liberté fondamentale et justifiant que soient prises des mesures de nature à sauvegarder une telle liberté.<br/>
<br/>
              11. Il résulte de ce qui précède que la demande présentée par M. B...devant le juge des référés du tribunal administratif de Paris doit être rejetée.<br/>
<br/>
              Sur le pourvoi dirigé contre l'ordonnance du juge des référés de la cour administrative d'appel de Paris :<br/>
<br/>
              12. Le pourvoi de M. B...tend à l'annulation de l'ordonnance par laquelle le juge des référés de la cour administrative d'appel de Paris a rejeté son recours contre l'ordonnance du juge des référés du tribunal administratif de Paris du 22 septembre 2017 comme porté devant une juridiction incompétente pour en connaître. Par la présente décision, l'ordonnance du juge des référés du tribunal administratif de Paris est annulée. Par suite, il n'y a plus lieu de statuer sur le pourvoi formé par M. B...contre l'ordonnance du 25 septembre 2017 du juge des référés de la cour administrative d'appel de Paris.<br/>
<br/>
              Sur les frais exposés par les parties à l'occasion du litige :<br/>
<br/>
              13. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du 22 septembre 2017 du juge des référés du tribunal administratif de Paris est annulée. <br/>
Article 2 : La demande présentée par M. B...devant le juge des référés du tribunal administratif de Paris est rejetée.<br/>
Article 3 : Il n'y a pas lieu de statuer sur le pourvoi de M. B...formé contre l'ordonnance du 25 septembre 2017 du juge des référés de la cour administrative d'appel de Paris.<br/>
Article 4 : Les conclusions de M. B...présentées au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 sont rejetées. <br/>
Article 5 : La présente décision sera notifiée à M. B...et à la ministre des solidarités et de la santé.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-035-03-03-01 PROCÉDURE. PROCÉDURES INSTITUÉES PAR LA LOI DU 30 JUIN 2000. RÉFÉRÉ TENDANT AU PRONONCÉ DE MESURES NÉCESSAIRES À LA SAUVEGARDE D'UNE LIBERTÉ FONDAMENTALE (ART. L. 521-2 DU CODE DE JUSTICE ADMINISTRATIVE). CONDITIONS D'OCTROI DE LA MESURE DEMANDÉE. ATTEINTE GRAVE ET MANIFESTEMENT ILLÉGALE À UNE LIBERTÉ FONDAMENTALE. - DROIT DE TOUTE PERSONNE DE RECEVOIR LES TRAITEMENTS ET SOINS LES PLUS APPROPRIÉS À SON ÉTAT DE SANTÉ - ATTEINTE À UNE LIBERTÉ FONDAMENTALE - EXISTENCE LORSQUE LA CARENCE DE L'AUTORITÉ ADMINISTRATIVE DANS LA MISE EN OEUVRE DE CE DROIT RISQUE D'ENTRAÎNER UNE ALTÉRATION GRAVE DE L'ÉTAT DE SANTÉ DE LA PERSONNE INTÉRESSÉE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-035-03-04-01 PROCÉDURE. PROCÉDURES INSTITUÉES PAR LA LOI DU 30 JUIN 2000. RÉFÉRÉ TENDANT AU PRONONCÉ DE MESURES NÉCESSAIRES À LA SAUVEGARDE D'UNE LIBERTÉ FONDAMENTALE (ART. L. 521-2 DU CODE DE JUSTICE ADMINISTRATIVE). POUVOIRS ET DEVOIRS DU JUGE. MESURES SUSCEPTIBLES D'ÊTRE ORDONNÉES PAR LE JUGE DES RÉFÉRÉS. - DROIT DE TOUTE PERSONNE DE RECEVOIR LES TRAITEMENTS ET SOINS LES PLUS APPROPRIÉS À SON ÉTAT DE SANTÉ - CONDITIONS D'INTERVENTION DU JUGE - CONSTAT DE LA POSSIBILITÉ DE PRENDRE UTILEMENT ET À TRÈS BREF DÉLAI LES MESURES DE SAUVEGARDE NÉCESSAIRES.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">61-04-01 SANTÉ PUBLIQUE. PHARMACIE. PRODUITS PHARMACEUTIQUES. - DROIT DE TOUTE PERSONNE DE RECEVOIR LES TRAITEMENTS ET SOINS LES PLUS APPROPRIÉS À SON ÉTAT DE SANTÉ - POSSIBILITÉ DE SAISIR LE JUGE DU RÉFÉRÉ LIBERTÉ EN CAS DE CARENCE CARACTÉRISÉE DE L'AUTORITÉ ADMINISTRATIVE DANS LA MISE EN OEUVRE DE CE DROIT [RJ1] - 1) ATTEINTE À UNE LIBERTÉ FONDAMENTALE - EXISTENCE LORSQUE CETTE CARENCE RISQUE D'ENTRAÎNER UNE ALTÉRATION GRAVE DE L'ÉTAT DE SANTÉ DE LA PERSONNE INTÉRESSÉE - 2) CONDITIONS D'INTERVENTION DU JUGE.
</SCT>
<ANA ID="9A"> 54-035-03-03-01 Une carence caractérisée d'une autorité administrative dans l'usage des pouvoirs que lui confère la loi pour mettre en oeuvre le droit de toute personne de recevoir, sous réserve de son consentement libre et éclairé, les traitements et les soins les plus appropriés à son état de santé, tels qu'appréciés par le médecin, peut faire apparaître, pour l'application de l'article L. 521-2 du code de justice administrative (CJA), une atteinte grave et manifestement illégale à une liberté fondamentale lorsqu'elle risque d'entraîner une altération grave de l'état de santé de la personne intéressée.</ANA>
<ANA ID="9B"> 54-035-03-04-01 Dans le cas d'une carence caractérisée d'une autorité administrative dans l'usage des pouvoirs que lui confère la loi pour mettre en oeuvre le droit de toute personne de recevoir, sous réserve de son consentement libre et éclairé, les traitements et les soins les plus appropriés à son état de santé, tels qu'appréciés par le médecin, l'intervention du juge des référés dans les conditions d'urgence particulière prévues par l'article L. 521-2 du code de justice administrative (CJA) est subordonnée au constat que la situation litigieuse permette de prendre utilement et à très bref délai les mesures de sauvegarde nécessaires.</ANA>
<ANA ID="9C"> 61-04-01 1) Une carence caractérisée d'une autorité administrative dans l'usage des pouvoirs que lui confère la loi pour mettre en oeuvre le droit de toute personne de recevoir, sous réserve de son consentement libre et éclairé, les traitements et les soins les plus appropriés à son état de santé, tels qu'appréciés par le médecin, peut faire apparaître, pour l'application de l'article L. 521-2 du code de justice administrative (CJA), une atteinte grave et manifestement illégale à une liberté fondamentale lorsqu'elle risque d'entraîner une altération grave de l'état de santé de la personne intéressée.... ...2) L'intervention du juge des référés dans les conditions d'urgence particulière prévues par l'article L. 521-2 du CJA est subordonnée au constat que la situation litigieuse permette de prendre utilement et à très bref délai les mesures de sauvegarde nécessaires.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., en matière d'hébergement d'urgence, CE, Section, 13 juillet 2016, Ministre des affaires sociales et de la santé c/ M. et Mme Rumija, n°400074, p. 363 ; en matière d'hébergement et de prise en charge de mineurs placés à l'ASE, CE, 27 juillet 2016, Département du Nord c/ M. Badiaga, n°400055, p. 387.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
