<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029751385</ID>
<ANCIEN_ID>JG_L_2014_11_000000361194</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/75/13/CETATEXT000029751385.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 12/11/2014, 361194</TITRE>
<DATE_DEC>2014-11-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>361194</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Rémi Decout-Paolini</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Maud Vialettes</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:361194.20141112</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 19 juillet 2012 au secrétariat du contentieux du Conseil d'Etat, présenté par le département de Maine-et-Loire, représenté par le président de son conseil général ; le département demande au Conseil d'Etat d'annuler la décision n° 111129 du 16 mai 2012 par laquelle la commission centrale d'aide sociale a, d'une part, annulé la décision de la commission départementale d'aide sociale de Maine-et-Loire du 17 juin 2011 et la décision du président du conseil général de Maine-et-Loire du 10 février 2011 refusant d'admettre Mme A... au bénéfice de l'aide ménagère et, d'autre part, l'a admise, à compter de la date de la notification de sa décision, au bénéfice de cette aide pour 9 heures par mois moyennant une participation horaire de 3 euros ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'action sociale et des familles ;<br/>
<br/>
              Vu le code général des collectivités territoriales ;<br/>
<br/>
              Vu le code de la sécurité sociale ;<br/>
<br/>
              Vu l'ordonnance n° 2005-1477 du 1er décembre 2005 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Rémi Decout-Paolini, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que Mme A...a sollicité le bénéfice des services ménagers au titre de l'aide sociale aux personnes handicapées, sur le fondement des dispositions du règlement départemental d'aide sociale de Maine-et-Loire permettant aux personnes handicapées " éligibles à la prestation de compensation " du handicap de bénéficier d'une telle prise en charge dans des conditions plus favorables que les conditions légales ; que le département de Maine-et-Loire lui a refusé ce bénéfice, au motif que ses ressources dépassaient le plafond fixé par le règlement départemental d'aide sociale, par une décision du 10 février 2011 que Mme A...a contestée devant la commission départementale d'aide sociale de Maine-et-Loire ; que, par une décision du 17 juin 2011, cette juridiction a décliné sa compétence au bénéfice de celle de la juridiction administrative de droit commun ; que le département se pourvoit en cassation contre la décision du 16 mai 2012 par laquelle la commission centrale d'aide sociale, statuant sur l'appel de Mme A..., a annulé la décision de la commission départementale et l'a admise au bénéfice des services ménagers ;<br/>
<br/>
              Sur la compétence des juridictions de l'aide sociale :<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 134-1 du code de l'action sociale et des familles : " A l'exception des décisions concernant l'attribution des prestations d'aide sociale à l'enfance ainsi que des décisions concernant le revenu de solidarité active, les décisions du président du conseil général et du représentant de l'Etat dans le département prévues à l'article L. 131-2 sont susceptibles de recours devant les commissions départementales d'aide sociale mentionnées à l'article L. 134-6 dans des conditions fixées par voie réglementaire " ; que si l'ordonnance du 1er décembre 2005 a modifié la rédaction de l'article L. 131-2 du même code, afin de tirer les conséquences de la suppression de la commission d'admission à l'aide sociale, elle n'a pas entendu remettre en cause son champ d'application, défini dans la rédaction antérieure de l'article par une énumération précise des prestations d'aide sociale, qui s'étend à l'ensemble des prestations légales d'aide sociale prévues par le code de l'action sociale et des familles ; qu'à ce titre, les juridictions de l'aide sociale ont compétence pour connaître, sous réserve des décisions concernant les prestations d'aide sociale à l'enfance ou le revenu de solidarité active, de toutes les décisions d'admission à l'aide sociale relatives aux prestations légales d'aide sociale, y compris lorsque le conseil général a décidé de conditions ou de montants plus favorables que ceux prévus par les lois et règlements, ainsi que le permet l'article L. 121-4 du code de l'action sociale et des familles ; que, par suite, la commission départementale d'aide sociale de Maine-et-Loire, en première instance, et la commission centrale d'aide sociale, en appel, étaient compétentes pour connaître du litige opposant Mme A...au département quant au bénéfice des services ménagers aux personnes handicapées, qui est une prestation légale, dans les conditions plus favorables prévues par le règlement départemental d'aide sociale ;<br/>
<br/>
              Sur le bien-fondé de la décision attaquée :<br/>
<br/>
              3. Considérant, en premier lieu, qu'aux termes du premier alinéa de l'article L. 241-1 du code de l'action sociale et des familles : " Toute personne handicapée dont l'incapacité permanente est au moins égale au pourcentage fixé par le décret prévu au premier alinéa de l'article L. 821-1 du code de la sécurité sociale ou qui est, compte tenu de son handicap, dans l'impossibilité de se procurer un emploi, peut bénéficier des prestations prévues au chapitre Ier du titre III du présent livre, à l'exception de l'allocation simple à domicile " ; qu'en application de ces dispositions, les personnes handicapées peuvent bénéficier de l'aide à domicile prévue à l'article L. 231-1 du même code en faveur des personnes âgées privées de ressources suffisantes et accordée par les départements, dès lors que leurs ressources ne dépassent pas, comme pour les personnes âgées, le plafond fixé à l'article D. 815-1 du code de la sécurité sociale ; <br/>
<br/>
              4. Considérant qu'aux termes de l'article L. 121-3 du code de l'action sociale et des familles : " Dans les conditions définies par la législation et la réglementation sociales, le conseil général adopte un règlement départemental d'aide sociale définissant les règles selon lesquelles sont accordées les prestations d'aide sociale relevant du département " ; qu'en vertu de l'article L. 121-4 du même code, le conseil général peut, ainsi qu'il a été dit, décider de conditions et de montants plus favorables que ceux prévus par les lois et règlements applicables aux prestations légales d'aide sociale dont il a la charge ; que, sur le fondement de ces dispositions, le conseil général de Maine-et-Loire a décidé d'étendre le bénéfice de la prestation légale d'aide sociale de services ménagers aux personnes handicapées dont les ressources sont supérieures au plafond fixé à l'article D. 815-1 du code de la sécurité sociale, en distinguant toutefois selon que ces personnes sont, ou non, " éligibles à la prestation de compensation " du handicap prévue à l'article L. 245-1 du code de l'action sociale et des familles ; que si, pour les personnes handicapées ne pouvant bénéficier de cette prestation, le règlement départemental d'aide sociale subordonne le bénéfice des services ménagers à un second plafond de ressources fixé à son annexe 6, il prévoit que les personnes handicapées " éligibles à la prestation de compensation " du handicap et dont les ressources sont supérieures à ce plafond " peuvent, le cas échéant, bénéficier d'une prise en charge dans les conditions d'attribution, de participation et de récupération régissant l'aide ménagère. / Les situations sont examinées au cas par cas ; l'ensemble des ressources du ménage y compris le capital placé sont pris en compte. / Aucune prise en charge n'est accordée dès lors que le montant des capitaux placés est supérieur à 7 000 euros " ; qu'il ne distingue pas entre les différents éléments de cette prestation énumérés à l'article L. 245-3 du même code ;<br/>
<br/>
              5. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par des décisions des 17 mars 2009 et 12 avril 2011, la commission des droits et de l'autonomie des personnes handicapées de Maine-et-Loire a reconnu le droit de Mme A...au bénéfice de la prestation de compensation du handicap, respectivement du 1er octobre 2008 au 30 septembre 2018 et du 1er juin 2010 au 31 mai 2020, en lui accordant, en application du 3° de l'article L. 245-3 du code de l'action sociale et des familles, la prise en charge de ses frais de déménagement et d'installation dans un logement adapté dans la limite de 3 000 euros, puis une aide de 7 238,40 euros au titre des charges liées à l'aménagement de son logement ; que Mme A... ayant été ainsi reconnue comme entrant dans le champ de la prestation de compensation du handicap, la circonstance que ses ressources dépassaient le plafond fixé à l'annexe 6 du règlement départemental d'aide sociale, applicable aux personnes handicapées ne pouvant pas bénéficier de la prestation de compensation du handicap, n'était pas de nature à faire obstacle à son admission à l'aide sociale au titre des services ménagers ni à justifier la décision prise par le président du conseil général d'interrompre, à compter du 1er mars 2011, la prise en charge de ces services ; que, par suite, en jugeant que le plafond de ressources fixé à l'annexe 6 du règlement départemental d'aide sociale n'était pas opposable à MmeA..., dès lors qu'elle entrait dans le champ de la prestation de compensation du handicap, la commission centrale d'aide sociale, qui n'a par ailleurs pas jugé que Mme A...pouvait bénéficier de plein droit des services ménagers, n'a pas commis d'erreur de droit ; <br/>
<br/>
              6. Considérant, en second lieu, qu'en vérifiant si Mme A...disposait d'un capital placé et en relevant que la seule circonstance qu'elle était propriétaire de la maison qu'elle occupait ne pouvait à elle seule l'exclure du bénéfice des services ménagers, la commission centrale d'aide sociale s'est bornée à vérifier, en sa qualité de juge de plein contentieux, et en répondant à l'argumentation présentée en défense par le département en ce qui concerne la situation financière de l'intéressée, si elle devait prononcer son admission au bénéfice de ces services ; qu'en procédant ainsi, elle ne s'est pas méprise sur la portée des écritures du département et n'a pas dénaturé les faits de l'espèce ;   <br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que le pourvoi du département de Maine-et-Loire doit être rejeté ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi du département de Maine-et-Loire est rejeté.<br/>
Article 2 : La présente décision sera notifiée au département de Maine-et-Loire et à Mme B...A....<br/>
Copie en sera adressée pour information à la ministre des affaires sociales, de la santé et des droits des femmes.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">04-04-01 AIDE SOCIALE. CONTENTIEUX DE L'AIDE SOCIALE ET DE LA TARIFICATION. CONTENTIEUX DE L'ADMISSION À L'AIDE SOCIALE. - COMPÉTENCE DES JURIDICTIONS SPÉCIALISÉES DE L'AIDE SOCIALE - LITIGES RELATIFS À DES PRESTATIONS LÉGALES D'AIDE SOCIALE, Y COMPRIS LORSQUE LE DÉPARTEMENT A DÉCIDÉ DE CONDITIONS D'ATTRIBUTION OU DE MONTANTS PLUS FAVORABLES - EXISTENCE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">135-03-02-01-01 COLLECTIVITÉS TERRITORIALES. DÉPARTEMENT. ATTRIBUTIONS. COMPÉTENCES TRANSFÉRÉES. ACTION SOCIALE. - PRESTATIONS LÉGALES D'AIDE SOCIALE POUR LESQUELLES LE DÉPARTEMENT A DÉCIDÉ DE CONDITIONS D'ATTRIBUTION OU DE MONTANTS PLUS FAVORABLES - COMPÉTENCE JURIDICTIONNELLE - JURIDICTIONS SPÉCIALISÉES DE L'AIDE SOCIALE [RJ1].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">17-05-04-02 COMPÉTENCE. COMPÉTENCE À L'INTÉRIEUR DE LA JURIDICTION ADMINISTRATIVE. COMPÉTENCE DES JURIDICTIONS ADMINISTRATIVES SPÉCIALES. JURIDICTION ADMINISTRATIVE DE DROIT COMMUN OU JURIDICTION ADMINISTRATIVE SPÉCIALISÉE. - JURIDICTIONS SPÉCIALISÉES DE L'AIDE SOCIALE - LITIGES RELATIFS À DES PRESTATIONS LÉGALES D'AIDE SOCIALE, Y COMPRIS LORSQUE LE DÉPARTEMENT A DÉCIDÉ DE CONDITIONS D'ATTRIBUTION OU DE MONTANTS PLUS FAVORABLES [RJ1].
</SCT>
<ANA ID="9A"> 04-04-01 Si l'ordonnance du 1er décembre 2005 a modifié la rédaction de l'article L. 131-2 du code de l'action sociale et des familles (CASF), auquel renvoie l'article L. 134-1 du même code, afin de tirer les conséquences de la suppression de la commission d'admission à l'aide sociale, elle n'a pas entendu remettre en cause son champ d'application, défini dans sa rédaction antérieure par une énumération précise des prestations d'aide sociale relevant de la compétence des juridictions spécialisées de l'aide sociale, qui s'étend à l'ensemble des prestations légales d'aide sociale prévues par le CASF.... ,,A ce titre, les juridictions de l'aide sociale ont compétence pour connaître, sous réserve des décisions concernant les prestations d'aide sociale à l'enfance ou le revenu de solidarité active, de toutes les décisions d'admission à l'aide sociale relatives aux prestations légales d'aide sociale, y compris lorsque le conseil général a décidé de conditions ou de montants plus favorables que ceux prévus par les lois et règlements, ainsi que le permet l'article L. 121-4 du CASF.</ANA>
<ANA ID="9B"> 135-03-02-01-01 Si l'ordonnance du 1er décembre 2005 a modifié la rédaction de l'article L. 131-2 du code de l'action sociale et des familles (CASF), auquel renvoie l'article L. 134-1 du même code, afin de tirer les conséquences de la suppression de la commission d'admission à l'aide sociale, elle n'a pas entendu remettre en cause son champ d'application, défini dans sa rédaction antérieure par une énumération précise des prestations d'aide sociale relevant de la compétence des juridictions spécialisées de l'aide sociale, qui s'étend à l'ensemble des prestations légales d'aide sociale prévues par le CASF.... ,,A ce titre, les juridictions de l'aide sociale ont compétence pour connaître, sous réserve des décisions concernant les prestations d'aide sociale à l'enfance ou le revenu de solidarité active, de toutes les décisions d'admission à l'aide sociale relatives aux prestations légales d'aide sociale, y compris lorsque le conseil général a décidé de conditions ou de montants plus favorables que ceux prévus par les lois et règlements, ainsi que le permet l'article L. 121-4 du CASF.</ANA>
<ANA ID="9C"> 17-05-04-02 Si l'ordonnance du 1er décembre 2005 a modifié la rédaction de l'article L. 131-2 du code de l'action sociale et des familles (CASF), auquel renvoie l'article L. 134-1 du même code, afin de tirer les conséquences de la suppression de la commission d'admission à l'aide sociale, elle n'a pas entendu remettre en cause son champ d'application, défini dans sa rédaction antérieure par une énumération précise des prestations d'aide sociale relevant de la compétence des juridictions spécialisées de l'aide sociale, qui s'étend à l'ensemble des prestations légales d'aide sociale prévues par le CASF.... ,,A ce titre, les juridictions de l'aide sociale ont compétence pour connaître, sous réserve des décisions concernant les prestations d'aide sociale à l'enfance ou le revenu de solidarité active, de toutes les décisions d'admission à l'aide sociale relatives aux prestations légales d'aide sociale, y compris lorsque le conseil général a décidé de conditions ou de montants plus favorables que ceux prévus par les lois et règlements, ainsi que le permet l'article L. 121-4 du CASF.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., retenant la compétence des juridictions de droit commun pour des mesures d'aide sociale facultative, CE, 28 avril 2004, Mme Monclaire, n° 259214, T. pp. 585-603-641.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
