<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025908784</ID>
<ANCIEN_ID>JG_L_2012_05_000000323882</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/90/87/CETATEXT000025908784.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème et 10ème sous-sections réunies, 21/05/2012, 323882</TITRE>
<DATE_DEC>2012-05-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>323882</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème et 10ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARTHELEMY, MATUCHANSKY, VEXLIARD ; SCP ODENT, POULET</AVOCATS>
<RAPPORTEUR>Mme Cécile Isidoro</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Pierre Collin</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:323882.20120521</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 5 janvier et 6 avril 2009 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. B... C..., demeurant..., et M. A... C..., demeurant... ; MM.  C...demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt du 3 novembre 2008 par lequel la cour administrative d'appel de Bordeaux a rejeté leur requête tendant à l'annulation du jugement du 5 décembre 2006 par lequel le tribunal administratif de Pau a rejeté leurs demandes tendant à l'annulation du certificat d'urbanisme positif du 29 avril 2004 portant sur un projet de lotissement en tant qu'il précise que le sursis à statuer pourra être opposé à une demande d'autorisation de lotir ultérieure, à l'annulation du plan local d'urbanisme de la commune de Tarnos approuvé par délibération du conseil de la communauté de communes du Seignanx du 22 février 2005 et rendu public le 12 mars 2005 et de l'arrêté du 25 avril 2005 du maire de Tarnos portant refus d'autorisation de lotir ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur requête d'appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement d'une somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'urbanisme ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Cécile Isidoro, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Barthélemy, Matuchansky, Vexliard, avocat de MM. C...et de la SCP Odent, Poulet, avocat de la commune de Tarnos et de la Communauté des communes du Seignanx, <br/>
<br/>
              - les conclusions de M. Pierre Collin, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Barthélemy, Matuchansky, Vexliard, avocat de MM. C...et à la SCP Odent, Poulet, avocat de la commune de Tarnos et de la Communauté des communes du Seignanx ;<br/>
<br/>
<br/>
<br/>Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que MM. B... et A...C...se sont vu délivrer le 29 avril 2004 par le maire de la commune de Tarnos un certificat d'urbanisme positif portant sur un projet de lotissement ; qu'ils ont demandé devant le tribunal administratif de Pau l'annulation de ce certificat en tant qu'il précise que le sursis à statuer pourra être opposé à une demande d'autorisation de lotir ultérieure, l'annulation du plan local d'urbanisme de la commune de Tarnos approuvé par délibération du conseil de la communauté de Seignanx du 22 février 2005 et rendu public le 12 mars 2005 et celle de l'arrêté du maire de Tarnos du 25 avril 2005 portant refus d'autorisation de lotir ; que le tribunal administratif de Pau a rejeté leur demande par un jugement du 5 décembre 2006 ; que la cour administrative d'appel de Bordeaux a confirmé ce jugement par un arrêt du 3 novembre 2008 contre lequel MM.  C...se sont pourvus en cassation ; que par une décision du 7 octobre 2010, le Conseil d'Etat a, d'une part, admis les conclusions de ce pourvoi dirigées contre l'arrêt attaqué qui rejette les conclusions de la requête de MM.  C...tendant à l'annulation du certificat d'urbanisme délivré le 29 avril 2004 en tant qu'il précise qu'un sursis à statuer pourra être opposé à une demande d'autorisation de lotir ultérieure, et a, d'autre part, refusé d'admettre le surplus des conclusions de leur pourvoi ;<br/>
<br/>
              Sur le surplus des conclusions du pourvoi :<br/>
<br/>
              Considérant, d'une part, qu'aux termes de l'article L. 410-1 du code de l'urbanisme, dans sa rédaction issue de l'article 30 de la loi n° 2000-1208 du 14 décembre 2000 : " Le certificat d'urbanisme indique les dispositions d'urbanisme et les limitations administratives au droit de propriété et le régime des taxes et participations d'urbanisme applicables à un terrain ainsi que l'état des équipements publics existants ou prévus. (...) " ; qu'en vertu du même article, lorsque la demande formulée en vue de réaliser l'opération projetée sur le terrain, notamment la demande de permis de construire prévue à l'article L. 421-1, est déposée dans le délai d'un an à compter de la délivrance d'un certificat d'urbanisme et respecte les dispositions d'urbanisme mentionnées par ce certificat, ces dispositions ne peuvent être remises en cause, à l'exception des règles qui ont pour objet la préservation de la sécurité ou de la salubrité publique ;<br/>
<br/>
              Considérant, d'autre part, qu'aux termes de l'article R. 410-16 du code de l'urbanisme, dans sa rédaction applicable à la date du certificat contesté : " Au cas où un sursis à statuer serait opposable à une demande d'autorisation tendant à affecter le terrain à la construction ou à y réaliser une opération déterminée, le certificat d'urbanisme en fait état. " ;<br/>
<br/>
              Considérant qu'il résulte de ces dispositions combinées qu'elles ont pour objet d'informer le pétitionnaire des règles d'urbanisme et des limitations administratives au droit de propriété applicables au terrain et de permettre à ce pétitionnaire de savoir qu'à compter de la publication de la délibération prescrivant l'élaboration d'un plan local d'urbanisme, le sursis à statuer prévu par l'article R. 410-16 du même code est susceptible de lui être opposé ; que la mention du sursis à statuer dans un certificat d'urbanisme complète ainsi l'information du pétitionnaire tout en pouvant lui faire grief dès lors qu'en cas de modification des documents d'urbanisme, le pétitionnaire est susceptible de perdre le bénéfice des règles applicables qu'est censé assurer le certificat d'urbanisme ; qu'ainsi, la mention dans un certificat d'urbanisme de la possibilité d'un sursis à statuer ultérieur est divisible du reste du certificat et susceptible d'être discutée au contentieux ;<br/>
<br/>
              Considérant que le certificat d'urbanisme positif délivré par le maire de Tarnos à MM.  C...le 29 avril 2004, qui portait sur la possibilité de réaliser une opération de lotissement au vu des dispositions d'urbanisme alors applicables, mentionne également qu'au vu du plan local d'urbanisme en cours d'élaboration, une demande ultérieure d'autorisation de lotir pourra faire l'objet d'un sursis à statuer ; qu'en jugeant que ces dernières énonciations du certificat d'urbanisme n'étaient pas divisibles de celles accordant un certificat d'urbanisme positif, alors qu'elles se fondaient sur des dispositions d'urbanisme différentes et avaient un autre objet, la cour administrative d'appel a commis une erreur de droit ; que, par suite, elle n'a pu, sans entacher son arrêt d'une seconde erreur de droit, rejeter comme irrecevables les conclusions de MM.  C...au motif que la mention du sursis à statuer dans le certificat d'urbanisme n'était pas divisible de ce certificat et n'était donc pas susceptible d'être contestée au contentieux ; qu'ainsi, l'arrêt que MM.  C...attaquent doit, dans cette mesure, être annulé ;<br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de Tarnos et de la communauté de communes du Seignanx le versement de la somme globale de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ; que les conclusions de la commune de Tarnos et de la communauté de communes du Seignanx présentées au titre des mêmes dispositions doivent être rejetées ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Bordeaux du 3 novembre 2008 est annulé en tant qu'il rejette les conclusions dirigées contre le certificat d'urbanisme délivré le 29 avril 2004 en tant qu'il précise que le sursis à statuer pourra être opposé à une demande d'autorisation de lotir ultérieure.<br/>
Article 2 : L'affaire est renvoyée dans cette mesure à la cour administrative d'appel de Bordeaux.<br/>
Article 3 : La commune de Tarnos et la communauté de communes du Seignanx verseront la somme globale de 3 000 euros à MM.  C...en application des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions de la commune de Tarnos et de la communauté de communes du Seignanx tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à M. B... C..., à M. A... C..., à la commune de Tarnos et à la communauté de communes du Seignanx.<br/>
Copie en sera adressée pour information à la ministre de l'écologie, du développement durable et de l'énergie.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-01-01-01 PROCÉDURE. INTRODUCTION DE L'INSTANCE. DÉCISIONS POUVANT OU NON FAIRE L'OBJET D'UN RECOURS. ACTES CONSTITUANT DES DÉCISIONS SUSCEPTIBLES DE RECOURS. - MENTION D'UN CERTIFICAT D'URBANISME POSITIF RELATIVE AU SURSIS À STATUER.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">68-025-03 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. CERTIFICAT D'URBANISME. CONTENU. - MENTION RELATIVE AU SURSIS À STATUER - DIVISIBILITÉ - EXISTENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">68-06-01-01 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. INTRODUCTION DE L'INSTANCE. DÉCISION FAISANT GRIEF. - MENTION D'UN CERTIFICAT D'URBANISME POSITIF RELATIVE AU SURSIS À STATUER.
</SCT>
<ANA ID="9A"> 54-01-01-01 La mention du sursis à statuer dans un certificat d'urbanisme complète l'information du pétitionnaire tout en pouvant lui faire grief, dès lors qu'en cas de modification des documents d'urbanisme, le pétitionnaire est susceptible de perdre le bénéfice des règles applicables qu'est censé assurer le certificat d'urbanisme. Ainsi, la mention dans un certificat d'urbanisme de la possibilité d'un sursis à statuer ultérieur est divisible du reste du certificat et susceptible d'être discutée au contentieux.</ANA>
<ANA ID="9B"> 68-025-03 La mention du sursis à statuer dans un certificat d'urbanisme complète l'information du pétitionnaire tout en pouvant lui faire grief, dès lors qu'en cas de modification des documents d'urbanisme, le pétitionnaire est susceptible de perdre le bénéfice des règles applicables qu'est censé assurer le certificat d'urbanisme. Ainsi, la mention dans un certificat d'urbanisme de la possibilité d'un sursis à statuer ultérieur est divisible du reste du certificat et susceptible d'être discutée au contentieux.</ANA>
<ANA ID="9C"> 68-06-01-01 La mention du sursis à statuer dans un certificat d'urbanisme complète l'information du pétitionnaire tout en pouvant lui faire grief, dès lors qu'en cas de modification des documents d'urbanisme, le pétitionnaire est susceptible de perdre le bénéfice des règles applicables qu'est censé assurer le certificat d'urbanisme. Ainsi, la mention dans un certificat d'urbanisme de la possibilité d'un sursis à statuer ultérieur est divisible du reste du certificat et susceptible d'être discutée au contentieux.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
