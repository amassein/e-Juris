<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037034083</ID>
<ANCIEN_ID>JG_L_2018_06_000000412744</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/03/40/CETATEXT000037034083.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 07/06/2018, 412744</TITRE>
<DATE_DEC>2018-06-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>412744</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROUSSEAU, TAPIE</AVOCATS>
<RAPPORTEUR>M. Jean-Yves Ollier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier Henrard</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:412744.20180607</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A...B...a demandé au tribunal administratif de Montpellier, à titre principal, d'annuler le titre de perception, émis le 23 décembre 2013, par lequel le ministre de l'agriculture a mis à sa charge la somme de 11 159,11 euros au titre d'un trop perçu de rémunération et de la décharger de l'obligation de payer cette somme et, à titre subsidiaire, de condamner l'Etat à réparer le préjudice d'un même montant subi du fait du retard fautif de l'administration à lui réclamer cette répétition d'indu. Par un jugement n° 1400767 du 16 juillet 2015, le tribunal administratif de Montpellier a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 15MA03794 du 6 juin 2017, la cour administrative d'appel de Marseille  a, sur appel de MmeB..., annulé ce jugement, annulé le titre de perception émis le 23 décembre 2013 et déchargé Mme B...de l'obligation de payer la somme en litige.  <br/>
<br/>
              Par un pourvoi, enregistré le 25 juillet 2017 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'agriculture et de l'alimentation demande au Conseil d'Etat d'annuler cet arrêt.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code civil ;<br/>
              - le code des assurances ; <br/>
              - le décret n° 2012-1246 du 7 novembre 2012 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Yves Ollier, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Olivier Henrard, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rousseau, Tapie, avocat de MmeB....<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des énonciations de l'arrêt attaqué que Mme B..., adjointe administrative principale à la direction régionale de l'alimentation, de l'agriculture et de la forêt du Languedoc-Roussillon, a été admise à faire valoir ses droits à la retraite pour invalidité imputable au service à compter du 17 mars 2011 ; qu'elle a toutefois continué à bénéficier du versement de son traitement jusqu'au 31 août 2011 ; que le ministre chargé de l'agriculture a émis, le 23 décembre 2013, un titre de perception d'un montant de 11 159,11 euros, expédié le 10 janvier 2014, pour le reversement de ce trop perçu ; que la requérante a demandé au tribunal administratif de Montpellier, à titre principal, l'annulation de ce titre de perception et la décharge de l'obligation de payer la somme demandée et, à titre subsidiaire, la condamnation de l'Etat à réparer le préjudice d'un même montant subi du fait du retard fautif de l'administration à lui réclamer cette répétition d'indu ; que, par un jugement du 16 juillet 2015, le tribunal administratif a rejeté sa demande ; que, par l'arrêt attaqué, la cour administrative d'appel de Marseille, sur appel de MmeB..., a annulé ce jugement ainsi que le titre de perception et déchargé l'intéressée de l'obligation de payer la somme en litige, en faisant droit à l'exception de prescription qu'elle avait opposée à la créance en cause ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article 118 du décret du 7 novembre 2012 relatif à la gestion budgétaire et comptable publique : " Avant de saisir la juridiction compétente, le redevable doit adresser une réclamation appuyée de toutes justifications utiles au comptable chargé du recouvrement de l'ordre de recouvrer (...) " ; <br/>
<br/>
              3. Considérant qu'aux termes de l'article L. 127 1 du code des assurances : " Est une opération d'assurance de protection juridique toute opération consistant, moyennant le paiement d'une prime ou d'une cotisation préalablement convenue, à prendre en charge des frais de procédure ou à fournir des services découlant de la couverture d'assurance, en cas de différend ou de litige opposant l'assuré à un tiers, en vue notamment de défendre ou représenter en demande l'assuré dans une procédure civile, pénale ou administrative ou autre ou contre une réclamation dont il est l'objet ou d'obtenir réparation à l'amiable du dommage subi " ; qu'eu égard aux termes de ces dispositions, un assureur au titre de la protection juridique peut présenter un recours administratif ou une réclamation préalable, au nom de son assuré, par l'intermédiaire de l'un de ses préposés sans être tenu de produire un mandat exprès de l'assuré ni une délégation de signature à son préposé ; qu'il suit de là que la cour administrative d'appel de Marseille n'a pas commis d'erreur de droit en écartant la fin de non recevoir opposée par le ministre tirée de ce que la réclamation préalable signée par le salarié de l'assureur de Mme B...était irrecevable faute pour celui-ci de produire un mandat exprès de l'intéressée et la preuve de la délégation de signature faite à son salarié, dès lors que le ministre n'a pas contesté l'existence du contrat d'assurance de protection juridique conclu entre Mme B...et cet assureur ; que le ministre n'est, par suite, pas fondé à demander l'annulation de l'arrêt qu'il attaque ;<br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à Mme B...au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi du ministre de l'agriculture et de l'alimentation est rejeté.<br/>
Article 2 : L'Etat versera à Mme B...une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée au ministre de l'agriculture et de l'alimentation et à Mme A...B....<br/>
Copie en sera adressée au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">12-03 ASSURANCE ET PRÉVOYANCE. CONTENTIEUX. - ASSUREUR AU TITRE DE LA PROTECTION JURIDIQUE (ART. L. 127-1 DU CODE DES ASSURANCES) - FACULTÉ DE PRÉSENTER UN RECOURS ADMINISTRATIF OU UNE RÉCLAMATION PRÉALABLE AU NOM DE L'ASSURÉ SANS ÊTRE TENU DE PRODUIRE UN MANDAT EXPRÈS NI UNE DÉLÉGATION DE SIGNATURE - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-01-05 PROCÉDURE. INTRODUCTION DE L'INSTANCE. QUALITÉ POUR AGIR. - ASSUREUR AU TITRE DE LA PROTECTION JURIDIQUE (ART. L. 127-1 DU CODE DES ASSURANCES) - FACULTÉ DE PRÉSENTER UN RECOURS ADMINISTRATIF AU NOM DE L'ASSURÉ SANS ÊTRE TENU DE PRODUIRE UN MANDAT EXPRÈS NI UNE DÉLÉGATION DE SIGNATURE - EXISTENCE.
</SCT>
<ANA ID="9A"> 12-03 Eu égard aux termes de l'article L. 127-1 du code des assurances, un assureur au titre de la protection juridique peut présenter un recours administratif ou une réclamation préalable, au nom de son assuré, par l'intermédiaire de l'un de ses préposés, sans être tenu de produire un mandat exprès de l'assuré ni une délégation de signature à son préposé.</ANA>
<ANA ID="9B"> 54-01-05 Eu égard aux termes de l'article L. 127-1 du code des assurances, un assureur au titre de la protection juridique peut présenter un recours administratif ou une réclamation préalable, au nom de son assuré, par l'intermédiaire de l'un de ses préposés, sans être tenu de produire un mandat exprès de l'assuré ni une délégation de signature à son préposé.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
