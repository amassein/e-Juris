<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026512053</ID>
<ANCIEN_ID>JG_L_2012_10_000000354023</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/51/20/CETATEXT000026512053.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 4ème sous-sections réunies, 19/10/2012, 354023</TITRE>
<DATE_DEC>2012-10-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>354023</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 4ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ ; SCP DEFRENOIS, LEVIS</AVOCATS>
<RAPPORTEUR>Mme Anissia Morel</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Fabienne Lambolez</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:354023.20121019</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la requête sommaire et le mémoire complémentaire, enregistrés les 14 novembre et 14 décembre 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. Christophe C, demeurant ... ; M. C demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement n° 1102649 du 14 octobre 2011 par lequel le tribunal administratif de Cergy-Pontoise a rejeté sa protestation contre les opérations électorales qui se sont déroulées les 20 et 27 mars 2011 en vue de la désignation du conseiller général du canton de Sannois ;<br/>
<br/>
              2°) d'annuler l'élection de Mme Marie-Evelyne A et de la déclarer inéligible ;<br/>
<br/>
              3°) de mettre à la charge de Mme A le versement d'une somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
	Vu le code électoral ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Anissia Morel, Auditeur,  <br/>
<br/>
              - les observations de la SCP Defrenois, Levis avocat de Mme A et de la SCP Lyon-Caen, Thiriez avocat de M. C ;<br/>
<br/>
              - les conclusions de Mme Fabienne Lambolez, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Defrenois, Levis avocat de Mme A et à la SCP Lyon-Caen, Thiriez avocat de M. C ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'à l'issue du second tour de scrutin organisé le 27 mars 2011 en vue de l'élection du conseiller général du canton de Sannois (Val d'Oise), Mme A a été élue avec 3 050 voix alors que son concurrent, M. C, en a obtenu 3 013 ; que ce dernier fait appel du jugement du 14 octobre 2011 par lequel le tribunal administratif de Cergy-Pontoise a rejeté sa protestation tendant à l'annulation de cette élection et à ce que Mme A soit déclarée inéligible ; <br/>
<br/>
              Sur le grief tiré de l'irrégularité de certaines inscriptions sur la liste électorale :<br/>
<br/>
              2. Considérant que si M. C soutient qu'auraient figuré sur la liste électorale à la fois des adresses erronées et des personnes décédées, il n'assortit ses allégations d'aucun élément qui permettrait d'établir l'existence d'une manoeuvre ; que, dans ces conditions, le grief qu'il tire de l'irrégularité de certaines inscriptions sur la liste électorale doit être écarté ;<br/>
<br/>
              Sur les griefs relatifs au décompte des voix et au déroulement du vote :<br/>
<br/>
              3. Considérant, en premier lieu, qu'aux termes du troisième alinéa de l'article L. 62-1 du code électoral : " Le vote de chaque électeur est constaté par sa signature apposée à l'encre en face de son nom sur la liste d'émargement " et qu'aux termes du second alinéa de l'article L. 64 : " Lorsqu'un électeur se trouve dans l'impossibilité de signer, l'émargement prévu par le troisième alinéa de l'article L. 62-1 est apposé par un électeur de son choix qui fait suivre sa signature de la mention suivante :  l'électeur ne peut signer lui-même " ; qu'il résulte de ces dispositions, destinées à assurer la sincérité des opérations électorales, que seule la signature personnelle, à l'encre, d'un électeur est de nature à apporter la preuve de sa participation au scrutin, sauf cas d'impossibilité dûment reporté sur la liste d'émargement ; qu'ainsi, la constatation d'une signature qui présente des différences manifestes entre les deux tours de scrutin sans qu'il soit fait mention d'un vote par procuration ne peut être regardée comme garantissant l'authenticité du vote ;<br/>
<br/>
              4. Considérant que les signatures figurant sur les listes d'émargement sous le n° 567 dans le bureau de vote n° 1,  sous le n° 1131 dans le bureau de vote n° 4 et sous le n° 919 dans le bureau de vote n° 7, présentent, sans explication, des différences significatives entre les deux tours de scrutin ; que, si ces trois émargements ne peuvent être regardés comme attestant le vote des électeurs en cause dans les conditions fixées par les dispositions des articles L. 62-1 et L. 64 du code électoral, les autres différences invoquées entre les signatures apposées sur les listes d'émargement au premier tour du scrutin et celles apposées pour les mêmes électeurs au second tour ne sont pas significatives ou trouvent leur explication dans l'utilisation par le même électeur d'un paraphe et d'une signature ou faisant apparaître, selon le cas, le nom de famille ou le nom d'usage ;<br/>
<br/>
              5. Considérant qu'il y a lieu, par suite, de retrancher trois voix tant du nombre de suffrages exprimés que du nombre des voix recueillies par Mme A ; qu'en conséquence de cette déduction, l'écart séparant Mme A de M. C s'établit à trente-quatre voix ;<br/>
<br/>
              6. Considérant, en deuxième lieu, qu'il résulte de l'instruction que le jour du second tour de scrutin ont été mis à la disposition des électeurs, dans les 13ème et 14ème bureaux de vote de la commune de Sannois, des bulletins de M. C du premier tour sur lesquels figuraient seulement les emblèmes du parti socialiste et du mouvement républicain et citoyen et qui mentionnaient que M. C était, avec sa suppléante, le " candidat du parti socialiste ", alors que les bulletins du même candidat qui auraient dû être mis à disposition des électeurs pour le second tour le désignaient, après le ralliement d'autres candidats, comme le " candidat de rassemblement de toute la gauche et des écologistes " et comportaient les emblèmes du parti de gauche, du parti communiste français, du mouvement républicain et citoyen, d'Europe écologie les verts et du parti socialiste ; que toutefois, il n'est pas contesté que les documents diffusés sept jours avant le second tour du scrutin pour la propagande officielle ainsi que les affiches utilisées pour celle-ci informaient les électeurs du ralliement de ces partis à la candidature de M. C ; qu'au surplus, de telles directives de ralliement avaient été données et diffusées au niveau national ; que dans ces conditions, l'irrégularité qu'a constituée la mise à disposition de bulletins du premier tour lors du second dans ces deux bureaux n'a pas altéré la sincérité du scrutin ;  <br/>
<br/>
              7. Considérant, en troisième lieu, qu'il n'est pas établi que le retard de vingt-cinq minutes dans l'ouverture du bureau de vote n° 7 lors du second tour aurait empêché des électeurs de participer au scrutin ; <br/>
<br/>
              Sur les griefs relatifs à la campagne électorale :<br/>
<br/>
              8. Considérant, en premier lieu, d'une part, qu'il ne résulte pas de l'instruction que Mme A aurait tiré avantage, dans le cadre de la campagne électorale, de ses fonctions de maire-adjoint de la commune de Sannois ; que les visites et déplacements qu'elle a accomplis en compagnie du maire de Sannois n'avaient pas un caractère électoral et se rattachaient aux fonctions qu'elle exerçait dans cette commune ;<br/>
<br/>
              9. Considérant, d'autre part, qu'aux termes du second alinéa de l'article L. 52-1 du code électoral : " A compter du premier jour du sixième mois précédant le mois au cours duquel il doit être procédé à des élections générales, aucune campagne de promotion publicitaire des réalisations ou de la gestion d'une collectivité ne peut être organisée sur le territoire des collectivités intéressées par le scrutin (...) " ;  <br/>
<br/>
              10. Considérant que ni les prises de position du maire de Sannois appelant à voter pour Mme A, ni les rencontres et réunions de quartier organisées par la commune avec la participation du maire et des adjoints concernés par ces manifestations ne constituent une campagne de promotion publicitaire des réalisations de la commune de Sannois ; qu'il en va de même des bulletins municipaux publiés par cette commune, dont le format, le contenu, et la périodicité n'ont pas été modifiés pendant la période précédant le scrutin ; que, si un encart dans l'un de ces bulletins municipaux annonce la réalisation future, dans le cadre du projet du " Grand Paris ", d'une gare permettant une interconnexion de voies ferrées améliorant la desserte de la commune et si la question des transports en commun était l'un des thèmes de la campagne électorale de Mme A, une telle circonstance ne suffit pas à caractériser l'existence d'une campagne de promotion publicitaire des réalisations d'une collectivité publique ; <br/>
<br/>
              11. Considérant qu'il résulte de ce qui précède que M. C n'est pas fondé à soutenir que la diffusion des documents mentionnés ci-dessus et le comportement de Mme A pendant la campagne électorale auraient altéré la sincérité du scrutin ;<br/>
<br/>
              12. Considérant, en second lieu, que si M. C met en cause le contenu des tracts diffusés dans le cadre de la campagne électorale de Mme A, il ne résulte pas de l'instruction que ces tracts aient fait l'objet d'une diffusion massive ; que, de surcroît, la date de diffusion de ces documents, le 25 mars 2011, permettait à M. C d'y répondre utilement ; qu'il en est de même du tract émanant du maire de la commune de Sannois indiquant qu'il soutenait la candidature de Mme A ; qu'ainsi, et compte tenu de l'écart des voix, le grief tiré de la diffusion de ces tracts doit être écarté ;<br/>
<br/>
              Sur le compte de campagne de Mme A :<br/>
<br/>
              13. Considérant qu'aux termes du deuxième alinéa de l'article L. 52-8 du code électoral : " Les personnes morales, à l'exception des partis ou groupements politiques, ne peuvent participer au financement de la campagne électorale d'un candidat, ni en lui consentant des dons sous quelque forme que ce soit, ni en lui fournissant des biens, services ou autres avantages directs ou indirects à des prix inférieurs à ceux qui sont habituellement pratiqués " et qu'aux termes de l'article L. 52-12  du même code : " Chaque candidat ou candidat tête de liste soumis au plafonnement prévu à l'article L. 52-11 est tenu d'établir un compte de campagne retraçant, selon leur origine, l'ensemble des recettes perçues et, selon leur nature, l'ensemble des dépenses engagées ou effectuées en vue de l'élection, hors celles de la campagne officielle, par lui-même ou pour son compte, au cours de la période mentionnée à l'article L. 52-4 (...)  " ;<br/>
<br/>
              14. Considérant, en premier lieu, que le requérant soutient, par la même argumentation que celle analysée aux points 8 et 10, que les prises de position du maire de Sannois appelant à voter pour Mme A, les rencontres et réunions de quartier organisées par la commune avec la participation du maire et des adjoints concernés ainsi que les bulletins municipaux publiés pendant la période précédant le scrutin constituent des avantages indirects ou des dons de cette commune contribuant au financement de la campagne électorale de la candidate, au sens des dispositions de l'article L. 52-8 du code électoral ; qu'il y a lieu d'écarter ce grief par les mêmes motifs que ceux énoncés aux points 8 et 10 ;<br/>
<br/>
              15. Considérant, en second lieu, que M. C soutient que les dépenses exposées en vue de l'élection de Mme A ont été sous-évaluées ou omises dans le compte de campagne de celle-ci en méconnaissance des dispositions de l'article L. 52-12 du code électoral ; qu'il résulte cependant de l'instruction que les dépenses relatives aux tracts que Mme A a fait imprimer et distribuer ainsi que celles relatives au local de campagne et à son équipement ont été retracées dans son compte de campagne et qu'elles correspondent aux sommes figurant sur les factures produites devant la Commission nationale des comptes de campagne et des financements politiques ; que les simples allégations du requérant concernant l'évaluation du coût de ces dépenses ne sont pas de nature à remettre en cause l'appréciation portée par la commission dans sa décision du 12 juillet 2011 sur la sincérité du compte de campagne de Mme A ; que, dès lors, le grief tiré de l'irrégularité du compte de Mme A doit être écarté ; <br/>
<br/>
              16. Considérant qu'il résulte de tout ce qui précède que M. C n'est pas fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Cergy-Pontoise a rejeté sa protestation tendant à l'annulation de l'élection de Mme A et à ce que celle-ci soit déclarée inéligible ;<br/>
<br/>
              Sur l'application des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              17. Considérant que ces dispositions font obstacle à ce que soit mise à la charge de Mme A, qui n'est pas la partie perdante, la somme que demande M. C au titre de ces dispositions ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées au même titre par Mme A ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
		Article 1er : La requête de M. C est rejetée. <br/>
Article 2 : Les conclusions présentées par Mme A au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à M. Christophe C, à Mme Marie-Evelyne A et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">28-04-05-01-02 ÉLECTIONS ET RÉFÉRENDUM. ÉLECTIONS MUNICIPALES. OPÉRATIONS ÉLECTORALES. DÉROULEMENT DU SCRUTIN. BULLETINS DE VOTE. - BULLETINS DU PREMIER TOUR PLACÉS AU SECOND TOUR DANS DES BUREAUX DE VOTE - IRRÉGULARITÉ AYANT ALTÉRÉ LA SINCÉRITÉ DU SCRUTIN - ABSENCE, DÈS LORS QU'IL N'EST PAS CONTESTÉ QUE LES DOCUMENTS DE PROPAGANDE ET AFFICHES OFFICIELS ONT INFORMÉ LES ÉLECTEURS DES RALLIEMENTS, QUI CORRESPONDAIENT À DES DIRECTIVES DONNÉES AU NIVEAU NATIONAL [RJ1].
</SCT>
<ANA ID="9A"> 28-04-05-01-02 Lors du second tour de scrutin, ont été mis à la disposition des électeurs, dans deux bureaux de vote, des bulletins d'un candidat du premier tour sur lesquels figuraient seulement les emblèmes du parti socialiste et du mouvement républicain et citoyen et qui mentionnaient que ce candidat était, avec sa suppléante, le « candidat du parti socialiste », alors que les bulletins du même candidat qui auraient dû être mis à disposition des électeurs pour le second tour le désignaient, après le ralliement d'autres candidats, comme le « candidat de rassemblement de toute la gauche et des écologistes » et comportaient les emblèmes du parti de gauche, du parti communiste français, du mouvement républicain et citoyen, d'Europe écologie les verts et du parti socialiste.,,Toutefois, il n'est pas contesté que les documents diffusés sept jours avant le second tour du scrutin pour la propagande officielle ainsi que les affiches utilisées pour celle-ci informaient les électeurs du ralliement de ces partis à la candidature de l'intéressé. Au surplus, de telles directives de ralliement avaient été données et diffusées au niveau national. Dans ces conditions, l'irrégularité qu'a constituée la mise à disposition de bulletins du premier tour lors du second dans ces deux bureaux n'a pas altéré la sincérité du scrutin.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Comp. CE, 24 février 1984, Elections municipales de Sète, n° 51744 51737, T. p. 633.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
