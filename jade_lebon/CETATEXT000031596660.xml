<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031596660</ID>
<ANCIEN_ID>JG_L_2015_12_000000391626</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/59/66/CETATEXT000031596660.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème / 7ème SSR, 09/12/2015, 391626, Publié au recueil Lebon</TITRE>
<DATE_DEC>2015-12-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>391626</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème / 7ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>COPPER-ROYER</AVOCATS>
<RAPPORTEUR>Mme Cécile Barrois de Sarigny</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier Domino</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:391626.20151209</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au juge des référés du tribunal administratif de Nice, sur le fondement de l'article R. 541-1 du code de justice administrative, de condamner solidairement la commune du Cannet et la société Areas Dommages à lui verser la somme de 3 000 euros à titre de provision à valoir sur l'indemnité qui lui est due en réparation du préjudice résultant pour lui de l'accident dont il a été victime le 6 juin 2012. Par une ordonnance n° 1500469 du 19 juin 2015, le juge des référés a fait droit à cette demande. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 8 juillet et 5 août 2015 au secrétariat du contentieux du Conseil d'Etat, la commune du Cannet et la société Areas Dommages demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette ordonnance ; <br/>
<br/>
              2°) de mettre à la charge de M. B...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le décret n° 2013-730 du 13 août 2013 ;  <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Cécile Barrois de Sarigny, maître des requêtes, <br/>
<br/>
              - les conclusions de M. Xavier Domino, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Copper-Royer, avocat de la commune du Cannet et de la société Areas Dommages ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant, d'une part, qu'aux termes de l'article R. 541-1 du code de justice administrative : " Le juge des référés peut, même en l'absence d'une demande au fond, accorder une provision au créancier qui l'a saisi lorsque l'existence de l'obligation n'est pas sérieusement contestable. Il peut, même d'office, subordonner le versement de la provision à la constitution d'une garantie " ; qu'en vertu de l'article R. 541-3 du même code : " L'ordonnance rendue par le président du tribunal administratif ou par son délégué est susceptible d'appel devant la cour administrative d'appel dans la quinzaine de sa notification " ; <br/>
<br/>
              2.	Considérant, d'autre part, qu'en vertu de l'article R. 811-1 du même code, dans sa version résultant du décret du 13 août 2013 portant modification du code de justice administrative, toute partie présente dans une instance devant le tribunal administratif peut, en principe, interjeter appel contre toute décision juridictionnelle rendue dans cette instance, à l'exception des litiges, énumérés au 1° à 8° de cet article, pour lesquels le tribunal administratif statue en premier et dernier ressort ; que les litiges mentionnés au 8° de cet article sont les actions indemnitaires, autres que celles portant sur des litiges énumérés aux 1° à 7°, " lorsque le montant des indemnités demandées est inférieur au montant déterminé par les articles R. 222-14 et R. 222-15 " ; que l'article R. 222-14 fait référence aux demandes dont le montant n'excède pas 10 000 euros ; que, selon l'article R. 222-15, le montant fixé à l'article R. 222-14, est déterminé par " la valeur totale des sommes demandées dans la requête introductive d'instance " ; qu'en outre, après l'énumération des différents litiges sur lesquels le tribunal administratif statue en premier et dernier ressort, le onzième alinéa de l'article R. 811-1 dispose que " Les ordonnances prises sur le fondement du titre IV du livre V sont également rendues en premier et dernier ressort lorsque l'obligation dont se prévaut le requérant pour obtenir le bénéfice d'une provision porte sur un litige énuméré aux alinéas précédents " ;<br/>
<br/>
              3.	Considérant que les dispositions de l'article R. 541-3 du code de justice administrative doivent être combinées avec celles du onzième alinéa de l'article R. 811-1 ; qu'il en résulte que les ordonnances rendues par le juge des référés du tribunal administratif statuant sur une demande de provision sur le fondement de l'article R. 541-1 du code de justice administrative sont rendues en dernier ressort lorsque l'obligation dont se prévaut le requérant pour obtenir le bénéfice d'une provision se rattache à l'un des litiges énumérés aux 1° à 8° de l'article R. 811-1 ; <br/>
<br/>
              4.	Considérant que, dans le cas où l'obligation se rattache à une action indemnitaire autre que celles portant sur des litiges énumérés aux 1° à 7° de l'article R. 811-1, le montant de l'obligation en cause doit être regardé comme excédant le montant déterminé par les articles R. 222-14 et R. 222-15 lorsque les conclusions présentées en référé tendent au versement d'une provision d'un montant supérieur à 10 000 euros ; à défaut, lorsque le montant demandé à titre de provision n'atteint pas cette somme, l'étendue de l'obligation doit être appréciée au vu de ce qui est exposé à l'appui de la demande de provision et, le cas échéant, de l'existence d'une demande corrélative d'expertise ; qu'en particulier, quand le requérant a, parallèlement à sa demande de provision, demandé qu'une expertise soit ordonnée afin de déterminer l'étendue de son préjudice, en se réservant de fixer le montant de sa demande au vu du rapport de l'expert, le montant de l'obligation dont il se prévaut pour obtenir une provision ne peut être tenu comme étant inférieur au montant fixé à l'article R. 222-14 ; qu'ainsi, dans ce dernier cas, la décision du juge des référés statuant sur la demande de provision est susceptible d'appel ;<br/>
<br/>
              5.	Considérant que la commune du Cannet et la société Areas Dommages ont saisi le Conseil d'Etat d'une requête formée contre l'ordonnance du 19 juin 2015 par laquelle le juge des référés du tribunal administratif de Nice les a condamnées solidairement à verser à M. B... la somme de 3 000 euros qu'il sollicitait à titre de provision à valoir sur l'indemnité qui lui est due en réparation du préjudice résultant pour lui de l'accident dont il a été victime le 6 juin 2012 ; qu'il ressort des pièces du dossier du juge des référés que, parallèlement à cette demande de provision, M. B...a demandé que soit ordonnée une expertise pour évaluer l'étendue de son préjudice, demande à laquelle le juge des référés du tribunal administratif a fait d'ailleurs droit par ordonnance du 2 juin 2015 ; qu'il résulte de ce qui a été dit précédemment que le montant de l'obligation dont se prévaut M. B...pour obtenir une provision ne peut, dans ces conditions, être tenu pour inférieur au montant fixé à l'article R. 222-14 ; qu'en conséquence, l'ordonnance par laquelle le juge des référés a statué sur sa demande de provision est susceptible d'appel ; que, par suite, il y a lieu d'attribuer à la cour administrative d'appel de Marseille le jugement de la requête de la commune du Cannet et de la société Areas Dommages, qui présente le caractère d'un appel ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le jugement de la requête de la commune du Cannet et de la société Areas Dommages est attribué à la cour administrative d'appel de Marseille.<br/>
<br/>
Article 2 : La présente décision sera notifiée à la commune du Cannet, à la société Areas Dommages et au président de la cour administrative d'appel de Marseille. Copie en sera adressée à M. A...B.... <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">17-05-012 COMPÉTENCE. COMPÉTENCE À L'INTÉRIEUR DE LA JURIDICTION ADMINISTRATIVE. COMPÉTENCE EN PREMIER ET DERNIER RESSORT DES TRIBUNAUX ADMINISTRATIFS. - RÉFÉRÉ-PROVISION - 1) JUGE DES RÉFÉRÉS DU TRIBUNAL ADMINISTRATIF STATUANT EN PREMIER ET DERNIER RESSORT - EXISTENCE, LORSQUE L'OBLIGATION EN CAUSE SE RATTACHE À L'UN DES LITIGES ÉNUMÉRÉS AUX 1° À 8° DE L'ART. R. 811-1 DU CJA - 2) CONDITIONS D'APPLICATION DU 8° DE L'ART. R. 811-1 DU CJA - A) CAS OÙ LA DEMANDE DE PROVISION EXCÈDE 10 000 EUROS - B) CAS OÙ LA DEMANDE DE PROVISION N'EXCÈDE PAS 10 000 EUROS - INCIDENCE D'UNE DEMANDE D'EXPERTISE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-03-015-01 PROCÉDURE. PROCÉDURES DE RÉFÉRÉ AUTRES QUE CELLES INSTITUÉES PAR LA LOI DU 30 JUIN 2000. RÉFÉRÉ-PROVISION. COMPÉTENCE. - COMPÉTENCE AU SEIN DE LA JURIDICTION ADMINISTRATIVE - 1) JUGE DU RÉFÉRÉ-PROVISION DU TRIBUNAL ADMINISTRATIF STATUANT EN PREMIER ET DERNIER RESSORT - EXISTENCE, LORSQUE L'OBLIGATION EN CAUSE SE RATTACHE À L'UN DES LITIGES ÉNUMÉRÉS AUX 1° À 8° DE L'ART. R. 811-1 DU CJA - 2) CONDITIONS D'APPLICATION DU 8° DE L'ART. R. 811-1 DU CJA - A) CAS OÙ LA DEMANDE DE PROVISION EXCÈDE 10 000 EUROS - B) CAS OÙ LA DEMANDE DE PROVISION N'EXCÈDE PAS 10 000 EUROS - INCIDENCE D'UNE DEMANDE D'EXPERTISE.
</SCT>
<ANA ID="9A"> 17-05-012 1) Il résulte de la combinaison des articles R. 541-3 et R. 811-1 du code de justice administrative (CJA) que les ordonnances rendues par le juge des référés du tribunal administratif statuant sur une demande de provision sur le fondement de l'article R. 541-1 du même code sont rendues en dernier ressort lorsque l'obligation dont se prévaut le requérant pour obtenir le bénéfice d'une provision se rattache à l'un des litiges énumérés aux 1° à 8° de l'article R. 811-1.... ,,2) a) Dans le cas où l'obligation se rattache à une action indemnitaire autre que celles portant sur des litiges énumérés aux 1° à 7° de l'article R. 811-1, le montant de l'obligation en cause doit être regardé comme excédant le montant déterminé par les articles R. 222-14 et R. 222-15 lorsque les conclusions présentées en référé tendent au versement d'une provision d'un montant supérieur à 10 000 euros.... ,,b) A défaut, lorsque le montant demandé à titre de provision n'atteint pas cette somme, l'étendue de l'obligation doit être appréciée au vu de ce qui est exposé à l'appui de la demande de provision et, le cas échéant, de l'existence d'une demande corrélative d'expertise. En particulier, quand le requérant a, parallèlement à sa demande de provision, demandé qu'une expertise soit ordonnée afin de déterminer l'étendue de son préjudice, en se réservant de fixer le montant de sa demande au vu du rapport de l'expert, le montant de l'obligation dont il se prévaut pour obtenir une provision ne peut être tenu comme étant inférieur au montant fixé à l'article R. 222-14. Ainsi, dans ce dernier cas, la décision du juge des référés statuant sur la demande de provision est susceptible d'appel.</ANA>
<ANA ID="9B"> 54-03-015-01 1) Il résulte de la combinaison des articles R. 541-3 et R. 811-1 du code de justice administrative (CJA) que les ordonnances rendues par le juge des référés du tribunal administratif statuant sur une demande de provision sur le fondement de l'article R. 541-1 du même code sont rendues en dernier ressort lorsque l'obligation dont se prévaut le requérant pour obtenir le bénéfice d'une provision se rattache à l'un des litiges énumérés aux 1° à 8° de l'article R. 811-1.... ,,2) a) Dans le cas où l'obligation se rattache à une action indemnitaire autre que celles portant sur des litiges énumérés aux 1° à 7° de l'article R. 811-1, le montant de l'obligation en cause doit être regardé comme excédant le montant déterminé par les articles R. 222-14 et R. 222-15 lorsque les conclusions présentées en référé tendent au versement d'une provision d'un montant supérieur à 10 000 euros.... ,,b) A défaut, lorsque le montant demandé à titre de provision n'atteint pas cette somme, l'étendue de l'obligation doit être appréciée au vu de ce qui est exposé à l'appui de la demande de provision et, le cas échéant, de l'existence d'une demande corrélative d'expertise. En particulier, quand le requérant a, parallèlement à sa demande de provision, demandé qu'une expertise soit ordonnée afin de déterminer l'étendue de son préjudice, en se réservant de fixer le montant de sa demande au vu du rapport de l'expert, le montant de l'obligation dont il se prévaut pour obtenir une provision ne peut être tenu comme étant inférieur au montant fixé à l'article R. 222-14. Ainsi, dans ce dernier cas, la décision du juge des référés statuant sur la demande de provision est susceptible d'appel.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
