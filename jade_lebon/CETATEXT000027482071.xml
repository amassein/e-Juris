<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027482071</ID>
<ANCIEN_ID>JG_L_2013_05_000000350887</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/48/20/CETATEXT000027482071.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème / 4ème SSR, 31/05/2013, 350887</TITRE>
<DATE_DEC>2013-05-31</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>350887</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème / 4ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP VINCENT, OHL ; SCP PEIGNOT, GARREAU, BAUER-VIOLAS ; SCP GATINEAU, FATTACCINI</AVOCATS>
<RAPPORTEUR>Mme Marie Gautier-Melleray</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Fabienne Lambolez</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2013:350887.20130531</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 13 juillet 2011 et 13 octobre 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. C...E..., Mme D...E..., Mlle A...E...et M. B...E..., demeurant... ; les consorts E... demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 10LY00710 du 12 mai 2011 de la cour administrative d'appel de Lyon en tant qu'il a rejeté leur requête tendant à l'annulation du jugement n° 0704382 du 19 janvier 2010 par lequel le tribunal administratif de Grenoble a rejeté leurs demandes tendant à ce que la commune de Chamrousse soit déclarée responsable des conséquences dommageables de l'accident de ski dont M. C...E...a été victime le 20 avril 2006, à ce qu'une expertise médicale soit prescrite en vue de déterminer l'étendue des préjudices subis et à ce que la commune de Chamrousse soit condamnée à lui verser une indemnité provisionnelle de 20 000 euros ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur requête ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Chamrousse le versement d'une somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
	Vu le code du tourisme, notamment son article L. 342-13 ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie Gautier-Melleray, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Fabienne Lambolez, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Peignot, Garreau, Bauer-Violas, avocat des ConsortsE..., à la SCP Vincent, Ohl, avocat de la commune de Chamrousse, et à la SCP Gatineau, Fattaccini, avocat de la caisse primaire d'assurance maladie de l'Essonne ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, le 20 avril 2006, alors qu'il skiait sur la piste bleue " de la Bérangère " dans la station de Chamrousse, M. C... E...s'est engagé sur un chemin hors-piste habituellement emprunté par les skieurs pour rejoindre une autre piste bleue de la station et qu'il a été victime d'une chute sur les rochers situés sur les bas-côtés du chemin ; que les consortsE..., attribuant cet accident à une faute du maire de Chamrousse, ont recherché la responsabilité de la commune devant le tribunal administratif de Grenoble qui a rejeté leur demande par un jugement du 19 janvier 2010 ; qu'ils se pourvoient en cassation contre l'arrêt du 12 mai 2011 de la cour administrative d'appel de Lyon en tant qu'il a rejeté l'appel qu'ils ont formé contre ce jugement ; qu'ayant reçu communication du pourvoi, la caisse primaire d'assurance maladie de l'Essonne a également demandé l'annulation du même arrêt en tant qu'il a rejeté les conclusions qu'elle avait formées en appel ;<br/>
<br/>
              Sur la régularité de l'arrêt :<br/>
<br/>
              2. Considérant que, contrairement à ce que soutient la caisse primaire d'assurance maladie de l'Essonne, la minute de l'arrêt attaqué est revêtue des signatures requises par l'article R. 741-7 du code de justice administrative ;<br/>
<br/>
              Sur le bien-fondé de l'arrêt :<br/>
<br/>
              3. Considérant, d'une part, que la cour n'a pas commis d'erreur de droit en indiquant que le maire ne devait prendre des dispositions pour assurer la sécurité des skieurs sur le chemin hors-piste habituellement emprunté par les skieurs et sur lequel a eu lieu l'accident qu'en cas de danger exceptionnel ; qu'en estimant que la présence de rochers en bordure de ce chemin ne constituait pas un danger exceptionnel compte tenu de leur caractère visible et qu'ainsi le maire n'avait pas commis de faute en ne la signalant pas, la cour administrative d'appel n'a pas inexactement qualifié les faits de l'espèce ; qu'en se fondant sur ces motifs, elle a nécessairement jugé que le fait de ne pas avoir interdit l'usage du chemin par les skieurs n'était pas davantage fautif ; qu'eu égard à l'argumentation dont elle était saisie, la cour administrative d'appel n'a pas entaché son arrêt d'une insuffisance de motivation en se prononçant implicitement sur ce point ; <br/>
<br/>
              4. Considérant, d'autre part, qu'il incombe à l'exploitant du domaine skiable, dont en vertu de l'article L.342-13 du code du tourisme la responsabilité ne peut être recherchée que devant le juge judiciaire dès lors qu'il gère un service public industriel et commercial, de signaler sur le terrain les limites de ce domaine ; que, par suite, la cour n'a pas commis d'erreur de qualification juridique en ne retenant pas une faute du maire dans l'exercice des pouvoirs de police, ayant consisté à ne pas mettre en place, en limite du domaine skiable, une signalisation indiquant que le parcours emprunté par M. E... ne constituait pas le prolongement de la piste " de la Bérangère " ;<br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que le pourvoi des consorts E...et celui de la caisse primaire d'assurance maladie de l'Essonne doivent être rejetés ; <br/>
<br/>
              6. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions de la commune de Chamrousse au titre des dispositions de l'article L. 761-1 du code de justice administrative ; que ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de la commune de Chamrousse qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
		Article 1er : Le pourvoi des consorts E...est rejeté.<br/>
<br/>
Article 2 : Le pourvoi de la caisse primaire d'assurance maladie de l'Essonne est rejeté.<br/>
<br/>
Article 3 : Les conclusions de la commune de Chamrousse présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 4 : La présente décision sera notifiée à M. C...E..., à Mme D...E..., à Mlle A...E..., à M. B...E..., à la commune de Chamrousse, à la caisse primaire d'assurance maladie de l'Essonne et à la caisse régionale d'assurance maladie d'Ile-de-France.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">135-02-03-02-02-01-02 COLLECTIVITÉS TERRITORIALES. COMMUNE. ATTRIBUTIONS. POLICE. POLICE DE LA SÉCURITÉ. POLICE DES LIEUX DANGEREUX. PISTES DE SKI. - SIGNALISATION DES LIMITES DU DOMAINE SKIABLE - RESPONSABILITÉ - MAIRE DE LA COMMUNE - ABSENCE - EXPLOITANT DU DOMAINE SKIABLE - EXISTENCE.
</SCT>
<ANA ID="9A"> 135-02-03-02-02-01-02 C'est non au maire de la commune, mais à l'exploitant du domaine skiable, dont, en vertu de l'article L. 342-13 du code du tourisme la responsabilité ne peut être recherchée que devant le juge judiciaire dès lors qu'il gère un service public industriel et commercial, qu'il incombe de signaler sur le terrain les limites de ce domaine.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
