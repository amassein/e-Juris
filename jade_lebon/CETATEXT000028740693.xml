<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028740693</ID>
<ANCIEN_ID>JG_L_2014_03_000000356577</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/74/06/CETATEXT000028740693.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème / 4ème SSR, 17/03/2014, 356577</TITRE>
<DATE_DEC>2014-03-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>356577</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème / 4ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>LE PRADO ; SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>M. Gérald Bégranger</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Fabienne Lambolez</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:356577.20140317</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 8 février et 7 mai 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. A... C..., Mme D... C...et Mme B...C..., demeurant... ; les consorts C...demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 10NT01233 du 8 décembre 2011 par lequel la cour administrative d'appel de Nantes a rejeté leur appel contre le jugement n° 0607922 du 9 avril 2010 du tribunal administratif de Nantes rejetant leur demande tendant à la condamnation du département de Loire-Atlantique à leur verser diverses indemnités au titre des préjudices résultant pour eux du décès de Candie C...survenu durant son placement au foyer départemental de l'enfance de Saint-Sébastien-sur-Loire ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel ;<br/>
<br/>
              3°) de mettre à la charge du département de Loire-Atlantique la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 15 janvier 2014, présentée pour les consortsC... ;<br/>
<br/>
              Vu le code de procédure pénale ;<br/>
<br/>
              Vu la loi n° 68-1250 du 31 décembre 1968 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Gérald Bégranger, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Fabienne Lambolez, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de M. et Mme A...C...et de Mme B...C...et à Me Le Prado, avocat du département de Loire-Atlantique ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que Candie C...est décédée le 26 septembre 1998, à l'âge de quinze ans, des suites d'un coma acidocétosique provoqué par une carence en insuline, alors qu'elle avait quitté sans autorisation le foyer départemental de l'enfance de Saint-Sébastien-sur-Loire où elle était placée depuis le 18 septembre 1998 ; que des poursuites pénales, dans le cadre desquelles les parents et la soeur de la victime se sont portés parties civiles, ont été engagées contre le directeur et un membre du personnel de l'établissement pour non-assistance à personne en danger ; que, par un arrêt du 20 novembre 2001, la cour d'appel de Rennes a condamné le directeur à six mois d'emprisonnement avec sursis pour avoir omis de porter secours à Candie C...et a déclaré la juridiction judiciaire incompétente pour connaître de l'action en réparation des préjudices des consorts C...résultant du décès de celle-ci ; que cet arrêt n'a fait l'objet que d'un pourvoi du condamné, que la Cour de cassation, par un arrêt du 18 juin 2002, a décidé de ne pas admettre ; que, par une lettre du 1er février 2008, le département de Loire-Atlantique a rejeté la demande indemnitaire dont les consorts C...l'avaient saisi le 20 mars 2006, en leur opposant la prescription de leur créance ; que, par un jugement du 9 avril 2010, le tribunal administratif de Nantes a rejeté leur demande tendant à la condamnation du département de Loire-Atlantique à leur verser diverses indemnités au même titre ; que les consorts C...se pourvoient en cassation contre l'arrêt du 8 décembre 2011 par lequel la cour administrative d'appel de Nantes a rejeté leur appel contre ce jugement ;<br/>
<br/>
              2. Considérant, d'une part, qu'aux termes de l'article 1er de la loi du 31 décembre 1968 relative à la prescription des créances sur l'Etat, les départements, les communes et les établissements publics : " Sont prescrites, au profit de l'Etat, des départements et des communes, sans préjudice des déchéances particulières édictées par la loi, et sous réserve des dispositions de la présente loi, toutes créances qui n'ont pas été payées dans un délai de quatre ans à partir du premier jour de l'année suivant celle au cours de laquelle les droits ont été acquis (...) " ; qu'aux termes de l'article 2 de la même loi : " La prescription est interrompue par :  / Toute demande de paiement ou toute réclamation écrite adressée par un créancier à l'autorité administrative, dès lors que la demande ou la réclamation a trait au fait générateur, à l'existence, au montant ou au paiement de la créance, alors même que l'administration saisie n'est pas celle qui aura finalement la charge du règlement. / Tout recours formé devant une juridiction, relatif au fait générateur, à l'existence, au montant ou au paiement de la créance, quel que soit l'auteur du recours et même si la juridiction saisie est incompétente pour en connaître, et si l'administration qui aura finalement la charge du règlement n'est pas partie à l'instance (...). / Un nouveau délai de quatre ans court à compter du premier jour de l'année suivant celle au cours de laquelle a eu lieu l'interruption. Toutefois, si l'interruption résulte d'un recours juridictionnel, le nouveau délai court à partir du premier jour de l'année suivant celle au cours de laquelle la décision est passée en force de chose jugée " ; <br/>
<br/>
              3. Considérant que lorsque la victime d'un dommage causé par des agissements de nature à engager la responsabilité d'une collectivité publique dépose contre l'auteur de ces agissements une plainte avec constitution de partie civile, ou se porte partie civile afin d'obtenir des dommages et intérêts dans le cadre d'une instruction pénale déjà ouverte, l'action ainsi engagée présente, au sens des dispositions précitées de l'article 2 de la loi du 31 décembre 1968, le caractère d'un recours relatif au fait générateur de la créance que son auteur détient sur la collectivité et interrompt par suite le délai de prescription de cette créance ; qu'en revanche, ne présentent un tel caractère ni l'engagement de l'action publique ni l'exercice par le condamné ou par le ministère public des voies de recours contre les décisions auxquelles cette action donne lieu en première instance et en appel ; <br/>
<br/>
              4. Considérant que l'arrêt attaqué retient que le délai de la prescription de la créance des consortsC..., qui avait commencé à courir le 1er janvier 1999, premier jour de l'année civile suivant celle au cours de laquelle le dommage s'est produit, a été interrompu par la constitution de partie civile des intéressés dans le cadre de l'action pénale engagée contre le directeur et un membre du personnel du foyer départemental de l'enfance de Saint-Sébastien-sur-Loire, que cette interruption a pris fin avec l'arrêt du 20 novembre 2001 par lequel la cour d'appel de Rennes a déclaré la juridiction judiciaire incompétente pour réparer le préjudice des consortsC..., que le délai a recommencé à courir le 1er janvier 2002 et qu'il était expiré lorsque les intéressés ont saisi d'une demande indemnitaire le département de la Loire-Atlantique, le 20 mars 2006 ; que la cour administrative d'appel, qui a relevé que le directeur du foyer avait présenté un pourvoi en cassation contre l'arrêt du 20 novembre 2001 en tant qu'il le condamnait à une peine de six mois d'emprisonnement assortie du sursis, n'a pas commis d'erreur de droit en ne regardant pas ce pourvoi, qui ne concernait que l'action publique, comme un recours relatif au fait générateur de la créance des consortsC..., de nature à interrompre à nouveau le délai de prescription de cette créance ; <br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que le pourvoi des consorts C... doit être rejeté, y compris leurs conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
		Article 1er : Le pourvoi des consorts C...est rejeté.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. A...C..., à Mme D...C..., à Mme B...C...et au département de Loire-Atlantique.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">18-04-02-05 COMPTABILITÉ PUBLIQUE ET BUDGET. DETTES DES COLLECTIVITÉS PUBLIQUES - PRESCRIPTION QUADRIENNALE. RÉGIME DE LA LOI DU 31 DÉCEMBRE 1968. INTERRUPTION DU COURS DU DÉLAI. - VICTIME DEMANDANT RÉPARATION D'UN DOMMAGE CAUSÉ PAR DES AGISSEMENTS DE NATURE À ENGAGER LA RESPONSABILITÉ D'UNE COLLECTIVITÉ PUBLIQUE - RECOURS RELATIF AU FAIT GÉNÉRATEUR DE LA CRÉANCE - NOTION - INCLUSION - PLAINTE AVEC CONSTITUTION DE PARTIE CIVILE OU CONSTITUTION DE PARTIE CIVILE DANS LE CADRE D'UNE INSTRUCTION DÉJÀ OUVERTE - EXCLUSION - ENGAGEMENT DE L'ACTION PUBLIQUE OU EXERCICE PAR LE CONDAMNÉ OU LE MINISTÈRE PUBLIC DE VOIES DE RECOURS CONTRE LES DÉCISIONS JURIDICTIONNELLES AUXQUELLES CETTE ACTION DONNE LIEU.
</SCT>
<ANA ID="9A"> 18-04-02-05 Lorsque la victime d'un dommage causé par des agissements de nature à engager la responsabilité d'une collectivité publique dépose contre l'auteur de ces agissements une plainte avec constitution de partie civile, ou se porte partie civile afin d'obtenir des dommages et intérêts dans le cadre d'une instruction pénale déjà ouverte, l'action ainsi engagée présente, au sens des dispositions de l'article 2 de la loi n° 68-1250 du 31 décembre 1968, le caractère d'un recours relatif au fait générateur de la créance que son auteur détient sur la collectivité et interrompt par suite le délai de prescription de cette créance. En revanche, ne présentent un tel caractère ni l'engagement de l'action publique, ni l'exercice par le condamné ou par le ministère public des voies de recours contre les décisions auxquelles cette action donne lieu en première instance et en appel.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
