<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042545466</ID>
<ANCIEN_ID>JG_L_2020_11_000000431508</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/54/54/CETATEXT000042545466.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 20/11/2020, 431508</TITRE>
<DATE_DEC>2020-11-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>431508</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Contentieux des pensions</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP RICHARD ; SCP L. POULET-ODENT</AVOCATS>
<RAPPORTEUR>M. Jean-Yves Ollier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Mireille Le Corre</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:431508.20201120</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B... A... a demandé au tribunal administratif de Rouen d'annuler, d'une part, l'avis défavorable émis par la Caisse des dépôts et consignations le 3 août 2016 sur sa demande d'allocation temporaire d'invalidité et, d'autre part, la décision du 14 octobre 2016 par laquelle la Caisse des dépôts et consignations a rejeté son recours gracieux contre cet avis. Par un jugement n° 1603760 du 30 octobre 2018, le tribunal administratif de Rouen a rejeté sa demande.<br/>
<br/>
              Par une ordonnance n° 18DA02560 du 3 juin 2019, le président de la cour administrative d'appel de Douai a transmis au Conseil d'Etat le pourvoi, enregistré le 17 décembre 2018 au greffe de la cour, présenté par Mme A.... <br/>
<br/>
              Par ce pourvoi et par un nouveau mémoire, enregistré le 5 août 2019 au secrétariat du contentieux du Conseil d'Etat, Mme A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses demandes ;<br/>
<br/>
              3°) de mettre à la charge de la Caisse des dépôts et consignations la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la défense ;<br/>
              - le code des pensions civiles et militaires de retraite ;<br/>
              - le code des pensions militaires d'invalidité et des victimes de la guerre ;<br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - la loi n° 84-53 du 26 janvier 1984 ;<br/>
              - le décret n° 68-756 du 13 août 1968 ;<br/>
              - le décret n° 2005-442 du 2 mai 2005 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Yves Ollier, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Mireille Le Corre, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Richard, avocat de Mme A... et à la SCP L. Poulet, Odent, avocat de la Caisse des dépôts et consignations ;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que Mme A..., qui était élève-officier sous contrat du 23 septembre 2009 au 1er février 2010, date à laquelle elle a été radiée des contrôles, a été victime d'un premier accident de service le 6 décembre 2009. Par une décision du 14 mars 2013, le ministre de la défense a rejeté la demande de pension militaire d'invalidité qu'elle avait présentée au titre des séquelles de cet accident. Recrutée par le service départemental d'incendie et de secours de la Seine-Maritime comme rédacteur territorial stagiaire à compter du 20 février 2012, puis titulaire à compter du 6 mars 2013, elle a été victime d'un second accident de service le 10 décembre 2013. Le 27 janvier 2015, Mme A... a demandé à bénéficier d'une allocation temporaire d'invalidité au titre des séquelles de ces deux accidents. Par un arrêté du 17 février 2016, le président du service départemental d'incendie et de secours de la Seine-Maritime a fait droit à sa demande, sous réserve de l'avis conforme de la Caisse des dépôts et consignations. Mme A... se pourvoit en cassation contre le jugement du tribunal administratif de Rouen rejetant sa demande tendant à l'annulation de l'avis défavorable émis par la Caisse des dépôts et consignations et de la décision rejetant son recours gracieux contre cet avis. <br/>
<br/>
              2. D'une part, l'article L. 4123-2 du code de la défense, applicable en vertu de l'article L. 4111-2 du même code aux militaires servant en vertu d'un contrat, dispose que : " Les militaires bénéficient des régimes de pensions ainsi que des prestations de sécurité sociale dans les conditions fixées par le code des pensions civiles et militaires de retraite, le code des pensions militaires d'invalidité et des victimes de la guerre et le code de la sécurité sociale (...) ". Aux termes de l'article L. 2, alors applicable, du code des pensions militaires d'invalidité et des victimes de la guerre : " Ouvrent droit à pension : / 1° Les infirmités résultant de blessures reçues par suite d'événements de guerre ou d'accidents éprouvés par le fait ou à l'occasion du service ; / 2° Les infirmités résultant de maladies contractées par le fait ou à l'occasion du service ; / 3° L'aggravation par le fait ou à l'occasion du service d'infirmités étrangères au service (...) ". Aux termes de l'article L. 4, alors applicable, du même code : " Les pensions sont établies d'après le degré d'invalidité.  / Sont prises en considération les infirmités entraînant une invalidité égale ou supérieure à 10 %. (...) ". Il résulte de ces dispositions que les militaires, y compris sous contrat, qui, en raison d'un accident de service, ont subi une infirmité entraînant une incapacité égale ou supérieure à 10 %, peuvent bénéficier d'une pension militaire d'invalidité.<br/>
<br/>
              3. D'autre part, l'article 65 de la loi du 11 janvier 1984 portant dispositions statutaires relatives à la fonction publique de l'Etat dispose que : " Le fonctionnaire qui a été atteint d'une invalidité résultant d'un accident de service ayant entraîné une incapacité permanente d'au moins 10 % ou d'une maladie professionnelle peut prétendre à une allocation temporaire d'invalidité cumulable avec son traitement dont le montant est fixé à la fraction du traitement minimal de la grille mentionnée à l'article 15 du titre Ier du statut général, correspondant au pourcentage d'invalidité ". Le III de l'article 119 de la loi du 26 janvier 1984 relative à la fonction publique territoriale maintient en vigueur et étend à l'ensemble des agents concernés par cette loi les dispositions de l'article L. 417-8 du code des communes aux termes duquel  : " Les communes et les établissements publics communaux et intercommunaux sont tenus d'allouer aux agents qui ont été atteints d'une invalidité résultant d'un accident de service ayant entraîné une incapacité permanente au moins égale à un taux minimum déterminé par l'autorité supérieure ou d'une maladie professionnelle une allocation temporaire d'invalidité cumulable avec le traitement, dans les mêmes conditions que pour les fonctionnaires de l'Etat ". L'article 2 du décret du 2 mai 2005 relatif à l'attribution de l'allocation temporaire d'invalidité aux fonctionnaires relevant de la fonction publique territoriale et de la fonction publique hospitalière dispose que : " L'allocation est attribuée aux fonctionnaires maintenus en activité qui justifient d'une invalidité permanente résultant : a) Soit d'un accident de service ayant entraîné une incapacité permanente d'un taux au moins égal à 10 % ; b) Soit de l'une des maladies d'origine professionnelle énumérées par les tableaux mentionnés à l'article L. 461-2 du code de la sécurité sociale ; c) Soit d'une maladie reconnue d'origine professionnelle dans les conditions mentionnées aux alinéas 3 et 4 de l'article L. 461-1 du code de la sécurité sociale, sous réserve des dispositions de l'article 6 du présent décret (...) ". Aux termes de l'article 5 de ce décret : " Le taux d'invalidité est déterminé compte tenu du barème indicatif prévu à l'article L. 28 du code des pensions civiles et militaires de retraite. / Dans le cas d'aggravation d'infirmités préexistantes, le taux d'invalidité à prendre en considération est apprécié par rapport à la validité restante du fonctionnaire ". Aux termes de l'article L. 28 du code des pensions civiles et militaires de retraite : " (...) Le taux d'invalidité est déterminé compte tenu d'un barème indicatif fixé par décret (...) ". Le barème visé par ces dispositions est annexé au décret du 13 août 1968 pris en application de l'article L. 28. Il précise, en son chapitre préliminaire I B, les conditions dans lesquelles il est tenu compte d'infirmités successives résultant d'événements différents imputables au service. Aux termes de l'article 10 du décret du 2 mai 2005 : " En cas de survenance d'un nouvel accident ouvrant droit à allocation et sous réserve qu'une demande ait été formulée dans les délais prescrits à l'article 3, il est procédé à un nouvel examen des droits du requérant compte tenu de l'ensemble des infirmités. Une nouvelle allocation est éventuellement accordée, en remplacement de la précédente, pour une durée de cinq ans (...) ".<br/>
<br/>
              4. Il résulte des dispositions citées au point 3 que, dans l'hypothèse où un fonctionnaire territorial a subi successivement deux accidents de service qui, pris isolément, se traduisent chacun par un taux d'incapacité inférieur à 10 %, mais qui, cumulés, atteignent ce seuil, ce fonctionnaire peut prétendre à une allocation temporaire d'invalidité tenant compte de l'ensemble de ces infirmités. <br/>
<br/>
              5. Il doit en aller de même, dès lors qu'en conséquence de l'article 119 de la loi du 26 janvier 1984, mentionné au point 3, l'allocation temporaire d'invalidité est allouée dans les mêmes conditions aux fonctionnaires territoriaux et aux fonctionnaires de l'Etat, dans le cas où le fonctionnaire appartenait à la fonction publique de l'Etat à la date du premier accident de service et était devenu fonctionnaire territorial à la date du second accident de service.<br/>
<br/>
              6. Les dispositions citées au point 3 doivent recevoir la même interprétation dans le cas où le fonctionnaire territorial avait, à la date du premier accident de service, la qualité de militaire, alors même que les conditions d'indemnisation forfaitaire des séquelles des accidents de service dont sont victimes les militaires et les fonctionnaires civils relèvent de régimes différents, dès lors qu'aucune différence de situation ne justifie, au regard du principe d'égalité, compte tenu de la nature et de l'objet de l'allocation temporaire d'invalidité, que l'incapacité résultant d'un premier accident de service subi en qualité de militaire ne soit pas prise en compte pour le bénéfice d'une allocation temporaire d'invalidité alors qu'elle le serait si cet accident avait été subi en tant que fonctionnaire civil. A cet égard, le décret du 2 mai 2005, dont l'article 14 se borne à traiter le cas de l'agent titulaire déjà bénéficiaire d'une allocation temporaire d'invalidité qui passe d'une fonction publique à une autre, ne saurait être interprété comme excluant la prise en compte, pour l'attribution de cette allocation, de l'incapacité résultant d'un accident de service antérieurement subi par un agent alors qu'il avait la qualité de militaire.<br/>
<br/>
              7. Il résulte de ce qui précède, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, que Mme A... est fondée à soutenir que le tribunal administratif de Rouen a commis une erreur de droit en jugeant, après avoir relevé que les infirmités survenues lorsqu'elle était officier sous contrat relevaient d'un régime spécial prévu par le code des pensions civiles et militaires de retraite, qu'elles ne pouvaient être prises en compte pour le calcul du taux d'invalidité ouvrant droit à une allocation temporaire d'invalidité, en l'absence de dispositions prévoyant une telle possibilité. Mme A... est, par suite, fondée à demander l'annulation du jugement qu'elle attaque. <br/>
<br/>
              8. Il y a lieu de mettre à la charge de la Caisse des dépôts et consignations la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du 30 octobre 2018 du tribunal administratif de Rouen est annulé.  <br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Rouen. <br/>
Article 3 : La Caisse des dépôts et consignations versera à Mme A... la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 4 : La présente décision sera notifiée à Mme B... A... et à la Caisse des dépôts et consignations. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-04-03-03-02 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. PRINCIPES GÉNÉRAUX DU DROIT. ÉGALITÉ DEVANT LE SERVICE PUBLIC. ÉGALITÉ DE TRAITEMENT DES AGENTS PUBLICS. - INTERPRÉTATION CONFORME - DISPOSITIONS RELATIVES À L'ATTRIBUTION DE L'ALLOCATION TEMPORAIRE D'INVALIDITÉ PRÉVOYANT LA POSSIBILITÉ DE TENIR COMPTE D'ACCIDENTS DE SERVICE SUCCESSIFS SUBIS PAR UN FONCTIONNAIRE AYANT CHANGÉ DE FONCTION PUBLIQUE - APPLICATION AUX MILITAIRES DEVENUS FONCTIONNAIRES TERRITORIAUX - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">36-08-03-01 FONCTIONNAIRES ET AGENTS PUBLICS. RÉMUNÉRATION. INDEMNITÉS ET AVANTAGES DIVERS. ALLOCATION TEMPORAIRE D'INVALIDITÉ. - CONDITIONS D'ATTRIBUTION - TAUX D'INCAPACITÉ SUPÉRIEUR À 10 % - POSSIBILITÉ DE TENIR COMPTE D'ACCIDENTS DE SERVICE SUCCESSIFS SUBIS PAR UN FONCTIONNAIRE AYANT CHANGÉ DE FONCTION PUBLIQUE - 1) FONCTIONNAIRE DE L'ETAT DEVENU FONCTIONNAIRE TERRITORIAL - EXISTENCE - 2) MILITAIRE DEVENU FONCTIONNAIRE TERRITORIAL - EXISTENCE, AU REGARD DU PRINCIPE D'ÉGALITÉ.
</SCT>
<ANA ID="9A"> 01-04-03-03-02 Il résulte de l'article L. 4123-2 du code de la défense, applicable en vertu de l'article L. 4111-2 du même code aux militaires servant en vertu d'un contrat, et des articles L. 2 et L. 4 du code des pensions militaires d'invalidité et des victimes de la guerre (CPMIVG) que les militaires, y compris sous contrat, qui, en raison d'un accident de service, ont subi une infirmité entraînant une incapacité égale ou supérieure à 10 %, peuvent bénéficier d'une pension militaire d'invalidité.,,,Il résulte de l'article 65 de la loi n° 84-16 du 11 janvier 1984, du III de l'article 119 de la loi n° 84-53 du 26 janvier 1984, de l'article L. 417-8 du code des communes, des articles 2, 5 et 10 du décret n° 2005-442 du 2 mai 2005, de l'article L. 28 du code des pensions civiles et militaires de retraite (CPCMR) et du décret n° 68-756 du 13 août 1968 que, dans l'hypothèse où un fonctionnaire territorial a subi successivement deux accidents de service qui, pris isolément, se traduisent chacun par un taux d'incapacité inférieur à 10 %, mais qui, cumulés, atteignent ce seuil, ce fonctionnaire peut prétendre à une allocation temporaire d'invalidité tenant compte de l'ensemble de ces infirmités.... ,,Il doit en aller de même, dès lors qu'en conséquence de l'article 119 de la loi du 26 janvier 1984, l'allocation temporaire d'invalidité est allouée dans les mêmes conditions aux fonctionnaires territoriaux et aux fonctionnaires de l'Etat, dans le cas où le fonctionnaire appartenait à la fonction publique de l'Etat à la date du premier accident de service et était devenu fonctionnaire territorial à la date du second accident de service.,,,Ces dispositions doivent recevoir la même interprétation dans le cas où le fonctionnaire territorial avait, à la date du premier accident de service, la qualité de militaire, alors même que les conditions d'indemnisation forfaitaire des séquelles des accidents de service dont sont victimes les militaires et les fonctionnaires civils relèvent de régimes différents, dès lors qu'aucune différence de situation ne justifie, au regard du principe d'égalité, compte tenu de la nature et de l'objet de l'allocation temporaire d'invalidité, que l'incapacité résultant d'un premier accident de service subi en qualité de militaire ne soit pas prise en compte pour le bénéfice d'une allocation temporaire d'invalidité alors qu'elle le serait si cet accident avait été subi en tant que fonctionnaire civil. A cet égard, le décret du 2 mai 2005, dont l'article 14 se borne à traiter le cas de l'agent titulaire déjà bénéficiaire d'une allocation temporaire d'invalidité qui passe d'une fonction publique à une autre, ne saurait être interprété comme excluant la prise en compte, pour l'attribution de cette allocation, de l'incapacité résultant d'un accident de service antérieurement subi par un agent alors qu'il avait la qualité de militaire.</ANA>
<ANA ID="9B"> 36-08-03-01 Il résulte de l'article L. 4123-2 du code de la défense, applicable en vertu de l'article L. 4111-2 du même code aux militaires servant en vertu d'un contrat, et des articles L. 2 et L. 4 du code des pensions militaires d'invalidité et des victimes de la guerre (CPMIVG) que les militaires, y compris sous contrat, qui, en raison d'un accident de service, ont subi une infirmité entraînant une incapacité égale ou supérieure à 10 %, peuvent bénéficier d'une pension militaire d'invalidité.,,,Il résulte de l'article 65 de la loi n° 84-16 du 11 janvier 1984, du III de l'article 119 de la loi n° 84-53 du 26 janvier 1984, de l'article L. 417-8 du code des communes, des articles 2, 5 et 10 du décret n° 2005-442 du 2 mai 2005, de l'article L. 28 du code des pensions civiles et militaires de retraite (CPCMR) et du décret n° 68-756 du 13 août 1968 que, dans l'hypothèse où un fonctionnaire territorial a subi successivement deux accidents de service qui, pris isolément, se traduisent chacun par un taux d'incapacité inférieur à 10 %, mais qui, cumulés, atteignent ce seuil, ce fonctionnaire peut prétendre à une allocation temporaire d'invalidité tenant compte de l'ensemble de ces infirmités.... ,,1) Il doit en aller de même, dès lors qu'en conséquence de l'article 119 de la loi du 26 janvier 1984, l'allocation temporaire d'invalidité est allouée dans les mêmes conditions aux fonctionnaires territoriaux et aux fonctionnaires de l'Etat, dans le cas où le fonctionnaire appartenait à la fonction publique de l'Etat à la date du premier accident de service et était devenu fonctionnaire territorial à la date du second accident de service.,,,2) Ces dispositions doivent recevoir la même interprétation dans le cas où le fonctionnaire territorial avait, à la date du premier accident de service, la qualité de militaire, alors même que les conditions d'indemnisation forfaitaire des séquelles des accidents de service dont sont victimes les militaires et les fonctionnaires civils relèvent de régimes différents, dès lors qu'aucune différence de situation ne justifie, au regard du principe d'égalité, compte tenu de la nature et de l'objet de l'allocation temporaire d'invalidité, que l'incapacité résultant d'un premier accident de service subi en qualité de militaire ne soit pas prise en compte pour le bénéfice d'une allocation temporaire d'invalidité alors qu'elle le serait si cet accident avait été subi en tant que fonctionnaire civil. A cet égard, le décret du 2 mai 2005, dont l'article 14 se borne à traiter le cas de l'agent titulaire déjà bénéficiaire d'une allocation temporaire d'invalidité qui passe d'une fonction publique à une autre, ne saurait être interprété comme excluant la prise en compte, pour l'attribution de cette allocation, de l'incapacité résultant d'un accident de service antérieurement subi par un agent alors qu'il avait la qualité de militaire.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
