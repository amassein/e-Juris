<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025041051</ID>
<ANCIEN_ID>JG_L_2011_12_000000330013</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/04/10/CETATEXT000025041051.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème et 8ème sous-sections réunies, 23/12/2011, 330013, Publié au recueil Lebon</TITRE>
<DATE_DEC>2011-12-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>330013</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème et 8ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP COUTARD, MUNIER-APAIRE</AVOCATS>
<RAPPORTEUR>Mme Emilie Bokdam-Tognetti</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CESSR:2011:330013.20111223</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 23 juillet 2009 au secrétariat du contentieux du Conseil d'Etat, présenté par le MINISTRE DE L'INTERIEUR, DE L'OUTRE-MER ET DES COLLECTIVITES TERRITORIALES ; le ministre demande au Conseil d'Etat d'annuler l'arrêt n° 08NC01146 du 20 mai 2009 par lequel la cour administrative d'appel de Nancy a rejeté l'appel qu'il a interjeté du jugement n° 0600302 du 15 mai 2008 par lequel le tribunal administratif de Besançon a annulé l'arrêté du 27 décembre 2005 du préfet du Doubs ordonnant à la communauté d'agglomération du pays de Montbéliard de reverser la somme de 1 646 838,12 euros, perçue au titre du fonds de compensation pour la taxe sur la valeur ajoutée (FCTVA) en 2001, 2002 et 2003 pour les dépenses de restructuration du stade Bonal ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des collectivités territoriales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Emilie Bokdam-Tognetti, Auditeur,<br/>
<br/>
              - les observations de la SCP Coutard, Munier-Apaire, avocat de la Communauté d'agglomération du pays de Montbéliard, <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Coutard, Munier-Apaire, avocat de la Communauté d'agglomération du pays de Montbéliard, <br/>
<br/>
<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que le préfet du Doubs a, par arrêtés pris en 2001, 2002 et 2003, attribué la somme globale de 1 646 838,12 euros au titre du fonds de compensation pour la taxe sur la valeur ajoutée (FCTVA) à la communauté d'agglomération du pays de Montbéliard à raison des dépenses que celle-ci avait effectuées pour la restructuration du stade Bonal ; que, estimant que les conditions dans lesquelles ce stade était mis à disposition du Football-Club Sochaux-Montbéliard avaient pour effet d'accorder un avantage particulier à ce dernier, le préfet du Doubs a, par un arrêté du 27 décembre 2005, décidé que la communauté d'agglomération devait reverser à l'Etat la somme de 1 646 838,12 euros ; que, par un jugement du 15 mai 2008, le tribunal administratif de Besançon a annulé cet arrêté ; que le MINISTRE DE L'INTERIEUR, DE L'OUTRE-MER ET DES COLLECTIVITES TERRITORIALES se pourvoit en cassation contre l'arrêt du 20 mai 2009 par lequel la cour administrative d'appel de Nancy a rejeté l'appel qu'il a formé contre ce jugement ;<br/>
<br/>
              Sans qu'il soit besoin d'examiner les autres moyens du pourvoi ;<br/>
<br/>
              Considérant, d'une part, qu'une décision administrative accordant un avantage financier crée des droits au profit de son bénéficiaire alors même que l'administration avait l'obligation de refuser cet avantage ; qu'en revanche, n'ont pas cet effet les mesures qui se bornent à procéder à la liquidation de la créance née d'une décision prise antérieurement ; que, sous réserve de dispositions législatives ou réglementaires contraires et hors le cas où il est satisfait à une demande du bénéficiaire, l'administration ne peut retirer une décision individuelle explicite créatrice de droits, si elle est illégale, que dans le délai de quatre mois suivant la prise de cette décision ; que ces règles ne font pas obstacle à la possibilité, pour l'administration, de demander à tout moment le reversement des sommes attribuées par suite d'une erreur dans la procédure de liquidation ou de paiement ou d'un retard dans l'exécution d'une décision de l'ordonnateur ; que, par ailleurs, un acte administratif obtenu par fraude ne crée pas de droits et, par suite, peut être abrogé ou retiré par l'autorité compétente pour le prendre, alors même que le délai de droit commun serait expiré ; <br/>
<br/>
              Considérant, d'autre part, qu'aux termes du premier alinéa de l'article L. 1615-1 du code général des collectivités territoriales : " Les ressources du Fonds de compensation pour la taxe sur la valeur ajoutée des collectivités territoriales comprennent les dotations budgétaires ouvertes chaque année par la loi et destinées à permettre progressivement le remboursement intégral de la taxe sur la valeur ajoutée acquittée par les collectivités territoriales et leurs groupements sur leurs dépenses réelles d'investissement " ; qu'aux termes du premier alinéa de l'article L. 1615-7 de ce code : " Les immobilisations cédées ou mises à disposition au profit d'un tiers ne figurant pas au nombre des collectivités ou établissements bénéficiaires du Fonds de compensation pour la taxe sur la valeur ajoutée ne peuvent donner lieu à une attribution dudit fonds " ; que l'article L. 1615-9 du même code dispose que : " Les modalités de remboursement des attributions du Fonds de compensation pour la taxe sur la valeur ajoutée par les collectivités locales ou les établissements bénéficiaires dudit fonds sont définies par décret en Conseil d'Etat " ; qu'enfin, aux termes de l'article R. 1615-5 du code général des collectivités territoriales, dans sa rédaction alors applicable : " Le remboursement mentionné à l'article L. 1615-9 et résultant des articles L. 1615-7 et L. 1615-8 est opéré dans les conditions suivantes : / 1° Lorsqu'il s'agit d'immeubles cédés ou mis à disposition avant le commencement de la neuvième année qui suit celle de leur acquisition ou de leur achèvement, la collectivité ou l'établissement bénéficiaire reverse une fraction de l'attribution initialement obtenue. Cette fraction est égale au montant de l'attribution initiale diminuée d'un dixième par année civile ou fraction d'année civile écoulée depuis la date à laquelle l'immeuble a été acquis ou achevé ; / 2° Lorsqu'il s'agit de biens mobiliers cédés ou mis à disposition avant la quatrième année qui suit celle de leur acquisition, la diminution est d'un cinquième au lieu d'un dixième par année civile ou fraction d'année civile " ;<br/>
<br/>
              Considérant que la décision par laquelle un préfet accorde à une collectivité territoriale ou à un groupement de collectivités le bénéfice du fonds de compensation pour la taxe sur la valeur ajoutée (FCTVA) au titre de dépenses qu'il juge éligibles pour une année donnée crée au profit de cette collectivité ou de ce groupement un droit au versement de dotations de ce fonds à raison de ces dépenses ; qu'ainsi, si le préfet peut, à tout moment, demander le reversement des sommes versées par suite d'une erreur dans la procédure de liquidation ou de paiement, il ne peut, en principe, sauf cas de fraude, retirer une décision portant attribution du fonds de compensation pour la taxe sur la valeur ajoutée, si elle est illégale, que dans un délai de quatre mois suivant la prise de cette décision ; que, toutefois, dans l'hypothèse où le bien au titre duquel le bénéfice du fonds a été accordé est ensuite cédé ou mis à disposition au profit d'un tiers non bénéficiaire dans des conditions telles que la dépense de la collectivité ou du groupement doit être regardée comme ayant eu principalement pour objet ou pour effet d'avantager ce tiers, la collectivité ou le groupement est tenu de reverser une fraction de l'attribution initialement obtenue, dans les conditions prévues par les dispositions précitées des articles L. 1615-9 et R. 1615-5 du code général des collectivités territoriales ;  <br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, s'il ne se prévalait pas expressément des dispositions des articles L. 1615-9 et R. 1615-5 du code général des collectivités territoriales, le ministre soutenait devant la cour que, dès lors que le stade Bonal avait été mis à la disposition du Football-Club Sochaux-Montbéliard dans des conditions telles que l'investissement de la communauté d'agglomération du pays de Montbéliard devait être regardé comme ayant eu principalement pour effet d'avantager ce club, le préfet du Doubs avait pu légalement lui demander le remboursement des dotations de FCTVA versées au titre des dépenses de restructuration de ce stade ; que la cour a elle-même mentionné, dans ses motifs, la procédure de remboursement prévue par les articles L. 1615-9 et R. 1615-5 du code général des collectivités territoriales ; que, dès lors, contrairement à ce que soutient la communauté d'agglomération du pays de Montbéliard, le moyen tiré de la méconnaissance, par la cour, de la portée des règles de remboursement prévues par l'article L. 1615-9 de ce code, ne saurait être regardé comme nouveau en cassation ; <br/>
<br/>
              Considérant qu'en jugeant que le caractère créateur de droits des décisions attribuant le FCTVA faisait, par principe, obstacle à toute demande de reversement des dotations attribuées après l'expiration d'un délai de quatre mois, alors que les dispositions des articles L. 1615-9 et R. 1615-5 du code général des collectivités territoriales ont prévu une obligation de reversement, dans les hypothèses et les conditions rappelées ci-dessus, la cour a commis d'erreur de droit ; que son arrêt doit, par suite, être annulé ;<br/>
<br/>
              Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nancy du 25 mai 2009 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nancy.<br/>
Article 3 : Les conclusions de la communauté d'agglomération du pays de Montbéliard présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 4 : La présente décision sera notifiée au MINISTRE DE L'INTERIEUR, DE L'OUTRE-MER, DES COLLECTIVITES TERRITORIALES ET DE L'IMMIGRATION et à la communauté d'agglomération du pays de Montbéliard.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">135-01-07-05 COLLECTIVITÉS TERRITORIALES. DISPOSITIONS GÉNÉRALES. DISPOSITIONS FINANCIÈRES. FONDS DE COMPENSATION DE LA TVA. - DÉCISION D'OCTROI À UNE COLLECTIVITÉ DU BÉNÉFICE DU FCTVA - CARACTÈRE DE DÉCISION CRÉATRICE DE DROITS - 1) EXISTENCE [RJ1] - 2) CONSÉQUENCES - CONDITIONS DE RETRAIT - A) PRINCIPE - DÉLAI DE QUATRE MOIS, SAUF FRAUDE [RJ2] - B) EXCEPTION - CAS PARTICULIER DE CESSION OU DE MISE À DISPOSITION AU PROFIT D'UN TIERS NON BÉNÉFICIAIRE DU BIEN AU TITRE DUQUEL LE BÉNÉFICE DU FONDS A ÉTÉ ACCORDÉ - DISPOSITIONS LÉGISLATIVES AUTORISANT LE REVERSEMENT DE L'AVANTAGE AU-DELÀ DU DÉLAI DE QUATRE MOIS (ART. L. 1615-9 DU CGCT) [RJ3] - EXISTENCE.
</SCT>
<ANA ID="9A"> 135-01-07-05 1) La décision par laquelle un préfet accorde à une collectivité territoriale ou à un groupement de collectivités le bénéfice du fonds de compensation pour la taxe sur la valeur ajoutée (FCTVA) au titre de dépenses qu'il juge éligibles pour une année donnée crée au profit de cette collectivité ou de ce groupement un droit au versement de dotations de ce fonds à raison de ces dépenses. 2) a) Le préfet ne peut, en principe, sauf cas de fraude et réserve faite des sommes versées par suite d'une erreur dans la procédure de liquidation ou de paiement, retirer une décision portant attribution du FCTVA, si elle est illégale, que dans un délai de quatre mois suivant la prise de cette décision. b) Toutefois, dans l'hypothèse où le bien au titre duquel le bénéfice du fonds a été accordé est ensuite cédé ou mis à disposition au profit d'un tiers non bénéficiaire dans des conditions telles que la dépense de la collectivité ou du groupement doit être regardée comme ayant eu principalement pour objet ou pour effet d'avantager ce tiers, la collectivité ou le groupement est tenu de reverser une fraction de l'attribution initialement obtenue, dans les conditions prévues par les dispositions des articles L. 1615-9 et R. 1615-5 du code général des collectivités territoriales.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. sur le caractère créateur de droits des décisions administratives octroyant un avantage financier, CE, Section, 6 novembre 2002, Mme Soulier, n° 223041, p. 369.,,[RJ2] Cf. sur le délai de quatre mois ouvert pour le retrait, sauf en cas de fraude, cf. CE, Assemblée, 26 octobre 2001, Ternon, n° 197178, p. 497.,,[RJ3] Cf. sur la dérogation au délai de retrait de quatre mois en cas de dispositions législatives contraires, cf. CE, Assemblée, 26 octobre 2001, Ternon, n° 197178, p. 497.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
