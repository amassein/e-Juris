<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044806187</ID>
<ANCIEN_ID>JG_L_2021_12_000000445128</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/80/61/CETATEXT000044806187.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 30/12/2021, 445128</TITRE>
<DATE_DEC>2021-12-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>445128</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER ; SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>M. Bertrand Mathieu</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie Roussel</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:445128.20211230</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. G... E... a demandé au tribunal administratif de Grenoble d'annuler pour excès de pouvoir la décision n° 405 du 13 juin 2016 par laquelle le directeur des services courrier Colis Isère Pays de Savoie de la société La Poste lui a infligé la sanction disciplinaire de l'exclusion temporaire des fonctions d'une durée de quinze jours dont huit jours avec sursis.   <br/>
<br/>
              Par un jugement n° 1604546 du 6 décembre 2018, le tribunal administratif de Grenoble a rejeté sa demande. <br/>
<br/>
              Par un arrêt n° 19LY00567 du 6 août 2020, la cour administrative d'appel de Lyon a, sur appel de M. E..., annulé ce jugement et la décision du 13 juin 2016. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 6 octobre 2020 et 6 janvier et 6 octobre 2021 au secrétariat du contentieux du Conseil d'Etat, la société La Poste demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de M. E... ;<br/>
<br/>
               3°) de mettre à la charge de M. E... une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - la loi n° 84-16 du 11 janvier 1984 ;<br/>
              - la loi n° 90-568 du 2 juillet 1990 ;<br/>
              - le décret n° 82-447 du 28 mai 1982 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bertrand Mathieu, conseiller d'Etat en service extraordinaire,<br/>
<br/>
              - les conclusions de Mme Sophie Roussel, rapporteure publique,<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Foussard, Froger, avocat de La Poste, et à la SCP Waquet, Farge, Hazan, avocat de M. E....<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1.	Il ressort des pièces soumis aux juges du fond que M. E..., agent de La Poste affecté à Annecy et bénéficiant de décharges de fonctions à raison de ses responsabilités syndicales, a fait l'objet, par décision prise le 13 juin 2016, d'une exclusion temporaire de fonctions d'une durée de quinze jours, dont huit jours avec sursis, fondée sur quatre motifs tirés respectivement de ce qu'il avait pris la parole de façon intempestive et collective sans autorisation pendant les heures de service et en perturbant l'exploitation des centres de tri de Thonon et d'Annemasse, de ce qu'il avait refusé d'obtempérer aux injonctions des directeurs de ces centres, de ce qu'il n'avait pas respecté les consignes de sécurité d'un espace sécurisé et de ce qu'il avait méconnu les règles d'exercice du droit syndical à La Poste. Par un jugement du 6 décembre 2018, le tribunal administratif de Grenoble a rejeté sa demande tendant à l'annulation de cette décision. La société La Poste se pourvoit en cassation contre l'arrêt du 6 août 2020 par lequel la cour administrative d'appel de Lyon a, sur l'appel de M. E..., annulé ce jugement et la sanction prononcée à l'encontre de celui-ci.<br/>
<br/>
              2.	Si les agents publics qui exercent des fonctions syndicales disposent de la liberté d'action et d'expression particulière qu'exigent l'exercice de leur mandat et la défense des intérêts des personnels qu'ils représentent, cette liberté doit être conciliée avec le respect des règles encadrant l'exercice du droit syndical dans la fonction publique et le droit de grève, ainsi que de leurs obligations déontologiques et des contraintes liées à la sécurité et au bon fonctionnement du service. <br/>
<br/>
              3.	Pour juger que les faits reprochés à M. E... ne pouvaient être qualifiés de faute disciplinaire, la cour administrative d'appel s'est bornée à relever que l'agent intervenant à titre syndical dans un établissement où il n'est pas affecté ne peut être regardé comme accomplissant une tâche liée à ses fonctions ni, partant, recevoir d'instruction hiérarchique et que l'intéressé ne pouvait dès lors être sanctionné en raison de la méconnaissance des consignes données par la hiérarchie des centres de tri de Thonon et Annemasse. En statuant ainsi, sans rechercher si les consignes en cause relevaient d'obligations de sécurité et de la nécessité d'assurer le bon fonctionnement du service, dont les directeurs des centres de tri sont responsables, la cour a commis une erreur de droit. <br/>
<br/>
              4.	Il résulte de ce qui précède, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi et sans qu'il y ait lieu de faire droit à la substitution de motifs demandée par M. E..., tirée de l'absence de caractérisation d'un manquement au décret du 28 mai 1982 relatif à l'exercice du droit syndical dans la fonction publique, laquelle requiert en tout état de cause une appréciation des faits qui ne saurait relever du juge de cassation, que la société La Poste est fondée à demander l'annulation de l'arrêt qu'elle attaque. <br/>
<br/>
              5.	Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de La Poste qui n'est pas, dans la présente instance, la partie perdante. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par La Poste au titre des mêmes dispositions. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Lyon du 6 août 2020 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Lyon. <br/>
Article 3 : Les conclusions de M. E... et de La Poste présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à la société La Poste et à M. G... E....<br/>
              Délibéré à l'issue de la séance du 17 décembre 2021 où siégeaient : M. Nicolas Boulouis, président de chambre, présidant ; M. Olivier Japiot, président de chambre ; M. I... K..., Mme N... M..., M. B... H..., Mme A... J..., M. F... L..., M. Jean-Yves Ollier, conseillers d'Etat et M. Bertrand Mathieu, conseiller d'Etat-rapporteur.<br/>
<br/>
              Rendu le 30 décembre 2021.<br/>
<br/>
<br/>
                 Le Président : <br/>
                 Signé : M. Nicolas Boulouis<br/>
 		Le rapporteur : <br/>
      Signé : M. Bertrand Mathieu<br/>
                 La secrétaire :<br/>
                 Signé : Mme C... D...<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-07-09 FONCTIONNAIRES ET AGENTS PUBLICS. - STATUTS, DROITS, OBLIGATIONS ET GARANTIES. - DROIT SYNDICAL. - AGENTS PUBLICS EXERÇANT DES FONCTIONS SYNDICALES - 1) EXIGENCE DE CONCILIATION ENTRE, D'UNE PART, LEUR LIBERTÉ D'ACTION ET D'EXPRESSION, D'AUTRE PART, LE RESPECT DES RÈGLES ENCADRANT L'EXERCICE DU DROIT SYNDICAL, DE LEURS OBLIGATIONS DÉONTOLOGIQUES [RJ1] ET DES CONTRAINTES LIÉES À LA SÉCURITÉ ET AU BON FONCTIONNEMENT DU SERVICE - 2) ILLUSTRATION - AGENT DE LA POSTE INTERVENANT À TITRE SYNDICAL DANS UN ÉTABLISSEMENT OÙ IL N'EST PAS AFFECTÉ.
</SCT>
<ANA ID="9A"> 36-07-09 1) Si les agents publics qui exercent des fonctions syndicales disposent de la liberté d'action et d'expression particulière qu'exigent l'exercice de leur mandat et la défense des intérêts des personnels qu'ils représentent, cette liberté doit être conciliée avec le respect des règles encadrant l'exercice du droit syndical dans la fonction publique et le droit de grève, ainsi que de leurs obligations déontologiques et des contraintes liées à la sécurité et au bon fonctionnement du service.... ...2) Agent de La Poste bénéficiant de décharges de fonctions à raison de ses responsabilités syndicales, ayant fait l'objet d'une sanction, fondée sur quatre motifs tirés respectivement de ce qu'il avait pris la parole de façon intempestive et collective sans autorisation pendant les heures de service et en perturbant l'exploitation de centres de tri autres que celui dans lequel il était affecté, de ce qu'il avait refusé d'obtempérer aux injonctions des directeurs de ces centres, de ce qu'il n'avait pas respecté les consignes de sécurité d'un espace sécurisé et de ce qu'il avait méconnu les règles d'exercice du droit syndical à La Poste.......Commet une erreur de droit la cour qui, pour juger que les faits reprochés à l'intéressé ne pouvaient être qualifiés de faute disciplinaire, se borne à relever que l'agent intervenant à titre syndical dans un établissement où il n'est pas affecté ne peut être regardé comme accomplissant une tâche liée à ses fonctions ni, partant, recevoir d'instruction hiérarchique et que l'intéressé ne pouvait dès lors être sanctionné en raison de la méconnaissance des consignes données par la hiérarchie des centres de tri en question, sans rechercher si les consignes en cause relevaient d'obligations de sécurité et de la nécessité d'assurer le bon fonctionnement du service, dont les directeurs des centres de tri sont responsables.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 27 janvier 2020, Mme Kabèche, n° 426569, T. pp. 798-803.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
