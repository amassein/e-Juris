<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042992816</ID>
<ANCIEN_ID>JG_L_2021_01_000000442985</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/99/28/CETATEXT000042992816.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 1ère chambres réunies, 14/01/2021, 442985</TITRE>
<DATE_DEC>2021-01-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>442985</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI, SUREAU ; SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>M. Edouard Solier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:442985.20210114</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu les procédures suivantes :<br/>
<br/>
              Le médecin-conseil, chef de service de l'échelon local du service médical du Val-d'Oise, a porté plainte contre M. B... A... devant la section des assurances sociales de la chambre disciplinaire de première instance d'Île-de-France de l'ordre des chirurgiens-dentistes. Par une décision du 24 octobre 2018, la section des assurances sociales de la chambre disciplinaire de première instance a infligé à M. A... la sanction de l'interdiction temporaire de délivrer des soins aux assurés sociaux pendant une durée de dix-huit mois dont six mois assortis du sursis.<br/>
<br/>
              Par une décision du 18 juin 2020, la section des assurances sociales du Conseil national de l'ordre des chirurgiens-dentistes a, sur appel de M. A..., ramené à douze mois, dont six mois assortis du sursis, la sanction infligée en première instance.<br/>
<br/>
              1° Sous le numéro 442985, par un pourvoi sommaire et un mémoire complémentaire enregistrés les 18 août 2020 et 16 octobre 2020 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette décision ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge du médecin-conseil, chef de service de l'échelon local du service médical du Val-d'Oise, la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              2° Sous le numéro 445397, par une requête enregistrée le 16 octobre 2020 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'ordonner qu'il soit sursis à exécution de la même décision de la section des assurances sociales du Conseil national de l'ordre des chirurgiens-dentistes ;<br/>
<br/>
              2°) de mettre à la charge de la caisse primaire d'assurance maladie du Val-d'Oise la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - la Constitution ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code de la santé publique ;<br/>
              - le code de la sécurité sociale ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Edouard Solier, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Spinosi, Sureau, avocat de M. A... et à la SCP Foussard, Froger, avocat du médecin-conseil, chef service de l'échelon local du service médical du Val d'Oise ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Le pourvoi par lequel M. A... demande l'annulation de la décision du 18 juin 2020 de la section des assurances sociales du Conseil national de l'ordre des chirurgiens-dentistes lui infligeant la sanction de l'interdiction de donner des soins aux assurés sociaux pendant une durée de douze mois, assortie de sursis pour six mois, et sa requête tendant à ce qu'il soit sursis à l'exécution de cette décision présentent à juger les mêmes questions. Il y a lieu de les joindre pour statuer par une seule décision. <br/>
<br/>
              Sur le pourvoi en cassation : <br/>
<br/>
              En ce qui concerne la question prioritaire de constitutionnalité : <br/>
<br/>
              2. Aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...) ". Il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              3. Aux termes du premier alinéa du IV de l'article L. 315-1 du code de la sécurité sociale, le service du contrôle médical " (...) procède également à l'analyse, sur le plan médical, de l'activité des professionnels de santé dispensant des soins aux bénéficiaires de l'assurance maladie, de l'aide médicale de l'Etat ou de la prise en charge des soins urgents mentionnée à l'article L. 254-1 du code de l'action sociale et des familles, notamment au regard des règles définies par les conventions qui régissent leurs relations avec les organismes d'assurance maladie ou, en ce qui concerne les médecins, du règlement mentionné à l'article L. 162-14-2. La procédure d'analyse de l'activité se déroule dans le respect des droits de la défense selon des conditions définies par décret ". A ce titre, les dispositions des articles R. 315-1-1 et R. 315-1-2 ainsi que les articles D. 315-1 et suivants du code de la sécurité sociale prévoient les conditions dans lesquelles le professionnel de santé est, durant l'analyse de son activité, informé de son déroulement et de ses conclusions et mis à même de présenter toutes observations utiles à sa défense, notamment, s'il le demande, lors d'un entretien avec le service du contrôle médical.<br/>
<br/>
              4. Lorsque, à l'issue de cette analyse d'activité ainsi que de ces éventuels échanges, il est estimé que le professionnel de santé n'a pas respecté les dispositions législatives ou réglementaires régissant la prise en charge des frais médicaux au titre des risques maladie, maternité, invalidité, accidents du travail et maladies professionnelles ou les règles de nature législative, réglementaire ou conventionnelle que les professionnels sont tenus d'appliquer dans leur exercice, l'organisme de sécurité sociale ou le médecin-conseil peuvent engager des poursuites disciplinaires contre le professionnel de santé, notamment, en portant plainte contre lui devant la juridiction du contrôle technique.<br/>
<br/>
              5. Il incombe au service du contrôle médical, lorsqu'il procède à l'analyse de l'activité d'un professionnel de santé en vertu du IV de l'article L. 315-1 du code de la sécurité sociale, de mettre en oeuvre les règles procédurales définies par le pouvoir réglementaire en vue de garantir le respect des droits de la défense, conformément à ce qu'exigent les dispositions de cet article du code de la sécurité sociale. <br/>
<br/>
              6. Si le respect de ces exigences procédurales par le service du contrôle médical pendant la phase d'analyse préalable à la saisine de la juridiction du contrôle technique ne constitue pas une condition de recevabilité de la plainte et si cette phase d'analyse préalable ne constitue pas un élément de la procédure suivie devant la juridiction, de sorte que l'éventuelle irrégularité de cette phase préalable ne saurait par elle-même entacher d'irrégularité la procédure juridictionnelle, le professionnel de santé poursuivi devant la juridiction du contrôle technique peut toujours se prévaloir de circonstances antérieures à l'engagement des poursuites disciplinaires de nature à affecter la régularité de la procédure juridictionnelle suivie ou le bien-fondé de la sanction susceptible d'être infligée. En particulier, il peut utilement faire valoir que, pendant la phase d'analyse préalable, il aurait été porté par avance une atteinte irrémédiable au respect des droits de la défense pendant la procédure juridictionnelle ou que des irrégularités ayant entaché cette phase d'analyse préalable affectent la valeur probante des éléments produits lors de l'instance juridictionnelle ou conduisent à remettre en cause l'existence matérielle ou la qualification des faits dénoncés dans la plainte. <br/>
<br/>
              7. Dans ces conditions, les dispositions du premier alinéa du IV de l'article L. 315-1 du code de la sécurité sociale, telles qu'interprétées par le Conseil d'Etat, ne portent, en tout état de cause, pas atteinte au droit à un recours juridictionnel effectif, aux droits de la défense ou au droit à une procédure juste et équitable, garantis par l'article 16 de la Déclaration des droits de l'homme et du citoyen.<br/>
<br/>
              8. Il s'ensuit que la question prioritaire de constitutionnalité soulevée par M. A..., qui n'est pas nouvelle, ne présente pas un caractère sérieux. Il n'y a, dès lors, pas lieu de la renvoyer au Conseil constitutionnel.<br/>
<br/>
              En ce qui concerne les autres moyens du pourvoi :<br/>
<br/>
              9. Aux termes de l'article L. 822-1 du code de justice administrative: " Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux ".<br/>
<br/>
              10. Pour demander l'annulation de la décision de la section des assurances sociales du Conseil national de l'ordre des chirurgiens-dentistes qu'il attaque, M. A... soutient qu'elle est entachée : <br/>
              - d'erreur de droit en ce qu'elle juge, en méconnaissance des droits et libertés garantis par la Constitution et de l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, que les conditions dans lesquelles a été réalisée l'analyse de son activité par le service du contrôle médical sont sans influence sur la régularité de la procédure disciplinaire, alors qu'il faisait valoir qu'il n'avait pas été informé des noms des patients examinés par le praticien-conseil, ni invité à participer à leur examen, qu'il n'avait pas eu connaissance des critères retenus pour choisir les dossiers ayant fait l'objet d'une analyse approfondie, et qu'il ne disposait pas de toutes les informations détenues par le médecin-conseil lorsqu'il s'était entretenu avec lui ; <br/>
              - de dénaturation des pièces du dossier et d'erreur de droit en ce qu'elle juge qu'il a méconnu la nomenclature générale des actes professionnels en facturant la réalisation d'un inlay-core, alors qu'il ne s'agissait que d'une couronne sur implant, et qu'il a réalisé des actes au-delà des besoins du patient dans le dossier n° 30, alors qu'il a produit des éléments de nature à établir l'état d'usure des dents du patient justifiant la réalisation de ces actes ;<br/>
              - d'erreur de droit en ce qu'elle lui inflige une sanction sans la motiver, alors qu'une telle motivation est requise par les stipulations de l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales.<br/>
<br/>
              11. Il soutient, en outre, que la décision qu'il attaque lui inflige une sanction hors de proportion avec la gravité des fautes retenues.<br/>
<br/>
              12. Aucun des moyens du pourvoi de M. A... n'est de nature à permettre son admission.<br/>
<br/>
              Sur la requête aux fins de sursis à exécution : <br/>
<br/>
              13. Il résulte de ce qui vient d'être dit que le pourvoi formé par M. A... contre la décision de la section des assurances sociales du Conseil national de l'ordre des chirurgiens-dentistes n'est pas admis. Par suite, les conclusions de sa requête tendant à ce qu'il soit sursis à l'exécution de cette décision deviennent sans objet.<br/>
<br/>
              14. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'il soit fait droit aux conclusions présentées par M. A... à ce titre. Dans les circonstances de l'espèce, il y a lieu, au même titre, de mettre à la charge de M. A... le versement au médecin-conseil, chef de service de l'échelon local du service médical du Val-d'Oise, de la somme de 3 000 euros.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par M. A.... <br/>
Article 2 : Le pourvoi de M. A... n'est pas admis.<br/>
Article 3 : Il n'y a pas lieu de statuer sur les conclusions de la requête de M. A... tendant à ce qu'il soit sursis à l'exécution de la décision de la section des assurances sociales du Conseil national des chirurgiens-dentistes du 18 juin 2020.<br/>
Article 4 : M. A... versera la somme de 3 000 euros au médecin-conseil, chef de service de l'échelon local du service médical du Val-d'Oise, au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 5 : Les conclusions de la requête de M. A... présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 6 : La présente décision sera notifiée à M. B... A..., au médecin-conseil, chef de service de l'échelon local du service médical du Val-d'Oise et au ministre des solidarités et de la santé.<br/>
Copie en sera adressée au Conseil constitutionnel, au Premier ministre et au Conseil national de l'ordre des chirurgiens-dentistes.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">37-03-02-01 JURIDICTIONS ADMINISTRATIVES ET JUDICIAIRES. RÈGLES GÉNÉRALES DE PROCÉDURE. INSTRUCTION. CARACTÈRE CONTRADICTOIRE DE LA PROCÉDURE. - JURIDICTIONS DU CONTRÔLE TECHNIQUE - ANALYSE DE L'ACTIVITÉ DES PROFESSIONNELS DE SANTÉ DISPENSANT DES SOINS AUX ASSURÉS SOCIAUX (IV DE L'ART. L. 315-1 DU CSS) - RESPECT DES DROITS DE LA DÉFENSE - INCIDENCE - 1) EN PRINCIPE - A) SUR LA RECEVABILITÉ DE LA PLAINTE - ABSENCE [RJ1] - B) SUR LA RÉGULARITÉ DE LA PROCÉDURE JURIDICTIONNELLE - ABSENCE [RJ2] - 3) A) PAR EXCEPTION - CIRCONSTANCE DE NATURE À AFFECTER LA RÉGULARITÉ DE LA PROCÉDURE JURIDICTIONNELLE OU LE BIEN-FONDÉ DE LA SANCTION - EXISTENCE - B) ILLUSTRATIONS - I) ATTEINTE IRRÉMÉDIABLE AU RESPECT DES DROITS DE LA DÉFENSE [RJ3] - II) AFFECTATION DE LA VALEUR PROBANTE DES ÉLÉMENTS VERSÉS À L'INSTRUCTION [RJ4] - III) REMISE EN CAUSE DES FAITS OU DE LEUR QUALIFICATION [RJ5].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">62-05-03 SÉCURITÉ SOCIALE. CONTENTIEUX ET RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. SECTION DES ASSURANCES SOCIALES DES ORDRES (CONTRÔLE TECHNIQUE, L. 145-1 DU CSS). - ANALYSE DE L'ACTIVITÉ DES PROFESSIONNELS DE SANTÉ DISPENSANT DES SOINS AUX ASSURÉS SOCIAUX (IV DE L'ART. L. 315-1 DU CSS) - RESPECT DES DROITS DE LA DÉFENSE - INCIDENCE - 1) EN PRINCIPE - A) SUR LA RECEVABILITÉ DE LA PLAINTE - ABSENCE [RJ1] - B) SUR LA RÉGULARITÉ DE LA PROCÉDURE JURIDICTIONNELLE - ABSENCE [RJ2] - 3) A) PAR EXCEPTION - CIRCONSTANCE DE NATURE À AFFECTER LA RÉGULARITÉ DE LA PROCÉDURE JURIDICTIONNELLE OU LE BIEN-FONDÉ DE LA SANCTION - EXISTENCE - B) ILLUSTRATIONS - I) ATTEINTE IRRÉMÉDIABLE AU RESPECT DES DROITS DE LA DÉFENSE [RJ3] - II) AFFECTATION DE LA VALEUR PROBANTE DES ÉLÉMENTS VERSÉS À L'INSTRUCTION [RJ4] - III) REMISE EN CAUSE DES FAITS OU DE LEUR QUALIFICATION [RJ5].
</SCT>
<ANA ID="9A"> 37-03-02-01 Il incombe au service du contrôle médical, lorsqu'il procède à l'analyse de l'activité d'un professionnel de santé en vertu du IV de l'article L. 315-1 du code de la sécurité sociale (CSS), de mettre en oeuvre les règles procédurales définies par le pouvoir réglementaire en vue de garantir le respect des droits de la défense, conformément à ce qu'exigent ces dispositions.... ,,1) a) Le respect de ces exigences procédurales par le service du contrôle médical pendant la phase d'analyse préalable à la saisine de la juridiction du contrôle technique ne constitue pas une condition de recevabilité de la plainte.,,,2) Cette phase d'analyse préalable ne constitue pas un élément de la procédure suivie devant la juridiction, de sorte que l'éventuelle irrégularité de cette phase préalable ne saurait par elle-même entacher d'irrégularité la procédure juridictionnelle.,,,3) a) Toutefois, le professionnel de santé poursuivi devant la juridiction du contrôle technique peut toujours se prévaloir de circonstances antérieures à l'engagement des poursuites disciplinaires de nature à affecter la régularité de la procédure juridictionnelle suivie ou le bien-fondé de la sanction susceptible d'être infligée.,,,b) En particulier, il peut utilement faire valoir i) que, pendant la phase d'analyse préalable, il aurait été porté par avance une atteinte irrémédiable au respect des droits de la défense pendant la procédure juridictionnelle ou ii) que des irrégularités ayant entaché cette phase d'analyse préalable affectent la valeur probante des éléments produits lors de l'instance juridictionnelle ou iii) conduisent à remettre en cause l'existence matérielle ou la qualification des faits dénoncés dans la plainte.</ANA>
<ANA ID="9B"> 62-05-03 Il incombe au service du contrôle médical, lorsqu'il procède à l'analyse de l'activité d'un professionnel de santé en vertu du IV de l'article L. 315-1 du code de la sécurité sociale (CSS), de mettre en oeuvre les règles procédurales définies par le pouvoir réglementaire en vue de garantir le respect des droits de la défense, conformément à ce qu'exigent ces dispositions.... ,,1) a) Le respect de ces exigences procédurales par le service du contrôle médical pendant la phase d'analyse préalable à la saisine de la juridiction du contrôle technique ne constitue pas une condition de recevabilité de la plainte.,,,2) Cette phase d'analyse préalable ne constitue pas un élément de la procédure suivie devant la juridiction, de sorte que l'éventuelle irrégularité de cette phase préalable ne saurait par elle-même entacher d'irrégularité la procédure juridictionnelle.,,,3) a) Toutefois, le professionnel de santé poursuivi devant la juridiction du contrôle technique peut toujours se prévaloir de circonstances antérieures à l'engagement des poursuites disciplinaires de nature à affecter la régularité de la procédure juridictionnelle suivie ou le bien-fondé de la sanction susceptible d'être infligée.,,,b) En particulier, il peut utilement faire valoir i) que, pendant la phase d'analyse préalable, il aurait été porté par avance une atteinte irrémédiable au respect des droits de la défense pendant la procédure juridictionnelle ou ii) que des irrégularités ayant entaché cette phase d'analyse préalable affectent la valeur probante des éléments produits lors de l'instance juridictionnelle ou iii) conduisent à remettre en cause l'existence matérielle ou la qualification des faits dénoncés dans la plainte.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 26 octobre 2011,,, n° 329295, T. pp. 748-1166.,,[RJ2] Cf. CE, 27 avril 1967,,, n° 63367, p. 179.,,[RJ3] Rappr., s'agissant des juridictions ordinales, CE, 12 novembre 2020, M.,, n° 428931, à mentionner aux Tables.,,[RJ4] Cf. CE, 12 février 2020, M.,, n° 425566, à mentionner aux Tables.,,[RJ5] Cf. CE, 27 avril 1967,,, n° 63367, p. 179.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
