<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032529615</ID>
<ANCIEN_ID>JG_L_2016_05_000000375501</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/52/96/CETATEXT000032529615.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème - 10ème chambres réunies, 13/05/2016, 375501</TITRE>
<DATE_DEC>2016-05-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>375501</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème - 10ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Bastien Lignereux</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Aladjidi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:375501.20160513</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire et un mémoire complémentaire, enregistrés les 17 février et 16 mai 2014 au secrétariat du contentieux du Conseil d'Etat, la société Direct Energie demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la délibération de la Commission de régulation de l'énergie (CRE) du 12 décembre 2013 portant décision relative aux tarifs d'utilisation d'un réseau public d'électricité dans le domaine de tension HTA ou BT ;<br/>
<br/>
              2°) d'enjoindre à la CRE d'adopter une nouvelle délibération ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat (CRE) la somme de 5 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le traité sur le fonctionnement de l'Union européenne ;<br/>
              - le règlement n° 714/2009 du Parlement européen et du Conseil du 13 juillet 2009 ;<br/>
              - la directive 2009/72/CE du Parlement européen et du Conseil du 13 juillet 2009 ;<br/>
              - le code de l'énergie ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bastien Lignereux, auditeur,<br/>
<br/>
              - les conclusions de M. Frédéric Aladjidi, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat de la société Electricité de France ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier que, par une délibération du 12 décembre 2013, la Commission de régulation de l'énergie (CRE) a défini la méthodologie d'élaboration des tarifs d'utilisation d'un réseau public d'électricité dans le domaine de tension HTA ou BT et a fixé les tarifs applicables à compter du 1er janvier 2014 ; que la société Direct Energie demande l'annulation pour excès de pouvoir de cette délibération ;<br/>
<br/>
              Sur le respect des obligations consultatives prévues par le code de l'énergie :<br/>
<br/>
              2. Considérant qu'aux termes du premier alinéa de l'article L. 341-2 du code de l'énergie, dans sa rédaction en vigueur à la date de la délibération attaquée : " Les tarifs d'utilisation du réseau public de transport et des réseaux publics de distribution sont calculés de manière transparente et non discriminatoire, afin de couvrir l'ensemble des coûts supportés par les gestionnaires de ces réseaux dans la mesure où ces coûts correspondent à ceux d'un gestionnaire de réseau efficace " ; qu'aux termes du quatrième alinéa de l'article L. 341-3 du même code, relatif aux méthodologies utilisées pour établir les tarifs d'utilisation des réseaux publics de transport et de distribution d'électricité, la CRE " procède, selon les modalités qu'elle détermine, à la consultation des acteurs du marché de l'énergie " ; <br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier qu'avant d'adopter la décision attaquée, la CRE a procédé, de juillet 2010 à novembre 2013, à cinq séries de consultations publiques, portant respectivement sur la forme de la grille tarifaire, la structure des tarifs, le cadre de régulation, puis à nouveau l'ensemble des volets tarifaires et, enfin, la méthodologie de calcul des charges de capital de la société ERDF ; que ces consultations ont été effectuées sur la base de documents qui, contrairement à ce que soutient la société requérante, exposaient de façon suffisamment précise les évolutions envisagées ; qu'en particulier, s'agissant des méthodes de calcul du coût du capital de la société ERDF, la CRE, qui a soumis à la consultation deux options reposant sur l'application d'un taux de rémunération à une base d'actifs régulés, n'était pas tenue de proposer une troisième option reposant sur une autre méthode, telle que la couverture des charges comptables constatées de la société ERDF ; qu'elle n'était pas davantage tenue de donner suite à la demande de la société requérante d'organiser, après la dernière consultation, portant sur le calcul des charges de capital des gestionnaires de réseau, une nouvelle consultation publique portant à nouveau sur l'ensemble des volets tarifaires, dès lors qu'elle était suffisamment informée sur les conséquences de ses choix grâce aux consultations antérieures ; qu'enfin, la CRE n'était pas tenue de répondre aux observations formulées lors de ces consultations ; que, par suite, le moyen tiré de ce que la CRE aurait méconnu les obligations consultatives prévues par le code de l'énergie doit être écarté ;<br/>
<br/>
              Sur le calcul des coûts pris en compte pour la détermination des tarifs d'utilisation d'un réseau public d'électricité :<br/>
<br/>
              4. Considérant, en premier lieu, qu'aux termes du 1 de l'article 14 du règlement du Parlement européen et du Conseil du 13 juillet 2009 sur les conditions d'accès au réseau pour les échanges transfrontaliers d'électricité et abrogeant le règlement n° 1228/2003 : " Les redevances d'accès aux réseaux appliquées par les gestionnaires de réseau sont transparentes, tiennent compte de la nécessité de garantir la sécurité des réseaux et reflètent les coûts effectivement engagés dans la mesure où ils correspondent à ceux d'un gestionnaire de réseau efficace et ayant une structure comparable et elles sont appliquées d'une manière non discriminatoire. (...) " ; qu'aux termes de l'article L. 341-2 du code de l'énergie : " Les tarifs d'utilisation du réseau public de transport et des réseaux publics de distribution sont calculés de manière transparente et non discriminatoire, afin de couvrir l'ensemble des coûts supportés par les gestionnaires de ces réseaux dans la mesure où ces coûts correspondent à ceux d'un gestionnaire de réseau efficace. (...) " ; <br/>
<br/>
              5. Considérant qu'il ressort des pièces du dossier que, pour fixer les tarifs d'utilisation des réseaux publics de distribution d'électricité, la CRE a défini les charges de capital comme la somme de la rémunération des actifs en service et du montant des provisions pour renouvellement des immobilisations ainsi que des amortissements relatifs aux immobilisations autres que celles qui ont été réalisées par les concédants avant le 31 décembre 2004, diminuée du montant des actifs financés par les concédants ; que la rémunération des actifs en service est obtenue en multipliant la " base d'actifs régulés ", égale à la valeur nette comptable des immobilisations figurant à l'actif du bilan de la société ERDF, déduction faite de celles qui ont été financées par les concédants avant le 31 décembre 2004, par un taux de rémunération ; que cette méthode de calcul a été substituée, à compter du 1er janvier 2006, à une précédente méthode consistant à évaluer le montant des charges de capital comme la somme des dotations aux amortissements et aux provisions pour renouvellement, des charges liées aux dettes financières et de la rémunération des capitaux propres, déduction faite de la trésorerie ;<br/>
<br/>
              6. Considérant qu'il ressort également des pièces du dossier que, pour l'application de cette méthode, la CRE a pris en compte non seulement les capitaux propres et, le cas échéant, les emprunts financiers figurant au passif de la société ERDF, mais également les " comptes spécifiques des concessions ", qui correspondent aux droits des concédants de récupérer gratuitement les biens de la concession en fin de contrat, ainsi que les provisions pour renouvellement des immobilisations ; qu'elle leur a appliqué des taux de rémunération différents, en appliquant aux capitaux propres régulés, définis comme la différence entre, d'une part, la valeur nette des actifs de réseau, et, d'autre part, les passifs de concession, les provisions pour renouvellement, les subventions d'investissement et, le cas échéant, les emprunts financiers, un taux " sans risque " auquel s'ajoute une " prime de risque ", alors que, pour les autres postes du passif, elle n'a appliqué que la " prime de risque " ; que, contrairement à ce que soutient la société requérante, les dispositions, citées au point 5, de l'article 14 du règlement du Parlement européen et du Conseil du 13 juillet 2009 et de l'article L. 341-2 du code de l'énergie n'excluent pas l'application d'une méthode telle que celle à laquelle a eu recours la CRE, dès lors que celle-ci prend en compte, dans les taux de rémunération qu'elle retient, les comptes spécifiques des concessions et les provisions pour renouvellement des immobilisations ; que ces dispositions n'excluent pas davantage l'intégration, dans la " base d'actifs régulés ", d'actifs détenus par l'autorité concédante et mis à la disposition de la société ERDF, dès lors que cette mise à disposition est rémunérée et implique l'obligation pour le concessionnaire de restituer ces actifs en état normal de fonctionnement au terme de la concession ; que, par suite, le moyen tiré de l'illégalité de la méthode de calcul des charges de capital retenue par la CRE doit être écarté ;<br/>
<br/>
              7. Considérant, en second lieu, que la requérante soutient que la CRE a méconnu l'article L. 341-2 du code de l'énergie cité au point 2 dès lors qu'elle a inclus dans la " base d'actifs régulés ", dont les tarifs contestés couvrent le coût, l'ensemble des investissements proposés par la société ERDF, sans s'assurer qu'ils correspondaient à ceux d'un gestionnaire de réseau efficace, comme l'exige cette disposition ; que, toutefois, la requérante se borne à contester le principe de l'inclusion des investissements projetés par cette société sans alléguer que certains d'entre eux en particulier ne répondraient pas à cette exigence ; qu'il ne ressort pas des pièces du dossier que ces investissements ne seraient manifestement pas conformes à ceux que réaliserait un gestionnaire de réseau efficace ; que, par suite, le moyen ne peut être accueilli ; <br/>
<br/>
              Sur l'existence alléguée d'une aide d'Etat :<br/>
<br/>
              8. Considérant qu'aux termes du 1 de l'article 107 du traité sur le fonctionnement de l'Union européenne : " Sauf dérogations prévues par les traités, sont incompatibles avec le marché intérieur, dans la mesure où elles affectent les échanges entre États membres, les aides accordées par les États ou au moyen de ressources d'État sous quelque forme que ce soit qui faussent ou qui menacent de fausser la concurrence en favorisant certaines entreprises ou certaines productions. " ; que la société Direct Energie soutient que les tarifs d'utilisation d'un réseau public d'électricité constituent une telle aide qui, à défaut d'avoir été notifiée à la Commission européenne en application de l'article 108 du même traité, est illégale ; qu'en vertu de la jurisprudence de la Cour de justice de l'Union européenne, la qualification d'aide au sens de l'article 107 requiert qu'il s'agisse d'une intervention de l'Etat ou au moyen de ressources d'Etat et que cette intervention soit susceptible d'affecter les échanges entre Etats membres, accorde un avantage à son bénéficiaire et fausse ou menace de fausser la concurrence ;<br/>
<br/>
              9. Considérant qu'il résulte toutefois de ce qui a été dit aux points 6 et 7 qu'il ne ressort pas des pièces du dossier que, au regard de leur montant, les tarifs contestés seraient constitutifs d'un avantage accordé aux gestionnaires d'un réseau de distribution d'électricité ; que, dès lors, le moyen tiré de ce que ces tarifs constituent une aide d'Etat non notifiée, en méconnaissance des stipulations de l'article 108 du traité sur le fonctionnement de l'Union européenne, ne peut, en tout état de cause, qu'être écarté ;<br/>
<br/>
              Sur la différenciation temporelle des tarifs :<br/>
<br/>
              10. Considérant qu'aux termes du deuxième alinéa de l'article L. 341-4 du code de l'énergie : " La structure et le niveau des tarifs d'utilisation des réseaux de transport et de distribution d'électricité sont fixés afin d'inciter les clients à limiter leur consommation aux périodes où la consommation de l'ensemble des consommateurs est la plus élevée " ; <br/>
<br/>
              11. Considérant qu'il ressort de la décision attaquée que la CRE a prévu, s'agissant, en premier lieu, des tarifs de soutirage sur le domaine de tension HTA, que les utilisateurs ont le choix entre trois options, dont deux présentent une différenciation temporelle, s'agissant, en deuxième lieu, des tarifs sur le domaine de tension BT supérieure à 36 kVA, que les utilisateurs peuvent choisir entre deux options comportant toutes deux une différenciation temporelle et, s'agissant, enfin, des tarifs sur le domaine de tension BT inférieure ou égale à 36 kVA, que les utilisateurs peuvent choisir entre trois options, dont l'une comporte une différenciation temporelle ; que, dès lors, la CRE n'a ni méconnu l'objectif d'incitation à la limitation de la consommation d'électricité pendant les périodes de pointe fixé par les dispositions de l'article L. 341-4 du code de l'énergie ni omis de prendre en compte les orientations de politique énergétique portées à sa connaissance le 10 octobre 2012 par le ministre chargé de l'énergie, qui se bornaient à inviter la CRE à renforcer la part des options avec différenciation temporelle dans le domaine de tension inférieure ou égale à 36 kVA afin " d'orienter les usagers qui ont déjà fait le choix d'un chauffage électrique vers des tarifs variables au sein de la journée ", auxquelles elle n'était pas tenue de se conformer ; que, si ces options tarifaires retiennent une pointe fixe, et non mobile, c'est-à-dire déterminée la veille pour le lendemain, comme le souhaitait la société Direct Energie lors des consultations préalables, cette seule circonstance ne suffit pas à établir que la délibération attaquée porterait une atteinte illégale à cet objectif ; qu'enfin, contrairement à ce que soutient la société requérante, la suppression, s'agissant des tarifs sur le domaine de tension BT inférieure ou égale à 36 kVA, d'une option sans différenciation temporelle, loin de méconnaître cet objectif, accroît au contraire la part des options comportant une différenciation temporelle dans les différentes options offertes aux utilisateurs ; que, par suite, le moyen tiré de la méconnaissance de l'article L. 341-4 du code de l'énergie doit être écarté ;<br/>
<br/>
              12. Considérant qu'il résulte de tout ce qui précède que la société Direct Energie n'est pas fondée à demander l'annulation de la délibération qu'elle attaque ; que ses conclusions aux fins d'injonction ne peuvent, par suite, qu'être rejetées ; que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante ; qu'il n'y a pas lieu, dans les circonstance de l'espèce, de mettre une somme à ce titre à la charge de la société requérante, dès lors que la CRE se borne à invoquer une " mobilisation des ressources ", sans faire état précisément de frais exposés pour se défendre à l'instance ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
Article 1er : La requête de la société Direct Energie est rejetée.<br/>
Article 2 : Les conclusions présentées par la Commission de régulation de l'énergie au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à la société Direct Energie, à la Commission de régulation de l'énergie et à la ministre de l'environnement, de l'énergie et de la mer.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-03-02-01 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - FORME ET PROCÉDURE. PROCÉDURE CONSULTATIVE. QUESTIONS GÉNÉRALES. - CONSULTATIONS MENÉES PAR LA CRE EN MATIÈRE DE TURPE - OBLIGATION DE LA CRE DE RÉPONDRE AUX OBSERVATIONS - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">29-06-01 ENERGIE. MARCHÉ DE L'ÉNERGIE. COMMISSION DE RÉGULATION DE L'ÉNERGIE. - TURPE - CONSULTATIONS MENÉES PAR LA CRE - OBLIGATION DE LA CRE DE RÉPONDRE AUX OBSERVATIONS - ABSENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">29-06-02-01 ENERGIE. MARCHÉ DE L'ÉNERGIE. TARIFICATION. ELECTRICITÉ. - TURPE - CONSULTATIONS MENÉES PAR LA CRE - OBLIGATION DE LA CRE DE RÉPONDRE AUX OBSERVATIONS - ABSENCE.
</SCT>
<ANA ID="9A"> 01-03-02-01 La Commission de régulation de l'énergie (CRE) n'est pas tenue de répondre aux observations formulées lors des consultations auxquelles elle procède, en vertu des articles L. 341-2 et L. 341-3 du code de l'énergie, en matière de tarifs d'utilisation des réseaux publics de transport et de distribution d'électricité (TURPE).</ANA>
<ANA ID="9B"> 29-06-01 La Commission de régulation de l'énergie (CRE) n'est pas tenue de répondre aux observations formulées lors des consultations auxquelles elle procède, en vertu des articles L. 341-2 et L. 341-3 du code de l'énergie, en matière de tarifs d'utilisation des réseaux publics de transport et de distribution d'électricité (TURPE).</ANA>
<ANA ID="9C"> 29-06-02-01 La Commission de régulation de l'énergie (CRE) n'est pas tenue de répondre aux observations formulées lors des consultations auxquelles elle procède, en vertu des articles L. 341-2 et L. 341-3 du code de l'énergie, en matière de tarifs d'utilisation des réseaux publics de transport et de distribution d'électricité (TURPE).</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
