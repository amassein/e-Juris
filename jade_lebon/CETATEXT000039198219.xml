<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039198219</ID>
<ANCIEN_ID>JG_L_2019_10_000000422712</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/19/82/CETATEXT000039198219.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 09/10/2019, 422712</TITRE>
<DATE_DEC>2019-10-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>422712</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>BALAT ; SCP OHL, VEXLIARD</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT>Mme Mireille Le Corre</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:422712.20191009</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Efficience a demandé au tribunal administratif de Strasbourg de condamner la commune de Lorry-Mardigny à lui verser la somme de 80 132 euros, avec intérêts à compter du 15 décembre 2014, en rétribution de missions accomplies en application d'une convention du 6 novembre 2006 lui confiant une mission d'opérateur du projet d'aménagement d'ensemble d'un secteur d'urbanisation de cette commune. Par un jugement n° 1406939 du 25 janvier 2017, le tribunal administratif de Strasbourg a rejeté cette demande.<br/>
<br/>
              Par un arrêt n° 17NC00725 du 29 mai 2018, la cour administrative d'appel de Nancy a rejeté l'appel formé par la société Efficience contre ce jugement.  <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 30 juillet et 30 octobre 2018 au secrétariat du contentieux du Conseil d'Etat, la société Efficience demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Lorry-Mardigny la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean Sirinelli, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Mireille Le Corre, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Balat, avocat de la société Efficience et à la SCP Ohl, Vexliard, avocat de la commune de Lorry-Mardigny ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la commune de Lorry-Mardigny a confié à la société Efficience, par une convention du 6 novembre 2006, une mission d'opérateur du projet d'aménagement d'ensemble d'un secteur à urbaniser de la commune dénommé " Le Colombier ". Après avoir refusé de signer les actes d'engagement proposés par la société Efficience correspondant aux prestations prévues par cette convention, la commune de Lorry-Mardigny a confié à cette société, par un acte d'engagement du 9 décembre 2008, une partie des prestations initialement prévues par la convention du 6 novembre 2006 puis lui a réglé les prestations correspondantes. Par une décision du 10 janvier 2011, la commune a rejeté la demande de paiement de la somme de 80 132 euros réclamée par la société Efficience au titre des prestations qu'elle aurait exécutées dans le cadre de la convention du 6 novembre 2006. Par un jugement du 25 janvier 2017, le tribunal administratif de Strasbourg a rejeté la demande de la société Efficience tendant à la condamnation de la commune à lui verser cette somme. Par l'arrêt attaqué du 29 mai 2018, la cour administrative d'appel de Nancy a rejeté l'appel formé par la société Efficience contre ce jugement.<br/>
<br/>
              2. En premier lieu, aux termes de l'article R. 611-11-1 du code de justice administrative : " Lorsque l'affaire est en état d'être jugée, les parties peuvent être informées de la date ou de la période à laquelle il est envisagé de l'appeler à l'audience. Cette information précise alors la date à partir de laquelle l'instruction pourra être close dans les conditions prévues par le dernier alinéa de l'article R. 613-1 et le dernier alinéa de l'article R. 613-2. (...) ". Aux termes du dernier alinéa de l'article R. 613-1 de ce code : " (...) lorsque la date prévue par l'article R. 611-11-1 est échue, l'instruction peut être close à la date d'émission de l'ordonnance prévue au premier alinéa ". Aux termes du dernier alinéa de l'article R. 613-2 de ce code : " (...) lorsque la date prévue par l'article R. 611-11-1 est échue, l'instruction peut être close à la date d'émission de l'avis d'audience. Cet avis le mentionne ". Il résulte de ces dispositions que lorsque l'ordonnance ou l'avis d'audience portant clôture de l'instruction est notifié aux parties au moyen de l'application informatique mentionnée à l'article R. 414-1 du code de justice administrative, dénommée Télérecours, l'instruction est, sauf mention contraire d'un horaire ou d'une date ultérieur, close à l'heure de l'envoi de l'ordonnance ou de l'avis par cette application.<br/>
<br/>
              3. Il ressort des pièces du dossier d'appel qu'en application des dispositions précitées de l'article R. 613-2 du code de justice administrative, un avis d'audience portant clôture immédiate de l'instruction a été envoyé aux parties, au moyen de l'application Télérecours, le 19 avril 2018 à 17h29. Par suite, en jugeant que le mémoire de la société Efficience transmis le même jour à 18h43 au greffe de la cour, par l'intermédiaire de la même application, avait été présenté postérieurement à la clôture de l'instruction, la cour administrative d'appel de Nancy n'a pas commis d'erreur de droit.<br/>
<br/>
              4. En second lieu, il ressort des énonciations de l'arrêt attaqué que la cour administrative d'appel de Nancy a relevé que la convention du 6 novembre 2006 ne contenait aucune clause financière ni précision sur les modalités de rémunération de la société Efficience. Puis elle a estimé, par une appréciation souveraine exempte de dénaturation, que cette société n'établissait pas que les sommes perçues de la part de la commune, en rétribution des missions énumérées dans l'acte d'engagement du 9 décembre 2008, seraient insuffisantes au regard des prestations qu'elle a réellement exécutées. Par suite, la cour n'a pas entaché son arrêt d'erreur de qualification juridique ni d'insuffisance de motivation en estimant, dans ces conditions, que la commune n'avait commis aucune faute en refusant de procéder au règlement à la société Efficience de la somme de 80 132 euros au titre de prestations autres que celles prévues par l'acte d'engagement du 9 décembre 2008.<br/>
<br/>
              5. Il résulte de tout ce qui précède que la société Efficience n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque.<br/>
<br/>
              6. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge la commune de Lorry-Mardigny qui n'est pas, dans la présente instance, la partie perdante. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société Efficience la somme de 3 000 euros à verser à la commune de Lorry-Mardigny au titre des mêmes dispositions.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la société Efficience est rejeté.<br/>
Article 2 : La société Efficience versera à la commune de Lorry-Mardigny une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la société Efficience et à la commune de Lorry-Mardigny.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-04-01-05 PROCÉDURE. INSTRUCTION. POUVOIRS GÉNÉRAUX D'INSTRUCTION DU JUGE. CLÔTURE DE L'INSTRUCTION. - ORDONNANCE OU AVIS D'AUDIENCE PORTANT CLÔTURE DE L'INSTRUCTION NOTIFIÉE VIA TÉLÉRECOURS, LA DATE DE CLÔTURE PRÉVUE ÉTANT ÉCHUE - INSTRUCTION CLOSE À L'HEURE DE L'ENVOI DE CETTE ORDONNANCE OU DE CET AVIS PAR TÉLÉRECOURS, SAUF MENTION D'UN HORAIRE OU D'UNE DATE ULTÉRIEURS.
</SCT>
<ANA ID="9A"> 54-04-01-05 Il résulte de l'article R. 611-11-1, du dernier alinéa de l'article R. 613-1 et du dernier alinéa de l'article R. 613-2 du code de justice administrative (CJA) que lorsque l'ordonnance ou l'avis d'audience portant clôture de l'instruction est notifié aux parties au moyen de l'application informatique mentionnée à l'article R. 414-1 du CJA, dénommée Télérecours, l'instruction est, sauf mention contraire d'un horaire ou d'une date ultérieurs, close à l'heure de l'envoi de l'ordonnance ou de l'avis par cette application.,,,Avis d'audience portant clôture immédiate de l'instruction envoyé aux parties, au moyen de l'application Télérecours, le 19 avril 2018 à 17h29. Par suite, en jugeant que le mémoire de la société requérante transmis le même jour à 18h43 au greffe de la cour, par l'intermédiaire de la même application, avait été présenté postérieurement à la clôture de l'instruction, la cour administrative d'appel n'a pas commis d'erreur de droit.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
