<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030863777</ID>
<ANCIEN_ID>JG_L_2015_07_000000388767</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/86/37/CETATEXT000030863777.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème / 7ème SSR, 09/07/2015, 388767</TITRE>
<DATE_DEC>2015-07-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>388767</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème / 7ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Bénédicte Vassallo-Pasquet</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Béatrice Bourgeois-Machureau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:388767.20150709</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La Commission nationale des comptes de campagne et des financements politiques a saisi, en application de l'article L. 52-15 du code électoral, le tribunal administratif de Cergy-Pontoise, sur le fondement de sa décision du 26 novembre 2014 par laquelle elle a rejeté le compte de campagne de M. C...D..., candidat tête de la liste " Sceaux 2020 " aux élections municipales et communautaires qui se sont déroulées les 23 et 30 mars 2014 dans la commune de Sceaux (Hauts-de-Seine).<br/>
<br/>
              Par un jugement n° 1411435 du 19 février 2015, le tribunal administratif de Cergy-Pontoise a jugé que le compte de campagne de M. D...avait été rejeté à bon droit et qu'il n'avait en conséquence pas droit au remboursement par l'Etat de ses dépenses de campagne. Le jugement a en outre déclaré l'intéressé inéligible pour une durée de deux ans, prononcé sa démission d'office de son mandat de conseiller municipal et proclamé élue la suivante de liste, Mme A...B....<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 17 mars et 5 juin 2015 au secrétariat du contentieux du Conseil d'Etat, M. D...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement ; <br/>
<br/>
              2°) de rejeter la saisine de la Commission nationale des comptes de campagne et des financements politiques ;<br/>
<br/>
              3°) d'annuler la décision du 26 novembre 2014 par laquelle la Commission nationale des comptes de campagne et des financements politiques a rejeté son compte de campagne et de juger qu'il a droit au remboursement de l'Etat dans les conditions prévues à l'article L. 52-15 du code électoral ;<br/>
<br/>
              4°) de mettre à la charge de l'Etat le versement de la somme de 10 000 euros sur le fondement de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code électoral ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Bénédicte Vassallo-Pasquet, maître des requêtes en service extraordinaire,<br/>
<br/>
              - les conclusions de Mme Béatrice Bourgeois-Machureau, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant que, par une décision du 26 novembre 2014, la Commission nationale des comptes de campagne et des financements politiques a rejeté le compte de campagne de M.D..., candidat tête de liste aux élections municipales et communautaires qui se sont déroulées les 23 et 30 mars 2014 à Sceaux (Hauts-de-Seine), au motif qu'il avait réglé directement des dépenses engagées en vue de son élection pour un montant de 5 098 euros ; que, saisi par la Commission en application des dispositions de l'article L. 52-15 du code électoral, le tribunal administratif de Cergy-Pontoise, par jugement du 19 février 2015, a jugé que le compte de campagne avait été rejeté à bon droit, a refusé à M. D...le remboursement par l'Etat de ses dépenses électorales, l'a déclaré inéligible pour une durée de deux ans, a prononcé sa démission d'office de son mandat de conseiller municipal et a proclamé élue la suivante de liste ; que M. D...relève appel de ce jugement ;<br/>
<br/>
              Sur la saisine de la Commission nationale des comptes de campagne et des financements politiques :<br/>
<br/>
              2.	Considérant qu'aux termes du 2ème alinéa de l'article L. 52-12 du code électoral : " Au plus tard avant 18 heures le dixième vendredi suivant le premier tour de scrutin, chaque candidat ou candidat tête de liste présent au premier tour dépose à la Commission nationale des comptes de campagne et des financements politiques son compte de campagne et ses annexes accompagné des justificatifs de ses recettes ainsi que des factures, devis et autres documents de nature à établir le montant des dépenses payées ou engagées par le candidat ou pour son compte. Le compte de campagne est présenté par un membre de l'ordre des experts-comptables et des comptables agréés ; celui-ci met le compte de campagne en état d'examen et s'assure de la présence des pièces justificatives requises (...) " ; qu'aux termes de l'article L. 52-15 du même code : " La Commission nationale des comptes de campagne et des financements politiques approuve et, après procédure contradictoire, rejette ou réforme les comptes de campagne. (...) Hors le cas prévu à l'article L. 118-2, elle se prononce dans les six mois du dépôt des comptes. Passé ce délai, les comptes sont réputés approuvés. / Lorsque la commission a constaté que le compte de campagne n'a pas été déposé dans le délai prescrit, si le compte a été rejeté ou si, le cas échéant après réformation, il fait apparaître un dépassement du plafond des dépenses électorales, la commission saisit le juge de l'élection (...) " ; que, dans ces dernières hypothèses, il appartient à la Commission nationale des comptes de campagne et des financements politiques de saisir le juge de l'élection dans le délai de six mois fixé par les dispositions précitées de l'article L. 52-15, lequel présente le caractère d'un délai franc et court à compter de la date de dépôt du compte lorsque celui-ci est effectué dans le délai prescrit ou à compter de la date d'expiration de ce délai, lorsque cette formalité n'est pas remplie ;<br/>
<br/>
              3.	Considérant qu'il résulte de l'instruction que M. D...a déposé son compte de campagne à la Commission le 28 mai 2014, soit dans le délai qui lui était imparti ; que le délai franc de six mois dont disposait la Commission nationale des comptes de campagne et des financements politiques pour saisir le juge de l'élection expirait ainsi le 29 novembre 2014 ; que le 29 novembre 2014 étant un samedi, ce délai a été prolongé jusqu'au lundi 1er décembre 2014 ; que, par suite, M. D...n'est pas fondé à soutenir que la saisine de la Commission, enregistrée au greffe du tribunal administratif de Cergy-Pontoise le 1er décembre 2014, aurait été tardive ;<br/>
<br/>
              Sur le rejet du compte de campagne : <br/>
<br/>
              4.	Considérant qu'aux termes de l'article L. 52-4 du code électoral : " Tout candidat à une élection déclare un mandataire conformément aux articles L. 52-5 et L. 52-6 au plus tard à la date à laquelle sa candidature est enregistrée. Ce mandataire peut être une association de financement électoral, ou une personne physique dénommée "le mandataire financier". Un même mandataire ne peut être commun à plusieurs candidats. / Le mandataire recueille, pendant l'année précédant le premier jour du mois de l'élection et jusqu'à la date du dépôt du compte de campagne du candidat, les fonds destinés au financement de la campagne. / Il règle les dépenses engagées en vue de l'élection et antérieures à la date du tour de scrutin où elle a été acquise, à l'exception des dépenses prises en charge par un parti ou groupement politique. Les dépenses antérieures à sa désignation payées directement par le candidat ou à son profit (...) font l'objet d'un remboursement par le mandataire et figurent dans son compte bancaire ou postal (...) " ;<br/>
<br/>
              5.	Considérant que si, par dérogation à la formalité substantielle que constitue l'obligation de recourir à un mandataire pour toute dépense effectuée en vue de la campagne, le règlement direct de menues dépenses par le candidat peut être admis, ce n'est qu'à la double condition que leur montant soit faible par rapport au total des dépenses du compte de campagne et négligeable au regard du plafond de dépenses autorisées fixé par l'article L. 52-11 du code électoral ; <br/>
<br/>
              6.	Considérant qu'il résulte de l'instruction que M. D...a directement réglé certaines dépenses de sa campagne au moyen de la carte de crédit, établie à son nom, de la société par actions simplifiée DML consulting dont il est le seul actionnaire ; que ces dépenses doivent ainsi être regardées comme ayant été directement réglées par le candidat ; qu'elles ont été refacturées et remboursées par l'association de financement qu'il a désignée comme mandataire, laquelle a été déclarée le 21 février 2014 à la sous-préfecture d'Antony avant que sa candidature ne soit enregistrée ; que les dépenses relevées par la Commission nationale des comptes de campagne et des financements politiques ont été réglées directement par le candidat avant la constitution de l'association de financement, à l'exception seulement de quatre factures  dont les montants sont de 11,99 euros pour un site internet le 24 mars 2014, de 123 et 152,02 euros pour des frais d'essence les 7 et 26 mars 2014 et de 744,08 euros pour des frais d'expédition le 25 février 2014 ; que ces quatre factures représentent un montant total de dépenses réglées directement par le candidat après le 21 février 2014 de 1 031,29 euros ; que ce montant ne représente que 6,22 % des dépenses électorales et 2,56 % du plafond des dépenses autorisées ; qu'un tel montant doit être regardé comme faible par rapport au total des dépenses du compte de campagne du candidat et négligeable au regard du plafond des dépenses autorisées ; que, dans ces conditions, M. D...est fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Cergy-Pontoise, qui n'a pas retenu d'autre irrégularité, a jugé que son compte de campagne avait été rejeté à bon droit ; <br/>
<br/>
              Sur le remboursement des dépenses électorales dû par l'Etat :<br/>
<br/>
              7.	Considérant qu'aux termes de l'article L. 52-11-1 du code électoral : " Les dépenses électorales des candidats aux élections auxquelles l'article L. 52-4 est applicable font l'objet d'un remboursement forfaitaire de la part de l'Etat égal à 47,5 % de leur plafond de dépenses. Ce remboursement ne peut excéder le montant des dépenses réglées sur l'apport personnel des candidats et retracées dans leur compte de campagne (...) / Dans les cas où les irrégularités commises ne conduisent pas au rejet du compte, la décision concernant ce dernier peut réduire le montant du remboursement forfaitaire en fonction du nombre et de la gravité de ces irrégularités " ; qu'il en résulte que les candidats ayant obtenu au moins 5 % des suffrages exprimés au premier tour de scrutin ont droit à un remboursement forfaitaire de la part de l'Etat égal à 47,5 % de leur plafond de dépenses, sans que ce remboursement ne puisse excéder le montant des dépenses réglées sur leur apport personnel et retracées dans leur compte de campagne ; que le caractère irrégulier d'une dépense exposée en méconnaissance de l'article L. 52-4 du code électoral fait obstacle à ce qu'elle puisse faire l'objet d'un remboursement par l'Etat ;<br/>
<br/>
              8.	Considérant qu'il résulte de l'instruction que M.D..., dont le compte de campagne n'a pas été rejeté à bon droit, a obtenu plus de 5 % des suffrages exprimés au premier tour de scrutin ; qu'il a droit, en application de l'article L. 52-11-1 du code électoral, à un remboursement forfaitaire égal à 47,5 % du plafond légal des dépenses fixé pour le scrutin considéré à 40 214 euros, c'est-à-dire 19 101,65 euros, sans que ce remboursement ne puisse excéder le montant des dépenses réglées sur son apport personnel et retracées dans le compte de campagne ; que les dépenses de M. D...réglées sur son apport personnel et retracées au compte de campagne se sont élevées à 10 984 euros ; qu'il y a lieu de retrancher de cette somme les dépenses irrégulièrement réglées en méconnaissance de l'article L. 52-4, soit 1 031,29 euros ; qu'ainsi, c'est à la somme de 9 952,71 euros que doit être fixé le montant du remboursement forfaitaire auquel a droit M. D...; <br/>
<br/>
              Sur l'inéligibilité :<br/>
<br/>
              9.	Considérant qu'en vertu de l'article L. 118-3 du code électoral, le juge de l'élection " prononce également l'inéligibilité du candidat (...) dont le compte de campagne a été rejeté à bon droit en cas de volonté de fraude ou de manquement d'une particulière gravité aux règles relatives au financement des campagnes électorales. / L'inéligibilité prévue aux trois premiers alinéas du présent article est prononcée pour une durée maximale de trois ans et s'applique à toutes les élections " ; <br/>
<br/>
              10.	Considérant qu'il résulte de ce qui a été dit précédemment que le compte de campagne de M. D...n'a pas été rejeté à bon droit ; que ce dernier est, par suite, fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Cergy-Pontoise l'a déclaré inéligible pour deux ans, l'a déclaré démissionnaire d'office de son mandat de conseiller municipal et a proclamé Mme B...élue en qualité de conseiller municipal ; <br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              11.	Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme que M. D...demande au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
                --------------<br/>
<br/>
Article 1er : Le jugement du tribunal administratif de Cergy-Pontoise est annulé.<br/>
<br/>
Article 2 : Le montant du remboursement dû par l'Etat à M. D...en application de l'article L. 52-11-1 du code électoral est fixé à 9 952,71 euros.<br/>
<br/>
Article 3 : Il n'y a pas lieu de déclarer M. D...inéligible.<br/>
Article 4 : Les conclusions présentées par M. D...au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 5 : La présente décision sera notifiée à M. C...D..., à la Commission nationale des comptes de campagne et des financements politiques, à Mme A...B...et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">28-005-04-03-01 ÉLECTIONS ET RÉFÉRENDUM. DISPOSITIONS GÉNÉRALES APPLICABLES AUX ÉLECTIONS. FINANCEMENT ET PLAFONNEMENT DES DÉPENSES ÉLECTORALES. COMMISSION NATIONALE DES COMPTES DE CAMPAGNE ET DES FINANCEMENTS POLITIQUES (CNCCFP). PROCÉDURE DEVANT LA COMMISSION. - DÉLAI DE SIX MOIS AVANT APPROBATION IMPLICITE DU COMPTE DU CANDIDAT (ART. L. 52-15 DU CODE ÉLECTORAL) - 1) DÉLAI DE SAISINE DU JUGE ÉLECTORAL - 2) MODALITÉS DE DÉCOMPTE DU DÉLAI.
</SCT>
<ANA ID="9A"> 28-005-04-03-01 1) En vertu de l'article L. 52-15 du code électoral, la Commission nationale des comptes de campagne et des financements politiques (CNCCFP) doit se prononcer sur le compte du candidat et, le cas échéant, saisir le juge de l'élection, dans un délai de six mois à compter de son dépôt. Passé ce délai, les comptes sont réputés approuvés.... ,,2) Ce délai présente le caractère d'un délai franc et court à compter de la date de dépôt du compte lorsque celui-ci est effectué dans le délai prescrit ou à compter de la date d'expiration de ce délai, lorsque cette formalité n'est pas remplie.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
