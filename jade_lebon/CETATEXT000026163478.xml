<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026163478</ID>
<ANCIEN_ID>JG_L_2012_07_000000359478</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/16/34/CETATEXT000026163478.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème et 3ème sous-sections réunies, 09/07/2012, 359478</TITRE>
<DATE_DEC>2012-07-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>359478</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème et 3ème sous-sections réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Hervé Cassagnabère</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:359478.20120709</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu l'ordonnance n°1201323 du 10 mai 2012, enregistrée le 16 mai 2012 au secrétariat du contentieux du Conseil d'État, par laquelle le président de la troisième chambre du tribunal administratif d'Orléans, avant qu'il soit statué sur la demande de la société par actions simplifiées (SAS) Bineau Agri Service tendant au dégrèvement partiel de la cotisation sur la valeur ajoutée des entreprises à laquelle elle a été assujettie au titre de l'année 2010, a décidé, par application des dispositions de l'article 23-2 de l'ordonnance n° 58-1067 du 7 novembre 1958, de transmettre au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution du 1 du I de l'article 1586 quinquies du code général des impôts dans sa rédaction issue de la loi n° 2009-1673 du 30 décembre 2009 de finances pour 2010 ; <br/>
<br/>
              Vu le mémoire, enregistré le 16 avril 2012 au greffe du tribunal administratif d'Orléans, présenté, en application de l'article 23-1 de l'ordonnance n° 58-1067 du 7 novembre 1958, pour la SAS Bineau Agri Service, dont le siège social est situé 7 rue Pasteur à Fleury-les-Aubrais (45400) ; <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution, notamment son article 61-1 ;<br/>
<br/>
              Vu l'ordonnance n° 58-1067 du 7 novembre 1958 ; <br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ; <br/>
<br/>
              Vu la loi n° 2009-1673 du 30 décembre 2009 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Hervé Cassagnabère, Maître des Requêtes, <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant qu'en vertu de l'article 23-2 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel, une juridiction relevant du Conseil d'Etat, décidant de transmettre une question prioritaire de constitutionnalité, statue sans délai par une décision insusceptible de recours qu'elle adresse au Conseil d'Etat dans les huit jours de son prononcé avec les mémoires ou les conclusions des parties ; que, pour l'application de ces dispositions, alors même que la juridiction décidant de procéder à cette transmission ne serait pas territorialement compétente pour connaître du litige à l'occasion duquel cette question est posée, il appartient au Conseil d'Etat de se prononcer, dans le délai de trois mois prescrit par l'article 23-5 de cette ordonnance, sur son renvoi au Conseil constitutionnel ; <br/>
<br/>
              Considérant que, par suite, alors même que le ministre a soutenu, avant le prononcé par l'ordonnance transmettant au Conseil d'Etat la question posée par la SAS Bineau Agri Services devant le tribunal administratif d'Orléans, que ce dernier était territorialement incompétent pour connaître du contentieux de l'assiette de la cotisation sur la valeur ajoutée des entreprises à laquelle cette société a été assujettie, et quand bien même cette position serait fondée, il y a lieu pour le Conseil d'Etat de se prononcer sur le renvoi au Conseil constitutionnel de la question prioritaire de constitutionnalité qui lui a été transmise par cette juridiction ; <br/>
<br/>
              Considérant qu'il résulte des dispositions de l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que, lorsqu'une juridiction relevant du Conseil d'Etat a transmis à ce dernier, en application de l'article 23-2 de cette même ordonnance, la question de la conformité à la Constitution d'une disposition législative, le Conseil constitutionnel est saisi de cette question de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et qu'elle soit nouvelle ou présente un caractère sérieux ; <br/>
<br/>
              Considérant que l'article 1586 quinquies du code général des impôts dans sa rédaction issue de la loi du 30 décembre 2009 portant loi de finances pour 2010 dispose : " I. 1. Sous réserve des 2, 3 et 4, la cotisation sur la valeur ajoutée des entreprises est déterminée en fonction du chiffre d'affaires réalisé et de la valeur ajoutée produite au cours de l'année au titre de laquelle l'imposition est établie ou au cours du dernier exercice de douze mois clos au cours de cette même année lorsque cet exercice ne coïncide pas avec l'année civile. (...) " ; que ces dispositions sont entrées en vigueur à compter du 1er janvier 2010 en vertu du 3° du II de l'article 1er de la même loi de finances, concomitamment à la suppression de la taxe professionnelle ;   <br/>
<br/>
              Considérant que la SAS Bineau Agri Services, qui clôture son exercice social le 30 juin de chaque année, soutient, à l'appui de sa demande de dégrèvement de cotisation sur la valeur ajoutée des entreprises au titre de l'année 2010 présentée devant le tribunal administratif d'Orléans, que l'article 1586 quinquies du code général des impôts, d'une part, méconnaît les principes d'égalité devant la loi fiscale et devant les charges publiques en traitant différemment les sociétés selon la date de clôture de leur exercice social et, d'autre part, contrevient, par son caractère rétroactif, aux principes de non-rétroactivité de la loi et de sécurité juridique, contraire à l'article 16 de la Déclaration des droits de l'homme et du citoyen ; <br/>
<br/>
              Considérant, en premier lieu, que la situation des redevables s'apprécie au regard de chaque imposition prise isolément ; qu'ainsi, la SAS Bineau Agri Services ne peut utilement soutenir que la période de référence retenue dans la disposition contestée instituerait en 2010, en violation des principes d'égalité devant la loi fiscale et devant les charges publiques, une différence de traitement entre redevables au motif qu'au titre de l'année 2009, les entreprises dont l'exercice coïncide avec l'année civile auraient seulement eu à s'acquitter de la taxe professionnelle, alors que celles qui clôturent leur exercice en cours d'année auraient dû s'acquitter, outre cette taxe, d'une quote-part de la cotisation sur la valeur ajoutée des entreprises en 2010 calculée sur le chiffre d'affaires et la valeur ajoutée produits au cours de ceux des mois de 2009 compris dans les douze mois précédant la date de clôture de l'exercice courant 2010 ; <br/>
<br/>
              Considérant, en second lieu, qu'en adoptant l'article 1586 quinquies du code général des impôts, le législateur n'a disposé que pour l'avenir ; qu'il lui était loisible, sans porter atteinte à des situations juridiquement acquises, de déterminer une assiette établie sur la base de données économiques afférentes à une période antérieure au fait générateur de l'imposition et à la promulgation de la loi ; qu'ainsi, la société ne peut soutenir que les dispositions contestées seraient rétroactives ni, en tout état de cause, qu'elles contreviendraient au principe de sécurité juridique dont elle se prévaut ; <br/>
<br/>
              Considérant qu'il résulte de ce qui précède que la question prioritaire de constitutionnalité soulevée par la SAS Bineau Agri Services, qui n'est pas nouvelle, ne présente pas un caractère sérieux ; que, par suite, il n'y a pas lieu de la renvoyer au Conseil constitutionnel ;<br/>
<br/>
<br/>
<br/>		D E C I D E :<br/>
              		--------------<br/>
<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité transmise par le tribunal administratif d'Orléans. <br/>
<br/>
Article 2 : La présente décision sera notifiée à la SAS Bineau Agri Service et au ministre de l'économie et des finances. <br/>
Copie en sera adressée au Conseil constitutionnel, au Premier ministre et au tribunal administratif d'Orléans. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-10-05 PROCÉDURE. - QUESTION TRANSMISE PAR UNE JURIDICTION RELEVANT DU CONSEIL D'ETAT - CIRCONSTANCE QUE LA JURIDICTION AYANT DÉCIDÉ DE PROCÉDER À CETTE TRANSMISSION NE SERAIT PAS TERRITORIALEMENT COMPÉTENTE POUR CONNAÎTRE DU LITIGE À L'OCCASION DUQUEL CETTE QUESTION EST POSÉE - CIRCONSTANCE SANS INCIDENCE SUR LE RENVOI AU CONSEIL CONSTITUTIONNEL DE LA QUESTION PAR LE CONSEIL D'ETAT.
</SCT>
<ANA ID="9A"> 54-10-05 Lorsqu'une juridiction relevant du Conseil d'Etat a décidé de transmettre à ce dernier une question prioritaire de constitutionnalité (QPC), il appartient au Conseil d'Etat, alors même que la juridiction ayant décidé de procéder à cette transmission ne serait pas territorialement compétente pour connaître du litige à l'occasion duquel cette question est posée, de se prononcer, dans le délai de trois mois prescrit par l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel, sur le renvoi de cette question au Conseil constitutionnel.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
