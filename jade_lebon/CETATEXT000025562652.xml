<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025562652</ID>
<ANCIEN_ID>JG_L_2012_03_000000352843</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/56/26/CETATEXT000025562652.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère / 6ème SSR, 19/03/2012, 352843</TITRE>
<DATE_DEC>2012-03-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>352843</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère / 6ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOULLEZ ; FOUSSARD</AVOCATS>
<RAPPORTEUR>M. Gaël Raimbault</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Maud Vialettes</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:352843.20120319</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>VU LA PROCEDURE SUIVANTE :<br/>
<br/>
 Procédure contentieuse antérieure<br/>
<br/>
              M. C...A...a demandé à la commission départementale d'aide sociale de Paris d'annuler la décision du département de Paris du 23 décembre 2008 de récupérer sur la succession de Mme B...A...le montant des prestations qui lui avaient été accordées au titre de l'aide sociale à hauteur de 64 691,90 euros. Par une décision prise à l'issue de l'audience publique du 15 janvier 2010 et notifiée le 24 mars 2010, la commission départementale d'aide sociale de Paris a rejeté sa demande.<br/>
<br/>
              Par une décision n° 100901 du 20 mai 2011, la commission centrale d'aide sociale, saisie par M.A..., a annulé la décision de la commission départementale d'aide sociale, reporté la récupération des prestations avancées par l'aide sociale à Mme B...A...au décès de M. A...ou, si elle intervenait antérieurement, à la vente de l'appartement qu'il occupait 25, rue Vauvenargues à Paris, réformé la décision du département de Paris dans cette mesure et rejeté le surplus des conclusions de l'appel de M.A....<br/>
<br/>
 Procédure devant le Conseil d'Etat  <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 21 septembre 2011, 22 décembre 2011 et 28 novembre 2012 au secrétariat du contentieux du Conseil d'Etat, M.A..., représenté par la SCP Boullez, demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette décision n° 100901 de la commission centrale d'aide sociale du 20 mai 2011 ;<br/>
<br/>
              2°) de lui allouer la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Par deux mémoires en défense, enregistrés les 27 août 2012 et 30 octobre 2013, le département de Paris, représenté par Me Foussard, conclut au rejet du pourvoi et à ce que la somme de 3 000 euros soit mise à la charge de M. A...au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu :<br/>
              - les autres pièces du dossier ;<br/>
              - la Constitution, notamment son article 62 ;<br/>
              - la décision du 19 mars 2012 par laquelle le Conseil d'Etat statuant au contentieux a renvoyé au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par M.A... ;<br/>
              - la décision du Conseil constitutionnel n° 2012-250 QPC du 8 juin 2012 statuant sur la question prioritaire de constitutionnalité soulevée par M.A... ;<br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
              Ont été entendus en séance publique :<br/>
<br/>
              - le rapport de M. Gaël Raimbault, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public.<br/>
<br/>
              La parole a été donnée, avant et après les conclusions, à la SCP Boullez, avocat de M. A...et à Me Foussard, avocat du département de Paris.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>CONSIDERANT CE QUI SUIT :<br/>
<br/>
              1. Aux termes des quatrième, sixième et septième alinéas de l'article L. 134-2 du code de l'action sociale et des familles, relatif à la commission centrale d'aide sociale, dans leur rédaction en vigueur à la date de la décision objet du présent pourvoi : " Chaque section ou sous-section comprend en nombre égal, d'une part, des membres du Conseil d'Etat, des magistrats de la Cour des comptes ou des magistrats de l'ordre judiciaire en activité ou honoraires désignés respectivement par le vice-président du Conseil d'Etat, le premier président de la Cour des comptes ou le garde des sceaux, ministre de la justice, d'autre part, des fonctionnaires ou personnes particulièrement qualifiées en matière d'aide ou d'action sociale désignées par le ministre chargé de l'action sociale. / (...) / Les rapporteurs qui ont pour fonction d'instruire les dossiers sont nommés par le ministre chargé de l'aide sociale soit parmi les membres du Conseil d'Etat et les magistrats de la Cour des comptes, soit parmi les fonctionnaires des administrations centrales des ministères, soit parmi les personnes particulièrement compétentes en matière d'aide ou d'action sociale. Ils ont voix délibérative dans les affaires où ils sont rapporteurs. / Des commissaires du Gouvernement, chargés de prononcer leurs conclusions sur les affaires que le président de la commission centrale, d'une section ou d'une sous-section leur confie, sont nommés par le ministre chargé de l'aide sociale parmi les membres du Conseil d'Etat, les magistrats de la Cour des comptes et les fonctionnaires du ministère chargé de l'aide sociale ".<br/>
<br/>
              2. Par sa décision n° 2012-250 QPC du 8 juin 2012, le Conseil constitutionnel, saisi par le Conseil d'Etat de la question prioritaire de constitutionnalité soulevée par M. A...à l'appui du présent pourvoi, a déclaré contraire à la Constitution la référence aux fonctionnaires figurant aux quatrième, sixième et septième alinéas de l'article L. 134-2 du code de l'action sociale et des familles qui détermine la composition de la commission centrale d'aide sociale, au motif qu'aucune disposition législative n'institue les garanties appropriées permettant de satisfaire au principe d'indépendance des fonctionnaires membres des sections ou sous-sections, rapporteurs ou commissaires du Gouvernement de cette juridiction, non plus que les garanties d'impartialité faisant obstacle à ce que des fonctionnaires exercent leurs fonctions en son sein lorsqu'elle connaît de questions relevant des services à l'activité desquels ils ont participé. Il a jugé que les décisions rendues par la commission antérieurement à la publication de sa décision ne pourraient être remises en cause sur le fondement de cette inconstitutionnalité que si une partie l'avait invoquée à l'encontre d'une décision n'ayant pas acquis un caractère définitif au jour de cette publication.<br/>
<br/>
              3. La décision de la commission centrale d'aide sociale contre laquelle M. A... se pourvoit en cassation a été rendue par une formation de jugement composée d'un conseiller d'Etat honoraire désigné par le vice-président du Conseil d'Etat, président, d'un fonctionnaire de l'Etat, attaché d'administration, assesseur, et d'une personne particulièrement compétente en matière d'aide ou d'action sociale, rapporteur. Il résulte de la décision mentionnée ci-dessus du Conseil constitutionnel que l'assesseur, alors même qu'il exerçait les fonctions de chargé de mission auprès du Médiateur de la République, ne pouvait régulièrement siéger au sein de la formation de jugement. M.A..., qui a invoqué l'inconstitutionnalité de l'article L. 134-2 du code de l'action sociale et des familles dans un mémoire enregistré le 22 décembre 2011, est dès lors fondé à demander pour ce motif l'annulation de la décision de la commission centrale d'aide sociale du 20 mai 2011, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, tirés de la méconnaissance des règles de dévolution de la charge de la preuve et de la violation de l'article L. 344-5 du même code.<br/>
<br/>
              4. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de M.A..., qui n'est pas la partie perdante dans la présente instance. En revanche, il y a lieu de mettre à la charge du département de Paris la somme de 3 000 euros à verser à M. A...sur ce fondement.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La décision de la commission centrale d'aide sociale du 20 mai 2011 est annulée.<br/>
Article 2 : L'affaire est renvoyée à la commission centrale d'aide sociale.<br/>
Article 3 : Le département de Paris versera à M. A...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions du département de Paris présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à M. C...A...et au département de Paris.<br/>
Copie en sera adressée pour information à la ministre des affaires sociales et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-10-05-01-02 PROCÉDURE. - ARTICLE FIXANT LA COMPOSITION D'UNE JURIDICTION, NONOBSTANT LA CIRCONSTANCE QU'EN L'ESPÈCE, LA COMPOSITION DE CETTE JURIDICTION NE SERAIT PAS CRITIQUABLE AU REGARD DU PRINCIPE CONSTITUTIONNEL INVOQUÉ.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-10-05-04-01 PROCÉDURE. - CARACTÈRE SÉRIEUX D'UNE QPC PORTANT SUR L'ARTICLE FIXANT LA COMPOSITION D'UNE JURIDICTION - CIRCONSTANCE QUE LE CONSEIL D'ETAT CONTRÔLE EN CASSATION LE RESPECT DES PRINCIPES D'INDÉPENDANCE ET D'IMPARTIALITÉ - CIRCONSTANCE SANS INCIDENCE SUR LE SÉRIEUX DE LA QPC.
</SCT>
<ANA ID="9A"> 54-10-05-01-02 L'article L. 134-2 du code de l'action sociale et des familles, qui fixe la composition de la commission centrale d'aide sociale, est, au sens et pour l'application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958, applicable à un pourvoi dirigé contre un arrêt rendu par cette commission, nonobstant la circonstance que cette commission aurait statué en l'espèce dans une composition qui la mettrait à l'abri de toute critique au regard du principe d'impartialité, dès lors que c'est sur le fondement des dispositions de cet article que les membres de la commission ayant statué sur cet appel ont été désignés.</ANA>
<ANA ID="9B"> 54-10-05-04-01 QPC portant sur l'article L. 134-2 du code de l'action sociale et des familles fixant la composition de la commission centrale d'aide sociale. La circonstance qu'il appartient au Conseil d'Etat d'exercer son contrôle sur la mise en oeuvre de ces dispositions au regard des principes d'indépendance et d'impartialité, lorsqu'il est saisi d'une telle contestation à l'occasion d'un pourvoi en cassation contre une décision de la commission, ne fait pas obstacle à ce que la QPC soit regardée comme présentant un caractère sérieux.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
