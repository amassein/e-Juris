<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033695630</ID>
<ANCIEN_ID>JG_L_2016_12_000000405791</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/69/56/CETATEXT000033695630.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 23/12/2016, 405791</TITRE>
<DATE_DEC>2016-12-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>405791</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2016:405791.20161223</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>  Vu la procédure suivante :<br/>
<br/>
              La Section française de l'Observatoire international des prisons a demandé au juge des référés du tribunal administratif d'Orléans, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'enjoindre à l'administration de prendre toutes les mesures utiles afin de faire cesser les atteintes graves et manifestement illégales portées aux libertés fondamentales des personnes détenues à la maison d'arrêt de Tours et, notamment, d'engager les mesures prescrites ou recommandées par la sous-commission départementale de sécurité contre les risques d'incendie (SRI) et non encore mises en oeuvre ou achevées.  Par une ordonnance n° 1603822 du 29 novembre 2016, le juge des référés du tribunal administratif d'Orléans a rejeté sa demande. <br/>
<br/>
              Par une requête, enregistrée le 8 décembre 2016 au secrétariat du contentieux du Conseil d'État, la Section française de l'Observatoire international des prisons demande au juge des référés du Conseil d'État, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de faire droit à sa demande de première instance ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros sur le fondement de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Elle soutient que : <br/>
              - la condition d'urgence est remplie dès lors que la sous-commission départementale de sécurité contre les risques d'incendie (SRI) a émis le 23 juin 2016 un avis défavorable à la poursuite de l'exploitation de la maison d'arrêt de Tours et que plusieurs des mesures prescrites pour mettre un terme à la situation de risque constatée n'ont fait l'objet d'aucun commencement d'exécution ;<br/>
              - la carence de l'administration à remédier à cette situation de danger porte une atteinte grave et immédiate aux libertés fondamentales que sont le droit à la vie et le droit de ne pas subir des traitements inhumains ou dégradants protégées par les articles 2 et 3 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, et particulièrement au regard de la surpopulation de cet établissement ;<br/>
              - les articles 2 et 3 de la convention précitée font peser sur les Etats des obligations positives et préventives particulièrement fortes ;<br/>
<br/>
              Par un mémoire en défense, enregistré le 19 décembre 2016, le garde des sceaux, ministre de la justice, conclut au rejet de la requête. Il soutient que la condition d'urgence n'est pas remplie et que les moyens soulevés par la Section française de l'Observatoire international des prisons sont infondés.<br/>
<br/>
<br/>
              Vu le procès-verbal de l'audience publique du mardi 20 décembre 2016 à 14 heures au cours de laquelle ont été entendus : <br/>
<br/>
<br/>
              - Me Spinosi, avocat au Conseil d'Etat et à la Cour de cassation, avocat de la Section française de l'Observatoire international des prisons ;<br/>
<br/>
              - le représentant de l'Observatoire international des prisons ; <br/>
<br/>
              - les représentants du garde des sceaux, ministre de la justice ;<br/>
<br/>
              et à l'issue de laquelle le juge des référés a prolongé l'instruction jusqu'au 22 décembre  ;<br/>
<br/>
              Vu, enregistré le 21 décembre, le mémoire présenté par la Section française de l'Observatoire international des prisons ;<br/>
<br/>
              Vu, enregistré le 22 décembre, le mémoire en défense présenté par le garde des sceaux, ministre de la justice ;<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
              - la loi n° 2009-1436 du 24 novembre 2009 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
                           Sur le cadre juridique du litige :<br/>
<br/>
              1. Considérant qu'aux termes de l'article 22 de la loi du 24 novembre 2009 : " L'administration pénitentiaire garantit à toute personne détenue le respect de sa dignité et de ses droits. L'exercice de ceux-ci ne peut faire l'objet d'autres restrictions que celles résultant des contraintes inhérentes à la détention, du maintien de la sécurité et du bon ordre des établissements, de la prévention de la récidive et de la protection de l'intérêt des victimes. Ces restrictions tiennent compte de l'âge, de l'état de santé, du handicap et de la personnalité de la personne détenue " ; <br/>
<br/>
              2. Considérant qu'eu égard à la vulnérabilité des détenus et à leur situation d'entière dépendance vis à vis de l'administration, il appartient à celle-ci, et notamment aux directeurs des établissements pénitentiaires, en leur qualité de chefs de service, de prendre les mesures propres à protéger leur vie ainsi qu'à leur éviter tout traitement inhumain ou dégradant afin de garantir le respect effectif des exigences découlant des principes rappelés notamment par les articles 2 et 3 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; que le droit au respect de la vie ainsi que le droit de ne pas être soumis à des traitements inhumains ou dégradants constituent des libertés fondamentales au sens des dispositions de  l'article L. 521-2 du code de justice administrative ; que, lorsque la carence de l'autorité publique crée un danger caractérisé et imminent pour la vie des personnes ou les expose à être soumises, de manière caractérisée, à un traitement inhumain ou dégradant, portant ainsi une atteinte grave et manifestement illégale à ces libertés fondamentales, et que la situation permet de prendre utilement des mesures de sauvegarde dans un délai de quarante-huit heures, le juge des référés peut, au titre de la procédure particulière prévue par l'article L. 521-2 précité, prescrire toutes les mesures de nature à faire cesser la situation résultant de cette carence ; <br/>
<br/>
              Sur les pouvoirs que le juge des référés tient de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              3. Considérant qu'aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale " ; que le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public aurait porté une atteinte grave et manifestement illégale " ;<br/>
<br/>
              4. Considérant qu'il résulte de la combinaison des dispositions des articles L. 511-1, L. 521-2 et L. 521-4 du code de justice administrative qu'il appartient au juge des référés, lorsqu'il est saisi sur le fondement de l'article L. 521-2 précité et qu'il constate une atteinte grave et manifestement illégale portée par une personne morale de droit public à une liberté fondamentale, de prendre les mesures qui sont de nature à faire disparaître les effets de cette atteinte ; que ces mesures doivent en principe présenter un caractère provisoire, sauf lorsque aucune mesure de cette nature n'est susceptible de sauvegarder l'exercice effectif de la liberté fondamentale à laquelle il est porté atteinte ; que le juge des référés peut, sur le fondement de l'article L. 521-2 du code de justice administrative, ordonner à l'autorité compétente de prendre, à titre provisoire, une mesure d'organisation des services placés sous son autorité lorsqu'une telle mesure est nécessaire à la sauvegarde d'une liberté fondamentale ; que, toutefois, le juge des référés ne peut, au titre de la procédure particulière prévue par l'article L. 521-2 précité, qu'ordonner les mesures d'urgence qui lui apparaissent de nature à sauvegarder, dans un délai de quarante-huit heures, la liberté fondamentale à laquelle il est porté une atteinte grave et manifestement illégale ; qu'eu égard à son office, il peut également, le cas échéant, décider de déterminer dans une décision ultérieure prise à brève échéance les mesures complémentaires qui s'imposent et qui peuvent être très rapidement mises en oeuvre ; que, dans tous les cas, l'intervention du juge des référés dans les conditions d'urgence particulière prévues par l'article L. 521-2 précité est subordonnée au constat que la situation litigieuse permette de prendre utilement et à très bref délai les mesures de sauvegarde nécessaires ;<br/>
<br/>
              5. Considérant que les règles générales de la procédure contentieuse interdisent au juge de se fonder sur des pièces qui n'auraient pas été soumises au débat contradictoire ; que, par suite, le juge des référés ne peut fonder sa décision sur le contenu de documents qui n'auraient pas été communiqués à l'autre partie, et ce alors même que ces documents auraient été couverts par un secret garanti par la loi ; que par suite doit être écartée des débats la pièce produite par le garde des sceaux ministre de la justice à l'appui de son dernier mémoire et dont ce dernier indique qu'elle présente un caractère secret et ne peut être communiquée à l'autre partie ; <br/>
<br/>
              Sur les conclusions à fin d'injonction :<br/>
              6. Considérant qu'il résulte de l'instruction que, le 23 juin 2016, la sous-commission départementale de sécurité contre les risques d'incendie (SRI), a procédé à une analyse des risques d'incendie présentés par la maison d'arrêt de Tours ; qu'elle a souligné  les risques d'apparition d'incendie découlant  de la vétusté de certaines installations techniques et de l'absence de vérification de ces dernières, les risques d'aggravation d'un éventuel incendie en raison de l'état d'encombrement du sous-sol de la maison d'arrêt et de l'insuffisante protection de certains locaux techniques à risque particulier  et enfin les risques de gêne à l'évacuation du public et à l'intervention des secours ; qu'au vu de cette analyse, elle a émis un avis défavorable ; que son avis était  assorti de prescriptions adressées à l'administration ;   <br/>
<br/>
              7. Considérant que la Section française de l'Observatoire international des prisons soutient que, compte tenu du caractère défavorable de l'avis émis, le défaut de mise en oeuvre des prescriptions de la sous-commission crée un danger caractérisé et imminent pour la vie des personnes qui sont détenues à la maison d'arrêt de Tours ou qui y travaillent ; qu'elle demande au juge des référés d'enjoindre à l'administration de mettre en oeuvre ces prescriptions ;<br/>
<br/>
              En ce qui concerne la prescription relative à  l'élaboration d'un plan directeur de mise en sécurité du site :<br/>
<br/>
              8. Considérant que, pour faire cesser les atteintes invoquées aux droits découlant des articles 2, 3 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, la Section française de l'Observatoire international des prisons demande qu'il soit enjoint à l'administration pénitentiaire d'élaborer un plan directeur de mise en sécurité du site ; qu'eu égard à son objet, cette injonction n'est pas, ainsi que l'a d'ailleurs relevé le juge des référés du tribunal administratif d'Orléans, au nombre des mesures d'urgence que la situation permet de prendre utilement et à très bref délai et ne relève donc pas du champ d'application de l'article L. 521-2 du code de justice administrative ;<br/>
              En ce qui concerne les autres prescriptions : <br/>
<br/>
            9. Considérant qu'il résulte de l'instruction est des échanges intervenus au cours de l'audience publique que l'administration avait entrepris, dès le mois de septembre 2016 de procéder, en liaison avec les services municipaux, au désencombrement du sous-sol de la maison d'arrêt afin de faire disparaître le principal risque relevé par la sous-commission départementale de sécurité contre les risques d'incendie ; qu'à la date de la présente ordonnance, ces travaux sont quasiment achevés et que l'administration a engagé les études préalables aux travaux qui permettront de garantir la pérennité de la mise en sécurité du sous-sol ; que, concernant les préconisations relatives à d'autres risques, l'administration établit que plusieurs ont été mises en oeuvre et que les travaux ou les mesures d'organisation nécessaires pour se conformer aux prescriptions restantes sont programmés et seront engagés au début de l'année 2017 ; que, dans ces conditions, il n'y a pas urgence pour le juge des référés à intervenir dans le bref délai prévu par l'article L. 521-2 du code de justice administrative ;<br/>
              10. Considérant qu'il résulte de ce qui précède que la demande de la Section française de l'Observatoire international des prisons ne peut qu'être rejetée, y compris en ce qu'elle tendait à l'application de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de la Section française de l'Observatoire international des prisons est rejetée. <br/>
Article 2 : La présente ordonnance sera notifiée à la Section française de l'Observatoire international des prisons et au garde des sceaux, ministre de la justice.  <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-04-03-01 PROCÉDURE. INSTRUCTION. CARACTÈRE CONTRADICTOIRE DE LA PROCÉDURE. COMMUNICATION DES MÉMOIRES ET PIÈCES. - CAS OÙ UNE PARTIE PRODUIT DES DOCUMENTS COUVERTS PAR UN SECRET GARANTI PAR LA LOI EN INDIQUANT QU'ILS NE PEUVENT ÊTRE COMMUNIQUÉS À L'AUTRE PARTIE - FACULTÉ POUR LE JUGE DE SE FONDER SUR CES DOCUMENTS - ABSENCE [RJ1] - CONSÉQUENCE - DOCUMENTS ÉCARTÉS DES DÉBATS.
</SCT>
<ANA ID="9A"> 54-04-03-01 Les règles générales de la procédure contentieuse interdisent au juge de se fonder sur des pièces qui n'auraient pas été soumises au débat contradictoire. Par suite, il ne peut fonder sa décision sur le contenu de documents qui n'auraient pas été communiqués à l'autre partie, et ce alors même que ces documents auraient été couverts par un secret garanti par la loi. Doit donc être écartée des débats la pièce produite par le ministre et dont il indique qu'elle présente un caractère secret et ne peut être communiquée à l'autre partie.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Cf., sur le principe de la communication contradictoire de tout élément sur lequel se fonde le juge, CE, Assemblée, 6 novembre 2002, M. Moon Sun Myung, n°s 194295 219587, p. 380 ; CE, 13 février 2006, Société Fiducial Informatique et Société Fiducial Expertise, n° 279180, p. 307 ; CE, 4 juin 2008, Société Sparflex, n° 201776, T. p. 665 ; CE, 10 juin 2009, Société Baudin Châteauneuf, n° 320037, T. pp. 841-890 ; CE, 26 janvier 2011, M. Weissenburger, n° 311808, p. 18 ; CE, 13 avril 2016, Etablissement public de santé de Ville-Evrard, n° 386059, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
