<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026219183</ID>
<ANCIEN_ID>JG_L_2012_07_000000347677</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/21/91/CETATEXT000026219183.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème et 7ème sous-sections réunies, 23/07/2012, 347677</TITRE>
<DATE_DEC>2012-07-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>347677</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème et 7ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Sophie-Caroline de Margerie</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Béatrice Bourgeois-Machureau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:347677.20120723</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 21 mars 2011 au secrétariat du contentieux du Conseil d'Etat, présentée par l'association Enfance et familles d'adoption, dont le siège est 221, rue La Fayette à Paris (75010) ; l'association Enfance et familles d'adoption demande au Conseil d'Etat d'annuler pour excès de pouvoir la partie III de la circulaire du garde des sceaux en date du 22 décembre 2010, relative au statut des enfants en cours de procédure d'adoption en Haïti et aux procédures judiciaires en France ; <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 17 juillet 2012, présentée par l'association Enfance et famille d'adoption ; <br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu le code civil ;<br/>
<br/>
              Vu le code de procédure civile ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie-Caroline de Margerie, Conseiller d'Etat,<br/>
<br/>
              - les conclusions de Mme Béatrice Bourgeois-Machureau, Rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant qu'aux termes de l'article 370-3 du code civil : " Les conditions de l'adoption sont soumises à la loi nationale de l'adoptant ou, en cas d'adoption par deux époux, par la loi qui régit les effets de leur union. L'adoption ne peut toutefois être prononcée si la loi nationale de l'un et l'autre époux la prohibe. / L'adoption d'un mineur étranger ne peut être prononcée si sa loi personnelle prohibe cette institution, sauf si ce mineur est né et réside habituellement en France. / Quelle que soit la loi applicable, l'adoption requiert le consentement du représentant légal de l'enfant. Le consentement doit être libre, obtenu sans aucune contrepartie, après la naissance de l'enfant et éclairé sur les conséquences de l'adoption, en particulier, s'il est donné en vue d'une adoption plénière, sur le caractère complet et irrévocable de la rupture du lien de filiation préexistant " ; qu'aux termes de l'article 370-5 du même code : " L'adoption régulièrement prononcée à l'étranger produit en France les effets de l'adoption plénière si elle rompt de manière complète et irrévocable le lien de filiation préexistant. A défaut, elle produit les effets de l'adoption simple. Elle peut être convertie en adoption plénière si les consentements requis ont été donnés expressément en connaissance de cause " ; <br/>
<br/>
              Considérant que, lorsqu'une autorité administrative prescrit, par la voie d'une circulaire, le respect de règles de portée générale qui tirent les conséquences d'une décision de justice, elle ne peut que respecter l'autorité qui s'attache à cette décision ; qu'il n'appartient pas au juge de l'excès de pouvoir, saisi d'un recours dirigé contre une telle circulaire, d'apprécier le bien-fondé de la décision de justice en cause, mais seulement d'apprécier, dans l'exercice de son contrôle de légalité et dans la limite des moyens soulevés, si l'interprétation retenue par la circulaire ne méconnaît pas le sens et la portée de cette décision ;<br/>
<br/>
              Considérant que la Cour de cassation a jugé, par deux arrêts du 4 juin 2009, que la formalité de la légalisation des actes de l'état civil établis par une autorité étrangère et destinés à être produits en France demeure, selon la coutume internationale et sauf convention internationale contraire, obligatoire pour y recevoir effet ; <br/>
<br/>
              Considérant que, par le III de la circulaire attaquée, relatif aux requêtes en adoption plénière des enfants haïtiens, le garde des sceaux, ministre de la justice, après avoir rappelé que le consentement des parents de naissance ou du représentant légal des enfants haïtiens doit, conformément au droit international public, être légalisé, Haïti n'étant lié par aucune convention internationale dispensant de cette formalité, et que, depuis 2009, les autorités haïtiennes refusent de légaliser les consentements donnés par les parents de naissance et reçus par les notaires locaux en vue de l'adoption plénière de droit français, dans la mesure où la règlementation haïtienne ne prévoit que l'adoption simple, prescrit aux procureurs généraux près les cours d'appel et les tribunaux supérieurs d'appel de " donner un avis négatif à toute requête en adoption plénière qui pourrait être déposée, dès lors que le consentement donné en vue de l'adoption plénière ne peut être légalisé " ; qu'en tirant cette conséquence de la jurisprudence de la Cour de cassation, qui a, au demeurant, ultérieurement précisé, par un avis du 4 avril 2011 et par un arrêt du 23 mai 2012, que l'obligation de légalisation par les autorités haïtiennes compétentes s'applique au consentement par acte authentique donné à Haïti par les parents biologiques haïtiens en vue de l'adoption plénière de leur enfant en France, le garde des sceaux n'a pas méconnu le sens et la portée de la jurisprudence de la Cour de cassation, ni les dispositions précitées du code civil, non plus que les stipulations de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; que, contrairement à ce que soutient l'association requérante, la circulaire attaquée n'a ni pour objet ni pour effet d'étendre les pouvoirs dont dispose le ministère public en matière d'adoption ;<br/>
<br/>
              Considérant qu'il résulte de ce qui précède que, sans qu'il soit besoin de se prononcer sur la fin de non-recevoir opposée par le garde des sceaux, ministre de la justice, la requête de l'association Enfance et familles d'adoption ne peut qu'être rejetée ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête de l'association Enfance et familles d'adoption est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à l'association Enfance et familles d'adoption et à la garde des sceaux, ministre de la justice.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-01-05-03 ACTES LÉGISLATIFS ET ADMINISTRATIFS. DIFFÉRENTES CATÉGORIES D'ACTES. ACTES ADMINISTRATIFS - NOTION. INSTRUCTIONS ET CIRCULAIRES. - CIRCULAIRE INTERPRÉTATIVE À PORTÉE IMPÉRATIVE - PRESCRIPTION DE RÈGLES DE PORTÉE GÉNÉRALE TIRANT LES CONSÉQUENCES D'UNE DÉCISION DE JUSTICE - DEMANDE D'ANNULATION POUR EXCÈS DE POUVOIR - 1) CONTRÔLE DU JUGE DE L'EXCÈS DE POUVOIR - CONTRÔLE DU BIEN-FONDÉ DE LA DÉCISION DE JUSTICE - ABSENCE - CONTRÔLE DE LA CONFORMITÉ À LA DÉCISION DE JUSTICE DE L'INTERPRÉTATION QUI EN EST DONNÉE - EXISTENCE [RJ1] - 2) APPLICATION EN L'ESPÈCE - ADOPTION PLÉNIÈRE D'ENFANTS HAÏTIENS - CIRCULAIRE PRESCRIVANT AUX PROCUREURS DE DONNER UN AVIS NÉGATIF À TOUTE REQUÊTE EN ADOPTION PLÉNIÈRE D'ENFANTS HAÏTIENS - LÉGALITÉ, AU VU DE LA JURISPRUDENCE DE LA COUR DE CASSATION [RJ2].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">01-01-05-03-02 ACTES LÉGISLATIFS ET ADMINISTRATIFS. DIFFÉRENTES CATÉGORIES D'ACTES. ACTES ADMINISTRATIFS - NOTION. INSTRUCTIONS ET CIRCULAIRES. LÉGALITÉ. - CIRCULAIRE INTERPRÉTATIVE À PORTÉE IMPÉRATIVE - PRESCRIPTION DE RÈGLES DE PORTÉE GÉNÉRALE TIRANT LES CONSÉQUENCES D'UNE DÉCISION DE JUSTICE - DEMANDE D'ANNULATION POUR EXCÈS DE POUVOIR - 1) CONTRÔLE DU JUGE DE L'EXCÈS DE POUVOIR - CONTRÔLE DU BIEN-FONDÉ DE LA DÉCISION DE JUSTICE - ABSENCE - CONTRÔLE DE LA CONFORMITÉ À LA DÉCISION DE JUSTICE DE L'INTERPRÉTATION QUI EN EST DONNÉE - EXISTENCE [RJ1] - 2) APPLICATION EN L'ESPÈCE - ADOPTION PLÉNIÈRE D'ENFANTS HAÏTIENS - CIRCULAIRE PRESCRIVANT AUX PROCUREURS DE DONNER UN AVIS NÉGATIF À TOUTE REQUÊTE EN ADOPTION PLÉNIÈRE D'ENFANTS HAÏTIENS - LÉGALITÉ, AU VU DE LA JURISPRUDENCE DE LA COUR DE CASSATION [RJ2].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">35-05 FAMILLE. ADOPTION. - ADOPTION D'ENFANTS HAÏTIENS - CIRCULAIRE PRESCRIVANT AUX PROCUREURS DE DONNER UN AVIS NÉGATIF À TOUTE REQUÊTE EN ADOPTION PLÉNIÈRE - LÉGALITÉ, AU VU DE LA JURISPRUDENCE DE LA COUR DE CASSATION [RJ1] [RJ2].
</SCT>
<ANA ID="9A"> 01-01-05-03 1) ll n'appartient pas au juge de l'excès de pouvoir, saisi d'un recours dirigé contre une circulaire prescrivant le respect de règles de portée générale qui tirent les conséquences d'une décision de justice, d'apprécier le bien-fondé de la décision de justice en cause, mais seulement d'apprécier, dans l'exercice de son contrôle de légalité et dans la limite des moyens soulevés, si l'interprétation retenue par la circulaire ne méconnaît pas le sens et la portée de cette décision.,,2) En l'espèce, le garde des sceaux a prescrit aux procureurs généraux près les cours d'appel et les tribunaux supérieurs d'appel de donner un avis négatif à toute requête en adoption plénière d'enfants haïtiens qui pourrait être déposée, dès lors que le consentement donné en vue de l'adoption plénière ne peut être légalisé.,,En tirant cette conséquence de la jurisprudence de la Cour de cassation, qui a jugé, par deux arrêts du 4 juin 2009, que la formalité de la légalisation des actes de l'état civil établis par une autorité étrangère et destinés à être produits en France demeure, selon la coutume internationale et sauf convention internationale contraire, obligatoire pour y recevoir effet et qui a, au demeurant, ultérieurement précisé, par un avis du 4 avril 2011 et par un arrêt du 23 mai 2012, que l'obligation de légalisation par les autorités haïtiennes compétentes s'applique au consentement par acte authentique donné à Haïti par les parents biologiques haïtiens en vue de l'adoption plénière de leur enfant en France, le garde des sceaux n'a pas méconnu le sens et la portée de la jurisprudence de la Cour de cassation.</ANA>
<ANA ID="9B"> 01-01-05-03-02 1) ll n'appartient pas au juge de l'excès de pouvoir, saisi d'un recours dirigé contre une circulaire prescrivant le respect de règles de portée générale qui tirent les conséquences d'une décision de justice, d'apprécier le bien-fondé de la décision de justice en cause, mais seulement d'apprécier, dans l'exercice de son contrôle de légalité et dans la limite des moyens soulevés, si l'interprétation retenue par la circulaire ne méconnaît pas le sens et la portée de cette décision.,,2) En l'espèce, le garde des sceaux a prescrit aux procureurs généraux près les cours d'appel et les tribunaux supérieurs d'appel de donner un avis négatif à toute requête en adoption plénière d'enfants haïtiens qui pourrait être déposée, dès lors que le consentement donné en vue de l'adoption plénière ne peut être légalisé.,,En tirant cette conséquence de la jurisprudence de la Cour de cassation, qui a jugé, par deux arrêts du 4 juin 2009, que la formalité de la légalisation des actes de l'état civil établis par une autorité étrangère et destinés à être produits en France demeure, selon la coutume internationale et sauf convention internationale contraire, obligatoire pour y recevoir effet et qui a, au demeurant, ultérieurement précisé, par un avis du 4 avril 2011 et par un arrêt du 23 mai 2012, que l'obligation de légalisation par les autorités haïtiennes compétentes s'applique au consentement par acte authentique donné à Haïti par les parents biologiques haïtiens en vue de l'adoption plénière de leur enfant en France, le garde des sceaux n'a pas méconnu le sens et la portée de la jurisprudence de la Cour de cassation.</ANA>
<ANA ID="9C"> 35-05 Le garde des sceaux a prescrit aux procureurs généraux près les cours d'appel et les tribunaux supérieurs d'appel de donner un avis négatif à toute requête en adoption plénière d'enfants haïtiens qui pourrait être déposée, dès lors que le consentement donné en vue de l'adoption plénière ne peut être légalisé.... ...En tirant cette conséquence de la jurisprudence de la Cour de cassation, qui a jugé, par deux arrêts du 4 juin 2009, que la formalité de la légalisation des actes de l'état civil établis par une autorité étrangère et destinés à être produits en France demeure, selon la coutume internationale et sauf convention internationale contraire, obligatoire pour y recevoir effet et qui a, au demeurant, ultérieurement précisé, par un avis du 4 avril 2011 et par un arrêt du 23 mai 2012, que l'obligation de légalisation par les autorités haïtiennes compétentes s'applique au consentement par acte authentique donné à Haïti par les parents biologiques haïtiens en vue de l'adoption plénière de leur enfant en France, le garde des sceaux n'a pas méconnu le sens et la portée de la jurisprudence de la Cour de cassation.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 24 avril 2012, Afane-Jacquart, n° 345301, à mentionner aux Tables.,,[RJ2] Rappr. Cass., civ. 1ère, 4 juin 2009, n° 08-10.962 et 08-13.541, Bull. 2009 I, n° 115 et 116 ; Cass., 4 avril 2011, n° 11-00.001, Bull. 2011, Avis n° 5 ; Cass., civ 1ère, 23 mai 2012, n° 11-17716, publié au bulletin.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
