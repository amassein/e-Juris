<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028466333</ID>
<ANCIEN_ID>JG_L_2014_01_000000372804</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/46/63/CETATEXT000028466333.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème et 5ème sous-sections réunies, 13/01/2014, 372804</TITRE>
<DATE_DEC>2014-01-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>372804</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème et 5ème sous-sections réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>FOUSSARD ; SCP BARTHELEMY, MATUCHANSKY, VEXLIARD</AVOCATS>
<RAPPORTEUR>M. David Moreau</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Gaëlle Dumortier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:372804.20140113</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la décision n° 11911/QPC du 14 octobre 2013, enregistrée au secrétariat du contentieux du Conseil d'Etat le 15 octobre 2013, par laquelle la chambre disciplinaire nationale de l'ordre des médecins, avant de statuer sur l'appel de M. C...B...tendant à l'annulation de la décision du 6 février 2013 de la chambre disciplinaire de première instance de l'ordre des médecins de Rhône-Alpes ayant rejeté sa plainte, a décidé, par application des dispositions de l'article 23-2 de l'ordonnance n° 58-1067 du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel, de transmettre au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution du second alinéa de l'article L. 4124-2 du code de la santé publique ; <br/>
<br/>
              Vu le mémoire, enregistré le 11 mars 2013 au greffe de la chambre disciplinaire nationale de l'ordre des médecins, présenté par M.B..., demeurant..., en application de l'article 23-1 de l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution, notamment son Préambule et son article 61-1 ; <br/>
<br/>
              Vu l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
<br/>
              Vu le code de la santé publique ;<br/>
<br/>
              Vu le code de la sécurité sociale ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. David Moreau, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Foussard, avocat de Mme D...et à la SCP Barthélemy, Matuchansky, Vexliard, avocat du Conseil national de l'ordre des médecins ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il résulte des dispositions de l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que, lorsqu'une juridiction relevant du Conseil d'Etat a transmis à ce dernier, en application de l'article 23-2 de cette même ordonnance, la question de la conformité à la Constitution d'une disposition législative, le Conseil constitutionnel est saisi de cette question de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ; que la question transmise par la chambre disciplinaire nationale de l'ordre des médecins a trait au second alinéa de l'article L. 4124-2 du code de la santé publique, sur le fondement duquel la plainte déposée par M. B...contre un médecin-conseil l'ayant contrôlé a été rejetée comme irrecevable par la chambre disciplinaire de première instance de Rhône-Alpes ;<br/>
<br/>
              2. Considérant que, selon le second alinéa de L. 4124-2 du code de la santé publique, les praticiens chargés d'un service public et inscrits au tableau de l'ordre, lorsqu'ils exercent une fonction de contrôle prévue par la loi ou le règlement, " ne peuvent être traduits devant la chambre disciplinaire de première instance, à l'occasion des actes commis dans l'exercice de cette fonction, que par le ministre chargé de la santé, le représentant de l'Etat dans le département, le directeur général de l'agence régionale de santé ou le procureur de la République " ; <br/>
<br/>
              3. Considérant, d'une part, que si les dispositions litigieuses ne permettent pas aux instances de l'ordre des médecins d'engager des poursuites disciplinaires à l'encontre d'un médecin exerçant des fonctions de contrôle, cette impossibilité ne fonde pas l'irrecevabilité qui a été opposée à M. B...; que, par suite, les dispositions du second alinéa de l'article L. 4124-2 du code de la santé publique ne peuvent être regardées comme applicables au litige, au sens et pour l'application de l'article 23-4 de l'ordonnance du 7 novembre 1958, en tant qu'elles n'incluent pas les instances ordinales parmi les personnes pouvant engager des poursuites disciplinaires contre les médecins exerçant une fonction de contrôle ;<br/>
<br/>
              4. Considérant, d'autre part, que M. B...soutient qu'en n'ouvrant pas cette possibilité aux victimes de manquements déontologiques commis par les médecins chargés d'une fonction de contrôle, le second alinéa de l'article L. 4124-2 du code de la santé publique est contraire au droit à un recours juridictionnel effectif découlant de l'article 16 de la Déclaration des droits de l'homme et du citoyen et introduit entre les médecins exerçant une fonction de contrôle et les autres une différence de traitement contraire au principe d'égalité garanti par les articles 1er et 6 du même texte ;<br/>
<br/>
              5. Considérant, en premier lieu, que la disposition contestée n'a ni pour objet ni pour effet de priver les victimes de manquements déontologiques commis par les médecins exerçant une fonction de contrôle de la possibilité d'exercer une action en réparation devant le juge civil ou de mettre en mouvement l'action publique si les faits reprochés sont susceptibles de recevoir une qualification pénale ; que par ailleurs, l'impossibilité, pour les personnes contrôlées par un médecin-conseil, de le traduire devant la chambre disciplinaire ordinale, d'une part, est limitée aux actes commis dans l'exercice de sa mission et, d'autre part, répond à la nécessité d'intérêt général de garantir l'indépendance et l'impartialité des médecins-conseils par rapport aux personnes qu'ils contrôlent et, par voie de conséquence, l'efficacité du contrôle médical sur l'activité des professionnels de santé, prévu par les articles L. 315-1 et suivants du code de la sécurité sociale ; que, compte tenu de l'ensemble de ces éléments, les dispositions en cause du second alinéa de l'article L. 4124-2 du code de la santé publique ne peuvent être regardées comme portant atteinte au droit des personnes contrôlées à un recours juridictionnel effectif ; <br/>
<br/>
              6. Considérant, en second lieu, que les médecins exerçant une fonction de contrôle ne sont pas dans une situation identique à celles des autres médecins dès lors, qu'ainsi qu'il vient d'être dit, l'exercice de leur mission exige qu'ils puissent agir en toute indépendance et impartialité par rapport aux personnes qu'ils contrôlent ; qu'en outre, ils ne sont pas à l'abri de toute poursuite disciplinaire pour des actes commis dans l'exercice de leur mission de contrôle puisque la disposition contestée permet au ministre chargé de la santé, au représentant de l'Etat dans le département, au directeur général de l'agence régionale de santé et au procureur de la République d'intenter une telle action ; que, par suite, le deuxième alinéa de l'article L. 4124-2 du code de la santé publique n'introduit pas entre les médecins exerçant une fonction de contrôle et les autres une discrimination contraire au principe d'égalité garanti par les articles 1er et 6 de la Déclaration des droits de l'homme et du citoyen ;<br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que la question soulevée, qui n'est pas nouvelle, ne présente pas un caractère sérieux ; que, par suite, il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par M. B... ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
     --------------<br/>
<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité transmise par la chambre disciplinaire nationale de l'ordre des médecins.<br/>
Article 2 : La présente décision sera notifiée à M. C...B..., à Mme A...D...et à la ministre des affaires sociales et de la santé.<br/>
Copie en sera adressée au Conseil constitutionnel, au Premier ministre, au Conseil national de l'ordre des médecins et à la chambre disciplinaire nationale de l'ordre des médecins.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-10-05-01-01 PROCÉDURE. - DISPOSITION LÉGISLATIVE CONTESTÉE EN TANT QU'ELLE EXCLUT DE SON BÉNÉFICE UNE CATÉGORIE DE PERSONNES - CONDITION D'APPLICABILITÉ - AUTEUR DE LA QPC VICTIME DE CETTE EXCLUSION DANS LE LITIGE AU PRINCIPAL.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-10-05-01-03 PROCÉDURE. - DISPOSITION LÉGISLATIVE CONTESTÉE EN TANT QU'ELLE EXCLUT DE SON BÉNÉFICE UNE CATÉGORIE DE PERSONNES - AUTEUR DE LA QPC N'ÉTANT PAS, DANS LE LITIGE AU PRINCIPAL, VICTIME DE CETTE EXCLUSION.
</SCT>
<ANA ID="9A"> 54-10-05-01-01 Une disposition législative ne peut être utilement contestée par la voie de la question prioritaire de constitutionnalité (QPC) en tant qu'elle exclut de son bénéfice une catégorie de personnes que si, dans le litige principal, le requérant est effectivement victime de la discrimination qu'il dénonce.</ANA>
<ANA ID="9B"> 54-10-05-01-03 Une disposition législative ne peut être utilement contestée par la voie de la question prioritaire de constitutionnalité (QPC) en tant qu'elle exclut de son bénéfice une catégorie de personnes que si, dans le litige principal, le requérant est effectivement victime de la discrimination qu'il dénonce. En l'espèce, un praticien ne peut contester les dispositions de l'article L. 4124-2 du code de la santé publique en tant qu'elles n'incluent pas les instances ordinales parmi les personnes pouvant engager des poursuites disciplinaires contre les médecins exerçant une fonction de contrôle, dès lors que cette exclusion ne fonde pas l'irrecevabilité qui a été opposée à sa propre plainte et qu'il conteste.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
