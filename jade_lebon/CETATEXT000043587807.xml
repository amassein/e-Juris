<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043587807</ID>
<ANCIEN_ID>JG_L_2021_05_000000441145</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/58/78/CETATEXT000043587807.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 4ème chambres réunies, 28/05/2021, 441145</TITRE>
<DATE_DEC>2021-05-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>441145</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>LE PRADO</AVOCATS>
<RAPPORTEUR>M. Arnaud Skzryerbak</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Villette</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:441145.20210528</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 11 juin, 11 septembre et 4 décembre 2020 au secrétariat du contentieux du Conseil d'Etat, la société Zentiva France demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir, d'une part, la décision du directeur général de l'Agence nationale de sécurité du médicament et des produits de santé (ANSM), révélée par le communiqué de presse du 25 mai 2020, de mettre fin à la recommandation temporaire d'utilisation des spécialités à base de Baclofène et, d'autre part, la décision du directeur général de l'Agence du 29 mai 2020 rejetant son recours gracieux contre cette communication ;<br/>
<br/>
              2°) d'enjoindre au directeur général de l'Agence de procéder au retrait de son site internet de la communication du 25 mai 2020 ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la santé publique ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;		<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Arnaud Skzryerbak, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Vincent Villette, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à Me Le Prado, avocat de l'Agence nationale de sécurité du médicament et des produits de santé ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Par des décisions du 22 octobre 2018, le directeur général de l'Agence nationale de sécurité du médicament et des produits de santé a autorisé la mise sur le marché des spécialités Baclocur 10 milligrammes, 20 milligrammes et 40 milligrammes, comprimé pelliculé sécable, dont le principe actif est le baclofène, indiquées à des fins de réduction de la consommation d'alcool, après échec des autres traitements médicamenteux disponibles, chez les patients adultes ayant une dépendance à l'alcool et une consommation d'alcool à risque élevé. Par un communiqué de presse du 23 octobre suivant, le directeur général de l'Agence a annoncé l'octroi de ces autorisations et indiqué que la recommandation temporaire d'utilisation du baclofène dans la prise en charge des patients alcoolo-dépendants, établie par l'Agence en 2014 et périodiquement renouvelée, serait prolongée jusqu'à la commercialisation effective des spécialités Baclocur. Après avoir informé la requérante, par un courrier du 15 mai 2020, qu'il serait mis à la recommandation temporaire d'utilisation des spécialités à base de baclofène en raison de la commercialisation à venir des spécialités Baclocur, le directeur général de l'Agence a indiqué, par un communiqué de presse du 25 mai 2020, que ces dernières spécialités seraient commercialisées à compter du 15 juin. La société Zentiva France, qui exploite une des spécialités faisant l'objet de la recommandation, demande au Conseil d'Etat d'annuler pour excès de pouvoir la décision qu'elle estime révélée par le communiqué de presse du 25 mai 2020 ainsi que le rejet de son recours gracieux contre cette communication.<br/>
<br/>
              Sur le cadre juridique du litige :  <br/>
<br/>
              2. Aux termes de l'article L. 5121-12-1 du code de la santé publique : " I. - Une spécialité pharmaceutique peut faire l'objet d'une prescription non conforme à son autorisation de mise sur le marché en l'absence de spécialité de même principe actif, de même dosage et de même forme pharmaceutique disposant d'une autorisation de mise sur le marché ou d'une autorisation temporaire d'utilisation dans l'indication ou les conditions d'utilisation considérées, sous réserve qu'une recommandation temporaire d'utilisation établie par l'Agence nationale de sécurité du médicament et des produits de santé sécurise l'utilisation de cette spécialité dans cette indication ou ces conditions d'utilisation et que le prescripteur juge indispensable le recours à cette spécialité pour améliorer ou stabiliser l'état clinique de son patient. (...) / II. - Les recommandations temporaires d'utilisation mentionnées au I sont établies pour une durée maximale de trois ans, renouvelable (...) / IV. - Les recommandations temporaires d'utilisation mentionnées au I sont établies après information du titulaire de l'autorisation de mise sur le marché. / Les recommandations temporaires d'utilisation sont élaborées dans des conditions fixées par décret en Conseil d'Etat (...) ".  Selon l'article R. 5121-76-8 du même code : " En cas de suspicion de risque pour la santé publique ou en cas de manquement à l'obligation de suivi des patients et de recueil d'informations ou si le directeur général de l'agence estime que les conditions mentionnées à l'article L. 5121-12-1 ne sont plus remplies, il peut modifier, suspendre ou retirer la recommandation temporaire d'utilisation. / Sauf en cas d'urgence, la modification, la suspension ou le retrait de la recommandation ne peut intervenir qu'à l'expiration d'un délai d'un mois après réception, par le titulaire de l'autorisation de mise sur le marché de la spécialité concernée ou l'entreprise en assurant l'exploitation et qui a été mandatée à cet effet par le titulaire, d'un courrier recommandé avec accusé de réception, l'informant de l'intention de l'agence de procéder à cette modification, à cette suspension ou à ce retrait.  / La délivrance d'une autorisation de mise sur le marché ou d'une autorisation temporaire d'utilisation mentionnée au 1° du I de l'article L. 5121-12 pour une ou plusieurs indications ou conditions d'utilisation prévues par une recommandation temporaire d'utilisation met immédiatement fin, pour ces indications et conditions, à la recommandation ". Enfin, l'article R. 5121-76-9 du même code dispose que : " La recommandation temporaire d'utilisation initiale et chacune de ses mises à jour sont notifiées au titulaire de l'autorisation de mise sur le marché ou à l'entreprise qui en assure l'exploitation mandatée à cet effet par le titulaire (...) / Les recommandations font l'objet d'une publication sur le site internet de l'Agence nationale de sécurité du médicament et des produits de santé (...) ".<br/>
<br/>
              3. Il résulte de ces dispositions qu'une recommandation temporaire d'utilisation a pour objet, pendant une durée en principe limitée, de sécuriser l'utilisation d'une spécialité dans une indication ou des conditions d'utilisation autres que celles de son autorisation de mise sur le marché, en l'absence de spécialité de même principe actif, de même dosage et de même forme pharmaceutique disposant d'une autorisation de mise sur le marché ou d'une autorisation temporaire d'utilisation dans l'indication ou les conditions d'utilisation considérées. Lorsque les conditions mentionnées à l'article L. 5121-12-1 ne sont plus remplies et en particulier lorsqu'il existe désormais une spécialité de même principe actif, de même dosage et de même forme pharmaceutique disposant d'une autorisation de mise sur le marché ou d'une autorisation temporaire d'utilisation dans l'indication ou les conditions d'utilisation considérées, il appartient au directeur général de l'Agence nationale de sécurité du médicament et des produits de santé d'en tirer les conséquences en mettant fin à la recommandation temporaire d'utilisation, en appliquant la procédure définie aux premier et deuxième alinéas de l'article R. 5121-76-8, qui implique une procédure d'information préalable. Cependant, cette procédure n'est pas requise dans le cas où la spécialité faisant l'objet de la recommandation temporaire d'utilisation se voit autorisée pour l'indication ou les conditions d'utilisation prévues par la recommandation, cette dernière prenant fin de plein droit, en application du troisième alinéa de l'article R. 5121-76-8, à compter de la délivrance de l'autorisation de mise sur le marché ou de l'autorisation temporaire d'utilisation mentionnée au 1° du I de l'article L. 5121-12.<br/>
<br/>
              Sur les conclusions à fin de non-lieu : <br/>
<br/>
              4. Si, par une ordonnance du 17 juin 2020, le juge des référés du tribunal administratif de Cergy-Pontoise a suspendu l'exécution des décisions du 22 octobre 2018 accordant l'autorisation de mise sur le marché des spécialités Baclocur et indiqué que sa décision impliquait la remise en vigueur automatique de la recommandation temporaire d'utilisation du baclofène, la décision attaquée avait en tout état de cause abrogé la recommandation à compter du 15 juin 2020. Elle a donc reçu application, de sorte que l'Agence nationale de sécurité du médicament et des produits de santé n'est pas fondée à soutenir que la requête serait privée d'objet.<br/>
<br/>
              Sur la recevabilité :<br/>
<br/>
              5. La recommandation d'utilisation du baclofène a été établie pour une durée de trois ans à compter du 17 mars 2014 et renouvelée à deux reprises pour une durée d'un an. Pour les raisons indiquées au point 3, la délivrance des autorisations de mise sur le marché des spécialités Baclocur, dont le principe actif est également le baclofène, pour les mêmes indications, n'a pas mis fin par elle-même à cette recommandation temporaire d'utilisation, les autorisations ne visant pas la même spécialité que celle faisant l'objet de la recommandation temporaire d'utilisation. Si un communiqué de presse du 23 octobre 2018 avait indiqué que la recommandation temporaire d'utilisation du baclofène dans la prise en charge des patients alcoolo-dépendants ne serait pas prolongée au-delà de la commercialisation effective des spécialités Baclocur, cette échéance n'était pas connue, de sorte qu'il n'annonçait pas ce faisant qu'il y serait mis fin avant son terme et, postérieurement à ce communiqué de presse, la recommandation a été à nouveau renouvelée pour six mois jusqu'au 17 septembre 2019, puis pour une durée d'un an  jusqu'au 17 septembre 2020. Le communiqué de presse du 25 mai 2020 révèle ainsi la décision de mettre fin à la recommandation temporaire d'utilisation du baclofène avant son terme, en application du premier alinéa de l'article R. 5121-76-8 du code de la santé publique. Par suite, l'Agence nationale de sécurité du médicament et des produits de santé n'est pas fondée à soutenir que le communiqué de presse du 25 mai 2020 ne révèlerait aucune décision ni que cette décision serait purement confirmative de celle révélée par le communiqué de presse du 23 octobre 2018. Ses fins de non-recevoir ne peuvent, dès lors, qu'être écartées.<br/>
<br/>
              Sur la légalité des décisions attaquées :<br/>
<br/>
              6. La société Zentiva France ne peut utilement soutenir que la décision révélée par le communiqué de presse du 25 mai 2020 n'est pas signée, qu'elle est insuffisamment motivée ni qu'elle aurait dû prendre la forme d'une décision écrite.<br/>
<br/>
              7. Il ressort des pièces du dossier que l'Agence nationale de sécurité du médicament et des produits de santé a informé la société Zentiva France de son intention de mettre fin à la recommandation temporaire d'utilisation du baclofène par un courrier du 15 mai 2020 lui indiquant qu'elle pouvait présenter ses observations jusqu'au 1er juin 2020. La décision attaquée a été prise au plus tard le 25 mai 2020. Il n'est pas allégué qu'elle serait intervenue dans un cas d'urgence de nature à dispenser l'Agence d'attendre l'échéance qu'elle avait elle-même fixée à la société Zentiva France. Par suite, la société Zentiva France est fondée à soutenir que la décision révélée par le communiqué de presse du 25 mai 2020 est intervenue à l'issue d'une procédure irrégulière. Cependant, d'une part, l'intention de prendre une décision mettant un terme à la recommandation temporaire d'utilisation du baclofène à compter de la commercialisation des spécialités Baclocur ressortait du communiqué de presse du 23 octobre 2018, d'autre part, la décision révélée par le communiqué de presse du 25 mai 2020 n'est entrée en vigueur que le 15 juin 2020 et la société Zentiva France a pu présenter ses observations dans ce délai. Dans ces conditions, l'irrégularité de la procédure préalable à la décision révélée par le communiqué de presse du 25 mai 2020 n'a, dans les circonstances particulières de l'espèce, privé la société Zentiva France d'aucune garantie et n'a exercé aucune influence sur le sens de la décision en litige. Dès lors, elle n'est pas de nature à entraîner l'illégalité de cette décision.<br/>
<br/>
              8. Il résulte des dispositions de l'article R. 5121-76-8 du code de la santé publique citées au point 2 que le retrait d'une recommandation temporaire d'utilisation sur le fondement du premier alinéa de cet article ne peut intervenir, sauf urgence, avant l'expiration d'un délai d'un mois à compter de la réception, par le titulaire de l'autorisation de mise sur le marché de la spécialité concernée, d'un courrier recommandé avec accusé de réception, l'informant de l'intention de l'agence de procéder à ce retrait. Si l'Agence nationale de sécurité du médicament et des produits de santé a informé la société Zentiva France, par un courrier daté du 15 mai 2020, du retrait de la recommandation temporaire d'utilisation du baclofène à compter du 15 juin 2020, cette dernière soutient sans être contredite qu'elle n'a reçu ce courrier que le 19 mai 2020. En l'absence d'urgence de nature à dispenser l'Agence du respect du délai fixé par l'article R. 5121-76-8 du code de la santé publique, la société Zentiva France est fondée à soutenir que la décision attaquée a été prise en méconnaissance de ces dispositions. Cependant, eu égard aux circonstances mentionnées au point précédent et au fait que le délai d'un mois prévu par l'article R. 5121-76-8 du code de la santé publique n'a été diminué que de quatre jours, cette irrégularité n'a, dans les circonstances particulières de l'espèce, privé la société Zentiva France d'aucune garantie et n'a exercé aucune influence sur le sens de la décision en litige. Dès lors, elle n'est pas de nature à entraîner l'illégalité de cette décision.<br/>
<br/>
              9. Aux termes de l'article R. 5121-76-9 du code de la santé publique : " La recommandation temporaire d'utilisation initiale et chacune de ses mises à jour sont notifiées au titulaire de l'autorisation de mise sur le marché ou à l'entreprise qui en assure l'exploitation mandatée à cet effet par le titulaire. Le titulaire de l'autorisation de mise sur le marché ou l'entreprise qui en assure l'exploitation diffuse auprès des prescripteurs la recommandation temporaire d'utilisation initiale et chacune de ses mises à jour. Les mesures prises pour cette diffusion sont soumises à l'avis préalable de l'agence et ne doivent pas constituer une publicité au sens de l'article L. 5122-1 ". La société requérante ne peut utilement soutenir que la décision révélée par le communiqué de presse du 25 mai 2020 constituerait une communication promotionnelle en faveur du Baclocur et méconnaîtrait pour ce motif les dispositions de l'article R. 5121-76-9 du code de la santé publique, celles-ci ne visant que les mesures prises par l'exploitant de la spécialité faisant l'objet de la recommandation temporaire d'utilisation pour en assurer la diffusion auprès des prescripteurs.<br/>
<br/>
              10. Ainsi qu'il a été dit au point 3, lorsque les conditions mentionnées à l'article L. 5121-12-1 du code de la santé publique pour qu'une recommandation temporaire d'utilisation soit établie ne sont plus remplies et en particulier lorsqu'il existe désormais une spécialité de même principe actif, de même dosage et de même forme pharmaceutique disposant d'une autorisation de mise sur le marché ou d'une autorisation temporaire d'utilisation dans l'indication ou les conditions d'utilisation considérées, il appartient au directeur général de l'Agence nationale de sécurité du médicament et des produits de santé d'en tirer les conséquences en mettant fin à la recommandation temporaire d'utilisation. Par suite, la société Zentiva France n'est pas fondée à soutenir que la décision de mettre fin à la recommandation temporaire d'utilisation du baclofène au motif de la commercialisation des spécialités Baclocur résulterait d'une inexacte application de l'article L. 5121-12-1 du code de la santé publique.<br/>
<br/>
              11. Il ne ressort pas des pièces du dossier que des spécialités Baclocur n'auraient pas été effectivement commercialisée à compter du 15 juin 2020, date à laquelle la décision attaquée a mis fin à la recommandation temporaire d'utilisation du baclofène. Le moyen tiré de l'erreur dont serait entachée la décision attaquée sur ce point doit donc être écartée.<br/>
<br/>
              12. La société Zentiva France ne peut utilement soutenir que la décision du 29 mai 2020 rejetant son recours gracieux contre la décision révélée par le communiqué de presse du 25 mai 2020, qui ne repose pas sur une interprétation inexacte de la portée de cette dernière décision, serait insuffisamment motivée.<br/>
<br/>
              13. Il résulte de tout ce qui précède que la société Zentiva France n'est pas fondée à demander l'annulation de la décision révélée par le communiqué de presse du 25 mai 2020, ainsi que de celle rejetant son recours gracieux contre cette décision.<br/>
<br/>
              Sur les conclusions aux fins d'injonction :<br/>
<br/>
              14. Les conclusions de la société Zentiva France à fin d'annulation étant rejetées, ses conclusions à fin d'injonction ne peuvent qu'être également rejetées.<br/>
<br/>
              Sur les frais d'instance :<br/>
<br/>
              15. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante. Il n'y a pas lieu de mettre à la charge de la société requérante la somme que demande l'Agence nationale de sécurité du médicament et des produits de santé au titre des mêmes dispositions.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la société Zentiva France est rejetée.<br/>
Article 2 : Les conclusions de l'Agence nationale de sécurité du médicament et des produits de santé présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à la société Zentiva France, à l'Agence nationale de sécurité du médicament et des produits de santé et au ministre des solidarités et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">61-04-01-01 SANTÉ PUBLIQUE. PHARMACIE. PRODUITS PHARMACEUTIQUES. AUTORISATIONS DE MISE SUR LE MARCHÉ. - RECOMMANDATION TEMPORAIRE D'UTILISATION (RTU) - CONDITIONS N'ÉTANT PLUS REMPLIES - 1) PRINCIPE - OBLIGATION, POUR L'ANSM, D'Y METTRE FIN EN APPLIQUANT LA PROCÉDURE D'INFORMATION PRÉALABLE - 2) EXCEPTION - DÉLIVRANCE D'UNE AMM OU D'UNE ATU POUR LA SPÉCIALITÉ FAISANT L'OBJET DE LA RTU POUR LA MÊME INDICATION.
</SCT>
<ANA ID="9A"> 61-04-01-01 1) Il résulte des articles L. 5121-12-1, R. 5121 76-8 et R. 5121-76-9 du code de la santé publique  (CSP) qu'une recommandation temporaire d'utilisation (RTU) a pour objet, pendant une durée en principe limitée, de sécuriser l'utilisation d'une spécialité dans une indication ou des conditions d'utilisation autres que celles de son autorisation de mise sur le marché (AMM), en l'absence de spécialité de même principe actif, de même dosage et de même forme pharmaceutique disposant d'une AMM ou d'une autorisation temporaire d'utilisation (ATU) dans l'indication ou les conditions d'utilisation considérées. Lorsque les conditions mentionnées à l'article L. 5121-12-1 ne sont plus remplies et en particulier lorsqu'il existe désormais une spécialité de même principe actif, de même dosage et de même forme pharmaceutique disposant d'une AMM ou d'une autorisation temporaire d'utilisation dans l'indication ou les conditions d'utilisation considérées, il appartient au directeur général de l'Agence nationale de sécurité du médicament et des produits de santé (ANSM) d'en tirer les conséquences en mettant fin à la RTU, en appliquant la procédure définie aux premier et deuxième alinéas de l'article R. 5121-76-8, qui implique une procédure d'information préalable.... ,,2) Cependant, cette procédure n'est pas requise dans le cas où la spécialité faisant l'objet de la RTU se voit autorisée pour l'indication ou les conditions d'utilisation prévues par la recommandation, cette dernière prenant fin de plein droit, en application du troisième alinéa de l'article R. 5121-76-8, à compter de la délivrance de l'AMM ou de l'ATU mentionnée au 1° du I de l'article L. 5121-12.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
