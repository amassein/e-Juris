<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029781213</ID>
<ANCIEN_ID>JG_L_2014_11_000000359223</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/78/12/CETATEXT000029781213.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème / 9ème SSR, 19/11/2014, 359223</TITRE>
<DATE_DEC>2014-11-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>359223</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème / 9ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Anne Iljic</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:359223.20141119</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 7 mai 2012 au secrétariat du contentieux du Conseil d'Etat, présentée par M. B...A..., demeurant ... ; M. A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite de rejet résultant du silence gardé par le ministre de l'intérieur, de l'outre-mer, des collectivités territoriales et de l'immigration sur sa demande, présentée le 27 février 2012, tendant à l'abrogation du décret n° 70-708 du 31 juillet 1970 portant application du titre Ier et de certaines dispositions du titre II de la loi n° 69-3 du 3 janvier 1969 relative à l'exercice des activités ambulantes et au régime applicable aux personnes circulant en France sans domicile ni résidence fixe ;<br/>
<br/>
              2°) d'enjoindre au Premier ministre d'abroger le décret n° 70-708 du 31 juillet 1970 ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu la Constitution, notamment son Préambule et son article 62 ; <br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et son quatrième protocole additionnel ;<br/>
<br/>
              Vu le pacte international relatif aux droits civils et politiques ;<br/>
<br/>
              Vu la convention internationale relative aux droits de l'enfant ;<br/>
<br/>
              Vu le traité sur le fonctionnement de l'Union européenne ;<br/>
<br/>
              Vu la loi n° 69-3 du 3 janvier 1969 ;<br/>
<br/>
              Vu la loi n° 79-587 du 11 juillet 1979 ;<br/>
<br/>
              Vu le décret n° 70-708 du 31 juillet 1970 ;<br/>
<br/>
              Vu la décision du 17 juillet 2012 par laquelle le Conseil d'Etat statuant au contentieux a renvoyé au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par M.A... ;<br/>
<br/>
              Vu la décision n° 2012-279 QPC du 5 octobre 2012 statuant sur la question prioritaire de constitutionnalité soulevée par M.A... ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Anne Iljic, auditeur,  <br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que, par sa décision n° 2012-279 QPC du 5 octobre 2012, le Conseil constitutionnel a déclaré contraires à la Constitution certaines dispositions de la loi du 3 janvier 1969 relative à l'exercice des activités ambulantes et au régime applicable aux personnes circulant en France sans domicile ni résidence fixe, notamment celles relatives au carnet de circulation, et qu'il a, dans cette même décision, jugé que cette déclaration d'inconstitutionnalité prenait effet à compter de la date de la publication de sa décision et qu'elle était applicable à toutes les affaires non jugées définitivement à cette date ; que s'il appartient au juge, saisi d'un litige relatif aux effets produits par des dispositions déclarées inconstitutionnelles, de les remettre en cause en écartant, pour la solution de ce litige, le cas échéant d'office, ces dispositions, dans les conditions et limites fixées par le Conseil constitutionnel, il ne peut le faire que dans les limites des conclusions recevables dont il est saisi ; que M.A..., qui se prévaut de sa profession de forain et de sa qualité de titulaire d'un livret spécial de circulation, ne justifie d'un intérêt à contester le décret du 31 juillet 1970 portant application du titre Ier et de certaines dispositions du titre II de la loi du 3 janvier 1969 qu'en tant que celui-ci régit la situation des personnes devant être munies, en application de l'article 2 de la loi du 3 janvier 1969, d'un livret spécial de circulation ; que, par suite, sa requête n'est recevable qu'en tant qu'elle porte sur les dispositions du décret régissant la situation de ces personnes, sans qu'ait d'incidence, à cet égard, la décision du 5 octobre 2012 du Conseil constitutionnel mentionnée précédemment ; qu'en tant qu'elle porte sur les dispositions du décret régissant la situation des personnes devant, en application de la loi du 3 janvier 1969, être munies d'un titre de circulation autre que le livret spécial, la requête n'est pas recevable et doit, dans cette mesure, être rejetée ;<br/>
<br/>
              2. Considérant, en premier lieu, qu'aucun texte, et notamment pas les articles 1er et 2 de la loi du 11 juillet 1979 relative à la motivation des actes administratifs et à l'amélioration des relations entre l'administration et le public, ni aucun principe, n'imposait que la décision qu'attaque M. A...fût motivée; que, par suite, M. A...ne peut utilement soutenir que la décision implicite rejetant sa demande d'abrogation du décret du 31 juillet 1970 est entachée d'un défaut de motivation ;<br/>
<br/>
              3. Considérant, en deuxième lieu, que si le requérant soutient que l'existence du livret spécial de circulation est contraire aux articles 3 de la Constitution et 6 de la Déclaration des droits de l'homme et du citoyen de 1789, l'existence de ce titre de circulation est prévue par les dispositions de l'article 2 de la loi du 3 janvier 1969 que le Conseil constitutionnel a jugées conformes à la Constitution dans la décision n° 2012-279 QPC du 5 octobre 2012 par laquelle il statué sur la question prioritaire de constitutionnalité soulevée par M.A... ; que M. A... ne saurait par suite utilement soutenir que l'existence du livret spécial de circulation, prévue par les dispositions de l'article 2 de la loi du 3 janvier 1969, méconnaît un principe général du droit ;<br/>
<br/>
              4. Considérant, en troisième lieu, qu'en imposant aux personnes se trouvant en France sans domicile ni résidence fixe depuis plus de six mois d'être munies d'un livret spécial de circulation, le législateur a entendu permettre, à des fins civiles, sociales, administratives ou judiciaires, l'identification et la recherche de ceux qui ne peuvent être trouvés à un domicile ou à une résidence fixe d'une certaine durée, tout en assurant, aux mêmes fins, un moyen de communiquer avec ceux-ci ; que ces dispositions sont fondées sur une différence de situation entre les personnes, quelles que soient leurs nationalités et leurs origines, qui ont un domicile ou une résidence fixe de plus de six mois et celles qui en sont dépourvues ; qu'ainsi, la distinction qu'elles opèrent repose sur des critères objectifs et rationnels en rapport direct avec le but que s'est assigné le législateur ; qu'elles n'instituent aucune discrimination fondée sur une origine ethnique ; que l'atteinte portée à la liberté d'aller et de venir qui en résulte est justifiée par la nécessité de protéger l'ordre public et proportionnée à cet objectif ; que, par suite, en imposant aux personnes visées d'être porteuses d'un titre de circulation, le législateur n'a méconnu ni l'article 14 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, qui interdit toute discrimination, ni l'article 2 du quatrième protocole additionnel à cette convention, qui garantit la liberté de circulation ;<br/>
<br/>
              5. Considérant, en revanche, que les dispositions de l'article 10 du décret du 31 juillet 1970, qui punissent d'une amende prévue pour les contraventions de 5ème classe les personnes qui circuleraient sans s'être fait délivrer un livret spécial de circulation, et les dispositions de l'article 12 de ce même décret, qui punissent d'une amende prévue pour les contraventions de 4ème classe les personnes qui ne pourraient justifier à toute réquisition des officiers ou agents de police judiciaire ou des agents de la force ou de l'autorité publique, de la possession d'un livret spécial de circulation, portent à l'exercice de la liberté de circulation, garantie par l'article 2 du quatrième protocole additionnel à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, une atteinte disproportionnée au regard du but poursuivi ; que, dans ces conditions, la décision implicite rejetant la demande de M. A...tendant à l'abrogation du décret du 31 juillet 1970 est entachée d'illégalité en ce que celle-ci refuse l'abrogation, en tant qu'ils sont applicables aux personnes devant être munies d'un livret spécial de circulation, des articles 10 et 12 du décret ;<br/>
<br/>
              6. Considérant, en quatrième lieu, qu'aucune disposition du décret du 31 juillet 1970 n'est relative au droit de vote ; qu'il s'ensuit que le moyen tiré de la méconnaissance l'article 25 du pacte international relatif aux droits civils et politiques, qui garantit le droit de voter et d'être élu, est inopérant ;<br/>
<br/>
              7. Considérant, en cinquième lieu, que M. A...ne saurait utilement invoquer la méconnaissance, par les dispositions du décret du 31 juillet 1970 et de la loi du 3 janvier 1969, des stipulations du 2 de l'article 2 de la convention internationale relative aux droits de l'enfant, qui sont dépourvues d'effet direct ; <br/>
<br/>
              8. Considérant, en sixième lieu, que si M. A...invoque la méconnaissance de la liberté de circulation des personnes garantie par le droit de l'Union européenne, il n'assortit pas ce moyen des précisions permettant d'en apprécier le bien-fondé ;<br/>
<br/>
              9. Considérant qu'il résulte de tout ce qui précède que M. A... n'est fondé à demander l'annulation de la décision implicite rejetant sa demande d'abrogation du décret du 31 juillet 1970 qu'en tant seulement que celle-ci refuse l'abrogation, en tant qu'ils sont applicables aux personnes devant être munies d'un livret spécial de circulation, des articles 10 et 12 du décret ; que cette annulation implique nécessairement l'abrogation des dispositions réglementaires dont l'illégalité a été constatée ; qu'il y a lieu pour le Conseil d'Etat d'ordonner cette mesure ; <br/>
<br/>
              10. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 1 000 euros à verser à M. A..., au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : La décision implicite refusant l'abrogation du décret du 31 juillet 1970 est annulée en ce qu'elle a refusé l'abrogation, en tant qu'ils sont applicables aux personnes devant être munies d'un livret spécial de circulation, des articles 10 et 12 de ce décret.<br/>
Article 2 : Il est enjoint au Premier ministre d'abroger, en tant qu'ils sont applicables aux personnes devant être munies d'un livret spécial de circulation, les articles 10 et 12 du décret 31 juillet 1970, dans le délai de deux mois à compter de la notification de la présente décision.<br/>
Article 3 : L'Etat versera à M. A...la somme de 1 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Le surplus des conclusions de la requête de M. A...est rejeté.<br/>
Article 5 : La présente décision sera notifiée à M. B...A..., au Premier ministre et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">26-03-05 DROITS CIVILS ET INDIVIDUELS. LIBERTÉS PUBLIQUES ET LIBERTÉS DE LA PERSONNE. LIBERTÉ D'ALLER ET VENIR. - LIVRET SPÉCIAL DE CIRCULATION - CONTRAVENTIONS DE 4E ET DE 5E CLASSE PRÉVUES RESPECTIVEMENT EN CAS DE NON PRÉSENTATION DU LIVRET ET DE NON POSSESSION DU LIVRET - ATTEINTE DISPROPORTIONNÉE À LA LIBERTÉ DE CIRCULATION AU REGARD DU BUT POURSUIVI.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">59-02 RÉPRESSION. DOMAINE DE LA RÉPRESSION ADMINISTRATIVE - LIVRET SPÉCIAL DE CIRCULATION - CONTRAVENTIONS DE 4E ET DE 5E CLASSE PRÉVUES RESPECTIVEMENT EN CAS DE NON PRÉSENTATION DU LIVRET ET DE NON POSSESSION DU LIVRET - ATTEINTE DISPROPORTIONNÉE À LA LIBERTÉ DE CIRCULATION AU REGARD DU BUT POURSUIVI.
</SCT>
<ANA ID="9A"> 26-03-05 Les dispositions de l'article 10 du décret du 31 juillet 1970, qui punissent d'une amende prévue pour les contraventions de 5ème classe les personnes qui circuleraient sans s'être fait délivrer un livret spécial de circulation, et les dispositions de l'article 12 de ce même décret, qui punissent d'une amende prévue pour les contraventions de 4ème  classe les personnes qui ne pourraient justifier à toute réquisition des officiers ou agents de police judiciaire ou des agents de la force ou de l'autorité publique, de la possession d'un livret spécial de circulation, portent à l'exercice de la liberté de circulation, garantie par l'article 2 du quatrième protocole additionnel à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales (CEDH), une atteinte disproportionnée au regard du but poursuivi.</ANA>
<ANA ID="9B"> 59-02 Les dispositions de l'article 10 du décret du 31 juillet 1970, qui punissent d'une amende prévue pour les contraventions de 5ème classe les personnes qui circuleraient sans s'être fait délivrer un livret spécial de circulation, et les dispositions de l'article 12 de ce même décret, qui punissent d'une amende prévue pour les contraventions de 4ème  classe les personnes qui ne pourraient justifier à toute réquisition des officiers ou agents de police judiciaire ou des agents de la force ou de l'autorité publique, de la possession d'un livret spécial de circulation, portent à l'exercice de la liberté de circulation, garantie par l'article 2 du quatrième protocole additionnel à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales (CEDH), une atteinte disproportionnée au regard du but poursuivi.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
