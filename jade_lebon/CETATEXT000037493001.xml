<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037493001</ID>
<ANCIEN_ID>JG_L_2018_10_000000412104</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/49/30/CETATEXT000037493001.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 12/10/2018, 412104</TITRE>
<DATE_DEC>2018-10-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>412104</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO ; SCP THOUIN-PALAT, BOUCARD</AVOCATS>
<RAPPORTEUR>M. Yves Doutriaux</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Guillaume Odinet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:412104.20181012</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La commune de Vesly a demandé au tribunal administratif de Rouen d'annuler pour excès de pouvoir le permis de construire que le préfet de l'Eure a délivré le 12 décembre 2011 à la société Juwi EnR en vue de l'édification de quatre éoliennes et d'un poste de livraison électrique sur le territoire de cette commune. Par un jugement n° 1201612 du 27 mai 2014, le tribunal administratif a annulé ce permis.<br/>
<br/>
              Par un arrêt n° 14DA01300 du 4 mai 2017, la cour administrative d'appel de Douai a rejeté l'appel formé par la société Juwi EnR, devenue Néoen Développement, contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 4 juillet et 3 octobre 2017 et le 24 septembre 2018 au secrétariat du contentieux du Conseil d'Etat, la société Néoen, venant aux droits de la société Néoen Développement, demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
              3°) de mettre à la charge de la commune de Vesly la somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'environnement ;<br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Yves Doutriaux, conseiller d'Etat,<br/>
<br/>
              - les conclusions de M. Guillaume Odinet, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de la société Néoen, et à la SCP Thouin-Palat, Boucard, avocat de la commune de Vesly ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'il ressort des énonciations de l'arrêt attaqué que la société Juwi EnR a déposé, le 8 juin 2007, une demande de permis de construire un parc éolien composé de quatre aérogénérateurs et d'un poste de livraison électrique à édifier sur le territoire de la commune de Vesly (Eure) ; que ce permis de construire a été refusé par le préfet de l'Eure le 18 janvier 2008 ; que, par un jugement du 4 novembre 2010, devenu définitif, le tribunal administratif de Rouen a annulé ce refus ; que, par un nouvel arrêté du 12 décembre 2011, le préfet de l'Eure a accordé le permis de construire sollicité ; qu'à la demande de la commune de Vesly, le tribunal administratif de Rouen a, par un jugement du 27 mai 2014, prononcé l'annulation pour excès de pouvoir du permis ainsi délivré ; que la cour administrative d'appel de Douai, par un arrêt du 4 mai 2017, a rejeté l'appel formé par la société Juwi EnR, devenue la société Néoen Développement, contre ce jugement ; que la société Néoen, venue aux droits de la société Néoen Développement, se pourvoit en cassation contre cet arrêt ; <br/>
<br/>
              2.	Considérant que, pour annuler pour excès de pouvoir le refus initial de permis de construire, le tribunal administratif de Rouen, par son jugement du 4 novembre 2010 devenu définitif, s'est notamment fondé sur les motifs tirés de ce que, si le projet d'implantation des quatre éoliennes et le château de Château-sur-Epte, classé au titre des monuments historiques et inscrit au titre des sites, sont distants d'environ 2,5 km et sont covisibles depuis le nord-ouest du site ainsi que depuis les abords du château, il ressort des pièces du dossier que l'impact visuel est faible et n'est pas de nature à porter atteinte au caractère ou à l'intérêt du site et que, si le projet en litige est situé à environ 1 500 m du périmètre du site classé de la vallée de l'Epte, il ressort des pièces du dossier que son incidence sur la qualité paysagère de ce site serait faible, les éoliennes n'étant visibles que depuis les collines situées au nord du site classé ; que le tribunal administratif en a déduit que le préfet avait commis une erreur d'appréciation en retenant que le projet était de nature à porter atteinte au caractère ou à l'intérêt des lieux avoisinants et en refusant, par suite, le permis de construire sollicité ; <br/>
<br/>
              3.	Considérant que l'autorité de chose jugée s'attachant au dispositif de ce jugement d'annulation devenu définitif ainsi qu'aux motifs qui en sont le support nécessaire faisait obstacle à ce que, en l'absence de modification de la situation de droit ou de fait, le permis de construire sollicité soit à nouveau refusé par l'autorité administrative ou que le permis accordé soit annulé par le juge administratif, pour un motif identique à celui qui avait été censuré par le tribunal administratif ; <br/>
<br/>
              4.	Considérant que la cour administrative d'appel de Douai, pour juger illégal le permis de construire délivré par le préfet le 12 décembre 2011, s'est fondée sur des documents produits dans l'instance dirigée contre ce permis et qui n'avaient pas été produits dans l'instance antérieure ayant conduit au jugement du tribunal administratif du 4 novembre 2010, pour retenir que des éléments nouveaux font apparaître une forte covisibilité entre trois des quatre éoliennes dans des proportions non négligeables, en particulier avec le monument historique constitué de l'église Saint-Maurice et de son clocher, que d'autres photomontages, en dépit de leur faible nombre, et les commentaires de l'étude d'impact " font ressortir une forte prégnance visuelle du parc éolien à partir de la vallée de l'Epte notamment des chemins de randonnées qui la traversent" et que, ainsi que l'autorité environnementale l'avait analysé initialement, l'étude paysagère avait été jusque-là " davantage axée sur la visibilité du parc éolien depuis les axes routiers que depuis le site classé " ; qu'elle s'est fondée sur ces éléments pour juger que le projet de construction traduisait un défaut " d'harmonie avec le site ", au sens des dispositions alors applicables du plan d'occupation des sols, et portait atteinte au caractère ou à l'intérêt des lieux avoisinants, aux sites, aux paysages naturels ou urbains au regard de l'article R. 111-21 du code de l'urbanisme ; <br/>
<br/>
              5.	Considérant qu'en s'affranchissant ainsi, pour annuler le permis accordé par le préfet le 12 décembre 2011, de l'autorité de la chose jugée s'attachant au jugement définitif du tribunal administratif de Rouen du 4 novembre 2010 sans relever aucun changement qui aurait affecté la réalité de la situation de fait, tenant notamment à la consistance ou à l'implantation du projet, mais en se bornant à prendre en compte d'autres documents que ceux qui avaient été soumis au tribunal administratif dans l'instance portant sur le refus de permis, la cour administrative d'appel de Douai a commis une erreur de droit ; que, par suite, la société Néoen est fondée, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, à demander l'annulation de l'arrêt qu'elle attaque ; <br/>
<br/>
              6.	Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de Vesly la somme de 3 000 euros à verser à la société Néoen au titre de l'article L. 761-1 du code de justice administrative ; que les dispositions de l'article L. 761-1 du code de justice administrative font, en revanche, obstacle à ce qu'une somme soit mise, à ce titre, à la charge de la société Néoen, qui n'est pas la partie perdante dans la présente instance ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
                                          --------------<br/>
<br/>
Article 1er : L'arrêt du 4 mai 2017 de la cour administrative d'appel de Douai est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administration d'appel de Douai.<br/>
<br/>
Article 3 : La commune de Vesly versera la somme de 3 000 euros à la société Néoen au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : Les conclusions de la commune de Vesly au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 5 : La présente décision sera notifiée à la société Néoen, à la commune de Vesly et au ministre de la cohésion des territoires.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-04-04-02 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. CHOSE JUGÉE. CHOSE JUGÉE PAR LE JUGE ADMINISTRATIF. - PORTÉE DE L'AUTORITÉ ABSOLUE DE LA CHOSE JUGÉE PAR UN JUGEMENT, DEVENU DÉFINITIF, ANNULANT UN REFUS DE PERMIS DE CONSTRUIRE - 1) PRINCIPE - AUTORITÉ FAISANT OBSTACLE À CE QUE LE PERMIS SOIT À NOUVEAU REFUSÉ OU QUE LE PERMIS ACCORDÉ SOIT ANNULÉ, POUR UN MOTIF IDENTIQUE À CELUI QUI AVAIT ÉTÉ CENSURÉ - EXISTENCE, EN L'ABSENCE DE MODIFICATION DE LA SITUATION DE DROIT OU DE FAIT [RJ1] - 2) ESPÈCE - ANNULATION D'UN PERMIS, AU MOTIF QUE LE PROJET PORTAIT ATTEINTE AU CARACTÈRE OU À L'INTÉRÊT DES LIEUX (ANCIEN ART. R. 111-21, DEVENU R. 111-27 DU CODE DE L'URBANISME), APRÈS L'ANNULATION, PAR UN JUGEMENT DEVENU DÉFINITIF, DU REFUS OPPOSÉ À CE MÊME PROJET, AU MOTIF QU'IL N'Y PORTAIT PAS ATTEINTE - ANNULATION RÉSULTANT DE LA SEULE PRISE EN COMPTE D'AUTRES DOCUMENTS QUE CEUX SOUMIS DANS L'INSTANCE PORTANT SUR LE REFUS - VIOLATION DE L'AUTORITÉ ABSOLUE DE LA CHOSE JUGÉE - EXISTENCE, EN L'ABSENCE DE CHANGEMENT QUI AURAIT AFFECTÉ LA RÉALITÉ DE LA SITUATION DE FAIT.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-06-06-01-03 PROCÉDURE. JUGEMENTS. CHOSE JUGÉE. CHOSE JUGÉE PAR LA JURIDICTION ADMINISTRATIVE. EFFETS. - PORTÉE DE L'AUTORITÉ ABSOLUE DE LA CHOSE JUGÉE PAR UN JUGEMENT, DEVENU DÉFINITIF, ANNULANT UN REFUS DE PERMIS DE CONSTRUIRE - 1) PRINCIPE - AUTORITÉ FAISANT OBSTACLE À CE QUE LE PERMIS SOIT À NOUVEAU REFUSÉ OU QUE LE PERMIS ACCORDÉ SOIT ANNULÉ, POUR UN MOTIF IDENTIQUE À CELUI QUI AVAIT ÉTÉ CENSURÉ - EXISTENCE, EN L'ABSENCE DE MODIFICATION DE LA SITUATION DE DROIT OU DE FAIT [RJ1] - 2) ESPÈCE - ANNULATION D'UN PERMIS, AU MOTIF QUE LE PROJET PORTAIT ATTEINTE AU CARACTÈRE OU À L'INTÉRÊT DES LIEUX (ANCIEN ART. R. 111-21, DEVENU R. 111-27 DU CODE DE L'URBANISME), APRÈS L'ANNULATION, PAR UN JUGEMENT DEVENU DÉFINITIF, DU REFUS OPPOSÉ À CE MÊME PROJET, AU MOTIF QU'IL N'Y PORTAIT PAS ATTEINTE - ANNULATION RÉSULTANT DE LA SEULE PRISE EN COMPTE D'AUTRES DOCUMENTS QUE CEUX SOUMIS DANS L'INSTANCE PORTANT SUR LE REFUS - VIOLATION DE L'AUTORITÉ ABSOLUE DE LA CHOSE JUGÉE - EXISTENCE, EN L'ABSENCE DE CHANGEMENT QUI AURAIT AFFECTÉ LA RÉALITÉ DE LA SITUATION DE FAIT.
</SCT>
<ANA ID="9A"> 01-04-04-02 1) L'autorité de chose jugée s'attachant au dispositif d'un jugement, devenu définitif, annulant un refus de permis de construire ainsi qu'aux motifs qui en sont le support nécessaire fait obstacle à ce que, en l'absence de modification de la situation de droit ou de fait, le permis de construire sollicité soit à nouveau refusé par l'autorité administrative ou que le permis accordé soit annulé par le juge administratif, pour un motif identique à celui qui avait été censuré par le tribunal administratif.,,2) Annulation d'un refus de permis de construire, par un jugement devenu définitif, au motif de l'erreur d'appréciation commise par l'autorité administrative en retenant que le projet était de nature à porter atteinte au caractère ou à l'intérêt des lieux avoisinants, suivi de la délivrance par l'autorité administrative du permis de construire sollicité, annulé par une cour, au motif que le projet était de nature à porter atteinte au caractère ou à l'intérêt des lieux avoisinants.,,En s'affranchissant ainsi, pour annuler le permis accordé, de l'autorité de la chose jugée s'attachant au jugement définitif sans relever aucun changement qui aurait affecté la réalité de la situation de fait, tenant notamment à la consistance ou à l'implantation du projet, mais en se bornant à prendre en compte d'autres documents que ceux qui avaient été soumis au tribunal dans l'instance portant sur le refus de permis, une cour commet une erreur de droit.</ANA>
<ANA ID="9B"> 54-06-06-01-03 1) L'autorité de chose jugée s'attachant au dispositif d'un jugement, devenu définitif, annulant un refus de permis de construire ainsi qu'aux motifs qui en sont le support nécessaire fait obstacle à ce que, en l'absence de modification de la situation de droit ou de fait, le permis de construire sollicité soit à nouveau refusé par l'autorité administrative ou que le permis accordé soit annulé par le juge administratif, pour un motif identique à celui qui avait été censuré par le tribunal administratif.,,2) Annulation d'un refus de permis de construire, par un jugement devenu définitif, au motif de l'erreur d'appréciation commise par l'autorité administrative en retenant que le projet était de nature à porter atteinte au caractère ou à l'intérêt des lieux avoisinants, suivi de la délivrance par l'autorité administrative du permis de construire sollicité, annulé par une cour, au motif que le projet était de nature à porter atteinte au caractère ou à l'intérêt des lieux avoisinants.,,En s'affranchissant ainsi, pour annuler le permis accordé, de l'autorité de la chose jugée s'attachant au jugement définitif sans relever aucun changement qui aurait affecté la réalité de la situation de fait, tenant notamment à la consistance ou à l'implantation du projet, mais en se bornant à prendre en compte d'autres documents que ceux qui avaient été soumis au tribunal dans l'instance portant sur le refus de permis, une cour commet une erreur de droit.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 17 novembre 1961,,, n° 35011, T. pp. 1138-1144 ; CE, 27 octobre 1976,,, n° 99365, p. 445 ; CE, 12 juillet 1978, Ministre de l'agriculture c/,, n° 7750, T. pp. 690-705 ; CE, 18 mars 1983, Ministre de l'environnement et du cadre de vie c/ SCI Résidence du parc, n° 20208, p. 126.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
