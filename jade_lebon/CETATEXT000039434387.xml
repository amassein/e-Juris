<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039434387</ID>
<ANCIEN_ID>JG_L_2019_11_000000421050</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/43/43/CETATEXT000039434387.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 29/11/2019, 421050</TITRE>
<DATE_DEC>2019-11-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>421050</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS</AVOCATS>
<RAPPORTEUR>M. Fabio Gennari</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Guillaume Odinet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:421050.20191129</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B... A... a demandé au tribunal administratif de Nantes d'annuler la décision du 12 décembre 2013 par laquelle le ministre de l'intérieur a rejeté sa demande de naturalisation. Par un jugement n° 1400733 du 19 avril 2016, le tribunal administratif de Nantes a rejeté sa demande. <br/>
<br/>
              Par un arrêt n° 16NT03451 du 16 mars 2018, la cour administrative d'appel de Nantes a rejeté l'appel qu'elle a formé contre ce jugement.  <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 29 mai et 29 août 2018 au secrétariat du contentieux du Conseil d'Etat, Mme A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros à verser à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, son avocat, au titre des articles 37 de la loi du 10 juillet 1991 et L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code civil ; <br/>
              - le décret n° 93-1362 du 30 décembre 1993 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Fabio Gennari, auditeur, <br/>
<br/>
              - les conclusions de M. Guillaume Odinet, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de Mme A... ;  <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1.	Aux termes de l'article 21-15 du code civil : " (...) l'acquisition de la nationalité française par décision de l'autorité publique résulte d'une naturalisation accordée par décret à la demande de l'étranger ". Aux termes de l'article 48 du décret du 30 décembre 1993 relatif aux déclarations de nationalité, aux décisions de naturalisation, de réintégration, de perte, de déchéance et de retrait de la nationalité française : " (...) Si le ministre chargé des naturalisations estime qu'il n'y a pas lieu d'accorder la naturalisation ou la réintégration sollicitée, il prononce le rejet de la demande. Il peut également prononcer l'ajournement en imposant un délai ou des conditions (...) ".<br/>
<br/>
              2.	L'autorité administrative dispose, en matière de naturalisation ou de réintégration dans la nationalité française, d'un large pouvoir d'appréciation. Elle peut, dans l'exercice de ce pouvoir, prendre en considération notamment, pour apprécier l'intérêt que présenterait l'octroi de la nationalité française, l'intégration de l'intéressé dans la société française, son insertion sociale et professionnelle et le fait qu'il dispose de ressources lui permettant de subvenir durablement à ses besoins en France. Pour rejeter une demande de naturalisation ou de réintégration dans la nationalité française, l'autorité administrative ne peut se fonder ni sur l'existence d'une maladie ou d'un handicap ni, par suite, sur l'insuffisance des ressources de l'intéressé lorsqu'elle résulte directement d'une maladie ou d'un handicap.<br/>
<br/>
              3.	En premier lieu, pour rejeter l'appel formé par Mme A..., la cour administrative d'appel de Nantes a relevé que le ministre s'était fondé, pour rejeter, le 12 décembre 2013, la demande de naturalisation de Mme A..., sur le motif tiré de ce qu'elle ne disposait pas de revenus personnels et ne subvenait à ses besoins qu'à l'aide de prestations sociales. Elle a constaté que les ressources de l'intéressée n'étaient constituées, à la date de la décision contestée, que d'une retraite personnelle au titre de l'inaptitude au travail, de l'allocation de solidarité pour personnes âgées, d'une allocation de retraite complémentaire et d'une prestation d'aide sociale facultative versée par le centre d'action sociale de la ville de Paris. En jugeant, au vu de ces constatations souveraines, que le ministre avait pu, sans illégalité, opposer à l'intéressée la nature de ses ressources, dont la faiblesse ne résulte pas directement d'une maladie ou d'un handicap, la cour n'a pas commis d'erreur de droit.<br/>
<br/>
              4.	En second lieu, la cour administrative d'appel, en constatant que les ressources de Mme A... étaient constituées pour l'essentiel d'allocations ou de prestations sociales qui n'ont pas le caractère d'allocations accordées en compensation d'un handicap et en retenant qu'elle ne pouvait subvenir à ses propres besoins indépendamment des prestations sociales qu'elle perçoit, s'est livrée à une appréciation souveraine des faits de l'espèce exempte de dénaturation.<br/>
<br/>
              5.	Il résulte de ce qui précède que Mme A... n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque. Les dispositions de l'article L. 761-1 du code de justice administrative font alors obstacle à ce qu'une somme soit mise, sur le fondement de l'article 37 de la loi du 10 juillet 1991, à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le pourvoi de Mme A... est rejeté.<br/>
<br/>
Article 2 : La présente décision sera notifiée à Mme B... A... et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">26-01-01-01-03 DROITS CIVILS ET INDIVIDUELS. ÉTAT DES PERSONNES. NATIONALITÉ. ACQUISITION DE LA NATIONALITÉ. NATURALISATION. - REFUS DE NATURALISATION FONDÉ SUR L'INSUFFISANCE DES RESSOURCES LORSQU'ELLE RÉSULTE DIRECTEMENT D'UNE MALADIE OU D'UN HANDICAP - ILLÉGALITÉ - EXISTENCE [RJ1].
</SCT>
<ANA ID="9A"> 26-01-01-01-03 Pour rejeter une demande de naturalisation ou de réintégration dans la nationalité française, l'autorité administrative ne peut se fonder ni sur l'existence d'une maladie ou d'un handicap ni, par suite, sur l'insuffisance des ressources de l'intéressé lorsqu'elle résulte directement d'une maladie ou d'un handicap.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., en étendant, CE, 11 mai 2016, MM.,, n°s 389399 389433, T. pp. 754-762.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
