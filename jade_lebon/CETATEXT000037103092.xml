<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037103092</ID>
<ANCIEN_ID>JG_L_2018_06_000000417734</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/10/30/CETATEXT000037103092.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 25/06/2018, 417734</TITRE>
<DATE_DEC>2018-06-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>417734</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>LE PRADO ; SCP MATUCHANSKY, POUPOT, VALDELIEVRE ; SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>M. Thomas Pez-Lavergne</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:417734.20180625</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société hospitalière d'assurances mutuelles (SHAM) a demandé au juge du référé précontractuel du tribunal administratif de Toulon, sur le fondement de l'article L. 551-1 du code de justice administrative, en premier lieu, d'annuler la décision par laquelle le centre hospitalier intercommunal de Fréjus Saint-Raphaël (Var) a rejeté l'offre qu'elle avait présentée pour l'attribution du marché de prestations de services d'assurance responsabilité civile hospitalière et risques annexes, ainsi que la décision d'attribuer ce marché au groupement constitué par le Bureau européen d'assurance hospitalière (BEAH), mandataire, et les sociétés Amtrust International Underwriters et Areas Assurances, en deuxième lieu, d'enjoindre au centre hospitalier intercommunal de Fréjus Saint-Raphaël de reprendre la procédure au stade de l'examen des offres, en troisième lieu, d'annuler la procédure de passation lancée par le centre hospitalier intercommunal de Fréjus Saint-Raphaël et, en dernier lieu, d'enjoindre au centre hospitalier intercommunal de lui communiquer les informations exigées par l'article 99 du décret du 25 mars 2016 relatif aux marchés publics, notamment le rapport d'analyse des offres ainsi qu'un ensemble d'autres documents relatifs au marché. Informée de ce que le marché avait été signé, la société a demandé au juge du référé contractuel du tribunal administratif de Toulon, sur le fondement des articles L. 551-13 et L. 551-18 du code de justice administrative, en premier lieu, d'annuler le contrat conclu le 23 décembre 2017 par le centre hospitalier intercommunal de Fréjus Saint-Raphaël et le groupement constitué par le Bureau européen d'assurance hospitalière, mandataire, et les sociétés Amtrust International Underwriters et Areas Assurances, en second lieu, d'enjoindre au centre hospitalier intercommunal de lui communiquer la lettre de candidature du groupement attributaire, le mandat au Bureau européen d'assurance hospitalière des sociétés Areas Assurances et Amtrust International Underwriters, le mémoire technique du Bureau européen d'assurance hospitalière, l'ensemble des pièces du marché signé, ainsi que le rapport d'analyse des offres et de présentation de la procédure de passation.<br/>
<br/>
              Par une ordonnance n° 1704809 du 15 janvier 2018, le juge des référés du tribunal administratif de Toulon a décidé un non-lieu à statuer sur ses conclusions présentées au titre de l'article L. 551-1 du code de justice administrative et rejeté le surplus de ses conclusions.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire, un nouveau mémoire et un mémoire en réplique, enregistrés les 29 janvier, 13 février, 9 mai et 7 juin 2018 au secrétariat du contentieux du Conseil d'Etat, la société hospitalière d'assurances mutuelles demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance en tant qu'elle rejette ses conclusions présentées devant le juge du référé contractuel ;<br/>
<br/>
              2°) de mettre à la charge du centre hospitalier intercommunal de Fréjus Saint-Raphaël et du Bureau européen d'assurance hospitalière la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code des assurances ;<br/>
              - l'ordonnance n° 2015-899 du 23 juillet 2015 ;<br/>
              - le décret n° 2016-360 du 25 mars 2016 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Thomas Pez-Lavergne, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Le Prado, avocat de la société hospitalière d'assurances mutuelles, à la SCP Foussard, Froger, avocat du bureau européen d'assurance hospitalière, et à la SCP Matuchansky, Poupot, Valdelièvre, avocat du centre hospitalier intercommunal de Fréjus Saint-Raphaël.<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis au juge des référés du tribunal administratif de Toulon que, par un avis d'appel public à la concurrence publié le 17 octobre 2017 au bulletin officiel des annonces des marchés publics, le centre hospitalier intercommunal de Fréjus Saint-Raphaël (Var) a lancé, au nom du groupement de commandes constitué avec le centre hospitalier de Saint-Tropez et dont il est le coordonnateur, une procédure d'appel d'offres ouvert pour la passation d'un marché public de prestations de services d'assurance responsabilité civile hospitalière et risques annexes ; que, par un courrier électronique du 12 décembre 2017, la société hospitalière d'assurances mutuelles a été informée du rejet de son offre et de l'attribution du marché au groupement constitué par le Bureau européen d'assurance hospitalière et les sociétés Amtrust International Underwriters et Areas Assurances ; que, dans un premier temps, la société hospitalière d'assurances mutuelles a demandé au juge du référé précontractuel, sur le fondement de l'article L. 551-1 du code de justice administrative, d'une part, d'annuler la décision par laquelle le centre hospitalier intercommunal de Fréjus Saint-Raphaël avait rejeté son offre et la procédure de passation du marché et, d'autre part, de lui enjoindre de reprendre la procédure au stade de l'examen des offres et de lui communiquer les motifs du rejet de son offre, les caractéristiques et avantages de l'offre retenue ainsi qu'un ensemble de documents relatifs au marché, dont le rapport d'analyse des offres ; qu'informée de la signature du marché par le centre hospitalier intercommunal de Fréjus Saint-Raphaël avec le groupement attributaire, elle a demandé au juge des référés l'annulation de ce marché sur le fondement des dispositions des articles L. 551-13 et L. 551-18 du code de justice administrative relatives au référé contractuel ; que, par une ordonnance du 15 janvier 2018, le juge des référés du tribunal administratif de Toulon a prononcé un non-lieu à statuer sur les conclusions de la société hospitalière d'assurances mutuelles relatives au référé précontractuel et rejeté ses conclusions présentées au titre du référé contractuel ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 551-1 du code de justice administrative : " Le président du tribunal administratif, ou le magistrat qu'il délègue, peut être saisi en cas de manquement aux obligations de publicité et de mise en concurrence auxquelles est soumise la passation par les pouvoirs adjudicateurs de contrats administratifs ayant pour objet l'exécution de travaux, la livraison de fournitures ou la prestation de services, avec une contrepartie économique constituée par un prix ou un droit d'exploitation, la délégation d'un service public ou la sélection d'un actionnaire opérateur économique d'une société d'économie mixte à opération unique. / Il peut également être saisi en cas de manquement aux mêmes obligations auxquelles sont soumises, en application de l'article L. 521-20 du code de l'énergie, la sélection de l'actionnaire opérateur d'une société d'économie mixte hydroélectrique et la désignation de l'attributaire de la concession. / Le juge est saisi avant la conclusion du contrat " ; qu'aux termes de l'article L. 551-4 du même code : " Le contrat ne peut être signé à compter de la saisine du tribunal administratif et jusqu'à la notification au pouvoir adjudicateur de la décision juridictionnelle " ; qu'aux termes de l'article R. 551-1 de ce code : " Le représentant de l'Etat ou l'auteur du recours est tenu de notifier son recours au pouvoir adjudicateur. / Cette notification est réputée accomplie à la date de sa réception par le pouvoir adjudicateur " ; qu'enfin, aux termes de l'article L. 551-14 du même code, le recours en référé contractuel " n'est pas ouvert au demandeur ayant fait usage du recours prévu à l'article L. 551-1 ou à l'article L. 551-5 dès lors que le pouvoir adjudicateur ou l'entité adjudicatrice a respecté la suspension prévue à l'article L. 551-4 ou à l'article L. 551-9 et s'est conformé à la décision juridictionnelle rendue sur ce recours " ;<br/>
<br/>
              3. Considérant qu'il résulte de ces dispositions que le pouvoir adjudicateur, lorsqu'est introduit un recours en référé précontractuel dirigé contre la procédure de passation d'un contrat, doit suspendre la signature de ce contrat à compter, soit de la communication du recours par le greffe du tribunal administratif, soit de sa notification par le représentant de l'Etat ou l'auteur du recours agissant conformément aux dispositions de l'article R. 551-1 du code de justice administrative ; qu'en vertu des dispositions de l'article L. 551-14 du même code, la méconnaissance de cette obligation par le pouvoir adjudicateur ouvre la voie du recours en référé contractuel au demandeur qui avait fait usage du référé précontractuel ; qu'en revanche, ni les dispositions précitées, ni aucune autre règle ou disposition ne subordonnent l'effet suspensif de la communication du recours au pouvoir adjudicateur à la transmission, par le demandeur, de documents attestant de la réception effective du recours par le tribunal ;<br/>
<br/>
              4. Considérant qu'il ressort des pièces du dossier soumis au juge des référés du tribunal administratif de Toulon que la société hospitalière d'assurances mutuelles a adressé au centre hospitalier intercommunal de Fréjus Saint-Raphaël un courrier l'informant du dépôt d'un référé précontractuel devant le tribunal, en lui joignant la copie de sa demande ; que, pour rejeter comme irrecevable le référé contractuel de la société hospitalière d'assurances mutuelles, le juge des référés du tribunal administratif de Toulon a relevé qu'elle n'avait pas envoyé au centre hospitalier intercommunal de Fréjus Saint-Raphaël l'accusé de réception du dépôt et de l'enregistrement de sa demande, délivré automatiquement par l'application télérecours, seul élément de nature à établir la saisine du tribunal et que, par suite, le référé précontractuel ne pouvait être regardé comme ayant été régulièrement notifié au pouvoir adjudicateur, dans les conditions prévues par l'article R. 551-1 du code de justice administrative ; qu'en exigeant ainsi que le demandeur apporte au pouvoir adjudicateur la preuve de la saisine du tribunal par la transmission de l'accusé de réception du dépôt et de l'enregistrement de la demande délivré par télérecours, en en déduisant qu'en l'absence d'une telle production, le centre hospitalier intercommunal n'avait pas méconnu l'obligation qui pesait sur lui de suspendre la signature du marché et en jugeant que, par suite, le référé contractuel était irrecevable, le juge des référés a entaché son ordonnance d'erreur de droit ;<br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que la société hospitalière d'assurances mutuelles est fondée à demander l'annulation de l'ordonnance attaquée en tant qu'elle statue sur les conclusions de référé contractuel ;<br/>
<br/>
              6. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du centre hospitalier intercommunal de Fréjus Saint-Raphaël la somme de 3 000 euros à verser à la société hospitalière d'assurances mutuelles, au titre de l'article L. 761-1 du code de justice administrative ; que ces dispositions font obstacle à ce qu'il soit fait droit aux conclusions présentées au même titre par le centre hospitalier intercommunal et par le Bureau européen d'assurance hospitalière ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du 15 janvier 2018 du juge des référés du tribunal administratif de Toulon est annulée en tant qu'elle statue sur les conclusions de référé contractuel.<br/>
Article 2 : L'affaire est renvoyée, dans la mesure de la cassation prononcée à l'article précédent, au tribunal administratif de Toulon.<br/>
Article 3 : Le centre hospitalier intercommunal de Fréjus Saint-Raphaël versera une somme de 3 000 euros à la société hospitalière d'assurances mutuelles au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions du centre hospitalier intercommunal de Fréjus Saint-Raphaël et du Bureau européen d'assurance hospitalière tendant à l'application de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 5 : La présente décision sera notifiée à la société hospitalière d'assurances mutuelles, au centre hospitalier intercommunal de Fréjus Saint-Raphaël et au Bureau européen d'assurance hospitalière.<br/>
Copie en sera adressée au centre hospitalier de Saint-Tropez.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-08-015-01 MARCHÉS ET CONTRATS ADMINISTRATIFS. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. PROCÉDURES D'URGENCE. - SUSPENSION DE LA SIGNATURE DU CONTRAT PAR LE POUVOIR ADJUDICATEUR - POINT DE DÉPART - COMMUNICATION AU POUVOIR ADJUDICATEUR DU RECOURS PAR LE GREFFE DU TA OU NOTIFICATION DU RECOURS AU POUVOIR ADJUDICATEUR PAR LE REPRÉSENTANT DE L'ETAT OU L'AUTEUR DU RECOURS - EXISTENCE - TRANSMISSION, PAR L'AUTEUR DU RECOURS, DE DOCUMENTS ATTESTANT DE LA RÉCEPTION EFFECTIVE DU RECOURS PAR LE TRIBUNAL - ABSENCE.
</SCT>
<ANA ID="9A"> 39-08-015-01 Il résulte des articles L. 551-1, L. 551-4, L. 551-14 et R. 551-1 du code de justice administrative (CJA) que le pouvoir adjudicateur, lorsqu'est introduit un recours en référé précontractuel dirigé contre la procédure de passation d'un contrat, doit suspendre la signature de ce contrat à compter, soit de la communication du recours par le greffe du tribunal administratif (TA), soit de sa notification par le représentant de l'Etat ou l'auteur du recours agissant conformément aux dispositions de l'article R. 551-1 du CJA. En vertu des dispositions de l'article L. 551-14 du même code, la méconnaissance de cette obligation par le pouvoir adjudicateur ouvre la voie du recours en référé contractuel au demandeur qui avait fait usage du référé précontractuel. En revanche, ni les dispositions mentionnées, ni aucune autre règle ou disposition ne subordonnent l'effet suspensif de la communication du recours au pouvoir adjudicateur à la transmission, par le demandeur, de documents attestant de la réception effective du recours par le tribunal.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
