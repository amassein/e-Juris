<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034017930</ID>
<ANCIEN_ID>JG_L_2017_02_000000402417</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/01/79/CETATEXT000034017930.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 08/02/2017, 402417, Publié au recueil Lebon</TITRE>
<DATE_DEC>2017-02-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>402417</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR>Mme Isabelle Lemesle</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:402417.20170208</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Le préfet de la Haute-Savoie a demandé au juge des référés du tribunal administratif de Grenoble, statuant sur le fondement de l'article L. 2131-6 du code général des collectivités territoriales, de suspendre l'exécution de l'arrêté du 9 juillet 2015 par lequel le maire de Chens-sur-Léman a délivré un permis de construire à M. A...pour l'édification de quatre villas. Par une ordonnance n°1507062 du 15 décembre 2015, le juge des référés du tribunal administratif de Grenoble a fait droit à sa demande.<br/>
<br/>
              Le préfet de la Haute-Savoie a, par une requête distincte, également demandé au tribunal administratif de Grenoble, d'annuler pour excès de pouvoir cet arrêté du 9 juillet 2015 du maire de Chens-sur-Léman. Par un jugement n°1507059 du 26 mai 2016, le tribunal administratif de Grenoble a rejeté sa demande.<br/>
<br/>
              Par une ordonnance n°16LY02604 du 29 juillet 2016, le juge des référés de la cour administrative d'appel de Lyon, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, a rejeté la demande du préfet de la Haute-Savoie tendant à la suspension de l'exécution du même arrêté.<br/>
<br/>
              Par un pourvoi et un mémoire complémentaire, enregistrés le 12 août 2016 et le 29 août 2016 au secrétariat du contentieux du Conseil d'Etat, la ministre du logement et de l'habitat durable demande au Conseil d'Etat d'annuler cette ordonnance.<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son article 72 ;<br/>
              - le code général des collectivités territoriales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Isabelle Lemesle, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Matuchansky, Poupot, Valdelièvre, avocat de la commune de Chens-sur-Leman  ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Par une lettre du 11 août 2015, le préfet de la Haute-Savoie a demandé au maire de Chens-sur-Léman de retirer le permis de construire délivré par un arrêté du 9 juillet 2015 à M. A...pour l'édification de quatre villas. Le maire ayant rejeté implicitement cette demande, le préfet a saisi le tribunal administratif de Grenoble de deux demandes tendant d'une part, à l'annulation de ce permis de construire et d'autre part, à la suspension de son exécution, sur le fondement de l'article L. 2131-6 du code général des collectivités territoriales. Le juge des référés a suspendu l'exécution du permis de construire contesté par une ordonnance du 15 décembre 2015 mais le tribunal administratif a rejeté le déféré préfectoral au fond par un jugement du 26 mai 2016. Le préfet a relevé appel de ce jugement et parallèlement demandé au juge des référés de la cour administrative d'appel de Lyon de suspendre l'exécution de ce permis de construire, à nouveau sur le fondement de l'article L. 2131-6 du code général des collectivités territoriales. La ministre du logement et de l'habitat durable se pourvoit en cassation contre l'ordonnance du 29 juillet 2016, prise sur le fondement de l'article L. 521-1 du code de justice administrative, par laquelle le juge des référés de la cour administrative d'appel de Lyon a rejeté la demande du préfet.<br/>
<br/>
              2. Aux termes du premier alinéa de l'article L. 2131-6 du code général des collectivités territoriales : "Le représentant de l'Etat dans le département défère au tribunal administratif les actes mentionnés à l'article L. 2131-2 qu'il estime contraires à la légalité dans les deux mois suivant leur transmission". Aux termes du troisième alinéa du même article, reproduit à l'article L. 554-1 du code de justice administrative : "Le représentant de l'Etat peut assortir son recours d'une demande de suspension. Il est fait droit à cette demande si l'un des moyens invoqués paraît, en l'état de l'instruction, propre à créer un doute sérieux quant à la légalité de l'acte attaqué. Il est statué dans un délai d'un mois". Aux termes du quatrième alinéa du même article : " Jusqu'à ce que le président du tribunal administratif ou le magistrat délégué par lui ait statué, la demande de suspension en matière d'urbanisme (...) formulée par le représentant de l'Etat dans les dix jours à compter de la réception de l'acte entraîne la suspension de celui-ci. Au terme d'un délai d'un mois à compter de la réception, si le juge des référés n'a pas statué, l'acte redevient exécutoire ". Aux termes enfin du sixième alinéa : "L'appel des jugements du tribunal administratif ainsi que des décisions relatives aux  demandes de suspension prévues aux alinéas précédents, rendus sur recours du représentant de l'Etat, est présenté par celui-ci"  ". Parmi les actes mentionnés par l'article L. 2131-2 de ce code figure, au 6° : " Le permis de construire et les autres autorisations d'utilisation du sol et le certificat d'urbanisme délivrés par le maire ".<br/>
<br/>
              3. Il résulte de ces dispositions du code général des collectivités territoriales, dans leur rédaction issue de la loi du 30 juin 2000, et notamment du sixième alinéa de l'article L. 2131-6, que lorsque le juge des référés du tribunal administratif se prononce sur une demande de suspension présentée par le représentant de l'Etat en application de cet article, sa décision, qui n'entre pas dans le champ d'application des articles L. 521-1 à L. 523-1 du code de justice administrative relatifs au juge des référés statuant en urgence, est susceptible de faire l'objet d'un appel. <br/>
<br/>
              4. En outre, alors même que le premier alinéa de l'article L. 2131-6 du code général des collectivités territoriales ne mentionne expressément que le tribunal administratif, il résulte du troisième alinéa de cet article que le représentant de l'Etat, eu égard aux missions que l'article 72 de la Constitution confie au préfet, peut assortir d'une demande de suspension l'appel qu'il relève du jugement rendu par le tribunal administratif statuant sur sa demande d'annulation de l'acte qu'il lui a déféré. En revanche, le respect du principe de libre administration des collectivités territoriales impose que le caractère suspensif du référé sur déféré ne s'applique, en vertu du quatrième alinéa, que lorsqu'il est présenté au juge des référés du tribunal administratif.<br/>
<br/>
              5. En tout état de cause, une telle demande de suspension de l'exécution d'une décision, qui n'entre pas dans le champ  de l'article L. 521-1 du code de justice administrative aux termes duquel " le juge des référés (...) peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ", ne peut être formée sur le fondement de cet article. <br/>
<br/>
              6. Dès lors, le juge des référés de la cour administrative d'appel de Lyon n'a pu, sans erreur de droit, juger que la requête du préfet de la Haute-Savoie tendant à la suspension de l'exécution de l'arrêté du 9 juillet 2015 du maire de Chens-sur-Léman délivrant à M. A...un permis de construire pour l'édification de quatre maisons individuelles, dont était assorti son déféré tendant à l'annulation du jugement du 26 mai 2016 du tribunal administratif de Grenoble rejetant sa demande d'annulation de cet acte, devait être regardée comme présentée sur le fondement de l'article L. 521-1 du code de justice administrative, alors que dans ses écritures le préfet de la Haute Savoie se fondait sur l'article L. 2131-6 du code général des collectivités territoriales.<br/>
<br/>
              7. Il résulte de ce qui précède que la ministre du logement et de l'habitat durable est fondée à demander l'annulation de l'ordonnance qu'elle attaque. Le motif retenu suffisant à entraîner cette annulation, il n'est pas nécessaire de statuer sur les autres moyens du pourvoi.<br/>
<br/>
              8. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du 29 juillet 2016 du juge des référés de la cour administrative d'appel de Lyon est annulée.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Lyon.<br/>
Article 3 : Les conclusions de la commune de Chens-sur-Léman tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à la ministre du logement et de l'habitat durable et à la commune de Chens-sur-Léman.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">135-01-015-03 COLLECTIVITÉS TERRITORIALES. DISPOSITIONS GÉNÉRALES. CONTRÔLE DE LA LÉGALITÉ DES ACTES DES AUTORITÉS LOCALES. DÉFÉRÉ ASSORTI D'UNE DEMANDE DE SURSIS À EXÉCUTION. - DISPOSITIONS RELATIVES AU RÉFÉRÉ SUR DÉFÉRÉ (ART. L. 2131-6 DU CGCT) - 1) FACULTÉ POUR LE PRÉFET D'ASSORTIR  L'APPEL QU'IL RELÈVE D'UN JUGEMENT STATUANT SUR LA DEMANDE D'ANNULATION D'UN ACTE QU'IL A DÉFÉRÉ D'UNE DEMANDE DE SUSPENSION DE CET ACTE - EXISTENCE - 2) EFFET SUSPENSIF DU RÉFÉRÉ SUR DÉFÉRÉ EN APPEL - ABSENCE.
</SCT>
<ANA ID="9A"> 135-01-015-03 1) Alors même que le premier alinéa de l'article L. 2131-6 du code général des collectivités territoriales (CGCT) ne mentionne expressément que le tribunal administratif, il résulte du troisième alinéa de cet article, reproduit à l'article L. 554-1 du code de justice administrative, que le représentant de l'Etat, eu égard aux missions que l'article 72 de la Constitution confie au préfet, peut assortir l'appel qu'il relève du jugement rendu par le tribunal administratif statuant sur sa demande d'annulation de l'acte qu'il lui a déféré d'une demande de suspension de cet acte.... ,,2) En revanche, le respect du principe de libre administration des collectivités territoriales impose que le caractère suspensif du référé sur déféré ne s'applique, en vertu du quatrième alinéa, que lorsqu'il est présenté au juge des référés du tribunal administratif.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
