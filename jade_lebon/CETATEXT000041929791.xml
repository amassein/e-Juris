<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041929791</ID>
<ANCIEN_ID>JG_L_2020_05_000000436984</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/92/97/CETATEXT000041929791.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 27/05/2020, 436984</TITRE>
<DATE_DEC>2020-05-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>436984</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ZRIBI, TEXIER</AVOCATS>
<RAPPORTEUR>Mme Mélanie Villiers</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:436984.20200527</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A... C... B... a demandé au tribunal administratif de Lyon, sur le fondement de l'article L. 521-1 du code de justice administrative, d'une part, de suspendre, jusqu'à ce qu'il soit statué au fond sur sa légalité, la décision du 30 septembre 2019 par laquelle le préfet du Rhône lui a délivré un récépissé de demande de titre de séjour, en tant qu'elle n'était pas assortie d'une autorisation de travailler pendant l'instruction de sa demande, d'autre part, d'enjoindre au préfet du Rhône de lui délivrer un récépissé l'autorisant à travailler, sous astreinte de 200 euros par jour de retard, et à titre subsidiaire, de réexaminer sa situation dans un délai de huit jours, sous les mêmes conditions. Par une ordonnance n° 1908847 du 3 décembre 2019, le juge des référés du tribunal administratif de Lyon a suspendu l'exécution de la décision du préfet du Rhône du 30 septembre 2019 en tant qu'elle ne l'autorisait pas à travailler et enjoint au même préfet de lui délivrer, dans un délai de deux jours et sous astreinte de 100 euros par jour de retard, un récépissé de demande de titre de séjour l'autorisant à travailler, valable jusqu'à ce qu'il soit statué au fond sur la décision attaquée ou jusqu'à l'intervention d'une décision sur cette demande de titre de séjour.<br/>
<br/>
              Par un pourvoi, enregistré le 23 décembre 2019 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'intérieur demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de rejeter la demande de première instance de M. B....<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le code civil ;<br/>
              - le code du travail ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 modifiée ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Mélanie Villiers, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Zribi et Texier, avocat de M. B... ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision. "<br/>
<br/>
              2. Il ressort des pièces du dossier soumis au juge des référés que M. B..., ressortissant de la République de Guinée né le 15 septembre 1999, est entré en France le 29 mai 2016, à l'âge de seize ans, et a été confié au service de l'aide sociale à l'enfance. Il a présenté, en avril 2018, une demande de titre de séjour, sur le fondement de l'article L. 313-15 du code de l'entrée et du séjour des étrangers et du droit d'asile. Le préfet du Rhône lui a délivré un récépissé de demande de titre de séjour, renouvelé en dernier lieu le 30 septembre 2019, sans toutefois l'assortir d'une autorisation de travailler. Le 15 novembre 2019, M. B... a demandé en vain au préfet de lui délivrer un récépissé avec autorisation de travail, en produisant le contrat d'apprentissage dont la conclusion lui avait été proposée par la société 2C Transport. Saisi par M. B... sur le fondement des dispositions de l'article L. 521-1 du code de justice administrative, le juge des référés du tribunal administratif de Lyon, par une ordonnance du 3 décembre 2019 contre laquelle le ministre de l'intérieur se pourvoit en cassation, a suspendu l'exécution de la décision du préfet du Rhône du 30 septembre 2019 lui délivrant un récépissé de demande de titre de séjour en tant qu'elle ne l'autorisait pas à travailler et enjoint au préfet de lui délivrer, dans un délai de deux jours et sous astreinte de 100 euros par jour de retard, un récépissé de demande de titre de séjour l'autorisant à travailler, valable jusqu'à ce qu'il soit statué au fond sur la décision attaquée ou jusqu'à l'intervention d'une décision sur cette demande de titre de séjour. <br/>
<br/>
              Sur l'existence d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision contestée :<br/>
<br/>
              3. D'une part, aux termes de l'article L. 313-15 du code de l'entrée et du séjour des étrangers et du droit d'asile : " A titre exceptionnel et sauf si sa présence constitue une menace pour l'ordre public, la carte de séjour temporaire prévue aux 1° et 2° de l'article L. 313-10 portant la mention " salarié " ou la mention " travailleur temporaire " peut être délivrée, dans l'année qui suit son dix-huitième anniversaire, à l'étranger qui a été confié à l'aide sociale à l'enfance entre l'âge de seize ans et l'âge de dix-huit ans et qui justifie suivre depuis au moins six mois une formation destinée à lui apporter une qualification professionnelle, sous réserve du caractère réel et sérieux du suivi de cette formation, de la nature de ses liens avec sa famille restée dans le pays d'origine et de l'avis de la structure d'accueil sur l'insertion de cet étranger dans la société française. Le respect de la condition prévue à l'article L. 313-2 n'est pas exigé. " Les 1° et 2° de l'article L. 313-10 du même code disposent qu'une carte de séjour temporaire, d'une durée maximale d'un an, autorisant l'exercice d'une activité professionnelle est délivrée à l'étranger, dans les conditions prévues à l'article L. 5221-2 du code du travail, soit pour l'exercice d'une activité salariée sous contrat de travail à durée indéterminée avec la mention " salarié ", soit pour l'exercice d'une activité salariée sous contrat de travail à durée déterminée avec la mention " travailleur temporaire ". En vertu de l'article R. 313-6 du même code, le récépissé de la demande de première délivrance d'une carte de séjour sur le fondement des 1° et 2° de l'article L. 313-10 autorise son titulaire à travailler, dès lors qu'il satisfait aux conditions mentionnées à l'article L. 5221 2 du code du travail.<br/>
<br/>
              4. D'autre part, aux termes de l'article L. 5221-2 du code du travail : " Pour entrer en France en vue d'y exercer une profession salariée, l'étranger présente : (...) 2° Un contrat de travail visé par l'autorité administrative ou une autorisation de travail. " Aux termes des deux premiers alinéas de l'article L. 5221-5 du même code : " Un étranger autorisé à séjourner en France ne peut exercer une activité professionnelle salariée en France sans avoir obtenu au préalable l'autorisation de travail mentionnée au 2° de l'article L. 5221-2. / L'autorisation de travail est accordée de droit à l'étranger autorisé à séjourner en France pour la conclusion d'un contrat d'apprentissage ou de professionnalisation à durée déterminée. Cette autorisation est accordée de droit aux mineurs isolés étrangers pris en charge par l'aide sociale à l'enfance, sous réserve de la présentation d'un contrat d'apprentissage ou de professionnalisation ".<br/>
<br/>
              5. Si l'article L. 5221-5 du code du travail prévoit, en son premier alinéa, qu'un étranger autorisé à séjourner en France ne peut y exercer une activité professionnelle salariée sans avoir obtenu au préalable l'autorisation de travail mentionnée au 2° de l'article L. 5221-2, il résulte des termes mêmes de la première phrase de son deuxième alinéa que cette autorisation est accordée de droit à l'étranger autorisé à séjourner en France pour la conclusion d'un contrat d'apprentissage ou de professionnalisation à durée déterminée. Par suite, les dispositions de l'article R. 311-6 du code du travail, en tant qu'elles prévoient que le récépissé de la demande de première délivrance d'une carte de séjour sur le fondement des 1° et 2° de l'article L. 313-10 du code de l'entrée et du séjour des étrangers et du droit d'asile n'autorise son titulaire à travailler que s'il satisfait aux conditions mentionnées à l'article L. 5221-2 du code du travail, ne sauraient être interprétées comme imposant une telle exigence aux étrangers auxquels une autorisation de travail est accordée de droit sur le fondement de la première phrase du deuxième alinéa de l'article L. 5221-5 du même code. Tel est le cas de l'étranger qui, admis au séjour sur le fondement de l'article L. 313-15 du code de l'entrée et du séjour des étrangers et du droit d'asile, s'est vu délivrer la carte de séjour temporaire portant la mention " salarié " ou " travailleur temporaire " prévue à l'article L. 313-10 du même code et suit une formation destinée à lui apporter une qualification professionnelle dans le cadre d'un contrat d'apprentissage ou de professionnalisation. <br/>
<br/>
              6. Il résulte de ce qui précède que le préfet, saisi d'une demande d'admission exceptionnelle au séjour, sur le fondement de l'article L. 313-15 du code de l'entrée et du séjour en France des étrangers et du droit d'asile, par un étranger admis à l'aide sociale à l'enfance entre l'âge de seize et l'âge de dix-huit ans, qui satisfait aux conditions de séjour définies par cet article et justifie qu'il dispose d'un contrat d'apprentissage ou de professionnalisation ou que la conclusion d'un tel contrat lui a été proposée, doit remettre au pétitionnaire un récépissé de demande de titre de séjour l'autorisant à travailler, en application des dispositions de l'article R. 311-6 du même code.<br/>
<br/>
              7. En l'espèce, pour suspendre la décision du préfet du Rhône et faire droit aux conclusions aux fins d'injonction dont il était saisi, le juge des référés du tribunal administratif de Lyon s'est fondé sur les dispositions de la seconde phrase du deuxième alinéa de l'article L. 5221-5 du code du travail, selon lesquelles une autorisation de travail est accordée de droit aux mineurs isolés étrangers pris en charge par l'aide sociale à l'enfance, sous réserve de la présentation d'un contrat d'apprentissage ou de professionnalisation. Il a ainsi entaché son ordonnance d'erreur de droit, les dispositions en cause n'étant applicables qu'aux étrangers mineurs alors qu'il est constant que M. B... était majeur. Toutefois, il résulte des énonciations mêmes de l'ordonnance attaquée que M. B... justifiait d'une promesse d'embauche dans le cadre d'un contrat d'apprentissage et pouvait donc se prévaloir des dispositions de la première phrase du deuxième alinéa de l'article L. 5221-5 du code du travail. Ainsi qu'il a été dit au point 6, le préfet était dès lors tenu de lui remettre un récépissé de demande de titre de séjour l'autorisant à travailler. Ce motif, qui n'appelle aucune autre appréciation de fait que celles auxquelles a procédé le juge des référés, doit être substitué à celui retenu dans l'ordonnance attaquée. Par suite, ne peuvent qu'être écartés les moyens d'erreur de droit et de dénaturation invoqués par le ministre de l'intérieur et dirigés contre l'ordonnance attaquée en tant qu'elle a jugé que le moyen tiré de ce que le préfet du Rhône avait méconnu les dispositions de l'article R. 311-6 du code de l'entrée et du séjour des étrangers et du droit d'asile en ne délivrant pas à M. B... un récépissé de demande de titre de séjour l'autorisant à travailler était propre à créer, en l'état de l'instruction, un doute sérieux sur la légalité cette décision.<br/>
<br/>
              Sur la condition d'urgence :<br/>
<br/>
              8. Pour juger que la condition d'urgence était remplie, le juge des référés du tribunal administratif de Lyon a relevé que la décision attaquée privait M. B... de la possibilité d'être embauché et, ainsi, de poursuivre sa formation et de subvenir à ses besoins. Contrairement à ce que soutient le ministre de l'intérieur, cette appréciation n'est pas entachée de dénaturation des pièces du dossier.<br/>
<br/>
              9. Il résulte de tout ce qui précède que le ministre de l'intérieur n'est pas fondé à demander l'annulation de l'ordonnance du juge des référés du tribunal administratif de Lyon. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à la SCP Zribi et Texier, l'avocat de M. B..., au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi du ministre de l'intérieur est rejeté.<br/>
Article 2 : L'Etat versera à la SCP Zribi et Texier la somme de 3 000 euros en application des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
Article 3 : La présente décision sera notifiée au ministre de l'intérieur et à M. A... C... B....<br/>
Copie en sera adressée à la ministre du travail.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">04-02-02 AIDE SOCIALE. DIFFÉRENTES FORMES D'AIDE SOCIALE. AIDE SOCIALE À L'ENFANCE. - DEMANDE D'ADMISSION EXCEPTIONNELLE AU SÉJOUR D'UN ÉTRANGER ADMIS À L'ASE ENTRE SEIZE ET DIX-HUIT ANS SATISFAISANT AUX CONDITIONS DE L'ARTICLE L. 313-15 DU CESEDA ET JUSTIFIANT DISPOSER OU S'ÊTRE VU PROPOSER UN CONTRAT D'APPRENTISSAGE OU DE PROFESSIONNALISATION - RÉCÉPISSÉ DE DEMANDE DE TITRE DE SÉJOUR AUTORISANT L'ÉTRANGER À TRAVAILLER (ART. R. 311-6 DU CESEDA) - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">335-01-02-01 ÉTRANGERS. SÉJOUR DES ÉTRANGERS. AUTORISATION DE SÉJOUR. DEMANDE DE TITRE DE SÉJOUR. - AUTORISATION DE TRAVAIL - 1) PRINCIPE - ETRANGER AUTORISÉ À SÉJOURNER EN FRANCE POUR LA CONCLUSION D'UN CONTRAT D'APPRENTISSAGE OU DE PROFESSIONNALISATION À DURÉE DÉTERMINÉE (2E ALINÉA DE L'ART. L. 5221-5 DU CODE DU TRAVAIL) - ETRANGER SOUMIS AUX CONDITIONS DE L'ARTICLE L. 5221-2 DU CODE DU TRAVAIL - ABSENCE - 2) APPLICATION - DEMANDE D'ADMISSION EXCEPTIONNELLE AU SÉJOUR D'UN ÉTRANGER SATISFAISANT AUX CONDITIONS DE L'ARTICLE L. 313-15 DU CESEDA ET JUSTIFIANT DISPOSER OU S'ÊTRE VU PROPOSER UN CONTRAT D'APPRENTISSAGE OU DE PROFESSIONNALISATION - RÉCÉPISSÉ DE DEMANDE DE TITRE DE SÉJOUR AUTORISANT L'ÉTRANGER À TRAVAILLER (ART. R. 311-6 DU CESEDA) - EXISTENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">335-06-02-01 ÉTRANGERS. EMPLOI DES ÉTRANGERS. MESURES INDIVIDUELLES. TITRE DE TRAVAIL. - AUTORISATION DE TRAVAIL - 1) PRINCIPE - ETRANGER AUTORISÉ À SÉJOURNER EN FRANCE POUR LA CONCLUSION D'UN CONTRAT D'APPRENTISSAGE OU DE PROFESSIONNALISATION À DURÉE DÉTERMINÉE (2E ALINÉA DE L'ART. L. 5221-5 DU CODE DU TRAVAIL) - ETRANGER SOUMIS AUX CONDITIONS DE L'ARTICLE L. 5221-2 DU CODE DU TRAVAIL - ABSENCE - 2) APPLICATION - DEMANDE D'ADMISSION EXCEPTIONNELLE AU SÉJOUR D'UN ÉTRANGER SATISFAISANT AUX CONDITIONS DE L'ARTICLE L. 313-15 DU CESEDA ET JUSTIFIANT DISPOSER OU S'ÊTRE VU PROPOSER UN CONTRAT D'APPRENTISSAGE OU DE PROFESSIONNALISATION - RÉCÉPISSÉ DE DEMANDE DE TITRE DE SÉJOUR AUTORISANT L'ÉTRANGER À TRAVAILLER (ART. R. 311-6 DU CESEDA) - EXISTENCE.
</SCT>
<ANA ID="9A"> 04-02-02 Le préfet, saisi d'une demande d'admission exceptionnelle au séjour, sur le fondement de l'article L. 313-15 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA), par un étranger admis à l'aide sociale à l'enfance (ASE) entre l'âge de seize et l'âge de dix-huit ans, qui satisfait aux conditions de séjour définies par cet article et justifie qu'il dispose d'un contrat d'apprentissage ou de professionnalisation ou que la conclusion d'un tel contrat lui a été proposée, doit remettre au pétitionnaire un récépissé de demande de titre de séjour l'autorisant à travailler, en application des dispositions de l'article R. 311-6 du même code.</ANA>
<ANA ID="9B"> 335-01-02-01 1) Si l'article L. 5221-5 du code du travail prévoit, en son premier alinéa, qu'un étranger autorisé à séjourner en France ne peut y exercer une activité professionnelle salariée sans avoir obtenu au préalable l'autorisation de travail mentionnée au 2° de l'article L. 5221-2, il résulte des termes mêmes de la première phrase de son deuxième alinéa que cette autorisation est accordée de droit à l'étranger autorisé à séjourner en France pour la conclusion d'un contrat d'apprentissage ou de professionnalisation à durée déterminée. Par suite, les dispositions de l'article R. 311-6 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA), en tant qu'elles prévoient que le récépissé de la demande de première délivrance d'une carte de séjour sur le fondement des 1° et 2° de l'article L. 313-10 du CESEDA n'autorise son titulaire à travailler que s'il satisfait aux conditions mentionnées à l'article L. 5221-2 du code du travail, ne sauraient être interprétées comme imposant une telle exigence aux étrangers auxquels une autorisation de travail est accordée de droit sur le fondement de la première phrase du deuxième alinéa de l'article L. 5221-5 du même code. Tel est le cas de l'étranger qui, admis au séjour sur le fondement de l'article L. 313-15 du CESEDA, s'est vu délivrer la carte de séjour temporaire portant la mention salarié ou  travailleur temporaire  prévue à l'article L. 313-10 du même code et suit une formation destinée à lui apporter une qualification professionnelle dans le cadre d'un contrat d'apprentissage ou de professionnalisation.... ,,2) Il résulte de ce qui précède que le préfet, saisi d'une demande d'admission exceptionnelle au séjour, sur le fondement de l'article L. 313-15 du CESEDA, par un étranger admis à l'aide sociale à l'enfance entre l'âge de seize et l'âge de dix-huit ans, qui satisfait aux conditions de séjour définies par cet article et justifie qu'il dispose d'un contrat d'apprentissage ou de professionnalisation ou que la conclusion d'un tel contrat lui a été proposée, doit remettre au pétitionnaire un récépissé de demande de titre de séjour l'autorisant à travailler, en application des dispositions de l'article R. 311-6 du même code.</ANA>
<ANA ID="9C"> 335-06-02-01 1) Si l'article L. 5221-5 du code du travail prévoit, en son premier alinéa, qu'un étranger autorisé à séjourner en France ne peut y exercer une activité professionnelle salariée sans avoir obtenu au préalable l'autorisation de travail mentionnée au 2° de l'article L. 5221-2, il résulte des termes mêmes de la première phrase de son deuxième alinéa que cette autorisation est accordée de droit à l'étranger autorisé à séjourner en France pour la conclusion d'un contrat d'apprentissage ou de professionnalisation à durée déterminée. Par suite, les dispositions de l'article R. 311-6 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA), en tant qu'elles prévoient que le récépissé de la demande de première délivrance d'une carte de séjour sur le fondement des 1° et 2° de l'article L. 313-10 du CESEDA n'autorise son titulaire à travailler que s'il satisfait aux conditions mentionnées à l'article L. 5221-2 du code du travail, ne sauraient être interprétées comme imposant une telle exigence aux étrangers auxquels une autorisation de travail est accordée de droit sur le fondement de la première phrase du deuxième alinéa de l'article L. 5221-5 du même code. Tel est le cas de l'étranger qui, admis au séjour sur le fondement de l'article L. 313-15 du CESEDA, s'est vu délivrer la carte de séjour temporaire portant la mention salarié ou  travailleur temporaire  prévue à l'article L. 313-10 du même code et suit une formation destinée à lui apporter une qualification professionnelle dans le cadre d'un contrat d'apprentissage ou de professionnalisation.... ,,2) Il résulte de ce qui précède que le préfet, saisi d'une demande d'admission exceptionnelle au séjour, sur le fondement de l'article L. 313-15 du CESEDA, par un étranger admis à l'aide sociale à l'enfance entre l'âge de seize et l'âge de dix-huit ans, qui satisfait aux conditions de séjour définies par cet article et justifie qu'il dispose d'un contrat d'apprentissage ou de professionnalisation ou que la conclusion d'un tel contrat lui a été proposée, doit remettre au pétitionnaire un récépissé de demande de titre de séjour l'autorisant à travailler, en application des dispositions de l'article R. 311-6 du même code.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
