<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041569426</ID>
<ANCIEN_ID>JG_L_2020_02_000000425566</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/56/94/CETATEXT000041569426.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 1ère chambres réunies, 12/02/2020, 425566</TITRE>
<DATE_DEC>2020-02-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>425566</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MARLANGE, DE LA BURGADE ; SCP BOUTET-HOURDEAUX ; SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR>M. Olivier  Fuchs</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Raphaël Chambon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:425566.20200212</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Le médecin-conseil, chef de service de l'échelon local du service médical du Var et la caisse primaire d'assurance maladie du Var ont porté plainte contre M. A... B... devant la section des assurances sociales de la chambre disciplinaire de première instance de Provence-Alpes-Côte d'Azur-Corse de l'ordre des médecins. Par une décision du 7 décembre 2017, la section des assurances sociales de la chambre disciplinaire de première instance a infligé à M. B... la sanction de l'interdiction de donner des soins aux assurés sociaux pour une durée de quatre mois, dont deux mois assortis du sursis.<br/>
<br/>
              Par une décision du 17 octobre 2018, la section des assurances sociales du Conseil national de l'ordre des médecins a rejeté l'appel formé par M. B... contre cette décision.<br/>
<br/>
              Par une requête sommaire, un mémoire complémentaire et deux nouveaux mémoires, enregistrés les 22 et 30 novembre 2018 et les 7 janvier et 4 octobre 2019 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette décision ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre solidairement à la charge du médecin-conseil, chef de service de l'échelon local du service médical du Var et de la caisse primaire d'assurance maladie du Var la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu : <br/>
              - le code de la sécurité sociale ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Fuchs, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Raphaël Chambon, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Marlange, de la Burgade, avocat de M. A... B... et à la SCP Boutet-Hourdeaux, avocat de la caisse primaire d'assurance maladie du Var et du médecin-conseil, chef de service de l'échelon local du service médical du Var ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 29 janvier 2020, présentée par M. B... ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que le médecin-conseil, chef de service de l'échelon local du service médical du Var et la caisse primaire d'assurance maladie du Var ont porté plainte contre M. A... B..., médecin qualifié en médecine générale, devant la section des assurances sociales de la chambre disciplinaire de première instance de Provence-Alpes-Côte d'Azur-Corse de l'ordre des médecins. Par une décision du 7 décembre 2017, la section des assurances sociales de la chambre disciplinaire de première instance a infligé à M. B... la sanction de l'interdiction de donner des soins aux assurés sociaux pour une durée de quatre mois, dont deux mois assortis du sursis. M. B... se pourvoit en cassation contre la décision du 17 octobre 2018 par laquelle la section des assurances sociales du Conseil national de l'ordre des médecins a rejeté son appel.<br/>
<br/>
              Sur la régularité de la décision attaquée :<br/>
<br/>
              2. L'article R. 145-41 du code de la sécurité sociale, rendu applicable à la procédure devant la section des assurances sociales du conseil national de l'ordre des médecins par l'article R. 145-57 du même code, dispose que " la minute de la décision est signée par le président de la formation de jugement et le secrétaire de l'audience ". Il ressort des pièces du dossier que la minute de la décision attaquée a été signée par le secrétaire de l'audience, conformément aux dispositions précitées. Par suite, le moyen tiré de l'irrégularité de cette décision à raison du défaut de signature de la minute par le secrétaire de l'audience ne peut qu'être écarté.<br/>
<br/>
              Sur le bien-fondé de la décision attaquée :<br/>
<br/>
              En ce qui concerne les moyens relatifs à l'habilitation des agents chargés du contrôle et des médecins-conseil :<br/>
<br/>
              3. D'une part, aux termes de l'article L. 114-10 du code de la sécurité sociale, dans sa rédaction applicable au litige : " Les directeurs des organismes de sécurité sociale confient à des agents chargés du contrôle, assermentés et agréés dans des conditions définies par arrêté du ministre chargé de la sécurité sociale, le soin de procéder à toutes vérifications ou enquêtes administratives concernant l'attribution des prestations et la tarification des accidents du travail et des maladies professionnelles. Des praticiens-conseils et auditeurs comptables peuvent, à ce titre, être assermentés et agréés dans des conditions définies par le même arrêté. Ces agents ont qualité pour dresser des procès-verbaux faisant foi jusqu'à preuve du contraire (...) ".<br/>
<br/>
              4. D'autre part, le service du contrôle médical constitue, en vertu de l'article R. 315-2 du code de la sécurité sociale, un service national confié à des médecins-conseils, des chirurgiens-dentistes conseils et des pharmaciens-conseils. Il a notamment pour mission, aux termes du IV de l'article L. 315-1 du même code, de procéder " à l'analyse, sur le plan médical, de l'activité des professionnels de santé dispensant des soins aux bénéficiaires de l'assurance maladie (...), notamment au regard des règles définies par les conventions qui régissent leurs relations avec les organismes d'assurance maladie (...). La procédure d'analyse de l'activité se déroule dans le respect des droits de la défense selon des conditions définies par décret. ". Le premier alinéa du III de l'article R. 315-1 du même code dispose que lorsque le service du contrôle médical constate le non-respect de règles de nature législative, réglementaire ou conventionnelle que les professionnels sont tenus d'appliquer, la procédure prévue aux articles L. 145-1 et suivants du même code peut notamment être mise en oeuvre. Cette procédure permet de soumettre aux juridiction du contrôle technique les " fautes, abus, fraudes et tous faits intéressant l'exercice de la profession relevés à l'encontre des médecins (...) à l'occasion des soins dispensés aux assurés sociaux ". Aux termes du deuxième alinéa de ce même III de l'article R. 315-1 : " Le service du contrôle médical exerce ses missions dans les conditions définies par le présent chapitre et par le chapitre 6 du titre VI du livre Ier ", soit les articles R. 166-1 à R. 166-8. L'article R. 315-1-1, en particulier, dispose que : " Lorsque le service du contrôle médical procède à l'analyse de l'activité d'un professionnel de santé en application du IV de l'article L. 315-1, il peut se faire communiquer, dans le cadre de cette mission, l'ensemble des documents, actes, prescriptions et éléments relatifs à cette activité. / Dans le respect des règles de la déontologie médicale, il peut consulter les dossiers médicaux des patients ayant fait l'objet de soins dispensés par le professionnel concerné au cours de la période couverte par l'analyse. Il peut, en tant que de besoin, entendre et examiner ces patients. Il en informe au préalable le professionnel (...) ". <br/>
<br/>
<br/>
              5. Il résulte de la combinaison des dispositions législatives et réglementaires citées au point 4 et de celles de l'article L. 114-10 du code de la sécurité sociale que l'exigence d'agrément et d'assermentation prévue par cet article ne s'applique pas aux médecins-conseils pour l'exercice, sur le fondement du IV de l'article L. 315-1 du code de la sécurité sociale, de leur mission d'analyse de l'activité des professionnels de santé dispensant des soins aux bénéficiaires de l'assurance maladie. Dès lors, la section des assurances sociales du Conseil national de l'ordre des médecins n'a pas commis d'erreur de droit en écartant le moyen par lequel M. B... contestait la validité des éléments recueillis par les médecins-conseils ayant participé aux opérations de contrôle de son activité, faute d'agrément et d'assermentation de ces médecins-conseils. Elle n'a, par ailleurs, pas méconnu son office ni commis d'erreur de droit en relevant, par une appréciation qui n'est pas arguée de dénaturation, qu'il résultait de l'instruction que les témoignages de patients versés au dossier avaient été recueillis par des agents assermentés dans les conditions fixées par l'article L. 114-10 du code de la sécurité sociale.  <br/>
<br/>
              En ce qui concerne les autres moyens :<br/>
<br/>
              6. En premier lieu, si le requérant soutient que la section des assurances sociales a commis une erreur de droit en écartant comme inopérant le moyen tiré de ce que le caractère anonyme des attestations produites a porté atteinte à l'exercice des droits de la défense, il ressort des énonciations de la décision attaquée qu'elle a écarté le moyen comme manquant en fait.  La section des assurances sociales a en particulier relevé, par une appréciation souveraine non entachée de dénaturation, que M. B... avait disposé, lors de la notification de la plainte, d'un tableau de concordance lui permettant d'identifier les patients auteurs de ces attestations, et écarté par voie de conséquence le moyen tiré d'une atteinte aux droits de la défense. Par suite, M. B... n'est pas fondé à soutenir que la section des assurances sociales aurait entaché sa décision d'erreur de droit en écartant ce moyen.<br/>
<br/>
              7. En second lieu, aux termes de l'article 13 C de la nomenclature générale des actes professionnels : " Lorsque la résidence du malade et le domicile professionnel du professionnel de santé ne sont pas situés dans la même agglomération et lorsque la distance qui les sépare est supérieure à deux kilomètres en plaine et un kilomètre en montagne, les frais de déplacement sont remboursés sur la base d'une indemnité kilométrique (...) ". En jugeant que pour l'application de cet article, la notion d'agglomération devait être déterminée par référence à la notion d'" unité urbaine " telle que définie par l'Institut national de la statistique et des études économiques, la section des assurances sociales du Conseil national de l'ordre des médecins n'a pas commis d'erreur de droit.<br/>
<br/>
              8. Il résulte de tout ce qui précède que le pourvoi de M. B... doit être rejeté, y compris ses conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de M. B... la somme de 3 000 euros à verser à la caisse primaire d'assurance maladie du Var et au médecin-conseil, chef de service de l'échelon local du service médical du Var, au titre des mêmes dispositions.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. B... est rejeté.<br/>
Article 2 : M. B... versera à la caisse primaire d'assurance maladie du Var et au médecin-conseil, chef de service de l'échelon local du service médical du Var, la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à M. A... B..., à la caisse primaire d'assurance maladie du Var et au médecin-conseil, chef de service de l'échelon local du service médical du Var.<br/>
Copie en sera adressée au Conseil national de l'ordre des médecins et à la ministre des solidarités et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">55-03-01-04 PROFESSIONS, CHARGES ET OFFICES. CONDITIONS D'EXERCICE DES PROFESSIONS. MÉDECINS. RELATIONS AVEC LA SÉCURITÉ SOCIALE (VOIR : SÉCURITÉ SOCIALE). - EXIGENCE D'AGRÉMENT ET D'ASSERMENTATION DES AGENTS CHARGÉS DU CONTRÔLE (ART. L. 114-10 DU CSS) - CHAMP D'APPLICATION - MÉDECINS-CONSEILS POUR L'EXERCICE DE LEUR MISSION DE CONTRÔLE MÉDICAL (ART. L. 315-1 DU CSS) - EXCLUSION.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">62-02-01 SÉCURITÉ SOCIALE. RELATIONS AVEC LES PROFESSIONS ET LES ÉTABLISSEMENTS SANITAIRES. RELATIONS AVEC LES PROFESSIONS DE SANTÉ. - EXIGENCE D'AGRÉMENT ET D'ASSERMENTATION DES AGENTS CHARGÉS DU CONTRÔLE (ART. L. 114-10 DU CSS) - CHAMP D'APPLICATION - MÉDECINS-CONSEILS POUR L'EXERCICE DE LEUR MISSION DE CONTRÔLE MÉDICAL (ART. L. 315-1 DU CSS) - EXCLUSION.
</SCT>
<ANA ID="9A"> 55-03-01-04 Il résulte de la combinaison du IV de l'article L. 315-1, du III de l'article R. 315-1, des articles R. 315-2, R. 315-1-1 et L. 114-10 du code de la sécurité sociale (CSS) que l'exigence d'agrément et d'assermentation prévue par ce dernier article ne s'applique pas aux médecins-conseils pour l'exercice, sur le fondement du IV de l'article L. 315-1 du CSS, de leur mission d'analyse de l'activité des professionnels de santé dispensant des soins aux bénéficiaires de l'assurance maladie. Dès lors, la section des assurances sociales du Conseil national de l'ordre des médecins n'a pas commis d'erreur de droit en écartant le moyen par lequel le requérant contestait la validité des éléments recueillis par les médecins-conseils ayant participé aux opérations de contrôle de son activité, faute d'agrément et d'assermentation de ces médecins-conseils.</ANA>
<ANA ID="9B"> 62-02-01 Il résulte de la combinaison du IV de l'article L. 315-1, du III de l'article R. 315-1, des articles R. 315-2, R. 315-1-1 et L. 114-10 du code de la sécurité sociale (CSS) que l'exigence d'agrément et d'assermentation prévue par ce dernier article ne s'applique pas aux médecins-conseils pour l'exercice, sur le fondement du IV de l'article L. 315-1 du CSS, de leur mission d'analyse de l'activité des professionnels de santé dispensant des soins aux bénéficiaires de l'assurance maladie. Dès lors, la section des assurances sociales du Conseil national de l'ordre des médecins n'a pas commis d'erreur de droit en écartant le moyen par lequel le requérant contestait la validité des éléments recueillis par les médecins-conseils ayant participé aux opérations de contrôle de son activité, faute d'agrément et d'assermentation de ces médecins-conseils.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
