<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030186689</ID>
<ANCIEN_ID>JG_L_2015_02_000000373520</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/18/66/CETATEXT000030186689.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème / 8ème SSR, 02/02/2015, 373520, Publié au recueil Lebon</TITRE>
<DATE_DEC>2015-02-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>373520</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème / 8ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, BLANCPAIN, SOLTNER, TEXIDOR ; HAAS</AVOCATS>
<RAPPORTEUR>M. Christophe Pourreau</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:373520.20150202</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>VU LA PROCEDURE SUIVANTE :<br/>
<br/>
Procédure contentieuse antérieure<br/>
<br/>
              Le 13 mai 2005, M. G...-H... F... a demandé au tribunal administratif de Marseille d'annuler le contrat du 18 avril 2001 portant recrutement de M. A... C...en tant que collaborateur de cabinet du maire de la commune d'Aix-en-Provence, ainsi que les deux avenants à ce contrat des 23 août 2001 et 24 octobre 2002.<br/>
<br/>
              Par un jugement n° 0502976 du 28 octobre 2008, le tribunal administratif de Marseille, faisant droit à la demande de M. G..., a annulé le contrat du 18 avril 2001 ainsi que les deux avenants des 23 août 2001 et 24 octobre 2002.<br/>
<br/>
              Par un premier arrêt n° 08MA05273 du 7 juin 2011, la cour administrative d'appel de Marseille, faisant droit à l'appel formé par la commune d'Aix-en-Provence et M. C..., a annulé le jugement du tribunal administratif et rejeté la demande de M. G....<br/>
<br/>
              Par une décision n° 351427 du 25 février 2013, le Conseil d'Etat, faisant droit au pourvoi de M. G..., a annulé l'arrêt de la cour administrative d'appel de Marseille et lui a renvoyé l'affaire.<br/>
<br/>
              Par un second arrêt n° 13MA01240 du 17 octobre 2013, la cour administrative d'appel de Marseille a rejeté l'appel formé par la commune d'Aix-en-Provence et M. C... ainsi que les conclusions subsidiaires présentées par M. C... tendant à ce que, si les rémunérations prévues par le contrat attaqué sont jugées illégales, l'action en remboursement soit déclarée prescrite et la responsabilité de la commune soit engagée.<br/>
<br/>
Procédure devant le Conseil d'Etat<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 26 novembre 2013 et 27 février 2014 au secrétariat du contentieux du Conseil d'Etat, la commune d'Aix-en-Provence et M. C... demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt de la cour administrative d'appel de Marseille du 17 octobre 2013 ;<br/>
<br/>
              2°) de mettre à la charge de M. G... la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
	Vu :<br/>
              - les autres pièces du dossier ;<br/>
              - le code général des collectivités territoriales ;<br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - la loi n° 84-53 du 26 janvier 1984 ;<br/>
              - le décret n° 87-1004 du 16 décembre 1987 ;<br/>
              - le code de justice administrative.<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Christophe Pourreau, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Haas, avocat de la commune d'Aix-en-Provence et de M. A...C...et à la SCP Célice, Blancpain, Soltner, Texidor, avocat de M. G...-H... F....<br/>
<br/>
              Vu la note en délibéré, enregistré le 29 janvier 2015, présentée par la commune d'Aix-en-Provence et M.C....<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par contrat du 18 avril 2001, Mme B... C...-E..., maire de la commune d'Aix-en-Provence, a recruté M. A... C...en qualité de collaborateur de cabinet pour remplir les fonctions de conseiller spécial à compter du 26 mars 2001 moyennant un traitement indiciaire correspondant à l'indice majoré 1232 ; que, par une délibération du 17 mai 2001, le conseil municipal a approuvé la création de cinq emplois de collaborateur de cabinet ; que, par deux avenants à son contrat des 23 août 2001 et 24 octobre 2002, M. C... s'est vu confier les fonctions de directeur de cabinet moyennant un traitement indiciaire correspondant à l'indice majoré 1279 ; que, le 13 mai 2005, M. G...-H... F..., conseiller municipal, a demandé au tribunal administratif de Marseille d'annuler le contrat du 18 avril 2001 ainsi que les deux avenants à ce contrat, au motif tiré de l'illégalité des stipulations relatives au niveau de rémunération de M. C... ; que, par un jugement du 28 octobre 2008, le tribunal administratif de Marseille a fait droit à la demande de M. G... ; que, par une décision du 25 février 2013, le Conseil d'Etat, statuant au contentieux a annulé l'arrêt du 7 juin 2011 par lequel la cour administrative d'appel de Marseille, faisant droit aux requêtes de la commune d'Aix-en-Provence et de M. C..., a annulé ce jugement ; que la commune d'Aix-en-Provence et M. C... se pourvoient en cassation contre l'arrêt du 17 octobre 2013 par lequel la cour administrative d'appel de Marseille, statuant sur renvoi après cassation, a rejeté leurs requêtes ainsi que les conclusions subsidiaires de M. C... tendant à ce que, si le niveau de sa rémunération est jugé illégal, l'action en remboursement soit déclarée prescrite et la responsabilité de la commune soit engagée ;<br/>
<br/>
              Sur l'arrêt attaqué en tant qu'il a statué sur les requêtes de la commune d'Aix-en-Provence et de M. C... :<br/>
<br/>
              2. Considérant, en premier lieu, que le moyen tiré de ce que la cour administrative d'appel aurait omis de statuer sur la fin de non-recevoir tirée de l'absence d'intérêt pour agir de M. G... manque en fait ;<br/>
<br/>
              3. Considérant, en deuxième lieu, que, devant la cour administrative d'appel, ni la commune d'Aix-en-Provence, ni M. C... n'ont soutenu que le jugement du tribunal administratif aurait été irrégulier au motif qu'ils n'avaient pas été mis en mesure de connaître, avant l'audience, le sens des conclusions du rapporteur public ; que, par suite, le moyen tiré de ce que l'arrêt de la cour administrative d'appel serait irrégulier au motif qu'il aurait omis de répondre à un  tel moyen ne peut qu'être écarté ;<br/>
<br/>
              4. Considérant, en troisième lieu, que la cour administrative d'appel a jugé que le principe de la liberté contractuelle ne fait pas, en lui-même, obstacle à ce que le pouvoir règlementaire, dans l'intérêt général et de manière proportionnée, fixe le niveau maximal de rémunération de diverses catégories d'agents publics, alors même que ces agents seraient recrutés par voie contractuelle ; que, par suite, le moyen tiré de ce que la cour administrative d'appel aurait répondu de façon insuffisamment motivée au moyen tiré de ce que le décret du 16 décembre 1987 relatif aux collaborateurs de cabinet des autorités territoriales, qui fixe notamment les règles relatives à leur rémunération, méconnaîtrait le principe constitutionnel de liberté contractuelle des collectivités territoriales doit être écarté ;<br/>
<br/>
              5. Considérant, en quatrième lieu, que, s'il ressort des pièces du dossier soumis aux juges du fond que M. G... a participé à la séance du conseil municipal du 17 mai 2001, lors de laquelle a été approuvée la création de cinq emplois de collaborateur de cabinet, ainsi qu'à des séances ultérieures, dont celle du 7 mars 2002, au cours desquelles ont été débattus les indices de rémunération de ces collaborateurs, il ne ressort d'aucune de ces pièces que M. G... aurait eu connaissance, plus de deux mois avant la saisine du tribunal administratif, du contenu du contrat de recrutement de M. C..., dont aucune des délibérations du conseil municipal examinées lors de ces séances n'impliquait nécessairement la conclusion ; que, par suite, en jugeant que M. G... ne pouvait pas être réputé avoir eu connaissance acquise du contrat de recrutement de M. C... plus de deux mois avant la saisine du tribunal administratif, la cour administrative d'appel n'a ni dénaturé les pièces du dossier, ni commis d'erreur de droit ;<br/>
<br/>
              6. Considérant, en cinquième lieu, que les membres de l'organe délibérant d'une collectivité territoriale ou d'un groupement de collectivités territoriales justifient d'un intérêt leur donnant qualité pour contester, devant le juge de l'excès de pouvoir, les contrats de recrutement d'agents non titulaires par la collectivité ou le groupement de collectivités concerné ; que, par suite, le moyen tiré de ce que la cour administrative d'appel aurait commis une erreur de droit en jugeant que M. G... justifiait, en sa qualité de conseiller municipal, d'un intérêt lui donnant qualité pour demander l'annulation du contrat de recrutement de M. C... et des avenants à ce contrat doit être écarté ;<br/>
<br/>
              7. Considérant, en sixième lieu, qu'eu égard aux intérêts dont ils ont la charge, les membres de l'organe délibérant d'une collectivité territoriale ou d'un groupement de collectivités territoriales peuvent invoquer tout moyen à l'appui d'un recours contre de tels contrats de recrutement ; qu'ainsi, le moyen tiré de ce que la cour administrative d'appel aurait commis une erreur de droit en accueillant un moyen, tiré de l'illégalité des stipulations relatives au montant de la rémunération de M. C..., qui ne se rapporte pas à la méconnaissance des prérogatives du conseil municipal, doit également être écarté ;<br/>
<br/>
              8. Considérant, en septième lieu, qu'aux termes de l'article 7 du décret du 16 décembre 1987, dans sa rédaction en vigueur lors de la signature du contrat de recrutement de M. C... : " La rémunération individuelle de chaque collaborateur de cabinet est fixée par l'autorité territoriale. / En aucun cas, cette rémunération ne doit être supérieure à 90% de celle afférente à l'indice terminal de rémunération du fonctionnaire territorial titulaire du grade le plus élevé en fonctions dans la collectivité ou l'établissement public administratif. " ; qu'aux termes du même article, dans sa rédaction en vigueur lors de la signature des deux avenants à ce contrat : " La rémunération individuelle de chaque collaborateur de cabinet est fixée par l'autorité territoriale. / En aucun cas, cette rémunération ne doit être supérieure à 90 % de celle qui correspond à l'indice terminal de l'emploi du fonctionnaire occupant l'emploi administratif fonctionnel de direction le plus élevé de la collectivité ou de l'établissement public. En l'absence de fonctionnaire occupant un tel emploi administratif fonctionnel de direction, cette rémunération ne doit pas être supérieure à 90 % de celle qui correspond à l'indice terminal du grade détenu par le fonctionnaire territorial titulaire du grade le plus élevé en fonctions dans la collectivité ou l'établissement public. " ; qu'il résulte de ces dispositions que l'autorité territoriale ne peut attribuer à un collaborateur de cabinet un traitement indiciaire supérieur à 90% du traitement indiciaire correspondant à l'indice terminal de rémunération du fonctionnaire occupant l'emploi administratif fonctionnel de direction le plus élevé de la collectivité ou de l'établissement public ou, à défaut, du fonctionnaire en fonction dans la collectivité ou l'établissement public ayant le grade le plus élevé ; qu'en jugeant que les clauses du contrat de recrutement de M. C... et du second avenant à ce contrat relatives à sa rémunération étaient illégales au motif que le traitement indiciaire qui lui était attribué excédait cette limite, la cour administrative d'appel a retenu une interprétation des dispositions de l'article 7 du décret du 16 décembre 1987 qui n'est pas entachée d'erreur de droit ;<br/>
<br/>
              Sur l'arrêt attaqué en tant qu'il a statué sur les conclusions subsidiaires de M. C... :<br/>
<br/>
              9. Considérant qu'il est constant que, pour rejeter les conclusions subsidiaires présentées pour la première fois par M. C... devant la cour administrative d'appel et qui tendaient à ce qu'en cas d'annulation du contrat du 18 avril 2001 et des avenants à ce contrat, l'action en remboursement de ses rémunérations soit déclarée prescrite et la responsabilité de la commune soit engagée, la cour administrative d'appel a relevé d'office le moyen tiré de ce que ces conclusions soulevaient un litige distinct du litige d'excès de pouvoir dont elle était saisie, sans avoir informé les parties, avant la séance de jugement, que son arrêt pouvait être fondé sur ce moyen et sans les avoir invitées à présenter leurs observations sur ce moyen ; que, par suite, en tant qu'il a statué sur les conclusions subsidiaires de M. C..., l'arrêt attaqué est entaché d'irrégularité ; qu'il doit, dans cette mesure, être annulé ;<br/>
<br/>
              10. Considérant qu'aux termes du second alinéa de l'article L. 821-2 du code de justice administrative : " Lorsque l'affaire fait l'objet d'un second pourvoi en cassation, le Conseil d'Etat statue définitivement sur cette affaire " ; qu'il y a lieu, par suite, de régler l'affaire au fond dans la mesure de l'annulation ainsi prononcée ;<br/>
<br/>
              11. Considérant que les conclusions subsidiaires présentées pour la première fois en appel par M. C... soulèvent un litige distinct du litige d'excès de pouvoir qui a fait l'objet de l'appel de la commune d'Aix-en-Provence ; qu'elles sont, par suite, irrecevables ;<br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              12. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune d'Aix-en-Provence et de M. C... la somme globale de 3 000 euros, à verser à M. G..., au titre des dispositions de l'article L. 761-1 du code de justice administrative ; qu'en revanche, ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de M. G..., qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'article 2 de l'arrêt de la cour administrative d'appel de Marseille du 17 octobre 2013 est annulé.<br/>
<br/>
Article 2 : Les conclusions subsidiaires présentées par M. C... devant la cour administrative d'appel et le surplus des conclusions du pourvoi sont rejetées.<br/>
<br/>
Article 3 : La commune d'Aix-en-Provence et M. C... verseront à M. G... une somme globale de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la commune d'Aix-en-Provence, à M. A... C...et à M. G...-H... F....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-12 FONCTIONNAIRES ET AGENTS PUBLICS. AGENTS CONTRACTUELS ET TEMPORAIRES. - RECOURS CONTRE LES CONTRATS DE RECRUTEMENT - 1) NATURE DU RECOURS - EXCÈS DE POUVOIR [RJ1] - 2) RECOURS FORMÉ PAR LES MEMBRES DE L'ORGANE DÉLIBÉRANT D'UNE COLLECTIVITÉ TERRITORIALE OU D'UN GROUPEMENT DE COLLECTIVITÉS - A) INTÉRÊT POUR AGIR - EXISTENCE - B) POSSIBILITÉ D'INVOQUER TOUT MOYEN - EXISTENCE [RJ2].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">36-13-01 FONCTIONNAIRES ET AGENTS PUBLICS. CONTENTIEUX DE LA FONCTION PUBLIQUE. CONTENTIEUX DE L'ANNULATION. - CONTRATS DE RECRUTEMENT D'AGENTS PUBLICS NON TITULAIRES - NATURE DU RECOURS - EXCÈS DE POUVOIR [RJ1].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">36-13-01-02-03 FONCTIONNAIRES ET AGENTS PUBLICS. CONTENTIEUX DE LA FONCTION PUBLIQUE. CONTENTIEUX DE L'ANNULATION. INTRODUCTION DE L'INSTANCE. INTÉRÊT POUR AGIR. - MEMBRES DE L'ORGANE DÉLIBÉRANT D'UNE COLLECTIVITÉ TERRITORIALE OU D'UN GROUPEMENT DE COLLECTIVITÉS - 1) INTÉRÊT À CONTESTER LES CONTRATS DE RECRUTEMENT D'AGENTS NON TITULAIRES PAR LA COLLECTIVITÉ OU LE GROUPEMENT DE COLLECTIVITÉS- EXISTENCE - 2) POSSIBILITÉ D'INVOQUER TOUT MOYEN CONTRE CES CONTRATS - EXISTENCE [RJ2].
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">39-08-01-01 MARCHÉS ET CONTRATS ADMINISTRATIFS. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. RECEVABILITÉ. RECEVABILITÉ DU RECOURS POUR EXCÈS DE POUVOIR EN MATIÈRE CONTRACTUELLE. - RECOURS CONTRE LES CONTRATS DE RECRUTEMENT D'AGENTS NON TITULAIRES - 1) NATURE DU RECOURS - EXCÈS DE POUVOIR [RJ1] - 2) RECOURS FORMÉ PAR LES MEMBRES DE L'ORGANE DÉLIBÉRANT D'UNE COLLECTIVITÉ TERRITORIALE OU D'UN GROUPEMENT DE COLLECTIVITÉS - A) INTÉRÊT POUR AGIR - EXISTENCE - B) POSSIBILITÉ D'INVOQUER TOUT MOYEN - EXISTENCE [RJ2].
</SCT>
<SCT ID="8E" TYPE="PRINCIPAL">54-01-04-02-01 PROCÉDURE. INTRODUCTION DE L'INSTANCE. INTÉRÊT POUR AGIR. EXISTENCE D'UN INTÉRÊT. INTÉRÊT LIÉ À UNE QUALITÉ PARTICULIÈRE. - MEMBRES DE L'ORGANE DÉLIBÉRANT D'UNE COLLECTIVITÉ TERRITORIALE OU D'UN GROUPEMENT DE COLLECTIVITÉS - 1) INTÉRÊT À CONTESTER LES CONTRATS DE RECRUTEMENT D'AGENTS NON TITULAIRES PAR LA COLLECTIVITÉ OU LE GROUPEMENT DE COLLECTIVITÉS - EXISTENCE - 2) POSSIBILITÉ D'INVOQUER TOUT MOYEN CONTRE CES CONTRATS - EXISTENCE [RJ2].
</SCT>
<SCT ID="8F" TYPE="PRINCIPAL">54-02-01-01 PROCÉDURE. DIVERSES SORTES DE RECOURS. RECOURS POUR EXCÈS DE POUVOIR. RECOURS AYANT CE CARACTÈRE. - RECOURS CONTRE LES CONTRATS DE RECRUTEMENT D'AGENTS PUBLICS NON TITULAIRES [RJ1].
</SCT>
<ANA ID="9A"> 36-12 1) Les recours contre les contrats de recrutement d'agents publics non titulaires relèvent du contentieux de l'excès de pouvoir.,,,2) a) Les membres de l'organe délibérant d'une collectivité territoriale ou d'un groupement de collectivités territoriales justifient d'un intérêt leur donnant qualité pour contester, devant le juge de l'excès de pouvoir, les contrats de recrutement d'agents non titulaires par la collectivité ou le groupement de collectivités concerné.,,,b) Eu égard aux intérêts dont ils ont la charge, les membres de l'organe délibérant d'une collectivité territoriale ou d'un groupement de collectivités territoriales peuvent invoquer tout moyen à l'appui d'un recours contre de tels contrats de recrutement.</ANA>
<ANA ID="9B"> 36-13-01 Les recours contre les contrats de recrutement d'agents publics non titulaires relèvent du contentieux de l'excès de pouvoir.</ANA>
<ANA ID="9C"> 36-13-01-02-03 1) Les membres de l'organe délibérant d'une collectivité territoriale ou d'un groupement de collectivités territoriales justifient d'un intérêt leur donnant qualité pour contester, devant le juge de l'excès de pouvoir, les contrats de recrutement d'agents non titulaires par la collectivité ou le groupement de collectivités concerné.,,,2) Eu égard aux intérêts dont ils ont la charge, les membres de l'organe délibérant d'une collectivité territoriale ou d'un groupement de collectivités territoriales peuvent invoquer tout moyen à l'appui d'un recours contre de tels contrats de recrutement.</ANA>
<ANA ID="9D"> 39-08-01-01 1) Les recours contre les contrats de recrutement d'agents publics non titulaires relèvent du contentieux de l'excès de pouvoir.,,,2) a) Les membres de l'organe délibérant d'une collectivité territoriale ou d'un groupement de collectivités territoriales justifient d'un intérêt leur donnant qualité pour contester, devant le juge de l'excès de pouvoir, les contrats de recrutement d'agents non titulaires par la collectivité ou le groupement de collectivités concerné.,,,b) Eu égard aux intérêts dont ils ont la charge, les membres de l'organe délibérant d'une collectivité territoriale ou d'un groupement de collectivités territoriales peuvent invoquer tout moyen à l'appui d'un recours contre de tels contrats de recrutement.</ANA>
<ANA ID="9E"> 54-01-04-02-01 1) Les membres de l'organe délibérant d'une collectivité territoriale ou d'un groupement de collectivités territoriales justifient d'un intérêt leur donnant qualité pour contester, devant le juge de l'excès de pouvoir, les contrats de recrutement d'agents non titulaires par la collectivité ou le groupement de collectivités concerné.,,,2) Eu égard aux intérêts dont ils ont la charge, les membres de l'organe délibérant d'une collectivité territoriale ou d'un groupement de collectivités territoriales peuvent invoquer tout moyen à l'appui d'un recours contre de tels contrats de recrutement.</ANA>
<ANA ID="9F"> 54-02-01-01 Les recours contre les contrats de recrutement d'agents publics non titulaires relèvent du contentieux de l'excès de pouvoir.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Section, 30 octobre 1998, Ville de Lisieux, n° 149662, p. 375.,,[RJ2] Rappr. CE, Assemblée, 4 avril 2014, Département de Tarn-et-Garonne, n° 358994, p. 70.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
