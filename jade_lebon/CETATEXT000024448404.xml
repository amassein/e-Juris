<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000024448404</ID>
<ANCIEN_ID>JG_L_2011_07_000000349043</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/24/44/84/CETATEXT000024448404.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème et 7ème sous-sections réunies, 26/07/2011, 349043</TITRE>
<DATE_DEC>2011-07-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>349043</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème et 7ème sous-sections réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Christian Vigouroux</PRESIDENT>
<AVOCATS/>
<RAPPORTEUR>Mme Catherine Chadelat</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier de Lesquen</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu l'ordonnance n° 0906367 du 2 mai 2011, enregistrée le 6 mai 2011 au secrétariat du contentieux du Conseil d'Etat, par laquelle le président de la première section du tribunal administratif de Paris, avant de statuer sur la demande de la SOCIETE FRANCAISE DU RADIOTELEPHONE (SFR), tendant 1°) à annuler la décision implicite par laquelle le receveur général des finances a rejeté sa demande en date du 15 décembre 2008 tendant au versement des intérêts moratoires afférents au versement indu de la taxe de gestion et de contrôle pour les années 1999 à 2002 par les sociétés Kertel, LD COM, LD COM CS, BLR Services, Kaptech, Kapt'SA, Kapt' Holding, Squadran, Fortel, Ventelo, GTS/Omnicom, Firstmark, Belgacom, 9 Telecom Réseau et SIRIS aux droits desquels vient SFR et des intérêts de ces intérêts à compter de la date de remboursement des sommes en cause, 2°) à mettre à la charge de l'Etat le versement à SFR, d'une part, des intérêts moratoires susmentionnés, d'autre part, des intérêts de ces intérêts à compter de la date de remboursement des sommes en cause, et enfin, la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative, a décidé, par application des dispositions de l'article 23-2 de l'ordonnance n° 58 1067 du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel, de transmettre au Conseil d'Etat le moyen tiré de ce que l'interprétation jurisprudentielle des dispositions de l'article 1153 du code civil, en ce qu'elle fixe le point de départ des intérêts moratoires dus en cas de restitution d'une imposition non soumise au livre des procédures fiscales, et indûment perçue par l'administration fiscale, à la date de la demande adressée par le contribuable et non à la date de paiement des sommes en cause, porte atteinte aux droits et libertés garantis par la Constitution ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution, notamment son article 61-1 ;<br/>
<br/>
              Vu l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
<br/>
              Vu le code civil, notamment son article 1153 ;<br/>
<br/>
              Vu le livre des procédures fiscales, notamment son article L. 208 ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Catherine Chadelat, Conseiller d'Etat,<br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant qu'il résulte des dispositions de l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que, lorsqu'une juridiction relevant du Conseil d'Etat a transmis à ce dernier, en application de l'article 23-2 de cette même ordonnance, la question de la conformité à la Constitution d'une disposition législative, le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              Considérant qu'aux termes de l'article 1153 du code civil : " Dans les obligations qui se bornent au paiement d'une certaine somme, les dommages-intérêts résultant du retard dans l'exécution ne consistent jamais que dans la condamnation aux intérêts au taux légal, sauf les règles particulières au commerce et au cautionnement " et que ces dommages et intérêts " ne sont dus que du jour de la sommation de payer ou d'un autre acte équivalent telle une lettre missive s'il en ressort une interpellation suffisante, excepté le cas où la loi les fait courir de plein droit " ; que la société SFR soutient que ces dispositions seraient contraires au principe d'égalité devant la loi et au principe d'égalité devant l'impôt et les charges publiques, en ce qu'elles retiennent un point de départ des intérêts moins favorable que les dispositions de l'article L. 208 du livre des procédures fiscales, aux termes desquelles : " Quand l'Etat est condamné à un dégrèvement d'impôt par un tribunal ou quand un dégrèvement est prononcé par l'administration à la suite d'une réclamation tendant à la réparation d'une erreur commise dans l'assiette ou le calcul des impositions, les sommes déjà perçues sont remboursées au contribuable et donnent lieu au paiement d'intérêts moratoires dont le taux est celui de l'intérêt de retard prévu à l'article 1727 du code général des impôts. Les intérêts courent du jour du paiement (...) " ;<br/>
<br/>
              Considérant que le principe d'égalité ne s'oppose ni à ce que législateur règle de façon différente des situations différentes, ni à ce qu'il déroge à l'égalité pour des raisons d'intérêt général, pourvu que, dans l'un et l'autre cas, la différence de traitement qui en résulte soit en rapport direct avec l'objet de la loi qui l'établit ; que les dispositions de l'article 1153 du code civil, de portée générale, qui n'introduisent, par elles-mêmes, aucune distinction entre les bénéficiaires du régime qu'elles définissent, ont pour objet de régir des situations différentes de celles régies par les dispositions particulières de l'article L. 208 du livre des procédures fiscales ; que, dès lors le point de départ des intérêts moratoires dus en cas de restitution d'une imposition indue peut être différent pour l'application de chacune de ces dispositions ; <br/>
<br/>
              Considérant que, dès lors, la question soulevée, qui n'est pas nouvelle, ne présente pas, en tout état de cause, un caractère sérieux ; qu'il résulte de ce qui précède qu'il n'y a pas lieu de la renvoyer au Conseil constitutionnel ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par la SOCIETE FRANCAISE DU RADIOTELEPHONE.<br/>
<br/>
Article 2 : La présente décision sera notifiée à la SOCIETE FRANCAISE DU RADIOTELEPHONE, à l'Autorité de régulation des communications électroniques et des postes, à la ministre du budget, des comptes publics et de la réforme de l'Etat, porte-parole du gouvernement, au ministre de l'économie, des finances et de l'industrie et au Premier ministre.<br/>
<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-10-05-04-02 PROCÉDURE. - ARTICLE 1153 DU CODE CIVIL - PRINCIPE D'ÉGALITÉ.
</SCT>
<ANA ID="9A"> 54-10-05-04-02 Les dispositions de portée générale de l'article 1153 du code civil, fixant le point de départ de principe des intérêts résultant du retard dans l'exécution d'une obligation au jour de la sommation de payer ou d'un autre acte équivalent, qui n'introduisent, par elles-mêmes, aucune distinction entre les bénéficiaires du régime qu'elles définissent, ont pour objet de régir des situations différentes de celles régies par les dispositions particulières de l'article L. 208 du livre des procédures fiscales, fixant le point de départ des intérêts moratoires dus par l'administration fiscale dans certains cas de dégrèvement au jour du paiement. Dès lors le point de départ des intérêts moratoires dus en cas de restitution d'une imposition indue peut être différent pour l'application de chacune de ces dispositions et la question de la non-conformité de ces dispositions au principe d'égalité, qui n'est pas nouvelle, ne présente pas de caractère sérieux.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
