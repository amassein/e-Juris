<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035317238</ID>
<ANCIEN_ID>JG_L_2017_07_000000397513</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/31/72/CETATEXT000035317238.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 4ème chambres réunies, 28/07/2017, 397513</TITRE>
<DATE_DEC>2017-07-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>397513</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 4ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROUSSEAU, TAPIE</AVOCATS>
<RAPPORTEUR>M. Olivier Rousselle</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:397513.20170728</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B... A...a demandé au tribunal administratif de Paris de condamner  l'Etat à lui verser la somme de 17 542 euros en réparation des préjudices résultant de son absence de relogement. Par un jugement n°1430256/6-2 du 24 décembre 2015, le tribunal administratif  a rejeté sa demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 1er mars et 13 mai 2016 au secrétariat du contentieux du Conseil d'Etat, M. A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3000 euros au titre de l'article L.761-1 du code de justice administrative et de l'article 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - le code de la construction et de l'habitation ;<br/>
<br/>
              -la loi n° 91-647 du 10 juillet 1991 ; <br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Rousselle, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rousseau, Tapie, avocat de M.A....<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. A... a été reconnu comme prioritaire et devant être relogé en urgence, sur le fondement de l'article L. 441-2-3 du code de la construction et de l'habitation, par une décision du 25 janvier 2013 de la commission de médiation de Paris, au double motif que les éléments fournis sur son logement permettaient de caractériser une situation d'urgence compte tenu de la composition de son ménage et que sa demande de logement social, présentée douze ans auparavant, n'avait pas reçu de réponse dans le délai réglementaire ; que, par un jugement du 8 octobre 2013, le magistrat désigné par le président du tribunal administratif de Paris, saisi par M. A...sur le fondement du I de l'article L. 441-2-3-1 du code de la construction et de l'habitation, a enjoint au préfet de la région Ile-de-France, préfet de Paris d'assurer le relogement de M. A...; que, constatant le défaut d'exécution du jugement du 8 octobre 2013, M. A...a demandé au tribunal de condamner l'Etat à lui verser 17 542 euros en réparation du préjudice subi du fait de son absence de relogement ; qu'il se pourvoit en cassation contre le jugement du 24 décembre 2015 par lequel le tribunal administratif a rejeté cette demande ;<br/>
<br/>
              2. Considérant que, lorsqu'une personne a été reconnue comme prioritaire et comme devant être logée ou relogée d'urgence par une commission de médiation, en application des dispositions de l'article L. 441-2-3 du code de la construction et de l'habitation, la carence fautive de l'Etat à exécuter cette décision dans le délai imparti engage sa responsabilité à l'égard du seul demandeur, au titre des troubles dans les conditions d'existence résultant du maintien de la situation qui a motivé la décision de la commission, alors même que l'intéressé n'a pas fait usage du recours en injonction contre l'Etat prévu par l'article L. 441-2-3-1 du code de la construction et de l'habitation ; que ces troubles doivent être appréciés en fonction des conditions de logement qui ont perduré du fait de la carence de l'Etat, de la durée de cette carence et du nombre de personnes composant le foyer du demandeur pendant la période de responsabilité de l'Etat, qui court à compter de l'expiration du délai de trois ou six mois à compter de la décision de la commission de médiation que les dispositions de l'article R. 441-16-1 du code de la construction et de l'habitation impartissent au préfet pour provoquer une offre de logement; que la circonstance que l'absence de relogement a contraint le demandeur à supporter un loyer manifestement disproportionné au regard de ses ressources, si elle ne peut donner lieu à l'indemnisation d'un préjudice pécuniaire égal à la différence entre le montant du loyer qu'il a payé durant cette période et celui qu'il aurait acquitté si un logement social lui avait été attribué, doit, si elle est établie, être prise en compte pour évaluer le préjudice résultant des troubles dans les conditions d'existence ; <br/>
<br/>
              3. Considérant qu'il résulte de ce qui précède qu'ayant constaté que le préfet n'avait pas proposé un relogement à M. A...dans le délai prévu par le code de la construction et de l'habitation à compter de la décision de la commission de médiation, le tribunal administratif de Paris ne pouvait, sans commettre une erreur de droit, juger que cette carence, constitutive d'une faute de nature à engager la responsabilité de l'Etat, ne causait à l'intéressé aucun préjudice réel, direct et certain, alors qu'il était constant que la situation qui avait motivé la décision de la commission perdurait et que M. A...justifiait de ce fait de troubles dans ses conditions d'existence lui ouvrant droit à réparation ; que le requérant est, par suite, fondé à demander l'annulation du jugement qu'il attaque ;<br/>
<br/>
              4. Considérant que M. A...a obtenu le bénéfice de l'aide juridictionnelle ; que, par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 relative à l'aide juridique; qu'il y a lieu, dans les circonstances de l'espèce, et sous réserve que la SCP Rousseau et Tapie, avocat de M. A..., renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de l'Etat la somme de 1 500 euros à verser à la SCP Rousseau et Tapie ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
Article 1er : Le jugement du tribunal administratif de Paris du 24 décembre 2015 est annulé.<br/>
Article 2 : L'affaire est renvoyée devant le tribunal administratif de Paris.<br/>
Article 3 : L'Etat versera à la SCP Rousseau et Tapie, avocat de M. A..., une somme de 1 500 euros en application des dispositions de l'article L. 761-1 du code de justice administrative et du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
Article 4 : La présente décision sera notifiée à M. D... C...et au ministre de la cohésion des territoires.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">38-07-01 LOGEMENT. - RESPONSABILITÉ DE L'ETAT À RAISON DE LA CARENCE FAUTIVE À ASSURER LE LOGEMENT D'UN DEMANDEUR RECONNU PRIORITAIRE ET URGENT - EVALUATION DU PRÉJUDICE [RJ1] - CAS DU PRÉJUDICE RÉSULTANT DE LA DIFFÉRENCE ENTRE LE LOYER SUPPORTÉ ET CELUI QUI AURAIT ÉTÉ ACQUITTÉ DANS LE PARC SOCIAL - INDEMNISATION - ABSENCE - PRISE EN COMPTE POUR L'ÉVALUATION DES TROUBLES DANS LES CONDITIONS D'EXISTENCE - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">60-02-012 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RESPONSABILITÉ EN RAISON DES DIFFÉRENTES ACTIVITÉS DES SERVICES PUBLICS. SERVICES SOCIAUX. - DALO - RESPONSABILITÉ DE L'ETAT À RAISON DE LA CARENCE FAUTIVE À ASSURER LE LOGEMENT D'UN DEMANDEUR RECONNU PRIORITAIRE ET URGENT - EVALUATION DU PRÉJUDICE [RJ1] - CAS DU PRÉJUDICE RÉSULTANT DE LA DIFFÉRENCE ENTRE LE LOYER SUPPORTÉ ET CELUI QUI AURAIT ÉTÉ ACQUITTÉ DANS LE PARC SOCIAL - INDEMNISATION - ABSENCE - PRISE EN COMPTE POUR L'ÉVALUATION DES TROUBLES DANS LES CONDITIONS D'EXISTENCE - EXISTENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">60-04-03-03 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RÉPARATION. ÉVALUATION DU PRÉJUDICE. TROUBLES DANS LES CONDITIONS D'EXISTENCE. - DALO - TROUBLES DANS LES CONDITIONS D'EXISTENCE RÉSULTANT DE L'ABSENCE DE RELOGEMENT D'UN DEMANDEUR RECONNU PRIORITAIRE ET URGENT - APPRÉCIATION [RJ1] - CAS DU PRÉJUDICE RÉSULTANT DE LA DIFFÉRENCE ENTRE LE LOYER SUPPORTÉ ET CELUI QUI AURAIT ÉTÉ ACQUITTÉ DANS LE PARC SOCIAL - INDEMNISATION - ABSENCE - PRISE EN COMPTE POUR L'ÉVALUATION DES TROUBLES DANS LES CONDITIONS D'EXISTENCE - EXISTENCE.
</SCT>
<ANA ID="9A"> 38-07-01 Engagement de la responsabilité de l'Etat à raison de la carence fautive à assurer le logement d'un demandeur reconnu prioritaire et urgent par une commission de médiation (art. L. 441-2-3 du code de la construction et de l'habitation), au titre des troubles dans ses conditions d'existence.... ,,La circonstance que l'absence de relogement a contraint le demandeur à supporter un loyer manifestement disproportionné au regard de ses ressources, si elle ne peut donner lieu à l'indemnisation d'un préjudice pécuniaire égal à la différence entre le montant du loyer qu'il a payé durant cette période et celui qu'il aurait acquitté si un logement social lui avait été attribué, doit, si elle est établie, être prise en compte pour évaluer le préjudice résultant des troubles dans les conditions d'existence.</ANA>
<ANA ID="9B"> 60-02-012 Engagement de la responsabilité de l'Etat à raison de la carence fautive à assurer le logement d'un demandeur reconnu prioritaire et urgent par une commission de médiation (art. L. 441-2-3 du code de la construction et de l'habitation), au titre des troubles dans ses conditions d'existence.... ,,La circonstance que l'absence de relogement a contraint le demandeur à supporter un loyer manifestement disproportionné au regard de ses ressources, si elle ne peut donner lieu à l'indemnisation d'un préjudice pécuniaire égal à la différence entre le montant du loyer qu'il a payé durant cette période et celui qu'il aurait acquitté si un logement social lui avait été attribué, doit, si elle est établie, être prise en compte pour évaluer le préjudice résultant des troubles dans les conditions d'existence.</ANA>
<ANA ID="9C"> 60-04-03-03 Engagement de la responsabilité de l'Etat à raison de la carence fautive à assurer le logement d'un demandeur reconnu prioritaire et urgent par une commission de médiation (art. L. 441-2-3 du code de la construction et de l'habitation), au titre des troubles dans ses conditions d'existence.... ,,La circonstance que l'absence de relogement a contraint le demandeur à supporter un loyer manifestement disproportionné au regard de ses ressources, si elle ne peut donner lieu à l'indemnisation d'un préjudice pécuniaire égal à la différence entre le montant du loyer qu'il a payé durant cette période et celui qu'il aurait acquitté si un logement social lui avait été attribué, doit, si elle est établie, être prise en compte pour évaluer le préjudice résultant des troubles dans les conditions d'existence.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 13 juillet 2016, Mme,, n° 382872, T. p. 945 ; CE, 16 décembre 2016, M.,, n° 383111, p. 563 ; CE, 19 juillet 2017, Consorts,n° 402172, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
