<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031184155</ID>
<ANCIEN_ID>JG_L_2015_09_000000387315</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/18/41/CETATEXT000031184155.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème / 3ème SSR, 18/09/2015, 387315</TITRE>
<DATE_DEC>2015-09-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>387315</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème / 3ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DE NERVO, POUPET ; SCP MASSE-DESSEN, THOUVENIN, COUDRAY</AVOCATS>
<RAPPORTEUR>Mme Esther de Moustier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Nathalie Escaut</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:387315.20150918</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La chambre de commerce et d'industrie de la Région Guyane a demandé au tribunal administratif de Cayenne d'ordonner, sous astreinte, sur le fondement de l'article L. 521-3 du code de justice administrative, l'expulsion de la société Prest'air du hangar qu'elle occupe, situé sur le domaine public de l'aéroport de Cayenne - Félix Eboué. Par une ordonnance n° 1401284 du 9 décembre 2014, le juge des référés du tribunal administratif de Cayenne a enjoint à la société Prest'air et à tout occupant de son chef de libérer le hangar de 376 m² et les bureaux d'une surface de 135 m² qu'elle occupe dans la zone de fret, côté piste, de l'aéroport Félix-Eboué dans un délai de huit jours et sous astreinte de 200 euros par jour de retard, au besoin avec le concours de la force publique.<br/>
<br/>
               Par un pourvoi sommaire, un mémoire complémentaire et deux mémoires en  réplique, enregistrés les 22 janvier, 5 février, 30 avril et 2 juillet 2015 au secrétariat du contentieux du Conseil d'État, la société Prest'air demande au Conseil d'État d'annuler cette ordonnance, de rejeter la demande de la chambre de commerce et d'industrie de la Région Guyane et de mettre à la charge de cette dernière la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Esther de Moustier, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Nathalie Escaut, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP de Nervo, Poupet, avocat de la société Prest'air et à la SCP Masse-Dessen, Thouvenin, Coudray, avocat de la chambre de commerce et d'industrie de la Guyane ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 521-3 du code de justice administrative : " En cas d'urgence et sur simple requête qui sera recevable même en l'absence de décision administrative préalable, le juge des référés peut ordonner toutes autres mesures utiles sans faire obstacle à l'exécution d'aucune décision administrative " ; que lorsque le juge des référés est saisi, sur le fondement de ces dispositions d'une demande d'expulsion d'un occupant du domaine public, il lui appartient de rechercher si, au jour où il statue, cette demande présente un caractère d'urgence et ne se heurte à aucune contestation sérieuse ;<br/>
<br/>
              2. Considérant que lorsque le juge des référés statue, sur le fondement de l'article L. 521-3 du code de justice administrative, qui instaure une procédure de référé pour laquelle la tenue d'une audience publique n'est pas prévue par les dispositions de l'article L. 522-1 du même code, sur une demande d'expulsion d'un occupant du domaine public, il doit, eu égard au caractère quasi-irréversible de la mesure qu'il peut être conduit à prendre, aux effets de celle-ci sur la situation des personnes concernées et dès lors qu'il se prononce en dernier ressort, mettre les parties à même de présenter, au cours d'une audience publique, des observations orales à l'appui de leurs observations écrites ; <br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier soumis au juge des référés du tribunal administratif de Cayenne que le courrier du 1er décembre 2014, par lequel le tribunal a communiqué à la société Prest'air la demande en référé de la chambre de commerce et d'industrie de la Région Guyane et qui valait également convocation à l'audience du 8 décembre 2014, a été distribué le 4 décembre 2014 à la société Actalis Montjoly dont l'adresse était la même que celle de la société requérante, mais qui n'était pas mandatée pour recevoir son courrier ; qu'il résulte d'une attestation de la société Actalis Montjoly, dont les termes ne sont pas critiqués, que ce courrier n'a été remis à la société Prest'air que le 19 décembre 2014, soit postérieurement à l'audience qui s'est tenue le 8 décembre 2014 ; qu'ainsi, la société n'a pas été mise à même de présenter des observations écrites ou orales et d'assister à l'audience publique ; que, dès lors, l'ordonnance attaquée est intervenue au terme d'une procédure irrégulière ; que, par suite, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, la société Prest'air est fondée à en demander l'annulation ; <br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au titre de la procédure de référé engagée par la chambre de commerce et d'industrie de la Région Guyane, en application de l'article L. 821-2 du code de justice administrative ; <br/>
<br/>
              5. Considérant qu'il ne peut y avoir transfert d'une autorisation ou d'une convention d'occupation du domaine public à un nouveau bénéficiaire que si le gestionnaire de ce domaine a donné son accord écrit ; <br/>
<br/>
              6. Considérant qu'il résulte de l'instruction que la société Prest'air occupe depuis le mois de janvier 2012 un hangar de 376 m² et des locaux attenants à usage de bureau de 135 m² dans la zone de fret de l'aéroport Félix-Eboué de Cayenne, où elle exploite une activité de transport aérien privé ; que si des négociations ont eu lieu et si un projet de convention a été transmis à la société en 2013, couvrant la période précédente à titre de régularisation, il est constant qu'aucune convention d'occupation du domaine public aéroportuaire n'a été conclue entre la société Prest'air et la chambre de commerce et d'industrie qui en est le gestionnaire ; que les circonstances que la chambre de commerce et d'industrie ne s'est pas formellement opposée à cette occupation durant les négociations et qu'elle a émis des factures depuis l'année 2012 n'impliquent pas que la société bénéficiait d'une autorisation d'occupation du domaine public ; que la reprise de l'activité de la société Air Amazonie par la société Prest'Air en 2012 n'a pas eu pour effet de lui transférer la convention d'occupation du domaine public dont bénéficiait antérieurement la société Air Amazonie, en l'absence d'accord écrit du gestionnaire du domaine public à cet égard ; qu'il résulte de ce qui précède que la société Prest'air occupe sans droit ni titre les locaux en cause ; que, par suite, la demande en référé de la chambre de commerce et d'industrie ne se heurte à aucune contestation sérieuse ;<br/>
<br/>
              	7. Mais considérant que, contrairement aux affirmations de la chambre de commerce et d'industrie, la société Prest'air justifie que l'activité de transport aérien qu'elle exerce est couverte par une assurance responsabilité civile au titre des deux avions Cessna qu'elle exploite et qu'elle loue à la société Avialoc ; que M.A..., son gérant, dispose d'une licence de pilote " SEP Terrestre " correspondant à des avions monomoteurs à pistons, valide du 21 novembre 2013 au 30 novembre 2015 ; qu'ainsi, alors même que les locaux que la société Prest'air occupe n'auraient pas fait l'objet d'une assurance, cette occupation ne peut être regardée comme compromettant la sécurité des autres occupants de l'aéroport ; que la seule perte de recettes budgétaires alléguée et l'impossibilité qu'un autre occupant puisse s'installer dans ces locaux, alors qu'il n'est fait état d'aucun projet particulier en ce sens, ne suffisent pas à caractériser l'urgence requise pour justifier l'intervention d'une mesure de la nature de celles qui peuvent être ordonnées par le juge des référés sur le fondement de l'article L. 521-3 du code de justice administrative ;<br/>
<br/>
              8. Considérant qu'il résulte de ce qui précède que la demande de la chambre de commerce et d'industrie de la Région Guyane doit être rejetée ; <br/>
<br/>
              9. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la société Prest'air, qui n'est pas, dans la présente instance, la partie perdante ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la chambre de commerce et d'industrie de la Région Guyane la somme de 1 000 euros à verser à la société Prest'air au titre de ces mêmes dispositions ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'ordonnance du juge des référés du tribunal administratif de Cayenne du 9 décembre 2014 est annulée. <br/>
<br/>
Article 2 : La demande en référé de la chambre de commerce et d'industrie de la Région Guyane et ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
<br/>
Article 3 : La chambre de commerce et d'industrie de la Région Guyane versera à la société Prest'air la somme de 1 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la société Prest'air et à la chambre de commerce et d'industrie de la Région Guyane.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">24-01-02-01-01-01 DOMAINE. DOMAINE PUBLIC. RÉGIME. OCCUPATION. UTILISATIONS PRIVATIVES DU DOMAINE. AUTORISATIONS UNILATÉRALES. - TRANSFERT D'UNE AUTORISATION - CONDITION - ACCORD ÉCRIT DU GESTIONNAIRE DU DOMAINE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">24-01-02-01-01-02 DOMAINE. DOMAINE PUBLIC. RÉGIME. OCCUPATION. UTILISATIONS PRIVATIVES DU DOMAINE. CONTRATS ET CONCESSIONS. - TRANSFERT DE CONVENTION - CONDITION - ACCORD ÉCRIT DU GESTIONNAIRE DU DOMAINE [RJ1].
</SCT>
<ANA ID="9A"> 24-01-02-01-01-01 Il ne peut y avoir transfert d'une autorisation ou d'une convention d'occupation du domaine public à un nouveau bénéficiaire que si le gestionnaire de ce domaine a donné son accord écrit.</ANA>
<ANA ID="9B"> 24-01-02-01-01-02 Il ne peut y avoir transfert d'une autorisation ou d'une convention d'occupation du domaine public à un nouveau bénéficiaire que si le gestionnaire de ce domaine a donné son accord écrit.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., sur le caractère écrit d'une convention d'occupation du domaine public, CE, Section, 19 juin 2015, Société immobilière du port de Boulogne (SIPB), n°369558, à publier au Recueil.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
