<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032712991</ID>
<ANCIEN_ID>JG_L_2016_06_000000381255</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/71/29/CETATEXT000032712991.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème - 10ème chambres réunies, 15/06/2016, 381255</TITRE>
<DATE_DEC>2016-06-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>381255</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème - 10ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Bastien Lignereux</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:381255.20160615</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 13 juin 2014, 12 septembre 2014 et 29 juillet 2015 au secrétariat du contentieux du Conseil d'Etat, l'Association nationale des opérateurs détaillants en énergie (ANODE) demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite de rejet résultant du silence gardé par le ministre de l'économie et des finances et la ministre de l'écologie, du développement durable et de l'énergie sur sa demande tendant à l'abrogation de l'arrêté du 26 juillet 2013 relatif aux tarifs réglementés de vente de l'électricité ;<br/>
<br/>
              2°) d'enjoindre aux auteurs de cet arrêté de le modifier afin d'assurer sa conformité aux dispositions qu'ils étaient tenus de respecter ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'énergie ;<br/>
              - le décret n° 2009-975 du 12 août 2009 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bastien Lignereux, auditeur,<br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes du dernier alinéa de l'article L. 337-4 du code de l'énergie : " Pendant une période transitoire s'achevant le 7 décembre 2015, les tarifs réglementés de vente d'électricité sont arrêtés par les ministres chargés de l'énergie et de l'économie, après avis de la Commission de régulation de l'énergie " ; que, sur le fondement de ces dispositions, les ministres chargés de l'énergie et de l'économie ont, par un arrêté du 26 juillet 2013, fixé les tarifs réglementés de vente de l'électricité applicables à compter du 1er août 2013 ; que, le 14 février 2014, l'Association nationale des opérateurs détaillants en énergie (ANODE) leur a demandé de modifier cet arrêté afin d'assurer sa conformité aux règles de détermination des tarifs résultant du code de l'énergie et du décret du 12 août 2009 relatif aux tarifs réglementés de vente de l'électricité ; que l'ANODE demande l'annulation pour excès de pouvoir du refus implicite opposé par les ministres à cette demande ;<br/>
<br/>
              2. Considérant que l'autorité compétente, saisie d'une demande tendant à l'abrogation ou à la modification d'un règlement illégal, est tenue d'y déférer, soit que ce règlement ait été illégal dès la date de sa signature, soit que l'illégalité résulte de circonstances de droit ou de fait postérieures à cette date ; que lorsque, postérieurement à l'introduction d'une requête dirigée contre un refus d'abroger ou de modifier des dispositions à caractère réglementaire, l'autorité qui a pris le règlement litigieux procède à son abrogation expresse ou implicite, le litige né de ce refus d'abroger perd son objet ; qu'il en va toutefois différemment lorsque cette même autorité reprend, dans un nouveau règlement, les dispositions qu'elle abroge, sans les modifier ou en ne leur apportant que des modifications de pure forme ;<br/>
<br/>
              3. Considérant que, postérieurement à l'introduction de la requête, les ministres chargés de l'économie et de l'énergie ont adopté l'arrêté du 30 octobre 2014, qui fixe les tarifs réglementés de vente de l'électricité applicables à compter du 1er novembre 2014 ; que cet arrêté du 30 octobre 2014 a implicitement abrogé les barèmes des tarifs réglementés de vente de l'électricité fixés par l'arrêté du 26 juillet 2013, entré en vigueur le 1er août 2013 ; que, dès lors que les barèmes de tarifs annexés à l'arrêté du 30 octobre 2014 diffèrent de ceux qui étaient prévus par l'arrêté du 26 juillet 2013, les dispositions de l'arrêté du 26 juillet 2013 ne peuvent être regardées comme ayant été reprises sans modification autre que de pure forme dans l'arrêté du 30 octobre 2014 ; qu'au demeurant, l'arrêté du 30 octobre 2014 s'insère dans un cadre juridique modifié par le décret du 28 octobre 2014 modifiant le décret du 12 août 2009 relatif aux tarifs réglementés de vente de l'électricité ;<br/>
<br/>
              4. Considérant que, contrairement à ce que soutient la requérante, la circonstance que l'arrêté abrogatif du 30 octobre 2014 n'aurait pas remédié à l'illégalité  dont elle soutient que l'arrêté du 26 juillet 2013 était entaché, notamment en ce qui concerne les obligations de rattrapage tarifaire, est sans incidence sur le fait que l'abrogation de cet arrêté a eu pour effet de priver de son objet le litige ;<br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède qu'il n'y a pas lieu de statuer sur les conclusions de l'ANODE tendant à l'annulation du refus de modifier l'arrêté du 26 juillet 2013 ; <br/>
<br/>
              6. Considérant qu'ainsi qu'il a été dit au point 3, l'arrêté du 26 juillet 2013 a été implicitement abrogé par l'arrêté du 30 octobre 2014 ; que, par suite, les conclusions aux fins d'injonction tendant à ce que l'arrêté du 26 juillet 2013 soit modifié sont également devenues sans objet ;<br/>
<br/>
              7. Considérant qu'il n'y a pas lieu de mettre à la charge de l'Etat la somme que demande l'ANODE au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
Article 1er : Il n'y a pas lieu de statuer  sur les conclusions aux fins d'annulation et d'injonction de la requête de l'ANODE.<br/>
Article 2 : Les conclusions présentées par l'ANODE au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à l'ANODE et au ministre de l'économie, de l'industrie et du numérique.<br/>
Copie en sera adressée pour information à la ministre de l'environnement, de l'énergie et de la mer et à la Commission de régulation de l'énergie.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-05-05-02 PROCÉDURE. INCIDENTS. NON-LIEU. EXISTENCE. - LITIGE RELATIF À UN REFUS D'ABROGATION - ABROGATION EN COURS D'INSTANCE [RJ1] - CIRCONSTANCE QUE L'ACTE ABROGATIF N'AURAIT PAS REMÉDIÉ À L'ILLÉGALITÉ DE L'ACTE ABROGÉ SANS INCIDENCE.
</SCT>
<ANA ID="9A"> 54-05-05-02 La circonstance qu'un acte abrogatif n'aurait pas remédié à l'illégalité dont il est soutenu que l'acte abrogé était entaché est sans incidence sur le fait que l'abrogation a eu pour effet de priver de son objet un litige relatif au refus d'abrogation.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 27 juillet 2001, Coopérative de consommation des adhérents de la mutuelle des instituteurs de France (CAMIF), n° 218067, p. 401 ; CE, 30 décembre 2002, Confédération nationale des syndicats dentaires, n° 238032, T. pp. 609-881 ; CE, 30 mai 2005, Association française des opérateurs de réseaux et services de télécommunications, n° 250516, T. p. 1047 ; CE, Section, 5 octobre 2007, Ordre des avocats au barreau d'Evreux, n° 282321, p. 411.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
