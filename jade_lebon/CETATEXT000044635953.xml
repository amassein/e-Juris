<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044635953</ID>
<ANCIEN_ID>JG_L_2021_12_000000444711</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/63/59/CETATEXT000044635953.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 24/12/2021, 444711</TITRE>
<DATE_DEC>2021-12-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>444711</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SARL DELVOLVE ET TRICHET ; CABINET ROUSSEAU ET TAPIE</AVOCATS>
<RAPPORTEUR>Mme Dominique Agniau-Canel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Domingo</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:444711.20211224</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B... E... a demandé au tribunal administratif de Bordeaux d'annuler la décision de l'Association de Tutelle et d'Intégration d'Aquitaine, devenue Association Territoires et Intégration Nouvelle-Aquitaine (ATINA) refusant de lui communiquer ses rapports d'activité pour les trois dernières années, la liste nominative des personnes affectées à ce service public et celle des responsables déclarés ou devant l'être, ses statuts et son règlement intérieur.<br/>
<br/>
              Par un jugement n° 1800413 du 10 avril 2020, le tribunal administratif de Bordeaux a rejeté sa demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 21 septembre et 15 décembre 2020 au secrétariat du contentieux du Conseil d'Etat, M. E... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses conclusions de première instance ; <br/>
<br/>
              3°) de mettre à la charge de l'ATINA la somme de 3 000 euros à verser à la SCP Delvolvé-Trichet, son avocat, au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
             Vu les autres pièces du dossier ;<br/>
<br/>
             Vu : <br/>
             - le code de l'action sociale et des familles ;<br/>
             - le code des relations entre le public et l'administration ;<br/>
             - la loi du 1er juillet 1901 relative au contrat d'association ;<br/>
             - la loi n° 75-535 du 30 juin 1975 ;<br/>
             - la loi n°2002-2 du 2 janvier 2002 ;<br/>
             - la loi n°91-647 du 10 juillet 1991 ;<br/>
             - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Dominique Agnau-Canel, maître des requêtes en service extraordinaire,<br/>
<br/>
              - les conclusions de M. Laurent Domingo, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SARL Delvolvé et Trichet, avocat de M. B... E... et au cabinet Rousseau et Tapie, avocat de l'association Territoires et d'Intégration Nouvelle-Aquitaine ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. E... a demandé, le 31 août 2017, à l'Association pour la Tutelle et l'Insertion d'Aquitaine, devenue Association Territoires et Intégration Nouvelle-Aquitaine (ATINA), de lui communiquer ses rapports d'activité pour les trois dernières années faisant ressortir notamment le nombre de mesures de protection des majeurs assumées, le nombre d'employés délégués à ces mesures, les ratios entre les délégués affectés aux mesures par rapport au nombre de personnes protégées et les résultats financiers, la liste nominative des personnes affectées à ce service public et celle des responsables déclarés ou devant l'être, ses statuts et son règlement intérieur. M. E... se pourvoit en cassation contre le jugement du 10 avril 2020 par lequel le tribunal administratif de Bordeaux s'est déclaré incompétent pour connaître du litige tendant à l'annulation de la décision de l'ATINA refusant la communication des documents demandés.<br/>
<br/>
              2. En premier lieu, l'article R. 711-3 du code de justice administrative dispose que " Si le jugement de l'affaire doit intervenir après le prononcé de conclusions du rapporteur public, les parties ou leurs mandataires sont mis en mesure de connaître, avant la tenue de l'audience, le sens de ces conclusions sur l'affaire qui les concerne ". La communication aux parties du sens des conclusions, prévue par ces dispositions, a pour objet de les mettre en mesure d'apprécier l'opportunité d'assister à l'audience publique, de préparer, le cas échéant, les observations orales qu'elles peuvent y présenter, après les conclusions du rapporteur public, à l'appui de leur argumentation écrite et d'envisager, si elles l'estiment utile, la production, après la séance publique, d'une note en délibéré. En conséquence, les parties ou leurs mandataires doivent être mis en mesure de connaître, dans un délai raisonnable avant l'audience, l'ensemble des éléments du dispositif de la décision que le rapporteur public compte proposer à la formation de jugement d'adopter, à l'exception de la réponse aux conclusions qui revêtent un caractère accessoire, notamment celles qui sont relatives à l'application de l'article L. 761-1 du code de justice administrative. Cette exigence s'impose à peine d'irrégularité de la décision rendue sur les conclusions du rapporteur public.<br/>
<br/>
              3. Il ressort des pièces de la procédure devant le tribunal administratif que le sens des conclusions du rapporteur public sur l'affaire en litige a été mis en ligne le 6 mars 2020 à 15 H 15 en vue d'une audience du 10 mars à 11 H. Il comportait la mention " rejet au fond ". Il s'ensuit que le requérant a été mis en mesure de connaître, dans un délai raisonnable avant l'audience, le sens des conclusions. Le jugement attaqué n'a dès lors pas été rendu à la suite d'une procédure irrégulière. <br/>
<br/>
              4. En second lieu, au sein du livre III du code des relations entre le public et l'administration, l'article L. 311-1 dispose que : " Sous réserve des dispositions des articles L. 311-5 et L. 311-6, les administrations mentionnées à l'article L. 300-2 sont tenues de publier en ligne ou de communiquer les documents administratifs qu'elles détiennent aux personnes qui en font la demande, dans les conditions prévues par le présent livre. " Aux termes de l'article L. 300-2 du même code : " Sont considérés comme documents administratifs, au sens des titres Ier, III et IV du présent livre, quels que soient leur date, leur lieu de conservation, leur forme et leur support, les documents produits ou reçus, dans le cadre de leur mission de service public, par l'Etat, les collectivités territoriales ainsi que par les autres personnes de droit public ou les personnes de droit privé chargées d'une telle mission. (...) ". <br/>
<br/>
              5. Indépendamment des cas dans lesquels le législateur a lui-même entendu reconnaître ou, à l'inverse, exclure l'existence d'un service public, une personne privée qui assure une mission d'intérêt général sous le contrôle de l'administration et qui est dotée à cette fin de prérogatives de puissance publique est chargée de l'exécution d'un service public. Même en l'absence de telles prérogatives, une personne privée doit également être regardée, dans le silence de la loi, comme assurant une mission de service public lorsque, eu égard à l'intérêt général de son activité, aux conditions de sa création, de son organisation ou de son fonctionnement, aux obligations qui lui sont imposées ainsi qu'aux mesures prises pour vérifier que les objectifs qui lui sont assignés sont atteints, il apparaît que l'administration a entendu lui confier une telle mission.<br/>
<br/>
              6. Il ressort des pièces du dossier soumis au juge de fond que l'ATINA est une association régie par la loi 1901, qui assure des actions sociales et médico-sociales mentionnées à l'article L. 312-1 du code de l'action sociale et des familles aux termes duquel : " I. Sont des établissements et services sociaux et médico-sociaux, au sens du présent code, les établissements et les services, dotés ou non d'une personnalité morale propre, énumérés ci-après/ : (...) 14 ° Les services mettant en œuvre les mesures de protection des majeurs ordonnées par l'autorité judiciaire au titre du mandat spécial auquel il peut être recouru dans le cadre de la sauvegarde de justice ou au titre de la curatelle, de la tutelle ou de la mesure d'accompagnement judiciaire (...)". Si la protection des majeurs ordonnée par l'autorité judiciaire au titre du mandat spécial auquel il peut être recouru au titre de la curatelle constitue une mission d'intérêt général, il résulte toutefois des dispositions de la loi du 30 juin 1975 relative aux institutions sociales et médico-sociales et de la loi du 2 janvier 2002 rénovant l'action sociale et médico-sociale, éclairées par leurs travaux préparatoires, que le législateur a entendu exclure que la mission assurée par les organismes privés gestionnaires des établissements et services aujourd'hui mentionnés au 14° du I de l'article L. 312-1 du code de l'action sociale et des familles revête le caractère d'une mission de service public. Il s'ensuit qu'en jugeant que le litige né du refus du président de l'ATINA de communiquer au requérant les documents demandés ne relevait pas de la compétence de la juridiction administrative dès lors que cette association n'exerçait pas de mission de service public, le tribunal administratif, dont le jugement est suffisamment motivé, n'a pas commis d'erreur de droit. <br/>
<br/>
              7. Il résulte de tout ce qui précède que M. E... n'est pas fondé à demander l'annulation du jugement qu'il attaque. Ses conclusions doivent par suite être rejetées, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de M. E... la somme demandée au même titre par l'ATINA.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
     --------------<br/>
<br/>
Article 1er : Le pourvoi de M. E... est rejeté. <br/>
 Article 2 : Les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative par l'association Territoires et d'Intégration Nouvelle-Aquitaine sont rejetées ;<br/>
 Article 3 : La présente décision sera notifiée à M. B... E... et à l'association Territoires et d'Intégration Nouvelle-Aquitaine ;<br/>
<br/>
              Délibéré à l'issue de la séance du 3 décembre 2021 où siégeaient : M. Rémy Schwartz, président adjoint de la section du contentieux présidant ; M. I... H..., M. Frédéric Aladjidi, présidents de chambre ; Mme K... C..., M. L... D..., Mme A... M..., M. F... G..., M. Alain Seban, conseillers d'Etat et Mme N..., maîre des requêtes en service extraordinaire-rapporteure.<br/>
<br/>
              Rendu le 24 décembre 2021.<br/>
<br/>
                                               Le Président : <br/>
                                               Signé : M. Rémy Schwartz <br/>
<br/>
<br/>
     La rapporteure : <br/>
     Signé : Mme Dominique Agnau-Canel <br/>
<br/>
<br/>
                                               La secrétaire :<br/>
                                               Signé : Mme J... O...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">17-03-02-005-02 COMPÉTENCE. - RÉPARTITION DES COMPÉTENCES ENTRE LES DEUX ORDRES DE JURIDICTION. - COMPÉTENCE DÉTERMINÉE PAR UN CRITÈRE JURISPRUDENTIEL. - ACTES. - ACTES DE DROIT PRIVÉ. - REFUS DE COMMUNICATION D'UN DOCUMENT OPPOSÉ PAR UNE PERSONNE PRIVÉE NON CHARGÉE D'UNE MISSION DE SERVICE PUBLIC - COMPÉTENCE JUDICIAIRE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">26-06-01-04 DROITS CIVILS ET INDIVIDUELS. - ACCÈS AUX DOCUMENTS ADMINISTRATIFS. - ACCÈS AUX DOCUMENTS ADMINISTRATIFS AU TITRE DE LA LOI DU 17 JUILLET 1978. - CONTENTIEUX. - REFUS DE COMMUNICATION D'UN DOCUMENT OPPOSÉ PAR UNE PERSONNE PRIVÉE NON CHARGÉE D'UNE MISSION DE SERVICE PUBLIC - COMPÉTENCE JUDICIAIRE [RJ1].
</SCT>
<ANA ID="9A"> 17-03-02-005-02 Le litige né du refus d'une personne privée de communiquer les documents demandés ne relève pas de la compétence de la juridiction administrative dès lors que cette personne n'exerce pas de mission de service public.</ANA>
<ANA ID="9B"> 26-06-01-04 Le litige né du refus d'une personne privée de communiquer les documents demandés ne relève pas de la compétence de la juridiction administrative dès lors que cette personne n'exerce pas de mission de service public.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 20 octobre 1995, Mugnier, n° 133470, p. 358 ; CE, Section, 22 février 2007, Association du personnel relevant des établissements pour inadaptés (APREI), n° 264541, p. 92, aux Tables sur un autre point. Cf. sol. contr., s'agissant d'une demande adressée à une personne privée chargée d'une mission de service public, quelle que soit la nature du document, TC, 2 juillet 1984, Vinçot et Le Borgne, n° 02324, p. 450 ; CE, 28 novembre 2016, M. Ablyazov, n° 390776, T. pp. 682-692-766-768 ; CE, 27 juin 2019, Association Regards citoyens, n° 427725, p.  247.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
