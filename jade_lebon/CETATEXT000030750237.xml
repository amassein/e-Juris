<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030750237</ID>
<ANCIEN_ID>JG_L_2015_06_000000379380</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/75/02/CETATEXT000030750237.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème / 2ème SSR, 17/06/2015, 379380</TITRE>
<DATE_DEC>2015-06-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>379380</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème / 2ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP POTIER DE LA VARDE, BUK LAMENT ; SCP BOUZIDI, BOUHANNA</AVOCATS>
<RAPPORTEUR>M. François Lelièvre</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:379380.20150617</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La Compagnie pour le développement du tourisme hyérois (CDTH) a demandé au tribunal administratif de Toulon l'annulation du titre exécutoire n° 604, notifié le 26 février 2009, d'un montant de 134 796 euros et la condamnation de la commune de Hyères à lui rembourser la somme de 96 200 euros versée à la commune en application du titre de recettes n° 3479 du 21 octobre 2003 émis sur le fondement de l'article 4-b d'une convention du 1er avril 1999.<br/>
<br/>
              Par un jugement n° 0900570 du 8 avril 2011, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 11MA01519 du 25 février 2014, la cour administrative d'appel de Marseille a annulé ce titre exécutoire et rejeté le surplus des conclusions de la CDTH.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 5 mai et 30 juillet 2014 au secrétariat du contentieux du Conseil d'Etat, la commune de Hyères demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt en tant qu'il a annulé le titre exécutoire n° 604 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter les conclusions d'appel présentées sur ce point par la CDTH ; <br/>
<br/>
              3°) de mettre à la charge de la CDTH le versement de la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des collectivités territoriales ;<br/>
              - la loi du 15 juin 1907 réglementant le jeu dans les cercles et les casinos des stations balnéaires, thermales et climatiques ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. François Lelièvre, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Potier de la Varde, Buk Lament, avocat de la commune de Hyères, et à la SCP Bouzidi, Bouhanna, avocat de la Compagnie pour le développement du tourisme hyérois ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la commune de Hyères a délégué, le 1er avril 1999, à la Compagnie pour le développement du tourisme hyérois (CDTH) l'exploitation de son casino pour une durée de dix-huit ans ; qu'en application de l'article 4-b du cahier des charges de la convention de délégation, la commune et la CDTH ont conclu le 9 octobre 2003 une convention ayant pour objet d'associer cette société à l'organisation de spectacles par la commune d'octobre 2003 à juin 2004 ; que la commune a émis un titre de perception, sur le fondement de ces stipulations, afin de recouvrer la participation de la CDTH au financement de ces spectacles ; que, par l'arrêt attaqué, la cour administrative d'appel de Marseille a annulé ce titre de perception en se fondant sur le caractère illicite des stipulations lui servant de fondement, au regard des dispositions de l'article L. 2333-54 du code général des collectivités territoriales limitant à 15 % l'ensemble des prélèvements pouvant être effectués par une commune sur le produit brut des jeux de casino ;<br/>
<br/>
              2. Considérant, d'une part, qu'il résulte des dispositions de la loi du 15 juin 1907 relative aux casinos, aujourd'hui codifiée aux articles L. 321-1 et suivants du code de la sécurité intérieure, ainsi que des travaux parlementaires qui ont précédé son adoption et de ses modifications successives, que le législateur, tout en soumettant à une surveillance particulière les jeux de casino, a entendu que ces activités concourent aux objectifs de développement touristique, économique et culturel des communes autorisées à les accueillir ; qu'ainsi, en vertu de l'article 2 de la loi du 15 juin 1907, les jeux de casino sont autorisés par arrêté du ministre de l'intérieur, sur avis conforme du conseil municipal de la commune concernée ; que ces dispositions imposent à la commune, d'une part, de conclure à cette fin avec le titulaire de l'autorisation une convention et, d'autre part, d'assortir celle-ci d'un cahier des charges fixant des obligations au cocontractant, relatives notamment à la prise en charge du financement d'infrastructures et de missions d'intérêt général en matière de développement économique, culturel et touristique ; que si ces jeux de casinos ne constituent pas, par eux-mêmes, une activité de service public, les conventions obligatoirement conclues pour leur installation et leur exploitation, dès lors que le cahier des charges impose au cocontractant une participation à ces missions et que sa rémunération est substantiellement assurée par les résultats de l'exploitation, ont le caractère de délégation de service public ;<br/>
<br/>
              3. Considérant, d'autre part, que selon des dispositions distinctes, insérées à l'article L. 2333-54 du code général des collectivités territoriales, les communes dotées d'un casino peuvent instituer sur le produit brut des jeux un prélèvement dont le taux ne peut dépasser 15 % de ce produit ; que ces dispositions ne font pas obstacle à ce que la convention de délégation de service public prévoie, compte tenu des exigences résultant de la loi du 15 juin 1907, la participation du délégataire au financement de manifestations artistiques communales, lesquelles concourent au développement culturel local et sont susceptibles de favoriser les différentes activités du délégataire ; qu'il en va ainsi, alors même que cette participation, qui ne constitue pas un prélèvement sur le produit brut des jeux, prendrait en compte ce produit dans son mode de calcul et serait ainsi susceptible de porter à plus de 15 % de cette assiette le montant total des sommes dont le délégataire serait redevable à l'égard de la commune ;<br/>
<br/>
              4. Considérant qu'il ressort des énonciations de l'arrêt attaqué que, par application des stipulations de l'article 4-b du cahier des charges de la convention de délégation de service public, des participations financières annuelles ont été instituées afin de permettre la création de " grands événements à Hyères " " dans le cadre exclusif d'événements artistiques de qualité co-organisés par la ville et le casino " ; qu'en se fondant sur la circonstance que le montant exigé par ces stipulations serait fixé par référence à un pourcentage du produit brut des jeux, pour en déduire qu'il serait inclus dans le plafond fixé à l'article L. 2333-54 du code général des collectivités territoriales, la cour administrative d'appel de Marseille a commis une erreur de droit ; que, par suite, la commune de Hyères est fondée, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, à demander l'annulation de l'arrêt attaqué ;<br/>
<br/>
              5. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mis à la charge de la commune de Hyères, qui n'est pas la partie perdante le versement des sommes que demande, à ce titre, la CDTH ; qu'en revanche, il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la CDTH le versement d'une somme de 3 000 euros à la commune de Hyères au titre des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 25 février 2014 de la cour administrative d'appel de Marseille est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Marseille.<br/>
Article 3 : La Compagnie pour le développement du tourisme hyérois versera la somme de 3 000 euros à la commune de Hyères en application des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions présentées par la Compagnie pour le développement du tourisme hyérois sur le fondement des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à la commune de Hyères et à la Compagnie pour le développement du tourisme hyérois.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">135-02-03-03 COLLECTIVITÉS TERRITORIALES. COMMUNE. ATTRIBUTIONS. SERVICES COMMUNAUX. - SPECTACLES - DÉLÉGATION DE SERVICE PUBLIC AVEC L'EXPLOITANT D'UN CASINO - PARTICIPATION DU DÉLÉGATAIRE AU FINANCEMENT DE SPECTACLES COMMUNAUX - LÉGALITÉ, Y COMPRIS SI LA PARTICIPATION EST ASSISE SUR LE PRODUIT BRUT DES JEUX QUI FAIT L'OBJET DU PRÉLÈVEMENT PRÉVU À L'ARTICLE L. 2333-54 DU CGCT [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">63-02 SPORTS ET JEUX. CASINOS. - DÉLÉGATION DE SERVICE PUBLIC - PARTICIPATION DU DÉLÉGATAIRE AU FINANCEMENT DE SPECTACLES COMMUNAUX - LÉGALITÉ, Y COMPRIS SI LA PARTICIPATION EST ASSISE SUR LE PRODUIT BRUT DES JEUX QUI FAIT L'OBJET DU PRÉLÈVEMENT PRÉVU À L'ARTICLE L. 2333-54 DU CGCT [RJ1].
</SCT>
<ANA ID="9A"> 135-02-03-03 Convention de délégation de service public obligatoirement conclue entre la commune et le titulaire de l'autorisation d'exploiter un casino (art. L. 321-1et ss du code de la sécurité intérieure). En vertu de l'article L. 2333-54 du code général des collectivités territoriales (CGCT), les communes dotées d'un casino peuvent instituer sur le produit brut des jeux un prélèvement dont le taux ne peut dépasser 15 % de ce produit. Ces dispositions ne font pas obstacle à ce que la convention de délégation de service public prévoie, compte tenu des exigences résultant de la loi du 15 juin 1907 relative aux casinos, la participation du délégataire au financement de manifestations artistiques communales, lesquelles concourent au développement culturel local et sont susceptibles de favoriser les différentes activités du délégataire. Cette participation, qui ne constitue pas un prélèvement sur le produit brut des jeux, peut prendre en compte ce produit dans son mode de calcul, alors même que cela est ainsi susceptible de porter à plus de 15 % de cette assiette le montant total des sommes dont le délégataire serait redevable à l'égard de la commune.</ANA>
<ANA ID="9B"> 63-02 Convention de délégation de service public obligatoirement conclue entre la commune et le titulaire de l'autorisation d'exploiter un casino (art. L. 321-1et ss du code de la sécurité intérieure). En vertu de l'article L. 2333-54 du code général des collectivités territoriales (CGCT), les communes dotées d'un casino peuvent instituer sur le produit brut des jeux un prélèvement dont le taux ne peut dépasser 15 % de ce produit. Ces dispositions ne font pas obstacle à ce que la convention de délégation de service public prévoie, compte tenu des exigences résultant de la loi du 15 juin 1907 relative aux casinos, la participation du délégataire au financement de manifestations artistiques communales, lesquelles concourent au développement culturel local et sont susceptibles de favoriser les différentes activités du délégataire. Cette participation, qui ne constitue pas un prélèvement sur le produit brut des jeux, peut prendre en compte ce produit dans son mode de calcul, alors même que cela est ainsi susceptible de porter à plus de 15 % de cette assiette le montant total des sommes dont le délégataire serait redevable à l'égard de la commune.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Rappr., pour la légalité de l'institution d'une redevance pour occupation du domaine public en plus du prélèvement prévu par l'article L. 2333-54 du CGCT, CE, 19 mars 2012, SA Groupe Partouche, n° 341562, p. 91.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
