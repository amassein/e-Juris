<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042570056</ID>
<ANCIEN_ID>JG_L_2020_11_000000430754</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/57/00/CETATEXT000042570056.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 4ème chambres réunies, 25/11/2020, 430754</TITRE>
<DATE_DEC>2020-11-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>430754</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER ; SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>M. Damien Pons</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie Sirinelli</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:430754.20201125</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. et Mme D... et Marie-Caroline A... ont demandé au tribunal administratif de Marseille:<br/>
              - à titre principal, d'annuler pour excès de pouvoir, d'une part, l'arrêté du 13 avril 2016 par lequel le maire de Marseille a délivré à M. C... B... un permis de construire portant sur la création d'un logement par changement partiel de destination d'un immeuble ainsi que la décision du 13 juillet 2016 par laquelle le maire a rejeté leur recours gracieux et, d'autre part, la décision implicite par laquelle il a délivré à M. B... un permis de construire modificatif ;<br/>
              - à titre subsidiaire, d'enjoindre au maire de Marseille de procéder au retrait de l'arrêté du 13 avril 2016 et du permis modificatif tacite, sous astreinte de 100 euros par jour de retard. <br/>
<br/>
              Par un jugement n° 1607424 du 14 mars 2019, le tribunal administratif de Marseille a rejeté leur demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 15 mai et 16 août 2019 au secrétariat du contentieux du Conseil d'Etat, M. et Mme A... demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur demande ;<br/>
<br/>
              3°) de mettre à la charge, solidairement, de la commune de Marseille et de M. B... la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la construction et de l'habitation ;<br/>
              - le code de l'urbanisme ; <br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Damien Pons, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Marie Sirinelli, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Foussard, Froger, avocat de M. et Mme A... et à la SCP Waquet, Farge, Hazan, avocat de M. B... ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 6 novembre 2020, présentée par M. B... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 13 avril 2016, le maire de Marseille a délivré à M. C... B... un permis de construire en vue de la réhabilitation de locaux situés 151-153 avenue Joseph Vidal, qui accueillaient un restaurant, la création en leur sein d'un logement par changement partiel de destination, la création d'une toiture terrasse accessible et l'aménagement des cours extérieures. Le recours gracieux de M. et Mme A..., voisins de l'immeuble, contre ce permis a été rejeté le 13 juillet 2016 par le maire de Marseille et celui-ci a certifié, le 30 janvier 2017, qu'un permis de construire modificatif tacite était né de la demande déposée par M. B... le 8 août 2016. M. et Mme A... se pourvoient en cassation contre le jugement du 14 mars 2019 par lequel le tribunal administratif de Marseille a rejeté leur demande d'annulation pour excès de pouvoir de ces autorisations d'urbanisme et de la décision de rejet de leur recours gracieux.<br/>
<br/>
              2. En premier lieu, aux termes de l'article L. 425-3 du code de l'urbanisme : " Lorsque le projet porte sur un établissement recevant du public, le permis de construire tient lieu de l'autorisation prévue par l'article L. 111-8 du code de la construction et de l'habitation dès lors que la décision a fait l'objet d'un accord de l'autorité administrative compétente qui peut imposer des prescriptions relatives à l'exploitation des bâtiments en application de l'article L. 123-2 du code de la construction et de l'habitation. Le permis de construire mentionne ces prescriptions. Toutefois, lorsque l'aménagement intérieur d'un établissement recevant du public ou d'une partie de celui-ci n'est pas connu lors du dépôt d'une demande de permis de construire, le permis de construire indique qu'une autorisation complémentaire au titre de l'article L. 111-8 du code de la construction et de l'habitation devra être demandée et obtenue en ce qui concerne l'aménagement intérieur du bâtiment ou de la partie de bâtiment concernée avant son ouverture au public ".<br/>
<br/>
              3. Il résulte de ces dispositions que lorsque l'aménagement intérieur de locaux constitutifs d'un établissement recevant du public, qui nécessite une autorisation spécifique au titre de l'article L. 111-8 du code de la construction et de l'habitation, n'est pas connu lors du dépôt de la demande de permis de construire, l'autorité compétente, dont la décision ne saurait tenir lieu sur ce point de l'autorisation prévue par le code de la construction et de l'habitation, ne peut légalement délivrer le permis sans mentionner expressément l'obligation de demander et d'obtenir une autorisation complémentaire avant l'ouverture au public et ce, alors même que le contenu du dossier de demande de permis de construire témoignerait de la connaissance, par le pétitionnaire, de cette obligation.<br/>
<br/>
              4. Le tribunal administratif de Marseille a relevé que si l'aménagement intérieur du restaurant à réhabiliter, qui n'était pas connu lors du dépôt de la demande de permis de construire, devrait ultérieurement être autorisé au titre de l'article L. 111-8 du code de la construction et de l'habitation, l'arrêté du 13 avril 2016 accordant ce permis mentionnait que son bénéficiaire devrait respecter diverses prescriptions, dont celles qui avaient été formulées par la commission communale d'accessibilité dans son avis du 9 février 2016, lequel faisait état de l'obligation, pour le demandeur, de solliciter l'autorisation prévue par le code de la construction et de l'habitation. En déduisant de ce simple renvoi à l'avis de la commission communale d'accessibilité que le permis de construire attaqué respectait les dispositions de l'article L. 425-3 du code de l'urbanisme, alors qu'il ne mentionnait pas qu'une autorisation complémentaire au titre de l'article L. 111-8 du code de la construction et de l'habitation devrait être demandée et obtenue pour l'aménagement intérieur de la partie de bâtiment destinée à accueillir un restaurant avant son ouverture au public, le tribunal a commis une erreur de droit.<br/>
<br/>
              5. En second lieu, d'une part, aux termes de l'article R. 151-27 du code de l'urbanisme : " Les destinations de constructions sont : (...) / 2° Habitation ; / 3° Commerce et activités de service (...) ". D'autre part, aux termes de l'article UT 12.1 du plan local d'urbanisme de la ville de Marseille, applicable à la zone dans laquelle se situent les locaux litigieux : " Le stationnement des véhicules correspondant aux destinations des constructions est assuré hors des voies publiques ". Aux termes de son article UT 12.2.1 : " (...) Pour les travaux sur les constructions existantes à destination d'habitat, s'il est créé plus de 40 mètres carrés de surface de plancher ou plus de 1 logement supplémentaire, il est exigé 1 place de stationnement par tranche entamée de 50 mètres carrés de surface de plancher créée ou 1 place de stationnement par logement supplémentaire créé. / Hormis le cas des hébergements hôteliers, il en est de même, en matière de changement de destination, lorsqu'un immeuble change de destination et prend la destination d'habitat ". Enfin, son article UT 12.2.2 n'exige aucune place de stationnement de voiture pour les constructions à destination de commerce ayant une surface de plancher inférieure ou égale à 250 mètres carrés.<br/>
<br/>
              6. Pour l'application des dispositions d'un règlement d'un plan local d'urbanisme déterminant les obligations en matière d'aires de stationnement, il convient, en cas de travaux donnant plusieurs destinations à une même construction, et sous réserve de dispositions particulières prévues dans ce cas par le règlement, de calculer distinctement puis de cumuler le nombre de places de stationnement exigées pour chacune des nouvelles destinations qu'aura la construction à l'issue des travaux autorisés. En cas de travaux sur une construction existante, il convient d'en retrancher ensuite le nombre de places existantes pour en déduire le nombre de nouvelles places à créer. Il suit de là que, pour l'application des dispositions du règlement du plan local d'urbanisme de Marseille citées au point 5, le tribunal administratif de Marseille devait apprécier distinctement le nombre de places de stationnement exigées du fait de la création d'une surface de plancher à destination d'habitat, sans qu'ait d'incidence la circonstance que la surface de plancher totale des locaux litigieux soit réduite. <br/>
<br/>
              7. Par suite, en jugeant que, dès lors que le projet prévoyait une réduction de 22,7 mètres carrés de la surface de plancher totale du bâtiment et la création d'un seul logement, il n'impliquait la création d'aucune place de stationnement, sans prendre en considération la surface de plancher à destination d'habitat qu'il créait, le tribunal a commis une erreur de droit.<br/>
<br/>
              8. Il résulte de tout ce qui précède que, sans qu'il soit besoin d'examiner les autres moyens de leur pourvoi, M. et Mme A... sont fondés à demander l'annulation du jugement qu'ils attaquent.<br/>
<br/>
              9. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de Marseille une somme de 3 000 euros à verser à M. et Mme A... au titre de l'article L. 761-1 du code de justice administrative. Les dispositions de cet article font obstacle à ce qu'une somme soit mise à ce titre à la charge de M. et Mme A..., qui ne sont pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Marseille du 14 mars 2019 est annulé. <br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Marseille.<br/>
Article 3 :  La commune de Marseille versera une somme de 3 000 euros à M. et Mme A... au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 :  Les conclusions de M. B... présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 :  La présente décision sera notifiée à M. et Mme D... et Marie-Caroline A..., à M. C... B... et à la commune de Marseille.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">49-05-003 POLICE. POLICES SPÉCIALES. - DEMANDE DE PERMIS DE CONSTRUIRE - CAS DANS LEQUEL L'AMÉNAGEUR INTÉRIEUR N'EST PAS CONNU LORS DE LA DEMANDE - OBLIGATION POUR L'AUTORITÉ COMPÉTENTE POUR DÉLIVRER LE PERMIS DE MENTIONNER, À PEINE D'ILLÉGALITÉ, L'OBLIGATION D'OBTENIR L'AUTORISATION PRÉVUE AU TITRE DE L'ARTICLE L. 111-8 DU CCH [RJ1] - MENTION DU SEUL AVIS DE LA COMMISSION COMMUNALE D'ACCESSIBILITÉ FAISANT ÉTAT DE CETTE OBLIGATION - MÉCONNAISSANCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">68-01-01-02-02-12 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PLANS D'AMÉNAGEMENT ET D'URBANISME. PLANS D`OCCUPATION DES SOLS (POS) ET PLANS LOCAUX D'URBANISME (PLU). APPLICATION DES RÈGLES FIXÉES PAR LES POS OU LES PLU. RÈGLES DE FOND. STATIONNEMENT DES VÉHICULES. - CAS DE TRAVAUX DONNANT PLUSIEURS DESTINATIONS À UNE MÊME CONSTRUCTION - MODALITÉS D'APPLICATION.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">68-03-02-02 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PERMIS DE CONSTRUIRE. PROCÉDURE D'ATTRIBUTION. INSTRUCTION DE LA DEMANDE. - PERMIS RELATIF À UN ERP - CAS DANS LEQUEL L'AMÉNAGEUR INTÉRIEUR N'EST PAS CONNU LORS DE LA DEMANDE DE PERMIS DE CONSTRUIRE - OBLIGATION POUR L'AUTORITÉ COMPÉTENTE POUR DÉLIVRER LE PERMIS DE MENTIONNER, À PEINE D'ILLÉGALITÉ, L'OBLIGATION D'OBTENIR L'AUTORISATION PRÉVUE AU TITRE DE L'ARTICLE L. 111-8 DU CCH [RJ1] - MENTION DE L'AVIS DE LA COMMISSION COMMUNALE D'ACCESSIBILITÉ FAISANT ÉTAT DE CETTE OBLIGATION - MÉCONNAISSANCE.
</SCT>
<ANA ID="9A"> 49-05-003 Il résulte de l'article L. 425-3 du code de l'urbanisme que lorsque l'aménagement intérieur de locaux constitutifs d'un établissement recevant du public (ERP), qui nécessite une autorisation spécifique au titre de l'article L. 111-8 du code de la construction et de l'habitation (CCH), n'est pas connu lors du dépôt de la demande de permis de construire, l'autorité compétente, dont la décision ne saurait tenir lieu sur ce point de l'autorisation prévue par le CCH, ne peut légalement délivrer le permis sans mentionner expressément l'obligation de demander et d'obtenir une autorisation complémentaire avant l'ouverture au public, et ce alors même que le contenu du dossier de demande de permis de construire témoignerait de la connaissance, par le pétitionnaire, de cette obligation.,,,Une telle obligation n'est pas satisfaite dans le cas où l'arrêté accordant le permis de construire se borne à mentionner que son bénéficiaire devra respecter les prescriptions formulées par l'avis de la commission communale d'accessibilité, lequel fait état de l'obligation, pour le demandeur, de solliciter l'autorisation prévue par le CCH.</ANA>
<ANA ID="9B"> 68-01-01-02-02-12 Pour l'application des dispositions d'un règlement d'un plan local d'urbanisme déterminant les obligations en matière d'aires de stationnement, il convient, en cas de travaux donnant plusieurs destinations à une même construction, et sous réserve de dispositions particulières prévues dans ce cas par le règlement, de calculer distinctement puis de cumuler le nombre de places de stationnement exigées pour chacune des nouvelles destinations qu'aura la construction à l'issue des travaux autorisés. En cas de travaux sur une construction existante, il convient d'en retrancher ensuite le nombre de places existantes pour en déduire le nombre de nouvelles places à créer.</ANA>
<ANA ID="9C"> 68-03-02-02 Il résulte de l'article L. 425-3 du code de l'urbanisme que lorsque l'aménagement intérieur de locaux constitutifs d'un établissement recevant du public (ERP), qui nécessite une autorisation spécifique au titre de l'article L. 111-8 du code de la construction et de l'habitation (CCH), n'est pas connu lors du dépôt de la demande de permis de construire, l'autorité compétente, dont la décision ne saurait tenir lieu sur ce point de l'autorisation prévue par le CCH, ne peut légalement délivrer le permis sans mentionner expressément l'obligation de demander et d'obtenir une autorisation complémentaire avant l'ouverture au public, et ce alors même que le contenu du dossier de demande de permis de construire témoignerait de la connaissance, par le pétitionnaire, de cette obligation.,,,Une telle obligation n'est pas satisfaite dans le cas où l'arrêté accordant le permis de construire se borne à mentionner que son bénéficiaire devra respecter les prescriptions formulées par l'avis de la commission communale d'accessibilité, lequel fait état de l'obligation, pour le demandeur, de solliciter l'autorisation prévue par le CCH.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 23 mai 2018, Ville de Paris et office public de l'habitat Paris Habitat, n°s 405937 405976, T. pp. 810-957.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
