<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028047770</ID>
<ANCIEN_ID>JG_L_2013_10_000000361934</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/04/77/CETATEXT000028047770.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 4ème SSR, 07/10/2013, 361934</TITRE>
<DATE_DEC>2013-10-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>361934</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 4ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, BLANCPAIN, SOLTNER ; SCP LE GRIEL</AVOCATS>
<RAPPORTEUR>Mme Anne-Françoise Roul</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2013:361934.20131007</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 16 août et 16 novembre 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour la commune de Saint-Jean-de-Monts, représentée par son maire ; la commune de Saint-Jean-de-Monts demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'arrêt n° 10NT01321 du 15 juin 2012 par lequel la cour administrative d'appel de Nantes a annulé le jugement n° 08-78 du 27 avril 2010 du tribunal administratif de Nantes ainsi que l'arrêté du 12 novembre 2007 par lequel son maire a modifié les cahiers des charges du lotissement de la Plage pour les mettre en concordance avec le plan d'occupation des sols ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de M.B... ; <br/>
<br/>
              3°) de mettre à la charge de M. B...le versement d'une somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
	Vu le code de l'urbanisme ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Anne-Françoise Roul, Conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Blancpain, Soltner, avocat de la commune de Saint-Jean-de-Monts et à la SCP Le Griel, avocat de M. B... ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article L. 442-11 du code de l'urbanisme, dans sa rédaction applicable au litige : " Lorsque l'approbation d'un plan local d'urbanisme ou d'un document d'urbanisme en tenant lieu intervient postérieurement au permis d'aménager un lotissement ou à la décision de non-opposition à une déclaration préalable, l'autorité compétente peut, après enquête publique et délibération du conseil municipal, modifier tout ou partie des documents du lotissement, et notamment le règlement et le cahier des charges, pour les mettre en concordance avec le plan local d'urbanisme ou le document d'urbanisme en tenant lieu " ; que ces dispositions ne prévoient aucune exception au pouvoir qu'elles confèrent au maire de modifier tous les documents d'un lotissement, y compris le cahier des charges, dès lors que la modification a pour objet de mettre ces documents en concordance avec le plan local d'urbanisme ou le document d'urbanisme en tenant lieu ; <br/>
<br/>
              2. Considérant qu'il résulte des dispositions de l'article L. 442-9 du même code que les règles d'urbanisme contenues dans les documents approuvés d'un lotissement deviennent caduques, en l'absence d'opposition d'une majorité qualifiée de colotis, au terme de dix années à compter de l'autorisation de lotir si, à cette date, le lotissement est couvert par un plan local d'urbanisme ou un document en tenant lieu, mais que les stipulations du cahier des charges du lotissement continuent néanmoins à régir les rapports entre colotis ; qu'en cas de discordance entre, d'une part, le cahier des charges qui continue à régir les rapports entre colotis et, d'autre part, le plan local d'urbanisme ou le document d'urbanisme en tenant lieu, le maire peut faire usage du pouvoir qu'il tient des dispositions de l'article L. 442-11 de modifier le cahier des charges pour le mettre en concordance avec le plan local d'urbanisme ou le document d'urbanisme en tenant lieu ;<br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 12 novembre 2007 pris en application de l'article L. 442-11 du code de l'urbanisme, le maire de la commune de Saint-Jean-de-Monts a modifié les cahiers des charges du lotissement de la Plage pour les mettre en concordance avec le plan d'occupation des sols ; qu'il résulte de ce qui précède qu'en annulant cet arrêté, par  l'arrêt attaqué du 15 juin 2012, au motif que la caducité des règles d'urbanisme contenues dans les documents approuvés du lotissement aurait eu pour effet de priver le maire de son pouvoir de modifier les stipulations contractuelles des cahiers des charges de ce lotissement, la cour administrative d'appel de Nantes a commis une erreur de droit ;<br/>
<br/>
              4. Considérant, par suite, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que la commune de Saint-Jean-de-Monts est fondée à demander l'annulation de l'arrêt de la cour administrative d'appel de Nantes du 15 juin 2012  ; que doivent être rejetées par voie de conséquence les conclusions présentées par M. B...au titre des dispositions de l'article L. 761-1 du code de justice administrative ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de M. B...la somme de 3 000 euros à verser à la commune de Saint-Jean-de-Monts au titre des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nantes du 15 juin 2012 est annulé.<br/>
<br/>
		Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nantes.<br/>
<br/>
Article 3 : M. B...versera une somme de 3 000 euros à la commune de Saint-Jean-de-Monts au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : Les conclusions présentées par M. B...au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 5 : La présente décision sera notifiée à M. A...B...et à la commune de Saint-Jean-de-Monts. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">68-02-04-02 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PROCÉDURES D'INTERVENTION FONCIÈRE. LOTISSEMENTS. AUTORISATION DE LOTIR. - APPROBATION D'UN PLU POSTÉRIEURE À L'AUTORISATION - FACULTÉ INSTAURÉE PAR L'ARTICLE L. 442-11 DU CODE DE L'URBANISME DE MODIFIER LES DOCUMENTS DU LOTISSEMENT POUR LES METTRE EN CONCORDANCE AVEC LE PLU - 1) ETENDUE - 2) POSSIBILITÉ D'EN FAIRE USAGE POUR MODIFIER UN CAHIER DES CHARGES DEVENU CADUC MAIS QUI CONTINUE À RÉGIR LES RAPPORTS ENTRE COLOTIS - EXISTENCE.
</SCT>
<ANA ID="9A"> 68-02-04-02 Les dispositions de l'article L. 442-11 du code de l'urbanisme, aux termes desquelles l'autorité compétente peut, après enquête publique et délibération du conseil municipal, modifier tout ou partie des documents d'un lotissement, et notamment le règlement et le cahier des charges, pour les mettre en concordance avec un plan local d'urbanisme (PLU) ou un document d'urbanisme en tenant lieu intervenu postérieurement au permis d'aménager ou à la décision de non-opposition à déclaration préalable :... ,,- 1) ne prévoient aucune exception au pouvoir qu'elles confèrent au maire de modifier tous les documents du lotissement, y compris le cahier des charges, dès lors que la modification a pour objet de mettre ces documents en concordance avec le PLU ou le document d'urbanisme en tenant lieu ;,,,- 2) peuvent être mises en oeuvre par le maire pour modifier le cahier des charges d'un lotissement devenu caduc en vertu de l'article L. 442-9 du code de l'urbanisme, mais qui continue de régir les rapports entre colotis, en cas de discordance entre ce cahier des charges et le PLU ou le document qui en tient lieu.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
