<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029677155</ID>
<ANCIEN_ID>JG_L_2014_10_000000376072</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/67/71/CETATEXT000029677155.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème / 7ème SSR, 31/10/2014, 376072</TITRE>
<DATE_DEC>2014-10-31</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>376072</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème / 7ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Tristan Aureau</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier Domino</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:376072.20141031</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 5 mars 2014 au secrétariat du contentieux du Conseil d'Etat, présenté par la société Free, dont le siège est 8, rue de la Ville l'Evêque à Paris (75008), et par la société Free Mobile, dont le siège est 16, rue de la Ville l'Evêque à Paris (75008) ; les sociétés Free et Free Mobile demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté du 31 décembre 2013 relatif aux factures des services de communications électroniques et à l'information du consommateur sur la consommation au sein de son offre ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros à verser à chacune des requérantes au titre de l'article L. 761-1 du code de justice administrative, ainsi que les entiers dépens ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 17 octobre 2014, présentée par les sociétés Free et Free Mobile ;<br/>
<br/>
              Vu la directive 2009/136/CE du Parlement européen et du Conseil du 25 novembre 2009 ; <br/>
<br/>
              Vu le code de la consommation ;<br/>
<br/>
              Vu le code des postes et des communications électroniques ;<br/>
<br/>
              Vu le décret n° 2012-912 du 25 juillet 2012 ;<br/>
<br/>
              Vu l'arrêté ministériel du 14 mars 2005 portant règlement intérieur du Conseil national de la consommation ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Tristan Aureau, auditeur,<br/>
<br/>
              - les conclusions de M. Xavier Domino, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant, d'une part, qu'aux termes du premier alinéa de l'article L. 113-3 du code de la consommation, dans sa version en vigueur à la date de la décision attaquée : " Tout vendeur de produit ou tout prestataire de services doit, par voie de marquage, d'étiquetage, d'affichage ou par tout autre procédé approprié, informer le consommateur sur les prix, les limitations éventuelles de la responsabilité contractuelle et les conditions particulières de la vente, selon des modalités fixées par arrêtés du ministre chargé de l'économie, après consultation du Conseil national de la consommation " ;  <br/>
<br/>
              2.	Considérant, d'autre part, qu'aux termes des dispositions du 1 du II de l'article D. 98-5 du code des postes et des communications électroniques, transposant les objectifs du a) de la partie A de l'annexe I de la directive 2009/136/CE du Parlement européen et du Conseil du 25 novembre 2009 : " L'opérateur garantit à tout client, outre les droits mentionnés à l'article R. 10, le droit : (...) - de recevoir des factures non détaillées et, sur sa demande, des factures détaillées. (...) La facturation détaillée est disponible gratuitement. Toutefois, des prestations supplémentaires peuvent être, le cas échéant, proposées à l'abonné à un tarif raisonnable " ;  <br/>
<br/>
              3.	Considérant que les sociétés requérantes demandent l'annulation pour excès de pouvoir de l'arrêté du 31 décembre 2013 relatif aux factures des services de communications électroniques et à l'information du consommateur sur la consommation au sein de son offre ; qu'en vertu des articles 1er et 3 de l'arrêté attaqué, les opérateurs de communications électroniques adressent gratuitement au consommateur, à la demande de celui-ci et avant paiement, ses factures et ses factures détaillées sur support papier ; <br/>
<br/>
              4.	Considérant, en premier lieu, qu'aux termes des dispositions précitées de l'article L. 113-3 du code de la consommation, il revient au ministre chargé de l'économie, après consultation du Conseil national de la consommation, de fixer par arrêté les modalités selon lesquelles le consommateur est informé sur les prix et les conditions particulières de la vente ; qu'en prévoyant que les prestataires de services de communications électroniques, qui sont par ailleurs soumis aux prescriptions du 1 du II de l'article D. 98-5 du code des postes et des communications électroniques, auront l'obligation de délivrer gratuitement au consommateur, avant tout paiement, et sur support papier s'il en fait la demande, les factures et les factures détaillées de consommation de ces services, le ministre chargé de l'économie s'est borné à déterminer des modalités d'information du consommateur sur les prix et les conditions particulières de vente des services de communications électroniques ; que, par suite, il a pu compétemment prendre l'arrêté attaqué sur le fondement des dispositions de l'article L. 113-3 du code de la consommation ; <br/>
<br/>
              5.	Considérant, en deuxième lieu, qu'aux termes des dispositions précitées de l'article L. 113-3 du code de la consommation, il revient au ministre chargé de l'économie de consulter le Conseil national de la consommation préalablement à l'adoption d'un arrêté fixant les modalités selon lesquelles le consommateur est informé sur les prix et les conditions particulières de la vente et de l'exécution des services ; qu'il ressort des pièces du dossier que le Conseil national de la consommation a été consulté sur le projet d'arrêté attaqué selon la procédure écrite prévue à l'article 1er de l'arrêté ministériel du 14 mars 2005 portant règlement intérieur du Conseil national de la consommation ; que, conformément à ces dispositions, une convocation a été adressée, le 10 mai 2013 par voie électronique, aux membres du bureau du Conseil national de la consommation en vue d'une réunion qui s'est tenue le 14 mai suivant et dont l'ordre du jour avait notamment pour objet la présentation d'un dossier de consultation écrite portant sur deux projets d'arrêtés relatifs aux factures des services de communications électroniques ; qu'un dossier de consultation écrite, comprenant une lettre de la directrice générale de la concurrence, de la consommation et de la répression des fraudes, un projet d'arrêté relatif aux factures des services de communications électroniques et à l'information du consommateur sur la consommation au sein de son offre, prévoyant notamment l'envoi au consommateur à titre gratuit et sur support papier des factures et factures détaillées de services de communications électroniques, ainsi qu'une note exposant les motifs de l'arrêté envisagé, a été adressé le 20 juin 2013 par voie électronique aux membres du Conseil national de la consommation ; que ces derniers ont tous pris part au vote dans les conditions prévues par l'avant-dernier alinéa de l'article 1er de l'arrêté précité du 14 mars 2005 ; que la circonstance que l'avis du Conseil national de la consommation n'ait pas été publié est sans incidence sur sa régularité ; que, par suite, le moyen tiré de ce que ce Conseil national de la consommation n'aurait pas été régulièrement consulté doit être écarté ; <br/>
<br/>
              6.	Considérant, en troisième lieu, que les dispositions précitées du II de l'article D. 98-5 du code des postes et des communications électroniques, transposant les dispositions du a) de la partie A de l'annexe I de la directive 2009/136/CE du Parlement européen et du Conseil du 25 novembre 2009, se bornent à garantir le droit du consommateur à recevoir, à titre gratuit, des factures non détaillées et, sur sa demande, des factures détaillées ; que ces dispositions n'ont ni pour objet ni pour effet d'imposer aux prestataires de services de communications électroniques la fourniture de factures sur un support plutôt que sur un autre ; que le moyen tiré de ce que l'arrêté attaqué méconnaîtrait ces dispositions, en tant qu'il fait obligation aux opérateurs de communications électroniques de mettre à disposition et d'adresser gratuitement au consommateur ses factures et ses factures détaillées sur support papier, ne peut, par suite, qu'être écarté ; <br/>
<br/>
              7.	Considérant, en quatrième lieu, que les dispositions de l'article 1er de la loi du 3 août 2009 de programmation relative à la mise en oeuvre du Grenelle de l'environnement sont dépourvues de portée normative ; qu'elles ne peuvent, par suite, être utilement invoquées à l'encontre de l'arrêté attaqué ; <br/>
<br/>
              8.	Considérant, en cinquième lieu, que, d'une part, la circonstance que l'arrêté attaqué ait pour conséquence une aggravation des charges d'exploitation des sociétés requérantes ainsi que la remise en cause de certaines de leurs offres commerciales est sans incidence sur sa légalité ; qu'au demeurant, l'arrêté attaqué s'applique à l'ensemble des opérateurs de communications électroniques ; que, d'autre part, si, aux termes de son article 2, les dispositions de l'arrêté attaqué " ne sont pas applicables aux offres sans abonnement intégralement prépayées ", toutefois, le consommateur d'une offre de services de communications électroniques avec abonnement est placé dans une situation différente de celle du consommateur d'une offre de services de communications électroniques sans abonnement intégralement prépayée ; qu'au demeurant, l'information du consommateur d'une offre de services de communications électroniques sans abonnement intégralement prépayée est réglementée par l'arrêté du 31 décembre 2013 relatif à l'information tarifaire des services de communications électroniques commercialisées sous la forme de cartes prépayées et de forfaits bloqués ; qu'ainsi, les moyens tirés de ce que l'arrêté attaqué méconnaîtrait le principe de liberté du commerce et de l'industrie et fausserait le jeu de la concurrence ne peuvent qu'être écartés ; <br/>
<br/>
              9.	Considérant, en sixième lieu, que le moyen tiré de ce que l'arrêté attaqué introduirait une discrimination entre les opérateurs de services liés à leurs clients par un contrat d'abonnement n'est pas assorti des précisions suffisantes permettant d'en apprécier le bien-fondé ;<br/>
<br/>
              10.	Considérant qu'il résulte de tout ce qui précède, et sans qu'il soit besoin de statuer sur la fin de non-recevoir opposée par le ministre de l'économie, du redressement productif et du numérique, que les sociétés Free et Free Mobile ne sont pas fondées à demander l'annulation de l'arrêté attaqué ; que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
                --------------<br/>
<br/>
Article 1er : La requête des sociétés Free et Free Mobile est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée aux sociétés Free et Free Mobile et au ministre de l'économie, de l'industrie et du numérique.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-02-02-01-03-05 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - COMPÉTENCE. RÉPARTITION DES COMPÉTENCES ENTRE AUTORITÉS DISPOSANT DU POUVOIR RÉGLEMENTAIRE. AUTORITÉS DISPOSANT DU POUVOIR RÉGLEMENTAIRE. MINISTRES. MINISTRE DE L'ÉCONOMIE ET DES FINANCES. - COMPÉTENCE DU MINISTRE CHARGÉ DE L'ÉCONOMIE POUR FIXER LES MODALITÉS SELON LESQUELLES LE CONSOMMATEUR EST INFORMÉ SUR LES PRIX ET CONDITIONS PARTICULIÈRES DE VENTE (ART. L. 113-3 DU CODE DE LA CONSOMMATION) - PORTÉE - INCLUSION - OBLIGATION FAITE AUX OPÉRATEURS DE COMMUNICATIONS ÉLECTRONIQUES DE DÉLIVRER GRATUITEMENT DES FACTURES DÉTAILLÉES AVANT TOUT PAIEMENT [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">14-02-01-03 COMMERCE, INDUSTRIE, INTERVENTION ÉCONOMIQUE DE LA PUISSANCE PUBLIQUE. RÉGLEMENTATION DES ACTIVITÉS ÉCONOMIQUES. ACTIVITÉS SOUMISES À RÉGLEMENTATION. RÉGLEMENTATION DE LA PROTECTION ET DE L'INFORMATION DES CONSOMMATEURS. - COMPÉTENCE DU MINISTRE CHARGÉ DE L'ÉCONOMIE POUR FIXER LES MODALITÉS SELON LESQUELLES LE CONSOMMATEUR EST INFORMÉ SUR LES PRIX ET CONDITIONS PARTICULIÈRES DE VENTE (ART. L. 113-3 DU CODE DE LA CONSOMMATION) - PORTÉE - INCLUSION - OBLIGATION FAITE AUX OPÉRATEURS DE COMMUNICATIONS ÉLECTRONIQUES DE DÉLIVRER GRATUITEMENT DES FACTURES DÉTAILLÉES AVANT TOUT PAIEMENT [RJ1].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">51-02-01-01 POSTES ET COMMUNICATIONS ÉLECTRONIQUES. COMMUNICATIONS ÉLECTRONIQUES. TÉLÉPHONE. CONTRATS D'ABONNEMENT. - COMPÉTENCE DU MINISTRE CHARGÉ DE L'ÉCONOMIE POUR FIXER LES MODALITÉS SELON LESQUELLES LE CONSOMMATEUR EST INFORMÉ SUR LES PRIX ET CONDITIONS PARTICULIÈRES DE VENTE (ART. L. 113-3 DU CODE DE LA CONSOMMATION) - PORTÉE - INCLUSION - OBLIGATION FAITE AUX OPÉRATEURS DE COMMUNICATIONS ÉLECTRONIQUES DE DÉLIVRER GRATUITEMENT DES FACTURES DÉTAILLÉES AVANT TOUT PAIEMENT [RJ1].
</SCT>
<ANA ID="9A"> 01-02-02-01-03-05 Aux termes de l'article L. 113-3 du code de la consommation, il revient au ministre chargé de l'économie, après consultation du Conseil national de la consommation, de fixer par arrêté les modalités selon lesquelles le consommateur est informé par le vendeur ou le prestataire de service sur les prix et les conditions particulières de la vente.,,,Ainsi, n'est pas entaché d'incompétence l'arrêté par lequel le ministre chargé de l'économie a prévu que les prestataires de services de communications électroniques, qui sont par ailleurs soumis aux prescriptions du 1 du II de l'article D. 98-5 du code des postes et des communications électroniques, auront l'obligation de délivrer gratuitement au consommateur, avant tout paiement, et sur support papier s'il en fait la demande, les factures et les factures détaillées de consommation de ces services.</ANA>
<ANA ID="9B"> 14-02-01-03 Aux termes de l'article L. 113-3 du code de la consommation, il revient au ministre chargé de l'économie, après consultation du Conseil national de la consommation, de fixer par arrêté les modalités selon lesquelles le consommateur est informé par le vendeur ou le prestataire de service sur les prix et les conditions particulières de la vente.,,,Ainsi, n'est pas entaché d'incompétence l'arrêté par lequel le ministre chargé de l'économie a prévu que les prestataires de services de communications électroniques, qui sont par ailleurs soumis aux prescriptions du 1 du II de l'article D. 98-5 du code des postes et des communications électroniques, auront l'obligation de délivrer gratuitement au consommateur, avant tout paiement, et sur support papier s'il en fait la demande, les factures et les factures détaillées de consommation de ces services.</ANA>
<ANA ID="9C"> 51-02-01-01 Aux termes de l'article L. 113-3 du code de la consommation, il revient au ministre chargé de l'économie, après consultation du Conseil national de la consommation, de fixer par arrêté les modalités selon lesquelles le consommateur est informé par le vendeur ou le prestataire de service sur les prix et les conditions particulières de la vente.,,,Ainsi, n'est pas entaché d'incompétence l'arrêté par lequel le ministre chargé de l'économie a prévu que les prestataires de services de communications électroniques, qui sont par ailleurs soumis aux prescriptions du 1 du II de l'article D. 98-5 du code des postes et des communications électroniques, auront l'obligation de délivrer gratuitement au consommateur, avant tout paiement, et sur support papier s'il en fait la demande, les factures et les factures détaillées de consommation de ces services.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., sur le fondement de l'article 33 de l'ordonnance du 30 juin 1945, CE, 23 mars 1988, Chambre de commerce et d'industrie de Paris, n° 55454, inédit au Recueil.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
