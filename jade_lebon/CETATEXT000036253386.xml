<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036253386</ID>
<ANCIEN_ID>JG_L_2017_12_000000400669</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/25/33/CETATEXT000036253386.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème - 10ème chambres réunies, 22/12/2017, 400669</TITRE>
<DATE_DEC>2017-12-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>400669</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème - 10ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Marie-Gabrielle Merloz</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Yohann Bénard</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:400669.20171222</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés le 13 juin 2016 et le 10 mars 2017 au secrétariat du contentieux du Conseil d'Etat, M. B... A..., le groupement agricole d'exploitation en commun (GAEC) Ausset et le groupement agricole d'exploitation en commun (GAEC) Rouches demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2016-434 du 11 avril 2016 portant modification de la partie réglementaire du code de l'énergie relative aux schémas régionaux de raccordement au réseau des énergies renouvelables ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'énergie ;<br/>
              - le décret n° 2012-533 du 20 avril 2012 ;<br/>
              - le décret n° 2014-760 du 2 juillet 2014 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie-Gabrielle Merloz, maître des requêtes,<br/>
<br/>
              - les conclusions de M. Yohann Bénard, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              Sur la recevabilité de l'intervention de la Confédération nationale de l'élevage :<br/>
<br/>
              1. La Confédération nationale de l'élevage, qui, selon l'article 7 de ses statuts, a pour objet " la défense des intérêts de l'élevage de ruminants " et, à cet effet, peut notamment " se saisir de toute question technique ou économique intéressant l'élevage... " justifie d'un intérêt suffisant à l'annulation du décret attaqué. Ainsi, son intervention est recevable. <br/>
<br/>
              Sur la recevabilité de la requête : <br/>
<br/>
              2. Aux termes de l'article 1er du décret du 20 avril 2012 relatif aux schémas régionaux de raccordement au réseau des énergies renouvelables, prévus par l'article L. 321-7 du code de l'énergie : " Les conditions de raccordement aux réseaux publics d'électricité des installations de production d'électricité à partir de sources d'énergies renouvelables, d'une puissance installée supérieure à 36 kilovoltampères, sont fixées par le présent décret ". L'article 2 du décret du 2 juillet 2014 a, d'une part, a supprimé les mots " d'une puissance installée supérieure à 36 kilovoltampères " et, d'autre part, ajouté un nouvel alinéa ainsi rédigé : " Pour l'application du deuxième alinéa de l'article L. 342-1 du code de l'énergie, ne s'inscrivent pas dans le schéma régional de raccordement au réseau des énergies renouvelables : / -les raccordements d'installations d'une puissance installée inférieure ou égale à 100 kilovoltampères ; / -les raccordements d'installations dont les conditions sont fixées dans le cadre d'un appel d'offres en application de l'article L. 311-10 du code de l'énergie ". Ces dispositions, désormais codifiées à l'article D. 321-10 du code de l'énergie, ont été de nouveau modifiées par l'article 1er du décret attaqué, aux termes duquel : " L'article D. 321-10 du code de l'énergie est remplacé par les dispositions suivantes : / " Art. D. 321-10. - La présente section et la section 6 du chapitre II du titre IV du présent livre fixent les conditions de raccordement aux réseaux publics d'électricité des catégories d'installation suivantes : / - installation de production d'électricité à partir de sources d'énergies renouvelables d'une puissance de raccordement supérieure à 100 kilovoltampères ; / - installations groupées dont la somme des puissances de raccordement est supérieure à 100 kilovoltampères. / Pour l'application du précédent alinéa, une installation est considérée comme faisant partie d'un groupe dès lors que d'autres installations utilisant le même type d'énergie et appartenant à la même société ou à des sociétés qui lui sont liées au sens de l'article L. 336-4 sont déjà raccordées ou entrées en file d'attente en vue de leur raccordement sur un poste dont le niveau de tension primaire est immédiatement supérieur à leur tension de raccordement de référence. / Pour l'application du deuxième alinéa de l'article L. 342-1, les installations dont les conditions de raccordement sont fixées dans le cadre de la procédure prévue à l'article L. 311-10 ne s'inscrivent pas dans le schéma régional de raccordement au réseau des énergies renouvelables ".<br/>
<br/>
              3. Il résulte de ces dispositions que l'article 1er du décret attaqué substitue une nouvelle rédaction de l'article D. 321-10 du code de l'énergie à celle issue de l'article 2 du décret du 2 juillet 2014 modifiant le décret du 20 avril 2012 relatif aux schémas régionaux de raccordement au réseau des énergies renouvelables, prévus par l'article L. 321-7 du code de l'énergie. S'il laisse inchangé le seuil de puissance installée, porté à 100 kilovoltampères par le décret du 2 juillet 2014, au-dessus duquel les installations de production d'électricité à partir de sources d'énergies renouvelables sont raccordées dans le cadre d'un schéma régional de raccordement au réseau des énergies renouvelables, il précise par ailleurs le dispositif en soumettant aux mêmes conditions de raccordement les installations dites groupées dont il donne la définition. Il suit de là que l'article 1er du décret attaqué, qui modifie la portée des dispositions de l'article D. 321-10 du code de l'énergie, ne peut être regardé comme se bornant à reproduire, sous réserve de modifications de pure forme, des dispositions antérieures devenues définitives. Dès lors, la ministre n'est pas fondée à soutenir que les conclusions dirigées contre ces dispositions sont tardives et, par suite, irrecevables. <br/>
<br/>
              Sans qu'il soit besoin d'examiner les autres moyens de la requête : <br/>
<br/>
              4. Aux termes de cet article L. 134-10 du code de l'énergie : " La Commission de régulation de l'énergie est préalablement consultée sur les projets de dispositions à caractère réglementaire relatifs à l'accès aux réseaux publics de transport et de distribution d'électricité, aux ouvrages de transport et de distribution de gaz naturel et aux installations de gaz naturel liquéfié et à leur utilisation. Elle est également consultée sur le projet de décret en Conseil d'Etat fixant les obligations d'Electricité de France et des fournisseurs bénéficiant de l'électricité nucléaire historique et les conditions de calcul des volumes et conditions d'achat de cette dernière prévu à l'article L. 336-10 ". <br/>
<br/>
              5. Le décret attaqué modifie la partie réglementaire du code de l'énergie relative aux schémas régionaux de raccordement au réseau des énergies renouvelables. Il précise notamment le champ d'application de ce dispositif, instaure une procédure d'adaptation de ces schémas régionaux afin de permettre d'effectuer des modifications de portée limitée sur les ouvrages des réseaux publics et ouvre la possibilité de les réviser dans certaines hypothèses. Ce texte, qui modifie les conditions de raccordement des installations de production d'électricité à partir de sources d'énergies renouvelables dans le cadre des schémas régionaux de raccordement au réseau des énergies renouvelables et, en particulier, le périmètre de facturation et le partage des coûts de ce raccordement, a des effets sur les modalités d'accès aux réseaux publics d'électricité des producteurs. Il devait, par suite, être obligatoirement soumis pour avis à la Commission de régulation de l'énergie en application de l'article L. 134-10 précité du code de l'énergie. L'omission de cette consultation préalable ayant été susceptible d'exercer, en l'espèce, une influence sur le contenu du décret attaqué, les requérants sont fondés à soutenir que le décret attaqué a été pris à l'issue d'une procédure irrégulière et doit, dès lors, être annulé.  <br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros, à verser par parts égales à M. B... A..., au GAEC Ausset et au GAEC Rouches au titre de l'article L. 761-1 du code de justice administrative.   <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'intervention de la Confédération nationale de l'élevage est admise. <br/>
Article 2 : Le décret du 11 avril 2016 portant modification de la partie réglementaire du code de l'énergie relative aux schémas régionaux de raccordement au réseau des énergies renouvelables est annulé. <br/>
Article 3 : L'Etat versera à M. B... A..., au GAEC Ausset et au GAEC Rouches la somme de 1 000 euros chacun au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à M. B... A..., représentant unique, pour l'ensemble des requérants et au Premier ministre et au ministre d'Etat, ministre de la transition écologique et solidaire.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-03-02-02 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - FORME ET PROCÉDURE. PROCÉDURE CONSULTATIVE. CONSULTATION OBLIGATOIRE. - CONSULTATION PRÉALABLE DE LA CRE SUR LES PROJETS DE DISPOSITIONS RELATIVES À L'ACCÈS AUX RÉSEAUX PUBLICS DE TRANSPORT ET DE DISTRIBUTION D'ÉLECTRICITÉ (ART. L. 134-10 DU CODE DE L'ÉNERGIE) - DÉCRET MODIFIANT LES CONDITIONS DE RACCORDEMENT DES INSTALLATIONS DE PRODUCTION D'ÉLECTRICITÉ À PARTIR DE SOURCES D'ÉNERGIES RENOUVELABLES DANS LE CADRE DES SCHÉMAS RÉGIONAUX DE RACCORDEMENT AU RÉSEAU DES ÉNERGIES RENOUVELABLES - INCLUSION.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">29-06-01 ENERGIE. MARCHÉ DE L'ÉNERGIE. COMMISSION DE RÉGULATION DE L'ÉNERGIE. - DISPOSITIONS RELATIVES À L'ACCÈS AUX RÉSEAUX PUBLICS DE TRANSPORT ET DE DISTRIBUTION D'ÉLECTRICITÉ (ART. L. 134-10 DU CODE DE L'ÉNERGIE) - NOTION - DÉCRET MODIFIANT LES CONDITIONS DE RACCORDEMENT DES INSTALLATIONS DE PRODUCTION D'ÉLECTRICITÉ À PARTIR DE SOURCES D'ÉNERGIES RENOUVELABLES DANS LE CADRE DES SCHÉMAS RÉGIONAUX DE RACCORDEMENT AU RÉSEAU DES ÉNERGIES RENOUVELABLES - INCLUSION - CONSÉQUENCE - CONSULTATION PRÉALABLE OBLIGATOIRE DE LA CRE.
</SCT>
<ANA ID="9A"> 01-03-02-02 Un décret qui modifie les conditions de raccordement des installations de production d'électricité à partir de sources d'énergies renouvelables dans le cadre des schémas régionaux de raccordement au réseau des énergies renouvelables et, en particulier, le périmètre de facturation et le partage des coûts de ce raccordement, a des effets sur les modalités d'accès aux réseaux publics d'électricité des producteurs. Il doit, par suite, être obligatoirement soumis pour avis à la Commission de régulation de l'énergie (CRE) en application de l'article L. 134-10 du code de l'énergie.</ANA>
<ANA ID="9B"> 29-06-01 Un décret qui modifie les conditions de raccordement des installations de production d'électricité à partir de sources d'énergies renouvelables dans le cadre des schémas régionaux de raccordement au réseau des énergies renouvelables et, en particulier, le périmètre de facturation et le partage des coûts de ce raccordement, a des effets sur les modalités d'accès aux réseaux publics d'électricité des producteurs. Il doit, par suite, être obligatoirement soumis pour avis à la Commission de régulation de l'énergie (CRE) en application de l'article L. 134-10 du code de l'énergie.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
