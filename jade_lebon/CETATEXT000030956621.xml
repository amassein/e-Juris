<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030956621</ID>
<ANCIEN_ID>JG_L_2015_07_000000375042</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/95/66/CETATEXT000030956621.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Section du Contentieux, 27/07/2015, 375042, Publié au recueil Lebon</TITRE>
<DATE_DEC>2015-07-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>375042</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Section du Contentieux</FORMATION>
<TYPE_REC>Contentieux des pensions</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELVOLVE</AVOCATS>
<RAPPORTEUR>Mme Mireille Le Corre</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESEC:2015:375042.20150727</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Mme B...A...a demandé au tribunal administratif de Nîmes d'annuler, d'une part, la décision du 20 décembre 2011 du directeur régional des finances publiques de la région Languedoc-Roussillon lui refusant un complément de pension de réversion, d'autre part, la décision du directeur du service des retraites de l'Etat du 2 février 2012 refusant à ses trois enfants le versement d'une pension d'orphelin. Par un jugement n° 1200550 du 24 décembre 2013, le tribunal administratif de Nîmes a, d'une part, annulé la décision du directeur du service des retraites de l'Etat du 2 février 2012, d'autre part, enjoint au directeur régional des finances publiques de la région Languedoc-Roussillon de procéder au paiement des sommes dues à Mme A...au titre des pensions d'orphelin de ses trois enfants, et a rejeté le surplus des conclusions de la demande.<br/>
<br/>
              Par un pourvoi et un mémoire en réplique, enregistrés les 30 janvier et 17 juin 2014 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'économie et des finances demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement du tribunal administratif de Nîmes en tant qu'il a annulé la décision du 2 février 2012 et enjoint au directeur régional des finances publiques de la région Languedoc-Roussillon de procéder au paiement des sommes dues à Mme A...au titre des pensions d'orphelin de ses trois enfants ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de Mme A... sur ce point ;<br/>
<br/>
              3°) de rejeter le pourvoi incident de MmeA....<br/>
<br/>
              Le ministre de l'économie et des finances soutient que le tribunal administratif de Nîmes a commis une erreur de droit en annulant la décision refusant à Mme A...le versement des pensions temporaires d'orphelin de ses trois enfants, dès lors que ces pensions ne peuvent être cumulées avec les prestations familiales ; il soutient, en outre, que les moyens du pourvoi incident ne sont pas fondés.<br/>
<br/>
              Par un mémoire en défense, enregistré le 2 mai 2014, Mme B... A...conclut au rejet du pourvoi et, par la voie du pourvoi incident, à l'annulation du jugement attaqué en tant qu'il a rejeté sa demande tendant à l'annulation de la décision du 20 décembre 2011 refusant de faire droit à sa demande de versement d'un complément de pension de réversion en application de l'article L. 38 du code des pensions civiles et militaires de retraite. <br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              	Vu :<br/>
              - le code des pensions civiles et militaires de retraite ;<br/>
              - le code de la sécurité sociale ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Mireille Le Corre, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Delvolvé, avocat de Mme B...A...;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il résulte des pièces du dossier soumis aux juges du fond que l'époux de MmeA..., militaire, est décédé en décembre 2006 ; que Mme A...bénéficie à ce titre d'une pension de réversion et ses trois enfants d'une pension temporaire d'orphelin prévue à l'article L. 40 du code des pensions civiles et militaires de retraite ; que, par décision du 20 décembre 2011, le directeur régional des finances publiques de la région Languedoc-Roussillon a refusé à Mme A... le versement d'un complément de pension de réversion ; que, par décision du 2 février 2012, le directeur du service des retraites de l'Etat a opposé un refus à sa demande de versement des sommes réclamées au titre de la pension d'orphelin pour ses trois enfants ; que le ministre de l'économie et des finances se pourvoit en cassation contre le jugement du tribunal administratif de Nîmes du 24 décembre 2013, en tant qu'il a annulé la décision du 2 février 2012 refusant à Mme A...le versement des pensions temporaires d'orphelin ; que, par la voie du pourvoi incident, Mme A...demande l'annulation du même jugement en tant qu'il a rejeté ses conclusions dirigées contre la décision du 20 décembre 2011 refusant de lui accorder un complément de pension de réversion ;<br/>
<br/>
              Sur le pourvoi principal :<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 40 du code des pensions civiles et militaires de retraite : " Chaque orphelin a droit jusqu'à l'âge de vingt-et-un ans à une pension égale à 10 % de la pension obtenue par le fonctionnaire ou qu'il aurait pu obtenir au jour de son décès, et augmentée, le cas échéant, de 10 % de la rente d'invalidité dont il bénéficiait ou aurait pu bénéficier, sans que le total des émoluments attribués aux conjoints survivants ou divorcés et aux orphelins puisse excéder le montant de la pension et, éventuellement, de la rente d'invalidité attribuées ou qui auraient été attribuées au fonctionnaire. S'il y a excédent, il est procédé à la même réduction temporaire des pensions des orphelins. (...)  " ; qu'aux termes de l'article L. 89 du même code : " Est interdit du chef d'un même enfant, le cumul de plusieurs accessoires de traitement, solde, salaire et pension servis par l'Etat, les collectivités publiques et les organismes de prévoyance collectifs ou obligatoires, aux intéressés ou à leur conjoint, dans les conditions prévues à l'article L. 553-3 du code de la sécurité sociale. Cette interdiction ne s'applique pas à la majoration de pension prévue à l'article L. 18. (...) " ; qu'aux termes de l'article L. 553-3 du code de la sécurité sociale : " Lorsqu'un même enfant ouvre droit aux prestations familiales et à une majoration de l'une quelconque des allocations ci-après énumérées : (...) 4° retraites ou pensions attribuées par l'Etat, les collectivités publiques ou les organismes de prévoyance obligatoire, les prestations familiales sont perçues par priorité et excluent, à due concurrence, lesdites majorations (...) " ; <br/>
<br/>
              3. Considérant que les dispositions de l'article L. 40 du code des pensions civiles et militaires précité confèrent à l'enfant orphelin d'un fonctionnaire décédé un droit à une pension ; que cette pension se distingue des droits du conjoint du fonctionnaire décédé et constitue, comme cela résulte d'ailleurs de la dénomination qui lui est donnée par les textes, un droit propre de l'enfant, ; qu'en outre, cette pension est due à l'enfant orphelin jusqu'à l'âge de vingt-et-un ans et peut donc bénéficier à des enfants majeurs ; qu'il résulte de ce qui précède qu'une telle pension d'orphelin ne peut être assimilée ni à un accessoire ni à une majoration de la pension de réversion perçue par le conjoint du fonctionnaire décédé ; que les dispositions en vigueur de l'article L. 553-3 du code de la sécurité sociale selon lesquelles les prestations familiales sont dues par priorité lorsqu'un enfant du fonctionnaire ouvre droit à une majoration de pension et excluent, à due concurrence, lesdites majorations, ne mentionnent pas les pensions d'orphelin, qui ont un objet distinct des prestations familiales comme des majorations de pension pour charges de famille ; que, dès lors, ces dispositions ne sont pas applicables à la pension d'orphelin ; qu'il suit de là que la pension d'orphelin prévue par l'article L. 40 du code des pensions civiles et militaires de retraite peut être cumulée avec les prestations familiales ; qu'ainsi, le tribunal administratif de Nîmes n'a pas commis d'erreur de droit en annulant, pour ce motif, la décision du 2 février 2012 refusant à Mme A...le versement des pensions d'orphelin dues à ses trois enfants ;<br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède que le pourvoi du ministre de l'économie et des finances doit être rejeté ;<br/>
<br/>
              Sur le pourvoi incident :<br/>
<br/>
              5. Considérant qu'aux termes de l'article L. 38 du code des pensions civiles et militaires de retraite : " Les conjoints d'un fonctionnaire civil ont droit à une pension de réversion égale à 50 % de la pension obtenue par le fonctionnaire ou qu'il aurait pu obtenir au jour de son décès. A la pension de réversion s'ajoutent, le cas échéant : 1° La moitié de la rente d'invalidité dont le fonctionnaire bénéficiait ou aurait pu bénéficier ; 2° La moitié de la majoration prévue à l'article L. 18, obtenue ou qu'aurait pu obtenir le fonctionnaire, si le bénéficiaire de la pension de réversion a élevé, dans les conditions prévues audit article L. 18, les enfants ouvrant droit à cette majoration. Le total de la pension de réversion, quelle que soit la date de sa mise en paiement, et des autres ressources de son bénéficiaire ne peut être inférieur à celui de l'allocation servie aux vieux travailleurs salariés augmentée de l'allocation supplémentaire du Fonds de solidarité vieillesse institué par les articles L. 811-1 et L. 815-2 du code de la sécurité sociale. " ; qu'aux termes de l'article 43 du même code dans sa version applicable au litige : " Lorsqu'il existe une pluralité d'ayants cause de lits différents, la pension définie à l'article L. 38 est divisée en parts égales entre les lits représentés par le conjoint survivant ou divorcé ayant droit à pension ou par un ou plusieurs orphelins âgés de moins de vingt et un ans. (...) " ; qu'aux termes de l'article D. 19-1 du même code : " Peuvent être élevées au minimum de pension prévu au troisième alinéa de l'article L. 38 du présent code les pensions de réversion au taux de 50 % allouées aux ayants cause de fonctionnaires ou de militaires. / Lorsque la pension est partagée entre plusieurs ayants cause, la part du minimum de pension pouvant être attribuée à chaque bénéficiaire en fonction de ses ressources propres est calculée au prorata de la fraction de pension qui lui est personnellement allouée. " ;<br/>
<br/>
              6. Considérant qu'il résulte des dispositions précitées de l'article L. 38 du code des pensions civiles et militaires de retraite que le total de la pension de réversion et des autres ressources de son bénéficiaire ne peut être inférieur à celui de l'allocation de solidarité aux personnes âgées ayant remplacé les allocations mentionnées par cet article ; que, dans l'hypothèse d'une pluralité d'ayants cause, les dispositions de l'article L. 43 du même code instituent un partage de la pension de réversion ; que c'est pour l'application combinée de ces dispositions qu'il est prévu à l'article D. 19-1 du même code qu'en cas de pluralité d'ayants cause, le montant du complément de pension attribué à chaque bénéficiaire en fonction de ses ressources propres est apprécié au regard d'un montant minimum calculé au prorata de la fraction de pension qui lui est personnellement allouée ; qu'ainsi, en faisant application, par une décision suffisamment motivée, des dispositions de l'article D. 19-1 du code des pensions civiles et militaires de retraite précité, prévoyant que la part du minimum de pension pouvant être attribuée à chaque bénéficiaire en fonction de ses ressources propres est calculée au prorata de la fraction de pension qui lui est personnellement allouée, le tribunal administratif de Nîmes n'a pas commis d'erreur de droit ;<br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que le pourvoi incident de Mme A... doit être rejeté ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi du ministre de l'économie et des finances est rejeté.<br/>
<br/>
      Article 2 : Le pourvoi incident de Mme A...est rejeté. <br/>
Article 3 : La présente décision sera notifiée au ministre des finances et des comptes publics et à Mme B... A....<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">48-02-01-09-02 PENSIONS. PENSIONS CIVILES ET MILITAIRES DE RETRAITE. QUESTIONS COMMUNES. AYANTS-CAUSE. ORPHELINS. - PENSION TEMPORAIRE D'ORPHELIN (ART. L. 40 DU CPCM) - CARACTÈRE DE MAJORATION OU D'ACCESSOIRE DE LA PENSION DE RÉVERSION PERÇUE PAR LE CONJOINT DU FONCTIONNAIRE DÉCÉDÉ - ABSENCE - CONSÉQUENCE - INAPPLICABILITÉ DE LA RÈGLE INTERDISANT LE CUMUL ENTRE LES PRESTATIONS FAMILIALES ET UNE MAJORATION DE PENSION DU FAIT D'UN MÊME ENFANT (ART. L. 533 DU CSS) [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">62-04-06 SÉCURITÉ SOCIALE. PRESTATIONS. PRESTATIONS FAMILIALES ET ASSIMILÉES. - PENSION TEMPORAIRE D'ORPHELIN (ART. L. 40 DU CPCM) - CARACTÈRE DE MAJORATION OU D'ACCESSOIRE DE LA PENSION DE RÉVERSION PERÇUE PAR LE CONJOINT DU FONCTIONNAIRE DÉCÉDÉ - ABSENCE - CONSÉQUENCE - INAPPLICABILITÉ DE LA RÈGLE INTERDISANT LE CUMUL ENTRE LES PRESTATIONS FAMILIALES ET UNE MAJORATION DE PENSION DU FAIT D'UN MÊME ENFANT (ART. L. 533 DU CSS) [RJ1].
</SCT>
<ANA ID="9A"> 48-02-01-09-02 L'article L. 553-3 du code de la sécurité sociale (CSS) prévoit que lorsqu'un même enfant ouvre droit aux prestations familiales et à une majoration de pension, les prestations familiales sont perçues par priorité et excluent, à due concurrence, cette majoration.... ,,La pension d'orphelin prévue par l'article L. 40 du code des pensions civiles et militaires (CPCM) en faveur de l'enfant orphelin d'un fonctionnaire décédé, se distingue des droits du conjoint du fonctionnaire décédé et constitue un droit propre de l'enfant. Cette pension est due à l'enfant orphelin jusqu'à l'âge de vingt-et-un ans et peut donc bénéficier à des enfants majeurs. Il en résulte qu'une telle pension d'orphelin ne peut être assimilée ni à un accessoire ni à une majoration de la pension de réversion perçue par le conjoint du fonctionnaire décédé. Dès lors, les dispositions de l'article L. 553-3 du CSS ne sont pas applicables à la pension d'orphelin qui peut être cumulée avec les prestations familiales.</ANA>
<ANA ID="9B"> 62-04-06 L'article L. 553-3 du code de la sécurité sociale (CSS) prévoit que lorsqu'un même enfant ouvre droit aux prestations familiales et à une majoration de pension, les prestations familiales sont perçues par priorité et excluent, à due concurrence, cette majoration.... ,,La pension d'orphelin prévue par l'article L. 40 du code des pensions civiles et militaires (CPCM) en faveur de l'enfant orphelin d'un fonctionnaire décédé, se distingue des droits du conjoint du fonctionnaire décédé et constitue un droit propre de l'enfant. Cette pension est due à l'enfant orphelin jusqu'à l'âge de vingt-et-un ans et peut donc bénéficier à des enfants majeurs. Il en résulte qu'une telle pension d'orphelin ne peut être assimilée ni à un accessoire ni à une majoration de la pension de réversion perçue par le conjoint du fonctionnaire décédé. Dès lors, les dispositions de l'article L. 553-3 du CSS ne sont pas applicables à la pension d'orphelin qui peut être cumulée avec les prestations familiales.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Ab. jur. CE, 30 novembre 1962, Dame veuve,, n° 44311, p. 650 ; CE, 17 septembre 2013, Ministre de l'économie et des finances c/ Mme,, n° 367396, T. p. 729.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
