<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000008252161</ID>
<ANCIEN_ID>JG_L_2006_12_000000285740</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/08/25/21/CETATEXT000008252161.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 06/12/2006, 285740</TITRE>
<DATE_DEC>2006-12-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>285740</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Genevois</PRESIDENT>
<AVOCATS/>
<RAPPORTEUR>M. Sébastien  Veil</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Derepas</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 4 octobre 2005 au secrétariat du contentieux du Conseil d'Etat, présentée par l'UNION SYNDICALE SOLIDAIRES ISERE, dont le siège est 12 bis, rue des Trembles à Grenoble (38100) ; l'UNION SYNDICALE SOLIDAIRES ISERE demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le décret n° 2005-908 du 2 août 2005 relatif à la durée du travail dans l'animation ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement d'une somme de 1 000 euros en application de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code du travail ;<br/>
<br/>
              Vu la convention collective nationale de l'animation du 28 juin 1988 étendue par l'arrêté du 10 janvier 1989 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Sébastien Veil, Auditeur,  <br/>
<br/>
              - les conclusions de M. Luc Derepas, Commissaire du gouvernement ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant qu'en vertu de l'article L. 212-1 du code du travail, « dans les établissements ou professions mentionnés à l'article L. 200-1, ainsi que dans les établissements artisanaux et coopératifs et leurs dépendances, la durée légale du travail effectif des salariés est fixée à trente-cinq heures par semaine » ; qu'aux termes du dernier alinéa de l'article L. 212-4 du même code : « Une durée équivalente à la durée légale peut être instituée dans les professions et pour des emplois déterminés comportant des périodes d'inaction soit par décret, pris après conclusion d'une convention ou d'un accord de branche, soit par décret en Conseil d'Etat. Ces périodes sont rémunérées conformément aux usages ou aux conventions ou accords collectifs » ;<br/>
<br/>
              Considérant que sur le fondement de ces dispositions, le décret du 2 août 2005 a institué deux régimes d'équivalence concernant les personnels du secteur de l'animation ; qu'aux termes de l'article 1er de ce texte : « Les dispositions du présent décret s'appliquent au personnel à temps complet des entreprises de droit privé, sans but lucratif, qui développent à titre principal des activités d'intérêt social dans les domaines culturels, éducatifs, de loisirs et de plein air, notamment par des actions continues ou ponctuelles d'animation, de diffusion ou d'information créatives ou récréatives ouvertes à toute catégorie de population. / Elles s'appliquent également aux salariés à temps complet des entreprises de droit privé, sans but lucratif, qui développent à titre principal des activités d'intérêt général de protection de la nature et de l'environnement, notamment par des actions continues ou ponctuelles, de protection de la conservation des sites et espèces, d'éducation à l'environnement, d'études, de contribution au débat public, de formation, de diffusion, d'information ouvertes à toute catégorie de population » ; qu'aux termes de l'article 2 de ce décret : « La durée du travail, équivalente à la durée légale, des personnels mentionnés à l'article 1er, amenés à travailler dans le cadre d'un accueil ou d'un accompagnement de groupe avec nuitées rendant leur présence obligatoire nécessaire de jour comme de nuit est fixée à sept heures pour une durée de présence journalière de treize heures » ; qu'enfin, aux termes de l'article 3 de ce décret : « La durée du travail, équivalente à la durée légale, des personnels mentionnés à l'article 1er est fixée, dans le cadre de permanences nocturnes effectuées sur le lieu de travail et comportant des périodes d'inaction, à 2 h 30 pour une durée de présence de onze heures » ;<br/>
<br/>
              Considérant, en premier lieu, que les dispositions combinées des articles L. 212-1 et L. 212-4 du code du travail, pas plus qu'aucune autre disposition législative ou réglementaire n'imposent que ce mode de comptabilisation servant à définir la durée équivalente à la durée légale soit défini sur une base hebdomadaire, dès lors que le mode de comptabilisation adopté permet de déterminer le nombre d'heures de travail effectif sur une semaine dans le cadre du régime d'équivalence ; que, par suite, en instituant une durée du travail équivalente à la durée légale avec un rapport d'équivalence défini à partir de la durée de présence journalière mais permettant de déterminer la durée effective de travail pendant une semaine, les articles 2 et 3 du décret attaqué n'ont pas méconnu les dispositions des articles L. 212-1 et L. 212-4 précités ;<br/>
<br/>
              Considérant, en deuxième lieu, qu'il résulte des termes mêmes de ses articles 2 et 3 que les régimes d'équivalence différents, institués par le décret attaqué, se bornent à définir les règles applicables aux personnels du secteur de l'animation respectivement lorsqu'ils sont amenés à travailler dans le cadre d'un accueil ou d'un accompagnement de groupe avec nuitées rendant leur présence obligatoire nécessaire de jour comme de nuit, et lorsqu'ils effectuent des permanences nocturnes sur le lieu de travail comportant des périodes d'inaction ; que le décret n'a ni pour objet, ni pour effet d'autoriser le cumul des deux régimes ainsi définis, ni de conduire un salarié relevant du secteur de l'animation à effectuer une activité professionnelle pendant une durée pouvant atteindre vingt-quatre heures ; que, par suite, le moyen tiré de la méconnaissance des dispositions de l'article L. 220-1 du code du travail qui garantit un temps de repos quotidien aux salariés ne peut qu'être écarté ;<br/>
<br/>
              Considérant, en troisième lieu, que si l'article 1er du décret litigieux précise qu'il s'applique au personnel des entreprises qui développent à titre principal des activités d'intérêt social dans les domaines culturels, éducatifs, de loisirs et de plein air, ainsi qu'aux salariés des entreprises qui développent à titre principal des activités d'intérêt général de protection de la nature et de l'environnement, ces dispositions doivent être combinées avec les articles 2 et 3 du même décret, qui définissent les seuls emplois pour lesquels s'appliquent les durées d'équivalence ; qu'il ressort des pièces du dossier que ces emplois comportent des périodes d'inaction ; qu'ainsi, le moyen tiré de la méconnaissance de l'article L. 212-4 du code du travail ne peut qu'être écarté ; <br/>
<br/>
              Considérant, en quatrième lieu, que le décret attaqué a été pris, en application des dispositions précitées de l'article L. 212-4 du code du travail, « après conclusion d'une convention ou d'un accord de branche » ; que, dès lors, il appartient au juge administratif, lorsqu'un moyen en ce sens est présenté à l'appui de conclusions tendant à l'annulation d'un tel décret, de vérifier que la conclusion de l'accord au vu duquel le décret a été pris n'est pas entachée d'une irrégularité telle qu'elle mettrait en cause l'existence même de l'acte conventionnel ; qu'en revanche, le syndicat requérant ne peut utilement invoquer, à l'appui de sa demande d'annulation du décret, l'illégalité de celles des stipulations de l'accord qui n'ont pas été reprises par le décret, faute d'avoir pour objet l'institution d'un régime d'équivalence ; que la légalité du décret n'est pas davantage subordonnée à celle de l'arrêté étendant l'annexe 2 de la convention collective nationale de l'animation et ses avenants n° 58 et n° 77, dès lors, en tout état de cause, que l'article L. 212-4 du code du travail n'impose pas que le décret créant un régime d'équivalence soit pris au vu d'une convention ou d'un accord de branche étendu ; que, par suite, le moyen tiré du caractère illégal de l'extension de l'annexe 2 de la convention collective nationale de l'animation et de celles de ses avenants n° 58 et n° 77 ne peut qu'être écarté ;<br/>
<br/>
              Considérant, enfin, qu'il résulte des dispositions de l'article L. 212-4 du code du travail selon lesquelles un décret simple peut intervenir après signature d'une convention ou d'un accord de branche pour instituer un régime d'équivalence, que le champ d'application du décret ne saurait excéder celui de cette convention ; qu'il ressort des pièces du dossier que le champ d'application du décret litigieux n'excède pas celui défini à l'article 1er de la convention collective nationale de l'animation du 28 juin 1988 ; qu'ainsi, le moyen tiré de ce que le décret excéderait le champ des activités visées et des publics accompagnés visés par la convention collective ne peut qu'être écarté ;<br/>
<br/>
              Considérant qu'il résulte de tout ce qui précède que l'UNION SYNDICALE SOLIDAIRES ISERE n'est pas fondée à demander l'annulation du décret du 2 août 2005 ; que, par suite, les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que l'Etat, qui n'est pas dans la présente instance la partie perdante, soit condamné à verser à la requérante la somme qu'elle demande au titre des frais exposés par elle et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de l'UNION SYNDICALE SOLIDAIRES ISERE est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à l'UNION SYNDICALE SOLIDAIRES ISERE, au Premier ministre et au ministre de l'emploi, de la cohésion sociale et du logement.<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">66-03 TRAVAIL ET EMPLOI. CONDITIONS DE TRAVAIL. - DURÉE DU TRAVAIL - DURÉE ÉQUIVALENTE À LA DURÉE LÉGALE - NOTION - MODE DE COMPTABILISATION.
</SCT>
<ANA ID="9A"> 66-03 Les dispositions combinées des articles L. 212-1 et L. 212-4 du code du travail, pas davantage qu'aucune autre disposition législative ou réglementaire n'imposent que le mode de comptabilisation servant à définir la durée équivalente à la durée légale soit défini sur une base hebdomadaire, dès lors que le mode adopté permet de déterminer le nombre d'heures de travail effectif sur une semaine dans le cadre du régime d'équivalence. Par suite, légalité d'un décret instituant une durée du travail équivalente à la durée légale avec un rapport d'équivalence défini à partir de la durée de présence journalière mais permettant de déterminer la durée effective de travail pendant une semaine.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
