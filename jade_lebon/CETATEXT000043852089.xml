<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043852089</ID>
<ANCIEN_ID>JG_L_2021_07_000000441463</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/85/20/CETATEXT000043852089.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 4ème chambres réunies, 22/07/2021, 441463</TITRE>
<DATE_DEC>2021-07-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>441463</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>M. Eric Buge</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie Sirinelli</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:441463.20210722</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, un mémoire en réplique et un nouveau mémoire, enregistrés le 26 juin 2020 et les 18 mars et 29 avril 2021 au secrétariat du contentieux du Conseil d'Etat, la société par actions simplifiée UPSA demande au Conseil d'État :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du Comité économique des produits de santé du 3 juin 2020 fixant le prix de la spécialité Paracétamol/codéine Mylan 500 mg/30 mg, comprimés (B/16) ;<br/>
<br/>
              2°) de mettre à la charge du Comité économique des produits de santé ou, à défaut, de l'État la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la santé publique ;<br/>
              - le code de la sécurité sociale ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Vu les notes en délibéré, enregistrées les 8 et 9 juillet 2021, présentées par la société UPSA ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Eric Buge, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Marie Sirinelli, rapporteure publique ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Foussard, Froger, avocat de la société Mylan ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier que, par une décision du 30 juillet 2019, le directeur général de l'Agence nationale de sécurité du médicament et des produits de santé a modifié l'annexe I du répertoire des groupes génériques afin de créer un groupe générique dont la spécialité de référence est le Dafalgan codéine, comprimé pelliculé, du laboratoire UPSA et dont le générique est la spécialité paracétamol / codéine 500 mg/30 mg, comprimés, du laboratoire Mylan, commercialisée sous le nom de A.... Par une décision publiée le 3 juin 2020, le Comité économique des produits de santé a fixé, par convention avec le laboratoire Mylan, le prix de vente au public de cette dernière spécialité, à 0,98 euro pour ce qui est du prix fabricant hors taxe et à 1,44 euro pour ce qui est du prix public toutes taxes comprises, correspondant à une décote de 22,2 % par rapport à la spécialité de référence. La société UPSA demande l'annulation pour excès de pouvoir de la décision du Comité économique des produits de santé.<br/>
<br/>
              2. En premier lieu, d'une part, aux termes du premier alinéa de l'article L. 162-16-4 du code de la sécurité sociale : " Le prix de vente au public de chacun des médicaments mentionnés au premier alinéa de l'article L. 162-17 est fixé par convention entre l'entreprise exploitant le médicament, l'entreprise assurant l'importation parallèle du médicament ou l'entreprise assurant la distribution parallèle du médicament et le Comité économique des produits de santé conformément à l'article L. 162-17-4 ou, à défaut, par décision du comité, sauf opposition conjointe des ministres concernés qui arrêtent dans ce cas le prix dans un délai de quinze jours après la décision du comité. La fixation de ce prix tient compte principalement de l'amélioration du service médical rendu par le médicament, le cas échéant des résultats de l'évaluation médico-économique, des prix des médicaments à même visée thérapeutique, des volumes de vente prévus ou constatés ainsi que des conditions prévisibles et réelles d'utilisation du médicament (...) ".<br/>
<br/>
              3. D'autre part, l'article L. 162-17-4 du même code dispose que : " En application des orientations qu'il reçoit annuellement des ministres compétents, le Comité économique des produits de santé peut conclure avec des entreprises ou groupes d'entreprises des conventions d'une durée maximum de quatre années relatives à un ou à des médicaments visés (...) aux premier et deuxième alinéas de l'article L. 162-17. (...) Ces conventions, dont le cadre peut être précisé par un accord conclu avec un ou plusieurs syndicats représentatifs des entreprises concernées, déterminent les relations entre le comité et chaque entreprise (...) ". Le a) de l'article 19 de l'accord cadre du 31 décembre 2015 conclu en application de ces dispositions entre le Comité économique des produits de santé et les entreprises du médicament et reconduit en dernier lieu, à la date de la décision attaquée, par un avenant du 18 décembre 2019, prévoit que : " Dans le respect des orientations ministérielles, les règles de tarification des médicaments inscrits au répertoire des médicaments génériques sont reprises en annexe du présent accord-cadre. / Ces règles peuvent être revues pour certaines spécialités pour lesquelles les conditions susmentionnées conduiraient à une impossibilité de développement de l'offre générique. Les laboratoires sollicitant cette dérogation apportent des éléments de justification de cette demande. " Aux termes de l'annexe 2 du même accord cadre : " A la commercialisation d'un médicament générique le prix PFHT [prix fabricant hors taxe] de ce dernier est fixé conventionnellement avec une décote par rapport au prix PFHT de la spécialité de référence (avant la décote mentionnée ci-après) ; le prix PFHT de la spécialité de référence fait l'objet d'une décote par rapport à son PFHT précédent. [Hors] conditions spécifiques mentionnées à l'article 19 a du présent accord cadre, ces taux sont respectivement fixés à 60 % et 20 % ".<br/>
<br/>
              4. Enfin, le Comité économique des produits de santé s'est donné pour lignes directrices de fixer le prix d'une première spécialité générique à un prix correspondant à une décote de 60 % par rapport au prix du médicament princeps, sauf circonstances particulières justifiant une moindre décote, tenant en particulier à l'absence de commercialisation d'un médicament générique, à l'importance des coûts de fabrication de la spécialité, à son faible niveau de prix lié à son ancienneté ou à la faible taille du marché. Il lui revient toutefois, le cas échéant, lors de l'examen de chaque situation particulière, d'y déroger si des considérations d'intérêt général ou les circonstances propres à cette situation le justifient. Il ne saurait, dans une telle hypothèse, contrairement à ce que soutient la société requérante, être regardé comme entachant, ce faisant, sa décision d'" incompétence négative ".<br/>
<br/>
              5. En l'espèce, d'une part, il ressort des pièces du dossier que la spécialité d'UPSA est l'un des médicaments les plus vendus en France, 11 millions d'unités ayant été vendues en 2019, pour un chiffre d'affaires supérieur à 17 millions d'euros, cette spécialité étant prise en charge par l'assurance maladie à hauteur de 65 %, et qu'elle est commercialisée depuis 1991 sans qu'aucun médicament générique n'ait pu être identifié. Il existe dès lors un motif d'intérêt général tenant à ce que se développe une offre générique pour cette spécialité, de nature à permettre des économies pour l'assurance maladie. D'autre part, il est constant que la marge permise par le prix de vente de cette spécialité est limitée compte tenu de son coût de fabrication contraint. L'application d'une décote de 60 % par rapport au prix de la spécialité de référence aurait conduit à fixer le prix fabricant hors taxe de la spécialité générique en cause, composée de 500 mg de paracétamol et de 30 mg de codéine, à 0,50 euro, soit un prix inférieur à celui, proche de leur coût de revient, fixé pour les spécialités composées uniquement de 500 mg de paracétamol, qui est de 0,76 euro. Ces circonstances permettaient au Comité économique des produits de santé de déroger, en l'espèce, au niveau de la décote résultant en principe de l'application de ses lignes directrices lors de la commercialisation d'un médicament générique, tant à l'égard de la spécialité générique qu'au demeurant, le cas échéant, à l'égard de la spécialité de référence.<br/>
<br/>
              6. Il résulte de ce qui précède que la société UPSA n'est pas fondée à soutenir que le Comité économique des produits de santé, qui s'est fondé sur des considérations d'intérêt général et a pris en compte les particularités du marché pour fixer le prix du médicament générique de Mylan, sans d'ailleurs appliquer dans le même temps de décote à la spécialité d'UPSA, et auquel aucune disposition n'imposait de faire état dans sa décision des justifications apportées par le laboratoire Mylan sur lesquelles il s'appuyait pour la prendre, aurait commis une erreur manifeste d'appréciation en fixant conventionnellement le prix de la spécialité générique de Mylan au niveau retenu.<br/>
<br/>
              7. En deuxième lieu, il appartient au Comité économique des produits de santé, quand il fixe par convention ou par décision unilatérale le prix d'un médicament en application de l'article L. 162-16-4 du code de la sécurité sociale précité, de concilier l'objectif de développement de la vente des génériques en vue de réduire le montant des dépenses d'assurance maladie avec l'obligation qui lui incombe, compte tenu du caractère partiellement administré de ce secteur économique qui reste soumis aux lois du marché et à la concurrence entre les entreprises qui exploitent les médicaments, de ne pas adopter de décision dont les effets économiques porteraient au principe d'égalité une atteinte qui excéderait ce qui est nécessaire à l'objectif poursuivi de maîtrise des dépenses publiques de santé et, de ce fait, porterait également atteinte au libre jeu de la concurrence. En l'espèce, pour les raisons mentionnées au point 5, il ne ressort en tout état de cause pas des pièces du dossier que le prix fixé par le Comité économique des produits de santé aurait porté aux principes d'égalité et de libre concurrence une atteinte excessive au regard de l'objectif poursuivi ou serait constitutif d'un avantage pour le laboratoire Mylan pouvant à ce titre être regardé comme une aide d'Etat.<br/>
<br/>
              8. En troisième lieu, si, aux termes des orientations adressées le 4 février 2019 par les ministres compétents au président du Comité économique des produits de santé sur le fondement de l'article L. 162-17-3 du code de la sécurité sociale, ce comité doit notamment rechercher " la maîtrise et l'efficience des dépenses de produits de santé ", la société requérante ne saurait utilement soutenir que la décision attaquée, qui permet la commercialisation d'un médicament générique, serait, en ce qu'elle retient un prix supérieur à celui qui aurait résulté de l'application de la décote de 60 % prévue en principe par les lignes directrices, contraire à l'objectif de maîtrise des dépenses de santé assigné au Comité économique des produits de santé par l'article L. 162-17-3 du code de la sécurité sociale.<br/>
<br/>
              9. Il résulte de tout ce qui précède, sans que puisse avoir aucune incidence sur la légalité de la décision attaquée la circonstance alléguée, à la supposer établie, que le président du Comité économique des produits de santé n'aurait pas eu qualité pour signer le mémoire en défense présenté devant le Conseil d'Etat, que la société UPSA n'est pas fondée à demander l'annulation de la décision qu'elle attaque.<br/>
<br/>
              10. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société UPSA une somme de 3 000 euros à verser à la société Mylan au titre de l'article L. 761-1 du code de justice administrative. Les mêmes dispositions font obstacle à ce qu'une somme soit mise à la charge de la société Mylan, qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
            Article 1er : La requête de la société UPSA est rejetée.<br/>
<br/>
Article 2 : La société UPSA versera à la société Mylan une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée à la société par actions simplifiées UPSA, au Comité économique des produits de santé et à la société Mylan.<br/>
            Copie en sera adressée au ministre des solidarités et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-01-05-03-03 ACTES LÉGISLATIFS ET ADMINISTRATIFS. DIFFÉRENTES CATÉGORIES D'ACTES. ACTES ADMINISTRATIFS - NOTION. INSTRUCTIONS ET CIRCULAIRES. DIRECTIVES ADMINISTRATIVES. - CEPS AYANT DÉROGÉ À SES LIGNES DIRECTRICES POUR LA FIXATION DU PRIX D'UNE PREMIÈRE SPÉCIALITÉ GÉNÉRIQUE - LÉGALITÉ - 1) MOTIFS JUSTIFIANT CETTE DÉROGATION [RJ1] - EXISTENCE - 2) EMA - ABSENCE, EN L'ESPÈCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">61-04-01-002 SANTÉ PUBLIQUE. PHARMACIE. PRODUITS PHARMACEUTIQUES. - CEPS AYANT DÉROGÉ À SES LIGNES DIRECTRICES POUR LA FIXATION DU PRIX D'UNE PREMIÈRE SPÉCIALITÉ GÉNÉRIQUE - LÉGALITÉ - 1) MOTIFS JUSTIFIANT CETTE DÉROGATION [RJ1] - EXISTENCE - 2) EMA - ABSENCE, EN L'ESPÈCE.
</SCT>
<ANA ID="9A"> 01-01-05-03-03 Comité économique des produits de santé (CEPS) s'étant donné pour lignes directrices de fixer le prix d'une première spécialité générique à un prix correspondant à une décote de 60 % par rapport au prix du médicament princeps, sauf circonstances particulières justifiant une moindre décote, tenant en particulier à l'absence de commercialisation d'un médicament générique, à l'importance des coûts de fabrication de la spécialité, à son faible niveau de prix lié à son ancienneté ou à la faible taille du marché. Il lui revient toutefois, le cas échéant, lors de l'examen de chaque situation particulière, d'y déroger si des considérations d'intérêt général ou les circonstances propres à cette situation le justifient.,,,Décision du CEPS fixant le prix de vente au public d'une spécialité à un prix correspondant à une décote de 22,2 % par rapport à la spécialité de référence.... ,,1) D'une part, cette spécialité constitue l'un des médicaments les plus vendus en France, est prise en charge par l'assurance maladie à hauteur de 65 %, et est commercialisée depuis 1991 sans qu'aucun médicament générique n'ait pu être identifié.... ,,Il existe dès lors un motif d'intérêt général tenant à ce que se développe une offre générique pour cette spécialité, de nature à permettre des économies pour l'assurance maladie.... ,,D'autre part, la marge permise par le prix de vente de cette spécialité est limitée compte tenu de son coût de fabrication contraint. L'application d'une décote de 60 % par rapport au prix de la spécialité de référence aurait conduit à fixer le prix fabricant hors taxe de la spécialité générique en cause, composée de 500 mg de paracétamol et de 30 mg de codéine, à 0,50 euro, soit un prix inférieur à celui, proche de leur coût de revient, fixé pour les spécialités composées uniquement de 500 mg de paracétamol, qui est de 0,76 euro.... ,,Ces circonstances permettaient au CEPS de déroger, en l'espèce, au niveau de la décote résultant en principe de l'application de ses lignes directrices lors de la commercialisation d'un médicament générique, tant à l'égard de la spécialité générique qu'au demeurant, le cas échéant, à l'égard de la spécialité de référence.,,,2) Le CEPS, qui s'est fondé sur des considérations d'intérêt général et a pris en compte les particularités du marché pour fixer le prix du médicament générique, n'a pas commis une erreur manifeste d'appréciation en fixant conventionnellement le prix de cette spécialité générique au niveau retenu.</ANA>
<ANA ID="9B"> 61-04-01-002 Comité économique des produits de santé (CEPS) s'étant donné pour lignes directrices de fixer le prix d'une première spécialité générique à un prix correspondant à une décote de 60 % par rapport au prix du médicament princeps, sauf circonstances particulières justifiant une moindre décote, tenant en particulier à l'absence de commercialisation d'un médicament générique, à l'importance des coûts de fabrication de la spécialité, à son faible niveau de prix lié à son ancienneté ou à la faible taille du marché. Il lui revient toutefois, le cas échéant, lors de l'examen de chaque situation particulière, d'y déroger si des considérations d'intérêt général ou les circonstances propres à cette situation le justifient.,,,Décision du CEPS fixant le prix de vente au public d'une spécialité à un prix correspondant à une décote de 22,2 % par rapport à la spécialité de référence.... ,,1) D'une part, cette spécialité constitue l'un des médicaments les plus vendus en France, est prise en charge par l'assurance maladie à hauteur de 65 %, et est commercialisée depuis 1991 sans qu'aucun médicament générique n'ait pu être identifié.... ,,Il existe dès lors un motif d'intérêt général tenant à ce que se développe une offre générique pour cette spécialité, de nature à permettre des économies pour l'assurance maladie.... ,,D'autre part, la marge permise par le prix de vente de cette spécialité est limitée compte tenu de son coût de fabrication contraint. L'application d'une décote de 60 % par rapport au prix de la spécialité de référence aurait conduit à fixer le prix fabricant hors taxe de la spécialité générique en cause, composée de 500 mg de paracétamol et de 30 mg de codéine, à 0,50 euro, soit un prix inférieur à celui, proche de leur coût de revient, fixé pour les spécialités composées uniquement de 500 mg de paracétamol, qui est de 0,76 euro.... ,,Ces circonstances permettaient au CEPS de déroger, en l'espèce, au niveau de la décote résultant en principe de l'application de ses lignes directrices lors de la commercialisation d'un médicament générique, tant à l'égard de la spécialité générique qu'au demeurant, le cas échéant, à l'égard de la spécialité de référence.,,,2) Le CEPS, qui s'est fondé sur des considérations d'intérêt général et a pris en compte les particularités du marché pour fixer le prix du médicament générique, n'a pas commis une erreur manifeste d'appréciation en fixant conventionnellement le prix de cette spécialité générique au niveau retenu.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., sur les conditions d'une telle dérogation, CE, Section, 11 décembre 1970, Crédit foncier de France, n° 78880, p. 750 ; CE, Section, 4 février 2015, Ministre de l'intérieur c/ M. Cortes Ortiz, n°s 383267 383268, p. 17.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
