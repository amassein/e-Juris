<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037254030</ID>
<ANCIEN_ID>JG_L_2018_07_000000417441</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/25/40/CETATEXT000037254030.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 26/07/2018, 417441, Publié au recueil Lebon</TITRE>
<DATE_DEC>2018-07-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>417441</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Clément Malverti</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CECHR:2018:417441.20180726</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par un arrêt n° 17VE01375 du 16 janvier 2018, enregistré le 22 janvier 2018 au secrétariat du contentieux du Conseil d'Etat, la cour administrative d'appel de Versailles, avant de statuer sur la demande de Mme B...A...tendant à l'annulation du jugement n° 1701461 du 13 mars 2017 par lequel le magistrat désigné par le président du tribunal administratif de Versailles a rejeté sa demande tendant à l'annulation des arrêtés du 27 février 2017 par lesquels le préfet de l'Essonne a ordonné son transfert aux autorités italiennes et son assignation à résidence pour une durée de 45 jours, a décidé, par application des dispositions de l'article L. 113-1 du code de justice administrative, de transmettre le dossier de cette demande au Conseil d'Etat, en soumettant à son examen les questions suivantes : <br/>
<br/>
              1°) Les dispositions des articles 29 du règlement (UE) n° 604/2013 du 26 juin 2013 et L. 561-2 du code de l'entrée et du séjour des étrangers et du droit d'asile font-elles obstacle à ce que l'assignation à résidence d'un étranger faisant l'objet d'un arrêté de transfert, pour lequel la requête aux fins de prise en charge ou de reprise en charge a été acceptée, implicitement ou expressément, par un Etat membre, puisse être légalement décidée, en l'absence de fuite ou d'emprisonnement de l'intéressé à la date à laquelle est prise cette décision, pour une durée ne dépassant pas la durée maximale de quarante cinq jours prévue par la loi mais s'étendant au-delà du délai de six mois à l'échéance duquel l'État membre requis est libéré de son obligation de prise en charge du demandeur d'asile et la responsabilité de cette prise en charge est transférée à l'État membre requérant '<br/>
<br/>
              2°) Ou bien l'application de ces dispositions a-t-elle seulement pour effet d'entraîner la caducité de l'assignation à résidence à compter de la date à laquelle, le délai de six mois étant expiré, la décision de transfert n'est plus susceptible d'exécution '<br/>
<br/>
              3°) En cas de réponse positive à la première question et négative à la seconde, l'illégalité ainsi constituée doit-elle entraîner l'annulation totale de la mesure d'assignation à résidence ou l'annulation partielle de cet acte en tant seulement qu'il porte effet au-delà du délai de six mois durant lequel l'éloignement de l'étranger demeure une perspective raisonnable '<br/>
<br/>
     .........................................................................<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le règlement (UE) n° 604/2013 du 26 juin 2013 ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le code des relations entre le public et l'administration ; <br/>
              - le code de justice administrative, notamment son article L. 113-1 ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Clément Malverti, maître des requêtes,<br/>
<br/>
              - les conclusions de M. Guillaume Odinet, rapporteur public,<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              REND L'AVIS SUIVANT<br/>
<br/>
<br/>
<br/>
              1. Les articles 20 et suivants du règlement (UE) n° 604/2013 du Parlement européen et du Conseil du 26 juin 2013 établissant les critères et mécanismes de détermination de l'État membre responsable de l'examen d'une demande de protection internationale introduite dans l'un des États membres par un ressortissant de pays tiers ou un apatride fixent les règles selon lesquelles sont organisées les procédures de prise en charge ou de reprise en charge d'un demandeur d'asile par l'Etat membre responsable de l'examen de sa demande d'asile. Ces articles déterminent notamment les conditions dans lesquelles l'Etat sur le territoire duquel se trouve le demandeur d'asile requiert de l'Etat qu'il estime responsable de l'examen de la demande la prise ou la reprise en charge du demandeur d'asile, ainsi que les conditions dans lesquelles, en cas d'acceptation par l'Etat requis, le demandeur d'asile est transféré vers cet Etat. <br/>
<br/>
              2. En vertu de l'article 26 du règlement (UE) n° 604/2013, lorsque l'Etat membre requis accepte la prise en charge ou la reprise en charge d'un demandeur d'asile, l'Etat sur le territoire duquel se situe ce dernier lui notifie la décision de le transférer vers l'Etat membre responsable. <br/>
<br/>
              En vertu des dispositions combinées du premier alinéa de l'article L. 742-5 et du I de l'article L. 561-2 du code de l'entrée et du séjour des étrangers et du droit d'asile, dans leur rédaction applicable au litige à l'origine des questions posées par la cour administrative d'appel de Versailles, l'autorité administrative peut prendre une décision d'assignation à résidence à l'égard de l'étranger faisant l'objet d'une décision de transfert dès la notification de cette décision, pour une durée de quarante-cinq jours, renouvelable une fois. L'assignation à résidence trouve dans ce cas son fondement légal dans la décision de transfert pour l'exécution de laquelle elle est prise. <br/>
<br/>
              3. Le premier paragraphe de l'article 29 du règlement (UE) n° 604/2013 prévoit que le transfert du demandeur d'asile de l'Etat membre requérant vers l'État membre responsable s'effectue " dès qu'il est matériellement possible et, au plus tard, dans un délai de six mois à compter de l'acceptation par un autre État membre de la requête aux fins de prise en charge ou de reprise en charge de la personne concernée ou de la décision définitive sur le recours ou la révision lorsque l'effet suspensif est accordé conformément à l'article 27, paragraphe 3 ". <br/>
<br/>
              Le paragraphe 3 de l'article 27 du règlement (UE) n° 604/2013 prévoit que : " Aux fins des recours contre des décisions de transfert ou des demandes de révision de ces décisions, les États membres prévoient les dispositions suivantes dans leur droit national : / a) le recours ou la révision confère à la personne concernée le droit de rester dans l'État membre concerné en attendant l'issue de son recours ou de sa demande de révision (...) ". <br/>
<br/>
              L'article L. 742-5 du code de l'entrée et du séjour des étrangers et du droit d'asile, dans sa rédaction résultant de la loi du 29 juillet 2015 relative à la réforme du droit d'asile, prévoit, en application de ces dispositions, que : " la décision de transfert ne peut faire l'objet d'une exécution d'office ni avant l'expiration d'un délai de quinze jours ou, si une décision de placement en rétention prise en application de l'article L. 551-1 ou d'assignation à résidence prise en application de l'article L. 561-2 a été notifiée avec la décision de transfert ou si celle-ci a été notifiée alors que l'étranger fait déjà l'objet d'une telle décision de placement en rétention ou d'assignation à résidence, avant l'expiration d'un délai de quarante-huit heures, ni avant que le tribunal administratif ait statué, s'il a été saisi ". <br/>
<br/>
              Il résulte de la combinaison de ces dispositions que l'introduction d'un recours contre la décision de transfert a pour effet d'interrompre le délai de six mois fixé à l'article 29 du règlement (UE) n° 604/2013 qui courait à compter de l'acceptation du transfert par l'Etat requis, délai qui recommence à courir intégralement à compter de la date à laquelle le tribunal administratif statue sur cette demande.<br/>
<br/>
              4. Le paragraphe 2 de l'article 29 du règlement (UE) n° 614/2013 dispose que : " si le transfert n'est pas exécuté dans le délai de six mois, l'État membre responsable est libéré de son obligation de prendre en charge ou de reprendre en charge la personne concernée et la responsabilité est alors transférée à l'État membre requérant. Ce délai peut être porté à un an au maximum s'il n'a pas pu être procédé au transfert en raison d'un emprisonnement de la personne concernée ou à dix-huit mois au maximum si la personne concernée prend la fuite ".<br/>
<br/>
              Il résulte de ces dispositions qu'à l'expiration du délai d'exécution du transfert, la décision de transfert notifiée au demandeur d'asile ne peut plus être légalement exécutée. Il en va de même, par voie de conséquence, de la décision d'assignation à résidence dont elle est le fondement légal.<br/>
<br/>
              5. Dès lors, une assignation à résidence ordonnée sur le fondement d'une décision de transfert dont la durée, à la date où elle est édictée, excède le terme du délai dans lequel le transfert du demandeur d'asile doit intervenir en vertu de l'article 29 du règlement (UE) n° 604/2013, est illégale en tant que sa durée s'étend au-delà de l'échéance de ce délai et le juge, dès lors qu'il est saisi d'une argumentation en ce sens, est tenu d'en prononcer l'annulation dans cette mesure.<br/>
<br/>
              Toutefois, lorsque le délai d'exécution du transfert a, postérieurement à l'édiction de l'assignation à résidence, été interrompu, il appartient au juge de constater, le cas échéant, que cette interruption a eu pour effet de régulariser la décision d'assignation à résidence en tant qu'elle avait été prise pour une durée excessive ; dans une telle hypothèse, il ne prononce donc pas l'annulation partielle de l'assignation à résidence. <br/>
<br/>
<br/>
<br/>
<br/>
              Le présent avis sera notifié à la cour administrative d'appel de Versailles, à Mme B...A...et au ministre d'Etat, ministre de l'intérieur. Il sera publié au Journal officiel de la République française.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">095-02-03 - ASSIGNATION À RÉSIDENCE ORDONNÉE SUR LE FONDEMENT D'UNE DÉCISION DE TRANSFERT (1ER AL. DE L'ART. L. 742-5 ET I DE L'ART. L. 561-2 DU CESEDA) - PRINCIPE - DURÉE EXCÉDANT, À SA DATE D'ÉDICTION, LE DÉLAI AU-DELÀ DUQUEL LE TRANSFERT DU DEMANDEUR D'ASILE DOIT INTERVENIR - DÉCISION D'ASSIGNATION ILLÉGALE EN TANT QUE SA DURÉE S'ÉTEND AU-DELÀ DE CE DÉLAI - EXCEPTION - INTERRUPTION DU DÉLAI D'EXÉCUTION DU TRANSFERT POSTÉRIEUREMENT À LA DÉCISION D'ASSIGNATION - RÉGULARISATION, LE CAS ÉCHÉANT, DE CETTE DÉCISION EN TANT QU'ELLE AVAIT ÉTÉ PRISE POUR UNE DURÉE EXCESSIVE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">335-01-04-01 ÉTRANGERS. SÉJOUR DES ÉTRANGERS. RESTRICTIONS APPORTÉES AU SÉJOUR. ASSIGNATION À RÉSIDENCE. - ASSIGNATION À RÉSIDENCE ORDONNÉE SUR LE FONDEMENT D'UNE DÉCISION DE TRANSFERT (1ER AL. DE L'ART. L. 742-5 ET I DE L'ART. L. 561-2 DU CESEDA) - PRINCIPE - DURÉE EXCÉDANT, À SA DATE D'ÉDICTION, LE DÉLAI AU DELÀ DUQUEL LE TRANSFERT DU DEMANDEUR D'ASILE DOIT INTERVENIR - DÉCISION D'ASSIGNATION ILLÉGALE EN TANT QUE SA DURÉE S'ÉTEND AU-DELÀ DE CE DÉLAI - EXCEPTION - INTERRUPTION DU DÉLAI D'EXÉCUTION DU TRANSFERT POSTÉRIEUREMENT À LA DÉCISION D'ASSIGNATION - RÉGULARISATION, LE CAS ÉCHÉANT, DE CETTE DÉCISION EN TANT QU'ELLE AVAIT ÉTÉ PRISE POUR UNE DURÉE EXCESSIVE.
</SCT>
<ANA ID="9A"> 095-02-03 Une assignation à résidence ordonnée sur le fondement d'une décision de transfert dont la durée, à la date où elle est édictée, excède le terme du délai dans lequel le transfert du demandeur d'asile doit intervenir en vertu de l'article 29 du règlement (UE) n° 604/2013, est illégale en tant que sa durée s'étend au-delà de l'échéance de ce délai et le juge, dès lors qu'il est saisi d'une argumentation en ce sens, est tenu d'un prononcer l'annulation dans cette mesure.,,,Toutefois, lorsque le délai d'exécution du transfert a, postérieurement à l'édiction de l'assignation à résidence, été interrompu, il appartient au juge de constater, le cas échéant, que cette interruption a eu pour effet de régulariser la décision d'assignation à résidence en tant qu'elle avait été prise pour une durée excessive ; dans une telle hypothèse, il ne prononce donc pas l'annulation partielle de la décision d'assignation à résidence.</ANA>
<ANA ID="9B"> 335-01-04-01 Une assignation à résidence ordonnée sur le fondement d'une décision de transfert dont la durée, à la date où elle est édictée, excède le terme du délai dans lequel le transfert du demandeur d'asile doit intervenir en vertu de l'article 29 du règlement (UE) n° 604/2013, est illégale en tant que sa durée s'étend au-delà de l'échéance de ce délai et le juge, dès lors qu'il est saisi d'une argumentation en ce sens, est tenu d'un prononcer l'annulation dans cette mesure.,,,Toutefois, lorsque le délai d'exécution du transfert a, postérieurement à l'édiction de l'assignation à résidence, été interrompu, il appartient au juge de constater, le cas échéant, que cette interruption a eu pour effet de régulariser la décision d'assignation à résidence en tant qu'elle avait été prise pour une durée excessive ; dans une telle hypothèse, il ne prononce donc pas l'annulation partielle de la décision d'assignation à résidence.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
