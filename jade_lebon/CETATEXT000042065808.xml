<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042065808</ID>
<ANCIEN_ID>JG_L_2020_06_000000434671</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/06/58/CETATEXT000042065808.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 19/06/2020, 434671</TITRE>
<DATE_DEC>2020-06-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>434671</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELAMARRE, JEHANNIN ; SCP MARLANGE, DE LA BURGADE ; SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR>Mme Myriam Benlolo Carabot</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:434671.20200619</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
      Mme C... E... a demandé au juge des référés du tribunal administratif de Lyon, sur le fondement de l'article L. 521-1 du code de justice administrative, d'une part, d'ordonner la suspension de l'exécution de la décision du 12 août 2019 par laquelle le maire de Saint-Didier-au-Mont-d'Or (Rhône) a refusé de constater la caducité du permis de construire du 5 septembre 2014, transféré à M. A... le 16 décembre 2016, de dresser un procès-verbal d'infraction aux fins de transmission au procureur de la République et de prendre un arrêté interruptif de travaux et, d'autre part, d'enjoindre au maire de constater, à titre provisoire, la caducité du permis de construire du 5 septembre 2014 et de prendre un arrêté interruptif de travaux jusqu'à ce qu'il soit statué sur sa demande d'annulation de la décision du 12 août 2019. <br/>
<br/>
      Par une ordonnance n° 1906562 du 11 septembre 2019, le juge des référés du tribunal administratif de Lyon a suspendu l'exécution de la décision du 12 août 2019 du maire de Saint-Didier-au-Mont-d'or et a enjoint au maire de constater la caducité du permis de construire du 5 septembre 2014 et de prendre un arrêté interruptif de travaux. <br/>
<br/>
      1° Sous le n° 434671, par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 18 septembre et 2 octobre 2019 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat : <br/>
      1°) d'annuler cette ordonnance ;<br/>
<br/>
      2°) statuant en référé, de rejeter les demandes de Mme E... ;<br/>
<br/>
      3°) de mettre à la charge de Mme E... la somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
      2° Sous le n° 434899, par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 26 septembre et 11 octobre 2019 et le 2 mars 2020 au secrétariat du contentieux du Conseil d'Etat, la commune de Saint-Didier-au-Mont-d'Or demande au Conseil d'Etat : <br/>
      1°) d'annuler cette ordonnance ;<br/>
<br/>
      2°) statuant en référé, de rejeter les demandes de Mme E... ;<br/>
<br/>
      3°) de mettre à la charge de Mme E... la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
        Vu les autres pièces des dossiers ; <br/>
<br/>
        Vu :<br/>
        - le code de l'urbanisme ;<br/>
        - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 modifiée ;<br/>
<br/>
<br/>
       Après avoir entendu en séance publique :<br/>
<br/>
       - le rapport de Mme Myriam Benlolo Carabot, maître des requêtes en service extraordinaire,  <br/>
<br/>
       - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
       La parole ayant été donnée, avant et après les conclusions, à la SCP Delamarre, Jéhannin, avocat de M. B... A..., à la SCP Marlange, de la Burgade, avocat de Mme E..., à la SCP Matuchansky, Poupot, Valdelièvre, avocat de la commune de Saint-Didier au Mont-d'or ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Les pourvois de M. A... et de la commune de Saint-Didier-au-Mont-d'Or sont dirigés contre la même ordonnance. Il y a lieu de les joindre pour statuer par une seule décision. <br/>
<br/>
              2. Il ressort des pièces du dossier soumis au juge des référés qu'un permis de construire portant sur la réalisation d'une maison individuelle sur une parcelle située 16, chemin du David sur le territoire de la commune de Saint-Didier-au-Mont-d'Or a été délivré le 5 septembre 2014 à M. D... puis transféré à M. A... par un arrêté du 16 décembre 2016. Mme E..., propriétaire d'une parcelle voisine, a formé un recours contre ce permis de construire, le 2 mars 2015, qui a été rejeté par un jugement du tribunal administratif de Lyon en date du 12 mai 2016, devenu irrévocable. Le 5 août 2017, le maire de Saint-Didier-au-Mont-d'Or a délivré à M. A... un permis de construire modificatif. Sur la requête de Mme E..., le tribunal administratif de Lyon, par un jugement du 27 décembre 2018, devenu définitif, a annulé ce permis modificatif. Le 13 février 2019, M. A... a alors déposé une déclaration d'ouverture de chantier afin de réaliser le projet tel qu'autorisé par le permis de construire initial délivré le 5 septembre 2014 et a commencé les travaux. Par un courrier du 12 juin 2019, Mme E... a demandé au maire de Saint-Didier-au-Mont-d'Or de constater la caducité de ce permis de construire, de dresser un procès-verbal d'infraction aux fins de transmission au procureur de la République et de prendre un arrêté interruptif de travaux. Par une décision du 12 août 2019, la commune a rejeté ces demandes. Mme E... a demandé au juge des référés du tribunal administratif de Lyon, sur le fondement de l'article L. 521-1 du code de justice administrative, d'une part, d'ordonner la suspension de l'exécution de cette décision et, d'autre part, d'enjoindre au maire de Saint-Didier-au-Mont-d'Or de constater, à titre provisoire, la caducité du permis de construire du 5 septembre 2014 et de prendre un arrêté interruptif de travaux. M. A... et la commune de Saint-Didier-au-Mont-d'Or se pourvoient en cassation contre l'ordonnance du 11 septembre 2019 par laquelle le juge des référés du tribunal administratif de Lyon a fait droit aux demandes de Mme E....<br/>
<br/>
              Sur le désistement partiel de la commune de Saint-Didier-au-Mont-d'Or des conclusions de son pourvoi :<br/>
<br/>
              4. Par un mémoire enregistré le 30 décembre 2019, la commune de Saint-Didier-au-Mont-d'Or a répondu au moyen relevé d'office par le Conseil d'Etat qui lui avait été communiqué en application des dispositions de l'article R. 611-7 du code de justice administrative en indiquant qu'elle ne contestait pas ne pas avoir qualité pour se pourvoir en cassation contre l'ordonnance du juge des référés du tribunal administratif de Lyon du 11 septembre 2019 en tant qu'elle a suspendu le refus de son maire de dresser un procès-verbal d'infraction aux fins de transmission au procureur de la République et de prendre un arrêté interruptif de travaux et a enjoint au maire de prendre un tel arrêté mais qu'elle a, en revanche, qualité pour former un pourvoi en cassation contre cette ordonnance en tant qu'elle a suspendu le refus de ce maire de constater la caducité du permis de construire du 5 septembre 2014 et lui a enjoint de constater la caducité de ce permis et qu'elle maintient les conclusions qu'elle a présentées à ce titre. La commune doit dès lors être regardée comme se désistant purement et simplement des conclusions de son pourvoi tendant à l'annulation de l'ordonnance attaquée en tant qu'elle a ordonné la suspension de l'exécution du refus de son maire de dresser un procès-verbal d'infraction aux fins de transmission au procureur de la République et de prendre un arrêté interruptif de travaux et en tant qu'elle a enjoint au maire de prendre un arrêté interruptif de travaux. Rien ne s'oppose à ce qu'il soit, dans cette mesure, donné acte de ce désistement.<br/>
<br/>
              Sur le pourvoi de M. A... et le surplus des conclusions du pourvoi de la commune de Saint-Didier-au-Mont-d'Or :<br/>
<br/>
              En ce qui concerne la fin de non-recevoir opposée au pourvoi de la commune de Saint-Didier-au-Mont-d'Or :<br/>
<br/>
              5. Lorsqu'il constate ou refuse de constater la péremption d'un permis de construire au regard des dispositions des articles R. 424-17 et R. 424-19 du code de l'urbanisme, le maire agit au nom de la commune. Par suite, la commune de Saint-Didier-au-Mont-d'Or a qualité pour se pourvoir en cassation contre l'ordonnance qu'elle attaque en tant qu'elle a suspendu le refus de son maire de constater la caducité du permis de construire du 5 septembre 2014 et lui a enjoint de constater la caducité de ce permis. Il s'ensuit que la fin de non-recevoir opposée par Mme E... doit, eu égard au désistement partiel de la commune de Saint-Didier-au-Mont-d'Or de ses conclusions dont il a été donné acte au point 4, être écartée. <br/>
<br/>
              En ce qui concerne le bien-fondé de l'ordonnance attaquée : <br/>
<br/>
              6. L'article R. 424-17 du code de l'urbanisme dispose que : " Le permis de construire, d'aménager ou de démolir est périmé si les travaux ne sont pas entrepris dans le délai de trois ans à compter de la notification mentionnée à l'article R. 424-10 ou de la date à laquelle la décision tacite est intervenue. / Il en est de même si, passé ce délai, les travaux sont interrompus pendant un délai supérieur à une année. / (...) ". Aux termes du premier alinéa de l'article R. 424-19 du même code : " En cas de recours devant la juridiction administrative contre le permis ou contre la décision de non-opposition à la déclaration préalable ou de recours devant la juridiction civile en application de l'article L. 480-13, le délai de validité prévu à l'article R. 424-17 est suspendu jusqu'au prononcé d'une décision juridictionnelle irrévocable ".<br/>
<br/>
              7. Il résulte de la combinaison des dispositions citées au point 6 que, si la délivrance d'un permis de construire modificatif n'a pas pour effet de faire courir à nouveau le délai de validité du permis de construire initial, le recours contentieux formé par un tiers à l'encontre de ce permis modificatif suspend ce délai jusqu'à l'intervention d'une décision juridictionnelle irrévocable. Dès lors, en jugeant que le moyen tiré de la méconnaissance par la décision attaquée des articles R. 424-17 et R. 424-19 du code de l'urbanisme était de nature à faire naître un doute sérieux quant à la légalité de la décision du maire de Saint-Didier-au-Mont-d'Or du 12 août 2019 refusant de constater la caducité du permis de construire initial du 5 septembre 2014 alors que le recours formé par Mme E... contre le permis modificatif délivré le 5 août 2017 à M. A... avait suspendu le délai de validité du permis de construire initial, le juge des référés du tribunal administratif de Lyon a, eu égard à son office, entaché son ordonnance d'erreur de droit. Par suite, et sans qu'il soit besoin de se prononcer sur l'autre moyen du pourvoi de la commune de Saint-Didier-au-Mont-d'Or, les requérants sont fondés à demander l'annulation de l'ordonnance qu'ils attaquent. <br/>
<br/>
              8. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de Mme E... le versement à M. A... d'une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. Il n'y a pas lieu dans les circonstances de l'espèce de faire droit aux conclusions de la commune de Saint-Didier-au-Mont-d'Or présentées au titre de l'article L. 761-1 du code de justice administrative. Ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de M. A... et de la commune de Saint-Didier-au-Mont-d'Or qui ne sont pas, dans la présente instance, les parties perdantes. <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Il est donné acte du désistement des conclusions du pourvoi de la commune de Saint-Didier-au-Mont-d'Or tendant à l'annulation de l'ordonnance du juge des référés du tribunal administratif de Lyon du 11 septembre 2019 en tant qu'elle a suspendu le refus du maire de cette commune de dresser un procès-verbal d'infraction aux fins de transmission au procureur de la République et de prendre un arrêté interruptif de travaux et en tant qu'elle a enjoint à ce maire de prendre un tel arrêté.<br/>
Article 2 : L'ordonnance du 11 septembre 2019 du juge des référés du tribunal administratif de Lyon est annulée.<br/>
<br/>
Article 3 : L'affaire est renvoyée au tribunal administratif de Lyon.<br/>
<br/>
Article 4 : Mme E... versera à M. A... une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 5 : Les conclusions de la commune de Saint-Didier-au-Mont-d'Or présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 6 : Les conclusions de Mme E... présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 7 : La présente décision sera notifiée à M. B... A..., à la commune de Saint-Didier-au-Mont-d'Or et à Mme C... E....<br/>
Copie en sera adressée à la ministre de la cohésion des territoires et des relations avec les collectivités territoriales.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">68-03-04-01 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PERMIS DE CONSTRUIRE. RÉGIME D'UTILISATION DU PERMIS. PÉREMPTION. - RECOURS FORMÉ PAR UN TIERS À L'ENCONTRE D'UN PERMIS DE CONSTRUIRE MODIFICATIF - EFFET SUSPENSIF SUR LE DÉLAI DE VALIDITÉ DU PERMIS DE CONSTRUIRE INITIAL (ART. R. 424-19 DU CODE DE L'URBANISME) - EXISTENCE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">68-06 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. - RECOURS FORMÉ PAR UN TIERS À L'ENCONTRE D'UN PERMIS DE CONSTRUIRE MODIFICATIF - EFFET SUSPENSIF SUR LE DÉLAI DE VALIDITÉ DU PERMIS DE CONSTRUIRE INITIAL (ART. R. 424-19 DU CODE DE L'URBANISME) - EXISTENCE [RJ1].
</SCT>
<ANA ID="9A"> 68-03-04-01 Il résulte de la combinaison des articles R. 424-17 et R. 424-19 du code de l'urbanisme que, si la délivrance d'un permis de construire modificatif n'a pas pour effet de faire courir à nouveau le délai de validité du permis de construire initial, le recours contentieux formé par un tiers à l'encontre de ce permis modificatif suspend ce délai jusqu'à l'intervention d'une décision juridictionnelle irrévocable.</ANA>
<ANA ID="9B"> 68-06 Il résulte de la combinaison des articles R. 424-17 et R. 424-19 du code de l'urbanisme que, si la délivrance d'un permis de construire modificatif n'a pas pour effet de faire courir à nouveau le délai de validité du permis de construire initial, le recours contentieux formé par un tiers à l'encontre de ce permis modificatif suspend ce délai jusqu'à l'intervention d'une décision juridictionnelle irrévocable.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Comp., s'agissant du recours formé contre un refus de délivrer un permis de construire modificatif, CE, 21 février 2018, Commune de Crest-Voland, n° 402109, T. p. 961- 963 ; s'agissant de la délivrance d'un permis modificatif, CE, Section, 16 février 1979, Société civile immobilière Cap Naïo, n° 3646, p. 66.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
