<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037022299</ID>
<ANCIEN_ID>JG_L_2018_06_000000410985</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/02/22/CETATEXT000037022299.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 06/06/2018, 410985</TITRE>
<DATE_DEC>2018-06-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>410985</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR>M. Richard Senghor</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:410985.20180606</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>M. Franck Dos Santos Pedro a demandé au tribunal administratif de Caen d'annuler pour excès de pouvoir la décision du 9 janvier 2012 par laquelle les autorités du centre pénitentiaire de Caen (Calvados) ont saisi et retenu son ordinateur et d'enjoindre à l'administration pénitentiaire, sous astreinte, de lui restituer son ordinateur. Par un jugement n° 120128 du 24 janvier 2013, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 13NT01435 du 24 avril 2014, la cour administrative d'appel de Nantes a rejeté l'appel formé par M. C...contre ce jugement.<br/>
<br/>
              Par une décision n° 383712 du 9 novembre 2005, le Conseil d'Etat, statuant au contentieux a annulé cet arrêt et renvoyé l'affaire devant la cour administrative d'appel de Nantes.<br/>
<br/>
              Par un arrêt n° 15NT06504 du 7 décembre 2016, la cour administrative d'appel de Nantes a annulé le jugement du tribunal administratif et rejeté la demande de M.C....<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 30 mai et 30 août 2017 au secrétariat du contentieux du Conseil d'Etat, M. C...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'arrêt du 7 décembre 2016 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              la loi n° 79-587 du 11 juillet 1979 ;<br/>
              la loi n° 2000-321 du 12 avril 2000 ;<br/>
              le code de procédure pénale ;<br/>
              le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Richard Senghor, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Spinosi, Sureau, avocat de M. A...C...;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, à la suite d'une fouille de la cellule de M. C...le 9 janvier 2012, il a été constaté que les scellés apposés sur l'unité centrale de son ordinateur avaient été brisés et qu'une corde y avait été dissimulée. Le directeur du centre pénitentiaire de Caen a ordonné un contrôle du matériel informatique de M. C...et décidé de retenir ces équipements en vue d'une éventuelle procédure pénale. Le 24 février 2012, une saisie judiciaire de ce matériel informatique a été réalisée. L'intéressé a saisi le tribunal administratif de Caen d'une demande tendant à l'annulation pour excès de pouvoir de la mesure de retenue de son matériel informatique. M. C...se pourvoit contre l'arrêt  du 28 décembre 2015 par lequel la cour administrative d'appel de Nantes a, après cassation d'un premier arrêt et renvoi de l'affaire par le Conseil d'Etat, rejeté sa demande.  <br/>
<br/>
              2. Aux termes de l'article D. 449-1 du code de procédure pénale alors applicable : " Les détenus peuvent acquérir par l'intermédiaire de l'administration et selon les modalités qu'elle détermine des équipements informatiques. Une instruction générale détermine les caractéristiques auxquelles doivent répondre ces équipements, ainsi que les conditions de leur utilisation. En aucun cas, les détenus ne sont autorisés à conserver des documents, autres que ceux liés à des activités socioculturelles ou d'enseignement ou de formation ou professionnelles, sur un support informatique. Ces équipements ainsi que les données qu'ils contiennent sont soumis au contrôle de l'administration. Sans préjudice d'une éventuelle saisie par l'autorité judiciaire, tout équipement informatique appartenant à un détenu peut, au surplus, être retenu, pour ne lui être restitué qu'au moment de sa libération, dans les cas suivants : 1° Pour des raisons d'ordre et de sécurité ; 2° En cas d'impossibilité d'accéder aux données informatiques, du fait volontaire du détenu ". <br/>
<br/>
              3. Aux termes de l'article 1er de la loi du 11 juillet 1979 relative à la motivation des actes administratifs et à l'amélioration des relations entre l'administration et le public, aujourd'hui codifié à l'article L.211-2 du code des relations entre le public et l'administration : " Les personnes physiques ou morales ont le droit d'être informées sans délai des motifs des décisions administratives individuelles défavorables qui les concernent. /A cet effet, doivent être motivées les décisions qui : -restreignent l'exercice des libertés publiques ou, de manière générale, constituent une mesure de police [...] ". L'article 3 de cette loi, aujourd'hui codifié à l'article L. 211-5 du code des relations entre le public et l'administration dispose que : " La motivation exigée par la présente loi doit être écrite et comporter l'énoncé des considérations de droit et de fait qui constituent le fondement de la décision ". Aux termes de l'article 4 de la même loi, aujourd'hui codifié à l'article L. 211-6 du code des relations entre le public et l'administration : " Lorsque l'urgence absolue a empêché qu'une décision soit motivée, le défaut de motivation n'entache pas d'illégalité cette décision. Toutefois, si l'intéressé en fait la demande, dans les délais du recours contentieux, l'autorité qui a pris la décision devra, dans un délai d'un mois, lui en communiquer les motifs [...] ".<br/>
<br/>
              4. Ainsi que l'a jugé la cour administrative d'appel de Nantes, la décision du 9 janvier 2012 procédant à la retenue du matériel informatique de M. B...présente le caractère d'une mesure de police, prononcée pour " des raisons d'ordre et de sécurité ", devant en principe faire l'objet d'une motivation en vertu des dispositions précitées de l'article 1er de la loi du 11 juillet 1979. En écartant toutefois le moyen tiré de l'absence de motivation de cette décision au motif que, prise pour faire obstacle à toute tentative d'évasion de l'intéressé, elle présentait un caractère d'urgence absolue, alors qu'il ressortait des pièces du dossier qui lui était soumis que la confiscation de la corde qui avait été dissimulée dans l'unité centrale de l'ordinateur suffisait à prévenir le risque d'une évasion imminente, la cour a entaché son arrêt d'inexacte qualification juridique des faits.<br/>
<br/>
              5. Il résulte de ce qui précède, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que M. C...est fondé à demander l'annulation de l'arrêt qu'il attaque.<br/>
<br/>
              6. Aux termes du second alinéa de l'article L. 821-2 du code de justice administrative : " Lorsque l'affaire fait l'objet d'un second pourvoi en cassation, le Conseil d'Etat statue définitivement sur cette affaire ". Le Conseil d'Etat étant saisi, en l'espèce, d'un second pourvoi en cassation, il lui incombe de régler l'affaire au fond.<br/>
<br/>
              7. M. C...est fondé à soutenir que c'est à tort que, par le jugement du 24 janvier 2013, le tribunal administratif de Caen a rejeté sa demande d'annulation de la décision du 9 janvier 2012 au motif qu'elle n'était pas susceptible de faire l'objet d'un recours pour excès de pouvoir. <br/>
<br/>
              8. Il y a lieu d'évoquer et de statuer immédiatement sur la demande de première instance.<br/>
<br/>
              9. D'une part, une mesure de retenue du matériel informatique d'un détenu, prononcée pour des raisons d'ordre et de sécurité sur le fondement de l'article D. 449-1 du code de procédure pénale précité, constitue une mesure de police devant faire, en principe, l'objet d'une motivation écrite en vertu des articles 1er et 3 de la loi du 11 juillet 1979 précités.<br/>
<br/>
              10. D'autre part, aux termes de l'article 24 de la loi du 12 avril 2000 relative aux droits des citoyens dans leurs relations avec les administrations, désormais codifié à l'article L. 122-1 du code des relations entre le public et les administrations : " Exception faite des cas où il est statué sur une demande, les décisions individuelles qui doivent être motivées en application des articles 1er et 2 de la loi n° 79-587 du 11 juillet 1979 relative à la motivation des actes administratifs et à l'amélioration des relations entre l'administration et le public n'interviennent qu'après que la personne intéressée a été mise à même de présenter des observations écrites et, le cas échéant, sur sa demande, des observations orales. (...) / Les dispositions de l'alinéa précédent ne sont pas applicables : / 1° En cas d'urgence ou de circonstances exceptionnelles ; (...) ". <br/>
<br/>
              11. Il ressort des pièces du dossier que si les conditions particulières dans lesquelles est intervenue la décision du 9 janvier 2012 caractérisaient une urgence dispensant le chef d'établissement pénitentiaire de Caen du respect de la procédure contradictoire prévue par les dispositions précitées, la situation ne présentait pas, ainsi qu'il a été dit au point 4, un caractère d'urgence absolue justifiant l'absence de motivation écrite de la décision ordonnant la retenue de l'ordinateur du requérant.<br/>
<br/>
              12. Il résulte de ce qui précède que, sans qu'il soit besoin d'examiner les autres moyens de sa demande, M. C...est fondé à demander l'annulation de la décision qu'il attaque. <br/>
<br/>
              13. Il résulte de l'instruction que l'équipement informatique du requérant a fait l'objet d'une saisie judiciaire à compter du 24 février 2012. Dans ces conditions, les conclusions tendant à ce que soit ordonnée la restitution de l'ordinateur ne peuvent qu'être rejetées.<br/>
<br/>
              14. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 4000 euros à verser à M.C..., au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nantes du 7 décembre 2016 et le jugement du tribunal administratif de Caen du 24 janvier 2013 sont annulés.<br/>
Article 2 : La décision du 9 janvier 2012 est annulée.<br/>
Article 3 : Le surplus des conclusions de la demande de M. C...est rejeté.<br/>
Article 4 : L'Etat versera à M. C...une somme de 4000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 5 : La présente décision sera notifiée à M. A...C...et à la garde des sceaux, ministre de la justice.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-03-01-02-01-01-01 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - FORME ET PROCÉDURE. QUESTIONS GÉNÉRALES. MOTIVATION. MOTIVATION OBLIGATOIRE. MOTIVATION OBLIGATOIRE EN VERTU DES ARTICLES 1 ET 2 DE LA LOI DU 11 JUILLET 1979. DÉCISION RESTREIGNANT L'EXERCICE DES LIBERTÉS PUBLIQUES OU, DE MANIÈRE GÉNÉRALE, CONSTITUANT UNE MESURE DE POLICE. - DÉCISION DE RETENUE DU MATÉRIEL INFORMATIQUE D'UN DÉTENU DANS LEQUEL CE DERNIER AVAIT DISSIMULÉ UNE CORDE, PRONONCÉE SUR LE FONDEMENT DE L'ARTICLE D. 449-1 DU CPP - URGENCE ABSOLUE JUSTIFIANT L'ABSENCE DE MOTIVATION ÉCRITE (ART. L. 211-6 DU CRPA) - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">01-03-03-02 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - FORME ET PROCÉDURE. PROCÉDURE CONTRADICTOIRE. CARACTÈRE NON OBLIGATOIRE. - DÉCISION DE RETENUE DU MATÉRIEL INFORMATIQUE D'UN DÉTENU DANS LEQUEL CE DERNIER AVAIT DISSIMULÉ UNE CORDE, PRONONCÉE SUR LE FONDEMENT DE L'ARTICLE D. 449-1 DU CPP - URGENCE DISPENSANT LE CHEF D'ÉTABLISSEMENT PÉNITENTIAIRE DU RESPECT DE LA PROCÉDURE CONTRADICTOIRE (ART. L. 122-1 DU CRPA) - EXISTENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">37-05-02-01 JURIDICTIONS ADMINISTRATIVES ET JUDICIAIRES. EXÉCUTION DES JUGEMENTS. EXÉCUTION DES PEINES. SERVICE PUBLIC PÉNITENTIAIRE. - DÉCISION DE RETENUE DU MATÉRIEL INFORMATIQUE D'UN DÉTENU DANS LEQUEL CE DERNIER AVAIT DISSIMULÉ UNE CORDE, PRONONCÉE SUR LE FONDEMENT DE L'ARTICLE D. 449-1 DU CPP - URGENCE DISPENSANT LE CHEF D'ÉTABLISSEMENT PÉNITENTIAIRE DU RESPECT DE LA PROCÉDURE CONTRADICTOIRE (ART. L. 122-1 DU CRPA) - EXISTENCE - URGENCE ABSOLUE JUSTIFIANT L'ABSENCE DE MOTIVATION ÉCRITE (ART. L. 211-6 DU CRPA) - ABSENCE [RJ1].
</SCT>
<ANA ID="9A"> 01-03-01-02-01-01-01 La situation ayant conduit un chef d'établissement pénitentiaire à prendre, sur le fondement de l'article D. 449-1 du code de procédure pénale (CPP), une décision de retenue du matériel informatique d'un détenu dans lequel ce dernier avait dissimulé une corde ne présentait pas un caractère d'urgence absolue justifiant l'absence de motivation écrite de cette décision en vertu de l'article 4 de la loi n° 79-587 du 11 juillet 1979, désormais codifié à l'article L. 211-6 du code des relations entre le public et l'administration (CRPA).</ANA>
<ANA ID="9B"> 01-03-03-02 La situation ayant conduit un chef d'établissement pénitentiaire à prendre, sur le fondement de l'article D. 449-1 du code de procédure pénale (CPP),  une décision de retenue du matériel informatique d'un détenu dans lequel ce dernier avait dissimulé une corde revêtait le caractère d'une situation d'urgence dispensant le chef d'établissement pénitentiaire du respect de la procédure contradictoire prévue par l'article 24 de la loi n° 2000-321 du 12 avril 2000, désormais codifié à l'article L. 122-1 du code des relations entre le public et l'administration (CRPA).</ANA>
<ANA ID="9C"> 37-05-02-01 Décision d'un chef d'établissement pénitentiaire, prononcée pour des raisons d'ordre et de sécurité sur le fondement de l'article D. 449-1 du code de procédure pénale (CPP), procédant à la retenue du matériel informatique d'un détenu dans lequel ce dernier avait dissimulé une corde.... ,,Si les conditions particulières dans lesquelles est intervenue cette décision caractérisaient une urgence dispensant le chef d'établissement pénitentiaire du respect de la procédure contradictoire prévue par l'article 24 de la loi n° 2000-321 du 12 avril 2000, désormais codifié à l'article L. 122-1 du code des relations entre le public et l'administration (CRPA), la situation ne présentait pas un caractère d'urgence absolue justifiant l'absence de motivation écrite, en vertu de l'article 4 de la loi n° 79-587 du 11 juillet 1979, désormais codifié à l'article L. 211-6 du CRPA, de la décision ordonnant la retenue de l'ordinateur du requérant.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., s'agissant de la dissociation des exigences de contradictoire et de motivation, CE, Assemblée, 6 juillet 2016,,, n°s 398234 399135, p. 320 ; CE, 28 septembre 2016, Ministre de l'intérieur c/,, n° 390438, T. pp. 610-612-851 ; CE, 18 décembre 2017, Ministre de l'intérieur c/,, n° 409694, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
