<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026410531</ID>
<ANCIEN_ID>JG_L_2012_09_000000353998</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/41/05/CETATEXT000026410531.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 4ème sous-sections réunies, 24/09/2012, 353998</TITRE>
<DATE_DEC>2012-09-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>353998</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 4ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Jacques Arrighi de Casanova</PRESIDENT>
<AVOCATS>SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>M. Gérald Bégranger</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Fabienne Lambolez</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:353998.20120924</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la requête, enregistrée le 10 novembre 2011 au secrétariat du contentieux du Conseil d'Etat, présentée par M. Xavier C, demeurant ... ; M. C demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler le jugement n° 1102659 du 14 octobre 2011 par lequel le tribunal administratif de Cergy-Pontoise a annulé, sur la protestation de M. Nicolas A, les opérations électorales qui se sont déroulées les 20 et 27 mars 2011 pour la désignation du conseiller général du canton d'Argenteuil-Ouest ; <br/>
<br/>
              2°) de rejeter la protestation de M. A contre ces opérations électorales ;<br/>
<br/>
              3°) de mettre à la charge de M. A la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 10 septembre 2012, présentée pour M. A ;<br/>
<br/>
              Vu le code électoral ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Gérald Bégranger, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les observations de la SCP Lyon-Caen, Thiriez avocat de M. A,<br/>
<br/>
              - les conclusions de Mme Fabienne Lambolez, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Lyon-Caen, Thiriez avocat de M. A ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'à l'issue du second tour des opérations électorales qui se sont déroulées le 27 mars 2011 pour l'élection d'un conseiller général dans le canton d'Argenteuil-Ouest (Val d'Oise), M. C a été proclamé élu par 3 265 voix contre 3 243 voix pour M. A ; que M. C relève appel du jugement du 14 octobre 2011 par lequel le tribunal administratif de Cergy-Pontoise a, sur la protestation de M. A, annulé ces opérations électorales au motif que des suffrages émis au second tour de scrutin en nombre supérieur à l'écart des voix devaient être regardés comme irréguliers en raison de différences manifestes entre la signature figurant sur la liste d'émargement pour ce tour de scrutin et celle qui y figurait pour le même électeur au premier tour ; <br/>
<br/>
              2. Considérant qu'en vertu du troisième alinéa de l'article L. 62-1 du code électoral : " Le vote de chaque électeur est constaté par sa signature apposée à l'encre en face de son nom sur la liste d'émargement " ; que le second alinéa de l'article L. 64 du même code dispose que : " Lorsqu'un électeur se trouve dans l'impossibilité de signer, l'émargement prévu par le troisième alinéa de l'article L. 62-1 est apposé par un électeur de son choix qui fait suivre sa signature de la mention suivante : l'électeur ne peut signer lui-même " ; qu'il résulte de ces dispositions, destinées à assurer la sincérité des opérations électorales, que seule la signature personnelle, à l'encre, d'un électeur est de nature à apporter la preuve de sa participation au scrutin, sauf cas d'impossibilité dûment mentionnée sur la liste d'émargement ;<br/>
<br/>
              3. Considérant qu'il résulte de l'examen des listes d'émargement que, sur les vingt-neuf émargements jugés irréguliers par le tribunal administratif, les signatures correspondant aux électeurs ayant voté sous le n° 1620 dans le bureau de vote n° 34, sous le n° 1162 dans le bureau de vote n° 36, sous les n° 1473 et 1798 dans le bureau de vote n° 37, sous les n° 2068, 2070, 2127, 2237, 2467 et 2540 dans le bureau de vote n° 38, sous le n° 761 dans le bureau de vote n° 49 et sous le n° 644 dans le bureau de vote n° 50 ne présentent pas des différences manifestes entre les deux tours de scrutin ; que, si les signatures des électeurs inscrits sous le n° 1940 dans le bureau de vote n° 37 et sous le n° 340 dans le bureau de vote n° 50 présentent de telles différences, la signature figurant sur la liste d'émargement pour le second tour est, pour chacun, identique à celle figurant sur la copie de la carte nationale d'identité produite à l'appui de l'attestation par laquelle ils assurent être l'auteur de leur vote ; que, par suite, il y a lieu de regarder les quatorze suffrages mentionnés ci-dessus comme régulièrement exprimés ; <br/>
<br/>
              4. Considérant que c'est par ailleurs à bon droit que le tribunal administratif a regardé comme réguliers les émargements des électeurs ayant voté sous les n°s 1320 et 1401 dans le bureau de vote n° 37, sous les n°s 1884, 1888, 1999 et 2045 dans le bureau de vote n° 38 et sous le n° 281 dans le bureau de vote n° 48 ;<br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que seuls quinze suffrages doivent être regardés comme irréguliers en raison des différences des signatures apposées sur la liste d'émargement aux deux tours de scrutin ; que, ce chiffre étant inférieur à l'écart de vingt-deux voix constaté au second tour de scrutin, M. C est fondé à soutenir que c'est à tort que le tribunal administratif a retenu ce grief pour annuler les opérations électorales ; <br/>
<br/>
              6. Considérant, toutefois, qu'il appartient au Conseil d'Etat, saisi de l'ensemble du litige par l'effet dévolutif de l'appel, d'examiner les autres griefs soulevés par M. A à l'appui de sa protestation ; <br/>
<br/>
              Sur les griefs relatifs au déroulement du vote :<br/>
<br/>
              7. Considérant, en premier lieu, que si, outre les quinze suffrages mentionnés ci-dessus, trois autres suffrages doivent être tenus pour irréguliers du fait que le nombre des enveloppes trouvées dans l'urne à l'issue du second tour était supérieur d'une unité au nombre d'émargements dans les bureaux de vote n° 39, 47 et 50, le nombre total des suffrages qui doivent ainsi être retranché du nombre de voix obtenu par M. C est inférieur à l'écart de voix constaté au second tour ; qu'ainsi, ces irrégularités n'ont pas pu avoir d'incidence sur l'issue du scrutin ; <br/>
<br/>
              8. Considérant, en deuxième lieu, qu'il ne résulte pas de l'instruction que, comme M. A l'affirme sans produire aucune pièce, l'épouse de M. C aurait voté au nom d'un électeur sans avoir été mandatée par lui ;<br/>
<br/>
              9. Considérant, en troisième lieu, que si M. A soutient que six électeurs n'ont pu voter au premier tour du scrutin parce que les procurations qui leur avaient été données n'avaient pas été transmises aux services municipaux, il résulte de l'instruction que ces procurations ont été remises le 19 mars 2011 aux services de la mairie d'Argenteuil par les fonctionnaires de police du commissariat de cette ville ; qu'ainsi, ce grief manque en fait ;<br/>
<br/>
              10. Considérant qu'il résulte de ce qui précède que M. A n'est pas fondé à soutenir que les opérations relatives au déroulement du vote ont été entachées par des irrégularités ayant pu altérer la sincérité du scrutin ;<br/>
<br/>
              Sur le grief tiré d'irrégularités commises pendant la campagne électorale :<br/>
<br/>
              11. Considérant, d'une part, que le contenu des tracts et des lettres d'information datées des mois de septembre, d'octobre et de décembre 2010, ainsi que des mois de janvier et de février-mars 2011, diffusés dans le cadre de la campagne électorale de M. C n'excédait pas les limites habituelles de la polémique électorale ; que, de surcroît, la date de diffusion de ces documents permettait à M. A d'y répondre utilement ; <br/>
<br/>
              12. Considérant, d'autre part, que les textes publiés sur le site internet " Argenteuil politique ", consistant principalement en des extraits d'articles de presse et en des échanges entre internautes, n'ont pas excédé les limites habituelles de la polémique électorale ; qu'il en va de même des tracts rédigés sous couvert d'anonymat, dont la date et l'ampleur de la diffusion ne sont d'ailleurs pas établies, de la lettre ouverte d'un habitant d'Argenteuil demandant à la municipalité de lui attribuer un logement et de la lettre d'information de l'association " Légitimes dépenses " publiée le 16 février 2011 ;<br/>
<br/>
              13. Considérant qu'il résulte de ce qui précède que M. A n'est pas fondé à soutenir que la diffusion des documents mentionnés ci-dessus aurait altéré la sincérité du scrutin ;<br/>
<br/>
              Sur le compte de campagne de M. C : <br/>
<br/>
              14. Considérant qu'aux termes de l'article L. 52-8 du code électoral, dans sa rédaction applicable au scrutin litigieux : "  (...) Les personnes morales, à l'exception des partis ou groupements politiques, ne peuvent participer au financement de la campagne électorale d'un candidat, ni en lui consentant des dons sous quelque forme que ce soit, ni en lui fournissant des biens, services ou autres avantages directs ou indirects à des prix inférieurs à ceux qui sont habituellement pratiqués. (...) " ; qu'aux termes de l'article L. 52-12 du code électoral, dans sa rédaction alors en vigueur : " Chaque candidat ou candidat tête de liste soumis au plafonnement prévu à l'article L. 52-11 est tenu d'établir un compte de campagne retraçant, selon leur origine, l'ensemble des recettes perçues et, selon leur nature, l'ensemble des dépenses engagées ou effectuées en vue de l'élection, hors celles de la campagne officielle par lui-même ou pour son compte, au cours de la période mentionnée à l'article L. 52-4. Sont réputées faites pour son compte les dépenses exposées directement au profit du candidat et avec l'accord de celui-ci, par les personnes physiques qui lui apportent leur soutien, ainsi que par les partis et groupements politiques qui ont été créés en vue de lui apporter leur soutien ou qui lui apportent leur soutien. Le candidat estime et inclut, en recettes et en dépenses, les avantages directs ou indirects, les prestations de services et dons en nature dont il a bénéficié. Le compte de campagne doit être en équilibre ou excédentaire et ne peut présenter un déficit. (...) " ;<br/>
<br/>
              15. Considérant, en premier lieu, que si M. A soutient que les dépenses liées à la réalisation et à la diffusion des tracts et des lettres d'information de M. C ainsi que celles relatives à la création et au fonctionnement de son " blog " n'ont pas été mentionnées dans son compte de campagne, il résulte de l'instruction que ce grief manque en fait ;<br/>
<br/>
              16. Considérant, en deuxième lieu, que si M. A soutient que le compte de campagne de M. C ne retrace pas les avantages en nature qu'il a retirés du fonctionnement du site internet " Argenteuil politique ", il résulte de l'instruction que ce site, qui n'a pas fonctionné au profit de M. C pour les besoins de sa campagne électorale, ne constitue pas un avantage indirect au sens des dispositions de l'article L. 52-8 du code électoral ; <br/>
<br/>
              17. Considérant, en troisième lieu, qu'il résulte de l'instruction que les concours en nature de l'association " Argenteuil que nous aimons " dont M. C a bénéficié ont été retracés dans son compte de campagne ; <br/>
<br/>
              18. Considérant, en quatrième lieu, qu'il ne résulte pas de l'instruction que l'association " Légitime dépenses " ait participé au financement de la campagne de M. C ; qu'en particulier, la lettre d'information diffusée en février 2011, par laquelle elle critique la politique fiscale conduite par la municipalité d'Argenteuil, ne constitue pas un avantage indirect au sens des dispositions précitées de l'article L. 52-8 du code électoral ; <br/>
<br/>
              19. Considérant qu'il résulte de ce qui précède que M. A n'est pas fondé à soutenir que la campagne de M. C aurait été financée dans des conditions contraires aux dispositions applicables ;<br/>
<br/>
              20. Considérant qu'il résulte de tout ce qui précède que, sans qu'il soit besoin d'examiner la fin de non-recevoir opposée par M. C, ce dernier est fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Cergy-Pontoise a annulé son élection en qualité de conseiller général du canton d'Argenteuil-Ouest ;<br/>
<br/>
              Sur les conclusions présentées au titre de l'article L.761-1 du code de justice administrative : <br/>
<br/>
              21. Considérant que ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de M. C qui n'est pas, dans la présente instance, la partie perdante ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions que M. C présente sur le fondement de ces mêmes dispositions ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Cergy-Pontoise du 14 octobre 2011 est annulé.<br/>
<br/>
Article 2 : L'élection de M. C en qualité de conseiller général du canton d'Argenteuil-Ouest est validée. <br/>
<br/>
		Article 3 : La protestation de M. A est rejetée.<br/>
<br/>
Article 4 : Les conclusions des parties présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
<br/>
Article 5 : La présente décision sera notifiée à M. Xavier C, à M. Nicolas A, au ministre de l'intérieur et à la Commission nationale des comptes de campagne et des financements politiques.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">28-005-03 ÉLECTIONS ET RÉFÉRENDUM. DISPOSITIONS GÉNÉRALES APPLICABLES AUX ÉLECTIONS. OPÉRATIONS ÉLECTORALES. - LISTE D'ÉMARGEMENT - SIGNATURE PAR LES ÉLECTEURS - DIFFÉRENCE DE SIGNATURE ENTRE LES DEUX TOURS DE SCRUTIN - VOTE IRRÉGULIÈREMENT EXPRIMÉ - ABSENCE, DÈS LORS QUE LA SIGNATURE EST, POUR LE TOUR DE SCRUTIN CONCERNÉ, IDENTIQUE À CELLE DE LA CARTE D'IDENTITÉ JOINTE À L'ATTESTATION PAR LAQUELLE LES ÉLECTEURS INTÉRESSÉS ASSURENT ÊTRE AUTEUR DE LEUR VOTE [RJ1].
</SCT>
<ANA ID="9A"> 28-005-03 La circonstance que la signature apposée par un électeur sur la liste d'émargement à un tour de scrutin diffère de celle apposée par lui sur la liste d'émargement de l'autre tour de scrutin est sans incidence sur la régularité du suffrage exprimé, dès lors que la signature en cause est identique à celle figurant sur la copie de la carte nationale d'identité produite à l'appui d'une attestation par laquelle l'électeur assure être l'auteur de son vote.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Comp., pour l'absence d'incidence, en cas d'apposition d'une croix sur la liste d'émargement, de la production a posteriori d'attestations émanant des électeurs intéressés, CE, 23 septembre 2005, Elections cantonales de Saint-Paul (Réunion) et Soret, n° 274402, T. p. 891.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
