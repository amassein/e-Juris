<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031978208</ID>
<ANCIEN_ID>JG_L_2016_02_000000376620</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/97/82/CETATEXT000031978208.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème / 4ème SSR, 03/02/2016, 376620</TITRE>
<DATE_DEC>2016-02-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>376620</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème / 4ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GADIOU, CHEVALLIER ; SCP SEVAUX, MATHONNET ; LE PRADO ; SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>M. Lionel Collet</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Laurence Marion</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2016:376620.20160203</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A...-C... B...a demandé au tribunal administratif de Lille de mettre à la charge du centre hospitalier régional universitaire (CHRU) de Lille la réparation des préjudices qu'elle a subis à la suite d'une intervention chirurgicale réalisée le 23 mars 2001 dans cet établissement. Par un jugement n° 1100429 du 19 décembre 2012, le tribunal administratif de Lille a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 13DA00084 du 21 janvier 2014, la cour administrative d'appel de Douai a rejeté l'appel formé par Mme A...-C... B...contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 24 mars 2014 et 24 juin 2014 au secrétariat du contentieux du Conseil d'Etat, Mme B...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge du centre hospitalier régional universitaire de Lille la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
	Vu :<br/>
- le code de la santé publique ;<br/>
	- le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Lionel Collet, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Laurence Marion, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gadiou, Chevallier, avocat de Mme A...-franceB..., à Me Le Prado, avocat du centre hospitalier régional universitaire de Lille, et à la SCP Sevaux, Mathonnet, avocat de l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales et à la SCP Foussard Froger, avocat de la caisse primaire d'assurance maladie du Hainaut ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que Mme B...a subi le 23 mars 2001 au centre hospitalier régional universitaire (CHRU) de Lille une intervention chirurgicale destinée à réduire deux hernies discales thoraciques qui entraînaient une compression de la moelle épinière ; qu'en raison d'une lésion médullaire provoquée par cette intervention, la patiente est demeurée atteinte de paraplégie ; qu'elle a recherché la responsabilité de l'établissement devant le tribunal administratif de Lille, qui a rejeté sa demande par un jugement du 19 décembre 2012 ; qu'elle se pourvoit en cassation contre l'arrêt du 21 janvier 2014 par lequel la cour administrative d'appel de Douai a rejeté son appel contre ce jugement ; que la caisse primaire d'assurance maladie (CPAM) du Hainault demande également l'annulation de l'arrêt ; <br/>
<br/>
              Sur la responsabilité du CHRU de Lille au titre d'un défaut d'information sur les risques de l'intervention pratiquée le 23 mars 2001 :<br/>
<br/>
              2. Considérant que lorsque l'acte médical envisagé, même accompli conformément aux règles de l'art, comporte des risques connus de décès ou d'invalidité, le patient doit en être informé dans des conditions qui permettent de recueillir son consentement éclairé ; que, si cette information n'est pas requise en cas d'urgence, d'impossibilité, de refus du patient d'être informé, la seule circonstance que les risques ne se réalisent qu'exceptionnellement ne dispense pas les praticiens de leur obligation ; qu'un manquement des médecins à leur obligation d'information engage la responsabilité de l'hôpital dans la mesure où il a privé le patient d'une chance de se soustraire au risque lié à l'intervention en refusant qu'elle soit pratiquée ; que c'est seulement dans le cas où l'intervention était impérieusement requise, en sorte que le patient ne disposait d'aucune possibilité raisonnable de refus, que les juges du fond peuvent nier l'existence d'une perte de chance ;     <br/>
<br/>
              3. Considérant que la cour administrative d'appel de Douai a constaté que le CHRU de Lille n'apportait pas la preuve que Mme B...avait été informée, avant l'opération chirurgicale du 23 mars 2001, que cette intervention comportait des risques de complications neurologiques ; que la cour a toutefois retenu qu'il résultait de l'instruction, notamment d'un rapport d'expertise, que la patiente présentait avant l'opération deux hernies discales calcifiées, provoquant une compression médullaire lente révélée par des troubles neurologiques et qu'eu égard à la gravité de ces pathologies, dont l'évolution vers une paraplégie était inéluctable, une intervention chirurgicale présentait un caractère impérieux, excluant toute possibilité raisonnable de refus ; que la cour en a déduit que le manquement des médecins à leur devoir d'information n'avait pas entraîné pour l'intéressée la perte d'une chance d'éviter le dommage ; qu'en se prononçant ainsi, sans rechercher dans quel délai une évolution vers une paraplégie était susceptible de se produire si la patiente refusait de subir dans l'immédiat l'intervention qui lui était proposée, la cour n'a pas légalement justifié son arrêt sur ce point ; <br/>
<br/>
              Sur la responsabilité sans faute :<br/>
<br/>
              4. Considérant que, jusqu'au 5 septembre 2001, date fixée par l'article 101 de la loi du 4 mars 2002 pour l'application des dispositions de l'article L. 1142-1 du code de la santé publique,  lorsqu'un acte médical nécessaire au diagnostic ou au traitement du malade présentait un risque dont l'existence était connue, mais dont la réalisation était exceptionnelle et dont aucune raison ne permettait de penser que le patient y serait particulièrement exposé, la responsabilité du service public hospitalier était engagée si l'exécution de cet acte était la cause directe de dommages sans rapport avec l'état initial du patient comme avec l'évolution prévisible de cet état, et présentant un caractère d'extrême gravité ;<br/>
<br/>
              5. Considérant que Mme B...n'a pas invoqué devant les juges du fond la responsabilité du CHRU de Lille au titre d'un accident médical ; que la responsabilité sans faute de l'administration étant d'ordre public, il appartenait à la cour administrative d'appel de vérifier d'office si les conditions énoncées ci-dessus étaient remplies en l'espèce ; que, dès lors qu'elle n'a pas condamné l'établissement à ce titre, elle doit être regardée comme ayant jugé implicitement que ces conditions n'étaient pas remplies ; qu'en l'absence de moyen en ce sens, elle n'était pas tenue de motiver son arrêt sur ce point ; <br/>
<br/>
              6. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond, notamment du rapport d'expertise, que, compte tenu de la gravité des deux hernies discales, le risque de lésion médullaire entraînant des séquelles neurologiques lors de l'intervention destinée à les réduire ne présentait pas un caractère exceptionnel ; que, l'une des conditions rappelées au point 5 n'étant pas remplie, les moyens tirés de ce que la cour aurait commis une erreur de droit et dénaturé les pièces du dossier en ne retenant pas la responsabilité sans faute du CHRU de Lille doivent être écartés ; <br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que Mme B...et la CPAM du Hainault ne sont fondées à demander l'annulation de l'arrêt du 21 janvier 2014 de la cour administrative d'appel de Douai qu'en tant qu'il se prononce sur la réparation des conséquences du défaut d'information sur les risques de l'intervention pratiquée le 23 mars 2001 ;<br/>
<br/>
              8. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du CHRU de Lille le versement à Mme B...et à la CPAM du Hainault d'une somme de 3 000 euros chacun au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 21 janvier 2014 de la cour administrative d'appel de Douai est annulé en tant qu'il se prononce sur la réparation des conséquences du défaut d'information sur les risques de l'intervention pratiquée le 23 mars 2001.<br/>
<br/>
Article 2: L'affaire est renvoyée dans la mesure de la cassation ainsi prononcée à la cour administrative d'appel de Douai.<br/>
<br/>
Article 3 : Le CHRU de Lille versera à Mme B...et à la CPAM du Hainault une somme de 3 000 euros chacun au titre des dispositions de l'article L. 761-1 du code de justice administrative<br/>
<br/>
Article 4 : Le surplus des conclusions des pourvois de Mme B... et de la CPAM du Hainault est rejeté.<br/>
<br/>
Article 5 : La présente décision sera notifiée à Mme A... -C...B..., au centre hospitalier régional universitaire de Lille et à la caisse primaire d'assurance maladie du Hainaut.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">60-02-01-01-01-01-04 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RESPONSABILITÉ EN RAISON DES DIFFÉRENTES ACTIVITÉS DES SERVICES PUBLICS. SERVICE PUBLIC DE SANTÉ. ÉTABLISSEMENTS PUBLICS D'HOSPITALISATION. RESPONSABILITÉ POUR FAUTE SIMPLE : ORGANISATION ET FONCTIONNEMENT DU SERVICE HOSPITALIER. EXISTENCE D'UNE FAUTE. MANQUEMENTS À UNE OBLIGATION D'INFORMATION ET DÉFAUTS DE CONSENTEMENT. - INDEMNISATION DE LA PERTE DE CHANCE DE SE SOUSTRAIRE AU RISQUE LIÉ À L'INTERVENTION - CAS OÙ LE PATIENT NE DISPOSAIT D'AUCUNE POSSIBILITÉ RAISONNABLE DE REFUS DÈS LORS QUE L'INTERVENTION ÉTAIT IMPÉRIEUSEMENT REQUISE [RJ1] - OBLIGATION DE TENIR COMPTE DU DÉLAI DANS LEQUEL UNE ÉVOLUTION VERS DES CONSÉQUENCES GRAVES EST SUSCEPTIBLE DE SE PRODUIRE POUR APPRÉCIER LE CARACTÈRE IMPÉRIEUSEMENT REQUIS DE L'INTERVENTION - EXISTENCE.
</SCT>
<ANA ID="9A"> 60-02-01-01-01-01-04 Si, à la suite d'un défaut d'information, le juge peut nier l'existence d'une perte de chance de se soustraire au risque lié à l'intervention au motif que celle-ci était impérieusement requise, il lui appartient, pour se prononcer en ce sens, de rechercher dans quel délai une évolution vers des conséquences graves était susceptible de se produire si le patient refusait de subir dans l'immédiat l'intervention.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 10 octobre 2012, M.,et Mme,, n° 350426, p. 357.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
