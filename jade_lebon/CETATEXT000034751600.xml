<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034751600</ID>
<ANCIEN_ID>JG_L_2017_05_000000398516</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/75/16/CETATEXT000034751600.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 17/05/2017, 398516</TITRE>
<DATE_DEC>2017-05-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>398516</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>DELAMARRE</AVOCATS>
<RAPPORTEUR>Mme Marie-Anne Lévêque</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier Henrard</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:398516.20170517</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Par une décision du 23 novembre 2016, le Conseil d'Etat, statuant au contentieux, a prononcé l'admission des conclusions du pourvoi de Mme C...B...dirigées contre l'arrêt la cour administrative d'appel de Bordeaux du 19 novembre 2015 en tant seulement que cet arrêt rejette ses conclusions tendant à l'annulation pour excès de pouvoir de la décision du préfet de la Haute-Garonne du 31 décembre 2014 portant obligation de quitter le territoire français dans un délai de trente jours et fixant le pays de renvoi. <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - l'accord entre le gouvernement de la République française et le gouvernement de la République gabonaise relatif à la gestion concertée des flux migratoires et au codéveloppement signé à Libreville le 5 juillet 2007 ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie-Anne Lévêque, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Olivier Henrard, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Delamarre, avocat de MmeB....<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des énonciations de l'arrêt attaqué que Mme B..., ressortissante gabonaise entrée en France en 2002, a bénéficié d'une carte de séjour temporaire portant la mention " étudiant ", régulièrement renouvelée du 4 février 2003 au 13 novembre 2011 ; qu'elle a été autorisée provisoirement au séjour pour une durée de neuf mois, en application des stipulations de l'article 2.2 de l'accord franco-gabonais du 5 juillet 2007, le 7 septembre 2011 ; que, par un jugement du 27 mai 2013, le tribunal administratif de Toulouse a enjoint au préfet de la Haute-Garonne de renouveler l'autorisation provisoire de séjour d'une durée de neuf mois dont elle bénéficiait ; qu'en réponse à une nouvelle demande de délivrance d'un titre de séjour présentée par la requérante, le préfet de la Haute-Garonne, par un arrêté du 31 décembre 2014, a refusé de lui délivrer un titre de séjour sur le fondement des dispositions du 7° de l'article L. 313-11 du code de l'entrée et du séjour des étrangers et du droit d'asile, lui a fait obligation de quitter le territoire français dans un délai de trente jours et a fixé le pays de destination ; que, par l'arrêt attaqué, la cour administrative d'appel de Bordeaux a annulé le jugement du 13 mai 2014 du tribunal administratif de Toulouse et, statuant par la voie de l'évocation, rejeté la demande de Mme B...tendant à l'annulation de cet arrêté ; que, par une décision du 23 novembre 2016, le Conseil d'Etat, statuant au contentieux, a admis les conclusions du pourvoi de Mme B...dirigées contre cet arrêt en tant qu'il statue sur l'obligation de quitter le territoire dans un délai de trente jours et sur la fixation du pays de destination ; <br/>
<br/>
              2. Considérant, en premier lieu, qu'aux termes de l'article L. 511-4 du code de l'entrée et du séjour des étrangers et du droit d'asile : " Ne peuvent faire l'objet d'une obligation de quitter le territoire français ou d'une mesure de reconduite à la frontière en application du présent chapitre : (...) / 4º L'étranger qui réside régulièrement en France depuis plus de dix ans, sauf s'il a été, pendant toute cette période, titulaire d'une carte de séjour temporaire portant la mention " étudiant " ; (...) " ; qu'aux termes de l'article L. 313-7 du même code, un titre de séjour portant la mention " étudiant " peut être délivré à " l'étranger qui établit qu'il suit en France un enseignement ou qu'il y fait des études (...) " ; qu'il résulte de ces dispositions que les étrangers n'ayant obtenu, pendant la période de dix ans, des titres de séjour qu'en qualité d'étudiant, en vue de suivre en France un enseignement ou d'y faire des études, ne peuvent bénéficier des dispositions du 4° de l'article L. 511-4 du code de l'entrée et du séjour des étrangers et du droit d'asile ; <br/>
<br/>
              3. Considérant, en second lieu, que l'article 2.2 de l'accord franco-gabonais du 5 juillet 2007 prévoit qu' " une autorisation provisoire de séjour d'une durée de validité de neuf (9) mois renouvelable une fois est délivrée au ressortissant gabonais, qui, ayant achevé avec succès (...) un cycle de formation conduisant à la licence professionnelle ou à un diplôme au moins équivalent au master, souhaite compléter sa formation par une première expérience professionnelle ", que, " pendant la durée de cette autorisation, son titulaire est autorisé à chercher et, le cas échéant, à exercer un emploi en relation avec sa formation (...) " et qu' " à l'issue de la période de validité " de cette autorisation, l'intéressé, pourvu d'un emploi ou titulaire d'une promesse d'embauche (...) est autorisé à séjourner en France pour l'exercice de son activité professionnelle (...) " ; qu'il résulte de ces stipulations que l'autorisation provisoire de séjour délivrée sur ce fondement à un ressortissant gabonais a pour objet de lui permettre de rechercher et d'exercer un emploi afin d'obtenir une première expérience professionnelle, et non de suivre un enseignement ou des études en France ; <br/>
<br/>
              4. Considérant qu'après avoir relevé que Mme B...avait séjourné régulièrement sur le territoire français pendant plus de dix ans et avait, durant cette période, bénéficié d'une autorisation de séjour provisoire d'une durée de neuf mois, renouvelée une fois pour la même durée, sur le fondement des stipulations de l'article 2.2 de l'accord franco-gabonais du 5 juillet 2007, la cour administrative d'appel de Bordeaux a estimé que l'intéressée ne pouvait se prévaloir des dispositions du 4° de l'article L. 511-4 du code de l'entrée et du séjour des étrangers et du droit d'asile, au motif que les autorisations provisoires de séjour dont elle avait bénéficié devaient être regardées comme des titres de séjour en qualité d'étudiant ; qu'elle a ainsi entaché son arrêt d'erreur de droit ; qu'il résulte de ce qui précède, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que Mme B...est fondée à en demander l'annulation en tant qu'il a rejeté ses conclusions dirigées contre la décision du préfet de la Haute-Garonne du 31 décembre 2014 portant obligation de quitter le territoire dans un délai de trente jours et fixant le pays de renvoi ; <br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler, dans cette mesure, l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              6. Considérant qu'il ressort des pièces du dossier que Mme B...a séjourné régulièrement en France du 4 février 2003 au 13 novembre 2011 sous couvert d'une carte de séjour temporaire portant la mention " étudiant ", puis, durant une période de dix-huit mois, sous couvert d'une autorisation temporaire provisoire de séjour délivrée sur le fondement des stipulations de l'article 2.2 de l'accord franco-gabonais du 5 juillet 2007 ; qu'elle justifiait ainsi, à la date de la décision par laquelle le préfet de la Haute-Garonne lui a fait obligation de quitter le territoire français dans un délai de trente jours et a fixé le pays de destination, avoir résidé régulièrement en France depuis plus de dix ans sans avoir été, durant toute cette période, titulaire d'une carte de séjour temporaire portant la mention " étudiant " ; que, par suite, et sans qu'il soit besoin d'examiner les autres moyens de la requête, Mme B...est fondée à soutenir qu'elle ne pouvait légalement faire l'objet d'une obligation de quitter le territoire français et à demander l'annulation de la décision attaquée ; <br/>
<br/>
              7. Considérant que Mme B...a obtenu le bénéfice de l'aide juridictionnelle ; que, par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ; qu'il y a lieu, dans les circonstances de l'espèce, et sous réserve que Maître A...Delamarre, avocat de Mme B..., renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de  l'Etat la somme de 3 000 euros à verser à Maître Delamarre ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Bordeaux du 19 novembre 2015 est annulé en  tant qu'il a rejeté les conclusions de Mme B...tendant à l'annulation de la décision du préfet de la Haute-Garonne du 31 décembre 2014 portant obligation de quitter le territoire dans un délai de trente jours et fixant le pays de renvoi. <br/>
Article 2 : L'arrêté du 31 décembre 2014 du préfet de la Haute-Garonne faisant obligation à Mme B...de quitter le territoire dans un délai de trente jours et fixant le pays de renvoi est annulé.<br/>
Article 3 : L'Etat versera à Maître A...Delamarre, avocat de MmeB..., une somme de 3 000 euros au titre du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991, sous réserve que cet avocat renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
Article 4 : La présente décision sera notifiée à Mme C...B...et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">335-03-02-01 ÉTRANGERS. OBLIGATION DE QUITTER LE TERRITOIRE FRANÇAIS (OQTF) ET RECONDUITE À LA FRONTIÈRE. LÉGALITÉ INTERNE. ÉTRANGERS NE POUVANT FAIRE L`OBJET D`UNE OQTF OU D`UNE MESURE DE RECONDUITE. - ETRANGER RÉSIDANT RÉGULIÈREMENT EN FRANCE DEPUIS PLUS DE DIX ANS, SAUF S'IL ÉTAIT TITULAIRE D'UNE CARTE DE SÉJOUR TEMPORAIRE PORTANT LA MENTION ÉTUDIANT (4° DE L'ART. L. 511-4 DU CESEDA) - 1) PORTÉE - 2) NOTION DE CARTE DE SÉJOUR TEMPORAIRE PORTANT LA MENTION ÉTUDIANT - AUTORISATION PROVISOIRE DE SÉJOUR DÉLIVRÉE SUR LE FONDEMENT DE L'ART. 2.2 DE L'ACCORD FRANCO-GABONAIS DU 5 JUILLET 2007 - EXCLUSION.
</SCT>
<ANA ID="9A"> 335-03-02-01 Dispositions du 4° de l'article L. 511-4 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA), en vertu desquelles ne peut faire l'objet d'une OQTF ou d'une mesure de reconduite à la frontière l'étranger qui réside régulièrement en France depuis plus de dix ans, sauf s'il a été, pendant toute cette période, titulaire d'une carte de séjour temporaire portant la mention étudiant.... ,,1) Il résulte de ces dispositions que les étrangers n'ayant obtenu de titre de séjour qu'en qualité d'étudiant, en vue de suivre en France un enseignement ou d'y faire des études, ne peuvent bénéficier de l'interdiction d'éloignement qu'elles prévoient.... ,,2) Il résulte des stipulations de l'article 2.2 de l'accord franco-gabonais du 5 juillet 2007 que l'autorisation provisoire de séjour délivrée sur ce fondement à un ressortissant gabonais a pour objet de lui permettre de rechercher et d'exercer un emploi afin d'obtenir une première expérience professionnelle, et non de suivre un enseignement ou des études en France. Par suite, un étranger ayant séjourné régulièrement sur le territoire français pendant plus de dix ans et ayant, durant cette période, bénéficié d'une autorisation de séjour provisoire d'une durée de neuf mois, renouvelée une fois pour la même durée, sur le fondement des stipulations de l'article 2.2 de l'accord franco-gabonais du 5 juillet 2007, peut se prévaloir des dispositions du 4° de l'article L. 511-4 du CESEDA.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
