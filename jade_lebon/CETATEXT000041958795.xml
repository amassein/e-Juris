<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041958795</ID>
<ANCIEN_ID>JG_L_2020_06_000000432172</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/95/87/CETATEXT000041958795.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 5ème chambres réunies, 03/06/2020, 432172</TITRE>
<DATE_DEC>2020-06-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>432172</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR>Mme Catherine Moreau</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier  Fuchs</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:432172.20200603</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 2 juillet 2019, 2 octobre 2019 et 4 mai 2020 au secrétariat du contentieux du Conseil d'Etat, M. A... B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret du Président de la République du 2 mai 2019 portant sa réintégration et sa radiation des cadres ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code civil ;<br/>
              - le code des juridictions financières ;<br/>
              - la loi n° 84-16 du 11 janvier 1984 ;<br/>
              - le décret n° 85-986 du 16 septembre 1985 ;<br/>
              - le décret n° 2014-1370 du 14 novembre 2014 ;<br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 modifiée ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Catherine Moreau, Conseiller d'Etat en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Olivier Fuchs, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Matuchansky, Poupot, Valdelièvre, avocat de M. B... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier que M. B..., nommé magistrat des chambres régionales des comptes le 1er avril 2000 à l'issue de sa scolarité à l'Ecole nationale d'administration, a été placé en disponibilité pour convenances personnelles du 1er avril 2002 au 1er avril 2005 puis du 1er avril 2005 au 7 novembre 2006 avant d'être placé en disponibilité pour création d'entreprise du 8 novembre 2006 au 7 novembre 2008 et à nouveau en disponibilité pour convenances personnelles du 8 novembre 2008 au 7 novembre 2009 puis du 8 novembre 2009 au 30 mars 2014, date à laquelle il ne pouvait plus bénéficier d'une autre mise en disponibilité. Par un courrier du 23 janvier 2019, M. B... a demandé sa réintégration dans son corps d'origine. Par une lettre du 27 février 2019, le Premier président de la Cour des comptes l'a informé de ce que, faute pour lui d'avoir demandé sa réintégration dans les délais légaux, sa radiation des cadres allait être proposée. Par un décret du 2 mai 2019, le Président de la République a réintégré pour ordre et radié des cadres M. B... et l'a soumis à l'obligation de versement de la somme prévue à l'article 1er du décret du 14 novembre 2014 relatif à la rupture de l'engagement de servir des anciens élèves de l'Ecole nationale d'administration.   <br/>
<br/>
              Sur la légalité externe du décret attaqué :<br/>
<br/>
              2. D'une part, si les dispositions de l'article 2 du décret du 14 novembre 2014 prévoient que la rupture de l'engagement de servir est constatée par l'autorité gestionnaire du corps, la radiation d'un magistrat des chambres régionales des comptes doit être prononcée par le Président de la République, autorité de nomination des membres de ce corps. Par suite, le moyen tiré de ce que le décret attaqué a été pris par une autorité incompétente ne peut qu'être écarté. <br/>
<br/>
              3. D'autre part, les dispositions des articles L. 220-12 et R. 226-7 du code des juridictions financières instituant un Conseil supérieur des chambres régionales des comptes ne prévoient pas que celui-ci donne son avis sur la radiation des cadres d'un magistrat. Par suite, l'absence de consultation du Conseil supérieur n'entache pas le décret attaqué, qui procède à la radiation des cadres de M. B..., d'un vice de procédure. <br/>
<br/>
<br/>
<br/>
              Sur la légalité interne du décret attaqué : <br/>
<br/>
              En ce qui concerne les conclusions dirigées contre le décret en tant qu'il prononce la radiation des cadres : <br/>
<br/>
              4. Aux termes de l'article 44 du décret du 16 septembre 1985 relatif au régime particulier de certaines positions de fonctionnaires de l'Etat et à certaines modalités de cessation définitive des fonctions, dans sa rédaction applicable au litige : " La mise en disponibilité sur demande de l'intéressé peut être accordée, sous réserve des nécessités du service, dans les cas suivants : (...) b) Pour convenances personnelles : la durée de la disponibilité ne peut, dans ce cas, excéder trois années ; elle est renouvelable mais la durée de la disponibilité ne peut excéder au total dix années pour l'ensemble de la carrière ". En vertu de l'article 46 du même décret : " La mise en disponibilité peut être également prononcée sur la demande du fonctionnaire, pour créer ou reprendre une entreprise au sens de l'article L. 351-24 du code du travail. / La mise en disponibilité prévue au présent article ne peut excéder deux années ". Le troisième alinéa de l'article 49 du même décret dispose que " Trois mois au moins avant l'expiration de la disponibilité, le fonctionnaire fait connaître à son administration d'origine sa décision de solliciter le renouvellement de la disponibilité ou de réintégrer son corps d'origine. Sous réserve des dispositions du deuxième alinéa du présent article et du respect par l'intéressé, pendant la période de mise en disponibilité, des obligations qui s'imposent à un fonctionnaire même en dehors du service, la réintégration est de droit. " L'article 24 de la loi du 13 juillet 1983 portant droits et obligations des fonctionnaires prévoit que la non-réintégration à l'issue d'une période de disponibilité entraîne la radiation des cadres. <br/>
<br/>
              5. En premier lieu, la période comprise entre le 1er avril 2002 et le 12 octobre 2003, pendant laquelle M. B... a bénéficié d'une disponibilité pour convenances personnelles afin d'exercer les fonctions de directeur général adjoint de l'Office national des forêts, établissement public à caractère industriel et commercial, qui l'a recruté par la voie d'un contrat de droit privé, ne saurait, contrairement à ce que soutient le requérant, être regardée comme une période de détachement. Le moyen tiré de ce que l'échéance au 30 mars 2014 des droits à disponibilité de l'intéressé serait erronée pour avoir inclus cette période du 1er avril 2002 au 12 octobre 2003 doit donc être écarté. <br/>
<br/>
              6. En deuxième lieu, si M. B... soutient qu'il ne pouvait être tenu compte, pour le calcul de la durée de sa disponibilité, de la période du 7 novembre 2008 au 30 mars 2014, dès lors qu'il n'aurait pas été informé de la fin de sa disponibilité pour création d'entreprise, entamée le 8 novembre 2006, et de ce qu'il avait alors à nouveau été placé en disponibilité pour convenances personnelles, il ressort cependant des pièces du dossier, notamment d'un courrier du 6 mai 2009, que c'est le requérant lui-même qui a sollicité, à la suite de plusieurs courriers lui demandant de régulariser sa situation, une disponibilité pour création d'entreprise du 8 novembre 2006 au 7 novembre 2008, puis, cette disponibilité étant limitée à une durée de deux ans, une nouvelle disponibilité pour convenances personnelles à compter du 8 novembre 2008. <br/>
<br/>
              7. Il ressort en outre des pièces du dossier que le Premier président de la Cour des comptes, par des lettres en date du 29 mai 2006, 31 décembre 2009 et 6 juillet 2010, l'a informé de la durée de ses droits à disponibilité restants et lui a rappelé, par un courrier du 7 avril 2011, l'obligation de faire connaître, trois mois avant le terme de la période de disponibilité, ses intentions d'être réintégré ou bien d'être radié des cadres, conformément à l'article 49 du décret du 16 septembre 1985 précité. Si M. B... soutient qu'il a demandé à être réintégré dans son corps d'origine, il se borne à invoquer des contacts avec l'administration au début de l'année 2014. Dès lors, les moyens tirés de ce qu'il n'aurait pas été informé de ce que la période de disponibilité pour convenances personnelles était susceptible d'arriver à expiration et de ce que le décret attaqué méconnaitrait l'article 49 du décret du 16 septembre 1985 faute d'avoir procédé à la réintégration qu'il avait demandée manquent en fait.<br/>
<br/>
              8. En troisième lieu, les décisions administratives ne pouvant légalement disposer que pour l'avenir, l'administration ne peut, par dérogation à cette règle, prendre des mesures à portée rétroactive que pour assurer la continuité de la carrière d'un agent public ou procéder à la régularisation de sa situation. Or, à la date du décret attaqué, M. B..., qui ne pouvait ni être maintenu en disponibilité, ni être réintégré dans son corps d'origine, se trouvait en situation irrégulière depuis le 30 mars 2014. Le moyen tiré de ce que ce décret, qui régularise la situation de M. B..., serait entaché d'une rétroactivité illégale doit donc être écarté. <br/>
<br/>
              En ce qui concerne les conclusions dirigées contre le décret en tant qu'il soumet le requérant au versement de l'indemnité pour rupture de l'engagement de servir :<br/>
<br/>
              9. Aux termes de l'article 2224 du code civil, dans sa rédaction issue de la loi du 17 juin 2008 portant réforme de la prescription en matière civile : " Les actions personnelles ou mobilières se prescrivent par cinq ans à compter du jour où le titulaire d'un droit a connu ou aurait dû connaître les faits lui permettant de l'exercer. " L'article 1er du décret du 14 novembre 2014 relatif à la rupture de l'engagement de servir des anciens élèves de l'Ecole nationale d'administration dispose que " En cas de rupture de l'engagement qu'ils ont signé (...), les anciens élèves de l'Ecole nationale d'administration versent une somme dont le montant est égal à deux fois le traitement net perçu durant les douze derniers mois de service ". <br/>
<br/>
              10. Il ressort des pièces du dossier que l'administration a eu connaissance le 30 mars 2014, au plus tard, de l'épuisement des droits à disponibilité pour convenances personnelles de M. B... et de la rupture de l'engagement de servir de celui-ci consécutive à son absence de demande de réintégration dans son corps d'origine. En vertu de l'article 2224 du code civil cité ci-dessus, l'administration disposait d'un délai de cinq ans pour le soumettre à l'obligation de versement de cette indemnité. Le décret du 2 mai 2019 ayant été pris après l'expiration de ce délai, la prescription quinquennale fait obstacle à ce que soit mis à la charge de M. B... le versement de cette indemnité. Par suite, M. B... est fondé à soutenir que cette créance est prescrite. <br/>
<br/>
              11. Il résulte de tout ce qui précède et sans qu'il soit besoin d'examiner les autres moyens présentés à l'appui de ces conclusions, que le requérant est fondé à demander l'annulation du décret du 2 mai 2019 en tant qu'il le soumet au versement de l'indemnité pour rupture de l'engagement de servir. <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Sur les conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              12. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat, la somme de 1 500 euros à verser à M. B..., au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le deuxième alinéa de l'article 1er du décret du 2 mai 2019 est annulé. <br/>
Article 2 : Le surplus des conclusions de M. A... B... est rejeté. <br/>
Article 3 : L'Etat versera à M. A... B... une somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à Monsieur A... B..., au Premier ministre et au Premier président de la Cour des comptes.  <br/>
Copie en sera adressée au ministre de l'économie et des finances et au ministre de l'action et des comptes publics.  <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-07-11-005 FONCTIONNAIRES ET AGENTS PUBLICS. STATUTS, DROITS, OBLIGATIONS ET GARANTIES. OBLIGATIONS DES FONCTIONNAIRES. ENGAGEMENT DE SERVIR L'ÉTAT. - INDEMNITÉ DUE PAR LES ANCIENS ÉLÈVES DE L'ENA EN CAS DE RUPTURE DE LEUR ENGAGEMENT DE SERVIR -APPLICATION DE LA PRESCRIPTION QUINQUENNALE (ART. 2224 DU CODE CIVIL) [RJ1] - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">36-10-08 FONCTIONNAIRES ET AGENTS PUBLICS. CESSATION DE FONCTIONS. DÉMISSION. - INDEMNITÉ DUE PAR LES ANCIENS ÉLÈVES DE L'ENA EN CAS DE RUPTURE DE LEUR ENGAGEMENT DE SERVIR -APPLICATION DE LA PRESCRIPTION QUINQUENNALE (ART. 2224 DU CODE CIVIL) [RJ1] - EXISTENCE.
</SCT>
<ANA ID="9A"> 36-07-11-005 En vertu de l'article 2224 du code civil dans sa rédaction issue de la loi n° 2008-561 du 17 juin 2008, l'obligation de verser l'indemnité relative à la rupture de l'engagement de servir des anciens élèves de l'Ecole nationale d'administration (ENA) se prescrit par cinq ans à compter de la date à laquelle l'administration a eu connaissance de la rupture de l'engagement de servir du fonctionnaire, notamment en raison de l'absence de demande de réintégration dans son corps d'origine à l'épuisement de ses droits à disponibilité pour convenances personnelles.</ANA>
<ANA ID="9B"> 36-10-08 En vertu de l'article 2224 du code civil dans sa rédaction issue de la loi n° 2008-561 du 17 juin 2008, l'obligation de verser l'indemnité relative à la rupture de l'engagement de servir des anciens élèves de l'Ecole nationale d'administration (ENA) se prescrit par cinq ans à compter de la date à laquelle l'administration a eu connaissance de la rupture de l'engagement de servir du fonctionnaire, notamment en raison de l'absence de demande de réintégration dans son corps d'origine à l'épuisement de ses droits à disponibilité pour convenances personnelles.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., sur l'application de la prescription quinquennale de l'article 2224 du code civil à la répétition de l'indu en matière de rémunérations, CE, 28 mai 2014, M.,et M.,, n°s 376501 376573, p. 143 ; en matière de pensions, CE, 20 septembre 2019, Mme,, n° 420489, à mentionner aux Tables. Comp., sur l'application de la prescription trentenaire sous l'empire du droit antérieur, CE, 22 février 2006,,, n° 258555, T. pp. 708-890-925-930.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
