<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042994575</ID>
<ANCIEN_ID>JG_L_2021_01_000000441265</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/99/45/CETATEXT000042994575.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 15/01/2021, 441265</TITRE>
<DATE_DEC>2021-01-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>441265</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SEVAUX, MATHONNET</AVOCATS>
<RAPPORTEUR>M. Laurent Roulaud</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Domingo</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:441265.20210115</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête enregistrée le 17 juin 2020 au secrétariat du contentieux du Conseil d'Etat, la Confédération générale du travail, la Confédération générale du travail - Force ouvrière, la Fédération syndicale unitaire, l'Union syndicale Solidaires, le Syndicat de la magistrature, le Syndicat des avocats de France, l'Union nationale des étudiants de France et la Fédération Droit au logement demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir les dispositions du I et du II bis, dans leur version issue du décret du 14 juin 2020, et du V de l'article 3 du décret du 31 mai 2020, en tant qu'elles s'appliquent aux manifestations sur la voie publique soumises à l'obligation d'une déclaration préalable en vertu de l'article L. 211-1 du code de la sécurité intérieure ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
           Vu les autres pièces du dossier ; <br/>
<br/>
           Vu :<br/>
           - la Constitution ;<br/>
           - le code pénal ;<br/>
           - le code de la santé publique ;<br/>
           - le code de la sécurité intérieure ;<br/>
           - la loi n° 2020-290 du 23 mars 2020 ;<br/>
           - la loi n° 2020-546 du 11 mai 2020 ;<br/>
           - le décret n° 2020-663 du 31 mai 2020 ;<br/>
           - le décret n° 2020-724 du 14 juin 2020 ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
<br/>
                    Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Laurent Roulaud, maître des requêtes en service extraordinaire,  <br/>
<br/>
                    - les conclusions de M. Laurent Domingo, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Sevaux, Mathonnet, avocat de la Confédération générale du travail et autres ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              Sur le cadre juridique du litige :<br/>
<br/>
              1. La loi du 23 mars 2020 d'urgence pour faire face à l'épidémie de covid-19 a introduit dans le titre III du livre Ier de la troisième partie du code de la santé publique un chapitre Ier bis relatif à l'état d'urgence sanitaire, comprenant les articles L. 3131-12 à<br/>
L. 3131-20. Aux termes de l'article L. 3131-12 : " L'état d'urgence sanitaire peut être déclaré sur tout ou partie du territoire (...) en cas de catastrophe sanitaire mettant en péril, par sa nature et sa gravité, la santé de la population. " L'article L. 3131-15 du code de la santé publique dispose, dans sa rédaction issue de la loi du 11 mai 2020, que : " I. - Dans les circonscriptions territoriales où l'état d'urgence sanitaire est déclaré, le Premier ministre peut, par décret réglementaire pris sur le rapport du ministre chargé de la santé, aux seules fins de garantir la santé publique :/ (...) 6° Limiter ou interdire les rassemblements sur la voie publique ainsi que les réunions de toute nature ; (...) III. - Les mesures prescrites en application du présent article sont strictement proportionnées aux risques sanitaires encourus et appropriées aux circonstances de temps et de lieu. Il y est mis fin sans délai lorsqu'elles ne sont plus nécessaires. ". <br/>
<br/>
              	2. Dans ce cadre, l'article 1er du décret du 31 mai 2020 prescrivant les mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire a défini au niveau national des règles d'hygiène et de distanciation physique, dites " mesures barrières ", et prévu notamment que les rassemblements, réunions, et déplacements qui n'étaient pas interdits en vertu de ce décret devaient être organisés en veillant au strict respect de ces mesures. L'article 3 du décret du 31 mai 2020 prévoit, à son I, que :<br/>
" Tout rassemblement, réunion ou activité à un titre autre que professionnel sur la voie publique ou dans un lieu public, mettant en présence de manière simultanée plus de dix personnes, est interdit sur l'ensemble du territoire de la République. Lorsqu'il n'est pas interdit par l'effet de ces dispositions, il est organisé dans les conditions de nature à permettre le respect des dispositions de l'article 1er ". Son V prévoit qu'aucun évènement réunissant plus de 5 000 personnes ne peut se dérouler sur le territoire de la République jusqu'au 31 août 2020. <br/>
<br/>
              3. Par une ordonnance du 13 juin 2020, le juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, a, sans prononcer aucune injonction à l'égard de l'administration, suspendu l'exécution des dispositions du I de l'article 3 du décret du 31 mai 2020, mentionnées au point précédent, en tant que l'interdiction qu'elles édictaient s'appliquait aux manifestations sur la voie publique soumises à l'obligation d'une déclaration préalable en vertu de l'article L. 211-1 du code de la sécurité intérieure. Le Premier ministre a, le lendemain, par un décret du 14 juin 2020, modifié l'article 3 du décret du 31 mai 2020. Le 1° de l'article 1er du décret du 14 juin 2020, d'une part, réitère les dispositions du I en étendant le champ de l'interdiction aux rassemblements professionnels et, d'autre part, insère un II bis disposant que : " II bis. - Par dérogation aux dispositions du I et sans préjudice de l'article L. 211-3 du code de la sécurité intérieure, les cortèges, défilés et rassemblements de personnes, et, d'une façon générale, toutes les manifestations sur la voie publique mentionnées au premier alinéa de l'article L. 211-1 du même code sont autorisés par le préfet de département si les conditions de leur organisation sont propres à garantir le respect des dispositions de l'article 1er du présent décret. / Pour l'application des dispositions de l'alinéa précédent, les organisateurs de la manifestation adressent au préfet du département sur le territoire duquel celle-ci doit avoir lieu la déclaration prévue par les dispositions de l'article<br/>
L. 211-2 du code de la sécurité intérieure, dans les conditions fixées à cet article, assortie des conditions d'organisation mentionnées à l'alinéa précédent. Cette déclaration tient lieu de demande d'autorisation. ". Par une ordonnance du 6 juillet 2020, le juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2, a, sans prononcer aucune injonction à l'égard de l'administration, suspendu l'exécution de ces dispositions du I et du II bis de l'article 3 du décret du 31 mai 2020, en tant qu'elles s'appliquent aux manifestations sur la voie publique soumises à l'obligation d'une déclaration préalable en vertu de l'article L. 211-1 du code de la santé publique. Le décret du 31 mai 2020 a été abrogé par l'article 52 du décret du 11 juillet 2020.<br/>
<br/>
              4. La Confédération générale du travail et autres demandent au Conseil d'Etat d'annuler pour excès de pouvoir les dispositions du I et du II bis, dans leur version issue du décret du 14 juin 2020, et du V de l'article 3 du décret du 31 mai 2020, en tant qu'elles s'appliquent aux manifestations sur la voie publique mentionnées à l'article L. 211-1 du code de la sécurité intérieure.<br/>
<br/>
              Sur les conclusions relatives au I et au II bis de l'article 3 du décret du 31 mai 2020 :<br/>
<br/>
              5. Par les dispositions citées au point 1, le législateur a institué une police spéciale donnant aux autorités de l'Etat mentionnées aux articles L. 3131-15 à L. 3131-17  compétence pour édicter, dans le cadre de l'état d'urgence sanitaire, les mesures générales ou individuelles visant à mettre fin à une catastrophe sanitaire telle que l'épidémie de covid-19, en vue, notamment, d'assurer, compte tenu des données scientifiques disponibles, leur cohérence et leur efficacité sur l'ensemble du territoire concerné et de les adapter en fonction de l'évolution de la situation. Si le Premier ministre peut, en vertu des pouvoirs qu'il tient du 6° du I de l'article L. 3131-15 du code de la santé publique, aux fins de garantir la santé publique, réglementer les rassemblements sur la voie publique ainsi que les réunions de toute nature et, le cas échéant, les interdire, il ne pouvait légalement, sans qu'une disposition législative lui ait donné compétence à cette fin, subordonner les manifestations sur la voie publique à un régime d'autorisation. Par suite, les requérants sont fondés à demander l'annulation des dispositions attaquées, qui prévoient un tel régime. <br/>
<br/>
              Sur les conclusions relatives au V de l'article 3 du décret du 31 mai 2020 :<br/>
<br/>
              6. Il résulte des dispositions attaquées du V de l'article 3 du décret du 31 mai 2020 que le Premier ministre a interdit jusqu'au 31 août 2020 tout événement de plus de<br/>
5 000 personnes sur le territoire de la République. S'il ressort des pièces du dossier que les rassemblements de masse peuvent amplifier la transmission du virus SARS-CoV-2 et que le risque de transmission semble être lié à la fréquence des interactions entre un individu infecté et un individu non infecté, cette interdiction, qui présente un caractère général et absolu, ne peut être regardée, bien que temporaire, comme une mesure nécessaire et adaptée et, ainsi, proportionnée à l'objectif de préservation de la santé publique qu'elle poursuit, d'une part, en ce qu'elle s'applique aux manifestations sur la voie publique, soumises par ailleurs à l'obligation d'une déclaration préalable en vertu de l'article L. 211-1 du code de la sécurité intérieure et susceptibles d'être interdites, en application de l'article L. 211-4 du même code, par l'autorité investie des pouvoirs de police ou, à défaut, par le représentant de l'Etat dans le département, si elle estime, notamment au vu des informations que comporte cette déclaration ou à l'occasion des échanges avec les organisateurs qu'elle peut susciter, y compris quant aux précautions sanitaires envisagées, qu'elle est de nature à troubler l'ordre public, dont la sécurité et la salubrité publique sont des composantes et, d'autre part, en ce qu'elles prévoient un seuil dont le respect ne peut être efficacement vérifié pour les manifestations sur la voie publique. Par suite, les requérants sont fondés à soutenir que le V de l'article 3 du décret du 31 mai 2020 porte à la liberté de manifester une atteinte qui n'est ni nécessaire, ni adaptée ni proportionnée.<br/>
<br/>
              7. Il résulte de tout ce qui précède que les requérants sont fondés, sans qu'il soit besoin de se prononcer sur les autres moyens de la requête, à demander l'annulation des dispositions du I et du II bis, dans leur version issue du décret du 14 juin 2020, et du V de l'article 3 du décret du 31 mai 2020, en tant qu'elles s'appliquent aux manifestations sur la voie publique mentionnées par l'article L. 211-1 du code de la sécurité intérieure. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme globale de 3 000 euros à verser à la Confédération générale du travail et autres au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Les dispositions du I et du II bis, dans leur version issue du décret n° 2020-724 du<br/>
14 juin 2020, et du V de l'article 3 du décret n° 2020-663 du 31 mai 2020 sont annulées en tant qu'elles s'appliquent aux manifestations sur la voie publique mentionnées par l'article L. 211-1 du code de la sécurité intérieure.<br/>
Article 2 : L'Etat versera une somme globale de 3 000 euros à la Confédération générale du travail, à la Confédération générale du travail - Force ouvrière, à la Fédération syndicale unitaire, à l'Union syndicale Solidaires, au Syndicat de la magistrature, au Syndicat des avocats de France, à l'Union nationale des étudiants de France et à la Fédération Droit au logement au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
Article 3 : La présente décision sera notifiée à la Confédération générale du travail, premier requérant dénommé, au Premier ministre, au ministre de l'intérieur et au ministre des solidarités et de la santé. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-02-02-01-02 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - COMPÉTENCE. RÉPARTITION DES COMPÉTENCES ENTRE AUTORITÉS DISPOSANT DU POUVOIR RÉGLEMENTAIRE. AUTORITÉS DISPOSANT DU POUVOIR RÉGLEMENTAIRE. PREMIER MINISTRE. - ETAT D'URGENCE SANITAIRE (ART. L. 3131-12 DU CSP) - COMPÉTENCE DU PREMIER MINISTRE POUR SUBORDONNER LES MANIFESTATIONS SUR LA VOIE PUBLIQUE À UN RÉGIME D'AUTORISATION - ABSENCE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">49-05-02 POLICE. POLICES SPÉCIALES. POLICE SANITAIRE (VOIR AUSSI : SANTÉ PUBLIQUE). - ETAT D'URGENCE SANITAIRE (ART. L. 3131-12 DU CSP) - COMPÉTENCE DU PREMIER MINISTRE POUR SUBORDONNER LES MANIFESTATIONS SUR LA VOIE PUBLIQUE À UN RÉGIME D'AUTORISATION - ABSENCE [RJ1].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">61-01-01-02 SANTÉ PUBLIQUE. PROTECTION GÉNÉRALE DE LA SANTÉ PUBLIQUE. POLICE ET RÉGLEMENTATION SANITAIRE. LUTTE CONTRE LES ÉPIDÉMIES. - ETAT D'URGENCE SANITAIRE (ART. L. 3131-12 DU CSP) - COMPÉTENCE DU PREMIER MINISTRE POUR SUBORDONNER LES MANIFESTATIONS SUR LA VOIE PUBLIQUE À UN RÉGIME D'AUTORISATION - ABSENCE [RJ1].
</SCT>
<ANA ID="9A"> 01-02-02-01-02 Par les articles L. 3131-12 et L. 3131-15 du code de la santé publique (CSP), dans leur version issue de la loi n° 2020-290 du 23 mars 2020, le législateur a institué une police spéciale donnant aux autorités de l'Etat mentionnées aux articles L. 3131-15 à L. 3131-17 compétence pour édicter, dans le cadre de l'état d'urgence sanitaire, les mesures générales ou individuelles visant à mettre fin à une catastrophe sanitaire telle que l'épidémie de covid-19, en vue, notamment, d'assurer, compte tenu des données scientifiques disponibles, leur cohérence et leur efficacité sur l'ensemble du territoire concerné et de les adapter en fonction de l'évolution de la situation.,,,Si le Premier ministre peut, en vertu des pouvoirs qu'il tient du 6° du I de l'article L. 3131-15 du CSP, aux fins de garantir la santé publique, réglementer les rassemblements sur la voie publique ainsi que les réunions de toute nature et, le cas échéant, les interdire, il ne pouvait légalement, sans qu'une disposition législative lui ait donné compétence à cette fin, subordonner les manifestations sur la voie publique à un régime d'autorisation.... ,,Par suite, annulation des dispositions qui prévoient un tel régime dans le décret n° 2020-663 du 31 mai 2020 prescrivant, à l'issue de la première période de confinement, les mesures générales nécessaires pour faire face à l'épidémie de covid-19.</ANA>
<ANA ID="9B"> 49-05-02 Par les articles L. 3131-12 et L. 3131-15 du code de la santé publique (CSP), dans leur version issue de la loi n° 2020-290 du 23 mars 2020, le législateur a institué une police spéciale donnant aux autorités de l'Etat mentionnées aux articles L. 3131-15 à L. 3131-17 compétence pour édicter, dans le cadre de l'état d'urgence sanitaire, les mesures générales ou individuelles visant à mettre fin à une catastrophe sanitaire telle que l'épidémie de covid-19, en vue, notamment, d'assurer, compte tenu des données scientifiques disponibles, leur cohérence et leur efficacité sur l'ensemble du territoire concerné et de les adapter en fonction de l'évolution de la situation.,,,Si le Premier ministre peut, en vertu des pouvoirs qu'il tient du 6° du I de l'article L. 3131-15 du CSP, aux fins de garantir la santé publique, réglementer les rassemblements sur la voie publique ainsi que les réunions de toute nature et, le cas échéant, les interdire, il ne pouvait légalement, sans qu'une disposition législative lui ait donné compétence à cette fin, subordonner les manifestations sur la voie publique à un régime d'autorisation.... ,,Par suite, annulation des dispositions qui prévoient un tel régime dans le décret n° 2020-663 du 31 mai 2020 prescrivant, à l'issue de la première période de confinement, les mesures générales nécessaires pour faire face à l'épidémie de covid-19.</ANA>
<ANA ID="9C"> 61-01-01-02 Par les articles L. 3131-12 et L. 3131-15 du code de la santé publique (CSP), dans leur version issue de la loi n° 2020-290 du 23 mars 2020, le législateur a institué une police spéciale donnant aux autorités de l'Etat mentionnées aux articles L. 3131-15 à L. 3131-17 compétence pour édicter, dans le cadre de l'état d'urgence sanitaire, les mesures générales ou individuelles visant à mettre fin à une catastrophe sanitaire telle que l'épidémie de covid-19, en vue, notamment, d'assurer, compte tenu des données scientifiques disponibles, leur cohérence et leur efficacité sur l'ensemble du territoire concerné et de les adapter en fonction de l'évolution de la situation.,,,Si le Premier ministre peut, en vertu des pouvoirs qu'il tient du 6° du I de l'article L. 3131-15 du CSP, aux fins de garantir la santé publique, réglementer les rassemblements sur la voie publique ainsi que les réunions de toute nature et, le cas échéant, les interdire, il ne pouvait légalement, sans qu'une disposition législative lui ait donné compétence à cette fin, subordonner les manifestations sur la voie publique à un régime d'autorisation.... ,,Par suite, annulation des dispositions qui prévoient un tel régime dans le décret n° 2020-663 du 31 mai 2020 prescrivant, à l'issue de la première période de confinement, les mesures générales nécessaires pour faire face à l'épidémie de covid-19.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., sur l'impossibilité pour une autorité administrative de police, si la loi ne le permet pas, de subordonner une activité à un régime d'autorisation préalable, s'agissant des manifestations sur la voie publique, CE, Section, 4 février 1938, Abbé Nicolet, n° 56293, p. 128 ; dans le domaine de la liberté du commerce et de l'industrie, CE, Assemblée, 22 juin 1951, Daudignac, n°s 590 2251, p. 362.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
