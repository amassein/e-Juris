<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043289885</ID>
<ANCIEN_ID>JG_L_2021_03_000000421065</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/28/98/CETATEXT000043289885.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 6ème chambres réunies, 24/03/2021, 421065</TITRE>
<DATE_DEC>2021-03-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>421065</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN ; BROUCHOT</AVOCATS>
<RAPPORTEUR>M. Joachim Bendavid</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:421065.20210324</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B... C... a demandé au tribunal administratif de Toulouse, d'une part, d'annuler pour excès de pouvoir la décision du 10 janvier 2014 par laquelle le centre hospitalier universitaire (CHU) de Toulouse a refusé son reclassement sur un emploi sédentaire et l'a informée de sa mise à la retraite par application de la limite d'âge à compter du 30 janvier 2014, la décision du 29 janvier 2014 par laquelle le même établissement l'a mise à la retraite et la décision de rejet implicite de sa demande du 4 février 2015 et, d'autre part, d'enjoindre au CHU de Toulouse de procéder à son reclassement professionnel et de reconstituer sa carrière à compter du 29 janvier 2014. Par un jugement n°s 1401524, 1502509 du 16 septembre 2016, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 16BX03662 du 3 avril 2018, la cour administrative d'appel de Bordeaux a, sur appel de Mme C..., annulé ce jugement et les décisions attaquées, enjoint au CHU de Toulouse de réintégrer Mme C... à compter du 1er février 2014, de reconstituer sa carrière et d'examiner sa demande de reclassement sur un emploi sédentaire dans un délai de deux mois.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 30 mai 2018, 23 août 2018 et 16 juillet 2019 au secrétariat du contentieux du Conseil d'Etat, le CHU de Toulouse demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de Mme C... ; <br/>
<br/>
              3°) de mettre à la charge de Mme C... la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
<br/>
              - le code de la santé publique ;<br/>
              - la loi du 18 août 1936 concernant les mises à la retraite par ancienneté ;<br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - la loi n° 86-33 du 9 janvier 1986 ;<br/>
              - la loi n° 2010-1330 du 9 novembre 2010 ;<br/>
              - le décret n° 2007-1188 du 3 août 2007 ;<br/>
              - l'arrêté du 12 novembre 1969 relatif au classement des emplois des agents des collectivités locales en catégories A et B ; <br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Joachim Bendavid, auditeur,  <br/>
<br/>
              - les conclusions de Mme A... D..., rapporteure publique.<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Waquet, Farge, Hazan, avocat du centre hospitalier universitaire de Toulouse et à Me Brouchot, avocat de Mme C....<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, saisi par Mme C..., auxiliaire de puériculture, d'une demande de reclassement sur un poste sédentaire présentée sur le fondement de l'article 71 de la loi du 9 janvier 1986 portant dispositions statuaires relatives à la fonction publique hospitalière, le centre hospitalier universitaire (CHU) de Toulouse a, le 10 janvier 2014, rejeté sa demande au motif qu'elle atteindrait la limite d'âge de son emploi le jour de ses soixante ans, le 30 janvier 2014. Par une décision du 29 janvier 2014, le CHU de Toulouse l'a radiée des cadres à compter du 30 janvier 2014. Enfin, le CHU de Toulouse a implicitement rejeté la demande présentée le 4 février 2015 par Mme C... tendant au retrait de ces décisions, à sa prolongation d'activité et à sa réintégration sur un poste sédentaire en qualité de titulaire. Le CHU de Toulouse se pourvoit en cassation contre l'arrêt du 3 avril 2018 par lequel la cour administrative d'appel de Bordeaux a, sur appel de Mme C..., annulé ces trois décisions ainsi que le jugement du 16 septembre 2016 du tribunal administratif de Toulouse rejetant la demande d'annulation présentée par Mme C....<br/>
<br/>
              2. En premier lieu, le statut particulier du cadre d'emplois des auxiliaires de puériculture, qui relèvent du corps des aides-soignants dont les statuts sont fixés par le décret du 3 août 2007 portant statut particulier du corps des aides-soignants et des agents des services hospitaliers qualifiés de la fonction publique hospitalière, ne comporte aucune disposition relative à la limite d'âge. <br/>
<br/>
              3. Si aucune limite d'âge n'est déterminée par le statut particulier du cadre d'emplois auquel appartient un agent de la fonction publique hospitalière, la limite d'âge qui lui est applicable est celle que ne peuvent pas dépasser les agents de la fonction publique hospitalière occupant les emplois classés dans la même catégorie que l'emploi qu'il occupe, à savoir : soit la catégorie A (catégorie dite " sédentaire "), soit la catégorie B (catégorie dite " active "), au sens des dispositions de l'article 1er de la loi du 18 août 1936 concernant les mises à la retraite par ancienneté. <br/>
<br/>
              4. En second lieu, il résulte des dispositions de l'arrêté du 12 novembre 1969 relatif au classement des emplois des agents des collectivités locales en catégories A et B que les auxiliaires de puériculture occupant des postes qui les conduisent nécessairement à collaborer aux soins infirmiers bénéficient du classement en catégorie B (" catégorie active "). Dans ces conditions, il résulte de ce qui a été dit au point précédent que la seule limite d'âge qui puisse être appliquée aux agents occupant, comme Mme C..., un emploi d'auxiliaire de puériculture de la fonction publique hospitalière conduisant à collaborer aux soins infirmiers est celle que ne peuvent pas dépasser les agents de la fonction publique hospitalière occupant un emploi classé en catégorie B (" catégorie active ").<br/>
<br/>
              5. Aux termes de l'article 28 de la loi du 9 novembre 2010 portant réforme des retraites : " I. _ Pour les fonctionnaires relevant de la loi n° 83-634 du 13 juillet 1983 précitée dont la limite d'âge était de soixante-cinq ans en application des dispositions législatives et réglementaires antérieures à l'entrée en vigueur de la présente loi et nés à compter du 1er janvier 1956, la limite d'âge est fixée à soixante-sept ans. / II. _ Cette limite d'âge est fixée par décret dans la limite de l'âge mentionné au I pour les fonctionnaires atteignant avant le 1er janvier 2015 l'âge d'ouverture du droit à une pension de retraite applicable antérieurement à la présente loi et, pour ceux atteignant cet âge entre le 1er juillet 2011 et le 31 décembre 2014, de manière croissante à raison : / 1° De quatre mois par génération pour les fonctionnaires atteignant cet âge entre le 1er juillet et le 31 décembre 2011 ; / 2° De cinq mois par génération pour les fonctionnaires atteignant cet âge entre le 1er janvier 2012 et le 31 décembre 2014. / (...) ". Aux termes du I de l'article 31 de la même loi : " I. _ Pour les fonctionnaires relevant de la loi n° 83-634 du 13 juillet 1983 précitée dont la limite d'âge est inférieure à soixante-cinq ans en application des dispositions législatives et réglementaires antérieures à l'entrée en vigueur de la présente loi, la limite d'âge est fixée : / 1° A cinquante-sept ans lorsque cette limite d'âge était fixée antérieurement à cinquante-cinq ans, pour les agents nés à compter du 1er janvier 1965 ; / 2° A cinquante-neuf ans lorsque cette limite d'âge était fixée antérieurement à cinquante-sept ans, pour les agents nés à compter du 1er janvier 1963 ; / 3° A soixante ans lorsque cette limite d'âge était fixée antérieurement à cinquante-huit ans, pour les agents nés à compter du 1er janvier 1962 ; / 4° A soixante et un ans lorsque cette limite d'âge était fixée antérieurement à cinquante-neuf ans, pour les agents nés à compter du 1er janvier 1961 ; / 5° A soixante-deux ans lorsque cette limite d'âge était fixée antérieurement à soixante ans, pour les agents nés à compter du 1er janvier 1960 ; / 6° A soixante-quatre ans lorsque cette limite d'âge était fixée antérieurement à soixante-deux ans, pour les agents nés à compter du 1er janvier 1958 ". Il résulte de ces dispositions, éclairées par les travaux préparatoires dont elles sont issues, que le législateur a entendu régir l'ensemble des fonctionnaires de la catégorie A, dite " sédentaire ", par les dispositions de l'article 28 et l'ensemble des fonctionnaires de la catégorie B, dite " active ", par les dispositions de l'article 31. Il résulte également des mêmes travaux préparatoires que, s'agissant des agents de la fonction publique hospitalière, le législateur a entendu fixer la nouvelle limite d'âge maximale applicable aux agents occupant un emploi de catégorie B, dite " active ", à soixante-deux ans.<br/>
<br/>
              6. Par suite, en se fondant, pour déterminer la limite d'âge applicable à Mme C..., sur les dispositions de l'article 28 de la loi du 9 novembre 2010, alors qu'il lui appartenait, dès lors que l'intéressée occupait un emploi relevant, ainsi qu'il a été dit au point 4, de la catégorie B dite " active ", de faire application des seules dispositions de l'article 31 de cette même loi, lesquelles doivent être regardées comme ayant fixé à soixante-deux ans la plus haute limite d'âge applicable aux agents de la fonction publique hospitalière occupant un emploi de cette catégorie, la cour administrative d'appel de Bordeaux a commis une erreur de droit. <br/>
<br/>
              7. Il résulte de tout ce qui précède que le CHU de Toulouse est fondé à demander l'annulation de l'arrêt qu'il attaque. <br/>
<br/>
              8. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de Mme C... la somme que demande le CHU de Toulouse au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Bordeaux du 3 avril 2018 est annulé. <br/>
<br/>
		Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Bordeaux.<br/>
<br/>
Article 3 : Le surplus des conclusions du CHU de Toulouse présenté au titre de l'article L. 761-1 du code de justice administrative est rejeté. <br/>
<br/>
Article 4 : La présente décision sera notifiée au centre hospitalier universitaire de Toulouse et à Mme B... C....<br/>
Copie en sera adressée à la ministre de la transformation et de la fonction publique.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-10 FONCTIONNAIRES ET AGENTS PUBLICS. CESSATION DE FONCTIONS. - FONCTION PUBLIQUE HOSPITALIÈRE [RJ1] - 1) CAS D'UNE ABSENCE DE LIMITE D'ÂGE DANS LE STATUT PARTICULIER - CONSÉQUENCE - APPLICATION DES LIMITES D'ÂGE MAXIMALES DE LA FONCTION PUBLIQUE HOSPITALIÈRE - DISTINCTION ENTRE CATÉGORIES ACTIVE ET SÉDENTAIRE - 2) ARTICLES DE LA LOI DU 9 NOVEMBRE 2010 RÉGISSANT CHACUNE DE CES CATÉGORIES - 3) AGENT OCCUPANT UN EMPLOI DE LA CATÉGORIE ACTIVE - LIMITE D'ÂGE MAXIMALE FIXÉE À 62 ANS.[RJ1]
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">36-11 FONCTIONNAIRES ET AGENTS PUBLICS. DISPOSITIONS PROPRES AUX PERSONNELS HOSPITALIERS. - LIMITES D'ÂGE [RJ1] - 1) CAS D'UNE ABSENCE DE LIMITE D'ÂGE DANS LE STATUT PARTICULIER - CONSÉQUENCE - APPLICATION DES LIMITES D'ÂGE MAXIMALES DE LA FONCTION PUBLIQUE HOSPITALIÈRE - DISTINCTION ENTRE CATÉGORIES ACTIVE ET SÉDENTAIRE - 2) ARTICLES DE LA LOI DU 9 NOVEMBRE 2010 RÉGISSANT CHACUNE DE CES CATÉGORIES - 3) AGENT OCCUPANT UN EMPLOI DE LA CATÉGORIE ACTIVE - LIMITE D'ÂGE MAXIMALE FIXÉE À 62 ANS.
</SCT>
<ANA ID="9A"> 36-10 1) Si aucune limite d'âge n'est déterminée par le statut particulier du cadre d'emplois auquel appartient un agent de la fonction publique hospitalière, la limite d'âge qui lui est applicable est celle que ne peuvent pas dépasser les agents de la fonction publique hospitalière occupant les emplois classés dans la même catégorie que l'emploi qu'il occupe, à savoir : soit la catégorie A (catégorie dite sédentaire), soit la catégorie B (catégorie dite active), au sens des dispositions de l'article 1er de la loi du 18 août 1936 concernant les mises à la retraite par ancienneté.,,,2) Il résulte des articles 28 et 31 de la loi n° 2010-1330 du 9 novembre 2010, éclairés par les travaux préparatoires dont ils sont issus, que le législateur a entendu régir l'ensemble des fonctionnaires de la catégorie A, dite sédentaire, par l'article 28 et l'ensemble des fonctionnaires de la catégorie B, dite active, par l'article 31.,,,3) Il résulte également des mêmes travaux préparatoires que, s'agissant des agents de la fonction publique hospitalière, le législateur a entendu fixer la nouvelle limite d'âge maximale applicable aux agents occupant un emploi de catégorie B, dite active, à soixante-deux ans.</ANA>
<ANA ID="9B"> 36-11 1) Si aucune limite d'âge n'est déterminée par le statut particulier du cadre d'emplois auquel appartient un agent de la fonction publique hospitalière, la limite d'âge qui lui est applicable est celle que ne peuvent pas dépasser les agents de la fonction publique hospitalière occupant les emplois classés dans la même catégorie que l'emploi qu'il occupe, à savoir : soit la catégorie A (catégorie dite sédentaire), soit la catégorie B (catégorie dite active), au sens des dispositions de l'article 1er de la loi du 18 août 1936 concernant les mises à la retraite par ancienneté.,,,2) Il résulte des articles 28 et 31 de la loi n° 2010-1330 du 9 novembre 2010, éclairés par les travaux préparatoires dont ils sont issus, que le législateur a entendu régir l'ensemble des fonctionnaires de la catégorie A, dite sédentaire, par l'article 28 et l'ensemble des fonctionnaires de la catégorie B, dite active, par l'article 31.... ,,3) Il résulte également des mêmes travaux préparatoires que, s'agissant des agents de la fonction publique hospitalière, le législateur a entendu fixer la nouvelle limite d'âge maximale applicable aux agents occupant un emploi de catégorie B, dite active, à soixante-deux ans.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Comp ., dans l'état antérieur des textes, CE, 7 août 2008, Caisse des dépôts et consignations c/ Mme,, n° 281359, T. p. 833.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
