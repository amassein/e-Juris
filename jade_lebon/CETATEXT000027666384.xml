<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027666384</ID>
<ANCIEN_ID>JG_L_2013_07_000000366552</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/66/63/CETATEXT000027666384.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème et 7ème sous-sections réunies, 05/07/2013, 366552</TITRE>
<DATE_DEC>2013-07-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>366552</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème et 7ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MASSE-DESSEN, THOUVENIN, COUDRAY ; SCP ODENT, POULET</AVOCATS>
<RAPPORTEUR>Mme Sophie-Caroline de Margerie</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Béatrice Bourgeois-Machureau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:366552.20130705</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré au secrétariat du contentieux du Conseil d'Etat le 4 mars 2013, présenté pour la commune de Ligugé, représentée par son maire ; la commune de Ligugé demande au Conseil d'Etat d'annuler les articles 2 à 6 de l'ordonnance n° 1300093 du 15 février 2013 par lesquels le juge des référés du tribunal administratif de Poitiers, statuant en application de l'article L. 521-1 du code de justice administrative, a, sur la demande de Mme B...A..., d'une part, suspendu l'exécution de la décision du 20 novembre 2012 par laquelle le maire de Ligugé a refusé d'intégrer Mme A...dans les effectifs de la commune, d'autre part, fait injonction à cette dernière d'intégrer Mme A...dans ses effectifs et de la placer dans une situation statutaire régulière dans un délai de 24 heures ; <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 2 juillet 2013, présentée pour Mme A... ; <br/>
<br/>
              Vu le code général des collectivités territoriales ;<br/>
<br/>
              Vu la loi n° 84-53 du 26 janvier 1984 ; <br/>
<br/>
              Vu le code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie-Caroline de Margerie, Conseiller d'Etat,<br/>
<br/>
              - les conclusions de Mme Béatrice Bourgeois-Machureau, Rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Odent, Poulet, avocat de la commune de Ligugé, et à la SCP Masse-Dessen, Thouvenin, Coudray, avocat de Mme B...A...; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ;<br/>
<br/>
              2.	Considérant qu'il ressort des pièces du dossier soumis au juge des référés que Mme B...A..., adjoint territorial d'animation de 1ère classe, a été recrutée à partir du 1er juillet 2006 par la communauté de communes Vonne et Clain pour exercer à titre principal les fonctions de directrice du domaine de Givray, centre d'accueil de loisirs sans hébergement, dont la compétence de gestion avait été transférée à la communauté de communes par la commune de Ligugé à la suite de l'adhésion de la commune à cet établissement ; qu'à la suite de l'inclusion de la commune de Ligugé dans le périmètre de la communauté d'agglomération du grand Poitiers, le préfet de la Vienne a autorisé par arrêté le retrait de la commune de Ligugé de la communauté de communes Vonne et Clain à compter du 1er janvier 2013 ; que, par une délibération du 13 novembre 2012, le conseil communautaire de la communauté de communes a résilié la convention de mise à disposition partielle du domaine de Givray ; que, par une lettre du 20 novembre 2012, la commune de Ligugé a proposé à Mme A...d'être mise à la disposition de la commune, alors que cette dernière souhaitait son intégration dans les effectifs communaux ; que, par une délibération du 18 décembre 2012, le conseil communautaire a prononcé la suppression de l'emploi précédemment occupé par l'intéressée, qui a été radiée des effectifs de la communauté de communes par un arrêté du 31 décembre 2012 ; que Mme A...a demandé au tribunal administratif de Poitiers l'annulation de la décision du maire de Ligugé du 20 novembre 2012, la suspension de cette décision et à ce qu'il soit enjoint à la commune de l'intégrer dans ses effectifs ; que, par une ordonnance du 15 février 2013 contre laquelle la commune de Ligugé se pourvoit en cassation, le juge des référés du tribunal administratif de Poitiers a fait droit à la demande de Mme A...tendant à la suspension de l'exécution de la décision du 20 novembre 2012 et à sa demande d'injonction ; <br/>
<br/>
              3.	Considérant, en premier lieu, qu'aux termes de l'article L. 5211 4-1 du code général des collectivités territoriales : " I -Le transfert de compétences d'une commune à un établissement public de coopération intercommunale entraîne le transfert du service ou de la partie de service chargé de sa mise en oeuvre. Toutefois, dans le cadre d'une bonne organisation des services, une commune peut conserver tout ou partie du service concerné par le transfert de compétences, à raison du caractère partiel de ce dernier. / Les fonctionnaires territoriaux et agents territoriaux non titulaires qui remplissent en totalité leurs fonctions dans un service ou une partie de service transféré en application de l'alinéa précédent sont transférés dans l'établissement public de coopération intercommunale. Ils relèvent de cet établissement dans les conditions de statut et d'emploi qui sont les leurs. / Les modalités du transfert prévu aux alinéas précédents font l'objet d'une décision conjointe de la commune et de l'établissement public de coopération intercommunale, prise respectivement après avis du comité technique compétent pour la commune et, s'il existe, du comité technique compétent pour l'établissement public. / Le transfert peut être proposé aux fonctionnaires territoriaux et agents territoriaux non titulaires exerçant pour partie seulement dans un service ou une partie de service transféré. En cas de refus, ils sont de plein droit et sans limitation de durée mis à disposition, à titre individuel et pour la partie de leurs fonctions relevant du service ou de la partie de service transféré, du président de l'organe délibérant de l'établissement public de coopération intercommunale. Ils sont placés, pour l'exercice de cette partie de leurs fonctions, sous son autorité fonctionnelle. Les modalités de cette mise à disposition sont réglées par une convention conclue entre la commune et l'établissement public de coopération intercommunale. / Les agents transférés en vertu des alinéas précédents conservent, s'ils y ont intérêt, le bénéfice du régime indemnitaire qui leur était applicable ainsi que, à titre individuel, les avantages acquis en application du troisième alinéa de l'article 111 de la loi n° 84-53 du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale. / Il en est de même lorsqu'à l'inverse, par suite de modifications des statuts de la communauté, des personnels de celle-ci sont transférés à des communes " ; <br/>
<br/>
              4.	Considérant que ces dispositions prévoient et organisent le transfert des fonctionnaires et agents territoriaux qui remplissent en totalité ou en partie leurs fonctions dans un service chargé de la mise en oeuvre de compétences transférées d'une commune à un établissement public de coopération intercommunale ; qu'elles n'ont pas pour objet et ne sauraient avoir pour effet d'imposer, dans le cas où une commune se retire d'un établissement public de coopération intercommunale auquel elle avait adhéré, le transfert des personnels affectés au fonctionnement d'un équipement que la commune avait mis à disposition de cet établissement pour l'exercice d'une compétence communautaire et dont elle reprend la gestion ; <br/>
<br/>
              5.	Considérant qu'aux termes de l'article L. 5211-25-1 du code général des collectivités territoriales : " En cas de retrait de la compétence transférée à un établissement public de coopération intercommunale : / 1° Les biens meubles et immeubles mis à la disposition de l'établissement bénéficiaire du transfert de compétences sont restitués aux communes antérieurement compétentes et réintégrés dans leur patrimoine pour leur valeur nette comptable, avec les adjonctions effectuées sur ces biens liquidées sur les mêmes bases. Le solde de l'encours de la dette transférée afférente à ces biens est également restituée à la commune propriétaire ; / 2° Les biens meubles et immeubles acquis ou réalisés postérieurement au transfert de compétences sont répartis entre les communes qui reprennent la compétence ou entre la commune qui se retire de l'établissement public de coopération intercommunale et l'établissement ou, dans le cas particulier d'un syndicat dont les statuts le permettent, entre la commune qui reprend la compétence et le syndicat de communes. Il en va de même pour le produit de la réalisation de tels biens, intervenant à cette occasion. Le solde de l'encours de la dette contractée postérieurement au transfert de compétences est réparti dans les mêmes conditions entre les communes qui reprennent la compétence ou entre la commune qui se retire et l'établissement public de coopération intercommunale ou, le cas échéant, entre la commune et le syndicat de communes (...) / Les contrats sont exécutés dans les conditions antérieures jusqu'à leur échéance, sauf accord contraire des parties (...) " ; que ces dispositions traitent exclusivement des effets sur les biens et les contrats du retrait d'une compétence transférée à un établissement public de coopération intercommunale ;<br/>
<br/>
              6.	Considérant qu'ainsi, en jugeant que le moyen tiré de ce que la restitution à la commune de Ligugé de la compétence de gestion du domaine de Givray impliquait le transfert de Mme A...était propre à créer un doute sérieux quant à la légalité de la décision du 20 novembre 2012 du maire de Ligugé refusant à Mme A...son intégration dans les effectifs communaux, alors qu'une telle obligation ne résulte d'aucune disposition législative et en particulier qu'elle ne résulte pas du I de l'article L. 5211-4-1  du code général des collectivités territoriales précité, le juge des référés du tribunal administratif de Poitiers a commis une erreur de droit ;<br/>
<br/>
              7.	Considérant, en second lieu, qu'aux termes de l'article 97 de la loi du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale : " Dès lors qu'un emploi est susceptible d'être supprimé, l'autorité territoriale recherche les possibilités de reclassement du fonctionnaire concerné. (...) Si la collectivité ou l'établissement ne peut lui offrir un emploi correspondant à son grade dans son cadre d'emplois ou, avec son accord, dans un autre cadre d'emplois, le fonctionnaire est maintenu en surnombre pendant un an. Pendant cette période, tout emploi créé ou vacant correspondant à son grade dans la collectivité ou l'établissement lui est proposé en priorité ; la collectivité ou l'établissement, (...) et le centre de gestion examinent, chacun pour ce qui le concerne, les possibilités de reclassement (...) " ; que la suppression de l'emploi de Mme A...emportait l'application de ces dispositions par la communauté de communes Vonne et Clain, qui l'avait recrutée ; qu'ainsi, en jugeant que le moyen tiré de ce que la commune de Ligugé aurait méconnu l'article 55 de la même loi relatif aux positions des fonctionnaires en ne plaçant pas Mme A...dans une situation statutaire régulière était propre à créer un doute sérieux quant à la légalité de la décision attaquée, le juge des référés du tribunal administratif de Poitiers a commis une erreur de droit ;<br/>
<br/>
              8.	Considérant qu'il résulte de ce qui précède que la commune de Ligugé est fondée à demander l'annulation de l'ordonnance attaquée ;<br/>
<br/>
              9.	Considérant qu'il y a lieu, dans les circonstances de l'espèce, en application des dispositions de l'article L. 821-2 du code de justice administrative, de régler l'affaire au titre de la procédure de référé engagée par Mme A...;<br/>
<br/>
              10.	Considérant qu'ainsi qu'il a été dit plus haut, les moyens tirés de la violation de l'article L. 5211-4-1 du code général des collectivités territoriales et de l'article 55 de la loi du 26 janvier 1984 ne sont pas de nature, en l'état de l'instruction, à créer un doute sérieux quant à la légalité de la décision du 20 novembre 2012 du maire de Ligugé ; qu'il en est de même du moyen tiré de ce que cette décision aurait été prise par une autorité incompétente ; qu'il y a lieu, par suite et sans qu'il soit besoin de statuer sur la condition d'urgence, de rejeter la demande de Mme A...tendant à la suspension de l'exécution de cette décision ainsi que, par voie de conséquence, ses conclusions à fin d'injonction ;<br/>
<br/>
              11.	Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par la commune de Ligugé au titre des dispositions de l'article L. 761-1 du code de justice administrative ; que ces dispositions font obstacle à ce que les frais exposés par Mme A...soient mis à la charge de la commune de Ligugé, qui n'est pas la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Les articles 2 à 6 de l'ordonnance du juge des référés du tribunal administratif de Poitiers en date du 15 février 2013 sont annulés.<br/>
<br/>
Article 2 : La demande présentée par Mme A...devant le juge des référés du tribunal administratif de Poitiers ainsi que ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 3 : Les conclusions de la commune de Ligugé présentées au titre des dispositions de l'article L. 761-1 du code de la justice administrative sont rejetées.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la commune de Ligugé et à Mme B...A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">135-05-01-01 COLLECTIVITÉS TERRITORIALES. COOPÉRATION. ÉTABLISSEMENTS PUBLICS DE COOPÉRATION INTERCOMMUNALE - QUESTIONS GÉNÉRALES. DISPOSITIONS GÉNÉRALES ET QUESTIONS COMMUNES. - TRANSFERT DES FONCTIONNAIRES ET AGENTS TERRITORIAUX REMPLISSANT LEURS FONCTIONS DANS UN SERVICE CHARGÉ DE LA MISE EN &#140;UVRE DE COMPÉTENCES TRANSFÉRÉES D'UNE COMMUNE À UN EPCI (ART. L. 5211-4-1 DU CGCT) - PORTÉE - OBLIGATION POUR UNE COMMUNE SE RETIRANT D'UN EPCI DE TRANSFÉRER DES AGENTS QU'ELLE AVAIT MIS À DISPOSITION DE CET EPCI POUR L'EXERCICE D'UNE COMPÉTENCE DONT ELLE REPREND LA GESTION - ABSENCE.
</SCT>
<ANA ID="9A"> 135-05-01-01 Les dispositions de l'article L. 5211-4-1 du code général des collectivités territoriales (CGCT), qui prévoient et organisent le transfert des fonctionnaires et agents territoriaux qui remplissent en totalité ou en partie leurs fonctions dans un service chargé de la mise en oeuvre de compétences transférées d'une commune à un établissement public de coopération intercommunale (EPCI), n'ont pas pour objet et ne sauraient avoir pour effet d'imposer, dans le cas où une commune se retire d'un EPCI auquel elle avait adhéré, le transfert des personnels affectés au fonctionnement d'un équipement que la commune avait mis à disposition de cet établissement pour l'exercice d'une compétence communautaire et dont elle reprend la gestion.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
