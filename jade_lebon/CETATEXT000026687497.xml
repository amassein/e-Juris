<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026687497</ID>
<ANCIEN_ID>JG_L_2012_11_000000352420</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/68/74/CETATEXT000026687497.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 4ème sous-sections réunies, 26/11/2012, 352420</TITRE>
<DATE_DEC>2012-11-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>352420</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 4ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Olivier Rousselle</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Fabienne Lambolez</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:352420.20121126</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi, enregistré le 5 septembre 2011 au secrétariat du contentieux du Conseil d'Etat, présenté par le ministre de l'écologie, du développement durable, des transports et du logement ; le ministre de l'écologie, du développement durable, des transports et du logement demande au Conseil d'Etat d'annuler l'arrêt n° 10VE02545 du 28 juin 2011 par lequel la cour administrative d'appel de Versailles a rejeté son appel contre le jugement n° 0908620 du 1er juillet 2010 du tribunal administratif de Versailles annulant la décision du 26 août 2009 de la commission de médiation de l'Essonne refusant de reconnaître M. Kamo A comme prioritaire et devant être logé d'urgence ;<br/>
<br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
	Vu le code de la construction et de l'habitation ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Rousselle, Conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Fabienne Lambolez, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes du premier alinéa de l'article L. 300-1 du code de la construction et de l'habitation : " Le droit à un logement décent et indépendant, (...) est garanti par l'Etat à toute personne qui, résidant sur le territoire français de façon régulière (...), n'est pas en mesure d'y accéder par ses propres moyens ou de s'y maintenir " ; qu'aux termes du premier alinéa du II de l'article L. 441-2-3 : " La commission de médiation peut être saisie par toute personne qui, satisfaisant aux conditions réglementaires d'accès à un logement locatif social, n'a reçu aucune proposition adaptée en réponse à sa demande de logement (...) " ; qu'aux termes du deuxième alinéa de l'article R. 441-14-1  : " Peuvent être désignées par la commission comme prioritaires et devant être logées d'urgence en application du II de l'article L. 441-2-3 les personnes de bonne foi qui satisfont aux conditions réglementaires d'accès au logement social (...) " ; qu'il résulte des dispositions des articles L. 441-1 et R. 441-1 du code de la construction et de l'habitation que les conditions réglementaires d'accès au logement social sont appréciées en prenant en compte la situation de l'ensemble des personnes du foyer pour le logement duquel un logement social est demandé et qu'au nombre de ces conditions figure notamment celle que ces personnes séjournent régulièrement sur le territoire français ; qu'il résulte de la combinaison de l'ensemble des dispositions mentionnées ci-dessus que la commission de médiation peut légalement refuser de reconnaître un demandeur comme prioritaire et devant être logé d'urgence au motif que les personnes composant le foyer pour le logement duquel il a présenté sa demande ne séjournent pas toutes régulièrement sur le territoire français ;<br/>
<br/>
              2. Considérant que, par un arrêt du 28 juin 2011 contre lequel le ministre de l'écologie, du développement durable, des transports et du logement se pourvoit en cassation, la cour administrative d'appel de Versailles a confirmé le jugement du tribunal administratif de Versailles du 1er juillet 2010 annulant pour excès de pouvoir la décision de la commission de médiation de l'Essonne du 26 août 2009 refusant de reconnaître M. A comme prioritaire et devant être logé d'urgence ; que, pour confirmer cette annulation, la cour a jugé que le motif sur lequel était fondée la décision de la commission, qui était tiré de ce que les membres de la famille de M. A, pour laquelle celui-ci demandait un logement, ne séjournaient pas tous régulièrement sur le territoire français, était erroné en droit au regard des dispositions du code de la construction et de l'habitation ; qu'il résulte de ce qui a été dit ci-dessus que la cour a ainsi entaché son arrêt d'erreur de droit ; que le ministre de l'écologie, du développement durable, des transports et du logement est par suite fondé à en demander l'annulation ;<br/>
<br/>
              3. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de faire application des dispositions de l'article L. 821-2 du code de justice administrative et de régler l'affaire au fond ;<br/>
<br/>
              4. Considérant qu'il ressort des pièces du dossier que M. A a présenté un recours devant la commission de médiation de l'Essonne afin d'obtenir un logement pour lui-même, sa compagne et un enfant ; qu'il est constant que sa compagne, de nationalité ukrainienne, ne résidait pas régulièrement sur le territoire français, à la date de la décision attaquée  ; qu'il résulte de ce qui a été dit ci-dessus qu'en se fondant sur ce motif pour rejeter, par sa décision du 26 août 2009, le recours présenté par M. A, la commission de médiation a fait une exacte application des dispositions du code de la construction et de l'habitation ; que c'est dès lors à tort que le tribunal administratif de Versailles s'est fondé sur ce que le motif retenu par la commission de médiation serait erroné en droit pour annuler pour excès de pouvoir sa décision du 26 août 2009 ; <br/>
<br/>
              5. Considérant qu'il appartient au Conseil d'Etat, saisi par l'objet dévolutif de l'appel, d'examiner les autres moyens invoqués par M. A devant le tribunal administratif de Versailles ;<br/>
<br/>
              6. Considérant qu'eu égard au motif de refus retenu par la commission, les moyens tirés de la difficulté de sa situation familiale invoqués par M. A, ne peuvent être utilement invoqués pour contester la légalité de sa décision ;<br/>
<br/>
              7. Considérant qu'il résulte de tout ce qui précède que le ministre de l'écologie, du développement durable, des transports et du logement est fondé à soutenir que c'est à tort que, par son jugement du 1er juillet 2010, le tribunal administratif de Versailles a annulé la décision de la commission de médiation de l'Essonne du 26 août 2009 ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Versailles du 28 juin 2011 et le jugement du tribunal administratif de Versailles du 1er juillet 2010 sont annulés.<br/>
<br/>
Article 2 : La demande présentée par M. A devant le tribunal administratif de Versailles est rejetée.<br/>
<br/>
Article 3 : La présente décision sera notifiée à la ministre de l'écologie, du développement durable, des transports et du logement et à M. Kamo A.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">335-01 ÉTRANGERS. SÉJOUR DES ÉTRANGERS. - DROIT AU LOGEMENT OPPOSABLE - CONDITION DE RÉGULARITÉ DU SÉJOUR - CONDITION APPLICABLE À L'ENSEMBLE DES PERSONNES DU FOYER POUR LE LOGEMENT DUQUEL LA DEMANDE EST EFFECTUÉE (ART. L. 441-1 ET R. 441-1 DU CCH).
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">38-07-01 LOGEMENT. - 1) RECOURS CONTRE LES DÉCISIONS DE LA COMMISSION DE MÉDIATION - NATURE - EXCÈS DE POUVOIR - 2) CONDITION DE RÉGULARITÉ DU SÉJOUR - CONDITION APPLICABLE À L'ENSEMBLE DES PERSONNES DU FOYER POUR LE LOGEMENT DUQUEL LA DEMANDE EST EFFECTUÉE (ART. L. 441-1 ET R. 441-1 DU CCH).
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-02-01-01 PROCÉDURE. DIVERSES SORTES DE RECOURS. RECOURS POUR EXCÈS DE POUVOIR. RECOURS AYANT CE CARACTÈRE. - DROIT AU LOGEMENT OPPOSABLE (DALO) - RECOURS CONTRE LES DÉCISIONS DE LA COMMISSION DE MÉDIATION.
</SCT>
<ANA ID="9A"> 335-01 Il résulte des dispositions des articles L. 441-1 et R. 441-1 du code de la construction et de l'habitation (CCH) que les conditions réglementaires d'accès au logement social sont appréciées en prenant en compte la situation de l'ensemble des personnes du foyer pour le logement duquel un logement social est demandé. Au nombre de ces conditions, figure notamment celle que ces personnes séjournent régulièrement sur le territoire français. Par suite, la commission de médiation peut légalement refuser de reconnaître un demandeur comme prioritaire et devant être logé d'urgence au motif que les personnes composant le foyer pour le logement duquel il a présenté sa demande ne séjournent pas toutes régulièrement sur le territoire français.</ANA>
<ANA ID="9B"> 38-07-01 1)  Les recours contre les décisions des commissions de médiation sur les demandes tendant à être déclaré prioritaire et devant être logé d'urgence relèvent du contentieux de l'excès de pouvoir.,,2) Il résulte des dispositions des articles L. 441-1 et R. 441-1 du code de la construction et de l'habitation (CCH) que les conditions réglementaires d'accès au logement social sont appréciées en prenant en compte la situation de l'ensemble des personnes du foyer pour le logement duquel un logement social est demandé. Au nombre de ces conditions, figure notamment celle que ces personnes séjournent régulièrement sur le territoire français. Par suite, la commission de médiation peut légalement refuser de reconnaître un demandeur comme prioritaire et devant être logé d'urgence au motif que les personnes composant le foyer pour le logement duquel il a présenté sa demande ne séjournent pas toutes régulièrement sur le territoire français.</ANA>
<ANA ID="9C"> 54-02-01-01 Les recours contre les décisions des commissions de médiation sur les demandes tendant à être déclaré prioritaire et devant être logé d'urgence relèvent du contentieux de l'excès de pouvoir.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
