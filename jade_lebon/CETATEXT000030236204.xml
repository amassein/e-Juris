<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030236204</ID>
<ANCIEN_ID>JG_L_2015_02_000000384447</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/23/62/CETATEXT000030236204.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème / 9ème SSR, 13/02/2015, 384447</TITRE>
<DATE_DEC>2015-02-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>384447</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème / 9ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Thomas Campeaux</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:384447.20150213</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête enregistrée le 12 septembre 2014 au secrétariat du contentieux du Conseil d'Etat, le haut-commissaire de la République en Polynésie française demande au Conseil d'Etat de déclarer la " loi du pays " n° 2014-26 LP/APF adoptée le 25 août 2014, portant modification du titre 8 du livre Ier de la première partie du code de l'aménagement de la Polynésie française, non conforme au bloc de légalité défini au III de l'article 176 de la loi organique du 27 février 2004 portant statut d'autonomie de la Polynésie française.<br/>
<br/>
<br/>
              Vu :<br/>
              - les autres pièces du dossier ;<br/>
              - la Constitution, notamment son Préambule et son article 74 ;<br/>
              - la loi organique n° 2004-192 du 27 février 2004 ;<br/>
              - le code de l'aménagement de la Polynésie française ;<br/>
              - le code de justice administrative.<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Thomas Campeaux, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>1.	Considérant que le haut-commissaire de la République en Polynésie française défère au Conseil d'Etat, sur le fondement des dispositions de l'article 176 de la loi organique du 27 février 2004 portant statut d'autonomie de la Polynésie française, la " loi du pays " adoptée, sur le fondement des dispositions de l'article 140 de la même loi organique, par l'assemblée de la Polynésie française, le 25 août 2004 et portant modification du titre 8 du livre Ier de la première partie du code de l'aménagement de la Polynésie française ;<br/>
<br/>
              2.	Considérant qu'aux termes de l'article 7 de la Charte de l'environnement : " Toute personne a le droit, dans les conditions et les limites définies par la loi, d'accéder aux informations relatives à l'environnement détenues par les autorités publiques et de participer à l'élaboration des décisions publiques ayant une incidence sur l'environnement " ; qu'aux termes de l'article 140 de la loi organique du 27 février 2004 : " Les actes de l'assemblée de la Polynésie française, dénommés " lois du pays ", sur lesquels le Conseil d'Etat exerce un contrôle juridictionnel spécifique sont ceux qui, relevant du domaine de la loi, soit ressortissent à la compétence de la Polynésie française en application de l'article 13, soit sont pris au titre de la participation de la Polynésie française à l'exercice des compétences de l'Etat dans les conditions prévues aux articles 31 à 36 " ; qu'il résulte de ces dispositions que lorsque l'assemblée de la Polynésie française édicte, par des actes dénommés " lois du pays ", des mesures relevant du domaine de la loi, il lui incombe de définir les conditions et limites dans lesquelles doit s'exercer le droit reconnu à toute personne par l'article 7 de la Charte de l'environnement d'accéder aux informations relatives à l'environnement détenues par les autorités publiques et de participer à l'élaboration des décisions publiques ayant une incidence sur l'environnement ;<br/>
<br/>
              3.	Considérant que les articles D. 181-1 et D. 181-2 du code de l'aménagement de la Polynésie française prévoient que les plans de prévention des risques naturels prévisibles ont notamment pour objet de délimiter les zones exposées à ces risques et celles dans lesquelles de tels risques sont susceptibles d'être provoqués ou aggravés par des constructions, des ouvrages, des aménagements ou des exploitations et de définir, compte tenu de leur gravité, les mesures qui doivent être prises dans ces zones ; que ces mesures peuvent consister en l'interdiction de tout type de construction, d'ouvrage, d'aménagement ou d'exploitation, ou en la prescription des conditions dans lesquelles ils doivent être réalisés, utilisés ou exploités ; qu'elles peuvent également consister en des mesures de prévention et de protection incombant aux collectivités publiques et aux particuliers ainsi qu'en des mesures relatives à l'aménagement, l'utilisation ou l'exploitation des constructions, des ouvrages ou des espaces existant à la date de l'approbation du plan ; que les plans de prévention des risques naturels constituent ainsi des décisions publiques susceptibles d'avoir une incidence sur l'environnement au sens de l'article 7 de la Charte de l'environnement ;<br/>
<br/>
              4.	Considérant que la " loi du pays " déférée ajoute à la fin du titre 8 du livre Ier de la deuxième partie du code de l'aménagement de la Polynésie française un article LP. 182-7 aux termes duquel : " Afin de prendre en compte l'évolution de la connaissance des risques mentionnés au PPR, ce dernier peut être actualisé par arrêté pris en conseil des ministres, au vu d'études précisant ou modifiant ces risques, et après avis de l'autorité administrative compétente pour l'élaboration des PPR et du conseil municipal de la commune concernée (...) " ;<br/>
<br/>
              5.	Considérant que ces dispositions ne limitent pas l'étendue des modifications qu'il est possible d'apporter à un plan de prévention des risques naturels prévisibles ; que l'actualisation qu'elles prévoient est dès lors susceptible d'avoir une incidence directe et significative sur l'environnement ; que ces mêmes dispositions définissent une procédure spécifique, exclusive de la procédure décrite aux articles D. 182-1 à D. 182-5 du code de l'aménagement de la Polynésie française pour l'approbation ou la révision d'un plan de prévention des risques naturels prévisibles, et qui n'inclut, à la différence de cette dernière, aucune modalité de participation du public ; qu'aucune autre disposition applicable en Polynésie française n'assure la mise en oeuvre du principe de participation du public à l'élaboration de cette décision ;<br/>
<br/>
              6.	Considérant qu'il résulte de ce qui précède qu'en adoptant les dispositions contestées sans déterminer les conditions et limites de la participation du public à la procédure d'actualisation des plans de prévention des risques naturels prévisibles, l'assemblée de la Polynésie française a méconnu l'étendue de sa compétence au regard des exigences de l'article 7 de la Charte de l'environnement ; que le haut-commissaire de la République en Polynésie française est donc fondé à demander que la " loi du pays " contestée soit déclarée illégale ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La " loi du pays " n° 2014-26 LP/APF adoptée le 25 août 2014 portant modification du titre 8 du livre Ier de la première partie du code de l'aménagement de la Polynésie française est illégale et ne peut être promulguée.<br/>
Article 2 : La présente décision sera notifiée au haut-commissaire de la République en Polynésie française, au président de la Polynésie française et au président de l'assemblée de la Polynésie française.<br/>
Copie en sera adressée pour information à la ministre des outre-mer.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">44-005-07 NATURE ET ENVIRONNEMENT. - 1) LOI DU PAYS DE POLYNÉSIE FRANÇAISE - OBLIGATION DE METTRE EN OEUVRE L'ARTICLE 7 DE LA CHARTE - EXISTENCE - 2) APPLICATION -  PLANS DE PRÉVENTION DES RISQUES NATURELS EN POLYNÉSIE FRANÇAISE - A) NATURE - DÉCISIONS PUBLIQUES SUSCEPTIBLES D'AVOIR UNE INCIDENCE SUR L'ENVIRONNEMENT - B) DISPOSITIONS D'UNE LOI DU PAYS PRÉVOYANT LA POSSIBILITÉ D'ACTUALISER UN TEL PLAN, SANS LIMITER L'ÉTENDUE DES MODIFICATIONS, DANS LE CADRE D'UNE PROCÉDURE SPÉCIFIQUE N'ASSURANT  PAS LA MISE EN &#140;UVRE DU PRINCIPE DE PARTICIPATION DU PUBLIC - ILLÉGALITÉ.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">46-01-03-02-03 OUTRE-MER. DROIT APPLICABLE. LOIS ET RÈGLEMENTS (HORS STATUTS DES COLLECTIVITÉS). COLLECTIVITÉS D'OUTRE-MER ET NOUVELLE-CALÉDONIE. POLYNÉSIE FRANÇAISE. - 1) LOI DU PAYS DE POLYNÉSIE FRANÇAISE - OBLIGATION DE METTRE EN OEUVRE L'ARTICLE 7 DE LA CHARTE DE L'ENVIRONNEMENT - EXISTENCE - 2) APPLICATION -  PLANS DE PRÉVENTION DES RISQUES NATURELS EN POLYNÉSIE FRANÇAISE - A) NATURE - DÉCISIONS PUBLIQUES SUSCEPTIBLES D'AVOIR UNE INCIDENCE SUR L'ENVIRONNEMENT - B) DISPOSITIONS D'UNE LOI DU PAYS PRÉVOYANT LA POSSIBILITÉ D'ACTUALISER UN TEL PLAN, SANS LIMITER L'ÉTENDUE DES MODIFICATIONS, DANS LE CADRE D'UNE PROCÉDURE SPÉCIFIQUE N'ASSURANT  PAS LA MISE EN &#140;UVRE DU PRINCIPE DE PARTICIPATION DU PUBLIC - ILLÉGALITÉ.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">68-01 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PLANS D'AMÉNAGEMENT ET D'URBANISME. - PLANS DE PRÉVENTION DES RISQUES NATURELS EN POLYNÉSIE FRANÇAISE - 1) NATURE - DÉCISIONS PUBLIQUES SUSCEPTIBLES D'AVOIR UNE INCIDENCE SUR L'ENVIRONNEMENT - 2) DISPOSITIONS D'UNE LOI DU PAYS PRÉVOYANT LA POSSIBILITÉ D'ACTUALISER UN TEL PLAN, SANS LIMITER L'ÉTENDUE DES MODIFICATIONS, DANS LE CADRE D'UNE PROCÉDURE SPÉCIFIQUE N'ASSURANT  PAS LA MISE EN &#140;UVRE DU PRINCIPE DE PARTICIPATION DU PUBLIC - ILLÉGALITÉ AU REGARD DES EXIGENCES DE L'ARTICLE 7 DE LA CHARTE DE L'ENVIRONNEMENT.
</SCT>
<ANA ID="9A"> 44-005-07 1) Lorsque l'assemblée de la Polynésie française édicte, par des actes dénommés  lois du pays , des mesures relevant du domaine de la loi, il lui incombe de définir les conditions et limites dans lesquelles doit s'exercer le droit reconnu à toute personne par l'article 7 de la Charte de l'environnement d'accéder aux informations relatives à l'environnement détenues par les autorités publiques et de participer à l'élaboration des décisions publiques ayant une incidence sur l'environnement.,,,2) a) Les articles D. 181-1 et D. 181-2 du code de l'aménagement de la Polynésie française prévoient que les plans de prévention des risques naturels prévisibles ont notamment pour objet de délimiter les zones exposées à ces risques et de définir les mesures qui doivent être prises dans ces zones. Les plans de prévention des risques naturels constituent, au regard de leur contenu et de la nature des mesures qu'ils prévoient, des décisions publiques susceptibles d'avoir une incidence sur l'environnement au sens de l'article 7 de la Charte de l'environnement.,,,b) Des dispositions d'une loi du pays prévoient la possibilité d'actualiser un plan de prévention des risques naturels prévisibles sans limiter l'étendue des modifications qu'il est possible d'apporter à celui-ci. L'actualisation qu'elles prévoient est dès lors susceptible d'avoir une incidence directe et significative sur l'environnement. Ces mêmes dispositions définissent une procédure spécifique, exclusive de la procédure décrite aux articles D. 182-1 à D. 182-5 du code de l'aménagement de la Polynésie française pour l'approbation ou la révision d'un plan de prévention des risques naturels prévisibles, et qui n'inclut, à la différence de cette dernière, aucune modalité de participation du public. Aucune autre disposition applicable en Polynésie française n'assure la mise en oeuvre du principe de participation du public à l'élaboration de cette décision. Par suite, en adoptant ces dispositions sans déterminer les conditions et limites de la participation du public à la procédure d'actualisation des plans de prévention des risques naturels prévisibles, l'assemblée de la Polynésie française a méconnu l'étendue de sa compétence au regard des exigences de l'article 7 de la Charte de l'environnement.</ANA>
<ANA ID="9B"> 46-01-03-02-03 1) Lorsque l'assemblée de la Polynésie française édicte, par des actes dénommés  lois du pays , des mesures relevant du domaine de la loi, il lui incombe de définir les conditions et limites dans lesquelles doit s'exercer le droit reconnu à toute personne par l'article 7 de la Charte de l'environnement d'accéder aux informations relatives à l'environnement détenues par les autorités publiques et de participer à l'élaboration des décisions publiques ayant une incidence sur l'environnement.,,,2) a) Les articles D. 181-1 et D. 181-2 du code de l'aménagement de la Polynésie française prévoient que les plans de prévention des risques naturels prévisibles ont notamment pour objet de délimiter les zones exposées à ces risques et de définir les mesures qui doivent être prises dans ces zones. Les plans de prévention des risques naturels constituent, au regard de leur contenu et de la nature des mesures qu'ils prévoient, des décisions publiques susceptibles d'avoir une incidence sur l'environnement au sens de l'article 7 de la Charte de l'environnement.,,,b) Des dispositions d'une loi du pays prévoient la possibilité d'actualiser un plan de prévention des risques naturels prévisibles sans limiter l'étendue des modifications qu'il est possible d'apporter à celui-ci. L'actualisation qu'elles prévoient est dès lors susceptible d'avoir une incidence directe et significative sur l'environnement. Ces mêmes dispositions définissent une procédure spécifique, exclusive de la procédure décrite aux articles D. 182-1 à D. 182-5 du code de l'aménagement de la Polynésie française pour l'approbation ou la révision d'un plan de prévention des risques naturels prévisibles, et qui n'inclut, à la différence de cette dernière, aucune modalité de participation du public. Aucune autre disposition applicable en Polynésie française n'assure la mise en oeuvre du principe de participation du public à l'élaboration de cette décision. Par suite, en adoptant ces dispositions sans déterminer les conditions et limites de la participation du public à la procédure d'actualisation des plans de prévention des risques naturels prévisibles, l'assemblée de la Polynésie française a méconnu l'étendue de sa compétence au regard des exigences de l'article 7 de la Charte de l'environnement.</ANA>
<ANA ID="9C"> 68-01 1) Les articles D. 181-1 et D. 181-2 du code de l'aménagement de la Polynésie française prévoient que les plans de prévention des risques naturels prévisibles ont notamment pour objet de délimiter les zones exposées à ces risques et de définir les mesures qui doivent être prises dans ces zones. Les plans de prévention des risques naturels constituent, au regard de leur contenu et de la nature des mesures qu'ils prévoient, des décisions publiques susceptibles d'avoir une incidence sur l'environnement au sens de l'article 7 de la Charte de l'environnement.,,,2) Des dispositions d'une loi du pays prévoient la possibilité d'actualiser un plan de prévention des risques naturels prévisibles sans limiter l'étendue des modifications qu'il est possible d'apporter à celui-ci. L'actualisation qu'elles prévoient est dès lors susceptible d'avoir une incidence directe et significative sur l'environnement. Ces mêmes dispositions définissent une procédure spécifique, exclusive de la procédure décrite aux articles D. 182-1 à D. 182-5 du code de l'aménagement de la Polynésie française pour l'approbation ou la révision d'un plan de prévention des risques naturels prévisibles, et qui n'inclut, à la différence de cette dernière, aucune modalité de participation du public. Aucune autre disposition applicable en Polynésie française n'assure la mise en oeuvre du principe de participation du public à l'élaboration de cette décision. Par suite, en adoptant ces dispositions sans déterminer les conditions et limites de la participation du public à la procédure d'actualisation des plans de prévention des risques naturels prévisibles, l'assemblée de la Polynésie française a méconnu l'étendue de sa compétence au regard des exigences de l'article 7 de la Charte de l'environnement.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
