<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042381916</ID>
<ANCIEN_ID>JG_L_2020_09_000000441059</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/38/19/CETATEXT000042381916.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 28/09/2020, 441059</TITRE>
<DATE_DEC>2020-09-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>441059</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Réda Wadjinny-Green</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:441059.20200928</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
      1°) Sous le numéro 441059, par deux mémoires, enregistrés le 8 juin 2020 au secrétariat du contentieux du Conseil d'État, et un mémoire en réplique, enregistré le 17 août 2020, M. W... U..., M. P... N..., Mme R... X..., M. C... Q..., M. L... F..., M. M... J..., Mme V... B..., Mme S... K..., M. H... A..., M. I... D..., Mme T... O..., Mme G... E... et M. Y... demandent au Conseil d'État, en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958 et à l'appui de leur requête tendant à l'annulation pour excès de pouvoir de certaines dispositions du décret n° 2020-663 du 31 mai 2020 prescrivant les mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution de l'article L. 3841-2 du code de la santé publique, d'une part, et des articles 4 de la loi n° 2020-290 du 23 mars 2020 d'urgence pour faire face à l'épidémie de covid-19 et 1er de la loi n° 2020-546 du 11 mai 2020 prorogeant l'état d'urgence sanitaire et complétant ses dispositions.<br/>
<br/>
      2°) Sous le numéro 442045, par un mémoire et un mémoire en réplique, enregistrés les 22 juillet et 17 août 2020 au secrétariat du contentieux du Conseil d'État, M. W... U..., M. P... N..., Mme R... X..., M. C... Q..., M. L... F..., M. M... J..., Mme V... B..., Mme S... K..., M. H... A..., M. I... D..., Mme T... O..., Mme G... E... et M. Y... demandent au Conseil d'État, en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958 et à l'appui de leur requête tendant à l'annulation pour excès de pouvoir de certaines dispositions du décret n° 2020-860 du 10 juillet 2020 prescrivant les mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans les territoires sortis de l'état d'urgence sanitaire et dans ceux où il a été prorogé, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution de l'article 5 de la loi n° 2020-856 du 9 juillet 2020 organisant la sortie de l'état d'urgence sanitaire.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
<br/>
      Vu les autres pièces des dossiers ;<br/>
<br/>
      Vu :<br/>
      - la Constitution, notamment son article 61-1 ; <br/>
      - la loi organique n° 99-209 du 19 mars 1999 ;<br/>
      - l'accord sur la Nouvelle-Calédonie signé à Nouméa le 5 mai 1998 ; <br/>
      - le code de la santé publique ;<br/>
      - la loi n° 2020-290 du 23 mars 2020 ;<br/>
      - la loi n° 2020-546 du 11 mai 2020 ;<br/>
      - la loi n° 2020-856 du 9 juillet 2020 ;<br/>
      - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
      - le code	de justice administrative ;	<br/>
<br/>
<br/>
<br/>
      Après avoir entendu en séance publique :<br/>
<br/>
      - le rapport de M. Réda Wadjinny-Green, auditeur,  <br/>
<br/>
      - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
              1. Les requêtes de M. U... et autres présentent à juger les mêmes questions. Il y a lieu de les joindre pour statuer par une seule décision.<br/>
<br/>
              2. Aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...) ". Il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              3. A l'appui des recours pour excès de pouvoir formés contre le décret du 31 mai 2020 prescrivant les mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire, d'une part, et contre le décret du 10 juillet 2020 prescrivant les mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans les territoires sortis de l'état d'urgence sanitaire et dans ceux où il a été prorogé, d'autre part, en tant que les règles qu'ils édictent sont applicables à la Nouvelle-Calédonie, M. U... et autres demandent au Conseil d'Etat de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des dispositions de l'article L. 3841-2 du code de la santé publique, de l'article 4 de la loi du 23 mars 2020 d'urgence pour faire face à l'épidémie de covid-19, de l'article 1er de la loi du 11 mai 2020 prorogeant l'état d'urgence sanitaire et complétant ses dispositions et de l'article 5 de la loi du 9 juillet 2020 organisant la sortie de l'état d'urgence sanitaire. <br/>
<br/>
              4. En premier lieu, la loi du 23 mars 2020 insère au tire III du livre Ier de la troisième partie du code de la santé publique un chapitre Ier bis relatif à l'état d'urgence sanitaire. Aux termes de l'article L. 3131-12 de ce code, l'état d'urgence sanitaire peut être déclaré " en cas de catastrophe sanitaire mettant en péril, par sa nature et sa gravité, la santé de la population ". L'article L. 3131-15 du même code dresse la liste des mesures de police que le Premier ministre peut prendre, lorsque l'état d'urgence sanitaire est déclaré, " aux seules fins de garantir la santé publique ", tandis qu'aux termes de l'article L. 3131-16 du code : " Dans les circonscriptions territoriales où l'état d'urgence sanitaire est déclaré, le ministre chargé de la santé peut prescrire, par arrêté motivé, toute mesure réglementaire relative à l'organisation et au fonctionnement du dispositif de santé, à l'exception des mesures prévues à l'article L. 3131-15, visant à mettre fin à la catastrophe sanitaire mentionnée à l'article L. 3131-12. / Dans les mêmes conditions, le ministre chargé de la santé peut prescrire toute mesure individuelle nécessaire à l'application des mesures prescrites par le Premier ministre en application des 1° à 9° du I de l'article L. 3131-15 (...) ". L'article 4 de cette loi dispose que : " L'état d'urgence sanitaire entre en vigueur sur l'ensemble du territoire national ", tandis que l'article 1er de la loi du 11 mai 2020 proroge l'état d'urgence sanitaire dans le même ressort géographique. L'article L. 3841-2 du code de la santé publique, dans sa rédaction applicable au litige, dispose quant à lui que : " Le chapitre Ier bis du titre III du livre Ier de la troisième partie est applicable en Nouvelle-Calédonie et en Polynésie française dans sa rédaction résultant de la loi n° 2020-546 du 11 mai 2020 prorogeant l'état d'urgence sanitaire et complétant ses dispositions jusqu'au 1er avril 2021, sous réserve des adaptations suivantes : / 1° Les références au département sont remplacées, selon le cas, par la référence à la Nouvelle-Calédonie ou par la référence à la Polynésie française ; / 2° Le premier alinéa du I de l'article L. 3131-17 est remplacé par les deux alinéas suivants : / Lorsque le Premier ministre ou le ministre chargé de la santé prennent des mesures mentionnées aux articles L. 3131-15 et L. 3131-16 et les rendent applicables à la Nouvelle-Calédonie ou à la Polynésie française, ils peuvent habiliter le haut-commissaire à les adapter en fonction des circonstances locales et à prendre toutes les mesures générales ou individuelles d'application de ces dispositions, lorsqu'elles relèvent de la compétence de l'Etat et après consultation du gouvernement de la collectivité. / Lorsqu'une des mesures mentionnées aux 1°, 2° et 5° à 9° du I de l'article L. 3131-15 ou à l'article L. 3131-16 doit s'appliquer dans un champ géographique qui n'excède pas la Nouvelle-Calédonie ou la Polynésie française, les autorités mentionnées aux mêmes articles peuvent habiliter le haut-commissaire à la décider lui-même, assortie des adaptations nécessaires s'il y a lieu et dans les mêmes conditions qu'au premier alinéa ". <br/>
<br/>
              5. En second lieu, l'article 1er de la loi du 9 juillet 2020 organisant la sortie de l'état d'urgence sanitaire dispose que : " le Premier ministre peut, par décret pris sur le rapport du ministre chargé de la santé et aux seules fins de lutter contre la propagation de l'épidémie de covid-19 : / 1° Réglementer ou (...) interdire la circulation des personnes et des véhicules (...) 2° Réglementer l'ouverture au public (...) d'une ou de plusieurs catégories d'établissements recevant du public (...) 3° (...) réglementer les rassemblements de personnes (...) 4° Imposer aux personnes souhaitant se déplacer par transport public (...)de présenter le résultat d'un examen biologique de dépistage virologique ne concluant pas à une contamination par le covid-19 ". Aux termes de l'article 5 de cette loi : " L'article 1er est applicable en Nouvelle-Calédonie et en Polynésie française, sous réserve des adaptations suivantes : / 1° Après le 4° du I, il est inséré un 5° ainsi rédigé : / " 5° Habiliter le haut-commissaire à prendre, dans le strict respect de la répartition des compétences, des mesures de mise en quarantaine des personnes susceptibles d'être affectées et de placement et maintien en isolement des personnes affectées dans les conditions prévues au II des articles L. 3131-15 et L. 3131-17 du code de la santé publique. " ; / 2° Le II est ainsi rédigé : / " II. - Lorsque le Premier ministre prend des mesures mentionnées au I et les rend applicables à la Nouvelle-Calédonie ou à la Polynésie française, il peut habiliter le haut-commissaire à les adapter en fonction des circonstances locales et à prendre toutes les mesures générales ou individuelles d'application de ces dispositions lorsqu'elles relèvent de la compétence de l'Etat, après consultation du Gouvernement de la collectivité. / " Lorsqu'une des mesures mentionnées au même I doit s'appliquer dans un champ géographique qui n'excède pas la Nouvelle-Calédonie ou la Polynésie française, le Premier ministre peut habiliter le haut-commissaire à la décider lui-même, assortie des adaptations nécessaires s'il y a lieu et dans les mêmes conditions qu'au premier alinéa du présent II. " (...) ".<br/>
<br/>
              Sur la recevabilité de la question prioritaire de constitutionnalité relative à l'article L. 3841-2 du code de la santé publique : <br/>
<br/>
              6. L'article 3 de la loi du 23 mars 2020 d'urgence pour faire face à l'épidémie de covid-19 a habilité le gouvernement, dans les conditions prévues à l'article 38 de la Constitution, à prendre, par voie d'ordonnance, dans un délai de deux mois, les mesures destinées à adapter le dispositif de l'état d'urgence sanitaire en Nouvelle-Calédonie. Sur le fondement de cette habilitation, l'ordonnance du 22 avril 2020 adaptant l'état d'urgence à la Nouvelle-Calédonie a inséré au code de la santé publique l'article L. 3841-2 du code de la santé publique. Un projet de loi de ratification de cette ordonnance a été déposé au Sénat le 13 mai 2020. Le délai d'habilitation ayant expiré et les dispositions de l'article L. 3841-2 étant intervenues dans des matières qui sont du domaine législatif, la circonstance que l'ordonnance du 22 avril 2020 n'ait pas encore été ratifiée ne fait pas obstacle à ce que, dans le cadre d'un recours dirigé contre un décret pris pour son application, la question de la conformité des dispositions en cause aux droits et libertés garantis par la Constitution soit transmise au Conseil constitutionnel. <br/>
<br/>
              Sur les questions prioritaires de constitutionnalité soulevées : <br/>
<br/>
              7. Les requérants soutiennent qu'en prévoyant que le chapitre relatif à l'état d'urgence sanitaire et le dispositif relatif à la sortie de ce régime puissent s'appliquer en Nouvelle-Calédonie, d'une part, et en y déclarant l'état d'urgence sanitaire, d'autre part, le législateur a méconnu le caractère irréversible du partage des compétences entre l'Etat et la Nouvelle-Calédonie prévu par les articles 76 et 77 de la Constitution et organisé par loi organique du 19 mars 1999 relative à la Nouvelle Calédonie. <br/>
<br/>
              8. L'article 76 de la Constitution dispose que : " Les populations de la Nouvelle-Calédonie sont appelées à se prononcer avant le 31 décembre 1998 sur les dispositions de l'accord signé à Nouméa le 5 mai 1998 et publié le 27 mai 1998 au Journal officiel de la République française. ", tandis qu'aux termes de son article 77 : " Après approbation de l'accord lors de la consultation prévue à l'article 76, la loi organique, prise après avis de l'assemblée délibérante de la Nouvelle-Calédonie, détermine, pour assurer l'évolution de la Nouvelle-Calédonie dans le respect des orientations définies par cet accord et selon les modalités nécessaires à sa mise en oeuvre : / - les compétences de l'Etat qui seront transférées, de façon définitive, aux institutions de la Nouvelle-Calédonie, l'échelonnement et les modalités de ces transferts, ainsi que la répartition des charges résultant de ceux-ci ; / - les règles d'organisation et de fonctionnement des institutions de la Nouvelle-Calédonie et notamment les conditions dans lesquelles certaines catégories d'actes de l'assemblée délibérante pourront être soumises avant publication au contrôle du Conseil constitutionnel ; / - les règles relatives à la citoyenneté, au régime électoral, à l'emploi et au statut civil coutumier ; / - les conditions et les délais dans lesquels les populations intéressées de la Nouvelle-Calédonie seront amenées à se prononcer sur l'accession à la pleine souveraineté (...) ". Le 3.3. de l'accord de Nouméa stipule quant à lui que : " La justice, l'ordre public, la défense et la monnaie (ainsi que le crédit et les changes), et les affaires étrangères (sous réserve des dispositions du 3.2.1) resteront de la compétence de l'Etat jusqu'à la nouvelle organisation politique résultant de la consultation des populations intéressées prévue au 5 ". Enfin, l'article 21 de la loi organique du 19 mars 1999 dispose que l'Etat est compétent dans les matières suivantes : " 1° (...) garantie des libertés publiques " tandis qu'aux termes de l'article 22 de la loi organique, la Nouvelle-Calédonie est compétente en matière de : " 4° Protection sociale, hygiène publique et santé, contrôle sanitaire aux frontières ". <br/>
<br/>
              9. Les dispositions de l'article L. 3841-2 du code de la santé publique, de l'article 4 de la loi du 23 mars 2020, de l'article 1er de la loi du 11 mai 2020 et de l'article 5 de la loi du 9 juillet 2020 sont applicables au litige engagé devant le Conseil d'Etat et n'ont pas déjà été déclarées conformes à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel. <br/>
<br/>
              10. La déclaration et la prorogation de l'état d'urgence sanitaire en Nouvelle-Calédonie ont eu pour effet d'autoriser le Premier ministre et le ministre de la santé à y édicter, dans les conditions prévues à l'article L. 3841-2 du code de la santé publique, les mesures prévues aux articles L. 3131-15 et L. 3131-16 afin de garantir la santé publique et de mettre fin à la catastrophe sanitaire. L'article 5 de la loi du 9 juillet 2020 a pour effet d'autoriser le Premier ministre à édicter, en Nouvelle Calédonie et dans les conditions qu'il prévoit, les mesures mentionnées à l'article 1er de cette loi dans l'intérêt de la santé publique et aux seules fins de lutter contre la propagation de l'épidémie de covid-19. Les mesures en cause concernent notamment l'organisation et le fonctionnement du dispositif de santé. Dans ces conditions, le grief tiré de ce que les dispositions législatives mises en cause méconnaitraient le caractère irréversible de la répartition des compétences découlant de l'article 77 de la Constitution entre l'Etat et la Nouvelle-Calédonie, laquelle est, aux termes de l'article 22 de la loi organique précité, compétente en matière de santé, soulève une question présentant un caractère nouveau et sérieux. <br/>
<br/>
              11. Il résulte de ce qui précède que, sans qu'il soit besoin de se prononcer sur les autres griefs soulevés par les requérants, il y a lieu de renvoyer au Conseil constitutionnel les questions prioritaires de constitutionnalité mettant en cause l'article L. 3841-2 du code de la santé publique, les articles 4 de la loi du 23 mars 2020 et 1er de la loi du 11 mai 2020 en tant qu'ils sont applicables à la Nouvelle-Calédonie et l'article 5 de la loi du 9 juillet 2020. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La question de la conformité à la Constitution de l'article L. 3841-2 du code de la santé publique, des articles 4 de la loi du 23 mars 2020 et 1er de la loi du 11 mai 2020 en tant qu'ils sont applicables à la Nouvelle-Calédonie et de l'article 5 de la loi du 9 juillet 2020 est renvoyée au Conseil constitutionnel.<br/>
Article 2 : Il est sursis à statuer sur la requête de M. U... et autres jusqu'à ce que le Conseil constitutionnel ait tranché la question prioritaire de constitutionnalité qui lui est renvoyée.<br/>
Article 3 : La présente décision sera notifiée à M. W... U..., représentant désigné, pour l'ensemble des requérants et au ministre des solidarités et de la santé. <br/>
Copie en sera adressée au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-01-045 ACTES LÉGISLATIFS ET ADMINISTRATIFS. DIFFÉRENTES CATÉGORIES D'ACTES. - DISPOSITIONS D'UNE ORDONNANCE NON RATIFIÉE RELEVANT DU DOMAINE DE LA LOI - POSSIBILITÉ DE LES CONTESTER, PAR LA VOIE DE L'EXCEPTION, EN SOULEVANT UNE QPC - EXISTENCE, À L'EXPIRATION DU DÉLAI D'HABILITATION [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-07-01-04-04 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. QUESTIONS GÉNÉRALES. MOYENS. EXCEPTION D'ILLÉGALITÉ. - DISPOSITIONS D'UNE ORDONNANCE NON RATIFIÉE RELEVANT DU DOMAINE DE LA LOI - POSSIBILITÉ DE LES CONTESTER, PAR LA VOIE DE L'EXCEPTION, EN SOULEVANT UNE QPC - EXISTENCE, À L'EXPIRATION DU DÉLAI D'HABILITATION [RJ1].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-10-01-01 PROCÉDURE. - INCLUSION - DISPOSITIONS D'UNE ORDONNANCE NON RATIFIÉE RELEVANT DU DOMAINE DE LA LOI ET CONTESTÉES PAR LA VOIE DE L'EXCEPTION, PASSÉ L'EXPIRATION DU DÉLAI D'HABILITATION [RJ1].
</SCT>
<ANA ID="9A"> 01-01-045 La circonstance qu'une ordonnance n'ait pas encore été ratifiée ne fait pas obstacle, lorsque le délai d'habilitation a expiré, à ce que, dans le cadre d'un recours dirigé contre un décret pris pour son application, la question de la conformité aux droits et libertés garantis par la Constitution de ses dispositions intervenues dans des matières qui sont du domaine législatif soit transmise au Conseil constitutionnel.</ANA>
<ANA ID="9B"> 54-07-01-04-04 La circonstance qu'une ordonnance n'ait pas encore été ratifiée ne fait pas obstacle, lorsque le délai d'habilitation a expiré, à ce que, dans le cadre d'un recours dirigé contre un décret pris pour son application, la question de la conformité aux droits et libertés garantis par la Constitution de ses dispositions intervenues dans des matières qui sont du domaine législatif soit transmise au Conseil constitutionnel.</ANA>
<ANA ID="9C"> 54-10-01-01 La circonstance qu'une ordonnance n'ait pas encore été ratifiée ne fait pas obstacle, lorsque le délai d'habilitation a expiré, à ce que, dans le cadre d'un recours dirigé contre un décret pris pour son application, la question de la conformité aux droits et libertés garantis par la Constitution de ses dispositions intervenues dans des matières qui sont du domaine législatif soit transmise au Conseil constitutionnel.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Ab. jur., dans cette mesure, CE, 13 juillet 2016, Syndicat national des entreprises des loisirs marchands (SNELM) et autres, n° 396170, T. pp. 599-917. Rappr. Cons. const., 28 mai 2020, n° 2020-843 QPC ; Cons. const., 3 juillet 2020, n° 2020-851/852 QPC.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
