<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032484118</ID>
<ANCIEN_ID>JG_L_2016_05_000000382282</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/48/41/CETATEXT000032484118.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 4ème chambres réunies, 02/05/2016, 382282</TITRE>
<DATE_DEC>2016-05-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>382282</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Marie Gautier-Melleray</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Laurence Marion</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:382282.20160502</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés au secrétariat de la section du contentieux le 4 juillet 2014 et le 2 avril 2015, la société Vortex demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision n° 2013-790 du 20 décembre 2013 du Conseil supérieur de l'audiovisuel (CSA) en tant que, par son article 2, elle la met en demeure de respecter les dispositions de l'article 8 du décret du 6 avril 1987 et les stipulations de l'article 3-3 de la convention du 2 octobre 2012 définissant les obligations particulières du service Skyrock, ensemble la décision du 6 mai 2014 par laquelle le conseil supérieur a rejeté son recours gracieux ;<br/>
<br/>
              2°) de mettre à la charge du CSA la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - la loi n° 86-1067 du 30 septembre 1986 ;<br/>
<br/>
              - le décret n° 87-239 du 6 avril 1987 ;<br/>
<br/>
              - le décret n° 92-280 du 27 mars 1992 ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie Gautier-Melleray, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Laurence Marion, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'à la suite de la diffusion par le service de radio Skyrock, au cours des émissions " Radio libre " des 2, 4 et 9 septembre, 21 octobre, 4 novembre et 2 décembre 2013, d'une séquence intitulée " Youporn Quiz ", le Conseil supérieur de l'audiovisuel (CSA) a, par une décision du 20 décembre 2013, mis la société Vortex en demeure de respecter à l'avenir, d'une part, l'interdiction de diffuser des programmes à caractère pornographique résultant du dernier alinéa de sa délibération du 10 février 2004 relative à la protection de l'enfance et de l'adolescence à l'antenne des services de radio et, d'autre part, l'interdiction de la publicité clandestine résultant de l'article 8 du décret du 6 avril 1987 et de l'article 3-3 de la convention conclue le 2 octobre 2012 entre le CSA et la société Vortex pour définir les obligations particulières du service Skyrock ; que la société Vortex demande l'annulation pour excès de pouvoir de cette décision, en tant qu'elle la met en demeure de respecter l'interdiction de diffusion de la publicité clandestine, et de la décision du 6 mai 2014 par laquelle le CSA a rejeté le recours gracieux dont elle l'avait saisi ;<br/>
<br/>
              2. Considérant que la décision contestée du 20 décembre 2013 constitue une mise en demeure de respecter à l'avenir les règles qu'elle mentionne ; que la circonstance, alléguée par la société requérante, que cette décision aurait été prise à une date où la diffusion de la séquence " Youporn Quiz " avait déjà pris fin est sans incidence sur sa légalité ; <br/>
<br/>
              3. Considérant qu'aux termes de l'article 8 du décret du 6 avril 1987 pris pour l'application de l'article 27-1 de la loi du 30 septembre 1986 relative à la liberté de communication et fixant pour les services privés de radiodiffusion sonore diffusés par voie hertzienne terrestre ou par satellite le régime applicable à la publicité et au parrainage : " Les messages publicitaires doivent être clairement annoncés et identifiés comme tels " ; que l'article 3-3 de la convention conclue le 2 octobre 2012 entre le CSA et la société Vortex rappelle cette exigence et ajoute que les émissions du service Skyrock " ne doivent pas inciter à l'achat ou à la location de produits ou services par l'intermédiaire de toute personne s'exprimant à l'antenne " ; <br/>
<br/>
              4. Considérant qu'il ressort des pièces du dossier que si le CSA a, dans une recommandation adoptée le 3 janvier 2013, indiqué que le nom des réseaux sociaux accessibles par internet peut être mentionné librement dans les programmes de radio lorsque ces réseaux constituent la source de l'information commentée, le site " Youporn ", qui permet aux internautes d'accéder à des films à caractère pornographique, ne saurait en tout état de cause être regardé, eu égard à sa nature, comme une source d'information dans le cadre de la séquence incriminée ;<br/>
<br/>
              5. Considérant que la  séquence intervenant dans le cadre de l'émission dite " Radio libre " et qui a donné lieu à la mise en demeure litigieuse, consistait en la diffusion à l'antenne d'extraits sonores de films pornographiques accessibles sur le site internet " Youporn ", en vue de faire deviner par les auditeurs et les animateurs de l'émission, le pays dans lequel ces films avaient été tournés ; qu'il ressort des comptes rendus d'écoutes que le nom du site internet, qui figurait dans le titre de la séquence, était mentionné à de très nombreuses reprises au cours de celle-ci et que le contenu du site était décrit et faisait l'objet d'une présentation favorable ; que cette séquence était manifestement de nature à assurer la promotion de ce site, dont les ressources financières viennent de la publicité et dépendent de son audience, auprès d'auditeurs qui étaient incités à le consulter ; qu'alors même qu'aucun élément ne permettait d'établir que sa diffusion avait donné lieu à une contrepartie, le CSA l'a exactement qualifiée en estimant qu'elle présentait, au sens des dispositions rappelées ci-dessus, le caractère d'un message publicitaire non identifié comme tel ; qu'il a, dès lors, pu légalement mettre la société Vortex en demeure de respecter à l'avenir les prescriptions de l'article 8 du décret du 6 avril 1987 et de l'article 3-1 de la convention du 2 octobre 2012 ; <br/>
<br/>
              6. Considérant que la circonstance alléguée que d'autres opérateurs n'auraient  fait l'objet que d'une simple mise en garde pour des faits similaires est sans incidence sur la légalité de la décision attaquée ; <br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que la requérante n'est pas fondée à demander l'annulation de la décision attaquée ;<br/>
<br/>
              8. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge du CSA qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
		Article 1er : La requête de la société Vortex est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à la société Vortex et au Conseil supérieur de l'audiovisuel. <br/>
Copie en sera adressée pour information à la ministre de la culture et de la communication.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">56-02-02 RADIO ET TÉLÉVISION. RÈGLES GÉNÉRALES. PUBLICITÉ. - NOTION DE PUBLICITÉ - INCLUSION - SÉQUENCE D'UNE ÉMISSION DE NATURE À ASSURER LA PROMOTION D'UN SITE INTERNET AUPRÈS D'AUDITEURS INCITÉS À LE CONSULTER, ALORS MÊME QU'AUCUN ÉLÉMENT NE PERMET D'ÉTABLIR QUE SA DIFFUSION A DONNÉ LIEU À CONTREPARTIE.
</SCT>
<ANA ID="9A"> 56-02-02 Séquence radiophonique consistant en la diffusion à l'antenne d'extraits sonores de films pornographiques accessibles sur un site internet, en vue de faire deviner par les auditeurs et les animateurs de l'émission le pays dans lequel ces films avaient été tournés. Il ressort des comptes rendus d'écoutes que le nom du site internet, qui figurait dans le titre de la séquence, était mentionné à de très nombreuses reprises au cours de celle-ci et que le contenu du site était décrit et faisait l'objet d'une présentation favorable. Cette séquence était manifestement de nature à assurer la promotion de ce site, dont les ressources financières viennent de la publicité et dépendent de son audience, auprès d'auditeurs qui étaient incités à le consulter. Alors même qu'aucun élément ne permettait d'établir que sa diffusion avait donné lieu à une contrepartie, le Conseil supérieur de l'audiovisuel (CSA) l'a exactement qualifiée en estimant qu'elle présentait le caractère d'un message publicitaire non identifié comme tel au sens de l'article 8 du décret n° 87-239 du 6 avril 1987.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
