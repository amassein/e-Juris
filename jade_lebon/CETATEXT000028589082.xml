<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028589082</ID>
<ANCIEN_ID>JG_L_2014_02_000000365644</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/58/90/CETATEXT000028589082.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème et 7ème sous-sections réunies, 12/02/2014, 365644, Publié au recueil Lebon</TITRE>
<DATE_DEC>2014-02-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>365644</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème et 7ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>Mme Airelle Niepce</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Béatrice Bourgeois-Machureau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:365644.20140212</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 30 janvier 2013 au secrétariat du contentieux du Conseil d'Etat, présenté par le ministre de l'intérieur ; le ministre demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 12PA01407 du 27 novembre 2012 par lequel la cour administrative d'appel de Paris a, à la demande de M. B...A..., annulé le jugement n° 1103402/1 du 4 novembre 2011 par lequel le tribunal administratif de Melun avait rejeté sa demande tendant à l'annulation de l'arrêté du 26 mars 2011 du préfet de police prononçant son expulsion du territoire français et annulé cet arrêté ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de M. A...;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
	Vu la note en délibéré, enregistrée le 12 février 2014, présentée par le ministre de l'intérieur ;<br/>
<br/>
              Vu le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
<br/>
              Vu la loi n° 91-647 du 10 juillet 1991 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Airelle Niepce, Maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Béatrice Bourgeois-Machureau, Rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de M. A...; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'aux termes de l'article L. 521-1 du code de l'entrée et du séjour des étrangers et du droit d'asile : " Sous réserve des dispositions des articles L. 521-2, L. 521-3 et L. 521-4, l'expulsion peut être prononcée si la présence en France d'un étranger constitue une menace grave pour l'ordre public. " ;<br/>
<br/>
              2.	Considérant que les infractions pénales commises par un étranger ne sauraient, à elles seules, justifier légalement une mesure d'expulsion et ne dispensent pas l'autorité compétente d'examiner, d'après l'ensemble des circonstances de l'affaire, si la présence de l'intéressé sur le territoire français est de nature à constituer une menace grave pour l'ordre public ; que lorsque l'administration se fonde sur l'existence d'une telle menace pour prononcer l'expulsion d'un étranger, il appartient au juge de l'excès de pouvoir, saisi d'un moyen en ce sens, de rechercher si les faits qu'elle invoque à cet égard sont de nature à justifier légalement sa décision ;<br/>
<br/>
              3.	Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M.A..., de nationalité mauritanienne, a fait l'objet d'un arrêté d'expulsion du préfet de police le 26 mars 2011, sur le fondement des dispositions de l'article L. 521-1 du code de l'entrée et du séjour des étrangers et du droit d'asile ; que pour justifier que la présence de l'intéressé sur le territoire constituait une menace grave pour l'ordre public, le préfet de police s'est fondé sur sept condamnations pénales dont il aurait été l'objet entre 2004 et 2009, les faits les ayant motivés et le comportement d'ensemble de l'intéressé ; <br/>
<br/>
              4.	Considérant que pour annuler l'arrêté du préfet de police, la cour administrative d'appel de Paris a relevé que le casier judiciaire de M. A...faisait état de quatre condamnations entre le 11 mai 2004 et le 1er juillet 2009 ; que l'intéressé avait été relevé des peines complémentaires d'interdiction du territoire français initialement prononcées à son encontre ; que la condamnation dont il a fait l'objet pour des faits de recel de biens provenant d'un vol concernait l'acquisition d'un téléphone portable volé ; que sa condamnation, en 2007, à dix mois d'emprisonnement pour des faits d'agression sexuelle concernait également une infraction à la législation sur les étrangers, était ancienne et n'avait pas été suivie de faits de même nature ; que les autres condamnations prononcées à son encontre étaient exclusivement consécutives à sa présence irrégulière sur le territoire ; <br/>
<br/>
              5.	Considérant qu'en déduisant de ces différents éléments, en l'absence de précision sur les faits ayant mené à la condamnation de M. A...pour agression sexuelle, qu'à la date de l'arrêté litigieux, la présence de l'intéressé en France n'était pas de nature à constituer une menace grave pour l'ordre public et, par voie de conséquence, que la décision du préfet était entachée d'erreur d'appréciation, la cour, qui a suffisamment motivé son arrêt et n'a pas dénaturé les faits de l'espèce, n'a pas inexactement qualifié les faits dont elle était saisie ;<br/>
<br/>
              6.	Considérant qu'il résulte de ce qui précède que le ministre de l'intérieur n'est pas fondé à demander l'annulation de l'arrêt de la cour administrative d'appel de Paris qu'il attaque ;<br/>
<br/>
              7.	Considérant M. A...a obtenu le bénéfice de l'aide juridictionnelle ; que, par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ; qu'il y a lieu, dans les circonstances de l'espèce, et sous réserve que la SCP Waquet-Farge-Hazan, son avocat, renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à cette SCP ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le pourvoi du ministre de l'intérieur est rejeté.<br/>
<br/>
Article 2 : L'Etat versera à la SCP Waquet-Farge-Hazan, avocat de M.A..., une somme de 3 000 euros en application des dispositions du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat. <br/>
<br/>
Article 3 : La présente décision sera notifiée au ministre de l'intérieur et à M. B...A....<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">335-02 ÉTRANGERS. EXPULSION. - CONTENTIEUX - 1) CONTRÔLE DU JUGE DE L'EXCÈS DE POUVOIR SUR L'EXISTENCE D'UNE MENACE GRAVE À L'ORDRE PUBLIC - CONTRÔLE NORMAL [RJ1] - 2) CONTRÔLE DU JUGE DE CASSATION SUR L'APPRÉCIATION PORTÉE SUR CE POINT PAR LES JUGES DU FOND - CONTRÔLE DE QUALIFICATION JURIDIQUE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">335-02-03 ÉTRANGERS. EXPULSION. MOTIFS. - COMMISSION D'INFRACTIONS PÉNALES - MOTIF INSUSCEPTIBLE, À LUI SEUL, DE FONDER UNE MESURE D'EXPULSION [RJ2].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-07-02-03 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. CONTRÔLE DU JUGE DE L'EXCÈS DE POUVOIR. APPRÉCIATIONS SOUMISES À UN CONTRÔLE NORMAL. - EXISTENCE D'UNE MENACE GRAVE À L'ORDRE PUBLIC JUSTIFIANT UNE MESURE D'EXPULSION [RJ1].
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">54-08-02-02-01-02 PROCÉDURE. VOIES DE RECOURS. CASSATION. CONTRÔLE DU JUGE DE CASSATION. BIEN-FONDÉ. QUALIFICATION JURIDIQUE DES FAITS. - EXISTENCE D'UNE MENACE GRAVE À L'ORDRE PUBLIC JUSTIFIANT UNE MESURE D'EXPULSION.
</SCT>
<ANA ID="9A"> 335-02 1) Lorsque l'administration se fonde sur l'existence d'une menace grave à l'ordre public pour prononcer l'expulsion d'un étranger, il appartient au juge de l'excès de pouvoir, saisi d'un moyen en ce sens, de rechercher si les faits qu'elle invoque à cet égard sont de nature à justifier légalement sa décision.,,,2) Le juge de cassation exerce un contrôle de qualification juridique sur l'appréciation portée par les juges du fond sur le point de savoir si la présence de l'intéressé en France est de nature à constituer une menace grave pour l'ordre public.</ANA>
<ANA ID="9B"> 335-02-03 Les infractions pénales commises par un étranger ne sauraient, à elles seules, justifier légalement une mesure d'expulsion et ne dispensent pas l'autorité compétente d'examiner, d'après l'ensemble des circonstances de l'affaire, si la présence de l'intéressé sur le territoire français est de nature à constituer une menace grave pour l'ordre public.</ANA>
<ANA ID="9C"> 54-07-02-03 Lorsque l'administration se fonde sur l'existence d'une menace grave à l'ordre public pour prononcer l'expulsion d'un étranger, il appartient au juge de l'excès de pouvoir, saisi d'un moyen en ce sens, de rechercher si les faits qu'elle invoque à cet égard sont de nature à justifier légalement sa décision.</ANA>
<ANA ID="9D"> 54-08-02-02-01-02 Le juge de cassation exerce un contrôle de qualification juridique sur l'appréciation portée par les juges du fond sur le point de savoir si la présence de l'intéressé en France est de nature à constituer une menace grave pour l'ordre public justifiant son expulsion.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., pour un refus de titre de séjour opposé pour ce motif, CE, Section, 17 octobre 2003,,, n° 249183, p. 413.,,[RJ2] Cf. CE, Assemblée, 21 janvier 1977, Ministre de l'intérieur c/,, n° 01333, p. 38.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
