<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028161249</ID>
<ANCIEN_ID>JG_L_2013_11_000000352480</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/16/12/CETATEXT000028161249.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 4ème sous-sections réunies, 06/11/2013, 352480</TITRE>
<DATE_DEC>2013-11-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>352480</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 4ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LE GRIEL ; SCP CELICE, BLANCPAIN, SOLTNER</AVOCATS>
<RAPPORTEUR>Mme Marie Gautier-Melleray</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:352480.20131106</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 7 septembre 2011 et 6 décembre 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. B... A..., demeurant au... ; M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 05LY01633 du 7 juillet 2011 par lequel la cour administrative d'appel de Lyon a condamné le centre hospitalier de Montceau-les-Mines à lui verser la somme de 44 201,72 euros, ramenée à 4 201,72 euros après déduction de la provision de 40 000 euros déjà versée, en tant qu'il statue sur ses conclusions tendant à la réparation de son préjudice économique ;<br/>
<br/>
              2°) réglant l'affaire au fond, de condamner le centre hospitalier de Montceau-les-Mines à lui verser la somme de 888 813,36 euros au titre de son préjudice économique, assorti des intérêts au taux légal ; <br/>
<br/>
              3°) de mettre à la charge du centre hospitalier de Montceau-les-Mines le versement d'une somme de 16 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie Gautier-Melleray, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Le Griel, avocat de M. A...et à la SCP Célice, Blancpain, Soltner, avocat du centre hospitalier de Montceau-les-Mines ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, le 18 mai 1999, M. A..., entrepreneur individuel exerçant l'activité de pépiniériste, s'est blessé au bras gauche en tombant d'un camion ; qu'il a été admis le même jour au service des urgences du centre hospitalier de Montceau-les-Mines qui a diagnostiqué une fracture simple de la tête radiale du coude gauche et a immobilisé le bras par une attelle ; qu'après la pose d'un plâtre sans radiographie de contrôle, une radiographie réalisée ultérieurement a révélé une fracture complexe de la tête radiale du coude qui a rendu nécessaire une intervention chirurgicale ; que l'apparition d'une nécrose a nécessité six autres interventions chirurgicales entre janvier 2000 et février 2009 ; que M. A...a recherché devant le juge administratif la responsabilité du centre hospitalier de Montceau-les-Mines au titre de l'erreur de diagnostic commise lors de sa prise en charge dans cet établissement ; que la cour administrative d'appel de Lyon, par un arrêt du 7 juillet 2011 réformant un jugement du 21 juillet 2005 du tribunal administratif de Dijon, a jugé que le centre hospitalier avait commis une faute ayant entraîné pour l'intéressé une perte de chance, évaluée à 85 %, d'échapper aux complications qu'il avait connues ; que M. A... se pourvoit en cassation contre cet arrêt en tant qu'il limite à 20 486,72 euros l'indemnisation de son préjudice économique ;<br/>
<br/>
              2. Considérant qu'après avoir relevé, d'une part, que les périodes d'incapacité subies par M. A...entre le 6 juillet 2005, date de la troisième intervention chirurgicale liée à la fracture du coude, et le 5 août 2009, date de consolidation de son état de santé, étaient en relation directe avec la faute du centre hospitalier et, d'autre part, que M.A...  consacrait la moitié de son temps de travail à des activités manuelles, la cour administrative d'appel a alloué au requérant, en réparation de son préjudice économique, une indemnité correspondant  au coût du recrutement d'un ouvrier agricole spécialisé employé à mi-temps ; qu'en statuant ainsi, alors qu'il lui appartenait d'évaluer, au titre des périodes d'incapacité, les pertes de revenus subies par l'intéressé directement liées aux complications auxquelles la faute du centre hospitalier lui avait fait perdre une chance d'échapper, puis de lui accorder une indemnité calculée sur cette base par application du coefficient de perte de chance qu'elle avait retenu, la cour a méconnu le principe de la réparation intégrale du préjudice et entaché son arrêt d'erreur de droit  ; qu'il suit de là que M. A...est fondé à en demander l'annulation, en tant qu'il statue sur ses conclusions tendant à l'indemnisation de son préjudice économique ; <br/>
<br/>
              3. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du centre hospitalier de Montceau-les-Mines la somme de 3 000 euros à verser à M. A..., au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt n° 05LY01633 de la cour administrative d'appel de Lyon du 7 juillet 2011 est annulé en tant qu'il statue sur les conclusions de M. A...tendant à l'indemnisation de son préjudice économique.<br/>
<br/>
Article 2 : L'affaire est renvoyée, dans la limite de la cassation ainsi prononcée, à la cour administrative d'appel de Lyon.<br/>
<br/>
Article 3 : Le centre hospitalier de Montceau-les-Mines versera à M. A...la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article  4 : La présente décision sera notifiée à M. B...A...et au centre hospitalier de Montceau-les-Mines.<br/>
Copie en sera adressée à la caisse de mutualité sociale agricole de Saône-et-Loire et à la société d'assurances Groupama Rhône-Alpes.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">60-04-03-02-01-01 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RÉPARATION. ÉVALUATION DU PRÉJUDICE. PRÉJUDICE MATÉRIEL. PERTE DE REVENUS. PERTE DE REVENUS SUBIE PAR LA VICTIME D'UN ACCIDENT. - INCAPACITÉ DUE À UNE FAUTE MÉDICALE AYANT FAIT PERDRE À L'INTÉRESSÉ UNE CHANCE D'ÉCHAPPER À DES COMPLICATIONS - INDEMNISATION DU PRÉJUDICE ÉCONOMIQUE - MÉTHODE - EVALUATION DES PERTES DE REVENUS DIRECTEMENT LIÉES AUX COMPLICATIONS ET APPLICATION DU COEFFICIENT DE PERTE DE CHANCE - CONSÉQUENCE - CAS D'UNE VICTIME CONSACRANT LA MOITIÉ DE SON TEMPS DE TRAVAIL À DES ACTIVITÉS MANUELLES - JURIDICTION LUI AYANT ALLOUÉ UNE INDEMNITÉ CORRESPONDANT AU COÛT DE RECRUTEMENT D'UN COLLABORATEUR À MI-TEMPS - MÉCONNAISSANCE DU PRINCIPE DE RÉPARATION INTÉGRALE DU PRÉJUDICE - EXISTENCE.
</SCT>
<ANA ID="9A"> 60-04-03-02-01-01 Cas d'un requérant ayant, du fait d'une faute d'un centre hospitalier commise à l'occasion d'une intervention chirurgicale, perdu une chance d'échapper à des complications médicales et subi de ce fait un préjudice économique du fait de périodes d'incapacité d'exercer son activité professionnelle.... ,,Il appartenait au juge d'évaluer, au titre des périodes d'incapacité, les pertes de revenus subies par l'intéressé directement liées aux complications auxquelles la faute du centre hospitalier lui avait fait perdre une chance d'échapper, puis de lui accorder une indemnité calculée sur cette base par application du coefficient de perte de chance retenu.,,,Dès lors, la cour administrative d'appel qui, après avoir relevé, d'une part, que les périodes d'incapacité étaient en relation directe avec la faute du centre hospitalier et, d'autre part, que l'intéressé consacrait la moitié de son temps de travail à des activités manuelles, a alloué au requérant, en réparation de son préjudice économique, une indemnité correspondant  au coût du recrutement d'un ouvrier agricole spécialisé employé à mi-temps, a méconnu le principe de réparation intégrale du préjudice.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
