<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041493376</ID>
<ANCIEN_ID>JG_L_2020_01_000000423529</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/49/33/CETATEXT000041493376.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 1ère chambres réunies, 27/01/2020, 423529</TITRE>
<DATE_DEC>2020-01-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>423529</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO ; SCP RICARD, BENDEL-VASSEUR, GHNASSIA ; SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Olivier  Fuchs</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CECHR:2020:423529.20200127</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Les sociétés AG-Zinate, Les Charmes et Sodipaz ont demandé à la cour administrative d'appel de Nantes d'annuler pour excès de pouvoir l'arrêté du 9 mai 2017 par lequel le maire de Sainte-Pazanne a accordé à la société Pazadis un permis de construire un ensemble commercial d'une surface de vente de 5 800 m2 sur le territoire de la commune. Par un arrêt n° 17NT03012 du 29 juin 2018, la cour administrative d'appel a rejeté leur requête. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 23 août et 19 novembre 2018 au secrétariat du contentieux du Conseil d'Etat, les sociétés Sodipaz, AG-Zinate et Les Charmes demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur requête ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Sainte-Pazanne et de la société Pazadis la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code de commerce ;<br/>
              - le code de l'urbanisme ;<br/>
              - la loi n° 2014-626 du 18 juin 2014 ;<br/>
              - la loi n° 2015-990 du 6 août 2015 ;<br/>
              - le décret n° 2015-165 du 12 février 2015 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Fuchs, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de la société Sodipaz, de la société AG-Zinate et de la société Les Charmes, à la SCP Ricard, Bendel-Vasseur, Ghnassia, avocat de la commune de Sainte-Pazanne et à la SCP Piwnica, Molinié, avocat de la société Pazadis ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que par une décision du 17 septembre 2013, la commission départementale d'aménagement commercial de Loire-Atlantique a autorisé la société Pazadis à exploiter un ensemble commercial d'une surface de vente de 5 800 m2 sur le territoire de la commune de Sainte-Pazanne. Par une décision du 18 décembre 2013, la Commission nationale d'aménagement commercial a annulé cette autorisation et rejeté la demande de la société Pazadis. Cette décision ayant toutefois été annulée par un arrêt du 7 mai 2015 de la cour administrative d'appel de Nantes, la Commission nationale d'aménagement commercial a procédé au réexamen de la demande de la société Pazadis et s'est prononcée favorablement sur le projet le 3 mars 2016. Par arrêté du 9 mai 2017, le maire de Sainte-Pazanne a accordé à la société Pazadis un permis de construire. La société Sodipaz, la société AG-Zinate et la société Les Charmes se pourvoient en cassation contre l'arrêt du 29 juin 2018 de la cour administrative d'appel de Nantes rejetant leur requête tendant à l'annulation de l'arrêté du 9 mai 2017. <br/>
<br/>
              2. Aux termes de l'article L. 752-1 du code de commerce : " Sont soumis à une autorisation d'exploitation commerciale les projets ayant pour objet : / 1° La création d'un magasin de commerce de détail d'une surface de vente supérieure à 1 000 mètres carrés, résultant soit d'une construction nouvelle, soit de la transformation d'un immeuble existant (...) ". Aux termes de l'article L. 425-4 du code de l'urbanisme, issu de la loi du 18 juin 2014 relative à l'urbanisme commercial : " Lorsque le projet est soumis à autorisation d'exploitation commerciale au sens de l'article L. 752-1 du code de commerce, le permis de construire tient lieu d'autorisation dès lors que la demande de permis a fait l'objet d'un avis favorable de la commission d'aménagement commercial ou, le cas échéant, de la Commission nationale d'aménagement commercial./ Une modification du projet qui revêt un caractère substantiel, au sens de l'article L. 752-15 du même code, mais n'a pas d'effet sur la conformité des travaux projetés par rapport aux dispositions législatives et réglementaires mentionnées à l'article L. 421-6 du présent code nécessite une nouvelle demande d'autorisation d'exploitation commerciale auprès de la commission départementale". En vertu des termes mêmes de l'article 6 du décret du 12 février 2015 relatif à l'aménagement commercial, ces dispositions sont entrées en vigueur le 15 février 2015. Toutefois, lorsqu'à la suite d'une annulation contentieuse d'une décision de la Commission nationale d'aménagement commercial antérieure au 15 février 2015, celle-ci statue à nouveau sur la demande d'autorisation commerciale dont elle se retrouve saisie du fait de cette annulation, l'acte par lequel elle se prononce sur le projet d'équipement commercial a le caractère d'une décision, susceptible de recours pour excès de pouvoir, et non d'un avis, à la condition qu'il n'ait été apporté au projet aucune modification substantielle au regard des règles dont la commission nationale doit faire application. Il en va ainsi même si la Commission nationale d'aménagement commercial se prononce à nouveau après le 15 février 2015.<br/>
<br/>
              Sur l'arrêt attaqué, en tant qu'il est relatif à l'autorisation d'exploitation commerciale :<br/>
<br/>
              3. Il ressort des pièces du dossier soumis aux juges du fond que, contrairement à ce qui est soutenu par les sociétés requérantes, la Commission nationale d'aménagement commercial s'est prononcée sur le même projet d'équipement commercial que celui initialement présenté par la société Pazadis, lorsqu'elle a procédé, après l'annulation de sa décision du 18 décembre 2013, au réexamen de la demande de cette société. En particulier, si la surface totale de l'ensemble immobilier dont la construction a été autorisée par cet arrêté était inférieure de 2000 m2 à celle du projet autorisé par le premier permis de construire, en raison de la suppression d'un magasin initialement envisagé, le projet d'équipement commercial sur lequel la Commission nationale de l'aménagement commercial s'est prononcée le 3 mars 2016 ne comportait pas ce magasin de meubles, qui avait fait l'objet d'une procédure distincte au titre de la législation relative aux autorisations d'exploitation commerciale. Il résulte de ce qui est dit au point 2 ci-dessus que l'acte par lequel la Commission nationale d'aménagement commercial a statué à nouveau sur la demande d'autorisation de la société Pazadis, le 3 mars 2016, après l'annulation par la cour administrative d'appel de Nantes de sa décision de refus d'autorisation du 18 décembre 2013, présente dès lors le caractère, non d'un avis, mais d'une décision susceptible de faire l'objet d'un recours pour excès de pouvoir. Par suite, le permis de construire attaqué, délivré le 9 mai 2017, ne pouvait pas faire l'objet d'un recours en tant qu'acte valant autorisation d'exploitation commerciale, seule la décision de la commission nationale pouvant être contestée en tant que telle. Les conclusions par lesquelles les sociétés requérantes demandaient à la cour administrative d'appel de Nantes d'annuler ce permis en tant qu'il vaut autorisation d'exploitation commerciale étaient, par conséquent, irrecevables. Ce motif, qui est d'ordre public et dont l'examen n'implique l'appréciation d'aucune circonstance de fait, doit être substitué aux motifs de droit erronés retenus par l'arrêt attaqué, dont il justifie le dispositif en tant qu'il rejette ces conclusions, sans que les requérantes ne soient fondées à soutenir qu'une telle substitution de motifs porte atteinte, par elle-même, à leur droit à un recours effectif tel que garanti par les stipulations de l'article 6, paragraphe 1 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales. <br/>
<br/>
              4. Toutefois, par un arrêt n° 16NT01460, devenu définitif, du même jour que l'arrêt attaqué la cour administrative d'appel de Nantes, qui avait également été saisie par la société Sodipaz d'une requête tendant à l'annulation de la décision de la Commission nationale d'aménagement commercial du 3 mars 2016, a rejeté cette requête comme irrecevable, au motif que l'acte attaqué n'était pas susceptible de recours et que seul le permis de construire, valant autorisation d'exploitation commerciale, pouvait faire l'objet d'un recours contentieux. En raison de la contrariété entre ce qui est dit au point 3 et l'arrêt n° 16NT01460 du 29 juin 2018, conduisant à un déni de justice, il y a lieu, en vertu des pouvoirs généraux de régulation de l'ordre juridictionnel administratif dont le Conseil d'Etat statuant au contentieux est investi, de déclarer cet arrêt nul et non avenu et de renvoyer à la cour administrative d'appel de Nantes le jugement de la requête de la société Sodipaz tendant à l'annulation de la décision de la Commission nationale d'aménagement commercial du 3 mars 2016.<br/>
<br/>
              Sur l'arrêt attaqué, en tant qu'il est relatif à l'autorisation d'urbanisme :<br/>
<br/>
              5. Aux termes de l'article L. 600-1-2 du code de l'urbanisme : " Une personne autre que l'Etat, les collectivités territoriales ou leurs groupements ou une association n'est recevable à former un recours pour excès de pouvoir contre un permis de construire, de démolir ou d'aménager que si la construction, l'aménagement ou les travaux sont de nature à affecter directement les conditions d'occupation, d'utilisation ou de jouissance du bien qu'elle détient ou occupe régulièrement ou pour lequel elle bénéficie d'une promesse de vente, de bail, ou d'un contrat préliminaire mentionné à l'article L. 261-15 du code de la construction et de l'habitation ". Il résulte de ces dispositions qu'il appartient, en particulier, à tout requérant qui saisit le juge administratif d'un recours pour excès de pouvoir tendant à l'annulation d'un permis de construire, de démolir ou d'aménager, de préciser l'atteinte qu'il invoque pour justifier d'un intérêt lui donnant qualité pour agir, en faisant état de tous éléments suffisamment précis et étayés de nature à établir que cette atteinte est susceptible d'affecter directement les conditions d'occupation, d'utilisation ou de jouissance de son bien. Il appartient au défendeur, s'il entend contester l'intérêt à agir du requérant, d'apporter tous éléments de nature à établir que les atteintes alléguées sont dépourvues de réalité. Le juge de l'excès de pouvoir apprécie la recevabilité de la requête au vu des éléments ainsi versés au dossier par les parties, en écartant le cas échéant les allégations qu'il jugerait insuffisamment étayées mais sans pour autant exiger de l'auteur du recours qu'il apporte la preuve du caractère certain des atteintes qu'il invoque au soutien de la recevabilité de celui-ci. Eu égard à sa situation particulière, le voisin immédiat justifie, en principe, d'un intérêt à agir lorsqu'il fait état devant le juge, qui statue au vu de l'ensemble des pièces du dossier, d'éléments relatifs à la nature, à l'importance ou à la localisation du projet de construction.<br/>
<br/>
              6. Il ressort des pièces du dossier soumis aux juges du fond, d'une part, que la société AG-Zinate est propriétaire d'un terrain non construit situé à moins de 200 mètres du terrain d'assiette du projet litigieux, d'autre part que la société Les Charmes est propriétaire d'un équipement commercial situé à moins de 150 mètres de ce terrain. La cour administrative d'appel de Nantes, qui a nécessairement considéré que ces deux sociétés ne pouvaient être regardées comme des voisines immédiates du projet, a relevé que ces deux sociétés se bornaient à faire valoir la proximité de leurs terrains et les nuisances susceptibles d'être causées par le projet, sans apporter d'éléments suffisamment précis de nature à établir qu'il en serait résulté une atteinte directe aux conditions d'occupation, d'utilisation ou de jouissance de leur bien, sur lesquelles elles n'avaient apporté aucune précision. En estimant qu'ainsi, elles ne justifiaient pas d'un intérêt leur donnant qualité à demander l'annulation du permis de construire litigieux, la cour n'a ni commis d'erreur de droit, ni inexactement qualifié les faits de l'espèce, qu'elle a souverainement appréciés sans les dénaturer.<br/>
<br/>
              7. Si la cour s'est également référée à la circonstance que le projet de construction aurait été connu de la société AG-Zinate lorsque celle-ci a acquis la parcelle de terrain en cause, il résulte de ce qui précède que ce motif ne peut qu'être regardé, quel qu'en soit le bien-fondé, comme surabondant. Il ne peut, dès lors, être utilement critiqué. <br/>
<br/>
              8. Il résulte de tout ce qui précède que le pourvoi des sociétés Sodipaz, AG-Zinate et Les Charmes doit être rejeté. <br/>
<br/>
              9. Dans les circonstances de l'espèce, il n'y a pas lieu faire droit aux conclusions présentées par les parties au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi des sociétés Sodipaz, AG-Zinate et Les Charmes est rejeté.<br/>
Article 2 : L'arrêt n° 16NT01460 de la cour administrative d'appel de Nantes est déclaré nul et non avenu. <br/>
Article 3 : Le jugement de la requête de la société Sodipaz dirigée contre la décision de la Commission nationale d'aménagement commercial du 3 mars 2016 est renvoyé à la cour administrative d'appel de Nantes. <br/>
<br/>
Article 4 : Les conclusions présentées par la société Pazadis et la commune de Sainte-Pazanne au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée aux sociétés Sodipaz, AG-Zinate, Les Charmes, à la commune de Sainte-Pazanne, à la société Pazadis et au ministre de l'économie et des finances.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">14-02-01-05-02 COMMERCE, INDUSTRIE, INTERVENTION ÉCONOMIQUE DE LA PUISSANCE PUBLIQUE. RÉGLEMENTATION DES ACTIVITÉS ÉCONOMIQUES. ACTIVITÉS SOUMISES À RÉGLEMENTATION. AMÉNAGEMENT COMMERCIAL. PROCÉDURE. - DÉCISION DE LA CNAC PRISE À LA SUITE DE L'ANNULATION D'UNE DÉCISION PRÉCÉDENTE ANTÉRIEURE À LA DATE D'ENTRÉE EN VIGUEUR DE LA LOI DU 18 JUIN 2014 - ACTE SUSCEPTIBLE DE RECOURS - EXISTENCE, À LA CONDITION QU'IL N'AIT ÉTÉ APPORTÉ AU PROJET AUCUNE MODIFICATION SUBSTANTIELLE, ET MÊME SI LA NOUVELLE DÉCISION DE LA CNAC EST INTERVENUE APRÈS CETTE DATE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-01-04-02-01 PROCÉDURE. INTRODUCTION DE L'INSTANCE. INTÉRÊT POUR AGIR. EXISTENCE D'UN INTÉRÊT. INTÉRÊT LIÉ À UNE QUALITÉ PARTICULIÈRE. - DISPOSITIONS SPÉCIFIQUES AU CONTENTIEUX DE L'URBANISME (ART. L. 600-1-2 DU CODE DE L'URBANISME) - VOISIN IMMÉDIAT [RJ1] - ESPÈCE - EXCLUSION.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-08-08 PROCÉDURE. VOIES DE RECOURS. RÈGLEMENT DE JUGES. - DÉCISION DU CONSEIL D'ETAT REJETANT POUR IRRECEVABILITÉ DES CONCLUSIONS DIRIGÉES CONTRE UN PERMIS DE CONSTRUIRE EN TANT QU'IL VAUT AUTORISATION D'EXPLOITATION COMMERCIALE, AU MOTIF QUE SEULE LA DÉCISION DE LA CNAC POUVAIT ÊTRE CONTESTÉE EN TANT QUE TELLE [RJ1] - ARRÊT REJETANT POUR IRRECEVABILITÉ DES CONCLUSIONS DIRIGÉES CONTRE LA DÉCISION DE LA CNAC AU MOTIF QUE SEUL LE PERMIS DE CONSTRUIRE EN TANT QU'IL VAUT AUTORISATION D'EXPLOITATION COMMERCIALE POUVAIT FAIRE L'OBJET D'UN RECOURS - CONTRARIÉTÉ CONDUISANT À UN DÉNI DE JUSTICE - ARRÊT DÉCLARÉ NUL ET NON AVENU [RJ2].
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">68-06-01-02 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. INTRODUCTION DE L'INSTANCE. INTÉRÊT À AGIR. - ARTICLE L. 600-1-2 DU CODE DE L'URBANISME - VOISIN IMMÉDIAT [RJ1] - ESPÈCE - EXCLUSION.
</SCT>
<ANA ID="9A"> 14-02-01-05-02 Lorsqu'à la suite d'une annulation contentieuse d'une décision de la Commission nationale d'aménagement commercial (CNAC) antérieure à la date d'entrée en vigueur de la loi n° 2014-626 du 18 juin 2014, soit le 15 février 2015, celle-ci statue à nouveau sur la demande d'autorisation commerciale dont elle se retrouve saisie du fait de cette annulation, l'acte par lequel elle se prononce sur le projet d'équipement commercial a le caractère d'une décision, susceptible de recours pour excès de pouvoir, et non d'un avis, à la condition qu'il n'ait été apporté au projet aucune modification substantielle au regard des règles dont la commission nationale doit faire application. Il en va ainsi même si la Commission nationale d'aménagement commercial se prononce à nouveau après le 15 février 2015.</ANA>
<ANA ID="9B"> 54-01-04-02-01 Il résulte de l'article L. 600-1-2 du code de l'urbanisme qu'il appartient, en particulier, à tout requérant qui saisit le juge administratif d'un recours pour excès de pouvoir tendant à l'annulation d'un permis de construire, de démolir ou d'aménager, de préciser l'atteinte qu'il invoque pour justifier d'un intérêt lui donnant qualité pour agir, en faisant état de tous éléments suffisamment précis et étayés de nature à établir que cette atteinte est susceptible d'affecter directement les conditions d'occupation, d'utilisation ou de jouissance de son bien. Il appartient au défendeur, s'il entend contester l'intérêt à agir du requérant, d'apporter tous éléments de nature à établir que les atteintes alléguées sont dépourvues de réalité. Le juge de l'excès de pouvoir apprécie la recevabilité de la requête au vu des éléments ainsi versés au dossier par les parties, en écartant le cas échéant les allégations qu'il jugerait insuffisamment étayées mais sans pour autant exiger de l'auteur du recours qu'il apporte la preuve du caractère certain des atteintes qu'il invoque au soutien de la recevabilité de celui-ci. Eu égard à sa situation particulière, le voisin immédiat justifie, en principe, d'un intérêt à agir lorsqu'il fait état devant le juge, qui statue au vu de l'ensemble des pièces du dossier, d'éléments relatifs à la nature, à l'importance ou à la localisation du projet de construction.,,,Sociétés requérantes propriétaires, d'une part, d'un terrain non construit situé à moins de 200 mètres du terrain d'assiette du projet litigieux, d'autre part, d'un équipement commercial situé à moins de 150 mètres de ce terrain.... ,,La cour administrative d'appel, qui a nécessairement considéré que ces deux sociétés ne pouvaient être regardées comme des voisines immédiates du projet, a relevé qu'elles se bornaient à faire valoir la proximité de leurs terrains et les nuisances susceptibles d'être causées par le projet, sans apporter d'éléments suffisamment précis de nature à établir qu'il en serait résulté une atteinte directe aux conditions d'occupation, d'utilisation ou de jouissance de leur bien, sur lesquelles elles n'avaient apporté aucune précision. En estimant qu'ainsi, elles ne justifiaient pas d'un intérêt leur donnant qualité à demander l'annulation du permis de construire litigieux, la cour n'a ni commis d'erreur de droit, ni inexactement qualifié les faits de l'espèce, qu'elle a souverainement appréciés sans les dénaturer.</ANA>
<ANA ID="9C"> 54-08-08 L'acte par lequel la Commission nationale d'aménagement commercial (CNAC) a statué à nouveau sur la demande d'autorisation de la société, le 3 mars 2016, après l'annulation par la cour administrative d'appel de sa décision de refus d'autorisation du 18 décembre 2013, présente le caractère, non d'un avis, mais d'une décision susceptible de faire l'objet d'un recours pour excès de pouvoir. Par suite, le permis de construire attaqué, délivré le 9 mai 2017, ne pouvait pas faire l'objet d'un recours en tant qu'acte valant autorisation d'exploitation commerciale, seule la décision de la commission nationale pouvant être contestée en tant que telle. Les conclusions par lesquelles les sociétés requérantes demandaient à la cour administrative d'appel d'annuler ce permis en tant qu'il vaut autorisation d'exploitation commerciale étaient, par conséquent, irrecevables.... ,,Toutefois, par un arrêt, devenu définitif, du même jour que l'arrêt attaqué, la cour administrative d'appel, qui avait également été saisie par la même société d'une requête tendant à l'annulation de la décision de la CNAC du 3 mars 2016, a rejeté cette requête comme irrecevable, au motif que l'acte attaqué n'était pas susceptible de recours et que seul le permis de construire, valant autorisation d'exploitation commerciale, pouvait faire l'objet d'un recours contentieux. En raison de la contrariété entre ce qui est dit au point précédent et cet arrêt, conduisant à un déni de justice, il y a lieu, en vertu des pouvoirs généraux de régulation de l'ordre juridictionnel administratif dont le Conseil d'Etat statuant au contentieux est investi, de déclarer cet arrêt nul et non avenu et de renvoyer à la cour administrative d'appel le jugement de la requête de la société tendant à l'annulation de la décision de la CNAC du 3 mars 2016.</ANA>
<ANA ID="9D"> 68-06-01-02 Il résulte de l'article L. 600-1-2 du code de l'urbanisme qu'il appartient, en particulier, à tout requérant qui saisit le juge administratif d'un recours pour excès de pouvoir tendant à l'annulation d'un permis de construire, de démolir ou d'aménager, de préciser l'atteinte qu'il invoque pour justifier d'un intérêt lui donnant qualité pour agir, en faisant état de tous éléments suffisamment précis et étayés de nature à établir que cette atteinte est susceptible d'affecter directement les conditions d'occupation, d'utilisation ou de jouissance de son bien. Il appartient au défendeur, s'il entend contester l'intérêt à agir du requérant, d'apporter tous éléments de nature à établir que les atteintes alléguées sont dépourvues de réalité. Le juge de l'excès de pouvoir apprécie la recevabilité de la requête au vu des éléments ainsi versés au dossier par les parties, en écartant le cas échéant les allégations qu'il jugerait insuffisamment étayées mais sans pour autant exiger de l'auteur du recours qu'il apporte la preuve du caractère certain des atteintes qu'il invoque au soutien de la recevabilité de celui-ci. Eu égard à sa situation particulière, le voisin immédiat justifie, en principe, d'un intérêt à agir lorsqu'il fait état devant le juge, qui statue au vu de l'ensemble des pièces du dossier, d'éléments relatifs à la nature, à l'importance ou à la localisation du projet de construction.,,,Sociétés requérantes propriétaires, d'une part, d'un terrain non construit situé à moins de 200 mètres du terrain d'assiette du projet litigieux, d'autre part, d'un équipement commercial situé à moins de 150 mètres de ce terrain.... ,,La cour administrative d'appel, qui a nécessairement considéré que ces deux sociétés ne pouvaient être regardées comme des voisines immédiates du projet, a relevé qu'elles se bornaient à faire valoir la proximité de leurs terrains et les nuisances susceptibles d'être causées par le projet, sans apporter d'éléments suffisamment précis de nature à établir qu'il en serait résulté une atteinte directe aux conditions d'occupation, d'utilisation ou de jouissance de leur bien, sur lesquelles elles n'avaient apporté aucune précision. En estimant qu'ainsi, elles ne justifiaient pas d'un intérêt leur donnant qualité à demander l'annulation du permis de construire litigieux, la cour n'a ni commis d'erreur de droit, ni inexactement qualifié les faits de l'espèce, qu'elle a souverainement appréciés sans les dénaturer.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 13 avril 2016, M.,, n° 389798, p. 135.,,[RJ2] Rappr. CE, 14 mars 1986, Ministre des P.T.T. c/ Consorts,, p. 73 ; CE, 12 février 1990, Commune de Bain-de-Bretagne, p. 33.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
