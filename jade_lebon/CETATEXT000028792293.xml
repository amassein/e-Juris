<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028792293</ID>
<ANCIEN_ID>JG_L_2014_03_000000351884</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/79/22/CETATEXT000028792293.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème / 9ème SSR, 28/03/2014, 351884</TITRE>
<DATE_DEC>2014-03-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>351884</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème / 9ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BORE, SALVE DE BRUNETON ; SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>M. Romain Godet</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:351884.20140328</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 12 août 2011 et 14 novembre 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour la société Camping de la Yole, dont le siège est Domaine de la Yole à Vendres (34350), représentée par son président directeur général en exercice ; la société Camping de la Yole demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 09MA02907 du 16 juin 2011 par lequel la cour administrative d'appel de Marseille, faisant droit à l'appel de la commune de Vendres, a annulé le jugement n° 0701591 du 28 mai 2009 du tribunal administratif de Montpellier en tant qu'il a annulé la clause financière du permis de construire délivré à la société le 5 février 2007 et relative au programme d'aménagement d'ensemble de cette commune ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
              3°) de mettre à la charge de la commune de Vendres une somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'urbanisme ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Romain Godet, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la Société Camping De La Yole  et à la SCP Boré, Salve de Bruneton, avocat de la commune de vendres ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que, en application des délibérations du conseil municipal du 8 décembre 1987, approuvant le programme d'aménagement d'ensemble du secteur littoral de la commune et instaurant une participation à la charge des constructeurs, et du 12 juillet 1996, décidant d'étendre le périmètre du programme d'aménagement d'ensemble, de compléter le programme des équipements publics initialement autorisés et de réviser le régime de la participation, le maire de Vendres a délivré à la société Camping de la Yole, le 5 février 2007, un permis de construire comportant des dispositions mettant à la charge de la société une participation au financement des équipements publics définis dans le programme d'aménagement d'ensemble de la commune ainsi que des taxes départementales ; que la société Camping de la Yole se pourvoit en cassation contre l'arrêt de la cour administrative d'appel de Marseille qui, faisant droit à l'appel de la commune de Vendres, a, d'une, part annulé le jugement du tribunal administratif de Montpellier en tant que ce dernier a annulé les dispositions du permis de construire relatives à la participation au financement des équipements publics et, d'autre part, rejeté les conclusions de son appel incident tendant à l'annulation des autres dispositions du permis relatives aux taxes départementales ;<br/>
<br/>
              2. Considérant, en premier lieu, que les dispositions du permis de construire dont la société requérante demandait, par la voie de l'appel incident, l'annulation sont relatives à la taxe départementale pour le financement des conseils d'architecture, d'urbanisme et de l'environnement ainsi qu'à la taxe départementale des espaces naturels sensibles ; que si ces prélèvements partagent avec la participation au financement des équipements publics définis dans le cadre du plan d'aménagement d'ensemble de la commune un même fait générateur, ils constituent néanmoins des prélèvements différents ; que, dès lors, la cour, qui a suffisamment motivé son arrêt, n'a pas commis d'erreur de droit en considérant que les conclusions de la société Camping de la Yole relevaient d'un litige distinct et étaient irrecevables, car tardives ;<br/>
<br/>
              3. Considérant, en deuxième lieu, qu'il résulte des dispositions de l'article L. 332-9 du code de l'urbanisme, dans sa rédaction applicable au litige, que la délibération du conseil municipal instituant un plan d'aménagement d'ensemble et mettant à la charge des constructeurs une participation au financement des équipements publics à réaliser doit identifier avec précision les aménagements prévus ainsi que leur coût prévisionnel et déterminer la part de ce coût mise à la charge des constructeurs, afin de permettre le contrôle du bien-fondé du montant de la participation mise à la charge de chaque constructeur ; que ces dispositions impliquent également, afin de permettre la répartition de la participation entre les constructeurs, que la délibération procède à une estimation quantitative des surfaces dont la construction est projetée à la date de la délibération et qui serviront de base à cette répartition ;<br/>
<br/>
              4. Considérant que, pour juger que les délibérations du conseil municipal de Vendres du 8 décembre 1987 et du 12 juillet 1996 pouvaient légalement fonder la participation mise à la charge de la société Camping de la Yole en application de l'article L. 332-9 précité, la cour s'est bornée à relever, d'une part, que le périmètre du programme d'aménagement d'ensemble avait bien été délimité et, d'autre part, que le programme de construction et l'implantation des équipements étaient suffisamment précisés dans les délibérations ainsi que dans les plan et documents y annexés ; que cependant, en s'abstenant de rechercher si le plan d'aménagement d'ensemble décrivait le programme des travaux tant par leur consistance que par leur implantation en faisant état de données physiques telles que la surface, pour les bâtiments, ou tout autre élément de nature à permettre d'apprécier le bien-fondé de l'évaluation des travaux mis à la charge des contributeurs ainsi que des modalités selon lesquelles le coût de ces travaux devait être réparti entre les différentes catégories de constructions, la cour a commis une erreur de droit ; que, par suite, sans qu'il soit besoin d'examiner les autres moyens du pourvoi dirigés contre cette partie de l'arrêt, l'arrêt de la cour administrative d'appel de Marseille doit être annulé en tant qu'il a fait droit à l'appel principal de la commune de Vendres ; <br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de Vendres la somme de 2 000 euros à verser à la société Camping de la Yole, au titre des dispositions de l'article L. 761-1 du code de justice administrative ; que ces mêmes dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de la société Camping de la Yole qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Marseille du 16 juin 2011 est annulé en tant qu'il a fait droit à l'appel principal de la commune de Vendres.<br/>
Article 2 : L'affaire est renvoyée, dans la limite de la cassation ainsi prononcée, à la cour administrative d'appel de Marseille.<br/>
Article 3 : La commune de Vendres versera à la société Camping de la Yole la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 ; Le surplus des conclusions du pourvoi de la société Camping de la Yole est rejeté.<br/>
Article 5 : Les conclusions de la commune de Vendres présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 6 : La présente décision sera notifiée à la société Camping de la Yole et à la commune de Vendres.<br/>
Copie sera adressée pour information au ministre de l'écologie, du développement durable et de l'énergie.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">68-024-06 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. CONTRIBUTIONS DES CONSTRUCTEURS AUX DÉPENSES D'ÉQUIPEMENT PUBLIC. PARTICIPATION DANS LE CADRE D'UN PROGRAMME D'AMÉNAGEMENT D'ENSEMBLE. - LÉGALITÉ DE LA DÉLIBÉRATION INSTITUANT UNE PARTICIPATION AU FINANCEMENT DES ÉQUIPEMENTS PUBLICS - CRITÈRES - DEGRÉ DE PRÉCISION EXIGÉ DU PLAN D'AMÉNAGEMENT D'ENSEMBLE [RJ1] - PORTÉE.
</SCT>
<ANA ID="9A"> 68-024-06 Pour que la délibération du conseil municipal instituant un plan d'aménagement d'ensemble et mettant à la charge des constructeurs une participation au financement des équipements publics à réaliser puisse légalement fonder cette participation en application de l'article L. 332-9 du code de l'urbanisme, le plan d'aménagement d'ensemble doit décrire le programme des travaux tant par leur consistance que par leur implantation en faisant état de données physiques telles que la surface, pour les bâtiments, ou tout autre élément de nature à permettre d'apprécier le bien-fondé de l'évaluation des travaux mis à la charge des contributeurs ainsi que des modalités selon lesquelles le coût de ces travaux doit être réparti entre les différentes catégories de constructions.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 28 juillet 2011, Commune de la Garde, n° 324123, p. 436.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
