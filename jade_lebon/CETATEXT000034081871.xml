<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034081871</ID>
<ANCIEN_ID>JG_L_2017_02_000000396809</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/08/18/CETATEXT000034081871.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 24/02/2017, 396809</TITRE>
<DATE_DEC>2017-02-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>396809</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARADUC, DUHAMEL, RAMEIX</AVOCATS>
<RAPPORTEUR>M. François Monteagle</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:396809.20170224</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La commune de Saint-Martin-de-Seignanx a demandé au tribunal administratif de Pau d'annuler pour excès de pouvoir la décision du préfet des Landes du 14 mai 2012 refusant de lui attribuer la première fraction de la dotation de solidarité rurale, ainsi que la décision implicite rejetant son recours gracieux formé le 19 juin 2012.<br/>
<br/>
              Par un jugement n° 1201872 du 18 mars 2014, le tribunal administratif a fait droit à cette demande et annulé la décision litigieuse.<br/>
<br/>
              Par un arrêt n° 14BX01533 du 3 décembre 2015, la cour administrative d'appel de Bordeaux a rejeté l'appel formé par le ministre de l'intérieur contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 5 février et 3 mai 2016 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'intérieur demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des collectivités territoriales ;<br/>
              - le décret n° 2012-717 du 7 mai 2012 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. François Monteagle, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Baraduc, Duhamel, Rameix, avocat de la commune de Saint-Martin de Seignanx ;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Le ministre de l'intérieur se pourvoit en cassation contre l'arrêt du 3 décembre 2015 par lequel la cour administrative d'appel de Bordeaux a rejeté l'appel qu'il avait formé contre le jugement du 18 mars 2014 du tribunal administratif de Pau annulant la décision du préfet des Landes du 14 mai 2012 refusant d'attribuer à la commune de Saint-Martin-de-Seignanx la première fraction de la dotation de solidarité rurale au titre de l'année 2012.<br/>
<br/>
              Sur le moyen tiré de l'erreur de droit que les juges d'appel auraient commise en jugeant que le préfet des Landes n'était pas lié par le rattachement de la commune de Saint-Martin de Seignanx à l'agglomération de Bayonne constaté par l'Institut national de la statistique et des études économiques (INSEE) :<br/>
<br/>
              2. Aux termes de l'article L. 2334-20 du code général des collectivités territoriales : " La dotation de solidarité rurale est attribuée aux communes de moins de 10 000 habitants et à certains chefs-lieux d'arrondissement de moins de 20 000 habitants pour tenir compte, d'une part, des charges qu'ils supportent pour contribuer au maintien de la vie sociale en milieu rural, d'autre part, de l'insuffisance de leurs ressources fiscales. / Cette dotation comporte trois fractions. (...) ". Aux termes de l'article L. 2334-21 du même code : " La première fraction de la dotation de solidarité rurale est attribuée aux communes dont la population représente au moins 15 % de la population du canton et aux communes chefs-lieux de canton./ Ne peuvent être éligibles les communes : / 1° Situées dans une agglomération : / a) Représentant au moins 10 % de la population du département ou comptant plus de 250 000 habitants ; / b) Comptant une commune soit de plus de 100 000 habitants, soit chef-lieu de département (...) ". Aux termes du troisième alinéa de l'article R. 2334-7 du même code, dans sa rédaction modifiée par le décret du 7 mai 2012 relatif aux dotations de l'Etat aux collectivités territoriales et à la péréquation des ressources fiscales : " Pour l'application de l'article L. 2334-21, " agglomération " s'entend au sens d'" unité urbaine ", dont la liste est publiée par l'Institut national de la statistique et des études économiques (...) ".<br/>
<br/>
              3. Il résulte de ces dispositions qu'il appartient au préfet, pour déterminer si une commune est éligible à la première fraction de la dotation de solidarité rurale, de vérifier que celle-ci n'est pas située dans une agglomération répondant aux critères mentionnés à l'article L. 2334-21 du code général des collectivités territoriales. L'article R. 2334-7 du même code invite à cette fin le préfet à se référer à la notion d' " unité urbaine " et à prendre en considération les listes des unités urbaines publiées par l'INSEE. Toutefois cette disposition ne saurait avoir pour effet de lier le préfet dans l'appréciation à laquelle il se livre à cet égard, du seul fait du rattachement par l'INSEE d'une commune à une unité urbaine, dès lors que ce rattachement, en l'absence de publication d'un acte administratif authentifiant la liste des unités urbaines et leur composition, est dépourvu de portée juridique et, pour ce motif, insusceptible d'être discuté devant le juge de l'excès de pouvoir. Par suite, la cour n'a pas commis d'erreur de droit en jugeant que c'était à tort que le préfet n'avait pas cru devoir exercer lui-même son pouvoir d'appréciation et s'était estimé lié par l'inscription de la commune de Saint-Martin-de-Seignanx sur la liste arrêtée par l'INSEE des communes faisant partie de l'unité urbaine de Bayonne. <br/>
<br/>
              Sur les autres moyens du pourvoi :<br/>
<br/>
              4. Le ministre de l'intérieur fait valoir que la cour a omis de statuer sur son moyen d'appel tiré de ce que le tribunal administratif n'avait pas tiré les conséquences de l'évolution de la rédaction de l'article R. 2334-7 du code général des collectivités territoriales. Toutefois, la cour s'est fondée sur cet article dans sa rédaction modifiée par le décret du 7 mai 2012 relatif aux dotations de l'Etat aux collectivités territoriales et à la péréquation des ressources fiscales  et a ainsi implicitement mais nécessairement répondu au moyen soulevé sur ce point par le ministre à l'appui de son appel.<br/>
<br/>
              5. Le ministre de l'intérieur soutient également que la cour a commis une erreur de droit en jugeant que, pour l'application du a) du 1° de l'article L. 2334-21 du code général des collectivités territoriales, la population de l'agglomération bayonnaise devait être rapportée à la population du département des Landes, sur lequel est située la commune de Saint-Martin-de-Seignanx, et non à la population du département des Pyrénées-Atlantiques, lequel inclut le centre de l'agglomération bayonnaise. Il résulte toutefois des termes mêmes de l'arrêt attaqué que ce moyen est dirigé contre un motif présentant un caractère surabondant, par lequel la cour a relevé que le ministre ne critiquait pas devant elle le second motif d'annulation retenu par le tribunal administratif, tiré de ce que le préfet avait méconnu l'article L. 2334-21 du code général des collectivités territoriales. Par suite, et en tout état de cause, le ministre n'est pas fondé à soutenir que la cour aurait sur ce point entaché son arrêt d'une erreur de droit.<br/>
<br/>
              6. Il résulte de tout ce qui précède que le pourvoi du ministre de l'intérieur doit être rejeté. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à la commune de Saint-Martin-de-Seignanx, au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le pourvoi du ministre de l'intérieur est rejeté. <br/>
Article 2 : L'Etat versera une somme de 3 000 euros à la commune de Saint-Martin-de-Seignanx au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée au ministre de l'intérieur et à la commune de Saint-Martin-de-Seignanx.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">135-02-04-03-03 COLLECTIVITÉS TERRITORIALES. COMMUNE. FINANCES COMMUNALES. RECETTES. DOTATIONS. - DOTATION DE SOLIDARITÉ RURALE - PREMIÈRE FRACTION - ELIGIBILITÉ D'UNE COMMUNE - APPRÉCIATION PAR LE PRÉFET - VÉRIFICATION QUE LA COMMUNE N'EST PAS SITUÉE DANS UNE AGGLOMÉRATION RÉPONDANT AUX CRITÈRES MENTIONNÉS À L'ARTICLE L. 2334-21 DU CGCT - RATTACHEMENT PAR L'INSEE À UNE UNITÉ URBAINE - CIRCONSTANCE N'AYANT PAS POUR EFFET DE LIER LE PRÉFET [RJ1].
</SCT>
<ANA ID="9A"> 135-02-04-03-03 Il résulte des articles L. 2334-20, L. 2334-21 et R. 2334-7 du code général des collectivités territoriales (CGCT) qu'il appartient au préfet, pour déterminer si une commune est éligible à la première fraction de la dotation de solidarité rurale, de vérifier que celle-ci n'est pas située dans une agglomération répondant aux critères mentionnés à l'article L. 2334-21 du code général des collectivités territoriales. L'article R. 2334-7 du même code invite à cette fin le préfet à se référer à la notion d' unité urbaine et à prendre en considération les listes des unités urbaines publiées par l'INSEE. Toutefois cette disposition ne saurait avoir pour effet de lier le préfet dans l'appréciation à laquelle il se livre à cet égard, du seul fait du rattachement par l'INSEE d'une commune à une unité urbaine, dès lors que ce rattachement, en l'absence de publication d'un acte administratif authentifiant la liste des unités urbaines et leur composition, est dépourvu de portée juridique et, pour ce motif, insusceptible d'être discuté devant le juge de l'excès de pouvoir [RJ2].</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 20 mars 2013, Ministre de l'intérieur contre Commune de Cysoing, n° 352570, T. p. 467., ,[RJ2] Cf., pour le caractère d'acte insusceptible de recours d'une étude de l'INSEE sur la composition communale des unités urbaines, CE, 18 décembre 1996, Comité de défense des intérêts des habitants de la commune d'Aumontzey, n° 165061, T. pp. 745-1066. Rappr., pour le caractère d'acte insusceptible de recours du choix par l'INSEE d'une méthodologie statistique pour élaborer un indice, CE, 11 mars 2015, Société Dalkia France et autres, n° 383062 et autres, T. p. 787.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
