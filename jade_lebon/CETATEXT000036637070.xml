<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036637070</ID>
<ANCIEN_ID>JG_L_2018_02_000000389518</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/63/70/CETATEXT000036637070.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 6ème chambres réunies, 22/02/2018, 389518</TITRE>
<DATE_DEC>2018-02-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>389518</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DIDIER, PINET ; SCP PIWNICA, MOLINIE ; BALAT</AVOCATS>
<RAPPORTEUR>M. Florian Roussel</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:XX:2018:389518.20180222</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              L'association Comité Anti-amiante Jussieu, la Fédération nationale des accidentés du travail et des handicaps (FNATH) - association des accidentés de la vie, le groupement parisien de la FNATH, l'association Treize Ecolo, l'association Diderot Transparence, la Fédération des syndicats SUD étudiants, M. C...B...et Mme F...D...ont demandé au tribunal administratif de Paris d'annuler l'arrêté du 28 avril 2010 par lequel le préfet de la région d'Ile-de-France, préfet de Paris, a délivré à la SAS Unicité, devenue Udicité, un permis de construire pour un bâtiment universitaire sur l'îlot M5B2 de la ZAC Paris Rive Gauche. Par un jugement n° 1012456 du 2 juillet 2013, le tribunal a fait droit à cette demande.<br/>
<br/>
              Par un arrêt n° 13PA03455, 13PA03474, 13PA03475 du 16 février 2015, la cour administrative d'appel de Paris a rejeté l'appel formé par la SAS Udicité, l'Université Paris-Diderot-Paris 7 et le ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche contre ce jugement. <br/>
<br/>
              1° Sous le numéro 389518, par un pourvoi sommaire un mémoire complémentaire et deux nouveaux mémoires, enregistrés les 16 avril, 17 juillet, 15 septembre 2015 et 19 septembre 2017 au secrétariat du contentieux du Conseil d'Etat, la SAS Udicité demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses conclusions d'appel ;<br/>
<br/>
              3°) de mettre à la charge de l'association Comité Anti-amiante Jussieu, de la FNATH - association des accidentés de la vie, du groupement de la région parisienne de la FNATH, de l'association Treize Ecolo, de l'association Diderot Transparence, de la Fédération des syndicats SUD étudiants, de M. B... et de Mme D...la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              2° Sous le numéro 389651, par un pourvoi sommaire et deux mémoires complémentaires, enregistrés les 21 avril et 21 juillet 2015 et 2 août 2017 au secrétariat du contentieux du Conseil d'Etat, l'Université Paris Diderot- Paris 7 demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses conclusions d'appel ; <br/>
<br/>
              3°) de mettre à la charge de l'association Comité Anti-amiante Jussieu, de la FNATH - association des accidentés de la vie, du groupement de la région parisienne de la FNATH, de l'association Treize Ecolo, de l'association Diderot Transparence, de la Fédération des syndicats SUD étudiants, de M. C... B...et de Mme F...D...la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
<br/>
              - la convention européenne de sauvegarde de droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              - le code de la construction et de l'habitation ;<br/>
<br/>
              - le code de l'urbanisme ;<br/>
<br/>
              - l'arrêté du 25 juin 1980 portant approbation des dispositions générales du règlement de sécurité contre les risques d'incendie et de panique dans les établissements recevant du public ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Florian Roussel, maître des requêtes, <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Didier, Pinet, avocat de la SAS Udicité, à la SCP Piwnica, Molinié, avocat de l'Université Paris Diderot-Paris 7, à Me Balat, avocat du Comité Anti-amiante Jussieu, de la société FNATH - association des accidentés de la vie, de l'association Treize Ecolo, de l'association Diderot transparence, de la Fédération des syndicats SUD étudiants, de M. B... et de Mme D....<br/>
<br/>
              Vu la note en délibéré, enregistrée le 7 février 2018, présentée par le Comité Anti-amiante Jussieu, la FNATH - association des accidentés de la vie, l'association Treize Ecolo, l'association Diderot transparence, la Fédération des syndicats SUD étudiants, M. C...B...et Mme F... D... ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que l'Université Paris Diderot - Paris 7 a conclu le 24 juillet 2009 avec le groupement Udicité un contrat de partenariat portant sur la construction de bâtiments universitaires dans le périmètre de la zone d'aménagement concerté Rive Gauche ; que, par un arrêté du 28 avril 2010, le préfet de région Ile-de-France, préfet de Paris, a délivré à la SAS Unicité un permis de construire en vue de la réalisation du bâtiment " Olympe de Gouges " sur l'îlot M5B2 ; que, par un arrêté du 16 avril 2012, le préfet a délivré à la société, désormais dénommée Udicité, un permis de construire modificatif portant sur ce même bâtiment ; que, par une requête enregistrée le 28 juin 2010, plusieurs associations et particuliers ont saisi le tribunal administratif de Paris d'une demande tendant à l'annulation de l'arrêté du 28 avril 2010 ; que le tribunal a fait droit à leur demande par un jugement du 2 juillet 2013 dont l'Université Paris Diderot - Paris 7, la SAS Udicité et le ministre de l'enseignement supérieur et de la recherche ont relevé appel ; que le préfet de région a délivré, le 23 décembre 2013, un permis de construire modificatif à la SAS Udicité ; que, par un arrêt du 16 février 2015, la cour administrative d'appel de Paris a rejeté les appels ; que les pourvois de la SAS Udicité et l'Université Paris Diderot - Paris 7 sont dirigés contre cet arrêt ; qu'il y a lieu de les joindre pour statuer par une seule décision ;<br/>
<br/>
              Sur la régularité de la procédure de première instance et d'appel :<br/>
<br/>
              2. Considérant, d'une part, qu'en relevant qu'avait été soulevé en première instance un moyen tiré de ce que le dossier de permis n'était pas conforme à l'article R. 123-22 du code de la construction et de l'habitation, pour en déduire que ce moyen n'avait pas été relevé d'office par le tribunal administratif dans des conditions irrégulières, la cour ne s'est pas méprise sur la portée des écritures de l'association Comité Anti-amiante Jussieu et autres devant les premiers juges ;<br/>
<br/>
              3. Considérant, d'autre part, qu'aux termes de l'article R. 611-1 du code de justice administrative : " (...) La requête, le mémoire complémentaire annoncé dans la requête et le premier mémoire de chaque défendeur sont communiqués aux parties avec les pièces jointes (...). / Les répliques, autres mémoires et pièces sont communiqués s'ils contiennent des éléments nouveaux " ; que si la SAS Udicité soutient que la cour administrative d'appel a méconnu ces dispositions en s'abstenant de lui communiquer le troisième mémoire en défense de l'association Comité Anti-amiante Jussieu et autres, alors qu'il comportait des éléments nouveaux, ce moyen n'est assorti d'aucune précision et ne peut donc qu'être écarté ; que, contrairement à ce que la société soutient également, les stipulations de l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales n'imposent pas aux juridictions de communiquer toutes les productions des parties, y compris celles qui sont insusceptibles d'avoir une influence sur la solution du litige ; que, par suite, le moyen tiré de la violation de ces stipulations doit également être écarté ; <br/>
<br/>
              Sur l'intérêt pour agir de l'association Treize Ecolo et de MmeD... :<br/>
<br/>
              4. Considérant, d'une part, qu'il ressort des statuts de l'association Treize Ecolo que celle-ci a notamment pour but d'améliorer, dans le 13ème arrondissement de Paris le cadre de vie des habitants et l'environnement ; qu'en jugeant qu'un tel objet conférait à l'association un intérêt lui donnant qualité pour demander l'annulation du permis de construire litigieux, la cour n'a pas inexactement qualifié les faits de l'espèce ;<br/>
<br/>
              5. Considérant, d'autre part, qu'en jugeant que Mme D...justifiait également d'un intérêt à agir, en sa qualité d'enseignant-chercheur à l'Université Paris 7, au sein de l'unité de formation et de recherche " Géographie, histoire et sciences de la société " qui a vocation à occuper le bâtiment M5B2, la cour n'a commis ni d'erreur de droit ni d'erreur de qualification juridique ;<br/>
<br/>
              Sur les motifs d'annulation du permis de construire retenus par la cour :<br/>
<br/>
              En ce qui concerne l'insuffisance du dossier de sécurité :<br/>
<br/>
              6. Considérant que la circonstance que le dossier de demande de permis de construire ne comporterait pas l'ensemble des documents exigés par les dispositions du code de l'urbanisme, ou que les documents produits seraient insuffisants, imprécis ou comporteraient des inexactitudes, n'est susceptible d'entacher d'illégalité le permis de construire qui a été accordé que dans le cas où les omissions, inexactitudes ou insuffisances entachant le dossier ont été de nature à fausser l'appréciation portée par l'autorité administrative sur la conformité du projet à la réglementation applicable ;<br/>
<br/>
              7. Considérant qu'aux termes de l'article L. 111-8 du code de la construction et de l'habitation : " Les travaux qui conduisent à la création, l'aménagement ou la modification d'un établissement recevant du public ne peuvent être exécutés qu'après autorisation délivrée par l'autorité administrative qui vérifie leur conformité aux règles prévues aux articles L. 111-7, L. 123-1 et L. 123-2. / Lorsque ces travaux sont soumis à permis de construire, celui-ci tient lieu de cette autorisation dès lors que sa délivrance a fait l'objet d'un accord de l'autorité administrative compétente mentionnée à l'alinéa précédent. (...) " ; qu'aux termes de l'article R. 123-2 du même code : " Pour l'application du présent chapitre, constituent des établissements recevant du public tous bâtiments, locaux et enceintes dans lesquels des personnes sont admises, soit librement, soit moyennant une rétribution ou une participation quelconque, ou dans lesquels sont tenues des réunions ouvertes à tout venant ou sur invitation, payantes ou non. / Sont considérées comme faisant partie du public toutes les personnes admises dans l'établissement à quelque titre que ce soit en plus du personnel " ; qu'aux termes de l'article GE1 du règlement de sécurité du 25 juin 1980 : " ... Sauf indications contraires, les dispositions du présent livre, relatives aux aménagements et installations techniques, ne s'appliquent qu'aux locaux ouverts au public. Les locaux et dégagements non accessibles au public doivent faire l'objet d'un examen spécial de la commission de sécurité. Selon leur importance, leur destination et leur disposition par rapport aux parties de l'établissement accessibles au public, la commission détermine les dangers qu'ils présentent pour le public et propose éventuellement les mesures de sécurité jugées nécessaire " ;<br/>
<br/>
              8. Considérant que la cour administrative d'appel a retenu qu'en dépit de la mention contraire figurant dans le dossier de sécurité joint à la demande de permis de construire en application des dispositions de l'article R. 431-30 du code de l'urbanisme, il ressortait des pièces du dossier qui lui était soumis que chacun des niveaux du quatrième au huitième étage du bâtiment M5B2 comportait des locaux devant être regardés comme ouverts au public au sens de l'article R. 123-2 du code de la construction dès lors qu'ils étaient destinés à accueillir des personnes admises dans l'établissement en plus du personnel de l'université ou assimilé, tels que des étudiants ; que la cour a jugé que la présentation des faits inexacte figurant dans le dossier joint à la demande, qui n'avait pas été redressée par la commission de sécurité, avait été susceptible de fausser l'appréciation de l'autorité administrative sur la conformité du projet aux règles de sécurité dans le cadre de la délivrance de l'autorisation prévue à l'article L. 111-8 du code de la construction et de l'habitation et entachait par suite d'illégalité le permis de construire qui tenait lieu de cette autorisation en vertu des dispositions du second alinéa de cet article ;<br/>
<br/>
              9. Considérant que, pour juger que certains des locaux des niveaux supérieurs du bâtiment M5B2 étaient accessibles au public, la cour s'est exclusivement référée à leur destination, telle que mentionnée dans les dossiers de demande de permis de construire ; qu'en relevant que les appelants ne contestaient pas sérieusement que la bibliothèque était ouverte à des personnes qui n'étaient pas directement rattachées au fonctionnement des unités de recherche et que les secrétariats pédagogiques de ces unités, situés dans les étages supérieurs, étaient régulièrement consultés par les étudiants, la cour a entendu faire référence non à l'utilisation effective des locaux une fois l'autorisation délivrée mais à la destination normale de ceux-ci ; que, par suite, elle n'a pas entaché son arrêt d'erreur de droit ;<br/>
<br/>
              10. Considérant qu'en se référant aux dispositions de l'article R. 123-2 du code de l'urbanisme pour déterminer si les locaux étaient accessibles au public au sens de l'article GE1 du règlement de sécurité, la cour n'a pas commis d'erreur de droit ; qu'en retenant qu'au sens de ces dispositions les étudiants ne faisaient pas partie du personnel de l'université et ne pouvaient lui être assimilés, la cour en a fait une exacte application ; qu'elle n'a pas dénaturé les faits de l'espèce en estimant qu'eu égard à la destination normale des secrétariats pédagogiques et bibliothèques de recherche, et alors même qu'un badge serait exigé pour y accéder, les locaux correspondants avaient vocation à être fréquentés habituellement par des personnes ne faisant pas partie du personnel de l'université ;<br/>
<br/>
              11. Considérant qu'après avoir constaté que l'appréciation portée par la commission de sécurité et le préfet sur les mesures nécessaires pour assurer la sécurité du public reposait sur des faits inexacts en ce qui concernait l'accessibilité au public de plusieurs des étages des bâtiments en cause, la cour a pu, sans commettre d'erreur de droit, regarder le permis de construire comme étant, de ce seul fait, entaché d'illégalité ; <br/>
<br/>
              En ce qui concerne la méconnaissance des articles CO3 et CO4 du règlement de sécurité :<br/>
<br/>
              12. Considérant qu'aux termes de l'article CO3 du règlement de sécurité du 25 juin 1980 : " § 1. Chaque bâtiment, en fonction de sa hauteur et de l'effectif du public reçu, doit avoir une ou plusieurs façades accessibles, desservies chacune par une voie ou un espace libre suivant les conditions fixées aux articles CO 1 (§ 3), CO 4 et CO 5 ; § 2. Façade accessible : façade permettant aux services de secours d'intervenir à tous les niveaux recevant du public. Elle comporte au moins une sortie normale au niveau d'accès du bâtiment et des baies accessibles à chacun de ses niveaux. (...) " ; qu'aux termes de l'article CO 4 de ce règlement : " a) Etablissements de 1re catégorie recevant plus de 3 500 personnes : Deux façades opposées desservies par deux voies de 12 mètres de large ou trois façades judicieusement réparties et desservies par deux voies de 12 mètres et une voie de 8 mètres de large, les deux conditions suivantes étant toujours réalisées : / 1. La longueur des façades accessibles est supérieure à la moitié du périmètre du bâtiment ; / 2. Tous les locaux recevant du public en étage sont situés sur les façades accessibles ou n'en sont séparés que par de larges dégagements ou zones de circulation. (...) " ;<br/>
<br/>
              13. Considérant que la cour administrative d'appel a retenu que le bâtiment M5B2, établissement de première catégorie recevant plus de 3 500 personnes, n'était desservi ni par deux façades opposées ni par trois façades judicieusement réparties répondant aux conditions posées par l'article CO4 du règlement de sécurité ; qu'en statuant ainsi, après souverainement estimé que les étages supérieurs du bâtiment étaient ouverts au public et, par suite, soumis au respect de la réglementation relative aux établissements recevant du public, la cour, qui a suffisamment motivé son arrêt au regard de la teneur de l'argumentation qui lui était soumise, n'a pas commis d'erreur de droit ;<br/>
<br/>
              Sur l'application de l'article L. 600-5-1 du code de l'urbanisme :<br/>
<br/>
              14. Considérant qu'aux termes de l'article L. 600-5-1 du code de l'urbanisme : " Le juge administratif qui, saisi de conclusions dirigées contre un permis de construire, de démolir ou d'aménager, estime, après avoir constaté que les autres moyens ne sont pas fondés, qu'un vice entraînant l'illégalité de cet acte est susceptible d'être régularisé par un permis modificatif peut, après avoir invité les parties à présenter leurs observations, surseoir à statuer jusqu'à l'expiration du délai qu'il fixe pour cette régularisation. Si un tel permis modificatif est notifié dans ce délai au juge, celui-ci statue après avoir invité les parties à présenter leurs observations " ;<br/>
<br/>
              15. Considérant qu'il résulte de ces dispositions que, lorsque le juge estime que le permis de construire, de démolir ou d'aménager qui lui est déféré est entaché d'un vice entraînant son illégalité mais susceptible d'être régularisé par la délivrance d'un permis modificatif, il peut, de sa propre initiative ou à la demande d'une partie, après avoir invité les parties à présenter leurs observations sur le principe de l'application de l'article L. 600-5-1 du code de l'urbanisme, constater, par une décision avant-dire droit, que les autres moyens ne sont pas fondés et surseoir à statuer jusqu'à l'expiration du délai qu'il fixe pour permettre, selon les modalités qu'il détermine, la régularisation du vice qu'il a relevé ; que le juge peut mettre en oeuvre les pouvoirs qu'il tient de l'article L. 600-5-1 du code de l'urbanisme pour la première fois en appel, alors même que l'autorisation d'urbanisme en cause a été annulée par les premiers juges ;<br/>
<br/>
              16. Considérant que, dans le cas où l'administration lui transmet spontanément des éléments visant à la régularisation d'un vice de nature à entraîner l'annulation du permis attaqué, le juge peut se fonder sur ces éléments sans être tenu de surseoir à statuer, dès lors qu'il a préalablement invité les parties à présenter leurs observations sur la question de savoir si ces éléments permettent une régularisation en application de l'article L. 600-5-1 du code de l'urbanisme ; que, toutefois, si les éléments spontanément transmis ne sont pas suffisants pour permettre de regarder le vice comme régularisé, le juge peut, dans les conditions rappelées au point précédent, notamment après avoir invité les parties à présenter leurs observations sur le principe de l'application de l'article L. 600-5-1 du code de l'urbanisme, surseoir à statuer en vue d'obtenir l'ensemble des éléments permettant la régularisation ; <br/>
<br/>
              17. Considérant qu'il résulte de ce qui précède qu'en jugeant, d'une part, que le permis modificatif du 23 décembre 2013, délivré à seule fin de tirer les conséquences du jugement du 2 juillet 2013, n'était, par principe, pas susceptible de régulariser les illégalités affectant le permis de construire initial et de rendre inopérants les moyens tirés de ces illégalités et, d'autre part, que la délivrance de ce même permis faisait obstacle à ce qu'il soit fait droit aux conclusions de la société présentées sur le fondement de l'article L. 600-5-1 du code de l'urbanisme, la cour administrative d'appel de Paris a commis une erreur de droit ;<br/>
<br/>
              18. Considérant qu'il résulte de tout ce qui précède qu'il n'y a pas lieu d'annuler l'arrêt attaqué en tant qu'il juge que le permis litigieux est entaché des vices rappelés ci-dessus ; qu'il y a lieu en revanche de l'annuler en tant qu'il rejette les conclusions de la SAS Udicité tendant à l'application des dispositions de l'article L. 600-5-1 du code de l'urbanisme et rejette en conséquence les appels dirigés contre le jugement du tribunal administratif annulant le permis, et en tant qu'il statue sur les conclusions présentées devant la cour au titre de l'article L. 761-1 du code de justice administrative ; que, dans les circonstances de l'espèce, il y a lieu de renvoyer l'affaire à la cour administrative d'appel de Paris, afin qu'elle se prononce à nouveau sur les conclusions tendant à l'application des dispositions de l'article L. 600-5-1 ;  que si la cour, après avoir recueilli les observations des parties, constate que ces vices ont été régularisés par un permis modificatif, ou envisage de surseoir à statuer en fixant un délai en vue de leur régularisation, il lui appartiendra de se prononcer sur le bien-fondé des moyens invoqués par les demandeurs de première instance autres que ceux qu'elle a accueillis par son arrêt du 16 février 2015 ; <br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              19. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'association Comité Anti-amiante Jussieu, de la FNATH - association des accidentés de la vie, de l'association Treize Ecolo, de l'association Diderot Transparence, de la Fédération des syndicats SUD étudiants, de M. B...et de Mme D...la somme de 400 euros chacun, à verser à la SAS Udicité et à l'Université Paris Diderot - Paris 7 au titre de l'article L. 761-1 du code de justice administrative ; que les conclusions par lesquelles la SAS Udicité et à l'Université Paris Diderot - Paris 7 demandent qu'une somme soit également mise à la charge du groupement de la région parisienne de la FNATH, qui s'est désisté de sa requête devant le tribunal administratif, doivent en revanche être rejetées ; que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de la SAS Udicité et de l'Université Paris Diderot - Paris 7, qui ne sont pas dans la présente instance les parties perdantes ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt n° 13PA03455, 13PA03474, 13PA03475 de la cour administrative d'appel de Paris du 16 février 2015 est annulé en tant qu'il rejette les conclusions tendant à l'application des dispositions de l'article L. 600-5-1 du code de l'urbanisme et rejette en conséquence les appels dirigés contre le jugement du tribunal administratif de Paris et en tant qu'il statue sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 2 : L'affaire est renvoyée, dans la mesure de la cassation prononcée, à la cour administrative d'appel de Paris.<br/>
<br/>
Article 3 : L'association Comité Anti-amiante Jussieu, la FNATH - association des accidentés de la vie, l'association Treize Ecolo, l'association Diderot Transparence, la Fédération des syndicats SUD étudiants, M. B...et Mme D...verseront chacun des sommes de 400 euros à l'Université Paris Diderot - Paris 7 et à la SAS Udicité, au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : Le surplus des conclusions des pourvois et les conclusions présentées par l'association Comité Anti-amiante Jussieu, la FNATH - association des accidentés de la vie, l'association Treize Ecolo, l'association Diderot Transparence, la Fédération des syndicats SUD étudiants, M. B... et Mme D...au titre de l'article L. 761-1 du code de justice administrative sont rejetés. <br/>
<br/>
Article 5 : La présente décision sera notifiée à la SAS Udicité, à l'Université Paris Diderot - Paris 7, à l'association Comité Anti-amiante Jussieu, premier dénommé pour tous ses cosignataires.<br/>
Copie sera adressée à la FNATH - association des accidentés de la vie - groupement de la région parisienne, à M. A...E..., au ministre de la cohésion des territoires et au ministre de l'enseignement supérieur, de la recherche et de l'innovation.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-08-01 PROCÉDURE. VOIES DE RECOURS. APPEL. - SURSIS À STATUER EN VUE DE PERMETTRE LA RÉGULARISATION D'UN VICE ENTACHANT UNE AUTORISATION D'URBANISME (ART. L. 600-5-1 DU CODE DE L'URBANISME) - CASSATION PARTIELLE D'UN ARRÊT EN TANT QU'IL REJETTE LES CONCLUSIONS TENDANT À L'APPLICATION DE L'ARTICLE L. 600-5-1 DU CODE DE URBANISME - OFFICE DU JUGE D'APPEL APRÈS RENVOI PAR LE CONSEIL D'ETAT.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-08-02-03 PROCÉDURE. VOIES DE RECOURS. CASSATION. POUVOIRS DU JUGE DE CASSATION. - SURSIS À STATUER EN VUE DE PERMETTRE LA RÉGULARISATION D'UN VICE ENTACHANT UNE AUTORISATION D'URBANISME (ART. L. 600-5-1 DU CODE DE L'URBANISME) - FACULTÉ POUR LE JUGE DE CASSATION D'ANNULER L'ARRÊT EN TANT QU'IL REJETTE LES CONCLUSIONS TENDANT À L'APPLICATION DE L'ARTICLE L. 600-5-1 - EXISTENCE - OFFICE DU JUGE D'APPEL APRÈS RENVOI PAR LE CONSEIL D'ETAT.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">68-06-04 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. POUVOIRS DU JUGE. - SURSIS À STATUER EN VUE DE PERMETTRE LA RÉGULARISATION D'UN VICE ENTACHANT UNE AUTORISATION D'URBANISME (ART. L. 600-5-1 DU CODE DE L'URBANISME) - 1) FACULTÉ DE METTRE EN OEUVRE L'ARTICLE L. 600-5-1 POUR LA PREMIÈRE FOIS EN APPEL - EXISTENCE [RJ1] - 2) PRISE EN COMPTE PAR LE JUGE DES ÉLÉMENTS SPONTANÉMENT PRODUITS PAR L'ADMINISTRATION EN VUE DE LA RÉGULARISATION - EXISTENCE - CONDITIONS [RJ1] - 3) FACULTÉ POUR LE JUGE DE CASSATION D'ANNULER L'ARRÊT EN TANT QU'IL REJETTE LES CONCLUSIONS TENDANT À L'APPLICATION DE L'ARTICLE L. 600-5-1 - EXISTENCE - OFFICE DU JUGE D'APPEL APRÈS RENVOI PAR LE CONSEIL D'ETAT.
</SCT>
<ANA ID="9A"> 54-08-01 Le juge de cassation a la faculté, lorsqu'il censure une erreur commise par les juges du fond dans la mise en oeuvre de l'article L. 600-5-1 du code de l'urbanisme, d'annuler l'arrêt attaqué en tant qu'il rejette les conclusions présentées par le bénéficiaire du permis litigieux tendant à l'application de cet article et de laisser subsister cet arrêt en tant qu'il juge que le permis est entaché de divers vices. Si la cour administrative d'appel à qui l'affaire est renvoyée après cassation afin qu'elle se prononce à nouveau sur les conclusions tendant à l'application des dispositions de l'article L. 600-5-1 constate, après avoir recueilli les observations des parties, que les vices ont été régularisés par un permis modificatif, ou envisage de surseoir à statuer en fixant un délai en vue de leur régularisation, il lui appartiendra de se prononcer sur le bien-fondé des moyens invoqués par les demandeurs de première instance autres que ceux qu'elle a accueillis par son premier arrêt.</ANA>
<ANA ID="9B"> 54-08-02-03 Le juge de cassation a la faculté, lorsqu'il censure une erreur commise par les juges du fond dans la mise en oeuvre de l'article L. 600-5-1 du code de l'urbanisme, d'annuler l'arrêt attaqué en tant qu'il rejette les conclusions présentées par le bénéficiaire du permis litigieux tendant à l'application de cet article et de laisser subsister cet arrêt en tant qu'il juge que le permis est entaché de divers vices. Si la cour administrative d'appel à qui l'affaire est renvoyée après cassation afin qu'elle se prononce à nouveau sur les conclusions tendant à l'application des dispositions de l'article L. 600-5-1 constate, après avoir recueilli les observations des parties, que les vices ont été régularisés par un permis modificatif, ou envisage de surseoir à statuer en fixant un délai en vue de leur régularisation, il lui appartiendra de se prononcer sur le bien-fondé des moyens invoqués par les demandeurs de première instance autres que ceux qu'elle a accueillis par son premier arrêt.</ANA>
<ANA ID="9C"> 68-06-04 1) Il résulte de l'article L. 600-5-1 du code de l'urbanisme que, lorsque le juge estime que le permis de construire, de démolir ou d'aménager qui lui est déféré est entaché d'un vice entraînant son illégalité mais susceptible d'être régularisé par la délivrance d'un permis modificatif, il peut, de sa propre initiative ou à la demande d'une partie, après avoir invité les parties à présenter leurs observations sur le principe de l'application de l'article L. 600-5-1 du code de l'urbanisme, constater, par une décision avant-dire droit, que les autres moyens ne sont pas fondés et surseoir à statuer jusqu'à l'expiration du délai qu'il fixe pour permettre, selon les modalités qu'il détermine, la régularisation du vice qu'il a relevé. Le juge peut mettre en oeuvre les pouvoirs qu'il tient de l'article L. 600-5-1 du code de l'urbanisme pour la première fois en appel, alors même que l'autorisation d'urbanisme en cause a été annulée par les premiers juges.... ,,2) Dans le cas où l'administration lui transmet spontanément des éléments visant à la régularisation d'un vice de nature à entraîner l'annulation du permis attaqué, le juge peut se fonder sur ces éléments sans être tenu de surseoir à statuer, dès lors qu'il a préalablement invité les parties à présenter leurs observations sur la question de savoir si ces éléments permettent une régularisation en application de l'article L. 600-5-1 du code de l'urbanisme. Toutefois, si les éléments spontanément transmis ne sont pas suffisants pour permettre de regarder le vice comme régularisé, le juge peut, notamment après avoir invité les parties à présenter leurs observations sur le principe de l'application de l'article L. 600-5-1 du code de l'urbanisme, surseoir à statuer en vue d'obtenir l'ensemble des éléments permettant la régularisation.... ,,3) Le juge de cassation a la faculté, lorsqu'il censure une erreur commise par les juges du fond dans la mise en oeuvre de l'article L. 600-5-1 du code de l'urbanisme, d'annuler l'arrêt attaqué en tant qu'il rejette les conclusions présentées par le bénéficiaire du permis litigieux tendant à l'application de cet article et de laisser subsister cet arrêt en tant qu'il juge que le permis est entaché de divers vices. Si la cour administrative d'appel à qui l'affaire est renvoyée après cassation afin qu'elle se prononce à nouveau sur les conclusions tendant à l'application des dispositions de l'article L. 600-5-1 constate, après avoir recueilli les observations des parties, que les vices ont été régularisés par un permis modificatif, ou envisage de surseoir à statuer en fixant un délai en vue de leur régularisation, il lui appartiendra de se prononcer sur le bien-fondé des moyens invoqués par les demandeurs de première instance autres que ceux qu'elle a accueillis par son premier arrêt.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., s'agissant de l'article L. 600-9 du code de l'urbanisme, CE, Section, 22 décembre 2017, Commune de Sempy c/ M. Merlot, n° 395963, p. 380. Cf. CE, décision du même jour, Société Udicité et Université Paris Diderot Paris 7, n°s 389520 389652, inédite au Recueil.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
