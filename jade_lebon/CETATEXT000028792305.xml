<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028792305</ID>
<ANCIEN_ID>JG_L_2014_03_000000373064</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/79/23/CETATEXT000028792305.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Section du Contentieux, 28/03/2014, 373064, Publié au recueil Lebon</TITRE>
<DATE_DEC>2014-03-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>373064</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Section du Contentieux</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>BLONDEL</AVOCATS>
<RAPPORTEUR>M. Gérald Bégranger</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESEC:2014:373064.20140328</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la requête sommaire, le mémoire complémentaire et le nouveau mémoire, enregistrés les 30 octobre, 15 novembre et 17 décembre 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. B... C..., demeurant... ; M. C... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir les décisions par lesquelles le groupe français de la Cour permanente d'arbitrage a refusé de proposer sa candidature à l'élection des juges à la Cour pénale internationale et a proposé celle de M. A... D... ;<br/>
<br/>
              2°) d'enjoindre à ce groupe de communiquer au Conseil d'Etat le résultat des auditions, réalisées par lui, des candidats à cette élection ;<br/>
<br/>
              3°) d'enjoindre à ce groupe d'organiser une nouvelle procédure tendant à la sélection d'un candidat à cette élection ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention pour le règlement pacifique des conflits internationaux, signée à La Haye le 18 octobre 1907 ;<br/>
<br/>
              Vu le statut de la Cour internationale de justice annexé à la Charte des Nations unies ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu la convention portant statut de la Cour pénale internationale, adoptée à Rome le 17 juillet 1998 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Gérald Bégranger, maître des requêtes en service extraordinaire,<br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Blondel, avocat de M. C... ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article 36 du statut de la Cour pénale internationale résultant de la convention adoptée à Rome le 17 juillet 1998 : " 1. (...) la Cour se compose de 18 juges. / (...) 3. Les juges sont choisis parmi des personnes jouissant d'une haute considération morale, connues pour leur impartialité et leur intégrité et réunissant les conditions requises dans leurs Etats respectifs pour l'exercice des plus hautes fonctions judiciaires. (...) / 4. a) Les candidats à un siège à la Cour peuvent être présentés par tout État Partie au présent Statut : / i) Selon la procédure de présentation de candidatures aux plus hautes fonctions judiciaires dans l'État en question ; ou / ii) Selon la procédure de présentation de candidatures à la Cour internationale de Justice prévue dans le Statut de celle-ci. (...) / 6. a) Les juges sont élus au scrutin secret lors d'une réunion de l'Assemblée des États Parties convoquée à cet effet (...) " ; que le statut de la Cour internationale de justice prévoit en son article 4 que les membres de la Cour sont élus sur une liste de personnes présentées par les groupes nationaux de la Cour permanente d'arbitrage ; que l'article 44 de la convention pour le règlement pacifique des conflits internationaux du 18 octobre 1907, qui fixe la composition de la Cour permanente d'arbitrage, stipule que : " Chaque Puissance contractante désigne quatre personnes au plus, d'une compétence reconnue dans les questions de droit international, jouissant de la plus haute considération morale et disposées à accepter les fonctions d'arbitre " ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier que, conformément à ces stipulations, le groupe français de la Cour permanente d'arbitrage a été chargé de proposer le nom d'un candidat appelé à être présenté, au nom de la France, à l'élection des juges à la Cour pénale internationale ; que, par sa requête, M. C... demande au Conseil d'Etat d'annuler les décisions par lesquelles, à la suite d'un appel à candidatures et des auditions auxquelles il a procédé, ce groupe a écarté sa candidature et proposé le nom d'un autre candidat ;<br/>
<br/>
              3. Considérant que les actes contestés ne sont pas détachables de la procédure d'élection des juges à la Cour pénale internationale par l'Assemblée des Etats parties à la convention portant statut de cette juridiction internationale et échappent, dès lors, à la compétence de la juridiction administrative française ; que, par suite, la requête de M. C... tendant à leur annulation doit être rejetée comme portée devant une juridiction incompétente pour en connaître ;<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : La requête de M. C... est rejetée comme portée devant une juridiction incompétente pour en connaître.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. B... C..., à M. A... D..., au ministre des affaires étrangères et à la garde des sceaux, ministre de la justice. Copie pour information en sera adressée au groupe français de la Cour permanente d'arbitrage.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-01-03 ACTES LÉGISLATIFS ET ADMINISTRATIFS. DIFFÉRENTES CATÉGORIES D'ACTES. ACTES DE GOUVERNEMENT. - INCLUSION - DÉCISIONS PAR LESQUELLES LE GROUPE FRANÇAIS DE LA COUR PERMANENTE D'ARBITRAGE PROPOSE OU REFUSE DE PROPOSER UNE CANDIDATURE À L'ÉLECTION DES JUGES À LA COUR PÉNALE INTERNATIONALE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">17-02-02-02 COMPÉTENCE. ACTES ÉCHAPPANT À LA COMPÉTENCE DES DEUX ORDRES DE JURIDICTION. ACTES DE GOUVERNEMENT. ACTES CONCERNANT LES RELATIONS INTERNATIONALES. - INCLUSION - DÉCISIONS PAR LESQUELLES LE GROUPE FRANÇAIS DE LA COUR PERMANENTE D'ARBITRAGE PROPOSE OU REFUSE DE PROPOSER UNE CANDIDATURE À L'ÉLECTION DES JUGES À LA COUR PÉNALE INTERNATIONALE.
</SCT>
<ANA ID="9A"> 01-01-03 Les décisions par lesquelles le groupe français de la Cour permanente d'arbitrage propose ou refuse de proposer une candidature à l'élection des juges à la Cour pénale internationale ne sont pas détachables de la procédure d'élection de ces juges par l'Assemblée des Etats parties à la convention portant statut de cette juridiction internationale. Ils échappent, dès lors, à la compétence de la juridiction administrative française.</ANA>
<ANA ID="9B"> 17-02-02-02 Les décisions par lesquelles le groupe français de la Cour permanente d'arbitrage propose ou refuse de proposer une candidature à l'élection des juges à la Cour pénale internationale ne sont pas détachables de la procédure d'élection de ces juges par l'Assemblée des Etats parties à la convention portant statut de cette juridiction internationale. Ils échappent, dès lors, à la compétence de la juridiction administrative française.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
