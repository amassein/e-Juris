<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030223889</ID>
<ANCIEN_ID>JG_L_2015_02_000000377470</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/22/38/CETATEXT000030223889.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème / 5ème SSR, 11/02/2015, 377470</TITRE>
<DATE_DEC>2015-02-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>377470</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème / 5ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Laurent Huet</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Keller</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:377470.20150211</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 14 avril 2014 au secrétariat du contentieux du Conseil d'Etat, présentée par Mme B...A..., demeurant ... ; Mme A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 7 février 2014 par laquelle la section 74 du Conseil national des universités n'a pas retenu sa candidature à l'inscription sur la liste de qualification aux fonctions de professeur des universités ;<br/>
<br/>
              2°) d'enjoindre à la section 74 du Conseil national des universités de réexaminer sa demande de qualification ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 300 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'éducation ;<br/>
<br/>
              Vu le décret n° 84-431 du 6 juin 1984 ;<br/>
<br/>
              Vu l'arrêté du 16 juillet 2009 relatif à la procédure d'inscription sur les listes de qualification aux fonctions de maître de conférences ou de professeur des universités ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Laurent Huet, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Rémi Keller, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              Sur les conclusions à fin d'annulation :<br/>
<br/>
              1. Considérant qu'aux termes de l'article 44 du décret du 6 juin 1984 relatif au statut des enseignants chercheurs de l'enseignement supérieur : " Les candidats à une inscription sur la liste de qualification aux fonctions de professeur des universités doivent remplir l'une des conditions suivantes : / 1° Etre titulaire, au plus tard à une date limite fixée par arrêté du ministre chargé de l'enseignement supérieur, pour l'envoi du dossier aux rapporteurs prévus au deuxième alinéa du I de l'article 45, d'une habilitation à diriger des recherches (... ) " ; qu'aux termes du I de l'article 45 du même décret : " Les demandes d'inscription sur la liste de qualification aux fonctions de professeur des universités, assorties d'un dossier individuel de qualification, sont examinées par la section compétente du Conseil national des universités. (...) La qualification est appréciée par rapport aux différentes fonctions des enseignants-chercheurs mentionnées à l'article L. 952-3 du code de l'éducation et compte tenu des diverses activités des candidats (...) Les modalités d'application du présent article sont fixées par arrêté du ministre chargé de l'enseignement supérieur. " ; qu'aux termes de l'article 2 de l'arrêté du 16 juillet 2009, pris pour l'application de ces dispositions : " Les candidats à une inscription sur la liste de qualification aux fonctions de professeur des universités doivent remplir l'une des conditions suivantes : 1° Etre titulaire de l'habilitation à diriger des recherches (...). " ; qu'aux termes de l'article 4 du même arrêté : " Lorsque les deux rapporteurs lui ont été désignés par la section compétente du Conseil national des universités, le candidat établit, pour chacun des deux rapporteurs, un dossier qui comporte obligatoirement les pièces suivantes : 1° Une pièce justificative permettant d'établir : a) Soit la possession de l'un des titres mentionnés au 1° de l'article 1er ou de l'article 2 ci-dessus ; (...) / 5° Lorsqu'un diplôme est exigé, une copie du rapport de soutenance du diplôme produit, comportant notamment la liste des membres du jury et la signature du président. / Tout dossier incomplet est déclaré irrecevable par le ministre chargé de l'enseignement supérieur. " ;<br/>
<br/>
              2. Considérant que MmeA..., maître de conférences à l'Université de Lyon I (spécialité " droit du sport "), a, en application de ces dispositions, fait parvenir sa candidature à une inscription sur la liste de qualification aux fonctions de professeur des universités, par voie électronique, le 18 décembre 2013 ; que, par la décision attaquée du 7 février 2014, la section 74 du Conseil national des universités a déclaré irrecevable la candidature de l'intéressée ; qu'il ressort cependant des pièces du dossier, et n'est d'ailleurs pas contesté, que le dossier de candidature de Mme A...contenait notamment une copie du rapport établi par le jury à la suite de sa soutenance d'habilitation à diriger des recherches, daté du 10 janvier 2013 ; que ce rapport comportait la décision de ce jury, à l'unanimité de ses membres, d'habiliter Mme A...à diriger des recherches en droit ; que ce document constituait une pièce justificative, requise par le a) du 1° de l'article 4 de l'arrêté du 16 juillet 2009, établissant qu'elle était en possession du titre l'habilitant à diriger des recherches ; qu'ainsi, et sans qu'il soit besoin d'examiner les autres moyens de la requête, la candidature de l'intéressée satisfaisait aux conditions prévues par cet arrêté et ne pouvait donc se voir opposer une irrecevabilité, laquelle ne pouvait d'ailleurs, en vertu des termes mêmes de cet article, émaner que d'une décision du ministre chargé de l'enseignement supérieur ; que Mme A...est ainsi fondée à demander l'annulation de la décision attaquée ;<br/>
<br/>
              Sur les conclusions à fin d'injonction :<br/>
<br/>
              3. Considérant qu'il y a lieu, en application des dispositions de l'article L. 911-2 du code de justice administrative, d'enjoindre au Conseil national des universités d'examiner la demande d'inscription présentée par Mme A...dans un délai de trois mois à compter de la notification de la présente décision ;<br/>
<br/>
              Sur les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement à Mme A...d'une somme de 300 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : La décision du 7 février 2014 de la section 74 du Conseil national des universités est annulée.<br/>
Article 2 : Il est enjoint au Conseil national des universités d'examiner la demande d'inscription présentée par Mme A...dans un délai de trois mois à compter de la notification de la présente décision.<br/>
Article 3 : L'Etat versera à Mme A...la somme de 300 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à Mme B...A...et à la ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">30-02-05-01-06-01-02 ENSEIGNEMENT ET RECHERCHE. QUESTIONS PROPRES AUX DIFFÉRENTES CATÉGORIES D'ENSEIGNEMENT. ENSEIGNEMENT SUPÉRIEUR ET GRANDES ÉCOLES. UNIVERSITÉS. GESTION DES UNIVERSITÉS. GESTION DU PERSONNEL. RECRUTEMENT. - INSCRIPTION PAR LE CNU SUR LA LISTE DE QUALIFICATION AUX FONCTIONS DE PROFESSEUR DES UNIVERSITÉS - PROCÉDURE - EXAMEN DE LA COMPLÉTUDE DU DOSSIER PAR LE MINISTRE.
</SCT>
<ANA ID="9A"> 30-02-05-01-06-01-02 En vertu de l'article 4 de l'arrêté du 16 juillet 2009 relatif à la procédure d'inscription sur les listes de qualification aux fonctions de maître de conférences ou de professeur des universités, tout dossier de candidature à une inscription sur la liste de qualification aux fonctions de professeur des université qui est incomplet est déclaré irrecevable par le ministre chargé de l'enseignement supérieur. Le Conseil national des universités (CNU) est donc incompétent pour opposer lui-même une telle irrecevabilité au candidat.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
