<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025587317</ID>
<ANCIEN_ID>JG_L_2012_03_000000343072</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/58/73/CETATEXT000025587317.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 28/03/2012, 343072</TITRE>
<DATE_DEC>2012-03-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>343072</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Jacques Arrighi de Casanova</PRESIDENT>
<AVOCATS>SCP GATINEAU, FATTACCINI</AVOCATS>
<RAPPORTEUR>M. Pascal Trouilly</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Claire Landais</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:343072.20120328</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu, 1° sous le n° 343072, la requête, enregistrée le 7 septembre 2010 au secrétariat du contentieux du Conseil d'Etat, présentée par la FEDERATION DES SYNDICATS SOLIDAIRES, UNITAIRES ET DEMOCRATIQUES DES ACTIVITES POSTALES ET DE TELECOMMUNICATIONS, dont le siège est 25/27, rue des Envierges à Paris (75020), représentée par son secrétaire général ; la fédération requérante demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2010-778 du 8 juillet 2010 instituant une dérogation au contrôle quotidien et hebdomadaire de la durée du travail de salariés ne travaillant pas selon le même horaire collectif de travail ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement de la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu, 2° sous le n° 343166, la requête sommaire et les mémoires complémentaires, enregistrés les 9 septembre et 10 décembre 2010 et le 28 mars 2011, présentés pour la FEDERATION DES EMPLOYES ET CADRES CGT FORCE OUVRIERE, dont le siège est 28, rue des Petits Hôtels à Paris (75010) et pour le SYNDICAT NATIONAL DE PRESSE, D'EDITION ET DE PUBLICITE FORCE OUVRIERE, dont le siège est 131, rue Damrémont à Paris (75018) ; la fédération et le syndicat requérants demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir le même décret du 8 juillet 2010 ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement de la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 20 mars 2012, présentée pour le Syndicat de la distribution directe ;<br/>
<br/>
              Vu la Constitution, notamment son Préambule ; <br/>
<br/>
              Vu la directive 93/104/CE du 23 novembre 1993 ;<br/>
<br/>
              Vu la directive 2003/88/CE  du 4 novembre 2003 ; <br/>
<br/>
              Vu le code du travail ; <br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Pascal Trouilly, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Gatineau, Fattaccini, avocat du Syndicat de la distribution directe, et de Me Haas, avocat de la FEDERATION DES EMPLOYES ET CADRES CGT FORCE OUVRIERE et du SYNDICAT NATIONAL DE PRESSE, D'EDITION ET DE PUBLICITE FORCE OUVRIERE ; <br/>
<br/>
              - les conclusions de Mme Claire Landais, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Gatineau, Fattaccini, avocat du Syndicat de la distribution directe, et à Me Haas, avocat de la FEDERATION DES EMPLOYES ET CADRES CGT FORCE OUVRIERE et du SYNDICAT NATIONAL DE PRESSE, D'EDITION ET DE PUBLICITE FORCE OUVRIERE ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant que la requête n° 343072 présentée par la FEDERATION DES SYNDICATS SOLIDAIRES, UNITAIRES ET DEMOCRATIQUES DES ACTIVITES POSTALES ET DE TELECOMMUNICATIONS et la requête n° 343166 présentée par la FEDERATION DES EMPLOYES ET CADRES CGT FORCE OUVRIERE et le SYNDICAT NATIONAL DE PRESSE, D'EDITION ET DE PUBLICITE FORCE OUVRIERE tendent à l'annulation pour excès de pouvoir du même décret n° 2010-778 du 8 juillet 2010 ; qu'il y a lieu de les joindre pour statuer par une même décision ;<br/>
<br/>
              Sur les interventions du Syndicat de la distribution directe :<br/>
<br/>
              Considérant que le Syndicat de la distribution directe a intérêt au maintien du décret attaqué ; qu'ainsi, ses interventions sont recevables ; <br/>
<br/>
              Sur la légalité du décret attaqué :<br/>
<br/>
              Considérant, d'une part, qu'il résulte des dispositions combinées des articles L. 3121-1, L. 3171-3 et L. 3171-4 du code du travail que l'employeur est tenu, notamment en vue du contrôle exercé par l'inspecteur du travail et, en cas de litige, par le juge du contrat de travail, de comptabiliser la durée du travail effectif des salariés, laquelle est définie par le premier de ces articles comme le temps pendant lequel le salarié est à la disposition de l'employeur et se conforme à ses directives, sans pouvoir vaquer librement à des occupations personnelles ; qu'ainsi, les dérogations éventuelles à cette obligation de comptabiliser la durée du travail effectif ne peuvent être autorisées que par la loi ;<br/>
<br/>
              Considérant, d'autre part, qu'aux termes de l'article L. 3121-52 du code du travail : " Des décrets en Conseil d'Etat déterminent les modalités d'application des articles L. 3121-5, L. 3121-10 et L. 3121-34 pour l'ensemble des branches d'activité ou des professions ou pour une branche ou une profession particulière. / Ces décrets déterminent, notamment : 1° / les conditions de recours aux astreintes ; / 2° les dérogations permanentes ou temporaires applicables dans certains cas et pour certains emplois ; / 3° les mesures de contrôle de ces diverses dispositions. / Ces décrets sont pris et révisés après consultation des organisations d'employeurs et de salariés intéressés et au vu, le cas échéant, des résultats des négociations intervenues entre ces dernières " ; que l'article L. 3122-46 du même code dispose : " Les décrets en Conseil d'Etat prévus à l'article L. 3121-52 déterminent également les modalités d'application du présent chapitre pour l'ensemble des branches d'activité ou des professions ou pour une branche ou une profession particulière. / Ces décrets déterminent, notamment : / 1° La répartition et l'aménagement des horaires de travail ; / 2° Les périodes de repos ; / 3° Les modalités de récupération des heures de travail perdues ; / 4° Les mesures de contrôle de ces diverses dispositions. / Ces décrets sont pris et révisés après consultation des organisations d'employeurs et de salariés intéressées et au vu des résultats des négociations intervenues entre ces dernières " ; <br/>
<br/>
              Considérant que les dispositions précitées permettent au pouvoir réglementaire d'adopter des dispositions dérogeant, pour une branche ou une profession, aux modalités de contrôle de la durée effective de travail prévues, en ce qui concerne les salariés ne travaillant pas selon un horaire collectif de travail, par les dispositions de l'article D. 3171-8 du même code, selon lesquelles le décompte quotidien de la durée effective de travail s'effectue par un enregistrement, selon tous moyens, des heures de début et de fin de chaque période de travail ou par le relevé du nombre d'heures de travail accomplies ; qu'elles n'ont cependant ni pour objet ni pour effet de l'autoriser à lever l'obligation qui, ainsi qu'il a été dit ci-dessus, pèse sur les employeurs en vertu de la loi, de décompter la durée du travail effectif de leurs salariés ;  <br/>
<br/>
              Considérant que le décret attaqué insère dans le code du travail un article R. 3171-9-1 ainsi rédigé : " Les dispositions de l'article D. 3171-8 ne sont pas applicables aux salariés exerçant une activité de distribution ou de portage de documents. Le temps de travail de ces salariés fait l'objet d'une quantification préalable selon des modalités établies par convention ou accord collectif de branche étendu, en fonction du secteur géographique sur lequel s'effectue le travail, de la part relative dans ce secteur de l'habitat collectif et de l'habitat individuel, du nombre de documents à distribuer et du poids total à emporter. La convention ou l'accord collectif de branche étendu peut fixer des critères complémentaires. / L'employeur remet au salarié, avant chacune de ses missions, le document qui évalue a priori sa durée de travail à partir des critères susmentionnés. Ce document est tenu à la disposition de l'inspecteur ou du contrôleur du travail pendant une durée d'un an " ; que ces dispositions prévoient ainsi, pour la branche de la distribution ou du portage de documents et compte tenu des spécificités des conditions concrètes de travail des salariés, une quantification horaire préalable des tâches à accomplir ; qu'il institue une présomption de durée du travail effectif qui ne peut être écartée, le cas échéant, qu'en cas de saisine du conseil de prud'hommes, dans les conditions prévues à l'article L. 3171-4 du code du travail ; qu'il résulte de ce qui a été dit plus haut que la possibilité de recourir à un tel mécanisme, qui déroge à la règle de décompte de la durée du travail effectif, ne pouvait être prévue, le cas échéant, que par le législateur ; qu'ainsi, et sans qu'il soit besoin d'examiner les autres moyens des requêtes, le décret attaqué doit être annulé pour incompétence ; <br/>
<br/>
              Sur les conclusions tendant à ce que le Conseil d'Etat limite dans le temps les effets de l'annulation :<br/>
<br/>
              Considérant que, si le Syndicat de la distribution directe fait valoir que l'annulation prononcée par la présente décision aurait pour effet de fragiliser les contrats de travail et de multiplier le nombre de litiges relatifs à la rémunération des heures de travail effectuées par les salariés exerçant une activité de distribution ou de portage de documents, le mécanisme de quantification préalable du temps de travail retenu par le décret attaqué ne dispense pas l'employeur et le salarié de fournir au juge du contrat de travail, conformément à l'article L. 3171-4 du code du travail, tous les éléments de nature à justifier les horaires effectivement réalisés ; que, dans ces conditions, il n'y a pas lieu de faire droit aux conclusions analysées ci-dessus ; <br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              Considérant, qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement d'une somme de 3 000 euros, d'une part, à la FEDERATION DES SYNDICATS SOLIDAIRES, UNITAIRES ET DEMOCRATIQUES DES ACTIVITES POSTALES ET DE TELECOMMUNICATIONS, d'autre part, à la FEDERATION DES EMPLOYES ET CADRES CGT FORCE OUVRIERE et au SYNDICAT NATIONAL DE PRESSE, D'EDITION ET DE PUBLICITE FORCE OUVRIERE, au titre de l'article L. 761-1 du code de justice administrative ; qu'en revanche, les conclusions présentées au même titre par le Syndicat de la distribution directe, intervenant en défense, ne peuvent, en tout état de cause, qu'être rejetées ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les interventions du Syndicat de la distribution directe sont admises. <br/>
Article 2 : Le décret n° 2010-778 du 8 juillet 2010 est annulé.<br/>
Article 3 : L'Etat versera une somme de 3 000 euros, d'une part, à la FEDERATION DES SYNDICATS SOLIDAIRES, UNITAIRES ET DEMOCRATIQUES DES ACTIVITES POSTALES ET DE TELECOMMUNICATIONS, d'autre part, à la FEDERATION DES EMPLOYES ET CADRES CGT FORCE OUVRIERE et au SYNDICAT NATIONAL DE PRESSE, D'EDITION ET DE PUBLICITE FORCE OUVRIERE, au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions présentées par le Syndicat de la distribution directe au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à la FEDERATION DES SYNDICATS SOLIDAIRES, UNITAIRES ET DEMOCRATIQUES DES ACTIVITES POSTALES ET DE TELECOMMUNICATIONS, à la FEDERATION DES EMPLOYES ET CADRES CGT FORCE OUVRIERE, au SYNDICAT NATIONAL DE PRESSE, D'EDITION ET DE PUBLICITE FORCE OUVRIERE, au Syndicat de la distribution directe, au Premier ministre et au ministre du travail, de l'emploi et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-02-01-04 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - COMPÉTENCE. LOI ET RÈGLEMENT. HABILITATIONS LÉGISLATIVES. - ARTICLE L. 3121-52 DU CODE DU TRAVAIL - HABILITATION DU POUVOIR RÉGLEMENTAIRE À LEVER L'OBLIGATION DE COMPTABILISER LA DURÉE DE TRAVAIL EFFECTIF DES SALARIÉS - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">66-03 TRAVAIL ET EMPLOI. CONDITIONS DE TRAVAIL. - CONTRÔLE DU TEMPS DE TRAVAIL - OBLIGATION DE COMPTABILISER LA DURÉE DE TRAVAIL EFFECTIF - 1) COMPÉTENCE POUR AUTORISER DES DÉROGATIONS - COMPÉTENCE DU LÉGISLATEUR [RJ1] - 2) PORTÉE DES DISPOSITIONS DE L'ARTICLE L. 3121-52 DU CODE DU TRAVAIL - HABILITATION DU POUVOIR RÉGLEMENTAIRE À LEVER L'OBLIGATION DE COMPTABILISATION - ABSENCE.
</SCT>
<ANA ID="9A"> 01-02-01-04 Les dispositions de l'article L. 3121-52 du code du travail, si elles permettent au pouvoir réglementaire d'adopter des dispositions dérogeant aux modalités normales de contrôle de la durée effective du travail des salariés, ne l'autorisent pas à lever l'obligation de comptabilisation de cette durée posée par les dispositions combinées des articles L. 3121-1, L. 3171-3 et L. 3171-4 du même code.</ANA>
<ANA ID="9B"> 66-03 Il résulte des dispositions combinées des articles L. 3121-1, L. 3171-3 et L. 3171-4 du code du travail que l'employeur est tenu de comptabiliser la durée du travail effectif des salariés. 1) Des dérogations éventuelles à cette obligation ne peuvent, dès lors, être autorisées que par la loi. 2) Les dispositions de l'article L. 3121-52 du même code, si elles permettent au pouvoir réglementaire d'adopter des dispositions dérogeant aux modalités normales de contrôle de la durée effective du travail, ne l'autorisent pas à lever l'obligation de comptabilisation de cette durée.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 11 mars 2009, Fédération des syndicats solidaires unitaires et démocratiques - PTT, n° 303396, p. 100.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
