<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035818974</ID>
<ANCIEN_ID>JG_L_2017_10_000000402352</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/81/89/CETATEXT000035818974.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 5ème chambres réunies, 16/10/2017, 402352</TITRE>
<DATE_DEC>2017-10-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>402352</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MONOD, COLIN, STOCLET ; SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR>Mme Françoise Tomé</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:402352.20171016</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 11 août et 10 novembre 2016 et le 4 juillet 2017 au secrétariat du contentieux du Conseil d'Etat, M. B...A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 24 mai 2016 du Conseil national de l'ordre des médecins, statuant en formation restreinte, en tant qu'elle confirme la décision de suspension prononcée par l'Organe de Nouvelle-Calédonie de l'ordre des médecins le 17 février 2016 ; <br/>
<br/>
              2°) de mettre à la charge du Conseil national de l'ordre des médecins la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution ;<br/>
              - la loi organique n° 99-209 du 19 mars 1999 ;<br/>
              - le code de la santé publique ;<br/>
              - la délibération n° 431 du 9 décembre 2008 du Congrès de Nouvelle-Calédonie relative à l'exercice des professions de médecin et de chirurgien-dentiste en Nouvelle-Calédonie ;<br/>
              - l'arrêté n° 2009-2057/GNC du 21 avril 2009 du gouvernement de la Nouvelle-Calédonie pris en application de l'article 46 de la délibération n° 431 du 9 décembre 2008 relative à l'exercice des professions de médecin et de chirurgien-dentiste en Nouvelle-Calédonie ;<br/>
              - la convention du 14 octobre 2013 conclue entre le Conseil national de l'ordre des médecins et l'Organe de Nouvelle-Calédonie de l'ordre des médecins ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Françoise Tomé, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Monod, Colin, Stoclet, avocat de M. A...et à la SCP Matuchansky, Poupot, Valdelièvre, avocat du Conseil national de l'ordre des médecins ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant, d'une part, que l'article 22 de la loi organique du 19 mars 1999 relative à la Nouvelle-Calédonie dispose que la Nouvelle-Calédonie est compétente en matière de santé et de réglementation des professions libérales ; que, pris dans l'exercice de cette compétence, l'article 46 de la délibération du 9 décembre 2008 du gouvernement de Nouvelle-Calédonie relative à l'exercice des professions de médecin et de chirurgien-dentiste en Nouvelle-Calédonie, dispose que : " Dans le cas d'infirmité, d'état pathologique ou d'insuffisance professionnelle rendant dangereux l'exercice de la profession, la suspension temporaire du droit d'exercer est prononcée par l'Organe de l'ordre de la profession concernée en Nouvelle-Calédonie pour une période déterminée, qui peut, s'il y a lieu, être renouvelée. Elle ne peut être ordonnée que sur un rapport motivé établi à la demande de l'Organe de l'ordre par trois médecins spécialistes désignés comme experts. / Les modalités de procédure de l'expertise sont définies par arrêté du gouvernement de la Nouvelle-Calédonie " ; que l'arrêté du 21 avril 2009 du gouvernement de la Nouvelle-Calédonie, pris pour l'application de ces dispositions, prévoit, en son article 3, que : " Les experts procèdent ensemble, sauf impossibilité manifeste, à l'expertise. Le rapport d'expertise est déposé au plus tard dans le délai de deux mois à compter de la saisine de l'Organe de l'ordre. Il indique les carences relevées au cours de l'expertise, leur dangerosité et préconise les domaines et moyens de les pallier " ; qu'enfin, aux termes de l'article 47 de la même délibération du 9 décembre 2008 : " Le praticien qui a fait l'objet d'une mesure de suspension totale ou partielle du droit d'exercer pour insuffisance professionnelle ne peut reprendre son activité sans avoir justifié auprès de l'Organe de l'ordre qu'il a complété sa formation. / S'il apparaît que les obligations posées dans sa décision ont été entièrement satisfaites, l'Organe de l'ordre peut décider que le praticien est apte à exercer sa profession et en informe les autorités qui ont reçu notification de la suspension. / A défaut, l'Organe de l'ordre peut prononcer une nouvelle suspension temporaire " ; <br/>
<br/>
              2. Considérant, d'autre part, que l'article 45 de la délibération du 9 décembre 2008 mentionnée ci-dessus renvoie à une convention conclue entre l'Organe de l'ordre et le Conseil national de la profession correspondante le soin d'organiser " les conditions et procédures dans lesquelles se déroulera l'appel " formé contre une décision de suspension d'un praticien en cas d'infirmité, d'état pathologique ou d'insuffisance professionnelle rendant dangereux l'exercice de sa profession ; qu'en application de ces dispositions, la convention conclue le 14 octobre 2013 entre le Conseil national de l'ordre des médecins et l'Organe de Nouvelle-Calédonie de l'ordre des médecins, publiée au Journal Officiel de la Nouvelle-Calédonie du 29 juillet 2014, prévoit à son article 10 que le Conseil national de l'ordre des médecins connaît des " appels " formés contre les décisions de suspension en cas d'insuffisance professionnelle, que ces recours n'ont pas d'effet suspensif et qu'ils doivent être introduits dans un délai de dix jours à compter de la notification de la décision, auquel s'ajoute un délai de distance d'un mois ; que ces stipulations doivent être regardées comme instituant, devant le Conseil national de l'ordre des médecins, un recours administratif contre les décisions de l'Organe de Nouvelle-Calédonie de l'ordre des médecins qui est un préalable obligatoire à la saisine du juge administratif ; qu'il s'ensuit que la décision prise sur ce recours par le Conseil national de l'ordre des médecins se substitue à la décision initiale de l'Organe de Nouvelle-Calédonie de l'ordre des médecins ; <br/>
<br/>
              3. Considérant que, sur le fondement des dispositions citées au point 1, l'Organe de Nouvelle-Calédonie de l'ordre des médecins a, par une décision du 17 février 2016, suspendu pour trois années M.A..., médecin spécialiste qualifié en gynécologie-médicale, du droit d'exercer la gynécologie-obstétrique et subordonné la reprise de son activité à la justification de l'accomplissement d'une formation de remise à niveau ; que, par une décision du 24 mai 2016, le Conseil national de l'ordre des médecins, statuant en formation restreinte, a, sur le recours de M.A..., confirmé la mesure de suspension prononcée le 17 février précédent ainsi que l'obligation de formation dont elle était assortie ; que M. A...demande l'annulation pour excès de pouvoir de cette dernière décision ; <br/>
<br/>
              4. Considérant, en premier lieu, que si l'article 3 de l'arrêté du 21 avril 2009 cité au point 1 prévoit que le rapport des experts doit être déposé dans le délai de deux mois à compter de la saisine de l'instance ordinale compétente, ce délai n'est pas prescrit à peine de nullité ; que M. A...ne saurait, par suite, utilement soutenir que la seule circonstance que ce rapport a été remis au terme d'un délai plus long entache d'irrégularité la décision qu'il attaque ; <br/>
<br/>
              5. Considérant, en deuxième lieu, que M. A...ne saurait non plus utilement invoquer que l'Organe de Nouvelle-Calédonie de l'ordre des médecins, qui n'était tenu par aucun délai fixé par les textes applicables pour statuer dans le cadre de la procédure pour insuffisance professionnelle définie par les dispositions citée au point 1, ne s'est pas prononcé dans un délai de deux mois ;<br/>
<br/>
              6. Considérant, en troisième lieu, que lorsqu'il examine l'aptitude d'un praticien à exercer ses fonctions, le Conseil national, statuant en formation restreinte, prend une décision administrative individuelle et ne se prononce pas en tant qu'autorité juridictionnelle qui serait astreinte à répondre à l'ensemble des moyens de fait et de droit invoqués par le praticien ; que, par suite, M. A...n'est pas fondé à soutenir que la décision attaquée, qui contient les considérations de fait et droit qui lui servent de fondement, serait insuffisamment motivée, en ce qu'elle ne répond pas à l'ensemble de l'argumentation qu'il avait présentée dans le cadre de son recours ; <br/>
<br/>
              7. Considérant, en quatrième lieu, que si la décision attaquée mentionne que M. A... a été suspendu, en 2012, du droit de pratiquer des échographies gynécologiques alors qu'il avait, en réalité, été suspendu du droit de pratiquer des échographies obstétricales, cette mention révèle une simple erreur de plume sans incidence sur la légalité de la décision ; qu'il ressort des pièces du dossier que le Conseil national s'est fondé sur les conclusions du rapport d'expertise et sur un ensemble de faits, dont l'exactitude n'est pas contestée, qui avaient déjà conduit l'Organe de l'ordre des médecins et le président du gouvernement de Nouvelle-Calédonie à décider en 2012 d'une mesure de suspension provisoire assortie d'une obligation de formation à l'encontre de M.A... ; qu'il a également relevé des insuffisances graves dans sa pratique médicale faisant courir des risques aux patients ainsi qu'un défaut de remise à niveau de ses connaissances, la formation engagée n'ayant pas été achevée ; que, dès lors, le Conseil national a pu, sans entacher sa décision d'inexactitude matérielle ou d'erreur d'appréciation, estimer que le comportement de M. A...révélait des insuffisances professionnelles rendant dangereux l'exercice de son activité et justifiant le prononcé d'une mesure de suspension de la totalité de son activité professionnelle durant trois années ; <br/>
<br/>
              8. Considérant, en cinquième lieu, qu'il ne ressort pas des pièces du dossier que la formation restreinte du Conseil national de l'ordre des médecins ait fait une inexacte application des dispositions mentionnées au point 1 en subordonnant la reprise d'activité de M. A... à la justification de l'accomplissement d'obligations de formation ; que la circonstance que cette obligation de formation a été décidée sans que les experts se soient prononcés sur ce point n'est pas de nature à entacher d'irrégularité la décision attaquée, dès lors que M. A...y a lui-même fait obstacle, en indiquant aux experts qu'il entendait mettre fin à son activité médicale ; <br/>
<br/>
              9. Considérant, en dernier lieu, que M. A...ne saurait utilement reprocher au Conseil national statuant dans le cadre du recours mentionné au point 2, de ne pas s'être prononcé sur les conditions dans lesquelles il pourrait exercer une autre spécialité, comme celle de la gynécologie médicale ; <br/>
<br/>
              10. Considérant qu'il résulte de tout ce qui précède que M. A... n'est pas fondé à demander l'annulation de la décision qu'il attaque ; que les dispositions de l'article L. 761-1 du code de justice administrative font, par suite, obstacle à ce qu'une somme soit mise à ce titre à la charge du Conseil national de l'ordre des médecins, qui n'est pas, dans la présente instance, la partie perdante ; <br/>
<br/>
<br/>
<br/>				D E C I D E<br/>
              				--------------<br/>
<br/>
Article 1er : La requête de M. A...est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. B...A..., à l'Organe de Nouvelle-Calédonie de l'ordre des médecins et au Conseil national de l'ordre des médecins.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">17-05-02 COMPÉTENCE. COMPÉTENCE À L'INTÉRIEUR DE LA JURIDICTION ADMINISTRATIVE. COMPÉTENCE DU CONSEIL D'ETAT EN PREMIER ET DERNIER RESSORT. - LITIGE RELATIF À LA DÉCISION DE L'ORDRE NATIONAL DES MÉDECINS EN MATIÈRE DE SUSPENSION À TITRE TEMPORAIRE DE MÉDECINS EN NOUVELLE-CALÉDONIE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">46-01-07 OUTRE-MER. DROIT APPLICABLE. RÉGLEMENTATION DES ACTIVITÉS PROFESSIONNELLES. - NOUVELLE-CALÉDONIE - SUSPENSION TEMPORAIRE D'UN MÉDECIN POUR INSUFFISANCE PROFESSIONNELLE PAR L'ORGANE DE L'ORDRE DES MÉDECINS DE NOUVELLE-CALÉDONIE - 1) RECOURS DEVANT LE CONSEIL NATIONAL DE L'ORDRE DES MÉDECINS - RECOURS ADMINISTRATIF PRÉALABLE OBLIGATOIRE - EXISTENCE - CONSÉQUENCE - 2) COMPÉTENCE DU CONSEIL D'ETAT  POUR CONNAÎTRE EN PREMIER ET DERNIER RESSORT DE LA DÉCISION PRISE PAR LE CONSEIL NATIONAL DE L'ORDRE - EXISTENCE (SOL. IMPL.).
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">55-01-02-01 PROFESSIONS, CHARGES ET OFFICES. ORDRES PROFESSIONNELS - ORGANISATION ET ATTRIBUTIONS NON DISCIPLINAIRES. QUESTIONS PROPRES À CHAQUE ORDRE PROFESSIONNEL. ORDRE DES MÉDECINS. - SUSPENSION TEMPORAIRE D'UN MÉDECIN POUR INSUFFISANCE PROFESSIONNELLE PAR L'ORGANE DE L'ORDRE DES MÉDECINS DE NOUVELLE-CALÉDONIE - 1) RECOURS DEVANT LE CONSEIL NATIONAL DE L'ORDRE DES MÉDECINS - RECOURS ADMINISTRATIF PRÉALABLE OBLIGATOIRE - EXISTENCE - CONSÉQUENCE - 2) COMPÉTENCE DU CONSEIL D'ETAT  POUR CONNAÎTRE EN PREMIER ET DERNIER RESSORT DE LA DÉCISION PRISE PAR LE CONSEIL NATIONAL DE L'ORDRE DANS CE CADRE - EXISTENCE (SOL. IMPL.).
</SCT>
<ANA ID="9A"> 17-05-02 Le Conseil d'Etat est compétent pour connaître en premier et dernier ressort des décisions du conseil national de l'ordre des médecins relatives à la suspension à titre temporaire de médecins en Nouvelle-Calédonie.</ANA>
<ANA ID="9B"> 46-01-07 1) La convention conclue en application de l'article 45 de la délibération du 9 décembre 2008 du Gouvernement de Nouvelle-Calédonie relative à l'exercice des professions de médecin et de chirurgien dentiste en Nouvelle-Calédonie le 14 octobre 2013 entre le conseil national de l'ordre des médecins et l'organe de l'ordre des médecins de Nouvelle-Calédonie, publiée au Journal Officiel de la Nouvelle-Calédonie du 29 juillet 2014, prévoit à son article 10 que le conseil national de l'ordre des médecins connaît des appels formés contre les décisions de suspension en cas d'insuffisance professionnelle, que ces recours n'ont pas d'effet suspensif et qu'ils doivent être introduits dans un délai de dix jours à compter de la notification de la décision, auquel s'ajoute un délai de distance d'un mois. Ces stipulations doivent être regardées comme instituant, devant le conseil national de l'ordre des médecins, un recours administratif contre les décisions de l'organe de l'ordre des médecins de Nouvelle-Calédonie qui est un préalable obligatoire à la saisine du juge administratif. Il s'ensuit que la décision prise sur ce recours par le conseil national de l'ordre des médecins se substitue à la décision initiale de l'organe de l'ordre des médecins de Nouvelle-Calédonie.... ,,2) Le Conseil d'Etat est compétent pour connaître en premier et dernier ressort des décisions du conseil national de l'ordre des médecins relatives à la suspension à titre temporaire de médecins en Nouvelle-Calédonie.</ANA>
<ANA ID="9C"> 55-01-02-01 1) La convention conclue en application de l'article 45 de la délibération du 9 décembre 2008 du Gouvernement de Nouvelle-Calédonie relative à l'exercice des professions de médecin et de chirurgien dentiste en Nouvelle-Calédonie le 14 octobre 2013 entre le conseil national de l'ordre des médecins et l'organe de l'ordre des médecins de Nouvelle-Calédonie, publiée au Journal Officiel de la Nouvelle-Calédonie du 29 juillet 2014, prévoit à son article 10 que le conseil national de l'ordre des médecins connaît des appels formés contre les décisions de suspension en cas d'insuffisance professionnelle, que ces recours n'ont pas d'effet suspensif et qu'ils doivent être introduits dans un délai de dix jours à compter de la notification de la décision, auquel s'ajoute un délai de distance d'un mois. Ces stipulations doivent être regardées comme instituant, devant le conseil national de l'ordre des médecins, un recours administratif contre les décisions de l'organe de l'ordre des médecins de Nouvelle-Calédonie qui est un préalable obligatoire à la saisine du juge administratif. Il s'ensuit que la décision prise sur ce recours par le conseil national de l'ordre des médecins se substitue à la décision initiale de l'organe de l'ordre des médecins de Nouvelle-Calédonie.... ,,2) Le Conseil d'Etat est compétent pour connaître en premier et dernier ressort des décisions du conseil national de l'ordre des médecins relatives à la suspension à titre temporaire de médecins en Nouvelle-Calédonie.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
