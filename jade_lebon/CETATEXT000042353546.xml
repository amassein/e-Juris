<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042353546</ID>
<ANCIEN_ID>JG_L_2020_09_000000425960</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/35/35/CETATEXT000042353546.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 1ère chambres réunies, 21/09/2020, 425960</TITRE>
<DATE_DEC>2020-09-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>425960</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SARL MEIER-BOURDEAU, LECUYER ET ASSOCIES ; SCP LE BRET-DESACHE</AVOCATS>
<RAPPORTEUR>Mme Céline Roux</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:425960.20200921</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. A... B... a demandé au tribunal administratif de Versailles d'annuler pour excès de pouvoir la décision de la déléguée régionale Ile-de-France-Sud du Centre national de la recherche scientifique (CNRS) du 29 juillet 2015 rejetant sa demande de prolongation d'activité au-delà de la limite d'âge. Par un jugement n° 1506367 du 19 mai 2016, le tribunal administratif a rejeté sa demande. <br/>
<br/>
              Par un arrêt n° 16VE02177 du 4 octobre 2018, la cour administrative d'appel de Versailles a, sur appel de M. B..., annulé ce jugement et la décision de la déléguée régionale Ile-de-France-Sud du CNRS.  <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un nouveau mémoire, enregistrés les 4 décembre 2018, 26 février 2019 et 1er juillet 2020 au secrétariat du contentieux du Conseil d'Etat, le CNRS demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de M. B... ;<br/>
<br/>
              3°) de mettre à la charge de M. B... la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - la loi n° 84-16 du 11 janvier 1984 ;<br/>
              - la loi n° 84-834 du 13 septembre 1984, modifiée notamment par la loi n° 2003-775 du 21 août 2003 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Céline Roux, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SARL Meier-Bourdeau, Lecuyer et associés, avocat du Centre national de la recherche scientifique et à la SCP Le Bret-Desaché, avocat de M. B... ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par une décision du 29 juillet 2015, la déléguée régionale Ile-de-France-Sud du Centre national de la recherche scientifique (CNRS) a rejeté la demande de M. B..., chargé de recherches de 1ère classe, tendant à la prolongation de son activité au-delà de la limite d'âge en application de l'article 1-1 de la loi du 13 septembre 1984 relative à la limite d'âge dans la fonction publique et le secteur public. Par un arrêt du 4 octobre 2018, contre lequel le CNRS se pourvoit en cassation, la cour administrative d'appel de Versailles a annulé le jugement du tribunal administratif de Versailles du 19 mai 2016 rejetant le recours formé par M. B... contre cette décision, ainsi que la décision du 29 juillet 2015. <br/>
<br/>
              2. Aux termes de l'article 68 de la loi du 11 janvier 1984 portant dispositions statutaires relatives à la fonction publique de l'Etat : " Les fonctionnaires ne peuvent être maintenus en fonctions au-delà de la limite d'âge de leur emploi sous réserve des exceptions prévues par les textes en vigueur ". Aux termes de l'article 1-1 de la loi du 13 septembre 1984, issu de la loi du 21 août 2003 portant réforme des retraites : " (...) les fonctionnaires dont la durée des services liquidables est inférieure à celle définie à l'article L. 13 du code des pensions civiles et militaires de retraite peuvent, lorsqu'ils atteignent les limites d'âge applicables aux corps auxquels ils appartiennent, sur leur demande, sous réserve de l'intérêt du service et de leur aptitude physique, être maintenus en activité (...) ". Ces dernières dispositions confèrent à l'autorité compétente un large pouvoir d'appréciation de l'intérêt, pour le service, d'autoriser un fonctionnaire atteignant la limite d'âge à être maintenu en activité.<br/>
<br/>
              3. Dans le cas où un texte prévoit l'attribution d'un avantage sans avoir défini l'ensemble des conditions permettant de déterminer à qui l'attribuer parmi ceux qui sont en droit d'y prétendre, l'autorité compétente peut, alors qu'elle ne dispose pas en la matière du pouvoir réglementaire, encadrer l'action de l'administration, dans le but d'en assurer la cohérence, en déterminant, par la voie de lignes directrices, sans édicter aucune condition nouvelle, des critères permettant de mettre en oeuvre le texte en cause, sous réserve de motifs d'intérêt général conduisant à y déroger et de l'appréciation particulière de chaque situation. Dans ce cas, la personne en droit de prétendre à l'avantage en cause peut se prévaloir, devant le juge administratif, de telles lignes directrices si elles ont été publiées. <br/>
<br/>
              4. La circulaire du 28 avril 2014 sur l'application des dispositifs de poursuite d'activité au-delà de la limite d'âge des agents titulaires et non titulaires du CNRS, au regard de laquelle l'administration a examiné la demande de M. B..., prévoyait, à titre d'orientation générale, de privilégier le recrutement de jeunes chercheurs plutôt que le maintien en activité des agents ayant atteint la limite d'âge, tout en invitant à procéder à un examen particulier de chaque demande et en précisant qu'il devait être dérogé à cette orientation générale lorsque les circonstances propres au cas particulier le justifient dans l'intérêt du service. Cette circulaire s'est ainsi bornée à fixer, à l'attention des services de l'établissement, des lignes directrices pour l'appréciation des demandes de maintien en activité au regard de l'intérêt du service. Il s'ensuit que la cour administrative d'appel a retenu une inexacte interprétation de la circulaire du 28 avril 2014 en jugeant qu'elle avait pour portée de fixer des règles impératives. Par suite, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, le CNRS est fondé à demander l'annulation de l'arrêt qu'il attaque. <br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              6. En premier lieu, il résulte de ce qui a été dit précédemment que la circulaire du 28 avril 2014 fixe des lignes directrices pour l'appréciation des demandes de maintien en activité des agents du CNRS au-delà de la limite d'âge au regard de l'intérêt du service. Ces lignes directrices, eu égard à leurs termes, ne méconnaissent pas les dispositions de l'article 1-1 de la loi du 13 septembre 1984, compte tenu du large pouvoir d'appréciation que ces dispositions laissent à l'autorité administrative. Par suite, le moyen tiré, par la voie de l'exception, de l'illégalité de la circulaire du 28 avril 2014 ne peut qu'être écarté. <br/>
<br/>
              7. En deuxième lieu, pour refuser de maintenir en service M. B... au-delà de la limite d'âge, la déléguée régionale Ile-de-France-Sud du CNRS a estimé qu'aucune circonstance particulière propre à l'intéressé et à la situation du laboratoire dans lequel il exerçait ses fonctions ne justifiait qu'il soit dérogé à l'orientation générale consistant à privilégier le recrutement de jeunes chercheurs sur le maintien en activité au-delà de la limite d'âge. Il ne ressort pas des pièces du dossier que cette appréciation serait entachée d'erreur manifeste. <br/>
<br/>
              8. En dernier lieu, il y a lieu, par adoption des motifs retenus par le tribunal administratif de Versailles, dont le jugement est suffisamment motivé, contrairement à ce qui est soutenu, d'écarter les autres moyens d'appel soulevés par M. B.... <br/>
<br/>
              9. Il résulte de ce qui précède que M. B... n'est pas fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Versailles a rejeté sa demande.<br/>
<br/>
              10. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par le CNRS au titre des dispositions de l'article L. 761-1 du code de justice administrative. Ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge du CNRS qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Versailles du 4 octobre 2018 est annulé.<br/>
Article 2 : La requête présentée par M. B... devant la cour administrative d'appel de Versailles et ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : Les conclusions du Centre national de la recherche scientifique présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée au Centre national de la recherche scientifique et à M. A... B....<br/>
Copie en sera adressée à la ministre de l'enseignement supérieur, de la recherche et de l'innovation. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-01-05-03 ACTES LÉGISLATIFS ET ADMINISTRATIFS. DIFFÉRENTES CATÉGORIES D'ACTES. ACTES ADMINISTRATIFS - NOTION. INSTRUCTIONS ET CIRCULAIRES. - CARACTÈRE DE CIRCULAIRE IMPÉRATIVE OU DE LIGNES DIRECTRICES - CONTRÔLE PAR LE JUGE DE CASSATION DE L'INTERPRÉTATION DONNÉE PAR LE JUGE DU FOND.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">36-10-01 FONCTIONNAIRES ET AGENTS PUBLICS. CESSATION DE FONCTIONS. MISE À LA RETRAITE POUR ANCIENNETÉ ; LIMITES D'ÂGE. - MAINTIEN EN ACTIVITÉ AU-DELÀ DE LA LIMITE D'ÂGE (ART. 1-1 DE LA LOI DU 13 SEPTEMBRE 1984) - CONTRÔLE DU JUGE DE L'EXCÈS DE POUVOIR - CONTRÔLE RESTREINT [RJ1].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-07-02-04 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. CONTRÔLE DU JUGE DE L'EXCÈS DE POUVOIR. APPRÉCIATIONS SOUMISES À UN CONTRÔLE RESTREINT. - MAINTIEN EN ACTIVITÉ AU-DELÀ DE LA LIMITE D'ÂGE (ART. 1-1 DE LA LOI DU 13 SEPTEMBRE 1984) [RJ1].
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">54-08-02-02-01 PROCÉDURE. VOIES DE RECOURS. CASSATION. CONTRÔLE DU JUGE DE CASSATION. BIEN-FONDÉ. - CARACTÈRE DE CIRCULAIRE IMPÉRATIVE OU DE LIGNES DIRECTRICES.
</SCT>
<ANA ID="9A"> 01-01-05-03 Le juge de cassation censure l'inexacte interprétation par laquelle le juge du fond a estimé qu'une circulaire avait pour portée de fixer des règles impératives alors qu'elle se borne à édicter des lignes directrices.</ANA>
<ANA ID="9B"> 36-10-01 Le juge de l'excès de pouvoir exerce un contrôle restreint à l'erreur manifeste sur le refus de maintenir un fonctionnaire en activité au-delà de la limite d'âge en application de l'article 1-1 de la loi n° 84-834 du 13 septembre 1984, qui laisse à l'administration un large pouvoir d'appréciation.</ANA>
<ANA ID="9C"> 54-07-02-04 Le juge de l'excès de pouvoir exerce un contrôle restreint à l'erreur manifeste sur le refus de maintenir un fonctionnaire en activité au-delà de la limite d'âge en application de l'article 1-1 de la loi n° 84-834 du 13 septembre 1984, qui laisse à l'administration un large pouvoir d'appréciation.</ANA>
<ANA ID="9D"> 54-08-02-02-01 Le juge de cassation censure l'inexacte interprétation par laquelle le juge du fond a estimé qu'une circulaire avait pour portée de fixer des règles impératives alors qu'elle se borne à édicter des lignes directrices.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., s'agissant d'un professeur des universités-praticien hospitalier, CE, 4 février 2004,,, n° 242442, T. pp. 750-847.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
