<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000045122262</ID>
<ANCIEN_ID>JG_L_2022_02_000000454046</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/45/12/22/CETATEXT000045122262.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 03/02/2022, 454046</TITRE>
<DATE_DEC>2022-02-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>454046</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUIN-PALAT, BOUCARD</AVOCATS>
<RAPPORTEUR>M. François Charmont</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2022:454046.20220203</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A... D... a demandé au tribunal administratif d'Orléans d'annuler la décision référencée " 48 SI " du 9 mars 2017 par laquelle le ministre de l'intérieur a constaté la perte de validité de son permis de conduire pour solde de points nul ainsi que les décisions de retrait de points consécutives à des infractions relevées les 4 août 2015, 7 août 2015, 2 février 2016 et 21 mai 2016, et d'enjoindre sous astreinte au ministre de l'intérieur de lui restituer les douze points de son permis de conduire ou à défaut un point. Par un jugement n° 2003275 du 28 avril 2021, le tribunal administratif a annulé la décision du ministre de l'intérieur du 9 mars 2017 ainsi que les décisions de retrait de points consécutives aux infractions relevées les 4 août 2015, 2 février 2016 et 21 mai 2016, enjoint au ministre de procéder à la restitution des points correspondants au capital de points du permis de conduire de l'intéressé dans un délai de deux mois, et rejeté le surplus de conclusions de la demande.<br/>
<br/>
              Par un pourvoi, enregistré le 29 juin 2021 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'intérieur demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de M. D....<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la route ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. François Charmont, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Thouin-Palat, Boucard, avocat de M. D....<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Le ministre de l'intérieur demande l'annulation du jugement du 28 avril 2021 par lequel le tribunal administratif d'Orléans a annulé sa décision référencée " 48 SI " du 9 mars 2017 constatant la perte de validité du permis de conduire de M. D... pour solde de points nul ainsi que les décisions de retrait de points au capital de points du permis de conduire de l'intéressé consécutives aux infractions relevées les 4 août 2015, 2 février et 21 mai 2016 et lui a enjoint de procéder à la restitution des points correspondants dans un délai de deux mois.<br/>
<br/>
              2. Il résulte de la combinaison des dispositions des articles R. 421-1 et R. 421-5 du code de justice administrative que le destinataire d'une décision administrative individuelle dispose, pour déférer cette décision devant la juridiction administrative, d'un délai de deux mois à compter de sa notification qui n'est opposable qu'à la condition que les délais et les voies de recours aient été indiqués dans cette notification. Pour l'application de ces dispositions, les décisions référencées " 48 SI ", constatant la perte de validité du permis de conduire pour solde de points nul, " 48 M ", informant le conducteur que le solde de points sur son permis de conduire est inférieur ou égal à six points, " 48 N ", informant le conducteur en période probatoire que le solde de points sur son permis de conduire est inférieur ou égal à trois points et qu'il doit suivre un stage de sensibilisation à la sécurité routière dans un délai de quatre mois et, enfin, les décisions référencées " 48 ", informant le conducteur d'un retrait de points, dont l'administration n'est pas en mesure d'éditer des copies, doivent être regardées, sauf preuve contraire, comme conformes au modèle qui sert de base à leur édition automatisée par l'Imprimerie nationale, lequel comporte la mention des délais et voies de recours.<br/>
<br/>
              3. En jugeant que le ministre de l'intérieur n'apportait pas la preuve que la décision " 48 SI " notifiée le 9 mars 2017 comportait la mention des délais et voies de recours, pour en déduire que sa fin de non-recevoir tirée de la tardiveté de la demande de M. D... ne pouvait pas être accueillie, le tribunal administratif a méconnu la règle énoncée au point précédent. Le ministre de l'intérieur est, par suite, fondé à demander l'annulation du jugement qu'il attaque.<br/>
<br/>
              4. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que la somme demandée à ce titre par M. D... soit mise à la charge de l'Etat qui n'est pas la partie perdante dans la présente instance. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le jugement du tribunal administratif d'Orléans du 28 avril 2021 est annulé.<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif d'Orléans.<br/>
Article 3 : Les conclusions présentées par M. D... sur le fondement de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée au ministre de l'intérieur et M. A... D....<br/>
              Délibéré à l'issue de la séance du 13 janvier 2022 où siégeaient : M. Olivier Yeznikian, conseiller d'Etat, présidant ; M. Jean-Philippe Mochon, conseiller d'Etat et M. François Charmont, maître des requêtes-rapporteur. <br/>
<br/>
<br/>
              Rendu le 3 février 2022.<br/>
<br/>
                 Le président : <br/>
                 Signé : M. Olivier Yeznikian<br/>
 		Le rapporteur : <br/>
      Signé : M. François Charmont<br/>
                 La secrétaire :<br/>
                 Signé : Mme C... B...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">49-04-01-04-025 POLICE. - POLICE GÉNÉRALE. - CIRCULATION ET STATIONNEMENT. - PERMIS DE CONDUIRE. - DÉCISIONS 48 SI, 48 M, 48 N ET 48 - MENTION DES VOIES ET DÉLAIS DE RECOURS RENDANT OPPOSABLE LE DÉLAI DE RECOURS DE DEUX MOIS - PRÉSOMPTION.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-01-07-02-01 PROCÉDURE. - INTRODUCTION DE L'INSTANCE. - DÉLAIS. - POINT DE DÉPART DES DÉLAIS. - NOTIFICATION. - DÉCISIONS RELATIVES AU PERMIS DE CONDUIRE À POINTS 48 SI, 48 M, 48 N ET 48 - MENTION DES VOIES ET DÉLAIS DE RECOURS RENDANT OPPOSABLE LE DÉLAI DE RECOURS DE DEUX MOIS - PRÉSOMPTION.
</SCT>
<ANA ID="9A"> 49-04-01-04-025 Il résulte de la combinaison des articles R. 421-1 et R. 421-5 du code de justice administrative (CJA) que le destinataire d'une décision administrative individuelle dispose, pour déférer cette décision devant la juridiction administrative, d'un délai de deux mois à compter de sa notification qui n'est opposable qu'à la condition que les délais et les voies de recours aient été indiqués dans cette notification.......Pour l'application de ces dispositions, les décisions référencées 48 SI, constatant la perte de validité du permis de conduire pour solde de points nul, 48 M, informant le conducteur que le solde de points sur son permis de conduire est inférieur ou égal à six points, 48 N, informant le conducteur en période probatoire que le solde de points sur son permis de conduire est inférieur ou égal à trois points et qu'il doit suivre un stage de sensibilisation à la sécurité routière dans un délai de quatre mois et, enfin, les décisions référencées 48, informant le conducteur d'un retrait de points, dont l'administration n'est pas en mesure d'éditer des copies, doivent être regardées, sauf preuve contraire, comme conformes au modèle qui sert de base à leur édition automatisée par l'Imprimerie nationale, lequel comporte la mention des délais et voies de recours.</ANA>
<ANA ID="9B"> 54-01-07-02-01 Il résulte de la combinaison des articles R. 421-1 et R. 421-5 du code de justice administrative (CJA) que le destinataire d'une décision administrative individuelle dispose, pour déférer cette décision devant la juridiction administrative, d'un délai de deux mois à compter de sa notification qui n'est opposable qu'à la condition que les délais et les voies de recours aient été indiqués dans cette notification.......Pour l'application de ces dispositions, les décisions référencées 48 SI, constatant la perte de validité du permis de conduire pour solde de points nul, 48 M, informant le conducteur que le solde de points sur son permis de conduire est inférieur ou égal à six points, 48 N, informant le conducteur en période probatoire que le solde de points sur son permis de conduire est inférieur ou égal à trois points et qu'il doit suivre un stage de sensibilisation à la sécurité routière dans un délai de quatre mois et, enfin, les décisions référencées 48, informant le conducteur d'un retrait de points, dont l'administration n'est pas en mesure d'éditer des copies, doivent être regardées, sauf preuve contraire, comme conformes au modèle qui sert de base à leur édition automatisée par l'Imprimerie nationale, lequel comporte la mention des délais et voies de recours.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
