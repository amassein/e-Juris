<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034651734</ID>
<ANCIEN_ID>JG_L_2017_05_000000396279</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/65/17/CETATEXT000034651734.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 4ème chambres réunies, 10/05/2017, 396279</TITRE>
<DATE_DEC>2017-05-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>396279</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>M. Lionel Collet</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Laurence Marion</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:396279.20170510</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A... B...a demandé au tribunal administratif de Basse-Terre d'annuler la décision du ministre de l'intérieur du 17 janvier 2013 le révoquant de ses fonctions de gardien de la paix. Par une ordonnance n° 1300839 du 26 juin 2013, la présidente du tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 13BX02469 du 20 octobre 2015, la cour administrative d'appel de Bordeaux  a rejeté l'appel formé  par M.B...  contre cette ordonnance.  <br/>
<br/>
              Par un pourvoi et un mémoire complémentaire, enregistrés les 20 janvier et 19 avril 2016 au secrétariat du contentieux du Conseil d'Etat, M. B...  demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat  la somme de 3 000  euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Lionel Collet, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Laurence Marion, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de M.B....<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que, par l'arrêt attaqué du 20 octobre 2015, la cour administrative d'appel de Bordeaux a, d'une part, confirmé l'ordonnance de la présidente du tribunal administratif de Basse-Terre du 26 juin 2013 rejetant pour tardiveté le recours pour excès de pouvoir formé par M. B..., gardien de la paix de la police nationale, contre l'arrêté du 17 janvier 2013 du ministre de l'intérieur lui infligeant la sanction de la révocation et, d'autre part, rejeté comme non fondées les conclusions de l'intéressé tendant à être indemnisé du préjudice que cette révocation lui a causé ; <br/>
<br/>
              Sur le rejet des conclusions d'excès de pouvoir :<br/>
<br/>
              2. Considérant qu'aux termes de l'article R. 421-1 du code de justice administrative, " Sauf en matière de travaux publics, la juridiction ne peut être saisie que par voie de recours formé contre une décision, et ce, dans les deux mois à partir de la notification ou de la publication de la décision attaquée " ; qu'aux termes de l'article R. 421-5 du même code : " Les délais de recours contre une décision administrative ne sont opposables qu'à la condition d'avoir été mentionnés, ainsi que les voies de recours, dans la notification de la décision " ; que, lorsque l'administration prend toute disposition pour notifier une décision à un agent public par une remise en mains propres par la voie hiérarchique et que l'intéressé refuse de recevoir la décision, la notification doit être regardée comme ayant été régulièrement effectuée, sans qu'il soit nécessaire de vérifier si le document qui devait être remis en mains propres comportait la mention des voies et délais de recours ;<br/>
<br/>
              3. Considérant que la cour a relevé par une appréciation souveraine exempte de dénaturation que, convoqué le 25 février 2013 par le directeur départemental de la sécurité publique de Guadeloupe en vue de se voir notifier en mains propres l'arrêté ministériel du 17 janvier 2013 le révoquant de ses fonctions, M. B... avait non seulement refusé de signer le procès-verbal de notification mais aussi refusé de recevoir l'arrêté ;  qu'il résulte de ce qui a été dit au point 2 que la cour n'a pas commis d'erreur de droit en retenant que le délai de recours contentieux avait couru à compter de la date de cette tentative de remise en mains propres de la décision ; qu'elle n'a pas davantage commis d'erreur de droit en estimant que la notification par voie postale ultérieure n'était pas, en principe, de nature à faire courir un nouveau délai de recours ; qu'il ressortait toutefois des pièces du dossier qui lui était soumis que la notification par voie postale, reçue moins de deux mois après la tentative de notification en mains propres, et donc avant que l'arrêté contesté soit devenu définitif, indiquait que cet arrêté pouvait faire l'objet d'un recours devant le tribunal administratif dans un délai de deux mois ; qu'en s'abstenant de rechercher si cette mention avait pu induire en erreur M. B...sur le terme du délai, alors que celui-ci n'était pas encore expiré, la cour a commis une erreur de droit qui doit entraîner l'annulation de son arrêt en tant qu'il confirme le rejet pour tardiveté des conclusions de l'intéressé tendant à l'annulation de l'arrêté, sans qu'il soit besoin d'examiner les autres moyens du pourvoi dirigés contre cette partie de l'arrêt ;<br/>
<br/>
              Sur le rejet des conclusions indemnitaires :<br/>
<br/>
              4. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la demande présentée par M. B...devant le tribunal administratif de Basse-Terre tendait exclusivement à l'annulation pour excès de pouvoir de cet arrêté ; que ses conclusions indemnitaires, présentées pour la première fois en appel, étaient par suite irrecevables ; qu'il y a lieu de substituer ce motif, invoqué en défense par le ministre devant la cour, à ceux retenus par la cour pour les rejeter ; que les moyens du pourvois dirigés contre les motifs retenus par la cour sont, par suite, inopérants ; <br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat, le versement d'une somme de 2 000 euros à M. B... au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Bordeaux du 20 octobre 2015 est annulé en tant qu'il statue sur les conclusions de M. B... tendant à l'annulation pour excès de pouvoir de l'arrêté du 17 janvier 2013.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Bordeaux dans la limite de la cassation ainsi prononcée.<br/>
Article 3 : L'Etat versera à M. B...une somme de 2 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
		Article 4 : Le surplus des conclusions du pourvoi de M. B...est rejeté. <br/>
Article 5 : La présente décision sera notifiée à M. A...B...et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-01-07-02-01 PROCÉDURE. INTRODUCTION DE L'INSTANCE. DÉLAIS. POINT DE DÉPART DES DÉLAIS. NOTIFICATION. - OPPOSABILITÉ DES DÉLAIS DE RECOURS - CONDITION - MENTION DES VOIES ET DÉLAIS DE RECOURS (ART. R. 421-5 DU CJA) - 1) NOTIFICATION DE LA DÉCISION PAR REMISE EN MAINS PROPRES À L'INTÉRESSÉ PAR VOIE HIÉRARCHIQUE - REFUS DE LA NOTIFICATION PAR L'AGENT - CONSÉQUENCE - NOTIFICATION RÉPUTÉE RÉGULIÈRE, SANS QU'IL SOIT NÉCESSAIRE DE VÉRIFIER LA MENTION DES VOIES ET DÉLAIS DE RECOURS - 2) CAS OÙ LA NOTIFICATION PAR VOIE HIÉRARCHIQUE EST SUIVIE, AVANT L'EXPIRATION DU DÉLAI DE RECOURS, PAR UNE NOTIFICATION PAR VOIE POSTALE INDIQUANT LES VOIES ET DÉLAIS DE RECOURS - CIRCONSTANCE DE NATURE À INDUIRE EN ERREUR L'INTÉRESSÉ SUR LE TERME DU DÉLAI - EXISTENCE.
</SCT>
<ANA ID="9A"> 54-01-07-02-01 1) Lorsque l'administration prend toute disposition pour notifier une décision à un agent public par une remise en mains propres par la voie hiérarchique et que l'intéressé refuse de recevoir la décision, la notification doit être regardée comme ayant été régulièrement effectuée, sans qu'il soit nécessaire de vérifier si le document qui devait être remis en mains propres comportait la mention des voies et délais de recours.... ,,2) Agent ayant refusé de recevoir l'arrêté le révoquant de ses fonctions et de signer le procès-verbal de notification, à qui l'arrêté a été ultérieurement notifié par voie postale, moins de deux mois après la tentative de notification en mains propres, et donc avant que l'arrêté soit devenu définitif, avec l'indication que cet arrêté pouvait faire l'objet d'un recours devant le tribunal administratif dans un délai de deux mois. Commet une erreur de droit la cour qui rejette pour tardiveté les conclusions du requérant contre l'arrêté en s'abstenant de rechercher si la mention accompagnant la notification par voie postale avait pu l'induire en erreur sur le terme du délai, alors que celui-ci n'était pas encore expiré.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
