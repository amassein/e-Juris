<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036743988</ID>
<ANCIEN_ID>JG_L_2018_03_000000405330</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/74/39/CETATEXT000036743988.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 4ème chambres réunies, 26/03/2018, 405330</TITRE>
<DATE_DEC>2018-03-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>405330</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DIDIER, PINET ; SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS</AVOCATS>
<RAPPORTEUR>Mme Sandrine Vérité</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:405330.20180326</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A... C...a demandé au tribunal administratif de Marseille d'annuler pour excès de pouvoir l'arrêté du 27 mai 2013 par lequel le maire de Cornillon-Confoux (Bouches-du-Rhône) a refusé de lui délivrer un permis de construire. Par un jugement n° 1304176 du 3 juillet 2014, le tribunal administratif de Marseille a rejeté cette demande.<br/>
<br/>
              Par un arrêt n° 14MA03810 du 22 septembre 2016, la cour administrative d'appel de Marseille a rejeté l'appel formé par M. C...contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés le 23 novembre 2016 et les 16 février et 14 novembre 2017 au secrétariat du contentieux du Conseil d'Etat, M. C...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de la commune de Cornillon-Confoux la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la construction et de l'habitation ;<br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sandrine Vérité, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Didier, Pinet, avocat de M. A...C...et à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de la commune de Cornillon-Confoux.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 23 janvier 2007, le maire de Cornillon-Confoux a délivré à M. B...C..., père du requérant, le permis de construire une remise agricole. Une partie du bâtiment ainsi autorisé a toutefois été transformée en un logement occupé par le requérant. Par un arrêté du 27 mai 2013, le maire de Cornillon-Confoux a refusé de délivrer à ce dernier un permis de construire en vue de la régularisation de ces travaux. Par un jugement du 3 juillet 2014, le tribunal administratif de Marseille a rejeté la demande de M. A...C...tendant à l'annulation de cette décision de refus. M. C...se pourvoit en cassation contre l'arrêt du 22 septembre 2016 par lequel la cour administrative d'appel de Marseille a rejeté l'appel qu'il avait formé contre ce jugement.<br/>
<br/>
              2. En premier lieu, aux termes de l'article R. 423-23 du code de l'urbanisme : " Le délai d'instruction de droit commun est de : (...) b) Deux mois (...) pour les demandes de permis de construire portant sur une maison individuelle, au sens du titre III du livre II du code de la construction et de l'habitation, ou ses annexes. / c) Trois mois pour les autres demandes de permis de construire (...) ". Eu égard à l'objet de ces dispositions, relèvent seules du b de cet article R. 423-23 les demandes portant sur un immeuble dont les surfaces sont exclusivement ou principalement affectées à un usage d'habitation et qui, selon les termes de l'article L. 231-1 du code de la construction et de l'habitation, ne comporte " pas plus de deux logements destinés au même maître de l'ouvrage ".<br/>
<br/>
              3. La cour a relevé, par une appréciation souveraine, que la demande de permis de construire concernait un hangar à usage agricole de 534,05 mètres carrés, dont 138,46 mètres carrés affectés à l'habitation et 395,59 mètres carrés affectés à l'activité agricole. Par suite, en jugeant que ce projet, qui n'était pas principalement affecté à l'habitation, ne pouvait, pour l'application des dispositions de l'article R. 423-23 du code de l'urbanisme, être regardé comme une maison individuelle et en en déduisant, pour écarter le moyen tiré de ce que l'arrêté attaqué aurait irrégulièrement retiré un permis de construire né tacitement, que le délai d'instruction de cette demande n'était pas celui de deux mois applicable aux projets de maison individuelle mais celui de trois mois applicable dans les autres cas, la cour administrative d'appel n'a pas commis d'erreur de droit.<br/>
<br/>
              4. En second lieu, aux termes de l'article NC 2 du règlement du plan d'occupation des sols de la commune de Cornillon-Corfou, applicable au projet, ne sont autorisés que : " (...) les logements strictement liés à l'exploitation agricole (...) ". <br/>
<br/>
              5. La cour a relevé, par une appréciation souveraine exempte de dénaturation, que M.C..., qui se prévalait de la taille de son exploitation, de ses horaires de travail journaliers, de l'absence de salarié, de la nécessité d'un suivi quotidien des animaux et des cultures, de celle de réagir en cas d'aléa climatique et de prévenir des vols et dégradations sur le site, ainsi que d'être présent à proximité de son enfant en bas âge, n'établissait pas que son exploitation maraîchère ou l'élevage de poules pondeuses entrepris par sa conjointe rendraient nécessaire leur présence continue. En jugeant par suite que la construction projetée devait être regardée comme  strictement liée à l'exploitation agricole pour l'application des dispositions précitées de l'article NC2 du règlement du plan d'occupation des sols de la commune de Cornillon-Corfou, la cour n'a pas commis d'erreur de droit. <br/>
<br/>
              6. Il en résulte que M. C...n'est pas fondé à demander l'annulation de l'arrêt qu'il attaque.<br/>
<br/>
              7. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la commune de Cornillon-Confoux, qui n'est pas, dans la présente instance, la partie perdante. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de M. C...le versement à la commune de Cornillon-Confoux d'une somme au titre de ces dispositions. <br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : Le pourvoi de M. C...est rejeté. <br/>
Article 2 : Les conclusions de la commune de Cornillon-Confoux présentées au titre de l'article L.761-1 du code de justice administrative sont rejetées. <br/>
Article 3 : La présente décision sera notifiée à M. A...C...et à la commune de Cornillon-Confoux.<br/>
Copie en sera transmise au ministre de la cohésion des territoires.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">68-03-02-02-01 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PERMIS DE CONSTRUIRE. PROCÉDURE D'ATTRIBUTION. INSTRUCTION DE LA DEMANDE. DÉLAI D'INSTRUCTION. - DÉTERMINATION DU DÉLAI D'INSTRUCTION DES DEMANDES DE PERMIS DE CONSTRUIRE PORTANT SUR UNE MAISON INDIVIDUELLE (B DE L'ART. R. 423-23 DU CODE DE L'URBANISME) - 1) NOTION DE MAISON INDIVIDUELLE - 2) ESPÈCE.
</SCT>
<ANA ID="9A"> 68-03-02-02-01 1) Eu égard à l'objet de ces dispositions, relèvent seules du b de l'article R. 423-23 les demandes portant sur un immeuble dont les surfaces sont exclusivement ou principalement affectées à un usage d'habitation et qui, selon les termes de l'article L. 231-1 du code de la construction et de l'habitation (CCH), ne comporte pas plus de deux logements destinés au même maître de l'ouvrage.,,,2) Demande de permis de construire concernant un hangar à usage agricole de 534,05 mètres carrés, dont 138,46 mètres carrés affectés à l'habitation et 395,59 mètres carrés affectés à l'activité agricole. Ce projet, qui n'est pas principalement affecté à l'habitation, ne peut, pour l'application des dispositions de l'article R. 423-23 du code de l'urbanisme, être regardé comme une maison individuelle. Le délai d'instruction de cette demande n'est donc pas celui de deux mois applicable aux projets de maison individuelle mais celui de trois mois applicable dans les autres cas.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
