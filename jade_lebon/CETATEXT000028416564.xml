<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028416564</ID>
<ANCIEN_ID>JG_L_2013_12_000000368065</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/41/65/CETATEXT000028416564.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème et 9ème sous-sections réunies, 30/12/2013, 368065</TITRE>
<DATE_DEC>2013-12-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>368065</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème et 9ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Florian Blazy</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:368065.20131230</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 24 avril 2013 au secrétariat du contentieux du Conseil d'Etat, présentée pour la SA Brasserie de Tahiti, dont le siège est 17 place Notre Dame BP 597 à Papeete (98713), représentée par son président directeur général en exercice ; la société demande au Conseil d'Etat :<br/>
<br/>
              1°) de déclarer la " loi du pays " n° 2013-3 LP/APF adoptée le 14 mars 2013 portant modification de la délibération n° 59-53 du 4 septembre 1959 réglementant le commerce des boissons non conforme au bloc de légalité défini au III de l'article 176 de la loi organique du 27 février 2004 portant statut d'autonomie de la Polynésie française ;<br/>
<br/>
              2°) de mettre à la charge de la Polynésie française le versement d'une somme de 4 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution, notamment son Préambule et son article 74 ;<br/>
<br/>
              Vu la loi organique n° 2004 192 du 27 février 2004 ;<br/>
<br/>
              Vu la délibération n° 59-53 du 4 septembre 1959 réglementant le commerce des boissons ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Florian Blazy, Maître des Requêtes,<br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes du II de l'article 176 de la loi organique du 27 février 2004 portant statut d'autonomie de la Polynésie française : " A l'expiration de la période de huit jours suivant l'adoption d'un acte prévu à l'article 140 dénommé "loi du pays" ou au lendemain du vote intervenu à l'issue de la nouvelle lecture prévue à l'article 143, l'acte prévu à l'article 140 dénommé "loi du pays" est publié au Journal officiel de la Polynésie française à titre d'information pour permettre aux personnes physiques ou morales, dans le délai d'un mois à compter de cette publication, de déférer cet acte au Conseil d'Etat. Le recours des personnes physiques ou morales est recevable si elles justifient d'un intérêt à agir (...) " ; qu'aux termes de l'article 177 de cette même loi : " (...) Si le Conseil d'Etat constate qu'un acte prévu à l'article 140 dénommé "loi du pays" contient une disposition contraire à la Constitution, aux lois organiques, ou aux engagements internationaux ou aux principes généraux du droit, et inséparable de l'ensemble de l'acte, celle-ci ne peut être promulguée. / Si le Conseil d'Etat décide qu'un acte prévu à l'article 140 dénommé "loi du pays" contient une disposition contraire à la Constitution, aux lois organiques ou aux engagements internationaux, ou aux principes généraux du droit, sans constater en même temps que cette disposition est inséparable de l'acte, seule cette dernière disposition ne peut être promulguée (...) " ;<br/>
<br/>
              2. Considérant que, sur le fondement de l'article 140 de la loi organique du 27 février 2004, l'assemblée de la Polynésie française a adopté, le 14 mars 2013, une " loi du pays " portant modification de la délibération n° 59-53 du 4 septembre 1959 réglementant le commerce des boissons ; que cette " loi du pays " a été publiée au Journal officiel de la Polynésie française à titre d'information le 25 mars 2013 ; qu'au titre du contrôle juridictionnel spécifique défini au chapitre II du titre VI de cette loi organique, la SA Brasserie de Tahiti a saisi le Conseil d'Etat d'une requête tendant à ce que cette " loi du pays " soit déclarée illégale ;<br/>
<br/>
              Sur la légalité externe de la " loi du pays " attaquée :<br/>
<br/>
              3. Considérant, en premier lieu, qu'aux termes de l'article 130 de la même loi organique : " Tout représentant à l'assemblée de la Polynésie française a le droit, dans le cadre de sa fonction, d'être informé des affaires qui font l'objet d'un projet ou d'une proposition d'acte prévu à l'article 140 dénommé "loi du pays" ou d'autres délibérations. / A cette fin, les représentants reçoivent, douze jours au moins avant la séance pour un projet ou une proposition d'acte prévu à l'article 140 dénommé "loi du pays" (...) un rapport sur chacune des affaires inscrites à l'ordre du jour " ; qu'aux termes du deuxième alinéa de l'article 142 de cette loi : " Aucun projet ou proposition d'acte prévu à l'article 140 dénommé "loi du pays" ne peut être mis en discussion et aux voix s'il n'a fait au préalable l'objet d'un rapport écrit, conformément à l'article 130, déposé, imprimé et publié dans les conditions fixées par le règlement intérieur. " ; que l'article 32 de ce règlement intérieur prévoit que : " Les rapports, dès qu'ils sont déposés et imprimés, sont mis en distribution. Chaque rapport fait l'objet d'une présentation pouvant se limiter à un complément d'information ou à un commentaire, sans qu'il en soit donné lecture (...). Chaque rapport fait l'objet d'une discussion générale (...) " ;<br/>
<br/>
              4. Considérant qu'il ressort des pièces du dossier que le rapport sur le projet de " loi du pays ", examiné lors de la séance du 14 mars 2013, a été déposé le 21 février 2013 et distribué le jour suivant aux représentants ; qu'ainsi le délai de douze jours, qui n'est pas un délai franc, a été respecté ; qu'en outre, aucune disposition n'impose que l'ordre du jour d'une séance de l'assemblée soit notifié aux représentants douze jours au moins avant ladite séance ; que, dès lors, le moyen tiré de ce que le droit à l'information des représentants à l'assemblée de Polynésie aurait été méconnu doit être écarté ;<br/>
<br/>
              5. Considérant, en second lieu, qu'aux termes du premier alinéa de l'article 130 de la loi organique du 27 février 2004 : " Deux séances par mois au moins sont réservées par priorité aux questions des représentants et aux réponses du président et des membres du gouvernement. " ; qu'eu égard à la nature des rapports entre le gouvernement et l'assemblée de la Polynésie française, la circonstance qu'une " loi du pays " aurait été adoptée au cours d'un mois pendant lequel aucune séance réservée par priorité aux questions des représentants et aux réponses du président et des membres du gouvernement n'aurait été tenue est également sans effet sur la régularité de la procédure ayant abouti à l'adoption de cette " loi du pays " ;<br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que la SA Brasserie de Tahiti n'est pas fondée à soutenir que la " loi du pays " qu'elle attaque aurait été adoptée au terme d'une procédure irrégulière ;<br/>
<br/>
              Sur la légalité interne de la " loi du pays " attaquée :<br/>
<br/>
              7. Considérant que l'article LP.3 de la " loi du pays " dispose : " L'article 7 est ainsi rédigé : / Article LP.7- Il est interdit de vendre à emporter des boissons alcooliques réfrigérées ou des boissons d'alimentation réfrigérées aux heures fixées par arrêté pris en conseil des ministres. Toute infraction à la disposition qui précède est punie de 890 000 F CFP d'amende. La récidive est punie d'un an d'emprisonnement et de 1 780 000 F CFP d'amende. " ; que les catégories " boissons alcooliques ", " boissons d'alimentation " et " boissons réfrigérées " sont définies dans la délibération du 4 septembre 1959 réglementant le commerce des boissons, modifiée en dernier lieu par la " loi du pays " attaquée ;<br/>
<br/>
              En ce qui concerne l'exception d'illégalité de la délibération du 4 septembre 1959 :<br/>
<br/>
              8. Considérant que l'illégalité d'un acte administratif, qu'il soit ou non réglementaire, ne peut être utilement invoquée à l'appui de conclusions dirigées contre un autre acte administratif que si ce dernier a été pris pour son application ou s'il en constitue la base légale ; que la " loi du pays " en cause, qui modifie la délibération du 4 septembre 1959 réglementant le commerce des boissons, n'a pas été prise pour son application ou sur son fondement ; que, par suite, l'exception d'illégalité invoquée est inopérante ;<br/>
<br/>
              En ce qui concerne les moyens tirés de la violation de la liberté d'entreprendre et de l'incompétence négative :<br/>
<br/>
              9. Considérant qu'il est loisible à l'assemblée de la Polynésie française de définir et de mettre en oeuvre une réglementation, à caractère général ou sectoriel, destinée à satisfaire l'intérêt général qui s'attache à la protection de la santé ; que, toutefois, le respect de la liberté d'entreprendre implique, notamment, que les personnes publiques n'apportent pas aux activités de production, de distribution ou de services exercées par des tiers des restrictions qui ne seraient pas justifiées par l'intérêt général et proportionnées à l'objectif poursuivi ;<br/>
<br/>
              10. Considérant que l'objectif visé par la " loi du pays " est d'assurer la protection de la santé publique en limitant, par des interdictions temporaires de vente à emporter de boissons alcoolisées et réfrigérées, la consommation de ces boissons et, par suite, en réduisant le nombre d'accidents causés par cette consommation ; que les interdictions qu'elles prévoient et qui prennent en compte les modes de consommation en Polynésie française sont justifiées par l'intérêt général ; qu'elles seront limitées à certaines tranches horaires ainsi qu'à certaines catégories de boissons, qui seront déterminées par le conseil des ministres, compétent, en vertu de l'article 89 de la loi organique, pour prendre les règlements nécessaires à la mise en oeuvre des " lois du pays " ; qu'elles pourront varier en fonction des nécessités particulières des communes ou îles composant la Polynésie française ; qu'il appartiendra au conseil des ministres, dans tous les cas, de déterminer les modalités d'application des dispositions contestées de manière à éviter toute atteinte illégale à la liberté d'entreprendre ; que, dans ces conditions, eu égard à l'objectif poursuivi, la " loi du pays " attaquée, qui n'est pas entachée d'incompétence négative, ne porte, par elle-même, aucune atteinte disproportionnée à la liberté d'entreprendre ;<br/>
<br/>
              11. Considérant qu'il résulte de tout ce qui précède que la SA Brasserie de Tahiti n'est pas fondée à demander l'annulation de la " loi du pays " qu'elle attaque ;<br/>
<br/>
              Sur les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              12. Considérant que ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de la Polynésie française qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : La requête de la SA Brasserie de Tahiti est rejetée. <br/>
Article 2 : La présente décision sera notifiée à la SA Brasserie de Tahiti, au président de la Polynésie française, au président de l'assemblée de la Polynésie française, au ministre des outre-mer et au haut-commissaire de la République en Polynésie française.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">46-01-02-02 OUTRE-MER. DROIT APPLICABLE. STATUTS. POLYNÉSIE FRANÇAISE. - ASSEMBLÉE DE LA POLYNÉSIE FRANÇAISE - RÈGLE RÉSERVANT DEUX SÉANCES PAR MOIS PAR PRIORITÉ AUX QUESTIONS DES REPRÉSENTANTS AU GOUVERNEMENT - MÉCONNAISSANCE - INCIDENCE SUR LA RÉGULARITÉ DE LA PROCÉDURE D'ADOPTION D'UNE LOI DU PAYS - ABSENCE [RJ1].
</SCT>
<ANA ID="9A"> 46-01-02-02 Eu égard à la nature des rapports entre le gouvernement et l'assemblée de la Polynésie française, la circonstance qu'une loi du pays aurait été adoptée au cours d'un mois pendant lequel aucune séance réservée par priorité aux questions des représentants et aux réponses du président et des membres du gouvernement n'aurait été tenue, en méconnaissance du premier alinéa de l'article 130 de la loi organique n° 2004-192 du 27 février 2004, est sans effet sur la régularité de la procédure ayant abouti à l'adoption de cette loi du pays.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Comp., pour l'application de l'article 48 de la Constitution, Cons. const., 9 août 2012, n° 2012-654 DC.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
