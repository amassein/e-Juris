<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028717868</ID>
<ANCIEN_ID>JG_L_2014_03_000000364092</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/71/78/CETATEXT000028717868.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème / 1ère SSR, 12/03/2014, 364092</TITRE>
<DATE_DEC>2014-03-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>364092</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème / 1ère SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ ; SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>Mme Sophie Roussel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier de Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:364092.20140312</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu 1°, sous le n° 364092, le pourvoi, enregistré le 26 novembre 2012 au secrétariat du contentieux du Conseil d'Etat, du ministre de l'intérieur ; le ministre demande au Conseil d'Etat d'annuler l'arrêt n° 11LY01226, 11LY01240, 11LY01229 du 27 septembre 2012 par lequel la cour administrative d'appel de Lyon a annulé, sur la requête des communes de Montcel et Combronde, de l'association " Oxygène pour l'Avenir ", de l'association de protection des propriétaires et fermiers exploitants et autres titulaires de droits des communes de Combronde et Montcel et de la SARL Rozana, d'une part, le jugement nos 1000737-1000739-1000771 du 15 mars 2011 du tribunal administratif de Clermont Ferrand, d'autre part, l'arrêté du 17 février 2010 par lequel le préfet du Puy-de-Dôme a déclaré d'utilité publique le projet du syndicat pour la valorisation et le traitement des déchets ménagers et assimilés du Puy-de-Dôme (VALTOM) d'acquérir les immeubles nécessaires à la création d'une installation de stockage de déchets non dangereux sur le territoire de ces communes ; <br/>
<br/>
<br/>
              Vu 2°, sous le n° 364125, le pourvoi sommaire et le mémoire complémentaire, enregistrés les 27 novembre 2012 et 26 février 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour le syndicat de valorisation et de traitement des déchets ménagers et assimilés (VALTOM), dont le siège est 43, avenue de la Margeride à Clermont-Ferrand (63000) ; le VALTOM demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler le même arrêt de la cour administrative d'appel de Lyon ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel des communes de Combronde et Montcel, de l'association " Oxygène pour l'avenir ", de l'association de protection des propriétaires et fermiers exploitants et autres titulaires de droits des communes de Combronde et Montcel et de la SARL Rozana ;<br/>
<br/>
              3°) de mettre à la charge des communes de Combronde et Montcel, de l'association " Oxygène pour l'avenir ", de l'association de protection des propriétaires et fermiers exploitants et autres titulaires de droits des communes de Combronde et Montcel et de la SARL Rozana une somme de 5 000 euros chacune au titre des dispositions de l'article           L. 761-1 du code de justice administrative, ainsi que le montant de la contribution à l'aide juridique prévue à l'article R. 761-1 du même code ; <br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu le code de l'expropriation pour cause d'utilité publique ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie Roussel, auditeur,  <br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat des communes de Montcel et Combronde, de l'association " Oxygène pour l'Avenir " et de l'association de protection des propriétaires et fermiers exploitants et autres titulaires de droit des communes de Combronde et Montcel, à la SCP Lyon-Caen, Thiriez, avocat du syndicat de valorisation et de traitement des déchets ménagers et assimilés (VALTOM) et à Me Bouthors, avocat de la SARL Rozana ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que le préfet du Puy-de-Dôme a décidé de soumettre à une enquête publique, close le 12 octobre 2007, un projet du syndicat pour la valorisation et le traitement des ordures ménagères du Puy-de-Dôme (VALTOM) relatif à l'implantation sur le territoire des communes de Combronde et Montcel d'un centre d'enfouissement technique de stockage de déchets non dangereux ; que, par un arrêté du 24 juin 2008, le préfet a toutefois refusé de déclarer d'utilité publique le projet qui lui était soumis par le maître d'ouvrage, pour des motifs tirés des insuffisances du contenu du dossier et de l'information du public ; que, par un jugement du 4 novembre 2009, passé en force de chose jugée, estimant ces motifs infondés, le tribunal administratif de Clermont-Ferrand a annulé ce refus ; qu'à la demande du président du comité syndical du VALTOM, le préfet a, par un arrêté du 17 février 2010, déclaré d'utilité publique le projet et autorisé le syndicat à poursuivre par voie d'expropriation les acquisitions foncières nécessaires à la réalisation des travaux ; que, par un jugement du 15 mars 2011, le tribunal administratif de Clermont-Ferrand a rejeté la demande présentée par les communes de Combronde et Montcel, l'association " Oxygène pour l'avenir ", l'association de protection des propriétaires et fermiers exploitants et autres titulaires de droits des communes de Combronde et Montcel et la SARL Rozana tendant à l'annulation de l'arrêté du 17 février 2010 ; que par un arrêt du 27 septembre 2012, contre lequel le ministre de l'intérieur, sous le n° 364092, et le VALTOM, sous le n° 364125, se pourvoient en cassation, la cour administrative d'appel de Lyon a annulé le jugement du 15 mars 2011 ainsi que l'arrêté préfectoral du 17 février 2010, au motif que le délai de validité de l'enquête publique d'un an prévu au I de l'article L. 11-5 du code de l'expropriation pour cause d'utilité publique était expiré à la date de cet arrêté et qu'il était donc nécessaire de procéder à une nouvelle enquête ; qu'il y a lieu de joindre ces deux pourvois, qui sont dirigés contre le même arrêt, pour statuer par une seule décision ; <br/>
<br/>
              2. Considérant qu'aux termes du I de l'article L. 11-5 du code de l'expropriation pour cause d'utilité publique : " L'acte déclarant l'utilité publique doit intervenir au plus tard un an après la clôture de l'enquête préalable. Ce délai est majoré de six mois lorsque la déclaration d'utilité publique ne peut être prononcée que par décret en Conseil d'Etat. Passé l'un ou l'autre de ces délais, il y a lieu de procéder à une nouvelle enquête. (...). " ; <br/>
<br/>
              3. Considérant qu'il résulte de ces dispositions que l'autorité compétente de l'Etat dispose d'un délai d'un an, éventuellement augmenté de six mois, à compter de la clôture de l'enquête préalable, pour déclarer d'utilité publique le projet ; qu'il en va toutefois différemment lorsque cette autorité refuse de prononcer cette déclaration et que cette décision de refus est annulée par le juge administratif ; que, dans un tel cas, le délai d'un an recommence à courir à compter de la date à laquelle la décision d'annulation a été notifiée à l'autorité compétente ; que cette dernière peut, dans ce nouveau délai, prendre l'arrêté déclarant le projet d'utilité publique au vu des résultats de l'enquête initiale, à la condition que ne soit intervenu depuis sa réalisation aucun changement dans les circonstances de fait ou de droit rendant nécessaire l'ouverture d'une nouvelle enquête publique ; <br/>
<br/>
              4. Considérant qu'il en résulte qu'en jugeant que l'annulation de l'arrêté du 24 juin 2008 par lequel le préfet avait refusé de déclarer d'utilité publique l'opération projetée était sans incidence sur l'application du délai prévu à l'article L. 11-5 du code de l'expropriation pour cause d'utilité publique courant à compter de la clôture de l'enquête publique, le 12 octobre 2007, la cour administrative d'appel de Lyon a commis une erreur de droit ; que le ministre de l'intérieur et le VALTOM sont fondés pour ce motif, et sans qu'il soit besoin d'examiner l'autre moyen des pourvois, à demander l'annulation de l'arrêt qu'ils attaquent ;<br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de Combronde, de la commune de Montcel, de l'association " Oxygène pour l'avenir ", de l'association de protection des propriétaires et fermiers exploitants et autres titulaires de droits des communes de Combronde et Montcel et de la SARL Rozana la somme de 500 euros chacune à verser au VALTOM, au titre des articles L. 761-1 et R. 761-1 du code de justice administrative ; qu'en revanche, ces mêmes dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat et du VALTOM qui ne sont pas, dans la présente instance, les parties perdantes ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 27 septembre 2012 de la cour administrative d'appel de Lyon est annulé. <br/>
<br/>
      Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Lyon. <br/>
<br/>
Article 3 : La commune de Combronde, la commune de Montcel, l'association " Oxygène pour l'avenir ", l'association de protection des propriétaires et fermiers exploitants et autres titulaires de droits des communes de Combronde et Montcel et la SARL Rozana verseront chacune une somme de 500 euros au VALTOM, au titre des dispositions des articles L. 761-1 et R. 761-1 du code de justice administrative. <br/>
<br/>
Article 4 : Les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative par les communes de Combronde et Montcel, l'association " Oxygène pour l'avenir ", l'association de protection des propriétaires et fermiers exploitants et autres titulaires de droit des communes de Combronde et Montcel et la SARL Rozana, sont rejetées. <br/>
<br/>
Article 5 : La présente décision sera notifiée au ministre de l'intérieur, au syndicat de valorisation et de traitement des déchets ménagers et assimilés (VALTOM), à la SARL Rozana, à la commune de Montcel, premier défendeur dénommé, les autres défendeurs seront informés par la  SCP Waquet, Farge, Hazan, avocat au Conseil d'Etat et à la Cour de Cassation, qui les représente devant le Conseil d'Etat. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">34-02-02-02 EXPROPRIATION POUR CAUSE D'UTILITÉ PUBLIQUE. RÈGLES GÉNÉRALES DE LA PROCÉDURE NORMALE. ACTE DÉCLARATIF D'UTILITÉ PUBLIQUE. FORMES ET PROCÉDURE. - DÉLAI D'UN AN POUR PRENDRE LA DÉCLARATION D'UTILITÉ PUBLIQUE À COMPTER DE LA CLÔTURE DE L'ENQUÊTE PRÉALABLE (ART. L. 11-5 DU CODE DE L'EXPROPRIATION POUR CAUSE D'UTILITÉ PUBLIQUE) - EFFETS DE L'ANNULATION PAR LE JUGE ADMINISTRATIF DU REFUS DE DÉCLARER UN PROJET D'UTILITÉ PUBLIQUE - RÉOUVERTURE DU DÉLAI D'UN AN À COMPTER DE LA NOTIFICATION À L'AUTORITÉ COMPÉTENTE DE LA DÉCISION D'ANNULATION - POSSIBILITÉ POUR CETTE AUTORITÉ DE DÉCLARER LE PROJET D'UTILITÉ PUBLIQUE SANS NOUVELLE ENQUÊTE, SAUF CHANGEMENT DE CIRCONSTANCES Y FAISANT OBSTACLE.
</SCT>
<ANA ID="9A"> 34-02-02-02 Il résulte du I de l'article L. 11-5 du code de l'expropriation pour cause d'utilité publique que l'autorité compétente de l'Etat dispose d'un délai d'un an, éventuellement augmenté de six mois, à compter de la clôture de l'enquête préalable pour déclarer d'utilité publique le projet. Il en va toutefois différemment lorsque cette autorité refuse de prononcer cette déclaration et que cette décision de refus est annulée par le juge administratif. Dans un tel cas, le délai d'un an recommence à courir à compter de la date à laquelle la décision d'annulation a été notifiée à l'autorité compétente. Cette dernière peut, dans ce nouveau délai, prendre l'arrêté déclarant le projet d'utilité publique au vu des résultats de l'enquête initiale, à la condition que ne soit intervenu depuis sa réalisation aucun changement dans les circonstances de fait ou de droit rendant nécessaire l'ouverture d'une nouvelle enquête publique.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
