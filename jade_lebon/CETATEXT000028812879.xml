<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028812879</ID>
<ANCIEN_ID>JG_L_2014_03_000000362140</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/81/28/CETATEXT000028812879.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème / 3ème SSR, 31/03/2014, 362140</TITRE>
<DATE_DEC>2014-03-31</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>362140</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème / 3ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO ; SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS</AVOCATS>
<RAPPORTEUR>Mme Esther de Moustier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Nathalie Escaut</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:362140.20140331</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 23 août et 23 novembre 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour la commune d'Avignon, représentée par son maire ; la commune d'Avignon demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'arrêt n° 11MA01675-11MA01676 du 26 juin 2012 par lequel la cour administrative d'appel de Marseille, sur appel de M. A...D..., de la société ABC La Brioche Chaude, la société Aux Délices d'Anaïs et de Mme C...B..., a, en premier lieu, annulé le jugement n° 1002678-1003096-1003188-1003190 du 3 mars 2011 du tribunal administratif de Nîmes en tant qu'il a rejeté leur demande tendant à l'annulation pour excès de pouvoir de la délibération du 21 octobre 2010 par laquelle le conseil municipal de la commune d'Avignon a instauré une redevance d'utilisation du domaine public pour tous distributeurs automatiques bancaires installés en façade de bâtiment et accessibles directement depuis le domaine public ainsi que pour tous les commerces pratiquant des ventes ou activités diverses au travers de vitrines ou de comptoirs ouvrant sur le domaine public, en second lieu, annulé cette délibération en tant qu'elle instaure une telle redevance, enfin, rejeté ses conclusions tendant à l'annulation du même jugement en tant qu'il a annulé la délibération du 21 octobre 2010 en tant qu'elle prévoyait l'exonération des commerces et distributeurs assurant la vente ou la location d'objets ou de services culturels ; <br/>
<br/>
              2°) de mettre à la charge de M. A...D..., de la société ABC La Brioche Chaude, de la société Aux Délices d'Anaïs et de Mme C...B...le versement d'une somme de 3 500 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;  <br/>
<br/>
<br/>
               Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général de la propriété des personnes publiques ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Esther de Moustier, auditeur,  <br/>
<br/>
              - les conclusions de Mme Nathalie Escaut, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de la commune d'Avignon et à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de M.D..., de la société ABC la Brioche Chaude, de la société <br/>
aux Délices d'Anaïs et de Mme B...;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la cour administrative d'appel de Marseille a annulé, par un arrêt du 26 juin 2012 contre lequel la commune d'Avignon se pourvoit en cassation, la délibération du 21 octobre 2010 par laquelle son conseil municipal a instauré une redevance d'utilisation du domaine public pour tous les distributeurs automatiques bancaires installés en façade de bâtiment et accessibles directement depuis le domaine public ainsi que pour tous les commerces exerçant leur activité au travers de vitrines ou de comptoirs ouvrant sur le domaine public ; <br/>
<br/>
              2. Considérant qu'aux termes des dispositions de l'article L. 2122-1 du code général de la propriété des personnes publiques : " Nul ne peut, sans disposer d'un titre l'y habilitant, occuper une dépendance du domaine public d'une personne publique mentionnée à l'article L. 1 ou l'utiliser dans des limites dépassant le droit d'usage qui appartient à tous " ; que l'article L. 2125-1 du même code dispose que : " Toute occupation ou utilisation du domaine public d'une personne publique mentionnée à l'article L. 1 donne lieu au paiement d'une redevance (...) " et prévoit les cas dans lesquels, par dérogation à ce principe, " l'autorisation d'occupation ou d'utilisation du domaine public peut être délivrée gratuitement " ; qu'aux termes de l'article L. 2125-3 du même code : " La redevance due pour l'occupation ou l'utilisation du domaine public tient compte des avantages de toute nature procurés au titulaire de l'autorisation " ; qu'il résulte de la combinaison de ces dispositions, d'une part, que l'occupation ou l'utilisation du domaine public n'est soumise à la délivrance d'une autorisation que lorsqu'elle constitue un usage privatif de ce domaine public, excédant le droit d'usage appartenant à tous, d'autre part, que lorsqu'une telle autorisation est donnée par la personne publique gestionnaire du domaine public concerné, la redevance d'occupation ou d'utilisation du domaine public constitue la contrepartie du droit d'occupation ou d'utilisation privative ainsi accordé ; que, dès lors, si la personne publique est fondée à demander à celui qui occupe ou utilise irrégulièrement le domaine public le versement d'une indemnité calculée par référence à la redevance qu'il aurait versée s'il avait été titulaire d'un titre régulier à cet effet, l'occupation ou l'utilisation du domaine public dans les limites ne dépassant pas le droit d'usage appartenant à tous, qui n'est soumise à la délivrance d'aucune autorisation, ne peut, par suite, être assujettie au paiement d'une redevance ;<br/>
<br/>
              3. Considérant, en premier lieu, qu'en jugeant que l'occupation d'une dépendance du domaine public ne pouvait être assujettie au versement d'une redevance domaniale dès lors qu'elle n'était pas soumise à la délivrance d'une autorisation par le gestionnaire de ce domaine, la cour administrative d'appel de Marseille n'a pas méconnu les dispositions du code général de la propriété des personnes publiques citées au point 2 ; <br/>
<br/>
              4. Considérant, en deuxième lieu, qu'en jugeant que la seule présence sur le domaine public, le temps d'une transaction bancaire ou commerciale, de la clientèle des établissements bancaires et commerciaux que la délibération litigieuse assujettit au paiement d'une redevance d'occupation du domaine public, n'est pas constitutive d'un usage privatif du domaine public par ces établissements, dès lors que ceux-ci ne disposent d'aucune installation sur le domaine public, la cour n'a commis ni erreur de droit ni erreur de qualification juridique des faits  ; <br/>
<br/>
              5. Considérant, en troisième lieu, qu'en jugeant que la présence momentanée des clients des établissements en cause sur le domaine public, le temps d'effectuer une transaction, qui n'est ni exclusive de la présence d'autres usagers du domaine public ni incompatible avec l'affectation de celui-ci, n'est pas constitutive, pour ces établissements, quand bien même elle est nécessaire au mode d'exercice de leur commerce, d'une occupation du domaine public excédant le droit d'usage qui appartient à tous, la cour n'a pas commis d'erreur de droit ; <br/>
<br/>
              6. Considérant, en quatrième lieu, qu'il ressort des termes même de la délibération de la commune d'Avignon du 21 octobre 2010 que celle-ci ne précise pas que sont seuls soumis à la redevance qu'elle instaure les établissements dont l'activité commerciale est exclusivement exercée au moyen d'une vitrine, d'un comptoir ou d'un distributeur ; que, par suite, la commune n'est pas fondée à soutenir, en tout état de cause, que la cour aurait commis une erreur de droit et dénaturé cette délibération en relevant que l'occupation du domaine public communal concourrait à l'exercice par les établissements concernés d'une partie, et non de la totalité, de leurs activités commerciales et économiques ; <br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que la commune d'Avignon n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque ; que ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées ; qu'il y a lieu, en revanche, dans les circonstances de l'espèce, de mettre à la charge de la commune d'Avignon à verser à M.D..., aux sociétés ABC la Brioche Chaude et aux Délices d'Anaïs et à Mme B...la somme de 750 euros chacun au titre de ces mêmes dispositions ; <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la commune d'Avignon est rejeté.<br/>
Article 2 : La commune d'Avignon versera à M.D..., à la société ABC La Brioche Chaude, à la société Aux Délices d'Anaïs et à Mme B... la somme de 750 euros chacun au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la commune d'Avignon, à M. A... D..., à la société ABC La Brioche Chaude, à la société Aux Délices d'Anaïs et à Mme C...B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">24-01-02-01 DOMAINE. DOMAINE PUBLIC. RÉGIME. OCCUPATION. - 1) NÉCESSITÉ D'UNE AUTORISATION SEULEMENT LORSQUE L'OCCUPATION OU L'UTILISATION CONSTITUE UN USAGE PRIVATIF DU DOMAINE PUBLIC, EXCÉDANT LE DROIT D'USAGE APPARTENANT À TOUS - REDEVANCE CONSTITUANT LA CONTREPARTIE DU DROIT D'OCCUPATION OU D'UTILISATION PRIVATIVE AINSI ACCORDÉ - CONSÉQUENCE - FACULTÉ D'ASSUJETTIR AU PAIEMENT D'UNE REDEVANCE UNE OCCUPATION OU UTILISATION DU DOMAINE PUBLIC NE DÉPASSANT PAS LE DROIT D'USAGE APPARTENANT À TOUS - ABSENCE - 2) ESPÈCE - PRÉSENCE MOMENTANÉE DES CLIENTS D'UN ÉTABLISSEMENT BANCAIRE OU COMMERCIAL SUR LE DOMAINE PUBLIC, LE TEMPS D'EFFECTUER UNE TRANSACTION - OCCUPATION DU DOMAINE PUBLIC EXCÉDANT LE DROIT D'USAGE QUI APPARTIENT À TOUS - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">24-01-02-01-01 DOMAINE. DOMAINE PUBLIC. RÉGIME. OCCUPATION. UTILISATIONS PRIVATIVES DU DOMAINE. - ABSENCE - PRÉSENCE MOMENTANÉE DES CLIENTS D'UN ÉTABLISSEMENT BANCAIRE OU COMMERCIAL SUR LE DOMAINE PUBLIC, LE TEMPS D'EFFECTUER UNE TRANSACTION.
</SCT>
<ANA ID="9A"> 24-01-02-01 1) Il résulte de la combinaison des dispositions des articles L. 2122-1, L. 2125-1 et L. 2125-3 du code général de la propriété des personnes publiques, d'une part, que l'occupation ou l'utilisation du domaine public n'est soumise à la délivrance d'une autorisation que lorsqu'elle constitue un usage privatif de ce domaine public, excédant le droit d'usage appartenant à tous, d'autre part, que lorsqu'une telle autorisation est donnée par la personne publique gestionnaire du domaine public concerné, la redevance d'occupation ou d'utilisation du domaine public constitue la contrepartie du droit d'occupation ou d'utilisation privative ainsi accordé. Dès lors, si la personne publique est fondée à demander à celui qui occupe ou utilise irrégulièrement le domaine public le versement d'une indemnité calculée par référence à la redevance qu'il aurait versée s'il avait été titulaire d'un titre régulier à cet effet, l'occupation ou l'utilisation du domaine public dans les limites ne dépassant pas le droit d'usage appartenant à tous, qui n'est soumise à la délivrance d'aucune autorisation, ne peut être assujettie au paiement d'une redevance.,,,2) La présence momentanée des clients des établissements bancaires et commerciaux sur le domaine public, le temps d'effectuer une transaction, qui n'est ni exclusive de la présence d'autres usagers du domaine public ni incompatible avec l'affectation de celui-ci, n'est pas constitutive, pour ces établissements, quand bien même elle est nécessaire au mode d'exercice de leur commerce, d'une occupation du domaine public excédant le droit d'usage qui appartient à tous.</ANA>
<ANA ID="9B"> 24-01-02-01-01 La présence momentanée des clients des établissements bancaires et commerciaux sur le domaine public, le temps d'effectuer une transaction, qui n'est ni exclusive de la présence d'autres usagers du domaine public ni incompatible avec l'affectation de celui-ci, n'est pas constitutive, pour ces établissements, quand bien même elle est nécessaire au mode d'exercice de leur commerce, d'une occupation du domaine public excédant le droit d'usage qui appartient à tous.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
