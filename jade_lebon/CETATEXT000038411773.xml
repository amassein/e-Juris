<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038411773</ID>
<ANCIEN_ID>JG_L_2019_04_000000419842</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/41/17/CETATEXT000038411773.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 24/04/2019, 419842</TITRE>
<DATE_DEC>2019-04-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>419842</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DIDIER, PINET ; SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>Mme Déborah Coricon</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Cytermann</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:419842.20190424</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              La communauté de communes du Vexin-Thelle a demandé au juge des référés du tribunal administratif d'Amiens d'ordonner, sur le fondement des dispositions de l'article L. 521-1 du code de justice administrative, la suspension de l'exécution de l'arrêté du 13 décembre 2017 par lequel le préfet de l'Oise a autorisé le retrait dérogatoire des communes de Boury-en-Vexin et de Courcelles-lès-Gisors de la communauté de communes du Vexin-Thelle et de l'arrêté conjoint des préfets de l'Eure et de l'Oise du 21 décembre 2017 portant adhésion des communes de Bezu-la-Forêt, Boury-en-Vexin, Château-sur-Epte, Courcelles-les-Gisors et Martigny à la communauté de communes du Vexin-Normand en tant qu'il concerne les communes de Boury-en-Vexin et de Courcelles-lès-Gisors. <br/>
<br/>
              Par une ordonnance n ° 1800745, 1800747 du 29 mars 2018, le juge des référés a fait droit à ces demandes.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 16 et 18 avril 2018 et le 4 avril 2019 au secrétariat du contentieux du Conseil d'Etat, la commune de Courcelles-les-Gisors, la commune de Boury-en-Vexin et la communauté de communes du Vexin-Normand demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de mettre à la charge de la communauté de communes du Vexin-Thelle la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des collectivités territoriales ;<br/>
              - la loi n° 99-586 du 12 juillet 1999 ;<br/>
              - la loi n° 2010-1563 du 16 décembre 2010 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Déborah Coricon, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Laurent Cytermann, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Didier, Pinet, avocat des communes de Courcelles-Lès-Gisors et de Boury-en-Vexin, de la Communauté de communes du Vexin-Normand et à la SCP Piwnica, Molinié, avocat de la communauté de communes du Vexin-Thelle ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge des référés du tribunal administratif d'Amiens que, par un arrêté du 13 décembre 2017, le préfet de l'Oise a autorisé le retrait, sur le fondement de l'article L. 5214-26 du code général des collectivités territoriales, des communes de Boury-en-Vexin et de Courcelles-lès-Gisors de la communauté de communes du Vexin-Thelle et que, par un arrêté conjoint des préfets de l'Eure et de l'Oise du 21 décembre 2017, ces communes ont été autorisées à adhérer à la communauté de communes du Vexin-Normand. Par une ordonnance du 29  mars 2018, le juge des référés a suspendu, à la demande de la communauté de communes du Vexin-Thelle, sur le fondement de l'article L. 521-1 du code de justice administrative, l'exécution de ces deux arrêtés. La commune de Courcelles-les-Gisors, la commune de Boury-en-Vexin et la communauté de communes du Vexin-Normand demandent au Conseil d'Etat d'annuler cette ordonnance. <br/>
<br/>
              Sur le mémoire produit par le ministre de l'intérieur : <br/>
<br/>
              2. Le ministre de l'intérieur, qui avait la qualité de partie devant le juge des référés, n'a pas formé de pourvoi contre l'ordonnance de ce dernier. Il a été mis en cause par le Conseil d'Etat. Son mémoire, intitulé mémoire en défense, présenté après l'expiration du délai de recours contentieux, doit être regardé comme de simples observations en réponse à la communication qui lui a été faite par le Conseil d'Etat du pourvoi du bénéficiaire de la décision attaquée. Il n'y a, en conséquence, pas lieu de répondre aux moyens qu'il développe.<br/>
<br/>
              Sur le pourvoi de la commune de Courcelles-les-Gisors et autres : <br/>
<br/>
              3. Aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision (...) ". <br/>
<br/>
              4. Le premier alinéa de l'article L. 5214-26 du code général des collectivités territoriales dispose : " Par dérogation à l'article L. 5211-19, une commune peut être autorisée, par le représentant de l'Etat dans le département après avis de la commission départementale de la coopération intercommunale réunie dans la formation prévue au second alinéa de l'article L. 5211-45, à se retirer d'une communauté de communes pour adhérer à un autre établissement public de coopération intercommunale à fiscalité propre dont le conseil communautaire a accepté la demande d'adhésion. L'avis de la commission départementale de la coopération intercommunale est réputé négatif s'il n'a pas été rendu à l'issue d'un délai de deux mois ". <br/>
<br/>
              5. Les décisions prises par le préfet sur le fondement de ces dispositions produisent des effets ininterrompus à compter de la date d'effet du retrait et de l'adhésion qu'elles autorisent. Par suite, il ne peut être soutenu, en tout état de cause, que les décisions litigieuses auraient été entièrement exécutées et que les demandes de suspension de ces décisions étaient irrecevables. <br/>
<br/>
              6. En premier lieu, c'est par une appréciation souveraine exempte de dénaturation des pièces qui lui étaient soumises que le juge des référés a estimé que la condition tenant à l'urgence était remplie aux motifs, d'une part, que les requérantes n'établissaient pas que la suspension des arrêtés, sollicitée dans le délai de recours par la communauté de communes du Vexin-Thelle, emporterait des conséquences financières et administratives importantes justifiant qu'elle ne puisse être prononcée et, d'autre part, que l'exécution des arrêtés attaqués conduisait à des transferts de compétences et de ressources fiscales.<br/>
<br/>
              7. En deuxième lieu, en vertu de l'article L. 5211-45 du code général des collectivités territoriales : " La commission départementale de la coopération intercommunale établit et tient à jour un état de la coopération intercommunale dans le département. Elle peut formuler toute proposition tendant à renforcer la coopération intercommunale. A cette fin elle entend, à leur demande, des représentants des collectivités territoriales concernées. Le représentant de l'Etat dans le département la consulte sur tout projet de création d'un établissement public de coopération intercommunale, dans les conditions fixées à l'article L. 5211-45, et sur tout projet de création d'un syndicat mixte. Elle est saisie par le représentant de l'Etat dans le département ou à la demande de 20 % de ses membres. Elle est également consultée sur tout projet de modification du périmètre d'un établissement public de coopération intercommunale ou de fusion de tels établissements qui diffère des propositions du schéma départemental de la coopération intercommunale prévu à l'article L. 5210-1-1 (....)./ La commission départementale de la coopération intercommunale, consultée par le représentant de l'Etat dans le département sur toute demande de retrait en application des articles L. 5212-29, L. 5212-29-1 et L. 5212-30, ou d'une communauté de communes en application de l'article L. 5214-26, est composé de la moitié des membres élus par le collège visé au 1° de l'article L. 5211-43, dont deux membres représentant les communes de moins de 2 000 habitants, du quart des membres élus par le collège visé au 2° du même article L. 5211-43, et de la moitié du collège visé au 3° dudit article L. 5211-43 ". Il résulte des dispositions de ce second alinéa, demeurées inchangées après la modification du premier alinéa par la loi du 16 décembre 2010 de réforme des collectivités territoriales, que le législateur a notamment entendu soumettre à l'avis d'une formation restreinte de la commission les demandes de retrait d'une communauté de communes justifiées par le souhait des communes demanderesses d'adhérer à une autre communauté de communes, sans que soit applicable la procédure de consultation de la commission en formation plénière prévue par le premier alinéa pour les projets de modification du périmètre d'un établissement public.<br/>
<br/>
              8. Si les collectivités requérantes soutiennent que la consultation de la formation restreinte pour les procédures de retrait régies par l'article L. 5214-26 du code général des collectivités territoriales  ne répond qu'à une volonté de simplification et d'efficacité de la procédure et que la consultation de la formation plénière en lieu et place de la formation retreinte serait en conséquence sans incidence sur la légalité de l'avis rendu, il ressort de la lettre même de ces dispositions que la création de la formation restreinte a aussi pour but de renforcer la représentation des communes et des établissements intercommunaux par rapport à celle des conseils départemental et régional, qui en sont exclus. Par suite, le juge des référés n'a pas commis d'erreur de droit en jugeant de nature à faire naître un doute sérieux sur la légalité de l'arrêté du 13 décembre 2017 le moyen tiré de ce que la commission départementale de coopération intercommunale de l'Oise avait siégé dans une formation plénière pour rendre son avis sur la demande des communes de Boury-en-Vexin et de Courcelles-lès-Gisors de retrait de la communauté de communes du Vexin-Thelle, alors qu'elle aurait dû siéger, conformément au second alinéa de l'article L. 5511-45 du code général des collectivités territoriales, en formation restreinte et n'a pas non plus commis d'erreur de droit en prononçant, par voie de conséquence, la suspension de l'arrêté du 21 décembre 2017 emportant adhésion de ces communes à la communauté de communes du Vexin-Normand dès lors que la suspension du premier doit, d'office, entraîner celle du second. <br/>
<br/>
              9. En dernier lieu, si les actes administratifs doivent être pris selon les formes et conformément aux procédures prévues par les lois et règlements, un vice affectant le déroulement d'une procédure administrative préalable n'est toutefois de nature à entacher d'illégalité la décision prise que s'il ressort des pièces du dossier qu'il a été susceptible d'exercer, en l'espèce, une influence sur le sens de la décision prise ou s'il a privé les intéressés d'une garantie. S'il appartient au juge des référés, dans l'exercice de son office, de vérifier le respect de ces principes, il n'est pas tenu, à peine d'irrégularité de sa décision, de motiver son ordonnance sur ce point. Par suite, en se bornant à qualifier de moyen de nature à faire naître un doute sérieux quant à la légalité des arrêtés en litige le vice de procédure tenant à la composition de la commission départementale de coopération intercommunale, le juge des référés n'a, sur ce point, ni dénaturé les faits qui lui étaient soumis ni commis d'erreur de droit. <br/>
<br/>
              10. Il résulte de tout ce qui précède que le pourvoi des communes de Courcelles-lès-Gisors et de Boury-en-Vexin et de la communauté de communes du Vexin-Normand doit être rejeté, y compris, par voie de conséquence, leurs conclusions présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              11. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge des communes de Courcelles-lès-Gisors et de Boury-en-Vexin et de la communauté de communes du Vexin-Normand la somme globale de 3 000 euros à verser à la communauté de communes du Vexin-Thelle au titre des mêmes dispositions. <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi des communes de Courcelles-lès-Gisors et de Boury-en-Vexin et de la communauté de communes du Vexin-Normand est rejeté.  <br/>
Article 2 : Les communes de Courcelles-lès-Gisors et de Boury-en-Vexin et la communauté de communes du Vexin-Normand verseront à la communauté de communes du Vexin-Thelle une somme globale de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la commune de Courcelles-lès-Gisors, à la commune de Boury-en-Vexin, à la communauté de communes du Vexin-Normand, à la communauté de communes du Vexin-Thelle et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">135-05-01-05 COLLECTIVITÉS TERRITORIALES. COOPÉRATION. ÉTABLISSEMENTS PUBLICS DE COOPÉRATION INTERCOMMUNALE - QUESTIONS GÉNÉRALES. COMMUNAUTÉS DE COMMUNES. - DEMANDE DE RETRAIT D'UNE COMMUNE DE SA COMMUNAUTÉ DE COMMUNES EN VUE D'ADHÉRER À UNE AUTRE COMMUNAUTÉ - CONSULTATION PRÉALABLE DE LA COMMISSION DÉPARTEMENTALE DE LA COOPÉRATION INTERCOMMUNALE EN FORMATION RESTREINTE, ET NON EN FORMATION PLÉNIÈRE (ART. L. 5211-45 DU CGCT).
</SCT>
<ANA ID="9A"> 135-05-01-05 Il résulte du second alinéa de l'article L. 5211-45 du code général des collectivités territoriales (CGCT), demeuré inchangé après la modification du premier alinéa par la loi n° 2010-1563 du 16 décembre 2010, que le législateur a notamment entendu soumettre à l'avis d'une formation restreinte de la commission départementale de la coopération intercommunale les demandes de retrait d'une communauté de communes justifiées par le souhait des communes demanderesses d'adhérer à une autre communauté de communes, sans que soit applicable la procédure de consultation de la commission en formation plénière prévue par le premier alinéa pour les projets de modification du périmètre d'un établissement public.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
