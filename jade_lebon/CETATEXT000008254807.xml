<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000008254807</ID>
<ANCIEN_ID>JG_L_2006_11_000000284128</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/08/25/48/CETATEXT000008254807.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème et 7ème sous-sections réunies, 22/11/2006, 284128</TITRE>
<DATE_DEC>2006-11-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>284128</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème et 7ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Stirn</PRESIDENT>
<AVOCATS/>
<RAPPORTEUR>M. Jérôme  Marchand-Arvier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme de Silva</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 16 août 2005 au secrétariat du contentieux du Conseil d'Etat, présentée par M. Laurent A, demeurant 10, square Heudon à Champigny-sur-Marne (94500), agissant en qualité de représentant légal de ses deux filles mineures, Laura Duchesse A-B et Ninelle A ; M. A demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision en date du 4 mars 2004 par laquelle le consul général de France à Pointe-Noire (République du Congo) a refusé de délivrer un visa d'entrée et de long séjour en France à ses filles mineures, Laura Duchesse A-B et Ninelle A ;<br/>
<br/>
              2°) d'enjoindre au consul général de délivrer les visas sollicités dans un délai de huit jours à compter de la décision à intervenir, sous astreinte de 100 euros par jour de retard ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement de la somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu la convention internationale relative aux droits de l'enfant du 26 janvier 1990 ;<br/>
<br/>
              Vu le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
<br/>
              Vu le décret n° 2000-1093 du 10 novembre 2000 ; <br/>
<br/>
	Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jérôme Marchand-Arvier, Auditeur,  <br/>
<br/>
              - les conclusions de Mme Isabelle de Silva, Commissaire du gouvernement ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant que la requête de M. A, bien qu'elle ne tende expressément qu'à l'annulation de la décision du consul général de France à Pointe-Noire (Congo) du 4 mars 2004, refusant à ses filles un visa au titre du regroupement familial, doit, dès lors que M. A a saisi la commission de recours contre les décisions de refus de visa d'entrée en France d'un recours contre cette décision, être regardée comme dirigée contre la décision implicite de rejet par la commission de son recours, née depuis l'introduction de sa requête ;<br/>
<br/>
              Sans qu'il soit besoin de statuer sur la fin de non-recevoir opposée par le ministre des affaires étrangères ;<br/>
<br/>
              Considérant, en premier lieu, que, si M. A invoque l'incompétence du signataire de la décision du consul général de France à Pointe-Noire, ce moyen est inopérant, la décision de la commission de recours contre les décisions de refus de visa d'entrée en France s'étant substituée à celle du consul ; <br/>
<br/>
              Sur la légalité interne : <br/>
<br/>
              Considérant que, lorsque la venue d'une personne en France a été autorisée au titre du regroupement familial, l'autorité consulaire n'est en droit de rejeter la demande de visa dont elle est saisie à cette fin que pour un motif d'ordre public ; que figure au nombre de ces motifs l'absence de caractère probant des actes de mariage ou de filiation produits ; qu'il ressort des pièces du dossier que le procureur général près la cour d'appel de Pointe-Noire, qui avait été saisi par l'autorité consulaire française, a qualifié de « non authentiques » les réquisitions du procureur de la République auprès du tribunal de grande instance aux fins de déclaration tardive de naissance concernant les enfants Laura Duchesse et Ninelle ; qu'en estimant que, dans les circonstances de l'espèce, le caractère frauduleux de cette demande révélait un risque d'atteinte à l'ordre public justifiant que soient refusés les visas sollicités par l'intéressé au titre de la procédure de regroupement familial autorisée par les autorités préfectorales, la commission de recours contre les décisions de refus de visa d'entrée en France n'a pas inexactement apprécié les faits de l'espèce ;<br/>
<br/>
              Considérant que M. A ne peut faire état d'aucune vie familiale avec les jeunes Laura Duchesse et Ninelle, dont il est séparé depuis plusieurs années ; que, dans ces circonstances, le requérant n'est pas fondé à soutenir que la décision de la commission de recours contre les décisions de refus de visa d'entrée en France a porté au droit au respect de sa vie privée et familiale une atteinte excessive ou méconnu l'intérêt supérieur de Laura Duchesse A-B et Ninelle A ; qu'ainsi, les moyens tirés de la violation des stipulations de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et de l'article 3 de la convention internationale relative aux droits de l'enfant du 26 janvier 1990 ne peuvent qu'être écartés ;<br/>
<br/>
              Considérant qu'il résulte de ce qui précède que M. A n'est pas fondé à demander l'annulation de la décision implicite de rejet de la commission de recours contre les décisions de refus de visa d'entrée en France ;<br/>
<br/>
              Sur les conclusions aux fins d'injonction :<br/>
<br/>
              Considérant que la présente décision, qui rejette la requête de M. A, n'appelle aucune mesure d'exécution ; que, par suite, les conclusions de M. A tendant à ce que le Conseil d'Etat enjoigne, sous astreinte, au consul général de France à Pointe-Noire de délivrer les visas sollicités doivent être rejetées ;<br/>
<br/>
              Sur les conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              Considérant que ces dispositions font obstacle à ce que soit mise à la charge de l'Etat, qui n'est pas dans la présente instance la partie perdante, le versement de la somme sollicitée par M. A au titre des frais exposés par lui et non compris dans les dépens ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. A est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. Laurent A et au ministre des affaires étrangères.<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">335-005-01 ÉTRANGERS. ENTRÉE EN FRANCE. VISAS. - REQUÊTE DIRIGÉE CONTRE LE REFUS DE VISA D'ENTRÉE EN FRANCE OPPOSÉ PAR LES AUTORITÉS CONSULAIRES - CAS OÙ LA DÉCISION DE LA COMMISSION DE RECOURS CONTRE LES REFUS DE VISA D'ENTRÉE EN FRANCE, RÉGULIÈREMENT SAISIE, INTERVIENT POSTÉRIEUREMENT À LA SAISINE DU JUGE - REQUALIFICATION DES CONCLUSIONS CONTRE LA DÉCISION INITIALE DE REFUS EN CONCLUSIONS DIRIGÉES CONTRE LA DÉCISION DE REFUS OPPOSÉE PAR LA COMMISSION.
</SCT>
<ANA ID="9A"> 335-005-01 Même si un étranger auquel un refus de visa a été opposé par les autorités consulaires se borne à attaquer cette seule décision de refus devant le Conseil d'Etat, ses conclusions doivent être regardées, lorsque l'intéressé a bien saisi la commission de recours contre les refus de visa d'entrée en France, comme dirigées contre la décision de cette dernière, née postérieurement à la saisine du juge.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
