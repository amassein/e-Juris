<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025210347</ID>
<ANCIEN_ID>JG_L_2011_07_000000328651</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/21/03/CETATEXT000025210347.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 1ère sous-sections réunies, 26/07/2011, 328651</TITRE>
<DATE_DEC>2011-07-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>328651</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 1ère sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Jacques Arrighi de Casanova</PRESIDENT>
<AVOCATS>SCP THOUIN-PALAT, BOUCARD ; SCP DE CHAISEMARTIN, COURJON</AVOCATS>
<RAPPORTEUR>M. Raphaël Chambon</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Mattias Guyomar</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2011:328651.20110726</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 8 juin et 26 août 2009 au secrétariat du contentieux du Conseil d'Etat, présentés pour la COMMUNE DE PALAIS-SUR-VIENNE, représentée par son maire ; la commune demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 08BX00315 du 6 avril 2009 par lequel la cour administrative d'appel de Bordeaux a, sur la requête de la société A et autres, annulé le jugement n° 0500083, 0700907 du 20 décembre 2007 du tribunal administratif de Limoges et l'arrêté du 13 juin 2007 par lequel le maire de Palais-sur-Vienne a mis en demeure la société A et les consorts A de prendre toutes mesures à l'effet d'éliminer les déchets se trouvant sur leur propriété située au lieu-dit " Puy Moulinier ", avant le 31 juillet 2007, faute de quoi ils seraient éliminés d'office aux frais des intéressés ;<br/>
<br/>
              2°) de mettre à la charge solidaire de la société A, M. Georges A, Mme Raymonde A, Mme Denise E épouse A, Mme France Jocelyne A épouse C, M. Dominique D et Mlle Anne Louise D la somme de 8 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la directive 2006/12/CE du 5 avril 2006 ;<br/>
<br/>
              Vu le code de l'environnement ; <br/>
<br/>
              Vu le code général des collectivités territoriales ;<br/>
<br/>
              Vu la loi n° 75-633 du 15 juillet 1975 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Raphaël Chambon, Auditeur,  <br/>
<br/>
              - les observations de la SCP Thouin-Palat, Boucard, avocat de la COMMUNE DE PALAIS-SUR-VIENNE et de la SCP de Chaisemartin, Courjon, avocat de la société A et autres, <br/>
<br/>
              - les conclusions de M. Mattias Guyomar, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Thouin-Palat, Boucard, avocat de la COMMUNE DE PALAIS-SUR-VIENNE et à la SCP de Chaisemartin, Courjon, avocat de la société A et autres ;<br/>
<br/>
<br/>
<br/>
<br/>
              Sans qu'il soit besoin d'examiner les autres moyens du pourvoi ;<br/>
<br/>
              Considérant, d'une part, qu'aux termes de l'article 1er de la directive 2006/12/CE du 5 avril 2006 relative aux déchets : " Aux fins de la présente directive, on entend par : / (...) b) producteur : toute personne dont l'activité a produit des déchets ("producteur initial") et / ou toute personne qui a effectué des opérations de prétraitement, de mélange ou autres conduisant à un changement de nature ou de composition de ces déchets ; / c) détenteur : le producteur des déchets ou la personne physique ou morale qui a les déchets en sa possession (...) " ; qu'aux termes de l'article 8 de cette directive : " Les Etats membres prennent les dispositions nécessaires pour que tout détenteur de déchets : / a) les remette à un ramasseur privé ou public ou à une entreprise qui effectue les opérations visées aux annexes II A ou II B ou / b) en assure lui-même la valorisation ou l'élimination en se conformant aux dispositions de la présente directive (...) " ; que, suivant l'article 15 de la même directive : " Conformément au principe du "pollueur-payeur", le coût de l'élimination des déchets doit être supporté par : / a) le détenteur qui remet des déchets à un ramasseur ou à une entreprise visée à l'article 9, / et/ou b) les détenteurs antérieurs ou le producteur du produit générateur de déchets " ;<br/>
<br/>
              Considérant, d'autre part, qu'aux termes de l'article 2 de la loi du 15 juillet 1975, repris à l'article L. 541-2 du code de l'environnement : " Toute personne qui produit ou détient des déchets, dans des conditions de nature à produire des effets nocifs sur le sol, la flore et la faune, à dégrader les sites ou les paysages, à polluer l'air et les eaux, à engendrer des bruits et des odeurs et d'une façon générale à porter atteinte à la santé de l'homme et à l'environnement, est tenue d'en assurer ou d'en faire assurer l'élimination conformément aux dispositions prévues par la présente loi, dans des conditions propres à éviter lesdits effets " ; qu'aux termes de l'article 3 de la même loi, devenu l'article L. 541-3 du code de l'environnement : " Au cas où les déchets sont abandonnés, déposés ou traités contrairement aux dispositions de la présente loi et des règlements pris pour leur application, l'autorité titulaire du pouvoir de police peut, après mise en demeure, assurer d'office l'élimination desdits déchets aux frais du responsable " ;<br/>
<br/>
              Considérant que le propriétaire du terrain sur lequel ont été entreposés des déchets peut, en l'absence de détenteur connu de ces déchets, être regardé comme leur détenteur au sens de l'article L. 541-2 du code de l'environnement, notamment s'il a fait preuve de négligence à l'égard d'abandons sur son terrain ;<br/>
<br/>
              Considérant qu'il ressort des énonciations de l'arrêt attaqué qu'après avoir elle-même exploité, sur un terrain lui appartenant, une usine de régénération de caoutchouc, la société A a vendu son fonds de commerce et, notamment, son stock de marchandises et de matières premières, à la société Eureca, par un contrat conclu le 30 mars 1989 ; qu'ayant été mise en liquidation de biens en février 1991, la société Eureca a cessé son activité et laissé sur le terrain plusieurs milliers de tonnes de pneumatiques usagés ; qu'en jugeant que, si ces pneumatiques sont devenus des déchets à la suite de leur abandon, les requérants, en leur seule qualité de propriétaires du terrain sur lequel ont été entreposés les déchets et en l'absence de tout acte d'appropriation portant sur ceux-ci, ne peuvent être regardés comme ayant la qualité de détenteurs de ces déchets au sens de l'article L. 541-2 du code de l'environnement et comme ayant ainsi celle de responsables au sens de son article L. 541-3, la cour administrative d'appel a commis une erreur de droit ; que, dès lors, son arrêt doit être annulé ;<br/>
<br/>
              Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'il soit fait droit aux conclusions présentées à ce titre par les défendeurs ; qu'en revanche il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société A, de M. Georges A, de Mme Raymonde A, de Mme Denise E épouse A, de Mme France Jocelyne A épouse C, de M. Dominique D et de Mlle Anne Louise D le versement de la somme de 500 euros chacun à la COMMUNE DE PALAIS-SUR-VIENNE ;<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              			--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Bordeaux du 6 avril 2009 est annulé.<br/>
		Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Bordeaux.<br/>
Article 3 : La société A, M. Georges A, Mme Raymonde A, Mme Denise E épouse A, Mme France Jocelyne A épouse C, M. Dominique D et Mlle Anne Louise D verseront chacun à la COMMUNE DE PALAIS-SUR-VIENNE une somme de 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la COMMUNE DE PALAIS-SUR-VIENNE, à la société A, premier défendeur dénommé, et à la ministre de l'écologie, du développement durable, des transports et du logement. <br/>
Les autres défendeurs seront informés de la présente décision par la SCP de Chaisemartin, Courjon, avocat au Conseil d'Etat et à la Cour de cassation, qui les représente devant le Conseil d'Etat.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">44-035-05 NATURE ET ENVIRONNEMENT. - OBLIGATION D'ÉLIMINATION DES DÉCHETS PESANT SUR LEUR PRODUCTEUR OU LEUR DÉTENTEUR (ART. L. 541-2 DU CODE DE L'ENVIRONNEMENT) - NOTION DE DÉTENTEUR DES DÉCHETS - PROPRIÉTAIRE D'UN TERRAIN SUR LEQUEL DE TELS DÉCHETS SONT ENTREPOSÉS - INCLUSION.
</SCT>
<ANA ID="9A"> 44-035-05 Le propriétaire du terrain sur lequel ont été entreposés des déchets peut, en l'absence de détenteur connu de ces déchets, être regardé comme leur détenteur au sens de l'article L. 541-2 du code de l'environnement, notamment s'il a fait preuve de négligence à l'égard d'abandons sur son terrain.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
