<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044505232</ID>
<ANCIEN_ID>JG_L_2021_12_000000436516</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/50/52/CETATEXT000044505232.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 5ème chambres réunies, 15/12/2021, 436516</TITRE>
<DATE_DEC>2021-12-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>436516</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>BALAT</AVOCATS>
<RAPPORTEUR>Mme Coralie Albumazard</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier  Fuchs</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:436516.20211215</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Gurdebeke a demandé au tribunal administratif d'Amiens de réformer les articles 4.3.1 à 4.3.11 du chapitre 4.3 de l'arrêté du 5 novembre 2014 par lequel le préfet de l'Oise l'a autorisée à exploiter une installation de stockage de déchets non dangereux à Hardivillers. Par un jugement n° 1404333 du 20 juin 2017, le tribunal administratif a abrogé le chapitre 4.3 de l'arrêté du 5 novembre 2014 du préfet de l'Oise à l'issue d'un délai de six mois à compter de la notification du jugement et a enjoint au préfet de l'Oise de prendre un arrêté définissant les modalités d'application de cette mesure dans le même délai.<br/>
<br/>
              Par un arrêt n° 17DA02037 du 19 novembre 2019, la cour administrative d'appel de Douai a, sur appel de la ministre de la transition écologique et solidaire, annulé ce jugement et rejeté la demande présentée par la société Gurdebeke.  <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 5 décembre 2019, 6 mars 2020 et 19 juillet 2021 au secrétariat du contentieux du Conseil d'Etat, la société Gurdebeke demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de la ministre de la transition écologique et solidaire ;<br/>
<br/>
              3°) à titre subsidiaire, de transmettre à titre préjudiciel à la Cour de justice de l'Union européenne la question de savoir si les articles 1, 2 et 3 de la directive du Conseil du 26 avril 1999 concernant la mise en décharge des déchets, les articles 1 et 2 de la directive du Conseil du 24 septembre 1996 relative à la prévention et à la réduction intégrée de la pollution et les articles 1, 2 et 3 de la directive du Parlement européen et du Conseil du 24 novembre 2010 relative aux émissions industrielles doivent être interprétés en ce sens qu'ils s'opposent à une réglementation telle que celle prévue par l'arrêté du 10 juillet 1990 relatif à l'interdiction des rejets de certaines substances dans les eaux souterraines en provenance d'installations classées ; <br/>
<br/>
              4°) de mettre à la charge de l'Etat la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - la directive 96/61/CE du Conseil du 24 septembre 1996 relative à la prévention et à la réduction intégrée de la pollution ; <br/>
              - la directive 1999/31/CE du Conseil du 26 avril 1999 concernant la mise en décharge des déchets ;<br/>
              - la directive 2010/75/UE du Parlement européen et du Conseil du 24 novembre 2010 relative aux émissions industrielles (prévention et réduction intégrées de la pollution) ;<br/>
              - le code de l'environnement ;<br/>
              - l'arrêté ministériel du 10 juillet 1990 relatif à l'interdiction des rejets de certaines substances dans les eaux souterraines en provenance d'installations classées ; <br/>
              - l'arrêté ministériel du 9 septembre 1997 relatif aux décharges existantes et aux nouvelles installations de stockage de déchets ménagers et assimilés ; <br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Coralie Albumazard, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Olivier Fuchs, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à Me Balat, avocat de la société Gurdebeke ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 16 juillet 2010, le préfet de l'Oise a autorisé la société Gurdebeke à exploiter une installation de stockage de déchets non fermentescibles peu évolutifs et non dangereux sur le territoire de la commune d'Hardivillers. Par un jugement du 1er octobre 2013, le tribunal administratif d'Amiens a annulé cet arrêté tout en autorisant la société à poursuivre temporairement l'exploitation de son installation pour une durée d'un an, dans l'attente de la régularisation de sa situation. Par un nouvel arrêté du 5 novembre 2014, le préfet de l'Oise a autorisé la société à poursuivre l'exploitation de cette installation sous réserve du respect des prescriptions figurant en annexe de cet arrêté, au nombre desquelles figure l'interdiction d'infiltrer dans les eaux souterraines les lixiviats traités issus du stockage de déchets. A la demande de la société, le tribunal administratif d'Amiens, par un jugement du 20 juin 2017, a abrogé le chapitre 4.3 de l'arrêté du 5 novembre 2014 relatif à cette interdiction et enjoint au préfet de prendre un arrêté fixant de nouvelles prescriptions. La société Gurdebeke demande au Conseil d'Etat d'annuler l'arrêt du 19 novembre 2019 par lequel la cour administrative d'appel de Douai, faisant droit à l'appel de la ministre de la transition écologique et solidaire, a annulé le jugement du 20 juin 2017 du tribunal administratif d'Amiens et rejeté sa demande. <br/>
<br/>
              2. En premier lieu, d'une part, aux termes de l'article 1er de l'arrêté du 10 juillet 1990 du ministre chargé des installations classées relatif à l'interdiction des rejets de certaines substances dans les eaux souterraines en provenance d'installations classées : " Sont visés par le présent arrêté les rejets directs ou indirects provenant des installations classées pour la protection de l'environnement, à l'exclusion de ceux dus à la réinjection dans leur nappe d'origine d'eaux à usage géothermique, d'eaux d'exhaure des carrières et des mines ou d'eaux pompées lors de certains travaux de génie civil. (...) ". L'article 2 de cet arrêté dispose que : " Sans préjudice de textes plus contraignants applicables à différentes catégories d'installations, le rejet en provenance d'installations classées de substances relevant de l'annexe au présent arrêté est interdit dans les eaux souterraines. / Cette interdiction ne s'applique pas aux eaux pluviales qui sont soumises aux dispositions de l'article 4 ter du présent arrêté. / Les éventuelles dispositions moins contraignantes des arrêtés ministériels pris en application de la loi du 19 juillet 1976 sont abrogées. ". D'autre part, aux termes de l'article 35 de l'arrêté ministériel du 9 septembre 1997 relatif aux décharges existantes et aux nouvelles installations de stockage de déchets ménagers et assimilés, dont les dispositions sont applicables aux installations autorisées avant le 1er juillet 2016 : " Les conditions de traitement des lixiviats sont fixées par l'arrêté préfectoral. / Les lixiviats ne peuvent être rejetés dans le milieu naturel que s'ils respectent les valeurs fixées à l'article 36. (...) ". Aux termes de l'article 36 de ce même arrêté : " Les normes minimales applicables aux rejets des effluents liquides dans le milieu naturel sont fixées à l'annexe III. Lorsque les conditions locales du milieu récepteur l'exigent, des normes plus sévères sont fixées dans l'arrêté préfectoral. ". <br/>
<br/>
              3. Il résulte de la combinaison des dispositions citées au point 2 que si les lixiviats issus des installations de stockage des déchets peuvent être rejetés dans le milieu naturel lorsqu'ils respectent les valeurs fixées à l'article 36 de l'arrêté du 9 septembre 1997, ce n'est qu'à la condition que, lorsqu'ils comportent des substances relevant de l'annexe à l'arrêté du 10 juillet 1990, lequel s'applique à l'ensemble des installations classées, ils ne soient pas rejetés dans les eaux souterraines. Par suite, la cour administrative d'appel n'a pas commis d'erreur de droit ni insuffisamment motivé son arrêt en se fondant, pour faire application de l'arrêté du 10 juillet 1990 et valider l'interdiction édictée par l'arrêté préfectoral, sur le motif que les lixiviats traités par la technique de l'osmose inverse, qui permet le respect des valeurs fixées à l'article 36 de l'arrêté du 9 septembre 1997, sont rejetés dans les eaux souterraines. <br/>
<br/>
              4. En deuxième lieu, la cour administrative d'appel n'a pas commis d'erreur de droit en jugeant, d'une part, que la directive 1999/31/CE du Conseil du 26 avril 1999 concernant la mise en décharge des déchets, qui impose le recueil et le traitement des lixiviats pour éviter leur infiltration dans les sols, n'a ni pour objet ni pour effet d'interdire aux Etats membres d'adopter des réglementations plus contraignantes que les exigences générales qu'elle fixe et, d'autre part, que la directive 2010/75/UE du Parlement et du Conseil du 24 novembre 2010 relative aux émissions industrielles ne s'oppose pas à l'interdiction du rejet dans les eaux souterraines des lixiviats traités par osmose inverse. Si cette directive impose de privilégier une approche intégrée de la protection de l'environnement, la cour administrative d'appel a pu, sans dénaturer les pièces du dossier qui lui était soumis, estimer qu'il n'en ressortait pas que le recours à d'autres techniques de traitement des lixiviats engendrerait une pollution plus importante que celle résultant de leur infiltration dans les eaux souterraines après traitement par osmose inverse.   <br/>
<br/>
              5. En troisième lieu, la cour administrative d'appel n'a pas insuffisamment motivé son arrêt ni commis une erreur de droit en relevant, pour juger que le préfet de l'Oise était tenu d'interdire le rejet des lixiviats dans les eaux souterraines en application de l'arrêté ministériel du 10 juillet 1990, que les dispositions de l'article R. 512-28 du code de l'environnement exigeant du préfet qu'il tienne compte de l'efficacité des meilleures techniques disponibles pour fixer des prescriptions ne lui permettaient pas de dispenser l'installation du respect de l'interdiction résultant de l'arrêté ministériel du 10 juillet 1990. <br/>
<br/>
              6. En quatrième lieu, il ressort des énonciations de l'arrêt attaqué que, pour écarter le moyen invoqué devant elle, tiré de la méconnaissance du principe d'égalité devant les charges publiques et du principe de non-discrimination garanti par les stipulations de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, la cour administrative d'appel a relevé que l'interdiction de rejet dans les eaux souterraines s'applique à l'ensemble des lixiviats contenant certaines substances en provenance des installations classées, sans distinction entre les différents types d'installations les ayant émis et donc sans distinction entre les exploitants, et que la société Gurdebeke ne soutenait pas que les exceptions prévues au sein de ce régime ne seraient pas justifiées par une différence de situation objective. En statuant ainsi, la cour administrative d'appel a implicitement mais nécessairement écarté le moyen comme inopérant s'agissant des lixiviats en provenance des stations d'épuration, dès lors que les systèmes d'assainissement des eaux usées sont régis, au titre de la police de l'eau, par les dispositions des articles L. 214-1 à L. 214-3 du code de l'environnement. Elle n'a, par suite, pas insuffisamment motivé son arrêt ni commis d'erreur de droit.<br/>
<br/>
              7. Il résulte de tout ce qui précède, sans qu'il y ait lieu de saisir d'une question préjudicielle la Cour de justice de l'Union européenne, que le pourvoi de la société Gurdebeke doit être rejeté, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la société Gurdebeke est rejeté. <br/>
Article 2 : La présente décision sera notifiée à la société Gurdebeke et à la ministre de la transition écologique.<br/>
              Délibéré à l'issue de la séance du 22 novembre 2021 où siégeaient : M. Rémy Schwartz, président adjoint de la Section du contentieux, présidant ; M. Fabien Raynaud, président de chambre ; Mme H... D..., M. F... B..., Mme C... E..., M. Cyril Roger-Lacan, conseillers d'Etat et Mme Coralie Albumazard, maître des requêtes en service extraordinaire-rapporteure. <br/>
<br/>
              Rendu le 15 décembre 2021.<br/>
<br/>
<br/>
                 Le président : <br/>
                 Signé : M. Rémy Schwartz<br/>
 		La rapporteure : <br/>
      Signé : Mme Coralie Albumazard<br/>
                 La secrétaire :<br/>
                 Signé : Mme G... A...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">27-06-01 EAUX. - ARRÊTÉ DU 10 JUILLET 1990 RELATIF À L'INTERDICTION DES REJETS DE CERTAINES SUBSTANCES DANS LES EAUX SOUTERRAINES EN PROVENANCE D'INSTALLATIONS CLASSÉES - APPLICATION CONCOMITANTE À CELLE DE L'ARRÊTÉ DU 9 SEPTEMBRE 1997 RELATIF AUX DÉCHARGES EXISTANTES ET AUX NOUVELLES INSTALLATIONS DE STOCKAGE DE DÉCHETS MÉNAGERS ET ASSIMILÉS - EXISTENCE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">44-02-01-01 NATURE ET ENVIRONNEMENT. - INSTALLATIONS CLASSÉES POUR LA PROTECTION DE L'ENVIRONNEMENT. - CHAMP D'APPLICATION DE LA LÉGISLATION. - INDÉPENDANCE À L'ÉGARD D'AUTRES LÉGISLATIONS. - ABSENCE, VIS-À-VIS DE L'ARRÊTÉ DU 10 JUILLET 1990 RELATIF À L'INTERDICTION DES REJETS DE CERTAINES SUBSTANCES DANS LES EAUX SOUTERRAINES EN PROVENANCE D'INSTALLATIONS CLASSÉES [RJ1].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">44-035-04 NATURE ET ENVIRONNEMENT. - ARRÊTÉ DU 9 SEPTEMBRE 1997 RELATIF AUX DÉCHARGES EXISTANTES ET AUX NOUVELLES INSTALLATIONS DE STOCKAGE DE DÉCHETS MÉNAGERS ET ASSIMILÉS - APPLICATION CONCOMITANTE À CELLE DE L'ARRÊTÉ DU 10 JUILLET 1990 RELATIF À L'INTERDICTION DES REJETS DE CERTAINES SUBSTANCES DANS LES EAUX SOUTERRAINES EN PROVENANCE D'INSTALLATIONS CLASSÉES - EXISTENCE [RJ1].
</SCT>
<ANA ID="9A"> 27-06-01 Si les lixiviats issus des installations de stockage des déchets peuvent être rejetés dans le milieu naturel lorsqu'ils respectent les valeurs fixées à l'article 36 de l'arrêté du 9 septembre 1997 relatif aux décharges existantes et aux nouvelles installations de stockage de déchets ménagers et assimilés, ce n'est qu'à la condition que, lorsqu'ils comportent des substances relevant de l'annexe à l'arrêté du 10 juillet 1990 relatif à l'interdiction des rejets de certaines substances dans les eaux souterraines en provenance d'installations classées, lequel s'applique à l'ensemble des installations classées, ils ne soient pas rejetés dans les eaux souterraines.</ANA>
<ANA ID="9B"> 44-02-01-01 Si les lixiviats issus des installations de stockage des déchets peuvent être rejetés dans le milieu naturel lorsqu'ils respectent les valeurs fixées à l'article 36 de l'arrêté du 9 septembre 1997 relatif aux décharges existantes et aux nouvelles installations de stockage de déchets ménagers et assimilés, ce n'est qu'à la condition que, lorsqu'ils comportent des substances relevant de l'annexe à l'arrêté du 10 juillet 1990 relatif à l'interdiction des rejets de certaines substances dans les eaux souterraines en provenance d'installations classées, lequel s'applique à l'ensemble des installations classées, ils ne soient pas rejetés dans les eaux souterraines.</ANA>
<ANA ID="9C"> 44-035-04 Si les lixiviats issus des installations de stockage des déchets peuvent être rejetés dans le milieu naturel lorsqu'ils respectent les valeurs fixées à l'article 36 de l'arrêté du 9 septembre 1997 relatif aux décharges existantes et aux nouvelles installations de stockage de déchets ménagers et assimilés, ce n'est qu'à la condition que, lorsqu'ils comportent des substances relevant de l'annexe à l'arrêté du 10 juillet 1990 relatif à l'interdiction des rejets de certaines substances dans les eaux souterraines en provenance d'installations classées, lequel s'applique à l'ensemble des installations classées, ils ne soient pas rejetés dans les eaux souterraines.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., s'agissant de l'applicabilité aux ICPE des dispositions du code de l'environnement relatives aux objectifs de qualité et de quantité des eaux, CE, 17 avril 2015, Société Porteret Beaulieu Industrie, n° 368397, T. pp. 685-765.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
