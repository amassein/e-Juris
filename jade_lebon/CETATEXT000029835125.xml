<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029835125</ID>
<ANCIEN_ID>JG_L_2014_12_000000382696</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/83/51/CETATEXT000029835125.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème / 3ème SSR, 03/12/2014, 382696</TITRE>
<DATE_DEC>2014-12-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>382696</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème / 3ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROUSSEAU, TAPIE</AVOCATS>
<RAPPORTEUR>M. Mathieu Herondart</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Nathalie Escaut</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:382696.20141203</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              	Vu la procédure suivante :<br/>
<br/>
Procédure contentieuse antérieure <br/>
<br/>
              M. A...D...a demandé au tribunal administratif de Grenoble d'annuler les opérations électorales qui se sont déroulées le 23 mars 2014 dans la commune de Brié-et-Angonnes (Isère) en vue de la désignation des conseillers municipaux. Par un jugement n° 1401725 du 13 juin 2014, le tribunal administratif de Grenoble a annulé ces opérations électorales.<br/>
<br/>
Procédure devant le Conseil d'Etat<br/>
<br/>
              1° Sous le n° 382696, par une requête et un mémoire enregistrés les 16 juillet et 13 août 2014, M. B...C...demande au Conseil d'Etat :<br/>
              1°) d'annuler ce jugement n° 1401725 du 13 juin 2014 du tribunal administratif de Grenoble ;<br/>
              2°) de mettre à la charge de M. D...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
              Il soutient que :<br/>
              - la lettre adressée le 4 mars 2014 aux personnes désignées pour leur demander d'assurer une permanence en application de l'article R. 42 du code électoral n'interdisait nullement aux assesseurs de la liste d'exercer leurs fonctions en dehors des plages horaires sur lesquelles ils ont été désignés ;<br/>
              - aucun élément du dossier n'indique que les assesseurs désignés par la liste " Brié et Angonnes 2014 " auraient été empêchés d'être présents toute la journée dans les bureaux de vote de la commune ;<br/>
              - il n'est pas soutenu, et encore moins établi, que les conditions de désignation des assesseurs dans les bureaux de vote de la commune auraient constitué des manoeuvres ayant pour but ou pour effet de fausser les résultats du scrutin ;<br/>
              - le tribunal administratif de Grenoble ne pouvait prononcer l'annulation eu égard à l'écart de voix important existant entre les deux listes ;<br/>
<br/>
              Par un mémoire en défense et un nouveau mémoire, enregistrés les 11 septembre et 13 octobre 2014, M. D...conclut au rejet de la requête. Il soutient que les moyens soulevés par M. C...ne sont pas fondés.<br/>
<br/>
<br/>
<br/>
              2° Sous le n° 382748, par une requête enregistrée le 16 juillet 2014 au secrétariat du contentieux du Conseil d'Etat, le préfet de l'Isère demande au Conseil d'Etat d'annuler le même jugement. <br/>
<br/>
              Il  soutient que :<br/>
              - la liste " Brié et Angonnes 2014 " ne peut se prévaloir du fait que la désignation de ses deux assesseurs sur des tranches horaires de deux heures l'a empêchée de contrôler la sincérité des opérations de vote, alors que cette décision pouvait être contestée dès le 19 février 2014 par cette liste qui, au demeurant, n'a formulé aucune observation sur le procès-verbal ;<br/>
              - il n'est pas établi que les conditions de désignation des assesseurs aient été constitutives d'une manoeuvre destinée à fausser les résultats du scrutin ;<br/>
              - en tout état de cause, compte tenu de l'écart de voix important entre les listes, la validité du scrutin ne saurait être remise en cause ;<br/>
<br/>
              Par un mémoire en défense, enregistré le 18 août 2014, et deux nouveaux mémoires, enregistrés les 11 septembre et 13 octobre 2014 et identiques à ceux présentés sous le n° 382696,  M. D...conclut au rejet de la requête. Il soutient que les moyens soulevés par le préfet ne sont pas fondés.<br/>
<br/>
<br/>
<br/>
<br/>
              Vu :<br/>
<br/>
              - les autres pièces des dossiers ;<br/>
<br/>
              - le code électoral ;<br/>
<br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Mathieu Herondart, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Nathalie Escaut, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rousseau, Tapie, avocat de M. C...;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 21 novembre 2014, présentée par M. D....<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que, lors des opérations électorales auxquelles il a été procédé le 23 mars 2014 pour l'élection des conseillers municipaux et conseillers communautaires de la commune de Brié-et-Angonnes, la liste " Unis pour Brié-et-Angonnes " conduite par M. B...C...a obtenu 724 voix et la " liste Brié et Angonnes 2014 " conduite par M. A...D...a obtenu 607 voix ; que, sur la protestation de M.D..., le tribunal administratif de Grenoble a annulé ces opérations électorales par un jugement du 13 juin 2014 ; que par deux requêtes, qu'il y a lieu de joindre, le préfet de l'Isère et M. C...font appel de ce jugement ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article R. 42 du code électoral : " Chaque bureau de vote est composé d'un président, d'au moins deux assesseurs et d'un secrétaire choisi par eux parmi les électeurs de la commune. (...) Deux membres du bureau au moins doivent être présents pendant tout le cours des opérations électorales (...) " ; qu'aux termes de l'article R. 44 du même code : " Les assesseurs de chaque bureau sont désignés conformément aux dispositions ci-après : / Chaque candidat, binôme de candidats ou chaque liste en présence a le droit de désigner un assesseur et un seul pris parmi les électeurs du département (...) " ; qu'aux termes de l'article R. 45 : " Chaque candidat, binôme de candidats ou chaque liste en présence, habilité à désigner un assesseur peut également désigner son suppléants pris parmi les électeurs du département (...) " ; qu'enfin, l'article R. 46 dispose que : " Les noms, prénoms, date et lieu de naissance et adresse des assesseurs et de leurs suppléants désignés par les candidats ou listes en présence, ainsi que l'indication du bureau de vote auquel ils sont affectés, notifiés au maire, par pli recommandé, au plus tard l'avant-veille du scrutin à 18 heures. / Le maire délivre un récépissé à cette déclaration. Ce récépissé servira de titre et garantira les droits attachés à la qualité d'assesseur ou de suppléant. / Le maire notifie les noms, prénoms, date et lieu de naissance des assesseurs et suppléants ainsi désignés, au président de chaque bureau de vote intéressé avant la constitution desdits bureaux " ;<br/>
<br/>
              3. Considérant qu'en arrêtant, par un courrier du 4 mars 2014, une organisation des permanences dans les deux bureaux de vote lors du scrutin du 23 mars 2014, pour assurer le respect des dispositions de l'article R. 42 du code électoral qui prévoient que deux membres du bureau au moins doivent être présents pendant tout le cours des opérations électorales, le maire de Brié-et-Angonnes n'a pas interdit aux assesseurs désignés par la liste " Brié et Angonnes 2014 " d'assurer le contrôle des opérations de vote pendant toute cette journée, alors même que les deux assesseurs de la liste n'étaient désignés que pour assurer une  permanence de deux heures dans chacun des bureaux de vote ; qu'il ne résulte pas de l'instruction, notamment des observations portées aux procès-verbaux des différents bureaux de vote, que les assesseurs de cette liste auraient été écartés des bureaux de vote pendant le reste de la journée ; que, par suite, le préfet de l'Isère et M. C...sont fondés à soutenir que c'est à tort que le tribunal administratif de Grenoble s'est fondé sur l'organisation de cette permanence pour annuler les opérations électorales qui se sont déroulées le 23 mars 2014 pour la désignation des conseillers municipaux de la commune de Brié-et-Angonnes ; <br/>
<br/>
              4. Considérant qu'il y a lieu pour le Conseil d'Etat, saisi par l'effet dévolutif de l'appel, d'examiner les autres griefs soulevés par M. D...dans sa protestation ;<br/>
<br/>
              5. Considérant que M. C...n'a pas méconnu les dispositions du premier alinéa de l'article R. 27 du code électoral, selon lesquelles : " les affiches et circulaires ayant un but ou un caractère électoral qui comprennent une combinaison des trois couleurs : bleu, blanc, rouge à l'exception de la reproduction de l'emblème d'un parti ou groupement politique sont interdites ", en choisissant une affiche électorale et une circulaire polychromes qui utilisaient, parmi d'autres teintes, les couleurs bleu, blanc et rouge ; qu'aucune disposition du code électoral n'interdit d'utiliser des mêmes éléments pour les affiches et les circulaires électorales ;<br/>
<br/>
              6. Considérant qu'il résulte de l'instruction qu'eu égard à leur contenu, l'envoi de messages sur téléphone mobile (" SMS ") par l'un des candidats à certaines personnes de sa connaissances, invitant à ne pas voter pour la liste conduite par M.D..., n'a pu, en tout état de cause, altérer les résultats du scrutin ; qu'il ne résulte pas de l'instruction que, pour regrettable qu'elle ait été, la diffusion de documents comportant des allusions malveillantes à l'encontre de M. D...ait été de nature à fausser les résultats de l'élection litigieuse, compte tenu de l'écart existant entre le nombre de voix obtenues par chacune des listes ; <br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que le préfet de l'Isère et M. C... sont fondés à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Grenoble a annulé les opérations électorales qui se sont déroulées le 23 mars 2014 dans la commune de Brié-et-Angonnes ;<br/>
<br/>
              8. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par M. C...au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              		D E C I D E :<br/>
              		--------------<br/>
<br/>
Article 1er  : Le jugement du tribunal administratif de Grenoble du 23 juin 2014 est annulé.<br/>
<br/>
Article 2 : Les opérations électorales qui se sont déroulées le 23 mars 2014 dans la commune de .Brié-et-Angonnes sont validées.<br/>
<br/>
Article 3 : La protestation de M. D...est rejetée<br/>
Article 4 : Les conclusions présentées par M. C...au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 5 : La présente décision sera notifiée à M. B...C..., à M. A...D...et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">28-005-03 ÉLECTIONS ET RÉFÉRENDUM. DISPOSITIONS GÉNÉRALES APPLICABLES AUX ÉLECTIONS. OPÉRATIONS ÉLECTORALES. - PRÉSENCE D'AU MOINS DEUX MEMBRES DU BUREAU PENDANT TOUT LE COURS DES OPÉRATIONS (ART. R. 42 DU CODE ÉLECTORAL) - ORGANISATION D'UNE PERMANENCE - DÉSIGNATION DES ASSESSEURS D'UNE LISTE POUR ASSURER UNE PERMANENCE DE DEUX HEURES - IRRÉGULARITÉ DES OPÉRATIONS ÉLECTORALES - ABSENCE DÈS LORS QUE CES ASSESSEURS ONT PU ASSURER LE CONTRÔLE DES OPÉRATIONS TOUTE LA JOURNÉE, EN SUS DE LEUR PERMANENCE.
</SCT>
<ANA ID="9A"> 28-005-03 En arrêtant une organisation des permanences dans les bureaux de vote lors d'un scrutin, pour assurer le respect des dispositions de l'article R. 42 du code électoral qui prévoient que deux membres du bureau au moins doivent être présents pendant tout le cours des opérations électorales, un maire n'a pas interdit aux assesseurs désignés par une des listes d'assurer le contrôle des opérations de vote pendant toute cette journée, alors même que les deux assesseurs de la liste n'étaient désignés que pour assurer une  permanence de deux heures dans chacun des bureaux de vote. Par suite, dès lors qu'il ne résulte pas de l'instruction, notamment des observations portées aux procès-verbaux des différents bureaux de vote, que les assesseurs de cette liste auraient été écartés des bureaux de vote pendant le reste de la journée, l'organisation de cette permanence n'est pas de nature à entraîner l'annulation des opérations électorales.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
