<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039442388</ID>
<ANCIEN_ID>JG_L_2019_12_000000412941</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/44/23/CETATEXT000039442388.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 1ère chambres réunies, 02/12/2019, 412941</TITRE>
<DATE_DEC>2019-12-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>412941</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MATUCHANSKY, POUPOT, VALDELIEVRE ; SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>Mme Tiphaine Pinault</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:412941.20191202</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A... B... a demandé au tribunal administratif de Paris d'annuler pour excès de pouvoir la décision du 26 novembre 2014 par laquelle le directeur par intérim de la 19ème section du centre d'action sociale de la Ville de Paris (CASVP) a mis fin à ses fonctions, d'enjoindre au CASVP de le réintégrer en qualité d'agent contractuel pour une durée indéterminée et de condamner le CASVP à lui verser diverses sommes. Par un jugement n° 1501436 du 15 avril 2016, le tribunal administratif a annulé cette décision, enjoint au CASVP de réintégrer M. B... et de prendre une nouvelle décision sur sa situation, condamné le CASVP à lui verser une somme de 9 144 euros et rejeté le surplus de ses conclusions.<br/>
<br/>
              Par un arrêt n°s 16PA01918, 16PA01937 du 30 mai 2017, la cour administrative d'appel de Paris a, sur appel du CASVP et sur appel incident de M. B..., condamné le CASVP à verser à M. B... une somme de 2 000 euros en réparation du préjudice moral causé par la décision mettant fin à ses fonctions, augmentée d'une somme correspondant au paiement des vacations que ce dernier n'a pas effectuées au mois de novembre 2014, dit qu'il n'y avait plus lieu de statuer sur les conclusions du CASVP tendant à l'annulation de l'article 2 de ce jugement faisant injonction au centre de prendre une nouvelle décision sur la situation de M. B... et réformé le jugement attaqué en ce qu'il avait de contraire à son arrêt. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 31 juillet et 31 octobre 2017 et le 8 août 2018 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt en tant qu'il lui fait grief ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 600 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - la loi n° 83- 634 du 13 juillet 1983 ; <br/>
              - la loi n° 84-53 du 26 janvier 1984 ;<br/>
              - la loi n° 2012-347 du 12 mars 2012 ;<br/>
              - le décret n° 88-145 du 15 février 1988 ; <br/>
              - le décret n° 94-415 du 24 mai 1994 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Tiphaine Pinault, maître des requêtes en service extraordinaire,<br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Matuchansky, Poupot, Valdelièvre, avocat de M. A... B... et à la SCP Foussard, Froger, avocat du centre d'action sociale de la Ville de Paris ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1.  Il ressort des pièces du dossier soumis aux juges du fond que M. B... a été, à compter du 17 décembre 2004, employé à de multiples reprises par le centre d'action sociale de la Ville de Paris (CASVP) pour exercer, en particulier pendant les week-ends et les jours fériés, des fonctions de gardien remplaçant au sein de résidences destinées à l'accueil de personnes âgées dépendant de la 19ème section du CASVP. A ce titre, il lui a été demandé, en octobre 2014, de remplacer les gardiens titulaires pendant plusieurs périodes s'échelonnant entre le 7 novembre et le 1er décembre 2014. Toutefois, par une décision du 6 novembre 2014, le directeur de la 19ème section du CASVP a suspendu l'exécution de cette mission, puis, par une décision du 26 novembre 2014, décidé qu'il ne ferait désormais plus appel à M. B..., en raison de son insuffisance professionnelle. Par un jugement du 15 avril 2016, le tribunal administratif de Paris a jugé que M. B... devait être regardé comme un agent non titulaire et a annulé la décision du 26 novembre 2014, seule attaquée, enjoint au CASVP de réintégrer M. B..., condamné le CASVP à verser à M. B... une somme de 6 912 &#128; au titre de l'indemnité de licenciement et une somme de 3 000 &#128; au titre du préjudice moral, enfin rejeté les conclusions de M. B... tendant à ce que le CASVP soit condamné à lui verser des rappels de salaire et à ce qu'il le réintègre sur un  contrat à durée indéterminée. M. B... se pourvoit en cassation contre l'arrêt du 30 mai 2017 par lequel la cour administrative d'appel de Paris, statuant sur l'appel du CASVP et sur son appel incident, a jugé qu'il n'y avait pas lieu de statuer sur les conclusions du CASVP tendant à l'annulation du jugement du 15 avril 2016 du tribunal administratif de Paris en tant qu'il lui enjoint de réintégrer M. B... et, estimant que ce dernier devait être regardé, non comme un agent non titulaire, mais comme un agent accomplissant des vacations, a décidé de  ramener de 3 000 &#128; à 2 000 &#128; la somme que le centre a été condamné à lui verser au titre du préjudice moral. <br/>
<br/>
              2. La loi du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale, dans sa rédaction issue de la loi du 12 mars 2012 relative à l'accès à l'emploi titulaire et à l'amélioration des conditions d'emploi des agents contractuels dans la fonction publique, à la lutte contre les discriminations et portant diverses dispositions relatives à la fonction publique, fixe aux articles 3-1 à 3-3 les cas dans lesquels les emplois permanents des collectivités territoriales peuvent par exception être pourvus par des agents non titulaires. L'article 136 de cette loi fixe les règles d'emploi de ces agents et précise qu'un décret en Conseil d'Etat déterminera les conditions d'application de cet article. Aux termes de l'article 1er du décret du 15 février 1988 relatif aux agents non titulaires de la fonction publique territoriale, dans sa rédaction alors en vigueur, applicable aux agents des administrations parisiennes en vertu du deuxième alinéa de l'article 4 du décret du 24 mai 1994 portant dispositions statutaires relatives aux personnels des administrations parisiennes : " Les dispositions du présent décret s'appliquent aux agents non titulaires de droit public des collectivités et établissements mentionnés à l'article 2 de la loi n° 84-53 du 26 janvier 1984 (...). Les dispositions du présent décret ne sont toutefois pas applicables aux agents engagés pour un acte déterminé ". En outre, aux termes de l'article 55 du décret du 24 mai 1994 : " Les fonctions qui, correspondant à un besoin permanent, impliquent un service à temps non complet sont assurées par des agents non titulaires ".<br/>
<br/>
              3. Un agent de droit public employé par une collectivité ou un établissement mentionné au premier alinéa de l'article 2 de la loi du 26 janvier 1984 doit être regardé comme ayant été engagé pour exécuter un acte déterminé lorsqu'il a été recruté pour répondre ponctuellement à un besoin de l'administration. La circonstance que cet agent a été recruté plusieurs fois pour exécuter des actes déterminés n'a pas pour effet, à elle seule, de lui conférer la qualité d'agent contractuel. En revanche, lorsque l'exécution d'actes déterminés multiples répond à un besoin permanent de l'administration, l'agent doit être regardé comme ayant la qualité d'agent non titulaire de l'administration.<br/>
<br/>
              4. Il ressort des pièces du dossier soumis aux juges du fond qu'entre 2004 et 2014, M. B... a été régulièrement employé par la 19ème section du CASVP en vue de remplacer les gardiens titulaires de résidences accueillant des personnes âgées lorsque ces derniers prenaient leur repos hebdomadaire, leurs congés légaux ou des jours de récupération, le fonctionnement de ces résidences exigeant la présence permanente jour et nuit d'une personne chargée d'assurer les fonctions de gardien et les agents titulaires ne suffisant pas à répondre à ce besoin.  Dès lors, en jugeant que les missions exercées par M. B... ne répondaient pas à un besoin permanent de l'administration et qu'en conséquence ce dernier ne pouvait être regardé comme un agent non titulaire, la cour a inexactement qualifié les faits qui lui étaient soumis. Sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, M. B... est donc fondé à demander l'annulation de l'arrêt de la cour administrative d'appel de Paris en tant qu'il lui fait grief.  <br/>
<br/>
              5. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de M. B..., qui n'est pas, dans la présente instance, la partie perdante, la somme demandée par le CASVP, au titre des frais exposés et non compris dans les dépens. En outre, l'Etat n'étant pas partie à la présente instance, les conclusions de M. B... tendant à ce qu'une somme soit mise à sa charge au titre des mêmes dispositions ne peuvent qu'être rejetées.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les articles 2, 3 et 4 de l'arrêt du 30 mai 2017 de la cour administrative d'appel de Paris sont annulés. <br/>
Article 2 : L'affaire est renvoyée dans cette mesure à la cour administrative d'appel de Paris. <br/>
Article 3 : Les conclusions de M. B... et du centre d'action sociale de la Ville de Paris présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 4: La présente décision sera notifiée à M. A... B... et au centre d'action sociale de la Ville de Paris.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-02-01-03 FONCTIONNAIRES ET AGENTS PUBLICS. CADRES ET EMPLOIS. NOTION DE CADRE, DE CORPS, DE GRADE ET D'EMPLOI. NOTION D'EMPLOI. - AGENTS NON TITULAIRES DES COLLECTIVITÉS TERRITORIALES - DÉCRET DU 15 FÉVRIER 1988 - CHAMP D'APPLICATION - EXCLUSION - AGENTS ENGAGÉS POUR EXÉCUTER UN ACTE DÉTERMINÉ (ART. 1ER) - 1) DÉFINITION - AGENTS RECRUTÉS POUR RÉPONDRE PONCTUELLEMENT À UN BESOIN DE L'ADMINISTRATION [RJ1] - 2) ILLUSTRATION.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">36-07-01-03 FONCTIONNAIRES ET AGENTS PUBLICS. STATUTS, DROITS, OBLIGATIONS ET GARANTIES. STATUT GÉNÉRAL DES FONCTIONNAIRES DE L'ÉTAT ET DES COLLECTIVITÉS LOCALES. DISPOSITIONS STATUTAIRES RELATIVES À LA FONCTION PUBLIQUE TERRITORIALE (LOI DU 26 JANVIER 1984). - AGENTS NON TITULAIRES DES COLLECTIVITÉS TERRITORIALES - DÉCRET DU 15 FÉVRIER 1988 - CHAMP D'APPLICATION - EXCLUSION - AGENTS ENGAGÉS POUR EXÉCUTER UN ACTE DÉTERMINÉ (ART. 1ER) - 1) DÉFINITION - AGENTS RECRUTÉS POUR RÉPONDRE PONCTUELLEMENT À UN BESOIN DE L'ADMINISTRATION [RJ1] - 2) ILLUSTRATION.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">36-12-01 FONCTIONNAIRES ET AGENTS PUBLICS. AGENTS CONTRACTUELS ET TEMPORAIRES. NATURE DU CONTRAT. - AGENTS NON TITULAIRES DES COLLECTIVITÉS TERRITORIALES - DÉCRET DU 15 FÉVRIER 1988 - CHAMP D'APPLICATION - EXCLUSION - AGENTS ENGAGÉS POUR EXÉCUTER UN ACTE DÉTERMINÉ (ART. 1ER) - 1) DÉFINITION - AGENTS RECRUTÉS POUR RÉPONDRE PONCTUELLEMENT À UN BESOIN DE L'ADMINISTRATION [RJ1] - 2) ILLUSTRATION.
</SCT>
<ANA ID="9A"> 36-02-01-03 1) Un agent de droit public employé par une collectivité ou un établissement mentionné au premier alinéa de l'article 2 de la loi n° 84-53 du 26 janvier 1984 doit être regardé comme ayant été engagé pour exécuter un acte déterminé lorsqu'il a été recruté pour répondre ponctuellement à un besoin de l'administration. La circonstance que cet agent a été recruté plusieurs fois pour exécuter des actes déterminés n'a pas pour effet, à elle seule, de lui conférer la qualité d'agent contractuel. En revanche, lorsque l'exécution d'actes déterminés multiples répond à un besoin permanent de l'administration, l'agent doit être regardé comme ayant la qualité d'agent non titulaire de l'administration.... ,,2) Agent ayant été régulièrement employé, entre 2004 et 2014, par le centre d'action sociale de la Ville de Paris (CASVP) en vue de remplacer les gardiens titulaires de résidences accueillant des personnes âgées lorsque ces derniers prenaient leur repos hebdomadaire, leurs congés légaux ou des jours de récupération, le fonctionnement de ces résidences exigeant la présence permanente jour et nuit d'une personne chargée d'assurer les fonctions de gardien et les agents titulaires ne suffisant pas à répondre à ce besoin....  ,,En jugeant que les missions exercées par cet agent ne répondaient pas à un besoin permanent de l'administration et qu'en conséquence ce dernier ne pouvait être regardé comme un agent non titulaire, la cour a inexactement qualifié les faits qui lui étaient soumis.</ANA>
<ANA ID="9B"> 36-07-01-03 1) Un agent de droit public employé par une collectivité ou un établissement mentionné au premier alinéa de l'article 2 de la loi n° 84-53 du 26 janvier 1984 doit être regardé comme ayant été engagé pour exécuter un acte déterminé lorsqu'il a été recruté pour répondre ponctuellement à un besoin de l'administration. La circonstance que cet agent a été recruté plusieurs fois pour exécuter des actes déterminés n'a pas pour effet, à elle seule, de lui conférer la qualité d'agent contractuel. En revanche, lorsque l'exécution d'actes déterminés multiples répond à un besoin permanent de l'administration, l'agent doit être regardé comme ayant la qualité d'agent non titulaire de l'administration.... ,,2) Agent ayant été régulièrement employé, entre 2004 et 2014, par le centre d'action sociale de la Ville de Paris (CASVP) en vue de remplacer les gardiens titulaires de résidences accueillant des personnes âgées lorsque ces derniers prenaient leur repos hebdomadaire, leurs congés légaux ou des jours de récupération, le fonctionnement de ces résidences exigeant la présence permanente jour et nuit d'une personne chargée d'assurer les fonctions de gardien et les agents titulaires ne suffisant pas à répondre à ce besoin....  ,,En jugeant que les missions exercées par cet agent ne répondaient pas à un besoin permanent de l'administration et qu'en conséquence ce dernier ne pouvait être regardé comme un agent non titulaire, la cour a inexactement qualifié les faits qui lui étaient soumis.</ANA>
<ANA ID="9C"> 36-12-01 1) Un agent de droit public employé par une collectivité ou un établissement mentionné au premier alinéa de l'article 2 de la loi n° 84-53 du 26 janvier 1984 doit être regardé comme ayant été engagé pour exécuter un acte déterminé lorsqu'il a été recruté pour répondre ponctuellement à un besoin de l'administration. La circonstance que cet agent a été recruté plusieurs fois pour exécuter des actes déterminés n'a pas pour effet, à elle seule, de lui conférer la qualité d'agent contractuel. En revanche, lorsque l'exécution d'actes déterminés multiples répond à un besoin permanent de l'administration, l'agent doit être regardé comme ayant la qualité d'agent non titulaire de l'administration.... ,,2) Agent ayant été régulièrement employé, entre 2004 et 2014, par le centre d'action sociale de la Ville de Paris (CASVP) en vue de remplacer les gardiens titulaires de résidences accueillant des personnes âgées lorsque ces derniers prenaient leur repos hebdomadaire, leurs congés légaux ou des jours de récupération, le fonctionnement de ces résidences exigeant la présence permanente jour et nuit d'une personne chargée d'assurer les fonctions de gardien et les agents titulaires ne suffisant pas à répondre à ce besoin....  ,,En jugeant que les missions exercées par cet agent ne répondaient pas à un besoin permanent de l'administration et qu'en conséquence ce dernier ne pouvait être regardé comme un agent non titulaire, la cour a inexactement qualifié les faits qui lui étaient soumis.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., s'agissant des agents non titulaires de l'Etat, CE, 11 février 2013, Mme Bakhtaoui, n° 347145, T. p. 668.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
