<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037080580</ID>
<ANCIEN_ID>JG_L_2018_06_000000412071</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/08/05/CETATEXT000037080580.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 6ème chambres réunies, 18/06/2018, 412071</TITRE>
<DATE_DEC>2018-06-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>412071</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO ; SCP BARADUC, DUHAMEL, RAMEIX ; SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Alain Seban</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Laurence Marion</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:412071.20180618</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et deux mémoires en réplique, enregistrés au secrétariat du contentieux du Conseil d'Etat le 3 juillet 2017 et les 21 février et 18 mai 2018, la société C8 demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision du Conseil supérieur de l'audiovisuel (CSA) n° 2017-297 du 7 juin 2017 lui infligeant la sanction de la suspension de la diffusion des séquences publicitaires au sein de l'émission " Touche pas à mon poste " et de celles diffusées pendant les quinze minutes qui précèdent et les quinze minutes qui suivent la diffusion de cette émission pendant une durée de deux semaines ;<br/>
<br/>
              2°) de mettre à la charge du CSA une somme de 10 000 euros au titre de l'article L.761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - la Constitution, et notamment son Préambule ;<br/>
<br/>
              - la  convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
<br/>
              - la loi n° 86-1067 du 30 septembre 1986 ;<br/>
<br/>
              - la décision du Conseil constitutionnel n° 88-248 DC du 17 janvier 1989 ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Alain Seban, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Laurence Marion, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat de la société C8, à la SCP Baraduc, Duhamel, Rameix, avocat du Conseil supérieur de l'audiovisuel et à la SCP Rocheteau, Uzan-Sarano, avocat de l'assocation de soutien à la Fondation des femmes.<br/>
<br/>
              Vu la note en délibéré, enregistrée le 30 mai 2018, présentée par le Conseil supérieur de l'audiovisuel ;<br/>
<br/>
<br/>
<br/>1. Considérant, d'une part, qu'aux termes de l'article 3-1 de la loi du 30 septembre 1986 relative à la liberté de communication : " Le Conseil supérieur de l'audiovisuel, autorité publique indépendante, garantit l'exercice de la liberté de communication audiovisuelle par tout procédé de communication électronique, dans les conditions définies par la présente loi. / (...) Il assure le respect des droits des femmes dans le domaine de la communication audiovisuelle. A cette fin, il veille, d'une part, à une juste représentation des femmes et des hommes dans les programmes des services de communication audiovisuelle et, d'autre part, à l'image des femmes qui apparaît dans ces programmes, notamment en luttant contre les stéréotypes, les préjugés sexistes, les images dégradantes, les violences faites aux femmes et les violences commises au sein des couples (...) " ; qu'aux termes du premier alinéa  de l'article 42 de cette loi : " Les éditeurs et distributeurs de services de communication audiovisuelle et les opérateurs de réseaux satellitaires peuvent être mis en demeure de respecter les obligations qui leur sont imposées par les textes législatifs et réglementaires et par les principes définis aux articles 1er et 3-1 " ; qu'aux termes de l'article 42-1 de la même loi : " Si la personne faisant l'objet de la mise en demeure ne se conforme pas à celle-ci, le Conseil supérieur de l'audiovisuel peut prononcer à son encontre, compte tenu de la gravité du manquement, et à la condition que celui-ci repose sur des faits distincts ou couvre une période distincte de ceux ayant déjà fait l'objet d'une mise en demeure, une des sanctions suivantes : / 1° La suspension de l'édition, de la diffusion ou de la distribution du ou des services d'une catégorie de programme, d'une partie du programme, ou d'une ou plusieurs séquences publicitaires pour un mois au plus ; (...) " ; <br/>
<br/>
              2. Considérant, d'autre part, qu'aux termes de l'article 4-2-2 de la convention relative au service de télévision " Direct 8 ", devenu C8, conclue le 10 juin 2003 entre le Conseil supérieur de l'audiovisuel (CSA) et la société Bolloré Médias, aux droits de laquelle est venue la société C8, sur le fondement de l'article 28 de la loi du 30 septembre 1986, le conseil supérieur peut, si l'éditeur ne se conforme pas aux mises en demeure de respecter les obligations prévues par cette convention, " compte tenu de la gravité du manquement, prononcer l'une des sanctions suivantes : (...) 2° la suspension pour un mois au plus de l'édition, de la diffusion ou de la distribution du service, d'une catégorie de programme, d'une partie du programme ou d'une ou plusieurs séquences publicitaires (...) " ; que l'article 4-2-4 de la convention prévoit que les sanctions mentionnées à ses articles 4-2-2 et 4-2-3 sont prononcées dans le respect des garanties fixées par les articles 42 et suivants de la loi du 30 septembre 1986 ;<br/>
<br/>
              3. Considérant que, par une décision adoptée lors de sa séance du 7 juin 2017, le CSA a estimé qu'une séquence diffusée le 7 décembre 2016 par le service de télévision C8 lors de l'émission " Touche pas à mon poste " était constitutive d'un manquement, d'une part, aux obligations résultant de l'article 3-1 de la loi du 30 septembre 1986 en matière d'image des femmes et de lutte contre les stéréotypes et les violences qui leur sont faites et, d'autre part, aux prescriptions de l'article 2-2-1 de la convention du 10 juin 2003, selon lesquelles l'éditeur doit maîtriser son antenne ; que le conseil supérieur a, en conséquence, infligé à la société C8, en sa qualité d'éditeur du service, la sanction de l'interdiction de diffuser des séquences publicitaires, pendant une durée de deux semaines, au sein de l'émission en cause et pendant les quinze minutes précédant et suivant la diffusion de cette émission ; que la société C8 demande l'annulation de cette décision ; <br/>
<br/>
              4. Considérant que l'association de soutien à la Fondation des femmes et autres, qui avaient invité le CSA à se saisir des faits qu'il a sanctionnés par la décision attaquée, ont intérêt au maintien de cette décision ; qu'ainsi leur intervention est recevable ;<br/>
<br/>
              Sur le respect du principe d'impartialité :<br/>
<br/>
              5. Considérant, d'une part, que les propos tenus à la radio par le président du CSA les 8 et 22 novembre 2016, qui présentaient d'ailleurs un caractère très général, ne révèlent aucun parti-pris à l'égard de l'émission " Touche pas à mon poste ", dans laquelle a été diffusée la séquence ayant conduit le CSA à prendre la décision litigieuse, et sont, au demeurant, antérieurs aux faits ayant donné lieu à la décision attaquée ; qu'il en va de même de l'interview, publiée dans un quotidien le 24 novembre 2016, d'un membre du CSA qui, au surplus, n'a pas pris part à la délibération de cette décision ; que les déclarations d'un autre membre du CSA à la radio le 18 juin 2017, qui se réfèrent certes expressément à l'émission " Touche pas à mon poste ", sont sans rapport avec cette séquence et ne sauraient davantage être regardées comme constitutives d'un manquement de ce membre à son devoir d'impartialité ;<br/>
<br/>
              6. Considérant, d'autre part, que ni l'ensemble des propos qui viennent d'être analysés, ni le fait que la société requérante a fait l'objet de plusieurs avertissements, mises en garde, mises en demeure et sanctions, de plus en plus sévères, de la part du CSA concernant l'émission " Touche pas à mon poste ", pas davantage que la position adoptée par le CSA à l'égard de divers autres médias, à l'occasion de faits d'une autre nature que ceux qui ont donné lieu à la sanction litigieuse, ne révèlent, en tout état de cause, que cette autorité aurait préjugé de la suite à donner à la procédure disciplinaire ayant conduit à la décision attaquée avant même la délibération de celle-ci ;<br/>
<br/>
              Sur la mise en demeure préalable :<br/>
<br/>
              7. Considérant, qu'ainsi qu'il a été dit, la décision attaquée sanctionne un manquement aux obligations résultant de l'article 3-1 de la loi du 30 septembre 1986 en matière d'image des femmes et de lutte contre les stéréotypes et les violences qui leur sont faites et aux prescriptions de l'article 2-2-1 de la convention du 10 juin 2003 relatives à la maîtrise de l'antenne ; que, par sa décision n° 2016-872 du 23 novembre 2016, devenue définitive à la suite du rejet par le Conseil d'Etat statuant au contentieux, le 4 décembre 2017, du recours formé à son encontre par la société requérante, le CSA a mis en demeure la société C8, alors dénommée D8, de respecter, à l'avenir, les dispositions de l'article 3-1 de la loi du 30 septembre 1986 à la suite de la diffusion d'une séquence jugée " véhicul[er] des préjugés sexistes et présent[er] une image dégradante des femmes " ; que, par sa décision n° 2015-274 du 1er juillet 2015, le CSA a notamment, contrairement à ce qui est allégué, mis en demeure la société C8, alors dénommée D8, de respecter, à l'avenir, les stipulations de l'article 2-2-1 de la convention précitée ; que ces deux mises en demeure fondent légalement les griefs retenus par la décision attaquée, conformément aux dispositions des articles 42 et 42-1 citées ci-dessus ;<br/>
<br/>
              Sur le respect du principe de légalité des délits et des peines :<br/>
<br/>
              8. Considérant que, se prononçant sur la conformité à la Constitution du texte adopté par le Parlement et qui allait devenir la loi du 17 janvier 1989 modifiant la loi du 30 septembre 1986 relative à la liberté de communication, le Conseil constitutionnel, par sa décision n° 88-248 DC du 17 janvier 1989, a estimé que les pouvoirs de sanction conférés par le législateur au CSA ne sont susceptibles de s'exercer qu'après mise en demeure des titulaires d'autorisation pour l'exploitation de services de communication audiovisuelle de respecter les obligations qui leur sont imposées par les textes législatifs et réglementaires, et faute pour les intéressés de respecter ces obligations ou de se conformer aux mises en demeure qui leur ont été adressées ; que c'est sous réserve de cette interprétation que les articles en cause ont été déclarés conformes à l'article 8 de la Déclaration des droits de l'homme et du citoyen du 26 août 1789 et à l'article 34 de la Constitution ; que cette réserve d'interprétation assure notamment le respect du principe de légalité des délits et des peines, consacré par l'article 8 de la Déclaration du 26 août 1789 et qui s'applique notamment devant les organismes administratifs dotés d'un pouvoir de sanction ; que le CSA ne peut, en effet, prononcer une sanction contre le titulaire de l'autorisation qu'en cas de réitération d'un comportement ayant fait auparavant l'objet d'une mise en demeure par laquelle il a été au besoin éclairé sur ses obligations ; que la décision attaquée se fonde sur une mise en demeure qui définit avec précision le comportement que le CSA a regardé comme contraire aux dispositions précitées de l'article 3-1 de la loi du 30 septembre 1986 ; que, dans ces conditions, le moyen tiré de ce qu'eu égard à la généralité des termes de cet article le CSA aurait méconnu le principe de légalité des délits et des peines ne saurait être accueilli ;<br/>
<br/>
              Sur la qualification juridique des faits :<br/>
<br/>
              9. Considérant qu'il résulte de l'instruction que, le 7 décembre 2016, lors de l'émission " Touche pas à mon poste ", a été diffusée une séquence, censée montrer les coulisses de l'émission, au cours de laquelle l'animateur a proposé à une chroniqueuse un " jeu " consistant à lui faire toucher, pendant qu'elle gardait les yeux fermés, diverses parties de son corps qu'elle devait ensuite identifier ; qu'après avoir fait toucher à l'intéressée sa poitrine et son bras, l'animateur a posé sa main sur son entrejambe ; que celle-ci a réagi en se récriant puis en relevant le caractère habituel de ce type de geste ; que la mise en scène d'un tel comportement, procédant par surprise, sans consentement préalable de l'intéressée et portant, de surcroît, sur la personne d'une chroniqueuse placée en situation de subordination vis-à-vis de l'animateur et producteur, ne peut que banaliser des comportements inacceptables et d'ailleurs susceptibles de faire l'objet, dans certains cas, d'une incrimination pénale ; qu'elle place la personne concernée dans une situation dégradante et, présentée comme habituelle, tend à donner de la femme une image stéréotypée la réduisant à un statut d'objet sexuel ; que le CSA a pu légalement estimer que ces faits, constituant, d'une part, une méconnaissance par la chaîne des obligations qui lui incombent en application des dispositions précitées de l'article 3-1 de la loi du 30 septembre 1986, rappelées dans la mise en demeure que lui a adressée le CSA le 23 novembre 2016, et révélant, d'autre part, un défaut de maîtrise de l'antenne, étaient, alors même qu'ils s'étaient produits dans le cadre d'une émission humoristique, de nature à justifier le prononcé d'une sanction sur le fondement de l'article 42-1 précité ; qu'eu égard tant aux pouvoirs dévolus au CSA, auquel le législateur a confié la mission de veiller à l'image donnée des femmes dans les programmes, qu'à la nature des faits décrits ci-dessus au regard des obligations qui s'imposent à la société requérante, la décision de sanctionner cette dernière ne porte pas une atteinte disproportionnée à la liberté d'expression, protégée tant par l'article 11 de la Déclaration des droits de l'homme et du citoyen du 26 août 1789 que par l'article 10 paragraphe 1 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Sur la nature et le quantum de la sanction prononcée :<br/>
<br/>
              10. Considérant, d'une part, que, contrairement à ce qui est soutenu, les dispositions citées ci-dessus du 1° de l'article 42-1 de la loi du 30 septembre 1986 n'ont ni pour objet ni pour effet de limiter la possibilité pour le CSA d'infliger à un opérateur la sanction de la suspension des programmes publicitaires pendant une durée et dans des conditions déterminées aux cas de manquement par cet opérateur à ses obligations en matière de publicité ;<br/>
<br/>
              11. Considérant, d'autre part, qu'il ne résulte pas de l'instruction, compte tenu  notamment de la circonstance que les faits incriminés se sont produits seulement une quinzaine de jours après la mise en demeure adressée par le CSA concernant des faits similaires observés dans la même émission, et eu égard à la nature de ces faits, que la sanction prononcée consistant en la suspension de la diffusion des séquences publicitaires au sein de l'émission " Touche pas à mon poste " et de celles diffusées pendant les quinze minutes qui précèdent et les quinze minutes qui suivent la diffusion de cette émission pendant une durée de deux semaines, doive être regardée comme excessive eu égard aux manquements commis ;<br/>
<br/>
              12. Considérant qu'il résulte de tout ce qui précède que la société C8 n'est pas fondée à demander l'annulation de la décision qu'elle attaque ;<br/>
<br/>
              13. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise, à ce titre, à la charge du CSA qui n'est pas, dans la présente instance, la partie perdante ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société C8 la somme de 3 000 euros que le CSA demande au titre de ces dispositions.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'intervention de l'association de soutien à la Fondation des femmes et autres est admise.<br/>
Article 2 : La requête de la société C8 est rejetée.<br/>
Article 3 : La société C8 versera la somme de 3 000 euros au Conseil supérieur de l'audiovisuel en l'application de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la société C8, à l'association de soutien à la Fondation des femmes et autres, première intervenante dénommée, et au Conseil supérieur de l'audiovisuel.<br/>
Copie en sera adressée à la ministre de la culture.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">56-01 RADIO ET TÉLÉVISION. CONSEIL SUPÉRIEUR DE L'AUDIOVISUEL. - 1) FACULTÉ POUR LE CSA DE PRONONCER LA SUSPENSION DE PROGRAMMES PUBLICITAIRES (1° DE L'ARTICLE 42-1 DE LA LOI N° 86-1067 DU 30 SEPTEMBRE 1986) - PRONONCÉ D'UNE TELLE SANCTION LIMITÉ AUX SEULS MANQUEMENTS AUX OBLIGATIONS DE PUBLICITÉ - ABSENCE - 2) ESPÈCE - SANCTION PRONONCÉE CONTRE LA SOCIÉTÉ C8 EN RAISON D'UNE SÉQUENCE DE L'ÉMISSION TOUCHE PAS À MON POSTE DU 7 DÉCEMBRE 2016 - MÉCONNAISSANCE DE L'ARTICLE 3 DE LA LOI N° 87-1067 ET DÉFAUT DE MAÎTRISE DE L'ANTENNE - EXISTENCE - ATTEINTE DISPROPORTIONNÉE À LA LIBERTÉ D'EXPRESSION - ABSENCE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">59-02-02-03 RÉPRESSION. DOMAINE DE LA RÉPRESSION ADMINISTRATIVE RÉGIME DE LA SANCTION ADMINISTRATIVE. BIEN-FONDÉ. - 1) FACULTÉ POUR LE CSA DE PRONONCER LA SUSPENSION DE PROGRAMMES PUBLICITAIRES (1° DE L'ARTICLE 42-1 DE LA LOI N° 86-1067 DU 30 SEPTEMBRE 1986) - PRONONCÉ D'UNE TELLE SANCTION LIMITÉ AUX SEULS MANQUEMENTS AUX OBLIGATIONS DE PUBLICITÉ - ABSENCE - 2) ESPÈCE - SANCTION PRONONCÉE CONTRE LA SOCIÉTÉ C8 EN RAISON D'UNE SÉQUENCE DE L'ÉMISSION TOUCHE PAS À MON POSTE DU 7 DÉCEMBRE 2016 - MÉCONNAISSANCE DE L'ARTICLE 3 DE LA LOI N° 87-1067 ET DÉFAUT DE MAÎTRISE DE L'ANTENNE - EXISTENCE - ATTEINTE DISPROPORTIONNÉE À LA LIBERTÉ D'EXPRESSION - ABSENCE [RJ1].
</SCT>
<ANA ID="9A"> 56-01 1) Le 1° de l'article 42-1 de la loi n° 86-1067 du 30 septembre 1986 n'a ni pour objet ni pour effet de limiter la possibilité pour le Conseil Supérieur de l'Audiovisuel (CSA) d'infliger à un opérateur la sanction de la suspension des programmes publicitaires pendant une durée et dans des conditions déterminées aux cas de manquement par cet opérateur à ses obligations en matière de publicité.,,,2) Diffusion le 7 décembre 2016 lors de l'émission Touche pas à mon poste d'une séquence, censée montrer les coulisses de l'émission, au cours de laquelle l'animateur a proposé à une chroniqueuse un jeu consistant à lui faire toucher, pendant qu'elle gardait les yeux fermés, diverses parties de son corps qu'elle devait ensuite identifier. Après avoir fait toucher à l'intéressée sa poitrine et son bras, l'animateur a posé sa main sur son entrejambe. Celle-ci a réagi en se récriant puis en relevant le caractère habituel de ce type de geste.... ...La mise en scène d'un tel comportement, procédant par surprise, sans consentement préalable de l'intéressée et portant, de surcroît, sur la personne d'une chroniqueuse placée en situation de subordination vis-à-vis de l'animateur et producteur, ne peut que banaliser des comportements inacceptables et d'ailleurs susceptibles de faire l'objet, dans certains cas, d'une incrimination pénale. Elle place la personne concernée dans une situation dégradante et, présentée comme habituelle, tend à donner de la femme une image stéréotypée la réduisant à un statut d'objet sexuel. Le Conseil supérieur de l'audiovisuel (CSA) a pu légalement estimer que ces faits, constituant, d'une part, une méconnaissance par la chaîne des obligations qui lui incombent en application de l'article 3-1 de la loi n° 86-1067 du 30 septembre 1986, rappelées dans la mise en demeure que lui a adressée le CSA le 23 novembre 2016, et révélant, d'autre part, un défaut de maîtrise de l'antenne, étaient, alors même qu'ils s'étaient produits dans le cadre d'une émission humoristique, de nature à justifier le prononcé d'une sanction sur le fondement de l'article 42-1 de cette même loi. Eu égard tant aux pouvoirs dévolus au CSA, auquel le législateur a confié le mission de veiller à l'image donnée des femmes dans les programmes, qu'à la nature des faits décrits ci-dessus au regard des obligations qui s'imposent à la société requérante, la décision de sanctionner cette dernière ne porte pas une atteinte disproportionnée à la liberté d'expression, protégée tant par l'article 11 de la Déclaration des droits de l'homme et du citoyen (DDHC) du 26 août 1789 que par l'article 10 paragraphe 1 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales (Conv. EDH).... ,,Compte tenu notamment de la circonstance que les faits incriminés se sont produits seulement une quinzaine de jours après la mise en demeure adressée par le CSA concernant des faits similaires observés dans la même émission, et eu égard à la nature de ces faits, la sanction prononcée consistant en la suspension de la diffusion des séquences publicitaires au sein de l'émission Touche pas à mon poste et de celles diffusées pendant les quinze minutes qui précèdent et les quinze minutes qui suivent la diffusion de cette émission pendant une durée de deux semaines, ne doit pas être regardée comme excessive eu égard aux manquements commis.</ANA>
<ANA ID="9B"> 59-02-02-03 1) Le 1° de l'article 42-1 de la loi n° 86-1067 du 30 septembre 1986 n'a ni pour objet ni pour effet de limiter la possibilité pour le Conseil Supérieur de l'Audiovisuel (CSA) d'infliger à un opérateur la sanction de la suspension des programmes publicitaires pendant une durée et dans des conditions déterminées aux cas de manquement par cet opérateur à ses obligations en matière de publicité.,,,2) Diffusion le 7 décembre 2016 lors de l'émission Touche pas à mon poste d'une séquence, censée montrer les coulisses de l'émission, au cours de laquelle l'animateur a proposé à une chroniqueuse un jeu consistant à lui faire toucher, pendant qu'elle gardait les yeux fermés, diverses parties de son corps qu'elle devait ensuite identifier. Après avoir fait toucher à l'intéressée sa poitrine et son bras, l'animateur a posé sa main sur son entrejambe. Celle-ci a réagi en se récriant puis en relevant le caractère habituel de ce type de geste.... ...La mise en scène d'un tel comportement, procédant par surprise, sans consentement préalable de l'intéressée et portant, de surcroît, sur la personne d'une chroniqueuse placée en situation de subordination vis-à-vis de l'animateur et producteur, ne peut que banaliser des comportements inacceptables et d'ailleurs susceptibles de faire l'objet, dans certains cas, d'une incrimination pénale. Elle place la personne concernée dans une situation dégradante et, présentée comme habituelle, tend à donner de la femme une image stéréotypée la réduisant à un statut d'objet sexuel. Le Conseil supérieur de l'audiovisuel (CSA) a pu légalement estimer que ces faits, constituant, d'une part, une méconnaissance par la chaîne des obligations qui lui incombent en application de l'article 3-1 de la loi n° 86-1067 du 30 septembre 1986, rappelées dans la mise en demeure que lui a adressée le CSA le 23 novembre 2016, et révélant, d'autre part, un défaut de maîtrise de l'antenne, étaient, alors même qu'ils s'étaient produits dans le cadre d'une émission humoristique, de nature à justifier le prononcé d'une sanction sur le fondement de l'article 42-1 de cette même loi. Eu égard tant aux pouvoirs dévolus au CSA, auquel le législateur a confié le mission de veiller à l'image donnée des femmes dans les programmes, qu'à la nature des faits décrits ci-dessus au regard des obligations qui s'imposent à la société requérante, la décision de sanctionner cette dernière ne porte pas une atteinte disproportionnée à la liberté d'expression, protégée tant par l'article 11 de la Déclaration des droits de l'homme et du citoyen (DDHC) du 26 août 1789 que par l'article 10 paragraphe 1 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales (Conv. EDH).... ,,Compte tenu notamment de la circonstance que les faits incriminés se sont produits seulement une quinzaine de jours après la mise en demeure adressée par le CSA concernant des faits similaires observés dans la même émission, et eu égard à la nature de ces faits, la sanction prononcée consistant en la suspension de la diffusion des séquences publicitaires au sein de l'émission Touche pas à mon poste et de celles diffusées pendant les quinze minutes qui précèdent et les quinze minutes qui suivent la diffusion de cette émission pendant une durée de deux semaines, ne doit pas être regardée comme excessive eu égard aux manquements commis.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, décision du même jour, Société C8, n° 414532, à mentionner aux Tables. Cf. sol. contr. CE, décision du même jour, Société C8, n° 412074, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
