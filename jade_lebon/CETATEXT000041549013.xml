<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041549013</ID>
<ANCIEN_ID>JG_L_2020_02_000000426225</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/54/90/CETATEXT000041549013.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 4ème chambres réunies, 05/02/2020, 426225</TITRE>
<DATE_DEC>2020-02-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>426225</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS</AVOCATS>
<RAPPORTEUR>M. Jean-Luc Nevache</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Villette</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:426225.20200205</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société à responsabilité limitée Les Ambulances Hurié, devenue Les Taxis Hurié, a demandé au tribunal administratif de Dijon d'annuler pour excès de pouvoir l'arrêté du 30 décembre 2014 par lequel le directeur général de l'agence régionale de santé de Bourgogne, d'une part, a définitivement retiré son agrément de transport sanitaire à compter du 28 décembre 2014 et abrogé l'arrêté du 21 septembre 2006 par lequel le préfet de l'Yonne lui avait délivré cet agrément et, d'autre part, a abrogé l'autorisation de mise en service du véhicule sanitaire léger immatriculé BK-919-ZQ. Par un jugement n° 1500601 du 1er février 2016, le tribunal administratif de Dijon a rejeté cette demande.<br/>
<br/>
              Par un arrêt n° 16LY01233 du 11 octobre 2018, la cour administrative d'appel de Lyon a rejeté l'appel formé par la société Les Taxis Hurié contre ce jugement.  <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 12 décembre 2018 et 12 mars 2019 au secrétariat du contentieux du Conseil d'Etat, la société Les Taxis Hurié demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code des relations entre le public et l'administration ;<br/>
              - le code de la santé publique ;<br/>
              - la loi n° 79-587 du 11 juillet 1979 ;<br/>
              - la loi n° 2000-321 du 12 avril 2000 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Luc Nevache, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Vincent Villette, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de la société Les Taxis Hurie ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la société Les Ambulances Hurié, devenue Les Taxis Hurié, agréée pour le transport sanitaire par arrêté préfectoral du 21 septembre 2006 et bénéficiaire d'autorisations de mise en service pour trois ambulances et cinq véhicules sanitaires légers, a conclu, le 8 novembre 2014, avec la société Ambulances Urgences Santé Assistance, un compromis de cession de son activité de transport sanitaire. Le 2 décembre 2014, le directeur général de l'agence régionale de santé de Bourgogne a accordé à la société Ambulances Urgences Santé Assistance le transfert des autorisations de mise en service de sept des huit véhicules et refusé le transfert de l'autorisation de mise en service de l'un des véhicules sanitaires légers. Puis, le 30 décembre 2014, cette même autorité, estimant que la société Les Ambulances Hurié n'en remplissait plus les conditions, lui a définitivement retiré son agrément à compter du 28 décembre 2014, abrogeant ainsi l'arrêté du 21 septembre 2006, et a abrogé l'autorisation de mise en service du véhicule sanitaire léger qui n'avait pas été transférée. Par un arrêt du 11 octobre 2018, contre lequel la société Les Taxis Hurié se pourvoit en cassation, la cour administrative d'appel de Lyon a rejeté l'appel de cette société contre le jugement par lequel le tribunal administratif de Dijon a rejeté sa demande tendant à l'annulation pour excès de pouvoir de l'arrêté du 30 décembre 2014. <br/>
<br/>
              2. D'une part, aux termes de l'article L. 6312-2 du code de la santé publique : " Toute personne effectuant un transport sanitaire doit avoir été préalablement agréée par le directeur général de l'agence régionale de santé. (...) ", l'article L. 6312-5 de ce code prévoyant que sont déterminées par décret en Conseil d'Etat " les conditions " de cet agrément ainsi que " les modalités " de sa délivrance et de son retrait par l'agence régionale de santé. Le paragraphe " conditions de délivrance de l'agrément " de la partie réglementaire de ce code regroupe les articles R. 6312-6 à R. 6312-10, l'article R. 6312-6 disposant notamment que : " L'agrément est délivré aux personnes physiques ou morales qui disposent : / 1° Des personnels nécessaires pour garantir la présence à bord de tout véhicule en service d'un équipage conforme aux normes définies à l'article R. 6312-10  ; / 2° De véhicules, appartenant aux catégories A, B, C ou D mentionnées à l'article R. 6312-8, véhicules dont elles ont un usage exclusif ". En vertu de l'article R. 6312-11 du même code, l'agrément est délivré dans tous les cas au titre de l'aide médicale urgente et peut être délivré au surplus au titre des transports effectués sur prescription médicale. L'article R. 6312-13 prévoit que l'agrément ne peut être délivré à ces deux titres à la fois qu'aux personnes ou établissements qui, outre qu'elles satisfont à des conditions relatives à la qualification de leur personnel et à leurs installations matérielles, disposent " d'au moins deux véhicules des catégories A, C ou D mentionnées à l'article R. 6312-8, dont au moins un véhicule des catégories A ou C ". Selon l'article R. 6312-8 de ce code : " Les véhicules spécialement adaptés au transport sanitaire ressortissent aux catégories suivantes : / 1° Véhicules spécialement aménagés : / a) Catégorie A : ambulance de secours et de soins d'urgence "ASSU" ; / b) Catégorie B : voiture de secours aux asphyxiés et blessés "VSAB" ; / c) Catégorie C : ambulance ; / 2° Autres véhicules affectés au transport sanitaire terrestre : / - catégorie D : véhicule sanitaire léger (...) ".  <br/>
<br/>
              3. D'autre part, l'article 1er de la loi du 11 juillet 1979 relative à la motivation des actes administratifs et à l'amélioration des relations entre l'administration et le public, désormais codifié à l'article L. 211-2 du code des relations entre le public et l'administration, prévoit que : " Les personnes physiques ou morales ont le droit d'être informées sans délai des motifs des décisions administratives individuelles défavorables qui les concernent. / A cet effet, doivent être motivées les décisions qui : / (...) retirent ou abrogent une décision créatrice de droits (...) ". L'article 24 de la loi du 12 avril 2000 relative aux droits des citoyens dans leurs relations avec les administrations, désormais codifié à l'article L. 121-1 du même code, dispose que : " (...) les décisions individuelles qui doivent être motivées en application des articles 1er et 2 de la loi n° 79-587 du 11 juillet 1979 relative à la motivation des actes administratifs et à l'amélioration des relations entre l'administration et le public n'interviennent qu'après que la personne intéressée a été mise à même de présenter des observations écrites et, le cas échéant, sur sa demande, des observations orales ". <br/>
<br/>
              4.	Il résulte des dispositions mentionnées au point 2 que tant la délivrance que le maintien de l'agrément de transport sanitaire sont subordonnés aux conditions déterminées par voie réglementaire, lesquelles imposent de disposer de moyens en personnel et en matériel qu'elles définissent, permettant d'assurer les obligations, notamment de participation au service de garde, auxquelles sont soumis les titulaires d'un tel agrément. Lorsque, par suite de circonstances postérieures à la délivrance de l'agrément, lequel a le caractère d'une décision individuelle créatrice de droits, son titulaire cesse d'en remplir les conditions, il incombe au directeur général de l'agence régionale de santé de l'abroger par une décision qui, en application des dispositions mentionnées au point 3, doit être motivée et prise après que le titulaire de l'agrément a été mis à même de présenter ses observations. <br/>
<br/>
              5.	La société requérante n'est ainsi pas fondée à soutenir que la cour administrative d'appel de Lyon a commis une erreur de droit en jugeant que le directeur général de l'agence régionale de santé était dans l'obligation de mettre fin à son agrément dès lors qu'elle ne disposait plus du nombre de véhicules auquel cet agrément est subordonné. Toutefois, l'appréciation selon laquelle le titulaire d'un agrément n'en remplit plus les conditions ne résultant pas d'un simple constat, la société requérante est en revanche fondée à soutenir que la cour a commis une erreur de droit en jugeant que l'obligation de l'administration était constitutive d'une situation de compétence liée et en en déduisant que devaient être écartés comme inopérants les autres moyens soulevés. <br/>
<br/>
              6.	Par suite, la société requérante est fondée, sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, à demander l'annulation de l'arrêt qu'elle attaque.<br/>
<br/>
              7.	Il y a lieu, dans les circonstances de l'espèce, en application de l'article L. 761-1 du code de justice administrative, de mettre à la charge de l'Etat une somme de 3 000 euros au titre des frais exposés par la société Les Taxis Hurié et non compris dans les dépens.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Lyon du 11 octobre 2018 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Lyon.<br/>
Article 3 : L'Etat versera une somme de 3 000 euros à la société Les Taxis Hurié au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la société à responsabilité limitée Les Taxis Hurié et à la ministre des solidarités et de la santé.<br/>
Copie en sera adressée à la société Ambulances Urgences Santé Assistance.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-01-06-02-01 ACTES LÉGISLATIFS ET ADMINISTRATIFS. DIFFÉRENTES CATÉGORIES D'ACTES. ACTES ADMINISTRATIFS - CLASSIFICATION. ACTES INDIVIDUELS OU COLLECTIFS. ACTES CRÉATEURS DE DROITS. - ACTES CRÉATEURS DE DROITS DONT LE MAINTIEN EST SOUMIS AU RESPECT D'UNE CONDITION - INCLUSION - AGRÉMENT DE TRANSPORT SANITAIRE (ART. L. 6312-2 DU CSP).
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">01-05-01-03 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - MOTIFS. POUVOIRS ET OBLIGATIONS DE L'ADMINISTRATION. COMPÉTENCE LIÉE. - ENTREPRISE DE TRANSPORT SANITAIRE - AGRÉMENT PRÉVU À L'ARTICLE L. 6312-2 DU CSP - TITULAIRE NE REMPLISSANT PLUS LES CONDITIONS DE CET AGRÉMENT - 1) OBLIGATION DU DIRECTEUR DE L'ARS D'ABROGER CET AGRÉMENT - EXISTENCE - 2) COMPÉTENCE LIÉE [RJ1] DU DIRECTEUR DE L'ARS - ABSENCE [RJ2].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">14-02-02-02 COMMERCE, INDUSTRIE, INTERVENTION ÉCONOMIQUE DE LA PUISSANCE PUBLIQUE. RÉGLEMENTATION DES ACTIVITÉS ÉCONOMIQUES. MODALITÉS DE LA RÉGLEMENTATION. AGRÉMENT. - ENTREPRISE DE TRANSPORT SANITAIRE - AGRÉMENT PRÉVU À L'ARTICLE L. 6312-2 DU CSP - TITULAIRE NE REMPLISSANT PLUS LES CONDITIONS DE CET AGRÉMENT - 1) OBLIGATION DU DIRECTEUR DE L'ARS D'ABROGER CET AGRÉMENT - EXISTENCE - 2) COMPÉTENCE LIÉE [RJ1] DU DIRECTEUR DE L'ARS - ABSENCE [RJ2].
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">61-01-02 SANTÉ PUBLIQUE. PROTECTION GÉNÉRALE DE LA SANTÉ PUBLIQUE. TRANSPORTS SANITAIRES. - ENTREPRISE DE TRANSPORT SANITAIRE - AGRÉMENT PRÉVU À L'ARTICLE L. 6312-2 DU CSP - TITULAIRE NE REMPLISSANT PLUS LES CONDITIONS DE CET AGRÉMENT - 1) OBLIGATION DU DIRECTEUR DE L'ARS D'ABROGER CET AGRÉMENT - EXISTENCE - 2) COMPÉTENCE LIÉE [RJ1] DU DIRECTEUR DE L'ARS - ABSENCE [RJ2].
</SCT>
<ANA ID="9A"> 01-01-06-02-01 Il résulte des articles L. 6312-2, L. 6312-5, R. 6312-6, R. 6312-11, R. 6312-13 et R. 6312-8 du code de la santé publique (CSP) que tant la délivrance que le maintien de l'agrément de transport sanitaire sont subordonnés aux conditions déterminées par voie réglementaire, lesquelles imposent de disposer de moyens en personnel et en matériel qu'elles définissent, permettant d'assurer les obligations, notamment de participation au service de garde, auxquelles sont soumis les titulaires d'un tel agrément.... ,,Lorsque, par suite de circonstances postérieures à la délivrance de l'agrément, lequel a le caractère d'une décision individuelle créatrice de droits, son titulaire cesse d'en remplir les conditions, il incombe au directeur général (DG) de l'agence régionale de santé (ARS) de l'abroger par une décision qui, en application des articles L. 211-2 et L. 121-1 du code des relations entre le public et l'administration (CRPA), doit être motivée et prise après que le titulaire de l'agrément a été mis à même de présenter ses observations.</ANA>
<ANA ID="9B"> 01-05-01-03 Il résulte des articles L. 6312-2, L. 6312-5, R. 6312-6, R. 6312-11, R. 6312-13 et R. 6312-8 du code de la santé publique (CSP) que tant la délivrance que le maintien de l'agrément de transport sanitaire sont subordonnés aux conditions déterminées par voie réglementaire, lesquelles imposent de disposer de moyens en personnel et en matériel qu'elles définissent, permettant d'assurer les obligations, notamment de participation au service de garde, auxquelles sont soumis les titulaires d'un tel agrément.... ,,1) Lorsque, par suite de circonstances postérieures à la délivrance de l'agrément, lequel a le caractère d'une décision individuelle créatrice de droits, son titulaire cesse d'en remplir les conditions, il incombe au directeur général (DG) de l'agence régionale de santé (ARS) de l'abroger par une décision qui, en application des articles L. 211-2 et L. 121-1 du code des relations entre le public et l'administration (CRPA), doit être motivée et prise après que le titulaire de l'agrément a été mis à même de présenter ses observations.... ,,La société requérante n'est ainsi pas fondée à soutenir que la cour administrative d'appel a commis une erreur de droit en jugeant que le DG de l'ARS était dans l'obligation de mettre fin à son agrément dès lors qu'elle ne disposait plus du nombre de véhicules auquel cet agrément est subordonné.... ,,2) Toutefois, l'appréciation selon laquelle le titulaire d'un agrément n'en remplit plus les conditions ne résultant pas d'un simple constat, la société requérante est en revanche fondée à soutenir que la cour a commis une erreur de droit en jugeant que l'obligation de l'administration était constitutive d'une situation de compétence liée et en en déduisant que devaient être écartés comme inopérants les autres moyens soulevés.</ANA>
<ANA ID="9C"> 14-02-02-02 Il résulte des articles L. 6312-2, L. 6312-5, R. 6312-6, R. 6312-11, R. 6312-13 et R. 6312-8 du code de la santé publique (CSP) que tant la délivrance que le maintien de l'agrément de transport sanitaire sont subordonnés aux conditions déterminées par voie réglementaire, lesquelles imposent de disposer de moyens en personnel et en matériel qu'elles définissent, permettant d'assurer les obligations, notamment de participation au service de garde, auxquelles sont soumis les titulaires d'un tel agrément.... ,,1) Lorsque, par suite de circonstances postérieures à la délivrance de l'agrément, lequel a le caractère d'une décision individuelle créatrice de droits, son titulaire cesse d'en remplir les conditions, il incombe au directeur général (DG) de l'agence régionale de santé (ARS) de l'abroger par une décision qui, en application des articles L. 211-2 et L. 121-1 du code des relations entre le public et l'administration (CRPA), doit être motivée et prise après que le titulaire de l'agrément a été mis à même de présenter ses observations.... ,,La société requérante n'est ainsi pas fondée à soutenir que la cour administrative d'appel a commis une erreur de droit en jugeant que le DG de l'ARS était dans l'obligation de mettre fin à son agrément dès lors qu'elle ne disposait plus du nombre de véhicules auquel cet agrément est subordonné.... ,,2) Toutefois, l'appréciation selon laquelle le titulaire d'un agrément n'en remplit plus les conditions ne résultant pas d'un simple constat, la société requérante est en revanche fondée à soutenir que la cour a commis une erreur de droit en jugeant que l'obligation de l'administration était constitutive d'une situation de compétence liée et en en déduisant que devaient être écartés comme inopérants les autres moyens soulevés.</ANA>
<ANA ID="9D"> 61-01-02 Il résulte des articles L. 6312-2, L. 6312-5, R. 6312-6, R. 6312-11, R. 6312-13 et R. 6312-8 du code de la santé publique (CSP) que tant la délivrance que le maintien de l'agrément de transport sanitaire sont subordonnés aux conditions déterminées par voie réglementaire, lesquelles imposent de disposer de moyens en personnel et en matériel qu'elles définissent, permettant d'assurer les obligations, notamment de participation au service de garde, auxquelles sont soumis les titulaires d'un tel agrément.... ,,1) Lorsque, par suite de circonstances postérieures à la délivrance de l'agrément, lequel a le caractère d'une décision individuelle créatrice de droits, son titulaire cesse d'en remplir les conditions, il incombe au directeur général (DG) de l'agence régionale de santé (ARS) de l'abroger par une décision qui, en application des articles L. 211-2 et L. 121-1 du code des relations entre le public et l'administration (CRPA), doit être motivée et prise après que le titulaire de l'agrément a été mis à même de présenter ses observations.... ,,La société requérante n'est ainsi pas fondée à soutenir que la cour administrative d'appel a commis une erreur de droit en jugeant que le DG de l'ARS était dans l'obligation de mettre fin à son agrément dès lors qu'elle ne disposait plus du nombre de véhicules auquel cet agrément est subordonné.... ,,2) Toutefois, l'appréciation selon laquelle le titulaire d'un agrément n'en remplit plus les conditions ne résultant pas d'un simple constat, la société requérante est en revanche fondée à soutenir que la cour a commis une erreur de droit en jugeant que l'obligation de l'administration était constitutive d'une situation de compétence liée et en en déduisant que devaient être écartés comme inopérants les autres moyens soulevés.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Section, 3 février 1999, M.,, n°s 149722 152848, p. 6.,,[RJ2] Ab. jur. CE, 30 avril 1997, Consorts,, n° 141178, T. pp. 659-721-1078.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
