<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039442408</ID>
<ANCIEN_ID>JG_L_2019_12_000000421715</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/44/24/CETATEXT000039442408.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 02/12/2019, 421715</TITRE>
<DATE_DEC>2019-12-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>421715</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUVENIN, COUDRAY, GREVY ; SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>M. Christian Fournier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Gabrielle Merloz</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:421715.20191202</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Mme B... A... a demandé au tribunal administratif de Toulon, d'une part, d'annuler son recrutement par contrat à durée indéterminée du 6 décembre 2012 conclu avec le centre communal d'action sociale (CCAS) de la commune d'Hyères, ensemble la décision du 26 mars 2013 du président du CCAS rejetant son recours gracieux et, d'autre part, d'enjoindre au CCAS de lui proposer un contrat de travail prenant effet au 1er janvier 2012 et reprenant les clauses substantielles de son contrat avec l'association " Comité de Vacances et de Loisirs ". Par un jugement n° 1301355 du 13 mai 2015, ce tribunal a rejeté cette demande. Par un arrêt n° 15MA02802 du 24 avril 2018, la cour administrative d'appel de Marseille, sur appel de Mme A..., a, d'une part, prononcé un non-lieu à statuer sur les conclusions tendant à ordonner au CCAS de la commune d'Hyères de donner effet au contrat en cause à compter du 1er janvier 2012 et, d'autre part, rejeté le surplus de sa requête d'appel.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés au secrétariat de la section du contentieux les 25 juin et 25 septembre 2018 et le 26 juin 2019, Mme A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt en tant qu'il rejette le surplus des conclusions de sa requête d'appel ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses conclusions d'appel ;<br/>
<br/>
              3°) de mettre à la charge du CCAS de la commune d'Hyères la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code du travail ;<br/>
              - la loi n° 84-53 du 26 janvier 1984 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Christian Fournier, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Marie-Gabrielle Merloz, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Thouvenin, Coudray, Grevy, avocat de Mme A... et à la SCP Lyon-Caen, Thiriez, avocat du centre communal d'action sociale (CCAS) de la commune d'Hyères ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond qu'à la suite de la reprise en régie, à compter du 1er janvier 2012, des activités assurées par l'association " Comité de Vacances et de Loisirs " par le centre communal d'action sociale (CCAS) de la commune d'Hyères, Mme A... a été recrutée par ce dernier sur un contrat à durée indéterminée en date du 6 décembre 2012 pour assurer les fonctions d'animation, à compter du 1er décembre 2012, au grade d'adjoint d'animation de 2ème classe, pour une rémunération mensuelle calculée sur la base du 8ème échelon de ce grade, soit à l'indice majoré 319. Par un recours gracieux reçu le 31 janvier 2013, Mme A... a sollicité la modification des articles 1er et 4 de son contrat de travail. Le président du CCAS ayant rejeté sa demande, Mme A... a demandé au tribunal administratif de Toulon l'annulation de cette décision et des clauses du contrat qu'elle contestait. Par un jugement du 13 mai 2015, le tribunal administratif a rejeté sa demande. Mme A... se pourvoit en cassation contre l'arrêt de la cour administrative d'appel de Marseille du 24 avril 2018 en tant que, après avoir prononcé un non-lieu à statuer sur ses conclusions tendant à ordonner au CCAS de donner effet au contrat en cause à compter du 1er janvier 2012, il a rejeté le surplus des conclusions de l'appel qu'elle a formé contre ce jugement.<br/>
<br/>
              2. Aux termes de l'article L. 1224-3 du code du travail, applicable à la date de la décision attaquée : " Lorsque l'activité d'une entité économique employant des salariés de droit privé est, par transfert de cette entité, reprise par une personne publique dans le cadre d'un service public administratif, il appartient à cette personne publique de proposer à ces salariés un contrat de droit public, à durée déterminée ou indéterminée selon la nature du contrat dont ils sont titulaires. Sauf disposition légale ou conditions générales de rémunération et d'emploi des agents non titulaires de la personne publique contraires, le contrat qu'elle propose reprend les clauses substantielles du contrat dont les salariés sont titulaires, en particulier celles qui concernent la rémunération. (...) ".<br/>
<br/>
              3. Il résulte de ces dispositions qu'en écartant, en l'absence même de toute disposition législative ou réglementaire contraire, la reprise des clauses du contrat dont le salarié transféré était titulaire relatives à la rémunération, lorsque celles-ci ne sont pas conformes aux " conditions générales de rémunération et d'emploi des agents non titulaires de la personne publique ", le législateur n'a pas entendu autoriser la personne publique concernée à proposer aux intéressés une rémunération inférieure à celle dont ils bénéficiaient auparavant au seul motif que celle-ci dépasserait, à niveaux de responsabilité et de qualification équivalents, celle des agents en fonctions dans l'organisme d'accueil à la date du transfert. En revanche, ces dispositions font obstacle à ce que soient reprises, dans le contrat de droit public proposé au salarié transféré, des clauses impliquant une rémunération dont le niveau, même corrigé de l'ancienneté, excèderait manifestement celui que prévoient les règles générales fixées, le cas échéant, pour la rémunération de ses agents non titulaires. En l'absence de telles règles au sein d'une collectivité territoriale, la reprise de la rémunération antérieure n'est en tout état de cause légalement possible que si elle peut être regardée comme n'excédant pas manifestement la rémunération que, dans le droit commun, il appartiendrait à l'autorité administrative compétente de fixer, sous le contrôle du juge, en tenant compte, notamment, des fonctions occupées par l'agent non titulaire, de sa qualification et de la rémunération des agents de l'Etat de qualification équivalente exerçant des fonctions analogues. Pour l'application de ces dispositions, la rémunération antérieure et la rémunération proposée doivent être comparées en prenant en considération, pour leurs montants bruts, les salaires ainsi que les primes éventuellement accordées à l'agent et liées à l'exercice normal des fonctions, dans le cadre de son ancien comme de son nouveau contrat. Par suite, en comparant le montant net de la rémunération perçue par Mme A... au mois de décembre 2011 dans le cadre de son ancien contrat avec le montant net de la rémunération qu'elle a perçue en janvier 2012 en qualité d'agent du centre communal d'action sociale et en jugeant que les différences de rémunération brute sont sans incidence sur l'appréciation du caractère équivalent des rémunérations en cause, la cour administrative d'appel de Marseille a commis une erreur de droit. Dès lors, Mme A... est fondée, sans qu'il soit besoin d'examiner l'autre moyen de son pourvoi, à demander l'annulation de l'article 2 de l'arrêt qu'elle attaque.<br/>
<br/>
              4. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du centre communal d'action sociale de la commune d'Hyères la somme de 3 000 euros à verser à Mme A... au titre de l'article L. 761-1 du code de justice administrative. Les dispositions de cet article font obstacle à ce qu'une somme soit mise à la charge de Mme A... qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'article 2 de l'arrêt du 24 avril 2018 de la cour administrative d'appel de Marseille est annulé.<br/>
Article 2 : L'affaire est renvoyée, dans cette mesure, à la cour administrative d'appel de Marseille.<br/>
Article 3 : Le centre communal d'action sociale de la commune d'Hyères versera à Mme A... une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions centre communal d'action sociale de la commune d'Hyères au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à Mme B... A... et au centre communal d'action sociale de la commune d'Hyères.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-04-04 FONCTIONNAIRES ET AGENTS PUBLICS. CHANGEMENT DE CADRES, RECLASSEMENTS, INTÉGRATIONS. INTÉGRATION DE PERSONNELS N'APPARTENANT PAS ANTÉRIEUREMENT À LA FONCTION PUBLIQUE. - PERSONNE MORALE DE DROIT PRIVÉ EXERÇANT UNE ACTIVITÉ ÉCONOMIQUE ET LIÉE PAR CONTRAT À SON PERSONNEL SALARIÉ - REPRISE DE CETTE ACTIVITÉ PAR UNE PERSONNE MORALE DE DROIT PUBLIC DANS LE CADRE D'UN SERVICE PUBLIC ADMINISTRATIF - EFFETS DE LA REPRISE À L'ÉGARD DU PERSONNEL (ART. L. 1224-3 DU CODE DU TRAVAIL) - PRINCIPE DU MAINTIEN DE LA RÉMUNÉRATION [RJ1] - APPRÉCIATION AU REGARD DE LA RÉMUNÉRATION BRUTE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">36-08-01 FONCTIONNAIRES ET AGENTS PUBLICS. RÉMUNÉRATION. QUESTIONS D'ORDRE GÉNÉRAL. - PERSONNE MORALE DE DROIT PRIVÉ EXERÇANT UNE ACTIVITÉ ÉCONOMIQUE ET LIÉE PAR CONTRAT À SON PERSONNEL SALARIÉ - REPRISE DE CETTE ACTIVITÉ PAR UNE PERSONNE MORALE DE DROIT PUBLIC DANS LE CADRE D'UN SERVICE PUBLIC ADMINISTRATIF - EFFETS DE LA REPRISE À L'ÉGARD DU PERSONNEL (ART. L. 1224-3 DU CODE DU TRAVAIL) - PRINCIPE DU MAINTIEN DE LA RÉMUNÉRATION [RJ1] - APPRÉCIATION AU REGARD DE LA RÉMUNÉRATION BRUTE.
</SCT>
<ANA ID="9A"> 36-04-04 Pour l'application de l'article L. 1224-3 du code du travail, la rémunération antérieure et la rémunération proposée doivent être comparées en prenant en considération, pour leurs montants bruts, les salaires ainsi que les primes éventuellement accordées à l'agent et liées à l'exercice normal des fonctions, dans le cadre de son ancien comme de son nouveau contrat.</ANA>
<ANA ID="9B"> 36-08-01 Pour l'application de l'article L. 1224-3 du code du travail, la rémunération antérieure et la rémunération proposée doivent être comparées en prenant en considération, pour leurs montants bruts, les salaires ainsi que les primes éventuellement accordées à l'agent et liées à l'exercice normal des fonctions, dans le cadre de son ancien comme de son nouveau contrat.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., sur les modalités de mise en oeuvre de ce principe, CE, 21 mai 2007,,et autres, n° 299307, p. 214 ; CE, 25 juillet 2013, Centre hospitalier général de Longjumeau, n° 355804, T. pp. 648-659.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
