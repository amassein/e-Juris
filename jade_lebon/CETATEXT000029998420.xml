<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029998420</ID>
<ANCIEN_ID>JG_L_2014_12_000000369102</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/99/84/CETATEXT000029998420.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème / 4ème SSR, 30/12/2014, 369102</TITRE>
<DATE_DEC>2014-12-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>369102</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème / 4ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>BALAT</AVOCATS>
<RAPPORTEUR>M. Marc Lambron</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:369102.20141230</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la requête sommaire et le mémoire complémentaire, enregistrés les 6 juin et 5 septembre 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. A... B..., demeurant... ; M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 11PA03997 du 25 mars 2013 par lequel la cour administrative d'appel de Paris a rejeté sa requête dirigée contre le jugement n° 1017149/3-3 du tribunal administratif de Paris du 28 juin 2011 rejetant sa demande tendant à l'annulation de la décision du 31 août 2010 du directeur régional des finances publiques d'Ile-de-France rejetant son opposition à l'exécution du titre de perception d'un montant de 152 449,02 euros émis à son encontre le 21 novembre 1995 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit aux conclusions de sa requête d'appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement d'une somme de 8 000 euros au titre de l'article L. 761-1 du code de justice administrative et de la contribution pour l'aide juridique instituée par l'article R. 761-1 du même code ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi organique n° 62-1292 du 6 novembre 1962 ; <br/>
<br/>
              Vu le code civil ;<br/>
<br/>
              Vu le code électoral ; <br/>
<br/>
              Vu le code général des impôts ; <br/>
<br/>
              Vu le livre des procédures fiscales ; <br/>
<br/>
              Vu la loi n° 68-1250 du 31 décembre 1968 ; <br/>
<br/>
              Vu le décret n° 62-1587 du 29 décembre 1962 ; <br/>
<br/>
              Vu la décision du Conseil constitutionnel n° 95-88 du 11 octobre 1995 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Marc Lambron, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Balat, avocat de M. B...;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. B..., s'étant porté candidat à l'élection présidentielle des 23 avril et 7 mai 1995, a perçu de l'Etat une avance de 1 000 000 francs, correspondant à 152 449,02 euros, à valoir sur le remboursement forfaitaire de ses dépenses de campagne ; que, par sa décision n° 95-88 du 11 octobre 1995, le Conseil constitutionnel a rejeté son compte de campagne ; que le ministre de l'intérieur a émis à l'encontre de l'intéressé le 21 novembre 1995 un titre de perception du montant mentionné ci-dessus, correspondant à l'avance perçue ; que M. B... n'ayant pas procédé au règlement de la somme sollicitée, un commandement de payer notifié le 4 avril 1996 a augmenté la dette de l'intéressé envers l'Etat de 4 573,47 euros ; que, par deux courriers en date du 21 mai et du 9 juillet 2010, M. B... a formé une opposition à exécution au titre de perception émis le 21 novembre 1995 au motif notamment que la prescription quadriennale instituée par l'article L. 274 du livre des procédures fiscales lui était acquise ; que le directeur régional des finances publiques d'Ile-de-France et du département de Paris ayant rejeté cette opposition par une décision en date du 31 août 2010, M. B...a saisi le tribunal administratif de Paris de conclusions tendant, d'une part, à  l'annulation de la décision litigieuse et, d'autre part, à la décharge de la somme de 152 449,02 euros ; que cette demande a été rejetée par un jugement du 28 juin 2011 ; que M. B...ayant relevé appel de celui-ci, la cour administrative d'appel de Paris a rejeté sa requête par un arrêt du 25 mars 2013 ; que M. B... se pourvoit en cassation contre cet arrêt ;<br/>
<br/>
              Sur le moyen tiré de la méconnaissance du décret du 29 décembre 1962 et du livre des procédures fiscales :<br/>
<br/>
              2. Considérant que M. B..., pour contester devant les juges du fond le bien-fondé de la décision litigieuse, s'est prévalu des dispositions du décret du 29 décembre 1962 relatives au recouvrement des créances de l'Etat étrangères à l'impôt et au domaine ; que si, aux termes de l'article 87 de ce décret, " les poursuites sont exercées comme en matière d'impôts directs ", ces dispositions, qui ne concernent que les formes et procédures à observer dans l'exercice des poursuites contre les débiteurs, n'entraînent pas l'application aux créances en cause des règles de fond qui régissent les créances ayant un caractère fiscal ; qu'en particulier, elles n'ont pas pour effet de soumettre le recouvrement des créances de l'Etat étrangères à l'impôt et au domaine  aux dispositions de l'article L. 274 du livre des procédures fiscales, aux termes desquelles " les comptables du Trésor qui n'ont fait aucune poursuite contre un contribuable retardataire pendant quatre années consécutives, à partir du jour de la mise en recouvrement du rôle, perdent leur recours et sont déchus de tous droits et de toutes actions contre ce redevable " ; qu'en jugeant qu'en l'absence de dispositions spéciales fixant une prescription plus courte pour cette catégorie de créances, le recouvrement de la créance détenue par l'Etat sur M. B... était soumis, non à la prescription quadriennale, mais à la prescription trentenaire instituée par l'article 2262 du code civil alors en vigueur, la cour a fait une exacte application des dispositions de l'article 87 du décret du 29 décembre 1962 et de l'article L. 274 du livre des procédures fiscales ; qu'elle ne s'est pas méprise sur la portée du litige qui lui était soumis en jugeant que M. B... ne soulevait pas de moyen permettant d'apprécier le bien-fondé de ses conclusions tendant à ce que soit déclaré caduc le titre de perception contesté ;<br/>
<br/>
              Sur le moyen tiré de la méconnaissance de la loi du 31 décembre 1968 :<br/>
<br/>
              3. Considérant que, si M. B...invoquait devant les juges du fond le bénéfice de la loi du 31 décembre 1968 en tant qu'elle régit l'application de la prescription quadriennale aux créances détenues sur l'Etat, c'est par un arrêt suffisamment motivé et exempt d'erreur de droit que la cour a écarté ce moyen au motif que ces dispositions législatives ne rendaient pas cette même prescription applicable aux créances détenues par l'Etat sur des personnes privées ;<br/>
<br/>
              Sur le moyen tiré de la méconnaissance de l'article L. 52-15 du code électoral :<br/>
<br/>
              4. Considérant qu'aux termes du V de l'article 3 de la loi organique du 6 novembre 1962 relative à l'élection du Président de la République au suffrage universel, dans sa rédaction alors en vigueur : " Lors de la publication de la liste des candidats au premier tour, l'Etat verse à chacun d'entre eux une somme d'un million de francs, à titre d'avance sur le remboursement forfaitaire de leurs dépenses de campagne prévu à l'alinéa suivant. Si le montant du remboursement n'atteint pas cette somme, l'excédent fait l'objet d'un reversement. / Une somme égale au vingtième du montant du plafond des dépenses de campagne qui leur est applicable est remboursée, à titre forfaitaire, à chaque candidat ; cette somme est portée au quart dudit plafond pour chaque candidat ayant obtenu plus de 5 p. 100 du total des suffrages exprimés au premier tour. Elle ne peut excéder le montant des dépenses du candidat retracées dans son compte de campagne. / Le remboursement forfaitaire prévu à l'alinéa précédent n'est pas effectué aux candidats (...) dont le compte de campagne a été rejeté " ;<br/>
<br/>
              5. Considérant qu'au soutien du moyen soulevé devant la cour par M. B... et tiré de ce qu'il aurait été illégalement astreint au remboursement de l'avance qui lui avait été versée en application du V de l'article 3 de la loi organique du 6 novembre 1962, l'intéressé se prévalait des dispositions de l'article L. 52-15 du code électoral, rendues applicables à l'élection du Président de la République par le II de l'article 3 de la même loi organique, lesquelles prévoient qu'en cas de rejet d'un compte de campagne pour cause de dépassement du plafond des dépenses électorales, la commission nationale des comptes de campagne et des financements politiques fixe le montant de la somme que le candidat en cause est tenu de verser au Trésor public ; que, toutefois, l'obligation de restitution de l'avance sur le remboursement forfaitaire des dépenses de campagne était applicable à M. B...par l'effet des seules dispositions du dernier alinéa du V de l'article 3 de la loi organique du 6 novembre 1962, qui n'opère aucune distinction selon les motifs fondant le rejet du compte de campagne ; qu'en revanche, les dispositions de l'article L. 52-15 du code électoral étaient sans rapport avec la situation de l'intéressé, dès lors que le rejet de son compte de campagne n'avait pas été motivé par le dépassement du plafond des dépenses électorales ; qu'ainsi, en écartant le moyen soulevé par M. B...et tiré de ce que la décision litigieuse aurait été prise en méconnaissance des dispositions de l'article L. 52-15 du code électoral, la cour n'a pas commis d'erreur de droit ;<br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède que M. B...n'est pas fondé à demander l'annulation de l'arrêt attaqué ;<br/>
<br/>
              Sur l'application des dispositions des articles L. 761-1 et R. 761-1 du code de justice administrative :<br/>
<br/>
              7. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que l'Etat, qui n'est pas la partie perdante dans la présente instance, verse à M. B...les sommes qu'il demande à ce titre ; qu'il y a lieu, dans les circonstances de l'espèce, de laisser à la charge de M. B...la contribution pour l'aide juridique ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
		Article 1er : Le pourvoi de M. B...est rejeté.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. A...B...et au ministre des finances et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">28-01 ÉLECTIONS ET RÉFÉRENDUM. ÉLECTIONS PRÉSIDENTIELLES. - REVERSEMENT DE L'AVANCE FAITE AU CANDIDAT - A) CAS DU DÉPASSEMENT DU PLAFOND DES DÉPENSES ÉLECTORALES - EXISTENCE, SUR LE FONDEMENT DE L'ARTICLE L. 52-15 DU CODE ÉLECTORAL - B) AUTRES MOTIFS DE REJET DU COMPTE DE CAMPAGNE - EXISTENCE, SUR LE FONDEMENT DU V DE L'ARTICLE 3 DE L'ORDONNANCE ORGANIQUE N° 62-1292 DU 6 NOVEMBRE 1962.
</SCT>
<ANA ID="9A"> 28-01 Les dispositions de l'article L. 52-15 du code électoral, rendues applicables à l'élection du Président de la République par le II de l'article 3 de l'ordonnance organique n° 62-1292 du 6 novembre 1962, prévoient qu'en cas de rejet d'un compte de campagne pour cause de dépassement du plafond des dépenses électorales, la commission nationale des comptes de campagne et des financements politiques fixe le montant de la somme que le candidat en cause est tenu de verser au Trésor public.... ,,Cependant, l'obligation de restitution de l'avance faite à un candidat à l'élection présidentielle sur le remboursement forfaitaire des dépenses de campagne est également applicable par l'effet des seules dispositions du dernier alinéa du V de l'article 3 de l'ordonnance organique n° 62-1292 du 6 novembre 1962, qui n'opère aucune distinction selon les motifs fondant le rejet du compte de campagne.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
