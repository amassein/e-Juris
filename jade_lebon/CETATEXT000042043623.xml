<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042043623</ID>
<ANCIEN_ID>JG_L_2020_06_000000421643</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/04/36/CETATEXT000042043623.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 25/06/2020, 421643</TITRE>
<DATE_DEC>2020-06-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>421643</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP COLIN-STOCLET ; SCP GATINEAU, FATTACCINI, REBEYROL</AVOCATS>
<RAPPORTEUR>M. Laurent-Xavier Simonel</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Gabrielle Merloz</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:421643.20200625</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Mme B... A... a demandé au tribunal administratif de Montpellier :<br/>
<br/>
              - d'annuler la décision du 1er octobre 2014 du président de la collectivité intercommunale de collecte et de valorisation des déchets ménagers de l'Aude (Covaldem 11) refusant de prendre en charge le remboursement d'honoraires d'avocat et d'huissier au titre de la protection fonctionnelle qu'elle sollicitait ainsi que la décision de rejet de son recours gracieux ;<br/>
              - d'enjoindre à la Covaldem 11 de lui accorder le bénéfice de la protection fonctionnelle intégrale et de rembourser les honoraires d'avocat et d'huissier avancés dans le cadre de la procédure pénale engagée ;<br/>
              - de condamner la Covaldem 11 à lui verser une somme de 10 000 euros, assortie des intérêts au taux légal à compter de la date de réception de sa demande préalable et de la capitalisation des intérêts, en réparation du préjudice moral subi du fait du refus de lui octroyer la protection fonctionnelle intégrale ; <br/>
              - de condamner la Covaldem 11 à lui verser une somme de 4 323, 74 euros à parfaire, assortie des intérêts au taux légal à compter de la date de réception de sa demande préalable et de la capitalisation des intérêts, en remboursement des frais avancés de la procédure pénale.<br/>
<br/>
              Par un jugement n° 1405446 du 8 avril 2016, le tribunal administratif de Montpellier a rejeté cette demande.<br/>
<br/>
              Par un arrêt n° 16MA02220 du 20 avril 2018, la cour administrative d'appel de Marseille, sur l'appel formé par Mme A..., après avoir annulé ce jugement, a annulé les deux décisions contestées, condamné la Covaldem 11 à verser à Mme A... la somme de 2 500 euros tous intérêts confondus et rejeté le surplus des conclusions de sa demande.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés au secrétariat de la section du contentieux les 20 juin 2018, 30 août et 4 octobre 2019, la Covaldem 11 demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt en ce qu'il a fait droit à l'appel de Mme A... ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter dans cette mesure les conclusions d'appel de Mme A... ;<br/>
<br/>
              3°) de mettre à la charge de Mme A... la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Laurent-Xavier Simonel, conseiller d'Etat en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Marie-Gabrielle Merloz, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Colin-Stoclet, avocat de la collectivité intercommunale de la collecte des déchets de l'Aude et à la SCP Gatineau, Fattaccini, Rebeyrol, avocat de Mme A... ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que Mme A..., fonctionnaire territoriale en service au sein de la collectivité intercommunale de collecte et de valorisation des déchets ménagers de l'Aude (Covaldem 11) et candidate aux élections municipales de Carcassonne de 2014, a demandé au président de la Covaldem 11, par une lettre du 18 février 2014, de lui accorder la protection fonctionnelle au titre de l'article 11 de la loi du 13 juillet 1983 portant droits et obligations des fonctionnaires par la prise en charge des frais d'une action en justice qu'elle entendait intenter à la suite de propos tenus publiquement pendant la campagne électorale par le représentant d'une liste adverse sur laquelle figurait le président de la Covaldem 11, lui imputant d'avoir obtenu son emploi par favoritisme et d'avoir ensuite manqué de gratitude envers son employeur. Par une lettre du 26 février 2014 à l'auteur de ces propos, le président de la Covaldem 11 lui a demandé de s'abstenir de les renouveler. Par une lettre du 26 février 2014 à Mme A..., dont les termes ont été repris par une lettre du 18 mars 2014, le président de la Covaldem 11 lui a fait part de l'avertissement adressé à l'auteur des propos litigieux. Par une lettre datée du 8 juillet 2014, Mme A... a renouvelé sa demande de prise en charge des frais de l'action en justice qu'elle entendait intenter. Par une décision du 1er octobre 2014, le président de la Covaldem 11 a considéré que la protection fonctionnelle accordée à Mme A... s'était traduite par la mesure déjà prise le 26 février 2014 et a refusé la prise en charge des frais de l'action en justice engagée par Mme A.... La Covaldem 11 se pourvoit en cassation contre l'arrêt du 20 avril 2018 par lequel la cour administrative d'appel de Marseille a annulé cette dernière décision et condamné la Covaldem 11 à verser à Mme A... la somme de 2 500 euros tous intérêts confondus. <br/>
<br/>
              2. En premier lieu, aux termes des deux premiers alinéas de l'article R. 421-2 du code de justice administrative, dans sa rédaction applicable au litige : " Sauf disposition législative ou réglementaire contraire, le silence gardé pendant plus de deux mois sur une réclamation par l'autorité compétente vaut décision de rejet. / Les intéressés disposent, pour se pourvoir contre cette décision implicite, d'un délai de deux mois à compter du jour de l'expiration de la période mentionnée au premier alinéa. Néanmoins, lorsqu'une décision explicite de rejet intervient dans ce délai de deux mois, elle fait à nouveau courir le délai du pourvoi ". Aux termes de l'article R. 421-5 du même code : " Les délais de recours contre une décision administrative ne sont opposables qu'à la condition d'avoir été mentionnés, ainsi que les voies de recours, dans la notification de la décision ".<br/>
<br/>
              3. Il ressort de manière constante des pièces du dossier soumis aux juges du fond que les lettres du président de la Covaldem 11 à Mme A... du 26 février et du 18 mars 2014 ne mentionnaient pas les voies et délais de recours et, ainsi, n'étaient pas devenues définitives à la date à laquelle la décision attaquée du 1er octobre 2014 est intervenue. Par ailleurs, si Mme A... a adressé à nouveau au président de la Covaldem 11, le 8 juillet 2014, une lettre par laquelle elle réitérait sa demande, le délai de recours contre la décision implicite de rejet de cette demande résultant du silence gardé pendant deux mois par le président de la Covaldem 11 n'était pas expiré lorsqu'est intervenue la décision expresse du 1er octobre 2014. Par conséquent, cette dernière décision ne pouvait être regardée comme étant confirmative d'une précédente décision portant sur le même objet qui serait devenue définitive et la demande de Mme A..., enregistrée le 1er décembre 2014 au greffe du tribunal administratif de Montpellier, n'était pas tardive. Ce motif, qui repose sur le constat de faits constants n'appelant pas d'appréciation, doit être substitué au motif erroné, tiré de ce que le président de la Covaldem 11 ne s'était pas prononcé avant le 1er octobre 2014 sur une demande de protection fonctionnelle présentée par Mme A..., retenu par l'arrêt de la cour administrative d'appel, dont il justifie légalement le dispositif.<br/>
<br/>
              4. En second lieu, aux termes de l'article 11 de la loi du 13 juillet 1983 portant droits et obligations des fonctionnaires, dans sa rédaction applicable au litige : " Les fonctionnaires bénéficient, à l'occasion de leurs fonctions et conformément aux règles fixées par le code pénal et les lois spéciales, d'une protection organisée par la collectivité publique qui les emploie à la date des faits en cause ou des faits ayant été imputés de façon diffamatoire au fonctionnaire. / (...) La collectivité publique est tenue de protéger les fonctionnaires contre les menaces, violences, voies de fait, injures, diffamations ou outrages dont ils pourraient être victimes à l'occasion de leurs fonctions, et de réparer, le cas échéant, le préjudice qui en est résulté. / (...) ".<br/>
<br/>
              5. D'une part, la circonstance que les propos qui motivaient la demande de protection, lesquels mettaient en cause Mme A... au titre de l'emploi qu'elle occupait à la Covaldem 11 et présentaient un lien avec l'exercice de ses fonctions, aient été tenus dans le cadre d'une campagne électorale n'était pas de nature à faire obstacle à l'application des dispositions de l'article 11 de la loi du 13 juillet 1983. Par suite, en jugeant qu'une telle circonstance était sans incidence sur l'obligation qui incombait à la Covaldem 11 en vertu des dispositions de l'article 11 de la loi du 13 juillet 1983 et que cette circonstance ne constituait pas un motif d'intérêt général pouvant justifier un refus d'accorder la protection sollicitée, la cour administrative d'appel, qui n'avait pas à se prononcer sur le moyen, inopérant dans le cadre du contentieux dont elle était saisie, tiré de ce que les propos incriminés n'excédaient pas les limites de la polémique électorale, n'a pas commis d'erreur de droit. <br/>
<br/>
              6. D'autre part, la cour administrative d'appel s'est livrée à une appréciation souveraine des faits de l'espèce, exempte de dénaturation, en jugeant que la seule admonestation adressée, par la lettre du 26 février 2014, à l'auteur des propos incriminés, laquelle n'avait pas été portée à la connaissance de l'intéressée qui ne l'a découverte qu'à l'occasion de l'instance devant le tribunal administratif, ne pouvait, dans les circonstances de l'espèce, être regardée comme une mesure de protection appropriée. <br/>
<br/>
              7. Il résulte de ce qui précède que la Covaldem 11 n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque. <br/>
<br/>
              8. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mis à la charge de Mme A..., qui n'est pas la partie perdante dans la présente instance, le versement de la somme que demande, à ce titre, la Covaldem 11. En revanche, il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la Covaldem 11 le versement d'une somme de 3 000 euros à Mme A... au titre de ces dispositions.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la collectivité intercommunale de collecte et de valorisation des déchets ménagers de l'Aude est rejeté.<br/>
Article 2 : La collectivité intercommunale de collecte et de valorisation des déchets ménagers de l'Aude versera la somme de 3 000 euros à Mme A... au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 3 : La présente décision sera notifiée à la collectivité intercommunale de collecte et de valorisation des déchets ménagers de l'Aude et à Mme B... A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-04-03-07-04 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. PRINCIPES GÉNÉRAUX DU DROIT. PRINCIPES INTÉRESSANT L'ACTION ADMINISTRATIVE. GARANTIES DIVERSES ACCORDÉES AUX AGENTS PUBLICS. - PROTECTION FONCTIONNELLE (ART. 11 DE LA LOI DU 13 JUILLET 1983) - 1) POSSIBILITÉ D'EN BÉNÉFICIER À RAISON D'ATTAQUES SURVENUES DANS LE CADRE D'UNE CAMPAGNE ÉLECTORALE - EXISTENCE - 2) CONTRÔLE DU JUGE DE CASSATION - DÉNATURATION [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">36-07-10-005 FONCTIONNAIRES ET AGENTS PUBLICS. STATUTS, DROITS, OBLIGATIONS ET GARANTIES. GARANTIES ET AVANTAGES DIVERS. PROTECTION CONTRE LES ATTAQUES. - PROTECTION FONCTIONNELLE (ART. 11 DE LA LOI DU 13 JUILLET 1983) - 1) POSSIBILITÉ D'EN BÉNÉFICIER À RAISON D'ATTAQUES SURVENUES DANS LE CADRE D'UNE CAMPAGNE ÉLECTORALE - EXISTENCE - 2) CONTRÔLE DU JUGE DE CASSATION - DÉNATURATION [RJ1].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-08-02-02-01-04 PROCÉDURE. VOIES DE RECOURS. CASSATION. CONTRÔLE DU JUGE DE CASSATION. BIEN-FONDÉ. DÉNATURATION. - ADÉQUATION DES MESURES DE PROTECTION FONCTIONNELLE (ART. 11 DE LA LOI DU 13 JUILLET 1983) [RJ1].
</SCT>
<ANA ID="9A"> 01-04-03-07-04 1) La circonstance que les propos motivant la demande de protection, lesquels présentaient un lien avec l'exercice des fonctions de l'intéressée, aient été tenus dans le cadre d'une campagne électorale n'est pas de nature à faire obstacle à l'application de l'article 11 de la loi n° 83-634 du 13 juillet 1983.,,,2) Les juges du fond apprécient souverainement, sauf dénaturation, le caractère approprié des mesures de protection prises en application de ces dispositions.</ANA>
<ANA ID="9B"> 36-07-10-005 1) La circonstance que les propos motivant la demande de protection, lesquels présentaient un lien avec l'exercice des fonctions de l'intéressée, aient été tenus dans le cadre d'une campagne électorale n'est pas de nature à faire obstacle à l'application de l'article 11 de la loi n° 83-634 du 13 juillet 1983.,,,2) Les juges du fond apprécient souverainement, sauf dénaturation, le caractère approprié des mesures de protection prises en application de ces dispositions.</ANA>
<ANA ID="9C"> 54-08-02-02-01-04 Les juges du fond apprécient souverainement, sauf dénaturation, le caractère approprié des mesures de protection prises en application de l'article 11 de la loi n° 83-634 du 13 juillet 1983.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., s'agissant de l'appréciation de la portée de propos tenus à l'encontre d'un fonctionnaire, CE, 3 mars 2003, Centre d'aide par le travail de Cheney, n° 235052, T. p. 963.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
