<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035608412</ID>
<ANCIEN_ID>JG_L_2017_09_000000407031</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/60/84/CETATEXT000035608412.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 22/09/2017, 407031</TITRE>
<DATE_DEC>2017-09-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>407031</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>BERTRAND ; SCP GADIOU, CHEVALLIER</AVOCATS>
<RAPPORTEUR>Mme Karin Ciavaldini</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:407031.20170922</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Le centre régional des oeuvres universitaires et scolaires (CROUS) de Lyon a demandé au juge des référés du tribunal administratif de Lyon d'ordonner, sur le fondement de l'article L. 521-3 du code de justice administrative, l'expulsion de M. B... A...du logement qu'il occupe dans la résidence des Quais à Lyon. Par une ordonnance n° 1608390 du 8 décembre 2016, le juge des référés a fait droit à la demande.  <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 20 janvier et 3 février 2017 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de rejeter la demande présentée par le CROUS de Lyon ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros à verser à son avocat, Me C... Bertrand, au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code des procédures civiles d'exécution ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Karin Ciavaldini, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Bertrand, avocat de M. A...et à la SCP Gadiou, Chevallier, avocat du centre régional des oeuvres universitaires et scolaires de Lyon.<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Le centre régional des oeuvres universitaires et scolaires (CROUS) de Lyon a demandé au juge des référés du tribunal administratif de Lyon d'ordonner, sur le fondement de l'article L. 521-3 du code de justice administrative, l'expulsion de M. B... A...du logement qu'il occupait dans la résidence des Quais à Lyon. Par l'ordonnance attaquée du 8 décembre 2016, le juge des référés a fait droit à la demande.<br/>
<br/>
              2. La circonstance que M. A...aurait évacué les lieux en exécution de l'ordonnance attaquée n'est pas de nature à rendre sans objet le pourvoi qu'il a formé contre cette ordonnance. Par suite, l'exception soulevée en défense par le CROUS de Lyon et tendant à ce que le Conseil d'Etat constate qu'il n'y a plus lieu de statuer sur le pourvoi ne peut qu'être écartée.<br/>
<br/>
              3. En premier lieu, aux termes de l'article L. 412-6 du code des procédures civiles d'exécution : " Nonobstant toute décision d'expulsion passée en force de chose jugée et malgré l'expiration des délais accordés en vertu de l'article L. 412-3, il est sursis à toute mesure d'expulsion non exécutée à la date du 1er novembre de chaque année jusqu'au 31 mars de l'année suivante, à moins que le relogement des intéressés soit assuré dans des conditions suffisantes respectant l'unité et les besoins de la famille (...) ". Aux termes de l'article L. 412-7 du même code : " Les dispositions des articles L. 412-3 à L. 412-6 ne sont pas applicables aux occupants de locaux spécialement destinés aux logements d'étudiants lorsque les intéressés cessent de satisfaire aux conditions en raison desquelles le logement a été mis à leur disposition (...) ". M. A... soutient qu'en jugeant que " le principe de la trêve hivernale des expulsions ne trouve pas à s'appliquer aux étudiants ", le juge des référés a commis une erreur de droit.<br/>
<br/>
              4. Les dispositions de l'article L. 412-6 du code des procédures civiles d'exécution prévoient seulement un sursis aux mesures d'expulsion non exécutées à la date du 1er novembre de chaque année, si le relogement de l'intéressé n'est pas assuré. Elles ne s'opposent pas au prononcé par le juge, même pendant la période dite de " trêve hivernale " mentionnée à cet article, d'une décision d'expulsion. Il en résulte que le principe de la " trêve hivernale " ne pouvait, en tout état de cause, trouver application dans le cadre de l'examen par le juge des référés de la demande dont il était saisi, laquelle concernait le prononcé d'une mesure d'expulsion. Dès lors, M. A...ne pouvait utilement invoquer ce principe pour contester la mesure demandée au juge des référés par le CROUS de Lyon. Ce motif, qui est d'ordre public et n'appelle l'appréciation d'aucune circonstance de fait, doit être substitué à celui retenu par le juge des référés, dont il justifie légalement le dispositif sur ce point.<br/>
<br/>
              5. En second lieu, le juge des référés n'a pas dénaturé les pièces du dossier qui lui était soumis en relevant, pour estimer que la demande d'expulsion présentait un caractère d'urgence et d'utilité, l'irrégularité du paiement des loyers, l'absence d'accord oral de la directrice du centre pour un maintien de M. A...dans les lieux, le défaut de justification de la part de M. A...de la nécessité de son maintien dans la résidence pour des raisons de santé et de scolarité ainsi que le fait que sa présence dans les lieux constituait un obstacle à l'accomplissement de la mission de service public de logement des étudiants dont est chargé le CROUS.<br/>
<br/>
              6. Il résulte de ce qui précède que le pourvoi de M. A...doit être rejeté, y compris ses conclusions présentées au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par le CROUS de Lyon au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. A...est rejeté.<br/>
Article 2 : Les conclusions présentées par le CROUS de Lyon au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à M. B...A...et au centre régional des oeuvres universitaires et scolaires de Lyon.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">38 LOGEMENT. - EXPULSION - MOYEN TIRÉ DE LA MÉCONNAISSANCE PAR UNE DÉCISION D'EXPULSION DES DISPOSITIONS DU CPCE RELATIVES À LA PÉRIODE DITE DE TRÊVE HIVERNALE - MOYEN INOPÉRANT [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-07-01-04-03 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. QUESTIONS GÉNÉRALES. MOYENS. MOYENS INOPÉRANTS. - EXPULSION D'UN LOGEMENT - MOYEN TIRÉ DE LA MÉCONNAISSANCE PAR UNE DÉCISION D'EXPULSION DES DISPOSITIONS DU CPCE RELATIVES À LA PÉRIODE DITE DE TRÊVE HIVERNALE [RJ1].
</SCT>
<ANA ID="9A"> 38 Les dispositions de l'article L. 412-6 du code des procédures civiles d'exécution (CPCE) prévoient seulement un sursis aux mesures d'expulsion non exécutées à la date du 1er novembre de chaque année, si le relogement de l'intéressé n'est pas assuré. Elles ne s'opposent pas au prononcé par le juge, même pendant la période dite de trêve hivernale mentionnée à cet article, d'une décision d'expulsion.</ANA>
<ANA ID="9B"> 54-07-01-04-03 Les dispositions de l'article L. 412-6 du code des procédures civiles d'exécution (CPCE) prévoient seulement un sursis aux mesures d'expulsion non exécutées à la date du 1er novembre de chaque année, si le relogement de l'intéressé n'est pas assuré. Elles ne s'opposent pas au prononcé par le juge, même pendant la période dite de trêve hivernale mentionnée à cet article, d'une décision d'expulsion.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr. Cass. civ. 2ème, 4 juillet 2007, Epoux Rinn c/ Association immobilière des Hautes-Vosges, n°05-15.382, Bull. civ. II n° 183; Cass. civ. 2ème, 14 octobre 2010, n°09-13.800.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
