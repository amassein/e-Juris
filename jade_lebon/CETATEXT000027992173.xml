<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027992173</ID>
<ANCIEN_ID>JG_L_2013_09_000000363184</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/99/21/CETATEXT000027992173.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème et 10ème sous-sections réunies, 25/09/2013, 363184</TITRE>
<DATE_DEC>2013-09-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>363184</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème et 10ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FABIANI, LUC-THALER</AVOCATS>
<RAPPORTEUR>Mme Maïlys Lange</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Claire Legras</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:363184.20130925</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 2 octobre 2012 au secrétariat du contentieux du Conseil d'Etat, présentée pour la société Rapidépannage 62, dont le siège est 1 rue de la Libération à Haillicourt (62940) ; la société demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2012-953 du 1er août 2012 portant sanction du dépannage exercé sans agrément sur les autoroutes et les ouvrages d'art concédés du réseau routier national ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
              3°) de condamner l'Etat à supporter les dépens et, notamment, les frais du timbre fiscal ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution ; <br/>
<br/>
              Vu le code pénal ;<br/>
<br/>
              Vu le code de la route ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Maïlys Lange, Auditeur,  <br/>
<br/>
              - les conclusions de Mme Claire Legras, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Fabiani, Luc-Thaler, avocat de la société Rapidépannage 62 ;<br/>
<br/>
<br/>
<br/>1. Considérant que le décret attaqué du 1er août 2012 insère dans le code de la route les articles R. 421-10 et R. 422-5, aux termes desquels " le fait d'exercer l'activité de dépannage sur une autoroute concédée " ou un ouvrage d'art concédé du réseau routier national, " ses dépendances domaniales ou ses installations annexes sans être titulaire d'un agrément délivré à cette fin par le préfet dans les conditions prévues par le contrat de concession est puni de l'amende prévue pour les contraventions de la 4ème classe " ; <br/>
<br/>
              2. Considérant qu'en donnant compétence au législateur pour fixer " les règles concernant (...) les garanties fondamentales accordées aux citoyens pour l'exercice des libertés publiques ", l'article 34 de la Constitution n'a pas retiré au chef du gouvernement les attributions de police générale qu'il exerçait antérieurement ; qu'à ce titre, il appartient au Premier ministre d'adopter par voie règlementaire les mesures propres à assurer la sécurité des personnes sur les autoroutes et les ouvrages d'art concédés du réseau routier national ; qu'il a pu apporter au libre exercice de l'activité de service public de dépannage des véhicules en panne ou accidentés sur l'ensemble du domaine ainsi concédé - lequel, compte tenu des exigences de son exploitation, fait déjà l'objet d'une règlementation - ainsi que sur les aires de repos et de stationnement qui en sont des installations annexes, une restriction qui, en se limitant à l'obtention préalable d'un agrément délivré par le représentant de l'Etat dans les conditions prévues par les contrats de concession, a principalement pour objet de s'assurer que les entreprises sélectionnées seront en mesure de remplir leurs missions dans l'ensemble du périmètre de la concession, et répond aux objectifs de la sécurité routière sur des voies où les conditions de circulation conjuguent vitesse élevée et importance du trafic ; qu'ainsi, la société requérante n'est pas fondée à soutenir qu'en règlementant l'exercice de l'activité de dépannage sur les autoroutes ou les ouvrages d'art concédés du réseau routier national, le décret attaqué serait intervenu dans une matière qui relève du domaine de la loi et aurait porté une atteinte excessive à la liberté d'entreprendre des entreprises de dépannage ;<br/>
<br/>
              3. Considérant que, d'après l'article 37 de la Constitution : " Les matières autres que celles qui sont du domaine de la loi ont un caractère règlementaire " ; que, si l'article 34 réserve à la loi le soin de fixer " les règles concernant (...) la détermination des crimes et délits ainsi que les peines qui leur sont applicables ", cet article ne mentionne pas, en revanche, les règles concernant la détermination des infractions punies de peines contraventionnelles ; qu'au nombre de celles-ci figure, en vertu de l'article 131-12 du code pénal, s'agissant des personnes physiques, et de l'article 131-40 du même code, s'agissant des personnes morales, l'amende ; qu'ainsi les dispositions du décret attaqué ont pu légalement punir de l'amende prévue pour les contraventions de 4° classe toute infraction aux dispositions qu'il édicte ; <br/>
<br/>
              4. Considérant que le décret attaqué renvoie aux stipulations des contrats de concession, approuvées par d'autres dispositions règlementaires, pour la détermination de la procédure d'attribution des agréments à laquelle l'activité de dépannage sur autoroutes est soumise ; que, par suite, la société requérante ne peut utilement se prévaloir, à l'appui de son recours contre le décret attaqué, de l'illégalité dont serait entachée la procédure d'attribution des agréments ; <br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que la société requérante n'est pas fondée à demander l'annulation du décret attaqué ; que ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées ; que, dans les circonstances de l'espèce, il y a lieu de laisser la contribution pour l'aide juridique à sa charge ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : La requête de la société Rapidépannage 62 est rejetée.<br/>
Article 2 : La présente décision sera notifiée à la société Rapidépannage 62, au Premier ministre et au ministre de l'écologie, du développement durable et de l'énergie. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-02-01-03-18 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - COMPÉTENCE. LOI ET RÈGLEMENT. ARTICLES 34 ET 37 DE LA CONSTITUTION - MESURES RELEVANT DU DOMAINE DU RÈGLEMENT. MESURES RELEVANT PAR NATURE DU DOMAINE DU RÈGLEMENT. - POLICE GÉNÉRALE [RJ1] - INCLUSION - MESURES RÉGLEMENTAIRES PROPRES À ASSURER LA SÉCURITÉ DES PERSONNES SUR LES AUTOROUTES ET LES OUVRAGES D'ART CONCÉDÉS DU RÉSEAU ROUTIER NATIONAL - POSSIBILITÉ POUR LE PREMIER MINISTRE DE SUBORDONNER L'EXERCICE DE L'ACTIVITÉ DE SERVICE PUBLIC DE DÉPANNAGE DES VÉHICULES EN PANNE OU ACCIDENTÉS SUR CE DOMAINE À L'OBTENTION PRÉALABLE D'UN AGRÉMENT DÉLIVRÉ PAR LE REPRÉSENTANT DE L'ETAT - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">01-02-02-01-02 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - COMPÉTENCE. RÉPARTITION DES COMPÉTENCES ENTRE AUTORITÉS DISPOSANT DU POUVOIR RÉGLEMENTAIRE. AUTORITÉS DISPOSANT DU POUVOIR RÉGLEMENTAIRE. PREMIER MINISTRE. - POLICE GÉNÉRALE [RJ1] - INCLUSION - MESURES RÉGLEMENTAIRES PROPRES À ASSURER LA SÉCURITÉ DES PERSONNES SUR LES AUTOROUTES ET LES OUVRAGES D'ART CONCÉDÉS DU RÉSEAU ROUTIER NATIONAL - POSSIBILITÉ DE SUBORDONNER L'EXERCICE DE L'ACTIVITÉ DE SERVICE PUBLIC DE DÉPANNAGE DES VÉHICULES EN PANNE OU ACCIDENTÉS SUR CE DOMAINE À L'OBTENTION PRÉALABLE D'UN AGRÉMENT DÉLIVRÉ PAR LE REPRÉSENTANT DE L'ETAT - EXISTENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">14-02-01-07 COMMERCE, INDUSTRIE, INTERVENTION ÉCONOMIQUE DE LA PUISSANCE PUBLIQUE. RÉGLEMENTATION DES ACTIVITÉS ÉCONOMIQUES. ACTIVITÉS SOUMISES À RÉGLEMENTATION. DIVERSES ACTIVITÉS. - DÉPANNAGE DES VÉHICULES EN PANNE OU ACCIDENTÉS SUR LES AUTOROUTES ET LES OUVRAGES D'ART CONCÉDÉS DU RÉSEAU ROUTIER NATIONAL - POSSIBILITÉ POUR LE PREMIER MINISTRE, AU TITRE DE SES POUVOIRS DE POLICE GÉNÉRALE [RJ1], DE SUBORDONNER L'EXERCICE DE CETTE ACTIVITÉ À L'OBTENTION PRÉALABLE D'UN AGRÉMENT DÉLIVRÉ PAR LE REPRÉSENTANT DE L'ETAT - EXISTENCE.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">14-02-02-02 COMMERCE, INDUSTRIE, INTERVENTION ÉCONOMIQUE DE LA PUISSANCE PUBLIQUE. RÉGLEMENTATION DES ACTIVITÉS ÉCONOMIQUES. MODALITÉS DE LA RÉGLEMENTATION. AGRÉMENT. - ACTIVITÉ DE SERVICE PUBLIC DE DÉPANNAGE DES VÉHICULES EN PANNE OU ACCIDENTÉS SUR LES AUTOROUTES ET LES OUVRAGES D'ART CONCÉDÉS DU RÉSEAU ROUTIER NATIONAL - POSSIBILITÉ POUR LE PREMIER MINISTRE, AU TITRE DE SES POUVOIRS DE POLICE GÉNÉRALE [RJ1], DE SUBORDONNER L'EXERCICE DE CETTE ACTIVITÉ À L'OBTENTION PRÉALABLE D'UN AGRÉMENT DÉLIVRÉ PAR LE REPRÉSENTANT DE L'ETAT - EXISTENCE.
</SCT>
<SCT ID="8E" TYPE="PRINCIPAL">49-02-02 POLICE. AUTORITÉS DÉTENTRICES DES POUVOIRS DE POLICE GÉNÉRALE. PREMIER MINISTRE. - POUVOIRS DE POLICE GÉNÉRALE [RJ1] - INCLUSION - MESURES RÉGLEMENTAIRES PROPRES À ASSURER LA SÉCURITÉ DES PERSONNES SUR LES AUTOROUTES ET LES OUVRAGES D'ART CONCÉDÉS DU RÉSEAU ROUTIER NATIONAL - POSSIBILITÉ DE SUBORDONNER L'EXERCICE DE L'ACTIVITÉ DE SERVICE PUBLIC DE DÉPANNAGE DES VÉHICULES EN PANNE OU ACCIDENTÉS SUR CE DOMAINE À L'OBTENTION PRÉALABLE D'UN AGRÉMENT DÉLIVRÉ PAR LE REPRÉSENTANT DE L'ETAT - EXISTENCE.
</SCT>
<SCT ID="8F" TYPE="PRINCIPAL">65-02 TRANSPORTS. TRANSPORTS ROUTIERS. - SERVICE PUBLIC DE DÉPANNAGE DES VÉHICULES EN PANNE OU ACCIDENTÉS SUR LES AUTOROUTES ET LES OUVRAGES D'ART CONCÉDÉS DU RÉSEAU ROUTIER NATIONAL - POSSIBILITÉ POUR LE PREMIER MINISTRE, AU TITRE DE SES POUVOIRS DE POLICE GÉNÉRALE, DE SUBORDONNER L'EXERCICE DE CETTE ACTIVITÉ À L'OBTENTION PRÉALABLE D'UN AGRÉMENT DÉLIVRÉ PAR LE REPRÉSENTANT DE L'ETAT - EXISTENCE.
</SCT>
<ANA ID="9A"> 01-02-01-03-18 Au titre de ses attributions de police générale, il appartient au Premier ministre d'adopter par voie règlementaire les mesures propres à assurer la sécurité des personnes sur les autoroutes et les ouvrages d'art concédés du réseau routier national.... ,,Le Premier ministre a ainsi pu apporter à l'exercice de l'activité de service public de dépannage des véhicules en panne ou accidentés sur l'ensemble du domaine ainsi concédé - lequel, compte tenu des exigences de son exploitation, fait déjà l'objet d'une règlementation - ainsi que sur les aires de repos et de stationnement qui en sont des installations annexes, une restriction qui, en se limitant à l'obtention préalable d'un agrément délivré par le représentant de l'Etat dans les conditions prévues par les contrats de concession, a principalement pour objet de s'assurer que les entreprises sélectionnées seront en mesure de remplir leurs missions dans l'ensemble du périmètre de la concession, et répond aux objectifs de la sécurité routière sur des voies où les conditions de circulation conjuguent vitesse élevée et importance du trafic.</ANA>
<ANA ID="9B"> 01-02-02-01-02 Au titre de ses attributions de police générale, il appartient au Premier ministre d'adopter par voie règlementaire les mesures propres à assurer la sécurité des personnes sur les autoroutes et les ouvrages d'art concédés du réseau routier national.... ,,Le Premier ministre a ainsi pu apporter à l'exercice de l'activité de service public de dépannage des véhicules en panne ou accidentés sur l'ensemble du domaine ainsi concédé - lequel, compte tenu des exigences de son exploitation, fait déjà l'objet d'une règlementation - ainsi que sur les aires de repos et de stationnement qui en sont des installations annexes, une restriction qui, en se limitant à l'obtention préalable d'un agrément délivré par le représentant de l'Etat dans les conditions prévues par les contrats de concession, a principalement pour objet de s'assurer que les entreprises sélectionnées seront en mesure de remplir leurs missions dans l'ensemble du périmètre de la concession, et répond aux objectifs de la sécurité routière sur des voies où les conditions de circulation conjuguent vitesse élevée et importance du trafic.</ANA>
<ANA ID="9C"> 14-02-01-07 Au titre de ses attributions de police générale, il appartient au Premier ministre d'adopter par voie règlementaire les mesures propres à assurer la sécurité des personnes sur les autoroutes et les ouvrages d'art concédés du réseau routier national.... ,,Le Premier ministre a ainsi pu apporter à l'exercice de l'activité de service public de dépannage des véhicules en panne ou accidentés sur l'ensemble du domaine ainsi concédé - lequel, compte tenu des exigences de son exploitation, fait déjà l'objet d'une règlementation - ainsi que sur les aires de repos et de stationnement qui en sont des installations annexes, une restriction qui, en se limitant à l'obtention préalable d'un agrément délivré par le représentant de l'Etat dans les conditions prévues par les contrats de concession, a principalement pour objet de s'assurer que les entreprises sélectionnées seront en mesure de remplir leurs missions dans l'ensemble du périmètre de la concession, et répond aux objectifs de la sécurité routière sur des voies où les conditions de circulation conjuguent vitesse élevée et importance du trafic.</ANA>
<ANA ID="9D"> 14-02-02-02 Au titre de ses attributions de police générale, il appartient au Premier ministre d'adopter par voie règlementaire les mesures propres à assurer la sécurité des personnes sur les autoroutes et les ouvrages d'art concédés du réseau routier national.... ,,Le Premier ministre a ainsi pu apporter à l'exercice de l'activité de service public de dépannage des véhicules en panne ou accidentés sur l'ensemble du domaine ainsi concédé - lequel, compte tenu des exigences de son exploitation, fait déjà l'objet d'une règlementation - ainsi que sur les aires de repos et de stationnement qui en sont des installations annexes, une restriction qui, en se limitant à l'obtention préalable d'un agrément délivré par le représentant de l'Etat dans les conditions prévues par les contrats de concession, a principalement pour objet de s'assurer que les entreprises sélectionnées seront en mesure de remplir leurs missions dans l'ensemble du périmètre de la concession, et répond aux objectifs de la sécurité routière sur des voies où les conditions de circulation conjuguent vitesse élevée et importance du trafic.</ANA>
<ANA ID="9E"> 49-02-02 Au titre de ses attributions de police générale, il appartient au Premier ministre d'adopter par voie règlementaire les mesures propres à assurer la sécurité des personnes sur les autoroutes et les ouvrages d'art concédés du réseau routier national.... ,,Le Premier ministre a ainsi pu apporter à l'exercice de l'activité de service public de dépannage des véhicules en panne ou accidentés sur l'ensemble du domaine ainsi concédé - lequel, compte tenu des exigences de son exploitation, fait déjà l'objet d'une règlementation - ainsi que sur les aires de repos et de stationnement qui en sont des installations annexes, une restriction qui, en se limitant à l'obtention préalable d'un agrément délivré par le représentant de l'Etat dans les conditions prévues par les contrats de concession, a principalement pour objet de s'assurer que les entreprises sélectionnées seront en mesure de remplir leurs missions dans l'ensemble du périmètre de la concession, et répond aux objectifs de la sécurité routière sur des voies où les conditions de circulation conjuguent vitesse élevée et importance du trafic.</ANA>
<ANA ID="9F"> 65-02 Au titre de ses attributions de police générale, il appartient au Premier ministre d'adopter par voie règlementaire les mesures propres à assurer la sécurité des personnes sur les autoroutes et les ouvrages d'art concédés du réseau routier national.... ,,Le Premier ministre a ainsi pu apporter à l'exercice de l'activité de service public de dépannage des véhicules en panne ou accidentés sur l'ensemble du domaine ainsi concédé - lequel, compte tenu des exigences de son exploitation, fait déjà l'objet d'une règlementation - ainsi que sur les aires de repos et de stationnement qui en sont des installations annexes, une restriction qui, en se limitant à l'obtention préalable d'un agrément délivré par le représentant de l'Etat dans les conditions prévues par les contrats de concession, a principalement pour objet de s'assurer que les entreprises sélectionnées seront en mesure de remplir leurs missions dans l'ensemble du périmètre de la concession, et répond aux objectifs de la sécurité routière sur des voies où les conditions de circulation conjuguent vitesse élevée et importance du trafic.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 8 août 1919, Labonne, n° 56377, p. 737.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
