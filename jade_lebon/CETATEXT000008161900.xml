<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000008161900</ID>
<ANCIEN_ID>JG_L_2005_01_000000276018</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/08/16/19/CETATEXT000008161900.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 18/01/2005, 276018</TITRE>
<DATE_DEC>2005-01-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>276018</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Stirn</PRESIDENT>
<AVOCATS>SPINOSI</AVOCATS>
<RAPPORTEUR>M. Bernard  Stirn</RAPPORTEUR>
<COMMISSAIRE_GVT/>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 29 décembre 2004 au secrétariat du contentieux du Conseil d'Etat, présentée pour la SECTION FRANCAISE DE L'OBSERVATOIRE INTERNATIONAL DES PRISONS, dont le siège est 31 rue des Lilas à Paris (75019) ; elle demande au juge des référés du Conseil d'Etat :<br/>
<br/>
              1°) de suspendre, sur le fondement de l'article L. 521-1 du code de justice administrative, l'exécution de la circulaire du garde des sceaux, ministre de la justice du 18 novembre 2004 relative à l'organisation des escortes pénitentiaires des détenus faisant l'objet d'une consultation médicale ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
              elle soutient qu'elle a intérêt à agir dès lors que, d'une part, la décision contestée présente un caractère impératif et que, d'autre part, elle porte atteinte aux droits des détenus qu'elle a pour objet de défendre ; qu'il y a urgence ; qu'en effet, la circulaire attaquée, qui tend à soumettre les détenus à des mesures de surveillance et de contrainte renforcées lors des extractions médicales, a déjà été diffusée dans les services déconcentrés et concerne un nombre important de détenus ; qu'elle risque d'entraîner l'annulation de nombreux examens médicaux, les détenus refusant l'extraction médicale et les médecins s'opposant à dispenser des soins dans de telles conditions ; que l'impératif de santé publique commande en conséquence sa suspension ; qu'au surplus, l'application de la circulaire présente des difficultés en matière de sécurité des détenus ; que plusieurs moyens sont, en l'état de l'instruction, susceptibles de créer un doute sérieux quant à la légalité de la décision litigieuse ; que la mesure contestée, qui instaure trois niveaux de surveillance, méconnaît les dispositions des articles D. 294 et D. 283-1 du code de procédure pénale en ne limitant pas les moyens de contrainte aux seules opérations de transfèrement et d'extraction mais en les étendant aux consultations médicales dès le niveau I de surveillance ; que la circulaire, qui risque d'aboutir au menottage systématique des détenus pendant la consultation, est disproportionnée au regard des exigences de sécurité publique ; qu'elle méconnaît les dispositions de l'article D. 397 dès lors qu'elle rend impossible le respect du principe de confidentialité de l'examen médical des détenus dans le cadre des niveaux II et III de surveillance qui prévoient la présence d'un surveillant à l'intérieur du local de consultation ; que l'utilisation de moyens de contrainte à l'égard des personnes qui reçoivent des soins, sans que les médecins puissent en ordonner le retrait, est susceptible de constituer un traitement inhumain ou dégradant au sens de l'article 3 de la convention européenne des droits de l'homme, notamment dans le cadre du niveau III de surveillance ; que la possibilité donnée aux chefs d'établissement de menotter certains détenus « dans le dos » risque d'être utilisée de manière systématique et de donner également lieu à de tels traitements ; que ces dispositions de la circulaire, qui excèdent ce qui est nécessaire à la sauvegarde de l'ordre public, sont contraires à l'article D. 242 du code de procédure pénale ; que, s'agissant des mesures de surveillance et de contraintes applicables aux détenus munis de béquilles, la circulaire est disproportionnée et contraire à l'article 3 de la convention européenne des droits de l'homme ; <br/>
<br/>
<br/>
      Vu la circulaire dont la suspension est demandée ;<br/>
<br/>
              Vu la copie de la requête tendant à l'annulation de cette circulaire ; <br/>
<br/>
              Vu le mémoire en défense enregistré le 14 janvier 2005, présentée par le garde des sceaux, ministre de la justice ; il tend au rejet de la requête ; il soutient que la condition d'urgence n'est pas remplie ; qu'en effet, la circulaire contestée n'impose pas de mesure de contrainte et de surveillance nouvelles mais vise à rappeler dans un document unique l'ensemble des instructions données depuis juillet 2003 ; que l'application de telles règles de sécurité ne remet pas en cause l'accès au soin des détenus ; qu'elle ne donne lieu à aucune difficulté particulière ; qu'il n'existe pas, en l'état de l'instruction, de doute sérieux quant à la légalité de la décision contestée ; qu'en vertu de l'article D. 291 du code de procédure pénale, le garde des sceaux, ministre de la justice est compétent pour donner des instructions de sécurité à respecter lors des consultations médicales ; que la circulaire, qui ne préconise pas l'usage systématique de moyens de contrainte lors des examens et soins médicaux, n'est pas disproportionnée ; que le port de menottes par des personnes qui reçoivent des soins ne constitue pas en lui-même un traitement inhumain et dégradant au sens de l'article 3 de la convention européenne de sauvegarde des droits de l'homme ; que la confidentialité de l'entretien est respectée dans les rares cas de mise en oeuvre des niveaux II et III de surveillance ; que le moyen tiré de la méconnaissance de l'article D. 242 du code de procédure pénale, qui ne concerne que la police intérieure de l'établissement pénitentiaire, est inopérant ; que les dispositions de la circulaire n'imposent pas le port systématique de menottes dans le dos ; qu'une telle pratique ne constitue pas en elle-même un traitement inhumain et dégradant ; que les mesures de surveillance et de contrainte applicables aux détenus munis de béquilles sont proportionnées et ne sont pas contraires à l'article 3 de la convention européenne des droits de l'homme ; <br/>
<br/>
              Vu les observations complémentaires enregistrées la 17 janvier 2005, présentées pour la SECTION FRANCAISE DE L'OBSERVATOIRE INTERNATIONAL DES PRISONS ; elles tendent aux mêmes fins que la requête, par les mêmes moyens ;<br/>
<br/>
<br/>
	Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
	Vu le code de procédure pénale ; <br/>
	Vu les autres pièces du dossier ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, la SECTION FRANCAISE DE L'OBSERVATOIRE INTERNATIONAL DES PRISONS, d'autre part, le garde des sceaux, ministre de la justice ;<br/>
              Vu le procès verbal de l'audience publique du 18 janvier 2005 à 10 heures 30 au cours de laquelle ont été entendus :<br/>
<br/>
              - Me SPINOSI, avocat au Conseil d'Etat et à la Cour de cassation, avocat de la SECTION FRANCAISE DE L'OBSERVATOIRE INTERNATIONAL DES PRISONS ;<br/>
<br/>
              - le représentant de la SECTION FRANCAISE DE L'OBSERVATOIRE INTERNATIONAL DES PRISONS ;<br/>
<br/>
              - les représentants du garde des sceaux, ministre de la justice ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant qu'en vertu de l'article L. 521-1 du code de justice administrative, le juge des référés peut ordonner la suspension d'une décision administrative lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de cette décision ;<br/>
<br/>
              Considérant que le premier alinéa de l'article 803 du code de procédure pénale dispose que : « Nul ne peut être soumis au port des menottes ou des entraves que s'il est considéré soit comme dangereux pour autrui ou pour lui-même, soit comme susceptible de tenter de prendre la fuite » ; qu'aux termes de l'article D. 291 de ce code « l'extraction est l'opération par laquelle un détenu est conduit sous surveillance en dehors de l'établissement de détention, lorsqu'il doit comparaître en justice, ou lorsqu'il doit recevoir des soins qu'il n'est pas possible de lui donner dans l'établissement pénitentiaire... » ; que, pour l'application de ces dispositions, les mesures de surveillance et de contrainte mises en oeuvre par l'administration pénitentiaire doivent d'une part être proportionnées aux dangers qui apparaissent dans chaque cas particulier, d'autre part assurer en toute hypothèse la confidentialité des relations entre les détenus et les médecins qu'ils consultent ;<br/>
<br/>
              Considérant que, par la circulaire du 18 novembre 2004 dont la SECTION FRANCAISE DE L'OBSERVATOIRE INTERNATIONAL DES PRISONS demande la suspension, le garde des sceaux, ministre de la justice a donné aux services de l'administration pénitentiaire des instructions relatives à l'organisation des escortes pénitentiaires des détenus qui font l'objet d'une extraction en vue d'une consultation médicale à l'extérieur d'un établissement pénitentiaire ; que cette circulaire prévoit que les moyens de contrainte et de surveillance, et notamment le recours aux menottes ou aux entraves, doivent être déterminés, dans chaque cas, en fonction des dangers qui résultent de la personnalité et du comportement du détenu concerné ; qu'elle ne recommande le recours à la pose des menottes dans le dos qu'en cas de risque particulier d'évasion ou de trouble à l'ordre public ; qu'elle indique que, quel que soit le niveau de surveillance retenu, les mesures de sécurité ne doivent pas porter atteinte à la confidentialité de l'entretien médical ; qu'il a été confirmé au cours de l'audience publique que l'administration pénitentiaire veillait au respect de cette confidentialité par des dispositifs appropriés, indépendamment des mesures particulières prévues par la circulaire contestée pour exclure toute surveillance à l'intérieur d'une salle d'accouchement ; que, dans ces conditions, et en l'état de l'instruction, les moyens tirés de ce que la circulaire contestée énoncerait des prescriptions impératives contraires à la règle énoncée par l'article 803 du code de procédure pénale, entraînerait une méconnaissance du secret médical, excéderait les mesures qui peuvent être ordonnées dans le cadre d'une extraction ou serait incompatible avec les exigences de proportionnalité entre les mesures de contrainte imposées aux détenus et les risques encourus qui découlent tant des exigences du droit interne que de l'application des stipulations de l'article 3 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ne sont pas de nature à créer un doute sérieux quant à la légalité de la circulaire en question ; que la requête à fin de suspension de cette circulaire, y compris ses conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative, ne peut donc qu'être rejetée ;<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de la SECTION FRANCAISE DE L'OBSERVATOIRE INTERNATIONAL DES PRISONS est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à la SECTION FRANCAISE DE L'OBSERVATOIRE INTERNATIONAL DES PRISONS et au garde des sceaux, ministre de la justice.<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">37-05-02-01 JURIDICTIONS ADMINISTRATIVES ET JUDICIAIRES. EXÉCUTION DES JUGEMENTS. EXÉCUTION DES PEINES. SERVICE PUBLIC PÉNITENTIAIRE. - EXTRACTION DES DÉTENUS (ART D. 291 DU CODE DE PROCÉDURE PÉNALE) - MESURES DE SURVEILLANCE ET DE CONTRAINTE - LÉGALITÉ - CONDITIONS.
</SCT>
<ANA ID="9A"> 37-05-02-01 Les mesures de surveillance et de contrainte mises en oeuvre par l'administration pénitentiaire lors de l'extraction d'un détenu, réalisée en application de l'article D. 291 du code de procédure pénale, doivent d'une part être proportionnées aux dangers qui apparaissent dans chaque cas particulier, d'autre part assurer en toute hypothèse la confidentialité des relations entre les détenus et les médecins qu'ils consultent.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
