<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029998369</ID>
<ANCIEN_ID>JG_L_2014_12_000000360850</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/99/83/CETATEXT000029998369.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème / 10ème SSR, 30/12/2014, 360850</TITRE>
<DATE_DEC>2014-12-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>360850</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème / 10ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN ; SCP BARADUC, DUHAMEL, RAMEIX</AVOCATS>
<RAPPORTEUR>Mme Maïlys Lange</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Aladjidi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:360850.20141230</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 9 juillet et 10 octobre 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour la société Groupe Patrice Pichet, dont le siège est 20-24 avenue de Canteranne à Pessac (33600) ; la société Groupe Patrice Pichet demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 11BX00970 du 10 mai 2012 par lequel la cour administrative d'appel de Bordeaux a, sur la requête de la commune de Biarritz, annulé le jugement n° 0901094 du 22 février 2011 du tribunal administratif de Pau la déchargeant de la somme de 30 451,56 euros correspondant au montant d'une participation à la réalisation de deux places de stationnement, puis a rejeté ses conclusions présentées devant le tribunal administratif ainsi que son appel incident tendant au versement d'intérêts moratoires sur la somme remboursée par la commune ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de la commune de Biarritz et de faire droit à son appel incident ; <br/>
<br/>
              3°) de mettre à la charge de la commune de Biarritz la somme de 7 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'urbanisme ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Maïlys Lange, Auditeur,  <br/>
<br/>
              - les conclusions de M. Frédéric Aladjidi, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de la Société Groupe Patrice Pichet et à la SCP Baraduc, Duhamel, Rameix, avocat de la mairie de Biarritz ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 20 avril 2007, le maire de Biarritz a délivré à la société Groupe Patrice Pichet un permis de construire pour la transformation en une agence immobilière d'un local antérieurement occupé par un traiteur ; que ce permis prescrivait au pétitionnaire le versement d'une participation pour non-réalisation de deux places de stationnement, en application de l'article UA 12 du règlement du plan local d'urbanisme ; que la société Groupe Patrice Pichet se pourvoit en cassation contre l'arrêt du 10 mai 2012 par lequel la cour administrative d'appel de Bordeaux a annulé le jugement du 22 février 2011 du tribunal administratif de Pau qui l'avait déchargée de la somme correspondant au montant de cette participation et l'avait remise à sa charge, avant de rejeter son appel incident tendant au versement d'intérêts moratoires sur la somme remboursée par la commune à la suite du jugement du tribunal administratif ;<br/>
<br/>
              2. Considérant qu'en vertu de l'article L. 123-1 du code de l'urbanisme, dans sa rédaction applicable à la participation en litige, repris désormais à l'article L. 123-1-5 du même code, les plans locaux d'urbanisme peuvent définir, en fonction des situations locales, les règles concernant la destination et la nature des constructions autorisées ; que le 12° de l'article R. 123-9 du même code prévoit que le règlement peut comprendre les obligations imposées aux constructeurs en matière de réalisation d'aires de stationnement ; que ce même article dispose que les règles qu'il édicte " peuvent être différentes, dans une même zone, selon que les constructions sont destinées à l'habitation, à l'hébergement hôtelier, aux bureaux, au commerce, à l'artisanat, à l'industrie, à l'exploitation agricole ou forestière ou à la fonction d'entrepôt " ; que s'il est loisible aux auteurs des plans locaux d'urbanisme de préciser, pour des motifs d'urbanisme et sous le contrôle du juge, le contenu des catégories énumérées à l'article R. 123-9, les dispositions de cet article ne leur permettent, toutefois, ni de créer de nouvelles catégories de destination pour lesquelles seraient prévues des règles spécifiques, ni de soumettre certains des locaux relevant de l'une des catégories qu'il énumère aux règles applicables à une autre catégorie ;<br/>
<br/>
              3. Considérant qu'aux termes de l'article UA 12 du règlement du plan local d'urbanisme de Biarritz, adopté sur le fondement des dispositions précitées du code de l'urbanisme : " Pour les changements d'affectation de locaux autres que les hôtels et les résidences de tourisme, la reconstruction de locaux après sinistre, les aménagements et les extensions de bâtiment, il ne sera exigé de places de stationnement que pour les besoins nouveaux engendrés par les projets, à moins que le nombre des aires existantes soit supérieur aux besoins existants. / Nombre d'aires de stationnement : (...) e- commerces, - 1 place pour 60 m2 de surface hors oeuvre nette, avec au minimum une place par commerce. / f-bureaux, services (y compris les agences bancaires, bureaux d'assurance), restaurants, - 1 place pour 30 m2 de surface hors oeuvre nette, avec un minimum d'une place par activité. / (...) j-autres catégories : la détermination du nombre d'aires de stationnement applicable aux constructions dont la catégorie n'est pas désignée ci-dessus, sera définie par référence à la catégorie la plus proche énoncée au règlement " ;<br/>
<br/>
              4. Considérant que, par ces dispositions et contrairement à ce que soutient la commune de Biarritz, le plan local d'urbanisme n'a pas, en soumettant les " services (y compris les agences bancaires, bureaux d'assurance) " et les " restaurants " aux règles relatives au nombre d'aires de stationnement applicables à la catégorie des bureaux, précisé le contenu de cette catégorie, mais a créé une catégorie nouvelle, pour partie constituée de locaux relevant de la destination " commerce " au sens de l'article R. 123-9 du code de l'urbanisme précité ; qu'il suit de là que l'article UA 12 du règlement de ce plan méconnaît ce même article R. 123-9 qui, comme il a été dit, fixe de manière limitative les catégories de destinations pouvant être soumises à des règles différentes au sein d'une même zone ; que, par suite, en écartant l'exception d'illégalité soulevée par la société Groupe Patrice Pichet tirée de ce que l'article UA 12 du règlement du plan local d'urbanisme de Biarritz ne pouvait, sans méconnaître l'article R. 123-9 du code de l'urbanisme, prévoir des règles différentes pour des constructions étant toutes le siège d'activités commerciales, la cour administrative d'appel de Bordeaux a commis une erreur de droit ; que, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, son arrêt doit être annulé ;<br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de Biarritz la somme de 3 500 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ; que ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de la société Groupe Patrice Pichet, qui n'est pas, dans la présente instance, la partie perdante ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Bordeaux du 10 mai 2012 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Bordeaux. <br/>
Article 3 : La commune de Biarritz versera à la société Groupe Patrice Pichet la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions de la commune de Biarritz présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à la société Groupe Patrice Pichet et à la commune de Biarritz.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">68-01-01-01-03-01 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PLANS D'AMÉNAGEMENT ET D'URBANISME. PLANS D`OCCUPATION DES SOLS (POS) ET PLANS LOCAUX D'URBANISME (PLU). LÉGALITÉ DES PLANS. LÉGALITÉ INTERNE. PRESCRIPTIONS POUVANT LÉGALEMENT FIGURER DANS UN POS OU UN PLU. - OBLIGATIONS IMPOSÉES AUX CONSTRUCTEURS EN MATIÈRE DE RÉALISATION D'AIRES DE STATIONNEMENT - POSSIBILITÉ DE DIFFÉRENCIER LES RÈGLES SELON LA DESTINATION DES CONSTRUCTIONS - CATÉGORIES DE DESTINATIONS ÉNUMÉRÉES AU 12° DE L'ART. R. 123-9 DU CODE DE L'URBANISME - POSSIBILITÉ DE PRÉCISER LE CONTENU DE CES CATÉGORIES - EXISTENCE - POSSIBILITÉ DE CRÉER DE NOUVELLES CATÉGORIES AVEC DES RÈGLES SPÉCIFIQUES OU DE SOUMETTRE DES LOCAUX DE L'UNE DES CATÉGORIES AUX RÈGLES APPLICABLES À UNE AUTRE CATÉGORIE - ABSENCE.
</SCT>
<ANA ID="9A"> 68-01-01-01-03-01 En vertu de l'article L. 123-1 du code de l'urbanisme, repris désormais à l'article L . 123-1-5 du même code, les plans locaux d'urbanisme peuvent définir, en fonction des situations locales, les règles concernant la destination et la nature des constructions autorisées. Le 12° de l'article R. 123-9 du même code prévoit que le règlement peut comprendre les obligations imposées aux constructeurs en matière de réalisation d'aires de stationnement. Ce même article dispose que les règles qu'il édicte peuvent être différentes, dans une même zone, selon que les constructions sont destinées à l'habitation, à l'hébergement hôtelier, aux bureaux, au commerce, à l'artisanat, à l'industrie, à l'exploitation agricole ou forestière ou à la fonction d'entrepôt. S'il est loisible aux auteurs des plans locaux d'urbanisme de préciser, pour des motifs d'urbanisme et sous le contrôle du juge, le contenu des catégories énumérées à l'article R. 123-9, les dispositions de cet article ne leur permettent, toutefois, ni de créer de nouvelles catégories de destination pour lesquelles seraient prévues des règles spécifiques, ni de soumettre certains des locaux relevant de l'une des catégories qu'il énumère aux règles applicables à une autre catégorie.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
