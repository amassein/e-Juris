<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039640724</ID>
<ANCIEN_ID>JG_L_2019_12_000000432590</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/64/07/CETATEXT000039640724.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 18/12/2019, 432590</TITRE>
<DATE_DEC>2019-12-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>432590</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP POTIER DE LA VARDE, BUK LAMENT, ROBILLOT ; SCP RICHARD</AVOCATS>
<RAPPORTEUR>Mme Cécile Renault</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Mireille Le Corre</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:432590.20191218</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. E... A... D... a demandé au juge des référés du tribunal administratif de Nouvelle-Calédonie, sur le fondement de l'article L. 551-24 du code de justice administrative, premièrement, d'enjoindre au port autonome de la Nouvelle-Calédonie (PANC) de différer la signature de la délégation de service public sur la gestion du port de plaisance dit " Sunset Marina ", deuxièmement, de suspendre l'exécution de toute décision se rapportant à cette passation jusqu'au terme de l'instance, troisièmement, de communiquer les motifs de rejet de l'offre de la société Sunset Marina ainsi que du choix de l'offre présentée par la société d'économie mixte de la baie de la Moselle (SODEMO), quatrièmement, d'annuler la décision rejetant l'offre de la société Sunset Marina et, plus largement, la procédure de passation de la délégation de service public en litige et, enfin, d'ordonner au PANC de régulariser les éléments du dossier de consultation des entreprises et de reprendre l'ensemble de la procédure de mise en concurrence. <br/>
<br/>
              Par une ordonnance n° 1900268 du 27 juin 2019, le juge des référés du tribunal administratif de Nouvelle-Calédonie a annulé la procédure de passation de la délégation de service public et enjoint le PANC, s'il entendait la conclure, de reprendre l'ensemble de la procédure en litige.<br/>
<br/>
<br/>
<br/>
              1° Sous le numéro 432590, par un pourvoi sommaire, un mémoire complémentaire et deux nouveaux mémoires, enregistrés les 15, 29 juillet, 9 octobre et 28 novembre 2019 au secrétariat du contentieux du Conseil d'Etat, le port autonome de la Nouvelle-Calédonie demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de rejeter la demande de M. A... D... ;<br/>
<br/>
              3°) de mettre à la charge de M. A... D..., la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              2° Sous le numéro 432782, par un pourvoi et un mémoire en réplique, enregistrés les 19 juillet et 7 octobre 2019 au secrétariat du contentieux du Conseil d'Etat, la société d'économie mixte de la baie de la Moselle (SODEMO) demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler la même ordonnance ;<br/>
<br/>
              2°) statuant en référé, de rejeter la demande de M. A... D... ;<br/>
<br/>
              3°) de mettre à la charge de M. A... D..., la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - la loi organique n° 99-209 et la loi n° 99-210 du 19 mars 1999 ;<br/>
              - le décret n° 2016-86 du 1er février 2016 ;<br/>
              - l'arrêté n° 2002-1571/GNC du 30 mai 2002 fixant le modèle type des concessions de port de plaisance prévues dans la loi du pays n° 2001-017 du 11 janvier 2002 sur le domaine public maritime de la Nouvelle-Calédonie et des provinces ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme B... C..., auditrice,  <br/>
<br/>
              - les conclusions de Mme Mireille Le Corre, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Potier de la Varde, Buk Lament, Robillot, avocat du port autonome de la Nouvelle-Caledonie, à la SCP Richard, avocat de M. A... D... et à la SCP Piwnica, Molinié, avocat de la société d'économie mixte de la baie de la Moselle ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Les pourvois du port autonome de la Nouvelle-Calédonie (PANC) et de la société d'économie mixte de la baie de la Moselle (SODEMO) sont dirigés contre la même ordonnance. Il y a lieu de les joindre pour statuer par une seule décision. <br/>
<br/>
              2. Aux termes de l'article L. 551-24 du code de justice administrative : " En Nouvelle-Calédonie, en Polynésie française et dans les îles Wallis et Futuna, le président du tribunal administratif, ou le magistrat qu'il délègue, peut être saisi en cas de manquement aux obligations de publicité et de mise en concurrence auxquelles est soumise la passation des marchés et contrats publics en vertu de dispositions applicables localement. Le président du tribunal administratif peut être saisi avant la conclusion du contrat. Il peut ordonner à l'auteur du manquement de se conformer à ses obligations et suspendre la passation du contrat ou l'exécution de toute décision qui s'y rapporte. Il peut également annuler ces décisions et supprimer les clauses ou prescriptions destinées à figurer dans le contrat et qui méconnaissent lesdites obligations. Dès qu'il est saisi, il peut enjoindre de différer la signature du contrat jusqu'au terme de la procédure et pour une durée maximum de vingt jours. Le président du tribunal administratif ou son délégué statue en premier et dernier ressort en la forme des référés ".<br/>
              3. Il ressort des pièces du dossier soumis au juge des référés que le PANC, établissement public à caractère industriel et commercial territorial, a organisé une procédure de publicité et de mise en concurrence, sur le fondement des dispositions de l'article 92 de la loi organique relative à la Nouvelle-Calédonie, afin de conclure une délégation de service public pour l'exploitation du port de plaisance dit " Sunset Marina ". Lors de sa séance du 21 février 2019, la commission de délégation de service public a classé l'offre de la SODEMO en première position et celle de la société Sunset Marina en seconde position. Par lettre du 6 juin 2019, l'établissement public portuaire a informé la société Sunset Marina du rejet de son offre. Saisi sur le fondement de l'article L. 521-24 du code de justice administrative par la société Sunset Marina, le juge du référé précontractuel du tribunal administratif de Nouvelle-Calédonie, a, par une ordonnance du 27 juin 2019 contre laquelle le PANC et la SODEMO se pourvoient en cassation, annulé la procédure de passation de la délégation de service public au motif que le PANC avait manqué au principe d'impartialité en favorisant délibérément un candidat et enjoint au port autonome, s'il entendait la conclure, de reprendre l'ensemble de la procédure en litige.<br/>
<br/>
              4. Au nombre des principes généraux du droit qui s'imposent à l'autorité concédante comme à toute autorité administrative figure le principe d'impartialité, dont la méconnaissance est constitutive d'un manquement aux obligations de publicité et de mise en concurrence. <br/>
<br/>
              5. Pour caractériser l'existence d'un manquement du PANC au principe d'impartialité, le juge du référé précontractuel s'est fondé sur la circonstance qu'il avait reporté la date limite de remise des offres à la demande de la SODEMO, alors que cette société, dont il détient 11,43% du capital, n'avait pas sollicité de renseignements complémentaires pour la remise de son offre. Toutefois, d'une part, le principe d'impartialité ne fait pas obstacle à ce qu'un acheteur public attribue un contrat de délégation de service public à une société d'économie mixte locale dont il est actionnaire, sous réserve que la procédure garantisse l'égalité de traitement entre les candidats et que soit prévenu tout risque de conflit d'intérêts. D'autre part, la seule circonstance qu'un candidat se soit abstenu de solliciter des renseignements complémentaires avant le délai de remise des offres n'est pas de nature à faire obstacle à ce que l'autorité concédante décide que des raisons objectives justifient la prolongation de ce délai. Dès lors, en déduisant des seuls éléments mentionnés ci-dessus que le PANC avait manqué au principe d'impartialité, le juge du référé précontractuel du tribunal administratif de Nouvelle-Calédonie a inexactement qualifié les faits qui lui étaient soumis.  <br/>
<br/>
              6. Il résulte de ce qui précède, sans qu'il soit besoin d'examiner les autres moyens des pourvois, que l'ordonnance attaquée doit être annulée.<br/>
<br/>
              7. Dans les circonstances de l'espèce, il y a lieu, en application des dispositions de l'article L. 821-2 du code de justice administrative, de régler l'affaire au titre de la procédure de référé engagée.<br/>
<br/>
              Sur la régularité de la procédure de passation :<br/>
<br/>
              8. En premier lieu, ainsi qu'il a été dit au point 5, la circonstance que la SODEMO soit une société d'économie mixte locale dont le PANC est actionnaire ne fait pas par elle-même obstacle à ce qu'une délégation de service public lui soit attribuée. Le fait que certains membres du conseil d'administration du PANC soient également administrateurs de la SODEMO n'a, en l'espèce, pas conduit à une situation de conflit d'intérêts, dès lors qu'il résulte de l'instruction que, lors de la réunion du 10 avril 2019 au cours de laquelle le conseil d'administration du PANC a approuvé l'attribution de la délégation de service public litigieuse à la SODEMO, les deux membres du conseil également administrateurs de la SODEMO n'ont participé ni  aux débats ni  aux votes sur ce point. Par ailleurs, il résulte de l'instruction que, si le PANC a été saisi d'une demande de prolongation du délai de remise des offres par la SODEMO, une telle prolongation était objectivement justifiée par la nécessité d'assurer une information égale des candidats, que ceux-ci ont l'un et l'autre bénéficié de ce nouveau délai de remise des offres initiales et ont accepté l'un et l'autre de participer à la négociation qui a ensuite été engagée. Dans ces conditions, les moyens tirés de ce que la procédure de passation de la délégation de service public aurait méconnu le principe d'impartialité doivent être écartés.  <br/>
<br/>
              9. En deuxième lieu, aux termes de l'article 92 de la loi organique du 19 mars 1999 relative à la Nouvelle-Calédonie : " Les articles L. 1411-1 à L. 1411-3 du code général des collectivités territoriales sont applicables aux délégations de service public de la Nouvelle-Calédonie, de ses établissements publics et des syndicats mixtes auxquels elle participe. / Les assemblées délibérantes de ces personnes morales de droit public (...) sont saisies, après une procédure de publicité et de recueil d'offres dans les conditions prévues aux troisième et quatrième alinéas de l'article L. 1411-1 du même code, et l'avis d'une commission élue en leur sein à la représentation proportionnelle au plus fort reste, du choix proposé par l'autorité habilitée à signer la convention parmi les entreprises qui ont présenté une offre./ Elles se prononcent deux mois au moins après la saisine de la commission. Les documents sur lesquels elles se prononcent leur sont transmis au moins quinze jours avant leur délibération ". Le moyen tiré de ce que le délai de deux mois prévu par ces dispositions aurait été méconnu ne peut en tout état de cause qu'être écarté en l'absence de toute atteinte sur ce point à l'égalité de traitement des candidats. Au demeurant, il résulte de l'instruction que les offres ont été remises par les candidats le 12 novembre 2018 et que le conseil d'administration du PANC s'est réuni pour choisir le nouveau délégataire le 10 avril 2019, plus de deux mois après la saisine de la commission de délégation de service public.  <br/>
<br/>
              10. En troisième lieu, il résulte de l'instruction que le dossier de consultation comprenait un projet de contrat et qu'en vertu de l'article 4 du règlement de consultation les candidats devaient remettre " le projet de contrat, daté et signé ". Il ne résulte pas de l'instruction et n'est d'ailleurs pas soutenu que la SODEMO aurait remis une offre qui ne comportait pas ce projet de contrat daté et signé. La circonstance qu'elle ait proposé, dans le cadre des négociations ultérieures avec l'autorité délégante, de compléter le projet de contrat figurant au règlement de consultation en se référant au  modèle de contrat de concession de port de plaisance approuvé par un arrêté n° 2002/1571/GNC et paru au Journal officiel de la Nouvelle-Calédonie, sans que le PANC n'informe le requérant de cette proposition, ne saurait caractériser une méconnaissance du principe d'égalité de traitement, eu égard à la marge de négociation dont l'autorité délégante dispose avec les candidats à une délégation de service public. Dès lors, le moyen tiré de ce que le PANC aurait, en favorisant un support contractuel qu'il n'avait pas proposé, méconnu les principes de la commande publique doit être écarté.<br/>
<br/>
              11. En quatrième lieu, aux termes de l'article 6 du règlement de la consultation les critères d'appréciation des offres sont au nombre de quatre : " Pertinence et adéquation du projet avec le cahier des charges / Qualités de fond et de forme du dossier présenté / Qualité du dossier technique proposé / Calendrier proposé ". Il résulte de l'instruction que le PANC a choisi la SODEMO notamment en raison de la compétence de son équipe, tant pour la gestion que pour la maintenance. Contrairement à ce qui est soutenu, de tels motifs peuvent être pris en compte pour apprécier la qualité du dossier technique et financier proposé. Dès lors, le moyen tiré de ce que le PANC a choisi la SODEMO en se fondant sur des critères relatifs à l'examen des candidatures et non des offres doit être écarté.<br/>
<br/>
              12. En dernier lieu, aux termes du I de l'article 29 du décret du 1er février 2016 relatif aux contrats de concession : " (...) l'autorité concédante, dès qu'elle a fait son choix pour une candidature ou une offre, notifie à tous les autres candidats et soumissionnaires le rejet de leur candidature ou de leur offre. Cette notification précise les motifs de ce rejet et, pour les soumissionnaires, le nom du ou des attributaires ainsi que les motifs qui ont conduit au choix de l'offre. / Un délai d'au moins seize jours est respecté entre la date d'envoi de la notification et la date de conclusion du contrat de concession. Ce délai est réduit à au moins onze jours en cas de transmission électronique de cette notification à l'ensemble des candidats et soumissionnaires intéressés ". Aux termes de l'article 44 du même décret : " Le présent décret est applicable en Nouvelle-Calédonie aux contrats de concession, définis à la section 1 du chapitre Ier du titre Ier de l'ordonnance du 29 janvier 2016 susvisée, conclus par l'Etat et ses établissements publics ". Il ne résulte ni de ces dispositions, applicables en Nouvelle-Calédonie aux seuls contrats conclus par l'Etat ou ses établissements publics, ni d'aucun principe que s'imposerait à un établissement public territorial de Nouvelle-Calédonie qui attribue un contrat de concession l'obligation d'informer les candidats évincés du rejet de leur offre ou des motifs de ce rejet ni de respecter un délai entre cette information et la date de conclusion du contrat. Dès lors, les moyens tirés, d'une part, de ce que la notification reçue par M. A... D... ne comportait aucune précision sur les motifs du rejet de son offre et que ce défaut d'information ne pouvait être pallié, en raison de l'absence de publication de la délibération du conseil d'administration du PANC portant approbation du choix du délégataire, et, d'autre part, de ce que le délai de suspension de la signature du contrat n'aurait pas été respecté, ne peuvent qu'être écartés.<br/>
<br/>
              13. Il résulte de tout ce qui précède que, sans qu'il soit besoin de statuer sur leur recevabilité, les conclusions de M. A... D... dirigées contre la procédure de passation du contrat doivent être rejetées.<br/>
<br/>
              Sur les conclusions aux fins d'injonction :<br/>
<br/>
              14. M. A... D... demande également que soit ordonné au PANC de communiquer les motifs de rejet de l'offre de la société Sunset Marina ainsi que du choix de l'offre présentée par la SODEMO. Toutefois il n'entre pas dans l'office du juge du référé précontractuel d'ordonner la communication de ces documents. <br/>
<br/>
              Sur les frais d'instance :<br/>
<br/>
              15. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de M. A... D... le versement d'une somme de 3 000 euros au PANC et de la même somme à la SODEMO, au titre des frais exposés au titre de l'ensemble de la procédure. Les dispositions de cet article font en revanche obstacle à ce qu'une somme soit mise à la charge du PANC et de la SODEMO, qui ne sont pas, dans la présente instance, les parties perdantes.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du 27 juin 2019 du tribunal administratif de Nouvelle-Calédonie est annulée. <br/>
Article 2 : La demande présentée par M. A... D... devant le tribunal administratif de Nouvelle-Calédonie et ses conclusions présentées devant le Conseil d'Etat au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 3 : M. A... D... versera au PANC et à la SODEMO une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 4 : La présente décision sera notifiée au port autonome de la Nouvelle-Calédonie, à la société d'économie mixte de la baie de la Moselle et à M. E... A... D....<br/>
Copie en sera adressée au haut-commissaire de la République en Nouvelle-Calédonie.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-04-03 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. PRINCIPES GÉNÉRAUX DU DROIT. - PRINCIPE D'IMPARTIALITÉ - PROCÉDURE DE PASSATION D'UNE DÉLÉGATION DE SERVICE PUBLIC - MÉCONNAISSANCE - 1) CIRCONSTANCE QUE LA PERSONNE PUBLIQUE EST ACTIONNAIRE DE L'UN DES CANDIDATS - ABSENCE - 2) DEMANDE PAR L'UN DES CANDIDATS DE PROLONGATION DU DÉLAI DE REMISE DES OFFRES - ABSENCE, EN L'ESPÈCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">39-02-02-01 MARCHÉS ET CONTRATS ADMINISTRATIFS. FORMATION DES CONTRATS ET MARCHÉS. MODE DE PASSATION DES CONTRATS. DÉLÉGATIONS DE SERVICE PUBLIC. - PROCÉDURE DE PASSATION - MÉCONNAISSANCE DU PRINCIPE D'IMPARTIALITÉ - 1) CIRCONSTANCE QUE LA PERSONNE PUBLIQUE EST ACTIONNAIRE DE L'UN DES CANDIDATS - ABSENCE - 2) DEMANDE PAR L'UN DES CANDIDATS DE PROLONGATION DU DÉLAI DE REMISE DES OFFRES - ABSENCE, EN L'ESPÈCE.
</SCT>
<ANA ID="9A"> 01-04-03 1) La circonstance que la société d'économie mixte de la baie de la Moselle (SODEMO) soit une société d'économie mixte locale dont le Port autonome de Nouvelle-Calédonie (PANC), autorité concédante, est actionnaire ne fait pas par elle-même obstacle à ce qu'une délégation de service public lui soit attribuée. Le fait que certains membres du conseil d'administration du PANC soient également administrateurs de la SODEMO n'a, en l'espèce, pas conduit à une situation de conflit d'intérêts, dès lors qu'il résulte de l'instruction que, lors de la réunion au cours de laquelle le conseil d'administration du PANC a approuvé l'attribution de la délégation de service public litigieuse à la SODEMO, les deux membres du conseil également administrateurs de la SODEMO n'ont participé ni aux débats ni aux votes sur ce point.... ,,2) Si le PANC a été saisi d'une demande de prolongation du délai de remise des offres par la SODEMO, une telle prolongation était objectivement justifiée par la nécessité d'assurer une information égale des candidats, que ceux-ci ont l'un et l'autre bénéficié de ce nouveau délai de remise des offres initiales et ont accepté l'un et l'autre de participer à la négociation qui a ensuite été engagée. Dans ces conditions, les moyens tirés de ce que la procédure de passation de la délégation de service public aurait méconnu le principe d'impartialité doivent être écartés.</ANA>
<ANA ID="9B"> 39-02-02-01 1) La circonstance que la société d'économie mixte de la baie de la Moselle (SODEMO) soit une société d'économie mixte locale dont le Port autonome de Nouvelle-Calédonie (PANC), autorité concédante, est actionnaire ne fait pas par elle-même obstacle à ce qu'une délégation de service public lui soit attribuée. Le fait que certains membres du conseil d'administration du PANC soient également administrateurs de la SODEMO n'a, en l'espèce, pas conduit à une situation de conflit d'intérêts, dès lors qu'il résulte de l'instruction que, lors de la réunion du 10 avril 2019 au cours de laquelle le conseil d'administration du PANC a approuvé l'attribution de la délégation de service public litigieuse à la SODEMO, les deux membres du conseil également administrateurs de la SODEMO n'ont participé ni aux débats ni aux votes sur ce point.... ,,2) Si le PANC a été saisi d'une demande de prolongation du délai de remise des offres par la SODEMO, une telle prolongation était objectivement justifiée par la nécessité d'assurer une information égale des candidats, que ceux-ci ont l'un et l'autre bénéficié de ce nouveau délai de remise des offres initiales et ont accepté l'un et l'autre de participer à la négociation qui a ensuite été engagée. Dans ces conditions, les moyens tirés de ce que la procédure de passation de la délégation de service public aurait méconnu le principe d'impartialité doivent être écartés.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
