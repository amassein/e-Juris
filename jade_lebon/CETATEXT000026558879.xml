<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026558879</ID>
<ANCIEN_ID>JG_L_2012_10_000000361327</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/55/88/CETATEXT000026558879.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème et 7ème sous-sections réunies, 29/10/2012, 361327</TITRE>
<DATE_DEC>2012-10-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>361327</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème et 7ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARTHELEMY, MATUCHANSKY, VEXLIARD</AVOCATS>
<RAPPORTEUR>Mme Sophie-Caroline de Margerie</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Damien Botteghi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:361327.20121029</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le mémoire, enregistré le 3 septembre 2012 au secrétariat du contentieux du Conseil d'Etat, présenté par l'Association Union des agents sportifs du football (UASF), dont le siège est chez CLK Foot 124 avenue Victor Hugo à Tulle (19000), représentée par son président en exercice, et par le Syndicat national des agents sportifs (SNAS), dont le siège est 11 rue Saint-Florentin à Paris (75008), représenté par son président en exercice, en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958 ; l'UASF et le SNAS demandent au Conseil d'Etat, à l'appui de leur requête tendant à l'annulation de la décision en date du 25 mai 2012 par laquelle le comité exécutif de la Fédération française de football a modifié les dispositions de l'article 6.2.2 du règlement des agents sportifs de cette fédération, notamment en limitant la rémunération de ces agents, pour certains contrats, à 6 % du salaire brut du joueur ou de l'entraîneur ou du montant hors taxe du contrat de travail, de renvoyer au Conseil Constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution de l'article 6 de la loi n° 2012-158 du 1er février 2012 visant à renforcer l'éthique du sport et les droits des sportifs ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution, notamment son Préambule et son article 61-1 ;<br/>
<br/>
              Vu l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
<br/>
              Vu le code du sport ;<br/>
<br/>
              Vu la loi n° 2012-158 du 1er février 2012, notamment son article 6 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie-Caroline de Margerie, Conseiller d'Etat,  <br/>
<br/>
              - les observations de la SCP Barthélemy, Matuchansky, Vexliard, avocat de la Fédération française de football,<br/>
<br/>
              - les conclusions de M. Damien Botteghi, Rapporteur public,<br/>
<br/>
              La parole ayant à nouveau été donnée à la SCP Barthélemy, Matuchansky, Vexliard, avocat de la Fédération française de football ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil Constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...) " ; qu'il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement de circonstances, et que la question soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              2. Considérant qu'en vertu de l'article L. 222-7 du code du sport, l'activité consistant à mettre en rapport, contre rémunération, les parties intéressées à la conclusion d'un contrat relatif à l'exercice rémunéré d'une activité sportive ou d'entraînement, ou d'un contrat qui prévoit la conclusion d'un contrat de travail ayant pour objet l'exercice rémunéré d'une telle activité, ne peut être exercée que par une personne détentrice d'une licence d'agent sportif ; que, selon le même article, cette licence est délivrée, suspendue et retirée, selon la discipline concernée, par la fédération sportive délégataire qui contrôle annuellement l'activité des agents sportifs ; que chaque fédération délégataire publie la liste des agents sportifs autorisés à exercer dans sa discipline ;<br/>
<br/>
              3. Considérant que les articles L. 222-9 à L. 222-14 du même code fixent les incompatibilités et incapacités faisant obstacle à l'exercice de l'activité d'agent sportif ; que les articles L. 222-8, L. 222-15 et L. 222-16 déterminent les conditions d'exercice de cette activité ;<br/>
<br/>
              4. Considérant que les deuxième, troisième, quatrième et cinquième alinéas de l'article L. 222-17 du même code prévoient que : " Le contrat écrit en exécution duquel l'agent sportif exerce l'activité consistant à mettre en rapport les parties intéressées à la conclusion d'un des contrats mentionnés à l'article L. 222-7 précise : / 1° Le montant de la rémunération de l'agent sportif, qui ne peut excéder 10 % du montant du contrat conclu par les parties qu'il a mises en rapport ; / 2° La partie à l'un des contrats mentionnés à l'article L. 222-7 qui rémunère l'agent sportif. / Lorsque, pour la conclusion d'un contrat mentionné à l'article L. 222-7, plusieurs agents sportifs interviennent, le montant total de leurs rémunérations ne peut excéder 10 % du montant de ce contrat " ; que le I de l'article 6 de la loi du 1er février 2012 visant à renforcer l'éthique du sport et les droits des sportifs a complété les dispositions de l'article L. 222-17 par un nouvel alinéa selon lequel : " Par dérogation au 1° et au cinquième alinéa, les fédérations délégataires peuvent fixer, pour la rémunération du ou des agents sportifs, un montant inférieur à 10 % du contrat conclu par les parties mises en rapport. " ; que le II du même article 6 a inséré des dispositions de portée analogue, en ce qui concerne la rémunération du ou des avocats, à l'article 10 de la loi du 31 décembre 1971 portant réforme de certaines professions judiciaires et juridiques ;<br/>
<br/>
              5. Considérant que la question prioritaire de constitutionnalité, soulevée par l'Association Union des agents sportifs du football et par le Syndicat national des agents sportifs à l'appui du recours pour excès de pouvoir qu'ils ont formé contre la décision de la Fédération française de football ayant limité, pour cette discipline sportive et pour certains contrats, la rémunération des agents sportifs à 6 % du salaire brut du joueur ou de l'entraîneur ou du montant hors taxe du contrat de travail, met en cause la conformité à la Constitution des dispositions résultant du I de l'article 6 de la loi du 1er février 2012 ;<br/>
<br/>
              6. Considérant, en premier lieu, que ni le respect des règles relatives à la procédure législative, ni celui de l'objectif à valeur constitutionnelle d'intelligibilité et d'accessibilité de la loi ne sont au nombre des droits et libertés que la Constitution garantit, au sens de l'article 61-1 de la Constitution ; qu'ils ne peuvent, par suite, être invoqués à l'appui d'une question prioritaire de constitutionnalité ; <br/>
<br/>
              7. Considérant, en deuxième lieu, qu'il est loisible au législateur d'apporter à la liberté d'entreprendre et à la liberté contractuelle, qui découlent de l'article 4 de la Déclaration des droits de l'homme et du citoyen, des limitations justifiées par l'intérêt général, à la condition qu'il n'en résulte pas d'atteintes disproportionnées au regard de l'objectif poursuivi ; que les dispositions critiquées de la loi du 1er février 2012 permettent aux fédérations sportives délégataires de fixer, pour leur discipline, une limite à la rémunération des agents sportifs inférieure à celle qui était déjà antérieurement fixée par la loi ; que ces dispositions ne sont pas susceptibles de s'appliquer à des contrats conclus avant leur entrée en vigueur ; qu'en les adoptant, le législateur a entendu permettre aux fédérations délégataires d'abaisser, compte tenu des rapports entre les clubs et les sportifs professionnels observés dans la discipline dont elles ont la charge, la limite mise à la rémunération des agents sportifs afin de préserver les intérêts des sportifs et d'éviter que le niveau des rémunérations des agents n'affecte ces rapports professionnels et n'empêche la conclusion de contrats entre clubs et sportifs ; que ce faisant, le législateur a adopté, dans l'intérêt général, une mesure qui ne porte pas d'atteinte disproportionnée à la liberté contractuelle, à la liberté d'entreprendre, ni, en tout état de cause, à la liberté du commerce et de l'industrie ;<br/>
<br/>
              8. Considérant, en troisième lieu, qu'eu égard aux différences constatées entre les disciplines sportives pour ce qui concerne la conclusion de contrats se rapportant à l'exercice rémunéré d'une activité sportive ou d'entraînement, le législateur a pu, sans méconnaître le principe d'égalité, investir les fédérations délégataires, pour la discipline dont elles ont la charge, du pouvoir de fixer une limite à la rémunération des agents sportifs inférieure à celle fixée de façon générale par la loi ;<br/>
<br/>
              9. Considérant, enfin, qu'il appartient aux fédérations délégataires de mettre en oeuvre, sans méconnaître la liberté contractuelle ni la liberté d'entreprendre, le pouvoir dont elles ont été investies et de déterminer, sous le contrôle du juge administratif, s'il y a lieu d'instituer une limite de rémunération pour les agents sportifs inférieure à celle prévue par la loi et d'en fixer le niveau, compte tenu de la situation observée dans leur discipline ; que, eu égard à la nature et à la portée de la mesure en cause, le moyen tiré de ce que, faute d'avoir encadré la décision des fédérations en leur interdisant de fixer une limite trop basse, le législateur aurait méconnu l'étendue de sa compétence doit être écarté ;<br/>
<br/>
              10. Considérant qu'il résulte de ce qui précède que la question soulevée, qui n'est pas nouvelle, ne présente pas un caractère sérieux ; qu'ainsi, sans qu'il soit besoin de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée, le moyen tiré de ce que le I de l'article 6 de la loi du 1er février 2012 porte atteinte aux droits et libertés garantis par la Constitution doit être écarté ;<br/>
<br/>
<br/>
<br/>
<br/>
              		D E C I D E :<br/>
              		--------------<br/>
<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par l'Association Union des agents sportifs du football et le Syndicat national des agents sportifs.<br/>
<br/>
Article 2 : La présente décision sera notifiée à l'Association Union des agents sportifs du football, au Syndicat national des agents sportifs et à la Fédération française de football. Copie en sera adressée au Conseil constitutionnel, au Premier ministre et à la ministre des sports, de la jeunesse, de l'éducation populaire et de la vie associative.<br/>
<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-10-05-04-02 PROCÉDURE. - DISPOSITIONS DU I DE L'ARTICLE 6 DE LA LOI DU 1ER FÉVRIER 2012 VISANT À RENFORCER L'ÉTHIQUE DU SPORT ET LES DROITS DES SPORTIFS, QUI CONFÈRENT AUX FÉDÉRATIONS DÉLÉGATAIRES LE POUVOIR D'INSTITUER UNE LIMITE DE RÉMUNÉRATION POUR LES AGENTS SPORTIFS INFÉRIEURE À CELLE PRÉVUE PAR LA LOI ET D'EN FIXER LE NIVEAU - GRIEF TIRÉ D'UNE MÉCONNAISSANCE PAR LE LÉGISLATEUR DE L'ÉTENDUE DE SA COMPÉTENCE - ABSENCE DE CARACTÈRE SÉRIEUX, EU ÉGARD À LA NATURE ET À LA PORTÉE DES DÉCISIONS PRISES PAR LES FÉDÉRATIONS, SOUS LE CONTRÔLE DU JUGE ADMINISTRATIF, POUR METTRE EN OEUVRE CES DISPOSITIONS.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">63-05-01-03 SPECTACLES, SPORTS ET JEUX. SPORTS. FÉDÉRATIONS SPORTIVES. EXERCICE D'UN POUVOIR RÉGLEMENTAIRE. - POUVOIR CONFÉRÉ AUX FÉDÉRATIONS D'INSTITUER UNE LIMITE DE RÉMUNÉRATION POUR LES AGENTS SPORTIFS INFÉRIEURE À CELLE PRÉVUE PAR LA LOI ET D'EN FIXER LE NIVEAU (ART. L. 222-17 DU CODE DU SPORT) - QPC - MOYEN TIRÉ D'UNE MÉCONNAISSANCE PAR LE LÉGISLATEUR DE L'ÉTENDUE DE SA COMPÉTENCE - CARACTÈRE SÉRIEUX - ABSENCE, EU ÉGARD À LA NATURE ET À LA PORTÉE DE LA MESURE EN CAUSE, PRISE PAR LES FÉDÉRATIONS SOUS LE CONTRÔLE DU JUGE ADMINISTRATIF.
</SCT>
<ANA ID="9A"> 54-10-05-04-02 Question prioritaire de constitutionnalité dirigée contre les dispositions du I de l'article 6 de la loi n° 2012-158 du 1er février 2012 visant à renforcer l'éthique du sport et les droits des sportifs, qui ont complété l'article L. 222-17 du code du sport par un nouvel alinéa selon lequel : « Par dérogation au 1° et au cinquième alinéa, les fédérations délégataires peuvent fixer, pour la rémunération du ou des agents sportifs, un montant inférieur à 10 % du contrat conclu par les parties mises en rapport. »,,Il appartient aux fédérations délégataires de mettre en oeuvre, sans méconnaître la liberté contractuelle ni la liberté d'entreprendre, le pouvoir dont elles ont été investies et de déterminer, sous le contrôle du juge administratif, s'il y a lieu d'instituer une limite de rémunération pour les agents sportifs inférieure à celle prévue par la loi et d'en fixer le niveau, compte tenu de la situation observée dans leur discipline. Eu égard à la nature et à la portée de la mesure en cause, le moyen tiré de ce que, faute d'avoir encadré la décision des fédérations en leur interdisant de fixer une limite trop basse, le législateur aurait méconnu l'étendue de sa compétence doit être écarté.</ANA>
<ANA ID="9B"> 63-05-01-03 Question prioritaire de constitutionnalité dirigée contre les dispositions du I de l'article 6 de la loi n° 2012-158 du 1er février 2012 visant à renforcer l'éthique du sport et les droits des sportifs, qui ont complété l'article L. 222-17 du code du sport par un nouvel alinéa selon lequel : « Par dérogation au 1° et au cinquième alinéa, les fédérations délégataires peuvent fixer, pour la rémunération du ou des agents sportifs, un montant inférieur à 10 % du contrat conclu par les parties mises en rapport. »,,Il appartient aux fédérations délégataires de mettre en oeuvre, sans méconnaître la liberté contractuelle ni la liberté d'entreprendre, le pouvoir dont elles ont été investies et de déterminer, sous le contrôle du juge administratif, s'il y a lieu d'instituer une limite de rémunération pour les agents sportifs inférieure à celle prévue par la loi et d'en fixer le niveau, compte tenu de la situation observée dans leur discipline. Eu égard à la nature et à la portée de la mesure en cause, le moyen tiré de ce que, faute d'avoir encadré la décision des fédérations en leur interdisant de fixer une limite trop basse, le législateur aurait méconnu l'étendue de sa compétence doit être écarté.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
