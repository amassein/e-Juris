<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034017913</ID>
<ANCIEN_ID>JG_L_2017_02_000000395821</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/01/79/CETATEXT000034017913.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 08/02/2017, 395821</TITRE>
<DATE_DEC>2017-02-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>395821</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ZRIBI, TEXIER</AVOCATS>
<RAPPORTEUR>M. Arno Klarsfeld</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:395821.20170208</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé à la Cour nationale du droit d'asile d'annuler la décision du 9 octobre 2014 par laquelle le directeur général de l'Office français de protection des réfugiés et apatrides a rejeté sa demande d'admission au bénéfice de l'asile. Par une décision n° 15001126 du 26 mai 2015, la Cour nationale du droit d'asile a rejeté sa demande.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un nouveau mémoire enregistrés les 4 janvier 2016, 4 avril 2016 et 9 décembre 2016 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette décision ;<br/>
<br/>
              2°) réglant l'affaire au fond, de lui accorder le statut de réfugié ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat une somme de 3 000 euros à verser à son avocat Me C...au titre de l'article L. 761-1 du code de justice administrative et de l'article 37 de la loi du 10 juillet 1991.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention de Genève du 28 juillet 1951 relative au statut des réfugiés et le protocole signé à New York le 31 janvier 1967 relatif au statut des réfugiés ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Arno Klarsfeld, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Zribi et Texier, avocat de M. A...B...;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes des stipulations du paragraphe A, 2° de l'article 1er de la convention de Genève du 28 juillet 1951 et du protocole signé à New York le 31 janvier 1967, doit être considérée comme réfugiée toute personne qui " craignant avec raison d'être persécutée du fait de sa race, de sa religion, de sa nationalité, de son appartenance à un certain groupe social ou de ses opinions politiques, se trouve hors du pays dont elle a la nationalité et qui ne peut, ou, du fait de cette crainte, ne veut se réclamer de la protection de ce pays ". <br/>
<br/>
              2. Un groupe social est, au sens de ces dispositions, constitué de personnes partageant un caractère inné, une histoire commune ou une caractéristique essentielle à leur identité et à leur conscience, auxquels il ne peut leur être demandé de renoncer, et une identité propre perçue comme étant différente par la société environnante ou par les institutions. En fonction des conditions qui prévalent dans un pays, des personnes peuvent, à raison de leur orientation sexuelle, constituer un groupe social au sens de ces dispositions. Il convient dès lors, dans l'hypothèse où une personne sollicite le bénéfice du statut de réfugié à raison de son orientation sexuelle, d'apprécier si les conditions existant dans le pays dont elle a la nationalité permettent d'assimiler les personnes se revendiquant de la même orientation sexuelle à un groupe social du fait du regard que portent sur ces personnes la société environnante ou les institutions et dont les membres peuvent craindre avec raison d'être persécutés du fait même de leur appartenance à ce groupe.<br/>
<br/>
               3. Il résulte de ce qui précède que l'octroi du statut de réfugié du fait de persécutions liées à l'appartenance à un groupe social fondé sur des orientations sexuelles communes ne saurait être subordonné à la manifestation publique de cette orientation sexuelle par la personne qui sollicite le bénéfice du statut de réfugié. D'une part, le groupe social n'est pas institué par ceux qui le composent, ni même du fait de l'existence objective de caractéristiques qu'on leur prête mais par le regard que portent sur ces personnes la société environnante ou les institutions. D'autre part, il est exclu que le demandeur d'asile doive, pour éviter le risque de persécution dans son pays d'origine, dissimuler son homosexualité ou faire preuve de réserve dans l'expression de son orientation sexuelle. La circonstance que l'appartenance au groupe social ne fasse l'objet d'aucune disposition pénale répressive spécifique est sans incidence sur l'appréciation de la réalité des persécutions à raison de cette appartenance qui peut, en l'absence de toute disposition pénale spécifique, reposer soit sur des dispositions de droit commun abusivement appliquées au groupe social considéré, soit sur des comportements émanant des autorités, encouragés ou favorisés par ces autorités ou même simplement tolérés par elles.<br/>
<br/>
              4. Il appartient à la Cour nationale du droit d'asile de former sa conviction sur les points en litige au vu des éléments versés au dossier par les parties et, tout spécialement, du récit personnel du demandeur d'asile. Elle ne peut exiger de ce dernier qu'il apporte la preuve des faits qu'il avance et, en particulier, de son orientation sexuelle, mais elle peut écarter des allégations qu'elle jugerait insuffisamment étayées et rejeter, pour ce motif, le recours dont elle est saisie. <br/>
<br/>
              5. Il ressort des pièces du dossier soumis aux juges du fond que pour solliciter son admission au bénéfice de l'asile, M. A...B..., de nationalité bangladaise, né en 1992, soutient qu'il craint d'être persécuté en cas de retour dans son pays d'origine en raison de son orientation sexuelle. Il ressort des énonciations de la décision attaquée que, pour refuser d'octroyer à M. B...le statut de réfugié, la Cour nationale du droit d'asile s'est fondée sur la circonstance que ni les pièces du dossier ni les déclarations de l'intéressé ne permettaient de tenir pour établis les faits allégués et les craintes évoquées. En se fondant sur le caractère faiblement circonstancié du récit personnel de M. B... et aux contradictions dont il est empreint, la Cour, qui n'a pas exigé qu'il établisse la réalité de son orientation sexuelle, a pu, par une décision suffisamment motivée et sans commettre d'erreur de droit, considérer que les persécutions dont l'intéressé alléguait qu'il y serait exposé à raison de son orientation sexuelle ne justifiait pas l'octroi de la qualité de réfugié. Dès lors qu'elle a rejeté son recours pour ce motif, elle n'a pas commis d'erreur de droit en s'abstenant de rechercher si, au regard des critères énoncés au point 2, les personnes homosexuelles constituent un groupe social au Bangladesh.<br/>
<br/>
              6. Il résulte de ce qui précède que le pourvoi de M. B...doit être rejeté, y compris les conclusions présentées au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le pourvoi de M. B...est rejeté.<br/>
Article 2 : La présente décision sera notifiée à M. A...B...et au directeur général de l'Office français de protection des réfugiés et apatrides.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">095-03-01-02-03-05 - PERSÉCUTIONS ENCOURUES DU FAIT DE L'APPARTENANCE À UN GROUPE SOCIAL FONDÉ SUR DES ORIENTATIONS SEXUELLES COMMUNES - 1) CONDITIONS D'OCTROI DE LA PROTECTION - A) MANIFESTATION PUBLIQUE DE SON ORIENTATION SEXUELLE PAR LA PERSONNE QUI SOLLICITE LE STATUT DE RÉFUGIÉ - ABSENCE [RJ1] -  B) EXISTENCE DE DISPOSITIONS RÉPRESSIVES SPÉCIFIQUES - ABSENCE - 2) OFFICE DU JUGE - NÉCESSITÉ POUR LE DEMANDEUR D'APPORTER LA PREUVE DE SON ORIENTATION SEXUELLE - ABSENCE [RJ2].
</SCT>
<ANA ID="9A"> 095-03-01-02-03-05 1) a) L'octroi du statut de réfugié du fait de persécutions liées à l'appartenance à un groupe social fondé sur des orientations sexuelles communes ne saurait être subordonné à la manifestation publique de cette orientation sexuelle par la personne qui sollicite le bénéfice du statut de réfugié. D'une part, le groupe social n'est pas institué par ceux qui le composent, ni même du fait de l'existence objective de caractéristiques qu'on leur prête mais par le regard que portent sur ces personnes la société environnante ou les institutions. D'autre part, il est exclu que le demandeur d'asile doive, pour éviter le risque de persécution dans son pays d'origine, dissimuler son homosexualité ou faire preuve de réserve dans l'expression de son orientation sexuelle.... ,,b) La circonstance que l'appartenance au groupe social ne fasse l'objet d'aucune disposition pénale répressive spécifique est sans incidence sur l'appréciation de la réalité des persécutions à raison de cette appartenance qui peut, en l'absence de toute disposition pénale spécifique, reposer soit sur des dispositions de droit commun abusivement appliquées au groupe social considéré, soit sur des comportements émanant des autorités, encouragés ou favorisés par ces autorités ou même simplement tolérés par elles.,,,2) Il appartient à la Cour nationale du droit d'asile de former sa conviction sur les points en litige au vu des éléments versés au dossier par les parties et, tout spécialement, du récit personnel du demandeur d'asile.  Elle ne peut exiger de ce dernier qu'il apporte la preuve des faits qu'il avance et, en particulier, de son orientation sexuelle, mais elle peut écarter des allégations qu'elle jugerait insuffisamment étayées et rejeter, pour ce motif, le recours dont elle est saisie.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 23 août 2006, Office français de protection des réfugiés et apatrides c/ Mlle,, n° 272679, T. p. 904 ; CE, 27 juillet 2012, Mbwene, n° 349824, p. 315., ,[RJ2] Cf. décisions du même jour, M.,, n° 396695, M.,, n° 397745, M.,, n° 379378, inédites au Recueil.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
