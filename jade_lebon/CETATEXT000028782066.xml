<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028782066</ID>
<ANCIEN_ID>JG_L_2014_03_000000369007</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/78/20/CETATEXT000028782066.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème / 1ère SSR, 26/03/2014, 369007</TITRE>
<DATE_DEC>2014-03-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>369007</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème / 1ère SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>M. Marc Pichon de Vendeuil</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier de Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:369007.20140326</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 3 et 18 juin 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour la commune de Saumane-de-Vaucluse, représentée par son maire ; la commune de Saumane-de-Vaucluse demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 13MA00674 du 17 mai 2013 par laquelle le juge des référés de la cour administrative d'appel de Marseille a rejeté sa requête tendant, d'une part, à l'annulation de l'ordonnance n° 1203342 du 5 février 2013 par laquelle le juge des référés du tribunal administratif de Nîmes a suspendu, sur la demande du préfet de Vaucluse, l'exécution de la délibération du 16 octobre 2012 par laquelle son conseil municipal a approuvé la révision du plan local d'urbanisme, d'autre part, au rejet de la demande de suspension du préfet de Vaucluse ;<br/>
<br/>
              2°) statuant en référé, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'environnement ;<br/>
<br/>
              Vu le code de l'urbanisme ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Marc Pichon de Vendeuil, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de la commune de Saumane-de-Vaucluse ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis au juge des référés de la cour administrative d'appel de Marseille que, par une délibération du 16 octobre 2012, le conseil municipal de Saumane-de-Vaucluse a approuvé la révision du plan local d'urbanisme de la commune afin d'ouvrir à l'urbanisation une trentaine d'hectares situés près de la Sorgue auparavant classés en zone agricole ; que, saisi par le préfet de Vaucluse sur le fondement de l'article L. 554-1 du code de justice administrative, le juge des référés du tribunal administratif de Nîmes a, par une ordonnance du 5 février 2013, suspendu l'exécution de cette décision ; que, par une ordonnance du 17 mai 2013 contre laquelle la commune de Saumane-de-Vaucluse se pourvoit en cassation, le juge des référés de la cour administrative d'appel de Marseille a confirmé la décision du premier juge ;<br/>
<br/>
              2. Considérant, en premier lieu, qu'en relevant qu'étaient de nature à faire naître un doute sérieux sur la légalité de la délibération attaquée les moyens tirés, d'une part, de la méconnaissance des dispositions de l'article L. 122-2 du code de l'urbanisme et, d'autre part, de l'absence de réalisation préalable de l'étude environnementale prescrite par les articles L. 121-10 et R. 121-14 du même code  et l'article L. 414-4 du code de l'environnement, le juge des référés a suffisamment motivé son ordonnance ;<br/>
<br/>
              3. Considérant, en deuxième lieu, qu'aux termes de l'article L. 122-2 du code de l'urbanisme, dans sa rédaction issue de l'article 17 de la loi du 12 juillet 2010 portant engagement national pour l'environnement : " Dans les conditions précisées au présent article, dans les communes qui ne sont pas couvertes par un schéma de cohérence territoriale applicable, le plan local d'urbanisme ne peut être modifié ou révisé en vue d'ouvrir à l'urbanisation une zone à urbaniser délimitée après le 1er juillet 2002 ou une zone naturelle. / Jusqu'au 31 décembre 2012, le premier alinéa s'applique dans les communes situées à moins de quinze kilomètres du rivage de la mer ou à moins de quinze kilomètres de la périphérie d'une agglomération de plus de 50 000 habitants au sens du recensement général de la population. A compter du 1er janvier 2013 et jusqu'au 31 décembre 2016, il s'applique dans les communes situées à moins de quinze kilomètres du rivage de la mer ou à moins de quinze kilomètres de la périphérie d'une agglomération de plus de 15 000 habitants au sens du recensement général de la population. A compter du 1er janvier 2017, il s'applique dans toutes les communes./ (...) / Il peut être dérogé aux dispositions des trois alinéas précédents soit avec l'accord du préfet donné après avis de la commission départementale compétente en matière de nature, de paysages et de sites et de la chambre d'agriculture, soit, jusqu'au 31 décembre 2016, lorsque le périmètre d'un schéma de cohérence territoriale incluant la commune a été arrêté, avec l'accord de l'établissement public prévu à l'article L. 122-4. La dérogation ne peut être refusée que si les inconvénients éventuels de l'urbanisation envisagée pour les communes voisines, pour l'environnement ou pour les activités agricoles sont excessifs au regard de l'intérêt que représente pour la commune la modification ou la révision du plan. (...) " ; <br/>
<br/>
              4. Considérant que ces dispositions posent une règle dite de " constructibilité limitée " selon laquelle le plan local d'urbanisme des communes qui ne sont pas couvertes par un schéma de cohérence territoriale (SCOT) applicable ne peut être modifié ou révisé en vue d'ouvrir à l'urbanisation une zone à urbaniser délimitée après le 1er juillet 2002 ou une zone naturelle ; qu'il peut toutefois être dérogé à cette interdiction dans les conditions qu'elles prévoient, notamment lorsque le périmètre du SCOT a été arrêté, avec l'accord de l'établissement public en charge de son élaboration ; qu'une extension progressive du champ géographique d'application de l'interdiction est prévue, avec une généralisation à partir de 2017 ; qu'il en résulte notamment que, jusqu'au 31 décembre 2012, la règle de constructibilité limitée était applicable dans les communes situées en tout ou partie à l'intérieur d'une agglomération de plus de 50 000 habitants, au sens du recensement général de la population, et dans une bande de 15 kilomètres par rapport à la périphérie de celle-ci ; qu'il résulte de ce qui précède qu'en indiquant que la règle énoncée par l'article L. 122-2 du code de l'urbanisme s'appliquait, en principe, aux communes situées dans le périmètre d'une telle agglomération, le juge des référés de la cour administrative d'appel de Marseille n'a pas commis d'erreur de droit ; <br/>
<br/>
              5. Considérant, en troisième lieu, qu'après avoir relevé qu'à la date de la délibération litigieuse, la commune de Saumane-de-Vaucluse avait été intégrée dans l'agglomération d'Avignon sans être couverte par un SCOT, qu'elle se trouvait dans le périmètre du futur SCOT du bassin de vie de Cavaillon, Coustellet, l'Isle-sur-la-Sorgue, dont le projet avait été arrêté par délibération de son conseil syndical le 23 mai 2012, que la révision du plan d'urbanisme engagée par la commune avait pour effet d'ouvrir à l'urbanisation des parcelles agricoles et que l'accord de l'établissement public compétent pour l'élaboration du schéma auquel est subordonnée la dérogation prévue par l'article L. 122-2 du code de l'urbanisme n'avait pas été sollicité, le juge des référés a  jugé que le moyen tiré de la méconnaissance des dispositions de cet article était de nature, en l'état de l'instruction, à faire naître un doute sérieux sur la légalité de la délibération contestée ; qu'il s'est ainsi  livré, sans commettre d'erreur de droit, à une appréciation souveraine des faits exempte de dénaturation, qui n'est pas susceptible d'être discutée devant le juge de cassation ;<br/>
<br/>
              6. Considérant, en dernier lieu, que, pour critiquer le motif de l'ordonnance selon lequel présentait un caractère sérieux le moyen tiré de l'absence de réalisation préalable d'une évaluation environnementale, la commune soutient que le site Natura 2000 dénommé " La Sorgues et l'Auzon " qui, selon le préfet, imposait cette évaluation, n'aurait été classé que postérieurement à cette délibération ; que ce moyen, qui n'est pas d'ordre public, procède d'une argumentation nouvelle en cassation ; que, par suite, et en tout état de cause, la commune ne peut utilement le soulever pour contester le bien fondé de l'ordonnance qu'elle attaque ;<br/>
<br/>
              7. Considérant qu'il résulte de tout ce qui précède que le pourvoi de la commune de Saumane-de-Vaucluse doit être rejeté, y compris ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
      Article 1er : Le pourvoi de la commune de Saumane-de-Vaucluse est rejeté.<br/>
<br/>
Article 2 : La présente décision sera notifiée à la commune de Saumane-de-Vaucluse et à la ministre de l'égalité des territoires et du logement.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">68-001 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. RÈGLES GÉNÉRALES D'UTILISATION DU SOL. - RÈGLE DE CONSTRUCTIBILITÉ LIMITÉE PRÉVUE POUR LES COMMUNES NON COUVERTES PAR UN SCOT (ART. L. 122-2 DU CODE DE L'URBANISME) - COMMUNES CONCERNÉES AVANT LE 1ER JANVIER 2013 - COMMUNES SITUÉES EN TOUT OU PARTIE À L'INTÉRIEUR DE L'AGGLOMÉRATION ET DANS UNE BANDE DE 15 KILOMÈTRES À LA PÉRIPHÉRIE DE CELLE-CI.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">68-01-006 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PLANS D'AMÉNAGEMENT ET D'URBANISME. SCHÉMAS DE COHÉRENCE TERRITORIALE. - COMMUNES DÉPOURVUES DE SCOT - RÈGLE DE CONSTRUCTIBILITÉ LIMITÉE (ART. L. 122-2 DU CODE DE L'URBANISME) - COMMUNES CONCERNÉES AVANT LE 1ER JANVIER 2013 - COMMUNES SITUÉES EN TOUT OU PARTIE À L'INTÉRIEUR DE L'AGGLOMÉRATION ET DANS UNE BANDE DE 15 KILOMÈTRES À LA PÉRIPHÉRIE DE CELLE-CI.
</SCT>
<ANA ID="9A"> 68-001 Il résulte des dispositions de l'article L. 122-2, dans leur rédaction issue de l'article 17 de la loi n° 2010-788 du 12 juillet 2010, que, jusqu'au 31 décembre 2012, la règle de constructibilité limitée qu'elles posent dans les communes qui ne sont pas couvertes par un schéma de cohérence territoriale (SCOT), à laquelle il n'est possible de déroger qu'avec l'accord du préfet ou de l'établissement public chargé d'élaborer ce SCOT, était applicable dans les communes situées en tout ou partie à l'intérieur d'une agglomération de plus de 50 000 habitants, au sens du recensement général de la population, et dans une bande de 15 kilomètres par rapport à la périphérie de celle-ci.</ANA>
<ANA ID="9B"> 68-01-006 Il résulte des dispositions de l'article L. 122-2, dans leur rédaction issue de l'article 17 de la loi n° 2010-788 du 12 juillet 2010, que, jusqu'au 31 décembre 2012, la règle de constructibilité limitée qu'elles posent dans les communes qui ne sont pas couvertes par un schéma de cohérence territoriale (SCOT), à laquelle il n'est possible de déroger qu'avec l'accord du préfet ou de l'établissement public chargé d'élaborer ce SCOT, était applicable dans les communes situées en tout ou partie à l'intérieur d'une agglomération de plus de 50 000 habitants, au sens du recensement général de la population, et dans une bande de 15 kilomètres par rapport à la périphérie de celle-ci.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
