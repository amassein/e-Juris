<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026636547</ID>
<ANCIEN_ID>JG_L_2012_11_000000350867</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/63/65/CETATEXT000026636547.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème et 2ème sous-sections réunies, 15/11/2012, 350867</TITRE>
<DATE_DEC>2012-11-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>350867</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème et 2ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BORE ET SALVE DE BRUNETON ; SCP PEIGNOT, GARREAU, BAUER-VIOLAS</AVOCATS>
<RAPPORTEUR>M. Fabrice Aubert</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Bertrand Dacosta</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:350867.20121115</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la décision du 23 décembre 2011 par laquelle le Conseil d'Etat, statuant au contentieux, a prononcé l'admission des conclusions du pourvoi de l'hôpital de <br/>
l'Isle-sur-la-Sorgue contre l'arrêt n° 08MA04073 du 9 mai 2011 de la cour administrative d'appel de Marseille, en tant seulement que cet arrêt a rejeté ses conclusions tendant à l'application de pénalités de retard pour les lots nos 11 et 12 du marché de rénovation-extension de l'hôpital, confiés à la société Tonin ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Fabrice Aubert, Auditeur,  <br/>
<br/>
              - les observations de la SCP Peignot, Garreau, Bauer-Violas, avocat de l'hôpital de l'Isle-sur-la-Sorgue et de la SCP Boré et Salve de Bruneton, avocat de la société Tonin,<br/>
<br/>
              - les conclusions de M. Bertrand Dacosta, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Peignot, Garreau, <br/>
Bauer-Violas, avocat de l'hôpital de l'Isle-sur-la-Sorgue et à la SCP Boré et Salve de Bruneton, avocat de la société Tonin ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que l'hôpital de l'Isle-sur-la-Sorgue a confié à la société Tonin l'exécution des lots nos 11 et 12 de chauffage-ventilation et plomberie sanitaire du marché de rénovation-extension de l'hôpital ; que cette société a contesté le décompte général du marché établi par le maître d'ouvrage, notamment en ce qu'il lui infligeait des pénalités de retard ; que par arrêt du 9 mai 2011, la cour administrative d'appel de Marseille a condamné l'hôpital de l'Isle-sur-la-Sorgue à lui rembourser les 10 210,42 euros de pénalités mis à sa charge ; que par décision du 23 décembre 2011, le Conseil d'Etat, statuant au contentieux, a prononcé l'admission des conclusions du pourvoi de l'hôpital dirigées contre cet arrêt, en tant seulement qu'il statue sur l'application de ces pénalités ; que par la voie du pourvoi incident, la société Tonin demande l'annulation de l'arrêt dans la même mesure ; <br/>
<br/>
              2. Considérant que, pour faire droit aux conclusions de la société Tonin et rejeter les conclusions d'appel incident de l'hôpital de l'Isle-sur-la-Sorgue au titre des pénalités de retard, la cour a relevé " qu'aucune pièce versée au dossier ne permettait d'établir l'existence d'un retard d'exécution au moins égal à 14 jours " dont se prévalait l'hôpital ; que toutefois, il ressort des pièces du dossier soumis au juge du fond, et notamment des courriers successifs de l'hôpital et des états d'acomptes produits par l'architecte-maître d'oeuvre que la société Tonin a, à plusieurs reprises, abandonné son activité sur le chantier, accumulant un retard d'au moins deux semaines ; que la société a elle-même reconnu, dans son mémoire en réclamation du 26 décembre 2002, l'existence d'un retard de dix-sept semaines et demi, bien qu'elle en rejetât la responsabilité ; qu'il suit de là que la cour a dénaturé les pièces du dossier en jugeant que l'existence d'un retard d'au moins quatorze jours n'était pas établie ; que, par suite, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, son arrêt doit être annulé en tant qu'il statue sur l'application des pénalités de retard ; que cette annulation rend sans objet le pourvoi incident formé par la société Tonin contre la même partie de l'arrêt ;<br/>
<br/>
              3. Considérant qu'il y a lieu, dans les circonstances de l'espèce de régler, dans cette mesure, l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ; <br/>
<br/>
              4. Considérant qu'aux termes de l'article 20.1 du cahier des clauses administratives générales applicable au marché de travaux en cause : " En cas de retard dans l'exécution des travaux, (...), il est appliqué, sauf stipulation différente du CCAP, une pénalité journalière de 1/3000ème du montant de l'ensemble du marché ou de la tranche considérée. (...) Les pénalités sont encourues du simple fait de la constatation du retard par le maître d'oeuvre " ; qu'il résulte de ces dispositions que, sauf stipulation contraire du cahier des clauses administratives particulières du marché, les pénalités de retard sont dues de plein droit et sans mise en demeure préalable du cocontractant, dès constatation par le maître d'oeuvre du dépassement des délais d'exécution ; qu'en l'espèce, le cahier des clauses administratives particulières, qui dérogeait seulement au cahier des clauses administratives générales quant au montant des pénalités, ne prévoyait pas de mise en demeure du cocontractant avant application des pénalités de retard ; que, par suite, la société Tonin n'est pas fondée à soutenir que les pénalités de retard infligées par l'hôpital de l'Isle-sur-la-Sorgue seraient irrégulières, faute de mise en demeure préalable ; <br/>
<br/>
<br/>
              5. Considérant que si la société Tonin conteste, à l'appui de ses conclusions tendant à la restitution des pénalités de retard infligées par le maître d'ouvrage, sa responsabilité dans les retards de chantier constatés par le maître d'oeuvre, elle n'apporte aucun élément de nature à expliquer le non respect des délais d'exécution du contrat par des aléas ou des évènements qui ne lui seraient pas imputables ; qu'ainsi qu'il a été dit, il résulte au contraire de l'instruction que le retard d'exécution qui lui est imputable est au moins égal aux quatorze jours retenus par le maître d'oeuvre pour le calcul des pénalités contractuelles ; que, par suite et sans qu'il soit besoin de se prononcer sur leur recevabilité, les conclusions de la société Tonin tendant à la condamnation de l'hôpital de l'Isle-sur-la-Sorgue à lui restituer les pénalités de retard doivent être rejetées ; <br/>
<br/>
              6. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mis à la charge de l'hôpital de l'Isle-sur-la-Sorgue qui n'est pas, dans la présente instance, la partie perdante, le versement de la somme demandée par la société Tonin ; qu'en revanche il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de cette société la somme de 3 000 euros à verser à l'hôpital de l'Isle-sur-la-Sorgue au titre de ces dispositions ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 9 mai 2011 de la cour administrative d'appel de Marseille est annulé en tant qu'il statue sur les pénalités de retard. <br/>
Article 2 : Il n'y a pas lieu de statuer sur le pourvoi incident de la société Tonin.<br/>
Article 3 : La société Tonin versera une somme de 3 000 euros à l'hôpital de l'Isle-sur-la-Sorgue au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions d'appel de la société Tonin tendant à la restitution des pénalités de retard infligées par le maître d'ouvrage et ses conclusions présentées sur le fondement de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 5 : La présente décision sera notifiée à l'hôpital de l'Isle-sur-la-Sorgue et à la société Tonin.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-03-01-02-02 MARCHÉS ET CONTRATS ADMINISTRATIFS. EXÉCUTION TECHNIQUE DU CONTRAT. CONDITIONS D'EXÉCUTION DES ENGAGEMENTS CONTRACTUELS EN L'ABSENCE D'ALÉAS. MARCHÉS. RETARDS D'EXÉCUTION. - PÉNALITÉS DE RETARD (ART. 20.1 DU CCAG) - NÉCESSITÉ D'UNE MISE EN DEMEURE PRÉALABLE - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">39-05-01-03 MARCHÉS ET CONTRATS ADMINISTRATIFS. EXÉCUTION FINANCIÈRE DU CONTRAT. RÉMUNÉRATION DU CO-CONTRACTANT. PÉNALITÉS DE RETARD. - ARTICLE 20.1 DU CCAG - NÉCESSITÉ D'UNE MISE EN DEMEURE PRÉALABLE - ABSENCE.
</SCT>
<ANA ID="9A"> 39-03-01-02-02 Il résulte des dispositions de l'article 20.1 du cahier des clauses administratives générales (CCAG) que, sauf stipulation contraire du cahier des clauses administratives particulières du marché, les pénalités de retard sont dues de plein droit et sans mise en demeure préalable du cocontractant, dès constatation par le maître d'oeuvre du dépassement des délais d'exécution.</ANA>
<ANA ID="9B"> 39-05-01-03 Il résulte des dispositions de l'article 20.1 du cahier des clauses administratives générales (CCAG) que, sauf stipulation contraire du cahier des clauses administratives particulières du marché, les pénalités de retard sont dues de plein droit et sans mise en demeure préalable du cocontractant, dès constatation par le maître d'oeuvre du dépassement des délais d'exécution.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
