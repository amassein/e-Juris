<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031274355</ID>
<ANCIEN_ID>JG_L_2015_10_000000384884</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/27/43/CETATEXT000031274355.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème SSR, 05/10/2015, 384884, Publié au recueil Lebon</TITRE>
<DATE_DEC>2015-10-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>384884</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROUSSEAU, TAPIE</AVOCATS>
<RAPPORTEUR>M. François Lelièvre</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Bertrand Dacosta</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2015:384884.20151005</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Bordeaux de condamner l'Etat à l'indemniser du préjudice résultant de sa titularisation tardive. <br/>
<br/>
              Par un jugement n° 0902697 du 16 avril 2013, le tribunal administratif de Bordeaux a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 13BX01630 du 28 juillet 2014, la cour administrative d'appel de Bordeaux, saisie en appel, a annulé ce jugement et condamné l'Etat à verser la somme de 114 952,81 euros assortie des intérêts capitalisés à M.B....<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 29 septembre et 22 décembre 2014 et le 2 juin 2015 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'agriculture, de l'agroalimentaire et de la forêt demande au Conseil d'Etat d'annuler cet arrêt.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la loi n° 68-1250 du 31 décembre 1968 ;<br/>
              - la loi n° 2000-321 du 12 avril 2000 ;<br/>
              - la décision du 26 juin 2015 par laquelle le Conseil d'Etat, statuant au contentieux, n'a pas renvoyé au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par M. B...;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. François Lelièvre, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Bertrand Dacosta, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rousseau, Tapie, avocat de M. B...;<br/>
<br/>
<br/>
<br/>1. Considérant que, d'une part, aux termes de l'article 1er de la loi du 31 décembre 1968 relative à la prescription des créances sur l'Etat, les départements, les communes et les établissements publics : " Sont prescrites, au profit de l'Etat, des départements et des communes (...) toutes créances qui n'ont pas été payées dans un délai de quatre ans à partir du premier jour de l'année suivant celle au cours de laquelle les droits ont été acquis (...) " ;  qu'aux termes de l'article 2 de la même loi : " La prescription est interrompue par : / Toute demande de paiement ou toute réclamation écrite adressée par un créancier à l'autorité administrative, dès lors que la demande ou la réclamation a trait au fait générateur, à l'existence, au montant ou au paiement de la créance, alors même que l'administration saisie n'est pas celle qui aura finalement la charge du règlement (...) " ; que, d'autre part, aux termes de l'article 16 de la loi du 12 Avril 2000 relative aux droits des citoyens dans leurs relations avec les administrations, dans sa rédaction applicable au litige : " Toute personne tenue de respecter une date limite ou un délai pour présenter une demande, déposer une déclaration, exécuter un paiement ou produire un document auprès d'une autorité administrative peut satisfaire à cette obligation au plus tard à la date prescrite au moyen d'un envoi postal, le cachet de la poste faisant foi, ou d'un envoi par voie électronique (...). Ces dispositions ne sont applicables ni aux procédures régies par le code des marchés publics, ni à celles relevant des articles L. 1411-1 et suivants du code général des collectivités territoriales, ni à celles pour lesquelles la présence personnelle du demandeur est exigée en application d'une disposition particulière. (...) " ; <br/>
<br/>
              2. Considérant qu'une demande tendant à mettre en jeu la responsabilité d'une collectivité publique, à laquelle celle-ci peut, le cas échéant, opposer la prescription régie par les dispositions citées ci-dessus de la loi du 31 décembre 1968 est au nombre des demandes présentées à une autorité administrative auxquelles s'applique la règle posée par l'article 16 de la loi du 12 avril 2000 ; qu'il suit de là qu'après avoir relevé que le fait générateur de la créance indemnitaire de M. B...était constitué par une décision du 25 novembre 2002 et que, d'après le cachet de la poste, l'intéressé avait adressé sa demande au ministre chargé de l'agriculture le 29 décembre 2006, la cour administrative d'appel de Bordeaux n'a pas commis d'erreur de droit en se fondant sur les dispositions de l'article 16 de la loi du 12 avril 2000 pour écarter l'exception de prescription quadriennale opposée par l'administration à cette demande, qu'elle n'avait reçue que le 2 janvier 2007 ;<br/>
<br/>
              3. Considérant que c'est par une simple erreur de plume que l'un des points de l'arrêt attaqué fait mention que M. B...avait été titularisé dans le grade d'ingénieur des travaux publics ; que, par suite, les moyens tirés de ce que la cour aurait commis une erreur de droit et dénaturé les pièces du dossier en se fondant sur cette circonstance doivent être écartés ;<br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède que le ministre chargé de l'agriculture n'est pas fondé à demander l'annulation de l'arrêt attaqué, qui est suffisamment motivé ;<br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement d'une somme de 3 000 euros à M. B...au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er  : Le pourvoi du ministre de l'agriculture, de l'agroalimentaire et de la forêt est rejeté.<br/>
Article 2 : L'Etat versera la somme de 3 000 euros à M. B...au titre de dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée au ministre de l'agriculture, de l'agroalimentaire et de la forêt et à M. A...B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">18-04-02-05 COMPTABILITÉ PUBLIQUE ET BUDGET. DETTES DES COLLECTIVITÉS PUBLIQUES - PRESCRIPTION QUADRIENNALE. RÉGIME DE LA LOI DU 31 DÉCEMBRE 1968. INTERRUPTION DU COURS DU DÉLAI. - DEMANDE INTERROMPANT LA PRESCRIPTION - DATE À PRENDRE EN COMPTE - DATE D'ENVOI DU COURRIER (ART. 16 DE LA LOI DU 12 AVRIL 2000 DITE LOI DCRA) [RJ1].
</SCT>
<ANA ID="9A"> 18-04-02-05 Une demande tendant à mettre en jeu la responsabilité d'une collectivité publique, à laquelle celle-ci peut, le cas échéant, opposer la prescription régie par les dispositions de la loi du 31 décembre 1968, est au nombre des demandes présentées à une autorité administrative auxquelles s'applique la règle posée par l'article 16 de la loi n° 2000-321 du 12 avril 2000. La date à prendre en compte pour savoir si la prescription est interrompue par la demande, en application de l'article 2 de la loi n° 68-1250 du 31 décembre 1968,  est donc la date d'envoi du courrier et non la date de sa réception par l'administration.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Ab. jur. CE, 25 juillet 2013, Sté Darty et fils, n° 352634, inédit.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
