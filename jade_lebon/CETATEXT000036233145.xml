<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036233145</ID>
<ANCIEN_ID>JG_L_2017_12_000000402011</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/23/31/CETATEXT000036233145.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 6ème chambres réunies, 18/12/2017, 402011</TITRE>
<DATE_DEC>2017-12-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>402011</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN ; SCP GASCHIGNARD</AVOCATS>
<RAPPORTEUR>Mme Marie Sirinelli</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Charles Touboul</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:402011.20171218</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Serenis a demandé au tribunal administratif de Montpellier d'annuler pour excès de pouvoir les décisions des 7 et 27 juillet 2010 par lesquelles le maire de Castries a refusé de lui délivrer un permis de construire et un permis d'aménager. Par un jugement n°s 1003917, 1003921 du 27 décembre 2012, le tribunal administratif de Montpellier a fait droit à ses demandes.<br/>
<br/>
              Par un arrêt n° 13MA01137 du 24 juin 2015, rectifié par une ordonnance n° 13MA01137 du 9 juillet 2015, la cour administrative d'appel de Marseille, saisie par la commune de Castries, a annulé le jugement du tribunal administratif de Montpellier et rejeté les demandes présentées par la société Serenis devant ce tribunal.<br/>
<br/>
              Par un arrêt n° 15MA03585 du 27 mai 2016, cette même cour a rejeté la requête en opposition présentée par la société Serenis contre l'arrêt du 24 juin 2015.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 1er août et 28 octobre 2016 au secrétariat du contentieux du Conseil d'Etat, la société Serenis demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt de la cour administrative d'appel de Marseille du 27 mai 2016 ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son opposition ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Castries la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code de l'urbanisme ;<br/>
              - le décret n° 2007-18 du 5 janvier 2007 ;<br/>
              - le décret n° 2007-817 du 11 mai 2007 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie Sirinelli, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Charles Touboul, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la société Serenis et à la SCP Gaschignard, avocat de la commune de Castries.<br/>
<br/>
              Vu la note en délibéré, enregistrée le 7 décembre 2017, présentée pour la commune de Castries ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par des décisions des 7 et 27 juillet 2010, le maire de la commune de Castries a refusé de délivrer à la société Serenis un permis d'aménager et un permis de construire en vue de l'édification d'un " centre d'hébergement de loisirs touristiques ". Par un jugement du 27 décembre 2012, le tribunal administratif de Montpellier a annulé ces décisions, au motif qu'elles devaient être regardées comme ayant illégalement retiré des décisions tacites nées les 12 avril et 2 juin 2010, à l'expiration des délais dont disposait l'autorité administrative pour statuer. Par un arrêt du 24 juin 2015, la cour administrative d'appel de Marseille, saisie par la commune, a annulé ce jugement et rejeté les demandes formées par la société Serenis, au motif notamment que les délais d'instruction des demandes de permis d'aménager et de construire avaient été régulièrement prorogés et qu'en conséquence, les décisions expresses de refus étaient intervenues avant la naissance de décisions tacites. Enfin, saisie par la société Serenis d'une requête en opposition demandant que soit déclaré non avenu son arrêt du 24 juin 2015, la cour administrative d'appel de Marseille a, par un nouvel arrêt du 27 mai 2016, rejeté cette demande. La société Serenis se pourvoit contre ce dernier arrêt.<br/>
<br/>
              2. Aux termes de l'article R. 831-1 du code de justice administrative : " Toute personne qui, mise en cause par la cour administrative d'appel ou le Conseil d'Etat, n'a pas produit de défense en forme régulière est admise à former opposition à la décision rendue par défaut, sauf si celle-ci a été rendue contradictoirement avec une partie qui a le même intérêt que la partie défaillante ". <br/>
<br/>
              3. Pour assurer l'instruction contradictoire d'un recours en opposition ou en tierce opposition, il appartient à la juridiction saisie, si elle estime ce recours recevable, de communiquer au requérant les pièces de la procédure ayant donné lieu à la décision dont la rétractation est ainsi demandée, d'office lorsque le requérant n'avait pas été régulièrement mis en cause dans l'instance précédente, ou à sa demande, s'il l'avait été.<br/>
<br/>
              4. Il ressort des pièces du dossier soumis aux juges du fond que la société Serenis, qui n'a pas reçu la notification qui lui avait été faite de la requête d'appel formée le 14 mars 2013 par la commune de Castries contre le jugement du 27 décembre 2012, a, par une requête enregistrée le 25 août 2015, formé opposition contre l'arrêt du 24 juin 2015 par lequel la cour administrative d'appel de Marseille avait fait droit aux conclusions de cette commune. Elle a demandé à la cour, le 25 mars 2016, la communication de la requête d'appel initialement formée par la commune et cette demande est restée sans réponse. En l'espèce, la seule circonstance que cette demande avait été formulée après que la cour eut informé les parties, en application de l'article R. 611-11-1 du code de justice administrative, le dossier étant en état d'être jugé, de l'éventualité d'une clôture de l'instruction à effet immédiat à compter du 29 mars suivant, ne pouvait être regardée comme témoignant d'une manoeuvre dilatoire. Ainsi, en rejetant l'opposition de la société Serenis, jugée recevable, sans avoir fait droit à sa demande de communication de la requête de la commune, la cour a statué au terme d'une procédure irrégulière.<br/>
<br/>
              5. Il résulte de ce qui précède que l'arrêt de la cour administrative d'appel de Marseille du 27 mai 2016 doit être annulé. Le moyen retenu suffisant à entraîner cette annulation, il n'est pas nécessaire d'examiner les autres moyens du pourvoi.<br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de Castries une somme de 2 000 euros à verser à la société Serenis au titre de l'article L. 761-1 du code de justice administrative. Les dispositions de cet article font, en revanche, obstacle à ce que la somme demandée par cette commune soit mise à la charge de la société Serenis, qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Marseille du 27 mai 2016 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Marseille.<br/>
Article 3 : La commune de Castries versera à la société Serenis une somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions de la commune de Castries présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à la société Serenis et à la commune de Castries.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-08-03 PROCÉDURE. VOIES DE RECOURS. OPPOSITION. - OBLIGATION POUR LE JUGE DE COMMUNIQUER LES PIÈCES DE LA PROCÉDURE AYANT DONNÉ LIEU À LA DÉCISION DONT IL EST DEMANDÉ LA RÉTRACTATION - EXISTENCE, DÈS LORS QUE LE RECOURS EST RECEVABLE ET QUE LE REQUÉRANT EN A FAIT LA DEMANDE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-08-04 PROCÉDURE. VOIES DE RECOURS. TIERCE-OPPOSITION. - OBLIGATION POUR LE JUGE DE COMMUNIQUER D'OFFICE LES PIÈCES DE LA PROCÉDURE AYANT DONNÉ LIEU À LA DÉCISION DONT IL EST DEMANDÉ LA RÉTRACTATION - EXISTENCE, DÈS LORS QUE LE RECOURS EST RECEVABLE.
</SCT>
<ANA ID="9A"> 54-08-03 Pour assurer l'instruction contradictoire d'un recours en tierce-opposition, il appartient à la juridiction saisie, si elle estime ce recours recevable, de communiquer au requérant, dès lors qu'il en a fait la demande, les pièces de la procédure ayant donné lieu à la décision dont il est ainsi demandé la rétractation.</ANA>
<ANA ID="9B"> 54-08-04 Pour assurer l'instruction contradictoire d'un recours en tierce-opposition, il appartient à la juridiction saisie, si elle estime ce recours recevable, de communiquer d'office au requérant les pièces de la procédure ayant donné lieu à la décision dont il est ainsi demandé la rétractation.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
