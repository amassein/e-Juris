<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027535313</ID>
<ANCIEN_ID>JG_L_2013_06_000000366880</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/53/53/CETATEXT000027535313.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 10/06/2013, 366880</TITRE>
<DATE_DEC>2013-06-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>366880</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELAPORTE, BRIARD, TRICHET</AVOCATS>
<RAPPORTEUR>M. Jean Lessi</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:366880.20130610</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu l'ordonnance n° 1220590 du 13 mars 2013, enregistrée le 18 mars 2013 au secrétariat du contentieux du Conseil d'Etat, par laquelle la vice-présidente de section du tribunal administratif de Paris, avant qu'il soit statué sur la demande de la société Natixis Asset Management, tendant à la condamnation de l'Etat à lui verser une indemnité de 9 765 000 euros en réparation du préjudice qu'elle estime avoir subi en raison de l'incompétence négative dont serait entaché le décret n° 87-948 du 26 novembre 1987, a décidé, par application des dispositions de l'article 23-2 de l'ordonnance n° 58-1067 du 7 novembre 1958, de transmettre au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution des articles 7 et 15 de l'ordonnance n° 86-1134 du 21 octobre 1986 relative à l'intéressement et à la participation des salariés aux résultats de l'entreprise et à l'actionnariat des salariés ;<br/>
<br/>
              Vu le mémoire, enregistré le 8 décembre 2012 au greffe du tribunal administratif de Paris, présenté par la société Natixis Asset Management, dont le siège est 21, quai d'Austerlitz à Paris (75013), représentée par son représentant légal, en application de l'article 23-1 de l'ordonnance n° 58-1067 du 7 novembre 1958 ; <br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu la Constitution, notamment son article 61-1 et son Préambule ;<br/>
<br/>
              Vu l'ordonnance n° 58-1067 du 7 novembre 1958 ; <br/>
<br/>
              Vu le code du travail ;<br/>
<br/>
              Vu l'ordonnance n° 86-1134 du 21 octobre 1986, notamment son article 15 ;<br/>
<br/>
              Vu la loi n° 90-1002 du 7 novembre 1990 ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean Lessi, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Delaporte, Briard, Trichet, avocat de la société Natixis Asset Management ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que lorsqu'une juridiction relevant du Conseil d'Etat a transmis à ce dernier, en application de l'article 23-2 de l'ordonnance du 7 novembre 1958, la question de la conformité à la Constitution d'une disposition législative, le Conseil d'Etat est régulièrement saisi et se prononce sur le renvoi de la question prioritaire de constitutionnalité telle qu'elle a été soulevée dans le mémoire distinct produit devant la juridiction qui la lui a transmise, dans la limite des dispositions dont la conformité à la Constitution a fait l'objet de la transmission ; qu'en l'espèce, si l'ordonnance transmettant au Conseil d'Etat la question soulevée par la société Natixis Asset Management mentionne l'article 7 de l'ordonnance du 21 octobre 1986 relative à l'intéressement et à la participation des salariés aux résultats de l'entreprise et à l'actionnariat des salariés, la société requérante n'a pas contesté, dans son mémoire produit devant le tribunal administratif de Paris, la conformité de ces dispositions aux droits et libertés garantis par la Constitution ; que, par suite, il y a lieu pour le Conseil d'Etat de se prononcer sur le renvoi au Conseil constitutionnel de la question prioritaire de constitutionnalité en tant seulement qu'elle porte sur les dispositions de l'article 15 de l'ordonnance du 21 octobre 1986 et sur le premier alinéa de l'article L. 442-9 du code du travail auquel ces dispositions ont été codifiées, dans sa rédaction en vigueur jusqu'au 31 décembre 2004 ;<br/>
<br/>
              2. Considérant qu'il résulte des dispositions de l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que le Conseil constitutionnel est saisi de la question de la conformité à la Constitution de la disposition législative contestée à la triple condition que cette disposition soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              3. Considérant qu'aux termes de l'article 15 de l'ordonnance du 21 octobre 1986, qui a été implicitement ratifié par l'article 5 de la loi du 7 novembre 1990 et dont les dispositions ont été ultérieurement codifiées au premier alinéa de l'article L. 442-9 du code du travail : " Un décret en Conseil d'Etat détermine les entreprises publiques et les sociétés nationales qui sont soumises aux dispositions du présent chapitre. Il fixe les conditions dans lesquelles ces dispositions leur sont applicables " ; <br/>
<br/>
              4. Considérant que l'article 15 de l'ordonnance du 21 octobre 1986 et le premier alinéa de l'article L. 442-9 du code du travail dans sa rédaction en vigueur jusqu'au 31 décembre 2004 sont applicables au litige dont est saisi le tribunal administratif de Paris au sens et pour l'application de l'article 23-4 de l'ordonnance du 7 novembre 1958 ; que ces dispositions n'ont pas déjà été déclarées conformes à la Constitution par le Conseil constitutionnel ; que le moyen tiré de ce que ces dispositions, telles qu'elles ont été interprétées par les juridictions compétentes et compte tenu des effets, notamment dans le temps, s'attachant à cette interprétation, portent atteinte aux droits et libertés garantis par la Constitution, notamment à la garantie des droits protégée par l'article 16 de la Déclaration des droits de l'homme et du citoyen ainsi qu'au principe d'égalité devant la loi et au principe d'égalité devant les charges publiques garantis par les articles 6 et 13 de cette même Déclaration, soulève une question nouvelle ; qu'ainsi, il y a lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La question de la conformité à la Constitution de l'article 15 de l'ordonnance du 21 octobre 1986 et du  premier alinéa de l'article L. 442-9 du code du travail, dans sa rédaction en vigueur jusqu'au 31 décembre 2004, est renvoyée au Conseil constitutionnel.<br/>
Article 2 : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité transmise par le tribunal administratif de Paris en tant qu'elle porte sur l'article 7 de l'ordonnance du 21 octobre 1986.<br/>
Article 3 : La présente décision sera notifiée à la société Natixis Asset Management, au ministre de l'économie et des finances, au ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social et au Premier ministre.<br/>
Copie en sera adressée au tribunal administratif de Paris.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-10-05-04-01 PROCÉDURE. - MOYEN TIRÉ DE CE QUE LA LOI, TELLE QU'INTERPRÉTÉE PAR LE JUGE COMPÉTENT, COMPTE TENU NOTAMMENT DES EFFETS DANS LE TEMPS DE CETTE INTERPRÉTATION, PORTE ATTEINTE AUX DROITS ET LIBERTÉS GARANTIS PAR LA CONSTITUTION.
</SCT>
<ANA ID="9A"> 54-10-05-04-01 Le moyen tiré de ce que la loi, telle qu'interprétée par le juge compétent, compte tenu notamment des effets dans le temps de cette interprétation, porte atteinte aux droits et libertés garantis par la Constitution, soulève une question nouvelle au sens et pour l'application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958. Renvoi au Conseil constitutionnel.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
