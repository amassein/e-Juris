<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029490955</ID>
<ANCIEN_ID>JG_L_2014_09_000000366628</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/49/09/CETATEXT000029490955.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème / 1ère SSR, 22/09/2014, 366628, Publié au recueil Lebon</TITRE>
<DATE_DEC>2014-09-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>366628</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème / 1ère SSR</FORMATION>
<TYPE_REC>Contentieux des pensions</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Sophie Roussel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier de Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:366628.20140922</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le nouveau mémoire, enregistrés les 6 mars et 10 mai 2013 au secrétariat du contentieux du Conseil d'Etat, présentés par M. A...B..., demeurant ... ; M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 119/00031 du 14 janvier 2013 par lequel la cour régionale des pensions d'Aix-en-Provence a confirmé le jugement du 14 mars 2011 du tribunal départemental des pensions du Var ayant rejeté sa demande d'octroi d'une pension militaire d'invalidité ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses conclusions d'appel ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
      Vu le code des pensions militaires d'invalidité et des victimes de la guerre ; <br/>
<br/>
              Vu le décret du 10 janvier 1992 déterminant les règles et barèmes pour la classification et l'évaluation des troubles psychiques de guerre ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie Roussel, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant, d'une part, qu'aux termes l'article L. 2 du code des pensions militaires d'invalidité et des victimes de la guerre : " Ouvrent droit à pension : / 1° Les infirmités résultant de blessures reçues par suite d'événements de guerre ou d'accidents éprouvés par le fait ou à l'occasion du service ; / 2° Les infirmités résultant de maladies contractées par le fait ou à l'occasion du service ; (...) " ; qu'aux termes de l'article L. 3 du même code : " Lorsqu'il n'est pas possible d'administrer ni la preuve que l'infirmité ou l'aggravation résulte d'une des causes prévues à l'article L. 2, ni la preuve contraire, la présomption d'imputabilité au service bénéficie à l'intéressé à condition : / 1° S'il s'agit de blessure, qu'elle ait été constatée avant le renvoi du militaire dans ses foyers ; / 2° S'il s'agit d'une maladie, qu'elle n'ait été constatée qu'après le quatre-vingt-dixième jour de service effectif et avant le soixantième jour suivant le retour du militaire dans ses foyers ; / 3° En tout état de cause, que soit établie, médicalement, la filiation entre la blessure ou la maladie ayant fait l'objet de la constatation et l'infirmité invoquée. / (...) " ; que, pour l'application de ces dispositions, une infirmité doit être regardée comme résultant d'une blessure lorsqu'elle trouve son origine dans une lésion soudaine, consécutive à un fait précis de service ; que, dans le cas contraire, elle doit être regardée comme résultant d'une maladie ; <br/>
<br/>
              2. Considérant qu'il résulte des dispositions combinées des articles L. 2 et L. 3 citées ci-dessus que, lorsque le demandeur d'une pension ne peut pas bénéficier de la présomption légale d'imputabilité au service, il incombe à ce dernier d'apporter la preuve de cette imputabilité par tous moyens de nature à emporter la conviction des juges ; que, dans les cas où sont en cause des troubles psychiques, il appartient aux juges du fond de prendre en considération l'ensemble des éléments du dossier permettant d'établir que ces troubles sont imputables à un fait précis ou à des circonstances particulières de service ; que lorsqu'il est établi que les troubles psychiques trouvent leur cause directe et déterminante dans une ou plusieurs situations traumatisantes auxquelles le militaire en opération a été exposé, en particulier pendant des campagnes de guerre, la seule circonstance que les faits à l'origine des troubles n'aient pas été subis par le seul demandeur de la pension mais par d'autres militaires participant à ces opérations, ne suffit pas, à elle-seule, à écarter la preuve de l'imputabilité ;  <br/>
<br/>
              3. Considérant que pour juger qu'aucun droit à pension n'était ouvert à M. B... à raison de ses troubles anxieux et dépressifs, la cour régionale des pensions d'Aix-en-Provence s'est exclusivement fondée sur l'absence de preuve d'un événement particulier et personnel, que seul M. B...aurait subi, à l'origine des troubles ; qu'il résulte de ce qui a été dit ci-dessus que la circonstance que les faits à l'origine des troubles psychiques aient également été subis par d'autres militaires que le demandeur de la pension ne suffit pas, à elle seule, à écarter la preuve de l'imputabilité au service de tels troubles ; qu'il appartenait, par ailleurs, à la cour de rechercher si, en l'absence de fait traumatique précis constitutif d'une blessure, au sens du 1° de l'article L. 2 du code, l'affection dont était victime le demandeur pouvait néanmoins être regardée comme imputable au service au vu des éléments relatifs aux circonstances particulières dont il avait fait état et ouvrir droit à une pension sur le fondement du 2° du même article ; que, par suite, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, M. B... est fondé à demander l'annulation de l'arrêt de la cour pour erreur de droit ;<br/>
<br/>
              4. Considérant que, dans les circonstances de l'espèce, il y a lieu de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              5. Considérant qu'il résulte de l'instruction, en particulier du certificat du médecin chef du service de psychiatrie de l'hôpital d'instruction des armées Sainte-Anne à Toulon, en date du 25 mars 2008, que M. B...a subi une " décompensation psychologique brutale avec effondrement anxiodépressif en octobre 2007 " ; qu'eu égard à la date à laquelle ces troubles se sont déclarés, l'intéressé ne pouvait pas bénéficier de la présomption d'imputabilité prévue par l'article L. 3 du code des pensions militaires d'invalidité et des victimes de la guerre ; que M. B...a demandé, le 9 janvier 2009, le bénéfice d'une pension d'invalidité au titre de ces troubles ; que, lors de sa séance du 17 septembre 2009, la commission de réforme des pensions militaires d'invalidité de Marseille a reconnu à M. B...un taux d'invalidité de 50% pour troubles anxieux et dépressifs, mais sans droit à pension en l'absence de rattachement de la pathologie au service ; que M. B...a saisi le tribunal départemental des pensions militaires du Var de la décision du ministre de la défense du 1er décembre 2009 rejetant sa demande de pension, au motif que ses troubles anxieux et dépressifs ne pouvaient être rattachés de façon directe et déterminante à un fait précis de service ;<br/>
<br/>
              6. Considérant que, s'agissant des névroses traumatiques de guerre, le décret du 10 janvier 1992 déterminant les règles et barèmes pour la classification et l'évaluation des troubles psychiques de guerre prévoit que, compte tenu de la difficulté de prouver leur imputabilité au service, notamment du fait des longs délais d'apparition de ces troubles, l'expertise médicale peut accéder au rang de preuve décisive à la condition toutefois d'être fondée sur une argumentation rigoureuse établissant une causalité directe et déterminante entre les troubles psychiques constatés et le service ; <br/>
<br/>
              7. Considérant, d'une part, qu'il résulte de l'instruction, notamment du certificat médical du 25 mars 2008 déjà mentionné et de celui établi par le même médecin le 7 octobre 2010, que M.B..., lieutenant-colonel de l'armée de terre, a été confronté dans l'exercice de ses missions d'encadrement et, en dernier lieu, en Afghanistan du 5 août 2004 au 11 février 2005, à des situations répétées d'extrême tension à l'origine d'un syndrome clinique de stress post-traumatique ; que ce constat est corroboré par les témoignages concordants des autorités sous les ordres desquelles il a servi ; qu'ainsi, il résulte de l'instruction que les troubles psychiques constatés chez l'intéressé trouvent leur cause directe et déterminante dans les conditions particulières du service de M. B...; qu'il suit de là que, dans les circonstances particulières de l'espèce, la preuve de l'imputabilité au service de sa pathologie doit être regardée comme établie, contrairement à ce qu'a retenu le ministre de la défense dans sa décision du 1er décembre 2009 ; que, d'autre part, en l'absence de fait traumatique précis, l'affection de M. B...doit être regardée comme résultant d'une maladie et non d'une blessure ; qu'il résulte de tout ce qui précède que M. B...est fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal des pensions du Var a rejeté sa demande tendant à l'annulation de la décision du 1er décembre 2009 par laquelle le ministre de la défense lui a refusé le bénéfice d'une pension militaire d'invalidité ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour régionale des pensions d'Aix-en-Provence du 14 janvier 2013, le jugement du tribunal des pensions du Var du 14 mars 2011 et la décision du 1er décembre 2009 du ministre de la défense sont annulés. <br/>
<br/>
Article 2 : La présente décision sera notifiée à M. A...B...et au ministre de la défense. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">48-01-02-03-01 PENSIONS. PENSIONS MILITAIRES D'INVALIDITÉ ET DES VICTIMES DE GUERRE. CONDITIONS D'OCTROI D'UNE PENSION. IMPUTABILITÉ. LIEN DE CAUSALITÉ MÉDICALE. - PREUVE DE L'IMPUTABILITÉ AU SERVICE, EN L'ABSENCE DE PRÉSOMPTION LÉGALE - TROUBLES PSYCHIQUES - 1) MÉTHODE [RJ1] - 2) CAS OÙ LES FAITS À L'ORIGINE DES TROUBLES ONT ÉTÉ SUBIS NON SEULEMENT PAR LE DEMANDEUR MAIS AUSSI PAR D'AUTRES MILITAIRES PARTICIPANT AUX OPÉRATIONS - CIRCONSTANCE DE NATURE À ÉCARTER LA PREUVE DE L'IMPUTABILITÉ - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-07-03 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. POUVOIRS DU JUGE DE PLEIN CONTENTIEUX. - JURIDICTION DES PENSIONS - PENSIONS MILITAIRES D'INVALIDITÉ ET DES VICTIMES DE GUERRE - PREUVE DE L'IMPUTABILITÉ AU SERVICE, EN L'ABSENCE DE PRÉSOMPTION LÉGALE - TROUBLES PSYCHIQUES - 1) MÉTHODE [RJ1] - 2) CAS OÙ LES FAITS À L'ORIGINE DES TROUBLES ONT ÉTÉ SUBIS NON SEULEMENT PAR LE DEMANDEUR MAIS AUSSI PAR D'AUTRES MILITAIRES PARTICIPANT AUX OPÉRATIONS - CIRCONSTANCE DE NATURE À ÉCARTER LA PREUVE DE L'IMPUTABILITÉ - ABSENCE.
</SCT>
<ANA ID="9A"> 48-01-02-03-01 1) Il résulte des dispositions combinées des articles L. 2 et L. 3 du code des pensions militaires d'invalidité et des victimes de la guerre que, lorsque le demandeur d'une pension ne peut pas bénéficier de la présomption légale d'imputabilité au service, il incombe à ce dernier d'apporter la preuve de cette imputabilité par tous moyens de nature à emporter la conviction des juges. Dans les cas où sont en cause des troubles psychiques, il appartient aux juges du fond de prendre en considération l'ensemble des éléments du dossier permettant d'établir que ces troubles sont imputables à un fait précis ou à des circonstances particulières de service.... ,,2) Lorsqu'il est établi que les troubles psychiques trouvent leur cause directe et déterminante dans une ou plusieurs situations traumatisantes auxquelles le militaire en opération a été exposé, en particulier pendant des campagnes de guerre, la circonstance que les faits à l'origine des troubles n'aient pas été subis par le seul demandeur de la pension mais par d'autres militaires participant à ces opérations, ne suffit pas, à elle-seule, à écarter la preuve de l'imputabilité.</ANA>
<ANA ID="9B"> 54-07-03 1) Il résulte des dispositions combinées des articles L. 2 et L. 3 du code des pensions militaires d'invalidité et des victimes de la guerre que, lorsque le demandeur d'une pension ne peut pas bénéficier de la présomption légale d'imputabilité au service, il incombe à ce dernier d'apporter la preuve de cette imputabilité par tous moyens de nature à emporter la conviction des juges. Dans les cas où sont en cause des troubles psychiques, il appartient aux juges du fond de prendre en considération l'ensemble des éléments du dossier permettant d'établir que ces troubles sont imputables à un fait précis ou à des circonstances particulières de service.... ,,2) Lorsqu'il est établi que les troubles psychiques trouvent leur cause directe et déterminante dans une ou plusieurs situations traumatisantes auxquelles le militaire en opération a été exposé, en particulier pendant des campagnes de guerre, la circonstance que les faits à l'origine des troubles n'aient pas été subis par le seul demandeur de la pension mais par d'autres militaires participant à ces opérations, ne suffit pas, à elle-seule, à écarter la preuve de l'imputabilité.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., pour les affections à évolution lente liées à l'exposition de substances toxiques, CE, 29 avril 2013, Mme Le Goascoz, veuve Pitor et Mme Pitor, n° 344749, T. p. 111.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
