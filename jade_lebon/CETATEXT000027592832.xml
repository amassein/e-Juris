<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027592832</ID>
<ANCIEN_ID>JG_L_2013_06_000000349730</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/59/28/CETATEXT000027592832.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Section du Contentieux, 21/06/2013, 349730, Publié au recueil Lebon</TITRE>
<DATE_DEC>2013-06-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>349730</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Section du Contentieux</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Leïla Derouich</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESEC:2013:349730.20130621</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu l'ordonnance n° 1100681 du 23 mai 2011, enregistrée le 30 mai 2011 au secrétariat du contentieux du Conseil d'Etat, par laquelle le président du tribunal administratif de Nancy a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, la requête présentée à ce tribunal par Mme B... A...;<br/>
<br/>
              Vu la requête, enregistrée le 18 avril 2011 au greffe du tribunal administratif de Nancy, présentée par Mme B... A..., demeurant... ; Mme A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision du 16 février 2011 par laquelle le ministre de l'intérieur, de l'outre-mer, des collectivités territoriales et de l'immigration a refusé de soumettre au comité de sélection sa candidature au recrutement au tour extérieur des administrateurs civils pour l'année 2011 ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 1 200 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le décret n° 99-945 du 16 novembre 1999 ;<br/>
<br/>
              Vu le décret n° 2005-716 du 29 juin 2005 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Leïla Derouich, Auditeur,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article 5 du décret du 16 novembre 1999 portant statut particulier du corps des administrateurs civils : " Les administrateurs civils sont recrutés parmi les anciens élèves de l'Ecole nationale d'administration ; ils sont nommés et titularisés en cette qualité à compter du lendemain du dernier jour de leur scolarité à l'école. / En outre, peuvent être nommés au choix dans le corps des administrateurs civils des fonctionnaires de l'Etat de catégorie A ou des fonctionnaires et agents en fonction dans une organisation internationale intergouvernementale justifiant, au 1er janvier de l'année considérée, de huit ans de services effectifs dans un corps de catégorie A ou sur un emploi de catégorie A ou assimilé. (...) " ;<br/>
<br/>
              2. Considérant que MmeA..., commandant de police, a présenté sa candidature, au titre de l'année 2011, au recrutement au tour extérieur des administrateurs civils prévu par les dispositions du deuxième alinéa de l'article 5 du décret du 16 novembre 1999 ; que, par une décision du 16 février 2011, le ministre de l'intérieur, de l'outre-mer, des collectivités territoriales et de l'immigration a refusé de soumettre sa candidature au comité de sélection, au motif que si les services qu'elle avait accomplis dans le corps de commandement de la police nationale depuis la création de ce corps par le décret du 29 juin 2005 étaient assimilables à des services effectifs dans un corps de catégorie A, il n'en allait pas de même des services qu'elle avait accomplis antérieurement dans le corps de commandement et d'encadrement de la police nationale régi par le décret du 9 mai 1995, si bien qu'elle ne justifiait pas de huit ans de services effectifs dans un corps de catégorie A ; que Mme A... demande l'annulation pour excès de pouvoir de cette décision ;<br/>
<br/>
              Sur la compétence du Conseil d'Etat en premier et dernier ressort :<br/>
<br/>
              3. Considérant qu'aux termes de l'article R. 311-1 du code de justice administrative dans sa rédaction issue du décret du 22 février 2010 : " Le Conseil d'Etat est compétent pour connaître en premier et dernier ressort : ... 3° Des litiges concernant le recrutement et la discipline des agents publics nommés par décret du Président de la République en vertu des dispositions de l'article 13 (3e alinéa) de la Constitution et des articles 1er et 2 de l'ordonnance n° 58-1136 du 28 novembre 1958 portant loi organique concernant les nominations aux emplois civils et militaires de l'Etat " ; que ces dispositions donnent compétence au Conseil d'Etat pour connaître en premier et dernier ressort de l'ensemble des litiges concernant le recrutement et la discipline des agents publics qu'elles mentionnent ; qu'il en résulte que, lorsqu'un concours de recrutement ou une procédure de sélection commande l'accès, fût-ce au terme d'une période de formation, à un corps de fonctionnaires nommés par décret du Président de la République, un litige relatif soit à un refus d'admission à concourir, soit aux résultats du concours ou de la sélection ressortit à la compétence de premier et dernier ressort du Conseil d'Etat ;<br/>
<br/>
              4. Considérant qu'aux termes de l'article 2 de l'ordonnance de 1958, sont nommés par décret du Président de la République : " à leur entrée dans leurs corps respectifs, les membres des corps dont le recrutement est normalement assuré par l'Ecole nationale d'administration " ; que le recrutement des membres de leur corps étant normalement assuré par l'Ecole nationale d'administration, les administrateurs civils sont nommés par décret du Président de la République en application de ces dispositions ; que, par suite, le litige né du refus du ministre de l'intérieur, de l'outre-mer, des collectivités territoriales et de l'immigration de soumettre la candidature de Mme A...au comité de sélection pour le recrutement au tour extérieur des administrateurs civils est au nombre de ceux dont, en vertu des dispositions précitées du 3° de l'article R. 311-1 du code de justice administrative, il appartient au Conseil d'Etat de connaître en premier et dernier ressort ;<br/>
<br/>
              Sur la légalité de la décision attaquée :<br/>
<br/>
              5. Considérant que Mme A... soutient que les services accomplis dans le corps de commandement et d'encadrement de la police nationale doivent bénéficier de la même assimilation à des services effectifs dans un corps de catégorie A que ceux accomplis dans le corps de commandement, dès lors qu'aux termes de l'article 20 du décret du 29 juin 2005 : " Les services accomplis dans le corps et les grades régis par les dispositions du décret n° 95-656 du 9 mai 1995 sont assimilés à des services accomplis dans le corps et les grades prévus par le présent décret " ; que, toutefois, ces dispositions ont uniquement pour objet de permettre la prise en compte des services accomplis dans le corps existant avant l'intervention du décret du 29 juin 2005 pour l'ancienneté, le classement et l'avancement dans le corps créé par ce décret ; qu'elles n'ont ni pour objet ni pour effet de les faire regarder comme des services effectifs dans le nouveau corps pour l'application de l'article 5 du décret du 16 novembre 1999 ; qu'ainsi, Mme A... n'est pas fondée à soutenir que la décision du 16 février 2011 méconnaîtrait les dispositions qu'elle invoque ; que ses conclusions tendant à l'annulation de cette décision doivent, par suite, être rejetées ;<br/>
<br/>
              Sur les conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              6. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de Mme A... est rejetée.<br/>
Article 2 : La présente décision sera notifiée à Mme B... A...et au ministre de l'intérieur.<br/>
Copie en sera adressée pour information à la ministre de la réforme de l'Etat, de la décentralisation et de la fonction publique.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">17-05-01 COMPÉTENCE. COMPÉTENCE À L'INTÉRIEUR DE LA JURIDICTION ADMINISTRATIVE. COMPÉTENCE EN PREMIER RESSORT DES TRIBUNAUX ADMINISTRATIFS. - 1) CORPS DE FONCTIONNAIRES NOMMÉS PAR DÉCRET DU PRÉSIDENT DE LA RÉPUBLIQUE AUQUEL L'ACCÈS EST COMMANDÉ PAR UN CONCOURS DE RECRUTEMENT OU UNE PROCÉDURE DE SÉLECTION - LITIGES RELATIFS À L'ADMISSION À CONCOURIR OU AUX RÉSULTATS DU CONCOURS ET DE LA SÉLECTION - EXCLUSION [RJ1] - 2) ESPÈCE - LITIGE NÉ DU REFUS DE SOUMETTRE UNE CANDIDATURE AU COMITÉ DE SÉLECTION POUR LE RECRUTEMENT AU TOUR EXTÉRIEUR DES ADMINISTRATEURS CIVILS - EXCLUSION [RJ2].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">17-05-02-02 COMPÉTENCE. COMPÉTENCE À L'INTÉRIEUR DE LA JURIDICTION ADMINISTRATIVE. COMPÉTENCE DU CONSEIL D'ETAT EN PREMIER ET DERNIER RESSORT. LITIGES RELATIFS À LA SITUATION INDIVIDUELLE DES FONCTIONNAIRES NOMMÉS PAR DÉCRET DU PRÉSIDENT DE LA RÉPUBLIQUE. - LITIGES CONCERNANT LA DISCIPLINE DE CES AGENTS PUBLICS (ART. R. 311-1, 3° DU CJA) - 1) CORPS DE FONCTIONNAIRES NOMMÉS PAR DÉCRET DU PRÉSIDENT DE LA RÉPUBLIQUE AUQUEL L'ACCÈS EST COMMANDÉ PAR UN CONCOURS DE RECRUTEMENT OU UNE PROCÉDURE DE SÉLECTION - LITIGES RELATIFS À L'ADMISSION À CONCOURIR OU AUX RÉSULTATS DU CONCOURS ET DE LA SÉLECTION - INCLUSION [RJ1] - 2) ESPÈCE - LITIGE NÉ DU REFUS DE SOUMETTRE UNE CANDIDATURE AU COMITÉ DE SÉLECTION POUR LE RECRUTEMENT AU TOUR EXTÉRIEUR DES ADMINISTRATEURS CIVILS - INCLUSION [RJ2].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">36-03-02 FONCTIONNAIRES ET AGENTS PUBLICS. ENTRÉE EN SERVICE. CONCOURS ET EXAMENS PROFESSIONNELS. - LITIGE RELATIF AUX RÉSULTATS - CONCOURS D'ACCÈS À UN CORPS DE FONCTIONNAIRES NOMMÉS PAR DÉCRET DU PRÉSIDENT DE LA RÉPUBLIQUE - COMPÉTENCE DE PREMIER ET DERNIER RESSORT DU CONSEIL D'ETAT (ART. R. 331-1, 3°) - EXISTENCE [RJ1].
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">36-03-02-01 FONCTIONNAIRES ET AGENTS PUBLICS. ENTRÉE EN SERVICE. CONCOURS ET EXAMENS PROFESSIONNELS. ADMISSION À CONCOURIR. - LITIGE - COMPÉTENCE DE PREMIER ET DERNIER RESSORT DU CONSEIL D'ETAT (ART. R. 331-1, 3°) - 1) ADMISSION À CONCOURIR POUR L'ACCÈS À UN CORPS DE FONCTIONNAIRES NOMMÉS PAR DÉCRET DU PRÉSIDENT DE LA RÉPUBLIQUE - EXISTENCE [RJ1] - 2) ESPÈCE - LITIGE NÉ DU REFUS DE SOUMETTRE UNE CANDIDATURE AU COMITÉ DE SÉLECTION POUR LE RECRUTEMENT AU TOUR EXTÉRIEUR DES ADMINISTRATEURS CIVILS - EXISTENCE [RJ2].
</SCT>
<ANA ID="9A"> 17-05-01 1) Il résulte des dispositions du 3° de l'article R. 311-1 du code de justice administrative (CJA) que lorsqu'un concours de recrutement ou une procédure de sélection commande l'accès, fût-ce au terme d'une période de formation, à un corps de fonctionnaires nommés par décret du Président de la République en vertu du troisième alinéa de l'article 13 de la Constitution ou des articles 1er et 2 de l'ordonnance n° 58-1136 du 28 novembre 1958 portant loi organique concernant les nominations aux emplois civils et militaires de l'Etat, un litige relatif soit à un refus d'admission à concourir, soit aux résultats du concours ou de la sélection ressortit à la compétence de premier et dernier ressort du Conseil d'Etat.,,,2) Il en va ainsi du litige né du refus du ministre de l'intérieur de soumettre une candidature au comité de sélection pour le recrutement au tour extérieur des administrateurs civils, les membres de ce corps étant nommés par décret du Président de la République en application de l'article 2 de l'ordonnance du 28 novembre 1958 dès lors que leur recrutement est normalement assuré par l'école nationale d'administration (ENA).</ANA>
<ANA ID="9B"> 17-05-02-02 1) Il résulte des dispositions du 3° de l'article R. 311-1 du code de justice administrative (CJA) que lorsqu'un concours de recrutement ou une procédure de sélection commande l'accès, fût-ce au terme d'une période de formation, à un corps de fonctionnaires nommés par décret du Président de la République en vertu du troisième alinéa de l'article 13 de la Constitution ou des articles 1er et 2 de l'ordonnance n° 58-1136 du 28 novembre 1958 portant loi organique concernant les nominations aux emplois civils et militaires de l'Etat, un litige relatif soit à un refus d'admission à concourir, soit aux résultats du concours ou de la sélection ressortit à la compétence de premier et dernier ressort du Conseil d'Etat.,,,2) Il en va ainsi du litige né du refus du ministre de l'intérieur de soumettre une candidature au comité de sélection pour le recrutement au tour extérieur des administrateurs civils, les membres de ce corps étant nommés par décret du Président de la République en application de l'article 2 de l'ordonnance du 28 novembre 1958 dès lors que leur recrutement est normalement assuré par l'école nationale d'administration (ENA).</ANA>
<ANA ID="9C"> 36-03-02 Il résulte des dispositions du 3° de l'article R. 311-1 du code de justice administrative (CJA) que lorsqu'un concours de recrutement ou une procédure de sélection commande l'accès, fût-ce au terme d'une période de formation, à un corps de fonctionnaires nommés par décret du Président de la République en application du troisième alinéa de l'article 13 de la Constitution ou des articles 1er et 2 de l'ordonnance n° 58-1136 du 28 novembre 1958 portant loi organique concernant les nominations aux emplois civils et militaires de l'Etat, un litige relatif soit à un refus d'admission à concourir, soit aux résultats du concours ou de la sélection ressortit à la compétence de premier et dernier ressort du Conseil d'Etat.</ANA>
<ANA ID="9D"> 36-03-02-01 1) Il résulte des dispositions du 3° de l'article R. 311-1 du code de justice administrative (CJA) que lorsqu'un concours de recrutement ou une procédure de sélection commande l'accès, fût-ce au terme d'une période de formation, à un corps de fonctionnaires nommés par décret du Président de la République en application du troisième alinéa de l'article 13 de la Constitution ou des articles 1er et 2 de l'ordonnance n° 58-1136 du 28 novembre 1958 portant loi organique concernant les nominations aux emplois civils et militaires de l'Etat, un litige relatif soit à un refus d'admission à concourir, soit aux résultats du concours ou de la sélection ressortit à la compétence de premier et dernier ressort du Conseil d'Etat.,,,2) Il en va ainsi du litige né du refus du ministre de l'intérieur de soumettre une candidature au comité de sélection pour le recrutement au tour extérieur des administrateurs civils, les membres de ce corps étant nommés par décret du Président de la République en application de l'article 2 de l'ordonnance du 28 novembre 1958 dès lors que leur recrutement est normalement assuré par l'école nationale d'administration.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., sous l'empire de l'article R. 311-1 du CJA dans sa rédaction antérieure au décret n° 2010-164 du 22 février 2010, pour l'assimilation d'un litige relatif au refus d'ouvrir à une personne l'accès à un corps de fonctionnaires nommés par décret du Président de la République à un litige relatif à la situation individuelle d'un fonctionnaire nommé par décret du Président de la République, CE, 9 juin 1971, Sieur,, n° 0079, p. 428. Cf., s'agissant de la compétence de premier ressort du Conseil d'Etat pour connaître d'un litige relatif à un refus de proposer au Président de la République la nomination d'une personne dans un corps dont les membres sont nommés par décret du Président de la République, CE, 23 octobre 1985,,et autres, n°s 42752,42753, T. p. 549. Ab. jur., s'agissant de la compétence du tribunal administratif pour connaître en premier ressort du refus d'admission à concourir pour l'accès à des corps de fonctionnaires dont les membres sont nommés, après une période de formation ou de stage, par décret du Président de la République, CE, 15 octobre 1986,,, n° 73140, T. p. 577 sur un autre point (à propos du troisième concours de l'ENA) ; CE, 18 mars 1983,,, n° 34782, p. 125 (à propos du concours d'entrée à l'école nationale de la magistrature) ; CE, 17 mai 1999,,, n° 199154, T. p. 717 (à propos du concours exceptionnel de recrutement des magistrats).,,,[RJ2] Cf., s'agissant du raisonnement corps par corps et non agent par agent pour la qualification de membres d'un corps nommés par décret du Président de la République, CE, Assemblée, 15 mai 1981,,, n° 3304, p. 221 ; pour l'application de ce raisonnement aux membres du corps des administrateurs civils, CE, Section, 6 février 1970, Association des administrateurs civils du Ministère du Travail et de la Caisse nationale de sécurité sociale, anciens élèves de l'E.N.A., n° 70298, p. 89.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
