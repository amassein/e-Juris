<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038212324</ID>
<ANCIEN_ID>JG_L_2019_03_000000417629</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/21/23/CETATEXT000038212324.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 07/03/2019, 417629, Publié au recueil Lebon</TITRE>
<DATE_DEC>2019-03-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>417629</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MONOD, COLIN, STOCLET</AVOCATS>
<RAPPORTEUR>M. Laurent Domingo</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:417629.20190307</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              L'association " Bien Vivre à Garbejaïre Valbonne " et Mme A... B...ont demandé au tribunal administratif de Nice d'annuler, d'une part, la délibération du 22 juin 2012 par laquelle le conseil municipal de la commune de Valbonne a décidé la mise à disposition d'un local situé rue Henri Barbara au bénéfice de l'association " Musulmans de Valbonne Sophia Antipolis " et a autorisé le maire ou son représentant à signer une convention d'occupation et, d'autre part, la décision du maire de la commune de Valbonne du 31 juillet 2012 de conclure une convention d'occupation avec cette association. Par un jugement nos 1202924 et 1203424 du 29 novembre 2016, le tribunal a annulé la délibération du 22 juin 2012 et la décision du 31 juillet 2012 et a enjoint à la commune et à l'association, sauf accord des parties pour mettre fin à leurs relations contractuelles, de saisir le juge du contrat dans un délai de trois mois.<br/>
<br/>
              Par un arrêt nos 17MA00659-17MA00661 du 27 novembre 2017, la cour administrative d'appel de Marseille a rejeté l'appel formé par la commune de Valbonne contre ce jugement et constaté qu'il n'y avait pas lieu de statuer sur la demande de sursis à exécution de ce jugement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un nouveau mémoire, enregistrés les 25 janvier, 19 avril et 15 novembre 2018 au secrétariat du contentieux du Conseil d'Etat, la commune de Valbonne demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'association " Bien Vivre à Garbejaïre Valbonne " et de Mme B... la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.  <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des collectivités territoriales ;<br/>
              - le code général de la propriété des personnes publiques ;<br/>
              - la loi du 9 décembre 1905 concernant la séparation des Eglises et de l'Etat ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Laurent Domingo, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Monod, Colin, Stoclet, avocat de la commune de Valbonne.<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par une délibération du 22 juin 2012, le conseil municipal de la commune de Valbonne a décidé de louer un local situé rue Henri Barbara à l'association " Musulmans de Valbonne Sophia Antipolis " en vue de l'exercice d'activités cultuelles et a autorisé le maire ou son représentant à signer une convention de location. Le 31 juillet 2012, le maire de la commune de Valbonne a signé cette convention avec l'association. Par un jugement du 29 novembre 2016, le tribunal administratif de Nice a, sur la demande de l'association " Bien Vivre à Garbejaïre Valbonne " et de Mme A...B..., annulé la délibération du conseil municipal du 22 juin 2012 et la décision du maire du 31 juillet 2012. La commune de Valbonne se pourvoit en cassation contre l'arrêt du 27 novembre 2017 par lequel la cour administrative d'appel de Marseille a rejeté son appel contre ce jugement.<br/>
<br/>
              Sur la compétence de la juridiction administrative : <br/>
<br/>
              2. Il ressort des pièces du dossier soumis au juge du fond que les locaux mis à la disposition de l'association " Musulmans de Valbonne Sophia Antipolis ", anciennement utilisés pour les besoins d'un restaurant interentreprises et n'ayant pas fait l'objet d'une affectation à l'usage direct du public, ni à un service public, appartiennent, ainsi que l'a jugé le tribunal administratif, au domaine privé de la commune de Valbonne, sans qu'ait d'incidence à cet égard ni la circonstance que la commune de Valbonne a mis à disposition du centre hospitalier d'Antibes des locaux situés au sous-sol du même immeuble disposant d'un accès distinct, ni l'existence d'un projet d'installation dans les locaux en litige d'une gendarmerie qui ne peut être regardé, ainsi que l'ont souverainement relevé les juges du fond, comme entrepris de façon certaine. <br/>
<br/>
              3. Si la contestation par une personne privée de l'acte, délibération ou décision du maire, par lequel une commune ou son représentant, gestionnaire du domaine privé, initie avec cette personne, conduit ou termine une relation contractuelle dont l'objet est la valorisation ou la protection de ce domaine et qui n'affecte ni son périmètre ni sa consistance relève de la compétence du juge judiciaire, la juridiction administrative est, contrairement à ce que soutient la commune de Valbonne, compétente pour connaître de la demande formée par un tiers tendant à l'annulation de la délibération d'un conseil municipal autorisant la conclusion d'une convention ayant pour objet la mise à disposition d'une dépendance du domaine privé communal et de la décision du maire de la signer. La commune requérante n'est par suite pas fondée à soutenir que la cour administrative d'appel de Marseille aurait commis une erreur de droit en s'abstenant de relever d'office l'incompétence de la juridiction administrative pour se prononcer sur le recours de l'association " Bien Vivre à Garbejaïre Valbonne " et Mme A...B....<br/>
<br/>
              Sur le bien fondé de l'arrêt attaqué : <br/>
<br/>
              4. Aux termes de l'article 1er de la loi du 9 décembre 1905 concernant la séparation des Eglises et de l'Etat : " La République assure la liberté de conscience. Elle garantit le libre exercice des cultes sous les seules restrictions édictées ci après dans l'intérêt de l'ordre public ". L'article 2 de cette loi dispose : " La République ne reconnaît, ne salarie ni ne subventionne aucun culte. En conséquence, à partir du 1er janvier qui suivra la promulgation de la présente loi, seront supprimées des budgets de l'Etat, des départements et des communes, toutes dépenses relatives à l'exercice des cultes ". Aux termes du dernier alinéa de l'article 19 de cette même loi, les associations formées pour subvenir aux frais, à l'entretien et à l'exercice d'un culte " ne pourront, sous quelque forme que ce soit, recevoir des subventions de l'Etat, des départements et des communes. Ne sont pas considérées comme subventions les sommes allouées pour réparations aux édifices affectés au culte public, qu'ils soient ou non classés monuments historiques ". <br/>
<br/>
              5. L'article L. 2144-3 du code général des collectivités territoriales prévoit que : " Des locaux communaux peuvent être utilisés par les associations, syndicats ou partis politiques qui en font la demande. / Le maire détermine les conditions dans lesquelles ces locaux peuvent être utilisés, compte tenu des nécessités de l'administration des propriétés communales, du fonctionnement des services et du maintien de l'ordre public. / Le conseil municipal fixe, en tant que de besoin, la contribution due à raison de cette utilisation ". Sont regardés comme des locaux communaux, au sens et pour l'application de ces dispositions, les locaux affectés aux services publics communaux.<br/>
<br/>
              6. D'une part, ces dispositions permettent à une commune, en tenant compte des nécessités qu'elles mentionnent, d'autoriser, dans le respect du principe de neutralité à l'égard des cultes et du principe d'égalité, l'utilisation pour l'exercice d'un culte par une association d'un local communal, tel que défini au point 5, dès lors que les conditions financières de cette autorisation excluent toute libéralité et, par suite, toute aide à un culte. Une commune ne peut rejeter une demande d'utilisation d'un tel local au seul motif que cette demande lui est adressée par une association dans le but d'exercer un culte. En revanche, une commune ne peut, sans méconnaître ces dispositions, décider qu'un local lui appartenant relevant des dispositions précitées de l'article L. 2144-3 du code général des collectivités territoriales sera laissé de façon exclusive et pérenne à la disposition d'une association pour l'exercice d'un culte et constituera ainsi un édifice cultuel.<br/>
<br/>
              7. D'autre part, les collectivités territoriales peuvent donner à bail, et ainsi pour un usage exclusif et pérenne, à une association cultuelle un local existant de leur domaine privé sans méconnaître les dispositions précitées de la loi du 9 décembre 1905 dès lors que les conditions, notamment financières, de cette location excluent toute libéralité.<br/>
<br/>
              8. Pour confirmer l'annulation de la délibération du 22 juin 2012 et de la décision du 31 juillet 2012 prononcée par le jugement du tribunal administratif de Nice du 29 novembre 2016, la cour administrative d'appel de Marseille s'est fondée sur ce que la commune de Valbonne ne pouvait mettre à disposition exclusive et pérenne au profit d'une association cultuelle des locaux appartenant à cette commune. Il résulte de la règle rappelée au point 7 que ce faisant la cour a commis une erreur de droit dès lors que, ainsi qu'il a été dit au point 2, les locaux mis à la disposition de l'association " Musulmans de Valbonne Sophia Antipolis " appartiennent au domaine privé de la commune et ne relèvent pas des dispositions précitées de l'article L. 2144-3 du code général des collectivités territoriales.<br/>
<br/>
              9. Il résulte de ce qui précède, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que la commune de Valbonne est fondée à demander l'annulation de l'arrêt qu'elle attaque.<br/>
<br/>
              10. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par la commune de Valbonne au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>                 D E C I D E :<br/>
                               --------------<br/>
<br/>
Article 1er : L'arrêt nos 17MA00659-17MA00661 du 27 novembre 2017 de la cour administrative d'appel de Marseille est annulé.<br/>
<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Marseille.<br/>
<br/>
Article 3 : Le surplus des conclusions du pourvoi de la commune de Valbonne est rejeté.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la commune de Valbonne, à l'association " Bien Vivre à Garbejaïre Valbonne ", à Mme A... B... et à l'Association " Musulmans de Valbonne Sophia Antipolis ".<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">135-01 COLLECTIVITÉS TERRITORIALES. DISPOSITIONS GÉNÉRALES. - 1) MISE À DISPOSITION PAR LES COMMUNES DE LEURS LOCAUX (ART. L. 2144-3 DU CGCT) [RJ1] - NOTION DE LOCAUX AU SENS DE CET ARTICLE - LOCAUX AFFECTÉS AUX SERVICES PUBLICS COMMUNAUX - A) POSSIBILITÉ DE METTRE CES LOCAUX À DISPOSITION D'UNE ASSOCIATION CULTUELLE POUR L'EXERCICE D'UN CULTE - EXISTENCE, À CONDITION QUE LES CONDITIONS FINANCIÈRES DE CETTE MISE À DISPOSITION RESPECTENT LE PRINCIPE D'ÉGALITÉ ET EXCLUENT TOUTE LIBÉRALITÉ - B) POSSIBILITÉ POUR UNE COMMUNE DE REJETER UNE DEMANDE D'UTILISATION DE CES LOCAUX AU MOTIF QUE CETTE DEMANDE LUI EST ADRESSÉE PAR UNE ASSOCIATION DANS LE BUT D'EXERCER UN CULTE - ABSENCE - C) POSSIBILITÉ DE LAISSER CES LOCAUX DE FAÇON EXCLUSIVE ET PÉRENNE À LA DISPOSITION D'UNE ASSOCIATION POUR L'EXERCICE D'UN CULTE - ABSENCE - 2) POSSIBILITÉ DE LAISSER DES LOCAUX DU DOMAINE PRIVÉ, DE FAÇON EXCLUSIVE ET PÉRENNE, À LA DISPOSITION D'UNE ASSOCIATION CULTUELLE - EXISTENCE, SAUF LIBÉRALITÉ.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">17-03-02-02-01 COMPÉTENCE. RÉPARTITION DES COMPÉTENCES ENTRE LES DEUX ORDRES DE JURIDICTION. COMPÉTENCE DÉTERMINÉE PAR UN CRITÈRE JURISPRUDENTIEL. DOMAINE. DOMAINE PRIVÉ. - CONTESTATION PAR UNE PERSONNE PRIVÉE DE L'ACTE PAR LEQUEL UNE COMMUNE INITIE, CONDUIT OU TERMINE AVEC ELLE UNE RELATION CONTRACTUELLE AYANT POUR OBJET LA VALORISATION OU LA PROTECTION DE SON DOMAINE PRIVÉ, SANS AFFECTER SON PÉRIMÈTRE NI SA CONSISTANCE - COMPÉTENCE JUDICIAIRE [RJ2] - CONTESTATION PAR UN TIERS DE LA DÉLIBÉRATION D'UN CONSEIL MUNICIPAL AUTORISANT LA CONCLUSION D'UNE CONVENTION AYANT POUR OBJET LA MISE À DISPOSITION D'UNE DÉPENDANCE DU DOMAINE PRIVÉ COMMUNAL ET DE LA DÉCISION DU MAIRE DE LA SIGNER - COMPÉTENCE DU JUGE ADMINISTRATIF.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">21-01 CULTES. EXERCICE DES CULTES. - 1) MISE À DISPOSITION PAR LES COMMUNES DE LEURS LOCAUX (ART. L. 2144-3 DU CGCT) [RJ1] - NOTION DE LOCAUX AU SENS DE CET ARTICLE - LOCAUX AFFECTÉS AUX SERVICES PUBLICS COMMUNAUX - A) POSSIBILITÉ DE METTRE CES LOCAUX À DISPOSITION D'UNE ASSOCIATION CULTUELLE POUR L'EXERCICE D'UN CULTE - EXISTENCE, À CONDITION QUE LES CONDITIONS FINANCIÈRES DE CETTE MISE À DISPOSITION RESPECTENT LE PRINCIPE D'ÉGALITÉ ET EXCLUENT TOUTE LIBÉRALITÉ - B) POSSIBILITÉ POUR UNE COMMUNE DE REJETER UNE DEMANDE D'UTILISATION DE CES LOCAUX AU MOTIF QUE CETTE DEMANDE LUI EST ADRESSÉE PAR UNE ASSOCIATION DANS LE BUT D'EXERCER UN CULTE - ABSENCE - C) POSSIBILITÉ DE LAISSER CES LOCAUX DE FAÇON EXCLUSIVE ET PÉRENNE À LA DISPOSITION D'UNE ASSOCIATION POUR L'EXERCICE D'UN CULTE - ABSENCE - 2) POSSIBILITÉ DE LAISSER DES LOCAUX DU DOMAINE PRIVÉ, DE FAÇON EXCLUSIVE ET PÉRENNE, À LA DISPOSITION D'UNE ASSOCIATION CULTUELLE - EXISTENCE, SAUF LIBÉRALITÉ.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">24-01-01-01-01 DOMAINE. DOMAINE PUBLIC. CONSISTANCE ET DÉLIMITATION. DOMAINE PUBLIC ARTIFICIEL. BIENS FAISANT PARTIE DU DOMAINE PUBLIC ARTIFICIEL. - RÉGIME DU CG3P - BIEN DONT L'AFFECTATION AU SERVICE PUBLIC EST DÉCIDÉE ET DONT L'AMÉNAGEMENT INDISPENSABLE PEUT ÊTRE REGARDÉ COMME ENTREPRIS DE FAÇON CERTAINE - INCLUSION (DOMAINE PUBLIC VIRTUEL) [RJ3] - BIEN DONT L'AFFECTATION AU SERVICE PUBLIC EST INCERTAINE - EXCLUSION.
</SCT>
<SCT ID="8E" TYPE="PRINCIPAL">24-02-03 DOMAINE. DOMAINE PRIVÉ. CONTENTIEUX. - CONTESTATION PAR UNE PERSONNE PRIVÉE DE L'ACTE PAR LEQUEL UNE COMMUNE INITIE, CONDUIT OU TERMINE AVEC ELLE UNE RELATION CONTRACTUELLE AYANT POUR OBJET LA VALORISATION OU LA PROTECTION DE SON DOMAINE PRIVÉ, SANS AFFECTER SON PÉRIMÈTRE NI SA CONSISTANCE - COMPÉTENCE JUDICIAIRE [RJ2] - CONTESTATION PAR UN TIERS DE LA DÉLIBÉRATION D'UN CONSEIL MUNICIPAL AUTORISANT LA CONCLUSION D'UNE CONVENTION AYANT POUR OBJET LA MISE À DISPOSITION D'UNE DÉPENDANCE DU DOMAINE PRIVÉ COMMUNAL ET DE LA DÉCISION DU MAIRE DE LA SIGNER - COMPÉTENCE DU JUGE ADMINISTRATIF.
</SCT>
<ANA ID="9A"> 135-01 1) Sont regardés comme des locaux communaux, au sens et pour l'application de l'article L. 2144-3 du code général des collectivités territoriales (CGCT), les locaux affectés aux services publics communaux.,,a) Les dispositions de l'article L. 2144-3 du CGCT permettent à une commune, en tenant compte des nécessités qu'elles mentionnent, d'autoriser, dans le respect du principe de neutralité à l'égard des cultes et du principe d'égalité, l'utilisation pour l'exercice d'un culte par une association d'un local communal, dès lors que les conditions financières de cette autorisation excluent toute libéralité et, par suite, toute aide à un culte.... ...b) Une commune ne peut rejeter une demande d'utilisation d'un tel local au seul motif que cette demande lui est adressée par une association dans le but d'exercer un culte.... ...c) En revanche, une commune ne peut, sans méconnaître ces dispositions, décider qu'un local lui appartenant relevant des dispositions de l'article L. 2144-3 du CGCT sera laissé de façon exclusive et pérenne à la disposition d'une association pour l'exercice d'un culte et constituera ainsi un édifice cultuel.,,2) Les collectivités territoriales peuvent donner à bail, et ainsi pour un usage exclusif et pérenne, à une association cultuelle un local existant de leur domaine privé sans méconnaître les dispositions des articles 1er et 2, et du dernier alinéa de l'article 19 de la loi du 9 décembre 1905 dès lors que les conditions, notamment financières, de cette location excluent toute libéralité.</ANA>
<ANA ID="9B"> 17-03-02-02-01 Si la contestation par une personne privée de l'acte, délibération ou décision du maire, par lequel une commune ou son représentant, gestionnaire du domaine privé, initie avec cette personne, conduit ou termine une relation contractuelle dont l'objet est la valorisation ou la protection de ce domaine et qui n'affecte ni son périmètre ni sa consistance relève de la compétence du juge judiciaire, la juridiction administrative est compétente pour connaître de la demande formée par un tiers tendant à l'annulation de la délibération d'un conseil municipal autorisant la conclusion d'une convention ayant pour objet la mise à disposition d'une dépendance du domaine privé communal et de la décision du maire de la signer.</ANA>
<ANA ID="9C"> 21-01 1) Sont regardés comme des locaux communaux, au sens et pour l'application de l'article L. 2144-3 du code général des collectivités territoriales (CGCT), les locaux affectés aux services publics communaux.,,a) Les dispositions de l'article L. 2144-3 du CGCT permettent à une commune, en tenant compte des nécessités qu'elles mentionnent, d'autoriser, dans le respect du principe de neutralité à l'égard des cultes et du principe d'égalité, l'utilisation pour l'exercice d'un culte par une association d'un local communal, dès lors que les conditions financières de cette autorisation excluent toute libéralité et, par suite, toute aide à un culte.... ...b) Une commune ne peut rejeter une demande d'utilisation d'un tel local au seul motif que cette demande lui est adressée par une association dans le but d'exercer un culte.... ...c) En revanche, une commune ne peut, sans méconnaître ces dispositions, décider qu'un local lui appartenant relevant des dispositions de l'article L. 2144-3 du CGCT sera laissé de façon exclusive et pérenne à la disposition d'une association pour l'exercice d'un culte et constituera ainsi un édifice cultuel.,,2) Les collectivités territoriales peuvent donner à bail, et ainsi pour un usage exclusif et pérenne, à une association cultuelle un local existant de leur domaine privé sans méconnaître les dispositions des articles 1er et 2, et du dernier alinéa de l'article 19 de la loi du 9 décembre 1905 dès lors que les conditions, notamment financières, de cette location excluent toute libéralité.</ANA>
<ANA ID="9D"> 24-01-01-01-01 Est sans incidence sur l'appartenance de locaux au domaine privé d'une commune l'existence d'un projet d'installation dans ces locaux d'une gendarmerie qui ne peut être regardé, ainsi que l'ont souverainement relevé les juges du fond, comme entrepris de façon certaine.</ANA>
<ANA ID="9E"> 24-02-03 Si la contestation par une personne privée de l'acte, délibération ou décision du maire, par lequel une commune ou son représentant, gestionnaire du domaine privé, initie avec cette personne, conduit ou termine une relation contractuelle dont l'objet est la valorisation ou la protection de ce domaine et qui n'affecte ni son périmètre ni sa consistance relève de la compétence du juge judiciaire, la juridiction administrative est compétente pour connaître de la demande formée par un tiers tendant à l'annulation de la délibération d'un conseil municipal autorisant la conclusion d'une convention ayant pour objet la mise à disposition d'une dépendance du domaine privé communal et de la décision du maire de la signer.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Assemblée, 19 juillet 2011, Commune de Montpellier, n° 313518, p. 398.,,[RJ2] Cf. TC, 22 novembre 2010, SARL Brasserie du Théâtre c/ Commune de Reims, n° 3764, p. 590,.,,[RJ3] Cf., CE, 13 avril 2016, Commune de Baillargues, n° 391431, p. 131.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
