<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036157830</ID>
<ANCIEN_ID>JG_L_2017_12_000000402474</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/15/78/CETATEXT000036157830.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 1ère chambres réunies, 06/12/2017, 402474</TITRE>
<DATE_DEC>2017-12-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>402474</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>M. Stéphane Decubber</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Louis Dutheillet de Lamothe</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:402474.20171206</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par un jugement n° 2015-0002 du 19 février 2015, la chambre régionale des comptes Nord-Pas-de-Calais-Picardie a déclaré MM. E...A...et B...D...et G...C...conjointement et solidairement gestionnaires de fait du collège William-Henri Classen à Ailly-sur-Noye (Somme) et leur a enjoint de produire les comptes de fait de l'établissement. <br/>
<br/>
              Par un arrêt n° S2016-1870 du 16 juin 2016, la Cour des comptes a rejeté leur appel formé contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 16 août et 16 novembre 2016 et le 20 septembre 2017 au secrétariat du contentieux du Conseil d'Etat, Mme C...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              -	le code des juridictions financières ;<br/>
              -	le code de l'éducation ; <br/>
              -	la loi n° 63-156 du 23 février 1963, notamment son article 60 ;<br/>
              -	le décret n° 62-1587 du 29 décembre 1962 ;<br/>
              -	le décret n° 92-681 du 20 juillet 1992 ;<br/>
              -	le décret n° 2012-1246 du 7 novembre 2012 ;<br/>
              -	l'instruction codificatrice N° 05-042-M9-R du 30 septembre 2005 ;<br/>
              -	le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Stéphane Decubber, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Louis Dutheillet de Lamothe, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Foussard, Froger, avocat de MmeC....<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il résulte des énonciations de l'arrêt attaqué que Mme C... a exercé, de 2004 à 2012, les fonctions de principale du collège William-Henri Classen à Ailly-sur-Noye (Somme), M. A...y exerçant les fonctions de régisseur de 2003 à 2012, M. D...étant alors agent comptable de ce collège ; que par un jugement du 19 février 2015, la chambre régionale des comptes Nord-Pas-de-Calais-Picardie a déclaré MmeC..., M. A...et M.D..., conjointement et solidairement gestionnaires de fait de cet établissement scolaire et leur a enjoint de produire les comptes de fait de l'établissement ; que la Cour des comptes a rejeté leur appel formé contre ce jugement par un arrêt du 16 juin 2016 contre lequel Mme C...se pourvoit en cassation ;<br/>
<br/>
              Sur la régularité de l'arrêt :<br/>
<br/>
              2. Considérant qu'aux termes de l'article R. 142-9 du code des juridictions financières : " I. - A l'audience publique, après l'exposé du rapporteur et les conclusions du représentant du ministère public, toute partie à l'instance peut formuler, soit en personne, soit par un avocat au Conseil d'Etat et à la Cour de cassation ou un avocat, des observations précisant celles fournies par écrit sur l'affaire qui la concerne. / A l'issue des débats, le président donne la parole à ces parties en dernier. / II. - La formation délibère ensuite sur le projet d'arrêt présenté par le réviseur ; elle examine les propositions du rapport sur chacun des griefs formulés par le réquisitoire du ministère public. (...) " ; qu'aux termes de l'article R. 142-11 du même code : " La Cour statue par un arrêt qui vise les comptes jugés, les pièces examinées ainsi que les dispositions législatives et réglementaires dont il fait application. (...) Mention est faite que le rapporteur et, le cas échéant, les personnes concernées ont été entendus, et que le représentant du ministère public a conclu. Les noms des magistrats de la formation de jugement qui ont participé au délibéré y sont mentionnés " ;<br/>
<br/>
              3. Considérant que Mme C...soutient que l'arrêt attaqué serait irrégulier au motif qu'il résulterait de ses visas que Mme Froment-Meurice aurait pris la parole après les parties ; qu'il résulte toutefois des visas de l'arrêt attaqué que la mention, après les indications relatives au déroulement de l'audience publique, de l'audition de Mme Froment-Meurice, présidente de chambre maintenue en activité, avait trait non pas au déroulement de l'audience publique mais à celui du délibéré, au cours duquel cette magistrate est intervenue en qualité de réviseur, conformément aux dispositions du II de l'article R. 142-9 du code des juridictions financières citées au point précédent ; que, dès lors, le moyen tiré de ce que l'arrêt attaqué aurait été rendu au terme d'une procédure irrégulière doit être écarté ;<br/>
<br/>
              Sur le bien-fondé de l'arrêt :<br/>
<br/>
              4. Considérant, en premier lieu, d'une part, qu'en vertu du III de l'article 60 de la loi du 23 février 1963 de finances pour 1963, la responsabilité pécuniaire des comptables publics s'étend aux opérations des régisseurs chargés, pour leur compte, d'opérations d'encaissement et de paiement, conformément aux dispositions de l'article 18 du décret du 29 décembre 1962 portant règlement général sur la comptabilité publique alors applicables, dont les dispositions ont été reprises à l'article 22 du décret du 7 novembre 2012 relatif à la gestion budgétaire et comptable publique ; qu'aux termes du X du même article : " Les régisseurs, chargés pour le compte des comptables publics d'opérations d'encaissement et de paiement, sont soumis aux règles, obligations et responsabilité des comptables publics dans les conditions et limites fixées par l'un des décrets prévus au paragraphe XII ci-après. (...) " ; qu'aux termes du XI du même article : " Toute personne qui, sans avoir la qualité de comptable public ou sans agir sous contrôle et pour le compte d'un comptable public, s'ingère dans le recouvrement de recettes affectées ou destinées à un organisme public doté d'un poste comptable ou dépendant d'un tel poste doit, nonobstant les poursuites qui pourraient être engagées devant les juridictions répressives, rendre compte au juge financier de l'emploi des fonds ou valeurs qu'elle a irrégulièrement détenus ou maniés. / (...) Les gestions de fait sont soumises aux mêmes juridictions et entraînent les mêmes obligations et responsabilités que les gestions régulières. (...) " ;<br/>
<br/>
              5. Considérant, d'autre part, qu'aux termes de l'article 3 du décret du 20 juillet 1992 relatif aux régies de recettes et aux régies d'avances des organismes publics : " Sauf disposition contraire, prise en accord avec le ministre du budget, le régisseur est nommé par arrêté ou décision de l'ordonnateur de l'organisme public auprès duquel la régie est instituée. (...) Selon les règles propres à chaque catégorie d'organisme public, la nomination du régisseur est soumise à l'agrément du comptable assignataire " ; qu'aux termes de l'article 48 du décret du 30 août 1985 relatif aux établissements publics locaux d'enseignement, alors applicable, dont les dispositions ont été reprises à l'article R. 421-70 du code de l'éducation : " Les régisseurs de recettes et d'avances sont nommés par le chef d'établissement avec l'agrément de l'agent comptable " ;<br/>
<br/>
              6. Considérant qu'il résulte des dispositions citées ci-dessus que la nomination d'un régisseur dans un établissement public local d'enseignement relève de la compétence du chef d'établissement, avec l'agrément de l'agent comptable ; que la nomination d'un régisseur affecte la détermination des personnes susceptibles d'être déclarées personnellement et pécuniairement responsables d'opérations relevant de la comptabilité publique ; que, dès lors, cette nomination ne saurait résulter d'une simple décision implicite du chef de l'établissement concerné, mais doit nécessairement être formalisée par une décision explicite, soumise à l'agrément de l'agent comptable ; que, par suite, c'est sans erreur de droit ni dénaturation des pièces du dossier que la Cour des comptes a jugé que, faute d'une décision explicite du chef d'établissement agréée par le comptable, M. A...n'avait pas été nommé régisseur de recettes et d'avances du collège William-Henri Classen ;<br/>
<br/>
              7. Considérant, en deuxième lieu, d'une part, que la procédure de gestion de fait permet de saisir en leur chef toutes les personnes ayant contribué à la mise en place de la gestion de fait, même si elles n'ont pas manipulé de deniers publics ; qu'elles peuvent être déclarées comptables de fait si elles ont participé, fût-ce indirectement, aux irrégularités financières, ou si elles les ont facilitées, par leur inaction, ou même tolérées ; <br/>
<br/>
              8. Considérant, d'autre part, qu'aux termes de l'article 15 du décret du 20 juillet 1992 relatif aux régies de recettes et aux régies d'avances des organismes publics : " Les régisseurs de recettes et d'avances sont soumis aux contrôles du comptable assignataire et de l'ordonnateur auprès duquel ils sont placés. (...) " ; que l'instruction codificatrice n° 05-042-M9-R du 30 septembre 2005 prévoit, notamment, que : " L'ordonnateur ouvre un dossier pour chaque régie dans lequel il conserve un exemplaire de l'acte constitutif de la régie, de l'acte de nomination du régisseur, (...) des rapports des vérifications effectuées par ses soins, ainsi que de toutes correspondances afférentes au fonctionnement de la régie et à la gestion du régisseur " ;<br/>
<br/>
              9. Considérant que, par l'arrêt attaqué, la Cour des comptes a relevé que Mme C... n'avait pas procédé, en sa qualité de chef d'établissement, à la nomination de M. A... en qualité de régisseur, ni diligenté aucun contrôle de la régie sur la période considérée, contrairement aux exigences du décret du 20 juillet 1992 et de l'instruction codificatrice n° 05-042-M9-R du 30 septembre 2005 rappelées au point précédent ; qu'elle en a déduit que Mme C...avait ainsi, par sa négligence, été à l'origine d'un maniement irrégulier de fonds publics ; qu'en déduisant de ces constatations souveraines, qui sont exemptes de dénaturation, que l'intéressée devait être déclarée solidairement comptable de fait, la Cour n'a commis ni erreur de droit ni erreur de qualification juridique des faits ;<br/>
<br/>
              10. Considérant, en troisième et dernier lieu, qu'il incombait à MmeC..., en sa qualité de chef d'établissement, de procéder dès sa nomination en 2004 aux contrôles de la régie prescrits par les dispositions, rappelées au point 8, du décret du 20 juillet 1992 et de l'instruction codificatrice n° 05-042-M9-R du 30 septembre 2005 ; que, dès lors, elle ne saurait utilement se prévaloir de ce qu'elle n'aurait eu connaissance de l'absence de nomination expresse de M. A...en qualité de régisseur qu'en 2010 pour soutenir qu'elle ne pouvait être regardée comme gestionnaire qu'à compter de cette date ; que, par suite, le moyen tiré de ce que la Cour des comptes aurait entaché son arrêt d'erreur de qualification juridique des faits en la considérant comme gestionnaire de fait pendant toute la durée de l'exercice de ses fonctions de chef d'établissement doit être écarté ;<br/>
<br/>
              11. Considérant qu'il résulte de tout ce qui précède que le pourvoi de Mme C... doit être rejeté, y compris ses conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
      Article 1er : Le pourvoi de Mme C...est rejeté.<br/>
<br/>
Article 2 : La présente décision sera notifiée à Mme F...C...et au parquet général près la Cour des comptes.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">18-01 COMPTABILITÉ PUBLIQUE ET BUDGET. RÉGIME JURIDIQUE DES ORDONNATEURS ET DES COMPTABLES. - NOMINATION D'UN RÉGISSEUR DE RECETTES ET D'AVANCES DANS UN EPLE - CONDITIONS DE VALIDITÉ - DÉCISION EXPLICITE DU CHEF D'ÉTABLISSEMENT ET AGRÉMENT DE L'AGENT COMPTABLE.
</SCT>
<ANA ID="9A"> 18-01 La nomination d'un régisseur dans un établissement public local d'enseignement (EPLE) relève de la compétence du chef d'établissement, avec l'agrément de l'agent comptable. La nomination d'un régisseur affectant la détermination des personnes susceptibles d'être déclarées personnellement et pécuniairement responsables d'opérations relevant de la comptabilité publique, cette nomination ne saurait résulter d'une simple décision implicite du chef de l'établissement concerné, mais doit nécessairement être formalisée par une décision explicite, soumise à l'agrément de l'agent comptable.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
