<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027288063</ID>
<ANCIEN_ID>JG_L_2013_04_000000363738</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/28/80/CETATEXT000027288063.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème / 3ème SSR, 08/04/2013, 363738, Publié au recueil Lebon</TITRE>
<DATE_DEC>2013-04-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>363738</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème / 3ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PEIGNOT, GARREAU, BAUER-VIOLAS</AVOCATS>
<RAPPORTEUR>Mme Esther de Moustier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2013:363738.20130408</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 6 et 21 novembre 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour l'association ATLALR, dont le siège est Avenue des amandiers, ancien Intersport, à Villeneuve-les-Béziers (34420) ; elle demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'ordonnance n° 1204081 du 22 octobre 2012 par laquelle le juge des référés du tribunal administratif de Montpellier, statuant sur le fondement de l'article L. 521-3 du code de justice administrative et sur la demande du préfet de l'Hérault, lui a enjoint ainsi qu'à tous occupants de son chef de libérer les biens immeubles situés sur les parcelles cadastrées section B n° 1081, 1480, 1072 et 1069 qu'ils occupent à Villeneuve-les-Béziers dans un délai d'un mois suivant la notification de cette ordonnance et autorisé le préfet, à défaut d'exécution de cette injonction, à recourir à la force publique pour libérer les lieux ; <br/>
<br/>
              2°) réglant l'affaire au titre de la procédure de référé, de rejeter la demande du préfet de l'Hérault ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général de la propriété des personnes publiques ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Esther de Moustier, Auditeur,  <br/>
<br/>
              - les observations de la SCP Peignot, Garreau, Bauer-Violas, avocat de l'association ATLALR,<br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Peignot, Garreau, Bauer-Violas, avocat de l'association ATLALR ;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'avant l'entrée en vigueur, le 1er juillet 2006, du code général de la propriété des personnes publiques, l'appartenance d'un bien au domaine public était, sauf si ce bien était directement affecté à l'usage du public, subordonnée à la double condition que le bien ait été affecté au service public et spécialement aménagé en vue du service public auquel il était destiné ; que le fait de prévoir de façon certaine un tel aménagement du bien concerné impliquait que celui-ci était soumis, dès ce moment, aux principes de la domanialité publique ; qu'en l'absence de toute disposition en ce sens, l'entrée en vigueur de ce code n'a pu, par elle-même, avoir pour effet d'entraîner le déclassement de dépendances qui, n'ayant encore fait l'objet d'aucun aménagement, appartenaient antérieurement au domaine public en application de la règle énoncée ci-dessus, alors même qu'en l'absence de réalisation de l'aménagement prévu, elles ne rempliraient pas l'une des conditions fixées depuis le 1er juillet 2006 par l'article L. 2111-1 du code général de la propriété des personnes publiques qui exige, pour qu'un bien affecté au service public constitue une dépendance du domaine public, que ce bien fasse déjà l'objet d'un aménagement indispensable à l'exécution des missions de ce service public ; <br/>
<br/>
              2. Considérant que l'association soutient, à l'appui de son pourvoi, que le juge administratif des référés n'était manifestement pas compétent pour statuer sur la demande d'expulsion dont il était saisi, dès lors que celle-ci portait sur des parcelles qui, en l'absence d'aménagement spécial, n'auraient jamais fait partie du domaine public ; que, toutefois, ainsi qu'il ressort des écritures des parties, avant la date à laquelle l'association a été autorisée par l'Etat, par la convention du 15 juin 2005, à occuper les parcelles cadastrées section B n° 1081, 1480, 1072 et 1069 situées sur le territoire de la commune de Villeneuve-les-Béziers, ces parcelles avaient été acquises par l'Etat en vue de la réalisation des travaux, déclarés d'utilité publique par décret du 30 mars 2000, de raccordement de l'autoroute A75 à l'autoroute A9 aux abords de l'échangeur de Béziers Est ; qu'ainsi, la personne publique avait prévu de manière certaine de réaliser les aménagements nécessaires ; que, par suite, ces parcelles étaient soumises aux principes de la domanialité publique ; que la circonstance qu'elles n'aient finalement pas été utilisées pour la réalisation des infrastructures de transport ainsi envisagées, ainsi qu'il résulte d'une déclaration d'utilité publique modificative du 16 novembre 2007, est sans incidence, en l'absence de décision de déclassement, sur leur appartenance au domaine public ; que, par suite, les emplacements occupés par l'association, alors même qu'ils n'ont fait l'objet ni des aménagements projetés en 2000 ni d'autres travaux d'aménagement ferroviaire, ne sont pas manifestement insusceptibles d'être qualifiés de dépendance du domaine public dont le contentieux relève de la juridiction administrative ; que le moyen tiré de ce que le juge des référés n'aurait manifestement pas été compétent pour statuer sur la demande du préfet de l'Hérault doit donc être écarté ;<br/>
<br/>
              3. Considérant, en second lieu, qu'aux termes du premier alinéa de l'article R. 522-4 du code de justice administrative : " Notification de la requête est faite aux défendeurs " ; qu'aux termes de l'article R. 522-7 du même code : " L'affaire est réputée en état d'être jugée dès lors qu'a été accomplie la formalité prévue au premier alinéa de l'article R. 522-4 et que les parties ont été régulièrement convoquées à une audience publique pour y présenter leurs observations " ;<br/>
<br/>
              4. Considérant qu'il ressort des pièces du dossier soumis au juge des référés qu'une copie de la demande du préfet de l'Hérault, accompagnée de l'avis précisant que l'affaire serait appelée à l'audience publique du 18 octobre 2012 à 14h30, a été adressée à M. Soulié, président de l'association ATLALR, par lettre recommandée du 27 septembre 2012, dont M. Soulié a accusé réception le 28 septembre 2012 ; qu'il suit de là que l'association ATLALR n'est pas fondée à soutenir que l'ordonnance attaquée est intervenue au terme d'une procédure irrégulière faute pour elle d'avoir reçu notification de la demande du préfet et de l'avis d'audience ;<br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède, et sans qu'il soit besoin de statuer sur la fin de non recevoir opposée par la ministre de l'écologie, du développement durable et de l'énergie, que l'association ATLALR n'est pas fondée à demander l'annulation de l'ordonnance attaquée ; que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de l'association ATLALR est rejeté.<br/>
Article 2 : La présente décision sera notifiée à l'association ATLALR et à la ministre de l'écologie, du développement durable et de l'énergie.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">24-01-01-01-01-01 DOMAINE. DOMAINE PUBLIC. CONSISTANCE ET DÉLIMITATION. DOMAINE PUBLIC ARTIFICIEL. BIENS FAISANT PARTIE DU DOMAINE PUBLIC ARTIFICIEL. AMÉNAGEMENT SPÉCIAL ET AFFECTATION AU SERVICE PUBLIC OU À L'USAGE DU PUBLIC. - ARTICLE L. 2111-1 DU CG3P - CONDITION D'AMÉNAGEMENT INDISPENSABLE - 1) PORTÉE - BIEN FAISANT DÉJÀ L'OBJET D'UN TEL AMÉNAGEMENT [RJ1] - 2) ENTRÉE EN VIGUEUR AU 1ER JUILLET 2006 - CONSÉQUENCE - DÉPENDANCES QUI, N'AYANT ENCORE FAIT L'OBJET D'AUCUN AMÉNAGEMENT SPÉCIAL, APPARTENAIENT ANTÉRIEUREMENT AU DOMAINE PUBLIC EN APPLICATION DE LA RÈGLE SUIVANT LAQUELLE LE FAIT DE PRÉVOIR DE FAÇON CERTAINE UN TEL AMÉNAGEMENT DU BIEN CONCERNÉ IMPLIQUAIT QUE CELUI-CI ÉTAIT SOUMIS, DÈS CE MOMENT, AUX PRINCIPES DE LA DOMANIALITÉ PUBLIQUE - DÉCLASSEMENT AUTOMATIQUE - ABSENCE [RJ2].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">24-01-02-025 DOMAINE. DOMAINE PUBLIC. RÉGIME. DÉCLASSEMENT. - ENTRÉE EN VIGUEUR DU CG3P - DÉPENDANCES QUI, N'AYANT ENCORE FAIT L'OBJET D'AUCUN AMÉNAGEMENT SPÉCIAL, APPARTENAIENT ANTÉRIEUREMENT AU DOMAINE PUBLIC EN APPLICATION DE LA RÈGLE SUIVANT LAQUELLE LE FAIT DE PRÉVOIR DE FAÇON CERTAINE UN TEL AMÉNAGEMENT DU BIEN CONCERNÉ, AFFECTÉ AU SERVICE PUBLIC, IMPLIQUAIT QUE CELUI-CI ÉTAIT SOUMIS, DÈS CE MOMENT, AUX PRINCIPES DE LA DOMANIALITÉ PUBLIQUE [RJ1] - DÉCLASSEMENT AUTOMATIQUE - ABSENCE [RJ2].
</SCT>
<ANA ID="9A"> 24-01-01-01-01-01 1) L'article L. 2111-1 du code général de la propriété des personnes publiques (CG3P) exige, pour qu'un bien affecté au service public constitue une dépendance du domaine public, que ce bien fasse déjà l'objet d'un aménagement indispensable à l'exécution des missions de ce service public.... ,,2) Avant l'entrée en vigueur, le 1er juillet 2006, de ce code, l'appartenance d'un bien au domaine public était, sauf si ce bien était directement affecté à l'usage du public, subordonnée à la double condition que le bien ait été affecté au service public et spécialement aménagé en vue du service public auquel il était destiné. Le fait de prévoir de façon certaine un tel aménagement du bien concerné impliquait que celui-ci était soumis, dès ce moment, aux principes de la domanialité publique. En l'absence de toute disposition en ce sens, l'entrée en vigueur de ce code n'a pu, par elle-même, avoir pour effet d'entraîner le déclassement de dépendances qui, n'ayant encore fait l'objet d'aucun aménagement, appartenaient antérieurement au domaine public en application de la règle énoncée ci-dessus, alors même qu'en l'absence de réalisation de l'aménagement prévu, elles ne rempliraient pas la condition d'aménagement indispensable fixée depuis le 1er juillet 2006 par l'article L. 2111-1 du CG3P.</ANA>
<ANA ID="9B"> 24-01-02-025 Avant l'entrée en vigueur, le 1er juillet 2006, du code général de la propriété des personnes publiques (CG3P), l'appartenance d'un bien au domaine public était, sauf si ce bien était directement affecté à l'usage du public, subordonnée à la double condition que le bien ait été affecté au service public et spécialement aménagé en vue du service public auquel il était destiné. Le fait de prévoir de façon certaine un tel aménagement du bien concerné impliquait que celui-ci était soumis, dès ce moment, aux principes de la domanialité publique. En l'absence de toute disposition en ce sens, l'entrée en vigueur de ce code n'a pu, par elle-même, avoir pour effet d'entraîner le déclassement de dépendances qui, n'ayant encore fait l'objet d'aucun aménagement, appartenaient antérieurement au domaine public en application de la règle énoncée ci-dessus, alors même qu'en l'absence de réalisation de l'aménagement prévu, elles ne rempliraient pas l'une des conditions fixées depuis le 1er juillet 2006 par l'article L. 2111-1 du CG3P, lequel exige, pour qu'un bien affecté au service public constitue une dépendance du domaine public, que ce bien fasse déjà l'objet d'un aménagement indispensable à l'exécution des missions de ce service public.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Comp., pour la jurisprudence relative aux règles antérieures à l'entrée en vigueur du code général de la propriété des personnes publiques, en ce qui concerne la soumission au régime de la domanialité publique d'un bien dont l'aménagement spécial est projeté de manière certaine, mais non encore réalisé, CE, 6 mai 1985, Association Eurolat Crédit Foncier de France, n° 41589, p. 141 ; CE, 1er février 1995, Préfet de la Meuse, n° 127969, T. p. 782.,,[RJ2] Cf. CE, 3 octobre 2012, Commune de Port-Vendres, n° 353915, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
