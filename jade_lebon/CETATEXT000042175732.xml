<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042175732</ID>
<ANCIEN_ID>JG_L_2020_07_000000435998</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/17/57/CETATEXT000042175732.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 29/07/2020, 435998</TITRE>
<DATE_DEC>2020-07-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>435998</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Philippe Ranquet</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Gabrielle Merloz</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:435998.20200729</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. A... B... a demandé au tribunal administratif de Bordeaux d'annuler la décision du 26 septembre 2018 par laquelle l'Agence de services et de paiement (ASP) a rejeté sa demande de chèque énergie. Par une ordonnance n° 1805204 du 28 novembre 2018, le président du tribunal administratif de Bordeaux a, sur le fondement du premier alinéa de l'article R. 351-3 du code de justice administrative, transmis la demande au tribunal administratif de Caen.<br/>
<br/>
              Par une ordonnance n° 1802975 du 14 novembre 2019, enregistrée le même jour au secrétariat du contentieux du Conseil d'Etat, le président du tribunal administratif de Caen a, sur le fondement du deuxième alinéa de l'article R. 351-6 du code de justice administrative, transmis le dossier au président de la section du contentieux du Conseil d'Etat. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'énergie ;<br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Philippe Ranquet, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Marie-Gabrielle Merloz, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Aux termes du premier alinéa de l'article R. 351-3 du code de justice administrative : " Lorsqu'une cour administrative d'appel ou un tribunal administratif est saisi de conclusions qu'il estime relever de la compétence d'une juridiction administrative autre que le Conseil d'Etat, son président, ou le magistrat qu'il délègue, transmet sans délai le dossier à la juridiction qu'il estime compétente ". Aux termes du deuxième alinéa de l'article R. 351-6 du même code : " Lorsque le président de la cour administrative d'appel ou du tribunal administratif, auquel un dossier a été transmis en application du premier alinéa ou de la seconde phrase du second alinéa de l'article R. 351-3, estime que cette juridiction n'est pas compétente, il transmet le dossier, dans le délai de trois mois suivant la réception de celui-ci, au président de la section du contentieux du Conseil d'Etat, qui règle la question de compétence et attribue le jugement de tout ou partie de l'affaire à la juridiction qu'il déclare compétente ". Enfin, son article R. 351-9 dispose : " Lorsqu'une juridiction à laquelle une affaire a été transmise en application du premier alinéa de l'article R. 351-3 n'a pas eu recours aux dispositions du deuxième alinéa de l'article R. 351-6 ou lorsqu'elle a été déclarée compétente par le président de la section du contentieux du Conseil d'Etat, sa compétence ne peut plus être remise en cause ni par elle-même, ni par les parties, ni d'office par le juge d'appel ou de cassation, sauf à soulever l'incompétence de la juridiction administrative ".<br/>
<br/>
              2. Il résulte de ces dispositions que le président de la juridiction à laquelle une affaire a été transmise par une ordonnance prise sur le fondement du premier alinéa de l'article R. 351-3 du code de justice administrative ne peut exercer la faculté prévue à l'article R. 351-6 du même code, s'il estime que cette juridiction n'est pas compétente, de les transmettre au président de la section du contentieux du Conseil d'Etat que dans le délai de trois mois à compter de l'enregistrement de l'ordonnance. Une fois ce délai expiré, le jugement de cette affaire ne peut en principe être attribué à une autre juridiction.<br/>
<br/>
              3. Le jugement d'une demande dirigée contre une décision de l'Agence de services et de paiement (ASP) refusant le bénéfice du chèque énergie relève, en vertu de l'article R. 312-7 du code de justice administrative, de la compétence du tribunal dans le ressort duquel se trouve le logement pour lequel l'aide est demandée. En l'espèce, la demande de M. A... B... dirigée contre la décision de l'Agence de services et de paiement (ASP) lui refusant le bénéfice du chèque énergie pour un logement situé dans le ressort du tribunal administratif de Bordeaux a été transmise, par une ordonnance du président de ce tribunal, au tribunal administratif de Caen qui l'a enregistrée le 18 décembre 2018. Par une ordonnance du 14 novembre 2019 prise sur le fondement du deuxième alinéa de l'article R. 351-6 du code de justice administrative, le président de ce tribunal a transmis le dossier au président de la section du contentieux du Conseil d'Etat, au motif que le jugement de l'affaire relevait de la compétence du tribunal administratif de Bordeaux. Cette ordonnance ayant été prise plus de trois mois après l'enregistrement de celle lui transmettant l'affaire, le tribunal administratif de Caen ne pouvait cependant plus recourir aux dispositions du deuxième alinéa de l'article R. 351-6.<br/>
<br/>
              4. Toutefois, l'affaire ayant été tout de même transmise au président de la section du contentieux du Conseil d'Etat, au demeurant peu de temps après l'intervention de la décision n° 427175 du 30 septembre 2019 par laquelle le Conseil d'Etat, statuant au contentieux, a précisé que les litiges relatifs au bénéfice du chèque énergie relèvent de la compétence du tribunal dans le ressort duquel se trouve le logement pour lequel l'aide est demandée, il y a lieu, dans les circonstances particulières de l'espèce et pour une bonne administration de la justice, de faire application des dispositions de l'article R. 351-8 du code de justice administrative et d'attribuer le jugement de l'affaire au tribunal administratif de Bordeaux, dans le ressort duquel se trouve le logement pour lequel l'aide en litige a été demandée.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le jugement de la demande de M. B... est attribué au tribunal administratif de Bordeaux.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. A... B..., au président du tribunal administratif Bordeaux et au président du tribunal administratif de Caen.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">17-05 COMPÉTENCE. COMPÉTENCE À L'INTÉRIEUR DE LA JURIDICTION ADMINISTRATIVE. - TRANSMISSION DE L'AFFAIRE AU PRÉSIDENT DE LA SECTION DU CONTENTIEUX LORSQUE LA SECONDE JURIDICTION S'ESTIME INCOMPÉTENTE (ART. R. 351-6 DU CJA) - CONDITION - TRANSMISSION DANS UN DÉLAI DE TROIS MOIS.
</SCT>
<ANA ID="9A"> 17-05 Il résulte des articles R. 351-3, R. 351-6 et R. 351-9 du code de justice administrative (CJA) que le président de la juridiction à laquelle une affaire a été transmise par une ordonnance prise sur le fondement du premier alinéa de l'article R. 351-3 du CJA ne peut exercer la faculté prévue à l'article R. 351-6 du même code, s'il estime que cette juridiction n'est pas compétente, de les transmettre au président de la section du contentieux du Conseil d'Etat que dans le délai de trois mois à compter de l'enregistrement de l'ordonnance. Une fois ce délai expiré, le jugement de cette affaire ne peut en principe être attribué à une autre juridiction.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
