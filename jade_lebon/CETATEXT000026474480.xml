<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026474480</ID>
<ANCIEN_ID>JG_L_2012_10_000000344742</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/47/44/CETATEXT000026474480.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème et 8ème sous-sections réunies, 08/10/2012, 344742</TITRE>
<DATE_DEC>2012-10-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>344742</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème et 8ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GATINEAU, FATTACCINI ; SCP MASSE-DESSEN, THOUVENIN</AVOCATS>
<RAPPORTEUR>M. Christophe Pourreau</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:344742.20121008</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 6 décembre 2010 et 7 mars 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour la communauté d'agglomération d'Annecy, dont le siège est 46, avenue des Iles - BP 90270 à Annecy cedex (74007), représentée par son président ; la communauté d'agglomération d'Annecy demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 08LY02705 du 5 octobre 2010 par lequel la cour administrative d'appel de Lyon a rejeté sa requête tendant, d'une part, à l'annulation du jugement n° 0605947 du 8 octobre 2008 par lequel le tribunal administratif de Grenoble a annulé les décisions des 31 juillet et 23 octobre 2006 par lesquelles le président de la communauté d'agglomération a rejeté la demande de la commune d'Annecy-le-Vieux tendant à ce que la communauté d'agglomération exerce la compétence relative à la gestion des abribus installés sur son territoire et, d'autre part, au rejet de la demande de la commune d'Annecy-le-Vieux ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de la commune d'Annecy-le-Vieux la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des collectivités territoriales ;<br/>
<br/>
              Vu le code des transports ;<br/>
<br/>
              Vu la loi n° 82-1153 du 30 décembre 1982 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Christophe Pourreau, Maître des Requêtes,  <br/>
<br/>
              -	 les observations de la SCP Gatineau, Fattaccini avocat de la communauté d'agglomération d'Annecy et de la SCP Masse-Dessen, Thouvenin avocat de la commune d'Annecy-le-Vieux,<br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Gatineau, Fattaccini avocat de la Communauté d'agglomération d'Annecy et à la SCP Masse-Dessen, Thouvenin avocat de la commune d'Annecy-le-Vieux ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, le 23 octobre 2006, le président de la communauté d'agglomération d'Annecy a rejeté le recours gracieux formé par la commune d'Annecy-le-Vieux contre la décision du 31 juillet 2006 par laquelle il avait rejeté la demande de cette dernière tendant à ce que la communauté d'agglomération prenne en charge l'installation et l'entretien des abribus au titre de sa compétence en matière d'organisation des transports urbains ; que, le 20 décembre 2006, la commune d'Annecy-le-Vieux a saisi le tribunal administratif de Grenoble d'une demande d'annulation des décisions des 31 juillet et 23 octobre 2006 ; que la communauté d'agglomération d'Annecy se pourvoit en cassation contre l'arrêt du 5 octobre 2010 par lequel la cour administrative d'appel de Lyon a rejeté sa requête tendant à l'annulation du jugement du 8 octobre 2008 par lequel le tribunal administratif de Grenoble a fait droit à la demande de la commune d'Annecy-le-Vieux ;<br/>
<br/>
              2. Considérant qu'aux termes du I de l'article L. 5216-5 du code général des collectivités territoriales : " I. - La communauté d'agglomération exerce de plein droit au lieu et place des communes membres les compétences suivantes : / [...] 2° En matière d'aménagement de l'espace communautaire : [...] organisation des transports urbains au sens du chapitre II du titre II de la loi n° 82-1153 du 30 décembre 1982 d'orientation des transports intérieurs, sous réserve des dispositions de l'article 46 de cette loi ; [...] " ; qu'en vertu des articles 27 et suivants de la loi du 30 décembre 1982 d'orientation des transports intérieurs, codifiés aux articles L. 1214-1 et suivants et L. 1231-1 et suivants du code des transports, l'autorité organisatrice de transports urbains est chargée d'élaborer un plan de déplacements urbains qui détermine les principes régissant l'organisation du transport de personnes et de marchandises, la circulation et le stationnement dans le périmètre de transports urbains ; que relèvent à ce titre notamment de la compétence de l'autorité organisatrice de transports urbains la définition des services de transports collectifs de personnes dans le périmètre de transports urbains, la réalisation des investissements correspondants, la gestion de ces services ainsi que la définition de la politique tarifaire ;<br/>
<br/>
              3. Considérant que, s'il résulte de ces dispositions que la localisation des points d'arrêt des véhicules de transport public de personnes et l'information des usagers sur ces points d'arrêt ainsi que sur les horaires de circulation des véhicules relèvent de la compétence obligatoire et de plein droit de la communauté d'agglomération au titre de sa compétence d'organisation des transports urbains, une telle compétence ne s'étend pas à la réalisation et à l'entretien des éléments de mobilier urbain que constituent les abribus, lesquels ne sont pas des équipements indispensables à l'exécution du service public de transport public ; que les abribus installés sur le territoire d'une commune à la date de création d'une communauté d'agglomération dont le périmètre inclut cette commune ne sont pas davantage mis à disposition de plein droit de la communauté d'agglomération ; qu'il est en revanche loisible à l'autorité compétente de prévoir, dans les statuts d'une communauté d'agglomération, que celle-ci prendra en charge l'installation et l'entretien des abribus sur le territoire des communes membres ; <br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède que la cour administrative d'appel de Lyon a commis une erreur de droit en jugeant, après avoir relevé que ses statuts ne le prévoyaient pas, que la gestion et l'entretien des abribus sur le territoire de la commune d'Annecy-le-Vieux relevaient, obligatoirement et de plein droit, des attributions de la communauté d'agglomération d'Annecy ; que le moyen soulevé par celle-ci sur ce point, qui n'est pas nouveau en cassation, doit être accueilli ; que, par suite, sans qu'il soit besoin d'examiner les autres moyens, l'arrêt attaqué doit être annulé ;<br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code justice administrative ;<br/>
<br/>
              6. Considérant qu'ainsi qu'il a été dit au point 3, l'installation et la gestion des abribus ne relevaient pas de la compétence obligatoire exercée de plein droit par la communauté d'agglomération d'Annecy au titre de l'organisation des transports urbains ; que, par suite, c'est à tort que le tribunal administratif de Grenoble s'est fondé sur ce motif pour annuler les décisions des 31 juillet et 23 octobre 2006 par lesquelles le président de la communauté d'agglomération d'Annecy a rejeté la demande de la commune d'Annecy-le-Vieux tendant à ce que la communauté d'agglomération prenne en charge la gestion des abribus sur le territoire de la commune ;<br/>
<br/>
              7. Considérant, toutefois, qu'il appartient au Conseil d'Etat, saisi de l'ensemble du litige par l'effet dévolutif de l'appel, d'examiner les autres moyens soulevés par la commune d'Annecy-le-Vieux devant le tribunal administratif de Grenoble ;<br/>
<br/>
              8. Considérant, en premier lieu, qu'aux termes du III de l'article L. 5211-5 du code général des collectivités territoriales : " III. Le transfert des compétences entraîne de plein droit l'application à l'ensemble des biens, équipements et services publics nécessaires à leur exercice, ainsi qu'à l'ensemble des droits et obligations qui leur sont attachés à la date du transfert, des dispositions des trois premiers alinéas de l'article L. 1321-1 [...]. " ; qu'aux termes de l'article L. 1321-1 du même code : " Le transfert d'une compétence entraîne de plein droit la mise à la disposition de la collectivité bénéficiaire des biens meubles et immeubles utilisés, à la date de ce transfert, pour l'exercice de cette compétence. [...] " ; que, pour les motifs exposés au point 3, la création de la communauté d'agglomération d'Annecy n'a entraîné, compte tenu du silence de ses statuts sur ce point, aucune mise à sa disposition de plein droit des abribus installés sur le territoire de la commune d'Annecy-le-Vieux au profit de la communauté d'agglomération ; que, dès lors, le moyen tiré de ce que l'installation et la gestion des abribus relèveraient de la compétence de la communauté d'agglomération en application des dispositions de l'article L. 5211-5 du code général des collectivités territoriales ne peut qu'être écarté ;<br/>
<br/>
              9 Considérant, en second lieu, que, à supposer même que la protection des usagers des transports urbains contre les intempéries relève du service public des transports, le moyen tiré de ce que, pour ce motif, l'installation et la gestion des abribus seraient de la compétence de la communauté d'agglomération d'Annecy au titre de sa compétence  d'organisation des transports publics telle que définie par les dispositions précitées de l'article L. 5216-5 du code général des collectivités territoriales, ne peut qu'être écarté ;<br/>
<br/>
              10. Considérant qu'il résulte de ce qui précède que la communauté d'agglomération d'Annecy est fondée à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Grenoble a annulé les décisions du président de la communauté d'agglomération des 31 juillet et 23 octobre 2006 ; <br/>
<br/>
              11. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune d'Annecy-le-Vieux la somme de 3 000 euros, à verser à la communauté d'agglomération d'Annecy, en application des dispositions de l'article L. 761-1 du code de justice administrative, au titre des frais exposés par elle devant les premiers juges et devant le Conseil d'Etat ; qu'en revanche, ces dispositions font obstacle à ce qu'une somme soit mise à la charge de la communauté d'agglomération d'Annecy, qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Lyon du 5 octobre 2010 est annulé.<br/>
Article 2 : Le jugement du tribunal administratif de Grenoble du 8 octobre 2008 est annulé.<br/>
Article 3 : La demande de la commune d'Annecy-le-Vieux devant le tribunal administratif de Grenoble est rejetée.<br/>
Article 4 : La commune d'Annecy-le-Vieux versera à la communauté d'agglomération d'Annecy une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 5 : Les conclusions de la commune d'Annecy-le-Vieux présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 6 : La présente décision sera notifiée à la communauté d'agglomération d'Annecy et à la commune d'Annecy-le-Vieux. <br/>
Copie en sera adressée pour information au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">135-05-01-06 COLLECTIVITÉS TERRITORIALES. COOPÉRATION. ÉTABLISSEMENTS PUBLICS DE COOPÉRATION INTERCOMMUNALE - QUESTIONS GÉNÉRALES. COMMUNAUTÉS D'AGGLOMÉRATION. - COMPÉTENCE EN MATIÈRE D'ORGANISATION DES TRANSPORTS URBAINS - 1) PORTÉE - RÉALISATION ET ENTRETIEN DES ABRIBUS - EXCLUSION - 2) ELÉMENTS MIS À DISPOSITION DE PLEIN DROIT PAR LES COMMUNES À LA DATE DE LA CRÉATION DE LA COMMUNAUTÉ - CHAMP - ABRIBUS - EXCLUSION - 3) FACULTÉ DE PRÉVOIR, DANS LES STATUTS DE LA COMMUNAUTÉ, QUE CELLE-CI PRENDRA EN CHARGE L'INSTALLATION ET L'ENTRETIEN DES ABRIBUS - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">65-02-01 TRANSPORTS. TRANSPORTS ROUTIERS. TRANSPORTS EN COMMUN DE VOYAGEURS. - COMPÉTENCE DES COMMUNAUTÉS D'AGGLOMÉRATION EN MATIÈRE D'ORGANISATION DES TRANSPORTS URBAINS - 1) PORTÉE - RÉALISATION ET ENTRETIEN DES ABRIBUS - EXCLUSION - 2) ELÉMENTS MIS À DISPOSITION DE PLEIN DROIT PAR LES COMMUNES À LA DATE DE LA CRÉATION DE LA COMMUNAUTÉ - CHAMP - ABRIBUS - EXCLUSION - 3) FACULTÉ DE PRÉVOIR, DANS LES STATUTS DE LA COMMUNAUTÉ, QUE CELLE-CI PRENDRA EN CHARGE L'INSTALLATION ET L'ENTRETIEN DES ABRIBUS - EXISTENCE.
</SCT>
<ANA ID="9A"> 135-05-01-06 1) S'il résulte des dispositions du I de l'article L. 5216-5 du code général des collectivités territoriales et des articles 27 et suivants de la loi n° 82-1153 du 30 décembre 1982 d'orientation des transports intérieurs, codifiés aux articles L. 1214-1 et suivants et L. 1231-1 et suivants du code des transports, que la localisation des points d'arrêt des véhicules de transport public de personnes et l'information des usagers sur ces points d'arrêt ainsi que sur les horaires de circulation des véhicules relèvent de la compétence obligatoire et de plein droit de la communauté d'agglomération au titre de sa compétence d'organisation des transports urbains, une telle compétence ne s'étend pas à la réalisation et à l'entretien des éléments de mobilier urbain que constituent les abribus, lesquels ne sont pas des équipements indispensables à l'exécution du service public de transport public. 2) Les abribus installés sur le territoire d'une commune à la date de création d'une communauté d'agglomération dont le périmètre inclut cette commune ne sont pas davantage mis à disposition de plein droit de la communauté d'agglomération. 3) Il est en revanche loisible à l'autorité compétente de prévoir, dans les statuts d'une communauté d'agglomération, que celle-ci prendra en charge l'installation et l'entretien des abribus sur le territoire des communes membres.</ANA>
<ANA ID="9B"> 65-02-01 S'il résulte des dispositions du I de l'article L. 5216-5 du code général des collectivités territoriales et des articles 27 et suivants de la loi n° 82-1153 du 30 décembre 1982 d'orientation des transports intérieurs, codifiés aux articles L. 1214-1 et suivants et L. 1231-1 et suivants du code des transports, que la localisation des points d'arrêt des véhicules de transport public de personnes et l'information des usagers sur ces points d'arrêt ainsi que sur les horaires de circulation des véhicules relèvent de la compétence obligatoire et de plein droit de la communauté d'agglomération au titre de sa compétence d'organisation des transports urbains, une telle compétence ne s'étend pas à la réalisation et à l'entretien des éléments de mobilier urbain que constituent les abribus, lesquels ne sont pas des équipements indispensables à l'exécution du service public de transport public. Les abribus installés sur le territoire d'une commune à la date de création d'une communauté d'agglomération dont le périmètre inclut cette commune ne sont pas davantage mis à disposition de plein droit de la communauté d'agglomération. Il est en revanche loisible à l'autorité compétente de prévoir, dans les statuts d'une communauté d'agglomération, que celle-ci prendra en charge l'installation et l'entretien des abribus sur le territoire des communes membres.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
