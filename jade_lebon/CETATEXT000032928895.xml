<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032928895</ID>
<ANCIEN_ID>JG_L_2016_07_000000396597</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/92/88/CETATEXT000032928895.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 22/07/2016, 396597</TITRE>
<DATE_DEC>2016-07-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>396597</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DE NERVO, POUPET ; SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>M. Grégory Rzepski</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier Henrard</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:396597.20160722</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              La société Sogema a demandé au juge des référés du tribunal administratif de la Guyane, statuant sur le fondement de l'article L. 551-1 du code de justice administrative :<br/>
              - d'annuler la procédure de passation des lots n°s 1 et 2 des marchés de collecte des déchets ménagers et assimilés et des recycables secs concernant la commune de Cayenne hors la zone Collery et les communes de Cayenne pour la zone Collery, Matoury Nord, Macouria et Montsinéry-Tonnégrande, ainsi que les décisions par lesquelles la communauté d'agglomération du Centre Littoral a attribué chacun des lots, respectivement, à la société G2C et à la société Guyanet et a rejeté ses offres ;<br/>
              - d'enjoindre à la communauté d'agglomération du Centre Littoral de reprendre l'intégralité des procédures.<br/>
<br/>
              Par une ordonnance n° 1500936 du 14 janvier 2016, le juge des référés du tribunal administratif de la Guyane a annulé la procédure de passation des lots n°s 1 et 2 au stade de l'examen des offres et a rejeté le surplus des conclusions de la société Sogema.<br/>
<br/>
              1° Par une décision n° 396597 du 23 mars 2016, le Conseil d'Etat, statuant au contentieux, a prononcé l'admission des conclusions du pourvoi de la communauté d'agglomération du Centre Littoral dirigées contre l'ordonnance du juge des référés du tribunal administratif de la Guyane du 14 janvier 2016 en tant seulement qu'elle s'est prononcée sur la procédure de passation du lot n° 2.<br/>
<br/>
              Par un mémoire en défense, enregistré le 15 avril 2016, la société Sogema conclut au rejet du pourvoi pour les mêmes motifs que ceux exposés sous le n° 396633 et à ce qu'il soit mis à la charge de la communauté d'agglomération du Centre Littoral la somme de 5 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
              Par un mémoire en réplique, enregistré le 11 mai 2016, la communauté d'agglomération du Centre Littoral conclut aux mêmes fins par les mêmes moyens.<br/>
<br/>
              Le pourvoi a été communiqué à la société Guyanet qui n'a pas produit de mémoire.<br/>
<br/>
              2° Sous le n° 396633, par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 1er février, 15 février et 19 mai 2016 au secrétariat du contentieux du Conseil d'Etat, la société Guyanet et la SAS Guyanet environnement demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance en tant qu'elle s'est prononcée sur la procédure de passation du lot n° 2 ;<br/>
<br/>
              2°) statuant en référé, de rejeter les conclusions de première instance de la société Sogema ;<br/>
<br/>
              3°) de mettre à la charge de la société Sogema la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - le code des marchés publics ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Grégory Rzepski, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Olivier Henrard, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Foussard, Froger, avocat de la communauté d'agglomération du Centre Littoral, à la SCP de Nervo, Poupet, avocat de la société Sogema et à la SCP Rousseau, Tapie, avocat des sociétés Guyanet et Guyanet Environnement ; <br/>
<br/>
              Vu la note en délibéré, enregistrée le 7 juillet 2016, présentée par la société Sogema sous les n°s 396597 et 396633 ;<br/>
<br/>
<br/>
<br/>1. Considérant que les pourvois, d'une part, de la communauté d'agglomération du Centre Littoral et, d'autre part, de la société Guyanet et de la SAS Guyanet environnement sont dirigés contre la même ordonnance ; qu'il y a lieu de les joindre pour qu'ils fassent l'objet d'une seule décision ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 551-1 du code de justice administrative : " Le président du tribunal administratif, ou le magistrat qu'il délègue, peut être saisi en cas de manquement aux obligations de publicité et de mise en concurrence auxquelles est soumise la passation par les pouvoirs adjudicateurs de contrats administratifs ayant pour objet l'exécution de travaux, la livraison de fournitures ou la prestation de services, avec une contrepartie économique constituée par un prix ou un droit d'exploitation, ou la délégation d'un service public (...) " ; qu'aux termes du I de l'article L. 551-2 de ce code : " I. Le juge peut ordonner à l'auteur du manquement de se conformer à ses obligations et suspendre l'exécution de toute décision qui se rapporte à la passation du contrat, sauf s'il estime, en considération de l'ensemble des intérêts susceptibles d'être lésés et notamment de l'intérêt public, que les conséquences négatives de ces mesures pourraient l'emporter sur leurs avantages. / Il peut, en outre, annuler les décisions qui se rapportent à la passation du contrat et supprimer les clauses ou prescriptions destinées à figurer dans le contrat et qui méconnaissent lesdites obligations (...) " ; qu'aux termes de l'article L. 551-10 du même code : " Les personnes habilitées à engager les recours prévus aux articles L. 551-1 et L. 551-5 sont celles qui ont un intérêt à conclure le contrat et qui sont susceptibles d'être lésées par le manquement invoqué (...) " ;<br/>
<br/>
              3. Considérant que, par un avis publié les 28 août et 1er septembre 2015, la communauté d'agglomération du Centre Littoral a lancé une procédure d'appel d'offres pour l'attribution d'un marché de collecte des déchets ménagers et assimilés et des recyclables secs ; que, par une ordonnance en date du 14 janvier 2016, le juge du référé précontractuel du tribunal administratif de la Guyane, saisi par la société Sogema, a annulé la procédure de passation du lot n° 2 à compter de l'examen des offres, ainsi que la décision par laquelle la communauté d'agglomération du Centre Littoral a attribué ce lot à la société Guyanet et à la SAS Guyanet environnement ; que celles-ci, sous le n° 396633, se pourvoient, dans cette mesure, contre cette ordonnance ; que, par une décision n° 396597 du 23 mars 2016, le Conseil d'Etat, statuant au contentieux, a aussi prononcé l'admission des conclusions du pourvoi de la communauté d'agglomération du Centre Littoral dirigées contre cette ordonnance en tant seulement qu'elle s'est prononcée sur la procédure de passation du lot n° 2 ; <br/>
<br/>
              4. Considérant qu'il ressort des pièces du dossier soumis au juge des référés qu'en vertu de l'article 1.1 du cahier des clauses techniques particulières applicable au lot n° 2, qui concerne les communes de Cayenne pour la zone Collery, Matoury Nord, Macouria et Montsinéry-Tonnégrande, le marché portait, d'une part, sur des prestations de collecte régulières, rémunérées par un prix forfaitaire et, d'autre part, sur des opérations de collecte occasionnelles, rémunérées à prix unitaire, dont le coût cumulé ne devait pas excéder 5 % du montant du marché ; que les documents de la consultation prévoyaient que la comparaison des offres, s'agissant du critère du prix, s'effectuerait sur la seule base du prix global et forfaitaire proposé par les candidats ; que, lors de la comparaison des offres des candidats, la communauté d'agglomération du Centre Littoral a décidé de prendre également en compte le prix des prestations occasionnelles ; que toutefois, pour procéder à cette appréciation, elle s'est bornée, pour chaque candidat, à valoriser les prestations occasionnelles à un montant correspondant à 5 % du prix global et forfaitaire proposé pour les prestations de collectes régulières ; qu'un tel choix était ainsi insusceptible, en l'espèce, compte tenu de la méthode de notation retenue, basée sur l'écart entre le montant de l'offre analysée et le montant de l'offre la moins chère, d'aboutir à des notes différentes de celles qui auraient été données aux candidats si seul le prix des prestations de collectes régulières avait été retenu conformément au règlement de la consultation ; qu'il suit de là qu'en jugeant que la société Sogema avait été susceptible d'être lésée par le manquement allégué, tiré de ce que le pouvoir adjudicateur avait modifié le critère du prix après le dépôt des offres, alors que la prise en compte du prix des prestations occasionnelles, non prévue par le règlement de la consultation, n'avait pu avoir d'incidence sur le classement des offres, le juge du référé précontractuel a entaché son ordonnance d'erreur de qualification juridique ; que, dès lors, et sans qu'il soit besoin d'examiner les autres moyens des pourvois, l'article 2 de son ordonnance ainsi que son article 3 en tant qu'il rejette les conclusions présentées par la société Guyanet et la SAS Guyanet environnement tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative doivent être annulés ;<br/>
<br/>
              5. Considérant qu'il y a lieu, en application des dispositions de l'article L. 821-2 du code de justice administrative, de régler, dans cette mesure, l'affaire au titre de la procédure de référé engagée ;<br/>
<br/>
              6. Considérant qu'ainsi qu'il a été dit au point 4, la société Sogema n'a pas été lésée par la modification du critère du prix au cours de la procédure de consultation après le dépôt des offres par les candidats ; <br/>
<br/>
              7. Considérant, il est vrai, que la société Sogema fait valoir que le critère du prix, tel que défini dans le règlement de la consultation, ne permettait pas à la communauté d'agglomération du Centre Littoral de déterminer l'offre économiquement la plus avantageuse, en méconnaissance des dispositions de l'article 53 du code des marchés publics, puisqu'il excluait de son appréciation du prix des offres les opérations de collecte à caractère occasionnel ; que, toutefois, eu égard au recours limité à ces prestations dont le coût cumulé ne pouvait excéder 5 % du montant du marché et à leur caractère hypothétique, le pouvoir adjudicateur n'était pas tenu de prévoir une telle appréciation ; que, par suite, le moyen doit être écarté ;<br/>
<br/>
              8. Considérant, enfin, que lorsque le pouvoir adjudicateur prévoit, pour fixer un critère ou un sous-critère d'attribution du marché, que la valeur des offres sera examinée au regard du respect d'une caractéristique technique déterminée, il lui incombe d'exiger la production de justificatifs lui permettant de vérifier l'exactitude des informations données par les candidats ; que, toutefois, en l'espèce, s'il ressort des documents de la consultation que devaient être examinés, au titre du critère des " effectifs humains et matériels ", le nombre et les caractéristiques sommaires des véhicules utilisés, la communauté d'agglomération du Centre Littoral n'avait pas émis d'exigences particulières à cet égard sanctionnées par le système d'évaluation des offres stipulé par le règlement de la consultation ; que le moyen tiré de ce qu'en s'abstenant d'exiger des candidats qu'ils produisent des justificatifs portant sur le respect de telles exigences, elle aurait manqué à ses obligations de publicité et de mise en concurrence ne peut ainsi, en tout état de cause, qu'être écarté ;<br/>
<br/>
              9. Considérant qu'il résulte de tout ce qui précède que la demande de la société Sogema tendant à l'annulation de la procédure de passation du lot n° 2 doit être rejetée ;<br/>
<br/>
              10. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mis à la charge de la société Guyanet et de la SAS Guyanet environnement, qui ne sont pas les parties perdantes dans le présent litige, le versement des sommes que demande, à ce titre, la société Sogema ; que dans les circonstances de l'espèce, il n'y a pas lieu de mettre à la charge de la communauté d'agglomération du Centre Littoral la somme demandée au même titre par la société Sogema ; qu'en revanche, il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société Sogema le versement, au même titre, d'une somme de 3 000 euros à la communauté d'agglomération du Centre Littoral et d'une somme globale de 4 500 euros pour l'ensemble de la procédure à la société Guyanet et à la SAS Guyanet environnement ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'article 2 de l'ordonnance du juge des référés du tribunal de la Guyane du 14 janvier 2016 et son article 3 en tant qu'il rejette les conclusions présentées par société Guyanet et la SAS Guyanet environnement tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative  sont annulés.<br/>
Article 2 : La demande de la société Sogema tendant à l'annulation de la procédure de passation du lot n° 2 du marché de collecte des déchets ménagers et assimilés et des recyclables secs concernant les communes de Cayenne pour la zone Collery, Matoury Nord, Macouria et Montsinéry-Tonnégrande, ainsi que de la décision par laquelle la communauté d'agglomération du Centre Littoral a attribué ce lot est rejetée.<br/>
Article 3 : La société Sogema versera une somme globale de 4 500 euros à la société Guyanet et à la SAS Guyanet environnement et une somme de 3 000 euros à la communauté d'agglomération du Centre Littoral au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions de la société Sogema présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
              Article 5 : La présente décision sera notifiée à la communauté d'agglomération du Centre Littoral, à la société Guyanet, à la société Guyanet environnement, à la société Sogema et à la société G2C.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-02-005 MARCHÉS ET CONTRATS ADMINISTRATIFS. FORMATION DES CONTRATS ET MARCHÉS. FORMALITÉS DE PUBLICITÉ ET DE MISE EN CONCURRENCE. - MARCHÉS PUBLICS - EXAMEN DES CRITÈRES D'ATTRIBUTION DES OFFRES - OBLIGATION DU POUVOIR ADJUDICATEUR D'EXIGER DES JUSTIFICATIFS - LIMITE - ELÉMENT D'APPRÉCIATION POUR LEQUEL AUCUNE EXIGENCE PARTICULIÈRE N'EST SANCTIONNÉE PAR LE SYSTÈME D'ÉVALUATION DES OFFRES [RJ1].
</SCT>
<ANA ID="9A"> 39-02-005 Lorsque le pouvoir adjudicateur prévoit, pour fixer un critère ou un sous-critère d'attribution du marché, que la valeur des offres sera examinée au regard du respect d'une caractéristique technique déterminée, il lui incombe d'exiger la production de justificatifs lui permettant de vérifier l'exactitude des informations données par les candidats. Toutefois, en l'espèce, s'il ressort des documents de la consultation que devaient être examinés, au titre du critère des effectifs humains et matériels, le nombre et les caractéristiques sommaires des véhicules utilisés, le pouvoir adjudicateur n'avait pas émis d'exigences particulières à cet égard sanctionnées par le système d'évaluation des offres stipulé par le règlement de la consultation et n'a donc pas manqué à ses obligations de publicité et de mise en concurrence en ne prévoyant pas de justificatif particulier.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., en précisant, CE, 11 septembre 2015, Société Autocars de l'île de Beauté, n° 392785, T. p. 746.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
