<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034076437</ID>
<ANCIEN_ID>JG_L_2017_02_000000395184</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/07/64/CETATEXT000034076437.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 22/02/2017, 395184</TITRE>
<DATE_DEC>2017-02-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>395184</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BORE, SALVE DE BRUNETON ; LE PRADO</AVOCATS>
<RAPPORTEUR>M. Arno Klarsfeld</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:395184.20170222</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Rennes l'annulation pour excès de pouvoir de la décision tacite de non-opposition résultant du silence gardé par le maire de la commune de Brec'h (Morbihan) sur la déclaration préalable de Mme D...C...relative à des travaux sur une construction à usage de dépendance. Par un jugement n° 1102794 du 10 janvier 2014, le tribunal administratif de Rennes a annulé cette décision.<br/>
<br/>
              Par un arrêt n° 14NT03360 du 12 octobre 2015, la cour administrative d'appel de Nantes a rejeté l'appel formé par Mme C...tendant à l'annulation de ce jugement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 10 décembre 2015, 3 mars 2016 et 10 janvier 2017 au secrétariat du contentieux du Conseil d'Etat, Mme C...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses conclusions d'appel ; <br/>
<br/>
              3°) de mettre à la charge de M. B...une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
              - le code de l'urbanisme ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Arno Klarsfeld, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Le Prado, avocat de Mme D...C...et à la SCP Boré, Salve de Bruneton, avocat de M. A...B...;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la notification à MmeC..., le 15 janvier 2014, du jugement du 10 janvier 2014 du tribunal administratif de Rennes, comportait l'indication que ce jugement était susceptible d'un recours en cassation devant le Conseil d'Etat dans un délai de deux mois. Or, les jugements rendus par les tribunaux administratifs à compter du 1er janvier 2014 en matière de déclaration préalable de travaux peuvent désormais faire l'objet d'un appel, en application des dispositions de l'article R. 811-1 du code de justice administrative, dans leur rédaction issue du décret n° 2013-730 du 13 août 2013. <br/>
<br/>
              2. MmeC..., qui n'a pas formé de pourvoi en cassation comme l'y invitait l'indication erronée portée sur la notification de ce jugement, a néanmoins formé le 26 décembre 2014 un appel devant la cour administrative d'appel de Nantes.<br/>
<br/>
              3. Par l'arrêt attaqué, la cour administrative d'appel de Nantes a jugé que cet appel était tardif et l'a rejeté comme irrecevable. La cour administrative d'appel a jugé que l'erreur commise dans la notification du jugement quant à l'indication de la nature de la voie de recours susceptible d'être utilement exercée n'était pas de nature à différer le point de départ du délai de recours contentieux dès lors, d'une part, que la notification du jugement attaqué comportait l'indication des délais de recours et, d'autre part, que les dispositions de l'article R. 351-1 du code de justice administrative permettent au Conseil d'Etat, au cas où il est saisi d'un pourvoi dans une matière qui relève de l'appel, d'attribuer le jugement de l'affaire à la cour administrative d'appel compétente.<br/>
<br/>
              4. En jugeant ainsi, alors que l'indication dans la notification du jugement attaqué de la voie de recours particulière que constitue le recours en cassation était susceptible d'exercer une influence sur l'appréciation de la requérante quant à l'opportunité de contester le jugement, la cour administrative d'appel a commis une erreur de droit. <br/>
<br/>
              5. Il résulte de ce qui précède, sans qu'il soit besoin de statuer sur l'autre moyen soulevé, que Mme C...est fondée à demander l'annulation de l'arrêt attaqué. Il y a lieu de mettre à la charge de M. B...la somme de 3 000 euros demandée par Mme C...au titre de l'article L. 761-1 du code de justice administrative. En revanche, les dispositions de cet article font obstacle à ce qu'une somme soit mise à la charge de MmeC..., qui n'est pas la partie perdante dans la présente instance.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'arrêt du 12 octobre 2015 de la cour administrative d'appel de Nantes est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nantes. <br/>
Article 3 : M. B...versera à Mme C...une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions présentées par M. B...au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à Mme D...C..., à M. A... B...et à la commune de Brec'h.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-01-07-02-01 PROCÉDURE. INTRODUCTION DE L'INSTANCE. DÉLAIS. POINT DE DÉPART DES DÉLAIS. NOTIFICATION. - CAS OÙ LA NOTIFICATION DU JUGEMENT INDIQUE À TORT QUE CELUI-CI N'EST SUSCEPTIBLE QUE D'UN RECOURS EN CASSATION DEVANT LE CONSEIL D'ETAT [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-08-01-01-03 PROCÉDURE. VOIES DE RECOURS. APPEL. RECEVABILITÉ. DÉLAI D'APPEL. - POINT DE DÉPART DU DÉLAI D'APPEL - NOTIFICATION - CAS OÙ LA NOTIFICATION DU JUGEMENT INDIQUE À TORT QUE CELUI-CI N'EST SUSCEPTIBLE QUE D'UN RECOURS EN CASSATION DEVANT LE CONSEIL D'ETAT [RJ1].
</SCT>
<ANA ID="9A"> 54-01-07-02-01 L'indication, dans la notification du jugement attaqué, de la voie de recours particulière que constitue le recours en cassation était susceptible d'exercer une influence sur l'appréciation du requérant quant à l'opportunité de contester le jugement. Par suite, cette indication erronée faisait obstacle à ce que le délai de recours contentieux courre à compter de cette notification.</ANA>
<ANA ID="9B"> 54-08-01-01-03 L'indication, dans la notification du jugement attaqué, de la voie de recours particulière que constitue le recours en cassation était susceptible d'exercer une influence sur l'appréciation du requérant quant à l'opportunité de contester le jugement. Par suite, cette indication erronée faisait obstacle à ce que le délai de recours contentieux courre à compter de cette notification.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr. CE, Section, 26 mars 1993, Melle,, n° 117557, p. 86 ; CE, 15 novembre 2006,,, n° 264636, T. pp. 713-1002. Comp., CE, 4 novembre 1992,,, n° 120283, T. p. 1205.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
