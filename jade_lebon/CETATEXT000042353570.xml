<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042353570</ID>
<ANCIEN_ID>JG_L_2020_09_000000430915</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/35/35/CETATEXT000042353570.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 21/09/2020, 430915</TITRE>
<DATE_DEC>2020-09-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>430915</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ ; OCCHIPINTI</AVOCATS>
<RAPPORTEUR>M. Vincent Daumas</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Cytermann</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:430915.20200921</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              La communauté d'agglomération Chalon-Val-de-Bourgogne a demandé au tribunal administratif de Dijon de condamner le département de Saône-et-Loire à lui verser une somme totale de 591 620,81 euros au titre de la participation de ce dernier au financement de travaux d'amélioration de la voirie de l'agglomération chalonnaise. Par un jugement n° 1501600 du 1er décembre 2016, le tribunal administratif de Dijon a fait droit à cette demande. <br/>
<br/>
              Par un arrêt n° 17LY00561 du 21 mars 2019, la cour administrative d'appel de Lyon a, sur appel du département de Saône-et-Loire, annulé ce jugement et rejeté la demande présentée par la communauté d'agglomération Chalon-Val-de-Bourgogne devant le tribunal administratif de Dijon. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 21 mai et 21 août 2019 au secrétariat du contentieux du Conseil d'Etat, la communauté d'agglomération Chalon-Val-de-Bourgogne demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel du département de Saône-et-Loire ;<br/>
<br/>
              3°) de mettre à la charge du département de Saône-et-Loire la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des collectivités territoriales ;<br/>
              - la loi n° 68-1250 du 31 décembre 1968 ;<br/>
              - le décret n° 62-1587 du 29 décembre 1962 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Vincent Daumas, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Laurent Cytermann, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de la communauté d'agglomération de Chalon-Val-de-Bourgogne et à Me Occhipinti, avocat du département de Saône-et-Loire ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la communauté d'agglomération Chalon-Val-de-Bourgogne a demandé au département de Saône-et-Loire le paiement de plusieurs sommes qu'elle estimait lui être dues, en exécution de conventions conclues avec le département, au titre de la participation de ce dernier au financement d'opérations d'amélioration de la voirie routière dans l'agglomération chalonnaise. A la suite du refus du département, la communauté d'agglomération a saisi du litige le tribunal administratif de Dijon qui, après avoir jugé non fondée l'exception de prescription soulevée par le département, a condamné ce dernier à lui verser une somme totale de 591 620,81 euros. Toutefois, sur appel du département, la cour administrative d'appel de Lyon, après avoir jugé prescrites les créances dont se prévalait la communauté d'agglomération, a annulé le jugement rendu en première instance et rejeté sa demande. La communauté d'agglomération Chalon-Val-de-Bourgogne se pourvoit en cassation contre l'arrêt rendu par la cour administrative d'appel.<br/>
<br/>
              2. D'une part, aux termes de l'article 1er de la loi du 31 décembre 1968 relative à la prescription des créances sur l'Etat, les départements, les communes et les établissements publics : " Sont prescrites, au profit de l'Etat, des départements et des communes, sans préjudice des déchéances particulières édictées par la loi, et sous réserve des dispositions de la présente loi, toutes créances qui n'ont pas été payées dans un délai de quatre ans à partir du premier jour de l'année suivant celle au cours de laquelle les droits ont été acquis (...) ". Aux termes de l'article 2 de cette même loi : " La prescription est interrompue par : / Toute demande de paiement ou toute réclamation écrite adressée par un créancier à l'autorité administrative, dès lors que la demande ou la réclamation a trait au fait générateur, à l'existence, au montant ou au paiement de la créance, alors même que l'administration saisie n'est pas celle qui aura finalement la charge du règlement (...) ".<br/>
<br/>
              3. D'autre part, aux termes de l'article 11 du décret du 29 décembre 1962 portant  règlement général sur la comptabilité publique, applicable à la résolution du litige, dont les dispositions ont depuis lors été reprises à l'article 18 du décret n° 2012-1246 du 7 novembre 2012 relatif à la gestion budgétaire et comptable publique : " Les comptables publics sont seuls chargés : / De la prise en charge et du recouvrement des ordres de recettes qui leur sont remis par les ordonnateurs, des créances constatées par un contrat, un titre de propriété ou autre titre dont ils assurent la conservation ainsi que de l'encaissement des droits au comptant et des recettes de toute nature que les organismes publics sont habilités à recevoir (...) ". Il résulte de ces dispositions que le comptable public d'une personne morale soumise aux principes généraux de la comptabilité publique, dès lors qu'il est chargé du recouvrement d'une créance dont cette dernière est titulaire sur une personne publique bénéficiaire de la prescription prévue par la loi du 31 décembre 1968, a qualité pour effectuer tous actes interruptifs du cours de cette prescription. Est sans incidence à cet égard la circonstance que l'action en recouvrement du comptable public se trouverait, par ailleurs, soumise au délai de prescription prévu au 3° de l'article L. 1617-5 du code général des collectivités territoriales.<br/>
<br/>
              4. Il résulte des énonciations de l'arrêt attaqué que les sommes en litige avaient fait l'objet de titres de recettes émis par la communauté d'agglomération Chalon-Val-de-Bourgogne à l'encontre du département de Saône-et-Loire. En jugeant que le comptable public de la communauté d'agglomération ne pouvait, agissant en cette seule qualité au titre de son action en recouvrement des recettes, interrompre valablement le cours de la prescription quadriennale prévue par la loi du 31 décembre 1968, la cour administrative d'appel a commis, eu égard à ce qui a été dit au point 3, une erreur de droit. La communauté d'agglomération Chalon-Val-de-Bourgogne est fondée, pour ce motif, et sans qu'il soit besoin d'examiner l'autre moyen de son pourvoi, à demander l'annulation de l'arrêt attaqué.<br/>
<br/>
              5. L'annulation de l'arrêt attaqué a pour conséquence de rendre sans objet les conclusions du pourvoi incident présenté par le département de Saône-et-Loire.<br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du département de Saône-et-Loire la somme de 3 000 euros à verser à la communauté d'agglomération Chalon-Val-de-Bourgogne au titre des dispositions de l'article L. 761-1 du code de justice administrative. Ces mêmes dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de la communauté d'agglomération qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'arrêt du 21 mars 2019 de la cour administrative d'appel de Lyon est annulé.<br/>
<br/>
Article 2 : Il n'y a pas lieu de statuer sur les conclusions incidentes présentées par le département de Saône-et-Loire à fin d'annulation de l'arrêt du 21 mars 2019 de la cour administrative d'appel de Lyon. <br/>
<br/>
Article 3 : L'affaire est renvoyée à la cour administrative d'appel de Lyon.<br/>
<br/>
Article 4 : Le département de Saône-et-Loire versera à la communauté d'agglomération Chalon-Val-de-Bourgogne la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 5 : Les conclusions du département de Saône-et-Loire présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 6 : La présente décision sera notifiée à la communauté d'agglomération Chalon-Val-de-Bourgogne et au département de Saône-et-Loire.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">18-04-02-05 COMPTABILITÉ PUBLIQUE ET BUDGET. DETTES DES COLLECTIVITÉS PUBLIQUES - PRESCRIPTION QUADRIENNALE. RÉGIME DE LA LOI DU 31 DÉCEMBRE 1968. INTERRUPTION DU COURS DU DÉLAI. - ACTES INTERRUPTIFS DE LA PRESCRIPTION QUADRIENNALE - ACTES DE RECOUVREMENT ACCOMPLIS PAR LE COMPTABLE PUBLIC - EXISTENCE, ALORS MÊME QUE L'ACTION EN RECOUVREMENT EST, PAR AILLEURS, SOUMISE AU DÉLAI DE PRESCRIPTION PRÉVU AU 3° DE L'ARTICLE L. 1617-5 DU CGCT.
</SCT>
<ANA ID="9A"> 18-04-02-05 Il résulte de l'article 11 du décret n° 62-1587 du 29 décembre 1962, dont les dispositions ont depuis lors été reprises à l'article 18 du décret n° 2012-1246 du 7 novembre 2012, que le comptable public d'une personne morale soumise aux principes généraux de la comptabilité publique, dès lors qu'il est chargé du recouvrement d'une créance dont cette dernière est titulaire sur une personne publique bénéficiaire de la prescription prévue par la loi n° 68-1250 du 31 décembre 1968, a qualité pour effectuer tous actes interruptifs du cours de cette prescription. Est sans incidence à cet égard la circonstance que l'action en recouvrement du comptable public se trouverait, par ailleurs, soumise au délai de prescription prévu au 3° de l'article L. 1617-5 du code général des collectivités territoriales (CGCT).</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
