<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000018802797</ID>
<ANCIEN_ID>JG_L_2008_05_000000300432</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/18/80/27/CETATEXT000018802797.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 14/05/2008, 300432</TITRE>
<DATE_DEC>2008-05-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>300432</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Martin</PRESIDENT>
<AVOCATS>SCP GASCHIGNARD</AVOCATS>
<RAPPORTEUR>M. Alain  Boulanger</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Derepas Luc</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 8 janvier et 10 avril 2007 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. Marcelino A, demeurant ... ; M. A demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision implicite par laquelle le Premier ministre a rejeté sa demande tendant à l'abrogation de l'article R. 262-6 du code de l'action sociale et des familles en tant que celui-ci ne fait pas figurer les rentes d'accident du travail parmi les prestations sociales spécialisées exclues des ressources prises en compte pour l'attribution du revenu minimum d'insertion ;<br/>
<br/>
              2°) d'enjoindre au Premier ministre de modifier ces dispositions ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 2 500 euros à verser à la SCP Gaschignard, avocat de M. A, au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, ainsi que le premier protocole additionnel à cette convention ;<br/>
<br/>
              Vu le code de l'action sociale et des familles, notamment ses articles L. 262-3, L. 262-10 et R. 262-6 ;<br/>
<br/>
              Vu le code de la sécurité sociale, notamment ses articles L. 432-1 et L. 434-2 ;<br/>
<br/>
              Vu la loi n° 91-647 du 10 juillet 1991 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Alain Boulanger, chargé des fonctions de Maître des requêtes,  <br/>
<br/>
              - les observations de la SCP Gaschignard, avocat de M. A, <br/>
<br/>
              - les conclusions de M. Luc Derepas, Commissaire du gouvernement ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant qu'aux termes de l'article L. 262-3 du code de l'action sociale et des familles : « Le bénéficiaire du revenu minimum d'insertion a droit à une allocation égale à la différence entre le montant du revenu minimum défini à l'article L. 262-2 et les ressources définies selon les modalités fixées aux articles L. 262-10 et L. 262-12. » ; qu'aux termes du premier alinéa de l'article L. 262-10 du même code : « L'ensemble des ressources des personnes retenues pour la détermination du montant du revenu minimum d'insertion est pris en compte pour le calcul de l'allocation » ; qu'aux termes du deuxième alinéa de ce même article : « Toutefois, certaines prestations sociales à objet spécialisé (...) peuvent, selon des modalités fixées par voie réglementaire, être exclues, en tout ou en partie, du montant des ressources servant au calcul de l'allocation (...) » ; qu'au nombre des prestations sociales énumérées par l'article R. 262-6 du même code ne figure pas la rente d'accident du travail prévue au deuxième alinéa de l'article L. 434-2 du code de la sécurité sociale ;<br/>
<br/>
              Considérant, en premier lieu, d'une part, que la rente versée aux victimes d'accidents du travail prévue au titre III intitulé : « Prestations » du livre IV du code de la sécurité sociale constitue, en tant que prestation sociale, une ressource au sens de l'article L. 262-3 du code de l'action sociale et des familles, susceptible d'être prise en compte pour la détermination du montant du revenu minimum d'insertion ; que, d'autre part, il résulte des dispositions de l'article L. 262-10 du code de l'action sociale et des familles que le législateur a entendu laisser au pouvoir réglementaire une large marge d'appréciation pour décider si, et le cas échéant dans quelle mesure, certaines prestations sociales à objet spécialisé, que la loi n'a pas elle-même exclues du montant des ressources déterminant le montant de l'allocation de revenu minimum d'insertion, pouvaient être exclues au même titre ; qu'eu égard à la nature et à l'objet de la rente d'accident du travail, le Premier ministre n'a ni méconnu les termes de l'article L. 262-10 du code de l'action sociale et des familles, ni commis une erreur manifeste d'appréciation en ne la faisant pas figurer parmi les prestations exclues du calcul des ressources déterminant le montant de l'allocation de revenu minimum d'insertion ; qu'eu égard à la différence de nature entre cette rente et l'indemnité en capital, prévue à l'article L. 434-1 du code de la sécurité sociale et exclue de ces ressources par le 7° de l'article R. 262-2 du code de l'action sociale et des familles, il n'a pas méconnu le principe d'égalité ;<br/>
<br/>
              Considérant, en second lieu, que les modalités de calcul de l'allocation de revenu minimum d'insertion déterminées par les dispositions précitées du code de l'action sociale et des familles sont sans incidence sur le droit à la rente d'accident du travail déterminé en application des articles L. 432-2 et suivants du code de la sécurité sociale ; qu'ainsi, le moyen tiré de ce que le refus de modifier l'article R. 262-6 du code de l'action sociale et des familles aurait pour effet de priver un allocataire du revenu minimum d'insertion de la jouissance du droit à l'allocation d'une rente d'accident du travail, en méconnaissance des stipulations de l'article 1er du premier protocole additionnel à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, est inopérant ;<br/>
<br/>
              Considérant qu'il résulte de ce qui précède que M. A n'est pas fondé à demander l'annulation de la décision implicite par laquelle le Premier ministre a rejeté sa demande de modification de l'article R. 262-6 du code de l'action sociale et des familles pour y inclure les rentes d'accident du travail parmi les prestations sociales spécialisées exclues des ressources prises en compte pour l'attribution du revenu minimum d'insertion ; que, par voie de conséquence, ses conclusions à fin d'injonction ainsi que celles présentées par son avocat au titre de l'article 37 de la loi du 10 juillet 1991 ne peuvent qu'être rejetées ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. A est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. Marcelino A, au Premier ministre et au ministre du travail, des relations sociales, de la famille et de la solidarité.<br/>
<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">04-02-06 AIDE SOCIALE. DIFFÉRENTES FORMES D'AIDE SOCIALE. REVENU MINIMUM D'INSERTION (RMI). - RESSOURCES À PRENDRE EN COMPTE POUR LA DÉTERMINATION DU MONTANT DE L'ALLOCATION - RENTE D'ACCIDENT DU TRAVAIL - COMPÉTENCE DU POUVOIR RÉGLEMENTAIRE POUR EXCLURE CERTAINES PRESTATIONS DES RESSOURCES DÉTERMINANT LE MONTANT - EXISTENCE - CONTRÔLE DU JUGE - CONTRÔLE D'ERREUR DE DROIT ET D'ERREUR MANIFESTE D'APPRÉCIATION.
</SCT>
<ANA ID="9A"> 04-02-06 La rente versée aux victimes d'accidents du travail prévue au titre III intitulé « Prestations » du livre IV du code de la sécurité sociale constitue, en tant que prestation sociale, une ressource au sens de l'article L. 262-3 du code de l'action sociale et des familles, susceptible d'être prise en compte pour la détermination du montant du revenu minimum d'insertion. Il résulte des dispositions de l'article L. 262-10 du code de l'action sociale et des familles que le législateur a entendu laisser au pouvoir réglementaire une large marge d'appréciation pour décider si, et le cas échéant dans quelle mesure, certaines prestations sociales à objet spécialisé, que la loi n'a pas elle-même exclues du montant des ressources déterminant le montant de l'allocation de revenu minimum d'insertion, pouvaient être exclues au même titre. Eu égard à la nature et à l'objet de la rente d'accident du travail, le Premier ministre n'a pas méconnu ces dispositions ni commis d'erreur manifeste d'appréciation en ne la faisant pas figurer parmi les prestations exclues du calcul des ressources déterminant le montant de l'allocation de revenu minimum d'insertion. Eu égard à la différence de nature entre cette rente et l'indemnité en capital, prévue à l'article L. 434-1 du code de la sécurité sociale et exclue de ces ressources par le 7° de l'article R. 262-2 du code de l'action sociale et des familles, le Premier ministre n'a pas méconnu le principe d'égalité en n'excluant pas celle-ci du calcul des ressources.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
