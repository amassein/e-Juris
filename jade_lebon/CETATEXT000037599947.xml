<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037599947</ID>
<ANCIEN_ID>JG_L_2018_11_000000408667</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/59/99/CETATEXT000037599947.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 6ème chambres réunies, 09/11/2018, 408667</TITRE>
<DATE_DEC>2018-11-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>408667</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS</AVOCATS>
<RAPPORTEUR>M. Jean-Dominique Langlais</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:408667.20181109</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Le Syndicat départemental de la propriété privée rurale de Vendée a demandé au tribunal administratif de Nantes d'annuler l'arrêté du 25 juin 2012 par lequel le préfet de la Vendée a fixé, en application de l'article L. 411-11 du code rural et de la pêche maritime, les minima et les maxima des loyers des bâtiments d'habitation relevant du statut du fermage et d'enjoindre au préfet de prendre un nouvel arrêté à cette fin. Par un jugement n° 1208191 du 13 mars 2015, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 15NT01520 du 6 janvier 2017, la cour administrative d'appel de Nantes a rejeté l'appel du Syndicat départemental de la propriété privée rurale de Vendée contre ce jugement. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 6 mars et 6 juin 2017 au secrétariat du contentieux du Conseil d'Etat, le Syndicat départemental de la propriété privée rurale de Vendée demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
<br/>
              - le code rural et de la pêche maritime ; <br/>
<br/>
              - le code de la construction et de l'habitation ; <br/>
<br/>
              - la loi n° 65-557 du 10 juillet 1965 ;<br/>
<br/>
              - le décret n° 67-223 du 17 mars 1967 ; <br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Langlais, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat du Syndicat départemental de la propriété privée rurale de Vendée.<br/>
<br/>
              Vu la note en délibéré, enregistrée le 24 octobre 2018, présentée par le ministre de l'agriculture.<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article L. 411-1 du code rural et de la pêche maritime : " Toute mise à disposition à titre onéreux d'un immeuble à usage agricole en vue de l'exploiter pour y exercer une activité agricole définie à l'article L. 311-1 est régie par les dispositions du présent titre " ; que, d'une part, aux termes des deux premiers alinéas de l'article L. 411-11 du même code : " Le prix de chaque fermage (...) est constitué, d'une part, du loyer des bâtiments d'habitation et, d'autre part, du loyer des bâtiments d'exploitation et des terres nues. / Le loyer des bâtiments d'habitation est fixé en monnaie entre des maxima et des minima qui sont arrêtés par l'autorité administrative sur la base de références calculées d'après des modalités définies par décret. Ce loyer ainsi que les maxima et les minima sont actualisés, chaque année, selon la variation de l'indice de référence des loyers publié par l'Institut national de la statistique et des études économiques chaque trimestre et qui correspond à la moyenne, sur les douze derniers mois, de l'évolution des prix à la consommation hors tabac et hors loyers. Ces références sont applicables aux baux en cours à la date d'entrée en vigueur de l'acte pris par l'autorité administrative dans chaque département pour arrêter les maxima et les minima. Le loyer des bâtiments d'habitation stipulé dans ces baux peut être révisé à l'initiative de l'une des parties au bail à compter de la publication de l'acte ci-dessus mentionné. A défaut d'accord entre les parties, le loyer des bâtiments d'habitation est fixé par le tribunal " ; que, d'autre part, aux termes du troisième et de l'avant-dernier alinéa de cet article : " Le loyer des terres nues et des bâtiments d'exploitation est fixé en monnaie entre des maxima et des minima arrêtés par l'autorité administrative. (...) Ces maxima et ces minima font l'objet d'un nouvel examen au plus tard tous les six ans. S'ils sont modifiés, le prix des baux en cours ne peut, sous réserve des dispositions figurant au premier alinéa de l'article L. 411-13, être révisé que lors du renouvellement ou, s'il s'agit d'un bail à long terme, en début de chaque nouvelle période de neuf ans. A défaut d'accord amiable, le tribunal paritaire des baux ruraux fixe le nouveau prix du bail "  ; qu'aux termes de l'article R. 411-1 du même code : " Pour l'application de l'article L. 411-11, le préfet fixe, par arrêté publié au recueil des actes administratifs de la préfecture : 1° Les maxima et minima des loyers des bâtiments d'habitation sont exprimés en monnaie et calculés par mètre carré de surface définie conformément aux dispositions de la loi n° 65-557 du 10 juillet 1965 fixant le statut de la copropriété des immeubles bâtis. Ces montants sont arrêtés par catégories en fonction de l'état d'entretien et de conservation des logements, de leur importance, de leur confort et de leur situation par rapport à l'exploitation ; ils tiennent compte des indicateurs publics ou privés mesurant les loyers pratiqués localement. (...) "<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 25 juin 2012, le préfet de la Vendée a fixé, sur le fondement des dispositions précitées, les minima et maxima des loyers au mètre carré des bâtiments d'habitation relevant du statut du fermage dans ce département ; que le Syndicat départemental de la propriété privée rurale de Vendée a présenté contre cet arrêté un recours pour excès de pouvoir que le tribunal administratif de Nantes a rejeté par un jugement du 13 mars 2015 ; qu'il se pourvoit en cassation contre l'arrêt du 6 janvier 2017 par lequel la cour administrative d'appel de Nantes a rejeté son appel contre ce jugement ;<br/>
<br/>
              3. Considérant, en premier lieu, que les dispositions de l'article R. 411-1 du code rural et de la pêche maritime n'impliquent de se référer aux dispositions de la loi du 10 juillet 1965 fixant le statut de la copropriété des immeubles bâtis pour déterminer la surface d'un bâtiment d'habitation que pour le calcul des minima et maxima encadrant son loyer ; qu'en jugeant que le préfet avait pu légalement prendre en compte la surface habitable des bâtiments en cause, définie conformément aux dispositions de l'article R. 111-2 du code de la construction et de l'habitation, pour procéder à leur classement par catégories, la cour n'a pas commis d'erreur de droit ; <br/>
<br/>
              4. Considérant, en deuxième lieu, qu'en jugeant que le préfet de la Vendée, qui avait tenu compte, pour fixer les minima et maxima des loyers des baux ruraux, de données statistiques pertinentes, différenciées selon les types de logements et les secteurs géographiques et pondérées au vu des loyers pratiqués couramment dans le département, notamment dans le secteur du logement social, n'avait pas commis d'erreur manifeste d'appréciation en procédant à l'encadrement des loyers en litige, la cour administrative d'appel n'a pas dénaturé les faits de l'espèce ni les pièces du dossier qui lui était soumis ; <br/>
<br/>
              5. Considérant, en troisième lieu, que la cour n'a pas commis d'erreur de droit en jugeant que le préfet de la Vendée avait pu, sans méconnaître ni la compétence qu'il tirait des dispositions de l'article R. 411-1 du code rural et de la pêche maritime, ni les dispositions citées ci-dessus des articles L. 411-1 et L. 411-11 du même code, fixer à 0 euro le loyer mensuel minimum pour les bâtiments d'habitation classés en 4ème catégorie ; <br/>
<br/>
              6. Considérant, en quatrième lieu, que l'arrêté attaqué retient, parmi les critères de classement des bâtiments, leur niveau de confort énergétique " établi au regard du diagnostic de performance énergétique en vigueur " ; que la cour administrative d'appel n'a pas commis d'erreur de droit en jugeant que cette disposition, qui ouvre aux propriétaires la faculté d'obtenir un classement plus favorable de leur bien en faisant réaliser le diagnostic prévu par l'article L. 134-1 du code de la construction et de l'habitation, ne méconnaissait pas les dispositions de l'article L. 134-3-1 de ce code, en vertu desquelles la production de ce diagnostic n'est pas obligatoire lors de la conclusion d'un bail rural ; <br/>
<br/>
              7. Mais considérant qu'il résulte du deuxième alinéa des dispositions précitées de l'article L. 411-11 du code rural et de la pêche maritime, éclairées par les travaux préparatoires de la loi du 27 juillet 2010 de modernisation de l'agriculture et de la pêche, de laquelle elles sont issues, que, par dérogation au principe résultant de l'avant-dernier alinéa, selon lequel le loyer des baux ruraux en cours ne peut être révisé qu'à la date de leur renouvellement ou, s'il s'agit de baux à long terme, au début de chaque nouvelle période de neuf ans, la publication de l'acte par lequel le préfet fixe des maxima et minima de loyer pour les bâtiments d'habitation ouvre à toute partie à un bail en cours relatif à un tel bâtiment la possibilité de demander que le loyer soit révisé pour être mis en conformité avec ces maxima et minima ; qu'ainsi, en jugeant que l'arrêté attaqué avait pu légalement prévoir, par son article 2,  qu'il n'était pas applicable aux baux en cours à la date de sa publication, ce qui impliquait qu'il ne pouvait donner lieu à une révision du loyer, la cour administrative d'appel a commis une erreur de droit qui justifie l'annulation de son arrêt en tant qu'il se prononce sur cet article 2 ;  <br/>
<br/>
              8. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler dans cette mesure l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ; <br/>
<br/>
              9. Considérant qu'il résulte de ce qui a été dit au point 7 que le préfet de Vendée ne pouvait, sans méconnaître les dispositions du code rural et de la pêche maritime, prévoir qu'il n'était possible de se prévaloir des maxima et minima qu'il fixait que pour les baux conclus ou renouvelés après la date de sa publication ; que le syndicat requérant est fondé à soutenir que c'est à tort que, par le jugement dont il relève appel, le tribunal administratif de Nantes a rejeté ses conclusions dirigées contre l'article 2 de l'arrêté litigieux ; <br/>
<br/>
              10. Considérant qu'aux termes de l'article L. 911-1 du code de justice administrative : " Lorsque sa décision implique nécessairement qu'une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public prenne une mesure d'exécution dans un sens déterminé, la juridiction, saisie de conclusions en ce sens, prescrit, par la même décision, cette mesure assortie, le cas échéant, d'un délai d'exécution " ; que la présente décision n'impliquant pas que le préfet de la Vendée substitue de nouvelles dispositions à celles de l'article 2 de son arrêté, il n'y a pas lieu de faire droit aux conclusions présentées en ce sens par le Syndicat départemental de la propriété privée de Vendée ; <br/>
<br/>
              11. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat, au titre des dispositions de l'article L. 761-1 du code de justice administrative, la somme de 7 000 euros à verser au Syndicat départemental de la propriété privée de Vendée pour les frais exposés par lui en première instance, en appel et en cassation ; <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 6 janvier 2017 de la cour administrative d'appel de Nantes est annulé en tant qu'il statue sur les conclusions du Syndicat départemental de la propriété privée rurale de Vendée tendant à l'annulation de l'article 2 de l'arrêté du 25 juin 2012 du préfet de la Vendée. <br/>
<br/>
Article 2 : L'article 2 de l'arrêté du 25 juin 2012 du préfet de la Vendée est annulé. <br/>
<br/>
Article 3 : Le jugement du 13 mars 2015 du tribunal administratif de Nantes est réformé en ce qu'il a de  contraire à la présente décision.<br/>
<br/>
Article 4 : L'Etat versera au Syndicat départemental de la propriété privée rurale de Vendée la somme de 7 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 5 : Le surplus des conclusions du Syndicat départemental de la propriété privée rurale de Vendée est rejeté.<br/>
<br/>
Article 6 : La présente décision sera notifiée au Syndicat départemental de la propriété privée rurale de Vendée et au ministre de l'agriculture.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">03-03-02-01 AGRICULTURE ET FORÊTS. EXPLOITATIONS AGRICOLES. STATUT DU FERMAGE ET DU MÉTAYAGE. BAUX RURAUX. - ARRÊTÉ PRÉFECTORAL FIXANT DES MAXIMA ET MINIMA DE LOYER POUR LES BÂTIMENTS D'HABITATION (2ÈME AL. DE L'ART. L. 411-11 DU CRPM) - PUBLICATION PERMETTANT À UNE PARTIE À UN BAIL EN COURS D'UN TEL BÂTIMENT DE DEMANDER LA RÉVISION DU LOYER EN VUE DE SA MISE EN CONFORMITÉ AVEC CES MAXIMA ET MINIMA - EXISTENCE, PAR DÉROGATION À LA DISPOSITION PRÉVOYANT UNE RÉVISION DES LOYERS DES BAUX RURAUX EN COURS À LA DATE DE LEUR RENOUVELLEMENT, OU AU DÉBUT DE CHAQUE NOUVELLE PÉRIODE DE NEUF ANS POUR LES BAUX À LONG TERME (AVANT-DERNIER AL. DU MÊME ART.).
</SCT>
<ANA ID="9A"> 03-03-02-01 Il résulte du deuxième alinéa de l'article L. 411-11 du code rural et de la pêche maritime (CRPM), éclairé par les travaux préparatoires de la loi n° 2010-874 du 27 juillet 2010, de laquelle il est issu, que, par dérogation au principe résultant de l'avant-dernier alinéa selon lequel le loyer des baux ruraux en cours ne peut être révisé qu'à la date de leur renouvellement ou, s'il s'agit de baux à long terme, au début de chaque nouvelle période de neuf ans, la publication de l'acte par lequel le préfet fixe des maxima et minima de loyer pour les bâtiments d'habitation ouvre à toute partie à un bail en cours relatif à un tel bâtiment la possibilité de demander que le loyer soit révisé pour être mis en conformité avec ces maxima et minima.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
