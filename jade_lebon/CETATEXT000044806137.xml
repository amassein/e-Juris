<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044806137</ID>
<ANCIEN_ID>JG_L_2021_12_000000436340</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/80/61/CETATEXT000044806137.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 5ème chambres réunies, 30/12/2021, 436340</TITRE>
<DATE_DEC>2021-12-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>436340</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DE NERVO, POUPET</AVOCATS>
<RAPPORTEUR>Mme Coralie Albumazard</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Stéphane Hoynck</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:436340.20211230</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par un arrêt n° S2019-2050 du 30 septembre 2019, la Cour des comptes a déclaré la société civile professionnelle " Chêne, Margollé, Martinage et Bertoux ", M. F... M... et M. N... E... conjointement et solidairement comptables de fait des deniers de Voies navigables de France et de la Chambre nationale de la batellerie artisanale pour la période du 13 février 2008 à la date la plus récente, ainsi que Maître Sylvain Martinage pour la période du 13 février 2008 au jour de son départ de la SCP, et Maître Nicole Chêne-Lozach pour la période du 20 janvier 2017 à la date la plus récente.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire, deux autres mémoires et un mémoire en réplique, enregistrés les 29 novembre 2019 et 2 mars, 9 mai, 9 juillet et 6 novembre 2020 au secrétariat du contentieux du Conseil d'Etat, M. E... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code des juridictions financières ;<br/>
              - l'ordonnance n° 45-2590 du 2 novembre 1945 ;<br/>
              - la loi n° 63-156 du 23 février 1963, notamment son article 60 ;<br/>
              - la loi n° 2014-1545 du 20 décembre 2014 ;<br/>
              - le décret n° 2016-544 du 3 mai 2016 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Coralie Albumazard, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Stéphane Hoynck, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP de Nervo, Poupet, avocat de M. E... ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis à la Cour des comptes que, par une convention signée le 20 novembre 1998, les établissements publics Voies navigables de France (VNF) et la Chambre nationale de la batellerie artisanale (CNBA), tous deux soumis aux règles de la comptabilité publique en vertu de leurs statuts, se sont engagés à créer un fonds spécial destiné à garantir les engagements accordés par la Société de cautionnement mutuel de la batellerie artisanale (SCMBA) lors des opérations d'emprunts réalisées par les sociétaires de cette dernière auprès d'établissements bancaires. A cette fin, une convention dite de séquestre a été signée le 7 janvier 1999 entre la CNBA, VNF, les banques intéressées et Me Sylvain Martinage, notaire associé de la société civile professionnelle (SCP) " Guy Cuvillon, Armand Martinage, Pierre Ducrocq, Nicole Chêne, Pierre Margollé et Sylvain Martinage, notaires ", par laquelle cet office a été chargé de contrôler l'exigibilité des dettes garanties, le respect des conditions initiales d'octroi de prêts et le respect de la procédure de mise en jeu de la garantie. Le caissier de la SCP, également signataire de cette convention, a été désigné comme tiers séquestre. M. E..., ancien président du conseil d'administration de VNF et signataire, à ce titre, de la convention du 20 novembre 1998, se pourvoit en cassation contre l'arrêt du 30 septembre 2019 par lequel la Cour des comptes l'a déclaré, conjointement et solidairement avec M. F... M..., ancien président de la CNBA, avec la SCP " Chêne, Margollé, Martinage et Bertoux ", et, pour certaines périodes avec Me Sylvain Martinage et Me Nicole Chêne-Lozach, comptable de fait des deniers de VNF et de la CNBA pour l'ensemble de la période non prescrite. <br/>
<br/>
              2. Aux termes du XI de l'article 60 de la loi du 23 février 1963 de finances pour 1963 : " XI - Toute personne qui, sans avoir la qualité de comptable public ou sans agir sous contrôle et pour le compte d'un comptable public, s'ingère dans le recouvrement de recettes affectées ou destinées à un organisme public doté d'un poste comptable ou dépendant d'un tel poste doit, nonobstant les poursuites qui pourraient être engagées devant les juridictions répressives, rendre compte au juge financier de l'emploi des fonds ou valeurs qu'elle a irrégulièrement détenus ou maniés. / Il en est de même pour toute personne qui reçoit ou manie directement ou indirectement des fonds ou valeurs extraits irrégulièrement de la caisse d'un organisme public et pour toute personne qui, sans avoir la qualité de comptable public, procède à des opérations portant sur des fonds ou valeurs n'appartenant pas aux organismes publics, mais que les comptables publics sont exclusivement chargés d'exécuter en vertu de la réglementation en vigueur. / Les gestions de fait sont soumises aux mêmes juridictions et entraînent les mêmes obligations et responsabilités que les gestions régulières. Néanmoins, le juge des comptes peut, hors le cas de mauvaise foi ou d'infidélité du comptable de fait, suppléer par des considérations d'équité à l'insuffisance des justifications produites. / Les comptables de fait pourront, dans le cas où ils n'ont pas fait l'objet pour les mêmes opérations des poursuites au titre du délit prévu et réprimé par l'article 433-12 du Code pénal, être condamnés aux amendes prévues par la loi ".<br/>
<br/>
              Sur la régularité de l'arrêt attaqué :<br/>
<br/>
              3. En premier lieu, aux termes de l'article L. 131-2 du code des juridictions financières : " La Cour des comptes juge les comptes que lui rendent les personnes qu'elle a déclarées comptables de fait. Elle n'a pas juridiction sur les ordonnateurs, sauf sur ceux qu'elle a déclarés comptables de fait (...) ". Aux termes de l'article R. 142-17 du même code : " La procédure applicable au jugement des comptes des comptables de fait est celle applicable aux comptables patents pour la phase contentieuse. (...) ". Aux termes de l'article R. 142-18 du même code : " Après que la Cour a déclaré une gestion de fait, elle en juge les comptes produits et statue sur l'application de l'amende prévue à l'article R. 131-1, au vu de nouvelles conclusions du ministère public, mais sans nouvelle réquisition du procureur général ". Si le premier alinéa de l'article R. 142-14 du même code impose à l'arrêt de la Cour de viser notamment les comptes jugés, il résulte des textes cités que cette exigence ne s'applique qu'à la procédure par laquelle, après avoir déclaré une gestion de fait, la cour en juge les comptes produits conformément à l'article R. 142-18. Par suite, le moyen tiré de ce que la cour aurait entaché d'irrégularité l'arrêt par lequel elle a déclaré M. E..., M. M..., la SCP " Chêne, Margollé, Martinage et Bertoux ", Me Martinage et Me Chêne-Lozach comptables de fait des deniers de VNF et de la CNBA, faute d'avoir visé les comptes jugés comme l'exige l'article R. 142-14 du code des juridictions financières, doit être écarté comme inopérant.<br/>
<br/>
              4. En deuxième lieu, par l'arrêt attaqué, la Cour des comptes a jugé que M. E... devait être attrait à la procédure d'apurement de la gestion de fait du 13 février 2008 jusqu'à la période la plus récente, en qualité de comptable de fait de longue main. Si M. E... reproche à la Cour d'avoir insuffisamment motivé sa décision, faute d'avoir précisément fixé le terme de la procédure d'apurement des comptes, il se déduit toutefois des énonciations de l'arrêt attaqué, aux termes desquelles le dispositif litigieux était toujours en place et n'avait pas donné lieu à régularisation à la date à laquelle la Cour a statué, que le compte unique de la gestion de fait qu'elle ordonne aux comptables de fait de produire doit porter sur la totalité des opérations constitutives de cette gestion, du 13 février 2008 à la date de cet arrêt. Par suite, le moyen d'insuffisance de motivation doit être écarté.<br/>
<br/>
              5. En troisième lieu, aux termes de l'article L. 142-1 du code des juridictions financières : " Les rapports d'examen des comptes à fin de jugement ou ceux contenant des faits soit susceptibles de conduire à une condamnation à l'amende, soit présomptifs de gestion de fait sont communiqués au représentant du ministère public près la Cour des comptes ". Aux termes de l'article L. 142-1-2 du même code : " Lorsque le ministère public relève, dans les rapports mentionnés à l'article L. 142-1 ou au vu des autres informations dont il dispose, un élément susceptible de conduire à la mise en jeu de la responsabilité personnelle et pécuniaire du comptable, ou présomptif de gestion de fait, il saisit la Cour des comptes. (...) ". S'il résulte de ces dispositions que le législateur a confié au ministère public près le juge des comptes le monopole des poursuites à l'encontre des comptables publics, le juge des comptes ne pouvant, ni en première instance ni en appel, se prononcer au-delà des termes du réquisitoire du ministère public, que ce soit de sa propre initiative ou pour répondre aux observations d'une partie, ces dispositions ne font pas obstacle à ce que ce juge déclare une gestion de fait portant sur une période postérieure à sa saisine par le ministère public, dès lors que les faits désignés dans le réquisitoire du ministère public comme constitutifs d'une gestion de fait ont perduré après cette date sans donner lieu à régularisation. Par suite, le moyen tiré de ce que la Cour aurait excédé le cadre de sa saisine en attrayant M. E... à la procédure de gestion de fait jusqu'à la date la plus récente doit être écarté.<br/>
<br/>
              Sur le caractère public des deniers en cause :<br/>
<br/>
              6. S'il ressort des pièces du dossier soumis à la Cour des comptes que, par la convention de séquestre du 7 janvier 1999, la CNBA et VNF ont octroyé au caissier de la SCP de notaires associée au dispositif litigieux de larges pouvoirs pour effectuer, en leur nom et pour leur compte, un certain nombre d'opérations financières, la Cour des comptes a pu, au terme d'une appréciation souveraine exempte de dénaturation, relever que ce caissier ne disposait pas d'une autonomie réelle dans le maniement des fonds puisqu'il ne pouvait agir que dans les limites étroites définies par la convention, pour en déduire que VNF et la CNBA étaient restés propriétaires des deniers qui avaient d'ailleurs continué à figurer à l'actif du bilan des deux établissements et qui, tout en étant détenus par l'office notarial, avaient conservé leur caractère public. Par suite, le moyen tiré de ce que la Cour aurait entaché son arrêt de dénaturation en refusant de reconnaître que ces deniers auraient perdu leur caractère public du fait de l'autonomie dans le maniement des fonds reconnue au tiers séquestre par le dispositif litigieux doit être écarté.<br/>
<br/>
              Sur la portée de la convention de séquestre :<br/>
<br/>
              7. En premier lieu, l'article 1er de l'ordonnance du 2 novembre 1945 relative au statut du notariat dispose que : " Les notaires sont les officiers publics, établis pour recevoir tous les actes et contrats auxquels les parties doivent ou veulent faire donner le caractère d'authenticité attaché aux actes de l'autorité publique, et pour en assurer la date, en conserver le dépôt, en délivrer des grosses et expéditions ". Contrairement à ce qui est soutenu, ces dispositions n'ont ni pour objet ni pour effet de permettre de déroger au profit des notaires au monopole de détention ou de maniement des deniers publics par les comptables publics, ni, par suite, de les autoriser à procéder à des opérations qui relèvent de la seule compétence de ces derniers. Par suite, le moyen tiré de ce que la Cour des comptes aurait entaché son arrêt d'une erreur de droit en jugeant que la SCP associée au dispositif litigieux ne disposait d'aucun titre l'habilitant à détenir et manier les deniers en cause et que, dès lors, aucune disposition législative n'autorisait le principe de la convention de séquestre conclue le 7 janvier 1999 doit être écarté. <br/>
<br/>
              8. En deuxième lieu, M. E... s'est prévalu devant la Cour des comptes de ce que la loi du 20 décembre 2014 relative à la simplification de la vie des entreprises et son décret d'application du 3 mai 2016 portant dispositions relatives aux conventions de mandat conclues par les établissements publics avec des tiers ont modifié l'état du droit et permis de donner une base légale à la convention de séquestre du 7 janvier 1999, qui constitue une convention de mandat désormais autorisée par ces dispositions. <br/>
<br/>
              9. Aux termes de l'article 40 de cette loi : " III.- L'Etat, ses établissements publics, les groupements nationaux d'intérêt public et les autorités publiques indépendantes peuvent, après avis conforme de leur comptable public et par convention écrite, confier à un organisme public ou privé l'encaissement de recettes ou le paiement de dépenses. / Peuvent être payées par convention de mandat : / 1° Les dépenses de fonctionnement ; / 2° Les dépenses d'investissement ; / 3° Les dépenses d'intervention ; / 4° Les aides à l'emploi ; / 5° Les dépenses de pensions, rentes et émoluments assimilés. / Peuvent être recouvrées par convention de mandat : / a) Les recettes propres des établissements publics de l'Etat, des groupements nationaux d'intérêt public et des autorités publiques indépendantes ; / b) Les recettes tirées des prestations fournies ; / c) Les redevances ; / d) Les recettes non fiscales issues de la délivrance des visas dans les chancelleries diplomatiques et consulaires. / La convention emporte mandat donné à l'organisme d'assurer l'encaissement de recettes ou le paiement de dépenses au nom et pour le compte de l'Etat, de l'établissement public, du groupement national d'intérêt public ou de l'autorité publique indépendante mandant. Elle prévoit une reddition au moins annuelle des comptes et des pièces correspondantes. Elle peut aussi prévoir le paiement par l'organisme mandataire du remboursement des recettes encaissées à tort et le recouvrement et l'apurement des éventuels indus résultant des paiements. / Les conditions d'application du présent III sont définies par décret. Pour les conventions de mandat conclues par l'Etat, le décret fixe notamment la durée des conventions, les montants susceptibles d'être payés et les conditions dans lesquelles le ministre chargé du budget peut autoriser des durées et des montants dérogatoires. / [...] V.  Les conventions de mandat en cours à la date de publication de la présente loi, conclues par l'Etat, ses établissements publics, les groupements nationaux d'intérêt public, les autorités publiques indépendantes, les collectivités territoriales et leurs établissements publics, sont rendues conformes, selon le cas, aux dispositions de l'article L. 1611-7-1 du code général des collectivités territoriales, tel qu'il résulte du II du présent article, ou aux dispositions du III, au plus tard lors de leur renouvellement ". Par ces dispositions, le législateur a notamment entendu que les conventions de mandat en cours à la date de publication de la loi du 20 décembre 2014 soient mises en conformité avec les conditions qu'elles édictent sans délai et, au plus tard, à la date de leur renouvellement. <br/>
<br/>
              10. Pour écarter le moyen tiré de ce que, par les dispositions citées au point précédent, le législateur aurait donné une base légale au dispositif litigieux, la Cour des comptes a jugé, qu'en l'absence de toute clause de renouvellement, la convention de séquestre du 7 janvier 1999 aurait dû être mise sans délai en conformité avec la loi du 20 décembre 2014 et que, faute de l'avoir été, cette convention ne pouvait, même à titre rétroactif, être considérée comme ayant validé le dispositif litigieux. En statuant ainsi, la Cour n'a pas commis d'erreur de droit ni d'erreur de qualification juridique. Elle n'a pas non plus commis d'erreur de droit en jugeant, pour écarter la loi du 20 décembre 2014, que le tiers séquestre s'était en tout état de cause dépouillé de son titre légal en n'appliquant pas les termes de la convention du 7 janvier 1999.<br/>
<br/>
              11. En troisième lieu, la Cour des comptes a relevé, dans l'arrêt attaqué, qu'en tout état de cause, dans l'hypothèse où il serait admis que la convention de séquestre du 7 janvier 1999 habilitait l'office notarial à détenir et manier des fonds publics, le tiers séquestre s'était dépouillé de son titre légal en n'appliquant pas les termes de cette convention, d'une part en ne rendant aucun compte de l'emploi des fonds entre 2005 et 2016, alors que la reddition annuelle des comptes et des pièces justificatives les appuyant s'imposait, et, d'autre part, en ne reversant pas aux établissements publics les sommes récupérées à titre de recouvrement sur les débiteurs défaillants, pas davantage que les fonds correspondant à des garanties devenues sans objet. Elle a également relevé que ni VNF, ni la CNBA ne disposaient d'éléments permettant de connaître exactement les opérations effectuées sur leurs fonds placés sous séquestre. En statuant ainsi, la Cour des comptes n'a, au terme de ses constatations souveraines, ni dénaturé les stipulations de la convention de séquestre ou les autres pièces du dossier qui lui était soumis, ni entaché sa décision d'une contradiction de motifs. <br/>
<br/>
              Sur le périmètre temporel et matériel de la gestion de fait :<br/>
<br/>
              12. En premier lieu, d'une part, la procédure de gestion de fait permet de saisir en leur chef toutes les personnes ayant contribué à la mise en place de la gestion de fait, même si elles n'ont pas manipulé directement les deniers publics en cause. Ces personnes peuvent être déclarées comptables de fait si elles ont participé, fût-ce indirectement, aux irrégularités financières, ou si elles les ont tolérées ou facilitées par leur inaction. D'autre part, aux termes du dernier alinéa de l'article L. 131-2 du code des juridictions financières : " L'action en déclaration de gestion de fait est prescrite pour les actes constitutifs de gestion de fait commis plus de dix ans avant la date à laquelle la Cour des comptes en est saisie ". Par l'arrêt attaqué, la Cour des comptes, après avoir qualifié le montage litigieux de gestion de fait, a jugé que, dès lors qu'elle avait été saisie par un premier réquisitoire du ministère public le 13 février 2018, cette gestion de fait devait être déclarée et apurée à compter du 13 février 2008 conformément aux dispositions du dernier alinéa de l'article L. 131-2 du code des juridictions financières. Puis elle a relevé que M. E..., organisateur du dispositif à son origine en sa qualité de président du conseil d'administration de VNF et signataire de la convention du 20 novembre 1998, avait, par définition, connu et toléré le dispositif, auquel il n'avait, à aucun moment, cherché à mettre fin, laissant ainsi le dispositif perdurer. En déduisant de ces constatations souveraines, exemptes de dénaturation, que bien qu'il ait quitté ses fonctions de président du conseil d'administration en juillet 2008, M. E... devait être attrait à la procédure d'apurement de la gestion de fait du 13 février 2008 jusqu'à la période la plus récente en sa qualité de comptable de fait de longue main, la Cour, qui s'est appuyée sur le fait que, pendant cette période non prescrite, M. E... avait eu la possibilité juridique de mettre un terme au dispositif litigieux pendant lorsqu'il était encore en fonctions et avait laissé perdurer le dispositif litigieux faute d'avoir informé ses successeurs de l'existence de ce dispositif auquel il n'avait ainsi jamais été mis un terme, n'a commis ni erreur de droit, ni erreur de qualification juridique des faits.<br/>
<br/>
              13. En deuxième lieu, si M. E... soutient que la Cour des comptes a méconnu les dispositions du paragraphe 1 de l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales en jugeant qu'il devait être attrait à la procédure d'apurement de la gestion de fait pour une période postérieure à la cessation de ses fonctions alors qu'elle a, en revanche, jugé que Me Martinage ne devait être attrait à cette procédure que jusqu'à la date de cessation de ses fonctions au sein de l'office notarial, en novembre 2016, cette circonstance n'est, en tout état de cause, pas de nature à caractériser une méconnaissance des exigences du procès équitable fixées par ces dispositions. Le moyen doit, par suite, être écarté.<br/>
<br/>
              14. En troisième et dernier lieu, au stade de la déclaration de gestion de fait, il appartient au juge des comptes de déterminer si chacune des personnes mises en cause a participé de façon suffisamment déterminante aux opérations irrégulières pour être déclarée comptable de fait. Lorsque plusieurs personnes ont participé de façon indifférenciée et suffisamment déterminante aux opérations irrégulières, le juge des comptes les déclare solidairement comptables de fait. Dans une telle hypothèse, le lien de solidarité ainsi instauré entre elles ne peut plus être remis en cause à l'occasion du jugement du compte de cette gestion de fait, seul pouvant être discuté à ce stade de la procédure le périmètre exact des opérations comptables auxquelles s'applique cette solidarité. <br/>
<br/>
              15. Si M. E... soutient qu'il ne peut, en tout état de cause, être déclaré comptable de fait des deniers de VNF et de la CNBA pour la période postérieure à juillet 2008, date à laquelle il a quitté ses fonctions de président du conseil d'administration de VNF, la Cour des comptes n'a commis ni erreur de droit, ni erreur de qualification juridique des faits en jugeant que l'intéressé, qui, pour avoir signé la convention du 20 novembre 1998, avait connaissance du dispositif litigieux et n'avait pas entrepris de démarche pour y mettre un terme, pouvait être déclaré comptable de fait, solidairement et conjointement avec M. M..., la SCP de notaires, Me Martinage et Me Chêne-Lozach, des deniers de VNF et de la CNBA pour la période allant du 13 février 2008 à la date la plus récente. Par suite, ces moyens doivent être écartés.<br/>
<br/>
              16. Il résulte de tout ce qui précède que M. E... n'est pas fondé à demander l'annulation de l'arrêt qu'il attaque. Son pourvoi doit, par suite, être rejeté, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le pourvoi de M. E... est rejeté.<br/>
Article 2 : La présente décision sera notifiée à M. N... E... et à la Procureure générale près la Cour des comptes.<br/>
Copie en sera adressée au ministre de l'économie, des finances et de la relance, à M. K... H..., à M. F... M..., à la société civile professionnelle " Chêne, Margollé, Martinage et Bertoux ", à Me Sylvain Martinage, à Me Pierre Margollé, à Me Bertoux, à Me Nicole Chêne-Lozach, à Me Laurent Deruy, à M. A... R..., à M. K... O..., et à M. I... D....<br/>
<br/>
<br/>
<br/>
<br/>
              Délibéré à l'issue de la séance du 13 décembre 2021 où siégeaient : Mme Christine Maugüé, présidente adjointe de la section du contentieux, présidant ; M. C... Q..., M. Fabien Raynaud, présidents de chambre ; Mme W... S..., M. V... G..., Mme J... U..., M. B... T..., Mme Rozen Noguellou, conseillers d'Etat et Mme Coralie Albumazard, maître des requêtes en service extraordinaire-rapporteure. <br/>
<br/>
              Rendu le 30 décembre 2021.<br/>
<br/>
<br/>
                 La présidente : <br/>
                 Signé : Mme Christine Maugüé<br/>
 		La rapporteure : <br/>
      Signé : Mme Coralie Albumazard<br/>
                 La secrétaire :<br/>
                 Signé : Mme L... P...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">18-01 COMPTABILITÉ PUBLIQUE ET BUDGET. - RÉGIME JURIDIQUE DES ORDONNATEURS ET DES COMPTABLES. - COMPÉTENCE EXCLUSIVE DES COMPTABLES PUBLICS POUR MANIPULER LES DENIERS PUBLICS - EXCEPTION - CONVENTIONS DE MANDAT - VALIDATION PAR LA LOI DU 20 DÉCEMBRE 2014 - CONDITIONS.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">18-01-04 COMPTABILITÉ PUBLIQUE ET BUDGET. - RÉGIME JURIDIQUE DES ORDONNATEURS ET DES COMPTABLES. - JUGEMENT DES COMPTES. - GESTION DE FAIT - PERSONNES POUVANT ÊTRE DÉCLARÉES COMPTABLES DE FAIT - PERSONNE AYANT CONTRIBUÉ À LA GESTION DE FAIT SANS AVOIR MANIPULÉ DES DENIERS PUBLICS (COMPTABLE DE FAIT DIT DE LONGUE MAIN) [RJ1] - INCLUSION, ALORS MÊME QUE CETTE PERSONNE A CESSÉ SES FONCTIONS PEU APRÈS LE DÉBUT DE LA PÉRIODE NON PRESCRITE ET QUE, POUR L'ESSENTIEL DE CETTE PÉRIODE, ELLE N'ÉTAIT PLUS EN FONCTIONS.
</SCT>
<ANA ID="9A"> 18-01 Article 40 de la loi n° 2014-1545 du 20 décembre 2014 autorisant l'Etat, ses établissements publics, les groupements nationaux d'intérêt public et les autorités publiques indépendantes à confier, après avis conforme de leur comptable public et par convention écrite, à un organisme public ou privé l'encaissement de certaines recettes ou le paiement de certaines dépenses.......Par ces dispositions, le législateur a notamment entendu que les conventions de mandat en cours à la date de publication de cette loi soient mises en conformité avec les conditions qu'elles édictent sans délai et, au plus tard, à la date de leur renouvellement.......Ces dispositions ne sauraient avoir pour effet de donner une base légale à une convention de séquestre, conclue antérieurement à celles-ci, qui ne comporte aucune clause de renouvellement et n'a pas été mise en conformité avec les conditions qu'elles édictent.</ANA>
<ANA ID="9B"> 18-01-04 La procédure de gestion de fait permet de saisir en leur chef toutes les personnes ayant contribué à la mise en place de la gestion de fait, même si elles n'ont pas manipulé directement les deniers publics en cause. Ces personnes peuvent être déclarées comptables de fait si elles ont participé, fût-ce indirectement, aux irrégularités financières, ou si elles les ont tolérées ou facilitées par leur inaction. ......Il en va ainsi d'une personne qui, en sa qualité de président du conseil d'administration d'un établissement public, a organisé à son origine puis laissé perdurer un dispositif constitutif de telles irrégularités.......Cette personne doit être attraite à la procédure d'apurement de la gestion de fait, en sa qualité de comptable de fait de longue main, alors même qu'elle a cessé ses fonctions peu après le début de la période de gestion de fait non prescrite et que, pour l'essentiel de cette période, elle n'était plus en fonctions, dès lors que, pendant la période non prescrite, elle avait eu la possibilité juridique de mettre un terme au dispositif litigieux lorsqu'elle était encore en fonction et avait laissé perdurer le dispositif litigieux faute d'avoir informé ses successeurs de l'existence de ce dispositif auquel elle n'avait ainsi jamais mis un terme.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 28 septembre 2016, M. Lecomte et autres, n°s 385903 385922, p. 395.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
