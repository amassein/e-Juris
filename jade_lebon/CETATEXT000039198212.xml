<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039198212</ID>
<ANCIEN_ID>JG_L_2019_10_000000421367</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/19/82/CETATEXT000039198212.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 09/10/2019, 421367</TITRE>
<DATE_DEC>2019-10-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>421367</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP POTIER DE LA VARDE, BUK LAMENT, ROBILLOT ; SCP MEIER-BOURDEAU, LECUYER</AVOCATS>
<RAPPORTEUR>M. Yves Doutriaux</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie Roussel</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:421367.20191009</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A... B... a demandé au tribunal administratif de Nouvelle-Calédonie l'annulation pour excès de pouvoir de la décision du 4 novembre 2016 par laquelle la commission de recours de la Fédération calédonienne de football a confirmé les décisions de la commission fédérale de discipline du 27 octobre 2015 lui infligeant les sanctions de radiation à vie de toutes fonctions officielles, d'interdiction d'accès au stade pendant cinq ans et d'interdiction de vestiaire des arbitres et de banc de touche à vie.<br/>
<br/>
              Par un jugement n° 1700005 du 1er juin 2017, le tribunal administratif de Nouvelle-Calédonie a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 17PA02157 du 8 mars 2018, la cour administrative de Paris, sur appel de M. B..., a annulé le jugement du 1er juin 2017 et la décision du 4 novembre 2016.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 11 juin et 12 septembre 2018 au secrétariat du contentieux du Conseil d'Etat, la Fédération calédonienne de football demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de M. B... ; <br/>
<br/>
              3°) de mettre à la charge de M. B... la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi organique n° 99-209 et la loi n° 99-210 du 19 mars 1999 ;<br/>
              - la délibération n° 251 du 16 octobre 2001 relative au sport en Nouvelle-Calédonie ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Yves Doutriaux, conseiller d'Etat,<br/>
<br/>
              - les conclusions de Mme Sophie Roussel, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Potier de La Varde, Buk Lament, Robillot, avocat de la fédération calédonienne de football, et à la SCP Meier-Bourdeau, Lecuyer, avocat de M. B... ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1.	Il ressort des pièces du dossier soumis aux juges du fond que M. B..., entraîneur-dirigeant du club de futsal AS Université de la Nouvelle-Calédonie, a fait l'objet, le 27 octobre 2015, d'une radiation à vie de toutes fonctions officielles, d'une interdiction de stade pendant cinq ans et d'une interdiction de vestiaire des arbitres et de banc de touche à vie prononcées par la commission de discipline de la Fédération calédonienne de football, à raison des coups qu'il aurait portés sur un joueur, des agissements brutaux auxquels il se serait livré à l'encontre d'un arbitre et de la falsification d'un document officiel, l'ensemble de ces faits ayant été commis à l'occasion d'une rencontre sportive s'étant déroulée le 25 juillet 2015. Par une décision du 4 novembre 2016, dont M. B... a demandé l'annulation au tribunal administratif de Nouvelle-Calédonie, la commission de recours de la Fédération calédonienne de football a confirmé la décision de la commission fédérale de discipline. Par un jugement du 1er juin 2017, le tribunal administratif a rejeté la demande de M. B.... Toutefois, sur appel de celui-ci, la cour administrative de Paris, par un arrêt du 8 mars 2018 contre lequel la Fédération calédonienne de football se pourvoit en cassation, a annulé ce jugement ainsi que la décision du 4 novembre 2016.<br/>
<br/>
              2.	Il ressort des énonciations de l'arrêt attaqué que, pour annuler la sanction infligée à M. B..., la cour administrative d'appel de Paris a relevé que la décision du 4 novembre 2016 de la commission de recours de la Fédération calédonienne de football devait être regardée comme reposant sur des faits matériellement inexacts, dès lors que la fédération ne produisait aucun élément de nature à établir qu'il aurait déjà fait l'objet d'une sanction définitive pour des faits similaires à ceux qui lui étaient reprochés et qu'aucune récidive ne pouvait en conséquence être caractérisée en l'espèce. En se fondant sur un tel motif, alors que la décision du 4 novembre 2016 se borne à indiquer, pour justifier les sanctions infligées à l'intéressé, que celui-ci a déjà fait l'objet d'une procédure disciplinaire pour des faits similaires, sans faire référence à la notion de récidive, ni même aux dispositions du règlement disciplinaire fédéral relatives à cette notion, la cour s'est fondée sur une inexacte interprétation de la décision attaquée. Par suite, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, son arrêt doit être annulé.<br/>
<br/>
              3.	Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de M. B... la somme de 3 000 euros à verser à la Fédération calédonienne de football, au titre des dispositions de l'article L. 761-1 du code de justice administrative. Ces dispositions font obstacle à ce qu'une somme soit mise à la charge de la Fédération calédonienne de football à ce titre. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'arrêt en date du 8 mars 2018 de la cour administrative de Paris est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative de Paris.<br/>
<br/>
Article 3 : M. B... versera la somme de 3 000 euros à la Fédération calédonienne de football au titre des dispositions de l'article L. 761-1 du code de justice administrative. Les conclusions de M. B... à ce titre sont rejetées.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la Fédération calédonienne de football et à M. A... B....<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">17-03-02-07-04 COMPÉTENCE. RÉPARTITION DES COMPÉTENCES ENTRE LES DEUX ORDRES DE JURIDICTION. COMPÉTENCE DÉTERMINÉE PAR UN CRITÈRE JURISPRUDENTIEL. PROBLÈMES PARTICULIERS POSÉS PAR CERTAINES CATÉGORIES DE SERVICES PUBLICS. ORGANISME PRIVÉ GÉRANT UN SERVICE PUBLIC. - FÉDÉRATION SPORTIVE CALÉDONIENNE AGRÉÉE - SANCTION, PRISE À L'ENCONTRE D'UN ENTRAÎNEUR-DIRIGEANT DE CLUB, PORTANT SUR L'ACCÈS DE L'INTÉRESSÉ AU SERVICE PUBLIC GÉRÉ PAR CETTE FÉDÉRATION - EXERCICE DE PRÉROGATIVES DE PUISSANCE PUBLIQUE - EXISTENCE - CONSÉQUENCE - COMPÉTENCE DE LA JURIDICTION ADMINISTRATIVE (SOL. IMP.) [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">46-01-02-01 OUTRE-MER. DROIT APPLICABLE. STATUTS. NOUVELLE-CALÉDONIE. - FÉDÉRATION SPORTIVE CALÉDONIENNE AGRÉÉE - SANCTION, PRISE À L'ENCONTRE D'UN ENTRAÎNEUR-DIRIGEANT DE CLUB, PORTANT SUR L'ACCÈS DE L'INTÉRESSÉ AU SERVICE PUBLIC GÉRÉ PAR CETTE FÉDÉRATION - EXERCICE DE PRÉROGATIVES DE PUISSANCE PUBLIQUE - EXISTENCE - CONSÉQUENCE - COMPÉTENCE DE LA JURIDICTION ADMINISTRATIVE (SOL. IMP.) [RJ1].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">63-05-01-02 SPORTS ET JEUX. SPORTS. FÉDÉRATIONS SPORTIVES. EXERCICE DU POUVOIR DISCIPLINAIRE. - FÉDÉRATION SPORTIVE CALÉDONIENNE AGRÉÉE - SANCTION, PRISE À L'ENCONTRE D'UN ENTRAÎNEUR-DIRIGEANT DE CLUB, PORTANT SUR L'ACCÈS DE L'INTÉRESSÉ AU SERVICE PUBLIC GÉRÉ PAR CETTE FÉDÉRATION - EXERCICE DE PRÉROGATIVES DE PUISSANCE PUBLIQUE - EXISTENCE - CONSÉQUENCE - COMPÉTENCE DE LA JURIDICTION ADMINISTRATIVE (SOL. IMP.) [RJ1].
</SCT>
<ANA ID="9A"> 17-03-02-07-04 Décision de la Fédération calédonienne de football, agréée par le gouvernement de Nouvelle-Calédonie et disposant à ce titre d'un monopole pour l'organisation des compétitions sportives à l'issue desquelles sont délivrés les titres de champions de Nouvelle-Calédonie, infligeant à un entraîneur-dirigeant de club une sanction de radiation à vie de toutes fonctions officielles, d'interdiction de stade pendant cinq ans et d'interdiction de vestiaire des arbitres et de banc de touche à vie.,,Une telle sanction, qui porte sur l'accès de l'intéressé au service public géré par la fédération, et non sur le fonctionnement interne de la fédération, relève de l'exercice de prérogatives de puissance publiques conférées à cette fédération pour assurer sa mission de service public. Par suite, le litige relatif à cette sanction ressortit à la compétence de la juridiction administrative.</ANA>
<ANA ID="9B"> 46-01-02-01 Décision de la Fédération calédonienne de football, agréée par le gouvernement de Nouvelle-Calédonie et disposant à ce titre d'un monopole pour l'organisation des compétitions sportives à l'issue desquelles sont délivrés les titres de champions de Nouvelle-Calédonie, infligeant à un entraîneur-dirigeant de club une sanction de radiation à vie de toutes fonctions officielles, d'interdiction de stade pendant cinq ans et d'interdiction de vestiaire des arbitres et de banc de touche à vie.... ...Une telle sanction, qui porte sur l'accès de l'intéressé au service public géré par la fédération, et non sur le fonctionnement interne de la fédération, relève de l'exercice de prérogatives de puissance publiques conférées à cette fédération pour assurer sa mission de service public. Par suite, le litige relatif à cette sanction ressortit à la compétence de la juridiction administrative.</ANA>
<ANA ID="9C"> 63-05-01-02 Décision de la Fédération calédonienne de football, agréée par le gouvernement de Nouvelle-Calédonie et disposant à ce titre d'un monopole pour l'organisation des compétitions sportives à l'issue desquelles sont délivrés les titres de champions de Nouvelle-Calédonie, infligeant à un entraîneur-dirigeant de club une sanction de radiation à vie de toutes fonctions officielles, d'interdiction de stade pendant cinq ans et d'interdiction de vestiaire des arbitres et de banc de touche à vie.... ...Une telle sanction, qui porte sur l'accès de l'intéressé au service public géré par la fédération, et non sur le fonctionnement interne de la fédération, relève de l'exercice de prérogatives de puissance publiques conférées à cette fédération pour assurer sa mission de service public. Par suite, le litige relatif à cette sanction ressortit à la compétence de la juridiction administrative.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Comp. CE,19 décembre 1988, Mme,et autres, n° 79962, p. 459.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
