<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034330309</ID>
<ANCIEN_ID>JG_L_2017_03_000000388099</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/33/03/CETATEXT000034330309.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 4ème chambres réunies, 31/03/2017, 388099</TITRE>
<DATE_DEC>2017-03-31</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>388099</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP RICHARD</AVOCATS>
<RAPPORTEUR>M. Jean-Dominique Langlais</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:388099.20170331</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Strasbourg d'annuler l'arrêté du 7 juin 2011 par lequel la directrice générale du centre national de gestion des praticiens hospitaliers et des personnels de direction de la fonction publique hospitalière a prononcé sa révocation. Par un jugement n° 1103955 du 3 juin 2013, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 13NC01485 du 18 décembre 2014, la cour administrative d'appel de Nancy a, à la demande de M.A..., annulé ce jugement ainsi que l'arrêté du 7 juin 2011.<br/>
<br/>
              Par un pourvoi, enregistré le 18 février 2015 au secrétariat du contentieux du Conseil d'Etat, le ministre des affaires sociales et de la santé demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de M.A....<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
<br/>
              - le code de la santé publique ; <br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Langlais, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Richard, avocat de M.A....<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 7 juin 2011, la directrice générale du centre national de gestion des praticiens hospitaliers et des personnels de direction de la fonction publique hospitalière a révoqué M. B... A...du corps des praticiens hospitaliers ; que, par un jugement du 3 juin 2013, le tribunal administratif de Strasbourg a rejeté le recours de l'intéressé contre cette décision ; que le ministre des affaires sociales et de la santé se pourvoit en cassation contre l'arrêt du 18 décembre 2014 par lequel la cour administrative d'appel de Nancy, statuant sur la requête de M.A..., a annulé ce jugement ainsi que la décision de révocation ; <br/>
<br/>
              2. Considérant qu'aux termes du premier alinéa de l'article R. 6152-313 du code de la santé publique : " Pour chaque affaire, le président du conseil de discipline choisit un rapporteur soit parmi les membres ou anciens membres de l'inspection générale des affaires sociales, docteurs en médecine n'appartenant pas au conseil de discipline, soit parmi les directeurs régionaux ou anciens directeurs régionaux des affaires sanitaires et sociales, docteurs en médecine, soit parmi les médecins ou pharmaciens inspecteurs régionaux de santé publique, exception faite du directeur régional des affaires sanitaires et sociales et du médecin ou du pharmacien inspecteur régional de santé publique de la région de l'établissement où exerce le praticien intéressé " ; qu'aux termes du premier et du troisième alinéa de l'article R. 6152-314 du même code : " Le rapporteur instruit l'affaire par tous les moyens propres à éclairer le conseil de discipline ; il établit un rapport écrit contenant l'exposé des faits et les moyens des parties et le transmet au président du conseil de discipline. / (... )/ Le rapporteur assiste avec voix consultative à la séance du conseil de discipline devant lequel il donne lecture de son rapport en présence du praticien intéressé et, le cas échéant, du défenseur qui l'assiste. Il peut fournir toutes observations complémentaires " ; que ces dispositions relatives à la procédure disciplinaire ne font pas obstacle à ce que le rapporteur auprès du conseil de discipline exprime, tant dans son rapport ou dans les observations qu'il formule verbalement en présence du praticien poursuivi et de son défenseur qu'au cours de la délibération du conseil, une appréciation sur les éléments que l'instruction a permis de dégager ; qu'ainsi, dès lors que le rapporteur n'a pas manifesté envers l'intéressé une animosité particulière révélant un défaut d'impartialité, la circonstance qu'il a fait état de son opinion sur l'opportunité de prononcer une sanction et, le cas échéant, sur la sanction qui lui paraissait adaptée aux faits n'est pas de nature à entacher d'irrégularité l'avis du conseil de discipline ;<br/>
<br/>
              3. Considérant qu'il résulte de ce qui précède, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, qu'en se fondant sur la seule circonstance que le rapporteur auprès du conseil de discipline réuni le 29 avril 2011 avait, dans ses observations orales prononcées en présence de M. A...et de son défenseur, émis l'opinion que les faits justifiaient " une lourde sanction " pour juger que l'avis émis par le conseil était irrégulier et que la décision de la directrice générale du centre national de gestion était, par suite, entachée d'illégalité, la cour administrative d'appel de Nancy a commis une erreur de droit qui justifie l'annulation de son arrêt ; <br/>
<br/>
              4. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'arrêt du 18 décembre 2014 de la cour administrative d'appel de Nancy est annulé.<br/>
<br/>
		Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nancy.<br/>
Article 3 : Les conclusions présentées par M. A...au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la ministre des affaires sociales et de la santé et à M. B...A....<br/>
Copie en sera adressée à la directrice générale du centre national de gestion des praticiens hospitaliers et des personnels de direction de la fonction publique hospitalière. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-09-05-01 FONCTIONNAIRES ET AGENTS PUBLICS. DISCIPLINE. PROCÉDURE. CONSEIL DE DISCIPLINE. - RAPPORTEUR AUPRÈS DU CONSEIL DE DISCIPLINE COMPÉTENT À L'ÉGARD DES PRATICIENS HOSPITALIERS (ART. R. 6152-313 ET R. 6152-314 DU CSP) - POSSIBILITÉ DE FORMULER UNE APPRÉCIATION SUR LES ÉLÉMENTS QUE L'INSTRUCTION A PERMIS DE DÉGAGER - EXISTENCE, SOUS RÉSERVE DU RESPECT DU PRINCIPE D'IMPARTIALITÉ.
</SCT>
<ANA ID="9A"> 36-09-05-01 Les dispositions des articles R. 6152-313 et R. 6152-314 du code de la santé publique (CSP) relatives à la procédure disciplinaire ne font pas obstacle à ce que le rapporteur auprès du conseil de discipline exprime, tant dans son rapport ou dans les observations qu'il formule verbalement en présence du praticien poursuivi et de son défenseur qu'au cours de la délibération du conseil, une appréciation sur les éléments que l'instruction a permis de dégager. Ainsi, dès lors que le rapporteur n'a pas manifesté envers l'intéressé une animosité particulière révélant un défaut d'impartialité, la circonstance qu'il a fait état de son opinion sur l'opportunité de prononcer une sanction et, le cas échéant, sur la sanction qui lui paraissait adaptée aux faits n'est pas de nature à entacher d'irrégularité l'avis du conseil de discipline.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
