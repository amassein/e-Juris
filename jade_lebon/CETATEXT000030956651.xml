<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030956651</ID>
<ANCIEN_ID>JG_L_2015_07_000000382443</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/95/66/CETATEXT000030956651.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème / 9ème SSR, 27/07/2015, 382443, Publié au recueil Lebon</TITRE>
<DATE_DEC>2015-07-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>382443</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème / 9ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ODENT, POULET</AVOCATS>
<RAPPORTEUR>M. Timothée Paris</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:382443.20150727</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. D...C...et Mme A...B...ont demandé au tribunal administratif de la Nouvelle-Calédonie d'annuler, respectivement, d'une part, l'arrêté du gouvernement de la Nouvelle-Calédonie du 21 juin 2011 portant ouverture des concours externe et interne pour le recrutement d'administrateurs de la Nouvelle-Calédonie et l'arrêté du même jour modifiant l'arrêté du 2 mars 2011 portant ouverture de la qualification pour l'accès au grade de technicien supérieur des études et de l'exploitation de l'aviation civile exceptionnel et, d'autre part, l'arrêté du gouvernement de la Nouvelle-Calédonie du 21 juin 2011 relatif à la nomination du chef de service de la météorologie de la Nouvelle-Calédonie, ainsi que, par voie de conséquence, l'arrêté du président du gouvernement de la Nouvelle-Calédonie du même jour portant délégation de signature à ce chef de service. Par un jugement n° 11343, 11344, 11345 du 8 décembre 2011, le tribunal a rejeté ces demandes. <br/>
<br/>
              Par un arrêt n° 12PA01175 du 31 mars 2014, la cour administrative d'appel de Paris a fait droit à l'appel formé par M. C...et Mme B...en annulant ce jugement et les arrêtés attaqués.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire enregistrés les 9 juillet et 3 septembre 2014, au secrétariat du contentieux du Conseil d'Etat, le gouvernement de la Nouvelle-Calédonie demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de M. C...et Mme B...;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la loi organique n° 99-209 du 19 mars 1999 ; <br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Timothée Paris, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Odent, Poulet, avocat du Gouvernement de la Nouvelle-Calédonie  ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. C...et MmeB..., ont, en qualité de membres du gouvernement de la Nouvelle-Calédonie, demandé au tribunal administratif de la Nouvelle-Calédonie, l'annulation de trois arrêtés de ce gouvernement du 21 juin 2011 au motif que ceux-ci étaient entrés en vigueur sans qu'ils les aient contresignés, alors qu'ils étaient chargés d'en contrôler l'exécution, en méconnaissance du troisième alinéa de l'article 128 de la loi organique du 19 mars 1999 relative à la Nouvelle-Calédonie ; que par un jugement du 8 décembre 2011, le tribunal a rejeté ces demandes ; que le gouvernement de la Nouvelle-Calédonie se pourvoit en cassation contre l'arrêt du 31 mars 2014 par lequel la cour administrative d'appel de Paris a annulé ce jugement et ces arrêtés ;<br/>
<br/>
              2. Considérant qu'il résulte de l'ensemble des dispositions de la loi organique du 19 mars 1999 relatives au gouvernement de la Nouvelle Calédonie et, en particulier, de celles de son article 128 aux termes duquel " Le gouvernement est chargé collégialement et solidairement des affaires de sa compétence " que les membres de ce gouvernement n'ont pas, à ce titre, quels que soient les moyens qu'ils invoquent, intérêt leur donnant qualité pour demander l'annulation d'arrêtés du gouvernement auquel ils appartiennent ; qu'ainsi, ni M.C..., ni MmeB..., n'étaient recevables à saisir le tribunal administratif de la Nouvelle-Calédonie d'une demande tendant à l'annulation des arrêtés du 21 juin 2011 ; qu'il résulte de ce qui précède que, sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, le gouvernement de la Nouvelle Calédonie est fondé à demander l'annulation de l'arrêt attaqué ;<br/>
<br/>
              3. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède que M. C...et Mme B...dont, ainsi qu'il vient d'être dit, les demandes étaient irrecevables, ne sont pas fondés à se plaindre de ce que, par le jugement attaqué, le tribunal administratif de la Nouvelle-Calédonie les a rejetées ;  <br/>
<br/>
              5. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de M. C...et de Mme B...la somme que demande le gouvernement de la Nouvelle-Calédonie au titre de l'article L. 761-1 du code de justice administrative ; que ces mêmes dispositions font obstacle à ce qu'une somme soit mise à la charge de la Nouvelle-Calédonie au titre des frais exposés par M. C...et Mme B...et non compris dans les dépens ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Paris du 31 mars 2014 est annulé. <br/>
Article 2 : L'appel de M. C...et de Mme B...devant la cour administrative d'appel de Paris est rejeté. <br/>
Article 3 : Les conclusions présentées par le gouvernement de la Nouvelle-Calédonie devant la cour administrative d'appel de Paris au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 4 : La présente décision sera notifiée au gouvernement de la Nouvelle-Calédonie, à M. D... C...et à Mme A...B.... <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">46-01-02-01 OUTRE-MER. DROIT APPLICABLE. STATUTS. NOUVELLE-CALÉDONIE. - MEMBRES DU GOUVERNEMENT DE LA NOUVELLE-CALÉDONIE - INTÉRÊT, À CE TITRE, POUR DEMANDER L'ANNULATION D'ARRÊTÉS DU GOUVERNEMENT AUQUEL ILS APPARTIENNENT - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">46-01-08 OUTRE-MER. DROIT APPLICABLE. ORGANISATION JUDICIAIRE ET PARTICULARITÉS CONTENTIEUSES. - MEMBRES DU GOUVERNEMENT DE LA NOUVELLE-CALÉDONIE - INTÉRÊT, À CE TITRE, POUR DEMANDER L'ANNULATION D'ARRÊTÉS DU GOUVERNEMENT AUQUEL ILS APPARTIENNENT - ABSENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-01-04-01-01 PROCÉDURE. INTRODUCTION DE L'INSTANCE. INTÉRÊT POUR AGIR. ABSENCE D'INTÉRÊT. CATÉGORIES DE REQUÉRANTS. - MEMBRE DU GOUVERNEMENT DE LA NOUVELLE-CALÉDONIE ATTAQUANT, EN SE PRÉVALANT DE CETTE QUALITÉ, UN ARRÊTÉ DU GOUVERNEMENT AUQUEL IL APPARTIENT.
</SCT>
<ANA ID="9A"> 46-01-02-01 Il résulte de l'ensemble des dispositions de la loi organique  n° 99-209 du 19 mars 1999 relatives au gouvernement de la Nouvelle Calédonie et, en particulier, de celles de son article 128 aux termes duquel  Le gouvernement est chargé collégialement et solidairement des affaires de sa compétence  que les membres de ce gouvernement n'ont pas, à ce titre, quels que soient les moyens qu'ils invoquent, intérêt leur donnant qualité pour demander l'annulation d'arrêtés du gouvernement auquel ils appartiennent.</ANA>
<ANA ID="9B"> 46-01-08 Il résulte de l'ensemble des dispositions de la loi organique  n° 99-209 du 19 mars 1999 relatives au gouvernement de la Nouvelle Calédonie et, en particulier, de celles de son article 128 aux termes duquel  Le gouvernement est chargé collégialement et solidairement des affaires de sa compétence  que les membres de ce gouvernement n'ont pas, à ce titre, quels que soient les moyens qu'ils invoquent, intérêt leur donnant qualité pour demander l'annulation d'arrêtés du gouvernement auquel ils appartiennent.</ANA>
<ANA ID="9C"> 54-01-04-01-01 Il résulte de l'ensemble des dispositions de la loi organique  n° 99-209 du 19 mars 1999 relatives au gouvernement de la Nouvelle Calédonie et, en particulier, de celles de son article 128 aux termes duquel  Le gouvernement est chargé collégialement et solidairement des affaires de sa compétence  que les membres de ce gouvernement n'ont pas, à ce titre, quels que soient les moyens qu'ils invoquent, intérêt leur donnant qualité pour demander l'annulation d'arrêtés du gouvernement auquel ils appartiennent.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
