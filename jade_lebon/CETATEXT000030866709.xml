<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030866709</ID>
<ANCIEN_ID>JG_L_2015_07_000000382737</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/86/67/CETATEXT000030866709.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème / 9ème SSR, 10/07/2015, 382737</TITRE>
<DATE_DEC>2015-07-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>382737</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème / 9ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>Mme Isabelle Lemesle</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:382737.20150710</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B...A...a demandé au tribunal administratif d'Amiens de réformer les résultats des opérations électorales auxquelles il a été procédé les 23 mars et 30 mars 2014 en vue de la désignation des conseillers municipaux de la commune de Reuil-sur-Brêche (Oise), de sorte qu'elle soit déclarée élue à l'issue du premier tour de scrutin.<br/>
<br/>
              Par un jugement n° 1400989 du 12 juin 2014, le tribunal administratif d'Amiens a rejeté la protestation que lui avait présentée MmeA....<br/>
<br/>
              Par une requête et un nouveau mémoire, enregistrés les 17 juillet 2014 et 24 février 2015 au secrétariat du contentieux du Conseil d'Etat, Mme A...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) de faire droit à sa protestation et de la proclamer élue ;<br/>
<br/>
              3°) à titre subsidiaire, d'enjoindre au préfet de l'Oise et au tribunal administratif d'Amiens de communiquer le bulletin de vote litigieux ou, à défaut, de prescrire une enquête concernant la conservation des bulletins de vote et en particulier du bulletin litigieux.<br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu :<br/>
              - le code électoral ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Isabelle Lemesle, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de Mme B...A...;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'à l'issue du premier tour de scrutin qui s'est déroulé le 23 mars 2014 dans le bureau de vote unique de la commune de Reuil-sur-Brêche, qui compte moins de 1 000 habitants, ont été déclarés élus conseillers municipaux neuf candidats ayant obtenu au moins 81 voix, deux sièges restant à pourvoir au second tour ; que Mme B...A..., maire sortante, qui a rassemblé 80 voix au premier tour du scrutin, a demandé au tribunal administratif d'Amiens d'en réformer les résultats et de la déclarer élue, au motif qu'un bulletin de vote qui lui était favorable aurait, à tort, été regardé comme nul ; qu'elle fait appel du jugement par lequel le tribunal administratif d'Amiens a rejeté sa protestation ;<br/>
<br/>
              2. Considérant que, d'une part, aux termes de l'article L. 252 du code électoral : " Les membres des conseils municipaux des communes de moins de 1000 habitants sont élus au scrutin majoritaire " ; qu'aux termes de l'article L. 253 du même code : " Nul n'est élu au premier tour de scrutin s'il n'a réuni : / 1° La majorité absolue des suffrages exprimés ; / 2° Un nombre de suffrages égal au quart de celui des électeurs inscrits. / Au deuxième tour de scrutin, l'élection a lieu à la majorité relative, quel que soit le nombre des votants (...) " ; que, d'autre part, l'article L. 66 dispose que les bulletins qui n'entrent pas en compte dans le résultat du dépouillement ainsi que les enveloppes non réglementaires sont annexés au procès-verbal et contresignés par les membres du bureau, chacun de ces bulletins annexés devant porter mention des causes de l'annexion et que, si l'annexion n'a pas été faite, cette circonstance n'entraîne l'annulation des opérations qu'autant qu'il est établi qu'elle a eu pour but et pour conséquence de porter atteinte à la sincérité du scrutin ; <br/>
<br/>
              3. Considérant qu'une protestation tendant à l'annulation d'une élection, lorsqu'elle est fondée sur des griefs tirés de la validité d'un bulletin de vote, saisit le juge de la validité du bulletin qui est contestée devant lui et de tous ceux du même bureau qui figurent au dossier ou  dont il ordonne le versement au dossier ; qu'au terme de ses vérifications, le juge doit réviser les décomptes des voix et modifier, le cas échéant, les résultats de l'élection ; que, dans l'hypothèse où tous les bulletins en cause n'ont pu, faute d'avoir été conservés, être versés au dossier et où le juge se trouve ainsi dans l'impossibilité de déterminer avec certitude le nombre et la teneur des bulletins valables et, par suite, de procéder à la rectification des résultats, il doit procéder à des calculs hypothétiques afin de déterminer si le grief dont il est saisi conduit à remettre en cause les résultats proclamés ;<br/>
<br/>
              4. Considérant que le procès-verbal des opérations électorales qui se sont déroulées le 23 mars 2014 pour le premier tour de scrutin fait état de 168 votants et de " huit bulletins blancs " déclarés nuls, 160 suffrages ayant ainsi été regardés comme valablement exprimés ; qu'il résulte toutefois de l'instruction que sont annexés au procès-verbal, qui ne comporte ni observation, ni réclamation, douze bulletins dont un blanc, aucun n'étant contresigné, en méconnaissance des dispositions précitées de l'article L. 66 du code électoral ; que cette irrégularité place le juge de l'élection dans l'impossibilité de contrôler le bien-fondé de l'annulation, non seulement du bulletin litigieux dont la description qu'en font Mme A...et ses colistiers ne correspond d'ailleurs à aucun des bulletins annexés, mais aussi des autres bulletins non décomptés mentionnés au procès-verbal ;<br/>
<br/>
              5. Considérant que, dans ces conditions, il y a lieu d'ajouter hypothétiquement aux 160 suffrages exprimés, non le seul bulletin mentionné dans la protestation comme l'ont fait à tort les premiers juges, mais les huit bulletins déclarés nuls, ce qui porte ce nombre à 168 et la majorité absolue de 81 à 85 ; qu'à supposer même que le bulletin dont Mme A...soutient qu'il a été déclaré nul à tort aurait été valablement exprimé en sa faveur, ce qui porterait le nombre de voix qu'elle a obtenues à 81, celle-ci ne serait pas davantage élue au premier tour ; <br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que, sans qu'il soit besoin de prescrire les mesures d'instruction demandées, Mme A...n'est pas fondée à se plaindre de ce que, par le jugement attaqué, le tribunal administratif a rejeté sa protestation ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de Mme A...est rejetée.<br/>
Article 2 : La présence décision sera notifiée à Mme B...A...et au ministre de l'intérieur. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">28-08-05-02 ÉLECTIONS ET RÉFÉRENDUM. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. POUVOIRS DU JUGE. GRIEFS. - GRIEF TIRÉ DE LA VALIDITÉ D'UN BULLETIN DE VOTE - PORTÉE - OBLIGATION POUR LE JUGE DE VÉRIFIER LA VALIDITÉ DU BULLETIN QUI EST CONTESTÉE DEVANT LUI ET DE TOUS CEUX DU MÊME BUREAU QUI FIGURENT AU DOSSIER OU DONT IL ORDONNE LE VERSEMENT - EXISTENCE - CAS OÙ CES BULLETINS N'ONT PAS ÉTÉ CONSERVÉS - OBLIGATION POUR LE JUGE DE PROCÉDER À DES CALCULS HYPOTHÉTIQUES [RJ1] - APPLICATION DE CES PRINCIPES.
</SCT>
<ANA ID="9A"> 28-08-05-02 Une protestation tendant à l'annulation d'une élection, lorsqu'elle est fondée sur des griefs tirés de la validité d'un bulletin de vote, saisit le juge de la validité du bulletin qui est contestée devant lui et de tous ceux du même bureau qui figurent au dossier ou dont il ordonne le versement au dossier. Au terme de ses vérifications, le juge doit réviser les décomptes des voix et modifier, le cas échéant, les résultats de l'élection. Dans l'hypothèse où tous les bulletins en cause n'ont pu, faute d'avoir été conservés, être versés au dossier et où le juge se trouve ainsi dans l'impossibilité de déterminer avec certitude le nombre et la teneur des bulletins valables et, par suite, de procéder à la rectification des résultats, il doit procéder à des calculs hypothétiques afin de déterminer si le grief dont il est saisi conduit à remettre en cause les résultats proclamés.,,,En l'espèce, juge de l'élection saisi par un protestataire de la contestation de la validité d'un bulletin, les bulletins annexés au procès-verbal ne lui permettant pas de contrôler le bien-fondé de l'annulation, non seulement du bulletin litigieux mais aussi des autres bulletins non décomptés mentionnés au procès-verbal. Dans ces conditions, il y a lieu d'ajouter hypothétiquement aux suffrages exprimés, non le seul bulletin mentionné dans la protestation, mais tous les bulletins déclarés nuls dans le procès verbal, ce qui modifie à la fois le nombre des suffrages exprimés et la majorité absolue (permettant l'élection au premier tour).</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Section, 25 janvier 1999, Elections régionales de Provence-Alpes-Côte-d'Azur (Département des Bouches-du-Rhône), n° 195139, p. 4 ; CE, Section, 1er décembre 1978, Elections municipales de Thiais, n°08775, p. 481.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
