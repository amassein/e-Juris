<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028583865</ID>
<ANCIEN_ID>JG_L_2014_02_000000356657</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/58/38/CETATEXT000028583865.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème et 8ème sous-sections réunies, 10/02/2014, 356657</TITRE>
<DATE_DEC>2014-02-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>356657</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème et 8ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>M. Fabrice Benkimoun</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:356657.20140210</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 10 février et 11 mai 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. B... A..., demeurant... ; M. A...demande au Conseil d'Etat :<br/>
<br/>
               1°) d'annuler l'arrêt n° 09NT02841-09NT03060 du 9 décembre 2011 par lequel la cour administrative d'appel de Nantes a rejeté sa requête tendant à l'annulation du jugement n° 0500191 du 7 octobre 2009 du tribunal administratif de Nantes en tant qu'il n'a pas entièrement fait droit à sa demande tendant à la condamnation de l'Etat à lui verser les sommes de 16 145,32 euros à titre principal, 41 925,56 euros au titre des intérêts légaux et de 35 239,81 euros au titre de la capitalisation des intérêts et, sur recours incident du ministre de l'intérieur, a ramené le montant de l'indemnité que l'Etat a été condamné à lui verser de 17 398,09 euros à 5 000 euros ;<br/>
<br/>
               2°) réglant l'affaire au fond, de faire droit à son appel et de rejeter l'appel incident du ministre de l'intérieur ;<br/>
<br/>
               3°) de mettre à la charge de l'Etat le versement de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 17 janvier 2014, présentée pour M. A... ;<br/>
<br/>
              Vu le code civil, notamment son article 1351 ;<br/>
<br/>
              Vu la loi n° 84-16 du 11 janvier 1984 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Fabrice Benkimoun, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de M. A...;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par un jugement du 6 novembre 2003, devenu définitif, le tribunal administratif de Nantes a statué sur la demande de M.A..., ancien agent contractuel, tendant à la condamnation de l'Etat à lui verser un rappel de rémunération, dont le Conseil d'Etat, statuant au contentieux, avait jugé, par une décision n° 121728 du 8 juillet 1992, qu'il lui était dû dans son principe sur le fondement des stipulations de son contrat, ainsi que les intérêts moratoires applicables à ce rappel et le montant des intérêts capitalisés ; que M. A...a présenté une nouvelle demande tendant à l'annulation des décisions du ministre de l'intérieur des 30 août et 12 novembre 2004 refusant de lui accorder un rappel supplémentaire de rémunération calculé conformément à la circulaire BAL 173 du 15 juillet 2003 prise par ce ministre en vue d'un règlement transactionnel des contentieux en cours liés à l'attribution de la part intégrée de l'indemnité de résidence aux agents non titulaires des services d'études ; que, par l'arrêt attaqué, la cour administrative d'appel de Nantes a rejeté l'appel de M. A...contre le jugement du 7 octobre 2009 du tribunal administratif de Nantes qui n'a pas entièrement fait droit à sa demande et, sur recours incident du ministre de l'intérieur, a ramené le montant de l'indemnité que l'Etat a été condamné à lui verser à 5 000 euros ;<br/>
<br/>
              2. Considérant, en premier lieu, qu'après avoir porté sur les faits une appréciation souveraine, exempte de dénaturation, en constatant l'identité de parties et d'objet entre la demande sur laquelle le tribunal administratif avait statué par son jugement du 6 novembre 2003 et en estimant que la nouvelle demande de M.A...  avait le même objet que la précédente, la cour administrative d'appel de Nantes n'a, alors même que l'intéressé se prévalait de la circulaire du 15 juillet 2003, commis aucune erreur de droit en jugeant que la seconde demande, également fondée sur la responsabilité contractuelle de l'Etat, reposait sur la même cause juridique que la première et que, dès lors, l'autorité de la chose jugée attachée au jugement du tribunal du 6 novembre 2003 s'opposait à ce qu'il soit fait droit à la nouvelle demande présentée par M. A...; que l'autorité de la chose jugée faisant ainsi obstacle à ce que la nouvelle demande du requérant soit examinée sur le fond, la cour n'avait pas à répondre à un moyen tiré de la violation du principe d'égalité entre agents publics ;<br/>
<br/>
              3. Considérant, en deuxième lieu, qu'en estimant que M. A... n'avait pas présenté de conclusions tendant à la condamnation de l'Etat à lui verser une indemnité en raison de la faute qu'aurait commise l'administration en ne lui révélant pas, avant la fin de l'instance ayant donné lieu au jugement du 6 novembre 2003, le montant du rappel qu'il aurait pu réclamer en le calculant selon la circulaire du 15 juillet 2003, la cour administrative d'appel de Nantes ne s'est pas méprise sur la portée des conclusions du requérant ;<br/>
<br/>
              4. Considérant, en troisième lieu, qu'en jugeant qu'il ne résultait pas de l'instruction que les revenus de M. A...auraient été insuffisants pour lui permettre de faire face à ses dépenses courantes et qu'il aurait été contraint d'emprunter du seul fait du retard de l'administration, la cour administrative d'appel de Nantes s'est bornée, sans soulever aucun moyen d'office, à porter sur les faits, en particulier l'absence de relation de cause à effet entre le préjudice et la faute invoqués, une appréciation souveraine exempte de dénaturation ; que les moyens tirés de ce qu'elle aurait inexactement qualifié les faits et, en méconnaissant son office, commis une erreur de droit ne peuvent donc qu'être écartés ;<br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que M. A...n'est pas fondé à demander l'annulation de l'arrêt attaqué ; que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. A...est rejeté.<br/>
Article 2 : La présente décision sera notifiée à M. B...A...et au ministre de l'intérieur.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-13-03 FONCTIONNAIRES ET AGENTS PUBLICS. CONTENTIEUX DE LA FONCTION PUBLIQUE. CONTENTIEUX DE L'INDEMNITÉ. - DEMANDE D'UN ANCIEN AGENT CONTRACTUEL TENDANT À LA CONDAMNATION DE L'ETAT À LUI VERSER UN RAPPEL DE RÉMUNÉRATION SUR LE FONDEMENT DES STIPULATIONS DE SON CONTRAT - NOUVELLE DEMANDE TENDANT À L'ANNULATION DU REFUS DU MINISTRE DE LUI ACCORDER UN RAPPEL SUPPLÉMENTAIRE DE RÉMUNÉRATION CALCULÉ CONFORMÉMENT À UNE CIRCULAIRE PRISE EN VUE D'UN RÈGLEMENT TRANSACTIONNEL DES CONTENTIEUX EN COURS - IDENTITÉ DE CAUSE JURIDIQUE - EXISTENCE - CONSÉQUENCES.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-06-06-01-02 PROCÉDURE. JUGEMENTS. CHOSE JUGÉE. CHOSE JUGÉE PAR LA JURIDICTION ADMINISTRATIVE. EXISTENCE. - AUTORITÉ RELATIVE DE LA CHOSE JUGÉE - IDENTITÉ DE CAUSE JURIDIQUE - NOTION - INCLUSION - DEMANDE D'UN ANCIEN AGENT CONTRACTUEL TENDANT À LA CONDAMNATION DE L'ETAT À LUI VERSER UN RAPPEL DE RÉMUNÉRATION SUR LE FONDEMENT DES STIPULATIONS DE SON CONTRAT - NOUVELLE DEMANDE TENDANT À L'ANNULATION DU REFUS DU MINISTRE DE LUI ACCORDER UN RAPPEL SUPPLÉMENTAIRE DE RÉMUNÉRATION CALCULÉ CONFORMÉMENT À UNE CIRCULAIRE PRISE EN VUE D'UN RÈGLEMENT TRANSACTIONNEL DES CONTENTIEUX EN COURS.
</SCT>
<ANA ID="9A"> 36-13-03 Une demande d'un ancien agent contractuel tendant à l'annulation des décisions par lesquelles le ministre a refusé de lui accorder un rappel supplémentaire de rémunération calculé conformément à une circulaire prise par ce ministre en vue d'un règlement transactionnel des contentieux en cours liés à l'attribution de la part intégrée de l'indemnité de résidence à certains agents non titulaires, qui est fondée sur la responsabilité contractuelle de l'Etat, repose sur la même cause juridique que la première demande par laquelle l'intéressé avait demandé la condamnation de l'Etat à lui verser un rappel de rémunération sur le fondement des stipulations de son contrat. Par suite, les conditions d'identité de parties et d'identité d'objet étant par ailleurs remplies, l'autorité de la chose jugée attachée au jugement ayant statué sur la première demande fait obstacle à ce qu'il soit fait droit à la nouvelle demande du requérant.</ANA>
<ANA ID="9B"> 54-06-06-01-02 Une demande d'un ancien agent contractuel tendant à l'annulation des décisions par lesquelles le ministre a refusé de lui accorder un rappel supplémentaire de rémunération calculé conformément à une circulaire prise par ce ministre en vue d'un règlement transactionnel des contentieux en cours liés à l'attribution de la part intégrée de l'indemnité de résidence à certains agents non titulaires, qui est fondée sur la responsabilité contractuelle de l'Etat, repose sur la même cause juridique que la première demande par laquelle l'intéressé avait demandé la condamnation de l'Etat à lui verser un rappel de rémunération sur le fondement des stipulations de son contrat. Par suite, les conditions d'identité de parties et d'identité d'objet étant par ailleurs remplies, l'autorité de la chose jugée attachée au jugement ayant statué sur la première demande fait obstacle à ce qu'il soit fait droit à la nouvelle demande du requérant.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
