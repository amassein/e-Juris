<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037205160</ID>
<ANCIEN_ID>JG_L_2018_07_000000411206</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/20/51/CETATEXT000037205160.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 5ème chambres réunies, 09/07/2018, 411206</TITRE>
<DATE_DEC>2018-07-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>411206</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP POTIER DE LA VARDE, BUK LAMENT, ROBILLOT ; SCP DELAMARRE, JEHANNIN ; SCP FABIANI, LUC-THALER, PINATEL</AVOCATS>
<RAPPORTEUR>Mme Marie-Laure Denis</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Louis Dutheillet de Lamothe</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:411206.20180709</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. R...P..., Mme E...P..., M. O...F..., Mme Q...F..., M. J...L..., M. I...G..., Mme H...A..., épouseG..., M. S...-I...D..., M. B...K...et Mme N...C..., épouse K...ont demandé au tribunal administratif de Nice d'annuler pour excès de pouvoir l'arrêté du 26 janvier 2016 par lequel le maire de Nice (Alpes-Maritimes) a accordé un permis de construire à la société BNP Paribas immobilier et a retiré le refus opposé à sa demande le 22 septembre 2015. Par un jugement n° 1601303 du 6 avril 2017, le tribunal administratif de Nice a rejeté leur demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 6 juin et 6 septembre 2017 au secrétariat du contentieux du Conseil d'Etat, M. et MmeP..., M. et MmeF..., M.L..., M. D...et M. et Mme K...demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur demande ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Nice et de la société BNP Paribas Immobilier la somme de 1 000 euros au profit de chacun des requérants au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la construction et de l'habitation ;<br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie-Laure Denis, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Louis Dutheillet de Lamothe, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Fabiani, Luc-Thaler, Pinatel, avocat de M.P..., et autres, à la SCP Potier de la Varde, Buk Lament, Robillot, avocat de la commune de Nice et à la SCP Delamarre, Jéhannin, avocat de la société BNP Paribas immobilier.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. La Société BNP Paribas immobilier a déposé le 4 mai 2015 une demande de permis de construire portant sur la réalisation d'un ensemble immobilier, d'une maison individuelle et de quatre bâtiments de type R+2 comprenant 73 logements dont 18 logements sociaux ainsi qu'une piscine, pour une surface de plancher de 4 210 mètres carrés, sur un terrain situé 278 avenue de Fabron à Nice (Alpes-Maritimes). Le maire de Nice a délivré le permis sollicité par un arrêté du 26 janvier 2016 portant retrait de la décision de refus qu'il avait initialement opposée à la demande le 22 septembre 2015. M. et Mme P...et autres ont demandé l'annulation pour excès de pouvoir du permis ainsi accordé au tribunal administratif de Nice, qui a rejeté leur requête par un jugement du 6 avril 2017, objet du présent pourvoi.<br/>
<br/>
              2. En premier lieu, il ressort des pièces du dossier soumis aux juges du fond que l'argumentation tirée de ce que le projet de construction autorisé par le permis de construire attaqué prévoit une voie de desserte qui n'est pas la propriété exclusive du bénéficiaire du permis a été présentée dans les développements de la requête pour excès de pouvoir en justifiant la recevabilité. Le tribunal ayant rejeté les conclusions à fin d'annulation sans qu'il soit besoin de statuer sur les fins de non-recevoir, le moyen tiré de ce que le jugement attaqué serait insuffisamment motivé faute de répondre à cette argumentation ne peut qu'être écarté.<br/>
<br/>
              3. En deuxième lieu, aux termes de l'article UB 3 du règlement du plan local d'urbanisme de la ville de Nice : " Le terrain doit être desservi par des voies publiques ou privées dans des conditions répondant à 1'importance ou à la destination du bâtiment ou des aménagements envisagés. (...). Les accès ne doivent pas présenter de risque pour la sécurité des usagers des voies ou pour celles des personnes utilisant ces accès. / Cette sécurité doit être appréciée compte tenu, notamment, de la position des accès, de leur configuration ainsi que de la nature et de l'intensité du trafic. (...) ". Si les requérants soutenaient que les conditions d'accès et de desserte des constructions projetées étaient insuffisantes, ce qui avait motivé un premier avis défavorable de la métropole de Nice, c'est par une appréciation souveraine de pièces du dossier qui lui étaient soumis, qui n'est pas entachée de dénaturation, que le tribunal administratif a relevé, d'une part, que le chemin Gilbert Castel avait une largeur de plus de 4 mètres, qu'il permettait la circulation de deux véhicules et qu'en plus du projet, il ne desservait que quelques maisons individuelles, d'autre part, que la société pétitionnaire avait prévu de faire un pan coupé pour accéder à l'avenue de Fabron et de réaliser le cheminement piéton sur sa parcelle entre le chemin Gilbert Castel et l'arrêt du bus le plus proche, enfin, qu'il n'était pas contesté que l'avenue de Fabron avait une largeur comprise entre 7 et 8 mètres, pour en déduire que le maire n'avait pas commis d'erreur manifeste d'appréciation en estimant que les dispositions précitées de l'article UB 3 du règlement du plan local d'urbanisme n'étaient pas méconnues. <br/>
<br/>
              4. En troisième lieu, aux termes de l'article L. 111-7 du code de la construction et de l'habitation : " Les dispositions architecturales, les aménagements et équipements intérieurs et extérieurs des locaux d'habitation, qu'ils soient la propriété de personnes privées ou publiques, des établissements recevant du public, des installations ouvertes au public et des lieux de travail doivent être tels que ces locaux et installations soient accessibles à tous, et notamment aux personnes handicapées, quel que soit le type de handicap, notamment physique, sensoriel, cognitif, mental ou psychique, dans les cas et selon les conditions déterminés aux articles L. 111-7-1 à L. 111-7-11. Ces dispositions ne sont pas obligatoires pour les propriétaires construisant ou améliorant un logement pour leur propre usage ". Les articles L. 111-7-1, L. 111-7-2 et L. 111-7-3 du même code définissent les règles générales d'accessibilité applicables respectivement aux bâtiments ou parties de bâtiments nouveaux, aux bâtiments ou parties de bâtiments d'habitation existants et aux établissements recevant du public situés dans un cadre bâti existant. Aux termes de l'article L. 111-7-4 du même code : " Un décret en Conseil d'Etat définit les conditions dans lesquelles, à l'issue de l'achèvement des travaux prévus aux articles L. 111-7-1, L. 111-7-2 et L. 111-7-3 et soumis à permis de construire, le maître d'ouvrage doit fournir à l'autorité qui a délivré ce permis un document attestant de la prise en compte des règles concernant l'accessibilité. Cette attestation est établie par un contrôleur technique visé à l'article L. 111-23 ou par une personne physique ou morale satisfaisant à des critères de compétence et d'indépendance déterminés par ce même décret. Ces dispositions ne s'appliquent pas pour les propriétaires construisant ou améliorant leur logement pour leur propre usage ". Aux termes de l'article L. 111-8 du même code : " Les travaux qui conduisent à la création, l'aménagement ou la modification d'un établissement recevant du public ne peuvent être exécutés qu'après autorisation délivrée par l'autorité administrative qui vérifie leur conformité aux règles prévues aux articles L. 111-7, L. 123-1 et L. 123-2. Lorsque ces travaux sont soumis à permis de construire, celui ci tient lieu de cette autorisation dès lors que sa délivrance a fait l'objet d'un accord de l'autorité administrative compétente mentionnée à l'alinéa précédent. Toutefois, lorsque l'aménagement intérieur d'un établissement recevant du public ou d'une partie de celui-ci n'est pas connu lors du dépôt d'une demande de permis de construire, le permis de construire indique qu'une autorisation complémentaire au titre de l'article L. 111-8 du code de la construction et de l'habitation devra être demandée et obtenue en ce qui concerne l'aménagement intérieur du bâtiment ou de la partie de bâtiment concernée avant son ouverture au public ".<br/>
<br/>
              5. Il résulte de ces dispositions qu'à l'exception des travaux qui conduisent à la création, l'aménagement ou la modification d'un établissement recevant du public, qui sont soumis au régime d'autorisation préalable prévu par l'article L. 111-8 du code de la construction et de l'habitation, les travaux prévus aux articles L. 111-7 et suivants du même code ne font pas l'objet d'une autorisation préalable, notamment à l'occasion de la délivrance du permis de construire. Le tribunal administratif n'a, par suite, pas commis d'erreur de droit en estimant que, dès lors que les travaux autorisés par le permis de construire attaqué ne conduisaient pas à la création d'un établissement recevant du public, le moyen tiré de la méconnaissance des dispositions des articles L. 111-7 et R. 111-18 du code de la construction et de l'habitation était sans incidence sur la légalité du permis de construire attaqué.<br/>
<br/>
              6. Il résulte de tout ce qui précède que les requérants ne sont pas fondés à demander l'annulation du jugement attaqué.<br/>
<br/>
              Sur les frais liés au litige :<br/>
<br/>
              7. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la ville de Nice et de la société BNP Paribas Immobilier qui ne sont pas, dans la présente instance, les parties perdantes. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de M. et MmeP..., de M. et MmeF..., de M.L..., de M. D...et de M. et Mme K...la somme de 2 000 euros qu'ils verseront solidairement à la ville de Nice ainsi que la somme de 2 000 euros qu'ils verseront solidairement à la société BNP Paribas Immobilier, au titre de ces mêmes dispositions.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. et MmeP..., de M. et MmeF..., de M.L..., de M. D..., de M. et Mme K...est rejeté.<br/>
<br/>
Article 2 : M. et MmeP..., M. et MmeF..., M.L..., M. D...et M. et Mme K...verseront solidairement la somme de 2 000 euros à la ville de Nice et la somme de 2 000 euros à la société BNP Paribas Immobilier, au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée à M. R...P..., premier dénommé, pour l'ensemble des requérants, à la ville de Nice et à la Société BNP Paribas Immobilier Résidentiel.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">38-01-02 LOGEMENT. RÈGLES DE CONSTRUCTION, DE SÉCURITÉ ET DE SALUBRITÉ DES IMMEUBLES. - CONTRÔLE PRÉALABLE, NOTAMMENT À L'OCCASION DE LA DÉLIVRANCE D'UN PERMIS DE CONSTRUIRE, DU RESPECT DES RÈGLES D'ACCESSIBILITÉ PRÉVUES PAR LE CCH - 1) TRAVAUX CONDUISANT À LA CRÉATION, L'AMÉNAGEMENT OU LA MODIFICATION D'UN ERP - EXISTENCE - 2) AUTRES TRAVAUX PRÉVUS AUX ARTICLES L. 111-7 ET SUIVANTS DU CCH - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">68-03-03-01-05 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PERMIS DE CONSTRUIRE. LÉGALITÉ INTERNE DU PERMIS DE CONSTRUIRE. LÉGALITÉ AU REGARD DE LA RÉGLEMENTATION NATIONALE. DIVERSES DISPOSITIONS LÉGISLATIVES OU RÉGLEMENTAIRES. - CONTRÔLE DU RESPECT DES RÈGLES D'ACCESSIBILITÉ PRÉVUES PAR LE CCH - 1) PRINCIPE - A) TRAVAUX CONDUISANT À LA CRÉATION, L'AMÉNAGEMENT OU LA MODIFICATION D'UN ERP - EXISTENCE - B) AUTRES TRAVAUX PRÉVUS AUX ARTICLES L. 111-7 ET SUIVANTS DU CCH - ABSENCE - 2) CONSÉQUENCE - MOYEN TIRÉ DE LA MÉCONNAISSANCE DES ARTICLES L. 111-7 ET R. 111-18 DU CCH - MOYEN SANS INCIDENCE SUR LA LÉGALITÉ DU PERMIS DE CONSTRUIRE, DÈS LORS QUE LES TRAVAUX AUTORISÉS NE CONDUISENT PAS À LA CRÉATION D'UN ERP.
</SCT>
<ANA ID="9A"> 38-01-02 A l'exception des travaux qui conduisent à la création, l'aménagement ou la modification d'un établissement recevant du public (ERP), qui sont soumis au régime d'autorisation préalable prévu par l'article L. 111-8 du code de la construction et de l'habitation (CCH), les travaux prévus aux articles L. 111-7 et suivants du même code ne font pas l'objet d'une autorisation préalable, notamment à l'occasion de la délivrance d'un permis de construire.</ANA>
<ANA ID="9B"> 68-03-03-01-05 1) A l'exception des travaux qui conduisent à la création, l'aménagement ou la modification d'un établissement recevant du public (ERP), qui sont soumis au régime d'autorisation préalable prévu par l'article L. 111-8 du code de la construction et de l'habitation (CCH), les travaux prévus aux articles L. 111-7 et suivants du même code ne font pas l'objet d'une autorisation préalable, notamment à l'occasion de la délivrance du permis de construire.... ...2) Dès lors que les travaux autorisés par le permis de construire ne conduisent pas à la création d'un ERP, le moyen tiré de la méconnaissance des dispositions des articles L. 111-7 et R. 111-18 du CCH est sans incidence sur la légalité du permis de construire.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
