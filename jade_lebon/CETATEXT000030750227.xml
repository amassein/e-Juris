<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030750227</ID>
<ANCIEN_ID>JG_L_2015_06_000000375853</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/75/02/CETATEXT000030750227.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème - 10ème SSR, 17/06/2015, 375853, Publié au recueil Lebon</TITRE>
<DATE_DEC>2015-06-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>375853</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème - 10ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MONOD, COLIN, STOCLET</AVOCATS>
<RAPPORTEUR>M. Jean-Luc Matt</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Aladjidi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2015:375853.20150617</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              1° Sous le n° 375853, par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 27 février 2014, 26 mai 2014 et 11 mai 2015 au secrétariat du contentieux du Conseil d'Etat, le syndicat national des industries des peintures, enduits et vernis (SIPEV) et l'association française des industries, colles, adhésifs et mastics (AFICAM) demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2013-1264 du 23 décembre 2013 relatif à la déclaration environnementale de certains produits de construction destinés à un usage dans les ouvrages de bâtiment ;<br/>
<br/>
              2°) de mettre à la charge de l'État la somme de 10 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              2° Sous le n° 375866, par une requête sommaire, deux mémoires complémentaires et un mémoire en réplique, enregistrés les 27 février 2014, 26 mai 2014, 5 décembre 2014 et 11 mai 2015 au secrétariat du contentieux du Conseil d'Etat, les mêmes requérants demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté du 23 décembre 2013 relatif à la déclaration environnementale des produits de construction et de décoration destinés à un usage dans les ouvrages de bâtiment ;<br/>
<br/>
              2°) de mettre à la charge de l'État la somme de 10 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              3° Sous le n° 384705, par une requête sommaire et un mémoire complémentaire, enregistrés les 23 septembre et 5 décembre 2014 au secrétariat du contentieux du Conseil d'Etat, les mêmes requérants demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté du 11 juillet 2014 portant création d'un traitement de données à caractère personnel relatif à la déclaration environnementale des produits de construction et de décoration destinés à un usage dans les ouvrages de bâtiment dénommé " déclaration environnementale " ;<br/>
<br/>
              2°) de mettre à la charge de l'État la somme de 3 000 euros à verser à chacun d'entre eux au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule ; <br/>
              - le code général des collectivités territoriales ;<br/>
              - le code de la consommation ;<br/>
              - le code de l'environnement ;<br/>
              - la loi n° 78-17 du 6 janvier 1978 ;<br/>
              - la loi n° 2010-788 du 12 juillet 2010 ;<br/>
              - la loi n° 2012-1460 du 27 décembre 2012 ;<br/>
              - l'ordonnance n° 2013-714 du 5 août 2013 ;<br/>
              - le décret n° 2005-850 du 27 juillet 2005 ;<br/>
              - la décision n° 375853 du 23 juillet 2014 par laquelle le Conseil d'Etat statuant au contentieux n'a pas renvoyé au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par les requérants ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Luc Matt, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Frédéric Aladjidi, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Monod, Colin, Stoclet, avocat du syndicat national des industries des peintures, enduits et vernis et de l'association française des industries, colles, adhésifs et mastics ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 22 mai 2015, présentée pour le SIPEV et l'AFICAM ;<br/>
<br/>
<br/>
<br/>1. Considérant que les requêtes visées ci-dessus présentent à juger des questions semblables ; qu'il y a lieu de les joindre pour statuer par une seule décision ;<br/>
<br/>
              Sur les conclusions tendant à l'annulation intégrale du décret du 23 décembre 2013 :<br/>
<br/>
              2. Considérant que les dispositions du décret attaqué s'appliquent à la mise sur le marché des produits de construction et de décoration ainsi que des équipements électriques, électroniques et de génie climatique destinés à la vente aux consommateurs ; que les requérants regroupent seulement des fabricants de produits de construction et de décoration ; qu'ils n'ont, dès lors, pas d'intérêt leur donnant qualité pour agir contre le décret en tant qu'il s'applique aux équipements électriques, électroniques et de génie climatique ; que leurs conclusions doivent être regardées comme dirigées contre le décret en tant seulement qu'il s'applique aux produits de construction et de décoration ; <br/>
<br/>
              3. Considérant qu'aux termes de l'article L. 214-1 du code de la consommation, dans sa rédaction issue de la loi du 12 juillet 2010 portant engagement national pour l'environnement : " Il sera statué par des décrets en Conseil d'Etat sur les mesures à prendre pour assurer l'exécution des chapitres II à VI du présent titre, notamment en ce qui concerne : / (...) 10° Les exigences de précision, de vérification et de prise en compte des éléments significatifs du cycle de vie des produits dans l'élaboration des allégations à caractère environnemental ou utilisant les termes de développement durable ou ses synonymes, lorsque ces allégations sont présentées sur les produits destinés à la vente aux consommateurs ou accompagnent leur commercialisation sous forme de mentions sur les emballages, les publications, la publicité, la télémercatique ou d'insertions sur supports numériques ou électroniques. " ; <br/>
<br/>
              4. Considérant qu'aux termes de l'article R. 214-27 du même code, créé par le décret attaqué : " Le responsable de la mise sur le marché de produits comportant des allégations à caractère environnemental ou utilisant les termes de développement durable ou ses synonymes, dans les conditions définies à l'article L. 214-1 (10°), établit une déclaration environnementale de l'ensemble des aspects environnementaux du produit conforme au programme de déclarations environnementales ou à un programme équivalent. Les modalités de mise en oeuvre de cette déclaration environnementale, et notamment la liste des indicateurs et les méthodes de calcul associées, sont précisées par arrêté des ministres chargés de la construction et du logement " ; que l'article R. 214-25 du même code, également créé par le décret attaqué, définit la déclaration environnementale comme une " déclaration indiquant les aspects environnementaux d'un produit ou d'un service fournissant des données environnementales quantifiées à l'aide de paramètres prédéterminés et, s'il y a lieu, complétés par d'autres informations environnementales " et le programme de déclarations environnementales comme un " programme volontaire destiné au développement et à l'utilisation des déclarations environnementales fondé sur un ensemble de règles de fonctionnement " ;<br/>
<br/>
              En ce qui concerne la légalité externe :<br/>
<br/>
              5. Considérant, en premier lieu, que la loi a précisé l'objet et le champ de l'obligation pesant sur les opérateurs économiques concernés, en vue de garantir la sincérité des mentions à caractère environnemental que ceux-ci peuvent décider d'apposer sur les produits destinés à la vente aux consommateurs ; que la déclaration environnementale prévue par le décret attaqué n'est obligatoire que pour les entreprises qui choisissent d'utiliser de telles mentions et, comme le précise l'article R 214-29 du code de la consommation, n'est pas requise lorsque les produits concernés font l'objet d'une certification relative à des caractéristiques environnementales délivrée par un organisme accrédité à cet effet ; que, dès lors, en imposant, dans les termes cités au point 4 ci-dessus, l'établissement d'une déclaration environnementale et en prévoyant la prise en compte, conformément au 10° de l'article L. 214-1 du code de la consommation, des éléments significatifs du cycle de vie des produits dans d'élaboration des allégations à caractère environnemental, le décret attaqué n'a pas excédé l'habilitation donnée par la loi ; que, par suite, le moyen tiré de ce qu'il a été pris incompétemment par le Premier ministre doit être écarté ; <br/>
<br/>
              6. Considérant, en deuxième lieu, qu'aux termes de l'article L. 120-1 du code de l'environnement, dans sa rédaction issue de l'ordonnance du 5 août 2013 relative à la mise en oeuvre du principe de participation du public défini à l'article 7 de la Charte de l'environnement : " I. - Le présent article définit les conditions et limites dans lesquelles le principe de participation du public, prévu à l'article 7 de la Charte de l'environnement, est applicable aux décisions, autres que les décisions individuelles, des autorités publiques ayant une incidence sur l'environnement lorsque celles-ci ne sont pas soumises, par les dispositions législatives qui leur sont applicables, à une procédure particulière organisant la participation du public à leur élaboration. / II. - Sous réserve des dispositions de l'article L. 120-2, le projet d'une décision mentionnée au I, accompagné d'une note de présentation précisant notamment le contexte et les objectifs de ce projet, est mis à disposition du public par voie électronique et, sur demande présentée dans des conditions prévues par décret, mis en consultation sur support papier dans les préfectures et les sous-préfectures en ce qui concerne les décisions des autorités de l'État, y compris les autorités administratives indépendantes, et des établissements publics de l'État, ou au siège de l'autorité en ce qui concerne les décisions des autres autorités. (...) / Les observations du public, déposées par voie électronique ou postale, doivent parvenir à l'autorité administrative concernée dans un délai qui ne peut être inférieur à vingt et un jours à compter de la mise à disposition prévue au même premier alinéa. / Pour les décisions des autorités de l'Etat, y compris les autorités administratives indépendantes, et des établissements publics de l'Etat, au terme de la période d'expérimentation prévue à l'article 3 de la loi n° 2012-1460 du 27 décembre 2012 relative à la mise en oeuvre du principe de participation du public défini à l'article 7 de la Charte de l'environnement, les observations déposées sur un projet de décision sont accessibles par voie électronique dans les mêmes conditions que le projet de décision. / Le projet de décision ne peut être définitivement adopté avant l'expiration d'un délai permettant la prise en considération des observations déposées par le public et la rédaction d'une synthèse de ces observations. Sauf en cas d'absence d'observations, ce délai ne peut être inférieur à quatre jours à compter de la date de la clôture de la consultation. (...) " ; <br/>
<br/>
              7. Considérant qu'il ressort des pièces du dossier que le projet de décret a été publié selon les modalités prévues par les dispositions précitées du II de l'article L. 120-1 du code de l'environnement en vue de permettre au public de formuler des observations ; que la consultation publique ayant eu lieu entre le 17 avril et le 10 mai 2013 et le décret n'ayant été signé que le 23 décembre 2013, le délai minimum de quatre jours entre la date de clôture de la consultation et la signature du décret attaqué a été respecté ; que la période d'expérimentation prévue par l'article 3 de la loi du 27 décembre 2012 relative à la mise en oeuvre du principe de participation du public défini à l'article 7 de la Charte de l'environnement n'étant pas expirée à la date de signature du décret attaqué, les observations déposées sur le projet de décret n'avaient pas à être rendues accessibles au public par voie électronique dans les mêmes conditions que le projet de décret ; que, dès lors, le moyen tiré de ce que la consultation du public aurait été entachée d'irrégularité, dès lors que seule une synthèse des observations du public sur le projet de décret a été publiée, doit être écarté ;<br/>
<br/>
              8. Considérant, en troisième lieu, que si les dispositions de l'article L. 120-1 du code de l'environnement impliquent que les projets d'acte réglementaire de l'État ayant une incidence directe et significative sur l'environnement fassent l'objet, soit d'une publication préalable permettant au public de formuler des observations, soit d'une publication avant la saisine d'un organisme consultatif comportant des représentants des catégories de personnes concernées, elles n'imposent pas de procéder à une nouvelle publication pour recueillir des observations du public sur les modifications qui sont ultérieurement apportées au projet de décision, au cours de son élaboration, dès lors que celles-ci n'ont pas pour effet de dénaturer le projet sur lequel ont été initialement recueillies les observations du public ;<br/>
<br/>
              9. Considérant que si les requérants soutiennent que le champ d'application du projet de décret soumis à consultation était limité aux produits de décoration assimilables à des produits de construction, alors que le décret attaqué s'applique aux produits utilisés pour les revêtements des murs, sols et plafonds, cette différence de rédaction résulte d'une erreur matérielle, la référence aux produits de construction ayant été reproduite à tort à partir d'une autre partie du texte, qui a été corrigée à un stade ultérieur de l'élaboration du projet de décret ; qu'il ne pouvait, toutefois, y avoir aucun doute sur le champ d'application du projet de décret soumis à consultation, dès lors que le projet d'arrêté pris pour son application, qui était soumis simultanément à la consultation, donnait, dans son annexe I, la liste des différents revêtements des sols et murs, ainsi que des peintures et produits de décoration concernés ; que, dès lors, cette rectification d'une erreur matérielle ne peut être regardée comme une dénaturation du projet sur lequel le public a été appelé à formuler ses observations ;  <br/>
<br/>
              10. Considérant, en quatrième lieu, qu'en tout état de cause et pour les mêmes motifs que ceux qui sont énoncés au point 9, le moyen tiré de ce que la consultation, facultative, de la commission consultative d'évaluation des normes serait entachée d'irrégularité pour avoir porté sur un projet de décret dont la version aurait été dénaturée dans le texte du décret publié, doit être écarté ;<br/>
<br/>
              11. Considérant, en cinquième lieu, que la circulaire du 17 février 2011 relative à la simplification des normes concernant les entreprises et les collectivités territoriales, adressée par le Premier ministre aux ministres, se borne à fixer des orientations pour l'organisation du travail gouvernemental ; que les requérants ne peuvent utilement invoquer sa méconnaissance à l'appui  de leur requête contre le décret attaqué ;<br/>
<br/>
              En ce qui concerne la légalité interne :<br/>
<br/>
              12. Considérant que les dispositions réglementaires attaquées, qui ne créent aucune obligation qui excèderait celles qu'autorise la loi, ne sauraient être regardées, comme le soutiennent les requérants, comme portant une atteinte disproportionnée à la liberté d'entreprendre garantie par l'article 4 de la Déclaration des droits de l'homme et du citoyen ;<br/>
<br/>
              Sur les conclusions tendant à l'annulation intégrale de l'arrêté du 23 décembre 2013 :<br/>
<br/>
              13. Considérant, en premier lieu, que, pour demander l'annulation de l'arrêté du 23 décembre 2013 pris pour l'application du décret du 23 décembre 2013, les requérants soulèvent, par la voie de l'exception, les mêmes moyens contre ce décret que ceux qui ont été examinés ci-dessus et qui doivent être écartés ;<br/>
<br/>
              14. Considérant, en deuxième lieu, que les requérants ne peuvent utilement soulever, contre l'arrêté attaqué, le moyen tiré de ce que le commissaire à la simplification n'aurait pas été mis en mesure de donner un avis, en méconnaissance de la circulaire du Premier ministre du 17 février 2011 ;<br/>
<br/>
              15. Considérant, en troisième lieu, que le moyen tiré de ce que l'arrêté attaqué méconnaîtrait les dispositions de l'article 27 de la loi du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés ne peut être utilement soulevé, dès lors que cet arrêté ne crée pas de traitement de données à caractère personnel ;<br/>
<br/>
              Sur le moyen relatif à la sécurité juridique :<br/>
<br/>
              16. Considérant que l'exercice du pouvoir réglementaire implique pour son détenteur la possibilité de modifier à tout moment les normes qu'il définit, sans que les personnes auxquelles sont, le cas échéant, imposées de nouvelles contraintes puissent invoquer un droit au maintien de la réglementation existante ; qu'en principe, les nouvelles normes ainsi édictées ont vocation à s'appliquer immédiatement, dans le respect des exigences attachées au principe de non-rétroactivité des actes administratifs ; que, toutefois, il incombe à l'autorité investie du pouvoir réglementaire, agissant dans les limites de sa compétence et dans le respect des règles qui s'imposent à elle, d'édicter, pour des motifs de sécurité juridique, les mesures transitoires qu'implique, s'il y a lieu, cette réglementation nouvelle ; qu'il en va ainsi lorsque l'application immédiate de celle-ci entraîne, au regard de l'objet et des effets de ses dispositions, une atteinte excessive aux intérêts publics ou privés en cause ; <br/>
<br/>
              17. Considérant qu'aux termes de l'article 2 du décret attaqué, publié au Journal officiel le 29 décembre 2013 : " Les dispositions du présent décret s'appliquent à compter du 1er janvier 2014 pour tous les produits de construction et de décoration au sens du présent décret. " ; que l'article 3 de l'arrêté attaqué, publié dans les mêmes conditions, fixe la liste des informations que doit contenir la déclaration environnementale des produits de construction et de décoration ; que, selon l'article 4 de l'arrêté attaqué, une déclaration simplifiée, contenant une liste réduite d'informations, peut être établie " par dérogation aux dispositions de l'article 3, jusqu'au 1er juillet 2014 " ; que l'entrée en vigueur du décret attaqué étant ainsi conditionnée à l'entrée en vigueur de l'arrêté, il convient d'apprécier le respect du principe de sécurité juridique en prenant en compte l'ensemble des dispositions du décret et de l'arrêté ;<br/>
<br/>
              18. Considérant que les ministres ne contestent pas que l'établissement de la déclaration environnementale, même simplifiée, nécessite la réalisation d'études techniques préalables, ni que les " fiches de déclarations environnementales et sanitaires " présentant un bilan environnemental et réalisées à titre volontaire par les entreprises concernées avant l'entrée en vigueur du décret attaqué, qui seules peuvent dispenser de telles études, n'existaient pas pour tous les produits de construction et de décoration ; qu'il incombait au pouvoir réglementaire, pour des motifs de sécurité juridique, de permettre aux entreprises qui commercialisaient les produits en cause avant l'entrée en vigueur du décret attaqué de disposer d'un délai raisonnable pour réaliser les études nécessaires ou, à défaut de supprimer les allégations figurant sur les produits, pour procéder à la certification de ceux-ci ; que si l'arrêté attaqué prévoit, ainsi qu'il a été dit, à titre dérogatoire, une liste réduite des indicateurs qui doivent figurer dans une déclaration simplifiée à compter du 1er janvier 2014 et une liste exhaustive d'informations exigées seulement à compter du 1er juillet 2014, les dispositions attaquées ne peuvent être regardées comme garantissant suffisamment la sécurité juridique des opérateurs économiques concernés ; qu'il appartient dès lors au juge de l'excès de pouvoir, statuant après l'entrée en vigueur de ces dispositions, de les annuler en tant qu'elles n'ont pas prévu un délai suffisant pour leur entrée en vigueur ; qu'en l'espèce, les requérants sont fondés à demander l'annulation du décret et de l'arrêté du 23 décembre 2013, en tant qu'ils n'ont pas différé de six mois l'obligation d'établir une déclaration environnementale simplifiée et d'un an celle d'établir une déclaration environnementale exhaustive ; <br/>
<br/>
              Sur les conclusions tendant à l'annulation de l'arrêté du 11 juillet 2014 :<br/>
<br/>
              19. Considérant, en premier lieu, que l'article R. 214-32 du code de la consommation, créé par le décret du 23 décembre 2013, prévoit que la déclaration environnementale doit être déposée sur un " site internet défini par arrêté des ministres chargés de la construction et du logement " ; que cette déclaration environnementale comportant, en vertu de l'arrêté du 23 décembre 2013, les coordonnées du déclarant, la mise en place de ce site internet nécessitait la création d'un traitement de données à caractère personnel mis en oeuvre par l'État aux fins de mettre à la disposition des usagers de l'administration un téléservice, autorisé par arrêté en vertu du 4° du II de l'article 27 de la loi du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés ; que les ministres de l'écologie, du développement durable et de l'énergie et du logement et de l'égalité des territoires étaient compétents pour signer conjointement l'arrêté du 11 juillet 2014 ;<br/>
<br/>
              20. Considérant, en deuxième lieu, qu'en vertu de l'article 1er du décret du 27 juillet 2005 relatif aux délégations de signature des membres du Gouvernement : " A compter du jour suivant la publication au Journal officiel de la République française de l'acte les nommant dans leurs fonctions ou à compter du jour où cet acte prend effet, si ce jour est postérieur, peuvent signer, au nom du ministre ou du secrétaire d'Etat et par délégation, l'ensemble des actes, à l'exception des décrets, relatifs aux affaires des services placés sous leur autorité : / 1° Les secrétaires généraux des ministères, les directeurs d'administration centrale (...) " ; que M.A..., nommé directeur de l'habitat, de l'urbanisme et des paysages par décret du Président de la République publié au Journal officiel le 4 juillet 2014, était compétent pour signer l'arrêté attaqué au nom des ministres de l'écologie, du développement durable et de l'énergie et du logement et de l'égalité des territoires ;<br/>
<br/>
              21. Considérant, en troisième lieu, que pour demander l'annulation de l'arrêté du 11 juillet 2014, les requérants soulèvent, par la voie de l'exception, les mêmes moyens contre ce décret que ceux qui ont été examinés ci-dessus et qui doivent être écartés ;   <br/>
<br/>
              22. Considérant, en quatrième lieu, que si l'arrêté autorisant un traitement de données à caractère personnel doit, en vertu du 4° du II de l'article 27 de la loi du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés, être pris après avis motivé et publié de la Commission nationale de l'informatique et des libertés, l'article 28 de la même loi prévoit que l'avis demandé à la commission sur un traitement qui n'est pas rendu dans un délai de deux mois à compter de la réception de la demande est réputé favorable ; que, dès lors que la saisine de la commission est intervenue le 10 mars 2014, son avis était réputé favorable à la date de signature de l'arrêté du 11 juillet 2014 ;<br/>
<br/>
              23. Considérant, en dernier lieu, qu'en vertu de l'article 32 de la loi du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés, il incombe au responsable d'un traitement de données à caractère personnel de fournir à toute personne concernée par l'inscription de données personnelles dans ce traitement, dès leur enregistrement, l'ensemble des informations prévues au I de cet article, y compris quand ces données personnelles ne sont pas recueillies auprès de la personne concernée elle-même ; que, toutefois, la méconnaissance de ces obligations par le responsable d'un traitement ne peut en tout état de cause être utilement invoquée à l'appui de conclusions dirigées contre l'acte portant création de ce traitement ;<br/>
<br/>
              24. Considérant qu'il résulte de ce qui précède, sans qu'il soit besoin de statuer sur la recevabilité de la requête n° 384705, que les requérants ne sont pas fondés à demander l'annulation de l'arrêté du 11 juillet 2014 ;<br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              25. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'État le versement d'une somme de 1 500 euros au syndicat national des industries des peintures enduits et vernis et de la même somme à l'association française des industries, colles, adhésifs et mastics au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le décret et l'arrêté du 23 décembre 2013 sont annulés en tant qu'ils n'ont pas différé de six mois l'obligation d'établir une déclaration environnementale simplifiée et d'un an celle d'établir une déclaration environnementale exhaustive, en ce qui concerne les produits de construction et de décoration.<br/>
Article 2 : L'État versera une somme de 1 500 euros au syndicat national des industries des peintures enduits et vernis et la même somme à l'association française des industries, colles, adhésifs et mastics au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : Le surplus des conclusions des requêtes n° 375856 et n° 375866 et la requête n° 384705 sont rejetés.<br/>
Article 4 : La présente décision sera notifiée au syndicat national des industries des peintures enduits et vernis, à l'association française des industries, colles, adhésifs et mastics, à la ministre de l'écologie, du développement durable et de l'énergie et à la ministre du logement, de l'égalité des territoires et de la ruralité.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-04-03-07 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. PRINCIPES GÉNÉRAUX DU DROIT. PRINCIPES INTÉRESSANT L'ACTION ADMINISTRATIVE. - PRINCIPE DE SÉCURITÉ JURIDIQUE - PORTÉE - OBLIGATION POUR L'AUTORITÉ INVESTIE DU POUVOIR RÉGLEMENTAIRE D'ÉDICTER LES MESURES TRANSITOIRES QU'IMPLIQUE, S'IL Y A LIEU, UNE RÉGLEMENTATION NOUVELLE [RJ1] - APPLICATION - DÉCRET ET ARRÊTÉ ORGANISANT UNE OBLIGATION DE DÉCLARATION ENVIRONNEMENTALE POUR CERTAINS PRODUITS DE CONSTRUCTION - MÉCONNAISSANCE DE CE PRINCIPE EN L'ESPÈCE - CONSÉQUENCE - ANNULATION DU DÉCRET ET DE L'ARRÊTÉ EN TANT QU'ILS N'ONT PAS PRÉVU UN DÉLAI SUFFISANT POUR LEUR ENTRÉE EN VIGUEUR.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">01-08 ACTES LÉGISLATIFS ET ADMINISTRATIFS. APPLICATION DANS LE TEMPS. - PRINCIPE - APPLICATION IMMÉDIATE - LIMITE - OBLIGATION POUR L'AUTORITÉ INVESTIE DU POUVOIR RÉGLEMENTAIRE D'ÉDICTER, POUR DES MOTIFS DE SÉCURITÉ JURIDIQUE, LES MESURES TRANSITOIRES QU'IMPLIQUE, S'IL Y A LIEU, UNE RÉGLEMENTATION NOUVELLE - EXISTENCE [RJ1] -  DÉCRET ET ARRÊTÉ ORGANISANT UNE OBLIGATION DE DÉCLARATION ENVIRONNEMENTALE POUR CERTAINS PRODUITS DE CONSTRUCTION - MÉCONNAISSANCE DE CE PRINCIPE EN L'ESPÈCE - CONSÉQUENCE - ANNULATION DU DÉCRET ET DE L'ARRÊTÉ EN TANT QU'ILS N'ONT PAS PRÉVU UN DÉLAI SUFFISANT POUR LEUR ENTRÉE EN VIGUEUR.
</SCT>
<ANA ID="9A"> 01-04-03-07 Obligation pour l'autorité investie du pouvoir réglementaire d'édicter les mesures transitoires qu'implique, s'il y a lieu, une réglementation nouvelle, pour assurer le respect du principe de sécurité juridique.,,,Décret relatif à la déclaration environnementale de certains produits de construction destinés à un usage dans les ouvrages de bâtiment, publié au Journal officiel le 29 décembre 2013 et prévoyant que ses dispositions s'appliquent à compter du 1er janvier 2014 pour tous les produits de construction et de décoration. Arrêté, publié dans les mêmes conditions, fixant la liste des informations que doit contenir la déclaration environnementale des produits de construction et de décoration et prévoyant qu'une déclaration simplifiée, contenant une liste réduite d'informations, peut être établie, jusqu'au 1er juillet 2014, par dérogation au droit commun. L'entrée en vigueur du décret étant ainsi conditionnée à l'entrée en vigueur de l'arrêté, il convient d'apprécier le respect du principe de sécurité juridique en prenant en compte l'ensemble des dispositions du décret et de l'arrêté.,,,L'administration ne conteste pas que l'établissement de la déclaration environnementale, même simplifiée, nécessite la réalisation d'études techniques préalables, ni que les  fiches de déclarations environnementales et sanitaires  présentant un bilan environnemental et réalisées à titre volontaire par les entreprises concernées avant l'entrée en vigueur du décret attaqué, qui seules peuvent dispenser de telles études, n'existaient pas pour tous les produits de construction et de décoration. Il incombait au pouvoir réglementaire, pour des motifs de sécurité juridique, de permettre aux entreprises qui commercialisaient les produits en cause avant l'entrée en vigueur du décret attaqué de disposer d'un délai raisonnable pour réaliser les études nécessaires ou, à défaut, de supprimer les allégations figurant sur les produits, pour procéder à la certification de ceux-ci. Si l'arrêté attaqué prévoit, ainsi qu'il a été dit, à titre dérogatoire, une liste réduite des indicateurs qui doivent figurer dans une déclaration simplifiée à compter du 1er janvier 2014 et une liste exhaustive d'informations exigées seulement à compter du 1er juillet 2014, les dispositions attaquées ne peuvent être regardées comme garantissant suffisamment la sécurité juridique des opérateurs économiques concernés. Il appartient dès lors au juge de l'excès de pouvoir, statuant après l'entrée en vigueur de ces dispositions, de les annuler en tant qu'elles n'ont pas prévu un délai suffisant pour leur entrée en vigueur. En l'espèce, annulation du décret et de l'arrêté en tant qu'ils n'ont pas différé de six mois l'obligation d'établir une déclaration environnementale simplifiée et d'un an celle d'établir une déclaration environnementale exhaustive.</ANA>
<ANA ID="9B"> 01-08 Obligation pour l'autorité investie du pouvoir réglementaire d'édicter les mesures transitoires qu'implique, s'il y a lieu, une réglementation nouvelle, pour assurer le respect du principe de sécurité juridique.,,,Décret relatif à la déclaration environnementale de certains produits de construction destinés à un usage dans les ouvrages de bâtiment, publié au Journal officiel le 29 décembre 2013 et prévoyant que ses dispositions s'appliquent à compter du 1er janvier 2014 pour tous les produits de construction et de décoration. Arrêté, publié dans les mêmes conditions, fixant la liste des informations que doit contenir la déclaration environnementale des produits de construction et de décoration et prévoyant qu'une déclaration simplifiée, contenant une liste réduite d'informations, peut être établie, jusqu'au 1er juillet 2014, par dérogation au droit commun. L'entrée en vigueur du décret étant ainsi conditionnée à l'entrée en vigueur de l'arrêté, il convient d'apprécier le respect du principe de sécurité juridique en prenant en compte l'ensemble des dispositions du décret et de l'arrêté.,,,L'administration ne conteste pas que l'établissement de la déclaration environnementale, même simplifiée, nécessite la réalisation d'études techniques préalables, ni que les  fiches de déclarations environnementales et sanitaires  présentant un bilan environnemental et réalisées à titre volontaire par les entreprises concernées avant l'entrée en vigueur du décret attaqué, qui seules peuvent dispenser de telles études, n'existaient pas pour tous les produits de construction et de décoration. Il incombait au pouvoir réglementaire, pour des motifs de sécurité juridique, de permettre aux entreprises qui commercialisaient les produits en cause avant l'entrée en vigueur du décret attaqué de disposer d'un délai raisonnable pour réaliser les études nécessaires ou, à défaut, de supprimer les allégations figurant sur les produits, pour procéder à la certification de ceux-ci. Si l'arrêté attaqué prévoit, ainsi qu'il a été dit, à titre dérogatoire, une liste réduite des indicateurs qui doivent figurer dans une déclaration simplifiée à compter du 1er janvier 2014 et une liste exhaustive d'informations exigées seulement à compter du 1er juillet 2014, les dispositions attaquées ne peuvent être regardées comme garantissant suffisamment la sécurité juridique des opérateurs économiques concernés. Il appartient dès lors au juge de l'excès de pouvoir, statuant après l'entrée en vigueur de ces dispositions, de les annuler en tant qu'elles n'ont pas prévu un délai suffisant pour leur entrée en vigueur. En l'espèce, annulation du décret et de l'arrêté en tant qu'ils n'ont pas différé de six mois l'obligation d'établir une déclaration environnementale simplifiée et d'un an celle d'établir une déclaration environnementale exhaustive.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Assemblée, 24 mars 2006, Sté KPMG et autres, n°s 288460 et autres, p. 154 ; CE, Section, 13 décembre 2006, Mme,, n° 287845, p. 540.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
