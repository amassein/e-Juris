<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034081846</ID>
<ANCIEN_ID>JG_L_2017_02_000000392924</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/08/18/CETATEXT000034081846.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 24/02/2017, 392924</TITRE>
<DATE_DEC>2017-02-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>392924</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP OHL, VEXLIARD</AVOCATS>
<RAPPORTEUR>M. François Monteagle</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:392924.20170224</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Le GAEC des Rocs a demandé au tribunal administratif de Châlons-en-Champagne  d'annuler pour excès de pouvoir la décision du 23 juin 2011 par laquelle le préfet de la Haute-Marne a appliqué un taux de réduction de 20 % sur le montant total des aides directes communautaires versées au groupement au titre de la campagne 2009.<br/>
<br/>
              Par un jugement n° 1101742 du 17 décembre 2013, le tribunal administratif a fait droit à sa demande et annulé la décision litigieuse.<br/>
<br/>
              Par un arrêt n° 14NC00331 du 25 juin 2015, faisant droit à l'appel du ministre de l'agriculture, de l'agroalimentaire et de la forêt, la cour administrative d'appel de Nancy a annulé ce jugement et rejeté la demande du GAEC des Rocs.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 25 août 2015 et 2 novembre 2015 au secrétariat du contentieux du Conseil d'Etat, le GAEC des Rocs  demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel du ministre de l'agriculture, de l'agroalimentaire et de la forêt ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le règlement (CE) n° 73-2009 du Conseil du 19 janvier 2009 établissant des règles communes pour les régimes de soutien direct en faveur des agriculteurs dans le cadre de la politique agricole commune et établissant certains régimes de soutien en faveur des agriculteurs ;<br/>
              - le règlement n° 1760/2000 du Parlement et du Conseil du 17 juillet 2000 ;<br/>
              - le code rural et de la pêche maritime ;<br/>
              - l'arrêté du 30 avril 2009 relatif à la mise en oeuvre de la conditionnalité au titre de l'année 2009 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. François Monteagle, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Ohl, Vexliard, avocat du GAEC des Rocs ;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Par une décision du 23 juin 2011, le préfet de la Haute-Marne a signifié au GAEC des Rocs qu'une réduction d'un montant de 20 % serait appliqué à l'ensemble des aides agricoles européennes perçues par ce dernier au titre de la politique agricole commune pour la campagne 2009, afin de tirer les conséquences d'anomalies constatées lors d'un contrôle effectué sur place sur des bovins par les services de la direction départementale des territoires de la Haute-Marne. Le GAEC des Rocs a saisi le tribunal administratif de Châlons-en-Champagne, qui a annulé la décision litigieuse par un jugement du 17 décembre 2011. Par un arrêt du 25 juin 2015, la cour administrative d'appel de Nancy, faisant droit à l'appel formé par le ministre de l'agriculture, de l'agroalimentaire et de la forêt, a annulé ce jugement et rejeté les conclusions de la demande de première instance du GAEC des Rocs. Celui-ci se pourvoit en cassation contre cet arrêt.<br/>
<br/>
              2. En premier lieu, si le GAEC des Rocs soutient que l'arrêt est irrégulier faute que la minute ait été signée par le greffier d'audience, ce moyen manque en fait.<br/>
<br/>
              3. En deuxième lieu, aux termes de l'article 23 du règlement (CE) 73/2009 du Conseil établissant des règles communes pour les régimes de soutien direct en faveur des agriculteurs dans le cadre de la politique agricole commune et établissant certains régimes de soutien en faveur des agriculteurs : " 1. Lorsque (...) les bonnes conditions agricoles et environnementales ne sont pas respectées à tout moment au cours d'une année civile donnée, ci-après dénommée "année civile concernée" et que la situation de non-respect en question est due à un acte ou à une omission directement imputable à l'agriculteur qui a présenté la demande d'aide durant l'année civile concernée, l'agriculteur concerné se voit appliquer une réduction du montant total des paiements directs octroyés ou à octroyer (...) ". Aux termes de l'article 24 du même règlement, relatif aux modalités applicables aux réductions et exclusions en cas de non-respect des règles de la conditionnalité : " (...) 2. En cas de négligence, le pourcentage de réduction ne peut pas dépasser 5 % ou, s'il s'agit d'un cas de non-respect répété, 15 %. 3. En cas de non-respect délibéré, le pourcentage de réduction ne peut, en principe, pas être inférieur à 20 % et peut aller jusqu'à l'exclusion totale du bénéfice d'un ou de plusieurs régimes d'aide et s'appliquer à une ou plusieurs années civiles. 4. En tout état de cause, le montant total des réductions et exclusions pour une année civile ne peut être supérieur au montant total visé à l'article 23, paragraphe 1 ". S'agissant de la définition des " bonnes conditions agricoles et environnementales " que l'agriculteur qui a présenté la demande d'aide doit respecter, l'annexe II au même règlement renvoie à l'article 4 du règlement 1760/2000 du Parlement et du Conseil du 17 juillet 2000 selon lequel tous les animaux d'une exploitation doivent être identifiés par une marque auriculaire qui lui est propre, apposée sur chaque oreille et comportant un numéro unique. Aux termes de l'article D. 341-10 du code rural et de la pêche maritime : " A compter de la date limite de dépôt de la demande et pendant toute la durée de son engagement, le bénéficiaire est tenu de respecter: (...) 2° Les exigences en matière de conditionnalité définies à la section 4 du chapitre V du titre Ier du livre VI du code rural, sur l'ensemble de son exploitation (...) ". Aux termes de l'article D. 341-14-1 du même code : " I.- Lorsque le bénéficiaire ne respecte pas, sur l'ensemble de son exploitation, les obligations définies au 2° de l'article D. 341-10, le préfet applique des réductions au montant total des paiements annuels mentionnés à l'article D. 341-21, selon les modalités définies aux articles D. 615-57 à D. 615-61. / (...) III.- Lorsque, dans le cadre du contrôle du respect des obligations mentionnées aux I et II, des cas de non-conformité sont constatés, le taux de réduction applicable est déterminé selon les modalités suivantes : / 1° Si, pour un sous-ensemble donné, plusieurs cas de non-conformité sont constatés, le pourcentage de réduction applicable correspond à celui des pourcentages affectés à ces cas dont la valeur est la plus élevée. (...) / Le préfet applique le taux de réduction au montant des paiements annuels mentionnés à l'article D. 341-21, selon les modalités définies au deuxième et au troisième alinéa de l'article D. 615-59 et à l'article D. 615-61 ". Aux termes de l'article D. 615-59 du même code : " (...) Lorsqu'un cas de non-conformité intentionnelle est constaté, le taux de réduction est fixé à 20 %. Par décision motivée, pour des raisons justifiées au vu des résultats des contrôles et de la situation particulière de l'exploitant, ce taux peut être ramené à 15 % ou porté jusqu'à 100 %. Un arrêté du ministre chargé de l'agriculture précise les cas dans lesquels une non-conformité est présumée intentionnelle (...) ".<br/>
<br/>
              4. Il résulte de ces dispositions que la décision portant réduction du montant total des paiements directs octroyés ou à octroyer, prise à l'issue d'un contrôle administratif, est destinée à vérifier que le bénéficiaire des aides respecte les conditions auxquelles leur octroi est conditionné par le droit de l'Union européenne. Cette mesure, qui ne revêt pas un caractère punitif, a pour seule portée d'entrainer le reversement de tout ou partie d'une aide indûment perçue. Ainsi, et alors même que la réduction ainsi décidée a un caractère forfaitaire et tire les conséquences d'une non-conformité intentionnelle, elle ne peut être regardée comme constituant une sanction prononcée à l'encontre d'un agriculteur dont la contestation relèverait de l'office du juge de plein contentieux. Par suite, en jugeant, par une motivation suffisante, que la décision litigieuse n'était pas au nombre des sanctions infligées par l'administration, la cour n'a pas entaché son arrêt d'une erreur de droit.<br/>
<br/>
              5. En troisième lieu, le GAEC des Rocs soutient que la cour a commis une erreur de droit en se fondant sur les dispositions de l'article D. 615-59 du code rural, alors que le pouvoir réglementaire est incompétent pour fixer par décret un régime de sanctions. Il résulte toutefois de ce qui a été dit au point 4 que les dispositions en cause n'instaurent pas un régime de sanctions. Ce moyen doit, dès lors et en tout état de cause, être écarté.<br/>
<br/>
              6. Enfin, en jugeant que le GAEC des Rocs ne produisait pas suffisamment d'éléments de nature à renverser la présomption du caractère intentionnel du manquement relevé lors du contrôle, les juges d'appel  ont porté sur les faits de l'espèce une appréciation souveraine exempte de dénaturation.<br/>
<br/>
              7. Il résulte de tout ce qui précède que le pourvoi du GAEC des Rocs doit être rejeté, y compris ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi du GAEC des Rocs est rejeté. <br/>
Article 2 : La présente décision sera notifiée au GAEC des Rocs et au ministre de l'agriculture, de l'agroalimentaire et de la forêt.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">03-03-06 AGRICULTURE ET FORÊTS. EXPLOITATIONS AGRICOLES. AIDES DE L'UNION EUROPÉENNE. - DÉCISION PORTANT RÉDUCTION DU MONTANT TOTAL DES PAIEMENTS DIRECTS OCTROYÉS OU À OCTROYER - CARACTÈRE DE SANCTION - ABSENCE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">15-05-14 COMMUNAUTÉS EUROPÉENNES ET UNION EUROPÉENNE. RÈGLES APPLICABLES. POLITIQUE AGRICOLE COMMUNE. - DÉCISION PORTANT RÉDUCTION DU MONTANT TOTAL DES PAIEMENTS DIRECTS OCTROYÉS OU À OCTROYER À UN GAEC AU TITRE DES AIDES ACCORDÉES DANS LE CADRE DE LA POLITIQUE AGRICOLE COMMUNE - CARACTÈRE DE SANCTION - ABSENCE [RJ1].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-02-01-01 PROCÉDURE. DIVERSES SORTES DE RECOURS. RECOURS POUR EXCÈS DE POUVOIR. RECOURS AYANT CE CARACTÈRE. - RECOURS DIRIGÉ CONTRE UNE DÉCISION PORTANT RÉDUCTION DU MONTANT TOTAL DES PAIEMENTS DIRECTS OCTROYÉS OU À OCTROYER À UN GAEC AU TITRE DES AIDES ACCORDÉES DANS LE CADRE DE LA POLITIQUE AGRICOLE COMMUNE [RJ1].
</SCT>
<ANA ID="9A"> 03-03-06 Il résulte des articles 23 et 24 du règlement (CE) 73/2009 du Conseil établissant des règles communes pour les régimes de soutien direct en faveur des agriculteurs dans le cadre de la politique agricole commune et établissant certains régimes de soutien en faveur des agriculteurs, ainsi que des articles D. 341-10, D. 341-14-1 et D. 615-59 du code rural et de la pêche maritime, que la décision portant réduction du montant total des paiements directs octroyés ou à octroyer, prise à l'issue d'un contrôle administratif, est destinée à vérifier que le bénéficiaire des aides respecte les conditions auxquelles leur octroi est conditionné par le droit de l'Union européenne. Cette mesure, qui ne revêt pas un caractère punitif, a pour seule portée d'entraîner le reversement de tout ou partie d'une aide indûment perçue. Ainsi, et alors même que la réduction ainsi décidée a un caractère forfaitaire et tire les conséquences d'une non-conformité intentionnelle, elle ne peut être regardée comme constituant une sanction prononcée à l'encontre d'un agriculteur dont la contestation relèverait de l'office du juge de plein contentieux.</ANA>
<ANA ID="9B"> 15-05-14 Il résulte des articles 23 et 24 du règlement (CE) 73/2009 du Conseil établissant des règles communes pour les régimes de soutien direct en faveur des agriculteurs dans le cadre de la politique agricole commune et établissant certains régimes de soutien en faveur des agriculteurs, ainsi que des articles D. 341-10, D. 341-14-1 et D. 615-59 du code rural et de la pêche maritime, que la décision portant réduction du montant total des paiements directs octroyés ou à octroyer, prise à l'issue d'un contrôle administratif, est destinée à vérifier que le bénéficiaire des aides respecte les conditions auxquelles leur octroi est conditionné par le droit de l'Union européenne. Cette mesure, qui ne revêt pas un caractère punitif, a pour seule portée d'entrainer le reversement de tout ou partie d'une aide indûment perçue. Ainsi, et alors même que la réduction ainsi décidée a un caractère forfaitaire et tire les conséquences d'une non-conformité intentionnelle, elle ne peut être regardée comme constituant une sanction prononcée à l'encontre d'un agriculteur dont la contestation relèverait de l'office du juge de plein contentieux.</ANA>
<ANA ID="9C"> 54-02-01-01 Il résulte des articles 23 et 24 du règlement (CE) 73/2009 du Conseil établissant des règles communes pour les régimes de soutien direct en faveur des agriculteurs dans le cadre de la politique agricole commune et établissant certains régimes de soutien en faveur des agriculteurs, ainsi que des articles D. 341-10, D. 341-14-1 et D. 615-59 du code rural et de la pêche maritime, que la décision portant réduction du montant total des paiements directs octroyés ou à octroyer, prise à l'issue d'un contrôle administratif, est destinée à vérifier que le bénéficiaire des aides respecte les conditions auxquelles leur octroi est conditionné par le droit de l'Union européenne. Cette mesure, qui ne revêt pas un caractère punitif, a pour seule portée d'entrainer le reversement de tout ou partie d'une aide indûment perçue. Ainsi, et alors même que la réduction ainsi décidée a un caractère forfaitaire et tire les conséquences d'une non-conformité intentionnelle, elle ne peut être regardée comme constituant une sanction prononcée à l'encontre d'un agriculteur dont la contestation relèverait de l'office du juge de plein contentieux.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. décision du même jour, GAEC des Marmottes, n° 397872, inédite au Recueil.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
