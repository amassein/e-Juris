<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029069562</ID>
<ANCIEN_ID>JG_L_2014_06_000000360135</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/06/95/CETATEXT000029069562.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème / 1ère SSR, 11/06/2014, 360135</TITRE>
<DATE_DEC>2014-06-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>360135</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème / 1ère SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN ; SCP GATINEAU, FATTACCINI ; SCP BARTHELEMY, MATUCHANSKY, VEXLIARD, POUPOT</AVOCATS>
<RAPPORTEUR>M. Samuel Gillis</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:360135.20140611</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 11 juin et 12 septembre 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour la société Devarocle, dont le siège est 5, rue du Pranet à Ouroux-sur-Saône (71370) ; la société Devarocle demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 11LY00852 du 10 avril 2012 par lequel la cour administrative d'appel de Lyon a rejeté sa requête tendant, d'une part, à l'annulation du jugement du 20 janvier 2011 par lequel le tribunal administratif de Dijon a rejeté sa demande tendant à l'annulation de l'arrêté du 30 avril 2009 du maire de la commune d'Ouroux-sur-Saône en tant qu'il a délivré à la société Mexy Promotion un permis de construire pour la création d'une surface commerciale alimentaire, d'autre part, à l'annulation, dans cette mesure, de cet arrêté ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de la commune d'Ouroux-sur-Saône et de la société Mexy Promotion le versement de la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution, notamment son Préambule ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
<br/>
              Vu le code de commerce ; <br/>
<br/>
              Vu le code de l'urbanisme ; <br/>
<br/>
              Vu la loi n° 2008-776 du 4 août 2008 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Samuel Gillis, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la société Devarocle, à la SCP Gatineau, Fattaccini, avocat de la commune d'Ouroux-sur-Saône, et à la SCP Barthélemy, Matuchansky, Vexliard, Poupot, avocat de la société Mexy Promotion ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que le maire de la commune d'Ouroux-sur-Saône ayant été saisi d'une demande de permis de construire un supermarché d'une surface de vente inférieure à 1000 m² par la société Mexy Promotion, la commission départementale d'équipement commercial a été saisie pour avis en application des dispositions transitoires du XXIX de l'article 102 de la loi du 4 août 2008 de modernisation de l'économie, mais n'a pu se réunir ; que, la commission départementale d'aménagement commercial, que la même loi a substitué à la commission départementale d'équipement commercial, ayant été ensuite consultée, conformément aux dispositions de l'article L. 752-4 du code de commerce, dans sa  rédaction issue de la même loi, elle n'a pas rendu d'avis dans le délai d'un mois qui lui était imparti et a ainsi tacitement approuvé le projet, conformément aux prévisions de l'article R. 752-43 du code ;  que le maire a alors, par un arrêté du 30 avril 2009, accordé le permis sollicité ; que, saisi par la société Devarocle, qui exploite un supermarché sur la même commune, le tribunal administratif de Dijon a, par un jugement du 20 janvier 2011, rejeté la demande d'annulation de cet arrêté ; que la société Devarocle se pourvoit en cassation contre l'arrêt du 10 avril 2012 par lequel la cour administrative d'appel de Lyon a confirmé ce jugement ; <br/>
<br/>
              2. Considérant que les dispositions mentionnées ci-dessus de la loi du 4 août 2008 et de l'article L. 752-4 du code de commerce prévoient notamment que, dans les communes de moins de 20 000 habitants, le maire ou le président de l'établissement public de coopération intercommunale compétent en matière d'urbanisme, lorsqu'il est saisi d'une demande de permis de construire un équipement commercial dont la surface est comprise entre 300 et 1 000 mètres carrés peut proposer au conseil municipal ou à l'organe délibérant de l'établissement public de saisir la commission départementale ; qu'en cas d'avis défavorable de celle-ci et, le cas échéant, si le promoteur a saisi la commission nationale, en cas d'avis défavorable de  cette dernière , le permis de construire ne peut être délivré ;<br/>
<br/>
              3. Considérant qu'en dehors du cas où les caractéristiques particulières de la construction envisagée sont de nature à affecter par elles-mêmes les conditions d'exploitation d'un établissement commercial, ce dernier ne justifie pas d'un intérêt à contester devant le juge de l'excès de pouvoir un permis de construire délivré à une entreprise concurrente, même située à proximité ; que, depuis l'entrée en vigueur de la loi du 4 août 2008, le législateur n'a entendu soumettre à une autorisation d'exploitation commerciale que les surfaces de vente supérieures à 1 000 m² ; que, pour les autres projets, la consultation facultative de la commission départementale compétente en matière d'urbanisme commercial et, le cas échéant, de la commission nationale, ne confère pas à la décision relative au permis de construire le caractère d'un acte relevant de la législation de l'urbanisme commercial ; que, d'une part, si un avis défavorable de la commission départementale ou nationale d'équipement commercial empêche la délivrance du permis de construire, une telle décision ne porte atteinte qu'aux droits du pétitionnaire ; que, d'autre part, un avis favorable ne lie pas l'autorité compétente en matière d'urbanisme, qui statue sur la demande de permis de construire dont elle est saisie au regard des règles d'urbanisme ; qu'ainsi, contrairement à ce qui est soutenu,  la faculté prévue par la loi de consulter la commission compétente en matière d'urbanisme commercial est sans incidence sur les conditions dans lesquelles doit être apprécié l'intérêt à agir d'une entreprise contre le permis de construire délivré à une entreprise concurrente ; que, dès lors, c'est sans erreur de droit que la cour administrative d'appel a, par une décision suffisamment motivée, jugé que la requérante, qui se bornait à se prévaloir de ce que l'ouverture de l'établissement commercial qui avait fait l'objet du permis de construire litigieux était susceptible de concurrencer l'établissement qu'elle exploitait, n'avait pas d'intérêt lui donnant qualité à contester ce permis; <br/>
<br/>
              4. Considérant, par ailleurs, que si un établissement commercial est sans intérêt à demander l'annulation de l'avis par lequel la commission compétente en matière d'urbanisme commercial se prononce sur un projet d'établissement concurrent, qui ne constitue qu'un acte préparatoire qui ne lui fait pas grief, et si son intérêt à contester le permis de construire délivré pour la réalisation de ce projet doit être apprécié dans les conditions qui ont été exposées au point 3, la société requérante ne saurait soutenir qu'il en résulte une atteinte aux droits à un procès équitable et à un recours effectif, garantis par l'article 16 de la Déclaration des droits de l'homme et du citoyen et par les articles 6 paragraphe 1 et 13 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              5. Considérant qu'il résulte de tout ce qui précède que la société Devarocle n'est pas fondée à demander l'annulation de l'arrêt attaqué ;<br/>
<br/>
              6. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la commune d'Ouroux-sur-Saône et de la société Mexy Promotion qui ne sont pas, dans la présente espèce, les parties perdantes ; qu'il y a lieu en revanche, dans les circonstances de l'espèce, de mettre à la charge de la société Devarocle la somme globale de 3 000 euros à verser, à parts égales, à la commune d'Ouroux-sur-Saône et à la société Mexy Promotion au titre de ces mêmes dispositions ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
      Article 1er : Le pourvoi de la société Devarocle est rejeté.<br/>
<br/>
Article 2 : La société Devarocle versera, à parts égales, à la commune d'Ouroux-sur-Saône et à la société Mexy Promotion, une somme globale de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée à la société Devarocle, à la commune d'Ouroux-sur-Saône et à la société Mexy Promotion.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">14-02-01-05-01 COMMERCE, INDUSTRIE, INTERVENTION ÉCONOMIQUE DE LA PUISSANCE PUBLIQUE. RÉGLEMENTATION DES ACTIVITÉS ÉCONOMIQUES. ACTIVITÉS SOUMISES À RÉGLEMENTATION. AMÉNAGEMENT COMMERCIAL. CHAMP D'APPLICATION. - EXCLUSION - PROJET D'ÉQUIPEMENT COMMERCIAL NON SOUMIS À AUTORISATION À CE TITRE, MÊME EN CAS DE CONSULTATION FACULTATIVE D'UNE COMMISSION D'URBANISME COMMERCIAL SUR LA DEMANDE DE PERMIS DE CONSTRUIRE - CONSÉQUENCE - INCIDENCE DE LA CONSULTATION SUR LES RÈGLES D'INTÉRÊT POUR AGIR CONTRE LE PERMIS - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-01-04-01-01 PROCÉDURE. INTRODUCTION DE L'INSTANCE. INTÉRÊT POUR AGIR. ABSENCE D'INTÉRÊT. CATÉGORIES DE REQUÉRANTS. - ENTREPRISE CONCURRENTE - PERMIS DE CONSTRUIRE UN ÉQUIPEMENT COMMERCIAL NON SOUMIS À AUTORISATION À CE TITRE, MÊME EN CAS DE CONSULTATION FACULTATIVE DE LA COMMISSION D'AMÉNAGEMENT COMMERCIAL.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">68-06-01-02 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. INTRODUCTION DE L'INSTANCE. INTÉRÊT À AGIR. - PERMIS DE CONSTRUIRE UN ÉQUIPEMENT COMMERCIAL NON SOUMIS À AUTORISATION À CE TITRE - ENTREPRISE CONCURRENTE - ABSENCE, MÊME EN CAS DE CONSULTATION FACULTATIVE DE LA COMMISSION D'AMÉNAGEMENT COMMERCIAL.
</SCT>
<ANA ID="9A"> 14-02-01-05-01 La faculté prévue par la loi n° 2008-776 du 4 août 2008 et par l'article L. 752-4 du code de commerce de consulter la commission compétente en matière d'urbanisme commercial sur les demandes de permis de construire un équipement commercial dont la surface est comprise entre 300 et 1 000 mètres carrés dans les communes de moins de 20 000 habitants est sans incidence sur les conditions dans lesquelles doit être apprécié l'intérêt pour agir d'une entreprise contre le permis de construire délivré à une entreprise concurrente.</ANA>
<ANA ID="9B"> 54-01-04-01-01 La faculté prévue par la loi n° 2008-776 du 4 août 2008 et par l'article L. 752-4 du code de commerce de consulter la commission compétente en matière d'urbanisme commercial sur les demandes de permis de construire un équipement commercial dont la surface est comprise entre 300 et 1 000 mètres carrés dans les communes de moins de 20 000 habitants est sans incidence sur les conditions dans lesquelles doit être apprécié l'intérêt pour agir d'une entreprise contre le permis de construire délivré à une entreprise concurrente. Une entreprise qui se borne à se prévaloir de ce que l'ouverture de l'établissement commercial qui fait l'objet du permis de construire litigieux est susceptible de concurrencer l'établissement qu'elle exploite n'a pas d'intérêt lui donnant qualité à contester ce permis.</ANA>
<ANA ID="9C"> 68-06-01-02 La faculté prévue par la loi n° 2008-776 du 4 août 2008 et par l'article L. 752-4 du code de commerce de consulter la commission compétente en matière d'urbanisme commercial sur les demandes de permis de construire un équipement commercial dont la surface est comprise entre 300 et 1 000 mètres carrés dans les communes de moins de 20 000 habitants est sans incidence sur les conditions dans lesquelles doit être apprécié l'intérêt pour agir d'une entreprise contre le permis de construire délivré à une entreprise concurrente. Une entreprise qui se borne à se prévaloir de ce que l'ouverture de l'établissement commercial qui fait l'objet du permis de construire litigieux est susceptible de concurrencer l'établissement qu'elle exploite n'a pas d'intérêt lui donnant qualité à contester ce permis.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
