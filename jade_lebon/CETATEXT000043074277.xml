<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043074277</ID>
<ANCIEN_ID>JG_L_2021_01_000000433817</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/07/42/CETATEXT000043074277.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 26/01/2021, 433817</TITRE>
<DATE_DEC>2021-01-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>433817</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GASCHIGNARD ; SCP MELKA - PRIGENT</AVOCATS>
<RAPPORTEUR>M. Hervé Cassagnabère</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:433817.20210126</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société anonyme (SA) Pigeon Entreprises a demandé au tribunal administratif de Rennes d'annuler la délibération du 13 janvier 2016 par laquelle le conseil municipal de la commune de Châteaubourg a abrogé les délibérations des 22 septembre 2011 et 17 octobre 2013 autorisant son maire à signer les actes de cession de parcelles à son profit. Par un jugement n° 1601085 du 22 juin 2018, ce tribunal a rejeté cette demande.<br/>
<br/>
              Par un arrêt n° 18NT03225 du 21 juin 2019, la cour administrative d'appel de Nantes a rejeté l'appel de la société Pigeon Entreprises contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 21 août et 22 novembre 2019 au secrétariat du contentieux du Conseil d'Etat, la société Pigeon Entreprises demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de la commune de Châteaubourg la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code civil ;<br/>
- le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Hervé Cassagnabère, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Gaschignard, avocat de la société Pigeon Entreprises et à la SCP Melka - Prigent, avocat de la commune de Châteaubourg ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 15 janvier 2021, présentée par la commune de Chateaubourg ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article 1582 du code civil : " La vente est une convention par laquelle l'un s'oblige à livrer une chose, et l'autre à la payer. " Aux termes de l'article 1583 du même code : la vente " est parfaite entre les parties, et la propriété acquise de droit à l'acheteur à l'égard du vendeur, dès qu'on est convenu de la chose et du prix, quoique la chose n'ait pas encore été livrée ni le prix payé ". <br/>
<br/>
              2. Il ressort des pièces du dossier soumis aux juges du fond que, par des délibérations des 22 septembre 2011 et 17 octobre 2013, le conseil municipal de la commune de Châteaubourg a décidé de donner une suite favorable à  une offre d'achat de la société Pigeon Entreprises concernant un terrain de son domaine privé. La cour administrative d'appel de Nantes ne pouvait, sans commettre de droit, juger que la commune avait pu légalement, par la délibération attaquée, retirer ces deux délibérations, sans rechercher s'il résultait de ces délibérations qu'une vente parfaite devait être regardée comme ayant été conclue entre la commune et la société Pigeon et si des droits avaient ainsi été créés au profit de celle-ci.  Son arrêt doit donc être annulé.<br/>
<br/>
              3. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              4. Il ressort des pièces du dossier qu'en 2008, la commune de Châteaubourg envisageait de développer le lieu-dit Le Plessis-Beuscher. Cette zone comprenait un terrain d'une superficie de 90 707 mètres carrés appartenant à M. et Mme A.... Ces derniers étaient entrés en discussion avec le groupe Pigeon, qui souhaitait acquérir une partie de cette parcelle pour y implanter une centrale à béton. La commune, en accord avec cette société et avec les époux A..., a conçu le projet d'acquérir elle-même le terrain en vue d'y créer une zone artisanale et d'en rétrocéder au groupe Pigeon la partie nécessaire à son activité. <br/>
<br/>
              5. Par un courrier du 1er juillet 2011 de son président-directeur général, la société Pigeon Entreprises, société mère du groupe Pigeon, a fait part au maire de Châteaubourg de son intention " de se porter acquéreur d'un terrain (ou d'un ensemble de terrains) au lieudit "le Plessis-Beuscher" d'une contenance totale de 6 ha 60 a 80 ca " en précisant que, " compte tenu des travaux envisagés préalablement à la commercialisation de l'emprise foncière concernée, cette vente se fera au prix d'environ 6,14 &#128; le mètre carré hors frais d'acte ", et qu'elle priait la commune de lui donner son " accord sur les termes de la présente dans l'attente de la régularisation d'un compromis de vente entre votre Commune et une entité du Groupe qu'il reste à désigner ". <br/>
<br/>
              6. Par une délibération du 22 septembre 2011, le conseil municipal de Chateaubourg a approuvé le principe de la vente au groupe Pigeon d'" environ 66 080 mètres carrés " au prix provisoirement fixé à 6,14 euros le mètre carré. Le terrain objet de la vente était composé de la totalité de cinq parcelles cadastrées ainsi que d'une sixième parcelle " en partie " et un bornage était prévu " pour définir les surfaces exactes cédées ". Le prix de 6,14 euros le mètre carré, qui incluait le coût estimatif des travaux de viabilisation des parcelles cédées à réaliser par la commune, était révisable, à la hausse ou à la baisse, en fonction du coût réel desdits travaux qui résulterait d'appels d'offres futurs. <br/>
<br/>
              7. Par une délibération du 17 octobre 2013, le conseil municipal a décidé de confirmer les modalités de cession et a fixé le prix de vente à 7,88 euros HT le mètre carré, sous réserve d'une révision de ce prix, à la hausse ou à la baisse, " pour être ajusté au résultat des appels d'offres pour la réalisation des travaux de l'ensemble de la zone " concernée qui, outre la viabilisation des parcelles cédées, comprenaient désormais la réalisation " d'un rond-point à cinq branches, dont une réservée exclusivement à l'accès de la future zone industrielle " et a autorisé le maire à signer un compromis de vente. <br/>
<br/>
              8. Toutefois, par une délibération du 13 janvier 2016, le conseil municipal a décidé d'" abroger " les délibérations des 22 septembre 2011 et 17 octobre 2013 aux motifs que le groupe Pigeon  n'avait " jamais donné de suite notamment sur les éléments essentiels du projet d'acquisition " ni " pris aucune initiative pour la réalisation de son projet et (...) ne s'est jamais manifesté pour la signature d'un acte authentique ", " qu'aucun prix définitif n'a jamais été fixé " et que " la zone (...) a connu une évolution ne permettant plus l'implantation d'une centrale à béton et d'une usine de préfabrication pour des motifs d'ordre public " liés " à la sécurité et à la tranquillité publiques notamment en raison de la présence de zone d'habitations ".<br/>
<br/>
              Sur les fins de non-recevoir opposées par la commune de Châteaubourg :<br/>
<br/>
              9. D'une part, la société Pigeon Entreprises, société mère du groupe Pigeon et auteur de l'offre d'acquisition mentionnée au point 5 ci-dessus, présente un intérêt suffisant lui donnant qualité pour demander l'annulation de la délibération du 13 janvier 2016.<br/>
<br/>
              10. D'autre part, la délibération du 13 janvier 2016 ne saurait être regardée comme confirmative du refus de signer un acte de cession que le maire de la commune avait opposé en juillet 2015 dans un courrier adressé au président-directeur général de la société mère du groupe Pigeon et la commune n'est pas fondée à soutenir que la requête de la société Pigeon Entreprises serait tardive pour ce motif. <br/>
<br/>
              Sur la légalité de la délibération du 13 janvier 2016 :<br/>
<br/>
              11. Aux termes de l'article 1129 du code civil, dans sa version applicable aux faits de l'espèce : " Il faut que l'obligation ait pour objet une chose au moins déterminée quant à son espèce. / La quotité de la chose peut être incertaine, pourvu qu'elle puisse être déterminée ". L'article 1591 du même code dispose que : " Le prix de la vente doit être déterminé et désigné par les parties ". Pour l'application de ces dispositions, un prix doit être regardé comme suffisamment déterminé s'il est déterminable en fonction d'éléments objectifs ne dépendant pas de la volonté d'une partie.<br/>
<br/>
              12. Par les délibérations des 22 septembre 2011 et 17 octobre 2013 mentionnées aux points 6 et 7, la commune de Chateaubourg a donné une suite favorable à l'offre d'achat du président-directeur général de la société mère du groupe Pigeon. Il en est résulté un accord entre les parties, d'une part, sur une chose suffisamment désignée dans sa quotité, d'autre part, sur un prix initial objectivement déterminable d'environ 6,14 euros par mètre carré, ajustable à la marge en fonction de l'issue de procédures de passation des marchés publics nécessaires pour la réalisation des travaux de viabilisation. Si la délibération du 17 octobre 2013, qui a par ailleurs confirmé les modalités de l'opération envisagée, a porté le prix initial de 6,14 euros à 7,88 euros par mètre carré, c'est pour tenir compte d'une fraction du coût de la réalisation non d'abord prévue d'un rond-point desservant notamment les parcelles en cause, ainsi que cela a été indiqué à la société par une lettre du maire du même jour. Cette modification de la consistance des travaux et du prix en résultant ont été approuvés par un courrier du 13 mai 2015 par lequel le " conseil habituel du groupe Pigeon " a confirmé la " volonté [du groupe Pigeon] d'acquérir les terrains ". Il résultait de l'ensemble de ces circonstances qu'une vente parfaite devait être regardée comme ayant été conclue entre les parties, de sorte que les délibérations des 22 septembre 2011 et 17 octobre 2013 ont créé des droits au profit de la société et que la délibération du 13 janvier 2016 ne pouvait légalement les retirer, fût-ce pour le motif d'intérêt général dont la commune se prévalait.<br/>
<br/>
              13. Par suite, la société Pigeon Entreprises est fondée à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Rennes a rejeté sa demande tendant à l'annulation de la délibération du 13 janvier 2016. <br/>
<br/>
              14. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de la société Pigeon Entreprises, qui n'est pas la partie perdante dans la présente instance. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de Châteaubourg une somme globale de 4 000 euros à verser à la société Pigeon Entreprises au titre de l'ensemble de la procédure. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nantes du 21 juin 2019 et le jugement du tribunal administratif de Rennes du 22 juin 2018 sont annulés.<br/>
Article 2 : La délibération du conseil municipal de la commune de Châteaubourg du 13 janvier 2016 abrogeant les délibérations du 22 septembre 2011 et du 17 octobre 2013 relatives à la vente de terrains à une société du groupe dont la société mère est la société Pigeon Entreprises est annulée. <br/>
Article 3 : La commune de Châteaubourg versera à la société Pigeon Entreprises la somme de 4 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
Article 4 : La présente décision sera notifiée à la société anonyme Pigeon Entreprises et à la commune de Châteaubourg.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-09-01-02 ACTES LÉGISLATIFS ET ADMINISTRATIFS. DISPARITION DE L'ACTE. RETRAIT. RETRAIT DES ACTES CRÉATEURS DE DROITS. - ALIÉNATION D'UN TERRAIN DU DOMAINE PRIVÉ - 1) RETRAIT PLUS DE QUATRE MOIS APRÈS LA PRISE DE LA DÉCISION [RJ1] - CONDITIONS - VENTE NE REVÊTANT PAS UN CARACTÈRE PARFAIT (ART. 1583 DU CODE CIVIL) [RJ2] - 2) PRIX SUFFISAMMENT DÉTERMINÉ (ART. 1163 ET 1591 DU CODE CIVIL) - PRIX DÉTERMINABLE EN FONCTION D'ÉLÉMENTS OBJECTIFS NE DÉPENDANT PAS DE LA VOLONTÉ D'UNE PARTIE [RJ3].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">24-02-02-01 DOMAINE. DOMAINE PRIVÉ. RÉGIME. ALIÉNATION. - 1) RETRAIT PLUS DE QUATRE MOIS APRÈS LA PRISE DE LA DÉCISION [RJ1] - CONDITIONS - VENTE NE REVÊTANT PAS UN CARACTÈRE PARFAIT (ART. 1583 DU CODE CIVIL) [RJ2] - 2) PRIX SUFFISAMMENT DÉTERMINÉ (ART. 1163 ET 1591 DU CODE CIVIL) - PRIX DÉTERMINABLE EN FONCTION D'ÉLÉMENTS OBJECTIFS NE DÉPENDANT PAS DE LA VOLONTÉ D'UNE PARTIE [RJ3].
</SCT>
<ANA ID="9A"> 01-09-01-02 1) La délibération d'un conseil municipal décidant de donner une suite favorable à une offre d'achat concernant un terrain du domaine privé de la commune ne peut être légalement retirée, plusieurs années après, s'il en résulte qu'une vente parfaite doit être regardée comme ayant été conclue entre la commune et l'acheteur et si des droits ont ainsi été créés au profit de celui-ci.,,,2) Pour l'application de l'article 1129, devenu l'article 1163, et de l'article 1591 du code civil, un prix doit être regardé comme suffisamment déterminé s'il est déterminable en fonction d'éléments objectifs ne dépendant pas de la volonté d'une partie.</ANA>
<ANA ID="9B"> 24-02-02-01 1) La délibération d'un conseil municipal décidant de donner une suite favorable à une offre d'achat concernant un terrain du domaine privé de la commune ne peut être légalement retirée, plusieurs années après, s'il en résulte qu'une vente parfaite doit être regardée comme ayant été conclue entre la commune et l'acheteur et si des droits ont ainsi été créés au profit de celui-ci.,,,2) Pour l'application de l'article 1129, devenu l'article 1163, et de l'article 1591 du code civil, un prix doit être regardé comme suffisamment déterminé s'il est déterminable en fonction d'éléments objectifs ne dépendant pas de la volonté d'une partie.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Assemblée, 26 octobre 2001,,, n° 197178, p. 497.,,[RJ2] Cf. CE, 15 mars 2017, SARL bowling du Hainaut et SARL bowling de Saint-Amand-les-Eaux, n° 393407, T. pp. 523-601. Rappr., s'agissant du transfert d'un bien relevant du domaine public communal à une autre personne publique, CE, 29 juillet 2020, SIVOM de la région de Chevreuse, n° 427738, à mentionner aux Tables.,,[RJ3] Rappr. Cass. req., 7 janvier 1925,,c/,et a., DH 1925. 57, GAJC T. 2 n° 262 ; Cass. civ. 1ère, 10 février 1965,,c/,, n° 63-10.397, Bull. 1965 I n° 123 ; Cass. civ. 3ème, 6 juin 1969, Epoux,c/ Epoux,, n° 67-13.324, Bull. 1969 III n° 4645.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
