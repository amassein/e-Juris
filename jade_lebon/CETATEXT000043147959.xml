<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043147959</ID>
<ANCIEN_ID>JG_L_2021_02_000000440401</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/14/79/CETATEXT000043147959.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 12/02/2021, 440401</TITRE>
<DATE_DEC>2021-02-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>440401</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI, SUREAU ; LE PRADO</AVOCATS>
<RAPPORTEUR>M. Paul Bernard</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Guillaume Odinet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:440401.20210212</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. A... B... a demandé au tribunal administratif de Paris d'annuler, d'une part, l'arrêté du 25 juin 2018 par lequel la garde des sceaux, ministre de la justice, a refusé de l'inscrire sur les registres du sceau de France comme ayant succédé au titre de duc B... et, d'autre part, l'arrêté du même jour investissant M. C... B... du titre de duc.<br/>
<br/>
              Par un jugement n° 1816301, 1816302 du 4 juillet 2019, le tribunal administratif de Paris a rejeté ses demandes. <br/>
<br/>
              Par un arrêt n° 19PA02876 du 5 mars 2020, la cour administrative d'appel de Paris a rejeté l'appel de M. B... contre ce jugement. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un nouveau mémoire, enregistrés les 5 mai, 5 août et 8 décembre 2020 au secrétariat du contentieux du Conseil d'Etat, M. A... B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
              Par un mémoire enregistré le 8 décembre 2020 au secrétariat du contentieux du Conseil d'Etat, Erreur ! Aucune variable de document fournie. demande au Conseil d'Etat, en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958 et à l'appui de son pourvoi, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des dispositions, d'une part, des articles 34, 34-1, 35, 40, 46 et 49 du code civil et, d'autre part, des articles 61, 61-1, 61-2, 61-3, 61-3-1 et 61-4 du même code.  <br/>
<br/>
<br/>
                     Vu les autres pièces du dossier ;<br/>
<br/>
                     Vu :<br/>
                     - la Constitution, notamment son Préambule et son article 61-1 ;<br/>
                     - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
                     - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
                     - le code civil ;<br/>
                     - le décret du 8 janvier 1859 portant rétablissement du conseil du sceau des titres, ensemble le décret du 10 janvier 1872 supprimant le conseil du sceau des titres et attribuant ses fonctions au conseil d'administration du ministère de la justice ;<br/>
                     - les lettres patentes du roi Louis XV en date de juin 1742 enregistrées au Parlement de Paris le 20 août 1742 ;<br/>
                     - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Paul Bernard, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Guillaume Odinet, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Spinosi, Sureau, avocat de M. B... ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1.	Il ressort des pièces du dossier soumis aux juges du fond que, par arrêté du 25 juin 2018, la garde des sceaux, ministre de la justice, a refusé d'inscrire sur les registres du sceau de France M. A... B... comme ayant succédé au titre de duc B..., au motif qu'il n'est pas né " en légitime mariage " de Victor François, huitième titulaire, décédé le 12 février 2012 et déclaré comme étant son père par un jugement du tribunal de grande instance de Paris du 5 février 1991, confirmé par un arrêt de la cour d'appel de Paris du 9 janvier 1992. Le même jour, la garde des sceaux, ministre de la justice, a inscrit M. C... B..., frère cadet de Victor François, sur le registre du sceau de France comme ayant succédé au titre de duc B..., conféré à son ancêtre par lettres patentes du roi Louis XV du mois de juin 1742. M. A... B... se pourvoit en cassation contre l'arrêt du 5 mars 2020 par lequel la cour administrative d'appel de Paris a rejeté son appel contre le jugement du tribunal administratif rejetant ses demandes d'annulation de ces arrêtés. <br/>
<br/>
              2. L'article L. 822-1 du code de justice administrative dispose que : " Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux ". <br/>
<br/>
              Sur les questions prioritaires de constitutionnalité :<br/>
<br/>
              3. Aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...) ". Il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              4. En premier lieu, M. B... demande, à l'appui de son pourvoi en cassation, que soit renvoyée au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des dispositions, d'une part, des articles 34, 34-1, 35, 40, 46 et 49 du code civil et, d'autre part, des articles 61, 61-1, 61-2, 61-3, 61 3-1 et 61-4 du même code, qui sont respectivement relatives à l'établissement, au contenu et à la tenue des actes de l'état civil et aux changements de prénoms et de nom. Il soutient qu'en n'y définissant pas les règles relatives à la transmission des titres nobiliaires, le législateur a méconnu sa propre compétence dans des conditions affectant des droits et libertés que la Constitution garantit, dont le principe d'égalité devant la loi résultant des articles 1er et 6 de la Déclaration des droits de l'homme et du citoyen de 1789.<br/>
<br/>
              5. Toutefois, depuis la promulgation des lois constitutionnelles de 1875, nulle autorité de la République ne dispose du pouvoir de collationner, de confirmer ou de reconnaître des titres nobiliaires, qui se transmettent de plein droit et sans intervention de ces autorités. La seule compétence maintenue au garde des sceaux, en application de l'article 7 du décret du 8 janvier 1859 et du décret du 10 janvier 1872, est celle de se prononcer sur les demandes de vérification des titres de noblesse, qui le conduisent uniquement à examiner les preuves de la propriété du titre par celui qui en fait la demande. En refusant à une personne l'inscription sur le registre du sceau de France de la transmission d'un titre nobiliaire et en inscrivant une autre personne comme ayant succédé à un tel titre, qui ne se confond pas avec le nom, le ministre ne fait pas application des dispositions précitées du code civil, qui ont un tout autre objet. Au demeurant, si le requérant invoque l'incompétence négative qui entacherait ces dispositions législatives faute de comporter des règles relatives à la transmission des titres nobiliaires, un tel grief ne peut être utilement présenté qu'à l'encontre de dispositions applicables au litige et à la condition de contester les insuffisances du dispositif qu'elles instaurent et non pour revendiquer la création d'un régime dédié. Ce grief est, par suite, inopérant.<br/>
<br/>
              6. En second lieu, M. B... demande, à l'appui de son pourvoi en cassation, que soit renvoyée au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des dispositions des lettres patentes du roi Louis XV de juin 1742, enregistrées au Parlement de Paris le 20 août 1742, au vu desquelles les arrêtés litigieux ont été pris, qui prévoient que " le titre, qualité et honneur de duc héréditaire " B... est exclusivement transmis de son titulaire à " l'aîné de ses mâles nés et à naître de lui en légitime mariage ". Toutefois, les actes conférant, confirmant ou maintenant les titres nobiliaires antérieurement à l'instauration de la République constituent des actes de la puissance souveraine dans l'exercice de son pouvoir administratif, y compris en ce qu'ils fixent, le cas échéant, les règles de transmission de ces titres. Par suite, les lettres patentes du roi Louis XV de juin 1742 ne sont pas au nombre des dispositions législatives susceptibles d'être renvoyées au Conseil constitutionnel en application de l'article 61-1 de la Constitution.<br/>
<br/>
              7. Il résulte de ce qui précède que, sans qu'il soit besoin de renvoyer au Conseil constitutionnel les questions prioritaires de constitutionnalité invoquées, les moyens tirés de ce que les dispositions contestées du code civil et les lettres patentes du roi Louis XV en date de juin 1742 enregistrées au Parlement de Paris le 20 août 1742 portent atteinte aux droits et libertés garantis par la Constitution ne sont pas de nature à permettre l'admission du pourvoi.<br/>
<br/>
              Sur les autres moyens du pourvoi :<br/>
<br/>
              8. Pour demander l'annulation de l'arrêt qu'il attaque, M. B... soutient que la cour administrative d'appel de Paris a insuffisamment motivé sa décision et commis une erreur de droit en écartant l'invocabilité des stipulations des articles 8 et 14 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales.<br/>
<br/>
              9. Aucun de ces moyens n'est de nature à permettre l'admission du pourvoi.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel les questions prioritaires de constitutionnalité soulevées par M. B.... <br/>
Article 2 : Le pourvoi de M. B... n'est pas admis. <br/>
Article 3 : La présente décision sera notifiée à M. A... B.... <br/>
Copie en sera adressée au Conseil constitutionnel, au Premier ministre, au garde des sceaux, ministre de la justice et à M. C... B....  <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-10-05-04-02 PROCÉDURE. - GRIEF D'INCOMPÉTENCE NÉGATIVE - OPÉRANCE - GRIEF CONTESTANT LES INSUFFISANCES DU DISPOSITIF INSTAURÉ PAR LA LOI - ABSENCE - GRIEF REVENDIQUANT LA CRÉATION D'UN RÉGIME DÉDIÉ - ABSENCE [RJ1].
</SCT>
<ANA ID="9A"> 54-10-05-04-02 Question prioritaire de constitutionnalité (QPC) dirigée contre les articles 34, 34-1, 35, 40, 46 et 49, 61, 61-1, 61-2, 61-3, 61-3-1 et 61-4 du code civil, relatifs à l'établissement, au contenu et à la tenue des actes de l'état civil et aux changements de prénoms et de nom.,,,Si le requérant invoque l'incompétence négative qui entacherait ces dispositions législatives faute de comporter des règles relatives à la transmission des titres nobiliaires, un tel grief ne peut être utilement présenté qu'à l'encontre de dispositions applicables au litige et à la condition de contester les insuffisances du dispositif qu'elles instaurent et non pour revendiquer la création d'un régime dédié.... ,,Ce grief est, par suite, inopérant.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr. Cons. Const., 28 décembre 2018, n° 2018-777 DC, pt. 73.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
