<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028047780</ID>
<ANCIEN_ID>JG_L_2013_10_000000369971</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/04/77/CETATEXT000028047780.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème et 7ème sous-sections réunies, 04/10/2013, 369971</TITRE>
<DATE_DEC>2013-10-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>369971</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème et 7ème sous-sections réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GASCHIGNARD</AVOCATS>
<RAPPORTEUR>M. Tristan Aureau</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Damien Botteghi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:369971.20131004</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le jugement n° 1303180/4 du 20 juin 2013, enregistré le 8 juillet 2013 au secrétariat du contentieux du Conseil d'Etat, par lequel le tribunal administratif de Melun, avant de statuer sur la demande de M. A...B..., demeurant..., tendant à l'annulation pour excès de pouvoir des décisions du 22 février 2013 par lesquelles le préfet du Val-de-Marne a refusé de renouveler son titre de séjour et a assorti ce refus d'une obligation de quitter le territoire français, a décidé, par application des dispositions de l'article 23-2 de l'ordonnance n° 58-1067 du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel, de transmettre au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution des dispositions du second alinéa de l'article L. 313-12 du code de l'entrée et du séjour des étrangers et du droit d'asile ; <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution, notamment son Préambule et son article 61-1 ; <br/>
<br/>
              Vu l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
<br/>
              Vu l'article L. 313-12 du code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
<br/>
              Vu la loi n° 91-647 du 10 juillet 1991 ; <br/>
<br/>
              Vu la loi n° 99-944 du 15 novembre 1999 ; <br/>
<br/>
              Vu la loi n° 2011-672 du 16 juin 2011 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Tristan Aureau, Auditeur,<br/>
<br/>
              - les conclusions de M. Damien Botteghi, Rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gaschignard, avocat de M. B...; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'il résulte de l'article 23-4 de l'ordonnance du 7 novembre 1958 que, lorsqu'une juridiction relevant du Conseil d'Etat lui a transmis, en application de l'article 23-2 de cette même ordonnance, un moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution, le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              2.	Considérant que l'article L. 313-11 du code de l'entrée et du séjour des étrangers et du droit d'asile énonce plusieurs cas dans lesquels la carte de séjour temporaire portant la mention " vie privée et familiale " est délivrée de plein droit, au nombre desquels figurent, au 4° " l'étranger ne vivant pas en état de polygamie, marié avec un ressortissant de nationalité française, à condition que la communauté de vie n'ait pas cessé depuis le mariage (...) " et, au 7° " l'étranger ne vivant pas en état de polygamie, qui n'entre pas dans les catégories précédentes ou dans celles qui ouvrent droit au regroupement familial, dont les liens personnels et familiaux en France, appréciés notamment au regard de leur intensité, de leur ancienneté et de leur stabilité, des conditions d'existence de l'intéressé, de son insertion dans la société française ainsi que de la nature de ses liens avec la famille restée dans le pays d'origine, sont tels que le refus d'autoriser son séjour porterait à son droit au respect de sa vie privée et familiale une atteinte disproportionnée au regard des motifs du refus " ; qu'il résulte de l'article 12 de la loi du 15 novembre 1999 que la conclusion d'un pacte civil de solidarité constitue l'un des éléments d'appréciation des liens personnels en France au sens de ces dispositions ; qu'aux termes du second alinéa de l'article L. 313-12 du même code : " Le renouvellement de la carte de séjour délivrée au titre du 4° de l'article L. 313-11 est subordonné au fait que la communauté de vie n'ait pas cessé, sauf si elle résulte du décès du conjoint français. Toutefois, lorsque la communauté de vie a été rompue en raison de violences conjugales qu'il a subies de la part de son conjoint, l'autorité administrative ne peut procéder au retrait du titre de séjour de l'étranger et peut en accorder le renouvellement (...) " ; qu'enfin, il résulte de l'article L. 316-3, dans sa rédaction issue de la loi du 16 juin 2011, que " Sauf si sa présence constitue une menace à l'ordre public, l'autorité administrative délivre dans les plus brefs délais une carte de séjour temporaire portant la mention "vie privée et familiale" à l'étranger qui bénéficie d'une ordonnance de protection en vertu de l'article 515-9 du code civil, en raison des violences commises par son conjoint, son partenaire lié par un pacte civil de solidarité ou son concubin. (...) " ;<br/>
<br/>
              3.	Considérant que M. B...soutient qu'en tant qu'elles s'appliquent, parmi les victimes de violences conjugales, aux seuls étrangers mariés, à l'exclusion de ceux qui sont liés à un ressortissant français par un pacte civil de solidarité, les dispositions du second alinéa de l'article L. 313-12 méconnaissent le principe constitutionnel d'égalité, dans la mesure où - à la différence de ce que le législateur a prévu à l'article L. 316-3 dans le cas où l'étranger victime de violences bénéficie d'une ordonnance de protection - le second alinéa de l'article L. 313-12 réserve aux personnes mariées la dérogation à la condition de communauté de vie ;<br/>
<br/>
              4.	Considérant que les dispositions du second alinéa de l'article L. 313-12 du code de l'entrée et du séjour des étrangers et du droit d'asile doivent être regardées comme applicables au litige dont est saisi le tribunal administratif de Melun, au sens et pour l'application de l'article 23-4 de l'ordonnance du 7 novembre 1958 ; que cette disposition n'a pas déjà été déclarée conforme à la Constitution par le Conseil constitutionnel ; que le moyen tiré de ce qu'elle porte atteinte aux droits et libertés garantis par la Constitution, notamment au principe constitutionnel d'égalité, en tant qu'elles s'appliquent aux seuls étrangers mariés et non à ceux qui sont liés à un ressortissant français par un pacte civil de solidarité, soulève une question présentant un caractère sérieux ; qu'ainsi, il y a lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée ;<br/>
<br/>
              5.	Considérant que la présente décision se borne à statuer sur le renvoi d'une question prioritaire de constitutionnalité au Conseil constitutionnel ; qu'en conséquence, les conclusions présentées au titre des frais de procédure ne peuvent être portées que devant le juge saisi du litige à l'occasion duquel la question prioritaire de constitutionnalité a été soulevée ; que par suite, les conclusions présentées par l'avocat de M. B...au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 sont irrecevables ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La question de la conformité aux droits et libertés garantis par la Constitution des dispositions du second alinéa de l'article L. 313-12 du code de l'entrée et du séjour des étrangers et du droit d'asile est renvoyée au Conseil constitutionnel.<br/>
<br/>
Article 2 : Les conclusions présentées par l'avocat de M. B...au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 sont rejetées.<br/>
<br/>
Article 3 : La présente décision sera notifiée à M. A...B...et au ministre de l'intérieur.  <br/>
  Copie en sera adressée au Premier ministre et au tribunal administratif de Melun.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-06-05-11 PROCÉDURE. JUGEMENTS. FRAIS ET DÉPENS. REMBOURSEMENT DES FRAIS NON COMPRIS DANS LES DÉPENS. - CAS DE CONCLUSIONS TENDANT AU REMBOURSEMENT DES FRAIS DE PROCÉDURE PRÉSENTÉES DANS UNE INSTANCE OÙ LE CONSEIL D'ETAT SE BORNE À STATUER SUR LE RENVOI D'UNE QPC - IRRECEVABILITÉ [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-10 PROCÉDURE. - CAS DE CONCLUSIONS TENDANT AU REMBOURSEMENT DES FRAIS DE PROCÉDURE PRÉSENTÉES DANS UNE INSTANCE OÙ LE CONSEIL D'ETAT SE BORNE À STATUER SUR LE RENVOI D'UNE QPC - IRRECEVABILITÉ [RJ1].
</SCT>
<ANA ID="9A"> 54-06-05-11 Les conclusions présentées au titre des frais de procédure ne peuvent être portées que devant le juge saisi du litige à l'occasion duquel une question prioritaire de constitutionnalité (QPC) a été soulevée. Par conséquent, de telles conclusions sont rejetées comme irrecevables par le Conseil d'Etat lorsqu'il se borne, dans sa décision, à statuer sur le renvoi d'une QPC au Conseil constitutionnel.</ANA>
<ANA ID="9B"> 54-10 Les conclusions présentées au titre des frais de procédure ne peuvent être portées que devant le juge saisi du litige à l'occasion duquel une question prioritaire de constitutionnalité (QPC) a été soulevée. Par conséquent, de telles conclusions sont rejetées comme irrecevables par le Conseil d'Etat lorsqu'il se borne, dans sa décision, à statuer sur le renvoi d'une QPC au Conseil constitutionnel.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 16 avril 2012, Association de chasse privée de Bonne rencontre, n° 355919, inédite au Recueil.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
