<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000024448435</ID>
<ANCIEN_ID>JG_L_2011_08_000000334287</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/24/44/84/CETATEXT000024448435.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème et 2ème sous-sections réunies, 02/08/2011, 334287, Publié au recueil Lebon</TITRE>
<DATE_DEC>2011-08-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>334287</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème et 2ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Christian Vigouroux</PRESIDENT>
<AVOCATS>SCP CELICE, BLANCPAIN, SOLTNER ; SCP BORE ET SALVE DE BRUNETON</AVOCATS>
<RAPPORTEUR>M. Laurent Cytermann</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Bertrand Dacosta</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2011:334287.20110802</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 2 décembre 2009 et 2 mars 2010 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. Cédric A, demeurant ... ; M. Cédric A demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 07MA03720 du 9 octobre 2009 par lequel la cour administrative d'appel de Marseille a rejeté sa requête tendant, d'une part, à l'annulation du jugement n° 0602927 du 29 juin 2007 par lequel le tribunal administratif de Marseille a rejeté sa demande dirigée contre l'arrêté du 18 novembre 2005 du maire de la commune de Meyreuil ayant refusé de lui délivrer un permis de construire, d'autre part, à l'annulation pour excès de pouvoir de cette décision ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses conclusions d'appel et d'annuler le jugement du tribunal administratif de Marseille et, en conséquence, d'annuler le refus de permis de construire ainsi que la décision rejet du recours gracieux du 22 février 2006 ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Meyreuil la somme de 4 000 euros en application de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'urbanisme ;<br/>
<br/>
              Vu la loi n° 2000-1208 du 13 décembre 2000 ;<br/>
<br/>
              Vu la loi n° 2003-590 du 2 juillet 2003 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Laurent Cytermann, chargé des fonctions de Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Célice, Blancpain, Soltner, avocat de M. Cédric A et de la SCP Boré et Salve de Bruneton, avocat de la commune de meyreuil, <br/>
<br/>
              - les conclusions de M. Bertrand Dacosta, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Célice, Blancpain, Soltner, avocat de M. Cédric A et à la SCP Boré et Salve de Bruneton, avocat de la commune de Meyreuil, <br/>
<br/>
<br/>
<br/>Considérant qu'aux termes de l'article L. 111-5 du code de l'urbanisme, dans sa rédaction antérieure à la loi du 13 décembre 2000 relative à la solidarité et au renouvellement urbains, : " Il ne peut plus être construit sur toute partie détachée d'un terrain dont la totalité des droits de construire, compte tenu notamment du coefficient d'occupation du sol en vigueur, a été précédemment utilisée. / Lorsqu'une partie est détachée d'un terrain dont les droits de construire n'ont été que partiellement utilisés, il ne peut y être construit que dans la limite des droits qui n'ont pas été utilisés avant la division. (...) " ; qu'aux termes du premier alinéa de l'article L. 123-19 du même code, dans sa rédaction applicable à la date de la décision attaquée : " Les plans d'occupation des sols approuvés avant l'entrée en vigueur de la loi n° 2000-1208 du 13 décembre 2000 précitée ont les mêmes effets que les plans locaux d'urbanisme. Ils sont soumis au régime juridique des plans locaux d'urbanisme défini par les articles L. 123-1-1 à <br/>
L. 123-18. Les dispositions de l'article L. 123-1, dans leur rédaction antérieure à cette loi, leur demeurent applicables. " ; que selon l'article L. 123-1, dans sa rédaction antérieure à la loi du 13 décembre 2000 : " les plans d'occupation des sols fixent (...) les règles générales et les servitudes d'utilisation des sols " ; <br/>
<br/>
              Considérant qu'il résulte de ces dispositions que si le plan d'occupation des sols peut fixer au titre de l'article L. 123-1, dans sa rédaction antérieure à la loi du 13 décembre 2000, des règles relatives à la superficie minimale des terrains, le fait de tenir compte pour apprécier cette superficie des droits à construire déjà utilisés sur des parcelles détachées ne pouvait résulter que d'une disposition législative expresse que n'avait pas rétablie la loi du 2 juillet 2003 ; <br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que l'article NB5 du règlement du plan d'occupation des sols de la commune de Meyreuil subordonnait la constructibilité en secteur NB1 à une superficie minimale de 4 000 m² et prévoyait qu'en cas de détachement d'une propriété bâtie, cette surface minimale devait continuer à s'appliquer à l'unité foncière restant attachée à la construction ; qu'en jugeant que cet article NB5, qui avait pour objet et pour effet de prendre en compte, pour le calcul de la superficie minimale, les terrains bâtis détachés de la parcelle objet du permis de construire, pouvait être appliqué malgré l'entrée en vigueur de la loi du 13 décembre 2000 abrogeant  l'article L. 111-5 qui en était la seule base légale, la cour administrative d'appel de Marseille a commis une erreur de droit ; que, par suite, et sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, M. A est fondé à demander, pour ce motif, l'annulation de l'arrêt attaqué ;<br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application de l'article L. 821-2 du code de justice administrative ; <br/>
<br/>
              Sans qu'il soit besoin d'examiner les autres moyens de la requête ;<br/>
<br/>
              Considérant, ainsi qu'il vient d'être dit, que le maire de la commune de Meyreuil ne pouvait légalement, postérieurement à l'abrogation de l'article L. 111-5 du code de l'urbanisme par la loi du 13 décembre 2000, opposer l'article NB5 du règlement du plan d'occupation des sols à la demande de permis de construire de M. A ; qu'il résulte de l'instruction que c'est sur ce seul fondement que l'autorisation de construire a été refusée ; que M. A est donc fondé à soutenir que c'est à tort que le tribunal administratif de Marseille a rejeté sa demande d'annulation de la décision de refus de permis du maire de la commune de Meyreuil ;<br/>
<br/>
              Considérant que l'exécution de la présente décision implique que la demande de M. A soit réexaminée ; qu'il y a lieu, par suite, d'enjoindre au maire de la commune de Meyreuil de procéder à ce réexamen dans un délai de trois mois à compter de la notification de la présente décision ; que dans les circonstances de l'espèce, il n'y a pas lieu d'assortir cette injonction de l'astreinte demandée par M. A ;<br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de Meyreuil le versement à M. A d'une somme de 4 000 euros en application des dispositions de l'article L. 761-1 du code de justice administrative ; que ces dispositions font obstacle à ce que soit mis à la charge de M. A, qui n'est pas, dans la présente instance, la partie perdante, le versement de la somme demandée par la commune de Meyreuil au titre des frais exposés par elle et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>            D E C I D E :<br/>
          --------------<br/>
Article 1er : L'arrêt du 9 octobre 2009 de la cour administrative d'appel de Marseille est annulé.<br/>
Article 2 : Le jugement du 29 juin 2007 du tribunal administratif de Marseille est annulé.<br/>
Article 3 : L'arrêté en date du 18 novembre 2005 par lequel le maire de Meyreuil a refusé de délivrer le permis de construire demandé par M. A est annulé.<br/>
Article 4 : Il est enjoint au maire de la commune de Meyreuil de procéder au réexamen de la demande de M. A dans un délai de trois mois à compter de la notification de la présente décision.<br/>
Article 5 : La commune de Meyreuil versera à M. Cédric A une somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 6 : Les conclusions présentées par la commune de Meyreuil sur le fondement de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 7 : Le surplus des conclusions de M. Cédric A est rejeté.<br/>
Article 8 : La présente décision sera notifiée à M. Cédric A et à la commune de meyreuil.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">68-01-01-02-02 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PLANS D'AMÉNAGEMENT ET D'URBANISME. PLANS D'OCCUPATION DES SOLS ET PLANS LOCAUX D'URBANISME. APPLICATION DES RÈGLES FIXÉES PAR LES POS OU LES PLU. RÈGLES DE FOND. - LIMITATIONS DU DROIT À CONSTRUIRE EN CAS DE DÉTACHEMENT DE PARCELLES - INCLUSION - DISPOSITION SOUMETTANT LE TERRAIN APRÈS DÉTACHEMENT À LA SUPERFICIE MINIMALE POUR CONSTRUIRE - CONSÉQUENCE - INOPPOSABILITÉ DE LA DISPOSITION DEPUIS L'ABROGATION DE L'ARTICLE L. 111-5 DU CODE DE L'URBANISME PAR LA LOI SRU.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">68-03-03-01-05 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PERMIS DE CONSTRUIRE. LÉGALITÉ INTERNE DU PERMIS DE CONSTRUIRE. LÉGALITÉ AU REGARD DE LA RÉGLEMENTATION NATIONALE. AUTRES DISPOSITIONS LÉGISLATIVES OU RÉGLEMENTAIRES. - 1) ARTICLE L. 111-5 DU CODE DE L'URBANISME - ABROGATION PAR LA LOI SRU - CONSÉQUENCE - INOPPOSABILITÉ DE LA RÈGLE, FIXÉE PAR UN POS OU UN PLU, SOUMETTANT UN TERRAIN APRÈS DÉTACHEMENT DE PARCELLES À LA SUPERFICIE MINIMALE POUR CONSTRUIRE - 2) ARTICLE L. 123-1 DU CODE DE L'URBANISME - POSSIBILITÉ POUR UN PLAN D'URBANISME D'ÉDICTER, SUR CE FONDEMENT, UNE RÈGLE DE LIMITATION DU DROIT À CONSTRUIRE APRÈS DÉTACHEMENT DE PARCELLES - ABSENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">68-03-03-02-02 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PERMIS DE CONSTRUIRE. LÉGALITÉ INTERNE DU PERMIS DE CONSTRUIRE. LÉGALITÉ AU REGARD DE LA RÉGLEMENTATION LOCALE. POS OU PLU (VOIR SUPRA PLANS D'AMÉNAGEMENT ET D'URBANISME). - LIMITATIONS DU DROIT À CONSTRUIRE EN CAS DE DÉTACHEMENT DE PARCELLES - INCLUSION - DISPOSITION SOUMETTANT LE TERRAIN APRÈS DÉTACHEMENT À LA SUPERFICIE MINIMALE POUR CONSTRUIRE - CONSÉQUENCE - INOPPOSABILITÉ DE LA DISPOSITION DEPUIS L'ABROGATION DE L'ARTICLE L. 111-5 DU CODE DE L'URBANISME PAR LA LOI SRU.
</SCT>
<ANA ID="9A"> 68-01-01-02-02 Une règle, posée par un plan d'occupation des sols (POS) ou un plan local d'urbanisme (PLU), selon laquelle, en cas de détachement de parcelles, le terrain dont la parcelle a été détachée doit lui-même conserver, dès lors qu'il est construit, la surface minimale pour construire, est une limitation du droit de construire en cas de détachement de parcelles qui ne pouvait légalement se fonder que sur l'article L. 111-5 du code de l'urbanisme dans sa rédaction antérieure à la loi n° 2000-1208 du 13 décembre 2000 relative à la solidarité et au renouvellement urbains (SRU), et non sur l'article L. 123-1 du même code. Par suite, l'abrogation de l'article L. 111-5 par la loi SRU a eu pour effet de rendre la règle inopposable, y compris après le rétablissement d'une version modifiée de cet article par la loi n° 2003-590 du 2 juillet 2003.</ANA>
<ANA ID="9B"> 68-03-03-01-05 1) Une règle, posée par un plan d'occupation des sols (POS) ou un plan local d'urbanisme (PLU), selon laquelle, en cas de détachement de parcelles, le terrain dont la parcelle a été détachée doit lui-même conserver, dès lors qu'il est construit, la surface minimale pour construire, est une limitation du droit de construire en cas de détachement de parcelles qui ne pouvait légalement se fonder que sur l'article L. 111-5 du code de l'urbanisme dans sa rédaction antérieure à la loi n° 2000-1208 du 13 décembre 2000 relative à la solidarité et au renouvellement urbains (SRU). Par suite, l'abrogation de l'article L. 111-5 par la loi SRU a eu pour effet de rendre la règle inopposable, y compris après le rétablissement d'une version modifiée de cet article par la loi du n° 2003-590 du 2 juillet 2003. 2) L'article L. 123-1 du code de l'urbanisme ne peut légalement fonder l'édiction d'une telle règle de limitation du droit à construire après détachement de parcelles.</ANA>
<ANA ID="9C"> 68-03-03-02-02 Une règle, posée par un plan d'occupation des sols (POS) ou un plan local d'urbanisme (PLU), selon laquelle, en cas de détachement de parcelles, le terrain dont la parcelle a été détachée doit lui-même conserver, dès lors qu'il est construit, la surface minimale pour construire, est une limitation du droit de construire en cas de détachement de parcelles qui ne pouvait légalement se fonder que sur l'article L. 111-5 du code de l'urbanisme dans sa rédaction antérieure à la loi n° 2000-1208 du 13 décembre 2000 relative à la solidarité et au renouvellement urbains (SRU), et non sur l'article L. 123-1 du même code. Par suite, l'abrogation de l'article L. 111-5 par la loi SRU a eu pour effet de rendre la règle inopposable, y compris après le rétablissement d'une version modifiée de cet article par la loi n° 2003-590 du 2 juillet 2003.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
