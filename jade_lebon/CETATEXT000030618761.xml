<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030618761</ID>
<ANCIEN_ID>JG_L_2015_05_000000380726</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/61/87/CETATEXT000030618761.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème / 10ème SSR, 20/05/2015, 380726</TITRE>
<DATE_DEC>2015-05-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>380726</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème / 10ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GHESTIN</AVOCATS>
<RAPPORTEUR>Mme Marie-Gabrielle Merloz</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Astrid Nicolazo de Barmon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:380726.20150520</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 28 mai 2014 au secrétariat du contentieux du Conseil d'Etat, la société Pneutech SAS demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite de rejet résultant du silence gardé par le Premier ministre sur sa demande du 3 février 2014 tendant à l'abrogation partielle du décret n° 2000-1196 du 6 décembre 2000 fixant, par catégories d'installations, les limites de puissance des installations pouvant bénéficier de l'obligation d'achat d'électricité ; <br/>
<br/>
              2°) d'annuler, pour excès de pouvoir, la décision implicite de rejet résultant du silence gardé par le ministre de l'écologie, du développement durable et de l'énergie sur sa demande tendant à l'abrogation partielle de l'arrêté du 27 janvier 2011 fixant les conditions d'achat d'électricité produite par les installations utilisant à titre principal l'énergie dégagée par la combustion de matières non fossiles d'origine végétale ou animale, telles que visées au 4° de l'article 2 du décret n° 2000-1196 du 6 décembre 2000 ;<br/>
<br/>
              3°) d'enjoindre au Premier ministre, sur le fondement de l'article L. 911-1 du code de justice administrative et dans un délai de trois mois à compter de la notification de la décision à intervenir, d'abroger les termes suivants du 4° de l'article 2 du décret du 6 décembre 2000 : " à titre principal " et " un arrêté du ministre chargé de l'énergie fixe les limites dans lesquelles ces installations peuvent utiliser une fraction d'énergie non renouvelable " ;<br/>
<br/>
              4°) d'enjoindre, sur le même fondement, au ministre de l'écologie, du développement durable et de l'énergie d'abroger, dans un délai de trois mois à compter de la notification de la décision à intervenir, les termes " à titre principal " de l'article 1er de l'arrêté du 27 janvier 2011 susmentionné et les cinq derniers alinéas de son annexe B.<br/>
<br/>
              5°) d'assortir ces injonctions, sur le fondement de l'article L. 911-3 du code de justice administrative, d'une astreinte de 1 000 euros par jour de retard pour chacune d'elles ;<br/>
<br/>
              6°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule ;<br/>
              - la directive 2009/28/CE du Parlement européen et du Conseil du 23 avril 2009 ;<br/>
              - le code de l'énergie ;<br/>
              - la loi n° 2000-108 du 10 février 2000, notamment son article 10 ;<br/>
              - le décret n° 2000-1196 du 6 décembre 2000 ;<br/>
              - le décret n° 2001-410 du 10 mai 2001 ;<br/>
              - l'arrêté du 27 janvier 2011 fixant les conditions d'achat de l'électricité produite par les installations utilisant à titre principal l'énergie dégagée par la combustion de matières non fossiles d'origine végétale ou animale telles que visées au 4° de l'article 2 du décret n° 2000-1196 du 6 décembre 2000 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie-Gabrielle Merloz, maître des requêtes,<br/>
<br/>
              - les conclusions de Mme Marie-Astrid Nicolazo de Barmon, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Ghestin, avocat de la société Pneutech SAS ;<br/>
<br/>
<br/>
<br/>1. Considérant que les dispositions du 2° de l'article 10 de la loi du 10 février 2000 relative à la modernisation et au développement du service public de l'électricité, aujourd'hui reprises à l'article L. 314-1 du code de l'énergie, prévoient que les producteurs intéressés qui en font la demande bénéficient d'une obligation d'achat de l'électricité produite par les installations qui utilisent des énergies renouvelables dont la puissance installée par site de production n'excède pas 12 mégawatts  ; que ces dispositions renvoient à un décret en Conseil d'Etat la fixation des limites de puissance installée pour chaque catégorie d'installations de production pouvant bénéficier de cette obligation d'achat ;<br/>
<br/>
              2. Considérant que, sur ce fondement, le 4° de l'article 2 du décret du 6 décembre 2000 fixant par catégorie d'installations les limites de puissance des installations pouvant bénéficier de l'obligation d'achat d'électricité ouvre le bénéfice de cette obligation, lorsque les conditions fixées par l'article 10 de la loi du 10 février 2000 sont réunies, aux installations, d'une puissance installée inférieure ou égale à 12 mégawatts, utilisant, à titre principal, l'énergie dégagée par la combustion ou l'explosion de matières non fossiles d'origine animale ou végétale ;<br/>
<br/>
              3. Considérant que, comme le prévoit l'article 8 du décret du 10 mai 2001 relatif aux conditions d'achat de l'électricité produite par des producteurs bénéficiant de l'obligation d'achat, un arrêté des ministres chargés de l'économie et de l'énergie du 27 janvier 2011 a fixé les conditions d'achat de l'électricité produite par les installations utilisant à titre principal l'énergie dégagée par la combustion de matières non fossiles d'origine végétale ou animale telles que visées au 4° de l'article 2 du décret du 6 décembre 2000 ; que l'annexe B de cet arrêté fixe la liste des ressources admissibles pour bénéficier du tarif de rachat d'électricité produite à partir de ces installations ; que la biomasse d'origine sylvicole est au nombre des ressources admissibles pour autant qu'elle représente une proportion minimale de 50 % ;<br/>
<br/>
              4. Considérant que par un courrier du 3 février 2014, la société Pneutech SAS a demandé au Premier ministre, d'abroger, d'une part, au 4° de l'article 2 du décret du 6 décembre 2000, les mots " à titre principal " et les mots " un arrêté du ministre chargé de l'énergie fixe les limites dans lesquelles ces installations peuvent utiliser une fraction d'énergie non renouvelable " et, d'autre part, à l'article 1er de l'arrêté du 27 janvier 2011, les mots " à titre principal " ainsi que les cinq derniers alinéas de l'annexe B de ce même arrêté relatifs à la biomasse d'origine sylvicole ; que, par un courrier du 28 février 2014, le Premier ministre a transmis ces demandes au ministre de l'écologie, du développement durable et de l'énergie ; que la société Pneutech SAS demande l'annulation pour excès de pouvoir des décisions implicites de rejet nées du silence gardé sur ces demandes ; <br/>
<br/>
              5. Considérant, en premier lieu, que la décision implicite qui résulte du silence gardé sur la demande de la société d'abroger certaines dispositions du 4° de l'article 2 du décret du 6 décembre 2000 doit être regardée, en tout état de cause, comme une décision de rejet prise par le Premier ministre ; que la société requérante n'est, par suite, pas fondée à soutenir que cette décision est entachée d'incompétence ;<br/>
<br/>
              6. Considérant, en deuxième lieu, que le refus d'engager la procédure d'abrogation de dispositions réglementaires adoptées après consultation préalable obligatoire d'un organisme n'implique pas une consultation de cet organisme ; que le moyen tiré de ce que la décision de rejet de la demande d'abrogation de certaines dispositions de l'arrêté du 27 janvier 2011 serait irrégulière faute de consultation préalable du Conseil supérieur de l'énergie et de la Commission de régulation de l'énergie doit, dès lors, être écarté ; <br/>
<br/>
              7. Considérant, en troisième lieu, que la société requérante fait valoir que le 4° de l'article 2 du décret du 6 décembre 2000, ainsi que l'article 1er et l'annexe B de l'arrêté du 27 janvier 2011 pris pour son application ont illégalement restreint le bénéfice de l'obligation d'achat prévue à l'article 10 de la loi du 10 février 2000 en imposant aux installations concernées que la part de biomasse dans le combustible soit prédominante ; <br/>
<br/>
              8. Considérant que, ainsi qu'il a été dit, l'article 10 de la loi du 10 février 2000, mentionné au point 1, renvoyait à un décret la fixation des limites de puissance installée pour chaque catégorie d'installations de production qui peuvent bénéficier de l'obligation d'achat, dans la limite de 12 mégawatts ; que, pour déterminer ainsi les limites de puissance installée, ce décret pouvait préciser les caractéristiques des installations concernées ; qu'ainsi, contrairement à ce que soutient la société requérante, le décret du 6 décembre 2000 n'a pas excédé l'habilitation donnée par la loi en réservant, au 4° de son article 2, aux installations utilisant à titre principal l'énergie dégagée par la combustion ou l'explosion de matières non fossiles d'origine animale ou végétale le bénéfice de l'obligation d'achat ;<br/>
<br/>
              9. Considérant que la circonstance que le dispositif d'obligation d'achat prévu à l'article 10 de la loi du 10 février 2000 ait pour objectif de promouvoir le recours à des énergies respectueuses de l'environnement n'implique pas qu'il doive s'appliquer automatiquement ni à toute installation utilisant des énergies renouvelables ni à des installations n'utilisant que partiellement ces énergies ; que, par suite, les dispositions critiquées du décret du 6 décembre 2000 et de l'arrêté du 27 janvier 2011 ne sauraient être regardées comme méconnaissant la volonté du législateur en ce qu'elles réservent le bénéfice de l'obligation d'achat à des installations recourant de manière prépondérante à une source d'énergie renouvelable ; qu'elles ne méconnaissent notamment pas les objectifs fixés aux alinéas un à quatre de l'article 1er de la loi du 10 février 2000, aujourd'hui repris au deuxième alinéa de l'article L. 121-1 du code de l'énergie, aux termes duquel le service public de l'électricité " contribue à l'indépendance et à la sécurité d'approvisionnement, à la qualité de l'air et à la lutte contre l'effet de serre, à la gestion optimale et au développement des ressources nationales, à la maîtrise de la demande d'énergie, à la compétitivité de l'activité économique et à la maîtrise des choix technologiques d'avenir, comme à l'utilisation rationnelle de l'énergie " ; <br/>
<br/>
              10. Considérant que la société requérante soutient par ailleurs que les dispositions litigieuses du décret du 6 décembre 2000 et de l'arrêté du 27 janvier 2011 méconnaissent  les objectifs et les dispositions de la directive 2009/28/CE du Parlement européen et du Conseil du 23 avril 2009 relative à la promotion de l'utilisation de l'énergie produite à partir de sources renouvelables ; que, cependant, cette directive ne prévoit aucun dispositif d'obligation d'achat au bénéfice des producteurs d'énergie utilisant des énergies renouvelables ; que, contrairement à ce que soutient la société requérante, il ne ressort pas des pièces du dossier que l'exigence d'une utilisation à titre principal de l'énergie dégagée par la combustion ou l'explosion de matières non fossiles d'origine animale ou végétale rendrait impossible pour la France le respect de l'objectif d'une proportion de 23 % d'énergies renouvelables dans la consommation totale d'énergie finale d'ici à 2020 ; qu'en outre, l'objectif de développement des ressources de biomasse existantes et d'exploitation de nouvelles ressources de biomasse pour des utilisations différentes, mentionné au considérant 19 et à l'article 4 de la directive, peut être poursuivi par d'autres mécanismes incitatifs ; que le plan d'action national pour les énergies renouvelables, pris en application de ces dispositions, n'a qu'un caractère indicatif ; qu'enfin, l'article 5 de la directive, dont la méconnaissance est invoquée, n'énonce qu'une règle de calcul de la part de l'énergie produite à partir de sources renouvelables afin de vérifier le respect des objectifs fixés ;<br/>
<br/>
              11. Considérant qu'aux termes de l'article 6 de la Charte de l'environnement, à laquelle le Préambule de la Constitution fait référence en vertu de la loi constitutionnelle du 1er mars 2005 : " Les politiques publiques doivent promouvoir un développement durable. A cet effet, elles concilient la protection et la mise en valeur de l'environnement, le développement économique et le progrès social " ; que le moyen tiré de ce que les dispositions litigieuses, qui ont pour effet de promouvoir les installations dont les émissions de gaz à effet de serre sont les plus limitées et qui sont les plus respectueuses de l'environnement, méconnaîtraient l'exigence de promotion du développement durable ne peut qu'être écarté ;<br/>
<br/>
              12. Considérant que la société requérante soutient enfin que les dispositions litigieuses méconnaissent le principe d'égalité dès lors qu'il n'y aurait aucune différence appréciable de situation entre un producteur d'électricité dont l'installation consomme une part supérieure à 50 % de biomasse et celui dont l'installation brûle des combustibles comportant une part de biomasse inférieure ; que, toutefois, la différence de traitement qui résulte de l'exigence d'une utilisation à titre principal de l'énergie dégagée par la combustion ou l'explosion de matières non fossiles d'origine animale ou végétale repose sur une différence de situation en rapport avec l'objet de la loi, tenant aux caractéristiques des combustibles utilisés et à leur impact sur l'environnement ; qu'il n'est pas établi, ni même allégué, qu'elle serait manifestement disproportionnée au regard des motifs qui la justifient ;<br/>
<br/>
              13. Considérant qu'il résulte de tout ce qui précède que la société Pneutech SAS n'est pas fondée à demander l'annulation pour excès de pouvoir des décisions attaquées ;<br/>
<br/>
              14. Considérant que la présente décision, qui rejette les conclusions de la société Pneutech SAS, n'appelle aucune mesure d'exécution ; qu'il ne peut, dès lors, être fait droit à ses conclusions aux  fins d'injonction ;<br/>
<br/>
              15. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
Article 1er : La requête de la société Pneutech SAS est rejetée.<br/>
Article 2 : La présente décision sera notifiée à la société Pneutech SAS et à la ministre de l'écologie, du développement durable et de l'énergie.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-02-01-04 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - COMPÉTENCE. LOI ET RÈGLEMENT. HABILITATIONS LÉGISLATIVES. - BÉNÉFICE DE L'OBLIGATION D'ACHAT DE L'ÉLECTRICITÉ PRODUITE PAR LES INSTALLATIONS QUI UTILISENT DES ÉNERGIES RENOUVELABLES (ART. 10 DE LA LOI DU 10 FÉVRIER 2000) - FIXATION DES LIMITES DE PUISSANCE INSTALLÉE POUR CHAQUE CATÉGORIE D'INSTALLATIONS RENVOYÉE À UN DÉCRET - POSSIBILITÉ POUR LE DÉCRET DE PRÉCISER LES CARACTÉRISTIQUES DES INSTALLATIONS CONCERNÉES - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">29-06-02-01 ENERGIE. MARCHÉ DE L'ÉNERGIE. TARIFICATION. ELECTRICITÉ. - BÉNÉFICE DE L'OBLIGATION D'ACHAT DE L'ÉLECTRICITÉ PRODUITE PAR LES INSTALLATIONS QUI UTILISENT DES ÉNERGIES RENOUVELABLES (ART. 10 DE LA LOI DU 10 FÉVRIER 2000) - FIXATION DES LIMITES DE PUISSANCE INSTALLÉE POUR CHAQUE CATÉGORIE D'INSTALLATIONS RENVOYÉE À UN DÉCRET - POSSIBILITÉ POUR LE DÉCRET DE PRÉCISER LES CARACTÉRISTIQUES DES INSTALLATIONS CONCERNÉES - EXISTENCE.
</SCT>
<ANA ID="9A"> 01-02-01-04 L'article 10 de la loi n° 2000-108 du 10 février 2000 relative à la modernisation et au développement du service public de l'électricité (aujourd'hui repris à l'article L. 314-1 du code de l'énergie) renvoyait à un décret la fixation des limites de puissance installée pour chaque catégorie d'installations de production qui peuvent bénéficier de l'obligation d'achat, dans la limite de 12 mégawatts. Pour déterminer ainsi les limites de puissance installée, ce décret pouvait préciser les caractéristiques des installations concernées. Ainsi, le décret n° 2000-108 du 6 décembre 2000 n'a pas excédé l'habilitation donnée par la loi en réservant, au 4° de son article 2, aux installations utilisant à titre principal l'énergie dégagée par la combustion ou l'explosion de matières non fossiles d'origine animale ou végétale le bénéfice de l'obligation d'achat.</ANA>
<ANA ID="9B"> 29-06-02-01 L'article 10 de la loi n° 2000-108 du 10 février 2000 relative à la modernisation et au développement du service public de l'électricité (aujourd'hui repris à l'article L. 314-1 du code de l'énergie) renvoyait à un décret la fixation des limites de puissance installée pour chaque catégorie d'installations de production qui peuvent bénéficier de l'obligation d'achat, dans la limite de 12 mégawatts. Pour déterminer ainsi les limites de puissance installée, ce décret pouvait préciser les caractéristiques des installations concernées. Ainsi, le décret n° 2000-108 du 6 décembre 2000 n'a pas excédé l'habilitation donnée par la loi en réservant, au 4° de son article 2, aux installations utilisant à titre principal l'énergie dégagée par la combustion ou l'explosion de matières non fossiles d'origine animale ou végétale le bénéfice de l'obligation d'achat.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
