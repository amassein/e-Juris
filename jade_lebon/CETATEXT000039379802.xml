<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039379802</ID>
<ANCIEN_ID>JG_L_2019_11_000000417855</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/37/98/CETATEXT000039379802.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 6ème chambres réunies, 13/11/2019, 417855</TITRE>
<DATE_DEC>2019-11-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>417855</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 6ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DE NERVO, POUPET ; SCP OHL, VEXLIARD</AVOCATS>
<RAPPORTEUR>M. Jean-Dominique Langlais</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2019:417855.20191113</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Mme B... C... a demandé au tribunal administratif de Lyon de condamner les hospices civils de Lyon à l'indemniser des préjudices qu'elle estime avoir subis du fait du refus de la réintégrer dans ses fonctions d'infirmière à la suite d'une mise en disponibilité pour convenances personnelles. Par une ordonnance n° 1500527 du 5 septembre 2017, le président de la 8ème chambre du tribunal administratif a donné acte du désistement de cette demande par application des dispositions de l'article R. 612-5-1 du code de justice administrative.<br/>
<br/>
              Par une ordonnance n° 17LY03798 du 4 décembre 2017, le président de la 3ème chambre de la cour administrative d'appel de Lyon a rejeté l'appel formé par Mme C... contre cette ordonnance. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 2 février et 30 avril 2018 et le 12 avril 2019 au secrétariat du contentieux du Conseil d'Etat, Mme C... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de mettre à la charge des Hospices civils de Lyon la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Langlais, maître des requêtes, <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP de Nervo, Poupet, avocat de Mme C... et à la SCP Ohl, Vexliard, avocat desHospices civils de Lyon ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article R. 612-5-1 du code de justice administrative : " Lorsque l'état du dossier permet de s'interroger sur l'intérêt que la requête conserve pour son auteur, le président de la formation de jugement ou le président de la chambre chargée de l'instruction, peut inviter le requérant à confirmer expressément le maintien de ses conclusions. La demande qui lui est adressée mentionne que, à défaut de réception de cette confirmation à l'expiration du délai fixé, qui ne peut être inférieur à un mois, il sera réputé s'être désisté de l'ensemble de ses conclusions ". <br/>
<br/>
              2. Il ressort des pièces du dossier soumis aux juges du fond que Mme C... a demandé au tribunal administratif de Lyon de condamner les Hospices civils de Lyon à lui verser une somme de 10 000 euros en réparation des préjudices qu'elle estimait avoir subis du fait du refus de la réintégrer dans ses fonctions d'infirmière. Elle demande l'annulation de l'ordonnance du 4 décembre 2017 par laquelle le président de la 3ème chambre de la cour administrative d'appel de Lyon a rejeté son appel dirigé contre l'ordonnance du 5 septembre 2017 par laquelle le président de la 8ème chambre du tribunal administratif a donné acte du désistement de sa demande par application des dispositions de l'article R. 612-5-1 du code de justice administrative.<br/>
<br/>
              Sur la compétence de la cour administrative d'appel : <br/>
<br/>
              3. Il ressort des pièces du dossier soumis aux juges du fond que la demande présentée par Mme C... au tribunal administratif de Lyon tendait au versement d'une indemnité de 10 000 euros. Par application des dispositions combinées des articles R. 811-1, R. 222-14 et R. 222-15 du code de justice administrative, dans leur rédaction alors applicable, cette demande était au nombre des litiges sur lesquels le tribunal administratif statue en premier et dernier ressort. La cour administrative d'appel de Lyon étant ainsi incompétente pour statuer par la voie de l'appel sur la requête dirigée contre l'ordonnance du 5 septembre 2017 du président de la 8ème chambre du tribunal administratif de Lyon, il a lieu d'annuler pour ce motif l'ordonnance du 4 décembre 2017 par laquelle le président de sa 3ème chambre a rejeté l'appel de Mme C.... <br/>
<br/>
              4. Il appartient au Conseil d'Etat de statuer, en tant que juge de cassation, sur les conclusions présentées par Mme C..., tant devant la cour administrative d'appel de Lyon que devant le Conseil d'Etat, contre l'ordonnance du 5 septembre 2017 du président de la 8ème chambre du tribunal administratif de Lyon.<br/>
<br/>
              Sur le pourvoi en cassation de Mme C... :<br/>
<br/>
              5. Il ressort des pièces du dossier de la procédure que, au cours de l'instruction de la demande de Mme C... devant le tribunal administratif de Lyon, celui-ci a, sur le fondement des dispositions de l'article R. 612-5-1 du code de justice administrative, demandé à la SCP Duval et Paris, avocat de Mme C... lors de l'introduction de sa demande, si cette demande conservait un intérêt pour sa cliente et en lui indiquant que, faute de réponse dans le délai imparti, elle serait réputée s'être désistée de ses conclusions. En réponse à cette demande, la SCP Duval et Paris a indiqué au tribunal administratif que Mme C... était désormais représentée par un nouvel avocat, Me A.... Le tribunal administratif a réitéré auprès de cet avocat la demande faite sur le fondement de l'article R. 612-5-1, par la voie de l'application Télérecours le 17 février 2017. Me A... ayant accusé réception de ce courrier le 27 juillet 2017 sans lui donner aucune suite et le délai imparti étant échu, le président de la 8ème chambre du tribunal administratif de Lyon a, par l'ordonnance attaquée du 5 septembre 2017, donné acte à Mme C... de son désistement.<br/>
<br/>
              6. En vertu de l'article R. 431-1 du code de justice administrative, lorsqu'une partie est représentée devant le tribunal administratif ou la cour administrative d'appel par un des mandataires mentionnés à l'article R. 431-2 du même code, c'est-à-dire par un avocat ou par un avocat au Conseil d'Etat et à la Cour de cassation, les actes de procédure, à l'exception de la notification de la décision prévue aux articles R. 751-3 et suivants, ne sont accomplis qu'à l'égard de ce mandataire. <br/>
<br/>
              7. En premier lieu, il ressort des pièces du dossier soumis aux juges du fond et n'est d'ailleurs pas contesté par Mme C... que Me A... était bien son mandataire à la date à laquelle a été adressé à ce dernier le courrier du 17 février 2017 l'invitant, sur le fondement de l'article R. 612-5-1 du code de justice administrative, à confirmer que la demande de sa cliente conservait pour elle un intérêt. Dans ces conditions, sans qu'ait d'incidence la circonstance que le tribunal administratif de Lyon ne se serait pas assuré directement auprès de Mme C... de l'identité de son nouveau mandataire, la requérante n'est pas fondée à soutenir que l'ordonnance qu'elle attaque serait, faute que le courrier du 17 février 2017 lui ait également été personnellement adressé, entachée d'irrégularité.<br/>
<br/>
              8. En deuxième lieu, si l'ordonnance attaquée porte la mention erronée que le courrier invitant Me A... à indiquer si la demande conservait un intérêt pour sa cliente lui aurait été adressé le 9 février, et non le 17 février, cette erreur de plume n'a pas d'incidence sur le bien-fondé de cette ordonnance.<br/>
<br/>
              9. Enfin, il ne ressort pas des pièces du dossier du tribunal administratif que le président de sa 8ème chambre du tribunal administratif aurait, en l'espèce, fait un usage abusif de la faculté ouverte par l'article R. 612-5-1 du code de justice administrative en donnant acte à Mme C... du désistement de sa demande.<br/>
<br/>
              10. Le pourvoi de Mme C... doit, par suite, être rejeté, y compris, par voie de conséquence, les conclusions présentées au titre de l'article L.761-1 du code de justice administrative. Dans les circonstances de l'espèce, il n'y a pas lieu de mettre à la charge de Mme C... la somme que demandent, au même titre, les Hospices civils de Lyon.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'ordonnance du 4 décembre 2017 du président de la 3ème chambre de la cour administrative d'appel de Lyon est annulée.<br/>
<br/>
Article 2 : Le pourvoi dirigé par Mme C... contre l'ordonnance du président de la 8ème chambre du tribunal administratif de Lyon est rejeté.<br/>
<br/>
Article 3 : Les conclusions présentées par les Hospices civils de Lyon au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 4 : La présente décision sera notifiée à Mme B... C... et aux Hospices civils de Lyon. <br/>
              .<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-01-05 PROCÉDURE. INTRODUCTION DE L'INSTANCE. QUALITÉ POUR AGIR. - REPRÉSENTATION PAR UN MANDATAIRE - 1) PRINCIPE - ACTES DE PROCÉDURE ACCOMPLIS À L'ÉGARD DU SEUL MANDATAIRE (ART. R. 431-1 DU CJA) [RJ1] - 2) APPLICATION - INVITATION À CONFIRMER, SOUS PEINE DE DÉSISTEMENT D'OFFICE, LE MAINTIEN DES CONCLUSIONS D'UNE REQUÊTE (ART. R. 612-5-1 DU CJA) - INVITATION DEVANT ÊTRE ADRESSÉE AU SEUL MANDATAIRE DU REQUÉRANT [RJ2] - RÉGULARITÉ DE L'ORDONNANCE DONNANT ACTE DU DÉSISTEMENT EN CAS D'ABSENCE DE RÉPONSE DE CE MANDATAIRE - EXISTENCE, SANS QU'AIT D'INCIDENCE LA CIRCONSTANCE QUE LE JUGE NE SE SERAIT PAS ASSURÉ DIRECTEMENT AUPRÈS DU REQUÉRANT DE L'IDENTITÉ DE SON MANDATAIRE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-01-08-02 PROCÉDURE. INTRODUCTION DE L'INSTANCE. FORMES DE LA REQUÊTE. MINISTÈRE D'AVOCAT. - REPRÉSENTATION PAR UN MANDATAIRE - 1) PRINCIPE - ACTES DE PROCÉDURE ACCOMPLIS À L'ÉGARD DU SEUL MANDATAIRE (ART. R. 431-1 DU CJA) [RJ1] - 2) APPLICATION - INVITATION À CONFIRMER, SOUS PEINE DE DÉSISTEMENT D'OFFICE, LE MAINTIEN DES CONCLUSIONS D'UNE REQUÊTE (ART. R. 612-5-1 DU CJA) - INVITATION DEVANT ÊTRE ADRESSÉE AU SEUL MANDATAIRE DU REQUÉRANT [RJ2] - RÉGULARITÉ DE L'ORDONNANCE DONNANT ACTE DU DÉSISTEMENT EN CAS D'ABSENCE DE RÉPONSE DE CE MANDATAIRE - EXISTENCE, SANS QU'AIT D'INCIDENCE LA CIRCONSTANCE QUE LE JUGE NE SE SERAIT PAS ASSURÉ DIRECTEMENT AUPRÈS DU REQUÉRANT DE L'IDENTITÉ DE SON MANDATAIRE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-05-04-03 PROCÉDURE. INCIDENTS. DÉSISTEMENT. DÉSISTEMENT D'OFFICE. - INVITATION À CONFIRMER, SOUS PEINE DE DÉSISTEMENT D'OFFICE, LE MAINTIEN DES CONCLUSIONS D'UNE REQUÊTE (ART. R. 612-5-1 DU CJA) - INVITATION DEVANT ÊTRE ADRESSÉE AU SEUL MANDATAIRE DU REQUÉRANT [RJ2] - RÉGULARITÉ DE L'ORDONNANCE DONNANT ACTE DU DÉSISTEMENT EN CAS D'ABSENCE DE RÉPONSE DE CE MANDATAIRE - EXISTENCE, SANS QU'AIT D'INCIDENCE LA CIRCONSTANCE QUE LE JUGE NE SE SERAIT PAS ASSURÉ DIRECTEMENT AUPRÈS DU REQUÉRANT DE L'IDENTITÉ DE SON MANDATAIRE.
</SCT>
<ANA ID="9A"> 54-01-05 1) En vertu de l'article R. 431-1 du code de justice administrative (CJA), lorsqu'une partie est représentée devant le tribunal administratif ou la cour administrative d'appel par un des mandataires mentionnés à l'article R. 431-2 du même code, c'est-à-dire par un avocat ou par un avocat au Conseil d'Etat et à la Cour de cassation, les actes de procédure, à l'exception de la notification de la décision prévue aux articles R. 751-3 et suivants, ne sont accomplis qu'à l'égard de ce mandataire.,,,2) Il ressort des pièces du dossier soumis aux juges du fond et n'est d'ailleurs pas contesté par le requérant que l'avocat contacté était bien son mandataire à la date à laquelle a été adressé à ce dernier le courrier l'invitant, sur le fondement de l'article R. 612-5-1 du CJA, à confirmer que la demande de son client conservait pour lui un intérêt. Dans ces conditions, sans qu'ait d'incidence la circonstance que le tribunal administratif ne se serait pas assuré directement auprès du requérant de l'identité de son nouveau mandataire, le requérant n'est pas fondé à soutenir que l'ordonnance qu'il attaque serait, faute que le courrier lui ait également été personnellement adressé, entachée d'irrégularité.</ANA>
<ANA ID="9B"> 54-01-08-02 1) En vertu de l'article R. 431-1 du code de justice administrative (CJA), lorsqu'une partie est représentée devant le tribunal administratif ou la cour administrative d'appel par un des mandataires mentionnés à l'article R. 431-2 du même code, c'est-à-dire par un avocat ou par un avocat au Conseil d'Etat et à la Cour de cassation, les actes de procédure, à l'exception de la notification de la décision prévue aux articles R. 751-3 et suivants, ne sont accomplis qu'à l'égard de ce mandataire.,,,2) Il ressort des pièces du dossier soumis aux juges du fond et n'est d'ailleurs pas contesté par le requérant que l'avocat contacté était bien son mandataire à la date à laquelle a été adressé à ce dernier le courrier l'invitant, sur le fondement de l'article R. 612-5-1 du CJA, à confirmer que la demande de son client conservait pour lui un intérêt. Dans ces conditions, sans qu'ait d'incidence la circonstance que le tribunal administratif ne se serait pas assuré directement auprès du requérant de l'identité de son nouveau mandataire, le requérant n'est pas fondé à soutenir que l'ordonnance qu'il attaque serait, faute que le courrier lui ait également été personnellement adressé, entachée d'irrégularité.</ANA>
<ANA ID="9C"> 54-05-04-03 En vertu de l'article R. 431-1 du code de justice administrative (CJA), lorsqu'une partie est représentée devant le tribunal administratif ou la cour administrative d'appel par un des mandataires mentionnés à l'article R. 431-2 du même code, c'est-à-dire par un avocat ou par un avocat au Conseil d'Etat et à la Cour de cassation, les actes de procédure, à l'exception de la notification de la décision prévue aux articles R. 751-3 et suivants, ne sont accomplis qu'à l'égard de ce mandataire.,,,Il ressort des pièces du dossier soumis aux juges du fond et n'est d'ailleurs pas contesté par le requérant que l'avocat contacté était bien son mandataire à la date à laquelle a été adressé à ce dernier le courrier l'invitant, sur le fondement de l'article R. 612-5-1 du CJA, à confirmer que la demande de son client conservait pour lui un intérêt. Dans ces conditions, sans qu'ait d'incidence la circonstance que le tribunal administratif ne se serait pas assuré directement auprès du requérant de l'identité de son nouveau mandataire, le requérant n'est pas fondé à soutenir que l'ordonnance qu'il attaque serait, faute que le courrier lui ait également été personnellement adressé, entachée d'irrégularité.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 21 février 2000, Ville d'Annecy, n° 196405, p. 67.,,[RJ2] Cf. décision du même jour, M. Cissé, n° 422938, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
