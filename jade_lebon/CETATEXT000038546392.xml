<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038546392</ID>
<ANCIEN_ID>JG_L_2019_06_000000422873</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/54/63/CETATEXT000038546392.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Section, 03/06/2019, 422873, Publié au recueil Lebon</TITRE>
<DATE_DEC>2019-06-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>422873</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Section</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ZRIBI, TEXIER</AVOCATS>
<RAPPORTEUR>M. Arnaud Skzryerbak</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESEC:2019:422873.20190603</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Montpellier d'annuler la décision du 6 avril 2017 par laquelle le préfet de l'Hérault a refusé de lui délivrer une carte de stationnement pour personnes handicapées et d'enjoindre au préfet de lui délivrer cette carte. Par un jugement n° 1702674 du 17 juillet 2018, le tribunal administratif de Montpellier a rejeté sa demande.<br/>
<br/>
              Par une ordonnance n° 18MA03539 du 31 juillet 2018, enregistrée le même jour au secrétariat du contentieux du Conseil d'Etat, le président de la cour administrative d'appel de Marseille a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, le pourvoi, enregistré le 26 juillet 2018 au greffe de cette cour, présenté par M. A.... Par ce pourvoi et par un mémoire, enregistré le 4 janvier 2019 au secrétariat du contentieux du Conseil d'Etat, M. A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 1 800 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'action sociale et des familles ;<br/>
              - la loi n° 2015-1776 du 28 décembre 2015 ;<br/>
              - la loi n° 2016-1321 du 7 octobre 2016 ;<br/>
              - l'arrêté du 3 janvier 2017 relatif aux modalités d'appréciation d'une mobilité pédestre réduite et de la perte d'autonomie dans le déplacement individuel, prévues aux articles R. 241-12-1 et R. 241-20-1 du code de l'action sociale et des familles ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Arnaud Skzryerbak, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Zribi et Texier, avocat de M. A...;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que M. A...a sollicité auprès de la maison départementale des personnes handicapées de l'Hérault l'attribution d'une carte de stationnement pour personnes handicapées. Par une décision du 6 avril 2017, prise au vu de l'évaluation de sa situation par le médecin de l'équipe pluridisciplinaire de la maison départementale, le préfet de l'Hérault a rejeté sa demande. M. A...se pourvoit en cassation contre le jugement du 17 juillet 2018 par lequel le tribunal administratif de Montpellier a rejeté son recours dirigé contre cette décision.<br/>
<br/>
              2. Aux termes des premier, quatrième et cinquième alinéas de l'article L. 241-3-2 du code de l'action sociale et des familles, dans sa rédaction issue de la loi du 28 décembre 2015 relative à l'adaptation de la société au vieillissement : " Toute personne (...) atteinte d'un handicap qui réduit de manière importante et durable sa capacité et son autonomie de déplacement à pied ou qui impose qu'elle soit accompagnée par une tierce personne dans ses déplacements, peut recevoir une carte de stationnement pour personnes handicapées. Cette carte est délivrée par le représentant de l'Etat dans le département conformément à l'avis du médecin chargé de l'instruction de la demande dans un délai de deux mois suivant la demande. A défaut de réponse du représentant de l'Etat dans le département dans ce délai, la carte est délivrée au demandeur " et " La carte de stationnement pour personnes handicapées permet à son titulaire ou à la tierce personne l'accompagnant d'utiliser, à titre gratuit et sans limitation de la durée de stationnement, toutes les places de stationnement ouvertes au public. Toutefois, les autorités compétentes en matière de circulation et de stationnement peuvent fixer une durée maximale de stationnement qui ne peut être inférieure à douze heures. La carte de stationnement permet, dans les mêmes conditions, de bénéficier des autres dispositions qui peuvent être prises en faveur des personnes handicapées par les autorités compétentes en matière de circulation et de stationnement. / Les mêmes autorités peuvent également prévoir que, pour les parcs de stationnement disposant de bornes d'entrée et de sortie accessibles aux personnes handicapées depuis leur véhicule, les titulaires de cette carte sont soumis au paiement de la redevance de stationnement en vigueur ". La loi du 7 octobre 2016 pour une République numérique a substitué à cette carte, par des dispositions insérées à l'article L. 241-3 du même code et entrées en vigueur le 1er janvier 2017, la carte " mobilité inclusion " mention " stationnement pour personnes handicapées ", délivrée par le président du conseil départemental au vu, en principe, de l'appréciation de la commission des droits et de l'autonomie des personnes handicapées, qui est subordonnée aux mêmes conditions et ouvre droit aux mêmes avantages. Le X de l'article 107 de la loi du 7 octobre 2016 a toutefois prévu, à titre transitoire, que des cartes de stationnement pourraient être délivrées, en tant que de besoin, jusqu'au 1er juillet 2017, sur le fondement des dispositions de l'article L. 241-3-2 du code de l'action sociale et des familles dans leur rédaction antérieure à cette loi.<br/>
<br/>
              3. Lorsqu'il statue sur un recours dirigé contre une décision par laquelle l'administration, sans remettre en cause des versements déjà effectués, détermine les droits d'une personne en matière d'aide ou d'action sociale, de logement ou au titre des dispositions en faveur des travailleurs privés d'emploi, et sous réserve du contentieux du droit au logement opposable, il appartient au juge administratif, eu égard tant à la finalité de son intervention qu'à sa qualité de juge de plein contentieux, non de se prononcer sur les éventuels vices propres de la décision attaquée, mais d'examiner les droits de l'intéressé, en tenant compte de l'ensemble des circonstances de fait qui résultent de l'instruction et, notamment, du dossier qui lui est communiqué en application de l'article R. 772-8 du code de justice administrative. Au vu de ces éléments, il lui appartient d'annuler ou de réformer, s'il y a lieu, cette décision, en fixant alors lui-même tout ou partie des droits de l'intéressé et en le renvoyant, au besoin, devant l'administration afin qu'elle procède à cette fixation pour le surplus, sur la base des motifs de son jugement. Dans le cas d'un contentieux portant sur une demande de carte de stationnement pour personnes handicapées ou de carte " mobilité inclusion " mention " stationnement pour personnes handicapées ", c'est au regard des dispositions applicables et de la situation de fait existant à la date à laquelle il rend sa propre décision que le juge doit statuer.<br/>
<br/>
              4. Il résulte des termes du jugement attaqué que le tribunal administratif de Montpellier a estimé que la demande formée par M. A... devant lui, dirigée contre le refus du préfet de l'Hérault de lui attribuer la carte de stationnement pour personnes handicapées qu'il sollicitait, relevait du contentieux de l'excès de pouvoir. En statuant ainsi, le tribunal a méconnu son office.<br/>
<br/>
              5. Il résulte de ce qui précède, sans qu'il soit besoin d'examiner les moyens du pourvoi, que le jugement attaqué doit être annulé.<br/>
<br/>
              6. Dans les circonstances de l'espèce, il y a lieu de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              7. D'une part, aux termes du IV de l'article R. 241-12-1 du code de l'action sociale et des familles : " Pour l'attribution de la mention " stationnement pour personnes handicapées ", un arrêté des ministres chargés des personnes handicapées, des personnes âgées et des anciens combattants définit les modalités d'appréciation d'une mobilité pédestre réduite et de la perte d'autonomie dans le déplacement individuel, en tenant compte notamment de la limitation du périmètre de marche de la personne ou de la nécessité pour celle-ci de recourir systématiquement à certaines aides techniques ou à une aide humaine lors de tous ses déplacements à l'extérieur ". Le premier alinéa de l'article R. 241-15 du même code précise que : " La carte mobilité inclusion peut être attribuée à titre définitif ou à durée déterminée, dans ce cas cette dernière ne peut être inférieure à un an, ni excéder vingt ans ". L'annexe à l'arrêté du 3 janvier 2017 relatif aux modalités d'appréciation d'une mobilité pédestre réduite et de la perte d'autonomie dans le déplacement individuel, prévues aux articles R. 241-12-1 et R. 241-20-1 du code de l'action sociale et des familles dispose que : " 1. Critère relatif à la réduction importante de la capacité et de l'autonomie de déplacement à pied : / La capacité et l'autonomie de déplacement à pied s'apprécient à partir de l'activité relative aux déplacements à l'extérieur. / Une réduction importante de la capacité et de l'autonomie de déplacement à pied correspond à une difficulté grave dans la réalisation de cette activité et peut se retrouver chez des personnes présentant notamment un handicap lié à des déficiences motrices ou viscérales (exemple : insuffisance cardiaque ou respiratoire). / Ce critère est rempli dans les situations suivantes : / - la personne a un périmètre de marche limité et inférieur à 200 mètres (...) / 3. Dispositions communes : / La réduction de la capacité et de l'autonomie de déplacement à pied ou le besoin d'accompagnement doit être définitif ou d'une durée prévisible d'au moins un an pour attribuer la mention " stationnement pour personnes handicapées " de la carte mobilité inclusion ou la carte de stationnement pour personnes handicapées. Il n'est cependant pas nécessaire que l'état de la personne soit stabilisé. / Lorsque les troubles à l'origine des difficultés de déplacement ont un caractère évolutif, la durée d'attribution de cette carte tient compte de l'évolutivité potentielle de ceux-ci ".<br/>
<br/>
              8. D'autre part, aux termes de l'article R. 772-8 du code de justice administrative, applicable aux requêtes relatives aux prestations, allocations ou droits attribués au titre de l'aide ou de l'action sociale, du logement ou en faveur des travailleurs privés d'emploi : " Lorsque la requête lui est notifiée, le défendeur est tenu de communiquer au tribunal administratif l'ensemble du dossier constitué pour l'instruction de la demande tendant à l'attribution de la prestation ou de l'allocation ou à la reconnaissance du droit, objet de la requête. / Lorsque ce dossier est, pour partie, constitué de pièces médicales concernant le requérant, le tribunal peut enjoindre au défendeur de communiquer ces pièces à celui-ci afin de le mettre en mesure de les communiquer lui-même au tribunal ". <br/>
<br/>
              9. M. A...soutient qu'il est atteint d'un handicap qui limite son périmètre de marche à moins de deux cents mètres, en produisant, outre la notification de la décision de la commission des droits et de l'autonomie des personnes handicapée du 6 avril 2017 évaluant son incapacité à un taux égal ou supérieur à 80 %, en vue du renouvellement de l'allocation aux adultes handicapés jusqu'au 30 juin 2021, des certificats du médecin cardiologue et du médecin généraliste qui le suivent, le dernier étant daté du 6 mai 2019, dont il résulte qu'il présente, à la suite d'un infarctus d'origine embolique, une cardiopathie ischémique sévère entraînant un essoufflement et une accélération du rythme cardiaque au moindre effort et nécessitant qu'il puisse se garer au plus près de l'endroit où il se rend lorsqu'il doit se déplacer. Invité par courrier du 9 juin 2017, dans un délai de trente jours, à produire des observations en défense à la requête de M. A... et à communiquer au tribunal, en application des dispositions de l'article R. 772-8 du code de justice administrative, l'ensemble du dossier constitué pour l'instruction de la demande du requérant, en lui transmettant, le cas échéant, les pièces médicales le concernant pour le mettre en mesure de les communiquer lui-même au tribunal, puis mis en demeure, par un nouveau courrier du 31 juillet 2017, de produire ses observations dans un délai de trente jours, le préfet de l'Hérault n'a pas produit de mémoire et ne s'est pas fait représenter à l'audience. La ministre des solidarités et de la santé et le département de l'Hérault n'ont pas produit en défense devant le Conseil d'Etat.<br/>
<br/>
              10. Eu égard, d'une part, aux éléments circonstanciés produits par M. A...et, d'autre part, à l'absence de communication, par l'administration, du dossier constitué pour l'instruction de la demande, il y a lieu de reconnaître le droit de M. A...à la carte " mobilité inclusion " mention " stationnement pour personnes handicapées " pour une durée qui doit être fixée, dans les circonstances de l'espèce, à un an, et, en conséquence, d'annuler la décision du 6 avril 2017 par laquelle le préfet de l'Hérault a rejeté sa demande. La présente décision implique la délivrance de cette carte par le président du conseil départemental de l'Hérault dans un délai de quinze jours à compter de la notification de la présente décision.<br/>
<br/>
              11. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 1 800 euros que M. A...demande, sur le fondement de l'article L. 761-1 du code de justice administrative, au titre des frais exposés par lui et non compris dans les dépens.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le jugement du tribunal administratif de Montpellier du 17 juillet 2018 est annulé.<br/>
Article 2 : La décision du préfet de l'Hérault du 6 avril 2017 est annulée.<br/>
Article 3 : M. A...a droit à la carte " mobilité inclusion " mention " stationnement pour personnes handicapées " pour une durée d'un an. Cette carte lui sera délivrée par le président du conseil départemental de l'Hérault dans un délai de quinze jours à compter de la notification de la présente décision.<br/>
Article 4 : L'Etat versera à M. A...la somme de 1 800 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 5 : La présente décision sera notifiée à M. B... A..., à la ministre des solidarités et de la santé et au département de l'Hérault.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">04-04 AIDE SOCIALE. CONTENTIEUX DE L'AIDE SOCIALE ET DE LA TARIFICATION. - 1) RECOURS DIRIGÉ CONTRE UNE DÉCISION DE L'ADMINISTRATION DÉTERMINANT LES DROITS D'UNE PERSONNE EN MATIÈRE D'AIDE OU D'ACTION SOCIALE, DE LOGEMENT OU AU TITRE DES DISPOSITIONS EN FAVEUR DES TRAVAILLEURS PRIVÉS D'EMPLOI, SANS REMETTRE EN CAUSE DES VERSEMENTS DÉJÀ EFFECTUÉS - RECOURS DE PLEIN CONTENTIEUX - OFFICE DU JUGE - EXAMEN LIMITÉ AUX DROITS DE L'INTÉRESSÉ SANS EXAMEN D'ÉVENTUELS VICES PROPRES DE LA DÉCISION [RJ1] - 2) APPLICATION AU CONTENTIEUX PORTANT SUR UNE DEMANDE DE CARTE DE STATIONNEMENT POUR PERSONNES HANDICAPÉES - JUGE STATUANT AU REGARD DES DISPOSITIONS APPLICABLES ET DE LA SITUATION DE FAIT EXISTANT À LA DATE À LAQUELLE IL REND SA PROPRE DÉCISION.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-02-02-01 PROCÉDURE. DIVERSES SORTES DE RECOURS. RECOURS DE PLEIN CONTENTIEUX. RECOURS AYANT CE CARACTÈRE. - 1) RECOURS DIRIGÉ CONTRE UNE DÉCISION DE L'ADMINISTRATION DÉTERMINANT LES DROITS D'UNE PERSONNE EN MATIÈRE D'AIDE OU D'ACTION SOCIALE, DE LOGEMENT OU AU TITRE DES DISPOSITIONS EN FAVEUR DES TRAVAILLEURS PRIVÉS D'EMPLOI, SANS REMETTRE EN CAUSE DES VERSEMENTS DÉJÀ EFFECTUÉS - OFFICE DU JUGE - EXAMEN LIMITÉ AUX DROITS DE L'INTÉRESSÉ SANS EXAMEN D'ÉVENTUELS VICES PROPRES DE LA DÉCISION [RJ1] - 2) APPLICATION AU CONTENTIEUX PORTANT SUR UNE DEMANDE DE CARTE DE STATIONNEMENT POUR PERSONNES HANDICAPÉES - JUGE STATUANT AU REGARD DES DISPOSITIONS APPLICABLES ET DE LA SITUATION DE FAIT EXISTANT À LA DATE À LAQUELLE IL REND SA PROPRE DÉCISION.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-07-03 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. POUVOIRS DU JUGE DE PLEIN CONTENTIEUX. - 1) RECOURS DIRIGÉ CONTRE UNE DÉCISION DE L'ADMINISTRATION DÉTERMINANT LES DROITS D'UNE PERSONNE EN MATIÈRE D'AIDE OU D'ACTION SOCIALE, DE LOGEMENT OU AU TITRE DES DISPOSITIONS EN FAVEUR DES TRAVAILLEURS PRIVÉS D'EMPLOI, SANS REMETTRE EN CAUSE DES VERSEMENTS DÉJÀ EFFECTUÉS - OFFICE DU JUGE - EXAMEN LIMITÉ AUX DROITS DE L'INTÉRESSÉ SANS EXAMEN D'ÉVENTUELS VICES PROPRES DE LA DÉCISION [RJ1] - 2) APPLICATION AU CONTENTIEUX PORTANT SUR UNE DEMANDE DE CARTE DE STATIONNEMENT POUR PERSONNES HANDICAPÉES - JUGE STATUANT AU REGARD DES DISPOSITIONS APPLICABLES ET DE LA SITUATION DE FAIT EXISTANT À LA DATE À LAQUELLE IL REND SA PROPRE DÉCISION.
</SCT>
<ANA ID="9A"> 04-04 1) Lorsqu'il statue sur un recours dirigé contre une décision par laquelle l'administration, sans remettre en cause des versements déjà effectués, détermine les droits d'une personne en matière d'aide ou d'action sociale, de logement ou au titre des dispositions en faveur des travailleurs privés d'emploi, et sous réserve du contentieux du droit au logement opposable, il appartient au juge administratif, eu égard tant à la finalité de son intervention qu'à sa qualité de juge de plein contentieux, non de se prononcer sur les éventuels vices propres de la décision attaquée, mais d'examiner les droits de l'intéressé, en tenant compte de l'ensemble des circonstances de fait qui résultent de l'instruction et, notamment, du dossier qui lui est communiqué en application de l'article R. 772-8 du code de justice administrative (CJA). Au vu de ces éléments, il lui appartient d'annuler ou de réformer, s'il y a lieu, cette décision, en fixant alors lui-même tout ou partie des droits de l'intéressé et en le renvoyant, au besoin, devant l'administration afin qu'elle procède à cette fixation pour le surplus, sur la base des motifs de son jugement.... ,,2) Dans le cas d'un contentieux portant sur une demande de carte de stationnement pour personnes handicapées ou de carte mobilité inclusion mention stationnement pour personnes handicapées, c'est au regard des dispositions applicables et de la situation de fait existant à la date à laquelle il rend sa propre décision que le juge doit statuer.</ANA>
<ANA ID="9B"> 54-02-02-01 1) Lorsqu'il statue sur un recours dirigé contre une décision par laquelle l'administration, sans remettre en cause des versements déjà effectués, détermine les droits d'une personne en matière d'aide ou d'action sociale, de logement ou au titre des dispositions en faveur des travailleurs privés d'emploi, et sous réserve du contentieux du droit au logement opposable, il appartient au juge administratif, eu égard tant à la finalité de son intervention qu'à sa qualité de juge de plein contentieux, non de se prononcer sur les éventuels vices propres de la décision attaquée, mais d'examiner les droits de l'intéressé, en tenant compte de l'ensemble des circonstances de fait qui résultent de l'instruction et, notamment, du dossier qui lui est communiqué en application de l'article R. 772-8 du code de justice administrative (CJA). Au vu de ces éléments, il lui appartient d'annuler ou de réformer, s'il y a lieu, cette décision, en fixant alors lui-même tout ou partie des droits de l'intéressé et en le renvoyant, au besoin, devant l'administration afin qu'elle procède à cette fixation pour le surplus, sur la base des motifs de son jugement.... ,,2) Dans le cas d'un contentieux portant sur une demande de carte de stationnement pour personnes handicapées ou de carte mobilité inclusion mention stationnement pour personnes handicapées, c'est au regard des dispositions applicables et de la situation de fait existant à la date à laquelle il rend sa propre décision que le juge doit statuer.</ANA>
<ANA ID="9C"> 54-07-03 1) Lorsqu'il statue sur un recours dirigé contre une décision par laquelle l'administration, sans remettre en cause des versements déjà effectués, détermine les droits d'une personne en matière d'aide ou d'action sociale, de logement ou au titre des dispositions en faveur des travailleurs privés d'emploi, et sous réserve du contentieux du droit au logement opposable, il appartient au juge administratif, eu égard tant à la finalité de son intervention qu'à sa qualité de juge de plein contentieux, non de se prononcer sur les éventuels vices propres de la décision attaquée, mais d'examiner les droits de l'intéressé, en tenant compte de l'ensemble des circonstances de fait qui résultent de l'instruction et, notamment, du dossier qui lui est communiqué en application de l'article R. 772-8 du code de justice administrative (CJA). Au vu de ces éléments, il lui appartient d'annuler ou de réformer, s'il y a lieu, cette décision, en fixant alors lui-même tout ou partie des droits de l'intéressé et en le renvoyant, au besoin, devant l'administration afin qu'elle procède à cette fixation pour le surplus, sur la base des motifs de son jugement.... ,,2) Dans le cas d'un contentieux portant sur une demande de carte de stationnement pour personnes handicapées ou de carte mobilité inclusion mention stationnement pour personnes handicapées, c'est au regard des dispositions applicables et de la situation de fait existant à la date à laquelle il rend sa propre décision que le juge doit statuer.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Section, décision du même jour, Mme,, n° 423001, à publier au Recueil ;  s'agissant du recours contre une décision déterminant les droits d'une personne au RMI, CE, Section, 27 juillet 2012, Mme,épouse,, n° 347114, p. 299 ; s'agissant du recours contre une décision déterminant les droits d'une personne au RSA, CE, Section, 16 décembre 2016, Mme,, n° 389642, p. 555.,Rappr., s'agissant du recours contre une décision refusant ou ne faisant que partiellement droit à une demande de remise gracieuse en matière d'aide ou d'action sociale, de logement ou au titre des dispositions en faveur des travailleurs privés d'emploi, CE, Section, décision du même jour, M.,, n° 415040, à publier au Recueil ; s'agissant du recours contre une décision refusant une prise en charge par le service de l'ASE, CE, Section, décision du même jour, Département de l'Oise, n° 419903, à publier au Recueil.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
