<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025562670</ID>
<ANCIEN_ID>JG_L_2012_03_000000355151</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/56/26/CETATEXT000025562670.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Section du Contentieux, 23/03/2012, 355151, Publié au recueil Lebon</TITRE>
<DATE_DEC>2012-03-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>355151</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Section du Contentieux</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ ; SCP PEIGNOT, GARREAU, BAUER-VIOLAS</AVOCATS>
<RAPPORTEUR>M. Nicolas Polge</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CESEC:2012:355151.20120323</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu l'arrêt n° 11MA03953, 11MA03954 du 19 décembre 2011, enregistré le 23 décembre 2011 au secrétariat du contentieux du Conseil d'Etat, par lequel la cour administrative d'appel de Marseille, avant de statuer sur les requêtes du CENTRE HOSPITALIER D'ALES-CEVENNES tendant, d'une part, à l'annulation du jugement n° 1102343 du 22 septembre 2011 par lequel le tribunal administratif de Nîmes a rejeté sa demande tendant à la récusation de M. Fabrice A, désigné à la demande de la société Sogea Sud et de la société Richard Satem comme expert par ordonnance n° 1101256 du 22 juin 2011 du juge des référés du tribunal administratif, avec mission notamment de déterminer les causes des retards du chantier de construction du nouvel hôpital d'Alès, à la récusation de M. A et à la désignation d'un autre expert et, d'autre part, à ce qu'il soit sursis à l'exécution de ce jugement, a décidé, par application des dispositions de l'article L. 113-1 du code de justice administrative, de transmettre le dossier de cette demande au Conseil d'Etat, en soumettant à son examen les questions suivantes :<br/>
<br/>
              1°) La décision par laquelle le tribunal administratif statue sur la demande de récusation d'un expert est-elle un jugement au sens de l'article L. 9 du code de justice administrative '<br/>
<br/>
              2°) Si tel est le cas, les caractéristiques particulières de l'action en récusation justifient-elles une dispense ou une atténuation de l'obligation de motivation '<br/>
<br/>
              3°) L'obligation de motivation d'une telle décision résulte-t-elle, par ailleurs, d'un principe général du droit ou des stipulations de l'article 6-1 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales '<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu le code de justice administrative, notamment son article L. 113-1 ;<br/>
<br/>
<br/>
<br/>
               Après avoir entendu en séance publique :<br/>
<br/>
- le rapport de M. Nicolas Polge, Maître des Requêtes ;<br/>
              - les observations de la SCP Lyon-Caen, Thiriez, avocat du CENTRE HOSPITALIER D'ALES-CEVENNES et les observations de la SCP Peignot, Garreau, Bauer-Violas, avocat de la SNC SOGEA SUD ;<br/>
<br/>
- les conclusions de M. Bertrand Dacosta, rapporteur public ;<br/>
              La parole ayant été à nouveau donnée à la SCP Lyon-Caen, Thiriez, avocat du CENTRE HOSPITALIER D'ALES-CEVENNES et à la SCP Peignot, Garreau, Bauer-Violas, avocat de la SNC SOGEA SUD ;<br/>
REND L'AVIS SUIVANT :<br/>
<br/>
<br/>
<br/>
              1. En vertu de l'article R. 621-6 du code de justice administrative, les experts ou sapiteurs peuvent être récusés par une demande présentée à la juridiction qui a ordonné l'expertise, pour les mêmes causes que les juges tenant en l'existence d'une raison sérieuse de douter de leur impartialité. Aux termes de l'article R. 621-6-4 de ce code : "Si l'expert acquiesce à la demande de récusation, il est aussitôt remplacé. Dans le cas contraire, la juridiction, par une décision non motivée, se prononce sur la demande, après audience publique dont l'expert et les parties sont averties. Sauf si l'expertise a été ordonnée sur le fondement du titre III du livre V, cette décision ne peut être contestée devant le juge d'appel ou de cassation qu'avec le jugement ou l'arrêt rendu ultérieurement. L'expert n'est pas admis à contester la décision qui le récuse".<br/>
<br/>
              La décision ainsi rendue par le tribunal ou la cour en audience publique, après que les parties en ont été averties dans un délai leur permettant  de présenter utilement  leurs observations, et qui peut être soit directement contestée en appel ou en cassation lorsque l'expert a été désigné au titre d'une expertise ordonnée par le juge des référés sur le fondement de l'article R. 531-1 ou R. 532-1 du code de justice administrative, soit avec le jugement ou l'arrêt rendu ultérieurement, est de nature juridictionnelle.<br/>
<br/>
              2. En précisant que le juge se prononce par une " décision non motivée ", l'article R. 621-6-4 du code de justice administrative n'a pas entendu écarter l'application de la règle générale de motivation des décisions juridictionnelles, rappelée à l'article L. 9 de ce code. Il a seulement entendu tenir compte des exigences d'une bonne administration de la justice ainsi que des particularités qui s'attachent à une demande de récusation, laquelle est notamment susceptible, selon la teneur de l'argumentation du requérant, de porter atteinte à la vie privée de l'expert ou de mettre en cause sa probité ou sa réputation professionnelle. Aussi appartient-il au juge d'adapter la motivation de sa décision, au regard de ces considérations, en se limitant, le cas échéant à énoncer qu'il y a lieu, ou qu'il n'y a pas lieu, de faire droit à la demande.<br/>
<br/>
              3. L'action en récusation d'un expert ne porte ni sur des droits et obligations de caractère civil, ni sur une accusation en matière pénale, au sens du paragraphe 1 de l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales. Par suite, la décision statuant sur cette action n'entre pas dans le champ d'application de ces stipulations.<br/>
<br/>
<br/>
<br/>
              Le présent avis sera notifié à la cour administrative d'appel de Marseille, au CENTRE HOSPITALIER D'ALES-CEVENNES, à la société Sogea Sud, à la société Richard Satem, à M. Fabrice A et au ministre de la justice et des libertés.<br/>
<br/>
              Il sera publié au Journal officiel de la République française.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">26-055-01-06-01 DROITS CIVILS ET INDIVIDUELS. CONVENTION EUROPÉENNE DES DROITS DE L'HOMME. DROITS GARANTIS PAR LA CONVENTION. DROIT À UN PROCÈS ÉQUITABLE (ART. 6). CHAMP D'APPLICATION. - DÉCISION RENDUE SUR UNE DEMANDE DE RÉCUSATION D'UN EXPERT (ART. R. 621-6-4 DU CJA) - EXCLUSION.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-05-02 PROCÉDURE. INCIDENTS. RÉCUSATION. - DÉCISION RENDUE SUR UNE DEMANDE DE RÉCUSATION D'UN EXPERT (ART. R. 621-6-4 DU CJA) - 1) NATURE JURIDICTIONNELLE - EXISTENCE - 2) EXIGENCE DE MOTIVATION  - EXISTENCE, SOUS RÉSERVE DES EXIGENCES D'UNE BONNE ADMINISTRATION DE LA JUSTICE ET DES PARTICULARITÉS D'UNE TELLE DEMANDE - 3) APPLICABILITÉ DE L'ARTICLE 6 § 1 DE LA CONV. EDH - ABSENCE.
</SCT>
<ANA ID="9A"> 26-055-01-06-01 L'action en récusation d'un expert ne porte ni sur des droits et obligations de caractère civil, ni sur une accusation en matière pénale, au sens du paragraphe 1 de l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales. Par suite, la décision statuant sur cette action n'entre pas dans le champ d'application de ces stipulations.</ANA>
<ANA ID="9B"> 54-05-02 1) La décision que rend un tribunal administratif ou une cour administrative d'appel en audience publique, en application de l'article R. 621-6-4 du code de justice administrative (CJA), sur une demande de récusation d'un expert, après que les parties en ont été averties dans un délai leur permettant de présenter utilement leurs observations, et qui peut être soit directement contestée en appel ou en cassation lorsque l'expert a été désigné au titre d'une expertise ordonnée par le juge des référés sur le fondement de l'article R. 531-1 ou R. 532-1 du code de justice administrative, soit avec le jugement ou l'arrêt rendu ultérieurement, est de nature juridictionnelle.,,2) En précisant que le juge se prononce par une « décision non motivée », l'article R. 621-6-4 du code de justice administrative n'a pas entendu écarter l'application de la règle générale de motivation des décisions juridictionnelles, rappelée à l'article L. 9 de ce code. Il a seulement entendu tenir compte des exigences d'une bonne administration de la justice ainsi que des particularités qui s'attachent à une demande de récusation, laquelle est notamment susceptible, selon la teneur de l'argumentation du requérant, de porter atteinte à la vie privée de l'expert ou de mettre en cause sa probité ou sa réputation professionnelle. Aussi appartient-il au juge d'adapter la motivation de sa décision, au regard de ces considérations, en se limitant, le cas échéant à énoncer qu'il y a lieu, ou qu'il n'y a pas lieu, de faire droit à la demande.,,3) L'action en récusation d'un expert ne porte ni sur des droits et obligations de caractère civil, ni sur une accusation en matière pénale, au sens du paragraphe 1 de l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales. Par suite, la décision statuant sur cette action n'entre pas dans le champ d'application de ces stipulations.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
