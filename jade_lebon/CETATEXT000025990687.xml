<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025990687</ID>
<ANCIEN_ID>JG_L_2012_06_000000359570</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/99/06/CETATEXT000025990687.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 08/06/2012, 359570</TITRE>
<DATE_DEC>2012-06-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>359570</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2012:359570.20120608</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 21 mai 2012 au secrétariat du contentieux du Conseil d'Etat, présentée par l'association FRANCE NATURE ENVIRONNEMENT, dont le siège est 10 rue Barbier au Mans (72000), représentée par son administrateur, et par l'association AGIR POUR LES PAYSAGES, dont le siège est 22 rue Meyrueis à Montpellier (34000), représentée par son président en exercice ; les associations demandent au juge des référés du Conseil d'Etat :<br/>
<br/>
              1°) de suspendre, sur le fondement des dispositions de l'article L. 521-1 du code de justice administrative, l'exécution du décret n° 2012-118 du 30 janvier 2012, d'une part, en ce qui concerne les articles R. 581-20 et R. 581-55 du code de l'environnement et, d'autre part, en tant qu'il abroge les règles énoncées par l'article R. 581-60 de ce code, dans sa rédaction antérieure au décret contesté ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement de la somme de 3 000 euros à chacune des associations requérantes au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              elles soutiennent que la requête est recevable dès lors que le décret contesté comporte des dispositions attentatoires à la protection des paysages et du cadre de vie qu'elles ont pour mission statutaire de défendre ; qu'il existe un doute sérieux quant à sa légalité ; qu'en ne fixant aucune limite de surface et de hauteur aux enseignes scellées au sol ou installées directement sur le sol, le décret contesté n'a pas défini les modalités d'application des conditions de la prévention des atteintes aux paysages et au cadre de vie par les enseignes, en méconnaissance de l'article 3 de la Charte de l'environnement et des dispositions du premier alinéa de l'article L. 581-18 et de l'article L. 581-1 du code de l'environnement ; qu'il est entaché d'une erreur manifeste d'appréciation au regard de l'article L. 581-2 de ce code ; qu'en prévoyant une procédure pour une autorisation d'implantation de bâche publicitaire, sans participation du public, le deuxième alinéa de l'article L. 581-9 du même code méconnaît l'article 7 de la Charte de l'environnement, d'où un défaut de base légale pour les articles R. 581-20 et R. 581-55, tels que modifiés par les articles 4 et 11 du décret contesté ; que la condition d'urgence est remplie dès lors qu'il résulte du décret contesté qu'aucune règle de format ne pourra plus s'appliquer aux enseignes scellées au sol ou installées directement sur le sol à compter du 1er juillet 2012, et que le rectificatif publié le 21 avril 2012 n'a pas corrigé cette suppression ; que le décret contesté porte une atteinte grave et immédiate aux intérêts qu'elles entendent défendre tant par ses dispositions relatives aux enseignes scellées au sol ou installées directement sur le sol que par celles concernant les emplacements de bâches publicitaires ; <br/>
<br/>
<br/>
              Vu le décret dont la suspension de l'exécution est demandée ;<br/>
<br/>
              Vu la copie de la requête à fin d'annulation ; <br/>
<br/>
              Vu le mémoire, enregistré le 21 mai 2012, présenté par les associations requérantes en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958 ; elles demandent au juge des référés du Conseil d'Etat de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des dispositions du deuxième alinéa de l'article L. 581-9 du code de l'environnement tel qu'issu du 2° de l'article 40 de la loi du 12 juillet 2010 ; elles soutiennent que ces dispositions, applicables au litige, n'ont pas fait l'objet d'un examen de constitutionnalité ; que la question de la méconnaissance par ces dispositions de l'article 7 de la Charte de l'environnement, du fait de l'absence de participation du public au processus d'autorisation d'emplacement d'une bâche publicitaire, est sérieuse ;<br/>
<br/>
              Vu le mémoire complémentaire, enregistré le 31 mai 2012, présenté par les associations requérantes, qui demandent en outre la suspension du décret, en tant qu'il instaure le troisième alinéa de l'article R. 581-64 du code de l'environnement et qu'il abroge, à l'ancien article R. 581-59, les mots " les enseignes de plus d'un mètre carré scellées au sol ou directement installées sur le sol sont limitées en nombre à un dispositif à double face ou deux dispositifs simples placés le long de chaque voie ouverte à la circulation publique bordant l'immeuble où est exercée l'activité signalée " ; elles soutiennent que les modifications contestées méconnaissent le principe d'intelligibilité de la norme et l'objectif de protection du cadre de vie garanti par l'article L. 581-2 de ce code ainsi que par l'article 3 de la Charte de l'environnement ; qu'elles sont illégales en l'absence de référence à l'importance des agglomérations ;<br/>
<br/>
              Vu le mémoire en défense, enregistré le 1er juin 2012, présenté par le ministre de l'écologie, du développement durable et de l'énergie qui conclut au rejet de la requête ; il soutient que les conclusions tendant à la suspension du premier alinéa de l'article R. 581-65 dans sa nouvelle rédaction sont irrecevables dès lors que les nouvelles numérotations d'articles par le décret litigieux forment un tout indivisible qu'il n'appartient pas au juge des référés de priver de cohérence ; que la condition d'urgence n'est pas remplie, aucune atteinte aux intérêts défendus par les associations requérantes n'étant établie et aucun préjudice grave ne résultant de l'incidence des dispositions litigieuses sur la protection du cadre de vie ; qu'à supposer qu'un préjudice puisse être causé, il est dénué de caractère immédiat ; qu'aucun doute sérieux n'entache la légalité du décret contesté ; que l'éventuelle méconnaissance de l'article 3 de la Charte de l'environnement résulte d'une erreur de renumérotation dont la rectification est en cours ; <br/>
<br/>
              Vu le mémoire, enregistré le 1er juin 2012, présenté par le même ministre ; il soutient qu'il n'y a pas lieu de saisir le Conseil constitutionnel de la question prioritaire de constitutionnalité invoquée, dès lors qu'il n'y a pas urgence ; que la question posée n'est pas nouvelle et ne présente pas de caractère sérieux ;<br/>
<br/>
              Vu les mémoires en réplique, enregistrés le 4 juin 2012, présentés par les associations requérantes, qui reprennent leurs conclusions précédentes et les mêmes moyens ; elles soutiennent en outre que les nouvelles dispositions portent atteinte à l'intérêt public et qu'aucune garantie n'est donnée qu'une modification interviendra en temps utile pour corriger l'erreur de renvoi dans l'article R. 581-65 du code de l'environnement ;<br/>
<br/>
              Vu le nouveau mémoire, enregistré le 4 juin 2012, présenté par le ministre de l'écologie, du développement durable et de l'énergie qui reprend les conclusions de son précédent mémoire et les mêmes moyens ; il soutient en outre que l'erreur matérielle entachant l'article R. 581-65 du code de l'environnement n'est pas de nature à en affecter la légalité ;<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu la Constitution, notamment son Préambule et son article 61-1 ;<br/>
<br/>
              Vu l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
<br/>
              Vu le code de l'environnement, modifié notamment par la loi n° 2010-788 du 12 juillet 2010 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, les associations FRANCE NATURE ENVIRONNEMENT et AGIR POUR LES PAYSAGES  et, d'autre part, le Premier ministre et le ministre de l'écologie, du développement durable et de l'énergie ; <br/>
<br/>
              Vu le procès-verbal de l'audience publique du 5 juin 2012 à 10 heures 30 au cours de laquelle ont été entendus : <br/>
<br/>
              - le représentant de l'association FRANCE NATURE ENVIRONNEMENT ;<br/>
<br/>
              - le représentant de l'association AGIR POUR LES PAYSAGES ;<br/>
<br/>
              - les représentants du ministre de l'écologie, du développement durable et de l'énergie ; <br/>
<br/>
              - la représentante du ministre de l'économie, des finances et du commerce extérieur ;<br/>
<br/>
              et à l'issue de laquelle le juge des référés a clôturé l'instruction ;<br/>
<br/>
<br/>
<br/>
              Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ;<br/>
<br/>
              Considérant qu'en application des articles 36 à 50 de la loi du 12 juillet 2010 portant engagement national pour l'environnement, qui tendent à protéger le cadre de vie en limitant la publicité extérieure tout en permettant l'utilisation de moyens nouveaux, le décret n° 2012-118 du 30 janvier 2012 relatif à la publicité extérieure, aux enseignes et aux préenseignes modifie les dispositions du code de l'environnement en vue notamment de préciser les règles applicables aux bâches publicitaires et aux enseignes scellées au sol ou installées directement sur le sol ;<br/>
<br/>
              Considérant que les associations FRANCE NATURE ENVIRONNEMENT et AGIR POUR LES PAYSAGES demandent au juge des référés du Conseil d'Etat de suspendre l'exécution de ce décret, d'une part, en tant qu'il instaure, aux articles R. 581-20 et R. 581-55 du code de l'environnement, un dispositif encadrant l'installation de bâches publicitaires sans que la loi ait prévu de participation du public à la procédure d'autorisation de telles publicités et, d'autre part, en tant qu'il modifie la réglementation applicable aux enseignes scellées au sol ou installées directement sur le sol, en abrogeant les exigences relatives à leur surface maximale unitaire et leur hauteur maximale, par la rédaction du nouvel article R. 581-65 et en rendant inapplicable toute règle de densité pour ces enseignes du fait de la rédaction du troisième alinéa de l'article R. 581-64 ;<br/>
<br/>
              Sur les dispositions relatives aux bâches publicitaires :<br/>
<br/>
              Considérant, d'une part, qu'il résulte de la combinaison des dispositions de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel, selon lesquelles le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé à l'occasion d'une instance devant le Conseil d'Etat, avec celles du livre V du code de justice administrative qu'une question prioritaire de constitutionnalité peut être soulevée devant le juge des référés du Conseil d'Etat statuant sur des conclusions à fin de suspension qui lui sont présentées sur le fondement de l'article L. 521-1 de ce code ; que toutefois le juge des référés du Conseil d'Etat peut en toute hypothèse, y compris lorsqu'une question prioritaire de constitutionnalité est soulevée devant lui, rejeter de telles conclusions pour irrecevabilité ou pour défaut d'urgence, sans se prononcer sur le renvoi de la question au Conseil constitutionnel ; <br/>
<br/>
              Considérant, d'autre part, que l'urgence justifie que soit prononcée la suspension d'un acte administratif lorsque l'exécution de celui-ci porte atteinte, de manière suffisamment grave et immédiate, à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre ;<br/>
<br/>
              Considérant que les dispositions du deuxième alinéa de l'article L. 581-9 du code de l'environnement, issues de l'article 40 de la loi du 12 juillet 2010, subordonnent à une autorisation du maire l'installation de bâches comportant de la publicité ; qu'en application de cet alinéa, le décret litigieux réglemente la demande d'autorisation d'emplacement d'une bâche publicitaire, telle que définie au nouvel article R. 581-53 de ce code ; qu'il est ainsi prévu à l'article R. 581-20 que l'autorisation est délivrée pour une durée maximale de huit ans et que le maire se prononce sur la demande en tenant compte des caractéristiques du dispositif, notamment quant à sa durée d'installation, sa surface, les procédés utilisés, son insertion architecturale ainsi que son impact sur le cadre de vie environnant ; <br/>
<br/>
              Considérant que pour justifier de l'urgence qui s'attache, selon elles, à ce que soit ordonnée la suspension de ces dispositions, les associations requérantes font valoir que l'entrée en vigueur de ce texte aurait pour conséquence de permettre la délivrance d'autorisations pour huit années, sans possibilité de retrait au terme d'un délai de quatre mois, portant une atteinte grave au cadre de vie du fait de l'impact visuel résultant de la surface que peuvent atteindre de telles bâches, non encadrée, et de l'absence de procédure de consultation du public préalablement à l'installation de tels dispositifs, en dépit de leurs incidences sur l'environnement ;<br/>
<br/>
              Considérant, toutefois, que le décret contesté fixe, ainsi que l'admettent les associations requérantes, des règles plus contraignantes qu'auparavant pour l'installation des dispositifs de bâches publicitaires ; que les dispositions contestées, dont l'entrée en vigueur est fixée au 1er juillet 2012, n'ont pas elles-mêmes pour effet d'autoriser l'installation de tels dispositifs, ni donc de porter directement atteinte au cadre de vie ; que l'installation de ces dispositifs devra faire l'objet d'autorisations délivrées au cas par cas par les maires, à qui il appartiendra de veiller, sous le contrôle du juge, au respect des conditions mentionnées ci-dessus ; qu'ainsi, les dispositions contestées ne sont pas susceptibles de porter par elles-mêmes aux intérêts que les associations requérantes entendent défendre une atteinte suffisamment grave et immédiate pour que la condition d'urgence posée par l'article L. 521-1 du code de justice administrative puisse être regardée comme remplie ;<br/>
<br/>
              Considérant que, cette condition n'étant ainsi pas satisfaite en ce qui concerne les conclusions relatives à la réglementation des bâches publicitaires, il n'y a pas lieu de statuer, dans le cadre de la présente instance en référé, sur la demande de renvoi au Conseil constitutionnel de la question prioritaire de constitutionnalité relative à la conformité des dispositions du deuxième alinéa de l'article L. 581-9 du code de l'environnement à l'article 7 de la Charte de l'environnement ;<br/>
<br/>
              Sur les dispositions relatives aux enseignes scellées au sol ou installées directement sur le sol :<br/>
<br/>
              Considérant qu'en vertu des dispositions actuellement en vigueur de l'article R. 581-59 du code de l'environnement, le nombre des enseignes de plus de 1 mètre carré scellées au sol ou installées directement sur le sol est limité " à un dispositif à double face ou deux dispositifs simples placés le long de chaque voie ouverte à la circulation publique bordant l'immeuble où est exercée l'activité signalée " ; que, selon l'article R. 581-60, la surface unitaire maximale de ces mêmes enseignes est de 6 mètres carrés, portée à 16 mètres carrés dans certaines agglomérations denses définies par ce même article ; que les associations requérantes critiquent le décret litigieux sur chacun de ces points ; <br/>
<br/>
              En ce qui concerne les règles de densité applicables à ces enseignes : <br/>
<br/>
              Considérant que, si le troisième alinéa de l'article R. 581-59, devenu R. 581-64 par l'effet de l'article 2 du décret du 30 janvier 2012, tel que modifié par le VII de l'article 12, remplace les termes " le long de chaque voie " par " sur chacune des voies ", et " l'immeuble où " par " l'immeuble dans lequel ", il n'apparaît pas, en l'état de l'instruction, que ces modifications, d'ordre essentiellement rédactionnel, puissent être interprétées comme supprimant ni même comme affaiblissant, par rapport aux dispositions qui figuraient auparavant à l'article R. 581-59, la réglementation de la densité des enseignes scellées au sol ou installées directement sur le sol sur l'assiette d'un même bien immobilier ; qu'il suit de là que l'atteinte grave et immédiate au cadre de vie dont se plaignent les associations requérantes n'est pas caractérisée ; que la condition d'urgence n'est, dès lors, pas remplie sur ce point ;<br/>
<br/>
              En ce qui concerne les règles de dimension applicables à ces enseignes :<br/>
<br/>
              Considérant qu'il résulte de la renumérotation à laquelle l'article 2 du décret litigieux a estimé devoir procéder que, d'une part, les dispositions concernant les enseignes scellées au sol ou installées directement sur le sol, qui figuraient auparavant à l'article R. 581-59 ont, ainsi qu'il a été dit, été transférées au nouvel article R. 581-64 mais que, d'autre part, le transfert de l'article R. 581-60, qui impartissait à ces mêmes enseignes des dimensions maximales, dans un nouvel article R. 581-65 ne s'est pas accompagné, même après la publication au Journal officiel du 21 avril 2012 d'un rectificatif qui n'a pas porté sur ce point, d'une modification corrélative de la référence aux " enseignes mentionnées à l'article R. 581-59 ", alors que, par l'effet du même article 2, le nouvel article R. 581-59 ne concerne désormais que les enseignes lumineuses ; qu'ainsi et alors même que le ministre fait valoir que telle n'était pas l'intention du Gouvernement, les enseignes scellées au sol ou installées directement sur le sol ne seront plus soumises à aucune restriction quant à leurs surface et hauteur maximales à compter du 1er juillet 2012, date d'entrée en vigueur de ces dispositions ; <br/>
<br/>
              Considérant, en premier lieu, que s'il résulte de l'instruction, notamment des indications fournies à l'audience par les représentants de l'administration, qu'un projet de décret en cours d'élaboration rendra les règles de dimension figurant à l'article R. 581-65 applicables aux enseignes scellées au sol ou installées directement sur le sol, aucune précision n'a pu être apportée quant à l'intervention de ce texte avant le 1er juillet prochain ni même quant à la date à laquelle la modification annoncée pourrait intervenir ; que dans ces conditions, tant les inconvénients résultant de l'absence de toute restriction de surface et de hauteur pour ces enseignes à compter du 1er juillet 2012, que l'intérêt général qui s'attache à ce que des dispositions entachées d'une erreur matérielle aussi substantielle n'entrent pas en vigueur, la condition d'urgence posée à l'article L. 521-1 du code de justice administrative doit être regardée comme remplie ;<br/>
<br/>
              Considérant, en second lieu, que le moyen tiré de ce qu'en ne restreignant plus, à compter du 1er juillet 2012, les dimensions maximales des enseignes de plus d'un mètre carré scellées au sol ou installées directement sur le sol les dispositions de l'article R. 581-65 de ce code issues du décret du 30 janvier 2012 méconnaissent l'obligation faite au Gouvernement de définir, au regard du principe énoncé à l'article 3 de la Charte de l'environnement, les modalités d'application de l'article L. 581-18 du code de l'environnement, en déterminant les conditions de la prévention des atteintes portées par ces enseignes aux paysages et au cadre de vie, doit être regardé, en l'état de l'instruction, comme étant propre à créer un doute sérieux quant à la légalité des dispositions dont la suspension est demandée ; <br/>
<br/>
              Considérant qu'il résulte de ce qui précède que les conditions posées par l'article L. 521-1 du code de justice administrative pour que le juge des référés puisse prononcer la suspension d'un acte administratif, sont remplies en l'espèce ; qu'il y a dès lors lieu de suspendre, à compter du 1er juillet 2012, l'exécution des dispositions de l'article R. 581-65 du code de l'environnement issues du décret du 30 janvier 2012, en ce qu'elles renvoient, pour l'application des restrictions de surface et de hauteur qu'elles définissent, à l'article R. 581-59 relatif aux enseignes lumineuses, au lieu de l'article R. 581-64 relatif aux enseignes de plus d'un mètre carré scellées au sol ou installées directement sur le sol et ce, jusqu'au jugement de l'affaire au fond ou, si elle est antérieure, jusqu'à la publication d'un décret portant modification des dispositions litigieuses du code de l'environnement ;<br/>
<br/>
              Sur les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement aux associations FRANCE NATURE ENVIRONNEMENT et AGIR POUR LES PAYSAGES d'une somme de 300 euros chacune, au titre des frais qu'elles ont dû exposer pour introduire la présente instance ;<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : L'exécution des dispositions de l'article R. 581-65 du code de l'environnement issues du décret n° 201-118 du 30 janvier 2012, en ce qu'elles renvoient à l'article R. 581-59 au lieu de l'article R. 581-64, est suspendue jusqu'au jugement de l'affaire au fond ou, si elle est antérieure, jusqu'à la date de publication d'un décret portant modification de ce renvoi, afin de l'étendre aux enseignes scellées au sol ou installées directement sur le sol.<br/>
<br/>
Article 2 : L'Etat versera une somme de 300 euros à chacune des associations requérantes, en application de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
Article 3 : Le surplus des conclusions de la requête est rejeté.<br/>
<br/>
Article 4 : La présente ordonnance sera notifiée à l'association FRANCE NATURE ENVIRONNEMENT, à l'association AGIR POUR LES PAYSAGES, au Premier ministre et à la ministre de l'écologie, du développement durable et de l'énergie. <br/>
Copie en sera adressée au ministre de l'économie, des finances et du commerce extérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">02-01-04-03 AFFICHAGE ET PUBLICITÉ. AFFICHAGE. RÉGIME DE LA LOI DU 29 DÉCEMBRE 1979. DISPOSITIONS APPLICABLES AUX ENSEIGNES ET AUX PRÉENSEIGNES. - DÉCRET ADOPTÉ EN VUE DE PRÉCISER LES RÈGLES APPLICABLES AUX BÂCHES PUBLICITAIRES ET AUX ENSEIGNES SCELLÉES AU SOL OU INSTALLÉES DIRECTEMENT SUR LE SOL, DONT IL RÉSULTE QU'ALORS QUE TELLE N'ÉTAIT PAS L'INTENTION DU GOUVERNEMENT, CES ENSEIGNES NE SERONT PLUS SOUMISES À AUCUNE RESTRICTION DE SURFACE ET DE HAUTEUR À COMPTER DU 1ER JUILLET 2012 - DEMANDE DE SUSPENSION - URGENCE - EXISTENCE, EU ÉGARD TANT AUX INCONVÉNIENTS RÉSULTANT DE L'ABSENCE DE TOUTE RESTRICTION DE SURFACE ET DE HAUTEUR POUR CES ENSEIGNES À COMPTER DU 1ER JUILLET 2012 QUE DE L'INTÉRÊT GÉNÉRAL QUI S'ATTACHE À CE QUE DES DISPOSITIONS ENTACHÉES D'UNE ERREUR MATÉRIELLE AUSSI SUBSTANTIELLE N'ENTRENT PAS EN VIGUEUR [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-035-02-03-02 PROCÉDURE. PROCÉDURES INSTITUÉES PAR LA LOI DU 30 JUIN 2000. RÉFÉRÉ SUSPENSION (ART. L. 521-1 DU CODE DE JUSTICE ADMINISTRATIVE). CONDITIONS D'OCTROI DE LA SUSPENSION DEMANDÉE. URGENCE. - DÉCRET ADOPTÉ EN VUE DE PRÉCISER LES RÈGLES APPLICABLES AUX BÂCHES PUBLICITAIRES ET AUX ENSEIGNES SCELLÉES AU SOL OU INSTALLÉES DIRECTEMENT SUR LE SOL, DONT IL RÉSULTE QU'ALORS QUE TELLE N'ÉTAIT PAS L'INTENTION DU GOUVERNEMENT, CES ENSEIGNES NE SERONT PLUS SOUMISES À AUCUNE RESTRICTION DE SURFACE ET DE HAUTEUR À COMPTER DU 1ER JUILLET 2012 - URGENCE - EXISTENCE, EU ÉGARD TANT AUX INCONVÉNIENTS RÉSULTANT DE L'ABSENCE DE TOUTE RESTRICTION DE SURFACE ET DE HAUTEUR POUR CES ENSEIGNES À COMPTER DU 1ER JUILLET 2012 QUE DE L'INTÉRÊT GÉNÉRAL QUI S'ATTACHE À CE QUE DES DISPOSITIONS ENTACHÉES D'UNE ERREUR MATÉRIELLE AUSSI SUBSTANTIELLE N'ENTRENT PAS EN VIGUEUR [RJ1].
</SCT>
<ANA ID="9A"> 02-01-04-03 Décret modifiant les dispositions du code de l'environnement en vue notamment de préciser les règles applicables aux bâches publicitaires et aux enseignes scellées au sol ou installées directement sur le sol et dont il résulte qu'alors même que le ministre fait valoir que telle n'était pas l'intention du Gouvernement, les enseignes scellées au sol ou installées directement sur le sol ne seront plus soumises à aucune restriction quant à leurs surface et hauteur maximales à compter du 1er juillet 2012, date d'entrée en vigueur de ces dispositions.... ...S'il résulte de l'instruction qu'un projet de décret en cours d'élaboration rendra les règles de dimension figurant à l'article R. 581-65 du code de l'environnement applicables aux enseignes scellées au sol ou installées directement sur le sol, aucune précision n'a pu être apportée quant à l'intervention de ce texte avant le 1er juillet 2012 ni même quant à la date à laquelle la modification annoncée pourrait intervenir. Dans ces conditions, eu égard tant aux inconvénients résultant de l'absence de toute restriction de surface et de hauteur pour ces enseignes à compter du 1er juillet 2012, qu'à l'intérêt général qui s'attache à ce que des dispositions entachées d'une erreur matérielle aussi substantielle n'entrent pas en vigueur, la condition d'urgence posée à l'article L. 521-1 du code de justice administrative doit être regardée comme remplie.</ANA>
<ANA ID="9B"> 54-035-02-03-02 Décret modifiant les dispositions du code de l'environnement en vue notamment de préciser les règles applicables aux bâches publicitaires et aux enseignes scellées au sol ou installées directement sur le sol et dont il résulte qu'alors même que le ministre fait valoir que telle n'était pas l'intention du Gouvernement, les enseignes scellées au sol ou installées directement sur le sol ne seront plus soumises à aucune restriction quant à leurs surface et hauteur maximales à compter du 1er juillet 2012, date d'entrée en vigueur de ces dispositions.... ...S'il résulte de l'instruction qu'un projet de décret en cours d'élaboration rendra les règles de dimension figurant à l'article R. 581-65 du code de l'environnement applicables aux enseignes scellées au sol ou installées directement sur le sol, aucune précision n'a pu être apportée quant à l'intervention de ce texte avant le 1er juillet 2012 ni même quant à la date à laquelle la modification annoncée pourrait intervenir. Dans ces conditions, eu égard tant aux inconvénients résultant de l'absence de toute restriction de surface et de hauteur pour ces enseignes à compter du 1er juillet 2012, qu'à l'intérêt général qui s'attache à ce que des dispositions entachées d'une erreur matérielle aussi substantielle n'entrent pas en vigueur, la condition d'urgence posée à l'article L. 521-1 du code de justice administrative doit être regardée comme remplie.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr. CE, juge des référés, 20 mars 2001 Syndicat national des horlogers, bijoutiers, joailliers, orfèvres et spécialistes de la table Saint-Eloi et autre, n° 230462, p. 142.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
