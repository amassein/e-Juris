<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043351178</ID>
<ANCIEN_ID>JG_L_2021_04_000000432733</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/35/11/CETATEXT000043351178.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 4ème chambres réunies, 07/04/2021, 432733</TITRE>
<DATE_DEC>2021-04-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>432733</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GASCHIGNARD</AVOCATS>
<RAPPORTEUR>M. Pierre Boussaroque</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie Sirinelli</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:432733.20210407</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire, un mémoire complémentaire, un mémoire en réplique et un nouveau mémoire, enregistrés les 17 juillet et 18 octobre 2019, le 31 décembre 2020 et le 12 février 2021 au secrétariat du contentieux du Conseil d'Etat, la société Groupe Lépine demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 15 mai 2019 par laquelle le Comité économique des produits de santé a fixé le tarif de responsabilité et le prix limite de vente au public des implants orthopédiques " Quattro Cim ", " Quattro Hap " et " Integra de reprise " ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la sécurité sociale ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Pierre Boussaroque, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Marie Sirinelli, rapporteure publique ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Gaschignard, avocat de la société Groupe Lépine ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. En vertu de l'article L. 165-1 du code de la sécurité sociale, le remboursement par l'assurance maladie des dispositifs médicaux à usage individuel et des prestations de services et d'adaptation associées est subordonné à leur inscription, effectuée, soit par la description générique de tout ou partie du produit concerné, soit sous forme de marque ou de nom commercial, sur une liste établie après avis de la commission nationale d'évaluation des dispositifs médicaux et des technologies de santé de la Haute Autorité de santé. En vertu des dispositions du I de l'article L. 165-2 et de l'article L. 165-3 du même code, dans leur rédaction en vigueur à la date de la décision attaquée, les tarifs de responsabilité et les prix des dispositifs médicaux remboursés par l'assurance maladie sous forme de marque ou de nom commercial sont établis par convention entre le fabricant ou le distributeur du produit concerné et le Comité économique des produits de santé ou, à défaut, par décision de ce comité, en tenant compte " principalement de l'amélioration éventuelle du service attendu ou rendu, le cas échéant, des résultats de l'évaluation médico-économique des tarifs des produits ou prestations comparables, des volumes de vente prévus ou constatés, des montants remboursés par l'assurance maladie obligatoire prévus ou constatés et des conditions prévisibles et réelles d'utilisation ". En vertu du II de l'article L. 165-2 et du dernier alinéa de l'article L. 165-3 y renvoyant, ces tarifs et prix peuvent être baissés par convention ou, à défaut, par décision du Comité économique des produits de santé, au regard de l'un au moins des huit critères que ces dispositions énumèrent. <br/>
<br/>
              2. Il ressort des pièces du dossier que les cotyles à double mobilité de la gamme " Quattro cim ", " Quattro Hap " et " Integra de reprise " en litige, qui sont des implants articulaires de hanche fabriqués par la société Groupe Lépine, ont été inscrits sur la liste mentionnée à l'article L. 165-1 du code de la sécurité sociale, d'abord sous description générique, puis, conformément à la nouvelle orientation prise pour assurer un meilleur suivi de leur utilisation, en nom de marque à compter du 19 juin 2018. La société Groupe Lépine ayant refusé la proposition du Comité économique des produits de santé de modifier la convention qu'elle avait signée le 23 avril 2018, en vue de baisser le tarif de remboursement et le prix de ces cotyles pour l'aligner sur celui des dispositifs comparables inscrits sous description générique, ce comité a adopté cette baisse par une décision du 15 mai 2019, sur le fondement des dispositions citées au point 1. La société Groupe Lépine demande l'annulation pour excès de pouvoir de cette décision.<br/>
<br/>
              Sur la légalité externe de la décision attaquée :<br/>
<br/>
              3.	Aux termes du premier alinéa de l'article D. 162-2-5 du code de la sécurité sociale : " Le Comité économique des produits de santé se réunit sur convocation de son président. Le président fixe l'ordre du jour des séances. Les délibérations du Comité économique des produits de santé ne sont valables que si au moins six de ses membres ayant voix délibérative sont présents ".<br/>
<br/>
              4. D'une part, si la société requérante affirme que tous les membres du Comité économique des produits de santé prévus par l'article D. 162-2-1 du code de la sécurité sociale n'auraient pas été régulièrement convoqués, ce dont il résulterait que ce comité n'aurait pas siégé dans une composition régulière, elle n'assortit pas cette allégation des précisions permettant d'en apprécier le bien-fondé.<br/>
<br/>
              5. D'autre part, il ressort des pièces du dossier que huit des membres du Comité économique des produits de santé étaient présents lors de la réunion du 15 mai 2019, au cours de laquelle la décision attaquée a été prise. Par suite, la société requérante n'est pas fondée à soutenir que la règle de quorum fixée par les dispositions rappelées au point 3 aurait été méconnue.<br/>
<br/>
              Sur la légalité interne de la décision attaquée :<br/>
<br/>
              6. Aux termes du II de l'article L. 165-2 du code de la sécurité sociale, également applicable au prix des dispositifs médicaux à usage individuel en vertu du dernier alinéa de l'article L. 165-3 du même code : " Le tarif de responsabilité mentionné au I peut être fixé à un niveau inférieur ou baissé, par convention ou, à défaut, par décision du Comité économique des produits de santé, au regard d'au moins l'un des critères suivants : / 1° L'ancienneté de l'inscription du produit ou de la prestation associée, ou d'un ensemble de produits et de prestations comparables, sur la liste prévue à l'article L. 165-1 ; / 2° Les tarifs des produits et prestations comparables et les remises applicables au produit ou à la prestation et à ceux comparables recouvrées dans les conditions prévues à l'article L. 165-4 au bénéfice de la Caisse nationale de l'assurance maladie ; / (...) 7° Les montants des produits ou prestations remboursés par l'assurance maladie obligatoire prévus ou constatés (...) ". Par la décision attaquée, le Comité économique des produits de santé a baissé les tarifs de responsabilité et les prix des cotyles à double mobilité de la gamme " Quattro cim ", " Quattro Hap " et " Integra de reprise " en considération de l'ancienneté de l'inscription de ce dispositif médical sur la liste des produits et prestations remboursables, du niveau élevé des montants remboursés par l'assurance maladie obligatoire pour les implants orthopédiques, dont les cotyles à double mobilité, et du prix des comparateurs. <br/>
<br/>
              7. Tout d'abord, les dispositions du 1° du II de l'article L. 165-2 du code de la sécurité sociale ne distinguent pas, s'agissant du critère de l'ancienneté de l'inscription sur la liste des produits et prestations responsables, selon que les produits ou les prestations considérés y ont été inscrits sous description générique ou en nom de marque. Elles permettent d'ailleurs de prendre également en considération, outre l'ancienneté de l'inscription du produit ou de la prestation en cause, celle d'un ensemble de produits et prestations comparables. Par suite, le Comité économique des produits de santé a pu légalement apprécier l'ancienneté de l'inscription des produits en litige sur cette liste en prenant en considération la période, importante, au cours de laquelle ils ont bénéficié de l'inscription sous description générique, alors même qu'ils y ont été inscrits en nom de marque à compter du 19 juin 2018 seulement. <br/>
<br/>
              8. Ensuite, le Comité économique des produits de santé fait valoir, sans que ce point soit contesté, que les cotyles à double mobilité ont donné lieu, en 2018, à compter de leur inscription en nom de marque, à des remboursements au titre de l'assurance maladie s'élevant à 17,6 millions d'euros et les cotyles dans leur ensemble, incluant les cotyles à simple et à double mobilité, à 97 millions d'euros. Le Comité économique des produits de santé pouvait légalement se fonder, en application des dispositions du 7° du II de l'article L. 165-2 du code de la sécurité sociale, sur l'importance des montants ainsi remboursés par l'assurance maladie obligatoire, alors même qu'ils seraient en baisse.<br/>
<br/>
              9. En outre, les dispositions du 2° du II de l'article L. 165-2 du code de la sécurité sociale et celles du dernier alinéa de l'article L. 165-3 de ce code prévoient que les tarifs de responsabilité et les prix des dispositifs médicaux à usage individuel peuvent être diminués en considération des tarifs et prix des produits et prestations comparables. La société requérante fait valoir que les produits en litige ont été inscrits en nom de marque sur la liste des produits et prestations remboursables au motif, prévu par l'article R. 165-3 du même code, que " l'impact sur les dépenses d'assurance maladie, les impératifs de santé publique ou le contrôle des spécifications techniques minimales nécessite un suivi particulier du produit " et que ce suivi particulier représente un coût supplémentaire, qu'il revient à l'administration de prendre en compte. Toutefois, s'il résulte du I de l'article L. 165-2 et du dernier alinéa de l'article L. 165-3 de ce code qu'il est tenu compte notamment, lors de la fixation du tarif et du prix, de l'amélioration éventuelle du service attendu ou rendu, la société requérante ne conteste pas que les cotyles en cause n'ont pu, en l'absence d'études cliniques comparatives, démontrer d'amélioration du service rendu par rapport aux produits comparables, qui ne sont pas inscrits en nom de marque. Dès lors, elle n'est en tout état de cause pas fondée à soutenir que l'inscription en nom de marque des produits en litige aurait fait obstacle à ce que le Comité économique des produits de santé puisse légalement prendre en considération, pour diminuer leurs tarifs et prix, la baisse des tarifs et prix des produits comparables inscrits sous description générique.<br/>
<br/>
              10. Enfin, il ne ressort pas des pièces du dossier que le Comité économique des produits de santé aurait commis une erreur manifeste d'appréciation en décidant de baisser d'environ 2 % les tarifs de responsabilité et les prix applicables aux cotyles en litige, au vu de l'ancienneté de l'inscription des produits considérés sur la liste des produits et prestations remboursables, du niveau des montants remboursés par l'assurance maladie obligatoire et du prix des comparateurs, critères qu'il a légalement pu prendre en considération ainsi qu'il a été dit ci-dessus.<br/>
<br/>
              11. Il résulte de tout ce qui précède que la société Groupe Lépine n'est pas fondée à demander l'annulation pour excès de pouvoir de la décision du 15 mai 2019 par laquelle le Comité économique des produits de santé a fixé le tarif de responsabilité et le prix des implants orthopédiques de la gamme " Quattro Cim ", " Quattro Hap " et " Integra de reprise ".<br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              12. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'il soit fait droit aux conclusions de la société Groupe Lépine présentées à ce titre, l'Etat n'étant pas la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : La requête de la société Groupe Lépine est rejetée.<br/>
Article 2 : La présente décision sera notifiée à la société Groupe Lépine et au Comité économique des produits de santé. <br/>
Copie en sera adressée au ministre des solidarités et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">61-04-01-022 SANTÉ PUBLIQUE. PHARMACIE. PRODUITS PHARMACEUTIQUES. - MODIFICATION DU TARIF DE RESPONSABILITÉ DES DISPOSITIFS MÉDICAUX À USAGE INDIVIDUEL - 1) CRITÈRE TENANT À L'ANCIENNETÉ DE L'INSCRIPTION SUR LA LISTE DES PRODUITS ET PRESTATIONS REMBOURSABLES (1° DU II DE L'ARTICLE L. 165-2 DU CSS) - CIRCONSTANCE QUE LES PRODUITS OU PRESTATIONS ONT ÉTÉ INSCRITS SOUS DESCRIPTION GÉNÉRIQUE OU EN NOM DE MARQUE - CIRCONSTANCE SANS INCIDENCE - 2) CRITÈRE TENANT AUX MONTANTS DES PRODUITS OU PRESTATIONS REMBOURSÉS PAR L'ASSURANCE MALADIE OBLIGATOIRE (7° DU II DE L'ARTICLE L. 165-2 DU CSS) - PRISE EN COMPTE DES MONTANTS ABSOLUS, ET NON TENDANCIELS [RJ1].
</SCT>
<ANA ID="9A"> 61-04-01-022 1) Le 1° du II de l'article L. 165-2 du code de la sécurité sociale (CSS) ne distingue pas, s'agissant du critère de l'ancienneté de l'inscription sur la liste des produits et prestations remboursables, selon que les produits ou les prestations considérés y ont été inscrits sous description générique ou en nom de marque. Il permet d'ailleurs de prendre également en considération, outre l'ancienneté de l'inscription du produit ou de la prestation en cause, celle d'un ensemble de produits et prestations comparables.,,,Par suite, le Comité économique des produits de santé (CEPS), lorsqu'il décide de baisser le tarif de responsabilité de produits en application de ces dispositions, peut légalement apprécier l'ancienneté de l'inscription de ces produits sur la liste des produits et prestations remboursables en prenant en considération la période au cours de laquelle ils ont bénéficié de l'inscription sous description générique, alors même qu'ils y ont été inscrits en nom de marque par la suite.,,,2) Le CEPS peut légalement se fonder, en application du 7° du II de l'article L. 165-2 du CSS, sur l'importance des montants remboursés par l'assurance maladie obligatoire, alors même qu'ils seraient en baisse.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., s'agissant de l'appréciation du critère tenant aux volumes de vente (6° du II de l'article L. 165-2 du CSS), CE, 16 décembre 2016, Société Advanced Technical Fabrication et autres, n°s 397908 398394 399351, point 16, aux Tables sur un autre point.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
