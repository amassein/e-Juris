<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033157857</ID>
<ANCIEN_ID>JG_L_2016_09_000000394360</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/15/78/CETATEXT000033157857.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 5ème chambres réunies, 21/09/2016, 394360</TITRE>
<DATE_DEC>2016-09-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>394360</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DE NERVO, POUPET</AVOCATS>
<RAPPORTEUR>M. Jean-François de Montgolfier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:394360.20160921</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Lactalis Ingrédients a demandé au tribunal administratif de Rennes d'annuler la décision du 5 mars 2012 par laquelle le garde des sceaux, ministre de la justice et des libertés a rejeté sa demande tendant au versement de la somme de 471 383,99 euros en réparation des préjudices résultant d'une faute commise dans l'exercice de la fonction juridictionnelle par la juridiction administrative. Par un jugement n° 1201703 du 11 avril 2014, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 14NT01471 du 29 octobre 2015, la cour administrative d'appel de Nantes, statuant sur l'appel de la société Lactalis Ingrédients, a annulé ce jugement et transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, la requête présentée à ce tribunal par cette société.<br/>
<br/>
              1° Sous le n° 394360, par une requête et deux mémoires en réplique transmis par la cour administrative d'appel de Nantes en application de cet arrêt, enregistrés le 2 novembre 2015 au secrétariat du contentieux du Conseil d'Etat, et par deux nouveaux mémoires enregistrés au secrétariat du contentieux du Conseil d'Etat les 30 décembre 2015 et 9 août 2016, la société Lactalis Ingrédients demande au Conseil d'Etat :<br/>
<br/>
              1°) de renvoyer à la Cour de justice de l'Union européenne, à titre préjudiciel, les questions de savoir si :<br/>
              - l'arrêt rendu le 29 janvier 2009 par la Cour de justice des communautés européennes dans les affaires C-278/07 à C-280/07 pouvait être interprété, le 27 juillet 2009, sans méconnaissance manifeste de cet arrêt et des dispositions de l'article 3 du règlement CE n° 2988/95 du Conseil du 18 décembre 1995, comme permettant aux Etats membres d'appliquer aux matières communautaires un délai de prescription au cas par cas et a posteriori et s'il pouvait être considéré, le 27 juillet 2009, comme délivrant une interprétation des dispositions de l'article 3 du règlement CE n° 2988/95 du Conseil du 18 décembre 1995 de nature à dispenser les juridictions nationales dont les décisions ne sont pas susceptibles d'un recours juridictionnel de droit interne de leur obligation de renvoi prévue à l'article 267, troisième alinéa, du traité sur le fonctionnement de l'Union ;<br/>
              - le droit à une procédure équitable et à un tribunal indépendant et impartial garantis par l'article 47 de la charte des droits de l'Union s'oppose à ce qu'une juridiction nationale statue sur l'action en responsabilité découlant d'une violation du droit de l'Union qu'elle a elle-même commise ;<br/>
<br/>
              2°) de condamner l'Etat à lui verser la somme de 471 383,99 euros en réparation du préjudice qu'elle estime avoir subi du fait d'une faute commise dans l'exercice de la fonction juridictionnelle par la juridiction administrative ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 15 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              2° Sous le n° 395548, par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 23 décembre 2015 et 23 mars 2016 au secrétariat du contentieux du Conseil d'Etat, la société Lactalis Ingrédients demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt de la cour administrative d'appel de Nantes du 29 octobre 2015 analysé ci-dessus ;<br/>
<br/>
              2°) de renvoyer l'affaire devant la cour administrative d'appel de Nantes ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-François de Montgolfier, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP de Nervo, Poupet, avocat de la société Lactalis Ingredients ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que la demande présentée par la société Lactalis Ingrédients transmise par l'arrêt de la cour administrative d'appel de Nantes du 29 octobre 2015 et le pourvoi en cassation formé par cette même société contre cet arrêt présentent à juger les mêmes questions ; qu'il y a lieu de les joindre pour statuer par une seule décision ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par deux décisions des 27 septembre et 15 décembre 1999 et par un titre exécutoire du 11 février 2000, le directeur de l'office national interprofessionnel du lait et des produits laitiers (ONILAIT) a demandé à la société Lactalis Industrie, devenue la société Lactalis Ingrédients, le reversement de restitutions à l'exportation versées à raison de contrats de vente de poudre de lait conclus entre mars et avril 1994 ; que, par un jugement du 6 janvier 2003, le tribunal administratif de Rennes a rejeté les demandes de la société tendant à l'annulation de ces décisions ; que, par une décision n° 292620 du 27 juillet 2009, le Conseil d'Etat, statuant au contentieux a annulé l'arrêt de la cour administrative d'appel de Nantes du 30 décembre 2005 rejetant l'appel formé par cette société contre ce jugement et, réglant l'affaire au fond, a rejeté cet appel ; que, la société Lactalis Ingrédients a demandé au tribunal administratif de Rennes de condamner l'Etat à réparer le préjudice qu'elle estime avoir subi du fait de cette décision du Conseil d'Etat, dont elle soutient qu'elle est entachée d'une violation manifeste du droit de l'Union européenne ; que, par l'arrêt du 29 octobre 2015 contre lequel la société Lactalis Ingrédients se pourvoit en cassation, la cour administrative d'appel de Nantes a annulé le jugement du tribunal administratif de Rennes rejetant cette demande et transmis le dossier au Conseil d'Etat ;<br/>
<br/>
              3. Considérant qu'en vertu des principes généraux régissant la responsabilité de la puissance publique, une faute lourde commise dans l'exercice de la fonction juridictionnelle par une juridiction administrative est susceptible d'ouvrir droit à indemnité ; que si l'autorité qui s'attache à la chose jugée s'oppose à la mise en jeu de cette responsabilité dans les cas où la faute lourde alléguée résulterait du contenu même de la décision juridictionnelle et où cette décision serait devenue définitive, la responsabilité de l'Etat peut cependant être engagée dans le cas où le contenu de la décision juridictionnelle est entachée d'une violation manifeste du droit de l'Union ayant pour objet de conférer des droits aux particuliers ; <br/>
<br/>
              4. Considérant que les articles L. 211-1 et L. 311-1 du code de justice administrative disposent que les tribunaux administratifs sont, en premier ressort, juges de droit commun du contentieux administratif ; que l'article R. 811-1 du même code prévoit que, sous réserve des exceptions qu'il énumère, les décisions des tribunaux administratifs sont susceptibles d'appel ; qu'il résulte de ces dispositions que les tribunaux administratifs et, en appel, les cours administratives d'appel, sont compétents pour connaître des actions en responsabilité dirigées contre l'Etat à raison de la faute lourde commise dans l'exercice de la fonction juridictionnelle par une juridiction administrative ; <br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède qu'en jugeant que la demande formée par la société Lactalis Ingrédients devant le tribunal administratif de Rennes relevait de la compétence en premier et dernier ressort du Conseil d'Etat, la cour administrative d'appel de Nantes a entaché son arrêt d'erreur de droit ; que la société Lactalis est fondée à demander son annulation ; qu'il y a lieu de renvoyer l'affaire à la cour administrative d'appel de Nantes ;<br/>
<br/>
              6. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros que la société Lactalis Ingrédients demande au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nantes du 29 octobre 2015 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nantes.<br/>
Article 3 : L'Etat versera à la société Lactalis Ingrédients la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la société Lactalis Ingrédients, au garde des sceaux, ministre de la justice et au ministre de l'agriculture, de l'agroalimentaire et de la forêt.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">17-05-01 COMPÉTENCE. COMPÉTENCE À L'INTÉRIEUR DE LA JURIDICTION ADMINISTRATIVE. COMPÉTENCE EN PREMIER RESSORT DES TRIBUNAUX ADMINISTRATIFS. - RECOURS EN RESPONSABILITÉ POUR FAUTE LOURDE DANS L'EXERCICE DE LA FONCTION JURIDICTIONNELLE ET POUR VIOLATION MANIFESTE DU DROIT DE L'UNION PAR UNE DÉCISION JURIDICTIONNELLE (CE, 18 JUIN 2008, M...., N° 295831, P. 230).
</SCT>
<ANA ID="9A"> 17-05-01 Il résulte des articles L. 211-1, L. 311-1 et R. 811-1 du code de justice administrative que les tribunaux administratifs et, en appel, les cours administratives d'appel, sont compétents pour connaître des actions en responsabilité dirigées contre l'Etat à raison de la faute lourde commise dans l'exercice de la fonction juridictionnelle par une juridiction administrative.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
