<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028267479</ID>
<ANCIEN_ID>JG_L_2013_11_000000353703</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/26/74/CETATEXT000028267479.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 27/11/2013, 353703</TITRE>
<DATE_DEC>2013-11-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>353703</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MASSE-DESSEN, THOUVENIN, COUDRAY</AVOCATS>
<RAPPORTEUR>M. Rémi Decout-Paolini</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:353703.20131127</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu 1°, sous le n° 353703, la requête sommaire, le mémoire complémentaire et le nouveau mémoire, enregistrés les 28 octobre 2011, 26 janvier 2012 et 5 juillet 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour le syndicat national CFDT des mineurs et assimilés et du personnel du régime minier, dont le siège est 13, rue de la Rosselle à Freyming-Merlebach (57803), représenté par son secrétaire général ; le syndicat requérant demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2011-1034 du 30 août 2011 relatif au régime spécial de sécurité sociale dans les mines ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu 2°, sous le n° 353707, la requête sommaire et le mémoire complémentaire, enregistrés les 28 octobre 2011 et 30 janvier 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour la fédération nationale des mines et de l'énergie CGT, dont le siège est 263, rue de Paris à Montreuil (93516), représentée par son secrétaire général adjoint ; la fédération requérante demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le même décret ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu 3°, sous le n° 353781, la requête sommaire et le mémoire complémentaire, enregistrés les 31 octobre 2011 et 1er février 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour la fédération de l'énergie et des mines Force ouvrière, dont le siège est 60, rue Vergniaud à Paris (75013), représentée par son secrétaire général ; la fédération requérante demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le même décret ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu la Constitution, notamment son Préambule ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et son premier protocole additionnel ;<br/>
<br/>
              Vu la charte des droits fondamentaux de l'Union européenne ;<br/>
<br/>
              Vu les directives 92/49/CEE du Conseil du 18 juin 1992 et 92/96/CEE du Conseil du 25 novembre 2009 ;<br/>
<br/>
              Vu le code monétaire et financier ;<br/>
<br/>
              Vu le code du travail ;<br/>
<br/>
              Vu le code de la sécurité sociale ;<br/>
<br/>
              Vu la loi n° 2004-105 du 3 février 2004 ;<br/>
<br/>
              Vu le décret n° 46-2769 du 27 novembre 1946 ;<br/>
<br/>
              Vu le décret n° 2012-434 du 30 mars 2012 ;<br/>
<br/>
              Vu le décret n° 2013-260 du 28 mars 2013 ;<br/>
<br/>
              Vu la décision n° 353781 du 4 avril 2012 par laquelle le Conseil d'Etat, statuant au contentieux, a renvoyé au Conseil constitutionnel la question de la conformité à la Constitution de l'article L. 711-1 du code de la sécurité sociale ;<br/>
<br/>
              Vu la décision n° 2012-254 QPC du 18 juin 2012 du Conseil constitutionnel statuant sur la question prioritaire de constitutionnalité soulevée par la fédération de l'énergie et des mines Force ouvrière ;<br/>
<br/>
              Vu le code de justice administrative ; <br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Rémi Decout-Paolini, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Masse-Dessen, Thouvenin, Coudray, avocat du syndicat national CFDT des mineurs et assimilés et du personnel du régime minier, à la SCP Lyon-Caen, Thiriez, avocat de la fédération nationale des mines et de l'énergie CGT et à Me Haas, avocat de la fédération de l'énergie et des mines Force Ouvrière ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que le syndicat national CFDT des mineurs et assimilés et du personnel du régime minier, la fédération nationale des mines et de l'énergie CGT et la fédération de l'énergie et des mines Force Ouvrière demandent l'annulation pour excès de pouvoir du même décret du 30 août 2011 ; qu'il y a lieu de joindre leurs requêtes pour statuer par une seule décision ;<br/>
<br/>
              Sur le non-lieu partiel :<br/>
<br/>
              2. Considérant que  le décret du 28 mars 2013 relatif au régime spécial de sécurité sociale dans les mines, intervenu postérieurement à l'enregistrement des présentes requêtes et devenu définitif, a abrogé l'article 80 du décret attaqué ; que les dispositions de cet article prévoyant le transfert au régime général d'assurance maladie, le 31 décembre 2013 au plus tard, de la gestion des activités assurantielles et d'offre de soins du régime spécial de la sécurité sociale dans les mines n'avaient fait l'objet d'aucune mesure d'application ; qu'ainsi, les conclusions tendant à l'annulation de l'article 80, qui est divisible des autres dispositions du décret, sont devenues sans objet ; qu'il n'y a, par suite, pas lieu d'y statuer ;<br/>
<br/>
              Sur la légalité externe :<br/>
<br/>
              En ce qui concerne la compétence du pouvoir réglementaire :<br/>
<br/>
              3. Considérant qu'aux termes de l'article L. 711-1 du code de la sécurité sociale : " Parmi celles jouissant déjà d'un régime spécial le 6 octobre 1945, demeurent.provisoirement soumises à une organisation spéciale de sécurité sociale, les branches d'activités ou entreprises énumérées par un décret en Conseil d'Etat / Des décrets établissent pour chaque branche d'activité ou entreprises mentionnées à l'alinéa précédent une organisation de sécurité sociale dotée de l'ensemble des attributions définies à l'article L. 111-1. Cette organisation peut comporter l'intervention de l'organisation générale de la sécurité sociale pour une partie des prestations " ; que l'article R. 711-1 du même code dispose que sont soumises à un régime spécial " les entreprises minières et les entreprises assimilées, définies par le décret nº 46-2769 du 27 novembre 1946, à l'exclusion des activités se rapportant à la recherche ou à l'exploitation des hydrocarbures liquides ou gazeux " ; que les dispositions de l'article L. 711-1 donnent au pouvoir réglementaire une large habilitation en vue de définir par décret les règles applicables aux régimes spéciaux de sécurité sociale, l'autorisant à intervenir, le cas échéant, dans le domaine réservé à la loi par l'article 34 de la Constitution ; que, par sa décision du 18 juin 2012, le Conseil constitutionnel a déclaré conforme à la Constitution l'article L. 711-1 du code de la sécurité sociale sur le fondement duquel a été pris le décret attaqué ; que, par suite, les moyens tirés de ce que cet article méconnaîtrait les droits et libertés garantis par la Constitution, de ce que le décret attaqué serait dépourvu de base légale ou de ce que le pouvoir réglementaire aurait excédé sa compétence en édictant des dispositions relevant du domaine de la loi doivent être écartés ;<br/>
<br/>
              En ce qui concerne l'absence de consultation des comités d'entreprise des caisses régionales de sécurité sociale dans les mines et de la Caisse autonome nationale de la sécurité sociale dans les mines :<br/>
<br/>
              4. Considérant que l'article L. 2323-6 du code du travail  dispose que " le comité d'entreprise est informé et consulté sur les questions intéressant l'organisation, la gestion et la marche générale de l'entreprise et, notamment, sur les mesures de nature à affecter le volume ou la structure des effectifs, la durée de travail, les conditions d'emploi, de travail et de formation professionnelle " ; qu'aux termes de l'article L. 2323-19 du même code : " Le comité d'entreprise est informé et consulté sur les modifications de l'organisation économique ou juridique de l'entreprise (...) / L'employeur indique les motifs des modifications projetées et consulte le comité d'entreprise sur les mesures envisagées à l'égard des salariés lorsque ces modifications comportent des conséquences pour ceux-ci " ; que l'article L. 2323-4 du même code, dans sa version alors en vigueur, précise que le comité d'entreprise doit disposer, dans l'exercice de ses attributions consultatives, " d'un délai d'examen suffisant " ; qu'en application de l'ensemble de ces dispositions, les caisses régionales de sécurité sociale dans les mines (CARMI), qu'il était prévu de transformer en services territoriaux de la Caisse autonome nationale de la sécurité sociale dans les mines (CANSSM), devaient, ainsi que la CANSSM, être mises en mesure de saisir leurs comités d'entreprise respectifs, afin de les consulter préalablement à l'édiction du décret attaqué sur les modalités de la restructuration envisagée, en leur laissant un délai suffisant pour se prononcer ; <br/>
<br/>
              5. Considérant, en premier lieu, qu'il ressort des pièces des dossiers que les présidents des six CARMI concernées, également présidents des comités d'entreprise de ces caisses, ont été invités par le président de la CANSSM, sur demande du ministre du travail, de l'emploi et de la santé, à procéder à la consultation de chaque comité d'entreprise avant le 1er septembre 2011 ; qu'à cette fin, le projet de décret ainsi qu'un dossier-type leur ont été adressés le 2 août 2011 ; que, dans les circonstances de l'espèce, le délai ainsi imparti aux présidents des CARMI pour saisir les comités d'entreprise et à ces derniers pour se prononcer était suffisant ;<br/>
<br/>
              6. Considérant, en second lieu, que le président de la CANSSM, rendu destinataire, ainsi qu'il a été indiqué, d'un courrier du ministre du travail de l'emploi et de la santé et de documents présentant la restructuration du régime spécial de sécurité sociale dans les mines, a été mis à même de procéder à la consultation du comité d'entreprise de la caisse sur cette opération ; que, par suite, le moyen tiré de ce que le décret litigieux n'aurait pas fait l'objet d'une consultation régulière du comité d'entreprise de la CANSSM ne peut qu'être écarté ;<br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que les moyens tirés de ce que le décret attaqué aurait été pris en méconnaissance des articles L. 2323-6 et L. 2323-29 du code du travail doivent être écartés ; qu'il en est de même des moyens tirés de la méconnaissance du principe de participation des travailleurs énoncé au huitième alinéa du Préambule de la Constitution de 1946 ;<br/>
<br/>
              En ce qui concerne l'absence de consultation des conseils d'administration des caisses régionales de sécurité sociale dans les mines et de la Caisse autonome nationale de la sécurité sociale dans les mines :<br/>
<br/>
              8. Considérant qu'aux termes de l'article L. 121-1 du code de la sécurité sociale : " Sauf dispositions particulières propres à certains régimes et à certains organismes, le conseil d'administration règle par ses délibérations les affaires de l'organisme " ; que ni ces dispositions ni celles du décret du 27 novembre 1946 portant organisation de la sécurité sociale dans les mines et prévoyant, dans leur version alors applicable, que les représentants des affiliés disposent de sièges aux conseils d'administration des CARMI et de la CANSSM, n'ont pour objet ou pour effet de subordonner à la consultation préalable de ces instances les modifications de leurs statuts ;<br/>
<br/>
              En ce qui concerne l'absence de consultation des représentants des affiliés et de leurs ayants droit ainsi que des personnels des CARMI :<br/>
<br/>
              9. Considérant qu'il résulte des stipulations de son article 51 que la charte des droits fondamentaux de l'Union européenne s'adresse " aux Etats membres uniquement lorsqu'ils mettent en oeuvre le droit de l'Union " ; que le décret attaqué n'ayant pas pour objet de mettre en oeuvre le droit de l'Union européenne, le syndicat national CFDT des mineurs et assimilés et du personnel du régime minier ne peut utilement soutenir que la procédure préalable à son adoption aurait méconnu les stipulations des articles 12 et 28 de cette charte ; qu'il n'est pas fondé à soutenir qu'il découlerait de l'article 11 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales une obligation de consultation préalable des affiliés et de leurs ayants droit sur le décret litigieux ;<br/>
<br/>
              En ce qui concerne la régularité de la consultation de la Caisse des dépôts et consignations :<br/>
<br/>
              10. Considérant qu'il ressort des pièces des dossiers que la commission de surveillance de la Caisse des dépôts et consignations a été régulièrement consultée sur les dispositions du projet de décret qui devaient être soumises à sa consultation en application de l'article L. 518-3 du code monétaire et financier ; que, par suite, le moyen tiré de ce que l'avis émis le 20 juillet 2011 par cette commission serait irrégulier manque en fait ;<br/>
<br/>
              En ce qui concerne la consultation du Conseil d'Etat :<br/>
<br/>
              11. Considérant que la circonstance que le Conseil d'Etat aurait été saisi du projet de décret avant que les organismes dont la consultation était obligatoire se soient prononcés est sans incidence sur la régularité de sa saisine ; que, par suite, le moyen tiré de ce que le décret attaqué serait illégal en l'absence de consultation régulière du Conseil d'Etat ne peut qu'être écarté ;<br/>
<br/>
              Sur la légalité interne :<br/>
<br/>
              En ce qui concerne les moyens tirés de ce que le décret attaqué serait entaché d'" incompétence négative " :<br/>
<br/>
              12. Considérant que le moyen tiré de ce que le décret attaqué ne préciserait pas les modalités de transfert à la Caisse autonome nationale de la sécurité sociale (CANSSM) des contrats de travail des salariés des caisses régionales de sécurité sociale dans les mines (CARMI) manque, en tout état de cause, en fait ; que les modalités de transfert des contrats de travail des salariés de la CANSSM à l'Agence nationale pour la garantie des droits des mineurs (ANGDM) sont régies par l'article L. 1224-3 du code du travail relatives au transfert du contrat de travail et n'avaient pas, en tout état de cause, à être précisées par décret ; qu'enfin, et en tout état de cause, aucun principe ni aucune disposition ne faisaient obligation au pouvoir réglementaire d'indiquer que cette réforme n'affectait pas les prestations délivrées aux affiliés ou à leurs ayants droit ;<br/>
<br/>
              En ce qui concerne l'article 3 du décret :<br/>
<br/>
              13. Considérant que les régimes obligatoires de sécurité sociale, qui mettent en oeuvre le principe de la solidarité sans poursuivre de but lucratif, sont exclus du champ d'application des directives 92/49/ CEE et 92/96/CEE du Conseil du 18 novembre 1992 relatives à la coordination des législations nationales en matière d'assurance ; que, par suite, leurs dispositions ne sauraient, en tout état de cause, être utilement invoquées à l'encontre du décret attaqué ;<br/>
<br/>
              En ce qui concerne l'article 24 :<br/>
<br/>
              14. Considérant que s'il est soutenu qu'en ne prévoyant pas la représentation des personnels des caisses au sein du conseil d'administration de la CANSSM, l'article 24 du décret litigieux méconnaîtrait le " principe fondamental de la sécurité sociale d'administration des caisses de sécurité sociale par des représentants des employeurs et des salariés ", aucun principe constitutionnel, notamment pas le principe de participation des travailleurs à la gestion de l'entreprise énoncé par le huitième alinéa du Préambule de la Constitution de 1946, n'impose une telle représentation ; que, par suite, le moyen ne peut, en tout état de cause, qu'être écarté ;<br/>
<br/>
              En ce qui concerne l'article 62 :<br/>
<br/>
              15. Considérant que le principe d'égalité ne s'oppose pas à ce que l'autorité investie du pouvoir réglementaire règle de façon différente des situations différentes ni à ce qu'elle déroge à l'égalité pour des raisons d'intérêt général, pourvu que la différence de traitement qui en résulte soit, dans l'un comme l'autre cas, en rapport avec l'objet de la norme qui l'établit et ne soit pas manifestement disproportionnée au regard des motifs susceptibles de la justifier ;<br/>
<br/>
              16. Considérant que si l'article 62 du décret attaqué permet aux affiliés du régime spécial de sécurité sociale dans les mines d'obtenir désormais dans les pharmacies libérales, dans les mêmes conditions que dans les pharmacies minières, la délivrance sans avance de frais des médicaments, une telle disposition n'impliquait pas, en tout état de cause, que, pour respecter le principe d'égalité entre ces deux catégories de pharmacies, ce même décret autorise  la délivrance par les pharmacies minières de médicaments à d'autres personnes que les affiliés du régime minier ; que, par suite, le moyen tiré de la méconnaissance du principe d'égalité ne peut qu'être écarté ;<br/>
<br/>
              En ce qui concerne l'article 79 :<br/>
<br/>
              17. Considérant que l'Agence nationale pour la garantie des droits des mineurs (ANGDM) a été créée sous la forme d'un établissement public administratif par la loi du 3 février 2004 aux fins d'assumer " les obligations de l'employeur, en lieu et place des entreprises minières et ardoisières ayant définitivement cessé leur activité " ; qu'aux termes de l'article 2 de cette loi : " l'Agence nationale pour la garantie des droits des mineurs liquide, verse ou attribue l'ensemble des prestations dues aux anciens agents des entreprises minières et ardoisières ayant cessé définitivement leur activité, aux anciens agents de leurs filiales relevant du régime spécial de la sécurité sociale dans les mines et à leurs ayants droit à l'exception (...) de celles prévues par le code de la sécurité sociale et les textes relatifs au régime spécial de la sécurité sociale dans les mines (...) " ; qu'il résulte de ces dispositions qu'en permettant à l'ANGDM, par l'article 79 du décret attaqué, de gérer l'action sanitaire et sociale précédemment assurée par les CARMI et en lui transmettant, par le même article, la gestion de ces prestations, qui relèvent de la sécurité sociale, à compter du 31 mars 2012 au plus tard, le pouvoir réglementaire a méconnu les dispositions précitées de l'article 2 de la loi du 3 février 2004 ; que, par suite, les dispositions de l'article 79, qui sont divisibles des autres dispositions du décret, doivent être annulées ;<br/>
<br/>
              En ce qui concerne l'article 81 :<br/>
<br/>
              18. Considérant que le décret du 3 mai 2002 modifiant le décret du 27 novembre 1946 portant organisation de la sécurité sociale dans les mines a procédé à une revalorisation graduée des pensions minières liquidées entre 1987 et 2001, allant de 0,5 % pour les pensions liquidées en 1987 à 17 % pour les pensions liquidées en 2001, afin de compenser l'écart né au cours de cette période, au détriment des premières, entre les pensions du régime de base des mines et celles du régime général des salariés ; qu'en complément de ces premières mesures, qui se sont notamment traduites par une revalorisation de 5 % des pensions liquidées en 1995, l'article 81 du décret attaqué procède à une revalorisation graduée des pensions liquidées avant cette date, afin de faire bénéficier les personnes concernées d'une revalorisation globale de 5 % à l'horizon 2015 ; qu'il réserve toutefois cet avantage, contrairement à ce que prévoyait le décret de 2002, aux seuls " affiliés justifiant d'au moins cent vingt trimestres susceptibles d'être pris en compte pour le calcul de la pension de vieillesse " ; que compte tenu de ses effets sur les montants servis et en l'absence de toute justification par le ministre, la différence de traitement ainsi retenue ne peut être regardée que comme manifestement disproportionnée ; que, par suite, si le principe d'égalité ne s'oppose pas à ce qu'une mesure de revalorisation soit réservée aux seuls affiliés justifiant d'une certaine durée d'affiliation et s'il reste loisible au pouvoir réglementaire de prévoir des dispositions en ce sens, l'article 81, qui est divisible des autres dispositions du décret, doit, dans la mesure indiquée, être annulé ;<br/>
<br/>
              19. Considérant qu'il résulte de tout ce qui précède que les requérants sont seulement fondés à demander l'annulation de l'article 79 du décret du 30 août 2011, ainsi que celle de l'article 81 du même décret, en tant qu'il limite aux seuls affiliés justifiant d'au moins cent vingt trimestres susceptibles d'être pris en compte pour le calcul de la pension de vieillesse les majorations du nombre de trimestres de services retenu pour le calcul des avantages mentionnés aux articles 125, 147 et 151 du décret du 27 novembre 1946 ;<br/>
<br/>
              Sur les conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              20. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement d'une somme de 1 500 euros au syndicat national CFDT des mineurs et assimilés et du personnel du régime minier et de la même somme à la fédération nationale des mines et de l'énergie CGT ; qu'il n'y a pas lieu, en revanche, de mettre à la charge de l'Etat la somme que demande au même titre la fédération de l'énergie et des mines Force ouvrière ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de statuer sur les conclusions des requêtes tendant à l'annulation de l'article 80 du décret n° 2011-1034 du 30 août 2011.<br/>
Article 2 : L'article 79 du décret n° 2011-1034 du 30 août 2011 et l'article 81 du même décret, en tant qu'il limite aux seuls affiliés justifiant d'au moins cent vingt trimestres susceptibles d'être pris en compte pour le calcul de la pension de vieillesse les majorations du nombre de trimestres de services retenu pour le calcul des avantages mentionnés aux articles 125, 147 et 151 du décret du 27 novembre 1946, sont annulés.<br/>
Article 3 : L'Etat versera au syndicat national CFDT des mineurs et assimilés et du personnel du régime minier et à la fédération nationale des mines et de l'énergie CGT une somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Le surplus des conclusions des requêtes est rejeté.<br/>
Article 5 : La présente décision sera notifiée au syndicat national CFDT des mineurs et assimilés et du personnel du régime minier, à la fédération nationale des mines et de l'énergie CGT, à la fédération de l'énergie et des mines Force Ouvrière, au Premier ministre et à la ministre des affaires sociales et de la santé.<br/>
Copie en sera adressée au ministre de l'économie et des finances.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-04-03-01 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. PRINCIPES GÉNÉRAUX DU DROIT. ÉGALITÉ DEVANT LA LOI. - REVALORISATION DES PENSIONS DÉJÀ LIQUIDÉES DU RÉGIME DES MINES - MESURE DE REVALORISATION NE CONCERNANT QUE LES AFFILIÉS JUSTIFIANT D'AU MOINS CENT VINGT TRIMESTRES À PRENDRE EN COMPTE POUR LE CALCUL DE LEUR PENSION - 1) MÉCONNAISSANCE DU PRINCIPE D'ÉGALITÉ - EXISTENCE - MOTIF - DIFFÉRENCE DE TRAITEMENT MANIFESTEMENT DISPROPORTIONNÉE - 2) CONSÉQUENCES - ANNULATION DES DISPOSITIONS EN CAUSE EN TANT QU'ELLES RÉSERVENT LE BÉNÉFICE DE LA MESURE À CETTE CATÉGORIE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">40-01-02-03 MINES ET CARRIÈRES. MINES. EXPLOITATION DES MINES. RÉGIME DU PERSONNEL. - RÉGIME DE RETRAITE DES MINEURS - REVALORISATION DES PENSIONS DÉJÀ LIQUIDÉES - MESURE DE REVALORISATION, VISANT À ATTEINDRE UNE REVALORISATION GLOBALE DE 5% DES PENSIONS DÉJÀ LIQUIDÉES, NE CONCERNANT QUE LES AFFILIÉS JUSTIFIANT D'AU MOINS CENT VINGT TRIMESTRES À PRENDRE EN COMPTE POUR LE CALCUL DE LEUR PENSION - 1) MÉCONNAISSANCE DU PRINCIPE D'ÉGALITÉ - EXISTENCE - MOTIF - DIFFÉRENCE DE TRAITEMENT MANIFESTEMENT DISPROPORTIONNÉE - 2) CONSÉQUENCES - ANNULATION DES DISPOSITIONS EN CAUSE EN TANT QU'ELLES RÉSERVENT LE BÉNÉFICE DE LA MESURE À CETTE CATÉGORIE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">48-03-05 PENSIONS. RÉGIMES PARTICULIERS DE RETRAITE. PENSIONS DIVERSES. - RÉGIME DES MINES - REVALORISATION DES PENSIONS DÉJÀ LIQUIDÉES - MESURE DE REVALORISATION, VISANT À ATTEINDRE UNE REVALORISATION GLOBALE DE 5% DES PENSIONS DÉJÀ LIQUIDÉES, NE CONCERNANT QUE LES AFFILIÉS JUSTIFIANT D'AU MOINS CENT VINGT TRIMESTRES À PRENDRE EN COMPTE POUR LE CALCUL DE LEUR PENSION - 1) MÉCONNAISSANCE DU PRINCIPE D'ÉGALITÉ - EXISTENCE - MOTIF - DIFFÉRENCE DE TRAITEMENT MANIFESTEMENT DISPROPORTIONNÉE - 2) CONSÉQUENCES - ANNULATION DES DISPOSITIONS EN CAUSE EN TANT QU'ELLES RÉSERVENT LE BÉNÉFICE DE LA MESURE À CETTE CATÉGORIE.
</SCT>
<ANA ID="9A"> 01-04-03-01 Dispositions du décret n° 2011-1034 du 30 août 2011 relatif au régime spécial de sécurité sociale dans les mines réservant aux seuls affiliés justifiant d'au moins cent vingt trimestres susceptibles d'être pris en compte pour le calcul de la pension de vieillesse une mesure de revalorisation des pensions déjà liquidées qui vise, en combinaison avec de précédentes mesures de revalorisation, à faire bénéficier l'ensemble des personnes concernées d'une revalorisation globale de 5 % à l'horizon 2015 quelle que soit leur année de liquidation.... ,,1) Compte tenu de ses effets sur les montants servis et en l'absence de toute justification par le ministre en défense, la différence de traitement ainsi retenue ne peut être regardée que comme manifestement disproportionnée.... ,,2) Par suite, si le principe d'égalité ne s'oppose pas à ce qu'une mesure de revalorisation soit réservée aux seuls affiliés justifiant d'une certaine durée d'affiliation et s'il reste loisible au pouvoir réglementaire de prévoir des dispositions en ce sens, les dispositions litigieuses, qui sont divisibles des autres dispositions du décret, doivent, dans cette mesure, être annulées.</ANA>
<ANA ID="9B"> 40-01-02-03 Dispositions du décret n° 2011-1034 du 30 août 2011 relatif au régime spécial de sécurité sociale dans les mines réservant aux seuls affiliés justifiant d'au moins cent vingt trimestres susceptibles d'être pris en compte pour le calcul de la pension de vieillesse une mesure de revalorisation des pensions déjà liquidées qui vise, en combinaison avec de précédentes mesures de revalorisation, à faire bénéficier l'ensemble des personnes concernées d'une revalorisation globale de 5 % à l'horizon 2015 quelle que soit leur année de liquidation.... ,,1) Compte tenu de ses effets sur les montants servis et en l'absence de toute justification par le ministre en défense, la différence de traitement ainsi retenue ne peut être regardée que comme manifestement disproportionnée.... ,,2) Par suite, si le principe d'égalité ne s'oppose pas à ce qu'une mesure de revalorisation soit réservée aux seuls affiliés justifiant d'une certaine durée d'affiliation et s'il reste loisible au pouvoir réglementaire de prévoir des dispositions en ce sens, les dispositions litigieuses, qui sont divisibles des autres dispositions du décret, doivent, dans cette mesure, être annulées.</ANA>
<ANA ID="9C"> 48-03-05 Dispositions du décret n° 2011-1034 du 30 août 2011 relatif au régime spécial de sécurité sociale dans les mines réservant aux seuls affiliés justifiant d'au moins cent vingt trimestres susceptibles d'être pris en compte pour le calcul de la pension de vieillesse une mesure de revalorisation des pensions déjà liquidées qui vise, en combinaison avec de précédentes mesures de revalorisation, à faire bénéficier l'ensemble des personnes concernées d'une revalorisation globale de 5 % à l'horizon 2015 quelle que soit leur année de liquidation.... ,,1) Compte tenu de ses effets sur les montants servis et en l'absence de toute justification par le ministre en défense, la différence de traitement ainsi retenue ne peut être regardée que comme manifestement disproportionnée.... ,,2) Par suite, si le principe d'égalité ne s'oppose pas à ce qu'une mesure de revalorisation soit réservée aux seuls affiliés justifiant d'une certaine durée d'affiliation et s'il reste loisible au pouvoir réglementaire de prévoir des dispositions en ce sens, les dispositions litigieuses, qui sont divisibles des autres dispositions du décret, doivent, dans cette mesure, être annulées.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
