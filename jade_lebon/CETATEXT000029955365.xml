<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029955365</ID>
<ANCIEN_ID>JG_L_2014_12_000000366440</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/95/53/CETATEXT000029955365.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 6ème SSR, 23/12/2014, 366440</TITRE>
<DATE_DEC>2014-12-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>366440</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 6ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Julia Beurton</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Maud Vialettes</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2014:366440.20141223</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu 1°, sous le n° 366440, la requête, enregistrée le 28 février 2013 au secrétariat du contentieux du Conseil d'Etat, présentée par l'association FASTE Sud Aveyron, dont le siège est à Brox à Brusque (12360), représentée par son président ; l'association requérante demande au Conseil d'Etat d'annuler pour excès de pouvoir le décret n° 2013-11 du 4 janvier 2013 relatif à la tarification et au financement des lieux de vie et d'accueil et modifiant le code de l'action sociale et des familles ;<br/>
<br/>
              Vu 2°, sous le n° 366563, la requête sommaire et le mémoire complémentaire, enregistrés les 4 mars et 2 avril 2013 au secrétariat du contentieux du Conseil d'Etat, présentés par le groupe d'étude et de recherche sur la pratique des lieux d'accueil (GERPLA), dont le siège est situé lieu-dit " La Coumette " à Pouyloubrin (32260), représenté par le coordinateur de son comité national ; le groupe requérant demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'article 1er du décret n° 2013-11 du 4 janvier 2013 ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 200 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu 3°, sous le n° 366583, la requête, enregistrée le 5 mars 2013 au secrétariat du contentieux du Conseil d'Etat, présentée par la fédération nationale des lieux de vie et d'accueil, dont le siège social est situé 8 rue Lemercier à Paris (75017), représentée par son président ; la fédération requérante demande au Conseil d'Etat d'annuler pour excès de pouvoir le décret n° 2013-11 du 4 janvier 2013 en tant qu'il introduit dans le code de l'action sociale et des familles le premier alinéa du 1° du II de l'article D. 315-5, le g) du 1° du II du même article D. 315-5 et le 3° du IV de l'article D. 316-6 ; <br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 11 décembre 2014, présentée par le ministre des affaires sociales, de la santé et des droits des femmes ; <br/>
<br/>
              Vu la Constitution ; <br/>
<br/>
              Vu le code de l'action sociale et des familles ;<br/>
<br/>
              Vu le code général des impôts ;<br/>
<br/>
              Vu la décision du 14 février 2014 par laquelle le Conseil d'Etat statuant au contentieux n'a pas renvoyé au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par l'association FASTE Sud Aveyron ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Julia Beurton, auditeur,  <br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que les requêtes de l'association FASTE Sud Aveyron, du groupe d'étude et de recherche sur la pratique des lieux d'accueil et de la fédération nationale des lieux de vie et d'accueil sont dirigées contre le même décret ; qu'il y a lieu de les joindre pour statuer par une seule décision ;<br/>
<br/>
              2. Considérant qu'aux termes du III de l'article L. 312-1 du code de l'action sociale et des familles : " Les lieux de vie et d'accueil qui ne constituent pas des établissements et services sociaux ou médico-sociaux au sens du I doivent faire l'application des articles L. 311-4 à L. 311-8. Ils sont également soumis à l'autorisation mentionnée à l'article L. 313-1 et aux dispositions des articles L. 313-13 à L. 313-25, dès lors qu'ils ne relèvent ni des dispositions prévues au titre II du livre IV relatives aux assistants maternels, ni de celles relatives aux particuliers accueillant des personnes âgées ou handicapées prévues au titre IV dudit livre. Un décret fixe le nombre minimal et maximal des personnes que ces structures peuvent accueillir et leurs règles de financement et de tarification " ; qu'en application de ces dispositions, le décret du 4 janvier 2013 relatif à la tarification et au financement des lieux de vie et d'accueil prévoit, au IV de l'article D. 316-2 et aux articles D. 316-5 et D. 316-6 du code de l'action sociale et des familles, les modalités de financement et la procédure de tarification applicables à ces structures ; <br/>
<br/>
              Sur la fin de non-recevoir opposée en défense à la requête de la Fédération nationale des lieux de vie et d'accueil : <br/>
<br/>
              3. Considérant que la requête de la Fédération nationale des lieux de vie et d'accueil contient l'exposé de moyens et est, par suite, recevable ; que la fin de non-recevoir soulevée par le ministre pour ce motif doit, dès lors, être écartée ; <br/>
<br/>
              Sur la compétence du pouvoir réglementaire pour fixer les règles de financement applicables aux lieux de vie et d'accueil :<br/>
<br/>
              4. Considérant, en premier lieu, que le III de l'article L. 312-1 du code de l'action sociale et des familles renvoie à un décret la fixation des règles de financement et de tarification des lieux de vie et d'accueil ; que ces dispositions s'appliquent à l'ensemble des lieux de vie et d'accueil sans distinction selon leur statut juridique ; que la circonstance que les lieux de vie et d'accueil soient gérés par des personnes physiques et non par des personnes morales est sans incidence sur la compétence du pouvoir réglementaire pour déterminer les règles de financement qui leur sont applicables ; <br/>
<br/>
              5. Considérant, en second lieu, qu'il résulte des termes mêmes du III de l'article L. 312-1 du code de l'action sociale et des familles qu'il renvoie la fixation des règles de financement et de tarification qu'il prévoit à un décret ; que, par suite, le pouvoir réglementaire, agissant par voie de décret, est compétent pour fixer ces règles alors même qu'elles sont susceptibles de s'appliquer aux départements, lorsqu'ils financent des lieux de vie et d'accueil ; que, dès lors, il ne peut être utilement soutenu que les articles 72 et 72-2 de la Constitution imposeraient l'adoption des dispositions litigieuses par la loi ou par un décret en Conseil d'Etat ; <br/>
<br/>
              Sur l'application aux lieux de vie et d'accueil de la nomenclature budgétaire et comptable applicable aux établissements et services sociaux ou médico-sociaux :<br/>
<br/>
              6. Considérant que le deuxième alinéa du I de l'article D. 316-5 du code de l'action sociale et des familles, issu du décret attaqué, dispose que, à la création du lieu de vie et d'accueil puis tous les trois ans, la personne ayant qualité pour le représenter adresse aux autorités compétentes une proposition de forfait journalier, fondée sur un projet de budget respectant la nomenclature comptable applicable aux établissements et services sociaux ou médico-sociaux, mentionnée à l'article R. 314-5 du même code ; que, contrairement à ce que soutiennent les requérants, la circonstance que les lieux de vie et d'accueil ne soient pas des établissements ou des services sociaux ou médico-sociaux au sens du I de l'article L. 312-1 du code de l'action sociale et des familles ne fait pas obstacle à ce que le pouvoir réglementaire puisse légalement prévoir que le budget des lieux de vie et d'accueil est établi en référence aux normes comptables applicables à ces établissements ; <br/>
<br/>
              Sur l'instauration d'un forfait journalier : <br/>
<br/>
              7. Considérant que le décret attaqué prévoit, à l'article D. 316-5 du code de l'action sociale et des familles, que la prise en charge financière des personnes accueillies dans un lieu de vie et d'accueil est assurée par un forfait journalier versé, selon les cas prévus au IV de l'article D. 316-2 du même code, par le département, l'Etat, les établissements sanitaires ou médico-sociaux ou les familles ; que le montant du forfait journalier est composé d'un forfait de base, dont le montant et l'objet sont précisés au 1° du II de l'article D. 316-5, et, le cas échéant, d'un forfait complémentaire ; <br/>
<br/>
              8. Considérant, en premier lieu, qu'en vertu du premier alinéa du 1° du II de l'article D. 316-5 du code de l'action sociale et des familles, le montant du forfait de base du forfait journalier de prise en charge ne peut être supérieur à 14,5 fois la valeur horaire du salaire minimum de croissance ; qu'il ne ressort pas des pièces du dossier que le plafonnement à ce montant serait entaché d'erreur manifeste d'appréciation ; que, lorsque le projet du lieu de vie et d'accueil repose sur des modes d'organisation particuliers ou fait appel à des supports spécifiques, le forfait de base peut être complété par un forfait complémentaire destiné à prendre en charge tout ou partie des dépenses non prévues dans le forfait de base ; que s'il est soutenu que le décret ne comprendrait pas de garanties suffisantes, il résulte néanmoins de l'ensemble des dispositions litigieuses que l'autorité de tarification compétente doit arrêter ce forfait, au vu de la proposition du représentant du lieu de vie et d'accueil, à un niveau permettant, dans le respect du montant maximal du forfait de base, la prise en charge des dépenses nécessaires à l'accueil des personnes qui lui sont adressées ; <br/>
<br/>
              9. Considérant, en deuxième lieu, que si les requérants allèguent que l'énumération des dépenses incluses dans le forfait de base réduirait les missions des lieux de vie et d'accueil à une succession de " charges " et méconnaîtrait la spécificité de leur fonctionnement, en évoquant notamment la " rémunération " des permanents alors que ceux-ci sont des travailleurs indépendants, la disposition litigieuse se borne à énumérer les dépenses que le forfait de base est destiné à couvrir et n'a ni pour objet ni pour effet de modifier les missions de ces lieux de vie ; qu'il ne peut pas davantage être utilement soutenu que les dépenses énumérées méconnaîtraient les règles de financement des lieux de vie et d'accueil ; <br/>
<br/>
              10. Considérant, en troisième lieu, que le d) du 1° du II de l'article D. 316-5 du code de l'action sociale et des familles prévoit que " les allocations arrêtées par les départements d'accueil en faveur des mineurs et des jeunes majeurs confiés par un service d'aide sociale à l'enfance " sont incluses dans le forfait de base du forfait journalier ; que, contrairement à ce que soutiennent les requérants, ces dispositions, qui relèvent de la compétence du pouvoir réglementaire, ne méconnaissent ni le principe de libre administration des collectivités territoriales ni leur autonomie financière ; qu'elles ne font pas obstacle à ce que la famille du mineur ou du jeune majeur pris en charge participe à son entretien ; qu'enfin, elles n'instaurent pas de différence de traitement entre mineurs et jeunes majeurs accueillis par des lieux de vie et d'accueil d'un même département selon qu'ils ont été adressés ou orientés vers un tel lieu par ce département ou par un autre département ; <br/>
<br/>
              11. Considérant, en dernier lieu, que le g) du 1° du II de l'article D. 316-5 du code de l'action sociale et des familles inclut dans le forfait de base du forfait journalier " la taxe nette sur la valeur ajoutée pour la fourniture de logement et de nourriture dès lors que ces services constituent les prestations principales couvertes par le forfait journalier " ; que cette disposition n'a ni pour objet ni pour effet de modifier le régime fiscal des lieux de vie et d'accueil ; que la circonstance qu'elle soit sans objet compte tenu de l'exonération de la taxe sur la valeur ajoutée dont bénéficient les prestations de services et les livraisons de biens qui leur sont étroitement liées effectuées dans les lieux de vie et d'accueil, en vertu de l'article 261 du code général des impôts, est sans incidence sur sa légalité ; que les requérants ne sont, en tout état de cause, pas davantage fondés à soutenir que cette disposition méconnaîtrait leurs missions, définies à l'article D. 316-1 du code de l'action sociale et des familles, en ce qu'elle les restreindrait à la fourniture de logement et de nourriture ; <br/>
<br/>
              Sur la convention triennale de prise en charge conclue entre l'organisme financier et le lieu de vie et d'accueil : <br/>
<br/>
              12. Considérant qu'aux termes du II de l'article D. 316-6 du code de l'action sociale et des familles, issu du décret attaqué : " Chaque organisme financeur peut conclure avec la personne ayant qualité pour représenter le lieu de vie et d'accueil une convention triennale de prise en charge déterminant, notamment, les conditions d'exercice des prestations et les modalités de versement des forfaits journaliers fixés dans les conditions prévues à l'article D. 316-5 " ; <br/>
<br/>
              13. Considérant, en premier lieu, qu'en vertu des dispositions combinées des articles L. 311-4 et L. 312-1 du code de l'action sociale et des familles, les lieux de vie et d'accueil ont l'obligation soit de conclure avec chaque personne accueillie ou son représentant un contrat de séjour soit d'élaborer avec elle un document individuel de prise en charge ; que la convention triennale conclue entre l'organisme financeur et la personne représentant le lieu de vie et d'accueil, mentionnée à l'article D. 316-6 du code de l'action sociale et des familles, a un objet différent du contrat de séjour prévu par l'article L. 311-4 de ce code ; que, par suite, le moyen tiré de ce que la faculté de conventionnement instituée au II de l'article D. 316-6 méconnaîtrait l'obligation de conclure des contrats de séjour qui s'impose aux lieux de vie et d'accueil ne peut qu'être écarté ; <br/>
<br/>
              14. Considérant, en second lieu, que les missions des lieux de vie et d'accueil sont précisées à l'article D. 316-1 du code de l'action sociale et des familles ; que la circonstance que le II de l'article D. 316-6 du même code se borne à évoquer les " prestations " de ces lieux de vie, sans les définir précisément, n'est pas de nature à entacher le décret attaqué d'illégalité ; que le moyen tiré de ce que le décret n'aurait pu légalement se borner à prévoir que les conventions triennales déterminent les conditions d'exercice des prestations doit, dès lors, être écarté ;  <br/>
<br/>
              Sur l'obligation de transmettre chaque année un compte d'emploi : <br/>
<br/>
              15. Considérant qu'aux termes du III de l'article D. 316-6 du code de l'action sociale et des familles, issu du décret attaqué : " Les lieux de vie et d'accueil transmettent chaque année avant le 30 avril aux organismes financeurs mentionnés au I de l'article D. 316-5 un compte d'emploi, dont le modèle est fixé par arrêté du ministre chargé de l'action sociale et du ministre de l'intérieur, relatif à l'utilisation des financements provenant des forfaits journaliers au titre de l'année précédente. Jusqu'à transmission du compte d'emploi, le montant du forfait journalier versé pour l'année considérée ne peut dépasser le montant du forfait arrêté pour l'exercice précédent " ;<br/>
<br/>
              16. Considérant que la circonstance que le forfait journalier soit versé aux lieux de vie et d'accueil en contrepartie des missions que ces lieux assurent ne fait pas obstacle à ce qu'ils soient tenus de rendre compte de l'utilisation de ce forfait ; que, par suite, le moyen tiré de ce que l'instauration d'un compte d'emploi serait illégale au motif que le forfait journalier rémunérerait une prestation et qu'il n'y aurait, dès lors, pas lieu d'en justifier l'utilisation doit être écarté ; <br/>
<br/>
              Sur les conditions de reversement aux organismes financeurs des sommes allouées :<br/>
<br/>
              17. Considérant qu'aux termes du IV de l'article D. 316-6 du code de l'action sociale et des familles, issu du décret attaqué : " Les sommes allouées sont totalement ou partiellement reversées aux organismes financeurs si elles ont couvert : / 1° Des dépenses sans rapport avec celles mentionnées au 1° du II de l'article D. 316-5 ou acceptées au titre du 2° du II du même article ; / 2° Des dépenses dont le lieu de vie et d'accueil n'est pas en mesure de justifier l'emploi ; / 3° Des dépenses dont le niveau paraît excessif, au regard de l'activité et des coûts des lieux de vie fournissant des prestations comparables " ;<br/>
<br/>
              18. Considérant que la tarification des lieux de vie et d'accueil vise à assurer le financement notamment par l'Etat et les départements des prestations fournies par ces lieux de vie ; qu'eu égard à l'objet de la tarification, le pouvoir réglementaire, habilité par le III de l'article L. 312-1 du code de l'action sociale et des familles à fixer les règles de financement et de tarification des lieux de vie et d'accueil, est compétent pour prévoir les conditions dans lesquelles les sommes procurées par la tarification qui n'auraient pas été utilisées pour la fourniture des prestations en vue desquelles elles avaient été allouées ou dont l'emploi ne serait pas justifié doivent être totalement ou partiellement reversées aux organismes financeurs ; qu'en revanche, le pouvoir réglementaire a excédé l'habilitation dont il dispose en imposant, au 3° du IV de l'article D. 316-6 du même code, un reversement des sommes qui correspondent à des dépenses admises lors de la fixation du forfait et qui ont été effectivement utilisées à cette fin ; que, par suite, les requérants sont fondés à soutenir que les dispositions du 3° du IV de l'article D. 316-6 du code de l'action sociale et des familles sont illégales ;<br/>
<br/>
              Sur la méconnaissance du principe de sécurité juridique :<br/>
<br/>
              19. Considérant, en premier lieu, que la circonstance que le décret attaqué ne mentionne pas les voies de recours existant contre les décisions relatives à la tarification des lieux de vie et d'accueil est, contrairement à ce qui est soutenu, sans incidence sur sa légalité ; <br/>
<br/>
              20. Considérant, en second lieu, que l'exercice du pouvoir réglementaire implique pour son détenteur la possibilité de modifier à tout moment les normes qu'il définit sans que les personnes auxquelles sont, le cas échéant, imposées de nouvelles contraintes puissent invoquer un droit au maintien de la réglementation ; qu'en principe, les nouvelles normes ainsi édictées ont vocation à s'appliquer immédiatement, dans le respect des exigences attachées au principe de non-rétroactivité des actes administratifs ; que, toutefois, il incombe à l'autorité investie du pouvoir réglementaire, agissant dans les limites de sa compétence et dans le respect des règles qui s'imposent à elle, d'édicter, pour des motifs de sécurité juridique, les mesures transitoires qu'implique, s'il y a lieu, cette réglementation nouvelle ; qu'il en va ainsi lorsque l'application immédiate de celle-ci entraîne, au regard de l'objet et des effets de ses dispositions, une atteinte excessive aux intérêts publics ou privés en cause ; <br/>
<br/>
              21. Considérant que le décret attaqué a pour objet de redéfinir les modalités de financement et la procédure de tarification applicables aux lieux de vie et d'accueil ; que les lieux de vie et d'accueil, dont le financement était jusqu'ici assuré par convention bilatérale établie avec chaque organisme financeur, sont désormais financés par un forfait journalier fixé uniformément par l'autorité compétente pour en autoriser la création, en fonction des frais de fonctionnement de ces structures ; que l'entrée en vigueur immédiate du décret attaqué a ainsi eu pour effet de soumettre les lieux de vie et d'accueil à un régime nouveau de tarification sans prévoir le délai indispensable à l'élaboration de la proposition de forfait journalier qu'ils doivent soumettre aux autorités compétentes et à la fixation du forfait par ces autorités ; qu'elle était ainsi susceptible d'entraîner une rupture dans le financement de certains de ces lieux, qui constituent de petites structures aux ressources limitées, et de faire obstacle à l'accueil de nouvelles personnes ; que, ce faisant, l'application immédiate du décret attaqué a porté une atteinte excessive aux intérêts des lieux de vie et d'accueil en cause et à ceux des personnes susceptibles d'être accueillies ; que les requérants sont, par suite, fondés à soutenir que l'absence de dispositions transitoires entache le décret attaqué d'illégalité ; <br/>
<br/>
              22. Considérant qu'il résulte de ce qui précède que les requérants sont fondés à demander l'annulation du décret qu'ils attaquent en tant seulement que, d'une part, il introduit dans le code de l'action sociale et des familles le 3° du IV de l'article D. 316-6, qui est divisible du reste du décret attaqué, et, d'autre part, il ne prévoit pas de dispositions transitoires ;<br/>
<br/>
              Sur les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative : <br/>
<br/>
              23. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par le groupe d'étude et de recherche sur la pratique des lieux d'accueil au titre de ces dispositions ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le décret du 4 janvier 2013 relatif à la tarification et au financement des lieux de vie et d'accueil et modifiant le code de l'action sociale et des familles est annulé en tant qu'il introduit dans ce code le 3° du IV de l'article D. 316-6 et en tant qu'il ne prévoit pas de dispositions transitoires à son entrée en vigueur. <br/>
Article 2 : La requête du Groupe d'étude et de recherche sur les pratiques des lieux d'accueil et le surplus des requêtes de l'association FASTE Sud Aveyron et de la Fédération nationale des lieux de vie et d'accueil sont rejetés.<br/>
Article 3 : La présente décision sera notifiée à l'association FASTE Sud Aveyron, au Groupe d'étude et de recherche sur la pratique des lieux d'accueil, à la Fédération nationale des lieux de vie et d'accueil, au Premier ministre et à la ministre des affaires sociales, de la santé et des droits des femmes.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-04-03-07 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. PRINCIPES GÉNÉRAUX DU DROIT. PRINCIPES INTÉRESSANT L'ACTION ADMINISTRATIVE. - PRINCIPE DE SÉCURITÉ JURIDIQUE - PORTÉE - OBLIGATION POUR L'AUTORITÉ INVESTIE DU POUVOIR RÉGLEMENTAIRE D'ÉDICTER, POUR DES MOTIFS DE SÉCURITÉ JURIDIQUE, LES MESURES TRANSITOIRES QU'IMPLIQUE, S'IL Y A LIEU, UNE RÉGLEMENTATION NOUVELLE - MÉCONNAISSANCE EN L'ESPÈCE - DÉCRET CRÉANT UN NOUVEAU RÉGIME DE TARIFICATION DES LIEUX DE VIE ET D'ACCUEIL.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">04-03-01-08 AIDE SOCIALE. INSTITUTIONS SOCIALES ET MÉDICO-SOCIALES. ÉTABLISSEMENTS - QUESTIONS COMMUNES. STRUCTURES D'HÉBERGEMENT EN VUE DE LA RÉADAPTATION SOCIALE. - LIEUX DE VIE ET D'ACCUEIL - INTRODUCTION D'UN RÉGIME NOUVEAU DE TARIFICATION (DÉCRET DU 4 JANVIER 2013) - 1) ETENDUE DE LA COMPÉTENCE DU POUVOIR RÉGLEMENTAIRE - A) INCLUSION - REVERSEMENT DES SOMMES NON UTILISÉES POUR LES FINALITÉS PRÉVUES OU DONT L'EMPLOI NE SERAIT PAS JUSTIFIÉ - B) EXCLUSION - REVERSEMENT DES SOMMES DONT LE NIVEAU PARAÎT EXCESSIF - 2) OBLIGATION DE PRÉVOIR LES MESURES TRANSITOIRES QUE LE PRINCIPE DE SÉCURITÉ JURIDIQUE IMPOSE - MÉCONNAISSANCE EN L'ESPÈCE.
</SCT>
<ANA ID="9A"> 01-04-03-07 Le décret n° 2013-11 du 4 janvier 2013 a pour objet de redéfinir les modalités de financement et la procédure de tarification applicables aux lieux de vie et d'accueil. Les lieux de vie et d'accueil, dont le financement était jusqu'ici assuré par convention bilatérale établie avec chaque organisme financeur, sont désormais financés par un forfait journalier fixé uniformément par l'autorité compétente pour en autoriser la création, en fonction des frais de fonctionnement de ces structures. L'entrée en vigueur immédiate du décret attaqué a ainsi eu pour effet de soumettre les lieux de vie et d'accueil à un régime nouveau de tarification sans prévoir le délai indispensable à l'élaboration de la proposition de forfait journalier qu'ils doivent soumettre aux autorités compétentes et à la fixation du forfait par ces autorité. Elle était ainsi susceptible d'entraîner une rupture dans le financement de certains de ces lieux, qui constituent de petites structures aux ressources limitées, et de faire obstacle à l'accueil de nouvelles personnes. Ce faisant, l'application immédiate du décret attaqué a porté une atteinte excessive aux intérêts des lieux de vie et d'accueil en cause et à ceux des personnes susceptibles d'être accueillies. Par suite, l'absence de dispositions transitoires entache ce décret d'illégalité.</ANA>
<ANA ID="9B"> 04-03-01-08 1) Le III de l'article L. 312-1 du code de l'action sociale et des familles (CASF) habilite le pouvoir réglementaire à fixer les règles de financement et de tarification des lieux de vie et d'accueil. La tarification des lieux de vie et d'accueil vise à assurer le financement, notamment par l'Etat et les départements, des prestations fournies par ces lieux de vie.... ,,a) Eu égard à l'objet de la tarification, le pouvoir réglementaire est compétent pour prévoir les conditions dans lesquelles les sommes procurées par la tarification qui n'auraient pas été utilisées pour la fourniture des prestations en vue desquelles elles avaient été allouées ou dont l'emploi ne serait pas justifié doivent être totalement ou partiellement reversées aux organismes financeurs.,,,b) En revanche, le pouvoir réglementaire a excédé l'habilitation dont il disposait en imposant, au 3° du IV de l'article D. 316-6 du CASF, un reversement des sommes qui correspondent à des dépenses admises lors de la fixation du forfait et qui ont été effectivement utilisées à cette fin, alors même que, aux termes de ces dispositions, il s'agirait de  dépenses dont le niveau paraît excessif, au regard de l'activité et des coûts des lieux de vie fournissant des prestations comparables .,,,2) Le décret n° 2013-11 du 4 janvier 2013 a pour objet de redéfinir les modalités de financement et la procédure de tarification applicables aux lieux de vie et d'accueil. Les lieux de vie et d'accueil, dont le financement était jusqu'ici assuré par convention bilatérale établie avec chaque organisme financeur, sont désormais financés par un forfait journalier fixé uniformément par l'autorité compétente pour en autoriser la création, en fonction des frais de fonctionnement de ces structures. L'entrée en vigueur immédiate du décret attaqué a ainsi eu pour effet de soumettre les lieux de vie et d'accueil à un régime nouveau de tarification sans prévoir le délai indispensable à l'élaboration de la proposition de forfait journalier qu'ils doivent soumettre aux autorités compétentes et à la fixation du forfait par ces autorité. Elle était ainsi susceptible d'entraîner une rupture dans le financement de certains de ces lieux, qui constituent de petites structures aux ressources limitées, et de faire obstacle à l'accueil de nouvelles personnes. Ce faisant, l'application immédiate du décret attaqué a porté une atteinte excessive aux intérêts des lieux de vie et d'accueil en cause et à ceux des personnes susceptibles d'être accueillies. Par suite, l'absence de dispositions transitoires entache ce décret d'illégalité.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., pour la tarification des établissements sociaux et médico-sociaux, CE, 2 octobre 2013, Association chrétienne de réadaptation, n° 366884, T. p. 436.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
