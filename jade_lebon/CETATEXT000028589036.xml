<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028589036</ID>
<ANCIEN_ID>JG_L_2014_02_000000352878</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/58/90/CETATEXT000028589036.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 1ère sous-sections réunies, 12/02/2014, 352878</TITRE>
<DATE_DEC>2014-02-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>352878</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 1ère sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DIDIER, PINET</AVOCATS>
<RAPPORTEUR>M. Bruno Chavanat</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:352878.20140212</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 22 septembre et 19 décembre 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. A...B..., demeurant ... ; M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 10BX00743 du 30 juin 2011 par lequel la cour administrative d'appel de Bordeaux a rejeté sa requête tendant, d'une part, à l'annulation du jugement n° 0502360 du 23 décembre 2009 par lequel le tribunal administratif de Toulouse a rejeté sa demande tendant à l'annulation de l'arrêté du 20 juillet 2004 par lequel le ministre de l'intérieur, de la sécurité intérieure et des libertés locales l'a licencié pour insuffisance professionnelle, d'autre part, à l'annulation de cet arrêté ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement d'une somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi n° 83-634 du 13 juillet 1983 ;<br/>
<br/>
              Vu la loi n° 84-16 du 11 janvier 1984 ;<br/>
<br/>
<br/>
              Vu le décret n° 84-961 du 25 octobre 1984 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bruno Chavanat, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Didier, Pinet, avocat de M. B...;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M.B..., ingénieur des travaux des services techniques à la délégation régionale de Toulouse du secrétariat général pour l'administration de la police du Sud-Ouest, a fait l'objet en 2002 d'une sanction de mise à la retraite d'office ; que, saisi par l'intéressé, le Conseil supérieur de la fonction publique de l'Etat a émis l'avis que l'administration avait à tort  mis en oeuvre une procédure disciplinaire et aurait été seulement fondée à engager une procédure de licenciement pour insuffisance professionnelle ; que la sanction  a été retirée par arrêté du 15 mars 2004 ; que, par une décision du 20 juillet 2004, le ministre de l'intérieur a prononcé le licenciement pour insuffisance professionnelle de M. B...; que, par un jugement du 23 décembre 2009, le tribunal administratif de Toulouse a rejeté la demande d'annulation de cette décision ; que, par un arrêt du 30 juin 2011, contre lequel M. B...se pourvoit en cassation, la cour administrative d'appel de Bordeaux a rejeté la requête de l'intéressé dirigée contre ce jugement ;<br/>
<br/>
              Sur la régularité de l'arrêt attaqué : <br/>
<br/>
              2. Considérant, en premier lieu, que les moyens tirés de ce que la cour aurait omis de se prononcer sur les moyens tirés de l'irrégularité de l'avis du conseil de discipline, compte tenu notamment des insuffisances du rapport qui lui a été soumis, du défaut de lecture en séance des observations écrites de M.B..., en méconnaissance des dispositions de l'article 5 du décret du 25 octobre 1984, et de l'erreur manifeste d'appréciation dont serait entachée la décision litigieuse manquent en fait ;<br/>
<br/>
              3. Considérant, en deuxième lieu, que M. B...soutient que la cour aurait omis de se prononcer sur un autre moyen qu'il avait soulevé devant le tribunal administratif et que celui-ci avait écarté, tiré de la méconnaissance du principe d'impartialité ; qu'il ressort toutefois des pièces du dosser soumis à la cour que si M. B...a indiqué, dans son mémoire d'appel, qu'il entend " soumettre à la juridiction de céans l'intégralité des moyens développés aux fins d'annulation de l'arrêté en date du 20 juillet 2004 prononçant son licenciement pour insuffisance professionnelle, et y ajouter, dans le cadre dévolutif de la présente procédure", il n'a ni exposé un tel moyen devant la cour ni joint une copie de sa demande de première instance sur ce point ; que, dans ces conditions, il ne peut être regardé comme ayant  repris ce moyen en appel ;  que, dès lors, la cour n'avait pas à y répondre ;<br/>
<br/>
              Sur le bien fondé de l'arrêt :<br/>
<br/>
              4. Considérant, en premier lieu, qu'aux termes de l'article 2 du décret du 25 octobre 1984 relatif à la procédure disciplinaire concernant les fonctionnaires de l'Etat et applicable au licenciement pour insuffisance professionnelle en vertu de l'article 70 de la loi du 11 janvier 1984 portant dispositions statutaires applicables à la fonction publique de l'Etat : " L'organisme siégeant en conseil de discipline (...) est saisi par un rapport émanant de l'autorité ayant pouvoir disciplinaire (...). " ; qu'en vertu de  l'article 3 du même décret, le fonctionnaire poursuivi peut présenter devant le conseil de discipline des observations écrites ou orales ; qu'aux termes de l'article 5 du même décret : " Le rapport établi par l'autorité ayant pouvoir disciplinaire (...) et les observations écrites éventuellement présentées par le fonctionnaire sont lus en séance. " ; que l'article 8 du même décret dispose : " Le conseil de discipline, au vu des observations écrites produites devant lui et compte tenu, le cas échéant, des déclarations orales de l'intéressé et des témoins ainsi que des résultats de l'enquête à laquelle il a pu être procédé, émet un avis motivé sur les suites qui lui paraissent devoir être réservées à la procédure disciplinaire engagée.(...)" ;<br/>
<br/>
              5. Considérant, d'une part, que si les actes administratifs doivent être pris selon les formes et conformément aux procédures prévues par les lois et règlements, un vice affectant le déroulement d'une procédure administrative préalable, suivie à titre obligatoire ou facultatif, n'est de nature à entacher d'illégalité la décision prise que s'il ressort des pièces du dossier qu'il a été susceptible d'exercer, en l'espèce, une influence sur le sens de la décision prise ou qu'il a privé les intéressés d'une garantie ; que la communication du rapport émanant de l'autorité ayant le pouvoir disciplinaire, en temps utile avant la séance, au fonctionnaire déféré devant le conseil de discipline et aux membres de celui-ci satisfait aux fins en vue desquelles sa lecture a été prévue par les dispositions précitées, notamment au respect des droits de la défense ; qu'ainsi la lecture du rapport en séance ne peut être regardée, en elle-même, comme une garantie dont la seule méconnaissance suffirait à entacher d'illégalité la décision prise à l'issue de la procédure ; que, dès lors, contrairement à ce que soutient le requérant, en jugeant que l'arrêté litigieux n'était pas illégal au seul motif que le rapport n'avait pas été lu en séance et que, dans les circonstances de l'espèce qui lui était soumise, le rapport ayant été communiqué avant la séance à l'intéressé et ce dernier ayant été mis en mesure de se défendre sur l'ensemble des griefs formulés à son encontre, la procédure n'était entachée d'aucune irrégularité, la cour n'a pas commis d'erreur de droit ; <br/>
<br/>
              6. Considérant, d'autre part, qu'après avoir relevé que M. B...avait eu la faculté de lire ses observations écrites en séance et qu'il avait été mis à même de formuler des observations orales, pour résumer ses observations écrites ou développer son argumentation, la cour a pu juger, sans méconnaître la portée des dispositions précitées du décret du 25 octobre 1984, qui n'imposent pas que les observations écrites de l'intéressé soient lues en séance par une autre personne que lui-même, ni commettre d'erreur de droit, que la procédure n'était entachée d'aucune irrégularité sur ce point ;<br/>
<br/>
              7. Considérant, en deuxième lieu, que la circonstance que certains des faits retenus pour justifier un licenciement pour insuffisance professionnelle seraient susceptibles de recevoir une qualification disciplinaire, n'est pas, par elle-même, de nature à entacher cette mesure d'illégalité, dès lors que l'administration se fonde sur des éléments révélant l'inaptitude de l'agent au regard des exigences de capacité qu'elle est en droit d'attendre d'un fonctionnaire de son grade ; que si le requérant soutient que l'administration lui a reproché, notamment à l'occasion de sa procédure de notation, certains faits susceptibles d'être qualifiés de fautes disciplinaires, il ressort des énonciations de l'arrêt attaqué que la cour a tenu compte d'un ensemble d'éléments démontrant son inaptitude professionnelle, tels que son inaptitude à accomplir les missions normalement dévolues à un ingénieur des services techniques ou la faiblesse de son activité, et, par là-même, de nature à justifier la mesure litigieuse ; qu'en jugeant que les faits sur lesquels s'est fondée l'administration justifiaient le licenciement de M. B...pour insuffisance professionnelle, la cour n'a ainsi commis aucune erreur de qualification juridique des faits ni aucune erreur de droit ; <br/>
<br/>
              8. Considérant, en troisième lieu, qu'il appartient à un agent public qui soutient avoir été victime d'agissements constitutifs de harcèlement moral de soumettre au juge des éléments de fait susceptibles de faire présumer l'existence d'un tel harcèlement ; qu'il incombe à l'administration de produire, en sens contraire, une argumentation de nature à démontrer que les agissements en cause sont justifiés par des considérations étrangères à tout harcèlement ; que la conviction du juge, à qui il revient d'apprécier si les agissements de harcèlement sont ou non établis, se détermine au vu de ces échanges contradictoires, que le juge peut compléter, en cas de doute, en ordonnant toute mesure d'instruction utile ; que M. B...soutient que ces principes ont été méconnus, au prix d'une erreur de droit,  et que la cour aurait fait exclusivement peser sur lui la charge de la preuve du harcèlement moral dont il estime avoir été l'objet ; que, toutefois, ce moyen ne peut qu'être écarté, dès lors que la cour a souverainement constaté que l'intéressé ne fournissait aucun élément ou témoignage circonstancié susceptible de faire présumer l'existence d'un tel harcèlement ; <br/>
<br/>
              9. Considérant, en quatrième lieu, qu'en jugeant qu'il ne ressortait pas des pièces du dossier qui lui était soumis, compte tenu du retrait de la sanction prononcée en 2002, qui était réputée ainsi n'être jamais intervenue, que l'intéressé aurait fait l'objet d'une sanction à raison des mêmes faits que ceux qui ont entraîné son licenciement pour insuffisance professionnelle, la cour, qui a suffisamment motivé son arrêt sur ce point, n'a, contrairement à ce qui est soutenu, commis aucune erreur de qualification juridique des faits ni aucune erreur de droit ; <br/>
<br/>
              10. Considérant qu'il résulte de tout ce qui précède que M. B...n'est pas fondé à demander l'annulation de l'arrêt qu'il attaque ; <br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative : <br/>
<br/>
              11. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de L'Etat, qui n'est pas, dans la présente instance, la partie perdante, la somme que demande M. B...au titre des frais exposés par lui et non compris dans les dépens ; <br/>
<br/>
              12. Considérant que si une personne publique qui n'a pas eu recours au ministère d'avocat peut néanmoins demander au juge le bénéfice de l'article L. 761-1 du code de justice administrative au titre des frais spécifiques exposés par elle à l'occasion de l'instance, elle ne saurait se borner à faire état d'un surcroît de travail de ses services et doit faire état précisément des frais qu'elle aurait exposés pour défendre à l'instance ; que le ministre de l'intérieur demande que soit mise à la charge de M.B..., compte tenu des coûts supportés par ses services, une somme de 400 euros, évaluée sur la base de " un jour/homme pour l'élaboration du mémoire et deux heures de validation " ; que ces conclusions, qui se bornent à exciper d'un surcroît de travail des services, ne répondent pas aux exigences ci-dessus rappelées ; que, par suite, il n'y a pas lieu de mettre à la charge de M. B...la somme que demande le ministre au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
      Article 1er : Le pourvoi de M. B...est rejeté.<br/>
<br/>
Article 2 : Les conclusions du ministre de l'intérieur présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à M. A...B...et au ministre de l'intérieur.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-09-05-01 FONCTIONNAIRES ET AGENTS PUBLICS. DISCIPLINE. PROCÉDURE. CONSEIL DE DISCIPLINE. - LECTURE EN SÉANCE DU RAPPORT ÉTABLI PAR L'AUTORITÉ DISCIPLINAIRE ET DES OBSERVATIONS ÉCRITES DU FONCTIONNAIRE - GARANTIE AU SENS DE LA JURISPRUDENCE DITE DANTHONY [RJ1] - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-06-04-02 PROCÉDURE. JUGEMENTS. RÉDACTION DES JUGEMENTS. MOTIFS. - OMISSION À SE PRONONCER SUR UN MOYEN D'APPEL - ABSENCE - REQUÉRANT AYANT INDIQUÉ REPRENDRE SES MOYENS DE PREMIÈRE INSTANCE SANS EXPOSER LE MOYEN EN CAUSE DEVANT LE JUGE D'APPEL NI JOINDRE UNE COPIE DE SA REQUÊTE DE PREMIÈRE INSTANCE SUR CE POINT.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-07-01-04 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. QUESTIONS GÉNÉRALES. MOYENS. - OMISSION À SE PRONONCER SUR UN MOYEN D'APPEL - ABSENCE - REQUÉRANT AYANT INDIQUÉ REPRENDRE SES MOYENS DE PREMIÈRE INSTANCE SANS EXPOSER LE MOYEN EN CAUSE DEVANT LE JUGE D'APPEL NI JOINDRE UNE COPIE DE SA REQUÊTE DE PREMIÈRE INSTANCE SUR CE POINT.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">54-08-01 PROCÉDURE. VOIES DE RECOURS. APPEL. - OMISSION À SE PRONONCER SUR UN MOYEN D'APPEL - ABSENCE - REQUÉRANT AYANT INDIQUÉ REPRENDRE SES MOYENS DE PREMIÈRE INSTANCE SANS EXPOSER LE MOYEN EN CAUSE DEVANT LE JUGE D'APPEL NI JOINDRE UNE COPIE DE SA REQUÊTE DE PREMIÈRE INSTANCE SUR CE POINT.
</SCT>
<ANA ID="9A"> 36-09-05-01 L'article 3 du décret n° 84-961 du 25 octobre 1984 prévoit que : Le rapport établi par l'autorité ayant pouvoir disciplinaire (&#133;) et les observations écrites éventuellement présentées par le fonctionnaire sont lus en séance.... ,,La communication du rapport émanant de l'autorité ayant le pouvoir disciplinaire, en temps utile avant la séance, au fonctionnaire déféré devant le conseil de discipline et aux membres de celui-ci satisfait aux fins en vue desquelles sa lecture a été prévue par ces dispositions, et notamment au respect des droits de la défense. Ainsi, la lecture du rapport en séance ne peut être regardée, en elle-même, comme une garantie dont la seule méconnaissance suffirait à entacher d'illégalité la décision prise à l'issue de la procédure.</ANA>
<ANA ID="9B"> 54-06-04-02 Un requérant qui se borne à indiquer dans son mémoire d'appel qu'il entend soumettre à la juridiction l'intégralité des moyens développés aux fins d'annulation de la décision qu'il attaque ne peut se plaindre de ce que le juge d'appel n'a pas répondu à l'un des moyens qu'il avait soulevé en première instance alors qu'il n'a ni exposé un tel moyen devant la cour, ni joint une copie de sa demande de première instance sur ce point.</ANA>
<ANA ID="9C"> 54-07-01-04 Un requérant qui se borne à indiquer dans son mémoire d'appel qu'il entend soumettre à la juridiction l'intégralité des moyens développés aux fins d'annulation de la décision qu'il attaque ne peut se plaindre de ce que le juge d'appel n'a pas répondu à l'un des moyens qu'il avait soulevé en première instance alors qu'il n'a ni exposé un tel moyen devant la cour, ni joint une copie de sa demande de première instance sur ce point.</ANA>
<ANA ID="9D"> 54-08-01 Un requérant qui se borne à indiquer dans son mémoire d'appel qu'il entend soumettre à la juridiction l'intégralité des moyens développés aux fins d'annulation de la décision qu'il attaque ne peut se plaindre de ce que le juge d'appel n'a pas répondu à l'un des moyens qu'il avait soulevé en première instance alors qu'il n'a ni exposé un tel moyen devant la cour, ni joint une copie de sa demande de première instance sur ce point.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Assemblée, 23 décembre 2011, Danthony et autres, n° 335033, p. 649.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
