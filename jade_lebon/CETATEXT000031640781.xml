<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031640781</ID>
<ANCIEN_ID>JG_L_2015_12_000000389645</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/64/07/CETATEXT000031640781.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème / 2ème SSR, 16/12/2015, 389645</TITRE>
<DATE_DEC>2015-12-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>389645</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème / 2ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Charline Nicolas</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier Henrard</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:389645.20151216</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête enregistrée le 20 avril 2015 au secrétariat du contentieux du Conseil d'Etat, la chambre de commerce et d'industrie de Seine-et-Marne demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite de refus née, le 10 avril 2015, du silence gardé par le Premier ministre sur sa demande tendant à ce que celui-ci engage la procédure prévue au second alinéa de l'article 37 de la Constitution pour procéder par décret à  l'abrogation des dispositions du III de l'article 33 de la loi du 29 décembre 2014 de finances pour 2015 ;<br/>
<br/>
              2°) d'enjoindre au Premier ministre d'engager la procédure prévue au second alinéa de l'article 37 de la Constitution, si besoin sous astreinte ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment ses article 34 et 37; <br/>
              - la loi n° 2014-1654 du 29 décembre 2014 de finances pour 2015 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Charline Nicolas, Auditeur,  <br/>
<br/>
              - les conclusions de M. Olivier Henrard, rapporteur public ;<br/>
<br/>
<br/>
<br/>1. Considérant que le III de l'article 33 de la loi du 29 décembre 2014 de finances pour 2015 a institué pour 2015 un prélèvement de 500 millions d'euros sur les fonds de roulement de certaines chambres de commerce et d'industrie, en ont  défini les modalités de calcul et en ont fixé le montant pour chacune des chambres concernées ; que la chambre de commerce et d'industrie de Seine-et-Marne a demandé au  Premier ministre d'engager la procédure prévue au second alinéa de l'article 37 de la Constitution pour procéder par décret à l'abrogation de ces dispositions ;  qu'une décision implicite de refus est née, le 10 avril 2015, du silence gardé par le Premier ministre ; que la chambre de commerce et d'industrie demande l'annulation de cette décision ; <br/>
<br/>
              2. Considérant que les dispositions du second alinéa de l'article 37 de la Constitution ne donnent compétence au Premier ministre pour abroger des dispositions contenues dans un texte de forme législative que pour autant qu'elles sont de nature réglementaire ; qu'en vertu de l'article 34 de la Constitution : " (...) La loi détermine les principes fondamentaux : (...) - du régime de la propriété, des droits réels et des obligations civiles et commerciales " ; que, eu égard à leur objet, rappelé au point 1 ci-dessus, les dispositions contestées du III de l'article 33 de la loi du 29 décembre 2014 de finances pour 2015 ont trait à ces principes fondamentaux ; que, par suite, en refusant d'engager la procédure de déclassement de ces dispositions législatives, le Premier ministre n'a pas méconnu les prérogatives qu'il tient de l'article 37 de la Constitution ; que la requête de la chambre de commerce et d'industrie de Seine-et-Marne doit donc être rejetée ;  <br/>
<br/>
              3. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la chambre de commerce et d'industrie de Seine-et-Marne est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à la chambre de commerce et d'industrie de Seine-et-Marne et au Premier ministre.<br/>
Copie en sera adressée pour information au ministre des finances et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-02-01-02-08 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - COMPÉTENCE. LOI ET RÈGLEMENT. ARTICLES 34 ET 37 DE LA CONSTITUTION - MESURES RELEVANT DU DOMAINE DE LA LOI. PRINCIPES FONDAMENTAUX DU RÉGIME DE LA PROPRIÉTÉ. - PRÉLÈVEMENT SUR LES FONDS DE ROULEMENT DE CERTAINES CCI - INCLUSION.
</SCT>
<ANA ID="9A"> 01-02-01-02-08 Eu égard à leur objet, les dispositions du III de l'article 33 de la loi n° 2014-1654 du 29 décembre 2014 de finances pour 2015, qui ont institué pour 2015 un prélèvement sur les fonds de roulement de certaines chambres de commerce et d'industrie (CCI), ont trait aux principes fondamentaux du régime de la propriété, des droits réels et des obligations civiles et commerciales. Par suite, ces dispositions sont de nature législative et non pas réglementaire.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
