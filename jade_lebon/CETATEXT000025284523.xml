<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025284523</ID>
<ANCIEN_ID>JG_L_2011_12_000000347993</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/28/45/CETATEXT000025284523.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème et 7ème sous-sections réunies, 30/12/2011, 347993</TITRE>
<DATE_DEC>2011-12-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>347993</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème et 7ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Christian Vigouroux</PRESIDENT>
<AVOCATS>SCP CELICE, BLANCPAIN, SOLTNER</AVOCATS>
<RAPPORTEUR>Mme Catherine Chadelat</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Béatrice Bourgeois-Machureau</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 30 mars 2011 au secrétariat du contentieux du Conseil d'Etat, présentée pour le SYNDICAT DES COMPAGNIES AERIENNES AUTONOMES (SCARA), dont le siège est au 22 rue Bénard à Paris (75014) ; le SYNDICAT DES COMPAGNIES AERIENNES AUTONOMES demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite par laquelle le ministre de l'écologie, de l'énergie, du développement durable et de la mer a refusé de procéder à la modification de l'article 6 de l'arrêté du 15 novembre 1994 relatif à la répartition du trafic intracommunautaire au sein du système aéroportuaire parisien ;<br/>
<br/>
              2°) d'enjoindre au ministre chargé de l'aviation civile, à titre principal, de modifier l'article 6 de l'arrêté du 15 novembre 1994 pour permettre l'exploitation sur l'aéroport du Bourget d'aéronefs allant jusqu'à 72 sièges, hors le cas des dérogations accordées par le directeur général de l'aviation civile, à titre subsidiaire, de réexaminer sa demande tendant à la modification de cet article 6, et ce, dans l'un et l'autre cas, dans un délai de deux mois à compter de la décision à intervenir sous astreinte de 500 euros par jour de retard ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement de la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'aviation civile ;<br/>
<br/>
              Vu la loi n° 79-587 du 11 juillet 1979 modifiée ;<br/>
<br/>
              Vu l'arrêté du 15 novembre 1994 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Catherine Chadelat, Conseiller d'Etat,<br/>
<br/>
              - les observations de la SCP Célice, Blancpain, Soltner, avocat du SYNDICAT DES COMPAGNIES AERIENNES AUTONOMES,<br/>
<br/>
              - les conclusions de Mme Béatrice Bourgeois-Machureau, rapporteur public,<br/>
<br/>
              La parole ayant à nouveau été donnée à la SCP Célice, Blancpain, Soltner, avocat du SYNDICAT DES COMPAGNIES AERIENNES AUTONOMES ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant qu'aux termes du second alinéa de l'article R. 221-3 du code de l'aviation civile : " Lorsque plusieurs aérodromes ouverts à la circulation aérienne publique desservent une même région, le ministre chargé de l'aviation civile peut réglementer leur utilisation dans l'intérêt général et, notamment, réserver spécialement chacun d'eux à certains types d'appareils ou à certaines natures d'activités aériennes ou d'opérations commerciales " ; <br/>
<br/>
              Considérant que, sur le fondement de ces dispositions, l'article 6 de l'arrêté du 15 novembre 1994 relatif à la répartition du trafic intracommunautaire au sein du système aéroportuaire parisien a réservé l'aéroport du Bourget, sauf dérogations accordées par le directeur général de l'aviation civile, à l'exploitation des services aériens non réguliers sur lesquels les sièges ne sont pas commercialisés séparément auprès du public et qui sont assurés au moyen d'aéronefs d'une capacité effective inférieure ou égale à vingt cinq sièges ; que le SYNDICAT DES COMPAGNIES AERIENNES AUTONOMES a demandé au ministre chargé de l'aviation civile de modifier ces dispositions en vue de permettre l'utilisation de cet aéroport par des aéronefs de la même catégorie mais d'une capacité supérieure ; <br/>
<br/>
              Considérant, en premier lieu, que l'arrêté du 15 novembre 1994 présente un caractère réglementaire ; que le refus de le modifier présente le même caractère et n'avait, en conséquence, pas à être obligatoirement motivé en application de la loi du 11 juillet 1979 ; que le moyen tiré du défaut de motivation de la décision attaquée ne peut, par suite et en tout état de cause, qu'être écarté ; <br/>
<br/>
              Considérant, en second lieu, que l'autorité compétente, saisie d'une demande tendant à l'abrogation ou à la modification d'un règlement illégal est tenue d'y déférer, soit que ce règlement ait été illégal dès la date de sa signature, soit que l'illégalité résulte de circonstances de droit ou de fait postérieures à cette date ;<br/>
<br/>
              Considérant toutefois que le SYNDICAT DES COMPAGNIES AERIENNES AUTONOMES ne soutient pas que l'article 6 de l'arrêté du 15 novembre 1994, en limitant, à la date de signature de cet arrêté, à vingt cinq sièges la capacité des aéronefs susceptibles, sauf dérogation, d'être utilisés sur l'aéroport du Bourget, aurait été entaché d'erreur manifeste d'appréciation ; qu'il ne ressort pas des pièces du dossier que, depuis la date de signature de l'arrêté, la capacité des aéronefs composant les flottes des compagnies aériennes d'affaires aurait connu une transformation, dans une proportion telle que la réglementation critiquée serait devenue illégale ; que la circonstance que les caractéristiques de certains appareils de plus grande capacité permettraient de limiter les nuisances sonores causées aux riverains de l'aéroport, est, en tout état de cause, sans incidence sur la légalité de l'arrêté litigieux dont l'objet est de répartir le trafic intracommunautaire entre les aéroports du système aéroportuaire parisien ; <br/>
<br/>
              Considérant qu'il résulte de ce qui précède que le SYNDICAT DES COMPAGNIES AERIENNES AUTONOMES n'est pas fondé à demander l'annulation de la décision implicite par laquelle le ministre chargé de l'aviation civile a refusé de modifier l'article 6 de l'arrêté du 15 novembre 1994 ; que ses conclusions à fin d 'injonction ainsi que celles présentées sur le fondement de l'article L. 761-1 du code de justice administrative ne peuvent qu'être rejetées en conséquence ; <br/>
<br/>
              Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées au titre du même article par le ministre de l'écologie, de l'énergie, du développement durable et de la mer ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête du SYNDICAT DES COMPAGNIES AERIENNES AUTONOMES est rejetée.<br/>
<br/>
Article 2 : Les conclusions présentées par le ministre de l'écologie, de l'énergie, du développement durable et de la mer au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
<br/>
Article 3 : La présente décision sera notifiée au SYNDICAT DES COMPAGNIES AERIENNES AUTONOMES et au ministre de l'écologie, du développement durable, des transports et du logement.<br/>
<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-04-03-07-06 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. PRINCIPES GÉNÉRAUX DU DROIT. PRINCIPES INTÉRESSANT L'ACTION ADMINISTRATIVE. OBLIGATION D'ABROGER UN RÈGLEMENT ILLÉGAL. - CHANGEMENT DE CIRCONSTANCES DE FAIT - ABSENCE, EN L'ESPÈCE - ARRÊTÉ LIMITANT LA CAPACITÉ DES AÉRONEFS DES COMPAGNIES D'AVIATION D'AFFAIRES UTILISATRICES DE L'AÉROPORT DU BOURGET.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">65-03-03 TRANSPORTS. TRANSPORTS AÉRIENS. AÉRONEFS. - ARRÊTÉ LIMITANT LA CAPACITÉ DES AÉRONEFS DES AVIONS D'AFFAIRES UTILISATRICES DE L'AÉROPORT DU BOURGET - ILLÉGALITÉ EN RAISON D'UN CHANGEMENT DE CIRCONSTANCES DE FAIT - ABSENCE.
</SCT>
<ANA ID="9A"> 01-04-03-07-06 Depuis la signature de l'arrêté limitant la capacité des aéronefs des compagnies d'aviation d'affaires utilisatrices de l'aéroport du Bourget, celle-ci n'a pas connu de transformation dans une proportion telle que la réglementation critiquée serait devenue illégale.</ANA>
<ANA ID="9B"> 65-03-03 Depuis la signature de l'arrêté limitant la capacité des aéronefs des compagnies d'aviation d'affaires utilisatrices de l'aéroport du Bourget, celle-ci n'a pas connu de transformation dans une proportion telle que la réglementation critiquée serait devenue illégale.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
