<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036550305</ID>
<ANCIEN_ID>JG_L_2018_01_000000407356</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/55/03/CETATEXT000036550305.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 26/01/2018, 407356</TITRE>
<DATE_DEC>2018-01-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>407356</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Jean Sirinelli</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:407356.20180126</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 31 janvier et 21 juillet 2017 au secrétariat du contentieux du Conseil d'Etat, le syndicat UNSA-Outre-Mer Service Militaire Adapté, Administration centrale et Etablissements publics (UNSA-OM) demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite de rejet résultant du silence gardé par le ministre des outre-mer sur sa demande tendant, en premier lieu, à la modification des points 1.3, 1.4, 1.5, 1.10, 2.5 et 5 de l'instruction n° 353586/DEF/SGA/DRH-MD des ministres de la défense et des outre-mer du 25 novembre 2015 relative aux modalités de gestion, d'administration et de formation du personnel civil du service militaire adapté, en deuxième lieu, à la prise en compte, par cette même instruction, des agents régis par les dispositions du I et du II de l'article 34 de la loi n° 2000-321 du 12 avril 2000 et, en dernier lieu, à la consultation du comité technique et du comité d'hygiène, de sécurité et des conditions de travail (CHSCT) ;<br/>
<br/>
              2°) d'enjoindre au ministre des armées et au ministre des outre-mer, d'une part, de faire droit à sa demande et, d'autre part, de suspendre l'application des points 1.3, 1.4, 1.5, 1.10, 1.13, 2.5 et 5 de l'instruction en litige tant que le ministre de la défense et le ministre des outre-mer n'auront pas effectué les modifications demandées.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la défense ;<br/>
              - la loi n° 84-16 du 11 janvier 1984 ;<br/>
              - la loi n° 2000-321 du 12 avril 2000 ;<br/>
              - le décret n° 82-453 du 28 mai 1982 ;<br/>
              - le décret n° 86-442 du 14 mars 1986 ;<br/>
              - le décret n° 2007-51 du 11 janvier 2007 ;<br/>
              - le décret n° 2008-370 du 18 avril 2008 ;<br/>
              - le décret n° 2011-184 du 15 février 2011 ;<br/>
              - le décret n° 2011-774 du 28 juin 2011 ;<br/>
              - le décret n° 2012-422 du 29 mars 2012 ;<br/>
              - le décret n° 2013-728 du 12 août 2013 ;<br/>
              - le décret n° 2014-513 du 20 mai 2014 ;<br/>
              - l'arrêté du 30 septembre 1991 portant mission et organisation du service militaire adapté ;<br/>
              - l'arrêté du 9 septembre 2011 portant création des comités techniques de base de défense ;<br/>
              - l'arrêté du 25 novembre 2015 fixant la liste des actes délégués au ministère des outre mer pour la gestion des fonctionnaires du ministère de la défense exerçant leurs fonctions en position normale d'activité au service militaire adapté ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean Sirinelli, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public.<br/>
<br/>
<br/>
<br/>Sur la fin de non recevoir soulevée par le ministre des armées :<br/>
<br/>
              1. Considérant que la requête du syndicat UNSA-OM n'est pas tardive, dès lors qu'elle est dirigée non contre l'instruction des ministres de la défense et des outre-mer du 25 novembre 2015 relative aux modalités de gestion, d'administration et de formation du personnel civil du service militaire adapté mais contre la décision implicite par laquelle le ministre des outre-mer a rejeté la demande du syndicat du 3 octobre 2016 tendant à la modification de cette instruction ; que la fin de non recevoir opposée par le ministre des armées doit donc être écartée ;<br/>
<br/>
              Sur le cadre juridique du litige :<br/>
<br/>
              2. Considérant qu'aux termes de l'article 33 de la loi du 11 janvier 1984 portant dispositions statutaires relatives à la fonction publique de l'Etat : " L'activité est la position du fonctionnaire qui, titulaire d'un grade, exerce effectivement les fonctions de l'un des emplois correspondant à ce grade dans les administrations de l'Etat, les autorités administratives indépendantes et les établissements publics administratifs de l'Etat " ; qu'en vertu de l'article 1er du décret du 18 avril 2008 organisant les conditions d'exercice des fonctions, en position d'activité, dans les administrations de l'Etat : " Les fonctionnaires de l'Etat ont vocation à exercer les fonctions afférentes à leur grade dans les services d'un ministère et, nonobstant toute disposition statutaire contraire : / (...) 2° Dans les services (...) relevant d'autres départements ministériels " ; que les conditions d'emploi des fonctionnaires qui, en application de ces dispositions et sans être détachés, sont affectés, en position normale d'activité dans les services relevant d'un autre département ministériel que celui qui assure leur gestion, sont en principe régies par les règles de l'administration d'accueil ; qu'il en va ainsi notamment des règles relatives aux congés, à l'aménagement et à la réduction du temps de travail et aux autorisations d'absence ; que si les règles régissant le régime indemnitaire sont, celles qui s'appliquent à l'agent dans son administration d'origine, les conditions de mise en oeuvre de celles-ci peuvent être définies soit par cette dernière, soit par l'administration d'accueil ; <br/>
<br/>
              3. Considérant qu'aux termes de l'article D. 3222-19 du code de la défense : " Le commandement du service militaire adapté est placé pour emploi auprès du ministre chargé de l'outre-mer " ; qu'aux termes de l'article D. 3222-21 du même code : " Les effectifs du service militaire adapté sont inscrits au budget du ministre chargé de l'outre-mer. Les emplois sont pourvus par le ministre de la défense " ; que, selon l'article 10 du décret du 12 août 2013 portant organisation de l'administration centrale du ministère de l'intérieur et du ministère des outre-mer : " Le commandement du service militaire adapté est (...) rattaché à la direction générale des outre-mer " ; qu'aux termes de l'article 4 de l'arrêté du 30 septembre 1991 portant mission et organisation du service militaire adapté, le ministre de la défense " met en place auprès du ministère des départements et territoires d'outre-mer les personnels nécessaires au fonctionnement du service militaire adapté " ; qu'il résulte de ces dispositions que les fonctionnaires dont la gestion est assurée par le ministère des armées et qui sont affectés, en position normale d'activité, au sein des formations du service militaire adapté exercent leurs fonctions dans un service placé sous l'autorité du ministre des outre mer ;<br/>
<br/>
              Sur les conclusions dirigées contre l'instruction en tant qu'elle ne prévoit pas les règles applicables aux agents contractuels du service militaire adapté régis par le I de l'article 34 de la loi du 12 avril 2000 :<br/>
<br/>
              4. Considérant que la circonstance que la circulaire attaquée ne rappelle pas les règles applicables aux agents contractuels du service militaire adapté régis par le I de l'article 34 de la loi du 12 avril 2000 relative aux droits des citoyens dans leurs relations avec les administrations, et bénéficiaires à ce titre d'un contrat à durée indéterminée, ni ne précise ces règles dans les limites des compétences des ministres signataires, est sans incidence sur sa légalité ; que, par suite, les conclusions analysées ci-dessus ne peuvent qu'être rejetées ;<br/>
<br/>
              Sur les conclusions dirigées contre les points 1.4, 1.10, 1.13 et 2.5 de l'instruction en litige relatifs aux instances de concertation, à l'action sociale, au comité médical et à la rémunération des agents du ministère de la défense affectés au service militaire adapté :<br/>
<br/>
              5. Considérant, en premier lieu, qu'aux termes de l'article 3 du décret du 15 février 2011 relatif aux comités techniques dans les administrations et les établissements publics de l'Etat : " Dans chaque département ministériel, un comité technique ministériel est créé auprès du ministre par arrêté du ministre intéressé " ; qu'aux termes de l'article 6 du même décret : " Au niveau déconcentré, en fonction de l'organisation territoriale du département ministériel concerné, est créé, par arrêté du ministre, au moins un comité technique de proximité dénommé comité technique de service déconcentré auprès du chef de service déconcentré concerné. Lorsque le service déconcentré est placé sous l'autorité de plusieurs ministres, le comité technique est créé par arrêté conjoint de ces ministres. (...) Il peut être créé un comité technique commun à tout ou partie des services déconcentrés d'un même niveau territorial, relevant de plusieurs départements ministériels (...) " ; qu'aux termes du II de l'article 18 du même décret : " (...) Les agents affectés, le cas échéant dans les conditions du décret du 18 avril 2008 susvisé, ou mis à disposition dans un service placé sous autorité d'un ministre autre que celui en charge de leur gestion sont électeurs au seul comité technique ministériel du département ministériel assurant leur gestion ainsi qu'au comité technique de proximité du service dans lequel ils exercent leurs fonctions. / (...) " ; qu'aux termes de l'article 1er de l'arrêté du 9 septembre 2011 portant création des comités techniques de base de défense : " Il est institué un comité technique de proximité dénommé comité technique de base de défense auprès de chaque commandant de base de défense en France métropolitaine et dans les départements et collectivités d'outre-mer " ;<br/>
<br/>
              6. Considérant qu'il résulte de ces dispositions que les fonctionnaires dont la gestion est assurée par le ministère des armées, affectés dans les formations du service militaire adapté, sont électeurs au comité technique ministériel placé auprès du ministre des armées ainsi qu'au comité technique de proximité du service dans lequel ils exercent leurs fonctions ; qu'en l'absence d'un tel comité spécifique au service militaire adapté, les agents civils affectés à ce service ont pu être légalement rattachés aux comités techniques des bases de défense créés, sur le fondement de l'article 6 du décret du 15 février 2011, par arrêté du 9 septembre 2011 auprès de chaque commandant de base de défense ; que, par suite, le moyen tiré de ce que les dispositions du point 1.4 de l'instruction du 25 novembre 2015 seraient illégales en tant qu'elles rappellent ces règles relatives aux comités technique ne peut qu'être écarté ;<br/>
<br/>
              7. Considérant, en deuxième lieu, qu'aucune règle ni aucun principe ne conduit à ce que les fonctionnaires civils placés en position d'activité au sein du service militaire adapté et les personnels ouvriers du ministère de la défense mis à disposition du service militaire adapté bénéficient à la fois de l'action sociale du ministère des armées et de celle du ministère des outre-mer ; que, par suite, le syndicat requérant n'est, en tout état de cause, pas fondé à soutenir que les dispositions du point 1.10 de l'instruction en litige, qui prévoient que les agents du service militaire adapté bénéficient de l'action sociale du ministère de la défense, devraient être complétées pour leur accorder en outre le bénéfice de l'action sociale du ministère des outre-mer ; <br/>
<br/>
              8. Considérant, en troisième lieu, qu'aux termes du premier alinéa de l'article 1er du décret du 20 mai 2014 portant création d'un régime indemnitaire tenant compte des fonctions, des sujétions, de l'expertise et de l'engagement professionnel dans la fonction publique de l'Etat : " Les fonctionnaires relevant de la loi du 11 janvier 1984 susvisée peuvent bénéficier, d'une part, d'une indemnité de fonctions, de sujétions et d'expertise et, d'autre part, d'un complément indemnitaire annuel lié à l'engagement professionnel et à la manière de servir, dans les conditions fixées par le présent décret " ; qu'aux termes de l'article 2 du même décret : " Le montant de l'indemnité de fonctions, de sujétions et d'expertise est fixé selon le niveau de responsabilité et d'expertise requis dans l'exercice des fonctions. / Les fonctions occupées par les fonctionnaires d'un même corps ou statut d'emploi sont réparties au sein de différents groupes au regard des critères professionnels suivants (...) / Le nombre de groupes de fonctions est fixé pour chaque corps ou statut d'emploi par arrêté du ministre chargé de la fonction publique et du ministre chargé du budget et, le cas échéant, du ministre intéressé. / Ce même arrêté fixe les montants minimaux par grade et statut d'emplois, les montants maximaux afférents à chaque groupe de fonctions et les montants maximaux applicables aux agents logés par nécessité de service. / (...) " ;<br/>
<br/>
              9. Considérant que le point 2.5 de l'instruction du 25 novembre 2015 prévoit que les fonctionnaires affectés au sein d'une formation du service militaire adapté sont rémunérés en application de la grille indiciaire et des textes indemnitaires de leur corps d'origine et que le montant des primes et indemnités ainsi que leur revalorisation annuelle sont déterminés en application des règles en vigueur dans le ministère de la défense ; que, s'agissant de l'indemnité de fonctions, de sujétions et d'expertise, l'instruction précise que la détermination du niveau de responsabilité et d'expertise des fonctions exercées par les fonctionnaires du ministère de la défense placés en position normale d'activité au sein du service militaire adapté est établie par le service militaire adapté sur la base des groupes de fonctions fixés au sein du ministère de la défense en application des dispositions précitées de l'article 2 du décret du 20 mai 2014 ; qu'elle indique également que les modalités pratiques de calcul et de versement de cette indemnité, dites " règles de gestion ", seront définies, à titre transitoire, par le ministère de la défense ; que le point 2.5 de l'instruction en litige se borne ainsi à rappeler les règles énoncées au point 2 de la présente décision relatives au régime indemnitaire des fonctionnaires affectés, en position normale d'activité, dans un service placé sous l'autorité d'un ministre autre que celui qui assure leur gestion ; que, par suite, le moyen tiré de ce que les auteurs de l'instruction en litige auraient excédé leurs compétences en édictant les dispositions du point 2.5 de l'instruction en litige doit être écarté ;<br/>
<br/>
              10. Considérant, en quatrième lieu, qu'aux termes de l'article 14 du décret du 14 mars 1986 relatif à la désignation des médecins agréés, à l'organisation des comités médicaux et des commissions de réforme, aux conditions d'aptitude physique pour l'admission aux emplois publics et au régime de congés de maladie des fonctionnaires : " Le comité médical et la commission de réforme ministérielle siégeant auprès de l'administration centrale sont compétents à l'égard des fonctionnaires en service à l'administration centrale " ; qu'il résulte de ces dispositions que les personnels qui, sans être détachés, sont placés en position normale d'activité dans un service relevant d'un autre département ministériel que celui qui assure leur gestion, relèvent du comité médical de leur administration d'origine ; que les dispositions du point 1.13 de l'instruction en litige se bornent à rappeler cette règle applicable aux agents placés en position normale d'activité au sein du service militaire adapté ; que, par suite, le syndicat requérant n'est pas fondé à soutenir que le point 1.13 de l'instruction en litige serait illégal en tant qu'il prévoit que le comité médical compétent est celui du ministère de la défense ; <br/>
<br/>
              11. Considérant, en dernier lieu, qu'aux termes du II de l'article 15 de la loi du 11 janvier 1984 portant dispositions statutaires relatives à la fonction publique de l'Etat : " Les comités techniques connaissent des questions relatives à l'organisation et au fonctionnement des services, des questions relatives aux effectifs, aux emplois et aux compétences, des projets de statuts particuliers ainsi que des questions prévues par un décret en Conseil d'Etat (...) " ; qu'aux termes de l'article 34 du décret du 15 février 2011 pris pour l'application de ces dispositions : " Les comités techniques sont consultés (...) sur les questions et projets de textes relatifs : / 1° A l'organisation et au fonctionnement des administrations, établissements ou services ; / 2° A la gestion prévisionnelle des effectifs, des emplois et des compétences ; / 3° Aux règles statutaires et aux règles relatives à l'échelonnement indiciaire ; / 4° Aux évolutions technologiques et de méthodes de travail des administrations, établissements ou services et à leur incidence sur les personnels ; / 5° Aux grandes orientations en matière de politique indemnitaire et de critères de répartition y afférents ; / 6° A la formation et au développement des compétences et qualifications professionnelles ; / 7° A l'insertion professionnelle ; / 8° A l'égalité professionnelle, la parité et à la lutte contre toutes les discriminations ; / 9° A l'hygiène, à la sécurité et aux conditions de travail, lorsqu'aucun comité d'hygiène, de sécurité et de conditions de travail n'est placé auprès d'eux / (...) " ; qu'aux termes de l'article 57 du décret du 28 mai 1982 relatif à l'hygiène et à la sécurité du travail ainsi qu'à la prévention médicale dans la fonction publique, le comité d'hygiène, de sécurité et de conditions de travail " est consulté : / 1° Sur les projets d'aménagement importants modifiant les conditions de santé et de sécurité ou les conditions de travail (...) / 2° Sur les projets importants d'introduction de nouvelles technologies (...) " ; <br/>
<br/>
              12. Considérant qu'il résulte de ce qui a été dit aux points 6 à 10 de la présente décision que les points 1.4, 1.10, 1.13 et 2.5 de l'instruction du 25 novembre 2015 se bornent à rappeler les règles applicables au personnel civil du service militaire adapté en matière de représentation du personnel dans les instances de concertation, d'action sociale, de régime indemnitaire et de comité médical ; que, par suite, le syndicat requérant ne peut utilement se prévaloir de ce que ces dispositions seraient entachées d'un vice de procédure en l'absence de consultation préalable d'un comité technique ou d'un comité d'hygiène, de sécurité et de conditions de travail ; <br/>
<br/>
              Sur les conclusions dirigées contre les points 1.3 et 1.5 de l'instruction en litige relatives au régime du temps de travail, des congés et des autorisations d'absence :<br/>
<br/>
              13. Considérant qu'il résulte de ce qui a été dit au point 2 de la présente décision qu'il appartient au ministre des outre-mer de déterminer les règles relatives aux conditions d'emploi des fonctionnaires affectés en position normale d'activité au sein du service militaire adapté, en ce qui concerne notamment les congés, l'aménagement et la réduction du temps de travail et les autorisations d'absence ; que cependant, en décidant d'exercer sa compétence pour fixer des conditions d'emploi de ces agents distinctes de celles applicables au sein des autres services de son département ministériel, il ne pouvait légalement renvoyer de façon générale et imprécise aux règles en vigueur au sein du ministère de la défense, compte tenu de leur diversité, ainsi que le prévoient les points 1.3 et 1.5 de l'instruction en litige ; que, par ailleurs, en arrêtant lui-même sur ce point des règles, et donc en ne se bornant pas à rappeler le droit applicable, il ne pouvait sans entacher la circulaire d'irrégularité ne pas consulter préalablement le comité technique compétent en application des dispositions précitées de l'article 34 du décret du 15 février 2011 ; <br/>
<br/>
              14. Considérant qu'il résulte de ce qui précède que le syndicat requérant est fondé à demander l'annulation de la décision attaquée du ministre des outre-mer en tant que celui-ci a refusé de modifier les points 1.3 et 1.5 de l'instruction du 25 novembre 2015 ;<br/>
<br/>
              Sur les conclusions dirigées contre le point 5 de l'instruction en litige :<br/>
<br/>
              15. Considérant que le moyen tiré de ce que l'instruction contestée ne pouvait pas déroger à un arrêté, sans que soient précisés l'auteur, la date et l'objet de celui-ci, n'est pas assorti de précisions suffisantes permettant d'en apprécier le bien-fondé ;<br/>
<br/>
              16. Considérant qu'il résulte de tout ce qui précède que le syndicat requérant est fondé à demander l'annulation de la décision attaquée du ministre des outre-mer en tant seulement que celui-ci a refusé de modifier les points 1.3 et 1.5 de l'instruction du 25 novembre 2015 ;<br/>
<br/>
              Sur les conclusions à fin d'injonction :<br/>
<br/>
              17. Considérant que l'annulation de la décision implicite par laquelle le ministre des outre-mer a refusé de réformer les points 1.3 et 1.5 de l'instruction du 25 novembre 2015 implique nécessairement la modification des dispositions réglementaires dont l'illégalité a été constatée ; qu'il y a lieu d'enjoindre au ministre des armées et au ministre des outre-mer de modifier ces dispositions conformément aux motifs de la présente décision dans un délai de trois mois à compter de la notification de la présente décision ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La décision implicite par laquelle le ministre des outre-mer a rejeté la demande du syndicat UNSA-OM tendant à la modification de l'instruction du 25 novembre 2015 relative aux modalités de gestion, d'administration et de formation du personnel civil du service militaire adapté est annulée en tant qu'elle porte sur les points 1.3 et 1.5 de cette instruction.<br/>
Article 2 : Il est enjoint au ministre des armées et au ministre des outre-mer de modifier les articles 1.3 et 1.5 de l'instruction du 25 novembre 2015 conformément aux motifs de la présente décision, dans un délai de trois mois à compter de la notification de la présente décision.<br/>
Article 3 : Le surplus des conclusions de la requête est rejeté.<br/>
Article 4 : La présente décision sera notifiée au syndicat UNSA-Outre-Mer Service Militaire Adapté, Administration centrale et Etablissements publics et à la ministre des armées.<br/>
Copie en sera adressée à la ministre des outre-mer.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-05-01-01 FONCTIONNAIRES ET AGENTS PUBLICS. POSITIONS. AFFECTATION ET MUTATION. AFFECTATION. - FONCTIONNAIRES EN POSITION NORMALE D'ACTIVITÉ AFFECTÉS DANS UN DÉPARTEMENT MINISTÉRIEL AUTRE QUE CELUI QUI ASSURE LEUR GESTION [RJ1] - CONDITIONS D'EMPLOI APPLICABLES - 1) PRINCIPE - RÈGLES DE L'ADMINISTRATION D'ACCUEIL - 2) EXCEPTION - RÉGIME INDEMNITAIRE - RÈGLES DE L'ADMINISTRATION D'ORIGINE - CONDITIONS DE MISE EN OEUVRE DE CES RÈGLES - DÉFINITION PAR L'ADMINISTRATION D'ORIGINE OU L'ADMINISTRATION D'ACCUEIL.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">36-08-01 FONCTIONNAIRES ET AGENTS PUBLICS. RÉMUNÉRATION. QUESTIONS D'ORDRE GÉNÉRAL. - RÉGIME INDEMNITAIRE APPLICABLE AUX FONCTIONNAIRES EN POSITION NORMALE D'ACTIVITÉ AFFECTÉS DANS UN DÉPARTEMENT MINISTÉRIEL AUTRE QUE CELUI QUI ASSURE LEUR GESTION [RJ1] - RÈGLES DE L'ADMINISTRATION D'ORIGINE - CONDITIONS DE MISE EN OEUVRE DE CES RÈGLES - DÉFINITION PAR L'ADMINISTRATION D'ORIGINE OU L'ADMINISTRATION D'ACCUEIL.
</SCT>
<ANA ID="9A"> 36-05-01-01 Les conditions d'emploi des fonctionnaires qui, en application de l'article 1er du décret n° 2008-370 du 18 avril 2010 et sans être détachés, sont affectés, en position normale d'activité dans les services relevant d'un autre département ministériel que celui qui assure leur gestion, sont en principe régies par les règles de l'administration d'accueil. Il en va ainsi notamment des règles relatives aux congés, à l'aménagement et à la réduction du temps de travail et aux autorisations d'absence. Si les règles régissant le régime indemnitaire sont celles qui s'appliquent à l'agent dans son administration d'origine, les conditions de mise en oeuvre de celles-ci peuvent être définies soit par cette dernière, soit par l'administration d'accueil.</ANA>
<ANA ID="9B"> 36-08-01 Si les règles régissant le régime indemnitaire des fonctionnaires qui, en application de l'article 1er du décret n° 2008-370 du 18 avril 2010 et sans être détachés, sont affectés, en position normale d'activité dans les services relevant d'un autre département ministériel que celui qui assure leur gestion sont celles qui s'appliquent à l'agent dans son administration d'origine, les conditions de mise en oeuvre de celles-ci peuvent être définies soit par cette dernière, soit par l'administration d'accueil.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Assemblée, 28 décembre 2009, Syndicat national du travail, de l'emploi et de la formation (SYNTEF-CFDT) et Syndicat unitaire travail emploi formation insertion - Fédération syndicale unitaire, n°s 316479 317271, p. 504.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
