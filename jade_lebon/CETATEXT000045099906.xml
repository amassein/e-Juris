<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000045099906</ID>
<ANCIEN_ID>JG_L_2022_01_000000457987</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/45/09/99/CETATEXT000045099906.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 6ème chambres réunies, 28/01/2022, 457987, Publié au recueil Lebon</TITRE>
<DATE_DEC>2022-01-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>457987</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP RICHARD</AVOCATS>
<RAPPORTEUR>Mme Ségolène Cavaliere</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2022:457987.20220128</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Mme B... F... a demandé au juge des référés du tribunal administratif de Versailles, d'ordonner, sur le fondement de l'article L. 521-1 du code de justice administrative, la suspension de l'exécution de la décision du 9 septembre 2021 par laquelle le directeur du centre hospitalier d'Arpajon l'a suspendue de ses fonctions. Par une ordonnance n° 2108352 du 13 octobre 2021, prise sur le fondement de l'article L. 522-3 du code de justice administrative, le juge des référés a rejeté sa demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 28 octobre et 12 novembre 2021 au secrétariat du contentieux du Conseil d'Etat, Mme F... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge du centre hospitalier d'Arpajon la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
              Par un mémoire distinct, enregistré le 12 novembre 2021, Mme F... demande au Conseil d'Etat, en application de l'article 23-5 de l'ordonnance n°58-1067 du 7 novembre 1958 et à l'appui de son pourvoi, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des dispositions des articles 12 et 14 de la loi n°2021-1040 du 5 août 2021 relative à la gestion de la crise sanitaire.<br/>
<br/>
              Par un mémoire, enregistré le 11 janvier 2022, le ministre des solidarités et de la santé conclut qu'il n'y a pas lieu de renvoyer la question prioritaire de constitutionnalité au Conseil constitutionnel. Il soutient que la question n'est ni nouvelle ni sérieuse.<br/>
<br/>
              Le mémoire a été transmis au Premier ministre, et au centre hospitalier d'Arpajon qui n'ont pas produit de mémoire.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son Préambule et son article 61-1 ;<br/>
              - l'ordonnance n°58-1067 du 7 novembre 1958 ;<br/>
              - la loi n° 2021-1040 du 5 août 2021 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Ségolène Cavaliere, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteure publique.<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Richard, avocat de Mme F....<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 822-1 du code de justice administrative : "Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux ".<br/>
<br/>
              2. Il ressort des pièces du dossier soumis au juge des référés du tribunal administratif de Versailles que, par une décision du 9 septembre 2021, le directeur du centre hospitalier d'Arpajon a suspendu Mme F..., animateur principal de première classe en service dans cet établissement, de ses fonctions à compter du 15 septembre 2021 et jusqu'à ce qu'elle satisfasse à l'obligation de vaccination contre la covid-19 prévue par l'article 12 de la loi du 5 août 2021 relative à la gestion de la crise sanitaire. Mme F... se pourvoit en cassation contre l'ordonnance du 13 octobre 2021 par laquelle le juge des référés a rejeté sa demande, présentée sur le fondement de l'article L. 521-1 du même code, tendant à la suspension de l'exécution de cette décision. <br/>
<br/>
              3. Par un mémoire distinct, présenté en application de l'article 23-5 de l'ordonnance du 7 novembre 1958 et à l'appui de son pourvoi, Mme F... demande au Conseil d'Etat de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des dispositions des articles 12 et 14 de la loi n°2021-1040 du 5 août 2021 relative à la gestion de la crise sanitaire.<br/>
<br/>
              Sur la procédure applicable :<br/>
<br/>
              4. Aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...) ". Il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et qu'elle soit nouvelle ou présente un caractère sérieux. L'article 23-3 de cette ordonnance prévoit qu'une juridiction saisie d'une question prioritaire de constitutionnalité " peut prendre les mesures provisoires ou conservatoires nécessaires " et qu'elle peut statuer " sans attendre la décision relative à la question prioritaire de constitutionnalité si la loi ou le règlement prévoit qu'elle statue dans un délai déterminé ou en urgence ".<br/>
<br/>
              5. Il résulte de la combinaison de ces dispositions avec celles du livre V du code de justice administrative qu'une question prioritaire de constitutionnalité peut être soulevée devant le juge administratif des référés statuant, en première instance, sur le fondement des articles L.521-1, L521-2 ou L. 522-3 de ce code. Le juge des référés peut, lorsqu'une question prioritaire de constitutionnalité est ainsi soulevée devant lui, rejeter la demande qui lui est soumise pour incompétence de la juridiction administrative, irrecevabilité ou défaut d'urgence et décider, ainsi, de ne pas transmettre la question prioritaire de constitutionnalité au Conseil d'Etat.<br/>
<br/>
              6. Il en résulte également que, lorsque le Conseil d'Etat est saisi d'un pourvoi dirigé contre une ordonnance par laquelle le juge des référés du tribunal administratif a rejeté, sur le fondement des articles L.521-1 ou L522-3 du code de justice administrative, la demande qui lui était soumise, pour incompétence de la juridiction administrative, irrecevabilité ou défaut d'urgence, le Conseil d'Etat, juge de cassation peut, si une question prioritaire de constitutionnalité est alors soulevée pour la première fois devant lui, rejeter le pourvoi qui lui est soumis et décider de ne pas transmettre la question prioritaire de constitutionnalité au Conseil constitutionnel, en jugeant, dans le délai de trois mois prévu par l'article 23-5 de l'ordonnance du 7 novembre 1958, que l'ordonnance attaquée a pu, régulièrement et à bon droit, opposer, selon le cas, l'incompétence de la juridiction administrative, l'irrecevabilité de la demande ou le défaut d'urgence. <br/>
<br/>
              Sur le pourvoi et la question prioritaire de constitutionnalité :<br/>
<br/>
              7. En premier lieu, pour demander l'annulation de l'ordonnance attaquée, Mme F... soutient qu'elle est entachée : <br/>
              - d'irrégularité en ce qu'elle se fonde sur un moyen d'ordre public sans que le tribunal en ait préalablement informé les parties ;<br/>
              - d'insuffisance de motivation et d'erreur de droit en ce qu'elle juge que la condition d'urgence prévue par les dispositions de l'article L.521-1 du code de justice administrative n'est pas remplie, alors qu'elle soutenait être privée de toute rémunération.<br/>
<br/>
              8. Aucun de ces moyens n'est sérieux et de nature à permettre l'admission du pourvoi. Par suite, il n'y a pas lieu, sans qu'il soit besoin de se prononcer sur son caractère nouveau ou sérieux, de renvoyer au Conseil constitutionnel la question de la conformité à la Constitution des articles 12 et 14 de la loi du 5 août 2021 relative à la gestion de la crise sanitaire.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
		Article 1er : Le pourvoi de Mme F... n'est pas admis.<br/>
<br/>
Article 2 : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par Mme F.... <br/>
<br/>
		Article 3 : La présente décision sera notifiée à Mme B... F.... <br/>
Copie en sera adressée au centre hospitalier d'Arpajon, au Premier ministre, au ministre des solidarités et de la santé et au Conseil constitutionnel. <br/>
              Délibéré à l'issue de la séance du 17 janvier 2022 où siégeaient : M. Rémy Schwartz, président adjoint de la section du contentieux, présidant ; M. A... H..., M. Fabien Raynaud, présidents de chambre ; M. L... C..., Mme G... K..., M. E... J..., M. Cyril Roger-Lacan, conseillers d'Etat, Mme Flavie Le Tallec, maître des requêtes en service extraordinaire et Mme Ségolène Cavaliere, maître des requêtes en service extraordinaire-rapporteure. <br/>
<br/>
              Rendu le 28 janvier 2022.<br/>
                 Le président : <br/>
                 Signé : M. Rémy Schwartz<br/>
 		La rapporteure : <br/>
      Signé : Mme Ségolène Cavaliere<br/>
                 Le secrétaire :<br/>
                 Signé : M. D... I...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-035-01 PROCÉDURE. - PROCÉDURES INSTITUÉES PAR LA LOI DU 30 JUIN 2000. - QUESTIONS COMMUNES. - DEMANDE EN RÉFÉRÉ REJETÉE EN PREMIER RESSORT POUR INCOMPÉTENCE DE LA JURIDICTION ADMINISTRATIVE, IRRECEVABILITÉ OU DÉFAUT D'URGENCE - 1) PREMIER RESSORT - POSSIBILITÉ DE REFUSER SANS MOTIVATION LA TRANSMISSION D'UNE QPC SOULEVÉE À L'APPUI DE CETTE DEMANDE - EXISTENCE [RJ1] - 2) CASSATION - POSSIBILITÉ DE REFUSER SANS MOTIVATION LE RENVOI D'UNE QPC SOULEVÉE À L'APPUI DU POURVOI [RJ2] - A) EXISTENCE - B) CONDITION - ORDONNANCE DE PREMIER RESSORT JUGÉE RÉGULIÈRE ET FONDÉE DANS LES TROIS MOIS (ART. 23-5 DE L'ORDONNANCE DU 7 NOVEMBRE 1958).
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-08-02-03 PROCÉDURE. - VOIES DE RECOURS. - CASSATION. - POUVOIRS DU JUGE DE CASSATION. - DEMANDE EN RÉFÉRÉ (PROCÉDURES INSTITUÉES PAR LA LOI DU 30 JUIN 2000) REJETÉE EN PREMIER RESSORT POUR INCOMPÉTENCE DE LA JURIDICTION ADMINISTRATIVE, IRRECEVABILITÉ OU DÉFAUT D'URGENCE - POSSIBILITÉ DE REFUSER SANS MOTIVATION LE RENVOI D'UNE QPC SOULEVÉE À L'APPUI DU POURVOI [RJ2] - 1) EXISTENCE - 2) CONDITION - ORDONNANCE DE PREMIER RESSORT JUGÉE RÉGULIÈRE ET FONDÉE DANS LES TROIS MOIS (ART. 23-5 DE L'ORDONNANCE DU 7 NOVEMBRE 1958).
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-10 PROCÉDURE. - DEMANDE EN RÉFÉRÉ (PROCÉDURES INSTITUÉES PAR LA LOI DU 30 JUIN 2000) REJETÉE EN PREMIER RESSORT POUR INCOMPÉTENCE DE LA JURIDICTION ADMINISTRATIVE, IRRECEVABILITÉ OU DÉFAUT D'URGENCE - 1) PREMIER RESSORT - POSSIBILITÉ DE REFUSER SANS MOTIVATION LA TRANSMISSION D'UNE QPC SOULEVÉE À L'APPUI DE CETTE DEMANDE - EXISTENCE [RJ1] - 2) CASSATION - POSSIBILITÉ DE REFUSER SANS MOTIVATION LE RENVOI D'UNE QPC SOULEVÉE À L'APPUI DU POURVOI [RJ2] - A) EXISTENCE - B) CONDITION - ORDONNANCE DE PREMIER RESSORT JUGÉE RÉGULIÈRE ET FONDÉE DANS LES TROIS MOIS (ART. 23-5 DE L'ORDONNANCE DU 7 NOVEMBRE 1958).
</SCT>
<ANA ID="9A"> 54-035-01 1) Il résulte de la combinaison de l'article 23-3 et du premier alinéa de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958 avec celles du livre V du code de justice administrative (CJA) qu'une question prioritaire de constitutionnalité (QPC) peut être soulevée devant le juge administratif des référés statuant, en première instance, sur le fondement des articles L. 521-1, L. 521-2 ou L. 522-3 de ce code. Le juge des référés peut, lorsqu'une QPC est ainsi soulevée devant lui, rejeter la demande qui lui est soumise pour incompétence de la juridiction administrative, irrecevabilité ou défaut d'urgence et décider, ainsi, de ne pas transmettre la QPC au Conseil d'Etat.......2) Il en résulte également que, lorsque le Conseil d'Etat est saisi d'un pourvoi dirigé contre une ordonnance par laquelle le juge des référés du tribunal administratif a rejeté, sur le fondement des articles L. 521-1 ou L. 522-3 du CJA, la demande qui lui était soumise, pour incompétence de la juridiction administrative, irrecevabilité ou défaut d'urgence, a) le Conseil d'Etat, juge de cassation peut, si une QPC est alors soulevée pour la première fois devant lui, rejeter le pourvoi qui lui est soumis et décider de ne pas transmettre la QPC au Conseil constitutionnel, b) en jugeant, dans le délai de trois mois prévu par l'article 23-5 de l'ordonnance du 7 novembre 1958, que l'ordonnance attaquée a pu, régulièrement et à bon droit, opposer, selon le cas, l'incompétence de la juridiction administrative, l'irrecevabilité de la demande ou le défaut d'urgence.</ANA>
<ANA ID="9B"> 54-08-02-03 Il résulte de la combinaison de l'article 23-3 et du premier alinéa de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958 avec celles du livre V du code de justice administrative (CJA) que, lorsque le Conseil d'Etat est saisi d'un pourvoi dirigé contre une ordonnance par laquelle le juge des référés du tribunal administratif a rejeté, sur le fondement des articles L. 521-1 ou L. 522-3 du CJA, la demande qui lui était soumise, pour incompétence de la juridiction administrative, irrecevabilité ou défaut d'urgence, 1) le Conseil d'Etat, juge de cassation peut, si une QPC est alors soulevée pour la première fois devant lui, rejeter le pourvoi qui lui est soumis et décider de ne pas transmettre la QPC au Conseil constitutionnel, 2) en jugeant, dans le délai de trois mois prévu par l'article 23-5 de l'ordonnance du 7 novembre 1958, que l'ordonnance attaquée a pu, régulièrement et à bon droit, opposer, selon le cas, l'incompétence de la juridiction administrative, l'irrecevabilité de la demande ou le défaut d'urgence.</ANA>
<ANA ID="9C"> 54-10 1) Il résulte de la combinaison de l'article 23-3 et du premier alinéa de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958 avec celles du livre V du code de justice administrative (CJA) qu'une question prioritaire de constitutionnalité (QPC) peut être soulevée devant le juge administratif des référés statuant, en première instance, sur le fondement des articles L. 521-1, L. 521-2 ou L. 522-3 de ce code. Le juge des référés peut, lorsqu'une QPC est ainsi soulevée devant lui, rejeter la demande qui lui est soumise pour incompétence de la juridiction administrative, irrecevabilité ou défaut d'urgence et décider, ainsi, de ne pas transmettre la QPC au Conseil d'Etat.......2) Il en résulte également que, lorsque le Conseil d'Etat est saisi d'un pourvoi dirigé contre une ordonnance par laquelle le juge des référés du tribunal administratif a rejeté, sur le fondement des articles L. 521-1 ou L. 522-3 du CJA, la demande qui lui était soumise, pour incompétence de la juridiction administrative, irrecevabilité ou défaut d'urgence, a) le Conseil d'Etat, juge de cassation peut, si une QPC est alors soulevée pour la première fois devant lui, rejeter le pourvoi qui lui est soumis et décider de ne pas transmettre la QPC au Conseil constitutionnel, b) en jugeant, dans le délai de trois mois prévu par l'article 23-5 de l'ordonnance du 7 novembre 1958, que l'ordonnance attaquée a pu, régulièrement et à bon droit, opposer, selon le cas, l'incompétence de la juridiction administrative, l'irrecevabilité de la demande ou le défaut d'urgence.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., s'agissant du référé-liberté, CE, Juge des référés, 16 juin 2010, Mme Diakité, n° 340250, p. 205 ; s'agissant du référé-suspension, CE, Juge des référés, 21 octobre 2010, Conférence nationale des présidents des unions régionales des médecins libéraux, n° 343527, p. 392 ; jugeant le refus de transmission implicite, CE, 29 avril 2013, Mme Agopian, n° 366058, T. pp. 763-812. Comp., s'agissant d'une demande en référé manifestement mal-fondée, CE, 16 janvier 2015, Mme Lehuédé, n° 374070, T. pp. 802-846....[RJ2] Rappr., s'agissant du pourvoi dirigé contre une ordonnance de référé ayant implicitement refusé la transmission d'une QPC, CE, décision du même jour, M. Dupuch, n° 457121, à publier au Recueil.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
