<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028839845</ID>
<ANCIEN_ID>JG_L_2014_04_000000365599</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/83/98/CETATEXT000028839845.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème / 8ème SSR, 09/04/2014, 365599</TITRE>
<DATE_DEC>2014-04-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>365599</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème / 8ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MONOD, COLIN, STOCLET</AVOCATS>
<RAPPORTEUR>M. Christophe Pourreau</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:365599.20140409</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 29 janvier et 29 avril 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. et Mme D...B..., demeurant..., et pour l'Association des centres distributeurs Edouard Leclerc, dont le siège est situé 26, quai Marcel Boyer à Ivry-sur-Seine (94200) ; M. et Mme B... et l'Association des centres distributeurs Edouard Leclerc demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 29 novembre 2012 par laquelle l'Autorité de la concurrence, constatant que le dossier de notification relatif à la prise de contrôle de deux magasins de commerce de détail exploités sous l'enseigne Leclerc était incomplet, a refusé d'accuser réception de la notification de l'opération de concentration ;<br/>
<br/>
              2°) de mettre à la charge de l'Autorité de la concurrence la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 19 mars 2014, présentée pour l'Association des centres distributeurs Edouard Leclerc ;<br/>
<br/>
              Vu le code de commerce ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Christophe Pourreau, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Monod, Colin, Stoclet, avocat de M. D...B...et de l'Association Des Centres Distributeurs Leclerc ;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article L. 430-3 du code de commerce : " L'opération de concentration doit être notifiée à l'Autorité de la concurrence avant sa réalisation. (...) / L'obligation de notification incombe aux personnes physiques ou morales qui acquièrent le contrôle de tout ou partie d'une entreprise (...). Le contenu du dossier de notification est fixé par décret. (...) " ; qu'aux termes de l'article R. 430-2 du même code : " Le dossier de notification mentionné à l'article L. 430-3 comprend les éléments énumérés aux annexes 4-3 à 4-5 du présent livre. (...) / Lorsque l'Autorité de la concurrence constate que le dossier est incomplet (...), elle demande que le dossier soit complété ou rectifié. / La notification complète fait l'objet d'un accusé de réception " ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier que M. A... C..., qui, par l'intermédiaire des sociétés Rémy C...Développement et RN.Patri.One, contrôle les sociétés Nobladis et Sodirev, lesquelles exploitent chacune un hypermarché sous l'enseigne Leclerc situé en Haute-Garonne, et M. et Mme D...B..., qui contrôlent les sociétés HBC 31 et HSO 31, ont notifié le 7 novembre 2012 à l'Autorité de la concurrence une convention de promesse de vente par le groupe C...et une option d'achat par M. et Mme B..., via HBC 31 et HSO 31, des parts des sociétés Nobladis et Sodirev ; que, le 29 novembre 2012, l'Autorité de la concurrence, après avoir relevé que le dossier de notification était incomplet, a refusé d'accuser réception de la notification de l'opération de concentration et demandé aux parties notifiantes de " faire notifier conjointement l'opération par l'ACDLec [Association des centres distributeurs E. Leclerc] " ; que M. et Mme B... et l'Association des centres distributeurs Edouard Leclerc (ACDLec) demandent l'annulation pour excès de pouvoir de cette " décision " ;<br/>
<br/>
              3. Considérant, toutefois, qu'il ressort des pièces du dossier que, après que M. et Mme B... et l'Association des centres distributeurs Edouard Leclerc ont notifié conjointement l'opération et communiqué des informations complémentaires relatives aux relations commerciales qu'elles entretiennent ensemble, l'Autorité de la concurrence a, le 20 décembre 2012, accusé réception de la notification de l'opération de concentration  ; que, dès lors, à la date à laquelle elle a été enregistrée, soit le 29 janvier 2013, la requête était dépourvue d'objet et était ainsi, en tout état de cause, irrecevable ; qu'elle ne peut donc qu'être rejetée ;<br/>
<br/>
              4. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Autorité de la concurrence, qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. et Mme B... et de l'Association des centres distributeurs Edouard Leclerc est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. et Mme D...B..., à l'Association des centres distributeurs Edouard Leclerc et à l'Autorité de la concurrence.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">14-05-005 COMMERCE, INDUSTRIE, INTERVENTION ÉCONOMIQUE DE LA PUISSANCE PUBLIQUE. DÉFENSE DE LA CONCURRENCE. AUTORITÉ DE LA CONCURRENCE. - NOTIFICATION DE L'OPÉRATION DE CONCENTRATION (ART. L. 430-3 DU CODE DE COMMERCE) - NOTIFICATION INCOMPLÈTE - DEMANDE D'ANNULATION DE LA DÉCISION DE L'AUTORITÉ DE REFUSER D'ACCUSER RÉCEPTION DE LA NOTIFICATION - ACCUSÉ DE RÉCEPTION DE LA NOTIFICATION PAR L'AUTORITÉ DE LA CONCURRENCE APRÈS RÉCEPTION DES INFORMATIONS COMPLÉMENTAIRES DEMANDÉES - CONSÉQUENCE - CONTESTATION PRIVÉE D'OBJET - EXISTENCE.
</SCT>
<ANA ID="9A"> 14-05-005 La demande d'annulation de la décision de l'Autorité de la concurrence de refuser d'accuser réception de la notification d'une opération de concentration et de demander des informations complémentaires est privée d'objet lorsque, la notification ayant été complétée, l'Autorité de la concurrence en accuse ultérieurement réception.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
