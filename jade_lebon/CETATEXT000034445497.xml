<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034445497</ID>
<ANCIEN_ID>JG_L_2017_04_000000395328</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/44/54/CETATEXT000034445497.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 19/04/2017, 395328</TITRE>
<DATE_DEC>2017-04-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>395328</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, SOLTNER, TEXIDOR, PERIER ; HAAS</AVOCATS>
<RAPPORTEUR>M. Marc Pichon de Vendeuil</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:395328.20170419</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La communauté urbaine de Dunkerque a demandé au tribunal administratif de Lille de condamner solidairement la société Couverture Bardage Etanchéité du Littoral, M. A... B..., architecte, la société Séchaud et Bossuyt Nord, et la société Socotec à lui verser la somme de 182 252,22 euros ainsi que les intérêts au taux légal à compter du 19 septembre 2007, en réparation des préjudices résultant des désordres apparus lors de l'opération de réhabilitation d'un bâtiment dénommé " La Cathédrale " situé dans la zone d'aménagement concertée du Grand Large à Dunkerque. Par un jugement n° 1006040 du 11 mars 2014, le tribunal administratif de Lille a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 14DA00812 du 15 octobre 2015, la cour administrative d'appel de Douai a rejeté son appel.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés au secrétariat du contentieux du Conseil d'Etat les 15 décembre 2015 et 15 mars 2016, la communauté urbaine de Dunkerque demande au Conseil d'État :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de la société Couverture Bardage Etanchéité du Littoral, de la société Grontmij, venue aux droits de la société Séchaud et Bossuyt Nord, de la société Socotec et de M. B...la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code civil ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Marc Pichon de Vendeuil, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Haas, avocat de la communauté urbaine de Dunkerque, et à la SCP Célice, Soltner, Texidor, Perier, avocat de la société Grontmij.<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des énonciations de l'arrêt attaqué que la communauté urbaine de Dunkerque loue à la société Debussche un bâtiment dénommé " La Cathédrale " aux termes d'un bail commercial conclu le 30 juin 2000, après avoir fait procéder à sa réhabilitation par des travaux dont la réception est intervenue au mois d'octobre 2000 ; que des désordres étant apparus sur le bâtiment dès 2001, la société Debussche a saisi le juge judiciaire de différentes demandes d'expertise à compter de l'année 2002 puis assigné la communauté urbaine devant le tribunal de grande instance de Dunkerque le 19 septembre 2007 aux fins d'obtenir le paiement de dommages-intérêts ; qu'à son tour, la communauté urbaine a assigné devant ce tribunal, le 31 décembre 2007, quatre des constructeurs étant intervenus lors de l'opération de réhabilitation, la société Couverture Bardage Etanchéité du Littoral (CBE Littoral), titulaire du lot couverture-bardage, M.B..., architecte, la société Séchaud et Bossuyt - aux droits de laquelle sont venues successivement la société Ginger Séchaud et Bossuyt, puis la société Grontmij - , maître d'oeuvre, et la société Socotec, contrôleur technique ; que toutefois, aux termes d'un arrêt de la cour d'appel de Douai du 8 juin 2010, le juge judiciaire s'est déclaré incompétent en estimant que ces demandes étaient relatives à l'exécution d'un marché de travaux publics et relevaient donc de la compétence de la juridiction administrative ; <br/>
<br/>
              2. Considérant que, par une requête du 5 octobre 2010, la communauté urbaine a saisi le tribunal administratif de Lille d'une demande tendant à la condamnation solidaire des sociétés CBE Littoral, Ginger Séchaud et Bossuyt et Socotec et de M. B...à réparer ses divers préjudices ; que par un jugement du 11 mars 2014, le tribunal administratif de Lille a, d'une part, rejeté comme irrecevables les demandes dirigées contre la société Socotec et la société Couverture Bardage Etanchéité du Littoral, faute pour la communauté urbaine de Dunkerque de justifier de sa qualité pour agir et, d'autre part, rejeté comme non fondées les conclusions dirigées contre la société Séchaud et Bossuyt du fait de l'expiration du délai d'action de la garantie décennale ; que, saisie par la communauté urbaine de Dunkerque, la cour administrative d'appel de Douai a, par un arrêt du 15 octobre 2015, annulé ce jugement en ce qu'il avait fait droit à la fin de non-recevoir de la société CBE Littoral et de la Socotec tirée du défaut de qualité pour agir ; que la cour a, en revanche, porté la même appréciation que le tribunal sur l'expiration du délai d'action de dix ans au titre de la garantie décennale ; qu'elle a ainsi rejeté l'appel de la communauté urbaine de Dunkerque et, après évocation, ses demandes dirigées en première instance contre la société CBE Littoral et la Socotec ; que la communauté urbaine de Dunkerque se pourvoit en cassation contre ce dernier arrêt ;<br/>
<br/>
              3. Considérant qu'aux termes de l'article 2244 du code civil, dans sa rédaction alors applicable : " Une citation en justice, même en référé, un commandement ou une saisie, signifiés à celui qu'on veut empêcher de prescrire, interrompent la prescription ainsi que les délais pour agir " ; qu'il résulte de ces dispositions, applicables à la responsabilité décennale des architectes et des entrepreneurs à l'égard des maîtres d'ouvrage publics, que, pour les désordres qui y sont expressément visés, une action en justice n'interrompt la prescription qu'à la condition d'émaner de celui qui a qualité pour exercer le droit menacé par la prescription et de viser celui-là même qui en bénéficierait ;<br/>
<br/>
              4. Considérant qu'en estimant que l'assignation au fond formée le 31 décembre 2007 par la communauté urbaine de Dunkerque contre les maîtres d'oeuvre, le contrôleur technique et la société chargée du lot couverture-bardage devant le tribunal de grande instance de Dunkerque n'a pas eu pour effet d'interrompre le délai de l'action en garantie décennale ouverte au maître de l'ouvrage contre ces constructeurs dès lors que cette assignation ne précisait pas qu'elle reposait sur ce fondement juridique et que les termes dans lesquels la demande était formulée ne permettaient pas de la regarder comme implicitement mais nécessairement fondée sur la garantie décennale, la cour administrative d'appel de Douai, à qui il n'appartenait que de rechercher si cette assignation identifiait de manière suffisamment précise les désordres dont elle demandait réparation, émanait de la personne qui avait qualité pour exercer le droit menacé par la prescription et visait ceux-là mêmes qui en bénéficieraient, a entaché son arrêt d'une erreur de droit ; qu'il résulte de ce qui précède, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que son arrêt doit être annulé ;<br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société Couverture Bardage Etanchéité du Littoral, de la société Grontmij, de la société Socotec et de M. B...la somme de 750 euros à verser chacun à la communauté urbaine de Dunkerque, au titre des dispositions de l'article L. 761-1 du code de justice administrative ; qu'en revanche, les mêmes dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de la communauté urbaine de Dunkerque qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Douai du 15 octobre 2015 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Douai.<br/>
Article 3 : La société Couverture Bardage Etanchéité du Littoral, la société Grontmij, la société Socotec et M. B...verseront chacun à la communauté urbaine de Dunkerque une somme de 750 euros au titre de l'article L. 761-1 du code de justice administrative. Les conclusions de la société Grontmij présentées au titre des mêmes dispositions sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à la communauté urbaine de Dunkerque et à la société Grontmij.<br/>
Copie en sera adressée à la société Couverture Bardage Etanchéité du Littoral, à la société Socotec et à M. A...B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-06-01-04-02-02 MARCHÉS ET CONTRATS ADMINISTRATIFS. RAPPORTS ENTRE L'ARCHITECTE, L'ENTREPRENEUR ET LE MAÎTRE DE L'OUVRAGE. RESPONSABILITÉ DES CONSTRUCTEURS À L'ÉGARD DU MAÎTRE DE L'OUVRAGE. RESPONSABILITÉ DÉCENNALE. DÉLAI DE MISE EN JEU. INTERRUPTION DU DÉLAI. - EXISTENCE - CITATION EN JUSTICE - CONDITIONS [RJ1].
</SCT>
<ANA ID="9A"> 39-06-01-04-02-02 Il résulte des dispositions alors en vigueur de l'article 2244 du code civil, applicables à la responsabilité décennale des architectes et des entrepreneurs à l'égard des maîtres d'ouvrage publics, que, pour les désordres qui y sont expressément visés, une action en justice n'interrompt la prescription qu'à la condition d'émaner de celui qui a qualité pour exercer le droit menacé par la prescription et de viser celui là même qui en bénéficierait.... ,,Par suite, erreur de droit d'une cour administrative d'appel à exclure l'effet interruptif d'une assignation devant le TGI au motif qu'elle ne précisait pas qu'elle reposait sur le fondement de la garantie décennale et qu'elle ne pouvait être regardée comme nécessairement fondée sur la garantie décennale. Il appartenait uniquement à la cour de rechercher si l'assignation identifiait de manière suffisamment précise les désordres dont elle demandait réparation, émanait de la personne qui avait qualité pour exercer le droit menacé par la prescription et visait ceux-là même qui en bénéficieraient.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Cf. CE,  octobre 2009, Société atelier des maîtres d'oeuvre Atmo et compagnie les souscripteurs du Lloyd's de Londres, n° 308163, T. p. 837 ; CE, 12 mars 2014, Société Ace Insurance, n° 364429, T. p. 744.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
