<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041982581</ID>
<ANCIEN_ID>JG_L_2020_06_000000438406</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/98/25/CETATEXT000041982581.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 09/06/2020, 438406</TITRE>
<DATE_DEC>2020-06-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>438406</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR>M. Jean-Yves Ollier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Mireille Le Corre</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:438406.20200609</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 10 février 2020 au secrétariat du contentieux du Conseil d'Etat, le Conseil national de l'ordre des médecins demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le décret n° 2019-1529 du 30 décembre 2019 relatif aux marchés passés par les conseils nationaux des ordres des professions de santé en tant, premièrement, qu'il fixe l'entrée en vigueur de ses dispositions au 1er janvier 2020 sans prévoir de mesures transitoires permettant aux conseils nationaux des ordres des professions médicales, pharmaceutiques et paramédicales de s'adapter à la nouvelle réglementation, deuxièmement, qu'il ne prévoit ni n'organise les conditions du recours de ces conseils aux centrales d'achats déjà constituées et, troisièmement, qu'il impose en procédure formalisée la publication des avis de marché à la fois dans un journal habilité à recevoir des annonces légales et dans un journal spécialisé correspondant au secteur économique concerné ;<br/>
<br/>
              2°) d'enjoindre aux auteurs du décret, premièrement, de modifier l'article 4 du décret relatif aux dispositions finales, afin de repousser son entrée en vigueur à une date ne pouvant correspondre à un délai inférieur à six mois à compter du 1er janvier 2020, ou de prendre toute autre mesure provisoire qui serait le mieux à même de préserver la sécurité juridique des contrats du Conseil national de l'ordre des médecins, deuxièmement, de prévoir la possibilité pour les conseils nationaux des ordres concernés d'adhérer à une centrale d'achats dans les conditions prévues par les articles L. 2113-2 à L. 2113-4 du code de la commande publique et, troisièmement, de prévoir que pour les marchés passés selon l'une des procédures formalisées énumérées à l'article R. 4122-4-15, le conseil national publie un avis de marché dans un journal habilité à recevoir des annonces légales ou, le cas échéant, dans le bulletin officiel des annonces de marchés publics et au Journal officiel de l'Union européenne ; <br/>
<br/>
              3°) d'assortir cette injonction d'un délai maximum de quinze jours à compter de l'ordonnance à intervenir et d'une astreinte de 500 euros par jour de retard ; <br/>
<br/>
              4°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son Préambule et ses articles 1er et 61-1 ; <br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - la loi organique n° 2020-365 du 30 mars 2020 ; <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code de la commande publique ;<br/>
              - le code de la santé publique ;<br/>
              - la loi n° 2017-1841 du 30 décembre 2017 ;<br/>
              - l'ordonnance n° 2017-644 du 27 avril 2017 ;<br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 modifiée ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Yves Ollier, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Mireille Le Corre, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Matuchansky, Poupot, Valdelièvre, avocat du conseil national de l'ordre des médecins ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. L'article L. 4122-2-1 du code de la santé publique, applicable aux conseils nationaux des professions médicales dispose que : " Les marchés conclus à titre onéreux par le conseil national avec un ou plusieurs opérateurs économiques pour répondre à ses besoins en matière de fournitures ou de services respectent les principes de liberté d'accès à la commande, d'égalité de traitement des candidats et de transparence des procédures définis à l'article 1er de l'ordonnance n° 2015-899 du 23 juillet 2015 relative aux marchés publics. / Les conseils nationaux des ordres peuvent constituer entre eux une centrale d'achats ou un groupement de commandes d'achats. / Dans les conditions et sous réserve des adaptations prévues par décret en Conseil d'Etat, le marché est passé, en fonction de son objet ou de sa valeur estimée, selon les procédures prévues à l'article 42 de l'ordonnance n° 2015-899 du 23 juillet 2015 précitée ". Ces dispositions sont également applicables aux conseils nationaux des ordres des infirmiers, des masseurs kinésithérapeutes et des pédicures podologues en vertu des dispositions des articles L. 4312-7, L. 4321-19 et L. 4322-12 du même code. Des dispositions identiques concernant le conseil national de l'ordre des pharmaciens figurent à l'article L. 4231-8 du même code.<br/>
<br/>
              2. Les dispositions des articles R. 4122-4-4 à R. 4122-4-30 du code de la santé publique, créées par le décret attaqué, fixent, pour assurer l'application des dispositions de l'article L. 4122-2-1 du code de la santé publique, les règles applicables aux marchés passés par les conseils nationaux des ordres des professions de santé. En particulier, l'article R. 4122-4-12 dispose que : " Pour organiser son achat, le conseil national de l'ordre peut procéder à une mutualisation de ses besoins avec d'autres conseils nationaux : / 1° Soit sous la forme de centrales d'achat, dans les conditions prévues aux articles L. 2113-2 à L. 2113-4 du code de la commande publique ; / 2° Soit sous la forme de groupements de commande, dans les conditions prévues au premier alinéa de l'article L. 2113-6 et à l'article L. 2113-7 du même code. / Le conseil national ne peut en revanche créer de centrales d'achat ou groupements de commande avec d'autres personnes publiques ou privées que celles mentionnées aux alinéas précédents ".<br/>
<br/>
              Sur la question prioritaire de constitutionnalité :<br/>
<br/>
              3. A l'appui de son recours pour excès de pouvoir, le Conseil national de l'ordre des médecins demande au Conseil d'Etat, en application de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution du deuxième alinéa de l'article L. 4122-2-1 du code de la santé publique.<br/>
<br/>
              4. Aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 précitée : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...) ". Il résulte des dispositions du même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              5. Le Conseil national de l'ordre national des médecins soutient que, si elles doivent être entendues comme interdisant à un conseil national de recourir à des centrales d'achat autres que celles constituées, le cas échéant, avec d'autres conseils nationaux, les dispositions du deuxième alinéa de l'article L. 4122-2-1 du code de la santé publique méconnaissent les principes d'égalité et de liberté contractuelle.<br/>
<br/>
              6. Si elles fixent les conditions dans lesquelles les conseils nationaux des ordres des professions de santé peuvent créer des centrales d'achat, les dispositions du deuxième alinéa de l'article L. 4122-2-1 du code de la santé publique sont sans incidence sur la possibilité pour eux de recourir à une centrale d'achat existante pour la réalisation de travaux ou l'acquisition de fournitures ou de services, dans les conditions prévues par les articles L. 2113-2 et suivants de la commande publique, lequel n'exclut par ailleurs, par aucune de ses dispositions, notamment pas par celles de son article L 2113-2, une telle possibilité. Ainsi, les moyens tirés de ce qu'elles méconnaîtraient le principe d'égalité et le principe de liberté contractuelle ne peuvent qu'être écartés. <br/>
<br/>
              7. Il résulte ce qui précède que la question prioritaire de constitutionnalité, qui n'est pas nouvelle, ne peut être regardée comme présentant un caractère sérieux. Par suite, il n'y a pas lieu de la renvoyer au Conseil constitutionnel.<br/>
<br/>
              Sur les autres moyens de la requête :<br/>
<br/>
              En ce qui concerne les dispositions du décret relatives aux centrales d'achat :<br/>
<br/>
              8. Pour les motifs exposés au point 6, les dispositions de l'article R 4122-4-12 du code de la santé publique, citées au point 2, n'ont, pas davantage que celles du deuxième alinéa de l'article L. 4122-2-1 du même code pour l'application desquelles elles sont prises, pour effet d'empêcher le conseil national de l'ordre d'une profession de santé de recourir à une centrale d'achat existante pour la réalisation de travaux ou l'acquisition de fournitures ou de services, pour respecter les obligations de publicité et de mise en concurrence auxquelles il est soumis. Par suite, les moyens tirés de ce que, en édictant une telle interdiction, le pouvoir réglementaire se serait fondé sur des dispositions législatives incompatibles avec l'article 14 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et l'article 1er de son premier protocole additionnel, aurait lui-même méconnu ces stipulations ainsi que le principe d'égalité et aurait entaché le décret d'erreur manifeste d'appréciation ne peuvent qu'être écartés.<br/>
<br/>
              En ce qui concerne les dispositions du décret imposant, en procédure formalisée, la publication des avis de marché à la fois dans un journal habilité à recevoir des annonces légales et dans un journal spécialisé correspondant au secteur économique concerné :<br/>
<br/>
              9. En vertu de l'article R. 4122-4-15 du code de la santé publique, dans sa rédaction issue du décret contesté, les conseils nationaux des ordres des professions de santé doivent passer leurs marchés selon une procédure formalisée - appel d'offres, procédure avec négociation ou dialogue compétitif - lorsque la valeur estimée du besoin est supérieure aux seuils européens définis, pour les marchés relevant du code de la commande publique, au b du I de l'annexe n° 2 de ce code. Aux termes du I de l'article R. 4122-4-19 du code de la santé publique : " Pour les marchés passés selon l'une des procédures formalisées énumérées à l'article R. 4122-4-15, le conseil national publie un avis de marché dans un journal habilité à recevoir des annonces légales et dans un journal spécialisé correspondant au secteur économique concerné ".<br/>
<br/>
              10. Le Conseil national de l'ordre des médecins ne saurait utilement soutenir que le décret attaqué méconnaîtrait sur ce point le principe d'égalité au motif que les marchés passés selon une procédure formalisée par les conseils des ordres des professions de santé, qui relèvent des dispositions du code de la santé publique, sont soumis à des règles de publicité différentes de celles qui s'appliquent aux marchés passés selon une telle procédure par des acheteurs qui  relèvent quant à eux du code de la commande publique.<br/>
<br/>
              En ce qui concerne l'absence de dispositions transitoires :<br/>
<br/>
              11. Le troisième alinéa du I de l'article 14 de l'ordonnance du 27 avril 2017 relative à l'adaptation des dispositions législatives relatives au fonctionnement des ordres des professions de santé avait fixé l'entrée en vigueur des dispositions de l'article L. 4122-2-1 du code de la santé publique au 1er janvier 2019. La loi du 30 décembre 2017 ratifiant cette ordonnance a reporté leur entrée en vigueur au 1er janvier 2020. L'article 4 du décret attaqué précise que " les dispositions du présent décret entrent en vigueur au 1er janvier 2020. / Elles s'appliquent aux marchés pour lesquels une consultation a été engagée ou un avis d'appel à la concurrence a été envoyé à la publication, à compter de cette date ". Le Conseil national de l'ordre des médecins soutient que ces dispositions méconnaissent le principe de sécurité juridique.<br/>
<br/>
              12. Lorsque de nouvelles normes générales sont édictées par voie réglementaire, elles ont vocation à s'appliquer immédiatement, sous réserve des exigences attachées au principe de non-rétroactivité des actes administratifs, qui exclut que les nouvelles dispositions s'appliquent à des situations juridiquement constituées avant l'entrée en vigueur de ces dispositions. Le décret du 30 décembre 2019, qui ne saurait légalement s'appliquer à des situations juridiquement constituées avant son entrée en vigueur, ne soumet pas aux nouvelles règles qu'il édicte les contrats conclus par le Conseil national de l'ordre des médecins comportant une clause de tacite reconduction sauf dénonciation moyennant un préavis lorsque le délai de ce préavis était déjà échu au 1er janvier 2020. Le Conseil national fait également valoir que, s'agissant des contrats arrivant à expiration peu après le 1er janvier 2020, il ne dispose pas du temps nécessaire, compte tenu des obligations de publicité et de mise en concurrence qui pèsent désormais sur lui, pour conclure de nouveaux contrats sans solution de continuité. Toutefois, les conseils nationaux des ordres de professions de santé étaient informés de longue date que leurs marchés seraient soumis à des règles de publicité et de mise en concurrence à compter du 1er janvier 2020 et rien ne faisait obstacle à ce qu'ils engagent avant cette date la procédure de passation des marchés arrivant à échéance au début de l'année 2020, de telle sorte que les nouvelles règles ne leur soient pas applicables. Par suite, le moyen tiré de ce que le décret attaqué porterait atteinte au principe de sécurité juridique en tant qu'il fixe sa date d'entrée en vigueur au 1er  janvier 2020, sans prévoir de mesures transitoires, doit être écarté. <br/>
<br/>
              13. Il résulte de tout ce qui précède que les conclusions du Conseil national de l'ordre des médecins dirigées contre le décret attaqué, ainsi, par suite, que ses conclusions à fin d'injonction doivent être rejetées.<br/>
<br/>
              14. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise, à ce titre, à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par le conseil national de l'ordre des médecins. <br/>
Article 2 : La requête du Conseil national de l'ordre des médecins est rejetée. <br/>
Article 3 : La présente décision sera notifiée au Conseil national de l'ordre des médecins et au ministre des solidarités et de la santé.<br/>
Copie en sera adressée au Premier ministre, au ministre de l'économie et des finances et au Conseil constitutionnel.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-02-02 MARCHÉS ET CONTRATS ADMINISTRATIFS. FORMATION DES CONTRATS ET MARCHÉS. MODE DE PASSATION DES CONTRATS. - CENTRALES D'ACHAT - POSSIBILITÉ, POUR LES CONSEILS NATIONAUX DES ORDRES DE PROFESSIONS DE SANTÉ, DE RECOURIR À UNE CENTRALE D'ACHAT EXISTANTE - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">55-01-01 PROFESSIONS, CHARGES ET OFFICES. ORDRES PROFESSIONNELS - ORGANISATION ET ATTRIBUTIONS NON DISCIPLINAIRES. QUESTIONS COMMUNES. - CONSEILS NATIONAUX DES ORDRES DE PROFESSIONS DE SANTÉ - POSSIBILITÉ DE RECOURIR À UNE CENTRALE D'ACHAT EXISTANTE - EXISTENCE.
</SCT>
<ANA ID="9A"> 39-02-02 S'il fixe les conditions dans lesquelles les conseils nationaux des ordres des professions de santé peuvent créer des centrales d'achat, le deuxième alinéa de l'article L. 4122-2-1 du code de la santé publique (CSP) est sans incidence sur la possibilité pour eux de recourir à une centrale d'achat existante pour la réalisation de travaux ou l'acquisition de fournitures ou de services, dans les conditions prévues par les articles L. 2113-2 et suivants du code de la commande publique, lequel n'exclut par ailleurs, par aucune de ses dispositions, notamment pas par celles de son article L. 2113-2, une telle possibilité.</ANA>
<ANA ID="9B"> 55-01-01 S'il fixe les conditions dans lesquelles les conseils nationaux des ordres des professions de santé peuvent créer des centrales d'achat, le deuxième alinéa de l'article L. 4122-2-1 du code de la santé publique (CSP) est sans incidence sur la possibilité pour eux de recourir à une centrale d'achat existante pour la réalisation de travaux ou l'acquisition de fournitures ou de services, dans les conditions prévues par les articles L. 2113-2 et suivants du code de la commande publique, lequel n'exclut par ailleurs, par aucune de ses dispositions, notamment pas par celles de son article L. 2113-2, une telle possibilité.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
