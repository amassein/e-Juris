<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042659684</ID>
<ANCIEN_ID>JG_L_2020_12_000000444762</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/65/96/CETATEXT000042659684.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 11/12/2020, 444762</TITRE>
<DATE_DEC>2020-12-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>444762</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Philippe Ranquet</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Cytermann</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:444762.20201211</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              La commune de Carnin, à l'appui de sa demande tendant à l'annulation de l'arrêté du 10 mars 2020 par lequel le préfet du Nord lui a transféré M. A... B..., adjoint administratif, a demandé au tribunal administratif de Lille, par un mémoire, enregistré le 1er septembre 2020 au greffe de ce tribunal, de transmettre au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution des dispositions du 2e alinéa du 2° du IV bis de l'article L. 5211-4-1 du code général des collectivités territoriales. <br/>
<br/>
              Par une ordonnance n° 2003411 du 21 septembre 2020, enregistrée le même jour au secrétariat du contentieux du Conseil d'État, le président de la 2e chambre du tribunal administratif de Lille a décidé, par application de l'article 23-2 de l'ordonnance n° 58-1067 du 7 novembre 1958, de transmettre cette question au Conseil d'Etat.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment ses articles 61-1 et 72 ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code général des collectivités territoriales, notamment son article L. 5211-4-1 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Philippe Ranquet, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Laurent Cytermann, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il résulte des dispositions de l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que, lorsqu'une juridiction relevant du Conseil d'Etat a transmis à ce dernier, en application de l'article 23-2 de cette même ordonnance, la question de la conformité à la Constitution d'une disposition législative, le Conseil constitutionnel est saisi de cette question de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              2. Aux termes du IV bis de l'article L. 5211-4-1 du code général des collectivités territoriales : " Lorsqu'un établissement public de coopération intercommunale restitue une compétence aux communes membres :/ (...) 2° La répartition des fonctionnaires et agents territoriaux non titulaires transférés par les communes en application du deuxième alinéa du I ou recrutés par l'établissement public de coopération intercommunale et qui sont chargés, pour la totalité de leurs fonctions, de la mise en oeuvre de la compétence restituée est décidée d'un commun accord par convention conclue entre l'établissement public et ses communes membres. Cette convention est soumise pour avis aux comités sociaux territoriaux placés auprès de l'établissement public de coopération intercommunale et auprès des communes. Elle est notifiée aux agents non titulaires et aux fonctionnaires concernés. / A défaut d'accord sur les conditions de répartition des personnels dans un délai de trois mois à compter de la restitution des compétences, le représentant de l'Etat dans le département fixe cette répartition par arrêté. / (...) ".<br/>
<br/>
              3. La commune de Carnin soutient que les dispositions du deuxième alinéa du 2° du IV bis de cet article, prévoyant que le représentant de l'Etat décide de la répartition des agents à défaut d'accord entre les collectivités intéressées, méconnaissent le principe de libre administration des collectivités territoriales garanti par le troisième alinéa de l'article 72 de la Constitution, dès lors qu'elles confient la décision à une autorité de l'Etat plutôt qu'à un organisme composé ou sous le contrôle d'élus locaux et qu'elles ne soumettent l'exercice de ce pouvoir à aucun critère ni à aucune consultation préalable.<br/>
<br/>
              4. Si le législateur peut, sur le fondement des dispositions des articles 34 et 72 de la Constitution, assujettir les collectivités territoriales ou leurs groupements à des obligations et à des charges, c'est à la condition que celles-ci répondent à des exigences constitutionnelles ou concourent à des fins d'intérêt général, qu'elles ne méconnaissent pas la compétence propre des collectivités concernées, qu'elles n'entravent pas leur libre administration et qu'elles soient définies de façon suffisamment précise quant à leur objet et à leur portée. En outre, aux termes du dernier alinéa de l'article 72 de la Constitution : " Dans les collectivités territoriales de la République, le représentant de l'État, représentant de chacun des membres du Gouvernement, a la charge des intérêts nationaux, du contrôle administratif et du respect des lois ", de sorte qu'il appartient au législateur de prévoir l'intervention du représentant de l'Etat pour remédier, sous le contrôle du juge, aux difficultés résultant de l'absence de décision de la part des autorités décentralisées compétentes en se substituant à ces dernières lorsque cette absence de décision risque de compromettre le fonctionnement des services publics et l'application des lois.<br/>
<br/>
              5. En prévoyant que les agents mentionnés au 2° du IV bis de l'article L. 5211-4-1 du code général des collectivités territoriales sont transférés aux communes membres en cas de restitution à celles-ci d'une compétence exercée par l'établissement public de coopération intercommunale (EPCI), le législateur a poursuivi des fins d'intérêt général tenant à la continuité dans l'exercice des compétences transférées et à la protection des garanties que les agents tirent de leur statut. Pour assurer l'effectivité de cette règle, il a confié au représentant de l'Etat dans le département le soin de fixer cette répartition dans la seule hypothèse d'une absence d'accord entre l'établissement public et les communes membres. Pour la mise en oeuvre de la répartition, il appartient au représentant de l'Etat de veiller, sous le contrôle du juge de l'excès de pouvoir, à garantir un partage équilibré qui tienne compte des besoins effectifs de chaque commune au regard des conditions d'exercice de la compétence restituée et des ressources dont elle dispose, y compris celles résultant de la répartition des biens et de la redéfinition des relations financières avec l'EPCI en conséquence de la même restitution de compétence. En outre, dès lors qu'il résulte du IV de l'article 94 de la loi du 6 août 2019 de transformation de la fonction publique que l'abrogation par cette loi des dispositions prévoyant la consultation des commissions administratives paritaires s'applique aux décisions relatives aux mutations et aux mobilités dès le 1er janvier 2020, la commune de Carnin ne saurait en tout état de cause se prévaloir de ce que les dispositions du IV bis de l'article L. 5211-4-1 du code général des collectivités territoriales dans leur rédaction antérieure à cette abrogation, qui ne s'est pas appliquée aux faits en litige, prescrivaient un avis des commissions administratives paritaires sur la répartition des agents par voie de convention mais l'auraient omise pour leur répartition par arrêté préfectoral. Dans ces conditions, le grief tiré de l'atteinte au principe de libre administration des collectivités territoriales ne présente pas un caractère sérieux.<br/>
<br/>
              6. Par suite, il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité, qui n'est pas nouvelle, soulevée par la commune de Carnin.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par la Commune de Carnin.<br/>
Article 2 : La présente décision sera notifiée à la commune de Carnin, au Premier ministre, au ministre de l'intérieur et à la ministre de la cohésion des territoires et des relations avec les collectivités territoriales.<br/>
Copie en sera adressée au Conseil constitutionnel et au tribunal administratif de Lille.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-015-03-01-02 ACTES LÉGISLATIFS ET ADMINISTRATIFS. - PRINCIPE DE LIBRE ADMINISTRATION DES COLLECTIVITÉS TERRITORIALES (ART. 72 DE LA CONSTITUTION) [RJ1] - MÉCONNAISSANCE - ABSENCE - FIXATION PAR LE PRÉFET, À DÉFAUT D'ACCORD ENTRE L'EPCI ET LES COMMUNES, DE LA RÉPARTITION ENTRE LES COMMUNES DES PERSONNELS EN CAS DE RESTITUTION D'UNE COMPÉTENCE EXERCÉE PAR L'EPCI (IV BIS DE L'ART. L. 5211-4-1 DU CGCT) [RJ2].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">135-05-01-01 COLLECTIVITÉS TERRITORIALES. COOPÉRATION. ÉTABLISSEMENTS PUBLICS DE COOPÉRATION INTERCOMMUNALE - QUESTIONS GÉNÉRALES. DISPOSITIONS GÉNÉRALES ET QUESTIONS COMMUNES. - RÉPARTITION ENTRE LES COMMUNES DES PERSONNELS CAS DE RESTITUTION D'UNE COMPÉTENCE EXERCÉE PAR L'EPCI (IV BIS DE L'ART. L. 5211-4-1 DU CGCT) - RÉPARTITION FIXÉE PAR LE PRÉFET À DÉFAUT D'ACCORD ENTRE L'EPCI ET LES COMMUNES - MODALITÉS [RJ2] - CONSÉQUENCE - ABSENCE D'ATTEINTE AU PRINCIPE DE LIBRE ADMINISTRATION DES COLLECTIVITÉS TERRITORIALES [RJ1].
</SCT>
<ANA ID="9A"> 01-015-03-01-02 Si le législateur peut, sur le fondement des articles 34 et 72 de la Constitution, assujettir les collectivités territoriales ou leurs groupements à des obligations et à des charges, c'est à la condition que celles-ci répondent à des exigences constitutionnelles ou concourent à des fins d'intérêt général, qu'elles ne méconnaissent pas la compétence propre des collectivités concernées, qu'elles n'entravent pas leur libre administration et qu'elles soient définies de façon suffisamment précise quant à leur objet et à leur portée. En outre, aux termes du dernier alinéa de l'article 72 de la Constitution : Dans les collectivités territoriales de la République, le représentant de l'État, représentant de chacun des membres du Gouvernement, a la charge des intérêts nationaux, du contrôle administratif et du respect des lois, de sorte qu'il appartient au législateur de prévoir l'intervention du représentant de l'État pour remédier, sous le contrôle du juge, aux difficultés résultant de l'absence de décision de la part des autorités décentralisées compétentes en se substituant à ces dernières lorsque cette absence de décision risque de compromettre le fonctionnement des services publics et l'application des lois.,,,En prévoyant que les agents mentionnés au 2° du IV bis de l'article L. 5211-4-1 du code général des collectivités territoriales (CGCT) sont transférés aux communes membres en cas de restitution à celles-ci d'une compétence exercée par l'établissement public de coopération intercommunale (EPCI), le législateur a poursuivi des fins d'intérêt général tenant à la continuité dans l'exercice des compétences transférées et à la protection des garanties que les agents tirent de leur statut. Pour assurer l'effectivité de cette règle, il a confié au représentant de l'Etat dans le département le soin de fixer cette répartition dans la seule hypothèse d'une absence d'accord entre l'établissement public et les communes membres. Pour la mise en oeuvre de la répartition, il appartient au représentant de l'Etat de veiller, sous le contrôle du juge de l'excès de pouvoir, à garantir un partage équilibré qui tienne compte des besoins effectifs de chaque commune au regard des conditions d'exercice de la compétence restituée et des ressources dont elle dispose, y compris celles résultant de la répartition des biens et de la redéfinition des relations financières avec l'EPCI en conséquence de la même restitution de compétence.... ,,Dans ces conditions, le grief tiré de l'atteinte au principe de libre administration des collectivités territoriales ne présente pas un caractère sérieux.</ANA>
<ANA ID="9B"> 135-05-01-01 Si le législateur peut, sur le fondement des articles 34 et 72 de la Constitution, assujettir les collectivités territoriales ou leurs groupements à des obligations et à des charges, c'est à la condition que celles-ci répondent à des exigences constitutionnelles ou concourent à des fins d'intérêt général, qu'elles ne méconnaissent pas la compétence propre des collectivités concernées, qu'elles n'entravent pas leur libre administration et qu'elles soient définies de façon suffisamment précise quant à leur objet et à leur portée. En outre, aux termes du dernier alinéa de l'article 72 de la Constitution : Dans les collectivités territoriales de la République, le représentant de l'État, représentant de chacun des membres du Gouvernement, a la charge des intérêts nationaux, du contrôle administratif et du respect des lois, de sorte qu'il appartient au législateur de prévoir l'intervention du représentant de l'État pour remédier, sous le contrôle du juge, aux difficultés résultant de l'absence de décision de la part des autorités décentralisées compétentes en se substituant à ces dernières lorsque cette absence de décision risque de compromettre le fonctionnement des services publics et l'application des lois.,,,En prévoyant que les agents mentionnés au 2° du IV bis de l'article L. 5211-4-1 du code général des collectivités territoriales (CGCT) sont transférés aux communes membres en cas de restitution à celles-ci d'une compétence exercée par l'établissement public de coopération intercommunale (EPCI), le législateur a poursuivi des fins d'intérêt général tenant à la continuité dans l'exercice des compétences transférées et à la protection des garanties que les agents tirent de leur statut. Pour assurer l'effectivité de cette règle, il a confié au représentant de l'Etat dans le département le soin de fixer cette répartition dans la seule hypothèse d'une absence d'accord entre l'établissement public et les communes membres. Pour la mise en oeuvre de la répartition, il appartient au représentant de l'Etat de veiller, sous le contrôle du juge de l'excès de pouvoir, à garantir un partage équilibré qui tienne compte des besoins effectifs de chaque commune au regard des conditions d'exercice de la compétence restituée et des ressources dont elle dispose, y compris celles résultant de la répartition des biens et de la redéfinition des relations financières avec l'EPCI en conséquence de la même restitution de compétence.... ,,Dans ces conditions, le grief tiré de l'atteinte au principe de libre administration des collectivités territoriales ne présente pas un caractère sérieux.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., sur la portée de ce principe s'agissant d'une intervention du représentant de l'Etat, Cons. const. 28 décembre 1982, n° 82-149 DC, pts. 11 à 13 ; Cons. const., 16 août 2007, n° 2007-556 DC, pt. 24.,,[RJ2] Cf., s'agissant de la possibilité pour le Conseil d'Etat de donner, à l'occasion de l'examen d'une QPC, une interprétation complémentaire de la loi, CE, 14 septembre 2011, M. Pierre, n° 348394, p. 441.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
