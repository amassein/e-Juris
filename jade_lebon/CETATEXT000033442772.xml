<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033442772</ID>
<ANCIEN_ID>JG_L_2016_11_000000392555</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/44/27/CETATEXT000033442772.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 4ème chambres réunies, 21/11/2016, 392555, Publié au recueil Lebon</TITRE>
<DATE_DEC>2016-11-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>392555</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Jean-Dominique Langlais</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Laurence Marion</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:392555.20161121</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B...A...a demandé au tribunal administratif de Châlons-en-Champagne d'annuler, d'une part, la décision du 18 juillet 2014 par laquelle le ministre de l'intérieur a retiré un point de son permis de conduire à la suite d'une infraction commise le 4 mai 2014, en tant que cette décision indique que le solde de points de ce permis est de trois, d'autre part, la décision retirant un point de son permis à la suite d'une infraction commise le 21 mars 2015. Par un jugement n° 1401719 du 2 juin 2015, le tribunal administratif a partiellement fait droit à sa demande en annulant la décision du 18 juillet 2014 du ministre de l'intérieur en tant qu'elle fixe le solde de son permis à trois points au lieu de onze. <br/>
<br/>
              Par un pourvoi, enregistré le 11 août 2015 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'intérieur demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement en tant qu'il fait partiellement droit aux conclusions de MmeA... ; <br/>
<br/>
              2°) réglant, dans cette mesure, l'affaire au fond, de rejeter la demande de l'intéressée.<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - le code de la route ;<br/>
<br/>
              - la loi n° 2011-267 du 14 mars 2011 ; <br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Langlais, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Laurence Marion, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par une décision du 18 juillet 2014, le ministre de l'intérieur a informé Mme A...qu'à la suite d'une infraction commise le 4 mai 2014, le capital de points attaché à son permis de conduire était ramené à trois points ; que le ministre se pourvoit en cassation contre le jugement du 2 juin 2015 par lequel le tribunal administratif de Châlons-en-Champagne a, à la demande de MmeA..., annulé cette décision en tant qu'elle fixait le solde de points restant affecté au capital du permis de conduire de l'intéressée à trois points au lieu de onze ; <br/>
<br/>
              2. Considérant que, d'une part, aux termes du premier alinéa de l'article L. 223-6 du code de la route, dans sa rédaction antérieure à la loi du 14 mars 2011 d'orientation et de programmation pour la performance de la sécurité intérieure : " Si le titulaire du permis de conduire n'a pas commis, dans le délai de trois ans à compter de la date du paiement de la dernière amende forfaitaire, de l'émission du titre exécutoire de la dernière amende forfaitaire majorée, de l'exécution de la dernière composition pénale ou de la dernière condamnation définitive, une nouvelle infraction ayant donné lieu au retrait de points, son permis est affecté du nombre maximal de points " ; que l'article 76 de la loi du 14 mars 2011 a modifié ces dispositions en ramenant à deux ans à compter de l'un des quatre événements qu'elles mentionnent le délai de reconstitution du nombre maximal de points et a ajouté au même article L. 223-6 un deuxième alinéa disposant que : " Le délai de deux ans mentionné au premier alinéa est porté à trois ans si l'une des infractions ayant entraîné un retrait de points est un délit ou une contravention de la quatrième ou de la cinquième classe " ; que, d'autre part, l'article 138 de la loi du 14 mars 2011 prévoit que la modification apportée par cette loi à l'article L. 223-6 du code de la route " s'applique aux infractions commises à compter du 1er janvier 2011 et aux infractions antérieures pour lesquelles le paiement de l'amende forfaitaire, l'émission du titre exécutoire de l'amende forfaitaire majorée, l'exécution de la composition pénale ou la condamnation définitive ne sont pas intervenus " ; <br/>
<br/>
<br/>
              3. Considérant qu'il résulte de ces dispositions que la date à laquelle la réalité d'une infraction entraînant retrait de points du permis de conduire est établie par le paiement de l'amende forfaitaire, l'émission d'un titre exécutoire d'amende forfaitaire majorée, l'exécution d'une composition pénale ou le prononcé d'une condamnation pénale définitive, fait courir un délai à l'expiration duquel, en l'absence de nouvelle infraction ayant entraîné un retrait de points, le titulaire du permis bénéficie d'une reconstitution intégrale de son capital de points ; que, lorsque la réalité de l'infraction a été établie à une date antérieure au 1er janvier 2011, la reconstitution de points n'a pu intervenir, en l'absence de nouvelle infraction, qu'à l'expiration du délai de trois ans prévu dans tous les cas par les dispositions de l'article L. 223-6 dans sa rédaction antérieure à la loi du 14 mars 2011 ; que, lorsque la réalité de l'infraction a été établie postérieurement au 31 décembre 2010, la durée du délai de reconstitution intégrale est déterminée par les dispositions du même article tel que modifié par cette loi ; qu'elle est normalement de deux ans mais est portée à trois ans si une des infractions commises par l'intéressé depuis la délivrance de son permis de conduire ou, le cas échéant, depuis la date de la dernière reconstitution intégrale opérée en application des deux premiers alinéas de l'article L. 223-6 a présenté le caractère d'un délit ou d'une contravention de la 4e ou de la 5e classe ; <br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède qu'en se bornant à relever que l'infraction qui avait donné lieu au paiement d'une amende par Mme A... le 14 mars 2012 relevait d'une contravention de la troisième classe, pour en déduire qu'en l'absence d'infraction ayant entraîné retrait de points pendant une période de deux ans à compter de cette date, le capital de points du permis de l'intéressée avait été entièrement reconstitué en application du premier alinéa de l'article L. 223-6 précité, sans rechercher si Mme A...avait commis un délit ou une infraction relevant de la quatrième ou de la cinquième classe depuis la délivrance de son permis de conduire ou depuis la dernière reconstitution intégrale de son capital de points opérée en application des deux premiers alinéas de l'article L. 223-6, le tribunal administratif a commis une erreur de droit ; que son jugement doit, par suite, être annulé en tant qu'il fait partiellement droit à la demande de MmeA... ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le jugement du 2 juin 2015 du tribunal administratif de Châlons-en-Champagne est annulé en tant qu'il annule la décision du 18 juillet 2014 du ministre de l'intérieur en tant qu'elle fixe le solde du permis de conduire de Mme A...à trois points au lieu de onze.<br/>
<br/>
Article 2 : L'affaire est renvoyée, dans cette mesure, au tribunal administratif de Châlons-en-Champagne.<br/>
Article 3 : La présente décision sera notifiée au ministre de l'intérieur et à Mme B...A....<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-08 ACTES LÉGISLATIFS ET ADMINISTRATIFS. APPLICATION DANS LE TEMPS. - MODIFICATION DU DÉLAI DE RECONSTITUTION INTÉGRALE DES POINTS DU PERMIS DE CONDUIRE (ART. L. 223-6 DU CODE DE LA ROUTE) - PRISE EN COMPTE D'UNE INFRACTION ANTÉRIEURE À L'ENTRÉE EN VIGUEUR D'UNE LOI - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">49-04-01-04-04 POLICE. POLICE GÉNÉRALE. CIRCULATION ET STATIONNEMENT. PERMIS DE CONDUIRE. RESTITUTION DE POINTS. - RECONSTITUTION INTÉGRALE DES POINTS DU PERMIS (ART. L. 223-6 DU CODE DE LA ROUTE) - NOUVEAU RÉGIME (LOI DU 14 MARS 2011) - DÉLAI DE DEUX ANS EN PRINCIPE ET DE TROIS EN CAS DE COMMISSION D'INFRACTION GRAVE - PRISE EN COMPTE D'UNE INFRACTION GRAVE COMMISE AVANT L'ENTRÉE EN VIGUEUR DE LA NOUVELLE LOI - EXISTENCE.
</SCT>
<ANA ID="9A"> 01-08 L'article 76 de la loi n° 2011-267 du 14 mars 2011 a modifié l'article L. 223-6 du code de la route pour faire passer le délai de reconstitution intégrale des points du permis de trois à deux ans, sauf en cas de commission d'un délit ou d'une infraction de quatrième ou cinquième classe.... ,,Il résulte de ces dispositions que la date à laquelle la réalité d'une infraction entraînant retrait de points du permis de conduire est établie par le paiement de l'amende forfaitaire, l'émission d'un titre exécutoire d'amende forfaitaire majorée, l'exécution d'une composition pénale ou le prononcé d'une condamnation pénale définitive, fait courir un délai à l'expiration duquel, en l'absence de nouvelle infraction ayant entraîné un retrait de points, le titulaire du permis bénéficie d'une reconstitution intégrale de son capital de points.... ,,Lorsque la réalité de l'infraction a été établie à une date antérieure au 1er janvier 2011, la reconstitution de points n'a pu intervenir, en l'absence de nouvelle infraction, qu'à l'expiration du délai de trois ans prévu dans tous les cas par les dispositions de l'article L. 223-6 dans sa rédaction antérieure à la loi du 14 mars 2011.... ,,Lorsque la réalité de l'infraction a été établie postérieurement au 31 décembre 2010, la durée du délai de reconstitution intégrale est déterminée par les dispositions du même article tel que modifié par cette loi. Elle est normalement de deux ans mais est portée à trois ans si une des infractions commises par l'intéressé depuis la délivrance de son permis de conduire ou, le cas échéant, depuis la date de la dernière reconstitution intégrale opérée en application des deux premiers alinéas de l'article L. 223-6 a présenté le caractère d'un délit ou d'une contravention de la 4e ou de la 5e classe.</ANA>
<ANA ID="9B"> 49-04-01-04-04 L'article 76 de la loi n° 2011-267 du 14 mars 2011 a modifié l'article  L. 223-6 du code de la route pour faire passer le délai de reconstitution intégrale des points du permis de trois à deux ans, sauf en cas de commission d'un délit ou d'une infraction de quatrième ou cinquième classe.... ,,Il résulte de ces dispositions que la date à laquelle la réalité d'une infraction entraînant retrait de points du permis de conduire est établie par le paiement de l'amende forfaitaire, l'émission d'un titre exécutoire d'amende forfaitaire majorée, l'exécution d'une composition pénale ou le prononcé d'une condamnation pénale définitive, fait courir un délai à l'expiration duquel, en l'absence de nouvelle infraction ayant entraîné un retrait de points, le titulaire du permis bénéficie d'une reconstitution intégrale de son capital de points.... ,,Lorsque la réalité de l'infraction a été établie à une date antérieure au 1er janvier 2011, la reconstitution de points n'a pu intervenir, en l'absence de nouvelle infraction, qu'à l'expiration du délai de trois ans prévu dans tous les cas par les dispositions de l'article L. 223-6 dans sa rédaction antérieure à la loi du 14 mars 2011.... ,,Lorsque la réalité de l'infraction a été établie postérieurement au 31 décembre 2010, la durée du délai de reconstitution intégrale est déterminée par les dispositions du même article tel que modifié par cette loi. Elle est normalement de deux ans mais est portée à trois ans si une des infractions commises par l'intéressé depuis la délivrance de son permis de conduire ou, le cas échéant, depuis la date de la dernière reconstitution intégrale opérée en application des deux premiers alinéas de l'article L. 223-6 a présenté le caractère d'un délit ou d'une contravention de la 4e ou de la 5e classe.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
