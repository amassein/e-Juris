<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044471236</ID>
<ANCIEN_ID>JG_L_2021_12_000000442111</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/47/12/CETATEXT000044471236.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 10/12/2021, 442111</TITRE>
<DATE_DEC>2021-12-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>442111</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Jean-Marc Vié</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Karin Ciavaldini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:442111.20211210</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. C... I... a demandé au tribunal des pensions de Marseille de réformer l'arrêté du 23 octobre 2017 portant révision de la pension militaire d'invalidité dont il est titulaire, en tant que cet arrêté ne prévoit pas son indemnisation à raison d'une infirmité nouvelle de " séquelles d'entorses de la cheville droite traitées chirurgicalement ". Par un jugement n° 17/00145 du 13 septembre 2018, ce tribunal a accordé à M. I..., à raison de cette infirmité, une pension militaire d'invalidité au taux de 20 %, dont 15 % imputables au service.<br/>
<br/>
              Par un arrêt n° 19MA05050 du 16 juin 2020, la cour administrative d'appel de Marseille a, sur appel de la ministre des armées, annulé ce jugement et rejeté la demande de M. I....<br/>
<br/>
              Par un pourvoi et un mémoire en réplique, enregistrés les 23 juillet 2020 et 1er juillet 2021, M. I... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de la ministre des armées ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le protocole provisoire du 27 juin 1977 fixant les conditions de stationnement des forces françaises sur le territoire de la République de Djibouti après l'indépendance et les principes de la coopération militaire entre le Gouvernement de la République française et le Gouvernement de la République de Djibouti ;<br/>
              - le code des pensions militaire d'invalidité et des victimes de la guerre ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Marc Vié, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Karin Ciavaldini, rapporteure publique ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. I..., militaire dans la Légion étrangère entre 2004 et 2015, est titulaire d'une pension militaire d'invalidité, concédée par arrêté du 19 janvier 2015 et révisée par arrêté du 23 octobre 2017, au taux de 55 %, pour " séquelles d'entorses de la cheville gauche traitées chirurgicalement, hypoacousie bilatérale et acouphènes bilatéraux permanents ". A l'occasion de la révision de cette pension, la ministre des armées a rejeté la demande de M. I... tendant à ce que soit indemnisée une infirmité nouvelle de " séquelles d'entorse de la cheville droite traitée chirurgicalement " au motif que si  le taux global de cette infirmité était de 20 %, elle ne résultait qu'à concurrence d'un taux de 5 % d'un accident survenu lors d'un exercice en Nouvelle-Calédonie le 1er septembre 2014 et n'était par suite imputable au service que dans cette mesure. M. I... a demandé au tribunal des pensions de Marseille de réformer cet arrêté en tant qu'il portait rejet de sa demande tendant à l'indemnisation de cette infirmité. Il faisait notamment valoir que cette entorse avait été constatée dès le 3 juin 2008, alors qu'il était affecté à Djibouti dans le cadre d'un renfort temporaire à l'étranger. Par un jugement du 13 septembre 2018, le tribunal des pensions de Marseille, faisant droit à sa demande, lui a accordé le bénéfice d'une pension militaire d'invalidité pour " séquelles d'entorses de la cheville droite traitées chirurgicalement " au taux de 20 %, dont 15 % imputables au service. M. I... se pourvoit en cassation contre l'arrêt du 16 juin 2020 par lequel la cour administrative d'appel de Marseille a, sur appel de la ministre des armées, annulé ce jugement et rejeté sa demande.  <br/>
<br/>
              2. Aux termes de l'article L. 2 du code des pensions militaires d'invalidité et des victimes de la guerre, applicable à la date de la constatation de l'infirmité invoquée par M. I...: " Ouvrent droit à pension : / 1° Les infirmités résultant de blessures reçues par suite d'événements de guerre ou d'accidents éprouvés par le fait ou à l'occasion du service ; / 2° Les infirmités résultant de maladies contractées par le fait ou à l'occasion du service ; / 3° L'aggravation par le fait ou à l'occasion du service d'infirmités étrangères au service ; / 4° Les infirmités résultant de blessures reçues par suite d'accidents éprouvés entre le début et la fin d'une mission opérationnelle, y compris les opérations d'expertise ou d'essai, ou d'entraînement ou en escale, sauf faute de la victime détachable du service. ". L'article D. 1 du même code, également applicable à cette date, précise que : " Sont considérées comme missions opérationnelles, au sens des dispositions du 4° de l'article L. 2, les missions suivantes : / a) Les opérations extérieures conduites sous la responsabilité de l'état-major des armées quelle que soit leur nature et les missions effectuées à l'étranger au titre d'unités françaises ou alliées ou de forces internationales conformément aux obligations et engagements internationaux de la France (...) ". <br/>
<br/>
              3. Il résulte des pièces du dossier soumis aux juges du fond que, pour juger que l'administration avait pu à bon droit rejeter la demande de M. I... tendant à la révision de sa pension, la cour administrative d'appel de Marseille s'est fondée sur ce qu'il ne résultait pas de l'instruction que l'affectation temporaire de ce dernier à Djibouti entre le 19 juin et le 22 octobre 2008, au titre d'une mission de renfort temporaire à l'étranger, aurait été justifiée par la participation à une mission effectuée à l'étranger au titre d'unités françaises ou alliées ou de forces internationales conformément aux obligations et engagements internationaux de la France. En statuant ainsi, alors que la présence militaire française à Djibouti, qui résultait de la mise en œuvre du protocole provisoire du 27 juin 1977 fixant les conditions de stationnement des forces françaises conclu entre la France et la République de Djibouti, constituait une mission opérationnelle au sens du a) de l'article D. 1 du code des pensions militaires d'invalidité et des victimes de la guerre et que les infirmités résultant de blessures reçues par suite d'accidents entre le début et la fin de cette mission étaient en conséquence susceptibles d'ouvrir droit à pension, en vertu du 4°) de l'article L. 2 du même code, au bénéfice des militaires qui y participaient, la cour a commis une erreur de droit.<br/>
<br/>
              4. Il résulte de ce qui précède que M. I... est fondé à demander l'annulation de l'arrêt qu'il attaque. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à M. I... au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 16 juin 2020 de la cour administrative d'appel de Marseille est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Marseille.<br/>
Article 3 : L'Etat versera à M. I... une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à M. C... I... et à la ministre des armées.<br/>
              Délibéré à l'issue de la séance du 1er décembre 2021 où siégeaient : M. Guillaume Goulard, président de chambre, présidant ; M. Pierre Collin, président de chambre ; M. H... M..., M. E... L..., M. J... G..., M. B... N..., Mme K... A..., M. Jonathan Bosredon, conseiller d'Etat et M. Jean-Marc Vié, maître des requêtes-rapporteur. <br/>
<br/>
              Rendu le 10 décembre 2021.<br/>
<br/>
<br/>
                 Le président : <br/>
                 Signé : M. Guillaume Goulard<br/>
 		Le rapporteur : <br/>
      Signé : M. Jean-Marc Vié<br/>
                 La secrétaire :<br/>
                 Signé : Mme D... F...<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">48-01-02-02 PENSIONS. - PENSIONS MILITAIRES D'INVALIDITÉ ET DES VICTIMES DE GUERRE. - CONDITIONS D'OCTROI D'UNE PENSION. - FAIT GÉNÉRATEUR. - INFIRMITÉS RÉSULTANT DE BLESSURES REÇUES PAR SUITE D'ACCIDENTS ÉPROUVÉS ENTRE LE DÉBUT ET LA FIN D'UNE MISSION OPÉRATIONNELLE (4° DE L'ART. L. 2 DU CPMIVG) - NOTION DE MISSION OPÉRATIONNELLE (A. DE L'ART. D1) - INCLUSION - PRÉSENCE MILITAIRE FRANÇAISE À DJIBOUTI RÉSULTANT DE LA MISE EN ŒUVRE DU PROTOCOLE PROVISOIRE DU 27 JUIN 1977.
</SCT>
<ANA ID="9A"> 48-01-02-02 La présence militaire française à Djibouti résultant de la mise en œuvre du protocole provisoire du 27 juin 1977 fixant les conditions de stationnement des forces françaises conclu entre la France et la République de Djibouti, constitue une mission opérationnelle au sens du a) de l'article D. 1 du code des pensions militaires d'invalidité et des victimes de la guerre (CPMIVG).......Dès lors, les infirmités résultant de blessures reçues par suite d'accidents entre le début et la fin de cette mission sont susceptibles d'ouvrir droit à pension, en vertu du 4°) de l'article L. 2 du même code, au bénéfice des militaires qui y participent.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
