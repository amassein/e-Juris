<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000021852511</ID>
<ANCIEN_ID>JG_L_2010_02_000000323158</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/21/85/25/CETATEXT000021852511.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème et 2ème sous-sections réunies, 08/02/2010, 323158</TITRE>
<DATE_DEC>2010-02-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>323158</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème et 2ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Arrighi de Casanova</PRESIDENT>
<AVOCATS>SCP BOULLOCHE ; SCP LYON-CAEN, FABIANI, THIRIEZ ; SCP BORE ET SALVE DE BRUNETON ; SCP PEIGNOT, GARREAU</AVOCATS>
<RAPPORTEUR>M. Nicolas  Polge</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Dacosta Bertrand</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2010:323158.20100208</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 11 décembre 2008 au secrétariat du contentieux du Conseil d'Etat, présenté pour la COMMUNE DE CHARTRES, représentée par son maire ; la commune demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt du 26 septembre 2008 par lequel la cour administrative d'appel de Nantes a rejeté sa requête tendant à l'annulation du jugement du 5 décembre 2006 du tribunal administratif d'Orléans, en tant qu'il a, d'une part, annulé la délibération du 24 octobre 2003 par laquelle le maire avait été autorisé à signer avec la société Chartres Stationnement une convention de délégation de service public pour la construction et l'exploitation d'un parc de stationnement souterrain et la rénovation et l'exploitation de trois autres parcs de stationnement et, d'autre part, enjoint à la commune de prendre toutes mesures utiles à l'effet de mettre fin à la convention et, à défaut d'accord amiable avec la société délégataire, de saisir le juge du contrat afin que celui-ci en constate la nullité ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre la somme de 4 000 euros à la charge de l'Association de défense des contribuables de Chartres et de son agglomération, de Mme C et de Mme A au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des collectivités territoriales ;<br/>
<br/>
              Vu la loi n° 91-647 du 10 juillet 1991 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Nicolas Polge, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Boulloche, avocat de la COMMUNE DE CHARTRES, de la SCP Peignot, Garreau, avocat de la société Q Park et de la SCP Boré et Salve de Bruneton, avocat de l'association de défense des intérêts des contribuables de Chartres, <br/>
<br/>
              - les conclusions de M. Bertrand Dacosta, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Boulloche, avocat de la COMMUNE DE CHARTRES, à la SCP Peignot, Garreau, avocat de la société Q Park et à la SCP Boré et Salve de Bruneton, avocat de l'association de défense des intérêts des contribuables de Chartres ; <br/>
<br/>
<br/>
<br/>
<br/>Considérant que par délibération du 24 octobre 2003, le conseil municipal de la COMMUNE DE CHARTRES a autorisé son maire à signer avec la société Chartres Stationnement une convention de délégation de service public pour la construction et l'exploitation d'un parc de stationnement souterrain et la rénovation et l'exploitation de trois autres parcs de stationnement ; que par jugement du 5 décembre 2006, le tribunal administratif d'Orléans a annulé cette délibération et a enjoint à la commune de prendre toutes mesures utiles à l'effet de mettre fin à ladite convention et, à défaut d'accord amiable avec la société délégataire, de saisir le juge du contrat  afin que celui-ci en constate la nullité ; que par arrêt du 26 septembre 2008, contre lequel la COMMUNE DE CHARTRES s'est pourvue en cassation, la cour administrative d'appel de Nantes a confirmé cette annulation ainsi que les mesures d'injonction prononcées à l'encontre de la commune ;<br/>
<br/>
              Sur l'intervention de la société Q Park France :<br/>
<br/>
              Considérant que la société Q Park France, en tant que société actionnaire de la société Chartres Stationnement dont l'offre a été retenue pour l'attribution de la délégation de service public en litige, ne se prévaut pas d'un intérêt propre, distinct de celui que défend cette société elle-même ; que, dès lors, son intervention à l'appui du pourvoi de la COMMUNE DE CHARTRES n'est pas recevable ;<br/>
<br/>
              Sur les conclusions tendant à l'annulation de l'arrêt attaqué : <br/>
<br/>
              Considérant qu'aux termes de l'article L. 1411-2 du code général des collectivités territoriales : " Les conventions de délégation de service public doivent être limitées dans leur durée. Celle-ci est déterminée par la collectivité en fonction des prestations demandées au délégataire. Lorsque les installations sont à la charge du délégataire, la convention de délégation tient compte, pour la détermination de sa durée, de la nature et du montant de l'investissement à réaliser et ne peut dans ce cas dépasser la durée normale d'amortissement des installations mises en oeuvre (...) " ; qu'il résulte de ces dispositions que la durée normale d'amortissement des installations susceptible d'être retenue par une collectivité délégante peut être la durée normalement attendue pour que le délégataire puisse couvrir ses charges d'exploitation et d'investissement, compte tenu des contraintes d'exploitation liées à la nature du service et des exigences du délégant, ainsi que de la prévision des tarifs payés par les usagers, que cette durée coïncide ou non avec la durée de l'amortissement comptable des investissements ; que, de plus, le point de départ de l'amortissement étant la date d'achèvement des investissements et de mise en service de l'ouvrage, il convient, afin d'évaluer la durée maximale de la délégation, d'ajouter le temps nécessaire à la réalisation de ces investissements à leur durée normale d'amortissement ; <br/>
<br/>
              Considérant que, pour annuler la délibération du 24 octobre 2003 autorisant la signature de la convention litigieuse, au motif que la durée de trente-deux ans qu'elle retenait excédait la durée maximale permise par les dispositions de l'article L. 1411-2 du code général des collectivités territoriales, la cour administrative d'appel de Nantes a relevé que la durée normale d'amortissement des installations dont la réalisation était mise à la charge du délégataire était de trente ans, excluant la période nécessaire à la réalisation de ces investissements ; qu'en statuant ainsi, alors que cette période s'ajoute, ainsi qu'il vient d'être dit, à la durée normale de leur amortissement, la cour administrative d'appel a commis une erreur de droit ; que, par suite, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, son arrêt doit être annulé ;<br/>
<br/>
              Sur les conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              Considérant que les dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 font obstacle à ce qu'il soit fait droit aux conclusions présentées sur le fondement de ces dispositions par la SCP Boré et Salve de Bruneton, avocat de l'Association de défense des contribuables de Chartres et de son agglomération, à l'encontre de la COMMUNE DE CHARTRES qui n'est pas, dans la présente instance, la partie perdante ; que la société Q Park France, intervenant en demande, n'étant pas partie à la présente instance, les dispositions de l'article L. 761-1 du code de justice administrative font également obstacle à ce qu'une somme soit mise à la charge des parties perdantes et à son bénéfice au titre des frais exposés par elle et non compris dans les dépens ; qu'il y a lieu, en revanche, dans les circonstances de l'espèce, de mettre à la charge de l'Association de défense des intérêts des contribuables de Chartres et de son agglomération, de Mme C et de Mme A la somme de 1 000 euros chacune au titre des frais exposés par la COMMUNE DE CHARTRES et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'intervention de la société Q Park France n'est pas admise.<br/>
<br/>
Article 2 : L'arrêt de la cour administrative d'appel de Nantes du 11 décembre 2008 est annulé.<br/>
<br/>
            Article 3 : L'affaire est renvoyée à la cour administrative d'appel de Nantes.<br/>
Article 4 : L'Association de défense des intérêts des contribuables de Chartres et de son agglomération, Mme C et Mme A verseront chacune la somme de 1 000 euros à la COMMUNE DE CHARTRES au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 5 : Les conclusions de la SCP Boré et Salve de Bruneton, avocat de l'Association de défense des intérêts des contribuables de Chartres et de son agglomération, tendant à l'application des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 sont rejetées.<br/>
Article 6 : La présente décision sera notifiée à la COMMUNE DE CHARTRES, à l'Association de défense des intérêts des contribuables de Chartres et de son agglomération, à Mme Chantal C, à Mme Mauricette A, à M. Laurent D, à la société Chartres Stationnement, à la société Fix-Auxifip, à la caisse régionale de Crédit agricole mutuel Val-de-France et à la société Q Park France.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-01-03-03 MARCHÉS ET CONTRATS ADMINISTRATIFS. NOTION DE CONTRAT ADMINISTRATIF. DIVERSES SORTES DE CONTRATS. DÉLÉGATIONS DE SERVICE PUBLIC. - DURÉE DE LA CONVENTION - 1) DURÉE NORMALE D'AMORTISSEMENT (ART. L. 1411-2 DU CGCT) - NOTION - 2) MODALITÉS DE CALCUL - POINT DE DÉPART - PRISE EN COMPTE DU TEMPS NÉCESSAIRE À LA RÉALISATION DES INVESTISSEMENTS.
</SCT>
<ANA ID="9A"> 39-01-03-03 L'article L. 1411-2 du code général des collectivités territoriales (CGCT) prévoit que la durée d'une convention de délégation de service public ne peut dépasser la durée normale d'amortissement des installations mises en oeuvre. 1) La durée normale d'amortissement des installations susceptible d'être retenue par une collectivité délégante peut être la durée normalement attendue pour que le délégataire puisse couvrir ses charges d'exploitation et d'investissement, compte tenu des contraintes d'exploitation liées à la nature du service et des exigences du délégant, ainsi que de la prévision des tarifs payés par les usagers - que cette durée coïncide ou non avec la durée de l'amortissement comptable des investissements. 2) Le point de départ de l'amortissement est la date d'achèvement des investissements et de mise en service de l'ouvrage. Il convient, afin d'évaluer la durée maximale de la délégation, d'ajouter le temps nécessaire à la réalisation de ces investissements à leur durée normale d'amortissement.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
