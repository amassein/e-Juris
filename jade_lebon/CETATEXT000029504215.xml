<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029504215</ID>
<ANCIEN_ID>JG_L_2014_09_000000361293</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/50/42/CETATEXT000029504215.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème / 7ème SSR, 26/09/2014, 361293</TITRE>
<DATE_DEC>2014-09-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>361293</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème / 7ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Tristan Aureau</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier Domino</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:361293.20140926</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 24 juillet 2012 au secrétariat du contentieux du Conseil d'Etat, présentée par le syndicat national des collèges et lycées, dont le siège est 13, avenue de Taillebourg à Paris (75013), représenté par son secrétaire général ; le syndicat requérant demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la circulaire du ministre de l'éducation nationale n° 2012-080 du 20 avril 2012 relative à l'accès aux technologies de l'information et de la communication, en tant qu'elle restreint aux seules organisations représentatives les possibilités d'utilisation de l'espace dédié à la communication des organisations syndicales sur l'intranet du ministère et d'inscription des agents aux listes de diffusion constituées par les organisations syndicales ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative et la somme de 35 euros au titre de l'article R. 761-1 du même code ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution, notamment son Préambule ;<br/>
<br/>
              Vu la loi n° 83-634 du 13 juillet 1983 ; <br/>
<br/>
              Vu la loi n° 84-16 du 11 janvier 1984 ;<br/>
<br/>
              Vu le décret n° 82-447 du 28 mai 1982 ;<br/>
<br/>
              Vu le décret n° 2012-224 du 16 février 2012 ; <br/>
<br/>
               Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Tristan Aureau, auditeur, <br/>
<br/>
              - les conclusions de Mme Béatrice Bourgeois-Machureau, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'aux termes du premier alinéa de l'article 8 de la loi du 13 juillet 1983 portant droits et obligations des fonctionnaires : " Le droit syndical est garanti aux fonctionnaires. Les intéressés peuvent librement créer des organisations syndicales, y adhérer et y exercer des mandats. Ces organisations peuvent ester en justice " ;  <br/>
<br/>
              2.	Considérant qu'aux termes des dispositions de l'article 3-1 du décret du 28 mai 1982 relatif à l'exercice du droit syndical dans la fonction publique, issu du décret du 16 février 2012 : " Les conditions d'utilisation par les organisations syndicales, au sein des services, des technologies de l'information et de la communication sont fixées dans chaque ministère, établissement public ou autorité administrative indépendante par une décision du ministre ou du chef de service après avis du comité technique correspondant. Un arrêté du ministre chargé de la fonction publique définit le cadre général de cette utilisation ainsi que les conditions dans lesquelles sont garantis la confidentialité, le libre choix et la non-discrimination auxquelles elle est subordonnée " ; que, sur le fondement de ces dispositions, le ministre de l'éducation nationale, de la jeunesse et de la vie associative a, par une circulaire du 20 avril 2014, précisé les moyens mis par le ministère à la disposition des organisations syndicales et de leurs représentants dans le domaine des technologies de l'information et de la communication ; qu'il a notamment fixé les modalités d'utilisation de la messagerie électronique et les conditions de création et d'utilisation d'un espace dédié à la communication des organisations syndicales sur l'intranet ; que le syndicat national des collèges et lycées demande l'annulation pour excès de pouvoir de cette circulaire, en tant qu'elle restreint, en dehors de la  période électorale, aux seules organisations syndicales représentatives les possibilités de création et d'utilisation d'un espace réservé sur l'intranet ministériel ; <br/>
<br/>
              3.	Considérant que les principes de liberté syndicale et de non discrimination  entre organisations syndicales légalement constituées font obstacle à ce que soient réservés aux seules organisations syndicales représentatives les moyens destinés à faciliter l'exercice du droit syndical au sein de l'administration si ceux-ci ne sont pas limités en raison de contraintes particulières ou des nécessités du service ; qu'il ne ressort pas des pièces du dossier, et n'est au demeurant pas allégué, que l'utilisation d'espaces sur le réseau intranet du ministère serait limité par des contraintes particulières ou par les nécessités du service ; que, par suite, ce moyen de communication ne peut être réservé aux seules organisations syndicales représentatives et doit être mis à la disposition de toutes les organisations syndicales légalement constituées, comme le sont, au demeurant, les panneaux d'affichage en vertu de l'article 8 du décret du 28 mai 1982 ;  <br/>
<br/>
              4.	Considérant, dès lors, que le syndicat requérant est fondé à soutenir que la circulaire attaquée, en tant qu'elle réserve hors période électorale, aux seules organisations syndicales représentatives l'utilisation d'un espace dédié sur l'intranet ministériel, est entachée d'illégalité ; que, par suite, sans qu'il soit besoin d'examiner les autres moyens de la requête, la circulaire attaquée doit être, dans cette mesure, annulée ; <br/>
<br/>
              5.	Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme globale de 1 500 euros à verser au syndicat national des collèges et lycées au titre des dispositions de l'article L. 761-1 du code de justice administrative et de l'article R. 761-1 du même code en vigueur à la date d'introduction de la requête ;  <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La circulaire du ministre de l'éducation nationale n° 2012-080 du 20 avril 2012 est annulée en tant qu'elle réserve hors période électorale, aux seules organisations syndicales représentatives l'utilisation d'un espace dédié sur l'intranet ministériel. <br/>
<br/>
Article 2 : L'Etat versera au syndicat national des collèges et lycées une somme de 1 500 euros au titre des dispositions des articles L. 761-1 et R. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée au syndicat national des collèges et lycées et au ministre de l'éducation nationale de l'enseignement supérieur et de la recherche.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-07-09 FONCTIONNAIRES ET AGENTS PUBLICS. STATUTS, DROITS, OBLIGATIONS ET GARANTIES. DROIT SYNDICAL. - MOYENS MIS À DISPOSITION DES ORGANISATIONS SYNDICALES - POSSIBILITÉ DE LES RÉSERVER AUX ORGANISATIONS SYNDICALES REPRÉSENTATIVES - ABSENCE, EN PRINCIPE - EXCEPTION - CARACTÈRE LIMITÉ DE CES MOYENS EN RAISON DE CONTRAINTES PARTICULIÈRES OU DES NÉCESSITÉS DU SERVICE.
</SCT>
<ANA ID="9A"> 36-07-09 Les principes de liberté syndicale et de non discrimination  entre organisations syndicales légalement constituées font obstacle à ce que soient réservés aux seules organisations syndicales représentatives les moyens destinés à faciliter l'exercice du droit syndical au sein de l'administration si ceux-ci ne sont pas limités en raison de contraintes particulières ou des nécessités du service.... ,,En l'espèce, illégalité d'une circulaire réservant aux seules organisations syndicales représentatives l'utilisation d'un espace dédié sur l'intranet hors période électorale.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>Comp., s'agissant de l'utilisation d'un local syndical, CE, 23 juillet 2014, n° 358349 358412 358552 358619 358628, Syndicat national des collèges et des lycées et autres, aux tables sur un autre point.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
