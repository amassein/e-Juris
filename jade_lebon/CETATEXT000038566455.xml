<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038566455</ID>
<ANCIEN_ID>JG_L_2019_06_000000426772</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/56/64/CETATEXT000038566455.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 07/06/2019, 426772, Publié au recueil Lebon</TITRE>
<DATE_DEC>2019-06-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>426772</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR>M. Raphaël Chambon</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Anne Iljic</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:426772.20190607</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A...B...a demandé au juge des référés du tribunal administratif d'Amiens de suspendre l'exécution de la décision du 25 octobre 2018 par laquelle la directrice interrégionale des services pénitentiaires de Lille a prolongé, à compter du 27 octobre 2018, la mesure de placement à l'isolement prise à son encontre.<br/>
<br/>
              Par une ordonnance n° 1803373 du 4 décembre 2018, le juge des référés du tribunal administratif d'Amiens a rejeté cette demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 2 et 14 janvier 2019 au secrétariat du contentieux du Conseil d'Etat, Mme B...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de faire droit à sa demande ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code de procédure pénale ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Raphaël Chambon, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Anne Iljic, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Spinosi, Sureau, avocat de Mme B...;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". <br/>
<br/>
              2. La Section française de l'observatoire international des prisons justifie, eu égard à la nature et à l'objet du litige, d'un intérêt suffisant pour intervenir dans la présente instance au soutien du pourvoi. Son intervention est, par suite, recevable.<br/>
<br/>
              3. Aux termes du premier alinéa de l'article 726-1 du code de procédure pénale : " Toute personne détenue, sauf si elle est mineure, peut être placée par l'autorité administrative, pour une durée maximale de trois mois, à l'isolement par mesure de protection ou de sécurité soit à sa demande, soit d'office. Cette mesure ne peut être renouvelée pour la même durée qu'après un débat contradictoire, au cours duquel la personne concernée, qui peut être assistée de son avocat, présente ses observations orales ou écrites. L'isolement ne peut être prolongé au-delà d'un an qu'après avis de l'autorité judiciaire ".<br/>
<br/>
              4. Eu égard à son objet et à ses effets sur les conditions de détention, la décision plaçant d'office à l'isolement une personne détenue ainsi que les décisions prolongeant éventuellement un tel placement, prises sur le fondement de l'article 726-1 du code de procédure pénale, portent en principe, sauf à ce que l'administration pénitentiaire fasse valoir des circonstances particulières, une atteinte grave et immédiate à la situation de la personne détenue, de nature à créer une situation d'urgence justifiant que le juge administratif des référés, saisi sur le fondement de l'article L. 521-1 du code de justice administrative, puisse ordonner la suspension de leur exécution s'il estime remplie l'autre condition posée par cet article.<br/>
<br/>
              5. En l'espèce, toutefois, si le juge des référés du tribunal administratif d'Amiens a rejeté pour défaut d'urgence la demande de Mme B...tendant, sur le fondement de l'article L. 521-1 du code de justice administrative, à la suspension de l'exécution de la décision du 25 octobre 2018 par laquelle la directrice interrégionale des services pénitentiaires de Lille a prolongé pour une durée de trois mois à compter du 27 octobre 2018 la mesure de placement à l'isolement de l'intéressée, il résulte de cette décision qu'elle a cessé de produire effet le 26 janvier 2019. Dans ces conditions, le pourvoi en cassation formé par Mme B...le 2 janvier 2019 contre l'ordonnance rendue le 4 décembre 2018 par le juge des référés a perdu son objet. Il n'y a, dès lors, plus lieu d'y statuer.<br/>
<br/>
              6. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par Mme B...au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
                               --------------<br/>
<br/>
Article 1er : L'intervention de la Section française de l'observatoire international des prisons est admise.<br/>
Article 2 : Il n'y a pas lieu de statuer sur le pourvoi de MmeB....<br/>
Article 3 : Les conclusions de Mme B...présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
<br/>
Article 4 : La présente décision sera notifiée à Mme A...B...et à la garde des sceaux, ministre de la justice.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-035-02-03-02 PROCÉDURE. PROCÉDURES INSTITUÉES PAR LA LOI DU 30 JUIN 2000. RÉFÉRÉ SUSPENSION (ART. L. 521-1 DU CODE DE JUSTICE ADMINISTRATIVE). CONDITIONS D'OCTROI DE LA SUSPENSION DEMANDÉE. URGENCE. - PRÉSOMPTION - EXISTENCE - MESURE DE PLACEMENT D'UN DÉTENU À L'ISOLEMENT OU DE PROLONGATION DE CETTE MESURE (ART. 726-1 DU CPP) [RJ1].
</SCT>
<ANA ID="9A"> 54-035-02-03-02 Eu égard à son objet et à ses effets sur les conditions de détention, la décision plaçant d'office à l'isolement une personne détenue ainsi que les décisions prolongeant éventuellement un tel placement, prises sur le fondement de l'article 726-1 du code de procédure pénale (CPP), portent en principe, sauf à ce que l'administration pénitentiaire fasse valoir des circonstances particulières, une atteinte grave et immédiate à la situation de la personne détenue, de nature à créer une situation d'urgence justifiant que le juge administratif des référés, saisi sur le fondement de l'article L. 521-1 du code de justice administrative (CJA), puisse ordonner la suspension de leur exécution s'il estime remplie l'autre condition posée par cet article.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Ab. jur., CE, 29 décembre 2004, Garde des sceaux, Ministre de la justice c/,, n° 268826, T. p. 821 ; CE, 1er février 2012,,, n° 350899, T. p. 912.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
