<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041523486</ID>
<ANCIEN_ID>JG_L_2020_01_000000416364</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/52/34/CETATEXT000041523486.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 31/01/2020, 416364</TITRE>
<DATE_DEC>2020-01-31</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>416364</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP L. POULET-ODENT ; SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>Mme Sophie-Caroline de Margerie</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie Roussel</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:416364.20200131</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme C... A... a demandé au tribunal administratif de Marseille d'annuler la délibération du 1er août 2013 par laquelle le conseil municipal de Thorame-Haute (Alpes-de-Haute-Provence) a approuvé la modification simplifiée du plan local d'urbanisme de la commune. <br/>
<br/>
              Par un jugement n° 1305987 du 17 décembre 2015, le tribunal administratif de Marseille a annulé cette délibération. <br/>
<br/>
              Par un arrêt n° 16MA00679 du 12 octobre 2017, la cour administrative d'appel de Marseille a rejeté l'appel formé par la commune de Thorame Haute contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 7 décembre 2017, 21 février 2018 et 21 mai 2019 au secrétariat du contentieux du Conseil d'Etat, la commune de Thorame Haute demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de Mme A... la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'urbanisme ;<br/>
              - la loi n° 85-30 du 9 janvier 1985 ;<br/>
              - la loi n° 2003-590 du 2 juillet 2003 ;<br/>
              - l'ordonnance n° 2012-11 du 5 janvier 2012 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie-Caroline de Margerie, conseiller d'Etat, <br/>
<br/>
              - les conclusions de Mme Sophie Roussel, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP L. Poulet-Odent, avocat de la commune de Thorame-Haute, et à la SCP Piwnica, Molinié, avocat de Mme A... ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1.	Il ressort des pièces du dossier soumis aux juges du fond que, par délibération du 1er août 2013, le conseil municipal de Thorame Haute a approuvé la modification simplifiée n° 2 du plan local d'urbanisme de la commune, résultant d'une délibération du 18 décembre 2008, relative à une zone Nc dédiée à l'exploitation de carrières. Par un jugement du 17 décembre 2015, le tribunal administratif de Marseille a, sur la demande de Mme A..., annulé cette délibération. La commune de Thorame-Haute se pourvoit en cassation contre l'arrêt du 12 octobre 2017 par lequel la cour administrative d'appel de Marseille a rejeté l'appel qu'elle avait formé contre ce jugement. Pour rejeter l'appel de la commune et juger, après le tribunal administratif, illégale la délibération attaquée, la cour administrative d'appel s'est fondée sur quatre motifs d'illégalité tirés, respectivement, du recours à la procédure de modification simplifiée du plan local d'urbanisme et non à celle de révision, de l'absence d'évaluation environnementale, de la méconnaissance des dispositions de l'article L. 145-3 du code de l'urbanisme et de la méconnaissance des dispositions de l'article R. 123-11 du même code. <br/>
<br/>
              2.	S'agissant, en premier lieu, de la mise en oeuvre de la procédure de modification simplifiée, l'article L. 123-13-3 du code de l'urbanisme, dans sa rédaction applicable au litige, dispose que : " I. En dehors des cas mentionnés à l'article L. 123-13-2, et dans le cas des majorations des possibilités de construire prévues au deuxième alinéa de l'article L. 123-1-11 ainsi qu'aux articles L. 127-1, L. 128-1 et L. 128-2, le projet de modification peut, à l'initiative du président de l'établissement public de coopération intercommunale ou, dans le cas prévu au deuxième alinéa de l'article L. 123-6, du maire, être adopté selon une procédure simplifiée. Il en est de même lorsque le projet de modification a uniquement pour objet la rectification d'une erreur matérielle ". <br/>
<br/>
              3.	Il résulte de ces dispositions que le recours à la procédure de modification simplifiée pour la correction d'une erreur matérielle est légalement possible en cas de malfaçon rédactionnelle ou cartographique portant sur l'intitulé, la délimitation ou la règlementation d'une parcelle, d'un secteur ou d'une zone ou le choix d'un zonage, dès lors que cette malfaçon conduit à une contradiction évidente avec les intentions des auteurs du plan local d'urbanisme, telles qu'elles ressortent des différents documents constitutifs du plan local d'urbanisme, comme le rapport de présentation, les orientations d'aménagement ou le projet d'aménagement et de développement durable.<br/>
<br/>
              4.	Il ressort des pièces du dossier soumis aux juges du fond que différents documents constitutifs du plan local d'urbanisme de la commune de Thorame-Haute de 2008, tels que le rapport de présentation qui mentionne à plusieurs reprises l'existence d'une carrière et d'installations de traitements des minéraux, de même que l'importance économique et sociale de la société qui les exploite, et le projet d'aménagement et de développement durable qui retient comme première orientation le maintien des entreprises présentes, attestent, sans aucun doute possible, que la commune n'avait, en aucun cas, entendu remettre en cause ou restreindre les activités liées à l'exploitation des carrières existant dans la zone Nc, y compris les activités qualifiées de " connexes " à cette exploitation, et que l'absence, dans le règlement du plan local d'urbanisme de 2008, de la référence à ces activités procédait d'une simple omission. Par suite, en jugeant que la modification, par la délibération litigieuse, du règlement de cette zone pour y autoriser explicitement, notamment " les équipements, installations et constructions nécessaires à l'exploitation de carrières et aux activités connexes " ainsi que les installations classées soumises à autorisation, ne pouvait pas être assimilée à la rectification d'une erreur matérielle, à laquelle il était loisible pour la commune de procéder en recourant à la procédure de modification simplifiée, la cour administrative d'appel de Marseille a inexactement qualifié les faits de l'espèce.<br/>
<br/>
              5.	S'agissant, en deuxième lieu, de l'absence d'évaluation environnementale, la cour s'est fondée, pour juger que la commune de Thorame-Haute ne pouvait recourir à la procédure de modification simplifiée au lieu de celle de révision du plan local d'urbanisme, sur la circonstance que la modification envisagée était susceptible d'induire de graves risques de nuisance, pour en déduire la nécessité d'une évaluation environnementale. Pour ce faire, la cour s'est toutefois fondée sur les dispositions de l'article L. 123-13 du code de l'urbanisme, dans leur rédaction issue de l'article 23 de la loi du 2 juillet 2003 alors que ces dispositions n'étaient plus en vigueur à la date à laquelle avait été engagée la procédure de modification, par l'effet de l'article 3 de l'ordonnance du 5 janvier 2012. La cour a, dès lors, commis une erreur de droit. Au demeurant, la modification apportée au plan local d'urbanisme, ainsi qu'il a été dit précédemment, entendait rétablir la mention d'activités existantes, antérieures au plan local d'urbanisme adopté en 2008, et n'était, par suite, pas susceptible d'emporter des nuisances supplémentaires.<br/>
<br/>
              6.	S'agissant, en troisième lieu, de l'article R. 123-11 du code de l'urbanisme, cet article, relatif à la délimitation des zones U, AU, A et N sur les documents graphiques, dans sa rédaction applicable au litige, prévoit que ces documents font apparaître s'il y a lieu, " les secteurs protégés en raison de la richesse du sol ou du sous-sol, dans lesquels les constructions et installations nécessaires à la mise en valeur de ces ressources naturelles sont autorisées ".<br/>
<br/>
              7.	Il ressort des pièces du dossier soumis aux juges du fond que la délibération litigieuse autorise, dans le secteur Nc, les équipements, installations et constructions nécessaires à l'exploitation de carrières y compris les " activités connexes (concassage, criblage, production de première transformation [centrale à béton, centrale d'enrobage, etc.]) ". Pour juger que la délibération litigieuse ne pouvait légalement ouvrir la possibilité, dans ce secteur protégé en raison de la richesse de son sol et sous-sol, de réaliser des installations comme les centrales d'enrobage à chaud et les centrales à béton, la cour administrative d'appel de Marseille a relevé que ces installations étaient " connexes et non pas seulement nécessaires à l'exploitation d'une carrière ". En excluant par principe de telles installations de première transformation, alors que les dispositions précitées de l'article R. 123-11 du code de l'urbanisme autorisent les installations " nécessaires à la mise en valeur " des " ressources naturelles " et que la mise en oeuvre de la délibération litigieuse suppose d'apprécier pour chaque projet, sous le contrôle du juge de l'excès de pouvoir, l'existence d'un rapport étroit entre les activités concernées, la cour a entaché son arrêt d'une erreur de droit.<br/>
<br/>
              8.	Mais, en quatrième lieu, aux termes de l'article L. 145-3 du code de l'urbanisme, dans sa rédaction applicable au litige : " (...) / III. - Sous réserve de l'adaptation, du changement de destination, de la réfection ou de l'extension limitée des constructions existantes et de la réalisation d'installations ou d'équipements publics incompatibles avec le voisinage des zones habitées, l'urbanisation doit se réaliser en continuité avec les bourgs, villages, hameaux, groupes de constructions traditionnelles ou d'habitations existants (...) ". Contrairement à ce que soutient le pourvoi, ces dispositions, issues de la loi du 9 janvier 1985 relative au développement et à la protection de la montagne, étaient applicables en l'espèce. La cour n'a donc pas commis d'erreur de droit en en faisant application. Elle n'a pas dénaturé les pièces du dossier en retenant que la modification apportée au plan local d'urbanisme était de nature à permettre une forme d'urbanisation.<br/>
<br/>
              9.	Le motif tiré de la méconnaissance de l'article L. 145-3 du code de l'urbanisme justifie à lui seul le dispositif de l'arrêt attaqué, par lequel la cour administrative d'appel a rejeté l'appel formé par la commune contre le jugement du tribunal administratif qui avait prononcé l'annulation de la délibération du 1er août 2013. Il s'ensuit que la commune de Thorame-Haute n'est pas fondée à en demander l'annulation. <br/>
<br/>
              10.	Les conclusions présentées par la commune au titre des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent, en conséquence, qu'être rejetées. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de Thorame-Haute le versement à Mme A... d'une somme à ce même titre.<br/>
<br/>
<br/>
<br/>
<br/>
        D É C I D E :<br/>
              			---------------<br/>
<br/>
Article 1er : Le pourvoi de la commune de Thorame-Haute est rejeté.<br/>
<br/>
Article 2 : Les conclusions présentées par Mme A... au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 3 : La présente décision sera notifiée à la commune de Thorame-Haute et à Mme B... A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">68-01-01-01-02-02 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PLANS D'AMÉNAGEMENT ET D'URBANISME. PLANS D`OCCUPATION DES SOLS (POS) ET PLANS LOCAUX D'URBANISME (PLU). LÉGALITÉ DES PLANS. MODIFICATION ET RÉVISION DES PLANS. PROCÉDURES DE MODIFICATION. - MODIFICATION SIMPLIFIÉE D'UN PLU POUR LA CORRECTION D'UNE ERREUR MATÉRIELLE (ART. L. 123-13-3 DU CODE DE L'URBANISME) - NOTION - MALFAÇON EN CONTRADICTION ÉVIDENTE AVEC LES INTENTIONS DES AUTEURS DU PLU.
</SCT>
<ANA ID="9A"> 68-01-01-01-02-02 Il résulte de l'article L. 123-13-3 du code de l'urbanisme que le recours à la procédure de modification simplifiée pour la correction d'une erreur matérielle est légalement possible en cas de malfaçon rédactionnelle ou cartographique portant sur l'intitulé, la délimitation ou la règlementation d'une parcelle, d'un secteur ou d'une zone ou le choix d'un zonage, dès lors que cette malfaçon conduit à une contradiction évidente avec les intentions des auteurs du plan local d'urbanisme, telles qu'elles ressortent des différents documents constitutifs du plan local d'urbanisme, comme le rapport de présentation, les orientations d'aménagement ou le projet d'aménagement et de développement durable.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
