<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000023564162</ID>
<ANCIEN_ID>JG_L_2011_02_000000343991</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/23/56/41/CETATEXT000023564162.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème et 9ème sous-sections réunies, 02/02/2011, 343991, Publié au recueil Lebon</TITRE>
<DATE_DEC>2011-02-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>343991</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème et 9ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DE CHAISEMARTIN, COURJON</AVOCATS>
<RAPPORTEUR>Mme Suzanne  von Coester</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Boucher Julien</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2011:343991.20110202</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu, 1° sous le numéro 343991, la requête enregistrée le 26 octobre 2010 au secrétariat du contentieux du Conseil d'Etat, présentée par le HAUT COMMISSAIRE DE LA REPUBLIQUE EN POLYNESIE FRANCAISE ; le haut-commissaire demande au Conseil d'Etat :<br/>
<br/>
              1°) de déclarer la loi du pays n° 2010-16 LP-APF du 5 octobre 2010 relative aux autorisations d'établir et d'exploiter un réseau ouvert au public ou de fourniture au public d'un service de télécommunication non conforme au bloc de légalité tel qu'il est défini au III de l'article 176 de la loi organique du 27 février 2004 portant statut d'autonomie de la Polynésie française ;<br/>
<br/>
              2°) de déclarer que cette loi du pays ne peut être promulguée ;<br/>
<br/>
<br/>
              Vu, 2° sous le numéro 344199, la requête, enregistrée le 5 novembre 2010 au secrétariat du contentieux du Conseil d'Etat, présentée par la SOCIETE DIGICEL TAHITI, dont le siège est situé Passage Cardella, immeuble Angèle Bambridge, BP 41293 à Papeete (98713) ; la société demande au Conseil d'Etat :<br/>
<br/>
              1°) de déclarer la même loi du pays du 5 octobre 2010 non conforme au bloc de légalité tel qu'il est défini au III de l'article 176 de la loi organique du 27 février 2004 portant statut d'autonomie de la Polynésie française ;<br/>
<br/>
              2°) de déclarer que cette loi du pays ne peut être promulguée ;<br/>
<br/>
              3°) de mettre à la charge de la Polynésie française la somme de 15 000 euros sur le fondement des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu la Constitution ;<br/>
<br/>
              Vu le traité sur le fonctionnement de l'Union européenne ;<br/>
<br/>
              Vu la décision du Conseil n° 2001/822/CE du 27 novembre 2001, notamment son article 45 ;<br/>
<br/>
              Vu la loi organique n° 2004-192 du 27 février 2004 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Suzanne von Coester, Maître des Requêtes,<br/>
<br/>
              - les observations de la SCP de Chaisemartin, Courjon, avocat de l'Assemblée de la Polynésie française,<br/>
<br/>
              - les conclusions de M. Julien Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP de Chaisemartin, Courjon, avocat de l'Assemblée de la Polynésie française ;<br/>
<br/>
<br/>
<br/>Considérant, d'une part, qu'en vertu du huitième alinéa de l'article 74 de la Constitution, la loi organique peut déterminer, pour les collectivités d'outre-mer qui sont dotées de l'autonomie, les conditions dans lesquelles "le Conseil d'Etat exerce un contrôle juridictionnel spécifique sur certaines catégories d'actes de l'assemblée délibérante intervenant au titre des compétences qu'elle exerce dans le domaine de la loi" ; qu'aux termes de l'article 139 de la loi organique du 27 février 2004 portant statut d'autonomie de la Polynésie française : " L'assemblée de la Polynésie française adopte des actes prévus à l'article 140 dénommés "lois du pays " et des délibérations" ; que l'article 140 de cette même loi organique dispose que les actes de l'assemblée de la Polynésie française, dénommés " lois du pays ", sur lesquels le Conseil d'Etat exerce un contrôle juridictionnel spécifique, sont ceux qui, relevant du domaine de la loi, soit ressortissent à la compétence de la Polynésie française en application de l'article 13, soit sont pris au titre de la participation de la Polynésie française aux compétences de l'Etat dans les conditions prévues aux articles 31 à 36 ; que, sous réserve des dispositions de l'article 14 de cette loi organique, les mesures relatives à la réglementation des investissements étrangers en Polynésie française font partie de ces actes ;<br/>
<br/>
              Considérant, d'autre part, que l'article 177 de cette même loi organique dispose que si le Conseil d'Etat, saisi sur le fondement de l'article 176 de la même loi, constate qu'un acte prévu à l'article 140 dénommé "loi du pays" contient une disposition contraire à la Constitution, aux lois organiques, aux engagements internationaux ou aux principes généraux du droit et inséparable de l'ensemble de l'acte, celle-ci ne peut être promulguée ; que, dans ce cas, " le président de la Polynésie française peut, dans les dix jours qui suivent la publication de la décision du Conseil d'Etat au Journal officiel de la Polynésie française, soumettre la disposition concernée à une nouvelle lecture de l'assemblée de la Polynésie française, afin d'en assurer la conformité aux normes mentionnées au deuxième alinéa " ;<br/>
<br/>
              Considérant que, sur le fondement de l'article 140 de la loi organique du 27 février 2004, l'assemblée de la Polynésie française a adopté, le 5 octobre 2010, une " loi du pays " relative aux autorisations d'établir et d'exploiter un réseau ouvert au public ou de fourniture au public d'un service de télécommunications prévoyant que le capital, les droits de vote ou les droits à dividende de tout opérateur exploitant un réseau de téléphonie mobile en Polynésie française ne peuvent être détenus directement ou indirectement à plus de 35 % par un ou plusieurs investisseurs étrangers ; que, dans le cadre du contrôle juridictionnel spécifique défini au chapitre II du titre VI de cette même loi organique, le HAUT-COMMISSAIRE DE LA REPUBLIQUE EN POLYNESIE FRANÇAISE et la SOCIETE DIGICEL TAHITI ont saisi le Conseil d'Etat de requêtes tendant à ce que cet acte soit déclaré illégal ; qu'il y a lieu de joindre ces requêtes pour statuer par une seule décision ;<br/>
<br/>
              Sans qu'il soit besoin d'examiner les autres moyens des requêtes ;<br/>
<br/>
              Considérant qu'aux termes du premier alinéa du 2 de l'article 355 du traité sur le fonctionnement de l'Union européenne : " Les pays et territoires d'outre-mer dont la liste figure à l'annexe II font l'objet du régime spécial d'association défini dans la quatrième partie " ; que la Polynésie française figure au nombre de ces pays et territoires d'outre-mer ; que, dans la quatrième partie du traité, l'article 199 dispose que : " L'association poursuit les objectifs ci-après : (...) 5) Dans les relations entre les Etats membres et les pays et territoires, le droit d'établissement des ressortissants et sociétés est réglé conformément aux dispositions et par application des procédures prévues au chapitre relatif au droit d'établissement et sur une base non-discriminatoire, sous réserve des dispositions particulières prises en vertu de l'article 203 " ; qu'ainsi, s'agissant de l'accès aux activités non salariées et de leur exercice, le droit d'établissement et de prestations de services est réglé, au titre des dispositions particulières prises en vertu de l'article 203 du traité, par l'article 45 de la décision n° 2001/822/CE du Conseil du 27 novembre 2001 relative à l'association des pays et territoires d'outre-mer ; qu'aux termes de cet article : " (...) 2. En ce qui concerne le régime applicable en matière d'établissement et de prestation de services (...) et sous réserve du paragraphe 3 ci-après : (...) b) les autorités des PTOM traitent les sociétés, ressortissants et entreprises des Etats membres de manière non moins favorable qu'ils traitent les sociétés, ressortissants et entreprises d'un pays tiers, et ne discriminent pas entre les sociétés, ressortissants et entreprises des Etats membres. " ; qu'en vertu de ces dispositions, les autorités compétentes des pays et territoires d'outre-mer sont tenues de traiter sur une base non discriminatoire les ressortissants et sociétés des autres Etats membres qui exercent ou cherchent à exercer le droit d'établissement ou de libre prestation de services dans ce territoire ;<br/>
<br/>
              Considérant qu'aux termes de l'article LP 1 de la " loi du pays " contestée : " Le capital, les droits de vote ou les droits à dividende de tout opérateur exploitant un réseau de téléphonie mobile en Polynésie française ne peuvent être détenus directement ou indirectement à plus de 35 % par un ou plusieurs investisseurs étrangers. " ; qu'en interdisant à des opérateurs de téléphonie mobile le droit d'exercer leur activité pour un motif tiré de la nationalité, quelle qu'elle soit, des personnes ou sociétés les détenant, l'assemblée de la Polynésie française a introduit une restriction au droit d'établissement constitutive d'une discrimination prohibée par les dispositions précitées de l'article 45 de la décision du 27 novembre 2001 ; que l'article LP 2 est indissociable de l'article LP 1 ; qu'eu égard à la mission impartie au Conseil d'Etat en vertu des dispositions de l'article 177 de la loi organique du 27 février 2004, cette illégalité fait obstacle à la promulgation du texte en cause ;<br/>
<br/>
              Considérant qu'il résulte de ce qui précède que le HAUT-COMMISSAIRE DE LA REPUBLIQUE EN POLYNESIE FRANÇAISE et la SOCIETE DIGICEL TAHITI sont fondés à demander au Conseil d'Etat de déclarer que cette loi du pays est illégale et ne peut être promulguée ;<br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de faire application des dispositions de l'article L. 761-1 du code de justice administrative et de mettre à la charge de la Polynésie française le versement de la somme de 3 000 euros au titre des frais exposés par la SOCIETE DIGICEL TAHITI et non compris dans les dépens ; qu'en revanche, ces dispositions font obstacle à ce qu'il soit fait droit aux conclusions présentées au même titre par l'assemblée de la Polynésie française ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : La " loi du pays " n° 2010-16 LP/APF du 5 octobre 2010 est illégale et ne peut être promulguée.<br/>
Article 2 : La Polynésie française versera à la SOCIETE DIGICEL TAHITI une somme de 3 000 euros en application des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : Les conclusions de l'assemblée de la Polynésie française tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée au HAUT-COMMISSAIRE DE LA REPUBLIQUE EN POLYNESIE FRANÇAISE, à la SOCIETE DIGICEL TAHITI, au président de l'assemblée de la Polynésie française, au président de la Polynésie française et au ministre de l'intérieur, de l'outre-mer, des collectivités territoriales et de l'immigration.<br/>
Copie en sera adressée pour information au Premier ministre et à la ministre de l'économie, des finances et de l'industrie.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">46-01 OUTRE-MER. DROIT APPLICABLE. - APPLICATION DU DROIT DE L'UE À LA POLYNÉSIE FRANÇAISE - TRAITÉ DE LISBONNE - 1) APPLICATION DU PRINCIPE DE NON DISCRIMINATION DES OPÉRATEURS EXERÇANT OU CHERCHANT À EXERCER LE DROIT D'ÉTABLISSEMENT OU LA LIBRE PRESTATION DE SERVICES - EXISTENCE - 2) APPLICATION EN L'ESPÈCE - INTERDICTION À DES OPÉRATEURS DE TÉLÉPHONIE MOBILE DU DROIT D'EXERCER LEUR ACTIVITÉ POUR UN MOTIF TIRÉ DE LEUR NATIONALITÉ - VIOLATION.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">46-01-02-02 OUTRE-MER. DROIT APPLICABLE. STATUTS. POLYNÉSIE FRANÇAISE. - CONSTAT, PAR LE CONSEIL D'ETAT, DE L'EXISTENCE D'UNE DISPOSITION FIGURANT DANS UNE LOI DU PAYS CONTRAIRE À LA CONSTITUTION - DISPOSITION INDISSOCIABLE DU RESTE DU PROJET DE LOI - CONSÉQUENCE - IMPOSSIBILITÉ DE PROMULGUER LA LOI DU PAYS - EXISTENCE.
</SCT>
<ANA ID="9A"> 46-01 1) La Polynésie française figure au nombre des pays et territoires d'outre-mer faisant l'objet du régime spécial d'association défini dans la quatrième partie du Traité sur le fonctionnement de l'Union européenne. Dans cette quatrième partie, l'article 199 dispose que : « L'association poursuit les objectifs ci-après : (&#133;) 5) Dans les relations entre les Etats membres et les pays et territoires, le droit d'établissement des ressortissants et sociétés est réglé conformément aux dispositions et par application des procédures prévues au chapitre relatif au droit d'établissement et sur une base non-discriminatoire, sous réserve des dispositions particulières prises en vertu de l'article 203 ».  S'agissant de l'accès aux activités non salariées et de leur exercice, le droit d'établissement et de prestations de services est réglé, au titre des dispositions particulières prises en vertu de l'article 203 du traité, par l'article 45 de la décision n° 2001/822/CE du Conseil du 27 novembre 2001 relative à l'association des pays et territoires d'outre-mer aux termes duquel : « (&#133;) 2. En ce qui concerne le régime applicable en matière d'établissement et de prestation de services (&#133;) et sous réserve du paragraphe 3 ci-après : (&#133;) b) les autorités des PTOM traitent les sociétés, ressortissants et entreprises des Etats membres de manière non moins favorable qu'ils traitent les sociétés, ressortissants et entreprises d'un pays tiers, et ne discriminent pas entre les sociétés, ressortissants et entreprises des Etats membres. En vertu de ces dispositions, les autorités compétentes des pays et territoires d'outre-mer sont tenues de traiter sur une base non discriminatoire les ressortissants et sociétés des autres Etats membres qui exercent ou cherchent à exercer le droit d'établissement ou de libre prestation de services dans ce territoire.,,2) En interdisant à des opérateurs de téléphonie mobile le droit d'exercer leur activité pour un motif tiré de la nationalité, quelle qu'elle soit, des personnes ou sociétés les détenant, l'assemblée de la Polynésie française a introduit une restriction au droit d'établissement constitutive d'une discrimination prohibée par les dispositions de l'article 45 de la décision du 27 novembre 2001.</ANA>
<ANA ID="9B"> 46-01-02-02 L'article 177 de la loi organique relative à la Polynésie française dispose que si le Conseil d'Etat, saisi sur le fondement de l'article 176 de la même loi, constate qu'un acte prévu à l'article 140 dénommé «loi du pays» contient une disposition contraire à la Constitution, aux lois organiques, aux engagements internationaux ou aux principes généraux du droit et inséparable de l'ensemble de l'acte, celle-ci ne peut être promulguée et que, dans ce cas, « le président de la Polynésie française peut, dans les dix jours qui suivent la publication de la décision du Conseil d'Etat au Journal officiel de la Polynésie française, soumettre la disposition concernée à une nouvelle lecture de l'assemblée de la Polynésie française, afin d'en assurer la conformité aux normes mentionnées au deuxième alinéa .,,En interdisant à des opérateurs de téléphonie mobile le droit d'exercer leur activité pour un motif tiré de la nationalité, quelle qu'elle soit, des personnes ou sociétés les détenant, l'assemblée de la Polynésie française a introduit une restriction au droit d'établissement constitutive d'une discrimination prohibée par le droit communautaire. L'article de loi du pays suivant l'article litigieux étant indissociable de ce dernier, eu égard à la mission impartie au Conseil d'Etat en vertu des dispositions de l'article 177 de la loi organique du 27 février 2004, cette illégalité fait obstacle à la promulgation du texte en cause.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
