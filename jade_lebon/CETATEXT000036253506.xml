<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036253506</ID>
<ANCIEN_ID>JG_L_2017_12_000000408811</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/25/35/CETATEXT000036253506.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Section, 22/12/2017, 408811, Publié au recueil Lebon</TITRE>
<DATE_DEC>2017-12-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>408811</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Section</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>OCCHIPINTI</AVOCATS>
<RAPPORTEUR>Mme Sophie-Caroline de Margerie</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier Domino</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESEC:2017:408811.20171222</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire et un mémoire complémentaire enregistré au secrétariat du contentieux du Conseil d'Etat les 10 mars et 20 avril 2017, M. B...A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret en date du 18 janvier 2017 en ce qu'il a accordé son extradition aux autorités marocaines ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement de la somme de 3 500 euros à Me Occhipinti, son avocat, au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - la convention d'aide mutuelle judiciaire, d'exequatur des jugements et d'extradition entre la France et le Maroc en date du 5 octobre 1957 ; <br/>
              - la convention contre la torture et autres peines ou traitements cruels, inhumains ou dégradants adoptée à New York le 10 décembre 1984 ; <br/>
              - le code pénal ;<br/>
              - le code de procédure pénale ; <br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie-Caroline de Margerie, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Xavier Domino, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Occhipinti, avocat de M. A...;<br/>
<br/>
<br/>
<br/>1. Considérant que les autorités marocaines ont demandé l'extradition de M. B... A..., ressortissant marocain, pour l'exécution d'un mandat d'arrêt décerné le 21 décembre 2009 par le procureur général du roi près la cour d'appel de Rabat, pour des faits de constitution de bande criminelle pour préparer et commettre des actes terroristes dans le cadre d'une entreprise collective visant à porter gravement atteinte à l'ordre public, incitation d'autrui à perpétrer des actes terroristes et prestation d'assistance à auteur d'actes terroristes ; qu'après que la chambre de l'instruction de la cour d'appel de Metz, dont l'avis est exigé par les dispositions des articles 696-15 et 696-17 du code de procédure pénale, eut émis un avis favorable le 25 mars 2010, le Premier ministre a accordé l'extradition demandée par un décret du 11 juillet 2011 ; que le recours pour excès de pouvoir formé par M. A...contre ce décret a été rejeté par une décision du Conseil d'Etat, statuant au contentieux en date du 22 mai 2012 ; que, par un arrêt du 30 mai 2013, la Cour européenne des droits de l'homme a toutefois jugé que la mise à exécution de la décision de renvoyer l'intéressé vers le Maroc emporterait violation de l'article 3 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; qu'ultérieurement, par note verbale du 26 décembre 2016 transmise par voie diplomatique, le gouvernement du Royaume du Maroc a assuré les autorités françaises des droits et garanties dont bénéficierait M. A...quant à son jugement au Maroc et aux conditions de son éventuelle détention ; qu'au vu des assurances ainsi données, le Premier ministre, par le décret attaqué du 18 janvier 2017, a rapporté le précédent décret du 11 juillet 2011 et accordé à nouveau l'extradition de M. A...aux autorités marocaines, sous réserve du respect des garanties données ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article 1er de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales : " Les Hautes Parties contractantes reconnaissent à toute personne relevant de leur juridiction les droits et libertés définis au titre I de la présente Convention " ; que l'article 41 de la même convention stipule que : " Si la Cour déclare qu'il y a eu violation de la Convention ou de ses Protocoles, et si le droit interne de la Haute Partie contractante ne permet d'effacer qu'imparfaitement les conséquences de cette violation, la Cour accorde à la partie lésée, s'il y a lieu, une satisfaction équitable " ; qu'en vertu de l'article 46 de la même convention : " 1. Les Hautes Parties contractantes s'engagent à se conformer aux arrêts définitifs de la Cour dans les litiges auxquels elles sont parties. / 2. L'arrêt définitif de la Cour est transmis au Comité des Ministres qui en surveille l'exécution. (...) " ;<br/>
<br/>
              3. Considérant qu'il résulte des stipulations de l'article 46 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales que la complète exécution d'un arrêt de la Cour européenne des droits de l'homme condamnant un Etat partie à la convention implique, en principe, que cet Etat prenne toutes les mesures qu'appellent, d'une part, la réparation des conséquences que la violation de la convention a entraînées pour le requérant et, d'autre part, la disparition de la source de cette violation ; qu'eu égard à la nature essentiellement déclaratoire des arrêts de la Cour, il appartient à l'Etat condamné de déterminer les moyens de s'acquitter de l'obligation qui lui incombe ainsi ; que l'autorité qui s'attache aux arrêts de la Cour implique en conséquence non seulement que l'Etat verse à l'intéressé les sommes que lui a, le cas échéant, allouées la Cour au titre de la satisfaction équitable prévue par l'article 41 de la convention mais aussi qu'il adopte les mesures individuelles et, le cas échéant, générales nécessaires pour mettre un terme à la violation constatée ;<br/>
<br/>
              4. Considérant que lorsque la Cour européenne des droits de l'homme a constaté par un arrêt que la mise à exécution d'un décret accordant l'extradition d'une personne à l'Etat qui la réclame emporterait violation de l'une des stipulations de la convention, l'exécution de cet arrêt implique qu'il ne puisse être procédé à l'extradition de la personne sur le fondement de ce décret ; que si un tel arrêt de la Cour ne fait pas obstacle à ce que soit ultérieurement reprise une décision d'extradition à l'égard de la personne réclamée, au vu d'éléments nouveaux de nature à satisfaire aux exigences de la convention et, en particulier, de garanties apportées par l'Etat requérant, une telle décision doit alors prendre la forme d'un nouveau décret et suppose que la chambre de l'instruction, préalablement saisie de ces éléments nouveaux, ait été consultée à nouveau et n'ait pas repoussé la demande d'extradition ; <br/>
<br/>
              5. Considérant que si, après l'arrêt du 30 mai 2013 de la Cour européenne des droits de l'homme, les autorités françaises ont reçu des autorités marocaines des assurances sur les droits et garanties dont bénéficierait M. A...quant à son jugement au Maroc et aux conditions de son éventuelle détention et si un nouveau décret a été pris sous réserve du respect de ces garanties, il est constant que la chambre de l'instruction n'a pas été saisie de ces éléments nouveaux et n'a pas été invitée à rendre un nouvel avis sur la demande d'extradition ; que, dans ces conditions et sans qu'il soit besoin d'examiner l'autre moyen de la requête, M. A...est fondé à soutenir que le décret du 18 janvier 2017, en ce qu'il a accordé son extradition aux autorités marocaines, a été pris sur une procédure irrégulière l'ayant privé d'une garantie et qu'il est, par suite, entaché d'excès de pouvoir ;<br/>
<br/>
              6. Considérant que M. A...a obtenu le bénéfice de l'aide juridictionnelle ; que son avocat peut, dès lors, se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ; qu'il y a lieu, dans les circonstances de l'espèce, sous réserve que Me Occhipinti, avocat de M.A..., renonce à percevoir la somme correspondante à la part contributive de l'Etat, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à cet avocat ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'article 2 du décret du 18 janvier 2017 accordant l'extradition de M. B...A...aux autorités marocaines est annulé.<br/>
Article 2 : L'Etat versera à Me Occhipinti, avocat de M.A..., une somme de 3 000 euros en application du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991, sous réserve que cet avocat renonce à percevoir la somme correspondante à la part contributive de l'Etat.<br/>
Article 3 : La présente décision sera notifiée à M. B...A...et à la garde des sceaux, ministre de la justice.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">26-055 DROITS CIVILS ET INDIVIDUELS. CONVENTION EUROPÉENNE DES DROITS DE L'HOMME. - PORTÉE DES ARRÊTS DE LA COUR EDH [RJ1] - MODALITÉS D'EXÉCUTION D'UN ARRÊT CONSTATANT QUE L'EXÉCUTION D'UN DÉCRET D'EXTRADITION EMPORTERAIT VIOLATION DE LA CONVENTION - POSSIBILITÉ D'EXTRADER LA PERSONNE SUR LE FONDEMENT DE CE DÉCRET - ABSENCE - POSSIBILITÉ, SOUS LA FORME D'UN NOUVEAU DÉCRET, D'EXTRADER LA PERSONNE AU VU D'ÉLÉMENTS NOUVEAUX DE NATURE À SATISFAIRE AUX EXIGENCES DE LA CONVENTION - EXISTENCE, SOUS RÉSERVE D'UNE NOUVELLE CONSULTATION ET DE L'AVIS FAVORABLE DE LA CHAMBRE DE L'INSTRUCTION DE LA COUR D'APPEL COMPÉTENTE [RJ2].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">335-04-03-01 ÉTRANGERS. EXTRADITION. DÉCRET D'EXTRADITION. LÉGALITÉ EXTERNE. - DÉCRET D'EXTRADITION DONT LA COUR EDH A CONSTATÉ QUE LA MISE À EXÉCUTION EMPORTERAIT VIOLATION DE LA CONVENTION - PORTÉE D'UN TEL ARRÊT [RJ1] - POSSIBILITÉ D'EXTRADER LA PERSONNE SUR LE FONDEMENT DU DÉCRET - ABSENCE - POSSIBILITÉ, SOUS LA FORME D'UN NOUVEAU DÉCRET, D'EXTRADER LA PERSONNE AU VU D'ÉLÉMENTS NOUVEAUX DE NATURE À SATISFAIRE AUX EXIGENCES DE LA CONVENTION - EXISTENCE, SOUS RÉSERVE D'UNE NOUVELLE CONSULTATION ET DE L'AVIS FAVORABLE DE LA CHAMBRE DE L'INSTRUCTION DE LA COUR D'APPEL COMPÉTENTE [RJ2].
</SCT>
<ANA ID="9A"> 26-055 Lorsque la Cour européenne des droits de l'homme a constaté par un arrêt que la mise à exécution d'un décret accordant l'extradition d'une personne à l'Etat qui la réclame emporterait violation de l'une des stipulations de la convention, l'exécution de cet arrêt implique qu'il ne puisse être procédé à l'extradition de la personne sur le fondement de ce décret. Si un tel arrêt de la Cour ne fait pas obstacle à ce que soit ultérieurement reprise une décision d'extradition à l'égard de la personne réclamée, au vu d'éléments nouveaux de nature à satisfaire aux exigences de la convention et, en particulier, de garanties apportées par l'Etat requérant, une telle décision doit alors prendre la forme d'un nouveau décret et suppose que la chambre de l'instruction, préalablement saisie de ces éléments nouveaux, ait été consultée à nouveau et n'ait pas repoussé la demande d'extradition.</ANA>
<ANA ID="9B"> 335-04-03-01 Lorsque la Cour européenne des droits de l'homme a constaté par un arrêt que la mise à exécution d'un décret accordant l'extradition d'une personne à l'Etat qui la réclame emporterait violation de l'une des stipulations de la convention, l'exécution de cet arrêt implique qu'il ne puisse être procédé à l'extradition de la personne sur le fondement de ce décret. Si un tel arrêt de la Cour ne fait pas obstacle à ce que soit ultérieurement reprise une décision d'extradition à l'égard de la personne réclamée, au vu d'éléments nouveaux de nature à satisfaire aux exigences de la convention et, en particulier, de garanties apportées par l'Etat requérant, une telle décision doit alors prendre la forme d'un nouveau décret et suppose que la chambre de l'instruction, préalablement saisie de ces éléments nouveaux, ait été consultée à nouveau et n'ait pas repoussé la demande d'extradition.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Section, 4 octobre 2012, M. X..., n° 328502, p. 347 ; CE, Assemblée, 30 juillet 2014, M. Z..., n° 358564, p. 260;,,[RJ2] Cf. CE, Assemblée, 18 novembre 1955, Pétalas, n° 36608, p. 548. Rappr. Cass. crim., 20 décembre 1988, n° 88-84728, Bull. crim. 1988 n° 439.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
