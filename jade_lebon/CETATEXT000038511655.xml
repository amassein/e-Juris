<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038511655</ID>
<ANCIEN_ID>JG_L_2019_05_000000424906</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/51/16/CETATEXT000038511655.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 22/05/2019, 424906</TITRE>
<DATE_DEC>2019-05-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>424906</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>CABINET BRIARD</AVOCATS>
<RAPPORTEUR>Mme Cécile Renault</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:424906.20190522</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 16 octobre 2018 et 1er avril 2019 au secrétariat du contentieux du Conseil d'Etat, la Fédération experts-comptables et commissaires aux comptes de France demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2018-857 du 8 octobre 2018 prorogeant les mandats des élus des conseils régionaux des commissaires aux comptes et du Conseil national des commissaires aux comptes ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code du commerce ;<br/>
              - le décret n° 2018-857 du 8 octobre 2018 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Cécile Renault, auditrice,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, au cabinet Briard, avocat de la Fédération experts-comptables et commissaires aux comptes de France ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article 1er du décret du 8 octobre 2018 : " Les mandats, en cours au jour de l'entrée en vigueur du présent décret, des commissaires aux comptes élus en application des articles R. 821-37, R. 821-39, R. 821-40, R. 821-54, R. 821-55, R. 821-58 du code de commerce, et des commissaires aux comptes désignés en application de l'article R. 821-38 de ce même code sont prorogés pour une période de dix-huit mois ". <br/>
<br/>
              2. Aux termes de l'article L. 821-6 du code de commerce : " Il est institué auprès du garde des sceaux, ministre de la justice une Compagnie nationale des commissaires aux comptes, établissement d'utilité publique doté de la personnalité morale, chargée de représenter la profession de commissaire aux comptes auprès des pouvoirs publics./Elle concourt au bon exercice de la profession, à sa surveillance ainsi qu'à la défense de l'honneur et de l'indépendance de ses membres./ Il est institué une compagnie régionale des commissaires aux comptes, dotée de la personnalité morale, par ressort de cour d'appel. Toutefois, le garde des sceaux, ministre de la justice, peut procéder à des regroupements, sur proposition de la compagnie nationale et après consultation, par cette dernière, des compagnies régionales intéressées (...) ". Aux termes de l'article R. 821-37 du même code : " Le conseil national est composé de commissaires aux comptes délégués par les compagnies régionales./ Les délégués sont élus dans son sein par le conseil régional, au scrutin secret, pour une durée de quatre ans, à raison d'un délégué par deux cents membres, personnes physiques ou fraction de deux cents membres, personnes physiques, sans pouvoir excéder quinze élus./ Sont seules éligibles les personnes physiques à jour de leurs cotisations professionnelles, exerçant des fonctions de commissaire aux comptes à la date du scrutin. Le Conseil national est renouvelé par moitié tous les deux ans ". Aux termes de l'article R. 821-54 du même code : " Les membres du conseil régional sont élus au scrutin secret, pour une durée de quatre ans. / Le conseil régional est renouvelé par moitié tous les deux ans. / Sont électeurs les personnes physiques membres de la compagnie régionale. / Sont éligibles les personnes physiques, à jour de leurs cotisations professionnelles, exerçant des fonctions de commissaire aux comptes à la date du scrutin ".<br/>
<br/>
              3. En premier lieu, le moyen tiré de ce que le décret attaqué aurait dû faire l'objet d'une consultation préalable des organisations syndicales concernées n'est pas assorti des précisions permettant d'en apprécier le bien-fondé.  <br/>
<br/>
              4. En deuxième lieu, les moyens tirés de ce que la prorogation des mandats des membres élus des conseils régionaux des commissaires aux comptes et du Conseil national des commissaires aux comptes méconnaîtrait le principe constitutionnel de périodicité raisonnable du suffrage est inopérant, un tel principe régissant les élections à caractère politique alors que les élections en cause présentent un caractère professionnel.<br/>
<br/>
              5. En troisième lieu, si le décret attaqué crée bien une différence de traitement entre les membres dont le mandat, prorogé de dix-huit mois, aura une durée de cinq ans et demi et ceux dont le mandat aura une durée de quatre ans, et aura pour effet que des élus aux mandats de durées différentes seront appelés à siéger en commun, en raison du renouvellement par moitié tous les deux ans de ces instances, ces élus ne peuvent pas, cependant, être regardés comme étant placés dans la même situation puisqu'ils ne seront pas élus lors du même scrutin. Cette prorogation a visé à éviter l'organisation d'élections au moment même où un projet de loi prévoyant un relèvement significatif des seuils de certification des comptes et impliquant une modification de grande ampleur du rôle et de l'organisation de la profession allait être débattu au Parlement, ce projet étant susceptible d'impliquer à court terme d'importants regroupements de conseils régionaux. Le décret contesté repose ainsi sur un motif d'intérêt général. La différence de traitement entre élus qui en résulte, en rapport avec l'objet de la norme, n'est pas manifestement disproportionnée au regard de la différence de situation susceptible de la justifier. Dès lors, le moyen tiré de la méconnaissance du principe d'égalité ne peut qu'être écarté.<br/>
<br/>
              6. En quatrième lieu, la fédération requérante ne saurait déduire des termes d'un discours de la garde des sceaux, prononcé le 12 juillet 2018, se bornant à présenter les raisons justifiant le report des élections, que le décret attaqué serait entaché d'erreur de droit et de détournement de pouvoir. <br/>
<br/>
              7. Il résulte de tout ce qui précède que la requête de la Fédération experts-comptables et commissaires aux comptes de France doit être rejetée. Les dispositions de l'article L. 761-1 du code de justice administrative font par suite obstacle à ce qu'une somme soit mise à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la Fédération experts-comptables et commissaires aux comptes de France est rejetée.<br/>
Article 2 : La présente décision sera notifiée à la Fédération experts-comptables et commissaires aux comptes de France, à la garde des sceaux, ministre de la justice, et au Premier ministre. <br/>
Copie en sera adressée au Conseil national des commissaires aux comptes.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">28-06 ÉLECTIONS ET RÉFÉRENDUM. ÉLECTIONS PROFESSIONNELLES. - APPLICABILITÉ DU PRINCIPE PRINCIPE CONSTITUTIONNEL DE PÉRIODICITÉ RAISONNABLE DU SUFFRAGE AUX ÉLECTIONS PROFESSIONNELLES - ABSENCE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-07-01-04-03 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. QUESTIONS GÉNÉRALES. MOYENS. MOYENS INOPÉRANTS. - MOYEN SOULEVÉ À L'APPUI D'UN RECOURS CONTRE UN DÉCRET PROROGEANT LES MANDATS DES ÉLUS DES CONSEILS RÉGIONAUX DES COMMISSAIRES AUX COMPTES ET DU CONSEIL NATIONAL DES COMMISSAIRES AUX COMPTES ET TIRÉ DE LA MÉCONNAISSANCE DU PRINCIPE CONSTITUTIONNEL DE PÉRIODICITÉ RAISONNABLE DU SUFFRAGE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">55-015 PROFESSIONS, CHARGES ET OFFICES. INSTANCES D`ORGANISATION DES PROFESSIONS AUTRES QUE LES ORDRES. - MOYEN SOULEVÉ À L'APPUI D'UN RECOURS DIRIGÉ CONTRE UN DÉCRET PROROGEANT LES MANDATS DES ÉLUS DES CONSEILS RÉGIONAUX DES COMMISSAIRES AUX COMPTES ET DU CONSEIL NATIONAL DES COMMISSAIRES AUX COMPTES ET TIRÉ DE LA MÉCONNAISSANCE DU PRINCIPE CONSTITUTIONNEL DE PÉRIODICITÉ RAISONNABLE DU SUFFRAGE - MOYENS INOPÉRANT.
</SCT>
<ANA ID="9A"> 28-06 Requérants contestant le décret prorogeant les mandats des élus des conseils régionaux des commissaires aux comptes et du Conseil national des commissaires aux comptes.,,Le moyen tiré de ce que la prorogation des mandats des membres élus des conseils régionaux des commissaires aux comptes et du Conseil national des commissaires aux comptes méconnaîtrait le principe constitutionnel de périodicité raisonnable du suffrage est inopérant, un tel principe régissant les élections à caractère politique alors que les élections en cause présentent un caractère professionnel.</ANA>
<ANA ID="9B"> 54-07-01-04-03 Requérants contestant le décret prorogeant les mandats des élus des conseils régionaux des commissaires aux comptes et du Conseil national des commissaires aux comptes.,,Le moyen tiré de ce que la prorogation des mandats des membres élus des conseils régionaux des commissaires aux comptes et du Conseil national des commissaires aux comptes méconnaîtrait le principe constitutionnel de périodicité raisonnable du suffrage est inopérant, un tel principe régissant les élections à caractère politique alors que les élections en cause présentent un caractère professionnel.</ANA>
<ANA ID="9C"> 55-015 Requérants contestant le décret prorogeant les mandats des élus des conseils régionaux des commissaires aux comptes et du Conseil national des commissaires aux comptes.,,Le moyen tiré de ce que la prorogation des mandats des membres élus des conseils régionaux des commissaires aux comptes et du Conseil national des commissaires aux comptes méconnaîtrait le principe constitutionnel de périodicité raisonnable du suffrage est inopérant, un tel principe régissant les élections à caractère politique alors que les élections en cause présentent un caractère professionnel.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., s'agissant de l'inapplicabilité du principe d'égalité devant le suffrage garanti par l'article 3 de la Constitution aux élections des membres du Conseil national des barreaux, CE, 18 décembre 1996, Toucas, n° 178957, p. 496.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
