<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029003691</ID>
<ANCIEN_ID>JG_L_2014_05_000000369456</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/00/36/CETATEXT000029003691.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème SSJS, 28/05/2014, 369456</TITRE>
<DATE_DEC>2014-05-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>369456</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>LE PRADO ; SCP FABIANI, LUC-THALER</AVOCATS>
<RAPPORTEUR>M. Gérald Bégranger</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:369456.20140528</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 18 juin et 18 septembre 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. et Mme B...C..., demeurant..., quartier des Espargades à Rians (83560) ; M. et Mme B...C...demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'arrêt n° 11MA01642 du 18 avril 2013 par lequel la cour administrative d'appel de Marseille a rejeté leur appel contre l'ordonnance n° 0900436 du 7 avril 2011 du président du tribunal administratif de Toulon rejetant leur demande tendant à l'annulation du permis de construire tacite dont M. et Mme A...sont titulaires depuis le 7 octobre 2008, ainsi que de la décision du maire de Rians du 29 décembre 2008 rejetant leur recours gracieux contre ce permis ;    <br/>
<br/>
              2°) de mettre à la charge de la commune de Rians la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
     	Vu le code de l'urbanisme ; <br/>
	Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Gérald Bégranger, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Le Prado, avocat de M. et Mme B...C...et à la SCP Fabiani, Luc-Thaler, avocat de la commune de Rians ;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que le maire de Rians a délivré tacitement à M. et Mme A...un permis de construire modificatif en vue de réaliser un " abri de piscine " et une clôture de mise en sécurité du bassin ; que, par une ordonnance du 7 avril 2011, le président du tribunal administratif de Toulon a rejeté la demande de M. et Mme B...C...tendant à l'annulation de ce permis, ainsi que de la décision du 29 décembre 2008 par laquelle le maire avait rejeté leur recours gracieux ; que, par un arrêt du 18 avril 2013, contre lequel M. et Mme C...se pourvoient en cassation, la cour administrative d'appel de Marseille a rejeté comme irrecevable, en application des dispositions de l'article R. 600-1 du code de l'urbanisme, l'appel qu'ils avaient formé contre l'ordonnance du 7 avril 2011 ;  <br/>
<br/>
              2. Considérant qu'aux termes de l'article R. 600-1 du code de l'urbanisme : " En cas de déféré du préfet ou de recours contentieux à l'encontre d'un (...) permis de construire (...), le préfet ou l'auteur du recours est tenu, à peine d'irrecevabilité, de notifier son recours à l'auteur de la décision et au titulaire de l'autorisation. Cette notification doit également être effectuée dans les mêmes conditions en cas de demande tendant à l'annulation ou à la réformation d'une décision juridictionnelle concernant (...) un permis de construire (...). L'auteur d'un recours administratif est également tenu de le notifier à peine d'irrecevabilité du recours contentieux qu'il pourrait intenter ultérieurement en cas de rejet du recours administratif (...) " ; qu'aux termes de l'article R. 424-15 du même code : " Mention du permis explicite ou tacite (...) doit être affichée sur le terrain, de manière visible de l'extérieur, par les soins de son bénéficiaire, dès la notification de l'arrêté ou dès la date à laquelle le permis tacite (...) est acquis et pendant toute la durée du chantier (...) / Cet affichage mentionne également l'obligation, prévue à peine d'irrecevabilité par l'article R. 600-1, de notifier tout recours administratif ou contentieux à l'auteur de la décision et au bénéficiaire du permis (...) " ;  <br/>
<br/>
              3. Considérant qu'il résulte de la combinaison de ces dispositions que l'irrecevabilité tirée de l'absence d'accomplissement des formalités de notification prescrites par l'article R. 600-1 du code de l'urbanisme ne peut être opposée, en première instance, en appel ou en cassation, qu'à la condition, prévue à l'article R. 424-15 du même code, que l'obligation de procéder à cette notification ait été mentionnée dans l'affichage du permis de construire ; qu'ainsi, en jugeant que l'obligation de notifier aux intimés une requête d'appel dirigée contre un jugement rejetant une demande d'annulation d'un permis de construire s'impose à peine d'irrecevabilité de cette requête alors même que le permis litigieux n'a pas été affiché sur le terrain, et en rejetant en conséquence comme irrecevable la requête de M. et MmeB..., la cour administrative d'appel de Marseille a commis une erreur de droit ; <br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de Rians la somme de 3 000 euros à verser aux requérants au titre des dispositions de l'article L. 761-1 du code de justice administrative ; que ces dispositions font obstacle à ce qu'une somme soit mise à la charge de M. et Mme B...C...qui ne sont pas, dans la présente instance, la partie perdante ;  <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 18 avril 2013 de la cour administrative d'appel de Marseille est annulé.<br/>
<br/>
		Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Marseille.<br/>
<br/>
Article 3 : La commune de Rians versera à M. et Mme B...C...la somme 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : Les conclusions présentées par la commune de Rians au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 5 : La présente décision sera notifiée à M. et Mme B...C..., à la commune de Rians et à M. et MmeA....<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">68-06-01-04 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. INTRODUCTION DE L'INSTANCE. OBLIGATION DE NOTIFICATION DU RECOURS. - IRRECEVABILITÉ LIÉE À L'ABSENCE DE NOTIFICATION - CONDITION - MENTION DE L'OBLIGATION DE NOTIFICATION SUR L'AFFICHAGE DU PERMIS - CHAMP - INCLUSION - REQUÊTES D'APPEL ET POURVOIS EN CASSATION [RJ1].
</SCT>
<ANA ID="9A"> 68-06-01-04 La règle selon laquelle l'irrecevabilité tirée de l'absence d'accomplissement des formalités de notification prescrites par l'article R. 600-1 du code de l'urbanisme ne peut être opposée qu'à la condition, prévue à l'article R. 424-15 du même code, que l'affichage du permis de construire ait fait mention de cette obligation, est applicable non seulement en première instance, mais également en appel et en cassation.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, avis, 19 novembre 2008, Société Sahelac et Mme Juventin, n° 317279, p. 429 ; CE, 17 février 2012, SCI 14 rue Bosquet, n° 337567, p. 52 ; CE, 10 avril 2013, Mme Bondot, n° 352870, inédite au Recueil.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
