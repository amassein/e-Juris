<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042120845</ID>
<ANCIEN_ID>JG_L_2020_07_000000440055</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/12/08/CETATEXT000042120845.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 4ème chambres réunies, 15/07/2020, 440055</TITRE>
<DATE_DEC>2020-07-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>440055</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 4ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Arnaud Skzryerbak</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Villette</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:440055.20200715</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. E... D... a demandé au tribunal administratif de Caen d'annuler les opérations électorales qui se sont déroulées le 15 mars 2020 pour l'élection des conseillers municipaux et communautaires dans la commune de Saint-Sulpice-sur-Risle (Orne).<br/>
<br/>
              Par une ordonnance n° 2000642 du 25 mars 2020, le président de la 1ère chambre du tribunal administratif de Caen a rejeté sa protestation.<br/>
<br/>
              Par une requête, enregistrée le 10 avril 2020 au secrétariat du contentieux du Conseil d'Etat, M. B... D... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) d'annuler ces opérations électorales.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code électoral ;<br/>
              - le code de procédure civile ;<br/>
              - la loi n° 2020-290 du 23 mars 2020 ;<br/>
              - l'ordonnance n° 2020-305 du 25 mars 2020 ;<br/>
              - le décret n° 2020-571 du 14 mai 2020 ;<br/>
              - la décision n° 2020-849 QPC du 17 juin 2020 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Arnaud Skzryerbak, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Vincent Villette, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. A l'issue des opérations électorales qui se sont déroulées le 15 mars 2020 dans la commune de Saint-Sulpice-sur-Risle, dans l'Orne, les 19 sièges de conseillers municipaux et les 3 sièges de conseillers communautaires ont été pourvus. 17 des sièges de conseillers municipaux et tous les sièges de conseillers communautaires ont été attribués à des candidats de la liste " Tous unis pour Saint Sulpice " conduite par M. C..., qui a obtenu 70,72 % des suffrages exprimés, tandis que les deux autres sièges de conseillers municipaux ont été attribués à des candidats de la liste " Préparons l'avenir ", conduite par M. B... D..., qui a obtenu 29,27 % des suffrages exprimés. M. B... D... relève appel de l'ordonnance du 25 mars 2020 par laquelle le président de la 1ère chambre du tribunal administratif de Caen a rejeté comme tardive la protestation qu'il a formée contre ces opérations électorales.<br/>
<br/>
              Sur la tardiveté de la protestation :<br/>
<br/>
              2. D'une part, aux termes de l'article R. 119 du code électoral : " Les réclamations contre les opérations électorales doivent être consignées au procès-verbal, sinon être déposées, à peine d'irrecevabilité, au plus tard à dix-huit heures le cinquième jour qui suit l'élection, à la sous-préfecture ou à la préfecture. Elles sont immédiatement adressées au préfet qui les fait enregistrer au greffe du tribunal administratif. / Les protestations peuvent également être déposées directement au greffe du tribunal administratif dans le même délai (...) ". <br/>
<br/>
              3. D'autre part, l'article 11 de la loi du 23 mars 2020 d'urgence pour faire face à l'épidémie de covid-19 a habilité le Gouvernement, dans les conditions prévues à l'article 38 de la Constitution, à prendre par ordonnances " toute mesure, pouvant entrer en vigueur, si nécessaire, à compter du 12 mars 2020, relevant du domaine de la loi (...) 2° (...) b) Adaptant, interrompant, suspendant ou reportant le terme des délais prévus à peine de nullité, caducité, forclusion, prescription, inopposabilité, déchéance d'un droit, fin d'un agrément ou d'une autorisation ou cessation d'une mesure, à l'exception des mesures privatives de liberté et des sanctions. Ces mesures sont rendues applicables à compter du 12 mars 2020 (...) ". Sur le fondement de ces dispositions, le 3° du II de l'article 15 de l'ordonnance du 25 mars 2020 portant adaptation des règles applicables devant les juridictions de l'ordre administratif a prévu que : " Les réclamations et les recours mentionnées à l'article R. 119 du code électoral peuvent être formées contre les opérations électorales du premier tour des élections municipales organisé le 15 mars 2020 au plus tard à dix-huit heures le cinquième jour qui suit la date de prise de fonction des conseillers municipaux et communautaires élus dès ce tour, fixée par décret au plus tard au mois de juin 2020 dans les conditions définies au premier alinéa du III de l'article 19 de la loi n° 2020-290 du 23 mars 2020 susvisée ou, par dérogation, aux dates prévues au deuxième ou troisième alinéa du même III du même article ". L'article 1er du décret du 14 mai 2020 définissant la date d'entrée en fonction des conseillers municipaux et communautaires élus dans les communes dont le conseil municipal a été entièrement renouvelé dès le premier tour des élections municipales et communautaires organisé le 15 mars 2020 prévoit que : " (...) les conseillers municipaux et communautaires élus dans les communes dans lesquelles le conseil municipal a été élu au complet lors du scrutin organisé le 15 mars 2020 entrent en fonction le 18 mai 2020 ".<br/>
<br/>
              4. Il résulte de l'ensemble de ces dispositions, combinées avec celles du second alinéa de l'article 642 du code de procédure civile selon lesquelles " Le délai qui expirerait normalement un samedi, un dimanche ou un jour férié ou chômé est prorogé jusqu'au premier jour ouvrable suivant ", que les réclamations contre les opérations électorales qui se sont déroulées le 15 mars 2020 pouvaient être formées au plus tard le lundi 25 mai 2020 à dix-huit heures.<br/>
<br/>
              5. Il résulte de l'instruction que la protestation de M. B... D... a été enregistrée le 22 mars 2020 au greffe du tribunal administratif de Caen, soit après l'expiration du délai normalement imparti par l'article R. 119 du code électoral mais avant le terme du délai indiqué au point 4, découlant de l'application des dispositions du 3° du II de l'article 15 de l'ordonnance du 25 mars 2020. Ces dernières dispositions devant être regardées comme ayant relevé de la forclusion encourue les protestations enregistrées entre l'expiration du délai normalement imparti et leur entrée en vigueur le 27 mars 2020, ainsi qu'elles pouvaient le faire sur le fondement du 2° du I de l'article 11 de la loi du 23 mars 2020, il y a lieu d'annuler l'ordonnance attaquée, d'évoquer et de statuer immédiatement sur la protestation de M. B... D....<br/>
<br/>
              Sur les opérations électorales :<br/>
<br/>
              6. L'émergence d'un nouveau coronavirus, responsable de la maladie à coronavirus 2019 ou covid-19 et particulièrement contagieux, a été qualifiée d'urgence de santé publique de portée internationale par l'Organisation mondiale de la santé le 30 janvier 2020, puis de pandémie le 11 mars 2020. La propagation du virus sur le territoire français a conduit le ministre des solidarités et de la santé à prendre, à compter du 4 mars 2020, des mesures de plus en plus strictes destinées à réduire les risques de contagion. Dans ce contexte, le Premier ministre a adressé à l'ensemble des maires le 7 mars 2020 une lettre présentant les mesures destinées à assurer le bon déroulement des élections municipales et communautaires prévues les 15 et 22 mars 2020. Ces mesures ont été précisées par une circulaire du ministre de l'intérieur du 9 mars 2020 relative à l'organisation des élections municipales des 15 et 22 mars 2020 en situation d'épidémie de coronavirus covid-19, formulant des recommandations relatives à l'aménagement des bureaux de vote et au respect des consignes sanitaires, et par une instruction de ce ministre, du même jour, destinée à faciliter l'exercice du droit de vote par procuration. Après consultation par le Gouvernement du conseil scientifique mis en place pour lui donner les informations scientifiques utiles à l'adoption des mesures nécessaires pour faire face à l'épidémie de covid-19, les 12 et 14 mars 2020, le premier tour des élections municipales a eu lieu comme prévu le 15 mars 2020. A l'issue du scrutin, les conseils municipaux ont été intégralement renouvelés dans 30 143 communes ou secteurs. Le taux d'abstention a atteint 55,34 % des inscrits, contre 36,45 % au premier tour des élections municipales de 2014.<br/>
<br/>
              7. Au vu de la situation sanitaire, l'article 19 de la loi du 23 mars 2020 d'urgence pour faire face à l'épidémie de covid-19 a reporté le second tour des élections, initialement fixé au 22 mars 2020, au plus tard en juin 2020 et prévu que : " Dans tous les cas, l'élection régulière des conseillers municipaux et communautaires, des conseillers d'arrondissement, des conseillers de Paris et des conseillers métropolitains de Lyon élus dès le premier tour organisé le 15 mars 2020 reste acquise, conformément à l'article 3 de la Constitution ". Ainsi que le Conseil constitutionnel l'a jugé dans sa décision n° 2020-849 QPC du 17 juin 2020, ces dispositions n'ont ni pour objet ni pour effet de valider rétroactivement les opérations électorales du premier tour ayant donné lieu à l'attribution de sièges et ne font ainsi pas obstacle à ce que ces opérations soient contestées devant le juge de l'élection.<br/>
<br/>
              8. Aux termes de l'article L. 262 du code électoral, applicable aux communes de mille habitants et plus : " Au premier tour de scrutin, il est attribué à la liste qui a recueilli la majorité absolue des suffrages exprimés un nombre de sièges égal à la moitié du nombre des sièges à pourvoir, arrondi, le cas échéant, à l'entier supérieur lorsqu'il y a plus de quatre sièges à pourvoir et à l'entier inférieur lorsqu'il y a moins de quatre sièges à pourvoir. Cette attribution opérée, les autres sièges sont répartis entre toutes les listes à la représentation proportionnelle suivant la règle de la plus forte moyenne, sous réserve de l'application des dispositions du troisième alinéa ci-après. / Si aucune liste n'a recueilli la majorité absolue des suffrages exprimés au premier tour, il est procédé à un deuxième tour (...) ". Aux termes de l'article L. 273-8 du code électoral : " Les sièges de conseiller communautaire sont répartis entre les listes par application aux suffrages exprimés lors de cette élection des règles prévues à l'article L. 262. (...) ".<br/>
<br/>
              9. Ni par ces dispositions, ni par celles de la loi du 23 mars 2020 le législateur n'a subordonné à un taux de participation minimal la répartition des sièges au conseil municipal à l'issue du premier tour de scrutin dans les communes de mille habitants et plus, lorsqu'une liste a recueilli la majorité absolue des suffrages exprimés. Le niveau de l'abstention n'est ainsi, par lui-même, pas de nature à remettre en cause les résultats du scrutin, s'il n'a pas altéré, dans les circonstances de l'espèce, sa sincérité.<br/>
<br/>
              10. En l'espèce, M. B... D... fait seulement valoir que le taux d'abstention s'est élevé à 56,07 % dans la commune, sans invoquer aucune autre circonstance relative au déroulement de la campagne électorale ou du scrutin dans la commune qui montrerait, en particulier, qu'il aurait été porté atteinte au libre exercice du droit de vote ou à l'égalité entre les candidats. Dans ces conditions, le niveau de l'abstention constatée ne peut être regardé comme ayant altéré la sincérité du scrutin.<br/>
<br/>
              11. Il résulte de ce qui précède que M. B... D... n'est pas fondé à demander l'annulation des opérations électorales qui se sont déroulées le 15 mars 2020 pour l'élection des conseillers municipaux et communautaires de la commune de Saint-Sulpice-sur-Risle.<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : L'ordonnance du président de la 1ère chambre du tribunal administratif de Caen du 25 mars 2020 est annulée.<br/>
Article 2 : La protestation de M. B... D... est rejetée.<br/>
Article 3 : La présente décision sera notifiée à M. E... D..., à M. A... C..., premier défendeur dénommé, et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">28-04-05-01 ÉLECTIONS ET RÉFÉRENDUM. ÉLECTIONS MUNICIPALES. OPÉRATIONS ÉLECTORALES. DÉROULEMENT DU SCRUTIN. - COMMUNES DE 1 000 HABITANTS ET PLUS - NIVEAU DE L'ABSTENTION - 1) PRINCIPE - CIRCONSTANCE PAR ELLE-MÊME SANS INCIDENCE SUR LES RÉSULTATS DU SCRUTIN [RJ2] - 2) ESPÈCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">28-08-01-02 ÉLECTIONS ET RÉFÉRENDUM. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. INTRODUCTION DE L'INSTANCE. DÉLAIS. - ELECTIONS MUNICIPALES DE 2020 - PREMIER TOUR (15 MARS) - 1) DÉLAI DE CONTESTATION ÉCHÉANT LE 25 MAI - 2) TARDIVETÉ OPPOSÉE PAR LE JUGE DE PREMIÈRE INSTANCE - RELEVÉ DE FORCLUSION EN APPEL [RJ1].
</SCT>
<ANA ID="9A"> 28-04-05-01 1) Ni par l'article L. 262 du code électoral, ni par la loi n° 2020-290 du 23 mars 2020 le législateur n'a subordonné à un taux de participation minimal la répartition des sièges au conseil municipal à l'issue du premier tour de scrutin dans les communes de mille habitants et plus, lorsqu'une liste a recueilli la majorité absolue des suffrages exprimés. Le niveau de l'abstention n'est ainsi, par lui-même, pas de nature à remettre en cause les résultats du scrutin, s'il n'a pas altéré, dans les circonstances de l'espèce, sa sincérité.,,,2) Requérant faisant seulement valoir que le taux d'abstention s'est élevé à 56,07 % dans sa commune, sans invoquer aucune autre circonstance relative au déroulement de la campagne électorale ou du scrutin dans la commune qui montrerait, en particulier, qu'il aurait été porté atteinte au libre exercice du droit de vote ou à l'égalité entre les candidats. Dans ces conditions, le niveau de l'abstention constatée ne peut être regardé comme ayant altéré la sincérité du scrutin.</ANA>
<ANA ID="9B"> 28-08-01-02 1) Il résulte de l'article 11 de la loi n° 2020-290 du 23 mars 2020, du 3° du II de l'article 15 de l'ordonnance n° 2020-305 du 25 mars 2020 et de l'article 1er du décret n° 2020-571 du 14 mai 2020, combinés avec le second alinéa de l'article 642 du code de procédure civile (CPC), que les réclamations contre les opérations électorales qui se sont déroulées le 15 mars 2020 pouvaient être formées au plus tard le lundi 25 mai 2020 à dix-huit heures.,,,2) Protestation enregistrée le 22 mars 2020 au greffe du tribunal administratif, soit après l'expiration du délai normalement imparti par l'article R. 119 du code électoral mais avant le terme du délai indiqué au point précédent, découlant du 3° du II de l'article 15 de l'ordonnance du 25 mars 2020. Ces dernières dispositions devant être regardées comme ayant relevé de la forclusion encourue les protestations enregistrées entre l'expiration du délai normalement imparti et leur entrée en vigueur le 27 mars 2020, ainsi qu'elles pouvaient le faire sur le fondement du 2° du I de l'article 11 de la loi du 23 mars 2020, il y a lieu d'annuler l'ordonnance attaquée rejetant comme tardive la protestation.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., s'agissant de l'application par le juge d'appel d'un relevé de forclusion prévu par un texte postérieur au jugement de première instance, CE, Section, 05 mai 1967,,, n° 69402, p. 195.,,[RJ2] Rappr., sur l'exigence que le juge de l'élection apprécie en revanche si le niveau de l'abstention a pu ou non altérer, dans les circonstances de l'espèce, la sincérité du scrutin, Cons. const., 17 juin 2020, n° 2020-849 QPC.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
