<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036743983</ID>
<ANCIEN_ID>JG_L_2018_03_000000404819</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/74/39/CETATEXT000036743983.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 4ème chambres réunies, 26/03/2018, 404819</TITRE>
<DATE_DEC>2018-03-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>404819</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER ; SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>M. Frédéric Puigserver</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:404819.20180326</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              L'association Le Colombier a demandé au tribunal administratif de Cergy-Pontoise d'annuler pour excès de pouvoir l'arrêté du 29 octobre 2010 par lequel le préfet du Val-d'Oise, le directeur général de l'agence régionale de santé d'Ile-de-France et le président du conseil général du Val-d'Oise ont procédé, en application des articles L. 313-19 et R. 314-97 du code de l'action sociale et des familles, à la dévolution, à la ligue pour l'adaptation du diminué physique au travail et à l'association Handicap, autisme, association réunie du Parisis, de l'actif net immobilisé de onze établissements et services médico-sociaux qu'elle gérait. Par un jugement n° 1009732 du 5 avril 2012, le tribunal administratif de Cergy-Pontoise a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 15VE03152 du 1er septembre 2016, statuant sur renvoi du Conseil d'Etat, la cour administrative d'appel de Versailles a rejeté l'appel formé par l'association Le Colombier contre le jugement du 5 avril 2012.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 2 novembre 2016, 2 février 2017 et 2 mars 2018 au secrétariat du contentieux du Conseil d'Etat, l'association Le Colombier demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt de la cour administrative d'appel de Versailles du 1er septembre 2016 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat et du département du Val-d'Oise la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'action sociale et des familles ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Puigserver, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Foussard, Froger, avocat de l'association Le Colombier et à la SCP Rocheteau, Uzan-Sarano, avocat du département du Val-d'Oise.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 31 mars 2010, le préfet du Val-d'Oise et le président du conseil général de ce département ont prononcé la fermeture définitive de l'ensemble des établissements et services sociaux et médico-sociaux gérés par l'association Le Colombier, en application de l'article L. 313-16 du code de l'action sociale et des familles. Par un arrêté du 29 octobre 2010, le préfet, le directeur général de l'agence régionale de santé d'Ile-de-France et le président du conseil général ont procédé, en application des articles L. 313-19 et R. 314-97 du même code, à la dévolution, à deux nouvelles associations gestionnaires, de l'actif net immobilisé des établissements et services ainsi fermés. Par un jugement du 5 avril 2012, le tribunal administratif de Cergy-Pontoise a rejeté la demande de l'association Le Colombier tendant à l'annulation de cet arrêté du 29 octobre 2010. L'association se pourvoit en cassation contre l'arrêt du 1er septembre 2016 par lequel la cour administrative d'appel de Versailles, statuant sur renvoi du Conseil d'Etat, a rejeté sa requête d'appel.<br/>
<br/>
              2. Aux termes de l'article L. 313-19 du code de l'action sociale et des familles, dans sa rédaction applicable à la décision attaquée : " En cas de fermeture définitive d'un établissement ou d'un service géré par une personne morale de droit public ou de droit privé celle-ci reverse à une collectivité publique ou à un établissement privé poursuivant un but similaire les sommes affectées à l'établissement ou service fermé, apportées par l'Etat, par l'agence régionale de santé, les collectivités territoriales et leurs établissements publics ou par les organismes de sécurité sociale, énumérées ci-après : / 1° Les subventions d'investissement non amortissables, grevées de droits, ayant permis le financement de l'actif immobilisé de l'établissement ou du service. Ces subventions sont revalorisées selon des modalités fixées par décret ; / (...) / 3° Des excédents d'exploitation provenant de la tarification affectés à l'investissement de l'établissement ou du service, revalorisés dans les conditions prévues au 1° ; / (...) / La collectivité publique ou l'établissement privé attributaire des sommes précitées peut être : / a) Choisi par le gestionnaire de l'établissement ou du service fermé, avec l'accord de l'autorité ou des autorités ayant délivré l'autorisation du lieu d'implantation de cet établissement ou service ; / b) Désigné par l'autorité compétente de l'Etat dans le département, en cas d'absence de choix du gestionnaire ou de refus par l'autorité ou les autorités mentionnées au a. / L'organisme gestionnaire de l'établissement ou du service fermé peut, avec l'accord de l'autorité de tarification concernée, s'acquitter des obligations prévues aux 1° et 3° en procédant à la dévolution de l'actif net immobilisé de l'établissement ou du service ". Aux termes de l'article R. 314-97 de ce code, dans sa rédaction alors applicable : " (...) L'organisme gestionnaire de l'établissement ou du service qui a cessé son activité ou a fermé peut, avec l'accord de l'autorité de tarification, s'acquitter de l'obligation relative au reversement (...) des subventions d'investissement mentionnées à l'article L. 313-19, en procédant à la dévolution de l'actif net immobilisé de l'établissement ou du service. / L'organisme gestionnaire dispose d'un délai de 30 jours à compter de l'arrêté de fermeture ou de la cessation d'activité de l'établissement ou du service pour choisir entre le versement des sommes exigibles au titre (...) des 1° et 3° de l'article L. 313-19 ou la dévolution de l'actif net immobilisé. Après ce délai, le représentant de l'Etat dans le département arrête l'option après accord, le cas échéant, de l'autorité de tarification. (...) ".<br/>
<br/>
              3. Il résulte des dispositions de l'article L. 313-19 du code de l'action sociale et des familles, citées au point précédent, qu'il appartient, en principe, à l'organisme gestionnaire d'un établissement ou d'un service social ou médico-social dont la fermeture définitive a été prononcée par l'autorité administrative de reverser à une collectivité publique ou à un établissement privé poursuivant un but similaire l'ensemble des sommes énumérées par cet article. Toutefois, en application du dernier alinéa de cet article, il lui est loisible d'opter en faveur d'une dévolution pure et simple de l'ensemble de " l'actif net immobilisé " de l'établissement ou du service, en lieu et place du reversement des subventions d'investissement non amortissables qu'il a perçues pour le financement de cet actif ainsi que des excédents d'exploitation, provenant de la tarification, affectés à l'investissement. A cet effet, l'article R. 314-97 du même code a prévu, afin que la procédure se poursuive dans des délais raisonnables, un délai de trente jours dans lequel l'organisme gestionnaire peut exercer l'option qui lui est offerte, étant précisé qu'à l'échéance de ce délai, il appartient au préfet, dans le cas où cet organisme opte en faveur de la dévolution, d'entériner ce choix, après avoir vérifié l'accord de l'autorité de tarification concernée. Il en résulte que lorsque, passé ce délai de trente jours, l'organisme n'a pas fait connaître son choix, seul le reversement des sommes énumérées par l'article L. 313-19 du code de l'action sociale et des familles peut être poursuivi par le préfet, le cas échéant, par application du régime de recouvrement forcé des créances publiques.<br/>
<br/>
              4. Il résulte de ce qui précède qu'en jugeant que, dès lors que l'association Le Colombier n'avait pas fait connaître, dans le délai de trente jours prévu par l'article R. 314-97 du code de l'action sociale et des familles, son choix entre le versement des sommes exigibles au titre des 1° et 3° de l'article L. 313-19 du même code ou la dévolution de l'actif net immobilisé des onze établissements et structures dont elle assurait la gestion, le préfet pouvait légalement prononcer cette dévolution, la cour a commis une erreur de droit. L'association est, par suite, fondée à demander l'annulation de l'arrêt qu'elle attaque. Le motif retenu suffisant à entraîner cette annulation, il n'est pas nécessaire d'examiner les autres moyens du pourvoi.<br/>
<br/>
              5. Aux termes du second alinéa de l'article L. 821-2 du code de justice administrative : " Lorsque l'affaire fait l'objet d'un second pourvoi en cassation, le Conseil d'Etat statue définitivement sur cette affaire ". Il y a lieu, par suite, de régler l'affaire au fond.<br/>
<br/>
              6. Il résulte de ce qui a été dit au point 3 qu'en l'absence de choix exprimé par l'association Le Colombier dans le délai de trente jours prévu par l'article R. 314-97 du code de l'action sociale et des familles en faveur de la dévolution de l'actif net immobilisé des onze établissements et structures dont elle assurait la gestion, l'arrêté attaqué ne pouvait procéder à cette dévolution. Par suite, sans qu'il soit besoin d'examiner les autres moyens de la requête, l'association requérante est fondée à soutenir que c'est à tort que, par le jugement du 5 avril 2012, le tribunal administratif de Cergy-Pontoise a rejeté sa demande tendant à l'annulation de cet arrêté.<br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge tant de l'Etat que du département du Val-d'Oise le versement à l'association Le Colombier d'une somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative. Les dispositions de cet article font, en revanche, obstacle à ce qu'il soit fait droit aux conclusions du département du Val-d'Oise et de l'agence régionale de santé d'Ile-de-France tendant aux mêmes fins.<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Versailles du 1er septembre 2016 est annulé.<br/>
Article 2 : Le jugement du tribunal administratif de Cergy-Pontoise du 5 avril 2012 est annulé.<br/>
Article 3 : L'arrêté du préfet du Val-d'Oise, du directeur général de l'agence régionale de santé d'Ile-de-France et du président du conseil général du Val-d'Oise du 29 octobre 2010 est annulé.<br/>
Article 4 : L'Etat et le département du Val-d'Oise verseront à l'association Le Colombier une somme de 2 000 euros chacun au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 5 : Les conclusions du département du Val-d'Oise et de l'agence régionale de santé d'Ile-de-France présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 6 : La présente décision sera notifiée à l'association Le Colombier et au département du Val-d'Oise.<br/>
Copie en sera adressée à la ligue pour l'adaptation du diminué physique au travail à l'association Handicap, autisme, association réunie du Parisis, à l'agence régionale de santé d'Ile-de-France et à la ministre des solidarités et de la santé <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">04-03-01 AIDE SOCIALE. INSTITUTIONS SOCIALES ET MÉDICO-SOCIALES. ÉTABLISSEMENTS - QUESTIONS COMMUNES. - CESSATION DÉFINITIVE DES ACTIVITÉS DE L'ÉTABLISSEMENT PRONONCÉE PAR L'AUTORITÉ ADMINISTRATIVE - DROIT DE L'ORGANISME GESTIONNAIRE D'OPTER ENTRE LE REVERSEMENT DES SOMMES ÉNUMÉRÉES PAR L'ARTICLE L. 313-19 DU CASF ET LA DÉVOLUTION DE L'ACTIF NET IMMOBILISÉ - CAS OÙ L'ORGANISME GESTIONNAIRE N'A PAS EXERCÉ L'OPTION DANS LE DÉLAI IMPARTI - CONSÉQUENCES.
</SCT>
<ANA ID="9A"> 04-03-01 Il résulte de l'article L. 313-19 du code de l'action sociale et des familles (CASF) qu'il appartient, en principe, à l'organisme gestionnaire  d'un établissement ou d'un service social ou médico-social dont la fermeture définitive a été prononcée par l'autorité administrative de reverser à une collectivité publique ou à un établissement privé poursuivant un but similaire l'ensemble des sommes énumérées par cet article. Toutefois, en application du dernier alinéa de cet article, il lui est loisible d'opter en faveur d'une dévolution pure et simple de l'ensemble de l'actif net immobilisé de l'établissement ou du service, en lieu et place du reversement des subventions d'investissement non amortissables qu'il a perçues pour le financement de cet actif ainsi que des excédents d'exploitation, provenant de la tarification, affectés à l'investissement. A cet effet, l'article R. 314-97 du même code a prévu, afin que la procédure se poursuive dans des délais raisonnables, un délai de trente jours dans lequel l'organisme gestionnaire peut exercer l'option qui lui est offerte, étant précisé qu'à l'échéance de ce délai, il appartient au préfet, dans le cas où cet organisme opte en faveur de la dévolution, d'entériner ce choix, après avoir vérifié l'accord de l'autorité de tarification concernée. Il en résulte que lorsque, passé ce délai de trente jours, l'organisme n'a pas fait connaître son choix, seul le reversement des sommes énumérées par l'article L. 313-19 du CASF peut être poursuivi par le préfet, le cas échéant, par application du régime de recouvrement forcé des créances publiques.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
