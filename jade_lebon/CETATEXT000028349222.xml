<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028349222</ID>
<ANCIEN_ID>JG_L_2013_12_000000367007</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/34/92/CETATEXT000028349222.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème et 2ème sous-sections réunies, 16/12/2013, 367007</TITRE>
<DATE_DEC>2013-12-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>367007</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème et 2ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN ; LE PRADO</AVOCATS>
<RAPPORTEUR>M. Stéphane Bouchard</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Bertrand Dacosta</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:367007.20131216</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 20 mars et 21 juin 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour le département du Loiret, représenté par le président du conseil général ; le département demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement n° 1104471 du 26 février 2013 par lequel le tribunal administratif d'Orléans a annulé pour excès de pouvoir la décision du président du conseil général du Loiret du 14 octobre 2011 mettant fin au détachement de M. B...A...sur un emploi fonctionnel ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de M. A... ;<br/>
<br/>
              3°) de mettre à la charge de M. A...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi n° 83-634 du 13 juillet 1983 ;<br/>
<br/>
              Vu la loi n° 84-53 du 26 janvier 1984 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Stéphane Bouchard, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Bertrand Dacosta, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat du département du Loiret, et à Me Le Prado, avocat de M. A... ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article 53 de la loi du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale, il ne peut être mis fin aux fonctions des agents occupant un des emplois fonctionnels mentionnés par cet article, lesquels recouvrent, dans les départements et les régions, l'emploi de directeur général des services et, lorsque l'emploi est créé, celui de directeur général adjoint des services, " qu'après un délai de six mois suivant soit leur nomination dans l'emploi, soit la désignation de l'autorité territoriale. La fin des fonctions de ces agents est précédée d'un entretien de l'autorité territoriale avec les intéressés et fait l'objet d'une information de l'assemblée délibérante et du Centre national de la fonction publique territoriale ; elle prend effet le premier jour du troisième mois suivant l'information de l'assemblée délibérante " ;<br/>
<br/>
              2. Considérant qu'il résulte de ces dispositions que l'entretien préalable à la fin de détachement d'un agent sur un emploi fonctionnel, prévu pour lui permettre de présenter ses observations à l'autorité territoriale, doit être mené, compte tenu de la nature particulière de ses fonctions exercées auprès du chef de l'exécutif territorial, directement par cette seule autorité et non par un agent des services ; que cet entretien constitue pour l'agent concerné une garantie dont la privation entache d'illégalité la décision mettant fin au détachement sur l'emploi fonctionnel ; que, par suite, en estimant que la conduite de l'entretien de fin du détachement de M.A..., directeur général adjoint de services du département du Loiret, ne pouvait être déléguée par le président du conseil général au directeur du " pôle ressources humaines " des services du département, sans méconnaître la portée utile de la garantie instituée par l'article 53 de la loi du 26 janvier 1984, le tribunal administratif d'Orléans n'a pas commis d'erreur de droit ; <br/>
<br/>
              3. Considérant qu'en estimant que le motif déterminant de la fin de détachement anticipée de M.A... résidait dans sa demande de protection fonctionnelle faite le 1er avril 2011, à raison de faits de harcèlement moral dont il s'estimait victime de la part du directeur général des services, le tribunal administratif d'Orléans a porté sur les faits de l'espèce une appréciation souveraine exempte de dénaturation ;<br/>
<br/>
              4. Considérant qu'il résulte de tout ce qui précède que le département du Loiret n'est pas fondé à demander l'annulation du jugement attaqué ;<br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du département du Loiret la somme de 3 500 euros à verser à M. A...au titre des dispositions de l'article L. 761-1 du code de justice administrative ; qu'en revanche, ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de M.A..., qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
 Article 1er : Le pourvoi du département du Loiret est rejeté.<br/>
<br/>
 Article 2 : Le département du Loiret versera à M. A...une somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
 Article 3 : La présente décision sera notifiée au département du Loiret et à M. B...A.... <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-07-01-03 FONCTIONNAIRES ET AGENTS PUBLICS. STATUTS, DROITS, OBLIGATIONS ET GARANTIES. STATUT GÉNÉRAL DES FONCTIONNAIRES DE L'ÉTAT ET DES COLLECTIVITÉS LOCALES. DISPOSITIONS STATUTAIRES RELATIVES À LA FONCTION PUBLIQUE TERRITORIALE (LOI DU 26 JANVIER 1984). - EMPLOIS FONCTIONNELS - FIN DES FONCTIONS - ENTRETIEN PRÉALABLE AVEC L'AUTORITÉ TERRITORIALE (ART. 53 DE LA LOI DU 26 JANVIER 1984) - 1) POSSIBILITÉ POUR L'AUTORITÉ TERRITORIALE DE DÉLÉGUER L'ENTRETIEN À UN AGENT DES SERVICES - ABSENCE - 2) CARACTÈRE DE GARANTIE AU SENS DE LA JURISPRUDENCE DANTHONY [RJ1] - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">36-10 FONCTIONNAIRES ET AGENTS PUBLICS. CESSATION DE FONCTIONS. - FONCTION PUBLIQUE TERRITORIALE - FIN DES FONCTIONS SUR UN EMPLOI FONCTIONNEL - ENTRETIEN PRÉALABLE AVEC L'AUTORITÉ TERRITORIALE (ART. 53 DE LA LOI DU 26 JANVIER 1984) - 1) POSSIBILITÉ POUR L'AUTORITÉ TERRITORIALE DE DÉLÉGUER L'ENTRETIEN À UN AGENT DES SERVICES - ABSENCE - 2) CARACTÈRE DE GARANTIE AU SENS DE LA JURISPRUDENCE DANTHONY [RJ1] - EXISTENCE.
</SCT>
<ANA ID="9A"> 36-07-01-03 1) L'entretien préalable à la fin de détachement d'un agent sur un emploi fonctionnel, prévu par l'article 53 de la loi n° 84-53 du 26 janvier 1984 pour lui permettre de présenter ses observations à l'autorité territoriale, doit être mené, compte tenu de la nature particulière de ses fonctions, exercées auprès du chef de l'exécutif territorial, directement par cette seule autorité et non par un agent des services.... ,,2) Cet entretien constitue pour l'agent concerné une garantie dont la privation entache d'illégalité la décision mettant fin au détachement sur l'emploi fonctionnel.</ANA>
<ANA ID="9B"> 36-10 1) L'entretien préalable à la fin de détachement d'un agent sur un emploi fonctionnel, prévu par l'article 53 de la loi n° 84-53 du 26 janvier 1984 pour lui permettre de présenter ses observations à l'autorité territoriale, doit être mené, compte tenu de la nature particulière de ses fonctions, exercées auprès du chef de l'exécutif territorial, directement par cette seule autorité et non par un agent des services.... ,,2) Cet entretien constitue pour l'agent concerné une garantie dont la privation entache d'illégalité la décision mettant fin au détachement sur l'emploi fonctionnel.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Assemblée, 23 décembre 2011, Danthony et autres, n° 335033, p. 649. Comp., pour l'entretien préalable au non renouvellement du contrat d'un agent employé depuis six ans par une collectivité territoriale, CE, 26 avril 2013, Cella, n° 355509, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
