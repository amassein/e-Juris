<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036610508</ID>
<ANCIEN_ID>JG_L_2018_02_000000406255</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/61/05/CETATEXT000036610508.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 16/02/2018, 406255</TITRE>
<DATE_DEC>2018-02-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>406255</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ZRIBI, TEXIER</AVOCATS>
<RAPPORTEUR>M. Clément Malverti</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Guillaume Odinet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:406255.20180216</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes : <br/>
<br/>
              1° Sous le numéro 406255, par une requête enregistrée le 22 décembre 2016 au secrétariat du contentieux du Conseil d'Etat, M. J...M..., la fédération de pankration et d'arts martiaux mixtes, la commission nationale Kenpô et la société à responsabilité limitée FAM demandent au Conseil d'Etat d'annuler pour excès de pouvoir l'arrêté du ministre de la ville, de la jeunesse et des sports du 3 octobre 2016 relatif aux règles techniques et de sécurité applicables aux manifestations publiques de sports de combat.  <br/>
<br/>
<br/>
<br/>
              2° Sous le numéro 406286, par une requête sommaire et un mémoire complémentaire, enregistrés les 23 décembre 2016 et 23 mars 2017, MM. A...G..., L...D..., E...K..., B...C..., N..., I...H..., demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté du ministre de la ville, de la jeunesse et des sports du 3 octobre 2016 relatif aux règles techniques et de sécurité applicables aux manifestations publiques de sports de combat ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat une somme de 6 000 euros en application des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ; <br/>
<br/>
              Vu : <br/>
              - le traité sur le fonctionnement de l'Union européenne ; <br/>
              - le code du sport ;<br/>
              - le décret n° 2016-843 du 24 juin 2016 ; <br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Clément Malverti, auditeur, <br/>
<br/>
              - les conclusions de M. Guillaume Odinet, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boullez, avocat de M. G...et autres, et à la SCP Zribi, Texier, avocat de la Commission française de mixed martial arts (CFMMA) ; <br/>
<br/>
              Vu les notes en délibéré, enregistrées le 31 janvier 2018, présentées par la Commission française de mixed martial arts ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant que la requête de M. J...M...et autres et la requête de M. A...G...et autres sont dirigées contre l'arrêté du ministre de la ville, de la jeunesse et des sports du 3 octobre 2016 relatif aux règles techniques et de sécurité applicables aux manifestations publiques de sports de combat ; qu'il y a lieu de les joindre pour statuer par une seule décision ; <br/>
<br/>
              Sur les interventions : <br/>
<br/>
              2.	Considérant que la Commission française de " mixed martial arts " justifie d'un intérêt suffisant à l'annulation de la décision attaquée ; que, conformément à l'article 13.6 de ses statuts, son président a été régulièrement habilité par une décision du 15 novembre 2016 ; qu'ainsi les interventions de la Commission française de " mixed martial arts " doivent être admises ;<br/>
<br/>
              Sur la légalité externe : <br/>
<br/>
              3.	Considérant en premier lieu qu'aux termes  de l'article L. 331-2 du code du sport : " L'autorité administrative peut, par arrêté motivé, interdire la tenue de toute compétition, rencontre, démonstration ou manifestation publique de quelque nature que ce soit, dans une discipline ou une activité sportive lorsqu'elle présente des risques d'atteinte à la dignité, à l'intégrité physique ou à la santé des participants. " ; que, selon  l'article R. 331-47 du code du sport applicable à la date d'entrée en vigueur de l'arrêté attaqué : " Les manifestations publiques de sports de combat : / 1° Organisées par une fédération sportive délégataire, ses organes régionaux ou départementaux ou par l'un de ses membres ; / 2° Relevant d'une discipline dans laquelle cette fédération a reçu la délégation prévue à l'article L. 131 14 ; / 3° Et inscrites au calendrier de cette fédération, / ne sont pas soumises à l'obligation d'être préalablement déclarées auprès du préfet du département dans lequel la manifestation est organisée. / Toute autre manifestation publique de sports de combat doit être préalablement déclarée auprès du préfet. " ; qu'aux termes de l'article R. 331-51 du même code, dans sa rédaction issue du décret du 24 juin 2016 relatif aux manifestations publiques de sports de combat, applicable à la date de l'arrêté attaqué : " Dans les disciplines dans lesquelles aucune fédération n'a reçu délégation, la déclaration est accompagnée d'une déclaration sur l'honneur de l'organisateur de se conformer aux règles techniques et de sécurité prévues par arrêté du ministre chargé des sports. " ; <br/>
<br/>
              4.	Considérant qu'il résulte des dispositions de l'article R. 331-51 du code du sport, qui trouvent leur fondement légal dans les dispositions de l'article L. 331-2 du même code, que, pour les disciplines dans lesquelles aucune fédération n'a reçu délégation, il appartient au ministre chargé des sports de déterminer les règles techniques et de sécurité applicables aux manifestations publiques, notamment pour préserver la dignité, l'intégrité physique et la santé des participants ; <br/>
<br/>
              5.	Considérant qu'il ressort des pièces du dossier qu'aucune fédération sportive n'a reçu délégation pour la discipline des arts martiaux mixtes ; que, par suite, les requérants ne sauraient utilement soutenir qu'en prenant l'arrêté litigieux le ministre chargé des sports aurait méconnu les compétences de fédérations sportives délégataires ;<br/>
<br/>
              6.	Considérant qu'aux termes de l'article D. 331-1 du code du sport  " Le ministre de l'intérieur et le ministre chargé des sports arrêtent, après avis de la Commission nationale de sécurité des enceintes sportives, les caractéristiques des manifestations sportives nécessitant des garanties particulières de sécurité et les modalités selon lesquelles les fédérations sportives en déterminent la liste et la transmettent aux autorités détentrices des pouvoirs de police. " ; que l'arrêté attaqué n'a ni pour objet ni pour effet de fixer les caractéristiques des manifestations sportives nécessitant des garanties particulières de sécurité et les modalités selon lesquelles les fédérations en déterminent la liste et la transmettent aux autorités détentrices des pouvoirs de police ; que, par suite, M. M...et autres ne sauraient utilement soutenir que l'arrêté aurait dû être pris conjointement par le ministre de l'intérieur et le ministre chargé des sports ; <br/>
<br/>
              7.	Considérant que MmeF..., chef de service et adjointe au directeur des sports, nommée par décret du 15 janvier 2016 publié au Journal officiel le 17 janvier 2016, était habilitée à signer l'arrêté attaqué au nom du ministre chargé des sports, en vertu des dispositions du 1° de l'article premier du décret du 27 juillet 2005 relatif aux délégations de signature des membres du Gouvernement ; que le moyen tiré de ce que l'arrêté attaqué aurait été signé par une autorité incompétente ne peut, par suite, qu'être écarté ;<br/>
<br/>
              8.	Considérant qu'il résulte de ce qui précède que les moyens tirés de ce que l'arrêté aurait été pris par une autorité incompétente doivent être écartés ; <br/>
<br/>
              9.	Considérant, en deuxième lieu, qu'ainsi qu'il a été dit ci dessus, l'arrêté attaqué n'a ni pour objet ni pour effet de fixer les caractéristiques des manifestations sportives nécessitant des garanties particulières de sécurité ; que le projet d'arrêté  n'avait donc pas à être soumis à la commission nationale de sécurité des enceintes sportives en vertu des dispositions de l'article D. 331-1 du code du sport ; que le moyen tiré du défaut de consultation de cette commission doit être écarté ; <br/>
<br/>
              10.	Considérant, en troisième lieu, qu'aux termes de l'article L. 1212-2 du code général des collectivités territoriales : " I - Le Conseil national d'évaluation des normes est consulté par le Gouvernement sur l'impact technique et financier, pour les collectivités territoriales et leurs établissements publics, des projets de textes réglementaires créant ou modifiant des normes qui leur sont applicables. (...) " ; que l'arrêté attaqué, qui ne crée ni ne modifie aucune norme applicable aux collectivités territoriales ou à leurs établissements publics, n'est pas au nombre des textes réglementaires pour lesquels la consultation du conseil national de l'évaluation des normes est obligatoire en vertu de ces dispositions ; que, par suite, le moyen tiré du défaut de consultation de ce conseil doit être écarté ;<br/>
<br/>
              11.	Considérant, en dernier lieu, que le moyen tiré de ce que l'arrêté attaqué serait entaché d'une erreur en ce qu'il aurait inséré dans le code du sport un article numéroté A. 331-36 alors que le code du sport comportait un article portant la même numérotation manque en fait ; qu'il ne peut, par suite et en tout état de cause, qu'être écarté ; <br/>
<br/>
              Sur la légalité interne :<br/>
<br/>
              12.	Considérant, en premier lieu, que le 1 de l'article 2 de l'arrêté attaqué prescrit le déroulement du combat sur un tapis ou un ring à trois ou quatre cordes, que le 4 du même article impose l'utilisation de gants et de protections corporelles et que le 6 du même article interdit le recours à certaines techniques de combat ; qu'alors même que ces prescriptions ne seraient pas conformes à certaines pratiques usuelles de la discipline, les requérants ne sont pas fondés à soutenir qu'elles ne seraient pas proportionnées à l'objectif de préservation de la dignité, de l'intégrité physique et de la santé des sportifs qu'il appartenait au ministre chargé des sports de poursuivre ou qu'elles porteraient illégalement atteinte à la liberté de pratiquer un sport ou à la liberté du commerce et de l'industrie ;<br/>
<br/>
              13.	Considérant, en deuxième lieu, que les restrictions que l'arrêté litigieux est susceptible de porter à la liberté d'établissement, à la libre prestation de services à l'intérieur de l'Union européenne, ainsi qu'au principe de reconnaissance mutuelle, sont en tout état de cause justifiées par les raisons impérieuses d'intérêt général de protection de la dignité, de l'intégrité physique et de la santé des sportifs ; que, par suite, le moyen tiré de ce que l'arrêté litigieux méconnaîtrait les dispositions des articles 49 et 56 du traité sur le fonctionnement de l'Union européenne ne peut qu'être écarté ;<br/>
<br/>
              14.	Considérant, en troisième lieu, que les disciplines qui ne font pas l'objet d'une délégation du ministre des sports à une fédération sont placées dans une situation différente des disciplines pour lesquelles existe une fédération délégataire ; que, ainsi qu'il a été dit, le ministre a fait usage des pouvoirs qu'il tient des dispositions des articles L. 331-2 et R. 331-51 du code du sport ; que le moyen tiré de ce qu'il aurait ainsi méconnu le principe d'égalité ne peut qu'être écarté ; <br/>
<br/>
              15.	Considérant, en quatrième lieu, que M. G...et autres soutiennent que l'arrêté attaqué aurait pour effet d'exclure la création d'une fédération d'arts martiaux mixtes susceptible de recevoir un agrément et une délégation du ministre chargé des sports en méconnaissance des articles L. 131-8 et L. 131-14 du code du sport ; qu'aux termes de l'article L. 131-8 de ce code : " I - Un agrément peut être délivré par le ministre chargé des sports aux fédérations qui, en vue de participer à l'exécution d'une mission de service public, ont adopté des statuts comportant certaines dispositions obligatoires et un règlement disciplinaire conforme à un règlement type. (...) " ; qu'aux termes de l'article L. 131-14 de ce code : " Dans chaque discipline sportive et pour une durée déterminée, une seule fédération agréée reçoit délégation du ministre chargé des sports. (...) " ; que s'il résulte de ces dispositions que le ministre chargé des sports peut agréer les fédérations sportives et délivrer une délégation à une fédération agréée par discipline sportive, l'arrêté attaqué n'a ni pour objet ni pour effet d'empêcher la délivrance d'un agrément ni d'une délégation à une fédération sportive ; que, par suite, que le moyen doit être écarté ; <br/>
<br/>
              16.	Considérant, en dernier lieu, que le détournement de pouvoir allégué n'est pas établi ; <br/>
<br/>
              17.	Considérant qu'il résulte de tout ce qui précède, sans qu'il soit besoin de statuer sur la recevabilité des requêtes, que M. M...et autres et M. A...G...et autres ne sont pas fondés à demander l'annulation pour excès de pouvoir de l'arrêté du ministre de la ville, de la jeunesse et des sports du 3 octobre 2016 ; que les conclusions présentées par M. A... G...et autres au titre des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent par suite qu'être rejetées ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
                            --------------<br/>
<br/>
Article 1er : Les interventions de la Commission française de " mixed martial arts " sont admises. <br/>
<br/>
Article 2 : Les requêtes de M. M...et autres et de M. G...et autres sont rejetées. <br/>
<br/>
Article 3 : La présente décision sera notifiée à M. J...M...et à M. A...G..., premiers requérants dénommés, ainsi qu'à la ministre des sports. Copie en sera adressée à la Commission française de " mixed martial arts " (CFMMA).<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">63-05 SPORTS ET JEUX. SPORTS. - DISCIPLINE SPORTIVE DANS LAQUELLE AUCUNE FÉDÉRATION N'A REÇU DÉLÉGATION - AUTORITÉ COMPÉTENTE POUR DÉTERMINER LES RÈGLES TECHNIQUES ET DE SÉCURITÉ APPLICABLES AUX MANIFESTATIONS PUBLIQUES - MINISTRE CHARGÉ DES SPORTS.
</SCT>
<ANA ID="9A"> 63-05 Il résulte de l'article R. 331-51 du code des sports, qui trouve son fondement légal dans l'article L. 331-2 du même code, que, pour les disciplines dans lesquelles aucune fédération n'a reçu délégation, il appartient au ministre chargé des sports de déterminer les règles techniques et de sécurité applicables aux manifestations publiques, notamment pour préserver la dignité, l'intégrité physique et la santé des participants.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
