<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028267542</ID>
<ANCIEN_ID>JG_L_2013_11_000000373300</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/26/75/CETATEXT000028267542.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 27/11/2013, 373300, Publié au recueil Lebon</TITRE>
<DATE_DEC>2013-11-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>373300</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ ; SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2013:373300.20131127</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 15 novembre 2013 au secrétariat du contentieux du Conseil d'Etat, présentée pour M. et MmeA..., demeurant..., agissant tant en leur nom personnel qu'au nom de leurs quatre enfants mineurs ; les requérants demandent au juge des référés du Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'ordonnance n° 1303029 du 31 octobre 2013 par laquelle le juge des référés du tribunal administratif d'Orléans, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, a rejeté leur demande tendant à ce qu'il soit enjoint au directeur de l'agence régionale de santé du Centre et au président du conseil général du Loir-et-Cher de prendre, dans le délai de quinze jours et sous astreinte de 200 euros par jour de retard, toutes mesures nécessaires pour assurer l'exécution de la décision du 29 novembre 2012 de la commission des droits et de l'autonomie des personnes handicapées de la maison départementale des personnes handicapées des Yvelines préconisant une orientation de leur fils C...dans un institut médico-éducatif du Loir-et-Cher ou, à défaut, pour assurer une prise en charge " effective dans la durée, pluridisciplinaire et adaptée à son état et à son âge " par la création d'une place dotée en personnels suffisants et compétents au sein d'un institut médico-éducatif ou de prononcer toute mesure jugée utile au rétablissement des libertés fondamentales ;<br/>
<br/>
              2°) de faire droit à leur demande de première instance ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement de la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              ils soutiennent que : <br/>
              - la condition d'urgence est remplie dès lors que, en l'absence de prise en charge, leur fils court et fait courir de graves dangers à son entourage compte tenu de son état, ainsi qu'en attestent les nombreuses pièces versées au dossier ;<br/>
              - la carence du département du Loir-et-Cher et de l'agence régionale de santé du Centre dans la prise en charge de leur fils, souffrant du syndrome autistique, porte une atteinte grave et manifestement illégale au droit à la vie et à la protection de la santé de ce dernier, à son droit à l'éducation, à son droit à une prise en charge pluridisciplinaire, à son droit au respect d'une vie privée et familiale normale ainsi qu'à son droit à la dignité ;<br/>
              - l'administration ne saurait utilement se retrancher derrière son manque de moyens dès lors qu'elle est tenue à une obligation de résultat en vertu de l'article L. 246-1 du code de l'action sociale et des familles ;<br/>
              - qu'il incombe à l'agence régionale de santé d'adapter l'offre de soins pour permettre la prise en charge de leur enfant en institut médico-éducatif et au département d'apporter l'aide nécessaire pour la mise en oeuvre des décisions de la commission des droits et de l'autonomie des personnes handicapées ;<br/>
<br/>
<br/>
              Vu le mémoire en défense, enregistré le 20 novembre 2013, présenté par la ministre des affaires sociales et de la santé, qui conclut au rejet de la requête ; <br/>
<br/>
              elle soutient que :<br/>
              - l'agence régionale de santé n'est pas compétente pour décider de l'admission d'une personne handicapée dans un institut médico-éducatif compte tenu des pouvoirs limités qui sont les siens en vertu du b) du 2° de l'article L. 1431-2 du code de la santé publique ;<br/>
              - le refus de prise en charge d'une personne handicapée dans un tel établissement ne relève pas de la compétence du juge administratif ; <br/>
              - les conditions prévues par l'article L. 521-2 du code de justice administrative ne sont, en l'espèce, pas réunies dès lors que, d'une part, l'administration a effectué toutes les diligences lui incombant compte tenu des moyens dont elle dispose et que, d'autre part, les requérants n'établissent pas le caractère manifestement grave et illégal de l'atteinte à une liberté fondamentale ;<br/>
<br/>
              Vu le mémoire en défense, enregistré le 20 novembre 2013, présenté pour le département du Loir-et-Cher, représenté par le président de son conseil général, qui conclut au rejet de la requête en tant qu'elle vise le département ; <br/>
<br/>
              il soutient que :<br/>
              - le président du conseil général n'a pas le pouvoir d'ordonner les mesures sollicitées par les requérants ;<br/>
              - la maison départementale des personnes handicapées n'a pas failli à sa mission ;<br/>
<br/>
              Vu le mémoire en réplique, enregistré le 21 novembre 2013, présenté pour M. et MmeA..., qui reprennent les conclusions de leur requête et les mêmes moyens ; ils soutiennent, en outre, d'une part, que l'agence régionale de santé est bien compétente dès lors qu'il lui revient d'organiser une offre de soins suffisante et de rechercher des places disponibles, d'autre part, que ni la ministre des affaires sociales, ni le département du Loir-et-Cher n'ont accompli les diligences nécessaires pour assurer une prise en charge effective et adaptée de leur enfant ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, M. et MmeA..., d'autre part, la ministre des affaires sociales et de la santé ainsi que le président du conseil général du Loir-et-Cher ;<br/>
<br/>
              Vu le procès-verbal de l'audience publique du 22 novembre 2013 à 10 heures  au cours de laquelle ont été entendus : <br/>
<br/>
              - Me Thiriez avocat au Conseil d'Etat et à la Cour de cassation, avocat de M. et Mme A...;<br/>
<br/>
              - M. et MmeA... ;<br/>
<br/>
              - Me Hazan, avocat au Conseil d'Etat et à la Cour de cassation, avocat du département du Loir-et-Cher ;<br/>
<br/>
- les représentants de la ministre des affaires sociales et de la santé ; <br/>
              et à l'issue de laquelle le juge des référés a clôturé puis rouvert l'instruction jusqu'au mardi 26 novembre à 14 heures pour tenir compte de nouveaux éléments produits par les requérants ;<br/>
<br/>
              Vu le nouveau mémoire, enregistré le 22 novembre 2013, présenté par la ministre des affaires sociales et de la santé, qui conclut au rejet de la requête par les mêmes moyens ; elle fait valoir, en outre, que le directeur de l'agence régionale de santé Centre a demandé au directeur général de l'association départementale de parents et amis de personnes handicapées mentales (ADAPEI) du Loir-et-Cher de mettre en oeuvre sans délai la procédure d'admission des enfants en rupture de parcours repérés par la maison départementale des adultes handicapés et, en particulier, celle de C...A... ;<br/>
<br/>
              Vu le nouveau mémoire, enregistré le 26 novembre 2013, présenté pour M. et MmeA..., qui reprennent les conclusions et les moyens de leur requête et demandent, en outre, au juge des référés du Conseil d'Etat d'enjoindre au directeur de l'agence régionale de santé du Centre de prendre toutes les dispositions nécessaires afin d'assurer l'ouverture d'une structure d'accueil de jour pour enfants autistes sur l'agglomération blésoise, permettant la prise en charge effective de leur enfant, au plus tard le 16 décembre 2013, et de prendre toutes les mesures nécessaires, notamment en garantissant une offre de soins suffisante et adaptée, afin d'assurer à leur enfant, au-delà de juillet 2014, une prise en charge pluridisciplinaire effective ; ils font valoir, en outre, que l'Etat dispose des pouvoirs et des moyens pour assurer l'accueil de leur enfant en structure de jour avant le 16 décembre 2013 ; que cet accueil doit, en outre, être pérennisé ;<br/>
<br/>
              Vu le nouveau mémoire, enregistré le 26 novembre 2013, présenté par la ministre des affaires sociales et de la santé, qui conclut au rejet de la requête par les mêmes moyens ;<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu la Constitution du 4 octobre 1958, notamment son Préambule ;<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              Vu le code de l'action sociale et des familles, notamment ses articles L. 114-1 et L. 246-1 ;<br/>
	Vu le code de la santé publique, notamment son article L. 1431-2 ;<br/>
<br/>
	Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures " ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 114-1 du code de l'action sociale et des familles : "  La personne handicapée a droit à la compensation des conséquences de son handicap quels que soient l'origine et la nature de sa déficience, son âge ou son mode de vie " ; qu'aux termes de l'article L. 246-1 du même code : "  Toute personne atteinte du handicap résultant du syndrome autistique et des troubles qui lui sont apparentés bénéficie, quel que soit son âge, d'une prise en charge pluridisciplinaire qui tient compte de ses besoins et difficultés spécifiques. Adaptée à l'état et à l'âge de la personne, cette prise en charge peut être d'ordre éducatif, pédagogique, thérapeutique et social (...) " ;<br/>
<br/>
              3. Considérant que ces dispositions imposent à l'Etat et aux autres personnes publiques chargées de l'action sociale en faveur des personnes handicapées d'assurer, dans le cadre de leurs compétences respectives, une prise en charge effective dans la durée, pluridisciplinaire et adaptée à l'état comme à l'âge des personnes atteintes du syndrome autistique ; que si une carence dans l'accomplissement de cette mission est de nature à engager la responsabilité de ces autorités, elle n'est susceptible de constituer une atteinte grave et manifestement illégale à une liberté fondamentale, au sens de l'article L. 521-2 du code de justice administrative, que si elle est caractérisée, au regard notamment des pouvoirs et des moyens dont disposent ces autorités, et si elle entraîne des conséquences graves pour la personne atteinte de ce syndrome, compte tenu notamment de son âge et de son état ; qu'en outre, le juge des référés ne peut intervenir, en application de cet article, que pour prendre des mesures justifiées par une urgence particulière et de nature à mettre fin immédiatement ou à très bref délai à l'atteinte constatée ; <br/>
<br/>
              4. Considérant qu'il résulte de l'instruction que le jeune C...A..., âgé de six ans, est atteint d'un syndrome autistique sévère ; qu'après avoir été pris en charge de la rentrée scolaire 2011 jusqu'au mois de juin 2013 dans une unité dédiée aux enfants de trois à six ans au centre hospitalier Théophile Roussel à Montesson, à raison de trois demi-journées par semaine, il a dû être orienté, notamment en raison de son âge, vers un institut médico-éducatif (IME) adapté aux enfants souffrant de troubles autistiques ; que, si trois décisions de la commission des droits et de l'autonomie des personnes handicapées (CDAPH) des Yvelines en date des 22 mars 2012, 12 juillet 2012 et 29 novembre 2012 ont autorisé sa prise en charge en semi-internat à temps plein dans un IME, notamment en ce qui concerne la dernière de ces décisions, dans un IME du Loir-et-Cher, département où la famille a déménagé, du 1er octobre 2012 au 31 juillet 2014, le jeune garçon ne bénéficie, depuis le mois de septembre 2013, que d'une prise en charge, à raison de quatre heures par semaine, par un service d'éducation spéciale et de soins à domicile (SESSAD) ainsi que d'une possibilité d'hébergement de nuit en cas d'urgence, dans la limite de 90 jours par an ; qu'en revanche, les nombreuses demandes présentées par ses parents depuis le mois de mars 2012, tant auprès des IME des Yvelines qu'auprès des IME du Loir-et-Cher mentionnés dans les décisions de la CDAPH, se sont heurtées à des refus en raison du manque de places disponibles ;<br/>
<br/>
              5. Considérant que si ce manque de places n'est pas contesté, il ressort toutefois des éléments versés au dossier et confirmés lors de l'audience publique par les représentants de l'administration que l'agence régionale de santé du Centre a engagé la mise en place, à très brève échéance, d'un dispositif provisoire d'accueil de jour dans la région de Blois pour quelques enfants atteints d'autisme sévère et dépourvus de prise en charge appropriée en raison du manque de places en établissement, au nombre desquels figure le jeune C...A... ; que le directeur de l'agence régionale a demandé, le 23 novembre 2013, au directeur général de l'association départementale de parents et amis de personnes handicapées mentales (ADAPEI) du Loir-et-Cher, auquel il a confié la mise en place de cette structure d'accueil au plus tard le 16 décembre 2013, de mettre en oeuvre sans délai la procédure d'admission des enfants en rupture de parcours repérés par la maison départementale des personnes handicapées et, en particulier, celle de C...A... ; que ces mesures sont de nature à assurer l'exécution partielle et à bref délai de la décision de la CDAPH des Yvelines du 29 novembre 2012 en ce qui concerne le placement de cet enfant en IME ; que celui-ci bénéficie, en outre, d'une prise en charge par un SESSAD, comme le prévoit aussi cette décision, ainsi que d'une possibilité d'hébergement de nuit en cas d'urgence, comme le prévoit une décision de la CDAPH du Loir-et-Cher en date du 24 octobre 2013 ; qu'à supposer même qu'une urgence particulière soit encore constituée, au regard des dangers que fait courir l'enfant, à ses proches comme à lui-même, faute de prise en charge par des structures adaptées à son état, les mesures prises par l'agence régionale de santé du Centre, eu égard aux compétences dont elle dispose à l'égard des IME en application du b) du 2° de l'article L. 1431-2 du code de la santé publique, lesquelles se limitent à autoriser la création de ces établissements, à contrôler leur fonctionnement et à leur allouer des ressources sans l'habiliter cependant à imposer la prise en charge d'une personne, et aux moyens, notamment budgétaires, dont elle dispose, ne révèlent aucune carence caractérisée dans l'accomplissement des obligations mises à la charge de l'Etat par l'article L. 246-1 du code de l'action sociale et des familles ; qu'aucune carence ne peut non plus être relevée et n'est d'ailleurs précisément invoquée à l'encontre du département du Loir-et-Cher, notamment en ce qui concerne la maison départementale des personnes handicapées ; qu'au surplus, les mesures demandées, à titre subsidiaire ou complémentaire, par les requérants, consistant à créer une place supplémentaire en IME, dotée de personnels suffisants et compétents, pour prendre en charge leur enfant, en particulier à compter du mois de juillet 2014, ne sont pas au nombre de celles qui peuvent être utilement ordonnées par le juge des référés, dans le cadre de la procédure d'urgence caractérisée prévue à l'article L. 521-2 du code de justice administrative, pour faire cesser à très brève échéance une atteinte à une liberté fondamentale ;<br/>
<br/>
              6. Considérant qu'il suit de là que la requête de M. et Mme A...ne peut qu'être rejetée, y compris leurs conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. et Mme A...est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. et Mme B...A..., au département du Loir-et-Cher et à la ministre des affaires sociales et de la santé. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">04-02-04 AIDE SOCIALE. DIFFÉRENTES FORMES D'AIDE SOCIALE. AIDE SOCIALE AUX PERSONNES HANDICAPÉES. - PERSONNES ATTEINTES DU HANDICAP RÉSULTANT DU SYNDROME AUTISTIQUE - OBLIGATION DE PRISE EN CHARGE PLURIDISCIPLINAIRE (ART. L. 114-1 ET L. 246-1 DU CODE DE L'ACTION SOCIALE ET DES FAMILLES) - 1) OBLIGATION DE RÉSULTAT - EXISTENCE [RJ1] - CONSÉQUENCE - CARENCE DANS L'ACCOMPLISSEMENT DE CETTE MISSION - FAUTE DE NATURE À ENGAGER LA RESPONSABILITÉ DES PERSONNES PUBLIQUES COMPÉTENTES - EXISTENCE - ATTEINTE GRAVE ET MANIFESTEMENT ILLÉGALE À UNE LIBERTÉ FONDAMENTALE AU SENS DU RÉFÉRÉ LIBERTÉ - CONDITIONS - CARENCE CARACTÉRISÉE AYANT DES CONSÉQUENCES GRAVES POUR LA PERSONNE AUTISTE - 2) ESPÈCE - ABSENCE DE PLACE EN IME POUR LA PRISE EN CHARGE D'UN ENFANT AUTISTE - CARENCE CARACTÉRISÉE DE L'ARS - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-035-03-03-01-02 PROCÉDURE. PROCÉDURES INSTITUÉES PAR LA LOI DU 30 JUIN 2000. RÉFÉRÉ TENDANT AU PRONONCÉ DE MESURES NÉCESSAIRES À LA SAUVEGARDE D'UNE LIBERTÉ FONDAMENTALE (ART. L. 521-2 DU CODE DE JUSTICE ADMINISTRATIVE). CONDITIONS D'OCTROI DE LA MESURE DEMANDÉE. ATTEINTE GRAVE ET MANIFESTEMENT ILLÉGALE À UNE LIBERTÉ FONDAMENTALE. ATTEINTE GRAVE ET MANIFESTEMENT ILLÉGALE. - CARENCE DE L'ADMINISTRATION DANS LA PRISE EN CHARGE D'UNE PERSONNE AUTISTE - CONDITION - CARENCE CARACTÉRISÉE ENTRAÎNANT DES CONSÉQUENCES GRAVES POUR LA PERSONNE AUTISTE.
</SCT>
<ANA ID="9A"> 04-02-04 1) Les articles L. 114-1 et L. 246-1 du code de l'action sociale et des familles imposent à l'Etat et aux autres personnes publiques chargées de l'action sociale en faveur des personnes handicapées d'assurer, dans le cadre de leurs compétences respectives, une prise en charge effective dans la durée, pluridisciplinaire et adaptée à l'état comme à l'âge des personnes atteintes du syndrome autistique.... ,,Si une carence dans l'accomplissement de cette mission est de nature à engager la responsabilité de ces autorités, elle n'est susceptible de constituer une atteinte grave et manifestement illégale à une liberté fondamentale, au sens de l'article L. 521-2 du code de justice administrative, que si elle est caractérisée, au regard notamment des pouvoirs et des moyens dont disposent ces autorités, et si elle entraîne des conséquences graves pour la personne atteinte de ce syndrome, compte tenu notamment de son âge et de son état.,,,2) Référé liberté formé par les parents d'un enfant autiste, dont la prise en charge en semi-internat à temps plein dans un institut médico-éducatif (IME) a été autorisée par la commission départementale des droits et de l'autonomie des personnes handicapées, mais qui, faute de place en institut, ne bénéficie que d'une prise en charge par un service d'éducation spéciale et de soins à domicile et d'une possibilité limitée d'hébergement de nuit en cas d'urgence.,,,En l'espèce, l'agence régionale de santé (ARS) s'est engagée à mettre en place, à très brève échéance, un dispositif d'accueil de jour dont pourra bénéficier le jeune enfant et a demandé la mise en oeuvre sans délai de la procédure d'admission en institut.... ,,Eu égard aux compétences de l'ARS à l'égard des IME en application du b du 2° de l'article L. 1431-2 du code de la santé publique, lesquelles se limitent à autoriser la création de ces établissements, à contrôler leur fonctionnement et à leur allouer des ressources, sans l'habiliter à imposer la prise en charge d'une personne, et aux moyens, notamment budgétaires, dont elle dispose, ces mesures ne révèlent aucune carence caractérisée dans l'accomplissement des obligations mises à la charge de l'Etat par l'article L. 246-1 du code de l'action sociale et des familles.</ANA>
<ANA ID="9B"> 54-035-03-03-01-02 Les articles L. 114-1 et L. 246-1 du code de l'action sociale et des familles imposent à l'Etat et aux autres personnes publiques chargées de l'action sociale en faveur des personnes handicapées d'assurer, dans le cadre de leurs compétences respectives, une prise en charge effective dans la durée, pluridisciplinaire et adaptée à l'état comme à l'âge des personnes atteintes du syndrome autistique.... ,,Si une carence dans l'accomplissement de cette mission est de nature à engager la responsabilité de ces autorités, elle n'est susceptible de constituer une atteinte grave et manifestement illégale à une liberté fondamentale, au sens de l'article L. 521-2 du code de justice administrative, que si elle est caractérisée, au regard notamment des pouvoirs et des moyens dont disposent ces autorités, et si elle entraîne des conséquences graves pour la personne atteinte de ce syndrome, compte tenu notamment de son âge et de son état.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 16 mai 2011, Mme,, n° 318501, p. 241.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
