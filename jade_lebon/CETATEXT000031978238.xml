<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031978238</ID>
<ANCIEN_ID>JG_L_2016_02_000000389223</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/97/82/CETATEXT000031978238.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème / 5ème SSR, 03/02/2016, 389223</TITRE>
<DATE_DEC>2016-02-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>389223</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème / 5ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, BLANCPAIN, SOLTNER, TEXIDOR ; SCP MASSE-DESSEN, THOUVENIN, COUDRAY</AVOCATS>
<RAPPORTEUR>Mme Pauline Pannier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Gaëlle Dumortier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2016:389223.20160203</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Caen d'annuler pour excès de pouvoir la décision du 21 décembre 2011 par laquelle le ministre du travail, de l'emploi et de la santé a annulé la décision du 23 juin 2011 de l'inspecteur du travail de la 6ème section du Calvados refusant d'autoriser la société ST-Ericsson France à le licencier. Par un jugement n° 1200364 du 20 novembre 2012, le tribunal administratif a annulé cette décision.<br/>
<br/>
              Par un arrêt n° 13NT00172 du 5 février 2015, la cour administrative d'appel de Nantes a rejeté l'appel formé contre ce jugement par la société ST Microelectronics Grand Ouest, venant aux droits de la société ST-Ericsson France.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 7 avril, 6 juillet et 31 décembre 2015 au secrétariat du contentieux du Conseil d'Etat, la société ST Microelectronics Grand Ouest demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de M. B...la somme de 1 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code du travail ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Pauline Pannier, auditeur,  <br/>
<br/>
              - les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Blancpain, Soltner, Texidor, avocat de la société St Microelectronics Grand Ouest et à la SCP Masse-Dessen, Thouvenin, Coudray, avocat de M. B...;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que l'inspecteur du travail de la 6ème section du Calvados a, par une décision du 23 juin 2011, refusé d'autoriser la société ST-Ericsson France à licencier pour motif économique M.B..., salarié protégé, en fondant sa décision sur quatre motifs distincts, tirés de ce que la procédure d'information et de consultation interne à l'entreprise était irrégulière, de ce que la réalité du motif économique n'était pas établie, de ce que l'employeur n'avait pas satisfait à son obligation de reclassement et de ce que la demande de licenciement présentait un lien avec les mandats exercés par M. B...; que, sur recours hiérarchique de l'employeur, le ministre du travail, de l'emploi et de la santé a, par une décision du 21 décembre 2011, annulé la décision de l'inspecteur du travail et, se prononçant à nouveau sur la demande d'autorisation, s'est déclaré incompétent pour en connaître au motif que M. B...n'avait plus, à cette date, la qualité de salarié protégé ; que le tribunal administratif de Caen a, à la demande de M.B..., annulé cette décision du 21 décembre 2011 ; que la société ST Microelectronics Grand Ouest, venant aux droits de la société ST-Ericsson France, se pourvoit en cassation contre l'arrêt du 5 février 2015 par lequel la cour administrative d'appel de Nantes a rejeté l'appel de la société ST-Ericsson France dirigé contre ce jugement ; <br/>
<br/>
              Sur la régularité de l'arrêt attaqué :<br/>
<br/>
              2. Considérant que, pour rejeter l'appel formé par la société ST-Ericsson France, la cour administrative d'appel de Nantes a relevé que la décision du 21 décembre 2011 du ministre du travail, de l'emploi et de la santé s'était bornée, pour annuler la décision de l'inspecteur du travail, à censurer deux seulement des quatre motifs retenus par ce dernier, sans statuer sur le motif tiré de ce que l'employeur n'avait pas satisfait à son obligation de reclassement ni sur celui tiré de ce que la demande de licenciement présentait un lien avec les mandats exercés par M. B...; <br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier soumis à la cour administrative d'appel que M. B...soutenait dans ses mémoires en défense, enregistrés au greffe de la cour les 27 mai 2013 et 14 mars 2014 et communiqués à la société appelante, que l'absence d'examen par le ministre chargé du travail des deux motifs, tirés respectivement de ce que l'employeur n'avait pas satisfait à son obligation de reclassement et de ce que la demande de licenciement présentait un lien avec ses mandats représentatifs, alors que chacun de ces motifs était à lui seul de nature à fonder la décision de l'inspecteur du travail, entachait sa décision d'illégalité ;<br/>
<br/>
              4. Considérant en premier lieu que, le moyen sur lequel la cour s'est fondée pour confirmer l'illégalité de la décision du 21 décembre 2011 du ministre chargé du travail et rejeter, par suite, l'appel de la société ST-Ericsson France, ayant ainsi été soumis au contradictoire avant l'inscription de l'affaire à la première audience du 20 novembre 2014, la société requérante n'est, en tout état de cause, pas fondée à soutenir qu'en procédant, après cette première audience, à une réouverture de l'instruction et à un réexamen de l'affaire lors d'une audience du 15 janvier 2015, la cour aurait méconnu le principe d'égalité des armes entre les parties garanti par les stipulations de l'article 6-1 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              5. Considérant en deuxième lieu que, ainsi qu'il a été dit ci-dessus, la cour s'est fondée, pour rejeter l'appel de la société  ST-Ericsson France, sur des éléments qui avaient été soumis au débat contradictoire entre les parties avant l'inscription de l'affaire à l'audience du 20 novembre 2014 ; que la société requérante n'est par suite, en tout état de cause, pas fondée à soutenir que l'arrêt serait entaché d'irrégularité au motif, au demeurant non établi en cassation, que le rapporteur public aurait, lors de cette première audience, fait état d'un motif d'illégalité de la décision du ministre qui n'avait pas été soulevé par M. B...;<br/>
<br/>
              6. Considérant, en troisième lieu, que le rapporteur public qui s'est publiquement prononcé sur l'affaire ne peut prendre part au délibéré ; qu'en revanche, aucune disposition ni aucun principe, et notamment pas le principe d'impartialité rappelé par les stipulations de l'article 6-1 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, ne font obstacle à ce qu'un rapporteur public qui a déjà prononcé des conclusions sur une affaire lors d'une première audience publique se prononce à nouveau sur la même affaire si, postérieurement à cette première audience, l'instruction de cette affaire est rouverte et que l'affaire est réinscrite à une audience ultérieure ;<br/>
<br/>
              7. Considérant enfin que la cour, à laquelle n'était demandée la mise en oeuvre d'aucune procédure tendant à la récusation de certains de ses membres ou au renvoi de l'affaire devant une autre juridiction, a pu, sans entacher son arrêt d'insuffisance de motivation, statuer sur la requête après avoir rouvert l'instruction, sans répondre expressément au moyen tiré de ce que cette réouverture de l'instruction aurait été irrégulière ;<br/>
<br/>
              Sur le bien-fondé de l'arrêt attaqué :<br/>
<br/>
              8. Considérant qu'en vertu des dispositions du code du travail, les salariés légalement investis de fonctions représentatives bénéficient, dans l'intérêt de l'ensemble des travailleurs qu'ils représentent, d'une protection exceptionnelle ; que, lorsque le licenciement d'un de ces salariés est envisagé, ce licenciement ne doit pas être en rapport avec les fonctions représentatives normalement exercées ou l'appartenance syndicale de l'intéressé ; que, dans le cas où la demande de licenciement est fondée sur un motif de caractère économique, il appartient à l'inspecteur du travail et, le cas échéant, au ministre, de rechercher si la situation de l'entreprise justifie le licenciement du salarié, en tenant compte notamment de la nécessité des réductions d'effectifs envisagées et de la possibilité d'assurer le reclassement du salarié dans l'entreprise ou au sein du groupe auquel appartient cette dernière ; que l'autorité administrative ne peut légalement faire droit à une telle demande d'autorisation de licenciement que si l'ensemble de ces exigences sont remplies ; que par suite, lorsqu'il est saisi par l'employeur d'un recours hiérarchique contre une décision d'un inspecteur du travail qui a estimé que plusieurs de ces exigences n'étaient pas remplies et qui s'est, par suite, fondé sur plusieurs motifs faisant, chacun, légalement obstacle à ce que le licenciement soit autorisé, le ministre ne peut annuler cette décision que si elle est entachée d'illégalité externe ou si aucun des motifs retenus par l'inspecteur du travail n'est fondé ;<br/>
<br/>
              9. Considérant qu'il ressort des pièces du dossier soumis au juge du fond que, ainsi qu'il a été dit au point 1, la décision du 23 juin 2011 par laquelle l'inspecteur du travail a refusé d'autoriser le licenciement de M. B...était fondée sur quatre motifs distincts ; que chacun de ces motifs, s'il était fondé, faisait obstacle à ce que l'administration puisse légalement autoriser le licenciement demandé ; que, par suite, la cour administrative d'appel n'a pas commis d'erreur de droit en annulant la décision du 21 décembre 2011 du ministre du travail, de l'emploi et de la santé, au motif que celui-ci s'était borné, pour annuler la décision de l'inspecteur du travail, à censurer deux seulement des quatre motifs retenus par ce dernier ;<br/>
<br/>
              10. Considérant qu'il résulte de tout ce qui précède que la société ST Microelectronics Grand Ouest n'est pas fondée à demander l'annulation de l'arrêt attaqué ; que son pourvoi doit dès lors être rejeté, y compris ses conclusions tendant à ce qu'une somme soit mise à la charge de M. B...au titre des dispositions de l'article L. 761-1 du code de justice administrative ; qu'en revanche il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société ST Microelectronics Grand Ouest la somme de 1 000 euros à verser à M. B... au titre des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le pourvoi de la société ST Microelectronics Grand Ouest est rejeté.<br/>
Article 2 : La société ST Microelectronics Grand Ouest versera à M. B...la somme de 1 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la société ST Microelectronics Grand Ouest et à M. A...B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-05-01-03 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - MOTIFS. POUVOIRS ET OBLIGATIONS DE L'ADMINISTRATION. COMPÉTENCE LIÉE. - REFUS DE L'AUTORISATION DE LICENCIEMENT D'UN SALARIÉ PROTÉGÉ EN CAS DE LIEN AVEC LE MANDAT - ABSENCE (SOL.IMPL.) [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">66-07-01-03-04 TRAVAIL ET EMPLOI. LICENCIEMENTS. AUTORISATION ADMINISTRATIVE - SALARIÉS PROTÉGÉS. MODALITÉS DE DÉLIVRANCE OU DE REFUS DE L'AUTORISATION. RECOURS HIÉRARCHIQUE. - CAS OÙ L'INSPECTEUR DU TRAVAIL S'EST FONDÉ SUR PLUSIEURS MOTIFS DE REFUS.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">66-07-01-04-01 TRAVAIL ET EMPLOI. LICENCIEMENTS. AUTORISATION ADMINISTRATIVE - SALARIÉS PROTÉGÉS. CONDITIONS DE FOND DE L'AUTORISATION OU DU REFUS D'AUTORISATION. ILLÉGALITÉ DU LICENCIEMENT EN RAPPORT AVEC LE MANDAT OU LES FONCTIONS REPRÉSENTATIVES. - MOTIF DE REFUS DE L'AUTORISATION DE LICENCIEMENT D'UN SALARIÉ PROTÉGÉ - LIEN AVEC LE MANDAT - COMPÉTENCE LIÉE AU SENS DE LA JURISPRUDENCE MONTAIGNAC - ABSENCE (SOL.IMPL.) [RJ1].
</SCT>
<ANA ID="9A"> 01-05-01-03 L'autorité administrative, lorsqu'elle refuse d'autoriser le licenciement d'un salarié protégé au motif que la demande d'autorisation n'est pas sans lien avec les mandats détenus par le salarié, n'est pas dans une situation de compétence liée au sens de la jurisprudence Montaignac [RJ2]. Dès lors, les moyens d'illégalité externe ou tirés de l'illégalité des autres motifs de la décision sont opérants.</ANA>
<ANA ID="9B"> 66-07-01-03-04 Lorsqu'il est saisi par l'employeur d'un recours hiérarchique contre une décision d'un inspecteur du travail qui a refusé l'autorisation de licenciement en se fondant sur plusieurs motifs de refus faisant, chacun, légalement obstacle à ce que le licenciement soit autorisé, le ministre ne peut annuler cette décision que si elle est entachée d'illégalité externe ou si aucun des motifs retenus par l'inspecteur du travail n'est fondé.</ANA>
<ANA ID="9C"> 66-07-01-04-01 L'autorité administrative, lorsqu'elle refuse d'autoriser le licenciement d'un salarié protégé au motif que la demande d'autorisation n'est pas sans lien avec les mandats détenus par le salarié, n'est pas dans une situation de compétence liée au sens de la jurisprudence Montaignac [RJ2]. Dès lors, les moyens d'illégalité externe ou tirés de l'illégalité des autres motifs de la décision sont opérants.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Ab. jur. CE, 16 juin 1995, S.A. Soubitez, n° 139337, T. p. 635., ,[RJ2] Cf. CE, Section, M. Montaignac, 3 février 1999, n° 149722, p. 6.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
