<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000008257277</ID>
<ANCIEN_ID>JG_L_2006_05_000000266495</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/08/25/72/CETATEXT000008257277.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère / 6ème SSR, 15/05/2006, 266495, Publié au recueil Lebon</TITRE>
<DATE_DEC>2006-05-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>266495</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère / 6ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>HAAS ; SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Luc  Derepas</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Stahl</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2006:266495.20060515</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 13 avril et 12 août 2004 au secrétariat du contentieux du Conseil d'Etat, présentés pour la COMMUNE DE FAYET (02100), représentée par son maire ; la COMMUNE DE FAYET demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt du 12 février 2004 par lequel la cour administrative d'appel de Douai, réformant le jugement du 13 juin 2002 du tribunal administratif d'Amiens, l'a condamnée à verser à la société Fidéicomi la somme de 163 882,69 euros ;<br/>
<br/>
              2°) statuant au fond, d'annuler le jugement du 13 juin 2002 du tribunal administratif d'Amiens et de rejeter les conclusions indemnitaires de la société Fidéicomi ;<br/>
<br/>
              3°) de mettre à la charge de la société Fidéicomi le versement de la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le  code de l'urbanisme ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Luc Derepas, Maître des Requêtes,  <br/>
<br/>
              - les observations de Me Haas, avocat de la COMMUNE DE FAYET et de la SCP Piwnica, Molinié, avocat de la société Compagnie foncière Fidéicomi, <br/>
<br/>
              - les conclusions de M. Jacques-Henri Stahl, Commissaire du gouvernement ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Sur le principe de la responsabilité de la COMMUNE DE FAYET :<br/>
<br/>
              Considérant qu'en énonçant qu'il résultait de l'instruction que la décision de préemption avait pour seul objet d'empêcher la transformation de l'immeuble en cause en centre d'hébergement pour personnes sans domicile fixe, la cour, qui a au surplus constaté que la décision de préemption litigieuse était entachée d'une autre illégalité, a suffisamment motivé son arrêt eu égard à l'argumentation des parties et n'a pas dénaturé les pièces qui lui étaient soumises ;<br/>
<br/>
              Considérant qu'en jugeant que la décision de préemption litigieuse était entachée d'illégalité au regard des dispositions de l'article L. 210-1 du code de l'urbanisme faute d'être justifiée par un projet suffisamment certain et précis, la cour n'a commis aucune erreur de droit ;<br/>
<br/>
              Considérant qu'il résulte de ce qui précède que la COMMUNE DE FAYET n'est pas fondée à demander l'annulation de l'arrêt attaqué en tant qu'il a jugé cette commune responsable du préjudice subi par la société Fidéicomi ;<br/>
<br/>
              Sur le préjudice :<br/>
<br/>
              Considérant qu'à l'issue d'une procédure de préemption qui n'a pas abouti, le propriétaire du bien en cause peut, si la décision de préemption est entachée d'illégalité, obtenir réparation du préjudice qui lui a causé de façon directe et certaine cette illégalité ; que lorsque le propriétaire a cédé le bien après renonciation de la collectivité, son préjudice résulte en premier lieu, dès lors que les termes de la promesse de vente initiale faisaient apparaître que la réalisation de cette vente était probable, de la différence entre le prix figurant dans cet acte et la valeur vénale du bien à la date de la décision de renonciation ; que pour l'évaluation de ce préjudice, le prix de vente effectif peut être regardé comme exprimant cette valeur vénale si un délai raisonnable sépare la vente de la renonciation, eu égard aux diligences effectuées par le vendeur, et sous réserve que ce prix de vente ne s'écarte pas anormalement de cette valeur vénale ;<br/>
<br/>
              Considérant que le propriétaire placé dans la situation indiquée ci-dessus subit un autre préjudice qui résulte, lorsque la vente initiale était suffisamment probable, de l'impossibilité dans laquelle il s'est trouvé de disposer du prix figurant dans la promesse de vente entre la date de cession prévue par cet acte et la date de vente effective, dès lors que cette dernière a eu lieu dans un délai raisonnable après la renonciation de la collectivité ; qu'en revanche, lorsque la vente a eu lieu dans un délai ne correspondant pas aux diligences attendues d'un propriétaire désireux de vendre rapidement son bien, quelles qu'en soient les raisons, le terme à prendre en compte pour l'évaluation de ce préjudice doit être fixé à la date de la décision de renonciation ;<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par un arrêt du 23 novembre 1996, la cour d'appel d'Amiens a estimé que le montant de 1 750 000 F qui figurait dans la promesse de vente passée entre la société Fidéicomi et la commune de Saint-Quentin correspondait à la valeur vénale du bien préempté par la COMMUNE DE FAYET ; qu'après que le maire de Fayet a, par lettre du 20 janvier 1997, informé la société Fidéicomi que la commune renonçait à acquérir le bien, cette société l'a cédé, par acte authentique du 5 mars 1999, au prix de 675 000 F ; qu'en jugeant, dans ces conditions, que le préjudice causé à la société par la décision de préemption illégale de la COMMUNE DE FAYET équivalait à la différence entre 1 750 000 F et 675 000 F, alors que la société Fidéicomi ne faisait état d'aucune circonstance l'ayant empêchée de céder le bien dans un délai raisonnable après la décision de renonciation à un prix correspondant à la valeur vénale, la cour a entaché son arrêt d'une erreur de droit ; que cet arrêt doit, pour ce motif, être annulé en tant qu'il a évalué le préjudice subi par la société Fidéicomi ;<br/>
<br/>
              Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de la COMMUNE DE FAYET, qui n'est pas la partie perdante dans la présente instance, la somme que la société Compagnie Foncière Fidéicom, venant aux droits de la société Fidéicomi, demande au titre des frais exposés par elle et non compris dans les dépens ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société Compagnie Foncière Fidéicom la somme de 3 000 euros sur ce fondement ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 12 février 2004 de la cour administrative d'appel de Douai est annulé en tant qu'il évalue le préjudice subi par la société Fidéicomi.<br/>
Article 2 : L'affaire est renvoyée dans cette mesure à la cour administrative d'appel de Douai.<br/>
Article 3 : La société Compagnie Foncière Fidéicomi versera la somme de 3 000 euros à la COMMUNE DE FAYET en application de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions présentées par la société Compagnie Foncière Fidéicomi sur le fondement de l'article L. 761-1 du code de justice administrative  sont rejetées. <br/>
Article 5 : Le surplus des conclusions présentées par la COMMUNE DE FAYET est rejeté.<br/>
Article 6 : La présente décision sera notifiée à la COMMUNE DE FAYET, à la société Compagnie foncière Fidéicomi et au ministre des transports, de l'équipement, du tourisme et de la mer.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">60-04-01 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RÉPARATION. PRÉJUDICE. - CONSISTANCE DU PRÉJUDICE DONT UN PROPRIÉTAIRE PEUT RÉCLAMER RÉPARATION EN CAS D'ILLÉGALITÉ D'UNE DÉCISION DE PRÉEMPTION À LAQUELLE LA COMMUNE A RENONCÉ COMPTE TENU DU PRIX FIXÉ PAR LA JURIDICTION JUDICIAIRE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">68-02-01-01-01 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PROCÉDURES D'INTERVENTION FONCIÈRE. PRÉEMPTION ET RÉSERVES FONCIÈRES. DROITS DE PRÉEMPTION. DROIT DE PRÉEMPTION URBAIN. - COMMUNE RENONÇANT À EXERCER SON DROIT DE PRÉEMPTION COMPTE TENU DU PRIX FIXÉ PAR LA JURIDICTION JUDICIAIRE - CONSISTANCE DU PRÉJUDICE DONT LE PROPRIÉTAIRE PEUT RÉCLAMER RÉPARATION EN CAS D'ILLÉGALITÉ DE LA DÉCISION DE PRÉEMPTION.
</SCT>
<ANA ID="9A"> 60-04-01 A l'issue d'une procédure de préemption qui n'a pas abouti, le propriétaire du bien en cause peut, si la décision de préemption est entachée d'illégalité, obtenir réparation du préjudice que lui a causé de façon directe et certaine cette illégalité. Lorsque le propriétaire a cédé le bien après renonciation de la collectivité, son préjudice résulte en premier lieu, dès lors que les termes de la promesse de vente initiale faisaient apparaître que la réalisation de cette vente était probable, de la différence entre le prix figurant dans cet acte et la valeur vénale du bien à la date de la décision de renonciation. Pour l'évaluation de ce préjudice, le prix de vente effectif peut être regardé comme exprimant cette valeur vénale si un délai raisonnable sépare la vente de la renonciation, eu égard aux diligences effectuées par le vendeur, et sous réserve que ce prix de vente ne s'écarte pas anormalement de cette valeur vénale.,,Le propriétaire placé dans la situation indiquée ci-dessus subit un autre préjudice qui résulte, lorsque la vente initiale était suffisamment probable, de l'impossibilité dans laquelle il s'est trouvé de disposer du prix figurant dans la promesse de vente entre la date de cession prévue par cet acte et la date de vente effective, dès lors que cette dernière a eu lieu dans un délai raisonnable après la renonciation de la collectivité. En revanche, lorsque la vente a eu lieu dans un délai ne correspondant pas aux diligences attendues d'un propriétaire désireux de vendre rapidement son bien, quelles qu'en soient les raisons, le terme à prendre en compte pour l'évaluation de ce préjudice doit être fixé à la date de la décision de renonciation.</ANA>
<ANA ID="9B"> 68-02-01-01-01 A l'issue d'une procédure de préemption qui n'a pas abouti, le propriétaire du bien en cause peut, si la décision de préemption est entachée d'illégalité, obtenir réparation du préjudice que lui a causé de façon directe et certaine cette illégalité. Lorsque le propriétaire a cédé le bien après renonciation de la collectivité, son préjudice résulte en premier lieu, dès lors que les termes de la promesse de vente initiale faisaient apparaître que la réalisation de cette vente était probable, de la différence entre le prix figurant dans cet acte et la valeur vénale du bien à la date de la décision de renonciation. Pour l'évaluation de ce préjudice, le prix de vente effectif peut être regardé comme exprimant cette valeur vénale si un délai raisonnable sépare la vente de la renonciation, eu égard aux diligences effectuées par le vendeur, et sous réserve que ce prix de vente ne s'écarte pas anormalement de cette valeur vénale.,,Le propriétaire placé dans la situation indiquée ci-dessus subit un autre préjudice qui résulte, lorsque la vente initiale était suffisamment probable, de l'impossibilité dans laquelle il s'est trouvé de disposer du prix figurant dans la promesse de vente entre la date de cession prévue par cet acte et la date de vente effective, dès lors que cette dernière a eu lieu dans un délai raisonnable après la renonciation de la collectivité. En revanche, lorsque la vente a eu lieu dans un délai ne correspondant pas aux diligences attendues d'un propriétaire désireux de vendre rapidement son bien, quelles qu'en soient les raisons, le terme à prendre en compte pour l'évaluation de ce préjudice doit être fixé à la date de la décision de renonciation.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
