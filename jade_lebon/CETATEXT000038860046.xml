<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038860046</ID>
<ANCIEN_ID>JG_L_2019_07_000000411984</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/86/00/CETATEXT000038860046.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 31/07/2019, 411984, Publié au recueil Lebon</TITRE>
<DATE_DEC>2019-07-31</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>411984</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>M. Fabio Gennari</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Guillaume Odinet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:411984.20190731</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire, un mémoire complémentaire, un mémoire en réplique et un nouveau mémoire, enregistrés les 29 juin et 29 septembre 2017, les 23 mars et 5 juillet 2018 au secrétariat du contentieux du Conseil d'Etat, M. A...E...et M. D...C..., agissant en leur nom propre et au nom des deux enfants mineurs, J...I...C...E...et K...F...A...C...E..., demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision du 2 mai 2017 par laquelle le ministre de l'intérieur a explicitement refusé de modifier le décret du 25 avril 2017 portant naturalisation de M. E...pour y porter le nom de ses deux enfants J... I... et K... F... A... ; <br/>
<br/>
              2°) d'enjoindre au Premier ministre de modifier le décret du 25 avril 2017 pour y porter les noms des deux enfants ou, à défaut, de réexaminer sa demande dans un délai d'un mois à compter de la décision à intervenir sous astreinte de 75 euros par jour de retard ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - la convention des Nations-Unies sur les droits de l'enfant, signée à New York le 26 janvier 1990 ;<br/>
              - le code civil ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Fabio Gennari, auditeur, <br/>
<br/>
              - les conclusions de M. Guillaume Odinet, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de M. E...et de M. C...;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. M. A...E..., de nationalité australienne, a présenté en juin 2015 une demande d'acquisition de la nationalité française par naturalisation. Il a également demandé le bénéfice de l'effet collectif attaché à l'acquisition de la nationalité française au profit de l'enfant J... I... C...E..., puis, dans le courant de l'instruction de sa demande, au profit de l'enfant K... F... A... C...E.... Un décret en date du 25 avril 2017 a naturalisé M. A...E...sans mention des deux enfants. M. E...et M.C..., son époux, demandent l'annulation pour excès de pouvoir de la décision en date du 2 mai 2017 par laquelle le ministre de l'intérieur a explicitement rejeté leur demande tendant à ce que soit accordé aux enfants le bénéfice de l'effet collectif attaché à l'acquisition de la nationalité française et que le décret de naturalisation soit modifié en ce sens.<br/>
<br/>
              2. D'une part, aux termes de l'article 21-15 du code civil : " (...) l'acquisition de la nationalité française par décision de l'autorité publique résulte d'une naturalisation accordée par décret à la demande de l'étranger ". L'article 22-1 du même code dispose que : " L'enfant mineur dont l'un des deux parents acquiert la nationalité française, devient français de plein droit s'il a la même résidence habituelle que ce parent ou s'il réside alternativement avec ce parent dans le cas de séparation ou divorce. Les dispositions du présent article ne sont applicables à l'enfant d'une personne qui acquiert la nationalité française par décision de l'autorité publique ou par déclaration de nationalité que si son nom est mentionné dans le décret ou dans la déclaration ". D'autre part, aux termes de l'article 47 du même code : " Tout acte de l'état civil des Français et des étrangers fait en pays étranger et rédigé dans les formes usitées dans ce pays fait foi, sauf si d'autres actes ou pièces détenus, des données extérieures ou des éléments tirés de l'acte lui-même établissent, le cas échéant après toutes vérifications utiles, que cet acte est irrégulier, falsifié ou que les faits qui y sont déclarés ne correspondent pas à la réalité ". Aux termes de l'article 16-7 du code civil, figurant au chapitre II, intitulé " Du respect du corps humain ", du titre Ier du livre Ier de ce code : " Toute convention portant sur la procréation ou la gestation pour le compte d'autrui est nulle ". Ces dispositions présentent, en vertu de l'article 16-9 du même code, un caractère d'ordre public.<br/>
<br/>
              3. L'effet qui s'attache, au bénéfice des enfants mineurs, en vertu des dispositions citées ci-dessus de l'article 22-1 du code civil, à l'acquisition de la nationalité française par l'un des parents est subordonné notamment à la preuve de l'existence d'un lien de filiation avec ce parent, susceptible de produire légalement des effets en France. <br/>
<br/>
              4. Il ressort des pièces du dossier que, dans le cadre de conventions de gestation pour autrui conclues dans l'Etat du Colorado (États-Unis d'Amérique), M. A...E..., ressortissant australien, et M. D...C..., son époux, sont devenus les parents de deux enfants, J... I... et K... L... A..., nés respectivement le 27 avril 2014 et le 7 mars 2016 dans cet Etat. La filiation des enfants a été déclarée avant leur naissance par une ordonnance de parenté rendue par le juge américain qui prévoyait que l'enfant devrait se voir délivrer un certificat de naissance indiquant que M. E...et M. C...sont légalement les pères de l'enfant et que ce certificat ne devrait comporter aucun nom pour la mère. Des certificats de naissance ainsi rédigés ont été délivrés par les autorités américaines. Les deux enfants résident en France avec M. E...et M.C....<br/>
<br/>
              5. Si le ministre chargé des naturalisations pouvait, dans l'exercice du large pouvoir d'appréciation dont il dispose en la matière, refuser de faire droit à la demande de naturalisation de M. E...en prenant en considération la circonstance que celui-ci avait eu recours à la gestation pour le compte d'autrui, prohibée en France par les dispositions citées ci-dessus du code civil, une telle circonstance ne pouvait en revanche, alors qu'il n'est pas soutenu que les actes d'état civil des deux enfants, établis selon la loi applicable aux faits dans l'Etat du Colorado, seraient entachés de fraude ou ne seraient pas conformes à cette loi, conduire à priver ces enfants de l'effet qui s'attache en principe, en vertu de l'article 22-1 du code civil, à la décision de naturaliser M.E..., sans qu'il soit porté une atteinte disproportionnée à ce qu'implique, en termes de nationalité, le droit au respect de leur vie privée, garanti par l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales.<br/>
<br/>
              6. Il résulte de ce qui précède que MM. E...et C...sont fondés, sans qu'il soit besoin d'examiner les autres moyens de la requête, à demande l'annulation pour excès de pouvoir de la décision du 2 mai 2017.<br/>
<br/>
              7. L'exécution de la présente décision implique nécessairement que le décret du 25 avril 2017 accordant la nationalité française à M. E...soit modifié pour y porter le nom des enfants J... I... et K... L... A.... Dès lors, il y a lieu d'enjoindre au ministre de l'intérieur de proposer au Premier ministre de modifier ainsi ce décret dans un délai de deux mois à compter de la notification de la présente décision. Il n'y pas lieu d'assortir cette injonction d'une astreinte.<br/>
<br/>
              8. Il y a lieu, dans les circonstances de l'espèce de mettre à la charge de l'Etat une somme de 3 000 euros à verser à MM. E...et C...sur le fondement de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>
              				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : La décision du 2 mai 2017 par laquelle le ministre d'Etat, ministre de l'intérieur a refusé de modifier le décret du 25 avril 2017 portant naturalisation de M. E... est annulée.<br/>
Article 2 : Il est enjoint au ministre de l'intérieur de proposer au Premier ministre de modifier le décret du 25 avril 2017 accordant à M. E...la nationalité française, pour y porter le nom des enfants J... I... et K... L... A..., dans un délai de deux mois à compter de la notification de la présente décision.<br/>
Article 3 : L'Etat versera une somme unique de 3 000 euros à M. E...et M. C... sur le fondement de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à M. A... E..., premier dénommé et au ministre de l'intérieur. Copie en sera adressée au Premier ministre.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">26-01-01-01 DROITS CIVILS ET INDIVIDUELS. ÉTAT DES PERSONNES. NATIONALITÉ. ACQUISITION DE LA NATIONALITÉ. - ENFANT NÉ À L'ÉTRANGER DANS LE CADRE D'UNE CONVENTION DE GESTATION POUR AUTRUI (GPA) - CIRCONSTANCE SUSCEPTIBLE DE JUSTIFIER LE REFUS DE NATURALISER LE PARENT ÉTRANGER - EXISTENCE - CIRCONSTANCE SUSCEPTIBLE DE PRIVER L'ENFANT DU BÉNÉFICE DE L'EFFET QUI S'ATTACHE À LA DÉCISION DE NATURALISER L'UN DE SES PARENTS (ART. 22 DU CODE CIVIL) - ABSENCE [RJ1], SI LES ACTES D'ÉTAT CIVIL DE L'ENFANT NE SONT PAS ENTACHÉS DE FRAUDE ET SONT CONFORMES À LA LOI DE L'ETAT QUI LES A ÉTABLIS.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">26-055-01-08-02 DROITS CIVILS ET INDIVIDUELS. CONVENTION EUROPÉENNE DES DROITS DE L'HOMME. DROITS GARANTIS PAR LA CONVENTION. DROIT AU RESPECT DE LA VIE PRIVÉE ET FAMILIALE (ART. 8). VIOLATION. - REFUS D'ACCORDER À UN ENFANT, AU SEUL MOTIF QU'IL EST NÉ À L'ÉTRANGER DANS LE CADRE D'UNE CONVENTION DE GESTATION POUR AUTRUI (GPA), LE BÉNÉFICE DE L'EFFET COLLECTIF ATTACHÉ À LA NATURALISATION D'UN DE SES PARENTS [RJ1].
</SCT>
<ANA ID="9A"> 26-01-01-01 L'effet qui s'attache, au bénéfice des enfants mineurs, en vertu de l'article 22-1 du code civil, à l'acquisition de la nationalité française par l'un des parents est subordonné notamment à la preuve de l'existence d'un lien de filiation avec ce parent, susceptible de produire légalement des effets en France.... ,,Si le ministre chargé des naturalisations pouvait, dans l'exercice du large pouvoir d'appréciation dont il dispose en la matière, refuser de faire droit à la demande de naturalisation du requérant en prenant en considération la circonstance que celui-ci avait eu recours à la gestation pour le compte d'autrui, prohibée en France par l'article 16-7 du code civil, une telle circonstance ne pouvait en revanche, alors qu'il n'est pas soutenu que les actes d'état civil des deux enfants, établis selon la loi applicable aux faits dans l'Etat du Colorado, seraient entachés de fraude ou ne seraient pas conformes à cette loi, conduire à priver ces enfants de l'effet qui s'attache en principe, en vertu de l'article 22-1 du code civil, à la décision de naturaliser le requérant, sans qu'il soit porté une atteinte disproportionnée à ce qu'implique, en termes de nationalité, le droit au respect de leur vie privée, garanti par l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales.</ANA>
<ANA ID="9B"> 26-055-01-08-02 Si le ministre chargé des naturalisations pouvait, dans l'exercice du large pouvoir d'appréciation dont il dispose en la matière, refuser de faire droit à la demande de naturalisation du requérant en prenant en considération la circonstance que celui-ci avait eu recours à la gestation pour le compte d'autrui, prohibée en France par l'article 16-7 du code civil, une telle circonstance ne pouvait en revanche, alors qu'il n'est pas soutenu que les actes d'état civil des deux enfants, établis selon la loi applicable aux faits dans l'Etat du Colorado, seraient entachés de fraude ou ne seraient pas conformes à cette loi, conduire à priver ces enfants de l'effet qui s'attache en principe, en vertu de l'article 22-1 du code civil, à la décision de naturaliser le requérant, sans qu'il soit porté une atteinte disproportionnée à ce qu'implique, en termes de nationalité, le droit au respect de leur vie privée, garanti par l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr. CE, 12 décembre 2014, Association juristes pour l'enfance et autres, n°s 365779 367324 366989 366710 367317 368861, p. 382.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
