<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039648659</ID>
<ANCIEN_ID>JG_L_2019_12_000000427639</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/64/86/CETATEXT000039648659.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 6ème chambres réunies, 19/12/2019, 427639</TITRE>
<DATE_DEC>2019-12-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>427639</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Louise Cadin</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2019:427639.20191219</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 4 février et 23 septembre 2019 au secrétariat du contentieux du Conseil d'Etat, la société Betclic Enterprises Limited demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2018-1076 du 3 décembre 2018 relatif aux modalités de liquidation et de recouvrement du montant des avoirs des joueurs en déshérence dû à l'Etat par les opérateurs de jeux en ligne agréés par l'Autorité de régulation des jeux en ligne et par la Française des jeux au titre de la loterie en ligne ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et son premier protocole additionnel ;<br/>
              - la directive 2015/1535/CE du 9 septembre 2015 du Parlement européen et du Conseil ;<br/>
              -le code civil ;<br/>
              - la loi n° 2010-476 du 12 mai 2010 ;<br/>
              - la loi n° 2015-1785 du 29 décembre 2015 ;<br/>
              - le décret n° 2010-509 du 18 mai 2010 ;<br/>
              - le décret n°2010-518 du 19 mai 2010 ;<br/>
              - le décret n° 2015-620 du 5 juin 2015 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme B... A..., auditrice,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. L'article 17 de la loi du 12 mai 2010 relative à l'ouverture à la concurrence et à la régulation du secteur des jeux d'argent et de hasard en ligne a prévu que l'opérateur agréé de jeux ou de paris en ligne, sans attendre la vérification des éléments qui doivent être fournis par le joueur relatifs à son identité, son âge, son adresse et ses coordonnées bancaires, peut proposer à celui-ci l'ouverture d'un compte-joueur provisoire qu'il peut créditer en vue de ses activités de jeu ou de pari.<br/>
<br/>
              2. Pour l'application de ces dispositions, l'article 8 du décret du 19 mai 2010 relatif à la mise à disposition de l'offre de jeux et de paris par les opérateurs agréés de jeux ou de paris en ligne dispose, dans sa rédaction en vigueur jusqu'au 6 juin 2015 : " L'opérateur clôturant un compte joueur provisoire informe le joueur du motif de cette clôture. / Si le compte est créditeur, l'opérateur met en réserve sans délai la somme correspondante, pour une durée de cinq ans à compter de la clôture du compte. Durant cette période, et sans préjudice de l'application de l'article L. 561-16 du code monétaire et financier dans les conditions prévues à l'article 9, le titulaire du compte peut obtenir le versement du montant du solde créditeur en communiquant à l'opérateur les pièces exigées à l'article 4, sauf si ces pièces permettent d'établir qu'il n'était pas autorisé à jouer au moment où le compte provisoire était actif ". Le décret du 5 juin 2015 relatif aux obligations imposées aux opérateurs agréés de jeux ou de paris en ligne, qui est entré en vigueur le 6 juin suivant, a modifié ces dispositions en portant de cinq à six ans la durée pendant laquelle les opérateurs de jeux et paris en ligne doivent mettre en réserve les sommes figurant sur les comptes-joueurs provisoires qui ont été ouverts auprès d'eux, lorsqu'ils les ont clôturés.<br/>
<br/>
              3. Enfin, l'article 50 de la loi du 29 décembre 2015 de finances pour 2016 a ajouté à l'article 17 de la loi du 12 mai 2010 des dispositions qui, d'une part, reprennent celles citées ci-dessus du décret du 19 mai 2010 en prévoyant que, lorsqu'un opérateur agréé de jeux ou de paris en ligne clôture un compte-joueur provisoire présentant un solde créditeur, il met en réserve la somme correspondante pendant six années durant lesquelles le joueur peut, à certaines conditions, obtenir le reversement de cette somme et qui, d'autre part, prévoient que lorsque, à l'issue des six années, la somme n'a pas été reversée au joueur, elle est acquise à l'Etat, selon des modalités précisées par décret en Conseil d'Etat.<br/>
<br/>
              4. Le décret attaqué du 3 décembre 2018 a été pris pour l'application de ces dernières dispositions, dont il permet l'entrée en vigueur. Il prévoit ainsi les modalités selon lesquelles les opérateurs de jeux et de paris en ligne déclarent à l'Etat les sommes qui figurent, au terme de six années de mise en réserve, sur leurs comptes-joueurs clôturés, ainsi que les modalités de reversement de ces sommes. Il dispose que les sommes mises en réserve, du fait de la clôture d'un compte-joueur provisoire, au cours des années 2010 à 2015, dès lors qu'elles ont été conservées durant au moins six années et sont, par suite, acquises à l'Etat, devront être déclarées et reversées en 2022 et que les sommes mises en réserve pour le même motif à compter du 1er janvier 2016, si elles sont conservées durant six ans, seront déclarées et reversées à l'Etat selon une périodicité annuelle, à partir de l'année 2023. Les opérateurs devront alors déclarer chaque année avant le 15 février le montant total des sommes mises en réserve au cours de la septième année précédente et qui n'ont pu être reversées aux joueurs et l'Autorité de régulation des jeux en ligne émettra, au plus tard le 31 mars de chaque année, un bordereau détaillant le montant des sommes à reverser au plus tard le 15 mai à la caisse du comptable public.<br/>
<br/>
              5. En premier lieu, il résulte de l'article 50 de la loi de finances pour 2016, éclairés par les travaux préparatoires dont elle est issue, que le principe du reversement à l'Etat des sommes mises en réserve lors de la clôture d'un compte-joueur et non réclamées au bout de six ans, tel qu'il est désormais inscrit à l'article 17 de la loi du 12 mai 2010, est applicable à toutes les sommes mises en réserve à ce titre, y compris celles qui l'ont été à compter de l'année 2010 en application des dispositions, citées au point 2, du décret du 19 mai 2010. La société requérante ne saurait, par suite, utilement soutenir qu'en ayant prévu, conformément à ces dispositions, le reversement des sommes figurant sur les comptes clôturés entre 2010 et 2015, le décret qu'elle attaque porterait atteinte au droit de propriété ou à la liberté d'entreprendre des opérateurs de jeux ou de paris en ligne, ou au droit de propriété des joueurs. Pour les mêmes raisons, elle ne saurait utilement soutenir que le décret attaqué est entaché, sur ce point, d'une rétroactivité illégale ou qu'il méconnaît, en raison de cette rétroactivité alléguée, le principe de sécurité juridique. Enfin, la loi ayant, ainsi qu'il a été dit, prévu le reversement à l'Etat de l'intégralité des sommes portées par les joueurs au crédit de leurs comptes, la société requérante ne saurait non plus utilement soutenir qu'en n'ayant pas prévu la déduction des sommes prélevées sur ces comptes par les opérateurs au titre de " frais de gestion " ou " frais de clôture ", le décret attaqué méconnaîtrait la liberté contractuelle.<br/>
<br/>
              6. En deuxième lieu, si, ainsi qu'il a été dit au point 4, le décret attaqué fait obligation aux opérateurs, à partir de l'année 2023, de déclarer chaque année avant le 15 février le montant total des sommes mises en réserve " au cours de la septième année précédente " et qui n'ont pu être reversées aux joueurs, ces dispositions visent précisément à établir, en remontant à la septième année précédant celle de la déclaration, celles des sommes qui n'ont pas été réclamées six ans après la clôture du compte. La société requérante n'est, par suite, pas fondée à soutenir qu'elles auraient pour effet de faire passer de six à sept années la durée au terme de laquelle les sommes non réclamées deviennent propriété de l'Etat.<br/>
<br/>
              7. En troisième lieu, la société requérante n'est pas fondée à soutenir qu'en ne prévoyant ni frais de garde ni frais de clôture des comptes et en imposant aux opérateurs agréés d'informer les joueurs, dans les trois mois précédant leur reversement à l'Etat, de la possibilité d'obtenir le remboursement des sommes figurant leur compte, le décret attaqué aurait porté atteinte à la liberté d'entreprendre de ces opérateurs.<br/>
<br/>
              8. En quatrième lieu, le 1 de l'article 5 de la directive 2015/1535 du Parlement européen et du Conseil du 9 septembre 2015 prévoyant une procédure d'information dans le domaine des réglementations techniques et des règles relatives aux services de la société de l'information dispose que : " Sous réserve de l'article 7, les États membres communiquent immédiatement à la Commission tout projet de règle technique (...) ". Il résulte clairement de ces dispositions que, pour leur application à une règle technique résultant, en droit interne, de la combinaison de dispositions de nature législative et de dispositions d'application de nature réglementaire, la communication à la Commission européenne des dispositions législatives relatives à cette règle technique peut n'être effectuée qu'au stade de l'élaboration des mesures réglementaires qui en fixent les conditions d'application, soit lorsque l'application de la loi est manifestement impossible en l'absence de ces mesures réglementaires et que, par suite, l'adoption de ces dernières conditionne l'entrée en vigueur de la règle technique, soit lorsque le texte législatif ne détermine pas, à lui seul, la règle technique d'une manière suffisamment précise pour que ses effets puissent être évalués par la Commission européenne et les Etats membres de l'Union européenne.<br/>
<br/>
              9. Il ressort des pièces du dossier que la mise en oeuvre de l'obligation de déclaration et de reversement des sommes qui figurent sur des comptes-joueurs clôturés depuis six ans était manifestement impossible sans que ses modalités soient fixées par le décret litigieux. La publication de ce dernier était, par suite, nécessaire à l'entrée en vigueur des dispositions de l'article 50 de la loi de finances pour 2016. Par suite, il résulte de ce qui a été dit ci-dessus que la notification de ces dispositions législatives à la Commission européenne le 31 juillet 2018, soit en même temps que celle des dispositions du décret litigieux, n'a, en tout état de cause, pas méconnu le principe de communication immédiate à la Commission de tout projet de règle technique, fixé par l'article 5 de la directive du 9 septembre 2015. La société requérante n'est, par suite, pas fondée à soutenir que le décret litigieux serait illégal au motif qu'il aurait été pris sur le fondement de dispositions législatives prises en violation de la directive du 9 septembre 2015.<br/>
<br/>
              10. En cinquième lieu, si l'article 10 du décret du 18 mai 2010 relatif aux obligations imposées aux opérateurs agréés de jeux ou de paris en ligne en vue du contrôle des données de jeux par l'Autorité de régulation des jeux en ligne fait obligation aux opérateurs de jeux et paris en ligne de supprimer, à l'expiration d'un délai de six ans à compter de la clôture d'un compte, toutes les données personnelles concernant le joueur et les opérations effectuées par lui, il ne fait pas obstacle à ce que ces mêmes opérateurs restent en mesure de déclarer le montant des sommes mises en réserve sur ce compte. La société requérante n'est, par suite, pas fondée à soutenir que l'obligation de déclaration de ces sommes imposée par le décret litigieux serait illégale en raison de sa contradiction avec l'obligation de suppression des données.<br/>
<br/>
              11. Enfin, si les dispositions du décret du 19 mai 2010, citées au point 2, se bornaient à créer une obligation de conserver pendant une certaine durée les sommes figurant sur des comptes-joueurs clôturés, sans avoir prévu le reversement à l'Etat des sommes non réclamées à l'issue de cette durée et si la société requérante soutient que les opérateurs pouvaient, en conséquence, escompter en devenir propriétaire en application des dispositions de l'article 2224 du code civil et L. 110-4 du code de commerce relatives à la prescription des créances, l'introduction de la règle du reversement à l'Etat par l'article 50 de la loi de finances pour 2016 ne saurait être regardée, ni comme ayant mis fin à la propriété de ces sommes au terme du délai de conservation, ni même comme ayant mis fin à une espérance légitime d'en devenir propriétaire. La requérante n'est, par suite, pas fondée à soutenir que ces dispositions législatives sont incompatibles avec les stipulations de l'article 1er du premier protocole additionnel à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales.<br/>
<br/>
              12. Il résulte de tout ce qui précède que la société Betclic Enterprises Limited n'est pas fondée à demander l'annulation du décret qu'elle attaque. Sa requête doit ainsi être rejetée, y compris, par voie de conséquence, les conclusions qu'elle présente au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
      Article 1er : La requête de la société Betclic Enterprises Limited est rejetée.<br/>
Article 2 : La présente décision sera notifiée à la société Betclic Enterprises Limited, au Premier ministre, au ministre de l'action et des comptes publics et à l'Autorité de régulation des jeux en ligne.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-03 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - FORME ET PROCÉDURE. - OBLIGATION DE COMMUNICATION À LA COMMISSION EUROPÉENNE, PRÉALABLEMENT À LEUR ADOPTION, DES DISPOSITIONS ÉDICTANT DES RÈGLES TECHNIQUES (ART. 5.1 DE LA DIRECTIVE 2015/1535 DU 9 SEPTEMBRE 2015) - HYPOTHÈSE D'UNE RÈGLE TECHNIQUE RÉSULTANT DE LA COMBINAISON DE DISPOSITIONS DE NATURE LÉGISLATIVE ET DE DISPOSITIONS D'APPLICATION DE NATURE RÉGLEMENTAIRE - 1) POSSIBILITÉ DE COMMUNIQUER AU SEUL STADE DE L'ÉLABORATION DES MESURES RÉGLEMENTAIRES - EXISTENCE, LORSQUE L'APPLICATION DE LA LOI EST MANIFESTEMENT IMPOSSIBLE EN L'ABSENCE DE CES MESURES RÉGLEMENTAIRES OU LORSQUE LE TEXTE LÉGISLATIF NE DÉTERMINE PAS LA RÈGLE TECHNIQUE D'UNE MANIÈRE SUFFISAMMENT PRÉCISE [RJ1] - 2) ILLUSTRATION - LOI INAPPLICABLE EN L'ABSENCE DE MESURES RÉGLEMENTAIRES.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">15-05-01-04 COMMUNAUTÉS EUROPÉENNES ET UNION EUROPÉENNE. RÈGLES APPLICABLES. LIBERTÉS DE CIRCULATION. LIBRE PRESTATION DE SERVICES. - OBLIGATION DE COMMUNICATION À LA COMMISSION EUROPÉENNE, PRÉALABLEMENT À LEUR ADOPTION, DES DISPOSITIONS ÉDICTANT DES RÈGLES TECHNIQUES (ART. 5.1 DE LA DIRECTIVE 2015/1535 DU 9 SEPTEMBRE 2015) - HYPOTHÈSE D'UNE RÈGLE TECHNIQUE RÉSULTANT DE LA COMBINAISON DE DISPOSITIONS DE NATURE LÉGISLATIVE ET DE DISPOSITIONS D'APPLICATION DE NATURE RÉGLEMENTAIRE - 1) POSSIBILITÉ DE COMMUNIQUER AU SEUL STADE DE L'ÉLABORATION DES MESURES RÉGLEMENTAIRES - EXISTENCE, LORSQUE L'APPLICATION DE LA LOI EST MANIFESTEMENT IMPOSSIBLE EN L'ABSENCE DE CES MESURES RÉGLEMENTAIRES OU LORSQUE LE TEXTE LÉGISLATIF NE DÉTERMINE PAS LA RÈGLE TECHNIQUE D'UNE MANIÈRE SUFFISAMMENT PRÉCISE [RJ1] - 2) ILLUSTRATION - LOI INAPPLICABLE EN L'ABSENCE DE MESURES RÉGLEMENTAIRES.
</SCT>
<ANA ID="9A"> 01-03 1) Il résulte clairement du 1 de l'article 5 de la directive 2015/1535 du Parlement européen et du Conseil du 9 septembre 2015 que, pour leur application à une règle technique résultant, en droit interne, de la combinaison de dispositions de nature législative et de dispositions d'application de nature réglementaire, la communication à la Commission européenne des dispositions législatives relatives à cette règle technique peut n'être effectuée qu'au stade de l'élaboration des mesures réglementaires qui en fixent les conditions d'application, soit lorsque l'application de la loi est manifestement impossible en l'absence de ces mesures réglementaires et que, par suite, l'adoption de ces dernières conditionne l'entrée en vigueur de la règle technique, soit lorsque le texte législatif ne détermine pas, à lui seul, la règle technique d'une manière suffisamment précise pour que ses effets puissent être évalués par la Commission européenne et les Etats membres de l'Union européenne.,,,2) Recours pour excès de pouvoir contre le décret n° 2018-1076 du 3 décembre 2018 relatif aux modalités de liquidation et de recouvrement du montant des avoirs des joueurs en déshérence dû à l'Etat par les opérateurs de jeux en ligne agréés par l'Autorité de régulation des jeux en ligne et par la Française des jeux au titre de la loterie en ligne, pris pour l'application de l'article 50 de la loi n° 2015-1785 du 29 décembre 2015 de finances pour 2016.,,,Il ressort des pièces du dossier que la mise en oeuvre de l'obligation de déclaration et de reversement des sommes qui figurent sur des comptes-joueurs clôturés depuis six ans était manifestement impossible sans que ses modalités soient fixées par le décret litigieux. La publication de ce dernier était, par suite, nécessaire à l'entrée en vigueur des dispositions de l'article 50 de la loi du 29 décembre 2015. Par suite, il résulte de ce qui a été dit ci-dessus que la notification de ces dispositions législatives à la Commission européenne en même temps que celle des dispositions du décret litigieux n'a, en tout état de cause, pas méconnu le principe de communication immédiate à la Commission de tout projet de règle technique, fixé par l'article 5 de la directive du 9 septembre 2015.</ANA>
<ANA ID="9B"> 15-05-01-04 1) Il résulte clairement du 1 de l'article 5 de la directive 2015/1535 du Parlement européen et du Conseil du 9 septembre 2015 que, pour leur application à une règle technique résultant, en droit interne, de la combinaison de dispositions de nature législative et de dispositions d'application de nature réglementaire, la communication à la Commission européenne des dispositions législatives relatives à cette règle technique peut n'être effectuée qu'au stade de l'élaboration des mesures réglementaires qui en fixent les conditions d'application, soit lorsque l'application de la loi est manifestement impossible en l'absence de ces mesures réglementaires et que, par suite, l'adoption de ces dernières conditionne l'entrée en vigueur de la règle technique, soit lorsque le texte législatif ne détermine pas, à lui seul, la règle technique d'une manière suffisamment précise pour que ses effets puissent être évalués par la Commission européenne et les Etats membres de l'Union européenne.,,,2) Recours pour excès de pouvoir contre le décret n° 2018-1076 du 3 décembre 2018 relatif aux modalités de liquidation et de recouvrement du montant des avoirs des joueurs en déshérence dû à l'Etat par les opérateurs de jeux en ligne agréés par l'Autorité de régulation des jeux en ligne et par la Française des jeux au titre de la loterie en ligne, pris pour l'application de l'article 50 de la loi n° 2015-1785 du 29 décembre 2015 de finances pour 2016.,,,Il ressort des pièces du dossier que la mise en oeuvre de l'obligation de déclaration et de reversement des sommes qui figurent sur des comptes-joueurs clôturés depuis six ans était manifestement impossible sans que ses modalités soient fixées par le décret litigieux. La publication de ce dernier était, par suite, nécessaire à l'entrée en vigueur des dispositions de l'article 50 de la loi du 29 décembre 2015. Par suite, il résulte de ce qui a été dit ci-dessus que la notification de ces dispositions législatives à la Commission européenne en même temps que celle des dispositions du décret litigieux n'a, en tout état de cause, pas méconnu le principe de communication immédiate à la Commission de tout projet de règle technique, fixé par l'article 5 de la directive du 9 septembre 2015.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., en précisant, CE, 17 mai 2013, Fédération française des industries d'aliments conservés (FIAC), n° 358027, T. pp. 401-487.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
