<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041663064</ID>
<ANCIEN_ID>JG_L_2020_02_000000428711</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/66/30/CETATEXT000041663064.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 28/02/2020, 428711</TITRE>
<DATE_DEC>2020-02-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>428711</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR>M. Yohann Bouquerel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:428711.20200228</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés le 8 mars 2019 et le 7 février 2020 au secrétariat du contentieux du Conseil d'Etat, M. B... A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret du 7 janvier 2019 par lequel le Président de la République l'a radié des cadres par mesure disciplinaire ; <br/>
<br/>
              2°) d'enjoindre à la ministre des armées de le réintégrer et de reconstituer sa carrière depuis le 7 janvier 2019 dans un délai d'un mois à compter de la décision du Conseil d'Etat ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la défense ;<br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Yohann Bouquerel, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Spinosi, Sureau, avocat de M. A... ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 12 février 2020, présentée par la ministre des armées ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le même jour, présentée par M. A... ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier que M. A..., alors lieutenant, a commandé du 30 janvier au 6 juin 2014 en République Centrafricaine une section de la 2ème compagnie du 2ème régiment d'infanterie de marine dans le cadre de l'opération Sangaris. Par un décret du Président de la République du 7 janvier 2019, M. A..., devenu capitaine, a été radié des cadres par mesure disciplinaire pour des faits commis lors de cette opération. Il demande au Conseil d'Etat d'annuler ce décret pour excès de pouvoir et d'ordonner sa réintégration.  <br/>
<br/>
              Sur la légalité externe de la décision attaquée :<br/>
<br/>
              2. M. A... fait valoir, en premier lieu, que la procédure disciplinaire au terme de laquelle a été prise la décision contestée serait entachée d'irrégularité en raison de la partialité dont aurait fait preuve à son égard l'officier rapporteur ainsi que le président du conseil d'enquête. Toutefois, d'une part, il ressort des pièces du dossier que le rapport établi par l'officier rapporteur présente les faits reprochés à M. A... de manière objective, sans manifester de parti pris à son encontre ni omettre de prendre en compte les états de service de l'intéressé. D'autre part, la seule circonstance que le président du conseil d'enquête ait lui-même participé à l'opération Sangaris ne faisait pas obstacle, par elle-même, à ce qu'il préside cette instance et les propos tenus par celui-ci lors de la tenue du conseil ne permettent pas de remettre en cause son impartialité. <br/>
<br/>
              3. En deuxième lieu, aux termes de l'article R. 4137-77 du code de la défense : " L'ensemble des pièces et documents au vu desquels il est envisagé de sanctionner le comparant est adressé au rapporteur dès la désignation de ce dernier ". Aux termes de l'article R. 4137-78 du même code : " Le rapporteur convoque le comparant et son défenseur. Il leur donne communication personnelle et confidentielle de l'ensemble des pièces et documents prévus à l'article R. 4137-77, recueille leurs explications et reçoit les pièces présentées en défense ".<br/>
<br/>
              4. Il ressort des pièces du dossier que le requérant a eu connaissance dès le mois de janvier 2018 de l'ensemble des pièces au vu desquelles il était envisagé de le sanctionner. Si trente-huit pièces nouvelles ont été remises au défendeur de M. A... à l'ouverture du conseil d'enquête, il ne s'agissait que de documents liés à l'organisation matérielle de la procédure disciplinaire, tels que des récépissés de convocation, versés au dossier pour que celui-ci soit complet. Il n'est, au demeurant, pas soutenu que l'une quelconque de ces pièces versées en séance aurait été utile à la préparation de la défense de M. A.... Le moyen tiré de ce qu'auraient été ainsi méconnus les droits de la défense ne peut donc qu'être écarté.<br/>
<br/>
              5. En troisième lieu, aux termes de l'article R. 4137-79 du code de la défense : " Au reçu du procès-verbal, le président fixe la date de la réunion du conseil et convoque soit d'office, soit sur la demande du comparant, les personnes dont l'audition est utile pour l'examen de l'affaire. / Le président du conseil ne peut refuser la demande du comparant de faire entendre son président de catégorie ou, pour la gendarmerie nationale, son président du personnel militaire si ce dernier le souhaite. / Il notifie la date de la réunion du conseil ainsi que la liste des personnes mentionnées aux deux alinéas précédents au comparant de manière que celui-ci dispose, au reçu de cette notification, d'un délai de huit jours francs au moins avant la date de ladite réunion. Il l'invite à se présenter aux lieu, jour et heure indiqués et l'avise que, s'il ne se présente pas, le conseil pourra siéger hors de sa présence. Il informe le défenseur de ces notifications ".<br/>
<br/>
              6. Il ressort des pièces du dossier que M. A... n'a reçu que le 22 juin 2018 sa convocation devant le conseil d'enquête qui s'est tenu les 27 et 28 juin suivants, soit dans un délai inférieur à huit jours francs. Toutefois, le conseil du requérant ainsi que ce dernier ont été informés dès le 12 juin 2018, par un échange de courriels avec l'officier rapporteur, de la tenue de la réunion du conseil d'enquête les 27 et 28 juin suivants. Par suite, le requérant ne saurait se prévaloir utilement, dans les circonstances de l'espèce, de la méconnaissance des dispositions de l'article R. 4137-79 du code de la défense. <br/>
<br/>
              7. En quatrième lieu, il ne ressort pas des pièces du dossier que la longueur des séances du conseil d'enquête les 27 et 28 juin 2018 et la brièveté des pauses au cours de ses débats aient fait obstacle à l'exercice effectif des droits de la défense.<br/>
<br/>
              8. En cinquième lieu, le décret attaqué qui, contrairement à ce qui est soutenu, mentionne bien le contexte dans lequel sont intervenus les faits reprochés à M. A..., est suffisamment motivé.<br/>
<br/>
              Sur la légalité interne de la décision attaquée :<br/>
<br/>
              9. En premier lieu, aux termes du premier alinéa de l'article L. 4122-3 du code de la défense : " Le militaire est soumis aux obligations qu'exige l'état militaire conformément au deuxième alinéa de l'article L. 4111-1. Il exerce ses fonctions avec dignité, impartialité, intégrité et probité ". Aux termes du premier alinéa de l'article L. 4122-1 : " Les militaires doivent obéissance aux ordres de leurs supérieurs et sont responsables de l'exécution des missions qui leur sont confiées ". Aux termes de l'article L. 4137-2 du code de la défense : " Les sanctions disciplinaires applicables aux militaires sont réparties en trois groupes : (...) / 3° Les sanctions du troisième groupe sont : / a) Le retrait d'emploi, défini par les dispositions de l'article L. 4138-15 ; / b) La radiation des cadres ou la résiliation du contrat ". <br/>
<br/>
              10. Il ressort des pièces du dossier que M. A... a été sanctionné pour avoir participé directement à des violences contre un civil et pour avoir dissimulé à sa hiérarchie des sévices commis par des militaires placés sous ses ordres à l'encontre d'un combattant ennemi prisonnier et entravé. L'intéressé ne conteste pas la matérialité de ces faits ni leur caractère fautif. S'il fait valoir qu'un délai de quatre ans et demi s'est écoulé entre ceux-ci et la sanction prononcée, les autorités militaires n'ont eu connaissance effective des faits litigieux que le 19 mai 2016 lors de la remise d'un rapport d'enquête de commandement et ont engagé la procédure disciplinaire dans le délai de trois ans fixé par l'article L. 4137-1 du code de la défense. Eu égard à la gravité des faits, et malgré le contexte particulièrement difficile dans lequel se déroulait l'opération Sangaris et les très bons états de service de M. A..., l'autorité disciplinaire n'a pas pris, compte tenu de l'ensemble des circonstances de l'espèce, une sanction disproportionnée en décidant de radier des cadres l'intéressé.<br/>
<br/>
              11. En second lieu, le détournement de pouvoir allégué n'est pas établi.<br/>
<br/>
              12. Il résulte de tout ce qui précède que M. A... n'est pas fondé à demander l'annulation de la décision qu'il attaque et que ses conclusions à fin d'injonction, ainsi que celles présentées au titre de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. A... est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. B... A... et à la ministre des armées.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">08-01-01-05 ARMÉES ET DÉFENSE. PERSONNELS MILITAIRES ET CIVILS DE LA DÉFENSE. QUESTIONS COMMUNES À L'ENSEMBLE DES PERSONNELS MILITAIRES. DISCIPLINE. - CONTRÔLE DE LA PROPORTIONNALITÉ DE LA SANCTION DISCIPLINAIRE À LA GRAVITÉ DES FAITS [RJ1] - MILITAIRE RADIÉ DES CADRES POUR DES FAITS DE VIOLENCES CONTRE UN CIVIL ET POUR AVOIR DISSIMULÉ À SA HIÉRARCHIE DES SÉVICES COMMIS PAR DES MILITAIRES PLACÉS SOUS SES ORDRES À L'ENCONTRE D'UN PRISONNIER - SANCTION PROPORTIONNÉE EN L'ESPÈCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">36-09-04 FONCTIONNAIRES ET AGENTS PUBLICS. DISCIPLINE. SANCTIONS. - CONTRÔLE DE LA PROPORTIONNALITÉ DE LA SANCTION DISCIPLINAIRE À LA GRAVITÉ DES FAITS [RJ1] - MILITAIRE RADIÉ DES CADRES POUR DES FAITS DE VIOLENCES CONTRE UN CIVIL ET POUR AVOIR DISSIMULÉ À SA HIÉRARCHIE DES SÉVICES COMMIS PAR DES MILITAIRES PLACÉS SOUS SES ORDRES À L'ENCONTRE D'UN PRISONNIER - SANCTION PROPORTIONNÉE EN L'ESPÈCE.
</SCT>
<ANA ID="9A"> 08-01-01-05 Militaire radié des cadres pour avoir, dans le cadre d'une opération extérieure, participé directement à des violences contre un civil et pour avoir dissimulé à sa hiérarchie des sévices commis par des militaires placés sous ses ordres à l'encontre d'un combattant ennemi prisonnier et entravé.,,,Eu égard à la gravité des faits, et malgré le contexte particulièrement difficile dans lequel se déroulait l'opération et les très bons états de service de l'intéressé, l'autorité disciplinaire n'a pas pris, compte tenu de l'ensemble des circonstances de l'espèce, une sanction disproportionnée en décidant de radier des cadres l'intéressé.</ANA>
<ANA ID="9B"> 36-09-04 Militaire radié des cadres pour avoir, dans le cadre d'une opération extérieure, participé directement à des violences contre un civil et pour avoir dissimulé à sa hiérarchie des sévices commis par des militaires placés sous ses ordres à l'encontre d'un combattant ennemi prisonnier et entravé.,,,Eu égard à la gravité des faits, et malgré le contexte particulièrement difficile dans lequel se déroulait l'opération et les très bons états de service de l'intéressé, l'autorité disciplinaire n'a pas pris, compte tenu de l'ensemble des circonstances de l'espèce, une sanction disproportionnée en décidant de radier des cadres l'intéressé.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Assemblée, 13 novembre 2013, M.,, n° 347704, p. 279 ; CE, 25 janvier 2016, M.,, n° 391178, T. pp. 643-904 ; CE, 14 mars 2016, M.,, n° 389361, T. pp. 643-904.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
