<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027328314</ID>
<ANCIEN_ID>JG_L_2013_04_000000348311</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/32/83/CETATEXT000027328314.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 6ème SSR, 17/04/2013, 348311</TITRE>
<DATE_DEC>2013-04-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>348311</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 6ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DIDIER, PINET ; SCP FABIANI, LUC-THALER</AVOCATS>
<RAPPORTEUR>M. Rémi Decout-Paolini</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Maud Vialettes</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2013:348311.20130417</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 11 avril et 5 juillet 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour la commune de Ramatuelle, représentée par son maire ; la commune de Ramatuelle demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 09MA0821 du 17 mars 2011 par lequel la cour administrative d'appel de Marseille, sur requête de M. B...A..., d'une part, a annulé le jugement n° 0603717 du 9 janvier 2009 par lequel le tribunal administratif de Toulon a rejeté la demande présentée devant ce tribunal par M. A...et tendant à l'annulation de la délibération du 18 mai 2006 approuvant son plan local d'urbanisme, d'autre part, a annulé cette délibération ;<br/>
<br/>
              2°) de mettre à la charge de M. A...le versement de la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu la note en délibéré, enregistrée le 29 mars 2013, présentée pour la commune de Ramatuelle ;<br/>
<br/>
              Vu le code de l'urbanisme ;<br/>
<br/>
	Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Rémi Decout-Paolini, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Didier, Pinet, avocat de la commune de Ramatuelle  et de la SCP Fabiani, Luc-Thaler, avocat de M.A...,<br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Didier, Pinet, avocat de la commune de Ramatuelle et à la SCP Fabiani, Luc-Thaler, avocat de M.A... ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que le conseil municipal de Ramatuelle a adopté le 6 juin 2001 une délibération prescrivant l'élaboration d'un plan local d'urbanisme, ultérieurement approuvé par une délibération du 18 mai 2006 ; que, par un jugement du 9 janvier 2009, le tribunal administratif de Toulon a rejeté la demande de M. A...tendant à l'annulation de cette délibération ; que la commune de Ramatuelle se pourvoit en cassation contre l'arrêt du 17 mars 2011 par lequel la cour administrative d'appel de Marseille, faisant droit à l'appel de M.A..., a annulé le jugement du 9 janvier 2009 et la délibération du 18 mars 2006 ;<br/>
<br/>
              2. Considérant qu'aux termes du I de l'article L. 300-2 du code de l'urbanisme, dans sa rédaction applicable au litige : " Le conseil municipal ou l'organe délibérant de l'établissement public de coopération intercommunale délibère sur les objectifs poursuivis et sur les modalités d'une concertation associant, pendant toute la durée de l'élaboration du projet, les habitants, les associations locales et les autres personnes concernées (...) avant : / a) Toute élaboration ou révision (...) du plan local d'urbanisme (...) " ; que l'article L. 123-6 du même code, dans sa rédaction également applicable au litige, prévoit que la décision prise par le conseil municipal en application de ces dispositions est notifiée à plusieurs autorités administratives et qu'à compter de sa publication, l'autorité compétente peut décider de surseoir à statuer sur les demandes d'autorisation concernant des constructions, installations ou opérations qui seraient de nature à compromettre ou à rendre plus onéreuse l'exécution du futur plan local d'urbanisme ; qu'il résulte de ces dispositions que le conseil municipal doit, avant que ne soit engagée la concertation avec les habitants, les associations locales et les autres personnes concernées, délibérer, d'une part, et au moins dans leurs grandes lignes, sur les objectifs poursuivis par la commune en projetant d'élaborer ou de réviser un document d'urbanisme, d'autre part, sur les modalités de la concertation ; que la méconnaissance de cette obligation est de nature à  entraîner l'illégalité du document d'urbanisme approuvé ; que, si les deux volets sont en principe adoptés simultanément, la décision du conseil municipal peut prendre la forme de deux délibérations successives, notifiées conformément aux dispositions de l'article L. 123-6 du code de l'urbanisme,  pourvu que cette circonstance n'ait pas pour effet de priver d'effet utile la concertation organisée sur les objectifs poursuivis par l'élaboration du plan local d'urbanisme ;<br/>
<br/>
              3. Considérant que, pour annuler la délibération du 18 mars 2006 par laquelle le conseil municipal de Ramatuelle a approuvé le plan local d'urbanisme, la cour administrative d'appel s'est fondée sur la seule circonstance que la délibération du 6 juin 2001, par laquelle la commune avait prescrit l'élaboration de ce plan et fixé les modalités de la concertation prévue par l'article L. 300-2 précité, n'avait pas défini les objectifs poursuivis et que leur définition avait donné lieu à une délibération adoptée ultérieurement, alors qu'un " diagnostic territorial préalable " avait déjà fait l'objet d'une exposition publique d'un mois suivie d'une réunion publique et que plusieurs dizaines d'avis avait été consignés sur un registre tenu en mairie ; qu'en statuant ainsi, sans rechercher si, après l'adoption, par les deux délibérations du conseil municipal, d'une décision complète prise en application des dispositions de l'article L. 300-2 du code de l'urbanisme, une concertation effective avait eu lieu sur les objectifs poursuivis par la commune,  la cour administrative d'appel de Marseille a commis une erreur de droit ;<br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que la commune de Ramatuelle est fondée à demander l'annulation de l'arrêt qu'elle attaque ;<br/>
<br/>
              5. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de la commune de Ramatuelle qui n'est pas, dans la présente instance, la partie perdante ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de M.A..., en application des mêmes dispositions, le versement à la commune de Ramatuelle d'une somme de 3 000 euros ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Marseille du 17 mars 2011 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Marseille.<br/>
Article 3 : M. A...versera à la commune de Ramatuelle la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions présentées par M. A...au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à la commune de Ramatuelle et à M. B...A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">68-01-01-01-01 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PLANS D'AMÉNAGEMENT ET D'URBANISME. PLANS D`OCCUPATION DES SOLS (POS) ET PLANS LOCAUX D'URBANISME (PLU). LÉGALITÉ DES PLANS. PROCÉDURE D'ÉLABORATION. - CONTENU DE LA DÉLIBÉRATION PRESCRIVANT L'ÉLABORATION OU LA RÉVISION DU PLAN (ART.  L. 123-6 ET L. 300-2 DU CODE DE L'URBANISME) - OBJECTIFS DE LA RÉVISION ET MODALITÉS DE LA CONCERTATION - FORMALITÉ SUBSTANTIELLE - EXISTENCE [RJ1] - POSSIBILITÉ QUE CES DEUX VOLETS FASSENT L'OBJET DE DEUX DÉLIBÉRATIONS SUCCESSIVES - EXISTENCE - CONDITIONS.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">68-01-01-01-02-01 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PLANS D'AMÉNAGEMENT ET D'URBANISME. PLANS D`OCCUPATION DES SOLS (POS) ET PLANS LOCAUX D'URBANISME (PLU). LÉGALITÉ DES PLANS. MODIFICATION ET RÉVISION DES PLANS. PROCÉDURES DE RÉVISION. - CONTENU DE LA DÉLIBÉRATION PRESCRIVANT L'ÉLABORATION OU LA RÉVISION DU PLAN (ART.  L. 123-6 ET L. 300-2 DU CODE DE L'URBANISME) - OBJECTIFS DE LA RÉVISION ET MODALITÉS DE LA CONCERTATION - FORMALITÉ SUBSTANTIELLE - EXISTENCE [RJ1] - POSSIBILITÉ QUE CES DEUX VOLETS FASSENT L'OBJET DE DEUX DÉLIBÉRATIONS SUCCESSIVES - EXISTENCE - CONDITIONS.
</SCT>
<ANA ID="9A"> 68-01-01-01-01 Il résulte des articles L. 123-6 et L. 300-2 du code de l'urbanisme que le conseil municipal doit, avant que ne soit engagée la concertation avec les habitants, les associations locales et les autres personnes concernées, délibérer, d'une part, et au moins dans leurs grandes lignes, sur les objectifs poursuivis par la commune en projetant d'élaborer ou de réviser un document d'urbanisme, d'autre part, sur les modalités de la concertation. La méconnaissance de cette obligation est de nature à entraîner l'illégalité du document d'urbanisme approuvé.,,,Si les deux volets sont en principe adoptés simultanément, la décision du conseil municipal peut prendre la forme de deux délibérations successives, notifiées conformément aux dispositions de l'article L. 123-6 du code de l'urbanisme, pourvu que cette circonstance n'ait pas pour effet de priver d'effet utile la concertation organisée sur les objectifs poursuivis par l'élaboration du plan local d'urbanisme.</ANA>
<ANA ID="9B"> 68-01-01-01-02-01 Il résulte des articles L. 123-6 et L. 300-2 du code de l'urbanisme que le conseil municipal doit, avant que ne soit engagée la concertation avec les habitants, les associations locales et les autres personnes concernées, délibérer, d'une part, et au moins dans leurs grandes lignes, sur les objectifs poursuivis par la commune en projetant d'élaborer ou de réviser un document d'urbanisme, d'autre part, sur les modalités de la concertation. La méconnaissance de cette obligation est de nature à entraîner l'illégalité du document d'urbanisme approuvé.,,,Si les deux volets sont en principe adoptés simultanément, la décision du conseil municipal peut prendre la forme de deux délibérations successives, notifiées conformément aux dispositions de l'article L. 123-6 du code de l'urbanisme, pourvu que cette circonstance n'ait pas pour effet de priver d'effet utile la concertation organisée sur les objectifs poursuivis par l'élaboration du plan local d'urbanisme.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 10 février 2010, Commune de Saint-Lunaire, n° 327149, T. pp. 921-1012-1024 ; Solution abandonnée, sur le fait que les illégalités entachant la délibération prescrivant l'adoption ou la révision du PLU sont susceptibles d'entraîner l'annulation de la délibération approuvant le PLU, par CE, Section, 5 mai 2017, Commune de Saint-Bon-Tarentaise, n° 388902, A.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
