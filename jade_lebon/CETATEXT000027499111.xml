<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027499111</ID>
<ANCIEN_ID>JG_L_2013_06_000000352655</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/49/91/CETATEXT000027499111.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 03/06/2013, 352655</TITRE>
<DATE_DEC>2013-06-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>352655</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Jean Lessi</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CESSR:2013:352655.20130603</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 13 septembre et 12 décembre 2011 au secrétariat du contentieux du Conseil d'Etat, présentés par la société Laboratoire GlaxoSmithKline, dont le siège est 100, route de Versailles à Marly-le-Roi (78162 Cedex) ; la société requérante demande au Conseil d'Etat d'annuler pour excès de pouvoir, d'une part, la décision du 14 mars 2011 par laquelle le ministre du travail, de l'emploi et de la santé et le ministre du budget, des comptes publics, de la fonction publique et de la réforme de l'Etat ont refusé d'inscrire la spécialité " Votrient (r) " sur la liste des médicaments remboursables aux assurés sociaux mentionnée à l'article L. 162-17 du code de la sécurité sociale et sur la liste des spécialités agréées à l'usage des collectivités publiques mentionnée à l'article L. 5123-2 du code de la santé publique et, d'autre part, la décision implicite par laquelle le ministre du travail, de l'emploi et de la santé a rejeté son recours gracieux dirigé contre la décision du 14 mars 2011 ;<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le règlement n° 726/2004 du Parlement européen et du Conseil du 31 mars 2004 ;<br/>
<br/>
              Vu le règlement n° 507/5006 de la Commission du 29 mars 2006 ;<br/>
<br/>
              Vu le code de la santé publique ;<br/>
<br/>
              Vu le code de la sécurité sociale ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean Lessi, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 162-17 du code de la sécurité sociale : " Les médicaments spécialisés, mentionnés à l'article L. 5121-8 du code de la santé publique et les médicaments bénéficiant d'une autorisation d'importation parallèle  en application de l'article L. 5124-13 du même code, ne peuvent être pris en charge ou donner lieu à remboursement par les caisses d'assurance maladie, lorsqu'ils sont dispensés en officine, que s'ils figurent sur une liste établie dans les conditions fixées par décret en Conseil d'État (...) " ; qu'il résulte des dispositions combinées des articles R. 163-4 et R. 162-19 du même code que l'inscription est prononcée par arrêté conjoint du ministre chargé de la santé et du ministre chargé de la sécurité sociale, après avis de la commission de la transparence ; qu'aux termes de l'article L. 5123-2 du code de la santé publique : " L'achat, la fourniture, la prise en charge et l'utilisation par les collectivités publiques des médicaments définis aux articles L. 5121-8 (...) ou importés selon la procédure prévue à l'article L. 5121-17 sont limités, dans les conditions propres à ces médicaments fixées par le décret mentionné à l'article L. 162-17 du code de la sécurité sociale, aux produits agréés dont la liste est établie par arrêté des ministres chargés de la santé et de la sécurité sociale (...) " ; qu'il résulte des dispositions combinées des articles L. 5123-3 du même code et L. 161-37 du code de la sécurité sociale que l'inscription sur cette liste est également prononcée après avis de la commission de la transparence ; que l'article L. 162-17-2 du code de la sécurité sociale prévoit que l'inscription des médicaments sur la liste mentionnée à l'article L. 162-17 vaut inscription sur la liste mentionnée à l'article L. 5123-2 du code de la santé publique en cas de demande d'inscription simultanée sur les deux listes ; <br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier que, le 14 juin 2010, la Commission européenne a délivré à la société Laboratoire GlaxoSmithKline une autorisation de mise sur le marché conditionnelle d'une durée d'un an renouvelable pour la spécialité " Votrient (r) " à base de pazopanib, indiquée pour le traitement en première ligne du cancer du rein avancé ainsi qu'en deuxième ligne chez les patients préalablement traités par des cytokines à un stade avancé de leur maladie, en imposant dans l'intervalle à la société de mener à bien des études cliniques complémentaires concernant la sécurité et l'efficacité de ce médicament ; que la société Laboratoire GlaxoSmithKline a ensuite sollicité l'inscription de la spécialité " Votrient (r) " sur les deux listes mentionnées respectivement à l'article L. 162-17 du code de la sécurité sociale et à l'article L. 5123-2 du code de la santé publique ; qu'à l'issue de ses séances du 3 novembre 2010, du 2 et du 28 février 2011, la commission de la transparence mentionnée à l'article R. 163-15 du code de la sécurité sociale a rendu des avis défavorables à cette inscription ; que, par une décision du 14 mars 2011, implicitement confirmée après rejet du recours gracieux de la société Laboratoire GlaxoSmithKline, les ministres du travail, de l'emploi et de la santé et le ministre du budget, des comptes publics, de la fonction publique et de la réforme de l'Etat, ont refusé de procéder aux inscriptions demandées ;<br/>
<br/>
              Sur la légalité externe de la décision du 14 mars 2011 :<br/>
<br/>
              3. Considérant, en premier lieu, qu'aux termes de l'article R. 163-16 du code de la sécurité sociale : " I. - Les délibérations de la commission mentionnée à l'article R. 163-15 ne sont valables que si au moins douze membres ayant voix délibérative de la commission sont présents. (...) / III. - Lorsque l'avis porte sur l'inscription (...) d'un médicament sur la liste prévue au premier alinéa de l'article L. 162-17 ou sur l'inscription (...) sur la liste prévue à l'article L. 5123-2 du code de la santé publique, cet avis est immédiatement communiqué à l'entreprise qui exploite le médicament. / L'entreprise peut, dans les huit jours suivant la réception de cet avis, demander à être entendue par la commission ou présenter ses observations écrites. La commission peut modifier son avis compte tenu des observations présentées. (...) " ; que, contrairement à ce que soutient la société requérante, il ressort des pièces du dossier que, lors des réunions du 2 février 2011 et du 28 février 2011 au cours desquelles la commission de la transparence s'est définitivement prononcée sur la spécialité " Votrient (r) "  après avoir entendu la société à sa demande, le quorum exigé par l'article R. 163-16, soit douze membres ayant voix délibérative, était atteint ; qu'il ne ressort pas de ces mêmes pièces que la société Laboratoire GlaxoSmithKline n'aurait pas été mise à même de faire valoir ses arguments au cours de son audition ; que, par suite, le moyen tiré de ce que la commission n'aurait pas observé la procédure prévue à l'article R. 163-16 ne peut qu'être écarté ; <br/>
<br/>
              4. Considérant, par ailleurs, que si l'article L. 161-37 du code de la sécurité sociale impose à la Haute Autorité de santé de rendre publics les comptes rendus des réunions de la commission de la transparence " assortis des détails et explications des votes, y compris les opinions minoritaires (...) ", cette obligation n'est pas prescrite à peine d'irrégularité des décisions rendues au vu des avis de la commission ; que, par suite, la circonstance, alléguée par la société requérante, que le compte rendu de la réunion du 2 février 2011 ne ferait état ni de ses arguments, ni des explications de vote des membres de la commission est sans incidence sur la légalité de la décision attaquée ; que le moyen tiré de la méconnaissance de l'article L. 161-37 doit, dès lors, être écarté ; <br/>
<br/>
              5. Considérant, en second lieu, que l'article R. 163-14 du code de la sécurité sociale prévoit que les décisions portant refus d'inscription sur les listes mentionnées à l'article L. 162-17 du même code et à l'article L. 5123-2 du code de la santé publique sont communiquées à l'entreprise avec la mention des motifs de ces décisions ; que la décision litigieuse du 14 mars 2011 indique, en se référant aux motifs de l'avis rendu par la commission de la transparence le 2 février 2011, lui-même préalablement communiqué à la société Laboratoire GlaxoSmithKline, que la spécialité " Votrient (r) " présente un service médical rendu insuffisant pour justifier sa prise en charge par l'assurance maladie en application de l'article R. 163-13 du code de la sécurité sociale ; que, par suite, le moyen tiré de ce que cette décision méconnaît les dispositions de l'article R. 163-14 du même code doit être écarté ;<br/>
<br/>
              Sur la légalité interne de la décision du 14 mars 2011 :<br/>
<br/>
              6. Considérant, en premier lieu, qu'il ne ressort pas des pièces du dossier que les ministres auteurs de la décision attaquée se seraient crus liés par la teneur de l'avis de la commission de la transparence du 2 février 2011 ; que le moyen tiré de ce qu'ils auraient méconnu l'étendue de leur compétence doit, par suite, être écarté ;<br/>
<br/>
              7. Considérant, en deuxième lieu, qu'aux termes du I de l'article R. 163-3 du code de la sécurité sociale : " Les médicaments sont inscrits sur la liste prévue au premier alinéa de l'article L. 162-17 au vu de l'appréciation du service médical rendu qu'ils apportent indication par indication. Cette appréciation prend en compte l'efficacité et les effets indésirables du médicament, sa place dans la stratégie thérapeutique, notamment au regard des autres thérapies disponibles, la gravité de l'affection à laquelle il est destiné, le caractère préventif, curatif ou symptomatique du traitement médicamenteux et son intérêt pour la santé publique. Les médicaments dont le service médical rendu est insuffisant au regard des autres médicaments ou thérapies disponibles ne sont pas inscrits sur la liste " ; qu'il résulte de ces dispositions, applicables à l'inscription sur la liste mentionnée à l'article L. 5123-2 du code de la santé publique ainsi qu'il résulte de l'article R. 163-18 du code de la sécurité sociale, que, pour apprécier le caractère suffisant du service médical rendu par un médicament sur une indication donnée, en particulier du point de vue de sa place dans la stratégie thérapeutique mais aussi de son efficacité et de ses effets indésirables, les ministres chargés de la santé et de la sécurité sociale peuvent tenir compte du service rendu par d'autres thérapies et médicaments poursuivant la même finalité ; <br/>
<br/>
              8. Considérant qu'il ressort de l'avis de la commission de la transparence du 2 février 2011, dont les ministres auteurs de la décision attaquée doivent être regardés comme s'étant approprié les motifs, que le refus d'inscription de la spécialité " Votrient (r) " est fondé sur la circonstance qu'au vu des seuls résultats des études cliniques, et en l'absence à ce stade des données - que la société Laboratoire GlaxoSmithKline est tenue de fournir à la Commission européenne en vertu de son autorisation de mise sur le marché conditionnelle - en comparant directement l'efficacité et les effets indésirables aux autres médicaments disponibles dans la classe pharmaco-thérapeutique des inhibiteurs de tyrosine kinase, en particulier au sunitinib s'agissant du traitement médicamenteux du cancer du rein avancé en première ligne et au sorafenib en deuxième ligne, une " perte de chance pour les patients vis-à-vis de ces traitements " ne pouvait être écartée ; <br/>
<br/>
              9. Considérant qu'il résulte de ce qui a été dit ci-dessus que les ministres pouvaient légalement rechercher si le service médical rendu de " Votrient (r) " était suffisant au regard des autres médicaments disponibles ; que, de même, les ministres pouvaient légalement se fonder, pour refuser l'inscription de ce médicament, sur la circonstance que le dossier, en l'absence, à ce stade, d'étude comparative directe par rapport à des comparateurs actifs, ne les mettait pas à même, dans les circonstances de l'espèce, de procéder à cette comparaison, et sur le risque en découlant d'inscrire un médicament qui présenterait un service médical insuffisant ; qu'ils n'ont, ce faisant, pas ajouté de motif d'appréciation du service médical rendu à ceux que prévoit limitativement l'article R. 163-3 du code de la sécurité sociale, ni exigé, dans tous les cas, de disposer d'études comparatives directes ; que si, selon la société Laboratoire GlaxoSmithKline - qui, au demeurant, ne soutient pas que la " non-infériorité " de " Votrient (r) " aurait pu, en l'espèce, être regardée comme établie sans recours à une comparaison clinique directe par rapport aux autres médicaments disponibles - l'exigence d'une étude clinique comparative créerait par elle-même une différence de traitement illégale entre les premiers médicaments inscrits sur les listes et ceux dont l'inscription est sollicitée ultérieurement, ces deux catégories de médicaments se trouvent dans une situation différente en rapport avec l'objet des dispositions prévoyant la prise en charge par la solidarité nationale des seules spécialités inscrites ; <br/>
<br/>
              10. Considérant, en troisième lieu, que si la société Laboratoire GlaxoSmithKline soutient que le refus d'inscription de la spécialité " Votrient (r) " a pour effet de priver les patients d'une thérapie supplémentaire potentiellement plus adaptée à leur état de santé et à leur degré de tolérance aux différents traitements, il ne ressort pas des pièces du dossier qu'en estimant que les résultats des seules études cliniques disponibles ne leur permettaient pas de vérifier de manière suffisamment fiable que le service rendu, en termes notamment de survie globale, de qualité de vie ou de survie sans progression de la maladie, était suffisant au regard du niveau de réponse déjà apporté au besoin thérapeutique en cause par les autres médicaments disponibles pour le traitement du cancer du rein avancé en première ou en deuxième ligne, et en en déduisant que le service médical rendu par la spécialité en cause ne pouvait être regardé comme suffisant, les ministres auteurs de la décision attaquée auraient commis une erreur manifeste d'appréciation ;<br/>
<br/>
              11. Considérant, en quatrième et dernier lieu, que l'absence d'étude comparative directe par rapport à des comparateurs actifs au dossier de demande d'autorisation de mise sur le marché n'a, il est vrai, pas fait obstacle à la délivrance par la Commission européenne d'une autorisation conditionnelle pour la spécialité " Votrient (r) " ; que, toutefois, cette première décision, prise sur le fondement du règlement de la Commission du 29 mars 2006 relatif aux autorisations de mise sur le marché conditionnelles de médicaments à usage humain, lequel permet précisément la délivrance d'une telle autorisation assortie de conditions spécifiques pour une durée limitée à un an alors même que le demandeur n'a pas fourni des données cliniques complètes concernant la sécurité et l'efficacité du médicament, ne s'opposait pas à ce que les autorités nationales, dans l'exercice de leurs compétences et en application d'une législation distincte, refusent ensuite d'inscrire cette même spécialité sur la liste mentionnée à l'article L. 162-17 du code de la sécurité sociale ou sur la liste mentionnée à l'article L. 5123-2 du code de la santé publique pour un motif tiré de ce que les études cliniques fournies à l'appui de la demande d'inscription ne permettaient pas de regarder le service médical rendu de cette spécialité comme suffisant ; que le moyen tiré de l'atteinte au principe de sécurité juridique doit dès lors, et en tout état de cause, être écarté ;<br/>
<br/>
              12. Considérant qu'il résulte de tout ce qui précède que la société Laboratoire GlaxoSmithKline n'est pas fondée à demander l'annulation de la décision du 14 mars 2011 par laquelle les ministres du travail, de l'emploi et de la santé et le ministre du budget, des comptes publics, de la fonction publique et de la réforme de l'Etat ont refusé d'inscrire la spécialité " Votrient (r) " sur les listes mentionnées aux articles L. 162-17 du code de la sécurité sociale et L. 5123-2 du code de la santé publique ni, par voie de conséquence, de la décision implicite de rejet de leur recours gracieux ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la société laboratoire GlaxoSmithKline est rejetée.<br/>
Article 2 : La présente décision sera notifiée à la société Laboratoire GlaxoSmithKline (GSK) et à la ministre des affaires sociales et de la santé.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-03-02-02 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - FORME ET PROCÉDURE. PROCÉDURE CONSULTATIVE. CONSULTATION OBLIGATOIRE. - COMMISSION DE LA TRANSPARENCE - CONSULTATION OBLIGATOIRE EN VUE DE L'INSCRIPTION DE MÉDICAMENTS SUR LA LISTE DES MÉDICAMENTS REMBOURSABLES - OBLIGATION LÉGALE DE PUBLICITÉ DES COMPTES RENDUS DES RÉUNIONS (ART. L. 161-17 DU CODE DE LA SÉCURITÉ SOCIALE) - OBLIGATION PRESCRITE À PEINE D'IRRÉGULARITÉ DES DÉCISIONS RENDUES AU VU DES AVIS DE LA COMMISSION - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">61-11-02 SANTÉ PUBLIQUE. - COMMISSION DE LA TRANSPARENCE - OBLIGATION LÉGALE DE PUBLICITÉ DES COMPTES RENDUS DES RÉUNIONS (ART. L. 161-17 DU CODE DE LA SÉCURITÉ SOCIALE) - OBLIGATION PRESCRITE À PEINE D'IRRÉGULARITÉ DES DÉCISIONS RENDUES AU VU DES AVIS DE LA COMMISSION - ABSENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">62-04-01 SÉCURITÉ SOCIALE. PRESTATIONS. PRESTATIONS D'ASSURANCE MALADIE. - REMBOURSEMENT DES MÉDICAMENTS - INSCRIPTION SUR LA LISTE DES MÉDICAMENTS REMBOURSABLES - APPRÉCIATION DU SERVICE MÉDICAL RENDU - MÉTHODE - POSSIBILITÉ DE TENIR COMPTE DU SERVICE MÉDICAL RENDU PAR D'AUTRES THÉRAPIES ET MÉDICAMENTS POURSUIVANT LA MÊME FINALITÉ - EXISTENCE.
</SCT>
<ANA ID="9A"> 01-03-02-02 Si l'article L. 161-37 du code de la sécurité sociale impose à la Haute Autorité de santé de rendre publics les comptes rendus des réunions de la commission de la transparence assortis des détails et explications des votes, y compris les opinions minoritaires, cette obligation n'est pas prescrite à peine d'irrégularité des décisions rendues au vu des avis de la commission.</ANA>
<ANA ID="9B"> 61-11-02 Si l'article L. 161-37 du code de la sécurité sociale impose à la Haute Autorité de santé de rendre publics les comptes rendus des réunions de la commission de la transparence assortis des détails et explications des votes, y compris les opinions minoritaires, cette obligation n'est pas prescrite à peine d'irrégularité des décisions rendues au vu des avis de la commission.</ANA>
<ANA ID="9C"> 62-04-01 Il résulte des dispositions du I de l'article R. 163-3 du code de la sécurité sociale, applicables à l'inscription sur la liste mentionnée à l'article L. 5123-2 du code de la santé publique ainsi qu'il résulte de l'article R. 163-18 du code de la sécurité sociale, que, pour apprécier le caractère suffisant du service médical rendu par un médicament sur une indication donnée, en particulier du point de vue de sa place dans la stratégie thérapeutique mais aussi de son efficacité et de ses effets indésirables, les ministres chargés de la santé et de la sécurité sociale peuvent tenir compte du service rendu par d'autres thérapies et médicaments poursuivant la même finalité.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
