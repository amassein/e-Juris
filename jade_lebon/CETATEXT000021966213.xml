<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000021966213</ID>
<ANCIEN_ID>JG_L_2010_03_000000324076</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/21/96/62/CETATEXT000021966213.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 10/03/2010, 324076</TITRE>
<DATE_DEC>2010-03-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>324076</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Vigouroux</PRESIDENT>
<AVOCATS>BLONDEL</AVOCATS>
<RAPPORTEUR>Mme Laure  Bédier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mlle Courrèges Anne</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2010:324076.20100310</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 13 janvier et 7 avril 2009 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. et Mme Paul A, demeurant ... ; M. et Mme A demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt du 13 novembre 2008 par lequel la cour administrative d'appel de Marseille a rejeté leur requête tendant à l'annulation du jugement du 18 mai 2006 du tribunal administratif de Montpellier rejetant leur demande d'annulation de l'arrêté du 6 août 2002 du maire de la commune de Jacou les mettant en demeure de cesser immédiatement les travaux de construction d'un mur de clôture ainsi qu'à l'annulation de cet arrêté ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Jacou la somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'urbanisme ; <br/>
<br/>
              Vu la loi n° 79-587 du 11 juillet 1979 ;<br/>
<br/>
              Vu la loi n° 2000-321 du 12 avril 2000 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Laure Bédier, Maître des Requêtes,  <br/>
<br/>
              - les observations de Me Blondel, avocat de M. et Mme A, <br/>
<br/>
              - les conclusions de Mlle Anne Courrèges, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à Me Blondel, avocat de M. et Mme A ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant qu'aux termes du troisième alinéa de l'article L. 480-2 du code de l'urbanisme, dans sa rédaction alors applicable : " Dès qu'un procès-verbal relevant de l'une des infractions prévues à l'article L. 480-4 a été dressé, le maire peut également, si l'autorité judiciaire ne s'est pas encore prononcée, ordonner par arrêté motivé l'interruption des travaux (...) " ; qu'aux termes de l'article 24 de la loi du 12 avril 2000 : "  Exception faite des cas où il est statué sur une demande, les décisions individuelles qui doivent être motivées en application des articles 1er et 2 de la loi n° 79-587 du 11 juillet 1979 (...) n'interviennent qu'après que la personne intéressée a été mise à même de présenter des observations écrites et, le cas échéant, sur sa demande, des observations orales (...) / Les dispositions de l'alinéa précédent ne sont pas applicables : / 1° En cas d'urgence ou de circonstances exceptionnelles (...) " ; qu'il résulte de ces dispositions que la décision par laquelle le maire ordonne l'interruption des travaux au motif qu'ils ne sont pas menés en conformité avec une autorisation de construire, qui est au nombre des mesures de police qui doivent être motivées en application de la loi du 11 juillet 1979, ne peut intervenir qu'après que son destinataire a été mis à même de présenter ses observations, sauf en cas d'urgence ou de circonstances exceptionnelles ; que la situation d'urgence permettant à l'administration de se dispenser de cette procédure contradictoire s'apprécie tant au regard des conséquences dommageables des travaux litigieux que de la nécessité de les interrompre rapidement en raison de la brièveté de leur exécution ; qu'ainsi, en se bornant à relever qu'eu égard au délai de réalisation des travaux, qui n'était que de quelques jours, le maire a été placé dans une situation d'urgence telle qu'il pouvait s'abstenir de respecter la procédure contradictoire prévue par l'article 24 de la loi du 12 avril 2000, sans rechercher quels étaient l'importance et les effets des travaux en cause, la cour administrative d'appel de Marseille a commis une erreur de droit ; que son arrêt doit, pour ce motif, être annulé ;<br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              Considérant que le permis de construire assorti de l'autorisation de clôture qui a été accordé à M. et Mme A portait sur la construction d'un mur d'une hauteur d'un mètre vingt en contrebas de leur maison ; que ces derniers ont entrepris la construction d'un mur d'une hauteur totale de trois mètres vingt, dont deux mètres leur permettaient de niveler le terrain d'assiette de leur maison ; que compte tenu tant de l'importance de ce mur et de ses effets sur le voisinage que de la nécessité d'interrompre rapidement les travaux en raison de la brièveté de leur exécution, la situation d'urgence doit être regardée comme constituée ; que, par suite, le non-respect de la procédure contradictoire prévue par l'article 24 de la loi du 12 avril 2000 n'a pas entaché d'illégalité l'arrêté du 6 août 2002 du maire de la commune de Jacou ; que le moyen tiré de ce que la partie du mur servant de mur de soutènement, qui n'est pas distinct de l'ensemble du mur, ne relèverait d'aucune procédure d'autorisation doit être écarté ;<br/>
<br/>
              Considérant qu'il résulte de ce qui précède que M. et Mme A ne sont pas fondés à se plaindre de ce que le tribunal administratif de Montpellier a rejeté leurs conclusions tendant à l'annulation de la décision du maire de la commune de Jacou du 6 août 2002 leur ordonnant d'interrompre les travaux de construction d'un mur de clôture autour de leur propriété ; <br/>
<br/>
              Considérant que les conclusions présentées en cassation par M. et Mme A tendant à mettre à la charge de la commune de Jacou une somme de 4 500 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent en tout état de cause qu'être rejetées, le maire ayant agi dans le cadre de l'article L. 480-2 du code de l'urbanisme en qualité d'autorité administrative de l'Etat ; qu'il en est de même des conclusions présentées à ce titre devant le tribunal administratif et la cour administrative d'appel par la commune de Jacou, qui n'était pas partie à l'instance ; que les dispositions de l'article      L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de l'Etat, qui n'est pas la partie perdante, la somme que demandaient M. et Mme A en première instance et en appel au titre des frais exposés par eux et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Marseille du 13 novembre 2008 est annulé.<br/>
Article 2 : Les conclusions d'appel de M. et Mme A, le surplus des conclusions de leur pourvoi et les conclusions présentées par la commune de Jacou tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à M. et Mme Paul A et au ministre d'Etat, ministre de l'écologie, de l'énergie, du développement durable et de la mer. <br/>
Copie en sera adressée pour information  à la commune de Jacou.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-03-03-01 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - FORME ET PROCÉDURE. PROCÉDURE CONTRADICTOIRE. OBLIGATOIRE. - ARRÊTÉ INTERRUPTIF DE TRAVAUX (ART. L. 480-2 DU CODE DE L'URBANISME).
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">68-03-05-02 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PERMIS DE CONSTRUIRE. CONTRÔLE DES TRAVAUX. INTERRUPTION DES TRAVAUX. - ARRÊTÉ INTERRUPTIF DE TRAVAUX (ART. L. 480-2 DU CODE DE L'URBANISME) - PROCÉDURE CONTRADICTOIRE EN VERTU DE L'ARTICLE 24 DE LA LOI DU 12 AVRIL 2000 - EXISTENCE.
</SCT>
<ANA ID="9A"> 01-03-03-01 L'interruption des travaux prévue par l'article L. 480-2 du code de l'urbanisme est au nombre des mesures de police qui, conformément à l'article 24 de la loi n° 2000-321 du 12 avril 2000, ne peuvent intervenir qu'après que son destinataire a été mis à même de présenter ses observations, sauf en cas d'urgence ou de circonstances exceptionnelles. Cette situation d'urgence s'apprécie tant au regard des conséquences dommageables des travaux litigieux que de la brièveté d'exécution de ces travaux.</ANA>
<ANA ID="9B"> 68-03-05-02 L'interruption des travaux prévue par l'article L. 480-2 du code de l'urbanisme est au nombre des mesures de police qui, conformément à l'article 24 de la loi n° 2000-321 du 12 avril 2000, ne peuvent intervenir qu'après que son destinataire a été mis à même de présenter ses observations, sauf en cas d'urgence ou de circonstances exceptionnelles. Cette situation d'urgence s'apprécie tant au regard des conséquences dommageables des travaux litigieux que de la brièveté d'exécution de ces travaux.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
