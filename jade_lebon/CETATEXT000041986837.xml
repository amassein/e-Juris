<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041986837</ID>
<ANCIEN_ID>JG_L_2020_06_000000422471</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/98/68/CETATEXT000041986837.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 4ème chambres réunies, 10/06/2020, 422471</TITRE>
<DATE_DEC>2020-06-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>422471</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 4ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELAMARRE, JEHANNIN ; SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>M. Arnaud Skzryerbak</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Villette</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:422471.20200610</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A... C... a demandé au tribunal administratif de Toulouse d'annuler la décision implicite née du silence gardé par le président du conseil départemental du Tarn sur son recours administratif du 20 novembre 2017 dirigé contre la décision suspendant ses droits au revenu de solidarité active à compter du 1er août 2015. Par une ordonnance n° 1802646 du 14 juin 2018, le président du tribunal administratif de Toulouse a rejeté cette demande.<br/>
<br/>
              Par un pourvoi et un mémoire, enregistrés le 20 juillet 2018 et le 18 avril 2019 au secrétariat du contentieux du Conseil d'Etat, M. C... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat, du département du Tarn et de la caisse d'allocations familiales du Tarn la somme de 3 000 euros à verser à la SCP Delamarre, Jehannin, son avocat, au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'action sociale et des familles ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ; <br/>
              - le décret n° 91-1266 du 19 décembre 1991 ;<br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Arnaud Skzryerbak, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Vincent Villette, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Delamarre, Jehannin, avocat de M. C... et à la SCP Lyon-Caen, Thiriez, avocat du département du Tarn ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond qu'à la suite d'un contrôle de la situation de M. A... C... et de Mme D... B..., la caisse d'allocations familiales du Tarn a suspendu leur droit au revenu de solidarité active à compter du 1er août 2015. Par un courrier du 20 novembre 2017, M. C... et Mme B... ont saisi le président du conseil départemental du Tarn d'un recours administratif contre cette décision. M. C... se pourvoit en cassation contre l'ordonnance du 14 juin 2018 par laquelle le président du tribunal administratif de Toulouse a rejeté comme tardive la demande par laquelle il a contesté la décision implicite de rejet née du silence gardé par le président du conseil départemental du Tarn sur ce recours administratif.<br/>
<br/>
              2. L'article 38 du décret du 19 décembre 1991 portant application de la loi n° 91-647 du 10 juillet 1991 relative à l'aide juridique prévoit que : " Lorsqu'une action en justice ou un recours doit être intenté avant l'expiration d'un délai devant les juridictions de première instance (...), l'action ou le recours est réputé avoir été intenté dans le délai si la demande d'aide juridictionnelle s'y rapportant est adressée au bureau d'aide juridictionnelle avant l'expiration dudit délai et si la demande en justice ou le recours est introduit dans un nouveau délai de même durée à compter : / a) De la notification de la décision d'admission provisoire ; / b) De la notification de la décision constatant la caducité de la demande ; / c) De la date à laquelle le demandeur à l'aide juridictionnelle ne peut plus contester la décision d'admission ou de rejet de sa demande en application du premier alinéa de l'article 56 et de l'article 160 ou, en cas de recours de ce demandeur, de la date à laquelle la décision relative à ce recours lui a été notifiée ; / d) Ou, en cas d'admission, de la date, si elle est plus tardive, à laquelle un auxiliaire de justice a été désigné (...) ". Aux termes du deuxième alinéa de l'article 23 de la loi du 10 juillet 1991 : " Les recours contre les décisions du bureau d'aide juridictionnelle peuvent être exercés par l'intéressé lui-même lorsque le bénéfice de l'aide juridictionnelle lui a été refusé, ne lui a été accordé que partiellement ou lorsque ce bénéfice lui a été retiré " et, en vertu du premier alinéa de l'article 56 du décret du 19 décembre 1991, le délai de ce recours " est de quinze jours à compter du jour de la notification de la décision à l'intéressé ".<br/>
<br/>
              3. Il résulte de la combinaison de ces dispositions qu'une demande d'aide juridictionnelle interrompt le délai de recours contentieux et qu'un nouveau délai de même durée recommence à courir à compter de l'expiration d'un délai de quinze jours après la notification à l'intéressé de la décision se prononçant sur sa demande d'aide juridictionnelle ou, si elle est plus tardive, à compter de la date de désignation de l'auxiliaire de justice au titre de l'aide juridictionnelle. Il en va ainsi quel que soit le sens de la décision se prononçant sur la demande d'aide juridictionnelle, qu'elle en ait refusé le bénéfice, qu'elle ait prononcé une admission partielle ou qu'elle ait admis le demandeur au bénéfice de l'aide juridictionnelle totale, quand bien même dans ce dernier cas le ministère public ou le bâtonnier ont, en vertu de l'article 23 de la loi du 10 juillet 1991, seuls vocation à contester une telle décision.<br/>
<br/>
              4. Pour juger que le recours de M. C... était tardif, le président du tribunal administratif de Toulouse s'est fondé sur la circonstance que le tribunal avait été saisi plus de deux mois après la décision du bureau d'aide juridictionnelle du 2 avril 2018 l'admettant au bénéfice de l'aide juridictionnelle totale, qui désignait également l'auxiliaire de justice chargé de l'assister. Il résulte de ce qui a été dit au point précédent qu'en jugeant que le nouveau délai du recours contentieux ouvert à M. C... courait à compter de la décision d'admission, sans rechercher la date à laquelle cette décision lui avait été notifiée, le président du tribunal administratif de Toulouse a commis une erreur de droit. Par suite, son ordonnance doit être annulée.<br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              6. L'exercice, au-delà du délai de recours contentieux contre un acte administratif, d'un recours gracieux tendant au retrait de cet acte ne saurait avoir pour effet de rouvrir le délai de recours. Par suite, le rejet d'une telle demande n'est, en principe, et hors le cas où l'administration a refusé de faire usage de son pouvoir de retirer un acte administratif obtenu par fraude, pas susceptible de recours. <br/>
<br/>
              7. Il résulte de l'instruction que M. C... et Mme B... ont contesté la décision de la caisse d'allocations familiales du Tarn suspendant leur droit au revenu de solidarité active à compter du 1er août 2015 par un recours administratif préalable que le président du conseil départemental du Tarn a rejeté le 17 mars 2016. Cette décision, qui s'est substituée à celle de la caisse d'allocations familiales et a été régulièrement notifiée avant le 22 mars 2016, date de retour aux services du département de l'avis de réception postal signé par M. C..., est devenue définitive à l'expiration du délai de deux mois qui a suivi cette notification. Par suite, le recours administratif formé par M. C... et Mme B... par courrier du 20 novembre 2017, qui contestait à nouveau la décision de la caisse d'allocations familiales du Tarn suspendant leur droit au revenu de solidarité active à compter du 1er août 2015, n'était pas de nature, en cas de refus du président du conseil départemental, à rouvrir le délai de recours contentieux qui était déjà expiré. Dès lors, la demande de M. C... contestant la décision implicite rejetant ce recours ne peut qu'être rejetée comme irrecevable. <br/>
<br/>
              8. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat, du département du Tarn ou de la caisse d'allocations familiales du Tarn. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre une somme au même titre à la charge de M. C....<br/>
<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : L'ordonnance du 14 juin 2018 du président du tribunal administratif de Toulouse est annulée.<br/>
<br/>
Article 2 : La demande présentée au tribunal administratif de Toulouse par M. C... est rejetée.<br/>
<br/>
Article 3 : Le surplus des conclusions du pourvoi et les conclusions présentées par le département du Tarn au titre de l'article L. 761-1 du code de justice administrative sont rejetés.<br/>
<br/>
Article 4 : La présente décision sera notifiée à M. A... C... et au département du Tarn.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-01-07-04 PROCÉDURE. INTRODUCTION DE L'INSTANCE. DÉLAIS. INTERRUPTION ET PROLONGATION DES DÉLAIS. - INTERRUPTION DU DÉLAI DE RECOURS CONTENTIEUX PAR UNE DEMANDE D'AIDE JURIDICTIONNELLE (AJ) - DATE À LAQUELLE LE DÉLAI INTERROMPU RECOMMENCE À COURIR (ART. 38 DU DÉCRET DU 19 DÉCEMBRE 1991) [RJ1] - QUINZE JOURS APRÈS LA NOTIFICATION À L'INTÉRESSÉ DE LA DÉCISION SE PRONONÇANT SUR SA DEMANDE D'AJ [RJ1] OU, SI ELLE EST PLUS TARDIVE, DATE DE DÉSIGNATION DE L'AUXILIAIRE DE JUSTICE, QUEL QUE SOIT LE SENS DE LA DÉCISION SE PRONONÇANT SUR LA DEMANDE D'AJ.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-06-05-09 PROCÉDURE. JUGEMENTS. FRAIS ET DÉPENS. AIDE JURIDICTIONNELLE. - INTERRUPTION DU DÉLAI DE RECOURS CONTENTIEUX PAR UNE DEMANDE D'AIDE JURIDICTIONNELLE (AJ) - DATE À LAQUELLE LE DÉLAI INTERROMPU RECOMMENCE À COURIR (ART. 38 DU DÉCRET DU 19 DÉCEMBRE 1991) [RJ1] - QUINZE JOURS APRÈS LA NOTIFICATION À L'INTÉRESSÉ DE LA DÉCISION SE PRONONÇANT SUR SA DEMANDE D'AJ [RJ1] OU, SI ELLE EST PLUS TARDIVE, DATE DE DÉSIGNATION DE L'AUXILIAIRE DE JUSTICE, QUEL QUE SOIT LE SENS DE LA DÉCISION SE PRONONÇANT SUR LA DEMANDE D'AJ.
</SCT>
<ANA ID="9A"> 54-01-07-04 Il résulte de la combinaison de l'article 38, du premier alinéa de l'article 56 du décret n° 91-1266 du 19 décembre 1991 et du deuxième alinéa de l'article 23 de la loi n° 91-647 du 10 juillet 1991 qu'une demande d'aide juridictionnelle interrompt le délai de recours contentieux et qu'un nouveau délai de même durée recommence à courir à compter de l'expiration d'un délai de quinze jours après la notification à l'intéressé de la décision se prononçant sur sa demande d'aide juridictionnelle ou, si elle est plus tardive, à compter de la date de désignation de l'auxiliaire de justice au titre de l'aide juridictionnelle. Il en va ainsi quel que soit le sens de la décision se prononçant sur la demande d'aide juridictionnelle, qu'elle en ait refusé le bénéfice, qu'elle ait prononcé une admission partielle ou qu'elle ait admis le demandeur au bénéfice de l'aide juridictionnelle totale, quand bien même dans ce dernier cas le ministère public ou le bâtonnier ont, en vertu de l'article 23 de la loi du 10 juillet 1991, seuls vocation à contester une telle décision.</ANA>
<ANA ID="9B"> 54-06-05-09 Il résulte de la combinaison de l'article 38, du premier alinéa de l'article 56 du décret n° 91-1266 du 19 décembre 1991 et du deuxième alinéa de l'article 23 de la loi n° 91-647 du 10 juillet 1991 qu'une demande d'aide juridictionnelle interrompt le délai de recours contentieux et qu'un nouveau délai de même durée recommence à courir à compter de l'expiration d'un délai de quinze jours après la notification à l'intéressé de la décision se prononçant sur sa demande d'aide juridictionnelle ou, si elle est plus tardive, à compter de la date de désignation de l'auxiliaire de justice au titre de l'aide juridictionnelle. Il en va ainsi quel que soit le sens de la décision se prononçant sur la demande d'aide juridictionnelle, qu'elle en ait refusé le bénéfice, qu'elle ait prononcé une admission partielle ou qu'elle ait admis le demandeur au bénéfice de l'aide juridictionnelle totale, quand bien même dans ce dernier cas le ministère public ou le bâtonnier ont, en vertu de l'article 23 de la loi du 10 juillet 1991, seuls vocation à contester une telle décision.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., sous l'empire du droit antérieur, CE, 28 décembre 2016, M.,, n° 394598, T. pp. 871-892.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
