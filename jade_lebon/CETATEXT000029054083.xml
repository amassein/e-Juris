<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029054083</ID>
<ANCIEN_ID>JG_L_2014_06_000000351582</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/05/40/CETATEXT000029054083.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Assemblée, 06/06/2014, 351582, Publié au recueil Lebon</TITRE>
<DATE_DEC>2014-06-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>351582</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Assemblée</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Louis Dutheillet de Lamothe</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Keller</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CEASS:2014:351582.20140606</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la requête, enregistrée au secrétariat du contentieux du Conseil d'Etat le 4 août 2011, présentée par la Fédération des conseils de parents d'élèves des écoles publiques (FCPE), dont le siège est 108-110 avenue Ledru-Rollin, à Paris (75544 Cedex 11), représentée par son président en exercice, et par l'Union nationale lycéenne (UNL), dont le siège est 13 boulevard de Rochechouart, à Paris (75009), représentée par M. B... A... ; la FCPE et l'UNL demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir les articles 2, 3, 5 et 9 du décret n° 2011-728 du 24 juin 2011 relatif à la discipline dans les établissements d'enseignement du second degré, ou, subsidiairement, d'annuler ce décret en son ensemble ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 1 800 euros au titre de l'article L. 761-1 du code justice administrative ; <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu la Constitution, notamment son Préambule ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu le code de l'éducation ; <br/>
<br/>
              Vu le code de justice administrative ; <br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Louis Dutheillet de Lamothe, maître des requêtes, <br/>
<br/>
              - les conclusions de M. Rémi Keller, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article L. 511-1 du code de l'éducation, les obligations des élèves de l'enseignement primaire et secondaire " consistent dans l'accomplissement des tâches inhérentes à leurs études ; elles incluent l'assiduité et le respect des règles de fonctionnement et de la vie collective des établissements " ; que les obligations des élèves des établissements d'enseignement du second degré sont, notamment, précisées, en vertu de l'article R. 511-1 du même code, dans le règlement intérieur de chaque établissement ; qu'en vertu des articles R. 511-12 et suivants du même code, ces élèves s'exposent, en cas de manquement à leurs obligations, aux sanctions disciplinaires énumérées à l'article R. 511-13 de ce code, qui vont de l'avertissement à l'exclusion définitive de l'établissement et qui sont prononcées, dans les collèges et lycées relevant du ministre chargé de l'éducation, par le chef d'établissement ou par un conseil de discipline ; <br/>
<br/>
              2. Considérant que le décret du 24 juin 2011 attaqué a modifié plusieurs articles du code de l'éducation afin de réformer le régime des sanctions disciplinaires prévues par l'article R. 511-13 de ce code ; qu'à cette fin, il prévoit, par ses articles 3 et 5, que le chef d'établissement est tenu d'engager une procédure disciplinaire dans certains cas, crée deux nouvelles sanctions disciplinaires et institue, par ses articles, 2 et 9, une commission éducative qui a pour mission d'examiner la situation des élèves dont le comportement est inapproprié ; <br/>
<br/>
              Sur les conclusions dirigées contre les articles 3 et 5 du décret :<br/>
<br/>
              3. Considérant que la Fédération des conseils de parents d'élèves des écoles publiques et l'Union nationale lycéenne demandent l'annulation pour excès de pouvoir de l'article 3 du décret attaqué qui modifie les dispositions du 5° de l'article R. 421-10 du code de l'éducation, relatif aux compétences du chef d'établissement des établissements publics locaux d'enseignement en matière disciplinaire, pour prévoir qu'il est, à l'égard des élèves, " tenu (...) d'engager une procédure disciplinaire (...) : / a) Lorsque l'élève est l'auteur de violence verbale à l'égard d'un membre du personnel de l'établissement ; / b) Lorsque l'élève commet un acte grave à l'égard d'un membre du personnel ou d'un autre élève " ; que cet article modifie également l'article R. 421-85 du même code relatif aux établissements professionnels maritimes pour y introduire des dispositions analogues ; qu'est également attaqué l'article 5 du même décret qui donne à l'article R. 511-12 du même code la rédaction suivante : " Sauf dans les cas où le chef d'établissement est tenu d'engager une procédure disciplinaire et préalablement à la mise en oeuvre de celle-ci, le chef d'établissement et l'équipe éducative recherchent, dans la mesure du possible, toute mesure utile de nature éducative. " ; <br/>
<br/>
              En ce qui concerne l'opportunité des poursuites : <br/>
<br/>
              4. Considérant que si, dans le silence des textes, l'autorité administrative compétente apprécie l'opportunité des poursuites en matière disciplinaire, aucun principe général du droit ne fait obstacle à ce qu'un texte réglementaire prévoie que, dans certaines hypothèses, des poursuites disciplinaires doivent être engagées ; que la fédération et l'union requérantes ne sont donc pas fondées à soutenir que le décret attaqué aurait méconnu un principe général du droit en faisant obligation aux chefs d'établissement scolaire d'engager les poursuites disciplinaires dans les cas qu'il énumère ; mais que l'obligation ainsi faite aux chefs d'établissement trouve sa limite dans les autres intérêts généraux dont ils ont la charge, notamment dans les nécessités de l'ordre public ; <br/>
<br/>
              En ce qui concerne la méconnaissance du principe de légalité des délits : <br/>
<br/>
              5. Considérant que la fédération et l'union requérantes soutiennent que le décret attaqué, faute de définir de façon claire et précise les termes d'" acte grave " et de " violence verbale ", méconnaît le principe de légalité des délits qui résulterait de l'article 8 de la Déclaration des droits de l'homme et du citoyen ainsi que des stipulations des articles 6 et 7 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
<br/>
              6. Considérant, en premier lieu, qu'il résulte des termes des articles 3 et 5 du décret attaqué cités ci-dessus que ces dispositions ne définissent pas d'obligation dont la méconnaissance constituerait un manquement disciplinaire, mais se bornent à faire référence à certains cas pour lesquels sont instituées des modalités spécifiques d'engagement des poursuites disciplinaires ; que, dès lors, le moyen tiré de la méconnaissance du principe de légalité des délits résultant de l'article 8 de la Déclaration des droits de l'homme ne peut, en tout état de cause, qu'être écarté ; <br/>
<br/>
              7. Considérant, en second lieu, qu'aux termes du 3 de l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales : " Tout accusé a droit notamment à : a) être informé, dans le plus court délai, dans une langue qu'il comprend et d'une manière détaillée, de la nature et de la cause de l'accusation portée contre lui ; (...) " ; qu'aux termes du 1 de l'article 7 de la même convention : " Nul ne peut être condamné pour une action ou une omission qui, au moment où elle a été commise, ne constituait pas une infraction d'après le droit national ou international. " ; <br/>
<br/>
              8. Considérant que, contrairement à ce qui est soutenu, les poursuites et les sanctions prévues à l'article R. 511-13 du code de l'éducation dont les élèves peuvent faire l'objet ne constituent, eu égard tant à leur nature disciplinaire qu'aux conséquences qu'elles emportent sur les élèves, ni des accusations en matière pénale au sens de l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, ni des condamnations au sens de l'article 7 de cette convention ; <br/>
<br/>
              Sur les conclusions dirigées contre les articles 2 et 9 du décret : <br/>
<br/>
              9. Considérant que les dispositions de ces articles créent dans certains établissements d'enseignement du second degré une commission éducative ; qu'aux termes de l'article R. 511-19-1 du code de l'éducation créé par l'article 9 du décret attaqué : " Dans les collèges et les lycées relevant du ministre chargé de l'éducation et dans les établissements publics locaux d'enseignement relevant du ministre chargé de la mer est instituée une commission éducative. / Cette commission, qui est présidée par le chef d'établissement ou son représentant, comprend notamment des personnels de l'établissement, dont au moins un professeur, et au moins un parent d'élève. Sa composition est arrêtée par le conseil d'administration et inscrite dans le règlement intérieur de l'établissement qui fixe les modalités de son fonctionnement. Elle associe, en tant que de besoin, toute personne susceptible d'apporter des éléments permettant de mieux appréhender la situation de l'élève concerné. Elle a pour mission d'examiner la situation d'un élève dont le comportement est inadapté aux règles de vie dans l'établissement et de favoriser la recherche d'une réponse éducative personnalisée. Elle est également consultée en cas d'incidents impliquant plusieurs élèves. (...) " ; que les dispositions du 3° de l'article R. 421-9 et du 3° de l'article R. 421-84 du code de l'éducation, dans leur rédaction issue de l'article 2 du décret attaqué, prévoient que cette commission est présidée par le chef d'établissement ; <br/>
<br/>
              10. Considérant que la fédération et l'union requérantes soutiennent que ces dispositions, dès lors qu'elles ne prévoient pas la représentation des élèves dans la commission éducative, méconnaissent un droit de participation des élèves du second degré, qui découlerait notamment des articles L. 111-3 et L. 511-2 du code de l'éducation ; que toutefois, un tel droit ne découle ni des dispositions invoquées de l'article L. 111-3 du code de l'éducation, qui se borne à définir la communauté éducative, ni de celles de l'article L. 511-2 du même code, qui régit les libertés d'information et d'expression dont jouissent les élèves des établissements scolaires, ni d'aucune autre disposition législative, ni d'aucun principe ; qu'au demeurant, si les dispositions précitées de l'article R. 511-19-1 du code de l'éducation prévoient que la commission doit comprendre au moins un professeur et un parent d'élève, il est loisible au conseil d'administration, qui est compétent pour fixer la composition de cette commission, de prévoir que des représentants des élèves y siégeront ; que, dès lors, le moyen ne peut qu'être écarté ; <br/>
<br/>
              11. Considérant qu'il résulte de l'ensemble de ce qui précède que la Fédération des conseils de parents d'élèves des écoles publiques et l'Union nationale lycéenne ne sont pas fondées à demander l'annulation des dispositions attaquées ; que, dès lors, leur requête doit être rejetée, y compris leurs conclusions présentées au titre de l'article L. 761 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : La requête de la Fédération des conseils de parents d'élèves des écoles publiques et de l'Union nationale lycéenne est rejetée. <br/>
<br/>
Article 2 : La présente décision sera notifiée à la Fédération des conseils de parents d'élèves des écoles publiques, à l'Union nationale lycéenne et au ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche. <br/>
      Copie en sera adressée pour information au Premier ministre.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-04-03-07 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. PRINCIPES GÉNÉRAUX DU DROIT. PRINCIPES INTÉRESSANT L'ACTION ADMINISTRATIVE. - OPPORTUNITÉ DES POURSUITES DISCIPLINAIRES - ABSENCE - CONSÉQUENCE - POSSIBILITÉ POUR UN TEXTE RÉGLEMENTAIRE DE PRÉVOIR UNE OBLIGATION DE POURSUIVRE DANS CERTAINS CAS - LIMITE - AUTRES INTÉRÊTS GÉNÉRAUX DONT L'AUTORITÉ ADMINISTRATIVE COMPÉTENTE A LA CHARGE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">30-02-02 ENSEIGNEMENT ET RECHERCHE. QUESTIONS PROPRES AUX DIFFÉRENTES CATÉGORIES D'ENSEIGNEMENT. ENSEIGNEMENT DU SECOND DEGRÉ. - POUVOIR DISCIPLINAIRE DES CHEFS D'ÉTABLISSEMENT - DISPOSITION RÉGLEMENTAIRE INSTAURANT DANS CERTAINS CAS UNE OBLIGATION DE POURSUIVRE - PRINCIPE GÉNÉRAL D'OPPORTUNITÉ DES POURSUITES Y FAISANT OBSTACLE - ABSENCE - LIMITE - AUTRES INTÉRÊTS GÉNÉRAUX DONT LE CHEF D'ÉTABLISSEMENT A LA CHARGE [RJ1], NOTAMMENT L'ORDRE PUBLIC.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">59-02 RÉPRESSION. DOMAINE DE LA RÉPRESSION ADMINISTRATIVE - POUVOIR DISCIPLINAIRE - PRINCIPE GÉNÉRAL D'OPPORTUNITÉ DES POURSUITES - ABSENCE - CONSÉQUENCE - POSSIBILITÉ POUR UN TEXTE RÉGLEMENTAIRE DE PRÉVOIR UNE OBLIGATION DE POURSUIVRE DANS CERTAINS CAS - LIMITE - AUTRES INTÉRÊTS GÉNÉRAUX DONT L'AUTORITÉ ADMINISTRATIVE COMPÉTENTE A LA CHARGE [RJ1].
</SCT>
<ANA ID="9A"> 01-04-03-07 Si, dans le silence des textes, l'autorité administrative compétente apprécie l'opportunité des poursuites en matière disciplinaire [RJ2], aucun principe général du droit ne fait obstacle à ce qu'un texte réglementaire prévoie que, dans certaines hypothèses, des poursuites disciplinaires doivent être engagées. L'obligation ainsi faite à l'autorité administrative trouve sa limite dans les autres intérêts généraux dont elle a la charge.</ANA>
<ANA ID="9B"> 30-02-02 Si, dans le silence des textes, l'autorité administrative compétente apprécie l'opportunité des poursuites en matière disciplinaire [RJ2], aucun principe général du droit ne fait obstacle à ce qu'un texte réglementaire prévoie que, dans certaines hypothèses, des poursuites disciplinaires doivent être engagées. L'obligation faite aux chefs d'établissement secondaire par le décret n° 2011-728 du 24 juin 2011 d'engager des poursuites à l'encontre des élèves auteurs de violence verbale à l'égard d'un membre du personnel de l'établissement ou d'un acte grave à l'égard d'un membre du personnel ou d'un autre élève trouve sa limite dans les autres intérêts généraux dont le chef d'établissement a la charge, notamment dans les nécessités de l'ordre public.</ANA>
<ANA ID="9C"> 59-02 Si, dans le silence des textes, l'autorité administrative compétente apprécie l'opportunité des poursuites en matière disciplinaire [RJ2], aucun principe général du droit ne fait obstacle à ce qu'un texte réglementaire prévoie que, dans certaines hypothèses, des poursuites disciplinaires doivent être engagées. L'obligation ainsi faite à l'autorité administrative trouve sa limite dans les autres intérêts généraux dont elle a la charge.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Section, 23 février 1979, Ministre de l'équipement c/ Association des « Amis des chemins de ronde », n° 4467, p. 75.,,[RJ2] Rappr. CE, Section, 18 mars 1994, Rimasson, n° 92410, p. 147 ; CE, Section, 30 novembre 2007, M. Tinez et autres, n° 293952, p. 459.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
