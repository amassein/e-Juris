<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043672603</ID>
<ANCIEN_ID>JG_L_2021_06_000000435374</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/67/26/CETATEXT000043672603.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 4ème chambres réunies, 16/06/2021, 435374</TITRE>
<DATE_DEC>2021-06-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>435374</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR>M. Arnaud Skzryerbak</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie Sirinelli</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:435374.20210616</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B... A... a demandé au tribunal administratif de Grenoble d'annuler la décision du 12 novembre 2014 par laquelle le président du conseil général de l'Isère a mis fin à sa prise en charge au titre de l'aide sociale à l'enfance. Par un jugement n° 1503316 du 20 décembre 2017, le tribunal administratif de Grenoble a fait droit à cette demande.<br/>
<br/>
              Par un arrêt n° 18LY00735 du 10 octobre 2019, enregistré le lendemain au secrétariat du contentieux du Conseil d'Etat, la cour administrative d'appel de Lyon a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, le pourvoi, enregistré le 20 février 2018 au greffe de cette cour, présenté par le département de l'Isère.<br/>
<br/>
              Par ce pourvoi et par un mémoire complémentaire enregistré le 11 décembre 2019 au secrétariat du contentieux du Conseil d'Etat, le département de l'Isère demande au Conseil d'Etat : <br/>
<br/>
              1°) à titre principal, d'annuler ce jugement ;<br/>
<br/>
              2°) à titre subsidiaire, de saisir le juge judiciaire d'une question préjudicielle portant sur la caducité de l'ordonnance de placement provisoire du procureur de la République près le tribunal de grande instance de Grenoble du 7 octobre 2014 ;<br/>
<br/>
              3°) réglant l'affaire au fond, de rejeter la demande présentée par M. A... devant le tribunal administratif de Grenoble.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'action sociale et des familles ;<br/>
              - le code civil ;<br/>
              - le code de procédure civile ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Arnaud Skzryerbak, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Marie Sirinelli, rapporteure publique ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Matuchansky, Poupot, Valdelièvre, avocat du département de l'Isère ;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. A..., ressortissant nigérien, a été pris en charge par le service de l'aide sociale à l'enfance du département de l'Isère dans le cadre de l'accueil provisoire d'urgence. Par un courrier du 7 octobre 2014, le président du conseil général de l'Isère en a avisé le procureur de la République près le tribunal de grande instance de Grenoble qui, par une ordonnance du même jour prise sur le fondement de l'article 375-5 du code civil, a décidé en urgence le placement provisoire de M. A.... Par une décision du 12 novembre 2014, le président du conseil général a indiqué à M. A... que, le procureur de la République n'ayant pas saisi le juge des enfants dans le délai qui lui était imparti, son placement provisoire avait cessé et qu'il était mis fin à son hébergement et à sa mise à l'abri. Le département de l'Isère se pourvoit en cassation contre le jugement du 20 décembre 2017 par lequel le tribunal administratif de Grenoble a annulé cette décision.<br/>
<br/>
              2. En premier lieu, d'une part, l'article L. 221-1 du code de l'action sociale et des familles dispose que : " Le service de l'aide sociale à l'enfance est un service non personnalisé du département chargé des missions suivantes : / (...) 4° Pourvoir à l'ensemble des besoins des mineurs confiés au service et veiller à leur orientation (...) ". L'article L. 222-5 du même code prévoit que : " Sont pris en charge par le service de l'aide sociale à l'enfance sur décision du président du conseil départemental : (...) / 3° Les mineurs confiés au service en application du 3° de l'article 375-3 du code civil, des articles 375-5 (...) du même code ". L'article L. 223-2 du même code dispose que : " Sauf si un enfant est confié au service par décision judiciaire ou s'il s'agit de prestations en espèces, aucune décision sur le principe ou les modalités de l'admission dans le service de l'aide sociale à l'enfance ne peut être prise sans l'accord écrit des représentants légaux ou du représentant légal du mineur ou du bénéficiaire lui-même s'il est mineur émancipé. / En cas d'urgence et lorsque le représentant légal du mineur est dans l'impossibilité de donner son accord, l'enfant est recueilli provisoirement par le service qui en avise immédiatement le procureur de la République. / (...) Si, dans le cas prévu au deuxième alinéa du présent article, l'enfant n'a pas pu être remis à sa famille ou le représentant légal n'a pas pu ou a refusé de donner son accord dans un délai de cinq jours, le service saisit également l'autorité judiciaire en vue de l'application de l'article 375-5 du code civil (...) ".<br/>
<br/>
              3. D'autre part, aux termes de l'article 375 du code civil : " Si la santé, la sécurité ou la moralité d'un mineur non émancipé sont en danger, ou si les conditions de son éducation ou de son développement physique, affectif, intellectuel et social sont gravement compromises, des mesures d'assistance éducative peuvent être ordonnées par justice à la requête des père et mère conjointement, ou de l'un d'eux, de la personne ou du service à qui l'enfant a été confié ou du tuteur, du mineur lui-même ou du ministère public (...) ". Aux termes de l'article 375-1 du même code : " Le juge des enfants est compétent, à charge d'appel, pour tout ce qui concerne l'assistance éducative (...) ". L'article 375-3 du même code dispose que : " Si la protection de l'enfant l'exige, le juge des enfants peut décider de le confier : / (...) 3° A un service départemental de l'aide sociale à l'enfance ". Aux termes de l'article 375-5 du même code : " A titre provisoire mais à charge d'appel, le juge peut, pendant l'instance, soit ordonner la remise provisoire du mineur à un centre d'accueil ou d'observation, soit prendre l'une des mesures prévues aux articles 375-3 et 375-4. / En cas d'urgence, le procureur de la République du lieu où le mineur a été trouvé a le même pouvoir, à charge de saisir dans les huit jours le juge compétent, qui maintiendra, modifiera ou rapportera la mesure. (...) ". L'article 1184 du code de procédure civile dispose que : " (...) Lorsque le juge est saisi, conformément aux dispositions du second alinéa de l'article 375-5 du code civil, par le procureur de la République ayant ordonné en urgence une mesure de placement provisoire, il convoque les parties et statue dans un délai qui ne peut excéder quinze jours à compter de sa saisine, faute de quoi le mineur est remis, sur leur demande, à ses parents ou tuteur, ou à la personne ou au service à qui il était confié ".<br/>
<br/>
              4. Il résulte de ces dispositions que, lorsqu'il est saisi par un mineur d'une demande d'admission à l'aide sociale à l'enfance et que le ou les représentants légaux de celui-ci ne sont pas en mesure, notamment en raison de leur éloignement géographique, de donner leur accord à cette admission, le président du conseil départemental peut seulement, au-delà de la période d'accueil provisoire de cinq jours prévue par l'article L. 223-2 du code de l'action sociale et des familles, décider de saisir l'autorité judiciaire, mais ne peut en aucun cas décider d'admettre le mineur à l'aide sociale à l'enfance sans que l'autorité judiciaire ne l'ait ordonné. Lorsque le juge des enfants ou le procureur de la République a ordonné en urgence une mesure de placement provisoire en application de l'article 375-5 du code civil, il incombe aux autorités du département de prendre en charge l'hébergement et de pourvoir aux besoins du mineur pour toute la durée de cette mesure.<br/>
<br/>
              5. Il en résulte également que le procureur de la République qui, en cas d'urgence, ordonne le placement provisoire du mineur sur le fondement du second alinéa de l'article 375-5 du code civil le fait à charge de saisir dans les huit jours le juge des enfants en vue que celui-ci maintienne, modifie ou rapporte cette mesure. A défaut de saisine du juge des enfants dans ce délai par le procureur de la République, la mesure de placement provisoire ordonnée par ce dernier prend fin. L'article 375 du code civil autorise le mineur à solliciter lui-même le juge des enfants pour que soient prononcées, le cas échéant, les mesures d'assistance éducative que sa situation nécessite.<br/>
<br/>
              6. Par suite, en jugeant que l'absence de saisine du juge des enfants par le procureur de la République près le tribunal de grande instance de Grenoble dans le délai prévu par l'article 375-5 du code civil était restée sans incidence sur la mesure de placement provisoire qu'il avait ordonnée à l'égard de M. A... et en en déduisant que le président du conseil départemental de l'Isère ne pouvait mettre fin à la prise en charge de l'intéressé tant qu'il n'avait pas été mis fin à cette mesure par l'autorité judiciaire, le tribunal administratif de Grenoble a commis une erreur de droit.<br/>
<br/>
              7. En second lieu, lorsqu'il statue sur un recours dirigé contre une décision refusant une prise en charge par le service de l'aide sociale à l'enfance ou mettant fin à une telle prise en charge, il appartient au juge administratif, eu égard tant à la finalité de son intervention qu'à sa qualité de juge de plein contentieux, non de se prononcer sur les éventuels vices propres de la décision attaquée, mais d'examiner la situation de l'intéressé, en tenant compte de l'ensemble des circonstances de fait qui résultent de l'instruction et, notamment, du dossier qui lui est communiqué en application de l'article R. 772-8 du code de justice administrative. Au vu de ces éléments, il lui appartient d'annuler, s'il y a lieu, cette décision en accueillant lui-même la demande de l'intéressé s'il apparaît, à la date à laquelle il statue, eu égard à la marge d'appréciation dont dispose le président du conseil départemental dans leur mise en oeuvre, qu'un défaut de prise en charge conduirait à une méconnaissance des dispositions du code de l'action sociale et des familles relatives à la protection de l'enfance et en renvoyant l'intéressé devant l'administration afin qu'elle précise les modalités de cette prise en charge sur la base des motifs de son jugement.<br/>
<br/>
              8. Il résulte des pièces du dossier soumis aux juges du fond, en particulier des propres déclarations de M. A..., que ce dernier est né le 23 octobre 1998, de sorte qu'il a atteint sa majorité au cours de l'instance devant le tribunal administratif de Grenoble. Il n'était ainsi plus susceptible de faire l'objet d'une prise en charge en tant que mineur par l'aide sociale à l'enfance à la date à laquelle le tribunal administratif a statué, ce qu'il appartenait au tribunal de relever d'office. En omettant de le faire et en statuant sur le litige dont il était saisi, alors que ce litige avait à la date de son jugement perdu son objet, le tribunal a commis une erreur de droit. <br/>
<br/>
              9. Il résulte de tout ce qui précède, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, que le département de l'Isère est fondé à demander l'annulation du jugement qu'il attaque.<br/>
<br/>
              10. Dans les circonstances de l'espèce, il y a lieu de régler l'affaire au fond, en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              11. Ainsi qu'il a été dit au point 8, les conclusions de M. A... dirigées contre la décision du 12 novembre 2014 par laquelle le président du conseil départemental de l'Isère a mis fin à sa prise en charge au titre de l'aide sociale à l'enfance sont devenues sans objet, postérieurement à l'introduction de sa demande devant le tribunal administratif de Grenoble. Il n'y a, dès lors, plus lieu d'y statuer.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Grenoble du 20 décembre 2017 est annulé.<br/>
Article 2 : Il n'y a pas lieu de statuer sur la demande présentée par M. A... devant ce tribunal.<br/>
Article 3 : La présente décision sera notifiée au département de l'Isère et à M. B... A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">04-02-02 AIDE SOCIALE. DIFFÉRENTES FORMES D'AIDE SOCIALE. AIDE SOCIALE À L'ENFANCE. - COMPÉTENCE EXCLUSIVE DE L'AUTORITÉ JUDICIAIRE POUR ADMETTRE UN MINEUR À L'ASE LORSQUE SES REPRÉSENTANTS LÉGAUX NE SONT PAS EN MESURE DE DONNER LEUR ACCORD [RJ1] - CAS D'UN PLACEMENT PROVISOIRE ORDONNÉ EN URGENCE PAR LE PROCUREUR (ART. 375-5 DU CODE CIVIL) - DÉFAUT DE SAISINE DU JUGE DES ENFANTS DANS LES HUIT JOURS - CONSÉQUENCE - FIN DE LA MESURE DE PLACEMENT PROVISOIRE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">135-03-02-01-01 COLLECTIVITÉS TERRITORIALES. DÉPARTEMENT. ATTRIBUTIONS. COMPÉTENCES TRANSFÉRÉES. ACTION SOCIALE. - COMPÉTENCE EXCLUSIVE DE L'AUTORITÉ JUDICIAIRE POUR ADMETTRE UN MINEUR À L'ASE LORSQUE SES REPRÉSENTANTS LÉGAUX NE SONT PAS EN MESURE DE DONNER LEUR ACCORD [RJ1] - CAS D'UN PLACEMENT PROVISOIRE ORDONNÉ EN URGENCE PAR LE PROCUREUR (ART. 375-5 DU CODE CIVIL) - DÉFAUT DE SAISINE DU JUGE DES ENFANTS DANS LES HUIT JOURS - CONSÉQUENCE - FIN DE LA MESURE DE PLACEMENT PROVISOIRE.
</SCT>
<ANA ID="9A"> 04-02-02 Il résulte, d'une part, des articles L. 221-1, L. 222-5 et L. 223-2 du code de l'action sociale et des familles (CASF), d'autre part, des articles 375, 375-1, 375-3 et 375-5 du code civil et 1184 du code de  procédure civile (CPC) que, lorsqu'il est saisi par un mineur d'une demande d'admission à l'aide sociale à l'enfance (ASE) et que le ou les représentants légaux de celui-ci ne sont pas en mesure, notamment en raison de leur éloignement géographique, de donner leur accord à cette admission, le président du conseil départemental peut seulement, au-delà de la période d'accueil provisoire de cinq jours prévue par l'article L. 223-2 du CASF, décider de saisir l'autorité judiciaire, mais ne peut en aucun cas décider d'admettre le mineur à l'ASE sans que l'autorité judiciaire ne l'ait ordonné.,,,Lorsque le juge des enfants ou le procureur de la République a ordonné en urgence une mesure de placement provisoire en application de l'article 375-5 du code civil, il incombe aux autorités du département de prendre en charge l'hébergement et de pourvoir aux besoins du mineur pour toute la durée de cette mesure. Il en résulte également que le procureur de la République qui, en cas d'urgence, ordonne le placement provisoire du mineur sur le fondement du second alinéa de l'article 375-5 du code civil le fait à charge de saisir dans les huit jours le juge des enfants en vue que celui-ci maintienne, modifie ou rapporte cette mesure.,,,A défaut de saisine du juge des enfants dans ce délai par le procureur de la République, la mesure de placement provisoire ordonnée par ce dernier prend fin. L'article 375 du code civil autorise le mineur à solliciter lui-même le juge des enfants pour que soient prononcées, le cas échéant, les mesures d'assistance éducative que sa situation nécessite.</ANA>
<ANA ID="9B"> 135-03-02-01-01 Il résulte, d'une part, des articles L. 221-1, L. 222-5 et L. 223-2 du code de l'action sociale et des familles (CASF), d'autre part, des articles 375, 375-1, 375-3 et 375-5 du code civil et 1184 du code de  procédure civile (CPC) que, lorsqu'il est saisi par un mineur d'une demande d'admission à l'aide sociale à l'enfance (ASE) et que le ou les représentants légaux de celui-ci ne sont pas en mesure, notamment en raison de leur éloignement géographique, de donner leur accord à cette admission, le président du conseil départemental peut seulement, au-delà de la période d'accueil provisoire de cinq jours prévue par l'article L. 223-2 du CASF, décider de saisir l'autorité judiciaire, mais ne peut en aucun cas décider d'admettre le mineur à l'ASE sans que l'autorité judiciaire ne l'ait ordonné.,,,Lorsque le juge des enfants ou le procureur de la République a ordonné en urgence une mesure de placement provisoire en application de l'article 375-5 du code civil, il incombe aux autorités du département de prendre en charge l'hébergement et de pourvoir aux besoins du mineur pour toute la durée de cette mesure. Il en résulte également que le procureur de la République qui, en cas d'urgence, ordonne le placement provisoire du mineur sur le fondement du second alinéa de l'article 375-5 du code civil le fait à charge de saisir dans les huit jours le juge des enfants en vue que celui-ci maintienne, modifie ou rapporte cette mesure.,,,A défaut de saisine du juge des enfants dans ce délai par le procureur de la République, la mesure de placement provisoire ordonnée par ce dernier prend fin. L'article 375 du code civil autorise le mineur à solliciter lui-même le juge des enfants pour que soient prononcées, le cas échéant, les mesures d'assistance éducative que sa situation nécessite.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 1er juillet 2015, Département du Nord, n° 386769, T. pp. 551-594-791.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
