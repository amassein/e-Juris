<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026207098</ID>
<ANCIEN_ID>JG_L_2012_06_000000353655</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/20/70/CETATEXT000026207098.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème et 7ème sous-sections réunies, 04/06/2012, 353655</TITRE>
<DATE_DEC>2012-06-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>353655</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème et 7ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Edmond Honorat</PRESIDENT>
<AVOCATS/>
<RAPPORTEUR>Mme Catherine Chadelat</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Damien Botteghi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:353655.20120604</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu 1°), sous le n° 353655, la requête, enregistrée le 26 octobre 2011 au secrétariat du contentieux du Conseil d'Etat, présentée par la COMMUNE DE CLAMART, représentée par son maire ; la COMMUNE DE CLAMART demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'article 1er du décret n° 2011-1011 du 24 août 2011 portant approbation du schéma d'ensemble du réseau de transport public du Grand Paris, en ce que ce schéma attribue à la gare ayant pour aire de couverture les communes de Clamart, Issy-les-Moulineaux, Vanves et Malakoff, la dénomination " Fort d'Issy-Vanves-Clamart " ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement à la COMMUNE DE CLAMART de la somme de 1 400 euros sur le fondement de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu 2°), sous le n° 353661, la requête, enregistrée le 26 octobre 2011 au secrétariat du contentieux du Conseil d'Etat, présentée par la COMMUNE DE DRANCY, représentée par son maire ; la COMMUNE DE DRANCY demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2011-1011 du 24 août 2011 portant approbation du schéma d'ensemble du réseau de transport public du Grand Paris ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement à la COMMUNE DE DRANCY de la somme de 2 000 euros  au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
<br/>
              Vu 3°), sous le n° 357235, l'ordonnance n° 1113281/7-1 du 29 février 2012, enregistrée le 29 février 2012 au secrétariat du contentieux du Conseil d'Etat, par laquelle le président du tribunal administratif de Paris a transmis au Conseil d'Etat, en application de l'article R. 341-2 du code de justice administrative, la requête présentée à ce tribunal par la COMMUNE DE DRANCY, représentée par son maire ;  <br/>
<br/>
              Vu la requête, enregistrée le 1er août 2011 au greffe du tribunal administratif de Paris, présentée par la COMMUNE DE DRANCY, représentée par son maire ; la COMMUNE DE DRANCY demande :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la délibération en date du 26 mai 2011 du conseil de surveillance de la société du Grand Paris adoptant l'acte motivé prévu au V de l'article 3 de la loi n° 2010-597 du 3 juin 2010 relative au Grand Paris ;<br/>
<br/>
              2°) de mettre à la charge de la société du Grand Paris la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu la Constitution du 4 octobre 1958 ;<br/>
<br/>
              Vu le code de l'environnement ; <br/>
<br/>
              Vu la loi n° 2000-321 du 12 avril 2000 ;   <br/>
<br/>
              Vu la loi n° 2010-597 du 3 juin 2010 ;<br/>
<br/>
              Vu le décret n° 2010-1133 du 28 septembre 2010 ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Catherine Chadelat, Conseiller d'Etat,<br/>
<br/>
              - les conclusions de M. Damien Botteghi, Rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant que les requêtes n° 353655 et 353661 sont dirigées contre le même décret ; que la requête n° 357235 comporte à juger des questions connexes ; qu'il y lieu de les joindre pour statuer par une seule décision ; <br/>
<br/>
              Sur la légalité du décret du 24 août 2011 :<br/>
<br/>
              En ce qui concerne le défaut de contreseing du ministre chargé des collectivités territoriales : <br/>
<br/>
              Considérant qu'aux termes de l'article 22 de la Constitution : " Les actes du Premier Ministre sont contresignés, le cas échéant, par les ministres chargés de leur exécution " ; que les ministres chargés de l'exécution sont ceux qui ont compétence pour signer les mesures que comporte nécessairement l'exécution des actes en cause ; qu'en l'espèce, aucune disposition du décret attaqué n'implique nécessairement l'intervention de mesures réglementaires ou individuelles que le ministre de l'intérieur, de l'outre mer, des collectivités territoriales et de l'immigration aurait été compétent pour signer ou contresigner ; que, par suite, le moyen tiré de ce que le décret attaqué n'aurait pas été contresigné par tous les ministres chargés de son exécution doit être écarté ; <br/>
<br/>
              En ce qui concerne l'irrégularité de la consultation du Conseil d'Etat : <br/>
<br/>
              Considérant qu'il ressort des mentions du décret attaqué que le Conseil d'Etat a été régulièrement consulté sur le décret attaqué, auquel était annexé le schéma d'ensemble du réseau de transport public qu'il approuve ; que, par suite, le moyen tiré de l'irrégularité de la consultation du Conseil d'Etat doit être écarté ;  <br/>
<br/>
              En ce qui concerne l'irrégularité des décisions de la commission nationale du débat public :<br/>
<br/>
              Considérant que les décisions des 2 juin et 7 juillet 2010 par lesquelles la commission nationale du débat public a procédé à la nomination du président et des membres d'une des deux commissions particulières du débat public qu'elle a mises en place et a fixé les dates de celui-ci, mentionnent qu'elles ont été prises par la commission nationale, ainsi qu'en atteste leur en-tête ; que la seule circonstance que la qualité de la personne qui les a signées n'ait pas été indiquée est sans influence sur la légalité de ces décisions ; que, par suite, le moyen tiré de ce que les décisions en cause contreviendraient à l'article 4 de la loi du 12 avril 2000 doit être écarté ; <br/>
<br/>
              En ce qui concerne le contenu du dossier soumis au débat public :<br/>
<br/>
              Considérant qu'aux termes de l'article 3 de la loi du 3 juin 2010 relative au Grand Paris : " I.- (...) Le public est associé au processus d'élaboration du schéma [d'ensemble du réseau de transport public du Grand Paris]. A cette fin, un débat public est organisé par la Commission nationale du débat public (...) / Le débat public porte sur l'opportunité, les objectifs et les principales caractéristiques du projet de réseau de transport public du Grand Paris. / II.- Le dossier destiné au public est établi par l'établissement public Société du Grand Paris. Il comporte tous les éléments nécessaires à l'information du public, notamment : / - les objectifs et les principales caractéristiques du projet de réseau de transport public du Grand Paris (...) ; / -l'exposé des enjeux socio-économiques, y compris au regard du rayonnement international de la région d'Ile-de-France et de la France ; / - l'estimation du coût et les modes de financement envisagés ; /- les prévisions de trafic ; /- l'analyse des incidences sur l'aménagement du territoire ; /- le rapport environnemental et l'avis de la formation d'autorité environnementale du Conseil général de l'environnement et du développement durable prévus par les articles L. 122-6 et L. 122-7 du code de l'environnement " ;<br/>
<br/>
              Considérant que si la COMMUNE DE DRANCY soutient que le dossier du débat public ne permettait pas d'assurer pleinement l'information du public sur l'impact des projets à l'égard des prévisions de trafic, de l'environnement et de la localisation de l'urbanisation nouvelle, sur leur coût et leur financement ainsi que sur la tarification applicables aux trajets , il ressort des rubriques de ce dossier qu'il a été procédé à l'analyse de l'impact sur les trafics présents et à venir au regard des différents modes de transport, sur l'accessibilité des territoires et sur les fonctions urbaines actuelles et futures au regard des secteurs d'implantation des gares du réseau ; que, contrairement à ce que soutient la COMMUNE DE DRANCY, le rapport de l'autorité environnementale, joint en annexe du dossier du débat public, ne révèle pas des insuffisances du dossier du débat public en matière d'impact environnemental mais se borne à souligner l'opportunité de débattre et approfondir certains aspects lors de la consultation du public ; que le coût des projets avec leurs variantes est précisé ainsi que les modes de financement ; que le dossier n'avait pas à comporter les tarifs envisagés pour les usagers des transports publics ; que le contenu du dossier répond ainsi à l'objectif d'information  appropriée du public et satisfait aux prescriptions du II de l'article 3 de la loi du 3 juin 2010 ;<br/>
<br/>
              En ce qui concerne les modifications du projet après le débat public :<br/>
<br/>
              Considérant qu'aux termes de l'article 3 de la loi du 3 juin 2010 : " I.- Le schéma d'ensemble du réseau de transport public du Grand Paris (...) est établi après avis des collectivités territoriales et de leurs établissements publics de coopération intercommunale, s'ils sont compétents en matière d'urbanisme ou d'aménagement, de l'association des maires d'Ile-de-France, du syndicat mixte Paris-Métropole, du Syndicat des transports d'Ile-de-France et de l'atelier international du Grand Paris. / V.- Dans un délai de deux mois à compter de la date de clôture du débat public, le président de la Commission nationale du débat public en publie le compte rendu et le bilan, auxquels sont joints les avis exprimés par les personnes visées au second alinéa du III. (...). / Dans un délai de deux mois suivant la publication de ce bilan, l'établissement public Société du Grand Paris, par un acte motivé qui est publié, indique les conséquences qu'il tire de ce bilan pour le schéma d'ensemble qui a fait l'objet du débat public. Cet acte fait notamment état des modalités de prise en compte des avis exprimés par les personnes visées au second alinéa du III. Il précise le schéma d'ensemble retenu et les modifications éventuellement apportées ainsi que les conditions prévues pour sa mise en oeuvre... " ; qu'enfin, selon les dispositions du II de l'article 2 de la même loi, le schéma d'ensemble ainsi retenu est approuvé par un décret en Conseil d'Etat ; <br/>
<br/>
              Considérant qu'il résulte des dispositions précitées que le maître d'ouvrage est fondé à adapter les orientations de son projet pour tenir compte, notamment, des observations faites au cours du débat public et des avis des collectivités territoriales concernées et opérer un choix entre les différentes variantes dont le projet soumis au débat public peut être assorti ; <br/>
<br/>
              Considérant qu'il ressort des pièces du dossier que deux projets distincts du nouveau réseau de transport public d'Ile-de-France ont été soumis à débat public, le projet Arc express et le projet du Grand Paris ; que ces projets ont été fusionnés à la suite du protocole d'accord conclu entre l'Etat et la région Ile-de-France le 26 janvier 2011 ; que le projet retenu reprend des éléments de chacun des deux schémas soumis à l'enquête, dans la continuité desquels il se situe ; qu'en particulier, les trois arcs de tracé retenus pour le sud, nord et ouest intègrent les éléments communs aux deux projets, auxquels ont été ajoutés des éléments du projet Arc express relatifs à l'arc est ; que les modifications apportées n'ont fait que tirer, pour l'essentiel,  les conséquences du débat public, au cours duquel nombre d'observations ont été émises en faveur d'une fusion des deux projets soumis à l'enquête, et sont restées dans la limite de ce qu'autorisent les dispositions précitées de l'article 3 de la loi du 3 juin 2010 ; qu'au surplus, trois réunions publiques se sont tenues après le dépôt du protocole du 26 janvier 2011, qui ont permis au public de s'exprimer sur celui-ci ; que, par suite, le schéma d'ensemble du réseau de transport public du Grand Paris a pu être approuvé par le décret attaqué sans qu'il soit procédé à une nouvelle concertation publique et à une nouvelle consultation pour avis des  collectivités territoriales concernées ;     <br/>
<br/>
              En ce qui concerne l'erreur manifeste d'appréciation :<br/>
<br/>
              Considérant, d'une part, qu'il ressort des pièces du dossier que le schéma d'ensemble approuvé par le décret attaqué établit, par les trois lignes de transport qu'il retient, un équilibre global du nouveau réseau de transports publics d'Ile-de-France ; que la seule circonstance que la desserte de la COMMUNE DE DRANCY soit moins favorable que celle envisagée dans l'un des schémas initialement élaborés, n'est pas de nature à permettre de considérer que l'approbation du schéma dans son ensemble par le décret attaqué serait entachée d'une erreur manifeste d'appréciation ;  <br/>
<br/>
              Considérant, d'autre part, que la dénomination de " Fort d'Issy-Vanves-Clamart " retenue par le schéma d'ensemble approuvé par le décret attaqué pour la gare desservant les quatre communes de Clamart, Issy-les-Moulineaux, Vanves et Malakoff pour la correspondance de la nouvelle ligne de métro automatique avec le Transilien N, n'est, en tout état de cause, pas entachée d'erreur manifeste d'appréciation ;<br/>
<br/>
              En ce qui concerne le détournement de pouvoir :<br/>
<br/>
              Considérant, que le détournement de pouvoir allégué par la COMMUNE DE CLAMART dans le choix de la dénomination " Fort d'Issy-Vanves-Clamart " de la gare desservant la correspondance de la nouvelle ligne de métro automatique avec le Transilien N n'est pas établi ;  <br/>
<br/>
              Considérant qu'il résulte de ce qui précède que la COMMUNE DE DRANCY et la COMMUNE DE CLAMART ne sont pas fondées à demander l'annulation du décret ou des dispositions de ce décret qu'elles attaquent ;<br/>
<br/>
              Sur la légalité de la délibération du 26 mai 2011 de la Société du Grand Paris :<br/>
<br/>
              Considérant que, par délibération du 26 mai 2011, le conseil de surveillance de la Société du Grand Paris a adopté l'acte motivé prévu au V de l'article 3 de la loi du 3 juin 2010 qui expose les conséquences que le maître d'ouvrage a entendu tirer du débat public pour le schéma d'ensemble du réseau de transport public et qui arrête ce schéma en précisant les conditions de sa mise en oeuvre ;  <br/>
<br/>
              Considérant qu'au soutien de sa demande d'annulation de cette délibération, la COMMUNE DE DRANCY invoque des moyens identiques à ceux qu'elle a présentés à l'appui de sa requête dirigée contre le décret du 24 août 2011 portant approbation du schéma d'ensemble du réseau de transport public du Grand Paris ; que, pour les motifs indiqués ci-dessus, ces moyens doivent également être écartés ; qu'il s'ensuit que la COMMUNE DE DRANCY n'est pas fondée à demander l'annulation de la délibération qu'elle attaque ; <br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative : <br/>
<br/>
              Considérant que ces dispositions font obstacle à ce que soit mis à la charge de l'Etat, qui n'est pas la partie perdante dans la présente instance, le versement à la COMMUNE DE CLAMART et à la COMMUNE DE DRANCY des sommes que celles-ci demandent au titre des frais exposés par elles et non compris dans les dépens ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la COMMUNE DE CLAMART et les requêtes de la COMMUNE DE DRANCY sont rejetées. <br/>
<br/>
Article 2 : La présente décision sera notifiée à la COMMUNE DE CLAMART, à la COMMUNE DE DRANCY, au Premier ministre, à la ministre de l'égalité des territoires et du logement et à la ministre de l'écologie, du développement durable et de l'énergie.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-07-02-04 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. CONTRÔLE DU JUGE DE L'EXCÈS DE POUVOIR. APPRÉCIATIONS SOUMISES À UN CONTRÔLE RESTREINT. - SCHÉMA D'ENSEMBLE DU RÉSEAU DE TRANSPORT PUBLIC DU GRAND PARIS - APPRÉCIATION D'ENSEMBLE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">65-05-01 TRANSPORTS. COORDINATION DES TRANSPORTS. PLAN DE DÉPLACEMENT URBAIN. - SCHÉMA D'ENSEMBLE DU RÉSEAU DE TRANSPORT PUBLIC DU GRAND PARIS - CONTRÔLE DU JUGE DE L'EXCÈS DE POUVOIR - CONTRÔLE RESTREINT SUR LE SCHÉMA PRIS DANS SON ENSEMBLE.
</SCT>
<ANA ID="9A"> 54-07-02-04 Le juge de l'excès de pouvoir exerce un contrôle de l'erreur manifeste sur le schéma de transport public du Grand Paris, pris dans son ensemble.</ANA>
<ANA ID="9B"> 65-05-01 Le juge de l'excès de pouvoir exerce un contrôle de l'erreur manifeste sur le schéma de transport public du Grand Paris, pris dans son ensemble.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
