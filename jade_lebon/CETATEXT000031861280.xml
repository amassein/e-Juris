<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031861280</ID>
<ANCIEN_ID>JG_L_2015_12_000000384524</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/86/12/CETATEXT000031861280.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème / 7ème SSR, 23/12/2015, 384524</TITRE>
<DATE_DEC>2015-12-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>384524</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème / 7ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER ; SCP GATINEAU, FATTACCINI</AVOCATS>
<RAPPORTEUR>Mme Sophie-Caroline de Margerie</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Béatrice Bourgeois-Machureau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:384524.20151223</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              L'Union de la publicité extérieure a demandé au tribunal administratif de Lille d'annuler les décisions de la communauté urbaine de Lille métropole rejetant implicitement ses demandes des 20 septembre et 21 décembre 2010 tendant à l'abrogation de l'arrêté municipal du 23 octobre 2007 portant règlement local de la publicité des communes de Lille, Lomme et Hellemmes. Par un jugement n° 1005779-1101792 du 6 juin 2013, le tribunal administratif de Lille a annulé ces décisions et a enjoint à la communauté urbaine de Lille métropole d'abroger cet arrêté. <br/>
<br/>
              Par un arrêt n° 13DA01152 du 10 juillet 2014, la cour administrative d'appel de Douai a, à la demande de la commune de Lille et de la communauté urbaine de Lille métropole, annulé ce jugement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 16 septembre et 15 décembre 2014 et 22 juin 2015 au secrétariat du contentieux du Conseil d'Etat, l'Union de la publicité extérieure demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de la commune de Lille et de la communauté urbaine de Lille métropole ; <br/>
<br/>
              3°) de mettre à la charge de la commune de Lille et de la communauté urbaine de Lille métropole le versement de la somme de 4 500 euros en application de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'environnement ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie-Caroline de Margerie, conseiller d'Etat,<br/>
<br/>
              - les conclusions de Mme Béatrice Bourgeois-Machureau, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gatineau, Fattaccini, avocat de l'Union de la publicité extérieure (UPE), et à la SCP Foussard, Froger, avocat de la commune de Lille et de la communauté urbaine de Lille métropole ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant que l'Union de la publicité extérieure (UPE) demande l'annulation de l'arrêt du 10 juillet 2014 de la cour administrative d'appel de Douai annulant le jugement du 6 juin 2013 par lequel le tribunal administratif de Lille a annulé les décisions de la communauté urbaine de Lille métropole rejetant implicitement les demandes des 20 septembre et 21 décembre 2010 de l'Union de la publicité extérieure tendant à l'abrogation de l'arrêté municipal du 23 octobre 2007 portant règlement local de la publicité des communes de Lille, Lomme et Hellemmes ; <br/>
<br/>
              2.	Considérant qu'aux termes de l'article L. 581-14 du code de l'environnement, dans sa rédaction en vigueur à la date de la décision attaquée : " La délimitation des zones de publicité autorisée, des zones de publicité restreinte ou des zones de publicité élargie, ainsi que les prescriptions qui s'y appliquent, sont établies à la demande du conseil municipal. / Le projet de réglementation spéciale est préparé par un groupe de travail dont la composition est fixée par arrêté préfectoral. Il est présidé par le maire qui, en cette qualité, dispose d'une voix prépondérante. Il comprend, en nombre égal, des membres du conseil municipal et éventuellement un représentant de l'assemblée délibérante de l'organisme intercommunal compétent en matière d'urbanisme, d'une part, et, d'autre part, des représentants des services de l'Etat. (...) Le projet établi par le groupe de travail (...) est arrêté par le maire après délibération du conseil municipal (...) " ; qu'il résulte de ces dispositions que le groupe de travail chargé de préparer le projet de réglementation de publicité doit comporter un représentant de l'assemblée délibérante d'un organisme intercommunal lorsque cet organisme détient, soit en application des textes le régissant, soit en raison d'un transfert de compétences effectué à son profit, la compétence d'élaboration des documents d'urbanisme de la commune ; qu'en revanche, ces dispositions n'imposent pas de faire siéger dans le groupe de travail un organisme intercommunal exerçant d'autres compétences en matière d'urbanisme que celles mentionnées ci-dessus ;<br/>
<br/>
              3.	Considérant, en premier lieu, que si les actes administratifs doivent être pris selon les formes et conformément aux procédures prévues par les lois et règlements, un vice affectant le déroulement d'une procédure administrative préalable, suivie à titre obligatoire ou facultatif, n'est de nature à entacher d'illégalité la décision prise que s'il ressort des pièces du dossier qu'il a été susceptible d'exercer, en l'espèce, une influence sur le sens de la décision prise ou qu'il a privé les intéressés d'une garantie ;<br/>
<br/>
              4. Considérant qu'après avoir constaté que la communauté urbaine de Lille, qui détenait la compétence d'élaboration des documents d'urbanisme pour le territoire des communes la composant, n'avait pas été représentée au sein du groupe de travail chargé de  préparer le règlement local de publicité des communes de Lille, Lomme et Hellemmes, la cour a jugé que l'arrêté du 23 octobre 2007 portant règlement local de la publicité était intervenu au terme d'une procédure irrégulière ; qu'elle a néanmoins estimé que c'est à tort que les premiers juges s'étaient fondés sur le motif tiré de ce vice de procédure pour annuler le rejet des demandes d'abrogation de l'arrêté du 23 octobre 2007 ; qu'elle s'est fondée, d'une part, sur le fait que le groupe de travail était présidé par le maire de la commune de Lille, vice-président de la communauté urbaine, et comprenait notamment quatre membres du conseil municipal également membres de la communauté urbaine et, d'autre part, sur le fait que la cohérence du règlement local de publicité avec le plan local d'urbanisme de la communauté urbaine et la politique d'urbanisme menée par cette dernière n'était pas contestée, et en a déduit que, dans les circonstances de l'espèce, l'absence de désignation d'un représentant de la communauté urbaine dans le groupe de travail n'avait pas été susceptible d'exercer une influence sur le document final ni que ce vice aurait privé la communauté urbaine d'une garantie ; qu'en statuant ainsi, la cour, qui n'a pas méconnu les règles déterminant la date à laquelle se place le juge pour contrôler la légalité de l'acte dont il est saisi, n'a pas entaché son arrêt d'erreur de droit et s'est livrée à une appréciation souveraine exempte de dénaturation ;<br/>
<br/>
              5. Considérant, en deuxième lieu, qu'en jugeant que le maire de Lille était compétent pour arrêter le règlement local de publicité dès lors qu'à la date de l'arrêté litigieux, les communes de Lille, de Lomme et d'Hellemmes avaient fusionné et formaient une entité communale unique, la cour n'a pas entaché son arrêt d'erreur de droit ; <br/>
<br/>
              6. Considérant, enfin, que la cour n'a pas davantage entaché son arrêt d'erreur de droit en jugeant que les règles d'espacement entre les dispositifs publicitaires dans certains secteurs délimités fixées par les articles 1-4-2 et 1-4-5 du règlement local de publicité n'avaient ni pour objet, ni pour effet, d'instaurer un régime d'autorisation préalable ni d'introduire de discrimination entre sociétés d'affichage ; <br/>
<br/>
              7. Considérant qu'il résulte de tout ce qui précède que l'Union de la publicité extérieure n'est pas fondée à demander l'annulation de l'arrêt attaqué ;<br/>
<br/>
              8. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mis à la charge de la commune de Lille et de la métropole européenne de Lille, qui ne sont pas, dans la présente instance, les parties perdantes, le versement à l'Union de la publicité extérieure de la somme que demande celle-ci au titre des frais exposés par elle et non compris dans les dépens ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Union de la publicité extérieure  la somme de 3 000 euros au titre des frais exposés par la commune de Lille et la métropole européenne de Lille et non compris dans les dépens ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le pourvoi de l'Union de la publicité extérieure est rejeté.<br/>
<br/>
Article 2 : L'Union de la publicité extérieure versera à la commune de Lille et à la métropole européenne de Lille une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée à l'Union de la publicité extérieure, à la commune de Lille et à la métropole européenne de Lille.<br/>
<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-03 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - FORME ET PROCÉDURE. - PROCÉDURE ADMINISTRATIVE PRÉALABLE - GROUPE DE TRAVAIL CHARGÉ DE PRÉPARER LE PROJET DE RÉGLEMENTATION DE PUBLICITÉ - OBLIGATION DE COMPORTER UN REPRÉSENTANT DE L'ASSEMBLÉE DÉLIBÉRANTE DE L'ORGANISME INTERCOMMUNAL COMPÉTENT - OBLIGATION PRÉSENTANT LE CARACTÈRE D'UNE GARANTIE AU SENS DE LA JURISPRUDENCE DANTHONY [RJ1] - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">02-01 AFFICHAGE ET PUBLICITÉ. AFFICHAGE. - ELABORATION DE LA RÉGLEMENTATION DE PUBLICITÉ - GROUPE DE TRAVAIL CHARGÉ DE PRÉPARER LE PROJET DE RÉGLEMENTATION - OBLIGATION DE COMPORTER UN REPRÉSENTANT DE L'ASSEMBLÉE DÉLIBÉRANTE DE L'ORGANISME INTERCOMMUNAL COMPÉTENT - OBLIGATION PRÉSENTANT LE CARACTÈRE D'UNE GARANTIE AU SENS DE LA JURISPRUDENCE DANTHONY [RJ1] - ABSENCE.
</SCT>
<ANA ID="9A"> 01-03 L'obligation que le groupe de travail chargé de préparer le projet de réglementation de publicité comporte un représentant de l'assemblée délibérante de l'organisme intercommunal compétent en matière d'urbanisme (article L. 581-14 du code de l'environnement) ne présente pas le caractère d'une garantie au sens de la jurisprudence Danthony.</ANA>
<ANA ID="9B"> 02-01 L'obligation que le groupe de travail chargé de préparer le projet de réglementation de publicité comporte un représentant de l'assemblée délibérante de l'organisme intercommunal compétent en matière d'urbanisme (article L. 581-14 du code de l'environnement) ne présente pas le caractère d'une garantie au sens de la jurisprudence Danthony.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Cf. CE, Assemblée, 23 décembre 2011, Danthony et autres, n° 335033, p. 649.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
