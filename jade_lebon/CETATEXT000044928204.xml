<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044928204</ID>
<ANCIEN_ID>JG_L_2021_12_000000435622</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/92/82/CETATEXT000044928204.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 24/12/2021, 435622</TITRE>
<DATE_DEC>2021-12-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>435622</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Exécution</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI</AVOCATS>
<RAPPORTEUR>M. Réda Wadjinny-Green</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:435622.20211224</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
       Par une décision enregistrée le 10 avril 2019 au secrétariat de la section du rapport et des études du Conseil d'Etat, la présidente du tribunal administratif de Melun a transmis au Conseil d'Etat, en application des articles L. 911-4, R. 921-2 et R. 921-3 du code de justice administrative, la demande d'exécution, enregistrée le 15 février 2019 au greffe de ce tribunal, présentée par la Section française de l'Observatoire international des prisons.<br/>
<br/>
       Par cette requête et deux nouveaux mémoires enregistrés les 27 mai et 26 août 2019 au secrétariat de la section du rapport et des études du Conseil d'Etat, la Section française de l'Observatoire international des prisons demande au Conseil d'Etat d'enjoindre au garde des sceaux, ministre de la justice de prendre les mesures qu'implique l'exécution des ordonnances n° 1608163 du 6 octobre 2016 et n° 1703085 du 28 avril 2017, par lesquelles le juge des référés du tribunal administratif de Melun a enjoint à l'administration pénitentiaire de prendre certaines mesures de nature à améliorer les conditions de détention des personnes détenues au centre pénitentiaire de Fresnes. <br/>
<br/>
<br/>
       Vu les autres pièces du dossier ;<br/>
       Vu la loi n° 2019-222 du 23 mars 2019 ;<br/>
       Vu le décret n° 2019-1502 du 30 décembre 2019 ;<br/>
       Vu la décision n° 410677 du 28 juillet 2017 par laquelle le juge des référés du Conseil d'Etat a rejeté l'appel formé par la Section française de l'Observatoire international des prisons contre l'ordonnance du 28 avril 2017 du juge des référés du tribunal administratif de Melun ;<br/>
       Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
       Après avoir entendu en séance publique :<br/>
<br/>
       - le rapport de M. Réda Wadjinny-Green, auditeur,  <br/>
<br/>
       - les conclusions de M. Laurent Domingo, rapporteur public ;<br/>
<br/>
       La parole ayant été donnée, après les conclusions, à la SCP Spinosi, avocat de la Section française de l'Observatoire international des prisons ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
              1. Par deux ordonnances du 6 octobre 2016 et du 28 avril 2017, le juge des référés du tribunal administratif de Melun a enjoint à l'administration pénitentiaire de prendre, à bref délai, diverses mesures de nature à améliorer les conditions de détention des personnes détenues au centre pénitentiaire de Fresnes. Par une décision du 28 juillet 2017, enregistrée sous le n° 410677, le Conseil d'Etat, statuant au contentieux, a rejeté l'appel formé par la section française de l'Observatoire international des prisons (SFOIP) contre la seconde de ces ordonnances. Deux ans plus tard, la SFOIP a saisi le tribunal administratif de Melun d'une demande tendant à l'exécution de ces ordonnances sur le fondement de l'article L. 911-4 du code de justice administrative. Le 10 avril 2019, la présidente du tribunal administratif de Melun a transmis cette demande au Conseil d'Etat. <br/>
              2. Aux termes de l'article L. 911-4 du code de justice administrative, dans sa version applicable au litige : " En cas d'inexécution d'un jugement ou d'un arrêt, la partie intéressée peut demander à la juridiction, une fois la décision rendue, d'en assurer l'exécution. / Si le jugement ou l'arrêt dont l'exécution est demandée n'a pas défini les mesures d'exécution, la juridiction saisie procède à cette définition. Elle peut fixer un délai d'exécution et prononcer une astreinte ". L'article R. 921-2 du même code, dans sa version issue du décret du 30 décembre 2019 portant application du titre III de la loi n° 2019-222 du 23 mars 2019 de programmation 2018-2022 et de réforme pour la justice et autres mesures relatives à la procédure contentieuse administrative, dispose que : " La demande d'exécution d'un jugement frappé d'appel, même partiellement, est adressée à la juridiction d'appel (...) ". L'article R. 921-3 du même code, dans sa version issue du décret du 30 décembre 2019, dispose que : " Le président du tribunal administratif ou le président de la cour administrative d'appel peut, dans l'intérêt d'une bonne administration de la justice, renvoyer au Conseil d'Etat une demande d'exécution, sauf si une procédure juridictionnelle a été ouverte en application de l'article R. 921-6 ". L'exécution d'une ordonnance prise par le juge des référés, sur le fondement de l'article L. 521-2 du code de justice administrative, peut être recherchée dans les conditions définies par le livre IX du même code, et en particulier par les articles L. 911-4 et L. 911-5.<br/>
              Sur l'office du juge de l'exécution : <br/>
              3. Il incombe aux différentes autorités administratives de prendre, dans les domaines de leurs compétences respectives, les mesures qu'implique le respect des décisions juridictionnelles. En revanche, une demande d'exécution d'une décision juridictionnelle ne peut porter que sur l'éventuelle abstention de l'administration, observée à la date à laquelle statue le juge de l'exécution, de prendre les mesures prescrites à la suite de l'injonction, et non sur l'éventuelle réitération des difficultés, qui relèvent d'un litige distinct. Il en va en particulier ainsi lorsqu'est demandée, en application des dispositions de l'article L. 911-4 du code de justice administrative, l'exécution de mesures ordonnées par le juge des référés sur le fondement de l'article L. 521-2 du même code, mesures qui ne peuvent avoir légalement pour objet que d'assurer la sauvegarde d'une liberté fondamentale dans une situation d'urgence impliquant en principe qu'une mesure soit ordonnée dans les quarante-huit heures. Il appartient à cet égard à la partie intéressée de saisir le juge de l'exécution dans un délai approprié à l'office du juge des référés. <br/>
              Sur l'exécution de l'ordonnance du 6 octobre 2016 :<br/>
              4. Par une ordonnance du 6 octobre 2016, le juge des référés du tribunal administratif de Melun, statuant à la demande de la SFOIP sur le fondement de l'article L. 521-2 du code de justice administrative, a enjoint à l'administration pénitentiaire de poursuivre, dans les meilleurs délais, toutes les mesures nécessaires pour bétonner les zones sableuses de l'établissement, reboucher les égouts par lesquels les rats peuvent s'infiltrer et d'intensifier les actions de dératisation afin de mettre un terme au risque sanitaire encouru par les personnes détenues du fait de la prolifération des animaux nuisibles dans les locaux du centre pénitentiaire de Fresnes. <br/>
              5. D'une part, il résulte de l'instruction que le garde des sceaux, ministre de la justice a conclu un nouveau contrat avec une entreprise de destruction de nuisibles qui procède trois fois par semaine à des opérations de dératisation, là où l'ordonnance de 2016 relevait que les interventions de dératisation n'avaient lieu que toutes les deux semaines. D'autre part, à la suite de l'ordonnance du juge des référés, une première campagne de rebouchage des trous a été initiée par l'administration. Si la SFOIP soutient qu'une partie des trous n'avait pas encore été bouchée en 2019, cette circonstance ne saurait être utilement invoquée dans le cadre de la présente instance, compte tenu du périmètre de l'injonction prononcée par le juge des référés. Par suite, la demande d'exécution présentée par la SFOIP doit être rejetée.<br/>
              Sur l'exécution de l'ordonnance du 28 avril 2017 :<br/>
              6. Par une ordonnance du 28 avril 2017, le juge des référés du tribunal administratif de Melun, statuant à la demande de la SFOIP sur le fondement de l'article L. 521-2 du code de justice administrative, a enjoint au garde des sceaux, ministre de la justice de procéder à la mise en œuvre d'actions relatives à la destruction des nuisibles, aux conditions de distribution de la nourriture aux détenus, aux fouilles intégrales, à la distribution d'eau et de chauffage, aux conditions de détention en cellule, au climat de violence régnant dans l'établissement, à l'accès au travail des détenus, à l'entretien et à la propreté des locaux ainsi qu'à l'hygiène corporelle des détenus.<br/>
              En ce qui concerne les actions visant à la destruction des nuisibles :<br/>
              7. Il résulte de l'instruction, notamment du marché conclu en mars 2017 avec la société Samsic Inc, que le centre pénitentiaire de Fresnes a mis en œuvre les actions visant à la destruction des nuisibles qui lui étaient prescrites par l'ordonnance du 28 avril 2017 du juge des référés du tribunal administratif de Melun.<br/>
              En ce qui concerne la distribution de nourriture :<br/>
              8. Il résulte de l'instruction que le garde des sceaux, ministre de la justice justifie avoir pris des mesures destinées à permettre aux détenus d'avoir des repas chauds, notamment par un changement des charriots de distribution des repas en 2017. Par ailleurs, à la date de la présente décision, l'administration justifie avoir organisé une formation des auxiliaires chargés de la distribution des repas au cours des mois de septembre-octobre 2019, tandis que des contrôles réguliers de température des repas servis aux détenus ont été effectués entre le 1er juin et le 23 août 2020, sans révéler de difficultés particulières.<br/>
              En ce qui concerne les conditions de fouille intégrale :<br/>
              9. Il résulte de l'instruction qu'une note de service relative au régime juridique encadrant les modalités de contrôle des personnes détenues a été diffusée le 12 octobre 2018 à l'ensemble du personnel du centre pénitentiaire de Fresnes. Si l'administration n'a pas exécuté cette injonction dans les brefs délais qu'impliquait l'intervention du juge des référés, cette circonstance ne saurait être utilement invoquée dans la présente instance, eu égard à la date tardive à laquelle le juge de l'exécution a été saisi et compte tenu de l'office du juge de l'exécution rappelé au point 3.<br/>
              En ce qui concerne les problèmes de distribution d'eau et de chauffage :<br/>
              10. Il résulte de l'instruction qu'un marché public a été conclu le 6 novembre 2018 avec la société GEPSA pour l'exploitation et l'entretien des installations thermiques dont la première tranche a été mise à exécution en 2018. Si le rapport de la troisième visite de la maison d'arrêt des hommes du centre pénitentiaire de Fresnes publié en avril 2021 par le contrôleur général des lieux de privation de liberté relève que les cellules ne sont pas alimentées en eau chaude, et ne pourront l'être que dans le cadre d'un schéma directeur de restructuration du centre pénitentiaire qui sera lancé à partir de 2022, il résulte de l'instruction que, depuis l'intervention du juge des référés, l'administration a cependant amélioré la distribution d'eau chaude, notamment dans les douches. Contrairement à la situation observée en 2017, il n'est plus relevé, à la date de la présente décision, que tout ou partie des bâtiments serait régulièrement privée d'eau chaude durant plusieurs jours.<br/>
              En ce qui concerne les conditions de détention en cellule :<br/>
              11. Il résulte de l'instruction que l'administration pénitentiaire a mis en œuvre, à la date de la présente décision, des actions visant à l'amélioration des conditions de détention en cellule, notamment l'amélioration des éclairages, conformément à ce qui lui a été prescrit par le juge des référés du tribunal administratif de Melun, qui a relevé dans son ordonnance qu'elle ne disposait cependant d'aucun pouvoir de décision en matière de mises sous écrou.<br/>
              En ce qui concerne le climat de violence régnant dans l'établissement :<br/>
              12. Il résulte de l'instruction que l'administration pénitentiaire a mis en œuvre des actions de formation sur la gestion de la violence et de l'agressivité au profit du personnel de surveillance. En outre, il résulte de l'instruction que l'administration a diffusé le 12 août 2020, une note de service rappelant les règles applicables en matière d'usage de la contrainte par les surveillants.<br/>
              En ce qui concerne l'accès au travail :<br/>
              13. Il résulte de l'instruction que le centre pénitentiaire de Fresnes a accru de plus de 10% par rapport à la situation existant en 2016 l'offre de de travail en détention, conformément à l'injonction prononcée par le juge des référés du tribunal administratif de Melun. <br/>
              En ce qui concerne l'entretien et la propreté de l'établissement :<br/>
              14. Il résulte de l'instruction que l'administration pénitentiaire a mis en œuvre des mesures concernant la propreté de l'établissement, à travers notamment le passage au moins une fois par jour d'auxiliaires chargés de l'entretien.  <br/>
              En ce qui concerne la distribution de kits d'hygiène aux détenus :<br/>
              15. Il résulte de l'instruction que l'administration pénitentiaire n'a pas augmenté la fréquence de renouvellement des kits et des trousses d'hygiène distribués aux détenus, contrairement à l'injonction prononcée par le juge des référés du tribunal administratif de Melun. Si l'administration soutient renouveler ces kits et trousses d'hygiène sur simple demande des détenus, il ressort des écritures du ministre que seules les personnes détenues ne disposant pas de ressources suffisantes peuvent bénéficier de cette mesure. Dans ces conditions, l'administration ne saurait être regardée comme ayant mis en œuvre les mesures ordonnées par le juge des référés du tribunal administratif de Melun en tant que ces mesures portaient sur le renouvellement des kits et des trousses d'hygiène à un rythme plus soutenu. Par suite, eu égard au délai écoulé depuis l'intervention de l'ordonnance dont l'exécution est demandée, il y a lieu, sur le fondement de l'article L. 911-5 du code de justice administrative, dans les circonstances de l'espèce, de prononcer contre l'Etat, à défaut pour lui de justifier de cette exécution complète dans un délai d'un mois à compter de la notification de la présente décision, une astreinte de 1 000 euros par jour de retard jusqu'à la date à laquelle l'ordonnance du 28 avril 2017 aura reçu une complète exécution. <br/>
              16. Il résulte de tout ce qui précède que, s'il y a lieu de prononcer contre l'Etat l'astreinte énoncée au point 15, il n'y a pas lieu en revanche de faire droit aux conclusions de la SFOIP en tant qu'elles portent sur l'exécution des autres mesures d'injonctions prononcées par le juge des référés du tribunal administratif de Melun.<br/>
<br/>
<br/>
<br/>
<br/>
       D E C I D E :<br/>
       --------------<br/>
<br/>
Article 1 : Une astreinte de 1 000 euros par jour est prononcée à l'encontre de l'Etat s'il n'est pas justifié de l'exécution des injonctions de l'ordonnance du 28 avril 2017 du juge des référés du tribunal administratif de Melun mentionnées au point 15 dans un délai d'un mois à compter de la notification de la présente décision. <br/>
Article 2 : Le garde des sceaux, ministre de la justice, communiquera à la section du rapport et des études copie des actes justifiant des mesures prises pour exécuter l'ordonnance du 28 avril 2017. <br/>
Article 3 : Le surplus des conclusions de la demande est rejeté.<br/>
Article 4 : La présente décision sera notifiée au garde des sceaux, ministre de la justice et à la Section française de l'Observatoire international des prisons.<br/>
<br/>
<br/>
     Délibéré à l'issue de la séance du 3 décembre 2021 où siégeaient : M. Rémy Schwartz, président adjoint de la section du contentieux présidant ; M. F... E..., M. Frédéric Aladjidi, présidents de chambre ; Mme H... B..., Mme A... I..., M. C... D..., M. Alain Seban, conseillers d'Etat ; Mme Dominique Agneau-Canel, maître des requêtes en service extraordinaire et M. Réda Wadjinny-Green, auditeur-rapporteur. <br/>
<br/>
              Rendu le 24 décembre 2021<br/>
<br/>
<br/>
                 Le Président : <br/>
                 Signé : M. Rémy Schwartz <br/>
 		Le rapporteur : <br/>
      Signé : M. Réda Wadjinny-Green<br/>
                 Le secrétaire :<br/>
                 Signé : Mme G... J...<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-035-03 PROCÉDURE. - PROCÉDURES INSTITUÉES PAR LA LOI DU 30 JUIN 2000. - RÉFÉRÉ TENDANT AU PRONONCÉ DE MESURES NÉCESSAIRES À LA SAUVEGARDE D'UNE LIBERTÉ FONDAMENTALE (ART. L. 521-2 DU CODE DE JUSTICE ADMINISTRATIVE). - OFFICE DU JUGE DE L'EXÉCUTION SAISI DE DEMANDES RELATIVES À DES INJONCTIONS PRONONCÉES EN RÉFÉRÉ-LIBERTÉ - 1) A) DEMANDES RECEVABLES - DEMANDE D'EXÉCUTION DES MESURES PRESCRITES - EXISTENCE [RJ1] - DEMANDE RELATIVE À LA RÉITÉRATION DES DIFFICULTÉS AUXQUELLES CES MESURES AVAIENT POUR OBJET DE REMÉDIER - ABSENCE - B) EXIGENCE D'UNE SAISINE RAPIDE PAR LA PARTIE INTÉRESSÉE - EXISTENCE - 2) ILLUSTRATION - INJONCTION EXÉCUTÉE AVEC RETARD - CIRCONSTANCE SANS INCIDENCE EU ÉGARD À L'OFFICE DU JUGE DE L'EXÉCUTION ET, EN L'ESPÈCE, AU RETARD MIS À LE SAISIR.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-06-07-008 PROCÉDURE. - JUGEMENTS. - EXÉCUTION DES JUGEMENTS. - PRESCRIPTION D'UNE MESURE D'EXÉCUTION. - EXÉCUTION D'INJONCTIONS PRONONCÉES EN RÉFÉRÉ-LIBERTÉ - 1) A) DEMANDES RECEVABLES - DEMANDE D'EXÉCUTION DES MESURES PRESCRITES - EXISTENCE [RJ1] - DEMANDE RELATIVE À LA RÉITÉRATION DES DIFFICULTÉS AUXQUELLES CES MESURES AVAIENT POUR OBJET DE REMÉDIER - ABSENCE - B) EXIGENCE D'UNE SAISINE RAPIDE PAR LA PARTIE INTÉRESSÉE - EXISTENCE - 2) ILLUSTRATION - INJONCTION EXÉCUTÉE AVEC RETARD - CIRCONSTANCE SANS INCIDENCE EU ÉGARD À L'OFFICE DU JUGE DE L'EXÉCUTION ET, EN L'ESPÈCE, AU RETARD MIS À LE SAISIR.
</SCT>
<ANA ID="9A"> 54-035-03 1) a) Il incombe aux différentes autorités administratives de prendre, dans les domaines de leurs compétences respectives, les mesures qu'implique le respect des décisions juridictionnelles. ......En revanche, une demande d'exécution d'une décision juridictionnelle ne peut porter que sur l'éventuelle abstention de l'administration, observée à la date à laquelle statue le juge de l'exécution, de prendre les mesures prescrites à la suite de l'injonction, et non sur l'éventuelle réitération des difficultés, qui relèvent d'un litige distinct. ......Il en va en particulier ainsi lorsqu'est demandée, en application de l'article L. 911-4 du code de justice administrative (CJA), l'exécution de mesures ordonnées par le juge des référés sur le fondement de l'article L. 521-2 du même code, mesures qui ne peuvent avoir légalement pour objet que d'assurer la sauvegarde d'une liberté fondamentale dans une situation d'urgence impliquant en principe qu'une mesure soit ordonnée dans les quarante-huit heures. ......b) Il appartient à cet égard à la partie intéressée de saisir le juge de l'exécution dans un délai approprié à l'office du juge des référés.......2) Juge des référés ayant, par une ordonnance du 28 avril 2017 prise sur le fondement de l'article L. 521-2 du CJA, enjoint au garde des sceaux, ministre de la justice, de procéder notamment à la mise en œuvre d'actions relatives aux fouilles intégrales dans un centre pénitentiaire.......Il résulte de l'instruction qu'une note de service relative au régime juridique encadrant les modalités de contrôle des personnes détenues a été diffusée le 12 octobre 2018 à l'ensemble du personnel du centre pénitentiaire. Si l'administration n'a pas exécuté cette injonction dans les brefs délais qu'impliquait l'intervention du juge des référés, cette circonstance ne saurait être utilement invoquée au soutien d'une demande d'exécution de l'ordonnance du 28 avril 2017, eu égard à la date tardive à laquelle le juge de l'exécution a été saisi, le 10 avril 2019, et compte tenu de l'office du juge de l'exécution rappelé au point 1).......Rejet des demandes portant sur l'exécution de cette mesure.</ANA>
<ANA ID="9B"> 54-06-07-008 1) a) Il incombe aux différentes autorités administratives de prendre, dans les domaines de leurs compétences respectives, les mesures qu'implique le respect des décisions juridictionnelles. ......En revanche, une demande d'exécution d'une décision juridictionnelle ne peut porter que sur l'éventuelle abstention de l'administration, observée à la date à laquelle statue le juge de l'exécution, de prendre les mesures prescrites à la suite de l'injonction, et non sur l'éventuelle réitération des difficultés, qui relèvent d'un litige distinct. ......Il en va en particulier ainsi lorsqu'est demandée, en application de l'article L. 911-4 du code de justice administrative (CJA), l'exécution de mesures ordonnées par le juge des référés sur le fondement de l'article L. 521-2 du même code, mesures qui ne peuvent avoir légalement pour objet que d'assurer la sauvegarde d'une liberté fondamentale dans une situation d'urgence impliquant en principe qu'une mesure soit ordonnée dans les quarante-huit heures. ......b) Il appartient à cet égard à la partie intéressée de saisir le juge de l'exécution dans un délai approprié à l'office du juge des référés.......2) Juge des référés ayant, par une ordonnance du 28 avril 2017 prise sur le fondement de l'article L. 521-2 du CJA, enjoint au garde des sceaux, ministre de la justice, de procéder notamment à la mise en œuvre d'actions relatives aux fouilles intégrales dans un centre pénitentiaire.......Il résulte de l'instruction qu'une note de service relative au régime juridique encadrant les modalités de contrôle des personnes détenues a été diffusée le 12 octobre 2018 à l'ensemble du personnel du centre pénitentiaire. Si l'administration n'a pas exécuté cette injonction dans les brefs délais qu'impliquait l'intervention du juge des référés, cette circonstance ne saurait être utilement invoquée au soutien d'une demande d'exécution de l'ordonnance du 28 avril 2017, eu égard à la date tardive à laquelle le juge de l'exécution a été saisi, le 10 avril 2019, et compte tenu de l'office du juge de l'exécution rappelé au point 1).......Rejet des demandes portant sur l'exécution de cette mesure.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 19 octobre 2020, Garde des sceaux, ministre de la justice c/ Section française de l'Observatoire international des prisons, n°s 439372 439444, p. 351.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
