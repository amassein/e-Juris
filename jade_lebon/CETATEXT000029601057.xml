<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029601057</ID>
<ANCIEN_ID>JG_L_2014_09_000000351689</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/60/10/CETATEXT000029601057.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème / 9ème SSR, 24/09/2014, 351689</TITRE>
<DATE_DEC>2014-09-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>351689</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème / 9ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP JEAN-PHILIPPE CASTON</AVOCATS>
<RAPPORTEUR>Mme Isabelle Lemesle</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Delphine Hedary</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:351689.20140924</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
Procédure contentieuse antérieure<br/>
	M. E...C...a demandé au tribunal administratif de Nice d'annuler l'arrêté du 14 mars 2005 par lequel le maire de la commune de Saint-Jean-Cap-Ferrat a délivré un permis de construire à Mme B...D.la cour a entaché son arrêt d'une erreur de droit Par un jugement n° 0503350 du 9 avril 2009, le tribunal administratif de Nice a rejeté sa demande.<br/>
	Par un arrêt n° 09MA02288 du 1er juin 2011 la cour administrative d'appel de Marseille a rejeté l'appel formé contre ce jugement par M.C.la cour a entaché son arrêt d'une erreur de droit<br/>
Procédure devant le Conseil d'Etat<br/>
	Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 8 août et 8 novembre 2011 au secrétariat du contentieux du Conseil d'Etat, M. C...demande au Conseil d'Etat :<br/>
	1°) d'annuler cet arrêt n° 09MA02288 du 1er juin 2011 de la cour administrative d'appel de Marseille ;<br/>
	2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
	3°) de mettre à la charge de Mme D...et de la commune de Saint-Jean-Cap-Ferrat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu :<br/>
              - les autres pièces du dossier ;<br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Isabelle Lemesle, maître des requêtes,<br/>
<br/>
              - les conclusions de Mme Delphine Hedary, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Jean-Philippe Caston, avocat de M. C...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que l'article R. 421-29 du code de l'urbanisme, applicable à la date à laquelle le permis de construire contesté a été délivré, prévoit que l'autorité compétente, pour statuer sur la demande de permis de construire, se prononce par arrêté ; que l'article A. 421-6-1 de ce code précisait que cet arrêté devait comporter notamment le nom et l'adresse du demandeur ; qu'aux termes de l'article R. 421 39 du même code, alors en vigueur : " (...) dans les huit jours de la délivrance expresse (...) du permis de construire, un extrait du permis (...) est publié par voie d'affichage à la mairie pendant deux mois (...) " ; qu'aux termes de l'article R. 600-1 : " En cas (...) de recours contentieux à l'encontre (...) d'un permis de construire (...) l'auteur du recours est tenu, à peine d'irrecevabilité, de notifier son recours à l'auteur de la décision et au titulaire de l'autorisation. Cette notification doit également être effectuée dans les mêmes conditions en cas de demande tendant à l'annulation (...) d'une décision juridictionnelle concernant (...) un permis de construire (...) " ;<br/>
<br/>
              2. Considérant qu'il résulte des termes mêmes de l'article R. 600-1 du code de l'urbanisme, dont le but est d'alerter tant l'auteur d'une décision d'urbanisme que son bénéficiaire de l'existence d'un recours contentieux formé contre cette décision, dès son introduction, que cette formalité peut être regardée comme régulièrement accomplie dès lors que la notification est faite au titulaire de l'autorisation désigné par l'acte attaqué, à l'adresse qui y est mentionnée ;<br/>
<br/>
              3. Considérant que, pour rejeter l'appel formé par M. C...contre le jugement du 9 avril 2009 par lequel le tribunal administratif de Nice avait rejeté sa demande d'annulation de l'arrêté du 14 mars 2005 du maire de Saint-Jean-Cap-Ferrat délivrant un permis de construire à MmeD..., la cour administrative d'appel de Marseille a accueilli la fin de non-recevoir, soulevée par la défenderesse, tirée de ce que le requérant n'avait pas rempli son obligation de lui notifier sa requête d'appel, posée par l'article R. 600-1 du code de l'urbanisme, en expédiant cette dernière non pas à l'adresse personnelle de MmeD..., mais à l'adresse de l'architecte auquel celle-ci avait donné mandat jusqu'à la notification de la décision définitive de l'administration ; <br/>
<br/>
              4. Considérant qu'en statuant ainsi, alors qu'il ressortait des pièces du dossier qui lui était soumis que cette adresse était mentionnée sur le permis litigieux comme étant celle à laquelle la bénéficiaire du permis de construire était domiciliée, la cour a entaché son arrêt d'une erreur de droit; que, dès lors, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, M. C...est fondé à demander l'annulation de l'arrêt attaqué ; <br/>
<br/>
              5. Considérant qu'il y lieu, dans les circonstances de l'espèce, de mettre à la charge de Mme D...et de la commune de Saint-Jean-Cap-Ferrat la somme de 1 500 euros chacun, à verser à M. C...au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Marseille du 1er juin 2011 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Marseille.<br/>
Article 3 : Mme D...et la commune de Saint-Jean-Cap-Ferrat verseront chacun la somme de 1 500 euros à M.C..., au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à M. E...C...et à Mme B...D.la cour a entaché son arrêt d'une erreur de droit<br/>
Copie en sera adressée à la ministre de l'écologie, du développement durable et de l'énergie, à M. A... F...et à la commune de Saint-Jean-Cap-Ferrat. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">68-06-01-04 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. INTRODUCTION DE L'INSTANCE. OBLIGATION DE NOTIFICATION DU RECOURS. - RÉGULARITÉ DE LA NOTIFICATION - 1) NOTIFICATION FAITE AU TITULAIRE DE L'AUTORISATION DÉSIGNÉ PAR L'ACTE ATTAQUÉ, À L'ADRESSE QUI Y EST MENTIONNÉE - EXISTENCE [RJ1] - 2) CONSÉQUENCE - NOTIFICATION DE L'APPEL FORMÉ CONTRE UN JUGEMENT REJETANT UN RECOURS CONTRE UNE DÉCISION D'URBANISME - NOTIFICATION FAITE, NON PAS À L'ADRESSE PERSONNELLE DU BÉNÉFICIAIRE DE L'AUTORISATION, MAIS À L'ADRESSE DE L'ARCHITECTE AUQUEL IL AVAIT DONNÉ MANDAT - RÉGULARITÉ EN L'ESPÈCE, DÈS LORS QUE CETTE ADRESSE EST MENTIONNÉE SUR L'AUTORISATION COMME ÉTANT CELLE À LAQUELLE LE BÉNÉFICIAIRE EST DOMICILIÉ [RJ2].
</SCT>
<ANA ID="9A"> 68-06-01-04 1) Il résulte des termes mêmes de l'article R. 600-1 du code de l'urbanisme, dont le but est d'alerter tant l'auteur d'une décision d'urbanisme que son bénéficiaire de l'existence d'un recours contentieux formé contre cette décision, dès son introduction, que cette formalité peut être regardée comme régulièrement accomplie dès lors que la notification est faite au titulaire de l'autorisation désigné par l'acte attaqué, à l'adresse qui y est mentionnée.,,,2) Par suite, régularité de la notification de l'appel formé contre un jugement rejetant un recours contre un permis de construire, faite non pas à l'adresse personnelle du bénéficiaire du permis attaqué, mais à l'adresse de l'architecte auquel le bénéficiaire avait donné mandat jusqu'à la notification de la décision définitive de l'administration, dès lors que cette adresse était mentionnée sur le permis litigieux comme étant celle à laquelle le bénéficiaire du permis de construire était domicilié.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., sur la prise en compte des mentions du permis de construire, CE, 23 avril 2003, Association Nos villages et Mme,, n° 251608, T. p. 1032.,,[RJ2] Comp., pour le cas où la notification est adressée à l'avocat de première instance, CE, 28 septembre 2011,,n° 341749, T. p. 1200.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
