<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030642960</ID>
<ANCIEN_ID>JG_L_2015_05_000000383076</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/64/29/CETATEXT000030642960.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème / 9ème SSR, 27/05/2015, 383076</TITRE>
<DATE_DEC>2015-05-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>383076</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème / 9ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Jacques Reiller</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:383076.20150527</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 25 juillet 2014 et 6 janvier 2015 au secrétariat du contentieux du Conseil d'Etat, M. B...A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'article 2 du décret n° 2014-527 du 23 mai 2014 portant modification du code de l'entrée et du séjour des étrangers et du droit d'asile (partie réglementaire) en ce qui concerne Mayotte, Wallis-et-Futuna, la Polynésie française et la Nouvelle-Calédonie ;  <br/>
<br/>
              2°) d'enjoindre au Premier ministre d'édicter de nouvelles dispositions conformes au principe d'égalité et au droit des ressortissants français à mener une vie familiale normale ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative; <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jacques Reiller, conseiller d'Etat,<br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 13 mai 2015, présentée par M.A... ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que le décret du 23 mai 2014 portant modification du code de l'entrée et du séjour des étrangers et du droit d'asile (partie réglementaire) en ce qui concerne Mayotte, Wallis-et-Futuna, la Polynésie française et la Nouvelle-Calédonie a eu notamment pour objet de rendre applicable à Mayotte, sous réserve de certaines adaptations, la partie réglementaire du code ; que M. A...demande l'annulation pour excès de pouvoir de son article 2 qui a adapté à Mayotte l'article R. 311-3 de ce code, dont l'objet est de définir les catégories d'étrangers dispensés de souscrire une demande de carte de séjour, et y a inséré un nouvel alinéa aux termes duquel : " A Mayotte, la condition tenant aux formalités accomplies auprès de l'Office français de l'immigration et de l'intégration en vertu de l'alinéa précédent n'est pas exigible " ; que le requérant soutient que l'article attaqué est illégal en tant qu'il n'a pas modifié l'article R. 311-3 pour aligner la situation des ressortissants étrangers liés par un pacte civil de solidarité avec un Français sur celle des ressortissants étrangers conjoints de Français dispensés de souscrire une demande de carte de séjour ; <br/>
<br/>
              2. Considérant que, d'une part, M. A...ne se prévaut d'aucun intérêt personnel lui donnant qualité pour demander l'annulation des dispositions de l'article 2 du décret attaqué définissant les conditions dans lesquelles l'article R. 311-3 s'applique à Mayotte ; que, d'autre part, eu égard à l'objet de ces dispositions, il n'est recevable à contester, à l'occasion du recours pour excès de pouvoir qu'il a introduit à leur encontre, ni par la voie de l'action ni par la voie de l'exception, la légalité des autres dispositions de l'article R. 311-3 applicables sur les autres parties du territoire de la République française ; <br/>
<br/>
              3. Considérant qu'il résulte de ce qui précède que la requête de M. A...doit être rejetée, y compris ses conclusions à fin d'injonction ainsi que celles présentées au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. A...est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. B...A...et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-01-04-01 PROCÉDURE. INTRODUCTION DE L'INSTANCE. INTÉRÊT POUR AGIR. ABSENCE D'INTÉRÊT. - DISPOSITIONS RÉGLEMENTAIRES ADAPTANT À MAYOTTE ET MODIFIANT, EN Y INTRODUISANT UNE DISPOSITION APPLICABLE À MAYOTTE, UN ARTICLE DU CESEDA - IRRECEVABILITÉ DE LA CONTESTATION DE CES DISPOSITIONS A) EN TANT QU'ELLES DÉFINISSENT LE RÉGIME APPLICABLE À MAYOTTE, FAUTE D'INTÉRÊT PERSONNEL DU REQUÉRANT ET B) EN TANT QU'ELLES VISENT LES AUTRES DISPOSITIONS DE CET ARTICLE DU CESEDA APPLICABLES SUR LES AUTRES PARTIES DU TERRITOIRE DE LA RÉPUBLIQUE FRANÇAISE, COMPTE TENU DE LEUR OBJET [RJ1].
</SCT>
<ANA ID="9A"> 54-01-04-01 Disposition réglementaire adaptant à Mayotte l'article R. 311-3 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA), dont l'objet est de définir les catégories d'étrangers dispensés de souscrire une demande de carte de séjour, et insérant dans cet article un nouvel alinéa dispensant les demandeurs situés à Mayotte de l'obligation de produire une attestation délivrée par l'Office de l'immigration et de l'intégration. Requérant demandant l'annulation pour excès de pouvoir de cette disposition en tant qu'elle n'a pas modifié l'article R. 311-3 pour aligner la situation des ressortissants étrangers liés par un pacte civil de solidarité avec un Français sur celle des ressortissants étrangers conjoints de Français dispensés de souscrire une demande de carte de séjour.,,,La requête est rejetée dès lors a), d'une part, que le requérant ne se prévaut d'aucun intérêt personnel lui donnant qualité pour demander l'annulation des dispositions définissant les conditions dans lesquelles l'article R. 311-3 s'applique à Mayotte et b), d'autre part, que, eu égard à l'objet de ces dispositions, il n'est recevable à contester, à l'occasion du recours pour excès de pouvoir qu'il a introduit à leur encontre, ni par la voie de l'action ni par la voie de l'exception, la légalité des autres dispositions de l'article R. 311-3 applicables sur les autres parties du territoire de la République française.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Comp. CE, Section, 24 janvier 1992,  Association des centres distributeurs Edouard Leclerc, n° 68122, Lebon p. 39.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
