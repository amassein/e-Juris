<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029812950</ID>
<ANCIEN_ID>JG_L_2014_11_000000363917</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/81/29/CETATEXT000029812950.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère / 6ème SSR, 28/11/2014, 363917</TITRE>
<DATE_DEC>2014-11-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>363917</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère / 6ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP TIFFREAU, MARLANGE, DE LA BURGADE ; SCP ROUSSEAU, TAPIE</AVOCATS>
<RAPPORTEUR>M. Jean-Denis Combrexelle</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:363917.20141128</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
Procédure contentieuse antérieure<br/>
<br/>
              Mme B...A...a demandé au tribunal administratif de Cergy-Pontoise d'annuler pour excès de pouvoir la décision du 4 décembre 2009 par laquelle la directrice générale de l'office public de l'habitat de Gennevilliers l'a placée en congé de maladie ordinaire sans traitement à compter du 1er juillet 2009. Par un jugement n° 1000850 du 20 septembre 2012, le tribunal administratif de Cergy-Pontoise a rejeté sa demande.<br/>
<br/>
Procédure devant le Conseil d'Etat<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 15 novembre 2012, 14 février 2013 et 7 novembre 2013 au secrétariat du contentieux du Conseil d'Etat, Mme A...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement n° 1000850 du tribunal administratif de Cergy-Pontoise du 20 septembre 2012 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'office public de l'habitat de Gennevilliers la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu :<br/>
              - les autres pièces du dossier ;<br/>
              - le code des pensions civiles et militaires de retraite ;<br/>
              - la loi n° 84-53 du 26 janvier 1984 ;<br/>
              - le décret n° 86-68 du 13 janvier 1986 ;<br/>
              - le décret n° 87-602 du 30 juillet 2007 ; <br/>
              - le code de justice administrative.<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Yannick Faure, auditeur,  <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rousseau, Tapie, avocat de Mme A...et à la SCP Tiffreau, Marlange, de la Burgade, avocat de l'office public de l'habitat de Gennevilliers.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que MmeA..., agent administratif de l'office public de l'habitat de Gennevilliers a, à la suite d'un syndrome anxio-dépressif qu'elle impute à sa hiérarchie, été placée en congé de maladie ordinaire à compter du 1er juillet 2008 ; qu'ayant contesté l'avis du comité médical indiquant qu'elle devait être placée en disponibilité à la fin de son congé et demandé à cette fin la saisine du comité médical supérieur, elle a été placée par son employeur, à compter du 1er juillet 2009 et dans l'attente de l'avis de ce comité, en congé de maladie sans traitement ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article 57 de la loi du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale : " Le fonctionnaire en activité a droit: / (...) 2° A des congés de maladie dont la durée totale peut atteindre un an  pendant une période de douze mois consécutifs en cas de maladie dûment constatée mettant l'intéressé dans l'impossibilité d'exercer ses fonctions. Celui-ci conserve alors l'intégralité de son traitement pendant une durée de trois mois ; ce traitement est réduit de moitié pendant les neuf mois suivants (...) / Toutefois, si la maladie provient de l'une des causes exceptionnelles prévues à l'article L. 27 du code des pensions civiles et militaires de retraite ou d'un accident survenu dans l'exercice ou à l'occasion de l'exercice de ses fonctions, le fonctionnaire conserve l'intégralité de son traitement jusqu'à ce qu'il soit en état de reprendre son service ou jusqu'à la mise à la retraite (...) " ; <br/>
<br/>
              3. Considérant que le deuxième alinéa de l'article 17 du décret du 30 juillet 1987, pris pour l'application de cette loi, dispose, dans sa rédaction applicable à la date de la décision attaquée, que : " Lorsque le fonctionnaire a obtenu pendant une période de douze mois consécutifs des congés de maladie d'une durée totale de douze mois, il ne peut, à l'expiration de sa période de congé, reprendre son service sans l'avis favorable du comité médical. En cas d'avis défavorable, il est soit mis en disponibilité, soit reclassé dans un autre emploi, soit, s'il est reconnu définitivement inapte à l'exercice de tout emploi, admis à la retraite après avis de la commission de réforme (...) " ; que le premier alinéa de l'article 5 du même décret dispose que : " Le comité médical supérieur institué auprès du ministre chargé de la santé par le décret n° 86-442 du 14 mars 1986 (...) peut être appelé, à la demande de l'autorité compétente ou du fonctionnaire concerné, à donner son avis sur les cas litigieux, qui doivent avoir été préalablement examinés en premier ressort par les comités médicaux " ; que le premier alinéa de l'article 19 du décret du 13 janvier 1986 relatif aux positions de détachement, hors cadres, de disponibilité et de congé parental des fonctionnaires territoriaux, dans sa rédaction applicable à la date de la décision attaquée, dispose que : " La mise en disponibilité peut être prononcée d'office à l'expiration des droits statutaires à congés de maladie prévus à l'article 57 (2°, 3° et 4°) de la loi du 26 janvier 1984 et s'il ne peut, dans l'immédiat, être procédé au reclassement du fonctionnaire dans les conditions prévues aux articles 81 à 86 de la loi du 26 janvier 1984 " ;<br/>
<br/>
              4. Considérant que lorsque, pour l'application de ces dispositions, le comité médical supérieur est saisi d'une contestation de l'avis du comité médical, il appartient à l'employeur de prendre une décision provisoire dans l'attente de cet avis pour placer le fonctionnaire dans l'une des positions prévues par son statut ; que si l'agent a épuisé ses droits à congé de maladie ordinaire et ne peut reprendre le service en raison de l'avis défavorable du comité médical, la circonstance que l'administration ait saisi le comité médical supérieur à la demande de l'agent ne fait pas obstacle à ce que ce dernier soit placé, par une décision à caractère provisoire et sous réserve de régularisation ultérieure, en disponibilité d'office ; qu'en revanche, l'administration ne peut légalement, hors le cas de prolongation du congé de maladie ordinaire dans les conditions prévues à l'article 57 de la loi du 26 janvier 1984, lui accorder le bénéfice d'un tel congé au-delà de la période d'un an, qu'il soit rémunéré ou non ; que, par suite, le tribunal, qui avait relevé que l'affection dont Mme A...était atteinte ne lui ouvrait droit au regard des dispositions de l'article 57 de la loi du 26 janvier 1984 qu'à un congé de maladie ordinaire d'une durée maximale d'un an arrivée à son terme, a commis une erreur de droit en jugeant que l'administration avait pu légalement la placer en congé de maladie sans traitement ; <br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que le jugement du tribunal administratif de Cergy-Pontoise doit être annulé ;<br/>
<br/>
              6. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'office public de l'habitat de Gennevilliers la somme de 3 000 euros, au titre des dispositions de l'article L. 761-1 du code de justice administrative ; qu'en revanche, ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de MmeA..., qui n'est pas, dans la présente instance, la partie perdante ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Cergy-Pontoise du 20 septembre 2012 est annulé.<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Cergy-Pontoise.<br/>
Article 3 : L'office public de l'habitat de Gennevilliers versera à Mme A...une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions de l'office public de l'habitat de Gennevilliers présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à Mme B...A...et à l'office public de l'habitat de Gennevilliers.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-05-04-01 FONCTIONNAIRES ET AGENTS PUBLICS. POSITIONS. CONGÉS. CONGÉS DE MALADIE. - FONCTIONNAIRE AYANT ÉPUISÉ SES DROITS À CONGÉ DE MALADIE ORDINAIRE - DEVOIRS DE L'ADMINISTRATION DANS L'ATTENTE DE L'ISSUE DE LA CONTESTATION DE L'AVIS DU COMITÉ MÉDICAL DEVANT LE COMITÉ MÉDICAL SUPÉRIEUR - OBLIGATION DE PLACER PAR UNE DÉCISION PROVISOIRE LE FONCTIONNAIRE DANS UNE POSITION STATUAIRE - MODALITÉS - POSSIBILITÉ DE LE PLACER EN CONGÉ DE MALADIE, RÉMUNÉRÉ OU NON - ABSENCE - POSSIBILITÉ DE LE PLACER EN DISPONIBILITÉ D'OFFICE - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">36-07-01-03 FONCTIONNAIRES ET AGENTS PUBLICS. STATUTS, DROITS, OBLIGATIONS ET GARANTIES. STATUT GÉNÉRAL DES FONCTIONNAIRES DE L'ÉTAT ET DES COLLECTIVITÉS LOCALES. DISPOSITIONS STATUTAIRES RELATIVES À LA FONCTION PUBLIQUE TERRITORIALE (LOI DU 26 JANVIER 1984). - DEVOIRS DE L'ADMINISTRATION DANS L'ATTENTE DE L'ISSUE DE LA CONTESTATION DE L'AVIS DU COMITÉ MÉDICAL DEVANT LE COMITÉ MÉDICAL SUPÉRIEUR - OBLIGATION DE PLACER PAR UNE DÉCISION PROVISOIRE LE FONCTIONNAIRE DANS UNE POSITION STATUAIRE - CAS OÙ LE FONCTIONNAIRE A ÉPUISÉ SES DROITS À CONGÉ DE MALADIE ORDINAIRE - POSSIBILITÉ DE LE PLACER EN CONGÉ DE MALADIE, RÉMUNÉRÉ OU NON - ABSENCE - POSSIBILITÉ DE LE PLACER EN DISPONIBILITÉ D'OFFICE - EXISTENCE.
</SCT>
<ANA ID="9A"> 36-05-04-01 Lorsque, pour l'application des dispositions de l'article 57 de la loi n° 84-53 du 26 janvier 1984 et des articles 5 et 17 du décret n° 87-602 du 30 juillet 2007, le comité médical supérieur est saisi d'une contestation de l'avis du comité médical, il appartient à l'employeur de prendre une décision provisoire dans l'attente de cet avis pour placer le fonctionnaire dans l'une des positions prévues par son statut.... ,,Si l'agent a épuisé ses droits à congé de maladie ordinaire et ne peut reprendre le service en raison de l'avis défavorable du comité médical, la circonstance que l'administration ait saisi le comité médical supérieur à la demande de l'agent ne fait pas obstacle à ce que ce dernier soit placé, par une décision à caractère provisoire et sous réserve de régularisation ultérieure, en disponibilité d'office, prévue à l'article 19 du décret n° 86-68 du 13 janvier 1986. En revanche, l'administration ne peut légalement, hors le cas de prolongation du congé de maladie ordinaire dans les conditions prévues à l'article 57 de la loi du 26 janvier 1984, lui accorder le bénéfice d'un tel congé au-delà de la période d'un an, qu'il soit rémunéré ou non.</ANA>
<ANA ID="9B"> 36-07-01-03 Lorsque, pour l'application des dispositions de l'article 57 de la loi n° 84-53 du 26 janvier 1984 et des articles 5 et 17 du décret n° 87-602 du 30 juillet 2007, le comité médical supérieur est saisi d'une contestation de l'avis du comité médical, il appartient à l'employeur de prendre une décision provisoire dans l'attente de cet avis pour placer le fonctionnaire dans l'une des positions prévues par son statut.... ,,Si l'agent a épuisé ses droits à congé de maladie ordinaire et ne peut reprendre le service en raison de l'avis défavorable du comité médical, la circonstance que l'administration ait saisi le comité médical supérieur à la demande de l'agent ne fait pas obstacle à ce que ce dernier soit placé, par une décision à caractère provisoire et sous réserve de régularisation ultérieure, en disponibilité d'office, prévue à l'article 19 du décret n° 86-68 du 13 janvier 1986. En revanche, l'administration ne peut légalement, hors le cas de prolongation du congé de maladie ordinaire dans les conditions prévues à l'article 57 de la loi du 26 janvier 1984, lui accorder le bénéfice d'un tel congé au-delà de la période d'un an, qu'il soit rémunéré ou non.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
