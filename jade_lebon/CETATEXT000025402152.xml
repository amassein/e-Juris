<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025402152</ID>
<ANCIEN_ID>JG_L_2012_02_000000340720</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/40/21/CETATEXT000025402152.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème et 2ème sous-sections réunies, 22/02/2012, 340720</TITRE>
<DATE_DEC>2012-02-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>340720</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème et 2ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Philippe Martin</PRESIDENT>
<AVOCATS>SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>M. Francis Girault</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Bertrand Dacosta</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:340720.20120222</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 21 juin et 21 septembre 2010 au secrétariat du contentieux du Conseil d'Etat, présentés pour la COMMUNE DE BASTIA, représentée par son maire ; la COMMUNE DE BASTIA demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement n° 0900665-2 du 8 avril 2010 par lequel le tribunal administratif de Bastia a, sur déféré du préfet de Haute-Corse, annulé l'arrêté du 8 décembre 2008 par lequel le maire de la COMMUNE DE BASTIA a nommé M. A en qualité d'administrateur territorial au titre de la promotion interne, ainsi que l'arrêté du 15 juin 2009 titularisant M. A en qualité d'administrateur territorial à compter du 15 juin 2009 ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter le déféré préfectoral ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement d'une somme de 4 000 euros en application de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi n° 84-53 du 26 janvier 1984 ;<br/>
<br/>
              Vu le décret n° 85-1229 du 20 novembre 1985 ;<br/>
<br/>
              Vu le décret n° 87-1097 du 30 décembre 1987 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Francis Girault, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Lyon-Caen, Thiriez, avocat de la COMMUNE DE BASTIA, <br/>
<br/>
              - les conclusions de M. Bertrand Dacosta, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Lyon-Caen, Thiriez, avocat de la COMMUNE DE BASTIA ; <br/>
<br/>
<br/>
<br/>Considérant qu'aux termes de l'article 20-5 du décret du 20 novembre 1985 relatif aux conditions générales de recrutement des agents de la fonction publique territoriale : " Lorsque le nombre de recrutements ouvrant droit à un recrutement au titre de la promotion interne en application des dispositions d'un statut particulier n'a pas été atteint pendant une période d'au moins quatre ans, un fonctionnaire territorial remplissant les conditions pour bénéficier d'une nomination au titre de la promotion interne peut être inscrit sur la liste d'aptitude si au moins un recrutement entrant en compte pour cette inscription est intervenu. / La période minimale mentionnée à l'alinéa précédent est, pour une durée de quatre ans à compter de l'entrée en vigueur du décret n° 2006-1462 du 28 novembre 2006 relatif à la promotion interne des fonctionnaires territoriaux, abaissée à deux ans " ;<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que par arrêté du 8 décembre 2008, le maire de la COMMUNE DE BASTIA a nommé M. A en qualité d'administrateur territorial au titre de la promotion interne en faisant application des dispositions ci-dessus rappelées de l'article 20-5 du décret du 20 novembre 1985, à compter du 15 décembre 2008 ; que, par arrêté du maire en date du 15 juin 2009, M. A a été titularisé en qualité d'administrateur territorial ; que le tribunal administratif de Bastia, saisi par le préfet de la Haute-Corse, a annulé par son jugement du 8 avril 2010 ces deux décisions ; que la COMMUNE DE BASTIA se pourvoit en cassation à l'encontre de ce jugement ;<br/>
<br/>
              Considérant qu'il résulte des dispositions réglementaires précitées que la seule condition posée pour procéder à une nomination au titre de la promotion interne d'un fonctionnaire territorial, dans le cas où le nombre de recrutements exigés pour ouvrir droit à cette nomination n'a pas été atteint durant la période d'au moins deux ans qui la précède, est qu'au moins un recrutement soit intervenu, sans autre considération de date ; que, dès lors, en rejetant la prise en compte, au titre de la promotion interne de M. A en 2008, du recrutement par la COMMUNE DE BASTIA d'un agent en qualité d'administrateur territorial au motif qu'il était intervenu en 1990 et que l'agent recruté à cette date avait été admis à la retraite en 1998, le tribunal administratif de Bastia a commis une erreur de droit ; que, par suite et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, la COMMUNE DE BASTIA est fondée à demander l'annulation du jugement du 8 avril 2010 ;<br/>
<br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              Considérant que si la COMMUNE DE BASTIA soutient que le déféré préfectoral est tardif en raison de la date d'enregistrement en préfecture de l'arrêté du 8 décembre 2008, il ressort des pièces du dossier que ce moyen n'est pas fondé ;<br/>
<br/>
              Considérant, comme il vient d'être dit, que l'article 20-5 du décret du 20 novembre 1985 prévoit l'exigence d'un recrutement d'au moins un agent, intervenu au moins 2 ans auparavant, pour qu'une nomination au titre de la promotion interne puisse être prononcée ; qu'aux termes de l'article 20-6 de ce même décret, dans sa rédaction applicable à la date de la décision de nomination contestée : " Lorsque les dispositions prévues par le statut particulier d'un cadre d'emplois permettent d'accéder à celui-ci par la voie de la promotion interne, selon les modalités prévues à l'article 39 de la loi du 26 janvier 1984 susvisée, le nombre de recrutements ouvrant droit à un recrutement par cette voie, intervenus dans la collectivité ou l'établissement ou l'ensemble des collectivités et établissements affiliés à un centre de gestion, comprend les recrutements de candidats admis à un concours d'accès au cadre d'emplois et les recrutements de fonctionnaires opérés par la voie de la mutation externe à la collectivité et aux établissements en relevant et par la voie du détachement. Il ne comprend ni les renouvellements de détachement ni les intégrations prononcées dans le cadre d'emplois de détachement. " ; qu'il résulte de ces dispositions que les nominations en qualité d'administrateur territorial prononcées lors de la constitution initiale de ce cadre d'emplois au bénéfice d'agents exerçant déjà leurs fonctions au sein de la collectivité qui les nomme et intégrés dans le cadre d'emplois des administrateurs territoriaux à raison de celles-ci, ne sont pas au nombre de celles qui ouvrent droit, au sens de ces dispositions, à un recrutement au titre de la promotion interne ; <br/>
<br/>
              Considérant qu'il ressort des pièces du dossier que le maire de la COMMUNE DE BASTIA a nommé M. B dans le grade d'administrateur territorial en 1990 en application de l'article 23 du décret du 30 décembre 1987 portant statut particulier du cadre d'emplois des administrateurs territoriaux aux termes duquel :  " Sont intégrés en qualité de titulaires dans le cadre d'emplois des administrateurs territoriaux, lorsqu'ils se trouvent en position d'activité et occupent effectivement leur emploi à la date de publication du présent décret, les fonctionnaires territoriaux titulaires suivants : (...) / 3° Le secrétaire général des villes de plus de 40 000 habitants (...) " ; que cette nomination, intervenue lors de la constitution initiale du cadre d'emplois au bénéfice d'un agent exerçant déjà les fonctions au titre desquelles celle-ci est prononcée, ne peut toutefois pas être regardée comme un recrutement intervenu selon l'une des modalités énumérées à l'article 20-6 du décret du 20 novembre 1985 ; qu'elle ne peut en conséquence être comptabilisée pour ouvrir droit à une nomination au titre de la promotion interne dans les conditions prévues par l'article 20-5 ; <br/>
<br/>
              Considérant qu'il est constant qu'aucun recrutement d'administrateur territorial n'a été effectué par la COMMUNE DE BASTIA entre l'intégration de M. B en 1990 et la nomination de M. A en 2008 ; qu'il en résulte, pour les motifs exposés plus haut, que l'arrêté du 8 décembre 2008 du maire de la COMMUNE DE BASTIA nommant M. A en qualité d'administrateur territorial en application de l'article 20-5 du décret du 20 novembre 1985 est entaché d'illégalité ; qu'il en va de même, par voie de conséquence, de l'arrêté du 15 juin 2009 le titularisant dans son grade ; que, si la COMMUNE DE BASTIA soutient que M. A remplissait dès 1991 les conditions statutaires pour être nommé administrateur territorial, cette circonstance, à la supposer exacte, est sans influence sur la légalité des décisions visées par les déférés préfectoraux ; que, dès lors, le préfet de la Haute-Corse est fondé à demander l'annulation des arrêtés des 8 décembre 2008 et 15 juin 2009 du maire de la COMMUNE DE BASTIA ; que les conclusions présentées par cette dernière au titre des dispositions de l'article L. 761-1 du code de justice administrative doivent, par suite, être rejetées ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Bastia du 8 avril 2010 est annulé.<br/>
Article 2 : L'arrêté du 8 décembre 2008 du maire de Bastia nommant M. A en qualité d'administrateur territorial, au titre de la promotion interne, à compter du 15 décembre 2008 et son arrêté du 15 juin 2009 titularisant M. A dans son grade à compter du 15 juin 2009 sont annulés.<br/>
Article 3 : Les conclusions présentées par la COMMUNE DE BASTIA au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à la COMMUNE DE BASTIA et à M. A.<br/>
Copie en sera adressée au ministre de l'intérieur, de l'outre-mer, des collectivités territoriales et de l'immigration.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-07-01-03 FONCTIONNAIRES ET AGENTS PUBLICS. STATUTS, DROITS, OBLIGATIONS ET GARANTIES. STATUT GÉNÉRAL DES FONCTIONNAIRES DE L'ÉTAT ET DES COLLECTIVITÉS LOCALES. DISPOSITIONS STATUTAIRES RELATIVES À LA FONCTION PUBLIQUE TERRITORIALE (LOI DU 26 JANVIER 1984). - RECRUTEMENT AU TITRE DE LA PROMOTION INTERNE (ART. 20-5 DU DÉCRET N° 85-1229 DU 20 NOVEMBRE 1985) - CONDITION D'UN RECRUTEMENT ANTÉRIEUR - 1) CONDITIONS DE DATE OU DE FONCTIONS ACTUELLES - ABSENCE - 2) NOTION DE RECRUTEMENT - NOMINATIONS PRONONCÉES LORS DE LA CONSTITUTION INITIALE D'UN CADRE D'EMPLOIS D'AGENTS EXERÇANT DÉJÀ LEURS FONCTIONS DANS LA COLLECTIVITÉ - EXCLUSION.
</SCT>
<ANA ID="9A"> 36-07-01-03 1) Il résulte des dispositions de l'article 20-5 du décret n° 85-1229 du 20 novembre 1985 que la seule condition posée pour procéder à une nomination au titre de la promotion interne d'un fonctionnaire territorial, dans le cas où le nombre de recrutements exigés pour ouvrir droit à cette nomination n'a pas été atteint durant la période d'au moins deux ans qui la précède, est qu'au moins un recrutement soit intervenu, sans autre considération de date ou de fonctions actuelles.,,2) Les nominations en qualité d'administrateur territorial prononcées lors de la constitution initiale de ce cadre d'emplois au bénéfice d'agents exerçant déjà leurs fonctions au sein de la collectivité qui les nomme et intégrés dans le cadre d'emplois des administrateurs territoriaux à raison de celles-ci, ne sont pas au nombre de celles qui ouvrent droit, au sens de ces dispositions, à un recrutement au titre de la promotion interne.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
