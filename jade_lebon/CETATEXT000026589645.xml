<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026589645</ID>
<ANCIEN_ID>JG_L_2012_11_000000337755</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/58/96/CETATEXT000026589645.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème et 10ème sous-sections réunies, 07/11/2012, 337755</TITRE>
<DATE_DEC>2012-11-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>337755</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème et 10ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>BALAT ; SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>M. Matthieu Schlesinger</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Claire Legras</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:337755.20121107</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 19 mars 2010 au secrétariat du contentieux du Conseil d'Etat, présenté par le ministre d'Etat, ministre de l'écologie, de l'énergie, du développement durable et de la mer, en charge des technologies vertes et des négociations sur le climat ; le ministre demande au Conseil d'Etat d'annuler l'arrêt n°s 07MA00918-07MA00925 du 15 janvier 2010 par lequel la cour administrative d'appel de Marseille, sur appel de la chambre d'agriculture du Var et du collectif de défense des personnes touchées par le plan de prévention des risques des inondations, d'une part, et de la commune de Solliès-Toucas (Var), d'autre part, a annulé le jugement n°s 0505486, 0400773, 0401530, 04303798, 043799 et 0403825 du 11 janvier 2007 du tribunal administratif de Nice et l'arrêté du 19 janvier 2004 du préfet du Var approuvant le plan de prévention des risques naturels prévisibles de type " inondation " (PPRI) de la vallée du Gapeau ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'environnement ;<br/>
<br/>
              Vu le code d'urbanisme ;<br/>
<br/>
              Vu le décret n° 95-1083 du 5 octobre 1995 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Matthieu Schlesinger, Auditeur,  <br/>
<br/>
              - les observations de la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la chambre d'agriculture du Var et de Me Balat, avocat de la commune de Solliès-Toucas,<br/>
<br/>
              - les conclusions de Mme Claire Legras, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la chambre d'agriculture du Var et à Me Balat, avocat de la commune de Solliès-Toucas ;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes du II de l'article L. 562-1 du code de l'environnement, dans sa rédaction applicable à la date de l'arrêté du préfet du Var du 19 janvier 2004 approuvant le plan de prévention des risques naturels prévisibles de type inondation de la vallée du Gapeau : " Ces plans ont pour objet, en tant que de besoin : / 1° De délimiter les zones exposées aux risques, dites " zones de danger ", en tenant compte de la nature et de l'intensité du risque encouru, d'y interdire tout type de construction, d'ouvrage, d'aménagement ou d'exploitation agricole, forestière, artisanale, commerciale ou industrielle ou, dans le cas où des constructions, ouvrages, aménagements ou exploitations agricoles, forestières, artisanales, commerciales ou industrielles pourraient y être autorisés, prescrire les conditions dans lesquelles ils doivent être réalisés, utilisés ou exploités ; / 2° De délimiter les zones, dites " zones de précaution ", qui ne sont pas directement exposées aux risques mais où des constructions, des ouvrages, des aménagements ou des exploitations agricoles, forestières, artisanales, commerciales ou industrielles pourraient aggraver des risques ou en provoquer de nouveaux et y prévoir des mesures d'interdiction ou des prescriptions telles que prévues au 1° (...) " ; qu'en vertu de l'article L. 562-4 du même code, le plan de prévention des risques naturels prévisibles approuvé vaut servitude d'utilité publique et est annexé au plan local d'urbanisme ; qu'en vertu de l'article 3 du décret du 5 octobre 1995 relatif aux plans de prévention des risques naturels prévisibles, aujourd'hui codifié à l'article R. 562-3 du code de l'environnement, le dossier de projet de plan comprend notamment " Un ou plusieurs documents graphiques délimitant les zones mentionnées aux 1° et 2° " de l'article L. 562-1 ;<br/>
<br/>
              2. Considérant qu'il résulte de ces dispositions que les documents graphiques des plans de prévention des risques naturels prévisibles, dont les prescriptions s'imposent directement aux autorisations de construire, doivent, au même titre que les documents d'urbanisme, être suffisamment précis pour permettre de déterminer les parcelles concernées par les mesures d'interdiction et les prescriptions qu'ils prévoient et, notamment, d'en assurer le respect lors de la délivrance des autorisations d'occupation ou d'utilisation du sol ; que ces dispositions n'ont, toutefois, ni pour objet ni pour effet d'imposer que ces documents fassent apparaître eux-mêmes le découpage parcellaire existant ;<br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que les documents graphiques du plan de prévention des risques naturels prévisibles de type inondation de la vallée du Gapeau comportaient, en l'espèce, un tracé suffisamment précis des limites des différentes zones que le plan avait pour objet de délimiter ; que, par suite, en estimant que les documents graphiques du plan de prévention des risques naturels prévisibles ne permettaient pas de reporter sur chaque parcelle cadastrale les éventuelles servitudes dont elle était grevée, la cour a dénaturé les pièces du dossier ; que dès lors, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, son arrêt doit être annulé ;<br/>
<br/>
              4. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soient mises à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante, les sommes que la commune de Solliès-Toucas et la chambre d'agriculture du Var demandent au titre des frais exposés par elles et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Marseille du 15 janvier 2010 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Marseille.<br/>
Article 3 : Les conclusions de la commune de Solliès-Toucas et de la chambre d'agriculture du Var présentées au titre des dispositions de l'article L. 761-1 sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à la ministre de l'écologie, du développement durable et de l'énergie, à la chambre d'agriculture du Var, au Collectif de défense des personnes touchées par le plan de prévention des risques des inondations et à la commune de Solliès-Toucas.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">68-01 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PLANS D'AMÉNAGEMENT ET D'URBANISME. - PLAN DE PRÉVENTION DES RISQUES NATURELS PRÉVISIBLES - DOCUMENTS GRAPHIQUES - OBLIGATION DE PRÉCISION SUFFISANTE POUR DÉTERMINER LES PARCELLES CONCERNÉES PAR LES MESURES D'INTERDICTION ET LES PRESCRIPTIONS - EXISTENCE - OBLIGATION DE FAIRE APPARAÎTRE LE DÉCOUPAGE PARCELLAIRE EXISTANT - ABSENCE.
</SCT>
<ANA ID="9A"> 68-01 S'il résulte des dispositions du II de l'article L. 562-1 du code de l'environnement, de l'article L. 562-4 de ce code et de l'article 3 du décret n° 95-1083 du 5 octobre 1995 relatif aux plans de prévention des risques naturels prévisibles, aujourd'hui codifié à l'article R. 562-3 du même code, que les documents graphiques des plans de prévention des risques naturels prévisibles, dont les prescriptions s'imposent directement aux autorisations de construire, doivent, au même titre que les documents d'urbanisme, être suffisamment précis pour permettre de déterminer les parcelles concernées par les mesures d'interdiction et les prescriptions qu'ils prévoient et, notamment, d'en assurer le respect lors de la délivrance des autorisations d'occupation ou d'utilisation du sol, ces dispositions n'ont toutefois ni pour objet ni pour effet d'imposer que ces documents fassent apparaître eux-mêmes le découpage parcellaire existant.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
