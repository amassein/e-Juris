<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037649058</ID>
<ANCIEN_ID>JG_L_2018_11_000000412584</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/64/90/CETATEXT000037649058.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 26/11/2018, 412584</TITRE>
<DATE_DEC>2018-11-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>412584</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUVENIN, COUDRAY, GREVY ; SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Vincent Uher</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:412584.20181126</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Le syndicat CFDT Interco Moselle a demandé au tribunal administratif de Strasbourg d'annuler la décision du 10 décembre 2014 par laquelle le président du bureau de vote du conseil général de la Moselle a refusé de retirer sa décision portant attribution des sièges aux élections des représentants à la commission administrative paritaire des agents de catégorie A. Par un jugement n° 1407156 du 2 décembre 2015, ce tribunal a rejeté sa demande. <br/>
<br/>
              Par un arrêt n° 16NC00134 du 18 mai 2017, la cour administrative d'appel de Nancy a rejeté l'appel formé par le syndicat CFDT Interco Moselle contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 18 juillet et 9 octobre 2017 au secrétariat du contentieux du Conseil d'Etat, le syndicat CFDT Interco Moselle demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge du département de la Moselle la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le décret n° 89-229 du 17 avril 1989 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Vincent Uher, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Thouvenin, Coudray, Grevy, avocat du syndicat CDFT Interco Moselle et à la SCP Piwnica, Molinié, avocat du département de la Moselle.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que des élections se sont tenues le 4 décembre 2014 pour désigner les représentants du personnel à la commission administrative paritaire des agents de catégorie A du département de la Moselle. A l'issue du scrutin, la liste présentée par le syndicat CFDT Interco Moselle et la liste présentée par le syndicat CFE-CGC ont chacune obtenu deux sièges de représentants titulaires et la liste présentée par le syndicat FO un siège. Deux sièges étaient à pourvoir pour le groupe hiérarchique supérieur (groupe 6), pour lequel seul le syndicat CFDT Interco Moselle, arrivé en première position en nombre de suffrages exprimés, avait présenté des candidats, et trois pour le groupe hiérarchique de base (groupe 5), pour lequel les trois syndicats avaient présenté des candidats. A l'issue du scrutin, la liste CFDT Interco Moselle s'est vu attribuer les deux sièges à pourvoir du groupe 6 et aucun siège dans le groupe 5. Les listes CFE-CGC et FO se sont vu attribuer respectivement deux sièges et un siège dans le groupe 5. Par une décision du 10 décembre 2014, le président du bureau de vote a rejeté la demande  du syndicat CFDT Interco Moselle du 8 décembre, tendant au retrait de la décision de répartition des sièges obtenus et à ce qu'un siège lui soit attribué dans le groupe 5. Par un jugement du 2 novembre 2015, le tribunal administratif de Strasbourg a rejeté la demande de ce syndicat d'annulation de la décision du 10 décembre 2014. Le syndicat CFDT Interco Moselle se pourvoit en cassation contre l'arrêt du 18 mai 2017 par lequel la cour administrative d'appel de Nancy a rejeté son appel.<br/>
<br/>
              2. Aux termes de l'article 23 du décret du 17 avril 1989 relatif aux commissions administratives paritaires des collectivités territoriales et de leurs établissements publics : " Les représentants du personnel au sein des commissions administratives paritaires sont élus à la proportionnelle. La désignation des membres titulaires est effectuée de la manière suivante : / a) Nombre total de sièges de représentants titulaires attribués à chaque liste : / Chaque liste a droit à autant de sièges de représentants titulaires que le nombre de voix recueillies par elle contient de fois le quotient électoral. / Les sièges de représentants titulaires restant éventuellement à pourvoir sont attribués suivant la règle de la plus forte moyenne. / b) Désignation des représentants titulaires : / Les listes exercent leur choix successivement dans l'ordre décroissant du nombre de sièges qu'elles obtiennent. La liste ayant droit au plus grand nombre de sièges choisit chacun d'eux, le cas échéant, dans un groupe hiérarchique différent sous réserve de ne pas empêcher par son choix une autre liste d'obtenir le nombre de sièges auxquels elle a droit dans les groupes hiérarchiques pour lesquels elle avait présenté des candidats. / Les autres listes exercent ensuite leur choix successivement dans l'ordre décroissant du nombre de sièges auxquels elles peuvent prétendre, dans les mêmes conditions et sous les mêmes réserves / (...) En cas d'égalité du nombre de sièges obtenus, l'ordre des choix est déterminé par le nombre respectif de suffrages obtenu par les listes en présence (...) / Dans l'hypothèse où une partie ou la totalité des sièges n'a pu être pourvue par voie d'élection, la commission administrative paritaire est complétée par voie de tirage au sort parmi les électeurs à cette commission relevant de chaque groupe hiérarchique concerné ". Il résulte des termes mêmes de cet article, qui vise à garantir les droits des listes qui ne sont pas arrivées en tête lors des élections des représentants du personnel aux commissions administratives paritaires des collectivités territoriales, que ces listes doivent être assurées, en raison des conditions imposées aux choix de la liste qui a obtenu le plus grand nombre de sièges, non seulement qu'elles obtiendront le nombre de sièges auxquels les résultats du scrutin leur donnent droit, mais encore qu'elles pourront obtenir ces sièges dans les groupes hiérarchiques pour lesquels elles avaient présenté des candidats, dans la mesure où le nombre des sièges qu'elles ont obtenus le leur permet. <br/>
<br/>
              3. En premier lieu, en jugeant que si la liste CFDT Interco Moselle, qui avait choisi de présenter des candidats dans les groupes 5 et 6 et avait obtenu le plus grand nombre de suffrages, devait, en application des dispositions précitées de l'article 23 du décret du 17 avril 1989, bénéficier de la priorité de choix des sièges à pourvoir dans les deux groupes hiérarchiques, l'attribution à ce syndicat d'un siège dans le groupe 5 aurait eu pour effet de priver soit le syndicat CFE-CGC, soit le syndicat FO, dont les listes étaient arrivées respectivement en deuxième et troisième positions, d'obtenir les sièges auxquels les résultats du scrutin leur donnaient droit, dans l'unique groupe hiérarchique dans lequel ils avaient présenté des candidats, pour en déduire qu'il convenait, pour respecter ces mêmes dispositions, d'attribuer à la liste CFDT Interco Moselle les deux sièges du groupe 6 et aucun des sièges du groupe 5, la cour administrative d'appel n'a pas entaché son arrêt d'erreur de droit.<br/>
<br/>
              4. En second lieu, il résulte des dispositions précitées de l'article 23 du décret du 17 avril 1989 que ce n'est que dans l'hypothèse où des sièges n'ont pu être pourvus par la voie de l'élection qu'il est procédé par voie de tirage au sort. En jugeant que les sièges de représentants du personnel titulaires avaient régulièrement été pourvus par la voie de l'élection, la cour a nécessairement écarté l'argumentation du syndicat requérant tirée de ce qu'il aurait fallu procéder par voie de tirage au sort pour attribuer le second siège du groupe 6. Le syndicat requérant n'est par suite pas fondé à soutenir que la cour aurait entaché son arrêt d'irrégularité en ne répondant pas à cette argumentation. <br/>
<br/>
              5. Il résulte de ce qui précède que le pourvoi du syndicat CFDT Interco Moselle doit être rejeté.<br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du syndicat CFDT Interco Moselle une somme de 3 000 euros à verser au département de Moselle au titre des dispositions de l'article L. 761-1 du code de justice administrative. Ces mêmes dispositions font obstacle à ce qu'il soit fait droit aux conclusions présentées, au même titre, par le syndicat CFDT Interco Moselle. <br/>
<br/>
<br/>
<br/>                          D E C I D E :<br/>
                                        --------------<br/>
<br/>
<br/>
Article 1er : Le pourvoi du syndicat CFDT Interco Moselle est rejeté. <br/>
<br/>
Article 2 : Le syndicat CFDT Interco Moselle versera la somme de 3 000 euros au département de la Moselle au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée au syndicat CFDT Interco Moselle et au département de la Moselle.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-07-05-015 FONCTIONNAIRES ET AGENTS PUBLICS. STATUTS, DROITS, OBLIGATIONS ET GARANTIES. COMMISSIONS ADMINISTRATIVES PARITAIRES. ÉLECTIONS. - ELECTIONS AUX CAP DES COLLECTIVITÉS TERRITORIALES - ATTRIBUTION DES SIÈGES AUX LISTES N'ÉTANT PAS ARRIVÉES EN TÊTE (ART. 23 DU DÉCRET DU 17 AVRIL 1989) - EXIGENCES [RJ1] - LISTES DEVANT ÊTRE ASSURÉES D'OBTENIR LE NOMBRE DE SIÈGES CORRESPONDANT À LEURS RÉSULTATS -  EXISTENCE - LISTES DEVANT OBTENIR CES SIÈGES DANS LES GROUPES HIÉRARCHIQUES POUR LESQUELS ELLES ONT PRÉSENTÉ DES CANDIDATS - EXISTENCE, DANS LA MESURE OÙ LE NOMBRE DE SIÈGES OBTENUS LE LEUR PERMET.
</SCT>
<ANA ID="9A"> 36-07-05-015 Il résulte des termes mêmes de l'article 23 du décret n° 89-229 du 17 avril 1989, qui vise à garantir les droits des listes qui ne sont pas arrivées en tête lors des élections des représentants du personnel aux commissions administratives paritaires (CAP) des collectivités territoriales, que ces listes doivent être assurées, en raison des conditions imposées aux choix de la liste qui a obtenu le plus grand nombre de sièges, non seulement qu'elles obtiendront le nombre de sièges auxquels les résultats du scrutin leur donnent droit, mais encore qu'elles pourront obtenir ces sièges dans les groupes hiérarchiques pour lesquels elles avaient présenté des candidats, dans la mesure où le nombre des sièges qu'elles ont obtenus le leur permet.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 15 octobre 1999, Ville de Dieppe, n° 195786, inédite au Recueil.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
