<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042854722</ID>
<ANCIEN_ID>JG_L_2020_12_000000431589</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/85/47/CETATEXT000042854722.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème - 10ème chambres réunies, 31/12/2020, 431589</TITRE>
<DATE_DEC>2020-12-31</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>431589</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème - 10ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Matias de Sainte Lorette</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Céline  Guibé</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:431589.20201231</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, un mémoire en réplique et cinq nouveaux mémoires, enregistrés les 12 juin, 10 octobre et 24 décembre 2019, le 24 février, 6 juin et 16 juillet 2020 au secrétariat du contentieux du Conseil d'Etat, la société Total Raffinage France demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2019-570 du 7 juin 2019 portant sur la taxe incitative relative à l'incorporation des biocarburants ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment ses articles 61-1 et 62 ;<br/>
              - la directive 2009/28/CE du Parlement européen et du Conseil du 23 avril 2009 ;<br/>
              - la directive (UE) 2018/2001 du Parlement européen et du Conseil du 11 décembre 2018 ;<br/>
              - le règlement délégué (UE) 2019/807 de la Commission du 13 mars 2019 ;<br/>
              - le code des douanes ; <br/>
              - la loi n° 2018-1317 du 28 décembre 2018 ;<br/>
              - la décision n° 2019-808 QPC du 11 octobre 2019 du Conseil constitutionnel statuant sur la question prioritaire de constitutionnalité soulevée par la société Total raffinage France ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Matias de Sainte Lorette, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Céline Guibé, rapporteur public ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 18 décembre 2020, présentée par la société Total Raffinage France ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. En premier lieu, aux termes de l'article 266 quindecies du code des douanes : " I.-Les redevables de la taxe intérieure de consommation prévue à l'article 265 sont redevables d'une taxe incitative relative à l'incorporation de biocarburants. / (...) III.-La taxe incitative relative à l'incorporation de biocarburants est assise sur le volume total, respectivement, des essences et des gazoles pour lesquels elle est devenue exigible au cours de l'année civile. / Le montant de la taxe est calculé séparément, d'une part, pour les essences et, d'autre part, pour les gazoles. / Ce montant est égal au produit de l'assiette définie au premier alinéa du présent III par le tarif fixé au IV, auquel est appliqué un coefficient égal à la différence entre le pourcentage national cible d'incorporation d'énergie renouvelable dans les transports, fixé au même IV, et la proportion d'énergie renouvelable contenue dans les produits inclus dans l'assiette. Si la proportion d'énergie renouvelable est supérieure ou égale au pourcentage national cible d'incorporation d'énergie renouvelable dans les transports, la taxe est nulle. / (...) V.-A.-La proportion d'énergie renouvelable désigne la proportion, évaluée en pouvoir calorifique inférieur, d'énergie produite à partir de sources renouvelables dont le redevable peut justifier qu'elle est contenue dans les carburants inclus dans l'assiette, compte tenu, le cas échéant, des règles de calcul propres à certaines matières premières prévues aux C et D du présent V et des dispositions du VII. / L'énergie contenue dans les biocarburants est renouvelable lorsque ces derniers remplissent les critères de durabilité définis à l'article 17 de la directive 2009/28/ CE du Parlement européen et du Conseil du 23 avril 2009 relative à la promotion de l'utilisation de l'énergie produite à partir de sources renouvelables et modifiant puis abrogeant les directives 2001/77/ CE et 2003/30/ CE dans sa rédaction en vigueur au 24 septembre 2018 ".<br/>
<br/>
              2. En vertu des dispositions du B du V du même article 266 quindecies du code des douanes, la part d'énergie issue de matières premières dont la culture et l'utilisation pour la production de biocarburants présentent un risque élevé d'induire indirectement une hausse des émissions de gaz à effet de serre neutralisant la réduction des émissions qui résulte de la substitution par ces biocarburants des carburants fossiles et dont l'expansion des cultures s'effectue sur des terres présentant un important stock de carbone, n'est pas prise en compte au-delà d'un seuil. Ce seuil est égal au produit entre, d'une part, la proportion de l'énergie issue des matières premières répondant aux conditions rappelées ci-dessus qui est contenue respectivement dans les gazoles et dans les essences, en France métropolitaine en 2017, et, d'autre part, un pourcentage fixé à 100 % pour les années 2020 à 2023, à 87,5 % pour l'année 2024 et diminuant progressivement jusqu'à 0 % à compter de l'année 2031. <br/>
<br/>
              3. En revanche, les dispositions mentionnées au point 2 ne s'appliquent pas à l'énergie issue de ces matières premières lorsqu'il est constaté qu'elles ont été produites dans des conditions particulières permettant d'éviter le risque élevé d'induire indirectement une hausse des émissions de gaz à effet de serre neutralisant la réduction de ces émissions qui résulte de la substitution par ces biocarburants des carburants fossiles.<br/>
<br/>
              4. Enfin, aux termes du dernier alinéa du B du V de l'article 266 quindecies du code des douanes : " Ne sont pas considérés comme des biocarburants les produits à base d'huile de palme ". Ces dispositions, éclairées par les travaux préparatoires de l'article 192 de la loi du 28 décembre 2018 de finances pour 2019 dont elles sont issues, ont pour objet de ne pas tenir compte des biocarburants issus d'huile de palme au titre de la part d'énergie renouvelable servant au calcul du coefficient mentionné au dernier alinéa du III de l'article 266 quindecies du code des douanes, à compter du 1er janvier 2020, date de leur entrée en vigueur conformément aux dispositions du III du même article 192 de la loi du 28 décembre 2018. <br/>
<br/>
              5. En second lieu, le décret du 7 juin 2019 portant sur la taxe incitative à l'incorporation de biocarburants fixe certaines modalités d'application des dispositions de l'article 266 quindecies du code des douanes, s'agissant des justificatifs de l'énergie renouvelable prise en compte pour la liquidation de la taxe, des conditions de traçabilité de certaines matières premières et des modalités déclaratives. Aux termes de l'article 1er de ce décret : " Pour l'application du présent décret : / (...) 4° Les biocarburants s'entendent des produits issus de la biomasse, destinés à être incorporés dans des carburants ou pouvant être utilisés en l'état en tant que carburants, à l'exception des produits à base d'huile de palme ". Aux termes de l'article 7 de ce décret : " Les certificats et comptabilités matières de suivi de l'énergie renouvelable mentionnent les dénominations et quantités de produits éligibles constitués d'énergie renouvelable, incorporés ou non dans des carburants imposables, en distinguant : / (...) 4° Les produits à base d'huile de palme ". <br/>
<br/>
              6. Eu égard aux moyens qu'elle invoque, la société Total raffinage France doit être regardée comme demandant l'annulation pour excès de pouvoir du 4° de l'article 1er, en tant qu'il exclut les produits à base d'huile de palme de la définition des biocarburants, et du 4° de l'article 7 du décret du 9 juin 2019. Elle soutient notamment, par la voie de l'exception, que le dernier alinéa du B du V de l'article 266 quindecies du code des douanes, pour l'application duquel le décret attaqué a été pris, méconnaît les objectifs de la directive 2009/28/CE du Parlement européen et du Conseil du 23 avril 2009 relative à la promotion de l'utilisation de l'énergie produite à partir de sources renouvelables et ceux de la directive (UE) 2018/2001 du Parlement européen et du Conseil du 11 décembre 2018 relative à la promotion de l'utilisation de l'énergie produite à partir de sources renouvelables.<br/>
<br/>
              7. L'association " Les amis de la Terre " justifie d'un intérêt suffisant au maintien du décret attaqué. Son intervention est donc recevable.<br/>
<br/>
              Sur le moyen tiré de la contrariété à la Constitution des dispositions législatives servant de base légale au décret attaqué :<br/>
<br/>
              8. La société requérante soulève, par voie d'exception, l'inconstitutionnalité du dernier alinéa du B du V de l'article 266 quindecies du code des douanes. Toutefois, par sa décision n° 2019-808 QPC du 11 octobre 2019, le Conseil constitutionnel a jugé que ces dispositions ne méconnaissent pas le principe d'égalité devant la loi et devant les charges publiques, ni aucun autre droit ou liberté que la Constitution garantit. Il a en conséquence déclaré ces dispositions conformes à la Constitution. Le moyen tiré de leur inconstitutionnalité doit par suite être écarté.<br/>
<br/>
              Sur le moyen tiré de l'incompatibilité des dispositions législatives servant de base légale au décret attaqué avec les objectifs de la directive 2018/2001/UE et avec le règlement délégué (UE) 2019/807 :<br/>
<br/>
              9. Aux termes de l'article 25 de la directive 2018/2001 du 11 décembre 2018 relative à la promotion de l'utilisation de l'énergie produite à partir de sources renouvelables : " 1. Afin d'intégrer l'utilisation de l'énergie renouvelable dans le secteur des transports, chaque État membre impose une obligation aux fournisseurs de carburants afin de faire en sorte que, d'ici à 2030, la part de l'énergie renouvelable dans la consommation finale d'énergie dans le secteur des transports atteigne au moins 14 % (part minimale), conformément à une trajectoire indicative définie par l'État membre en question et calculée conformément à la méthode établie dans le présent article et aux articles 26 et 27 (...) ". Aux termes de l'article 26 de cette même directive : " 1. Aux fins du calcul, dans un État membre donné, (...) de la part minimale visée à l'article 25, paragraphe 1, premier alinéa, la part des biocarburants et des bioliquides, ainsi que des combustibles issus de la biomasse consommés dans le secteur des transports, lorsqu'ils sont produits à partir de cultures destinées à l'alimentation humaine et animale, ne dépasse pas de plus de un point de pourcentage la part de ces carburants dans la consommation finale d'énergie dans les secteurs des transports routier et ferroviaire dans cet État membre en 2020, avec un maximum de 7 % de la consommation finale d'énergie dans les secteurs des transports routier et ferroviaire dans ledit État membre. / (...) Les États membres peuvent fixer une limite inférieure et peuvent opérer une distinction aux fins de l'article 29, paragraphe 1, entre différents biocarburants, bioliquides et combustibles issus de la biomasse produits à partir de cultures destinées à l'alimentation humaine ou animale, en tenant compte des meilleures données disponibles relatives à l'impact des changements indirects dans l'affectation des sols. Les États membres peuvent par exemple fixer une limite inférieure pour la part des biocarburants, bioliquides et combustibles issus de la biomasse produits à partir de plantes oléagineuses. / (...) 2. Aux fins du calcul, dans un État membre donné, (...) de la part minimale visée à l'article 25, paragraphe 1, premier alinéa, la part des biocarburants, bioliquides et combustibles issus de la biomasse produits à partir de cultures destinées à l'alimentation humaine et animale, présentant un risque élevé d'induire des changements indirects dans l'affectation des sols et dont la zone de production gagne nettement sur les terres présentant un important stock de carbone, n'excède pas le niveau de consommation de ces combustibles ou carburants dans l'État membre concerné enregistré en 2019, à moins que les produits en question ne soient certifiés comme étant des biocarburants, bioliquides ou combustibles issus de la biomasse présentant un faible risque d'induire des changements indirects dans l'affectation des sols conformément au présent paragraphe. / À compter du 31 décembre 2023 et jusqu'au 31 décembre 2030 au plus tard, cette limite diminue progressivement pour s'établir à 0 %. / (...) Le 1er février 2019 au plus tard, la Commission adopte un acte délégué conformément à l'article 35 pour compléter la présente directive en définissant les critères pour la certification des biocarburants, bioliquides et combustibles issus de la biomasse présentant un faible risque d'induire des changements indirects dans l'affectation des sols et pour la détermination des matières premières présentant un risque élevé d'induire des changements indirects dans l'affectation des sols et dont la zone de production gagne nettement sur les terres présentant un important stock de carbone. Le rapport et l'acte délégué l'accompagnant sont fondés sur les meilleures données scientifiques disponibles ". Aux termes de l'article 29 de la même directive : " 1. L'énergie produite à partir des biocarburants, des bioliquides et des combustibles issus de la biomasse est prise en considération aux fins visées aux points a), b) et c), du présent alinéa uniquement si ceux-ci répondent aux critères de durabilité et de réduction des émissions de gaz à effet de serre énoncés aux paragraphes 2 à 7 et au paragraphe 10: / (...) c) déterminer l'admissibilité à une aide financière pour la consommation de biocarburants, de bioliquides et de combustibles issus de la biomasse. / (...) Les critères de durabilité et de réduction des émissions de gaz à effet de serre énoncés aux paragraphes 2 à 7 et au paragraphe 10 s'appliquent quelle que soit l'origine géographique de la biomasse. / (...) 12. Aux fins visées au présent article, paragraphe 1, premier alinéa, points a), b) et c), et sans préjudice des articles 25 et 26, les États membres ne refusent pas de prendre en considération, pour d'autres motifs de durabilité, les biocarburants et les bioliquides obtenus conformément au présent article. La présente disposition s'entend sans préjudice de l'aide publique accordée en vertu des régimes d'aide approuvés avant le 24 décembre 2018. (...) 14. Aux fins visées au paragraphe 1, premier alinéa, points a), b) et c), les États membres peuvent établir des critères de durabilité supplémentaires pour les combustibles ou carburants issus de la biomasse. / Le 31 décembre 2026 au plus tard, la Commission évalue l'incidence de ces critères supplémentaires sur le marché intérieur et présente au besoin une proposition visant à en assurer l'harmonisation ". Enfin, aux termes de l'article 36 de la même directive : " 1. Les États membres mettent en vigueur les dispositions législatives, réglementaires et administratives nécessaires pour se conformer aux articles 2 à 13, aux articles 15 à 31, (...) et aux annexes II, III et V à IX au plus tard le 30 juin 2021 ".<br/>
<br/>
              10. L'article 3 du règlement délégué (UE) 2019/807 de la Commission du 13 mars 2019 fixe les critères servant à déterminer les matières premières présentant un risque élevé d'induire des changements indirects dans l'affectation des sols dont la zone de production gagne nettement sur les terres présentant un important stock de carbone. L'article 4 du même règlement délégué prévoit que les biocarburants, les bioliquides et les combustibles issus de la biomasse ne peuvent être certifiés comme présentant un faible risque d'induire des changements indirects dans l'affectation des sols que s'ils respectent les critères de durabilité et de réduction des émissions de gaz à effet de serre énoncés à l'article 29 de la directive (UE) 2018/2001 et s'ils ont été produits à partir de matières premières supplémentaires obtenues au moyen de mesures d'additionnalité. Il résulte des informations figurant dans l'annexe à ce même règlement que la palme, qui remplit les critères cumulatifs fixés à l'article 4 du même règlement, doit être regardée comme présentant un risque élevé d'induire des changements indirects dans l'affectation des sols et comme étant produite dans une zone de production qui gagne nettement sur les terres présentant un important stock de carbone. <br/>
<br/>
              11. La société requérante soutient que les dispositions du décret du 7 juin 2019 qu'elle attaque sont illégales en ce qu'elles font application du dernier alinéa du B du V de l'article 266 quindecies du code des douanes, lequel méconnaît selon elle les objectifs de la directive (UE) 2018/2001 et les dispositions du règlement délégué (UE) 2019/807, dès lors que ces dispositions législatives excluent dès le 1er janvier 2020 que les produits à base d'huile de palme soient considérés comme des biocarburants durables pouvant bénéficier du régime d'aide prévu à l'article 266 quindecies sans prévoir qu'ils puissent être certifiés comme présentant un faible risque d'induire des changements indirects dans l'affectation des sols et sans appliquer cette limitation selon un calendrier progressif entre le 31 décembre 2023 et le 31 décembre 2030.<br/>
<br/>
              12. Toutefois, il résulte des termes clairs de l'article 29 de la directive 2018/2001 et notamment de son paragraphe 14 que les Etats membres sont autorisés, notamment pour déterminer l'admissibilité à une aide financière pour la consommation de biocarburants issus de la biomasse, à établir des critères de durabilité supplémentaires pour les carburants issus de la biomasse.<br/>
<br/>
              13. Dans son rapport du 13 mars 2019 adressé notamment au Parlement européen et au Conseil " sur l'état de l'expansion, à l'échelle mondiale, de la production de certaines cultures destinées à l'alimentation humaine et animale ", la Commission européenne relève, d'une part, que l'augmentation annuelle nette de la superficie récoltée entre 2008 et 2016 est de 4 % pour l'huile de palme contre 3 % pour le soja et 1 % pour le colza, d'autre part, que la fraction de l'expansion de la culture d'huile de palme sur des terres présentant un stock de carbone important est de 45 % contre 8 % pour la culture du soja.<br/>
<br/>
              14. Par suite, il résulte de ce qui a été dit aux points 12 et 13 que les autorités françaises pouvaient opérer entre biocarburants la distinction retenue par le B du V de l'article 266 quindecies du code des douanes sans méconnaître ni les objectifs fixés par la directive 2018/2001, ni les dispositions du règlement délégué (UE) 2019/807. <br/>
<br/>
              Sur le moyen tiré de l'incompatibilité des dispositions législatives servant de base légale au décret attaqué avec les objectifs de la directive 2009/28/CE :<br/>
<br/>
              15. La société requérante soutient également que les dispositions du dernier alinéa du B du V de l'article 266 quindecies du code des douanes sont incompatibles avec les objectifs de l'article 17 de la directive 2009/28/CE du 23 avril 2009 relative à la promotion de l'utilisation de l'énergie produite à partir de sources renouvelables et modifiant puis abrogeant les directives 2001/77/CE et 2003/30/CE. <br/>
<br/>
              16. Toutefois, les dispositions législatives contestées ont été prises pendant le délai de transposition de la directive 2018/2001, laquelle n'abroge la directive 2009/28/CE qu'à compter du 1er juillet 2021 mais était en vigueur à la date d'adoption du décret attaqué. Par suite, et dès lors qu'il résulte de ce qui a été dit au point 14 ci-dessus que les dispositions législatives ne sont pas incompatibles avec les objectifs fixés par la directive 2018/2001, la requérante ne peut utilement soutenir que ces dispositions méconnaîtraient les objectifs de la directive 2009/28/CE. <br/>
<br/>
              Sur le moyen tiré de l'incompatibilité des dispositions législatives servant de base légale au décret attaqué avec l'article 30 du traité sur le fonctionnement de l'Union européenne :<br/>
<br/>
              17. Aux termes de l'article 30 du traité sur le fonctionnement de l'Union européenne : " Les droits de douane à l'importation et à l'exportation ou taxes d'effet équivalent sont interdits entre les États membres. Cette interdiction s'applique également aux droits de douane à caractère fiscal ". Aux termes de l'article 110 du même traité : " Aucun État membre ne frappe directement ou indirectement les produits des autres États membres d'impositions intérieures, de quelque nature qu'elles soient, supérieures à celles qui frappent directement ou indirectement les produits nationaux similaires. / En outre, aucun État membre ne frappe les produits des autres États membres d'impositions intérieures de nature à protéger indirectement d'autres productions ". Constitue une taxe d'effet équivalent à un droit de douane toute charge pécuniaire, même minime, unilatéralement imposée, quelles que soient son appellation et sa technique, et frappant les marchandises en raison du fait qu'elles franchissent une frontière, lorsqu'elle n'est pas un droit de douane proprement dit. En revanche, une charge pécuniaire résultant d'un régime général d'impositions intérieures appréhendant systématiquement selon les mêmes critères objectifs des catégories de produits indépendamment de leur origine ou de leur destination relève de l'article 110 du traité sur le fonctionnement de l'Union européenne, qui interdit les impositions intérieures discriminatoires.<br/>
<br/>
              18. La société requérante soutient que l'exclusion de la minoration du taux de taxe incitative à l'incorporation de biocarburants, qui ne s'applique qu'aux seuls produits à base d'huile de palme, lesquels sont des produits importés d'Etats tiers, méconnaît l'interdiction fixée à l'article 30 du traité sur le fonctionnement de l'Union européenne. <br/>
<br/>
              19. Toutefois, la taxe incitative à l'incorporation des biocarburants frappe les essences et les gazoles, quelle que soit l'origine des matières premières incorporées dans ceux-ci, selon les mêmes critères objectifs et à la date du même fait générateur, à savoir lors de leur mise à la consommation. La minoration du taux de la taxe en cas d'incorporation de certains biocarburants fait partie intégrante de la taxe elle-même et ne constitue pas une taxe distincte. Dès lors, le fait que l'exclusion de la minoration du taux frappe exclusivement des produits importés issus de l'huile de palme ne saurait faire échapper ni la taxe en général, ni le mécanisme de minoration du taux, au champ d'application de l'article 110 du traité sur le fonctionnement de l'Union européenne relatif aux impositions intérieures, de sorte que leur compatibilité avec le droit de l'Union européenne doit être appréciée dans le cadre des stipulations de cet article et non pas au regard de l'article 30 du traité sur le fonctionnement de l'Union européenne. Par suite, le moyen soulevé par la requérante ne peut qu'être écarté. <br/>
<br/>
              Sur le moyen tiré de l'incompatibilité des dispositions législatives servant de base légale au décret attaqué avec le principe d'égalité de traitement et de proportionnalité :<br/>
<br/>
              20. Le principe d'égalité de traitement, en tant que principe général du droit de l'Union, impose que des situations comparables ne soient pas traitées de manière différente et que des situations différentes ne soient pas traitées de manière égale, à moins qu'un tel traitement ne soit objectivement justifié. Le caractère comparable de situations différentes s'apprécie eu égard à l'ensemble des éléments qui les caractérisent. Ces éléments doivent, notamment, être déterminés et appréciés à la lumière de l'objet et du but de l'acte de l'Union qui institue la distinction en cause. Doivent, en outre, être pris en considération les principes et les objectifs du domaine dont relève l'acte en cause. Cependant, une différence de traitement entre des situations comparables est justifiée dès lors qu'elle est fondée sur un critère objectif et raisonnable, c'est-à-dire lorsqu'elle est en rapport avec un but légalement admissible poursuivi par la législation en cause et que cette différence est proportionnée au but poursuivi par le traitement concerné.<br/>
<br/>
              21. Pour soutenir que les dispositions législatives contestées méconnaissent les principes d'égalité de traitement et de proportionnalité garantis par le droit de l'Union européenne, la requérante estime qu'en écartant les biocarburants produits à base d'huile de palme de l'avantage fiscal qu'elles instituent, ces dispositions créent entre biocarburants provenant de différentes plantes oléagineuses une différence de traitement qui n'est pas justifiée par les objectifs de la directive (UE) 2018/2001 et les dispositions du règlement délégué (UE) 2019/807. Toutefois, il résulte de ce qui a été dit aux points 13 et 14 ci-dessus que l'huile de palme n'est pas dans une situation comparable à celles des autres plantes oléagineuses au regard de l'impact de sa culture sur les changements indirects dans l'affectation des sols. Par suite, le moyen de la requérante doit être écarté. <br/>
<br/>
              Sur le moyen tiré de l'incompatibilité des dispositions législatives servant de base légale au décret attaqué avec le principe de sécurité juridique et de protection de la confiance légitime :<br/>
<br/>
              22. Le principe de sécurité juridique, qui a pour corollaire le principe de protection de la confiance légitime, exige, notamment, que les règles de droit soient claires, précises et prévisibles dans leurs effets, en particulier lorsqu'elles peuvent avoir sur les individus et les entreprises des conséquences défavorables. Par ailleurs, le droit de se prévaloir du principe de protection de la confiance légitime s'étend à tout particulier ou toute entreprise qui se trouve dans une situation de laquelle il ressort que l'autorité compétente, en lui fournissant des assurances précises, a fait naître, chez lui, des espérances fondées. Enfin, le principe de sécurité juridique n'exige pas l'absence de modification législative, mais requiert plutôt que le législateur national tienne compte des situations particulières des opérateurs économiques et prévoie, le cas échéant, des adaptations à l'application des nouvelles règles juridiques.<br/>
<br/>
              23. La requérante soutient que les dispositions législatives contestées méconnaissent les principes de sécurité juridique et de protection de la confiance légitime garantis par le droit de l'Union européenne, dès lors que le report d'un an de l'entrée en vigueur du dispositif voté dans la loi du 28 décembre 2018 n'est pas suffisant pour tenir compte de la situation des opérateurs économiques ayant consenti des investissements importants pour utiliser dans le cadre de leur activité de l'huile de palme afin de produire des biocarburants et qui devront consentir de nouveaux investissements pour adapter leurs procédés industriels. <br/>
<br/>
              24. Toutefois, en premier lieu, si la requérante se prévaut de ce qu'elle a obtenu, par arrêté du 16 août 2018, l'autorisation de reconvertir les installations de la raffinerie de La Mède en bioraffinerie, elle ne pouvait tirer de cette seule délivrance des espérances fondées, relatives au maintien de la législation fiscale relative aux biocarburants à base d'huile de palme.<br/>
<br/>
              25. En second lieu, en prévoyant un délai d'un an pour leur entrée en vigueur, les dispositions attaquées n'ont pas porté atteinte à la sécurité juridique des opérateurs économiques concernés. <br/>
<br/>
              26. Il résulte de ce qui précède que le moyen tiré de l'atteinte au principe de sécurité juridique et de protection de la confiance légitime ne peut qu'être écarté. <br/>
<br/>
              27. Il résulte de tout ce qui précède que la société Total Raffinage France n'est pas fondée à demander l'annulation les dispositions du décret du 7 juin 2019 qu'elle attaque. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de l'Etat lequel n'est pas, dans la présente instance, la partie perdante. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'intervention de l'association " Les amis de la Terre " est admise. <br/>
Article 2 : La requête de la société Total Raffinage France est rejetée. <br/>
Article 3 : La présente décision sera notifiée à la société Total Raffinage France, au Premier ministre, à la ministre de la transition écologique, au ministre de l'économie, des finances et de la relance, au ministre de l'agriculture et à l'association " Les amis de la Terre ". <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">15-02-04 COMMUNAUTÉS EUROPÉENNES ET UNION EUROPÉENNE. PORTÉE DES RÈGLES DU DROIT DE L'UNION EUROPÉENNE. DIRECTIVES. - SUCCESSION DE DIRECTIVES DANS LE TEMPS - MOYEN TIRÉ DE L'INCOMPATIBILITÉ D'UNE DISPOSITION LÉGISLATIVE AVEC LA DIRECTIVE ANCIENNE ALORS QUE LA DIRECTIVE NOUVELLE, AVEC LAQUELLE CETTE DISPOSITION EST COMPATIBLE, EST ENTRÉE EN VIGUEUR - MOYEN INOPÉRANT, ALORS MÊME QUE LA DIRECTIVE ANCIENNE N'A PAS ENCORE ÉTÉ ABROGÉE [RJ1].
</SCT>
<ANA ID="9A"> 15-02-04 Directive (UE) 2018/2001 du 11 décembre 2018 relative à la promotion de l'utilisation de l'énergie produite à partir de sources renouvelables n'ayant pas encore abrogé la directive 2009/28/CE du 23 avril 2009, de même objet et qu'elle remplace, mais étant déjà entrée en vigueur à la date d'adoption du décret attaqué.,,,Dès lors que les dispositions législatives prises pendant le délai de transposition de la directive (UE) 2018/2001, contestées par la voie de l'exception, ne sont pas incompatibles avec les objectifs fixés par celle-ci, le moyen tiré de ce que ces dispositions méconnaîtraient les objectifs de la directive 2009/28/CE est inopérant.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., s'agissant de la portée d'une directive avant l'expiration de son délai de transposition, CE, 10 janvier 2001, France Nature Environnement, n° 217237, p. 10.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
