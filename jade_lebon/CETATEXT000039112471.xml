<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039112471</ID>
<ANCIEN_ID>JG_L_2019_09_000000430368</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/11/24/CETATEXT000039112471.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 18/09/2019, 430368</TITRE>
<DATE_DEC>2019-09-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>430368</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO ; SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:430368.20190918</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
Procédure contentieuse antérieure <br/>
<br/>
              La société des eaux de Corse a demandé au juge des référés du tribunal administratif de Bastia, sur le fondement de l'article L. 551-1 du code de justice administrative, d'une part, d'enjoindre avant dire droit à la communauté de communes de l'Ile-Rousse-Balagne (CCIRB) de produire les documents permettant de vérifier de quelle manière elle a contrôlé la candidature et l'offre de l'Office d'équipement hydraulique de Corse (OEHC) au regard, respectivement, de l'obligation de justifier d'un intérêt public local et de l'obligation d'opérer une séparation comptable permettant de distinguer ses moyens et ressources relevant de sa mission de service public, et, d'autre part, d'annuler la procédure relative au contrat de concession de service public de distribution de l'eau potable lancée par la communauté de communes de l'Ile-Rousse-Balagne ou, à titre subsidiaire, d'enjoindre à celle-ci de se conformer à ses obligations de mise en concurrence en reprenant la procédure au stade de l'examen des candidatures. Par une ordonnance n° 1900420 du 18 avril 2019, le juge des référés du tribunal administratif de Bastia a annulé la procédure relative à la concession du service public de distribution de l'eau potable de la communauté de communes de l'Ile-Rousse-Balagne<br/>
<br/>
Procédures devant le Conseil d'Etat <br/>
<br/>
              1° Sous le n° 430368, par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 3 et 17 mai et le 8 juillet 2019 au secrétariat du contentieux du Conseil d'Etat, la communauté de communes de l'Ile-Rousse-Balagne demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de rejeter la demande de la société des eaux de Corse ;<br/>
<br/>
              3°) de mettre à la charge de la société des eaux de Corse la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
              2° Sous le n° 430474, par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 3 et 17 mai et le 19 juillet 2019 au secrétariat du contentieux du Conseil d'Etat, l'Office d'équipement hydraulique de la Corse demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de rejeter la demande de la société des eaux de Corse ;<br/>
<br/>
              3°) de mettre à la charge de la société des eaux de Corse la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - l'ordonnance n° 2016-65 du 29 janvier 2016 ;<br/>
              - le décret n° 2016-86 du 1er février 2016 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean Sirinelli, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de la communauté de commune de l'Ile-Rousse-Balagne, à la SCP Piwnica, Molinié, avocat de la société des eaux de Corse et à la SCP Foussard, Froger, avocat de l'Office d'équipement hydraulique de la Corse ;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Les pourvois de la communauté de communes de l'Ile-Rousse-Balagne et de l'Office d'équipement hydraulique de Corse sont dirigés contre la même décision. Il y a lieu de les joindre pour statuer par une seule décision.<br/>
<br/>
              2. Aux termes de l'article L. 551-1 du code de justice administrative : " Le président du tribunal administratif, ou le magistrat qu'il délègue, peut être saisi en cas de manquement aux obligations de publicité et de mise en concurrence auxquelles est soumise la passation par les pouvoirs adjudicateurs de contrats administratifs ayant pour objet l'exécution de travaux, la livraison de fournitures ou la prestation de services, avec une contrepartie économique constituée par un prix ou un droit d'exploitation, la délégation d'un service public ou la sélection d'un actionnaire opérateur économique d'une société d'économie mixte à opération unique (...) / Le juge est saisi avant la conclusion du contrat "<br/>
<br/>
              3. Il ressort des pièces du dossier soumis au juge des référés du tribunal administratif de Bastia que, par un avis d'appel public à la concurrence publié au Bulletin officiel des annonces des marchés publics le 14 août 2018, la communauté de communes du bassin de vie de l'Ile-Rousse, devenue la communauté de communes de l'Ile-Rousse-Balagne (CRIRB), a lancé une procédure en vue de concéder le service public de distribution de l'eau potable. La société des eaux de Corse a été informée, par un courriel du 21 mars 2019, du rejet de son offre. Le conseil communautaire de la CRIRB a approuvé, par une délibération du 28 mars 2019, l'attribution du contrat de concession à l'Office d'équipement hydraulique de Corse (OEHC). Par l'ordonnance attaquée, contre laquelle la CRIRB et l'OEHC se pourvoient en cassation, le juge des référés a annulé, à la demande de la société des eaux de Corse, la procédure de passation litigieuse.<br/>
<br/>
              4. Aux termes de l'article 27 du décret du 1er février 2016 relatif aux contrats de concession, alors applicable : " I. - Pour attribuer le contrat de concession, l'autorité concédante se fonde, conformément aux dispositions de l'article 47 de l'ordonnance du 29 janvier 2016 susvisée, sur une pluralité de critères non discriminatoires. (...) / Les critères et leur description sont indiqués dans l'avis de concession, dans l'invitation à présenter une offre ou dans tout autre document de la consultation. / II. - Pour les contrats de concession qui relèvent du 1° de l'article 9, l'autorité concédante fixe les critères d'attribution par ordre décroissant d'importance. Leur hiérarchisation est indiquée dans l'avis de concession, dans l'invitation à présenter une offre ou dans tout autre document de la consultation ". Aux termes de l'article 9 du même décret : " Les contrats de concession sont passés dans le respect des règles procédurales communes prévues par le présent chapitre. / Le présent chapitre fixe également les règles de passation particulières respectivement applicables : / 1° Aux contrats dont la valeur estimée hors taxe est égale ou supérieure au seuil européen publié au Journal officiel de la République française ; 2° Aux contrats définis à l'article 10 ". L'article 10 du même décret dispose que : " Les contrats de concession mentionnés au 2° de l'article 9 sont les contrats suivants : (...) / 2° Les contrats de concession qui ont, quelle que soit leur valeur estimée, pour objet : / a) Les activités relevant du 3° du I de l'article 11 de l'ordonnance du 29 janvier 2016 susvisée ". Aux termes de l'article 11 de l'ordonnance du 29 janvier 2016 relative aux contrats de concession, alors applicable : " I. - Sont des activités d'opérateur de réseaux au sens de la présente ordonnance : / (...) / 3° La mise à disposition, l'exploitation ou l'alimentation de réseaux fixes destinés à fournir un service au public dans le domaine de la production, du transport ou de la distribution d'eau potable ".<br/>
<br/>
              5. En renvoyant aux activités mentionnées au 3° du I de l'article 11 de l'ordonnance du 29 janvier 2016, l'article 10 du décret du 1er février 2016 s'est fondé sur le critère matériel de l'objet du contrat pour exclure l'application des règles de passation particulières applicables aux contrats dont la valeur estimée hors taxe est égale ou supérieure au seuil européen, au nombre desquelles figure l'obligation pour l'autorité concédante, prévue au II de l'article 27 du décret, de fixer les critères d'attribution du contrat par ordre décroissant d'importance, aux contrats relatifs à la mise à disposition, à l'exploitation ou à l'alimentation de réseaux fixes destinés à fournir un service au public dans le domaine de la production, du transport ou de la distribution d'eau potable, quelle que soit leur valeur estimée et qu'ils soient conclus par un pouvoir adjudicateur ou une entité adjudicatrice.<br/>
<br/>
              6. Il ressort des pièces du dossier soumis au juge des référés du tribunal administratif de Bastia que le contrat de concession du service public de distribution d'eau potable de la CCIRB a pour objet l'exploitation des installations de production, de stockage et de distribution d'eau potable en vue assurer la fourniture d'eau aux usagers du service. Un contrat ayant un tel objet est au nombre de ceux que vise le 2° de l'article 10 du décret du 1er février 2016 précité. Il résulte dès lors de ce qui a été dit au point précédent que le juge des référés a commis une erreur de droit en jugeant qu'eu égard à la valeur estimée du contrat, la CCIRB était tenue de procéder à une hiérarchisation des critères d'attribution des offres et d'indiquer cette hiérarchie dans l'avis de concession, dans l'invitation à présenter une offre ou dans tout autre document de la consultation. Ainsi, sans qu'il soit besoin d'examiner les autres moyens des pourvois, la CCIRB et l'OEHC sont fondés à demander l'annulation de l'ordonnance qu'ils attaquent.<br/>
<br/>
              7. Dans les circonstances de l'espèce, il y a lieu, en application des dispositions de l'article L. 821-2 du code de justice administrative, de régler l'affaire au titre de la procédure de référé engagée.<br/>
<br/>
              8. En premier lieu, il appartient au juge du référé précontractuel, saisi de moyens sur ce point, de s'assurer que l'appréciation portée par le pouvoir adjudicateur pour exclure ou admettre une candidature ne caractérise pas un manquement aux obligations de publicité et de mise en concurrence. Dans ce cadre, lorsque le candidat est une personne morale de droit public, il lui incombe de vérifier que l'exécution du contrat en cause entrerait dans le champ de sa compétence et, s'il s'agit d'un établissement public, ne méconnaîtrait pas le principe de spécialité auquel il est tenu. <br/>
<br/>
              9. Aux termes de l'article L. 112-32 du code rural, l'Office d'équipement hydraulique de la Corse est autorisé, à la demande des collectivités locales, à " étudier, réaliser ou exploiter les équipements nécessaires à la distribution d'eau potable ainsi qu'au traitement des eaux usées ". L'article R. 112-34 du même code dispose que : " L'office peut intervenir en tant que : ... b) concessionnaire ou exploitant pour le compte des collectivités territoriales ". Il résulte de ces dispositions que l'exploitation de réseaux de distribution pour le compte des collectivités territoriales, sur l'ensemble du territoire de la Corse, est au nombre des missions qui relèvent de la spécialité de l'Office d'équipement hydraulique de Corse. Le moyen tiré de la méconnaissance du principe de spécialité des établissements publics doit dès lors être écarté et la société requérante ne saurait utilement soutenir que l'intervention de l'Office ne serait justifiée par aucun intérêt public local.<br/>
<br/>
              10. En deuxième lieu, il résulte de ce qui a été dit aux points 5 et 6 ci-dessus que le moyen tiré de ce que la procédure de passation du contrat de concession serait irrégulière, faute pour la CCIRB d'avoir procédé à la hiérarchisation des critères d'attribution des offres et d'avoir fait connaître cette hiérarchie, doit être écarté.<br/>
<br/>
              11. En troisième lieu, lorsqu'une personne publique est candidate à l'attribution d'un contrat de concession, il appartient à l'autorité concédante, dès lors que l'équilibre économique de l'offre de cette personne publique diffère substantiellement de celui des offres des autres candidats, de s'assurer, en demandant la production des documents nécessaires, que l'ensemble des coûts directs et indirects a été pris en compte pour la détermination de cette offre, afin que ne soient pas faussées les conditions de la concurrence. Il incombe au juge du référé précontractuel, saisi d'un moyen en ce sens, de vérifier que le contrat n'a pas été attribué à une personne publique qui a présenté une offre qui, faute de prendre en compte l'ensemble des coûts exposés, a faussé les conditions de la concurrence. <br/>
<br/>
              12. Il résulte de ce qui précède que la société des eaux de Corse ne saurait utilement soutenir devant le juge du référé précontractuel que la CCIRB aurait dû s'assurer, en demandant la production des documents nécessaires, que l'offre de l'OEHC n'avait pas faussé la concurrence en omettant de prendre en compte l'ensemble des coûts et en profitant des ressources et moyens qui lui sont attribués au titre de sa mission de service public. Elle ne saurait davantage soutenir utilement que l'OEHC n'opère aucune séparation comptable entre les moyens et ressources qui lui sont attribués au titre de sa mission de service public et ceux qu'il utilise pour l'exécution du contrat de concession du service public de distribution d'eau. Par ailleurs, il résulte de l'instruction que l'équilibre économique de l'offre présentée par l'OEHC ne diffère pas substantiellement de celui de l'offre concurrente présentée par la société des eaux de Corse. Il en résulte qu'en retenant son offre, la CCIRB ne saurait être regardée comme ayant retenu une offre qui aurait, pour les raisons mentionnées ci-dessus, faussé les conditions de la concurrence et comme ayant, pour ce motif, méconnu ses obligations de publicité et de mise en concurrence.   <br/>
<br/>
              13. En dernier lieu, la société des eaux de Corse soutient que la CCIRB aurait méconnu le principe d'égalité de traitement entre les candidats, d'une part en prévoyant, dans le dossier de consultation, que l'attributaire achèterait l'intégralité de l'eau potable à l'OEHC, alors que ce dernier était candidat à l'attribution de la concession, d'autre part, en appréciant les offres au regard de leur capacité à maîtriser les pertes d'eau, alors que l'OEHC n'est pas placé, sur ce point, dans la même situation que les autres candidats du fait de l'imbrication du réseau de transport de l'OEHC et du réseau de distribution de la CCIRB. Cependant, d'une part la clause relative à l'obligation d'achat de l'eau auprès de l'OEHC n'est pas de nature, dès lors que chaque candidat doit prendre en compte, dans son offre, le même prix d'achat de l'eau, à altérer les conditions de mise en concurrence entre les candidats. D'autre part, il résulte de l'instruction que la société des eaux de Corse, actuel concessionnaire du service public de l'eau dans le secteur concerné, est, autant que l'OEHC, à même de prendre en compte les éventuelles pertes du réseau et d'adapter son offre en conséquence. Il suit de là que le moyen tiré d'une rupture d'égalité entre les candidats ne peut qu'être écarté.<br/>
<br/>
              14. Il résulte de tout ce qui précède que la demande présentée par la société des eaux de Corse devant le juge des référés du tribunal administratif de Bastia doit être rejetée.<br/>
<br/>
              15. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mis à la charge de la CCIRB et de l'OEHC, qui ne sont pas les parties perdantes dans la présente instance, le versement des sommes demandées par la société des eaux de Corse au titre de ces dispositions. Il y a lieu, en revanche, dans les circonstances de l'espèce, de mettre à la charge de la société des eaux de Corse, au titre des mêmes dispositions, le versement à la CCIRB et à l'OEHC d'une somme de 4 000 euros chacun pour la procédure suivie devant le Conseil d'Etat et devant le juge des référés du tribunal administratif de Bastia.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du juge des référés du tribunal administratif de Bastia du 18 avril 2019 est annulée. <br/>
Article 2 : La demande de la société des eaux de Corse devant le juge des référés du tribunal administratif de Bastia et ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 3 : La société des eaux de Corse versera à la communauté de communes de l'Ile-Rousse-Balagne, d'une part, et à l'Office d'équipement hydraulique de Corse, d'autre part, une somme de 4 000 euros, au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la communauté de communes de l'Ile-Rousse-Balagne, à l'Office d'équipement hydraulique de Corse et à la société des eaux de Corse. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">14-01-01 COMMERCE, INDUSTRIE, INTERVENTION ÉCONOMIQUE DE LA PUISSANCE PUBLIQUE. PRINCIPES GÉNÉRAUX. LIBERTÉ DU COMMERCE ET DE L'INDUSTRIE. - CANDIDATURE D'UNE PERSONNE PUBLIQUE À UN CONTRAT DE CONCESSION - 1) MODALITÉS DE CETTE CANDIDATURE - A) RESPECT DU DROIT DE LA CONCURRENCE [RJ1], EN PARTICULIER S'AGISSANT DE L'ÉQUILIBRE ÉCONOMIQUE DE L'OFFRE [RJ2] - A) OBLIGATIONS INCOMBANT À CE TITRE AU POUVOIR ADJUDICATEUR [RJ3] - B) CONTRÔLE DU JUGE DU RÉFÉRÉ-PRÉCONTRACTUEL - 2) CANDIDATURE D'UN ÉTABLISSEMENT PUBLIC - CONDITION D'EXISTENCE D'UN INTÉRÊT PUBLIC LOCAL [RJ4] - ABSENCE [RJ5].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">39-02-005 MARCHÉS ET CONTRATS ADMINISTRATIFS. FORMATION DES CONTRATS ET MARCHÉS. FORMALITÉS DE PUBLICITÉ ET DE MISE EN CONCURRENCE. - CONTRATS DE CONCESSION - OBLIGATION DE HIÉRARCHISATION POUR LES CONTRATS DONT LA VALEUR EST SUPÉRIEURE AU SEUIL EUROPÉEN (ART. 10 DU DÉCRET DU 1ER FÉVRIER 2016) - EXCEPTION POUR LES CONTRATS AYANT POUR OBJET UNE ACTIVITÉ MENTIONNÉE AU 3° DU I DE L'ARTICLE 11 DE L'ORDONNANCE DU 29 JANVIER 2016 - EXCEPTION APPLICABLE AUX ENTITÉS ADJUDICATRICES ET AUX POUVOIRS ADJUDICATEURS.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">39-02-01 MARCHÉS ET CONTRATS ADMINISTRATIFS. FORMATION DES CONTRATS ET MARCHÉS. QUALITÉ POUR CONTRACTER. - CANDIDATURE D'UNE PERSONNE PUBLIQUE À UN CONTRAT DE CONCESSION - 1) MODALITÉS DE CETTE CANDIDATURE - A) RESPECT DU DROIT DE LA CONCURRENCE [RJ1], EN PARTICULIER S'AGISSANT DE L'ÉQUILIBRE ÉCONOMIQUE DE L'OFFRE [RJ2] - A) OBLIGATIONS INCOMBANT À CE TITRE AU POUVOIR ADJUDICATEUR [RJ3] - B) CONTRÔLE DU JUGE DU RÉFÉRÉ-PRÉCONTRACTUEL - 2) CANDIDATURE D'UN ÉTABLISSEMENT PUBLIC - CONDITION D'EXISTENCE D'UN INTÉRÊT PUBLIC LOCAL [RJ4] - ABSENCE [RJ5].
</SCT>
<ANA ID="9A"> 14-01-01 1) a) Lorsqu'une personne publique est candidate à l'attribution d'un contrat de concession, il appartient à l'autorité concédante, dès lors que l'équilibre économique de l'offre de cette personne publique diffère substantiellement de celui des offres des autres candidats, de s'assurer, en demandant la production des documents nécessaires, que l'ensemble des coûts directs et indirects a été pris en compte pour la détermination de cette offre, afin que ne soient pas faussées les conditions de la concurrence.... ,,b) Il incombe au juge du référé précontractuel, saisi d'un moyen en ce sens, de vérifier que le contrat n'a pas été attribué à une personne publique qui a présenté une offre qui, faute de prendre en compte l'ensemble des coûts exposés, a faussé les conditions de la concurrence.,,,2) La candidature d'un établissement public à un contrat de concession n'est pas soumise à la condition de l'existence d'un intérêt public local. Le moyen tiré de ce que cette candidature n'est pas justifiée par un tel intérêt est donc inopérant.</ANA>
<ANA ID="9B"> 39-02-005 En renvoyant aux activités mentionnées au 3° du I de l'article 11 de l'ordonnance n° 2016-65 du 29 janvier 2016, l'article 10 du décret n° 2016-86 du 1er février 2016 s'est fondé sur le critère matériel de l'objet du contrat pour exclure l'application des règles de passation particulières applicables aux contrats dont la valeur estimée hors taxe est égale ou supérieure au seuil européen, au nombre desquelles figure l'obligation pour l'autorité concédante, prévue au II de l'article 27 du décret, de fixer les critères d'attribution du contrat par ordre décroissant d'importance, aux contrats relatifs à la mise à disposition, à l'exploitation ou à l'alimentation de réseaux fixes destinés à fournir un service au public dans le domaine de la production, du transport ou de la distribution d'eau potable, quelle que soit leur valeur estimée et qu'ils soient conclus par un pouvoir adjudicateur ou une entité adjudicatrice.</ANA>
<ANA ID="9C"> 39-02-01 1) a) Lorsqu'une personne publique est candidate à l'attribution d'un contrat de concession, il appartient à l'autorité concédante, dès lors que l'équilibre économique de l'offre de cette personne publique diffère substantiellement de celui des offres des autres candidats, de s'assurer, en demandant la production des documents nécessaires, que l'ensemble des coûts directs et indirects a été pris en compte pour la détermination de cette offre, afin que ne soient pas faussées les conditions de la concurrence.... ,,b) Il incombe au juge du référé précontractuel, saisi d'un moyen en ce sens, de vérifier que le contrat n'a pas été attribué à une personne publique qui a présenté une offre qui, faute de prendre en compte l'ensemble des coûts exposés, a faussé les conditions de la concurrence.,,,2) La candidature d'un établissement public à un contrat de concession n'est pas soumise à la condition de l'existence d'un intérêt public local. Le moyen tiré de ce que cette candidature n'est pas justifiée par un tel intérêt est donc inopérant.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, avis, 8 novembre 2000, Société Jean-Louis Bernard Consultants, p. 492.,,[RJ2] Rappr., sur la notion d'équilibre économique du contrat de concession, CE, Section, 29 juin 2018, Ministre de l'intérieur c/ Communauté de communes de la vallée de l'Ubaye, n° 402251, p. 285.,,[RJ3] Cf. CE, 14 juin 2019, Société Vinci construction maritime et fluvial, n° 411444, à publier au Recueil.,,[RJ4] Rappr., sur l'existence de cette condition s'agissant des collectivités territoriales et EPCI, CE, Assemblée, 20 décembre 2014, Société Armor SNC, n° 355563, p. 433 ;, ,[RJ5] Rappr., sur l'obligation, pour l'établissement public candidat, de respecter en revanche le principe de spécialité, CE, 18 septembre 2015, Association de gestion du conservatoire national des arts et métiers des pays de la Loire et autres, n° 390041, T. pp. 757-800.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
