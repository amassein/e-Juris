<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000023109964</ID>
<ANCIEN_ID>JG_L_2010_11_000000320827</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/23/10/99/CETATEXT000023109964.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 17/11/2010, 320827</TITRE>
<DATE_DEC>2010-11-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>320827</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Laure  Bédier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Vialettes Maud</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2010:320827.20101117</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 19 septembre 2008 au secrétariat du contentieux du Conseil d'Etat, présentée par la SOCIETE ARTHUS CONSULTING, dont le siège est 16, rue Albert Einstein, Champs-sur-Marne à Marne-la-Vallée (77420), représentée par son président-directeur général en exercice ; la SOCIETE ARTHUS CONSULTING demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision par laquelle la Haute autorité de santé a implicitement rejeté son recours gracieux dirigé contre la décision du 2 avril 2008 refusant de prolonger son agrément pour l'évaluation des pratiques professionnelles, ainsi que cette décision ;<br/>
<br/>
              2°) d'enjoindre à la Haute autorité de santé de prolonger l'agrément pour une période de cinq ans à compter de la notification de l'annulation de la décision de refus de prolongation d'agrément ;<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu le code de la sécurité sociale ;<br/>
<br/>
              Vu le code de la santé publique ; <br/>
<br/>
              Vu la décision du 7 novembre 2007 de la Haute autorité de santé relative aux modalités de mise en oeuvre de l'évaluation des pratiques professionnelles ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Laure Bédier, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant qu'aux termes de l'article L. 4133-1-1 du code de la santé publique, dans sa rédaction alors en vigueur : " L'évaluation individuelle des pratiques professionnelles constitue une obligation pour les médecins exerçant à titre libéral, les médecins salariés non hospitaliers ainsi que pour les médecins mentionnés à l'article L. 6155-1 et les médecins exerçant dans les établissements de santé privés. / Il est satisfait à cette obligation par la participation du médecin à un des dispositifs prévus par l'article L. 4134-5 ou à un des dispositifs agréés dans des conditions fixées par décret (...) " ; qu'ainsi, l'article D. 4133-29 du même code, pris pour l'application de ces dispositions, n'a pu, sans les méconnaître, renvoyer purement et simplement au règlement intérieur de la Haute autorité de santé le soin de définir les conditions et la durée de l'agrément des organismes concourant à l'évaluation des pratiques professionnelles ; que, s'il est soutenu que la compétence de la Haute autorité de santé trouve sa base légale dans l'article L. 161-37 du code de la sécurité sociale qui, dans sa rédaction applicable en l'espèce, dispose que la Haute autorité de santé " établit et met en oeuvre des procédures d'évaluation des pratiques professionnelles ", cette disposition générale doit être combinée avec l'article L. 161-46 du même code, qui renvoie à un décret en Conseil d'Etat le soin de fixer les conditions dans lesquelles la Haute autorité de santé procède aux évaluations mentionnées à l'article L. 161-37, et avec l'article R. 161-73, pris pour l'application de l'article L. 161-46, qui définit précisément le rôle de la Haute autorité de santé dans le domaine de l'évaluation des pratiques professionnelles mais ne mentionne pas la détermination des conditions d'agrément des organismes concourant à cette évaluation ; qu'ainsi, l'article L. 161-37 du code de la sécurité sociale ne peut être interprété comme attribuant une compétence réglementaire à la Haute autorité de santé pour définir les conditions d'agrément des organismes participant à l'évaluation des pratiques professionnelles ;<br/>
<br/>
              Considérant qu'il résulte de ce qui précède que la décision du 7 novembre 2007 de la Haute autorité de santé n'a pu compétemment fixer les conditions d'agrément des organismes concourant à l'évaluation des pratiques professionnelles dans le cadre du dispositif prévu par l'article L. 4133-1-1 du code de la santé publique ; que, dès lors, la décision du 2 avril 2008 de cette autorité refusant de prolonger l'agrément de la SOCIETE ARTHUS CONSULTING pour l'évaluation des pratiques professionnelles et la décision implicite de rejet du recours gracieux de cette société contre la décision du 2 avril 2008, prises sur le fondement de la décision du 7 novembre 2007, sont entachées d'illégalité ; qu'ainsi, et sans qu'il soit besoin d'examiner les moyens de la requête qui, contrairement à ce que soutient la Haute autorité de santé, est suffisamment motivée, la SOCIETE ARTHUS CONSULTING est fondée à en demander l'annulation ; qu'il y a lieu, en revanche, de rejeter sa demande d'injonction tendant à la prolongation de son agrément, l'annulation des décisions litigieuses ne conduisant pas nécessairement à une telle prolongation ; <br/>
<br/>
              Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mis à la charge de la SOCIETE ARTHUS CONSULTING, qui n'est pas, dans la présente instance, la partie perdante, le versement d'une somme au titre des frais exposés par la Haute autorité de santé et non compris dans les dépens ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La décision de la Haute autorité de santé du 2 avril 2008 et la décision de rejet implicite du recours gracieux de la SOCIETE ARTHUS CONSULTING contre cette décision sont annulées.<br/>
Article 2 : Le surplus des conclusions de la requête est rejeté.<br/>
Article 3 : Les conclusions présentées par la Haute autorité de santé en application de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à la SOCIETE ARTHUS CONSULTING et à la Haute autorité de santé.<br/>
Copie en sera adressée pour information au ministre du travail, de l'emploi et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-02-02-02 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - COMPÉTENCE. RÉPARTITION DES COMPÉTENCES ENTRE AUTORITÉS DISPOSANT DU POUVOIR RÉGLEMENTAIRE. MESURES À PRENDRE PAR DÉCRET. - SUBDÉLÉGATION ILLÉGALE DU POUVOIR RÉGLEMENTAIRE - MOYEN D'ORDRE PUBLIC - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-07-01-04-01-02 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. QUESTIONS GÉNÉRALES. MOYENS. MOYENS D'ORDRE PUBLIC À SOULEVER D'OFFICE. EXISTENCE. - MOYEN TIRÉ DE L'ILLÉGALITÉ DE LA SUBDÉLÉGATION SUR LE FONDEMENT DE LAQUELLE UNE DISPOSITION RÉGLEMENTAIRE A ÉTÉ PRISE [RJ1].
</SCT>
<ANA ID="9A"> 01-02-02-02 A l'occasion d'un litige portant sur une disposition réglementaire, le juge soulève d'office le moyen d'ordre public tiré de l'illégalité de la subdélégation à laquelle a procédé un décret sur le fondement duquel la disposition réglementaire litigieuse a été prise, alors que la loi avait chargé le décret d'édicter cette réglementation.</ANA>
<ANA ID="9B"> 54-07-01-04-01-02 A l'occasion d'un litige portant sur une disposition réglementaire, le juge soulève d'office le moyen d'ordre public tiré de l'illégalité de la subdélégation à laquelle a procédé un décret sur le fondement duquel la disposition réglementaire litigieuse a été prise, alors que la loi avait chargé le décret d'édicter cette réglementation.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. 14 janvier 1987, Mme Gosset, n° 59145, T. p. 539.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
