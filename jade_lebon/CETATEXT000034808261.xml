<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034808261</ID>
<ANCIEN_ID>JG_L_2017_05_000000403569</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/80/82/CETATEXT000034808261.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 1ère chambres réunies, 24/05/2017, 403569</TITRE>
<DATE_DEC>2017-05-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>403569</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>M. Cyrille Beaufils</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:403569.20170524</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Investaq Energie et la société Celtique Energie Limited ont demandé au juge des référés du tribunal administratif de Pau, d'une part, de liquider provisoirement l'astreinte prononcée par son ordonnance n° 1502489 du 21 décembre 2015, pour la période ayant couru entre le 22 février 2016 et la date de l'ordonnance à intervenir et, d'autre part, de condamner l'Etat à leur verser la totalité des sommes dues au titre de la liquidation provisoire. Par une ordonnance n° 1601270 du 1er septembre 2016, le président du tribunal administratif de Pau a, sur le fondement de l'article R. 222-1 du code de justice administrative, constaté qu'il n'y avait pas lieu de liquider l'astreinte prononcée à l'encontre de l'Etat. <br/>
<br/>
              Par un pourvoi et un mémoire en réplique, enregistrés au secrétariat du contentieux du Conseil d'État les 16 septembre 2016 et 2 mai 2017, la société Investaq Energie et la société Celtique Energie Limited demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, d'une part, de liquider l'astreinte prononcée par l'ordonnance du 21 décembre 2015 du juge des référés du tribunal administratif de Pau pour la période ayant couru entre le 22 février 2016 et le 15 août 2016 ou, subsidiairement, entre le 22 février 2016 et le 15 juillet 2016 ou, plus subsidiairement encore, entre le 22 février 2016 et le 7 juillet 2016 et, d'autre part, de condamner l'Etat à leur payer la totalité de la somme due au titre de la liquidation de l'astreinte ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative.<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Cyrille Beaufils, auditeur,  <br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Foussard, Froger, avocat de la société Investaq Energie et autre.<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que, par une ordonnance du 21 décembre 2015, le juge des référés du tribunal administratif de Pau, saisi sur le fondement de l'article L. 521-1 du code de justice administrative, a suspendu l'exécution des décisions implicites par lesquelles le ministre chargé de l'écologie, du développement durable et de l'énergie a rejeté les demandes présentées le 28 juillet 2014 par les sociétés requérantes tendant à obtenir, à titre principal, la prolongation exceptionnelle du permis de recherches de mines d'hydrocarbures liquides ou gazeux, dit " permis de Claracq ", et, à titre subsidiaire, son renouvellement pour une troisième période ; que, par la même ordonnance, le juge des référés a enjoint au ministre de procéder au réexamen des deux demandes de prolongation dans un délai de deux mois à compter de la notification de 1'ordonnance, sous astreinte de 1 000 euros par jour de retard ; que la société Investaq Energie et autre se pourvoient en cassation contre l'ordonnance du 1er septembre 2016 par laquelle le président du tribunal administratif de Pau, statuant sur le fondement de l'article R. 222-1 du code de justice administrative, a dit n'y avoir pas lieu à statuer sur leur demande de liquidation de cette astreinte ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 911-4 du code de justice administrative : " En cas d'inexécution d'un jugement ou d'un arrêt, la partie intéressée peut demander au tribunal administratif ou à la cour administrative d'appel qui a rendu le jugement d'en assurer l'exécution " ; qu'aux termes de l'article L. 911-7 du même code : " En cas d'inexécution totale ou partielle ou d'exécution tardive, la juridiction procède à la liquidation de l'astreinte qu'elle avait prononcée. Sauf s'il est établi que l'inexécution de la décision provient d'un cas fortuit ou de force majeure, la juridiction ne peut modifier le taux de l'astreinte définitive lors de sa liquidation. Elle peut modérer ou supprimer l'astreinte provisoire, même en cas d'inexécution constatée " ; qu'aux termes de l'article R. 222-1 du même code : " Les présidents de tribunal administratif (...) peuvent, par ordonnance : (... / 3° Constater qu'il n'y a pas lieu de statuer sur une requête ; (...) " ;<br/>
<br/>
              3. Considérant que l'astreinte a pour finalité de contraindre la personne qui s'y refuse à exécuter les obligations qui lui ont été assignées par une décision de justice ; que sa liquidation a pour objet de tirer les conséquences du refus ou du retard mis à exécuter ces obligations ; que, lorsqu'est ordonnée par le juge des référés, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, une mesure provisoire assortie d'une astreinte, l'intervention du jugement au principal, qui met fin à l'obligation d'exécuter cette mesure, prive, pour l'avenir, cette astreinte de base légale ; qu'elle n'a, en revanche, pas pour effet de priver d'objet la demande de liquidation de cette astreinte pour la période comprise entre la fin du délai imparti pour exécuter la mesure ordonnée en référé et la notification à la personne soumise à l'astreinte du jugement rendu dans l'instance engagée au principal, dès lors que la mesure en cause n'a pas été exécutée dans cet intervalle, ou a été exécutée tardivement ; <br/>
<br/>
              4. Considérant, par jugement du 7 juillet 2016 statuant au principal sur le litige ayant donné lieu à l'intervention du juge des référés, le tribunal administratif de Pau a annulé le refus implicite de délivrer aux sociétés un permis prolongeant un précédent permis de recherche, a enjoint au ministre de l'écologie, du développement durable et de l'énergie de délivrer, dans un délai de trente jours, ce permis et assorti cette injonction d'une astreinte de 3 000 euros par jour de retard ; qu'ainsi qu'il a été dit au point 3, l'intervention de ce jugement au principal ne privait pas d'objet la demande de liquidation de l'astreinte prononcée par le juge des référés en vue d'assurer l'exécution la mesure ordonnée en référé ; que par suite, en jugeant qu'il n'y avait plus lieu de statuer sur la demande de liquidation de l'astreinte prononcée par le juge des référés, au motif tiré de l'intervention du jugement au principal, le président du tribunal administratif de Pau, statuant sur le fondement de l'article R. 222-1 du code de justice administrative, a entaché son ordonnance d'erreur de droit ; que, sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, les sociétés requérantes sont fondées à en demander l'annulation ; <br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au titre de la procédure engagée par la société Investaq Energie et autre, en application des dispositions de l'article L. 821-2 du code de justice administrative ; <br/>
<br/>
              6. Considérant, d'une part, qu'il résulte de ce qui a été dit au point 4 que la notification au ministre du jugement du 7 juillet 2016 rendu dans l'instance engagée au principal, a privé de base légale l'injonction provisoire qu'avait ordonnée le juge des référés et à l'astreinte dont elle était assortie ; que cette astreinte a cessé de produire ses effets à compter de cette date ; qu'il résulte, d'autre part, de l'instruction que l'administration ne justifiait pas, à cette date, avoir exécuté cette ordonnance du juge des référés, notifiée au ministre chargé de l'environnement le 21 décembre 2015, en réexaminant dans un délai de deux mois les demandes des sociétés requérantes ; qu'il y a lieu, par suite, de procéder à la liquidation définitive de l'astreinte pour la période courant du 22 février 2015, date de fin du délai imparti pour exécuter la mesure ordonnée par le juge des référés, au 15 juillet 2016, date de la notification au ministre chargé de l'environnement du jugement rendu dans l'instance engagée au principal ; que, toutefois, il y a lieu, en application des dispositions précitées de l'article L. 911-7 du code de justice administrative, de modérer l'astreinte initialement prononcée et de fixer le montant de la somme due par l'Etat aux sociétés requérantes à 30 000 euros chacune ;<br/>
<br/>
              7. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 1 000 euros à verser à chacune des sociétés requérantes au titre des dispositions de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du 1er septembre 2016 du président du tribunal administratif de Pau est annulée. <br/>
<br/>
Article 2 : L'Etat est condamné à verser une somme de 30 000 euros chacune à la société Investaq Energie et à la société Celtique Energie Limited.<br/>
<br/>
Article 3 : L'Etat versera à la société Investaq Energie et à la société Celtique Energie Limited la somme de 1 000 euros chacune au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : Le surplus des conclusions du pourvoi de la société Investaq Energie et autre est rejeté. <br/>
<br/>
Article 5 : La présente décision sera notifiée à la société Investaq Energie, première requérante dénommée et au ministre d'Etat, ministre de la transition écologique et solidaire. <br/>
Copie en sera adressée au ministre de l'économie et au parquet général près la Cour des comptes, à qui sera également adressée copie de l'ordonnance du 21 décembre 2015 du juge des référés du tribunal administratif de Pau.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-035-02-04 PROCÉDURE. PROCÉDURES INSTITUÉES PAR LA LOI DU 30 JUIN 2000. RÉFÉRÉ SUSPENSION (ART. L. 521-1 DU CODE DE JUSTICE ADMINISTRATIVE). POUVOIRS ET DEVOIRS DU JUGE. - 1) MESURE PROVISOIRE ORDONNÉE PAR LE JUGE DU RÉFÉRÉ-SUSPENSION ASSORTIE D'UNE ASTREINTE - INTERVENTION DU JUGEMENT AU PRINCIPAL - EFFET - A) POUR L'AVENIR - PERTE DE BASE LÉGALE - EXISTENCE - B) POUR LA PÉRIODE COMPRISE ENTRE LA FIN DU DÉLAI IMPARTI POUR EXÉCUTER LA MESURE ORDONNÉE EN RÉFÉRÉ ET LA NOTIFICATION À LA PERSONNE SOUMISE À L'ASTREINTE DU JUGEMENT AU PRINCIPAL - PERTE DE BASE LÉGALE - ABSENCE - CONSÉQUENCE - NON-LIEU SUR LA DEMANDE DE LIQUIDATION - ABSENCE - 2) ESPÈCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-06-07-01 PROCÉDURE. JUGEMENTS. EXÉCUTION DES JUGEMENTS. ASTREINTE. - 1) MESURE PROVISOIRE ORDONNÉE PAR LE JUGE DU RÉFÉRÉ-SUSPENSION ASSORTIE D'UNE ASTREINTE - INTERVENTION DU JUGEMENT AU PRINCIPAL - EFFET - A) POUR L'AVENIR - PERTE DE BASE LÉGALE - EXISTENCE - B) POUR LA PÉRIODE COMPRISE ENTRE LA FIN DU DÉLAI IMPARTI POUR EXÉCUTER LA MESURE ORDONNÉE EN RÉFÉRÉ ET LA NOTIFICATION À LA PERSONNE SOUMISE À L'ASTREINTE DU JUGEMENT AU PRINCIPAL - PERTE DE BASE LÉGALE - ABSENCE - CONSÉQUENCE - NON-LIEU SUR LA DEMANDE DE LIQUIDATION - ABSENCE - 2) ESPÈCE.
</SCT>
<ANA ID="9A"> 54-035-02-04 1) L'astreinte a pour finalité de contraindre la personne qui s'y refuse à exécuter les obligations qui lui ont été assignées par une décision de justice. Sa liquidation a pour objet de tirer les conséquences du refus ou du retard mis à exécuter ces obligations.... ,,a) Lorsqu'est ordonnée par le juge des référés, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, une mesure provisoire assortie d'une astreinte, l'intervention du jugement au principal, qui met fin à l'obligation d'exécuter cette mesure, prive, pour l'avenir, cette astreinte de base légale.... ,,b) Elle n'a, en revanche, pas pour effet de priver d'objet la demande de liquidation de cette astreinte pour la période comprise entre la fin du délai imparti pour exécuter la mesure ordonnée en référé et la notification à la personne soumise à l'astreinte du jugement rendu dans l'instance engagée au principal, dès lors que la mesure en cause n'a pas été exécutée dans cet intervalle, ou a été exécutée tardivement.... ,,2) Erreur de droit du tribunal administratif à avoir jugé qu'il n'y avait plus lieu de statuer sur la demande de liquidation de l'astreinte prononcée par le juge des référés au motif tiré de l'intervention du jugement au principal. La notification au ministre du jugement rendu dans l'instance au principal a privé, pour l'avenir, de base légale l'injonction provisoire ordonnée par le juge des référés et l'astreinte dont elle était assortie. En revanche, l'administration ne justifiant pas, à cette date, avoir exécuté l'ordonnance du juge des référés, il y a lieu de procéder à la liquidation de l'astreinte pour la période courant de la date de fin du délai imparti pour exécuter la mesure ordonnée par le juge des référés et la date de la notification au ministre du jugement rendu dans l'instance engagée au principal.</ANA>
<ANA ID="9B"> 54-06-07-01 1) L'astreinte a pour finalité de contraindre la personne qui s'y refuse à exécuter les obligations qui lui ont été assignées par une décision de justice. Sa liquidation a pour objet de tirer les conséquences du refus ou du retard mis à exécuter ces obligations.... ,,a) Lorsqu'est ordonnée par le juge des référés, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, une mesure provisoire assortie d'une astreinte, l'intervention du jugement au principal, qui met fin à l'obligation d'exécuter cette mesure, prive, pour l'avenir, cette astreinte de base légale.... ,,b) Elle n'a, en revanche, pas pour effet de priver d'objet la demande de liquidation de cette astreinte pour la période comprise entre la fin du délai imparti pour exécuter la mesure ordonnée en référé et la notification à la personne soumise à l'astreinte du jugement rendu dans l'instance engagée au principal, dès lors que la mesure en cause n'a pas été exécutée dans cet intervalle, ou a été exécutée tardivement.... ,,2) Erreur de droit du tribunal administratif à avoir jugé qu'il n'y avait plus lieu de statuer sur la demande de liquidation de l'astreinte prononcée par le juge des référés au motif tiré de l'intervention du jugement au principal. La notification au ministre du jugement rendu dans l'instance au principal a privé, pour l'avenir, de base légale l'injonction provisoire ordonnée par le juge des référés et l'astreinte dont elle était assortie. En revanche, l'administration ne justifiant pas, à cette date, avoir exécuté l'ordonnance du juge des référés, il y a lieu de procéder à la liquidation de l'astreinte pour la période courant de la date de fin du délai imparti pour exécuter la mesure ordonnée par le juge des référés et la date de la notification au ministre du jugement rendu dans l'instance engagée au principal.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
