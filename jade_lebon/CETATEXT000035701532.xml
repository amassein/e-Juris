<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035701532</ID>
<ANCIEN_ID>JG_L_2017_10_000000399578</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/70/15/CETATEXT000035701532.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 6ème chambres réunies, 02/10/2017, 399578, Publié au recueil Lebon</TITRE>
<DATE_DEC>2017-10-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>399578</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>DELAMARRE ; SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>Mme Sabine Monchambert</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Charles Touboul</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:399578.20171002</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A...B...a demandé au tribunal administratif de Cergy-Pontoise d'annuler la décision du 16 octobre 2014 par laquelle le président du conseil général des Hauts-de-Seine a rejeté son recours préalable contre la décision du 25 juin 2014 par laquelle la caisse d'allocations familiales des Hauts-de-Seine a mis fin à son droit au revenu de solidarité active. Par un jugement n° 1410452 du 17 février 2016, le tribunal administratif de Cergy-Pontoise a rejeté sa demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 6 mai et 29 juillet 2016 au secrétariat du contentieux du Conseil d'Etat, Mme B...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler le jugement du tribunal administratif de Cergy-Pontoise du 17 février 2016 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge du département des Hauts-de-Seine la somme de 3 000 euros à verser à son avocat, Me Delamarre, au titre de l'article L. 761-1 du code de justice administrative et de l'article 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'action sociale et des familles ; <br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sabine Monchambert, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Charles Touboul, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Delamarre, avocat de Mme B...et à la SCP Piwnica, Molinié, avocat du département des Hauts-de-Seine.<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que la caisse d'allocations familiales des Hauts-de-Seine a, par un courrier du 25 juin 2014, notifié à Mme B... la décision de mettre fin à son droit au revenu de solidarité active. Par le jugement attaqué, le tribunal administratif de Cergy-Pontoise a rejeté la requête de l'intéressée tendant à l'annulation de la décision du 16 octobre 2014 par laquelle le président du conseil général des Hauts-de-Seine a rejeté son recours administratif obligatoire contre la décision de la caisse d'allocations familiales et sa demande de rétablissement dans ses droits à compter de juin 2014.<br/>
<br/>
              2. Aux termes de l'article R. 772-5 du code de justice administrative : " Sont présentées, instruites et jugées selon les dispositions du présent code, sous réserve des dispositions du présent chapitre, les requêtes relatives aux prestations, allocations ou droits attribués au titre de l'aide ou de l'action sociale, du logement ou en faveur des travailleurs privés d'emploi (...) ". Aux termes de l'article R. 772-9 du même code : " La procédure contradictoire peut être poursuivie à l'audience sur les éléments de fait qui conditionnent l'attribution de la prestation ou de l'allocation ou la reconnaissance du droit, objet de la requête. / L'instruction est close soit après que les parties ou leurs mandataires ont formulé leurs observations orales, soit, si ces parties sont absentes ou ne sont pas représentées, après appel de leur affaire à l'audience. Toutefois, afin de permettre aux parties de verser des pièces complémentaires, le juge peut décider de différer la clôture de l'instruction à une date postérieure dont il les avise par tous moyens. / L'instruction fait l'objet d'une réouverture en cas de renvoi à une autre audience ".<br/>
<br/>
              3. L'article R. 772-9 du code de justice administrative, qui déroge aux règles de droit commun de la procédure administrative contentieuse, tend, eu égard aux spécificités de l'office du juge en matière de contentieux sociaux, à assouplir les contraintes de la procédure écrite en ouvrant la possibilité à ce juge de poursuivre à l'audience la procédure contradictoire sur des éléments de fait et en décalant la clôture de l'instruction, laquelle est entièrement régie par les dispositions de son deuxième alinéa. Dès lors, les règles fixées par l'article R. 613-2 du même code, selon lesquelles l'instruction est close à la date fixée par une ordonnance de clôture ou, à défaut, trois jours francs avant la date de l'audience, ne sont pas applicables aux contentieux sociaux régis par les articles R. 772-5 et suivants du code de justice administrative. Il suit de là que le tribunal doit, pour juger les requêtes régies par ces articles, prendre en considération tant les éléments de fait invoqués oralement à l'audience qui conditionnent l'attribution de la prestation ou de l'allocation ou la reconnaissance du droit, objet de la requête, que tous les mémoires enregistrés jusqu'à la clôture de l'instruction, qui intervient, sous réserve de la décision du juge de la différer, après que les parties ou leurs mandataires ont formulé leurs observations orales ou, si ces parties sont absentes ou ne sont pas représentées, après appel de leur affaire à l'audience.<br/>
<br/>
              4. Il ressort des mentions du jugement attaqué que le premier juge a visé le mémoire en réplique présenté par Mme B...le 3 février 2016, soit la veille de l'audience, sans l'analyser, en précisant qu'il avait été enregistré " postérieurement à la clôture d'instruction ". Par suite, la requérante est fondée à soutenir qu'en refusant de prendre en compte ce mémoire qui, quelle que soit leur incidence sur l'issue du litige, contenait des moyens nouveaux, le premier juge a méconnu les dispositions de l'article R. 772-9 du code de justice administrative et a, ainsi, entaché son jugement d'irrégularité.<br/>
<br/>
              5. Il résulte de ce qui précède que le jugement du tribunal administratif de Cergy-Pontoise du 17 février 2016 doit être annulé.<br/>
<br/>
              6. Mme B...a obtenu le bénéfice de l'aide juridictionnelle. Par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. Il y a lieu, dans les circonstances de l'espèce, et sous réserve que Me Delamarre, avocat de MmeB..., renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge du département des Hauts-de-Seine une somme de 2 000 euros à verser à Me Delamarre.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Cergy-Pontoise du 17 février 2016 est annulé. <br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Cergy-Pontoise.<br/>
Article 3 : Le département des Hauts-de-Seine versera à Me Delamarre, avocat de Mme B..., une somme de 2 000 euros en application des dispositions du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991, sous réserve qu'il renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
Article 4 : La présente décision sera notifiée à Mme A...B...et au département des Hauts-de-Seine. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">04-04 AIDE SOCIALE. CONTENTIEUX DE L'AIDE SOCIALE ET DE LA TARIFICATION. - CLÔTURE DE L'INSTRUCTION (ART. R. 772-9 DU CJA) - APPLICATION DES DISPOSITIONS DE L'ARTICLE R. 613-2 DU CJA AUX CONTENTIEUX SOCIAUX - ABSENCE - CONSÉQUENCE - PRISE EN COMPTE PAR LE JUGE DES ÉLÉMENTS DE FAIT INVOQUÉS À L'ORAL À L'AUDIENCE ET DES MÉMOIRES ENREGISTRÉS JUSQU'À LA CLÔTURE DE L'INSTRUCTION.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-06-01 PROCÉDURE. JUGEMENTS. RÈGLES GÉNÉRALES DE PROCÉDURE. - CLÔTURE DE L'INSTRUCTION (ART. R. 772-9 DU CJA) - APPLICATION DES DISPOSITIONS DE L'ARTICLE R. 613-2 DU CJA AUX CONTENTIEUX SOCIAUX - ABSENCE - CONSÉQUENCE - PRISE EN COMPTE PAR LE JUGE DES ÉLÉMENTS DE FAIT INVOQUÉS À L'ORAL À L'AUDIENCE ET DES MÉMOIRES ENREGISTRÉS JUSQU'À LA CLÔTURE DE L'INSTRUCTION.
</SCT>
<ANA ID="9A"> 04-04 L'article R. 772-9 du code de justice administrative (CJA), qui déroge aux règles de droit commun de la procédure administrative contentieuse, tend, eu égard aux spécificités de l'office du juge en matière de contentieux sociaux, à assouplir les contraintes de la procédure écrite en ouvrant la possibilité à ce juge de poursuivre à l'audience la procédure contradictoire sur des éléments de fait et en décalant la clôture de l'instruction, laquelle est entièrement régie par les dispositions de son deuxième alinéa. Dès lors, les règles fixées par l'article R. 613-2 du même code, selon lesquelles l'instruction est close à la date fixée par une ordonnance de clôture ou, à défaut, trois jours francs avant la date de l'audience, ne sont pas applicables aux contentieux sociaux régis par les articles R. 772-5 et suivants du CJA.... ,,Il suit de là que pour juger les requêtes régies par ces articles, il convient de prendre en considération tant les éléments de fait invoqués oralement à l'audience qui conditionnent l'attribution de la prestation ou de l'allocation ou la reconnaissance du droit, objet de la requête, que tous les mémoires enregistrés jusqu'à la clôture de l'instruction, qui intervient, sous réserve de la décision du juge de la différer, après que les parties ou leurs mandataires ont formulé leurs observations orales ou, si ces parties sont absentes ou ne sont pas représentées, après appel de leur affaire à l'audience.</ANA>
<ANA ID="9B"> 54-06-01 L'article R. 772-9 du code de justice administrative (CJA), qui déroge aux règles de droit commun de la procédure administrative contentieuse, tend, eu égard aux spécificités de l'office du juge en matière de contentieux sociaux, à assouplir les contraintes de la procédure écrite en ouvrant la possibilité à ce juge de poursuivre à l'audience la procédure contradictoire sur des éléments de fait et en décalant la clôture de l'instruction, laquelle est entièrement régie par les dispositions de son deuxième alinéa. Dès lors, les règles fixées par l'article R. 613-2 du même code, selon lesquelles l'instruction est close à la date fixée par une ordonnance de clôture ou, à défaut, trois jours francs avant la date de l'audience, ne sont pas applicables aux contentieux sociaux régis par les articles R. 772-5 et suivants du CJA.... ,,Il suit de là que pour juger les requêtes régies par ces articles, il convient de prendre en considération tant les éléments de fait invoqués oralement à l'audience qui conditionnent l'attribution de la prestation ou de l'allocation ou la reconnaissance du droit, objet de la requête, que tous les mémoires enregistrés jusqu'à la clôture de l'instruction, qui intervient, sous réserve de la décision du juge de la différer, après que les parties ou leurs mandataires ont formulé leurs observations orales ou, si ces parties sont absentes ou ne sont pas représentées, après appel de leur affaire à l'audience.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
