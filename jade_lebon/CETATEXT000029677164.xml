<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029677164</ID>
<ANCIEN_ID>JG_L_2014_10_000000385173</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/67/71/CETATEXT000029677164.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 25/10/2014, 385173</TITRE>
<DATE_DEC>2014-10-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>385173</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARTHELEMY, MATUCHANSKY, VEXLIARD, POUPOT</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2014:385173.20141025</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 16 octobre 2014 au secrétariat du contentieux du Conseil d'Etat, présentée par Mme F...I..., élisant domicile ...; la requérante demande au juge des référés du Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 14600 du 8 octobre 2014 par laquelle le juge des référés du tribunal administratif de Mayotte, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, a rejeté sa demande tendant, d'une part, à la suspension de l'exécution des arrêtés des 4 et 6 octobre 2014 du préfet de Mayotte plaçant en rétention administrative Mme B... C...et les quatre enfants mineurs l'accompagnant et prononçant à leur encontre une obligation de quitter le territoire français en tant qu'ils concernent sa fille, d'autre part, à ce qu'il soit enjoint au préfet  d'organiser le retour à Mayotte de celle-ci ;<br/>
<br/>
              2°) d'enjoindre au ministre de l'intérieur ainsi qu'aux autorités consulaires à Anjouan de prendre toutes mesures nécessaires pour le retour à Mayotte de sa fille Mlle J...A...E..., sous astreinte de 200 euros par jour de retard ou bien, à défaut, toute mesure qu'il estimera utile afin que soit organisé son retour effectif ; <br/>
<br/>
              3°) de l'admettre au bénéfice de l'aide juridictionnelle à titre provisoire ;<br/>
<br/>
              4°) de mettre à la charge de l'Etat la somme de 2 000 euros au titre des articles 37 de la loi du 10 juillet 1991 et L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
              elle soutient que : <br/>
<br/>
              - la condition d'urgence est remplie, dès lors que Mlle A...E..., âgée de six ans, est privée de la possibilité de vivre avec sa mère et ne peut être effectivement prise en charge par un membre de sa famille aux Comores ;<br/>
              - le préfet de Mayotte a porté une atteinte grave et manifestement illégale au droit de mener une vie familiale normale ;<br/>
              - la décision du préfet d'éloigner Mlle A...E...à destination des Comores méconnaît la convention internationale relative aux droits de l'enfant ; <br/>
              - le placement en rétention administrative de Mlle A...E...au sein du centre de rétention administrative de Mayotte, qui n'a pas été précédé de la désignation d'un administrateur ad hoc, constitue un traitement inhumain et dégradant au sens de l'article 3 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
<br/>
<br/>
                          	Vu l'ordonnance attaquée ;<br/>
<br/>
              Vu le mémoire en défense, enregistré le 22 octobre 2014, présenté par le ministre de l'intérieur, qui conclut au rejet de la requête ; <br/>
              il soutient que :<br/>
<br/>
              - la condition d'urgence n'est pas remplie dès lors que, d'une part, la décision litigieuse n'a eu pour conséquence que de replacer l'enfant dans la situation dans laquelle elle était avant son arrivée en France et que, d'autre part, la requérante se plaint d'une urgence qu'elle a elle-même contribué à créer ;<br/>
              - s'agissant des conditions de la rétention administrative de Mlle A...E... : d'une part, elle était accompagnée d'un représentant légal ; d'autre part, en tout état de cause, la rétention a pris fin le 9 octobre 2014 avec la mise en oeuvre de la mesure d'éloignement ; le moyen tiré de ce que les conditions matérielles de rétention du centre de rétention administrative de Mayotte constitueraient un traitement inhumain et dégradant au sens de l'article 3 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales est dès lors inopérant ;<br/>
              - la décision litigieuse ne méconnaît pas la convention internationale relative aux droits de l'enfant ; <br/>
              - il ne saurait être soutenu que la mesure litigieuse porte atteinte au droit de mener une vie familiale normale dès lors que la requérante ne justifie pas de l'identité de l'enfant placé en rétention ; <br/>
              - en tout état de cause, il n'appartient pas au juge des référés d'enjoindre à l'administration de procéder au retour d'un enfant ;<br/>
              - la requérante, qui n'a pas présenté de demande de regroupement familial, a la faculté, se trouvant en situation régulière, de quitter le territoire français pour les Comores ;  <br/>
<br/>
              Vu le mémoire en intervention, enregistré le 22 octobre 2014, présenté par la Cimade, dont le siège social est 64, rue Clisson à Paris (75013), représentée par son président en exercice, qui tend aux mêmes fins par les mêmes moyens ; elle soutient en outre qu'elle a intérêt à agir dès lors qu'elle défend les droits des personnes réfugiées et migrantes ; <br/>
<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, Mme I..., d'autre part, le ministre de l'intérieur ainsi que la Cimade ; <br/>
<br/>
              Vu le procès-verbal de l'audience publique du 23 octobre 2014 à 9 heures au cours de laquelle ont été entendus : <br/>
<br/>
              - Me Poupot, avocat au Conseil d'Etat et à la Cour de cassation, avocat de Mme I... ;<br/>
<br/>
- les représentants du ministre de l'intérieur ; <br/>
- le représentant de la Cimade ;<br/>
              et à l'issue de laquelle le juge des référés prolongé l'instruction jusqu'au vendredi 24 octobre 2014 à 16 heures ;<br/>
              Vu le mémoire complémentaire et le mémoire de production, enregistrés le 24 octobre 2014, présentés par MmeI... ; <br/>
<br/>
              Vu le mémoire complémentaire, enregistré le 24 octobre 2014, présenté par le ministre de l'intérieur ; <br/>
<br/>
                          Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
<br/>
              Vu la convention internationale relative aux droits de l'enfant ; <br/>
<br/>
              Vu le pacte international relatif aux droits civils et politiques ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures " ;<br/>
<br/>
              2. Considérant qu'il résulte des pièces de la procédure que, le 4 octobre 2014, Mme C...B...a été interpellée, en compagnie de quatre enfants mineurs, dans les eaux territoriales de Mayotte, dans une embarcation en provenance des Comores ; qu'il ressort du procès-verbal établi le même jour qu'elle a déclaré à la gendarmerie nationale que seuls deux de ces mineurs étaient ses propres enfants ; qu'elle a présenté un des autres enfants mineurs qui l'accompagnait, âgée d'environ cinq ans et de sexe féminin, sous l'identité de Fairouze H...sans préciser la nature du lien qu'elle entretenait avec elle ; que, le 6 octobre 2014, le préfet de Mayotte a pris un arrêté obligeant Mme B...à quitter le territoire français sans délai avec les enfants qui l'accompagnaient, fixant les Comores comme pays de renvoi, après avoir ordonné leur placement en rétention administrative pour une durée de cinq jours, par un arrêté du 4 octobre 2014 ; que Mme I...a saisi, le 6 octobre 2014, le tribunal administratif de Mayotte, d'une demande présentée sur le fondement de l'article L. 521-2 du code de justice administrative tendant à la suspension de l'exécution des arrêtés des 4 et 6 octobre 2014 en tant qu'ils concernent sa fille J...A...E..., d'autre part, à ce qu'il soit enjoint au préfet d'organiser le retour à Mayotte de celle-ci ; que, par une ordonnance du 8 octobre 2014, le juge des référés du tribunal administratif de Mayotte a rejeté cette demande ; qu'elle relève appel de cette ordonnance ;<br/>
<br/>
              3. Considérant que la Cimade a intérêt à l'annulation de l'ordonnance attaquée ; que son intervention est, par suite, recevable ;<br/>
              4. Considérant qu'il résulte de l'instruction et, en particulier, des éléments produits après l'audience publique, qu'à compter du 3 octobre 2014, dès qu'elle a eu connaissance de la circonstance que sa fille J...A...E..., âgée de six ans, aurait quitté les Comores où elle résidait jusqu'alors dans la famille de son père, pour rejoindre irrégulièrement Mayotte, Mme I...ressortissante comorienne, qui réside régulièrement à Mayotte, sous couvert d'une carte vie privée et familiale, a effectué de nombreuses démarches pour tenter d'entrer en contact avec elle ; qu'il ressort de plusieurs attestations versées au dossier émanant de travailleurs sociaux placés auprès du centre de rétention administrative de Mayotte qu'elle s'est rendue au centre de rétention munie de photos et du passeport de son enfant afin de rencontrer la jeune mineure qu'elle présentait comme étant sa fille ; qu'il lui a été indiqué que celle-ci avait été identifiée, dans le cadre de la procédure d'éloignement forcé dont elle faisait l'objet en qualité de mineure accompagnant Mme C...B..., sous le nom de G...H... ; qu'une attestation versée au dossier rapporte précisément que Mme I...a alors indiqué à une assistante sociale de l'association TAMA que cette enfant portait avec elle une preuve de son identité ; que cette même attestation témoigne, photocopie à l'appui, de la présence, dans le sac à dos de l'enfant, d'une pièce d'identité au nom d'J... A...E...correspondant à l'identité figurant sur l'original du passeport de l'enfant et l'acte de naissance qui étaient en possession de la requérante ; qu'il suit de là que l'état de l'instruction permet de regarder comme établis tant l'identité de la jeune mineure que son lien de filiation avec la requérante ; qu'il résulte de ce qui précède que Mme I... est fondée à soutenir que c'est à tort que le juge des référés du tribunal administratif de Mayotte a estimé qu'un doute sur l'identité de l'enfant persistait et a rejeté, pour ce motif, la demande dont il était saisi ;<br/>
              5. Considérant que la mise en oeuvre de la mesure d'éloignement litigieuse, le 9 octobre 2014, et la circonstance que la jeune enfant se trouve actuellement aux Comores, ne prive d'objet la procédure de référé engagée sur le fondement de l'article L. 521-2 du code de justice administrative, qui est destinée à protéger les libertés fondamentales en permettant au juge des référés d'ordonner toute mesure nécessaire à cette fin, que dans la seule mesure où elle portait sur le placement en rétention de l'enfant mineure ; que ces circonstances ne sont pas davantage de nature à mettre fin à la situation d'urgence que caractérisent les circonstances très particulières de l'espèce ;<br/>
<br/>
              6. Considérant que l'article L. 511-4 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA) dispose que : " Ne peuvent faire l'objet d'une obligation de quitter le territoire français ou d'une mesure de reconduite à la frontière en application du présent chapitre : / 1° L'étranger mineur de dix-huit ans (...) " ; que, toutefois, dès lors que l'article L. 553-1 du même code prévoit expressément la possibilité qu'un enfant mineur étranger soit accueilli dans un centre de rétention, par voie de conséquence du placement en rétention de la personne majeure qu'il accompagne, l'éloignement forcé d'un étranger majeur décidé sur le fondement de l'article L. 511-1 du CESEDA peut légalement entraîner celui du ou des enfants mineurs l'accompagnant ; que, dans une telle hypothèse, la mise en oeuvre de la mesure d'éloignement forcé d'un étranger mineur doit être entourée de garanties particulières de nature à assurer le respect effectif de ses droits et libertés fondamentaux ; qu'au nombre des exigences permettant d'en garantir l'effectivité figure notamment l'obligation, posée par l'article L. 553-1, que le registre qui doit être tenu dans tous les lieux recevant des personnes placées ou maintenues en rétention, mentionne " l'état-civil des enfants mineurs [...] ainsi que les conditions de leur accueil " ; qu'il s'ensuit que l'autorité administrative doit s'attacher à vérifier, dans toute la mesure du possible, l'identité d'un étranger mineur placé en rétention et faisant l'objet d'une mesure d'éloignement forcé par voie de conséquence de celle ordonnée à l'encontre de la personne qu'il accompagne ainsi que la nature exacte des liens qu'il entretient avec cette dernière <br/>
<br/>
              7. Considérant qu'en ordonnant et en mettant en oeuvre l'éloignement forcé de la jeune J...A...E..., au vu des seules allégations de MmeB..., sans s'être attachée à vérifier, dans toute la mesure du possible, l'identité de cette enfant mineure ainsi que la nature exacte des liens qu'elle entretenait avec la personne qu'elle accompagnait, l'administration n'a pas accompli les diligences nécessaires pour réunir les informations qu'elle devait, dans le cas d'un mineur, s'efforcer, dans la mesure du possible, de collecter ; qu'il suit de là que l'arrêté du 6 octobre 2014, qui ordonne l'éloignement forcé d'un enfant mineur en se méprenant sur son identité, est entaché d'une illégalité manifeste qui a porté et continue de porter atteinte, dans les circonstances particulières de l'espèce, au droit au respect de la vie privée et familiale de la jeune J...A...E... ; <br/>
<br/>
              8. Considérant toutefois, d'une part, qu'il appartient à un ressortissant étranger établi à Mayotte qui souhaite que ses enfants le rejoignent au titre du regroupement familial de se conformer aux exigences de la réglementation applicable à la mise en oeuvre de ce droit ; que, d'autre part, il n'appartient pas au juge administratif de prescrire des mesures qui, telle l'autorisation de sortie du territoire d'un autre Etat d'un de ses ressortissant mineur, relèvent de la compétence des autorités de cet Etat ; que, dans ces conditions, il y a seulement lieu d'ordonner au préfet de Mayotte d'instruire, dans les plus brefs délais, la demande de regroupement familial que Mme I... a annoncé vouloir déposer ; qu'il lui appartient, à cet effet, de prendre l'attache du consulat de France compétent et de solliciter de ce dernier un examen diligent de la situation de l'intéressée dès qu'il en sera saisi ; qu'il n'y a pas lieu d'assortir cette injonction d'une astreinte ; <br/>
              9. Considérant qu'il y a lieu, dans les circonstances de l'espèce, sans qu'il y ait lieu d'admettre Mme I...au bénéfice de l'aide juridictionnelle à titre provisoire, de mettre à la charge de l'Etat, la somme de 2 000 euros, au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : L'intervention de la Cimade est admise. <br/>
 Article 2 : L'ordonnance du juge des référés du 8 octobre 2014 du tribunal administratif de Mayotte est annulée.<br/>
<br/>
Article 3 : Il est enjoint au préfet de Mayotte de prendre les mesures prescrites au point 8 de la présente ordonnance.<br/>
Article 4 : L'Etat versera la somme de 2 000 euros à Mme I...au titre  de l'article L. 761-1 du code de justice administrative ;<br/>
Article 5: Le surplus des conclusions de la requête de  Mme I...est rejeté.<br/>
Article 6 : La présente ordonnance sera notifiée à Mme F...I..., au ministre de l'intérieur et à la Cimade.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">335-03-02 ÉTRANGERS. OBLIGATION DE QUITTER LE TERRITOIRE FRANÇAIS (OQTF) ET RECONDUITE À LA FRONTIÈRE. LÉGALITÉ INTERNE. - ELOIGNEMENT FORCÉ D'UN MINEUR - 1) MESURE POUVANT LÉGALEMENT ÊTRE MISE EN OEUVRE DANS LE CAS OÙ LE MINEUR ACCOMPAGNE  UN ÉTRANGER MAJEUR PLACÉ EN RÉTENTION FAISANT LUI-MÊME L'OBJET D'UNE TELLE MESURE - EXISTENCE [RJ1] - 2) GARANTIES PARTICULIÈRES À RESPECTER - OBLIGATION POUR L'AUTORITÉ ADMINISTRATIVE DE VÉRIFIER, DANS LA MESURE DU POSSIBLE, L'IDENTITÉ DU MINEUR ET LA NATURE DES LIENS QU'IL ENTRETIENT AVEC LE MAJEUR QU'IL ACCOMPAGNE.
</SCT>
<ANA ID="9A"> 335-03-02 1) Dès lors que l'article L. 553-1 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA) prévoit expressément la possibilité qu'un enfant mineur étranger soit accueilli dans un centre de rétention, par voie de conséquence du placement en rétention de la personne majeure qu'il accompagne, l'éloignement forcé d'un étranger majeur décidé sur le fondement de l'article L. 511-1 du CESEDA peut légalement entraîner celui du ou des enfants mineurs l'accompagnant.,,,2) Dans une telle hypothèse, la mise en oeuvre de la mesure d'éloignement forcé d'un étranger mineur doit être entourée de garanties particulières de nature à assurer le respect effectif de ses droits et libertés fondamentaux. Au nombre des exigences permettant d'en garantir l'effectivité figure notamment l'obligation, posée par l'article L. 553-1, que le registre qui doit être tenu dans tous les lieux recevant des personnes placées ou maintenues en rétention, mentionne l'état-civil des enfants mineurs ainsi que les conditions de leur accueil.... ,,Il s'ensuit que l'autorité administrative doit s'attacher à vérifier, dans toute la mesure du possible, l'identité d'un étranger mineur placé en rétention et faisant l'objet d'une mesure d'éloignement forcé par voie de conséquence de celle ordonnée à l'encontre de la personne qu'il accompagne ainsi que la nature exacte des liens qu'il entretient avec cette dernière.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., CE, 18 novembre 2011, Association avocats pour la défense du droit des étrangers et autres, n°s 335532 et autres, p. 568.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
