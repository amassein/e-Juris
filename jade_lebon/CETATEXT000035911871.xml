<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035911871</ID>
<ANCIEN_ID>JG_L_2017_10_000000396990</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/91/18/CETATEXT000035911871.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 25/10/2017, 396990</TITRE>
<DATE_DEC>2017-10-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>396990</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER ; SCP CELICE, SOLTNER, TEXIDOR, PERIER ; SCP GASCHIGNARD</AVOCATS>
<RAPPORTEUR>Mme Manon Perrière</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:396990.20171025</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La Fédération morbihannaise de la libre pensée, Mme C... B... et M. A... D...ont demandé au tribunal administratif de Rennes d'annuler les décisions implicites de rejet nées du silence gardé sur leurs demandes, présentées au maire de la commune de Ploërmel le 6 avril 2012 et le 26 juin 2012, tendant à ce que soit enlevé de tout emplacement public le monument consacré au pape Jean-Paul II, et d'enjoindre au maire de Ploërmel de faire respecter l'article 28 de la loi du 9 décembre 1905 en faisant disparaître ce monument de tout emplacement public. Par un jugement n°s 1203099, 1204355, 1204356 du 30 avril 2015, le tribunal administratif de Rennes a annulé les décisions contestées du maire de Ploërmel et lui a enjoint de procéder, dans le délai de six mois à compter de la notification du jugement, au retrait de son emplacement actuel du monument dédié au pape Jean-Paul II.<br/>
<br/>
              Par un arrêt n°s 15NT02053, 15NT02054 du 15 décembre 2015, la cour administrative d'appel de Nantes a annulé ce jugement et rejeté les conclusions de la Fédération morbihannaise de la libre pensée, de Mme B...et M. D....<br/>
<br/>
              Par un pourvoi et deux mémoires en réplique, enregistrés le 15 février 2016 et les 4 et 10 octobre 2017 au secrétariat du contentieux du Conseil d'Etat, la Fédération morbihannaise de la libre pensée, Mme B...et M. D...demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de la commune de Ploërmel ;<br/>
<br/>
              3°) de mettre à la charge de la commune la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des collectivités territoriales ;<br/>
              - la loi du 9 décembre 1905 concernant la séparation des Eglises et de l'Etat ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Manon Perrière, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Foussard, Froger, avocat de la Fédération morbihannaise de la libre pensée, de Mme B...et de M. D... et à la SCP Gaschignard, avocat de la commune de Ploërmel.<br/>
<br/>
              Vu la note en délibéré, enregistrée le 18 octobre 2017, présentée par la commune de Ploërmel.<br/>
<br/>
              Vu la note en délibéré, enregistrée le 23 octobre 2017, présentée par l'Association de défense de la statue de Jean-Paul II " Touche pas à mon pape ".<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la Fédération morbihannaise de la libre pensée, d'une part, Mme  B...et M.D..., d'autre part, ont demandé par courriers adressés au maire de la commune de Ploërmel, respectivement reçus les 6 avril et 26 juin 2012, de retirer de tout emplacement public de cette commune le monument consacré au pape Jean-Paul II, érigé sur la place Jean-Paul II à la suite d'une délibération du 28 octobre 2006 par laquelle le conseil municipal a accepté le don, fait par l'artiste russe F...E..., d'une statue représentant ce pape destinée à être implantée sur une place publique de la commune. Par un jugement du 30 avril 2015, le tribunal administratif de Rennes a accueilli les demandes des intéressés tendant à l'annulation des décisions implicites de rejet qui leur ont été opposées et a enjoint au maire de la commune de Ploërmel de faire procéder, dans un délai de six mois, au retrait du monument de son emplacement actuel. Par un arrêt du 15 décembre 2015, contre lequel la Fédération morbihannaise de la Libre Pensée, Mme B... et M. D...se pourvoient en cassation, la cour administrative d'appel de Nantes a annulé ce jugement et a rejeté leurs demandes. <br/>
<br/>
              Sur l'intervention de l'association " Touche pas à mon pape " :<br/>
<br/>
              2.  L'association a intérêt au maintien de l'arrêt attaqué. Ainsi, son intervention est recevable.<br/>
<br/>
              Sur l'arrêt attaqué :<br/>
<br/>
              3. Pour annuler le jugement du tribunal administratif et rejeter les demandes de Fédération morbihannaise de la Libre Pensée, de Mme  B...et de M.D..., la cour, après avoir analysé les demandes adressées au maire de la commune les 6 avril et 26 juin 2012 comme tendant implicitement mais nécessairement à l'abrogation de la délibération susmentionnée du 28 octobre 2006, s'est fondée sur le moyen, qu'elle a relevé d'office, tiré de ce que les requérants ne pouvaient utilement invoquer, à l'encontre du refus d'abrogation, la circonstance que la décision d'implanter le monument litigieux sur une place publique de la commune méconnaîtrait l'article 28 de la loi du 9 décembre 1905 concernant la séparation des Eglises et de l'Etat, dès lors que la délibération du 28 octobre 2006 était devenue définitive à la date des demandes et que l'illégalité alléguée ne procédait pas de changements dans les circonstances de droit ou de fait intervenus postérieurement à son édiction mais l'affectait depuis son origine.<br/>
<br/>
              4. L'autorité administrative compétente, saisie par une personne intéressée d'une demande en ce sens, n'est tenue de procéder à l'abrogation d'une décision non réglementaire qui n'a pas créé de droits que si cette décision est devenue illégale à la suite de changements dans les circonstances de droit ou de fait intervenus postérieurement à son édiction. <br/>
<br/>
              5. Il ressort des pièces du dossier soumis aux juges du fond que la cour administrative d'appel, contrairement à ce qui est soutenu, ne s'est pas méprise sur le sens des demandes adressées au maire de Ploërmel les 6 avril et 26 juin 2012 en les interprétant comme tendant à l'abrogation de la décision de la commune d'implanter sur un emplacement public le monument litigieux, qui se compose d'une statue du pape Jean-Paul II ainsi que d'une arche surmontée d'une croix, l'ensemble étant d'une hauteur de 7,5 mètres hors socle, et non comme tendant à ce que le maire fasse usage des pouvoirs dont il dispose en vue d'assurer la protection du domaine public communal. <br/>
<br/>
              6. Toutefois, il ressort également des pièces du dossier soumis aux juges du fond que la délibération du 28 octobre 2006 avait exclusivement pour objet l'acceptation, par la commune, d'un don de M. E...portant sur une statue représentant le pape Jean-Paul II en vue de son installation sur la place éponyme de la commune et ne comportait aucun élément relatif à l'arche et à la croix de grande dimension, distinctes de la statue et installées en surplomb de celle-ci. L'installation, au-dessus de la statue, d'une arche et d'une croix doit ainsi être regardée comme révélant l'existence d'une décision du maire de la commune distincte de la délibération du 28 octobre 2006, alors même que le monument aurait comporté ces deux éléments dès sa création par l'artiste. En jugeant que la décision d'implanter le monument sur la place Jean-Paul II était contenue, pour la totalité de ce monument, dans la délibération du 28 octobre 2006, la cour a, en conséquence, inexactement apprécié la portée de cette délibération.<br/>
<br/>
              7. Il en résulte que si la cour a pu, après avoir relevé, sans entacher son arrêt d'insuffisance de motivation sur ce point, que la délibération du 28 octobre 2006 était devenue définitive à la date des demandes d'abrogation, en déduire sans erreur de droit que le maire n'était pas tenu d'abroger la décision d'implanter le monument en tant que celui-ci se compose de la statue du pape Jean-Paul II, dès lors que l'illégalité invoquée à l'encontre de cette décision non réglementaire non créatrice de droit l'affectait dès son adoption, elle n'a pu, sans commettre d'erreur de droit, juger pour ce même motif que le maire était fondé à refuser d'abroger la décision distincte de faire surplomber la statue d'une arche et d'une croix, qui n'avait fait l'objet d'aucune mesure de publicité de nature à faire courir les délais de recours à son encontre.<br/>
<br/>
              8. Par suite, les requérants sont seulement fondés à demander l'annulation de l'arrêt qu'ils attaquent en tant qu'il porte sur leurs conclusions relatives à leur demande de retrait du monument, en ce que celui-ci comporte une arche et une croix. <br/>
<br/>
              9. Il y a lieu de régler, dans cette mesure, l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              10. Mme B...et M.D..., dont il est constant qu'ils résident dans la commune de Ploërmel, justifient à ce titre d'un intérêt leur donnant qualité pour demander l'annulation pour excès de pouvoir des décisions implicites de refus opposées par le maire à leur demande tendant à ce que le monument, en ce qu'il comporte une arche et une croix, soit retiré de tout emplacement public de la commune. De même, la Fédération de la libre pensée du Morbihan, qui s'est donné pour objet, notamment, de défendre le principe de laïcité et l'application de la loi du 9 décembre 1905 de séparation des églises et de l'Etat sur le territoire du département du Morbihan, dans lequel se situe la commune de Ploërmel, dispose d'un intérêt lui donnant qualité pour demander l'annulation de la décision implicite de refus opposé à sa propre demande d'abrogation. Il en résulte que la commune n'est pas fondée à soutenir que le tribunal aurait dû relever d'office l'irrecevabilité, pour défaut d'intérêt à agir, des demandes qui lui ont été soumises.<br/>
<br/>
              11. Aux termes de l'article 28 de la loi du 9 décembre 1905 concernant la séparation des Eglises et de l'Etat : " Il est interdit, à l'avenir, d'élever ou d'apposer aucun signe ou emblème religieux sur les monuments publics ou en quelque emplacement public que ce soit. à l'exception des édifices servant au culte, des terrains de sépulture dans les cimetières, des monuments funéraires ainsi que des musées ou expositions ". Ces dernières dispositions, qui ont pour objet d'assurer la neutralité des personnes publiques à l'égard des cultes, s'opposent à l'installation par celles-ci, dans un emplacement public, d'un signe ou emblème manifestant la reconnaissance d'un culte ou marquant une préférence religieuse, sous réserve des exceptions qu'elles ménagent.<br/>
<br/>
              12. Il  ressort des pièces du dossier que la statue du pape Jean-Paul II, érigée en 2006 sur une place publique de la commune de Ploërmel, est, ainsi qu'il a été dit, surplombée d'une croix de grande dimension reposant sur une arche, l'ensemble monumental étant d'une hauteur de 7,5 mètres hors socle. Si l'arche surplombant la statue ne saurait, par elle-même, être regardée comme un signe ou emblème religieux au sens de l'article 28 précité de la loi du 9 décembre 1905, il en va différemment, eu égard à ses caractéristiques, de la croix. Par suite, l'édification de cette croix sur un emplacement public autre que ceux prévus par l'article 28 de la loi du 9 décembre 1905 précité méconnaît ces dispositions, sans que la commune et l'association intervenante en défense soient utilement fondées à se prévaloir ni du caractère d'oeuvre d'art du monument, ni de ce que la croix constituerait l'expression d'une forte tradition catholique locale, ni de la circonstance, au demeurant non établie, que la parcelle communale sur laquelle a été implantée la statue aurait fait l'objet d'un déclassement postérieurement aux décisions attaquées. En outre, sont sans incidence sur la légalité des décisions attaquées la circonstance que l'installation de la statue aurait fait l'objet d'une décision de non-opposition à déclaration de travaux au profit de la commune devenue définitive ainsi que les moyens tirés de l'intérêt économique et touristique du monument pour la commune et de ce que le retrait de tout ou partie de l'oeuvre méconnaîtrait les engagements contractuels la liant à l'artiste.<br/>
<br/>
              13. Il résulte de ce qui précède, l'arrêt n'ayant été cassé que dans la mesure où il a rejeté les conclusions tendant à ce que soient retirées du monument l'arche et la croix surplombant la statue, que la commune de Ploërmel est seulement fondée à soutenir que c'est à tort que le tribunal administratif, dont le jugement est suffisamment motivé et n'est pas entaché de contradiction de motifs, s'est fondé, pour annuler les décisions attaquées en tant qu'elles portent sur l'arche installée en surplomb de la statue du pape Jean-Paul II, sur ce que l'implantation de cette arche sur un emplacement public méconnaissait l'article 28 de la loi du 9 décembre 1905.<br/>
<br/>
              14. Il appartient, toutefois, au Conseil d'Etat, saisi de l'ensemble du litige par l'effet dévolutif de l'appel, d'examiner les autres moyens soulevés par la Fédération morbihannaise de la Libre Pensée, Mme  B...et M. D...devant le tribunal administratif. <br/>
<br/>
              15. D'une part, l'implantation de l'arche en surplomb de la statue ne méconnaissant pas, ainsi qu'il a été dit, les dispositions de l'article 28 de la loi du 9 décembre 1905, les requérants ne sont pas fondés à soutenir que le maire était tenu de faire cesser un trouble allégué à l'ordre public qui procèderait de la méconnaissance de cette loi, ou, en tout état de cause, de faire droit à leur demande en application des dispositions de l'article L. 2122-27 du code général des collectivités territoriales. D'autre part, la circonstance que la convention par laquelle l'artiste a cédé à la commune ses droits patrimoniaux sur l'oeuvre comporterait des clauses entachées de nullité est sans incidence sur la légalité des décisions attaquées. <br/>
<br/>
              16. Il résulte de ce qui précède que la commune de Ploërmel, qui n'est pas fondée à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif a annulé les décisions attaquées en tant qu'elles portent sur la croix installée sur l'arche surplombant la statue de Jean Paul II,  est seulement fondée à soutenir que c'est à tort que, par ce jugement, le tribunal administratif a annulé les décisions attaquées en tant qu'elles portent sur cette arche et enjoint à la commune de procéder au retrait de celle-ci. <br/>
<br/>
              17. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de Ploërmel la somme de 3 000 euros à verser à la Fédération morbihannaise de la libre pensée, à Mme B...et à M. D...au titre de l'article L. 761-1 du code de justice administrative. Ces mêmes dispositions font, en revanche, obstacle à ce qu'une somme soit mise à la charge de la Fédération morbihannaise de la libre pensée, de Mme B...et de M. D..., qui ne sont pas les parties perdantes.<br/>
<br/>
<br/>
<br/>                      D E C I D E :<br/>
                                    --------------<br/>
<br/>
Article 1er : L'intervention de l'association " Touche pas à mon pape " est admise. <br/>
Article 2 : L'arrêt du 15 décembre 2015 de la cour administrative d'appel de Nantes est annulé en tant qu'il porte sur les conclusions relatives aux décisions de refus de retirer l'arche et la croix installées en surplomb de la statue du pape Jean-Paul II.<br/>
Article 3 : Les demandes tendant à l'annulation des décisions attaquées et à ce qu'une injonction soit prononcée à l'encontre de la commune de Ploërmel sont rejetées en tant qu'elles portent sur l'arche surplombant la statue du pape Jean-Paul II.<br/>
Article 4 : Les articles 1er et 2 du  jugement du tribunal administratif de Rennes du 30 avril 2015 sont réformés en ce qu'ils sont contraires à l'article 3 de la présente décision compte tenu de la cassation prononcée à l'article 2.<br/>
Article 5 : La commune de Ploërmel versera la somme globale de 3 000 euros à la Fédération morbihannaise de la libre pensée, à Mme B...et à M.D....<br/>
Article 6 : Les conclusions de la commune de Ploërmel et de l'association " Touche pas à mon pape " tendant à la mise en oeuvre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 7 : Le surplus des conclusions d'appel de la commune et le surplus des conclusions du pourvoi sont rejetés.<br/>
Article 8 : La présente décision sera notifiée à la Fédération morbihannaise de la libre pensée, à Mme C...B..., à M. A...D..., à la commune de Ploërmel, à M. F...E...et à l'association de défense de la statue de Jean-Paul II " Touche pas à mon pape ".<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-04-03-07-02 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. PRINCIPES GÉNÉRAUX DU DROIT. PRINCIPES INTÉRESSANT L'ACTION ADMINISTRATIVE. NEUTRALITÉ DU SERVICE PUBLIC. - PRINCIPE DE LAÏCITÉ - INTERDICTION DES SIGNES OU EMBLÈMES RELIGIEUX SUR LES EMPLACEMENTS PUBLICS (ART. 28 DE LA LOI DU 9 DÉCEMBRE 1905) - 1) PORTÉE [RJ1] - 2) ESPÈCE - CROIX REPOSANT SUR UNE ARCHE ET SURPLOMBANT LA STATUE D'UN PAPE, ÉRIGÉE SUR UNE PLACE PUBLIQUE D'UNE COMMUNE - LÉGALITÉ - ABSENCE [RJ2].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">21 CULTES. - PRINCIPE DE LAÏCITÉ - INTERDICTION DES SIGNES OU EMBLÈMES RELIGIEUX SUR LES EMPLACEMENTS PUBLICS (ART. 28 DE LA LOI DU 9 DÉCEMBRE 1905) - 1) PORTÉE [RJ1] - 2) ESPÈCE - CROIX REPOSANT SUR UNE ARCHE ET SURPLOMBANT LA STATUE D'UN PAPE, ÉRIGÉE SUR UNE PLACE PUBLIQUE D'UNE COMMUNE - LÉGALITÉ - ABSENCE [RJ2].
</SCT>
<ANA ID="9A"> 01-04-03-07-02 1) Les dispositions de l'article 28 de la loi du 9 décembre 1905 concernant la séparation des Eglises de l'Etat, qui ont pour objet d'assurer la neutralité des personnes publiques à l'égard des cultes, s'opposent à l'installation par celles-ci, dans un emplacement public, d'un signe ou emblème manifestant la reconnaissance d'un culte ou marquant une préférence religieuse, sous réserve des exceptions qu'elles ménagent.,,,2) Statue du pape Jean-Paul II, érigée en 2006 sur une place publique de la commune de Ploërmel, surplombée d'une croix de grande dimension reposant sur une arche, l'ensemble monumental étant d'une hauteur de 7,5 mètres hors socle.... ,,Si l'arche surplombant la statue ne saurait, par elle-même, être regardée comme un signe ou emblème religieux au sens de l'article 28 de la loi du 9 décembre 1905, il en va différemment, eu égard à ses caractéristiques, de la croix.... ,,Par suite, l'édification de cette croix sur un emplacement public autre que ceux prévus par cet article méconnaît ces dispositions, sans que la commune soit utilement fondée à se prévaloir ni du caractère d'oeuvre d'art du monument, ni de ce que la croix constituerait l'expression d'une forte tradition catholique locale, ni de la circonstance, au demeurant non établie, que la parcelle communale sur laquelle a été implantée la statue aurait fait l'objet d'un déclassement postérieurement aux décisions attaquées. En outre, sont sans incidence sur la légalité des décisions attaquées la circonstance que l'installation de la statue aurait fait l'objet d'une décision de non-opposition à déclaration de travaux au profit de la commune devenue définitive ainsi que les moyens tirés de l'intérêt économique et touristique du monument pour cette dernière et de ce que le retrait de tout ou partie de l'oeuvre méconnaîtrait les engagements contractuels la liant à l'artiste.</ANA>
<ANA ID="9B"> 21 1) Les dispositions de l'article 28 de la loi du 9 décembre 1905 concernant la séparation des Eglises de l'Etat, qui ont pour objet d'assurer la neutralité des personnes publiques à l'égard des cultes, s'opposent à l'installation par celles-ci, dans un emplacement public, d'un signe ou emblème manifestant la reconnaissance d'un culte ou marquant une préférence religieuse, sous réserve des exceptions qu'elles ménagent.,,,2) Statue du pape Jean-Paul II, érigée en 2006 sur une place publique de la commune de Ploërmel, surplombée d'une croix de grande dimension reposant sur une arche, l'ensemble monumental étant d'une hauteur de 7,5 mètres hors socle.... ,,Si l'arche surplombant la statue ne saurait, par elle-même, être regardée comme un signe ou emblème religieux au sens de l'article 28 de la loi du 9 décembre 1905, il en va différemment, eu égard à ses caractéristiques, de la croix.... ,,Par suite, l'édification de cette croix sur un emplacement public autre que ceux prévus par cet article méconnaît ces dispositions, sans que la commune soit utilement fondée à se prévaloir ni du caractère d'oeuvre d'art du monument, ni de ce que la croix constituerait l'expression d'une forte tradition catholique locale, ni de la circonstance, au demeurant non établie, que la parcelle communale sur laquelle a été implantée la statue aurait fait l'objet d'un déclassement postérieurement aux décisions attaquées. En outre, sont sans incidence sur la légalité des décisions attaquées la circonstance que l'installation de la statue aurait fait l'objet d'une décision de non-opposition à déclaration de travaux au profit de la commune devenue définitive ainsi que les moyens tirés de l'intérêt économique et touristique du monument pour cette dernière et de ce que le retrait de tout ou partie de l'oeuvre méconnaîtrait les engagements contractuels la liant à l'artiste.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Assemblée, 9 novembre 2016, Commune de Melun, n° 395122, p. 462 ; CE, Assemblée, 9 novembre 2016, Fédération de la libre pensée de Vendée, n° 395223, p. 449.,,[RJ2] Cf., s'agissant des conditions de légalité de l'installation d'une croix, CE, Avis, 28 juillet 2017,,, n° 408920, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
