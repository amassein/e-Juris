<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028349193</ID>
<ANCIEN_ID>JG_L_2013_12_000000362514</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/34/91/CETATEXT000028349193.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème et 7ème sous-sections réunies, 18/12/2013, 362514</TITRE>
<DATE_DEC>2013-12-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>362514</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème et 7ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BLANC, ROUSSEAU ; HAAS</AVOCATS>
<RAPPORTEUR>Mme Sophie-Caroline de Margerie</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier Domino</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:362514.20131218</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés au secrétariat du contentieux du Conseil d'Etat les 6 septembre et 6 décembre 2012, présentés pour Mme A...B..., demeurant ...; Mme B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement n° 09055620-1002917-1007405-1103135-1105686 du 3 juillet 2012 du tribunal administratif de Marseille, en tant qu'il a rejeté sa demande tendant à l'annulation de la décision du 25 juillet 2011 par laquelle La Poste a prolongé sa mise en disponibilité d'office pour la période allant du 1er août au 30 septembre 2011 ;<br/>
<br/>
              2°) de mettre à la charge de La Poste le versement de la somme de 3 000 &#128; au titre de l'article L 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi n° 79-587 du 11 juillet 1979 ;<br/>
<br/>
              Vu la loi n° 84-16 du 11 janvier 1984 ;<br/>
<br/>
              Vu le décret n° 85-986 du 16 septembre 1985 ;<br/>
<br/>
              Vu le décret n° 86-442 du 14 mars 1986 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie-Caroline de Margerie, Conseiller d'Etat,<br/>
<br/>
              - les conclusions de M. Xavier Domino, Rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Blanc, Rousseau, avocat de MmeB..., et à Me Haas, avocat de La Poste ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que Mme B..., agent de La Poste, affectée au centre financier de Marseille, a été placée en congé de maladie du 31 juillet 2008 au 31 juillet 2009 ; qu'à l'issue de ce congé, elle a été placée en disponibilité d'office pour une durée de six mois par une décision du 9 juillet 2009 ; que cette disponibilité a fait l'objet de cinq renouvellements par des décisions des 16 mars 2010, 20 septembre 2010, 18 janvier 2011, 29 avril 2011 et 25 juillet 2011 ; que, par un jugement du 3 juillet 2012, le tribunal administratif de Marseille a, à la demande de Mme B..., annulé les décisions des 9 juillet 2009, 16 mars 2010, 20 septembre 2010, 18 janvier 2011 et 29 avril 2011, mais a rejeté sa demande tendant à l'annulation de la décision du 25 juillet 2011 ainsi que ses conclusions tendant à ce qu'il soit enjoint, sous astreinte, à La Poste de réexaminer sa situation ; que Mme B...en demande, dans cette mesure, l'annulation ;<br/>
<br/>
              2.	Considérant qu'aux termes de l'article 43 du décret du 16 septembre 1985 relatif au régime particulier de certaines positions des fonctionnaires de l'Etat, à la mise à disposition, à l'intégration et à la cessation définitive de fonctions  : " La mise en disponibilité ne peut être prononcée d'office qu'à l'expiration des droits statutaires à congés de maladie (...) et s'il ne peut, dans l'immédiat, être procédé au reclassement du fonctionnaire (...) La durée de la disponibilité prononcée d'office ne peut excéder une année. Elle peut être renouvelée deux fois pour une durée égale. Si le fonctionnaire n'a pu, durant cette période, bénéficier d'un reclassement, il est, à l'expiration de cette durée, soit réintégré dans son administration s'il est physiquement apte à reprendre ses fonctions, soit, en cas d'inaptitude définitive à l'exercice des fonctions, admis à la retraite, ou, s'il n'a pas droit à pension, licencié (...) " ; qu'aux termes de l'article 49 du même décret : " (...) La réintégration est subordonnée à la vérification par un médecin agréé et, éventuellement, par le comité médical compétent, saisi dans les conditions prévues par la réglementation en vigueur, de l'aptitude physique du fonctionnaire à l'exercice des fonctions afférentes à son grade (...).  Le fonctionnaire qui, à l'issue de sa disponibilité ou avant cette date, s'il sollicite sa réintégration anticipée, ne peut être réintégré pour cause d'inaptitude physique est soit reclassé dans les conditions prévues par la réglementation en vigueur, soit mis en disponibilité d'office (...), soit, en cas d'inaptitude définitive à l'exercice des fonctions, admis à la retraite ou, s'il n'a pas droit à pension, licencié " ; qu'aux termes de l'article 13 du décret du 14 mars 1986 relatif à la désignation des médecins agréés, à l'organisation des comités médicaux et des commissions de réforme, aux conditions d'aptitude physique pour l'admission aux emplois publics et au régime de congés de maladie des fonctionnaires : " La commission de réforme est consultée notamment sur : / (...) 6. L'application des dispositions du code des pensions civiles et militaires de retraite. / 7. L'application, s'il y a lieu, des dispositions réglementaires relatives à la mise en disponibilité d'office pour raison de santé (....) " ; qu'enfin, aux termes de l'article 19 du même décret : " (...) Le secrétariat de la commission de réforme informe le fonctionnaire : / - de la date à laquelle la commission de réforme examinera son dossier ; / - de ses droits concernant la communication de son dossier et la possibilité de se faire entendre par la commission de réforme, de même que de faire entendre le médecin et la personne de son choix (...) " ; <br/>
<br/>
              3.	Considérant que le dossier mentionné par les dispositions précitées de l'article 19 du décret du 14 mars 1986 doit contenir le rapport du médecin agréé qui a examiné le fonctionnaire ; que, si ces dispositions n'exigent pas que l'administration procède de sa propre initiative à la communication des pièces médicales du dossier d'un fonctionnaire avant la réunion de la commission de réforme, elles impliquent que ce dernier ait été informé de la possibilité d'obtenir la consultation de ces pièces ; qu'il résulte de ce qui précède que le tribunal administratif a commis une erreur de droit en écartant le moyen, invoqué par la requérante, tiré de ce que le rapport du médecin spécialiste agréé ne lui avait pas été communiqué avant la réunion de la commission de réforme, au motif qu'elle n'avait pas formé de demande en ce sens et que l'administration n'était pas tenue de procéder de sa propre initiative à cette communication, sans rechercher si Mme B...avait été informée de cette possibilité ; <br/>
<br/>
              4.	Considérant qu'il résulte de ce qui précède, et sans qu'il soit besoin de statuer sur les autres moyens du pourvoi, que Mme B...est fondée à demander l'annulation du jugement attaqué en tant qu'il a rejeté sa demande tendant à l'annulation de la décision du 25 juillet 2011 par laquelle La Poste a prolongé sa mise en disponibilité d'office pour la période allant du 1er août au 30 septembre 2011 et à ce qu'il soit enjoint, sous astreinte, à La Poste de réexaminer sa situation ;<br/>
<br/>
              5.	Considérant qu'il y a lieu, dans les circonstances de l'espèce, en application des dispositions de l'article L. 821-2 du code de justice administrative, de régler l'affaire au fond dans la mesure de la cassation prononcée ci-dessus ;<br/>
<br/>
              6.	Considérant que Mme B...a produit la décision du 25 juillet 2011 qu'elle attaque ; que, par suite, la fin de non-recevoir opposée par La Poste et tirée du défaut de production de cette décision ne peut qu'être rejetée ; <br/>
<br/>
              7.	Considérant qu'il ressort des pièces du dossier que la lettre en date du 29 avril 2011 par laquelle Mme B...a été informée que la commission de réforme examinerait son cas lors de sa séance du 10 mai 2011 portait la mention qu'elle pouvait consulter la partie administrative de son dossier ; qu'il en résulte que Mme B...n'a pas été informée, contrairement aux dispositions de l'article 19 du décret du 14 mars 1986 précité, qu'elle pouvait également consulter les pièces médicales de son dossier ; qu'elle a été ainsi privée d'une garantie ; qu'elle est, par suite, et sans qu'il soit besoin d'examiner les autres moyens de sa demande, fondée à demander l'annulation de la décision litigieuse ; qu'il y a lieu de prescrire à La Poste de réexaminer la situation administrative de Mme B...pour la période allant du 1er août au 30 septembre 2011 ; qu'il n'y a pas lieu d'assortir cette injonction d'une astreinte ; <br/>
<br/>
              Sur l'application des dispositions des articles L. 761-1 du code de justice administrative :<br/>
<br/>
              8.	Considérant que ces dispositions font obstacle à ce que soit mise à la charge de MmeB..., qui n'est pas la partie perdante dans la présente instance, la somme que demande La Poste sur ce fondement ; qu'il y a lieu, en revanche, dans les circonstances de l'espèce, de mettre à la charge de La Poste le versement à Mme B...de la somme de 3 000 euros au même titre ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Marseille en date du 3 juillet 2012 est annulé en tant qu'il a rejeté les conclusions de Mme B...tendant à l'annulation de la décision du 25 juillet 2011, par laquelle La Poste a prolongé sa mise en disponibilité d'office pour la période allant du 1er août au 30 septembre 2011, ainsi que ses conclusions à fin d'injonction.<br/>
<br/>
Article 2 : La décision du 25 juillet 2011, par laquelle La Poste a prolongé la mise en disponibilité d'office de Mme B...pour la période allant du 1er août au 30 septembre 2011, est annulée.<br/>
<br/>
Article 3 : Il est enjoint à La Poste de réexaminer la situation administrative de Mme B...pour la période allant du 1er août au 30 septembre 2011.<br/>
Article 4 : La Poste versera à Mme B... la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 5 : Les conclusions de La Poste présentées au titre des dispositions de l'article L. 761-1 du code de la justice administrative sont rejetées.<br/>
<br/>
Article 6 : La présente décision sera notifiée à Mme A...B...et à La Poste.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-07-04-01 FONCTIONNAIRES ET AGENTS PUBLICS. STATUTS, DROITS, OBLIGATIONS ET GARANTIES. COMITÉS MÉDICAUX. PROCÉDURE. - COMMISSION DE RÉFORME - DROIT D'ACCÈS DU FONCTIONNAIRE À SON DOSSIER AVANT LA RÉUNION DE LA COMMISSION DE RÉFORME - PORTÉE - DROIT D'ACCÈS DU FONCTIONNAIRE AU RAPPORT DU MÉDECIN AGRÉÉ QUI L'A EXAMINÉ - INCLUSION - MODALITÉS.
</SCT>
<ANA ID="9A"> 36-07-04-01 Le dossier mentionné par les dispositions n° 86-442 de l'article 19 du décret du 14 mars 1986, à la communication duquel le fonctionnaire a droit avant la réunion de la commission de réforme, doit contenir le rapport du médecin agréé qui a examiné le fonctionnaire. Si ces dispositions n'exigent pas que l'administration procède de sa propre initiative à la communication des pièces médicales du dossier d'un fonctionnaire avant la réunion de la commission de réforme, elles impliquent que ce dernier ait été informé de la possibilité d'obtenir la consultation de ces pièces.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
