<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026687501</ID>
<ANCIEN_ID>JG_L_2012_11_000000352916</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/68/75/CETATEXT000026687501.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème et 7ème sous-sections réunies, 26/11/2012, 352916</TITRE>
<DATE_DEC>2012-11-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>352916</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème et 7ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>Mme Airelle Niepce</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Béatrice Bourgeois-Machureau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:352916.20121126</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu pourvoi sommaire et le mémoire complémentaire, enregistrés les 23 septembre et 21 novembre 2011 au secrétariat du contentieux du Conseil d'Etat, présentés par le ministre de l'écologie, du développement durable, des transports et du logement ; le ministre demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 10DA01073 du 19 juillet 2011 par lequel la cour administrative d'appel de Douai a rejeté son recours tendant, d'une part, à l'annulation du jugement n° 0800147 du 20 mai 2010 par lequel le tribunal administratif d'Amiens a annulé, à la demande de la société Avenir, la décision en date du 20 novembre 2007 du préfet de l'Aisne mettant en demeure cette société de supprimer un dispositif publicitaire implanté entre les numéros 109 et 78 de la route de Paris sur le territoire de la commune de Vauxbuin, d'autre part, au rejet de la demande présentée par la société Avenir devant le tribunal administratif d'Amiens ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son recours en appel ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 8 novembre 2012, présentée pour la société JC Decaux France ;<br/>
<br/>
              Vu le code de l'environnement ; <br/>
<br/>
              Vu la code de la route ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Airelle Niepce, Maître des requêtes en service extraordinaire, <br/>
<br/>
              - les observations de la SCP Lyon-Caen, Thiriez, avocat de la société Avenir,  <br/>
<br/>
              - les conclusions de Mme Béatrice Bourgeois-Machureau, Rapporteur public,<br/>
<br/>
              La parole ayant à nouveau été donnée à la SCP Lyon-Caen, Thiriez, avocat de la société Avenir ;  <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Sur le pourvoi : <br/>
<br/>
              1. Considérant qu'aux termes de l'article L. 581-7 du code de l'environnement, dans sa rédaction en vigueur à la date de la décision attaquée devant les juges du fond : " En dehors des lieux qualifiés d'agglomération par les règlements relatifs à la circulation routière, toute publicité est interdite, sauf dans des zones dénommée "zones de publicité autorisée" " ; que si, en vertu de l'article L. 581-9 du même code, la publicité est admise dans les agglomérations, elle " doit toutefois satisfaire, notamment en matière d'emplacements, de surface, de hauteur et d'entretien, à des prescriptions fixées par décret en Conseil d'Etat en fonction des procédés, des dispositifs utilisés, des caractéristiques des supports et de l'importance des agglomérations concernées " ; <br/>
<br/>
              2. Considérant qu'aux termes du premier alinéa de l'article R. 581-23, pris sur le fondement de ces dernières dispositions, dans sa rédaction alors en vigueur : " Les dispositifs publicitaires non lumineux, scellés au sol ou installés directement sur le sol, sont interdits dans les agglomérations de moins de 10 000 habitants qui ne font pas partie d'une ensemble multicommunal de plus de 100 000 habitants " ; <br/>
<br/>
              3. Considérant que selon l'article R. 110-2 du code de la route, le terme " agglomération " désigne un " espace sur lequel sont groupés des immeubles bâtis rapprochés et dont l'entrée et la sortie sont signalés par des panneaux placés à cet effet le long de la route qui le traverse ou qui le borde " ; que l'article R. 411-2 du même code prévoit que les limites des agglomérations sont fixées par arrêté du maire ; <br/>
<br/>
              4. Considérant que, pour l'application des dispositions législatives et réglementaires citées ci-dessus, la notion d'agglomération, qui doit être entendue comme un espace sur lequel sont groupés des immeubles bâtis rapprochés, ne saurait, en l'absence de disposition contraire, être appréhendée qu'à l'intérieur du territoire d'une seule commune ; que le seuil de 10 000 habitants de l'article R. 581-23 doit, par suite, être apprécié pour chaque commune isolément ; qu'il s'ensuit que les dispositifs publicitaires scellés au sol ne peuvent être admis, en vertu de l'article R. 581-23, dans une agglomération de moins de 10 000 habitants que si la commune correspondante forme, avec d'autres communes, un ensemble qui dépasse 100 000 habitants ;<br/>
<br/>
              5. Considérant que, pour confirmer le jugement par lequel le tribunal administratif d'Amiens avait annulé l'arrêté du préfet de l'Aisne du 20 novembre 2007 mettant en demeure la société Avenir de procéder à l'enlèvement d'un dispositif publicitaire implanté sur le territoire de la commune de Vauxbuin, la cour administrative d'appel de Douai a estimé que cette commune formait avec celle de Soissons une même agglomération comportant une population supérieure à 10 000 habitants ; qu'il résulte de ce qui vient d'être dit qu'en statuant ainsi, la cour a commis une erreur de droit ; que, par suite et sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, l'arrêt attaqué doit être annulé ; <br/>
<br/>
              6. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              Sur la fin de non-recevoir opposée à l'appel du ministre :<br/>
<br/>
              7. Considérant qu'il ressort des pièces du dossier que le ministre de l'écologie, de l'énergie, du développement durable et de la mer a reçu notification le 23 juin 2010 du jugement du tribunal administratif d'Amiens ; que le recours du ministre transmis par télécopie et enregistré au greffe de la cour administrative d'appel de Douai le 24 août 2010, ultérieurement régularisé par la production le 30 août 2010 du mémoire signé, a ainsi été formé avant l'expiration du délai d'appel ; qu'il n'est, dès lors, pas tardif ; <br/>
<br/>
              Sur la légalité de la décision attaquée : <br/>
<br/>
              8. Considérant qu'il ressort des pièces du dossier et qu'il n'est pas contesté qu'à la date de l'arrêté attaqué, la commune de Vauxbuin comptait moins de 10 000 habitants et ne faisait pas partie d'un ensemble aggloméré de plusieurs communes comptant plus de 100 000 habitants ; que, par suite, il résulte de ce qui a été dit ci-dessus que les dispositifs publicitaires mentionnés à l'article R. 581-23 du code de l'environnement étaient interdits sur son territoire en application des dispositions de cet article ;<br/>
<br/>
              9. Considérant qu'il résulte de ce qui précède que c'est à tort que le tribunal administratif d'Amiens s'est fondé, pour annuler l'arrêté attaqué, sur ce que le préfet de l'Aisne ne pouvait pas légalement se fonder sur l'article R. 581-23 du code de l'environnement pour mettre la société Avenir en demeure de retirer le dispositif publicitaire implanté entre les numéros 79 et 109 route de Paris à Vauxbuin ; <br/>
<br/>
              10. Considérant, toutefois, qu'il appartient au Conseil d'Etat, saisi de l'ensemble du litige par l'effet dévolutif de l'appel, d'examiner les autres moyens soulevés par la société Avenir devant le tribunal administratif d'Amiens ; <br/>
<br/>
              11. Considérant que, pour mettre en demeure cette société de supprimer le dispositif publicitaire qu'elle avait installé sur le territoire de la commune de Vauxbuin, le préfet s'est borné à constater la méconnaissance des dispositions de l'article R. 581-23 du code de l'environnement, sans avoir à porter une appréciation sur les faits de l'espèce ; qu'en application des dispositions de l'article L. 581-27 du même code, il était tenu, après avoir constaté cette violation, de mettre en demeure la requérante de retirer ce panneau ; qu'ainsi, les moyens tirés de ce qu'il n'aurait pas mis la société Avenir à même de présenter ses observations préalablement à l'intervention de l'arrêté attaqué et de ce que cet arrêté serait insuffisamment motivé sont inopérants et ne peuvent conduire à l'annulation de l'arrêté attaqué ;<br/>
<br/>
              12. Considérant qu'il résulte de tout ce qui précède que le ministre est fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif d'Amiens a annulé l'arrêté du préfet de l'Aisne du 20 novembre 2007 mettant en demeure la société Avenir d'enlever le dispositif publicitaire en cause ;<br/>
<br/>
              13. Considérant, enfin, que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'il soit fait droit aux conclusions présentées par la société JC Decaux France au titre des frais exposés et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Douai du 19 juillet 2011 et le jugement du tribunal administratif d'Amiens du 20 mai 2010 sont annulés.<br/>
<br/>
Article 2 : La demande présentée par la société Avenir devant le tribunal administratif d'Amiens est rejetée.<br/>
<br/>
Article 3 : Les conclusions de la société JC Decaux France présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la ministre de l'écologie, du développement durable et de l'énergie et à la société JC Decaux.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">02-01-04-02-03 AFFICHAGE ET PUBLICITÉ. AFFICHAGE. RÉGIME DE LA LOI DU 29 DÉCEMBRE 1979. DISPOSITIONS APPLICABLES À LA PUBLICITÉ. PUBLICITÉ À L'INTÉRIEUR DES AGGLOMÉRATIONS. - NOTION D'AGGLOMÉRATION - APPRÉHENSION À L'INTÉRIEUR DU TERRITOIRE D'UNE SEULE COMMUNE - CONSÉQUENCE - SEUIL DE 10 000 HABITANTS FIXÉ PAR L'ARTICLE R. 581-23 DU CODE DE L'ENVIRONNEMENT - APPRÉCIATION AU NIVEAU DE LA COMMUNE ISOLÉMENT.
</SCT>
<ANA ID="9A"> 02-01-04-02-03 Pour l'application des dispositions des articles L. 581-7, L. 581-9 et R. 581-23 du code de l'environnement, ainsi que de l'article R. 110-2 du code de la route, la notion d'agglomération, qui doit être entendue comme un espace sur lequel sont groupés des immeubles bâtis rapprochés, ne saurait, en l'absence de disposition contraire, être appréhendée qu'à l'intérieur du territoire d'une seule commune. Par suite, le seuil de 10 000 habitants de l'article R. 581-23 du code de l'environnement, qui dispose que les dispositifs publicitaires scellés au sol sont interdits dans les agglomérations de moins de 10 000 habitants qui ne font pas partie d'un ensemble multicommunal de plus de 100 000 habitants doit être apprécié pour chaque commune isolément. Il s'ensuit que les dispositifs publicitaires scellés au sol ne peuvent être admis dans une commune de moins de 10 000 habitants que si elle forme, avec d'autres communes, un ensemble qui dépasse 100 000 habitants.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
