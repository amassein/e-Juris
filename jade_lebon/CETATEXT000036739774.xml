<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036739774</ID>
<ANCIEN_ID>JG_L_2018_03_000000405474</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/73/97/CETATEXT000036739774.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 05/03/2018, 405474</TITRE>
<DATE_DEC>2018-03-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>405474</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR>M. Stéphane Hoynck</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:405474.20180305</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Par une requête, un mémoire complémentaire et un mémoire en réplique, enregistrés les 28 novembre 2016, 7 mars 2017 et 26 janvier 2018 au secrétariat du contentieux du Conseil d'Etat, la CIMADE, service oecuménique d'entraide, demande au Conseil d'Etat :<br/>
<br/>
              1°) avant-dire droit, d'enjoindre au ministre de l'intérieur de communiquer l'instruction du 19 juillet 2016 relative à l'application du règlement (UE) n° 604/2013 dit " Dublin III " ; <br/>
<br/>
              2°) d'annuler pour excès de pouvoir cette instruction ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 1 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu :<br/>
              - le règlement UE n ° 603/2013 du Parlement européen et du Conseil du 26 juin 2013 ; <br/>
              - le règlement UE n° 604/2013 du Parlement européen et du Conseil du 26 juin 2013 ; <br/>
              - la directive 2013/32/UE du Parlement européen et du Conseil du 26 juin 2013 ;<br/>
              - la directive 2013/33/UE du Parlement européen et du Conseil du 26 juin 2013 ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ; <br/>
              - le code des relations entre le public et l'administration ; <br/>
              - la loi n° 2015-925 du 29 juillet 2015 ;<br/>
              - l'arrêt de la Cour de justice de l'Union Européenne du 15 mars 2017, Al Chodor (C-528/15) ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Stéphane Hoynck, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Spinosi, Sureau, avocat de la CIMADE ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              Sur les conclusions avant-dire-droit tendant à la production de la circulaire litigieuse :<br/>
<br/>
              1. Il ressort des pièces du dossier que la circulaire attaquée a été produite par le ministre de l'intérieur postérieurement à l'introduction de la requête. Il n'y a, par suite, pas lieu de statuer sur les conclusions tendant à cette production.<br/>
<br/>
              Sur les conclusions dirigées contre la circulaire du 19 juillet 2016 :<br/>
<br/>
              En ce qui concerne la fin de non-recevoir soulevée par le ministre de l'intérieur :<br/>
<br/>
              2. Aux termes de l'article R. 312-8 du code des relations entre le public et l'administration : " Sans préjudice des autres formes de publication éventuellement applicables à ces actes, les circulaires et instructions adressées par les ministres aux services et établissements de l'Etat sont tenues à la disposition du public sur un site internet relevant du Premier ministre. Elles sont classées et répertoriées de manière à faciliter leur consultation./ Une circulaire ou une instruction qui ne figure pas sur le site mentionné au précédent alinéa n'est pas applicable. Les services ne peuvent en aucun cas s'en prévaloir à l'égard des administrés ". La circonstance qu'une circulaire n'ait pas, contrairement à ce que les dispositions précitées exigent à peine d'inapplicabilité, été mise en ligne sur le site internet créé à cet effet est sans incidence sur la recevabilité du recours pour excès de pouvoir formé contre elle.<br/>
<br/>
              3. L'interprétation que par voie, notamment, de circulaires ou d'instructions l'autorité administrative donne des lois et règlements qu'elle a pour mission de mettre en oeuvre n'est pas susceptible d'être déférée au juge de l'excès de pouvoir lorsque, étant dénuée de caractère impératif, elle ne saurait, quel qu'en soit le bien-fondé, faire grief. En revanche, les dispositions impératives à caractère général d'une circulaire ou d'une instruction doivent être regardées comme faisant grief. La circulaire du 19 juillet 2016, qui précise à l'attention des préfets les conditions dans lesquelles doivent être menées les procédures de détermination de l'Etat responsable de l'examen d'une demande d'asile et de transfert du demandeur d'asile vers cet Etat, comporte des dispositions impératives à caractère général et est, par suite, susceptible de faire l'objet d'un recours pour excès de pouvoir.<br/>
<br/>
              4. Il s'ensuit que la fin de non-recevoir opposée par le ministre de l'intérieur doit être écartée.<br/>
<br/>
              En ce qui concerne les dispositions de la circulaire relatives à l'entretien individuel et à l'assignation à résidence de demandeurs d'asile à l'occasion de la mise en oeuvre de la procédure de détermination de l'Etat responsable de l'examen de la demande d'asile :<br/>
<br/>
              5. En premier lieu, la circulaire attaquée, en prescrivant que l'entretien individuel avec les demandeurs d'asile permette d'identifier, le cas échéant, l'existence de liens familiaux dans d'autres Etats membres, n'a pas pour objet et ne saurait avoir pour effet, contrairement à ce qui est soutenu, de faire obstacle à ce que le demandeur d'asile fasse valoir, au cours de cet entretien, les liens familiaux qui existeraient en France. <br/>
<br/>
              6. En deuxième lieu, contrairement à ce qui est soutenu, les dispositions de la circulaire relatives à la nécessité de recourir à un interprète afin que l'entretien individuel soit mené dans une langue que comprend le demandeur d'asile n'ont pas pour objet et ne saurait en avoir pour effet de mettre les éventuels frais d'interprétariat à la charge du demandeur d'asile.<br/>
<br/>
              7. En troisième lieu, l'article L. 742-2 du code de l'entrée et du séjour des étrangers et du droit d'asile, qui prévoit la possibilité de prononcer, sous certaines conditions, l'assignation à résidence d'un demandeur d'asile dispose, à son deuxième alinéa que : "La décision d'assignation à résidence est motivée. Elle peut être prise pour une durée maximale de six mois et renouvelée une fois dans la même limite de durée, par une décision également motivée ". Contrairement à ce qui est soutenu, ces dispositions ne méconnaissent en rien celles des articles 21 et 22 du règlement (UE) n° 604/2013 du Parlement européen et du Conseil du 26 juin 2013, établissant les critères et mécanismes de détermination de l'Etat membre responsable de l'examen d'une demande de protection internationale introduite dans l'un des Etats membres par un ressortissant de pays tiers ou un apatride (refonte), dit  " Dublin III ", qui traitent et fixent les délais d'instruction d'une demande aux fins de prise en charge. Elles se bornent à fixer la durée maximale d'une mesure d'assignation à résidence qu'il incombe à l'autorité administrative de prendre, sous le contrôle du juge, pour une durée limitée à ce qu'implique nécessairement le déroulement de la procédure de détermination de l'Etat membre responsable de l'examen de la demande d'asile et de transfert du demandeur d'asile dans cet Etat. <br/>
<br/>
              En ce qui concerne les dispositions de la circulaire relatives au placement en rétention administrative de demandeurs d'asile à l'occasion de la mise en oeuvre de la procédure de détermination de l'Etat responsable de l'examen de la demande d'asile :<br/>
<br/>
              8. Il résulte des dispositions combinées de l'article L. 551-1 et du 1° de l'article L. 561-2 du code de l'entrée et du séjour des étrangers et du droit d'asile que l'autorité administrative peut placer en rétention administrative l'étranger qui fait l'objet d'une décision de transfert en application de l'article L. 742-3 du même code après l'intervention de la décision de transfert et dans le seul cas où il ne présente pas de garanties de représentation effectives propres à prévenir le risque de fuite mentionné au 3° du II de l'article L. 511-1. <br/>
<br/>
              9. Or la Cour de justice de l'Union européenne a dit pour droit, dans son arrêt du 15 mars 2017, Al Chodor (C-528/15), que : " L'article 2, sous n), et l'article 28, paragraphe 2, du règlement (UE) n° 604/2013 du Parlement européen et du Conseil, du 26 juin 2013, établissant les critères et mécanismes de détermination de l'État membre responsable de l'examen d'une demande de protection internationale introduite dans l'un des États membres par un ressortissant de pays tiers ou un apatride, lus conjointement, doivent être interprétés en ce sens qu'ils imposent aux États membres de fixer, dans une disposition contraignante de portée générale, les critères objectifs sur lesquels sont fondées les raisons de craindre la fuite du demandeur d'une protection internationale qui fait l'objet d'une procédure de transfert. L'absence d'une telle disposition entraîne l'inapplicabilité de l'article 28, paragraphe 2, de ce règlement ". <br/>
<br/>
              10. Dès lors que les cas retenus par l'article L. 511-1 du code de l'entrée et du séjour des étrangers et du droit d'asile pour caractériser un risque de fuite ne sauraient être regardés, ainsi que l'a d'ailleurs jugé la Cour de cassation dans son arrêt n° 17-15.160 du 27 septembre 2017, comme valant définition des raisons de craindre la fuite du demandeur d'une protection internationale qui fait l'objet d'une procédure de transfert, le placement en rétention administrative d'un demandeur d'asile pour lequel a été engagée une procédure aux fins de remise, n'est pas, en l'état du droit, légalement possible au regard des exigences attachées au respect du règlement " Dublin III ", tel qu'interprété par la Cour de justice. Il s'ensuit que les dispositions de la circulaire attaquée qui prescrivent, en cas de risque de fuite, le placement en rétention d'un demandeur d'asile faisant l'objet d'une procédure de transfert doivent être annulées.<br/>
<br/>
              11. Les dispositions de la circulaire qui prescrivent aux préfets, lorsque le risque de fuite est caractérisé, d'en informer l'Office français de l'immigration et de l'intégration doivent être annulées par voie de conséquence. <br/>
<br/>
              En ce qui concerne les dispositions de la circulaire relatives à l'application de la procédure du règlement " Dublin III " aux personnes placées en rétention administrative qui ont la qualité de demandeur d'asile :<br/>
<br/>
              12. En premier lieu, aux termes de l'article L. 556-1 du code de l'entrée et du séjour des étrangers et du droit d'asile : " Lorsqu'un étranger placé en rétention en application de l'article L. 551-1 présente une demande d'asile, l'autorité administrative peut, si elle estime, sur le fondement de critères objectifs, que cette demande est présentée dans le seul but de faire échec à l'exécution de la mesure d'éloignement, maintenir l'intéressé en rétention le temps strictement nécessaire à l'examen de sa demande d'asile par l'Office français de protection des réfugiés et apatrides et, en cas de décision de rejet ou d'irrecevabilité de celui-ci, dans l'attente de son départ. Cette décision de maintien en rétention n'affecte ni le contrôle du juge des libertés et de la détention exercé sur la décision de placement en rétention en application de l'article L. 512-1 ni sa compétence pour examiner la prolongation de la rétention en application du chapitre II du titre V du livre V. La décision de maintien en rétention est écrite et motivée. A défaut d'une telle décision, il est immédiatement mis fin à la rétention et l'autorité administrative compétente délivre à l'intéressé l'attestation mentionnée à l'article L. 741-1. / L'étranger peut demander au président du tribunal administratif l'annulation de la décision de maintien en rétention dans les quarante-huit heures suivant sa notification pour contester les motifs retenus par l'autorité administrative pour estimer que sa demande d'asile a été présentée dans le seul but de faire échec à l'exécution de la mesure d'éloignement. (...) / En cas d'annulation de la décision de maintien en rétention, il est immédiatement mis fin à la rétention et l'autorité administrative compétente délivre à l'intéressé l'attestation mentionnée à l'article L. 741-1 ". Il résulte de ces dispositions qu'il doit, en principe, être mis fin à la rétention administrative d'un étranger qui formule une demande d'asile. Toutefois, l'administration peut maintenir l'intéressé en rétention, par une décision écrite et motivée, dans le cas où elle estime que sa demande d'asile a été présentée dans le seul but de faire échec à l'exécution de la mesure d'éloignement prise à son encontre. L'engagement d'une procédure de détermination de l'État membre responsable de l'examen de la demande d'asile ne constitue pas, par lui-même, un motif de nature à justifier le maintien en rétention administrative de l'intéressé.<br/>
<br/>
              13. Il s'ensuit que les dispositions de la circulaire attaquée qui prescrivent, sans autre précision, le maintien en rétention de l'étranger qui y a formulé une demande d'asile au seul motif qu'il fait l'objet d'une procédure de transfert doivent être annulées. Les dispositions de la circulaire qui indiquent que, lorsque l'étranger placé en rétention administrative fait une demande d'asile pour laquelle un autre Etat membre est requis, le dossier de demande d'asile rempli par l'étranger ne doit pas être transmis à l'OFPRA tant que la détermination de l'Etat responsable est en cours doivent être annulées par voie de conséquence.<br/>
<br/>
              14. En second lieu, la circulaire attaquée prescrit aux préfets, lorsqu'un étranger en situation irrégulière fait l'objet d'un placement en rétention, de vérifier préalablement à l'exécution de la mesure d'éloignement envisagée qu'il n'a pas auparavant introduit une demande de protection internationale dans un autre État membre de l'Union européenne, dans les cas prévus par l'article 17 du règlement (UE) n° 603/2013 du Parlement européen et du Conseil, du 26 juin 2013, relatif à la création d'Eurodac. Elle indique, en outre, que s'il apparaît que l'étranger est connu comme demandeur d'asile dans un autre Etat membre, il convient, d'une part, de mettre en oeuvre la procédure de prise en charge par cet Etat au titre du règlement " Dublin III " et, d'autre part, de ne pas procéder à l'éloignement forcé de l'étranger vers son pays d'origine. Outre ces recommandations, qui traduisent exactement les exigences attachées au respect du droit d'asile, la circulaire prescrit, dans pareil cas, le maintien en rétention du demandeur d'asile. Dès lors que, dans une telle hypothèse, un placement n'est légalement possible qu'après l'intervention de la décision de transfert et en cas de risque de fuite, sous réserve de la définition par la loi nationale d'un tel risque, les dispositions qui prescrivent le maintien en rétention administrative doivent être annulées.<br/>
<br/>
              15. Il résulte de tout ce qui précède que la CIMADE est fondée à demander l'annulation de la circulaire attaquée en tant, d'une part, qu'elle prescrit, à son I, le placement en rétention de demandeurs d'asile faisant l'objet d'une décision de transfert et le signalement des cas de fuite à l'Office français de l'immigration et de l'intégration et, d'autre part, qu'elle prescrit, à son II et dans son annexe, le maintien en rétention des étrangers ayant formulé une demande d'asile ou dont il est apparu qu'ils en avaient formulé une dans un autre Etat membre. Il y a lieu dans les circonstances de l'espèce de mettre à la charge de l'Etat le versement à la CIMADE de la somme de 1 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Les dispositions du I de l'instruction du 19 juillet 2016 relative à l'application du règlement (UE) n° 604/2013 dit " Dublin III " en tant qu'elles prescrivent le placement en rétention  d'un étranger qui fait l'objet d'une décision de transfert et qu'elles prescrivent aux préfets de signaler les cas de fuite à l'Office français de l'immigration et de l'intégration, ainsi que les dispositions du II de cette instruction et de son annexe qui prescrivent le maintien en rétention des étrangers ayant formulé une demande d'asile ou dont il est apparu qu'ils en avaient formulé une dans un autre Etat membre sont annulées.<br/>
Article 2 : L'Etat versera à la CIMADE la somme de 1 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 3 : Le surplus des conclusions de la requête de la CIMADE est rejeté.<br/>
Article 4 : La présente décision sera notifiée à la CIMADE et au ministre d'Etat, ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">095-02-03 - POSSIBILITÉ DE PLACEMENT EN RÉTENTION ADMINISTRATIVE DES DEMANDEURS D'ASILE FAISANT L'OBJET D'UNE PROCÉDURE DE TRANSFERT (2 DE L'ART. 28 DU RÈGLEMENT DUBLIN III) - ABSENCE EN L'ÉTAT DU DROIT, FAUTE DE DÉFINITION DU RISQUE DE FUITE DES DEMANDEURS D'ASILE FAISANT L'OBJET D'UNE TELLE PROCÉDURE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">15-05-045-05 COMMUNAUTÉS EUROPÉENNES ET UNION EUROPÉENNE. RÈGLES APPLICABLES. - POSSIBILITÉ DE PLACEMENT EN RÉTENTION ADMINISTRATIVE DES DEMANDEURS D'ASILE FAISANT L'OBJET D'UNE PROCÉDURE DE TRANSFERT (2 DE L'ART. 28 DU RÈGLEMENT DUBLIN III) - ABSENCE EN L'ÉTAT DU DROIT, FAUTE DE DÉFINITION DU RISQUE DE FUITE DES DEMANDEURS D'ASILE FAISANT L'OBJET D'UNE TELLE PROCÉDURE [RJ1].
</SCT>
<ANA ID="9A"> 095-02-03 Dès lors que les cas retenus par l'article L. 511-1 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA) pour caractériser un risque de fuite ne sauraient être regardés, ainsi que l'a d'ailleurs jugé la Cour de cassation dans son arrêt n°17-15.160 du 27 septembre 2017, comme valant définition des raisons de craindre la fuite du demandeur d'une protection internationale qui fait l'objet d'une procédure de transfert, le placement en rétention administrative d'un demandeur d'asile pour lequel a été engagée une procédure aux fins de remise, n'est pas, en l'état du droit, légalement possible au regard des exigences attachées au respect du règlement (UE) n° 604/2013 du 26 juin 2013 dit Dublin III, tel qu'interprété par la Cour de justice de l'Union européenne.</ANA>
<ANA ID="9B"> 15-05-045-05 Dès lors que les cas retenus par l'article L. 511-1 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA) pour caractériser un risque de fuite ne sauraient être regardés, ainsi que l'a d'ailleurs jugé la Cour de cassation dans son arrêt n°17-15.160 du 27 septembre 2017, comme valant définition des raisons de craindre la fuite du demandeur d'une protection internationale qui fait l'objet d'une procédure de transfert, le placement en rétention administrative d'un demandeur d'asile pour lequel a été engagée une procédure aux fins de remise, n'est pas, en l'état du droit, légalement possible au regard des exigences attachées au respect du règlement (UE) n° 604/2013 du 26 juin 2013 dit Dublin III, tel qu'interprété par la Cour de justice de l'Union européenne.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr. CJUE, 15 mars 2017,,e.a., aff. C-528/15 ; Cass. civ. 1ère, 27 septembre 2017, n° 17-15.160.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
