<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030926103</ID>
<ANCIEN_ID>JG_L_2015_07_000000387236</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/92/61/CETATEXT000030926103.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème / 5ème SSR, 22/07/2015, 387236</TITRE>
<DATE_DEC>2015-07-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>387236</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème / 5ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN ; SCP GASCHIGNARD</AVOCATS>
<RAPPORTEUR>M. Laurent Huet</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Gaëlle Dumortier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:387236.20150722</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme C...E...a demandé au tribunal administratif de Melun d'annuler l'élection de Mme D...A...en qualité d'adjointe au maire de la commune de Saint-Maur-des-Fossés (Val-de-Marne). Par un jugement n° 1409160 du 12 décembre 2014, le tribunal administratif de Melun a annulé l'élection de MmeA....<br/>
<br/>
              Par une requête sommaire et un mémoire complémentaire, enregistrés les 19 janvier et 17 février 2015 au secrétariat du contentieux du Conseil d'Etat, Mme A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) de rejeter la protestation de Mme E...;<br/>
<br/>
              3°) de mettre à la charge de Mme E...la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des collectivités territoriales ;<br/>
              - la loi n° 2004-809 du 13 août 2004 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Laurent Huet, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de Mme A...et à la SCP Gaschignard, avocat de Mme E... ; <br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 2122-6 du code général des collectivités territoriales, dans sa rédaction applicable à l'espèce, issue des dispositions de la loi du 13 août 2004 relative aux libertés et responsabilités locales : " Les agents salariés du maire ne peuvent être adjoints si cette activité salariée est directement liée à l'exercice du mandat de maire " ;<br/>
<br/>
              2. Considérant qu'un parlementaire bénéficie de crédits alloués par la chambre à laquelle il appartient afin d'employer des assistants pour l'aider dans l'accomplissement de son mandat ; que si un assistant parlementaire est recruté et employé par le parlementaire en qualité de salarié, il ne saurait, lorsque le parlementaire qui l'emploie détient également un mandat de maire, relever de l'incompatibilité définie par les dispositions citées ci-dessus que s'il a, en réalité, une activité directement liée à l'exercice de ce second mandat ; qu'il ne résulte pas de l'instruction que l'activité salariée de MmeA..., assistante parlementaire de M.B..., député et maire de Saint-Maur-des-Fossés, soit directement liée à l'exercice, par ce dernier, de son mandat de maire ; qu'ainsi, Mme A...est fondée à soutenir que c'est à tort que le tribunal administratif de Melun s'est fondé sur l'incompatibilité de son activité d'assistante parlementaire avec des fonctions de maire-adjoint pour annuler son élection en qualité de maire-adjoint de la commune de Saint-Maur-des-Fossés et, en l'absence de tout autre grief dans la protestation que Mme E...a formée devant le tribunal administratif de Melun, à demander l'annulation de ce jugement et le rejet de cette protestation ; <br/>
<br/>
              3. Considérant que les passages du mémoire en défense présenté par Mme E... dont Mme A...demande la suppression en application de l'article L. 741-2 du code de justice administrative, qui reproduit les dispositions de l'article 41 de la loi du 29 juillet 1881, ne peuvent être regardés comme injurieux ou diffamatoires ; qu'il n'y a pas lieu de faire droit à la demande de Mme A...sur ce point ;  <br/>
<br/>
              4. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par Mme A...au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : Le jugement du tribunal administratif de Melun du 12 décembre 2014 est annulé.<br/>
<br/>
Article 2 : L'élection de Mme A...en qualité d'adjointe au maire de la commune de Saint-Maur-des-Fossés est validée.<br/>
<br/>
Article 3 : La protestation de Mme E...est rejetée.<br/>
<br/>
Article 4 : Les conclusions de Mme A...présentées au titre des dispositions de l'article L. 741-2 du code de justice administrative sont rejetées.<br/>
<br/>
Article 5 : Les conclusions de Mme A...présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 6 : La présente décision sera notifiée à Mme D...A..., à Mme C...E...et au ministre de l'intérieur.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">28-04-07 ÉLECTIONS ET RÉFÉRENDUM. ÉLECTIONS MUNICIPALES. ÉLECTION DES MAIRES ET ADJOINTS. - INCOMPATIBILITÉ ENTRE ADJOINT AU MAIRE ET SALARIÉ DU MAIRE (ART. L. 2122-6 DU CGCT) - ASSISTANT D'UN PARLEMENTAIRE DÉTENANT UN MANDAT DE MAIRE - APPRÉCIATION AU CAS PAR CAS.
</SCT>
<ANA ID="9A"> 28-04-07 Les agents salariés du maire ne peuvent être adjoints si cette activité salariée est directement liée à l'exercice du mandat de maire (art. L. 2122-6 du code général des collectivités territoriales (CGCT)).... ,,Un parlementaire bénéficie de crédits alloués par la chambre à laquelle il appartient afin d'employer des assistants pour l'aider dans l'accomplissement de son mandat. Si un assistant parlementaire est recruté et employé par le parlementaire en qualité de salarié, il ne saurait, lorsque le parlementaire qui l'emploie détient également un mandat de maire, relever de cette incompatibilité  que s'il a, en réalité, une activité directement liée à l'exercice de ce second mandat.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
