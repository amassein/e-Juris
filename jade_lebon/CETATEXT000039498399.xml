<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039498399</ID>
<ANCIEN_ID>JG_L_2019_12_000000427522</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/49/83/CETATEXT000039498399.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 11/12/2019, 427522</TITRE>
<DATE_DEC>2019-12-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>427522</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOUTET-HOURDEAUX</AVOCATS>
<RAPPORTEUR>M. Sébastien Gauthier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie Roussel</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:427522.20191211</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, un mémoire complémentaire et un mémoire en réplique, enregistrés les 31 janvier, 26 avril et 16 octobre 2019 au secrétariat du contentieux du Conseil d'Etat, Mme A... B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret du Président de la République du 4 décembre 2018 portant cessation de ses fonctions de sous-préfète, directrice de cabinet de la préfète du Cher ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 600 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code des relations entre le public et l'administration ;<br/>
              - le décret n° 64-260 du 14 mars 1964 ;<br/>
              - le décret n° 2006-1482 du 29 novembre 2006 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Sébastien Gauthier, maître des requêtes en service extraordinaire,<br/>
<br/>
              - les conclusions de Mme Sophie Roussel, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boutet-Hourdeaux, avocat de Mme B... ; <br/>
<br/>
              Vu la note en délibéré, enregistrée le 26 novembre 2019, présentée par Mme B... ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1.	Par décret du Président de la République en date du 2 août 2016 Mme A... B... a été nommée sous-préfète, directrice de cabinet du préfet de la Charente, sur le fondement du 3° du I de l'article 8 du décret du 14 mars 1964 portant statut des sous-préfets. Un décret du 31 juillet 2018 l'a nommée directrice de cabinet de la préfète du Cher. Le 4 décembre 2018, il a été mis fin à ses fonctions par un décret du Président de la République dont l'intéressée demande l'annulation. <br/>
<br/>
              2.	Aux termes du I de l'article 8 du décret du 14 mars 1964 portant statut des sous-préfets : " I. (...) Peuvent être nommés au choix dans le corps des sous-préfets : 3° Des candidats non fonctionnaires âgés de trente-cinq ans au moins au 1er janvier de l'année considérée, et justifiant de l'exercice, durant huit années au total, d'une ou plusieurs activités professionnelles, remplissant les conditions générales d'accès à la fonction publique et titulaires d'un des diplômes requis pour le concours externe d'entrée à l'Ecole nationale d'administration (... ). " Aux termes du III du même article : " Les sous-préfets recrutés en application du présent article effectuent un stage de deux années. S'ils ont déjà la qualité de fonctionnaire, ils sont placés en position de détachement pour la durée de leur stage. / Lorsqu'ils sont recrutés au titre des dispositions du 3° du I du présent article, les sous-préfets effectuent obligatoirement leur stage dans les fonctions de directeur du cabinet de préfet. / A l'expiration de la période de stage, les sous-préfets recrutés en application du présent article sont soit titularisés, soit réintégrés dans leur corps ou cadre d'emplois d'origine, soit licenciés. "<br/>
<br/>
              3.	En premier lieu, si ces dispositions ne prévoient pas la possibilité de proroger la période de stage de deux ans que doit accomplir, dans les fonctions de directeur du cabinet de préfet, le sous-préfet recruté au titre des dispositions du 3° du I de l'article 8 précité, l'absence de décision prise à l'issue du stage de Mme B... en août 2018 n'a pas eu pour effet de faire bénéficier l'intéressée d'une titularisation tacite. Mme B... a conservé la qualité de stagiaire jusqu'à la date de la décision attaquée, ainsi intervenue à l'issue du stage et non dans le cours de celui-ci. Cette décision, qui n'a pas davantage eu pour objet ou pour effet de prolonger la durée du stage de l'intéressée, ne revêt pas de caractère disciplinaire et n'entre, de ce fait, dans aucune des catégories de décisions qui doivent être motivées, notamment en application de l'article L. 211-2 du code des relations entre le public et l'administration ou qui doivent donner lieu à un entretien préalable. Par suite, le moyen tiré d'un défaut de motivation du décret prononçant la cessation de ses fonctions et du non-respect des droits de la défense ne peut qu'être écarté.  <br/>
<br/>
              4.	En deuxième lieu, l'article 2 du décret du 29 novembre 2006 relatif au Conseil supérieur de l'appui territorial et de l'évaluation (CSATE) dispose, au premier alinéa de son II, que le conseil supérieur de l'appui territorial et de l'évaluation " assiste le ministre de l'intérieur dans l'évaluation, à intervalles réguliers, des préfets et sous-préfets. " L'article 4 du même décret dispose, que " chacun des membres du conseil supérieur, autre que le président, est chargé du suivi d'une circonscription. (...) / Le président du conseil supérieur est responsable des évaluations (...) Il arrête l'évaluation des préfets et, sur proposition des membres du conseil, celle des sous-préfets (...) / Aucun membre ne peut intervenir au titre de l'évaluation d'un fonctionnaire cité au premier alinéa de l'article 2 auprès duquel il a assumé une mission de conseil et de soutien ". Il ressort des pièces du dossier que, lorsqu'elle occupait les fonctions de directrice de cabinet du préfet de la Charente, Mme B... a fait l'objet, à partir notamment d'entretiens avec les membres du corps préfectoral, ses collaborateurs et ses divers correspondants, d'une évaluation qui a eu lieu les 27 et 28 février et 1er mars 2018, cette évaluation ayant été arrêtée par le président du CSATE le 4 avril 2018. M. C..., qui n'a pas participé à cette évaluation, a par la suite été désigné pour accompagner Mme B... dans la suite de son stage, qu'elle a effectué en tant que directrice de cabinet de la préfète du Cher. Dans ce cadre, à l'issue d'un entretien avec l'intéressée qui s'est tenu le 1er octobre 2018, il a estimé qu'un accompagnement renforcé de Mme B... n'était pas nécessaire. Ce faisant, il n'a donc pas procédé à l'évaluation de l'intéressée au sens des dispositions précitées. Par suite, le moyen tiré de ce qu'aurait été méconnue la règle fixée par le dernier alinéa de l'article 4 du décret du 29 novembre 2006 ne peut qu'être écarté.<br/>
<br/>
              5.	En troisième lieu, si Mme B... a été placée en congé maladie à compter du 22 novembre 2018 et se trouvait dans cette position à la date de la décision attaquée, aucun texte ni aucun principe ne faisait obstacle à ce qu'il soit mis fin à ses fonctions et qu'il soit ainsi décidé de la licencier à l'issue de son stage.  <br/>
<br/>
              6.	En quatrième lieu, il ressort des pièces du dossier qu'il a été décidé de ne pas titulariser Mme B... au vu de l'évaluation réalisée à la fin du mois de février 2018. Tout en retenant son investissement et son dynamisme, celle-ci retenait en particulier la nécessité pour l'intéressée de trouver sa place en matière de sécurité publique et de confirmer son potentiel. Faisant par ailleurs état de témoignages, qui ne sont pas sérieusement contestés, sur ses demandes relatives aux différents moyens logistiques devant être mis à sa disposition et sur l'incidence de contingences personnelles sur son activité professionnelle, cette première évaluation a été corroborée par celle qui a été effectuée par la préfète du Cher à la fin du mois de septembre 2018, laquelle a confirmé l'existence de problèmes relationnels de l'intéressé avec ses supérieurs hiérarchiques, ses collaborateurs et ses divers interlocuteurs dans le département, et d'un investissement globalement insuffisant dans ses diverses fonctions. Il ressort des pièces du dossier que le Président de la République, qui ne s'est pas fondé sur des faits matériellement inexacts, n'a pas, au vu de l'ensemble de ces circonstances, commis d'erreur manifeste dans l'appréciation des aptitudes de l'intéressée à exercer, en qualité de titulaire, des fonctions dans le corps des sous-préfets. <br/>
<br/>
              7.	En dernier lieu, le détournement de procédure n'est pas établi.  <br/>
<br/>
              8.	Il résulte de tout ce qui précède que les conclusions tendant à l'annulation pour excès de pouvoir du décret du 4 décembre 2018 ne peuvent être accueillies.<br/>
<br/>
              9.	Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de Mme A... B... est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à Mme A... B... et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-04-03-07-04 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. PRINCIPES GÉNÉRAUX DU DROIT. PRINCIPES INTÉRESSANT L'ACTION ADMINISTRATIVE. GARANTIES DIVERSES ACCORDÉES AUX AGENTS PUBLICS. - LICENCIEMENT D'UN AGENT PUBLIC EN CONGÉ MALADIE - TEXTE OU PRINCIPE L'INTERDISANT - ABSENCE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">36-03-04-01 FONCTIONNAIRES ET AGENTS PUBLICS. ENTRÉE EN SERVICE. STAGE. FIN DE STAGE. - DÉCISION METTANT FIN AUX FONCTIONS D'UN AGENT APRÈS L'ISSUE DE SON STAGE - 1) QUALIFICATION DE LA DÉCISION - REFUS DE TITULARISATION - A) AGENT POUVANT ÊTRE REGARDÉ COMME AYANT ÉTÉ TITULARISÉ TACITEMENT À L'ISSUE DU STAGE - ABSENCE [RJ2] - B) DÉCISION AYANT EU POUR EFFET DE PROLONGER LA DURÉE DU STAGE - ABSENCE - 2) CONSÉQUENCES - DÉCISION REVÊTANT UN CARACTÈRE DISCIPLINAIRE - ABSENCE [RJ3] - EXIGENCES DE MOTIVATION ET D'ENTRETIEN PRÉALABLE - ABSENCE [RJ4].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">36-10-06 FONCTIONNAIRES ET AGENTS PUBLICS. CESSATION DE FONCTIONS. LICENCIEMENT. - LICENCIEMENT D'UN AGENT PUBLIC EN CONGÉ MALADIE - TEXTE OU PRINCIPE L'INTERDISANT - ABSENCE [RJ1].
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">36-10-06-01 FONCTIONNAIRES ET AGENTS PUBLICS. CESSATION DE FONCTIONS. LICENCIEMENT. STAGIAIRES. - DÉCISION METTANT FIN AUX FONCTIONS D'UN AGENT APRÈS L'ISSUE DE SON STAGE - 1) QUALIFICATION DE LA DÉCISION - REFUS DE TITULARISATION - A) AGENT POUVANT ÊTRE REGARDÉ COMME AYANT ÉTÉ TITULARISÉ TACITEMENT À L'ISSUE DU STAGE - ABSENCE [RJ2] - B) DÉCISION AYANT EU POUR EFFET DE PROLONGER LA DURÉE DU STAGE - ABSENCE - 2) CONSÉQUENCES - DÉCISION REVÊTANT UN CARACTÈRE DISCIPLINAIRE - ABSENCE [RJ3] - EXIGENCES DE MOTIVATION ET D'ENTRETIEN PRÉALABLE - ABSENCE [RJ4].
</SCT>
<ANA ID="9A"> 01-04-03-07-04 Si l'intéressée était placée en congé maladie à la date de la décision mettant fin à ses fonctions, aucun texte ni aucun principe ne faisait obstacle à ce qu'il soit mis fin à ses fonctions et qu'il soit ainsi décidé de la licencier à l'issue de son stage.</ANA>
<ANA ID="9B"> 36-03-04-01 1) a) Si les I et III de l'article 8 du décret n° 64-260 du 14 mars 1964 ne prévoient pas la possibilité de proroger la période de stage de deux ans que doit accomplir, dans les fonctions de directeur du cabinet de préfet, le sous-préfet recruté au titre des dispositions du 3° du I, l'absence de décision prise à l'issue du stage de l'intéressée n'a pas eu pour effet de la faire bénéficier d'une titularisation tacite. L'intéressée a conservé la qualité de stagiaire jusqu'à la date de la décision mettant fin à ses fonctions, ainsi intervenue à l'issue du stage et non dans le cours de celui-ci.... ,,b) Cette décision n'a pas davantage eu pour objet ou pour effet de prolonger la durée du stage de l'intéressée.,,,2) Cette décision ne revêt pas de caractère disciplinaire et n'entre, de ce fait, dans aucune des catégories de décisions qui doivent être motivées, notamment en application de l'article L. 211-2 du code des relations entre le public et l'administration ou qui doivent donner lieu à un entretien préalable. Par suite, le moyen tiré d'un défaut de motivation du décret prononçant la cessation de ses fonctions et du non-respect des droits de la défense ne peut qu'être écarté.</ANA>
<ANA ID="9C"> 36-10-06 Si l'intéressée était placée en congé maladie à la date de la décision mettant fin à ses fonctions, aucun texte ni aucun principe ne faisait obstacle à ce qu'il soit mis fin à ses fonctions et qu'il soit ainsi décidé de la licencier à l'issue de son stage.</ANA>
<ANA ID="9D"> 36-10-06-01 1) a) Si les I et III de l'article 8 du décret n° 64-260 du 14 mars 1964 ne prévoient pas la possibilité de proroger la période de stage de deux ans que doit accomplir, dans les fonctions de directeur du cabinet de préfet, le sous-préfet recruté au titre des dispositions du 3° du I, l'absence de décision prise à l'issue du stage de l'intéressée n'a pas eu pour effet de la faire bénéficier d'une titularisation tacite. L'intéressée a conservé la qualité de stagiaire jusqu'à la date de la décision mettant fin à ses fonctions, ainsi intervenue à l'issue du stage et non dans le cours de celui-ci.... ,,b) Cette décision n'a pas davantage eu pour objet ou pour effet de prolonger la durée du stage de l'intéressée.,,,2) Cette décision ne revêt pas de caractère disciplinaire et n'entre, de ce fait, dans aucune des catégories de décisions qui doivent être motivées, notamment en application de l'article L. 211-2 du code des relations entre le public et l'administration ou qui doivent donner lieu à un entretien préalable. Par suite, le moyen tiré d'un défaut de motivation du décret prononçant la cessation de ses fonctions et du non-respect des droits de la défense ne peut qu'être écarté.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., CE, 22 octobre 1993, Chambre de commerce et d'industrie de Digne et des Alpes de Haute-Provence c/ Mme,, n° 122191, T. pp. 579-637-853.,,[RJ2] Rappr., sur l'exigence d'une décision expresse de titularisation en fin de stage, CE, 6 décembre 1999, M.,, n° 198566, T. p. 842 ; s'agissant du renouvellement d'un praticien contractuel au-delà de la période de six ans mentionnée à l'article R. 6152-403 du CSP, CE, 30 juin 2017, M.,, n° 393583, T. pp. 653-809.,,[RJ3] Cf. CE, Section, 13 mai 1932, Sieur,n° 14918, p. 487.,,[RJ4] Cf., s'agissant de l'absence d'obligation de motivation, CE, 29 juillet 1983, Ministre de la justice c/ Mlle,, n° 49641, T. pp. 595-762 ; s'agissant de l'absence d'obligation de mettre l'intéressé à même de faire valoir ses observations ou de prendre connaissance de son dossier, CE, Section, 3 décembre 2003, Syndicat intercommunal de restauration collective, n° 256879, p. 489.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
