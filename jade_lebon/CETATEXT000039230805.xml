<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039230805</ID>
<ANCIEN_ID>JG_L_2019_10_000000423275</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/23/08/CETATEXT000039230805.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 16/10/2019, 423275</TITRE>
<DATE_DEC>2019-10-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>423275</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GASCHIGNARD ; SCP DELVOLVE ET TRICHET</AVOCATS>
<RAPPORTEUR>Mme Christelle Thomas</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Anne Iljic</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:423275.20191016</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...-C... B... a demandé au juge des référés du tribunal administratif de Bastia, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, de suspendre l'exécution de l'arrêté interruptif de travaux pris à son encontre par le maire de Centuri (Haute-Corse) le 5 octobre 2017. <br/>
<br/>
              Par une ordonnance n° 1800744 du 24 juillet 2018, le juge des référés du tribunal administratif de Bastia a rejeté cette demande.<br/>
<br/>
              Par un pourvoi, enregistré le 16 août 2018 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de faire droit à sa demande ; <br/>
<br/>
              3°) de mettre à la charge de la commune de Centuri la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Christelle Thomas, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Anne Iljic, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Delvolvé et Trichet, avocat de M. A...-marc B... et à la SCP Gaschignard, avocat de la commune de Centuri ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge des référés qu'à la suite de l'établissement d'un procès-verbal constatant la réalisation de travaux non conformes au permis de construire qui avait été délivré le 10 août 2016, le maire de Centuri a pris à l'encontre de M. B..., le 5 octobre 2017, un arrêté ordonnant l'interruption des travaux. M. B... se pourvoit en cassation contre l'ordonnance du 24 juillet 2018 par laquelle le juge des référés du tribunal administratif de Bastia a rejeté sa demande tendant à ce que soit ordonnée, sur le fondement de l'article L. 521-1 du code de justice administrative, la suspension de l'exécution de cet arrêté.<br/>
<br/>
              2. L'article L. 480-2 du code de l'urbanisme dispose que : " (...) Dès qu'un procès-verbal relevant l'une des infractions prévues à l'article L. 480-4 du présent code a été dressé, le maire peut également, si l'autorité judiciaire ne s'est pas encore prononcée, ordonner par arrêté motivé l'interruption des travaux. (...) Dans le cas de constructions sans permis de construire ou d'aménagement sans permis d'aménager, ou de constructions ou d'aménagement poursuivis malgré une décision de la juridiction administrative suspendant le permis de construire ou le permis d'aménager, le maire prescrira par arrêté l'interruption des travaux ainsi que, le cas échéant, l'exécution, aux frais du constructeur, des mesures nécessaires à la sécurité des personnes ou des biens ; copie de l'arrêté du maire est transmise sans délai au ministère public (...) ".<br/>
<br/>
              3. Il ressort des pièces du dossier soumis au juge des référés que, après l'intervention le 5 octobre 2017 d'un arrêté du maire de Centuri ordonnant l'interruption des travaux entrepris par M. B... au motif que ces travaux étaient effectués en méconnaissance du permis de construire initial délivré à l'intéressé le 10 août 2016, le maire a délivré à M. B..., par un arrêté du 27 novembre 2017, un permis de construire modificatif régularisant au moins partie des travaux en cause. L'intervention du permis de construire modificatif a eu implicitement mais nécessairement pour effet d'abroger l'arrêté ordonnant l'interruption des travaux. Il s'ensuit que la demande de référé tendant à la suspension de l'exécution de l'arrêté interruptif de travaux, présentée alors que cet arrêté devait être regardé comme implicitement abrogé, était dépourvue d'objet et, en conséquence, irrecevable. Ce motif, qui repose sur des faits constants, doit être substitué au motif retenu par l'ordonnance attaquée dont il justifie le dispositif.<br/>
<br/>
              4. Il résulte de ce qui précède que M. B... n'est pas fondé à demander l'annulation de l'ordonnance qu'il attaque.<br/>
<br/>
              5. Lorsqu'il exerce le pouvoir d'interruption des travaux qui lui est attribué par l'article L. 480-2 du code de l'urbanisme, cité au point 2, le maire agit en qualité d'autorité de l'Etat. Ainsi, la commune de Centuri n'est pas partie à la présente instance au sens des dispositions de l'article L. 761-1 du code de justice administrative. Par suite, ces dispositions font obstacle à ce que soit mise à la charge de M. B... la somme que la commune demande au titre des frais exposés par elle et non compris dans les dépens. De même, elles font en tout état de cause obstacle à ce que soit mise à la charge de la commune la somme que M. B... demande au même titre.<br/>
<br/>
<br/>
<br/>
<br/>
                              D E C I D E :<br/>
                              --------------<br/>
Article 1er : Le pourvoi de M. B... est rejeté.<br/>
Article 2 : Les conclusions présentées par M. B... et par la commune de Centuri au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à M. A...-C... B... et à la ministre de la cohésion des territoires et des relations avec les collectivités territoriales. <br/>
Copie en sera adressée à la commune de Centuri. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-09-02-02 ACTES LÉGISLATIFS ET ADMINISTRATIFS. DISPARITION DE L'ACTE. ABROGATION. ABROGATION DES ACTES NON RÉGLEMENTAIRES. - ARRÊTÉ D'INTERRUPTION DES TRAVAUX (L. 480-2 DU CODE DE L'URBANISME) - INTERVENTION D'UN PERMIS DE CONSTRUIRE MODIFICATIF RÉGULARISANT LES TRAVAUX LITIGIEUX - CONSÉQUENCE - ABROGATION IMPLICITE DE CET ARRÊTÉ.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-035-02-02 PROCÉDURE. PROCÉDURES INSTITUÉES PAR LA LOI DU 30 JUIN 2000. RÉFÉRÉ SUSPENSION (ART. L. 521-1 DU CODE DE JUSTICE ADMINISTRATIVE). RECEVABILITÉ. - ARRÊTÉ D'INTERRUPTION DES TRAVAUX (L. 480-2 DU CODE DE L'URBANISME) - INTERVENTION D'UN PERMIS DE CONSTRUIRE MODIFICATIF RÉGULARISANT LES TRAVAUX LITIGIEUX - CONSÉQUENCES - ABROGATION IMPLICITE DE CET ARRÊTÉ - IRRECEVABILITÉ DU RÉFÉRÉ INTRODUIT POSTÉRIEUREMENT CONTRE CET ARRÊTÉ.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">68-03-05-02 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PERMIS DE CONSTRUIRE. CONTRÔLE DES TRAVAUX. INTERRUPTION DES TRAVAUX. - ARRÊTÉ D'INTERRUPTION DES TRAVAUX (L. 480-2 DU CODE DE L'URBANISME) - INTERVENTION D'UN PERMIS DE CONSTRUIRE MODIFICATIF RÉGULARISANT LES TRAVAUX LITIGIEUX - CONSÉQUENCES - ABROGATION IMPLICITE DE CET ARRÊTÉ - IRRECEVABILITÉ DU RÉFÉRÉ SUSPENSION INTRODUIT POSTÉRIEUREMENT CONTRE CET ARRÊTÉ.
</SCT>
<ANA ID="9A"> 01-09-02-02 Maire ordonnant l'interruption des travaux entrepris par le requérant au motif que ceux-ci étaient effectués en méconnaissance du permis de construire. Maire délivrant ensuite un permis de construire modificatif régularisant au moins une partie des travaux en cause.... ,,L'intervention du permis de construire modificatif a eu implicitement mais nécessairement pour effet d'abroger l'arrêté ordonnant l'interruption des travaux.</ANA>
<ANA ID="9B"> 54-035-02-02 Maire ordonnant l'interruption des travaux entrepris par le requérant au motif que ceux-ci étaient effectués en méconnaissance du permis de construire. Maire délivrant ensuite un permis de construire modificatif régularisant au moins une partie des travaux en cause.... ,,L'intervention du permis de construire modificatif a eu implicitement mais nécessairement pour effet d'abroger l'arrêté ordonnant l'interruption des travaux.... ,,Il s'ensuit que la demande de référé tendant à la suspension de l'exécution de l'arrêté interruptif de travaux, présentée alors que cet arrêté devait être regardé comme implicitement abrogé, était dépourvue d'objet et, en conséquence, irrecevable.</ANA>
<ANA ID="9C"> 68-03-05-02 Maire ordonnant l'interruption des travaux entrepris par le requérant au motif que ceux-ci étaient effectués en méconnaissance du permis de construire. Maire délivrant ensuite un permis de construire modificatif régularisant au moins une partie des travaux en cause.... ,,L'intervention du permis de construire modificatif a eu implicitement mais nécessairement pour effet d'abroger l'arrêté ordonnant l'interruption des travaux.... ,,Il s'ensuit que la demande de référé tendant à la suspension de l'exécution de l'arrêté interruptif de travaux, présentée alors que cet arrêté devait être regardé comme implicitement abrogé, était dépourvue d'objet et, en conséquence, irrecevable.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
