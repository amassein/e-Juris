<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033255697</ID>
<ANCIEN_ID>JG_L_2016_10_000000400172</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/25/56/CETATEXT000033255697.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 17/10/2016, 400172</TITRE>
<DATE_DEC>2016-10-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>400172</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ ; SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>Mme Charline Nicolas</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:400172.20161017</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société MLR Plage a saisi le juge des référés du tribunal administratif de Toulon, d'une demande tendant, sur le fondement des dispositions de l'article L. 551-1 du code de justice administratif, à l'annulation, au stade de la sélection des candidatures, de la procédure de délégation de service public engagée par la commune d'Hyères-les-Palmiers ayant pour objet l'exploitation des bains de mer sur l'emplacement de plagiste sis 1550 boulevard de la Marine. <br/>
<br/>
              Par une ordonnance n° 1601234 du 12 mai 2016, le juge des référés du tribunal administratif de Toulon a annulé la procédure de délégation de service public engagée par la commune d'Hyères-les-Palmiers au stade de la sélection des candidatures, la délibération du conseil municipal de la commune d'Hyères-les-Palmiers ayant attribué ladite délégation de service public à la société Le Petit Bain ainsi que toute décision de la commune se rapportant à la passation dudit contrat, et a rejeté le surplus des conclusions. <br/>
<br/>
              1° Sous le n° 400172, par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique enregistrés les 27 mai, 13 juin et 22 septembre 2016 au secrétariat du contentieux du Conseil d'Etat, la commune d'Hyères-les-Palmiers demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de rejeter la demande de la société MLR Plage ;<br/>
<br/>
              3°) de mettre à la charge de la société MLR Plage la somme de 7 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              2° Sous le n° 400175, par un pourvoi sommaire et deux mémoires complémentaires, enregistrés les 27 mai, 13 juin et 23 juin 2016 au secrétariat du contentieux du Conseil d'Etat, Mme E...D...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de rejeter la demande de la société MLR Plage ;<br/>
<br/>
              3°) de mettre à la charge de la société MLR Plage la somme de 3 800 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - le code général des collectivités territoriales ;<br/>
              - le code des relations entre le public et l'administration ; <br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Charline Nicolas, auditeur,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de la commune d'Hyères-les-Palmiers, à la SCP Rocheteau, Uzan-Sarano, avocat de la société MLR plage et à la SCP Célice, Soltner, Texidor, Périer, avocat de Mme D...;<br/>
<br/>
<br/>
<br/>1. Considérant que les pourvois de la commune d'Hyères-les-Palmiers et de MmeD..., agissant au nom de la société en cours de formation " Le Petit Bain ", sont dirigés contre la même ordonnance ; qu'il y a lieu de les joindre pour statuer par une seule décision ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 551-1 du code de justice administrative : " Le président du tribunal administratif, ou le magistrat qu'il délègue, peut être saisi en cas de manquement aux obligations de publicité et de mise en concurrence auxquelles est soumise la passation par les pouvoirs adjudicateurs de contrats administratifs ayant pour objet l'exécution de travaux, la livraison de fournitures ou la prestation de services, avec une contrepartie économique constituée par un prix ou un droit d'exploitation, ou la délégation d'un service public (...) " ;<br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier soumis au juge des référés du tribunal administratif de Toulon que la commune d'Hyères-les-Palmiers a engagé une procédure de passation d'une délégation de service public portant sur l'exploitation des bains de mer sur l'emplacement de plagiste sis 1550 boulevard de la Marine ; que l'offre de la société Le Petit Bain a été retenue ; que, par une ordonnance du 12 mai 2016, contre laquelle la commune de Hyères-les-Palmiers et Mme D...se pourvoient en cassation, le juge du référé précontractuel a, sur la demande de la société MLR Plage, annulé cette procédure à compter de la phase d'examen des candidatures ; <br/>
<br/>
              4. Considérant qu'aux termes de l'article L. 1411-1 du code général des collectivités territoriales, dans sa rédaction alors applicable : " Une délégation de service public est un contrat par lequel une personne morale de droit public confie la gestion d'un service public dont elle a la responsabilité à un délégataire public ou privé, dont la rémunération est substantiellement liée aux résultats de l'exploitation du service. Le délégataire peut être chargé de construire des ouvrages ou d'acquérir des biens nécessaires au service./ Les délégations de service public des personnes morales de droit public relevant du présent code sont soumises par l'autorité délégante à une procédure de publicité permettant la présentation de plusieurs offres concurrentes, dans des conditions prévues par un décret en Conseil d'Etat. Les garanties professionnelles sont appréciées notamment dans la personne des associés et au vu des garanties professionnelles réunies en son sein. Les sociétés en cours de constitution ou nouvellement créées peuvent être admises à présenter une offre dans les mêmes conditions que les sociétés existantes./ La commission mentionnée à l'article L. 1411-5 dresse la liste des candidats admis à présenter une offre après examen de leurs garanties professionnelles et financières, de leur respect de l'obligation d'emploi des travailleurs handicapés prévue aux articles L. 5212-1 à L. 5212-4 du code du travail et de leur aptitude à assurer la continuité du service public et l'égalité des usagers devant le service public " ; <br/>
<br/>
              5. Considérant, en premier lieu, qu'il appartient au juge du référé précontractuel, lorsque est invoqué devant lui le secret commercial et industriel, et s'il l'estime indispensable pour forger sa conviction sur les points en litige, d'inviter la partie qui s'en prévaut à lui procurer tous les éclaircissements nécessaires sur la nature des pièces écartées et sur les raisons de leur exclusion ; qu'il lui revient, si ce secret lui est opposé à tort, d'enjoindre à la collectivité de produire les pièces en cause et de tirer les conséquences, le cas échéant, de son abstention ; que, pour annuler partiellement la procédure litigieuse, le juge du référé précontractuel a accueilli le moyen tiré de ce que la commission de délégation de service public aurait dû écarter la candidature de la société Le Petit Bain, au motif que celle-ci ne justifiait pas des garanties professionnelles et financières requises, en se bornant à relever que les mentions chiffrées afférentes à ces garanties, figurant dans les documents relatifs à l'analyse de la candidature de la société qui lui avaient été transmis par la commune, avaient été occultées, alors, selon lui, qu'elles ne relevaient pas du secret commercial ou industriel ; que, toutefois, la seule circonstance que la commune ait cru devoir, devant le juge, occulter des éléments chiffrés portant sur la société attributaire, afin d'éviter qu'ils ne soient versés aux débats dans le cadre de la procédure contradictoire et qu'il soit ainsi porté atteinte au secret des affaires, ne pouvait, à supposer même que l'analyse de la collectivité ait été erronée quant à l'applicabilité de ce secret en l'espèce, être regardée comme établissant, par elle-même, le caractère insuffisant des garanties offertes par la société ; que les requérantes sont, par suite, fondées à soutenir que le premier motif de l'ordonnance attaquée est entaché d'erreur de droit ; <br/>
<br/>
              6. Considérant, en second lieu, que l'ordonnance attaquée repose sur un second motif, tiré de ce que la candidature de la société Le Petit Bain était irrégulière, en l'absence de production du pouvoir des signataires des attestations bancaires ; qu'en se fondant sur un tel constat, alors qu'il ressortait des pièces du dossier qui lui était soumis, en particulier du procès-verbal de la séance du 5 février 2016 de la commission de délégation de service public, et qu'il n'était d'ailleurs pas contesté devant lui, que ce document avait bien été transmis par la société Le Petit Bain après que la commune eut demandé aux candidats de fournir les pièces manquantes, le juge du référé précontractuel a entaché son ordonnance de dénaturation ;  <br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que la commune d'Hyères-les-Palmiers et Mme D... sont fondées à demander l'annulation de l'ordonnance contestée du 12 mai 2016 ;<br/>
<br/>
              8. Considérant que, dans les circonstances de l'espèce, il y a lieu, en application de l'article L. 821-2 du code de justice administrative, de régler l'affaire au titre de la procédure de référé engagée ;<br/>
<br/>
              9. Considérant, en premier lieu, qu'il résulte de l'instruction que M.B..., membre titulaire de la commission de délégation de service public et MmeC..., trésorière municipale, ont régulièrement été convoqués aux réunions des 5 janvier, 5 février et 22 février 2016 de la commission et que MmeA..., représentante du ministre chargé de la concurrence, a régulièrement été convoquée aux réunions des 5 janvier et 22 février 2016 de cette même commission ; que dès lors, le moyen tiré de la composition irrégulière de la commission de service public lors de ces réunions, au motif que certains membres qui n'y ont pas siégé n'avaient pas été convoqués, ne peut, en tout état de cause, qu'être écarté ; <br/>
<br/>
              10. Considérant, en deuxième lieu, qu'il ne résulte pas de l'instruction, et en particulier de l'extrait du procès-verbal de la réunion de la commission de délégation de service public du 25 janvier 2016 intéressant la candidature de la société Le Petit Bain et produit par la commune d'Hyères-les-Palmiers sans occultation après autorisation de la société Le Petit Bain, que le pouvoir adjudicateur ait, en l'espèce, commis une erreur manifeste d'appréciation en estimant que les garanties professionnelles et financières de cette société étaient suffisantes ; que, par suite, la société MRL Plage n'est pas fondée à soutenir que la candidature de la société Le Petit Bain aurait dû, pour ce motif, être rejetée ;<br/>
<br/>
              11. Considérant, en troisième lieu, que le compte d'exploitation prévisionnel modifié remis le 9 mars 2016 par la société Le Petit Bain à la commune d'Hyères-les-Palmiers, est un élément constitutif de son offre, non de sa candidature ; que par conséquent, le moyen tiré du caractère tardif, car postérieur à la date de dépôt des candidatures, de la remise des pièces relatives aux garanties financières de la société attributaire doit être écarté ; <br/>
<br/>
              12. Considérant, en quatrième lieu, qu'en vertu des dispositions de l'article L. 1411-5 du code général des collectivités territoriales, dans sa rédaction alors applicable, la commission de délégation de service public, après avoir ouvert les plis contenant les offres, donne un avis sur celles-ci et l'autorité habilitée à signer la convention engage ensuite librement toute discussion utile avec une ou des entreprises ayant présenté une offre ; qu'il résulte de l'instruction que la commission a, en l'espèce, rendu un avis circonstancié et a indiqué quelles étaient, à son sens, les trois offres les plus satisfaisantes ; qu'ainsi, le moyen tiré de l'irrégularité de son avis doit être écarté ; <br/>
<br/>
              13. Considérant, en cinquième lieu, que ne saurait être utilement invoqué devant le juge du référé précontractuel le moyen tiré du caractère insuffisant de l'information donnée aux membres de l'assemblée délibérante avant qu'ils ne se prononcent sur le choix du délégataire ; <br/>
<br/>
              14. Considérant, enfin, qu'il ne résulte de l'instruction ni que la commune aurait méconnu les critères de sélection des offres, ni que l'offre de la société Le Petit Bain n'aurait pas été conforme aux exigences de la collectivité, notamment en ce qui concerne l'accessibilité des lieux ; <br/>
<br/>
              15. Considérant qu'il résulte de ce qui précède que la société MLR Plage n'est pas fondée à demander l'annulation de la procédure engagée par la commune de Hyères-les-Palmiers en vue de la passation du contrat en litige ; que ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative, tant devant le juge des référés du tribunal administratif de Toulon que devant le Conseil d'Etat, ne peuvent, par suite, qu'être rejetées ; qu'il y a lieu, en revanche, dans les circonstances de l'espèce, de mettre à la charge de la société MLR Plage, sur le fondement de ces mêmes dispositions, la somme de 4 500 euros à verser à la commune d'Hyères-les-Palmiers et à Mme D...chacun, au titre de l'ensemble de la procédure ;<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du 12 mai 2016 du juge des référés du tribunal administratif de Toulon est annulée.<br/>
Article 2 : La requête de la société MLR Plage présentée devant le juge des référés du tribunal administratif de Toulon et ses conclusions présentées devant le Conseil d'Etat tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La société MLR Plage versera à la commune d'Hyères-les-Palmiers et à Mme D... une somme de 4 500 euros chacun, au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
Article 4 : La présente décision sera notifiée à la commune d'Hyères-les-Palmiers, à la société MLR Plage et à Mme E...D....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-08-015-01 MARCHÉS ET CONTRATS ADMINISTRATIFS. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. PROCÉDURES D'URGENCE. - OFFICE DU JUGE DU RÉFÉRÉ PRÉCONTRACTUEL LORSQU'EST INVOQUÉ DEVANT LUI LE SECRET INDUSTRIEL ET COMMERCIAL [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-04-01 PROCÉDURE. INSTRUCTION. POUVOIRS GÉNÉRAUX D'INSTRUCTION DU JUGE. - OFFICE DU JUGE DU RÉFÉRÉ PRÉCONTRACTUEL LORSQU'EST INVOQUÉ DEVANT LUI LE SECRET INDUSTRIEL ET COMMERCIAL [RJ1].
</SCT>
<ANA ID="9A"> 39-08-015-01 Il appartient au juge du référé précontractuel, lorsqu'est invoqué devant lui le secret commercial et industriel, et s'il l'estime indispensable pour forger sa conviction sur les points en litige, d'inviter la partie qui s'en prévaut à lui procurer tous les éclaircissements nécessaires sur la nature des pièces écartées et sur les raisons de leur exclusion. Il lui revient, si ce secret lui est opposé à tort, d'enjoindre à cette partie de produire les pièces en cause et de tirer les conséquences, le cas échéant, de son abstention.</ANA>
<ANA ID="9B"> 54-04-01 Il appartient au juge du référé précontractuel, lorsqu'est invoqué devant lui le secret commercial et industriel, et s'il l'estime indispensable pour forger sa conviction sur les points en litige, d'inviter la partie qui s'en prévaut à lui procurer tous les éclaircissements nécessaires sur la nature des pièces écartées et sur les raisons de leur exclusion. Il lui revient, si ce secret lui est opposé à tort, d'enjoindre à cette partie de produire les pièces en cause et de tirer les conséquences, le cas échéant, de son abstention.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Cf. CE, Assemblée, 6 novembre 2002, M. Moon Sun Myung, n° 194295, p. 380.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
