<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042737138</ID>
<ANCIEN_ID>JG_L_2020_12_000000428196</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/73/71/CETATEXT000042737138.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 6ème chambres réunies, 23/12/2020, 428196</TITRE>
<DATE_DEC>2020-12-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>428196</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BUK LAMENT - ROBILLOT</AVOCATS>
<RAPPORTEUR>M. Alain Seban</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:428196.20201223</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A... B... a demandé au tribunal administratif de Paris d'annuler pour excès de pouvoir la décision du 13 décembre 2016 par laquelle la commission déléguée pour la mise en oeuvre de l'accord collectif catégorie 1 du département de Paris du 1er octobre 2013 a rejeté sa candidature à l'attribution d'un logement social. Par un jugement n° 1800405/6-1 du 20 juillet 2018, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés au secrétariat du contentieux du Conseil d'Etat les 19 février et 20 mai 2019, demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat et du département de Paris la somme de 3 000 euros à verser à la SCP Potier de la Varde - Buk Lament - Robillot, son avocat, au titre des articles 37 de la loi du 10 juillet 1991 et L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
              Vu :<br/>
              - le code de la construction et de l'habitation ;<br/>
              - le code de justice administrative ;<br/>
              - l'accord collectif départemental de Paris du 1er octobre 2013 ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
	-le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Alain Seban, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Buk Lament-Robillot, avocat de M. B....<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au tribunal administratif que M. B... a demandé à être admis au bénéfice de l'accord collectif du département de Paris, conclu le 1er octobre 2013 et relatif au relogement des personnes défavorisées cumulant des difficultés économiques et sociales. Sa candidature à l'attribution d'un logement social dans ce cadre a été rejetée le 13 décembre 2016 par la commission instituée par cet accord. M. B... a demandé l'annulation de cette décision au tribunal administratif de Paris qui, par le jugement attaqué du 20 juillet 2018, a rejeté sa demande.<br/>
<br/>
              2. Aux termes de l'article L. 441-1-2 du code de la construction et de l'habitation : " Dans chaque département, le représentant de l'Etat conclut tous les trois ans un accord collectif avec les organismes disposant d'un patrimoine locatif social dans le département. Les représentants des organismes titulaires de droits de réservation sur des logements inclus dans ce patrimoine peuvent être signataires de l'accord. Cet accord, qui doit respecter la mixité sociale des villes et des quartiers et tenir compte, par secteur géographique, des capacités d'accueil et des conditions d'occupation des immeubles constituant le patrimoine des différents organismes, définit : / -pour chaque organisme, un engagement annuel quantifié d'attribution de logements aux personnes connaissant des difficultés économiques et sociales, notamment aux personnes et familles mentionnées à l'article 4 de la loi n° 90-449 du 31 mai 1990 précitée dont les besoins ont été identifiés dans le plan local d'action pour le logement et l'hébergement des personnes défavorisées ; / -les moyens d'accompagnement et les dispositions nécessaires à la mise en oeuvre et au suivi de cet engagement annuel (...) ". <br/>
<br/>
              3. L'accord collectif du département de Paris, conclu le 1er octobre 2013 en application des dispositions citées ci-dessus, prévoit, en son article 1er, que : " L'accord collectif a pour objectif d'apporter une solution de relogement dans les meilleurs délais aux ménages susceptibles d'accéder à un logement autonome et confrontés aux difficultés sociales et de logement les plus aigües (...) et répondant aux critères définis ci-dessous :  / Catégorie 1 : les ménages à faibles ressources nécessitant un relogement urgent et rencontrant des difficultés sociales, familiales, professionnelles ou de santé sérieuses, et/ou pour lesquels le relogement conforte un processus d'insertion. Une attention particulière est accordée dans ce cadre aux femmes en grande difficulté, plus particulièrement les femmes victimes de violence (...) ". Aux termes de l'article 6 du même accord : " (...) Il est constitué deux commissions déléguées, dénommées respectivement " commission déléguée 1 ", pour l'examen des demandes des ménages à faibles ressources nécessitant un relogement urgent, et " commission déléguée 2 ", pour l'examen des demandes des ménages concernés par les problématiques d'habitat indigne, tels que définis à l'article 1 du présent accord. (...) Les commissions déléguées établissent et mettent à jour la liste des ménages agréés qui devront être relogés au titre du présent accord (...) ". L'appréciation par laquelle les commissions instituées par ce dernier article estiment qu'un demandeur de logement social remplit les conditions pour être regardé comme prioritaire au titre des engagements d'attribution prévu par cet accord s'exerce sous le contrôle du juge de l'excès de pouvoir.<br/>
<br/>
              4. Il résulte des termes mêmes du jugement attaqué que, pour rejeter la demande de M. B..., le tribunal administratif s'est fondé sur ce que, en estimant que la situation de l'intéressé ne relevait pas d'une urgence de relogement justifiant que sa candidature soit retenue, la commission instituée pour examiner les demandes présentées au titre de la " catégorie 1 " n'avait pas entaché sa décision d'une erreur manifeste d'appréciation. Il résulte de ce qui a été dit ci-dessus qu'en se bornant à exercer, sur la légalité de la décision attaquée, un contrôle restreint à l'erreur manifeste d'appréciation, le tribunal a commis une erreur de droit. M. B... est par suite fondé, sans qu'il soit besoin de se prononcer sur les autres moyens de son pourvoi, à demander l'annulation du jugement qu'il attaque.<br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat et de la ville de Paris la somme de 1500 euros chacun à verser à la SCP Potier de la Varde - Buk Lament - Robillot, avocat de M. B..., au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la part contributive de l'Etat.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du 20 juillet 2018 du tribunal administratif de Paris est annulé.<br/>
<br/>
		Article 2 : L'affaire est renvoyée devant le tribunal administratif de Paris.<br/>
<br/>
Article 3 : L'Etat et la ville de Paris verseront la somme de 1500 euros chacun, au titre des article L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991, à la SCP Potier de la Varde - Buk Lament - Robillot, avocat de M. B..., sous réserve que celle-ci renonce à percevoir la part contributive de l'Etat.<br/>
<br/>
Article 4 : La présente décision sera notifiée à M. A... B..., à la ville de Paris et à la ministre de la transition écologique.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">38-04 LOGEMENT. HABITATIONS À LOYER MODÉRÉ. - DÉCISIONS DES COMMISSIONS INSTITUÉES PAR L'ACCORD COLLECTIF PRÉVU À L'ARTICLE L. 441-1-2 DU CCH - 1) CONTENTIEUX RELEVANT DE L'EXCÈS DE POUVOIR - 1) CONTRÔLE DU JUGE SUR LE CARACTÈRE PRIORITAIRE D'UN DEMANDEUR DE LOGEMENT SOCIAL. - CONTRÔLE NORMAL.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-02-01-01 PROCÉDURE. DIVERSES SORTES DE RECOURS. RECOURS POUR EXCÈS DE POUVOIR. RECOURS AYANT CE CARACTÈRE. - RECOURS CONTRE LES DÉCISIONS DES COMMISSIONS INSTITUÉES PAR L'ACCORD COLLECTIF PRÉVU À L'ARTICLE L. 441-1-2 DU CCH.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-07-02-03 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. CONTRÔLE DU JUGE DE L'EXCÈS DE POUVOIR. APPRÉCIATIONS SOUMISES À UN CONTRÔLE NORMAL. - APPRÉCIATION DES COMMISSIONS INSTITUÉES PAR L'ACCORD COLLECTIF PRÉVU À L'ARTICLE L. 441-1-2 DU CCH RELATIVE AU CARACTÈRE PRIORITAIRE D'UN DEMANDEUR DE LOGEMENT SOCIAL.
</SCT>
<ANA ID="9A"> 38-04 1) L'appréciation par laquelle les commissions instituées par l'accord collectif conclu, en vertu de l'article L. 441-1-2 du code de la construction et de l'habitation (CCH), entre le représentant de l'Etat et les organismes disposant d'un patrimoine locatif social dans le département, estiment qu'un demandeur de logement social remplit les conditions pour être regardé comme prioritaire au titre des engagements d'attribution prévu par cet accord s'exerce sous le contrôle du juge de l'excès de pouvoir, 2) au terme d'un contrôle normal.</ANA>
<ANA ID="9B"> 54-02-01-01 L'appréciation par laquelle les commissions instituées par l'accord collectif conclu, en vertu de l'article L. 441-1-2 du code de la construction et de l'habitation (CCH), entre le représentant de l'Etat et les organismes disposant d'un patrimoine locatif social dans le département, estiment qu'un demandeur de logement social remplit les conditions pour être regardé comme prioritaire au titre des engagements d'attribution prévu par cet accord s'exerce sous le contrôle du juge de l'excès de pouvoir.</ANA>
<ANA ID="9C"> 54-07-02-03 Le juge de l'excès de pouvoir exerce un contrôle normal sur l'appréciation par laquelle les commissions instituées par l'accord collectif conclu, en vertu de l'article L. 441-1-2 du code de la construction et de l'habitation (CCH), entre le représentant de l'Etat et les organismes disposant d'un patrimoine locatif social dans le département, estiment qu'un demandeur de logement social remplit les conditions pour être regardé comme prioritaire au titre des engagements d'attribution prévu par cet accord.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
