<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039258834</ID>
<ANCIEN_ID>JG_L_2019_10_000000419996</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/25/88/CETATEXT000039258834.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 4ème chambres réunies, 21/10/2019, 419996, Publié au recueil Lebon</TITRE>
<DATE_DEC>2019-10-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>419996</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>Mme Cécile Chaduteau-Monplaisir</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie Sirinelli</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:419996.20191021</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes : <br/>
<br/>
              1° Sous le n° 419996, par une requête et un mémoire en réplique, enregistrés les 19 avril et 12 décembre 2018 au secrétariat du contentieux du Conseil d'Etat, l'Association française de l'industrie pharmaceutique pour une automédication responsable (AFIPA) demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir les recommandations, adoptées en janvier 2018 par l'Agence nationale de sécurité du médicament et des produits de santé (ANSM), intitulées " Etiquetage des conditionnements des médicaments sous forme orale solide (hors homéopathie) - Recommandations à l'attention des demandeurs et titulaires d'autorisations de mise sur le marché et d'enregistrements " ;<br/>
<br/>
              2°) de mettre à la charge de l'ANSM ou de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              2° Sous le n° 419997, par une requête et un mémoire en réplique, enregistrés les 19 avril et 12 décembre 2018, l'Association française de l'industrie pharmaceutique pour une automédication responsable demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir les recommandations, adoptées en janvier 2018 par l'Agence nationale de sécurité du médicament et des produits de santé, intitulées " Noms des médicaments : recommandations à l'attention des demandeurs et titulaires d'autorisations de mise sur le marché et d'enregistrements " en tant qu'elles interdisent l'utilisation de " marques ombrelles " pour les médicaments de prescription facultative ou, à défaut de divisibilité de ces dispositions, d'annuler les recommandations dans leur entier ;<br/>
<br/>
              2°) de mettre à la charge de l'ANSM ou de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - la Constitution ;<br/>
              - le code de la propriété intellectuelle ;<br/>
              - le code de la santé publique ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Cécile Chaduteau-Monplaisir, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Marie Sirinelli, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat de l'Association française de l'industrie pharmaceutique pour une automédication responsable ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Aux termes de l'article L. 5311-1 du code de la santé publique : " I.- L'Agence nationale de sécurité du médicament et des produits de santé est un établissement public de l'Etat (...) / II.- L'agence procède à l'évaluation des bénéfices et des risques liés à l'utilisation des produits à finalité sanitaire destinés à l'homme (...). Elle surveille le risque lié à ces produits et effectue des réévaluations des bénéfices et des risques. / (...) / L'agence participe à l'application des lois et règlements et prend, dans les cas prévus par des dispositions particulières, des décisions relatives (...) au conditionnement, (...) à l'exploitation, à la mise sur le marché, à la publicité, à la mise en service ou à l'utilisation des produits à finalité sanitaire destinés à l'homme (...) ", notamment des médicaments à usage humain. Il résulte des dispositions des articles L. 5121-8 et L. 5121-9 du même code que toute spécialité pharmaceutique qui ne fait pas l'objet d'une autorisation de mise sur le marché délivrée par l'Union européenne doit faire l'objet, avant sa mise sur le marché ou sa distribution à titre gratuit, d'une autorisation de mise sur le marché délivrée par l'Agence nationale de sécurité du médicament et des produits de santé et que cette autorisation est refusée, notamment, " lorsque la documentation et les renseignements fournis ne sont pas conformes au dossier qui doit être présenté à l'appui de la demande ". En vertu des articles R. 5121-21 et R. 5121-25 de ce code, la demande d'autorisation de mise sur le marché mentionne notamment le nom du médicament, lequel est réglementé par les articles R. 5121-2 et R. 5121-3 du même code, et est accompagnée d'un dossier comprenant notamment une ou plusieurs maquettes ou échantillons de son conditionnement extérieur et de son conditionnement primaire, lesquels sont soumis aux dispositions des articles R. 5121-138 et suivants de ce code. <br/>
<br/>
              2. Il ressort des pièces des dossiers que l'Agence nationale de sécurité du médicament et des produits de santé a adopté, en janvier 2018, des recommandations intitulées " Noms des médicaments - Recommandations à l'attention des demandeurs et titulaires d'autorisations de mise sur le marché et d'enregistrements " et " Etiquetage des conditionnements des médicaments sous forme orale solide (hors homéopathie) - Recommandations à l'attention des demandeurs et titulaires d'autorisations de mise sur le marché et d'enregistrements ", qu'elle a mises en ligne sur son site internet. Par deux requêtes qu'il y a lieu de joindre, l'Association française de l'industrie pharmaceutique pour une automédication responsable (AFIPA) demande au Conseil d'Etat d'annuler ces recommandations pour excès de pouvoir. <br/>
<br/>
              Sur la fin de non-recevoir opposée par l'Agence nationale de sécurité du médicament et des produits de santé :<br/>
<br/>
              3. Par les recommandations litigieuses, élaborées à l'issue d'une évaluation du risque d'erreurs médicamenteuses liées au nom et à l'étiquetage des médicaments, l'Agence nationale de sécurité du médicament et des produits de santé a précisé les éléments qu'elle entendait prendre en considération, à l'occasion de l'examen des demandes d'autorisation de mise sur le marché ou de modification d'autorisation, pour apprécier le respect des dispositions applicables au nom et au conditionnement des médicaments, afin de contribuer à la prévention des erreurs médicamenteuses. A ce titre, ces recommandations préconisent notamment, s'agissant du nom des médicaments, de ne pas utiliser de " marques ombrelles " et, s'agissant de l'étiquetage des médicaments sous forme orale solide, de privilégier, sur les conditionnements extérieurs et primaires, la mention de la dénomination commune du médicament et son dosage, par rapport au nom de fantaisie choisi par le laboratoire et à la marque. Alors même qu'elles sont, par elles-mêmes, dépourvues d'effets juridiques, ces recommandations, prises par une autorité administrative, ont pour objet d'influer de manière significative sur les comportements des demandeurs et titulaires d'autorisations de mise sur le marché et d'enregistrements, ainsi que sur les comportements de consommation des patients recourant à l'automédication, et sont de ce fait de nature à produire des effets notables. Dans ces conditions, ces recommandations doivent être regardées comme faisant grief aux laboratoires pharmaceutiques, notamment ceux commercialisant des spécialités non soumises à prescription médicale. L'association française de l'industrie pharmaceutique pour une automédication responsable, qui regroupe certains d'entre eux, est ainsi recevable à demander l'annulation des recommandations qu'elle attaque. La fin de non-recevoir soulevée par l'Agence nationale de sécurité du médicament et des produits de santé doit donc être écartée.<br/>
<br/>
              Sur le bien-fondé des requêtes : <br/>
<br/>
              En ce qui concerne les moyens communs aux deux requêtes :<br/>
<br/>
              4. En premier lieu, d'une part, le choix du nom d'un médicament et des mentions de son étiquetage n'affecte pas l'environnement au sens des dispositions de l'article 5 de la Charte de l'environnement, à laquelle le Préambule de la Constitution fait référence. D'autre part, les recommandations litigieuses visant à réduire les risques existant d'erreurs médicamenteuses, l'association requérante ne peut utilement invoquer à leur encontre, en soutenant que l'Agence nationale de sécurité du médicament et des produits de santé en aurait fait une application excessive, le principe de précaution tel qu'interprété par la jurisprudence de la Cour de justice de l'Union européenne, qui permet, lorsque des incertitudes subsistent quant à l'existence ou à la portée de risques pour la santé des personnes, de prendre des mesures de protection sans avoir à attendre que la réalité et la gravité de ces risques soient pleinement démontrées.<br/>
<br/>
              5. D'autre part, ces recommandations ne peuvent être regardées, eu égard à leur objet et à leurs effets comme des mesures de police sanitaire. Par suite, les moyens tirés par l'association requérante de ce que, pour ce motif, elles méconnaîtraient différents textes ou principes ne peuvent qu'être écartés comme inopérants. <br/>
<br/>
              En ce qui concerne les moyens dirigés contre les recommandations relatives aux noms des médicaments :<br/>
<br/>
              6. Aux termes de l'article R. 5121-2 du code de la santé publique : " Le nom d'un médicament peut être soit un nom de fantaisie, soit une dénomination commune ou scientifique assortie d'une marque ou du nom du titulaire de l'autorisation de mise sur le marché ou de l'entreprise assurant l'exploitation du médicament. Le nom de fantaisie ne peut se confondre avec la dénomination commune ". En outre, aux termes de l'article R 5121-3 du même code : " Sans préjudice de l'application de la législation relative aux marques de fabrique, de commerce et de service, le nom de fantaisie est choisi de façon à éviter toute confusion avec d'autres médicaments et ne pas induire en erreur sur la qualité ou les propriétés de la spécialité ".<br/>
<br/>
              7. Ainsi que le précisent les recommandations litigieuses, la pratique des " marques ombrelles " consiste, pour un titulaire d'autorisations de mises sur le marché de médicaments pouvant être délivrés sans prescription médicale, soit à utiliser un même nom de fantaisie pour plusieurs médicaments dont la composition en substances actives et les indications thérapeutiques sont différentes, soit à choisir, pour un médicament, un nom de fantaisie qui partage tout ou partie du nom d'un autre produit de santé tel un dispositif médical, d'un produit cosmétique ou encore d'une denrée alimentaire. <br/>
<br/>
              8. En premier lieu, l'Agence nationale de sécurité du médicament et des produits de santé, qui est chargée, en vertu des dispositions citées au point 1, de délivrer les autorisations de mise sur le marché et, à cette occasion, de contrôler le respect des dispositions des articles R. 5121-2 et R. 5121-3 du code de la santé publique, avait compétence pour préconiser, par les recommandations en litige, les précautions à prendre dans le choix d'un nom de fantaisie pour un médicament.<br/>
<br/>
              9. En second lieu, il ressort des pièces du dossier que la pratique des " marques ombrelles " est susceptible de favoriser la confusion entre des médicaments de composition en substances actives et d'indications différentes et peut ainsi induire en erreur sur leur qualité ou leurs propriétés, ce que vise à proscrire l'article R. 5121-3 du code de la santé publique. Contrairement à ce que soutient l'association requérante, la circonstance qu'un médicament ne soit pas délivré sur prescription médicale est de nature à renforcer les risques de confusion que peut entraîner l'usage d'un même nom ou de noms très proches pour des produits différents. Par suite, en préconisant que le recours à des " marques ombrelles " soit évité, en raison des risques d'erreurs médicamenteuses que cette pratique est susceptible d'entraîner, l'Agence nationale de sécurité du médicament et des produits de santé n'a ni méconnu le sens et la portée des articles R. 5121-2 et R. 5121-3 du code de la santé publique, ni commis d'erreur manifeste d'appréciation.<br/>
<br/>
              En ce qui concerne les moyens dirigés contre les recommandations relatives à l'étiquetage des conditionnements des médicaments sous forme orale solide :<br/>
<br/>
              10. Aux termes de l'article R. 5121-1 du code de la santé publique : " Pour l'application du présent livre, on entend par (...) 3° Conditionnement primaire, le récipient ou toute autre forme de conditionnement avec lequel le médicament se trouve en contact direct ; / 4° Conditionnement extérieur, l'emballage dans lequel est placé le conditionnement primaire ; (...) / 7° Etiquetage, les mentions portées sur le conditionnement extérieur ou le conditionnement primaire (...) ". L'article R. 5121-138 du même code fixe la liste des mentions que doit comporter l'étiquetage du conditionnement extérieur des médicaments ou, à défaut d'un tel conditionnement, celui de son conditionnement primaire, en exigeant qu'elles soient " inscrites de manière à être facilement lisibles, clairement compréhensibles et indélébiles ". Enfin, le I de l'article R. 5121-139 du même code précise que : " Le conditionnement extérieur peut comporter, outre le signe distinctif de l'entreprise, des signes ou des pictogrammes explicitant certaines des informations ci-dessus ainsi que d'autres informations compatibles avec le résumé des caractéristiques du produit. Ces éléments doivent être utiles pour les patients et ne présenter aucun caractère promotionnel ".<br/>
<br/>
              11. En premier lieu, le principe d'égalité n'impose pas de traiter différemment des situations différentes. Alors, au surplus, que l'article R. 5121-138 du code de la santé publique détermine de façon identique les mentions que doit comporter l'étiquetage des médicaments, qu'ils soient soumis ou non à prescription médicale, sous la seule réserve de l'ajout de l'indication thérapeutique pour les médicaments non soumis à prescription, l'association requérante n'est pas fondée à soutenir que l'Agence nationale de sécurité du médicament et des produits de santé aurait méconnu le principe d'égalité en leur appliquant le même régime qu'aux médicaments soumis à prescription médicale. <br/>
<br/>
              12. En deuxième lieu, les recommandations litigieuses n'ajoutent pas d'éléments à la liste des mentions qui doivent figurer sur les conditionnements des médicaments, fixée par l'article R. 5121-138 du code de la santé publique mentionné au point 10, mais comportent des préconisations relatives à la disposition et à la présentation de ces mentions ainsi que des pictogrammes obligatoires ou facultatifs et des autres mentions autorisées, en insistant sur la mise en valeur de la dénomination commune du médicament et de son dosage. En proposant ainsi des " principes généraux " de " compréhensibilité, visibilité et lisibilité des mentions " et en suggérant un agencement des mentions et signes sur les faces des conditionnements, ainsi que les types de formats, les codes de couleurs et les types de matériaux à privilégier ou à éviter, pour améliorer la lisibilité et la bonne compréhension des informations qu'ils comprennent, tout en laissant aux laboratoires le soin d'adapter ces préconisations en fonction de leurs contraintes, les recommandations litigieuses n'ont pas méconnu le sens et la portée des dispositions de l'article R. 5121-138 du code de la santé publique.<br/>
<br/>
              13. En troisième lieu, si les recommandations litigieuses préconisent, pour favoriser son identification et son bon usage, de privilégier sur le conditionnement, par la taille des caractères, la dénomination commune et le dosage du médicament par rapport à son nom de fantaisie et de ne pas faire figurer le logo de la marque ou du laboratoire sur la face principale du conditionnement, elles ne s'opposent pas à ce que ce nom et ce logo apparaissent de façon claire et lisible dans l'étiquetage. Par suite, l'association requérante n'est pas fondée à soutenir que les recommandations méconnaîtraient les dispositions de l'article R. 5121-139 citées au point 10. Elle n'est pas plus fondée à soutenir qu'elles pourraient entraîner pour les laboratoires une privation de leur droit sur la marque par application de l'article L. 714-5 du code de la propriété intellectuelle, relatif à la déchéance de ses droits qu'encourt le propriétaire d'une marque qui, sans justes motifs, n'en fait pas un usage sérieux pendant une période ininterrompue de cinq ans.<br/>
<br/>
              14. En dernier lieu, si l'application des recommandations litigieuses peut conduire à privilégier la dénomination commune sur le nom de fantaisie et le signe distinctif de l'entreprise et à harmoniser en partie l'étiquetage du conditionnement des médicaments, il n'en résulte pas qu'elles priveraient les patients recourant à l'automédication de la possibilité d'identifier le médicament par sa marque et son nom de fantaisie ou par le logo du laboratoire ni qu'elles accroîtraient les risques de confusion entre médicaments. L'Association française de l'industrie pharmaceutique pour une automédication responsable n'est ainsi pas fondée à soutenir que l'Agence nationale de sécurité du médicament et des produits de santé se serait fondée sur des faits matériellement inexacts ou aurait commis une erreur manifeste d'appréciation en adoptant les recommandations litigieuses, destinées à améliorer la lisibilité des informations portées sur le conditionnement des médicaments et à réduire les risques d'erreurs médicamenteuses.<br/>
<br/>
              15. Il résulte de tout ce qui précède que l'Association française de l'industrie pharmaceutique pour une automédication responsable n'est pas fondée à demander l'annulation des recommandations qu'elle attaque.<br/>
<br/>
              16. Par suite, ses requêtes, y compris ses conclusions présentées sur le fondement de l'article L. 761-1 du code de justice administrative, doivent être rejetées.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Les requêtes de l'Association française de l'industrie pharmaceutique pour une automédication responsable sont rejetées.<br/>
Article 2 : La présente décision sera notifiée à l'Association française de l'industrie pharmaceutique pour une automédication responsable et à l'Agence nationale de sécurité du médicament et des produits de santé.<br/>
Copie en sera adressée à la ministre de la santé et des solidarités.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-01-05-02-02 ACTES LÉGISLATIFS ET ADMINISTRATIFS. DIFFÉRENTES CATÉGORIES D'ACTES. ACTES ADMINISTRATIFS - NOTION. ACTES À CARACTÈRE DE DÉCISION. ACTES NE PRÉSENTANT PAS CE CARACTÈRE. - RECOMMANDATIONS DE L'ANSM PRÉCISANT LES ÉLÉMENTS QU'ELLE ENTEND PRENDRE EN CONSIDÉRATION POUR APPRÉCIER LE RESPECT DES DISPOSITIONS APPLICABLES AU NOM ET AU CONDITIONNEMENT DES MÉDICAMENTS - ACTE SUSCEPTIBLE DE FAIRE L'OBJET D'UN RECOURS POUR EXCÈS DE POUVOIR [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-01-01-01 PROCÉDURE. INTRODUCTION DE L'INSTANCE. DÉCISIONS POUVANT OU NON FAIRE L'OBJET D'UN RECOURS. ACTES CONSTITUANT DES DÉCISIONS SUSCEPTIBLES DE RECOURS. - RECOMMANDATIONS DE L'ANSM PRÉCISANT LES ÉLÉMENTS QU'ELLE ENTEND PRENDRE EN CONSIDÉRATION POUR APPRÉCIER LE RESPECT DES DISPOSITIONS APPLICABLES AU NOM ET AU CONDITIONNEMENT DES MÉDICAMENTS [RJ1].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">61-01 SANTÉ PUBLIQUE. PROTECTION GÉNÉRALE DE LA SANTÉ PUBLIQUE. - RECOMMANDATIONS DE L'ANSM PRÉCISANT LES ÉLÉMENTS QU'ELLE ENTEND PRENDRE EN CONSIDÉRATION POUR APPRÉCIER LE RESPECT DES DISPOSITIONS APPLICABLES AU NOM ET AU CONDITIONNEMENT DES MÉDICAMENTS - RECOMMANDATIONS PRÉCONISANT NOTAMMENT DE NE PAS UTILISER DE MARQUES OMBRELLES - LÉGALITÉ - EXISTENCE.
</SCT>
<ANA ID="9A"> 01-01-05-02-02 Par les recommandations litigieuses, élaborées à l'issue d'une évaluation du risque d'erreurs médicamenteuses liées au nom et à l'étiquetage des médicaments, l'Agence nationale de sécurité du médicament et des produits de santé (ANSM) a précisé les éléments qu'elle entendait prendre en considération, à l'occasion de l'examen des demandes d'autorisation de mise sur le marché (AMM) ou de modification d'autorisation, pour apprécier le respect des dispositions applicables au nom et au conditionnement des médicaments, afin de contribuer à la prévention des erreurs médicamenteuses. A ce titre, ces recommandations préconisent notamment, s'agissant du nom des médicaments, de ne pas utiliser de marques ombrelles et, s'agissant de l'étiquetage des médicaments sous forme orale solide, de privilégier, sur les conditionnements extérieurs et primaires, la mention de la dénomination commune du médicament et son dosage, par rapport au nom de fantaisie choisi par le laboratoire et à la marque.,,,Alors même qu'elles sont, par elles-mêmes, dépourvues d'effets juridiques, ces recommandations, prises par une autorité administrative, ont pour objet d'influer de manière significative sur les comportements des demandeurs et titulaires d'AMM et d'enregistrements, ainsi que sur les comportements de consommation des patients recourant à l'automédication, et sont de ce fait de nature à produire des effets notables. Dans ces conditions, ces recommandations doivent être regardées comme faisant grief aux laboratoires pharmaceutiques, notamment ceux commercialisant des spécialités non soumises à prescription médicale, et sont par suite susceptibles de faire l'objet d'un recours pour excès de pouvoir.</ANA>
<ANA ID="9B"> 54-01-01-01 Par les recommandations litigieuses, élaborées à l'issue d'une évaluation du risque d'erreurs médicamenteuses liées au nom et à l'étiquetage des médicaments, l'Agence nationale de sécurité du médicament et des produits de santé (ANSM) a précisé les éléments qu'elle entendait prendre en considération, à l'occasion de l'examen des demandes d'autorisation de mise sur le marché (AMM) ou de modification d'autorisation, pour apprécier le respect des dispositions applicables au nom et au conditionnement des médicaments, afin de contribuer à la prévention des erreurs médicamenteuses. A ce titre, ces recommandations préconisent notamment, s'agissant du nom des médicaments, de ne pas utiliser de marques ombrelles et, s'agissant de l'étiquetage des médicaments sous forme orale solide, de privilégier, sur les conditionnements extérieurs et primaires, la mention de la dénomination commune du médicament et son dosage, par rapport au nom de fantaisie choisi par le laboratoire et à la marque.,,,Alors même qu'elles sont, par elles-mêmes, dépourvues d'effets juridiques, ces recommandations, prises par une autorité administrative, ont pour objet d'influer de manière significative sur les comportements des demandeurs et titulaires d'AMM et d'enregistrements, ainsi que sur les comportements de consommation des patients recourant à l'automédication, et sont de ce fait de nature à produire des effets notables. Dans ces conditions, ces recommandations doivent être regardées comme faisant grief aux laboratoires pharmaceutiques, notamment ceux commercialisant des spécialités non soumises à prescription médicale, et sont par suite susceptibles de faire l'objet d'un recours pour excès de pouvoir.</ANA>
<ANA ID="9C"> 61-01 Par les recommandations litigieuses, élaborées à l'issue d'une évaluation du risque d'erreurs médicamenteuses liées au nom et à l'étiquetage des médicaments, l'Agence nationale de sécurité du médicament et des produits de santé (ANSM) a précisé les éléments qu'elle entendait prendre en considération, à l'occasion de l'examen des demandes d'autorisation de mise sur le marché ou de modification d'autorisation, pour apprécier le respect des dispositions applicables au nom et au conditionnement des médicaments, afin de contribuer à la prévention des erreurs médicamenteuses.... ,,A ce titre, ces recommandations préconisent notamment, s'agissant du nom des médicaments, de ne pas utiliser de marques ombrelles, pratique qui consiste, pour un titulaire d'autorisations de mises sur le marché de médicaments pouvant être délivrés sans prescription médicale, soit à utiliser un même nom de fantaisie pour plusieurs médicaments dont la composition en substances actives et les indications thérapeutiques sont différentes, soit à choisir, pour un médicament, un nom de fantaisie qui partage tout ou partie du nom d'un autre produit de santé tel un dispositif médical, d'un produit cosmétique ou encore d'une denrée alimentaire.,, ...Cette pratique est susceptible de favoriser la confusion entre des médicaments de composition en substances actives et d'indications différentes et peut ainsi induire en erreur sur leur qualité ou leurs propriétés, ce que vise à proscrire l'article R. 5121-3 du code de la santé publique (CSP). La circonstance qu'un médicament ne soit pas délivré sur prescription médicale est de nature à renforcer les risques de confusion que peut entraîner l'usage d'un même nom ou de noms très proches pour des produits différents. Par suite, en préconisant que le recours à des marques ombrelles soit évité, en raison des risques d'erreurs médicamenteuses que cette pratique est susceptible d'entraîner, l'ANSM n'a ni méconnu le sens et la portée des articles R. 5121-2 et R. 5121-3 du CSP, ni commis d'erreur manifeste d'appréciation.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Assemblée, 19 juillet 2019, Mme,, n° 426389, à publier au Recueil. Rappr., s'agissant des actes de droit souple des autorités de régulation, CE, Assemblée, 21 mars 2016, Société NC Numericable, n° 390023, p. 88 ; CE, Assemblée, 21 mars 2016, Société Fairvesta International GMBH et autres, n°s 368082 368083 368084, p. 76.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
