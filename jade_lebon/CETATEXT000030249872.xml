<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030249872</ID>
<ANCIEN_ID>JG_L_2015_02_000000369831</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/24/98/CETATEXT000030249872.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème / 3ème SSR, 16/02/2015, 369831</TITRE>
<DATE_DEC>2015-02-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>369831</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème / 3ème SSR</FORMATION>
<TYPE_REC/>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MONOD, COLIN, STOCLET</AVOCATS>
<RAPPORTEUR>M. Mathieu Herondart</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Nathalie Escaut</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:369831.20150216</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>              Vu la procédure suivante :<br/>
<br/>
<br/>
              La commune de Saint-Dié-des-Vosges a demandé au tribunal administratif de Nancy d'annuler l'avis du conseil de discipline régional de recours du 26 septembre 2011 préconisant une exclusion temporaire de fonctions d'une durée de six mois à l'encontre de M. B... A.... Par un jugement n° 1102159 du 28 août 2012, le tribunal administratif de Nancy a annulé cet avis.<br/>
<br/>
              Par un arrêt n° 12NC01673 du 2 mai 2013, la cour administrative d'appel de Nancy a, sur appel de M.A..., annulé ce jugement du 28 août 2012 du tribunal administratif de Nancy et rejeté la demande de la commune de Saint-Dié-des-Vosges.<br/>
<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 2 juillet et 30 septembre 2013 au secrétariat du contentieux du Conseil d'Etat, la commune de Saint-Dié-des-Vosges demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt du 2 mai 2013 de la cour administrative d'appel de Nancy ; <br/>
<br/>
              2°) de mettre à la charge de M. A... une somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.  <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu : <br/>
<br/>
              - la loi n° 84-53 du 26 janvier 1984 ; <br/>
<br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Mathieu Herondart, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Nathalie Escaut, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Monod, Colin, Stoclet, avocat de la commune de Saint-Dié-des-Vosges.<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 19 janvier 2011, le maire de la commune de Saint-Dié-des-Vosges a prononcé la révocation de M.A... ; que le conseil de discipline de recours de la région Lorraine, par un avis du 26 septembre 2011, a proposé de substituer une sanction d'exclusion temporaire de six mois à la sanction de révocation retenue par le maire ; que le tribunal administratif de Nancy, par un jugement du 28 août 2012, a annulé cet avis du conseil de discipline de recours à la demande de la commune de Saint-Dié-des-Vosges ; que cette commune se pourvoit en cassation contre l'arrêt du 2 mai 2013 par lequel la cour administrative d'appel de Nancy a annulé ce jugement et rejeté sa demande ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article 89 de la loi du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale : " Les sanctions disciplinaires sont réparties en quatre groupes :  (...) Quatrième groupe : / la mise à la retraite d'office ; / la révocation " ; qu'aux termes de l'article 91 de la même loi : " Les fonctionnaires qui ont fait l'objet d'une sanction des deuxième, troisième et quatrième groupes peuvent introduire un recours auprès du conseil de discipline départemental ou interdépartemental dans les cas et conditions fixés par un décret en Conseil d'Etat. / L'autorité territoriale ne peut prononcer de sanction plus sévère que celle proposée par le conseil de discipline de recours " ; <br/>
<br/>
              3. Considérant qu'il appartient au juge de l'excès de pouvoir, saisi de moyens en ce sens, de rechercher si les faits reprochés à un agent public ayant fait l'objet d'une sanction disciplinaire constituent des fautes de nature à justifier une sanction et si la sanction retenue est proportionnée à la gravité de ces fautes ; qu'il lui appartient également de rechercher si la sanction proposée par un conseil de discipline de recours statuant sur le recours d'un fonctionnaire territorial est proportionnée à la gravité des fautes qui lui sont reprochées ; qu'en se bornant à rechercher si le conseil de discipline de recours n'avait pas entaché sa décision d'erreur manifeste d'appréciation, la cour a commis une erreur de droit ; que, par suite, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, la commune est fondée à demander l'annulation de l'arrêt qu'elle attaque ; <br/>
<br/>
              4. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par la commune au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nancy du 2 mai 2013 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nancy.<br/>
Article 3 : Les conclusions de la commune de Saint-Dié-des-Vosges présentées sur le fondement de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 4 : La présente décision sera notifiée à la commune de Saint-Dié-des-Vosges et à M. B... A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-09-05-01 FONCTIONNAIRES ET AGENTS PUBLICS. DISCIPLINE. PROCÉDURE. CONSEIL DE DISCIPLINE. - PROPOSITION DE SANCTION - CARACTÈRE PROPORTIONNÉ - CONTRÔLE DU JUGE DE L'EXCÈS DE POUVOIR - CONTRÔLE NORMAL.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-07-02-03 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. CONTRÔLE DU JUGE DE L'EXCÈS DE POUVOIR. APPRÉCIATIONS SOUMISES À UN CONTRÔLE NORMAL. - CARACTÈRE PROPORTIONNÉ DE LA SANCTION PROPOSÉE PAR UN CONSEIL DE DISCIPLINE.
</SCT>
<ANA ID="9A"> 36-09-05-01 Il appartient au juge de l'excès de pouvoir, saisi de moyens en ce sens, de rechercher si les faits reprochés à un agent public ayant fait l'objet d'une sanction disciplinaire constituent des fautes de nature à justifier une sanction et si la sanction retenue est proportionnée à la gravité de ces fautes [RJ1]. Il lui appartient également de rechercher si la sanction proposée par un conseil de discipline de recours statuant sur le recours d'un fonctionnaire territorial est proportionnée à la gravité des fautes qui lui sont reprochées.</ANA>
<ANA ID="9B"> 54-07-02-03 Il appartient au juge de l'excès de pouvoir, saisi de moyens en ce sens, de rechercher si les faits reprochés à un agent public ayant fait l'objet d'une sanction disciplinaire constituent des fautes de nature à justifier une sanction et si la sanction retenue est proportionnée à la gravité de ces fautes [RJ1]. Il lui appartient également de rechercher si la sanction proposée par un conseil de discipline de recours statuant sur le recours d'un fonctionnaire territorial est proportionnée à la gravité des fautes qui lui sont reprochées.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Assemblée, 13 novembre 2013, M. Dahan, n° 347704, p. 279.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
