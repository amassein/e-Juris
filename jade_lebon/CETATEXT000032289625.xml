<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032289625</ID>
<ANCIEN_ID>JG_L_2016_03_000000386108</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/28/96/CETATEXT000032289625.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème / 5ème SSR, 23/03/2016, 386108</TITRE>
<DATE_DEC>2016-03-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>386108</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème / 5ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELVOLVE ; SCP MASSE-DESSEN, THOUVENIN, COUDRAY</AVOCATS>
<RAPPORTEUR>M. Jean-François de Montgolfier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2016:386108.20160323</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Strasbourg d'annuler pour excès de pouvoir la décision du 17 novembre 2011 par laquelle le ministre du travail, de l'emploi et de la santé a annulé la décision du 16 mai 2011 de l'inspecteur du travail de la 4ème section de l'unité territoriale du Bas-Rhin qui avait refusé l'autorisation de procéder à son licenciement, et a autorisé ce licenciement. Par un jugement n° 1201715 du 15 octobre 2013, le tribunal administratif a rejeté cette demande.<br/>
<br/>
              Par un arrêt n° 13NC02257 du 30 septembre 2014, la cour administrative d'appel de Nancy, sur appel de M.B..., a annulé ce jugement ainsi que la décision du 17 novembre 2011.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 1er décembre 2014 et 27 février 2015 au secrétariat du contentieux du Conseil d'Etat, la société Sotralentz Packaging demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de M. B...;<br/>
<br/>
              3°) de mettre à la charge de M. B...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code du travail ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-François de Montgolfier, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Delvolvé, avocat de la société Sotralenz Packaging et à la SCP Masse-Dessen, Thouvenin, Coudray, avocat de M. B...;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, dans le cadre d'un projet de réorganisation de la société Sotralenz Packaging, l'employeur a envisagé la suppression du poste de travail de M.B..., salarié protégé ; que ce dernier a indiqué qu'il n'acceptait pas de recevoir des offres de reclassement hors du territoire national puis a refusé trois offres écrites de reclassement qui lui avaient été remises le 8 octobre 2010, l'une dans l'entreprise qui l'employait et deux au sein d'une autre entreprise du groupe, situées sur le même site que l'entreprise ; qu'après qu'une première demande d'autorisation de licencier M. B...a été rejetée par l'inspecteur du travail le 23 février 2011 pour un motif tiré de l'irrégularité de la consultation du comité d'entreprise, l'employeur a adressé une nouvelle demande en ce sens le 18 mars 2011, que l'inspecteur du travail a de nouveau rejetée par une décision du 16 mai 2011 ; que, par une décision du 17 novembre 2011, le ministre chargé du travail a annulé cette décision et accordé l'autorisation demandée ; que la société Sotralenz Packaging se pourvoit en cassation contre l'arrêt du 30 septembre 2014 par lequel la cour administrative d'appel de Nancy a annulé le jugement du tribunal administratif de Strasbourg du 15 octobre 2013 ayant rejeté le recours de M. B...contre la décision du ministre et a annulé cette décision au motif que l'employeur avait manqué à son obligation de reclassement ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 1233-4 du code du travail : " Le licenciement pour motif économique d'un salarié ne peut intervenir que lorsque tous les efforts de formation et d'adaptation ont été réalisés et que le reclassement de l'intéressé ne peut être opéré dans l'entreprise ou dans les entreprises du groupe auquel l'entreprise appartient. / Le reclassement du salarié s'effectue sur un emploi relevant de la même catégorie que celui qu'il occupe ou sur un emploi équivalent assorti d'une rémunération équivalente. A défaut, et sous réserve de l'accord exprès du salarié, le reclassement s'effectue sur un emploi d'une catégorie inférieure. / Les offres de reclassement proposées au salarié sont écrites et précises " ; que, dans sa rédaction applicable à l'espèce, le premier alinéa de l'article L. 1233-4-1 du même code dispose que : " Lorsque l'entreprise ou le groupe auquel elle appartient est implanté hors du territoire national, l'employeur demande au salarié, préalablement au licenciement, s'il accepte de recevoir des offres de reclassement hors de ce territoire, dans chacune des implantations en cause, et sous quelles restrictions éventuelles quant aux caractéristiques des emplois offerts, notamment en matière de rémunération et de localisation " ; que les possibilités de reclassement dans l'entreprise, et éventuellement au sein du groupe, s'apprécient antérieurement à la date d'autorisation du licenciement, à compter du moment où celui-ci est envisagé ; <br/>
<br/>
              3. Considérant que si, après qu'une première demande d'autorisation de licenciement d'un salarié a été refusée par l'administration, celle-ci est à nouveau saisie par l'employeur d'une demande d'autorisation de licencier le même salarié, il lui appartient d'apprécier cette nouvelle demande compte tenu des circonstances de droit et de fait à la date à laquelle elle prend sa nouvelle décision ; que, s'agissant, en particulier, de l'obligation de reclassement qui pèse sur l'employeur, il appartient à l'administration de vérifier qu'à cette date, l'employeur a recherché l'ensemble des possibilités de reclassement dans l'entreprise et éventuellement au sein du groupe compte tenu, le cas échéant, de changements des circonstances survenus postérieurement au premier refus ; que l'employeur n'est, en revanche, pas tenu, au titre de cette obligation, d'adresser à nouveau au salarié, avant de présenter cette seconde demande, celles des propositions de reclassement encore valides qu'il avait déjà faites au salarié avant de présenter sa première demande d'autorisation de licenciement et que ce dernier aurait refusées ;<br/>
<br/>
              4. Considérant que pour annuler la décision d'autorisation délivrée par le ministre chargé du travail, la cour administrative d'appel a jugé que l'employeur avait méconnu ses obligations de reclassement au seul motif que, avant de présenter pour la deuxième fois une demande d'autorisation de licencier M.B..., son employeur ne lui avait adressé aucune offre de reclassement, en France ou à l'étranger, alors qu'un délai de plus de cinq mois s'était écoulé depuis les propositions qui lui avaient été faites avant la première demande d'autorisation de licenciement ; qu'en déduisant cette méconnaissance des obligations de reclassement du seul écoulement du temps entre les deux demandes, alors que cette circonstance n'était pas, à elle-seule, de nature à établir qu'à la date à laquelle le ministre a autorisé le licenciement, la recherche, par l'employeur, des possibilités de reclassement n'était pas complète, la cour administrative d'appel a commis une erreur de droit ; que, par suite, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, son arrêt doit être annulé ;<br/>
<br/>
              5. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de la société Sotralenz Packaging, qui n'est pas la partie perdante dans la présente instance ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions de la société Sotralenz Packaging au titre des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
      --------------<br/>
Article 1er : L'arrêt du 30 septembre 2014 de la cour administrative d'appel de Nancy est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nancy.<br/>
Article 3 : Le surplus des conclusions de la société Sotralentz Packaging est rejeté.<br/>
Article 4 : Les conclusions de M. B...tendant à l'application de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à la société Sotralenz Packaging et à M. A...B.... <br/>
Copie en sera adressée à la ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">66-07-01-04-03-01 TRAVAIL ET EMPLOI. LICENCIEMENTS. AUTORISATION ADMINISTRATIVE - SALARIÉS PROTÉGÉS. CONDITIONS DE FOND DE L'AUTORISATION OU DU REFUS D'AUTORISATION. LICENCIEMENT POUR MOTIF ÉCONOMIQUE. OBLIGATION DE RECLASSEMENT. - OBLIGATIONS DE L'EMPLOYEUR APRÈS UN PREMIER REFUS D'AUTORISATION DE LICENCIEMENT D'UN SALARIÉ, LORSQU'IL DEMANDE À NOUVEAU L'AUTORISATION DE LICENCIER LE MÊME SALARIÉ [RJ1].
</SCT>
<ANA ID="9A"> 66-07-01-04-03-01 Si, après qu'une première demande d'autorisation de licenciement d'un salarié a été refusée par l'administration, celle-ci est à nouveau saisie par l'employeur d'une demande d'autorisation de licencier le même salarié, il lui appartient d'apprécier cette nouvelle demande compte tenu des circonstances de droit et de fait à la date à laquelle elle prend sa nouvelle décision. S'agissant, en particulier, de l'obligation de reclassement qui pèse sur l'employeur, il appartient à l'administration de vérifier qu'à cette date, l'employeur a recherché l'ensemble des possibilités de reclassement dans l'entreprise et éventuellement au sein du groupe compte tenu, le cas échéant, de changements des circonstances survenus postérieurement au premier refus. L'employeur n'est, en revanche, pas tenu, au titre de cette obligation, d'adresser à nouveau au salarié, avant de présenter cette seconde demande, celles des propositions de reclassement encore valides qu'il avait déjà faites au salarié avant de présenter sa première demande d'autorisation de licenciement et que ce dernier aurait refusées.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., en ce qui concerne l'entretien préalable, CE, 24 octobre 1984,,, n° 40555, T. p. 763 et CE, 19 septembre 2014, Mme,, n° 362660, T. p. 810-889 ; en ce qui concerne l'enquête contradictoire, CE, 12 octobre 1990, Coopérative d'élevage du Vexin, n° 80533, inédite au Recueil. Cf., pour la date à laquelle s'apprécie l'obligation de reclassement, CE, 3 juillet 2013, M.,, n° 342477, T. p. 866.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
