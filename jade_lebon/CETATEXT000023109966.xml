<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000023109966</ID>
<ANCIEN_ID>JG_L_2010_11_000000324323</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/23/10/99/CETATEXT000023109966.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème et 5ème sous-sections réunies, 15/11/2010, 324323</TITRE>
<DATE_DEC>2010-11-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>324323</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème et 5ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>Mme Bethânia  Gaschet</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Dumortier Gaëlle</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2010:324323.20101115</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 22 janvier et 19 mars 2009 au secrétariat du contentieux du Conseil d'Etat, présentés pour Me C..., demeurant ...Cedex 01), agissant en qualité d'administrateur judiciaire de la société Solymatic SA ; Me C...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 07LY02040 du 25 novembre 2008 par lequel la cour administrative d'appel de Lyon a rejeté sa requête tendant, d'une part, à l'annulation du jugement n° 0503308 du 26 juin 2007 du tribunal administratif de Lyon ayant rejeté sa demande d'annulation de la décision du 13 décembre 2004 de l'inspecteur du travail et de la décision du 31 mars 2005 du ministre de l'emploi, du travail et de la cohésion sociale qui avaient refusé l'autorisation de licencier M. B...D..., d'autre part, à l'annulation de ces décisions ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de commerce ;<br/>
<br/>
              Vu le code du travail ;<br/>
<br/>
              Vu le décret n° 85-1388 du 27 décembre 1985 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Bethânia Gaschet, Auditeur, <br/>
<br/>
              - les observations de la SCP Rocheteau, Uzan-Sarano, avocat de Me C..., <br/>
<br/>
              - les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Rocheteau, Uzan-Sarano, avocat de MeC..., <br/>
<br/>
<br/>
<br/>
<br/>
              Sans qu'il soit besoin d'examiner l'autre moyen du pourvoi ;<br/>
<br/>
              Considérant qu'aux termes du second alinéa de l'article L. 122-12 du code du travail alors en vigueur, devenu l'article L. 1224-1 de ce code : " S'il survient une modification dans la situation juridique de l'employeur, notamment par succession, vente, fusion, transformation du fonds, mise en société, tous les contrats de travail en cours au jour de la modification subsistent entre le nouvel employeur et le personnel de l'entreprise. " ; que, selon l'article L. 621-62 du code de commerce alors en vigueur, le tribunal de commerce " (...) arrête un plan de redressement (...). / Ce plan organise soit la continuation de l'entreprise, soit sa cession, soit sa continuation assortie d'une cession partielle (...) " ; qu'aux termes de l'article L. 621-63 du même code, alors en vigueur : " Le plan désigne les personnes qui sont tenues de l'exécuter (...) " ; qu'aux termes de l'article L. 621-64 du même code, alors en vigueur : " Lorsque le plan prévoit des licenciements pour motif économique, il ne peut être arrêté par le tribunal qu'après que le comité d'entreprise ou, à défaut, les délégués du personnel ainsi que l'autorité administrative compétente ont été informés et consultés (...) / Le plan précise notamment les licenciements qui doivent intervenir dans le délai d'un mois après le jugement. Dans ce délai, ces licenciements interviennent sur simple notification de l'administrateur (...) " ; qu'enfin, selon le dernier alinéa de l'article 64 du décret du 27 décembre 1985 relatif au redressement et à la liquidation judiciaires des entreprises, alors en vigueur, " Le jugement arrêtant le plan indique le nombre de salariés dont le licenciement est autorisé ainsi que les activités professionnelles concernées. " ; <br/>
<br/>
              Considérant qu'il résulte de ces dispositions que, si la cession de l'entreprise en redressement judiciaire arrêtée par un jugement du tribunal de commerce entraîne en principe, de plein droit, le transfert d'une entité économique autonome conservant son identité et, par voie de conséquence, la poursuite par le cessionnaire des contrats de travail attachés à l'entreprise cédée, conformément aux dispositions de l'article L. 122-12 du code du travail citées ci-dessus, il peut être dérogé à ces dispositions lorsque le plan de redressement prévoit des licenciements pour motif économique, à la double condition, prévue par les dispositions de l'article L. 621-64 du code de commerce et de l'article 64 du décret du 27 décembre 1985 citées ci-dessus, d'une part, que le plan de cession ait prévu les licenciements devant intervenir dans le délai d'un mois après le jugement arrêtant le plan, d'autre part, que ce jugement indique le nombre de salariés dont le licenciement est autorisé, ainsi que les activités et catégories professionnelles concernées ;<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que le plan de cession de la société Solymatic SA, dont Me C... est administrateur judiciaire, a été arrêté par un jugement du 1er septembre 2004 du tribunal de commerce de Lyon, confirmé par un arrêt du 5 octobre 2004 de la cour d'appel de Lyon ; que ce jugement indiquait notamment que l'administrateur devrait procéder, dans le mois, au licenciement des salariés non repris ; que Me C...a demandé le 20 octobre 2004 à l'inspecteur du travail, sur le fondement de ce jugement, l'autorisation de licencier M.D..., salarié protégé ; qu'il se pourvoit en cassation contre l'arrêt du 25 novembre 2008 par lequel la cour administrative d'appel de Lyon a confirmé le jugement du tribunal administratif de Lyon qui avait rejeté sa demande tendant à l'annulation des décisions du 13 décembre 2004 de l'inspecteur du travail et du 31 mars 2005 du ministre lui refusant l'autorisation demandée ;<br/>
<br/>
              Considérant que, pour retenir que Me C...n'était pas compétent pour demander, le 20 octobre 2004, l'autorisation de licencier M. D...en application de l'article L. 621-64 du code de commerce, en dépit de l'habilitation qui lui avait été donnée par le jugement du 1er septembre 2004 du tribunal de commerce, la cour administrative d'appel de Lyon a jugé que cette disposition ne pouvait faire obstacle au transfert du contrat de travail à la société cessionnaire dès le 1er septembre 2004 en application de l'article L. 122-12 du code du travail ; qu'en statuant ainsi, sans rechercher, ni si le plan de cession de la société Solymatic SA prévoyait les licenciements devant intervenir dans le délai d'un mois après le jugement, ni si le jugement arrêtant le plan indiquait le nombre de salariés dont le licenciement était autorisé ainsi que les activités et catégories professionnelles concernées, la cour a commis une erreur de droit ; que, par suite, Me C...est fondé à demander pour ce motif l'annulation de l'arrêt attaqué ; <br/>
<br/>
              Considérant qu'il y a lieu, en application de l'article L. 821-2 du code de justice administrative, de régler l'affaire au fond ;<br/>
<br/>
              Considérant, en premier lieu, qu'en relevant dans un motif surabondant de son jugement que Me C...avait demandé à l'inspecteur du travail l'autorisation de licencier les salariés protégés postérieurement au délai d'un mois qui lui était imparti par le jugement du tribunal de commerce, le tribunal administratif de Lyon n'a pas soulevé d'office un moyen qui n'aurait pas été invoqué en défense par l'administration mais s'est borné à répondre au moyen, soulevé par le requérant, selon lequel sa mission continuait même après le 1er septembre 2004 ; que, par suite, le moyen tiré de ce que le tribunal aurait méconnu le principe du caractère contradictoire de l'instruction doit être écarté ;<br/>
<br/>
              Considérant, en deuxième lieu, que ni le jugement du 1er septembre 2004 du tribunal de commerce, ni l'arrêt du 5 octobre 2004 de la cour d'appel de Lyon n'indiquent les catégories professionnelles concernées par les licenciements économiques qu'ils autorisent ; qu'ainsi, les conditions permettant d'écarter les dispositions de l'article L. 122-12 du code du travail au profit de celles de l'article L. 621-64 du code de commerce donnant compétence à l'administrateur judiciaire de la société cédée pour procéder au licenciement économique des salariés non repris dans le cadre de la cession n'étaient pas réunies ;<br/>
<br/>
              Considérant qu'il résulte de tout ce qui précède que Me C... n'est pas fondé à se plaindre de ce que le tribunal administratif de Lyon, par son jugement du 26 juin 2007, a rejeté sa demande tendant à l'annulation des décisions de l'inspecteur du travail et du ministre lui refusant l'autorisation de licencier M.D... ;<br/>
<br/>
              Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante, la somme que demande Me C...au titre des frais exposés par lui et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : L'arrêt du 25 novembre 2008 de la cour administrative d'appel de Lyon est annulé.<br/>
<br/>
Article 2 : La requête d'appel de Me C...et le surplus des conclusions de son pourvoi sont rejetés.<br/>
<br/>
Article 3 : La présente décision sera notifiée à Me A...C..., au ministre du travail, de la solidarité et de la fonction publique et à M. B... D.... <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">66-07-01-03 TRAVAIL ET EMPLOI. LICENCIEMENTS. AUTORISATION ADMINISTRATIVE - SALARIÉS PROTÉGÉS. MODALITÉS DE DÉLIVRANCE OU DE REFUS DE L'AUTORISATION. - QUALITÉ POUR DEMANDER L'AUTORISATION - LICENCIEMENT PRÉVU DANS LE CADRE DU PLAN DE CESSION D'UNE ENTREPRISE EN REDRESSEMENT JUDICIAIRE (ART. L. 621-64 DU CODE DE COMMERCE, REPRIS À L'ARTICLE L. 631-19 ET ART. 64 DU DÉCRET DU 27 DÉCEMBRE 1985 ALORS EN VIGUEUR) - COMMISSAIRE À L'EXÉCUTION DU PLAN - CONDITIONS [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">66-07-01-04-03 TRAVAIL ET EMPLOI. LICENCIEMENTS. AUTORISATION ADMINISTRATIVE - SALARIÉS PROTÉGÉS. CONDITIONS DE FOND DE L'AUTORISATION OU DU REFUS D'AUTORISATION. LICENCIEMENT POUR MOTIF ÉCONOMIQUE. - LICENCIEMENT PRÉVU DANS LE CADRE DU PLAN DE CESSION D'UNE ENTREPRISE EN REDRESSEMENT JUDICIAIRE (ART. L. 621-64 DU CODE DE COMMERCE, REPRIS À L'ARTICLE L. 631-19 ET ART. 64 DU DÉCRET DU 27 DÉCEMBRE 1985 ALORS EN VIGUEUR) - LÉGALITÉ - CONDITIONS [RJ1].
</SCT>
<ANA ID="9A"> 66-07-01-03 Il résulte des dispositions de l'article L. 122-12 du code du travail, devenu l'article L. 1224-1 de ce code, des articles L. 621-62 et suivants du code de commerce et de l'article 64 du décret n° 85-1388 du 27 décembre 1985, alors en vigueur, que la cession de l'entreprise en redressement judiciaire arrêtée par un jugement du tribunal de commerce entraîne en principe, de plein droit, le transfert d'une entité économique autonome conservant son identité et, par voie de conséquence, la poursuite par le cessionnaire des contrats de travail attachés à l'entreprise cédée. Toutefois, il peut être dérogé à ces dispositions lorsque le plan de redressement prévoit des licenciements pour motif économique, à la double condition, prévue par les dispositions de l'article L. 621-64 du code de commerce et de l'article 64 du décret du 27 décembre 1985, d'une part, que le plan de cession ait prévu les licenciements devant intervenir dans le délai d'un mois après le jugement arrêtant le plan, d'autre part, que ce jugement indique le nombre de salariés dont le licenciement est autorisé, ainsi que les activités et catégories professionnelles concernées. Lorsque ces conditions sont remplies, c'est le commissaire à l'exécution du plan - et non le repreneur - qui a qualité pour demander à l'administration, s'il s'agit de salariés protégés, l'autorisation de licencier les salariés concernés.</ANA>
<ANA ID="9B"> 66-07-01-04-03 Il résulte des dispositions de l'article L. 122-12 du code du travail, devenu l'article L. 1224-1 de ce code, des articles L. 621-62 et suivants du code de commerce et de l'article 64 du décret n° 85-1388 du 27 décembre 1985, alors en vigueur, que la cession de l'entreprise en redressement judiciaire arrêtée par un jugement du tribunal de commerce entraîne en principe, de plein droit, le transfert d'une entité économique autonome conservant son identité et, par voie de conséquence, la poursuite par le cessionnaire des contrats de travail attachés à l'entreprise cédée. Toutefois, il peut être dérogé à ces dispositions lorsque le plan de redressement prévoit des licenciements pour motif économique, à la double condition, prévue par les dispositions de l'article L. 621-64 du code de commerce et de l'article 64 du décret du 27 décembre 1985, d'une part, que le plan de cession ait prévu les licenciements devant intervenir dans le délai d'un mois après le jugement arrêtant le plan, d'autre part, que ce jugement indique le nombre de salariés dont le licenciement est autorisé, ainsi que les activités et catégories professionnelles concernées. Lorsque ces conditions sont remplies, c'est le commissaire à l'exécution du plan - et non le repreneur - qui a qualité pour demander à l'administration, s'il s'agit de salariés protégés, l'autorisation de licencier les salariés concernés.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. Cass. soc. 10 juillet 2001, Leblanc, n° 99-44.466, Bull. V n° 254.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
