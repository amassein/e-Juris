<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025284612</ID>
<ANCIEN_ID>JG_L_2012_02_000000350899</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/28/46/CETATEXT000025284612.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 1ère sous-sections réunies, 01/02/2012, 350899</TITRE>
<DATE_DEC>2012-02-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>350899</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 1ère sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Jacques Arrighi de Casanova</PRESIDENT>
<AVOCATS>SPINOSI</AVOCATS>
<RAPPORTEUR>Mme Sophie Roussel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier de Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:350899.20120201</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 13 et 28 juillet 2011 au secrétariat du contentieux du Conseil d'Etat, présenté pour M. Christophe A, demeurant ... ; M. A demande au Conseil d'Etat :<br/>
<br/>
               1°) d'annuler l'ordonnance n°s 1102662 et 1102694 du 6 juin 2011 par laquelle le juge des référés du tribunal administratif de Versailles a rejeté ses demandes tendant, d'une part, à ce que soit ordonnée la suspension de l'exécution de la décision du 6 mai 2011 par laquelle l'administration pénitentiaire a modifié son affectation et l'a transféré de la maison centrale de Clairvaux à la maison d'arrêt de Bois-d'Arcy et à ce qu'il soit enjoint à l'administration de le réintégrer sans délai à la maison centrale de Clairvaux et, d'autre part, à ce que soit ordonnée la suspension de l'exécution de la décision du 10 mai 2011 par laquelle le garde des sceaux, ministre de la justice et des libertés a prolongé son placement à l'isolement à compter du 10 mai 2011 ;<br/>
<br/>
               2°) statuant en référé, de faire droit à ses demandes ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat une somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu le code de procédure pénale ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie Roussel, Auditeur,  <br/>
<br/>
              - les observations de Me Spinosi, avocat de M. A, <br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à Me Spinosi, avocat de M. A ; <br/>
<br/>
<br/>
<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis au juge des référés que, par une décision du 6 mai 2011, le directeur régional des services pénitentiaires a transféré M. A de la maison centrale de Clairvaux à la maison d'arrêt de Bois-d'Arcy ; que, dès son arrivée dans cette maison d'arrêt, M. A a été placé à l'isolement ; que, par une décision du 10 mai 2011, le ministre de la justice et des libertés a prolongé son placement à l'isolement ; que, par l'ordonnance attaquée du 6 juin 2011, le juge des référés du tribunal administratif de Versailles a rejeté pour défaut d'urgence la demande de M. A tendant à la suspension de l'exécution de ces deux décisions ; <br/>
<br/>
              Considérant, d'une part, qu'aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision (...) " ; <br/>
<br/>
              Considérant, d'autre part, qu'aux termes de l'article 726-1 du code de procédure pénale : " Toute personne détenue, sauf si elle est mineure, peut être placée par l'autorité administrative, pour une durée maximale de trois mois, à l'isolement par mesure de protection ou de sécurité soit à sa demande, soit d'office. Cette mesure ne peut être renouvelée pour la même durée qu'après un débat contradictoire, au cours duquel la personne concernée, qui peut être assistée de son avocat, présente ses observations orales ou écrites. L'isolement ne peut être prolongé au-delà d'un an qu'après avis de l'autorité judiciaire (...) " ; <br/>
<br/>
              Considérant que l'urgence justifie que soit prononcée la suspension d'un acte administratif lorsque l'exécution de celui-ci porte atteinte, de manière suffisamment grave et immédiate, à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre ; qu'il appartient au juge des référés d'apprécier objectivement et concrètement, compte tenu des justifications fournies par le requérant et de l'ensemble des circonstances de chaque espèce, si les effets de l'acte litigieux sont de nature à caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, l'exécution de la décision soit suspendue ; que, pour apprécier l'urgence à suspendre l'exécution d'une décision de placement d'un détenu à l'isolement ou de prolongation de cette mesure, il appartient au juge des référés de tenir compte, notamment, de la durée de la mesure d'isolement ;  <br/>
<br/>
              Considérant, en premier lieu, qu'il ressort des pièces du dossier soumis au juge des référés que, pour établir l'urgence à suspendre la décision le transférant à la maison d'arrêt de Bois d'Arcy, M. A ne faisait état que des conséquences de ce transfèrement sur ses conditions de détention ; que, le caractère suffisant de la motivation d'une ordonnance de référé devant s'apprécier au regard de l'argumentation du demandeur, le juge des référés n'a pas insuffisamment motivé son ordonnance en s'abstenant d'évoquer, à l'appui des éléments susceptibles d'établir l'existence d'une situation d'urgence pour le demandeur, d'autres éléments que ceux tenant aux conditions de détention dont M. A faisait état ;<br/>
<br/>
              Considérant, en deuxième lieu, qu'en estimant que la condition d'urgence n'était pas, par principe, constatée dans le cas de l'exécution d'une décision de mise à l'isolement d'un détenu et de prolongation de cette mesure et en appréciant concrètement, pour vérifier si cette condition était remplie, la gravité des effets de la mesure en cause sur M. A au vu de l'argumentation présentée par l'intéressé, le juge des référés n'a pas commis d'erreur de droit ;<br/>
<br/>
              Considérant, en troisième lieu, que le requérant soutient que le juge des référés, en ne faisant pas référence dans son ordonnance aux motifs qui ont fondé la décision de l'administration pénitentiaire de le placer de nouveau à l'isolement à son arrivée à la maison d'arrêt de Bois d'Arcy, l'a privé d'un recours lui permettant de bénéficier d'une protection effective de ses droits reconnus par les articles 3 et 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, alors que la mesure de mise à l'isolement contestée constitue une ingérence dans l'exercice de ces droits ; que, cependant, les décisions de mise à l'isolement prises sur le fondement des dispositions de l'article 726-1 du code de procédure pénale peuvent faire l'objet d'un recours pour excès de pouvoir ainsi que d'une demande en référé permettant à celui qui en a fait l'objet d'en obtenir, le cas échéant et en urgence, la suspension ; qu'en se prononçant en l'espèce et en l'état de l'instruction sur l'absence de situation d'urgence, alors même qu'il ne se fonde pas sur les motifs de sécurité publique invoqués par le ministre pour justifier le maintien du détenu en situation d'isolement, le juge des référés s'est livré à un examen effectif de l'argumentation présentée devant lui par le requérant ; que, par suite, le moyen tiré de ce que son droit à un recours effectif reconnu par l'article 13 de la convention précitée aurait été méconnu doit être écarté ;<br/>
<br/>
              Considérant, en dernier lieu, qu'il ressort des pièces du dossier soumis au juge des référés que la décision litigieuse de placement à l'isolement faisait suite à plusieurs mois de détention de M. A sans isolement à la maison centrale de Clairvaux ; qu'en estimant, au vu des éléments avancés par le requérant, que la condition d'urgence n'était pas satisfaite, alors même que le requérant avait déjà passé plusieurs années en quartier d'isolement et que, du fait de ce nouveau placement à l'isolement, il ne pouvait plus participer aux activités de groupe, le juge des référés n'a pas entaché de dénaturation l'appréciation qu'il a portée sur les faits de l'espèce ;<br/>
<br/>
              Considérant qu'il résulte de tout ce qui précède que M. A n'est pas fondé à demander l'annulation de l'ordonnance attaquée ; que ses conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées ;<br/>
<br/>
<br/>
<br/>
              			D E C I D E :<br/>
              			--------------<br/>
<br/>
		Article 1er : Le pourvoi de M. A est rejeté.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. Christophe A et au garde des sceaux, ministre de la justice et des libertés.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-035-02-03-02 PROCÉDURE. PROCÉDURES INSTITUÉES PAR LA LOI DU 30 JUIN 2000. RÉFÉRÉ SUSPENSION (ART. L. 521-1 DU CODE DE JUSTICE ADMINISTRATIVE). CONDITIONS D'OCTROI DE LA SUSPENSION DEMANDÉE. URGENCE. - PRÉSOMPTION D'URGENCE - ABSENCE - MESURE DE PLACEMENT D'UN DÉTENU À L'ISOLEMENT OU DE PROLONGATION DE CETTE MESURE [RJ1].
</SCT>
<ANA ID="9A"> 54-035-02-03-02 Absence de présomption d'urgence à suspendre l'exécution d'une mesure de placement d'un détenu à l'isolement ou de prolongation de cette mesure.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., antérieurement au régime défini par l'art. 726-1 du code de procédure pénal issu de la loi du 24 novembre 2009, CE, 29 décembre 2004, Ministre de la justice c/ Attou, n° 268826, T. p. 821 ; CE, 26 janvier 2007, Khider, n° 299267, T. p. 1010.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
