<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027353496</ID>
<ANCIEN_ID>JG_L_2013_04_000000343024</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/35/34/CETATEXT000027353496.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème et 9ème sous-sections réunies, 24/04/2013, 343024</TITRE>
<DATE_DEC>2013-04-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>343024</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème et 9ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MASSE-DESSEN, THOUVENIN, COUDRAY</AVOCATS>
<RAPPORTEUR>M. Thierry Carriol</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:343024.20130424</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 6 septembre et 3 décembre 2010 au secrétariat du contentieux du Conseil d'Etat, présentés pour le Syndicat CFDT Culture, dont le siège est 12, rue de Louvois à Paris (75002) ; le Syndicat CFDT Culture demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement n° 0815530 du 8 juillet 2010 par lequel le tribunal administratif de Paris a rejeté sa demande tendant à l'annulation de la décision implicite du ministre de la culture et de la communication rejetant sa demande de communication de l'indice de rémunération figurant dans le contrat de travail du directeur de l'école nationale supérieure d'architecture de Versailles ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) d'enjoindre au ministre de la culture et de la communication de communiquer, dans un délai de 15 jours à compter de la notification de la décision à intervenir, et sous astreinte de 500 euros par jour de retard, l'indice de rémunération figurant dans le contrat de travail du directeur de l'école nationale supérieure d'architecture de Versailles ;<br/>
<br/>
              4°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi n° 78-753 du 17 juillet 1978 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Thierry Carriol, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les observations de la SCP Masse-Dessen, Thouvenin, Coudray, avocat du Syndicat CFDT Culture,<br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Masse-Dessen, Thouvenin, Coudray, avocat du Syndicat CFDT Culture ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes du deuxième alinéa de l'article 1er de la loi du 17 juillet 1978, dans sa rédaction applicable à la date de la demande de communication adressée par le syndicat requérant à la commission d'accès aux documents administratifs : " Sont considérés comme documents administratifs (...) quel que soit le support utilisé pour la saisie, le stockage ou la transmission des informations qui en composent le contenu, les documents élaborés ou détenus par l'Etat (...) " ; qu'aux termes du II de l'article 6 de la même loi : " Ne sont communicables qu'à l'intéressé les documents administratifs : / - dont la communication porterait atteinte au secret de la vie privée (...) ; / - portant une appréciation ou un jugement de valeur sur une personne physique, nommément désignée ou facilement identifiable ; " ;<br/>
<br/>
              2. Considérant que, saisie par le Syndicat CFDT Culture du refus opposé par le ministre de la culture et de la communication de lui communiquer une copie du contrat de travail du directeur de l'école nationale supérieure d'architecture de Versailles, la commission d'accès aux documents administratifs a, dans sa séance du 3 juillet 2008, émis un avis favorable à la communication du contrat de travail de cet agent public, sous réserve de l'occultation préalable de toutes les mentions couvertes par la vie privée ou susceptibles de révéler la manière de servir de l'agent ; que le tribunal administratif de Paris, saisi par le syndicat, après avoir prononcé un non-lieu du fait de la production, par le ministre, du contrat de travail litigieux, a jugé que le ministre avait à bon droit occulté l'indice de rémunération de cet agent, au motif qu'il n'avait pu l'arrêter qu'après avoir porté une appréciation sur la valeur de l'agent ; <br/>
<br/>
              3. Considérant que le contrat de travail d'un agent public est un document administratif librement communicable à toute personne qui en fait la demande en application des dispositions de la loi du 17 juillet 1978 sous réserve que soient occultées préalablement à la communication toutes les mentions qui porteraient atteinte à la protection de la vie privée ou comporteraient une appréciation ou un jugement sur la valeur de l'agent public en cause ; que lorsque la rémunération qui figure dans ce contrat de travail résulte de l'application des règles régissant l'emploi concerné, sa communication n'est pas susceptible de révéler une appréciation ou un jugement de valeur, au sens des dispositions du II de l'article 6 de la loi du 17 juillet 1978, sur la personne recrutée ; qu'en revanche, lorsqu' elle est arrêtée d'un commun accord entre les parties sans référence à des règles la déterminant, elle révèle nécessairement une appréciation et un jugement de valeur portés sur la personne recrutée ; que la communication du contrat ne peut dans ce cas intervenir qu'après occultation des éléments relatifs à la rémunération ; <br/>
<br/>
              4. Considérant qu'il s'en suit qu'en regardant par principe la fixation d'une rémunération par un contrat comme révélatrice d'une appréciation portée sur la valeur de la personne recrutée, sans rechercher si des règles de droit applicables à l'emploi considéré n'en déterminaient pas la rémunération, le tribunal administratif a entaché son jugement d'une erreur de droit ; que le Syndicat CFDT Culture est fondé pour ce motif, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, à en demander l'annulation ; <br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser au Syndicat CFDT Culture, au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>				D E C I D E  :<br/>
              				--------------<br/>
<br/>
Article 1er : Le jugement du tribunal administratif de Paris du 8 juillet 2010 est annulé.<br/>
<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Paris.<br/>
Article 3 : L'Etat versera au Syndicat CFDT Culture la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée au Syndicat CFDT Culture et à la ministre de la culture et de la communication.<br/>
Copie en sera adressée pour information à M. A...B...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">26-06-01-02-02 DROITS CIVILS ET INDIVIDUELS. ACCÈS AUX DOCUMENTS ADMINISTRATIFS. ACCÈS AUX DOCUMENTS ADMINISTRATIFS AU TITRE DE LA LOI DU 17 JUILLET 1978. DROIT À LA COMMUNICATION. DOCUMENTS ADMINISTRATIFS COMMUNICABLES. - CONTRAT DE TRAVAIL D'UN AGENT PUBLIC - CONDITION - OCCULTATION PRÉALABLE, AVANT COMMUNICATION À DES TIERS, DES MENTIONS COMPORTANT UNE APPRÉCIATION OU UN JUGEMENT DE VALEUR SUR L'AGENT PUBLIC - CONSÉQUENCE - RÉMUNÉRATION - 1) RÉMUNÉRATION RÉSULTANT DE L'APPLICATION DES RÈGLES RÉGISSANT L'EMPLOI - OCCULTATION - ABSENCE - 2) RÉMUNÉRATION N'ÉTANT PAS DÉTERMINÉE PAR DES RÈGLES - OCCULTATION - EXISTENCE [RJ1].
</SCT>
<ANA ID="9A"> 26-06-01-02-02 Le contrat de travail d'un agent public est un document administratif librement communicable à toute personne qui en fait la demande en application des dispositions de la loi n° 78-753 du 17 juillet 1978 sous réserve que soient occultées préalablement à la communication toutes les mentions qui porteraient atteinte à la protection de la vie privée ou comporteraient une appréciation ou un jugement sur la valeur de l'agent public en cause.... ,,1) Lorsque la rémunération qui figure dans ce contrat de travail résulte de l'application des règles régissant l'emploi en cause, sa communication n'est pas susceptible de révéler une appréciation ou un jugement de valeur, au sens des dispositions du II de l'article 6 de la loi du 17 juillet 1978, sur la personne recrutée. Pas de nécessité d'occultation.... ,,2) En revanche, lorsqu'elle est arrêtée d'un commun accord entre les parties sans être déterminée par de telles règles, elle révèle nécessairement une appréciation et un jugement de valeur portés sur la personne recrutée. La communication du contrat ne peut dans ce cas intervenir qu'après occultation des éléments relatifs à la rémunération.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr. CE, 10 mars 2010, Commune de Sète, n° 303814, p. 70.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
