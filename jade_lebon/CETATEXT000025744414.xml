<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025744414</ID>
<ANCIEN_ID>JG_L_2012_04_000000329737</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/74/44/CETATEXT000025744414.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 4ème sous-sections réunies, 24/04/2012, 329737</TITRE>
<DATE_DEC>2012-04-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>329737</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 4ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Philippe Martin</PRESIDENT>
<AVOCATS>LE PRADO ; SCP GADIOU, CHEVALLIER ; SCP BLANC, ROUSSEAU ; SCP ODENT, POULET</AVOCATS>
<RAPPORTEUR>M. Philippe Ranquet</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Jean-Philippe Thiellay</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:329737.20120424</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 15 juillet et 15 octobre 2009 au secrétariat du contentieux du Conseil d'Etat, présentés pour le CENTRE HOSPITALIER INTERCOMMUNAL DE SEVRES, dont le siège est au 141 Grande rue à Sèvres (92310) ; le CENTRE HOSPITALIER INTERCOMMUNAL DE SEVRES demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 04PA01115 du 11 mai 2009 de la cour administrative d'appel de Paris dans la mesure où par cet arrêt, statuant sur les préjudices et réformant le jugement n° 0017139 du 20 janvier 2004 du tribunal administratif de Paris, elle l'a condamné à verser à Mlle Sandra A une rente annuelle de 30 378 euros au titre des frais liés au handicap et à la caisse de prévoyance et de retraite de la Société nationale des chemins de fer français (SNCF) la somme totale de 556 856,93 euros ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses conclusions d'appel tendant à la réformation du jugement en ce qui concerne les droits à indemnisation de la caisse de prévoyance et de retraite de la SNCF et leur imputation sur les droits à indemnisation de Mlle A ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code civil ;<br/>
<br/>
              Vu le code de la sécurité sociale ;<br/>
<br/>
              Vu la loi n° 68-1250 du 31 décembre 1968 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Philippe Ranquet, Maître des Requêtes,  <br/>
<br/>
              - les observations de Me Le Prado, avocat du CENTRE HOSPITALIER INTERCOMMUNAL DE SEVRES, de la SCP Gadiou, Chevallier, avocat de M. Roland A, de la SCP Blanc, Rousseau, avocat de la caisse primaire d'assurance maladie de la Haute-Vienne et de la SCP Odent, Poulet, avocat de la caisse de prévoyance et de retraite de la SNCF, <br/>
<br/>
              - les conclusions de M. Jean-Philippe Thiellay, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à Me Le Prado, avocat du CENTRE HOSPITALIER INTERCOMMUNAL DE SEVRES, à la SCP Gadiou, Chevallier, avocat de M. Roland A, à la SCP Blanc, Rousseau, avocat de la caisse primaire d'assurance maladie de la Haute-Vienne et à la SCP Odent, Poulet, avocat de la caisse de prévoyance et de retraite de la SNCF ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la jeune Sandra A, née le 5 mars 1981, a subi au CENTRE HOSPITALIER INTERCOMMUNAL DE SEVRES, le 19 novembre 1981, une intervention chirurgicale au cours de laquelle une compresse a été oubliée dans le pharynx de l'enfant, ce qui a causé un arrêt cardio-respiratoire et de graves séquelles neurologiques ; que M. et Mme A, en leur qualité de représentants légaux de leur fille, ont conclu le 23 avril 1985 une transaction avec l'assureur de l'établissement, relative à l'indemnisation des troubles de toute nature dans les conditions d'existence de la victime jusqu'à sa majorité, puis, après cette majorité, ont poursuivi l'établissement devant le tribunal administratif de Paris aux fins d'obtenir l'indemnisation définitive des préjudices ; que sont intervenues à cette instance la caisse de prévoyance et de retraite de la Société nationale des chemins de fer français (SNCF), à laquelle Mlle Sandra A était affiliée jusqu'au 28 septembre 2001, au titre des débours exposés jusqu'à cette date, et la caisse primaire d'assurance maladie de la Haute Vienne au titre des débours ultérieurs ; que le CENTRE HOSPITALIER INTERCOMMUNAL DE SEVRES se pourvoit en cassation contre l'arrêt du 11 mai 2009 de la cour administrative d'appel de Paris dans la mesure où, statuant sur les préjudices et réformant le jugement du 20 janvier 2004 du tribunal administratif de Paris, elle l'a condamné à verser à Mlle Sandra A une rente annuelle de 30 378 euros au titre des frais liés au handicap et à la caisse de prévoyance et de retraite de la SNCF la somme totale de 556 856,93 euros ;<br/>
<br/>
              Sans qu'il soit besoin d'examiner les autres moyens du pourvoi ;<br/>
<br/>
              Considérant qu'aux termes de l'article 1er de la loi du 31 décembre 1968 relative à la prescription des créances sur l'Etat, les départements, les communes et les établissements publics : " Sont prescrites, au profit de l'Etat, des départements et des communes, sans préjudice des déchéances particulières édictées par la loi et sous réserve des dispositions de la présente loi, toutes créances qui n'ont pas été payées dans un délai de quatre ans à partir du premier jour de l'année suivant celle au cours de laquelle les droits ont été acquis. / Sont prescrites, dans le même délai et sous la même réserve, les créances sur les établissements publics dotés d'un comptable public " ; qu'aux termes de son article 2 : " La prescription est interrompue par : / Toute demande de paiement ou toute réclamation écrite adressée par un créancier à l'autorité administrative, dès lors que la demande ou la réclamation a trait au fait générateur, à l'existence, au montant ou au paiement de la créance, alors même que l'administration saisie n'est pas celle qui aura finalement la charge du règlement ; / Tout recours formé devant une juridiction, relatif au fait générateur, à l'existence, au montant ou au paiement de la créance, quel que soit l'auteur du recours et même si la juridiction saisie est incompétente pour en connaître, et si l'administration qui aura finalement la charge du règlement n'est pas partie à l'instance ; / Toute communication écrite d'une administration intéressée, même si cette communication n'a pas été faite directement au créancier qui s'en prévaut, dès lors que cette communication a trait au fait générateur, à l'existence, au montant ou au paiement de la créance ; / Toute émission de moyen de règlement, même si ce règlement ne couvre qu'une partie de la créance ou si le créancier n'a pas été exactement désigné. (...) " ; qu'aux termes de son article 3 : " La prescription ne court ni contre le créancier qui ne peut agir (...) ni contre celui qui peut être légitimement regardé comme ignorant l'existence de sa créance (...) " ; <br/>
<br/>
              Considérant qu'en vertu de la subrogation régie par l'article L. 376-1 du code de la sécurité sociale, les effets susceptibles de s'attacher quant au cours de la prescription quadriennale à un acte accompli par l'assuré victime d'un accident peuvent en principe être valablement invoqués par la caisse de sécurité sociale lui ayant versé des prestations à raison du dommage ; que toutefois, les organismes de sécurité sociale peuvent exercer leur recours subrogatoire à l'encontre du tiers responsable alors même que la victime s'est pour sa part abstenue d'introduire un recours indemnitaire ; que la circonstance que la victime ne puisse agir est par conséquent sans effet, par elle-même, sur le cours de la prescription susceptible d'être opposée aux créances que la caisse détient par subrogation ;<br/>
<br/>
              Considérant, dès lors, qu'en jugeant que la transaction conclue entre l'assureur du centre hospitalier et M. et Mme A, par laquelle ces derniers s'étaient interdit d'agir en justice pour faire valoir les droits de leur fille jusqu'à la date de sa majorité, avait aussi pour effet de suspendre la prescription des créances de la caisse de prévoyance et de retraite de la SNCF jusqu'à cette date, alors que cette transaction à laquelle le tiers payeur n'était pas partie ne lui était pas opposable et ne lui interdisait pas d'agir aux fins de remboursement de ces créances, la cour administrative d'appel a commis une erreur de droit ; que le CENTRE HOSPITALIER INTERCOMMUNAL DE SEVRES est, par suite, fondé à demander l'annulation de l'arrêt attaqué en tant qu'il statue sur les postes de préjudice au titre desquels l'exception de prescription des créances de la caisse a été écartée, c'est-à-dire sur les dépenses de santé et sur les frais liés au handicap ;<br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler dans cette mesure l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              Sur l'exception de prescription opposée aux créances de la caisse de prévoyance et de retraite de la SNCF :<br/>
<br/>
              Considérant que le délai de quatre ans prévu par les dispositions précitées de l'article 1er de la loi du 31 décembre 1968 court, en ce qui concerne les frais supportés par la caisse de sécurité sociale avant la date de consolidation des dommages, au premier jour de l'année suivant celle au cours de laquelle les dépenses ont été exposées ; qu'ainsi qu'il a été dit ci-dessus, la circonstance que M. et Mme A se soient interdit d'agir en justice entre la conclusion de la transaction du 23 avril 1985 et la majorité de leur fille est restée sans incidence sur le cours de la prescription des créances nées des débours exposés par la caisse de prévoyance et de retraite de la SNCF ; que contrairement à ce que soutient cette dernière, le centre hospitalier ne peut davantage être regardé comme ayant consenti, par la transaction, à un report de la date dont court la prescription des mêmes créances ;<br/>
<br/>
              Considérant, il est vrai, que la caisse soutient également que les versements trimestriels de la rente prévue par la transaction constituent des émissions de moyens de paiement, au sens de l'article 2 de la loi du 31 décembre 1968, qui ont interrompu le délai de prescription pendant la même période ; qu'il résulte toutefois de l'instruction que les stipulations de la transaction prévoyaient également la suspension des versements en cas d'accueil de la jeune Sandra A en institution spécialisée et que l'intéressée a été accueillie dans une telle institution du 1er septembre 1986 au 5 mars 2001 ; que dès lors, en l'absence d'élément de nature à démontrer que la rente aurait malgré cela continué d'être acquittée, il n'y a pas lieu de retenir que la prescription était interrompue pendant cette période ;<br/>
<br/>
              Considérant qu'il résulte de ce qui précède que les créances de la caisse de prévoyance et de retraite de la SNCF afférentes aux débours exposés jusqu'au 31 décembre 1996 étaient prescrites à la date du 28 février 2001 où elle est intervenue à l'instance ; que le CENTRE HOSPITALIER INTERCOMMUNAL DE SEVRES est, par suite, fondé à soutenir que c'est à tort que le tribunal administratif a écarté l'exception de prescription opposée aux demandes correspondantes de cette caisse ;<br/>
<br/>
              Sur les dépenses de santé :<br/>
<br/>
              Considérant que les frais de soins et d'appareillage que la caisse de prévoyance et de retraite de la SNCF justifie avoir exposés du 1er janvier 1997 à la date du changement d'affiliation de Mlle Sandra A s'élèvent à un montant de 28 013 euros, qu'il y a lieu de mettre à la charge du CENTRE HOSPITALIER INTERCOMMUNAL DE SEVRES ;<br/>
<br/>
              Considérant que la caisse d'assurance maladie de la Haute-Vienne justifie avoir exposé de la date du changement d'affiliation au 24 octobre 2003 des frais de soins et d'appareillage pour un montant de 11 671,56 euros, qu'il y a lieu de mettre à la charge du CENTRE HOSPITALIER INTERCOMMUNAL DE SEVRES ; que cette caisse justifie également devoir exposer de tels frais après cette dernière date et en produit une estimation prévisionnelle s'élevant à 323 635,95 euros ; qu'il y a lieu de lui en allouer, dans la limite de ce montant, le remboursement sur justificatifs à mesure de leur engagement ;<br/>
<br/>
              Sur les frais liés au handicap :<br/>
<br/>
              Considérant que la caisse de prévoyance et de retraite de la SNCF justifie avoir exposé au titre de l'accueil de Mlle Sandra A en institution spécialisée, du 1er janvier 1997 à son vingtième anniversaire le 5 mars 2001, la somme de 170 534,95 euros ; qu'il y a lieu de mettre cette somme à la charge du CENTRE HOSPITALIER INTERCOMMUNAL DE SEVRES ;<br/>
<br/>
              Considérant qu'il résulte de l'instruction que depuis le 5 mars 2001, Mlle Sandra A est accueillie au domicile familial et qu'aucun placement en institution spécialisée n'est envisagé ; que l'état de complète dépendance dans lequel se trouve l'intéressée est à l'origine de besoins en assistance d'une tierce personne, en aménagement du véhicule et du domicile familiaux et en aides techniques dont il sera fait une juste appréciation en les évaluant à une rente annuelle versée à compter du 5 mars 2001 et d'un montant de 70 000 euros à cette date ; que doit être déduit de cette rente le montant des provisions déjà reçues, au titre des mêmes chefs de préjudice, en exécution d'ordonnances du juge des référés du tribunal administratif de Paris ; qu'en revanche, contrairement à ce que soutient le CENTRE HOSPITALIER INTERCOMMUNAL DE SEVRES, il n'y a lieu d'imputer sur cette rente aucun des droits de la caisse de prévoyance et de retraite de la SNCF à raison de ses débours afférents au placement de la victime en institution spécialisée avant le 5 mars 2001, qui réparent une fraction distincte du poste de préjudice des frais liés au handicap ;<br/>
<br/>
              Considérant qu'il y a lieu, pour procéder à l'imputation des sommes reçues à titre de provision, de convertir la rente annuelle en capital, selon un barème reposant sur la table de mortalité de 2001 pour les femmes publiée par l'Institut national de la statistique et des études économiques et un taux d'intérêt de 3,20 %, d'où résulte, pour une victime âgée de vingt ans à la date où il convient de se placer pour la détermination de la rente, un coefficient de capitalisation de 27,483 ; que du capital ainsi obtenu, soit 1 923 810 euros, doivent être déduits l'encours total des rentes versées en exécution de l'ordonnance n° 0100794 du 12 mars 2001, soit 25 916,50 euros, et le capital alloué par l'ordonnance n° 0105874 du 20 juin 2001, soit 22 867,35 euros ; que le solde, soit un capital de 1 875 026,15 euros, correspond selon le même barème à une rente annuelle de 68 224,95 euros ; qu'il y a ainsi lieu de mettre à la charge du CENTRE HOSPITALIER INTERCOMMUNAL DE SEVRES une rente versée par trimestres échus fixée à ce montant annuel à la date du 5 mars 2001 et revalorisé par la suite par application des coefficients prévus à l'article L. 434-17 du code de la sécurité sociale ;<br/>
<br/>
              Sur les intérêts :<br/>
<br/>
              Considérant que les sommes dues en capital à la caisse de prévoyance et de retraite de la SNCF porteront intérêts au taux légal à compter du 28 février 2001, date d'enregistrement de son mémoire en intervention au greffe du tribunal administratif de Paris ; que les arrérages de la rente due à M. et Mme A porteront intérêts au taux légal à compter de leur date d'échéance respective et jusqu'à leur paiement ;<br/>
<br/>
              Sur l'application des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              Considérant que ces dispositions font obstacle à ce que soient mises à la charge du CENTRE HOSPITALIER INTERCOMMUNAL DE SEVRES, qui n'est pas la partie perdante dans la présente instance, les sommes demandées par M. et Mme A et par la caisse de prévoyance et de retraite de la SNCF au titre des frais exposés devant le Conseil d'Etat et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les articles 2 à 5 de l'arrêt du 11 mai 2009 de la cour administrative d'appel de Paris sont annulés.<br/>
<br/>
Article 2 : Le CENTRE HOSPITALIER INTERCOMMUNAL DE SEVRES est condamné à verser par trimestre échu à compter du 5 mars 2001 à M. et à Mme Roland A, en leur qualité de représentants légaux de leur fille Sandra, une rente dont le montant annuel, fixé à cette date à 68 224,95 euros, sera revalorisé par la suite par application des coefficients prévus à l'article L. 434-17 du code de la sécurité sociale. Ses arrérages portent intérêts au taux légal à compter de leur date d'échéance respective et jusqu'à leur paiement.<br/>
<br/>
Article 3 : Le CENTRE HOSPITALIER INTERCOMMUNAL DE SEVRES est condamné à verser à la caisse de prévoyance et de retraite de la SNCF la somme de 198 547,95 euros, assortie des intérêts au taux légal à compter du 28 février 2001.<br/>
<br/>
Article 4 : Le CENTRE HOSPITALIER INTERCOMMUNAL DE SEVRES est condamné à verser à la caisse primaire d'assurance maladie de la Haute-Vienne la somme de 11 671,56 euros et à lui rembourser les frais de soins et d'appareillage exposés à compter du 24 octobre 2003 à raison du dommage subi par Mlle Sandra A, sur justificatifs à mesure de leur engagement, dans la limite de 323 635,95 euros.<br/>
<br/>
Article 5 : Le jugement du 20 janvier 2004 du tribunal administratif de Paris est réformé en ce qu'il a de contraire à la présente décision.<br/>
<br/>
Article 6 : Le surplus des conclusions du CENTRE HOSPITALIER INTERCOMMUNAL DE SEVRES, de M. et Mme A, de la caisse de prévoyance et de retraite de la SNCF et de la caisse primaire d'assurance maladie de la Haute-Vienne devant la cour administrative d'appel de Paris et les conclusions de M. et Mme A et de la caisse de prévoyance et de retraite de la SNCF au titre de l'article L. 761-1 du code de justice administrative devant le Conseil d'Etat sont rejetés.<br/>
<br/>
Article 7 : La présente décision sera notifiée au CENTRE HOSPITALIER INTERCOMMUNAL DE SEVRES, à M. et Mme Roland A, à la caisse de prévoyance et de retraite de la Société nationale des chemins de fer français, à la caisse primaire d'assurance maladie de la Haute-Vienne et à la Société hospitalière d'assurances mutuelles.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">60-02-01-01 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RESPONSABILITÉ EN RAISON DES DIFFÉRENTES ACTIVITÉS DES SERVICES PUBLICS. SERVICE PUBLIC DE SANTÉ. ÉTABLISSEMENTS PUBLICS D'HOSPITALISATION. - ACTION SUBROGATOIRE D'UNE CAISSE - TRANSACTION ENTRE L'HÔPITAL ET LA VICTIME - CIRCONSTANCE INOPPOSABLE À LA CAISSE TIERCE À CETTE TRANSACTION.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">60-05-04-01-01 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RECOURS OUVERTS AUX DÉBITEURS DE L'INDEMNITÉ, AUX ASSUREURS DE LA VICTIME ET AUX CAISSES DE SÉCURITÉ SOCIALE. DROITS DES CAISSES DE SÉCURITÉ SOCIALE. IMPUTATION DES DROITS À REMBOURSEMENT DE LA CAISSE. ARTICLE L. 376-1 (ANCIEN ART. L. 397) DU CODE DE LA SÉCURITÉ SOCIALE. - ACTION SUBROGATOIRE D'UNE CAISSE - TRANSACTION ENTRE L'HÔPITAL ET LA VICTIME - CIRCONSTANCE INOPPOSABLE À LA CAISSE TIERCE À CETTE TRANSACTION.
</SCT>
<ANA ID="9A"> 60-02-01-01 En vertu de la subrogation régie par l'article L. 376-1 du code de la sécurité sociale, les effets susceptibles de s'attacher quant au cours de la prescription quadriennale à un acte accompli par l'assuré victime d'un accident peuvent en principe être valablement invoqués par la caisse de sécurité sociale lui ayant versé des prestations à raison du dommage. Toutefois, les organismes de sécurité sociale peuvent exercer leur recours subrogatoire à l'encontre du tiers responsable alors même que la victime s'est pour sa part abstenue d'introduire un recours indemnitaire. La circonstance que la victime ne puisse agir est par conséquent sans effet, par elle-même, sur le cours de la prescription susceptible d'être opposée aux créances que la caisse détient par subrogation. Dès lors, une transaction conclue entre l'hôpital et la victime n'est pas opposable à la caisse et ne lui interdit pas d'agir aux fins de remboursement des créances.</ANA>
<ANA ID="9B"> 60-05-04-01-01 En vertu de la subrogation régie par l'article L. 376-1 du code de la sécurité sociale, les effets susceptibles de s'attacher quant au cours de la prescription quadriennale à un acte accompli par l'assuré victime d'un accident peuvent en principe être valablement invoqués par la caisse de sécurité sociale lui ayant versé des prestations à raison du dommage. Toutefois, les organismes de sécurité sociale peuvent exercer leur recours subrogatoire à l'encontre du tiers responsable alors même que la victime s'est pour sa part abstenue d'introduire un recours indemnitaire. La circonstance que la victime ne puisse agir est par conséquent sans effet, par elle-même, sur le cours de la prescription susceptible d'être opposée aux créances que la caisse détient par subrogation. Dès lors, une transaction conclue entre l'hôpital et la victime n'est pas opposable à la caisse et ne lui interdit pas d'agir aux fins de remboursement des créances.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
