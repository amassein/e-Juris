<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026141366</ID>
<ANCIEN_ID>JG_L_2012_07_000000337698</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/14/13/CETATEXT000026141366.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 04/07/2012, 337698, Publié au recueil Lebon</TITRE>
<DATE_DEC>2012-07-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>337698</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>M. Gaël Raimbault</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Claire Landais</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:337698.20120704</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 18 mars et 18 juin 2010 au secrétariat du contentieux du Conseil d'Etat, présentés pour la fédération nationale des transports routiers, dont le siège est 6 rue Ampère à Paris (75017) ; la fédération nationale des transports routiers demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêté du 12 février 2010 du ministre de l'écologie, de l'énergie, du développement durable et de la mer et du ministre du travail, des relations sociales, de la famille, de la solidarité et de la ville, portant extension d'un accord et d'un avenant conclus le 14 décembre 2009 dans le cadre de la convention collective nationale des transports routiers et des activités auxiliaires du transport du 21 décembre 1950 ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement de la somme de 6 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code du travail ;<br/>
<br/>
	Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Gaël Raimbault, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les observations de la SCP Lyon-Caen, Thiriez, avocat de la fédération nationale des transports routiers,<br/>
<br/>
              - les conclusions de Mme Claire Landais, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Lyon-Caen, Thiriez, avocat de la fédération nationale des transports routiers ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 2261-19 du code du travail : " Pour pouvoir être étendus, la convention de branche ou l'accord professionnel ou interprofessionnel, leurs avenants ou annexes, doivent avoir été négociés et conclus en commission paritaire. / Cette commission est composée de représentants des organisations syndicales d'employeurs et de salariés représentatives dans le champ d'application considéré " ; que, si ces dispositions ne font pas obstacle à ce que soient conduites des consultations entre les participants aux négociations, elles soumettent en revanche la légalité de l'extension des accords à la condition que les étapes essentielles de la négociation de ceux-ci se soient déroulées en présence de toutes les organisations syndicales représentatives dans leur champ d'application ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier que l'avenant du 14 décembre 2009 à la convention collective nationale des transports routiers et des activités auxiliaires du transport et l'accord salarial du même jour, étendus par l'arrêté attaqué, ont fait l'objet de premières négociations en commission paritaire entre les mois de février et d'octobre 2009, puis d'une adoption lors d'une réunion de la même commission paritaire le 14 décembre 2009 ; que toutefois, dans l'intervalle, au motif que les difficultés rencontrées dans cette négociation créaient un risque de grève dans les transports routiers, le secrétaire d'Etat aux transports a procédé, les 9, 10 et 11 décembre 2009, à des consultations avec certaines fédérations de salariés et d'employeurs engagées dans la négociation ; <br/>
<br/>
              3. Considérant que ces consultations, qui se sont déroulées sous la forme de discussions collectives organisées par les pouvoirs publics entre les organisations syndicales de salariés et d'employeurs qui y avaient été conviées, qui ont porté sur les sujets en débat dans les négociations alors en cours, et qui ont abouti à la signature, par certains des participants, d'un " protocole d'accord " qui a servi de base à l'avenant conventionnel et à l'accord salarial signés à l'issue de la réunion paritaire du 14 décembre suivant, constituaient en réalité, en l'espèce, une étape essentielle de la négociation de l'avenant conventionnel et de l'accord salarial étendus par l'arrêté attaqué et non pas de simples consultations informelles ;<br/>
<br/>
              4. Considérant que la fédération nationale des transports routiers soutient notamment, sans être contredite, que les réunions des 9, 10 et 11 décembre 2009 se sont déroulées sans qu'ait été invitée une des organisations syndicales de salariés représentative des chauffeurs routiers ; que l'avenant conventionnel et l'accord salarial du 14 décembre 2009 n'ont, ainsi, pas fait l'objet d'une négociation conduite avec l'ensemble des organisations syndicales de salariés représentatives dans leur champ d'application ; qu'il résulte des dispositions citées ci-dessus de l'article L. 2261-19 du code du travail que le ministre du travail, des relations sociales, de la famille, de la solidarité et de la ville ne pouvait, en conséquence, légalement procéder à leur extension ; que l'arrêté d'extension du 12 février 2010 doit donc être annulé ; qu'il convient toutefois de surseoir à statuer sur la date d'effet de cette annulation, jusqu'à ce que les parties aient débattu de la question de savoir s'il y a lieu, en l'espèce, de limiter dans le temps les effets de l'annulation ainsi prononcée ; <br/>
<br/>
              5. Considérant enfin qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement à la fédération nationale des transports routiers d'une somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêté du 12 février 2010 du ministre de l'écologie, de l'énergie, du développement durable et de la mer et du ministre du travail, des relations sociales, de la famille, de la solidarité et de la ville est annulé.<br/>
Article 2 : Il est sursis à statuer sur la date d'effet de l'annulation prononcée à l'article 1er, jusqu'à ce que les parties aient débattu de la question de savoir s'il y a lieu, en l'espèce, de limiter dans le temps les effets de cette annulation.<br/>
Article 3 : L'Etat versera à la fédération nationale des transports routiers la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la fédération nationale des transports routiers, à la ministre de l'écologie, du développement durable et de l'énergie et au ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social.<br/>
Copie en sera adressée pour information à la fédération des entreprises de transport et logistique de France, à la fédération nationale des transports et de la logistique Force ouvrière, à la fédération générale des transports et de l'équipement - CFDT et à la fédération générale CFTC des transports.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">66-02-015 TRAVAIL ET EMPLOI. CONVENTIONS COLLECTIVES. ÉLABORATION DES CONVENTIONS COLLECTIVES. - DISCUSSIONS COLLECTIVES MENÉES HORS COMMISSION PARITAIRE AVEC CERTAINES SEULEMENT DES ORGANISATIONS SYNDICALES - IRRÉGULARITÉ - EXISTENCE, SI LES DISCUSSIONS ONT CONSTITUÉ UNE ÉTAPE ESSENTIELLE DE LA NÉGOCIATION.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">66-02-02-035 TRAVAIL ET EMPLOI. CONVENTIONS COLLECTIVES. EXTENSION DES CONVENTIONS COLLECTIVES. CONDITION DE LÉGALITÉ DE L'EXTENSION TENANT À LA VALIDITÉ DE LA CONVENTION. - DISCUSSIONS COLLECTIVES MENÉES HORS COMMISSION PARITAIRE AVEC CERTAINES SEULEMENT DES ORGANISATIONS SYNDICALES - IRRÉGULARITÉ - EXISTENCE, SI LES DISCUSSIONS ONT CONSTITUÉ UNE ÉTAPE ESSENTIELLE DE LA NÉGOCIATION - CONSÉQUENCE - ILLÉGALITÉ DE L'ARRÊTÉ D'EXTENSION.
</SCT>
<ANA ID="9A"> 66-02-015 Aux termes de l'article L. 2261-19 du code du travail :  Pour pouvoir être étendus, la convention de branche ou l'accord professionnel ou interprofessionnel, leurs avenants ou annexes, doivent avoir été négociés et conclus en commission paritaire .... ...Des discussions collectives organisées par les pouvoirs publics avec certaines seulement des organisations syndicales de salariés et d'employeurs faisant partie de la commission paritaire sont, si elles ont constitué une étape essentielle de la négociation et non pas de simples consultations informelles, de nature à entacher d'illégalité interne l'extension de l'accord ou de l'avenant ultérieurement adopté par la commission paritaire.</ANA>
<ANA ID="9B"> 66-02-02-035 Aux termes de l'article L. 2261-19 du code du travail :  Pour pouvoir être étendus, la convention de branche ou l'accord professionnel ou interprofessionnel, leurs avenants ou annexes, doivent avoir été négociés et conclus en commission paritaire .... ...Des discussions collectives organisées par les pouvoirs publics avec certaines seulement des organisations syndicales de salariés et d'employeurs faisant partie de la commission paritaire sont, si elles ont constitué une étape essentielle de la négociation et non pas de simples consultations informelles, de nature à entacher d'illégalité interne l'extension de l'accord ou de l'avenant ultérieurement adopté par la commission paritaire.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
