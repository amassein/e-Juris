<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036631182</ID>
<ANCIEN_ID>JG_L_2018_02_000000390601</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/63/11/CETATEXT000036631182.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 5ème chambres réunies, 21/02/2018, 390601</TITRE>
<DATE_DEC>2018-02-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>390601</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR>M. Jean-Baptiste de Froment</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Julie Burguburu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:390601.20180221</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme F...et autres ont demandé au tribunal administratif de Strasbourg d'annuler pour excès de pouvoir les décisions implicites nées du silence gardé par le préfet après leurs demandes d'abrogation de l'article 29 de l'arrêté préfectoral du 11 septembre 1998 et enjoignant au préfet d'abroger cet article 29. Par un jugement nos 1103996, 1103997 du 12 mars 2013, le tribunal administratif a fait droit à cette demande.<br/>
<br/>
              Par un arrêt n° 13NC00877 du 24 mars 2015, la cour administrative d'appel de Nancy a rejeté l'appel formé par le préfet du Bas-Rhin contre ce jugement du 12 mars 2013. <br/>
<br/>
              Par un pourvoi enregistré le 1er juin 2015 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'intérieur demande au Conseil d'Etat d'annuler cet arrêt.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              -	le code général des collectivités territoriales ;<br/>
              -	la loi n° 82-213 du 2 mars 1982 ;<br/>
              -	le décret n° 2004-374 du 29 avril 2004 ;<br/>
              -	le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Baptiste de Froment, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Julie Burguburu, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Spinosi, Sureau, avocat de MmeF..., et autres ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 9 juillet 2008, le préfet du Bas-Rhin a notamment modifié l'article 29 du règlement départemental des taxis du département du Bas-Rhin, adopté par un arrêté du 11 septembre 1998 ; que cette modification a consisté, d'une part, à limiter, sous peine de sanction disciplinaire, la durée de stationnement sur la voie publique hors de son ressort géographique d'un taxi ayant fait l'objet d'une réservation à 30 minutes préalablement à l'heure de la réservation, d'autre part, à imposer aux taxis, pour justifier d'une réservation préalable, de disposer d'un carnet numéroté, agenda ou tout autre support dédié à cet effet, sur lequel seraient portées les réservations dans l'ordre de réception ; que par un jugement du 12 mars 2013, le tribunal administratif de Strasbourg a annulé les décisions implicites de rejet résultant du silence gardé par le préfet du Bas-Rhin sur les demandes qui lui avaient été adressées tendant à l'abrogation de l'article 29 modifié de l'arrêté préfectoral du 11 septembre 1998 ; que par un arrêt du 24 mars 2015, contre lequel le ministre de l'intérieur se pourvoit en cassation, la cour administrative d'appel de Nancy a confirmé ce jugement ;  <br/>
<br/>
              2. Considérant que, en vertu de l'article L. 2542-1 du code général des collectivités territoriales, les dispositions de l'article L. 2215-1 du même code, qui définissent les pouvoirs de police du maire et du représentant de l'Etat dans les départements, ne sont pas applicables dans les départements de la Moselle, du Bas-Rhin et du Haut-Rhin ; que, toutefois, le représentant de l'Etat dans l'un de ces départements est compétent pour prendre, en vertu des pouvoirs de police générale dont il dispose sur le fondement du I de l'article 34 de la loi du 2 mars 1982 relative aux droits et libertés des communes, des départements et des régions et de l'article 11 du décret du 29 avril 2004 relatif aux pouvoirs des préfets, les mesures qu'il estime nécessaires pour faire respecter l'ordre, la sûreté, la sécurité et la salubrité publiques et qui, eu égard à leur nature et à leur objet, doivent être prises à une échelle qui excède le territoire d'une seule commune ; que, dès lors, en déniant toute compétence au préfet du Bas-Rhin pour prendre des règlements de police, la cour administrative d'appel de Nancy a commis une erreur de droit ; que, par suite, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, son arrêt doit être annulé ;<br/>
<br/>
              3. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'arrêt du 24 mars 2015 de la cour administrative d'appel de Nancy est annulé.<br/>
<br/>
      Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nancy. <br/>
<br/>
Article 3 : Les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative par Mme F...et autres sont rejetées.<br/>
<br/>
Article 4 : La présente décision sera notifiée au ministre d'Etat, ministre de l'intérieur et à Mme F..., premier défendeur dénommé, pour l'ensemble des défendeurs.<br/>
Copie en sera adressée à MM. A...I..., E...D..., C...J..., B...G..., H...J...et au syndicat des taxis du Bas-Rhin.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">06 ALSACE-MOSELLE. - POUVOIR DE POLICE DU PRÉFET - EXISTENCE [RJ1] - CHAMP D'APPLICATION - MESURES NÉCESSAIRES POUR FAIRE RESPECTER L'ORDRE, LA SÛRETÉ, LA SÉCURITÉ ET LA SALUBRITÉ PUBLIQUES DEVANT ÊTRE PRISES À UNE ÉCHELLE QUI EXCÈDE LE TERRITOIRE D'UNE SEULE COMMUNE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">49-02-03 POLICE. AUTORITÉS DÉTENTRICES DES POUVOIRS DE POLICE GÉNÉRALE. PRÉFETS. - POUVOIR DE POLICE GÉNÉRAL DU PRÉFET EN ALSACE-MOSELLE - EXISTENCE [RJ1] - CHAMP D'APPLICATION - MESURES NÉCESSAIRES POUR FAIRE RESPECTER L'ORDRE, LA SÛRETÉ, LA SÉCURITÉ ET LA SALUBRITÉ PUBLIQUES DEVANT ÊTRE PRISES À UNE ÉCHELLE QUI EXCÈDE LE TERRITOIRE D'UNE SEULE COMMUNE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">49-04 POLICE. POLICE GÉNÉRALE. - ALSACE-MOSELLE - POUVOIR DE POLICE GÉNÉRAL DU PRÉFET - EXISTENCE [RJ1] - CHAMP D'APPLICATION - MESURES NÉCESSAIRES POUR FAIRE RESPECTER L'ORDRE, LA SÛRETÉ, LA SÉCURITÉ ET LA SALUBRITÉ PUBLIQUES DEVANT ÊTRE PRISES À UNE ÉCHELLE QUI EXCÈDE LE TERRITOIRE D'UNE SEULE COMMUNE.
</SCT>
<ANA ID="9A"> 06 En vertu de l'article L. 2542-1 du code général des collectivités territoriales (CGCT), les dispositions de l'article L. 2215-1 du même code, qui définissent les pouvoirs de police du maire et du représentant de l'Etat dans les départements, ne sont pas applicables dans les départements de la Moselle, du Bas-Rhin et du Haut-Rhin. Toutefois, le représentant de l'Etat dans l'un de ces départements est compétent pour prendre, en vertu des pouvoirs de police générale dont il dispose sur le fondement du I de l'article 34 de la loi n° 82-213 du 2 mars 1982 relative aux droits et libertés des communes, des départements et des régions et de l'article 11 du décret n° 2004-374 du 29 avril 2004 relatif aux pouvoirs des préfets, les mesures qu'il estime nécessaires pour faire respecter l'ordre, la sûreté, la sécurité et la salubrité publiques et qui, eu égard à leur nature et à leur objet, doivent être prises à une échelle qui excède le territoire d'une seule commune.</ANA>
<ANA ID="9B"> 49-02-03 En vertu de l'article L. 2542-1 du code général des collectivités territoriales (CGCT), les dispositions de l'article L. 2215-1 du même code, qui définissent les pouvoirs de police du maire et du représentant de l'Etat dans les départements, ne sont pas applicables dans les départements de la Moselle, du Bas-Rhin et du Haut-Rhin. Toutefois, le représentant de l'Etat dans l'un de ces départements est compétent pour prendre, en vertu des pouvoirs de police générale dont il dispose sur le fondement du I de l'article 34 de la loi n° 82-213 du 2 mars 1982 relative aux droits et libertés des communes, des départements et des régions et de l'article 11 du décret n° 2004-374 du 29 avril 2004 relatif aux pouvoirs des préfets, les mesures qu'il estime nécessaires pour faire respecter l'ordre, la sûreté, la sécurité et la salubrité publiques et qui, eu égard à leur nature et à leur objet, doivent être prises à une échelle qui excède le territoire d'une seule commune.</ANA>
<ANA ID="9C"> 49-04 En vertu de l'article L. 2542-1 du code général des collectivités territoriales (CGCT), les dispositions de l'article L. 2215-1 du même code, qui définissent les pouvoirs de police du maire et du représentant de l'Etat dans les départements, ne sont pas applicables dans les départements de la Moselle, du Bas-Rhin et du Haut-Rhin. Toutefois, le représentant de l'Etat dans l'un de ces départements est compétent pour prendre, en vertu des pouvoirs de police générale dont il dispose sur le fondement du I de l'article 34 de la loi n° 82-213 du 2 mars 1982 relative aux droits et libertés des communes, des départements et des régions et de l'article 11 du décret n° 2004-374 du 29 avril 2004 relatif aux pouvoirs des préfets, les mesures qu'il estime nécessaires pour faire respecter l'ordre, la sûreté, la sécurité et la salubrité publiques et qui, eu égard à leur nature et à leur objet, doivent être prises à une échelle qui excède le territoire d'une seule commune.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., s'agissant du pouvoir de réquisition du préfet en Alsace-Moselle, CE, 4 décembre 2017, Commune de Sainte-Croix-en-Plaine, n° 405598, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
