<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036631212</ID>
<ANCIEN_ID>JG_L_2018_02_000000404879</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/63/12/CETATEXT000036631212.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 4ème chambres réunies, 21/02/2018, 404879, Publié au recueil Lebon</TITRE>
<DATE_DEC>2018-02-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>404879</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>M. Frédéric Pacoud</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Charles Touboul</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:404879.20180221</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, un nouveau mémoire et deux mémoire en réplique, enregistrés au secrétariat du contentieux du Conseil d'Etat les 4 novembre 2016, 21 juin 2017, 9 août 2017 et 22 janvier 2018, la région Provence-Alpes-Côte-d'Azur demande au Conseil d'Etat :<br/>
<br/>
              1°) à titre principal, d'annuler pour excès de pouvoir la décision implicite de rejet opposée à sa demande, adressée le 5 juillet 2016 à la ministre des affaires sociales, de la santé et des droits des femmes, tendant à l'abrogation du décret n° 2016-74 du 29 janvier 2016 relatif au diplôme d'Etat d'accompagnant éducatif et social et modifiant le code de l'action sociale et des familles et de l'arrêté du 29 janvier 2016 relatif à la formation conduisant au diplôme d'Etat d'accompagnant éducatif et social et à l'édiction d'un nouveau décret et d'un nouvel arrêté assurant la compensation des charges nouvelles résultant de la création de ce diplôme ;<br/>
<br/>
              2°) d'enjoindre à la ministre des affaires sociales, de la santé et des droits des femmes d'abroger le décret n° 2016-74 du 29 janvier 2016 et l'arrêté du même jour pris pour son application ;<br/>
<br/>
              3°) à titre subsidiaire, d'annuler pour excès de pouvoir la décision implicite de rejet opposée à sa demande adressée le 5 juillet 2016 à la ministre des affaires sociales, de la santé et des droits des femmes tendant à la saisine du ministre de l'intérieur et du secrétaire d'Etat auprès du ministre des finances et des comptes publics, chargé du budget, aux fins de prendre un arrêté conjoint définissant le montant de la compensation financière des charges résultant pour la région de la création du diplôme d'Etat d'accompagnant éducatif et social ;<br/>
<br/>
              4°) d'enjoindre à la ministre des affaires sociales, de la santé et des droits des femmes de saisir le ministre de l'intérieur et le secrétaire d'Etat auprès du ministre des finances et des comptes publics, chargé du budget, aux fins de prendre un arrêté conjoint définissant le montant de la compensation financière ;<br/>
<br/>
              5°) de mettre à la charge de l'Etat la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution ;<br/>
              - le code de l'action sociale et des familles ;<br/>
              - le code général des collectivités territoriales ; <br/>
              - la loi n° 2004-809 du 13 août 2004 ;<br/>
              - la loi n° 2014-288 du 5 mars 2014 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Pacoud, maître des requêtes, <br/>
<br/>
              - les conclusions de M. Charles Touboul, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de la région Provence-Alpes-Côte-d'Azur ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              Sur les conclusions tendant à l'annulation du refus d'abrogation du décret du 29 janvier 2016 et de l'arrêté du 29 janvier 2016 pris pour son application :<br/>
<br/>
              1. D'une part, il résulte du troisième alinéa de l'article 72 de la Constitution que, dans les conditions définies par la loi, les collectivités territoriales " s'administrent librement ". Aux termes du quatrième alinéa de l'article 72-2 de la Constitution : " Tout transfert de compétences entre l'Etat et les collectivités territoriales s'accompagne de l'attribution de ressources équivalentes à celles qui étaient consacrées à leur exercice. Toute création ou extension de compétences ayant pour conséquence d'augmenter les dépenses des collectivités territoriales est accompagnée de ressources déterminées par la loi ". En vertu des dispositions de l'article L. 1614-1 du code général des collectivités territoriales, le transfert d'une compétence de l'Etat aux collectivités territoriales donne lieu, lorsqu'il induit un accroissement net de charges pour ces dernières, au transfert concomitant des ressources nécessaires à l'exercice normal de cette compétence. Selon les dispositions de l'article L. 1614-1-1 du même code, toute création ou extension de compétence ayant pour conséquence d'augmenter les charges des collectivités territoriales est accompagnée des ressources nécessaires. Le second alinéa de l'article L. 1614-2 de ce code dispose que : " Toute charge nouvelle incombant aux collectivités territoriales du fait de la modification par l'Etat, par voie réglementaire, des règles relatives à l'exercice des compétences transférées est compensée dans les conditions prévues à l'article L. 1614-1. Toutefois, cette compensation n'intervient que pour la partie de la charge qui n'est pas déjà compensée par l'accroissement de la dotation générale de décentralisation mentionnée à l'article L. 1614-4 ". Aux termes du premier alinéa de l'article L. 1614-3 de ce code : " Le montant des dépenses résultant des accroissements et diminutions de charges est constaté pour chaque collectivité par arrêté conjoint du ministre chargé de l'intérieur et du ministre chargé du budget, après avis de la commission consultative sur l'évaluation des charges du Comité des finances locales, dans les conditions définies à l'article L. 1211-4-1 ". En vertu de l'article L. 1614-5-1 de ce code, l'arrêté mentionné à l'article L. 1614-3 intervient dans les six mois de la publication des dispositions législatives ou réglementaires auxquelles il se rapporte. <br/>
<br/>
              2. D'autre part, aux termes de l'article L. 451-2 du code de l'action sociale et des familles : " La région définit et met en oeuvre la politique de formation des travailleurs sociaux. (...) / La région assure, dans les conditions prévues à l'article L. 451-2-1 du présent code, le financement des établissements agréés pour dispenser une formation sociale initiale, (...). Ces établissements agréés participent au service public régional de la formation professionnelle. / Elle assure également le financement des établissements agréés pour dispenser une formation sociale continue pour les demandeurs d'emplois, lorsqu'ils participent au service public régional de la formation professionnelle défini à l'article L. 6121-2 du code du travail ". L'article L. 451-2-1 du même code dispose que : " Les établissements agréés par la région pour dispenser des formations sociales initiales souscrivent avec elle une convention pour bénéficier des financements nécessaires à la mise en oeuvre desdites formations. / L'aide financière de la région à ces établissements est constituée par une subvention annuelle couvrant les dépenses administratives et celles liées à leur activité pédagogique. La région participe également, dans des conditions définies par une délibération du conseil régional, à leurs dépenses d'investissement, d'entretien et de fonctionnement des locaux. (...) ".<br/>
<br/>
              3. Le décret du 29 janvier 2016 dont la région Provence-Alpes-Côte-d'Azur a demandé l'abrogation crée un diplôme d'Etat d'accompagnant éducatif et social, en remplacement des diplômes d'Etat d'auxiliaire de vie sociale et d'aide médico-psychologique. Il prévoit que ce diplôme comporte un socle commun de compétences et trois spécialités : " accompagnement de la vie à domicile ", " accompagnement de la vie en structure collective " et " accompagnement à l'éducation inclusive et à la vie ordinaire ". L'arrêté du 29 janvier 2016, pris pour l'application de ce décret, dont la région Provence-Alpes-Côte-d'Azur a également demandé l'abrogation, définit les modalités d'accès à la formation, le contenu et l'organisation de cette formation, ainsi que les modalités de certification du diplôme d'Etat d'accompagnant éducatif et social. <br/>
<br/>
              4. En premier lieu, le décret et l'arrêté litigieux, qui réforment la formation des accompagnants éducatifs et sociaux, dans le cadre de la compétence transférée aux régions par les lois du 13 août 2004 relative aux libertés et responsabilités locales et du 5 mars 2014 relative à la formation professionnelle, à l'emploi et à la démocratie sociale dont sont issues les dispositions de l'article L. 451-2 du code de l'action sociale et des familles citées au point 2, n'ont ni pour objet, ni pour effet d'emporter un transfert de compétences vers les régions ou une création ou une extension de leurs compétences, au sens de l'article 72-2 de la Constitution et des articles L. 1614-1 et L. 1614-1-1 du code général des collectivités territoriales. <br/>
<br/>
              5. En deuxième lieu, à supposer même que la réforme ainsi opérée crée pour la région requérante des charges nouvelles, dans les conditions définies par les dispositions du second alinéa de l'article L. 1614-2 du même code, ces dispositions ne subordonnent pas la légalité de la modification des règles relatives à l'exercice de compétences transférées à la compensation des charges nouvelles qui en résultent. Par suite, l'absence d'adoption de l'arrêté constatant les dépenses résultant d'un accroissement des charges prévu par l'article L. 1614-3 dans le délai de six mois fixé par l'article L. 1614-5-1 ne saurait être utilement invoquée ni pour soutenir que le décret et l'arrêté litigieux étaient illégaux, à la date à laquelle ils sont intervenus, ni même qu'ils le seraient devenus, à l'expiration de ce délai de six mois. Il appartient seulement aux régions qui estiment que la réforme litigieuse leur aurait indûment imposé de telles charges de contester l'absence de compensation, notamment en demandant l'annulation du refus des ministres compétents de prendre l'arrêté prévu par l'article L. 1614-3 du code général des collectivités territoriales, comme la région Provence-Alpes-Côte-d'Azur le fait, du reste, également par la présente requête. <br/>
<br/>
              6. En dernier lieu, si la région requérante soutient que ce décret et cet arrêté ont pour effet d'accroître les charges qui lui incombent en vertu des articles L. 451-2 et L. 451-2-1 du code de l'action sociale et des familles, il ne ressort pas des pièces du dossier, en tout état de cause, que les nouvelles dispositions feraient peser sur les régions des charges qui, par leur ampleur, seraient de nature à dénaturer le principe de libre administration des collectivités territoriales, en méconnaissance de l'article 72 de la Constitution.<br/>
<br/>
              7. Il résulte de ce qui précède que la région Provence-Alpes-Côte-d'Azur n'est pas fondée à demander l'annulation du refus opposé à sa demande d'abrogation du décret du 29 janvier 2016 et de l'arrêté du même jour. Par suite, ses conclusions à fin d'injonction doivent également être rejetées. <br/>
<br/>
              Sur les conclusions tendant à l'annulation du refus de prendre un arrêté interministériel constatant le montant des dépenses résultant des accroissements de charges :<br/>
<br/>
              8. Les arrêtés interministériels constatant le montant des dépenses devant être compensées par l'Etat, en application de l'article L. 1614-3 du code général des collectivités territoriales, du fait de la modification des règles relatives à l'exercice de compétences transférées à des collectivités territoriales, n'ont pas le caractère d'actes réglementaires. Le Conseil d'Etat n'est donc pas compétent pour connaître en premier et dernier ressort des requêtes dirigées contre les décisions ministérielles refusant de prendre de tels actes. <br/>
<br/>
              9. Aux termes de l'article R. 312-1 du code de justice administrative : " Lorsqu'il n'en est pas disposé autrement par les dispositions de la section 2 du présent chapitre ou par un texte spécial, le tribunal administratif territorialement compétent est celui dans le ressort duquel a légalement son siège l'autorité qui, soit en vertu de son pouvoir propre, soit par délégation, a pris la décision attaquée (...) ". Par suite, il y a lieu de transmettre la requête, en tant qu'elle tend à l'annulation de la décision refusant l'édiction d'un arrêté interministériel en application de l'article L. 1614-3 du code général des collectivités territoriales, ainsi que les conclusions à fin d'injonction y afférentes et les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative, au tribunal administratif de Paris, compétent pour en connaître. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les conclusions de la région Provence-Alpes-Côte-d'Azur tendant à l'annulation du refus d'abrogation du décret du 29 janvier 2016 et de l'arrêté du 29 janvier 2016 ainsi que les conclusions à fin d'injonction y afférentes sont rejetées. <br/>
Article 2 : Le jugement du surplus des conclusions de la région Provence-Alpes-Côte-d'Azur  est attribué au tribunal administratif de Paris.<br/>
Article 3 : La présente décision sera notifiée à la région Provence-Alpes-Côte-d'Azur, au Premier ministre, à la ministre des solidarités et de la santé et à la présidente du tribunal administratif de Paris. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-04-005 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. CONSTITUTION ET PRINCIPES DE VALEUR CONSTITUTIONNELLE. - DÉCRET N° 2016-74 DU 29 JANVIER 2016 ET ARRÊTÉ DU 29 JANVIER 2016 RÉFORMANT LA FORMATION DES ACCOMPAGNANTS ÉDUCATIFS ET SOCIAUX - PRINCIPE DE LIBRE ADMINISTRATION DES COLLECTIVITÉS TERRITORIALES (ART. 72 DE LA CONSTITUTION) - ABSENCE DE DÉNATURATION [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">135-04-02-01 COLLECTIVITÉS TERRITORIALES. RÉGION. ATTRIBUTIONS. COMPÉTENCES TRANSFÉRÉES. - DÉCRET N° 2016-74 DU 29 JANVIER 2016 ET ARRÊTÉ DU 29 JANVIER 2016 RÉFORMANT LA FORMATION DES ACCOMPAGNANTS ÉDUCATIFS ET SOCIAUX - TRANSFERT, CRÉATION OU EXTENSION DE COMPÉTENCES - ABSENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">135-04-03 COLLECTIVITÉS TERRITORIALES. RÉGION. FINANCES RÉGIONALES. - DÉCRET N° 2016-74 DU 29 JANVIER 2016 ET ARRÊTÉ DU 29 JANVIER 2016 RÉFORMANT LA FORMATION DES ACCOMPAGNANTS ÉDUCATIFS ET SOCIAUX - 1) TRANSFERT, CRÉATION OU EXTENSION DE COMPÉTENCES - ABSENCE - MODIFICATION DES RÈGLES RELATIVES À L'EXERCICE DE COMPÉTENCES TRANSFÉRÉES - EXISTENCE - 2) ABSENCE DE COMPENSATION DES CHARGES NOUVELLES QUI EN RÉSULTENT PRÉVUES PAR CES TEXTES - CIRCONSTANCE SANS INCIDENCE SUR LEUR LÉGALITÉ - 3) PRINCIPE DE LIBRE ADMINISTRATION DES COLLECTIVITÉS TERRITORIALES (ART. 72 DE LA CONSTITUTION) - ABSENCE DE DÉNATURATION.
</SCT>
<ANA ID="9A"> 01-04-005 Le décret n° 2016-74 du 29 janvier 2016 relatif au diplôme d'Etat d'accompagnant éducatif et social et modifiant le code de l'action sociale et des familles et l'arrêté du 29 janvier 2016 relatif à la formation conduisant au diplôme d'Etat d'accompagnant éducatif et social, qui réforment la formation des accompagnants éducatifs et sociaux, ne font pas peser sur les régions des charges qui, par leur ampleur, seraient de nature à dénaturer le principe de libre administration des collectivités territoriales en méconnaissance de l'article 72 de la Constitution.</ANA>
<ANA ID="9B"> 135-04-02-01 Le décret n° 2016-74 du 29 janvier 2016 relatif au diplôme d'Etat d'accompagnant éducatif et social et modifiant le code de l'action sociale et des familles et l'arrêté du 29 janvier 2016 relatif à la formation conduisant au diplôme d'Etat d'accompagnant éducatif et social, qui réforment la formation des accompagnants éducatifs et sociaux, dans le cadre de la compétence transférée aux régions par les lois n° 2004-809 du 13 août 2004 et n° 2014-288 du 5 mars 2014 dont est issu l'article L. 451-2 du code de l'action sociale et des familles (CASF), n'ont ni pour objet, ni pour effet d'emporter un transfert de compétences vers les régions ou une création ou une extension de leurs compétences, au sens de l'article 72-2 de la Constitution et des articles L. 1614-1 et L. 1614-1-1 du code général des collectivités territoriales (CGCT).</ANA>
<ANA ID="9C"> 135-04-03 1) Le décret n° 2016-74 du 29 janvier 2016 relatif au diplôme d'Etat d'accompagnant éducatif et social et modifiant le code de l'action sociale et des familles et l'arrêté du 29 janvier 2016 relatif à la formation conduisant au diplôme d'Etat d'accompagnant éducatif et social, qui réforment la formation des accompagnants éducatifs et sociaux, dans le cadre de la compétence transférée aux régions par les lois n° 2004-809 du 13 août 2004 et n° 2014-288 du 5 mars 2014 dont est issu l'article L. 451-2 du code de l'action sociale et des familles (CASF), n'ont ni pour objet, ni pour effet d'emporter un transfert de compétences vers les régions ou une création ou une extension de leurs compétences, au sens de l'article 72-2 de la Constitution et des articles L. 1614-1 et L. 1614-1-1 du code général des collectivités territoriales (CGCT).,,,2) A supposer même que cette réforme crée pour la région des charges nouvelles, dans les conditions définies par le second alinéa de l'article L. 1614-2 du CGCT, celui-ci ne subordonne pas la légalité de la modification des règles relatives à l'exercice de compétences transférées à la compensation des charges nouvelles qui en résultent. Par suite, l'absence d'adoption de l'arrêté constatant les dépenses résultant d'un accroissement des charges prévu par l'article L. 1614-3 dans le délai de six mois fixé par l'article L. 1614-5-1 ne saurait être utilement invoquée ni pour soutenir que le décret et l'arrêté litigieux étaient illégaux, à la date à laquelle ils sont intervenus, ni même qu'ils le seraient devenus, à l'expiration de ce délai de six mois. Il appartient seulement aux régions qui estiment que la réforme litigieuse leur aurait indûment imposé de telles charges de contester l'absence de compensation, notamment en demandant l'annulation du refus des ministres compétents de prendre l'arrêté prévu par l'article L. 1614-3 du CGCT.,,,3) Les nouvelles dispositions ne font pas peser sur les régions des charges qui, par leur ampleur, seraient de nature à dénaturer le principe de libre administration des collectivités territoriales en méconnaissance de l'article 72 de la Constitution.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, décision du même jour, Département du Calvados et autres, n° 409286, à publier au Recueil. Rappr. Cons. const., 30 juin 2011, n° 2011-144 QPC, cons. 7.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
