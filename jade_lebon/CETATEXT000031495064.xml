<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031495064</ID>
<ANCIEN_ID>JG_L_2015_11_000000390461</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/49/50/CETATEXT000031495064.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème / 3ème SSR, 18/11/2015, 390461</TITRE>
<DATE_DEC>2015-11-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>390461</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème / 3ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, BLANCPAIN, SOLTNER, TEXIDOR ; SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>Mme Karin Ciavaldini</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:390461.20151118</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La SCI Les II C, l'EURL Le Ponant et Mme B...A...ont demandé au juge des référés du tribunal administratif de Toulon d'ordonner, sur le fondement de l'article L. 521-1 du code de justice administrative, la suspension de l'exécution de l'arrêté du maire du Lavandou du 17 février 2015 abrogeant l'arrêté du 20 juin 2011 autorisant la société civile immobilière à occuper temporairement le domaine public dans le port du Lavandou. Par une ordonnance n° 1501254 du 12 mai 2015, le juge des référés du tribunal administratif a rejeté la demande.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 27 mai, 11 juin et 24 juillet 2015 au secrétariat du contentieux du Conseil d'Etat, la SCI Les II C, l'EURL Le Ponant et Mme A... demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de faire droit à leur demande ;<br/>
<br/>
              3°) de mettre à la charge de la commune du Lavandou la somme de 3 500 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des collectivités territoriales ;<br/>
              - la loi n° 2000-321 du 12 avril 2000 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Karin Ciavaldini, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Blancpain, Soltner, Texidor, avocat de la SCI les II C et autres et à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la commune du Lavandou ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis au juge des référés que, par un arrêté du 17 février 2015, le maire de la commune du Lavandou a abrogé l'autorisation temporaire d'occupation du domaine public qui avait été accordée à la SCI Les II C pour un local commercial situé dans l'enceinte du port du Lavandou, au motif qu'elle ne s'était pas acquittée de l'ensemble des redevances dues au titre de cette occupation ; que, par l'ordonnance attaquée du 12 mai 2015, le juge des référés du tribunal administratif de Toulon a rejeté la demande présentée par la SCI Les II C, par sa gérante, Mme B...A...et par l'EURL Le Ponant tendant, sur le fondement de l'article L. 521-1 du code de justice administrative, à la suspension de l'exécution de cet arrêté, au motif qu'aucun des moyens développés devant lui n'était de nature, en l'état de l'instruction, à faire naître un doute sérieux sur la légalité de cet arrêté ; <br/>
<br/>
              2. Considérant, en premier lieu, qu'il ressort des mentions de l'ordonnance attaquée que le juge des référés a fixé la clôture de l'instruction le mardi 12 mai 2015 à 12 heures ; que, par suite, le mémoire des requérantes enregistré au greffe du tribunal administratif le 12 mai à 11h41 a été produit avant la clôture de l'instruction et ne constituait pas une note en délibéré ; que le moyen tiré de ce que le juge des référés n'aurait pas pris connaissance du contenu de cette production, faute de l'avoir visée comme une " note en délibéré " et aurait de ce fait entaché la procédure d'irrégularité, ne peut, en tout état de cause, qu'être écarté ;<br/>
<br/>
              3. Considérant, en deuxième lieu, qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ces effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ; qu'aux termes du premier alinéa de l'article R. 742-2 du même code : " Les ordonnances mentionnent le nom des parties, l'analyse des conclusions ainsi que les visas des dispositions législatives ou réglementaires dont elles font application " ;<br/>
<br/>
              4. Considérant qu'il appartient au juge des référés qui rejette une demande tendant à la suspension de l'exécution d'une décision administrative au motif qu'il n'est pas fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de cette décision, d'analyser soit dans les visas de son ordonnance, soit dans les motifs de celle-ci, les moyens développés au soutien de la demande de suspension, afin, notamment, de mettre le juge de cassation en mesure d'exercer son contrôle ;<br/>
<br/>
              5. Considérant que le moyen tiré de ce que l'arrêté du 17 février 2015 dont elles demandaient la suspension aurait été retiré par le maire de la commune du Lavandou, présenté par la SCI Les II C et autres au soutien de la contestation de la légalité interne de cet arrêté, était sans influence sur sa légalité ; que, dès lors, le juge des référés n'a pas entaché son ordonnance d'irrégularité en s'abstenant d'analyser ce moyen inopérant dans ses visas ou dans ses motifs ;<br/>
<br/>
              6. Considérant, en troisième lieu, qu'aux termes de l'article L. 2122-21 du code général des collectivités territoriales : " Sous le contrôle du conseil municipal et sous le contrôle administratif du représentant de l'Etat dans le département, le maire est chargé, d'une manière générale, d'exécuter les décisions du conseil municipal et, en particulier : / 1° De conserver et d'administrer les propriétés de la commune et de faire, en conséquence, tous actes conservatoires de ses droits (...) " ; qu'il résulte de ces dispositions que, s'il appartient au conseil municipal de délibérer sur les conditions générales d'administration et de gestion du domaine public communal, le maire est seul compétent pour délivrer les autorisations d'occupation du domaine public ; qu'il est également compétent, sur le fondement de ces mêmes dispositions, pour les retirer ou les abroger ; que, par suite, le juge des référés n'a pas commis d'erreur de droit en jugeant que le moyen tiré de ce que le maire n'aurait pas été compétent pour abroger l'autorisation d'occupation du domaine public n'était pas de nature à faire naître un doute sérieux sur la légalité de l'arrêté du 17 février 2015 ;<br/>
<br/>
              7. Considérant, en quatrième lieu, qu'aux termes de l'article 24 de la loi du 12 avril 2000 relative aux droits des citoyens dans leurs relations avec l'administration : " Exception faite des cas où il est statué sur une demande, les décisions individuelles qui doivent être motivées en application des articles 1er et 2 de la loi n° 79-587 du 11 juillet 1979 relative à la motivation des actes administratifs et à l'amélioration des relations entre l'administration et le public n'interviennent qu'après que la personne intéressée a été mise à même de présenter des observations écrites et, le cas échéant, sur sa demande, des observations orales (...) " ; que si ces dispositions impliquent que l'intéressé ait été averti de la mesure que l'administration envisage de prendre, ainsi que des motifs sur lesquels elle se fonde, et qu'il bénéficie d'un délai suffisant pour présenter ses observations, elles n'imposent pas à l'administration d'informer l'intéressé de sa faculté de présenter des observations écrites ; <br/>
<br/>
              8. Considérant qu'il ressort des pièces du dossier soumis au juge des référés que, par lettres des 17 et 24 septembre 2014, le maire de la commune du Lavandou a informé la gérante de la SCI Les II C que, si les parts variables des redevances dues au titre des années 2011, 2012 et 2013 pour l'occupation du domaine public d'un montant de 10 693,65 euros n'étaient pas versées rapidement, l'autorisation accordée à la société serait abrogée ; que, par suite, en jugeant que n'était pas de nature à faire naître un doute sérieux sur la légalité de l'arrêté du 17 février 2015 le moyen tiré de ce que l'autorisation d'occupation du domaine public aurait été abrogée sans que le bénéficiaire ait été mis à même de présenter des observations écrites, le juge des référés n'a, en tout état de cause, ni dénaturé les pièces du dossier qui lui était soumis, ni commis d'erreur de droit ;<br/>
<br/>
              9. Considérant, enfin, qu'en ne retenant pas non plus le moyen tiré de ce que la décision d'abrogation de l'autorisation n'aurait pas été justifiée par le non-respect des prescriptions figurant dans l'autorisation, le juge des référés n'a pas dénaturé les pièces du dossier qui lui était soumis ; <br/>
<br/>
              10. Considérant qu'il résulte de ce qui précède que le pourvoi de la SCI Les II C et autres doit être rejeté, y compris ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la SCI Les II C et autres la somme de 1 500 euros à verser à la commune du Lavandou au même titre ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la SCI Les II C et autres est rejeté. <br/>
Article 2 : La SCI Les II C et autres verseront à la commune du Lavandou une somme de 1 500 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la SCI Les II C et à la commune du Lavandou. Les autres requérants seront informés de la présente décision par la SCP Celice, Blancpain, Soltner, Texidor, avocat au Conseil d'Etat et à la Cour de cassation, qui les représente devant le Conseil d'Etat.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">135-02-01-02-02-03-01 COLLECTIVITÉS TERRITORIALES. COMMUNE. ORGANISATION DE LA COMMUNE. ORGANES DE LA COMMUNE. MAIRE ET ADJOINTS. POUVOIRS DU MAIRE. ATTRIBUTIONS EXERCÉES AU NOM DE LA COMMUNE. - DOMAINE PUBLIC COMMUNAL - COMPÉTENCE DU MAIRE POUR DÉLIVRER, RETIRER ET ABROGER LES AUTORISATIONS D'OCCUPATION [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">24-01-02-01-01-01 DOMAINE. DOMAINE PUBLIC. RÉGIME. OCCUPATION. UTILISATIONS PRIVATIVES DU DOMAINE. AUTORISATIONS UNILATÉRALES. - DOMAINE PUBLIC COMMUNAL - COMPÉTENCE DU MAIRE POUR DÉLIVRER, RETIRER ET ABROGER LES AUTORISATIONS D'OCCUPATION [RJ1].
</SCT>
<ANA ID="9A"> 135-02-01-02-02-03-01 Il résulte de l'article L. 2122-21 du code général des collectivités territoriales, qui prévoit que le maire est compétent sous le contrôle du conseil municipal pour conserver et administrer les propriétés de la commune, que, s'il appartient au conseil municipal de délibérer sur les conditions générales d'administration et de gestion du domaine public communal, le maire est seul compétent pour délivrer les autorisations d'occupation du domaine public. Il est également compétent, sur le fondement de ces mêmes dispositions, pour les retirer ou les abroger.</ANA>
<ANA ID="9B"> 24-01-02-01-01-01 Il résulte de l'article L. 2122-21 du code général des collectivités territoriales, qui prévoit que le maire est compétent sous le contrôle du conseil municipal pour conserver et administrer les propriétés de la commune, que, s'il appartient au conseil municipal de délibérer sur les conditions générales d'administration et de gestion du domaine public communal, le maire est seul compétent pour délivrer les autorisations d'occupation du domaine public. Il est également compétent, sur le fondement de ces mêmes dispositions, pour les retirer ou les abroger.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Cf. CE, 26 mai 2004, société Paloma, n° 242087, inédit au Recueil.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
