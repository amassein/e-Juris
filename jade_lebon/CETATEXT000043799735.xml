<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043799735</ID>
<ANCIEN_ID>JG_L_2021_07_000000437562</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/79/97/CETATEXT000043799735.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 6ème chambres réunies, 16/07/2021, 437562</TITRE>
<DATE_DEC>2021-07-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>437562</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BAUER-VIOLAS, FESCHOTTE-DESBOIS, SEBAGH ; SCP THOUVENIN, COUDRAY, GREVY</AVOCATS>
<RAPPORTEUR>M. Jean-Dominique Langlais</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:437562.20210716</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Kennel Tonnelier a demandé au tribunal administratif de Toulon, statuant sur le fondement de l'article L. 911-4 du code de justice administrative, d'assurer l'exécution du jugement n° 1302259 du 2 juin 2016 par lequel il a annulé la délibération du 19 juin 2013 du conseil municipal de La Londe-les-Maures approuvant le plan local d'urbanisme, en tant qu'elle classe les parcelles cadastrées section BA n° 107 et 226 appartenant à cette SCI en zone UE. Par un jugement n° 1800241 du 19 juin 2018, le tribunal administratif de Toulon a enjoint à la commune de La Londe-les-Maures d'adopter une délibération approuvant un nouveau classement de ces parcelles dans une zone du plan local d'urbanisme dont le règlement autorise les aires de stationnement collectif de bateaux, dans un délai de quatre mois à compter de la notification du jugement, sous astreinte de 100 euros par jour de retard.<br/>
<br/>
              Par un arrêt n° 18MA03427 du 13 novembre 2019, la cour administrative d'appel de Marseille a rejeté l'appel formé par la commune de La Londe-les-Maures contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 13 janvier et 14 avril 2020 au secrétariat du contentieux du Conseil d'Etat, la commune de La Londe-les-Maures demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de la société Kennel Tonnelier la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'urbanisme ;<br/>
              - la loi n° 2014-366 du 24 mars 2014 ;<br/>
              - la loi n° 2018-1021 du 23 novembre 2018 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Langlais, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Thouvenin, Coudray, Grevy, avocat de la commune de La Londe-les-Maures et à la SCP Bauer-Violas, Feschotte-Desbois, Sebagh, avocat de la société Kennel Tonnelier ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la société Kennel Tonnelier est propriétaire, sur le territoire de la commune de La Londe-les-Maures, de deux parcelles sur lesquelles s'exerce une activité de stationnement collectif de bateaux. Par une délibération du 19 juin 2013 adoptant son nouveau plan local d'urbanisme, la commune a procédé au classement de ces parcelles dans une zone ne permettant plus l'exercice de cette activité. Par un jugement du 2 juin 2016, devenu définitif, le tribunal administratif de Toulon a, à la demande de la société Kennel Tonnelier, jugé que ce classement était entaché d'erreur manifeste d'appréciation et annulé dans cette mesure le plan local d'urbanisme de La Londe-les-Maures. A nouveau saisi par la société Kennel Tonnelier sur le fondement de l'article L. 911-4 du code de justice administrative, le tribunal a, par un second jugement du 19 juin 2018, enjoint à la commune d'adopter dans un délai de quatre mois une délibération approuvant un nouveau classement des parcelles concernées dans une zone où les aires de stationnement collectif de bateaux sont autorisées, en assortissant cette injonction d'une astreinte. La commune de La Londe-les-Maures, tout en ayant exécuté le jugement du 2 juin 2016 par l'adoption d'une délibération du 19 septembre 2018, se pourvoit en cassation contre l'arrêt du 13 novembre 2019 par lequel la cour administrative d'appel de Marseille a rejeté sa requête dirigée contre le jugement du 19 juin 2018.<br/>
<br/>
              2. Le premier alinéa de l'article L. 153-7 du code de l'urbanisme dispose que : " En cas d'annulation partielle par voie juridictionnelle d'un plan local d'urbanisme, l'autorité compétente élabore sans délai les nouvelles dispositions du plan applicables à la partie du territoire communal concernée par l'annulation (...) ". Ces dispositions font obligation à l'autorité compétente d'élaborer, dans le respect de l'autorité de la chose jugée par la décision juridictionnelle ayant partiellement annulé un plan local d'urbanisme, de nouvelles dispositions se substituant à celles qui ont été annulées par le juge, alors même que l'annulation contentieuse aurait eu pour effet de remettre en vigueur, en application des dispositions de l'article L. 600-12 du même code ou de son article L. 174-6, des dispositions d'un plan local d'urbanisme ou, pour une durée maximale de vingt-quatre mois, des dispositions d'un plan d'occupation des sols qui ne méconnaîtraient pas l'autorité de la chose jugée par ce même jugement d'annulation.<br/>
<br/>
              3. En revanche, les dispositions de l'article L. 153-7 du code de l'urbanisme n'ont pas pour effet de permettre à l'autorité compétente de s'affranchir, pour l'édiction de ces nouvelles dispositions, des règles qui régissent les procédures de révision, de modification ou de modification simplifiée du plan local d'urbanisme prévues, respectivement, par les articles L. 153-31, L. 153-41 et L. 153-45 du même code. Ainsi, lorsque l'exécution d'une décision juridictionnelle prononçant l'annulation partielle d'un plan local d'urbanisme implique nécessairement qu'une commune modifie le règlement de son plan local d'urbanisme dans un sens déterminé, il appartient à la commune de faire application, selon la nature et l'importance de la modification requise, de l'une de ces procédures, en se fondant le cas échéant, dans le respect de l'autorité de la chose jugée, sur certains actes de procédure accomplis pour l'adoption des dispositions censurées par le juge.<br/>
<br/>
              4. Par suite, en jugeant, ainsi qu'il résulte des termes de l'arrêt attaqué, que le changement de classement des parcelles litigieuses dans le plan local d'urbanisme de La Londe-les-Maures pouvait, en application des dispositions de l'article L. 153-7 du code de l'urbanisme et dès lors qu'il intervenait pour l'exécution du jugement du tribunal administratif de Toulon du 2 juin 2016, s'effectuer sans que la commune soit tenue de suivre une procédure particulière de modification ou de révision de son plan local d'urbanisme, la cour a entaché son arrêt d'erreur de droit.<br/>
<br/>
              5. La commune de La Londe-les-Maures est, par suite, fondée à demander l'annulation de l'arrêt qu'elle attaque.<br/>
<br/>
              6. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de la société Kennel Tonnelier la somme que demande la commune de La Londe-les-Maures au titre des dispositions de l'article L. 761-1 du code de justice administrative. Ces mêmes dispositions font obstacle à ce que soit mise à la charge de la commune de La Londe-les-Maures, qui n'est pas, dans la présente instance, la partie perdante, la somme que demande, au même titre, la société Kennel Tonnelier.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				 --------------<br/>
<br/>
Article 1er : L'arrêt du 13 novembre 2019 de la cour administrative d'appel de Marseille est annulé.<br/>
<br/>
		Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Marseille.<br/>
<br/>
Article 3 : Les conclusions présentées par les parties au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la commune de La Londe-les-Maures et à la société Kennel Tonnelier.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">68-01-01-01-02 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PLANS D'AMÉNAGEMENT ET D'URBANISME. PLANS D`OCCUPATION DES SOLS (POS) ET PLANS LOCAUX D'URBANISME (PLU). LÉGALITÉ DES PLANS. MODIFICATION ET RÉVISION DES PLANS. - ANNULATION PARTIELLE D'UN PLU - ELABORATION DE NOUVELLES DISPOSITIONS SE SUBSTITUANT AUX DISPOSITIONS ANNULÉES (ART. L. 153-7 DU CODE DE L'URBANISME) - OBLIGATION DE RESPECTER LES RÈGLES RÉGISSANT LES PROCÉDURES DE RÉVISION, DE MODIFICATION OU DE MODIFICATION SIMPLIFIÉE DU PLU (ART. L. 153-31, L. 153-41 ET L. 153-45) - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">68-06-05 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. EFFETS DES ANNULATIONS. - ANNULATION PARTIELLE D'UN PLU - ELABORATION DE NOUVELLES DISPOSITIONS SE SUBSTITUANT AUX DISPOSITIONS ANNULÉES (ART. L. 153-7 DU CODE DE L'URBANISME) - OBLIGATION DE RESPECTER LES RÈGLES RÉGISSANT LES PROCÉDURES DE RÉVISION, DE MODIFICATION OU DE MODIFICATION SIMPLIFIÉE DU PLU (ART. L. 153-31, L. 153-41 ET L. 153-45) - EXISTENCE.
</SCT>
<ANA ID="9A"> 68-01-01-01-02 Le premier alinéa de l'article L. 153-7 du code de l'urbanisme fait obligation à l'autorité compétente d'élaborer, dans le respect de l'autorité de la chose jugée par la décision juridictionnelle ayant partiellement annulé un plan local d'urbanisme (PLU), de nouvelles dispositions se substituant à celles qui ont été annulées par le juge, alors même que l'annulation contentieuse aurait eu pour effet de remettre en vigueur, en application des dispositions de l'article L. 600-12 du même code ou de son article L. 174-6, des dispositions d'un PLU ou, pour une durée maximale de vingt-quatre mois, des dispositions d'un plan d'occupation des sols (POS) qui ne méconnaîtraient pas l'autorité de la chose jugée par ce même jugement d'annulation.,,,En revanche, l'article L. 153-7 du code de l'urbanisme n'a pas pour effet de permettre à l'autorité compétente de s'affranchir, pour l'édiction de ces nouvelles dispositions, des règles qui régissent les procédures de révision, de modification ou de modification simplifiée du PLU prévues, respectivement, par les articles L. 153-31, L. 153-41 et L. 153-45 du même code. Ainsi, lorsque l'exécution d'une décision juridictionnelle prononçant l'annulation partielle d'un PLU implique nécessairement qu'une commune modifie le règlement de son PLU dans un sens déterminé, il appartient à la commune de faire application, selon la nature et l'importance de la modification requise, de l'une de ces procédures, en se fondant le cas échéant, dans le respect de l'autorité de la chose jugée, sur certains actes de procédure accomplis pour l'adoption des dispositions censurées par le juge.</ANA>
<ANA ID="9B"> 68-06-05 Le premier alinéa de l'article L. 153-7 du code de l'urbanisme fait obligation à l'autorité compétente d'élaborer, dans le respect de l'autorité de la chose jugée par la décision juridictionnelle ayant partiellement annulé un plan local d'urbanisme (PLU), de nouvelles dispositions se substituant à celles qui ont été annulées par le juge, alors même que l'annulation contentieuse aurait eu pour effet de remettre en vigueur, en application des dispositions de l'article L. 600-12 du même code ou de son article L. 174-6, des dispositions d'un PLU ou, pour une durée maximale de vingt-quatre mois, des dispositions d'un plan d'occupation des sols (POS) qui ne méconnaîtraient pas l'autorité de la chose jugée par ce même jugement d'annulation.,,,En revanche, l'article L. 153-7 du code de l'urbanisme n'a pas pour effet de permettre à l'autorité compétente de s'affranchir, pour l'édiction de ces nouvelles dispositions, des règles qui régissent les procédures de révision, de modification ou de modification simplifiée du PLU prévues, respectivement, par les articles L. 153-31, L. 153-41 et L. 153-45 du même code. Ainsi, lorsque l'exécution d'une décision juridictionnelle prononçant l'annulation partielle d'un PLU implique nécessairement qu'une commune modifie le règlement de son PLU dans un sens déterminé, il appartient à la commune de faire application, selon la nature et l'importance de la modification requise, de l'une de ces procédures, en se fondant le cas échéant, dans le respect de l'autorité de la chose jugée, sur certains actes de procédure accomplis pour l'adoption des dispositions censurées par le juge.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
