<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038077337</ID>
<ANCIEN_ID>JG_L_2019_01_000000416013</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/07/73/CETATEXT000038077337.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 30/01/2019, 416013, Publié au recueil Lebon</TITRE>
<DATE_DEC>2019-01-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>416013</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER ; LE PRADO</AVOCATS>
<RAPPORTEUR>M. Arno Klarsfeld</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:416013.20190130</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...C...B...a demandé à la Cour nationale du droit d'asile d'annuler la décision du 5 août 2016 par laquelle l'Office français de protection des réfugiés et apatrides lui a retiré la qualité de réfugié qui lui avait été reconnue le 30 septembre 2005. Par une décision n° 1602768 du 11 septembre 2017, la Cour nationale du droit d'asile a fait droit à sa demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 27 novembre 2017 et 27 février 2018 au secrétariat du contentieux du Conseil d'Etat, l'Office français de protection des réfugiés et apatrides demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette décision ;<br/>
<br/>
              2°) de renvoyer l'affaire devant la Cour nationale du droit d'asile.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention de Genève du 28 juillet 1951 relative au statut des réfugiés et le protocole signé à New York le 31 janvier 1967 relatif au statut des réfugiés ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ; <br/>
              - le décret n° 2010-569 du 28 mai 2010 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Arno Klarsfeld, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Foussard, Froger, avocat de l'Office français de protection des réfugiés et apatrides et à Me Le Prado, avocat de M. A...C...B...;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis à la Cour nationale du droit d'asile que, pour retirer à M.B..., de nationalité russe et d'origine tchétchène, la qualité de réfugié qui lui avait été reconnue en 2005, l'Office français de protection des réfugiés et apatrides (OFPRA), après avoir estimé que M. B...devait être regardé comme s'étant volontairement réclamé de la protection du pays dont il a la nationalité, a fait application de l'article L. 711-4 du code de l'entrée et du séjour des étrangers et du droit d'asile relatif aux clauses de cessation prévues au 1° du paragraphe C de l'article 1er de la convention de Genève, par une décision du 5 août 2016. Par une décision du 11 septembre 2017, contre laquelle l'Office se pourvoit en cassation, la Cour nationale du droit d'asile a annulé cette décision et rétabli M. B... dans sa qualité de réfugié. <br/>
<br/>
              2. Pour faire droit à la demande de M.B..., la cour a, en premier lieu, écarté le motif retenu par l'OFPRA comme non fondé, en jugeant que, dans les circonstances particulières de l'espèce, ni ses brefs séjours en Russie ni la délivrance d'un passeport russe ne suffisaient à caractériser un acte d'allégeance de sa part. Elle a, en second lieu, écarté un motif nouveau, invoqué par l'Office en cours d'instance sur le fondement de l'article L. 711-6 du même code et tiré de ce qu'il y avait des raisons sérieuses de considérer que la présence en France de M. B...constituerait une menace grave pour la sûreté de l'Etat. Elle a jugé, sur ce point, que la fiche " S " versée au dossier ne suffisait pas à établir une telle menace.<br/>
<br/>
              3. L'article L. 711-6 du code de l'entrée et du séjour des étrangers et du droit d'asile dispose que : " Le statut de réfugié peut être refusé ou il peut être mis fin à ce statut lorsque : 1° Il y a des raisons sérieuses de considérer que la présence en France de la personne concernée constitue une menace grave pour la sûreté de l'État ". Aux termes du 8° de l'article 2 du décret du 28 mai 2010 relatif au fichier des personnes recherchées, peuvent être inscrites dans ce fichier, sous la rubrique " S " : " Les personnes faisant l'objet de recherches pour prévenir des menaces graves pour la sécurité publique ou la sûreté de l'Etat, dès lors que des informations ou des indices réels ont été recueillis à leur égard ". Si l'inscription d'une personne dans le fichier des personnes recherchées pour prévenir des menaces graves pour la sécurité publique ou la sûreté de l'Etat ne saurait, par elle-même, suffire à établir que la condition posée par le 1° de l'article L. 711-6 précité est remplie, il appartient au juge de l'asile, lorsqu'il est informé d'une telle inscription, que la fiche soit ou non produite à l'instance, de se forger une conviction au vu de l'argumentation des parties sur ce point dans le cadre de la procédure contradictoire et il ne saurait dénier toute force probante à l'inscription au fichier d'une personne faisant l'objet de recherches pour prévenir des menaces graves pour la sécurité publique ou la sûreté de l'Etat sans user de ses pouvoirs d'instruction pour recueillir toutes informations pertinentes, notamment auprès du ministre de l'intérieur, qu'il peut appeler dans l'instance afin qu'il apporte au débat contradictoire tous éléments et informations sur les circonstances et les motifs de l'inscription en cause. <br/>
<br/>
              4. Alors que l'OFPRA a versé au dossier qui lui était soumis une " fiche S " relative à M.B..., personne recherchée pour prévenir des menaces graves pour la sécurité publique ou la sûreté de l'Etat, la Cour a considéré qu'il ne résultait pas de l'instruction qu'il existait des raisons sérieuses de penser que la présence en France de l'intéressé constituerait une telle menace au motif que cette fiche n'apportait aucune justification ni explication sur les circonstances de son inscription ni sur des velléités affirmées d'enrôlement dans un réseau djihadiste. En se fondant sur le seul contenu de la fiche S versée au dossier pour juger que la condition posée par le 1° de l'article L. 711-6 du code de l'entrée et du séjour des étrangers et du droit d'asile n'était pas remplie, sans recueillir, pour se forger sa conviction, aucune autre information pertinente, notamment en appelant dans l'instance le ministre de l'intérieur, elle a méconnu son office de juge de pleine juridiction et entaché sa décision d'erreur de droit. <br/>
<br/>
              5. Il résulte de ce qui précède que, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, l'Office français de protection des réfugiés et apatrides est fondé à demander l'annulation de la décision du 11 septembre 2017 de la Cour nationale du droit d'asile. <br/>
<br/>
              6. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de l'Office français de protection des réfugiés et apatrides, qui n'est pas, dans la présente instance, la partie perdante .<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : La décision de la Cour nationale du droit d'asile du 11 septembre 2017 est annulée.<br/>
Article 2 : L'affaire est renvoyée à la Cour nationale du droit d'asile.<br/>
Article 3 : Les conclusions de M. B...présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 4 : La présente décision sera notifiée à l'Office français de protection des refugiés et apatrides et à M. A...C...B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">095-04-02 - CESSATION DU STATUT DE RÉFUGIÉ EN CAS DE MENACE GRAVE POUR LA SÛRETÉ DE L'ETAT (ART. L. 711-6 DU CESEDA) - CAS D'UNE PERSONNE INSCRITE AU FICHIER DES PERSONNES RECHERCHÉES, POUR PRÉVENIR DES MENACES GRAVES POUR LA SÉCURITÉ PUBLIQUE OU LA SÛRETÉ DE L'ETAT (8° DE L'ARTICLE 2 DU DÉCRET DU 28 MAI 2010) - FORMATION DE LA CONVICTION DU JUGE - INSCRIPTION SUFFISANTE, PAR ELLE-MÊME, POUR JUSTIFIER QU'IL SOIT MIS FIN À CE STATUT - ABSENCE - INSCRIPTION POUVANT SE VOIR DÉNIER TOUTE FORCE PROBANTE - ABSENCE, SI LE JUGE N'A PAS PRÉALABLEMENT USÉ DE SES POUVOIRS D'INSTRUCTION POUR RECUEILLIR TOUTES INFORMATIONS PERTINENTES SUR LES CIRCONSTANCES ET LES MOTIFS DE CETTE INSCRIPTION.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">095-08-05-01-06 - CESSATION DU STATUT DE RÉFUGIÉ EN CAS DE MENACE GRAVE POUR LA SÛRETÉ DE L'ETAT (ART. L. 711-6 DU CESEDA) - CAS D'UNE PERSONNE INSCRITE AU FICHIER DES PERSONNES RECHERCHÉES, POUR PRÉVENIR DES MENACES GRAVES POUR LA SÉCURITÉ PUBLIQUE OU LA SÛRETÉ DE L'ETAT (8° DE L'ARTICLE 2 DU DÉCRET DU 28 MAI 2010) - FORMATION DE LA CONVICTION DU JUGE - INSCRIPTION SUFFISANTE, PAR ELLE-MÊME, POUR JUSTIFIER QU'IL SOIT MIS FIN À CE STATUT - ABSENCE - INSCRIPTION POUVANT SE VOIR DÉNIER TOUTE FORCE PROBANTE - ABSENCE, SI LE JUGE N'A PAS PRÉALABLEMENT USÉ DE SES POUVOIRS D'INSTRUCTION POUR RECUEILLIR TOUTES INFORMATIONS PERTINENTES SUR LES CIRCONSTANCES ET LES MOTIFS DE CETTE INSCRIPTION.
</SCT>
<ANA ID="9A"> 095-04-02 Si l'inscription d'une personne dans le fichier des personnes recherchées pour prévenir des menaces graves pour la sécurité publique ou la sûreté de l'Etat ne saurait, par elle-même, suffire à établir que la condition posée par le 1° de l'article L.711-6 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA) est remplie, il appartient au juge de l'asile, lorsqu'il est informé d'une telle inscription, que la fiche soit ou non produite à l'instance, de se forger une conviction au vu de l'argumentation des parties sur ce point dans le cadre de la procédure contradictoire et il ne saurait dénier toute force probante à l'inscription au fichier d'une personne faisant l'objet de recherches pour prévenir des menaces graves pour la sécurité publique ou la sûreté de l'Etat sans user de ses pouvoirs d'instruction pour recueillir toutes informations pertinentes, notamment auprès du ministre de l'intérieur, qu'il peut appeler dans l'instance afin qu'il apporte au débat contradictoire tous éléments et informations sur les circonstances et les motifs de l'inscription en cause.</ANA>
<ANA ID="9B"> 095-08-05-01-06 Si l'inscription d'une personne dans le fichier des personnes recherchées pour prévenir des menaces graves pour la sécurité publique ou la sûreté de l'Etat ne saurait, par elle-même, suffire à établir que la condition posée par le 1° de l'article L.711-6 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA) est remplie, il appartient au juge de l'asile, lorsqu'il est informé d'une telle inscription, que la fiche soit ou non produite à l'instance, de se forger une conviction au vu de l'argumentation des parties sur ce point dans le cadre de la procédure contradictoire et il ne saurait dénier toute force probante à l'inscription au fichier d'une personne faisant l'objet de recherches pour prévenir des menaces graves pour la sécurité publique ou la sûreté de l'Etat sans user de ses pouvoirs d'instruction pour recueillir toutes informations pertinentes, notamment auprès du ministre de l'intérieur, qu'il peut appeler dans l'instance afin qu'il apporte au débat contradictoire tous éléments et informations sur les circonstances et les motifs de l'inscription en cause.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
