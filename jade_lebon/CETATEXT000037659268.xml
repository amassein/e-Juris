<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037659268</ID>
<ANCIEN_ID>JG_L_2018_11_000000410974</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/65/92/CETATEXT000037659268.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 28/11/2018, 410974</TITRE>
<DATE_DEC>2018-11-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>410974</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROUSSEAU, TAPIE ; SCP ODENT, POULET</AVOCATS>
<RAPPORTEUR>M. Yves Doutriaux</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Guillaume Odinet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:410974.20181128</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B...A...a demandé au tribunal administratif de Cergy-Pontoise :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision de la Fédération française de savate, boxe française et disciplines associées (FFSBFDA) du 2 novembre 2011 l'excluant pour une période de douze mois de toute compétition ainsi que la décision de la commission de discipline fédérale de première instance du 3 janvier 2012 lui infligeant la sanction de la suspension de toute compétition pour un titre national ou international au sein de la fédération pendant douze mois et la décision du 15 mai 2012 par laquelle la commission de discipline fédérale d'appel a réduit à six mois  la durée de la suspension ;<br/>
<br/>
              2°) qu'il soit enjoint à la fédération de modifier l'article 13.4 de son règlement intérieur ; <br/>
<br/>
              3°) de condamner la fédération à lui verser 12 000 euros en réparation  des préjudices qu'elle estime avoir subis.<br/>
<br/>
              Par un jugement n° 1110125, 1201867, 1300799 du 5 juin 2014, le tribunal administratif a constaté qu'il n'y avait plus lieu de statuer sur les conclusions dirigées contre la décision du 2 novembre 2011 et  rejeté le surplus des conclusions de la demande.<br/>
<br/>
              Par un arrêt n° 14VE02476 du 22 septembre 2016, la cour administrative d'appel de Versailles a, sur appel de MmeA..., annulé ce jugement, constaté qu'il n'y avait pas lieu de statuer sur la décision du 2 novembre 2011 et rejeté le surplus des conclusions.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 29 mai et 29 août 2017 au secrétariat du contentieux du Conseil d'Etat, Mme A...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt en tant qu'il constate qu'il n'y a plus lieu de statuer sur la décision du 2 novembre 2011 et qu'il rejette le surplus de ses conclusions ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
              3°) de mettre à la charge de la Fédération française de savate, boxe française et disciplines associées la somme de 3 000 euros au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code du sport ;<br/>
              - la loi n° 91-647 du 21 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Yves Doutriaux, conseiller d'Etat, <br/>
<br/>
              - les conclusions de M. Guillaume Odinet, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rousseau, Tapie, avocat de MmeA..., et à la SCP Odent, Poulet, avocat de la Fédération française de savate boxe française et disciplines associées ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que MmeA..., licenciée de la Fédération française de savate, boxe française et disciplines associées, a fait l'objet, le 2 novembre 2011, d'une décision l'excluant de toute compétition pour une durée de douze mois ; que cette décision a été retirée par une décision du 12 décembre 2011 ; que, par la suite, elle a, le 15 mai 2012, fait l'objet d'une sanction d'exclusion pendant six mois de toute compétition pour un titre national ou international au sein de la fédération, pour avoir, sans en demander préalablement l'autorisation, participé à une compétition organisée par une autre fédération de boxe pied-poings, en méconnaissance des obligations prévues par l'article 13.4 du règlement de la fédération ; <br/>
<br/>
              Sur l'arrêt en tant qu'il statue sur les conclusions tendant à l'annulation de la décision du 2 novembre 2011 : <br/>
<br/>
              2.	Considérant que la cour administrative d'appel a souverainement constaté que la fédération avait retiré la décision d'exclusion du 2 novembre 2011 et avait réintégré Mme A... dans le championnat de France Elite 2011/2012 dont elle avait modifié à cette fin le déroulement ; qu'elle a pu, sans erreur de droit, déduire de ses constatations, qui ne sont entachées d'aucune dénaturation, qu'il n'y avait plus lieu de statuer sur les conclusions dirigées contre la décision du 2 novembre 2011 ; que les conclusions du pourvoi dirigées contre l'article 2 de l'arrêt doivent donc être rejetées ;<br/>
<br/>
              Sur l'arrêt en tant qu'il statue sur les conclusions tendant à l'indemnisation du préjudice résultant de l'exécution immédiate de la sanction prononcée par l'organe disciplinaire de première instance :<br/>
<br/>
              3.	Considérant qu'aux termes de l'article L. 141-4 du code du sport : " Le Comité national olympique et sportif français est chargé d'une mission de conciliation dans les conflits opposant les licenciés, les agents sportifs, les associations et sociétés sportives et les fédérations sportives agréées, à l'exception des conflits mettant en cause des faits de dopage " ; qu'il ressort des pièces du dossier soumis aux juges du fond que Mme A...a saisi le Comité national olympique et sportif français de la décision du 3 janvier 2012 de l'organe disciplinaire de première instance de la fédération française de savate, boxe française et disciplines associées ; que, dès lors, en jugeant que Mme A...n'avait pas saisi le Comité du conflit résultant du refus de la fédération de suspendre l'exécution de la décision du 3 janvier 2012, la cour a dénaturé les pièces du dossier qui lui était soumis ; que Mme A...est fondée, pour ce motif, à demander l'annulation de l'arrêt attaqué en tant qu'il rejette ses conclusions indemnitaires tendant à la réparation du préjudice né de l'application immédiate de la sanction prononcée par l'organe disciplinaire de première instance ;<br/>
<br/>
              Sur l'arrêt en tant qu'il statue sur les autres conclusions :<br/>
<br/>
              4.	Considérant qu'aux termes  de l'article L. 100-1 du code du sport,  dans sa version alors applicable : " Les activités physiques et sportives constituent un élément important de l'éducation, de la culture, de l'intégration et de la vie sociale. / Elles contribuent notamment à la lutte contre l'échec scolaire et à la réduction des inégalités sociales et culturelles, ainsi qu'à la santé. / La promotion et le développement des activités physiques et sportives pour tous, (...) sont d'intérêt général. " ; qu'aux termes de l'article L. 100-2 du même code : " L'Etat, les collectivités territoriales et leurs groupements, les associations, les fédérations sportives, les entreprises et leurs institutions sociales contribuent à la promotion et au développement des activités physiques et sportives. / L'Etat et les associations et fédérations sportives assurent le développement du sport de haut niveau, avec le concours des collectivités territoriales, de leurs groupements et des entreprises intéressées. " ; qu'aux termes de l'article L. 131-15 du  même code : " Les fédérations délégataires : / 1° Organisent les compétitions sportives à l'issue desquelles sont délivrés les titres internationaux, nationaux, régionaux ou départementaux ; / 2° Procèdent aux sélections correspondantes (...) " ; qu'en vertu de l'article L. 131-16 de ce code, les fédérations délégataires édictent les règles techniques propres à leur discipline ainsi que les règlements relatifs à l'organisation de toute manifestation ouverte à leurs licenciés ; qu'aux termes du premier alinéa de l'article L. 231-5 du code : " Les fédérations sportives veillent à la santé de leurs licenciés et prennent à cet effet les dispositions nécessaires, notamment en ce qui concerne les programmes d'entraînement et le calendrier des compétitions et manifestations sportives qu'elles organisent ou qu'elles autorisent " ; <br/>
<br/>
              5.	Considérant qu'il résulte de ces dispositions qu'il appartient aux fédérations sportives délégataires, habilitées à organiser les compétitions sportives officielles, de prendre les dispositions utiles pour assurer l'organisation des compétitions nationales en veillant à la santé des sportifs ; que, toutefois, dans l'exercice de ce pouvoir, ces fédérations ne peuvent légalement porter atteinte au principe du libre accès aux activités sportives pour tous et à tous les niveaux, qui résulte de l'article L. 100-1 du code du sport, que dans la mesure où ces atteintes ne sont pas excessives au regard des objectifs poursuivis ;<br/>
<br/>
              6.	Considérant qu'aux termes de l'article 13.4 du règlement intérieur de la FFSBFDA : " En l'absence d'autorisation spéciale préalable et ponctuelle accordée par le responsable des compétitions et la DTN de la FFSBFDA, (...) il est interdit à tout compétiteur ayant concouru pour un titre quelconque, au niveau national ou international, dans une forme de boxe pieds-poings, de concourir pour un titre national et international au sein de la FFSBFDA pendant les 12 mois suivants " ; <br/>
<br/>
              7.	Considérant que, pour refuser de faire droit aux conclusions de Mme A... dirigées contre la sanction litigieuse, la cour administrative d'appel de Versailles a écarté le moyen tiré, par la voie de l'exception, de ce que l'article 13.4 du règlement de la fédération était illégal, en jugeant que les atteintes qu'il porte au principe du libre accès aux activités sportives pour tous étaient justifiées par l'objectif d'intérêt général tenant à la prise en compte de l'impératif de protection de la santé des licenciés et au bon déroulement des compétitions que la fédération est chargée d'organiser ;<br/>
<br/>
              8.	Considérant toutefois qu'en vertu de cet article, un sportif participant aux compétitions organisées par la FFSBFDA doit solliciter une autorisation préalable avant de concourir pour un titre quelconque dans une autre forme de boxe pieds-poings dans le cadre d'une compétition organisée par une autre fédération ou un organisme sportif ; que, s'il incombe à une fédération de veiller à la santé des sportifs et à l'organisation des compétitions nationales, elle ne saurait, sans porter une atteinte excessive à la liberté d'accès aux activités sportives, soumettre à un régime d'autorisation préalable leur participation à une compétition ou une manifestation sportive organisée par une autre fédération ou un organisme sportif ; qu'il résulte de ce qui précède qu'en jugeant que les dispositions en cause ne portaient pas une atteinte disproportionnée au principe de libre accès aux activités sportives, la cour administrative d'appel a commis une erreur de droit ; que, par suite et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, l'article 3 de son arrêt doit être annulé ;<br/>
<br/>
              9.	Considérant que Mme A... a obtenu le bénéfice de l'aide juridictionnelle ; que, par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ; qu'il y a lieu, dans les circonstances de l'espèce, et sous réserve que la SCP Rousseau, Tapie, avocat de MmeA..., renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de la FFSBFDA la somme de 3 000 euros à verser à cette société ; que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de Mme A...qui n'est pas, dans la présente instance, la partie perdante ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'article 3 de l'arrêt en date du 22 septembre 2016 de la cour administrative de Versailles est annulé.<br/>
<br/>
Article 2 : L'affaire est renvoyée dans cette mesure à la cour administrative d'appel de Versailles.<br/>
<br/>
Article 3 : La FFSBFDA versera à la la SCP Rousseau, Tapie, avocat de MmeA..., une somme de 3 000 euros en application des dispositions de l'article L. 761-1 du code de justice administrative et du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat. <br/>
<br/>
Article 4 : Les conclusions de la FFSBFDA présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative et le surplus des conclusions de MmeA...  sont rejetés.<br/>
Article 5 : La présente décision sera notifiée à Mme B...A...et à la Fédération française de savate, boxe française et disciplines associées.<br/>
Copie en sera adressée à la ministre des sports. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-04-02-02 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. LOI. VIOLATION. - PRINCIPE DE LIBRE ACCÈS AUX ACTIVITÉS SPORTIVES (ART. L. 100-1 DU CODE DU SPORT) - RÈGLEMENT D'UNE FÉDÉRATION SPORTIVE PRÉVOYANT L'OBLIGATION POUR UN SPORTIF PARTICIPANT AUX COMPÉTITIONS ORGANISÉES PAR CELLE-CI DE SOLLICITER UNE AUTORISATION PRÉALABLE AVANT DE PARTICIPER À UNE COMPÉTITION ORGANISÉE PAR UNE AUTRE FÉDÉRATION OU ORGANISME SPORTIF - ATTEINTE EXCESSIVE - EXISTENCE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">63-05-01-04 SPORTS ET JEUX. SPORTS. FÉDÉRATIONS SPORTIVES. ORGANISATION DES COMPÉTITIONS. - RÈGLEMENT D'UNE FÉDÉRATION SPORTIVE PRÉVOYANT L'OBLIGATION POUR UN SPORTIF PARTICIPANT AUX COMPÉTITIONS ORGANISÉES PAR CELLE-CI DE SOLLICITER UNE AUTORISATION PRÉALABLE AVANT DE PARTICIPER À UNE COMPÉTITION ORGANISÉE PAR UNE AUTRE FÉDÉRATION OU ORGANISME SPORTIF - ATTEINTE EXCESSIVE À LA LIBERTÉ D'ACCÈS AUX ACTIVITÉS SPORTIVES (ART. L. 100-1 DU CODE DU SPORT) - EXISTENCE [RJ1].
</SCT>
<ANA ID="9A"> 01-04-02-02 En vertu de l'article 13.4 du règlement intérieur de la Fédération française de savate boxe française et disciplines associées (FFSBFDA), un sportif participant aux compétitions organisées par cette fédération doit solliciter une autorisation préalable avant de concourir pour un titre quelconque dans une autre forme de boxe pieds-poings dans le cadre d'une compétition organisée par une autre fédération ou organisme sportif. S'il incombe à une fédération de veiller à la santé des sportifs et à l'organisation des compétitions nationales, elle ne saurait, sans porter une atteinte excessive à la liberté d'accès aux activités sportives  soumettre à un régime d'autorisation préalable leur participation  à une compétition ou une manifestation sportive organisée par une autre fédération ou un organisme sportif.</ANA>
<ANA ID="9B"> 63-05-01-04 En vertu de l'article 13.4 du règlement intérieur de la Fédération française de savate boxe française et disciplines associées (FFSBFDA), un sportif participant aux compétitions organisées par cette fédération doit solliciter une autorisation préalable avant de concourir pour un titre quelconque dans une autre forme de boxe pieds-poings dans le cadre d'une compétition organisée par une autre fédération ou organisme sportif. S'il incombe à une fédération de veiller à la santé des sportifs et à l'organisation des compétitions nationales, elle ne saurait, sans porter une atteinte excessive à la liberté d'accès aux activités sportives  soumettre à un régime d'autorisation préalable leur participation  à une compétition ou une manifestation sportive organisée par une autre fédération ou un organisme sportif.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Section, 16 mars 1984,,, n° 50878, p. 108. Rappr. CE, 14 mai 1990, Lille Université Club, n° 94917, T. pp. 557-1007.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
