<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041938559</ID>
<ANCIEN_ID>JG_L_2020_05_000000421569</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/93/85/CETATEXT000041938559.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 1ère chambres réunies, 29/05/2020, 421569</TITRE>
<DATE_DEC>2020-05-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>421569</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO ; SCP ROUSSEAU, TAPIE ; SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>M. Eric Thiers</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:421569.20200529</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. I... G..., Mme C... D... et le conseil régional de Normandie de l'ordre des vétérinaires ont porté plainte contre M. H... B..., M. A... F... et la société civile professionnelle B...-F... devant la chambre régionale de discipline de Normandie de l'ordre des vétérinaires. Par une décision du 7 octobre 2016, la chambre régionale de discipline a infligé à M. B..., à M. F... et à la société civile professionnelle B...-F... la sanction de la suspension temporaire du droit d'exercer la profession de vétérinaire d'une durée de six mois, assortie du sursis pour une durée de trois mois, sur le territoire du ressort de la chambre régionale.<br/>
<br/>
              Par une décision du 17 avril 2018, la chambre nationale de discipline de l'ordre des vétérinaires, saisie par M. B..., M. F... et la société B...-F..., d'une part, par le Conseil national de l'ordre des vétérinaires, d'autre part, a réformé la décision de la chambre disciplinaire de première instance en ne prononçant pas de sanction à l'encontre de M. B... et en infligeant à M. F... et à la société B...-F... la sanction de la suspension temporaire du droit d'exercer la profession de vétérinaire d'une durée de quatre mois, assortie du sursis pour une durée de trois mois, sur l'ensemble du territoire national.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 18 juin et 12 septembre 2018 au secrétariat du contentieux du Conseil d'Etat, la société civile professionnelle B...-F... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette décision ;<br/>
<br/>
              2°) de mettre à la charge du Conseil national de l'ordre des vétérinaires, de M. G... et de Mme D... la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code rural et de la pêche maritime ;<br/>
              - le code de la santé publique ;<br/>
              - la loi n° 66-879 du 29 novembre 1966 ;<br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 modifiée ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Eric Thiers, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de la société B...-F..., à la SCP Rousseau, Tapie, avocat du Conseil national de l'ordre des vétérinaires et à la SCP Foussard, Froger, avocat de M. G... et de Mme D... ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par une décision du 7 octobre 2016, la chambre régionale de discipline de Normandie de l'ordre des vétérinaires a infligé à M. B..., à M. F... et à la société B...-F... une sanction de suspension temporaire du droit d'exercer la profession de vétérinaire d'une durée de six mois, assortie d'un sursis pour trois mois, sur le territoire de son ressort. Saisie d'un appel des intéressés ainsi que d'un appel du Conseil national de l'ordre des vétérinaires, la chambre nationale de discipline de l'ordre des vétérinaires a, par une décision du 17 avril 2018, réformé la décision de la chambre régionale de discipline en ne prononçant pas de sanction à l'encontre de M. B... et en infligeant à M. F... et à la société civile professionnelle B...-F... une sanction de suspension du droit d'exercer la profession de vétérinaire d'une durée de quatre mois, assortie d'un sursis pour trois mois, sur l'ensemble du territoire national. La société civile professionnelle B...-F... se pourvoit en cassation contre cette décision, dont elle n'est recevable à demander l'annulation qu'en tant qu'elle lui inflige une sanction. <br/>
<br/>
              2. En premier lieu, aux termes de l'article L. 242-7 du code rural et de la pêche maritime : "  (...) II.- Sans préjudice des sanctions disciplinaires pouvant être prononcées, le cas échéant, à l'encontre des personnes physiques mentionnées au I exerçant en leur sein, les sociétés mentionnées aux articles L. 241-3 et L. 241-17 peuvent se voir appliquer, dans les conditions prévues au I, les sanctions disciplinaires suivantes : / 1° L'avertissement ; / 2° La suspension temporaire du droit d'exercer la profession pour une durée maximale de dix ans, sur tout ou partie du territoire national, assortie ou non d'un sursis partiel ou total ; / 3° La radiation du tableau de l'ordre (...) ".<br/>
<br/>
              3. Il résulte des principes généraux du droit disciplinaire qu'une sanction infligée en première instance par une juridiction disciplinaire ne peut être aggravée par le juge d'appel, lorsqu'il n'est régulièrement saisi que du recours de la personne frappée par la sanction. Pour l'application de cette règle, dont la méconnaissance doit le cas échéant être relevée d'office par le juge de cassation, la gravité d'une sanction d'interdiction prononcée par la juridiction disciplinaire s'apprécie au regard de son objet et de sa durée, indépendamment des modalités d'exécution de la sanction, notamment de l'octroi éventuel d'un sursis ou de la fixation de son champ géographique d'application. Dès lors, en réduisant de six mois à quatre mois la durée de la suspension temporaire du droit d'exercer la profession de vétérinaire infligée à la société B...-F..., tout en étendant le champ géographique de cette sanction du ressort de la chambre régionale de discipline de Normandie à l'ensemble du territoire national, la chambre nationale de discipline ne peut être regardée comme ayant aggravé la sanction prononcée en première instance. Le moyen tiré de ce qu'elle aurait méconnu le principe général du droit disciplinaire rappelé ci-dessus ne peut par suite, en tout état de cause, qu'être écarté.<br/>
<br/>
              4. En deuxième lieu, aux termes de l'article R. 241-44 du code rural et de la pêche maritime, applicable aux sociétés civiles professionnelles : " Les statuts organisent la gérance et déterminent les pouvoirs des gérants dans les conditions prévues par l'article 11 de la loi n° 66-879 du 29 novembre 1966 ", lequel dispose que : " Tous les associés sont gérants sauf stipulation contraire des statuts qui peuvent désigner un ou plusieurs gérants parmi les associés ou en prévoir la désignation par un acte ultérieur ". En estimant que la société civile professionnelle B...-F... avait été " entendue " par le rapporteur devant la chambre de discipline de première instance et avait " signé " le procès-verbal d'audition, dès lors que M. B... et M. F..., gérants de la société civile professionnelle B...-F..., avaient été entendus par le rapporteur, tant sur leur situation personnelle que sur celle de la société, et avaient signé le procès-verbal de cette audition, la chambre nationale de discipline n'a pas dénaturé les pièces du dossier qui lui était soumis.<br/>
<br/>
              5. En troisième lieu, la décision attaquée est suffisamment motivée en ce qu'elle juge que la société civile professionnelle B...-F... ne pouvait ignorer les faits qui lui sont reprochés.<br/>
<br/>
              6. En quatrième lieu, aux termes de l'article L. 5141-14-2 du code de la santé publique, dans sa rédaction alors applicable : " A l'occasion de la vente de médicaments vétérinaires contenant une ou plusieurs substances antibiotiques, les remises, rabais, ristournes, la différenciation des conditions générales et particulières de vente au sens du I de l'article L. 441-6 du code de commerce ou la remise d'unités gratuites et toutes pratiques équivalentes sont interdits. Toute pratique commerciale visant à contourner, directement ou indirectement, cette interdiction par l'attribution de remises, rabais ou ristournes sur une autre gamme de produits qui serait liée à l'achat de ces médicaments est prohibée (...) ".<br/>
<br/>
              7. En se fondant sur la circonstance que la lettre et le contrat adressés aux clients ruraux de la société B...-F... proposaient " une remise qualifiée d'exceptionnelle de 2 % sur les antibiotiques " ainsi qu'une " remise couplée de 10 % sur les autres médicaments " pour juger que les dispositions précitées de l'article L. 5141-14-2 du code de la santé publique avaient été méconnues, la chambre de discipline nationale n'a pas commis d'erreur de droit, ni insuffisamment motivé sa décision.<br/>
<br/>
              8. Il résulte de tout ce qui précède que le pourvoi de la société B...-F... doit être rejeté, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative. Il y a lieu, en revanche, dans les circonstances de l'espèce, de mettre à la charge de la société B...-F... une somme de 1 500 euros à verser à M. G... et de 1 500 euros à verser à Mme D... au titre de ces mêmes dispositions.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la société civile professionnelle B...-F... est rejeté.<br/>
Article 2 : La société civile professionnelle B...-F... versera une somme de 1 500 euros à M. G... et une somme de 1 500 euros à Mme D... au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la société civile professionnelle B...-F..., à M. I... G..., à Mme C... D... et au Conseil national de l'ordre des vétérinaires.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-07-06 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. POUVOIRS DU JUGE DISCIPLINAIRE. - APPEL - JUGE SAISI DU SEUL RECOURS DE LA PERSONNE SANCTIONNÉE - IMPOSSIBILITÉ D'AGGRAVER LA SANCTION [RJ1] - 1) PORTÉE - AGGRAVATION DE LA PEINE APPRÉCIÉE INDÉPENDAMMENT DE SES MODALITÉS D'EXÉCUTION - NOTION DE MODALITÉS D'EXÉCUTION [RJ2] - INCLUSION - CHAMP GÉOGRAPHIQUE D'APPLICATION DE LA SANCTION - 2) ESPÈCE - RÉDUCTION DE LA DURÉE DE LA PEINE ET EXTENSION DE SON CHAMP GÉOGRAPHIQUE - LÉGALITÉ [RJ3].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">55-04-02 PROFESSIONS, CHARGES ET OFFICES. DISCIPLINE PROFESSIONNELLE. SANCTIONS. - APPEL - JUGE SAISI DU SEUL RECOURS DE LA PERSONNE SANCTIONNÉE - IMPOSSIBILITÉ D'AGGRAVER LA SANCTION [RJ1] - 1) PORTÉE - AGGRAVATION DE LA PEINE APPRÉCIÉE INDÉPENDAMMENT DE SES MODALITÉS D'EXÉCUTION - NOTION DE MODALITÉS D'EXÉCUTION [RJ2] - INCLUSION - CHAMP GÉOGRAPHIQUE D'APPLICATION DE LA SANCTION - 2) ESPÈCE - RÉDUCTION DE LA DURÉE DE LA PEINE ET EXTENSION DE SON CHAMP GÉOGRAPHIQUE - LÉGALITÉ [RJ3].
</SCT>
<ANA ID="9A"> 54-07-06 1) Il résulte des principes généraux du droit disciplinaire qu'une sanction infligée en première instance par une juridiction disciplinaire ne peut être aggravée par le juge d'appel, lorsqu'il n'est régulièrement saisi que du recours de la personne frappée par la sanction. Pour l'application de cette règle dont la méconnaissance doit le cas échéant être relevée d'office par le juge de cassation, la gravité d'une sanction d'interdiction prononcée par la juridiction disciplinaire s'apprécie au regard de son objet et de sa durée, celle-ci s'entendant indépendamment des modalités d'exécution de la sanction, notamment de l'octroi éventuel d'un sursis ou de la fixation de son champ géographique d'application.... ,,2) En réduisant de six mois à quatre mois la durée de la suspension temporaire du droit d'exercer la profession infligée à la société mise en cause, tout en étendant le champ géographique de cette sanction du ressort de la chambre régionale de discipline à l'ensemble du territoire national, la chambre nationale de discipline ne peut être regardée comme ayant aggravé la sanction prononcée en première instance.</ANA>
<ANA ID="9B"> 55-04-02 1) Il résulte des principes généraux du droit disciplinaire qu'une sanction infligée en première instance par une juridiction disciplinaire ne peut être aggravée par le juge d'appel, lorsqu'il n'est régulièrement saisi que du recours de la personne frappée par la sanction. Pour l'application de cette règle dont la méconnaissance doit le cas échéant être relevée d'office par le juge de cassation, la gravité d'une sanction d'interdiction prononcée par la juridiction disciplinaire s'apprécie au regard de son objet et de sa durée, celle-ci s'entendant indépendamment des modalités d'exécution de la sanction, notamment de l'octroi éventuel d'un sursis ou de la fixation de son champ géographique d'application.... ,,2) En réduisant de six mois à quatre mois la durée de la suspension temporaire du droit d'exercer la profession infligée à la société mise en cause, tout en étendant le champ géographique de cette sanction du ressort de la chambre régionale de discipline à l'ensemble du territoire national, la chambre nationale de discipline ne peut être regardée comme ayant aggravé la sanction prononcée en première instance.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 17 juillet 2013, M.,, n° 362481, p. 223.,,[RJ2] Cf., en précisant, CE, 1er février 2017, M.,, n° 384483, T. pp. 769-787,,[RJ3] Comp., s'agissant d'une extension du champ géographique d'une sanction inchangée, CE, 9 décembre 2016, Mme,, n° 393414, inédite au Recueil.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
