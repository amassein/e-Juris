<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043328507</ID>
<ANCIEN_ID>JG_L_2021_04_000000434919</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/32/85/CETATEXT000043328507.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 02/04/2021, 434919</TITRE>
<DATE_DEC>2021-04-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>434919</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC/>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP RICARD, BENDEL-VASSEUR, GHNASSIA</AVOCATS>
<RAPPORTEUR>M. Martin Guesdon</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Gabrielle Merloz</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:434919.20210402</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. A... B..., l'association " Les Enfants de demain " et M. D... C... ont demandé au tribunal administratif de Toulouse d'annuler la décision du 20 juillet 2016 par laquelle le directeur académique des services de l'éducation nationale de la Haute-Garonne a mis en demeure les responsables légaux des élèves fréquentant l'école primaire Al Badr d'inscrire leurs enfants dans un autre établissement dans les plus brefs délais et précisant que le refus de se conformer à cette injonction constitue un délit, ainsi que la décision du 29 août 2016 rejetant le recours gracieux formé le 30 juillet 2016 contre la décision du 20 juillet 2016.<br/>
<br/>
              Par un jugement n° 1604837 du 4 juillet 2017, le tribunal administratif de Toulouse a rejeté leur demande.<br/>
<br/>
              Par un arrêt n° 17BX03127 du 30 juillet 2019, la cour administrative d'appel de Bordeaux a annulé ce jugement et les décisions du directeur académique des services de l'éducation nationale de la Haute-Garonne des 20 juillet et 29 août 2016. <br/>
<br/>
              Par un pourvoi, enregistré le 26 septembre 2019 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'éducation nationale et de la jeunesse demande au Conseil d'Etat d'annuler cet arrêt.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'éducation ;<br/>
              - le code pénal ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Martin Guesdon, auditeur,  <br/>
<br/>
              - les conclusions de Mme Marie-Gabrielle Merloz, rapporteure publique ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Ricard, Bendel-Vasseur, Ghnassia, avocat de M. C... et de l'association " Les enfants de demain " ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond qu'à la suite d'un contrôle de l'établissement privé d'enseignement hors contrat dénommé " groupe scolaire privé Al Badr ", réalisé le 7 avril 2015, le directeur académique des services de l'éducation nationale de la Haute-Garonne a adressé, le 7 mai 2015, à M D... C..., directeur de cet établissement, une lettre l'informant de carences ayant été constatées au regard des obligations de délivrer un enseignement conforme à l'objet de l'instruction obligatoire et de ce qu'un nouveau contrôle aurait lieu l'année scolaire suivante. Après que ce nouveau contrôle, effectué le 12 avril 2016, eut confirmé l'existence de carences, le directeur académique des services de l'éducation nationale a mis en demeure, le 20 juillet 2016, les parents d'enfants scolarisés dans cette école de les inscrire dans un autre établissement puis, le 29 août 2016, a rejeté les recours gracieux formés contre cette mise en demeure. M. A... B..., l'association " Les Enfants de demain ", gérant l'établissement, et M. C... ont demandé au tribunal administratif de Toulouse d'annuler ces décisions. Par un jugement du 4 juillet 2017, le tribunal administratif de Toulouse a rejeté cette demande. Le ministre de l'éducation nationale et de la jeunesse se pourvoit en cassation contre l'arrêt du 30 juillet 2019 par lequel la cour administrative d'appel de Bordeaux, après avoir annulé ce jugement, a annulé les décisions du directeur académique des services de l'éducation nationale de Haute-Garonne des 20 juillet et 29 août 2016. <br/>
<br/>
              2. Aux termes de l'article L. 442-2 du code de l'éducation, dans sa rédaction applicable au litige : " Le contrôle de l'Etat sur les établissements d'enseignement privés qui ne sont pas liés à l'Etat par contrat se limite aux titres exigés des directeurs et des maîtres, à l'obligation scolaire, à l'instruction obligatoire, au respect de l'ordre public et des bonnes moeurs, à la prévention sanitaire et sociale. / L'autorité de l'Etat compétente en matière d'éducation peut prescrire chaque année un contrôle des classes hors contrat afin de s'assurer que l'enseignement qui y est dispensé respecte les normes minimales de connaissances requises par l'article L. 131-1-1 et que les élèves de ces classes ont accès au droit à l'éducation tel que celui-ci est défini par l'article L. 111-1. / Ce contrôle a lieu dans l'établissement d'enseignement privé dont relèvent ces classes hors contrat. / Les résultats de ce contrôle sont notifiés au directeur de l'établissement avec l'indication du délai dans lequel il sera mis en demeure de fournir ses explications ou d'améliorer la situation et des sanctions dont il serait l'objet dans le cas contraire. / En cas de refus de sa part d'améliorer la situation et notamment de dispenser, malgré la mise en demeure de l'autorité de l'Etat compétente en matière d'éducation, un enseignement conforme à l'objet de l'instruction obligatoire, tel que celui-ci est défini par les articles L. 131-1-1 et L. 131-10, l'autorité académique avise le procureur de la République des faits susceptibles de constituer une infraction pénale. / Dans cette hypothèse, les parents des élèves concernés sont mis en demeure d'inscrire leur enfant dans un autre établissement ".<br/>
<br/>
              3. Aux termes du second alinéa de l'article 227-17-1 du code pénal, dans sa rédaction applicable au litige : " Le fait, par un directeur d'établissement privé accueillant des classes hors contrat, de n'avoir pas pris, malgré la mise en demeure de l'autorité de l'Etat compétente en matière d'éducation, les dispositions nécessaires pour que l'enseignement qui y est dispensé soit conforme à l'objet de l'instruction obligatoire, tel que celui-ci est défini par l'article L. 131-1-1 et L. 131-10 du code de l'éducation, et de n'avoir pas procédé à la fermeture de ces classes est puni de six mois d'emprisonnement et de 7 500 euros d'amende. En outre, le tribunal peut ordonner à l'encontre de celui-ci l'interdiction de diriger ou d'enseigner ainsi que la fermeture de l'établissement ".<br/>
<br/>
              4. En principe, l'autorité de la chose jugée au pénal ne s'impose à l'administration comme au juge administratif qu'en ce qui concerne les constatations de fait que les juges répressifs ont retenues et qui sont le support nécessaire du dispositif d'un jugement devenu définitif, tandis que la même autorité ne saurait s'attacher aux motifs d'un jugement de relaxe tirés de ce que les faits reprochés ne sont pas établis ou de ce qu'un doute subsiste sur leur réalité. Il appartient, dans ce cas, à l'autorité administrative d'apprécier si les mêmes faits sont suffisamment établis et, dans l'affirmative, s'ils justifient l'application d'une sanction administrative. Il n'en va autrement que lorsque la légalité de la décision administrative est subordonnée à la condition que les faits qui servent de fondement à cette décision constituent une infraction pénale, l'autorité de la chose jugée s'étendant alors exceptionnellement à la qualification juridique donnée aux faits par le juge pénal.<br/>
<br/>
              5. Il résulte des dispositions citées au point 2 de l'article L. 442-2 du code de l'éducation que si, à la suite du contrôle d'un établissement privé hors contrat et de la notification à son directeur des résultats de ce contrôle et d'une mise en demeure d'améliorer la situation de l'établissement, le directeur refuse d'y procéder et, notamment, de dispenser un enseignement conforme à l'objet de l'instruction obligatoire, l'autorité de l'Etat compétente en matière d'éducation avise le procureur de la République des faits susceptibles de constituer une infraction pénale et met en demeure les parents des élèves concernés d'inscrire leur enfant dans un autre établissement. La légalité de cette mise en demeure adressée aux parents des élèves n'est ni conditionnée à l'engagement ultérieur par le procureur de la République de poursuites pénales sur le fondement des dispositions citées au point 3 de l'article 227-17-1 du code pénal, ni fondée sur la seule circonstance que le non-respect par le directeur de l'établissement des obligations imposées par la mise en demeure, qui lui avait été antérieurement adressée, serait constitutif d'une infraction pénale. Par suite, en se fondant exclusivement, pour annuler la mise en demeure adressée aux parents des élèves, sur l'arrêt de la cour d'appel de Toulouse du 20 décembre 2018 renvoyant des fins de la poursuite M. C... et l'association " Les Enfants de demain ", respectivement directeur et gérant de l'établissement, au motif que l'infraction réprimée par l'article 227-17-1 du code pénal n'était pas caractérisée, la cour a commis une erreur de droit. <br/>
<br/>
              6. Il résulte de ce qui précède que le ministre de l'éducation nationale et de la jeunesse est fondé à demander l'annulation de l'arrêt de la cour administrative d'appel de Bordeaux qu'il attaque.<br/>
<br/>
              7. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>
              D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Bordeaux du 30 juillet 2019 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Bordeaux.<br/>
Article 3 : Les conclusions de l'association " Les Enfants de demain " et de M. C... présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée au ministre de l'éducation nationale, de la jeunesse et des sports, à l'association " Les Enfants de demain " et à M. D... C....<br/>
Copie sera transmise à M. A... B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">30-02-07-02 ENSEIGNEMENT ET RECHERCHE. QUESTIONS PROPRES AUX DIFFÉRENTES CATÉGORIES D'ENSEIGNEMENT. ÉTABLISSEMENTS D'ENSEIGNEMENT PRIVÉS. RELATIONS ENTRE LES COLLECTIVITÉS PUBLIQUES ET LES ÉTABLISSEMENTS PRIVÉS. - ETABLISSEMENT PRIVÉ HORS CONTRAT - MISE EN DEMEURE AUX PARENTS D'INSCRIRE LEUR ENFANT DANS UN AUTRE ÉTABLISSEMENT À LA SUITE D'UN CONTRÔLE - DÉCISION CONDITIONNÉE À L'EXISTENCE D'UNE INFRACTION DU DIRECTEUR DE L'ÉTABLISSEMENT - ABSENCE - CONSÉQUENCE - ABSENCE D'AUTORITÉ DE LA CHOSE JUGÉE PAR LE JUGE PÉNAL CONSTATANT L'ABSENCE D'INFRACTION [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-06-06-02-02 PROCÉDURE. JUGEMENTS. CHOSE JUGÉE. CHOSE JUGÉE PAR LA JURIDICTION JUDICIAIRE. CHOSE JUGÉE PAR LE JUGE PÉNAL. - AUTORITÉ S'ÉTENDANT À LA QUALIFICATION JURIDIQUE DES FAITS [RJ1] - EXCLUSION - MISE EN DEMEURE AUX PARENTS D'INSCRIRE LEUR ENFANT DANS UN AUTRE ÉTABLISSEMENT À LA SUITE DU CONTRÔLE D'UN ÉTABLISSEMENT PRIVÉ HORS CONTRAT, DÈS LORS QUE CETTE DÉCISION N'EST PAS CONDITIONNÉE À L'EXISTENCE D'UNE INFRACTION DU DIRECTEUR DE L'ÉTABLISSEMENT.
</SCT>
<ANA ID="9A"> 30-02-07-02 Il résulte de l'article L 442-2 du code de l'éducation que si, à la suite du contrôle d'un établissement privé hors contrat et de la notification à son directeur des résultats de ce contrôle et d'une mise en demeure d'améliorer la situation de l'établissement, le directeur refuse d'y procéder et, notamment, de dispenser un enseignement conforme  à l'objet de l'instruction obligatoire, l'autorité de l'Etat compétente en matière d'éducation avise le procureur de la République des faits susceptibles de constituer une infraction pénale et met en demeure les parents des élèves concernés d'inscrire leur enfant dans un autre établissement.... ,,La légalité de cette mise en demeure adressée aux parents des élèves n'est ni conditionnée à l'engagement ultérieur par le procureur de la République de poursuites pénales sur le fondement de l'article 227-17-1 du code pénal, ni fondée sur la seule circonstance que le non-respect par le directeur de l'établissement des obligations imposées par la mise en demeure, qui lui avait été antérieurement adressée, serait constitutif d'une infraction pénale.... ,,Par suite, le juge du fond ne peut, pour annuler la mise en demeure adressée aux parents des élèves, se fonder exclusivement sur un jugement du juge pénal renvoyant des fins de la poursuite le directeur et le gérant de l'établissement au motif que l'infraction réprimée par l'article 227-17-1 du code pénal n'était pas caractérisée.</ANA>
<ANA ID="9B"> 54-06-06-02-02 Il résulte de l'article L 442-2 du code de l'éducation que si, à la suite du contrôle d'un établissement privé hors contrat et de la notification à son directeur des résultats de ce contrôle et d'une mise en demeure d'améliorer la situation de l'établissement, le directeur refuse d'y procéder et, notamment, de dispenser un enseignement conforme  à l'objet de l'instruction obligatoire, l'autorité de l'Etat compétente en matière d'éducation avise le procureur de la République des faits susceptibles de constituer une infraction pénale et met en demeure les parents des élèves concernés d'inscrire leur enfant dans un autre établissement.... ,,La légalité de cette mise en demeure adressée aux parents des élèves n'est ni conditionnée à l'engagement ultérieur par le procureur de la République de poursuites pénales sur le fondement de l'article 227-17-1 du code pénal, ni fondée sur la seule circonstance que le non-respect par le directeur de l'établissement des obligations imposées par la mise en demeure, qui lui avait été antérieurement adressée, serait constitutif d'une infraction pénale.... ,,Par suite, le juge du fond ne peut, pour annuler la mise en demeure adressée aux parents des élèves, se fonder exclusivement sur un jugement du juge pénal renvoyant des fins de la poursuite le directeur et le gérant de l'établissement au motif que l'infraction réprimée par l'article 227-17-1 du code pénal n'était pas caractérisée.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., sur les principes gouvernant l'autorité de la chose jugée par le juge pénal pour l'administration, CE, Assemblée, 8 janvier 1971, Ministre de l'intérieur c/ Dame,, n° 77800, p. 19 ; CE, Assemblée, 12 octobre 2018, SARL Super Coiffeur, n° 408567, p. 373.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
