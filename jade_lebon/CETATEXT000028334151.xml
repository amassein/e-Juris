<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028334151</ID>
<ANCIEN_ID>JG_L_2013_12_000000354268</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/33/41/CETATEXT000028334151.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 4ème sous-sections réunies, 16/12/2013, 354268</TITRE>
<DATE_DEC>2013-12-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>354268</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 4ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN ; SCP ROGER, SEVAUX, MATHONNET</AVOCATS>
<RAPPORTEUR>M. Jean-Dominique Langlais</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Fabienne Lambolez</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:354268.20131216</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 23 novembre 2011 et 20 février 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour Mme B...A..., demeurant... ; Mme A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 10LY01814 du 7 juillet 2011 par lequel la cour administrative d'appel de Lyon a rejeté son appel et celui de ses enfants contre le jugement n° 0901954 du 25 mai 2010 du tribunal administratif de Clermont-Ferrand rejetant leur demande de condamnation de l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales (ONIAM) à les indemniser des préjudices ayant résulté pour eux du décès de M. C...A..., survenu le 15 octobre 2007 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à cet appel ; <br/>
<br/>
              3°) de mettre à la charge de l'ONIAM le versement à la SCP Nicolaÿ- de Lanouvelle- Hannotin, son avocat, d'une somme de 3 000 euros au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 27 novembre 2013, présentée pour Mme A... ; <br/>
<br/>
              Vu le code de la santé publique ;<br/>
<br/>
              Vu la loi n° 91-647 du 10 juillet 1991 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Langlais, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de Mme Fabienne Lambolez, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de Mme A...et à la SCP Roger, Sevaux, Mathonnet, avocat de l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales (ONIAM) ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que, le 15 octobre 2007, M. C...A..., âgé de 69 ans et atteint depuis 2004 d'un cancer du poumon, est décédé d'une hémorragie au cours d'une lobectomie réalisée au centre hospitalier universitaire de Clermont-Ferrand ; que l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales (ONIAM) ayant refusé de réparer au titre de la solidarité nationale les préjudices ayant résulté pour eux de ce décès, la veuve et les enfants de la victime ont saisi le tribunal administratif de Clermont-Ferrand d'une action dirigée contre cet établissement public, que le tribunal a rejetée par un jugement du 25 mai 2010 ; que Mme A...se pourvoit en cassation contre l'arrêt du 7 juillet 2011 par lequel la cour administrative d'appel de Lyon a confirmé ce jugement ; <br/>
<br/>
              2. Considérant, en premier lieu, que si les consorts A...soutenaient devant la cour administrative d'appel que M. A...n'avait pas été informé des risques de l'intervention chirurgicale pratiquée le 15 octobre 2007, ce moyen, qui ne pouvait être utilement soulevé qu'à l'appui d'une action indemnitaire dirigée contre le centre hospitalier universitaire de Clermont-Ferrand, était inopérant dans le cadre du litige relatif à la prise en charge du préjudice par l'ONIAM au titre de la solidarité nationale ; que, par suite, la cour administrative d'appel n'a pas entaché son arrêt d'irrégularité en s'abstenant d'y répondre ; <br/>
<br/>
              3. Considérant, en second lieu, qu'aux termes du II de l'article L. 1142-1 du code de la santé publique : " Lorsque la responsabilité d'un professionnel, d'un établissement, service ou organisme mentionné au I ou d'un producteur de produits n'est pas engagée, un accident médical, une affection iatrogène ou une infection nosocomiale ouvre droit à la réparation des préjudices du patient, et, en cas de décès, de ses ayants droit au titre de la solidarité nationale, lorsqu'ils sont directement imputables à des actes de prévention, de diagnostic ou de soins et qu'ils ont eu pour le patient des conséquences anormales au regard de son état de santé comme de l'évolution prévisible de celui-ci et présentent un caractère de gravité, fixé par décret (...) " ; <br/>
<br/>
              4. Considérant que, pour juger que le décès de M. A...lors de l'intervention du 15 octobre 2007 n'ouvrait pas droit à réparation au titre de la solidarité nationale, la cour, se fondant sur le rapport d'expertise, a relevé qu'en raison de la fragilité importante des tissus induite par la très grande proximité de la tumeur et des zones cardio-vasculaires, le patient était particulièrement exposé au risque d'un accident hémorragique, qualifié par l'expert de " risque interventionnel classique ", et que cette fragilité tissulaire n'avait pas permis une dissection ordinaire de la tumeur ni un contrôle de l'hémorragie ; qu'il ressortait de ces éléments que M. A... avait dû subir, dans l'espoir d'obtenir une amélioration de son état de santé, une intervention indispensable présentant des risques importants liés à sa pathologie, et que l'accident résultait de la réalisation de l'un de ces risques ; qu'en en déduisant que le dommage ne pouvait être regardé comme anormal au regard de l'état de santé du patient comme de l'évolution prévisible de cet état, la cour n'a pas commis d'erreur de droit et a exactement qualifié les faits qui lui étaient soumis ;<br/>
<br/>
              5. Considérant qu'il résulte de tout ce qui précède que Mme A...n'est pas fondée à demander l'annulation de l'arrêt attaqué ; que ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative et de l'article 37 de la loi du 10 juillet 1991 ne peuvent, par suite, qu'être rejetées ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
		Article 1er : Le pourvoi de Mme A... est rejeté.<br/>
<br/>
Article 2 : La présente décision sera notifiée à Mme B...A..., à l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales et à la caisse de la mutualité sociale agricole de la Dordogne.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-08-02-02-01-02 PROCÉDURE. VOIES DE RECOURS. CASSATION. CONTRÔLE DU JUGE DE CASSATION. BIEN-FONDÉ. QUALIFICATION JURIDIQUE DES FAITS. - APPRÉCIATION DU CARACTÈRE ANORMAL DES CONSÉQUENCES D'UN ACTE MÉDICAL POUR L'APPLICATION DU II DE L'ARTICLE L. 1142-1 DU CSP.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">60-02-01-01-005-02 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RESPONSABILITÉ EN RAISON DES DIFFÉRENTES ACTIVITÉS DES SERVICES PUBLICS. SERVICE PUBLIC DE SANTÉ. ÉTABLISSEMENTS PUBLICS D'HOSPITALISATION. RESPONSABILITÉ SANS FAUTE. ACTES MÉDICAUX. - PRISE EN CHARGE PAR LA SOLIDARITÉ NATIONALE DES CONSÉQUENCES ANORMALES ET GRAVES DES ACTES MÉDICAUX (II DE L'ART. L. 1142-1 DU CSP) - APPRÉCIATION DU CARACTÈRE ANORMAL - 1) ELÉMENTS POUVANT ÊTRE PRIS EN COMPTE - PROBABILITÉ HABITUELLE DE RÉALISATION DU RISQUE - EXPOSITION PARTICULIÈRE DU PATIENT AU RISQUE EN RAISON DE SON ÉTAT DE SANTÉ - CARACTÈRE INCONTOURNABLE DE L'INTERVENTION - INCLUSION - 2)  CASSATION - CONTRÔLE DU JUGE - QUALIFICATION JURIDIQUE DES FAITS - 3) APPLICATION EN L'ESPÈCE - CAS DE DÉCÈS D'UN PATIENT - CONSÉQUENCES NON ANORMALES AU SENS DU II DE L'ARTICLE L. 1142-1 DU CSP.
</SCT>
<ANA ID="9A"> 54-08-02-02-01-02 Le juge de cassation contrôle la qualification juridique des faits retenus par les juges du fond pour apprécier le caractère anormal des conséquences d'un acte de prévention, de diagnostic ou de soins au sens du II de l'article L. 1142-1 du code de la santé publique (CSP).</ANA>
<ANA ID="9B"> 60-02-01-01-005-02 1) Pour apprécier le caractère anormal des conséquences d'un acte de prévention, de diagnostic ou de soins au sens du II de l'article L. 1142-1 du code de la santé publique (CSP), le juge peut se fonder sur la probabilité habituelle de réalisation de l'un des risques lié à l'intervention, sur l'exposition particulière du patient à ce risque en raison de son état de santé, et sur le caractère incontournable de l'intervention.,,,2) Le juge de cassation contrôle la qualification juridique des faits retenus par les juges du fond pour apprécier le caractère anormal des conséquences d'un acte de prévention, de diagnostic ou de soins.... ,,3) En l'espèce, ne présente pas un caractère anormal, au sens de ces mêmes dispositions, le décès d'un patient en raison d'une hémorragie survenue à l'occasion d'une lobectomie, qui revêtait un caractère indispensable dans l'espoir d'obtenir une amélioration de son état de santé, dès lors que le patient était, en raison d'une fragilité tissulaire, particulièrement exposé au risque d'accident hémorragique et que ce risque était qualifié par l'expert de risque interventionnel classique.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
