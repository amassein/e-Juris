<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037022316</ID>
<ANCIEN_ID>JG_L_2018_06_000000413511</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/02/23/CETATEXT000037022316.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème et 1ère chambres réunies, 06/06/2018, 413511</TITRE>
<DATE_DEC>2018-06-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>413511</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème et 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER ; SCP COUTARD, MUNIER-APAIRE</AVOCATS>
<RAPPORTEUR>Mme Céline Roux</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:413511.20180606</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Monsieur A...B...a demandé au juge des référés du tribunal administratif de Grenoble, sur le fondement de l'article L. 521-1 du code de justice administrative, d'une part, d'ordonner la suspension de l'exécution de la décision du 5 septembre 2016, par laquelle l'Institut polytechnique de Grenoble a rejeté son recours gracieux formé contre la décision du 7 juillet 2016, aux termes de laquelle le jury d'admission en 2ème année d'école d'ingénieur a refusé son admission et, d'autre part, de faire injonction à l'Institut polytechnique de Grenoble d'émettre un avis favorable à la poursuite de ses études en école d'ingénieur ou de réexaminer sa demande dans un délai de huit jours à compter de la notification de l'ordonnance à intervenir sous astreinte de 200 euros par jour de retard. Par une ordonnance n° 1703654 du 12 juillet 2017, le juge des référés a rejeté sa demande. <br/>
<br/>
              Par un pourvoi et un mémoire en réplique, enregistrés les 18 août 2017 et 17 mai 2018 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Institut polytechnique de Grenoble la somme de 3 000 euros au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le décret n° 91-1266 du 19 décembre 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Céline Roux, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Foussard, Froger, avocat de M. B...et à la SCP Coutard, Munier-Apaire, avocat de l'Institut polytechnique de Grenoble ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis au juge des référés du tribunal administratif de Grenoble que M. B...s'est vu refuser l'admission à l'Ecole nationale supérieure de l'énergie, l'eau et l'environnement, qui dépend de l'Institut polytechnique de Grenoble ; qu'il a formé un recours gracieux contre cette décision, qui a été rejeté le 5 septembre 2016 par l'Institut polytechnique de Grenoble ; qu'ayant été admis au bénéfice de l'aide juridictionnelle en vue de contester cette décision en excès de pouvoir et s'étant vu, à ce titre, désigner un premier avocat, puis un second avocat en remplacement du premier, il a formé, d'une part, un recours pour excès de pouvoir contre la décision du 5 septembre 2016 et, d'autre part, une demande de suspension de l'exécution de la même décision, sur le fondement de l'article L. 521-1 du code de justice administrative ; que, par l'ordonnance du 12 juillet 2017 dont M. B...demande l'annulation, le juge des référés du tribunal administratif de Grenoble a rejeté sa demande de suspension, en se fondant sur la circonstance que sa demande au fond, qui avait été introduite plus de deux mois après la désignation du premier avocat, était tardive et, par suite, irrecevable ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article 76 du décret du 19 décembre 1991 portant application de la loi du 10 juillet 1991 relative à l'aide juridique : " Lorsque le demandeur à l'aide juridictionnelle ne produit pas de document attestant l'acceptation d'un avocat ou d'un officier public ou ministériel choisi par lui, la désignation de l'auxiliaire de justice peut être effectuée sur-le-champ par le membre du bureau ou de la section du bureau représentant la profession et ayant reçu délégation à cet effet " ; qu'aux termes de l'article 84 du même décret : " dans tous les cas où un auxiliaire de justice qui prêtait son concours au bénéficiaire de l'aide juridictionnelle est déchargé de sa mission, un remplaçant lui est immédiatement désigné " ; qu'enfin, aux termes de son article 38 : " lorsqu'une action en justice ou un recours doit être intenté avant l'expiration d'un délai devant les juridictions de première instance ou d'appel, l'action ou le recours est réputé avoir été intenté dans le délai si la demande d'aide juridictionnelle s'y rapportant est adressée au bureau d'aide juridictionnelle avant l'expiration dudit délai et si la demande en justice ou le recours est introduit dans un nouveau délai de même durée à compter : / a) De la notification de la décision d'admission provisoire ; / b) De la notification de la décision constatant la caducité de la demande ; / c) De la date à laquelle le demandeur à l'aide juridictionnelle ne peut plus contester la décision d'admission ou de rejet de sa demande en application du premier alinéa de l'article 56 et de l'article 160 ou, en cas de recours de ce demandeur, de la date à laquelle la décision relative à ce recours lui a été notifiée ; / d) Ou, en cas d'admission, de la date, si elle est plus tardive, à laquelle un auxiliaire de justice a été désigné " ; qu'il résulte de ces dispositions que, dans l'hypothèse où un auxiliaire de justice a été désigné en application de l'article 76 du décret du 19 décembre 1991 et que celui-ci est, avant que le recours ou l'action en justice ne soit intenté, remplacé par un autre auxiliaire de justice désigné dans les conditions prévues à l'article 84, le délai de recours contentieux qui, dans le cas mentionné au d) de l'article 38, aurait commencé à courir à compter de la première désignation, recommence à courir à compter de cette nouvelle désignation ; <br/>
<br/>
              3. Considérant, par suite, qu'en se fondant, pour juger que le recours pour excès de pouvoir formé par M. B...était tardif, sur la circonstance qu'il s'était écoulé plus de deux mois entre la désignation du premier avocat et l'introduction de ce recours, le juge des référés a entaché son ordonnance d'une erreur de droit ; que, par suite, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, M. B...est fondé à demander l'annulation de l'ordonnance qu'il attaque ; <br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de faire application des dispositions de l'article L. 821-2 du code de justice administrative et de régler l'affaire au titre de la procédure de référé engagée ; <br/>
<br/>
              5. Considérant que l'urgence justifie que soit prononcée la suspension d'un acte administratif lorsque l'exécution de celui-ci porte atteinte, de manière suffisamment grave et immédiate, à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre ; qu'il appartient au juge des référés d'apprécier concrètement, compte tenu des justifications fournies par le requérant, si les effets de l'acte litigieux sont de nature à caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, l'exécution de la décision soit suspendue ;<br/>
<br/>
              6. Considérant qu'en l'état de l'instruction, M. B...ne fait état d'aucune circonstance de nature à établir, à la date de la présente décision, l'existence d'une urgence à suspendre la décision contestée ; que, par suite, la condition d'urgence prévue par l'article L. 521-1 du code de justice administrative n'étant pas remplie, sa demande de suspension doit être rejetée ;<br/>
<br/>
              7. Considérant que, l'Institut polytechnique de Grenoble n'étant pas la partie perdante, les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'il soit fait droit aux conclusions présentées à ce titre par M.B..., tant en première instance qu'en cassation ; que, dans les circonstances de l'espèce, il n'y a pas lieu de faire droit aux conclusions présentées, au même titre, en première instance, par l'Institut polytechnique de Grenoble ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du 12 juillet 2017 du juge des référés du tribunal administratif de Grenoble est annulée.<br/>
Article 2 : La demande présentée par M. B...devant le juge des référés du tribunal administratif de Grenoble est rejetée. <br/>
Article 3 : Le surplus des conclusions du pourvoi de M. B...présenté au titre de l'article L. 761-1 du code de justice administrative est rejeté.<br/>
Article 4 : Les conclusions présentées, devant le tribunal administratif de Grenoble, au titre de l'article L. 761-1 du code de justice administrative, par l'Institut polytechnique de Grenoble sont rejetées. <br/>
Article 5 : La présente décision sera notifiée à M. A... B...et à l'Institut polytechnique de Grenoble.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-01-07-04 PROCÉDURE. INTRODUCTION DE L'INSTANCE. DÉLAIS. INTERRUPTION ET PROLONGATION DES DÉLAIS. - INTERRUPTION DU DÉLAI DE RECOURS CONTENTIEUX PAR UNE DEMANDE D'AIDE JURIDICTIONNELLE - DATE À LAQUELLE LE DÉLAI INTERROMPU RECOMMENCE À COURIR - CAS OÙ L'AUXILIAIRE DE JUSTICE DÉSIGNÉ EST REMPLACÉ [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-06-05-09 PROCÉDURE. JUGEMENTS. FRAIS ET DÉPENS. AIDE JURIDICTIONNELLE. - INTERRUPTION DU DÉLAI DE RECOURS CONTENTIEUX PAR UNE DEMANDE D'AIDE JURIDICTIONNELLE - DATE À LAQUELLE LE DÉLAI INTERROMPU RECOMMENCE À COURIR - CAS OÙ L'AUXILIAIRE DE JUSTICE DÉSIGNÉ EST REMPLACÉ [RJ1].
</SCT>
<ANA ID="9A"> 54-01-07-04 Il résulte des articles 38, 38 et 84 du décret n° 91-1266 du 19 décembre 1991 que, dans l'hypothèse où un auxiliaire de justice a été désigné en application de l'article 76 de ce décret et que celui-ci est, avant que le recours ou l'action en justice ne soit intenté, remplacé par un autre auxiliaire de justice désigné dans les conditions prévues à l'article 84, le délai de recours contentieux qui, dans le cas mentionné au d) de l'article 38, aurait commencé à courir à compter de la première désignation, recommence à courir à compter de cette nouvelle désignation.</ANA>
<ANA ID="9B"> 54-06-05-09 Il résulte des articles 38, 38 et 84 du décret n° 91-1266 du 19 décembre 1991 que, dans l'hypothèse où un auxiliaire de justice a été désigné en application de l'article 76 de ce décret et que celui-ci est, avant que le recours ou l'action en justice ne soit intenté, remplacé par un autre auxiliaire de justice désigné dans les conditions prévues à l'article 84, le délai de recours contentieux qui, dans le cas mentionné au d) de l'article 38, aurait commencé à courir à compter de la première désignation, recommence à courir à compter de cette nouvelle désignation.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., pour l'hypothèse où la désignation de l'auxiliaire de justice est postérieure à la date à laquelle la décision d'admission ou de rejet du BAJ devient définitive, CE, Section, 28 juin 2013,,, n° 363460, p. 185.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
