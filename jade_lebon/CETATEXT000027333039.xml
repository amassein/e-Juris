<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027333039</ID>
<ANCIEN_ID>JG_L_2013_04_000000361721</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/33/30/CETATEXT000027333039.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème et 2ème sous-sections réunies, 19/04/2013, 361721</TITRE>
<DATE_DEC>2013-04-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>361721</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème et 2ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELAPORTE, BRIARD, TRICHET ; SCP BARTHELEMY, MATUCHANSKY, VEXLIARD</AVOCATS>
<RAPPORTEUR>M. François Lelièvre</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:361721.20130419</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 7 août et 7 novembre 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour la société Elres, dont le siège est 61-69 rue de Bercy à Paris (75012) ; la société Elres demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 07VE00670-07VE00715-09VE02184 du 14 juin 2012 par lequel la cour administrative d'appel de Versailles, en premier lieu, a annulé le jugement du 4 octobre 2004 n° 0004874-0103875-0303921-0305012 du tribunal administratif de Versailles en tant qu'il fixe ses parts de responsabilité et celles de la commune de Draveil, en deuxième lieu, a annulé le jugement n° 0103875 du 10 janvier 2007 du même tribunal, en troisième lieu, a fixé la part de responsabilité restant à la charge des parties sur le fondement quasi-délictuel à 20 % pour la commune et à 80 % pour sa part, en quatrième lieu, a condamné la commune à lui verser la somme de 235 200 euros au titre du bénéfice manqué ainsi que les intérêts déterminés en fonction du manque à gagner constitué à l'issue de chaque année d'exécution du contrat et l'a condamnée à verser à la commune la somme de 62 978,08 euros, en cinquième lieu, a mis à la charge de la commune de Draveil pour 20 % les frais d'expertise, taxés et liquidés à la somme de 34 923,20 euros par ordonnance du 20 avril 2006 et 80 % pour sa part, et, en dernier lieu, a rejeté le surplus des conclusions des parties ; <br/>
<br/>
              2°) réglant l'affaire au fond, d'annuler les jugements des 4 octobre 2004 et 10 janvier 2007 du tribunal administratif de Versailles, de condamner la commune de Draveil à lui verser les sommes de 197 173,20 euros et 2 335 172,39 euros, de rejeter l'ensemble des demandes reconventionnelles de la commune de Draveil et de mettre les frais d'expertise à la charge exclusive de la commune de Draveil ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Draveil le versement de la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code des marchés publics ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. François Lelièvre, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Delaporte, Briard, Trichet, avocat de la société Elres, et de la SCP Barthélemy, Matuchansky, Vexliard, avocat de la commune de Draveil,<br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Delaporte, Briard, Trichet, avocat de la société Elres, et à la SCP Barthélemy, Matuchansky, Vexliard, avocat de la commune de Draveil ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis au juge du fond que la commune de Draveil a conclu le 10 septembre 1990 avec la société générale de restauration, aux droits de laquelle est venue la société Avenance enseignement et santé, désormais dénommée société Elres, une convention lui déléguant la gestion du service de restauration scolaire et municipale pour une durée de cinq ans ; que, par un avenant du 15 janvier 1993, la durée de cette convention a été portée à quinze ans ; que la commune ayant résilié la convention le 26 juin 2000, son cocontractant a saisi le tribunal administratif de Versailles d'une demande d'annulation de la mesure de résiliation ainsi que de conclusions indemnitaires ; que par jugement avant-dire droit du 2 octobre 2004, le tribunal administratif de Versailles a relevé la nullité de cette convention et statué sur le terrain des responsabilités quasi-contractuelle et quasi-délictuelle ; que par arrêt du 14 septembre 2006, la cour administrative d'appel de Versailles a confirmé la nullité du contrat et rejeté les conclusions de la société contestant la part de responsabilité laissée à sa charge ; que par une décision du 5 juin 2009, le Conseil d'Etat statuant au contentieux a annulé cet arrêt en tant seulement qu'il avait rejeté ses conclusions contestant le partage de responsabilité ;<br/>
<br/>
              2. Considérant que, par l'arrêt attaqué du 14 juin 2012, la cour administrative d'appel de Versailles s'est prononcée à nouveau, dans la limite du renvoi après cassation, sur le jugement du 4 octobre 2004 ; qu'elle s'est également prononcée sur l'appel dirigé contre  le jugement du 10 janvier 2007 statuant sur les conclusions indemnitaires des parties ; que la cour a annulé le premier jugement en tant qu'il avait fixé les parts respectives de responsabilité de la société Elres, et de la commune de Draveil et, par voie de conséquence, le jugement du 10 janvier 2007 appliquant ce partage de responsabilité ; qu'elle a ensuite statué par la voie de l'évocation sur les demandes et, d'une part, condamné la société Elres à verser à la commune de Draveil la somme de 62 978,08 euros avec intérêts légaux au titre des dépenses utiles, sur le fondement de l'enrichissement sans cause et, d'autre part, condamné la commune de Draveil à verser à la société Elres notamment la somme de 235 200 euros hors taxes avec intérêts légaux et capitalisation au titre du bénéfice manqué, sur le fondement quasi-délictuel ;<br/>
<br/>
              3. Considérant, en premier lieu, d'une part, que la cour administrative d'appel de Versailles ayant annulé le jugement du 10 janvier 2007 par voie de conséquence de l'annulation du jugement du 4 octobre 2004, la société Elres ne saurait utilement soutenir que la cour n'a pas répondu à un moyen tiré de l'irrégularité du jugement du 10 janvier 2007 ; que, d'autre part, la cour a suffisamment motivé son arrêt sur le partage de responsabilité entre la commune de Draveil et la société Elres ;<br/>
<br/>
              4. Considérant, en deuxième lieu, qu'il ressort de ses écritures d'appel que la commune de Draveil a demandé à être exonérée de toute responsabilité ; que, par suite, la cour administrative d'appel n'a pas statué au delà de la demande en limitant la part de responsabilité de la commune à 20 % ;<br/>
<br/>
              5. Considérant, en troisième lieu, que la décision du Conseil d'Etat mentionnée au point 1 a rejeté les conclusions du pourvoi dirigé contre l'arrêt du 14 septembre 2006 en ce qu'il avait jugé que le contrat conclu entre la commune de Draveil et la société Elres était entaché de nullité ; que cette partie de l'arrêt est ainsi devenue définitive ; qu'en estimant que, dans ces conditions, la société Elres ne pouvait utilement invoquer la méconnaissance du principe de loyauté des relations contractuelles à l'appui de ses conclusions indemnitaires, la cour n'a pas commis d'erreur de droit ;<br/>
<br/>
              6. Considérant, en quatrième lieu, que la cour a relevé que, le 15 janvier 1993, deux ans avant le terme du contrat et deux semaines avant la promulgation de la loi du 29 janvier 1993, les parties avaient conclu un avenant, sans publicité et mise en concurrence, afin de porter sa durée à quinze ans ; qu'elle a estimé que la société Avenance Enseignement, devenue la société Elres, était spécialisée dans le domaine de la restauration collective et disposait de larges connaissance de ce milieu ainsi que des dispositions en vigueur et de leur éventuelle évolution ; qu'après avoir porté sur ces faits une appréciation souveraine qui, dès lors qu'elle est exempte de dénaturation, ne peut être discutée en cassation, la cour a pu en déduire, sans entacher son arrêt d'erreur de qualification juridique, que la société Elres avait commis une faute en acceptant de conclure un tel contrat ;<br/>
<br/>
              7. Considérant, en cinquième lieu, qu'en estimant que la part respective de responsabilité de la commune de Draveil et de la société Avenance Enseignement et santé dans la signature de ce contrat irrégulier, faute d'avoir été soumis aux procédures de passation prévues par le code des marchés publics, devait être fixée respectivement à 20 % et 80 %, la cour administrative d'appel de Versailles a porté sur les faits de l'espèce une appréciation souveraine, exempte de dénaturation ;<br/>
<br/>
              8. Considérant, en sixième lieu, que la nullité du contrat a privé la société Elres de la possibilité de réaliser un bénéfice qui aurait été perçu chaque année pendant la durée de son exécution ; que la cour administrative d'appel ayant évalué son manque à gagner futur pour chacune des années, postérieures à la saisine du juge et à sa demande d'intérêts, pendant lesquelles le contrat aurait été exécuté si le juge ne l'avait pas jugé irrégulier, elle n'a pas commis d'erreur de droit en fixant le point de départ des intérêts en fonction du manque à gagner constitué à l'issue de chacune de ces années pendant lesquelles le contrat n'a pu être exécuté ;<br/>
<br/>
              9. Considérant, en septième lieu, que le moyen tiré de ce que les motifs de l'arrêt attaqué contrediraient son dispositif manque en fait ;<br/>
<br/>
              10. Considérant, en dernier lieu, qu'étant saisie à la fois du jugement du 4 octobre 2004 et du jugement rendu sur le fond le 10 janvier 2007, la cour administrative d'appel de Versailles, qui n'a pas censuré ces jugements pour irrégularité, a commis une erreur de droit en ne s'estimant pas tenue par l'effet dévolutif de l'appel et en décidant de statuer par la voie de l'évocation ; que, toutefois, cette erreur n'a pu avoir en l'espèce aucune incidence sur le bien fondé de son arrêt, dès lors qu'eu égard à l'ensemble des conclusions d'appel principal et incident dont la cour était saisie, le litige se présentait de manière identique dans le cadre de l'effet dévolutif de l'appel ; que, par suite, le moyen tiré de ce que la cour a statué à tort par la voie de l'évocation n'est pas de nature à justifier l'annulation de l'arrêt attaqué ;  <br/>
<br/>
              11. Considérant qu'il résulte de ce qui précède, que le pourvoi de la société Elres doit être rejeté, y compris ses conclusions tenant à l'application de l'article L. 761-1 du code de justice administrative ; qu'il y a lieu, en revanche, de mettre à sa charge une somme de 3 000 euros à verser à la commune de Draveil au titre des frais exposés par elle et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la société Elres est rejeté.<br/>
Article 2 : La société Elres versera la somme de 3 000 euros à la commune de Draveil au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à société Elres et à la commune de Draveil.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-08-01-04 PROCÉDURE. VOIES DE RECOURS. APPEL. EFFET DÉVOLUTIF ET ÉVOCATION. - 1) COUR ADMINISTRATIVE D'APPEL SAISIE À LA FOIS DU JUGEMENT AVANT DIRE DROIT ET DU JUGEMENT SUR LE FOND RENDUS PAR LE TRIBUNAL ADMINISTRATIF DANS LA MÊME AFFAIRE - ABSENCE DE MOYEN D'IRRÉGULARITÉ RETENU - CONSÉQUENCE - COUR TENUE PAR L'EFFET DÉVOLUTIF DE L'APPEL - 2) INOPÉRANCE EN L'ESPÈCE DU MOYEN DE CASSATION TIRÉ DE L'ERREUR DE DROIT DE LA COUR À AVOIR ÉVOQUÉ, DÈS LORS QUE CETTE ERREUR N'A PU AVOIR AUCUNE INCIDENCE SUR LE JUGEMENT DU LITIGE.
</SCT>
<ANA ID="9A"> 54-08-01-04 1) Si une cour administrative d'appel, saisie à la fois du jugement avant dire droit et du jugement sur le fond rendus par le tribunal administratif dans la même affaire, ne fonde pas son arrêt sur l'irrégularité de ces jugements, elle est tenue par l'effet dévolutif de l'appel.,,,2) Toutefois, en l'espèce, si la cour a évoqué, cette erreur n'a pu avoir aucune incidence sur le jugement de ce litige, qui se présentait de manière identique selon que la cour statue dans le cadre de l'effet dévolutif de l'appel ou, comme elle l'a fait, qu'elle se prononce par la voie de l'évocation. Par suite, et dans les circonstances particulières de l'espèce, inopérance des moyens tirés de l'erreur de droit relatifs à cette évocation.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
