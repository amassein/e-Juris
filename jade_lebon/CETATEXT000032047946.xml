<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032047946</ID>
<ANCIEN_ID>JG_L_2016_02_000000382074</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/04/79/CETATEXT000032047946.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème / 8ème SSR, 12/02/2016, 382074</TITRE>
<DATE_DEC>2016-02-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>382074</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème / 8ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS ; SCP ODENT, POULET</AVOCATS>
<RAPPORTEUR>M. Pierre Lombard</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2016:382074.20160212</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante:<br/>
<br/>
              L'hôpital départemental de Felleries-Liessies a demandé au tribunal administratif de Bordeaux d'annuler huit décisions du 7 octobre 2010 du directeur général de la Caisse nationale de retraite des agents des collectivités locales (CNRACL) lui réclamant le versement de sommes correspondant aux contributions au titre de la validation de services pour la prise en compte des droits à pension de huit de ses agents. <br/>
<br/>
              Par un jugement n° 1004430 du 7 novembre 2012, le tribunal administratif de Bordeaux a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 12BX03243 du 6 mai 2014, la cour administrative d'appel de Bordeaux a annulé le jugement du tribunal administratif de Bordeaux en tant qu'il a rejeté la demande d'annulation de ces décisions réclamant à l'hôpital départemental de Felleries-Liessies le versement de contributions au titre de la validation de services accordée à six de ses agents, et rejeté le surplus des conclusions de cet établissement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés le 2 juillet et 17 septembre 2014 au secrétariat du contentieux du Conseil d'Etat, la Caisse des dépôts et consignations demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt en tant qu'il lui est défavorable ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter les conclusions d'appel de l'hôpital départemental de Felleries-Liessies ; <br/>
<br/>
              3°) de mettre à la charge de l'hôpital départemental de Felleries-Liessies la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code des pensions civiles et militaires de retraite ; <br/>
              - le code de la sécurité sociale ; <br/>
              - la loi n°83-634 du 13 juillet 1983 ; <br/>
              - la loi n°86-33 du 9 janvier 1986 ; <br/>
              - le décret n°2003-1306 du 26 décembre 2003 ; <br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Pierre Lombard, auditeur, <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Odent, Poulet, avocat de la Caisse des dépôts et consignations et à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de l'hôpital départemental de Felleries-Liessies ;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la Caisse nationale de retraite des agents des collectivités territoriales (CNRACL), après avoir fait droit aux demandes de validation des périodes d'études présentées par sept infirmières et un infirmier de l'hôpital départemental de Felleries-Liessies, a adressé à cet établissement huit factures entre le 8 novembre 2007 et le 24 novembre 2009 pour des sommes correspondant aux contributions de l'établissement au titre de la validation de services pour la prise en compte des droits à pension de huit de ses agents, puis le 7 octobre 2010, des demandes en vue du règlement de ces sommes ; que l'hôpital a saisi le tribunal administratif de Bordeaux d'une demande d'annulation de ces décisions ; que la Caisse des dépôts et des consignations se pourvoit en cassation contre l'arrêt du 6 mai 2014 par lequel la cour administrative d'appel de Bordeaux a annulé le jugement du 7 novembre 2012 du tribunal administratif de Bordeaux, en tant qu'il a rejeté la demande d'annulation des décisions de la CNRACL du 7 octobre 2010 relatives à six de ces agents, ainsi que ces décisions ; <br/>
<br/>
              2. Considérant, en premier lieu, qu'en fondant sa décision notamment sur l'article 12 du décret du 26 décembre 2003 relatif au régime de retraite des fonctionnaires affiliés à la Caisse nationale de retraites des agents des collectivités locales la cour n'a pas soulevé d'office un moyen mais examiné les conditions dans lesquelles les périodes d'études des infirmiers peuvent être prises en compte dans la constitution du droit à pension ; que la Caisse des dépôts et des consignations n'est dès lors pas fondée à soutenir que la cour aurait méconnu l'article R. 611-7 du code de justice administrative en omettant de communiquer aux parties un moyen relevé d'office ;<br/>
<br/>
              3. Considérant, en deuxième lieu, qu'en jugeant que les décisions attaquées, qui demandaient à l'hôpital de régler, dans les meilleurs délais, des contributions au titre des services accomplis par des infirmiers stagiaires au sein de l'établissement, n'étaient pas confirmatives des propositions de décisions validant les services des intéressés, notifiées antérieurement à l'hôpital, et présentaient le caractère d'actes faisant grief et ainsi susceptibles de recours, la cour, qui a suffisamment motivé son arrêt, n'a pas inexactement qualifié les pièces du dossier dont elle était saisie ni commis une erreur de droit ; <br/>
<br/>
              4. Considérant, en troisième lieu, que l'article 8 du décret du 26 décembre 2003 relatif au régime de retraite des fonctionnaires affiliés à la Caisse nationale de retraites des agents des collectivités locales dispose : " Les services pris en compte dans la constitution du droit à pension sont : (...) / 2° Les périodes de services dûment validées. Est admise à validation toute période de services, quelle qu'en soit la durée, effectués en qualité d'agent non titulaire de l'une des collectivités mentionnées aux 1°, 2° et 3° de l'article L. 86-1 du code des pensions civiles et militaires de retraite (...) " ; que l'article 12 de ce décret prévoit également que les périodes d'études accomplies dans les établissements d'enseignement supérieur, les écoles techniques supérieures, les grandes écoles et classes du second degré préparatoires à ces écoles peuvent faire l'objet, suite à la demande d'un fonctionnaire et sur présentation de la copie de son diplôme, d'une proposition de rachat, de la part de la CNRACL, pour leur prise en compte dans le calcul de sa pension ;<br/>
<br/>
              5. Considérant qu'il résulte de ces dispositions du décret du 26 décembre 2003 que si en vertu de son article 8, peuvent être validées et par suite prises en compte dans la constitution du droit à pension les périodes de services effectués en qualité d'agent non titulaire de l'une des collectivités mentionnées aux 1°, 2° et 3° de l'article L. 86-1 du code des pensions civiles et militaires de retraite, la prise en compte des périodes d'études, au titre de la constitution et de la liquidation des droits à pension, relève du dispositif spécifique prévu à l'article 12 du même décret qui prévoit qu'elles peuvent faire l'objet, de la part de la CNRACL, d'une proposition de rachat ; qu'en revanche, il ne résulte pas de ces dispositions que les périodes d'études mentionnées au point 4 doivent être regardées comme des périodes de services effectués en qualité d'agent non titulaire de l'une des collectivités mentionnées aux 1°, 2° et 3° de l'article L. 86-1 du code des pensions civiles et militaires de retraite ; que par suite le conseil d'administration de la CNRACL, qui ne tire d'aucun texte ni aucun principe compétence pour déroger aux dispositions du décret du 26 décembre 2003 fixant les conditions de validation des services effectués par des agents en tant que non titulaires, n'était pas compétent pour décider que les périodes consacrées aux années d'études d'infirmiers pouvaient être regardées, sous certaines conditions, comme des périodes de services effectués en qualité d'agent non titulaire et susceptibles d'être validées en application de l'article 8 du décret du 26 décembre 2003 ; que, dès lors, en jugeant que la délibération du 31 mars 2004 du conseil d'administration de la CNRACL n'a pas pu donner de fondement légal aux décisions contestées par l'hôpital départemental de Felleries-Liessies, la cour n'a pas commis d'erreur de droit ; <br/>
<br/>
              6. Considérant, en quatrième lieu, que contrairement à ce que soutient la requérante, la cour, en jugeant que la date de notification des factures litigieuses, autres que celles de Mesdames Jumiaux et Cetra, n'était pas établie et qu'il ne ressortait pas des pièces du dossier que les notifications adressées à l'hôpital aient mentionné les voies et délais de recours, n'a pas dénaturé les pièces du dossier ; <br/>
<br/>
              7. Considérant, enfin, qu'il résulte de ce qui précède que la cour a ni commis d'erreur de droit ni entaché son arrêt de contradiction de motifs en jugeant qu'alors même l'hôpital n'avait pas contesté les décisions de validation il n'était pas tenu de payer les contributions faisant l'objet des factures litigieuses ; <br/>
<br/>
              8. Considérant qu'il résulte de tout ce qui précède que la Caisse des dépôts et consignations n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque ; <br/>
<br/>
              9. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de l'hôpital de Felleries-Liessies qui n'est pas, dans la présente instance, la partie perdante ; qu'il y a lieu, en revanche, dans les circonstances de l'espèce, de mettre à la charge de la Caisse des dépôts et consignations la somme de 3 000 euros au titres des dispositions de l'article L. 761-1 du même code ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la Caisse des dépôts et consignations est rejeté.<br/>
Article 2 : La Caisse des dépôts et consignations versera à l'hôpital de Felleries-Liessies la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la Caisse des dépôts et consignations et à l'hôpital de Felleries-Liessies. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">48-02-02-02 PENSIONS. PENSIONS CIVILES ET MILITAIRES DE RETRAITE. PENSIONS CIVILES. CONDITIONS D'OUVERTURE DU DROIT À PENSION. - CONSTITUTION DU DROIT À PENSION DES FONCTIONNAIRES AFFILIÉS À LA CNRACL (ART. 8 DU DÉCRET DU 26 DÉCEMBRE 2003) - PÉRIODES PRISES EN COMPTE - PÉRIODES DE SERVICES EFFECTUÉS EN QUALITÉ D'AGENT NON TITULAIRE - EXISTENCE - PÉRIODES D'ÉTUDES - ABSENCE.
</SCT>
<ANA ID="9A"> 48-02-02-02 En vertu de l'article 8 du décret n° 2003-1306 du 26 décembre 2003, peuvent être validées et par suite prises en compte dans la constitution du droit à pension des fonctionnaires affiliés à la caisse nationale de retraites des agents des collectivités locales (CNARCL) les périodes de services effectués en qualité d'agent non titulaire de l'une des collectivités mentionnées aux 1°, 2° et 3° de l'article L. 86-1 du code des pensions civiles et militaires de retraite. En revanche, la prise en compte des périodes d'études, au titre de la constitution et de la liquidation des droits à pension, relève du dispositif spécifique prévu à l'article 12 du même décret qui prévoit qu'elles peuvent faire l'objet, de la part de la CNRACL, d'une proposition de rachat. Les périodes d'études ne peuvent donc être regardées comme des périodes de services effectués en qualité d'agent non titulaire de l'une des collectivités mentionnées aux 1°, 2° et 3° de l'article L. 86-1 du code des pensions civiles et militaires de retraite.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
