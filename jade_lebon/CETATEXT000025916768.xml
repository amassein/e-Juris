<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025916768</ID>
<ANCIEN_ID>JG_L_2012_05_000000349480</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/91/67/CETATEXT000025916768.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème et 7ème sous-sections réunies, 22/05/2012, 349480</TITRE>
<DATE_DEC>2012-05-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>349480</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème et 7ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Aymeric Pontvianne</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Béatrice Bourgeois-Machureau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:349480.20120522</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 20 mai 2011 au secrétariat du contentieux du Conseil d'Etat, présenté par M. A... B..., ayant élu domicile chez..., ; M.  B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir, d'une part, le décret du 19 mars 2009 en tant qu'il retire à ses enfants mineurs Ismail, Rime et Sarah la nationalité française qui leur avait été accordée par l'effet collectif attaché au décret du 4 juin 2008 procédant à la naturalisation de Mme D...C..., d'autre part, la décision du 19 août 2010 de l'ambassadeur de France au Ghana retirant les cartes nationales d'identité et les passeports de ces enfants, enfin, la décision du 22 mars 2011 par laquelle le ministre de l'intérieur, de l'outre-mer, des collectivités territoriales et de l'immigration a rejeté son recours gracieux ; <br/>
<br/>
              2°) d'enjoindre à l'administration à titre principal, d'une part, de rapporter le décret du 19 mars 2009 en tant qu'il retire à ses enfants le bénéfice de la nationalité française, d'autre part, de leur restituer leurs passeports et cartes nationales d'identités dans un délai d'un mois à compter de la décision à intervenir, ou à titre subsidiaire, de réexaminer le dossier dans un délai d'un mois à compter de la décision à intervenir ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement d'une somme de 3 000 euros sur le fondement des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu la convention internationale relative aux droits de l'enfant ;<br/>
<br/>
              Vu le code civil ;<br/>
<br/>
              Vu le décret n° 93-1362 du 30 décembre 1993 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Aymeric Pontvianne, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Béatrice Bourgeois-Machureau, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant que, par décret du 4 juin 2008, publié au Journal officiel de la République française le 5 juin, Mme C...a été naturalisée française ; que ses enfants mineurs Ismail, Rime et Sarah B...ont acquis la nationalité française en raison de l'effet collectif attaché à cette naturalisation par l'article 22-1 du code civil ; qu'en raison toutefois du décès de Mme C..., survenu le 1er mars 2008 avant la signature du décret prononçant sa naturalisation, un nouveau décret du 19 mars 2009, publié au Journal officiel de la République française le 21 mars, a rapporté la naturalisation de Mme C... ainsi que l'effet collectif attaché à cette acquisition de la nationalité française dont ont bénéficié ses enfants ; qu'à la suite de l'intervention de ce dernier décret, les passeports et cartes nationales d'identité ont été retirés aux trois enfants par décision de l'ambassadeur de France au Ghana le 19 août 2010 ; que, par décision du 22 mars 2011, le ministre de l'intérieur, de l'outre-mer, des collectivités territoriales et de l'immigration a rejeté le recours gracieux formé par M.B..., père et représentant légal des trois enfants ; <br/>
<br/>
              Considérant qu'en vertu de l'article 21-15 du code civil, l'acquisition de la nationalité française par décision de l'autorité publique résulte d'une naturalisation accordée par décret à la demande de l'étranger, qui est subordonnée, selon l'article 21-16 du même code, à la condition que l'étranger réside en France au moment de la signature du décret de naturalisation ; que selon l'article 22-1 du même code, l'enfant mineur dont l'un des deux parents acquiert la nationalité française, devient français de plein droit s'il a la même résidence habituelle que ce parent ou s'il réside alternativement avec ce parent dans le cas de séparation ou divorce ; <br/>
<br/>
              Considérant qu'en l'absence de dispositions législatives contraires, le décès de l'étranger qui a demandé à devenir français fait obstacle à sa naturalisation et, en conséquence, à ce que son enfant mineur puisse devenir français par l'effet collectif, résultant de l'article 22-1 du code civil, attaché à cette naturalisation ; qu'il s'ensuit qu'un décret qui procède à la naturalisation d'un étranger décédé ne saurait conférer aucun droit et peut être rapporté à toute époque, sous le contrôle du juge de l'excès de pouvoir, indépendamment des conditions de délai, de forme et de procédure prévues, pour le retrait de décrets de naturalisation lorsque le demandeur ne satisfait pas aux conditions légales, par l'article 27-2 du code civil et l'article 62 du décret du 30 décembre 1993 relatif aux déclarations de nationalité, aux décisions de naturalisation, de réintégration, de perte, de déchéance et de retrait de la nationalité française ; <br/>
<br/>
              Considérant qu'il est constant que Mme C...est décédée avant la signature du décret prononçant sa naturalisation ; que ce décret n'a pu, dès lors, conférer aucun droit, ni à son profit, ni à celui de ses enfants, et pouvait être légalement rapporté, par le décret attaqué, sans que soient observées les règles de motivation et de procédure établies par les dispositions de l'article 27-2 du code civil et de l'article 59, auquel renvoie l'article 62, du décret du 30 décembre 1993 ;<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier que les enfants Ismail, Rime et Sarah B...n'ont été mis en possession de cartes nationales d'identité et de passeports français que pendant un peu plus d'un an ; qu'ils ne peuvent, par suite, se prévaloir d'une possession d'état de Français d'une durée suffisante pour faire échec à l'intervention du décret attaqué ; que les circonstances que les enfants soient, pour deux d'entre eux, nés en France, qu'ils aient vécu sur le territoire français et qu'ils ne parlent que le français sont sans incidence sur la légalité du décret attaqué ;<br/>
<br/>
              Considérant qu'une décision de retrait de la nationalité française est, par elle-même, dépourvue d'effet sur la présence sur le territoire français ou sur les liens de la personne concernée avec les membres de sa famille ; qu'ainsi les stipulations de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ne peuvent être utilement invoquées à l'appui des conclusions dirigées contre le décret attaqué, non plus que celles de l'article 3-1 de la convention internationale relative aux droits de l'enfant ; <br/>
<br/>
              Considérant qu'il résulte de ce qui précède, sans qu'il soit besoin de se prononcer sur la fin de non-recevoir opposée par le ministre de l'intérieur, de l'outre-mer, des collectivités territoriales et de l'immigration, que M.  B...n'est pas fondé à demander l'annulation du décret du 19 mars 2009 en tant qu'il retire la nationalité française aux enfants Ismail, Rime et SarahB... ; <br/>
<br/>
              Considérant que ne peuvent, en conséquence, qu'être rejetées les conclusions de M.  B...dirigées contre la décision de l'ambassadeur de France au Ghana du 19 août 2010 retirant les passeports français et cartes nationales d'identité, dont l'annulation n'est demandée que par voie de conséquence de l'illégalité du décret du 19 mars 2009 ; que doivent, de même, être rejetées les conclusions dirigées contre la décision du ministre chargé de l'immigration en date du 22 mars 2011 rejetant le recours gracieux formé par M. B..., sans que le moyen tiré du vice propre dont cette dernière décision serait entachée puisse être utilement invoqué ;<br/>
<br/>
              Considérant, enfin, que les conclusions à fin d'injonction ainsi que celles présentées par M.  B...sur le fondement de l'article L. 761-1 du code de justice administrative ne peuvent, en conséquence, qu'être rejetées ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er  : La requête de M. B...est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. A... B...et au ministre de l'intérieur.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-01-06-02-02 ACTES LÉGISLATIFS ET ADMINISTRATIFS. DIFFÉRENTES CATÉGORIES D'ACTES. ACTES ADMINISTRATIFS - CLASSIFICATION. ACTES INDIVIDUELS OU COLLECTIFS. ACTES NON CRÉATEURS DE DROITS. - DÉCRET NATURALISANT UN ÉTRANGER DÉCÉDÉ.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">01-09-01-01 ACTES LÉGISLATIFS ET ADMINISTRATIFS. DISPARITION DE L'ACTE. RETRAIT. RETRAIT DES ACTES NON CRÉATEURS DE DROITS. - RETRAIT D'UN DÉCRET NATURALISANT UN ÉTRANGER DÉCÉDÉ - APPLICABILITÉ DES CONDITIONS DE DÉLAI, DE FORME ET DE PROCÉDURE PRÉVUES POUR LE RETRAIT DES DÉCRETS DE NATURALISATION - ABSENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">26-01-01-01-03 DROITS CIVILS ET INDIVIDUELS. ÉTAT DES PERSONNES. NATIONALITÉ. ACQUISITION DE LA NATIONALITÉ. NATURALISATION. - ETRANGER DÉCÉDÉ AVANT L'ÉDICTION DU DÉCRET DE NATURALISATION - PORTÉE DU DÉCRET - ABSENCE DE DROITS CONFÉRÉS À L'ÉTRANGER ET À SES ENFANTS MINEURS - CONSÉQUENCE - POSSIBILITÉ DE RETRAIT SANS DÉLAI NI PROCÉDURE.
</SCT>
<ANA ID="9A"> 01-01-06-02-02 En l'absence de dispositions législatives contraires, le décès de l'étranger qui a demandé à devenir français fait obstacle à sa naturalisation et, en conséquence, à ce que son enfant mineur puisse devenir français par l'effet collectif attaché à cette naturalisation. Un décret qui procède à la naturalisation d'un étranger décédé ne saurait donc conférer aucun droit et peut être rapporté à toute époque, sous le contrôle du juge de l'excès de pouvoir, indépendamment des conditions de délai, de forme et de procédure prévues pour le retrait des décrets de naturalisation par les articles 27-2 du code civil et 62 du décret n° 93-1362 du 30 décembre 1993.</ANA>
<ANA ID="9B"> 01-09-01-01 En l'absence de dispositions législatives contraires, le décès de l'étranger qui a demandé à devenir français fait obstacle à sa naturalisation et, en conséquence, à ce que son enfant mineur puisse devenir français par l'effet collectif attaché à cette naturalisation. Un décret qui procède à la naturalisation d'un étranger décédé ne saurait donc conférer aucun droit et peut être rapporté à toute époque, sous le contrôle du juge de l'excès de pouvoir, indépendamment des conditions de délai, de forme et de procédure prévues pour le retrait des décrets de naturalisation par les articles 27-2 du code civil et 62 du décret n° 93-1362 du 30 décembre 1993.</ANA>
<ANA ID="9C"> 26-01-01-01-03 En l'absence de dispositions législatives contraires, le décès de l'étranger qui a demandé à devenir français fait obstacle à sa naturalisation et, en conséquence, à ce que son enfant mineur puisse devenir français par l'effet collectif attaché à cette naturalisation. Un décret qui procède à la naturalisation d'un étranger décédé ne saurait donc conférer aucun droit et peut être rapporté à toute époque, sous le contrôle du juge de l'excès de pouvoir, indépendamment des conditions de délai, de forme et de procédure prévues pour le retrait des décrets de naturalisation par les articles 27-2 du code civil et 62 du décret n° 93-1362 du 30 décembre 1993.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
