<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042175704</ID>
<ANCIEN_ID>JG_L_2020_07_000000432969</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/17/57/CETATEXT000042175704.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 29/07/2020, 432969</TITRE>
<DATE_DEC>2020-07-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>432969</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ ; SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Bertrand Mathieu</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Guillaume Odinet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:432969.20200729</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              La société Air Horizont Limited a demandé au tribunal administratif de Paris d'annuler la décision du 19 décembre 2017 par laquelle l'Autorité de contrôle des nuisances sonores aéroportuaires (ACNUSA) lui a infligé une amende administrative d'un montant de 24 000 euros.<br/>
<br/>
              Par un jugement n° 1802574 du 10 juillet 2018, le tribunal administratif de Paris a annulé cette décision.<br/>
<br/>
              Par un arrêt n° 18PA03025 du 12 juillet 2019, la cour administrative d'appel de Paris a rejeté l'appel de l'ACNUSA contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et des mémoire complémentaires, enregistrés les et 25 juillet et 28 octobre 2019 et les 6 mai et 7 juillet 2020 au secrétariat du contentieux du Conseil d'Etat, l'ACNUSA demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
              3°) de mettre à la charge de la société Air Horizont Limited la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution ;<br/>
              - la décision du Conseil constitutionnel n°2017-675 QPC du 24 novembre 2017 ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code de l'aviation civile ;<br/>
              - le code des transports ;<br/>
              - la loi n° 2017-55 du 20 janvier 2017 ;<br/>
              - l'arrêté ministériel du 20 septembre 2011 portant restriction d'exploitation de l'aérodrome de Paris Charles de Gaulle ;<br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bertrand Mathieu, conseiller d'Etat en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Guillaume Odinet, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de l'Autorité de contrôle des nuisances sonores aéroportuaires, et à la SCP Piwnica, Molinié, avocat de la société Air Horizont Limited ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. D'une part, aux termes de l'article L. 6361-14 du code des transports, dans sa rédaction maintenue en vigueur du fait du report au 30 juin 2018 de l'abrogation de ses dispositions déclarées inconstitutionnelles par la décision n° 2017-675 QPC du 24 novembre 2017 du Conseil constitutionnel : " Les fonctionnaires et agents mentionnés à l'article L. 6142-1 constatent les manquements aux mesures définies par l'article L. 6361-12. Ces manquements font l'objet de procès-verbaux qui, ainsi que le montant de l'amende encourue, sont notifiés à la personne concernée et communiqués à l'autorité. / A l'issue de l'instruction, le président de l'autorité peut classer sans suite la procédure dès lors que les circonstances particulières à la commission des faits le justifient ou que ceux-ci ne sont pas constitutifs d'un manquement pouvant donner lieu à sanction. / (...) Un rapporteur permanent et son suppléant sont placés auprès de l'autorité. / Au terme de l'instruction, le rapporteur notifie le dossier complet d'instruction à la personne concernée. Celle-ci peut présenter ses observations au rapporteur. / L'autorité met la personne concernée en mesure de se présenter devant elle ou de se faire représenter. Elle délibère valablement au cas où la personne concernée néglige de comparaître ou de se faire représenter. / Après avoir entendu le rapporteur et, le cas échéant, la personne concernée ou son représentant, l'autorité délibère hors de leur présence (...) ". <br/>
<br/>
              2. D'autre part, l'article R. 227-2 du code de l'aviation civile, dans sa rédaction applicable au litige, dispose que : " Le rapporteur permanent clôt l'instruction menée par les fonctionnaires et agents mentionnés à l'article R. 227-1. Il communique le dossier d'instruction à la personne concernée en lui précisant les faits reprochés, leur qualification, les textes applicables à ces faits et l'amende encourue et en l'invitant à présenter ses observations dans un délai d'un mois. / A réception de ces observations ou, à défaut, à l'issue de ce délai, le rapporteur permanent communique le dossier au président de l'autorité. Ce dernier fait convoquer la personne concernée au minimum un mois avant la séance au cours de laquelle l'affaire doit être examinée en lui communiquant le dossier complet de l'instruction qui comporte une notification des griefs retenus, les textes qui les fondent et le montant de l'amende encourue et en lui indiquant qu'elle peut se présenter ou se faire représenter à la séance. / Dans les cas prévus au deuxième alinéa de l'article L. 227-4 [deuxième alinéa de l'article L. 6361-14 du code des transports], le président de l'autorité peut prononcer le classement sans suite de la procédure. Le rapporteur permanent notifie cette décision à la personne concernée. ".<br/>
<br/>
              3. Il ressort des pièces soumis aux juges du fond qu'à la suite du procès-verbal dressé le 25 octobre 2016 par des agents de la direction générale de l'aviation civile pour manquement aux règles de restriction d'exploitation de l'aéroport de Paris-Charles de Gaulle, l'Autorité de contrôle des nuisances aéroportuaires (ACNUSA) a prononcé, par décision du 19 décembre 2017, à l'encontre de la société Air Horizont Limited une amende administrative de 24 000 euros. Par un jugement du 10 juillet 2018, le tribunal administratif de Paris a annulé cette sanction pour méconnaissance du principe d'impartialité garanti par l'article 6, paragraphe 1, de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales. L'ACNUSA se pourvoit en cassation contre l'arrêt du 12 juillet 2019 par lequel la cour administrative d'appel de Paris a rejeté son appel contre ce jugement.<br/>
<br/>
              4. Pour juger que la société Air Horizont Limited avait pu raisonnablement avoir l'impression, compte tenu de l'enchaînement des actes pris au cours de la procédure devant l'ACNUSA, d'être poursuivie et jugée par la même personne et en déduire que la décision de sanction était intervenue en méconnaissance du principe d'impartialité, la cour a relevé que le président de l'ACNUSA avait " fait convoquer " la société Air Horizont Limited à la séance du 5 septembre 2017 lors de laquelle a été examiné son dossier et avait siégé à cette séance puis participé au délibéré. Toutefois, il ressortait des pièces du dossier qui lui était soumis qu'ainsi qu'elle l'a souligné, la lettre de convocation datée du 13 juillet 2017 n'avait pas été signée par le président de l'ACNUSA. Celui-ci ayant renoncé à ses prérogatives en matière de convocation de la personne mise en cause, cette lettre avait été signée par le rapporteur permanent agissant " au nom de l'autorité " et exerçant ses compétences " en pleine indépendance " en vertu de l'article 3 de la délibération du 28 avril 2010 portant règlement intérieur de l'Autorité de contrôle des nuisances sonores aéroportuaires. Dans ces conditions, en jugeant que les conditions de convocation de la société Air Horizont Limited caractérisaient une méconnaissance du principe d'impartialité garanti par les stipulations de l'article 6, paragraphe 1, de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, la cour a commis une erreur de droit. <br/>
<br/>
              5. Par suite, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, l'ACNUSA est fondée à demander l'annulation de l'arrêt qu'elle attaque.<br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              Sur la fin de non-recevoir opposée par la société Air Horizont Limited :<br/>
<br/>
              7. Aux termes du premier alinéa de l'article L. 6361-1 du code des transports et comme le mentionne l'annexe à la loi du 20 janvier 2017 portant statut général des autorités administratives indépendantes et des autorités publiques indépendantes, l'ACNUSA est une autorité administrative indépendante. En cette qualité et alors même qu'elle ne dispose pas de la personnalité morale, elle peut agir devant les juridictions administratives, en demande comme en défense, en particulier dans les litiges relatifs aux décisions qu'elle prend, sans qu'y fassent obstacle les dispositions du code de justice administrative relatives à la représentation de l'Etat devant ces juridictions. Par suite, l'ACNUSA a qualité pour faire appel du jugement du tribunal administratif ayant annulé la décision de sanction qu'elle a prononcée à l'encontre de la société Air Horizont Limited.<br/>
<br/>
              Sur le règlement du litige :<br/>
<br/>
              8. D'une part, si par la décision n° 2017-675 QPC du 24 novembre 2017, le Conseil constitutionnel a déclaré contraires à l'article 16 de la Déclaration des droits de l'homme et du citoyen les dispositions précitées de l'article L. 6361-14 du code des transports il a différé jusqu'au 30 juin 2018 les effets de la déclaration d'inconstitutionnalité qu'il prononçait. Toutefois, l'autorité qui s'attache à cette décision en vertu de l'article 62 de la Constitution ne fait pas obstacle à ce que le juge du litige examine un moyen tiré de l'incompatibilité des dispositions législatives ainsi maintenues en vigueur avec une stipulation conventionnelle ou contestant, au regard d'une telle stipulation, les conditions dans lesquelles elles ont été appliquées.<br/>
<br/>
              9. D'autre part, lorsqu'elle est saisie d'agissements pouvant donner lieu aux sanctions prévues par le code des transports, l'ACNUSA doit être regardée comme décidant du bien-fondé d'accusations en matière pénale au sens des stipulations de l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales. Eu égard à la nature, à la composition et aux attributions de cette autorité, il appartient au juge du litige d'examiner si celle-ci, alors même qu'elle n'est pas une juridiction au regard du droit interne, a statué dans des conditions qui ne respecteraient pas le principe d'impartialité rappelé par cet article.<br/>
<br/>
              10. Il résulte des dispositions précitées de l'article L. 6361-14 du code des transports, dans leur version applicable au présent litige, qu'à l'issue de l'instruction, le président de l'ACNUSA a le pouvoir de classer sans suite une procédure de sanction engagée à l'encontre d'une personne ayant fait l'objet d'un constat de manquement aux mesures définies par l'article L. 6361-12 du même code s'il estime que les faits ne sont pas constitutifs d'un manquement pouvant donner lieu à sanction ou que les circonstances particulières à la commission des faits justifient un tel classement. Il résulte de l'instruction que dans la procédure concernant la société Air Horizont Limited, il a fait usage de son pouvoir de poursuite des manquements constatés. Dans ces conditions, la participation du président de l'ACNUSA aux débats et au vote à l'issue desquels a été infligée une sanction à cette société alors qu'il devait être regardé comme ayant refusé de procéder au classement sans suite du dossier, a méconnu les exigences attachées au principe d'impartialité rappelées par l'article 6, paragraphe 1, de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales.<br/>
<br/>
              11. Dès lors, l'ACNUSA n'est pas fondée à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Paris a jugé que la décision du 19 décembre 2017 avait été adoptée par une formation composée en méconnaissance du principe d'impartialité et devait, en conséquence, être annulée.<br/>
<br/>
              12. Toutefois, cette méconnaissance du principe d'impartialité n'entache pas d'irrégularité l'ensemble de la procédure suivie devant l'ACNUSA et n'affecte ni l'engagement de cette procédure, ni l'instruction et les poursuites. Dans les circonstances de l'espèce, et alors que la régularité de la procédure ayant précédé le prononcé de la sanction ne fait l'objet d'aucune contestation, il y a lieu pour le juge administratif, eu égard à son office de plein contentieux, de statuer sur les poursuites en prenant une décision qui se substitue à celle qui avait été prise.<br/>
<br/>
              13. D'une part, aux termes de l'article L. 6361-12 du code des transports : " L'Autorité de contrôle des nuisances aéroportuaires prononce une amende administrative à l'encontre :/ 1° De la personne exerçant une activité de transport aérien public au sens de l'article L. 6412-1 ; / 2° De la personne au profit de laquelle est exercée une activité de transport aérien au sens de l'article L. 6400-1 ;/ 3° De la personne exerçant une activité aérienne, rémunérée ou non, autre que celles mentionnées aux 1° et 2° du présent article ;/ 4° Du fréteur dans le cas défini par l'article L. 6400-2,/ ne respectant pas les mesures prises par l'autorité administrative sur un aérodrome fixant:/ a) Des restrictions permanentes ou temporaires d'usage de certains types d'aéronefs en fonction de leurs émissions atmosphériques polluantes, de la classification acoustique, de leur capacité en sièges ou de leur masse maximale certifiée au décollage ; / b) Des restrictions permanentes ou temporaires apportées à l'exercice de certaines activités en raison des nuisances environnementales qu'elles occasionnent ;/ c) Des procédures particulières de décollage ou d'atterrissage en vue de limiter les nuisances environnementales engendrées par ces phases de vol ;/ d) Des règles relatives aux essais moteurs ;/ e) Des valeurs maximales de bruit ou d'émissions atmosphériques polluantes à ne pas dépasser ". Aux termes de l'article L. 6400-2 du même code : " L'affrètement d'un aéronef est l'opération par laquelle un fréteur met à la disposition d'un affréteur un aéronef avec équipage (...) ".<br/>
<br/>
              14. Il résulte de ces dispositions que, contrairement à ce que soutient la société Air Horizont Limited, les amendes administratives encourues en cas de manquement aux mesures énoncées au titre de la prévention des nuisances aéroportuaires peuvent être infligées par l'ACNUSA à l'une ou l'autre des personnes visées aux 1° à 4° de l'article L. 6361-12, dont l'affréteur en cas de manquement imputable à un aéronef faisant l'objet d'un affrètement.<br/>
<br/>
              15. D'autre part, aux termes de l'article L. 6361-13 du code des transports, dans sa version alors en vigueur : " Les amendes administratives mentionnées à l'article L. 6361-12 ne peuvent excéder, par manquement constaté, un montant de 1 500 &#128; pour une personne physique et de 20 000 &#128; pour une personne morale. S'agissant des personnes morales, ce montant maximal est porté à 40 000 &#128; lorsque le manquement concerne :/ 1° Les restrictions permanentes ou temporaires d'usage de certains types d'aéronefs en fonction de leurs émissions atmosphériques polluantes ou de la classification acoustique ;/ 2° Les mesures de restriction des vols de nuit (...) ". <br/>
<br/>
              16. Il résulte de l'instruction, notamment du procès-verbal dressé le 25 octobre 2016 par des agents de la direction générale de l'aviation civile, et n'est pas contesté, qu'un aéronef opéré par la société Air Horizont Limited a atterri, le 10 septembre 2016 à 01h21 sur l'aéroport de Paris Charles de Gaulle, dans des conditions méconnaissant les dispositions de l'arrêté ministériel du 20 septembre 2011 portant restriction d'exploitation de cet aérodrome dont le V de l'article 1er V interdit aux appareils de marge cumulée inférieure à 10EPNdB la possibilité " [...] d'atterrir entre 22 heures et 6 heures, heures locales ". Eu égard à l'importance du dépassement constaté, à l'existence de précédents manquements de la société mise en cause et à l'absence d'éléments avancés par celle-ci quant à sa situation, il y a lieu de prononcer à l'encontre de cette société une amende d'un montant de 24 000 euros. <br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative : <br/>
<br/>
              17. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de la société Air Horizont Limited le versement d'une somme au titre des frais engagés devant le tribunal administratif, la cour administrative d'appel et le Conseil d'Etat. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de l'Etat, qui n'est pas la partie perdante, les sommes que demande la société Air Horizont Limited à ce titre.<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Paris du 12 juillet 2019 est annulé.<br/>
Article 2 : Une amende administrative d'un montant de 24 000 euros est prononcée à l'encontre de la société Air Horizont Limited. <br/>
Article 3 : Le jugement du tribunal administratif de Paris du 10 juillet 2018 est réformé en ce qu'il a de contraire à la présente décision.<br/>
Article 4 : Les conclusions présentées par l'ACNUSA et par la société Air Horizont Limited au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 5 : La présente décision sera notifiée à l'Autorité de contrôle des nuisances aéroportuaires et à la société Air Horizont Limited. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-04-03 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. PRINCIPES GÉNÉRAUX DU DROIT. - PRINCIPE D'IMPARTIALITÉ - AUTORITÉ DE CONTRÔLE DES NUISANCES SONORES AÉROPORTUAIRES (ACNUSA) - PROCÉDURE DE SANCTION - 1) PRÉSIDENT DE L'ACNUSA AYANT ENGAGÉ DES POURSUITES PUIS PARTICIPÉ À L'ADOPTION DE LA DÉCISION DE SANCTION - MÉCONNAISSANCE DU PRINCIPE D'IMPARTIALITÉ [RJ2] N'ENTACHANT PAS L'ENSEMBLE DE LA PROCÉDURE - 2) CONSÉQUENCE - FACULTÉ POUR LE JUGE DE PLEIN CONTENTIEUX DE STATUER SUR LES POURSUITES ET DE PRENDRE UNE DÉCISION DE SANCTION [RJ3].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">44-05-01 NATURE ET ENVIRONNEMENT. DIVERS RÉGIMES PROTECTEURS DE L`ENVIRONNEMENT. LUTTE CONTRE LES NUISANCES SONORES ET LUMINEUSES. - TRANSPORTS AÉRIENS - AÉROPORTS - NUISANCES CAUSÉES AUX RIVERAINS - AUTORITÉ DE CONTRÔLE DES NUISANCES SONORES AÉROPORTUAIRES (ACNUSA) - 1) A) QUALITÉ D'AAI - EXISTENCE - B) CONSÉQUENCE - QUALITÉ POUR AGIR DEVANT LES JURIDICTIONS ADMINISTRATIVES - EXISTENCE [RJ1] - 2) PROCÉDURE DE SANCTION - A) PRÉSIDENT DE L'ACNUSA AYANT ENGAGÉ DES POURSUITES PUIS PARTICIPÉ À L'ADOPTION DE LA DÉCISION DE SANCTION - MÉCONNAISSANCE DU PRINCIPE D'IMPARTIALITÉ [RJ2] N'ENTACHANT PAS L'ENSEMBLE DE LA PROCÉDURE - B) CONSÉQUENCE - FACULTÉ POUR LE JUGE DE PLEIN CONTENTIEUX DE STATUER SUR LES POURSUITES ET DE PRENDRE UNE DÉCISION DE SANCTION [RJ3].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">52-045 POUVOIRS PUBLICS ET AUTORITÉS INDÉPENDANTES. AUTORITÉS ADMINISTRATIVES INDÉPENDANTES. - ACNUSA - 1) A) QUALITÉ D'AAI - EXISTENCE - B) CONSÉQUENCE - QUALITÉ POUR AGIR DEVANT LES JURIDICTIONS ADMINISTRATIVES - EXISTENCE [RJ1] - 2) PROCÉDURE DE SANCTION - A) PRÉSIDENT DE L'ACNUSA AYANT ENGAGÉ DES POURSUITES PUIS PARTICIPÉ À L'ADOPTION DE LA DÉCISION DE SANCTION - MÉCONNAISSANCE DU PRINCIPE D'IMPARTIALITÉ [RJ2] N'ENTACHANT PAS L'ENSEMBLE DE LA PROCÉDURE - B) CONSÉQUENCE - FACULTÉ POUR LE JUGE DE PLEIN CONTENTIEUX DE STATUER SUR LES POURSUITES ET DE PRENDRE UNE DÉCISION DE SANCTION [RJ3].
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">54-01-05 PROCÉDURE. INTRODUCTION DE L'INSTANCE. QUALITÉ POUR AGIR. - AUTORITÉ DE CONTRÔLE DES NUISANCES SONORES AÉROPORTUAIRES (ACNUSA) - 1) QUALITÉ D'AAI - EXISTENCE - 2) CONSÉQUENCE - QUALITÉ POUR AGIR DEVANT LES JURIDICTIONS ADMINISTRATIVES - EXISTENCE [RJ1].
</SCT>
<SCT ID="8E" TYPE="PRINCIPAL">54-07-03 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. POUVOIRS DU JUGE DE PLEIN CONTENTIEUX. - SANCTION ADMINISTRATIVE INFLIGÉE PAR L'AUTORITÉ DE CONTRÔLE DES NUISANCES SONORES AÉROPORTUAIRES (ACNUSA) - 1) PRÉSIDENT DE L'ACNUSA AYANT ENGAGÉ DES POURSUITES PUIS PARTICIPÉ À L'ADOPTION DE LA DÉCISION DE SANCTION - MÉCONNAISSANCE DU PRINCIPE D'IMPARTIALITÉ [RJ2] N'ENTACHANT PAS L'ENSEMBLE DE LA PROCÉDURE - 2) CONSÉQUENCE - FACULTÉ POUR LE JUGE DE PLEIN CONTENTIEUX DE STATUER SUR LES POURSUITES ET DE PRENDRE UNE DÉCISION DE SANCTION [RJ3].
</SCT>
<SCT ID="8F" TYPE="PRINCIPAL">59-02-02-02 RÉPRESSION. DOMAINE DE LA RÉPRESSION ADMINISTRATIVE RÉGIME DE LA SANCTION ADMINISTRATIVE. RÉGULARITÉ. - PRINCIPE D'IMPARTIALITÉ - AUTORITÉ DE CONTRÔLE DES NUISANCES SONORES AÉROPORTUAIRES (ACNUSA) - PROCÉDURE DE SANCTION - 1) PRÉSIDENT DE L'ACNUSA AYANT ENGAGÉ DES POURSUITES PUIS PARTICIPÉ À L'ADOPTION DE LA DÉCISION DE SANCTION - MÉCONNAISSANCE DU PRINCIPE D'IMPARTIALITÉ [RJ2] N'ENTACHANT PAS L'ENSEMBLE DE LA PROCÉDURE - 2) CONSÉQUENCE - FACULTÉ POUR LE JUGE DE PLEIN CONTENTIEUX DE STATUER SUR LES POURSUITES ET DE PRENDRE UNE DÉCISION DE SANCTION [RJ3].
</SCT>
<SCT ID="8G" TYPE="PRINCIPAL">65-03-04-05 TRANSPORTS. TRANSPORTS AÉRIENS. AÉROPORTS. NUISANCES CAUSÉES AUX RIVERAINS. - ACNUSA - 1) A) QUALITÉ D'AAI - EXISTENCE - B) CONSÉQUENCE - QUALITÉ POUR AGIR DEVANT LES JURIDICTIONS ADMINISTRATIVES - EXISTENCE [RJ1] - 2) PROCÉDURE DE SANCTION - A) PRÉSIDENT DE L'ACNUSA AYANT ENGAGÉ DES POURSUITES PUIS PARTICIPÉ À L'ADOPTION DE LA DÉCISION DE SANCTION - MÉCONNAISSANCE DU PRINCIPE D'IMPARTIALITÉ [RJ2] N'ENTACHANT PAS L'ENSEMBLE DE LA PROCÉDURE - B) CONSÉQUENCE - FACULTÉ POUR LE JUGE DE PLEIN CONTENTIEUX DE STATUER SUR LES POURSUITES ET DE PRENDRE UNE DÉCISION DE SANCTION [RJ3].
</SCT>
<ANA ID="9A"> 01-04-03 1) Président de l'Autorité de contrôle des nuisances sonores aéroportuaires (ACNUSA) ayant fait usage de son pouvoir de poursuite des manquements constatés puis ayant participé aux débats et au vote à l'issue desquels a été infligée une sanction à leur auteur.,,,Cette méconnaissance du principe d'impartialité n'entache pas d'irrégularité l'ensemble de la procédure suivie devant l'ACNUSA et n'affecte ni l'engagement de cette procédure, ni l'instruction et les poursuites.... ,,2) Dans les circonstances de l'espèce, et alors que la régularité de la procédure ayant précédé le prononcé de la sanction ne fait l'objet d'aucune contestation, il y a lieu pour le juge administratif, eu égard à son office de plein contentieux, de statuer sur les poursuites en prenant une décision qui se substitue à celle qui avait été prise.</ANA>
<ANA ID="9B"> 44-05-01 1) a) Aux termes du premier alinéa de l'article L. 6361-1 du code des transports et comme le mentionne l'annexe à la loi n° 2017-55 du 20 janvier 2017, l'Autorité de contrôle des nuisances aéroportuaires (ACNUSA) est une autorité administrative indépendante (AAI).,,,b) En cette qualité et alors même qu'elle ne dispose pas de la personnalité morale, elle peut agir devant les juridictions administratives, en demande comme en défense, en particulier dans les litiges relatifs aux décisions qu'elle prend, sans qu'y fassent obstacle les dispositions du code de justice administrative relatives à la représentation de l'Etat devant ces juridictions. Par suite, l'ACNUSA a qualité pour faire appel du jugement du tribunal administratif ayant annulé une décision de sanction qu'elle a prononcée.,,,2) a) Président de l'ACNUSA ayant fait usage de son pouvoir de poursuite des manquements constatés puis ayant participé aux débats et au vote à l'issue desquels a été infligée une sanction à leur auteur.... ,,Cette méconnaissance du principe d'impartialité n'entache pas d'irrégularité l'ensemble de la procédure suivie devant l'ACNUSA et n'affecte ni l'engagement de cette procédure, ni l'instruction et les poursuites.... ,,b) Dans les circonstances de l'espèce, et alors que la régularité de la procédure ayant précédé le prononcé de la sanction ne fait l'objet d'aucune contestation, il y a lieu pour le juge administratif, eu égard à son office de plein contentieux, de statuer sur les poursuites en prenant une décision qui se substitue à celle qui avait été prise.</ANA>
<ANA ID="9C"> 52-045 1) a) Aux termes du premier alinéa de l'article L. 6361-1 du code des transports et comme le mentionne l'annexe à la loi n° 2017-55 du 20 janvier 2017, l'Autorité de contrôle des nuisances aéroportuaires (ACNUSA) est une autorité administrative indépendante (AAI).,,,b) En cette qualité et alors même qu'elle ne dispose pas de la personnalité morale, elle peut agir devant les juridictions administratives, en demande comme en défense, en particulier dans les litiges relatifs aux décisions qu'elle prend, sans qu'y fassent obstacle les dispositions du code de justice administrative relatives à la représentation de l'Etat devant ces juridictions. Par suite, l'ACNUSA a qualité pour faire appel du jugement du tribunal administratif ayant annulé une décision de sanction qu'elle a prononcée.,,,2) a) Président de l'ACNUSA ayant fait usage de son pouvoir de poursuite des manquements constatés puis ayant participé aux débats et au vote à l'issue desquels a été infligée une sanction à leur auteur.,,,Cette méconnaissance du principe d'impartialité n'entache pas d'irrégularité l'ensemble de la procédure suivie devant l'ACNUSA et n'affecte ni l'engagement de cette procédure, ni l'instruction et les poursuites.... ,,b) Dans les circonstances de l'espèce, et alors que la régularité de la procédure ayant précédé le prononcé de la sanction ne fait l'objet d'aucune contestation, il y a lieu pour le juge administratif, eu égard à son office de plein contentieux, de statuer sur les poursuites en prenant une décision qui se substitue à celle qui avait été prise.</ANA>
<ANA ID="9D"> 54-01-05 1) Aux termes du premier alinéa de l'article L. 6361-1 du code des transports et comme le mentionne l'annexe à la loi n° 2017-55 du 20 janvier 2017, l'Autorité de contrôle des nuisances aéroportuaires (ACNUSA) est une autorité administrative indépendante (AAI).,,,2) En cette qualité et alors même qu'elle ne dispose pas de la personnalité morale, elle peut agir devant les juridictions administratives, en demande comme en défense, en particulier dans les litiges relatifs aux décisions qu'elle prend, sans qu'y fassent obstacle les dispositions du code de justice administrative relatives à la représentation de l'Etat devant ces juridictions. Par suite, l'ACNUSA a qualité pour faire appel du jugement du tribunal administratif ayant annulé une décision de sanction qu'elle a prononcée.</ANA>
<ANA ID="9E"> 54-07-03 1) Président de l'Autorité de contrôle des nuisances sonores aéroportuaires (ACNUSA) ayant fait usage de son pouvoir de poursuite des manquements constatés puis ayant participé aux débats et au vote à l'issue desquels a été infligée une sanction à leur auteur.,,,Cette méconnaissance du principe d'impartialité n'entache pas d'irrégularité l'ensemble de la procédure suivie devant l'ACNUSA et n'affecte ni l'engagement de cette procédure, ni l'instruction et les poursuites.... ,,2) Dans les circonstances de l'espèce, et alors que la régularité de la procédure ayant précédé le prononcé de la sanction ne fait l'objet d'aucune contestation, il y a lieu pour le juge administratif, eu égard à son office de plein contentieux, de statuer sur les poursuites en prenant une décision qui se substitue à celle qui avait été prise.</ANA>
<ANA ID="9F"> 59-02-02-02 1) Président de l'Autorité de contrôle des nuisances sonores aéroportuaires (ACNUSA) ayant fait usage de son pouvoir de poursuite des manquements constatés puis ayant participé aux débats et au vote à l'issue desquels a été infligée une sanction à leur auteur.,,,Cette méconnaissance du principe d'impartialité n'entache pas d'irrégularité l'ensemble de la procédure suivie devant l'ACNUSA et n'affecte ni l'engagement de cette procédure, ni l'instruction et les poursuites.... ,,2) Dans les circonstances de l'espèce, et alors que la régularité de la procédure ayant précédé le prononcé de la sanction ne fait l'objet d'aucune contestation, il y a lieu pour le juge administratif, eu égard à son office de plein contentieux, de statuer sur les poursuites en prenant une décision qui se substitue à celle qui avait été prise.</ANA>
<ANA ID="9G"> 65-03-04-05 1) a) Aux termes du premier alinéa de l'article L. 6361-1 du code des transports et comme le mentionne l'annexe à la loi n° 2017-55 du 20 janvier 2017, l'Autorité de contrôle des nuisances aéroportuaires (ACNUSA) est une autorité administrative indépendante (AAI).... ,,b) En cette qualité et alors même qu'elle ne dispose pas de la personnalité morale, elle peut agir devant les juridictions administratives, en demande comme en défense, en particulier dans les litiges relatifs aux décisions qu'elle prend, sans qu'y fassent obstacle les dispositions du code de justice administrative relatives à la représentation de l'Etat devant ces juridictions. Par suite, l'ACNUSA a qualité pour faire appel du jugement du tribunal administratif ayant annulé une décision de sanction qu'elle a prononcée.,,,2) a) Président de l'ACNUSA ayant fait usage de son pouvoir de poursuite des manquements constatés puis ayant participé aux débats et au vote à l'issue desquels a été infligée une sanction à leur auteur.,,,Cette méconnaissance du principe d'impartialité n'entache pas d'irrégularité l'ensemble de la procédure suivie devant l'ACNUSA et n'affecte ni l'engagement de cette procédure, ni l'instruction et les poursuites.... ,,b) Dans les circonstances de l'espèce, et alors que la régularité de la procédure ayant précédé le prononcé de la sanction ne fait l'objet d'aucune contestation, il y a lieu pour le juge administratif, eu égard à son office de plein contentieux, de statuer sur les poursuites en prenant une décision qui se substitue à celle qui avait été prise.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., avant l'entrée en vigueur de la loi du 20 janvier 2017, CE, 5 novembre 1993, Commission des opérations de bourse, n° 143973, T. p. 955 ; CE, Assemblée, 27 mars 2015, Commission nationale des comptes de campagne et des financements politiques c/ Mme,et société éditrice de Mediapart, n° 382083, p. 128.,,[RJ2] Cf., s'agissant de l'application à l'ACNUSA du principe d'impartialité découlant de l'article 6 de la convention EDH, qui exige la séparation des fonctions de poursuite et de jugement, CE, 23 avril 2009, Compagnie Blue Line, n° 314918, T. pp. 744-875-969. Rappr., s'agissant de la même exigence de séparation découlant de l'article 16 de la DDHC, Cons. const., 24 novembre 2017, n° 2017-675 QPC.,,[RJ3] Comp., lorsque l'irrégularité censurée entache la procédure dès la saisine de l'autorité administrative investie du pouvoir de sanction, CE, 11 avril 2018, M.,, n° 413349, T. pp. 859- 931 ; dès la saisine de l'autorité juridictionnelle, CE, Section, 20 octobre 2000, Société Habib Bank Limited, n° 180122, p. 433.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
