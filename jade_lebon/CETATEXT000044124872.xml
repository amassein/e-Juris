<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044124872</ID>
<ANCIEN_ID>JG_L_2021_09_000000437650</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/12/48/CETATEXT000044124872.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 28/09/2021, 437650, Publié au recueil Lebon</TITRE>
<DATE_DEC>2021-09-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>437650</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC/>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BORE, SALVE DE BRUNETON, MEGRET</AVOCATS>
<RAPPORTEUR>M. Géraud Sajust de Bergues</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Cytermann</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:437650.20210928</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu les procédures suivantes :<br/>
<br/>
              M. A... B... a demandé au tribunal administratif de Nantes d'annuler la décision du 20 novembre 2008 du préfet de la région Pays-de-la-Loire lui demandant de rembourser l'aide d'urgence d'un montant de 30 000 euros qui lui avait été accordée la même année au titre du plan de sauvegarde et de restructuration de la pêche, ainsi que le titre exécutoire du 27 mars 2015, notifié le 15 avril 2015, du directeur de l'Agence de services et de paiement, émis pour le recouvrement de cette somme. <br/>
<br/>
              Par un jugement n° 1503575 du 7 décembre 2017, le tribunal administratif de Nantes a rejeté les conclusions de M. B... tendant à l'annulation de la décision du 20 novembre 2008 mais a annulé le titre exécutoire du 27 mars 2015 et a, en conséquence, déchargé M. B... de son obligation de reverser la somme de 30 000 euros qui lui avaient été allouée.<br/>
<br/>
              Par un arrêt n° 18NT00548 du 15 novembre 2019, la cour administrative d'appel de Nantes a rejeté l'appel formé par l'Agence de services et de paiement contre ce jugement.<br/>
<br/>
              1° Sous le n° 437650, par un pourvoi, enregistré le 15 janvier 2020 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'agriculture et de l'alimentation demande au Conseil d'Etat d'annuler cet arrêt.<br/>
<br/>
<br/>
<br/>
              2° Sous le n° 437683, par un pourvoi sommaire et un mémoire complémentaire, enregistrés le 15 janvier et le 4 mai 2020, l'Agence de services et de paiement demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le même arrêt de la cour administrative d'appel de Nantes ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de M. B... la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - le décret n° 2012-1246 du 7 novembre 2012 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Géraud Sajust de Bergues, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Laurent Cytermann, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Boré, Salve de Bruneton, Mégret, avocat de l'Agence de services et de paiement ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. M. A... B..., marin pêcheur en Vendée, a obtenu en 2008, au titre du plan de sauvegarde et de restructuration de la pêche, une aide d'urgence de 30 000 euros pour le navire Castor et Pollux qu'il avait affrété. Toutefois, ce navire ayant été détruit le 22 août 2008 avec le soutien d'un plan de sortie de flotte, le préfet de la région Pays-de-la-Loire a, par une décision du 20 novembre 2008, demandé à M. B... de rembourser la totalité de la somme perçue. Ce dernier a formé le 18 décembre 2008 contre cette décision, auprès du ministre de l'agriculture, de l'alimentation et de la pêche, un recours qui a été implicitement rejeté. En exécution de la décision du 20 novembre 2008, un ordre de reversement a été émis le 2 décembre 2008 par le Centre national pour l'aménagement des structures des exploitations agricoles (CNASEA) puis, après l'envoi de plusieurs actes de poursuites, a été réitéré par un titre exécutoire émis le 27 mars 2015 par l'Agence de services et de paiement, qui avait succédé au CNASEA, et signifié par huissier à M. B... le 15 avril 2015. Par un jugement du 7 décembre 2017, le tribunal administratif de Nantes a annulé pour vice de forme le titre exécutoire du 27 mars 2015 et rejeté le surplus des conclusions de M. B.... Le ministre de l'agriculture et de l'alimentation et l'Agence de services et de paiement se pourvoient contre l'arrêt du 15 novembre 2019 par lequel la cour administrative d'appel de Nantes, si elle a jugé que le préfet de la région Pays-de-la-Loire était fondé à demander à M. B... le remboursement partiel de l'aide d'urgence qui lui avait été accordée, a rejeté l'appel de l'Agence de services et de paiement contre ce jugement.<br/>
<br/>
              2. Les pourvois présentés par le ministre de l'agriculture et de l'alimentation et l'Agence de services et de paiement sont dirigés contre le même arrêt et présentent à juger les mêmes questions. Il y a lieu de les joindre pour statuer par une même décision. <br/>
<br/>
              3. Le destinataire d'un ordre de versement est recevable à contester, à l'appui de son recours contre cet ordre de versement, et dans un délai de deux mois suivant la notification de ce dernier, le bien-fondé de la créance correspondante, alors même que la décision initiale constatant et liquidant cette créance est devenue définitive, comme le prévoient au demeurant, pour les dépenses de l'Etat, les articles 117 et 118 du décret du 7 novembre 2012 relatif à la gestion budgétaire et comptable publique ou, pour les dépenses des collectivités locales, l'article L. 1617-5 du code général des collectivités territoriales. <br/>
<br/>
              4. C'est par suite sans erreur de droit que la cour administrative d'appel a écarté le moyen tiré de ce que, la décision du 20 novembre 2008 par laquelle le préfet de la région Pays-de-la-Loire a demandé à M. B... de rembourser la totalité de l'aide d'urgence de 30 000 euros qui lui avait été versée pour le navire Castor et Pollux étant devenue définitive, le requérant n'était pas recevable à contester le bien-fondé de la créance correspondante à l'appui de son recours contre l'ordre de reversement émis le 27 mars 2015 par l'Agence de service et de paiement en exécution de cette décision.<br/>
<br/>
              5. Toutefois, il ressort des pièces du dossier soumis aux juges du fond que l'ordre de reversement du 27 mars 2015, qui porte comme date d'émission le 2 décembre 2008, se borne à rappeler le montant de la somme due par M. B... en vertu de l'ordre de reversement émis par le CNASEA le 2 décembre 2008, lequel était devenu définitif. Dans ces conditions, la demande présentée par M. B... devant le tribunal administratif le 28 avril 2015, dirigée contre un acte qui se borne à réitérer le titre exécutoire devenu définitif du 2 décembre 2008, était tardive et par suite irrecevable. Dès lors, en jugeant recevables les conclusions de M. B... dirigées contre l'ordre de reversement du 27 mars 2015, la cour administrative d'appel a commis une erreur de droit. Par suite et sans qu'il soit besoin d'examiner les autres moyens des pourvois, son arrêt doit être annulé.<br/>
<br/>
              6. Dans les circonstances de l'espèce, il y a lieu de régler l'affaire au fond en application de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              7. Pour les motifs indiqués au point 5, la demande de M. B... devant le tribunal administratif de Nantes, en tant qu'elle demandait l'annulation de l'ordre de reversement du 27 mars 2015, était irrecevable. Par suite, le jugement du tribunal administratif doit être annulé sur ce point et les conclusions de M. B... dirigées contre l'acte du 27 mars 2015 ne peuvent qu'être rejetées.<br/>
<br/>
              8. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de M. B... la somme demandée par l'Agence de services et de paiement au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nantes du 15 novembre 2019 et les articles 1er et 2 du jugement du tribunal administratif de Nantes du 7 décembre 2017 sont annulés.<br/>
Article 2 : La demande présentée par M. B... devant le tribunal administratif de Nantes contre le titre du 27 mars 2015 est rejetée.<br/>
Article 3 : Les conclusions présentées par l'Agence de services et de paiement au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée au ministre de l'agriculture et de l'alimentation, à l'Agence de services et de paiement et à M. A... B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">18-03-02-01-02 COMPTABILITÉ PUBLIQUE ET BUDGET. - CRÉANCES DES COLLECTIVITÉS PUBLIQUES. - RECOUVREMENT. - PROCÉDURE. - ORDRE DE VERSEMENT. - RECOURS - RECEVABILITÉ DE LA CONTESTATION, DANS LE DÉLAI DE RECOURS, DU BIEN-FONDÉ DE LA CRÉANCE - EXISTENCE [RJ1], ALORS MÊME QUE LA DÉCISION ÉTABLISSANT LA CRÉANCE SERAIT DEVENUE DÉFINITIVE [RJ2].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-07-01-04-02 PROCÉDURE. - POUVOIRS ET DEVOIRS DU JUGE. - QUESTIONS GÉNÉRALES. - MOYENS. - MOYENS IRRECEVABLES. - EXCLUSION - RECOURS CONTRE UN ORDRE DE REVERSEMENT - MOYEN CONTESTANT, DANS LE DÉLAI DE RECOURS, LE BIEN-FONDÉ DE LA CRÉANCE [RJ1], ALORS MÊME QUE LA DÉCISION ÉTABLISSANT CELLE-CI SERAIT DEVENUE DÉFINITIVE [RJ2].
</SCT>
<ANA ID="9A"> 18-03-02-01-02 Le destinataire d'un ordre de versement est recevable à contester, à l'appui de son recours contre cet ordre de versement, et dans un délai de deux mois suivant la notification de ce dernier, le bien-fondé de la créance correspondante, alors même que la décision initiale constatant et liquidant cette créance est devenue définitive, comme le prévoient au demeurant, pour les dépenses de l'Etat, les articles 117 et 118 du décret n° 2012-1246 du 7 novembre 2012 ou, pour les dépenses des collectivités locales, l'article L. 1617-5 du code général des collectivités territoriales (CGCT).</ANA>
<ANA ID="9B"> 54-07-01-04-02 Le destinataire d'un ordre de versement est recevable à contester, à l'appui de son recours contre cet ordre de versement, et dans un délai de deux mois suivant la notification de ce dernier, le bien-fondé de la créance correspondante, alors même que la décision initiale constatant et liquidant cette créance est devenue définitive, comme le prévoient au demeurant, pour les dépenses de l'Etat, les articles 117 et 118 du décret n° 2012-1246 du 7 novembre 2012 ou, pour les dépenses des collectivités locales, l'article L. 1617-5 du code général des collectivités territoriales (CGCT).</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., pour l'application du décret n° 62-1587 du 29 décembre 1962, CE, Section, 10 janvier 1969, Société d'approvisionnement alimentaire, n° 66379, p. 18 ; CE, Section, 12 janvier 1973, Ville du Cannet c/ Sieur Pantacchini, n° 78730, p. 36 ; pour l'application de l'article L. 1617-5 du CGCT, CE, 6 avril 2018, Mme Thiers, n° 405014, T. p. 555 ; CE, 18 mars 2020, Mme Sidoux, n° 421911, T. pp. 598-599-600-949 ; pour l'application des articles L. 2333-87 et R. 2333-120-35 du CGCT, CE, 10 juin 2020, M. Nsimba-Ntumba, n° 427155, p. 180....[RJ2] Comp., s'agissant d'une exception d'illégalité, CE, Section, 30 décembre 2013, Mme Okosun, n° 367615, p. 342.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
