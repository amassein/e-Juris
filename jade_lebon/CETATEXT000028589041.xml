<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028589041</ID>
<ANCIEN_ID>JG_L_2014_02_000000354989</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/58/90/CETATEXT000028589041.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 12/02/2014, 354989</TITRE>
<DATE_DEC>2014-02-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>354989</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP HEMERY, THOMAS-RAQUIN ; SCP BORE, SALVE DE BRUNETON</AVOCATS>
<RAPPORTEUR>M. Rémi Decout-Paolini</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Maud Vialettes</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:354989.20140212</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 19 décembre 2011 et 19 mars 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. C...A..., demeurant... ; M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 09MA04005 du 20 octobre 2011 de la cour administrative d'appel de Marseille en tant qu'il a, après avoir annulé le jugement n° 0803124 du 1er octobre 2009 du tribunal administratif de Nîmes, rejeté ses conclusions tendant, en premier lieu, à l'annulation des décisions des 31 juillet, 7 août et 27 août 2008 par lesquelles la vice-présidente du centre communal d'action sociale de Bollène a décidé de ne pas renouveler son contrat de concierge du foyer-logement Alphonse-Daudet, en deuxième lieu, à sa réintégration à ce poste jusqu'au 28 février 2009 et, en troisième lieu, à la condamnation du centre communal d'action sociale de Bollène à lui verser en réparation des préjudices subis une somme de 15 000 euros, augmentée des intérêts au taux légal ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à celles de ses conclusions d'appel qui ont été rejetées par la cour ;<br/>
<br/>
              3°) de mettre à la charge du centre communal d'action sociale de Bollène la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'action sociale et des familles ;<br/>
<br/>
              Vu la loi n° 79-587 du 11 juillet 1979 ;<br/>
<br/>
              Vu la loi n° 84-53 du 26 janvier 1984 ;<br/>
<br/>
              Vu la loi n° 2000-321 du 12 avril 2000 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Rémi Decout-Paolini, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Hémery, Thomas-Raquin, avocat de M. A...et à la SCP Boré, Salve de Bruneton, avocat du centre communal d'action sociale de Bollène ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. A...a été recruté le 27 février 2008 en qualité d'agent non titulaire par le centre communal d'action sociale (CCAS) de Bollène (Vaucluse), aux fins d'exercer, du 3 mars au 31 août 2008, les fonctions de concierge du foyer-logement Alphonse-Daudet ; que par lettre du 4 juin 2008, la présidente du centre a indiqué à M. A...que son contrat serait renouvelé jusqu'au 28 février 2009 ; que, cependant, par trois courriers des 31 juillet, 7 août et 27 août 2008, la vice-présidente du centre a informé l'intéressé du non-renouvellement de son contrat ; que M. A...se pourvoit en cassation contre l'arrêt du 20 octobre 2011 par lequel la cour administrative d'appel de Marseille, après avoir annulé le jugement du 1er octobre 2009 du tribunal administratif de Nîmes et évoqué l'affaire, a rejeté sa demande d'annulation des trois décisions précitées ;<br/>
<br/>
              2. Considérant, en premier lieu, que, par sa décision du 7 août 2008, la vice-présidente du CCAS de Bollène a indiqué à M. A...qu'il avait été recruté sur le fondement du deuxième alinéa de l'article 3 de la loi du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale, aux fins de répondre à un besoin saisonnier, et que la durée d'un tel contrat se trouvait limitée, en application de ces dispositions, à une période maximale de six mois sur une même période de douze mois ; que cette décision, motivée en fait et en droit, par laquelle la vice-présidente du centre a informé M. A...que son engagement en qualité de concierge du foyer-logement expirerait le 31 août 2008, à la date initialement prévue dans son contrat, a implicitement mais nécessairement retiré, en s'y substituant, la précédente décision, non motivée, du 31 juillet 2008, par laquelle la même autorité informait l'intéressé du non-renouvellement de ce contrat ; que, par suite, c'est sans commettre d'erreur de droit ni violer les dispositions de l'article 1er de la loi du 11 juillet 1979 relative à la motivation des actes administratifs et à l'amélioration des relations entre l'administration et le public que la cour administrative d'appel de Marseille a jugé que M. A... n'était pas recevable à demander l'annulation, pour défaut de motivation, de la décision du 31 juillet 2008 ;<br/>
<br/>
              3. Considérant, en deuxième lieu, qu'aux termes de l'article L. 123-6 du code de l'action sociale et des familles : " Le centre d'action sociale est un établissement public administratif communal ou intercommunal. Il est administré par un conseil d'administration présidé, selon le cas, par le maire ou le président de l'établissement public de coopération intercommunale. / Dès qu'il est constitué, le conseil d'administration élit en son sein un vice-président qui le préside en l'absence du maire, nonobstant les dispositions de l'article L. 2122-17 du code général des collectivités territoriales, ou en l'absence du président de l'établissement de coopération intercommunale (...) " ; qu'il résulte de ces dispositions qu'en cas d'absence ou d'empêchement, le président du centre communal d'action sociale est provisoirement remplacé, dans la plénitude de ses fonctions, par son vice-président, sans que l'exercice de cette suppléance soit subordonnée à une délégation donnée à cet effet par le président au vice-président ; qu'aux termes de l'article R. 123-23 du même code : " Le président du conseil d'administration prépare et exécute les délibérations du conseil ; il est ordonnateur des dépenses et des recettes du budget du centre. Il nomme les agents du centre " ; qu'ainsi, la cour administrative d'appel de Marseille, qui avait relevé, par une appréciation souveraine exempte de dénaturation, que Mme Bompard, présidente du CCAS de Bollène, se trouvait empêchée à la date des décisions litigieuses, a pu en déduire sans erreur de droit que Mme B..., vice-présidente, était compétente pour refuser le renouvellement du contrat de M. A... ;<br/>
<br/>
              4. Considérant, en troisième lieu, que ni les dispositions de la loi du 12 avril 2000 relative aux droits des citoyens dans leurs relations avec les administrations ni aucun principe général du droit n'imposent à l'administration de respecter le principe du contradictoire avant de prendre, pour des motifs étrangers à la personne de l'agent non titulaire concerné, la décision de ne pas renouveler son contrat à durée déterminée ; que, par suite, c'est sans erreur de droit que la cour administrative de Marseille a jugé que les décisions des 7 et 27 août 2008 n'étaient pas intervenues à l'issue d'une procédure irrégulière ;<br/>
<br/>
              5. Considérant, en dernier lieu, qu'en vertu du deuxième alinéa de l'article 3 de la loi du 26 janvier 1984, les établissements publics communaux peuvent " recruter des agents non titulaires pour exercer des fonctions correspondant à un besoin saisonnier pour une durée maximale de six mois pendant une même période de douze mois " ; que la cour ne s'est pas méprise, eu égard à la délibération du conseil d'administration du CCAS et au texte qu'il vise, en regardant le contrat de recrutement à durée déterminée passé le 27 février 2008 entre le CCAS de Bollène et M. A...comme conclu, sur le fondement de ces dispositions, pour couvrir un besoin saisonnier ; que si ce contrat a fait naître au profit de ce dernier des droits jusqu'à sa date d'expiration, alors même que l'absence de caractère saisonnier des fonctions de concierge du foyer-logement Alphonse-Daudet l'entachait d'illégalité, la vice-présidente du CCAS pouvait légalement retirer la décision du 4 juin 2008 de le renouveler, dès lors, ainsi que l'a jugé à bon droit la cour administrative d'appel de Marseille, que la durée d'un contrat saisonnier ne pouvait légalement excéder une période de six mois sur une même période de douze mois et que ce retrait intervenait dans le délai de quatre mois à compter de l'édiction de la décision litigieuse ; qu'ainsi, la cour n'a pas commis d'erreur de droit en jugeant que la décision du 4 juin 2008 avait pu être légalement retirée ;<br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède que M. A...n'est pas fondé à demander l'annulation de l'arrêt qu'il attaque ;<br/>
<br/>
              7. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge du CCAS de Bollène qui n'est pas, dans la présente instance, la partie perdante, la somme que M. A...demande au titre des frais exposés par lui et non compris dans les dépens ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées au même titre par le CCAS de Bollène ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. A...est rejeté.<br/>
Article 2 : Les conclusions présentées par le CCAS de Bollène au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à M. C...A...et au centre communal d'action sociale (CCAS) de Bollène.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">04-01-02-01 AIDE SOCIALE. ORGANISATION DE L'AIDE SOCIALE. COMPÉTENCES DES COMMUNES. CENTRES COMMUNAUX D'ACTION SOCIALE. - ABSENCE OU EMPÊCHEMENT DU PRÉSIDENT - CONSÉQUENCE - VICE-PRÉSIDENT DISPOSANT DE L'ENSEMBLE DES COMPÉTENCES DU PRÉSIDENT, MÊME EN L'ABSENCE DE DÉLÉGATION DONNÉE À CET EFFET - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">135-02-01-02-02-03 COLLECTIVITÉS TERRITORIALES. COMMUNE. ORGANISATION DE LA COMMUNE. ORGANES DE LA COMMUNE. MAIRE ET ADJOINTS. POUVOIRS DU MAIRE. - PRÉSIDENCE DU CENTRE COMMUNAL D'ACTION SOCIALE - ABSENCE OU EMPÊCHEMENT - CONSÉQUENCE - VICE-PRÉSIDENT DISPOSANT DE L'ENSEMBLE DES COMPÉTENCES DU PRÉSIDENT, MÊME EN L'ABSENCE DE DÉLÉGATION DONNÉE À CET EFFET - EXISTENCE.
</SCT>
<ANA ID="9A"> 04-01-02-01 Il résulte de l'article L. 123-6 du code de l'action sociale et des familles qu'en cas d'absence ou d'empêchement, le président d'un centre communal d'action sociale est provisoirement remplacé, dans la plénitude de ses fonctions, par son vice-président, sans que l'exercice de cette suppléance soit subordonnée à une délégation donnée à cet effet par le président au vice-président.</ANA>
<ANA ID="9B"> 135-02-01-02-02-03 Il résulte de l'article L. 123-6 du code de l'action sociale et des familles qu'en cas d'absence ou d'empêchement, le président d'un centre communal d'action sociale est provisoirement remplacé, dans la plénitude de ses fonctions, par son vice-président, sans que l'exercice de cette suppléance soit subordonnée à une délégation donnée à cet effet par le président au vice-président.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
