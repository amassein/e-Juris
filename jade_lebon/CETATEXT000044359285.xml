<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044359285</ID>
<ANCIEN_ID>JG_L_2021_11_000000445758</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/35/92/CETATEXT000044359285.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 1ère chambres réunies, 22/11/2021, 445758</TITRE>
<DATE_DEC>2021-11-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>445758</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Dorothée Pradines</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Raphaël Chambon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:445758.20211122</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Le préfet de la Loire-Atlantique a demandé au tribunal administratif de Nantes de rectifier les résultats des opérations électorales qui se sont déroulées le 28 mai 2020 en vue de l'élection des adjoints au maire de la commune de La Haye-Fouassière (Loire-Atlantique), de proclamer élus les adjoints au maire dans l'ordre de leur présentation sur la liste " Ensemble, vivons La Haye-Fouassière " ou d'enjoindre au conseil municipal de procéder à une nouvelle élection, de déclarer inexistante la seconde version de la proclamation des résultats des opérations électorales du 28 mai 2020, transmise le 11 juin, d'annuler pour excès de pouvoir les trois versions du tableau du conseil municipal, transmises respectivement les 29 mai, 5 juin et 11 juin 2020 ou, à titre subsidiaire, de déclarer inexistante la version transmise le 11 juin et d'enjoindre au maire d'établir un nouveau tableau du conseil municipal tenant compte de la rectification de l'ordre des adjoints et classant les autres conseillers municipaux selon l'ordre prescrit par l'article L. 2121-1 du code général des collectivités territoriales.<br/>
<br/>
              Par un jugement du 30 septembre 2020, le tribunal administratif a rejeté ce déféré du préfet de la Loire-Atlantique. <br/>
<br/>
              Par une requête, enregistrée le 28 octobre 2020 au secrétariat du contentieux du Conseil d'Etat, le préfet de la Loire-Atlantique demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) de faire droit à ses conclusions de première instance.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code électoral ;<br/>
              - le code général des collectivités territoriales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Dorothée Pradines, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Raphaël Chambon, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Aux termes de l'article L. 2122-1 du code général des collectivités territoriales : " Il y a, dans chaque commune, un maire et un ou plusieurs adjoints élus parmi les membres du conseil municipal ". Aux termes de l'article L. 2122-2 du même code : " Le conseil municipal détermine le nombre des adjoints au maire sans que ce nombre puisse excéder 30 % de l'effectif légal du conseil municipal ". Aux termes de l'article L. 2122-4 de ce code : " Le conseil municipal élit le maire et les adjoints parmi ses membres, au scrutin secret. (...) ". Aux termes de l'article L. 2122-7-2 de ce code : " Dans les communes de 1 000 habitants et plus, les adjoints sont élus au scrutin de liste à la majorité absolue, sans panachage ni vote préférentiel. La liste est composée alternativement d'un candidat de chaque sexe. / (...) ". <br/>
<br/>
              2. Aux termes du II de l'article L. 2121-1 du code général des collectivités territoriales : " Les membres du conseil municipal sont classés dans l'ordre du tableau selon les modalités suivantes. / Après le maire, prennent rang les adjoints puis les conseillers municipaux. / Sous réserve du dernier alinéa des articles L. 2122-7-1 et L. 2122-7-2 et du second alinéa de l'article L. 2113-8-2, les adjoints prennent rang selon l'ordre de leur élection et, entre adjoints élus sur la même liste, selon l'ordre de présentation sur la liste (...) / En ce qui concerne les conseillers municipaux, l'ordre du tableau est déterminé, même quand il y a des sections électorales : / ( ...) / 2° Entre conseillers élus le même jour, par le plus grand nombre de suffrages obtenus ; / 3° Et, à égalité de voix, par priorité d'âge ". <br/>
<br/>
              3. À l'issue des opérations électorales qui se sont déroulées le 15 mars 2020 dans la commune de La Haye-Fouassière (Loire-Atlantique), les vingt-sept sièges de conseillers municipaux ont été pourvus. Le 28 mai 2020, les conseillers municipaux se sont réunis afin de procéder à l'installation du conseil municipal. Après l'élection du maire, la séance s'est poursuivie par la fixation du nombre d'adjoints au maire suivie de leur élection. Par courriel du 29 mai, le maire a transmis à la préfecture de la Loire-Atlantique le procès-verbal de l'élection du maire et des adjoints, auquel était annexée la feuille de proclamation des résultats ainsi que le tableau du conseil municipal établi en application de l'article L. 2121-1 du code général des collectivités territoriales. Postérieurement à cette transmission, il est apparu, d'une part, que les adjoints au maire ne figuraient selon leur ordre de présentation sur la liste soumise au vote des conseillers municipaux ni sur la feuille de proclamation annexée au procès-verbal ni sur le tableau du conseil municipal, d'autre part que les conseillers municipaux autres que les adjoints au maire n'étaient pas classés sur le tableau du conseil municipal par priorité d'âge, à nombre de voix égal. Par courriel du 5 juin 2020, le maire a adressé à la préfecture une deuxième version du tableau du conseil municipal, datée du 28 mai, dans laquelle les conseillers municipaux autres que les adjoints au maire étaient classés, à nombre de voix égal, par priorité d'âge. Enfin par courriel du 11 juin 2020, le maire a transmis à la préfecture, d'une part, le procès-verbal de l'élection, auquel était annexée une seconde version de la feuille de proclamation des résultats, datée du 28 mai, faisant figurer les adjoints au maire dans le même ordre que celui de la liste soumise au vote des conseillers municipaux, d'autre part, une troisième version du tableau du conseil municipal, également datée du 28 mai, faisant figurer les adjoints au maire dans cet ordre et les autres conseillers municipaux par priorité d'âge, à nombre de voix égal. Le préfet de la Loire-Atlantique relève appel du jugement du 30 octobre 2020 par lequel le tribunal administratif de Nantes a rejeté son déféré tendant, à titre principal, à l'annulation de la proclamation des résultats de l'élection des adjoints au maire de La Haye-Fouassière et du tableau du conseil municipal établis le 28 mai 2020 et à ce que les documents transmis les 5 et 11 juin 2020 soient déclarés inexistants.<br/>
<br/>
              Sur la compétence du Conseil d'État :<br/>
<br/>
              4. L'établissement du tableau du conseil municipal est distinct des opérations électorales et de la proclamation des résultats de ces opérations. Par suite, la contestation du préfet de la Loire-Atlantique concernant les versions successives du tableau du conseil municipal de La Haye-Fouassière n'a pas le caractère d'un litige en matière électorale, et ne relève pas de la compétence d'appel du Conseil d'État. Toutefois, aux termes de l'article R. 343-1 du code de justice administrative : " Lorsque le Conseil d'Etat est saisi de conclusions relevant de sa compétence comme juge d'appel, il est également compétent pour connaître de conclusions connexes relevant normalement de la compétence d'une cour administrative d'appel ". Il y a lieu pour le Conseil d'État, compétent pour connaître en appel du jugement du tribunal administratif de Nantes en tant qu'il statue sur les contestations relatives à la proclamation des résultats de l'élection des adjoints au maire de la commune de La Haye-Fouassière, de statuer sur les conclusions connexes par lesquelles le préfet de la Loire-Atlantique conteste le même jugement en tant qu'il statue sur ses conclusions relatives au tableau du conseil municipal.<br/>
<br/>
              Sur les conclusions relatives à la proclamation des résultats de l'élection des adjoints au maire :<br/>
<br/>
              5. En premier lieu, aux termes de l'article L. 2122-13 du code général des collectivités territoriales : " L'élection du maire et des adjoints peut être arguée de nullité dans les conditions, formes et délais prescrits pour les réclamations contre les élections du conseil municipal ". Aux termes du deuxième alinéa de l'article L. 248 du code électoral, applicable à l'élection du conseil municipal : " Le préfet, s'il estime que les conditions et les formes légalement prescrites n'ont pas été remplies, peut également déférer les opérations électorales au tribunal administratif. " Aux termes de l'article R. 118 du même code : " Un exemplaire du procès-verbal est, après signature, aussitôt envoyé au sous-préfet ou, dans l'arrondissement du chef-lieu du département, au préfet ; le sous-préfet ou le préfet en constate la réception sur un registre et en donne récépissé ". Enfin, aux termes de l'article R. 119 du même code : " (...) Le recours formé par le préfet en application de l'article L. 248 doit être exercé dans le délai de quinzaine à dater de la réception du procès-verbal ". <br/>
<br/>
              6. Il résulte des dispositions combinées des articles R. 118 et R. 119 du code électoral que la réception à la sous-préfecture ou à la préfecture du procès-verbal des opérations électorales fait courir le délai de quinze jours imparti au préfet pour déférer au tribunal administratif ces opérations électorales, y compris lorsque le procès-verbal a été transmis par voie électronique.<br/>
<br/>
              7. Il résulte de l'instruction que les première et seconde versions du procès-verbal de l'élection du maire et des adjoints de la commune de La Haye-Fouassière, transmises par voie électronique, ont été reçues respectivement les 29 mai et 11 juin 2020 par la préfecture de la Loire-Atlantique, et que le déféré du préfet n'a été enregistré au greffe du tribunal administratif de Nantes que le 17 juillet 2020, soit après l'expiration du délai prévu par l'article R. 119 du code électoral, lequel expirait le 15 juin 2020 à vingt-quatre heures, s'agissant de la première version du procès-verbal de l'élection du maire et des adjoints. Ainsi, ce déféré était tardif en tant qu'il tendait à l'annulation de la proclamation des résultats de l'élection des adjoints au maire. <br/>
<br/>
              8. En deuxième lieu, s'il résulte de la combinaison des articles R. 118 et R. 119 du code électoral et L. 2122-13 du code général des collectivités territoriales qu'il appartient à la seule juridiction administrative saisie d'une protestation ou d'un déféré préfectoral de rectifier les résultats de l'élection du maire et des adjoints, dès lors qu'ils ont été transcrits au procès-verbal signé des membres du bureau de vote, et si le maire de La Haye-Fouassière ne pouvait légalement, après cette proclamation, apporter la moindre rectification à l'annexe au procès-verbal des opérations électorales du 28 mai 2020, la modification à laquelle il a pourtant été procédé en l'espèce n'a pas altéré le résultat des opérations électorales mais a uniquement visé à faire apparaître les adjoints dans l'ordre prescrit par l'article L. 2121-1 du code général des collectivités territoriales. Dans ces conditions, cette modification ne peut être regardée comme un acte nul et de nul effet qui pourrait être contesté devant le juge administratif sans condition de délai. Il s'ensuit que le préfet de la Loire-Atlantique n'est pas fondé à soutenir que c'est à tort que, par le jugement attaqué qui est suffisamment motivé sur ce point, le tribunal administratif de Nantes a rejeté ses conclusions tendant à ce que seconde version de la proclamation des résultats soit déclarée nulle et non avenue. <br/>
<br/>
              Sur les conclusions relatives au tableau du conseil municipal :<br/>
<br/>
              9. Aux termes du premier alinéa de l'article R. 421-1 du code de justice administrative : " La juridiction ne peut être saisie que par voie de recours formé contre une décision, et ce, dans les deux mois à partir de la notification ou de la publication de la décision attaquée ".<br/>
<br/>
              10. Ainsi qu'il est dit au point 4, la contestation du tableau du conseil municipal ne constitue pas un litige en matière électorale. Elle n'est pas soumise au délai spécial fixé par l'article R. 119 du code électoral, mais peut être faite dans le délai de droit commun de deux mois imparti par les dispositions de l'article R. 421-1 du code de justice administrative. Par suite, c'est à tort que le tribunal administratif a rejeté comme tardives les conclusions du déféré du préfet de la Loire-Atlantique, enregistrées le 17 juillet 2020, relatives aux trois versions du tableau du conseil municipal. Il en résulte que le jugement attaqué du tribunal administratif doit être annulé en tant qu'il a statué sur ces conclusions.<br/>
<br/>
              11. Il y a lieu, pour le Conseil d'État, d'évoquer et de statuer immédiatement sur ces conclusions du préfet de la Loire-Atlantique. <br/>
<br/>
              En ce qui concerne la première version du tableau du conseil municipal :<br/>
<br/>
              12. Il ressort des pièces du dossier que la première version du tableau du conseil municipal, transmise au préfet le 29 mai 2020, d'une part ne fait pas apparaître les adjoints au maire dans l'ordre de leur présentation sur la liste, d'autre part ne classe pas les autres conseillers municipaux, à nombre de voix égales, par priorité d'âge. Par suite, ce tableau méconnaît les dispositions du II de l'article L. 2121-1 du code général des collectivités territoriales, citées au point 2, et le préfet de la Loire-Atlantique est fondé à en demander, pour ce motif, l'annulation pour excès de pouvoir.<br/>
<br/>
              En ce qui concerne la deuxième version du tableau du conseil municipal :<br/>
<br/>
              13. Il ressort des pièces du dossier que les adjoints au maire ne sont pas inscrits dans l'ordre de leur présentation sur la liste dans la deuxième version du tableau du conseil municipal, qui méconnaît ainsi les dispositions du II de l'article L. 2121-1 du code général des collectivités territoriales. Par suite, le préfet de la Loire-Atlantique est fondé à en demander l'annulation pour excès de pouvoir, sans qu'il soit besoin de se prononcer sur l'autre moyen qu'il invoque.<br/>
<br/>
              En ce qui concerne la troisième version du tableau du conseil municipal : <br/>
<br/>
              14. Il ressort des pièces du dossier que les adjoints au maire et les autres conseillers municipaux sont inscrits selon l'ordre prescrit par les dispositions du II de l'article L. 2121-1 du code général des collectivités territoriales sur la troisième version du tableau du conseil municipal, établie et transmise au préfet le 11 juin 2020. Si le préfet fait valoir que cet acte a été antidaté, cette circonstance n'est pas, dans les circonstances particulières de l'espèce, de nature à le faire regarder comme nul et de nul effet. En revanche, ce tableau ne saurait légalement produire effet avant la date de son établissement le 11 juin 2020. Par suite, le préfet n'est fondé à en demander l'annulation qu'en tant qu'il a entendu produire effet entre le 28 mai et le 11 juin 2020. <br/>
<br/>
              Sur les conclusions présentées par la commune de La Haye-Fouassière devant le Conseil d'État :<br/>
<br/>
              15. Il résulte de ce tout ce qui précède que les conclusions de la commune de La Haye-Fouassière tendant à ce que le Conseil d'État rectifie la première version de la proclamation des résultats des opérations électorales du 28 mai 2020 et l'autorise à établir un nouveau tableau du conseil municipal sont devenues sans objet. Il s'ensuit qu'il n'y a plus lieu d'y statuer. <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du 30 septembre 2020 du tribunal administratif de Nantes est annulé en tant qu'il rejette les conclusions du préfet de la Loire-Atlantique relatives aux tableaux du conseil municipal de la commune de La Haye-Fouassière. <br/>
Article 2 : Les tableaux du conseil municipal établis le 28 mai et le 5 juin 2020 sont annulés. Le tableau établi et transmis le 11 juin 2020 à la préfecture de la Loire-Atlantique est annulé en tant qu'il a entendu produire effet entre le 28 mai et le 11 juin 2020.<br/>
Article 3 : Le surplus des conclusions de la requête du préfet de la Loire-Atlantique et des conclusions de son déféré sont rejetées. <br/>
Article 4 : Il n'y a pas lieu de statuer sur les conclusions de la commune de La Haye-Fouassière tendant à ce que le Conseil d'État rectifie la première version de la proclamation des résultats des opérations électorales du 28 mai 2020 et l'autorise à établir un nouveau tableau du conseil municipal. <br/>
Article 5 : La présente décision sera notifiée au préfet de la Loire-Atlantique, à la commune de La Haye-Fouassière et au ministre de l'intérieur.<br/>
<br/>
              Délibéré à l'issue de la séance du 22 octobre 2021 où siégeaient : M. Jacques-Henri Stahl, président adjoint de la section du contentieux, présidant ; Mme A... O..., Mme F... N..., présidentes de chambre ; M. B... M..., M. K... I..., Mme J... L..., Mme D... H..., M. Damien Botteghi, conseillers d'Etat et Mme Dorothée Pradines, maître des requêtes-rapporteure. <br/>
<br/>
              Rendu le 22 novembre 2021.<br/>
<br/>
<br/>
                 Le président : <br/>
                 Signé : M. P... C...<br/>
 		La rapporteure : <br/>
      Signé : Mme Dorothée Pradines<br/>
                 La secrétaire :<br/>
                 Signé : Mme E... G...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">28-08-01-02 ÉLECTIONS ET RÉFÉRENDUM. - RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. - INTRODUCTION DE L'INSTANCE. - DÉLAIS. - DÉFÉRÉ PRÉFECTORAL - POINT DE DÉPART - TRANSMISSION DU PROCÈS-VERBAL, Y COMPRIS PAR VOIE ÉLECTRONIQUE.
</SCT>
<ANA ID="9A"> 28-08-01-02 Il résulte de la combinaison des articles R. 118 et R. 119 du code électoral que la réception à la sous-préfecture ou à la préfecture du procès-verbal des opérations électorales fait courir le délai de quinze jours imparti au préfet pour déférer au tribunal administratif ces opérations électorales, y compris lorsque le procès-verbal a été transmis par voie électronique.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
