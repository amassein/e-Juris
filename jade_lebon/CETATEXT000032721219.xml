<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032721219</ID>
<ANCIEN_ID>JG_L_2016_06_000000387531</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/72/12/CETATEXT000032721219.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 4ème chambres réunies, 16/06/2016, 387531</TITRE>
<DATE_DEC>2016-06-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>387531</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ODENT, POULET ; SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Guillaume Leforestier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Laurence Marion</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:387531.20160616</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire complémentaire, enregistrés les 30 janvier et 13 avril 2015 au secrétariat du contentieux du Conseil d'Etat, la commune de Châteaufort demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté du 1er août 2014 du ministre des finances et des comptes publics, de la ministre du logement et de l'égalité des territoires et du secrétaire d'Etat chargé du budget pris en application de l'article R. 304-1 du code de la construction et de l'habitation, en tant qu'il classe la commune de Châteaufort en zone B1, ainsi que la décision ministérielle du 3 décembre 2014 rejetant son recours gracieux ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - le code de la construction et de l'habitation ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Guillaume Leforestier, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Laurence Marion, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Odent, Poulet, avocat de la commune de Châteaufort et à la SCP Piwnica, Molinié, avocat de la ministre du logement et de l'habitat durable ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant, d'une part, qu'aux termes de l'article L. 311-1 du code de justice administrative : " Les tribunaux administratifs sont, en premier ressort, juges de droit commun du contentieux administratif, sous réserve des compétences que l'objet du litige ou l'intérêt d'une bonne administration de la justice conduisent à attribuer à une autre juridiction administrative " ; qu'aux termes de l'article R. 311-1 du même code : " Le Conseil d'Etat est compétent pour connaître en premier et dernier ressort : (...) 2° Des recours dirigés contre les actes réglementaires des ministres ainsi que contre les actes des ministres qui ne peuvent être pris qu'après avis du Conseil d'Etat " ; qu'aux termes du premier alinéa de l'article R. 312-1 du même code : " Lorsqu'il n'en est pas disposé autrement par les dispositions de la section 2 du présent chapitre ou par un texte spécial, le tribunal administratif territorialement compétent est celui dans le ressort duquel a légalement son siège l'autorité qui, soit en vertu de son pouvoir propre, soit par délégation, a pris la décision attaquée ou a signé le contrat litigieux (...) " ; que l'article R. 312-7 de ce code dispose que : " Les litiges relatifs aux déclarations d'utilité publique, au domaine public, aux affectations d'immeubles, au remembrement, à l'urbanisme et à l'habitation, au permis de construire, d'aménager ou de démolir, au classement des monuments et des sites et, de manière générale, aux décisions concernant des immeubles relèvent de la compétence du tribunal administratif dans le ressort duquel se trouvent les immeubles faisant l'objet du litige " ;<br/>
<br/>
              2. Considérant, d'autre part, qu'aux termes de l'article R. 304-1 du code de la construction et de l'habitation : " Pour l'application de certaines aides au logement, un arrêté des ministres chargés du logement et du budget, révisé au moins tous les trois ans, établit un classement des communes du territoire national en zones géographiques en fonction du déséquilibre entre l'offre et de la demande de logements. / Ces zones sont désignées, par ordre de déséquilibre décroissant, sous les lettres A bis, A, B1, B2 et C. La zone A bis est incluse dans la zone A, les zones B1 et B2 forment la zone B " ;<br/>
<br/>
              3. Considérant qu'un arrêté pris par les ministres chargés du logement et du budget en application de l'article R. 304-1 du code de la construction et de l'habitation, qui se borne à classer les communes dans différentes zones en fonction du déséquilibre entre l'offre et la demande de logements, ne revêt pas un caractère réglementaire ; qu'une demande d'annulation d'un tel arrêté ne relève d'aucune des catégories de recours dont il appartient au Conseil d'Etat de connaître en premier et dernier ressort en vertu des dispositions de l'article R. 311-1 du code de justice administrative ; <br/>
<br/>
              4. Considérant que la requête de la commune de Châteaufort tend à l'annulation de l'arrêté du 1er août 2014 en tant qu'il classe cette commune en zone B1 ; que cette requête, qui soulève un litige qui est au nombre de ceux que mentionne l'article R. 312-7 du code de justice administrative, ressortit, en vertu des dispositions de cet article, à la compétence de premier ressort du tribunal administratif de Versailles, dans le ressort duquel la commune est située ; <br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède qu'il y a lieu d'attribuer le jugement de la requête de la commune de Châteaufort au tribunal administratif de Versailles ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement de la requête de la commune de Châteaufort est attribué au tribunal administratif de Versailles.<br/>
<br/>
Article 2 : La présente décision sera notifiée à la commune de Châteaufort et à la ministre du logement et de l'habitat durable.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">17-05-01-02 COMPÉTENCE. COMPÉTENCE À L'INTÉRIEUR DE LA JURIDICTION ADMINISTRATIVE. COMPÉTENCE EN PREMIER RESSORT DES TRIBUNAUX ADMINISTRATIFS. COMPÉTENCE TERRITORIALE. - CONTESTATION D'UN ARRÊTÉ MINISTÉRIEL CLASSANT LES COMMUNES DANS DIFFÉRENTES ZONES EN FONCTION DU DÉSÉQUILIBRE ENTRE L'OFFRE ET LA DEMANDE DE LOGEMENTS - APPLICATION DE L'ART. R. 312-7 DU CJA (LITIGES RELATIFS AUX IMMEUBLES).
</SCT>
<ANA ID="9A"> 17-05-01-02 Arrêté ministériel classant les communes dans différentes zones en fonction du déséquilibre entre l'offre et de la demande de logements. Requête d'une commune tendant à l'annulation de l'arrêté en tant qu'il la classe dans une zone.... ,,Cette requête soulève un litige qui est au nombre de ceux que mentionne l'article R. 312-7 du code de justice administrative. Elle ressortit à la compétence de premier ressort du tribunal administratif dans le ressort duquel la commune est située.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
