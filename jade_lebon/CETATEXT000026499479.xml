<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026499479</ID>
<ANCIEN_ID>JG_L_2012_10_000000352770</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/49/94/CETATEXT000026499479.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 4ème sous-sections réunies, 10/10/2012, 352770</TITRE>
<DATE_DEC>2012-10-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>352770</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 4ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELAPORTE, BRIARD, TRICHET</AVOCATS>
<RAPPORTEUR>M. Gérald Bégranger</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:352770.20121010</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 19 septembre et 19 décembre 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour Paris Habitat OPH, dont le siège est au 21 bis rue Claude Bernard à Paris (75253) ; Paris Habitat OPH demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement n° 0809125/4 du 30 juin 2011 par lequel le tribunal administratif de Melun a rejeté sa demande tendant à la condamnation de l'Etat à lui verser, d'une part, une indemnité de 14 175,22 euros, assortie des intérêts au taux légal à compter du 17 octobre 2007, d'autre part, une indemnité de 1 417,52 euros, en réparation des préjudices résultant pour lui, pendant la période du 1er avril 2003 au 10 juin 2005, du refus du préfet du Val-de-Marne de lui accorder le concours de la force publique pour l'exécution d'un jugement du tribunal d'instance de Boissy-Saint-Léger du 15 février 2001 ordonnant l'expulsion des occupants d'un logement situé 4 place de la Sapinière à Boissy-Saint-Léger ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement d'une somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code rural ;<br/>
<br/>
              Vu la loi n° 91-650 du 9 juillet 1991 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Gérald Bégranger, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les observations de la SCP Delaporte, Briard, Trichet, avocat de Paris Habitat OPH ;<br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Delaporte, Briard, Trichet, avocat de Paris Habitat OPH ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la société " Les HLM Réunies ", devenue ultérieurement la société Aximo, a demandé le concours de la force publique le 22 mai 2001 pour l'exécution d'un jugement du 15 février 2001 du tribunal d'instance de Boissy-Saint-Léger ordonnant l'expulsion des occupants d'un logement situé 4 place de la Sapinière à Boissy-Saint-Léger dont elle était propriétaire ; que le préfet du Val-de-Marne a implicitement refusé de faire droit à sa demande ; que, par lettre du 15 octobre 2007, l'Office Public d'Aménagement et de Construction (OPAC) de Paris, devenu ultérieurement Paris Habitat OPH, a demandé au préfet du Val-de-Marne de lui verser une indemnité en réparation du préjudice résultant pour lui de cette décision de refus, en faisant valoir les droits qu'il tirait du bail emphytéotique, joint à sa demande d'indemnité, qu'il avait conclu le 21 octobre 2004 avec la société Aximo, propriétaire du logement ; que Paris Habitat OPH se pourvoit en cassation contre le jugement du 30 juin 2011 par lequel le tribunal administratif de Melun a rejeté sa demande tendant à la condamnation de l'Etat à lui verser une indemnité en réparation de ce préjudice ;<br/>
<br/>
              2. Considérant que la responsabilité de l'Etat née du refus de prêter le concours de la force publique pour assurer l'exécution d'une décision de justice peut être engagée à l'égard de la personne au profit de laquelle a été rendue cette décision ou de la personne investie ultérieurement de ses droits ; qu'un bail emphytéotique consenti par un propriétaire au profit duquel a été rendue une décision de l'autorité judiciaire ordonnant l'expulsion des occupants d'un immeuble, est de nature à investir le preneur des droits que le propriétaire tient de cette décision, sous réserve des stipulations contractuelles contraires ;<br/>
<br/>
              3. Considérant que, pour rejeter la demande de Paris Habitat OPH, le tribunal administratif de Melun s'est fondé sur la seule circonstance que le demandeur n'était pas le propriétaire du logement, au profit duquel avait été rendu le jugement du 15 février 2001 du tribunal d'instance de Boissy-Saint-Léger, sans rechercher si le bail emphytéotique conclu le 21 octobre 2004 avec ce propriétaire, sur lequel Paris Habitat OPH fondait sa demande d'indemnité, avait investi celui-ci des droits du propriétaire ; qu'il n'a pas, ainsi, justifié légalement sa décision ; que, par suite, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, Paris Habitat OPH est fondé à demander l'annulation du jugement qu'il attaque ;<br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à Paris Habitat OPH en application des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Melun du 30 juin 2011 est annulé.<br/>
<br/>
		Article 2 : L'affaire est renvoyée au tribunal administratif de Melun.<br/>
<br/>
Article 3 : L'Etat versera à Paris Habitat OPH la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à Paris Habitat OPH et au ministre de l'intérieur. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">37-05-01 JURIDICTIONS ADMINISTRATIVES ET JUDICIAIRES. EXÉCUTION DES JUGEMENTS. CONCOURS DE LA FORCE PUBLIQUE. - REFUS DE CONCOURS DE LA FORCE PUBLIQUE - POSSIBILITÉ POUR LE TITULAIRE D'UN BAIL EMPHYTÉOTIQUE D'ENGAGER LA RESPONSABILITÉ DE L'ETAT - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">60-02-03-01-03 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RESPONSABILITÉ EN RAISON DES DIFFÉRENTES ACTIVITÉS DES SERVICES PUBLICS. SERVICES DE POLICE. SERVICES DE L'ETAT. EXÉCUTION DES DÉCISIONS DE JUSTICE. - REFUS DE CONCOURS DE LA FORCE PUBLIQUE - POSSIBILITÉ POUR LE TITULAIRE D'UN BAIL EMPHYTÉOTIQUE D'ENGAGER LA RESPONSABILITÉ DE L'ETAT - EXISTENCE.
</SCT>
<ANA ID="9A"> 37-05-01 La responsabilité de l'Etat née du refus de prêter le concours de la force publique pour assurer l'exécution d'une décision de justice peut être engagée à l'égard de la personne au profit de laquelle a été rendue cette décision ou de la personne investie ultérieurement de ses droits. Un bail emphytéotique consenti par un propriétaire au profit duquel a été rendue une décision de l'autorité judiciaire ordonnant l'expulsion des occupants d'un immeuble est de nature à investir le preneur des droits que le propriétaire tient de cette décision, sous réserve des stipulations contractuelles contraires.</ANA>
<ANA ID="9B"> 60-02-03-01-03 La responsabilité de l'Etat née du refus de prêter le concours de la force publique pour assurer l'exécution d'une décision de justice peut être engagée à l'égard de la personne au profit de laquelle a été rendue cette décision ou de la personne investie ultérieurement de ses droits. Un bail emphytéotique consenti par un propriétaire au profit duquel a été rendue une décision de l'autorité judiciaire ordonnant l'expulsion des occupants d'un immeuble est de nature à investir le preneur des droits que le propriétaire tient de cette décision, sous réserve des stipulations contractuelles contraires.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
