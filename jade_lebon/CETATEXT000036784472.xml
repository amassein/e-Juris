<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036784472</ID>
<ANCIEN_ID>JG_L_2018_04_000000402065</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/78/44/CETATEXT000036784472.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 06/04/2018, 402065</TITRE>
<DATE_DEC>2018-04-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>402065</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP COUTARD, MUNIER-APAIRE</AVOCATS>
<RAPPORTEUR>M. Paul-François Schira</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:402065.20180406</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              1° Sous le numéro 402065, par une requête sommaire et trois mémoires complémentaires, enregistrés le 2 août 2016, le 2 novembre 2016, le 7 août 2017 et le 9 mars 2018 au secrétariat du contentieux du Conseil d'Etat, Madame D...B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2016-736 du 2 juin 2016 portant classement au titre des monuments historiques d'un ensemble d'objets mobiliers conservés au château de Craon à  Haroué (Meurthe-et-Moselle) ; <br/>
<br/>
              2°) de mettre à la charge de  l'Etat la somme de  6 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              2° Sous le numéro 415575, par une ordonnance n° 1512615 du 17 octobre 2017, enregistrée le 8 novembre 2017 au secrétariat du contentieux du Conseil d'Etat, le président du tribunal administratif de Paris a transmis au Conseil d'Etat, en application des articles R. 341-1 et R. 341-2 du code de justice administrative, la requête et 4 nouveaux mémoires, enregistrés les 23 juillet 2015, 12 juillet 2016 et les 3 et 11 octobre 2017 au greffe de ce tribunal, présentée par Mme B.... Par cette requête, et deux nouveaux mémoires enregistrés les 16 et 19 mars 2018 au secrétariat du contentieux du Conseil d'Etat, Mme D...B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler, pour excès de pouvoir, la décision du 5 juin 2015 par laquelle le directeur général des patrimoines a placé sous le régime de l'instance de classement au titre des monuments historiques un ensemble d'objets mobiliers conservés au château de Craon à Haroué (Meurthe-et-Moselle) et inscrits au catalogue d'une vente qui était prévue le 15 juin 2015 ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 10 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu  les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, et notamment son Préambule ;<br/>
              - le traité sur le fonctionnement de l'Union européenne ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés   fondamentales ;<br/>
              - le règlement n° 116/2009/CEE du Conseil du 18 décembre 2008 ;<br/>
              - le code du patrimoine ;<br/>
              - le code des relations entre le public et l'administration ;<br/>
              - le décret n° 2005-850 du 27 juillet 2005 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Paul-François Schira, rapporteur,  <br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Coutard, Munier-Apaire, avocat de Mme B...;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 21 mars 2018, présentée par la requérante ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Sous le n° 402065, Mme B...demande l'annulation pour excès de pouvoir du décret du 2 juin 2016 classant au titre des monuments historiques un ensemble d'objets mobiliers conservés au château de Craon à Haroué. <br/>
<br/>
              2. Sous le numéro n° 415575, Mme B...demande l'annulation pour excès de pouvoir de la décision du 5 juin 2015 plaçant sous le régime de l'instance de classement au titre des monuments historiques un ensemble d'objets mobiliers conservés au château de Craon à Haroué. Le Conseil d'Etat est compétent pour connaître en premier et dernier ressort de cette requête au titre du lien de connexité qu'elle présente avec celle dirigée contre le décret du 2 juin 2016.<br/>
<br/>
              3. Il y a lieu de joindre les deux requêtes qui présentent à juger des questions semblables pour statuer par une seule décision.<br/>
<br/>
              Sur les désistements partiels :<br/>
<br/>
              4. Les désistements de Mme B...de ses conclusions tendant, d'une part, à l'annulation de la décision du 5 juin 2015 en tant qu'elle place sous le régime de classement au titre des monuments historiques les meubles acquis par l'Etat par le contrat de vente des 26 et 31 octobre 2016 et, d'autre part, à l'annulation du décret du 2 juin 2016 en tant qu'il classe ces mêmes meubles au titre des monuments historiques, sont purs et simples. Rien ne s'oppose à ce qu'il en soit donné acte.<br/>
<br/>
              Sur la légalité externe de la décision du 5 juin 2015 :<br/>
<br/>
              5. Il résulte des dispositions de l'article 1er du décret du 27 juillet 2005 relatif aux délégations de signature des membres du Gouvernement que les directeurs d'administration centrale peuvent signer, au nom du ministre et par délégation, l'ensemble des actes, à l'exception des décrets, relatifs aux services placés sous leur autorité. Il suit de là que le moyen tiré de l'incompétence du directeur général des patrimoines pour signer la décision portant ouverture d'une instance de classement au titre des monuments historiques au nom du ministre chargé de la culture doit être écarté.<br/>
<br/>
              Sur la légalité externe du décret du 2 juin 2016 :                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          <br/>
<br/>
              6. Aux termes de l'article L. 622-4 du code du patrimoine : " Les objets mobiliers appartenant à une personne privée peuvent être classés au titre des monuments historiques, avec le consentement du propriétaire, par décision de l'autorité administrative / A défaut de consentement du propriétaire, le classement d'office est prononcé par un décret en Conseil d'Etat pris après avis de la Commission nationale des monuments historiques". Aux termes du troisième alinéa de l'article R. 622-4 du même code : " Lorsque le ministre chargé de la culture est saisi par le préfet d'une demande ou d'une proposition de classement, il statue après avoir recueilli l'avis de la Commission nationale des monuments historiques. Il consulte également la Commission nationale des monuments historiques lorsqu'il prend l'initiative d'un classement. Il informe la commission, avant qu'elle ne rende son avis, de l'avis du propriétaire ou de l'affectataire domanial sur la proposition de classement". Aux termes de l'article L. 122-1 du code des relations entre le public et l'administration : " Les décisions mentionnées à l'article L. 211-2 n'interviennent qu'après que la personne intéressée a été mise à même de présenter des observations écrites et, le cas échéant, sur sa demande, des observations orales. Cette personne peut se faire assister par un conseil ou représenter par un mandataire de son choix." Aux termes de l'article R. 133-10 du même code : " Le quorum est atteint lorsque la moitié au moins des membres composant la commission sont présents, y compris les membres prenant part aux débats au moyen d'une conférence téléphonique ou audiovisuelle, ou ont donné mandat ". Aux termes de l'arrêté du 7 septembre 2012 portant règlement intérieur de la commission nationale des monuments historiques " la convocation aux réunions de chaque section et aux réunions du comité des sections est adressée, avec l'ordre du jour, aux membres titulaires par les mêmes moyens, 15 jours au moins avant la date de chaque séance ". <br/>
<br/>
              7. D'une part, il ressort des pièces du dossier et notamment du procès-verbal de la séance du 1er avril 2016 de la commission nationale des monuments historiques, que les membres ont été régulièrement convoqués le 17 mars 2016, que plus de la moitié de ces derniers étaient présents et que la commission était informée du sens de l'avis de Mme B...sur la procédure de classement dont le ministre chargé de la culture n'était pas tenu de préciser les motifs. Il s'ensuit que le moyen tiré de l'irrégularité de la procédure préalable à l'édiction du décret attaqué doit être écarté. D'autre part, la requérante a été informée le 2 mai 2016 de l'avis de la commission, lorsque son accord au classement a été sollicité par le ministre chargé de la culture et a été mise à même de faire valoir ses observations par un courrier de son conseil, en date du 17 mai 2016. Il suit de là que le moyen tiré de ce que le décret attaqué n'aurait pas été précédé d'un temps utile pour permettre à la requérante de faire valoir ses observations manque, en tout état de cause, en fait. Il en va de même du moyen tiré de ce que le décret attaqué serait insuffisamment motivé.<br/>
<br/>
<br/>
              Sur la légalité interne des actes attaqués :      <br/>
<br/>
              En ce qui concerne le moyen tiré de l'inexactitude matérielle :<br/>
<br/>
              8. La décision du 5 juin 2015 et le décret du 2 juin 2016 visent un ensemble d'objets mobiliers conservés au château de Craon à Haroué (Meurthe-et-Moselle) parmi lesquels figure le portrait de Mme C...A...peint par François Gérard. Il s'ensuit que le moyen tiré de ce que les actes attaqués auraient inexactement rattaché ce portrait à un ensemble du mobilier du château de Saint-Ouen manque en fait.<br/>
<br/>
              En ce qui concerne les moyens tirés de l'impossibilité de classer au titre des monuments historiques les biens bénéficiant de certificats d'exportation :                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         <br/>
<br/>
              9. Aux termes de l'article L. 111-1 du code du patrimoine, dans sa version applicable au litige : " Les biens appartenant aux collections publiques et aux collections des musées de France, les biens classés en application des dispositions relatives aux monuments historiques et aux archives, ainsi que les autres biens qui présentent un intérêt majeur pour le patrimoine national au point de vue de l'histoire, de l'art ou de l'archéologie sont considérés comme trésors nationaux ". Aux termes de l'article L. 111-2 du code du patrimoine : " L'exportation temporaire ou définitive hors du territoire douanier des biens culturels, autres que les trésors nationaux, qui présentent un intérêt historique, artistique ou archéologique et entrent dans l'une des catégories définies par décret en Conseil d'Etat est subordonnée à l'obtention d'un certificat délivré par l'autorité administrative. Ce certificat atteste à titre permanent que le bien n'a pas le caractère de trésor national ". Aux termes de l'article L. 111-6 du même code : " En cas de refus du certificat, toute demande nouvelle pour le même bien est irrecevable pendant une durée de trente mois à compter de la date du refus. (...) Après ce délai, le refus de délivrance du certificat ne peut être renouvelé que dans le cas prévu pour la procédure d'offre d'achat au sixième alinéa de l'article L. 121-1, sans préjudice de la possibilité de classement du bien en application des dispositions relatives aux monuments historiques ou aux archives, ou de sa revendication par l'Etat en application des dispositions relatives aux fouilles archéologiques ou aux biens culturels maritimes. (...) Les demandes de certificat sont également irrecevables en cas d'offre d'achat du bien par l'Etat dans les conditions prévues à l'article L. 121-1, jusqu'à l'expiration des délais prévus aux cinquième, sixième et septième alinéas du même article ". Aux termes du dernier alinéa de l'article L. 622-4 du même code : " Le classement pourra donner lieu au paiement d'une indemnité représentative du préjudice résultant pour le propriétaire de l'application de la servitude de classement d'office. La demande d'indemnité devra être produite dans les six mois à dater de la notification du décret de classement. A défaut d'accord amiable, l'indemnité est fixée, selon le montant de la demande, par le tribunal d'instance ou de grande instance. " Aux termes de l'article L. 622-5 du même code : " Lorsque la conservation ou le maintien sur le territoire national d'un objet mobilier est menacée, l'autorité administrative peut notifier au propriétaire par décision sans formalité préalable une instance de classement au titre des monuments historiques. (...) A compter du jour où l'autorité administrative notifie au propriétaire une instance de classement au titre des monuments historiques, tous les effets du classement s'appliquent de plein droit à l'objet mobilier visé. Ils cessent de s'appliquer si la décision de classement n'intervient pas dans les douze mois de cette notification ".<br/>
<br/>
              10. Il résulte de ces dispositions combinées que la délivrance d'un certificat d'exportation de bien culturel ne fait obstacle ni au classement ultérieur de ce même bien au titre des dispositions relatives aux monuments historiques, ni à ce qu'il soit qualifié, à ce titre, de " trésor national ". Il suit de là que la requérante n'est pas fondée à soutenir que l'existence de certificats d'exportation faisait obstacle au classement et à la qualification, à ce titre, de " trésor national " de l'ensemble d'objets mobiliers conservés au château de Craon à Haroué concernés par les décisions attaquées. Doivent être écartés, par voie de conséquence, les moyens tirés de ce que l'administration aurait entaché les actes attaqués de détournement de procédure et d'une atteinte au principe de sécurité juridique.<br/>
<br/>
              En ce qui concerne le moyen tiré de l'inexacte qualification juridique dont serait entaché le décret du 2 juin 2016 :<br/>
<br/>
              11. Aux termes du premier alinéa de l'article L. 622-1 du code du patrimoine : " Les objets mobiliers, soit meubles proprement dits, soit immeubles par destination, dont la conservation présente, au point de vue de l'histoire, de l'art, de la science ou de la technique, un intérêt public peuvent être classés au titre des monuments historiques par décision de l'autorité administrative ". <br/>
<br/>
              12. Il ressort des pièces du dossier que le portrait de Mme C...A...peint par François Gérard classé par le décret attaqué constitue une toile unique du baron Gérard, premier peintre du roi, et fait partie d'un ensemble mobilier remarquable, cohérent, signé et jamais dispersé du mobilier de la Restauration issu d'une commande du roi Louis XVIII en faveur de la comtesse du Cayla, pour l'ameublement du château de Saint-Ouen. Il suit de là que le ministre chargé de la culture n'a pas inexactement qualifié les faits en estimant que l'ensemble mobilier classé par le décret attaqué présente un intérêt public au sens des dispositions précitées.  <br/>
<br/>
              En ce qui concerne le moyen tiré de l'atteinte excessive au droit de propriété des décisions attaquées :<br/>
<br/>
              13. Aux termes de l'article 17 de la Déclaration des droits de l'homme et du citoyen : " La propriété étant un droit inviolable et sacré, nul ne peut en être privé, si ce n'est lorsque la nécessité publique, légalement constatée, l'exige évidemment, et sous la condition d'une juste et préalable indemnité ". Aux termes de l'article 1er du premier protocole additionnel à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales : " Toute personne physique ou morale a droit au respect de ses biens. Nul ne peut être privé de sa propriété que pour cause d'utilité publique et dans les conditions prévues par la loi et les principes généraux du droit international ". <br/>
<br/>
              14. Si les actes attaqués limitent le droit de propriété de la requérante, privée, temporairement à compter de la notification de la décision du 5 juin 2015, puis définitivement à compter de la publication du décret du 2 juin 2016, de la possibilité de faire valoir le certificat d'exportation de bien culturel qui lui avait été délivré le 8 février 2007, ces limitations sont toutefois justifiées par l'objectif d'intérêt général de conservation du patrimoine national. Il résulte des dispositions citées au point 9 que ces limitations, qui pourront donner lieu au paiement d'une indemnité représentative du préjudice résultant pour le propriétaire de l'application de la servitude du classement d'office, sont proportionnées à cet objectif. Par suite, eu égard à l'intérêt public qui s'attache à la conservation du portrait de Mme C...A...peint par François Gérard, et sans que n'y fassent obstacle les dispositions combinées des articles 34, 35 et 36 du traité sur le fonctionnement de l'Union européenne et du règlement 116/2009/CEE du Conseil du 18 décembre 2008 relatives aux restrictions à la libre circulation des biens culturels, et notamment des trésors nationaux, le moyen tiré d'une atteinte excessive au droit de propriété et à ses attributs, dont le principe de libre circulation des marchandises, ne peut qu'être écarté.<br/>
<br/>
              15. Il résulte de tout ce qui précède que Mme B...n'est pas fondée à demander l'annulation de la décision du 5 juin 2015 ni du décret du 2 juin 2016 en tant qu'ils portent respectivement sur le placement sous le régime de l'instance de classement au titre des monuments historiques du portrait de Mme C...A...peint par François Gérard et sur le classement au titre des monuments historiques de ce même portrait. <br/>
<br/>
              16. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans les présentes instances, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il est donné acte du désistement de Mme B...de ses conclusions tendant, d'une part, à l'annulation de la décision du 5 juin 2015 du directeur général des patrimoines portant ouverture d'une instance de classement au titre des monuments historiques en ce qui concerne les meubles acquis par l'Etat par le contrat de vente des 26 et 31 octobre 2016 et, d'autre part, à l'annulation du décret du 2 juin 2016 portant classement au titre des monuments historiques d'un ensemble d'objets mobiliers conservés au château de Craon à Haroué (Meurthe-et-Moselle) en ce qui concerne les meubles acquis par l'Etat par ce même contrat de vente des 26 et 31 octobre 2016.<br/>
Article 2 : Le surplus des conclusions des requêtes n° 402065 et n° 415575 est rejeté.<br/>
Article 3 : La présente décision sera notifiée à Mme D...B..., au Premier ministre et à la ministre de la culture.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">09 ARTS ET LETTRES. - DÉLIVRANCE D'UN CERTIFICAT D'EXPORTATION D'UN BIEN CULTUREL - CIRCONSTANCE FAISANT OBSTACLE AU CLASSEMENT ULTÉRIEUR DE CE MÊME BIEN AU TITRE DES DISPOSITIONS RELATIVES AUX MONUMENTS HISTORIQUES ET À SA QUALIFICATION À CE TITRE DE TRÉSOR NATIONAL - ABSENCE.
</SCT>
<ANA ID="9A"> 09 Il résulte des dispositions combinées des articles L. 111-1, L. 111-2, L. 111-6, L. 622-4 et L. 622-5 du code du patrimoine que la délivrance d'un certificat d'exportation de bien culturel ne fait obstacle ni au classement ultérieur de ce même bien au titre des dispositions relatives aux monuments historiques, ni à ce qu'il soit qualifié, à ce titre, de  trésor national .</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
