<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039498389</ID>
<ANCIEN_ID>JG_L_2019_12_000000424219</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/49/83/CETATEXT000039498389.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 11/12/2019, 424219</TITRE>
<DATE_DEC>2019-12-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>424219</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MARLANGE, DE LA BURGADE</AVOCATS>
<RAPPORTEUR>M. Paul Bernard</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie Roussel</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:424219.20191211</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B... A... a demandé à l'Office français de protection des réfugiés et apatrides (OFPRA) de lui reconnaître la qualité de réfugié ou, à défaut, de lui accorder le bénéfice de la protection subsidiaire. <br/>
<br/>
              Par une décision du 18 octobre 2017, le directeur général de l'Office a rejeté sa demande. <br/>
<br/>
              Par une décision n° 18000824 du 14 mai 2018, la Cour nationale du droit d'asile a rejeté l'appel de M. A... contre cette décision. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 17 septembre et 10 décembre 2018 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette décision ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
              3°) de mettre à la charge de l'OFPRA la somme de 3 000 euros à verser à la SCP Marlange-de La Burgade, son avocat, au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention de Genève du 28 juillet 1951 et le protocole signé à New York le 31 janvier 1967 relatifs au statut des réfugiés ; <br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ; <br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Paul Bernard, maître des requêtes, <br/>
<br/>
              - les conclusions de Mme Sophie Roussel, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Marlange, de La Burgade, avocat de M. A... ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1.	Il ressort des pièces du dossier soumis aux juges du fond que M. A..., né le 4 janvier 1993 en Afghanistan, a demandé que lui soit reconnu le statut de réfugié ou, à défaut, accordé le bénéfice de la protection subsidiaire. Par une décision du 18 octobre 2017, le directeur général de l'OFPRA a rejeté sa demande. Par une décision du 14 mai 2018, contre laquelle M. A... se pourvoit en cassation, la Cour nationale du droit d'asile a rejeté sa demande d'annulation de cette décision. <br/>
<br/>
              2.	En premier lieu, il ressort de l'examen de la minute de la décision attaquée que celle-ci a été signée, conformément aux dispositions de l'article R. 733-30 du code de l'entrée et du séjour des étrangers et du droit d'asile, par le président de la formation de jugement qui a rendu la décision et par un chef de service de la cour. <br/>
<br/>
              3.	En deuxième lieu, aux termes de l'article 1er A 2° de la convention de Genève du 28 juillet 1951 sur le statut des réfugiés, modifiée par l'article 1er 2 du protocole signé le 31 janvier 1967, la qualité de réfugié est notamment reconnue à : " Toute personne (. . .) qui, craignant avec raison d'être persécutée du fait de sa race, de sa religion, de sa nationalité, de son appartenance à un certain groupe social ou de ses opinions politiques, se trouve hors du pays dont elle a la nationalité et qui ne peut, ou, du fait de cette crainte, ne veut se réclamer de la protection de ce pays ". Selon l'article L. 712-1 du code de l'entrée et du séjour des étrangers et du droit d'asile : " Le bénéfice de la protection subsidiaire est accordé à toute personne qui ne remplit pas les conditions pour se voir reconnaître la qualité de réfugié et pour laquelle il existe des motifs sérieux et avérés de croire qu'elle courrait dans son pays un risque réel de subir l'une des atteintes graves suivantes : / a) La peine de mort ou une exécution ; / b) La torture ou des peines ou traitements inhumains ou dégradants ; / c) S'agissant d'un civil, une menace grave et individuelle contre sa vie ou sa personne en raison d'une violence qui peut s'étendre à des personnes sans considération de leur situation personnelle et résultant d'une situation de conflit armé interne ou international. ". Il résulte de ces dernières dispositions du c) de l'article L. 712-1 que l'existence d'une menace grave, directe et individuelle contre la vie ou la personne d'un demandeur à la protection subsidiaire n'est pas subordonnée à la condition qu'il rapporte la preuve qu'il est visé spécifiquement en raison d'éléments propres à sa situation personnelle dès lors que le degré de violence généralisée caractérisant le conflit armé atteint un niveau si élevé qu'il existe des motifs sérieux et avérés de croire qu'un civil renvoyé dans le pays ou la région concernés courrait, du seul fait de sa présence sur le territoire, un risque réel de subir lesdites menaces. Toutefois, il en résulte également que la protection qu'elles prévoient n'a vocation à s'appliquer qu'aux civils.<br/>
<br/>
              4.	Pour rejeter la demande de M. B... A..., la Cour nationale du droit d'asile, s'est fondée, d'une part, sur le caractère faiblement circonstancié de son récit sur les persécutions et menaces dont il dit avoir été victime. En ne mentionnant pas le certificat médical délivré par le service de psychiatrie du centre hospitalier universitaire de Saint-Etienne se bornant à décrire les symptômes psychiatriques affectant M. A..., elle n'a ni insuffisamment motivé sa décision, ni commis erreur de droit. La cour a, d'autre part, jugé, sans commettre d'erreur de droit, que l'appartenance au corps de la police nationale afghane, alors même qu'il ne s'agit pas d'une force militaire, interdisait de considérer l'un de ses membres comme un civil pour l'application des dispositions précitées du c) de l'article L. 712-1 du code de l'entrée et du séjour des étrangers et du droit d'asile.  Enfin en estimant que M. A... ne pouvait être regardé comme ayant démissionné de ce corps par son seul départ d'Afghanistan et en l'absence d'acte formel en ce sens, la cour a porté sur les faits qui lui étaient soumis une appréciation souveraine exempte de dénaturation et a pu par suite en déduire, sans inexacte appréciation des pièces du dossier, que l'intéressé ne pouvait bénéficier de l'application des dispositions du c) de l'article L. 712-1 du code de l'entrée et du séjour des étrangers et du droit d'asile.<br/>
<br/>
              5.	Il résulte de tout ce qui précède que M. A... n'est pas fondé à demander l'annulation de la décision qu'il attaque. <br/>
<br/>
              6.	Les dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 font obstacle ce que soit mis à la charge de l'OFPRA qui n'est pas, dans la présente instance, la partie perdante, une somme à ce titre. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le pourvoi de M. A... est rejeté. <br/>
<br/>
Article 2 : La présente décision sera notifiée à M. B... A... et à l'Office français de protection des réfugiés et apatrides. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">095-03-01-03 - OCTROI AU TITRE DU C) DE L'ARTICLE L. 712-1 DU CESEDA - CHAMP D'APPLICATION - 1) OCTROI RÉSERVÉ AUX CIVILS - 2) ILLUSTRATION - MEMBRE DE LA POLICE NATIONALE AFGHANE - EXCLUSION, ALORS MÊME QU'IL NE S'AGIT PAS D'UNE FORCE MILITAIRE [RJ1].
</SCT>
<ANA ID="9A"> 095-03-01-03 1) Il résulte du c) de l'article L 712-1 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA) que la protection subsidiaire qu'elles prévoient n'a vocation à s'appliquer qu'aux civils.,,,2) La Cour nationale du droit d'asile (CNDA) a jugé, sans commettre d'erreur de droit, que l'appartenance au corps de la police nationale afghane, alors même qu'il ne s'agit pas d'une force militaire, interdisait de considérer l'un de ses membres comme un civil pour l'application du c) de l'article L. 712-1 du CESEDA.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., s'agissant d'un membre d'une unité de la police locale afghane, CE, décision du même jour, Office français de protection des réfugiés et apatrides, n° 427714, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
