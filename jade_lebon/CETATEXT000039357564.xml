<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039357564</ID>
<ANCIEN_ID>JG_L_2019_11_000000421867</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/35/75/CETATEXT000039357564.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 08/11/2019, 421867</TITRE>
<DATE_DEC>2019-11-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>421867</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GASCHIGNARD ; SCP BOULLOCHE</AVOCATS>
<RAPPORTEUR>M. Laurent Domingo</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:421867.20191108</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A... B... a demandé au tribunal administratif de Rennes d'annuler l'arrêté du 27 novembre 2009 par lequel le préfet du Finistère a autorisé le président de la fondation Pierre C... à conclure avec la commune de Plouescat un bail emphytéotique d'une durée de trente ans pour la location de bâtiments situés dans cette commune. Par un jugement n°1000776 du 8 mars 2013, ce tribunal a rejeté cette demande.<br/>
<br/>
              Par un arrêt n° 13NT01289 du 30 juin 2015, la cour administrative d'appel de Nantes a rejeté l'appel formé par M. B... contre ce jugement. <br/>
<br/>
              Par une décision n° 393448 du 29 mai 2017, le Conseil d'Etat a annulé cet arrêt du 30 juin 2015 et a renvoyé l'affaire devant la cour administrative d'appel de Nantes.<br/>
<br/>
              Par un arrêt n° 17NT01772 du 27 avril 2018, la cour administrative d'appel de Nantes, statuant après renvoi, a annulé le jugement du tribunal administratif de Rennes et rejeté la demande de M. B.... <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 29 juin et 1er octobre 2018 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'article 2 de cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond dans cette mesure, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code civil ;<br/>
              - le décret n° 2007-807 du 11 mai 2007 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Laurent Domingo, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gaschignard, avocat de M. B... et à la SCP Boulloche, avocat de la société Fondation Pierre C... ;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la fondation Pierre C..., qui est propriétaire à Plouescat de bâtiments qui lui ont été donnés ou légués par M. C..., les a mis gratuitement à disposition de l'association de gestion du lycée professionnel rural privé Pierre C..., lequel y a dispensé un enseignement jusqu'à sa fermeture en juin 2009. Le conseil d'administration de cette fondation a alors approuvé le principe d'une mise à disposition gracieuse de ces locaux à la commune de Plouescat à l'effet d'y accueillir des associations à caractère éducatif ou de loisir, à charge pour la commune de s'acquitter de l'entretien des locaux, de leur mise aux normes, des assurances et des diverses taxes. Un projet de bail emphytéotique a été établi pour une durée de trente ans et soumis à l'autorisation du préfet du Finistère. Cette autorisation a été délivrée par un arrêté du 27 novembre 2009 dont M. A... B... a demandé l'annulation au tribunal administratif de Rennes, qui a rejeté cette demande par un jugement du 8 mars 2013. Par une décision du 29 mai 2017, le Conseil d'Etat, statuant au contentieux a annulé l'arrêt de la cour administrative d'appel de Nantes du 30 juin 2015 qui avait confirmé ce jugement. Statuant après renvoi, la cour administrative d'appel de Nantes a, par un arrêt du 27 avril 2018, annulé le jugement du tribunal administratif de Rennes et rejeté la demande de première instance de M. B... ainsi que le surplus de ses conclusions d'appel. M. B... se pourvoit en cassation contre cet arrêt, en tant seulement qu'il a, par son article 2, rejeté sa demande tendant à l'annulation de l'arrêté du préfet du Finistère du 27 novembre 2009.<br/>
<br/>
              2. D'une part, aux termes de l'article 8 du décret du 11 mai 2007 relatif aux associations, fondations, congrégations et établissements publics du culte et portant application de l'article 910 du code civil : " Lorsque les statuts des associations ou des fondations reconnues d'utilité publique soumettent à autorisation administrative les opérations portant sur les droits réels immobiliers, les emprunts, l'aliénation ou le remploi des biens mobiliers dépendant de la dotation ou du fonds de réserve, cette autorisation est donnée par arrêté du préfet du département où est le siège de l'association ou de la fondation. / L'autorisation est réputée accordée si le préfet n'y a pas fait opposition dans les deux mois de leur notification par l'association ou la fondation ". L'article 9 des statuts de la fondation C..., approuvés par un arrêté du 1er mars 1993, stipule que : " les délibérations du conseils d'administration relatives aux aliénations de biens mobiliers et immobiliers dépendant de la dotation à constitution d'hypothèques et aux emprunts ne sont valables qu'après approbation administrative (...) ". Ces dispositions et stipulations ont pour effet de soumettre à autorisation du préfet les actes de disposition des biens immeubles de la fondation qui confèrent au preneur un droit réel immobilier, notamment les baux emphytéotiques.<br/>
<br/>
              3. D'autre part, aux termes de l'article 900-2 du code civil : " Tout gratifié peut demander que soient révisées en justice les conditions et charges grevant les donations ou legs qu'il a reçus, lorsque, par suite d'un changement de circonstances, l'exécution en est devenue pour lui soit extrêmement difficile, soit sérieusement dommageable ". Aux termes de l'article 900-4 du même code : " Le juge saisi de la demande en révision peut, selon les cas et même d'office, soit réduire en quantité ou périodicité les prestations grevant la libéralité, soit en modifier l'objet en s'inspirant de l'intention du disposant, soit même les regrouper, avec des prestations analogues résultant d'autres libéralités. / Il peut autoriser l'aliénation de tout ou partie des biens faisant l'objet de la libéralité en ordonnant que le prix en sera employé à des fins en rapport avec la volonté du disposant. / Il prescrit les mesures propres à maintenir, autant qu'il est possible, l'appellation que le disposant avait entendu donner à sa libéralité ". Il résulte de la combinaison de ces dispositions que la modification des charges et conditions grevant un bien légué ou l'aliénation de ce bien ne peut avoir lieu que par décision de justice, dans les conditions et selon la procédure définies par les articles 900-2 à 900-8 du code civil.<br/>
<br/>
              4. En jugeant que M. B... ne pouvait utilement soutenir que l'arrêté du 27 novembre 2009 du préfet du Finistère méconnaîtrait les règles du code civil relatives aux conditions dans lesquelles les prestations grevant une libéralité peuvent être modifiées au motif que le bail emphytéotique conclu entre la fondation et la commune de Plouescat ne constitue pas une libéralité, alors qu'il appartient à l'autorité administrative, dès lors que ce bail confère au preneur un droit réel immobilier, de vérifier qu'il n'a pas pour effet de modifier les charges grevant la donation ou le legs en méconnaissance des dispositions des articles 900-2 à 900-8 du code civil, la cour a commis une erreur de droit.<br/>
<br/>
              5. Il résulte de ce qui précède que M. B... est fondé, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, à demander l'annulation de l'article 2 de l'arrêt attaqué.<br/>
<br/>
              6. Aux termes du second alinéa de l'article L. 821-2 du code de justice administrative : " Lorsque l'affaire fait l'objet d'un second pourvoi en cassation, le Conseil d'Etat statue définitivement sur cette affaire ". Le Conseil d'Etat étant saisi, en l'espèce, d'un second pourvoi en cassation, il lui incombe de régler l'affaire au fond et de statuer sur la demande tendant à l'annulation de l'arrêté du préfet du Finistère du 27 novembre 2009.<br/>
<br/>
              7. Il ressort des pièces du dossier que le conseil d'administration de la Fondation Pierre C... a, lors de sa séance du 23 juin 2009, donné son accord à la conclusion d'un contrat de mise à disposition des locaux anciennement occupés par le lycée professionnel rural privé Pierre C... à la commune de Plouescat. <br/>
<br/>
              8. Il ne résulte d'aucune disposition que les avis des ministres chargés de la culture, de l'agriculture et de l'éducation nationale étaient requis avant que le préfet du Finistère prenne l'arrêté en litige.<br/>
<br/>
              9. Il ressort de l'acte du 19 avril 1950, par lequel M. C... a donné à la fondation un des immeubles en litige, que cette donation a eu " pour objet de créer un établissement qui aura pour but de donner à la Paysanne Bretonne une formation morale et professionnelle qui lui permettra de remplir dans le milieu rural où elle est appelée à vivre, ses devoirs de ménagère, de mère de famille, de conseillère et d'aide du chef de l'exploitation. Par suite l'établissement ainsi fondé a une destination bien définie, et les membres du conseil d'administration devront toujours travailler à rester dans l'esprit de la Fondation (...) ". Les locaux en litige ont ainsi, lors de la création de la fondation, hébergé un foyer chargé d'assurer la " formation morale et professionnelle de la paysanne bretonne " puis, alors que la fondation se donnait pour but, aux termes de ses statuts approuvés le 1er mars 1993, " de donner aux jeunes du secteur de Plouescat une formation culturelle, morale et professionnelle qui leur permette de remplir, dans le milieu où ils sont appelés à vivre, leur rôle d'acteur responsable ", ils ont accueilli un lycée professionnel rural privé. Le bail emphytéotique que la fondation a conclu avec la commune de Plouescat stipule, alors que les locaux ne sont plus occupés et qu'il est prévu qu'ils soient utilisés pour des activités éducatives, culturelles et de loisirs à destination des jeunes de la commune, que " le preneur pourra changer la destination des lieux loués ", tout en précisant, ainsi qu'il est dit dans l'acte de donation précité, qu'il devra conserver " l'esprit de Pierre C..., et de la Fondation qu'il avait créée ". Dans ces conditions, le préfet du Finistère, en adoptant l'arrêté attaqué, n'a pas approuvé un contrat conclu en méconnaissance des statuts de la fondation ou ayant pour effet de remettre en cause, sans respecter les dispositions des articles 900-2 à 900-8 du code civil, les charges et conditions grevant une partie des biens qui en sont l'objet. Ce contrat n'a pas non plus pour effet de priver la fondation du patrimoine par lequel elle accomplit sa mission statutaire.<br/>
<br/>
              10. Il résulte de ce qui précède que M. B..., qui ne peut utilement se prévaloir, à l'encontre de l'acte attaqué, de l'intention du donateur autrement exprimée que dans l'acte de donation précité, n'est pas fondé à demander l'annulation de l'arrêté du préfet du Finistère du 27 novembre 2009.<br/>
<br/>
              11. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de M. B... une somme de 3 000 euros à verser à la fondation Pierre C... au titre des dispositions de l'article L. 761-1 du code de justice administrative. Ces dispositions font obstacle à ce que l'Etat, qui n'est pas la partie perdante dans la présente instance, soit condamné à verser à M. B... la somme qu'il réclame au titre des frais exposés par lui et non compris dans les dépens.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'article 2 de l'arrêt du 27 avril 2018 de la cour administrative d'appel de Nantes est annulé.<br/>
Article 2 : La demande présentée par M. B... devant le tribunal administratif de Rennes est rejetée.<br/>
Article 3 : Le surplus des conclusions du pourvoi de M. B... est rejeté.<br/>
Article 4 : M. B... versera une somme de 3 000 euros à la fondation Pierre C... au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 5 : La présente décision sera notifiée à M. A... B..., à la fondation Pierre C... et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">25-02 DONS ET LEGS. DOMAINE ET PROCÉDURE DE L'AUTORISATION. - RECOURS CONTRE UN ACTE ADMINISTRATIF APPROUVANT UN CONTRAT DE BAIL EMPHYTÉOTIQUE PERMETTANT LA CONSTITUTION DE DROITS RÉELS IMMOBILIERS SUR DES BIENS DONNÉS OU LÉGUÉS PAR LE TESTATEUR À UNE FONDATION RECONNUE D'UTILITÉ PUBLIQUE - 1) OBLIGATION POUR L'AUTORITÉ ADMINISTRATIVE DE VÉRIFIER QUE LE BAIL NE MODIFIE PAS LES CHARGES GREVANT LA DONATION OU LE LEGS - EXISTENCE - CONSÉQUENCE - OPÉRANCE DU MOYEN TIRÉ DE LA MÉCONNAISSANCE DES ARTICLES 900-2 À 900-8 DU CODE CIVIL [RJ1] - 2) ESPÈCE - ACTE MÉCONNAISSANT LES STATUTS DE LA FONDATION OU REMETTANT EN CAUSE LES CHARGES GREVANT LA DONATION OU LE LEGS - ABSENCE.
</SCT>
<ANA ID="9A"> 25-02 1) Arrêté préfectoral autorisant le président d'une fondation reconnue d'utilité publique à conclure avec une commune un bail emphytéotique d'une durée de trente ans pour la location de bâtiments situés dans cette commune. Est opérant contre cet acte le moyen tiré de ce qu'il méconnaîtrait les règles du code civil relatives aux conditions dans lesquelles les prestations grevant une libéralité peuvent être modifiées puisqu'il appartient à l'autorité administrative, dès lors que ce bail confère au preneur un droit réel immobilier, de vérifier qu'il n'a pas pour effet de modifier les charges grevant la donation ou le legs en méconnaissance des articles 900-2 à 900-8 du code civil.,,,2) Il ressort de l'acte du 19 avril 1950, par lequel M. Tremintin a donné à la fondation l'immeuble en litige, que cette donation a eu pour objet de créer un établissement qui aura pour but de donner à la Paysanne Bretonne une formation morale et professionnelle qui lui permettra de remplir dans le milieu rural où elle est appelée à vivre, ses devoirs de ménagère, de mère de famille, de conseillère et d'aide du chef de l'exploitation. Par suite l'établissement ainsi fondé a une destination bien définie, et les membres du conseil d'administration devront toujours travailler à rester dans l'esprit de la Fondation (&#133;). Les locaux en litige ont ainsi, lors de la création de la fondation, hébergé un foyer chargé d'assurer la formation morale et professionnelle de la paysanne bretonne puis, alors que la fondation se donnait pour but, aux termes de ses statuts approuvés le 1er mars 1993, de donner aux jeunes du secteur de Plouescat une formation culturelle, morale et professionnelle qui leur permette de remplir, dans le milieu où ils sont appelés à vivre, leur rôle d'acteur responsable, ils ont accueilli un lycée professionnel rural privé. Le bail emphytéotique que la fondation a conclu avec la commune de Plouescat stipule, alors que les locaux ne sont plus occupés et qu'il est prévu qu'ils soient utilisés pour des activités éducatives, culturelles et de loisirs à destination des jeunes de la commune, que le preneur pourra changer la destination des lieux loués, tout en précisant, ainsi qu'il est dit dans l'acte de donation précité, qu'il devra conserver l'esprit de Pierre Trémintin, et de la Fondation qu'il avait créée. Dans ces conditions, le préfet du Finistère, en adoptant l'arrêté attaqué, n'a pas approuvé un contrat conclu en méconnaissance des statuts de la fondation ou ayant pour effet de remettre en cause, sans respecter les dispositions des articles 900-2 à 900-8 du code civil, les charges et conditions grevant une partie des biens qui en sont l'objet.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 8 décembre 2000,,, n° 205000, aux Tables sur d'autres points.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
