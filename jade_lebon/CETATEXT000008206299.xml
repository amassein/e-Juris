<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000008206299</ID>
<ANCIEN_ID>JG_L_2003_07_000000220437</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/08/20/62/CETATEXT000008206299.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 7ème sous-sections réunies, 09/07/2003, 220437, Publié au recueil Lebon</TITRE>
<DATE_DEC>2003-07-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>220437</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 7ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Stirn</PRESIDENT>
<AVOCATS>DE NERVO ; FOUSSARD</AVOCATS>
<RAPPORTEUR>M. Frédéric  Aladjidi</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olson</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2003:220437.20030709</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la requête et les observations complémentaires, enregistrées le 28 avril 2000 et le 12 janvier 2001, au secrétariat de la section du contentieux du Conseil d'Etat, présentés pour l'ASSISTANCE PUBLIQUE-HOPITAUX DE PARIS, dont le siège est au n° 3 de l'avenue Victoria à Paris (4ème) ; l'ASSISTANCE PUBLIQUE-HOPITAUX DE PARIS demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt en date du 8 février 2000 par lequel la cour administrative d'appel de Paris a rejeté son appel dirigé contre le jugement du tribunal administratif de Paris du 3 mars 1998 et l'a condamnée à verser les sommes de 850 000 F, 170 000 F et 175 000 F respectivement à Mme Souad ZYX et MM. Moktar et Malek ZYX, assorties des intérêts au taux légal à compter du 12 août 1995, ainsi que la somme de 560 202 F à la caisse nationale de retraite et de prévoyance sociale de Tunisie, assortie des intérêts légaux à compter du 10 mars 1997 ;<br/>
<br/>
              2°) de rejeter la demande présentée par Mme ZYX et MM. Moktar et Malek ZYX, ainsi que la demande présentée par la caisse nationale de retraite et de prévoyance sociale de Tunisie ;<br/>
<br/>
              3°) de condamner Mme ZYX, et MM. Moktar et Malek ZYX, ainsi que la caisse nationale de retraite et de prévoyance sociale de Tunisie  à lui verser la somme de 12 000 F au titre des frais exposés par elle et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Aladjidi, Maître des requêtes,  <br/>
<br/>
              - les observations de Me Foussard, avocat de l'ASSISTANCE PUBLIQUE-HOPITAUX DE PARIS et de Me de Nervo, avocat de Mme ZYX et autres, <br/>
<br/>
              - les conclusions de M. Olson, Commissaire du gouvernement ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant que, sans préjudice d'éventuels recours en garantie, le service public hospitalier est responsable, même en l'absence de faute de sa part, des conséquences dommageables pour les usagers de la défaillance des produits et appareils de santé qu'il utilise ; <br/>
<br/>
              Considérant qu'après avoir relevé, par une appréciation souveraine des faits de l'espèce suffisamment motivée, que le décès de M. ZYX était imputable aux conséquences du fonctionnement défectueux d'un respirateur artificiel ayant entraîné un arrêt cardiaque et une anoxie, la cour a commis une erreur de droit en retenant la responsabilité de l'ASSISTANCE PUBLIQUE-HOPITAUX DE PARIS au motif que la défaillance du matériel utilisé faisait présumer une faute dans l'organisation et le fonctionnement du service hospitalier ; que, toutefois, eu égard aux faits relevés par la cour, la défaillance du respirateur artificiel engage, sans préjudice d'un éventuel recours en garantie contre le fabricant de cet appareil, la responsabilité de l'ASSISTANCE PUBLIQUE-HOPITAUX DE PARIS, même en l'absence de faute de sa part, à réparer le préjudice qui en résulte ; que ce motif, qui est d'ordre public et ne comporte l'appréciation d'aucune circonstance de fait autre que celles qu'a relevées la cour, doit être substitué au motif retenu par l'arrêt attaqué ; <br/>
<br/>
              Considérant qu'après avoir relevé, par une application souveraine des faits, que la gravité des séquelles consécutives à la panne du respirateur s'expliquait, pour l'essentiel, par l'état de santé antérieur du patient, la cour, dont l'arrêt est suffisamment motivé sur ce point, a pu, sans commettre d'erreur de droit, d'une part juger qu'il n'y avait pas lieu d'exonérer, totalement ou partiellement, de sa responsabilité l'ASSISTANCE PUBLIQUE-HOPITAUX DE PARIS, et d'autre part, condamner cette dernière à réparer l'intégralité du préjudice résultant du décès de M. ZYX pour sa veuve et ses enfants ; <br/>
<br/>
              Considérant qu'en fixant le montant des réparations dues à Mme ZYX et à ses deux fils Moktar et Malek respectivement à 850 000 F, 170 000 F et 175 000 F, compte-tenu notamment de la perte des revenus de M. ZYX, la cour a suffisamment motivé l'arrêt attaqué et n'a pas méconnu le principe selon lequel l'indemnité allouée doit correspondre au préjudice subi ; <br/>
<br/>
              Considérant qu'il résulte de ce qui précède que l'ASSISTANCE PUBLIQUE-HOPITAUX DE PARIS n'est pas fondée à demander l'annulation de l'arrêt attaqué ;<br/>
<br/>
              Sur les conclusions tendant au remboursement des frais exposés et non compris dans les dépens : <br/>
<br/>
              Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que Mme ZYX, ses fils et la caisse nationale de retraite et de prévoyance sociale de Tunisie, qui ne sont pas dans la présente instance la partie perdante, soient condamnés à verser à l'ASSISTANCE PUBLIQUE-HOPITAUX DE PARIS, la somme qu'elle demande au titre des frais exposés par elle et non compris dans les dépens ; qu'il y a lieu, dans les circonstances de l'espèce, de condamner l'ASSISTANCE PUBLIQUE-HOPITAUX DE PARIS à verser à Mme ZYX à ses fils et à la caisse nationale de retraite et de prévoyance sociale de Tunisie une somme globale de 2 200 euros qu'ils demandent au même titre ; <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
<br/>
		Article 1er : La requête de l'ASSISTANCE PUBLIQUE-HOPITAUX DE PARIS est rejetée.<br/>
Article 2 : L'ASSISTANCE PUBLIQUE-HOPITAUX DE PARIS versera une somme globale de 2 200 euros à Mme ZYX à ses deux fils et à la caisse nationale de retraite et de prévoyance sociale de Tunisie en application de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à l'ASSISTANCE PUBLIQUE-HOPITAUX DE PARIS, à Mme Souad Khelil veuve ZYX, à MM. Moktar et Malek ZYX, à la caisse nationale de retraite et de prévoyance sociale de Tunisie et au ministre de la santé, de la famille et des personnes handicapées.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">60-01-02-01-01-04 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. FAITS SUSCEPTIBLES OU NON D'OUVRIR UNE ACTION EN RESPONSABILITÉ. FONDEMENT DE LA RESPONSABILITÉ. RESPONSABILITÉ SANS FAUTE. RESPONSABILITÉ FONDÉE SUR L'ÉGALITÉ DEVANT LES CHARGES PUBLIQUES. RESPONSABILITÉ DU FAIT D'AGISSEMENTS ADMINISTRATIFS NON FAUTIFS. - UTILISATION DE PRODUITS OU APPAREILS DE SANTÉ DÉFAILLANTS [RJ1].
</SCT>
<ANA ID="9A"> 60-01-02-01-01-04 Sans préjudice d'éventuels recours en garantie, le service public hospitalier est responsable, même en l'absence de faute de sa part, des conséquences dommageables pour les usagers de la défaillance des produits et appareils de santé qu'il utilise. Cas du fonctionnement défectueux d'un respirateur artificiel.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Ab. jur. 14 décembre 1984, Centre hospitalier de Meulan, T. p. 734.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
