<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030926086</ID>
<ANCIEN_ID>JG_L_2015_07_000000383481</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/92/60/CETATEXT000030926086.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Assemblée, 22/07/2015, 383481, Publié au recueil Lebon</TITRE>
<DATE_DEC>2015-07-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>383481</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Assemblée</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ ; SCP DIDIER, PINET</AVOCATS>
<RAPPORTEUR>Mme Pauline Pannier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Gaëlle Dumortier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CEASS:2015:383481.20150722</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Le syndicat CGT de l'union locale de Calais et environs a demandé au tribunal administratif de Lille d'annuler pour excès de pouvoir la décision du 31 octobre 2013 par laquelle le directeur régional des entreprises, de la concurrence, de la consommation, du travail et de l'emploi du Nord-Pas de Calais a homologué le document élaboré par la société AJJIS et la société FHB, agissant en qualité d'administrateurs judiciaires de la société Calaire Chimie, fixant le contenu d'un plan de sauvegarde de l'emploi. Par un jugement n° 1307584 du 26 mars 2014, le tribunal administratif de Lille a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 14DA00635 du 3 juillet 2014, la cour administrative d'appel de Douai a rejeté l'appel du syndicat CGT de l'union locale de Calais et environs dirigé contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire enregistrés les 5 août et 6 novembre 2014 au secrétariat du contentieux du Conseil d'Etat, le syndicat CGT de l'union locale de Calais et environs demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de la société AJIIS, de la société FHB, de M. A... et de l'Etat la somme globale de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de commerce ;<br/>
              - le code du travail ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Pauline Pannier, auditeur,  <br/>
<br/>
              - les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Didier, Pinet, avocat du Syndicat CGT de l'union locale de Calais et environs  et à la SCP Lyon-Caen, Thiriez, avocat de la société AJJIS et autres ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 6 juillet 2015, présentée par le ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que par trois jugements des 28 mai, 22 octobre et 24 octobre 2013, le tribunal de commerce de Boulogne-sur-Mer a placé la société Calaire Chimie en redressement judiciaire, puis a arrêté le plan de sa cession totale à la société Synthexim qui reprenait 80 salariés, les 111 autres salariés étant licenciés, et enfin prononcé sa liquidation judiciaire ; qu'à la demande des sociétés AJIIS et FHB, administrateurs judiciaires de la société Calaire Chimie, le directeur régional des entreprises, de la concurrence, de la consommation, du travail et de l'emploi du Nord-Pas de Calais a, par une décision du 31 octobre 2013, homologué le document unilatéral de l'employeur fixant un plan de sauvegarde de l'emploi ; que le syndicat CGT de l'union locale de Calais et environs se pourvoit en cassation contre l'arrêt du 3 juillet 2014 par lequel la cour administrative d'appel de Douai a rejeté sa requête tendant à l'annulation du jugement du 26 mars 2014 du tribunal administratif de Lille rejetant sa demande d'annulation de cette décision ;<br/>
<br/>
              Sur la recevabilité de la demande de première instance du syndicat CGT de l'union locale de Calais et environs :<br/>
<br/>
              2. Considérant qu'aux termes du troisième alinéa de l'article L. 1235-7-1 du code du travail, relatif aux délais de contestation et aux voies de recours contre les décisions administratives de validation ou d'homologation d'un accord collectif ou d'un document unilatéral portant plan de sauvegarde de l'emploi : " Le recours est présenté dans un délai de deux mois par l'employeur à compter de la notification de la décision de validation ou d'homologation, et par les organisations syndicales et les salariés à compter de la date à laquelle cette décision a été portée à leur connaissance conformément à l'article L. 1233-57-4 " ; que l'article L. 1233-57-4 du même code définit les modalités selon lesquelles une décision de validation ou d'homologation est, d'une part, notifiée à l'employeur, au comité d'entreprise, ainsi que, en cas de validation d'un accord collectif, aux organisations syndicales qui en sont signataires et, d'autre part, portée à la connaissance des salariés ; qu'il résulte de ces dispositions que les syndicats présents dans l'entreprise ont qualité pour agir contre ces décisions et que leurs recours doivent être présentés dans un délai de deux mois à compter, soit de la notification de la décision lorsque celle-ci doit leur être notifiée sur le fondement des dispositions de l'article L. 1233-57-4 du code du travail, soit de l'accomplissement des modalités d'information des salariés prévues par cet article ;<br/>
<br/>
              3. Considérant qu'eu égard à la portée de la décision par laquelle l'administration a homologué le document présenté par les administrateurs judiciaires de la société Calaire Chimie, le syndicat CGT de l'union locale de Calais et environs, qui est une union de syndicats régie par les articles L. 2133-1 à L. 2133-3 du code du travail, justifie, au regard des intérêts collectifs qu'elle a, en vertu de ces articles, pour objet de défendre, d'un intérêt lui donnant qualité pour agir, dans le délai opposable aux syndicats présents dans l'entreprise, contre la décision d'homologation contestée ; que la société AJIIS et autres ne sont, par suite, pas fondés à soutenir que la demande présentée par le syndicat CGT de l'union locale de Calais et environs devant le tribunal administratif de Lille n'était pas recevable ;<br/>
<br/>
              Sur le bien-fondé de l'arrêt attaqué :<br/>
<br/>
              4. Considérant qu'aux termes de l'article L. 1233-61 du code du travail : " Dans les entreprises d'au moins cinquante salariés, lorsque le projet de licenciement concerne au moins dix salariés dans une même période de trente jours, l'employeur établit et met en oeuvre un plan de sauvegarde de l'emploi pour éviter les licenciements ou en limiter le nombre. / Ce plan intègre un plan de reclassement visant à faciliter le reclassement des salariés dont le licenciement ne pourrait être évité (...) " ; qu'aux termes de l'article L. 1233-24-4 du même code : " A défaut d'accord (...), un document élaboré par l'employeur après la dernière réunion du comité d'entreprise fixe le contenu du plan de sauvegarde de l'emploi et précise les éléments prévus aux 1° à 5° de l'article L. 1233-24-2, dans le cadre des dispositions légales et conventionnelles en vigueur " ; qu'aux termes de l'article L. 1233-58 du code du travail : " I. En cas de redressement ou de liquidation judiciaire, l'employeur, l'administrateur ou le liquidateur, selon le cas, qui envisage des licenciements économiques, met en oeuvre un plan de licenciement dans les conditions prévues aux articles L.1233-24-1 à L.1233-24-4. (...) II. Pour un licenciement d'au moins dix salariés dans une entreprise d'au moins cinquante salariés, (...) le document mentionné à l'article L.1233-24-4, élaboré par l'employeur, l'administrateur ou le liquidateur, est homologué dans les conditions fixées aux articles L.1233-57-1 à L.1233-57-3, aux deuxième et troisième alinéa de l'article L.1233-57-4 et à l'article L.1233-57-7 (...)" ; qu'enfin, aux termes de l'article L. 1233-57-3 du même code, dans sa rédaction en vigueur à la date de la décision litigieuse : " (...) l'autorité administrative homologue le document élaboré par l'employeur mentionné à l'article L. 1233-24-4, après avoir vérifié la conformité de son contenu aux dispositions législatives et aux stipulations conventionnelles relatives aux éléments mentionnés aux 1° à 5° de l'article L.1233-24-2, la régularité de la procédure d'information et de consultation du comité d'entreprise et (...) le respect par le plan de sauvegarde de l'emploi des articles L. 1233-61 à L. 1233-63 en fonction des critères suivants : / 1° Les moyens dont disposent l'entreprise, l'unité économique et sociale et le groupe ; / 2° Les mesures d'accompagnement prévues au regard de l'importance du projet de licenciement ; / 3° Les efforts de formation et d'adaptation tels que mentionnés aux articles L. 1233-4 et L. 6321-1 " ; <br/>
<br/>
              5. Considérant qu'il résulte de l'ensemble des dispositions citées ci-dessus que, lorsqu'elle est saisie d'une demande d'homologation d'un document élaboré en application de l'article L. 1233-24-4 du code du travail, il appartient à l'administration, sous le contrôle du juge de l'excès de pouvoir, de vérifier la conformité de ce document et du plan de sauvegarde de l'emploi dont il fixe le contenu aux dispositions législatives et aux stipulations conventionnelles applicables, en s'assurant notamment du respect par le plan de sauvegarde de l'emploi des dispositions des articles L. 1233-61 à L. 1233-63 du même code ; qu'à ce titre elle doit, au regard de l'importance du projet de licenciement, apprécier si les mesures contenues dans le plan sont précises et concrètes et si, à raison, pour chacune, de sa contribution aux objectifs de maintien dans l'emploi et de reclassement des salariés, elles sont, prises dans leur ensemble, propres à satisfaire à ces objectifs compte tenu, d'une part, des efforts de formation et d'adaptation déjà réalisés par l'employeur et, d'autre part, des moyens dont disposent l'entreprise et, le cas échéant, l'unité économique et sociale et le groupe ;<br/>
<br/>
              6. Considérant qu'à ce titre, il revient notamment à l'autorité administrative de s'assurer que le plan de reclassement intégré au plan de sauvegarde de l'emploi est de nature à faciliter le reclassement des salariés dont le licenciement ne pourrait être évité ; que l'employeur doit, à cette fin, avoir identifié dans le plan l'ensemble des possibilités de reclassement des salariés dans l'entreprise ; qu'en outre, lorsque l'entreprise appartient à un groupe, l'employeur, seul débiteur de l'obligation de reclassement, doit avoir procédé à une recherche sérieuse des postes disponibles pour un reclassement dans les autres entreprises du groupe ; que pour l'ensemble des postes de reclassement ainsi identifiés, l'employeur doit avoir indiqué dans le plan leur nombre, leur nature et leur localisation ; <br/>
<br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que la cour n'a pas commis d'erreur de droit en prenant en compte, ainsi qu'il ressort des termes de son arrêt, pour apprécier le respect par le plan de sauvegarde de l'emploi de l'entreprise Calaire Chimie des dispositions des articles L. 1233-61 à L. 1233-63 du code du travail, des mesures qui sont de nature à contribuer, de manière directe ou indirecte, au maintien dans l'emploi ou au reclassement des salariés telles que la participation de l'entreprise aux contrats de sécurisation professionnelle, la priorité d'embauche par la société Synthexim des salariés licenciés, le maintien de la contribution de l'entreprise au régime de protection sociale complémentaire des salariés licenciés ou la mise en place d'une cellule de soutien psychologique, alors même que certaines de ces mesures bénéficieraient de financements publics ; qu'en estimant que, prises dans leur ensemble, les mesures de ce plan étaient suffisantes, compte tenu des moyens de l'entreprise Calaire Chimie et du groupe ICIG auquel elle appartient, pour assurer le respect des dispositions des articles L. 1233-61 à L. 1233-63 du code du travail, la cour a porté sur les faits de l'espèce une appréciation souveraine exempte de dénaturation ; qu'elle a pu, par suite, sans erreur de droit, juger que le directeur régional des entreprises, de la concurrence, de la consommation, du travail et de l'emploi du Nord-Pas de Calais avait pu légalement homologuer le document unilatéral présenté par l'employeur ;<br/>
<br/>
              8. Considérant, enfin, que le moyen tiré de ce que la motivation de la décision d'homologation ne permet pas de vérifier que l'autorité administrative a effectivement fait application de l'ensemble des critères fixés à l'article L. 1233-57-3 du code du travail n'a pas été soulevé devant la cour administrative d'appel de Douai ; que le moyen tiré de ce que la cour aurait commis une erreur de droit en ne l'accueillant pas doit, par suite, être écarté comme inopérant ;<br/>
<br/>
              9. Considérant qu'il résulte de tout ce qui précède que le syndicat CGT de l'union locale de Calais et environs n'est pas fondé à demander l'annulation de l'arrêt attaqué ; <br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              10. Considérant que ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat et de la société AJIIS et autres qui ne sont pas, dans la présente instance, les parties perdantes ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge du syndicat CGT de l'union locale de Calais et environs la somme que demandent la société AJIIS et autres au titre des mêmes dispositions ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi du syndicat CGT de l'union locale de Calais et environs est rejeté.<br/>
Article 2 : Les conclusions de la société AJIIS et autres présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 3 : La présente décision sera notifiée au syndicat CGT de l'union locale de Calais et environs, à la société AJIIS et à la société FHB en leur qualité d'administrateurs judiciaires de la société Calaire Chimie, à M. A...en sa qualité de mandataire judiciaire de la société Calaire Chimie et au ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-01-04-02-02 PROCÉDURE. INTRODUCTION DE L'INSTANCE. INTÉRÊT POUR AGIR. EXISTENCE D'UN INTÉRÊT. SYNDICATS, GROUPEMENTS ET ASSOCIATIONS. - DÉCISION DE VALIDATION OU D'HOMOLOGATION D'UN PSE - 1) SYNDICATS PRÉSENTS DANS L'ENTREPRISE - INTÉRÊT POUR AGIR - EXISTENCE - 2) UNION DE SYNDICATS - INTÉRÊT POUR AGIR - APPRÉCIATION AU CAS PAR CAS [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">66-07 TRAVAIL ET EMPLOI. LICENCIEMENTS. - HOMOLOGATION ADMINISTRATIVE DES PSE (LOI DU 14 JUIN 2013) - 1) CONTRÔLE DU CONTENU DU PSE - MODALITÉS - 2) CONTENTIEUX - INTÉRÊT POUR AGIR - A) SYNDICATS PRÉSENTS DANS L'ENTREPRISE - B) UNION DE SYNDICATS.
</SCT>
<ANA ID="9A"> 54-01-04-02-02 1) Les syndicats présents dans une entreprise ont qualité pour agir contre une décision validant ou homologuant un plan de sauvegarde de l'emploi (PSE) dans leur entreprise et leurs recours doivent être présentés dans un délai de deux mois à compter, soit de la notification de la décision lorsque celle-ci doit leur être notifiée sur le fondement des dispositions de l'article L. 1233-57-4 du code du travail, soit de l'accomplissement des modalités d'information des salariés prévues par cet article.,,,2) En l'espèce, le syndicat CGT de l'union locale de Calais et environs, qui est une union de syndicats régie par les articles L. 2133-1 à L. 2133-3 du code du travail, justifie, au regard des intérêts collectifs qu'elle a, en vertu de ces articles, pour objet de défendre, d'un intérêt lui donnant qualité pour agir, dans le délai opposable aux syndicats présents dans l'entreprise, contre la décision d'homologation contestée.</ANA>
<ANA ID="9B"> 66-07 1) Lorsqu'elle est saisie d'une demande d'homologation d'un document élaboré en application de l'article L. 1233-24-4 du code du travail, il appartient à l'administration, sous le contrôle du juge de l'excès de pouvoir, de vérifier la conformité de ce document et du plan de sauvegarde de l'emploi (PSE) dont il fixe le contenu aux dispositions législatives et aux stipulations conventionnelles applicables, en s'assurant notamment du respect par le plan de sauvegarde de l'emploi des dispositions des articles L. 1233-61 à L. 1233-63 du même code.... ,,A ce titre elle doit, au regard de l'importance du projet de licenciement, apprécier si les mesures contenues dans le plan sont précises et concrètes et si, à raison, pour chacune, de sa contribution aux objectifs de maintien dans l'emploi et de reclassement des salariés, elles sont, prises dans leur ensemble, propres à satisfaire à ces objectifs compte tenu, d'une part, des efforts de formation et d'adaptation déjà réalisés par l'employeur et, d'autre part, des moyens dont disposent l'entreprise et, le cas échéant, l'unité économique et sociale et le groupe.... ,,Il revient notamment à l'autorité administrative de s'assurer que le plan de reclassement intégré au PSE est de nature à faciliter le reclassement des salariés dont le licenciement ne pourrait être évité. L'employeur doit, à cette fin, avoir identifié dans le plan l'ensemble des possibilités de reclassement des salariés dans l'entreprise. En outre, lorsque l'entreprise appartient à un groupe, l'employeur, seul débiteur de l'obligation de reclassement, doit avoir procédé à une recherche sérieuse des postes disponibles pour un reclassement dans les autres entreprises du groupe. Pour l'ensemble des postes de reclassement ainsi identifiés, l'employeur doit avoir indiqué dans le plan leur nombre, leur nature et leur localisation.... ,,2) a) Les syndicats présents dans une entreprise ont qualité pour agir contre une décision validant ou homologuant un PSE dans leur entreprise et leurs recours doivent être présentés dans un délai de deux mois à compter, soit de la notification de la décision lorsque celle-ci doit leur être notifiée sur le fondement des dispositions de l'article L. 1233-57-4 du code du travail, soit de l'accomplissement des modalités d'information des salariés prévues par cet article.,,,b) En l'espèce, le syndicat CGT de l'union locale de Calais et environs, qui est une union de syndicats régie par les articles L. 2133-1 à L. 2133-3 du code du travail, justifie, au regard des intérêts collectifs qu'elle a, en vertu de ces articles, pour objet de défendre, d'un intérêt lui donnant qualité pour agir, dans le délai opposable aux syndicats présents dans l'entreprise, contre la décision d'homologation contestée.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Cf. CE, Assemblée, 12 décembre 2003, USPAC CGT,  n°s 239507 245195, p. 508 ; CE, 2 juin 2010, Commune de Loos, n° 309446, p. 186.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
