<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029311324</ID>
<ANCIEN_ID>JG_L_2014_07_000000363007</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/31/13/CETATEXT000029311324.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème / 2ème SSR, 30/07/2014, 363007</TITRE>
<DATE_DEC>2014-07-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>363007</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème / 2ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUIN-PALAT, BOUCARD ; SCP PIWNICA, MOLINIE ; SCP DIDIER, PINET</AVOCATS>
<RAPPORTEUR>M. Jean-Dominique Nuttens</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:363007.20140730</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et les mémoires complémentaires, enregistrés les 25 septembre et 26 décembre 2012 et le 24 janvier 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour la commune de Biarritz, représentée par son maire ; la commune demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'arrêt n° 10BX02109 du 26 juillet 2012 par lequel la cour administrative d'appel de Bordeaux a, sur la requête de M. C...B...-A..., d'une part, annulé le jugement n° 0801791 du 30 juin 2010 par lequel le tribunal administratif de Pau a rejeté la demande de M. B...-A... tendant à l'annulation de la délibération du 23 juillet 2008 par laquelle le conseil municipal de Biarritz a, notamment, autorisé le maire de la commune à signer le contrat de partenariat pour la réalisation de la cité de l'océan et du surf et de l'aquarium du musée de la mer et, d'autre part, annulé cette délibération ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de M. B...-A... ; <br/>
<br/>
              3°) de mettre à la charge de M. B...-A... le versement de la somme de 5 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des collectivités territoriales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Nuttens, maître des requêtes en service extraordinaire, <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat de la commune de Biarritz , à la SCP Thouin-Palat, Boucard, avocat de M. B... -A... et à la SCP Didier, Pinet, avocat de la société Biarritz Océan ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la commune de Biarritz a décidé, par une délibération du 26 janvier 2007, le principe du recours à un contrat de partenariat pour la réalisation d'un projet dénommé " Biarritz-Océan " comportant le financement et la réalisation d'une nouvelle " Cité de l'océan et du surf ", le financement et la réalisation de travaux d'extension et de modernisation de son " Musée de la mer " et la maintenance des deux installations ; que la commune se pourvoit en cassation contre l'arrêt par lequel la cour administrative d'appel de Bordeaux a, sur requête de M. B...-A..., annulé la délibération du 23 juillet 2008 par laquelle, au terme de la procédure de dialogue compétitif qui a conduit à sélectionner la société en nom collectif Biarritz Océan, le conseil municipal de Biarritz a autorisé le maire de la ville à signer le contrat de partenariat avec cette société ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la commune de Biarritz avait opposé devant le tribunal administratif de Toulouse une fin de non-recevoir tirée de ce les requérants de première instance, au nombre desquels figurait M. B... A..., n'avaient pas, en leur qualité de conseillers municipaux, intérêt à agir dès lors que la délibération attaquée ne portait pas atteinte à leurs prérogatives ou à leur statut ; que, contrairement à ce que soutient M. B...-A... devant le Conseil d'Etat, une telle fin de non-recevoir pouvait être utilement présentée ; que, dès lors, la cour administrative d'appel de Bordeaux ne pouvait régulièrement faire droit aux conclusions présentées, en appel, par M. B... -A... sans avoir au préalable écarté expressément cette fin de non-recevoir qui, même non reprise en appel, n'avait pas été abandonnée par la commune ; qu'en omettant d'écarter cette fin de non-recevoir, elle a par suite méconnu son office ; qu'ainsi, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, son arrêt doit, pour ce motif, être annulé ; <br/>
<br/>
              3. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application de l'article L. 821-2 du code de justice administrative ; <br/>
<br/>
              Sur les fins de non-recevoir opposées en première instance et en appel par la commune de Biarritz :<br/>
<br/>
              4. Considérant, d'une part, que M. B...-A... justifiait, en sa qualité de conseiller municipal de la commune de Biarritz, d'un intérêt à attaquer la délibération dont il demandait l'annulation, même sans se prévaloir d'une atteinte portée à ses prérogatives ; <br/>
<br/>
              5. Considérant, d'autre part, que le mémoire d'appel présenté par M. B... A... comporte des moyens critiquant le jugement du tribunal administratif de Pau et ne se borne pas à la reproduction littérale de son argumentation devant ce tribunal ; qu'il répond ainsi aux exigences de l'article R. 411-1 du code de justice administrative ; <br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que les fins de non-recevoir opposées par la commune de Biarritz doivent être écartées ; <br/>
<br/>
              Sur la légalité de la délibération attaquée :<br/>
<br/>
              7. Considérant qu'aux termes de l'article L. 1414-1 du code général des collectivités territoriales, dans sa rédaction applicable à l'espèce : " Les contrats de partenariat sont des contrats administratifs par lesquels la personne publique confie à un tiers, pour une période déterminée en fonction de la durée d'amortissement des investissements ou des modalités de financement retenues, une mission globale relative au financement d'investissements immatériels, d'ouvrages ou d'équipements nécessaires au service public, à la construction ou transformation des ouvrages ou équipements, ainsi qu'à leur entretien, leur maintenance, leur exploitation ou leur gestion et, le cas échéant, à d'autres prestations de services concourant à l'exercice, par la personne publique, de la mission de service public dont elle est chargée (...) Le cocontractant de la personne publique assure la maîtrise d'ouvrage des travaux à réaliser. Il peut se voir confier tout ou partie de la conception  des ouvrages. La rémunération du cocontractant fait l'objet d'un paiement par la personne publique pendant toute la durée du contrat (...) " ; <br/>
<br/>
              8. Considérant que l'article L. 1414-2 du même code, dans sa rédaction applicable à l'espèce, limite le recours au contrat de partenariat aux seuls projets pour lesquels une évaluation préalablement conduite par la personne publique " montre ou bien que, compte tenu de la complexité du projet, la personne publique n'est pas objectivement en mesure de définir seule et à l'avance les moyens techniques pouvant répondre à ses besoins ou d'établir le montage financier ou juridique du projet, ou bien que le projet présente un caractère d'urgence ; (...) " ; qu'aux termes du dernier alinéa du même article : " L'évaluation mentionnée ci-dessus est présentée à l'assemblée délibérante de la collectivité territoriale ou à l'organe délibérant de l'établissement public, qui se prononce sur le principe du recours à un contrat de partenariat. " ; <br/>
<br/>
              9. Considérant que M. B...-A... soutient que la délibération du 23 juillet 2008 autorisant le maire de Biarritz à signer un contrat de partenariat est entachée d'illégalité, faute, pour le projet " Biarritz Océan ", de remplir une des conditions, posées par l'article L. 1414-2 du code général des collectivités territoriales cité ci-dessus, auxquelles il peut être légalement recouru à un contrat de partenariat ; que, contrairement à ce que soutient la commune de Biarritz, le moyen tiré de ce que les conditions de recours au contrat de partenariat ne sont pas réunies peut être utilement soulevé à l'appui d'un recours dirigé contre l'acte par lequel la signature d'un tel contrat est autorisée ; que le respect des conditions posées par la loi s'apprécie au vu de l'évaluation préalable qui a donné lieu à la délibération prévue par le dernier alinéa de l'article L. 1414-2 du même code ;<br/>
<br/>
              10. Considérant que la commune de Biarritz a justifié le recours au contrat de partenariat par la complexité technique du projet " Biarritz-Océan ", dans ses deux composantes, et, subsidiairement, par la complexité du montage juridique et financier à établir pour l'ensemble du projet ;<br/>
<br/>
              En ce qui concerne l'extension et la rénovation du " Musée de la mer " :<br/>
<br/>
              11. Considérant que la commune fait valoir, en prenant appui sur l'évaluation préalable au recours au contrat de partenariat à laquelle elle a procédé, que le projet d'extension et de rénovation du " Musée de la mer " se traduit par la réalisation d'un aquarium de 1 300 m3 réalisé dans un espace souterrain mitoyen d'un tunnel routier et d'un ancien bunker, implique la mise en place d'équipements de haute technologie et requiert le maintien de l'ouverture au public des installations existantes pendant la durée des travaux ; que la seule invocation de la complexité des procédés techniques à mettre en oeuvre ne peut suffire à justifier légalement le recours au contrat de partenariat, en l'absence de circonstances particulières de nature à établir qu'il était impossible à la commune de définir, seule et à l'avance, les moyens techniques propres à satisfaire ses besoins ;<br/>
<br/>
              En ce qui concerne la " Cité de l'océan et du surf " :<br/>
<br/>
              12. Considérant que, pour apprécier la capacité objective de la personne publique à définir seule et à l'avance les moyens techniques permettant de répondre à ses besoins et, par suite, pour déterminer si la complexité technique du projet justifie légalement le recours au contrat de partenariat, il n'y a pas lieu de tenir compte des études postérieures au lancement de la procédure de passation du contrat que cette personne publique serait en mesure de confier à un tiers, soit dans le cadre du contrat de partenariat qu'elle envisage de conclure, soit au titre d'un contrat distinct ; qu'en revanche, il y a lieu de tenir compte de l'ensemble des études, même réalisées par des tiers, dont la personne publique dispose déjà à la date à laquelle elle décide de recourir au contrat de partenariat ;<br/>
<br/>
              13. Considérant, d'une part, qu'il ressort des pièces du dossier que le conseil municipal de la commune de Biarritz avait décidé dès 2004 le lancement d'un concours de maîtrise d'oeuvre en vue de la conception de la " Cité de l'océan et du surf " ; qu'à l'issue de ce concours, la commune de Biarritz a, à la fin de l'année 2005, passé un contrat de maîtrise d'oeuvre portant sur le bâtiment et la scénographie de la " Cité de l'océan et du surf ", qui incluait  la conception du projet et le suivi de sa réalisation ; que ce contrat a donné lieu à l'établissement d'un avant-projet détaillé, que la commune de Biarritz a annexé au programme fonctionnel remis à chaque candidat au contrat de partenariat dès la première phase du dialogue engagé pour sa passation, et même, s'agissant du bâtiment, à des études de projet remises aux candidats lors du lancement, fin 2007, de la deuxième phase du dialogue ; que la commune de Biarritz ne fournit aucune précision suffisante de nature à établir que, malgré les circonstances rappelées ci-dessus, elle n'aurait pas été objectivement en mesure de définir, à la date à laquelle elle a décidé de recourir au contrat de partenariat, les moyens techniques pouvant répondre à ses besoins ;<br/>
<br/>
              14. Considérant, d'autre part, que si la commune fait état de la complexité de la scénographie de la " Cité de l'océan et du surf " et de la nécessité de faire appel à des équipements de haute technologie, elle n'invoque aucune circonstance particulière de nature à établir que cet aspect particulier du projet, qui, au demeurant, a été ultérieurement exclu du périmètre du contrat au cours de la phase de dialogue compétitif, l'empêchait de définir seule et à l'avance les moyens techniques pouvant répondre à ses besoins ; <br/>
<br/>
              En ce qui concerne le projet " Biarritz-Océan " pris dans son ensemble :<br/>
<br/>
              15. Considérant, d'une part, que si la commune de Biarritz a souhaité que la construction de la " Cité de l'océan et du surf " et l'extension et la rénovation du " Musée de la mer " soient confiés à un même opérateur privé, chargé, après la réalisation des travaux, de l'entretien et de la maintenance des deux équipements, elle n'apporte aucun élément précis de nature à établir que le regroupement des deux composantes du projet la mettait dans l'impossibilité de définir, seule et à l'avance, les moyens techniques propres à satisfaire ses besoins, alors même que ce rapprochement aurait été décidé dans un souci d'optimisation des coûts et de complémentarité dans la gestion de ces équipements ;<br/>
<br/>
              16. Considérant, d'autre part, que si la commune de Biarritz fait valoir que le recours au contrat de partenariat était également justifié par l'impossibilité dans laquelle elle se trouvait d'établir le montage financier et juridique du projet pris dans son ensemble, elle n'apporte aucun élément permettant d'apprécier le bien fondé de cette allégation ; <br/>
<br/>
              17. Considérant qu'il résulte de tout ce qui précède que si l'évaluation préalable du projet " Biarritz-Océan " effectuée par la commune de Biarritz en application de l'article L. 1414-2 du code général des collectivités territoriales faisait apparaître de nombreux éléments de complexité technique, il ne ressort pas des pièces du dossier que, compte tenu de l'ensemble des circonstances particulières exposées ci-dessus, la commune aurait été dans l'impossibilité de définir seule et à l'avance les moyens techniques pouvant répondre à ses besoins ou d'établir le montage financier ou juridique du projet ; qu'ainsi le projet ne remplissait pas la condition de complexité, seule invoquée par la commune, pour qu'il fût légalement possible de recourir au contrat de partenariat pour le réaliser ; <br/>
<br/>
              18. Considérant, par suite, et sans qu'il soit besoin d'examiner les autres moyens de sa requête, que M. B...-A... est fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Pau a rejeté sa demande dirigée contre la délibération du 23 juillet 2008 en tant qu'elle autorise le maire de Biarritz à signer un contrat de partenariat avec la société en nom collectif Biarritz-Océan ; <br/>
<br/>
              19. Considérant que si M. B...-A... demande également l'annulation de cette délibération du 23 juillet 2008 en tant qu'elle autorise le maire de Biarritz, d'une part, à signer l'acte d'acceptation de la cession de créances consentie par la SNC Biarritz Océan à la société Dexia ainsi que l'accord direct passé entre la commune et cette société et, d'autre part, à verser une somme de 150 000 euros à chacun des deux candidats non retenus, il n'articule aucun moyen utile contre cette partie de la délibération litigieuse, qui est divisible de l'ensemble ; qu'il n'est par suite pas fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Pau a rejeté, dans cette mesure, sa demande d'annulation de la délibération du 23 juillet 2008 ;<br/>
<br/>
              Sur les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              20. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que les sommes demandées à ce titre par la commune de Biarritz et la SNC Biarritz Océan soient mises à la charge de M. B...-A... qui n'est pas, pour l'essentiel, la partie perdante dans la présente instance ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de Biarritz le versement à M. B...-A... d'une somme de 6 000 euros au titre des frais engagés par lui et non compris dans les dépens tant devant le Conseil d'Etat que devant la cour administrative d'appel de Bordeaux et le tribunal administratif de Pau ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Bordeaux du 26 juillet 2012 est annulé.<br/>
<br/>
Article 2 : Le jugement du tribunal administratif de Pau du 30 juin 2010 est annulé en tant qu'il rejette la demande de M. B... -A... tendant à l'annulation de la délibération du 23 juillet 2008 en tant qu'elle autorise le maire de Biarritz à signer un contrat de partenariat avec la société en nom collectif Biarritz-Océan.<br/>
<br/>
Article 3 : La délibération du conseil municipal de Biarritz en date du 23 juillet 2008 est annulée en tant qu'elle autorise le maire de Biarritz à signer un contrat de partenariat avec la société en nom collectif Biarritz-Océan. <br/>
Article 4 : Le surplus des conclusions d'appel de M. B... -A... est rejeté.<br/>
<br/>
Article 5 : La commune de Biarritz versera à M. B...-A... une somme de 6 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
Article 6 : Les conclusions présentées par la commune de Biarritz et la SNC Biarritz Océan au titre des mêmes dispositions sont rejetées. <br/>
<br/>
Article 7 : La présente décision sera notifiée à la commune de Biarritz, à M. C... B...-A... et à la SNC Biarritz Océan. <br/>
Copie en sera adressée pour information au ministre de l'économie, du redressement productif et du numérique.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-01-03-05 MARCHÉS ET CONTRATS ADMINISTRATIFS. NOTION DE CONTRAT ADMINISTRATIF. DIVERSES SORTES DE CONTRATS. - 1) CONTENTIEUX - RECOURS DIRIGÉ CONTRE L'ACTE AUTORISANT LA SIGNATURE D'UN TEL CONTRAT - MOYEN TIRÉ DE CE QUE LES CONDITIONS DE RECOURS AU CONTRAT DE PARTENARIAT NE SONT PAS RÉUNIES - A) OPÉRANCE - EXISTENCE - B) APPRÉCIATION DU RESPECT DE CES CONDITIONS - MODALITÉS - APPRÉCIATION AU VU DE L'ÉVALUATION PRÉALABLE AYANT DONNÉ LIEU À LA DÉLIBÉRATION PRÉVUE À L'ARTICLE L. 1414-2 DU CGCT - 2) CONDITIONS DE RECOURS AU CONTRAT DE PARTENARIAT (ART. L. 1414-2 DU CGCT) - INCAPACITÉ DE LA COLLECTIVITÉ TERRITORIALE DE DÉFINIR, SEULE ET À L'AVANCE, LES MOYENS TECHNIQUES PROPRES À SATISFAIRE SES BESOINS - A) PORTÉE - SEULE INVOCATION DE LA COMPLEXITÉ DES PROCÉDÉS TECHNIQUES À METTRE EN &#140;UVRE - ABSENCE - B) MODALITÉS D'APPRÉCIATION - PRISE EN COMPTE DES ÉTUDES DONT LA PERSONNE PUBLIQUE DISPOSE DÉJÀ À LA DATE DE LA DÉCISION DE RECOURIR AU CONTRAT - EXISTENCE - PRISE EN COMPTE DES ÉTUDES POSTÉRIEURES AU LANCEMENT DE LA PROCÉDURE DE PASSATION DU CONTRAT - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">39-08 MARCHÉS ET CONTRATS ADMINISTRATIFS. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. - RECOURS DIRIGÉ CONTRE L'ACTE AUTORISANT LA SIGNATURE D'UN CONTRAT DE PARTENARIAT - MOYEN TIRÉ DE CE QUE LES CONDITIONS DE RECOURS AU CONTRAT DE PARTENARIAT NE SONT PAS RÉUNIES - 1) OPÉRANCE - EXISTENCE - 2) APPRÉCIATION DU RESPECT DE CES CONDITIONS - MODALITÉS - APPRÉCIATION AU VU DE L'ÉVALUATION PRÉALABLE AYANT DONNÉ LIEU À LA DÉLIBÉRATION PRÉVUE À L'ARTICLE L. 1414-2 DU CGCT.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-07-01-04-03 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. QUESTIONS GÉNÉRALES. MOYENS. MOYENS INOPÉRANTS. - ABSENCE - RECOURS DIRIGÉ CONTRE L'ACTE AUTORISANT LA SIGNATURE D'UN CONTRAT DE PARTENARIAT - MOYEN TIRÉ DE CE QUE LES CONDITIONS DE RECOURS AU CONTRAT DE PARTENARIAT NE SONT PAS RÉUNIES.
</SCT>
<ANA ID="9A"> 39-01-03-05 1) a) Le moyen tiré de ce que les conditions de recours au contrat de partenariat ne sont pas réunies peut être utilement soulevé à l'appui d'un recours dirigé contre l'acte par lequel la signature d'un tel contrat est autorisée.,,,b) Le respect des conditions posées par la loi s'apprécie au vu de l'évaluation préalable qui a donné lieu à la délibération prévue par le dernier alinéa de l'article L. 1414-2 du code général des collectivités territoriales (CGCT), dans sa rédaction antérieure à la loi n° 2008-735 du 28 juillet 2008.,,,2) a) La seule invocation de la complexité des procédés techniques à mettre en oeuvre ne peut suffire à justifier légalement le recours au contrat de partenariat, en l'absence de circonstances particulières de nature à établir qu'il était impossible à la collectivité territoriale de définir, seule et à l'avance, les moyens techniques propres à satisfaire ses besoins.,,,b) Pour apprécier la capacité objective de la personne publique à définir seule et à l'avance les moyens techniques permettant de répondre à ses besoins et, par suite, pour déterminer si la complexité technique du projet justifiait légalement le recours au contrat de partenariat, il n'y a pas lieu de tenir compte des études postérieures au lancement de la procédure de passation du contrat que cette personne publique serait en mesure de confier à un tiers, soit dans le cadre du contrat de partenariat qu'elle envisage de conclure, soit au titre d'un contrat distinct. En revanche, il y a lieu de tenir compte de l'ensemble des études, même réalisées par des tiers, dont la personne publique dispose déjà à la date à laquelle elle décide de recourir au contrat de partenariat.</ANA>
<ANA ID="9B"> 39-08 1) Le moyen tiré de ce que les conditions de recours au contrat de partenariat ne sont pas réunies peut être utilement soulevé à l'appui d'un recours dirigé contre l'acte par lequel la signature d'un tel contrat est autorisée.,,,2) Le respect des conditions posées par la loi s'apprécie au vu de l'évaluation préalable qui a donné lieu à la délibération prévue par le dernier alinéa de l'article L. 1414-2 du CGCT, dans sa rédaction antérieure à la loi n° 2008-735 du 28 juillet 2008.</ANA>
<ANA ID="9C"> 54-07-01-04-03 Le moyen tiré de ce que les conditions de recours au contrat de partenariat ne sont pas réunies peut être utilement soulevé à l'appui d'un recours dirigé contre l'acte par lequel la signature d'un tel contrat est autorisée.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
