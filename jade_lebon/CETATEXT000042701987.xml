<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042701987</ID>
<ANCIEN_ID>JG_L_2020_12_000000430592</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/70/19/CETATEXT000042701987.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 5ème chambres réunies, 17/12/2020, 430592</TITRE>
<DATE_DEC>2020-12-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>430592</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, TEXIDOR, PERIER</AVOCATS>
<RAPPORTEUR>Mme Catherine Calothy</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier  Fuchs</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:430592.20201217</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Smurfit Kappa Papier Recycle France (SKPRF) a demandé au tribunal administratif de Nîmes d'annuler pour excès de pouvoir l'arrêté des préfets du Gard et de Vaucluse en date du 13 décembre 2013 approuvant le plan de prévention des risques technologiques autour de l'établissement EURENCO à Sorgues. Par un jugement n° 1400660 du 28 juin 2016, le tribunal administratif de Nîmes a annulé cet arrêté.<br/>
<br/>
              Par un arrêt n° 16MA03481 du 8 mars 2019, la cour administrative d'appel de Marseille a rejeté l'appel formé par le ministre de la transition écologique et solidaire contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire enregistrés les 9 mai et 7 août 2019 au secrétariat du contentieux du Conseil d'Etat, le ministre de la transition écologique et solidaire demande au Conseil d'Etat d'annuler cet arrêt. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'environnement ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme B... A..., maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Olivier Fuchs, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Célice, Texidor, Perier, avocat de la société Smurfit Kappa Papier Recycle France ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la société Smurfit Kappa Papier Recycle France (SKPRF) exploite une activité de papeterie à proximité du site d'implantation d'une installation classée, exploitée par la société Eurenco France, de fabrication d'explosifs militaires et de liquides inflammables additifs des carburants sur le territoire de la commune de Sorgues. Les préfets de Vaucluse et du Gard ont approuvé par arrêté du 13 décembre 2013 un plan de prévention des risques technologiques pour ce site industriel. Saisi par la société SKPRF, le tribunal administratif de Nîmes a, par jugement du 28 juin 2016, annulé cet arrêté au motif que le commissaire enquêteur n'avait pas motivé l'avis favorable qu'il avait émis au terme de l'enquête publique, en méconnaissance des exigences de l'article R. 123-19 du code de l'environnement dans sa rédaction alors applicable. La cour administrative d'appel de Marseille a rejeté l'appel formé par le ministre de la transition écologique et solidaire contre ce jugement par un arrêt du 8 mars 2019 contre lequel le ministre se pourvoit en cassation.<br/>
<br/>
              2. En premier lieu, les inexactitudes, omissions ou insuffisances d'une enquête publique ne sont susceptibles de vicier la procédure et donc d'entraîner l'illégalité de la décision prise à l'issue de cette enquête publique que si elles ont pu avoir pour effet de nuire à l'information complète de la population ou si elles ont été de nature à exercer une influence sur la décision de l'autorité administrative.<br/>
<br/>
              3. Il ressort des termes de l'arrêt attaqué que la cour administrative d'appel, après avoir retenu que l'avis favorable émis par le commissaire enquêteur à l'issue de l'enquête publique ne comportait pas la motivation requise par l'article R. 123-19 du code de l'environnement alors applicable, a jugé que cette insuffisance de motivation avait entaché la procédure d'enquête publique d'irrégularité. En se fondant sur un tel motif pour juger illégal l'arrêté attaqué, sans rechercher si, ainsi que le faisait valoir le ministre à l'appui de son appel, cette insuffisance avait pu avoir pour effet de nuire à l'information complète de la population ou si elle avait été de nature à exercer une influence sur la décision de l'autorité administrative, la cour a entaché son arrêt d'une insuffisance de motivation. <br/>
<br/>
              4. En second lieu, l'annulation d'un acte administratif implique en principe que cet acte est réputé n'être jamais intervenu. Toutefois, s'il apparaît que cet effet rétroactif de l'annulation est de nature à emporter des conséquences manifestement excessives en raison tant des effets que cet acte a produits et des situations qui ont pu se constituer lorsqu'il était en vigueur, que de l'intérêt général pouvant s'attacher à un maintien temporaire de ses effets, il appartient au juge administratif - après avoir recueilli sur ce point les observations des parties et examiné l'ensemble des moyens, d'ordre public ou invoqués devant lui, pouvant affecter la légalité de l'acte en cause - de prendre en considération, d'une part, les conséquences de la rétroactivité de l'annulation pour les divers intérêts publics ou privés en présence et, d'autre part, les inconvénients que présenterait, au regard du principe de légalité et du droit des justiciables à un recours effectif, une limitation dans le temps des effets de l'annulation. Il lui revient d'apprécier, en rapprochant ces éléments, s'ils peuvent justifier qu'il soit dérogé au principe de l'effet rétroactif des annulations contentieuses et, dans l'affirmative, de prévoir dans sa décision d'annulation que, sous réserve des actions contentieuses engagées à la date de sa décision, tout ou partie des effets de cet acte antérieurs à son annulation devront être regardés comme définitifs ou même, le cas échéant, que l'annulation ne prendra effet qu'à une date ultérieure qu'il détermine.<br/>
<br/>
              5. Lorsque le juge d'appel est saisi d'un jugement ayant annulé un acte administratif et qu'il rejette l'appel formé contre ce jugement en ce qu'il a jugé illégal l'acte administratif, la circonstance que l'annulation ait été prononcée par le tribunal administratif avec un effet rétroactif ne fait pas obstacle à ce que le juge d'appel, saisi dans le cadre de l'effet dévolutif, apprécie, conformément à ce qui a été dit au point 4 et à la date à laquelle il statue, s'il y a lieu de déroger en l'espèce au principe de l'effet rétroactif de l'annulation contentieuse et détermine, en conséquence, les effets dans le temps de l'annulation, en réformant le cas échéant sur ce point le jugement de première instance.<br/>
<br/>
              6. Il s'ensuit que le ministre est fondé à soutenir que la cour administrative d'appel de Marseille a commis une erreur de droit en jugeant qu'il ne lui appartenait pas de décider de différer dans le temps les effets de l'annulation de l'arrêté des préfets de Vaucluse et du Gard en date du 13 décembre 2013 au motif que cet arrêté avait été annulé par le jugement du tribunal administratif et non par son arrêt. <br/>
<br/>
              7. Il résulte de tout ce qui précède, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, que le ministre de la transition écologique et solidaire est fondé à demander l'annulation de l'arrêt qu'il attaque. <br/>
<br/>
              8. Les dispositions de l'article L. 761-1 font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative de Marseille est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Marseille.<br/>
Article 3 : Les conclusion de la société Smurfit Kappa Papier Recycle France présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à la ministre de la transition écologique et à la société Smurfit Kappa Papier Recycle France.<br/>
Copie en sera adressée à la société Eurenco France.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-07-023 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. - FACULTÉ POUR LE JUGE D'APPEL QUI CONFIRME UN JUGEMENT PRONONÇANT UNE ANNULATION [RJ1] - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-08-01-04-01 PROCÉDURE. VOIES DE RECOURS. APPEL. EFFET DÉVOLUTIF ET ÉVOCATION. EFFET DÉVOLUTIF. - FACULTÉ, POUR LE JUGE D'APPEL QUI CONFIRME UN JUGEMENT PRONONÇANT UNE ANNULATION, DE MODULER SES EFFETS DANS LE TEMPS [RJ1] - EXISTENCE.
</SCT>
<ANA ID="9A"> 54-07-023 Lorsque le juge d'appel est saisi d'un jugement ayant annulé un acte administratif et qu'il rejette l'appel formé contre ce jugement en ce qu'il a jugé illégal l'acte administratif, la circonstance que l'annulation ait été prononcée par le tribunal administratif avec un effet rétroactif ne fait pas obstacle à ce que le juge d'appel, saisi dans le cadre de l'effet dévolutif, apprécie, à la date à laquelle il statue, s'il y a lieu de déroger en l'espèce au principe de l'effet rétroactif de l'annulation contentieuse et détermine, en conséquence, les effets dans le temps de l'annulation, en réformant le cas échéant sur ce point le jugement de première instance.</ANA>
<ANA ID="9B"> 54-08-01-04-01 Lorsque le juge d'appel est saisi d'un jugement ayant annulé un acte administratif et qu'il rejette l'appel formé contre ce jugement en ce qu'il a jugé illégal l'acte administratif, la circonstance que l'annulation ait été prononcée par le tribunal administratif avec un effet rétroactif ne fait pas obstacle à ce que le juge d'appel, saisi dans le cadre de l'effet dévolutif, apprécie, à la date à laquelle il statue, s'il y a lieu de déroger en l'espèce au principe de l'effet rétroactif de l'annulation contentieuse et détermine, en conséquence, les effets dans le temps de l'annulation, en réformant le cas échéant sur ce point le jugement de première instance.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., sur le principe de la modulation et ses conditions, CE, Assemblée, 11 mai 2004, Association AC !, n°s 255886 à 255892, p. 197 ; CE, Assemblée, 23 décembre 2013, Société Métropole télévision (M6) et Société Télévision Française 1 (TF1), n°s 363702 363719, p. 322.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
