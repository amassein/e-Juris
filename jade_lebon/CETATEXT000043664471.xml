<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043664471</ID>
<ANCIEN_ID>JG_L_2021_06_000000417940</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/66/44/CETATEXT000043664471.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 1ère chambres réunies, 14/06/2021, 417940</TITRE>
<DATE_DEC>2021-06-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>417940</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Appréciation de la légalité</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, TEXIDOR, PERIER ; SARL DIDIER-PINET</AVOCATS>
<RAPPORTEUR>Mme Catherine Brouard-Gallet</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Raphaël Chambon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:417940.20210614</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une décision n° 417940 du 29 juin 2020, le Conseil d'Etat, statuant au contentieux, a, sur le pourvoi de la société Papeteries du Léman, d'une part, annulé le jugement n° 1705010 du 22 janvier 2018 du tribunal administratif de Grenoble et, d'autre part, sursis à statuer sur la question de la légalité de la décision du 6 novembre 2012 de l'inspecteur du travail de la 1ère section de l'unité territoriale de la Haute-Savoie ayant autorisé la société Papeteries du Léman à licencier M. C... E..., renvoyée par un arrêt n° 16/01841 du 29 juin 2017 de la cour d'appel de Chambéry, ainsi que sur l'ensemble des conclusions présentées au titre de l'article L. 761-1 du code de justice administrative et rouvert l'instruction afin de permettre un débat contradictoire. <br/>
<br/>
              Par un mémoire, enregistré le 15 septembre 2020 au secrétariat du contentieux du Conseil d'Etat, la société Papeteries du Léman demande au Conseil d'Etat :<br/>
<br/>
              1°) de déclarer que la décision de l'inspecteur du travail du 6 novembre 2012 autorisant le licenciement de M. E... n'est pas entachée d'illégalité ;<br/>
<br/>
              2°) de mettre à la charge de M. E... la somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de commerce ; <br/>
              - le code de procédure civile ; <br/>
              - le code du travail ; <br/>
              - l'ordonnance n° 2015-1576 du 3 décembre 2015 ;<br/>
              - l'ordonnance n° 2017-1387 du 22 septembre 2017 ;<br/>
              - le code de justice administrative ; <br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme D... F..., conseillère d'Etat en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Raphaël Chambon, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Célice, Texidor, Perier, avocat de la société Papeteries du Leman et à la SARL Didier-Pinet, avocat de M. E... ;<br/>
<br/>
<br/>
              Vu la note en délibéré, enregistrée le 28 mai 2021, présentée par la société Papeteries du Léman ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Par une décision du 6 novembre 2012, l'inspecteur du travail de la 1ère section de l'unité territoriale de la Haute-Savoie a autorisé la société Papeteries du Léman (PDL) à licencier M. E..., salarié protégé. A la suite de son licenciement, M. E... a demandé au conseil de prud'hommes d'Annemasse de déclarer son licenciement nul ou sans cause réelle et sérieuse, et de l'indemniser. Par un jugement du 8 août 2016, le conseil des prud'hommes a rejeté sa demande. Sur appel de M. E..., la cour d'appel de Chambéry a, par un arrêt du 29 juin 2017, sursis à statuer et décidé de saisir le tribunal administratif de Grenoble de la question de la légalité de la décision de l'inspecteur du travail. Par une décision n° 417940 du 29 juin 2020, le Conseil d'Etat, statuant au contentieux, a, sur le pourvoi de la société Papeteries du Léman, d'une part, annulé le jugement du tribunal administratif de Grenoble du 22 janvier 2018 ayant déclaré que cette décision était entachée d'illégalité et, d'autre part, sursis à statuer sur la question préjudicielle ainsi renvoyée. <br/>
<br/>
              Sur la fin de non-recevoir opposée par la société Papeterie du Léman :<br/>
<br/>
              2. Aux termes du second alinéa de l'article 49 du code de procédure civile : " Lorsque la solution d'un litige dépend d'une question soulevant une difficulté sérieuse et relevant de la compétence de la juridiction administrative, la juridiction judiciaire initialement saisie la transmet à la juridiction administrative compétente en application du titre Ier du livre III du code de justice administrative. Elle sursoit à statuer jusqu'à la décision sur la question préjudicielle ".<br/>
<br/>
              3. Le tribunal administratif de Grenoble ayant été saisi de la question préjudicielle mentionnée au point 1 par la transmission, le 16 août 2017, par la cour d'appel de Chambéry, de l'arrêt rendu par cette juridiction le 29 juin 2017, conformément aux dispositions du second alinéa de l'article 49 du code de procédure civile citées ci-dessus, la circonstance que M. E... ait saisi le tribunal administratif de Grenoble de la même question préjudicielle dès le 7 août 2017, soit antérieurement à la transmission effectuée par la juridiction judiciaire, ne saurait affecter d'irrégularité la saisine du juge administratif. Dès lors, la société Papeterie du Léman n'est en tout état de cause pas fondée à soutenir que la saisine du juge administratif est irrégulière. <br/>
<br/>
              Sur la portée de la question préjudicielle : <br/>
<br/>
              4. En vertu des principes généraux relatifs à la répartition des compétences entre les deux ordres de juridiction, il n'appartient pas à la juridiction administrative, lorsqu'elle est saisie d'une question préjudicielle en appréciation de validité d'un acte administratif, de trancher d'autres questions que celle qui lui a été renvoyée par l'autorité judiciaire. Il suit de là que, lorsque la juridiction de l'ordre judiciaire a énoncé dans son jugement le ou les moyens invoqués devant elle qui lui paraissent justifier ce renvoi, la juridiction administrative doit limiter son examen à ce ou ces moyens et ne peut connaître d'aucun autre, fût-il d'ordre public, que les parties viendraient à présenter devant elle à l'encontre de cet acte. Ce n'est que dans le cas où, ni dans ses motifs ni dans son dispositif, la juridiction de l'ordre judiciaire n'a limité la portée de la question qu'elle entend soumettre à la juridiction administrative, que cette dernière doit examiner tous les moyens présentés devant elle, sans qu'il y ait lieu alors de rechercher si ces moyens avaient été invoqués dans l'instance judiciaire.<br/>
<br/>
              5. Avant de surseoir à statuer, la cour d'appel de Chambéry a relevé dans les motifs de son arrêt que M. E... soutenait que l'inspecteur du travail avait inexactement apprécié le motif économique de son licenciement, en se fondant sur la situation économique de l'ensemble des sociétés intervenant dans le même secteur d'activité que la société Papeterie du Léman dans le périmètre du seul groupe PVL Holdings, alors que la société Papeterie du Léman relève, en réalité, d'un groupe plus étendu, détenu par M. A... B... et comportant notamment plusieurs sociétés produisant également du papier à cigarette. En mentionnant ce seul moyen, la cour d'appel de Chambéry a défini et limité l'étendue de la question qu'elle entendait soumettre à la juridiction administrative. Dès lors, il n'y a pas lieu d'examiner les moyens présentés par M. E... tenant à l'insuffisante motivation de la décision de l'inspecteur du travail et à son inexacte appréciation de l'accomplissement par l'employeur de son obligation en matière de reclassement.<br/>
<br/>
              Sur la légalité de la décision de l'inspecteur du travail du 6 novembre 2012 :<br/>
<br/>
              6. En vertu des dispositions du code du travail, le licenciement des salariés qui bénéficient d'une protection exceptionnelle dans l'intérêt de l'ensemble des travailleurs qu'ils représentent ne peut intervenir que sur autorisation de l'inspecteur du travail. Lorsque le licenciement d'un de ces salariés est envisagé, ce licenciement ne doit pas être en rapport avec les fonctions représentatives normalement exercées ou l'appartenance syndicale de l'intéressé. Dans le cas où la demande de licenciement est fondée sur un motif de caractère économique, il appartient à l'inspecteur du travail et, le cas échéant, au ministre, de rechercher, sous le contrôle du juge de l'excès de pouvoir, si la situation de l'entreprise justifie le licenciement du salarié, en tenant compte notamment de la nécessité des réductions envisagées d'effectifs et de la possibilité d'assurer le reclassement du salarié dans l'entreprise ou au sein du groupe auquel appartient cette dernière. Pour apprécier la réalité des motifs économiques allégués à l'appui d'une demande d'autorisation de licenciement d'un salarié protégé présentée par une société qui fait partie d'un groupe, l'autorité administrative est tenue de faire porter son examen sur la situation économique de l'ensemble des sociétés du groupe intervenant dans le même secteur d'activité que la société en cause. <br/>
<br/>
              7. A ce titre, le groupe s'entend, ainsi qu'il est dit au I de l'article L. 2331-1 du code du travail, de l'ensemble constitué par les entreprises placées sous le contrôle d'une même entreprise dominante dans les conditions définies à l'article L. 233-1, aux I et II de l'article L. 233-3 et à l'article L. 233-16 du code de commerce. A cet égard, une personne physique doit, au même titre qu'une personne morale, être considérée comme en contrôlant une autre dès lors qu'elle remplit les conditions visées à l'article L. 233-3 du code de commerce, y compris sous l'empire de la rédaction de cet article antérieure à l'entrée en vigueur de l'ordonnance du 3 décembre 2015 portant transposition de la directive 2013/50/UE du Parlement européen et du Conseil du 22 octobre 2013 modifiant la directive 2004/109/CE du Parlement européen et du Conseil sur l'harmonisation des obligations de transparence concernant l'information sur les émetteurs dont les valeurs mobilières sont admises à la négociation sur un marché réglementé. Toutes les entreprises ainsi placées sous le contrôle d'une même personne physique ou morale sont prises en compte, quel que soit le lieu d'implantation de leur siège, tant que ne sont pas applicables à la décision attaquée les dispositions introduites par l'article 15 de l'ordonnance du 22 septembre 2017 relative à la prévisibilité et la sécurisation des relations de travail à l'article L. 1233-3 du code du travail en vertu desquelles seules les entreprises implantées en France doivent alors être prises en considération. <br/>
<br/>
              8. Il ressort des pièces du dossier que l'inspecteur du travail a estimé que le motif économique du licenciement de M. E... était justifié, en se fondant sur la situation économique de la société Papeteries des Vosges (PDV) et sur celle de la société PDL, ces deux sociétés relevant d'un même secteur d'activité constitué par la production et la commercialisation de papier fin, notamment de papier à cigarette, et appartenant l'une et l'autre au groupe PVL Holdings. M. E... fait valoir que la société PVL Holdings, dont la société PDL et la société PDV sont des filiales, est détenue par deux sociétés américaines appartenant à M. A... B... et que M. A... B... détient en outre directement ou indirectement plusieurs autres sociétés dont les activités sont proches de celles de la société PDL, parmi lesquelles les sociétés du groupe Republic Technologies International (RTI), lequel comprend non seulement la société Republic Technologies France (RTF) dont le siège est à Perpignan, mais aussi la société Altesse en Autriche et la société Productos tecnologicos catalanes (PTC) en Espagne. M. E... en déduit que le périmètre d'appréciation du motif économique de son licenciement est constitué par l'ensemble des entreprises relevant du même secteur d'activité que la société PDL et se trouvant sous le contrôle de M. A... B..., qui peut être considéré comme exerçant le contrôle effectif sur ces sociétés au sens de l'article L. 2331-1 du code du travail dans les conditions rappelées au point précédent, y compris celles du groupe Republic Technologies International. <br/>
<br/>
              9. Dans le cadre du débat contradictoire qui s'est tenu devant le tribunal administratif de Grenoble puis devant le Conseil d'Etat, statuant au contentieux, après sa décision avant dire droit du 29 juin 2020, si M. E... a fourni, au regard des éléments auxquels il pouvait avoir accès, une argumentation circonstanciée sur le groupe ici en cause, la société PDL s'est bornée à soutenir qu'un groupe ne pouvait alors être détenu par une personne physique - ce qui, comme il a été dit précédemment, ne peut être retenu - et n'a pas produit d'élément concret relatif aux entreprises détenues par M. A... B... alors qu'elle était nécessairement en mesure de produire de telles informations. En conséquence, elle n'a pas permis qu'il puisse être statué sur la consistance du groupe au sein duquel le motif économique du licenciement de M. E... doit être apprécié. Dans ces conditions, M. E... est fondé à soutenir que la réalité du motif économique fondant son licenciement n'est pas établie. <br/>
<br/>
              10. Il résulte de ce qui précède qu'il y a lieu de déclarer illégale la décision du 6 novembre 2012 de l'inspecteur du travail autorisant le licenciement de M. E.... <br/>
<br/>
              11. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'il soit fait droit aux conclusions présentées par la société Papeteries du Léman. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société Papeteries du Léman une somme de 3 000 euros à verser à M. E... au titre des mêmes dispositions. <br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il est déclaré que la décision de l'inspecteur du travail du 6 novembre 2012 est entachée d'illégalité.<br/>
Article 2 : La société Papeteries du Léman versera à M. E... une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à M. C... E..., à la société Papeteries du Léman, à la cour d'appel de Chambéry et à la ministre du travail, de l'emploi et de l'insertion.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">66-07-01-04-03 TRAVAIL ET EMPLOI. LICENCIEMENTS. AUTORISATION ADMINISTRATIVE - SALARIÉS PROTÉGÉS. CONDITIONS DE FOND DE L'AUTORISATION OU DU REFUS D'AUTORISATION. LICENCIEMENT POUR MOTIF ÉCONOMIQUE. - APPRÉCIATION DE LA RÉALITÉ DES MOTIFS ÉCONOMIQUES LORSQUE LA SOCIÉTÉ FAIT PARTIE D'UN GROUPE - 1) NOTION DE GROUPE [RJ1] - INCLUSION - ENSEMBLE DES ENTREPRISES PLACÉES SOUS LE CONTRÔLE D'UNE PERSONNE PHYSIQUE [RJ2] - 2) ADMINISTRATION DE LA PREUVE [RJ3] - SALARIÉ FOURNISSANT UNE ARGUMENTATION CIRCONSTANCIÉE SUR LE GROUPE EN CAUSE - EMPLOYEUR NE FOURNISSANT AUCUN ÉLÉMENT SUR LA CONSISTANCE DE CE GROUPE, ALORS QU'IL ÉTAIT EN MESURE DE LE FAIRE - CONSÉQUENCE - MOTIF ÉCONOMIQUE NON ÉTABLI.
</SCT>
<ANA ID="9A"> 66-07-01-04-03 Pour apprécier la réalité des motifs économiques allégués à l'appui d'une demande d'autorisation de licenciement d'un salarié protégé présentée par une société qui fait partie d'un groupe, l'autorité administrative est tenue de faire porter son examen sur la situation économique de l'ensemble des sociétés du groupe intervenant dans le même secteur d'activité que la société en cause.... ,,1) A ce titre, le groupe s'entend, ainsi qu'il est dit au I de l'article L. 2331-1 du code du travail, de l'ensemble constitué par les entreprises placées sous le contrôle d'une même entreprise dominante dans les conditions définies à l'article L. 233 1, aux I et II de l'article L. 233-3 et à l'article L. 233-16 du code de commerce.... ,,A cet égard, une personne physique doit, au même titre qu'une personne morale, être considérée comme en contrôlant une autre dès lors qu'elle remplit les conditions visées à l'article L. 233-3 du code de commerce, y compris sous l'empire de la rédaction de cet article antérieure à l'entrée en vigueur de l'ordonnance n° 2015-1576 du 3 décembre 2015.... ,,2) Salarié licencié ayant, dans le cadre du débat contradictoire devant le juge, fourni, au regard des éléments auxquels il pouvait avoir accès, une argumentation circonstanciée sur le groupe en cause, soutenant que son employeur relevait en réalité d'un groupe, détenu par une personne physique, plus étendu que celui retenu par l'inspecteur du travail.,, ...Employeur s'étant borné à soutenir qu'un groupe ne pouvait être détenu par une personne physique et n'ayant pas produit d'élément concret relatif aux entreprises détenues par la personne physique identifiée par le salarié, alors qu'il était nécessairement en mesure de produire de telles informations.... ,,En conséquence, l'employeur pas permis qu'il puisse être statué sur la consistance du groupe au sein duquel le motif économique du licenciement du salarié doit être apprécié.... ,,Dans ces conditions, la réalité du motif économique fondant le licenciement n'est pas établi, et il y a lieu de déclarer illégale la décision de l'inspecteur du travail autorisant ce licenciement.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 29 juin 2020, Société Papeteries du Léman, n° 417940, T. p. 1037.,,[RJ2] Rappr. Cass. soc. 21 septembre 2017, n° 16-23.223, Bull. 2017, V, n° 149,,[RJ3] Rappr., sur les modalités d'établissement de la preuve de la réalité et du sérieux du motif économique, Cass. soc., 31 mars 2021, n° 19-26.054, à publier au Bulletin.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
