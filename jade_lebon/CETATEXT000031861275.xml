<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031861275</ID>
<ANCIEN_ID>JG_L_2015_12_000000384321</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/86/12/CETATEXT000031861275.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème / 8ème SSR, 30/12/2015, 384321, Publié au recueil Lebon</TITRE>
<DATE_DEC>2015-12-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>384321</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème / 8ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Célia Verot</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:384321.20151230</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 8 septembre et 4 décembre 2014 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision implicite par laquelle le ministre des affaires étrangères a refusé " d'exclure l'Institut pour le commerce extérieur italien (ICE) de la liste des bénéficiaires du statut diplomatique en France " ;<br/>
<br/>
              2°) d'enjoindre au ministre des affaires étrangères " d'exclure l'ICE de la liste des  bénéficiaires du statut diplomatique en France " dans un délai d'un mois sous astreinte de 500 euros par jour de retard.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - la convention de Vienne du 18 avril 1961 et la loi n° 69-1039 du 20 novembre 1969 autorisant sa ratification ;<br/>
              - le pacte international relatif aux droits civils et politiques ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Célia Verot, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article 2 de la convention de Vienne du 18 avril 1961 sur les relations diplomatiques : " L'établissement de relations diplomatiques entre Etats et l'envoi de missions diplomatiques permanentes se font par consentement mutuel. " ; que son article 3 stipule : " Les fonctions d'une mission diplomatique consistent notamment à : a) Représenter l'Etat accréditant auprès de l'Etat accréditaire ; b) Protéger dans l'Etat accréditaire les intérêts de l'Etat accréditant et de ses ressortissants, dans les limites admises par le droit international ; c) Négocier avec le gouvernement de l'Etat accréditaire ; d) S'informer par tous les moyens licites des conditions et de l'évolution des évènements dans l'Etat accréditaire et faire rapport à ce sujet au gouvernement de l'Etat accréditant ; e) Promouvoir des relations amicales et développer les relations économiques, culturelles et scientifiques entre l'Etat accréditant et l'Etat accréditaire " ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier que, par une " note verbale " du 13 juillet 2004, l'ambassadeur d'Italie en France a informé le ministre des affaires étrangères que les bureaux à l'étranger de l'Institut pour le commerce extérieur italien (ICE) avaient été intégrés dans le réseau diplomatique italien en qualité de " sections pour la promotion des échanges commerciaux ", si bien que le bureau de l'ICE situé à Paris devait être considéré comme une section détachée de son ambassade, et l'a prié de bien vouloir l'accréditer en tant que telle ; que, à la suite d'échanges entre l'ambassade d'Italie et le ministère des affaires étrangères, le ministre des affaires étrangères a informé l'ambassadeur d'Italie, par une " note verbale " du 30 juin 2005, que les autorités françaises n'émettaient pas d'objection au changement de statut de l'ICE et qu'il avait pris bonne note que la " section pour la promotion des échanges " située à Paris faisait donc partie intégrante de la représentation diplomatique italienne en France et bénéficiait à ce titre des stipulations de la convention de Vienne du 18 avril 1961 ;<br/>
<br/>
              3. Considérant que M. A...demande l'annulation pour excès de pouvoir de la décision implicite, née le 6 août 2014, par laquelle le ministre des affaires étrangères a refusé de mettre fin à sa décision, contenue dans la " note verbale " du 30 juin 2005, de regarder l'ICE comme partie intégrante de la mission diplomatique permanente de l'Italie en France ; <br/>
<br/>
              4. Considérant qu'eu égard à son objet, la décision du ministre des affaires étrangères de reconnaître le statut diplomatique de l'ICE n'est pas détachable de la conduite des relations internationales de la France ; qu'elle échappe, par suite, à la compétence de la juridiction administrative française, sans que soit méconnu le droit au recours dont M. A...se prévaut en invoquant les articles 13 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et 1er du premier protocole additionnel à cette convention ; qu'ainsi la requête de M. A...doit être rejetée comme portée devant une juridiction incompétente pour en connaître ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. A...est rejetée comme portée devant une juridiction incompétente pour en connaître.<br/>
Article 2 : La présente décision sera notifiée à M. B...A...et au ministre des affaires étrangères et du développement international.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">17-02-02-02 COMPÉTENCE. ACTES ÉCHAPPANT À LA COMPÉTENCE DES DEUX ORDRES DE JURIDICTION. ACTES DE GOUVERNEMENT. ACTES CONCERNANT LES RELATIONS INTERNATIONALES. - DÉCISION DU MINISTRE DES AFFAIRES ÉTRANGÈRES DE RECONNAÎTRE LE STATUT DIPLOMATIQUE D'UNE INSTITUTION ÉTRANGÈRE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">26-055-01-13 DROITS CIVILS ET INDIVIDUELS. CONVENTION EUROPÉENNE DES DROITS DE L'HOMME. DROITS GARANTIS PAR LA CONVENTION. DROIT À UN RECOURS EFFECTIF (ART. 13). - MÉCONNAISSANCE - ABSENCE - ACTE DE GOUVERNEMENT ÉCHAPPANT À LA COMPÉTENCE DE LA JURIDICTION ADMINISTRATIVE FRANÇAISE.
</SCT>
<ANA ID="9A"> 17-02-02-02 Eu égard à son objet, la décision du ministre des affaires étrangères de reconnaître le statut diplomatique d'une institution étrangère n'est pas détachable de la conduite des relations internationales de la France. Elle échappe, par suite, à la compétence de la juridiction administrative française sans que soit méconnu le droit au recours garanti par l'article 13 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales (CEDH).</ANA>
<ANA ID="9B"> 26-055-01-13 Eu égard à son objet, la décision du ministre des affaires étrangères de reconnaître le statut diplomatique d'une institution étrangère n'est pas détachable de la conduite des relations internationales de la France. Elle échappe, par suite, à la compétence de la juridiction administrative française sans que soit méconnu le droit au recours garanti par l'article 13 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales (CEDH).</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
