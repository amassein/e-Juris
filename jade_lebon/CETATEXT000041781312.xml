<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041781312</ID>
<ANCIEN_ID>JG_L_2020_03_000000426623</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/78/13/CETATEXT000041781312.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 27/03/2020, 426623</TITRE>
<DATE_DEC>2020-03-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>426623</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUVENIN, COUDRAY, GREVY ; SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>Mme Myriam Benlolo Carabot</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:426623.20200327</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
         L'Association contre l'extension et les nuisances de l'aéroport de Lyon Saint-Exupéry (ACENAS) et Madame B... A... ont demandé au tribunal administratif de Paris, d'une part, d'annuler pour excès de pouvoir la décision du 1er juin 2017 de l'Agence des participations de l'Etat (APE) rejetant partiellement leur demande de communication de documents administratifs relatifs à la procédure de cession par l'Etat de sa participation majoritaire au capital de la société Aéroports de Lyon, d'autre part, d'enjoindre au Premier ministre de communiquer la réponse des candidats, le choix du candidat, toutes les pièces se rapportant aux conditions de vente des actions, notamment le contrat de vente et l'engagement d'investissement, dans le cadre de la privatisation de l'aéroport de Lyon Saint-Exupéry, dans un délai d'un mois à compter de la notification du jugement. <br/>
<br/>
         Par un jugement n° 1712342/5-1 du 18 octobre 2018, le tribunal a rejeté cette demande. <br/>
<br/>
         Par une ordonnance n° 18PA03769 du 19 décembre 2018, enregistrée le 11 février 2019 au secrétariat du contentieux du Conseil d'Etat, le président de la cour administrative d'appel de Paris a transmis au Conseil d'Etat, en application des dispositions de l'article R. 351-2 du code de justice administrative, le pourvoi, enregistré le 3 décembre 2018 au greffe de cette cour, présenté par l'ACENAS et Mme A.... <br/>
<br/>
         Par ce pourvoi et un mémoire en réplique, enregistré le 9 janvier 2020, l'ACENAS et Mme A... demandent au Conseil d'Etat :<br/>
<br/>
         1°) d'annuler le jugement du 18 octobre 2018 du tribunal administratif de Paris et faire droit à leur demande ;<br/>
<br/>
         2°) de mettre à la charge de l'Etat la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
         Vu les autres pièces du dossier ; <br/>
<br/>
         Vu :<br/>
         - la directive n° 2016/943/UE du 8 juin 2016 du Parlement européen et du Conseil <br/>
         - le code des relations entre le public et l'administration ;<br/>
         - le code de justice administrative. <br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Myriam Benlolo Carabot, maître des requêtes en service extraordinaire,<br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Thouvenin, Coudray, Grevy, avocat de l'association cContre l'extension et les nuisances de l'aéroport de Lyon-st-Exupery et à la SCP Piwnica, Molinié, avocat du ministre de l'économie et des finances ;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que l'Association contre l'extension et les nuisances de l'aéroport de Lyon Saint-Exupéry (ACENAS) et Mme A... ont saisi le Premier ministre le 2 novembre 2016 d'une demande tendant à la communication de l'intégralité du dossier relatif à la cession au secteur privé de la participation majoritaire de l'Etat dans le capital de la société Aéroports de Lyon, notamment " les pièces de la mise en concurrence, la réponse des candidats, le choix du candidat, le cahier des charges et toute pièce se rapportant aux conditions de la vente des actions ". Le 9 mars 2017, la commission d'accès aux documents administratifs (CADA) a donné un avis favorable à la communication des documents sollicités, " sous réserve de l'occultation des informations protégées par le secret industriel et commercial, en l'espèce les mentions et données relatives à la situation économique et financière des entreprises candidates, notamment les bilans, comptes de résultat et les éléments relatifs au chiffre d'affaires et au niveau d'activité, ainsi que les données révélant les stratégies commerciales, procédés techniques et savoir-faire de ces sociétés ". Le 1er juin 2017, le commissaire aux participations de l'Etat de l'Agence des participations de l'Etat (APE) a communiqué aux requérantes le cahier des charges de la procédure de cession, les cinq avis rendus par la Commission des participations et des transferts entre mars et octobre 2016, notamment celui du 27 juillet 2016 qui présente les projets industriels et commerciaux des deux acquéreurs candidats, ainsi que l'arrêté du 3 novembre 2016 qui fixe les modalités de transfert au secteur privé de la participation majoritaire de l'Etat au capital de la société Aéroport de Lyon et précise le prix par action retenu. L'APE a cependant indiqué qu'il ne lui était pas possible de communiquer les offres des candidats au motif qu'elles étaient protégées par le secret industriel et commercial. <br/>
<br/>
              2. Par un jugement du 18 octobre 2018, contre lequel elles se pourvoient en cassation, le tribunal administratif de Paris a rejeté la demande de l'ACENAS et de Mme A... tendant à l'annulation pour excès de pouvoir de la décision du 1er juin 2017 de l'APE et à ce qu'il soit enjoint au Premier ministre de communiquer la réponse des candidats, le choix du candidat, toutes les pièces se rapportant aux conditions de vente des actions, notamment le contrat de vente et " l'engagement d'investissement ". <br/>
<br/>
              3. L'article L. 300-1 du code des relations entre le public et l'administration dispose que : " Le droit de toute personne à l'information est précisé et garanti par les dispositions des titres Ier, III et IV du présent livre en ce qui concerne la liberté d'accès aux documents administratifs.". Aux termes de l'article L. 300-2 du même code : " Sont considérés comme documents administratifs, au sens des titres Ier, III et IV du livre, quels que soient leur date, leur lieu de conservation, leur forme et leur support, les documents produits ou reçus, dans le cadre de leur mission de service public, par l'Etat, les collectivités territoriales ainsi que par les autres personnes de droit public ou les personnes de droit privé chargées d'une telle mission. Constituent de tels documents notamment les dossiers, rapports, études, comptes rendus, procès-verbaux, statistiques, instructions, circulaires, notes et réponses ministérielles, correspondances, avis, prévisions codes sources et décisions. (...) ". L'article L. 311-1 du même code dispose que : " Sous réserve des dispositions des articles L. 311-5 et L. 311-6, les administrations mentionnées à l'article L. 300-2 sont tenues de publier en ligne ou de communiquer les documents administratifs qu'elles détiennent aux personnes qui en font la demande, dans les conditions prévues par le présent livre.". Aux termes de l'article L. 311-6 du même code, dans sa rédaction applicable à la date de la décision attaquée : " Ne sont communicables qu'à l'intéressé les documents administratifs : / - dont la communication porterait atteinte à la protection de la vie privée, au secret médical et au secret en matière commerciale et industrielle lequel comprend le secret des procédés, des informations économiques et financières et des stratégies commerciales ou industrielles (...) ". L'article L. 311-7 du même code dispose que : " Lorsque la demande porte sur un document comportant des mentions qui ne sont pas communicables en application des articles L. 311-5 et L. 311-6 mais qu'il est possible d'occulter ou de disjoindre, le document est communiqué au demandeur après occultation ou disjonction de ces mentions. ".<br/>
<br/>
              4. Il résulte des dispositions précitées que les documents relatifs à une procédure de cession de titres détenus par l'Etat à un acteur privé, y compris les réponses des candidats et les documents relatifs au choix du candidat retenu, sont des documents administratifs au sens l'article L. 300-2 du code des relations entre le public et l'administration. Saisi d'un recours relatif à la communication de tels documents, il incombe au juge du fond d'examiner si les renseignements contenus dans les documents dont il est demandé communication peuvent porter atteinte au secret industriel et commercial et faire ainsi obstacle à cette communication.<br/>
<br/>
              5. En premier lieu, si le juge administratif a la faculté d'ordonner avant dire droit la production devant lui, par les administrations compétentes, des documents dont le refus de communication constitue l'objet même du litige, sans que la partie à laquelle ce refus a été opposé n'ait le droit d'en prendre connaissance au cours de l'instance, il ne commet d'irrégularité en s'abstenant de le faire que si l'état de l'instruction ne lui permet pas de déterminer, au regard des contestations des parties, le caractère légalement communicable ou non de ces documents ou d'apprécier les modalités de cette communication. A cet égard, dans le cas où tous les éléments d'information que doit comporter un document administratif sont définis par un texte, notamment par un cahier des charges ou par les documents d'une consultation, le juge administratif, saisi d'un litige relatif au refus de le communiquer, peut, sans être tenu d'en ordonner la production, décider si, eu égard au contenu des informations qui doivent y figurer, il est, en tout ou partie, communicable. En revanche, lorsque le contenu d'un document administratif, comme le contrat de vente de titres détenus par l'Etat, n'est défini par aucun texte, le juge ne saurait, au seul motif qu'il est susceptible de comporter des éléments couverts par un secret que la loi protège, décider qu'il n'est pas communicable, sans avoir au préalable ordonné sa production, hors contradictoire, afin d'apprécier l'ampleur des éléments protégés et la possibilité de communiquer le document après leur occultation. <br/>
<br/>
              6. Il ressort des pièces du dossier soumis aux juges du fond, notamment des annexes 5 et 6 du cahier des charges, intitulées respectivement " contenu des offres indicatives " et " contenu des offres fermes ", que ces documents  devaient comporter des informations relatives notamment au prix et à " la structure d'acquisition " ou à sa  " structure financière ", au " projet industriel, stratégique et social " du candidat, y compris " les principaux axes de sa politique ", notamment en termes de " développement du trafic " ou " d'extension des capacités ", les moyens mobilisés, le montant des investissements annuels envisagés, le calendrier proposé pour le transfert de la participation, ainsi que la description précise des accords industriels ou de partenariat que le candidat conclurait à compter du transfert . L'annexe 5 précisait en outre que le candidat devait présenter une analyse précise des risques concurrentiels susceptibles de résulter du transfert de la participation. Il résulte de ce qui a été dit au point 5 ci-dessus que, dès lors que les éléments d'information que devaient comporter les offres des candidats étaient ainsi précisément définis, le tribunal administratif n'a, eu égard au contenu de ces différents éléments, entaché son jugement ni d'erreur de droit ni d'inexacte qualification juridique des faits en jugeant, sans ordonner au préalable la production des offres, que le nombre des actions mises en vente et le prix global des offres des entreprises en concurrence étaient les seuls éléments communicables de ces documents, tous les autres étant couverts par le secret industriel et commercial.<br/>
<br/>
              7. En revanche, il résulte également de ce qui a été dit au point 5 ci-dessus que le tribunal administratif a entaché son jugement d'une erreur de droit  en jugeant légal, sans ordonner au préalable leur production, le refus de communication du contrat de vente et de l'engagement d'investissement signés par le candidat retenu au seul motif que ces documents contenaient nécessairement certains éléments protégés par le secret industriel et commercial, alors que, ainsi que cela ressortait des pièces du dossier qui lui était soumis, le contenu de ces documents n'avait fait l'objet d'aucune définition préalable.<br/>
<br/>
              8. En second lieu, les dispositions citées au point 3 ci-dessus consacrent un droit à la communication des documents administratifs qui ne se confond pas avec un droit d'accès aux informations contenues dans ces documents. Il en résulte que le tribunal administratif a commis une erreur de droit en jugeant légal le refus de communiquer les offres des candidats au seul motif que les éléments qu'il estimait communicables, qui sont rappelés au point 5 ci-dessus, figuraient dans les différents avis de la commission des participations et des transferts et que ces avis étaient publics et avaient été transmis aux requérants. Il lui appartenait de rechercher si, dès lors que les éléments d'information non communicables contenus dans les offres étaient, ainsi qu'il a été dit au point 5, très nombreux et qu'il était possible de se procurer les éléments communicables autrement,  la communication des offres après occultation des éléments non communicables pouvait être, dans les circonstances particulières de l'espèce, légalement refusée sur le fondement du dernier alinéa de l'article L. 311-2 du code des relations entre le public et l'administration aux termes duquel : " L'administration n'est pas tenue de donner suite aux demandes abusives (...)" au motif qu'elle ferait peser sur l'administration une charge excessive, eu égard aux moyens dont elle dispose et à l'intérêt que présenterait, pour les requérants, le fait de bénéficier, non de la seule connaissance des éléments communicables, mais de la communication des offres occultées elles-mêmes.<br/>
<br/>
              9. Il résulte de tout ce qui précède, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, que l'ACENAS et Mme A... sont fondées à demander l'annulation du jugement qu'elles attaquent. <br/>
<br/>
              10. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 2 000 euros, à verser à l'ACENAS et à Mme A..., au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Paris du 18 octobre 2018 est annulé.<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Paris.<br/>
Article 3 : L'Etat versera à l'ACENAS et à Mme A... la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à l'Association contre l'extension et les nuisances de l'Aéroport de Lyon Saint-Exupéry et à Madame B... A.... <br/>
             Copie en sera adressée au ministre de l'économie et des finances (Agence des participations de l'Etat).<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">26-06-01-02 DROITS CIVILS ET INDIVIDUELS. ACCÈS AUX DOCUMENTS ADMINISTRATIFS. ACCÈS AUX DOCUMENTS ADMINISTRATIFS AU TITRE DE LA LOI DU 17 JUILLET 1978. DROIT À LA COMMUNICATION. - FACULTÉ DE REFUSER LA COMMUNICATION D'UN DOCUMENT CONTENANT DES INFORMATIONS AUXQUELLES L'INTÉRESSÉ PEUT ACCÉDER PAR D'AUTRES MOYENS - 1) POUR CE SEUL MOTIF - ABSENCE - 2) SI LE DOCUMENT CONTIENT EN OUTRE DE NOMBREUX ÉLÉMENTS NON COMMUNICABLES - EXISTENCE, S'IL EN RÉSULTE POUR L'ADMINISTRATION UNE CHARGE EXCESSIVE EU ÉGARD À L'INTÉRÊT QUE REPRÉSENTE, POUR L'INTÉRESSÉ, LA COMMUNICATION DU DOCUMENT PARTIELLEMENT OCCULTÉ [RJ2].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">26-06-01-04 DROITS CIVILS ET INDIVIDUELS. ACCÈS AUX DOCUMENTS ADMINISTRATIFS. ACCÈS AUX DOCUMENTS ADMINISTRATIFS AU TITRE DE LA LOI DU 17 JUILLET 1978. CONTENTIEUX. - FACULTÉ POUR LE JUGE D'ORDONNER LA PRODUCTION DES DOCUMENTS FAISANT L'OBJET DU LITIGE, SANS COMMUNICATION À L'AUTRE PARTIE - NON USAGE DE CETTE FACULTÉ - IRRÉGULARITÉ - 1) CONDITIONS [RJ1] - 2) APPLICATION - A) CAS DANS LEQUEL LES ÉLÉMENTS D'INFORMATIONS QUE DOIT COMPORTER UN DOCUMENT SONT DÉFINIS PAR UN TEXTE - ABSENCE - B) CAS DANS LEQUEL LE CONTENU D'UN DOCUMENT N'EST DÉFINI PAR AUCUN TEXTE - EXISTENCE.
</SCT>
<ANA ID="9A"> 26-06-01-02 1) Les articles L.311-1 à L. 311-6 du code des relations entre le public et l'administration (CRPA) consacrent un droit à la communication des documents administratifs qui ne se confond pas avec un droit d'accès aux informations contenues dans ces documents. Il en résulte que le juge administratif ne peut juger légal le refus de communiquer les offres des candidats à l'acquisition d'actifs publics au seul motif que les éléments qui seraient communicables figureraient dans les différents avis de la commission des participations et des transferts et que ces avis étaient publics et avaient été transmis aux requérants.... ,,2) Il lui appartient de rechercher si, dès lors que les éléments d'information non communicables contenus dans les offres étaient très nombreux et qu'il était possible de se procurer les éléments communicables autrement,  la communication des offres après occultation des éléments non communicables pouvait être, dans les circonstances particulières de l'espèce, légalement refusée au motif qu'elle ferait peser sur l'administration une charge excessive, eu égard aux moyens dont elle dispose et à l'intérêt que présenterait, pour les requérants, le fait de bénéficier, non de la seule connaissance des éléments communicables, mais de la communication des offres occultées elles-mêmes.</ANA>
<ANA ID="9B"> 26-06-01-04 1) Si le juge administratif a la faculté d'ordonner avant dire droit la production devant lui, par les administrations compétentes, des documents dont le refus de communication constitue l'objet même du litige, sans que la partie à laquelle ce refus a été opposé n'ait le droit d'en prendre connaissance au cours de l'instance, il ne commet d'irrégularité en s'abstenant de le faire que si l'état de l'instruction ne lui permet pas de déterminer, au regard des contestations des parties, le caractère légalement communicable ou non de ces documents ou d'apprécier les modalités de cette communication.... ...2) a) A cet égard, dans le cas où les différents éléments d'information que doit comporter un document administratif sont définis par un texte, notamment par un cahier des charges ou par les documents d'une consultation, le juge administratif, saisi d'un litige relatif au refus de le communiquer, peut, sans être tenu d'en ordonner la production, décider si, eu égard au contenu des informations qui doivent y figurer, il est, en tout ou partie, communicable.... ...b) En revanche, lorsque le contenu d'un document administratif, comme le contrat de vente de titres détenus par l'Etat, n'est défini par aucun texte, le juge ne saurait, au seul motif qu'il est susceptible de comporter des éléments couverts par un secret que la loi protège, décider qu'il n'est pas communicable, sans avoir au préalable ordonné sa production, hors contradictoire, afin d'apprécier l'ampleur des éléments protégés et la possibilité de communiquer le document après leur occultation.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 23 juillet 2010, Office national des forêts c/ de La Gravière, n° 321138, T. pp. 777-905.,,[RJ2] Rappr., s'agissant des demandes abusives, CE, 14 novembre 2018, Ministre de la culture c/ Société pour la protection des paysages et de l'esthétique de la France, n°s 420055 422500, T. pp. 691.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
