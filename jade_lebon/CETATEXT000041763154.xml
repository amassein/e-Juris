<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041763154</ID>
<ANCIEN_ID>JG_L_2020_03_000000426955</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/76/31/CETATEXT000041763154.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 27/03/2020, 426955</TITRE>
<DATE_DEC>2020-03-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>426955</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, TEXIDOR, PERIER ; SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT>Mme Mireille Le Corre</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:426955.20200327</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Géomat a demandé au tribunal administratif de Nantes de condamner le département de la Loire-Atlantique à lui verser différentes sommes au titre de l'exécution du marché public relatif à l'opération de remembrement de la commune de Saint-Etienne de Montluc, dont la somme de 374 081,14 euros au titre des travaux supplémentaires réalisés. Par un jugement n° 1403879 du 2 janvier 2017, le tribunal administratif de Nantes a constaté qu'il n'y avait pas lieu de statuer à hauteur d'une somme de 513,92 euros HT payée en cours d'instance et a rejeté le surplus de sa demande. <br/>
<br/>
              Par un arrêt n° 17NT00813 du 9 novembre 2018, la cour administrative d'appel de Nantes a rejeté l'appel formé par la société Géomat contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 9 janvier et 9 avril 2019 au secrétariat du contentieux du Conseil d'Etat, la société Géomat demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge du département de la Loire-Atlantique la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code des marchés publics ;<br/>
              - le code rural et de la pêche maritime ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean Sirinelli, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Mireille Le Corre, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Texidor, Perier, avocat de la société Géomat, et à la SCP Matuchansky, Poupot, Valdelièvre, avocat du département de la Loire-Atlantique ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la société Géomat a conclu, le 9 juillet 2003, avec le département de la Loire-Atlantique un marché à prix unitaires ayant pour objet des prestations de géomètre-expert dans le cadre du remembrement d'une partie du territoire de la commune de Saint-Etienne de Montluc. Le 12 décembre 2011, le département de la Loire-Atlantique a rejeté le projet de décompte final transmis le 4 novembre 2011 par cette société, qui, outre le solde restant du marché fixé à la somme de 50 638,05 euros HT, comprenait une somme de 374 081,14 euros HT correspondant à des travaux exécutés en sus des prestations initialement prévues. Par un jugement du 2 janvier 2017, le tribunal administratif de Nantes, après avoir constaté un non-lieu à statuer à hauteur de 513,92 euros, a rejeté le surplus de la demande de la société Géomat tendant à la condamnation du département de la Loire-Atlantique à lui verser différentes sommes au titre de l'exécution du marché, dont la somme de 374 081,14 euros HT au titre des travaux supplémentaires. Par l'arrêt attaqué du 9 novembre 2018, la cour administrative d'appel de Nantes a rejeté l'appel formé par la société Géomat contre ce jugement.<br/>
<br/>
              2. En premier lieu, en relevant que certaines des prestations litigieuses étaient incluses dans la prestation de base de la société Géomat, la cour n'a pas entaché sur ce point son arrêt d'une insuffisance de motivation.<br/>
<br/>
              3. En deuxième lieu, il ressort des pièces du dossier soumis au juge du fond que la cour n'a pas méconnu la portée des écritures de la société Géomat en relevant que cette société faisait valoir que les travaux supplémentaires ont permis de procéder à un remembrement complet, conforme aux règles de l'art de la profession, alors que le strict respect des prescriptions du marché n'aurait en aucun cas permis la réalisation d'un remembrement complet et conforme.<br/>
<br/>
              4. En troisième lieu, aux termes du premier alinéa de l'article L. 121-16 du code rural et de la pêche maritime, dans sa version applicable au litige : " La préparation et l'exécution des opérations d'aménagement foncier sont assurées, sous la direction des commissions communales ou intercommunales d'aménagement foncier, par des techniciens rémunérés par le département en application de barèmes fixés (...) conjointement par le ministre de l'agriculture et le ministre chargé du budget ". En jugeant que ces dispositions, relatives aux conditions de rémunération des techniciens en charge de la préparation et de l'exécution des opérations d'aménagement foncier, ne font pas obstacle à l'application des dispositions de l'article 118 du code des marchés publics, alors en vigueur, prévoyant que lorsque le montant des prestations réalisées atteint le montant prévu par le marché, la poursuite de l'exécution des prestations est subordonnée à la conclusion d'un avenant ou, si le marché le prévoit, à une décision de poursuite prise par la personne responsable du marché, la cour n'a pas commis d'erreur de droit.<br/>
<br/>
              5. En quatrième lieu, le prestataire a le droit d'être indemnisé du coût des prestations supplémentaires indispensables à l'exécution du marché dans les règles de l'art, sauf dans le cas où la personne publique s'est préalablement opposée, de manière précise, à leur réalisation.<br/>
<br/>
              6. Il ressort des énonciations de l'arrêt attaqué que, pour rejeter la demande de la société Géomat tendant à l'indemnisation de prestations supplémentaires, la cour administrative d'appel de Nantes a relevé, d'une part, que le département de la Loire-Atlantique avait, par un courrier du 16 juillet 2008 adressé à la société, fait connaître sa volonté de ne pas rémunérer les prestations supplémentaires fournies sans commande expresse de sa part et sans avenant et, d'autre part, que la société n'établissait pas que les prestations non prévues par le contrat dont elle demandait l'indemnisation avaient été exécutées avant la réception de ce courrier. Il résulte de ce qui a été dit au point précédent qu'en statuant ainsi, la cour n'a pas commis d'erreur de droit.<br/>
<br/>
              7. En cinquième lieu, la cour n'a pas davantage commis d'erreur de droit, en jugeant que la circonstance, à la supposer établie, qu'une partie des prestations supplémentaires en litige, réalisées dans le cadre du marché conclu par le département de la Loire-Atlantique, auraient été réalisées à la demande de la sous-commission d'aménagement foncier de la commune de Saint-Etienne de Montluc, quand bien même ses membres appartiendraient par ailleurs à la commission communale d'aménagement foncier instituée en application de l'article L. 121-16 du code rural et de la pêche maritime, cité au point 4, n'est pas de nature à conférer, par elle-même, à ces prestations un caractère indispensable à l'exécution du marché dans les règles de l'art. <br/>
<br/>
              8. En dernier lieu, il ne ressort pas des pièces du dossier soumis aux juges du fond que la cour administrative d'appel de Nantes aurait méconnu son office ou commis une erreur de droit en estimant qu'il n'était pas nécessaire de procéder à l'expertise sollicitée par la société requérante.<br/>
<br/>
              9. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge du département de la Loire-Atlantique qui n'est pas, dans la présente instance, la partie perdante. Il y a lieu en revanche, dans les circonstances de l'espèce, de mettre à la charge de la société Géomat la somme de 3 000 euros à verser au département de la Loire-Atlantique au titre des mêmes dispositions.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la société Géomat est rejeté.<br/>
Article 2 : La société Géomat versera au département de la Loire-Atlantique une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la société Géomat et au département de la Loire-Atlantique.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-05-01-02-01 MARCHÉS ET CONTRATS ADMINISTRATIFS. EXÉCUTION FINANCIÈRE DU CONTRAT. RÉMUNÉRATION DU CO-CONTRACTANT. INDEMNITÉS. TRAVAUX SUPPLÉMENTAIRES. - 1) PRINCIPE - INDEMNISATION PAR LA PERSONNE PUBLIQUE DES PRESTATIONS INDISPENSABLES À L'EXÉCUTION DU MARCHÉ DANS LES RÈGLES DE L'ART [RJ1] - 2) EXCEPTION - PERSONNE PUBLIQUE S'ÉTANT PRÉALABLEMENT OPPOSÉE, DE MANIÈRE PRÉCISE, À LEUR RÉALISATION.
</SCT>
<ANA ID="9A"> 39-05-01-02-01 1) Le prestataire a le droit d'être indemnisé du coût des prestations supplémentaires indispensables à l'exécution du marché dans les règles de l'art, 2) sauf dans le cas où la personne publique s'est préalablement opposée, de manière précise, à leur réalisation.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Section, 17 octobre 1975, Commune de Canari, n° 93704, p. 516 ; CE, 14 juin 2002, Ville d'Angers, n° 219874, T. p. 812 ; CE, 29 septembre 2010, Société Babel, n° 319481, T. p. 851.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
