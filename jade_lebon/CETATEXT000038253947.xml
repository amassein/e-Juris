<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038253947</ID>
<ANCIEN_ID>JG_L_2019_03_000000404405</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/25/39/CETATEXT000038253947.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème et 1ère chambres réunies, 20/03/2019, 404405</TITRE>
<DATE_DEC>2019-03-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>404405</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème et 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Jean-François de Montgolfier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:404405.20190320</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une ordonnance n° 1606289 du 5 octobre 2016, enregistrée au secrétariat du contentieux du Conseil d'Etat le 12 octobre 2016, la présidente du tribunal administratif de Lille a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, la requête, enregistrée au greffe de ce tribunal le 18 août 2016, présentée par M. B... A...et l'association Sang d'encre. Par cette requête et un mémoire en réplique, enregistrés au secrétariat du contentieux du Conseil d'Etat le 27 juillet 2017, M. A... et l'association Sang d'encre demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite de rejet résultant du silence gardé par le ministre des finances et des comptes publics sur leur demande tendant à l'abrogation des articles 6 et 7 de la décision du 2 février 2006 du ministre de l'économie, des finances et de l'industrie et du ministre délégué au budget et à la réforme de l'Etat instituant une indemnité différentielle en faveur de certains ouvriers et contractuels de droit public de l'Imprimerie nationale ; <br/>
<br/>
              2°) d'enjoindre au ministre de l'économie et des finances d'abroger ces articles ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - la loi n° 93-1419 du 31 décembre 1993 ;<br/>
              - le décret n° 86-83 du 17 janvier 1986 ;<br/>
              - le décret n° 2006-392 du 31 mars 2006 ;<br/>
              - le décret n° 2008-539 du 6 juin 2008 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-François de Montgolfier, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1.  Aux termes de l'article 4-1 de la loi du 31 décembre 1993 relative à l'Imprimerie nationale : " Les ouvriers de l'Imprimerie nationale (...) peuvent être recrutés sur leur demande en qualité d'agent non titulaire de droit public par l'une des collectivités publiques ou établissements publics à caractère administratif mentionnés à l'article 2 de la loi n° 83-634 du 13 juillet 1983 portant droits et obligations des fonctionnaires. / En cette qualité, ils bénéficient d'un engagement à durée indéterminée, des dispositions légales et réglementaires régissant les agents non titulaires de la fonction publique dont relève la collectivité ou l'établissement public qui les recrute ainsi que, dans des conditions fixées par un décret en Conseil d'Etat, des dispositions réglementaires régissant ces mêmes agents. Les administrations ou organismes d'accueil pourront bénéficier de mesures financières ou d'accompagnement à la charge de l'Imprimerie nationale (...) ". Le ministre de l'économie, des finances et de l'industrie et le ministre délégué au budget et à la réforme de l'Etat ont pris, le 2 février 2006, une décision instituant une indemnité différentielle en faveur de certains ouvriers et contractuels de droit public de l'Imprimerie nationale recrutés, sur le fondement de l'article 4-1 de la loi du 31 décembre 1993, par un service ou une direction du ministère de l'économie, des finances et de l'industrie. Cette indemnité est versée en complément de la rémunération inscrite au contrat d'engagement. Aux termes de l'article 6 de cette décision : " Le montant de l'indemnité différentielle sera réduit à proportion des augmentations de rémunération dont les agents intéressés bénéficieront dans leur emploi, y compris les augmentations dues à la revalorisation de la valeur du point de fonction publique. / L'indemnité différentielle cesse d'être versée dès que son montant initial brut est intégralement compensé ". Son article 7 prévoit en outre que cette indemnité " est exclusive de toute indemnité de même nature ". M. A... et l'association Sang d'encre demandent l'annulation de la décision implicite du ministre des finances et des comptes publics rejetant leur demande tendant à l'abrogation de ces seuls articles 6 et 7.<br/>
<br/>
              2. Si, comme le soutient le ministre chargé de l'économie, l'instauration d'une indemnité au bénéfice d'anciens ouvriers de l'Imprimerie nationale est, à leur égard, une mesure favorable qui n'est imposée par aucun texte, sa décision du 2 février 2006 qui institue cette indemnité et fixe les règles selon lesquelles elle est versée présente un caractère réglementaire. Ce dispositif ne saurait dès lors, contrairement à ce que soutient également le ministre, revêtir le caractère d'une mesure purement gracieuse dont les modalités seraient, pour ce motif, insusceptibles de recours. <br/>
<br/>
              3.  Par ailleurs, contrairement à ce que soutient le ministre, les articles critiqués sont divisibles des autres dispositions de la décision du 2 février 2006. Par suite, la requête de M. A... et de l'association Sang d'encre n'est pas irrecevable au motif qu'elle limite à ces deux seuls articles ses conclusions.<br/>
<br/>
              4.  Toutefois, l'article 1er du décret du 31 mars 2006, pris en application de l'article 4-1 de la loi du 31 décembre 1993 cité ci-dessus, dispose que les ouvriers recrutés en application de cet article 4-1 sont " soumis aux dispositions régissant les agents non titulaires de la fonction publique dont relève la collectivité ou l'établissement public qui les a recrutés ". Ces dispositions ont ainsi expressément prévu que les rémunérations des anciens ouvriers de l'Imprimerie nationale dépendraient des collectivités et services au sein desquels ils seraient recrutés. Par suite, les requérants ne sauraient utilement soutenir que les dispositions dont ils ont demandé l'abrogation méconnaissent le principe d'égalité au motif qu'elles introduiraient une différence de rémunération entre les anciens ouvriers recrutés dans une direction du ministère de l'économie, des finances et de l'industrie et ceux qui sont recrutés par des collectivités territoriales. Par ailleurs, les agents intéressés par l'indemnité en cause n'étant pas fonctionnaires, le moyen tiré de la méconnaissance des dispositions de l'article 20 de la loi du 13 juillet 1983, applicable au traitement des seuls agents titulaires de l'Etat, est également inopérant.<br/>
<br/>
              5.  Enfin, l'indemnité différentielle instituée par la décision litigieuse et l'indemnité dite de " garantie individuelle du pouvoir d'achat " instaurée par le décret du 6 juin 2008 n'étant pas de même nature, le moyen tiré de ce que l'article 7 de la décision critiquée priverait les anciens ouvriers de l'Imprimerie nationale du bénéfice de cette dernière indemnité manque en fait.<br/>
<br/>
              6.  Il résulte de ce qui a été dit aux points 4 et 5 que, sans qu'il soit besoin de statuer sur la troisième fin de non-recevoir opposée par le ministre de l'économie et des finances, la requête de M. A... et de l'association Sang d'encre doit être rejetée, y compris les conclusions qu'ils présentent au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. A...et de l'association Sang d'encre est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. B... A..., à l'association Sang d'encre et au ministre de l'économie et des finances.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-01-06-01-01 ACTES LÉGISLATIFS ET ADMINISTRATIFS. DIFFÉRENTES CATÉGORIES D'ACTES. ACTES ADMINISTRATIFS - CLASSIFICATION. ACTES RÉGLEMENTAIRES. PRÉSENTENT CE CARACTÈRE. - DÉCISION INSTITUANT UNE INDEMNITÉ ET FIXANT LES RÈGLES SELON LESQUELLES ELLE EST VERSÉE - CONSÉQUENCE - CARACTÈRE PUREMENT GRACIEUX, RENDANT LA DÉCISION INSUSCEPTIBLE DE RECOURS - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-01-01-02-06 PROCÉDURE. INTRODUCTION DE L'INSTANCE. DÉCISIONS POUVANT OU NON FAIRE L'OBJET D'UN RECOURS. ACTES NE CONSTITUANT PAS DES DÉCISIONS SUSCEPTIBLES DE RECOURS. MESURES PUREMENT GRACIEUSES. - EXCLUSION - ACTE RÉGLEMENTAIRE.
</SCT>
<ANA ID="9A"> 01-01-06-01-01 La décision qui institue une indemnité et fixe les règles selon lesquelles elle est versée présente un caractère réglementaire. Ce dispositif ne saurait dès lors revêtir le caractère d'une mesure purement gracieuse dont les modalités seraient, pour ce motif, insusceptibles de recours.</ANA>
<ANA ID="9B"> 54-01-01-02-06 Un acte réglementaire ne saurait revêtir le caractère d'une mesure purement gracieuse qui serait, pour ce motif, insusceptible de recours.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
