<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026477826</ID>
<ANCIEN_ID>JG_L_2012_10_000000347128</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/47/78/CETATEXT000026477826.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème et 7ème sous-sections réunies, 10/10/2012, 347128</TITRE>
<DATE_DEC>2012-10-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>347128</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème et 7ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN ; SCP DELAPORTE, BRIARD, TRICHET</AVOCATS>
<RAPPORTEUR>M. David Gaudillère</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Damien Botteghi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:347128.20121010</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et les mémoires complémentaires, enregistrés les 28 février, 30 mai et 22 juillet 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour l'Office public de l'habitat de Châtillon, qui s'est substitué à l'Office public municipal d'habitations à loyer modéré (OPHLM) " Châtillon - Habitat ", dont le siège est 29, rue Jean-Pierre Timbaud à Châtillon (92320), représenté par son directeur général en exercice ;  l'office demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 08VE04005 du 17 décembre 2010 par lequel la cour administrative d'appel de Versailles a réformé le jugement n° 0610933-0702345-0703346 du tribunal administratif de Versailles du 7 octobre 2008 en tant qu'il a, d'une part, annulé pour excès de pouvoir la décision du 2 février 2007 de son président portant révocation de Mme Martine A et, d'autre part, lui a enjoint de procéder à la réintégration de Mme A dans ses fonctions de directeur général, à la date de son éviction illégale du service, dans le délai de deux mois à compter de la notification de l'arrêt, sous astreinte de 500 euros par jour de retard ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de Mme A ;<br/>
<br/>
              3°) de mettre à la charge de Mme A le versement d'une somme de 3 000 euros sur le fondement des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 24 septembre 2012, présentée pour l'Office public de l'habitat de Châtillon ;<br/>
<br/>
              Vu le code de la construction et de l'habitation ;<br/>
<br/>
              Vu la loi n° 83-634 du 13 juillet 1983 ;<br/>
<br/>
              Vu la loi n° 84-53 du 26 janvier 1984 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. David Gaudillère, Auditeur, <br/>
<br/>
              - les observations de la SCP Nicolay, de Lanouvelle, Hannotin, avocat de l'Office public de l'habitat de Châtillon et de la SCP Delaporte, Briard, Trichet, avocat de Mme A,<br/>
<br/>
              - les conclusions de M. Damien Botteghi, Rapporteur public,<br/>
<br/>
              La parole ayant à nouveau été donnée à la SCP Nicolay, de Lanouvelle, Hannotin, avocat de l'Office public de l'habitat de Châtillon et à la SCP Delaporte, Briard, Trichet, avocat de Mme A ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par décision du 2 février 2007, le président de l'Office public municipal d'habitations à loyer modéré (OPHLM) de Châtillon a prononcé la révocation de Mme A, sa directrice générale, en raison de négligences et d'insuffisances relevant d'une mauvaise administration, de manquements à son devoir de réserve et à son obligation de discrétion professionnelle, de manquements au devoir de loyauté et d'un comportement faisant obstacle à la continuité du service public ; que, par jugement du 7 octobre 2008, le tribunal administratif de Versailles a notamment rejeté les conclusions de Mme A dirigées contre cette décision ; que, par l'arrêt attaqué du 17 décembre 2010, la cour administrative d'appel de Versailles a annulé sur ce point le jugement du tribunal administratif et annulé la décision du 2 février 2007, en ordonnant en outre à l'Office public de l'habitat de Châtillon, qui a succédé à l'OPHLM, de procéder à la réintégration de Mme A dans ses fonctions de directrice générale, à la date de son éviction illégale du service, dans le délai de deux mois à compter de la notification de l'arrêt, sous astreinte de 500 euros par jour de retard ; que l'office se pourvoit, dans cette mesure, contre cet arrêt ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article 89 de la loi du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale : " Les sanctions disciplinaires sont réparties en quatre groupes : / (...) Quatrième groupe : / (...) la révocation. " ; qu'aux termes de l'article 26 de la loi du 13 juillet 1983 portant droits et obligations des fonctionnaires : " Les fonctionnaires sont tenus au secret professionnel dans le cadre des règles instituées dans le code pénal. / Les fonctionnaires doivent faire preuve de discrétion professionnelle pour tous les faits, informations ou documents dont ils ont connaissance dans l'exercice ou à l'occasion de l'exercice de leurs fonctions. (...) " ; <br/>
<br/>
              3. Considérant, en premier lieu, que la cour administrative d'appel a estimé que si, dans son rapport rédigé en juin 2004, la mission interministérielle d'inspection du logement social avait estimé que la gestion comptable et administrative de l'office présentait des insuffisances qui devaient être corrigées, la mission n'avait relevé aucune irrégularité de gestion qui aurait pu être imputable à Mme A ; qu'en se prononçant ainsi, la cour administrative d'appel a porté sur cette pièce du dossier, qu'elle n'a pas dénaturée, une appréciation souveraine qui ne peut être discutée devant le juge de cassation ;<br/>
<br/>
              4. Considérant, en deuxième lieu, que la cour a, après avoir estimé que le rapport du cabinet de conseil Groupe Europ'Consult de septembre 2006 mentionnait des irrégularités dans les procédures de passation des marchés publics de l'office, jugé que le responsable technique, qui était en charge des marchés publics, relevait directement du président de l'office, qui l'avait lui-même recruté ; qu'en se prononçant ainsi, la cour administrative d'appel, qui a suffisamment motivé son arrêt sur ce point au regard de l'argumentation dont elle était saisie, s'est livrée à une appréciation souveraine qui, en l'absence de dénaturation des faits, n'est pas non plus susceptible d'être discutée devant le juge de cassation ;<br/>
<br/>
              5. Considérant, en troisième lieu, que la révocation de Mme A était également fondée sur la circonstance que celle-ci avait communiqué aux membres du conseil d'administration de l'OPHLM de Châtillon un échange de courriers faisant état d'un différend avec le responsable technique concernant les procédures de passation des marchés publics de l'office ; que, s'agissant de ce grief, la cour a estimé qu'eu égard à la nature des documents communiqués, au contexte dans lequel il avait été procédé à cette communication lors des réunions des 19 et 29 septembre 2006 ainsi qu'au statut et aux fonctions du conseil d'administration de l'OPHLM de Châtillon, la communication par Mme A de tels documents aux membres de cette instance ne pouvait être regardée comme fautive et ne constituait pas un manquement à l'obligation de discrétion professionnelle ou au devoir de réserve ; qu'en estimant que les faits indiqués n'avaient pas de caractère fautif, la cour, qui a par suite nécessairement estimé que ces faits n'étaient pas non plus constitutifs d'un manquement au devoir de loyauté, n'a pas donné aux faits de la cause une qualification juridique erronée ; qu'en jugeant que ne constituait pas davantage un manquement à l'obligation de discrétion professionnelle ni, en tout état de cause, au devoir de réserve la communication à la société Groupe Europ'Consult de ces documents, qu'elle a, par une appréciation souveraine, jugés nécessaires à l'accomplissement par cet organisme de la mission d'audit qui lui avait été confiée par l'office, la cour n'a pas entaché son arrêt d'insuffisance de motivation ni donné aux faits de l'espèce une qualification juridique erronée ; <br/>
<br/>
              6. Considérant, en quatrième lieu, que la cour a jugé que l'absence de transmission au centre interdépartemental de gestion de la notation des agents pour 2005 et la mise à la charge de l'office d'intérêts de retard en raison de déclarations sociales et fiscales effectuées tardivement par le responsable des ressources humaines ne révélaient pas à eux seuls une carence fautive de Mme A dans la gestion du personnel placé sous son autorité ; que la cour n'a pas donné aux faits ainsi énoncés une qualification juridique erronée et s'est livrée à une appréciation qui, en l'absence de dénaturation, n'est pas susceptible d'être discutée devant le juge de cassation ; <br/>
<br/>
              7. Considérant, en cinquième lieu, qu'eu égard au caractère accessoire des conclusions à fin d'injonction, les parties sont recevables à les présenter pour la première fois en appel ; qu'ainsi, la cour a pu, sans commettre d'erreur de droit, faire droit aux conclusions à fin d'injonction que Mme A a présentées pour la première fois en appel ; <br/>
<br/>
              8. Considérant, en sixième lieu, que la cour, après avoir annulé la révocation de Mme A, a enjoint à l'office de la réintégrer dans ses fonctions à la date de son éviction illégale du service, soit le 2 février 2007 ; qu'à cette dernière date, l'intéressée bénéficiait d'un droit à réintégration dans l'emploi unique de directrice de l'office dont elle avait été illégalement écartée ; que l'office ne saurait utilement se prévaloir, pour la première fois en cassation, de ce que le poste de directeur de l'office avait changé de nature avec la transformation de l'OPHLM de Châtillon en Office public de l'habitat de Châtillon résultant de l'ordonnance du 1er février 2007 relative aux offices publics de l'habitat ; que par suite, l'arrêt de la cour n'est pas entaché d'erreur de droit en tant qu'il lui enjoint de procéder à la réintégration de Mme A à compter de la date de son éviction illégale du service ;<br/>
<br/>
              9. Considérant qu'il résulte de ce qui précède que l'Office public de l'habitat de Châtillon n'est pas fondé à demander l'annulation de l'arrêt qu'il attaque ; que ses conclusions présentées sur le fondement des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent qu'être rejetées par voie de conséquence ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à sa charge une somme de 3 000 euros au titre des frais exposés par Mme A et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le pourvoi de l'Office public de l'habitat de Châtillon est rejeté.<br/>
<br/>
Article 2 : L'Office public de l'habitat de Châtillon versera à Mme A une somme de 3 000 euros sur le fondement des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée à l'Office public de l'habitat de Châtillon et à Mme Martine A.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-09-04 FONCTIONNAIRES ET AGENTS PUBLICS. DISCIPLINE. SANCTIONS. - RÉVOCATION - DIRECTRICE GÉNÉRALE D'UN OPHLM AYANT COMMUNIQUÉ AUX MEMBRES DU CA DE L'OPHLM UN ÉCHANGE DE COURRIERS FAISANT ÉTAT D'UN DIFFÉREND AVEC LE RESPONSABLE TECHNIQUE CONCERNANT LES PROCÉDURES DE PASSATION DES MARCHÉS PUBLICS DE L'OFFICE - QUALIFICATION - MANQUEMENT À L'OBLIGATION DE DISCRÉTION PROFESSIONNELLE OU AU DEVOIR DE RÉSERVE - ABSENCE.
</SCT>
<ANA ID="9A"> 36-09-04 Directrice générale d'un OPHLM révoquée au motif notamment qu'elle avait communiqué aux membres du conseil d'administration (CA) de l'OPHLM de Châtillon un échange de courriers faisant état d'un différend avec le responsable technique concernant les procédures de passation des marchés publics de l'office. Eu égard à la nature des documents communiqués, au contexte dans lequel il a été procédé à cette communication ainsi qu'au statut et aux fonctions du conseil d'administration de l'OPHLM, la communication de tels documents aux membres de cette instance ne pouvait être regardée comme fautive et ne constituait pas un manquement à l'obligation de discrétion professionnelle ou au devoir de réserve.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
