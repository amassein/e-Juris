<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031281290</ID>
<ANCIEN_ID>JG_L_2015_10_000000387048</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/28/12/CETATEXT000031281290.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème / 2ème SSR, 05/10/2015, 387048</TITRE>
<DATE_DEC>2015-10-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>387048</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème / 2ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MEIER-BOURDEAU, LECUYER</AVOCATS>
<RAPPORTEUR>M. François Lelièvre</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Bertrand Dacosta</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:387048.20151005</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Cayenne d'annuler les arrêtés des 13 septembre, 18 octobre, 11 décembre et 24 décembre 2012 et du 22 février 2013 du président de la caisse des écoles de Matoury le concernant. <br/>
<br/>
              Par un jugement n° 1300166 du 20 mars 2014, le tribunal administratif de Cayenne a rejeté sa demande.<br/>
<br/>
              Par une ordonnance n° 14BX01859 du 3 décembre 2014, le président de la 6ème chambre de la cour administrative d'appel de Bordeaux a rejeté la requête formée par M. B... à l'encontre de ce jugement.<br/>
<br/>
              1° Par un pourvoi, enregistré le 12 janvier 2015 sous le n° 387048 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat d'annuler cette ordonnance.<br/>
<br/>
              2° Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 25 février et 26 mai 2015 sous le n° 388295 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de mettre à la charge de la commune de Matoury le versement de la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Il soutient que le président de la 6ème chambre de la cour administrative d'appel de Bordeaux a :<br/>
              - commis une erreur de droit en jugeant sa requête manifestement irrecevable alors qu'il n'avait pas été informé de ce que compte tenu du rejet de sa demande d'aide juridictionnelle, le délai d'appel avait recommencé à courir ;<br/>
              - commis une erreur de droit et dénaturé les faits en qualifiant sa demande d'aide juridictionnelle de requête d'appel.<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - la Constitution ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le décret n° 91-1266 du 19 décembre 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. François Lelièvre, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Bertrand Dacosta, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Meier-Bourdeau, Lecuyer, avocat de M. B...;<br/>
<br/>
<br/>
<br/>1. Considérant que les pourvois n°s 387048 et 388295 de M. B...sont dirigés contre la même ordonnance rejetant son appel contre un jugement du 20 mars 2014 du tribunal administratif de Cayenne ; qu'il y a lieu de les joindre pour statuer par une seule décision ;<br/>
<br/>
              2. Considérant qu'aux termes du second alinéa de l'article R. 411-1 du code de justice administrative : " L'auteur d'une requête ne contenant l'exposé d'aucun moyen ne peut la régulariser par le dépôt d'un mémoire exposant un ou plusieurs moyens que jusqu'à l'expiration du délai de recours. " ; qu'aux termes de l'article 39 du décret du 19 décembre 1991 portant application de la loi du 10 juillet 1991 relative à l'aide juridique : " Lorsqu'une demande d'aide juridictionnelle en vue de se pourvoir en matière civile devant la Cour de cassation est adressée au bureau d'aide juridictionnelle établi près cette juridiction avant l'expiration du délai imparti pour le dépôt du pourvoi ou des mémoires, ce délai est interrompu. (...) Les délais de recours sont interrompus dans les mêmes conditions lorsque l'aide juridictionnelle est sollicitée à l'occasion d'une instance devant le Conseil d'Etat ou une juridiction administrative statuant à charge de recours devant le Conseil d'Etat " ; qu'aucun texte ni aucun principe, notamment pas le principe à valeur constitutionnelle du droit d'exercer un recours juridictionnel, rappelé par les stipulations du paragraphe I de l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, n'implique que le délai de recours contentieux ainsi interrompu par la demande d'aide juridictionnelle ne recommence à courir qu'à la condition que le demandeur en soit préalablement informé ; <br/>
<br/>
              3. Considérant que, par l'ordonnance attaquée du 3 décembre 2014, le président de la 6ème chambre de la cour administrative d'appel de Bordeaux, après avoir relevé que la décision du bureau d'aide juridictionnelle rejetant la demande de M. B...tendant au bénéfice de cette aide pour faire appel avait été reçue par l'intéressé le 30 août 2014, a rejeté sa requête comme irrecevable au motif que celui-ci n'avait assorti cette requête d'aucun moyen et qu'à la date à laquelle il statuait, le délai d'appel dont disposait M. B...était expiré ; qu'en statuant ainsi, alors même que la notification du rejet de la demande d'aide juridictionnelle du requérant n'était pas accompagné de l'indication selon laquelle le délai d'appel recommencerait à courir une fois ce rejet devenu définitif, l'auteur de cette ordonnance, qui ne s'est pas mépris sur la portée des écritures de M.B..., n'a pas commis d'erreur de droit ; <br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède que les pourvois de M. B...doivent être rejetés, y compris ses conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les pourvois de M. B...sont rejetés.<br/>
Article 2 : La présente décision sera notifiée à M. A...B...et à la commune de Matoury.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-01-07-04 PROCÉDURE. INTRODUCTION DE L'INSTANCE. DÉLAIS. INTERRUPTION ET PROLONGATION DES DÉLAIS. - INTERRUPTION PAR UNE DEMANDE D'AIDE JURIDICTIONNELLE (ART. 39 DU DÉCRET DU 19 DÉCEMBRE 1991) - REPRISE DU DÉLAI CONDITIONNÉE À UNE INFORMATION DU DEMANDEUR - ABSENCE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-06-05-09 PROCÉDURE. JUGEMENTS. FRAIS ET DÉPENS. AIDE JURIDICTIONNELLE. - INTERRUPTION PAR UNE DEMANDE D'AIDE JURIDICTIONNELLE (ART. 39 DU DÉCRET DU 19 DÉCEMBRE 1991) - REPRISE DU DÉLAI CONDITIONNÉE À UNE INFORMATION DU DEMANDEUR - ABSENCE [RJ1].
</SCT>
<ANA ID="9A"> 54-01-07-04 Aucun texte ni aucun principe, notamment pas le principe à valeur constitutionnelle du droit d'exercer un recours juridictionnel, n'implique que le délai de recours contentieux interrompu par une demande d'aide juridictionnelle (art. 39 du décret n° 91-1266 du 19 décembre 1991) ne recommence à courir qu'à la condition que le demandeur en soit préalablement informé.</ANA>
<ANA ID="9B"> 54-06-05-09 Aucun texte ni aucun principe, notamment pas le principe à valeur constitutionnelle du droit d'exercer un recours juridictionnel, n'implique que le délai de recours contentieux interrompu par une demande d'aide juridictionnelle (art. 39 du décret n° 91-1266 du 19 décembre 1991) ne recommence à courir qu'à la condition que le demandeur en soit préalablement informé.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Comp. CE, Section, 28 juillet 2000, M. E.A., n° 151068, p. 347.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
