<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042143086</ID>
<ANCIEN_ID>JG_L_2020_07_000000427398</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/14/30/CETATEXT000042143086.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 4ème chambres réunies, 22/07/2020, 427398</TITRE>
<DATE_DEC>2020-07-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>427398</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN ; SCP JEAN-PHILIPPE CASTON ; SCP BAUER-VIOLAS, FESCHOTTE-DESBOIS, SEBAGH</AVOCATS>
<RAPPORTEUR>Mme Bénédicte Fauvarque-Cosson</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie Sirinelli</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:427398.20200722</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. J... I..., Mme E... D..., M. A... et Mme B... H..., M. L... G... et M. C... F... ont demandé au tribunal administratif de Toulon d'annuler pour excès de pouvoir l'arrêté du 7 mars 2017 par lequel le maire de Six-Fours-les-Plages a accordé à la société anonyme Bouygues Immobilier le permis de construire deux immeubles comprenant 59 logements, après démolition des constructions existantes, sur un terrain cadastré CY 22, 26 et 27 situé avenue des Moulins et Traverse des Moulins. Par un jugement n°s 1702053, 1702120, 1702121, 1702122, 1702123 du 27 novembre 2018, le tribunal administratif de Toulon a rejeté ces demandes.<br/>
<br/>
              1° Sous le n° 427398, par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 28 janvier et 29 avril 2019 au secrétariat du contentieux du Conseil d'Etat, M. I... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Six-Fours-les-Plages et de la société Bouygues Immobilier la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              2° Sous le n° 427421, par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 28 janvier, 26 avril et 6 novembre 2019, Mme D... et M. et Mme H... demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le même jugement ;<br/>
<br/>
              2°) de mettre à la charge de la commune de Six-Fours-les-Plages et de la société Bouygues Immobilier la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - le code de l'urbanisme ;<br/>
              - le code de la construction et de l'habitation ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme K... M..., conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Marie Sirinelli, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de M. I..., à la SCP Jean-Philippe Caston, avocat de la société anonyme Bouygues immobilier, à la SCP Bauer-Violas, Feschotte-Desbois, Sebagh, avocat de la commune de Six-Fours-Les-Plages et à Me Le Prado, avocat de Mme D..., de M. H... et de Mme H... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces des dossiers soumis aux juges du fond que, par un arrêté du 7 mars 2017, le maire de Six-Fours-Les-Plages a délivré à la société Bouygues Immobilier le permis de construire un ensemble de 2 immeubles comprenant 59 logements dont 20 logements sociaux, après démolition des constructions existantes, sur un terrain situé avenue des Moulins et traverse des Moulins. M. I..., Mme D... et M. et Mme H... ont formé des recours gracieux contre cet arrêté, que le maire de Six-Fours-Les-Plages a rejetés, puis ont demandé au tribunal administratif de Toulon d'annuler pour excès de pouvoir le permis de construire et les décisions de rejet de leurs recours gracieux. Par un jugement du 27 novembre 2018, le tribunal administratif de Toulon a rejeté leurs demandes. Par deux pourvois qu'il y a lieu de joindre, M. I..., d'une part, et Mme D... et M. et Mme H..., d'autre part, demandent l'annulation de ce jugement.<br/>
<br/>
              2. En premier lieu, aux termes de l'article UC 12 du règlement du plan local d'urbanisme de la commune de Six-Fours-les-Plages : " 1°) Il doit être aménagé : / - pour les constructions destinées à l'habitat : 1 place de stationnement par tranche de 60 mètres carrés de surface de plancher créée entamée, avec au minimum 1,5 place de stationnement ou de garage par logement. / Dans le cadre des programmes de logements ou de toute opération à vocation d'habitat créant une surface de plancher de 375 mètres carrés ou plus, ou comportant au moins 5 logements, le nombre de places à créer ainsi obtenu doit être majoré d'une place de stationnement pour visiteurs par tranche de 375 mètres carrés de surface de plancher créée entamée ou par tranche de 5 logements. / - pour les constructions et travaux de transformation ou d'aménagement de bâtiments affectés à des logements locatifs bénéficiant de prêts aidés par l'Etat : 1 place de stationnement par logement (...) ". <br/>
<br/>
              3. Aucune des dispositions citées ci-dessus du règlement du plan local d'urbanisme n'interdit que certaines places de stationnement soient en enfilade de places directement accessibles, dès lors que chacune d'elles, affectée au même logement que celle qui en commande l'accès, est effectivement utilisable. Par suite, le tribunal, qui a relevé, au terme d'une appréciation souveraine exempte de dénaturation, que chacune des places de stationnement situées en second rang était affectée au même appartement que celle qui en commandait l'accès, n'a pas commis d'erreur de droit en prenant en considération les places en enfilade pour apprécier le respect des dispositions de l'article UC 12 du règlement du plan local d'urbanisme.<br/>
<br/>
              4. En second lieu toutefois, en vertu des articles L. 127-1 et L. 128-1 du code de l'urbanisme, comme des 2° et 3° de l'article L. 151-28 du même code qui en reprennent les dispositions depuis le 1er janvier 2016, le règlement du plan local d'urbanisme peut prévoir " des secteurs à l'intérieur desquels la réalisation de programmes de logements comportant des logements locatifs sociaux au sens de l'article L. 302 5 du code de la construction et de l'habitation bénéficie d'une majoration du volume constructible tel qu'il résulte des règles relatives au gabarit, à la hauteur et à l'emprise au sol. Cette majoration, fixée pour chaque secteur, ne peut excéder 50 %. Pour chaque opération, elle ne peut être supérieure au rapport entre le nombre de logements locatifs sociaux et le nombre total des logements de l'opération ", de même que, dans les zones urbaines ou à urbaniser, " un dépassement des règles relatives au gabarit ", dans la limite de 30 %, " pour les constructions faisant preuve d'exemplarité énergétique ou environnementale ou qui sont à énergie positive ". En vertu de l'article L. 128-3 de ce code, comme de l'article L. 151-29 qui en reprend les dispositions depuis la même date, l'application combinée de ces règles " ne peut conduire à autoriser un dépassement de plus de 50 % du volume autorisé par le gabarit de la construction ". Il résulte de ces dispositions que les majorations de constructibilité autorisées au titre, d'une part, de la construction de logements locatifs sociaux et, d'autre part, des constructions faisant preuve d'exemplarité énergétique ou environnementale ou à énergie positive ne sont pas exclusives l'une de l'autre mais peuvent se cumuler, sans toutefois que leur application puisse conduire à un dépassement de plus de 50 % du volume maximal qu'autorise l'application, en fonction de l'implantation de la construction, des règles du plan local d'urbanisme relatives au gabarit ou à la hauteur de la construction combinées avec les règles d'emprise au sol.<br/>
<br/>
              5. Les articles 13 et 14 des dispositions générales du règlement du plan local d'urbanisme du Six-Fours-les-Plages, qui prévoient, sur le fondement de ces dispositions du code de l'urbanisme, des majorations de constructibilité pour les programmes de logements comprenant des logements locatifs sociaux et pour les constructions satisfaisant à des critères de performance énergétique élevée, de respectivement 50 % et, selon les secteurs, de 10, 20 ou 30 %, rappellent que ces majorations se cumulent sans toutefois conduire à autoriser un dépassement de plus de 50 % du volume autorisé par le gabarit.<br/>
<br/>
              6. Le tribunal administratif de Toulon a relevé que le projet d'ensemble immobilier objet du permis de construire en litige, qui comprend 20 logements locatifs sociaux sur un total de 59 logements, est situé dans le périmètre d'application d'une majoration de constructibilité en faveur de la diversité de l'habitat prévue à l'article 13 des dispositions générales du règlement du plan local d'urbanisme, s'élevant en l'espèce à 33,89 % compte tenu de la proportion de logements sociaux, et que ce projet, qui fait l'objet d'une étude thermique préliminaire garantissant sa conformité à la " qualification d'exemplarité énergétique " du plan local d'urbanisme, est également situé dans le périmètre d'application d'une majoration de constructibilité de 30 % en faveur de la performance énergétique prévue à l'article 14 de ces dispositions générales. <br/>
<br/>
              7. En appliquant un coefficient de 50 %, résultant du cumul de ces deux majorations, distinctement, d'une part, à l'emprise au sol maximale permise par l'article UC 9 du règlement du plan local d'urbanisme et, d'autre part, à la hauteur maximale de construction permise par l'article UC 10 de ce même règlement, pour en déduire que le projet autorisé ne méconnaissait ni les règles d'emprise au sol, ni les règles de hauteur, sans  tenir compte de ce qu'il en résultait nécessairement un dépassement de plus de 50 % du volume maximal résultant de l'application combinée de ces règles, le tribunal a commis une erreur de droit. <br/>
<br/>
              8. Par suite, sans qu'il soit besoin d'examiner les autres moyens de leur pourvoi, les requérants sont fondés à demander, pour ce motif, l'annulation du jugement qu'ils attaquent en tant qu'il rejette leurs demandes.<br/>
<br/>
              9. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société Bouygues Immobilier et de la commune de Six-Fours-les-Plages une somme de 2 000 euros chacune à verser à M. I... et une somme de 1 000 euros chacune à verser tant à Mme D..., d'une part, qu'à M. H... et Mme H..., d'autre part, au titre de l'article L. 761-1 du code de justice administrative. Les dispositions de cet article font, en revanche, obstacle à ce qu'il soit fait droit aux conclusions de la commune de Six-Fours-les-Plages et de la société Bouygues Immobilier présentées au même titre.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le jugement du tribunal administratif de Toulon du 27 novembre 2018 est annulé en tant qu'il rejette les demandes de M. I..., de Mme D... et de M. et Mme H....<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Toulon dans la mesure de la cassation prononcée.<br/>
Article 3 : La société anonyme Bouygues immobilier et la commune de Six-Fours-les-Plages verseront chacune une somme de 2 000 euros à M. I..., une somme de 1 000 euros à Mme D... et une somme de 1 000 euros à M. et Mme H....<br/>
Article 4 : Les conclusions de la commune de Six-Fours-les-Plages et de la société anonyme Bouygues Immobilier présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à M. J... I..., à Mme E... D..., première requérante dénommée, pour l'ensemble des requérants ayant présenté le pourvoi n° 427421, à la commune de Six-Fours-les-Plages et à la société anonyme Bouygues Immobilier.<br/>
Copie en sera adressée à la ministre de la cohésion des territoires et des relations avec les collectivités territoriales.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">68-01-01-02-02-12 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PLANS D'AMÉNAGEMENT ET D'URBANISME. PLANS D`OCCUPATION DES SOLS (POS) ET PLANS LOCAUX D'URBANISME (PLU). APPLICATION DES RÈGLES FIXÉES PAR LES POS OU LES PLU. RÈGLES DE FOND. STATIONNEMENT DES VÉHICULES. - PLU IMPOSANT UN NOMBRE DE PLACES DE STATIONNEMENT PAR SURFACE ET PAR LOGEMENT - POSSIBILITÉ DE PRÉVOIR DES PLACES EN ENFILADE - EXISTENCE.
</SCT>
<ANA ID="9A"> 68-01-01-02-02-12 Article UC 12 du règlement du plan local d'urbanisme (PLU) de la commune de Six-Fours-les-Plages fixant le nombre de places de stationnement devant être aménagées en fonction de la surface de plancher et du nombre de logements créés.,,,Aucune des dispositions de cet article n'interdit que certaines places de stationnement soient en enfilade de places directement accessibles, dès lors que chacune d'elles, affectée au même logement que celle qui en commande l'accès, est effectivement utilisable.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
