<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000023141270</ID>
<ANCIEN_ID>JG_L_2010_11_000000328189</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/23/14/12/CETATEXT000023141270.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème et 2ème sous-sections réunies, 24/11/2010, 328189</TITRE>
<DATE_DEC>2010-11-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>328189</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème et 2ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Martin</PRESIDENT>
<AVOCATS>SCP ORTSCHEIDT ; ODENT ; SCP DEFRENOIS, LEVIS</AVOCATS>
<RAPPORTEUR>Mme Cécile  Chaduteau-Monplaisir</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Boulouis Nicolas</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés le 22 mai et le 21 août 2009 au secrétariat du contentieux du Conseil d'Etat, présentés pour Me Muriel A, en sa qualité de mandataire liquidateur de la société Atelier Construction Métallique Rochefortaise (ACMR), dont le siège est BP33 à Rochefort (17300), et demeurant 2 ter, rue Jean Jaurès, BP 60289 à Rochefort (17312 cedex) ; Me A demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt du 19 mars 2009 par lequel la cour administrative d'appel de Bordeaux a rejeté sa requête tendant, d'une part, à l'annulation du jugement du 19 juin 2007 par lequel le tribunal administratif de Pau a condamné la société à verser à la chambre de commerce et d'industrie de Bayonne-Pays basque la somme de 378 579,04 euros correspondant au coût de remise en état du revêtement du ponton flottant réalisé dans la zone Saint Bernard du port de Bayonne et aux frais d'ouverture et de fermeture des trappes de ce ponton et, d'autre part, au rejet de la demande de la chambre de commerce et d'industrie ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa requête devant la cour administrative d'appel de Bordeaux ;<br/>
<br/>
              3°) de mettre la somme de 4 000 euros à la charge de la chambre de commerce et d'industrie de Bayonne-Pays basque en application des dispositions de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de commerce ;<br/>
<br/>
              Vu le cahier des clauses administratives générales applicables aux marchés industriels ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Cécile Chaduteau-Monplaisir, Auditeur,  <br/>
<br/>
              - les observations de la SCP Ortscheidt, avocat de Me Muriel A, de Me Odent, avocat de la chambre de commerce et d'industrie de Bayonne et de la SCP Defrenois, Levis, avocat de la compagnie Maaf assurances, <br/>
<br/>
              - les conclusions de M. Nicolas Boulouis, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Ortscheidt, avocat de Me Muriel A, à Me Odent, avocat de la chambre de commerce et d'industrie de Bayonne et à la SCP Defrenois, Levis, avocat de la compagnie Maaf assurances ;<br/>
<br/>
<br/>
<br/>Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la chambre de commerce et d'industrie de Bayonne-Pays basque (CCIB) a confié à la société Atelier Construction Métallique Rochefortaise (ACMR), par un acte d'engagement du 25 mars 1997, un marché industriel ayant pour objet la conception, la construction, le transport, l'installation sur le site et les essais d'un ponton flottant et d'une passerelle d'accès dans le port de Bayonne ; que, par un courrier du 23 novembre 1999, la CCIB a informé la société ACMR de l'apparition de désordres importants affectant le revêtement du ponton flottant ; que la société ACMR n'a pas déféré aux mises en demeure de la CCIB de procéder aux réparations ; que la société ACMR a été placée en liquidation judiciaire le 29 mai 2002 ; que, par un jugement du 19 juin 2007, le tribunal administratif de Bordeaux a condamné Me A, en sa qualité de mandataire liquidateur de la société ACMR, à verser à la CCIB une indemnité de 378 579,04 euros en réparation du préjudice subi du fait de ces désordres ; que, par un arrêt du 19 mars 2009, contre lequel Me A se pourvoit en cassation, la cour administrative d'appel de Bordeaux a rejeté l'appel de Me A contre ce jugement ;<br/>
<br/>
              Considérant, en premier lieu, que les dispositions des articles L. 621-40 et suivants du code de commerce d'où résultent, d'une part, le principe de la suspension ou de l'interdiction de toute action en justice de la part de tous les créanciers à compter du jugement d'ouverture de la procédure de redressement judiciaire, d'autre part, l'obligation, qui s'impose aux collectivités publiques comme à tous autres créanciers, de déclarer leurs créances dans les conditions et délais fixés, ne comportent pas de dérogation aux dispositions régissant les compétences respectives des juridictions administratives et judiciaires ; qu'il résulte de ces dispositions qu'il appartient de façon exclusive à l'autorité judiciaire de statuer sur l'admission ou la non-admission des créances déclarées ; que la circonstance que la collectivité publique dont l'action devant le juge administratif tend à faire reconnaître et évaluer ses droits à la suite des désordres constatés dans un ouvrage construit pour elle par une entreprise admise ultérieurement à la procédure de redressement, puis de liquidation judiciaire, n'aurait pas déclaré sa créance éventuelle ou n'aurait pas demandé à être relevée de la forclusion dans les conditions prévues par le code de commerce, est sans influence sur la compétence du juge administratif pour se prononcer sur ces conclusions dès lors qu'elles ne sont elles-mêmes entachées d'aucune irrecevabilité au regard des dispositions dont l'appréciation relève de la juridiction administrative, et ce, sans préjudice des suites que la procédure judiciaire est susceptible d'avoir sur l'extinction de cette créance ; qu'il résulte également de ce qui précède que si les dispositions législatives mentionnées ci-dessus réservent à l'autorité judiciaire la détermination des modalités de règlement des créances sur les entreprises en état de redressement, puis de liquidation judiciaire, il appartient au juge administratif d'examiner si la collectivité publique a droit à réparation et de fixer le montant des indemnités qui lui sont dues à ce titre par l'entreprise défaillante ou son liquidateur, sans préjudice des suites que la procédure judiciaire est susceptible d'avoir sur le recouvrement de cette créance ; que, par suite, la cour administrative d'appel de Bordeaux n'a pas commis d'erreur de droit en retenant que ni la liquidation judiciaire de la société ACMR ni la circonstance que la CCI n'avait pas déclaré sa créance ne faisaient obstacle à la condamnation de la société ACMR à verser une indemnité à la chambre en réparation des désordres affectant le ponton flottant ;<br/>
<br/>
              Considérant, en second lieu, qu'aux termes de l'article 34-1 du cahier des clauses administratives générales applicables aux marchés publics industriels, relatif à la garantie technique, dans sa version applicable à l'espèce :  Si le marché prévoit que les prestations feront l'objet d'une garantie technique d'une certaine durée de la part du titulaire, cette garantie, dans le silence du marché, couvre le démontage, le remplacement et le remontage des parties de la prestation qui seraient à l'usage reconnues défectueuses. (..). / Le titulaire n'est libéré de son obligation que si l'avarie provient de la faute de la personne publique ou de la force majeure  ; que la cour administrative d'appel a pu, sans erreur de droit, déduire de ces stipulations que la livraison d'un équipement industriel conçu et réalisé à la demande du maître d'ouvrage par un prestataire extérieur ne relevait sur le terrain contractuel que d'une garantie technique à la charge de ce prestataire, celle-ci n'étant susceptible d'être écartée qu'en raison de la force majeure ou de la faute du maître de l'ouvrage ; qu'il suit de là que la cour n'a pas commis d'erreur de droit ni insuffisamment motivé son arrêt en jugeant qu'était sans incidence sur la mise en jeu de la garantie technique du prestataire la circonstance que la direction départementale de l'équipement des Pyrénées Atlantiques, maître d'oeuvre de l'opération, aurait commis une faute en acceptant  le ponton flottant livré par la société ACMR en dépit des malfaçons constatées sur le revêtement intérieur ; <br/>
<br/>
              Considérant qu'il résulte de tout ce qui précède que Me A n'est pas fondée à demander l'annulation de l'arrêt attaqué ;<br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de faire application des dispositions de l'article L. 761-1 du code de justice administrative et de mettre à la charge de Me A le versement de la somme de 3 000 euros à la CCIB ; qu'en revanche ces dispositions font obstacle à ce qu'il soit fait droit aux conclusions que Me A présente au même titre ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
     --------------<br/>
Article 1er : Le pourvoi de Me A, mandataire liquidateur de la société Atelier Construction Métallique Rochefortaise, est rejeté. <br/>
<br/>
Article 2 : Me A versera la somme de 3 000 euros à la chambre de commerce et d'industrie de Bayonne-Pays basque en application de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
Article 3 : La présente décision sera notifiée à Me A et à la chambre de commerce et d'industrie de Bayonne-Pays basque.<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-06-02-01 MARCHÉS ET CONTRATS ADMINISTRATIFS. RAPPORTS ENTRE L'ARCHITECTE, L'ENTREPRENEUR ET LE MAÎTRE DE L'OUVRAGE. RESPONSABILITÉ DU MAÎTRE DE L'OUVRAGE ET DES CONSTRUCTEURS À L'ÉGARD DES TIERS. RESPONSABILITÉ DE L'ENTREPRENEUR. - GARANTIE TECHNIQUE (1. DE L'ART. 34 DU CCAG MARCHÉS PUBLICS INDUSTRIELS) - EXONÉRATION DE RESPONSABILITÉ DE L'ENTREPRENEUR EN CAS DE FAUTE DE LA PERSONNE PUBLIQUE - FAUTE DE LA PERSONNE PUBLIQUE MAÎTRE D'OUVRAGE - INCLUSION - FAUTE DE LA PERSONNE PUBLIQUE MAÎTRE D'OEUVRE - EXCLUSION.
</SCT>
<ANA ID="9A"> 39-06-02-01 Le 1. de l'article 34 du cahier des clauses administratives générales (CCAG) applicables aux marchés publics industriels prévoit que le titulaire du marché est libéré de son obligation au titre de la garantie technique en cas de faute de la personne publique. Pour l'application de ces dispositions, seule la faute de la personne publique maître d'ouvrage est exonératoire. La faute de la personne publique maître d'oeuvre ne l'est pas.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
