<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028353511</ID>
<ANCIEN_ID>JG_L_2013_12_000000335235</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/35/35/CETATEXT000028353511.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème et 9ème sous-sections réunies, 20/12/2013, 335235</TITRE>
<DATE_DEC>2013-12-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>335235</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème et 9ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GADIOU, CHEVALLIER</AVOCATS>
<RAPPORTEUR>M. Jacques Reiller</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Delphine Hedary</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:335235.20131220</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 4 janvier 2010 et 6 avril 2010 au secrétariat du contentieux du Conseil d'Etat, présentés pour Mme C...A..., demeurant..., Mme G... L...A..., demeurant..., Mme I... M...-A..., demeurant..., Mme I...E..., demeurant au..., M. D... E..., demeurant au..., M. J... E..., domicilié..., M. K... A..., demeurant..., M. B... A..., demeurant..., M. H... A..., demeurant..., M. F... A..., demeurant au..., la société immobilière de Chine, dont le siège est au 86, rue Cardinet à Paris (75017), la société immobilière de Picardie, dont le siège est au 86, rue Cardinet à Paris (75017), la société internationale d'épargne, dont le siège est au 86, rue Cardinet à Paris (75017) ; Mme A... et autres demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 08PA02004 du 2 novembre 2009 par lequel la cour administrative d'appel de Paris a rejeté leur requête tendant à l'annulation du jugement du 15 février 2008 du tribunal administratif de Paris rejetant leur demande tendant à la condamnation de l'Etat à leur verser la somme de 1,5 milliard d'euros en réparation du préjudice qu'ils ont subi du fait de la spoliation par les autorités chinoises en 1949 des biens et titres que leur famille possédait à Shanghaï et à la condamnation de l'Etat à leur verser cette somme ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le traité franco-chinois du 28 février 1946 de renonciation à l'extraterritorialité en Chine et aux droits y relatifs ;<br/>
<br/>
              Vu l'accord franco-chinois du 30 mai 1984 sur l'encouragement et la protection réciproque des investissements ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jacques Reiller, Conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Delphine Hedary, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gadiou, Chevallier, avocat de Mme A...et autres ;<br/>
<br/>
<br/>
<br/>1. Considérant que Mme A...et les autres requérants ont demandé à l'Etat la réparation du préjudice résultant de la confiscation par les autorités chinoises, en 1949, des droits et titres de propriétés détenus par leur famille dans les territoires de la Chine anciennement concédés à la France ; que, par l'arrêt attaqué, la cour administrative d'appel de Paris a rejeté leur appel contre le jugement du 15 février 2008 du tribunal administratif de Paris rejetant leur demande ;<br/>
<br/>
              2. Considérant que, d'une part, par les stipulations du 1 de l'article 5 du traité franco-chinois du 28 février 1946 de renonciation à l'extraterritorialité en Chine et aux droits y relatifs, la République française et la République de Chine sont convenues que " pour parer à toutes questions relatives aux droits et titres existants de propriétés immobilières, possédés par des sociétés ou des ressortissants français ou par le Gouvernement de la République Française dans les territoires de la République de Chine, et en particulier aux questions qui pourraient surgir de l'abrogation des dispositions des traités et accords prévues à l'article 2 du présent traité (...) ces droits et titres existants seront imprescriptibles et ne seront mis en cause sous aucun prétexte à moins que la preuve ne soit établie par une procédure légale régulière de leur acquisition frauduleuse ou par des moyens frauduleux et malhonnêtes, étant entendu qu'aucun droit ou titre ne sera invalidé en vertu de changements postérieurs de quelque nature que ce soit dans la procédure originale suivant laquelle ils ont été acquis " ; qu'il résulte du 2 du même article que " si le Gouvernement National de la République de Chine désirait remplacer par de nouveaux titres de propriété les baux à perpétuité actuellement existants ou tous autres documents probatoires relatifs aux propriétés immobilières, possédés par des sociétés ou des ressortissants français ou par le Gouvernement de la République Française, le remplacement en sera fait par les autorités chinoises sans frais d'aucune sorte et les nouveaux titres de propriété protègeront pleinement les détenteurs de ces baux ou autres documents probatoires et leurs héritiers légaux ou leurs ayants cause, sans discrimination de leurs droits et intérêts antérieurs y compris le droit d'aliénation. " ; que, d'autre part, aux termes de l'article 4 de l'accord franco-chinois du 30 mai 1984 sur l'encouragement et la protection réciproques des investissements : " 1. Les investissements effectués par les investisseurs de chacune des Parties contractantes bénéficient, sur le territoire et dans les zones maritimes de l'autre Partie, d'une pleine protection et d'une entière sécurité./ 2. Aucune des deux Parties contractantes ne peut soumettre les investissements effectués sur son territoire ou dans ses zones maritimes par des investisseurs de l'autre Partie à des mesures d'expropriation, de nationalisation ou à toutes autres mesures aboutissant au même résultat, si ce n'est à des fins d'utilité publique, de manière non discriminatoire, suivant une procédure légale et contre une indemnisation (...)  " ; que l'article 7 du même accord stipule que " le présent Accord est également applicable aux investissements effectués par des investisseurs de l'une des Parties contractantes sur le territoire ou dans les zones maritimes de l'autre Partie contractante, conformément aux lois et règlements de la République populaire de Chine pour les investisseurs français ou de la République française pour les investisseurs chinois, avant l'entrée en vigueur du présent Accord. " ; <br/>
<br/>
              3. Considérant, en premier lieu, qu'il résulte de ces stipulations qu'elles ne mettent à la charge des parties contractantes aucune obligation de protection des biens de leurs ressortissants respectifs ; que, par suite, en jugeant qu'elles ne pouvaient être invoquées par des particuliers à l'encontre de l'Etat dont ils sont ressortissants, la cour, qui a suffisamment motivé son arrêt, n'a pas commis d'erreur de droit ; qu'elle a pu légalement en déduire que la responsabilité pour faute de la France ne peut être engagée sur ce fondement ; <br/>
<br/>
              4. Considérant, en deuxième lieu, que la cour n'a pas inexactement qualifié les faits de l'espèce en écartant la responsabilité sans faute de l'Etat sur le terrain de l'égalité devant les charges publiques au motif que le préjudice subi par les requérants trouve son origine directe dans le fait d'un Etat étranger ; <br/>
<br/>
              5. Considérant, enfin, qu'en jugeant que le choix de la France de ne pas dissocier, dans ses négociations avec les autorités chinoises, le règlement de la question du remboursement des emprunts or de 1903 et de 1925 et celui de l'indemnisation des biens confisqués lors de la proclamation de la République populaire de Chine en 1949, n'était pas détachable de la conduite des relations internationales de la France ni, par suite, susceptible d'engager la responsabilité de l'Etat, la cour, qui n'a pas dénaturé les faits de l'espèce, n'a pas commis d'erreur de droit ;<br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que, et sans qu'il soit besoin de statuer sur la fin de non-recevoir opposée en défense, Mme A...et les autres requérants ne sont pas fondés à demander l'annulation de l'arrêt attaqué ; que, par voie de conséquence, leurs conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent qu'être rejetées ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : Le pourvoi de Mme A...et autres est rejeté.<br/>
Article 2 : La présente décision sera notifiée à Mme C...A..., première requérante dénommée, au Premier ministre, au ministre des affaires étrangères et au ministre de l'économie et des finances.<br/>
Les autres requérants seront informés de la présente décision par la SCP Gadiou, Chevallier, avocat au Conseil d'Etat et à la Cour de cassation, qui les représente devant le Conseil d'Etat.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">60-01-01-01 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. FAITS SUSCEPTIBLES OU NON D'OUVRIR UNE ACTION EN RESPONSABILITÉ. FAITS N'ENGAGEANT PAS LA RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. FAITS ÉMANANT D'UNE AUTORITÉ ÉTRANGÈRE. - ABSENCE DE RESPONSABILITÉ SANS FAUTE POUR RUPTURE D'ÉGALITÉ DEVANT LES CHARGES PUBLIQUES [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">60-01-02-01-005 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. FAITS SUSCEPTIBLES OU NON D'OUVRIR UNE ACTION EN RESPONSABILITÉ. FONDEMENT DE LA RESPONSABILITÉ. RESPONSABILITÉ SANS FAUTE. CAS DANS LESQUELS LE TERRAIN DE LA RESPONSABILITÉ SANS FAUTE NE PEUT ÊTRE UTILEMENT INVOQUÉ. - PRÉJUDICE TROUVANT SON ORIGINE DIRECTE DANS LE FAIT D'UN ETAT ÉTRANGER [RJ1].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">60-01-02-01-01 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. FAITS SUSCEPTIBLES OU NON D'OUVRIR UNE ACTION EN RESPONSABILITÉ. FONDEMENT DE LA RESPONSABILITÉ. RESPONSABILITÉ SANS FAUTE. RESPONSABILITÉ FONDÉE SUR L'ÉGALITÉ DEVANT LES CHARGES PUBLIQUES. - ABSENCE - PRÉJUDICE TROUVANT SON ORIGINE DIRECTE DANS LE FAIT D'UN ETAT ÉTRANGER [RJ1].
</SCT>
<ANA ID="9A"> 60-01-01-01 La responsabilité sans faute de l'Etat sur le terrain de l'égalité devant les charges publiques ne saurait être engagée au titre d'un préjudice trouvant son origine directe dans le fait d'un Etat étranger.</ANA>
<ANA ID="9B"> 60-01-02-01-005 La responsabilité sans faute de l'Etat sur le terrain de l'égalité devant les charges publiques ne saurait être engagée au titre d'un préjudice trouvant son origine directe dans le fait d'un Etat étranger.</ANA>
<ANA ID="9C"> 60-01-02-01-01 La responsabilité sans faute de l'Etat sur le terrain de l'égalité devant les charges publiques ne saurait être engagée au titre d'un préjudice trouvant son origine directe dans le fait d'un Etat étranger.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 28 juillet 1999, Grillo, n° 178498, T. pp. 577-907 sur un autre point.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
