<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032928878</ID>
<ANCIEN_ID>JG_L_2016_07_000000394514</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/92/88/CETATEXT000032928878.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 1ère chambres réunies, 22/07/2016, 394514</TITRE>
<DATE_DEC>2016-07-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>394514</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ ; HAAS</AVOCATS>
<RAPPORTEUR>M. Didier Ribes</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:394514.20160722</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A...I...a demandé au tribunal administratif de la Guadeloupe, d'une part, l'annulation de la déclaration de candidature de Mme H...L...et des opérations électorales qui se sont déroulées les 22 et 29 mars 2015 en vue de l'élection des conseillers départementaux du 15ème canton de Guadeloupe (Pointe-à-Pitre) et, d'autre part, la suspension du mandat des conseillers départementaux proclamés élus. Par un jugement n° 1500255 du 1er octobre 2015, le tribunal administratif a annulé l'élection du binôme composé de Mme L...et de M. D...K...et rejeté le surplus des conclusions de la protestation.<br/>
<br/>
              Par une requête sommaire, un mémoire complémentaire et deux mémoires en réplique, enregistrés les 12 novembre et 14 décembre 2015 et les 6 et 19 avril 2016 au secrétariat du contentieux du Conseil d'État, Mme L...demande au Conseil d'État :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) de rejeter la protestation de Mme I...contre ces opérations électorales et de valider son élection.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code électoral ;<br/>
              - la loi n° 2013-403 du 17 mai 2013 ;<br/>
              - la loi n° 2015-29 du 16 janvier 2015, notamment son article 10 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Didier Ribes, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de Mme L...et à Me Haas, avocat de Mme I...;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il résulte de l'instruction qu'à l'issue du second tour de scrutin organisé le 29 mars 2015 en vue de l'élection des conseillers départementaux du canton de Pointe-à-Pitre (Guadeloupe), Mme H...L...et M. D...K...ont été proclamés élus par 2560 voix contre 2368 voix pour Mme B...F...et M. G... E... ; que Mme L...relève appel du jugement du 1er octobre 2015 par lequel le tribunal administratif de la Guadeloupe a, sur la protestation de Mme A...I..., annulé l'élection du binôme qu'elle formait avec M.K... ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article R. 114 du code électoral : " En cas de renouvellement général, le tribunal administratif prononce sa décision dans le délai de trois mois à compter de l'enregistrement de la réclamation au greffe ; la décision est notifiée dans les huit jours à partir de sa date au préfet et aux parties intéressées, dans les conditions fixées à l'article R. 751-3 du code de justice administrative. / En cas d'élection partielle, ce délai est réduit à deux mois. / S'il intervient une décision ordonnant une preuve, le tribunal administratif doit statuer dans le mois à partir de cette décision. / (...) / Lorsqu'il est fait application des dispositions de l'article L. 118-2, les délais, prévus aux premier et deuxième alinéas, dans lesquels le tribunal administratif doit se prononcer, courent à partir de la date de réception par le tribunal administratif des décisions de la commission nationale des comptes de campagne et des financements politiques ou, à défaut de décision explicite, à partir de l'expiration du délai de deux mois prévu audit article " ; qu'en vertu de l'article R. 117 du même code, faute d'avoir statué dans ces délais, le tribunal administratif est dessaisi ; qu'il résulte de ces dispositions que le tribunal administratif dispose, à peine de dessaisissement, d'un délai de trois mois, réduit à deux mois en cas d'élection partielle, courant à compter de la date fixée par l'article R. 114 du code électoral, pour statuer sur les protestations dont il est saisi ; que, toutefois, lorsqu'une formation d'instruction ou de jugement du tribunal décide d'ordonner une enquête qu'elle estime utile à la solution du litige, le délai ainsi prescrit peut être prorogé dans la limite d'un mois courant à compter de l'intervention de la décision ordonnant l'enquête ; <br/>
<br/>
              3. Considérant qu'il résulte de l'instruction que les décisions en date du 16 juillet 2015 de la Commission nationale des comptes de campagne et des financements politiques se rapportant à l'élection contestée ont été reçues par le tribunal administratif de la Guadeloupe le 20 juillet 2015 ; que, s'agissant d'un renouvellement général, le tribunal disposait d'un délai de trois mois à partir de la date de réception de ces décisions pour statuer ; que si, par un jugement avant-dire droit du 21 août 2015, le tribunal administratif a ordonné une enquête sur les fonctions administratives antérieurement exercées par MmeL..., il a prononcé sa décision le 1er octobre 2015, soit dans le délai de trois mois courant à compter du 20 juillet précédent, qui expirait le 20 octobre 2015 ; qu'ainsi, le moyen tiré de ce que le tribunal administratif aurait statué au-delà du délai qui lui était imparti par l'article R. 114 du code électoral doit, en tout état de cause, être écarté ;<br/>
<br/>
              4. Considérant qu'il résulte de l'instruction que le tribunal administratif a recueilli, dans le cadre de l'enquête ordonnée par son jugement avant-dire droit, les témoignages de plusieurs personnes exerçant ou ayant exercé des fonctions au sein du conseil général de la Guadeloupe, parmi lesquelles M.C...  J..., ancien directeur de cabinet du président du conseil général ; que la circonstance, à la supposer établie, que M. C...J...aurait participé à la campagne de M. E... et aurait, par suite, entretenu une communauté d'intérêt avec Mme I...n'est pas, dans les circonstances de l'espèce, de nature à entacher d'irrégularité la procédure au terme de laquelle est intervenu le jugement attaqué ;<br/>
<br/>
              5. Considérant que l'article L. 191 du code électoral dispose : " Les électeurs de chaque canton du département élisent au conseil départemental deux membres de sexe différent, qui se présentent en binôme de candidats dont les noms sont ordonnés dans l'ordre alphabétique sur tout bulletin de vote imprimé à l'occasion de l'élection " ; qu'aux termes de l'article L. 195 du même code : " Ne peuvent être élus membres du conseil départemental: / (...) 18° Les membres du cabinet du président du conseil départemental et du président du conseil régional, les directeurs généraux, les directeurs, les directeurs adjoints, les chefs de service et les chefs de bureau de conseil départemental et de conseil régional dans la circonscription où ils exercent ou ont exercé leurs fonctions depuis moins d'un an ; (...) " ; <br/>
<br/>
              6. Considérant qu'aux termes du 4° du I de l'article 10 de la loi du 16 janvier 2015 relative à la délimitation des régions, aux élections régionales et départementales et modifiant le calendrier électoral : " Les articles L. 195 et L. 196 ne sont applicables qu'aux fonctions exercées à partir du 1er décembre 2014, à l'exception des fonctions de préfet " ; que ces dispositions, tirant les conséquences de l'avancement au mois de mars 2015 de la date des élections départementales initialement prévues en décembre 2015, ont eu pour objet non de permettre aux personnes ayant exercé leurs fonctions avant le mois de décembre 2014 d'être éligibles, mais d'éviter de rendre inéligibles les personnes ayant prévu de se mettre en conformité avec les dispositions des articles L. 195 et L. 196 antérieurement à la période d'un an initialement prévue et risquant d'être pénalisées par le raccourcissement de cette période ; qu'ainsi, les articles L. 195 et L. 196 du code électoral sont applicables à toutes les personnes qui ont exercé les fonctions visées durant tout ou partie de la période comprise entre le 1er décembre 2014 et la date du scrutin ; qu'il résulte de l'instruction que Mme L...a exercé ses fonctions au sein des services du conseil général du 2 mai 2012 au 1er février 2015 ; qu'elle a ainsi exercé des fonctions au sein du conseil général durant la période d'inéligibilité prévue par l'article 10 de la loi du 16 janvier 2015 ;<br/>
<br/>
              7. Considérant qu'il résulte de l'instruction qu'en vertu de son arrêté de nomination, Mme L...était, à la date du scrutin et depuis le 2 mai 2012, affectée auprès du directeur du cabinet du président du conseil général de la Guadeloupe et placée sous son autorité hiérarchique directe ; qu'elle avait la qualité d'agent public assimilé à un directeur territorial et bénéficiait d'une rémunération correspondant à un IB 985 ; qu'elle était responsable des services du cabinet à l'antenne du conseil général à Pointe-à-Pitre et assurait l'encadrement des agents qui y étaient affectés ; qu'à ce titre, elle était notamment chargée de la gestion de la résidence départementale à l'antenne du conseil général où étaient organisés des réunions d'élus et divers événements et préparait les projets de réponse du président du conseil général pour une partie des courriers des administrations et des particuliers reçus à Pointe-à-Pitre ; qu'elle était également chargée de la préparation des discours du président du conseil général selon les orientations générales données par le directeur du cabinet ; qu'elle assurait, en outre, la liaison entre les services relevant de la direction de l'insertion et  le cabinet du président du conseil général et traitait, en liaison, avec ces services, de différents sujets tels que la programmation des fonds européens ; qu'il résulte de ce qui précède qu'alors même qu'elle ne disposait pas d'une délégation de signature, Mme L...doit être regardée comme ayant exercé des fonctions équivalentes à celles de membre du cabinet du président du conseil général de la Guadeloupe et tombant, par suite, sous le coup de l'inéligibilité édictée par les dispositions citées ci-dessus de l'article L. 195 du code électoral ;<br/>
<br/>
              8. Considérant qu'il résulte de tout ce qui précède que Mme L...n'est pas fondée à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de la Guadeloupe a annulé l'élection du binôme qu'elle formait avec M. K...en qualité de conseillers départementaux ;<br/>
<br/>
              9. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par Mme I...au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de Mme L...est rejetée.<br/>
Article 2 : Les conclusions présentées par Mme I...au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à Mme H...L..., à M. D... K..., à Mme A...I...et au ministre de l'intérieur.<br/>
Copie en sera adressée à la ministre des outre-mer et à la Commission nationale des comptes de campagne et des financements politiques.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">28-08-04 ÉLECTIONS ET RÉFÉRENDUM. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. JUGEMENTS. - DÉLAI IMPARTI AU TRIBUNAL ADMINISTRATIF POUR STATUER, À PEINE DE DESSAISISSEMENT - CAS OÙ LE TRIBUNAL ADMINISTRATIF ORDONNE UNE ENQUÊTE - DÉLAI D'UN MOIS À COMPTER DE LA DÉCISION ORDONNANT L'ENQUÊTE - DÉLAI UNIQUEMENT SUSCEPTIBLE DE PROROGER LE DÉLAI IMPARTI [RJ1].
</SCT>
<ANA ID="9A"> 28-08-04 Il résulte des articles R. 114 et R. 117 du code électoral que le tribunal administratif dispose, à peine de dessaisissement, d'un délai de trois mois, réduit à deux mois en cas d'élection partielle, courant à compter de la date fixée par l'article R. 114, pour statuer sur les protestations dont il est saisi. Toutefois, lorsqu'une formation d'instruction ou de jugement du tribunal décide d'ordonner une enquête qu'elle estime utile à la solution du litige, le délai ainsi prescrit peut être prorogé dans la limite d'un mois courant à compter de l'intervention de la décision ordonnant l'enquête.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Cf. CE, 13 février 1885, Elections municipales de Remèze, p. 187.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
