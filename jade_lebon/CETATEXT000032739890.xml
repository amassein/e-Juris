<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032739890</ID>
<ANCIEN_ID>JG_L_2016_06_000000384297</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/73/98/CETATEXT000032739890.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème - 10ème chambres réunies, 20/06/2016, 384297</TITRE>
<DATE_DEC>2016-06-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>384297</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème - 10ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MATUCHANSKY, VEXLIARD, POUPOT ; SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>Mme Séverine Larere</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:384297.20160620</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire, un mémoire complémentaire, un mémoire en réplique, des observations complémentaires et deux nouveaux mémoires, enregistrés les 8 septembre et 5 décembre 2014, 5 mai, 16 juillet et 9 novembre 2015 et 13 avril 2016 au secrétariat du contentieux du Conseil d'Etat, la Fédération française des sociétés d'assurances (FFSA) demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la recommandation n° 2014-R-01 du 3 juillet 2014 de l'Autorité de contrôle prudentiel et de résolution sur les conventions concernant la distribution des contrats d'assurance vie ;<br/>
<br/>
              2°) de mettre une somme de 5 000 euros à la charge de l'Etat au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code des assurances ;<br/>
              - le code monétaire et financier ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Séverine Larere, maître des requêtes,<br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat de la Fédération française des sociétés d'assurances (FFSA) et à la SCP Matuchansky, Vexliard, Poupot, avocat de l'Autorité de contrôle prudentiel et de résolution ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              Sur la fin de non-recevoir opposée par l'Autorité de contrôle prudentiel et de résolution :<br/>
<br/>
              1. Les avis, recommandations, mises en garde et prises de position adoptés par les autorités de régulation dans l'exercice des missions dont elles sont investies, peuvent être déférés au juge de l'excès de pouvoir lorsqu'ils revêtent le caractère de dispositions générales et impératives ou lorsqu'ils énoncent des prescriptions individuelles dont ces autorités pourraient ultérieurement censurer la méconnaissance. Ces actes peuvent également faire l'objet d'un tel recours, introduit par un requérant justifiant d'un intérêt direct et certain à leur annulation, lorsqu'ils sont de nature à produire des effets notables, notamment de nature économique, ou ont pour objet d'influer de manière significative sur les comportements des personnes auxquelles ils s'adressent. Dans ce dernier cas, il appartient au juge, saisi de moyens en ce sens, d'examiner les vices susceptibles d'affecter la légalité de ces actes en tenant compte de leur nature et de leurs caractéristiques, ainsi que du pouvoir d'appréciation dont dispose l'autorité de régulation. <br/>
<br/>
              2. Aux termes du deuxième alinéa de l'article L. 612-29-1 du code monétaire et financier, l'Autorité de contrôle prudentiel et de résolution (ACPR) " peut constater l'existence de bonnes pratiques professionnelles ou formuler des recommandations définissant des règles de bonne pratique professionnelle en matière de commercialisation et de protection de la clientèle ". Sur le fondement de ces dispositions, l'ACPR a, par l'acte du 3 juillet 2014 dont la Fédération française des sociétés d'assurances (FFSA) demande l'annulation pour excès de pouvoir, émis des recommandations sur les conventions, conclues entre les entreprises d'assurance et les intermédiaires en assurance, concernant la distribution des contrats d'assurance vie. Ces recommandations ont pour objet d'inciter les entreprises d'assurance et les intermédiaires, qui en sont les destinataires, à modifier sensiblement leurs relations réciproques. La FFSA, qui représente les intérêts des entreprises d'assurance, est dès lors recevable à en demander l'annulation.<br/>
<br/>
              Sur les interventions :<br/>
<br/>
              3. La Chambre syndicale des courtiers d'assurances (CSCA), le Groupement des entreprises mutuelles d'assurance (GEMA) et la Fédération bancaire française (FBF) justifient d'un intérêt suffisant à l'annulation de la recommandation attaquée. Ainsi leurs interventions sont recevables.<br/>
<br/>
              Sur la compétence de l'ACPR :<br/>
<br/>
              4. Il résulte des termes mêmes de l'acte attaqué que les recommandations qu'il contient ne présentent pas de caractère impératif et n'ont pas vocation à modifier l'ordonnancement juridique. En formulant ces recommandations, l'ACPR s'est bornée à inviter les professionnels du secteur concerné à adopter des règles de bonne pratique professionnelle en matière de distribution des contrats d'assurance vie. Cet acte ne saurait être regardé comme édictant des règles nouvelles relevant du domaine de la loi ou du règlement. L'ACPR était, dans ces conditions, compétente pour formuler la recommandation attaquée.<br/>
<br/>
              Sur le respect des dispositions du code des assurances et des principes constitutionnels :<br/>
<br/>
              En ce qui concerne le moyen tiré de la méconnaissance des articles L. 132-28, R. 132-5-1 et R. 132-5-2 du code des assurances :<br/>
<br/>
              5. Aux termes de l'article L. 132-28 du code des assurances : " I.-L'intermédiaire mentionné à l'article L. 511-1 établit des conventions avec les entreprises d'assurance ou de capitalisation proposant les contrats d'assurance individuels comportant des valeurs de rachat, les contrats de capitalisation, les contrats mentionnés à l'article L. 132-5-3 et à l'article L. 441-1 et en raison desquels il exerce son activité d'intermédiation. / Ces conventions prévoient notamment : / 1° Les conditions dans lesquelles l'intermédiaire (...) est tenu de soumettre à l'entreprise d'assurance ou de capitalisation les documents à caractère publicitaire préalablement à leur diffusion afin de vérifier leur conformité au contrat d'assurance ou de capitalisation et, le cas échéant, à la notice ou note ; / 2° Les conditions dans lesquelles sont mises à disposition de l'intermédiaire par l'entreprise d'assurance ou de capitalisation les informations nécessaires à l'appréciation de l'ensemble des caractéristiques du contrat. / II.-Un décret en Conseil d'Etat précise les modalités d'application du I, notamment les cas et conditions dans lesquels l'obligation d'établir des conventions n'est pas justifiée compte tenu de la nature des contrats ou de leur mode de distribution. ". Aux termes de l'article R. 132-5-1 du même code : " Les conventions mentionnées à l'article L. 132-28 sont établies par écrit à la demande des intermédiaires et prévoient notamment : / 1° A la charge de l'intermédiaire d'assurance : / a) La soumission à l'entreprise d'assurance de tout projet de document à caractère publicitaire qu'il a établi, quel que soit son support, et de toute modification qu'il entend apporter à ce document, préalablement à sa diffusion ; / b) L'obligation de n'utiliser que les documents à caractère publicitaire approuvés par l'entreprise d'assurance ; / 2° A la charge de l'entreprise d'assurance : / a) La vérification de la conformité au contrat d'assurance ou de capitalisation de tout projet ou modification de document à caractère publicitaire relatif à ce contrat et établi par l'intermédiaire, dans un délai fixé par la convention ; / b) La transmission et la mise à jour systématique, notamment sous forme de fiches de présentation, des informations nécessaires à l'appréciation de l'ensemble des caractéristiques du contrat, tant par l'intermédiaire que par la clientèle (...) ". Enfin, aux termes de l'article R. 132-5-2 du même code : " I. - Les conventions prévues à l'article L. 132-28 ne sont pas exigées dès lors que l'intermédiaire n'a recours qu'aux documents à caractère publicitaire mis à sa disposition par l'entreprise d'assurance et que celle-ci s'est engagée par écrit à lui transmettre les informations mentionnées au b du 2° de l'article R. 132-5-1. / II. - L'établissement d'une telle convention n'est pas exigé en cas de commercialisation des contrats mentionnés à l'article L. 441-1 lorsque le lien qui unit l'adhérent au souscripteur rend obligatoire l'adhésion au contrat. ".<br/>
<br/>
              6. D'une part, la recommandation attaquée préconise, à son paragraphe 4.1.3, que lorsque l'intermédiaire a recours à des communications à caractère publicitaire autres que celles mises à sa disposition par l'organisme d'assurance, la convention prévue à l'article L. 132-28 du code des assurances prévoie clairement l'engagement de l'organisme d'assurance de vérifier la conformité des communications à caractère publicitaire " dans un (des) délai(s) fixé(s) en nombre maximum de jours, adapté(s) le cas échéant aux modalités de commercialisation de l'intermédiaire et aux support(s) de communication utilisés ". Ces énonciations, qui se bornent à préconiser des modalités pratiques de mise en oeuvre des dispositions fixées au a du 2° de l'article R. 132-5-1 du code des assurances, laissent aux parties à la convention le soin de fixer la durée du délai dans lequel l'organisme d'assurance doit vérifier la conformité des communications à caractère publicitaire qui lui sont transmises. Dès lors elles ne méconnaissent pas les dispositions de cet article.<br/>
<br/>
              7. D'autre part, si la recommandation énonce, à son paragraphe 4.2.3, qu'en cas d'absence de convention, l'écrit par lequel l'entreprise d'assurance doit, aux termes de l'article R. 132-5-2, s'engager à transmettre au distributeur les informations relatives au contrat, devrait préciser le type de documents destinés à cette communication ainsi que le délai et les modalités de transmission de ces informations, ces préconisations constituent, également, des modalités pratiques de mise en oeuvre des dispositions de l'article R. 132-5-2 et n'ajoutent aucune obligation nouvelle qui serait édictée en méconnaissance de ces dispositions.<br/>
<br/>
              En ce qui concerne le moyen tiré de la violation du principe de la liberté contractuelle : <br/>
<br/>
              8. La liberté contractuelle est au nombre des principes de valeur constitutionnelle auxquels seule la loi est, le cas échéant, susceptible d'apporter des restrictions et limitations. La FFSA soutient que la recommandation attaquée porte atteinte à cette liberté en ce qu'elle impose, à son paragraphe 4.3, en cas de " chaîne de distribution ", c'est-à-dire d'interposition de deux ou plusieurs intermédiaires entre l'organisme d'assurance et l'assuré, la conclusion d'un contrat, non prévu par la loi, entre l'intermédiaire " initial " et l'intermédiaire " distributeur ". Il lui est également reproché d'imposer une modification des conventions en cours afin d'y insérer une clause prévoyant la conclusion de convention entre l'intermédiaire initial et l'intermédiaire distributeur. <br/>
<br/>
              9. D'une part, si elle prévoit son application à compter du 1er janvier 2015, la recommandation attaquée ne saurait conduire à modifier les contrats conclus avant cette date.<br/>
<br/>
              10. D'autre part, si le législateur n'a, par l'article L. 132-28 du code des assurances, prévu la conclusion de conventions qu'entre les entreprises d'assurance ou de capitalisation et les intermédiaires mentionnés à l'article L. 511-1 du même code, la recommandation attaquée ne saurait être regardée comme ayant méconnu le principe de la liberté contractuelle en invitant les intermédiaires d'assurance à conclure, entre eux, de telles conventions en cas de " chaîne de distribution ", dès lors que ces dispositions, qui sont dépourvues de caractère impératif, ne contraignent pas les entreprises concernées à conclure de telles conventions mais se bornent à les y inviter, en leur laissant la faculté d'adopter d'autres pratiques qui préserveraient de façon équivalente les intérêts de leurs clients. <br/>
<br/>
              11. Il résulte de ce qui précède que la FFSA n'est pas fondée à demander l'annulation de la recommandation qu'elle attaque. Les dispositions de l'article L. 761-1 du code de justice administrative font dès lors obstacle à ce qu'une somme soit mise, à ce titre, à la charge de l'Etat (ACPR) qui n'est pas, dans la présente instance, la partie perdante. Il y a lieu, en revanche, dans les circonstances de l'espèce, de mettre à la charge de la FFSA la somme de 3 000 euros à verser à l'Etat (ACPR) au titre des mêmes dispositions.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Les interventions de la Chambre syndicale des courtiers d'assurances (CSCA), du Groupement des entreprises mutuelles d'assurance (GEMA) et de la Fédération bancaire française (FBF) sont admises.<br/>
Article 2 : La requête de la FFSA est rejetée.<br/>
Article 3 : La FFSA versera à l'Etat (ACPR) la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la Fédération française des sociétés d'assurances (FFSA), à la Chambre syndicale des courtiers d'assurances, au Groupement des entreprises mutuelles d'assurance, à la Fédération bancaire française et à l'Autorité de contrôle prudentiel et de résolution.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">13-027 CAPITAUX, MONNAIE, BANQUES. - RECOMMANDATIONS DE BONNES PRATIQUES PROFESSIONNELLES DE L'ACPR - ACTES DE DROIT SOUPLE SUSCEPTIBLES D'UN RECOURS POUR EXCÈS DE POUVOIR [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-01-01-01 PROCÉDURE. INTRODUCTION DE L'INSTANCE. DÉCISIONS POUVANT OU NON FAIRE L'OBJET D'UN RECOURS. ACTES CONSTITUANT DES DÉCISIONS SUSCEPTIBLES DE RECOURS. - ACTES DE DROIT SOUPLE - RECOMMANDATIONS DE BONNES PRATIQUES PROFESSIONNELLES DE L'ACPR [RJ1].
</SCT>
<ANA ID="9A"> 13-027 Sur le fondement du 2e alinéa de l'article L. 612-29-1 du code monétaire et financier, l'Autorité de contrôle prudentiel et de résolution (ACPR) a émis des recommandations sur les conventions, conclues entre les entreprises d'assurance et les intermédiaires en assurance, concernant la distribution des contrats d'assurance vie. Ces recommandations ont pour objet d'inciter les entreprises d'assurance et les intermédiaires, qui en sont les destinataires, à modifier sensiblement leurs relations réciproques. Elles peuvent faire l'objet d'un recours pour excès de pouvoir.</ANA>
<ANA ID="9B"> 54-01-01-01 Sur le fondement du 2e alinéa de l'article L. 612-29-1 du code monétaire et financier, l'Autorité de contrôle prudentiel et de résolution (ACPR) a émis des recommandations sur les conventions, conclues entre les entreprises d'assurance et les intermédiaires en assurance, concernant la distribution des contrats d'assurance vie. Ces recommandations ont pour objet d'inciter les entreprises d'assurance et les intermédiaires, qui en sont les destinataires, à modifier sensiblement leurs relations réciproques. Elles peuvent faire l'objet d'un recours pour excès de pouvoir.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Assemblée, 21 mars 2016, Société Fairvesta International GMBH et autres, n°s 368082 368083 368084, à publier au Recueil.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
