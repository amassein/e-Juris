<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030866697</ID>
<ANCIEN_ID>JG_L_2015_07_000000374157</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/86/66/CETATEXT000030866697.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème / 1ère SSR, 10/07/2015, 374157</TITRE>
<DATE_DEC>2015-07-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>374157</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème / 1ère SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI, SUREAU ; CORLAY</AVOCATS>
<RAPPORTEUR>Mme Mireille Le Corre</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:374157.20150710</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Bastia de condamner le département de la Haute-Corse à lui verser les sommes de 98 149,80 euros à titre d'indemnité de licenciement et de 15 000 euros en réparation du préjudice moral qu'il estime avoir subi. Par un jugement n° 0400927 du 26 janvier 2006, le tribunal administratif de Bastia a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 06MA01619 du 2 février 2010, la cour administrative d'appel de Marseille a, sur la requête de M.B..., d'une part, annulé ce jugement du tribunal administratif de Bastia, d'autre part, condamné le département de la Haute-Corse à lui verser la somme de 33 656 euros.<br/>
<br/>
              Par une décision n° 338783 du 20 février 2012, le Conseil d'Etat a annulé cet arrêt de la cour administrative d'appel de Marseille et renvoyé l'affaire à cette cour.<br/>
<br/>
              Par un arrêt n° 12MA01005 du 17 octobre 2013, la cour administrative d'appel de Marseille, statuant sur renvoi du Conseil d'Etat, a, sur la requête de M.B..., d'une part, de nouveau annulé le jugement du 26 janvier 2006 du tribunal administratif de Bastia, d'autre part, condamné le département de la Haute-Corse à lui verser la somme de 25 500 euros.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un autre mémoire enregistrés les 23 décembre 2013, 24 mars et 16 avril 2014 au secrétariat du contentieux du Conseil d'Etat, le département de la Haute-Corse demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt de la cour administrative d'appel de Marseille ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de M. B...;<br/>
<br/>
              3°) de mettre à la charge de M. B...la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - la loi n° 84-16 du 11 janvier 1984 ;<br/>
              - la loi n° 84-53 du 26 janvier 1984 ;<br/>
              - le décret n° 88-145 du 15 février 1988 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Mireille Le Corre, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Spinosi, Sureau, avocat du département de la Haute-Corse et à Me Corlay, avocat de M. B...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. B...exerçait les fonctions de responsable du laboratoire agronomique et oenologique de la chambre d'agriculture de la Haute-Corse ; qu'à la suite de la reprise de ce laboratoire par le département de la Haute-Corse, il a été engagé par celui-ci par un contrat de droit public conclu le 6 janvier 2000, avec effet au 1er janvier 2000, pour une durée de trois ans ; qu'il a refusé de signer le nouveau contrat, d'une durée d'un an seulement, proposé au terme du précédent contrat ; que le département, estimant qu'il avait ainsi renoncé de lui-même à son emploi, a pris acte de la fin du lien contractuel en l'invitant à quitter son lieu de travail ; que, par un jugement du 26 janvier 2006, le tribunal administratif de Bastia a rejeté la demande de M. B...tendant à la condamnation du département de la Haute-Corse à lui verser une indemnité de licenciement et des dommages et intérêts en réparation du préjudice qu'il estimait avoir subi ; que, par un arrêt du 17 octobre 2013, contre lequel le département de la Haute-Corse se pourvoit en cassation, la cour administrative d'appel de Marseille a annulé ce jugement et condamné le département de la Haute-Corse à verser à M. B...la somme de 25 500 euros ; <br/>
<br/>
              2. Considérant qu'un agent public qui a été recruté par un contrat à durée déterminée ne bénéficie ni d'un droit au renouvellement de son contrat ni, à plus forte raison, d'un droit au maintien de ses clauses, si l'administration envisage de procéder à son renouvellement ; que, toutefois, l'administration ne peut légalement décider, au terme de son contrat, de ne pas le renouveler ou de proposer à l'agent, sans son accord, un nouveau contrat substantiellement différent du précédent, que pour un motif tiré de l'intérêt du service ; <br/>
<br/>
              3. Considérant que la cour a relevé que le département de la Haute-Corse avait proposé à M. B...de renouveler son contrat pour une durée d'un an seulement et que l'intéressé avait refusé cette proposition ; qu'en jugeant que la modification apportée au contrat initial, qui prévoyait une durée de trois ans, revêtait un caractère substantiel, la cour n'a commis aucune erreur de droit et a porté sur les faits qui lui étaient soumis une appréciation souveraine, exempte de toute dénaturation ; qu'il résulte de ce qui a été dit au point 2 ci-dessus, qu'en jugeant qu'aucun motif tiré de l'intérêt du service ne justifiait légalement la décision du département et que celui-ci avait dès lors commis une faute de nature à engager sa responsabilité, la cour n'a pas commis d'erreur de droit et a suffisamment motivé son arrêt ; <br/>
<br/>
              4. Considérant, toutefois, que lorsqu'un agent public sollicite le versement d'une indemnité en réparation du préjudice subi du fait de l'illégalité de la décision de ne pas renouveler son contrat ou de le modifier substantiellement sans son accord, sans demander l'annulation de cette décision, il appartient au juge de plein contentieux, forgeant sa conviction au vu de l'ensemble des éléments produits par les parties, de lui accorder une indemnité versée pour solde de tout compte et déterminée en tenant compte notamment de la nature et de la gravité de l'illégalité, de l'ancienneté de l'intéressé, de sa rémunération antérieure et des troubles dans ses conditions d'existence ; <br/>
<br/>
              5. Considérant que, pour évaluer le préjudice financier subi par M.B..., la cour administrative d'appel de Marseille a jugé que la faute commise par le département de la Haute-Corse avait eu pour effet de le priver de la rémunération à laquelle il pouvait prétendre jusqu'en décembre 2005, correspondant au terme d'un contrat renouvelé pour une durée de trois ans, et a jugé que le préjudice indemnisable devait être évalué à hauteur de la différence entre les traitements nets perçus, exclusion faite des primes liées à l'exercice effectif des fonctions, et les rémunérations d'activité ou de remplacement perçues pendant la même période ; qu'en statuant ainsi, la cour a méconnu les principes exposés au point 4 ci-dessus et commis une erreur de droit ; <br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que le département de la Haute-Corse est fondé à demander l'annulation de l'arrêt qu'il attaque en tant qu'il statue, par ses articles 2 et 4, sur l'indemnisation des préjudices ; <br/>
<br/>
              7. Considérant qu'aux termes du second alinéa de l'article L. 821-2 du code de justice administrative : " Lorsque l'affaire fait l'objet d'un second pourvoi en cassation, le Conseil d'Etat statue définitivement sur cette affaire " ; qu'il appartient, par suite, au Conseil d'Etat de régler l'affaire au fond dans la mesure de la cassation prononcée au point 6 ;<br/>
<br/>
              8. Considérant que la décision de ne pas renouveler le contrat de l'intéressé à son terme échu ne constitue pas une mesure de licenciement ; que les conclusions présentées par l'intéressé tendant au paiement d'une indemnité de licenciement sur le fondement des dispositions du décret du 15 février 1988 pris pour l'application de l'article 136 de la loi du 26 janvier 1984 modifiée portant dispositions statutaires relatives à la fonction publique territoriale et relatif aux agents non titulaires de la fonction publique territoriale doivent, par suite, être rejetées ; <br/>
<br/>
              9. Considérant, en revanche, qu'il résulte de ce qui a été dit au point 3 ci-dessus que le département de la Haute-Corse a commis une faute de nature à engager sa responsabilité ; que l'indemnisation des préjudices doit être déterminée conformément aux règles énoncées au point 4 ; qu'il résulte de l'instruction que M.B..., qui avait 53 ans au terme de son premier contrat avec le département, a exercé ses fonctions au sein du département pendant une durée de trois ans, après avoir été employé par la chambre d'agriculture pour les mêmes fonctions, et qu'il percevait un revenu net mensuel d'environ 2 800 euros ; que, compte tenu de l'ensemble des circonstances de l'espèce, notamment de la nature et de la gravité de l'illégalité commise, il sera fait une juste appréciation du préjudice subi par M. B...en l'évaluant à la somme de 15 000 euros, tous préjudices et intérêts compris au jour de la présente décision ; <br/>
<br/>
              10. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de M.B..., qui n'est pas, dans la présente instance, la partie perdante ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du département de la Haute-Corse la somme de 2 000 euros à verser à M. B...au titre des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les articles 2 et 4 de l'arrêt de la cour administrative d'appel de Marseille du 17 octobre 2013 sont annulés.<br/>
Article 2 : Le surplus des conclusions du pourvoi du département de la Haute-Corse est rejeté.<br/>
Article 3 : Le département de la Haute-Corse est condamné à verser à M. B...la somme de 15 000 euros.<br/>
Article 4 : Le département de la Haute-Corse versera à M. B...la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 5 : Le surplus des conclusions indemnitaires de M. B...est rejeté.<br/>
Article 6 : La présente décision sera notifiée au département de la Haute-Corse et à M. A...B....<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-12-03-02 FONCTIONNAIRES ET AGENTS PUBLICS. AGENTS CONTRACTUELS ET TEMPORAIRES. FIN DU CONTRAT. REFUS DE RENOUVELLEMENT. - NON RENOUVELLEMENT DU CONTRAT OU PROPOSITION DE RENOUVELLEMENT SUBSTANTIELLEMENT DIFFÉRENTE DU CONTRAT PRÉCÉDENT - 1) CONDITION DE LÉGALITÉ - MOTIF TIRÉ DE L'INTÉRÊT DU SERVICE [RJ1] - 2) RÉPARATION DES PRÉJUDICES EN CAS D'ILLÉGALITÉ - CAS OÙ L'AGENT NE DEMANDE PAS L'ANNULATION DE LA DÉCISION ADMINISTRATIVE - CALCUL DE LA PERTE DE RÉMUNÉRATION SUBIE EN RECONSTITUANT LES REVENUS QUI AURAIENT ÉTÉ PERÇUS EN CAS DE RENOUVELLEMENT DU CONTRAT INITIAL AU MÊMES CONDITIONS - ABSENCE - INDEMNITÉ POUR SOLDE DE TOUT COMPTE - EXISTENCE [RJ2].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">60-04-03 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RÉPARATION. ÉVALUATION DU PRÉJUDICE. - ILLÉGALITÉ DU NON RENOUVELLEMENT DU CONTRAT OU D'UNE PROPOSITION DE RENOUVELLEMENT SUBSTANTIELLEMENT DIFFÉRENTE DU CONTRAT PRÉCÉDENT - RÉPARATION DES PRÉJUDICES - CAS OÙ L'AGENT NE DEMANDE PAS L'ANNULATION DE LA DÉCISION ADMINISTRATIVE - CALCUL DE LA PERTE DE RÉMUNÉRATION SUBIE EN RECONSTITUANT LES REVENUS QUI AURAIENT ÉTÉ PERÇUS EN CAS DE RENOUVELLEMENT DU CONTRAT INITIAL AU MÊMES CONDITIONS - ABSENCE - INDEMNITÉ POUR SOLDE DE TOUT COMPTE - EXISTENCE [RJ2].
</SCT>
<ANA ID="9A"> 36-12-03-02 1) Un agent public qui a été recruté par un contrat à durée déterminée ne bénéficie ni d'un droit au renouvellement de son contrat ni, à plus forte raison, d'un droit au maintien de ses clauses, si l'administration envisage de procéder à son renouvellement. Toutefois, l'administration ne peut légalement décider, au terme de son contrat, de ne pas le renouveler ou de proposer à l'agent, sans son accord, un nouveau contrat substantiellement différent du précédent (en l'espèce renouvellement pour un an d'un contrat de trois ans), que pour un motif tiré de l'intérêt du service....  ,,2) Lorsqu'un agent public sollicite le versement d'une indemnité en réparation du préjudice subi du fait de l'illégalité de la décision de ne pas renouveler son contrat ou de le modifier substantiellement sans son accord, sans demander l'annulation de cette décision, il appartient au juge de plein contentieux, forgeant sa conviction au vu de l'ensemble des éléments produits par les parties, de lui accorder une indemnité versée pour solde de tout compte et déterminée en tenant compte notamment de la nature et de la gravité de l'illégalité, de l'ancienneté de l'intéressé, de sa rémunération antérieure, et  des troubles dans ses conditions d'existence. En l'espèce, la cour administrative d'appel qui a déterminé une indemnité de perte de rémunération calculée en fonction d'un renouvellement du contrat initial pour trois ans, a commis une erreur de droit.</ANA>
<ANA ID="9B"> 60-04-03 Lorsqu'un agent public sollicite le versement d'une indemnité en réparation du préjudice subi du fait de l'illégalité de la décision de ne pas renouveler son contrat ou de le modifier substantiellement sans son accord, sans demander l'annulation de cette décision, il appartient au juge de plein contentieux, forgeant sa conviction au vu de l'ensemble des éléments produits par les parties, de lui accorder une indemnité versée pour solde de tout compte et déterminée en tenant compte notamment de la nature et de la gravité de l'illégalité, de l'ancienneté de l'intéressé, de sa rémunération antérieure, et  des troubles dans ses conditions d'existence. En l'espèce, la cour administrative d'appel qui a déterminé une indemnité de perte de rémunération calculée en fonction d'un renouvellement du contrat initial pour trois ans, a commis une erreur de droit.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Cf., pour le renouvellement du contrat, CE, 19 octobre 1979, commune de Marseille, n° 09922, T. p. 660 ; CE, 5 novembre 1986, commune de Blanquefort, n° 58870, T. p. 432., ,[RJ2]Rappr., pour un cas d'éviction illégale, CE, 22 septembre 2014, Mme,, n° 365199, à mentionner aux tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
