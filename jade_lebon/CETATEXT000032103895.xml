<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032103895</ID>
<ANCIEN_ID>JG_L_2016_02_000000380556</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/10/38/CETATEXT000032103895.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère / 6ème SSR, 24/02/2016, 380556</TITRE>
<DATE_DEC>2016-02-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>380556</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère / 6ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP TIFFREAU, MARLANGE, DE LA BURGADE ; SCP MASSE-DESSEN, THOUVENIN, COUDRAY</AVOCATS>
<RAPPORTEUR>M. Marc Thoumelou</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Jean Lessi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2016:380556.20160224</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              L'entreprise agricole à responsabilité limitée (EARL) Enderlin Marcel a demandé au tribunal administratif de Strasbourg d'annuler pour excès de pouvoir la décision du 12 avril 2010 par laquelle le préfet du Haut-Rhin a délivré un permis de construire à M. B...A...et la décision rejetant son recours gracieux. Par un jugement n° 1004081 du 18 juin 2013, le tribunal administratif de Strasbourg a annulé l'arrêté du 12 avril 2010.<br/>
<br/>
              Par un arrêt n° 13NC01531 du 24 mars 2014, la cour administrative d'appel de Nancy a, à la requête de M.A..., annulé ce jugement et rejeté la demande présentée par l'EARL Enderlin Marcel devant le tribunal administratif de Strasbourg.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 22 mai et 12 août 2014 au secrétariat du contentieux du Conseil d'Etat, l'EARL Enderlin Marcel demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de M. A...; <br/>
<br/>
              3°) de mettre à la charge de l'Etat et de M. A...la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code rural et de la pêche maritime ; <br/>
              - le code de l'urbanisme ;<br/>
              - l'arrêté du ministre de l'écologie et du développement durable du 7 février 2005 fixant les règles techniques auxquelles doivent satisfaire les élevages de bovins, de volailles et/ou de gibier à plumes et de porcs soumis à déclaration au titre du livre V du code de l'environnement ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Marc Thoumelou, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Jean Lessi, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Tiffreau, Marlange, de la Burgade, avocat de l'EARL Enderlin Marcel, et à la SCP Masse-Dessen, Thouvenin, Coudray, avocat de M.A... ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que le préfet du Haut-Rhin a délivré à M.A..., le 12 avril 2010, un permis de construire une maison individuelle sur le territoire de la commune de Moernach, située à moins de cent mètres de l'exploitation agricole dirigée par l'EARL Enderlin Marcel, qui a notamment pour activité l'élevage de bovins et constitue une installation classée pour la protection de l'environnement ; que l'EARL Enderlin Marcel se pourvoit en cassation contre l'arrêt du 24 mars 2014 par lequel la cour administrative d'appel de Nancy a annulé, à la demande de M.A..., le jugement du tribunal administratif de Strasbourg du 18 juin 2013 ayant annulé l'arrêté du 12 avril 2010 ; <br/>
<br/>
              2. Considérant qu'aux termes du premier alinéa de l'article L. 111-3 du code rural et de la pêche maritime : " Lorsque des dispositions législatives ou réglementaires soumettent à des conditions de distance l'implantation ou l'extension de bâtiments agricoles vis-à-vis des habitations et immeubles habituellement occupés par des tiers, la même exigence d'éloignement doit être imposée à ces derniers à toute nouvelle construction et à tout changement de destination précités à usage non agricole nécessitant un permis de construire, à l'exception des extensions de constructions existantes (...) " ; qu'en application de l'article 2.1.1 de l'annexe I de l'arrêté du ministre de l'écologie et du développement durable du 7 février 2005 fixant les règles techniques auxquelles doivent satisfaire, notamment, les élevages de bovins soumis à déclaration au titre du livre V du code de l'environnement : " Les bâtiments d'élevage et leurs annexes sont implantés à au moins 100 mètres des habitations des tiers (...) " ; <br/>
<br/>
              3. Considérant qu'il résulte de l'article L. 111-3 du code rural et de la pêche maritime que les règles de distance imposées, par rapport notamment aux habitations existantes, à l'implantation d'un bâtiment agricole en vertu, en particulier, de la législation relative aux installations classées pour la protection de l'environnement sont également applicables, par effet de réciprocité, à la délivrance du permis de construire une habitation située à proximité d'un tel bâtiment agricole ; qu'il appartient ainsi à l'autorité compétente pour délivrer le permis de construire un bâtiment à usage d'habitation de vérifier le respect des dispositions législatives ou réglementaires fixant de telles règles de distance, quelle qu'en soit la nature ; que, par suite, la cour administrative d'appel de Nancy a commis une erreur de droit en jugeant que la vérification du respect des dispositions de l'arrêté du 7 février 2005 citées au point 2, dès lors qu'elles relevaient de la législation relative aux installations classées pour la protection de l'environnement, ne s'imposait pas au préfet du Haut-Rhin pour la délivrance du permis de construire en litige ; <br/>
<br/>
              4. Considérant qu'il résulte de tout ce qui précède que l'EARL Enderlin Marcel est fondée, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, à demander l'annulation de l'arrêt de la cour administrative d'appel de Nancy qu'elle attaque ;<br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge l'Etat et de M. A...une somme de 1 500 euros chacun à verser à l'EARL Enderlin Marcel, au titre de l'article L. 761-1 du code de justice administrative ; que ces dispositions font obstacle à ce qu'une somme soit mis à la charge de l'EARL Enderlin Marcel, qui n'est pas, dans la présente instance, la partie perdante ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nancy du 24 mars 2014 est annulé. <br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nancy.<br/>
Article 3 : L'Etat et M. A...verseront à l'EARL Enderlin Marcel une somme de 1 500 euros chacun au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 4 : Les conclusions de M. A...présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 5 : La présente décision sera notifiée à l'EARL Enderlin Marcel, à M. B...A...et à la ministre du logement et de l'habitat durable.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">44-02-01-01 NATURE ET ENVIRONNEMENT. INSTALLATIONS CLASSÉES POUR LA PROTECTION DE L'ENVIRONNEMENT. CHAMP D'APPLICATION DE LA LÉGISLATION. INDÉPENDANCE À L'ÉGARD D'AUTRES LÉGISLATIONS. - RÈGLES DE DISTANCE ENTRE LES BÂTIMENTS AGRICOLES ET LES HABITATIONS - APPLICABILITÉ AUX PERMIS DE CONSTRUIRE - EXISTENCE, Y COMPRIS SI LA RÈGLE EST FIXÉE PAR LA LÉGISLATION DES ICPE (ART. L. 111-3 DU CODE RURAL ET DE LA PÊCHE MARITIME) [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">68-03-03-01-05 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PERMIS DE CONSTRUIRE. LÉGALITÉ INTERNE DU PERMIS DE CONSTRUIRE. LÉGALITÉ AU REGARD DE LA RÉGLEMENTATION NATIONALE. DIVERSES DISPOSITIONS LÉGISLATIVES OU RÉGLEMENTAIRES. - RÈGLES DE DISTANCE ENTRE LES BÂTIMENTS AGRICOLES ET LES HABITATIONS - APPLICABILITÉ AUX PERMIS DE CONSTRUIRE - EXISTENCE, Y COMPRIS SI LA RÈGLE EST FIXÉE PAR LA LÉGISLATION DES ICPE (ART. L. 111-3 DU CODE RURAL ET DE LA PÊCHE MARITIME) [RJ1].
</SCT>
<ANA ID="9A"> 44-02-01-01 Il résulte de l'article L. 111-3 du code rural et de la pêche maritime que les règles de distance imposées, par rapport notamment aux habitations existantes, à l'implantation d'un bâtiment agricole en vertu, en particulier, de la législation relative aux installations classées pour la protection de l'environnement (ICPE) sont également applicables, par effet de réciprocité, à la délivrance du permis de construire une habitation située à proximité d'un tel bâtiment agricole.... ,,Il appartient ainsi à l'autorité compétente pour délivrer le permis de construire un bâtiment à usage d'habitation de vérifier le respect des dispositions législatives ou réglementaires fixant de telles règles de distance, quelle qu'en soit la nature. En l'espèce, la règle fixée par l'article 2.1.1 de l'annexe I de l'arrêté du ministre de l'écologie et du développement durable du 7 février 2005 prévoyant une distance minimale de 100 mètres entre les bâtiments d'élevage de bovins et les habitations des tiers est donc applicable à la délivrance du permis de construire sollicité.</ANA>
<ANA ID="9B"> 68-03-03-01-05 Il résulte de l'article L. 111-3 du code rural et de la pêche maritime que les règles de distance imposées, par rapport notamment aux habitations existantes, à l'implantation d'un bâtiment agricole en vertu, en particulier, de la législation relative aux installations classées pour la protection de l'environnement (ICPE) sont également applicables, par effet de réciprocité, à la délivrance du permis de construire une habitation située à proximité d'un tel bâtiment agricole.... ,,Il appartient ainsi à l'autorité compétente pour délivrer le permis de construire un bâtiment à usage d'habitation de vérifier le respect des dispositions législatives ou réglementaires fixant de telles règles de distance, quelle qu'en soit la nature. En l'espèce, la règle fixée par l'article 2.1.1 de l'annexe I de l'arrêté du ministre de l'écologie et du développement durable du 7 février 2005 prévoyant une distance minimale de 100 mètres entre les bâtiments d'élevage de bovins et les habitations des tiers est donc applicable à la délivrance du permis de construire sollicité.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Cf. CE, 4 novembre 2013, M. Casavielle-Soule, n° 351538, inédite au Recueil. Comp., s'agissant de l'inapplicabilité de ces règles lors de la délivrance du permis de construire de l'installation classée compte tenu de leur application à l'autorisation d'ouverture de l'installation, CE, 2 février 2009, M. Dohm et autres, n° 312131, inédite au Recueil ; CE, 16 octobre 2013, GFA du Château du Villard et autres, n°s 357444, 358425, inédite au Recueil.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
