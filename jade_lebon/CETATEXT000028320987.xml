<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028320987</ID>
<ANCIEN_ID>JG_L_2013_12_000000362987</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/32/09/CETATEXT000028320987.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème / 5ème SSR, 11/12/2013, 362987</TITRE>
<DATE_DEC>2013-12-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>362987</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème / 5ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Florence Chaltiel-Terral</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Gaëlle Dumortier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:362987.20131211</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu 1°, sous le n° 362987, la requête, enregistrée le 24 septembre 2012 au secrétariat du contentieux du Conseil d'Etat, présentée par M. et Mme A...B..., demeurant à ...; M. et MmeB..., agissant au nom de leur enfant mineur, demandent au Conseil d'Etat d'annuler pour excès de pouvoir l'arrêté du 16 juillet 2012 modifiant l'arrêté du 15 septembre 1993 modifié relatif aux épreuves anticipées du baccalauréat général et du baccalauréat technologique ;<br/>
<br/>
<br/>
              Vu 2°, sous le n° 363029, la requête, enregistrée le 26 septembre 2012 au secrétariat du contentieux du Conseil d'Etat, présentée par M. et MmeC..., demeurant ... ; M. et MmeC..., agissant au nom de leur enfant mineur, demandent au Conseil d'Etat d'annuler pour excès de pouvoir le même arrêté ;<br/>
<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;	<br/>
<br/>
              Vu le code de l'éducation ;<br/>
<br/>
              Vu l'arrêté du 15 septembre 1993 relatif aux épreuves anticipées du baccalauréat général et du baccalauréat technologique ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Florence Chaltiel-Terral, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que les requêtes de M. et Mme B...et de M. et Mme C... sont dirigées contre le même arrêté du 16 juillet 2012 ; qu'il y a lieu de les joindre pour statuer par une seule décision ; <br/>
<br/>
              2. Considérant que l'arrêté attaqué du ministre de l'éducation nationale a pour objet de modifier l'article 6 de l'arrêté du 15 septembre 1993 relatif aux épreuves anticipées du baccalauréat général et du baccalauréat technologique, en supprimant la possibilité, notamment pour les candidats ayant subi par anticipation l'épreuve d'histoire-géographie de la série scientifique du baccalauréat général, de conserver leur note, lorsqu'ils présentent les épreuves du baccalauréat général en terminale des séries économique et sociale et littéraire ; que l'article 3 de l'arrêté prévoit que ces dispositions " sont applicables à compter de la session 2013 (...) et prennent effet pour les épreuves anticipées de cette session qui sont organisées en 2012 " ; que par l'effet de ces dispositions, les candidats ayant subi par anticipation l'épreuve d'histoire-géographie en première scientifique en 2012 ne conservent pas le bénéfice de la note obtenue lorsqu'ils présentent les épreuves du baccalauréat général en terminale des séries économique et sociale et littéraire de la session 2013 et doivent en conséquence subir l'épreuve correspondante en terminale ;<br/>
<br/>
              3. Considérant, en premier lieu, que si M. et Mme B...soutiennent que l'arrêté est entaché de défaut de motivation, cet arrêté réglementaire n'est pas au nombre des actes devant faire l'objet d'une motivation en vertu des textes applicables ; que, par suite, le moyen doit être écarté ;<br/>
<br/>
              4. Considérant, en deuxième lieu, que si l'arrêté contesté fait référence aux épreuves " qui ont lieu en 2012 ", cette référence concerne nécessairement les épreuves subies en 2012, quelle qu'en soit la date, dont les notes ne pourront être conservées en 2013 ; que dès lors, M. et Mme B...ne sont, en tout état de cause, pas fondés à soutenir qu'en n'employant pas un temps passé, l'arrêté attaqué serait entaché de " vice de forme " ;<br/>
<br/>
              5. Considérant, en troisième lieu, qu'en principe, lorsque de nouvelles normes générales sont édictées par voie de décret ou d'arrêté, elles ont vocation à s'appliquer immédiatement, sans que les personnes auxquelles sont, le cas échéant, imposées de nouvelles contraintes puissent invoquer le droit au maintien de la réglementation existante, sous réserve des exigences attachées au principe de non-rétroactivité des actes administratifs, qui exclut que les nouvelles dispositions s'appliquent à des situations juridiquement constituées avant l'entrée en vigueur de ces dispositions ; qu'en matière d'enseignement, ce principe ne fait pas obstacle à l'application immédiate, même aux élèves engagés dans un cycle de formation sanctionné par un diplôme, des  dispositions réglementaires relatives à la formation qui leur est dispensée et notamment aux modalités d'évaluation des connaissances ;<br/>
<br/>
              6. Considérant qu'en décidant que les nouvelles dispositions relatives aux épreuves du baccalauréat sont applicables aux élèves ayant auparavant subi les épreuves anticipées qu'il mentionne, l'arrêté contesté se borne à régler les effets futurs d'une situation passée, sans remettre en cause aucune situation juridiquement constituée sous l'empire de la réglementation antérieure et ne comporte ainsi, contrairement à ce que soutiennent les requérants, aucun effet rétroactif ;<br/>
<br/>
              7. Considérant, en quatrième lieu, qu'il résulte des termes mêmes de l'article 2 de l'arrêté attaqué que les nouvelles dispositions contestées ne concernent pas seulement les élèves de première scientifique décidant de se réorienter en terminale économique et sociale ; que doit, par suite, être écarté le moyen tiré de ce qu'en lésant spécifiquement cette catégorie d'élèves, l'arrêté serait contraire au principe d'égalité ; <br/>
<br/>
              8. Considérant qu'il résulte de tout ce qui précède que les requérants ne sont pas fondés à demander l'annulation de l'arrêté contesté ; <br/>
<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : Les requêtes nos 362987 et 363029 sont rejetées.<br/>
Article 2 : La présente décision sera notifiée à M. et Mme A...B..., à M. et Mme C... et au ministre de l'éducation nationale.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-08 ACTES LÉGISLATIFS ET ADMINISTRATIFS. APPLICATION DANS LE TEMPS. - ABSENCE DE DROIT ACQUIS AU MAINTIEN D'UNE RÉGLEMENTATION - 1) PORTÉE GÉNÉRALE DU PRINCIPE - LIMITE - PRINCIPE DE NON-RÉTROACTIVITÉ - 2) PORTÉE EN MATIÈRE D'ENSEIGNEMENT - POSSIBILITÉ DE MODIFIER LES RÈGLES RELATIVES À LA FORMATION ET À L'ÉVALUATION DES ÉLÈVES DÉJÀ ENGAGÉS DANS UN CYCLE DE FORMATION SANCTIONNÉ PAR UN DIPLÔME, DÈS LORS QUE LA MODIFICATION NE REVÊT PAS UN CARACTÈRE RÉTROACTIF [RJ1] - 3) CAS D'ESPÈCE - RÉFORME PRIVANT DES CANDIDATS AU BACCALAURÉAT AYANT CHANGÉ DE SÉRIE DU BÉNÉFICE DE LA NOTE OBTENUE DANS UNE CERTAINE DISCIPLINE LORS DE L'ÉPREUVE ANTICIPÉE ORGANISÉE EN PREMIÈRE DANS LEUR SÉRIE D'ORIGINE - CARACTÈRE RÉTROACTIF - ABSENCE - CONSÉQUENCE - LÉGALITÉ DE LA MODIFICATION.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">30-01-04-03 ENSEIGNEMENT ET RECHERCHE. QUESTIONS GÉNÉRALES. EXAMENS ET CONCOURS. DROITS DES CANDIDATS. - ABSENCE DE DROIT ACQUIS AU MAINTIEN DE LA RÉGLEMENTATION D'UN EXAMEN - 1) PORTÉE DU PRINCIPE - POSSIBILITÉ DE MODIFIER LES RÈGLES RELATIVES À LA FORMATION ET À L'ÉVALUATION DES ÉLÈVES DÉJÀ ENGAGÉS DANS UN CYCLE DE FORMATION SANCTIONNÉ PAR UN DIPLÔME, DÈS LORS QUE LA MODIFICATION NE REVÊT PAS UN CARACTÈRE RÉTROACTIF [RJ1] - 2) CAS D'ESPÈCE - RÉFORME PRIVANT DES CANDIDATS AU BACCALAURÉAT AYANT CHANGÉ DE SÉRIE DU BÉNÉFICE DE LA NOTE OBTENUE DANS UNE CERTAINE DISCIPLINE LORS DE L'ÉPREUVE ANTICIPÉE ORGANISÉE EN PREMIÈRE DANS LEUR SÉRIE D'ORIGINE - CARACTÈRE RÉTROACTIF - ABSENCE - CONSÉQUENCE - LÉGALITÉ DE LA MODIFICATION.
</SCT>
<ANA ID="9A"> 01-08 1) En principe, lorsque de nouvelles normes générales sont édictées par voie de décret ou d'arrêté, elles ont vocation à s'appliquer immédiatement, sans que les personnes auxquelles sont, le cas échéant, imposées de nouvelles contraintes puissent invoquer le droit au maintien de la réglementation existante, sous réserve des exigences attachées au principe de non-rétroactivité des actes administratifs, qui exclut que les nouvelles dispositions s'appliquent à des situations juridiquement constituées avant l'entrée en vigueur de ces dispositions.,,,2) En matière d'enseignement, ce principe ne fait pas obstacle à l'application immédiate, même aux élèves engagés dans un cycle de formation sanctionné par un diplôme, des dispositions réglementaires relatives à la formation qui leur est dispensée et notamment aux modalités d'évaluation des connaissances.,,,3) Modification de la réglementation du baccalauréat conduisant à ce que les candidats effectuant certains changements de séries entre la première et la terminale ne pourront désormais plus, à la différence de ce que prévoyait la réglementation antérieure, conserver le bénéfice de la note obtenue lors de l'épreuve anticipée d'histoire-géographie subie en première dans leur série d'origine et devront repasser l'épreuve dans cette discipline en terminale dans leur nouvelle série.... ,,En décidant que les nouvelles dispositions relatives aux épreuves du baccalauréat sont applicables aux élèves ayant auparavant subi les épreuves anticipées qu'il mentionne, l'arrêté contesté se borne à régler les effets futurs d'une situation passée, sans remettre en cause aucune situation juridiquement constituée sous l'empire de la réglementation antérieure et ne comporte ainsi aucun effet rétroactif.</ANA>
<ANA ID="9B"> 30-01-04-03 1) En principe, lorsque de nouvelles normes générales sont édictées par voie de décret ou d'arrêté, elles ont vocation à s'appliquer immédiatement, sans que les personnes auxquelles sont, le cas échéant, imposées de nouvelles contraintes puissent invoquer le droit au maintien de la réglementation existante, sous réserve des exigences attachées au principe de non-rétroactivité des actes administratifs, qui exclut que les nouvelles dispositions s'appliquent à des situations juridiquement constituées avant l'entrée en vigueur de ces dispositions.,,,En matière d'enseignement, ce principe ne fait pas obstacle à l'application immédiate, même aux élèves engagés dans un cycle de formation sanctionné par un diplôme, des dispositions réglementaires relatives à la formation qui leur est dispensée et notamment aux modalités d'évaluation des connaissances.,,,2) Modification de la réglementation du baccalauréat conduisant à ce que les candidats effectuant certains changements de séries entre la première et la terminale ne pourront désormais plus, à la différence de ce que prévoyait la réglementation antérieure, conserver le bénéfice de la note obtenue lors de l'épreuve anticipée d'histoire-géographie subie en première dans leur série d'origine et devront repasser l'épreuve dans cette discipline en terminale dans leur nouvelle série.... ,,En décidant que les nouvelles dispositions relatives aux épreuves du baccalauréat sont applicables aux élèves ayant auparavant subi les épreuves anticipées qu'il mentionne, l'arrêté contesté se borne à régler les effets futurs d'une situation passée, sans remettre en cause aucune situation juridiquement constituée sous l'empire de la réglementation antérieure et ne comporte ainsi aucun effet rétroactif.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr. CE, 18 février 1994, Association générale des étudiants de sciences politiques, n° 149548, T. pp. 968-969-975-978.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
