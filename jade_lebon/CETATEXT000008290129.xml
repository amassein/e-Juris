<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000008290129</ID>
<ANCIEN_ID>JG_L_2006_12_000000298912</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/08/29/01/CETATEXT000008290129.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 14/12/2006, 298912</TITRE>
<DATE_DEC>2006-12-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>298912</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>Mme Hagelsteen</PRESIDENT>
<AVOCATS>SCP PIWNICA, MOLINIE ; SCP LYON-CAEN, FABIANI, THIRIEZ</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 20 novembre 2006 au secrétariat du contentieux du Conseil d'Etat, présentée pour la société BOURSE DIRECT SA, dont le siège est 253, Bd Péreire à Paris (75017) ; la société BOURSE DIRECT SA demande au juge des référés du Conseil d'Etat de suspendre l'exécution de la décision de la commission des sanctions de l'Autorité des marchés financiers en date du 5 octobre 2006 en ce qu'elle a ordonné la publication de la sanction infligée à la société exposante ;<br/>
<br/>
<br/>
<br/>
              la société BOURSE DIRECT SA soutient qu'il y a urgence à suspendre la décision attaquée ; qu'en effet, compte tenu de la spécificité du marché sur lequel elle intervient, la publication de la sanction désignera immédiatement et durablement la société BOURSE DIRECT SA et ses nouveaux dirigeants comme fautifs alors même que les faits reprochés ont été commis il y a plusieurs années par les dirigeants fondateurs de la société ; qu'ainsi, l'exécution de la décision contestée entraînera une diminution du nombre de transactions et une perte du chiffre d'affaire, préjudiciant de ce fait immédiatement et gravement aux intérêts de la société requérante ; que du reste, l'éventuelle publication de l'annulation ou de la réformation de la mesure de publicité ne permettra pas de remédier à ses effets négatifs ; que, par ailleurs, il existe plusieurs moyens de nature à créer un doute sérieux sur la légalité de la décision litigieuse ; qu'ainsi, la motivation de la décision de sanction, qui doit s'apprécier au regard de la teneur des arguments des parties, est insuffisante et stéréotypée ; qu'en effet, compte tenu de la recommandation du rapporteur de ne pas assortir la sanction d'une mesure de publication, et de l'argumentation de la société requérante, qui a fait valoir l'ancienneté des faits reprochés et leur imputabilité aux seuls dirigeants historiques de la société BOURSE DIRECT SA, la commission des sanctions aurait dû précisément exposer les considérations qui motivent sa décision de publication, et ce d'autant plus que la directive 2003/6/CE du parlement et du conseil du 28 janvier 2003 prévoit notamment que les sanctions pourront être publiées sauf si cette publication cause un préjudice disproportionné aux parties en cause ; que, de plus, la commission des sanctions, en omettant de procéder à la mise en balance des préjudices subis par la société BOURSE DIRECT SA et des avantages résultant de la publication de la sanction, a commis une erreur de droit, par fausse application de l'article L. 621-15 du code monétaire et financier, de l'article 14 de la directive 2003/6/CE du parlement et du conseil du 28 janvier 2003 et du principe de proportionnalité ; qu'enfin, la décision attaquée viole l'article 6§2 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et le droit à un délai raisonnable de jugement ; qu'ainsi, la décision attaquée, qui est une mesure très redoutée des acteurs du marché, porte à la société requérante une atteinte grave à sa réputation, qui n'est pas proportionnée aux objectifs de pédagogie et de sécurité juridique poursuivis par la publication ; que la seule mesure nécessaire consiste en une anonymisation de la publication de la sanction ; que, pour finir, la sanction litigieuse a été prononcée dans un délai non raisonnable ;<br/>
<br/>
<br/>
<br/>
              Vu la décision dont la suspension est demandée ;<br/>
<br/>
              Vu la copie de la requête aux fins d'annulation présentée pour la société BOURSE DIRECT SA à l'encontre de cette décision ;<br/>
              Vu le mémoire en défense, enregistré le 1er décembre 2006, présenté pour l'Autorité des marchés financiers, qui conclut au rejet de la requête et à ce que la société BOURSE DIRECT SA lui verse la somme de 5 000 euros au titre des frais non compris dans les dépens ; elle soutient que les conditions de l'article L. 521-1 du code de justice administrative ne sont pas remplies ; que l'urgence, qui s'apprécie en fonction des justifications fournies par le requérant et des observations fournies par le défendeur, n'est pas constituée en l'espèce ; qu'il résulte de la jurisprudence du juge des référés du Conseil d'Etat qu'une mesure de publication ne fait pas présumer en elle-même l'existence d'un préjudice de nature à caractériser une situation d'urgence ; que, pourtant, la société requérante se borne à alléguer que la publication de la sanction litigieuse causera une atteinte à sa réputation, laquelle se traduira par une perte de chiffre d'affaire, sans préciser en quoi sa situation caractérise concrètement l'urgence, et alors que la décision attaquée fait état, d'une part, de l'installation d'une nouvelle équipe dirigeante, à laquelle les faits sanctionnés ne sont pas imputables, et, d'autre part, des diligences consenties par cette nouvelle équipe dirigeante pour assurer une bonne application de la réglementation ; que, par ailleurs, la société BOURSE DIRECT SA a été sanctionnée en raison de faits violant les obligations d'exactitude, de précision et de sincérité des informations données au public ; que la publication de cette sanction vise à avertir l'ensemble des opérateurs de l'interprétation donnée aux textes par la commission des sanctions, à restituer la vérité sur l'activité de la société requérante et à protéger les intérêts des épargnants ; qu'ainsi, à défaut pour la société requérante d'établir une situation d'urgence, les objectifs poursuivis par la publication de la sanction justifient l'exécution immédiate de la décision attaquée ; qu'ensuite, aucun des moyens soulevés par la société BOURSE DIRECT SA n'est de nature à créer un doute sérieux sur la légalité de la décision litigieuse ; que la motivation de la mesure de publication doit s'apprécier en tenant compte de la motivation de la sanction principale et des faits qui ont servi de fondement à cette dernière ; qu'à cet égard, la sanction comporte une motivation en droit et en fait particulièrement rigoureuse ; que dès lors, la commission des sanctions, qui dispose, s'agissant des mesures de publication des sanctions, d'un pouvoir discrétionnaire n'impliquant aucune obligation de motivation spécifique, a correctement motivé la décision attaquée ; qu'enfin, la commission des sanctions n'a commis aucune erreur de droit ; que, d'abord, la directive 2003/6/CE du parlement et du conseil du 28 janvier 2003 n'est pas dotée d'un effet direct permettant à un particulier de s'en prévaloir à l'encontre d'une décision individuelle ; qu'ensuite, la commission des sanctions a correctement mis en oeuvre le principe de proportionnalité ; que, du reste, l'office du juge des référés est en l'espèce limité au contrôle de l'absence de disproportion manifeste de la sanction ;<br/>
<br/>
              Vu le mémoire en réplique enregistré le 7 décembre 2006, présenté pour la société BOURSE DIRECT SA ; la société requérante persiste dans ses moyens et conclusions ;<br/>
<br/>
              Vu la note en délibéré enregistrée le 12 décembre 2006 présentée pour l'Autorité des marchés financiers ;<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu le code monétaire et financier ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, la société BOURSE DIRECT SA et, d'autre part, l'Autorité des marchés financiers ;<br/>
<br/>
              Vu le procès-verbal de l'audience publique du 11 décembre 2006 à 17 heures au cours de laquelle ont été entendus :<br/>
<br/>
              - Me Molinié, avocat au Conseil d'Etat et à la Cour de cassation, avocat de la société BOURSE DIRECT SA ;<br/>
<br/>
              - les représentants de la société BOURSE DIRECT SA ;<br/>
<br/>
              - Me Lyon-Caen, avocat au Conseil d'Etat et à la Cour de cassation, avocat de l'Autorité des marchés financiers ;<br/>
<br/>
              - la représentante de l'Autorité des marchés financiers ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : «  Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision » ; qu'il résulte de ces dispositions que le prononcé de la suspension d'un acte administratif est subordonné notamment à une condition d'urgence ; que l'urgence justifie la suspension de l'exécution d'un acte administratif lorsque celui-ci porte atteinte de manière suffisamment grave et immédiate à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre ; qu'il appartient au juge des référés d'apprécier concrètement, compte tenu des justifications fournies par le requérant, si les effets de l'acte contesté sont de nature à caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, l'exécution de la décision soit suspendue ; que l'urgence doit être appréciée objectivement et compte tenu de l'ensemble des circonstances de l'affaire ;<br/>
<br/>
              Considérant qu'une décision de la commission des sanctions de l'Autorité des marchés financiers (AMF) en date du 5 octobre 2006 a prononcé à l'encontre de la société BOURSE DIRECT SA une sanction pécuniaire de 5 000 euros et a ordonné la publication de cette décision au « Bulletin des annonces légales obligatoires » ainsi que sur le site Internet et dans la revue de l'Autorité des marchés financiers ; que la société BOURSE DIRECT SA demande la suspension de cette décision en tant qu'elle a ordonné sa publication ;<br/>
<br/>
              Considérant qu'aux termes de l'article L. 621-15 V du code monétaire et financier : « La commission des sanctions peut rendre publique sa décision dans les publications, journaux ou supports qu'elle désigne. Les frais sont supportés par les personnes sanctionnées » ; que par ces dispositions, le législateur a entendu permettre à la commission de tenir compte de l'exigence d'intérêt général qui s'attache, afin de protéger les épargnants, d'une part, à la transparence et au bon fonctionnement du marché qui implique la connaissance par l'ensemble des opérateurs de l'interprétation qu'elle donne des règles qu'ils doivent observer, et d'autre part, à l'information des tiers intéressés ; qu'il ne peut être exclu toutefois que, dans certaines circonstances particulières, la publication d'une décision de sanction cause à la personne sanctionnée un préjudice d'une telle gravité qu'il pourrait y avoir urgence à suspendre cette publication jusqu'à ce que le juge se soit prononcé au fond ;<br/>
<br/>
              Considérant d'une part, que la société BOURSE DIRECT SA n'a pas contesté la décision de la commission des sanctions de l'AMF en tant qu'elle lui a infligé une sanction pécuniaire de 5 000 euros ; que d'autre part, elle ne justifie d'aucune circonstance particulière de nature à démontrer que la publication immédiate de cette décision lui cause un préjudice tel qu'une situation d'urgence serait constituée à son égard ; que, notamment, si elle se prévaut de la circonstance que la publication de cette décision est de nature à porter atteinte à son image sur la place de Paris et donc à affecter immédiatement les courants d'affaires qu'elle traite, alors que les faits sanctionnés sont anciens et imputables seulement aux anciens dirigeants, que le contrôle de la société a changé et que la nouvelle direction a corrigé les errements relevés, ces circonstances sont expressément mentionnées dans la décision du 5 octobre 2006 dont la publication intégrale est ordonnée ; <br/>
<br/>
               Considérant que dans ces conditions, et en l'état de l'instruction, la condition d'urgence posée par l'article L. 521-1 du code de justice administrative n'est pas satisfaite ;<br/>
<br/>
              Sur les conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société BOURSE DIRECT SA le paiement d'une somme de 3 000 euros au titre des frais exposés par l'Autorité des Marchés Financiers et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de la société BOURSE DIRECT SA est rejetée.<br/>
Article 2 : La société BOURSE DIRECT SA versera à l'Autorité des Marchés Financiers une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente ordonnance sera notifiée à la société BOURSE DIRECT SA, à l'Autorité des marchés financiers et au ministre de l'économie, des finances et de l'industrie.<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">13-01-02-01 CAPITAUX, MONNAIE, BANQUES. CAPITAUX. OPÉRATIONS DE BOURSE. AUTORITÉ DES MARCHÉS FINANCIERS. - DÉCISION DE RENDRE PUBLIQUE UNE DÉCISION DE SANCTION (ART. L. 621-15 V DU CODE MONÉTAIRE ET FINANCIER) - FACULTÉ POUR LA SOCIÉTÉ VISÉE PAR LA SANCTION DE DEMANDER LA SUSPENSION DE LA MESURE DE PUBLICITÉ - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-035-02 PROCÉDURE. PROCÉDURES INSTITUÉES PAR LA LOI DU 30 JUIN 2000. RÉFÉRÉ SUSPENSION (ART. L. 521-1 DU CODE DE JUSTICE ADMINISTRATIVE). - CHAMP D'APPLICATION - INCLUSION - DEMANDE DE SUSPENSION DE LA DÉCISION PAR LAQUELLE LA COMMISSION DES SANCTIONS DE L'AUTORITÉ DES MARCHÉS FINANCIERS DÉCIDE DE RENDRE PUBLIQUE UNE DÉCISION DE SANCTION À L'ENCONTRE D'UNE SOCIÉTÉ (ART. L. 621-15 V DU CODE MONÉTAIRE ET FINANCIER).
</SCT>
<ANA ID="9A"> 13-01-02-01 Aux termes du V de l'article L. 621-15 du code monétaire et financier : « La commission des sanctions peut rendre publique sa décision dans les publications, journaux ou supports qu'elle désigne. Les frais sont supportés par les personnes sanctionnées ». Par ces dispositions, le législateur a entendu permettre à la commission de tenir compte de l'exigence d'intérêt général qui s'attache, afin de protéger les épargnants, d'une part, à la transparence et au bon fonctionnement du marché qui implique la connaissance par l'ensemble des opérateurs de l'interprétation qu'elle donne des règles qu'ils doivent observer, et d'autre part, à l'information des tiers intéressés. Il ne peut être exclu toutefois que, dans certaines circonstances particulières, la publication d'une décision de sanction cause à la personne sanctionnée un préjudice d'une telle gravité qu'il pourrait y avoir urgence à suspendre cette publication jusqu'à ce que le juge se soit prononcé au fond.</ANA>
<ANA ID="9B"> 54-035-02 Aux termes du V de l'article L. 621-15 du code monétaire et financier : « La commission des sanctions peut rendre publique sa décision dans les publications, journaux ou supports qu'elle désigne. Les frais sont supportés par les personnes sanctionnées ». Par ces dispositions, le législateur a entendu permettre à la commission de tenir compte de l'exigence d'intérêt général qui s'attache, afin de protéger les épargnants, d'une part, à la transparence et au bon fonctionnement du marché qui implique la connaissance par l'ensemble des opérateurs de l'interprétation qu'elle donne des règles qu'ils doivent observer, et d'autre part, à l'information des tiers intéressés. Il ne peut être exclu toutefois que, dans certaines circonstances particulières, la publication d'une décision de sanction cause à la personne sanctionnée un préjudice d'une telle gravité qu'il pourrait y avoir urgence à suspendre cette publication jusqu'à ce que le juge se soit prononcé au fond.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
