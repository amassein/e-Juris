<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044200812</ID>
<ANCIEN_ID>JG_L_2021_10_000000443903</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/20/08/CETATEXT000044200812.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 1ère chambres réunies, 12/10/2021, 443903</TITRE>
<DATE_DEC>2021-10-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>443903</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUVENIN, COUDRAY, GREVY</AVOCATS>
<RAPPORTEUR>Mme Thalia Breton</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Raphaël Chambon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:443903.20211012</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A... B... a demandé au juge des référés du tribunal administratif de Mayotte, statuant sur le fondement de l'article R. 541-1 du code de justice administrative, de condamner l'État à lui verser une somme de plus de 10 000 euros assortie des intérêts au taux légal, à titre de provision à valoir sur l'indemnisation du dommage qu'il estime avoir subi du fait de la réduction de moitié de sa rémunération par l'arrêté du 15 octobre 2019 du vice-recteur de l'académie de Mayotte. Par une ordonnance n° 2000488 du 22 avril 2020, le juge des référés du tribunal administratif de Mayotte a rejeté sa demande.<br/>
<br/>
              Par une ordonnance n° 20BX01603 du 27 juillet 2020, le juge des référés de la cour administrative d'appel de Bordeaux a rejeté l'appel formé par M. B... contre cette ordonnance. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 9 et 24 septembre 2020 et le 30 avril 2021 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant sur le fondement de l'article R. 541-1 du code de justice administrative, de faire droit à son appel et de condamner l'État à lui verser, à titre de provision, une somme de 15 000 euros, majorée de 1 700 euros par mois à compter du 8 juillet 2020, assortie des intérêts au taux légal eux-mêmes capitalisés ;<br/>
<br/>
              3°) de mettre à la charge de l'État la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code pénal ;<br/>
              - le code de procédure pénale ; <br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - le décret n° 94-874 du 7 octobre 1994 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Thalia Breton, auditrice,  <br/>
<br/>
              - les conclusions de M. Raphaël Chambon, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Thouvenin, Coudray, Grevy, avocat de M. B... ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge des référés de la cour administrative d'appel de Bordeaux que, par un jugement du 15 mai 2019, le tribunal correctionnel de Mamoudzou a condamné M. B..., conseiller principal d'éducation stagiaire, à une peine de vingt mois d'emprisonnement pour des faits d'agressions sexuelles sur mineur de quinze ans. M. B... en a interjeté appel le 20 mai 2019. Par un arrêté du 7 juin 2019, pris sur le fondement des dispositions de l'article 30 de la loi du 13 juillet 1983 portant droits et obligations des fonctionnaires, le vice-recteur de l'académie de Mayotte l'a suspendu temporairement de ses fonctions à compter du 7 juin 2019. Par un arrêté du 15 octobre 2019, il a prolongé cette mesure de suspension et réduit de moitié la rémunération de M. B.... M. B... a demandé au juge des référés du tribunal administratif de Mayotte, statuant sur le fondement de l'article R. 541-1 du code de justice administrative, de condamner l'État à lui verser une somme de plus de 10 000 euros assortie des intérêts au taux légal à titre de provision à valoir sur l'indemnisation du dommage qu'il estime avoir subi du fait de la réduction de moitié de sa rémunération. Par une ordonnance du 22 avril 2020, le juge des référés du tribunal administratif de Mayotte a rejeté sa demande. Par une ordonnance du 27 juillet 2020, contre laquelle M. B... se pourvoit en cassation, le juge des référés de la cour administrative d'appel de Bordeaux a rejeté l'appel qu'il avait formé contre l'ordonnance du 22 avril 2020. <br/>
<br/>
              2. Aux termes, d'une part, de l'article R. 541-1 du code de justice administrative : " Le juge des référés peut, même en l'absence d'une demande au fond, accorder une provision au créancier qui l'a saisi lorsque l'existence de l'obligation n'est pas sérieusement contestable. Il peut, même d'office, subordonner le versement de la provision à la constitution d'une garantie ".<br/>
<br/>
              3. Aux termes, d'autre part, de l'article 30 de la loi du 13 juillet 1983 : " En cas de faute grave commise par un fonctionnaire, qu'il s'agisse d'un manquement à ses obligations professionnelles ou d'une infraction de droit commun, l'auteur de cette faute peut être suspendu par l'autorité ayant pouvoir disciplinaire qui saisit, sans délai, le conseil de discipline. / Le fonctionnaire suspendu conserve son traitement, l'indemnité de résidence, le supplément familial de traitement et les prestations familiales obligatoires. Sa situation doit être définitivement réglée dans le délai de quatre mois. / Si, à l'expiration d'un délai de quatre mois, aucune décision n'a été prise par l'autorité ayant le pouvoir disciplinaire, le fonctionnaire qui ne fait pas l'objet de poursuites pénales est rétabli dans ses fonctions. S'il fait l'objet de poursuites pénales et que les mesures décidées par l'autorité judicaire ou l'intérêt du service n'y font pas obstacle, il est également rétabli dans ses fonctions à l'expiration du même délai. Lorsque, sur décision motivée, il n'est pas rétabli dans ses fonctions, il peut être affecté provisoirement par l'autorité investie du pouvoir de nomination, sous réserve de l'intérêt du service, dans un emploi compatible avec les obligations du contrôle judiciaire auquel il est, le cas échéant, soumis. A défaut, il peut être détaché d'office, à titre provisoire, dans un autre corps ou cadre d'emplois pour occuper un emploi compatible avec de telles obligations. L'affectation provisoire ou le détachement provisoire prend fin lorsque la situation du fonctionnaire est définitivement réglée par l'administration ou lorsque l'évolution des poursuites pénales rend impossible sa prolongation. / (...) Le fonctionnaire qui, en raison de poursuites pénales, n'est pas rétabli dans ses fonctions, affecté provisoirement ou détaché provisoirement dans un autre emploi peut subir une retenue, qui ne peut être supérieure à la moitié de la rémunération mentionnée au deuxième alinéa. Il continue, néanmoins, à percevoir la totalité des suppléments pour charges de famille ". Aux termes du premier alinéa de l'article 8 du décret du 7 octobre 1994 fixant les dispositions communes applicables aux stagiaires de l'État et de ses établissements publics : " Le fonctionnaire stagiaire peut être suspendu dans les conditions qui sont prévues, pour les fonctionnaires titulaires, par l'article 30 de la loi du 13 juillet 1983 susvisée ". <br/>
<br/>
              4. Il résulte des dispositions citées au point précédent que si, à l'expiration d'un délai de quatre mois, aucune décision n'a été prise par l'autorité ayant le pouvoir disciplinaire à l'encontre d'un fonctionnaire suspendu, celui-ci est rétabli dans ses fonctions, sauf s'il fait l'objet de poursuites pénales. Un fonctionnaire doit pour l'application de ces dispositions être regardé comme faisant l'objet de poursuites pénales lorsque l'action publique a été mise en mouvement à son encontre et ne s'est pas éteinte. Lorsque c'est le cas, l'autorité administrative peut, au vu de la situation en cause et des conditions prévues par ces dispositions, le rétablir dans ses fonctions, lui attribuer provisoirement une autre affectation, procéder à son détachement ou encore prolonger la mesure de suspension en l'assortissant, le cas échéant, d'une retenue sur traitement.<br/>
<br/>
              5. Aux termes, de troisième part, du premier alinéa de l'article 6 du code de procédure pénale : " L'action publique pour l'application de la peine s'éteint par la mort du prévenu, la prescription, l'amnistie, l'abrogation de la loi pénale et la chose jugée ". Tel n'est pas le cas lorsqu'un jugement pénal est frappé d'appel. <br/>
<br/>
              6. En premier lieu, il résulte de ce qui a été dit aux points 3 à 5 qu'en se bornant à relever que M. B... avait été condamné par le tribunal correctionnel de Mamoudzou le 15 mai 2019, sans rechercher, pour s'assurer que ce dernier faisait encore l'objet de poursuites pénales, si ce jugement était frappé d'appel, pour juger que la créance dont il se prévalait, à raison de la décision, selon lui, illégale de prolongation de sa suspension et de retenue sur traitement, ne pouvait être regardée comme présentant un caractère non sérieusement contestable au sens de l'article R. 541-1 du code de justice administrative, le juge des référés de la cour administrative d'appel de Bordeaux a commis une erreur de droit.<br/>
<br/>
              7. Toutefois, il ressort de manière constante des pièces soumises au juge des référés qu'il avait été interjeté appel du jugement du tribunal correctionnel de Mamoudzou, de sorte que l'action publique n'était pas éteinte et que M. B... faisait toujours l'objet de poursuites pénales au sens des dispositions de l'article 30 de la loi du 13 juillet 1983 lorsque l'arrêté du 15 octobre 2019 a été pris. Ce motif, établi de manière certaine par le dossier soumis au juge des référés et au demeurant reconnu par l'intéressé, qui n'appelle l'appréciation d'aucune circonstance de fait et justifie le dispositif de l'ordonnance attaquée, doit être substitué à celui retenu par cette même ordonnance.<br/>
<br/>
              8. En deuxième lieu, le juge des référés de la cour administrative d'appel de Bordeaux ne s'est pas mépris sur la portée des écritures des parties en appel en jugeant que le mémoire en défense du recteur de l'académie de Mayotte soutenait qu'au regard de la nature des faits reprochés à M. B... et à raison desquels il avait été condamné par le tribunal correctionnel de Mamoudzou, l'intérêt du service faisait obstacle à sa réintégration dans les services de l'éducation nationale.<br/>
<br/>
              9. En dernier lieu, si les dispositions de l'article 30 de la loi du 13 juillet 1983 citées au point 3, issues de la loi du 20 avril 2016 relative à la déontologie et aux droits et obligations des fonctionnaires, permettent à l'administration d'affecter provisoirement un fonctionnaire faisant l'objet de poursuites pénales dans un autre emploi, celle-ci n'est pas tenue de le faire. Il s'ensuit qu'en jugeant que ces dispositions n'imposaient pas à l'administration d'affecter provisoirement M. B... sur un autre poste et que, dès lors, la créance dont se prévalait M. B... à l'encontre de l'Etat ne pouvait être regardée comme présentant un caractère non sérieusement contestable au sens de l'article R. 541-1 du code de justice administrative, le juge des référés de la cour administrative d'appel de Bordeaux n'a commis aucune erreur de droit.<br/>
<br/>
              10. Il résulte de tout ce qui précède que M. B... n'est pas fondé à demander l'annulation de l'ordonnance du juge des référés de la cour administrative de Bordeaux qu'il attaque.<br/>
<br/>
              11. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'État qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le pourvoi de M. B... est rejeté.<br/>
Article 2 : La présente décision sera notifiée à M. A... B... et au ministre de l'éducation nationale, de la jeunesse et des sports.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-09-01 FONCTIONNAIRES ET AGENTS PUBLICS. - DISCIPLINE. - SUSPENSION. - EXCEPTION AU RÉTABLISSEMENT D'UN FONCTIONNAIRE SUSPENDU FAISANT L'OBJET DE POURSUITES PÉNALES ET POSSIBILITÉ DANS CE CAS D'UNE RETENUE SUR TRAITEMENT (ARTICLE 30 DE LA LOI DU 13 JUILLET 1983) - 1) CONDITION TENANT À L'EXISTENCE DE POURSUITES PÉNALES - A) ACTION PUBLIQUE MISE EN MOUVEMENT [RJ1] ET NON ÉTEINTE - B) EXTINCTION DE L'ACTION PUBLIQUE PAR LA CHOSE JUGÉE (ART. 6 DU CPP) - EXCLUSION - JUGEMENT PÉNAL FRAPPÉ D'APPEL [RJ2] - 2) CASSATION - ERREUR DE DROIT À NE PAS AVOIR RECHERCHÉ, POUR VALIDER UNE RETENUE SUR LE TRAITEMENT D'UN FONCTIONNAIRE CONDAMNÉ, SI CETTE CONDAMNATION ÉTAIT FRAPPÉE D'APPEL - SUBSTITUTION DU MOTIF TIRÉ DE CE QUE TEL ÉTAIT LE CAS - EXISTENCE [RJ3], EN L'ESPÈCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-08-02-03-015 PROCÉDURE. - VOIES DE RECOURS. - CASSATION. - POUVOIRS DU JUGE DE CASSATION. - EXCEPTION AU RÉTABLISSEMENT D'UN FONCTIONNAIRE SUSPENDU FAISANT L'OBJET DE POURSUITES PÉNALES ET POSSIBILITÉ DANS CE CAS D'UNE RETENUE SUR TRAITEMENT (ART. 30 DE LA LOI DU 13 JUILLET 1983) - FONCTIONNAIRE CONDAMNÉ FAISANT L'OBJET D'UNE PROLONGATION DE SUSPENSION ET D'UNE RETENUE SUR TRAITEMENT - ERREUR DE DROIT À NE PAS AVOIR RECHERCHÉ, POUR VALIDER CETTE RETENUE, SI LA CONDAMNATION ÉTAIT FRAPPÉE D'APPEL - SUBSTITUTION DU MOTIF TIRÉ DE CE QUE TEL ÉTAIT LE CAS - EXISTENCE [RJ3], EN L'ESPÈCE.
</SCT>
<ANA ID="9A"> 36-09-01 Il résulte de l'article 30 de la loi n° 83-634 du 13 juillet 1983 que si, à l'expiration d'un délai de quatre mois, aucune décision n'a été prise par l'autorité ayant le pouvoir disciplinaire à l'encontre d'un fonctionnaire suspendu, celui-ci est rétabli dans ses fonctions, sauf s'il fait l'objet de poursuites pénales. Lorsque c'est le cas, l'autorité administrative peut, au vu de la situation en cause et des conditions prévues par ces dispositions, le rétablir dans ses fonctions, lui attribuer provisoirement une autre affectation, procéder à son détachement ou encore prolonger la mesure de suspension en l'assortissant, le cas échéant, d'une retenue sur traitement.......1) a) Un fonctionnaire doit pour l'application de ces dispositions être regardé comme faisant l'objet de poursuites pénales lorsque l'action publique a été mise en mouvement à son encontre et ne s'est pas éteinte. ......b) Si le premier alinéa de l'article 6 du code de procédure pénale (CPP) dispose que l'action publique pour l'application de la peine s'éteint par (…) la chose jugée, tel n'est pas le cas lorsqu'un jugement pénal est frappé d'appel. ......2) Commet une erreur de droit le juge des référés qui, statuant sur le fondement de l'article R. 541-1 du code de justice administrative (CJA), refuse d'accorder au fonctionnaire qui fait l'objet d'une prolongation de suspension avec retenue sur traitement une provision au titre du dommage que celui-ci estime subir à ce titre, en se bornant à relever que ce fonctionnaire a été condamné par un jugement pénal et sans rechercher, pour s'assurer que l'intéressé fait encore l'objet de poursuites pénales, si ce jugement est frappé d'appel.......Lorsqu'il ressort de manière constante des pièces soumises au juge des référés qu'il avait été interjeté appel du jugement pénal, de sorte que l'action publique n'était pas éteinte et que l'intéressé faisait toujours l'objet de poursuites pénales au sens de l'article 30 de la loi du 13 juillet 1983 lorsque la retenue sur traitement a été décidée, ce motif, établi de manière certaine par le dossier soumis au juge des référés, qui n'appelle l'appréciation par le juge de cassation d'aucune circonstance de fait et justifie le dispositif de l'ordonnance attaquée, doit être substitué à celui retenu par cette ordonnance.</ANA>
<ANA ID="9B"> 54-08-02-03-015 Il résulte de l'article 30 de la loi n° 83-634 du 13 juillet 1983 que si, à l'expiration d'un délai de quatre mois, aucune décision n'a été prise par l'autorité ayant le pouvoir disciplinaire à l'encontre d'un fonctionnaire suspendu, celui-ci est rétabli dans ses fonctions, sauf s'il fait l'objet de poursuites pénales. Lorsque c'est le cas, l'autorité administrative peut, au vu de la situation en cause et des conditions prévues par ces dispositions, le rétablir dans ses fonctions, lui attribuer provisoirement une autre affectation, procéder à son détachement ou encore prolonger la mesure de suspension en l'assortissant, le cas échéant, d'une retenue sur traitement.......Un fonctionnaire doit pour l'application de ces dispositions être regardé comme faisant l'objet de poursuites pénales lorsque l'action publique a été mise en mouvement à son encontre et ne s'est pas éteinte. ......Si le premier alinéa de l'article 6 du code de procédure pénale (CPP) dispose que l'action publique pour l'application de la peine s'éteint par (…) la chose jugée, tel n'est pas le cas lorsqu'un jugement pénal est frappé d'appel. ......Commet une erreur de droit le juge des référés qui, statuant sur le fondement de l'article R. 541-1 du code de justice administrative (CJA), refuse d'accorder au fonctionnaire qui fait l'objet d'une prolongation de suspension avec retenue sur traitement une provision au titre du dommage que celui-ci estime subir à ce titre, en se bornant à relever que ce fonctionnaire a été condamné par un jugement pénal et sans rechercher, pour s'assurer que l'intéressé fait encore l'objet de poursuites pénales, si ce jugement est frappé d'appel.......Lorsqu'il ressort de manière constante des pièces soumises au juge des référés qu'il avait été interjeté appel du jugement pénal, de sorte que l'action publique n'était pas éteinte et que l'intéressé faisait toujours l'objet de poursuites pénales au sens de l'article 30 de la loi du 13 juillet 1983 lorsque la retenue sur traitement a été décidée, ce motif, établi de manière certaine par le dossier soumis au juge des référés, qui n'appelle l'appréciation par le juge de cassation d'aucune circonstance de fait et justifie le dispositif de l'ordonnance attaquée, doit être substitué à celui retenu par cette ordonnance.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Section, 19 novembre 1993, Védrenne, n° 74235, p. 323....[RJ2] Rappr. Cass. crim., 25 février 2003, n° 02-81.638, Bull. crim. 2003, n° 51, p. 187. Comp., s'agissant d'une décision du juge répressif rendue en dernier ressort, CE, 29 mai 2009, Commune de Ligne, n° 319334, T. p. 904....[RJ3] Cf., sur la possibilité de substituer un motif ne comportant l'appréciation d'aucune circonstance de fait, CE, 3 mars 1998, Vanslembrouck, n° 171295, T. p. 1234 ; CE, 8 juillet 2002, Caisse fédérale du crédit mutuel d'Anjou, n° 212867, p. 262. Rappr., s'agissant de la faculté de substituer au motif tiré d'un constat du juge pénal celui tiré du caractère constant des mêmes faits, CE, 18 janvier 2017, M. Panizza, n° 386144, T. pp. 775-786-787.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
