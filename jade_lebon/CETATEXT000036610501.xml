<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036610501</ID>
<ANCIEN_ID>JG_L_2018_02_000000404982</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/61/05/CETATEXT000036610501.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 09/02/2018, 404982, Publié au recueil Lebon</TITRE>
<DATE_DEC>2018-02-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>404982</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Marc Pichon de Vendeuil</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier Henrard</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:404982.20180209</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 10 novembre 2016 et 24 mai 2017 au secrétariat du contentieux du Conseil d'Etat, la communauté d'agglomération Val d'Europe agglomération demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite de rejet résultant du silence gardé par le Premier ministre sur la demande du président de la communauté d'agglomération du 7 juillet 2016 tendant à l'abrogation des annexes de l'article 47.2 g) du cahier des charges de la convention passée entre l'Etat et la Société des Autoroutes du Nord et de l'Est de la France pour la concession de la construction, de l'entretien et de l'exploitation d'autoroutes introduit par l'avenant n° 12 approuvé par décret n° 2015-1046 du 21 août 2015 approuvant des avenants aux conventions passées, d'une part, entre l'Etat et la Société des Autoroutes du Nord et de l'Est de la France et, d'autre part, entre l'Etat et la Société des autoroutes Paris-Normandie pour la concession de la construction, de l'entretien et de l'exploitation d'autoroutes et aux cahiers des charges annexés à ces conventions ;<br/>
<br/>
              2°) d'enjoindre au Premier ministre d'abroger les annexes de l'article 47.2 g) du cahier des charges ;<br/>
<br/>
              3°) de prononcer une astreinte de 10 000 euros par jour de retard à compter de la date fixée par la décision à intervenir ;<br/>
<br/>
              4°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la voirie routière ;<br/>
              - le décret n° 2015-1046 du 21 août 2015 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Marc Pichon de Vendeuil, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Olivier Henrard, rapporteur public.<br/>
<br/>
              Vu la note en délibéré, enregistrée le 24 janvier 2018, présentée par la communauté d'agglomération Val d'Europe agglomération.<br/>
<br/>
<br/>
<br/>1. Considérant que, par un courrier en date du 7 juillet 2016, le président de la communauté d'agglomération Val d'Europe agglomération a demandé au Premier ministre d'abroger les annexes de l'article 47.2 g) du cahier des charges de la convention, approuvée par décret du 29 octobre 1990, passée entre l'Etat et la Société des Autoroutes du Nord et de l'Est de la France (SANEF) pour la concession de la construction, de l'entretien et de l'exploitation d'autoroutes, au motif que ces annexes ne prévoient pas la réalisation du barreau de liaison entre l'autoroute A4 et la RN 36 déclarée d'utilité publique par un arrêté préfectoral du 27 juillet 2012 ; que les dispositions de ces annexes, relatives à la reconfiguration de l'échangeur autoroutier de Bailly-Romainvilliers, ont été introduites par le douzième avenant à cette convention, lui-même approuvé par décret du 21 août 2015 ; que la présente requête tend à l'annulation pour excès de pouvoir du refus implicite opposé par le Premier ministre à cette demande ;<br/>
<br/>
              2. Considérant qu'indépendamment du recours de pleine juridiction dont disposent les tiers à un contrat administratif pour en contester la validité, un tiers à un contrat est recevable à demander, par la voie du recours pour excès de pouvoir, l'annulation des clauses réglementaires contenues dans un contrat administratif qui portent une atteinte directe et certaine à ses intérêts ; qu'il est également recevable à demander, par la même voie, l'annulation du refus d'abroger de telles clauses à raison de leur illégalité ; que, d'autre part, il appartient à toute personne y ayant intérêt de contester par la voie de l'excès de pouvoir une décision autorisant l'exécution de travaux autoroutiers ;<br/>
<br/>
              3. Considérant, d'une part, que revêtent un caractère réglementaire les clauses d'un contrat qui ont, par elles-mêmes, pour objet l'organisation ou le fonctionnement d'un service public ; que, s'agissant d'une convention de concession autoroutière, relèvent notamment de cette catégorie les clauses qui définissent l'objet de la concession et les règles de desserte, ainsi que celles qui définissent les conditions d'utilisation des ouvrages et fixent les tarifs des péages applicables sur le réseau concédé ; qu'en revanche, les stipulations relatives notamment au régime financier de la concession ou à la réalisation des ouvrages, qu'il s'agisse de leurs caractéristiques, de leur tracé, ou des modalités de cette réalisation, sont dépourvues de caractère réglementaire et revêtent un caractère purement contractuel ;<br/>
<br/>
              4. Considérant que les stipulations contestées des annexes de l'article 47.2 g) du cahier des charges de la convention de concession autoroutière en cause, qui portent sur la reconfiguration de l'échangeur autoroutier de Bailly-Romainvilliers et déterminent les conditions de réalisation d'un aménagement complémentaire à cet échangeur, et sont ainsi relatives à la réalisation d'ouvrages, ne présentent pas un caractère réglementaire ; que, dès lors, les conclusions tendant à l'annulation pour excès de pouvoir du refus d'abroger ces dispositions sont irrecevables ;<br/>
<br/>
              5. Considérant, d'autre part, que si les stipulations de l'avenant à la convention de concession litigieuse révèlent, au plus tard à la date à laquelle cet avenant a été approuvé par le décret du 21 août 2015, la décision par laquelle la ministre de l'écologie, du développement durable et de l'énergie a autorisé la SANEF à exécuter les travaux nécessaires à la réalisation d'une bretelle de sortie de l'autoroute A4 au niveau de l'échangeur n° 14, une telle mesure, qui ne constitue pas une décision réglementaire et ne présente pas davantage le caractère d'une décision administrative individuelle, ne pouvait être contestée par la voie contentieuse au-delà du délai de recours de droit commun de deux mois à compter de sa publication ; que le décret approuvant l'avenant litigieux a été publié au Journal officiel le 23 août 2015 ; que dès lors, si la présente requête, qui est dirigée contre le rejet résultant du silence gardé par le Premier ministre sur la demande que lui a adressée le président de la communauté d'agglomération le 7 juillet 2016, tend également à contester la décision ministérielle d'autorisation de travaux, de telles conclusions ne peuvent qu'être rejetées comme tardives ;<br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède que la requête de la communauté d'agglomération Val d'Europe agglomération doit être rejetée, y compris ses conclusions à fin d'injonction et celles présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la communauté d'agglomération Val d'Europe agglomération est rejetée.<br/>
Article 2 : La présente décision sera notifiée à la communauté d'agglomération Val d'Europe agglomération, au ministre d'Etat, ministre de la transition écologique et solidaire et à la Société des Autoroutes du Nord et de l'Est de la France.<br/>
Copie en sera transmise au Premier ministre et à la Société des autoroutes Paris-Normandie.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-01-03-03-01 MARCHÉS ET CONTRATS ADMINISTRATIFS. NOTION DE CONTRAT ADMINISTRATIF. DIVERSES SORTES DE CONTRATS. DÉLÉGATIONS DE SERVICE PUBLIC. CONCESSION DE SERVICE PUBLIC. - CONCESSIONS AUTOROUTIÈRES - CLAUSES RÉGLEMENTAIRES - CLAUSES DÉFINISSANT L'OBJET DE LA CONCESSION, LES RÈGLES DE DESSERTE, LES CONDITIONS D'UTILISATION DES OUVRAGES, ET LES TARIFS DES PÉAGES APPLICABLES SUR LE RÉSEAU CONCÉDÉ - INCLUSION - CLAUSES RELATIVES AU RÉGIME FINANCIER OU À LA RÉALISATION DES OUVRAGES - EXCLUSION - CONSÉQUENCE - IRRECEVABILITÉ D'UN RECOURS POUR EXCÈS DE POUVOIR DIRIGÉ CONTRE LE REFUS D'ABROGER DES STIPULATIONS CONTRACTUELLES PORTANT SUR LA RECONFIGURATION D'UN ÉCHANGEUR AUTOROUTIER ET DÉTERMINANT LES CONDITIONS DE RÉALISATION D'UN AMÉNAGEMENT COMPLÉMENTAIRE À CET ÉCHANGEUR [RJ1][RJ2].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">39-08-01-01 MARCHÉS ET CONTRATS ADMINISTRATIFS. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. RECEVABILITÉ. RECEVABILITÉ DU RECOURS POUR EXCÈS DE POUVOIR EN MATIÈRE CONTRACTUELLE. - RECOURS PRÉSENTÉ PAR LES TIERS - CONCLUSIONS TENDANT À L'ANNULATION DES CLAUSES RÉGLEMENTAIRES D'UN CONTRAT ADMINISTRATIF QUI PORTENT UNE ATTEINTE DIRECTE ET CERTAINE À LEURS INTÉRÊTS - EXISTENCE [RJ1] - CONCLUSIONS TENDANT À L'ANNULATION DU REFUS D'ABROGER DE TELLES CLAUSES À RAISON DE LEUR ILLÉGALITÉ - EXISTENCE [RJ2].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">71-01-02-02 VOIRIE. COMPOSITION ET CONSISTANCE. VOIES NATIONALES. - CONCESSIONS AUTOROUTIÈRES - CLAUSES RÉGLEMENTAIRES - CLAUSES DÉFINISSANT L'OBJET DE LA CONCESSION, LES RÈGLES DE DESSERTE, LES CONDITIONS D'UTILISATION DES OUVRAGES OU LES TARIFS DES PÉAGES APPLICABLES SUR LE RÉSEAU CONCÉDÉ - INCLUSION - CLAUSES RELATIVES AU RÉGIME FINANCIER OU À LA RÉALISATION DES OUVRAGES - EXCLUSION - CONSÉQUENCE - IRRECEVABILITÉ D'UN RECOURS POUR EXCÈS DE POUVOIR DIRIGÉ CONTRE LE REFUS D'ABROGER DES STIPULATIONS CONTRACTUELLES PORTANT SUR LA RECONFIGURATION D'UN ÉCHANGEUR AUTOROUTIER ET DÉTERMINANT LES CONDITIONS DE RÉALISATION D'UN AMÉNAGEMENT COMPLÉMENTAIRE À CET ÉCHANGEUR [RJ1][RJ2].
</SCT>
<ANA ID="9A"> 39-01-03-03-01 Revêtent un caractère réglementaire les clauses d'un contrat qui ont, par elles-mêmes, pour objet l'organisation ou le fonctionnement d'un service public. S'agissant d'une convention de concession autoroutière, relèvent notamment de cette catégorie les clauses qui définissent l'objet de la concession et les règles de desserte, ainsi que celles qui définissent les conditions d'utilisation des ouvrages et fixent les tarifs des péages applicables sur le réseau concédé. En revanche, les stipulations relatives notamment au régime financier de la concession ou à la réalisation des ouvrages, qu'il s'agisse de leurs caractéristiques, de leur tracé, ou des modalités de cette réalisation, sont dépourvues de caractère réglementaire et revêtent un caractère purement contractuel. Par suite, irrecevabilité des conclusions tendant à l'annulation pour excès de pouvoir du refus d'abroger des stipulations contractuelles portant sur la reconfiguration d'un échangeur autoroutier et déterminant les conditions de réalisation d'un aménagement complémentaire à cet échangeur.</ANA>
<ANA ID="9B"> 39-08-01-01 Indépendamment du recours de pleine juridiction [RJ3] dont disposent les tiers à un contrat administratif pour en contester la validité, un tiers à un contrat est recevable à demander, par la voie du recours pour excès de pouvoir, l'annulation des clauses réglementaires contenues dans un contrat administratif qui portent une atteinte directe et certaine à ses intérêts. Il est également recevable à demander, par la même voie, l'annulation du refus d'abroger de telles clauses à raison de leur illégalité.</ANA>
<ANA ID="9C"> 71-01-02-02 Revêtent un caractère réglementaire les clauses d'un contrat qui ont, par elles-mêmes, pour objet l'organisation ou le fonctionnement d'un service public. S'agissant d'une convention de concession autoroutière, relèvent notamment de cette catégorie les clauses qui définissent l'objet de la concession et les règles de desserte, ainsi que celles qui définissent les conditions d'utilisation des ouvrages et fixent les tarifs des péages applicables sur le réseau concédé. En revanche, les stipulations relatives notamment au régime financier de la concession ou à la réalisation des ouvrages, qu'il s'agisse de leurs caractéristiques, de leur tracé, ou des modalités de cette réalisation, sont dépourvues de caractère réglementaire et revêtent un caractère purement contractuel. Par suite, irrecevabilité des conclusions tendant à l'annulation pour excès de pouvoir du refus d'abroger des stipulations contractuelles portant sur la reconfiguration d'un échangeur autoroutier et déterminant les conditions de réalisation d'un aménagement complémentaire à cet échangeur.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., en précisant, CE, Assemblée, 10 juillet 1996,,n° 138536, p. 274.,,[RJ2] Cf., s'agissant de l'obligation pour l'administration d'abroger un règlement illégal, CE, Assemblée, 3 février 1989, Compagnie Alitalia, n° 74052, p. 44.,,[RJ3] Cf. CE, Assemblée, 4 avril 2014, Département de Tarn-et-Garonne, n° 358994, p. 70.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
