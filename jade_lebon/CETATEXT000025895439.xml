<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025895439</ID>
<ANCIEN_ID>JG_L_2012_05_000000331346</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/89/54/CETATEXT000025895439.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Section du Contentieux, 16/05/2012, 331346, Publié au recueil Lebon</TITRE>
<DATE_DEC>2012-05-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>331346</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Section du Contentieux</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARTHELEMY, MATUCHANSKY, VEXLIARD ; SCP DEFRENOIS, LEVIS</AVOCATS>
<RAPPORTEUR>M. Raphaël Chambon</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Cyril Roger-Lacan</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESEC:2012:331346.20120516</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 31 août et 30 novembre 2009 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. Jean-François A, demeurant ... ; M. A demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision du 25 juin 2009 par laquelle le Haut Conseil du commissariat aux comptes a confirmé la décision du 6 février 2008 de la chambre régionale de discipline des commissaires aux comptes du ressort de la cour d'appel de Paris ayant déclaré irrecevable son recours en révision de la décision rendue le 1er décembre 2004 par la même chambre régionale ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu le code de commerce ;	<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Raphaël Chambon, Auditeur ;<br/>
<br/>
              - les observations de la SCP Barthélemy, Matuchansky, Vexliard, avocat de M. Jean-François A et de la SCP Defrenois, Levis, avocat de M. Jean-Louis Vorburger ;<br/>
<br/>
              - les conclusions de M. Cyril Roger-Lacan, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Barthélemy, Matuchansky, Vexliard, avocat de M. Jean-François A et à la SCP Defrenois, Levis, avocat de M. Jean-Louis Vorburger ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant que, par une décision rendue le 1er décembre 2004, la chambre régionale de discipline des commissaires aux comptes du ressort de la cour d'appel de Paris a infligé une sanction disciplinaire à M. A ; que ce dernier a déposé un recours en révision de cette décision devant la chambre régionale ; que, par une décision du 6 février 2008, cette chambre régionale a déclaré son recours en révision irrecevable, au motif qu'un tel recours n'est ouvert que s'il est expressément prévu par un texte et qu'il n'en existe aucun en la matière ; que, saisi en appel, le Haut Conseil du commissariat aux comptes a confirmé la décision de la chambre régionale, par une décision du 25 juin 2009, au motif que l'ouverture d'un recours en révision est subordonnée à l'existence d'un texte prévoyant et organisant l'exercice de cette voie de recours extraordinaire et qu'aucun recours de cette nature n'est prévu par les textes applicables en matière de discipline des commissaires aux comptes ; que M. A se pourvoit en cassation contre cette décision ;<br/>
<br/>
              Sans qu'il soit besoin d'examiner les autres moyens du pourvoi ;<br/>
<br/>
              Considérant qu'en vertu des dispositions des 1° et 2° de l'article R. 834-1 du code de justice administrative, le recours en révision n'est ouvert, lorsqu'une décision juridictionnelle a été rendue sur pièces fausses ou qu'une partie a été condamnée faute d'avoir produit une pièce décisive qui était retenue par son adversaire, qu'à l'égard des décisions du Conseil d'Etat ; que cette voie particulière de recours ne saurait, en l'absence de texte l'ayant prévue, être étendue aux autres juridictions régies par ce code ; que, s'agissant en revanche des juridictions administratives qui n'en relèvent pas et pour lesquelles aucun texte n'a prévu l'existence d'une telle voie de recours, un tel recours peut être formé, en vertu d'une règle générale de procédure découlant des exigences de la bonne administration de la justice, à l'égard d'une décision passée en force de chose jugée, dans l'hypothèse où cette décision l'a été sur pièces fausses ou si elle l'a été faute pour la partie perdante d'avoir produit une pièce décisive qui était retenue par son adversaire ; que cette possibilité est ouverte à toute partie à l'instance, dans un délai de deux mois courant à compter du jour où la partie a eu connaissance de la cause de révision qu'elle invoque ;<br/>
<br/>
              Considérant, par suite, qu'en confirmant, au motif de l'absence de texte organisant cette voie de recours extraordinaire, la décision par laquelle la chambre régionale de discipline des commissaires aux comptes du ressort de la cour d'appel de Paris a déclaré irrecevable le recours en révision formé par M. A contre sa décision du 1er décembre 2004, sans réserver l'existence des deux cas d'ouverture d'un recours en révision existant même sans texte devant les juridictions administratives ne relevant pas du code de justice administrative ni rechercher si la cause de révision invoquée par M. A pouvait se rattacher à l'un de ces cas d'ouverture, le Haut Conseil du commissariat aux comptes a entaché sa décision d'une erreur de droit ; que, dès lors, M. A est fondé à demander l'annulation de la décision du Haut Conseil du commissariat aux comptes du 25 juin 2009 ;<br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 4 000 euros à verser à M. A, au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
      --------------<br/>
Article 1er : La décision du 25 juin 2009 du Haut Conseil du commissariat aux comptes est annulée.<br/>
Article 2 : L'affaire est renvoyée au Haut Conseil du commissariat aux comptes.<br/>
Article 3 : L'Etat versera à M. A une somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à M. A et au garde des sceaux, ministre de la justice et des libertés.<br/>
Copie en sera adressée pour information au Haut Conseil du commissariat aux comptes.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-08-06 PROCÉDURE. VOIES DE RECOURS. RECOURS EN RÉVISION. - ABSENCE DE TEXTE PRÉVOYANT CETTE VOIE DE RECOURS - 1) OUVERTURE - A) DEVANT LES JURIDICTIONS ADMINISTRATIVES RÉGIES PAR LE CJA - ABSENCE - B) DEVANT LES AUTRES JURIDICTIONS ADMINISTRATIVES - EXISTENCE - 2) CAS D'OUVERTURE - 3) MODALITÉS D'EXERCICE.
</SCT>
<ANA ID="9A"> 54-08-06 1) a) La voie du recours en révision, qui n'est ouverte qu'à l'égard des décisions du Conseil d'Etat par les dispositions de l'article R. 834-1 du code de justice administrative (CJA), ne saurait, en l'absence de texte l'ayant prévue, être étendue aux autres juridictions régies par ce code. b) En revanche, en vertu d'une règle générale de procédure découlant des exigences de la bonne administration de la justice, la voie du recours en révision est ouverte devant les juridictions administratives ne relevant pas du CJA pour lesquelles aucun texte n'en a prévu l'existence.... ...2) En l'absence de texte, le recours en révision peut être formé à l'égard d'une décision passée en force de chose jugée, dans l'hypothèse où cette décision l'a été sur pièces fausses ou faute pour la partie perdante d'avoir produit une pièce décisive qui était retenue par son adversaire.,,3) Cette voie de recours est ouverte, en l'absence de texte, à toute partie à l'instance dans un délai de deux mois courant à compter du jour où la partie a eu connaissance de la cause de révision qu'elle invoque.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
