<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041675191</ID>
<ANCIEN_ID>JG_L_2020_03_000000418219</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/67/51/CETATEXT000041675191.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 1ère chambres réunies, 02/03/2020, 418219</TITRE>
<DATE_DEC>2020-03-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>418219</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR>Mme Françoise Tomé</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:418219.20200302</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. C... A... a demandé au tribunal administratif de Lyon d'annuler pour excès de pouvoir la décision du 20 octobre 2011 par laquelle le Conseil national de l'ordre des médecins a annulé la décision du 19 avril 2011 du conseil départemental de l'Ain de l'ordre des médecins l'autorisant à exercer la cardiologie interventionnelle au centre hospitalier de Bourg-en-Bresse, à raison d'une demi-journée par semaine. Par un jugement n° 1200982 du 9 juin 2015, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un arrêt n°15LY02815 du 19 décembre 2017, la cour administrative d'appel de Lyon, sur appel de M. A..., a annulé ce jugement et la décision du Conseil national de l'ordre des médecins du 20 octobre 2011.<br/>
<br/>
              Par une requête sommaire et un mémoire complémentaire, enregistrés les 15 février et 15 mai 2018 au secrétariat du contentieux du Conseil d'Etat, le Conseil national de l'ordre des médecins demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de M. A... la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la santé publique ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Françoise Tomé, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Matuchansky, Poupot, Valdelièvre, avocat du Conseil national de l'ordre des médecins ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. A..., médecin spécialiste, qualifié en pathologie cardio-vasculaire, qui exerce, à titre libéral, à Caluire-et-Cuire (Rhône), a constitué, le 15 novembre 2010, un groupement de coopération sanitaire de moyens avec le centre hospitalier de Bourg-en-Bresse (Ain), dans le cadre duquel il est appelé à pratiquer, à raison d'une demi-journée par semaine, la cardiologie interventionnelle au centre hospitalier. Des médecins, membres d'un groupement de coopération sanitaire concurrent, ont formé devant le Conseil national de l'ordre des médecins, un recours contre la décision du conseil départemental de l'Ain de l'ordre des médecins autorisant M. A... à exercer une telle activité en sus de son activité libérale. Le Conseil national de l'ordre des médecins ayant annulé la décision du conseil départemental et refusé d'autoriser M. A... à exercer ces fonctions, M. A... a déféré sa décision au tribunal administratif de Lyon. Le Conseil national de l'ordre des médecins se pourvoit en cassation contre l'arrêt, en date du 19 décembre 2017, par lequel la cour administrative d'appel de Lyon, sur appel de M. A..., a annulé le jugement du tribunal administratif et la décision du Conseil national de l'ordre des médecins.<br/>
<br/>
              2. D'une part, aux termes de l'articles L. 6133-1 du code de la santé publique, dans sa rédaction alors applicable : " Le groupement de coopération sanitaire de moyens a pour objet de faciliter, de développer ou d'améliorer l'activité de ses membres./ Un groupement de coopération sanitaire de moyens peut être constitué pour : / (...) 3° Permettre les interventions communes de professionnels médicaux et non médicaux exerçant dans les établissements ou centres de santé membres du groupement ainsi que des professionnels libéraux membres du groupement. / Ce groupement poursuit un but non lucratif ". Aux termes de l'article L. 6133-6 du même code : " Dans le cas prévu au 3° de l'article L. 6133-1, les professionnels médicaux des établissements de santé membres du groupement, les professionnels médicaux des centres de santé membres du groupement et les professionnels médicaux libéraux membres du groupement peuvent assurer des prestations médicales au bénéfice des patients pris en charge par l'un ou l'autre des établissements de santé membres du groupement et participer à la permanence des soins (...) ". Aux termes de l'article L. 6133-3 du même code, la convention constitutive de ce groupement " est soumise à l'approbation du directeur général de l'agence régionale de santé, qui en assure la publication (...) ". Enfin, l'article R. 6133-1 du même code prévoit que la convention constitutive du groupement mentionne, notamment, " l'objet du groupement et la répartition des activités entre le groupement et ses membres ", " l'identité de ses membres et leur qualité " et les " conditions d'intervention des professionnels médicaux libéraux et des personnels médicaux et non médicaux des établissements ou centres de santé membres (...) ". <br/>
<br/>
              3. D'autre part, s'agissant des médecins qui exercent " en clientèle privée ", l'article R. 4127-85 du code de la santé publique, dans sa version alors applicable, disposait que : " Le lieu habituel d'exercice d'un médecin est celui de la résidence professionnelle au titre de laquelle il est inscrit sur le tableau du conseil départemental, conformément à l'article L. 4112-1. / Dans l'intérêt de la population, un médecin peut exercer son activité professionnelle sur un ou plusieurs sites distincts de sa résidence professionnelle habituelle : / - lorsqu'il existe dans le secteur géographique considéré une carence ou une insuffisance de l'offre de soins préjudiciable aux besoins des patients ou à la permanence des soins ; / - ou lorsque les investigations et les soins qu'il entreprend nécessitent un environnement adapté, l'utilisation d'équipements particuliers, la mise en oeuvre de techniques spécifiques ou la coordination de différents intervenants. / (...) /La demande d'ouverture d'un lieu d'exercice distinct est adressée au conseil départemental dans le ressort duquel se situe l'activité envisagée. (...) / Le silence gardé par le conseil départemental sollicité vaut autorisation implicite à l'expiration d'un délai de trois mois à compter de la date de réception de la demande (...). / L'autorisation est personnelle et incessible (...) ".<br/>
<br/>
              4. Il résulte des dispositions citées au point 2 qu'il revient au seul directeur général de l'agence régionale de santé d'approuver la convention constitutive d'un groupement de coopération sanitaire de moyens entre un établissement de santé et un professionnel de santé libéral, laquelle précise notamment l'identité de ses membres ainsi que les conditions d'intervention des professionnels médicaux libéraux. Il s'ensuit que l'activité exercée dans le cadre d'un tel groupement par un médecin libéral qui en est membre n'entre pas dans le champ d'application des dispositions de l'article R. 4127-85 du code de la santé publique, qui prévoient que l'ouverture, par un médecin libéral, d'un site d'exercice distinct de celui de sa résidence professionnelle habituelle, est subordonnée à l'autorisation préalable de l'instance ordinale. Dès lors, c'est sans erreur de droit que la cour administrative d'appel de Lyon a jugé que l'activité de M. A... dans le cadre du groupement de coopération sanitaire qu'il avait constitué avec le centre hospitalier de Bourg-en-Bresse n'avait pas à être autorisée sur le fondement de l'article R. 4127-85 du code de la santé publique. <br/>
<br/>
              5. Il résulte de tout ce qui précède que le Conseil national de l'ordre des médecins n'est pas fondé à demander l'annulation de l'arrêt du 19 décembre 2017 de la cour administrative d'appel de Lyon qu'il attaque. Les dispositions de l'article L. 761-1 du code de justice administrative font, par suite, obstacle à ce qu'une somme soit mise, à ce titre, à la charge de M. A... qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi du Conseil national de l'ordre des médecins est rejeté. <br/>
Article 2 : La présente décision sera notifiée au Conseil national de l'ordre des médecins et à M. B... A....<br/>
Copie en sera adressée à la ministre des solidarités et de la santé.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">55-01-02-01-03 PROFESSIONS, CHARGES ET OFFICES. ORDRES PROFESSIONNELS - ORGANISATION ET ATTRIBUTIONS NON DISCIPLINAIRES. QUESTIONS PROPRES À CHAQUE ORDRE PROFESSIONNEL. ORDRE DES MÉDECINS. CONSEILS DÉPARTEMENTAUX. - COMPÉTENCE - AUTORISATION D'EXERCER DANS UN GCS CRÉÉ AVEC UN ÉTABLISSEMENT DE SANTÉ - ABSENCE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">55-03-01-01 PROFESSIONS, CHARGES ET OFFICES. CONDITIONS D'EXERCICE DES PROFESSIONS. MÉDECINS. CABINET MÉDICAL. - CRÉATION D'UN GCS AVEC UN ÉTABLISSEMENT DE SANTÉ - APPROBATION PAR LE DIRECTEUR GÉNÉRAL DE L'ARS - EXISTENCE - AUTORISATION, PAR LE CONSEIL DE L'ORDRE, DE LA PARTICIPATION DU MÉDECIN LIBÉRAL À CE GCS - ABSENCE [RJ1].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">61-06-04 SANTÉ PUBLIQUE. ÉTABLISSEMENTS PUBLICS DE SANTÉ. RÉGIME DES CLINIQUES OUVERTES ET DES GROUPEMENTS DE COOPÉRATION SANITAIRE (GCS). - CRÉATION D'UN GCS AVEC UN PROFESSIONNEL LIBÉRAL - APPROBATION PAR LE DIRECTEUR GÉNÉRAL DE L'ARS - EXISTENCE - AUTORISATION, PAR LE CONSEIL DE L'ORDRE, DE LA PARTICIPATION DU MÉDECIN LIBÉRAL À CE GCS - ABSENCE [RJ1].
</SCT>
<ANA ID="9A"> 55-01-02-01-03 Il résulte des articles L. 6133-1, L. 6133-3, L. 6133-6 et R. 6133-1 du code de la santé publique (CSP) qu'il revient au seul directeur général de l'agence régionale de santé (ARS) d'approuver la convention constitutive d'un groupement de coopération sanitaire (GCS) de moyens entre un établissement de santé et un professionnel de santé libéral, laquelle précise notamment l'identité de ses membres ainsi que les conditions d'intervention des professionnels médicaux libéraux. Il s'ensuit que l'activité exercée dans le cadre d'un tel groupement par un médecin libéral qui en est membre n'entre pas dans le champ d'application de l'article R. 4127-85 du CSP, qui prévoit que l'ouverture, par un médecin libéral, d'un site d'exercice distinct de celui de sa résidence professionnelle habituelle, est subordonnée à l'autorisation préalable de l'instance ordinale.</ANA>
<ANA ID="9B"> 55-03-01-01 Il résulte des articles L. 6133-1, L. 6133-3, L. 6133-6 et R. 6133-1 du code de la santé publique (CSP) qu'il revient au seul directeur général de l'agence régionale de santé (ARS) d'approuver la convention constitutive d'un groupement de coopération sanitaire (GCS) de moyens entre un établissement de santé et un professionnel de santé libéral, laquelle précise notamment l'identité de ses membres ainsi que les conditions d'intervention des professionnels médicaux libéraux. Il s'ensuit que l'activité exercée dans le cadre d'un tel groupement par un médecin libéral qui en est membre n'entre pas dans le champ d'application de l'article R. 4127-85 du CSP, qui prévoit que l'ouverture, par un médecin libéral, d'un site d'exercice distinct de celui de sa résidence professionnelle habituelle, est subordonnée à l'autorisation préalable de l'instance ordinale.</ANA>
<ANA ID="9C"> 61-06-04 Il résulte des articles L. 6133-1, L. 6133-3, L. 6133-6 et R. 6133-1 du code de la santé publique (CSP) qu'il revient au seul directeur général de l'agence régionale de santé (ARS) d'approuver la convention constitutive d'un groupement de coopération sanitaire (GCS) de moyens entre un établissement de santé et un professionnel de santé libéral, laquelle précise notamment l'identité de ses membres ainsi que les conditions d'intervention des professionnels médicaux libéraux. Il s'ensuit que l'activité exercée dans le cadre d'un tel groupement par un médecin libéral qui en est membre n'entre pas dans le champ d'application de l'article R. 4127-85 du CSP, qui prévoit que l'ouverture, par un médecin libéral, d'un site d'exercice distinct de celui de sa résidence professionnelle habituelle, est subordonnée à l'autorisation préalable de l'instance ordinale.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., sous l'empire du droit antérieur, CE, Section, 3 mai 1982, Conseil départemental de l'Essonne de l'Ordre des médecins, n° 12345, p. 167 ; CE, 12 mars 1999, Conseil départemental de l'ordre des médecins de Haute-Savoie, n° 188619, T. pp. 994-997-1029.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
