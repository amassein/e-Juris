<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039627792</ID>
<ANCIEN_ID>JG_L_2019_12_000000422672</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/62/77/CETATEXT000039627792.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 4ème chambres réunies, 16/12/2019, 422672</TITRE>
<DATE_DEC>2019-12-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>422672</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, SOLTNER, TEXIDOR, PERIER</AVOCATS>
<RAPPORTEUR>Mme Bénédicte Fauvarque-Cosson</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:422672.20191216</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire, un mémoire complémentaire et deux mémoires en réplique, enregistrés les 27 juillet et 29 octobre 2018 et 12 mars et 27 juin 2019 au secrétariat du contentieux du Conseil d'Etat, la Fédération des centres mémoire, la Fédération française de neurologie, la Société française de gériatrie et de gérontologie, la Société de neuropsychologie de langue française, la Société francophone de psychogériatrie et de psychiatrie de la personne âgée, l'Union nationale des associations France Alzheimer, l'Association des neurologues libéraux de langue française et la Société française de neurologie demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté de la ministre des solidarités et de la santé et du ministre de l'action et des comptes publics du 29 mai 2018 portant radiation de spécialités pharmaceutiques de la liste mentionnée au premier alinéa de l'article L. 162-17 du code de la sécurité sociale ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la santé publique ;<br/>
              - le code de la sécurité sociale ; <br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - la loi n° 2017-55 du 20 janvier 2017 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Bénédicte Fauvarque-Cosson, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public :<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Soltner, Texidor, Perier, avocat de la Fédération des centres mémoire, de la Fédération française de neurologie, de la Société française de gériatrie et de gérontologie, de la Société de neuropsychologie de langue française, de la Société francophone de psychogériatrie et de psychiatrie de la personne âgée, de l'Union nationale des associations France Alzheimer, de l'Association des neurologues libéraux de langue française et de la Société française de neurologie ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 28 novembre 2019, présentée par la Fédération des centres mémoire, la Fédération française de neurologie, la Société française de gériatrie et de gérontologie, la Société de neuropsychologie de langue française, la Société francophone de psychogériatrie et de psychiatrie de la personne âgée, l'Union nationale des associations France Alzheimer, l'Association des neurologues libéraux de langue française et la Société française de neurologie ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. En vertu de l'article L. 162-17 du code de la sécurité sociale, les spécialités pharmaceutiques ne peuvent être prises en charge ou donner lieu à remboursement par les caisses d'assurance maladie, lorsqu'elles sont dispensées en officine, que si elles figurent sur une liste établie, selon les articles R. 163-2 et R. 163-4 du même code, par arrêté conjoint du ministre chargé de la santé et du ministre chargé de la sécurité sociale, pris après avis de la commission de la transparence de la Haute Autorité de santé. Il résulte des dispositions de l'article R. 163-3 et du I de l'article R. 163-7 du même code que peuvent être radiés de cette liste, par arrêté de ces ministres pris après avis de la commission de la transparence, les médicaments qui ne peuvent plus y figurer parce que leur " service médical rendu est insuffisant au regard des autres médicaments ou thérapies disponibles ". <br/>
<br/>
              2. Sur le fondement de ces dispositions, par un arrêté du 29 mai 2018, pris après des avis rendus par la commission de la transparence de la Haute Autorité de santé les 6 juillet et 19 octobre 2016 et le 21 mars 2018 qu'ils ont décidé de suivre, la ministre des solidarités et de la santé et le ministre de l'action et des comptes publics ont radié de la liste des médicaments remboursables aux assurés sociaux mentionnée à l'article L. 162-17 du code de la sécurité sociale les spécialités pharmaceutiques Aricept, Exelon, Reminyl et Ebixa et leurs génériques, à base respectivement de donépézil, de rivastigmine, de galantamine et de mémantine, indiquées dans le traitement de la maladie d'Alzheimer dans ses formes, pour les trois premières, légères à modérément sévères et, pour la dernière, modérée à sévère, ainsi que, pour Exelon, dans celui des formes légères à modérément sévères d'une démence chez les patients atteints de la maladie de Parkinson idiopathique, au motif que ces spécialités, dont le taux de remboursement avait déjà été réduit à 15 % en 2012, présentaient un service médical rendu insuffisant. La Fédération des centres mémoire, la Fédération française de neurologie, la Société française de gériatrie et de gérontologie, la Société de neuropsychologie de langue française, la Société francophone de psychogériatrie et de psychiatrie de la personne âgée, l'Union nationale des associations France Alzheimer, l'Association des neurologues libéraux de langue Française et la Société française de neurologie demandent l'annulation pour excès de pouvoir de cet arrêté.<br/>
<br/>
              Sur la régularité des avis de la commission de la transparence : <br/>
<br/>
              3. L'article R. 161-85 du code de la sécurité sociale prévoit que les membres de la commission de la transparence de la Haute Autorité de santé et les personnes qui lui apportent leur concours ne peuvent traiter une question dans laquelle ils auraient un intérêt direct ou indirect. En application des dispositions combinées de cet article et de l'article L. 1451-1 du code de la santé publique, ils ne peuvent prendre part ni aux travaux ni aux délibérations ni aux votes au sein de la commission de la transparence s'ils ont un intérêt direct ou indirect à l'affaire examinée. Ils sont, en outre, tenus au respect du secret professionnel et doivent faire preuve de discrétion professionnelle s'agissant des faits, informations ou documents dont ils ont connaissance dans l'exercice de leurs fonctions, dans les mêmes conditions que celles qui sont définies, pour les fonctionnaires, à l'article 26 de la loi du 13 juillet 1983 portant droits et obligations des fonctionnaires. Enfin, il incombe aux membres de la commission de la transparence de s'abstenir de toute prise de position publique qui serait de nature à compromettre le respect du principe d'impartialité.<br/>
<br/>
              4. Les requérantes font valoir que l'un des membres de la commission de la transparence, rapporteur du dossier, et deux des experts extérieurs sollicités par la commission ont pris publiquement position en faveur du déremboursement des spécialités considérées, ce qui entacherait d'irrégularité les avis rendus par la commission les 6 juillet et 19 octobre 2016. Il ne ressort toutefois pas des pièces du dossier que les personnes ainsi mises en cause auraient été mues par un intérêt personnel ou qu'elles auraient eu des liens avec une entreprise intéressée aux résultats de l'examen des spécialités en litige auquel la commission de la transparence devait procéder les 6 juillet et 19 octobre 2016, leur conférant un intérêt à la question de leur prise en charge par l'assurance maladie. Il ne ressort pas davantage des pièces du dossier que les articles publiés dans la presse médicale en 2010 et 2016 par l'un des médecins experts auraient fait obstacle à ce qu'il puisse être sollicité par la commission comme expert extérieur. Par ailleurs, l'entretien accordé en 2011, plusieurs années avant les avis considérés, par le médecin rapporteur du dossier, non plus que l'article paru dans la presse généraliste en 2015 exposant ses positions sur les médicaments indiqués dans le traitement de la maladie d'Alzheimer ne s'opposaient à ce qu'il prenne part, dans le respect du principe d'impartialité qui s'impose à tous les organes administratifs, à la délibération des avis en cause. La circonstance que ce médecin ait laissé publier un entretien dans la presse généraliste le jour même de l'avis du 19 octobre 2016, pour regrettable qu'elle soit au regard de l'obligation de discrétion professionnelle qui s'impose à lui, n'est pas de nature à entacher d'irrégularité les avis rendus par la commission de la transparence les 6 juillet et 19 octobre 2016, pas davantage que la circonstance qu'il ait, postérieurement à ces avis, écrit un ouvrage, publié en avril 2018, consacré à la maladie d'Alzheimer, dans lequel il dénonce la médicalisation excessive de cette maladie et décrit la procédure qui s'est déroulée devant la commission de la transparence. Dans ces conditions, les requérantes ne sont pas fondées à soutenir que l'arrêté attaqué aurait été pris au vu d'avis entachés d'irrégularité.<br/>
<br/>
              Sur l'erreur manifeste dans l'appréciation du service médical rendu par les spécialités en litige :  <br/>
<br/>
              5. Aux termes du I de l'article R. 163-3 du code de la sécurité sociale, l'appréciation du service médical rendu par un médicament dans une indication " prend en compte l'efficacité et les effets indésirables du médicament, sa place dans la stratégie thérapeutique, notamment au regard des autres thérapies disponibles, la gravité de l'affection à laquelle il est destiné, le caractère préventif, curatif ou symptomatique du traitement médicamenteux et son intérêt pour la santé publique ". <br/>
<br/>
              6. Il ressort des pièces du dossier que la maladie d'Alzheimer, maladie neurologique dégénérative du système nerveux central entraînant une détérioration progressive des fonctions cognitives, une perte d'autonomie progressive et des troubles du comportement, est une maladie fréquente et grave, dont les répercussions sur les proches des patients sont au surplus importantes, et qu'il n'existe pas de médicaments autres que les spécialités en litige disposant d'une autorisation de mise sur le marché indiqués dans le traitement de cette maladie. Toutefois, il ressort également des pièces du dossier, y compris des méta-analyses les plus récentes, que ces spécialités, à visée uniquement symptomatique, ont une efficacité au mieux modeste sur les troubles cognitifs et les activités de la vie quotidienne des patients, eu égard au caractère difficilement transposable en conditions réelles d'utilisation des résultats des essais cliniques menés, alors qu'ils ont des effets indésirables notables et accroissent de façon importante les risques d'interactions médicamenteuses chez les patients auxquels ils sont prescrits, sans qu'il soit possible en l'état des connaissances d'identifier a priori les patients ayant une bonne réponse à ces spécialités, et alors que les interventions non médicamenteuses, qu'il appartient au demeurant aux pouvoirs publics de développer, sont essentielles à la prise en charge des patients. Dans ces conditions, les ministres n'ont pas commis d'erreur manifeste d'appréciation en jugeant insuffisant le service médical rendu par ces spécialités au regard des autres thérapies existantes et en décidant de les radier pour ce motif de la liste des médicaments remboursables aux assurés sociaux mentionnée à l'article L. 162-17 du code de la sécurité sociale.<br/>
<br/>
              7. Il résulte de tout ce qui précède, sans qu'il soit besoin de se prononcer sur la fin de non-recevoir soulevée par la ministre des solidarités et de la santé, que les requérantes ne sont pas fondées à demander l'annulation de l'arrêté qu'elles attaquent.<br/>
<br/>
              8. Dès lors, les conclusions qu'elles présentent au titre de l'article L. 761-1 du code de justice administrative ne peuvent qu'être également rejetées. <br/>
<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : La requête de la Fédération des centres mémoire et des autres requérantes est rejetée.<br/>
Article 2  : La présente décision sera notifiée, pour l'ensemble des requérantes, à la Fédération des centres mémoire, première dénommée, et à la ministre des solidarités et de la santé.<br/>
Copie en sera adressée au ministre de l'action et des comptes publics et à la Haute Autorité de santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">61-11-02 SANTÉ PUBLIQUE. - COMMISSION DE LA TRANSPARENCE - OBLIGATIONS INCOMBANT AUX MEMBRES - 1) INTERDICTION DE PARTICIPER À L'EXAMEN D'UNE AFFAIRE À LAQUELLE ILS ONT UN INTÉRÊT DIRECT OU INDIRECT [RJ1] - 2) SECRET ET DISCRÉTION PROFESSIONNELS - 3) ABSTENTION DE TOUTE PRISE DE POSITION PUBLIQUE DE NATURE À COMPROMETTRE LE PRINCIPE D'IMPARTIALITÉ [RJ2].
</SCT>
<ANA ID="9A"> 61-11-02 1) L'article R. 161-85 du code de la sécurité sociale (CSS) prévoit que les membres de la commission de la transparence de la Haute Autorité de santé (HAS) et les personnes qui lui apportent leur concours ne peuvent traiter une question dans laquelle ils auraient un intérêt direct ou indirect. En application des dispositions combinées de cet article et de l'article L. 1451-1 du code de la santé publique (CSP), ils ne peuvent prendre part ni aux travaux ni aux délibérations ni aux votes au sein de la commission de la transparence s'ils ont un intérêt direct ou indirect à l'affaire examinée.... ,,2) Ils sont tenus au respect du secret professionnel et doivent faire preuve de discrétion professionnelle s'agissant des faits, informations ou documents dont ils ont connaissance dans l'exercice de leurs fonctions, dans les mêmes conditions que celles qui sont définies, pour les fonctionnaires, à l'article 26 de la loi n° 83-634 du 13 juillet 1983.... ,,3) Il incombe aux membres de la commission de la transparence de s'abstenir de toute prise de position publique qui serait de nature à compromettre le respect du principe d'impartialité.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 13 novembre 2013, Société Novartis Pharma SAS, n° 344490, T. pp. 410-851.,,[RJ2] Cf., sur le principe d'impartialité qui s'impose à toute autorité administrative et sa portée en termes de prises de position publiques, CE, Section, 30 décembre 2010, Société Métropole Télévision (M6), n° 338273, p. 544.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
