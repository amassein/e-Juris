<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033891915</ID>
<ANCIEN_ID>JG_L_2017_01_000000398918</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/89/19/CETATEXT000033891915.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 1ère chambres réunies, 18/01/2017, 398918, Publié au recueil Lebon</TITRE>
<DATE_DEC>2017-01-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>398918</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Didier Ribes</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CECHR:2017:398918.20170118</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le jugement nos 1404402, 1409128 du 19 avril 2016, enregistré le 20 avril 2016 au secrétariat du contentieux du Conseil d'État, par lequel le tribunal administratif de Cergy-Pontoise, avant de statuer sur les requêtes de Mme A...B...tendant, d'une part, à l'annulation de la décision du 15 janvier 2014 par laquelle le président du tribunal administratif de Montreuil a rejeté sa demande tendant à ce que lui soient délivrées respectivement neuf attestations de fin de mission, dix-neuf attestations de fin de mission et dix attestations de fin de mission correspondant à trois instances dans lesquelles elle a représenté des bénéficiaires de l'aide juridictionnelle et à ce qu'il soit enjoint au président du tribunal de lui délivrer les attestations sollicitées et, d'autre part, à l'annulation de la décision du 8 juillet 2014 par laquelle le président du tribunal administratif de Montreuil a rejeté sa demande tendant à ce que lui soient délivrées cinq attestations de fin de mission correspondant à une instance dans laquelle elle a représenté des bénéficiaires de l'aide juridictionnelle et à ce qu'il soit enjoint au président du tribunal de lui délivrer les attestations sollicitées, a décidé, par application des dispositions de l'article L. 113-1 du code de justice administrative, de transmettre le dossier de ces requêtes jointes au Conseil d'État, en soumettant à son examen les questions suivantes : <br/>
<br/>
              1°) Y a-t-il lieu pour le juge administratif de se prononcer sur les décisions administratives prises par un président de juridiction relatives à la rétribution de l'avocat désigné au titre de l'aide juridictionnelle comme juge de plein contentieux '<br/>
<br/>
              2°) Quelle portée donner à la notion de " série d'affaires " au regard des dispositions combinées des articles 38 de la loi du 10 juillet 1991 et de l'article 109 du décret du 19 décembre 1991 ' Concerne-t-elle exclusivement des instances distinctes, la contribution de l'État étant réduite à partir de la deuxième " affaire " dont est chargé un avocat lorsque celles-ci présentent à juger des questions semblables ' Ou trouve-t-elle également à s'appliquer à une seule instance dans laquelle en demande ou en défense un avocat représente plusieurs bénéficiaires de l'aide juridictionnelle contestant une décision ou une mesure les concernant collectivement, chaque bénéficiaire de l'aide juridictionnelle représentant alors une " affaire " dans un litige reposant sur les mêmes faits et comportant des prétentions ayant un objet similaire '<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le décret n° 91-1266 du 19 décembre 1991 ;<br/>
              - le code de justice administrative, notamment son article L. 113-1 ;<br/>
<br/>
<br/>
<br/>
               Après avoir entendu en séance publique :<br/>
<br/>
- le rapport de M. Didier Ribes, maître des requêtes, <br/>
- les conclusions de M. Xavier de Lesquen, rapporteur public.<br/>
<br/>
<br/>REND L'AVIS SUIVANT :<br/>
<br/>
              1. L'article 27 de la loi du 10 juillet 1991 relative à l'aide juridique dispose que : " L'avocat qui prête son concours au bénéficiaire de l'aide juridictionnelle perçoit une rétribution. / L'État affecte annuellement à chaque barreau une dotation représentant sa part contributive aux missions d'aide juridictionnelle accomplies par les avocats du barreau. / Le montant de cette dotation résulte, d'une part, du nombre de missions d'aide juridictionnelle accomplies par les avocats du barreau et, d'autre part, du produit d'un coefficient par type de procédure et d'une unité de valeur de référence. / (...) ". En vertu de l'article 38 de la même loi, " la contribution versée par l'État est réduite, selon des modalités fixées par décret en Conseil d'Etat, lorsqu'un avocat ou un avocat au Conseil d'Etat et à la Cour de cassation est chargé d'une série d'affaires présentant à juger des questions semblables ". L'article 70 de la même loi précise que : " Un décret en Conseil d'Etat fixe les conditions d'application de la présente loi, et notamment : / (...) / 8° Les modalités suivant lesquelles est réduite la part contributive de l'État en cas de pluralité de parties au cas prévu par l'article 38 ; / (...) ".<br/>
<br/>
              2. L'article 104 du décret du 19 décembre 1991 portant application de la loi n° 91-647 du 10 juillet 1991 relative à l'aide juridique dispose que : " Les sommes revenant aux avocats et aux avocats au Conseil d'État et à la Cour de cassation sont réglées sur justification de la désignation au titre de l'aide juridictionnelle et production d'une attestation de mission délivrée par le greffier en chef ou le secrétaire de la juridiction saisie. / Cette attestation mentionne la nature de la procédure, les diligences effectuées et, selon le cas : / - le montant de la contribution de l'État à la rétribution de l'avocat après, le cas échéant, application de la réduction prévue à l'article 109 ou imputation de la somme perçue par lui au titre de l'aide juridictionnelle pour des pourparlers transactionnels ayant échoué ou une procédure participative n'ayant pas abouti à un accord total ; (...) / L'attestation est délivrée ou remise à l'auxiliaire de justice au moment où le juge rend sa décision ou, au plus tard, en même temps que lui en est adressée une expédition, sous réserve des dispositions du premier alinéa de l'article 108 et de l'article 108-1. / Les difficultés auxquelles donne lieu l'application du présent article sont tranchées sans forme par le président de la juridiction ". L'article 109 du même décret précise que : " La part contributive versée par l'État à l'avocat choisi ou désigné pour assister plusieurs personnes dans une procédure reposant sur les mêmes faits en matière pénale ou dans un litige reposant sur les mêmes faits et comportant des prétentions ayant un objet similaire dans les autres matières est réduite de 30 % pour la deuxième affaire, de 40 % pour la troisième, de 50 % pour la quatrième et de 60 % pour la cinquième et s'il y a lieu pour les affaires supplémentaires ". <br/>
<br/>
              3. Les décisions prises par le président de la juridiction saisie en application des dispositions mentionnées au point 2 relatives à la rétribution de l'avocat désigné au titre de l'aide juridictionnelle ont le caractère de décisions administratives. Le recours dont elles peuvent faire l'objet est un recours de plein contentieux à l'occasion duquel le juge détermine la part contributive de l'État à la rétribution de la mission d'aide juridictionnelle assurée par l'avocat.<br/>
<br/>
              4. Il résulte de la combinaison des dispositions, citées aux points 1 et 2, de la loi du 10 juillet 1991 et du décret du 19 décembre 1991 pris pour son application, que l'avocat perçoit en principe une rétribution pour toute mission de représentation d'une personne bénéficiaire de l'aide juridictionnelle dans une instance déterminée.<br/>
<br/>
              5. Toutefois, lorsque plusieurs bénéficiaires de l'aide juridictionnelle présentent, dans une même instance ou dans plusieurs instances, des conclusions identiques en demande ou en défense conduisant le juge à trancher les mêmes questions, l'avocat les représentant au titre de l'aide juridictionnelle réalise à leur égard une seule et même mission.<br/>
<br/>
              6. La réduction de la part contributive de l'Etat à la rétribution des missions d'aide juridictionnelle assurées par l'avocat devant la juridiction administrative s'applique lorsque celui-ci assiste plusieurs bénéficiaires de l'aide juridictionnelle présentant des conclusions similaires en demande ou en défense et que le juge est conduit à trancher des questions semblables, soit dans le cadre d'une même instance, soit dans le cadre d'instances distinctes reposant sur les mêmes faits.<br/>
<br/>
              Le présent avis sera notifié au tribunal administratif de Cergy Pontoise, à Mme A...B..., à l'ordre des avocats au barreau de Seine-Saint-Denis et au garde des sceaux, ministre de la justice. <br/>
<br/>
              Il sera publié au Journal officiel de la République française.<br/>
<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-02-02-01 PROCÉDURE. DIVERSES SORTES DE RECOURS. RECOURS DE PLEIN CONTENTIEUX. RECOURS AYANT CE CARACTÈRE. - RECOURS CONTRE LA DÉCISION DU PRÉSIDENT D'UNE JURIDICTION RELATIVE À LA RÉTRIBUTION D'UN AVOCAT AU TITRE DE L'AIDE JURIDICTIONNELLE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-06-05-09 PROCÉDURE. JUGEMENTS. FRAIS ET DÉPENS. AIDE JURIDICTIONNELLE. - RÉTRIBUTION DE L'AVOCAT AU TITRE DE L'AIDE JURIDICTIONNELLE - 1) RÉMUNÉRATION DE LA MISSION - A) NOTION DE MISSION - B) RÉDUCTION POUR LE TRAITEMENT DES SÉRIES - 2) RECOURS ADMINISTRATIF AUPRÈS DU PRÉSIDENT DE LA JURIDICTION - FACULTÉ D'EXERCER UN RECOURS DE PLEIN CONTENTIEUX CONTRE LA DÉCISION DU PRÉSIDENT - EXISTENCE.
</SCT>
<ANA ID="9A"> 54-02-02-01 Recours administratif auprès du président de la juridiction en cas de contestation de l'attestation de mission délivrée à l'avocat par le greffe (art. 104 du décret n° 91-1266 du 19 décembre 1991).... ,,Les décisions prises sur ce recours relatif à la rétribution de l'avocat désigné au titre de l'aide juridictionnelle par le président de la juridiction ont le caractère de décisions administratives. Le recours juridictionnel dont elles peuvent faire l'objet est un recours de plein contentieux à l'occasion duquel le juge détermine la part contributive de l'État à la rétribution de la mission d'aide juridictionnelle assurée par l'avocat.</ANA>
<ANA ID="9B"> 54-06-05-09 1) Il résulte de la combinaison des dispositions de loi n° 91-647 du 10 juillet 1991 et du décret n° 91-1266 du 19 décembre 1991 pris pour son application que l'avocat perçoit en principe une rétribution pour toute mission de représentation d'une personne bénéficiaire de l'aide juridictionnelle dans une instance déterminée.,,,a) Toutefois, lorsque plusieurs bénéficiaires de l'aide juridictionnelle présentent, dans une même instance ou dans plusieurs instances, des conclusions identiques en demande ou en défense conduisant le juge à trancher les mêmes questions, l'avocat les représentant au titre de l'aide juridictionnelle réalise à leur égard une seule et même mission.,,,b) La réduction de la part contributive de l'Etat à la rétribution des missions d'aide juridictionnelle assurées par l'avocat devant la juridiction administrative, prévue par l'article 109 du décret n° 91-1266 du 19 décembre 1991 s'applique lorsque celui-ci assiste plusieurs bénéficiaires de l'aide juridictionnelle présentant des conclusions similaires en demande ou en défense et que le juge est conduit à trancher des questions semblables, soit dans le cadre d'une même instance, soit dans le cadre d'instances distinctes reposant sur les mêmes faits.,,,2) Recours administratif auprès du président de la juridiction en cas de contestation de l'attestation de mission délivrée à l'avocat par le greffe (art. 104 du décret n° 91-1266 du 19 décembre 1991). Les décisions prises sur ce recours relatif à la rétribution de l'avocat désigné au titre de l'aide juridictionnelle par le président de la juridiction ont le caractère de décisions administratives. Le recours juridictionnel dont elles peuvent faire l'objet est un recours de plein contentieux à l'occasion duquel le juge détermine la part contributive de l'État à la rétribution de la mission d'aide juridictionnelle assurée par l'avocat.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
