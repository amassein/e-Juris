<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037599970</ID>
<ANCIEN_ID>JG_L_2018_11_000000411816</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/59/99/CETATEXT000037599970.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 4ème chambres réunies, 09/11/2018, 411816</TITRE>
<DATE_DEC>2018-11-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>411816</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Thibaut Félix</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:411816.20181109</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et par un mémoire en réplique, enregistrés les 23 juin 2017 et 16 mars 2018 au secrétariat du contentieux du Conseil d'Etat, la confédération nationale des syndicats dentaires demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté du 19 avril 2017 pris pour l'application de l'article L. 162-9 du code de la sécurité sociale et fixant le niveau maximal de dépassement sur les soins dentaires prothétiques ou d'orthopédie dento-faciale applicable aux bénéficiaires de l'aide au paiement d'une assurance complémentaire de santé mentionnée à l'article L. 863-1 du code de la sécurité sociale ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la santé publique ;<br/>
              - le code de la sécurité sociale ;<br/>
              - le décret n° 2000-685 du 21 juillet 2000 ;<br/>
              - le décret n° 2005-850 du 27 juillet 2005 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Thibaut Félix, auditeur,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public.<br/>
<br/>
<br/>
              Vu la note en délibéré, enregistrée le 19 octobre 2018, présentée par le ministre des solidarités et de la santé ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. L'article L. 162-9 du code de la sécurité sociale prévoit que : " Les rapports entre les organismes d'assurance maladie et les chirurgiens-dentistes, les sages-femmes et les auxiliaires médicaux sont définis par des conventions nationales conclues entre l'Union nationale des caisses d'assurance maladie et une ou plusieurs des organisations syndicales nationales les plus représentatives de chacune de ces professions. (...) Si elle autorise un dépassement pour les soins visés au 3° de l'article L. 861-3 ", c'est-à-dire les frais exposés, en sus des tarifs de responsabilité, pour les soins dentaires prothétiques ou d'orthopédie dento-faciale, " la convention nationale intéressant les chirurgiens-dentistes fixe le montant maximal de ce dépassement applicable aux bénéficiaires du droit à la protection complémentaire en matière de santé et aux bénéficiaires de l'aide au paiement d'une assurance complémentaire de santé ; à défaut de convention, ou si la convention ne prévoit pas de dispositions spécifiques aux bénéficiaires de cette protection ou de cette aide, un arrêté interministériel détermine la limite applicable à ces dépassements pour les intéressés ". Il résulte de ces dispositions, eu égard à l'objet de l'arrêté interministériel ainsi prévu, que le législateur a entendu, comme pour l'approbation de la convention nationale en vertu de l'article L. 162-15 du même code, donner compétence pour le signer au ministre chargé de la sécurité sociale et au ministre chargé de la santé agissant conjointement. <br/>
<br/>
              2. Aux termes de l'article 1er du décret du 27 juillet 2005 relatif aux délégations de signature des membres du Gouvernement : " A compter du jour suivant la publication au Journal officiel de la République française de l'acte les nommant dans leurs fonctions ou à compter du jour où cet acte prend effet, si ce jour est postérieur, peuvent signer, au nom du ministre ou du secrétaire d'Etat et par délégation, l'ensemble des actes, à l'exception des décrets, relatifs aux affaires des services placés sous leur autorité : / (...) les directeurs d'administration centrale (...) ". L'article 6 du décret du 21 juillet 2000 relatif à l'organisation de l'administration centrale du ministère de l'emploi et de la solidarité et aux attributions de certains de ses services prévoit que : " La direction de la sécurité sociale est chargée de l'élaboration et de la mise en oeuvre de la politique relative à la sécurité sociale (...) ". Il suit de là que M.A..., nommé directeur de la sécurité sociale à compter du 13 février 2012 par décret du 26 janvier 2012, avait compétence pour signer, au nom du ou des ministres chargés de la sécurité sociale, l'arrêté attaqué, pris pour l'application de l'article L. 162-9 du code de la sécurité sociale et fixant le niveau maximal de dépassement sur les soins dentaires prothétiques ou d'orthopédie dento-faciale applicable aux bénéficiaires de l'aide au paiement d'une assurance complémentaire de santé. En revanche, en l'absence de signature par le ministre des affaires sociales et de la santé ou par un agent ayant délégation pour signer un tel acte au nom de ce ministre, en sa qualité de ministre chargé de la santé, l'arrêté attaqué est entaché d'incompétence. <br/>
<br/>
              3. Par suite, et sans qu'il soit besoin d'examiner les moyens de la requête, la confédération nationale des syndicats dentaires est fondée à demander l'annulation de l'arrêté attaqué. <br/>
<br/>
              4. Toutefois, compte tenu des effets manifestement excessifs d'une disparition immédiate de l'encadrement du niveau de dépassement sur les soins dentaires prothétiques ou d'orthopédie dento-faciale applicable aux bénéficiaires de l'aide au paiement d'une assurance complémentaire de santé, susceptible de restreindre l'accès à ces soins, il y a lieu de différer l'effet de l'annulation de cet arrêté, sous réserve des droits des personnes qui auraient engagé une action contentieuse à la date de la présente décision, jusqu'au 1er janvier 2019, et de regarder comme définitifs les effets produits par cet arrêté antérieurement à son annulation.<br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 1 500 euros à verser à la confédération nationale des syndicats dentaires, au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
				--------------<br/>
Article 1er : L'arrêté du 19 avril 2017 pris pour l'application de l'article L. 162-9 du code de la sécurité sociale et fixant le niveau maximal de dépassement sur les soins dentaires prothétiques ou d'orthopédie dento-faciale applicable aux bénéficiaires de l'aide au paiement d'une assurance complémentaire de santé mentionnée à l'article L. 863-1 du code de la sécurité sociale est annulé à compter du 1er janvier 2019. Sous réserve des actions contentieuses engagées à la date de la présente décision contre les actes pris sur son fondement, les effets produits par cet arrêté antérieurement à son annulation sont regardés comme définitifs.<br/>
Article 2 : L'Etat versera à la confédération nationale des syndicats dentaires une somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la confédération nationale des syndicats dentaires, au ministre de l'économie et des finances et à la ministre des solidarités et de la santé.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-02-02-01-03-14 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - COMPÉTENCE. RÉPARTITION DES COMPÉTENCES ENTRE AUTORITÉS DISPOSANT DU POUVOIR RÉGLEMENTAIRE. AUTORITÉS DISPOSANT DU POUVOIR RÉGLEMENTAIRE. MINISTRES. MINISTRE CHARGÉ DE LA SANTÉ PUBLIQUE. - ARRÊTÉ INTERMINISTÉRIEL (DERNIER AL. DE L'ART. L. 162-9 DU CSS), RELEVANT DE LA COMPÉTENCE DES MINISTRES CHARGÉS DE LA SÉCURITÉ SOCIALE ET DE LA SANTÉ, AGISSANT CONJOINTEMENT [RJ1] - COMPÉTENCE DU DIRECTEUR DE LA SÉCURITÉ SOCIALE POUR LE SIGNER, AU NOM DU OU DES MINISTRES CHARGÉS DE LA SÉCURITÉ SOCIALE - EXISTENCE (ART. 1ER DU DÉCRET DU 27 JUILLET 2005 ET ART. 6 DU DÉCRET DU 21 JUILLET 2000) - ABSENCE DE SIGNATURE DE CET ARRÊTÉ PAR LE MINISTRE CHARGÉ DE LA SANTÉ OU PAR UN AGENT AYANT DÉLÉGATION POUR LE SIGNER AU NOM DE CE MINISTRE, EN SA QUALITÉ DE MINISTRE DE LA SANTÉ - CONSÉQUENCE - INCOMPÉTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">01-02-02-01-03-15 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - COMPÉTENCE. RÉPARTITION DES COMPÉTENCES ENTRE AUTORITÉS DISPOSANT DU POUVOIR RÉGLEMENTAIRE. AUTORITÉS DISPOSANT DU POUVOIR RÉGLEMENTAIRE. MINISTRES. MINISTRE CHARGÉ DE LA SÉCURITÉ SOCIALE. - ARRÊTÉ INTERMINISTÉRIEL (DERNIER AL. DE L'ART. L. 162-9 DU CSS), RELEVANT DE LA COMPÉTENCE DES MINISTRES CHARGÉS DE LA SÉCURITÉ SOCIALE ET DE LA SANTÉ, AGISSANT CONJOINTEMENT [RJ1] - COMPÉTENCE DU DIRECTEUR DE LA SÉCURITÉ SOCIALE POUR LE SIGNER, AU NOM DU OU DES MINISTRES CHARGÉS DE LA SÉCURITÉ SOCIALE - EXISTENCE (ART. 1ER DU DÉCRET DU 27 JUILLET 2005 ET ART. 6 DU DÉCRET DU 21 JUILLET 2000) - ABSENCE DE SIGNATURE DE CET ARRÊTÉ PAR LE MINISTRE CHARGÉ DE LA SANTÉ OU PAR UN AGENT AYANT DÉLÉGATION POUR LE SIGNER AU NOM DE CE MINISTRE, EN SA QUALITÉ DE MINISTRE DE LA SANTÉ - CONSÉQUENCE - INCOMPÉTENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">01-02-05-02 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - COMPÉTENCE. DÉLÉGATIONS, SUPPLÉANCE, INTÉRIM. DÉLÉGATION DE SIGNATURE. - ARRÊTÉ INTERMINISTÉRIEL (DERNIER AL. DE L'ART. L. 162-9 DU CSS), RELEVANT DE LA COMPÉTENCE DES MINISTRES CHARGÉS DE LA SÉCURITÉ SOCIALE ET DE LA SANTÉ, AGISSANT CONJOINTEMENT [RJ1] - COMPÉTENCE DU DIRECTEUR DE LA SÉCURITÉ SOCIALE POUR LE SIGNER, AU NOM DU OU DES MINISTRES CHARGÉS DE LA SÉCURITÉ SOCIALE - EXISTENCE (ART. 1ER DU DÉCRET DU 27 JUILLET 2005 ET ART. 6 DU DÉCRET DU 21 JUILLET 2000) - COMPÉTENCE DE CE DIRECTEUR POUR LE SIGNER, AU NOM DU MINISTRE CHARGÉ DE LA SANTÉ - ABSENCE.
</SCT>
<ANA ID="9A"> 01-02-02-01-03-14 Il résulte de l'article L. 162-9 du code de la sécurité sociale (CSS), eu égard à l'objet de l'arrêté interministériel prévu à son dernier alinéa, que le législateur a entendu donner compétence pour le signer au ministre chargé de la sécurité sociale et au ministre chargé de la santé agissant conjointement.,,Aux termes de l'article 1er du décret n° 2005-850 du 27 juillet 2005, les directeurs d'administration centrale peuvent signer, au nom du ministre ou du secrétaire d'Etat et par délégation, l'ensemble des actes, à l'exception des décrets, relatifs aux affaires des services placés sous leur autorité. L'article 6 du décret n° 2000-685 du 21 juillet 2000 prévoit que la direction de la sécurité sociale (DSS) est chargée de l'élaboration et de la mise en oeuvre de la politique relative à la sécurité sociale.... ...Il suit de là que le directeur de la sécurité sociale a compétence pour signer, au nom du ou des ministres chargés de la sécurité sociale, un arrêté pris pour l'application du dernier alinéa de l'article L. 162-9 du CSS. En revanche, en l'absence de signature par le ministre des affaires sociales et de la santé ou par un agent ayant délégation pour signer un tel acte au nom de ce ministre, en sa qualité de ministre chargé de la santé, cet arrêté est entaché d'incompétence.</ANA>
<ANA ID="9B"> 01-02-02-01-03-15 Il résulte de l'article L. 162-9 du code de la sécurité sociale (CSS), eu égard à l'objet de l'arrêté interministériel prévu à son dernier alinéa, que le législateur a entendu donner compétence pour le signer au ministre chargé de la sécurité sociale et au ministre chargé de la santé agissant conjointement.,,Aux termes de l'article 1er du décret n° 2005-850 du 27 juillet 2005, les directeurs d'administration centrale peuvent signer, au nom du ministre ou du secrétaire d'Etat et par délégation, l'ensemble des actes, à l'exception des décrets, relatifs aux affaires des services placés sous leur autorité. L'article 6 du décret n° 2000-685 du 21 juillet 2000 prévoit que la direction de la sécurité sociale (DSS) est chargée de l'élaboration et de la mise en oeuvre de la politique relative à la sécurité sociale.... ...Il suit de là que le directeur de la sécurité sociale a compétence pour signer, au nom du ou des ministres chargés de la sécurité sociale, un arrêté pris pour l'application du dernier alinéa de l'article L. 162-9 du CSS. En revanche, en l'absence de signature par le ministre des affaires sociales et de la santé ou par un agent ayant délégation pour signer un tel acte au nom de ce ministre, en sa qualité de ministre chargé de la santé, cet arrêté est entaché d'incompétence.</ANA>
<ANA ID="9C"> 01-02-05-02 Il résulte de l'article L. 162-9 du code de la sécurité sociale (CSS), eu égard à l'objet de l'arrêté interministériel prévu à son dernier alinéa, que le législateur a entendu donner compétence pour le signer au ministre chargé de la sécurité sociale et au ministre chargé de la santé agissant conjointement.,,Aux termes de l'article 1er du décret n° 2005-850 du 27 juillet 2005, les directeurs d'administration centrale peuvent signer, au nom du ministre ou du secrétaire d'Etat et par délégation, l'ensemble des actes, à l'exception des décrets, relatifs aux affaires des services placés sous leur autorité. L'article 6 du décret n° 2000-685 du 21 juillet 2000 prévoit que la direction de la sécurité sociale (DSS) est chargée de l'élaboration et de la mise en oeuvre de la politique relative à la sécurité sociale.... ...Il suit de là que le directeur de la sécurité sociale a compétence pour signer, au nom du ou des ministres chargés de la sécurité sociale, un arrêté pris pour l'application du dernier alinéa de l'article L. 162-9 du CSS. En revanche, en l'absence de signature par le ministre des affaires sociales et de la santé ou par un agent ayant délégation pour signer un tel acte au nom de ce ministre, en sa qualité de ministre chargé de la santé, cet arrêté est entaché d'incompétence.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., s'agissant de la méthode de détermination des ministres compétents, CE, 26 mai 1989, Syndicat des sylviculteurs du Sud-Ouest, n° 78164 78165, T. pp. 442-452-483-867 ; CE, 14 mai 2003, Ville d'Agen, n° 235051, p. 208.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
