<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037487171</ID>
<ANCIEN_ID>JG_L_2018_10_000000395280</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/48/71/CETATEXT000037487171.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème et 1ère chambres réunies, 10/10/2018, 395280</TITRE>
<DATE_DEC>2018-10-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>395280</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème et 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ ; SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR>M. Jean-François de Montgolfier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:395280.20181010</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La fédération Force Ouvrière des employés et cadres et la fédération des syndicats du personnel de la banque et de l'assurance CGT ont demandé au tribunal administratif de Melun d'annuler pour excès de pouvoir la décision du 2 janvier 2015 du directeur régional des entreprises, de la concurrence, de la consommation, du travail et de l'emploi d'Ile-de-France validant l'accord collectif et homologuant le document unilatéral complétant cet accord relatifs au plan de sauvegarde de l'emploi de la société Le Crédit Lyonnais (LCL). Par un jugement n° 1501476, 1501477 du 22 mai 2015, le tribunal administratif a rejeté leurs demandes.<br/>
<br/>
              Par un arrêt n° 15PA02792, 15PA02881 du 15 octobre 2015, la cour administrative d'appel de Paris a rejeté les appels formés par la fédération Force Ouvrière des employés et cadres et par la fédération des syndicats du personnel de la banque et de l'assurance CGT contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire un mémoire en réplique et deux nouveaux mémoires, enregistrés le 15 décembre 2015, les 15 février, 1er juillet et 15 novembre 2016 et le 25 juin 2018 au secrétariat du contentieux du Conseil d'Etat, la fédération Force Ouvrière des employés et cadres et la fédération des syndicats du personnel de la banque et de l'assurance CGT demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
              2°) réglant l'affaire au fond, de faire droit à leurs appels ;<br/>
<br/>
              3°) de mettre à la charge de la société LCL la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code du travail ;<br/>
              - la convention collective nationale des banques ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-François de Montgolfier, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de la fédération Force Ouvrière des employés et cadres et de la fédération des syndicats du personnel de la banque et de l'assurance CGT et à la SCP Matuchansky, Poupot, Valdelièvre, avocat de la société Le Crédit Lyonnais (LCL) ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la société Le Crédit Lyonnais (LCL) a décidé en 2014, dans le cadre de la stratégie d'entreprise " Centricité Clients 2018 ", la réorganisation de plusieurs de ses services ; que cette réorganisation comportait des suppressions de postes au sein, d'une part, de la direction des services banque et assurance (DSBa), à hauteur de 289 unités de temps plein sur la période 2014-2016 et, d'autre part, du réseau des agences (réseau Retail hors CRC), à hauteur de 1 369 unités de temps plein sur la période 2014-2018 ; que, par une décision du 2 janvier 2015, le directeur régional des entreprises, de la concurrence, de la consommation, du travail et de l'emploi d'Ile-de-France a, d'une part, validé l'accord collectif majoritaire du 11 décembre 2014 portant sur les mesures d'accompagnement social du plan de sauvegarde de l'emploi relatif à cette opération et, d'autre part, homologué le document unilatéral complétant cet accord ; que la fédération Force Ouvrière des employés et cadres et la fédération des syndicats du personnel de la banque et de l'assurance CGT se pourvoient en cassation contre l'arrêt du 15 octobre 2015 par lequel la cour administrative d'appel de Paris a rejeté leurs appels formés contre le jugement du tribunal administratif de Melun du 22 mai 2015 rejetant leurs demandes d'annulation de cette décision ;<br/>
<br/>
<br/>
              2. Considérant qu'il ressort des termes de l'arrêt attaqué, qui n'est pas entaché de dénaturation sur ce point, que, compte tenu du nombre des départs volontaires en retraite et des autres départs volontaires des salariés de la DSBa et du réseau Retail hors CRC qui étaient anticipés par l'employeur, le plan de sauvegarde de l'emploi a été élaboré en estimant que le nombre de postes qui deviendraient, de fait, vacants au sein de la DSBa et du réseau Retail hors CRC, dépasserait largement, pour chacune des catégories professionnelles concernées, le nombre de suppressions de postes qui devait être atteint dans ces mêmes services ; qu'il ressort ainsi des pièces du dossier soumis aux juges du fond que le plan soumis à l'administration ne prévoyait, compte tenu des départs volontaires, aucun licenciement lié à une suppression d'emploi ; qu'il retenait au contraire, pour sa période d'exécution, la nécessité d'un recrutement net de 387 équivalents temps plein (ETP) au sein de la DSBa et de 750 ETP au sein du réseau Retail hors CRC ; que, toutefois, les postes affectés par les départs volontaires n'étant pas nécessairement ceux qui devaient être supprimés, l'employeur prévoyait, une fois effectués ces départs volontaires, de proposer aux salariés qui occuperaient un poste ayant vocation à être supprimé un autre poste au sein de la DSBa ou du réseau Retail hors CRC ; que, dans la mesure où ces propositions étaient susceptibles d'entraîner des modifications des contrats de travail des salariés concernés et donc à des refus de leur part, le plan de sauvegarde de l'emploi envisageait la possibilité de licenciements consécutifs à de tels refus, qu'il évaluait à, au plus, 231 pour la DSBa et 260 pour le réseau Retail hors CRC ;<br/>
<br/>
              Sur le droit applicable :<br/>
<br/>
              3. Considérant qu'aux termes de l'article L. 1233-25 du code du travail : " Lorsqu'au moins dix salariés ont refusé la modification d'un élément essentiel de leur contrat de travail, proposée par leur employeur pour l'un des motifs économiques énoncés à l'article L. 1233-3 et que leur licenciement est envisagé, celui-ci est soumis aux dispositions applicables en cas de licenciement collectif pour motif économique " ; qu'aux termes de l'article L. 1233-61 du code du travail : " Dans les entreprises d'au moins cinquante salariés, lorsque le projet de licenciement concerne au moins dix salariés dans une même période de trente jours, l'employeur établit et met en oeuvre un plan de sauvegarde de l'emploi pour éviter les licenciements ou en limiter le nombre (...) " ; que, par suite, alors même que le projet de réorganisation et de compression d'effectifs de la société LCL n'aboutissait par lui-même, une fois tenu compte des départs volontaires en retraite et des autres départs volontaires, à aucune suppression d'emploi, il devait, dès lors qu'il prévoyait en revanche le licenciement des salariés refusant la modification de leur contrat de travail, faire l'objet d'un plan de sauvegarde de l'emploi, soumis à l'homologation ou à la validation de l'autorité administrative ;<br/>
<br/>
              Sur les moyens du pourvoi :<br/>
<br/>
              4. Considérant, en premier lieu, que l'article L. 1233-57-2 du code du travail fixe, notamment par renvoi à l'article L. 1233-24-3 du même code, les dispositions auxquelles un accord collectif majoritaire fixant un plan de sauvegarde de l'emploi ne peut déroger ; qu'un tel accord ne peut, par suite, faire l'objet d'un refus de validation par l'autorité administrative que s'il méconnaît ces dispositions ou s'il comporte des clauses qui l'entachent de nullité, en raison notamment de ce qu'elles revêtiraient un caractère discriminatoire ;<br/>
<br/>
              5. Considérant que les requérantes se bornaient à soutenir, devant la cour administrative d'appel, que le seul fait que l'accord collectif du 11 décembre 2014 réserve les mesures d'accompagnement social aux seuls salariés de la DSBa et du réseau Retail hors CRC, sans les étendre à l'ensemble des salariés de la société LCL, violait une prétendue règle d'application uniforme des plans de sauvegarde de l'emploi au sein des entreprises et créait une inégalité de traitement entre les salariés d'une même entreprise ; qu'il résulte de ce qui précède que de telles circonstances, qui ne sont pas au nombre de celles mentionnées au point précédent, ne pouvaient être utilement invoquées pour faire obstacle à la validation d'un accord collectif majoritaire par l'administration ; que ce motif de pur droit pouvant être substitué au motif par lequel l'arrêt attaqué répond à ces moyens d'appel, il y a lieu, par suite, d'écarter les moyens de cassation dirigés contre le motif retenu par la cour ;<br/>
<br/>
              6. Considérant, en deuxième lieu, qu'aux termes de l'article L. 1233-5 du code du travail : " Lorsque l'employeur procède à un licenciement collectif pour motif économique et en l'absence de convention ou accord collectif de travail applicable, il définit les critères retenus pour fixer l'ordre des licenciement (...) " ; qu'aux termes des dispositions de l'article L. 1233-24-2 du code du travail, l'accord collectif fixant le contenu d'un plan de sauvegarde de l'emploi peut également porter sur : " (...) 2° La pondération et le périmètre d'application des critères d'ordre des licenciements mentionnés à l'article L. 1233-5 ; (...) " ; qu'enfin, aux termes des dispositions de l'article L. 1233-57-3 du même code : " En l'absence d'accord collectif ou en cas d'accord ne portant pas sur l'ensemble des points mentionnés aux 1° à 5° de l'article L. 1233-24-2, l'autorité administrative homologue le document élaboré par l'employeur mentionné à l'article L. 1233-24-4 (...) " ; qu'il résulte de l'ensemble de ces dispositions que, lorsqu'un plan de sauvegarde de l'emploi fait l'objet, pour certaines de ses dispositions, d'un accord collectif majoritaire et, pour le reste de ses dispositions, d'une décision unilatérale de l'employeur, l'administration ne peut homologuer cette dernière qu'après avoir vérifié que l'ensemble des points mentionnés aux 1° à 5° de l'article L. 1233-24-2, notamment la pondération et le périmètre d'application des critères d'ordre des licenciements, figurent, soit dans l'accord collectif déjà validé par elle ou en cours d'examen devant elle, soit dans le document unilatéral soumis à son homologation ;<br/>
<br/>
              7. Considérant, toutefois, que les critères d'ordre prévus par les dispositions de l'article L. 1233-5 du code du travail citées ci-dessus se trouvent privés d'objet lorsque l'employeur, soit en l'absence de toute suppression d'emploi, soit après avoir procédé aux licenciements consécutifs à des suppressions d'emploi en respectant ces critères d'ordre, envisage seulement de proposer à des salariés une modification de leur contrat de travail et ne prévoit leur licenciement qu'à raison de leur refus ; que par suite, dans ce cas, la circonstance que le plan de sauvegarde de l'emploi ne comporte pas la pondération des critères d'ordre et la définition de leur périmètre d'application ne fait pas légalement obstacle à ce que l'administration homologue le document unilatéral relatif à ce plan ; que, le cas échéant, il lui appartient toutefois de s'assurer, sous le contrôle du juge de l'excès de pouvoir, de la légalité des règles auxquelles ce document aurait décidé de soumettre les propositions de modification de contrat de travail envisagées par le plan ;<br/>
<br/>
              8. Considérant qu'il résulte de ce qui précède et de ce que, ainsi qu'il a été dit au point 2, le plan de sauvegarde de l'emploi de la société LCL n'envisageait le licenciement que des seuls salariés qui, après que les réductions d'effectifs aient été atteintes par des départs volontaires, refuseraient une proposition de modification de leur contrat de travail, que la cour administrative d'appel n'a pas commis d'erreur de droit en jugeant que l'absence de la pondération des critères d'ordre et la définition de leur périmètre d'application n'entachait pas d'illégalité la décision attaquée en tant qu'elle homologue le document unilatéral ; que, pour les mêmes motifs, la cour n'a pas davantage commis d'erreur de droit en jugeant, par une décision qui, alors même qu'elle ne fait pas expressément référence aux stipulations de l'article 29-2 de la convention collective de la banque, est suffisamment motivée sur ce point, que l'absence de consultation des représentants du personnel sur les critères d'ordre applicables à l'opération n'entachait pas d'irrégularité la procédure d'information et de consultation ;<br/>
<br/>
              9.  Considérant, en troisième lieu, que l'article L. 1233-30 du code du travail impose à l'employeur qui envisage de procéder à un licenciement collectif pour motif économique d'au moins dix salariés dans une période de trente jours de consulter les représentants du personnel sur, notamment, " le nombre de suppressions d'emplois " ; que, le plan de sauvegarde de l'emploi de la société LCL n'envisageant, ainsi qu'il a été dit au point 2, que le licenciement des salariés refusant la modification de leur contrat de travail, la cour administrative d'appel n'a pas commis d'erreur de droit ni insuffisamment motivé sa décision en jugeant, par adoption des motifs du jugement du tribunal administratif, que la présentation, par l'employeur, du nombre des propositions de modification du contrat de travail qu'il estimait nécessaires, ne méconnaissait pas les dispositions de l'article L. 1233-30 du code du travail ;<br/>
<br/>
              10.  Considérant, enfin, qu'il résulte des termes mêmes de l'arrêt attaqué que, contrairement à ce que soutiennent les requérantes, celui-ci n'adopte pas les motifs du jugement du tribunal administratif pour écarter le moyen tiré de l'absence de définition des catégories professionnelles par le plan de sauvegarde de l'emploi ; que, par suite, le moyen tiré de ce que la cour administrative d'appel aurait, en raison d'une telle adoption de motifs, réitéré une erreur de droit précédemment commise par le tribunal administratif, est inopérant ;<br/>
<br/>
              11.  Considérant qu'il résulte de tout ce qui précède que la fédération Force Ouvrière des employés et cadres et la fédération des syndicats du personnel de la banque et de l'assurance CGT ne sont pas fondées à demander l'annulation de l'arrêt qu'elles attaquent ;<br/>
<br/>
              12. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la société LCL, qui n'est pas la partie perdante dans la présente instance ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de la fédération Force Ouvrière des employés et cadres ou de la fédération des syndicats du personnel de la banque et de l'assurance CGT la somme que demande au même titre la société LCL ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la fédération Force Ouvrière des employés et cadres et de la fédération des syndicats du personnel de la banque et de l'assurance CGT est rejeté.<br/>
Article 2 : Les conclusions de la société LCL, présentées au titre de l'article L. 761-1 du code de justice administrative, sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à la fédération Force Ouvrière des employés et cadres, à la fédération des syndicats du personnel de la banque et de l'assurance CGT, à la société Le Crédit Lyonnais et à la ministre du travail.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">66-07 TRAVAIL ET EMPLOI. LICENCIEMENTS. - PSE - 1) PROJET DE RÉORGANISATION ET DE COMPRESSION D'EFFECTIFS N'ABOUTISSANT PAR LUI-MÊME À AUCUNE SUPPRESSION D'EMPLOIS - OBLIGATION D'ÉLABORER UN PSE - EXISTENCE, DÈS LORS QU'IL PRÉVOIT LE LICENCIEMENT DE SALARIÉS REFUSANT LA MODIFICATION DE LEUR CONTRAT DE TRAVAIL [RJ1] - 2) PSE FAISANT L'OBJET D'UN ACCORD COLLECTIF MAJORITAIRE ET D'UNE DÉCISION UNILATÉRALE DE L'EMPLOYEUR - A) MENTION DANS LA DÉCISION UNILATÉRALE DES STIPULATIONS OBLIGATOIRES QUI NE SONT PAS DANS L'ACCORD COLLECTIF - OBLIGATION - EXISTENCE - CONSÉQUENCE - REFUS D'HOMOLOGATION DE LA DÉCISION UNILATÉRALE EN CAS D'OMISSION [RJ2] - B) MENTION DES STIPULATIONS RELATIVES AUX CRITÈRES D'ORDRE DES LICENCIEMENTS - HYPOTHÈSE DANS LAQUELLE SOIT AUCUN EMPLOI N'EST SUPPRIMÉ, SOIT L'EMPLOYEUR, APRÈS AVOIR PROCÉDÉ AUX LICENCIEMENTS CONSÉCUTIFS À DES SUPPRESSIONS D'EMPLOI, ENVISAGE DE PROPOSER À SES SALARIÉS UNE MODIFICATION DE LEURS CONTRAT DE TRAVAIL ET NE PRÉVOIT LEUR LICENCIEMENT QU'À RAISON DE LEUR REFUS - OBLIGATION - ABSENCE [RJ3].
</SCT>
<ANA ID="9A"> 66-07 1) Alors même que le projet de réorganisation et de compression d'effectifs d'une entreprise n'aboutit par lui-même, une fois tenu compte des départs volontaires en retraite et des autres départs volontaires, à aucune suppression d'emploi, il doit, dès lors qu'il prévoit en revanche le licenciement des salariés refusant la modification de leur contrat de travail, faire l'objet d'un plan de sauvegarde de l'emploi (PSE), soumis à l'homologation ou à la validation de l'autorité administrative.,,,2) a) Il résulte des articles L. 1233-5, L. 1233-24-2 et L. 1233-57-3 du code du travail que, lorsqu'un PSE fait l'objet, pour certaines de ses dispositions, d'un accord collectif majoritaire et, pour le reste de ses dispositions, d'une décision unilatérale de l'employeur, l'administration ne peut homologuer cette dernière qu'après avoir vérifié que l'ensemble des points mentionnés aux 1° à 5° de l'article L.1233-24-2, notamment la pondération et le périmètre d'application des critères d'ordre des licenciements, figurent, soit dans l'accord collectif déjà validé par elle ou en cours d'examen devant elle, soit dans le document unilatéral soumis à son homologation.,,b) Toutefois, les critères d'ordre prévus par l'article L.1233-5 du code du travail se trouvent privés d'objet lorsque l'employeur, soit en l'absence de toute suppression d'emploi, soit après avoir procédé aux licenciements consécutifs à des suppressions d'emploi en respectant ces critères d'ordre, envisage seulement de proposer à des salariés une modification de leur contrat de travail et ne prévoit leur licenciement qu'à raison de leur refus. Par suite, dans ce cas, la circonstance que le PSE ne comporte pas la pondération des critères d'ordre et la définition de leur périmètre d'application ne fait pas légalement obstacle à ce que l'administration homologue le document unilatéral relatif à ce plan. Le cas échéant, il lui appartient toutefois de s'assurer, sous le contrôle du juge de l'excès de pouvoir, de la légalité des règles auxquelles ce document aurait décidé de soumettre les propositions de modification de contrat de travail envisagées par le plan.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr. Cass. soc., 3 décembre 1996, n° 95-17.352 et n° 95-20.360, Bull. 1996 V, n° 411., ,[RJ2] Rappr. CE, 7 février 2018, Société Polymont It Services et SCP Laureau-Jeannerot et ministre du travail, n°s 403989 404077, p. 31., ,[RJ3] Rappr. Cass. soc., 27 mars 2012, n° 11-14.223, Bull. 2012 V, n° 108 ; Cass. soc., 1er juin 2017, n° 16-154.56, Bull. 2017 V, n° 97.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
