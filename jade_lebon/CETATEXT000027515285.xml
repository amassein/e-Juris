<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027515285</ID>
<ANCIEN_ID>JG_L_2013_06_000000366219</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/51/52/CETATEXT000027515285.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème SSR, 05/06/2013, 366219</TITRE>
<DATE_DEC>2013-06-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>366219</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>Mme Laurence Marion</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Bertrand Dacosta</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2013:366219.20130605</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi du ministre de l'intérieur, enregistré le 20 février 2013 au secrétariat du contentieux du Conseil d'Etat ; le ministre  demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 11VE03720 du 6 décembre 2012 par lequel la cour administrative d'appel de Versailles a, en premier lieu, rejeté la requête du préfet de l'Essonne tendant à l'annulation du jugement n° 1102726 du 6 octobre 2011 par lequel le tribunal administratif de Versailles a, à la demande de Mme A...B...veuveC..., annulé pour excès de pouvoir l'arrêté du 4 mai 2011 lui refusant un titre de séjour, l'obligeant à quitter le territoire français et fixant le pays à destination duquel elle serait renvoyée et, en second lieu, enjoint au préfet de l'Essonne de délivrer un titre de séjour mention " vie privée et familiale " à Mme B...veuve C...dans le délai d'un mois à compter de la notification de l'arrêt ; <br/>
<br/>
              2°) réglant au fond, de faire droit à l'appel du préfet ;  <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
<br/>
              Vu le code de l'entrée et du séjour des étrangers et du droit d'asile ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Laurence Marion, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de M. Bertrand Dacosta, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de Mme C...;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 313-11 du code de l'entrée et du séjour des étrangers et du droit d'asile, dans sa rédaction en vigueur à la date de la décision attaquée : " Sauf si sa présence constitue une menace pour l'ordre public, la carte de séjour temporaire portant la mention " vie privée et familiale " est délivrée de plein droit : 7° A l'étranger ne vivant pas en état de polygamie, qui n'entre pas dans les catégories précédentes ou dans celles qui ouvrent droit au regroupement familial, dont les liens personnels et familiaux en France, appréciés notamment au regard de leur intensité, de leur ancienneté et de leur stabilité, des conditions d'existence de l'intéressé, de son insertion dans la société française ainsi que de la nature de ses liens avec la famille restée dans le pays d'origine, sont tels que le refus d'autoriser son séjour porterait à son droit au respect de sa vie privée et familiale une atteinte disproportionnée au regard des motifs du refus, sans que la condition prévue à l'article L. 311-7 soit exigée. L'insertion de l'étranger dans la société française est évaluée en tenant compte notamment de sa connaissance des valeurs de la République " ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que le préfet de l'Essonne a, par un arrêté du 4 mai 2011, rejeté la demande de titre de séjour présentée sur le fondement des dispositions du code de l'entrée et du séjour des étrangers et du droit d'asile citées ci-dessus par MmeC..., de nationalité rwandaise, au motif que sa présence en France constituait une menace pour l'ordre public et qu'il n'était pas porté à son droit au respect de sa vie privée et familiale une atteinte disproportionnée ; que le tribunal administratif de Versailles a annulé pour excès de pouvoir cette décision par un jugement du 6 octobre 2011 ; que, par l'arrêt attaqué, la cour administrative d'appel de Versailles a rejeté l'appel du préfet de l'Essonne contre ce jugement ; <br/>
<br/>
              3. Considérant qu'il appartient en principe à l'autorité administrative de délivrer, lorsqu'elle est saisie d'une demande en ce sens, une carte de séjour temporaire portant la mention " vie privée et familiale " à l'étranger ne vivant pas en état de polygamie qui remplit les conditions prévues par les dispositions précitées du 7° de l'article L. 313-11 du code de l'entrée et du séjour des étrangers et du droit d'asile ; qu'elle ne peut opposer un refus à une telle demande que pour un motif d'ordre public suffisamment grave pour que ce refus ne porte pas une atteinte disproportionnée au droit au respect de la vie privée et familiale du demandeur ; qu'elle peut prendre en compte, sur un tel fondement, le fait qu'un demandeur a été impliqué dans des crimes graves contre les personnes et que sa présence régulière sur le territoire national, eu égard aux principes qu'elle mettrait en cause et à son retentissement, serait de nature à porter atteinte à l'ordre public ; <br/>
<br/>
              4. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que Mme C... a joué un rôle central au sein du régime au pouvoir au Rwanda jusqu'en 1994 ; que ce régime, pendant plusieurs années, a préparé et planifié le génocide perpétré, dans ce pays, d'avril à juin 1994  ; que, par ailleurs, si l'intéressée fait état de la présence en France de plusieurs de ses enfants, ceux-ci sont majeurs et n'assument pas sa charge ; qu'en outre l'intéressée n'est pas dépourvue de liens familiaux et personnels dans d'autres pays que la France ; <br/>
<br/>
              5. Considérant, dès lors, qu'en estimant que le refus opposé à la demande de titre de séjour de Mme C... portait au droit au respect de sa vie privée et familiale une atteinte disproportionnée aux buts de préservation de l'ordre public que ce refus poursuit, la cour administrative d'appel de Versailles a inexactement qualifié les faits qui lui étaient soumis ; que le ministre de l'intérieur est fondé, par suite, à demander l'annulation de l'arrêt attaqué, sans qu'il soit besoin d'examiner les autres moyens du pourvoi ;  <br/>
<br/>
              6. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              7. Considérant, d'une part, qu'en faisant référence aux décisions de l'Office français de protection des réfugiés et apatrides et de la commission des recours des réfugiés refusant de lui accorder le statut de réfugié et à celle du Conseil d'Etat du 16 octobre 2009 rejetant le pourvoi de Mme C... contre ce refus, le préfet de l'Essonne ne s'est pas cru lié par elles dans l'appréciation de la menace à l'ordre public à laquelle il a procédé, mais s'est référé aux faits que ces décisions ont pris en considération ; d'autre part, qu'eu égard aux buts en vue desquels il a été pris, le refus opposé à la demande de Mme C...n'a pas porté à son droit au respect de sa vie privée et familiale une atteinte disproportionnée ;  <br/>
<br/>
              8. Considérant qu'il résulte de ce qui précède que c'est à tort que le tribunal administratif de Versailles s'est fondé, pour annuler l'arrêté du 4 mai 2011 du préfet de l'Essonne, sur l'illégalité de ses motifs ; <br/>
<br/>
              9. Considérant, toutefois, qu'il appartient au Conseil d'Etat, saisi de l'ensemble du litige par l'effet dévolutif de l'appel, d'examiner les autres moyens soulevés par Mme C... devant le tribunal administratif de Versailles ;<br/>
<br/>
              10. Considérant, en premier lieu, que l'arrêté attaqué vise les textes applicables à la demande de Mme C... et comporte les considérations de fait, relatives tant à l'ordre public qu'à la vie privée et familiale de l'intéressée, sur lesquelles le préfet s'est fondé pour rejeter la demande ;  <br/>
<br/>
              11. Considérant, en second lieu, que le moyen tiré de ce que l'arrêté, en tant qu'il comporte obligation de quitter le territoire français, serait illégal par voie de conséquence de l'illégalité du refus de séjour ne peut qu'être écarté ;<br/>
<br/>
              12. Considérant qu'il résulte de ce qui précède que le préfet de l'Essonne est fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Versailles a annulé son arrêté du 4 mai 2011 ;  <br/>
<br/>
              13. Considérant que la présente décision n'implique aucune mesure d'exécution ; que, par suite, les conclusions de Mme C...aux fins d'injonction ne peuvent qu'être rejetées ; qu'il en va de même des conclusions qu'elle présente au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 21 décembre 2012 de la cour administrative d'appel de Versailles et le jugement du 6 octobre 2011 du tribunal administratif de Versailles sont annulés.<br/>
Article 2 : La demande présentée par Mme C...devant le tribunal administratif de Versailles et ses conclusions présentées devant le Conseil d'Etat au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 3 : La présente décision sera notifiée au ministre de l'intérieur et à Mme A...B...veuveC....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">335-01-03-04 ÉTRANGERS. SÉJOUR DES ÉTRANGERS. REFUS DE SÉJOUR. MOTIFS. - POSSIBILITÉ DE NE PAS DÉLIVRER UN TITRE VIE PRIVÉE ET FAMILIALE À L'ÉTRANGER QUI REMPLIT LES CONDITIONS POSÉES PAR LE 7° DE L'ARTICLE L. 313-11 DU CESEDA - EXISTENCE - MOTIF - ORDRE PUBLIC - POSSIBILITÉ À CE TITRE DE PRENDRE EN COMPTE LE FAIT QU'UN DEMANDEUR A ÉTÉ IMPLIQUÉ DANS DES CRIMES GRAVES CONTRE LES PERSONNES  ET QUE SA PRÉSENCE RÉGULIÈRE SUR LE TERRITOIRE NATIONAL, EU ÉGARD AUX PRINCIPES QU'ELLE METTRAIT EN CAUSE ET À SON RETENTISSEMENT, SERAIT DE NATURE À PORTER ATTEINTE À L'ORDRE PUBLIC - EXISTENCE [RJ1].
</SCT>
<ANA ID="9A"> 335-01-03-04 Il appartient en principe à l'autorité administrative de délivrer, lorsqu'elle est saisie d'une demande en ce sens, une carte de séjour temporaire portant la mention  vie privée et familiale  à l'étranger ne vivant pas en état de polygamie qui remplit les conditions prévues par les dispositions du 7° de l'article L. 313-11 du code de l'entrée et du séjour des étrangers (CESEDA). Elle ne peut opposer un refus à une telle demande que pour un motif d'ordre public suffisamment grave pour que ce refus ne porte pas une atteinte disproportionnée au droit au respect de la vie privée et familiale du demandeur. Sur un tel fondement, elle peut prendre en compte le fait qu'un demandeur a été impliqué dans des crimes graves contre les personnes et que sa présence régulière sur le territoire national, eu égard aux principes qu'elle mettrait en cause et à son retentissement, serait de nature à porter atteinte à l'ordre public.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., s'agissant de la possibilité de refuser la délivrance d'un visa pour un tel motif, CE, 3 février 2012, Ministre de l'intérieur, de l'outre-mer, des collectivités territoriales et de l'immigration, n° 353952 353953, p. 16.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
