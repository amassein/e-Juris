<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034808237</ID>
<ANCIEN_ID>JG_L_2017_05_000000397197</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/80/82/CETATEXT000034808237.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 6ème chambres réunies, 24/05/2017, 397197</TITRE>
<DATE_DEC>2017-05-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>397197</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MONOD, COLIN, STOCLET ; RICARD</AVOCATS>
<RAPPORTEUR>M. Frédéric Pacoud</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Jean Lessi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:397197.20170524</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. C...B...a demandé au tribunal administratif de Nantes d'annuler pour excès de pouvoir la décision du 23 janvier 2012 par laquelle le directeur de la société anonyme d'économie mixte locale " Société nazairienne de développement " (SONADEV) a exercé son droit de préemption sur une parcelle lui appartenant, située rue de Bretagne à Saint-André-des-Eaux, cadastrée BS n° 438. Par un jugement n° 1203264 du 24 juin 2014, le tribunal administratif de Nantes a annulé la décision du 23 janvier 2012 et enjoint à la SONADEV de prendre toute mesure pour faire cesser les effets de cette décision dans un délai de deux mois.<br/>
<br/>
              Par un arrêt n° 14NT01840 du 22 décembre 2015, la cour administrative d'appel de Nantes a, sur appel de la SONADEV, annulé ce jugement et rejeté la demande de M. B....<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 22 février et 23 mai 2016 au secrétariat du contentieux du Conseil d'Etat, Mme A...B..., venant aux droits de M. C...B..., son époux décédé, demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt de la cour administrative d'appel de Nantes du 22 décembre 2015 ; <br/>
<br/>
              2°) de mettre à la charge de la SONADEV la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des collectivités territoriales ;<br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Pacoud, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Jean Lessi, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Monod, Colin, Stoclet, avocat de Mme B...et à Me Ricard, avocat de la société anonyme d'économie mixte locale " Société nazairienne de développement ".<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la communauté d'agglomération de la région nazairienne et de l'estuaire a conclu, le 27 octobre 2009, avec la société anonyme d'économie mixte locale " Société nazairienne de développement " (SONADEV) un traité de concession d'aménagement pour la réalisation de la zone d'aménagement concerté " centre bourg " à Saint-André-des-Eaux. Par un arrêté du 22 mars 2010, le préfet de la Loire-Atlantique a créé, sur le territoire de la commune de Saint-André-des-Eaux, la zone d'aménagement différé du " centre-bourg ", qui correspondait à la plus grande partie de la zone d'aménagement concerté, et désigné la SONADEV en qualité de titulaire du droit de préemption urbain. Par une décision du 23 janvier 2012, le directeur de la SONADEV a exercé le droit de préemption sur la parcelle cadastrée BS n° 438, située rue de Bretagne, à Saint-André-des-Eaux et appartenant à M. C... B.... Le tribunal administratif de Nantes a annulé cette décision par un jugement du 24 juin 2014, à la demande de M.B.... Mme A...B..., venant aux droits de M. C...B..., décédé, se pourvoit en cassation contre l'arrêt du 22 décembre 2015 par lequel la cour administrative d'appel de Nantes a annulé le jugement du tribunal administratif de Nantes du 24 juin 2014 et rejeté la demande de première instance de M.B.... <br/>
<br/>
              2. Aux termes de l'article L. 300-4 du code de l'urbanisme : " L'Etat et les collectivités territoriales, ainsi que leurs établissements publics, peuvent concéder la réalisation des opérations d'aménagement prévues par le présent code à toute personne y ayant vocation. / (...) / Le concessionnaire assure la maîtrise d'ouvrage des travaux et équipements concourant à l'opération prévus dans la concession, ainsi que la réalisation des études et de toutes missions nécessaires à leur exécution. Il peut être chargé par le concédant d'acquérir des biens nécessaires à la réalisation de l'opération, y compris, le cas échéant, par la voie d'expropriation ou de préemption. Il procède à la vente, à la location ou à la concession des biens immobiliers situés à l'intérieur du périmètre de la concession ". En vertu des dispositions combinées de l'article L. 212-2 du même code et du II de l'article 6 de la loi du 3 juin 2010 relative au Grand Paris, dans les zones d'aménagement différé, un droit de préemption, qui peut être exercé pendant une période courant jusqu'au 5 juin 2016 en l'espèce, susceptible d'être prolongée de six ans, " est ouvert soit à une collectivité publique ou à un établissement public y ayant vocation, soit au concessionnaire d'une opération d'aménagement. / L'acte créant la zone désigne le titulaire du droit de préemption ". Selon le premier alinéa de l'article L. 2131-1 du code général des collectivités territoriales : " Les actes pris par les autorités communales sont exécutoires de plein droit dès qu'il a été procédé à leur publication ou affichage ou à leur notification aux intéressés ainsi qu'à leur transmission au représentant de l'Etat dans le département ou à son délégué dans l'arrondissement. Pour les décisions individuelles, cette transmission intervient dans un délai de quinze jours à compter de leur signature ". Aux termes de l'article L. 2131-2 du même code : " Sont soumis aux dispositions de l'article L. 2131-1 les actes suivants : (...) / 8° Les décisions relevant de l'exercice de prérogatives de puissance publique, prises par les sociétés d'économie mixte locales pour le compte d'une commune ou d'un établissement public de coopération intercommunale ". <br/>
<br/>
              3. Il résulte des dispositions du 8° de l'article L. 2131-2, du 7° de l'article L. 3131-2 et du 6° de l'article L. 4141-2 du code général des collectivités territoriales, éclairées par les travaux préparatoires de l'article 82 de la loi du 29 janvier 1993 relative à la prévention de la corruption et à la transparence de la vie économique et des procédures publiques duquel elles sont issues, que le législateur a entendu prévoir la transmission au représentant de l'Etat de l'ensemble des décisions relevant de l'exercice de prérogatives de puissance publique prises par les sociétés d'économie mixte locales, en modifiant les dispositions respectivement consacrées à la transmission des actes des communes, des départements et des régions. En précisant qu'il visait ainsi, selon les cas, les décisions prises " pour le compte " d'une commune ou d'un établissement public de coopération intercommunale, d'un département ou d'une institution interdépartementale ou d'une région ou d'un établissement public de coopération interrégionale, le législateur n'a pas entendu poser une condition supplémentaire tenant à la nature des relations contractuelles existant entre la société d'économie mixte locale et la collectivité territoriale mais a distingué les actes visés selon la catégorie de collectivités concernée. Ainsi, les décisions de préemption prises par une société d'économie mixte concessionnaire d'une commune ou d'un établissement public de coopération intercommunale, désignée en qualité de titulaire du droit de préemption par l'acte créant une zone d'aménagement différé, doivent être regardées comme entrant dans le champ d'application du 8° de l'article L. 2131-1 du code général des collectivités territoriales, quelle que soit la nature des relations contractuelles liant la société d'économie mixte à la commune ou à l'établissement public de coopération intercommunale pour la réalisation de l'opération d'aménagement.<br/>
<br/>
              4. Il ressort des pièces du dossier soumis aux juges du fond que, par la décision litigieuse du 23 janvier 2012, la SONADEV, à laquelle la communauté d'agglomération de la région nazairienne et de l'estuaire avait concédé la réalisation de la zone d'aménagement concerté " centre bourg " à Saint-André-des-Eaux, a exercé sur la parcelle appartenant à M. et Mme B...le droit de préemption qui lui a été ouvert par l'arrêté préfectoral du 22 mars 2010 sur une partie de cette zone. Il résulte de ce qui a été dit au point 3 ci-dessus qu'une telle décision, qui relève de la mise en oeuvre d'une prérogative de puissance publique, devait être transmise au représentant de l'Etat en application du 8° de l'article L. 2131-2 du code général des collectivités territoriales. Par suite, en retenant, pour juger que la décision de préemption litigieuse n'avait pas à être transmise au représentant de l'Etat en application de ces dispositions, que la SONADEV ne pouvait être regardée comme ayant exercé le droit de préemption ni pour le compte de la communauté d'agglomération de la région nazairienne et de l'estuaire ni pour celui de la commune de Saint-André-des-Eaux, au motif que la concession d'aménagement conclue le 27 octobre 2009 n'avait pas le caractère d'un mandat donné par la personne publique à l'aménageur, la cour administrative d'appel de Nantes a commis une erreur de droit.  <br/>
<br/>
              5. Il résulte de ce qui précède, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que Mme B...est fondée à demander l'annulation de l'arrêt de la cour administrative d'appel de Nantes qu'elle attaque. <br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la SONADEV une somme de 3 000 euros à verser à Mme B...au titre des dispositions de l'article L. 761-1 du code de justice administrative. Ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de MmeB..., qui n'est pas, dans la présente instance, la partie perdante. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nantes du 22 décembre 2015 est annulé. <br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nantes. <br/>
Article 3 : La SONADEV versera à Mme B...une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions de la SONADEV présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à Mme A...B...et à la société anonyme d'économie mixte locale " Société nazairienne de développement ".<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">135-01-015 COLLECTIVITÉS TERRITORIALES. DISPOSITIONS GÉNÉRALES. CONTRÔLE DE LA LÉGALITÉ DES ACTES DES AUTORITÉS LOCALES. - CHAMP DU CONTRÔLE DE LÉGALITÉ DU PRÉFET - DÉCISIONS RELEVANT DE L'EXERCICE DE PRÉROGATIVES DE PUISSANCE PUBLIQUE, PRISES PAR LES SEM LOCALES POUR LE COMPTE D'UNE COLLECTIVITÉ TERRITORIALE OU L'UN DE SES ÉTABLISSEMENTS - NOTION - EXIGENCE DE RELATIONS CONTRACTUELLES ENTRE LA SEM ET LA COLLECTIVITÉ TERRITORIALE - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">68-02-01-01-02 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PROCÉDURES D'INTERVENTION FONCIÈRE. PRÉEMPTION ET RÉSERVES FONCIÈRES. DROITS DE PRÉEMPTION. ZONES D'AMÉNAGEMENT DIFFÉRÉ. - DÉCISION DE PRÉEMPTION PRISE PAR UNE SEM CONCESSIONNAIRE D'UNE COMMUNE OU D'UN EPCI, DÉSIGNÉE EN QUALITÉ DE TITULAIRE DU DROIT DE PRÉEMPTION PAR L'ACTE CRÉANT UNE ZONE D'AMÉNAGEMENT DIFFÉRÉ - DÉCISION PRISE POUR LE COMPTE DE LA COMMUNE OU DE L'EPCI (8° DE L'ARTICLE L. 2131-1 DU CGCT) - EXISTENCE, QUELLE QUE SOIT LA NATURE DES RELATIONS CONTRACTUELLES LIANT LA SEM À LA COMMUNE OU À L'EPCI - CONSÉQUENCE - INCLUSION DANS LE CHAMP DU CONTRÔLE DE LÉGALITÉ DU PRÉFET.
</SCT>
<ANA ID="9A"> 135-01-015 Il résulte des dispositions du 8° de l'article L. 2131-2, du 7° de l'article L. 3131-2 et du 6° de l'article L. 4141-2 du code général des collectivités territoriales (CGCT), éclairées par leurs travaux préparatoires, que le législateur a entendu prévoir la transmission au représentant de l'Etat de l'ensemble des décisions relevant de l'exercice de prérogatives de puissance publique prises par les sociétés d'économie mixte (SEM) locales, en modifiant les dispositions respectivement consacrées à la transmission des actes des communes, des départements et des régions. En précisant qu'il visait ainsi, selon les cas, les décisions prises pour le compte d'une commune ou d'un établissement public de coopération intercommunale, d'un département ou d'une institution interdépartementale ou d'une région ou d'un établissement public de coopération interrégionale, le législateur n'a pas entendu poser une condition supplémentaire tenant à la nature des relations contractuelles existant entre la SEM locale et la collectivité territoriale mais a distingué les actes visés selon la catégorie de collectivité concernée. Ainsi, les décisions de préemption prises par une SEM concessionnaire d'une commune ou d'un établissement public de coopération intercommunale, désignée en qualité de titulaire du droit de préemption par l'acte créant une zone d'aménagement différé, doivent être regardées comme entrant dans le champ d'application du 8° de l'article L. 2131-1 du CGCT, quelle que soit la nature des relations contractuelles liant la SEM à la commune ou à l'établissement public de coopération intercommunale pour la réalisation de l'opération d'aménagement.</ANA>
<ANA ID="9B"> 68-02-01-01-02 Il résulte des dispositions du 8° de l'article L. 2131-2, du 7° de l'article L. 3131-2 et du 6° de l'article L. 4141-2 du code général des collectivités territoriales (CGCT), éclairées par leurs travaux préparatoires, que le législateur a entendu prévoir la transmission au représentant de l'Etat de l'ensemble des décisions relevant de l'exercice de prérogatives de puissance publique prises par les sociétés d'économie mixte (SEM) locales, en modifiant les dispositions respectivement consacrées à la transmission des actes des communes, des départements et des régions. En précisant qu'il visait ainsi, selon les cas, les décisions prises pour le compte d'une commune ou d'un établissement public de coopération intercommunale, d'un département ou d'une institution interdépartementale ou d'une région ou d'un établissement public de coopération interrégionale, le législateur n'a pas entendu poser une condition supplémentaire tenant à la nature des relations contractuelles existant entre la SEM locale et la collectivité territoriale mais a distingué les actes visés selon la catégorie de collectivité concernée. Ainsi, les décisions de préemption prises par une SEM concessionnaire d'une commune ou d'un établissement public de coopération intercommunale, désignée en qualité de titulaire du droit de préemption par l'acte créant une zone d'aménagement différé, doivent être regardées comme entrant dans le champ d'application du 8° de l'article L. 2131-1 du CGCT, quelle que soit la nature des relations contractuelles liant la SEM à la commune ou à l'établissement public de coopération intercommunale pour la réalisation de l'opération d'aménagement.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
