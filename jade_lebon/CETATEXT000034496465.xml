<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034496465</ID>
<ANCIEN_ID>JG_L_2017_04_000000406009</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/49/64/CETATEXT000034496465.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 4ème chambres réunies, 26/04/2017, 406009, Publié au recueil Lebon</TITRE>
<DATE_DEC>2017-04-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>406009</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 4ème chambres réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Florian Roussel</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CECHR:2017:406009.20170426</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par un arrêt n°14BX03684 du 15 décembre 2016, la cour administrative d'appel de Bordeaux, avant de statuer sur l'appel du ministre de l'intérieur contre le jugement n° 1300677 du 16 octobre 2014 du tribunal administratif de Pau condamnant l'Etat à verser à M. A... une indemnité représentative des congés annuels non pris correspondant aux périodes où 1'intéressé a été placé en congé de maladie ordinaire, en 2011 et 2012, a décidé, par application des dispositions de l'article L. 113-1 du code de justice administrative, de surseoir à statuer et de transmettre pour avis le dossier de cette affaire au Conseil d'Etat, en soumettant à son examen les questions suivantes : <br/>
<br/>
              1°) En l'absence de règlementation nationale compatible avec les principes fixés par le droit européen, un agent public qui n'a pu prendre de congés annuels pour cause de maladie peut-il revendiquer un droit de report de ses congés annuels sans limitation temporelle, ou appartient-il au juge d'apprécier, afin de préserver la finalité des congés annuels payés, le délai pendant lequel les congés non pris pour cause de maladie sont reportables et, en cas de fin de relation de travail, indemnisables par l'administration ' <br/>
<br/>
              2°) Dans le cas où cette dernière hypothèse serait retenue, pour fixer la période de report " dépassant substantiellement la durée de la période de référence " comme indiqué par la Cour de justice de l'Union européenne dans son arrêt C-214/10 du 22 novembre 2011, le juge doit-il faire une appréciation au cas par cas, ou bien retenir comme règle une période de quinze mois dès lors que ce choix d'un autre pays a été reconnu pertinent par la décision précitée de la Cour de justice de l'Union européenne, ou bien encore fixer cette limite en s'inspirant du délai de dix-huit mois prévu par la convention n° 132 de l'OIT du 24 juin 1970, concernant le droit à des congés payés annuels, dès lors que la directive 2003/88/CE indique avoir tenu compte des principes de l'OIT en matière d'aménagement du temps de travail, ou de toute autre norme ' <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
<br/>
              - la directive 2003/88/CE du Parlement européen et du Conseil du 4 novembre 2003 ;<br/>
<br/>
              - l'arrêt C-214/10 du 22 novembre 2011 de la Cour de justice de l'Union européenne ; <br/>
<br/>
              - la loi n° 84-16 du 11 janvier 1984 ;<br/>
<br/>
              - le décret n° 84-972 du 26 octobre 1984 ;<br/>
<br/>
              - Vu le code de justice administrative ;<br/>
<br/>
<br/>
               Après avoir entendu en séance publique :<br/>
<br/>
- le rapport de M. Florian Roussel, maître des requêtes, <br/>
- les conclusions de M. Nicolas Polge, rapporteur public.<br/>
REND L'AVIS SUIVANT<br/>
<br/>
<br/>
<br/>
              1. Aux termes de l'article 7 de la directive 2003/88/CE du Parlement européen et du Conseil du 4 novembre 2003 relative à certains aspects de l'aménagement du temps de travail : " 1. Les États membres prennent les mesures nécessaires pour que tout travailleur bénéficie d'un congé annuel payé d'au moins quatre semaines, conformément aux conditions d'obtention et d'octroi prévues par les législations et/ou pratiques nationales / 2. La période minimale de congé annuel payé ne peut être remplacée par une indemnité financière, sauf en cas de fin de relation de travail ". Selon la jurisprudence de la Cour de justice de l'Union européenne, ces dispositions font obstacle à ce que le droit au congé annuel payé qu'un travailleur n'a pas pu exercer pendant une certaine période parce qu'il était placé en congé de maladie pendant tout ou partie de cette période s'éteigne à l'expiration de celle-ci. Le droit au report des congés annuels non exercés pour ce motif n'est toutefois pas illimité dans le temps. Si, selon la Cour, la durée de la période de report doit dépasser substantiellement celle de la période au cours de laquelle le droit peut être exercé, pour permettre à l'agent d'exercer effectivement son droit à congé sans perturber le fonctionnement du service, la finalité même du droit au congé annuel payé, qui est de bénéficier d'un temps de repos ainsi que d'un temps de détente et de loisirs, s'oppose à ce qu'un travailleur en incapacité de travail durant plusieurs années consécutives, puisse avoir le droit de cumuler de manière illimitée des droits au congé annuel payé acquis durant cette période.<br/>
<br/>
              2. Aux termes de l'article 1er du décret du 26 octobre 1984 relatif aux congés annuels des fonctionnaires de l'Etat visé ci-dessus : " Tout fonctionnaire de l'Etat en activité a droit, dans les conditions et sous les réserves précisées aux articles ci-après, pour une année de service accompli du 1er janvier au 31 décembre, à un congé annuel d'une durée égale à cinq fois ses obligations hebdomadaires de service ". Aux termes de l'article 5 du même décret : " Le congé dû pour une année de service accompli ne peut se reporter sur l'année suivante, sauf autorisation exceptionnelle donnée par le chef de service./ Un congé non pris ne donne lieu à aucune indemnité compensatrice ". Ces dispositions réglementaires, qui ne prévoient le report des congés non pris au cours d'une année de service qu'à titre exceptionnel, sans réserver le cas des agents qui ont été dans l'impossibilité de prendre leurs congés annuels en raison d'un congé de maladie, sont, dans cette mesure, incompatibles avec les dispositions de l'article 7 de la directive citée au point 1 et, par suite, illégales. <br/>
<br/>
              3. En l'absence de dispositions législatives ou réglementaires fixant ainsi une période de report des congés payés qu'un agent s'est trouvé, du fait d'un congé maladie, dans l'impossibilité de prendre au cours d'une année civile donnée, le juge peut en principe considérer, afin d'assurer le respect des dispositions de la directive 2003/88/CE du 4 novembre 2003, que ces congés peuvent être pris au cours d'une période de quinze mois après le terme de cette année. La Cour de justice de l'Union européenne a en effet jugé, dans son arrêt C-214/10 du 22 novembre 2011, qu'une telle durée de quinze mois, substantiellement supérieure à la durée de la période annuelle au cours de laquelle le droit peut être exercé, est compatible avec les dispositions de l'article 7 de la directive. Toutefois ce droit au report s'exerce, en l'absence de dispositions, sur ce point également, dans le droit national, dans la limite de quatre semaines prévue par cet article 7.<br/>
<br/>
              Le présent avis sera notifié à la cour administrative d'appel de Bordeaux, au ministre de l'intérieur et à M.A.... <br/>
<br/>
              Il sera publié au Journal officiel de la République française.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">15-02 COMMUNAUTÉS EUROPÉENNES ET UNION EUROPÉENNE. PORTÉE DES RÈGLES DU DROIT DE L'UNION EUROPÉENNE. - DISPOSITIONS RÉGLEMENTAIRES INCOMPATIBLES AVEC L'ARTICLE 7 DE LA DIRECTIVE 2003/88 DONT RÉSULTE UN DROIT À CONGÉ ANNUEL EN CAS DE CONGÉ DE MALADIE [RJ1] - CONSÉQUENCE - ABSENCE DE TEXTE APPLICABLE AU REPORT DES CONGÉS DES AGENTS AYANT ÉTÉ DANS L'IMPOSSIBILITÉ DE PRENDRE LEURS CONGÉS ANNUELS EN RAISON D'UN CONGÉ DE MALADIE - DÉDUCTION DE LA JURISPRUDENCE DE LA CJUE DES PRINCIPES APPLICABLES EN L'ABSENCE DE TEXTE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">15-05-17 COMMUNAUTÉS EUROPÉENNES ET UNION EUROPÉENNE. RÈGLES APPLICABLES. POLITIQUE SOCIALE. - DROIT À CONGÉ ANNUEL EN CAS DE CONGÉ DE MALADIE (ART. 7 DE LA DIRECTIVE 2003/88) - 1) PRINCIPE - 2) ILLÉGALITÉ DE L'ARTICLE 5 DU DÉCRET DU 26 OCTOBRE 1984 NE PRÉVOYANT PAS LE REPORT DES CONGÉS DES AGENTS QUI ONT ÉTÉ DANS L'IMPOSSIBILITÉ DE PRENDRE LEURS CONGÉS ANNUELS EN RAISON D'UN CONGÉ DE MALADIE [RJ1] - 3) CONSÉQUENCE - PRINCIPES APPLICABLES EN L'ABSENCE DE TEXTE - POSSIBILITÉ DE REPORT PENDANT QUINZE MOIS APRÈS LE TERME D'UNE ANNÉE CIVILE DES CONGÉS NON PRIS, DANS LA LIMITE DE QUATRE SEMAINES, AU COURS DE CETTE ANNÉE CIVILE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">36-05-04-01 FONCTIONNAIRES ET AGENTS PUBLICS. POSITIONS. CONGÉS. CONGÉS DE MALADIE. - DROIT À CONGÉ ANNUEL EN CAS DE CONGÉ DE MALADIE (ART. 7 DE LA DIRECTIVE 2003/88) - 1) PRINCIPE - 2) ILLÉGALITÉ DE L'ARTICLE 5 DU DÉCRET DU 26 OCTOBRE 1984 NE PRÉVOYANT PAS LE REPORT DES CONGÉS DES AGENTS QUI ONT ÉTÉ DANS L'IMPOSSIBILITÉ DE PRENDRE LEURS CONGÉS ANNUELS EN RAISON D'UN CONGÉ DE MALADIE [RJ1] - 3) CONSÉQUENCE - PRINCIPES APPLICABLES EN L'ABSENCE DE TEXTE - POSSIBILITÉ DE REPORT PENDANT QUINZE MOIS APRÈS LE TERME D'UNE ANNÉE CIVILE DES CONGÉS NON PRIS, DANS LA LIMITE DE QUATRE SEMAINES, AU COURS DE CETTE ANNÉE CIVILE.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">36-05-04-03 FONCTIONNAIRES ET AGENTS PUBLICS. POSITIONS. CONGÉS. CONGÉS ANNUELS. - DROIT À CONGÉ ANNUEL EN CAS DE CONGÉ DE MALADIE (ART. 7 DE LA DIRECTIVE 2003/88) - 1) PRINCIPE - 2) ILLÉGALITÉ DE L'ARTICLE 5 DU DÉCRET DU 26 OCTOBRE 1984 NE PRÉVOYANT PAS LE REPORT DES CONGÉS DES AGENTS QUI ONT ÉTÉ DANS L'IMPOSSIBILITÉ DE PRENDRE LEURS CONGÉS ANNUELS EN RAISON D'UN CONGÉ DE MALADIE [RJ1] - 3) CONSÉQUENCE - PRINCIPES APPLICABLES EN L'ABSENCE DE TEXTE - POSSIBILITÉ DE REPORT PENDANT QUINZE MOIS APRÈS LE TERME D'UNE ANNÉE CIVILE DES CONGÉS NON PRIS, DANS LA LIMITE DE QUATRE SEMAINES, AU COURS DE CETTE ANNÉE CIVILE.
</SCT>
<ANA ID="9A"> 15-02 Selon la jurisprudence de la Cour de justice de l'Union européenne (CJUE), l'article 7 de la directive 2003/88/CE du 4 novembre 2003 fait obstacle à ce que le droit au congé annuel payé qu'un travailleur n'a pas pu exercer pendant une certaine période parce qu'il était placé en congé de maladie pendant tout ou partie de cette période s'éteigne à l'expiration de celle-ci. Le droit au report des congés annuels non exercés pour ce motif n'est toutefois pas illimité dans le temps. Si, selon la Cour, la durée de la période de report doit dépasser substantiellement celle de la période au cours de laquelle le droit peut être exercé, pour permettre à l'agent d'exercer effectivement son droit à congé sans perturber le fonctionnement du service, la finalité même du droit au congé annuel payé, qui est de bénéficier d'un temps de repos ainsi que d'un temps de détente et de loisirs, s'oppose à ce qu'un travailleur en incapacité de travail durant plusieurs années consécutives, puisse avoir le droit de cumuler de manière illimitée des droits au congé annuel payé acquis durant cette période.,,,Les dispositions de l'article 5 du décret n° 84-972, qui ne prévoient le report des congés non pris au cours d'une année de service qu'à titre exceptionnel, sans réserver le cas des agents qui ont été dans l'impossibilité de prendre leurs congés annuels en raison d'un congé de maladie, sont, dans cette mesure, incompatibles avec les dispositions de l'article 7 de la directive et, par suite, illégales.,,,En l'absence de dispositions législatives ou réglementaires fixant ainsi une période de report des congés payés qu'un agent s'est trouvé, du fait d'un congé maladie, dans l'impossibilité de prendre au cours d'une année civile donnée, le juge peut en principe considérer, afin d'assurer le respect des dispositions de la directive 2003/88/CE du 4 novembre 2003, que ces congés peuvent être pris au cours d'une période de quinze mois après le terme de cette année. La CJUE a en effet jugé, dans son arrêt C-214/10 du 22 novembre 2011, qu'une telle durée de quinze mois, substantiellement supérieure à la durée de la période annuelle au cours de laquelle le droit peut être exercé, est compatible avec les dispositions de l'article 7 de la directive. Toutefois ce droit au report s'exerce, en l'absence de dispositions, sur ce point également, dans le droit national, dans la limite de quatre semaines prévue par cet article 7.</ANA>
<ANA ID="9B"> 15-05-17 1) Selon la jurisprudence de la Cour de justice de l'Union européenne, l'article 7 de la directive 2003/88/CE du 4 novembre 2003 fait obstacle à ce que le droit au congé annuel payé qu'un travailleur n'a pas pu exercer pendant une certaine période parce qu'il était placé en congé de maladie pendant tout ou partie de cette période s'éteigne à l'expiration de celle-ci. Le droit au report des congés annuels non exercés pour ce motif n'est toutefois pas illimité dans le temps. Si, selon la Cour, la durée de la période de report doit dépasser substantiellement celle de la période au cours de laquelle le droit peut être exercé, pour permettre à l'agent d'exercer effectivement son droit à congé sans perturber le fonctionnement du service, la finalité même du droit au congé annuel payé, qui est de bénéficier d'un temps de repos ainsi que d'un temps de détente et de loisirs, s'oppose à ce qu'un travailleur en incapacité de travail durant plusieurs années consécutives, puisse avoir le droit de cumuler de manière illimitée des droits au congé annuel payé acquis durant cette période.,,,2) Les dispositions de l'article 5 du décret n° 84-972, qui ne prévoient le report des congés non pris au cours d'une année de service qu'à titre exceptionnel, sans réserver le cas des agents qui ont été dans l'impossibilité de prendre leurs congés annuels en raison d'un congé de maladie, sont, dans cette mesure, incompatibles avec les dispositions de l'article 7 de la directive et, par suite, illégales.,,,3) En l'absence de dispositions législatives ou réglementaires fixant ainsi une période de report des congés payés qu'un agent s'est trouvé, du fait d'un congé maladie, dans l'impossibilité de prendre au cours d'une année civile donnée, le juge peut en principe considérer, afin d'assurer le respect des dispositions de la directive 2003/88/CE du 4 novembre 2003, que ces congés peuvent être pris au cours d'une période de quinze mois après le terme de cette année. Toutefois ce droit au report s'exerce, en l'absence de dispositions, sur ce point également, dans le droit national, dans la limite de quatre semaines prévue par cet article 7.</ANA>
<ANA ID="9C"> 36-05-04-01 1) Selon la jurisprudence de la Cour de justice de l'Union européenne, l'article 7 de la directive 2003/88/CE du 4 novembre 2003 fait obstacle à ce que le droit au congé annuel payé qu'un travailleur n'a pas pu exercer pendant une certaine période parce qu'il était placé en congé de maladie pendant tout ou partie de cette période s'éteigne à l'expiration de celle-ci. Le droit au report des congés annuels non exercés pour ce motif n'est toutefois pas illimité dans le temps. Si, selon la Cour, la durée de la période de report doit dépasser substantiellement celle de la période au cours de laquelle le droit peut être exercé, pour permettre à l'agent d'exercer effectivement son droit à congé sans perturber le fonctionnement du service, la finalité même du droit au congé annuel payé, qui est de bénéficier d'un temps de repos ainsi que d'un temps de détente et de loisirs, s'oppose à ce qu'un travailleur en incapacité de travail durant plusieurs années consécutives, puisse avoir le droit de cumuler de manière illimitée des droits au congé annuel payé acquis durant cette période.,,,2) Les dispositions de l'article 5 du décret n° 84-972, qui ne prévoient le report des congés non pris au cours d'une année de service qu'à titre exceptionnel, sans réserver le cas des agents qui ont été dans l'impossibilité de prendre leurs congés annuels en raison d'un congé de maladie, sont, dans cette mesure, incompatibles avec les dispositions de l'article 7 de la directive et, par suite, illégales.,,,3) En l'absence de dispositions législatives ou réglementaires fixant ainsi une période de report des congés payés qu'un agent s'est trouvé, du fait d'un congé maladie, dans l'impossibilité de prendre au cours d'une année civile donnée, le juge peut en principe considérer, afin d'assurer le respect des dispositions de la directive 2003/88/CE du 4 novembre 2003, que ces congés peuvent être pris au cours d'une période de quinze mois après le terme de cette année. Toutefois ce droit au report s'exerce, en l'absence de dispositions, sur ce point également, dans le droit national, dans la limite de quatre semaines prévue par cet article 7.</ANA>
<ANA ID="9D"> 36-05-04-03 1) Selon la jurisprudence de la Cour de justice de l'Union européenne, l'article 7 de la directive 2003/88/CE du 4 novembre 2003 fait obstacle à ce que le droit au congé annuel payé qu'un travailleur n'a pas pu exercer pendant une certaine période parce qu'il était placé en congé de maladie pendant tout ou partie de cette période s'éteigne à l'expiration de celle-ci. Le droit au report des congés annuels non exercés pour ce motif n'est toutefois pas illimité dans le temps. Si, selon la Cour, la durée de la période de report doit dépasser substantiellement celle de la période au cours de laquelle le droit peut être exercé, pour permettre à l'agent d'exercer effectivement son droit à congé sans perturber le fonctionnement du service, la finalité même du droit au congé annuel payé, qui est de bénéficier d'un temps de repos ainsi que d'un temps de détente et de loisirs, s'oppose à ce qu'un travailleur en incapacité de travail durant plusieurs années consécutives, puisse avoir le droit de cumuler de manière illimitée des droits au congé annuel payé acquis durant cette période.,,,2) Les dispositions de l'article 5 du décret n° 84-972, qui ne prévoient le report des congés non pris au cours d'une année de service qu'à titre exceptionnel, sans réserver le cas des agents qui ont été dans l'impossibilité de prendre leurs congés annuels en raison d'un congé de maladie, sont, dans cette mesure, incompatibles avec les dispositions de l'article 7 de la directive et, par suite, illégales.,,,3) En l'absence de dispositions législatives ou réglementaires fixant ainsi une période de report des congés payés qu'un agent s'est trouvé, du fait d'un congé maladie, dans l'impossibilité de prendre au cours d'une année civile donnée, le juge peut en principe considérer, afin d'assurer le respect des dispositions de la directive 2003/88/CE du 4 novembre 2003, que ces congés peuvent être pris au cours d'une période de quinze mois après le terme de cette année. Toutefois ce droit au report s'exerce, en l'absence de dispositions, sur ce point également, dans le droit national, dans la limite de quatre semaines prévue par cet article 7.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Cf. CE, 26 octobre 2012,,, n° 346648, T. pp. 631-634-808-808-903.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
