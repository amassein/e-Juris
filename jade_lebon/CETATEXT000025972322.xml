<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025972322</ID>
<ANCIEN_ID>JG_L_2012_06_000000356505</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/97/23/CETATEXT000025972322.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème et 7ème sous-sections réunies, 04/06/2012, 356505</TITRE>
<DATE_DEC>2012-06-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>356505</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème et 7ème sous-sections réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Isabelle de Silva</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CESSR:2012:356505.20120604</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le jugement n° 1106837 du 26 janvier 2012, enregistré le 6 février 2012 au secrétariat du contentieux du Conseil d'Etat, par lequel le tribunal administratif de Marseille, avant de statuer sur la demande de M. A...B...tendant, d'une part, à l'annulation pour excès de pouvoir de l'arrêté du 8 juin 2011 par lequel le préfet des Bouches-du-Rhône lui a refusé la délivrance d'un titre de séjour et fait obligation de quitter le territoire français, d'autre part, à ce qu'il soit ordonné au préfet de lui délivrer un titre de séjour portant mention "vie privée et familiale" dans un délai de deux mois, ou, à défaut, d'instruire à nouveau sa demande et, dans l'attente de la nouvelle décision, de lui délivrer, dans un délai de huit jours, une autorisation provisoire de séjour l'autorisant à travailler, a décidé, par application de l'article L. 113-1 du code de justice administrative, de transmettre le dossier de cette demande au Conseil d'Etat en soumettant à son examen la question de savoir "s'il convient de donner de l'article L. 511-1 du code de l'entrée et du séjour des étrangers et du droit d'asile une interprétation conforme à l'article 6 (...) de la directive du 16 décembre 2008, aux termes de laquelle le préfet, étant privé du pouvoir de ne pas assortir la décision de refus de séjour qu'il prend d'une obligation de quitter le territoire français hormis les cas limitativement énumérés par les paragraphes 2 à 5 de l'article 6 de ladite directive, ne saurait se voir imputer une erreur manifeste d'appréciation eu égard aux conséquences d'une exceptionnelle gravité que comporterait, pour l'intéressé, l'obligation de quitter le territoire français dont est assortie la décision de refus d'admission au séjour " ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la directive 2008/115/CE du Parlement européen et du Conseil, du 16 décembre 2008, notamment son article 6 ;<br/>
<br/>
              Vu le code de l'entrée et du séjour des étrangers et du droit d'asile, notamment son article L. 511-1 ;<br/>
<br/>
              Vu le code de justice administrative, notamment son article L. 113-1 :<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de  Mme Isabelle de Silva,<br/>
<br/>
              - les conclusions de M. Damien Botteghi ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              REND L'AVIS SUIVANT<br/>
<br/>
<br/>
I.                         Aux termes de l'article L. 511-1 du code de l'entrée et du séjour des étrangers et du droit d'asile, dans sa rédaction applicable au litige soumis au tribunal administratif : " I. - L'autorité administrative qui refuse la délivrance ou le renouvellement d'un titre de séjour à un étranger ou qui lui retire son titre de séjour (...) pour un motif autre que l'existence d'une menace à l'ordre public, peut assortir sa décision d'une obligation de quitter le territoire français, laquelle fixe le pays à destination duquel l'étranger sera renvoyé s'il ne respecte pas le délai de départ volontaire (...) ".<br/>
<br/>
              Le Parlement européen et le Conseil ont pris, le 16 décembre 2008, une directive relative aux normes et procédures communes applicables dans les Etats membres au retour des ressortissants de pays tiers en séjour irrégulier. L'article 6 de cette directive, relatif à la " Décision de retour ", dispose que : <br/>
<br/>
              " 1. Les Etats membres prennent une décision de retour à l'encontre de tout ressortissant d'un pays tiers en séjour irrégulier sur leur territoire, sans préjudice des exceptions visées aux paragraphes 2 à 5./ (...) 4. A tout moment, les Etats membres peuvent décider d'accorder un titre de séjour autonome ou une autre autorisation conférant un droit de séjour pour des motifs charitables, humanitaires ou autres à un ressortissant d'un pays tiers en séjour irrégulier sur leur territoire. Dans ce cas, aucune décision de retour n'est prise. Si une décision de retour a déjà été prise, elle est annulée ou suspendue pour la durée de validité du titre de séjour ou d'une autre autorisation conférant un droit de séjour (...) ".<br/>
<br/>
II.                        Il résulte clairement de l'article 6 de la directive du 16 décembre 2008 que celui-ci n'impose pas aux autorités compétentes de l'Etat membre de prendre systématiquement une décision de retour à l'encontre des ressortissants de pays tiers en séjour irrégulier. En effet, l'article 6 lui-même prévoit de façon très large des exceptions à la règle posée par le premier paragraphe, notamment en son paragraphe 4 qui autorise l'Etat membre à délivrer une autorisation de séjour pour des motifs charitables, humanitaire " ou autres ". Il résulte ainsi des termes mêmes de l'article 6 de la directive, comme d'ailleurs de l'économie de la directive et des objectifs qu'elle poursuit, que l'autorité compétente de l'Etat membre dispose d'un large pouvoir d'appréciation lorsqu'elle prend une décision de retour, qui correspond notamment en droit interne à l'obligation de quitter le territoire français prévue par l'article L. 511-1 du code de l'entrée et du séjour des étrangers et du droit d'asile. <br/>
<br/>
              Le prononcé des décisions de retour ne saurait ainsi avoir un caractère automatique, alors qu'il appartient à l'autorité administrative de se livrer à un examen de la situation personnelle et familiale de l'étranger et de prendre en compte les éventuelles circonstances faisant obstacle à l'adoption d'une mesure d'éloignement à son encontre. Au nombre de ces circonstances figurent notamment celles qui sont mentionnées à l'article 5 de la directive du 16 décembre 2008, selon lequel les Etats membres tiennent dûment compte, lorsqu'ils mettent en oeuvre la directive, de l'intérêt supérieur de l'enfant, de la vie familiale et de l'état de santé du ressortissant concerné.<br/>
<br/>
III.                      L'article 6 de la directive du 16 décembre 2008 ne fait donc nullement obstacle à ce que le juge administratif, lorsqu'il est saisi d'un moyen en ce sens, examine si la décision portant obligation de quitter le territoire français est entachée d'une erreur manifeste dans l'appréciation des conséquences de cette décision sur la situation personnelle de l'étranger qui en fait l'objet.<br/>
<br/>
<br/>
<br/>
              Le présent avis sera notifié au tribunal administratif de Marseille, à M. A...B...et au ministre de l'intérieur.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">15-02-04 COMMUNAUTÉS EUROPÉENNES ET UNION EUROPÉENNE. PORTÉE DES RÈGLES DU DROIT DE L'UNION EUROPÉENNE. DIRECTIVES. - DIRECTIVE RETOUR - PORTÉE DU 1 DE SON ARTICLE 6 - INCIDENCE SUR L'OBLIGATION D'EXAMEN DE LA SITUATION PERSONNELLE ET FAMILIALE DE L'ÉTRANGER SOUS LE CONTRÔLE RESTREINT DU JUGE DE L'EXCÈS DE POUVOIR [RJ1] - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">15-05-045-07 COMMUNAUTÉS EUROPÉENNES ET UNION EUROPÉENNE. RÈGLES APPLICABLES. - DIRECTIVE RETOUR - PORTÉE DU 1 DE SON ARTICLE 6 - INCIDENCE SUR L'OBLIGATION D'EXAMEN DE LA SITUATION PERSONNELLE ET FAMILIALE DE L'ÉTRANGER SOUS LE CONTRÔLE RESTREINT DU JUGE DE L'EXCÈS DE POUVOIR [RJ1] - ABSENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">335-03-02 ÉTRANGERS. OBLIGATION DE QUITTER LE TERRITOIRE FRANÇAIS (OQTF) ET RECONDUITE À LA FRONTIÈRE. LÉGALITÉ INTERNE. - OBLIGATION D'EXAMEN DE LA SITUATION PERSONNELLE ET FAMILIALE DE L'ÉTRANGER SOUS LE CONTRÔLE RESTREINT DU JUGE DE L'EXCÈS DE POUVOIR [RJ1] - INCIDENCE DES DISPOSITIONS DU 1 DE L'ARTICLE 6 DE LA DIRECTIVE RETOUR - ABSENCE.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">54-07-02-04 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. CONTRÔLE DU JUGE DE L'EXCÈS DE POUVOIR. APPRÉCIATIONS SOUMISES À UN CONTRÔLE RESTREINT. - ETRANGERS - OQTF - EXAMEN DE LA SITUATION PERSONNELLE OU FAMILIALE [RJ1] - INCIDENCE SUR CE POINT DU 1 DE L'ARTICLE 6 DE LA DIRECTIVE RETOUR - ABSENCE.
</SCT>
<ANA ID="9A"> 15-02-04 L'article 6 de la directive 2008/115/CE du Parlement européen et du Conseil du 16 décembre 2008 dite directive retour est sans incidence sur l'obligation pour l'administration de se livrer à un examen de la situation personnelle et familiale de l'étranger avant de prononcer une obligation de quitter le territoire français à son encontre et ne fait nullement obstacle à ce que le juge administratif, saisi d'un moyen en ce sens, examine si la décision prise est entachée sur ce point d'une erreur manifeste d'appréciation.</ANA>
<ANA ID="9B"> 15-05-045-07 L'article 6 de la directive 2008/115/CE du Parlement européen et du Conseil du 16 décembre 2008 dite directive retour est sans incidence sur l'obligation pour l'administration de se livrer à un examen de la situation personnelle et familiale de l'étranger avant de prononcer une obligation de quitter le territoire français à son encontre et ne fait nullement obstacle à ce que le juge administratif, saisi d'un moyen en ce sens, examine si la décision prise est entachée sur ce point d'une erreur manifeste d'appréciation.</ANA>
<ANA ID="9C"> 335-03-02 L'article 6 de la directive 2008/115/CE du Parlement européen et du Conseil du 16 décembre 2008 dite directive retour est sans incidence sur l'obligation pour l'administration de se livrer à un examen de la situation personnelle et familiale de l'étranger avant de prononcer une OQTF à son encontre et ne fait nullement obstacle à ce que le juge administratif, saisi d'un moyen en ce sens, examine si la décision prise est entachée sur ce point d'une erreur manifeste d'appréciation.</ANA>
<ANA ID="9D"> 54-07-02-04 L'article 6 de la directive 2008/115/CE du Parlement européen et du Conseil du 16 décembre 2008 dite directive retour est sans incidence sur l'obligation pour l'administration de se livrer à un examen de la situation personnelle et familiale de l'étranger avant de prononcer une obligation de quitter le territoire français (OQTF) à son encontre et ne fait nullement obstacle à ce que le juge administratif, saisi d'un moyen en ce sens, examine si la décision prise est entachée sur ce point d'une erreur manifeste d'appréciation.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Assemblée, 29 juin 1990, Préfet du Doubs c/ Mme Olmos Quintero, n° 115687, p. 184 ; CE, Assemblée, 29 juin 1990, Imanbaccus, n° 115971, p. 192.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
