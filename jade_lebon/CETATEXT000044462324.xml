<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044462324</ID>
<ANCIEN_ID>JG_L_2021_12_000000439631</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/46/23/CETATEXT000044462324.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 1ère chambres réunies, 08/12/2021, 439631</TITRE>
<DATE_DEC>2021-12-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>439631</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO ; SCP BAUER-VIOLAS, FESCHOTTE-DESBOIS, SEBAGH</AVOCATS>
<RAPPORTEUR>Mme Catherine Brouard-Gallet</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:439631.20211208</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              M. Q... G... a demandé au tribunal administratif de Paris d'annuler pour excès de pouvoir la décision du 20 novembre 2017 par laquelle la ministre du travail a, d'une part, annulé la décision de l'inspecteur du travail de la section 10.08 de l'unité départementale de Paris de la direction régionale des entreprises, de la concurrence, de la consommation, du travail et de l'emploi d'Ile-de-France ayant refusé d'accorder à la société Aquanet services l'autorisation de le licencier et, d'autre part, autorisé son licenciement. Par un jugement n° 1800829 du 25 septembre 2018, le tribunal administratif a rejeté sa demande. <br/>
<br/>
              Par un arrêt n° 18PA03654 du 30 janvier 2020, la cour administrative d'appel de Paris a, sur appel de M. G..., annulé le jugement du tribunal administratif et la décision du 20 novembre 2017 de la ministre du travail.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 17 mars et 9 juin 2020 et le 6 janvier 2021 au secrétariat du contentieux du Conseil d'Etat, la société Compagnie française d'entretien et de maintenance (Cofrem) et la société Aquanet services demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de M. G...;  <br/>
<br/>
              3°) de mettre à la charge de M. G... la somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - le code du travail ; <br/>
              - l'ordonnance n° 45-2592 du 2 novembre 1945 ;<br/>
              - la loi n° 2010-1609 du 22 décembre 2010 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Catherine Brouard-Gallet, conseillère d'Etat en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de la société Compagnie française d'entretien et de maintenance et de la société Aquanet services et à la SCP Bauer-Violas, Feschotte-Desbois, Sebagh, avocat de M. G... ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. G... a été recruté en 2002 par la société Aquanet services, filiale du groupe Alhena, par un contrat à durée indéterminée, en qualité d'agent très qualifié de service. Il détenait les mandats de membre titulaire du comité d'entreprise et de délégué du personnel de l'unité économique et sociale formée par la société Compagnie française d'entretien et de maintenance (Cofrem), la société Aquanet et la société Afranett ainsi que du comité d'hygiène, de sécurité et des conditions de travail. Par une décision du 30 juin 2017, l'inspecteur du travail a refusé d'accorder l'autorisation de licencier M. G..., sollicitée par la société Aquanet services. La ministre du travail, par décision du 20 novembre 2017, a annulé la décision de l'inspecteur du travail et autorisé le licenciement de M. G.... Le tribunal administratif de Paris, par un jugement du 25 septembre 2018, a rejeté la demande de M. G... en annulation pour excès de pouvoir de cette décision. Les sociétés Compagnie française d'entretien et de maintenance et Aquanet services se pourvoient en cassation contre l'arrêt du 30 janvier 2020 par lequel la cour administrative d'appel de Paris a, sur l'appel de M. G..., annulé le jugement du 25 septembre 2018 du tribunal administratif de Paris ainsi que la décision du 20 novembre 2017 de la ministre du travail.<br/>
<br/>
              2. En vertu des dispositions du code du travail, les salariés légalement investis de fonctions représentatives qui bénéficient, dans l'intérêt des travailleurs qu'ils représentent, d'une protection exceptionnelle, ne peuvent être licenciés qu'avec l'autorisation de l'inspecteur du travail. Lorsque le licenciement d'un de ces salariés est envisagé, il ne doit pas être en rapport avec les fonctions représentatives normalement exercées par l'intéressé ou avec son appartenance syndicale. Dans le cas où la demande est motivée par un comportement fautif, il appartient à l'inspecteur du travail saisi et, le cas échéant, au ministre compétent, de rechercher, sous le contrôle du juge de l'excès de pouvoir, si les faits reprochés au salarié sont d'une gravité suffisante pour justifier son licenciement, compte tenu de l'ensemble des règles applicables à son contrat de travail et des exigences propres à l'exécution normale du mandat dont il est investi. <br/>
<br/>
              3. Aux termes de l'article 1er de l'ordonnance du 2 novembre 1945 relative au statut des huissiers, dans sa rédaction issue de la loi du 22 décembre 2010 relative à l'exécution des décisions de justice, aux conditions d'exercice de certaines professions réglementées et aux experts judiciaires  : " Les huissiers de justice sont les officiers ministériels qui ont seuls qualité pour signifier les actes et les exploits, faire les notifications prescrites par les lois et règlements lorsque le mode de notification n'a pas été précisé et ramener à exécution les décisions de justice, ainsi que les actes ou titres en forme exécutoire./(...) Ils peuvent, commis par justice ou à la requête de particuliers, effectuer des constatations purement matérielles, exclusives de tout avis sur les conséquences de fait ou de droit qui peuvent en résulter. Sauf en matière pénale où elles ont valeur de simples renseignements, ces constatations font foi jusqu'à preuve contraire (...) ". <br/>
<br/>
              4. Il résulte des énonciations de l'arrêt attaqué que, pour faire droit à l'appel de M. G..., la cour administrative d'appel a retenu, après avoir confronté un procès-verbal de constat d'huissier de justice en date du 20 février 2017 produit par la société Aquanet services, duquel il résultait que M. G... avait participé aux incidents survenus lors du dépouillement d'un scrutin professionnel portant sur la révocation du mandat de certains élus organisé sur le fondement des articles L. 2314-29 et L. 2324-27 du code du travail, aux attestations de salariés fournies par M. G..., qu'un doute subsistait quant à sa participation aux incidents litigieux et que ce doute devait lui profiter. En statuant ainsi, alors pourtant qu'il résultait des termes mêmes de son arrêt que la preuve contraire au sens des dispositions de l'article 1er de l'ordonnance du 2 novembre 1945 citées au point 3 n'était pas rapportée, la cour administrative d'appel a commis une erreur de droit.<br/>
<br/>
              5. Il résulte de tout ce qui précède, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, que la société Compagnie française d'entretien et de maintenance et la société Aquanet services sont fondées à demander l'annulation de l'arrêt qu'elles attaquent. <br/>
<br/>
              6. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de la société Compagnie française d'entretien et de la société Aquanet services, qui ne sont pas, dans la présente instance, les parties perdantes. Il n'y a pas lieu dans les circonstances de l'espèce de faire droit aux conclusions de la Compagnie française d'entretien et de maintenance et de la société Aquanet services au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 30 janvier 2020 de la cour administrative d'appel de Paris est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Paris.<br/>
Article 3 : Les conclusions présentées par M. G... et par la Compagnie française d'entretien et de maintenance et la société Aquanet services au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à M. C... G..., premier désigné, pour l'ensemble des ayants droit de M. Q... G..., à la Compagnie française d'entretien et de maintenance et à la société Aquanet services. <br/>
Copie en sera adressée à la ministre du travail, de l'emploi et de l'insertion.<br/>
              Délibéré à l'issue de la séance du 19 novembre 2021 où siégeaient : Mme Christine Maugüé, présidente adjointe de la section du contentieux, présidant ; Mme A... P..., Mme F... O..., présidentes de chambre ; M. B... N..., M. L... J..., Mme K... M..., Mme D... I..., M. Damien Botteghi, conseillers d'Etat et Mme Catherine Brouard-Gallet, conseillère d'Etat en service extraordinaire-rapporteure.<br/>
Rendu le 8 décembre 2021 <br/>
<br/>
<br/>
<br/>
      La présidente : <br/>
      Signé : Mme Christine Maugüé<br/>
 		La rapporteure :<br/>
      Signé : Mme Catherine Brouard-Gallet<br/>
      La secrétaire :<br/>
      Signé : Mme E... H...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-04-04 PROCÉDURE. - INSTRUCTION. - PREUVE. - EXISTENCE D'UNE FAUTE D'UNE GRAVITÉ SUFFISANTE JUSTIFIANT LE LICENCIEMENT D'UN SALARIÉ PROTÉGÉ - FORCE PROBANTE D'UN PROCÈS-VERBAL DE CONSTAT D'HUISSIER DE JUSTICE (ART. 1ER DE L'ORDONNANCE DU 2 NOVEMBRE 1945) - CONSÉQUENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">66-07-01-04-02-01 TRAVAIL ET EMPLOI. - LICENCIEMENTS. - AUTORISATION ADMINISTRATIVE - SALARIÉS PROTÉGÉS. - CONDITIONS DE FOND DE L'AUTORISATION OU DU REFUS D'AUTORISATION. - LICENCIEMENT POUR FAUTE. - EXISTENCE D'UNE FAUTE D'UNE GRAVITÉ SUFFISANTE. - FORCE PROBANTE D'UN PROCÈS-VERBAL DE CONSTAT D'HUISSIER DE JUSTICE (ART. 1ER DE L'ORDONNANCE DU 2 NOVEMBRE 1945) - CONSÉQUENCE.
</SCT>
<ANA ID="9A"> 54-04-04 En vertu des dispositions du code du travail, les salariés légalement investis de fonctions représentatives qui bénéficient, dans l'intérêt des travailleurs qu'ils représentent, d'une protection exceptionnelle, ne peuvent être licenciés qu'avec l'autorisation de l'inspecteur du travail. Lorsque le licenciement d'un de ces salariés est envisagé, il ne doit pas être en rapport avec les fonctions représentatives normalement exercées par l'intéressé ou avec son appartenance syndicale. ......Dans le cas où la demande est motivée par un comportement fautif, il appartient à l'inspecteur du travail saisi et, le cas échéant, au ministre compétent, de rechercher, sous le contrôle du juge de l'excès de pouvoir, si les faits reprochés au salarié sont d'une gravité suffisante pour justifier son licenciement, compte tenu de l'ensemble des règles applicables à son contrat de travail et des exigences propres à l'exécution normale du mandat dont il est investi.......Lorsqu'il résulte d'un procès-verbal de constat d'huissier de justice, effectué en application l'article 1er de l'ordonnance n° 45-2592 du 2 novembre 1945, que les faits reprochés au salarié protégé sont établis, ces constatations font foi jusqu'à preuve contraire. Dès lors, il ne saurait être retenu que, en confrontant ces constatations à des attestations de salariés qui ne rapportent pas la preuve contraire, un doute subsiste qui doit profiter au salarié.</ANA>
<ANA ID="9B"> 66-07-01-04-02-01 En vertu des dispositions du code du travail, les salariés légalement investis de fonctions représentatives qui bénéficient, dans l'intérêt des travailleurs qu'ils représentent, d'une protection exceptionnelle, ne peuvent être licenciés qu'avec l'autorisation de l'inspecteur du travail. Lorsque le licenciement d'un de ces salariés est envisagé, il ne doit pas être en rapport avec les fonctions représentatives normalement exercées par l'intéressé ou avec son appartenance syndicale. ......Dans le cas où la demande est motivée par un comportement fautif, il appartient à l'inspecteur du travail saisi et, le cas échéant, au ministre compétent, de rechercher, sous le contrôle du juge de l'excès de pouvoir, si les faits reprochés au salarié sont d'une gravité suffisante pour justifier son licenciement, compte tenu de l'ensemble des règles applicables à son contrat de travail et des exigences propres à l'exécution normale du mandat dont il est investi.......Lorsqu'il résulte d'un procès-verbal de constat d'huissier de justice, effectué en application l'article 1er de l'ordonnance n° 45-2592 du 2 novembre 1945, que les faits reprochés au salarié protégé sont établis, ces constatations font foi jusqu'à preuve contraire. Dès lors, il ne saurait être retenu que, en confrontant ces constatations à des attestations de salariés qui ne rapportent pas la preuve contraire, un doute subsiste qui doit profiter au salarié.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
