<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038601872</ID>
<ANCIEN_ID>JG_L_2019_06_000000409394</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/60/18/CETATEXT000038601872.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 1ère chambres réunies, 12/06/2019, 409394</TITRE>
<DATE_DEC>2019-06-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>409394</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER ; SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>M. Jean-François de Montgolfier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:409394.20190612</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. A... B...a demandé au tribunal administratif de Paris d'annuler pour excès de pouvoir la délibération du conseil d'administration du Muséum national d'histoire naturelle du 25 avril 2013 statuant sur la composition du comité de sélection pour le recrutement d'un maître de conférences en biologie des organismes, profil " entomologie intégrative (spéciation paléarctique) ", la délibération du comité de sélection du 14 octobre 2013 établissant le classement des candidats, la délibération du conseil d'administration de cet établissement du 19 novembre 2013 proposant la liste des candidats classés par ordre de préférence, la décision du 9 janvier 2014 par laquelle la ministre de l'enseignement supérieur et de la recherche a nommé M. C...D...maître de conférences et, enfin, la décision implicite par laquelle le Muséum national d'histoire naturelle a rejeté son recours gracieux. Par un jugement n° 1406960 du 15 décembre 2015, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 16PA00637 du 30 décembre 2016, la cour administrative d'appel de Paris a rejeté l'appel formé par M. B... contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 30 mars et 28 juin 2017 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge du Muséum national d'histoire naturelle la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
                     Vu les autres pièces du dossier ;<br/>
<br/>
                     Vu :<br/>
                     - le code de l'éducation ;<br/>
                     - le décret n° 84-431 du 6 juin 1984 ;<br/>
                     - le décret n° 92-1178 du 2 novembre 1992 ;<br/>
                     - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-François de Montgolfier, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de M. B...et à la SCP Foussard, Froger, avocat du Muséum national d'histoire naturelle ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que le Muséum national d'histoire naturelle a ouvert au recrutement un poste de maître de conférence. M. D... et M. B... ont notamment présenté leur candidature et ont été classés respectivement en première et deuxième position par le comité de sélection constitué pour examiner les candidatures. M. B... a demandé au tribunal administratif de Paris d'annuler la décision du 9 janvier 2014 de la ministre de l'enseignement supérieur et de la recherche nommant M. D... à ce poste ainsi que les délibérations du comité de sélection et du conseil d'administration portant sur ce recrutement. Par un arrêt du 30 décembre 2016 contre lequel M. B...se pourvoit en cassation, la cour administrative d'appel de Paris a rejeté son appel contre le jugement du tribunal administratif rejetant sa demande.<br/>
<br/>
              Sur l'arrêt attaqué en tant qu'il rejette les conclusions relatives à la délibération du conseil d'administration fixant la composition du comité de sélection :<br/>
<br/>
              2. En premier lieu, en estimant que, pour un recrutement sur un poste de maître de conférence en biologie des organismes, profil " Entomologie intégrative (spéciation paléarctique)", le comité de sélection était composé majoritairement de spécialistes de la discipline en cause, la cour administrative d'appel de Paris n'a pas dénaturé les pièces du dossier qui lui était soumis. Elle a pu, par suite, en déduire sans erreur de droit que la composition du comité ne méconnaissait pas les dispositions du deuxième alinéa de l'article L. 952-6-1 du code de l'éducation en vertu desquelles les membres du comité sont choisis en majorité parmi les spécialistes de la discipline en cause.<br/>
<br/>
              3. En second lieu, les moyens dirigés contre l'arrêt litigieux en tant qu'il juge, d'une part, que le requérant n'était pas recevable à invoquer l'absence de publication de la composition du comité de sélection et, d'autre part, que cette absence de publication ne privait pas l'intéressé d'une garantie sont dirigés contre des motifs surabondants de l'arrêt attaqué. Ils sont, par suite, inopérants. <br/>
<br/>
              4. Il résulte de ce qui précède que M. B...n'est pas fondé à demander l'annulation de l'arrêt de la cour administrative d'appel de Paris du 30 décembre 2016 en tant qu'il rejette ses conclusions tendant à l'annulation du jugement du tribunal administratif de Paris en tant que ce dernier rejette sa demande d'annulation de la délibération du conseil d'administration du 25 avril 2013 statuant sur la composition du comité de sélection.<br/>
<br/>
              Sur le surplus des conclusions du pourvoi :<br/>
<br/>
              5. Le respect du principe d'impartialité fait obstacle à ce qu'un comité de sélection constitué pour le recrutement d'un enseignant-chercheur puisse régulièrement siéger, en qualité de jury de concours, si l'un de ses membres a, avec l'un des candidats, des liens tenant aux activités professionnelles dont l'intensité est de nature à influer sur son appréciation. A ce titre toutefois, la nature hautement spécialisée du recrutement et le faible nombre de spécialistes de la discipline susceptibles de participer au comité de sélection doivent être pris en considération pour l'appréciation de l'intensité des liens faisant obstacle à une participation au comité de sélection.<br/>
<br/>
              6. Il ressort des pièces du dossier soumis aux juges du fond que l'un des membres du comité de sélection constitué pour examiner les candidatures du concours litigieux avait été le directeur de thèse de M. D..., lequel avait soutenu sa thèse moins de deux ans avant la délibération du comité de sélection et avait, ensuite, poursuivi une collaboration scientifique avec son directeur de thèse en cosignant plusieurs articles avec lui. Par suite, en jugeant que les liens existant entre ce candidat et son ancien directeur de thèse n'étaient pas de nature à influer sur son appréciation et ne pouvaient, par suite, entacher d'irrégularité la délibération du jury, la cour administrative d'appel de Paris, alors même que le recrutement en cause concernait un champ disciplinaire très spécialisé, a inexactement qualifié les faits qui lui étaient soumis. Dès lors, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, M. B... est fondé à demander l'annulation de l'arrêt attaqué en tant qu'il rejette son appel contre le jugement du tribunal administratif de Paris en tant que ce dernier rejette sa demande d'annulation de la délibération du comité de sélection du 14 octobre 2013 et des actes subséquents de la procédure de recrutement.<br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond dans cette mesure en application de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              8. Ainsi qu'il a été dit au point 6, compte tenu des liens entre M. D... et son ancien directeur de thèse, ce dernier ne pouvait, sans que soit méconnu le principe d'impartialité du jury, participer à la délibération du comité de sélection du 14 octobre 2013 établissant le classement des candidats au nombre desquels figurait M. D.... Cette délibération étant entachée d'illégalité, il en va de même, par voie de conséquence, de la délibération du conseil d'administration du Muséum national d'histoire naturelle du 19 novembre 2013 proposant la liste de candidats par ordre de préférence, de la décision du 9 janvier 2014 nommant M. D... maître de conférences et de la décision rejetant le recours gracieux de M. B... contre cette dernière décision. Il suit de là que, sans qu'il soit besoin d'examiner les autres moyens de sa requête, M. B... est fondé à soutenir que c'est à tort que, par le jugement du 15 décembre 2015, le tribunal administratif de Paris a rejeté sa demande tendant à l'annulation de ces décisions.<br/>
<br/>
              9. L'annulation des décisions mentionnées au point précédent implique seulement, si le recrutement litigieux est maintenu, la reprise des opérations du concours. Les conclusions à fin d'injonction présentées par M. B..., tendant à ce que le président du Muséum national d'histoire naturelle, le conseil d'administration et un comité de sélection autrement composé transmettent au ministre compétent des délibérations permettant sa nomination ne peuvent, par suite, qu'être rejetées.<br/>
<br/>
              10. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de M. B... qui n'est pas la partie perdante dans la présence instance. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du Muséum national d'histoire naturelle une somme de 6 000 euros à verser à M. B... au titre de ces mêmes dispositions pour les frais exposés par lui devant le tribunal administratif de Paris, la cour administrative d'appel de Paris et le Conseil d'Etat.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Paris du 30 décembre 2016 et le jugement du tribunal administratif de Paris du 15 décembre 2015, dans la mesure indiquée par la présente décision, sont annulés.<br/>
Article 2 : Le surplus des conclusions du pourvoi de M. B...est rejeté.<br/>
Article 3 : La délibération du comité de sélection du Muséum national d'histoire naturelle du 14 octobre 2013 établissant le classement des candidats pour le recrutement d'un maître de conférences en biologie des organismes, profil " entomologie intégrative (spéciation paléarctique) ", la délibération du conseil d'administration de cet établissement du 19 novembre 2013 proposant la liste des candidats classés par ordre de préférence, la décision du 9 janvier 2014 par laquelle la ministre de l'enseignement supérieur et de la recherche a nommé M. C... D...maître de conférences et la décision implicite par laquelle le Muséum national d'histoire naturelle a rejeté son recours gracieux sont annulées.<br/>
Article 4 : Les conclusions à fins d'injonction présentées par M. B...sont rejetées.<br/>
Article 5 : Le Muséum national d'histoire naturelle versera à M. B... la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 6 : Les conclusions présentées par le Muséum national d'histoire naturelle au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 7 : La présente décision sera notifiée à M. A... B..., au Muséum national d'histoire naturelle et à M. C... D....<br/>
Copie en sera adressée à la ministre de l'enseignement supérieur et de la recherche.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-04-03-07 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. PRINCIPES GÉNÉRAUX DU DROIT. PRINCIPES INTÉRESSANT L'ACTION ADMINISTRATIVE. - PRINCIPE D'IMPARTIALITÉ DU JURY D'UN EXAMEN OU D'UN CONCOURS - COMPOSITION DU COMITÉ DE SÉLECTION POUR LE RECRUTEMENT D'UN ENSEIGNANT-CHERCHEUR - 1) MEMBRE DU JURY AYANT AVEC L'UN DES CANDIDATS DES LIENS PROFESSIONNELS DONT L'INTENSITÉ EST DE NATURE À INFLUER SUR SON APPRÉCIATION - MÉCONNAISSANCE DU PRINCIPE D'IMPARTIALITÉ [RJ1] - 2) CAS DES RECRUTEMENTS HAUTEMENT SPÉCIALISÉS - APPRÉCIATION DE L'INTENSITÉ DU LIEN COMPTE-TENU DE CETTE CARACTÉRISTIQUE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">30-02-05 ENSEIGNEMENT ET RECHERCHE. QUESTIONS PROPRES AUX DIFFÉRENTES CATÉGORIES D'ENSEIGNEMENT. ENSEIGNEMENT SUPÉRIEUR ET GRANDES ÉCOLES. - COMPOSITION DU COMITÉ DE SÉLECTION POUR LE RECRUTEMENT D'UN ENSEIGNANT-CHERCHEUR - 1) A) MEMBRE DU JURY AYANT AVEC L'UN DES CANDIDATS DES LIENS PROFESSIONNELS DONT L'INTENSITÉ EST DE NATURE À INFLUER SUR SON APPRÉCIATION - MÉCONNAISSANCE DU PRINCIPE D'IMPARTIALITÉ [RJ1] - B) CAS DES RECRUTEMENTS HAUTEMENT SPÉCIALISÉS - APPRÉCIATION DE L'INTENSITÉ DU LIEN COMPTE-TENU DE CETTE CARACTÉRISTIQUE - 2) ESPÈCE - MEMBRE DU COMITÉ DE SÉLECTION AYANT ÉTÉ LE DIRECTEUR DE THÈSE DE L'UN DES CANDIDATS, QUI AVAIT SOUTENU SA THÈSE MOINS DE DEUX ANS AUPARAVANT, ET AYANT POURSUIVI UNE COLLABORATION SCIENTIFIQUE AVEC CELUI-CI - IRRÉGULARITÉ [RJ2].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">36-03-02-03 FONCTIONNAIRES ET AGENTS PUBLICS. ENTRÉE EN SERVICE. CONCOURS ET EXAMENS PROFESSIONNELS. ORGANISATION DES CONCOURS - JURY. - PRINCIPE D'IMPARTIALITÉ DU JURY D'UN EXAMEN OU D'UN CONCOURS - COMPOSITION DU COMITÉ DE SÉLECTION POUR LE RECRUTEMENT D'UN ENSEIGNANT-CHERCHEUR - 1) MEMBRE DU JURY AYANT AVEC L'UN DES CANDIDATS DES LIENS PROFESSIONNELS DONT L'INTENSITÉ EST DE NATURE À INFLUER SUR SON APPRÉCIATION - MÉCONNAISSANCE DU PRINCIPE D'IMPARTIALITÉ [RJ1] - 2) CAS DES RECRUTEMENTS HAUTEMENT SPÉCIALISÉS - APPRÉCIATION DE L'INTENSITÉ DU LIEN COMPTE-TENU DE CETTE CARACTÉRISTIQUE.
</SCT>
<ANA ID="9A"> 01-04-03-07 1) Le respect du principe d'impartialité fait obstacle à ce qu'un comité de sélection constitué pour le recrutement d'un enseignant-chercheur puisse régulièrement siéger, en qualité de jury de concours, si l'un de ses membres a, avec l'un des candidats, des liens tenant aux activités professionnelles dont l'intensité est de nature à influer sur son appréciation.... ,,2) A ce titre toutefois, la nature hautement spécialisée du recrutement et le faible nombre de spécialistes de la discipline susceptibles de participer au comité de sélection doivent être pris en considération pour l'appréciation de l'intensité des liens faisant obstacle à une participation au comité de sélection.</ANA>
<ANA ID="9B"> 30-02-05 1) a) Le respect du principe d'impartialité fait obstacle à ce qu'un comité de sélection constitué pour le recrutement d'un enseignant-chercheur puisse régulièrement siéger, en qualité de jury de concours, si l'un de ses membres a, avec l'un des candidats, des liens tenant aux activités professionnelles dont l'intensité est de nature à influer sur son appréciation.... ,,b) A ce titre toutefois, la nature hautement spécialisée du recrutement et le faible nombre de spécialistes de la discipline susceptibles de participer au comité de sélection doivent être pris en considération pour l'appréciation de l'intensité des liens faisant obstacle à une participation au comité de sélection.,,,2) Il ressort des pièces du dossier soumis aux juges du fond que l'un des membres du comité de sélection constitué pour examiner les candidatures du concours litigieux avait été le directeur de thèse d'un candidat, lequel avait soutenu sa thèse moins de deux ans avant la délibération du comité de sélection et avait, ensuite, poursuivi une collaboration scientifique avec son directeur de thèse en cosignant plusieurs articles avec lui. Par suite, en jugeant que les liens existant entre ce candidat et son ancien directeur de thèse n'étaient pas de nature à influer sur son appréciation et ne pouvaient, par suite, entacher d'irrégularité la délibération du jury, la cour administrative d'appel, alors même que le recrutement en cause concernait un champ disciplinaire très spécialisé, a inexactement qualifié les faits qui lui étaient soumis.</ANA>
<ANA ID="9C"> 36-03-02-03 1) Le respect du principe d'impartialité fait obstacle à ce qu'un comité de sélection constitué pour le recrutement d'un enseignant-chercheur puisse régulièrement siéger, en qualité de jury de concours, si l'un de ses membres a, avec l'un des candidats, des liens tenant aux activités professionnelles dont l'intensité est de nature à influer sur son appréciation.... ,,2) A ce titre toutefois, la nature hautement spécialisée du recrutement et le faible nombre de spécialistes de la discipline susceptibles de participer au comité de sélection doivent être pris en considération pour l'appréciation de l'intensité des liens faisant obstacle à une participation au comité de sélection.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 17 décembre 2016, Université de Nice-Sophia Antipolis, n° 386400, T. pp. 619-800., ,[RJ2] Rappr., au regard du principe d'égalité et s'agissant du chef de service d'un candidat, CE, Section, 18 mars 1983,,, n° 33379, p. 125 ; s'agissant du directeur du département scientifique, CE, 20 septembre 1991,,, n° 100225, T. p. 990. Comp., au regard du principe d'impartialité et s'agissant du président du jury de thèse, CE, 13 mars 1991,,, n° 109792, T. pp. 692-697-991.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
