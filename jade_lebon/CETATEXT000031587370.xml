<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031587370</ID>
<ANCIEN_ID>JG_L_2015_12_000000375643</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/58/73/CETATEXT000031587370.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème / 3ème SSR, 07/12/2015, 375643</TITRE>
<DATE_DEC>2015-12-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>375643</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème / 3ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Mathieu Herondart</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Nathalie Escaut</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:375643.20151207</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société nationale immobilière a demandé au tribunal administratif d'Orléans d'annuler la décision implicite par laquelle l'Etat a rejeté sa demande du 13 janvier 2011 tendant à ce que lui soient réglés les loyers dus à compter du 11 juin 2009 pour l'occupation de la caserne de gendarmerie de Montbazon, de condamner l'Etat au paiement de ces loyers, assorti des intérêts au taux légal, et d'enjoindre à l'Etat de lui verser directement les loyers à venir. <br/>
<br/>
              Par un jugement n° 1101057 du 2 février 2012, le tribunal administratif d'Orléans a rejeté cette demande.<br/>
<br/>
              Par un arrêt n° 12NT00898 du 20 décembre 2013, la cour administrative d'appel de Nantes a rejeté l'appel formé contre ce jugement par la société nationale immobilière.<br/>
<br/>
              Par un pourvoi sommaire, deux mémoires complémentaires et un mémoire en réplique, enregistrés les 20 février et 19 mai 2014, et les 3 février et 7 mai 2015 au secrétariat du contentieux du Conseil d'Etat, la société nationale immobilière demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des collectivités territoriales ;<br/>
              - la loi n° 2011-267 du 14 mars 2011 ;<br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Mathieu Herondart, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Nathalie Escaut, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat de la société nationale immobilière ;<br/>
<br/>
<br/>
<br/>
               1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la commune de Montbazon a édifié, pour la gendarmerie nationale, un ensemble immobilier constitué de locaux de service et techniques d'une brigade territoriale et de seize logements destinés aux gendarmes ; qu'un contrat conclu entre la commune et l'Etat le 5 janvier 2006 a donné cet ensemble immobilier en location à l'Etat pour une durée de neuf ans, en contrepartie d'un loyer annuel de 148 325,10 euros ; que la commune a conclu avec la société nationale immobilière, le 11 juin 2009, un bail emphytéotique d'une durée de cinquante ans, portant sur cet ensemble immobilier et précisant que le contrat conclu par la commune avec l'Etat serait à cet effet transféré de la commune à la société nationale immobilière, en mentionnant que ce transfert donnerait lieu à avenant à ce contrat ; que l'Etat, qui a refusé de signer l'avenant au contrat du 5 janvier 2006 prévoyant la substitution de la société nationale immobilière à la commune en qualité de bailleur, a implicitement rejeté la demande de la société tendant à ce que les loyers dus à compter du 11 juin 2009 soient directement versés à cette dernière ; que la société nationale immobilière se pourvoit en cassation contre l'arrêt du 20 décembre 2013 par lequel la cour administrative d'appel de Nantes a rejeté son appel formé contre le jugement du tribunal administratif d'Orléans du 2 février 2012 rejetant ses demandes tendant à l'annulation de cette décision de refus, à ce que l'Etat soit condamné à lui verser le montant des loyers échus depuis le 11 juin 2009 et à ce qu'il soit enjoint à l'Etat de lui verser directement les loyers à venir ;<br/>
<br/>
              2. Considérant qu'aux termes des dispositions de l'article L. 1311-2 du code général des collectivités territoriales, applicables antérieurement à la loi du 14 mars 2011 et issues, avant leur codification par la loi du 21 février 1996, de l'article 13 de la loi du 5 janvier 1988 : " Un bien immobilier appartenant à une collectivité territoriale peut faire l'objet d'un bail emphytéotique prévu à l'article L. 451-1 du code rural, en vue de l'accomplissement, pour le compte de la collectivité territoriale, d'une mission de service public ou en vue de la réalisation d'une opération d'intérêt général relevant de sa compétence (...) " ; <br/>
<br/>
               3. Considérant qu'il résulte de ces dispositions, notamment de la référence qu'elles comportent au bail emphytéotique prévu à l'article L. 451-1 du code rural, que le législateur n'a ainsi entendu viser que les contrats dans lesquels le preneur a la charge de réaliser, sur le bien immobilier qu'il est ainsi autorisé à occuper, des investissements qui reviendront à la collectivité en fin de bail, et non de permettre la conclusion, dans le cadre de ce régime, de contrats par lesquels une collectivité territoriale confie à un tiers une mission de gestion courante d'un bien lui appartenant ; que, s'il résulte des dispositions insérées à l'article L. 1311-2 par l'article 96 de la loi du 14 mars 2011 qu'un bail emphytéotique peut dorénavant être conclu en vue de la restauration, de la réparation, de l'entretien-maintenance ou de la mise en valeur d'un bien immobilier appartenant à une collectivité territoriale, il ne résulte ni de ce dernier article ni d'aucune autre disposition de cette loi qu'elle comporterait un effet rétroactif ;<br/>
<br/>
              4. Considérant que, pour rejeter l'appel de la société nationale immobilière, la cour a jugé que cette dernière ne pouvait se prévaloir d'aucun droit réel sur l'ensemble immobilier concerné, au motif que, à la date de sa signature, la convention conclue entre cette société et la commune de Montbazon n'entrait pas dans le champ d'application des dispositions de l'article L. 1311-2 du code général des collectivités territoriales ; qu'elle a cependant omis de répondre au moyen, qui n'était pas inopérant, tiré de ce que cette convention mettait à la charge de la société des travaux et des investissements importants, y compris d'amélioration, excédant une simple mission de gestion courante de cet ensemble immobilier ; que, dès lors, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, la société nationale immobilière est fondée à demander l'annulation de l'arrêt qu'elle attaque ;  <br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ; <br/>
<br/>
              6. Considérant qu'un contrat par lequel le propriétaire d'une dépendance du domaine public confie la gestion de cette dépendance à un tiers n'est pas opposable à la personne publique à qui ce bien a été loué pour y exercer une mission de service public sans que cette dernière y ait consenti ; que la société nationale immobilière ne saurait donc soutenir que le contrat de bail emphytéotique qu'elle a conclu avec la commune de Montbazon a eu pour effet de rendre opposable à l'Etat sa substitution à la commune en qualité de bailleur ; <br/>
<br/>
              7. Considérant qu'il résulte de l'instruction que, par une décision du 2 juillet 2009, les services du domaine et de la gendarmerie ont refusé de signer au nom de l'Etat l'avenant transférant de la commune de Montbazon à la société nationale immobilière le contrat de location de la caserne de gendarmerie ; que ni la commune, ni la société n'ont formé de recours à l'encontre de cette décision de refus ; que si la convention conclue entre la commune de Montbazon et l'Etat pour la mise à disposition de la gendarmerie ne comportait aucune stipulation imposant l'accord de l'Etat en cas de transfert de cette convention, il résulte de ce qui vient d'être dit qu'un tel accord était néanmoins nécessaire pour que la société nationale immobilière puisse se substituer à la commune de Montbazon dans l'exécution du contrat de location que celle-ci avait conclu avec l'Etat ; que la société nationale immobilière n'est donc pas fondée, en tout état de cause, à réclamer à l'Etat, sur le terrain contractuel, les loyers litigieux ; que, par suite, ses conclusions tendant à la condamnation de l'Etat à lui verser les loyers dus à raison de l'exécution de cette convention ne peuvent qu'être rejetées ;<br/>
<br/>
              8. Considérant qu'il résulte de ce qui précède que la société nationale immobilière n'est pas fondée à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif d'Orléans a rejeté sa demande ;<br/>
<br/>
              9. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de l'Etat qui n'est pas, dans la présente instance la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nantes du 20 décembre 2013 est annulé. <br/>
Article 2 : La requête présentée par la société nationale immobilière devant la cour administrative d'appel de Nantes et le surplus des conclusions de son pourvoi sont rejetés.<br/>
Article 3 : La présente décision sera notifiée à la société nationale immobilière et au ministre des finances et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">24-01-02-01-01-02 DOMAINE. DOMAINE PUBLIC. RÉGIME. OCCUPATION. UTILISATIONS PRIVATIVES DU DOMAINE. CONTRATS ET CONCESSIONS. - 1) DÉPENDANCE DU DOMAINE PUBLIC LOUÉE À UNE PERSONNE PUBLIQUE POUR Y EXERCER UNE MISSION DE SERVICE PUBLIC - OPPOSABILITÉ À CETTE PERSONNE DU CONTRAT PAR LEQUEL LE PROPRIÉTAIRE CONFIE LA GESTION DE CETTE DÉPENDANCE À UN TIERS - CONDITION - ACCORD DE LA PERSONNE PUBLIQUE LOCATAIRE - 2) APPLICATION À L'ESPÈCE - COMMUNE LOUANT À L'ETAT UNE CASERNE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">39-03-01 MARCHÉS ET CONTRATS ADMINISTRATIFS. EXÉCUTION TECHNIQUE DU CONTRAT. CONDITIONS D'EXÉCUTION DES ENGAGEMENTS CONTRACTUELS EN L'ABSENCE D'ALÉAS. - 1) DÉPENDANCE DU DOMAINE PUBLIC LOUÉE À UNE PERSONNE PUBLIQUE POUR Y EXERCER UNE MISSION DE SERVICE PUBLIC - OPPOSABILITÉ À CETTE PERSONNE DU CONTRAT PAR LEQUEL LE PROPRIÉTAIRE CONFIE LA GESTION DE CETTE DÉPENDANCE À UN TIERS - CONDITION - ACCORD DE LA PERSONNE PUBLIQUE LOCATAIRE - 2) APPLICATION À L'ESPÈCE - COMMUNE LOUANT À L'ETAT UNE CASERNE.
</SCT>
<ANA ID="9A"> 24-01-02-01-01-02 1) Un contrat par lequel le propriétaire d'une dépendance du domaine public confie la gestion de cette dépendance à un tiers n'est pas opposable à la personne publique à qui ce bien a été loué pour y exercer une mission de service public sans que cette dernière y ait consenti.,,,2) Cas d'une commune louant à l'Etat une caserne de gendarmerie et ayant conclu une convention à cet effet. Une société tierce ne saurait soutenir que le contrat de bail emphytéotique qu'elle a conclu avec la commune a eu pour effet de rendre opposable à l'Etat sa substitution à la commune en qualité de bailleur. Les services du domaine et de la gendarmerie ont refusé de signer au nom de l'Etat l'avenant transférant de la commune à cette société le contrat de location de la caserne de gendarmerie et ni la commune, ni le tiers n'ont formé de recours à l'encontre de cette décision de refus. Si la convention conclue entre la commune et l'Etat pour la mise à disposition de la gendarmerie ne comportait aucune stipulation imposant l'accord de l'Etat en cas de transfert de cette convention, un tel accord était néanmoins nécessaire pour que la société puisse se substituer à la commune dans l'exécution du contrat de location que celle-ci avait conclu avec l'Etat. La société n'est donc pas fondée, en tout état de cause, à réclamer à l'Etat, sur le terrain contractuel, des loyers.</ANA>
<ANA ID="9B"> 39-03-01 1) Un contrat par lequel le propriétaire d'une dépendance du domaine public confie la gestion de cette dépendance à un tiers n'est pas opposable à la personne publique à qui ce bien a été loué pour y exercer une mission de service public sans que cette dernière y ait consenti.,,,2) Cas d'une commune louant à l'Etat une caserne de gendarmerie et ayant conclu une convention à cet effet. Une société tierce ne saurait soutenir que le contrat de bail emphytéotique qu'elle a conclu avec la commune a eu pour effet de rendre opposable à l'Etat sa substitution à la commune en qualité de bailleur. Les services du domaine et de la gendarmerie ont refusé de signer au nom de l'Etat l'avenant transférant de la commune à cette société le contrat de location de la caserne de gendarmerie et ni la commune, ni le tiers n'ont formé de recours à l'encontre de cette décision de refus. Si la convention conclue entre la commune et l'Etat pour la mise à disposition de la gendarmerie ne comportait aucune stipulation imposant l'accord de l'Etat en cas de transfert de cette convention, un tel accord était néanmoins nécessaire pour que la société puisse se substituer à la commune dans l'exécution du contrat de location que celle-ci avait conclu avec l'Etat. La société n'est donc pas fondée, en tout état de cause, à réclamer à l'Etat, sur le terrain contractuel, des loyers.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
