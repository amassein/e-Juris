<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028161235</ID>
<ANCIEN_ID>JG_L_2013_11_000000345696</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/16/12/CETATEXT000028161235.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème / 4ème SSR, 06/11/2013, 345696</TITRE>
<DATE_DEC>2013-11-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>345696</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème / 4ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROGER, SEVAUX, MATHONNET ; FOUSSARD ; SCP DIDIER, PINET</AVOCATS>
<RAPPORTEUR>M. Jean-Dominique Langlais</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Fabienne Lambolez</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:345696.20131106</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 10 janvier et 29 mars 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour Mme B...A..., demeurant... ; Mme A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement n° 0500439 du 8 décembre 2009 du tribunal administratif de Strasbourg rejetant sa demande tendant à la condamnation de l'Etat à réparer les préjudices qu'elle estime avoir subis du fait de sa vaccination obligatoire contre l'hépatite B ;<br/>
<br/>
              2°) réglant l'affaire au fond, de déclarer l'Etat responsable de l'intégralité de son préjudice ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;  <br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de la santé publique ; <br/>
<br/>
              Vu le code de la sécurité sociale ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Langlais, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de Mme Fabienne Lambolez, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Roger, Sevaux, Mathonnet, avocat de MmeA..., à Me Foussard, avocat de la caisse primaire d'assurance maladie de Strasbourg et à la SCP Didier, Pinet, avocat de l'Etablissement public de santé Alsace nord ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que Mme A...a subi, dans le cadre de l'obligation vaccinale liée à son activité professionnelle, trois injections d'un vaccin anti-hépatite B en octobre et novembre 1992 et en janvier 1993, puis un rappel le 24 novembre 1993 ; qu'atteinte d'une sclérose en plaques diagnostiquée en septembre 1994, elle a recherché, sur le fondement de l'article L. 3111-9 du code de la santé publique, la responsabilité de l'Etat au titre de cette affection, qu'elle impute à la vaccination ; que le ministre du travail, de l'emploi et de la santé, après une expertise médicale réalisée le 6 décembre 2001 et un avis négatif du 17 juin 2004 de la commission de règlement amiable des accidents vaccinaux, a rejeté sa demande d'indemnisation par une décision du 27 novembre 2004 ; que Mme A...a introduit devant le tribunal administratif de Strasbourg un recours indemnitaire dirigé contre l'Etat, qui a été rejeté par un jugement du 13 février 2007  ; que ce jugement a été annulé par une décision du 27 mai 2009 du Conseil d'Etat, statuant au contentieux, faute pour le tribunal administratif d'avoir mis en cause les organismes qui avaient versé des prestations à Mme A...au titre de sa pathologie ; que l'intéressée se pourvoit en cassation contre le jugement du 8 décembre 2009 par lequel le tribunal administratif, auquel sa demande avait été renvoyée, l'a de nouveau rejetée ; que la caisse primaire d'assurance maladie de Strasbourg demande également l'annulation de ce jugement ; <br/>
<br/>
              2. Considérant qu'aux termes du premier alinéa de l'article L. 3111-4 du code de la santé publique : " Une personne qui, dans un établissement ou organisme public ou privé de prévention, de soins ou hébergeant des personnes âgées, exerce une activité professionnelle l'exposant à des risques de contamination doit être immunisée contre l'hépatite B, la diphtérie, le tétanos, la poliomyélite et la grippe " ; qu'aux termes du premier alinéa de l'article L. 3111-9 du même code, dans sa rédaction applicable à la date de la décision rejetant la demande d'indemnisation de la requérante : " Sans préjudice des actions qui pourraient être exercées conformément au droit commun, la réparation d'un dommage imputable directement à une vaccination obligatoire pratiquée dans les conditions mentionnées au présent chapitre est supportée par l'Etat " ; <br/>
<br/>
              3. Considérant qu'alors même qu'un rapport d'expertise, sans l'exclure, n'établirait pas de lien de causalité entre la vaccination et l'affection, la responsabilité de l'Etat peut être engagée en raison des conséquences dommageables d'injections vaccinales contre l'hépatite B réalisées dans le cadre d'une activité professionnelle eu égard, d'une part, au bref délai ayant séparé l'injection des premiers symptômes d'une sclérose en plaques, éprouvés par l'intéressé et validés par les constatations de l'expertise médicale, et, d'autre part, à la bonne santé de la personne concernée et à l'absence, chez elle, de tous antécédents à cette pathologie antérieurement à sa vaccination ; que la preuve des différentes circonstances à prendre ainsi en compte, notamment celle de la date d'apparition des premiers symptômes d'une sclérose en plaques, peut être apportée par tout moyen ; <br/>
<br/>
              4. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que l'expert commis au titre du règlement amiable a, au vu des affirmations circonstanciées de Mme A... et des éléments dont il disposait, retenu que les premiers symptômes de l'affection diagnostiquée en septembre 1994 étaient apparus dans le mois ayant suivi l'injection de janvier 1993 ; que, pour s'écarter de cette affirmation et rejeter, en conséquence, le recours indemnitaire dont il était saisi, le tribunal administratif a estimé que seule la production de pièces médicales était susceptible d'établir la date d'apparition de ces symptômes ; qu'il a, ce faisant, commis une erreur de droit ; que, dès lors, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, son jugement doit être annulé ; <br/>
<br/>
              5. Considérant qu'aux termes du second alinéa de l'article L. 821-2 du code de justice administrative : " Lorsque l'affaire fait l'objet d'un second pourvoi en cassation, le Conseil d'Etat statue définitivement sur cette affaire " ; qu'il y a lieu, par suite, de régler l'affaire au fond ;<br/>
<br/>
              Sur le principe de la responsabilité :<br/>
<br/>
              6. Considérant qu'il résulte de l'instruction, notamment du rapport de l'expert commis au titre du règlement amiable, que MmeA..., qui n'avait pas présenté de symptômes de sclérose en plaques avant les premières injections du vaccin, a été atteinte dans un bref délai après l'injection de janvier 1993 de troubles évocateurs de cette pathologie ; que, dans ces conditions, celle-ci doit être regardée comme imputable à la vaccination ; qu'il revient dès lors à l'Etat, en application des dispositions précitées, de réparer les dommages subis par Mme A... du fait de cette affection ;  <br/>
<br/>
              Sur la réparation des préjudices résultant de la sclérose en plaques :<br/>
<br/>
              En ce qui concerne les dépenses de santé : <br/>
<br/>
              7. Considérant que la caisse primaire d'assurance maladie de Strasbourg justifie avoir exposé, pour la prise en charge de la sclérose en plaques dont est atteinte Mme A..., des dépenses de santé d'un montant de 34 646,31 euros, dont elle est fondée à demander le remboursement sur le fondement des dispositions de l'article L. 376-1 du code de la sécurité sociale ; que Mme A...ne fait pas état de dépenses de santé qui seraient demeurées à sa charge ; <br/>
<br/>
              En ce qui concerne les préjudices personnels : <br/>
<br/>
              8. Considérant qu'il résulte de l'instruction que la sclérose en plaques dont est atteinte MmeA..., après n'avoir d'abord causé que des troubles intermittents, a entraîné, à partir de la fin de l'année 2001, une incapacité permanente partielle de 20 % ; que son état est susceptible de dégradation à l'avenir ; que la maladie a un retentissement important sur la vie personnelle et familiale de l'intéressée ainsi que sur la possibilité pour elle de continuer à pratiquer ses activités de loisir habituelles ; qu'il sera fait une juste appréciation des préjudices subis par Mme A...en lui accordant les sommes de 30 000 euros au titre des troubles résultant du déficit fonctionnel permanent, de 5 500 euros au titre des souffrances endurées, évaluées par l'expert à 5/7, de 1 500 euros au titre du préjudice esthétique, évalué par l'expert à 2/7, et de 3 000 euros au titre du préjudice d'agrément ; <br/>
<br/>
              9. Considérant qu'il résulte de tout ce qui précède qu'il y a eu lieu de condamner l'Etat à verser à Mme A...la somme de 40 000 euros et à la caisse primaire d'assurance maladie de Strasbourg la somme de 34 646,31 euros  ; que ces sommes réparent les conséquences de la pathologie dont Mme A...est atteinte jusqu'à la date de la présente décision ; qu'il appartient à l'intéressée de demander, le cas échéant, la prise en charge des conséquences qui résulteraient à l'avenir d'une aggravation de la pathologie dont elle est atteinte ;<br/>
<br/>
              Sur les conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative : <br/>
<br/>
              10. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat les sommes de 3 000 euros à verser à Mme A... et de 1 000 euros à verser à la caisse primaire d'assurance maladie de Strasbourg au titre des dispositions de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du 8 décembre 2009 du tribunal administratif de Strasbourg est annulé. <br/>
Article 2 : L'Etat est condamné à verser à Mme A...la somme de 40 000 euros et à la caisse primaire d'assurance maladie de Strasbourg la somme de 34 646,31 euros. <br/>
Article 3 : L'Etat versera à Mme A...une somme de 3 000 euros et à la caisse primaire d'assurance maladie une somme de 1 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à Mme B...A..., au ministre du travail, de l'emploi et de la santé, à l'établissement public de santé Alsace nord et à la caisse primaire d'assurance maladie de Strasbourg. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">60-02-01-03 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RESPONSABILITÉ EN RAISON DES DIFFÉRENTES ACTIVITÉS DES SERVICES PUBLICS. SERVICE PUBLIC DE SANTÉ. SERVICE DES VACCINATIONS. - LIEN DIRECT ENTRE LA VACCINATION CONTRE L'HÉPATITE B ET L'APPARITION DE LA SCLÉROSE EN PLAQUE - CIRCONSTANCES DE FAIT EMPORTANT PRÉSOMPTION DE L'EXISTENCE DE CE LIEN [RJ1] - 1) PRINCIPE - POSSIBILITÉ DE PREUVE PAR TOUT MOYEN - EXISTENCE - 2) ESPÈCE - ERREUR DE DROIT COMMISE EN EXIGEANT LA PRODUCTION DE PIÈCES MÉDICALES POUR ÉTABLIR LA DATE D'APPARITION DES SYMPTÔMES.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">61-01 SANTÉ PUBLIQUE. PROTECTION GÉNÉRALE DE LA SANTÉ PUBLIQUE. - VACCINATION - LIEN DIRECT ENTRE LA VACCINATION CONTRE L'HÉPATITE B ET L'APPARITION DE LA SCLÉROSE EN PLAQUE - CIRCONSTANCES DE FAIT EMPORTANT PRÉSOMPTION DE L'EXISTENCE DE CE LIEN [RJ1] - 1) PRINCIPE - POSSIBILITÉ DE PREUVE PAR TOUT MOYEN - EXISTENCE - 2) ESPÈCE - ERREUR DE DROIT COMMISE EN EXIGEANT LA PRODUCTION DE PIÈCES MÉDICALES POUR ÉTABLIR LA DATE D'APPARITION DES SYMPTÔMES.
</SCT>
<ANA ID="9A"> 60-02-01-03 1) La preuve des différentes circonstances prises en compte pour établir la présomption de lien de causalité entre une vaccination contre l'hépatite B et l'apparition de la sclérose en plaque, à savoir le bref délai ayant séparé l'injection des premiers symptômes éprouvés par l'intéressé et validés par les constatations de l'expertise médicale, la bonne santé de la personne concernée et l'absence de tous antécédents à cette pathologie antérieurement à sa vaccination, peut être apportée par tout moyen.,,,2) Commet par suite une erreur de droit le tribunal administratif qui a estimé que seule la production de pièces médicales était susceptible d'établir la date d'apparition de ces symptômes.</ANA>
<ANA ID="9B"> 61-01 1) La preuve des différentes circonstances prises en compte pour établir la présomption de lien de causalité entre une vaccination contre l'hépatite B et l'apparition de la sclérose en plaque, à savoir le bref délai ayant séparé l'injection des premiers symptômes éprouvés par l'intéressé et validés par les constatations de l'expertise médicale, la bonne santé de la personne concernée et l'absence de tous antécédents à cette pathologie antérieurement à sa vaccination, peut être apportée par tout moyen.,,,2) Commet par suite une erreur de droit le tribunal administratif qui a estimé que seule la production de pièces médicales était susceptible d'établir la date d'apparition de ces symptômes.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 9 mars 2007, Mme Schwartz, n° 267635, p. 118.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
