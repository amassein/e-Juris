<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044505248</ID>
<ANCIEN_ID>JG_L_2021_12_000000444759</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/50/52/CETATEXT000044505248.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 5ème chambres réunies, 15/12/2021, 444759</TITRE>
<DATE_DEC>2021-12-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>444759</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Bruno Bachini</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier  Fuchs</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:444759.20211215</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, un nouveau mémoire et un mémoire en réplique, enregistrés les 21 septembre 2020,  7 octobre 2020 et 18 novembre 2021 au secrétariat du contentieux du Conseil d'Etat, l'association de défense des libertés constitutionnelles et le syndicat unité magistrats SNM FO demandent au Conseil d'Etat d'annuler pour excès de pouvoir le communiqué de presse du 18 septembre 2020 par lequel le garde des sceaux, ministre de la justice, révèle avoir demandé au chef de l'inspection générale de la justice de mener une enquête administrative sur le comportement professionnel de trois magistrats du parquet national financier.  <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
              Vu : <br/>
              - le décret n° 2016-1675 du 5 décembre 2016 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bruno Bachini, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Olivier Fuchs, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Aux termes de l'article R. 351-4 du code de justice administrative : " Lorsque tout ou partie des conclusions dont est saisi un tribunal administratif, une cour administrative d'appel ou le Conseil d'Etat relève de la compétence d'une de ces juridictions administratives, le tribunal administratif, la cour administrative d'appel ou le Conseil d'Etat, selon le cas, est compétent, nonobstant les règles de répartition des compétences entre juridictions administratives, (...) pour rejeter la requête en se fondant sur l'irrecevabilité manifeste de la demande de première instance ".<br/>
<br/>
              2. Aux termes de l'article 2 du décret du 5 décembre 2016 portant création de l'inspection générale de la justice : " L'inspection générale exerce une mission permanente d'inspection, de contrôle, d'étude, de conseil et d'évaluation sur l'ensemble des organismes, des directions, établissements et services du ministère de la justice et des juridictions de l'ordre judiciaire ainsi que sur les personnes morales de droit public soumises à la tutelle du ministère de la justice et sur les personnes morales de droit privé dont l'activité relève des missions du ministère de la justice ou bénéficiant de financements publics auxquels contribuent les programmes du ministère de la justice. Elle apprécie l'activité, le fonctionnement et la performance des juridictions, établissements, services et organismes soumis à son contrôle ainsi que, dans le cadre d'une mission d'enquête, la manière de servir des personnels. Elle présente toutes recommandations et observations utiles ".<br/>
<br/>
              3. Par un communiqué de presse du 18 septembre 2020 dont l'association de défense des libertés constitutionnelles et le syndicat unité magistrats SNM FO demandent l'annulation pour excès de pouvoir, le garde des sceaux, ministre de la justice, informe avoir demandé à l'inspection générale de la justice de mener une enquête administrative sur le comportement professionnel de trois magistrats affectés au parquet national financier. Les requérants doivent être regardés, eu égard à la teneur de leurs écritures, comme demandant l'annulation de l'acte de saisine de l'inspection générale de la justice révélé par ce communiqué de presse mais également de ce communiqué.<br/>
<br/>
              4. En premier lieu, l'acte par lequel un ministre saisit l'un des services de son ministère pour l'exercice de missions relevant de sa compétence n'est pas susceptible de faire l'objet d'un recours pour excès de pouvoir. Par suite, l'acte par lequel le garde des sceaux, ministre de la justice, a saisi l'inspection générale de la justice sur le fondement de l'article 2 du décret du 5 décembre 2016 portant création de l'inspection générale de la justice n'est pas susceptible de faire l'objet d'un recours pour excès de pouvoir. <br/>
<br/>
              5. En second lieu, si, en principe, un simple communiqué de presse n'est pas en lui-même susceptible de faire l'objet d'un recours pour excès de pouvoir, le communiqué litigieux, en ce qu'il rend publique l'appréciation selon laquelle trois magistrats nommément désignés sont susceptibles d'avoir commis des " manquements au devoir de diligence, de rigueur et de loyauté " et qu'ils sont, pour ce motif, visés par une enquête administrative, est de nature à produire des effets notables, notamment sur les conditions d'exercice de leurs fonctions par les intéressés qui seraient, à ce titre, recevables à en demander l'annulation. <br/>
<br/>
              6. Toutefois, d'une part, si un syndicat, chargé de défendre les intérêts collectifs des magistrats, est recevable à intervenir dans une instance au soutien de conclusions de magistrats contestant des sanctions ou des actes individuels défavorables les concernant, il ne justifie pas d'un intérêt pour présenter, en sa qualité, des conclusions tendant à l'annulation de tels actes. Par suite, le syndicat unité magistrats SNM FO n'est pas recevable à demander l'annulation du communiqué de presse litigieux en tant qu'il met en cause trois magistrats. <br/>
<br/>
              7. D'autre part, l'association de défense des libertés constitutionnelles, par la généralité de son objet social, qui est " d'assurer en France la promotion et la garantie des droits et libertés fondamentaux ", " de veiller à la séparation des pouvoirs et d'œuvrer à la protection et l'indépendance des services publics, la transparence de l'action publique et la lutte contre les conflits d'intérêts et la corruption " et de " développer et de soutenir, par tous moyens, y compris par la voie contentieuse, les actions en vue de la reconnaissance et le respect de l'effectivité des droits et libertés en France et en Europe, y compris les droits sociaux, économiques et culturels, environnementaux ou des générations futures ", ne justifie pas, en tout état de cause, d'un intérêt pour demander, dans cette même mesure, l'annulation pour excès de pouvoir du communiqué de presse litigieux. <br/>
<br/>
              8. Il résulte de ce qui précède que la requête présentée par l'association de défense des libertés constitutionnelles et le syndicat unité magistrats SNM FO est irrecevable et doit être rejetée par application des dispositions précitées de l'article R. 351-4 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de l'association de défense des libertés constitutionnelles et du syndicat unité magistrats SNM FO est rejetée.<br/>
Article 2 : La présente décision sera notifiée à l'association de défense des libertés constitutionnelles, au syndicat unité magistrats SNM FO, au garde des sceaux, ministre de la justice et au Premier ministre. <br/>
Copie en sera adressée à M. F... A..., Mme M... O... et Mme D... H.... <br/>
<br/>
<br/>
              Délibéré à l'issue de la séance du 22 novembre 2021 où siégeaient : M. Rémy Schwartz, président adjoint de la Section du contentieux, présidant ; Mme P... I..., M. L... E..., Mme G... K..., M. B... J..., Mme Rozen Noguellou, conseillers d'Etat et M. Bruno Bachini, maître des requêtes-rapporteur.<br/>
<br/>
<br/>
              Rendu le 15 décembre 2021.<br/>
<br/>
                 Le président : <br/>
                 Signé : M. Rémy Schwartz<br/>
 		Le rapporteur : <br/>
      Signé : M. Bruno Bachini<br/>
                 La secrétaire :<br/>
                 Signé : Mme N... C...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">37-04-02 JURIDICTIONS ADMINISTRATIVES ET JUDICIAIRES. - MAGISTRATS ET AUXILIAIRES DE LA JUSTICE. - MAGISTRATS DE L'ORDRE JUDICIAIRE. - COMMUNIQUÉ DE PRESSE RÉVÉLANT LA SAISINE DE L'IGJ AFIN DE MENER UNE ENQUÊTE SUR LE COMPORTEMENT DE CERTAINS MAGISTRATS [RJ1] - 1) ACTE SUSCEPTIBLE DE RECOURS - A) SAISINE DE L'IGJ - ABSENCE [RJ2] - B) COMMUNIQUÉ DE PRESSE - EXISTENCE - 2) INTÉRÊT D'UN SYNDICAT DE MAGISTRATS [RJ3] - A) POUR INTERVENIR - EXISTENCE - B) POUR AGIR - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-01-01-01 PROCÉDURE. - INTRODUCTION DE L'INSTANCE. - DÉCISIONS POUVANT OU NON FAIRE L'OBJET D'UN RECOURS. - ACTES CONSTITUANT DES DÉCISIONS SUSCEPTIBLES DE RECOURS. - COMMUNIQUÉ DE PRESSE RÉVÉLANT LA SAISINE DE L'IGJ AFIN DE MENER UNE ENQUÊTE SUR LE COMPORTEMENT DE CERTAINS MAGISTRATS.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-01-01-02 PROCÉDURE. - INTRODUCTION DE L'INSTANCE. - DÉCISIONS POUVANT OU NON FAIRE L'OBJET D'UN RECOURS. - ACTES NE CONSTITUANT PAS DES DÉCISIONS SUSCEPTIBLES DE RECOURS. - SAISINE PAR UN MINISTRE D'UN SERVICE DE SON MINISTÈRE - 1) PRINCIPE - INCLUSION - 2) ILLUSTRATION - SAISINE DE L'IGJ AFIN DE MENER UNE ENQUÊTE SUR LE COMPORTEMENT DE CERTAINS MAGISTRATS [RJ1] [RJ2].
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">54-01-04-01-02 PROCÉDURE. - INTRODUCTION DE L'INSTANCE. - INTÉRÊT POUR AGIR. - ABSENCE D'INTÉRÊT. - SYNDICATS, GROUPEMENTS ET ASSOCIATIONS. - SYNDICAT DE MAGISTRATS - RECOURS CONTRE UNE DÉCISION INDIVIDUELLE DÉFAVORABLE - 1) PRINCIPE [RJ3] - A) INTÉRÊT POUR INTERVENIR - EXISTENCE - B) INTÉRÊT POUR AGIR - ABSENCE - 2) ILLUSTRATION - RECOURS CONTRE UN COMMUNIQUÉ DE PRESSE RÉVÉLANT LA SAISINE DE L'IGJ AFIN DE MENER UNE ENQUÊTE SUR LE COMPORTEMENT DE CERTAINS MAGISTRATS.
</SCT>
<ANA ID="9A"> 37-04-02 Communiqué de presse du garde des sceaux, ministre de la justice, informant avoir demandé à l'inspection générale de la justice (IGJ) de mener une enquête administrative sur le comportement professionnel de trois magistrats affectés au parquet national financier (PNF).......1) a) L'acte par lequel un ministre saisit un des services de son ministère pour l'exercice de missions relevant de sa compétence n'est pas susceptible de faire l'objet d'un recours pour excès de pouvoir. Par suite, l'acte par lequel le garde des sceaux, ministre de la justice, a saisi l'IGJ, sur le fondement de l'article 2 du décret n° 2016-1675 du 5 décembre 2016, afin qu'elle mène une enquête administrative sur le comportement professionnel de trois magistrats affectés au PNF n'est pas susceptible de faire l'objet d'un recours pour excès de pouvoir.......b) Si, en principe, un simple communiqué de presse n'est pas en lui-même susceptible de faire l'objet d'un recours pour excès de pouvoir, le communiqué litigieux, en ce qu'il rend publics l'appréciation selon laquelle trois magistrats nommément désignés sont susceptibles d'avoir commis des manquements au devoir de diligence, de rigueur et de loyauté et qu'ils sont pour ce motif visés par une enquête administrative, est de nature à produire des effets notables, notamment sur les conditions d'exercice de leurs fonctions par les intéressés qui seraient, à ce titre, recevable à en demander l'annulation.......2) a) Un syndicat, chargé de défendre les intérêts collectifs des magistrats, est recevable à intervenir dans une instance au soutien de conclusions de magistrats contestant des sanctions ou des actes individuels défavorables les concernant. ......b) Il ne justifie en revanche pas d'un intérêt pour présenter en sa qualité des conclusions tendant à l'annulation de tels actes. ......Le syndicat de magistrats requérant n'est, par suite, pas recevable à demander l'annulation du communiqué de presse litigieux en tant qu'il met en cause trois magistrats.</ANA>
<ANA ID="9B"> 54-01-01-01 Communiqué de presse du garde des sceaux, ministre de la justice, informant avoir demandé à l'inspection générale de la justice (IGJ) de mener une enquête administrative sur le comportement professionnel de trois magistrats affectés au parquet national financier.......Si, en principe, un simple communiqué de presse n'est pas en lui-même susceptible de faire l'objet d'un recours pour excès de pouvoir, le communiqué litigieux, en ce qu'il rend publics l'appréciation selon laquelle trois magistrats nommément désignés sont susceptibles d'avoir commis des manquements au devoir de diligence, de rigueur et de loyauté et qu'ils sont pour ce motif visés par une enquête administrative, est de nature à produire des effets notables, notamment sur les conditions d'exercice de leurs fonctions par les intéressés qui seraient, à ce titre, recevable à en demander l'annulation.</ANA>
<ANA ID="9C"> 54-01-01-02 1) a) L'acte par lequel un ministre saisit un des services de son ministère pour l'exercice de missions relevant de sa compétence n'est pas susceptible de faire l'objet d'un recours pour excès de pouvoir. ......b) Par suite, l'acte par lequel le garde des sceaux, ministre de la justice, a saisi l'inspection générale de la justice (IGJ), sur le fondement de l'article 2 du décret n° 2016-1675 du 5 décembre 2016, afin qu'elle mène une enquête administrative sur le comportement professionnel de trois magistrats affectés au parquet national financier n'est pas susceptible de faire l'objet d'un recours pour excès de pouvoir.</ANA>
<ANA ID="9D"> 54-01-04-01-02 1) a) Un syndicat, chargé de défendre les intérêts collectifs des magistrats, est recevable à intervenir dans une instance au soutien de conclusions de magistrats contestant des sanctions ou des actes individuels défavorables les concernant. ......b) Il ne justifie en revanche pas d'un intérêt pour présenter en sa qualité des conclusions tendant à l'annulation de tels actes. ......2) Communiqué de presse du garde des sceaux, ministre de la justice, informant avoir demandé à l'inspection générale de la justice (IGJ) de mener une enquête administrative sur le comportement professionnel de trois magistrats affectés au parquet national financier.......Le syndicat de magistrats requérant n'est pas recevable à demander l'annulation du communiqué de presse litigieux en tant qu'il met en cause trois magistrats.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., sur la conformité des modalités de saisine de l'IGJ à la Constitution, CE, Section, 23 mars 2018, Syndicat Force ouvrière Magistrats et autres, n°s 406066 et autres, p. 75....[RJ2] Comp., s'agissant de la constitution d'une commission administrative chargée d'une enquête sur le fonctionnement du service public de la justice, CE, Section, 25 février 2005, Syndicat de la magistrature, n° 265482, p. 80. ...[RJ3] Cf. CE, 19 mars 1997, Mme Raud-Lefevre et Syndicat de la magistrature, n° 167677, p. 101, aux Tables sur un autre point.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
