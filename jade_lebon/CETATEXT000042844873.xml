<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042844873</ID>
<ANCIEN_ID>JG_L_2020_12_000000431544</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/84/48/CETATEXT000042844873.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 5ème chambres réunies, 30/12/2020, 431544</TITRE>
<DATE_DEC>2020-12-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>431544</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GASCHIGNARD ; SARL DIDIER, PINET</AVOCATS>
<RAPPORTEUR>Mme Catherine Moreau</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Stéphane Hoynck</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:431544.20201230</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Les communes de Saint-Laurent-du-Cros, de Gap, de Rambaud, la fédération départementale des syndicats d'exploitants agricoles des Hautes-Alpes, l'association " Les jeunes agriculteurs des Hautes-Alpes ", la coordination rurale du Rhône, la chambre régionale d'agriculture d'Auvergne-Rhône-Alpes, la chambre d'agriculture de l'Ain, la chambre d'agriculture de l'Ardèche, la chambre d'agriculture des Bouches-du-Rhône, la chambre d'agriculture de l'Isère, la chambre d'agriculture du Rhône, la chambre d'agriculture de Savoie Mont-Blanc, la fédération régionale des syndicats d'exploitants agricoles de Provence-Alpes-Côte d'Azur, la fédération régionale des syndicats d'exploitants agricoles de Rhône-Alpes, la fédération départementale des syndicats d'exploitants agricoles de l'Ain, la fédération départementale des syndicats d'exploitants agricoles de l'Isère, la fédération départementale des syndicats d'exploitants agricoles du Rhône, la fédération départementale des syndicats d'exploitants agricoles de Savoie et la fédération départementale des syndicats d'exploitants agricoles du Vaucluse ont demandé au tribunal administratif de Lyon d'annuler pour excès de pouvoir l'arrêté du 14 mars 2015 par lequel le préfet de la région Rhône-Alpes, préfet coordonnateur du bassin Rhône-Méditerranée, a délimité les zones vulnérables à la pollution par les nitrates d'origine agricole du bassin Rhône-Méditerranée et les décisions des 1er, 8 et 25 juin 2015 rejetant les différents recours gracieux formés à son encontre. Par un jugement n° 1505756, 1506796, 1507200, 1507231, 1507239, 1507621 du 22 juin 2017, le tribunal administratif a annulé cet arrêté.<br/>
<br/>
              Par un arrêt n° 17LY3235 du 9 avril 2019, la cour administrative d'appel de Lyon a rejeté l'appel formé par le ministre d'Etat, ministre de la transition écologique et solidaire contre ce jugement. <br/>
<br/>
              Par un pourvoi, enregistré le 11 juin 2019 au secrétariat du contentieux du Conseil d'Etat, le ministre d'Etat, ministre de la transition écologique et solidaire demande au Conseil d'Etat d'annuler cet arrêt.<br/>
<br/>
              Il soutient que l'arrêt de la cour administrative d'appel de Lyon qu'il attaque est entaché :<br/>
              - d'une erreur de droit en ce qu'il juge que la procédure de concertation conduite avec les chambres d'agriculture a méconnu l'article R. 211-77 du code de l'environnement ;<br/>
              - d'une dénaturation des pièces du dossier en ce qu'il estime que la procédure suivie a privé les personnes intéressées d'une garantie et a été susceptible d'exercer une influence sur le sens de la décision prise.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'environnement ;<br/>
              - le code rural et de la pêche maritime ;<br/>
              - le décret n° 2015-126 du 5 février 2015 ;<br/>
              - le code de justice administrative et le décret n° 2020-146 du 18 novembre 2020 ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme B... A..., conseillère d'Etat en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Stéphane Hoynck, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Gaschignard, avocat de la commune de Saint-Laurent-du-Cros et autre et à la SARL Didier, Pinet, avocat de la commune de Rambaud ;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 14 mars 2015, le préfet de la région Rhône-Alpes, préfet coordonnateur du bassin Rhône-Méditerranée, a défini la liste des zones vulnérables à la pollution par les nitrates d'origine agricole du bassin Rhône-Méditerranée. Le ministre chargé de la transition écologique et solidaire se pourvoit en cassation contre l'arrêt de la cour administrative de Lyon qui a rejeté son appel contre le jugement du tribunal administratif de Lyon ayant annulé cet arrêté.<br/>
<br/>
              2. Aux termes du I de l'article R. 211-77 du code de l'environnement : " Sont désignées comme zones vulnérables toutes les zones qui alimentent les eaux atteintes par la pollution par les nitrates ou susceptibles de l'être et qui contribuent à la pollution ou à la menace de pollution. / La désignation des zones vulnérables se fonde sur la teneur en nitrate des eaux douces et sur l'état d'eutrophisation des eaux douces superficielles, des eaux des estuaires, des eaux côtières et marines qui résultent du programme de surveillance prévu par l'article R. 211-76, tout en tenant compte des caractéristiques physiques et environnementales des eaux et des terres, des connaissances scientifiques et techniques ainsi que des résultats des programmes d'action pris en application des articles R. 211-80 à R. 211-84. / Peuvent également être désignées comme zones vulnérables certaines zones qui, sans répondre aux critères définis au premier alinéa, sont considérées comme telles afin de garantir l'efficacité des mesures des programmes d'action mentionnés à l'alinéa précédent. " Aux termes de l'article R. 211-77 du code de l'environnement, dans sa rédaction issue du décret du 22 mars 2007 relatif à la partie réglementaire du code de l'environnement, maintenue en vigueur jusqu'au 14 mars 2015 en application des dispositions transitoires de l'article 4 du décret du 5 février 2015 relatif à la désignation et à la délimitation des zones vulnérables en vue de la protection des eaux contre la pollution par les nitrates d'origine agricole : " Le préfet coordonnateur de bassin élabore, avec le concours des préfets de département, à partir des résultats obtenus par le programme de surveillance de la teneur des eaux en nitrates d'origine agricole et de toute autre donnée disponible, un projet de délimitation des zones vulnérables en concertation avec les organisations professionnelles agricoles, des représentants des usagers de l'eau, des communes et de leurs groupements, des personnes publiques ou privées qui concourent à la distribution de l'eau, des associations agréées de protection de l'environnement intervenant en matière d'eau et des associations de consommateurs. / Le préfet coordonnateur de bassin transmet le projet de délimitation des zones vulnérables aux préfets intéressés qui consultent les conseils généraux et les conseils régionaux et, en Corse, la collectivité territoriale, ainsi que les conseils départementaux de l'environnement et des risques sanitaires et technologiques et les chambres d'agriculture. / Le préfet coordonnateur de bassin arrête la délimitation des zones vulnérables après avis du comité de bassin. / Les avis sont réputés favorables s'ils n'interviennent pas dans un délai de deux mois à compter de la transmission de la demande d'avis. / (...) ".<br/>
<br/>
              3. Il résulte de l'article R. 211-77 du code de l'environnement cité précédemment que la procédure d'élaboration de l'arrêté par lequel le préfet coordonnateur de bassin procède à la délimitation des zones vulnérables aux pollutions par les nitrates comporte une phase d'élaboration d'un projet en concertation avec les acteurs énumérés, notamment les organisations professionnelles agricoles, puis une phase de consultation portant sur le projet de délimitation des zones vulnérables, cette dernière devant être effectuée auprès de personnes publiques et organismes énumérés, dont les chambres régionales d'agriculture. En jugeant que la circonstance que les organisations professionnelles agricoles sont représentées au sein des chambres d'agriculture ne permettait pas de les assimiler à ces dernières pour la mise en oeuvre de la procédure de concertation prévue par le premier alinéa de l'article R. 211-27 dès lors que les chambres d'agriculture constituent des organismes professionnels distincts des organisations professionnelles agricoles, la cour n'a pas entaché son arrêt d'erreur de droit.  <br/>
<br/>
              4. En second lieu, c'est par une appréciation souveraine exempte de dénaturation que la cour a estimé que l'absence de consultation des organisations professionnelles agricoles au cours de la phase de concertation avait privé les organisations professionnelles en cause d'une garantie et avait été susceptible d'exercer une influence sur le sens de l'arrêté contesté. <br/>
<br/>
              5. Il en résulte de ce qui précède que le pourvoi du ministre chargé de la transition écologique et solidaire doit être rejeté.  <br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement de la somme de 2500 euros à la commune de Rambaud et une somme globale de 2500 euros aux communes de Saint-Laurent-du-Cros et Gap au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi du ministre de la transition écologique et solidaire est rejeté.<br/>
Article 2 : L'Etat versera la somme de 2 500 euros à la commune de Rambaud, et la somme globale de 2 500 euros aux communes de Saint-Laurent-du-Cros et Gap au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la ministre de la transition écologique et aux communes de Rambaud, de Saint-Laurent-du Cros et de Gap. <br/>
Copie en sera adressée à la fédération départementale des syndicats d'exploitants agricoles des Hautes-Alpes, première dénommée pour l'ensemble des intimés devant la cour administrative d'appel de Lyon.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">03-01-01 AGRICULTURE ET FORÊTS. INSTITUTIONS AGRICOLES. CHAMBRES D'AGRICULTURE. - CARACTÈRE D' ORGANISATIONS PROFESSIONNELLES AGRICOLES POUR LA CONCERTATION PRÉALABLE À LA DÉLIMITATION DES ZONES VULNÉRABLES AUX POLLUTIONS PAR LES NITRATES (ART. R. 211-77 DU CODE DE L'ENVIRONNEMENT) - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">27-06 EAUX. - DÉLIMITATION DES ZONES VULNÉRABLES AUX POLLUTIONS PAR LES NITRATES - PROCÉDURE D'ÉLABORATION (ART. R. 211-77 DU CODE DE L'ENVIRONNEMENT) - CONCERTATION INCLUANT LES ORGANISATIONS PROFESSIONNELLES AGRICOLES - NOTION - CHAMBRES D'AGRICULTURE - EXCLUSION.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">44-05-02 NATURE ET ENVIRONNEMENT. DIVERS RÉGIMES PROTECTEURS DE L`ENVIRONNEMENT. LUTTE CONTRE LA POLLUTION DES EAUX (VOIR : EAUX). - DÉLIMITATION DES ZONES VULNÉRABLES AUX POLLUTIONS PAR LES NITRATES - PROCÉDURE D'ÉLABORATION (ART. R. 211-77 DU CODE DE L'ENVIRONNEMENT) - CONCERTATION INCLUANT LES ORGANISATIONS PROFESSIONNELLES AGRICOLES - NOTION - CHAMBRES D'AGRICULTURE - EXCLUSION.
</SCT>
<ANA ID="9A"> 03-01-01 Il résulte de l'article R. 211-77 du code de l'environnement que la procédure d'élaboration de l'arrêté par lequel le préfet coordonnateur de bassin procède à la délimitation des zones vulnérables aux pollutions par les nitrates comporte une phase d'élaboration d'un projet en concertation avec les acteurs énumérés, notamment les organisations professionnelles agricoles, puis une phase de consultation portant sur le projet de délimitation des zones vulnérables, cette dernière devant être effectuée auprès de personnes publiques et organismes énumérés, dont les chambres régionales d'agriculture.... ,,La circonstance que les organisations professionnelles agricoles sont représentées au sein des chambres d'agriculture ne permet pas de les assimiler à ces dernières pour la mise en oeuvre de la procédure de concertation prévue par le premier alinéa de l'article R. 211-27 dès lors que les chambres d'agriculture constituent des organismes professionnels distincts des organisations professionnelles agricoles.</ANA>
<ANA ID="9B"> 27-06 Il résulte de l'article R. 211-77 du code de l'environnement que la procédure d'élaboration de l'arrêté par lequel le préfet coordonnateur de bassin procède à la délimitation des zones vulnérables aux pollutions par les nitrates comporte une phase d'élaboration d'un projet en concertation avec les acteurs énumérés, notamment les organisations professionnelles agricoles, puis une phase de consultation portant sur le projet de délimitation des zones vulnérables, cette dernière devant être effectuée auprès de personnes publiques et organismes énumérés, dont les chambres régionales d'agriculture.... ,,La circonstance que les organisations professionnelles agricoles sont représentées au sein des chambres d'agriculture ne permet pas de les assimiler à ces dernières pour la mise en oeuvre de la procédure de concertation prévue par le premier alinéa de l'article R. 211-27 dès lors que les chambres d'agriculture constituent des organismes professionnels distincts des organisations professionnelles agricoles.</ANA>
<ANA ID="9C"> 44-05-02 Il résulte de l'article R. 211-77 du code de l'environnement que la procédure d'élaboration de l'arrêté par lequel le préfet coordonnateur de bassin procède à la délimitation des zones vulnérables aux pollutions par les nitrates comporte une phase d'élaboration d'un projet en concertation avec les acteurs énumérés, notamment les organisations professionnelles agricoles, puis une phase de consultation portant sur le projet de délimitation des zones vulnérables, cette dernière devant être effectuée auprès de personnes publiques et organismes énumérés, dont les chambres régionales d'agriculture.... ,,La circonstance que les organisations professionnelles agricoles sont représentées au sein des chambres d'agriculture ne permet pas de les assimiler à ces dernières pour la mise en oeuvre de la procédure de concertation prévue par le premier alinéa de l'article R. 211-27 dès lors que les chambres d'agriculture constituent des organismes professionnels distincts des organisations professionnelles agricoles.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
