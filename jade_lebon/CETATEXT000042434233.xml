<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042434233</ID>
<ANCIEN_ID>JG_L_2020_10_000000429357</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/43/42/CETATEXT000042434233.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 16/10/2020, 429357</TITRE>
<DATE_DEC>2020-10-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>429357</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN ; SCP SPINOSI, SUREAU ; SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Yves Doutriaux</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Guillaume Odinet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:429357.20201016</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme C... A... épouse B... et M. D... B... ont demandé au tribunal administratif de Bastia d'annuler pour excès de pouvoir l'arrêté en date du 8 juin 2017 par lequel le maire d'Ajaccio a délivré à la société Chemin de Trabacchina SAS un permis de construire un immeuble collectif comportant neuf logements au lieu-dit Trabacchina. <br/>
<br/>
              Par un jugement n° 1701027 et 1701049 du 31 janvier 2019, le tribunal administratif de Bastia a annulé ce permis de construire.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 2 avril et 2 juillet 2019 et 7 juillet 2020 au secrétariat du contentieux du Conseil d'Etat, la société Chemin de Trabacchina SAS demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter les conclusions de M. et Mme B... devant le tribunal administratif ; <br/>
<br/>
              3°) de mettre à la charge de M. et Mme B... la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Yves Doutriaux, conseiller d'Etat,<br/>
<br/>
              - les conclusions de M. Guillaume Odinet, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la société Chemin de Trabacchina, à la SCP Piwnica, Molinié, avocat de M. et Mme B... et à la SCP Spinosi, Sureau, avocat de la commune d'Ajaccio ; <br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :    <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par arrêté du 8 juin 2017, le maire d'Ajaccio a délivré à la société Chemin de Trabacchina SAS un permis de construire un immeuble collectif comportant neuf logements au lieu-dit Trabacchina sur le territoire de la commune. M. et Mme B... ont saisi le tribunal administratif de Bastia d'un recours dirigé contre cet arrêté. Par un jugement du 31 janvier 2019, le tribunal administratif a annulé pour excès de pouvoir ce permis de construire. La société Chemin de Trabacchina SAS se pourvoit en cassation contre ce jugement.<br/>
<br/>
              2. L'article R.* 600-2 du code de l'urbanisme dispose que : " Le délai de recours contentieux à l'encontre d'une décision de non-opposition à une déclaration préalable ou d'un permis de construire, d'aménager ou de démolir court à l'égard des tiers à compter du premier jour d'une période continue de deux mois d'affichage sur le terrain des pièces mentionnées à l'article R. 424-15 ". Aux termes de l'article R.* 424-15 du même code : " Mention du permis explicite ou tacite ou de la déclaration préalable doit être affichée sur le terrain, de manière visible de l'extérieur, par les soins de son bénéficiaire, dès la notification de l'arrêté (...) / Un arrêté du ministre chargé de l'urbanisme règle le contenu et les formes de l'affichage ". L'article A. 424-16 de ce code dans sa rédaction applicable au litige dispose que : " Le panneau prévu à l'article A. 424-1 indique le nom, la raison sociale ou la dénomination sociale du bénéficiaire, la date et le numéro du permis, la nature du projet et la superficie du terrain ainsi que l'adresse de la mairie où le dossier peut être consulté (...) ".<br/>
<br/>
              3. En imposant que figurent sur le panneau d'affichage du permis de construire diverses informations sur le permis et le lieu de consultation du dossier, les dispositions citées au point précédent ont notamment pour objet de mettre les tiers à même de consulter le dossier du permis. Il s'ensuit que, si les mentions relatives à l'identification du permis et au lieu de consultation du dossier prévues par l'article A. 424-16 du code de l'urbanisme doivent, en principe, figurer sur le panneau d'affichage, une erreur ou omission entachant l'une d'entre elles ne conduit à faire obstacle au déclenchement du délai de recours que dans le cas où cette erreur est de nature à affecter la capacité des tiers à identifier, à la seule lecture du panneau d'affichage, le permis et l'administration à laquelle il convient de s'adresser pour consulter le dossier. <br/>
<br/>
              4. Pour juger que l'affichage du permis de construire litigieux sur le terrain n'était pas régulier et n'avait pu ainsi déclencher le délai de recours contentieux à l'égard des tiers, le tribunal administratif de Bastia a relevé que le panneau ne mentionnait pas l'adresse de la mairie où le dossier pouvait être consulté et que, compte tenu de la taille de la commune d'Ajaccio et de la dispersion des services municipaux sur le territoire de la commune, une telle mention revêtait un caractère substantiel. En statuant ainsi, alors qu'en mentionnant la mairie d'Ajaccio le panneau d'affichage renseignait les tiers sur l'administration à laquelle s'adresser, le tribunal administratif a commis une erreur de droit.<br/>
<br/>
              5. Il résulte de tout ce qui précède, et sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, que la société Chemin de Trabacchina SAS est fondée à demander l'annulation du jugement qu'elle attaque.<br/>
<br/>
              6. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de cette société, qui n'est pas, dans la présente instance, la partie perdante, la somme que M. et Mme B... demandent à ce titre. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de ces derniers une somme de 3 000 euros à verser à la société Chemin de Trabacchina SAS, au titre de ces dispositions.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le jugement du tribunal administratif de Bastia en date du 31 janvier 2019 est annulé.<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Bastia.<br/>
<br/>
Article 3 : M. et Mme B... verseront la somme de 3 000 euros à la société Chemin de Trabacchina SAS au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions présentées par M. et Mme B... au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 5 : La présente décision sera notifiée à la société Chemin de Trabacchina SAS, à Mme C... A... épouse B... et à M. D... B....<br/>
Copie en sera adressée à la commune d'Ajaccio.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-01-07-02-02-04 PROCÉDURE. - INTRODUCTION DE L'INSTANCE. - DÉLAIS. - POINT DE DÉPART DES DÉLAIS. - PUBLICATION. - AFFICHAGE. - PERMIS DE CONSTRUIRE - AFFICHAGE COMPLET ET RÉGULIER SUR LE TERRAIN (R. -  600-2 DU CODE DE L'URBANISME) - 1) OBJET [RJ1] - INFORMATIONS VISANT NOTAMMENT À METTRE LES TIERS À MÊME DE CONSULTER LE DOSSIER - 2) CONSÉQUENCE - ERREURS ENTACHANT LES MENTIONS RELATIVES À L'IDENTIFICATION DU PERMIS ET AU LIEU DE CONSULTATION DU DOSSIER (ART. A. 424-16 DU CODE DE L'URBANISME) - ERREURS SUSCEPTIBLES DE FAIRE OBSTACLE AU DÉCLENCHEMENT DU DÉLAI - ABSENCE, SAUF SI L'ERREUR EST DE NATURE À AFFECTER LA CAPACITÉ DES TIERS À IDENTIFIER LE PERMIS ET L'ADMINISTRATION À LAQUELLE IL CONVIENT DE S'ADRESSER POUR CONSULTER LE DOSSIER [RJ2] - 3) ILLUSTRATION - ABSENCE DE MENTION DE L'ADRESSE DE LA MAIRIE - OMISSION INSUSCEPTIBLE DE FAIRE OBSTACLE AU DÉCLENCHEMENT DU DÉLAI.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">68-06-01-03-01 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. - RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. - INTRODUCTION DE L'INSTANCE. - DÉLAIS DE RECOURS. - POINT DE DÉPART DU DÉLAI. - PERMIS DE CONSTRUIRE - AFFICHAGE COMPLET ET RÉGULIER SUR LE TERRAIN (R. -  600-2 DU CODE DE L'URBANISME) - 1) OBJET [RJ1] - INFORMATIONS VISANT NOTAMMENT À METTRE LES TIERS À MÊME DE CONSULTER LE DOSSIER - 2) CONSÉQUENCE - ERREURS ENTACHANT LES MENTIONS RELATIVES À L'IDENTIFICATION DU PERMIS ET AU LIEU DE CONSULTATION DU DOSSIER (ART. A. 424-16 DU CODE DE L'URBANISME) - ERREURS SUSCEPTIBLES DE FAIRE OBSTACLE AU DÉCLENCHEMENT DU DÉLAI - ABSENCE, SAUF SI L'ERREUR EST DE NATURE À AFFECTER LA CAPACITÉ DES TIERS À IDENTIFIER LE PERMIS ET L'ADMINISTRATION À LAQUELLE IL CONVIENT DE S'ADRESSER POUR CONSULTER LE DOSSIER [RJ2] - 3) ILLUSTRATION - ABSENCE DE MENTION DE L'ADRESSE DE LA MAIRIE - OMISSION INSUSCEPTIBLE DE FAIRE OBSTACLE AU DÉCLENCHEMENT DU DÉLAI.
</SCT>
<ANA ID="9A"> 54-01-07-02-02-04 1) En imposant que figurent sur le panneau d'affichage du permis de construire diverses informations sur le permis et le lieu de consultation du dossier, les articles R.* 600-2, R.* 424-15 et A. 424-16 du code de l'urbanisme ont notamment pour objet de mettre les tiers à même de consulter le dossier du permis.... ...2) Il s'ensuit que, si les mentions relatives à l'identification du permis et au lieu de consultation du dossier prévues par l'article A. 424-16 du code de l'urbanisme doivent, en principe, figurer sur le panneau d'affichage, une erreur ou omission entachant l'une d'entre elles ne conduit à faire obstacle au déclenchement du délai de recours que dans le cas où cette erreur est de nature à affecter la capacité des tiers à identifier, à la seule lecture du panneau d'affichage, le permis et l'administration à laquelle il convient de s'adresser pour consulter le dossier. ... ...3) Panneau ne mentionnant pas l'adresse de la mairie où le dossier pouvait être consulté.... ...Une telle omission n'entache pas d'irrégularité l'affichage du permis dès lors qu'en mentionnant la mairie, le panneau d'affichage renseignait les tiers sur l'administration à laquelle s'adresser. Par suite, cette omission ne fait pas obstacle au déclenchement du délai de recours contentieux à l'égard des tiers.</ANA>
<ANA ID="9B"> 68-06-01-03-01 1) En imposant que figurent sur le panneau d'affichage du permis de construire diverses informations sur le permis et le lieu de consultation du dossier, les articles R.* 600-2, R.* 424-15 et A. 424-16 du code de l'urbanisme ont notamment pour objet de mettre les tiers à même de consulter le dossier du permis.... ...2) Il s'ensuit que, si les mentions relatives à l'identification du permis et au lieu de consultation du dossier prévues par l'article A. 424-16 du code de l'urbanisme doivent, en principe, figurer sur le panneau d'affichage, une erreur ou omission entachant l'une d'entre elles ne conduit à faire obstacle au déclenchement du délai de recours que dans le cas où cette erreur est de nature à affecter la capacité des tiers à identifier, à la seule lecture du panneau d'affichage, le permis et l'administration à laquelle il convient de s'adresser pour consulter le dossier. ... ...3) Panneau ne mentionnant pas l'adresse de la mairie où le dossier pouvait être consulté.... ...Une telle omission n'entache pas d'irrégularité l'affichage du permis dès lors qu'en mentionnant la mairie, le panneau d'affichage renseignait les tiers sur l'administration à laquelle s'adresser. Par suite, cette omission ne fait pas obstacle au déclenchement du délai de recours contentieux à l'égard des tiers.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]  Cf., F2 sur l'objet général de ces règles d'affichage, CE, 9 mars 2016, Commune de Chapet, n° 384341, T. p. 996. Rappr., sur leur objet s'agissant de la connaissance du projet lui-même, CE, 25 février 2019, M. et Mme Vanoverberghe, n° 416610, T. p. 1076....[RJ2] Rappr., s'agissant d'erreurs affectant l'appréciation de l'importance et de la consistance du projet, CE, 16 octobre 2019, M. et Mme Gaillard et M. et Mme Tepelian, n° 419756, T. pp. 901-1075.Rappr., s'agissant d'erreurs affectant l'appréciation de l'importance et de la consistance du projet, CE, 16 octobre 2019, M. et Mme Gaillard et M. et Mme Tepelian, n° 419756, T. pp. 901-1075.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
