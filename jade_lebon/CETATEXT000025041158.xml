<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025041158</ID>
<ANCIEN_ID>JG_L_2011_12_000000347415</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/04/11/CETATEXT000025041158.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème et 10ème sous-sections réunies, 23/12/2011, 347415, Publié au recueil Lebon</TITRE>
<DATE_DEC>2011-12-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>347415</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème et 10ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Philippe Martin</PRESIDENT>
<AVOCATS>SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>Mme Pauline Flauss</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Pierre Collin</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2011:347415.20111223</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu 1°), sous le n° 347 415, la protestation, enregistrée le 11 mars 2011 au secrétariat du contentieux du Conseil d'Etat, présentée par M. Philippe AH, demeurant ... ; M. AH demande au Conseil d'Etat, après dessaisissement du tribunal administratif de Lille, en application des articles R. 120 et R. 121 du code électoral, de la demande dont il l'avait saisi, d'annuler l'élection des adjoints au maire de la commune de Dunkerque, à laquelle il a été procédé par délibération du conseil municipal de la commune du 3 janvier 2011 ;<br/>
<br/>
<br/>
              Vu 2°), sous le n° 347741, l'ordonnance n° 1100059-2 du 16 mars 2011, enregistrée le 23 mars 2011 au secrétariat du contentieux du Conseil d'Etat, par laquelle le président du tribunal administratif de Lille a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, la protestation présentée à ce tribunal par M. Philippe AH, demeurant ...;<br/>
<br/>
              Vu la protestation, enregistrée au greffe du tribunal administratif de Lille le 6 janvier 2011, présentée par M. AH et tendant à l'annulation de l'élection des adjoints au maire de Dunkerque, à laquelle il a été procédé par délibération du conseil municipal de la commune en date du 3 janvier 2011 ;<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu le code électoral ; <br/>
<br/>
              Vu le code général des collectivités territoriales ; <br/>
<br/>
              Vu la loi no 2010-1563 du 16 décembre 2010 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Pauline Flauss, chargée des fonctions de Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Lyon-Caen, Thiriez, avocat de la commune de Dunkerque, <br/>
<br/>
              - les conclusions de M. Pierre Collin, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Lyon-Caen, Thiriez, avocat de la commune de Dunkerque ;<br/>
<br/>
<br/>
<br/>Considérant que l'ordonnance du 16 mars 2011, enregistrée sous le n° 347741, constitue en réalité la transmission par le président du tribunal administratif de Lille, dessaisi en application des dispositions des articles R. 120 et R. 121 du code électoral, du dossier de la protestation formée devant cette juridiction par M. Philippe AH à l'encontre de l'élection des adjoints au maire de Dunkerque et renouvelée devant le Conseil d'Etat par une requête enregistrée sous le n° 347415 ; que, par suite, cette ordonnance et les documents afférents doivent être rayés des registres du secrétariat du contentieux du Conseil d'Etat et être joints à la protestation enregistrée sous le n° 347415 ;<br/>
<br/>
              Considérant que par arrêté du 8 décembre 2010, le préfet du Nord a prononcé, à compter du 9 décembre suivant, la fusion des communes de Dunkerque, Saint-Pol-sur-Mer et Fort-Mardyk avec création des communes associées de Saint-Pol-sur-Mer et de Fort-Mardyk ; que cet arrêté prévoit que la nouvelle commune, prenant le nom de Dunkerque, est administrée jusqu'au prochain renouvellement par un conseil municipal comprenant, comme le permettent les articles L. 2113-6 et L. 2113-7 du code général des collectivités territoriales, des membres des anciennes assemblées ; que par délibération n° 5 du 3 janvier 2011, le conseil municipal de la commune nouvelle de Dunkerque a créé 46 postes d'adjoints correspondant à la somme des adjoints, adjoints de quartier et adjoints spéciaux des communes fusionnées ; que par délibération n° 6 du même jour, le conseil municipal a procédé à l'élection ces adjoints ; que M. AH, membre du conseil municipal de la commune nouvelle de Dunkerque, a contesté cette élection devant le tribunal administratif de Lille puis, après dessaisissement de ce dernier, devant le Conseil d'Etat ;<br/>
<br/>
              Sur la fin de non-recevoir opposée à la protestation : <br/>
<br/>
              Considérant que M. AH est recevable à demander l'annulation de l'élection de tous les adjoints au maire, quand bien même certains de ses griefs sont tirés de ce que le nombre d'adjoints élus excèderait le maximum légal ;<br/>
<br/>
              Sur la régularité de l'élection, sans qu'il soit besoin d'examiner les autres griefs de la protestation :<br/>
<br/>
              Considérant qu'aux termes de l'article L. 2122-2 du code général des collectivités territoriales : "Le conseil municipal détermine le nombre des adjoints au maire sans que ce nombre puisse excéder 30 % de l'effectif légal du conseil municipal" ; qu'aux termes de l'article L. 2122-2-1 du même code : "Dans les communes de 80 000 habitants et plus, la limite fixée à l'article L. 2122-2 peut donner lieu à dépassement en vue de la création de postes d'adjoints chargés principalement d'un ou plusieurs quartiers, sans toutefois que le nombre de ceux-ci puisse excéder 10 % de l'effectif légal du conseil municipal" ; qu'aux termes de l'article L. 2122-3 : "Lorsqu'un obstacle quelconque, ou l'éloignement, rend difficiles, dangereuses ou momentanément impossibles les communications entre le chef-lieu et une fraction de commune, un poste d'adjoint spécial peut être institué par délibération motivée du conseil municipal. / Un ou plusieurs postes d'adjoint spécial peuvent également être institués en cas de fusion de communes" ;<br/>
<br/>
              Considérant qu'en application des dispositions précitées, le conseil municipal de Dunkerque, valablement composé de 74 membres en application des dispositions des articles L. 2113-6 et L. 2113-7 du code général des collectivités territoriales, pouvait prévoir la désignation d'un nombre maximal de 22 adjoints au maire et de 7 adjoints de quartier, ainsi que d'adjoints spéciaux ; qu'en procédant à l'élection de 32 adjoints, outre 4 adjoints de quartier et 10 adjoints spéciaux, le conseil municipal a méconnu ces dispositions ;<br/>
<br/>
              Considérant que les adjoints au maire dont l'élection est contestée soutiennent cependant qu'ils seraient devenus de plein droit adjoints au maire, par application des dispositions prévoyant leur intégration dans le nouveau conseil municipal du fait de leur qualité d'adjoints au maire de l'une des anciennes communes ;<br/>
<br/>
              Considérant, d'une part, qu'aux termes de l'article L. 2113-6 du code général des collectivités territoriales, dans sa rédaction demeurée applicable en l'espèce en vertu des dispositions du I de l'article 25 de la loi du 16 décembre 2010 de réforme des collectivités territoriales : "L'acte qui prononce la fusion de deux ou plusieurs communes peut prévoir que la nouvelle commune est, sous réserve de l'accord préalable des conseils municipaux et jusqu'au prochain renouvellement, administrée par un conseil où entrent tout ou partie des membres en exercice des anciennes assemblées et, dans tous les cas, le maire et les adjoints de chacune d'entre elles. / L'effectif total du conseil ne peut dépasser soixante-neuf membres, sauf dans les cas où l'intégration des maires et adjoints des anciennes communes rend nécessaire l'attribution de sièges complémentaires" ; que, d'autre part, l'article L. 2122-4 du même code prévoit que le conseil municipal élit le maire et les adjoints parmi ses membres, au scrutin secret, et l'article L. 2122-10 dispose : "Le maire et les adjoints sont élus pour la même durée que le conseil municipal (...). / Quand il y a lieu, pour quelque cause que ce soit, à une nouvelle élection du maire, il est procédé à une nouvelle élection des adjoints. (...)" ; qu'il résulte de la combinaison de ces dispositions que, bien que la fusion de communes entraîne nécessairement la création d'une commune nouvelle, le législateur a entendu permettre, dans l'attente du plus prochain renouvellement général des conseils municipaux, et lorsque les conseils municipaux des anciennes communes l'ont souhaité, d'éviter la tenue de nouvelles élections municipales en prévoyant que le conseil municipal de la nouvelle collectivité puisse être composé de membres des anciennes assemblées, en nombre proportionnel à celui des électeurs inscrits dans les anciennes communes et comprenant au moins les maires et adjoints de ces dernières ; que, toutefois, ces règles transitoires n'ont ni pour objet ni pour effet de conférer aux maires et adjoints des anciennes communes membres du nouveau conseil municipal la qualité d'adjoints au maire, d'adjoints spéciaux ou d'adjoints de quartier de la nouvelle commune ; qu'en l'absence de dispositions législatives spécifiques relatives à la désignation de l'exécutif de la commune issue d'une fusion, il appartient au conseil municipal de la commune nouvelle de procéder à l'élection de son maire et de ses adjoints ; qu'il suit de là que ni la convention déterminant les modalités de la fusion des communes en application de l'article L. 2113-12 du code général des collectivités territoriales, ni les délibérations du conseil municipal de la nouvelle commune ne pouvaient prévoir que les membres du conseil municipal ayant antérieurement la qualité d'adjoint au maire de l'une des anciennes communes auraient de ce seul fait la qualité d'adjoint au maire de la nouvelle commune et, par suite, fixer un nombre de postes d'adjoints supérieur à celui qui résulte de l'application des dispositions des articles L. 2122-2 et L. 2122-2-1 précités du code général des collectivités territoriales ;<br/>
<br/>
              Considérant qu'il résulte de ce qui précède que l'élection des adjoints, adjoints spéciaux et adjoints de quartier de la commune de Dunkerque est intervenue au terme d'une procédure irrégulière, qui a altéré le résultat du scrutin dans sa totalité ; qu'elle doit par suite être annulée ;<br/>
<br/>
              Sur les conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de M. AH la somme que la commune de Dunkerque, qui n'est pas partie à l'instance, demande au titre des frais exposés par elle et non compris dans les dépens ; qu'elles font, de même, obstacle à ce que soit mise à la charge de la commune la somme que M. AH demande au même titre ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er :. Les pièces enregistrées sous le n° 347 741 seront rayées des registres du secrétariat du contentieux du Conseil d'Etat pour être jointes à la protestation n° 347 415.<br/>
Article 2 : L'élection des adjoints de la commune de Dunkerque est annulée. <br/>
Article 3 : Le surplus de la protestation de M. AH est rejeté. <br/>
Article 4 : Les conclusions de la commune de Dunkerque tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5  : La présente décision sera notifiée à M. Philippe AH à M. Alain A, à Mme Danièle AC, à M. Jean-Pierre AM, à M. Marcel O, à Mme Gracienne AE, à M. Jacques C, à M. François AP, à M. Wulfran L, à Mme Joëlle P, à M. Salim AU, à Mme Marie AT, à M. Philippe D, à Mme Marianne Q, à M. Vincent R, à Mme Marie-Noëlle AS, à Mme Jeanne S, à M. Patrice H, à Mme Zoé AR, à Mme Claudine I, à Mme Monique AG, à M. Jean AB, à Mme Marie-Paule le Garrec, à M. René N, à Mme Chantal AN, à M. René AF, à Mme Delphine AA, à M. Patrice AD, à Mme Cécile B, à M. René Z, à Mme Florence K, à M. Jean-Marie Y, à M. Etienne X, à Mme Thérèse F, à Mme Isabelle E, à M. Jean-Aimé AL, à Mme Murielle AQ, à M. Jacques AK, à M. Georges AJ, à M. Christian W, à M. Bernard V, à M. Daniel AI, à Mme Anne-Marie M, à M. Fabrice AO, à M. Louardi G, à M. Claude U et à Mme Karima T.<br/>
Copie en sera adressée à la commune de Dunkerque.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">135-02-01-02-02-01 COLLECTIVITÉS TERRITORIALES. COMMUNE. ORGANISATION DE LA COMMUNE. ORGANES DE LA COMMUNE. MAIRE ET ADJOINTS. DISPOSITIONS GÉNÉRALES. - FUSION DE COMMUNES - RÈGLES DE FIXATION DU NOMBRE D'ADJOINTS (ARTICLES L. 2122-2, L. 2122-2-1 ET L. 2122-3 DU CGCT) - POSSIBILITÉ D'Y DÉROGER EN PRÉVOYANT, PAR CONVENTION OU PAR DÉLIBÉRATION DU CONSEIL MUNICIPAL DE LA NOUVELLE COMMUNE, QUE LES ADJOINTS DES ANCIENNES COMMUNES ACQUIÈRENT DE CE SEUL FAIT LA QUALITÉ D'ADJOINT AU MAIRE DE LA NOUVELLE COMMUNE - ABSENCE.
</SCT>
<ANA ID="9A"> 135-02-01-02-02-01 Les articles L. 2122-2, L. 2122-2-1 et L. 2122-3 du code général des collectivités territoriales (CGCT) prévoient les règles de fixation du nombre maximal d'adjoints au maire. Les règles transitoires prévues par l'article L. 2113-6 du même code n'ont ni pour objet ni pour effet de conférer, en cas de fusion de communes, aux maires et adjoints des anciennes communes membres du nouveau conseil municipal la qualité d'adjoints au maire, d'adjoints spéciaux ou d'adjoints de quartier de la nouvelle commune. En l'absence de dispositions législatives spécifiques relatives à la désignation de l'exécutif de la commune issue d'une fusion, il appartient au conseil municipal de la commune nouvelle de procéder à l'élection de son maire et de ses adjoints. Par suite, ni la convention déterminant les modalités de la fusion des communes en application de l'article L. 2113-12 du CGCT, ni les délibérations du conseil municipal de la nouvelle commune ne peuvent légalement prévoir que les membres du conseil municipal ayant antérieurement la qualité d'adjoint au maire de l'une des anciennes communes auraient de ce seul fait la qualité d'adjoint au maire de la nouvelle commune et, par suite, fixer un nombre de postes d'adjoints supérieur à celui qui résulte de l'application des dispositions des articles L. 2122-2 et L. 2122-2-1 du CGCT.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
