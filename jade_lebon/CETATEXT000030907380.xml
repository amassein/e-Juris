<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030907380</ID>
<ANCIEN_ID>JG_L_2015_06_000000383203</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/90/73/CETATEXT000030907380.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème / 2ème SSR, 17/06/2015, 383203, Publié au recueil Lebon</TITRE>
<DATE_DEC>2015-06-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>383203</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème / 2ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, BLANCPAIN, SOLTNER, TEXIDOR ; SCP SEVAUX, MATHONNET</AVOCATS>
<RAPPORTEUR>M. Vincent Montrieux</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:383203.20150617</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société des autoroutes Paris-Rhin-Rhône (APRR) a demandé au tribunal administratif de Dijon de condamner la société Trabet à l'indemniser du préjudice résultant de désordres affectant l'autoroute A6 à la suite de la réfection de la chaussée réalisée par cette société. <br/>
<br/>
              Par un jugement n° 0701705 du 7 février 2013, le tribunal administratif de Dijon a condamné la société Trabet à verser à la société APRR la somme de 148 462,50 euros ainsi que la moitié des frais d'expertise.<br/>
<br/>
              Par un arrêt n° 13LY00617 du 28 mai 2014, la cour administrative d'appel de Lyon a annulé le jugement du 7 février 2013 du tribunal administratif de Dijon, rejeté la demande de la société APRR et mis à la charge de cette dernière la totalité des frais d'expertise.  <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique enregistrés les 28 juillet, 24 octobre 2014 et 27 février 2015 au secrétariat du contentieux du Conseil d'Etat, la société APRR demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de la société Trabet ; <br/>
<br/>
              3°) de mettre à la charge de la société Trabet le versement de la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code civil ;<br/>
              - le code des marchés publics ;<br/>
              - la décision n° 3984 du Tribunal des conflits du 9 mars 2015 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Vincent Montrieux, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Blancpain, Soltner, Texidor, avocat de la société des autoroutes Paris-Rhin-Rhône, et à la SCP Sevaux, Mathonnet, avocat de la société Trabet ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par acte d'engagement du 16 août 2001, la société des autoroutes Paris-Rhin-Rhône (APRR) a confié à la société Trabet le marché de réfection de voies de la section Avallon-Pouilly de l'autoroute A6 ; que la réception a été prononcée le 30 août 2002 ; qu'au cours de l'année 2005, la chaussée a été affectée de fissurations longitudinales et transversales ; que la société APRR a demandé au tribunal administratif de Dijon de condamner la société Trabet à réparer le préjudice résultant de ces désordres ; que par l'arrêt attaqué du 28 mai 2014, la cour administrative d'appel de Lyon a annulé le jugement par lequel le tribunal administratif de Dijon avait partiellement accueilli cette demande, rejeté la demande de la société APRR et mis à sa charge les frais d'expertise ; <br/>
<br/>
              2. Considérant qu'une société concessionnaire d'autoroute qui conclut avec une autre personne privée un contrat ayant pour objet la construction, l'exploitation ou l'entretien de l'autoroute ne peut, en l'absence de conditions particulières, être regardée comme ayant agi pour le compte de l'Etat ; que les litiges nés de l'exécution de ce contrat ressortissent à la compétence des juridictions de l'ordre judiciaire ; que cependant, ainsi que l'a jugé le Tribunal des conflits par décision du 9 mars 2015, la nature juridique d'un contrat s'appréciant à la date à laquelle il a été conclu, les contrats conclus par une société concessionnaire antérieurement au 9 mars 2015 sous le régime des contrats administratifs demeurent régis par le droit public et continuent de relever des juridictions de l'ordre administratif; que par suite, le présent litige relève de la compétence de la juridiction administrative ;<br/>
<br/>
              3. Considérant qu'aux termes de l'article 44.1 du cahier des clauses administratives générales applicable aux marchés publics de travaux : " Le délai de garantie est, sauf stipulation différente du marché et sauf prolongation décidée comme il est dit au 2 du présent article, d'un an à compter de la date d'effet de la réception, ou de six mois à compter de cette date si le marché ne concerne que des travaux d'entretien ou des terrassements. Pendant le délai de garantie, indépendamment des obligations qui peuvent résulter pour lui de l'application du 4 de l'article 41, l'entrepreneur est tenu à une obligation dite "obligation de parfait achèvement" au titre de laquelle il doit : / a) Exécuter les travaux ou prestations éventuels de finition ou de reprise prévus aux 5 et 6 de l'articles 41 ; / b) Remédier à tous les désordres signalés par le maître de l'ouvrage ou le maître d'oeuvre, de telle sorte que l'ouvrage soit conforme à l'état où il était lors de la réception ou après correction des imperfections constatées lors de celle-ci ; / c) Procéder, le cas échéant, aux travaux confortatifs ou modificatifs dont la nécessité serait apparue à l'issue des épreuves effectuées conformément au CCAP ; / d) Remettre au maître d'oeuvre les plans des ouvrages conformes à l'exécution dans les conditions précisées à l'article 40. / Les dépenses correspondant aux travaux complémentaires prescrits par le maître de l'ouvrage ou le maître d'oeuvre ayant pour l'objet de remédier aux déficient énoncées aux b et c ci-dessus ne sont à la charge de l'entrepreneur que si la cause de ces déficiences lui est imputable. / L'obligation de parfait achèvement ne s'étend pas aux travaux nécessaires pour remédier aux effets de l'usage ou de l'usure normale. " ; qu'aux termes de l'article 9.5.1 du cahier des clauses administratives particulières : " par dérogation à l'article 44.1 du cahier des clauses administratives générales, le délai de garantie contractuelle est fixé à cinq (5) ans à compter de la réception, pour les travaux devant respecter les obligations de résultat  particulières (...). Si pendant la période de garantie contractuelle, il est constaté que les obligations de résultats particulières fixés au C.C.T.P. ne sont pas respectées, l'entrepreneur, suivant les conditions fixées au C.C.T.P. et aux articles 44.1 et 44.2 du C.C.A.G. peut se voir soit appliquer des réfactions de prix, soit être mis en demeure d'effectuer des réparations en supportant les frais correspondants, soit être mis en demeure de démolir et reconstruire les ouvrages incriminés en supportant les frais correspondants " ;<br/>
<br/>
              4. Considérant que, pour écarter la responsabilité de la société Trabet sur le fondement de la garantie de parfait achèvement, la cour a relevé, d'une part, que le matériau utilisé, dont il n'est pas contesté par les parties qu'il soit à l'origine des désordres, avait fait l'objet d'un contrôle technique agrémenté d'un contrôle de qualité et que la société APRR, maître d'ouvrage et maître d'oeuvre, avait expressément approuvé cette proposition de  l'entrepreneur et l'avait incluse dans le marché, d'autre part, que la société Trabet n'avait commis aucune faute dans la mise en oeuvre de ce produit ; <br/>
<br/>
              5. Considérant qu'en statuant ainsi, alors que les circonstances que l'utilisation d'un matériau a été acceptée par le maître de l'ouvrage, sur proposition de l'entrepreneur, et que celui-ci n'avait pas connaissance des défauts de ce matériau à la date des travaux ne sont pas de nature à exonérer entièrement l'entrepreneur de son obligation de remédier aux désordres imputables à l'insuffisante qualité du matériau, sur le fondement de la garantie de parfait achèvement, la cour a commis une erreur de droit ; que, par suite et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, son arrêt doit être annulé ;<br/>
<br/>
              6. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société Trabet le versement à la société APRR de la somme de 3 000 euros, au titre des dispositions de l'article L. 761-1 du code de justice administrative ; qu'en revanche, ces dispositions font obstacle à ce qu'une somme soit mise à la charge de la société APRR qui n'est pas la partie perdante dans la présente instance ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative de Lyon du 28 mai 2014 est annulé. <br/>
Article 2 : L'affaire est renvoyée à la cour administrative de Lyon. <br/>
Article 3 : La société Trabet versera à la société des autoroutes Paris-Rhin-Rhône une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions présentées par la société Trabet au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 5 : La présente décision sera notifiée à la société des autoroutes Paris-Rhin-Rhône et à la société Trabet.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">17-03-02-03 COMPÉTENCE. RÉPARTITION DES COMPÉTENCES ENTRE LES DEUX ORDRES DE JURIDICTION. COMPÉTENCE DÉTERMINÉE PAR UN CRITÈRE JURISPRUDENTIEL. CONTRATS. - MODULATION DANS LE TEMPS DU REVIREMENT DE JURISPRUDENCE ABANDONNANT LA JURISPRUDENCE PEYROT - APPLICATION PAR LE CONSEIL D'ETAT DE LA MODULATION DÉCIDÉE PAR LE TRIBUNAL DES CONFLITS DANS SA DÉCISION DE REVIREMENT.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">39-06-01 MARCHÉS ET CONTRATS ADMINISTRATIFS. RAPPORTS ENTRE L'ARCHITECTE, L'ENTREPRENEUR ET LE MAÎTRE DE L'OUVRAGE. RESPONSABILITÉ DES CONSTRUCTEURS À L'ÉGARD DU MAÎTRE DE L'OUVRAGE. - GARANTIE DE PARFAIT ACHÈVEMENT - PORTÉE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-07-09 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. - MODULATION DANS LE TEMPS DU REVIREMENT DE JURISPRUDENCE ABANDONNANT LA JURISPRUDENCE PEYROT - APPLICATION PAR LE CONSEIL D'ETAT DE LA MODULATION DÉCIDÉE PAR LE TRIBUNAL DES CONFLITS DANS SA DÉCISION DE REVIREMENT.
</SCT>
<ANA ID="9A"> 17-03-02-03 Reprenant à son compte l'abandon de la jurisprudence Peyrot  (TC, 8 juillet 1963, Société Entreprise Peyrot c/ société de l'autoroute Estérel-Côte-d'Azur, n° 01804, p. 787) par le Tribunal des conflits par une décision Mme Rispal c/ Société Autoroutes du Sud de la France n° 3984 du 9 mars 2015, le Conseil d'Etat vise et se réfère explicitement à cette décision en tant qu'elle fixe les modalités d'application dans le temps de la nouvelle jurisprudence.</ANA>
<ANA ID="9B"> 39-06-01 Garantie de parfait achèvement (art. 44.1 cahier des clauses administratives générales applicable aux marchés publics de travaux). La circonstance que l'utilisation du matériau à l'origine du désordre a été acceptée par le maître de l'ouvrage sur proposition de l'entrepreneur, et celle que celui-ci n'avait pas connaissance des défauts de ce matériau à la date des travaux ne sont pas de nature à exonérer entièrement l'entrepreneur de son obligation de remédier aux désordres imputables à l'insuffisante qualité du matériau, sur le fondement de la garantie de parfait achèvement.</ANA>
<ANA ID="9C"> 54-07-09 Reprenant à son compte l'abandon de la jurisprudence Peyrot  (TC, 8 juillet 1963, Société Entreprise Peyrot c/ société de l'autoroute Estérel-Côte-d'Azur, n° 01804, p. 787) par le Tribunal des conflits par une décision Mme Rispal c/ Société Autoroutes du Sud de la France n° 3984 du 9 mars 2015, le Conseil d'Etat vise et se réfère explicitement à cette décision en tant qu'elle fixe les modalités d'application dans le temps de la nouvelle jurisprudence.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
