<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035911896</ID>
<ANCIEN_ID>JG_L_2017_10_000000402921</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/91/18/CETATEXT000035911896.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 25/10/2017, 402921</TITRE>
<DATE_DEC>2017-10-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>402921</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BORE, SALVE DE BRUNETON, MEGRET ; SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR>M. Marc Firoud</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:402921.20171025</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La commune du Croisic a demandé au tribunal administratif de Nantes de condamner le département de Loire-Atlantique à lui verser une somme de 1 382 237 euros, assortie des intérêts moratoires et de leur capitalisation, en réparation des préjudices subis du fait de la résiliation de la concession du port de plaisance du Croisic. Par un jugement n° 1200815 du 21 mai 2014, le tribunal administratif de Nantes a condamné le département de Loire-Atlantique à verser une somme de 957 095,45 euros, assortie des intérêts moratoires à compter du 21 octobre 2011 et de leur capitalisation, à la commune du Croisic en réparation de la valeur non amortie des biens de retour, de la perte des bénéfices manqués et de la perte du fonds de trésorerie.<br/>
<br/>
              Par un arrêt n° 14NT01984 du 28 juin 2016, la cour administrative d'appel de Nantes a, sur appel du département de Loire-Atlantique, annulé ce jugement et rejeté les demandes de la commune du Croisic.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un nouveau mémoire, enregistrés les 29 août, 29 novembre 2016 et 28 mars 2017 au secrétariat du contentieux du Conseil d'Etat, la commune du Croisic demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel du département de Loire-Atlantique ;<br/>
<br/>
              3°) de mettre à la charge du département de Loire-Atlantique la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des collectivités territoriales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Marc Firoud, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boré, Salve de Bruneton, Mégret, avocat de la commune du Croisic, et à la SCP Matuchansky, Poupot, Valdelièvre, avocat du département de Loire-Atlantique.<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que l'Etat a concédé à la commune du Croisic (Loire-Atlantique), par un arrêté du 27 février 1970, l'établissement et l'exploitation d'un port de plaisance ; qu'à la suite de l'intervention des lois de décentralisation, le département de Loire-Atlantique s'est substitué à l'Etat en 1983 ; que, par une délibération du 6 mai 2010, la commission permanente du conseil général a résilié pour motif d'intérêt général la concession du port de plaisance du Croisic à compter du 31 décembre 2010 ; que la commune du Croisic a demandé au département de lui verser une indemnité de 1 382 237 euros au titre des préjudices subis du fait de cette résiliation ; que, le département n'ayant satisfait à cette demande qu'à hauteur de 45 367 euros, la commune a saisi le tribunal administratif de Nantes qui, par un jugement du 21 mai 2014, a condamné le département de Loire-Atlantique à lui verser une somme de 957 095,45 euros ; que, par un arrêt du 28 juin 2016, contre lequel la commune du Croisic se pourvoit en cassation, la cour administrative d'appel de Nantes a annulé le jugement du tribunal administratif de Nantes et rejeté la demande de la commune du Croisic ; <br/>
<br/>
              2. Considérant que, pour déterminer les droits à indemnisation de la commune, la cour administrative d'appel de Nantes a fait application des stipulations de l'article 45 du cahier des charges de la concession, aux termes duquel, en cas de résiliation, le concédant doit " pourvoir au paiement des annuités restant à courir pour l'intérêt et l'amortissement des emprunts affectés à l'établissement de l'outillage et supporter toutes dépenses régulièrement engagées qui se rattacheraient à l'administration du service " ; qu'elle a interprété l'article 45 comme excluant toute indemnité complémentaire, notamment au titre des investissements réalisés par le concessionnaire sur ses fonds propres, et a constaté que l'indemnité due sur le fondement de ses stipulations n'excédait pas le montant de 45 367 euros versé par le département ; qu'elle a cependant relevé que, si les règles générales applicables aux contrats administratifs permettent aux parties de déterminer l'étendue et les modalités des droits à indemnité du cocontractant en cas de résiliation du contrat pour un motif d'intérêt général, l'interdiction faite aux personnes publiques de consentir des libéralités exclut qu'il en résulte, au détriment d'une personne publique, une disproportion manifeste entre l'indemnité ainsi fixée et le préjudice subi ; qu'elle a donc recherché quel avait été le préjudice réellement subi par la commune ; que, dans ce cadre, elle a estimé que l'existence d'un éventuel manque à gagner du fait de la résiliation n'était pas établie, que la commune n'avait pas vocation à récupérer le fonds de trésorerie de la concession, qu'elle ne justifiait pas avoir financé certains des biens de la concessions sur ses ressources propres et qu'à supposer que tel ait été le cas, la valeur nette comptable des biens non amortis s'élèverait à une somme de 200 039,72 euros ; qu'elle en a déduit que le montant versé par le département en application de l'article 45 du cahier des charges n'était pas manifestement disproportionné au préjudice subi par la commune ;<br/>
<br/>
              3. Considérant, toutefois, que si les parties à un contrat administratif peuvent déterminer l'étendue et les modalités des droits à indemnité du cocontractant en cas de résiliation du contrat pour un motif d'intérêt général, sous réserve qu'il n'en résulte pas, au détriment d'une personne publique, une disproportion manifeste entre l'indemnité ainsi fixée et le préjudice subi, la fixation des modalités d'indemnisation de la part non amortie des biens de retour dans un contrat de concession obéit, compte tenu de la nature d'un tel préjudice, à des règles spécifiques ; que lorsqu'une personne publique résilie une concession avant son terme normal, le concessionnaire est fondé à demander l'indemnisation du préjudice qu'il subit à raison du retour anticipé des biens à titre gratuit dans le patrimoine de la collectivité publique, dès lors qu'ils n'ont pu être totalement amortis ; que lorsque l'amortissement de ces biens a été calculé sur la base d'une durée d'utilisation inférieure à la durée du contrat, cette indemnité est égale à leur valeur nette comptable inscrite au bilan ; que, dans le cas où leur durée d'utilisation était supérieure à la durée du contrat, l'indemnité est égale à la valeur nette comptable qui résulterait de l'amortissement de ces biens sur la durée du contrat ; que si, en présence d'une convention conclue entre une personne publique et une personne privée, il est loisible aux parties de déroger à ces principes, sous réserve que l'indemnité mise à la charge de la personne publique au titre de ces biens ne puisse, en toute hypothèse, excéder le montant calculé selon les modalités précisées ci-dessus, il est exclu qu'une telle dérogation, permettant de ne pas indemniser ou de n'indemniser que partiellement les biens de retour non amortis, puisse être prévue par le contrat lorsque le concessionnaire est une personne publique ;<br/>
<br/>
              4. Considérant, par suite, qu'en estimant qu'elle pouvait se fonder sur les stipulations de l'article 45 du cahier des charges de la concession pour apprécier les droits à indemnisation de la commune au titre de la valeur non amortie des biens de retour, sous la seule réserve que leur application ne conduise pas à un montant manifestement disproportionné au regard du préjudice subi par celle-ci, alors qu'il lui revenait, s'agissant d'un contrat de concession conclu entre deux personnes publiques, de vérifier que les stipulations contractuelles permettaient d'assurer au concessionnaire l'indemnisation de la part non amortie des biens de retour dans les conditions rappelées au point 3 et, à défaut, de les écarter, la cour a entaché son arrêt d'erreur de droit et d'insuffisance de motivation ; qu'il résulte de ce qui précède que la commune du Croisic est fondée, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, à en demander l'annulation ; <br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du département de Loire-Atlantique la somme de 3 000 euros à verser à la commune du Croisic au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nantes du 28 juin 2016 est annulé.<br/>
Article 2 : L'affaire est renvoyée devant la cour administrative d'appel de Nantes.<br/>
Article 3 : Le département de Loire-Atlantique versera à la commune du Croisic la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la commune du Croisic et au département de Loire-Atlantique.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39 MARCHÉS ET CONTRATS ADMINISTRATIFS. - CONTRAT DE CONCESSION - RÉSILIATION POUR MOTIF D'INTÉRÊT GÉNÉRAL - INDEMNISATION DU COCONTRACTANT - 1) RÈGLES D'INDEMNISATION DE LA PART NON AMORTIE DES BIENS DE RETOUR [RJ1] - 2) CAS D'UNE CONCESSION CONCLUE ENTRE DEUX PERSONNES PUBLIQUES - POSSIBILITÉ DE DÉROGER À CES RÈGLES DANS LE CONTRAT - ABSENCE [RJ2].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">39-04-02-03 MARCHÉS ET CONTRATS ADMINISTRATIFS. FIN DES CONTRATS. RÉSILIATION. DROIT À INDEMNITÉ. - CONTRAT DE CONCESSION - 1) RÈGLES D'INDEMNISATION DE LA PART NON AMORTIE DES BIENS DE RETOUR [RJ1] - 2) CAS D'UNE CONCESSION CONCLUE ENTRE DEUX PERSONNES PUBLIQUES - POSSIBILITÉ DE DÉROGER À CES RÈGLES DANS LE CONTRAT - ABSENCE [RJ2].
</SCT>
<ANA ID="9A"> 39 1) Si les parties à un contrat administratif peuvent déterminer l'étendue et les modalités des droits à indemnité du cocontractant en cas de résiliation du contrat pour un motif d'intérêt général, sous réserve qu'il n'en résulte pas, au détriment d'une personne publique, une disproportion manifeste entre l'indemnité ainsi fixée et le préjudice subi, la fixation des modalités d'indemnisation de la part non amortie des biens de retour dans un contrat de concession obéit, compte tenu de la nature d'un tel préjudice, à des règles spécifiques. Lorsqu'une personne publique résilie une concession avant son terme normal, le concessionnaire est fondé à demander l'indemnisation du préjudice qu'il subit à raison du retour anticipé des biens à titre gratuit dans le patrimoine de la collectivité publique, dès lors qu'ils n'ont pu être totalement amortis. Lorsque l'amortissement de ces biens a été calculé sur la base d'une durée d'utilisation inférieure à la durée du contrat, cette indemnité est égale à leur valeur nette comptable inscrite au bilan. Dans le cas où leur durée d'utilisation était supérieure à la durée du contrat, l'indemnité est égale à la valeur nette comptable qui résulterait de l'amortissement de ces biens sur la durée du contrat.... ,,2) Si, en présence d'une convention conclue entre une personne publique et une personne privée, il est loisible aux parties de déroger à ces principes, sous réserve que l'indemnité mise à la charge de la personne publique au titre de ces biens ne puisse, en toute hypothèse, excéder le montant calculé selon les modalités précisées ci-dessus, il est exclu qu'une telle dérogation, permettant de ne pas indemniser ou de n'indemniser que partiellement les biens de retour non amortis, puisse être prévue par le contrat lorsque le concessionnaire est une personne publique.</ANA>
<ANA ID="9B"> 39-04-02-03 1) Si les parties à un contrat administratif peuvent déterminer l'étendue et les modalités des droits à indemnité du cocontractant en cas de résiliation du contrat pour un motif d'intérêt général, sous réserve qu'il n'en résulte pas, au détriment d'une personne publique, une disproportion manifeste entre l'indemnité ainsi fixée et le préjudice subi, la fixation des modalités d'indemnisation de la part non amortie des biens de retour dans un contrat de concession obéit, compte tenu de la nature d'un tel préjudice, à des règles spécifiques. Lorsqu'une personne publique résilie une concession avant son terme normal, le concessionnaire est fondé à demander l'indemnisation du préjudice qu'il subit à raison du retour anticipé des biens à titre gratuit dans le patrimoine de la collectivité publique, dès lors qu'ils n'ont pu être totalement amortis. Lorsque l'amortissement de ces biens a été calculé sur la base d'une durée d'utilisation inférieure à la durée du contrat, cette indemnité est égale à leur valeur nette comptable inscrite au bilan. Dans le cas où leur durée d'utilisation était supérieure à la durée du contrat, l'indemnité est égale à la valeur nette comptable qui résulterait de l'amortissement de ces biens sur la durée du contrat.... ,,2) Si, en présence d'une convention conclue entre une personne publique et une personne privée, il est loisible aux parties de déroger à ces principes, sous réserve que l'indemnité mise à la charge de la personne publique au titre de ces biens ne puisse, en toute hypothèse, excéder le montant calculé selon les modalités précisées ci-dessus, il est exclu qu'une telle dérogation, permettant de ne pas indemniser ou de n'indemniser que partiellement les biens de retour non amortis, puisse être prévue par le contrat lorsque le concessionnaire est une personne publique.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Assemblée, 21 décembre 2012, Commune de Douai, n° 342788, p. 479. Comp., s'agissant des règles générales d'indemnisation du cocontractant en cas de résiliation, CE, 5 avril 2011, Chambre de commerce et d'industrie de Nîmes, Uzès, Bagnols, Le Vigan, n°334280, p. 205.,,[RJ2] Comp., dans le cas d'un contrat conclu entre une personne publique et une personne privée, CE, Assemblée, 21 décembre 2012, Commune de Douai, n° 342788, p. 479.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
