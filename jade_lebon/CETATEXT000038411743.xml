<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038411743</ID>
<ANCIEN_ID>JG_L_2019_04_000000412271</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/41/17/CETATEXT000038411743.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 5ème chambres réunies, 24/04/2019, 412271</TITRE>
<DATE_DEC>2019-04-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>412271</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Cécile Vaullerin</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Louis Dutheillet de Lamothe</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:412271.20190424</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              1° Sous le n° 412271, par une requête et un mémoire en réplique, enregistrés le 8 juillet 2017 et le 18 juin 2018 au secrétariat du contentieux du Conseil d'Etat, M. B...C...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2017-892 du 6 mai 2017 portant diverses mesures de modernisation et de simplification de la procédure civile ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 12 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              2° Sous le n° 412310, par une requête et un mémoire en réplique, enregistrés le 10 juillet 2017 et le 18 juin 2018 au secrétariat du contentieux du Conseil d'Etat, M. A...D...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le même décret ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 12 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son Préambule et ses articles 34 et 37 ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le pacte international relatif aux droits civils et politiques du 19 décembre 1996 ;<br/>
              - le code de procédure civile ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Cécile Vaullerin, auditeur,  <br/>
<br/>
              - les conclusions de M. Louis Dutheillet de Lamothe, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1.	Les requêtes de MM. C...et D...sont dirigées contre le même décret. Il y a lieu de les joindre pour statuer par une seule décision.<br/>
<br/>
              2.	L'association Le grand barreau de France justifie, eu égard à son objet, d'un intérêt suffisant à l'annulation du décret attaqué. Ses interventions sous les nos 412271 et 412310 sont donc recevables. <br/>
<br/>
              Sur la consultation du Conseil d'Etat :<br/>
<br/>
              3.	Lorsque, comme en l'espèce, un décret doit être pris en Conseil d'Etat, le texte retenu par le Gouvernement ne peut être différent à la fois du projet qu'il a soumis au Conseil d'Etat et du texte adopté par ce dernier. Il ressort des pièces versées au dossier par le Premier ministre que le décret attaqué ne comporte aucune disposition différant à la fois de celles qui figuraient dans le projet du Gouvernement et de celles qui ont été adoptées par la section de l'intérieur du Conseil d'Etat. Dès lors, le moyen tiré de la méconnaissance des règles qui gouvernent l'examen par le Conseil d'Etat des projets de décret doit être écarté. <br/>
<br/>
              Sur les amendes prévues aux articles 2 et 67 du décret attaqué :<br/>
<br/>
              4.	MM. C...et D...contestent les articles 2 et 67 du décret attaqué en tant qu'ils portent de 3 000 à 10 000 euros le montant maximal des amendes civiles respectivement prévues, d'une part, dorénavant à l'article 348 du code de procédure civile en cas de rejet de la procédure de récusation ou de renvoi pour cause de suspicion légitime et, d'autre part, aux articles 32-1, 207, 295, 305, 559, 581, 628, 1180-19 et 1216 du code de procédure civile, R. 121-22 et R. 213-8 du code des procédures civiles d'exécution et R. 3252-25 du code du travail. <br/>
<br/>
              5.	En premier lieu, les amendes mentionnées au point précédent présentent le caractère de mesures de procédure civile. Il résulte des articles 34 et 37 de la Constitution que les dispositions relatives à la procédure à suivre devant les juridictions relèvent de la compétence réglementaire dès lors qu'elles ne concernent pas la procédure pénale et qu'elles ne mettent pas en cause les règles ou les principes fondamentaux placés par la Constitution dans le domaine de la loi. Par suite, les moyens tirés de ce que le pouvoir réglementaire aurait été incompétent pour fixer le montant de ces amendes ne peut qu'être écarté.<br/>
<br/>
              6.	En deuxième lieu, ces mêmes amendes, instituées dans l'intérêt d'une bonne administration de la justice sur le fondement de dispositions qui définissent les conditions dans lesquelles elles peuvent être appliquées, présentent le caractère de mesures d'ordre public que le juge peut prononcer d'office. Si les amendes prononcées doivent être appropriées au regard des circonstances de l'espèce et du comportement du requérant, elles ne peuvent être regardées comme des sanctions ayant le caractère de punition au sens de l'article 8 de la Déclaration des droits de l'homme et du citoyen. Par suite, les requérants ne peuvent utilement soutenir que les dispositions contestées méconnaîtraient le principe de légalité des délits et des peines, tel qu'il est consacré par cet article. <br/>
<br/>
              7.	En troisième lieu, les dispositions contestées ne restreignent pas le droit reconnu à toute personne de soumettre sa cause à une juridiction. Par suite, cet article ne méconnaît pas les dispositions du premier paragraphe de l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales qui garantissent le droit à un procès équitable, ni, en tout état de cause, celles de l'article 14 du pacte international relatif aux droits civils et politiques concernant l'égalité devant la justice. <br/>
<br/>
              8.	Il résulte de tout ce qui précède, sans qu'il soit besoin d'examiner les fins de non-recevoir opposées par le garde des sceaux, ministre de la justice, que MM. C...et D...ne sont pas fondés à demander l'annulation du décret qu'ils attaquent. <br/>
<br/>
              9.	Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Les interventions de l'association Le grand barreau de France, sous les nos 412271 et 412310, sont admises. <br/>
<br/>
Article 2 : Les requêtes de MM. C...et D...sont rejetées. <br/>
<br/>
Article 3 : La présente décision sera notifiée à MM. B...C...et A...D...ainsi qu'à la garde des sceaux, ministre de la justice.<br/>
Copie en sera adressée à l'association Le grand barreau de France, au Premier ministre et à la ministre des outre-mer.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-015-03-01-01-01 ACTES LÉGISLATIFS ET ADMINISTRATIFS. - ARTICLE 8 DE LA DDHC - AMENDES DE PROCÉDURE CIVILE - CARACTÈRE DE PUNITION - ABSENCE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">37-03-07 JURIDICTIONS ADMINISTRATIVES ET JUDICIAIRES. RÈGLES GÉNÉRALES DE PROCÉDURE. POUVOIRS DES JURIDICTIONS. - AMENDES DE PROCÉDURE CIVILE - PUNITION ENTRANT DANS LE CHAMP DE L'ART. 8 DE LA DDHC - ABSENCE [RJ1].
</SCT>
<ANA ID="9A"> 01-015-03-01-01-01 Les amendes instituées dans l'intérêt d'une bonne administration de la justice sur le fondement des articles 32-1, 207, 295, 305, 348, 559, 581, 628, 1180-19 et 1216 du code de procédure civile (CPC), R. 121-22 et R. 213-8 du code des procédures civiles d'exécution (CPCE) et R. 3252-25 du code du travail, qui définissent les conditions dans lesquelles elles peuvent être appliquées, présentent le caractère de mesures d'ordre public que le juge peut prononcer d'office. Si les amendes prononcées doivent être appropriées au regard des circonstances de l'espèce et du comportement du requérant, elles ne peuvent être regardées comme des sanctions ayant le caractère de punition au sens de l'article 8 de la Déclaration des droits de l'homme et du citoyen (DDHC).</ANA>
<ANA ID="9B"> 37-03-07 Les amendes prévues par les articles 32-1, 207, 295, 305, 348, 559, 581, 628, 1180-19 et 1216 du code de procédure civile (CPC), R. 121-22 et R. 213-8 du code des procédures civiles d'exécution (CPCE) et R. 3252-25 du code du travail, instituées dans l'intérêt d'une bonne administration de la justice sur le fondement de dispositions qui définissent les conditions dans lesquelles elles peuvent être appliquées, présentent le caractère de mesures d'ordre public que le juge peut prononcer d'office. Si les amendes prononcées doivent être appropriées au regard des circonstances de l'espèce et du comportement du requérant, elles ne peuvent être regardées comme des sanctions ayant le caractère de punition au sens de l'article 8 de la Déclaration des droits de l'homme et du citoyen (DDHC).</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Assemblée, 5 juillet 1985, Confédération générale du travail et autres, n° 21893, p. 217. Rappr. Cass. civ. 2ème, 3 septembre 2015, n° 14-11.676, inédit au Bulletin ; Cass. civ. 2ème, 7 juin 2018, n° 17-16.521, inédit au Bulletin.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
