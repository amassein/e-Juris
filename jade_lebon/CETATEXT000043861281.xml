<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043861281</ID>
<ANCIEN_ID>JG_L_2021_07_000000434362</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/86/12/CETATEXT000043861281.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 1ère chambres réunies, 22/07/2021, 434362</TITRE>
<DATE_DEC>2021-07-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>434362</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GATINEAU, FATTACCINI, REBEYROL ; SARL DIDIER-PINET</AVOCATS>
<RAPPORTEUR>Mme Yaël Treille</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Raphaël Chambon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:434362.20210722</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme AZ... D..., M. Q... W..., M. AS... N..., Mme AO... AU..., M. AC... X..., M. AT... AN..., M. AL... BG..., M. Q... BE..., M. AE... AP..., M. Q... F..., M. AH... Y..., Mme AM... Z..., Mme V... P..., M. L... AA..., M. BF... G..., Mme M... H..., M. AY... AB..., Mme AX... I..., Mme AW... S..., M. X... BA..., Mme J... U..., Mme J... BC..., M. E... AD..., M. O... K..., M. AK... T..., M. AF... AQ..., M. AJ... AQ..., Mme AV... BB..., M. R... AG..., Mme BD... A... et Mme C... AR... ont demandé au tribunal administratif de Caen d'annuler pour excès de pouvoir la décision du 28 septembre 2018 par laquelle le directeur régional des entreprises, de la concurrence, de la consommation, du travail et de l'emploi de Normandie a homologué le document unilatéral portant plan de sauvegarde de l'emploi de la société Nouvelle France Ouest Imprim (SNFOI). Par un jugement n° 1802813 du 25 février 2019, le tribunal administratif a rejeté leur demande.<br/>
<br/>
              Par un arrêt n° 19NT01405 du 9 juillet 2019, la cour administrative d'appel de Nantes a, sur appel de Mme D... et autres, annulé ce jugement et la décision du 28 septembre 2018.  <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 6 septembre et 4 décembre 2019 et le 24 novembre 2020 au secrétariat du contentieux du Conseil d'Etat, la SNFOI demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de Mme D... et autres ; <br/>
<br/>
              3°) de mettre à la charge de Mme D... et autres la somme de 4500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code du travail ; <br/>
              - l'ordonnance n° 2017-1718 du 20 décembre 2017 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme B... AI..., auditrice,  <br/>
<br/>
              - les conclusions de M. Raphaël Chambon, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Gatineau, Fattaccini, Rebeyrol, avocat de la société Nouvelle France Ouest Imprim et à la SCP Didier-Pinet, avocat de Mme AZ... D..., de M. Q... W..., de M. AS... N..., de Mme AO... AU..., de M. AC... X..., de M. AT... AN..., de M. AL... BG..., de M. Q... BE..., de M. AE... AP..., de M Q... F..., de M. AH... Y..., de Mme AM... Z..., de Mme V... P..., de M. L... AA..., de M. BF... G..., de Mme M... H..., de M. AY... AB..., de Mme AX... I..., de M. AW... S..., de M. X... BA..., de Mme J... U..., de Mme J... BC..., de M. E... AD..., de M. O... K..., de M. AK... T..., de M. AF... AQ..., de M. AJ... AQ..., de Mme AV... BB..., de M. R... AG..., de Mme BD... A... et de Mme C... AR... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par une décision du 28 septembre 2018, le directeur régional des entreprises, de la concurrence, de la consommation, du travail et de l'emploi de Normandie a homologué le document unilatéral fixant le contenu du plan de sauvegarde de l'emploi de la société Nouvelle France Ouest Imprim (SNFOI), en vue de la cessation totale de son activité. Trente-et-un salariés de la SNFOI ont demandé au tribunal administratif de Caen d'annuler pour excès de pouvoir cette décision. Par un jugement du 25 février 2019, le tribunal administratif a rejeté leur demande. Par un arrêt du 9 juillet 2019, la cour administrative d'appel de Nantes a fait droit à leur appel formé contre ce jugement et a annulé la décision du 28 septembre 2018. La SNFOI se pourvoit en cassation contre cet arrêt. <br/>
<br/>
              2. D'une part, aux termes de l'article L. 1233-61 du code du travail, dans sa rédaction issue de l'ordonnance n° 2017-1718 du 20 décembre 2017 : " Dans les entreprises d'au moins cinquante salariés, lorsque le projet de licenciement concerne au moins dix salariés dans une même période de trente jours, l'employeur établit et met en oeuvre un plan de sauvegarde de l'emploi pour éviter les licenciements ou en limiter le nombre. Ce plan intègre un plan de reclassement visant à faciliter le reclassement sur le territoire national des salariés dont le licenciement ne pourrait être évité (...) ".  Aux termes de l'article L. 1233-24-4 du même code : " A défaut d'accord (...), un document élaboré par l'employeur après la dernière réunion du comité social et économique fixe le contenu du plan de sauvegarde de l'emploi et précise les éléments prévus aux 1° à 5° de l'article L. 1233-24-2, dans le cadre des dispositions légales et conventionnelles en vigueur. ". Enfin, aux termes de l'article L. 1233-57-3 du même code : " (...) l'autorité administrative homologue le document élaboré par l'employeur mentionné à l'article L. 1233-24-4, après avoir vérifié la conformité de son contenu aux dispositions législatives et aux stipulations conventionnelles relatives aux éléments mentionnés aux 1° à 5° de l'article L. 1233-24-2, la régularité de la procédure d'information et de consultation du comité social et économique, le respect, le cas échéant, des obligations prévues aux articles L. 1233-57-9 à L. 1233-57-16, L. 1233-57-19 et L. 1233-57-20 et le respect par le plan de sauvegarde de l'emploi des articles L. 1233-61 à L. 1233-63 en fonction des critères suivants : / 1° Les moyens dont disposent l'entreprise, l'unité économique et sociale et le groupe ; / 2° Les mesures d'accompagnement prévues au regard de l'importance du projet de licenciement ; / 3° Les efforts de formation et d'adaptation tels que mentionnés aux articles L. 1233-4 et L. 6321-1 ". <br/>
<br/>
              3. Il résulte de l'ensemble des dispositions citées ci-dessus que, lorsqu'elle est saisie d'une demande d'homologation d'un document élaboré en application de l'article L. 1233-24-4 du code du travail, il appartient à l'administration, sous le contrôle du juge de l'excès de pouvoir, de vérifier la conformité de ce document et du plan de sauvegarde de l'emploi dont il fixe le contenu aux dispositions législatives et aux stipulations conventionnelles applicables, en s'assurant notamment du respect par le plan de sauvegarde de l'emploi des dispositions des articles L. 1233-61 à L. 1233-63 du même code. A ce titre elle doit, au regard de l'importance du projet de licenciement, apprécier si les mesures contenues dans le plan sont précises et concrètes et si, à raison, pour chacune, de sa contribution aux objectifs de maintien dans l'emploi et de reclassement des salariés, elles sont, prises dans leur ensemble, propres à satisfaire à ces objectifs compte tenu, d'une part, des efforts de formation et d'adaptation déjà réalisés par l'employeur et, d'autre part, des moyens dont disposent l'entreprise et, le cas échéant, l'unité économique et sociale et le groupe. <br/>
<br/>
              4. A ce titre, il revient notamment à l'autorité administrative de s'assurer que le plan de reclassement intégré au plan de sauvegarde de l'emploi est de nature à faciliter le reclassement des salariés dont le licenciement ne pourrait être évité. L'employeur doit, à cette fin, avoir identifié dans le plan l'ensemble des possibilités de reclassement des salariés dans l'entreprise. En outre, lorsque l'entreprise appartient à un groupe, l'employeur, seul débiteur de l'obligation de reclassement, doit avoir procédé à une recherche sérieuse des postes disponibles pour un reclassement sur le territoire national dans les autres entreprises du groupe, quelle que soit la durée des contrats susceptibles d'être proposés pour pourvoir à ces postes. Pour l'ensemble des postes de reclassement ainsi identifiés l'employeur doit avoir indiqué dans le plan leur nombre, leur nature et leur localisation. <br/>
<br/>
              5. D'autre part, aux termes de l'article L. 1233-4 du code du travail, dans sa rédaction issue en dernier lieu de l'ordonnance du 20 décembre 2017 : " Le licenciement pour motif économique d'un salarié ne peut intervenir que lorsque tous les efforts de formation et d'adaptation ont été réalisés et que le reclassement de l'intéressé ne peut être opéré sur les emplois disponibles, situés sur le territoire national dans l'entreprise ou les autres entreprises du groupe dont l'entreprise fait partie et dont l'organisation, les activités ou le lieu d'exploitation assurent la permutation de tout ou partie du personnel./ (...) Le reclassement du salarié s'effectue sur un emploi relevant de la même catégorie que celui qu'il occupe ou sur un emploi équivalent assorti d'une rémunération équivalente. A défaut, et sous réserve de l'accord exprès du salarié, le reclassement s'effectue sur un emploi d'une catégorie inférieure. / L'employeur adresse de manière personnalisée les offres de reclassement à chaque salarié ou diffuse par tout moyen une liste des postes disponibles à l'ensemble des salariés, dans des conditions précisées par décret. / Les offres de reclassement proposées au salarié sont écrites et précises. ". Aux termes de l'article D. 1233-2-1 du même code : " I. - Pour l'application de l'article L. 1233-4, l'employeur adresse des offres de reclassement de manière personnalisée ou communique la liste des offres disponibles aux salariés, et le cas échéant l'actualisation de celle-ci, par tout moyen permettant de conférer date certaine. (...) III.  En cas de diffusion d'une liste des offres de reclassement interne, celle-ci comprend les postes disponibles situés sur le territoire national dans l'entreprise et les autres entreprises du groupe dont l'entreprise fait partie. La liste précise les critères de départage entre salariés en cas de candidatures multiples sur un même poste, ainsi que le délai dont dispose le salarié pour présenter sa candidature écrite. Ce délai ne peut être inférieur à quinze jours francs à compter de la publication de la liste, sauf lorsque l'entreprise fait l'objet d'un redressement ou d'une liquidation judiciaire ". <br/>
<br/>
              6. Il résulte des termes mêmes de l'arrêt attaqué que pour annuler la décision homologuant le document unilatéral fixant le contenu du plan de sauvegarde de l'emploi de la SNFOI, la cour administrative d'appel de Nantes a estimé que l'autorité administrative n'avait pu légalement homologuer ce document dès lors qu'il était entaché d'une irrégularité ayant privé les salariés intéressés d'une garantie en ce que, alors que la SNFOI devait être regardée comme ayant adressé à l'ensemble des salariés  une liste des offres de reclassement disponibles, le délai qu'il impartissait aux salariés pour présenter leur candidature était inférieur au délai de quinze jours francs prévu par l'article D. 1233-2-1 du code du travail. Or, au stade de l'examen d'une demande d'homologation d'un document unilatéral fixant le contenu du plan de sauvegarde de l'emploi, il revient seulement à l'autorité administrative, ainsi qu'il est dit au point 4, de vérifier que l'employeur a identifié dans le plan l'ensemble des possibilités de reclassement des salariés dans l'entreprise, et, lorsque l'entreprise appartient à un groupe, que l'employeur a procédé à une recherche sérieuse des postes disponibles pour un reclassement sur le territoire national, dans les autres entreprises du groupe de reclassement, en indiquant pour tous ces postes dans le plan leur nombre, leur nature et leur localisation. En se fondant ainsi sur un moyen inopérant pour annuler la décision d'homologation en litige, la cour administrative d'appel de Nantes a entaché son arrêt d'erreur de droit. Par suite, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, la SNFOI est fondée à en demander l'annulation. <br/>
<br/>
              7. Le délai de trois mois imparti à la cour administrative d'appel pour statuer par les dispositions de l'article L. 1235-7-1 du code du travail étant expiré, il y a lieu pour le Conseil d'Etat, en application des mêmes dispositions, de statuer immédiatement sur l'appel formé par Mme D... et autres contre le jugement du 25 février 2019, par lequel le tribunal administratif de Caen a rejeté leur demande tendant à l'annulation de la décision d'homologation du 28 septembre 2018. <br/>
<br/>
              8. Aux termes de l'article L. 1235-10 du code du travail : " Dans les entreprises d'au moins cinquante salariés, lorsque le projet de licenciement concerne au moins dix salariés dans une même période de trente jours, le licenciement intervenu en l'absence de toute décision relative à la validation ou à l'homologation ou alors qu'une décision négative a été rendue est nul. / En cas d'annulation d'une décision de validation mentionnée à l'article L. 1233-57-2 ou d'homologation mentionnée à l'article L. 1233-57-3 en raison d'une absence ou d'une insuffisance de plan de sauvegarde de l'emploi mentionné à l'article L. 1233-61, la procédure de licenciement est nulle. / Les deux premiers alinéas ne sont pas applicables aux entreprises en redressement ou liquidation judiciaires ". Aux termes de l'article L. 1235-11 du même code : " Lorsque le juge constate que le licenciement est intervenu alors que la procédure de licenciement est nulle, conformément aux dispositions des deux premiers alinéas de l'article L. 1235-10, il peut ordonner la poursuite du contrat de travail ou prononcer la nullité du licenciement et ordonner la réintégration du salarié à la demande de ce dernier, sauf si cette réintégration est devenue impossible, notamment du fait de la fermeture de l'établissement ou du site ou de l'absence d'emploi disponible. / Lorsque le salarié ne demande pas la poursuite de son contrat de travail ou lorsque la réintégration est impossible, le juge octroie au salarié une indemnité à la charge de l'employeur qui ne peut être inférieure aux salaires des six derniers mois ". Aux termes de l'article L. 1235-16 du code du travail, dans sa rédaction issue de la loi du 6 août 2015 pour la croissance, l'activité et l'égalité des chances économiques : " L'annulation de la décision de validation mentionnée à l'article L. 1233-57-2 ou d'homologation mentionnée à l'article L. 1233-57-3 pour un motif autre que celui mentionné au dernier alinéa du présent article et au deuxième alinéa de l'article L. 1235-10 donne lieu, sous réserve de l'accord des parties, à la réintégration du salarié dans l'entreprise, avec maintien de ses avantages acquis. / A défaut, le salarié a droit à une indemnité à la charge de l'employeur, qui ne peut être inférieure aux salaires des six derniers mois. Elle est due sans préjudice de l'indemnité de licenciement prévue à l'article L 1234-9. / En cas d'annulation d'une décision de validation mentionnée à l'article L. 1233-57-2 ou d'homologation mentionnée à l'article L. 1233-57-3 en raison d'une insuffisance de motivation, l'autorité administrative prend une nouvelle décision suffisamment motivée dans un délai de quinze jours à compter de la notification du jugement à l'administration. Cette décision est portée par l'employeur à la connaissance des salariés licenciés à la suite de la première décision de validation ou d'homologation, par tout moyen permettant de conférer une date certaine à cette information. / Dès lors que l'autorité administrative a édicté cette nouvelle décision, l'annulation pour le seul motif d'insuffisance de motivation de la première décision de l'autorité administrative est sans incidence sur la validité du licenciement et ne donne lieu ni à réintégration, ni au versement d'une indemnité à la charge de l'employeur ". <br/>
<br/>
              9. Il résulte des dispositions qui viennent d'être citées que, pour les entreprises qui ne sont pas en redressement ou en liquidation judiciaire, le législateur a attaché à l'annulation pour excès de pouvoir d'une décision d'homologation ou de validation d'un plan de sauvegarde de l'emploi, des effets qui diffèrent selon le motif pour lequel cette annulation est prononcée. Par suite, lorsque le juge administratif est saisi d'une requête dirigée contre une décision d'homologation ou de validation d'un plan de sauvegarde de l'emploi d'une entreprise qui n'est pas en redressement ou en liquidation judiciaire, il doit, si cette requête soulève plusieurs moyens, toujours commencer par se prononcer, s'il est soulevé devant lui, sur le moyen tiré de l'absence ou de l'insuffisance du plan, même lorsqu'un autre moyen est de nature à fonder l'annulation de la décision administrative, compte tenu des conséquences particulières qui, en application de l'article L. 1235-11 du code du travail, sont susceptibles d'en découler pour les salariés. En outre, compte tenu de ce que l'article L. 1235-16 de ce code, dans sa rédaction issue de la loi du 6 août 2015, prévoit désormais que l'annulation d'une telle décision administrative, pour un autre motif que celui tiré de l'absence ou de l'insuffisance du plan, est susceptible d'avoir des conséquences différentes selon que cette annulation est fondée sur un moyen tiré de l'insuffisance de la motivation de la décision en cause ou sur un autre moyen, il appartient au juge administratif de se prononcer ensuite sur les autres moyens éventuellement présentés à l'appui des conclusions aux fins d'annulation pour excès de pouvoir de cette décision, en réservant, à ce stade, celui tiré de l'insuffisance de la motivation de la décision administrative. Enfin, lorsqu'aucun de ces moyens n'est fondé, le juge administratif doit se prononcer sur le moyen tiré de l'insuffisance de la motivation de la décision administrative lorsqu'il est soulevé. <br/>
<br/>
              Sur le moyen tiré de l'insuffisance du plan de sauvegarde de l'emploi :<br/>
<br/>
              10. Si le plan de sauvegarde de l'emploi n'identifie aucun poste de reclassement interne, dès lors que la  recherche de reclassement interne conduite par la SNFOI auprès des cinq sociétés composant le groupe FILFOI était restée infructueuse à la date à laquelle la SNFOI a adressé le plan à l'administration en vue de son homologation, le 17 septembre 2018, cette circonstance n'est pas, à elle seule, de nature à établir l'absence de sérieux de la recherche de postes de reclassement internes au groupe. Par ailleurs, Mme D... et autres ne sont en tout état de cause pas fondés à se prévaloir de l'absence de mention, dans le plan, de sept offres d'emplois au sein de la société FILMAG qui ont été proposées aux salariés de la SNFOI postérieurement à la saisine de l'administration aux fins d'homologation du plan.  En outre, s'il résulte de ce qui est dit au point 4 qu'un plan de sauvegarde de l'emploi doit identifier l'ensemble des postes disponibles pour un reclassement interne, peu important qu'ils soient, le cas échéant, d'une durée limitée, ni la circonstance que le plan ait limité l'identification des possibilités de reclassement interne aux besoins en personnel d'une durée d'au moins trois mois, ni celle qu'il prévoie une période d'adaptation au terme de laquelle l'employeur ou le salarié pourront décider de ne pas donner suite au reclassement, ne sont, en l'espèce, de nature à l'entacher d'insuffisance, compte tenu des autres mesures qu'il comporte tendant à favoriser le reclassement des salariés, notamment la mise en place d'une antenne emploi, les aides financières individuelles à la formation, les aides financières à la recherche d'emploi et à la mobilité géographique, les aides à la création d'entreprises. Ainsi, prises dans leur ensemble, les mesures prévues par le plan sont propres à satisfaire les objectifs de maintien dans l'emploi et de reclassement des salariés, compte tenu des moyens dont disposaient, à la date de la décision litigieuse, la SNFOI et le groupe FILFOI auquel elle appartient. Par suite, le moyen tiré de ce que la décision attaquée ne pouvait légalement homologuer le document unilatéral au motif que le plan de sauvegarde de l'emploi était insuffisant doit être écarté. <br/>
<br/>
              Sur le moyen tiré de la méconnaissance des articles L. 1233-4 et D. 1233-2-1 du code du travail : <br/>
<br/>
              11. Il résulte de ce qui est dit au point 6 que la méconnaissance des dispositions des articles L. 1233-4 et D. 1233-2-1 du code du travail, relatives aux modalités de diffusion des offres de reclassement aux salariés faisant l'objet d'un licenciement pour motif économique, ne peut utilement être invoquée à l'appui d'un recours tendant à l'annulation de la décision par laquelle l'autorité administrative homologue un plan de sauvegarde de l'emploi.  <br/>
<br/>
              Sur les autres moyens :<br/>
<br/>
              12. Il y a lieu, par adoption des motifs retenus par le tribunal administratif de Caen, dont le jugement est suffisamment motivé, contrairement à ce qui est soutenu, d'écarter les autres moyens d'appel soulevés par Mme D... et autres. <br/>
<br/>
              13. Il résulte de tout ce qui précède, sans qu'il soit besoin de se prononcer sur la fin de non-recevoir opposée par la SFNOI, que Mme D... et autres ne sont pas fondés à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Caen a rejeté leur demande.  <br/>
<br/>
              14. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions de la SNFOI présentées au titre de l'article L. 761-1 du code de justice administrative. Ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de la SNFOI et de l'Etat qui ne sont pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 9 juillet 2019 de la cour administrative d'appel de Nantes est annulé.<br/>
Article 2 : La requête présentée par Mme D... et autres devant la cour administrative d'appel de Nantes est rejetée.<br/>
Article 3 : Les conclusions de la SNFOI et de Mme D... et autres présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à la société Nouvelle France Ouest Imprim et à Mme AZ... D..., première défenderesse dénommée.<br/>
Copie en sera adressée à la ministre du travail, de l'emploi et de l'insertion. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">66-07-01-04-03-01 TRAVAIL ET EMPLOI. LICENCIEMENTS. AUTORISATION ADMINISTRATIVE - SALARIÉS PROTÉGÉS. CONDITIONS DE FOND DE L'AUTORISATION OU DU REFUS D'AUTORISATION. LICENCIEMENT POUR MOTIF ÉCONOMIQUE. OBLIGATION DE RECLASSEMENT. - HOMOLOGATION ADMINISTRATIVE DES PSE - CONDITION TENANT À LA RECHERCHE SÉRIEUSE DES POSTES DISPONIBLES [RJ1] - CARACTÈRE INDIFFÉRENT DE LA DURÉE DES CONTRATS SUSCEPTIBLES D'ÊTRE PROPOSÉS [RJ2].
</SCT>
<ANA ID="9A"> 66-07-01-04-03-01 Un plan de sauvegarde de l'emploi (PSE) doit identifier l'ensemble des postes disponibles pour un reclassement interne, quelle que soit la durée des contrats susceptibles d'être proposés pour pourvoir à ces postes.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Assemblée, 22 juillet 2015, Syndicat CGT de l'union locale de Calais et environs, n° 383481, p. 265.,,[RJ2] Rappr. Cass. soc., 4 septembre 2019, n° 18-18.169, inédit au Bulletin  ; s'agissant de l'obligation de recherche sérieuse d'un poste de reclassement avant licenciement pour inaptitude physique d'un salarié protégé, CE, 11 juin 1990, Sté Pornichet Distribution, n° 84650, inédite au Recueil.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
