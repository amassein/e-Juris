<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028411824</ID>
<ANCIEN_ID>JG_L_2013_12_000000355556</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/41/18/CETATEXT000028411824.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère / 6ème SSR, 30/12/2013, 355556, Publié au recueil Lebon</TITRE>
<DATE_DEC>2013-12-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>355556</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère / 6ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>FOUSSARD ; SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>Mme Marie Grosset</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:355556.20131230</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu 1°, sous le n° 355556, le pourvoi et le mémoire complémentaire, enregistrés les 4 janvier et 4 avril 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour la société immobilière d'économie mixte de la ville de Paris (SIEMP), dont le siège est 29, boulevard Bourdon à Paris (75004) ; la SIEMP demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n°10PA04025 du 4 novembre 2011 par lequel la cour administrative d'appel de Paris a, à la demande de la société Cofinfo, d'une part, annulé le jugement n° 0705967/0707469 du 9 juillet 2010 du tribunal administratif de Paris et, d'autre part, annulé l'arrêté du préfet de Paris du 28 décembre 2006 portant déclaration d'utilité publique de l'acquisition de l'immeuble situé 3 et 5, rue Godefroy Cavaignac ainsi que l'arrêté du 11 juillet 2007 déclarant cessible ce même immeuble ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de la société Cofinfo et de faire droit à ses conclusions et à celles de la Ville de Paris ; <br/>
<br/>
              3°) de mettre à la charge de la société Cofinfo la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ainsi que les entiers dépens ;<br/>
<br/>
<br/>
              Vu 2°, sous le n° 355557, le pourvoi et le mémoire complémentaire, enregistrés les 4 janvier et 4 avril 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour la Ville de Paris, représentée par son maire ; la Ville de Paris demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler le même arrêt de la cour administrative d'appel de Paris ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de la société Cofinfo et de faire droit à ses conclusions et à celles de la SIEMP, <br/>
<br/>
              3°) de mettre à la charge de la société Cofinfo la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ainsi que les entiers dépens;<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu le code de l'expropriation pour cause d'utilité publique ; <br/>
<br/>
              Vu le code de l'urbanisme ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie Grosset, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Foussard, avocat de la société immobilière d'économie mixte de la ville de Paris et de la Ville de Paris et à la SCP Piwnica, Molinié, avocat de la société Cofinfo ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que les pourvois de la société immobilière d'économie mixte de la ville de Paris (SIEMP) et de la Ville de Paris sont dirigés contre le même arrêt ; qu'il y a lieu de les joindre pour statuer par une seule décision ; <br/>
<br/>
              Sur le pourvoi de la Ville de Paris :<br/>
<br/>
              2. Considérant que la personne qui est régulièrement intervenue devant la cour administrative d'appel n'est recevable à se pourvoir en cassation contre l'arrêt rendu contrairement aux conclusions de son intervention que lorsqu'elle aurait eu, à défaut d'intervention de sa part, qualité pour former tierce opposition contre la décision du juge d'appel ; que l'arrêt par lequel la cour administrative d'appel de Paris a annulé les arrêtés du préfet de Paris du 28 décembre 2006 portant déclaration d'utilité publique de l'immeuble situé 3 et 5 rue Godefroy Cavaignac et du 11 juillet 2007 déclarant cessible cet immeuble ne préjudicie pas aux droits de la Ville de Paris, dès lors qu'elle n'est pas le bénéficiaire de la déclaration d'utilité publique ; que, par suite, elle n'aurait pas eu qualité, à défaut d'intervention de sa part, pour former tierce opposition contre l'arrêt attaqué et est, par suite, irrecevable à se pourvoir en cassation contre celui-ci ; <br/>
<br/>
              Sur le pourvoi de la société immobilière d'économie mixte de la ville de Paris :<br/>
<br/>
              Sur l'intervention de la Ville de Paris :<br/>
<br/>
              3. Considérant que la Ville de Paris justifie d'un intérêt suffisant à l'annulation de l'arrêt attaqué ; qu'ainsi son intervention à l'appui du pourvoi de la SIEMP est recevable ;<br/>
<br/>
              Sur la régularité de l'arrêt attaqué :<br/>
<br/>
              4. Considérant que la circonstance que la cour ait visé un texte dont elle ne faisait pas application est sans incidence sur la régularité de son arrêt ; <br/>
<br/>
              Sur le bien-fondé de l'arrêt attaqué :<br/>
<br/>
              Sur la nature et la légalité de la délibération de la SIEMP demandant la mise en oeuvre de la procédure d'expropriation :<br/>
<br/>
              5. Considérant que l'acte par lequel une personne privée chargée d'une mission de service public et ayant reçu délégation à cette fin en matière d'expropriation demande au préfet l'expropriation d'un immeuble pour cause d'utilité publique traduit l'usage de prérogatives de puissance publique et constitue ainsi un acte administratif ; que la cour a relevé que la SIEMP avait été chargée par la Ville de Paris d'une mission de service public d'éradication de l'habitat insalubre et avait reçu de la ville délégation de ses pouvoirs en matière d'expropriation pour l'exercice de cette mission ; que, par suite, elle n'a pas commis d'erreur de droit en jugeant qu'était une décision administrative, dont elle était compétente pour apprécier la légalité, la délibération du conseil d'administration de la SIEMP demandant au préfet de Paris l'ouverture d'une procédure de déclaration d'utilité publique en vue d'obtenir, à son profit, l'expropriation de l'immeuble situé 3 et 5 rue Godefroy Cavaignac à Paris ;<br/>
<br/>
              6. Considérant qu'aux termes de l'article 1-1-1 de la convention publique d'aménagement conclue entre la Ville de Paris et la SIEMP le 30 mai 2002, en application de l'article L. 300-4 du code de l'urbanisme, cette dernière s'est vu confier la mission de lutter contre l'habitat insalubre et chargée, à ce titre, " d'assurer, notamment par des interventions foncières et la conduite d'opérations de réhabilitation lourde, le traitement de secteurs dominés par l'insalubrité et d'immeubles dont l'état de dégradation et d'insalubrité a justifié une intervention de la puissance publique ", en sollicitant au besoin la mise en oeuvre de procédures d'expropriation auprès de l'autorité compétente ; que la SIEMP ne pouvait légalement mettre en oeuvre les pouvoirs dont elle était délégataire en matière d'expropriation pour d'autres fins que celles définies par la convention par laquelle la Ville de Paris lui avait délégué ces pouvoirs ; que, par suite, la cour n'a pas commis d'erreur de droit en jugeant que la délibération du 9 mars 2006 avait été prise pour un motif erroné et en se fondant sur les clauses réglementaires de la convention du 30 mai 2002, qu'elle a exactement interprétées, pour juger que cette délibération avait pour but d'engager une procédure d'expropriation en dehors de la mission d'éradication de l'habitat insalubre confiée à la SIEMP ;<br/>
<br/>
              7. Considérant que la cour n'a pas commis d'erreur de droit en tenant compte, pour apprécier l'état de l'immeuble, de documents établis postérieurement à la délibération de la SIEMP du 9 mars 2006, dès lors que ces documents permettaient d'apprécier la situation de fait existant à la date de cette délibération ; qu'en déduisant des pièces du dossier et notamment du jugement rendu par le juge de l'expropriation du tribunal de grande instance de Paris du 25 juin 2007, rendu après une visite de l'immeuble, que cet immeuble nécessitait d'importants travaux d'entretien mais ne pouvait être regardé comme insalubre ni " en état de délabrement général " ni même " particulièrement dégradé ", les juges d'appel ont porté sur les faits une appréciation souveraine, exempte de dénaturation ; que le motif par lequel la cour a relevé que les importants travaux d'entretien nécessaires étaient largement imputables aux décisions de refus de concours de la force publique pour l'expulsion d'occupants sans titre ayant un caractère surabondant, il ne peut être utilement soutenu qu'elle aurait, ce faisant, dénaturé les pièces du dossier ; <br/>
<br/>
              Sur la légalité des arrêtés préfectoraux portant déclaration d'utilité publique et cessibilité :<br/>
<br/>
              8. Considérant que la délibération par laquelle l'expropriant demande au préfet l'expropriation d'un immeuble pour cause d'utilité publique constitue un acte préparatoire aux arrêtés portant déclaration d'utilité publique et cessibilité ; que, par suite, son illégalité peut être utilement invoquée à l'appui d'un recours contre l'arrêté déclarant d'utilité publique l'acquisition de cet immeuble et contre celui qui le déclare cessible ; que, dès lors, après avoir exactement déduit de l'illégalité de la délibération du 9 mars 2006 de la SIEMP demandant au préfet de Paris l'ouverture d'une procédure de déclaration d'utilité publique permettant de procéder à l'expropriation de l'immeuble en cause que les arrêtés préfectoraux des 28 décembre 2006 et 11 juillet 2007 étaient de ce seul fait entachés d'illégalité, la cour n'avait pas à rechercher si l'opération envisagée présentait un caractère d'utilité publique et si la circonstance que l'immeuble n'était ni délabré ni insalubre suffisait à ôter à l'opération son caractère d'utilité publique ; que, par suite, les moyens tirés de l'insuffisante motivation de l'arrêt et de l'erreur de droit à ne pas avoir examiné ces questions doivent être écartés ;<br/>
<br/>
              9. Considérant qu'il résulte de tout ce qui précède que la SIEMP n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque ;<br/>
<br/>
              Sur l'application des articles R. 761-1 et L. 761-1 du code de justice administrative :<br/>
<br/>
              10. Considérant que, dans les circonstances de l'espèce, il y a lieu de laisser la contribution pour l'aide juridique à la charge de la SIEMP et de la Ville de Paris ; <br/>
<br/>
              11. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la société Cofinfo, qui n'est pas, dans la présente instance, la partie perdante ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de la Ville de Paris et de la SIEMP la somme que la société Cofinfo demande au même titre ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'intervention de la Ville de Paris est admise.<br/>
Article 2 : Les pourvois de la Ville de Paris et de la société immobilière d'économie mixte de la ville de Paris sont rejetés. <br/>
Article 3 : Les conclusions de la société Cofinfo présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4: La présente décision sera notifiée à la société immobilière d'économie mixte de la ville de Paris, à la Ville de Paris et à la société Cofinfo. <br/>
Copie en sera adressée au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-01-05-01-01 ACTES LÉGISLATIFS ET ADMINISTRATIFS. DIFFÉRENTES CATÉGORIES D'ACTES. ACTES ADMINISTRATIFS - NOTION. ACTES À CARACTÈRE ADMINISTRATIF. ACTES PRÉSENTANT CE CARACTÈRE. - ACTE PAR LEQUEL UNE PERSONNE PRIVÉE CHARGÉE D'UNE MISSION DE SERVICE PUBLIC ET AYANT REÇU DÉLÉGATION À CETTE FIN DEMANDE AU PRÉFET L'EXPROPRIATION D'UN IMMEUBLE POUR CAUSE D'UTILITÉ PUBLIQUE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">34-04-01 EXPROPRIATION POUR CAUSE D'UTILITÉ PUBLIQUE. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. COMPÉTENCE JURIDICTIONNELLE. - JURIDICTION COMPÉTENTE POUR CONNAÎTRE DE L'ACTE PAR LEQUEL UNE PERSONNE PRIVÉE CHARGÉE D'UNE MISSION DE SERVICE PUBLIC ET AYANT REÇU DÉLÉGATION À CETTE FIN DEMANDE AU PRÉFET L'EXPROPRIATION D'UN IMMEUBLE POUR CAUSE D'UTILITÉ PUBLIQUE - JURIDICTION ADMINISTRATIVE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">34-04-02-01 EXPROPRIATION POUR CAUSE D'UTILITÉ PUBLIQUE. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. POUVOIRS DU JUGE. MOYENS. - ACTE PAR LEQUEL L'EXPROPRIANT DEMANDE AU PRÉFET DE PROCÉDER À UNE EXPROPRIATION - ACTE PRÉPARATOIRE AUX ARRÊTÉS PORTANT DÉCLARATION D'UTILITÉ PUBLIQUE ET CESSIBILITÉ - CONSÉQUENCE - OPÉRANCE DU MOYEN TIRÉ DE SON ILLÉGALITÉ À L'APPUI DU RECOURS DIRIGÉ CONTRE CES ARRÊTÉS.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">54-01-01-02 PROCÉDURE. INTRODUCTION DE L'INSTANCE. DÉCISIONS POUVANT OU NON FAIRE L'OBJET D'UN RECOURS. ACTES NE CONSTITUANT PAS DES DÉCISIONS SUSCEPTIBLES DE RECOURS. - ACTE PAR LEQUEL L'EXPROPRIANT DEMANDE AU PRÉFET DE PROCÉDER À UNE EXPROPRIATION - ACTE PRÉPARATOIRE AUX ARRÊTÉS PORTANT DÉCLARATION D'UTILITÉ PUBLIQUE ET CESSIBILITÉ - CONSÉQUENCES - ACTE INSUSCEPTIBLE DE RECOURS - OPÉRANCE DU MOYEN TIRÉ DE SON ILLÉGALITÉ À L'APPUI DU RECOURS DIRIGÉ CONTRE CES ARRÊTÉS.
</SCT>
<ANA ID="9A"> 01-01-05-01-01 L'acte par lequel une personne privée chargée d'une mission de service public et ayant reçu délégation à cette fin en matière d'expropriation demande au préfet l'expropriation d'un immeuble pour cause d'utilité publique traduit l'usage de prérogatives de puissance publique et constitue ainsi une décision administrative.</ANA>
<ANA ID="9B"> 34-04-01 L'acte par lequel une personne privée chargée d'une mission de service public et ayant reçu délégation à cette fin en matière d'expropriation demande au préfet l'expropriation d'un immeuble pour cause d'utilité publique traduit l'usage de prérogatives de puissance publique et constitue ainsi une décision administrative.</ANA>
<ANA ID="9C"> 34-04-02-01 L'acte par lequel l'expropriant demande au préfet l'expropriation d'un immeuble pour cause d'utilité publique constitue un acte préparatoire aux arrêtés portant déclaration d'utilité publique et cessibilité. Par suite, son illégalité peut être utilement invoquée à l'appui d'un recours contre l'arrêté déclarant d'utilité publique l'acquisition de cet immeuble et contre celui qui le déclare cessible.</ANA>
<ANA ID="9D"> 54-01-01-02 L'acte par lequel l'expropriant demande au préfet l'expropriation d'un immeuble pour cause d'utilité publique constitue un acte préparatoire aux arrêtés portant déclaration d'utilité publique et cessibilité. Ainsi, il ne peut être attaqué directement devant un tribunal administratif. Son illégalité peut en revanche être utilement invoquée à l'appui d'un recours contre l'arrêté déclarant d'utilité publique l'acquisition de cet immeuble et contre celui qui le déclare cessible.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
