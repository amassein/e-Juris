<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037882263</ID>
<ANCIEN_ID>JG_L_2018_12_000000412401</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/88/22/CETATEXT000037882263.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 4ème chambres réunies, 28/12/2018, 412401</TITRE>
<DATE_DEC>2018-12-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>412401</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 4ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ZRIBI, TEXIER ; SCP POTIER DE LA VARDE, BUK LAMENT, ROBILLOT</AVOCATS>
<RAPPORTEUR>Mme Cécile Chaduteau-Monplaisir</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Charles Touboul</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:412401.20181228</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Caen d'annuler la décision du 22 juillet 2016 par laquelle le président du conseil départemental du Calvados a confirmé, sur recours gracieux, d'une part, l'évaluation de son revenu mensuel à la somme de 447,38 euros pour le calcul de son droit au revenu de solidarité active (RSA) au titre de la période postérieure au 1er avril 2016 et, d'autre part, la prise en compte de ses avantages en nature dans le calcul de ses droits au RSA au titre des années 2012 et 2013. Par un jugement n°1601886 du 10 mai 2017, le tribunal administratif de Caen a annulé la décision du 22 juillet 2016 en tant qu'elle a confirmé l'évaluation du revenu mensuel de M. A...à la somme de 447,38 euros pour le calcul de son droit au RSA au titre de la période postérieure au 1er avril 2016, l'a renvoyé devant l'administration pour la détermination de ses droits au RSA au titre de cette période et a rejeté le surplus des conclusions de sa demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 12 juillet et 12 octobre 2017 au secrétariat du contentieux du Conseil d'Etat, le département du Calvados demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) de mettre à la charge de M. A...la somme de 2 400 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'action sociale et des familles ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Cécile Chaduteau-Monplaisir, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Charles Touboul, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Zribi et Texier, avocat du département du Calvados, et à la SCP Potier de la Varde, Buk Lament, Robillot, avocat de M.A....<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que M.A..., qui tient un bar-restaurant dans une partie du bien immobilier qu'il habite avec sa famille, a bénéficié, pour lui-même et les autres membres de son foyer, du revenu de solidarité active en qualité de travailleur indépendant à compter d'octobre 2009. Par une décision du 2 mai 2016, à laquelle s'est substituée la décision du 22 juillet 2016 rejetant son recours préalable obligatoire, le président du conseil départemental du Calvados a notamment décidé de l'exclure du bénéfice du revenu de solidarité active à compter du 1er avril 2016. Par son pourvoi en cassation, le département du Calvados doit être regardé comme demandant l'annulation des articles 1er et 2 du jugement du 10 mai 2017 par lesquels le tribunal administratif de Caen a annulé dans cette mesure la décision du 22 juillet 2016 et l'a renvoyé devant l'administration pour la détermination de ses droits au revenu de solidarité active au titre de la période courant à compter du 1er avril 2016.<br/>
<br/>
              2. En premier lieu, le premier alinéa de l'article L. 262-7 du code de l'action sociale et des familles, dans sa rédaction applicable au litige, prévoit que, pour pouvoir bénéficier du revenu de solidarité active, le travailleur relevant du régime social des indépendants " doit n'employer, au titre de son activité professionnelle, aucun salarié et réaliser un chiffre d'affaires n'excédant pas un niveau fixé par décret ". L'article D. 262-16 du même code, alors applicable, précise que ces personnes " peuvent prétendre au revenu de solidarité active lorsque le dernier chiffre d'affaires annuel connu, actualisé le cas échéant, n'excède pas, selon la nature de l'activité exercée, les montants fixés aux articles 50-0 et 102 ter du code général des impôts (...) ", permettant la soumission au régime des micro-entreprises pour les bénéfices industriels et commerciaux ou au régime déclaratif spécial pour les revenus non commerciaux.<br/>
<br/>
              3. En second lieu, d'une part, le dernier alinéa de l'article L. 262-7 du même code, dans sa rédaction applicable au litige, renvoie à un décret en Conseil d'Etat la définition des règles de calcul du revenu de solidarité active applicables aux travailleurs relevant notamment du régime social des indépendants. L'article R. 262-19 du code de l'action sociale et des familles, dans sa rédaction applicable au litige, prévoit à son premier alinéa le principe selon lequel : " Les bénéfices industriels et commerciaux et les bénéfices non commerciaux s'entendent des résultats ou bénéfices déterminés en fonction des régimes d'imposition applicables au titre de la pénultième année. S'y ajoutent les amortissements et les plus-values professionnels ". L'article R. 262-21 du même code précise que pour l'appréciation de ces revenus professionnels, et sous réserve notamment de la situation des travailleurs indépendants ayant opté pour le régime " micro-social ", " il est fait abstraction des déficits catégoriels et des moins-values subis au cours de l'année de référence ainsi que des déficits constatés au cours des années antérieures. / Ces revenus professionnels sont revalorisés en fonction du taux d'évolution en moyenne annuelle de l'indice général des prix à la consommation hors tabac entre l'année à laquelle ces revenus professionnels se rapportent et celle à laquelle est présentée la demande (...) ". L'article R. 262-23 de ce code prévoit que : " Selon les modalités prévues aux articles R. 262-18 à R. 262-22, le président du conseil départemental arrête l'évaluation des revenus professionnels non salariés nécessaires au calcul du revenu de solidarité active. A cet effet, il tient compte, soit à son initiative, soit à la demande de l'intéressé, des éléments de toute nature relatifs aux revenus professionnels de l'intéressé ". Enfin, l'article R. 262-24 de ce code dispose que : " En l'absence de déclaration ou d'imposition d'une ou plusieurs activités non salariées, le président du conseil départemental évalue le revenu au vu de l'ensemble des éléments d'appréciation fournis par le demandeur ".<br/>
<br/>
              4. D'autre part, l'article R. 262-6 du même code prévoit, pour la détermination des ressources de l'ensemble des personnes demandant à bénéficier du revenu de solidarité active, que : " Les ressources prises en compte pour la détermination du montant du revenu de solidarité active comprennent, sous les réserves et selon les modalités figurant au présent chapitre, l'ensemble des ressources, de quelque nature qu'elles soient, de toutes les personnes composant le foyer, et notamment les avantages en nature (...) ".<br/>
<br/>
              5. Il résulte des dispositions mentionnées au point 3 que, pour arrêter les revenus professionnels non salariés nécessaires au calcul du revenu de solidarité active, lorsqu'il s'agit de bénéfices industriels et commerciaux ou de bénéfices non commerciaux, le président du conseil départemental doit, en cas de déclaration ou d'imposition, se référer aux bénéfices déterminés en fonction des régimes d'imposition applicables au titre de la pénultième année, auxquels s'ajoutent les amortissements et les plus-values professionnels, et sans tenir compte des déficits catégoriels et des moins-values subis au cours de l'année de référence ainsi que des déficits constatés au cours des années antérieures. Il peut également tenir compte de tout autre élément relatif aux revenus professionnels de l'intéressé, dans le but notamment de mieux appréhender la grande variété des situations des travailleurs indépendants et de procéder à une meilleure approximation des revenus perçus par ceux-ci à la date à laquelle ils bénéficient du revenu de solidarité active.<br/>
<br/>
              6. Il suit de là que le tribunal administratif de Caen n'a pas commis d'erreur de droit en jugeant que, pour déterminer les droits au revenu de solidarité active de M.A..., qui exploitait un bar-restaurant, pour la période postérieure au 1er avril 2016, ses revenus professionnels devaient être appréciés à partir de ses revenus imposés dans la catégorie des bénéfices industriels et commerciaux en 2014. Toutefois, les ressources de M.A..., en vertu de l'article R. 262-6 du code de l'action sociale et des familles cité au point 4, comprenaient également les avantages en nature dont il bénéficiait. Par suite, en jugeant que le président du conseil départemental du Calvados ne pouvait tenir compte des dépenses de logement et d'alimentation du foyer directement prises en charge par l'entreprise exploitée par M. A...et non prises en compte dans le montant des revenus industriels et commerciaux professionnels déclarés, le tribunal administratif a commis une erreur de droit.<br/>
<br/>
              7. Il résulte de ce qui précède que le jugement attaqué doit être annulé. <br/>
<br/>
              8. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de M. A...le versement de la somme que le département du Calvados demande au titre de l'article L. 761-1 du code de justice administrative. Les dispositions de cet article et de l'article 37 de la loi du 10 juillet 1991 font obstacle à ce qu'il soit fait droit aux conclusions présentées sur leur fondement par la SCP Potier de Varde, Buk Lament, Robillot, avocat de M.A....<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les articles 1er et 2 du jugement du tribunal administratif de Caen du 10 mai 2017 sont annulés.<br/>
Article 2 : L'affaire est renvoyée, dans la mesure de la cassation prononcée, au tribunal administratif de Caen.<br/>
Article 3 : Les conclusions du département du Calvados présentées au titre de l'article L. 761-1 du code de justice administrative et celles de l'avocat de M. A...présentées au titre de cet article et de l'article 37 de la loi du 10 juillet 1991 sont rejetées.<br/>
<br/>
Article 4 : La présente décision sera notifiée au département du Calvados et à M. B...A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">04-02-06 AIDE SOCIALE. DIFFÉRENTES FORMES D'AIDE SOCIALE. REVENU MINIMUM D'INSERTION (RMI). - CALCUL DES REVENUS PROFESSIONNELS NON SALARIÉS - BÉNÉFICES INDUSTRIELS OU COMMERCIAUX OU BÉNÉFICES NON COMMERCIAUX - OBLIGATION DE SE RÉFÉRER AUX BÉNÉFICES DÉTERMINÉS EN FONCTION DES RÉGIMES D'IMPOSITION APPLICABLES AU TITRE DE LA PÉNULTIÈME ANNÉE, AUXQUELS S'AJOUTENT LES AMORTISSEMENTS ET LES PLUS-VALUES PROFESSIONNELS, ET SANS TENIR COMPTE DES DÉFICITS CATÉGORIELS ET DES MOINS-VALUES SUBIS AU COURS DE L'ANNÉE DE RÉFÉRENCE AINSI QUE DES DÉFICITS CONSTATÉS AU COURS DES ANNÉES ANTÉRIEURES - EXISTENCE - FACULTÉ DE PRENDRE EN COMPTE TOUT AUTRE ÉLÉMENT RELATIF AUX REVENUS PROFESSIONNELS DE L'INTÉRESSÉ - EXISTENCE.
</SCT>
<ANA ID="9A"> 04-02-06 Il résulte du dernier alinéa de l'article L. 262-7  du code de l'action sociale et des familles (CASF), dans sa rédaction applicable, de l'article R. 262-19, dans sa rédaction applicable, et des articles R. 262-21, R. 262-23 et R. 262-24 du même code que, pour arrêter les revenus professionnels non salariés nécessaires au calcul du revenu de solidarité active, lorsqu'il s'agit de bénéfices industriels et commerciaux ou de bénéfices non commerciaux, le président du conseil départemental doit, en cas de déclaration ou d'imposition, se référer aux bénéfices déterminés en fonction des régimes d'imposition applicables au titre de la pénultième année, auxquels s'ajoutent les amortissements et les plus-values professionnels, et sans tenir compte des déficits catégoriels et des moins-values subis au cours de l'année de référence ainsi que des déficits constatés au cours des années antérieures. Il peut également tenir compte de tout autre élément relatif aux revenus professionnels de l'intéressé, dans le but notamment de mieux appréhender la grande variété des situations des travailleurs indépendants et de procéder à une meilleure approximation des revenus perçus par ceux-ci à la date à laquelle ils bénéficient du revenu de solidarité active.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
