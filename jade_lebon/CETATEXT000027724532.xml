<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027724532</ID>
<ANCIEN_ID>JG_L_2013_07_000000359451</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/72/45/CETATEXT000027724532.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème et 7ème sous-sections réunies, 10/07/2013, 359451</TITRE>
<DATE_DEC>2013-07-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>359451</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème et 7ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SPINOSI</AVOCATS>
<RAPPORTEUR>M. Tristan Aureau</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Damien Botteghi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:359451.20130710</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 15 mai et 16 août 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. B...A..., demeurant ...; M. A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 10MA01404 du 13 mars 2012 par lequel la cour administrative d'appel de Marseille a rejeté sa requête tendant, d'une part, à l'annulation du jugement n° 0902260 du 5 février 2010 par lequel le tribunal administratif de Marseille a rejeté sa demande tendant à l'annulation de l'arrêté du 16 janvier 2009 par lequel le préfet des Bouches-du-Rhône a prononcé son expulsion du territoire français, d'autre part, à l'annulation pour excès de pouvoir de cet arrêté ainsi que de la décision de retrait de son titre de séjour, enfin, à ce qu'il soit enjoint à l'administration de lui restituer son titre de séjour sous astreinte de 100 euros par jour de retard ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat une somme de 4 000 euros sur le fondement de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
<br/>
              Vu l'accord franco-algérien du 30 décembre 1968 relatif à la circulation, à l'emploi et au séjour en France des ressortissants algériens et de leurs familles ;<br/>
<br/>
              Vu le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
<br/>
              Vu la loi n° 78-753 du 17 juillet 1978 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Tristan Aureau, Auditeur,<br/>
<br/>
              - les conclusions de M. Damien Botteghi, Rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Spinosi, avocat de M. A...; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'il ressort des énonciations de l'arrêt attaqué que, par un arrêté du 16 janvier 2009, le préfet des Bouches-du-Rhône a, sur le fondement des dispositions de l'article L. 521-1 du code de l'entrée et du séjour des étrangers et du droit d'asile, prononcé l'expulsion du territoire français de M.A..., de nationalité algérienne, au motif que la présence de ce dernier sur le territoire constituait une menace grave pour l'ordre public ; qu'alors que M. A... s'apprêtait, le 8 février 2009, à quitter volontairement le territoire français pour l'Algérie, les agents de la police de l'air et des frontières ont indiqué à l'intéressé qu'il faisait l'objet d'un arrêté d'expulsion et lui ont retiré le certificat de résidence dont il disposait et qui était valable jusqu'en 2015 ; <br/>
<br/>
              Sur l'arrêt attaqué en tant qu'il a statué sur la légalité de l'arrêté d'expulsion :<br/>
<br/>
              2.	Considérant, en premier lieu, que, pour écarter le moyen tiré de ce que l'arrêté d'expulsion pris le 16 janvier 2009 par le préfet des Bouches-du-Rhône a porté une atteinte disproportionnée au droit de l'intéressé au respect de sa vie privée et familiale garanti par l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, la cour administrative d'appel a notamment relevé que, si le père du requérant réside en France depuis 1990 ainsi que sa soeur, qui est de nationalité française, la réalité et l'intensité des liens de l'intéressé avec les membres de sa famille résidant en France n'étaient pas établies, que la mère de l'intéressé et un de ses frères résident en Grande-Bretagne, que l'intéressé est divorcé de son épouse de nationalité française avec laquelle il n'a pas eu d'enfants et qu'il n'avait pas démenti les affirmations du préfet selon lesquelles les enfants du second mariage de son père étaient installés en Algérie ; qu'en l'état de ces constatations souveraines, qui sont exemptes de dénaturation, la cour administrative d'appel a pu légalement juger que la mesure d'expulsion de M.A..., qui avait fait l'objet de treize condamnations entre 1998 et 2007 notamment pour des faits de vol avec violence et de violences conjugales, ne portait pas une atteinte disproportionnée au droit au respect de sa vie privée et familiale ; qu'en jugeant, en outre, que la condamnation de l'intéressé en 1999 à une peine d'interdiction du territoire français de trois ans, bien que non exécutée, faisait obstacle à ce que les années en cause soient prises en compte au titre des années de résidence habituelle en France, la cour n'a pas commis d'erreur de droit ;<br/>
<br/>
              3.	Considérant, en second lieu, qu'ainsi que l'a jugé la cour sans erreur de droit, la circonstance, alléguée par l'intéressé, selon laquelle son état de santé se serait détérioré postérieurement à son expulsion est dépourvue d'incidence sur la légalité de l'arrêté attaqué, laquelle s'apprécie en fonctions des circonstances de droit et de la situation de fait prévalant à la date de son édiction ; <br/>
<br/>
              4.	Considérant qu'il résulte de ce qui précède que M. A...n'est pas fondé à demander l'annulation de l'arrêt qu'il attaque en tant que cet arrêt a statué sur ses conclusions tendant à l'annulation pour excès de pouvoir de l'arrêté d'expulsion du 16 janvier 2009 ;<br/>
<br/>
              Sur l'arrêt attaqué en tant qu'il a statué sur le retrait du titre de séjour :<br/>
<br/>
              5.	Considérant que les stipulations de l'accord franco-algérien du 27 décembre 1968 relatif à la circulation, à l'emploi et au séjour en France des ressortissants algériens et de leurs familles ne font pas obstacle à l'application à un ressortissant algérien de la réglementation générale autorisant qu'il soit procédé à l'expulsion d'un étranger suivant les modalités définies par le code de l'entrée et du séjour des étrangers et du droit d'asile ; qu'une décision d'expulsion, eu égard à sa portée, a par elle-même pour effet de mettre fin au titre qui autorisait l'étranger à séjourner en France jusqu'à son intervention ; qu'il s'ensuit qu'un arrêté ordonnant l'expulsion d'un ressortissant algérien a pour effet de mettre fin au certificat de résidence, délivré sur le fondement de l'accord franco-algérien, qui permettait à ce ressortissant de résider en France avant son expulsion ; que les services de police et les unités de gendarmerie sont, en conséquence, en droit de retenir le document matérialisant le certificat de résidence lorsque l'intéressé quitte le territoire français alors qu'il a fait l'objet d'une mesure d'expulsion exécutoire ; <br/>
<br/>
              6.	Considérant, toutefois, qu'ainsi que le rappelle l'article 8 de la loi du 17 juillet 1978 portant diverses mesures d'amélioration des relations entre l'administration et le public et diverses dispositions d'ordre administratif, social et fiscal, les décisions administratives individuelles défavorables à leurs destinataires ne sont opposables à ces derniers que pour autant qu'elles leur aient été préalablement notifiées et soient, par suite, exécutoires à leur encontre ; qu'il s'ensuit que la cour administrative d'appel a commis une erreur de droit en jugeant que la circonstance que l'arrêté d'expulsion n'aurait pas été notifié à M. A...antérieurement au retrait du titre de séjour auquel ont procédé les services de police était sans incidence sur la légalité de ce retrait ; que M. A...est, dès lors, fondé à demander l'annulation de l'arrêt attaqué en tant qu'il a rejeté ses conclusions tendant à l'annulation du retrait de son titre de séjour ;<br/>
<br/>
              7.	Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler, dans cette mesure, l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              Sur la légalité du retrait du certificat de résidence :<br/>
<br/>
              8.	Considérant qu'il ressort des pièces du dossier que M. A...avait demandé, devant le tribunal administratif de Marseille, l'annulation de la décision ayant procédé au retrait du certificat de résidence dont il était titulaire, en se prévalant de ce que l'arrêté d'expulsion ne lui avait pas été notifié préalablement à ce retrait ; que le tribunal administratif a omis de statuer sur ces conclusions ; que, par suite, le jugement attaqué doit être annulé dans cette mesure ;<br/>
<br/>
              9.	Considérant qu'il y a lieu d'évoquer et de statuer immédiatement sur ces conclusions ; <br/>
<br/>
              10. Considérant qu'il ressort des pièces du dossier que des agents de la police de l'air et des frontières ont retenu le document matérialisant le certificat de résidence de M. A... le 8 février 2009, alors que celui-ci s'apprêtait à quitter volontairement le territoire français pour l'Algérie ; qu'à cette date, l'arrêté d'expulsion qui avait été pris à son encontre le 16 janvier 2009 par le préfet des Bouches-du-Rhône ne lui avait pas été notifié ; que si les services de police l'ont avisé, le 8 février 2009, de ce qu'il faisait l'objet d'un arrêté d'expulsion, ils ne lui ont pas remis d'exemplaire de cet arrêté et n'ont, ainsi, pas procédé à sa notification ; que la notification de l'arrêté n'a été faite que postérieurement, par une lettre datée du 9 février 2009, qui a été adressée à l'intéressé le 11 février 2009 ; que, par suite, ainsi qu'il a été dit précédemment, les services de police ne pouvaient légalement, faute de notification préalable, retenir le 8 février 2009 le document matérialisant le certificat de résidence de M. A...; que ce dernier est, dès lors, fondé à demander l'annulation de la décision qui a procédé à ce retrait ;<br/>
<br/>
              Sur les conclusions à fin de restitution du certificat de résidence :<br/>
<br/>
              11. Considérant qu'il appartient au Conseil d'Etat, lorsqu'il est saisi de conclusions présentées sur le fondement de l'article L. 911-1 du code de justice administrative, d'y statuer en tenant compte de la situation de droit et de fait existant à la date de sa décision ;<br/>
<br/>
              12. Considérant qu'il résulte de l'instruction que l'arrêté du 16 janvier 2009 prononçant l'expulsion de M. A...a été désormais notifié à l'intéressé, avec pour conséquence de mettre fin légalement au certificat de résidence dont il était titulaire avant son départ du territoire français ; que cette circonstance fait, par suite, obstacle à ce que ce certificat de résidence lui soit restitué ; que les conclusions à fin d'injonction ne peuvent, dès lors, qu'être rejetées ; <br/>
<br/>
              Sur les conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              13. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par M. A...au titre des dispositions de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Marseille du 13 mars 2012 est annulé en tant qu'il a rejeté les conclusions de M. A...tendant à l'annulation du retrait de son certificat de résidence.<br/>
Article 2 : Le jugement du tribunal administratif de Marseille du 5 février 2010 est annulé en tant qu'il a omis de statuer sur les conclusions tendant à l'annulation de la décision de retrait du certificat de résidence de M.A....<br/>
Article 3 : La décision du 8 février 2009 procédant au retrait du document matérialisant le certificat de résidence de M. A...est annulée.<br/>
<br/>
Article 4 : Les conclusions présentées par M. A...devant le tribunal administratif de Marseille et tendant à l'annulation du retrait de son certificat de résidence et le surplus des conclusions de son pourvoi devant le Conseil d'Etat sont rejetés. <br/>
Article 5 : La présente décision sera notifiée à M. B...A...et au ministre de l'intérieur.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">335-01-02 ÉTRANGERS. SÉJOUR DES ÉTRANGERS. AUTORISATION DE SÉJOUR. - EFFETS D'UNE DÉCISION D'EXPULSION PAR ELLE-MÊME - 1) ABROGATION DU TITRE DE SÉJOUR DE L'ÉTRANGER - EXISTENCE - 2) CONDITION POUR QUE CETTE DÉCISION AIT UN TEL EFFET - NOTIFICATION À L'INTÉRESSÉ.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">335-02 ÉTRANGERS. EXPULSION. - EFFETS D'UNE DÉCISION D'EXPULSION PAR ELLE-MÊME - 1) ABROGATION DU TITRE DE SÉJOUR DE L'ÉTRANGER - EXISTENCE - 2) CONDITION POUR QUE CETTE DÉCISION AIT UN TEL EFFET - NOTIFICATION À L'INTÉRESSÉ.
</SCT>
<ANA ID="9A"> 335-01-02 1) Une décision d'expulsion, eu égard à sa portée, a par elle-même pour effet de mettre fin au titre qui autorisait l'étranger à séjourner en France jusqu'à son intervention.,,,2) Une décision d'expulsion ne peut toutefois avoir un tel effet que si elle revêt un caractère exécutoire, et qu'elle a donc été notifiée à l'intéressé.</ANA>
<ANA ID="9B"> 335-02 1) Une décision d'expulsion, eu égard à sa portée, a par elle-même pour effet de mettre fin au titre qui autorisait l'étranger à séjourner en France jusqu'à son intervention.,,,2) Une décision d'expulsion ne peut toutefois avoir un tel effet que si elle revêt un caractère exécutoire, et qu'elle a donc été notifiée à l'intéressé.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
