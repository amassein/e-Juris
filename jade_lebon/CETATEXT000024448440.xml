<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000024448440</ID>
<ANCIEN_ID>JG_L_2011_08_000000343617</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/24/44/84/CETATEXT000024448440.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème et 2ème sous-sections réunies, 02/08/2011, 343617</TITRE>
<DATE_DEC>2011-08-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>343617</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème et 2ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Christian Vigouroux</PRESIDENT>
<AVOCATS>SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>M. Laurent Cytermann</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Bertrand Dacosta</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 30 septembre et 29 décembre 2010 au secrétariat du contentieux du Conseil d'Etat, présentés pour l'ASSOCIATION NATIONALE DES CHEMINOTS ANCIENS COMBATTANTS RESISTANTS PRISONNIERS (ANCAC), dont le siège est au 9 rue du Château-Landon à Paris (75010) ; l'ASSOCIATION NATIONALE DES CHEMINOTS ANCIENS COMBATTANTS RESISTANTS PRISONNIERS demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le décret n° 2010-890 du 29 juillet 2010, portant attribution du bénéfice de la campagne double aux anciens combattants d'Afrique du Nord, publié au Journal Officiel du 30 juillet 2010 ;<br/>
<br/>
              2°) d'enjoindre au pouvoir réglementaire, en vertu des articles L. 911-1 et L. 911-3 du code de justice administrative, de prendre un nouveau décret dans un délai d'un mois à compter de la notification de la décision à intervenir, sous astreinte de 500 euros par jour de retard ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat une somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi n° 99-882 du 18 octobre 1999 ;<br/>
<br/>
              Vu le code des pensions civiles et militaires de retraite ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Laurent Cytermann, chargé des fonctions de Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Lyon-Caen, Thiriez, avocat de l'ASSOCIATION NATIONALE DES CHEMINOTS ANCIENS COMBATTANTS RESISTANTS PRISONNIERS ET VICTIMES DE GUERRE, <br/>
<br/>
              - les conclusions de M. Bertrand Dacosta, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Lyon-Caen, Thiriez, avocat de l'ASSOCIATION NATIONALE DES CHEMINOTS ANCIENS COMBATTANTS RESISTANTS PRISONNIERS ET VICTIMES DE GUERRE, <br/>
<br/>
<br/>
<br/>Considérant que par une décision n° 328282 rendue le 17 mars 2010, le Conseil d'Etat statuant au contentieux a enjoint aux ministres chargés de la défense et du budget de prendre, en application de l'article R. 19 du code des pensions civiles et militaires de retraite, les dispositions réglementaires permettant l'attribution du bénéfice de la campagne double aux titulaires de pensions civiles et militaires de l'Etat ayant participé à la guerre d'Algérie ou aux combats en Tunisie et au Maroc et accompli à ce titre des services militaires en opérations de guerre, en fonction de la nature et de la durée de ces services ; que par décret du 29 juillet 2010, le Premier ministre a attribué le bénéfice de la campagne double aux anciens combattants d'Afrique du Nord ; que l'ASSOCIATION NATIONALE DES CHEMINOTS ANCIENS COMBATTANTS RESISTANTS PRISONNIERS demande au Conseil d'Etat l'annulation de ce décret ; <br/>
<br/>
              Sans qu'il soit besoin de se prononcer sur la fin de non-recevoir opposée à la requête de l'ASSOCIATION NATIONALE DES CHEMINOTS ANCIENS COMBATTANTS RESISTANTS PRISONNIERS  ;<br/>
<br/>
              Sur les conclusions dirigées contre l'article 2 du décret :<br/>
<br/>
              Considérant, en premier lieu, que l'article 2 du décret attaqué dispose : " Le bénéfice de la campagne double est accordé pour toute journée durant laquelle les appelés et les militaires désignés à l'article 1er ont pris part à une action de feu ou de combat ou ont subi le feu. / L'exposition invoquée en faveur de ce bénéfice sera établie par les archives collectives de l'unité à laquelle les intéressés appartenaient ou étaient rattachés. " ; qu'il résulte de ces dispositions que la participation à des actions de feu ou de combat, ou le fait d'avoir subi le feu, sont en principe établis à partir des archives de l'unité, et donc compte tenu des conditions d'engagement collectif de celle-ci ; qu'en cas d'insuffisance des archives de l'unité, il appartiendra à l'administration, sous le contrôle du juge, de prendre en compte tous les éléments à sa disposition ainsi que ceux apportés par les demandeurs ; qu'en édictant ces dispositions, le Premier ministre a fait usage de son pouvoir de définir les circonstances de temps et de lieu ouvrant droit au bénéfice de la campagne double, par une appréciation qui n'est pas entachée d'erreur manifeste ;<br/>
<br/>
              Considérant, en second lieu, que si les dispositions en vigueur applicables à d'autres conflits ont prévu la possibilité d'obtenir le bénéfice de la campagne double pour l'intégralité de la période de service sans exiger la preuve d'une participation jour par jour, le pouvoir réglementaire n'était tenu par aucun texte ni aucun principe d'adopter au cas présent des dispositions analogues ; qu'il appartenait à celui-ci de définir les conditions du bénéfice de cet avantage eu égard aux circonstances particulières de chacun de ces conflits ; <br/>
<br/>
              Sur les conclusions dirigées contre l'article 3 du décret :<br/>
<br/>
              Considérant que l'article 3 de ce décret dispose : " Les pensions de retraite liquidées à compter du 19 octobre 1999 pourront être révisées en application du présent décret, sans ouvrir droit à intérêt de retard, à compter de la demande des intéressés déposée postérieurement à l'entrée en vigueur du présent décret auprès de l'administration qui a instruit leur droit à pension. " ; <br/>
<br/>
              Considérant, en premier lieu, que la loi du 18 octobre 1999 a substitué aux mots : " aux opérations effectuées en Afrique du Nord " les mots : " à la guerre d'Algérie et aux combats de Tunisie et du Maroc " aux articles L. 1er bis, L. 243, L. 253 bis et L. 401 bis du code des pensions militaires d'invalidité et des victimes de la guerre, ainsi qu'à l'article L. 321-9 du code de la mutualité ; que par ces dispositions, le législateur a entendu permettre l'attribution du bénéfice de la campagne double aux titulaires de pensions civiles et militaires de l'Etat ayant participé à la guerre d'Algérie ou aux combats en Tunisie et au Maroc et accompli à ce titre des services militaires en opérations de guerre, selon des modalités déterminées par les ministres chargés de la défense et du budget dans le cadre des pouvoirs qui leur sont attribués par l'article R. 19 du code des pensions civiles et militaires de retraite ; qu'en revanche, il ne résulte ni des termes de la loi, ni de ses travaux préparatoires que le législateur ait entendu donner une portée rétroactive aux dispositions qu'il a édictées, seule à même de permettre la révision des pensions liquidées avant leur entrée en vigueur, les décisions relatives à l'attribution de la campagne double n'ayant pas un caractère recognitif ; que le décret attaqué n'a donc méconnu ni la loi du 18 octobre 1999, ni aucune disposition du code des pensions civiles et militaires de retraite, en ne permettant la révision que des pensions liquidées à compter du 19 octobre 1999 ;<br/>
<br/>
              Considérant, en second lieu, que la loi du 18 octobre 1999 n'ayant pas permis au pouvoir réglementaire de procéder à la révision des pensions liquidées avant son entrée en vigueur, le moyen tiré de ce que les dispositions du décret attaqué auraient méconnu, en ne permettant pas cette révision, le principe d'égalité ou les stipulations combinées de l'article 14 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et l'article 1er de son protocole additionnel, ne peut être utilement invoqué ;<br/>
<br/>
              Considérant, en troisième lieu, que le décret attaqué n'a pas procédé à une exécution incomplète de la décision n° 328282 du Conseil d'Etat en ne prévoyant pas la révision des pensions liquidées avant l'entrée en vigueur de la loi du 18 octobre 1999 ;<br/>
<br/>
              Considérant qu'il résulte de tout ce qui précède que les conclusions des requérants dirigées contre le décret attaqué doivent être rejetées ainsi que, par suite, leurs conclusions aux fins d'injonction et d'astreinte ; <br/>
<br/>
              Sur l'application des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'il soit mis à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante, le versement de la somme demandée par l'ASSOCIATION NATIONALE DES CHEMINOTS ANCIENS COMBATTANTS RESISTANTS PRISONNIERS ;<br/>
<br/>
<br/>
<br/>          D E C I D E :<br/>
        --------------<br/>
Article 1er : La requête de l'ASSOCIATION NATIONALE DES CHEMINOTS ANCIENS COMBATTANTS RESISTANTS PRISONNIERS est rejetée.<br/>
Article 2 : La présente décision sera notifiée à l'ASSOCIATION NATIONALE DES CHEMINOTS ANCIENS COMBATTANTS RESISTANTS PRISONNIERS, à la ministre du budget, des comptes publics et de la réforme de l'Etat, porte-parole du Gouvernement, au ministre de la défense et des anciens combattants et au secrétaire général du Gouvernement. <br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">08-03-05 ARMÉES ET DÉFENSE. COMBATTANTS. RETRAITE DU COMBATTANT. - BÉNÉFICE DE LA CAMPAGNE DOUBLE - PARTICIPATION À UNE ACTION DE FEU OU DE COMBAT - ETABLISSEMENT DES FAITS - 1) PRINCIPE - RÉFÉRENCE AUX ARCHIVES DE L'UNITÉ - 2) MÉTHODE EN CAS D'INSUFFISANCE DES ARCHIVES.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">48-02-03-02 PENSIONS. PENSIONS CIVILES ET MILITAIRES DE RETRAITE. PENSIONS MILITAIRES. CONDITIONS D'OUVERTURE DU DROIT À PENSION. - BÉNÉFICE DE LA CAMPAGNE DOUBLE - PARTICIPATION À UNE ACTION DE FEU OU DE COMBAT - ETABLISSEMENT DES FAITS - 1) PRINCIPE - RÉFÉRENCE AUX ARCHIVES DE L'UNITÉ - 2) MÉTHODE EN CAS D'INSUFFISANCE DES ARCHIVES.
</SCT>
<ANA ID="9A"> 08-03-05 1) Pour l'application du décret n° 2010-890 du 29 juillet 2010 portant attribution du bénéfice de la campagne double aux anciens combattants d'Afrique du Nord, c'est en principe à partir des archives de l'unité qu'est établie la participation à une action de feu ou de combat. 2) En cas d'insuffisance des archives, il appartient alors à l'administration de prendre en compte tous les éléments à sa disposition ainsi que ceux apportés par les demandeurs.</ANA>
<ANA ID="9B"> 48-02-03-02 1) Pour l'application du décret n° 2010-890 du 29 juillet 2010 portant attribution du bénéfice de la campagne double aux anciens combattants d'Afrique du Nord, c'est en principe à partir des archives de l'unité qu'est établie la participation à une action de feu ou de combat. 2) En cas d'insuffisance des archives, il appartient alors à l'administration de prendre en compte tous les éléments à sa disposition ainsi que ceux apportés par les demandeurs.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
