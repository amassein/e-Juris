<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034026103</ID>
<ANCIEN_ID>JG_L_2017_02_000000400257</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/02/61/CETATEXT000034026103.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 1ère chambres réunies, 10/02/2017, 400257</TITRE>
<DATE_DEC>2017-02-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>400257</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Laurence Franceschini</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier de Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:400257.20170210</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Paris d'annuler pour excès de pouvoir l'arrêté du 29 octobre 2015 par lequel le préfet du Pas-de-Calais lui a fait obligation de quitter le territoire français, a refusé de lui accorder un délai de départ volontaire et a fixé le pays à destination duquel il doit être éloigné, ainsi que l'arrêté du même jour ordonnant son placement en rétention administrative. Par un jugement n° 1517873/8 du 3 novembre 2015, le tribunal administratif de Paris a annulé cet arrêté et rejeté le surplus de ses conclusions.<br/>
<br/>
              Par une ordonnance n° 15PA04832 du 24 mars 2016, le président de la 5ème chambre de la cour administrative d'appel de Paris a rejeté l'appel formé par le préfet du Pas-de-Calais contre ce jugement.  <br/>
<br/>
              Par un pourvoi enregistré le 30 mai 2016 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'intérieur demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter les conclusions présentées par M. B... devant le tribunal administratif de Paris. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Laurence Franceschini, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que M. A...B...a demandé au tribunal administratif de Paris d'annuler pour excès de pouvoir l'arrêté du 29 octobre 2015 par lequel le préfet du Pas-de-Calais lui a fait obligation de quitter le territoire français, a refusé de lui accorder un délai de départ volontaire, a fixé le pays à destination duquel il doit être éloigné ainsi que l'arrêté du même jour ordonnant son placement en rétention administrative ; que par un jugement du 3 novembre 2015, le tribunal administratif de Paris a annulé l'arrêté du 29 octobre 2015 ; que par une ordonnance du 24 mars 2016, contre laquelle le ministre de l'intérieur se pourvoit en cassation, le président de la 5ème chambre de la cour administrative d'appel de Paris a rejeté comme irrecevable l'appel formé par le préfet du Pas-de-Calais contre ce jugement, au motif que sa requête méconnaissait les dispositions de l'article R. 411-1 du code de justice administrative faute que le préfet y ait indiqué le domicile exact du défendeur malgré la demande de régularisation qui lui avait été adressée ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article R. 411-1 du même code : " La juridiction est saisie par requête. La requête indique les nom et domicile des parties (...). " ; qu'en se fondant sur la méconnaissance de ces dispositions pour rejeter comme irrecevable l'appel formé par le préfet du Pas-de-Calais contre le jugement annulant son arrêté, au motif qu'il n'indique pas le domicile du défendeur, alors que cette prescription de l'article R. 411-1, s'agissant du domicile des parties défenderesses, vise seulement à faciliter la mise en oeuvre du caractère contradictoire de la procédure, le président de la 5ème chambre de la cour administrative d'appel de Paris a entaché son ordonnance d'une erreur de droit ; que cette ordonnance doit en conséquence être annulée ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'ordonnance du 24 mars 2016 du président de la 5ème chambre de la cour administrative d'appel de Paris du 24 mars 2016 est annulée.<br/>
<br/>
      Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Paris. <br/>
<br/>
Article 3 : La présente décision sera notifiée au ministre de l'intérieur et à Monsieur A...B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-08-01-01 PROCÉDURE. VOIES DE RECOURS. APPEL. RECEVABILITÉ. - MENTION DES NOMS ET DOMICILES DES PARTIES ADVERSES (ART. R. 411-1 DU CJA) - OBLIGATION À PEINE D'IRRECEVABILITÉ - ABSENCE [RJ1].
</SCT>
<ANA ID="9A"> 54-08-01-01 La prescription de l'article R. 411-1 du code de justice administrative (CJA) en vertu de laquelle un appelant doit mentionner dans sa requête les noms et domiciles des parties défenderesses, vise seulement à faciliter la mise en oeuvre du caractère contradictoire de la procédure et ne constitue pas une condition de recevabilité de l'appel.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., CE, 30 mai 2016, Me Deltour et autres, n°s 383928 e.a., aux Tables sur un autre point.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
