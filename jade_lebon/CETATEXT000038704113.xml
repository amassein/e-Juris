<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038704113</ID>
<ANCIEN_ID>JG_L_2019_06_000000425975</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/70/41/CETATEXT000038704113.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 28/06/2019, 425975</TITRE>
<DATE_DEC>2019-06-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>425975</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BORE, SALVE DE BRUNETON, MEGRET ; SCP COUTARD, MUNIER-APAIRE</AVOCATS>
<RAPPORTEUR>M. Christian Fournier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Cytermann</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:425975.20190628</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Le préfet d'Ille-et-Vilaine a demandé au tribunal administratif de Rennes d'annuler la délibération du 17 mars 2017 par laquelle le conseil municipal de Bovel a refusé le déploiement des compteurs " Linky " sur le territoire de la commune ainsi que la délibération du 23 juin 2017 par laquelle ce même conseil municipal a rejeté son recours gracieux.<br/>
<br/>
              Par un jugement n° 1703803 du 7 décembre 2017, le tribunal administratif de Rennes a annulé ces délibérations.<br/>
<br/>
              Par un arrêt n° 18NT00454 du 5 octobre 2018, la cour administrative d'appel de Nantes a rejeté l'appel formé par la commune de Bovel contre ce jugement.<br/>
<br/>
              Par un pourvoi et un mémoire en réplique, enregistrés les 5 décembre 2018 et 27 mai 2019 au secrétariat du contentieux du Conseil d'Etat, la commune de Bovel demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat une somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'énergie ;<br/>
              - le code général des collectivités territoriales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Christian Fournier, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Laurent Cytermann, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boré, Salve de Bruneton, Mégret, avocat de la commune de Bovel et à la SCP Coutard, Munier-Apaire, avocat de la société Enedis ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que par une délibération du 17 mars 2017, le conseil municipal de la commune de Bovel a refusé l'élimination sur son territoire des compteurs électriques existants et leur remplacement par les compteurs communicants dénommés " Linky ". Par une délibération du 23 juin 2017, le conseil municipal a décidé de maintenir son opposition au déploiement dans la commune des compteurs dits " Linky " et rejeté en conséquence le recours gracieux formé par le préfet d'Ille-et-Vilaine. Par un jugement du 7 décembre 2017, le tribunal administratif de Rennes a, faisant droit au déféré du préfet d'Ille-et-Vilaine, annulé ces deux délibérations. La commune de Bovel se pourvoit en cassation contre l'arrêt du 5 octobre 2018 par lequel la cour administrative de Nantes a rejeté l'appel qu'elle a formé contre ce jugement.<br/>
<br/>
              2. D'une part, aux termes du premier alinéa de l'article L. 1321-1 du code général des collectivités territoriales : " Le transfert d'une compétence entraîne de plein droit la mise à la disposition de la collectivité bénéficiaire des biens meubles et immeubles utilisés, à la date de ce transfert, pour l'exercice de cette compétence ". Aux termes de l'article L. 1321-4 du même code : " Les conditions dans lesquelles les biens mis à disposition, en application de l'article L. 1321-2, peuvent faire l'objet d'un transfert en pleine propriété à la collectivité bénéficiaire sont définies par la loi ".<br/>
<br/>
              3. D'autre part, aux termes du premier alinéa de l'article L. 322-4 du code de l'énergie : " Sous réserve des dispositions de l'article L. 324-1, les ouvrages des réseaux publics de distribution, y compris ceux qui, ayant appartenu à Electricité de France, ont fait l'objet d'un transfert au 1er janvier 2005, appartiennent aux collectivités territoriales ou à leurs groupements désignés au IV de l'article L. 2224-31 du code général des collectivités territoriales. ". Aux termes du deuxième alinéa du IV de l'article L. 2224-31 du code général des collectivités territoriales : " L'autorité organisatrice d'un réseau public de distribution, exploité en régie ou concédé, est la commune ou l'établissement public de coopération auquel elle a transféré cette compétence (...) ".<br/>
<br/>
              4. Il résulte de la combinaison des dispositions précitées que la propriété des ouvrages des réseaux publics de distribution d'électricité est attachée à la qualité d'autorité organisatrice de ces réseaux. En conséquence, lorsqu'une commune transfère sa compétence en matière d'organisation de la distribution d'électricité à un établissement public de coopération, celui-ci devient autorité organisatrice sur le territoire de la commune et propriétaire des ouvrages des réseaux en cause, y compris des installations de comptage visées à l'article D. 342-1 du code de l'énergie.<br/>
<br/>
              5. Il ressort des pièces du dossier soumis aux juges du fond que la compétence en matière d'organisation des réseaux publics de distribution d'électricité dans la commune de Bovel a été transférée, le 1er mars 2010, au syndicat mixte départemental d'énergie 35. Par suite, à compter du transfert de cette compétence, le syndicat mixte est devenu, en qualité d'autorité organisatrice du service public de distribution d'électricité sur le territoire de la commune de Bovel, propriétaire des ouvrages affectés aux réseaux de distribution de cette commune, notamment des compteurs électriques qui y sont installés. Dès lors, la cour administrative d'appel n'a pas commis d'erreur de droit en jugeant, par un arrêt suffisamment motivé, que la commune de Bovel n'était pas propriétaire des compteurs électriques installés sur son territoire.<br/>
<br/>
              6. Il résulte de ce qui précède que la commune de Bovel n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque. En conséquence, ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ne peuvent qu'être rejetées.<br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de Bovel la somme de 3 000 euros à verser à la société Enedis au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la commune de Bovel est rejeté.<br/>
Article 2 : La commune de Bovel versera à la société Enedis une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3: La présente décision sera notifiée à la commune de Bovel, au ministre de la cohésion des territoires et des relations avec les collectivités territoriales et à la société Enedis.<br/>
Copie sera transmise au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">135-01-03 COLLECTIVITÉS TERRITORIALES. DISPOSITIONS GÉNÉRALES. BIENS DES COLLECTIVITÉS TERRITORIALES. - TRANSFERT PAR UNE COMMUNE DE SA COMPÉTENCE EN MATIÈRE D'ORGANISATION DE LA DISTRIBUTION D'ÉLECTRICITÉ À UN ÉTABLISSEMENT PUBLIC DE COOPÉRATION - CONSÉQUENCE - TRANSFERT À CET ÉTABLISSEMENT DE LA PROPRIÉTÉ DES OUVRAGES DES RÉSEAUX EN CAUSE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">29 ENERGIE. - TRANSFERT PAR UNE COMMUNE DE SA COMPÉTENCE EN MATIÈRE D'ORGANISATION DE LA DISTRIBUTION D'ÉLECTRICITÉ À UN ÉTABLISSEMENT PUBLIC DE COOPÉRATION - CONSÉQUENCE - TRANSFERT À CET ÉTABLISSEMENT DE LA PROPRIÉTÉ DES OUVRAGES DES RÉSEAUX EN CAUSE.
</SCT>
<ANA ID="9A"> 135-01-03 Il résulte de la combinaison du premier alinéa de l'article L. 1321-1, de l'article L. 1321-4 et du deuxième alinéa du IV de l'article L. 2224-31 du code général des collectivités territoriales (CGCT) ainsi que du premier alinéa de l'article L. 322-4 du code de l'énergie que la propriété des ouvrages des réseaux publics de distribution d'électricité est attachée à la qualité d'autorité organisatrice de ces réseaux. En conséquence, lorsqu'une commune transfère sa compétence en matière d'organisation de la distribution d'électricité à un établissement public de coopération, celui-ci devient autorité organisatrice sur le territoire de la commune, et propriétaire des ouvrages des réseaux en cause, y compris des installations de comptage visées à l'article D. 342-1 du code de l'énergie.</ANA>
<ANA ID="9B"> 29 Il résulte de la combinaison du premier alinéa de l'article L. 1321-1, de l'article L. 1321-4 et du deuxième alinéa du IV de l'article L. 2224-31 du code général des collectivités territoriales (CGCT) ainsi que du premier alinéa de l'article L. 322-4 du code de l'énergie que la propriété des ouvrages des réseaux publics de distribution d'électricité est attachée à la qualité d'autorité organisatrice de ces réseaux. En conséquence, lorsqu'une commune transfère sa compétence en matière d'organisation de la distribution d'électricité à un établissement public de coopération, celui-ci devient autorité organisatrice sur le territoire de la commune, et propriétaire des ouvrages des réseaux en cause, y compris des installations de comptage visées à l'article D. 342-1 du code de l'énergie.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
