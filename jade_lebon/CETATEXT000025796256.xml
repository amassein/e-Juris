<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025796256</ID>
<ANCIEN_ID>JG_L_2012_04_000000342589</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/79/62/CETATEXT000025796256.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 4ème sous-sections réunies, 24/04/2012, 342589</TITRE>
<DATE_DEC>2012-04-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>342589</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 4ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Philippe Martin</PRESIDENT>
<AVOCATS>SCP DELVOLVE, DELVOLVE ; SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Jean-Dominique Langlais</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Jean-Philippe Thiellay</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:342589.20120424</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la requête, enregistrée le 19 août 2010 au secrétariat du contentieux du Conseil d'Etat, présentée pour la société PARISII IMAGES, dont le siège est au 14, rue Charles V à Paris (75004) ; la société PARISII IMAGES demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'avis du 15 juin 2010 par lequel le Conseil supérieur de l'audiovisuel (CSA), saisi par le procureur de la République près le tribunal de grande instance de Paris, en application de l'article 42-12 de la loi du 30 septembre 1986 relative à la liberté de communication, s'est prononcé sur les offres de reprise présentées dans le cadre d'un plan de cession avec location-gérance de la société IDF Télé, éditrice du programme " Cap 24 " diffusé en région parisienne ;<br/>
<br/>
              2°) après annulation, de formuler son propre avis sur les offres des candidats à la reprise déclarés au 21 mai 2010 ;<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de commerce ;<br/>
<br/>
              Vu la loi n° 86-1067 du 30 septembre 1986 ;<br/>
<br/>
              Vu le décret n° 94-749 du 2 septembre 1994 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Philippe Ranquet, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Delvolvé, Delvolvé, avocat de la SOCIETE PARISII IMAGES et de la SCP Piwnica, Molinié, avocat de la société Nextradio TV, <br/>
<br/>
              - les conclusions de M. Jean-Philippe Thiellay, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Delvolvé, Delvolvé, avocat de la SOCIETE PARISII IMAGES et à la SCP Piwnica, Molinié, avocat de la société Nextradio TV ;<br/>
<br/>
<br/>
<br/>
              Sur l'objet de la requête :<br/>
<br/>
              Considérant qu'aux termes de l'article 42-12 de la loi du 30 septembre 1986 relative à la liberté de communication, dans sa rédaction en vigueur issue, en dernier lieu, de la loi du 17 décembre 2009 relative à la lutte contre la fracture numérique : " Lorsqu'un débiteur soumis à une procédure de sauvegarde, de redressement judiciaire ou de liquidation judiciaire est titulaire d'une autorisation relative à un service de communication audiovisuelle et que la cession d'une activité ou de l'entreprise est envisagée dans les conditions prévues aux articles L. 626-1, L. 631-22 ou L. 642-1 et suivants du code de commerce, le tribunal peut, à la demande du procureur de la République et après que ce magistrat a obtenu, dans un délai d'un mois, l'avis favorable du Conseil supérieur de l'audiovisuel, dans des conditions prévues par décret, autoriser la conclusion d'un contrat de location-gérance conformément aux articles L. 642-13 et suivants du code de commerce. Pendant la durée de cette location-gérance, le cessionnaire bénéficie, nonobstant les dispositions de l'article 42-3 de la présente loi, de l'autorisation qui avait été accordée au débiteur. / Si, au cours de la location-gérance, le cessionnaire n'obtient pas l'autorisation nécessaire du Conseil supérieur de l'audiovisuel, le tribunal, d'office ou à la demande du commissaire à l'exécution du plan de sauvegarde ou de redressement, du liquidateur ou du procureur de la République, ordonne la résiliation du contrat de location-gérance et la résolution du plan. Dans ce cas, il n'y a pas lieu à application des dispositions de l'article L. 642-17 du code de commerce ni à versement de dommages et intérêts. (...) " ;<br/>
<br/>
              Considérant que l'avis émis par le Conseil supérieur de l'audiovisuel dans le cadre de ces dispositions peut faire l'objet, devant le Conseil d'Etat, tant d'une question préjudicielle du juge saisi de la procédure de sauvegarde, de redressement judiciaire ou de liquidation judiciaire, tendant à l'appréciation de la légalité de cet avis, que d'un recours direct de toute personne y ayant intérêt, le cas échéant assorti d'une demande de suspension ; qu'un tel recours conserve un objet, s'il est dirigé contre un avis défavorable à une offre de reprise ou un avis favorable à une offre que le tribunal de commerce n'a, par la suite, pas retenue, jusqu'à la date à laquelle le jugement du tribunal de commerce autorisant la conclusion d'un contrat de location-gérance devient définitif et, s'il est dirigé contre l'avis favorable donné au candidat autorisé par le tribunal à conclure le contrat de location-gérance, tant que le Conseil supérieur de l'audiovisuel n'a pas délivré en propre à ce dernier une autorisation d'exploiter un service de communication audiovisuelle, dès lors qu'à défaut de celle-ci, l'avis favorable lui permet de bénéficier de l'autorisation qui avait été accordée à l'éditeur dont il a repris l'activité, l'éventuelle annulation de l'avis entraînant l'application des dispositions du deuxième alinéa de l'article 42-12 de la loi du 30 septembre 1986 ;<br/>
<br/>
              Considérant que, dans le cadre d'une procédure de redressement judiciaire concernant la société IDF Télé, qui exploitait en vertu d'une autorisation délivrée par le Conseil supérieur de l'audiovisuel le service de télévision " Cap 24 " diffusé dans la région Ile-de-France, le conseil supérieur a émis le 15 juin 2010, à la demande du procureur de la République près le tribunal de grande instance de Paris, un avis favorable à quatre candidats à la reprise de cette société, dont la société Nextradio TV, et défavorable aux trois autres candidats, dont la société PARISII IMAGES ; que, par un jugement du 28 juin 2010, le tribunal de commerce de Paris a arrêté le plan de cession de la société IDF Télé en faveur de la société Nextradio TV ; que la société PARISII IMAGES a, le 19 août 2010, saisi le Conseil d'Etat d'une requête tendant à l'annulation des avis du Conseil supérieur de l'audiovisuel ; que cette requête ayant été présentée à une date où le jugement du tribunal de commerce était devenu définitif, elle était dépourvue d'objet, dès la date de son introduction, en ce qu'elle était dirigée contre les avis défavorables et contre les avis favorables aux trois candidats à qui le tribunal de commerce n'a pas accordé l'autorisation de conclure de contrat de location-gérance ; qu'elle est, par suite, irrecevable dans cette mesure ; qu'elle est en revanche recevable en tant qu'elle est dirigée contre l'avis favorable à l'offre de la société Nextradio TV ;<br/>
<br/>
              Sur la légalité de l'avis attaqué en tant qu'il est favorable à l'offre de la société Nextradio TV :<br/>
<br/>
              Considérant, en premier lieu, qu'il ressort du procès-verbal de la séance plénière du 15 juin 2010 que, contrairement à ce que soutient la société PARISII IMAGES, l'avis attaqué a été rendu par le collège des membres du Conseil supérieur de l'audiovisuel ; que la circonstance qu'un projet d'avis lui ait été soumis par son président n'est pas de nature à entacher sa délibération d'irrégularité ;<br/>
<br/>
              Considérant, en deuxième lieu, que l'avis expose en quoi, au regard des critères fixés par la loi du 30 septembre 1986, l'offre de la société Nextradio TV appelait une appréciation favorable ; que le moyen tiré d'un défaut de motivation manque ainsi, en tout état de cause, en fait ;<br/>
<br/>
              Considérant, en troisième lieu, que l'avis rendu par le Conseil supérieur de l'audiovisuel en vertu de l'article 42-12 de la loi du 30 septembre 1986 a pour objet d'identifier les candidats à la reprise d'un service audiovisuel dont les projets présentent un intérêt suffisant au regard des critères fixés par cette même loi, notamment par les dispositions de son article 29, et non uniquement, comme le soutient la société PARISII IMAGES, au regard des choix opérés lors de l'attribution de l'autorisation à l'éditeur dont la reprise est envisagée ; qu'il ne ressort pas des pièces du dossier qu'en estimant que le projet de la société Nextradio TV présentait un intérêt pour le public permettant l'attribution d'une autorisation d'exploiter un service de télévision à vocation régionale, le Conseil supérieur de l'audiovisuel ait commis une erreur d'appréciation ;<br/>
<br/>
              Considérant, en quatrième lieu, que l'irrégularité dont serait entaché, selon la société PARISII IMAGES, le calcul du prix de cession proposé par la société Nextradio TV est sans incidence sur la légalité de l'avis attaqué ;<br/>
<br/>
              Considérant, enfin, que si la société PARISII IMAGES soutient que le Conseil supérieur de l'audiovisuel aurait manqué à l'obligation d'équité envers les candidats à la reprise, ce moyen n'est pas assorti des précisions permettant d'en apprécier la portée et le bien-fondé ;<br/>
<br/>
              Considérant qu'il résulte de ce qui précède que la société PARISII IMAGES n'est pas fondée à demander l'annulation de l'avis attaqué en tant qu'il est favorable à l'offre de la société Nextradio TV ; que doivent être rejetées, par voie de conséquence et en tout état de cause, ses conclusions tendant à ce que soit formulé un nouvel avis ;<br/>
<br/>
              Sur l'application des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              Considérant que ces dispositions font obstacle à ce que soit mise à la charge de l'Etat, qui n'est pas la partie perdante dans la présente instance, la somme demandée à ce titre par la société PARISII IMAGES ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées au même titre par la société Nextradio TV ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
		Article 1er : La requête de la société PARISII IMAGES est rejetée.<br/>
<br/>
Article 2 : Les conclusions présentées par la société Nextradio TV au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 3 : La présente décision sera notifiée à la société PARISII IMAGES, au Conseil supérieur de l'audiovisuel et aux sociétés Nextradio TV, AB Thématiques, Premier Investissement et Bolloré Média.<br/>
Copie en sera adressée pour information aux sociétés LoyalTouch et G-Participations et au ministre de la culture et de la communication.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">17-03-02-005-01 COMPÉTENCE. RÉPARTITION DES COMPÉTENCES ENTRE LES DEUX ORDRES DE JURIDICTION. COMPÉTENCE DÉTERMINÉE PAR UN CRITÈRE JURISPRUDENTIEL. ACTES. ACTES ADMINISTRATIFS. - AVIS DU CSA SUR LE PROJET DE LOCATION-GÉRANCE ENVISAGÉ PAR LE TRIBUNAL DE COMMERCE EN APPLICATION DE L'ARTICLE 42-12 DE LA LOI DU 30 SEPTEMBRE 1986 [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-01-01-01-01 PROCÉDURE. INTRODUCTION DE L'INSTANCE. DÉCISIONS POUVANT OU NON FAIRE L'OBJET D'UN RECOURS. ACTES CONSTITUANT DES DÉCISIONS SUSCEPTIBLES DE RECOURS. AVIS ET PROPOSITIONS. - AVIS DU CSA SUR LE PROJET DE LOCATION-GÉRANCE ENVISAGÉ PAR LE TRIBUNAL DE COMMERCE EN APPLICATION DE L'ARTICLE 42-12 DE LA LOI DU 30 SEPTEMBRE 1986 [RJ2].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-05-05 PROCÉDURE. INCIDENTS. NON-LIEU. - AVIS DU CSA SUR LE PROJET DE LOCATION-GÉRANCE ENVISAGÉ PAR LE TRIBUNAL DE COMMERCE EN APPLICATION DE L'ARTICLE 42-12 DE LA LOI DU 30 SEPTEMBRE 1986 - CAS DANS LESQUELS LE RECOURS CONSERVE OU NON UN OBJET.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">56-01 RADIODIFFUSION SONORE ET TÉLÉVISION. CONSEIL SUPÉRIEUR DE L'AUDIOVISUEL. - AVIS DU CSA SUR LE PROJET DE LOCATION-GÉRANCE ENVISAGÉ PAR LE TRIBUNAL DE COMMERCE EN APPLICATION DE L'ARTICLE 42-12 DE LA LOI DU 30 SEPTEMBRE 1986 - 1) COMPÉTENCE DE LA JURIDICTION ADMINISTRATIVE - EXISTENCE [RJ1] - 2) DÉCISION FAISANT GRIEF - EXISTENCE [RJ2] - 3) CAS DANS LESQUELS LE RECOURS CONSERVE OU NON UN OBJET.
</SCT>
<ANA ID="9A"> 17-03-02-005-01 Le juge administratif est compétent pour connaître d'un recours dirigé contre l'avis que donne le Conseil supérieur de l'audiovisuel (CSA) sur le projet de location-gérance envisagé par le tribunal de commerce en application de l'article 42-12 de la loi n° 86-1067 du 30 septembre 1986.</ANA>
<ANA ID="9B"> 54-01-01-01-01 L'avis que donne le Conseil supérieur de l'audiovisuel (CSA) sur le projet de location-gérance envisagé par le tribunal de commerce en application de l'article 42-12 de la loi n° 86-1067 du 30 septembre 1986 est une décision faisant grief, susceptible de recours.</ANA>
<ANA ID="9C"> 54-05-05 Un recours dirigé contre l'avis que donne le Conseil supérieur de l'audiovisuel (CSA) sur le projet de location-gérance envisagé par le tribunal de commerce en application de l'article 42-12 de la loi n° 86-1067 du 30 septembre 1986 conserve un objet :,,a) s'il est dirigé contre un avis défavorable à une offre de reprise ou un avis favorable à une offre que le tribunal de commerce n'a, par la suite, pas retenue, jusqu'à la date à laquelle le jugement du tribunal de commerce autorisant la conclusion d'un contrat de location-gérance devient définitif ;,,b) s'il est dirigé contre l'avis favorable donné au candidat autorisé par le tribunal à conclure le contrat de location-gérance, tant que le CSA n'a pas délivré en propre à ce dernier une autorisation d'exploiter un service de communication audiovisuelle, dès lors qu'à défaut de celle-ci, l'avis favorable lui permet de bénéficier de l'autorisation qui avait été accordée à l'éditeur dont il a repris l'activité, l'éventuelle annulation de l'avis entraînant l'application des dispositions du deuxième alinéa de l'article 42-12 de la loi du 30 septembre 1986.</ANA>
<ANA ID="9D"> 56-01 1) Le juge administratif est compétent pour connaître d'un recours dirigé contre l'avis que donne le Conseil supérieur de l'audiovisuel (CSA) sur le projet de location-gérance envisagé par le tribunal de commerce en application de l'article 42-12 de la loi n° 86-1067 du 30 septembre 1986.,,2) L'avis que rend le CSA dans ce cadre est une décision faisant grief, susceptible de recours.,,3) Un tel recours conserve un objet :,,a) s'il est dirigé contre un avis défavorable à une offre de reprise ou un avis favorable à une offre que le tribunal de commerce n'a, par la suite, pas retenue, jusqu'à la date à laquelle le jugement du tribunal de commerce autorisant la conclusion d'un contrat de location-gérance devient définitif ;,,b) s'il est dirigé contre l'avis favorable donné au candidat autorisé par le tribunal à conclure le contrat de location-gérance, tant que le CSA n'a pas délivré en propre à ce dernier une autorisation d'exploiter un service de communication audiovisuelle, dès lors qu'à défaut de celle-ci, l'avis favorable lui permet de bénéficier de l'autorisation qui avait été accordée à l'éditeur dont il a repris l'activité, l'éventuelle annulation de l'avis entraînant l'application des dispositions du deuxième alinéa de l'article 42-12 de la loi du 30 septembre 1986.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 17 octobre 1997, S.A.R.L. Cirtes et Sociét é Radio Monte-Carlo, n° 158871, T. pp. 629-737-1053.,,[RJ2] Ab. jur. sur ce point CE, 17 octobre 1997, S.A.R.L. Cirtes et Société Radio Monte-Carlo, n° 158871, T. pp. 629-737-1053.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
