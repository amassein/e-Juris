<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036606816</ID>
<ANCIEN_ID>JG_L_2001_11_0000239840</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/60/68/CETATEXT000036606816.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 12/11/2001, 239840, Publié au recueil Lebon</TITRE>
<DATE_DEC>2001-11-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>239840</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC/>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2001:239840.20011112</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<p>Vu la requête, enregistrée au secrétariat du contentieux du Conseil d'Etat le 7 novembre 2001 présentée pour la commune de MONTREUIL-BELLAY, représentée par son maire ; la commune de Montreuil-Bellay demande au juge des référés du Conseil d'Etat, sur le fondement de l'article L. 523-1 (alinéa 2) du code de justice administrative :<br clear="none"/>
<br clear="none"/>
1°) d'annuler l'ordonnance en date du 19 octobre 2001 par laquelle le juge des référés du tribunal administratif de Nantes a, sur le fondement de l'article L. 521-2 du code de justice administrative, ordonné la suspension de l'exécution d'une délibération prise par son conseil municipal à l'effet d'exercer le droit de préemption urbain à l'égard de biens immobiliers appartenant à M. et Mme A... situés sur les parcelles cadastrées D 201 n°s 1902 et 1865 au lieudit "camp de Méron" ;<br clear="none"/>
<br clear="none"/>
2°) de rejeter la demande présentée devant le juge des référés du tribunal administratif de Nantes conjointement par la société civile immobilière "De Méron" et par la société "Sud Crema" ;<br clear="none"/>
<br clear="none"/>
<br clear="none"/>
la commune de Montreuil-Bellay expose qu'elle a prévu de créer un pôle économique important dans la zone industrielle de Méron dans le cadre de la Communauté d'agglomération Saumur-Loire-développement ; qu'une charte de développement économique durable est en cours d'adoption ; qu'elle prévoit notamment qu'une partie de la zone de Méron devrait accueillir des ouvrages publics destinés au traitement des eaux ; qu'est également prévue l'inclusion de la zone de Méron dans une zone d'aménagement concerté ; qu'une délibération du 14 juin 2001 du conseil de la communauté d'agglomération a approuvé la passation d'une convention publique d'aménagement avec la Société d'équipement du département de Maine-et-Loire (Sodemel) qui a été signé le 17 octobre 2001 ; qu'en vue de s'assurer la maîtrise foncière à l'intérieur de la zone et de contrôler l'évolution des occupations privatives, en particulier, celles qui seraient de nature à contrarier la préservation des ressources en eau, le conseil municipal a exercé le 6 septembre 2001 le droit de préemption urbain lors de la cession par M. et Mme A... de terrains ayant respectivement pour superficie 15 715 mètres carrés et 23 598 mètres carrés ; que cette délibération a été contestée suivant la procédure de l'article L. 521-2 du code de justice administrative par la société civile immobilière de Méron qui s'était portée acquéreur des biens cédés et par la société Sud Crema désireuse d'exercer une activité de collecte, transport et incinération de cadavres d'animaux familiers dans la zone industrielle ; que l'ordonnance rendue le 19 octobre 2001 par le juge des référés du tribunal administratif de Nantes ayant suspendu l'exécution de la délibération du conseil municipal doit être annulée ; qu'en effet, contrairement à ce qu'a estimé le premier juge la liberté contractuelle n'est pas une liberté fondamentale ; que si le droit de propriété constitue effectivement une liberté fondamentale au sens de l'article L. 521-2 du code, le droit de préemption ne le met pas en cause car le propriétaire d'un bien soumis à ce droit ne perd pas sa liberté de le céder ; que s'il est vrai qu'en pareil cas le propriétaire d'un bien soumis au droit de préemption perd la possibilité de choisir librement l'acquéreur du bien qu'il a décidé de céder, il n'y a pas pour autant atteinte à une liberté constitutionnellement garantie ; qu'en effet, la liberté contractuelle ne jouit d'une protection de niveau constitutionnel que lorsque l'atteinte dont elle est l'objet constitue, de par son extrême gravité et seulement pour les contrats déjà conclus, une méconnaissance de la liberté protégée par l'article 4 de la déclaration des droits de l'homme et du citoyen ; que la condition d'urgence posée par l'article L. 521-2 du code n'est pas davantage remplie ; qu'à cet égard, il y a lieu de relever que la société civile immobilière de Méron ne subit du fait de l'exercice du droit de préemption aucune contrainte financière grave et immédiate ; que la société Sud Crema, titulaire d'un bail sur les terrains concernés, subira uniquement un changement de propriétaire ; que, par ailleurs, le premier juge a négligé l'intérêt public qui s'attache à ce que la commune contrôle le devenir foncier d'une zone d'aménagement concerté en cours d'élaboration, qui plus est dans un endroit sensible pour l'environnement ; qu'en tout état de cause, l'atteinte éventuellement causée à une liberté fondamentale par la décision d'exercer le droit de préemption n'est pas manifestement grave car ce droit est devenu une technique usuelle de l'interventionnisme foncier des collectivités publiques ; qu'il n'y a pas non plus d'illégalité manifeste car, d'une part, le défaut d'avis du service des domaines dans le délai réglementaire constitue un simple vice de procédure, et d'autre part, la question de la légalité de la délibération au regard des dispositions de l'article L. 210-1 du code de l'urbanisme est sujette à discussion ;<br clear="none"/>
<br clear="none"/>
Vu l'ordonnance attaquée ;<br clear="none"/>
<br clear="none"/>
Vu enregistré comme ci-dessus le 8 novembre 2001, le mémoire en défense présenté pour la société civile immobilière de Méron et pour la société Sud Crema qui conclut au rejet de la requête et à ce que la commune de Montreuil-Bellay soit condamnée à leur payer la somme de 15 000 F au titre de l'article L. 761-1 du code de justice administrative ; que la liberté contractuelle constitue une liberté fondamentale au sens de l'article L. 521-2 du code de justice administrative car il s'agit d'une liberté à laquelle le législateur a entendu accorder une protection juridictionnelle particulière ; qu'au surplus, la délibération du conseil municipal porte atteinte au droit de propriété, lequel a pour corollaire le droit de disposer librement de son bien et notamment le libre choix de l'acquéreur de celui-ci ; qu'elle porte également atteinte à la liberté du commerce et de l'industrie dans la mesure où elle fait obstacle à l'exercice de l'activité projetée par la société Sud Crema, laquelle a repris au début de l'année 2001, les activités de la Sarl Crematorium animalier centre ouest, à la suite d'une procédure de redressement judiciaire ; qu'il est satisfait à la condition d'urgence puisque l'acte de vente entre la commune et M. et Mme A... devait être passé devant notaire le 22 octobre 2001 et que la passation d'un tel acte serait de nature à faire définitivement échec au projet de la société Sud Crema ; qu'aucun intérêt public ne s'oppose à la suspension de la délibération du conseil municipal, faute pour la commune de justifier de tout projet précis d'opération dans la zone industrielle de Méron ; qu'enfin, c'est à juste titre que le juge des référés a estimé que la délibération litigieuse était manifestement illégale au regard de l'article L. 210-1 du code de l'urbanisme car elle est fondée sur la seule circonstance que l'ensemble immobilier préempté "s'inscrit dans la perspective générale de l'aménagement de la zone industrielle de Méron" ;<br clear="none"/>
<br clear="none"/>
Vu les autres pièces du dossier ;<br clear="none"/>
<br clear="none"/>
Vu la Constitution du 4 octobre 1958 notamment son Préambule et les articles 34 et 72 ;<br clear="none"/>
<br clear="none"/>
Vu le code de l'urbanisme modifié notamment par la loi n° 2000-1208 du 13 décembre 2000 en particulier ses articles L. 210-1, L. 211-1 à L. 211-7, L. 213-1 à L. 213-18, L. 300-1, L. 314-1 à L. 314-9, R. 211-1 à R. 211-8, R. 213-1 à R. 213-26 ;<br clear="none"/>
<br clear="none"/>
Vu le code général des collectivités territoriales, modifié notamment par la loi n° 99-586 du 12 juillet 1999, en particulier ses articles L. 5216-1 et suivants ;<br clear="none"/>
<br clear="none"/>
Vu le code civil, notamment ses articles 6, 1101, 1134, 1165, 1582 et 1589 ;<br clear="none"/>
<br clear="none"/>
Vu le code de l'environnement, notamment son article L. 110-1 ;<br clear="none"/>
<br clear="none"/>
Vu la loi n° 99-533 du 25 juin 1999 d'orientation pour l'aménagement et le développement durable du territoire, notamment son article 26 ;<br clear="none"/>
<br clear="none"/>
Vu le code de justice administrative, notamment ses articles L. 511-2 (alinéa 2), L. 521-2, L. 522-1, L. 523-1, L. 761-1 et R. 522-1 et suivants ;<br clear="none"/>
<br clear="none"/>
Après avoir convoqué à une audience publique d'une part la commune de Montreuil-Bellay et d'autre part, la société civile immobilière de Méron et la société Sud Crema ;<br clear="none"/>
<br clear="none"/>
Vu le procès-verbal de l'audience publique du vendredi 9 novembre 2001 à 10 heures, à laquelle ont été entendus Me Luc-Thaler, avocat au Conseil d'Etat et à la Cour de cassation pour la commune de Montreuil-Bellay et Me Mandelkern, avocat au Conseil d'Etat et à la Cour de cassation pour la société civile immobilière de Méron et la société Sud Crema et au terme de laquelle le juge des référés a décidé de poursuivre l'instruction en application de l'article R. 522-8 du code de justice administrative et de prescrire une nouvelle audience ;<br clear="none"/>
<br clear="none"/>
Vu enregistrées comme ci-dessus le 9 novembre 2001 les pièces produites pour la commune de Montreuil-Bellay ;<br clear="none"/>
<br clear="none"/>
Vu enregistrées comme ci-dessus le 9 novembre 2001 les pièces produites pour la société civile immobilière de Méron et la société Sud Crema ;<br clear="none"/>
<br clear="none"/>
Vu le procès-verbal de l'audience publique du lundi 12 novembre 2001 à 11 heures, à laquelle ont été entendus Me Luc-Thaler, avocat au Conseil d'Etat et à la Cour de cassation pour la commune de Montreuil-Bellay et Me Mandelkern, avocat au Conseil d'Etat et à la Cour de cassation pour la société civile immobilière de Méron et la société Sud Crema ;<br clear="none"/>
<br clear="none"/>
<br clear="none"/>
Considérant qu'aux termes de l'article L. 521-2 du code de justice administrative : "Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public (...) aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale..." ; que le respect de ces conditions revêt un caractère cumulatif ;<br clear="none"/>
<br clear="none"/>
Considérant qu'il ressort des pièces du dossier, tel que celui-ci a été complété devant le juge d'appel, que M. et Mme A... propriétaires sur le territoire de la commune de Montreuil-Bellay de deux parcelles d'une superficie totale de 39 313 mètres carrés ont conclu, le 12 juillet 2001, avec la société civile immobilière de Méron, une promesse de vente de ces terrains pour un prix stipulé de 525 000 F ; que la société civile immobilière de Méron a pour seuls associés le gérant majoritaire de la Sarl Sud Crema et son épouse ; que cette dernière société, après la mise en liquidation judiciaire de la Sarl Crematorium animalier du Centre Ouest, est devenue titulaire, d'une part, d'un droit de bail sur certains des immeubles visés par la promesse de vente et, d'autre part, a bénéficié du transfert à son profit de l'autorisation d'exploiter un crématorium animalier au lieu-dit "Camp de Méron", dans la zone industrielle de Méron ; que, dans la mesure où les terrains objet de la promesse de vente se trouvent compris dans un secteur où la commune, depuis une délibération de son conseil municipal du 11 septembre 1987, est titulaire du droit de préemption urbain, M. et Mme A... ont déposé en mairie le 20 juillet 2001 une déclaration d'intention d'aliéner ; que le directeur des services fiscaux qui, en vertu de l'article R. 213-6 du code de l'urbanisme, doit être destinataire d'un exemplaire d'une telle déclaration dès sa réception en mairie, n'a été consulté, aux fins d'émettre l'avis exigé par l'article R. 213-21 du code précité, que le 3 septembre ; que, sans attendre que l'avis sollicité ait été émis, le conseil municipal de Montreuil-Bellay a, par une délibération du 6 septembre 2001 décidé d'exercer, au prix de 525 000 F fixé par le vendeur, le droit de préemption urbain au motif que l'ensemble immobilier cédé "s'inscrit dans la perspective de l'aménagement de la zone industrielle de Méron" ; que le directeur des services fiscaux de Maine-et-Loire a, par un avis rendu le 12 septembre 2001, estimé que le prix de cession était "acceptable" ;<br clear="none"/>
<br clear="none"/>
Considérant que pour prescrire par une ordonnance du 19 octobre 2001, la suspension de la délibération du conseil municipal sur demande conjointe de la société civile immobilière de Méron et de la société Sud Crema, le juge des référés du tribunal administratif de Nantes, qui avait été saisi par ces deux sociétés sur le fondement de l'article L. 521-2 du code de justice administrative, a d'abord relevé que cette délibération était entachée d'une double illégalité, d'une part, en ce qu'elle est intervenue à la suite d'une procédure irrégulière faute d'avoir été prise au vu de l'avis du service des domaines, d'autre part et surtout, en ce qu'elle ne satisfait pas à l'exigence de motivation imposée par l'article L. 210-1 du code de l'urbanisme dans la mesure où la commune s'est montrée incapable de justifier d'aucun projet précis d'opération dans la zone considérée ; qu'il a ensuite estimé qu'il y avait urgence à prescrire la suspension en raison de l'imminence de la passation d'un acte notarié consacrant la cession entre le vendeur et la commune ; qu'il a jugé enfin que la délibération litigieuse portait une atteinte grave à la liberté contractuelle qu'il a qualifié de liberté fondamentale au sens de l'article L. 521-2 du code de justice administrative ; que, dans leurs observations sur l'appel interjeté par la commune à l'encontre de l'ordonnance, la société civile immobilière de Méron et la société Sud Crema font valoir que l'atteinte portée à la liberté contractuelle se double d'une mise en cause du libre choix par le propriétaire d'un bien de l'acquéreur de ce dernier et d'une entrave apportée au libre exercice par la société Sud Crema de son activité professionnelle dans la zone industrielle de Méron ;<br clear="none"/>
<br clear="none"/>
Considérant que la mise en oeuvre de la protection juridictionnelle particulière instituée par l'article L. 521-2 du code de justice administrative implique qu'il soit satisfait non seulement à la condition d'urgence inhérente à la procédure de référé mais également que l'illégalité commise par une personne publique revête un caractère manifeste et ait pour effet de porter une atteinte grave à une liberté fondamentale ; qu'il résulte tant des termes de l'article L. 521-2 que du but dans lequel la procédure qu'il instaure a été créée que doit exister un rapport direct entre l'illégalité relevée à l'encontre de l'autorité administrative et la gravité de ses effets au regard de l'exercice de la liberté fondamentale en cause ;<br clear="none"/>
<br clear="none"/>
Considérant en outre que, pour apprécier le degré de gravité que peut revêtir une atteinte portée à la liberté d'entreprendre, à la libre disposition de son bien par un propriétaire ou à la liberté contractuelle, il y a lieu de prendre en compte les limitations de portée générale qui ont été introduites par la législation pour permettre certaines interventions jugées nécessaires de la puissance publique dans les relations entre particuliers ; que tel est le cas notamment en matière d'urbanisme de la possibilité reconnue par la loi aux personnes publiques de disposer, dans certaines zones, d'un droit d'acquisition prioritaire d'un bien librement mis en vente par son propriétaire ;<br clear="none"/>
<br clear="none"/>
Considérant que la promesse de vente du 12 juillet 2001 entre les propriétaires et la société civile immobilière de Méron a été conclue sous la condition suspensive que toute personne physique ou morale titulaire d'un droit de préemption renonce à l'exercer et que, pour le cas où le bénéficiaire d'un droit de préemption l'exercerait aux prix et conditions fixés par la promesse de vente, les parties au contrat en reconnaîtraient la caducité, sans indemnité de part et d'autre ; qu'eu égard à ces stipulations l'exercice par la commune au cas présent du droit de préemption urbain, même s'il est entaché d'illégalité, n'est pas pour autant constitutif d'une atteinte grave à la libre disposition de son bien par tout propriétaire ou à l'économie d'un contrat légalement conclu ;<br clear="none"/>
<br clear="none"/>
Considérant par ailleurs que l'exercice par la commune du droit de préemption n'affecte ni le droit de bail dont est titulaire la société Sud Crema sur les installations de crémation animalière et leurs annexes, ni l'autorisation accordée le 18 novembre 1999 au titre de la législation sur les installations classées et dont le transfert a été opéré à son profit le 16 juillet 2001 ; qu'ainsi la délibération litigieuse ne saurait être regardée comme portant une atteinte grave à la liberté du commerce et de l'industrie qui est une composante de la liberté fondamentale d'entreprendre ;<br clear="none"/>
<br clear="none"/>
Considérant qu'il résulte de ce qui précède que la commune de Montreuil-Bellay est fondée à soutenir que c'est à tort que le juge des référés du tribunal administratif de Nantes a estimé que toutes les conditions requises pour l'application des dispositions de l'article L. 521-2 du code de justice administrative étaient remplies ; que l'ordonnance de suspension rendue sur le fondement de ce dernier texte, et non de l'article L. 521-1 du même code, doit par suite être annulée ;<br clear="none"/>
<br clear="none"/>
Sur les conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative :<br clear="none"/>
<br clear="none"/>
Considérant que les dispositions de cet article font obstacle à ce que la commune de Montreuil-Bellay, qui n'est pas dans la présente instance la partie perdante, soit condamnée à verser à la société civile immobilière de Méron et à la société Sud Crema la somme de 15 000 F qu'elles demandent au titre des frais exposés par elles et non compris dans les dépens ;<br clear="none"/>
<br clear="none"/>
<br clear="none"/>
<br clear="none"/>
O R D O N N E :<br clear="none"/>
------------------<br clear="none"/>
Article 1er : L'ordonnance en date du 19 octobre 2001 du juge des référés du tribunal administratif de Nantes est annulée.<br clear="none"/>
Article 2 : La demande présentée par la société civile immobilière de Méron et la société Sud Crema devant le juge des référés sur le fondement de l'article L. 521-2 du code de justice administrative est rejetée.<br clear="none"/>
Article 3 : Les conclusions de la société civile immobilière de Méron et de la société Sud Crema tendant à l'application de l'article L. 761-1 du code de justice administrative sont rejetées.<br clear="none"/>
Article 4 : La présente ordonnance sera notifiée à la commune de MONTREUIL-BELLAY, à la société civile immobilière de Méron et à la société Sud Crema. Copie en sera adressée pour information au préfet de Maine-et-Loire.<br clear="none"/>
</p>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-035-03-03-01-01 PROCÉDURE. PROCÉDURES INSTITUÉES PAR LA LOI DU 30 JUIN 2000. RÉFÉRÉ TENDANT AU PRONONCÉ DE MESURES NÉCESSAIRES À LA SAUVEGARDE D'UNE LIBERTÉ FONDAMENTALE (ART. L. 521-2 DU CODE DE JUSTICE ADMINISTRATIVE). CONDITIONS D'OCTROI DE LA MESURE DEMANDÉE. ATTEINTE GRAVE ET MANIFESTEMENT ILLÉGALE À UNE LIBERTÉ FONDAMENTALE. LIBERTÉ FONDAMENTALE. - EXISTENCE - LIBERTÉ D'ENTREPRENDRE, LIBERTÉ DU COMMERCE ET DE L'INDUSTRIE, LIBRE DISPOSITION DE SON BIEN PAR UN PROPRIÉTAIRE ET LIBERTÉ CONTRACTUELLE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-035-03-03-01-02 PROCÉDURE. PROCÉDURES INSTITUÉES PAR LA LOI DU 30 JUIN 2000. RÉFÉRÉ TENDANT AU PRONONCÉ DE MESURES NÉCESSAIRES À LA SAUVEGARDE D'UNE LIBERTÉ FONDAMENTALE (ART. L. 521-2 DU CODE DE JUSTICE ADMINISTRATIVE). CONDITIONS D'OCTROI DE LA MESURE DEMANDÉE. ATTEINTE GRAVE ET MANIFESTEMENT ILLÉGALE À UNE LIBERTÉ FONDAMENTALE. ATTEINTE GRAVE ET MANIFESTEMENT ILLÉGALE. - 1) NÉCESSITÉ D'UN RAPPORT DIRECT ENTRE L'ILLÉGALITÉ RELEVÉE ET LA GRAVITÉ DE SES EFFETS AU REGARD DE L'EXERCICE DE LA LIBERTÉ FONDAMENTALE EN CAUSE - 2) NÉCESSITÉ DE PRENDRE EN COMPTE LES LIMITATIONS DE PORTÉE GÉNÉRALE INTRODUITES PAR LA LÉGISLATION.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">68-02-01-01 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PROCÉDURES D'INTERVENTION FONCIÈRE. PRÉEMPTION ET RÉSERVES FONCIÈRES. DROITS DE PRÉEMPTION. - DEMANDE DE SUSPENSION DE L'EXÉCUTION D'UNE DÉCISION D'EXERCICE DU DROIT DE PRÉEMPTION SUR LE FONDEMENT DE L'ARTICLE L.521-2 DU CODE DE JUSTICE ADMINISTRATIVE - ATTEINTE À LA LIBERTÉ D'ENTREPRENDRE, À LA LIBRE DISPOSITION DE SON BIEN PAR UN PROPRIÉTAIRE OU À LA LIBERTÉ CONTRACTUELLE - MODE D'APPRÉCIATION - 1) PRISE EN COMPTE DU RAPPORT ENTRE L'ILLÉGALITÉ RELEVÉE ET LA GRAVITÉ DE SES EFFETS AU REGARD DE L'EXERCICE DES LIBERTÉS FONDAMENTALES EN CAUSE - 2) PRISE EN COMPTE DES LIMITATIONS DE PORTÉE GÉNÉRALE INTRODUITES PAR LA LÉGISLATION.
</SCT>
<ANA ID="9A"> 54-035-03-03-01-01 La liberté d'entreprendre, la liberté du commerce et de l'industrie qui en est une composante, la libre disposition de son bien par un propriétaire et la liberté contractuelle constituent des libertés fondamentales au sens de l'article L.521-2 du code de justice administrative.</ANA>
<ANA ID="9B"> 54-035-03-03-01-02 ll résulte tant des termes de l'article L. 521-2 que du but dans lequel la procédure qu'il instaure a été créée que 1) doit exister un rapport direct entre l'illégalité relevée à l'encontre de l'autorité administrative et la gravité de ses effets au regard de l'exercice de la liberté fondamentale en cause. 2) En outre, pour apprécier le degré de gravité que peut revêtir une atteinte portée à une liberté fondamentale, il y a lieu de prendre en compte les limitations de portée générale qui ont été introduites par la législation. En matière d'urbanisme, la gravité des atteintes à la liberté d'entreprendre, à la libre disposition de son bien par un propriétaire ou à la liberté contractuelle doit être appréciée en tenant compte de la législation qui a pour objet de permettre certaines interventions jugées nécessaires de la puissance publique dans les relations entre particuliers, et notamment de la possibilité reconnue par la loi aux personnes publiques de disposer, dans certaines zones, d'un droit d'acquisition prioritaire d'un bien librement mis en vente par son propriétaire.</ANA>
<ANA ID="9C"> 68-02-01-01 Demande présentée devant le juge des référés, sur le fondement de l'article L.521-2, tendant à la suspension de l'exécution d'une décision d'une commune d'exercer son droit de préemption, au motif que cette décision serait manifestement illégale et porterait une atteinte grave à la liberté d'entreprendre, à la libre disposition de son bien par un propriétaire ou à la liberté contractuelle. ll résulte tant des termes de l'article L. 521-2 que du but dans lequel la procédure qu'il instaure a été créée que 1) doit exister un rapport direct entre l'illégalité relevée à l'encontre de l'autorité administrative et la gravité de ses effets au regard de l'exercice de la liberté fondamentale en cause. 2) En outre, pour apprécier le degré de gravité que peut revêtir une atteinte portée à une liberté fondamentale, il y a lieu de prendre en compte les limitations de portée générale qui ont été introduites par la législation. En matière d'urbanisme, la gravité des atteintes à la liberté d'entreprendre, à la libre disposition de son bien par un propriétaire ou à la liberté contractuelle doit être appréciée en tenant compte de la législation qui a pour objet de permettre certaines interventions jugées nécessaires de la puissance publique dans les relations entre particuliers, et notamment de la possibilité reconnue par la loi aux personnes publiques de disposer, dans certaines zones, d'un droit d'acquisition prioritaire d'un bien librement mis en vente par son propriétaire.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Ordonnance du juge des référés, 23 mars 2001, Société Lidl, A ; 9 avril 2001, Belrose et autres, B.


</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
