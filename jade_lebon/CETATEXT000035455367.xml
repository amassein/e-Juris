<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035455367</ID>
<ANCIEN_ID>JG_L_2017_07_000000410620</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/45/53/CETATEXT000035455367.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 07/07/2017, 410620</TITRE>
<DATE_DEC>2017-07-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>410620</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Vincent Ploquin-Duchefdelaville</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:410620.20170707</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une ordonnance n° 16MA03334 du 16 mai 2017, enregistrée le 17 mai 2017 au secrétariat du contentieux du Conseil d'Etat, le président de la 3ème chambre de la cour administrative d'appel de Marseille, avant qu'il soit statué sur la requête de M. et MmeA..., a décidé, par application des dispositions de l'article 23-2 de l'ordonnance n° 58-1067 du 7 novembre 1958, de transmettre au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution de l'article 123 bis du code général des impôts dans sa rédaction issue de la loi du 30 décembre 1998 de finances pour 1999. <br/>
<br/>
              Dans la question prioritaire de constitutionnalité transmise le 17 mai 2017 au secrétariat du contentieux du Conseil d'Etat, M. et Mme A...soutiennent que l'article 123 bis du code général des impôts, applicable au litige, méconnaît le principe d'égalité devant la loi et le principe d'égalité devant les charges publiques garantis par les articles 6 et 13 de la Déclaration des droits de l'homme et du citoyen. <br/>
<br/>
              Par un mémoire, enregistré le 3 mai 2017 au greffe de la cour administrative d'appel de Marseille, et un nouveau mémoire, enregistré le 14 juin au secrétariat de la section du contentieux du Conseil d'Etat, le ministre de l'économie et des finances et le ministre de l'action et des comptes publics soutiennent que les conditions posées par l'article 23-4 de l'ordonnance du 7 novembre 1958 ne sont pas remplies, en particulier celles tenant à l'absence de déclaration de conformité de la disposition par le Conseil constitutionnel dans les motifs et le dispositif d'une de ses décisions. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule et son article 61-1 ; <br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code général des impôts et, notamment, son article 123 bis du code général des impôts ; <br/>
              - la décision n° 2016-614 QPC du 1er mars 2017 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Vincent Ploquin-Duchefdelaville, auditeur,  <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public.<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il résulte des dispositions de l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que, lorsqu'une juridiction relevant du Conseil d'Etat a transmis à ce dernier, en application de l'article 23-2 de cette même ordonnance, la question de la conformité à la Constitution d'une disposition législative, le Conseil constitutionnel est saisi de cette question de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux. <br/>
<br/>
              2. Dans la question prioritaire transmise, M. et Mme A...soutiennent à l'appui de leur contestation des impositions auxquelles ils ont été assujettis en 2006, que les dispositions de l'article 123 bis du code général des impôts, dans leur rédaction issue de la loi du 30 décembre 1998 de finances pour 1999, méconnaissent les principes d'égalité devant la loi et d'égalité devant les charges publiques au motif, d'une part, qu'elles instaurent au 1 une présomption irréfragable de fraude pour tout contribuable détenant directement ou indirectement 10 % au moins des parts, droits financiers ou droits de vote dans une personne morale, un organisme, une fiducie ou une institution comparable établi ou constitué hors de France et soumis à un régime fiscal privilégié, et, d'autre part, qu'elles définissent forfaitairement, au second alinéa du 3, un montant minimal de revenu imposable, applicable lorsque l'entité juridique est localisée dans un Etat ou un territoire n'ayant pas conclu de convention d'assistance administrative avec la France. <br/>
<br/>
              3. Aux termes de l'article 123 bis du code général des impôts, dans sa version issue de la loi du 30 décembre 1998 de finances pour 1999 : " 1. Lorsqu'une personne physique domiciliée en Francedétient directement ou indirectement 10 % au moins des actions, parts, droits financiers ou droits de vote dans une personne morale, un organisme, une fiducie ou une institution comparable, établi ou constitué hors de France et soumis à un régime fiscal privilégié, les bénéfices ou les revenus positifs de cette personne morale, organisme, fiducie ou institution comparable sont réputés constituer un revenu de capitaux mobiliers de cette personne physique dans la proportion des actions, parts ou droits financiers qu'elle détient directement ou indirectement lorsque l'actif ou les biens de la personne morale, de l'organisme, de la fiducie ou de l'institution comparable sont principalement constitués de valeurs mobilières, de créances, de dépôts ou de comptes courants. / Pour l'application du premier alinéa, le caractère privilégié d'un régime fiscal est déterminé conformément aux dispositions de l'article 238 A par comparaison avec le régime fiscal applicable à une société ou collectivité mentionnée au 1 de l'article 206 (...). / 3. Les bénéfices ou les revenus positifs mentionnés au 1 sont réputés acquis le premier jour du mois qui suit la clôture de l'exercice de la personne morale, de l'organisme, de la fiducie ou de l'institution comparable établi ou constitué hors de France ou, en l'absence d'exercice clos au cours d'une année, le 31 décembre. Ils sont déterminés selon les règles fixées par le présent code comme si les personnes morales, organismes, fiducies ou institutions comparables étaient imposables à l'impôt sur les sociétés en France. L'impôt acquitté localement sur les bénéfices ou revenus positifs en cause par la personne morale, l'organisme, la fiducie ou l'institution comparable est déductible du revenu réputé constituer un revenu de capitaux mobiliers de la personne physique, dans la proportion mentionnée au 1, à condition d'être comparable à l'impôt sur les sociétés. Toutefois, lorsque la personne morale, l'organisme, la fiducie ou l'institution comparable est établi ou constitué dans un Etat ou territoire n'ayant pas conclu de convention d'assistance administrative avec la France, le revenu imposable de la personne physique ne peut être inférieur au produit de la fraction de l'actif net ou de la valeur nette des biens de la personne morale, de l'organisme, de la fiducie ou de l'institution comparable, calculée dans les conditions fixées au 1, par un taux égal à celui mentionné au 3° du 1 de l'article 39 (...)". <br/>
<br/>
              Sur le second alinéa du 3 de l'article 123 bis du code général des impôts, dans sa version issue de la loi du 30 décembre 1998 :<br/>
<br/>
              4. Par sa décision n° 2016-614 QPC du 1er mars 2017, le Conseil constitutionnel a, dans ses motifs et son dispositif, déclaré conforme à la Constitution le second alinéa du 3 de l'article 123 bis du code général des impôts, dans sa version issue de la loi du 30 décembre 2009, sous la réserve d'interprétation mentionnée au point 12, selon laquelle cet alinéa ne saurait, sans porter une atteinte disproportionnée au principe d'égalité devant les charges publiques, faire obstacle à ce que le contribuable puisse être autorisé à apporter la preuve que le revenu réellement perçu par l'intermédiaire de l'entité juridique est inférieur au revenu défini forfaitairement en application de ces dispositions. Il y a, dès lors, lieu de considérer que la version initiale de cet alinéa, issue de la loi du 30 décembre 1998, similaire dans sa substance à celui sur lequel le Conseil constitutionnel s'est prononcé dans sa décision précitée, est conforme à la Constitution, sous la même réserve. Il en résulte qu'il n'y a pas lieu de renvoyer, dans cette mesure, la question prioritaire de constitutionnalité.   <br/>
<br/>
              Sur le 1 de l'article 123 bis du code général des impôts, dans sa version issue de la loi du 30 décembre 1998 :<br/>
<br/>
              5. Ces dispositions du 1 de l'article 123 bis du code général des impôts, applicables au litige dont est saisie la cour administrative d'appel de Marseille au sens et pour l'application de l'article 23-4 de l'ordonnance du 7 novembre 1958, n'ont pas déjà été déclarées conformes à la Constitution par le Conseil constitutionnel. La question de savoir si elles portent atteinte aux droits et libertés garantis par la Constitution, notamment aux principes d'égalité devant la loi et d'égalité devant les charges publiques, en réputant constituer des revenus de capitaux mobiliers les bénéfices et revenus positifs d'une structure établie hors de France et soumise à un régime fiscal privilégié, comme des revenus de capitaux mobiliers acquis par le contribuable domicilié en Franceà hauteur de sa participation dans cette structure, sans l'autoriser à apporter la preuve de ce que l'interposition de celle-ci n'a ni pour objet, ni pour effet de lui permettre de contourner la loi fiscale française, présente un caractère sérieux. Il y a, ainsi, lieu de renvoyer la question prioritaire de constitutionnalité invoquée.   <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La question de la conformité à la Constitution du 1 de l'article 123 bis du code général des impôts, dans sa version issue de la loi du 30 décembre 1998, est renvoyée au Conseil constitutionnel.<br/>
<br/>
Article 2 : Il n'y a pas lieu de renvoyer au Conseil Constitutionnel la question de la conformité à la Constitution du second alinéa du 3 de l'article 123 bis du code général des impôts. <br/>
<br/>
Article 3 : La présente décision sera notifiée à M. et MmeA..., au Premier ministre et au ministre de l'action et des comptes publics.<br/>
Copie en sera adressée au Conseil constitutionnel ainsi qu'à la cour administrative d'appel de Marseille.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-10-05-02-03 PROCÉDURE. - CAS OÙ UNE VERSION ULTÉRIEURE D'UNE DISPOSITION, SIMILAIRE DANS SA SUBSTANCE À LA DISPOSITION CRITIQUÉE, A DÉJÀ ÉTÉ DÉCLARÉE CONFORME À LA CONSTITUTION AVEC UNE RÉSERVE D'INTERPRÉTATION [RJ1].
</SCT>
<ANA ID="9A"> 54-10-05-02-03 Conseil constitutionnel ayant, dans les motifs et le dispositif d'une de ses décisions, déclaré conforme à la Constitution le second alinéa du 3 de l'article 123 bis du code général des impôts (CGI), dans sa version issue d'une loi du 30 décembre 2009, sous une réserve d'interprétation.... ,,Il y a lieu de considérer que la version initiale de cet alinéa, similaire dans sa substance à celui sur lequel le Conseil constitutionnel s'est prononcé, est conforme à la Constitution sous la même réserve.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Comp. s'agissant de dispositions analogues mais distinctes, CE, 9 mai 2017, M.,n° 407999, p. 168.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
