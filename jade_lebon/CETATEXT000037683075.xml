<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037683075</ID>
<ANCIEN_ID>JG_L_2018_12_000000409934</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/68/30/CETATEXT000037683075.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème - 10ème chambres réunies, 03/12/2018, 409934</TITRE>
<DATE_DEC>2018-12-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>409934</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème - 10ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ORTSCHEIDT ; SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR>M. Aurélien Caron</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:409934.20181203</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, un mémoire en réplique et un autre mémoire enregistrés le 20 avril 2017 et les 5 mars et 21 juin 2018 au secrétariat du contentieux du Conseil d'Etat, Mme A...D..., M. C...B...et l'Entreprise Unipersonnelle à Responsabilité Limitée (EURL) Abbatial Immobilier demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler la décision n° 2015-11 du 19 juillet 2016 par laquelle la commission des sanctions de l'Autorité de contrôle prudentiel et de résolution (ACPR) a prononcé un blâme et une sanction pécuniaire de 300 000 euros à l'encontre de la Caisse de retraite du personnel des avocats près les cours d'appel (CREPA) et ordonné la publication de cette décision au registre officiel de l'Autorité de contrôle prudentiel et de résolution ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat une somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code monétaire et financier ; <br/>
              - le code de la sécurité sociale ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Aurélien Caron, auditeur,<br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Ortscheidt, avocat de MmeD..., de M. B...et de la société Abbatial Immobilier et à la SCP Matuchansky, Poupot, Valdelièvre, avocat de l'Autorité de contrôle prudentiel et de résolution.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Aux termes de l'article L. 612-16 du code monétaire et financier : " (...) III.- Les décisions prononcées par la commission des sanctions peuvent faire l'objet d'un recours de pleine juridiction devant le Conseil d'Etat par les personnes sanctionnées et par le président de l'Autorité de contrôle prudentiel et de résolution, après accord de la formation du collège de supervision ou du collège de résolution à l'origine de la notification des griefs, dans un délai de deux mois suivant leur notification (...) ".<br/>
<br/>
              2. Par une décision n° 2015-11 du 19 juillet 2016, la commission des sanctions de l'Autorité de contrôle prudentiel et de résolution (ACPR) a prononcé, à l'encontre de la Caisse de retraite du personnel des avocats près les cours d'appel (CREPA), institution de prévoyance chargée de garantir le personnel salarié des avocats contre les risques vie, décès, incapacité et invalidité, un blâme assorti d'une sanction pécuniaire de 300 000 euros et ordonné que sa décision fasse l'objet d'une publication au registre officiel de l'ACPR. Cette décision retient deux griefs, tirés respectivement du versement, aux membres du bureau du conseil d'administration de la CREPA, d'indemnités de fonction prohibées par l'article R. 931-3-23 du code de la sécurité sociale alors en vigueur et de la conclusion, en violation de l'article R. 931-3-22 du même code, de conventions avec l'EURL Abbatial Immobilier, dont le gérant et unique associé était le fils de MmeD..., qui occupait quant à elle les fonctions de présidente ou première vice-présidente du conseil d'administration de la CREPA, pour la réalisation et la gestion de placements immobiliers.<br/>
<br/>
              3. Si, dans ses motifs, cette décision mentionne, sous forme d'ailleurs anonyme, le rôle de Mme D...et de l'EURL Abbatial Immobilier, son dispositif ne leur fait pas grief, pas davantage qu'à M. B..., ancien président du conseil d'administration. Ainsi que le soutient l'ACPR en défense, ils sont, dès lors, irrecevables à en demander l'annulation, sans pouvoir utilement faire valoir qu'à la suite de cette décision, la CREPA, dont le bureau du conseil d'administration a été renouvelé, a assigné à comparaître devant le tribunal de grande instance de Paris, d'une part, Mme D...et M.B..., en leur qualité d'anciens dirigeants, pour obtenir, à raison de leur responsabilité personnelle dans les manquements relevés par la commission des sanctions de l'ACPR, le remboursement de l'amende, d'autre part l'EURL Abbatial Immobilier, en sa qualité de partie aux conventions illégalement conclues, pour obtenir le remboursement des sommes qui lui avaient été versées en exécution de ces conventions. <br/>
<br/>
              3. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise, à ce titre, à la charge de l'Etat, qui n'est pas la partie perdante dans la présente instance. Dans les circonstances de l'espèce, il y a en revanche lieu de mettre à la charge des requérants une somme globale de 3 000 euros à verser à l'Etat au même titre.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de MmeD..., de M. B...et de l'EURL Abbatial Immobilier est rejetée. <br/>
Article 2 : MmeD..., M. B...et l'EURL Abbatial Immobilier verseront à l'Etat une somme globale de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à Mme A...D..., représentant désigné, pour l'ensemble des requérants et à l'Autorité de contrôle prudentiel et de résolution.<br/>
Copie en sera adressée au ministre de l'économie et des finances.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">13-027 CAPITAUX, MONNAIE, BANQUES. - RECOURS CONTRE UNE SANCTION DE L'ACPR (ART. L. 612-16 DU CMF) - INTÉRÊT POUR AGIR D'UN TIERS CONTRE LA SANCTION - ABSENCE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-01-04-01 PROCÉDURE. INTRODUCTION DE L'INSTANCE. INTÉRÊT POUR AGIR. ABSENCE D'INTÉRÊT. - RECOURS CONTRE UNE SANCTION DE L'ACPR (ART. L. 612-16 DU CMF) - INTÉRÊT POUR AGIR D'UN TIERS CONTRE LA SANCTION - ABSENCE [RJ1].
</SCT>
<ANA ID="9A"> 13-027 Sanction de l'Autorité de contrôle prudentiel et de résolution (ACPR) contre une caisse de retraite. Griefs retenus mettant en évidence le rôle joué par deux anciens présidents du conseil d'administration, et relevant la conclusion illégale d'une convention avec une entreprise tierce.,,Si, dans ses motifs, cette décision mentionne, sous forme d'ailleurs anonyme, le rôle de ces dirigeants et de cette entreprise, son dispositif ne leur fait pas grief. Ils sont, dès lors, irrecevables à en demander l'annulation, sans pouvoir utilement faire valoir qu'à la suite de cette sanction, la caisse de retraite a assigné à comparaître les intéressés en leur qualité d'anciens dirigeants pour obtenir, à raison de leur responsabilité personnelle dans les manquements relevés par la commission des sanctions de l'ACPR, le remboursement de l'amende, et l'entreprise, en sa qualité de partie aux conventions illégalement conclues, pour obtenir le remboursement des sommes qui lui avaient été versées en exécution de ces conventions.</ANA>
<ANA ID="9B"> 54-01-04-01 Sanction de l'Autorité de contrôle prudentiel et de résolution (ACPR) contre une caisse de retraite. Griefs retenus mettant en évidence le rôle joué par deux anciens présidents du conseil d'administration, et relevant la conclusion illégale d'une convention avec une entreprise.,,Si, dans ses motifs, cette décision mentionne, sous forme d'ailleurs anonyme, le rôle de ces dirigeants et de cette entreprise, son dispositif ne leur fait pas grief. Ils sont, dès lors, irrecevables à en demander l'annulation, sans pouvoir utilement faire valoir qu'à la suite de cette sanction, la caisse de retraite a assigné à comparaître les intéressés en leur qualité d'anciens dirigeants pour obtenir, à raison de leur responsabilité personnelle dans les manquements relevés par la commission des sanctions de l'ACPR, le remboursement de l'amende, et l'entreprise, en sa qualité de partie aux conventions illégalement conclues, pour obtenir le remboursement des sommes qui lui avaient été versées en exécution de ces conventions.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., s'agissant de l'AMF, CE, 13 juillet 2006,,, n° 285081, T. p. 741.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
