<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033404365</ID>
<ANCIEN_ID>JG_L_2016_11_000000401660</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/40/43/CETATEXT000033404365.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 16/11/2016, 401660</TITRE>
<DATE_DEC>2016-11-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>401660</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP RICHARD ; SCP DELAPORTE, BRIARD</AVOCATS>
<RAPPORTEUR>M. Grégory Rzepski</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier Henrard</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:401660.20161116</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Procédure contentieuse antérieure :<br/>
<br/>
              La société Travaux électriques du Midi (TEM) a demandé au juge des référés du tribunal administratif de Marseille, statuant sur le fondement de l'article L. 551-1 du code de justice administrative, d'annuler la décision de la ville de Marseille rejetant l'offre du groupement dont elle était membre et attribuant le marché ayant pour objet l'exploitation et le maintien des installations d'éclairage public de la commune à la société SNEF, ainsi que l'ensemble de la procédure de passation depuis l'origine et toutes les décisions subséquentes prises par le pouvoir adjudicateur.<br/>
<br/>
              Par une ordonnance n° 1604918 du 5 juillet 2016, le juge des référés du tribunal administratif de Marseille a annulé la décision de la ville de Marseille rejetant l'offre du groupement dont était membre la société TEM et attribuant le marché ayant pour objet l'exploitation et le maintien de ses installations d'éclairage public à la société SNEF, ainsi que l'ensemble de la procédure de passation de ce marché.<br/>
<br/>
              Procédure devant le Conseil d'État :<br/>
<br/>
              1° Sous le n° 401660, par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 20 juillet et 3 août 2016 au secrétariat du contentieux du Conseil d'Etat, la société SNEF demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de mettre à la charge de la société Travaux électriques du Midi la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              2° Sous le n° 401710, par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 21 juillet et 3 août 2016 au secrétariat du contentieux du Conseil d'Etat, la ville de Marseille demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de rejeter les conclusions de première instance de la société TEM ;<br/>
<br/>
              3°) de mettre à la charge de la société TEM la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - le code des marchés publics ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Grégory Rzepski, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Olivier Henrard, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Richard, avocat de la société SNEF, à la SCP Delaporte, Briard, avocat de la société Travaux électriques du midi et à Me Haas, avocat de la ville de Marseille ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 25 octobre 2016, présentée par la société Travaux électriques du midi ; <br/>
<br/>
<br/>
<br/>1. Considérant que les pourvois de la société SNEF et de la ville de Marseille sont dirigés contre la même ordonnance ; qu'il y a lieu de les joindre pour qu'ils fassent l'objet d'une seule décision ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 551-1 du code de justice administrative : " Le président du tribunal administratif, ou le magistrat qu'il délègue, peut être saisi en cas de manquement aux obligations de publicité et de mise en concurrence auxquelles est soumise la passation par les pouvoirs adjudicateurs de contrats administratifs ayant pour objet l'exécution de travaux, la livraison de fournitures ou la prestation de services, avec une contrepartie économique constituée par un prix ou un droit d'exploitation, ou la délégation d'un service public (...) " ; qu'aux termes du I de l'article L. 551-2 de ce code : " I. Le juge peut ordonner à l'auteur du manquement de se conformer à ses obligations et suspendre l'exécution de toute décision qui se rapporte à la passation du contrat, sauf s'il estime, en considération de l'ensemble des intérêts susceptibles d'être lésés et notamment de l'intérêt public, que les conséquences négatives de ces mesures pourraient l'emporter sur leurs avantages./ Il peut, en outre, annuler les décisions qui se rapportent à la passation du contrat et supprimer les clauses ou prescriptions destinées à figurer dans le contrat et qui méconnaissent lesdites obligations (...)" ; qu'aux termes de l'article L. 551-10 du même code : " Les personnes habilitées à engager les recours prévus aux articles L. 551-1 et L. 551-5 sont celles qui ont un intérêt à conclure le contrat et qui sont susceptibles d'être lésées par le manquement invoqué (...) " ;<br/>
<br/>
              3. Considérant que, par un avis publié le 8 février 2016, la ville de Marseille a lancé une procédure formalisée d'appel d'offres ouvert pour l'attribution d'un marché de travaux ayant pour objet l'exploitation et le maintien de ses installations d'éclairage public ; que ce marché unique englobait divers types de travaux et prestations répartis selon quatre postes distincts, exploitation, entretien, études et maintien des installations d'éclairage ; que, par une ordonnance en date du 5 juillet 2016, le juge du référé précontractuel du tribunal administratif de Marseille, saisi par la société Travaux électriques du Midi (TEM), a annulé l'ensemble de la procédure de passation de ce marché, ainsi que la décision de la ville de Marseille attribuant le marché à la société SNEF et rejetant l'offre du groupement dont était membre la société TEM ; que la société SNEF, sous le n° 401660, et la ville de Marseille, sous le n° 401710, se pourvoient contre cette ordonnance ;<br/>
<br/>
              4. Considérant qu'il ressort des énonciations de l'ordonnance attaquée que le règlement de la consultation prévoyait que la note attribuée aux candidats sur le critère du prix reposait sur six prix correspondant aux quatre postes de prestations prévus au marché; qu'en vertu de l'article 6.2 du règlement pour deux des postes de prestations, la note attribuée aux candidats selon le critère du prix était fondée sur l'application au bordereau des prix unitaires (BPU) fourni par les candidats d'un " Détail Quantitatif Estimatif (DQE), dit "chantier masqué", non publié et non communiqué aux candidats. (...) Ces DQE "chantiers masqués" comportent des articles et des prestations du Bordereau des Prix Unitaires (BPU) affectés de quantités (...). A partir du BPU complété par lui, chaque candidat verra ses chantiers masqués reconstitués par l'Administration (...). Par contre l'Administration retiendra pour la notation du prix qu'un seul DQE "chantiers masqués" qui demeurera sous pli cacheté jusqu'à l'ouverture des plis. Ce dernier sera tiré au sort au moment de l'ouverture des plis par le Représentant Légal du Pouvoir Adjudicateur ou la personne ayant reçu le pouvoir de le représenter, parmi les deux DQE "chantiers masqués" préparés qui lui ont été remis sous pli cacheté conformément aux dispositions précédentes. Le pli non tiré au sort par le Pouvoir Adjudicateur le jour de l'ouverture des plis est conservé cacheté par l'Administration " ; <br/>
<br/>
              5. Considérant que le pouvoir adjudicateur définit librement la méthode de notation pour la mise en oeuvre de chacun des critères de sélection des offres qu'il a définis et rendus publics ; qu'en effectuant, pour évaluer le montant des offres qui lui sont présentées, une "simulation" consistant à multiplier les prix unitaires proposés par les candidats par le nombre d'interventions envisagées, un pouvoir adjudicateur n'a pas recours à un sous-critère, mais à une simple méthode de notation des offres destinée à les évaluer au regard du critère du prix ; qu'il n'est donc pas tenu d'informer les candidats, dans les documents de la consultation, qu'il aura recours à une telle méthode ; qu'il ne manque pas non plus à ses obligations de mise en concurrence en élaborant plusieurs commandes fictives et en tirant au sort, avant l'ouverture des plis, celle à partir de laquelle le critère du prix sera évalué, à la triple condition que les simulations correspondent toutes à l'objet du marché, que le choix du contenu de la simulation n'ait pas pour effet d'en privilégier un aspect particulier de telle sorte que le critère du prix s'en trouverait dénaturé et que le montant des offres proposées par chaque candidat soit reconstitué en recourant à la même simulation ; qu'en censurant le recours à une telle méthode de notation du critère du prix au seul motif que l'introduction du hasard dans la procédure de désignation du bénéficiaire du marché en litige, sous la forme de l'application des stipulations citées au point 4, avait nécessairement privé de leur portée les critères de sélection ou neutralisé leur pondération et induit, de ce fait, que la meilleure note ne soit pas nécessairement attribuée à la meilleure offre, ou, au regard de l'ensemble des critères pondérés que l'offre économiquement la plus avantageuse ne soit pas choisie, dès lors que le choix de l'attributaire ne résulte pas de l'analyse conduite par le pouvoir adjudicateur mais des résultats d'un tirage au sort aléatoire, le juge des référés a entaché son ordonnance d'erreur de droit ; <br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède, sans qu'il soit besoin d'examiner les autres moyens des pourvois, que la société SNEF et la commune de Marseille sont fondées à demander l'annulation de l'ordonnance attaquée ;<br/>
<br/>
              7. Considérant que, dans les circonstances de l'espèce, il y a lieu, en application de l'article L. 821-2 du code de justice administrative, de régler l'affaire au titre de la procédure de référé engagée ;<br/>
<br/>
              8. Considérant que la société TEM soutient qu'en procédant à des tirages au sort de DQE "chantiers masqués", ainsi qu'il a été dit ci-dessus, la commune a recouru à une méthode de notation du critère du prix qui, en méconnaissance des principes fondamentaux d'égalité de traitement des candidats et de transparence des procédures, est par elle-même de nature à priver de leur portée les critères de sélection ou à neutraliser leur pondération et est, de ce fait, susceptible de conduire, pour la mise en oeuvre de chaque critère, à ce que la meilleure note ne soit pas attribuée à la meilleure offre, ou, au regard de l'ensemble des critères pondérés, à ce que l'offre économiquement la plus avantageuse ne soit pas choisie ; que, toutefois, ainsi qu'il a été dit au point 5, le choix et l'utilisation d'une commande par tirage au sort réalisé avant l'ouverture des plis parmi plusieurs commandes fictives figurant sous pli cacheté pour valoriser les offres des candidats selon le critère du prix ne sont pas, par eux-mêmes, de nature à empêcher que l'offre économiquement la plus avantageuse soit choisie conformément aux dispositions de l'article 53 du code des marchés publics ; que, par suite, le moyen tiré de ce qu'en recourant à cette méthode de notation du critère du prix, la commune aurait manqué à ses obligations de publicité et de mise en concurrence ne peut qu'être écarté ;<br/>
<br/>
              9. Considérant, en deuxième lieu, que si la société TEM soutient que le pouvoir adjudicateur a méconnu son propre règlement de consultation s'agissant de la notation des offres, elle n'apporte aucun élément au soutien de cette affirmation ;<br/>
<br/>
              10. Considérant, en dernier lieu, que le courrier en date du 1er juin 2016, adressé par la ville de Marseille au groupement dont la société TEM était membre pour lui notifier le rejet de son offre, précisait le classement de celle-ci, les notes qui lui avait été attribuées ainsi que le nom de l'attributaire et les notes obtenues par ce dernier ; qu'il s'ensuit que le moyen tiré de la méconnaissance des articles 80 et 83 du code des marchés publics ne peut qu'être écarté ;<br/>
<br/>
              11. Considérant qu'il résulte de tout ce qui précède que la demande de la société TEM doit être rejetée, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ; qu'il y a lieu, en revanche, de mettre à sa charge, au même titre, le versement à la société SNEF et à la commune de Marseille de la somme de 4 500 euros chacune pour l'ensemble de la procédure ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du juge des référés du tribunal administratif de Marseille du 5 juillet 2016 est annulée.<br/>
Article 2 : Les demandes de la société TEM tendant à l'annulation de la procédure de passation du marché ayant pour objet l'exploitation et le maintien de ses installations d'éclairage public de la ville de Marseille ainsi que de la décision de la ville de Marseille rejetant l'offre du groupement dont était membre la société TEM et attribuant le marché à la société SNEF sont rejetées.<br/>
Article 3 : La société TEM versera une somme de 4 500 euros tant à la société SNEF qu'à la ville de Marseille au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions de la société TEM présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à la société TEM, à la société SNEF et à la ville de Marseille.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-02-005 MARCHÉS ET CONTRATS ADMINISTRATIFS. FORMATION DES CONTRATS ET MARCHÉS. FORMALITÉS DE PUBLICITÉ ET DE MISE EN CONCURRENCE. - MÉTHODE DE NOTATION DES OFFRES [RJ1] - EVALUATION DU PRIX - 1) FACULTÉ DE RECOURIR À UNE COMMANDE FICTIVE, CHOISIE PAR TIRAGE AUX SORTS PARMI PLUSIEURS COMMANDES FICTIVES ÉLABORÉES - EXISTENCE - CONDITIONS - 2) ESPÈCE.
</SCT>
<ANA ID="9A"> 39-02-005 1) Le pouvoir adjudicateur ne manque pas à ses obligations de mise en concurrence en élaborant plusieurs commandes fictives et en tirant au sort, avant l'ouverture des plis, celle à partir de laquelle le critère du prix sera évalué, à la triple condition que les simulations correspondent toutes à l'objet du marché, que le choix du contenu de la simulation n'ait pas pour effet d'en privilégier un aspect particulier de telle sorte que le critère du prix s'en trouverait dénaturé et que le montant des offres proposées par chaque candidat soit reconstitué en recourant à la même simulation.,,,2) Société requérante soutenant qu'en procédant à des tirages au sort de chantiers masqués, la commune aurait recouru à une méthode de notation du critère du prix qui, en méconnaissance des principes fondamentaux d'égalité de traitement des candidats et de transparence des procédures, serait par elle-même de nature à priver de leur portée les critères de sélection ou à neutraliser leur pondération et serait, de ce fait, susceptible de conduire, pour la mise en oeuvre de chaque critère, à ce que la meilleure note ne soit pas attribuée à la meilleure offre, ou, au regard de l'ensemble des critères pondérés, à ce que l'offre économiquement la plus avantageuse ne soit pas choisie.,,,Toutefois, le choix et l'utilisation d'une commande par tirage au sort réalisé avant l'ouverture des plis parmi plusieurs commandes fictives figurant sous pli cacheté pour valoriser les offres des candidats selon le critère du prix ne sont pas, par eux-mêmes, de nature à empêcher que l'offre économiquement la plus avantageuse soit choisie conformément aux dispositions de l'article 53 du code des marchés publics.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Rappr. CE, 2 août 2011, Syndicat mixte de la vallée de l'Orge aval, n° 348711, T. p. 1006.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
