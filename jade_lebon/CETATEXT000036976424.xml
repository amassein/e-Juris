<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036976424</ID>
<ANCIEN_ID>JG_L_2018_05_000000410172</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/97/64/CETATEXT000036976424.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 4ème chambres réunies, 30/05/2018, 410172</TITRE>
<DATE_DEC>2018-05-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>410172</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 4ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS ; SCP DELAMARRE, JEHANNIN</AVOCATS>
<RAPPORTEUR>Mme Florence Marguerite</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:410172.20180530</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Mme A...D...B...a demandé au tribunal administratif de Versailles d'annuler la décision implicite par laquelle le président du conseil général des Yvelines a rejeté son recours dirigé contre la décision de la caisse d'allocations familiales des Yvelines du 9 décembre 2013 mettant à sa charge un indu de revenu de solidarité active d'un montant de 21 734,05 euros pour la période de janvier 2010 à juillet 2013 ainsi qu'un indu d'allocation de logement pour un montant de 6 495,31 euros. Par un jugement n° 1401397 du 28 février 2017, le tribunal administratif de Versailles a rejeté cette demande.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 28 avril et 20 juillet 2017 et le 14 mai 2018 au secrétariat du contentieux du Conseil d'Etat, Mme B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande en ce qui concerne l'indu de revenu de solidarité active ;<br/>
<br/>
              3°) de mettre à la charge du département des Yvelines et de la caisse d'allocations familiales des Yvelines la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'action sociale et des familles ;<br/>
              - le code de la sécurité sociale ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Florence Marguerite, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de Mme B...et à la SCP Delamarre, Jéhannin, avocat du département des Yvelines.<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond qu'à compter de décembre 2008, Mme B...a été bénéficiaire du revenu minimum d'insertion puis du revenu de solidarité active. A l'issue d'un contrôle de sa situation, la caisse d'allocations familiales des Yvelines lui a réclamé, par une décision du 9 décembre 2013, un indu d'allocation de revenu de solidarité active d'un montant de 21 734, 05 euros, pour la période du 1er janvier 2010 au 30 juin 2013, ainsi qu'un indu d'allocation de logement d'un montant de 6 495,31 euros, pour la période de janvier 2012 à novembre 2013. Mme B...a demandé au tribunal administratif de Versailles l'annulation de la décision par laquelle le président du conseil départemental des Yvelines a rejeté son recours administratif préalable formé contre la décision du 9 décembre 2013. Elle se pourvoit en cassation contre le jugement du 28 février 2017 par lequel le tribunal administratif de Versailles a rejeté sa demande. <br/>
<br/>
              Sur les conclusions de Mme B...tendant à l'annulation du jugement attaqué en tant qu'il statue sur l'indu d'allocation de logement :<br/>
<br/>
              2. Aux termes de l'article R. 351-5-1 du code de justice administrative : " Lorsque le Conseil d'Etat est saisi de conclusions se rapportant à un litige qui ne relève pas de la compétence de la juridiction administrative, il est compétent, nonobstant les règles relatives aux voies de recours et à la répartition des compétences entre les juridictions administratives, pour se prononcer sur ces conclusions et décliner la compétence de la juridiction administrative ".<br/>
<br/>
              3. Aux termes de l'article L. 142-1 du code de la sécurité sociale : " Il est institué une organisation du contentieux général de la sécurité sociale. / Cette organisation règle les différends auxquels donnent lieu l'application des législations et réglementations de sécurité sociale et de mutualité sociale agricole, et qui ne relèvent pas, par leur nature, d'un autre contentieux, ainsi que le recouvrement mentionné au 5° de l'article L. 213-1 ". Selon l'article L. 142-2 du même code : " Le tribunal des affaires de sécurité sociale connaît en première instance des litiges relevant du contentieux général de la sécurité sociale (...) ".<br/>
<br/>
              4. Enfin, d'une part, il résulte de l'article L. 511-1 du code de la sécurité sociale que l'allocation de logement à caractère familial prévue par l'article L. 542-1 de ce code a le caractère d'une prestation familiale. D'autre part, il résulte des dispositions des articles L. 835-1, L. 835-3 et L. 835-4 du même code que les différends relatifs au recouvrement d'un trop-perçu de l'allocation de logement à caractère social prévue par l'article L. 831-2 de ce code " sont réglés conformément aux dispositions concernant le contentieux général de la sécurité sociale ".<br/>
<br/>
              5. Mme B...a demandé au tribunal administratif de Versailles d'annuler la décision de récupération d'indus d'allocation de logement à caractère familial pour la période de janvier à décembre 2012 et d'allocation de logement à caractère social pour la période de janvier à novembre 2013. Il résulte des dispositions citées ci-dessus qu'il n'appartient qu'à la juridiction du contentieux général de la sécurité sociale - et à ce titre, en première instance, au tribunal des affaires de sécurité sociale - de connaître de telles conclusions. Par suite, ses conclusions tendant à l'annulation du jugement dans cette mesure se rapportent à un litige qui, ainsi que l'a jugé le tribunal administratif de Versailles, ne relève pas de la compétence de la juridiction administrative. Elles ne peuvent, dès lors, qu'être rejetées.<br/>
<br/>
              Sur les conclusions de Mme B...tendant à l'annulation du jugement attaqué en tant qu'il statue sur l'indu d'allocation de revenu de solidarité active :<br/>
<br/>
              6. Aux termes de l'article R. 772-5 du code de justice administrative : " Sont présentées, instruites et jugées selon les dispositions du présent code, sous réserve des dispositions du présent chapitre, les requêtes relatives aux prestations, allocations ou droits attribués au titre de l'aide ou de l'action sociale, du logement ou en faveur des travailleurs privés d'emploi (...) ". Aux termes de l'article R. 772-9 du même code : " La procédure contradictoire peut être poursuivie à l'audience sur les éléments de fait qui conditionnent l'attribution de la prestation ou de l'allocation ou la reconnaissance du droit, objet de la requête. / L'instruction est close soit après que les parties ou leurs mandataires ont formulé leurs observations orales, soit, si ces parties sont absentes ou ne sont pas représentées, après appel de leur affaire à l'audience. Toutefois, afin de permettre aux parties de verser des pièces complémentaires, le juge peut décider de différer la clôture de l'instruction à une date postérieure dont il les avise par tous moyens. / L'instruction fait l'objet d'une réouverture en cas de renvoi à une autre audience ". <br/>
<br/>
              7. Il résulte de ces dispositions que le tribunal doit, pour juger les requêtes régies par ces articles, prendre en considération tant les éléments de fait invoqués oralement à l'audience qui conditionnent l'attribution de la prestation ou de l'allocation ou la reconnaissance du droit, objet de la requête, que tous les mémoires enregistrés jusqu'à la clôture de l'instruction, qui intervient, sous réserve de la décision du juge de la différer, après que les parties ou leurs mandataires ont formulé leurs observations orales ou, si ces parties sont absentes ou ne sont pas représentées, après appel de leur affaire à l'audience. Lorsque le tribunal décide de différer la clôture de l'instruction pour permettre aux parties de verser des pièces complémentaires, sur lesquelles seules le débat contradictoire peut alors utilement se poursuivre, il doit en informer les parties et, avant de statuer, leur indiquer la date ainsi que, le cas échéant, l'heure à laquelle l'instruction sera close. Il ne saurait par suite, sans irrégularité, rendre son jugement tant que l'instruction est ouverte, une telle irrégularité pouvant être utilement invoquée par toute partie au litige.<br/>
<br/>
              8. Il ressort des pièces du dossier soumis aux juges du fond que, le 26 janvier 2017, Mme B...a présenté des observations lors de l'audience devant le tribunal administratif et produit un nouveau mémoire, auquel étaient jointes de nouvelles pièces, qui a été communiqué après l'audience au département des Yvelines et à la caisse d'allocations familiales de ce département, avec l'indication que les défendeurs pouvaient produire des observations aussi rapidement que possible. Le tribunal, qui, en l'espèce, doit être regardé comme ayant différé l'instruction, a ensuite statué sans avoir informé les parties de la date à laquelle l'instruction était close. Par suite, il résulte de ce qui a été dit ci-dessus que Mme B...est fondée à soutenir que le jugement est intervenu à la suite d'une procédure irrégulière.<br/>
<br/>
              9. Il résulte de ce qui précède que Mme B...est fondée à demander l'annulation du jugement qu'elle attaque en tant qu'il statue sur l'indu de revenu de solidarité active. Le moyen retenu suffisant à entraîner cette annulation, il n'est pas nécessaire de se prononcer sur les autres moyens du pourvoi.<br/>
<br/>
              Sur les frais liés au litige :<br/>
<br/>
              10. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du département des Yvelines la somme de 3 000 euros à verser à Mme B...au titre de l'article L. 761-1 du code de justice administrative. Les dispositions de l'article L. 761-1 du code de justice administrative font, en revanche, obstacle à ce qu'une somme soit mise, à ce titre, à la charge de MmeC..., qui n'est pas la partie perdante dans la présente instance. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
                         --------------<br/>
Article 1er : Le jugement du 28 février 2017 du tribunal administratif de Versailles est annulé en tant qu'il statue sur les conclusions de Mme B...relatives à l'indu de revenu de solidarité active.<br/>
Article 2 : L'affaire est renvoyée dans cette mesure au tribunal administratif de Versailles.<br/>
Article 3 : Le département des Yvelines versera à Mme B...une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Le surplus des conclusions de Mme B...et les conclusions présentées par le département des Yvelines au titre de l'article L. 761-1 du code de justice administrative sont rejetés.<br/>
Article 5 : La présente décision sera notifiée à Mme A... D...B...et au département des Yvelines. <br/>
Copie en sera adressée à la caisse d'allocations familiales des Yvelines. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">04-04 AIDE SOCIALE. CONTENTIEUX DE L'AIDE SOCIALE ET DE LA TARIFICATION. - DÉCISION DU JUGE DE DIFFÉRER LA CLÔTURE DE L'INSTRUCTION POUR PERMETTRE LE VERSEMENT DE PIÈCES COMPLÉMENTAIRES - MODALITÉS - INDICATION AUX PARTIES DE LA DATE ET DE L'HEURE DE LA CLÔTURE DE L'INSTRUCTION - CONSÉQUENCE - IRRÉGULARITÉ DU JUGEMENT INTERVENU ALORS QUE L'INSTRUCTION EST OUVERTE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-06-01 PROCÉDURE. JUGEMENTS. RÈGLES GÉNÉRALES DE PROCÉDURE. - PROCÉDURE APPLICABLE DANS LES CONTENTIEUX SOCIAUX - DÉCISION DU JUGE DE DIFFÉRER LA CLÔTURE DE L'INSTRUCTION POUR PERMETTRE LE VERSEMENT DE PIÈCES COMPLÉMENTAIRES - MODALITÉS - INDICATION AUX PARTIES DE LA DATE ET DE L'HEURE DE LA CLÔTURE DE L'INSTRUCTION - CONSÉQUENCE - IRRÉGULARITÉ DU JUGEMENT INTERVENU ALORS QUE L'INSTRUCTION EST OUVERTE.
</SCT>
<ANA ID="9A"> 04-04 Il résulte des articles R. 772-5 et R. 772-9 du code de justice administrative (CJA) que le tribunal doit, pour juger les requêtes régies par ces articles, prendre en considération tant les éléments de fait invoqués oralement à l'audience qui conditionnent l'attribution de la prestation ou de l'allocation ou la reconnaissance du droit, objet de la requête, que tous les mémoires enregistrés jusqu'à la clôture de l'instruction qui intervient, sous réserve de la décision du juge de la différer, après que les parties ou leurs mandataires ont formulé leurs observations orales ou, si ces parties sont absentes ou ne sont pas représentées, après appel de leur affaire à l'audience. Lorsque le tribunal décide de différer la clôture de l'instruction pour permettre aux parties de verser des pièces complémentaires, sur lesquelles seules le débat contradictoire peut alors utilement se poursuivre, il doit en informer les parties et, avant de statuer, leur indiquer la date ainsi que, le cas échéant, l'heure à laquelle l'instruction sera close. Il ne saurait par suite, sans irrégularité, rendre son jugement tant que l'instruction est ouverte, une telle irrégularité pouvant être utilement invoquées par toute partie au litige.</ANA>
<ANA ID="9B"> 54-06-01 Il résulte des articles R. 772-5 et R. 772-9 du code de justice administrative (CJA) que le tribunal doit, pour juger les requêtes régies par ces articles, prendre en considération tant les éléments de fait invoqués oralement à l'audience qui conditionnent l'attribution de la prestation ou de l'allocation ou la reconnaissance du droit, objet de la requête, que tous les mémoires enregistrés jusqu'à la clôture de l'instruction qui intervient, sous réserve de la décision du juge de la différer, après que les parties ou leurs mandataires ont formulé leurs observations orales ou, si ces parties sont absentes ou ne sont pas représentées, après appel de leur affaire à l'audience. Lorsque le tribunal décide de différer la clôture de l'instruction pour permettre aux parties de verser des pièces complémentaires, sur lesquelles seules le débat contradictoire peut alors utilement se poursuivre, il doit en informer les parties et, avant de statuer, leur indiquer la date ainsi que, le cas échéant, l'heure à laquelle l'instruction sera close. Il ne saurait par suite, sans irrégularité, rendre son jugement tant que l'instruction est ouverte, une telle irrégularité pouvant être utilement invoquées par toute partie au litige.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
