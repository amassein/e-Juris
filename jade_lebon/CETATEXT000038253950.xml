<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038253950</ID>
<ANCIEN_ID>JG_L_2019_03_000000408658</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/25/39/CETATEXT000038253950.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème et 1ère chambres réunies, 20/03/2019, 408658</TITRE>
<DATE_DEC>2019-03-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>408658</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème et 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FABIANI, LUC-THALER, PINATEL ; SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>Mme Sara-Lou Gerber</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:408658.20190320</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Montreuil d'annuler pour excès de pouvoir la décision du 8 novembre 2013 par laquelle le ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social a autorisé la Société de Fret et de Services (SFS) à le licencier. Par un jugement n° 1400050 du 9 février 2015, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 15VE01277 du 29 décembre 2016, la cour administrative d'appel de Versailles a rejeté l'appel formé par M. A...contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire, un mémoire en réplique et un nouveau mémoire, enregistrés les 6 mars et 6 juin 2017 et les 13 juin et 31 octobre 2018 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de la société SFS la somme de 2 800 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code du travail ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sara-Lou Gerber,  auditeur,  <br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de M. A...et à la SCP Fabiani, Luc-Thaler, Pinatel, avocat de la Société de fret et de services ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par une décision du 8 novembre 2013, le ministre chargé du travail a autorisé la Société de Fret et de Services (SFS) à licencier pour faute M.A..., salarié protégé. M. A...se pourvoit en cassation contre l'arrêt du 29 décembre 2016 par lequel la cour administrative d'appel de Versailles a rejeté son appel dirigé contre le jugement du 9 février 2015 par lequel le tribunal administratif de Montreuil a rejeté sa demande d'annulation de l'autorisation du ministre.<br/>
<br/>
              2. Il ressort des termes mêmes de l'arrêt attaqué que, pour juger qu'il n'était pas établi que le licenciement de M. A...était en rapport avec ses mandats, la cour administrative d'appel de Versailles s'est fondée sur ce que, s'il soutenait que l'employeur avait refusé de lui payer ses heures de délégation, le requérant ne mettait pas la cour à même de connaître le contexte du litige. Toutefois, M. A...analysait dans ses écritures et produisait devant la cour administrative d'appel un arrêt du 7 mai 2015 de la cour d'appel de Paris, statuant en référé, jugeant, notamment, d'une part, que la société SFS ne lui avait pas payé les heures de délégation qui lui étaient dues à raison de son mandat au comité d'hygiène, de sécurité et des conditions de travail, pour la période du 1er janvier au 31 octobre 2010 et à raison de son mandat de représentant syndical au comité d'entreprise d'avril à septembre 2013 et qu'elle devait, par suite, être condamnée à lui verser par provision la somme de 3 009,43 euros et, d'autre part, que ces manquements de la société à ses obligations d'employeur laissaient supposer l'existence d'une discrimination syndicale à l'encontre de M.A..., justifiant qu'une somme provisionnelle de 1 000 euros lui soit accordée à titre de dommages et intérêts. Dans ces conditions, M. A...est fondé à soutenir que, faute, pour se prononcer sur l'existence d'un lien entre son licenciement et ses mandats, d'avoir expressément pris position sur ces éléments, la cour administrative d'appel de Versailles a entaché son arrêt d'insuffisance de motivation. Par suite, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, M. A...est fondé à demander l'annulation de l'arrêt qu'il attaque. <br/>
<br/>
              3. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              4. Aux termes de l'article R. 411-1 du code de justice administrative : " La juridiction est saisie par requête. La requête (...) contient l'exposé des faits et moyens, ainsi que l'énoncé des conclusions soumises au juge ". La requête d'appel présentée par M. A...satisfaisant à ces exigences, la fin de non-recevoir soulevée par la Société de Fret et de Services doit être écartée. <br/>
<br/>
              5. En vertu des dispositions du code du travail, le licenciement des représentants du personnel, qui bénéficient dans l'intérêt de l'ensemble des travailleurs qu'ils représentent d'une protection exceptionnelle, ne peut intervenir que sur autorisation de l'inspecteur du travail dont dépend l'établissement. Lorsque le licenciement d'un de ces salariés est envisagé, il ne doit pas être en rapport avec les fonctions représentatives normalement exercées ou l'appartenance syndicale de l'intéressé. Dans le cas où la demande est motivée par un comportement fautif, il appartient à l'autorité compétente de rechercher, sous le contrôle du juge de l'excès de pouvoir, si les faits reprochés au salarié sont d'une gravité suffisante pour justifier son licenciement, compte tenu de l'ensemble des règles applicables au contrat de travail de l'intéressé et des exigences propres à l'exécution normale du mandat dont il est investi.<br/>
<br/>
              6. A l'appui de sa demande d'autorisation de licencier M. A... en date du 26 mars 2013, la société SFS invoquait la faute qu'il aurait commise en parcourant plusieurs centaines de kilomètres en utilisant un véhicule de service emprunté sans autorisation. Toutefois, il ressort des pièces du dossier, ainsi que l'a notamment relevé la cour d'appel de Paris dans son arrêt du 7 mai 2015 mentionné au point 2 ci-dessus, que la société SFS a, à plusieurs reprises, notamment en 2013, refusé de payer des heures de délégation à M. A... au titre de ses différents mandats. Ainsi qu'il a déjà été dit, ces manquements, ajoutés à d'autres, également contemporains de la demande de licenciement adressée à l'inspection du travail, ont conduit le juge judiciaire à condamner l'employeur à payer une provision à M. A...en relevant qu'ils laissaient supposer l'existence d'une discrimination syndicale à son encontre. Dans ces conditions, compte tenu de l'absence d'éléments justificatifs précis présentés en défense par la société SFS, la demande d'autorisation de licenciement présentée par cette société doit être regardée comme n'étant pas sans rapport avec les mandats détenus par l'intéressé. Par suite, le ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social ne pouvait légalement faire droit à la demande d'autorisation de licencier M.A.... <br/>
<br/>
              7. Il résulte de tout ce qui précède que, sans qu'il soit besoin d'examiner les autres moyens de sa requête, M. A...est fondé à soutenir que c'est à tort que, par son jugement du 9 février 2015, le tribunal administratif de Montreuil a rejeté sa demande d'annulation de la décision du 8 novembre 2013 par laquelle le ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social a autorisé la société SFS à le licencier. <br/>
<br/>
              8. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société SFS une somme de 3 500 euros à verser à M. A...au titre des frais exposés par lui devant le tribunal administratif de Montreuil, la cour administrative d'appel de Paris et le Conseil d'Etat et non compris dans les dépens. En revanche, les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de M. A..., qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Versailles du 29 décembre 2016, le jugement du 9 février 2015 du tribunal administratif de Montreuil et la décision du 8 novembre 2013 du ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social sont annulés.<br/>
Article 2 : La Société de Fret et de Services versera à M. A...une somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : Les conclusions de la Société de Fret et de Services présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à M. B...A..., à la Société de Fret et de services et à la ministre du travail.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">66-07-01-04-02 TRAVAIL ET EMPLOI. LICENCIEMENTS. AUTORISATION ADMINISTRATIVE - SALARIÉS PROTÉGÉS. CONDITIONS DE FOND DE L'AUTORISATION OU DU REFUS D'AUTORISATION. LICENCIEMENT POUR FAUTE. - APPRÉCIATION, PAR LE JUGE, DE L'ABSENCE DE RAPPORT ENTRE LE LICENCIEMENT ET LES MANDATS DÉTENUS PAR L'INTÉRESSÉ - ESPÈCE - PRISE EN COMPTE D'UNE DÉCISION DU JUGE JUDICIAIRE RELEVANT DES MANQUEMENTS DE L'EMPLOYEUR LAISSANT SUPPOSER L'EXISTENCE D'UNE DISCRIMINATION SYNDICALE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">66-07-01-05 TRAVAIL ET EMPLOI. LICENCIEMENTS. AUTORISATION ADMINISTRATIVE - SALARIÉS PROTÉGÉS. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. - RECOURS CONTRE LES DÉCISIONS DE L'INSPECTEUR DU TRAVAIL ET DU MINISTRE AUTORISANT LE LICENCIEMENT D'UN SALARIÉ PROTÉGÉ - APPRÉCIATION, PAR LE JUGE, DE L'ABSENCE DE RAPPORT ENTRE LE LICENCIEMENT ET LES MANDATS DÉTENUS PAR L'INTÉRESSÉ - ESPÈCE - DÉCISION DU JUGE JUDICIAIRE RELEVANT DES MANQUEMENTS DE L'EMPLOYEUR LAISSANT SUPPOSER L'EXISTENCE D'UNE DISCRIMINATION SYNDICALE.
</SCT>
<ANA ID="9A"> 66-07-01-04-02 A l'appui de sa demande d'autorisation de licencier le requérant en date du 26 mars 2013, l'employeur invoquait la faute qu'il aurait commise en parcourant plusieurs centaines de kilomètres en utilisant un véhicule de service emprunté sans autorisation. Toutefois, il ressort des pièces du dossier, ainsi que l'a notamment relevé la cour d'appel de Paris dans son arrêt du 7 mai 2015, que l'employeur a, à plusieurs reprises, notamment en 2013, refusé de payer des heures de délégation au requérant au titre de ses différents mandats. Ces manquements, ajoutés à d'autres, également contemporains de la demande de licenciement adressée à l'inspection du travail, ont conduit le juge judiciaire à condamner l'employeur à payer une provision au requérant en relevant qu'ils laissaient supposer l'existence d'une discrimination syndicale à son encontre. Dans ces conditions, compte tenu de l'absence d'éléments justificatifs précis présentés en défense par l'employeur, la demande d'autorisation de licenciement présentée par cette société doit être regardée comme n'étant pas sans rapport avec les mandats détenus par l'intéressé. Par suite, le ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social ne pouvait légalement faire droit à la demande d'autorisation de licencier le requérant. Est, à cet égard, sans incidence la circonstance que la faute commise par l'intéressé aurait revêtu une gravité de nature à justifier son licenciement.</ANA>
<ANA ID="9B"> 66-07-01-05 A l'appui de sa demande d'autorisation de licencier le requérant en date du 26 mars 2013, l'employeur invoquait la faute qu'il aurait commise en parcourant plusieurs centaines de kilomètres en utilisant un véhicule de service emprunté sans autorisation. Toutefois, il ressort des pièces du dossier, ainsi que l'a notamment relevé la cour d'appel de Paris dans son arrêt du 7 mai 2015, que l'employeur a, à plusieurs reprises, notamment en 2013, refusé de payer des heures de délégation au requérant au titre de ses différents mandats. Ces manquements, ajoutés à d'autres, également contemporains de la demande de licenciement adressée à l'inspection du travail, ont conduit le juge judiciaire à condamner l'employeur à payer une provision au requérant en relevant qu'ils laissaient supposer l'existence d'une discrimination syndicale à son encontre. Dans ces conditions, compte tenu de l'absence d'éléments justificatifs précis présentés en défense par l'employeur, la demande d'autorisation de licenciement présentée par cette société doit être regardée comme n'étant pas sans rapport avec les mandats détenus par l'intéressé. Par suite, le ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social ne pouvait légalement faire droit à la demande d'autorisation de licencier le requérant. Est, à cet égard, sans incidence la circonstance que la faute commise par l'intéressé aurait revêtu une gravité de nature à justifier son licenciement.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
