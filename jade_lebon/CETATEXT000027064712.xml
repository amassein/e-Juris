<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027064712</ID>
<ANCIEN_ID>JG_L_2013_02_000000338695</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/06/47/CETATEXT000027064712.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 13/02/2013, 338695</TITRE>
<DATE_DEC>2013-02-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>338695</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Jean Lessi</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:338695.20130213</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 16 avril 2010 au secrétariat du contentieux du Conseil d'Etat, présenté par le ministre du travail, de la solidarité et de la fonction publique ; le ministre demande au Conseil d'Etat d'annuler les articles 1er et 2 de l'arrêt n° 09PA00746 du 18 février 2010 par lesquels la cour administrative d'appel de Paris a, d'une part, annulé le jugement n° 0420572 du tribunal administratif de Paris du 10 décembre 2008 en tant que ce dernier avait rejeté la demande de la Société 1 Pacte International tendant à l'annulation de la décision du préfet de la région Ile-de-France du 13 juillet 2004 lui ordonnant de verser une somme de 50 156,49 euros au Trésor public et, d'autre part, annulé, dans cette même mesure, la décision préfectorale et déchargé la société de cette somme ;<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu le code du travail ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean Lessi, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 920-9 du code du travail, dans sa rédaction applicable en l'espèce, et dont la cour administrative d'appel de Paris a pu faire à bon droit application à la date de l'arrêt attaqué du 18 février 2010 par lequel elle s'est elle-même prononcée sur cette sanction : " Faute de réalisation totale ou partielle d'une prestation de formation, l'organisme prestataire doit rembourser au cocontractant les sommes indûment perçues de ce fait. / En cas de manoeuvres frauduleuses relatives à l'exécution d'une prestation de formation, le ou les contractants sont assujettis à un versement d'égal montant de cette prestation au profit du Trésor " ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis au juge du fond que le préfet de la région Ile-de-France a, sur le fondement des dispositions citées ci-dessus du code du travail, prononcé à l'encontre de la SARL 1 Pacte International une sanction de versement au Trésor public d'une somme de 50 156,49 euros, au motif que cette société avait systématiquement établi des attestations de présence ne correspondant pas à l'assiduité réelle des stagiaires dont elle assurait la formation et qu'elle avait, sur la base de ces fausses attestations sciemment établies, facturé à ses cocontractants un nombre d'heures de formation excédant les prestations réellement exécutées ; <br/>
<br/>
              3. Considérant que, pour annuler la sanction infligée par le préfet de la région Ile-de-France à la SARL 1 Pacte International, la cour administrative d'appel de Paris a jugé que l'existence de deux cas seulement d'attestations inexactes délibérément établies par cette société n'était pas de nature à justifier l'application du deuxième alinéa de l'article L. 920-9 du code du travail ; <br/>
<br/>
              4. Considérant cependant que s'il appartenait à la cour administrative d'appel, sur le fondement de l'appréciation selon laquelle une partie seulement des prestations de formation était entachée de fraude, de réformer la sanction attaquée pour en limiter le montant à celui des seules facturations frauduleuses, elle ne pouvait en revanche, sans erreur de droit, en prononcer pour ce seul motif l'annulation totale ; que, par suite, sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, le ministre du travail, de la solidarité et de la fonction publique est fondé à demander l'annulation des articles 1er et 2 de l'arrêt qu'il attaque ; <br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler, dans cette même mesure, l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ; <br/>
<br/>
              6. Considérant qu'il résulte de l'instruction, notamment des mentions portées sur les feuilles d'émargement remplies lors des séances de formation, dont il n'est d'ailleurs pas soutenu qu'elles seraient erronées ou incomplètes, ainsi que d'éléments issus en particulier de deux dossiers de stagiaires, que la SARL 1 Pacte international a produit, à l'appui des factures qu'elle adressait à ses cocontractants et pour un total de facturation de 46 789,29 euros, des attestations de présence correspondant en réalité à des heures de formation non effectuées ; qu'eu égard au procédé utilisé et à sa réitération au cours de toute la période sur laquelle portait le contrôle des services de l'Etat, cette pratique doit être regardée comme délibérée ; qu'elle a par suite, et sur l'intégralité des sommes indûment perçues, revêtu le caractère d'une manoeuvre frauduleuse ; que la SARL 1 Pacte International n'est, dès lors, pas fondée à contester la mise à sa charge d'un versement au Trésor public d'une somme de 46 789,29 euros en application des dispositions précédemment citées de l'article L. 920-9 du code du travail ;<br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que la SARL 1 Pacte International n'est pas fondée à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Paris s'est borné à réformer la décision du 13 juillet 2004 du préfet de la région Ile-de-France en ramenant de 50 156,49 euros à 46 789,29 euros le montant du versement au Trésor Public mis à sa charge ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les articles 1er et 2 de l'arrêt du 18 février 2010 de la cour administrative d'appel de Paris sont annulés.<br/>
Article 2 : Les conclusions de la SARL 1 Pacte International présentées devant la cour administrative d'appel de Paris tendant à l'annulation du jugement du 10 décembre 2008 du tribunal administratif de Paris en tant qu'il se borne à réduire à 46 789,29 euros le montant du versement au Trésor Public mis à sa charge, sont rejetées.<br/>
Article 3 : La présente décision sera notifiée au ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social et à MeA..., mandataire liquidateur judiciaire de la SARL 1Pacte international.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">59-02-02-03 RÉPRESSION. DOMAINE DE LA RÉPRESSION ADMINISTRATIVE RÉGIME DE LA SANCTION ADMINISTRATIVE. BIEN-FONDÉ. - SANCTION PRÉVUE EN CAS DE NON RÉALISATION D'UNE PRESTATION DE FORMATION (ART. L. 920-9 DU CODE DU TRAVAIL) - HYPOTHÈSE OÙ UNE PARTIE SEULEMENT DES PRESTATIONS EST ENTACHÉE DE FRAUDE - OFFICE DU JUGE - RÉFORMATION À DUE CONCURRENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">66-09 TRAVAIL ET EMPLOI. FORMATION PROFESSIONNELLE. - SANCTION PRÉVUE EN CAS DE NON RÉALISATION D'UNE PRESTATION DE FORMATION (ART. L. 920-9 DU CODE DU TRAVAIL) - HYPOTHÈSE OÙ UNE PARTIE SEULEMENT DES PRESTATIONS EST ENTACHÉE DE FRAUDE - OFFICE DU JUGE - RÉFORMATION À DUE CONCURRENCE.
</SCT>
<ANA ID="9A"> 59-02-02-03 Article L. 920-9 du code du travail, dans sa rédaction en vigueur avant le 1er juillet 2005, prévoyant que faute de réalisation totale ou partielle d'une prestation de formation, l'organisme prestataire doit rembourser au cocontractant les sommes indûment perçues de ce fait et qu'en outre, en cas de manoeuvres frauduleuses relatives à l'exécution d'une prestation de formation, le ou les contractants sont assujettis à une sanction d'un montant égal à celui de cette prestation à verser au profit du Trésor.,,Dans l'hypothèse où le juge constate qu'une partie seulement des prestations de formation ayant donné lieu à sanction était entachée de fraude, il lui appartient de réformer cette sanction pour en limiter le montant à celui des seules facturations frauduleuses, et non d'en prononcer l'annulation totale.</ANA>
<ANA ID="9B"> 66-09 Article L. 920-9 du code du travail, dans sa rédaction en vigueur avant le 1er juillet 2005, prévoyant que faute de réalisation totale ou partielle d'une prestation de formation, l'organisme prestataire doit rembourser au cocontractant les sommes indûment perçues de ce fait et qu'en outre, en cas de manoeuvres frauduleuses relatives à l'exécution d'une prestation de formation, le ou les contractants sont assujettis à une sanction d'un montant égal à celui de cette prestation à verser au profit du Trésor.,,Dans l'hypothèse où le juge constate qu'une partie seulement des prestations de formation ayant donné lieu à sanction était entachée de fraude, il lui appartient de réformer cette sanction pour en limiter le montant à celui des seules facturations frauduleuses, et non d'en prononcer l'annulation totale.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
