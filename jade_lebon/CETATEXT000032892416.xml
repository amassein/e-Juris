<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032892416</ID>
<ANCIEN_ID>JG_L_2016_07_000000387763</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/89/24/CETATEXT000032892416.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Assemblée, 13/07/2016, 387763, Publié au recueil Lebon</TITRE>
<DATE_DEC>2016-07-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>387763</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Assemblée</FORMATION>
<TYPE_REC>Contentieux des pensions</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP POTIER DE LA VARDE, BUK LAMENT</AVOCATS>
<RAPPORTEUR>M. Vincent Montrieux</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier Henrard</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CEASS:2016:387763.20160713</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Lille, d'une part, d'annuler l'arrêté du 24 juin 1991 du ministre de l'économie et des finances lui concédant une pension de retraite, en tant que cet arrêté ne prend pas en compte la bonification pour enfants prévue par les dispositions du b) de l'article L. 12 du code des pensions civiles et militaires de retraite, d'autre part, d'enjoindre au ministre de l'économie et des finances de procéder à une nouvelle liquidation de sa pension prenant en compte cette bonification. Par une ordonnance n° 1408180 du 2 décembre 2014, le tribunal administratif de Lille a rejeté sa demande. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 6 février et 6 mai 2015 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) réglant l'affaire au fond, d'annuler l'arrêté du ministre de l'économie et des finances du 24 juin 1991, d'enjoindre au ministre des finances et des comptes publics de modifier dans le délai de deux mois suivant la notification de l'arrêt du Conseil d'Etat les conditions dans lesquelles sa pension lui a été concédée, de revaloriser rétroactivement cette pension à compter du 1er janvier 2010 , d'assortir les sommes dues des intérêts au taux légal à compter du 19 novembre 2014 et d'ordonner la capitalisation ultérieure de ceux-ci ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code des pensions civiles et militaires de retraite ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Vincent Montrieux, maître des requêtes en service extraordinaire,  <br/>
              - les conclusions de M. Olivier Henrard, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Potier de la Varde, Buk Lament, avocat de M. B...;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article R. 104 du code des tribunaux administratifs et des cours administratives d'appel, en vigueur à la date de la décision contestée devant le juge du fond et dont les dispositions sont désormais reprises à l'article R. 421-5 du code de justice administrative : " Les délais de recours contre une décision administrative ne sont opposables qu'à la condition d'avoir été mentionnés, ainsi que les voies de recours, dans la notification de la décision. " ; qu'il résulte de ces dispositions que cette notification doit, s'agissant des voies de recours, mentionner, le cas échéant, l'existence d'un recours administratif préalable obligatoire ainsi que l'autorité devant laquelle il doit être porté ou, dans l'hypothèse d'un recours contentieux direct, indiquer si celui-ci doit être formé auprès de la juridiction administrative de droit commun ou devant une juridiction spécialisée et, dans ce dernier cas, préciser laquelle ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis au juge du fond que M.B..., ancien brigadier de police, a reçu le 26 septembre 1991 notification de l'arrêté du 24 juin 1991 lui concédant une pension de retraite, ainsi que l'atteste le procès-verbal de remise de son livret de pension ; que cette notification mentionnait le délai de recours contentieux dont l'intéressé disposait à l'encontre de cet arrêté mais ne contenait aucune indication sur la juridiction compétente ; qu'ainsi, en jugeant que cette notification comportait l'indication des voies et délais de recours conformément aux dispositions de l'article R. 421-5 citées ci-dessus, le tribunal administratif de Lille a dénaturé les pièces du dossier ; que M. B... est donc fondé à demander l'annulation de l'ordonnance attaquée, qui a rejeté sa demande tendant à l'annulation de cet arrêté ;<br/>
<br/>
              3. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              4. Considérant qu'aux termes de l'article R. 102 du code des tribunaux administratifs et des cours administratives d'appel, alors en vigueur, repris au premier alinéa de l'article R. 421-1 du code de justice administrative : " Sauf en matière de travaux publics, la juridiction ne peut être saisie que par voie de recours formé contre une décision, et ce, dans les deux mois à partir de la notification ou de la publication de la décision attaquée. " ; qu'il résulte des dispositions citées au point 1 que lorsque la notification ne comporte pas les mentions requises, ce délai n'est pas opposable ; <br/>
<br/>
              5. Considérant toutefois que le principe de sécurité juridique, qui implique que ne puissent être remises en cause sans condition de délai des situations consolidées par l'effet du temps, fait obstacle à ce que puisse être contestée indéfiniment une décision administrative individuelle qui a été notifiée à son destinataire, ou dont il est établi, à défaut d'une telle notification, que celui-ci a eu connaissance ; qu'en une telle hypothèse, si le non-respect de l'obligation d'informer l'intéressé sur les voies et les délais de recours, ou l'absence de preuve qu'une telle information a bien été fournie, ne permet pas que lui soient opposés les délais de recours fixés par le code de justice administrative, le destinataire de la décision ne peut exercer de recours juridictionnel au-delà d'un délai raisonnable ; qu'en règle générale et sauf circonstances particulières dont se prévaudrait le requérant, ce délai ne saurait, sous réserve de l'exercice de recours administratifs pour lesquels les textes prévoient des délais particuliers, excéder un an à compter de la date à laquelle une décision expresse lui a été notifiée ou de la date à laquelle il est établi qu'il en a eu connaissance ; <br/>
<br/>
              6. Considérant que la règle énoncée ci-dessus, qui a pour seul objet de borner dans le temps les conséquences de la sanction attachée au défaut de mention des voies et délais de recours, ne porte pas atteinte à la substance du droit au recours, mais tend seulement à éviter que son exercice, au-delà d'un délai raisonnable, ne mette en péril la stabilité des situations juridiques et la bonne administration de la justice, en exposant les défendeurs potentiels à des recours excessivement tardifs ; qu'il appartient dès lors au juge administratif d'en faire application au litige dont il est saisi, quelle que soit la date des faits qui lui ont donné naissance ;<br/>
<br/>
              7. Considérant qu'il résulte de l'instruction que M. B...a reçu notification le 26 septembre 1991 de l'arrêté portant concession de sa pension de retraite du 24 juin 1991, comme l'atteste le procès-verbal de remise de son livret de pension, et que cette notification comportait mention du délai de recours de deux mois et indication que l'intéressé pouvait former, dans ce délai, un recours contentieux ; que si une telle notification était incomplète au regard des dispositions de l'article R. 421-5 du code de justice administrative, faute de préciser si le recours pouvait être porté devant la juridiction administrative ou une juridiction spécialisée, et si, par suite, le délai de deux mois fixé par l'article R. 421-1 du même code ne lui était pas opposable, il résulte de ce qui précède que le recours dont M. B...a saisi le tribunal administratif de Lille plus de vingt-deux ans après la notification de l'arrêté contesté excédait le délai raisonnable durant lequel il pouvait être exercé ; que sa demande doit, en conséquence, être rejetée comme tardive ; qu'il en résulte que les conclusions présentées par M. B...sur le fondement de l'article L. 761-1 du code de justice administrative doivent également être rejetées ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'ordonnance du 2 décembre 2014 du tribunal administratif de Lille est annulée.<br/>
Article 2 : La demande de M. B...et le surplus des conclusions de son pourvoi sont rejetés. <br/>
Article 3 : La présente décision sera notifiée à M. A...B...et au ministre des finances et des comptes publics. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-04-03-07 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. PRINCIPES GÉNÉRAUX DU DROIT. PRINCIPES INTÉRESSANT L'ACTION ADMINISTRATIVE. - PRINCIPE DE SÉCURITÉ JURIDIQUE [RJ1] - PORTÉE - 1) INCLUSION - IMPOSSIBILITÉ DE CONTESTER INDÉFINIMENT UNE DÉCISION INDIVIDUELLE DONT SON DESTINATAIRE A EU CONNAISSANCE - 2) CONSÉQUENCE - IMPOSSIBILITÉ D'EXERCER UN RECOURS JURIDICTIONNEL AU-DELÀ D'UN DÉLAI RAISONNABLE - NOTION DE DÉLAI RAISONNABLE - CAS DES DÉCISIONS EXPRESSES.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-01-07 PROCÉDURE. INTRODUCTION DE L'INSTANCE. DÉLAIS. - CAS OÙ LES DÉLAIS N'ONT PU COURIR FAUTE D'AVOIR ÉTÉ MENTIONNÉS DANS LA NOTIFICATION - FACULTÉ, POUR LE DESTINATAIRE, DE CONTESTER INDÉFINIMENT LA DÉCISION INDIVIDUELLE DONT IL A EU CONNAISSANCE - ABSENCE - OBLIGATION D'EXERCER UN RECOURS JURIDICTIONNEL DANS UN DÉLAI RAISONNABLE [RJ2] - NOTION DE DÉLAI RAISONNABLE - CAS DES DÉCISIONS EXPRESSES.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-07-09 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. - ABSENCE - IMPOSSIBILITÉ POUR LE DESTINATAIRE D'UNE DÉCISION INDIVIDUELLE QUI EN A EU CONNAISSANCE D'EXERCER UN RECOURS JURIDICTIONNEL CONTRE CETTE DÉCISION AU-DELÀ D'UN DÉLAI RAISONNABLE.
</SCT>
<ANA ID="9A"> 01-04-03-07 1) Le principe de sécurité juridique, qui implique que ne puissent être remises en cause sans condition de délai des situations consolidées par l'effet du temps, fait obstacle à ce que puisse être contestée indéfiniment une décision administrative individuelle qui a été notifiée à son destinataire, ou dont il est établi, à défaut d'une telle notification, que celui-ci a eu connaissance.,,,2) En une telle hypothèse, si le non-respect de l'obligation d'informer l'intéressé sur les voies et les délais de recours, ou l'absence de preuve qu'une telle information a bien été fournie, ne permet pas que lui soient opposés les délais de recours fixés par le code de justice administrative, le destinataire de la décision ne peut exercer de recours juridictionnel au-delà d'un délai raisonnable.,,,En règle générale et sauf circonstances particulières dont se prévaudrait le requérant, ce délai ne saurait, sous réserve de l'exercice de recours administratifs pour lesquels les textes prévoient des délais particuliers, excéder un an à compter de la date à laquelle une décision expresse lui a été notifiée ou de la date à laquelle il est établi qu'il en a eu connaissance.</ANA>
<ANA ID="9B"> 54-01-07 Le principe de sécurité juridique, qui implique que ne puissent être remises en cause sans condition de délai des situations consolidées par l'effet du temps, fait obstacle à ce que puisse être contestée indéfiniment une décision administrative individuelle qui a été notifiée à son destinataire, ou dont il est établi, à défaut d'une telle notification, que celui-ci a eu connaissance.,,,En une telle hypothèse, si le non-respect de l'obligation d'informer l'intéressé sur les voies et les délais de recours, ou l'absence de preuve qu'une telle information a bien été fournie, ne permet pas que lui soient opposés les délais de recours fixés par le code de justice administrative, le destinataire de la décision ne peut exercer de recours juridictionnel au-delà d'un délai raisonnable.,,,En règle générale et sauf circonstances particulières dont se prévaudrait le requérant, ce délai ne saurait, sous réserve de l'exercice de recours administratifs pour lesquels les textes prévoient des délais particuliers, excéder un an à compter de la date à laquelle une décision expresse lui a été notifiée ou de la date à laquelle il est établi qu'il en a eu connaissance.</ANA>
<ANA ID="9C"> 54-07-09 La règle selon laquelle le destinataire d'une décision individuelle auquel les voies et délais de recours n'ont pas été notifiées ne peut exercer un recours juridictionnel contre cette décision au-delà d'un délai raisonnable à compter de la date où il a eu connaissance de la décision, qui a pour seul objet de borner dans le temps les conséquences de la sanction attachée au défaut de mention des voies et délais de recours, ne porte pas atteinte à la substance du droit au recours, mais tend seulement à éviter que son exercice, au-delà d'un délai raisonnable, ne mette en péril la stabilité des situations juridiques et la bonne administration de la justice, en exposant les défendeurs potentiels à des recours excessivement tardifs. Il appartient dès lors au juge administratif d'en faire application au litige dont il est saisi, quelle que soit la date des faits qui lui ont donné naissance.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Cf. CE, Assemblée, 24 mars 2006, Société KPMG et autres, n° 288460, p. 154.,,[RJ2]Comp. CE, Section, 13 mars 1998, Mme Mauline, p. 80.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
