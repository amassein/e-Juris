<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044505268</ID>
<ANCIEN_ID>JG_L_2021_12_000000453316</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/50/52/CETATEXT000044505268.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 5ème chambres réunies, 15/12/2021, 453316</TITRE>
<DATE_DEC>2021-12-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>453316</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>Mme Pauline Hot</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier  Fuchs</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:453316.20211215</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Mme G... J..., M. F... H... et M. A... D... ont demandé à au tribunal administratif de Nancy l'annulation de l'arrêté du 2 juillet 2020 par lequel le maire d'Heillecourt a délivré à la société civile de construction vente (SCCV) Viridis République un permis de construire un ensemble de vingt logements.<br/>
<br/>
              Par trois ordonnances distinctes n° 2002126, 2002128 et 2002129 du 20 mai 2021, la présidente du tribunal administratif de Nancy a transmis à la cour administrative d'appel de Nancy, en application de l'article R. 351-3 du code de justice administrative, les requêtes de Mme J..., M. H... et M. D.... <br/>
<br/>
              Par une ordonnance n° 21NC01487, 21NC01488, 21NC01489 du 4 juin 2021, la présidente de la cour administrative d'appel de Nancy a transmis au président de la section du contentieux du Conseil d'État, en application de l'article R. 351-3 du code de justice administrative, les requêtes de Mme J..., M. H... et M. D... qui ont été enregistrées sous les nos 453316, 453317 et 453318.  <br/>
<br/>
              Sous le n°453316, par des observations enregistrées au secrétariat de la section du contentieux le 30 juillet 2021, la SCCV Viridis République soutient qu'en application de l'article L. 600-5-2 du code de l'urbanisme, l'affaire doit être attribuée à la cour administrative d'appel de Nancy. <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ; <br/>
<br/>
              Vu :<br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Pauline Hot, auditrice,  <br/>
<br/>
              - les conclusions de M. Olivier Fuchs, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de la SCCV Viridis République ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes l'article R. 351-3 du code de justice administrative : " Lorsqu'une cour administrative d'appel ou un tribunal administratif est saisi de conclusions qu'il estime relever de la compétence d'une juridiction administrative autre que le Conseil d'Etat, son président, ou le magistrat qu'il délègue, transmet sans délai le dossier à la juridiction qu'il estime compétente. / Toutefois, en cas de difficultés particulières, il peut transmettre sans délai le dossier au président de la section du contentieux du Conseil d'Etat qui règle la question de compétence et attribue le jugement de tout ou partie de l'affaire à la juridiction qu'il déclare compétente. (...) ".<br/>
<br/>
              2. Aux termes de l'article L. 600-5-2 du code de l'urbanisme, dans sa rédaction issue de la même loi : " Lorsqu'un permis modificatif, une décision modificative ou une mesure de régularisation intervient au cours d'une instance portant sur un recours dirigé contre le permis de construire, de démolir ou d'aménager initialement délivré ou contre la décision de non-opposition à déclaration préalable initialement obtenue et que ce permis modificatif, cette décision modificative ou cette mesure de régularisation ont été communiqués aux parties à cette instance, la légalité de cet acte ne peut être contestée par les parties que dans le cadre de cette même instance ". <br/>
<br/>
              3. Il résulte de ces dispositions que, lorsque le juge d'appel est saisi d'un appel contre un jugement d'un tribunal administratif ayant annulé un permis de construire en retenant l'existence d'un ou plusieurs vices entachant sa légalité et qu'un permis modificatif, une décision modificative ou une mesure visant à la régularisation de ces vices a été pris, seul le juge d'appel est compétent pour connaître de sa contestation dès lors que ce permis, cette décision ou cette mesure lui a été communiqué ainsi qu'aux parties. Par suite, si un recours pour excès de pouvoir a été formé contre ce permis, cette décision ou cette mesure devant le tribunal administratif, il incombe à ce dernier de le transmettre, en application des articles R. 351-3 et, le cas échéant, R. 345-2 du code de justice administrative, à la cour administrative d'appel saisie de l'appel contre le jugement relatif au permis initial. <br/>
<br/>
              4. Il ressort des pièces du dossier transmis au Conseil d'Etat que, par un jugement du 7 mai 2019, le tribunal administratif de Nancy, saisi par Mme J..., a, d'une part, annulé l'arrêté du 2 juillet 2018 par lequel le maire d'Heillecourt (Meurthe-et-Moselle) a délivré à la SCCV Viridis République un permis de construire un ensemble de vingt-et-un logements et, d'autre part, rejeté les conclusions de la commune et de la pétitionnaire tendant à la mise en œuvre des articles L. 600-5 et L. 600-5-1 du code de l'urbanisme en jugeant que les vices relevés n'étaient pas susceptibles d'être régularisés. La commune d'Heillecourt et la SCCV Viridis République ont fait appel de ce jugement devant la cour administrative d'appel de Nancy, où ces instances sont actuellement pendantes. Entretemps, par un arrêté du 2 juillet 2020, le maire d'Heillecourt, saisi d'une demande visant à régulariser le permis de construire annulé, a délivré à la SCCV Viridis République un permis de construire un ensemble de vingt logements à la même adresse, dont Mme J..., M. H... et M. D... ont demandé l'annulation au tribunal administratif de Nancy. Par trois ordonnances du 20 mai 2021, la présidente de ce tribunal a transmis les dossiers à la cour administrative d'appel, aux motifs que l'arrêté du 2 juillet 2020 visait à régulariser les vices que le tribunal avait relevés dans son jugement du 7 mai 2019 et qu'il résultait des dispositions du code de l'urbanisme citées au point 2 que la cour était seule compétente pour connaître des conclusions contre ce nouvel arrêté. Par une ordonnance du 4 juin 2021, la présidente de la cour administrative d'appel de Nancy a toutefois transmis au président de la section du contentieux du Conseil d'État, en application de l'article R. 351-3 du code de justice administrative, les requêtes de Mme J..., M. H... et M. D....  <br/>
<br/>
              5. Il résulte de ce qui a été dit au point 3, que la cour administrative d'appel de Nancy est seule compétente pour connaître, dans le cadre de l'instance d'appel dirigée contre le jugement du tribunal administratif de Nancy du 7 mai 2019 ayant annulé le permis de construire délivré le 2 juillet 2018 par le maire d'Heillecourt à la SCCV Viridis République, de la contestation dirigée contre l'arrêté pris le 2 juillet 2020 par le même maire d'Heillecourt, à la demande de la même société, en vue de régulariser les vices affectant le permis initial retenus par le tribunal administratif, permis qui a été communiqué au juge d'appel et aux parties à cette instance par son bénéficiaire. La seule circonstance que le tribunal administratif a considéré, dans le jugement frappé d'appel, que les vices qu'il relevait n'étaient pas susceptibles d'être régularisés par un permis de construire modificatif ne saurait à elle seule faire obstacle à la compétence de la cour administrative d'appel, saisi en appel de ce jugement, pour connaître de cette contestation.<br/>
<br/>
              6. Par suite, il y a lieu d'attribuer à la cour administrative d'appel de Nancy le jugement des requêtes, enregistrées sous les numéros 453316, 453317 et 453318 au secrétariat du contentieux du Conseil d'Etat, présentées par Mme J..., M. H... et M. D... contre le permis de construire accordé à la SCCV Viridis République.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le jugement des requêtes de Mme J..., M. H... et M. D... est attribué à la cour administrative d'appel de Nancy.<br/>
Article 2 : La présente décision sera notifiée à Mme G... J..., à M. F... H... et à M. A... D..., à la présidente du tribunal administratif de Nancy, et à la présidente de la cour administrative d'appel de Nancy.<br/>
              Délibéré à l'issue de la séance du 22 novembre 2021 où siégeaient : M. Rémy Schwartz, président adjoint de la Section du contentieux, présidant ; M. Fabien Raynaud, président de chambre ; Mme N... I..., M. L... C..., Mme E... K..., M. Cyril Roger-Lacan, conseillers d'Etat et Mme Pauline Hot, auditrice-rapporteure. <br/>
<br/>
              Rendu le 15 décembre 2021.<br/>
<br/>
<br/>
                 Le président : <br/>
                 Signé : M. Rémy Schwartz<br/>
 		La rapporteure : <br/>
      Signé : Mme Pauline Hot<br/>
                 La secrétaire :<br/>
                 Signé : Mme M... B...<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">17-05-01-01 COMPÉTENCE. - COMPÉTENCE À L'INTÉRIEUR DE LA JURIDICTION ADMINISTRATIVE. - COMPÉTENCE EN PREMIER RESSORT DES TRIBUNAUX ADMINISTRATIFS. - COMPÉTENCE MATÉRIELLE. - EXCLUSION - RECOURS CONTRE LA MESURE DE RÉGULARISATION D'UN PERMIS INITIAL ENTIÈREMENT ANNULÉ PAR UN JUGEMENT FRAPPÉ D'APPEL [RJ1] - CONSÉQUENCE - OBLIGATION DE TRANSMISSION AU JUGE D'APPEL.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">17-05-015 COMPÉTENCE. - COMPÉTENCE À L'INTÉRIEUR DE LA JURIDICTION ADMINISTRATIVE. - COMPÉTENCE D'APPEL DES COURS ADMINISTRATIVES D'APPEL. - COMPÉTENCE EN PREMIER ET DERNIER RESSORT DES CAA - INCLUSION - RECOURS CONTRE LA MESURE DE RÉGULARISATION D'UN PERMIS INITIAL ENTIÈREMENT ANNULÉ PAR UN JUGEMENT FRAPPÉ D'APPEL [RJ1] - CONSÉQUENCE - OBLIGATION DE TRANSMISSION AU JUGE D'APPEL.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">68-06-01 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. - RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. - INTRODUCTION DE L'INSTANCE. - MESURE DE RÉGULARISATION D'UN PERMIS INITIAL ENTIÈREMENT ANNULÉ PAR UN JUGEMENT FRAPPÉ D'APPEL - COMPÉTENCE DU JUGE D'APPEL POUR CONNAÎTRE DU RECOURS CONTRE CETTE MESURE - EXISTENCE [RJ1] - CONSÉQUENCE - OBLIGATION DE TRANSMISSION.
</SCT>
<ANA ID="9A"> 17-05-01-01 Il résulte de l'article L. 600-5-2 du code de l'urbanisme que, lorsque le juge d'appel est saisi d'un appel contre un jugement d'un tribunal administratif ayant annulé un permis de construire en retenant l'existence d'un ou plusieurs vices entachant sa légalité et qu'un permis modificatif, une décision modificative ou une mesure visant à la régularisation de ces vices a été pris, seul le juge d'appel est compétent pour connaître de sa contestation dès lors que ce permis, cette décision ou cette mesure lui a été communiqué ainsi qu'aux parties. ......Par suite, si un recours pour excès de pouvoir a été formé contre ce permis, cette décision ou cette mesure devant le tribunal administratif, il incombe à ce dernier de le transmettre, en application des articles R. 351-3 et, le cas échéant, R. 345-2 du code de justice administrative (CJA), à la cour administrative d'appel saisie de l'appel contre le jugement relatif au permis initial.</ANA>
<ANA ID="9B"> 17-05-015 Il résulte de l'article L. 600-5-2 du code de l'urbanisme que, lorsque le juge d'appel est saisi d'un appel contre un jugement d'un tribunal administratif ayant annulé un permis de construire en retenant l'existence d'un ou plusieurs vices entachant sa légalité et qu'un permis modificatif, une décision modificative ou une mesure visant à la régularisation de ces vices a été pris, seul le juge d'appel est compétent pour connaître de sa contestation dès lors que ce permis, cette décision ou cette mesure lui a été communiqué ainsi qu'aux parties. ......Par suite, si un recours pour excès de pouvoir a été formé contre ce permis, cette décision ou cette mesure devant le tribunal administratif, il incombe à ce dernier de le transmettre, en application des articles R. 351-3 et, le cas échéant, R. 345-2 du code de justice administrative (CJA), à la cour administrative d'appel saisie de l'appel contre le jugement relatif au permis initial.</ANA>
<ANA ID="9C"> 68-06-01 Il résulte de l'article L. 600-5-2 du code de l'urbanisme que, lorsque le juge d'appel est saisi d'un appel contre un jugement d'un tribunal administratif ayant annulé un permis de construire en retenant l'existence d'un ou plusieurs vices entachant sa légalité et qu'un permis modificatif, une décision modificative ou une mesure visant à la régularisation de ces vices a été pris, seul le juge d'appel est compétent pour connaître de sa contestation dès lors que ce permis, cette décision ou cette mesure lui a été communiqué ainsi qu'aux parties. ......Par suite, si un recours pour excès de pouvoir a été formé contre ce permis, cette décision ou cette mesure devant le tribunal administratif, il incombe à ce dernier de le transmettre, en application des articles R. 351-3 et, le cas échéant, R. 345-2 du code de justice administrative (CJA), à la cour administrative d'appel saisie de l'appel contre le jugement relatif au permis initial.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., lorsqu'une telle mesure est délivrée à la suite d'une annulation partielle par les premiers juges, CE, Section, 15 février 2019, Commune de Cogolin, n° 401384, p. 26. Comp., lorsqu'une telle mesure est délivrée dans le cadre d'un sursis à statuer ordonné par les premiers juges, CE, 5 février 2021, M. et Mme Boissery, n° 430990, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
