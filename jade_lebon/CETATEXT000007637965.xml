<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000007637965</ID>
<ANCIEN_ID>JGXLX1969X11X0000071780</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/07/63/79/CETATEXT000007637965.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'Etat, du 5 novembre 1969, 71780, publié au recueil Lebon</TITRE>
<DATE_DEC>1969-11-05</DATE_DEC>
<JURIDICTION>Conseil d'Etat</JURIDICTION>
<NUMERO>71780</NUMERO>
<SOLUTION>REJET</SOLUTION>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Recours pour excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Négrier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Braibant</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>   REQUETE DU SIEUR X... TENDANT A L'ANNULATION D'UN JUGEMENT DU 10 NOVEMBRE 1966 PAR LEQUEL LE TRIBUNAL ADMINISTRATIF DE ROUEN A REJETE SA DEMANDE TENDANT A L'ANNULATION D'UNE DECISION DU 25 SEPTEMBRE 1964 DU MINISTRE DE LA SANTE PUBLIQUE ET DE LA POPULATION, QUI A SUBORDONNE SA NOMINATION COMME DIRECTEUR ECONOME DE L 'HOPITAL-HOSPICE D'EU A SON ACCEPTATION DE N'ETRE REMUNERE QUE SUR LA BASE DE L'INDICE BRUT 530, ENSEMBLE A L'ANNULATION POUR EXCES DE POUVOIR DE LADITE DECISION ;<br/>   VU LE DECRET DU 2 AOUT 1960 ; L'ORDONNANCE DU 31 JUILLET 1945 ET LE DECRET DU 30 SEPTEMBRE 1953 ; LE CODE GENERAL DES IMPOTS ;<br/>   CONSIDERANT QUE, PAR UNE DECISION EN DATE DU 25 SEPTEMBRE 1964, LE MINISTRE DE LA SANTE PUBLIQUE ET DE LA POPULATION A REFUSE AU SIEUR X..., OFFICIER DE POLICE PRINCIPAL DE LA SURETE NATIONALE, LE Y... DE CONSERVER A TITRE PERSONNEL DANS L'EMPLOI DE DIRECTEUR ECONOME DE L'HOPITAL-HOSPICE D'EU QUI LUI ETAIT PROPOSE ET AUQUEL CORRESPONDAIT L'ECHELLE INDICIAIRE DE TRAITEMENT 370-530, L'INDICE 585 SUR LA BASE DUQUEL IL ETAIT REMUNERE EN QUALITE D'OFFICIER DE POLICE PRINCIPAL ;<br/>   CONS., D'UNE PART, QUE L'ARTICLE 5 DU DECRET SUSMENTIONNE DU 2 AOUT 1960, SELON LEQUEL "TOUT AGENT POURVU D'UNE NOUVELLE AFFECTATION DANS LA MEME CLASSE CONSERVE A TITRE PERSONNEL LE BENEFICE DE SON ANCIEN INDICE DE TRAITEMENT SI CELUI-CI EST SUPERIEUR A L'INDICE AFFERENT A L'ECHELON TERMINAL DE SON NOUVEL EMPLOI" N'EST APPLICABLE, D'APRES SES TERMES MEMES, QU'AUX AGENTS DU CADRE DU PERSONNEL DE DIRECTION DES HOPITAUX ET HOSPICES PUBLICS QUI FONT L'OBJET D'UNE NOUVELLE AFFECTATION DANS LA MEME CLASSE ; QUE CE N'ETAIT PAS LE CAS DU SIEUR X... ;<br/>   CONS., D'AUTRE PART, QUE SI L'ARTICLE 20 DU MEME DECRET PREVOIT QUE LES AGENTS QUI APPARTENAIENT A UN CORPS DE CATEGORIE A SONT CLASSES, DES LEUR NOMINATION, A L'ECHELON COMPORTANT UN INDICE EGAL OU A DEFAUT IMMEDIATEMENT SUPERIEUR A L'INDICE DONT ILS BENEFICIAIENT DANS LEUR ANCIEN EMPLOI, CETTE DISPOSITION N'A EU NI POUR OBJET NI POUR EFFET DE PERMETTRE A ES AGENTS DE CONSERVER A TITRE PERSONNEL UN INDICE DE TRAITEMENT SUPERIEUR A L'INDICE AFFERENT A L'ECHELON TERMINAL DE LEUR NOUVEL EMPLOI ; QU'AINSI, EN TOUT ETAT DE CAUSE, LE SIEUR X... N'EST PAS, NON PLUS, FONDE A INVOQUER UNE VIOLATION DUDIT ARTICLE 20 ;<br/>   CONS. ENFIN, QU'AUCUNE AUTRE DISPOSITION DU DECRET DONT S'AGIT, NI AUCUN PRINCIPE GENERAL DU Y... DE LA FONCTION PUBLIQUE, NE DONNAIT AU SIEUR X... LE Y... DE CONSERVER SON INDICE DE TRAITEMENT ;<br/>   REJET AVEC DEPENS.<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8AA" TYPE="PRINCIPAL">01-04-03 ACTES LEGISLATIFS ET ADMINISTRATIFS - VALIDITE DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA REGLE DE DROIT - PRINCIPES GENERAUX DU DROIT -Absence de tels principes - Absence de droit, pour un fonctionnaire affecté à un nouvel emploi, de conserver son indice de traitement.</SCT>
<SCT ID="8BA" TYPE="PRINCIPAL">36-08-02 FONCTIONNAIRES ET AGENTS PUBLICS - REMUNERATION - TRAITEMENT -Classement indiciaire - Reclassement prévu par un texte, après nouvelle affectation.</SCT>
<SCT ID="8CA" TYPE="PRINCIPAL">61-02-04 SANTE PUBLIQUE - ETABLISSEMENTS PUBLICS D'HOSPITALISATION - PERSONNELS ADMINISTRATIFS ET AUTRES -Personnel administratif - Classement indiciaire - Absence de droit, pour des agents pourvus d'une nouvelle affectation, à conserver l'ancien indice de traitement [décret du 2 août 1960].</SCT>
<ANA ID="9AA">01-04-03          Aucun principe général du droit de la fonction publique ne donne à un fonctionnaire affecté à un nouvel emploi le droit de conserver son indice de traitement.</ANA>
<ANA ID="9BA">36-08-02          Reclassement prévu par un texte, après nouvelle affectation, à un "indice égal ou immédiatement supérieur" à celui dont l'agent bénéficiait dans son corps d'origine ou dans son ancien emploi. Nouvel emploi ayant un indice terminal inférieur à celui dont l'agent bénéficiait antérieurement. Absence de droit des intéressés à conserver à titre personnel le bénéfice de l'indice plus élevé dont ils bénéficiaient antérieurement : jugé pour un agent du personnel de direction d'un hôpital ayant reçu une nouvelle affectation.</ANA>
<ANA ID="9CA">61-02-04          L'article 5 du décret du 2 août 1960 n'est, d'après ses termes mêmes, applicable qu'aux agents du cadre du personnel de direction des hôpitaux et hospices publics qui font l'objet d'une nouvelle affectation "dans la même classe". L'article 20 du même décret, qui prévoit que les agents qui appartenaient à un corps de catégorie A sont classés, dès leur nomination, à l'échelon comportant un indice égal ou, à défaut immédiatement supérieur à l'indice dont ils bénéficiaient dans leur ancien emploi, n'a eu ni pour objet ni pour effet de permettre à ces agents de conserver à titre personnel un indice de traitement supérieur à l'indice afférent à l'échelon terminal de leur nouvel emploi.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS>
<LIEN cidtexte="" datesignatexte="" id="" naturetexte="" nortexte="" num="" numtexte="" sens="source" typelien="CITATION">Décret 60-796 1960-08-02 ART. 5, ART. 20</LIEN>
</LIENS>
</TEXTE_JURI_ADMIN>
