<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027150939</ID>
<ANCIEN_ID>JG_L_2013_03_000000361273</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/15/09/CETATEXT000027150939.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Section du Contentieux, 08/03/2013, 361273, Publié au recueil Lebon</TITRE>
<DATE_DEC>2013-03-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>361273</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Section du Contentieux</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Jean Lessi</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CESEC:2013:361273.20130308</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le jugement n° 0804612ADD2 du 12 juillet 2012, enregistré le 23 juillet 2012 au secrétariat du contentieux du Conseil d'Etat, par lequel le tribunal administratif de Rennes, avant de statuer sur la demande de M. B... A...tendant à ce que la Société nationale des chemins de fer français (SNCF) soit condamnée à lui payer la somme de 76 240 euros à titre d'indemnisation pour les conséquences dommageables de l'accident dont il a été victime le 28 juillet 1994, a décidé, par application de l'article L. 113-1 du code de justice administrative, de transmettre le dossier de cette demande au Conseil d'Etat, en soumettant à son examen les questions suivantes :<br/>
<br/>
              1°) Lorsqu'il résulte de l'instruction que la rente d'accident du travail ne peut être rattachée à un quelconque préjudice patrimonial, le juge doit-il nécessairement imputer cette rente sur le poste des préjudices personnels '<br/>
<br/>
              2°) Dans le cas contraire, quels éléments le tiers payeur doit-il produire à l'instance afin d'établir de manière incontestable que la rente d'accident du travail répare en tout ou partie un préjudice personnel '<br/>
<br/>
<br/>
<br/>
....................................................................................<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de la sécurité sociale ;<br/>
<br/>
              Vu la loi n° 85-677 du 5 juillet 1985 ;<br/>
<br/>
              Vu le code de justice administrative, notamment son article L. 113-1 ;<br/>
<br/>
<br/>
               Après avoir entendu en séance publique :<br/>
<br/>
- le rapport de M. Jean Lessi, Maître des Requêtes, <br/>
<br/>
- les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
REND L'AVIS SUIVANT<br/>
<br/>
<br/>
<br/>
              1. L'article 31 de la loi du 5 juillet 1985 tendant à l'amélioration de la situation des victimes d'accidents de la circulation et à l'accélération des procédures d'indemnisation détermine les droits respectifs, à l'égard des responsables du dommage, des victimes de dommages résultant d'une atteinte à la personne et des tiers payeurs qui leur versent des prestations. Il dispose, dans sa rédaction issue de la loi du 21 décembre 2006 de financement de la sécurité sociale pour 2007, que : " Les recours subrogatoires des tiers payeurs s'exercent poste par poste sur les seules indemnités qui réparent des préjudices qu'elles ont pris en charge, à l'exclusion des préjudices à caractère personnel. / Conformément à l'article 1252 du code civil, la subrogation ne peut nuire à la victime subrogeante, créancière de l'indemnisation, lorsqu'elle n'a été indemnisée qu'en partie ; en ce cas, elle peut exercer ses droits contre le responsable, pour ce qui lui reste dû, par préférence au tiers payeur dont elle n'a reçu qu'une indemnisation partielle. / Cependant, si le tiers payeur établit qu'il a effectivement et préalablement versé à la victime une prestation indemnisant de manière incontestable un poste de préjudice personnel, son recours peut s'exercer sur ce poste de préjudice ".<br/>
<br/>
              2. Il résulte de ces dispositions que le recours subrogatoire du tiers payeur ne peut s'exercer sur les indemnités mises à la charge du responsable du dommage que dans la mesure où celles-ci réparent des préjudices que les prestations en cause ont pour objet d'indemniser.<br/>
<br/>
              3. Ces dispositions s'appliquent au recours subrogatoire exercé par une caisse de sécurité sociale au titre des prestations servies à la victime d'un accident du travail en application du livre IV du code de la sécurité sociale, notamment de la rente versée à la victime atteinte d'une incapacité permanente de travail dont le taux est supérieur à un seuil déterminé, mentionnée au 4° de l'article L. 431-1 de ce code.<br/>
<br/>
              4. Aux termes de l'article L. 434-2 du même code : " Le taux de l'incapacité permanente est déterminé d'après la nature de l'infirmité, l'état général, l'âge, les facultés physiques et mentales de la victime ainsi que d'après ses aptitudes et sa qualification professionnelle, compte tenu d'un barème indicatif d'invalidité. / Lorsque l'incapacité permanente est égale ou supérieure à un taux minimum, la victime a droit à une rente égale au salaire annuel multiplié par le taux d'incapacité qui peut être réduit ou augmenté en fonction de la gravité de celle-ci (...) ".<br/>
<br/>
              5. Eu égard à sa finalité de réparation d'une incapacité permanente de travail, qui lui est assignée par l'article L. 431-1, et à son mode de calcul, appliquant au salaire de référence de la victime le taux d'incapacité permanente défini par l'article L. 434-2, la rente d'accident du travail doit être regardée comme ayant pour objet exclusif de réparer, sur une base forfaitaire, les préjudices subis par la victime dans sa vie professionnelle en conséquence de l'accident, c'est-à-dire ses pertes de gains professionnels et l'incidence professionnelle de l'incapacité.<br/>
<br/>
              Dès lors, le recours exercé par la caisse au titre d'une rente d'accident du travail ne saurait s'exercer que sur ces deux postes de préjudice. En particulier, une telle rente ne saurait être imputée sur un poste de préjudice personnel.<br/>
<br/>
              6. Compte tenu de la réponse apportée à la première question posée par le tribunal administratif de Rennes, il n'y a pas lieu de répondre à sa deuxième question.<br/>
<br/>
              Le présent avis sera notifié au tribunal administratif de Rennes, à M. B... A..., à la caisse primaire d'assurance maladie du Finistère, à la Société nationale des chemins de fer français et à la ministre des affaires sociales et de la santé.<br/>
<br/>
<br/>
<br/>
<br/>
              Il sera publié au Journal officiel de la République française.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">60-05-04-01-02 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RECOURS OUVERTS AUX DÉBITEURS DE L'INDEMNITÉ, AUX ASSUREURS DE LA VICTIME ET AUX CAISSES DE SÉCURITÉ SOCIALE. DROITS DES CAISSES DE SÉCURITÉ SOCIALE. IMPUTATION DES DROITS À REMBOURSEMENT DE LA CAISSE. ARTICLE L. 454-1 (ANCIEN ART. L. 470) DU CODE DE LA SÉCURITÉ SOCIALE. - RECOURS SUBROGATOIRE EXERCÉ PAR LA CAISSE AU TITRE D'UNE RENTE D'ACCIDENT DU TRAVAIL (4° DE L'ART. L. 431-1 DU CODE DE LA SÉCURITÉ SOCIALE) - OBJET EXCLUSIF DE LA RENTE - RÉPARATION DES PRÉJUDICES SUBIS PAR LA VICTIME DANS SA VIE PROFESSIONNELLE EN CONSÉQUENCE DE L'ACCIDENT - CONSÉQUENCE - APPLICATION DES DISPOSITIONS ISSUES DU IV DE L'ARTICLE 25 DE LA LOI DU 21 DÉCEMBRE 2006 [RJ1] - POSTES DE PRÉJUDICE SUR LESQUELS IMPUTER LA RENTE - PERTES DE GAINS PROFESSIONNELS ET INCIDENCE PROFESSIONNELLE DE L'INCAPACITÉ [RJ2] - EXISTENCE - POSTE DE PRÉJUDICE PERSONNEL - ABSENCE [RJ3].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">62-04-05 SÉCURITÉ SOCIALE. PRESTATIONS. PRESTATIONS D'ASSURANCES ACCIDENTS DU TRAVAIL ET MALADIES PROFESSIONNELLES. - RENTE D'ACCIDENT DU TRAVAIL - OBJET EXCLUSIF DE LA RENTE - RÉPARATION DES PRÉJUDICES SUBIS PAR LA VICTIME DANS SA VIE PROFESSIONNELLE EN CONSÉQUENCE DE L'ACCIDENT - CONSÉQUENCE - RECOURS SUBROGATOIRE EXERCÉ PAR LA CAISSE AU TITRE D'UNE TELLE RENTE (4° DE L'ART. L. 431-1 DU CODE DE LA SÉCURITÉ SOCIALE) - APPLICATION DES DISPOSITIONS ISSUES DU IV DE L'ARTICLE 25 DE LA LOI DU 21 DÉCEMBRE 2006 [RJ1] - POSTES DE PRÉJUDICE SUR LESQUELS IMPUTER LA RENTE - PERTES DE GAINS PROFESSIONNELS ET INCIDENCE PROFESSIONNELLE DE L'INCAPACITÉ [RJ2] - EXISTENCE - POSTE DE PRÉJUDICE PERSONNEL - ABSENCE [RJ3].
</SCT>
<ANA ID="9A"> 60-05-04-01-02 Eu égard à sa finalité de réparation d'une incapacité permanente de travail, qui lui est assignée par l'article L. 431-1 du code de la sécurité sociale, et à son mode de calcul, appliquant au salaire de référence de la victime le taux d'incapacité permanente défini par l'article L. 434-2 du même code, la rente d'accident du travail doit être regardée comme ayant pour objet exclusif de réparer, sur une base forfaitaire, les préjudices subis par la victime dans sa vie professionnelle en conséquence de l'accident, c'est-à-dire ses pertes de gains professionnels et l'incidence professionnelle de l'incapacité. Dès lors, le recours subrogatoire exercé par une caisse de sécurité sociale au titre d'une rente d'accident du travail ne saurait, pour l'application des règles résultant de l'article 31 de la loi n° 85-677 du 5 juillet 1985, dans sa rédaction issue du IV de l'article 25 de la loi n° 2006-1640 du 21 décembre 2006, s'exercer que sur ces deux postes de préjudice. En particulier, une telle rente ne saurait être imputée sur un poste de préjudice personnel.</ANA>
<ANA ID="9B"> 62-04-05 Eu égard à sa finalité de réparation d'une incapacité permanente de travail, qui lui est assignée par l'article L. 431-1 du code de la sécurité sociale, et à son mode de calcul, appliquant au salaire de référence de la victime le taux d'incapacité permanente défini par l'article L. 434-2 du même code, la rente d'accident du travail doit être regardée comme ayant pour objet exclusif de réparer, sur une base forfaitaire, les préjudices subis par la victime dans sa vie professionnelle en conséquence de l'accident, c'est-à-dire ses pertes de gains professionnels et l'incidence professionnelle de l'incapacité. Dès lors, le recours subrogatoire exercé par une caisse de sécurité sociale au titre d'une rente d'accident du travail ne saurait, pour l'application des règles résultant de l'article 31 de la loi n° 85-677 du 5 juillet 1985, dans sa rédaction issue du IV de l'article 25 de la loi n° 2006-1640 du 21 décembre 2006, s'exercer que sur ces deux postes de préjudice. En particulier, une telle rente ne saurait être imputée sur un poste de préjudice personnel.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., s'agissant de l'applicabilité de ces dispositions aux recours subrogatoires exercés par les caisses au titre de prestations servies aux victimes d'accidents du travail en application du livre IV du code de la sécurité sociale, CE, 5 mars 2008, Caisse primaire d'assurance maladie de Seine-Saint-Denis, n° 272447, p. 95.,,[RJ2] Cf., s'agissant de la nomenclature des postes de préjudice, CE, avis, Section, 4 juin 2007, Lagier et Consorts Guignon, n°s 303422 304214, p. 228.,,[RJ3] Comp. Cass. crim., 19 mai 2009, n° 08-82666, Bull. crim. 2009, n° 97 ; Cass. civ. 2è, 11 juin 2009, n° 07-21768, Bull. 2009, II, n° 153 ; Cass. civ. 2è, 11 juin 2009, n° 08-16089, Bull. 2009, II, n° 154 ; Cass. civ. 2è, 11 juin 2009, n° 08-17851, Bull. 2009, II, n° 155. Cf., s'agissant de l'imputation de la rente d'accident du travail sur la part patrimoniale du préjudice, CE, 5 mars 2008, Caisse primaire d'assurance maladie de Seine-Saint-Denis, n° 272447, p. 95. Ab. jur. cette même décision s'agissant de la possibilité pour la caisse d'établir qu'une telle rente a réparé de manière incontestable tout ou partie d'un préjudice personnel.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
