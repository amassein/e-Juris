<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041485713</ID>
<ANCIEN_ID>JG_L_2019_11_000000426372</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/48/57/CETATEXT000041485713.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 29/11/2019, 426372, Publié au recueil Lebon</TITRE>
<DATE_DEC>2019-11-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>426372</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Stéphanie Vera</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Guillaume Odinet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:426372.20191129</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique enregistrés les 18 décembre 2018 et 30 octobre 2019 au secrétariat du contentieux du Conseil d'Etat, Mme A... B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le décret du 8 avril 1977 portant libération de ses liens d'allégeance avec la France ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.  <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code civil ;<br/>
              - le code de la nationalité française ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Stéphanie Vera, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Guillaume Odinet, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1.	Aux termes de l'article 91 du code de la nationalité française, dans sa rédaction à la date du décret attaqué : " Perd la nationalité française, le Français même mineur, qui, ayant une nationalité étrangère, est autorisé, sur sa demande, par le Gouvernement français, à perdre la qualité de Français. - Cette autorisation est accordée par décret. - Le mineur doit, le cas échéant, être autorisé ou représenté dans les conditions prévues aux articles 53 et 54 ". L'article 53 du même code dispose que : " La qualité de Français peut être réclamée à partir de dix-huit ans. - Le mineur âgé de seize ans peut également la réclamer avec l'autorisation de celui ou de ceux qui exercent à son égard l'autorité parentale ". Aux termes de l'article 54 du même code : " Si l'enfant est âgé de moins de seize ans, les personnes visées à l'alinéa 2 de l'article précédent peuvent déclarer qu'elles réclament, au nom du mineur, la qualité de Français (...) ".<br/>
<br/>
              2.	En l'absence de prescription en disposant autrement, les conditions d'âge fixées par ces articles s'apprécient à la date de signature des décrets pris sur leur fondement. Il en résulte que, si des parents peuvent formuler au nom d'un enfant mineur une demande tendant à ce que celui-ci soit libéré de ses liens d'allégeance avec la France, le décret prononçant une telle libération ne peut, toutefois, être signé, si l'intéressé a atteint l'âge de seize ans, sans qu'il ait lui-même exprimé, avec l'accord de ceux qui exercent sur lui l'autorité parentale, une demande en ce sens et, s'il a atteint l'âge de dix-huit ans, sans qu'il ait personnellement déposé une demande à cette fin.<br/>
<br/>
              3.	Il ressort des pièces du dossier que le père de Mme B... a demandé, le 25 juin 1975, l'autorisation de perdre la qualité de Français pour lui-même et ses enfants mineurs. Sur cette demande, par un décret du 8 avril 1977 publié au Journal Officiel le 22 avril suivant, Mme B... a été libérée de ses liens d'allégeance avec la France.<br/>
<br/>
              4.	Le principe de sécurité juridique, qui implique que ne puissent être remises en cause sans condition de délai des situations consolidées par l'effet du temps, fait obstacle à ce que puisse être contestée indéfiniment une décision administrative individuelle qui a été notifiée à son destinataire, ou dont il est établi, à défaut d'une telle notification, que celui-ci a eu connaissance. En une telle hypothèse, si le non-respect de l'obligation d'informer l'intéressé sur les voies et les délais de recours, ou l'absence de preuve qu'une telle information a bien été fournie, ne permet pas que lui soient opposés les délais de recours fixés par le code de justice administrative, le destinataire de la décision ne peut exercer de recours juridictionnel au-delà d'un délai raisonnable. S'agissant d'un décret de libération des liens d'allégeance, ce délai ne saurait, eu égard aux effets de cette décision, excéder, sauf circonstances particulières dont se prévaudrait le requérant, trois ans à compter de la date de publication du décret ou si elle est plus tardive de la date de la majorité de l'intéressé. <br/>
<br/>
              5.	Il ressort des pièces du dossier que Mme B..., à laquelle le décret du 8 avril 1977 portant libération des liens d'allégeance avec la France n'a pas été notifié, n'a été informée de son existence qu'à la suite d'une assignation délivrée en 2017 par huissier de justice à la demande du procureur de la République près le tribunal de grande instance de Paris contestant la délivrance d'un certificat de nationalité française à l'intéressée, procédure toujours pendante devant cette juridiction. Il n'est pas contesté que l'intéressée, qui vit en France, exerce des fonctions d'agent administratif dans la fonction publique territoriale depuis 1978, que lui ont été délivrées, à plusieurs reprises, des pièces d'identité françaises ainsi qu'un certificat de nationalité française et qu'elle n'a jamais cessé d'être regardée comme Française, en particulier dans ses relations avec les administrations de l'Etat et des collectivités territoriales. En conséquence, au regard des circonstances particulières dont se prévaut Mme B..., la requête de l'intéressée, contrairement à ce que soutient le ministre de l'intérieur, est recevable.<br/>
<br/>
              6.	Il ressort des pièces du dossier que si le père de la requérante a sollicité le 25 juin 1975 l'autorisation de perdre la qualité de Français pour lui-même et ses enfants mineurs, Mme B..., née le 9 janvier 1959, était âgée de plus de dix-huit ans à la date du décret contesté du 8 avril 1977. Le Premier ministre ne pouvait donc légalement, à cette date, autoriser Mme B... à perdre la nationalité française qu'au vu d'une demande présentée personnellement par celle-ci. Or, il ne ressort pas des pièces du dossier que Mme B... aurait présenté une telle demande.  Il résulte de ce qui précède que cette dernière est fondée à demander l'annulation du décret du 8 avril 1977 en ce qu'il porte libération de ses liens d'allégeance avec la France.<br/>
<br/>
              7.	Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le décret du 8 avril 1977 est annulé en tant qu'il libère Mme A... B... de ses liens d'allégeance avec la France.<br/>
<br/>
Article 2 : L'Etat versera à Mme B... une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée à Mme A... B... et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-04-03-07 ACTES LÉGISLATIFS ET ADMINISTRATIFS. - VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. - PRINCIPES GÉNÉRAUX DU DROIT. - PRINCIPES INTÉRESSANT L'ACTION ADMINISTRATIVE. - PRINCIPE DE SÉCURITÉ JURIDIQUE - IMPOSSIBILITÉ D'EXERCER UN RECOURS JURIDICTIONNEL AU-DELÀ D'UN DÉLAI RAISONNABLE [RJ1] - NOTION DE DÉLAI RAISONNABLE - APPLICATION AUX RECOURS CONTRE LES DÉCRETS DE LIBÉRATION DES LIENS D'ALLÉGEANCE - 1) PRINCIPE - DÉLAI DE TROIS ANS À COMPTER DE LA DATE DE PUBLICATION DU DÉCRET OU, SI ELLE EST PLUS TARDIVE, DE LA DATE DE LA MAJORITÉ DE L'INTÉRESSÉ [RJ2] - EXCEPTION - DÉLAI NON APPLICABLE EN CAS DE CIRCONSTANCES EXCEPTIONNELLES - 2) EXISTENCE DE TELLES CIRCONSTANCES EN L'ESPÈCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">26-01-01-015 DROITS CIVILS ET INDIVIDUELS. - ÉTAT DES PERSONNES. - NATIONALITÉ. - PERTE DE LA NATIONALITÉ. - DÉCRET DE LIBÉRATION DES LIENS D'ALLÉGEANCE AVEC LA FRANCE PAR DÉCRET - IMPOSSIBILITÉ D'EXERCER UN RECOURS JURIDICTIONNEL AU-DELÀ D'UN DÉLAI RAISONNABLE [RJ1] - NOTION DE DÉLAI RAISONNABLE - 1) PRINCIPE - DÉLAI DE TROIS ANS À COMPTER DE LA DATE DE PUBLICATION DU DÉCRET OU, SI ELLE EST PLUS TARDIVE, DE LA DATE DE LA MAJORITÉ DE L'INTÉRESSÉ [RJ2] - EXCEPTION - DÉLAI NON APPLICABLE EN CAS DE CIRCONSTANCES EXCEPTIONNELLES - 2) EXISTENCE DE TELLES CIRCONSTANCES EN L'ESPÈCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-01-07 PROCÉDURE. - INTRODUCTION DE L'INSTANCE. - DÉLAIS. - IMPOSSIBILITÉ D'EXERCER UN RECOURS JURIDICTIONNEL AU-DELÀ D'UN DÉLAI RAISONNABLE [RJ1] - NOTION DE DÉLAI RAISONNABLE - APPLICATION AUX RECOURS CONTRE LES DÉCRETS DE LIBÉRATION DES LIENS D'ALLÉGEANCE - 1) PRINCIPE - DÉLAI DE TROIS ANS À COMPTER DE LA DATE DE PUBLICATION DU DÉCRET OU, SI ELLE EST PLUS TARDIVE, DE LA DATE DE LA MAJORITÉ DE L'INTÉRESSÉ [RJ2] - EXCEPTION - DÉLAI NON APPLICABLE EN CAS DE CIRCONSTANCES EXCEPTIONNELLES - 2) EXISTENCE DE TELLES CIRCONSTANCES EN L'ESPÈCE.
</SCT>
<ANA ID="9A"> 01-04-03-07 Le principe de sécurité juridique, qui implique que ne puissent être remises en cause sans condition de délai des situations consolidées par l'effet du temps, fait obstacle à ce que puisse être contestée indéfiniment une décision administrative individuelle qui a été notifiée à son destinataire, ou dont il est établi, à défaut d'une telle notification, que celui-ci a eu connaissance. En une telle hypothèse, si le non-respect de l'obligation d'informer l'intéressé sur les voies et les délais de recours, ou l'absence de preuve qu'une telle information a bien été fournie, ne permet pas que lui soient opposés les délais de recours fixés par le code de justice administrative, le destinataire de la décision ne peut exercer de recours juridictionnel au-delà d'un délai raisonnable. ......1) S'agissant d'un décret de libération des liens d'allégeance, ce délai ne saurait, eu égard aux effets de cette décision, excéder, sauf circonstances particulières dont se prévaudrait le requérant, trois ans à compter de la date de publication du décret ou, si elle est plus tardive, de la date de la majorité de l'intéressé. ......2) Requérante, à laquelle le décret du 8 avril 1977 portant libération des liens d'allégeance avec la France n'a pas été notifié, n'ayant été informée de son existence qu'à la suite d'une assignation délivrée en 2017 par huissier de justice à la demande du procureur de la République près le tribunal de grande instance de Paris contestant la délivrance d'un certificat de nationalité française à l'intéressée, procédure toujours pendante devant cette juridiction. ......Requérante vivant en France, exerçant des fonctions d'agent administratif dans la fonction publique territoriale depuis 1978, s'étant vu délivrer, à plusieurs reprises, des pièces d'identité françaises ainsi qu'un certificat de nationalité française et n'ayant jamais cessé d'être regardée comme française, en particulier dans ses relations avec les administrations de l'Etat et des collectivités territoriales. ......En conséquence, au regard des circonstances particulières dont elle se prévaut, la requête de l'intéressée, contrairement à ce que soutient le ministre de l'intérieur, est recevable.</ANA>
<ANA ID="9B"> 26-01-01-015 Le principe de sécurité juridique, qui implique que ne puissent être remises en cause sans condition de délai des situations consolidées par l'effet du temps, fait obstacle à ce que puisse être contestée indéfiniment une décision administrative individuelle qui a été notifiée à son destinataire, ou dont il est établi, à défaut d'une telle notification, que celui-ci a eu connaissance. En une telle hypothèse, si le non-respect de l'obligation d'informer l'intéressé sur les voies et les délais de recours, ou l'absence de preuve qu'une telle information a bien été fournie, ne permet pas que lui soient opposés les délais de recours fixés par le code de justice administrative, le destinataire de la décision ne peut exercer de recours juridictionnel au-delà d'un délai raisonnable. ......1) S'agissant d'un décret de libération des liens d'allégeance, ce délai ne saurait, eu égard aux effets de cette décision, excéder, sauf circonstances particulières dont se prévaudrait le requérant, trois ans à compter de la date de publication du décret ou, si elle est plus tardive, de la date de la majorité de l'intéressé. ......2) Requérante, à laquelle le décret du 8 avril 1977 portant libération des liens d'allégeance avec la France n'a pas été notifié, n'ayant été informée de son existence qu'à la suite d'une assignation délivrée en 2017 par huissier de justice à la demande du procureur de la République près le tribunal de grande instance de Paris contestant la délivrance d'un certificat de nationalité française à l'intéressée, procédure toujours pendante devant cette juridiction. ......Requérante vivant en France, exerçant des fonctions d'agent administratif dans la fonction publique territoriale depuis 1978, s'étant vu délivrer, à plusieurs reprises, des pièces d'identité françaises ainsi qu'un certificat de nationalité française et n'ayant jamais cessé d'être regardée comme française, en particulier dans ses relations avec les administrations de l'Etat et des collectivités territoriales. ......En conséquence, au regard des circonstances particulières dont elle se prévaut, la requête de l'intéressée, contrairement à ce que soutient le ministre de l'intérieur, est recevable.</ANA>
<ANA ID="9C"> 54-01-07 Le principe de sécurité juridique, qui implique que ne puissent être remises en cause sans condition de délai des situations consolidées par l'effet du temps, fait obstacle à ce que puisse être contestée indéfiniment une décision administrative individuelle qui a été notifiée à son destinataire, ou dont il est établi, à défaut d'une telle notification, que celui-ci a eu connaissance. En une telle hypothèse, si le non-respect de l'obligation d'informer l'intéressé sur les voies et les délais de recours, ou l'absence de preuve qu'une telle information a bien été fournie, ne permet pas que lui soient opposés les délais de recours fixés par le code de justice administrative, le destinataire de la décision ne peut exercer de recours juridictionnel au-delà d'un délai raisonnable. ......1) S'agissant d'un décret de libération des liens d'allégeance, ce délai ne saurait, eu égard aux effets de cette décision, excéder, sauf circonstances particulières dont se prévaudrait le requérant, trois ans à compter de la date de publication du décret ou, si elle est plus tardive, de la date de la majorité de l'intéressé. ......2) Requérante, à laquelle le décret du 8 avril 1977 portant libération des liens d'allégeance avec la France n'a pas été notifié, n'ayant été informée de son existence qu'à la suite d'une assignation délivrée en 2017 par huissier de justice à la demande du procureur de la République près le tribunal de grande instance de Paris contestant la délivrance d'un certificat de nationalité française à l'intéressée, procédure toujours pendante devant cette juridiction. ......Requérante vivant en France, exerçant des fonctions d'agent administratif dans la fonction publique territoriale depuis 1978, s'étant vu délivrer, à plusieurs reprises, des pièces d'identité françaises ainsi qu'un certificat de nationalité française et n'ayant jamais cessé d'être regardée comme française, en particulier dans ses relations avec les administrations de l'Etat et des collectivités territoriales. ......En conséquence, au regard des circonstances particulières dont elle se prévaut, la requête de l'intéressée, contrairement à ce que soutient le ministre de l'intérieur, est recevable.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Assemblée, 13 juillet 2016, M. Czabaj, n° 387763, p. 340....[RJ2] Cf. CE, décision du même jour, M. Boumrar, n° 411145, à publier au Recueil.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
