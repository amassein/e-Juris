<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037882265</ID>
<ANCIEN_ID>JG_L_2018_12_000000412849</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/88/22/CETATEXT000037882265.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 4ème chambres réunies, 28/12/2018, 412849</TITRE>
<DATE_DEC>2018-12-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>412849</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, SOLTNER, TEXIDOR, PERIER</AVOCATS>
<RAPPORTEUR>M. Thibaut Félix</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Charles Touboul</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:412849.20181228</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Procédure contentieuse antérieure<br/>
<br/>
              La fédération CGT Santé et action sociale a demandé au tribunal administratif de Paris d'annuler pour excès de pouvoir l'arrêté du ministre des affaires sociales et de la santé du 21 décembre 2012 relatif à l'agrément de certains accords de travail applicables dans les établissements et services du secteur social et médico-social privé à but non lucratif. Par un jugement n° 1302482 du 23 décembre 2014, le tribunal administratif de Paris a rejeté cette demande.<br/>
<br/>
              Par un arrêt n° 15PA00836 du 31 mai 2017, la cour administrative d'appel de Paris a, sur l'appel de la fédération CGT Santé et action sociale, annulé ce jugement et l'arrêté du 21 décembre 2012.<br/>
<br/>
              Procédures devant le Conseil d'Etat<br/>
<br/>
              1° Sous le n° 412849, par un pourvoi, enregistré le 28 juillet 2017 au secrétariat du contentieux du Conseil d'Etat, le ministre des solidarités et de la santé demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de la fédération CGT Santé et action sociale.<br/>
<br/>
              2° Sous le n° 412895, par un pourvoi sommaire, un mémoire complémentaire et un nouveau mémoire, enregistrés les 31 juillet, 27 octobre et 11 décembre 2017, la fédération des établissements hospitaliers et d'aide à la personne privés à but non lucratif demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le même arrêt de la cour administrative de Paris du 31 mai 2017 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de la fédération CGT Santé et action sociale ;<br/>
<br/>
              3°) de mettre à la charge de la fédération CGT Santé et action sociale la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - le code de l'action sociale et des familles ;<br/>
              - le code du travail ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Thibaut Félix, auditeur, <br/>
<br/>
              - les conclusions de M. Charles Touboul, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Soltner, Texidor, Perier, avocat de la fédération des établissements hospitaliers et d'aide à la personne privés, à but non lucratif.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 314-6 du code de l'action sociale et des familles : " Les conventions collectives de travail, conventions d'entreprise ou d'établissement et accords de retraite applicables aux salariés des établissements et services sociaux et médico-sociaux à but non lucratif dont les dépenses de fonctionnement sont, en vertu de dispositions législatives ou réglementaires, supportées, en tout ou partie, directement ou indirectement, soit par des personnes morales de droit public, soit par des organismes de sécurité sociale, ne prennent effet qu'après agrément donné par le ministre compétent après avis d'une commission où sont représentés des élus locaux et dans des conditions fixées par voie réglementaire (...) ". En vertu de l'article R. 314-197 du même code, le ministre compétent pour donner l'agrément mentionné à l'article L. 314-6 est celui chargé de l'action sociale.<br/>
<br/>
              2. Il ressort des pièces du dossier soumis aux juges du fond qu'en application de ces dispositions, le ministre des affaires sociales et de la santé a, le 21 décembre 2012, agréé la recommandation patronale émise le 4 septembre 2012 par la Fédération des établissements hospitaliers et d'aide à la personne (FEHAP) en vue de combler le vide créé par la dénonciation partielle, par cette fédération, le 1er septembre 2011, de la convention collective nationale des établissements privés d'hospitalisation, de soins, de cure et de garde à but non lucratif du 31 octobre 1951, ce à quoi n'avait pu pourvoir l'accord de substitution, reprenant en substance cette recommandation patronale, conclu le 12 novembre 2012 mais frappé d'opposition majoritaire et par suite réputé non écrit en vertu de l'article L. 2231-9 du code du travail. La fédération CGT Santé et action sociale a demandé au tribunal administratif de Paris d'annuler pour excès de pouvoir l'arrêté du ministre des affaires sociales et de la santé du 21 décembre 2012. Par un jugement du 23 décembre 2014, le tribunal administratif de Paris a rejeté cette demande. Par des pourvois qu'il y a lieu de joindre, le ministre des solidarités et de la santé et la FEHAP se pourvoient en cassation contre l'arrêt par lequel, sur l'appel de la fédération CGT Santé et action sociale, la cour administrative d'appel de Paris a annulé ce jugement et l'arrêté du 21 décembre 2012.<br/>
<br/>
              3. Lorsque, à l'occasion d'un litige relatif à un arrêté prononçant l'extension ou l'agrément d'une convention ou d'un accord collectif de travail, qui relève de la compétence de la juridiction administrative, une contestation sérieuse s'élève sur la validité de cette convention ou de cet accord, il appartient au juge saisi du litige de surseoir à statuer jusqu'à ce que l'autorité judiciaire se soit prononcée sur la question préjudicielle que présente à juger cette contestation, sauf s'il apparaît manifestement, au vu d'une jurisprudence établie, que la contestation peut être accueillie par le juge saisi au principal ou si elle met en cause la conformité de la convention ou de l'accord litigieux au droit de l'Union européenne.<br/>
<br/>
              4. Pour annuler l'arrêté du 21 décembre 2012 du ministre des affaires sociales et de la santé, la cour administrative d'appel a jugé, d'une part, qu'une recommandation patronale, qui est une déclaration unilatérale par laquelle un groupement ou un syndicat d'employeurs reconnaît certains droits au profit des salariés, ne saurait avoir pour objet ou pour effet, notamment du fait de l'étendue et de l'exhaustivité de son contenu, de se substituer à un accord collectif et, d'autre part, que le ministre des affaires sociales et de la santé n'avait pu, sans méconnaître les dispositions de l'article L. 2231-9 du code du travail, agréer une recommandation patronale d'un contenu similaire à celui d'un accord collectif ayant fait l'objet d'une opposition majoritaire. Ce faisant, la cour s'est implicitement mais nécessairement prononcée sur une contestation sérieuse qui s'élevait quant à la validité de la recommandation patronale agréée par la décision litigieuse, qu'il n'appartenait, en l'absence de toute jurisprudence établie permettant manifestement de l'accueillir, qu'au juge judiciaire de trancher. En s'abstenant de surseoir à statuer jusqu'à ce que l'autorité judiciaire se soit prononcée sur cette question, elle a méconnu sa compétence.<br/>
<br/>
              5. Par suite, sans qu'il soit besoin d'examiner les moyens des pourvois, les requérants sont fondés à demander l'annulation de l'arrêt qu'ils attaquent.<br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative. <br/>
<br/>
              7. En premier lieu, il ressort de l'examen de la minute du jugement attaqué que celle-ci comporte les signatures du président de la formation de jugement, du rapporteur et du greffier d'audience, requises par l'article R. 741-7 du code de justice administrative. Par suite,  le moyen tiré de la méconnaissance de ces dispositions doit être écarté.<br/>
<br/>
              8. En deuxième lieu, une recommandation patronale intervenant dans le champ d'une convention ou d'un accord collectif applicable aux salariés des établissements et services sociaux et médico-sociaux à but non lucratif dont les dépenses de fonctionnement sont, en vertu de dispositions législatives ou réglementaires, supportées, en tout ou partie, par des personnes morales de droit public ou par des organismes de sécurité sociale ne peut, en vertu de l'article L. 314-6 du code de l'action sociale et des familles, prendre effet, comme une telle convention ou un tel accord collectif, qu'après agrément du ministre chargé des affaires sociales. Par suite, la fédération requérante n'est pas fondée à soutenir qu'une telle recommandation ne peut faire l'objet d'un agrément de ce ministre. <br/>
<br/>
              9. En troisième lieu, toutefois, pour contester l'arrêté attaqué, la fédération CGT Santé et action sociale soutient que le ministre ne pouvait légalement, le 21 décembre 2012, agréer la recommandation patronale émise par la FEHAP le 4 septembre 2012, dès lors qu'elle était de contenu similaire à celui d'un accord de substitution frappé d'opposition majoritaire le 12 novembre 2012, dont les stipulations étaient par suite réputées non écrites en vertu de l'article L. 2231-9 du code du travail. L'appréciation du bien-fondé de ce moyen dépend notamment du point de savoir si cette recommandation patronale pouvait valablement suppléer à l'absence d'un accord collectif, alors même que sa négociation était en cours, et si l'opposition syndicale majoritaire à l'accord de substitution du 12 novembre 2012 a eu un effet sur la validité de la recommandation patronale, de contenu similaire à cet accord, du 4 septembre 2012. Cette question présente à juger une difficulté sérieuse, qui ne peut être résolue au vu d'une jurisprudence établie et qu'il n'appartient, dès lors, qu'au juge judiciaire de trancher.<br/>
<br/>
              10. Par suite, il y a lieu pour le Conseil d'Etat de surseoir à statuer sur l'appel de la fédération CGT Santé et action sociale tendant à l'annulation du jugement du tribunal administratif de Paris du 23 décembre 2014 jusqu'à ce que la juridiction compétente se soit prononcée sur cette question préjudicielle. Il y a lieu, par suite, de surseoir également à statuer sur les conclusions des parties présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
                                   --------------<br/>
Article 1er : L'arrêt du 31 mai 2017 de la cour administrative d'appel de Paris est annulé.<br/>
Article 2 : Il est sursis à statuer sur l'appel de la fédération CGT Santé et action sociale dirigé contre le jugement du tribunal administratif de Paris du 23 décembre 2014 jusqu'à ce que le tribunal de grande instance de Paris se soit prononcé sur la question de la validité de la recommandation patronale du 4 septembre 2012 au regard des moyens mentionnés au point 9 de la présente décision.<br/>
Article 3 : Il est sursis à statuer sur les conclusions des parties présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la ministre des solidarités et de la santé, à la fédération des établissements hospitaliers, et d'aide à la personne, privés à but non lucratif et au président du tribunal de grande instance de Paris. <br/>
Copie en sera adressée à la fédération CGT Santé et action sociale.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">17-03-01-02-05 COMPÉTENCE. RÉPARTITION DES COMPÉTENCES ENTRE LES DEUX ORDRES DE JURIDICTION. COMPÉTENCE DÉTERMINÉE PAR DES TEXTES SPÉCIAUX. ATTRIBUTIONS LÉGALES DE COMPÉTENCE AU PROFIT DES JURIDICTIONS JUDICIAIRES. DIVERS CAS D`ATTRIBUTIONS LÉGALES DE COMPÉTENCE AU PROFIT DES JURIDICTIONS JUDICIAIRES. - COUR AYANT JUGÉ QU'UNE RECOMMANDATION PATRONALE INTERVENANT DANS LE CHAMP D'UNE CONVENTION OU D'UN ACCORD COLLECTIF NE SAURAIT AVOIR POUR OBJET OU POUR EFFET DE S'Y SUBSTITUER - COUR S'ÉTANT, CE FAISANT, PRONONCÉE SUR UNE CONTESTATION SÉRIEUSE QUANT À LA VALIDITÉ DE CETTE RECOMMENDATION PATRONALE EN L'ABSENCE DE JURISPRUDENCE ÉTABLIE - CONSÉQUENCE - INCOMPÉTENCE DE LA COUR À RELEVER D'OFFICE PAR LE JUGE DE CASSATION - EXISTENCE [RJ1] - CONSÉQUENCE - ANNULATION DE LA DÉCISION DE LA COUR ET RENVOI D'UNE QUESTION PRÉJUDICIELLE AU JUGE JUDICIAIRE DANS LE CADRE DU RÉGLEMENT AU FOND.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">66-02-03 TRAVAIL ET EMPLOI. CONVENTIONS COLLECTIVES. AGRÉMENT DE CERTAINES CONVENTIONS COLLECTIVES. - RECOMMENDATION PATRONALE INTERVENANT DANS LE CHAMP D'UNE CONVENTION OU D'UN ACCORD COLLECTIF  - 1) COUR AYANT JUGÉ QUE CETTE RECOMMANDATION PATRONALE NE SAURAIT AVOIR POUR OBJET OU POUR EFFET DE S'Y SUBSTITUER - COUR S'ÉTANT, CE FAISANT, PRONONCÉE SUR UNE CONTESTATION SÉRIEUSE QUANT À LA VALIDITÉ DE CETTE RECOMMENDATION PATRONALE EN L'ABSENCE DE JURISPRUDENCE ÉTABLIE - CONSÉQUENCE - INCOMPÉTENCE DE LA COUR À RELEVER D'OFFICE PAR LE JUGE DE CASSATION - EXISTENCE [RJ1] - CONSÉQUENCE - ANNULATION DE LA DÉCISION DE LA COUR ET RENVOI D'UNE QUESTION PRÉJUDICIELLE AU JUGE JUDICIAIRE DANS LE CADRE DU RÉGLEMENT AU FOND - 2) OBLIGATION D'AGRÉMENT, SUR LE FONDEMENT DE L'ARTICLE L. 314-6 DU CASF, POUR PRENDRE EFFET - EXISTENCE.
</SCT>
<ANA ID="9A"> 17-03-01-02-05 Cour administrative d'appel ayant jugé, pour annuler l'arrêté, en litige, du 21 décembre 2012 du ministre des affaires sociales et de la santé relatif à l'agrément de certains accords de travail applicables dans les établissements et services du secteur social et médico-social privé à but non lucratif, d'une part, qu'une recommandation patronale, qui est une déclaration unilatérale par laquelle un groupement ou un syndicat d'employeurs reconnaît certains droits au profit des salariés, ne saurait avoir pour objet ou pour effet, notamment du fait de l'étendue et de l'exhaustivité de son contenu, de se substituer à un accord collectif et, d'autre part, que le ministre des affaires sociales et de la santé n'avait pu, sans méconnaître les dispositions de l'article L. 2231-9 du code du travail, agréer une recommandation patronale d'un contenu similaire à celui d'un accord collectif ayant fait l'objet d'une opposition majoritaire.... ,,Ce faisant, la cour s'est implicitement mais nécessairement prononcée sur une contestation sérieuse qui s'élevait quant à la validité de la recommandation patronale agréée par la décision litigieuse, qu'il n'appartenait, en l'absence de toute jurisprudence établie permettant manifestement de l'accueillir, qu'au juge judiciaire de trancher. En s'abstenant de surseoir à statuer jusqu'à ce que l'autorité judiciaire se soit prononcée sur cette question, elle a méconnu sa compétence. Par suite, annulation de son arrêt et renvoi d'une question préjudicielle dans le cadre du réglement au fond.</ANA>
<ANA ID="9B"> 66-02-03 1) Cour administrative d'appel ayant jugé, pour annuler l'arrêté, en litige, du 21 décembre 2012 du ministre des affaires sociales et de la santé relatif à l'agrément de certains accords de travail applicables dans les établissements et services du secteur social et médico-social privé à but non lucratif, d'une part, qu'une recommandation patronale, qui est une déclaration unilatérale par laquelle un groupement ou un syndicat d'employeurs reconnaît certains droits au profit des salariés, ne saurait avoir pour objet ou pour effet, notamment du fait de l'étendue et de l'exhaustivité de son contenu, de se substituer à un accord collectif et, d'autre part, que le ministre des affaires sociales et de la santé n'avait pu, sans méconnaître les dispositions de l'article L. 2231-9 du code du travail, agréer une recommandation patronale d'un contenu similaire à celui d'un accord collectif ayant fait l'objet d'une opposition majoritaire.... ,,Ce faisant, la cour s'est implicitement mais nécessairement prononcée sur une contestation sérieuse qui s'élevait quant à la validité de la recommandation patronale agréée par la décision litigieuse, qu'il n'appartenait, en l'absence de toute jurisprudence établie permettant manifestement de l'accueillir, qu'au juge judiciaire de trancher. En s'abstenant de surseoir à statuer jusqu'à ce que l'autorité judiciaire se soit prononcée sur cette question, elle a méconnu sa compétence. Par suite, annulation de son arrêt et renvoi d'une question préjudicielle dans le cadre du réglement au fond.,,,2) Une recommandation patronale intervenant dans le champ d'une convention ou d'un accord collectif applicable aux salariés des établissements et services sociaux et médico-sociaux à but non lucratif dont les dépenses de fonctionnement sont, en vertu de dispositions législatives ou réglementaires, supportées, en tout ou partie, par des personnes morales de droit public ou par des organismes de sécurité sociale ne peut, en vertu de l'article L. 314-6 du code de l'action sociale et des familles (CASF), prendre effet, comme une telle convention ou un tel accord collectif, qu'après agrément du ministre chargé des affaires sociales.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 20 mai 1955, Sieur,,  p. 275 ; CE, Section, 11 décembre 1959, Banque des Pays de l'Europe centrale, n° 40660, p. 672 ; CE, Section, 18 novembre 1960, Sieur,ès qualités de liquidateur de la Société des Mines et usines à zinc de Silésie, n° 43574, p. 634 ; CE, 26 juin 1964, Société anonyme de l'industrie textile, n° 45566, p. 360.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
