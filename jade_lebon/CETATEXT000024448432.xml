<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000024448432</ID>
<ANCIEN_ID>JG_L_2011_08_000000330310</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/24/44/84/CETATEXT000024448432.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème et 7ème sous-sections réunies, 03/08/2011, 330310</TITRE>
<DATE_DEC>2011-08-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>330310</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème et 7ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELVOLVE, DELVOLVE</AVOCATS>
<RAPPORTEUR>Mme Stéphanie Gargoullaud</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier de Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2011:330310.20110803</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 31 juillet 2009 au secrétariat du contentieux du Conseil d'Etat, présentée pour la COMMUNE DE BUC, représentée par son maire en exercice ; la COMMUNE DE BUC demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2009-615 du 3 juin 2009 fixant la liste des routes à grande circulation en tant qu'il inclut dans cette liste la partie de la route départementale 938 (RD 938) entre Versailles et Toussus-le-Noble (Yvelines) ;<br/>
<br/>
               2°) de mettre à la charge de l'Etat le versement de la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu le code de la route ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Vu la décision du Conseil d'Etat 19 mai 2010 décidant de ne pas transmettre au Conseil Constitutionnel la question prioritaire de constitutionnalité présentée par la COMMUNE DE BUC ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Stéphanie Gargoullaud, chargée des fonctions de Maître des requêtes, <br/>
<br/>
              - les observations de la SCP Delvolvé, Delvolvé, avocat de la COMMUNE DE BUC et de l'Association de la rue Blériot à Buc pour la réduction des nuisances dues à la circulation,<br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public,<br/>
<br/>
              La parole ayant à nouveau été donnée à la SCP Delvolvé, Delvolvé, avocat de la COMMUNE DE BUC et de l'Association de la rue Blériot à Buc pour la réduction des nuisances dues à la circulation ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant qu'aux termes de l'article L. 110-3 du code de la route : " Les routes à grande circulation, quelle que soit leur appartenance domaniale, sont les routes qui permettent d'assurer la continuité des itinéraires principaux et, notamment, le délestage du trafic, la circulation des transports exceptionnels, des convois et des transports militaires et la desserte économique du territoire, et justifient, à ce titre, des règles particulières en matières de police de la circulation. La liste des routes à grande circulation est fixée par décret, après avis des collectivités et des groupements propriétaires des voies. / Les collectivités et groupements propriétaires des voies classées comme routes à grande circulation communiquent au représentant de l'Etat dans le département, avant leur mise en oeuvre, les projets de modification des caractéristiques techniques de ces voies et toutes mesures susceptibles de rendre ces routes impropres à leur destination. / Un décret en Conseil d'Etat détermine les conditions d'application du présent article " ;<br/>
<br/>
              Considérant que le décret du 3 juin 2009 a fixé, en application de ces dispositions, la liste des routes à grande circulation ; que ce décret a notamment inscrit sur cette liste, dans le département des Yvelines, la route départementale 938 qui relie Versailles à Toussus-le-Noble sur le territoire de Buc ; que la COMMUNE DE BUC est recevable, compte tenu en particulier des incidences que le classement de la route sur la liste des routes à grande circulation emporte sur l'exercice du pouvoir de police municipale, à demander l'annulation pour excès de pouvoir du décret attaqué, en tant qu'il porte classement de la portion de la route départementale 938 traversant le territoire de la commune ; <br/>
<br/>
              Sur l'intervention : <br/>
<br/>
              Considérant que l'Association de la rue Louis Blériot à Buc pour la réduction des nuisances dues à la circulation (ARBBUC), qui s'est donnée pour objet d'agir en vue de réduire les nuisances liées à la circulation à Buc et, en particulier, sur la rue Louis Blériot, a intérêt à l'annulation du décret attaqué en tant qu'il est relatif au classement comme route à grande circulation de la route départementale 938 qui répond, à Buc, à l'appellation de rue Louis Blériot ; qu'ainsi son intervention est recevable ;<br/>
<br/>
              Sur la légalité du décret attaqué :<br/>
<br/>
              Considérant, en premier lieu, que ni les dispositions de l'article L. 110-3 du code de la route ni aucun principe n'imposaient que la COMMUNE DE BUC, qui n'est pas propriétaire de la route en cause, fût consultée préalablement à l'intervention du décret attaqué ; qu'il ne ressort en tout état de cause pas des pièces du dossier que l'autorité administrative aurait décidé de procéder à titre facultatif à la consultation d'autres communes traversées par des routes susceptibles d'être classées en s'abstenant de le faire pour la seule commune requérante ; que, par suite, le moyen tiré de ce que le décret serait illégal faute de consultation préalable de la COMMUNE DE BUC ne peut qu'être écarté ;<br/>
<br/>
              Considérant, en deuxième lieu, que c'est sans erreur matérielle que le décret attaqué fait référence à la route départementale 838, dans la nomenclature du département de l'Essonne, pour déterminer la limite du classement au sud, dans le département des Yvelines, de la route départementale 938 aux confins du territoire de la COMMUNE DE BUC ; que si, comme le soutient la commune requérante, le décret attaqué comporte une erreur matérielle en faisant référence à une route nationale 266 pour préciser la limite du classement au nord de la route départementale 938, la portion de la route classée est clairement établie par ce décret dont l'annexe indique qu'elle est limitée au nord par la ville de Versailles ; que l'erreur matérielle est, par suite, dépourvue d'incidence sur la légalité du décret attaqué ; <br/>
<br/>
              Considérant, en troisième lieu, que s'il n'était pas initialement proposé de faire figurer la route départementale 938 sur la liste des routes à grande circulation et que le conseil général des Yvelines, par l'avis qu'il a émis sur le projet de décret, s'est montré favorable à un tel classement, il ne ressort nullement des pièces du dossier que le Premier ministre se serait pour autant cru lié par l'avis émis par le conseil général des Yvelines pour décider in fine de l'inscription de la route en cause sur la liste établie par le décret attaqué ; <br/>
<br/>
              Considérant, en quatrième lieu, qu'il résulte des termes de l'article L. 110-3 du code de la route que les routes à grande circulation sont les voies qui permettent d'assurer la continuité des itinéraires principaux, et notamment le délestage du trafic, la circulation des transports exceptionnels, des convois et des transports militaires et la desserte économique du territoire ; qu'il ressort des pièces du dossier que la route départementale 938 permet, d'une part, de relier la route nationale 12, qui constitue un axe important entre Paris et l'ouest de l'Ile-de-France, à la zone d'activités située au sud de Buc et, d'autre part, de rejoindre la route départementale 36 qui dessert le plateau de Saclay, servant ainsi l'objectif de desserte économique du territoire ; que si la commune requérante et l'association intervenante font valoir que la voie serait inadaptée au transit des poids lourds, il ne ressort pas des pièces du dossier que cette route, qui comptait déjà au nombre des routes à grande circulation avant l'intervention du décret attaqué, serait impropre au trafic des véhicules susceptibles de l'emprunter ; que les nuisances causées par le transit des poids lourds sont, par elles-mêmes sans incidence sur la légalité de l'inscription sur la liste des routes à grande circulation ; que le Premier ministre n'a, dans ces conditions, pas fait une inexacte application des dispositions de l'article L. 110-3 du code de la route en procédant à l'inscription de la route départementale 938, pour ce qui concerne le territoire de la commune requérante, sur la liste des routes à grande circulation ; <br/>
<br/>
              Considérant qu'il résulte de ce qui précède que la COMMUNE DE BUC n'est pas fondée à demander l'annulation pour excès de pouvoir du décret attaqué ; que ses conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative doivent, en conséquence, être rejetées ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'intervention de l'Association de la rue Louis Blériot à Buc pour la réduction des nuisances dues à la circulation (ARBBUC) est admise.<br/>
<br/>
Article 2 : La requête de la COMMUNE DE BUC est rejetée.<br/>
Article 3 : La présente décision sera notifiée à la COMMUNE DE BUC, à l'Association de la rue Louis Blériot à Buc pour la réduction des nuisances dues à la circulation (ARBBUC), à la ministre de l'écologie, du développement durable, des transports et du logement et au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-07-02-03 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. CONTRÔLE DU JUGE DE L'EXCÈS DE POUVOIR. APPRÉCIATIONS SOUMISES À UN CONTRÔLE NORMAL. - CLASSEMENT D'UNE VOIE SUR LA LISTE DES ROUTES À GRANDE CIRCULATION [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">71-01 VOIRIE. COMPOSITION ET CONSISTANCE. - CLASSEMENT D'UNE VOIE SUR LA LISTE DES ROUTES À GRANDE CIRCULATION - CONTRÔLE DU JUGE - CONTRÔLE NORMAL [RJ1].
</SCT>
<ANA ID="9A"> 54-07-02-03 Le juge exerce un contrôle normal sur la décision de classement d'une voie sur la liste des routes à grande circulation.</ANA>
<ANA ID="9B"> 71-01 Le juge exerce un contrôle normal sur la décision de classement d'une voie sur la liste des routes à grande circulation.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., sur le refus d'inscription, décision du même jour, Commune de Clichy-la-Garenne, n° 330476, à publier au Recueil.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
