<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027437319</ID>
<ANCIEN_ID>JG_L_2013_05_000000359592</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/43/73/CETATEXT000027437319.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème / 8ème SSR, 22/05/2013, 359592</TITRE>
<DATE_DEC>2013-05-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>359592</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème / 8ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>BOUTHORS</AVOCATS>
<RAPPORTEUR>M. Christophe Pourreau</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2013:359592.20130522</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu 1°, sous le n° 359592, le pourvoi sommaire et le mémoire complémentaire, enregistrés les 22 mai et 22 août 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour la commune des Hermaux, représentée par son maire ; la commune des Hermaux demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 10MA02141 du 22 mars 2012 par lequel la cour administrative d'appel de Marseille, faisant droit à la requête présentée par M. B... A..., a, premièrement, annulé le jugement n° 0801531 du 5 novembre 2009 par lequel le tribunal administratif de Nîmes a rejeté sa demande d'annulation de la décision du 4 mars 2008 par laquelle le maire des Hermaux a rejeté sa demande de saisine du conseil municipal en vue de l'abrogation de la délibération du conseil municipal du 14 janvier 1994 fixant les règles d'attribution des terrains communaux à vocation agricole, deuxièmement, annulé la décision du maire du 4 mars 2008 et, troisièmement, enjoint au conseil municipal d'abroger sa délibération du 14 janvier 1994, dans le délai de deux mois à compter de la notification de l'arrêt ;<br/>
<br/>
              2°) de mettre à la charge de M. A... la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu 2°, sous le n° 359593, le pourvoi sommaire et le mémoire complémentaire, enregistrés les 22 mai et 22 août 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour la commune des Hermaux, représentée par son maire ; la commune des Hermaux demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 10MA02255 du 22 mars 2012 par lequel la cour administrative d'appel de Marseille, faisant droit à la requête présentée par M. B... A..., a, d'une part, annulé le jugement n° 0702525-0703671-0800414 du tribunal administratif de Nîmes du 5 novembre 2009 en tant qu'il a rejeté sa demande d'annulation de la délibération du conseil municipal du 11 octobre 2007 en tant qu'elle a rejeté sa demande d'attribution du lot n° 9 des biens communaux à vocation agricole et, d'autre part, annulé cette délibération ;<br/>
<br/>
              2°) de mettre à la charge de M. A... la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code civil ;<br/>
<br/>
              Vu le code général des collectivités territoriales ;<br/>
<br/>
              Vu la loi du 10 juin 1793 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Christophe Pourreau, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée avant et après les conclusions à Me Bouthors, avocat de la commune des Hermaux ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que, par deux arrêts du 22 mars 2012, la cour administrative d'appel de Marseille, faisant droit aux requêtes présentées par M. B... A..., habitant de la commune des Hermaux (Lozère), a, par des motifs identiques, d'une part, annulé un premier jugement du tribunal administratif de Nîmes du 5 novembre 2009 en tant qu'il a rejeté sa demande d'annulation de la délibération du conseil municipal du 11 octobre 2007 lui attribuant le lot n°6 des terrains communaux à vocation agricole, et non le lot n°9 qu'il demandait, et, d'autre part, annulé un second jugement du tribunal administratif de Nîmes du 5 novembre 2009 rejetant sa demande d'annulation de la décision du maire du 4 mars 2008 refusant de saisir le conseil municipal d'un projet d'abrogation de la délibération du 14 janvier 1994 fixant le règlement d'attribution des terrains communaux à vocation agricole et prévoyant, pour la mise à disposition de ces terrains, la conclusion d'un " bail emphytéotique " avec la SAFER de Lozère suivie de la mise à bail des terrains, précédemment allotis, aux agriculteurs en activité résidant dans la commune, moyennant le paiement d'une redevance, annulé la décision du maire et enjoint au conseil municipal d'abroger sa délibération du 14 janvier 1994, dans le délai de deux mois à compter de la notification de l'arrêt ;<br/>
<br/>
              2. Considérant que la commune des Hermaux demande l'annulation des deux arrêts de la cour administrative d'appel de Marseille du 22 mars 2012 en soulevant des moyens identiques ; qu'il y a lieu de joindre les deux pourvois pour statuer par une seule décision ;<br/>
<br/>
              3. Considérant, d'une part, qu'aux termes de la loi du 10 juin 1793 : " Tous les biens communaux en général connus dans toute la République sous les divers noms de terres vaines et vagues, gastes, garrigues, landes, pacages, pâtis, ajoncs, bruyères, bois communs, hermes vacants, palus, marais, marécages, montagnes et sous toute autre dénomination quelconque, sont et appartiennent de leur nature, à la généralité des habitants ou membres des communes ou des sections de communes dans le territoire desquelles ces communaux sont situés. " ; qu'aux termes de l'article 542 du code civil : " Les biens communaux sont ceux à la propriété ou au produit desquels les habitants d'une ou plusieurs communes ont un droit acquis. " ; <br/>
<br/>
              4. Considérant, d'autre part, qu'aux termes de l'article L. 2241-1 du code général des collectivités territoriales : " Le conseil municipal délibère sur la gestion des biens et les opérations immobilières effectuées par la commune. (...) " ; <br/>
<br/>
              5. Considérant qu'il résulte de l'ensemble des dispositions précitées qu'en jugeant que le conseil municipal, qui est compétent pour délibérer sur l'aliénation de biens communaux ou sur la cession de droits réels afférents à de tels biens, ne pouvait pas organiser la mise à disposition des terrains communaux à vocation agricole de la commune dans le cadre d'un bail conclu avec la SAFER de Lozère suivi de la mise à bail des terrains, précédemment allotis, au profit des agriculteurs en activité résidant dans la commune au motif qu'il ne disposait pas du pouvoir d'aliéner de tels biens, la cour administrative d'appel a méconnu la portée de la loi du 10 juin 1793 et de l'article 542 du code civil et commis une erreur de droit ; que, sans qu'il soit besoin d'examiner les autres moyens de chacun des pourvois, la commune des Hermaux est fondée à demander l'annulation des arrêts attaqués ;<br/>
<br/>
              6. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de M. A... la somme globale de 2 000 euros à verser à la commune des Hermaux au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les deux arrêts de la cour administrative d'appel de Marseille du 22 mars 2012 sont annulés.<br/>
Article 2 : Les deux affaires sont renvoyées à la cour administrative d'appel de Marseille.<br/>
Article 3 : M. A... versera à la commune des Hermaux une somme globale de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la commune des Hermaux et à M. B... A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">135-02 COLLECTIVITÉS TERRITORIALES. COMMUNE. - BIENS COMMUNAUX - COMPÉTENCE DU CONSEIL MUNICIPAL POUR DÉLIBÉRER SUR LEUR ALIÉNATION OU LA CESSION DE DROITS RÉELS AFFÉRENTS À DE TELS BIENS - EXISTENCE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">135-02-02 COLLECTIVITÉS TERRITORIALES. COMMUNE. BIENS DE LA COMMUNE. - BIENS COMMUNAUX - COMPÉTENCE DU CONSEIL MUNICIPAL POUR DÉLIBÉRER SUR LEUR ALIÉNATION OU LA CESSION DE DROITS RÉELS AFFÉRENTS À DE TELS BIENS - EXISTENCE [RJ1].
</SCT>
<ANA ID="9A"> 135-02 Il résulte de l'ensemble des dispositions de la loi du 10 juin 1793, de l'article 542 du code civil et de l'article L. 2241-1 du code général des collectivités territoriales (CGCT) que le conseil municipal est compétent pour délibérer sur l'aliénation de biens communaux ou sur la cession de droits réels afférents à de tels biens et peut organiser la mise à disposition des terrains communaux à vocation agricole de la commune dans le cadre d'un bail.</ANA>
<ANA ID="9B"> 135-02-02 Il résulte de l'ensemble des dispositions de la loi du 10 juin 1793, de l'article 542 du code civil et de l'article L. 2241-1 du code général des collectivités territoriales (CGCT) que le conseil municipal est compétent pour délibérer sur l'aliénation de biens communaux ou sur la cession de droits réels afférents à de tels biens et peut organiser la mise à disposition des terrains communaux à vocation agricole de la commune dans le cadre d'un bail.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 4 août 1864, Sieurs Bellinet et autres, n° 34674, p. 723. Comp., s'agissant des biens d'une section de commune, CE, 11 mars 2005, M.,, n° 269941, T. pp. 730-755-757.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
