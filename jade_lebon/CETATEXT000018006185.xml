<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000018006185</ID>
<ANCIEN_ID>JG_L_2007_05_000000268230</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/18/00/61/CETATEXT000018006185.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 1ère sous-sections réunies, 30/05/2007, 268230</TITRE>
<DATE_DEC>2007-05-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>268230</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 1ère sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Martin</PRESIDENT>
<AVOCATS/>
<RAPPORTEUR>M. Olivier  Henrard</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Guyomar</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la requête, enregistrée le 2 juin 2004 au secrétariat du contentieux du Conseil d'Etat, présentée par M. Joss A, notaire à Paris, représenté par Me Stéphane B, ... ; M. A demande au Conseil d'État d'annuler pour excès de pouvoir le refus implicite opposé par le Premier ministre à sa demande d'abrogation du second alinéa de l'article 18 du décret du 28 décembre 1973 relatif à la discipline et au statut des officiers publics ou ministériels ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré présentée le 15 mars 2007 par M. A ; <br/>
<br/>
              Vu la Constitution du 4 octobre 1958, notamment son préambule ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et le premier protocole additionnel à cette convention ;<br/>
<br/>
              Vu le nouveau code de procédure civile ;<br/>
<br/>
              Vu l'ordonnance n°45-1418 du 28 juin 1945 relative à la discipline des notaires et de certains officiers ministériels ;<br/>
<br/>
              Vu le décret du 28 décembre 1973 relatif à la discipline et au statut des officiers publics ou ministériels ;<br/>
<br/>
              Vu le décret n° 83-1025 du 28 novembre 1983 concernant les relations entre l'administration et les usagers ;<br/>
<br/>
              Vu le décret n° 2004-836 du 20 août 2004 portant modification de la procédure civile ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Henrard, Maître des Requêtes,  <br/>
<br/>
<br/>
              - les conclusions de M. Mattias Guyomar, Commissaire du gouvernement ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant que M. A demande au Conseil d'État d'annuler pour excès de pouvoir le refus implicite opposé par le Premier ministre à sa demande d'abrogation, présentée le 27 avril 2004, du second alinéa de l'article 18 du décret du 28 décembre 1973 relatif à la discipline et au statut des officiers publics ou ministériels qui prévoit que, lorsque le tribunal de grande instance statue en matière de discipline des officiers publics ou ministériels : « Le jugement est exécutoire par provision sur minute s'il est contradictoire ou dès sa signification à l'officier public ou ministériel s'il est rendu par défaut » ;<br/>
<br/>
              Sur le moyen tiré de la violation des droits de la défense :<br/>
<br/>
              Considérant que le requérant soutient que l'absence, dans les dispositions du décret du 28 décembre 1973 dont l'abrogation était demandée, de la faculté, pour l'officier public ou ministériel qui fait l'objet d'une sanction disciplinaire, d'obtenir, du juge saisi en appel, l'arrêt de l'exécution provisoire de droit mise en oeuvre en application de ces dispositions, notamment lorsque cette mesure risque d'entraîner des conséquences manifestement excessives ou dans le cas d'une violation manifeste du principe du contradictoire par les premiers juges, méconnaît les droits de la défense, rappelés par les stipulations de l'article 6§1 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; que, toutefois, il ressort des pièces du dossier que, par décret du 20 août 2004 réformant la procédure civile, le pouvoir réglementaire a introduit à l'article 524 du nouveau code de procédure civile un sixième alinéa en vertu duquel le premier président de la cour d'appel peut désormais arrêter l'exécution provisoire de droit en cas de violation manifeste du principe du contradictoire, lorsqu'elle risque d'entraîner des conséquences manifestement excessives, ou encore lorsque le juge a méconnu l'obligation, prévue à l'article 12 du nouveau code de procédure civile, de trancher le litige conformément aux règles de droit qui lui sont applicables et de donner ou restituer leur exacte qualification aux faits et actes litigieux ; que ces dispositions sont applicables, conformément à l'article 749 du même code et à l'article 38 du décret du 28 décembre 1973, devant les juridictions de l'ordre judiciaire statuant en matière de discipline des officiers publics ou ministériels ; qu'il suit de là que, le motif d'illégalité allégué tiré de la méconnaissance des droits de la défense ayant, en tout état de cause, disparu, le moyen doit être écarté ;<br/>
<br/>
              Sur les autres moyens de la requête :<br/>
<br/>
              Considérant que les dispositions dont l'abrogation est demandée, qui confèrent l'exécution provisoire de droit à un jugement du tribunal de grande instance qui a établi la culpabilité d'un officier public ou ministériel et prononcé une sanction disciplinaire, sur le fondement de l'ordonnance du 28 juin 1945 relative à la discipline des notaires et de certains officiers ministériels et du décret contesté du 28 décembre 1973, n'ont ni pour objet, ni pour effet, d'infliger une telle sanction ; qu'il suit de là que les moyens tirés de la méconnaissance des principes de la présomption d'innocence, garanti notamment par l'article 9 de la Déclaration des droits de l'homme et du citoyen et le paragraphe 2 de l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, et de proportionnalité et de nécessité des peines, ne peuvent être utilement invoqués à l'encontre du refus d'abrogation attaqué ;<br/>
<br/>
              Considérant qu'aucune disposition de valeur législative, ni aucun principe général du droit, n'imposent que l'exercice de l'appel soit, d'une manière générale et en dehors des cas où la loi l'a prévu, comme en matière de procédure pénale, suspensif de l'exécution du jugement attaqué ; que, contrairement à ce que soutient le requérant, les dispositions dont l'abrogation est demandée ne sauraient en aucun cas être regardées comme présentant un caractère pénal ; qu'il suit de là que le moyen tiré de la méconnaissance d'un tel principe ne peut être utilement invoqué ;<br/>
<br/>
              Considérant que ni les stipulations du paragraphe 1 de l'article 6 de la convention européenne, ni aucun principe général du droit, ne consacrent l'existence d'une règle du double degré de juridiction qui s'imposerait au pouvoir réglementaire ; qu'il suit de là que le moyen tiré de la méconnaissance d'un tel principe ne peut être utilement invoqué ;<br/>
<br/>
              Considérant que l'atteinte aux biens d'un officier public ou ministériel, à la supposer constituée par une sanction disciplinaire prononcée en application de l'ordonnance du 28 juin 1945 et du décret du 28 décembre 1973, ne saurait en aucun cas résulter de l'exécution provisoire du jugement mais de la peine infligée, laquelle ne trouve pas son fondement dans les dispositions dont l'abrogation est demandée ; qu'il suit de là que le moyen tiré de la méconnaissance de l'article 1er du premier protocole additionnel à la convention européenne ne peut être utilement invoqué ;<br/>
<br/>
              Considérant enfin que, compte tenu des responsabilités particulières confiées aux notaires et aux autres officiers publics ou ministériels dans le cadre du service public de la justice et de la nécessité de préserver les intérêts, et notamment la sécurité juridique, des usagers de ce service public, le refus d'abrogation attaqué n'est pas entaché d'erreur manifeste d'appréciation ;<br/>
<br/>
              Considérant qu'il résulte de tout ce qui précède que M. A n'est pas fondé à demander l'annulation du refus implicite opposé par le Premier ministre à sa demande d'abrogation ;<br/>
<br/>
<br/>
<br/>
<br/>
              		D E C I D E :<br/>
              		--------------<br/>
<br/>
		Article 1er : La requête de M. A est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. Joss A et au garde des sceaux, ministre de la justice.<br/>
<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-04-03-07-06 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. PRINCIPES GÉNÉRAUX DU DROIT. PRINCIPES INTÉRESSANT L'ACTION ADMINISTRATIVE. OBLIGATION D'ABROGER UN RÈGLEMENT ILLÉGAL. - RECOURS CONTRE LE REFUS D'ABROGER UN ACTE RÉGLEMENTAIRE - MODIFICATION DE L'ÉTAT DU DROIT POSTÉRIEUREMENT À L'INTRODUCTION DE LA REQUÊTE - CONSÉQUENCE - APPRÉCIATION DU BIEN-FONDÉ DES MOYENS SOULEVÉS À L'ENCONTRE DU REFUS D'ABROGATION COMPTE TENU DE LA MODIFICATION INTERVENUE - EXISTENCE [RJ1] [RJ2].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">01-09-02-01 ACTES LÉGISLATIFS ET ADMINISTRATIFS. DISPARITION DE L'ACTE. ABROGATION. ABROGATION DES ACTES RÉGLEMENTAIRES. - RECOURS CONTRE LE REFUS D'ABROGER UN ACTE RÉGLEMENTAIRE - MODIFICATION DE L'ÉTAT DU DROIT POSTÉRIEUREMENT À L'INTRODUCTION DE LA REQUÊTE - CONSÉQUENCE - APPRÉCIATION DU BIEN-FONDÉ DES MOYENS SOULEVÉS À L'ENCONTRE DU REFUS D'ABROGATION COMPTE TENU DE LA MODIFICATION INTERVENUE - EXISTENCE [RJ1] [RJ2].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-07-01-04 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. QUESTIONS GÉNÉRALES. MOYENS. - MOYENS SOULEVÉS À L'APPUI D'UN RECOURS CONTRE LE REFUS D'ABROGER UN ACTE RÉGLEMENTAIRE - MODIFICATION DE L'ÉTAT DU DROIT POSTÉRIEUREMENT À L'INTRODUCTION DE LA REQUÊTE - CONSÉQUENCE - APPRÉCIATION DU BIEN-FONDÉ DES MOYENS COMPTE TENU DE LA MODIFICATION INTERVENUE - EXISTENCE [RJ1] [RJ2].
</SCT>
<ANA ID="9A"> 01-04-03-07-06 Le requérant soutient que l'absence, dans les dispositions du second alinéa de l'article 18 du décret n° 73-1202 du 28 décembre 1973 dont il avait demandé l'abrogation, de la faculté, pour l'officier public ou ministériel qui fait l'objet d'une sanction disciplinaire, d'obtenir du juge saisi en appel l'arrêt de l'exécution provisoire de droit mise en oeuvre en application de ces dispositions, notamment lorsque cette mesure risque d'entraîner des conséquences manifestement excessives ou dans le cas d'une violation manifeste du principe du contradictoire par les premiers juges, méconnaît les droits de la défense, rappelés par les stipulations de l'article 6 § 1 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales. Toutefois, postérieurement au refus d'abrogation litigieux, le pouvoir réglementaire a, par le décret n° 2004-836 du 20 août 2004 portant modification de la procédure civile, introduit à l'article 524 du nouveau code de procédure civile un sixième alinéa en vertu duquel le premier président de la cour d'appel peut désormais arrêter l'exécution provisoire de droit en cas de violation manifeste du principe du contradictoire, lorsqu'elle risque d'entraîner des conséquences manifestement excessives, ou encore lorsque le juge a méconnu l'obligation, prévue à l'article 12 du nouveau code de procédure civile, de trancher le litige conformément aux règles de droit qui lui sont applicables et de donner ou restituer leur exacte qualification aux faits et actes litigieux. Ces dispositions sont applicables, conformément à l'article 749 du même code et à l'article 38 du décret du 28 décembre 1973, devant les juridictions de l'ordre judiciaire statuant en matière de discipline des officiers publics ou ministériels. Dans ces conditions, le motif d'illégalité allégué ayant, en tout état de cause, disparu, le moyen tiré de la méconnaissance des droits de la défense doit être écarté.</ANA>
<ANA ID="9B"> 01-09-02-01 Le requérant soutient que l'absence, dans les dispositions du second alinéa de l'article 18 du décret n° 73-1202 du 28 décembre 1973 dont il avait demandé l'abrogation, de la faculté, pour l'officier public ou ministériel qui fait l'objet d'une sanction disciplinaire, d'obtenir du juge saisi en appel l'arrêt de l'exécution provisoire de droit mise en oeuvre en application de ces dispositions, notamment lorsque cette mesure risque d'entraîner des conséquences manifestement excessives ou dans le cas d'une violation manifeste du principe du contradictoire par les premiers juges, méconnaît les droits de la défense, rappelés par les stipulations de l'article 6 § 1 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales. Toutefois, postérieurement au refus d'abrogation litigieux, le pouvoir réglementaire a, par le décret n° 2004-836 du 20 août 2004 portant modification de la procédure civile, introduit à l'article 524 du nouveau code de procédure civile un sixième alinéa en vertu duquel le premier président de la cour d'appel peut désormais arrêter l'exécution provisoire de droit en cas de violation manifeste du principe du contradictoire, lorsqu'elle risque d'entraîner des conséquences manifestement excessives, ou encore lorsque le juge a méconnu l'obligation, prévue à l'article 12 du nouveau code de procédure civile, de trancher le litige conformément aux règles de droit qui lui sont applicables et de donner ou restituer leur exacte qualification aux faits et actes litigieux. Ces dispositions sont applicables, conformément à l'article 749 du même code et à l'article 38 du décret du 28 décembre 1973, devant les juridictions de l'ordre judiciaire statuant en matière de discipline des officiers publics ou ministériels. Dans ces conditions, le motif d'illégalité allégué ayant, en tout état de cause, disparu, le moyen tiré de la méconnaissance des droits de la défense doit être écarté.</ANA>
<ANA ID="9C"> 54-07-01-04 Le requérant soutient que l'absence, dans les dispositions du second alinéa de l'article 18 du décret n° 73-1202 du 28 décembre 1973 dont il avait demandé l'abrogation, de la faculté, pour l'officier public ou ministériel qui fait l'objet d'une sanction disciplinaire, d'obtenir du juge saisi en appel l'arrêt de l'exécution provisoire de droit mise en oeuvre en application de ces dispositions, notamment lorsque cette mesure risque d'entraîner des conséquences manifestement excessives ou dans le cas d'une violation manifeste du principe du contradictoire par les premiers juges, méconnaît les droits de la défense, rappelés par les stipulations de l'article 6 § 1 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales. Toutefois, postérieurement au refus d'abrogation litigieux, le pouvoir réglementaire a, par le décret n° 2004-836 du 20 août 2004 portant modification de la procédure civile, introduit à l'article 524 du nouveau code de procédure civile un sixième alinéa en vertu duquel le premier président de la cour d'appel peut désormais arrêter l'exécution provisoire de droit en cas de violation manifeste du principe du contradictoire, lorsqu'elle risque d'entraîner des conséquences manifestement excessives, ou encore lorsque le juge a méconnu l'obligation, prévue à l'article 12 du nouveau code de procédure civile, de trancher le litige conformément aux règles de droit qui lui sont applicables et de donner ou restituer leur exacte qualification aux faits et actes litigieux. Ces dispositions sont applicables, conformément à l'article 749 du même code et à l'article 38 du décret du 28 décembre 1973, devant les juridictions de l'ordre judiciaire statuant en matière de discipline des officiers publics ou ministériels. Dans ces conditions, le motif d'illégalité allégué ayant, en tout état de cause, disparu, le moyen tiré de la méconnaissance des droits de la défense doit être écarté.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. 24 janvier 2007, Groupement d'information et de soutien des immigrés (GISTI), n° 243976, à publier au recueil, feuilles roses p. 8-9-83.,,[RJ2] Rappr., dans le contentieux du permis de construire, 9 décembre 1994, SARL Séri, n° 116447, T. p. 1261 ; 2 février 2004, SCI La Fontaine de Villiers, n° 238315, T. p. 914.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
