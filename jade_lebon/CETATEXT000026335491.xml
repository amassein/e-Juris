<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026335491</ID>
<ANCIEN_ID>JG_L_2012_07_000000359496</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/33/54/CETATEXT000026335491.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème et 7ème sous-sections réunies, 23/07/2012, 359496</TITRE>
<DATE_DEC>2012-07-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>359496</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème et 7ème sous-sections réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. David Gaudillère</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CESSR:2012:359496.20120723</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le jugement n° 1200921 du 15 mai 2012, enregistré le 18 mai 2012 au secrétariat du contentieux du Conseil d'Etat, par lequel le tribunal administratif de Lyon, avant de statuer sur la demande de M. B...A...tendant, d'une part, à l'annulation pour excès de pouvoir des décisions du 18 janvier 2012 par lesquelles le préfet du Rhône lui a refusé la délivrance d'un titre de séjour, l'a obligé à quitter le territoire français dans un délai de trente jours, l'a astreint à se présenter une fois par semaine auprès des services de police afin de justifier de ses diligences en vue de son départ et a fixé le pays à destination duquel il est susceptible d'être renvoyé d'office, d'autre part, à ce qu'il soit enjoint au préfet du Rhône de lui délivrer un titre de séjour temporaire " salarié " dans un délai d'un mois, sous astreinte de 100 euros par jour de retard, a décidé, par application de l'article L. 113-1 du code de justice administrative, de transmettre le dossier de cette demande au Conseil d'Etat, en soumettant à son examen les questions suivantes :<br/>
<br/>
              1°) L'astreinte dont peut être assorti le délai de départ volontaire pour exécuter l'obligation faite à un étranger de quitter le territoire français constitue-t-elle une mesure de police distincte de la décision portant obligation de quitter le territoire français et de celle accordant à l'intéressé un délai de départ volontaire, et doit-elle, à ce titre, être motivée en application de la loi du 11 juillet 1979 '<br/>
<br/>
              2°) Dans l'affirmative, cette motivation peut-elle se limiter, pour l'autorité administrative, à mentionner la base légale de l'astreinte et à faire état de l'existence de décisions obligeant l'étranger à quitter le territoire français et lui accordant un délai de départ volontaire, ou doit-elle, en outre, faire état des circonstances particulières tirées de la situation de l'étranger pour lesquelles elle estime nécessaire d'édicter une telle mesure, au regard notamment du risque que l'intéressé se soustraie à l'exécution de la mesure d'éloignement prise à son encontre '<br/>
<br/>
              3°) Quel degré du contrôle appartient-il au juge administratif d'exercer tant sur le principe même de cette décision que sur les modalités qui ont été fixées, telles que la désignation du service auprès duquel l'étranger doit se présenter ou la fréquence de ces présentations '<br/>
<br/>
              ...........................................................................<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la directive 2008/115/CE du Parlement européen et du Conseil du 16 décembre 2008 ;<br/>
<br/>
              Vu le code de l'entrée et du séjour des étrangers et du droit d'asile modifié notamment par la loi n° 2011-672 du 16 juin 2011 ;<br/>
<br/>
              Vu la loi n° 79-587 du 11 juillet 1979 ;<br/>
<br/>
              Vu le décret n° 2011-820 du 8 juillet 2011 ;<br/>
<br/>
              Vu le code de justice administrative, notamment son article L. 113-1 ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. David Gaudillère, Auditeur,<br/>
<br/>
              - les conclusions de Mme Béatrice Bourgeois-Machureau, Rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              REND L'AVIS SUIVANT :<br/>
<br/>
<br/>
              1. La loi du 16 juin 2011 relative à l'immigration, à l'intégration et à la nationalité a notamment procédé à la transposition, dans l'ordre juridique interne, des dispositions de la directive du 16 décembre 2008 relative aux normes et procédures communes applicables dans les Etats membres au retour des ressortissants de pays tiers en séjour irrégulier, dite directive " retour ", et a modifié les dispositions de l'article L. 511-1 du code de l'entrée et du séjour des étrangers et du droit d'asile, qui définissent les conditions dans lesquelles les étrangers non ressortissants d'un Etat membre de l'Union européenne, d'un Etat partie à l'accord sur l'Espace économique européen ou de la Confédération Suisse et qui ne sont pas membres de la famille de ces ressortissants, peuvent faire l'objet d'une décision les obligeant à quitter le territoire français.<br/>
<br/>
              En vertu du II de l'article L. 511-1, une décision portant obligation de quitter le territoire français est désormais assortie, en principe, d'un délai " de départ volontaire " de trente jours, permettant à l'étranger de définir lui-même les conditions de son départ vers le pays d'accueil. L'autorité administrative peut décider d'accorder, à titre exceptionnel, un délai de départ volontaire supérieur à trente jours. Elle peut également décider, par exception et en prenant  une décision motivée, que l'étranger est obligé de quitter sans délai le territoire français si son comportement constitue une menace pour l'ordre public, s'il s'est vu refuser la délivrance ou le renouvellement de son titre de séjour, de son récépissé de demande de carte de séjour ou de son autorisation provisoire de séjour au motif que sa demande était manifestement infondée ou frauduleuse, ou si, dans une série de cas définis par la loi, le risque qu'il se soustraie à l'obligation de quitter le territoire français est regardé comme établi. L'autorité administrative peut notamment faire usage de cette dernière faculté lorsque le motif apparaît au cours du délai de départ volontaire accordé précédemment.<br/>
<br/>
              Lorsque l'étranger dispose d'un délai de départ volontaire, il peut, en application de l'article L. 513-4 du code de l'entrée et du séjour des étrangers et du droit d'asile, être soumis à une obligation de présentation. Aux termes de ces dispositions : " L'étranger auquel un délai de départ volontaire a été accordé en application du II de l'article L. 511-1 peut, dès la notification de l'obligation de quitter le territoire français, être astreint à se présenter à l'autorité administrative ou aux services de police ou aux unités de gendarmerie pour y indiquer ses diligences dans la préparation de son départ. / Un décret en Conseil d'Etat prévoit les modalités d'application du présent article ".<br/>
<br/>
              L'autorité administrative compétente pour astreindre un étranger à cette obligation de présentation, est, en application de l'article R. 513-2,  le préfet de département et, à Paris, le préfet de police. Aux termes de l'article R. 513-3 du même code, l'autorité administrative désigne le service auprès duquel l'étranger doit effectuer les présentations prescrites et fixe leur fréquence qui ne peut excéder trois présentations par semaine. L'étranger peut être tenu de remettre à ce service l'original de son passeport et de tout autre document d'identité ou de voyage en sa possession en échange d'un récépissé valant justification d'identité sur lequel est portée la mention du délai accordé pour son départ.<br/>
<br/>
              Enfin, aux termes de l'article L. 512-4 du même code : " Si l'obligation de quitter le territoire français est annulée, il est immédiatement mis fin aux mesures de surveillance prévues aux articles L. 513-4 (...) et l'étranger est muni d'une autorisation provisoire de séjour jusqu'à ce que l'autorité administrative ait à nouveau statué sur son cas (...) ".<br/>
<br/>
              2. Il ressort des termes de l'article L. 513-4 du code de l'entrée et du séjour des étrangers et du droit d'asile que l'autorité administrative a la faculté d'imposer une obligation de présentation à tout étranger s'étant vu accorder un délai de départ volontaire et que cette mesure ne se confond ni avec l'obligation de quitter le territoire français, ni avec la décision accordant un délai de départ volontaire. En conséquence, il est loisible aux intéressés de contester devant le juge la légalité de la décision prise sur le fondement de l'article L. 513-4. <br/>
<br/>
              3. Si l'obligation de présentation à laquelle un étranger est susceptible d'être astreint sur le fondement de l'article L. 513-4 a, ainsi qu'il vient d'être dit, le caractère d'une décision distincte de l'obligation de quitter le territoire français, cette décision, qui tend à assurer que l'étranger accomplit les diligences nécessaires à son départ dans le délai qui lui est imparti, concourt à la mise en oeuvre de l'obligation de quitter le territoire français. <br/>
<br/>
              Dans ces conditions, si l'article 1er de la loi du 11 juillet 1979 relative à la motivation des actes administratifs et à l'amélioration des relations entre l'administration et le public impose que cette décision soit motivée au titre des mesures de police, cette motivation peut, outre la référence à l'article L. 513-4, se confondre avec celle de l'obligation de quitter le territoire français assortie d'un délai de départ volontaire. <br/>
<br/>
              4. Au regard du pouvoir d'appréciation dont dispose, aux termes de la loi, l'autorité administrative pour apprécier la nécessité d'imposer une obligation de présentation sur le fondement de l'article L. 513-4, il appartient au juge de l'excès de pouvoir, saisi d'un moyen en ce sens, de vérifier que l'administration n'a pas commis d'erreur manifeste tant dans sa décision de recourir à cette mesure que dans le choix des modalités de celle-ci.<br/>
<br/>
<br/>
<br/>
<br/>
              Le présent avis sera notifié au tribunal administratif de Lyon, à M. B...A...et au ministre de l'intérieur.<br/>
<br/>
              Il sera publié au Journal officiel de la République française.<br/>
<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-03-01-02-01-01-01 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - FORME ET PROCÉDURE. QUESTIONS GÉNÉRALES. MOTIVATION. MOTIVATION OBLIGATOIRE. MOTIVATION OBLIGATOIRE EN VERTU DES ARTICLES 1 ET 2 DE LA LOI DU 11 JUILLET 1979. DÉCISION RESTREIGNANT L'EXERCICE DES LIBERTÉS PUBLIQUES OU, DE MANIÈRE GÉNÉRALE, CONSTITUANT UNE MESURE DE POLICE. - OBLIGATION DE PRÉSENTATION IMPOSÉE À UN ÉTRANGER S'ÉTANT VU ACCORDER UN DÉLAI DE DÉPART VOLONTAIRE (ART. L. 513-4 DU CESEDA) - MODALITÉS - MOTIVATION POUVANT SE CONFONDRE AVEC CELLE DE L'OQTF.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">335-03-03 ÉTRANGERS. OBLIGATION DE QUITTER LE TERRITOIRE FRANÇAIS (OQTF) ET RECONDUITE À LA FRONTIÈRE. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. - OBLIGATION DE PRÉSENTATION IMPOSÉE À UN ÉTRANGER S'ÉTANT VU ACCORDER UN DÉLAI DE DÉPART VOLONTAIRE (ART. L. 513-4 DU CESEDA) - 1) DÉCISION DISTINCTE SUSCEPTIBLE DE RECOURS - EXISTENCE - 2) OBLIGATION DE MOTIVATION (ART. 1ER DE LA LOI DU 11 JUILLET 1979) - EXISTENCE - MODALITÉS - MOTIVATION POUVANT SE CONFONDRE AVEC CELLE DE L'OQTF - 3) DEGRÉ DE CONTRÔLE DU JUGE - CONTRÔLE DE L'ERREUR MANIFESTE D'APPRÉCIATION, TANT SUR LE PRINCIPE QUE SUR LES MODALITÉS.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-01-01-01 PROCÉDURE. INTRODUCTION DE L'INSTANCE. DÉCISIONS POUVANT OU NON FAIRE L'OBJET D'UN RECOURS. ACTES CONSTITUANT DES DÉCISIONS SUSCEPTIBLES DE RECOURS. - OBLIGATION DE PRÉSENTATION IMPOSÉE À UN ÉTRANGER S'ÉTANT VU ACCORDER UN DÉLAI DE DÉPART VOLONTAIRE (ART. L. 513-4 DU CESEDA) -  DÉCISION DISTINCTE SUSCEPTIBLE DE RECOURS - EXISTENCE.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">54-07-02-04 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. CONTRÔLE DU JUGE DE L'EXCÈS DE POUVOIR. APPRÉCIATIONS SOUMISES À UN CONTRÔLE RESTREINT. - OBLIGATION DE PRÉSENTATION IMPOSÉE À UN ÉTRANGER S'ÉTANT VU ACCORDER UN DÉLAI DE DÉPART VOLONTAIRE (ART. L. 513-4 DU CESEDA) - CONTRÔLE DE L'ERREUR MANIFESTE D'APPRÉCIATION, TANT SUR LE PRINCIPE QUE SUR LES MODALITÉS.
</SCT>
<ANA ID="9A"> 01-03-01-02-01-01-01 Bien que distincte, l'obligation de présentation à laquelle un étranger est susceptible d'être astreint sur le fondement de l'article L. 513-4 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA) est une décision concourrant à la mise en oeuvre de l'obligation de quitter le territoire français (OQTF). Dans ces conditions, si l'article 1er de la loi n° 79-587 du 11 juillet 1979 relative à la motivation des actes administratifs et à l'amélioration des relations entre l'administration et le public impose que cette décision soit motivée au titre des mesures de police, cette motivation peut, outre la référence à l'article L. 513-4, se confondre avec celle de l'obligation de quitter le territoire français assortie d'un délai de départ volontaire.</ANA>
<ANA ID="9B"> 335-03-03 1) Il ressort des termes de l'article L. 513-4 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA) que l'autorité administrative a la faculté d'imposer une obligation de présentation à tout étranger s'étant vu accorder un délai de départ volontaire et que cette mesure, qui ne se confond ni avec l'obligation de quitter le territoire français (OQTF), ni avec la décision accordant un délai de départ volontaire, est une décision distincte, susceptible de recours.,,2) Bien que distincte, l'obligation de présentation à laquelle un étranger est susceptible d'être astreint sur le fondement de l'article L. 513-4 est une décision concourant à la mise en oeuvre de l'OQTF. Dans ces conditions, si l'article 1er de la loi n° 79-587 du 11 juillet 1979 relative à la motivation des actes administratifs et à l'amélioration des relations entre l'administration et le public impose que cette décision soit motivée au titre des mesures de police, cette motivation peut, outre la référence à l'article L. 513-4, se confondre avec celle de l'OQTF assortie d'un délai de départ volontaire.,,3) Au regard du pouvoir d'appréciation dont dispose, aux termes de la loi, l'autorité administrative pour apprécier la nécessité d'imposer une obligation de présentation sur le fondement de l'article L. 513-4, il appartient au juge de l'excès de pouvoir, saisi d'un moyen en ce sens, de vérifier que l'administration n'a pas commis d'erreur manifeste tant dans sa décision de recourir à cette mesure que dans le choix des modalités de celle-ci.</ANA>
<ANA ID="9C"> 54-01-01-01 Il ressort des termes de l'article L. 513-4 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA) que l'autorité administrative a la faculté d'imposer une obligation de présentation à tout étranger s'étant vu accorder un délai de départ volontaire et que cette mesure, qui ne se confond ni avec l'obligation de quitter le territoire français, ni avec la décision accordant un délai de départ volontaire, est une décision distincte, susceptible de recours.</ANA>
<ANA ID="9D"> 54-07-02-04 Au regard du pouvoir d'appréciation dont dispose, aux termes de la loi, l'autorité administrative pour apprécier la nécessité d'imposer, sur le fondement de l'article L. 513-4 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA), une obligation de présentation à un étranger s'étant vu accorder un délai de départ volontaire, s'il appartient au juge de l'excès de pouvoir, saisi d'un moyen en ce sens, de vérifier que l'administration n'a pas commis d'erreur manifeste tant dans sa décision de recourir à cette mesure que dans le choix des modalités de celle-ci.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
