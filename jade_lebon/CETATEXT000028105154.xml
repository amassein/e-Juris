<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028105154</ID>
<ANCIEN_ID>JG_L_2013_10_000000366508</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/10/51/CETATEXT000028105154.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 1ère sous-sections réunies, 18/10/2013, 366508</TITRE>
<DATE_DEC>2013-10-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>366508</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 1ère sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>BALAT ; SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS</AVOCATS>
<RAPPORTEUR>Mme Sophie Roussel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier de Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:366508.20131018</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu 1°, sous le n° 366508, le pourvoi, enregistré le 1er  mars 2013 au secrétariat du contentieux du Conseil d'Etat, présenté pour l'association Centre national d'information indépendante sur les déchets, dont le siège est 21, rue Alexandre Dumas à Paris (75011) ; l'association demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'ordonnance n° 1300159 du 14 février 2013 par laquelle le juge des référés du tribunal administratif de Caen, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, a rejeté sa demande tendant à la suspension de l'exécution de l'arrêté du 12 juillet 2011 par lequel le préfet de l'Orne a fixé les conditions d'exploitation du centre de stockage de déchets non dangereux ultimes et du centre de tri de déchets industriels banals, de métaux ferreux et non ferreux et de déchets d'équipement électroniques et électriques dont ce même tribunal a autorisé, par jugement du 18 février 2011, l'exploitation par la société Guy Dauphin Environnement sur le territoire de la commune de Nonant-le-Pin, jusqu'à ce qu'il soit statué au fond sur la légalité de cet arrêté ; <br/>
<br/>
              2°) statuant en référé, de faire droit à sa demande de suspension ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
              Vu 2°, sous le n° 366509, le pourvoi enregistré le 1er mars 2013 au secrétariat du contentieux du Conseil d'Etat, présenté pour l'association Nonant Environnement, dont le siège est 11, route de Sées à Nonant-le-Pin (61240) ; l'association demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'ordonnance n° 1300090 du 14 février 2013 par laquelle le même juge des référés a rejeté sa demande tendant à la suspension de l'exécution de l'arrêté du 12 juillet 2011 du préfet de l'Orne visé au 1°, jusqu'à ce qu'il soit statué au fond sur la légalité de cet arrêté ; <br/>
<br/>
              2°) statuant en référé, de faire droit à sa demande de suspension ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu 3°, sous le n° 366510, le pourvoi enregistré le 1er mars 2013 au secrétariat du contentieux du Conseil d'Etat, présenté pour l'association France Nature Environnement, dont le siège est 57, rue Cuvier à Paris (75005) ; l'association demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'ordonnance n° 1300218 du 14 février 2013 par laquelle le même juge des référés a rejeté sa demande tendant à la suspension de l'exécution de l'arrêté du 12 juillet 2011 du préfet de l'Orne visé au 1°, jusqu'à ce qu'il soit statué au fond sur la légalité de cet arrêté ; <br/>
<br/>
              2°) statuant en référé, de faire droit à sa demande de suspension ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu le code de l'environnement ; <br/>
<br/>
              Vu l'arrêté du 9 septembre 1997 du ministre de l'aménagement du territoire et de l'environnement relatif aux installations de stockage de déchets non dangereux ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie Roussel, Auditeur,  <br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat des associations Centre national d'information indépendante sur les déchets, Nonant Environnement et France Nature Environnement, et à Me Balat, avocat de la société Guy Dauphin Environnement ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis au juge des référés que par un jugement du 18 février 2011, le tribunal administratif de Caen a, d'une part, annulé le refus opposé par le préfet de l'Orne à la demande d'autorisation présentée par la société Guy Dauphin Environnement au titre de la législation sur les installations classées pour la protection de l'environnement, en vue d'exploiter un centre de stockage de déchets non dangereux ultimes et un centre de tri de déchets industriels banals, de métaux ferreux et non ferreux et de déchets d'équipement électroniques et électriques sur le territoire de la commune de Nonant-le-Pin, et a, d'autre part, délivré à la société l'autorisation sollicitée ; que par ce même jugement, le tribunal administratif a renvoyé la société Guy Dauphin Environnement devant le préfet de l'Orne afin que soient fixées les prescriptions applicables à ces installations ; que, par un arrêté du 12 juillet 2011, le préfet a fixé ces conditions ; que, par trois demandes distinctes, les associations Centre national d'information indépendante sur les déchets, Nonant Environnement et France Nature Environnement ont demandé la suspension de l'exécution de cet arrêté préfectoral sur le fondement des dispositions de l'article L. 123-16 du code de l'environnement ; que, par les trois ordonnances attaquées, le juge des référés du tribunal administratif de Caen a rejeté leurs demandes ; que les trois associations se pourvoient en cassation contre ces ordonnances ; qu'il y a lieu de joindre leurs pourvois pour statuer par une seule décision ; <br/>
<br/>
              2. Considérant, d'une part, qu'aux termes du premier alinéa de l'article           L. 123-12 du code de l'environnement, auquel renvoie l'article L. 554-12 du code de justice administrative, dans leur version applicable en l'espèce et dont les dispositions sont désormais reprises au premier alinéa de l'article L. 123-16 du code de l'environnement : " Le juge administratif des référés, saisi d'une demande de suspension d'une décision prise après des conclusions défavorables du commissaire enquêteur ou de la commission d'enquête, fait droit à cette demande si elle comporte un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de celle-ci. " ;<br/>
<br/>
              3. Considérant, d'autre part, qu'aux termes de l'article L. 512-1 du code de l'environnement : " Sont soumises à autorisation préfectorale les installations qui présentent de graves dangers ou inconvénients pour les intérêts visés à l'article L. 511-1. / L'autorisation ne peut être accordée que si ces dangers ou inconvénients peuvent être prévenus par des mesures que spécifie l'arrêté préfectoral. (...) " ; qu'aux termes de l'article L. 512-2 de ce code : " L'autorisation prévue à l'article L. 512-1 est accordée par le préfet, après enquête publique réalisée conformément au chapitre III du titre II du livre Ier du présent code relative aux incidences éventuelles du projet sur les intérêts mentionnés à l'article L. 511-1 et après avis des conseils municipaux intéressés. (...) " ; qu'aux termes de l'article L. 512-3 du même code : " Les conditions d'installation et d'exploitation jugées indispensables pour la protection des intérêts mentionnés à l'article L. 511-1, les moyens de suivi, de surveillance, d'analyse et de mesure et les moyens d'intervention en cas de sinistre sont fixés par l'arrêté d'autorisation et, éventuellement, par des arrêtés complémentaires pris postérieurement à cette autorisation. " ; <br/>
<br/>
              4. Considérant qu'il résulte de la combinaison de ces dispositions que l'autorisation d'exploiter une installation classée pour la protection de l'environnement est indissociable des prescriptions qui l'accompagnent, l'installation projetée ne pouvant, en l'absence de ces prescriptions, fonctionner dans des conditions permettant le respect des intérêts visés à l'article L. 511-1 du code de l'environnement ; que dès lors, pour l'application des dispositions de l'article L. 123-12 de ce code qui ne soumettent pas le prononcé d'une mesure de suspension par le juge des référés à une condition d'urgence, l'arrêté par lequel l'autorité administrative fixe les prescriptions initiales applicables à une installation classée pour la protection de l'environnement doit être regardé comme une décision soumise à une enquête publique préalable, alors même que le préfet n'est pas tenu de procéder à une nouvelle enquête publique pour édicter ces prescriptions, dès lors que celle-ci a été réalisée dans le cadre de l'instruction de la demande d'autorisation ; <br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède qu'en jugeant que l'arrêté du 12 juillet 2011 par lequel le préfet a fixé les prescriptions initiales applicables aux installations autorisées par le jugement du 18 février 2011 ne figure pas au nombre des décisions soumises à une enquête publique préalable au sens de l'article L. 123-12 du code de l'environnement, alors que, ainsi qu'il a été dit au point 1, l'arrêté litigieux fixait les prescriptions initiales applicables à l'exploitation autorisée par le jugement du 18 février 2011, le juge des référés du tribunal administratif de Caen a commis une erreur de droit ; que, par suite, sans qu'il soit besoin d'examiner l'autre moyen des pourvois, les trois associations requérantes sont fondées à demander l'annulation des ordonnances attaquées ; <br/>
<br/>
              6. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de statuer sur les demandes en référé en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              7. Considérant qu'il ressort des pièces du dossier que le commissaire enquêteur a rendu un avis défavorable sur le projet de centre de stockage de déchets ; que s'il a rendu un avis favorable sur le projet de centre de tri de déchets, il a néanmoins assorti cet avis de recommandations consistant à aménager un rond-point sur la route départementale 438 pour améliorer l'accès au site et à recentrer le bâtiment du centre de tri plus au sud-est afin de l'éloigner des voies ferrées ; qu'eu égard à la portée de cette recommandation, relative à l'implantation du projet, cet avis doit être regardé comme défavorable ; <br/>
<br/>
              8. Considérant toutefois, d'une part, que dès lors que le préfet ne saurait, sans méconnaître l'autorité de la chose jugée par le tribunal administratif de Caen, remettre en cause l'autorisation délivrée par le jugement du 18 février 2011, les moyens tirés du caractère incomplet du dossier de demande d'autorisation, de l'insuffisance de l'étude d'impact, de la nullité de l'enquête publique, de la méconnaissance de l'article L. 512-2 du code de l'environnement relatif aux garanties techniques et financières exigées des exploitants d'installations classées pour la protection de l'environnement, de la violation du principe de gestion des déchets à proximité de leur lieu de collecte issu du 4° de l'article L. 541-1 de ce code, de l'incompatibilité des deux projets avec le plan départemental de gestion des déchets ménagers et assimilés de l'Orne et avec le schéma directeur d'aménagement et de gestion des eaux du bassin de Seine-Normandie, par lesquels les associations contestent en réalité l'autorisation délivrée par le jugement du 18 février 2011 et non le caractère suffisant des prescriptions édictées par l'arrêté litigieux, sont inopérants ;  <br/>
<br/>
              9. Considérant, d'autre part, que les moyens tirés de la méconnaissance, à plusieurs titres, de l'arrêté du 9 septembre 1997 relatif aux installations de stockage de déchets non dangereux et de l'erreur manifeste d'appréciation commise par le préfet quant au caractère suffisant des prescriptions édictées au regard des inconvénients générés par les deux installations en cause ne sont pas de nature, en l'état de l'instruction, à créer un doute sérieux quant à la légalité de l'arrêté du 12 juillet 2011 ; <br/>
<br/>
              10. Considérant qu'il résulte de ce qui précède que, sans qu'il soit besoin de se prononcer sur les fins de non-recevoir opposées par la société Guy Dauphin Environnement et par le préfet de l'Orne, les conclusions à fin de suspension présentées par les associations requérantes ne peuvent qu'être rejetées ; <br/>
<br/>
              11. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de chacune des associations Centre national d'information indépendante sur les déchets, Nonant Environnement et France Nature Environnement la somme de 1 000 euros à verser à la société Guy Dauphin Environnement au titre des dispositions de l'article L. 761-1 du code de justice administrative ; que ces dispositions font obstacle à ce qu'il soit fait droit aux conclusions présentées par les associations requérantes au même titre ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les ordonnances nos 1300159, 1300090 et 1300218 du 14 février 2013 du juge des référés du tribunal administratif de Caen sont annulées. <br/>
<br/>
Article 2 : Les demandes des associations Centre national d'information indépendante sur les déchets, Nonant Environnement et France Nature Environnement devant le juge des référés de ce tribunal et le surplus des conclusions de leurs pourvois sont rejetés. <br/>
<br/>
Article 3 : Les associations Centre national d'information indépendante sur les déchets, Nonant Environnement et France Nature Environnement verseront chacune une somme de 1 000 euros à la société Guy Dauphin Environnement au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
Article 4 : La présente décision sera notifiée aux associations Centre national d'information indépendante sur les déchets, Nonant Environnement et France Nature Environnement, à la société Guy Dauphin Environnement et au ministre de l'écologie, du développement durable et de l'énergie<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">44-006-05-07 NATURE ET ENVIRONNEMENT. - ARRÊTÉ FIXANT LES PRESCRIPTIONS INITIALES APPLICABLES À UNE ICPE (ART. L. 512-3 DU CODE DE L'ENVIRONNEMENT) - CARACTÈRE INDISSOCIABLE DE L'AUTORISATION D'EXPLOITER PRISE APRÈS ENQUÊTE PUBLIQUE - EXISTENCE - CONSÉQUENCE - NOTION DE DÉCISION SOUMISE À ENQUÊTE PUBLIQUE PRÉALABLE POUR L'APPLICATION DE L'ANCIEN ARTICLE L. 123-12, DEVENU L. 123-16, DU MÊME CODE - INCLUSION.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-03 PROCÉDURE. PROCÉDURES DE RÉFÉRÉ AUTRES QUE CELLES INSTITUÉES PAR LA LOI DU 30 JUIN 2000. - PROCÉDURE ENGAGÉE SUR LE FONDEMENT DE L'ARTICLE L. 123-12, DEVENU L. 123-16, DU CODE DE L'ENVIRONNEMENT - CHAMP D'APPLICATION - ARRÊTÉ FIXANT LES PRESCRIPTIONS INITIALES APPLICABLES À UNE ICPE (ART. L. 512-3 DU CODE DE L'ENVIRONNEMENT) - CARACTÈRE INDISSOCIABLE DE L'AUTORISATION D'EXPLOITER PRISE APRÈS ENQUÊTE PUBLIQUE - EXISTENCE - CONSÉQUENCE - INCLUSION.
</SCT>
<ANA ID="9A"> 44-006-05-07 Il résulte de la combinaison des articles L. 512-1 à L. 512-3 du code de l'environnement que l'autorisation d'exploiter une installation classée pour la protection de l'environnement (ICPE) est indissociable des prescriptions qui l'accompagnent, l'installation projetée ne pouvant, en l'absence de ces prescriptions, fonctionner dans des conditions permettant le respect des intérêts visés à l'article L. 511-1 du même code. Dès lors, pour l'application des dispositions de l'ancien article L. 123-12, devenu L. 123-16, de ce code, qui ne soumettent pas le prononcé d'une mesure de suspension par le juge des référés à une condition d'urgence, l'arrêté par lequel l'autorité administrative fixe les prescriptions initiales applicables à une ICPE doit être regardé comme une décision soumise à une enquête publique préalable, alors même que le préfet n'est pas tenu de procéder à une nouvelle enquête publique pour édicter ces prescriptions, dès lors que celle-ci a été réalisée dans le cadre de l'instruction de la demande d'autorisation.</ANA>
<ANA ID="9B"> 54-03 Il résulte de la combinaison des articles L. 512-1 à L. 512-3 du code de l'environnement que l'autorisation d'exploiter une installation classée pour la protection de l'environnement (ICPE) est indissociable des prescriptions qui l'accompagnent, l'installation projetée ne pouvant, en l'absence de ces prescriptions, fonctionner dans des conditions permettant le respect des intérêts visés à l'article L. 511-1 du même code. Dès lors, pour l'application des dispositions de l'ancien article L. 123-12, devenu L. 123-16, de ce code, qui ne soumettent pas le prononcé d'une mesure de suspension par le juge des référés à une condition d'urgence, l'arrêté par lequel l'autorité administrative fixe les prescriptions initiales applicables à une ICPE doit être regardé comme une décision soumise à une enquête publique préalable, alors même que le préfet n'est pas tenu de procéder à une nouvelle enquête publique pour édicter ces prescriptions, dès lors que celle-ci a été réalisée dans le cadre de l'instruction de la demande d'autorisation.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
