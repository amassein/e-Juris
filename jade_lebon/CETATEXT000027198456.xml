<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027198456</ID>
<ANCIEN_ID>JG_L_2013_03_000000352551</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/19/84/CETATEXT000027198456.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 1ère sous-sections réunies, 20/03/2013, 352551</TITRE>
<DATE_DEC>2013-03-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>352551</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 1ère sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Samuel Gillis</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:352551.20130320</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 9 septembre 2011 au secrétariat du contentieux du Conseil d'Etat, présenté par le ministre de l'écologie, du développement durable, des transports et du logement ; le ministre demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 09MA01121 du 4 juillet 2011 par lequel la cour administrative d'appel de Marseille a annulé le jugement n° 0701744 du 21 novembre 2008 du tribunal administratif de Nîmes et l'arrêté du 12 avril 2007 par lequel le préfet du Gard a prescrit à M. A...B...de consigner la somme de 35 000 euros correspondant au coût des travaux de décontamination du sous-sol pollué par des hydrocarbures à proximité immédiate de la station service qu'il exploitait ;<br/>
<br/>
               2°) réglant l'affaire au fond, de rejeter l'appel de M. B...;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'environnement ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Samuel Gillis, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 514-1 du code de l'environnement : " Indépendamment des poursuites pénales qui peuvent être exercées, et lorsqu'un inspecteur des installations classées ou un expert désigné par le ministre chargé des installations classées a constaté l'inobservation des conditions imposées à l'exploitant d'une installation classée, le préfet met en demeure ce dernier de satisfaire à ces conditions dans un délai déterminé. Si, à l'expiration du délai fixé pour l'exécution, l'exploitant n'a pas obtempéré à cette injonction, le préfet peut :/ 1° Obliger l'exploitant à consigner entre les mains d'un comptable public une somme répondant du montant des travaux à réaliser, laquelle sera restituée à l'exploitant au fur et à mesure de l'exécution des mesures prescrites ; (...) " ; <br/>
<br/>
              2. Considérant que l'illégalité d'un arrêté de mise en demeure, pris sur le fondement de ces dispositions, peut utilement être invoquée, par la voie de l'exception, à l'encontre de l'arrêté de consignation pris à sa suite ; que, toutefois, une telle exception d'illégalité n'est recevable que si cet arrêté, qui est dépourvu de caractère réglementaire, n'était pas devenu définitif à la date à laquelle elle est soulevée ;<br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par arrêté du 19 juillet 2006, le préfet du Gard a prescrit à M. A...B..., en sa qualité de dernier exploitant d'une station service de distribution de carburants située à Beauvoisin, la réalisation de travaux de dépollution du sous sol contaminé par des hydrocarbures ; qu'après que l'inspecteur des installations classées eut constaté que ces travaux n'avaient pas été entrepris et relevé que M. B...n'entendait pas les réaliser, le préfet a, en application de l'article L. 514-1 du code de l'environnement, mis en demeure l'exploitant, le 20 décembre 2006, de transmettre dans les deux mois, un justificatif de l'engagement des travaux ; qu'au vu d'un nouveau rapport de l'inspection des installations classées constatant que les travaux n'avaient pas été entrepris alors que le délai fixé pour leur engagement était expiré, le préfet a pris, le 12 avril 2007 un arrêté de consignation de la somme de 35 000 euros répondant du montant de ces travaux ; que, par un jugement du 21 novembre 2008, le tribunal administratif de Nîmes a rejeté la demande de M. B...tendant à l'annulation de cet arrêté, en jugeant notamment que le caractère définitif de l'arrêté de mise en demeure du 20 décembre 2006 faisait obstacle à sa contestation par voie d'exception ; que, pour annuler, par l'arrêt attaqué, ce jugement ainsi que l'arrêté du 12 avril 2007, la cour administrative d'appel de Marseille a jugé illégal l'arrêté de mise en demeure qui, selon elle, n'était pas définitif ;<br/>
<br/>
              4. Considérant qu'il ressort du dossier soumis à la cour que l'appréciation portée par les premiers juges sur le caractère définitif de l'arrêté de mise du 20 décembre 2006 n'était pas contestée devant elle ; qu'en jugeant le contraire, la cour a dénaturé les pièces du dossier ; qu'au demeurant, elle a commis une erreur de droit dès lors qu'il n'appartient pas au juge d'appel, devant lequel l'appelant ne conteste pas l'irrecevabilité opposée en première instance au moyen d'exception d'illégalité qu'il avait soulevé à l'appui de ses conclusions, de rechercher d'office si cette irrecevabilité a été opposée à bon droit par les premiers juges ; que, par suite, et sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, le ministre est fondé à demander l'annulation de l'arrêt de la cour administrative d'appel de Marseille du 4 juillet 2011 ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Marseille du 4 juillet 2011 est annulé.<br/>
      Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Marseille. <br/>
<br/>
Article 3 : La présente décision sera notifiée au ministre de l'écologie, du développement durable et de l'énergie et à M A...B....<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">44-02-04 NATURE ET ENVIRONNEMENT. INSTALLATIONS CLASSÉES POUR LA PROTECTION DE L'ENVIRONNEMENT. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. - ARRÊTÉ DE CONSIGNATION PRIS À LA SUITE D'UN ARRÊTÉ DE MISE EN DEMEURE DE RESPECTER LES CONDITIONS IMPOSÉES À L'EXPLOITANT D'UNE ICPE (ART. L. 514-1 DU CODE DE L'ENVIRONNEMENT) - POSSIBILITÉ D'INVOQUER PAR LA VOIE DE L'EXCEPTION L'ILLÉGALITÉ DE CE DERNIER ARRÊTÉ À L'ENCONTRE DU PREMIER - EXISTENCE - CONDITION - ABSENCE DE CARACTÈRE DÉFINITIF DE L'ARRÊTÉ DE MISE EN DEMEURE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-07-01-04-04 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. QUESTIONS GÉNÉRALES. MOYENS. EXCEPTION D'ILLÉGALITÉ. - 1) IRRECEVABILITÉ D'UNE EXCEPTION D'ILLÉGALITÉ OPPOSÉE PAR LES PREMIERS JUGES - OFFICE DU JUGE D'APPEL - RECHERCHE D'OFFICE DU BIEN-FONDÉ DU JUGEMENT SUR CE POINT - ABSENCE - 2) ARRÊTÉ DE CONSIGNATION PRIS À LA SUITE D'UN ARRÊTÉ DE MISE EN DEMEURE DE RESPECTER LES CONDITIONS IMPOSÉES À L'EXPLOITANT D'UNE ICPE (ART. L. 514-1 DU CODE DE L'ENVIRONNEMENT) - POSSIBILITÉ D'INVOQUER PAR LA VOIE DE L'EXCEPTION L'ILLÉGALITÉ DE CE DERNIER ARRÊTÉ À L'ENCONTRE DU PREMIER - EXISTENCE - CONDITION - ABSENCE DE CARACTÈRE DÉFINITIF DE L'ARRÊTÉ DE MISE EN DEMEURE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-08-01 PROCÉDURE. VOIES DE RECOURS. APPEL. - IRRECEVABILITÉ D'UNE EXCEPTION D'ILLÉGALITÉ OPPOSÉE PAR LES PREMIERS JUGES - OFFICE DU JUGE D'APPEL - RECHERCHE D'OFFICE DU BIEN-FONDÉ DU JUGEMENT SUR CE POINT - ABSENCE.
</SCT>
<ANA ID="9A"> 44-02-04 Aux termes de l'article L. 514-1 du code de l'environnement :  Indépendamment des poursuites pénales qui peuvent être exercées, et lorsqu'un inspecteur des installations classées ou un expert désigné par le ministre chargé des installations classées a constaté l'inobservation des conditions imposées à l'exploitant d'une installation classée, le préfet met en demeure ce dernier de satisfaire à ces conditions dans un délai déterminé. Si, à l'expiration du délai fixé pour l'exécution, l'exploitant n'a pas obtempéré à cette injonction, le préfet peut : / 1° Obliger l'exploitant à consigner entre les mains d'un comptable public une somme répondant du montant des travaux à réaliser, laquelle sera restituée à l'exploitant au fur et à mesure de l'exécution des mesures prescrites (...) .,,,L'illégalité d'un arrêté de mise en demeure, pris sur le fondement de ces dispositions, peut utilement être invoquée, par la voie de l'exception, à l'encontre de l'arrêté de consignation pris à sa suite. Une telle exception d'illégalité n'est toutefois recevable que si cet arrêté, qui est dépourvu de caractère réglementaire, n'était pas devenu définitif à la date à laquelle elle est soulevée.</ANA>
<ANA ID="9B"> 54-07-01-04-04 1) Il n'appartient pas au juge d'appel, devant lequel l'appelant ne conteste pas l'irrecevabilité opposée en première instance au moyen d'exception d'illégalité qu'il avait soulevé à l'appui de ses conclusions, de rechercher d'office si cette irrecevabilité a été opposée à bon droit par les premiers juges.,,,2) Aux termes de l'article L. 514-1 du code de l'environnement :  Indépendamment des poursuites pénales qui peuvent être exercées, et lorsqu'un inspecteur des installations classées ou un expert désigné par le ministre chargé des installations classées a constaté l'inobservation des conditions imposées à l'exploitant d'une installation classée, le préfet met en demeure ce dernier de satisfaire à ces conditions dans un délai déterminé. Si, à l'expiration du délai fixé pour l'exécution, l'exploitant n'a pas obtempéré à cette injonction, le préfet peut : / 1° Obliger l'exploitant à consigner entre les mains d'un comptable public une somme répondant du montant des travaux à réaliser, laquelle sera restituée à l'exploitant au fur et à mesure de l'exécution des mesures prescrites (...) .,,,L'illégalité d'un arrêté de mise en demeure, pris sur le fondement de ces dispositions, peut utilement être invoquée, par la voie de l'exception, à l'encontre de l'arrêté de consignation pris à sa suite. Une telle exception d'illégalité n'est toutefois recevable que si cet arrêté, qui est dépourvu de caractère réglementaire, n'était pas devenu définitif à la date à laquelle elle est soulevée.</ANA>
<ANA ID="9C"> 54-08-01 Il n'appartient pas au juge d'appel, devant lequel l'appelant ne conteste pas l'irrecevabilité opposée en première instance au moyen d'exception d'illégalité qu'il avait soulevé à l'appui de ses conclusions, de rechercher d'office si cette irrecevabilité a été opposée à bon droit par les premiers juges.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
