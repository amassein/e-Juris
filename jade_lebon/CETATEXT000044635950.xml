<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044635950</ID>
<ANCIEN_ID>JG_L_2021_12_000000443269</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/63/59/CETATEXT000044635950.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 6ème chambres réunies, 29/12/2021, 443269</TITRE>
<DATE_DEC>2021-12-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>443269</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 6ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUVENIN, COUDRAY, GREVY</AVOCATS>
<RAPPORTEUR>Mme Flavie Le Tallec</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:443269.20211229</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 24 août et 24 novembre 2020 et le 12 novembre 2021 au secrétariat du contentieux du Conseil d'Etat, M. B... C... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision du 28 février 2020 par laquelle la ministre de la cohésion des territoires et des relations avec les collectivités territoriales et le ministre auprès de la ministre de la cohésion des territoires et des relations avec les collectivités territoriales, chargé de la ville et du logement, ont prononcé à son encontre la sanction de révocation de ses fonctions de directeur général d'un office public de l'habitat ;<br/>
<br/>
              2°) d'annuler pour excès de pouvoir la décision du 25 mars 2020 par laquelle le président de l'office public de l'habitat de Saint-Claude a mis fin à ses fonctions ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat et de l'office public de l'habitat de Saint-Claude la somme de 4000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la construction et de l'habitation ;<br/>
              - la loi n° 83-63 du 13 juillet 1983 ;<br/>
              - la loi n°2014-366 du 24 mars 2014 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Flavie Le Tallec, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Thouvenin, Coudray, Grevy, avocat de M. C....<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il résulte de l'instruction qu'à la suite d'un rapport de contrôle d'octobre 2017 portant sur l'office public de l'habitat (OPH) de Saint-Claude (Jura), le conseil d'administration de l'Agence nationale de contrôle du logement social (ANCOLS) a, par une délibération du 19 avril 2019, proposé au ministre chargé du logement de prononcer une sanction de révocation à l'encontre de M. C..., directeur général de cet office. Ce dernier demande l'annulation de la décision du 28 février 2020 par laquelle la ministre de la cohésion des territoires et des relations avec les collectivités territoriales et le ministre auprès de la ministre de la cohésion des territoires et des relations avec les collectivités territoriales, chargé de la ville et du logement lui ont infligé la sanction de révocation de ses fonctions de directeur général. Il demande également que soit, par voie de conséquence, annulée pour excès de pouvoir la décision du 25 mars 2020 par laquelle le président de l'office public de l'habitat a mis fin à ses fonctions.<br/>
<br/>
              Sur la régularité de la sanction attaquée :<br/>
<br/>
              En ce qui concerne la procédure applicable :<br/>
<br/>
              2. Aux termes de l'article L. 342-12 du code de la construction et de l'habitation, relatif aux suites que l'ANCOLS est susceptible de donner à ses opérations de contrôle des organismes de logement social : " En cas de manquements aux dispositions législatives et réglementaires qui lui sont applicables, d'irrégularité dans l'emploi des fonds de la participation à l'effort de construction ou des subventions, prêts ou avantages consentis par l'Etat ou par ses établissements publics et par les collectivités territoriales ou leurs établissements publics, de faute grave de gestion, de carence dans la réalisation de l'objet social ou de non-respect des conditions d'agrément constatés, l'agence demande à l'organisme ou la personne contrôlée de présenter ses observations et, le cas échéant, le met en demeure de procéder à la rectification des irrégularités dans un délai déterminé ". Et aux termes de l'article L. 342-14 du même code : " I- Après que la personne ou l'organisme a été mis en mesure de présenter ses observations en application de l'article L. 342-12 ou, en cas de mise en demeure, à l'issue du délai mentionné à ce même article, l'agence peut proposer au ministre chargé du logement de prononcer les sanctions suivantes (...)/ 2° S'il s'agit d'un organisme d'habitations à loyer modéré mentionné à l'article L. 411-2, d'un groupement d'intérêt économique ou de toute autre structure de mutualisation comprenant un organisme d'habitations à loyer modéré mentionné au même article L. 411-2 :/ (...) d) La révocation d'un ou plusieurs dirigeants ou membres du conseil d'administration, du conseil de surveillance ou du directoire ; (...)  / II- Par dérogation au I, lorsque la sanction concerne un office public de l'habitat ou une société d'économie mixte, elle est prise conjointement par les ministres chargés du logement et des collectivités territoriales, dans les mêmes conditions ".<br/>
<br/>
              3. En premier lieu, il résulte des dispositions citées ci-dessus et du principe des droits de la défense que l'ANCOLS ne peut régulièrement proposer au ministre chargé du logement et, le cas échéant, au ministre chargé des collectivités territoriales, de prononcer une sanction contre un organisme qu'elle a contrôlé ou contre l'un de ses dirigeants ou membres de son conseil d'administration, de son conseil de surveillance ou de son directoire qu'après que, s'agissant d'une sanction visant un organisme, le conseil de surveillance, le conseil d'administration ou l'organe délibérant de cet organisme, ou, s'agissant d'une sanction visant une personne physique, cette personne elle-même, après avoir été informée des griefs formulés à son encontre, ait été mise en mesure de présenter utilement ses observations avant que le conseil d'administration de l'agence ne délibère sur la sanction proposée au ministre compétent.<br/>
<br/>
              4. A cet effet, le premier alinéa de l'article L. 342-9 du code de la construction et de l'habitation prévoit que : " Le rapport provisoire est communiqué à la personne concernée, au président ou au dirigeant de l'organisme concerné, qui est mis en mesure de présenter ses observations dans le délai d'un mois ". Toutefois, l'agence n'étant pas tenue, au titre de cette communication du rapport provisoire, d'indiquer à l'organisme contrôlé, ou à la personne physique mise en cause, ceux des constats pour lesquels elle envisage, le cas échéant, de proposer aux ministres compétents de prononcer une sanction, il lui incombe dans un tel cas, pour assurer le respect des exigences rappelées au point précédent, d'assurer spécifiquement l'information de l'organisme ou de la personne mise en cause sur ce point.<br/>
<br/>
              5. Cette information peut notamment résulter de la transmission à l'organisme contrôlé ou à la personne mise en cause, dans des conditions lui permettant d'y répondre utilement, de la décision par laquelle le comité du contrôle et des suites de l'ANCOLS, mentionné à l'article R. 342-6 du code de la construction et de l'habitation, après avoir été saisi du rapport définitif de contrôle, indique au conseil d'administration de l'agence ceux des griefs figurant dans ce rapport pour lesquels il lui demande de proposer aux ministres compétents de prononcer une sanction. Une telle transmission n'est toutefois de nature à assurer le respect des droits de la défense qu'à la condition que la proposition de sanction transmise par l'agence aux ministres compétents ne se fonde pas sur d'autres griefs que ceux retenus par le comité du contrôle et des suites.<br/>
<br/>
              6. En second lieu, aux termes du second alinéa de l'article L. 342-9 du code de la construction et de l'habitation : " Le rapport définitif et, le cas échéant, les observations de l'organisme contrôlé et les suites apportées au contrôle sont communiqués au conseil de surveillance, au conseil d'administration ou à l'organe délibérant en tenant lieu et soumis à délibération à sa plus prochaine réunion ". Et  l'article R.342-14 du même code dispose que : " Le rapport définitif de contrôle est établi après examen des observations écrites apportées au rapport provisoire par le président ou le dirigeant de l'organisme contrôlé (...), complétées par leurs observations orales lorsqu'ils ont été entendus./ Le rapport définitif est notifié au président du conseil de surveillance, du conseil d'administration ou de l'organe délibérant ou, à défaut, au dirigeant de l'organisme contrôlé (...)/ Le président ou le dirigeant de l'organisme contrôlé communique le rapport définitif, accompagné de ses éventuelles observations écrites sur ce rapport, à chaque membre du conseil de surveillance, du conseil d'administration ou de l'organe délibérant. (...) Dans un délai de quatre mois à compter du lendemain du jour de la notification du rapport définitif à l'organisme, le conseil de surveillance, le conseil d'administration ou l'organe délibérant de l'organisme contrôlé peut adresser à l'agence ses observations écrites sur le rapport définitif de contrôle aux fins de leur publication ".<br/>
<br/>
              7. Il résulte ainsi de ces dispositions que, outre les exigences liées aux droits de la défense mentionnées aux points 4 et 5, l'ANCOLS ne peut régulièrement proposer une sanction aux ministres compétents à l'égard d'un organisme contrôlé qu'après que le conseil de surveillance, le conseil d'administration ou l'organe délibérant de cet organisme a été mis en mesure de présenter, en disposant à cette fin d'un délai de quatre mois, ses observations sur le rapport définitif de contrôle. En revanche, aucune disposition ni aucun principe n'impose à l'ANCOLS, en sus des exigences liées aux droits de la défense, de notifier ce même rapport définitif à une personne physique, dirigeant ou membre d'un conseil d'administration, d'un conseil de surveillance ou d'un directoire d'organisme contrôlé, à l'encontre duquel elle envisage de proposer une sanction, ni de mettre cette personne en mesure de présenter des observations sur le rapport définitif avant de proposer une sanction aux ministres compétents. <br/>
<br/>
              En ce qui concerne l'espèce :<br/>
<br/>
              8. En premier lieu, il résulte de l'instruction et n'est d'ailleurs pas contesté que, par un courrier du 24 octobre 2017, l'ANCOLS a informé M. C... qu'elle avait transmis son rapport définitif de contrôle à l'OPH de Saint-Claude et l'a invité à présenter dans un délai d'un mois ses observations sur plusieurs éléments de ce rapport, reproduits dans ce courrier. Elle l'informait que, sur le fondement de ces éléments, le comité du contrôle et des suites de l'agence avait préparé un projet de délibération pour le conseil d'administration, proposant aux ministres compétents de lui infliger la sanction de révocation de ses fonctions de directeur général.<br/>
<br/>
              9. Par suite, il résulte de ce qui a été dit aux points 1 à 7 que M. C..., qui ne saurait soutenir qu'il aurait dû se voir communiquer le rapport définitif, n'est pas non plus fondé à soutenir que les termes du courrier du 24 octobre 2017 faisaient obstacle à ce qu'il puisse présenter ses observations et que les droits de la défense ont été méconnus. S'il soutient que la proposition de sanction faite par l'ANCOLS n'a pas tenu compte des arguments figurant dans sa réponse du 23 novembre 2017, cette circonstance ne saurait utilement caractériser une méconnaissance des droits de la défense.<br/>
<br/>
              10 En deuxième lieu, la circonstance qu'un long délai s'est écoulé entre la procédure contradictoire organisée en octobre et novembre 2017 à l'égard de M. C... et la délibération du 19 avril 2019 par laquelle le conseil d'administration de l'ANCOLS a proposé au ministre de le sanctionner est sans incidence sur la régularité de la sanction attaquée. S'il résulte de l'instruction qu'un délai d'environ huit mois s'est, ensuite, écoulé entre la transmission de la proposition de l'ANCOLS et l'intervention de la sanction litigieuse, ce dernier délai n'a pas, dans les circonstances de l'espèce, revêtu un caractère déraisonnable qui entacherait d'irrégularité la sanction attaquée.<br/>
<br/>
              11. Enfin, la circonstance que la sanction attaquée, qui comporte l'ensemble des éléments de droit et de fait sur lesquels elle se fonde, ne reprendrait pas les mentions du rapport définitif de l'ANCOLS auxquelles sa motivation fait référence, n'est pas, par elle-même, de nature à caractériser une insuffisance de motivation.<br/>
<br/>
<br/>
              Sur le bien-fondé de la sanction attaquée<br/>
<br/>
              12. En premier lieu, il ne résulte pas de l'instruction que la ministre de la cohésion des territoires et des relations avec les collectivités territoriales et le ministre auprès de la ministre de la cohésion des territoires et des relations avec les collectivités territoriales, chargé de la ville et du logement se seraient crus tenus de reprendre, sans la modifier et sans apprécier les circonstances particulières de l'espèce, la proposition de sanction transmise par l'ANCOLS.<br/>
<br/>
              13. En deuxième lieu, en se bornant à soutenir que les faits sanctionnés ne sont pas établis, M. C... n'assortit pas ce moyen des précisions permettant d'en apprécier le bien-fondé. <br/>
<br/>
              14. En troisième lieu, la sanction attaquée ne revêtant pas le caractère d'une sanction disciplinaire, M. C... ne saurait utilement invoquer les dispositions de l'article 19 de la loi du 13 juillet 1983 portant droits et obligations des fonctionnaires, relatives au délai de prescription des procédures disciplinaires.<br/>
<br/>
              15. En quatrième lieu, il résulte de l'instruction que, si la motivation de l'acte attaqué rappelle que M. C... était directeur général depuis octobre 2000 et avait occupé un logement social au sein de son propre organisme dès sa prise de fonctions, les faits qui fondent la sanction litigieuse sont postérieurs à l'entrée en vigueur de la loi du 24 mars 2014 sur l'accès au logement et l'urbanisme rénové. Par suite, alors même que les dispositions de l'article L. 342-14 du code de la construction et de l'habitation, citées au point 2, qui prévoient la faculté de prononcer des sanctions à l'encontre des dirigeants d'organismes d'habitations à loyer modéré, n'ont été introduites qu'à compter de l'entrée en vigueur de cette loi, M. C... n'est pas fondé à soutenir que la sanction qu'il attaque a méconnu le principe de légalité des délits et des peines.<br/>
<br/>
              16. Enfin, en cinquième lieu, il résulte de l'instruction qu'entre 2014 et 2017, M. C... a bénéficié successivement de deux logements sociaux de 110m2 puis 80m2 au sein du parc de l'office public de l'habitat de Saint-Claude dont il assurait la direction générale, dans des conditions d'attribution dérogatoires et à des loyers qui, eu égard à son niveau de ressources, étaient anormalement bas. S'il soutient sur ce dernier point que l'absence de paiement, de sa part, du supplément de loyer de solidarité résultait de ce qu'il occupait son logement à titre de logement de fonction et que, par suite, il n'avait pas à acquitter ce supplément, il résulte de l'instruction que la sanction litigieuse n'est, en tout état de cause, pas fondée sur le fait qu'il aurait omis de verser certaines sommes qui auraient été dues, mais sur ce que, par des manœuvres rendues possibles par sa position au sein de l'organisme, il s'est assuré des conditions de logement particulièrement avantageuses.<br/>
<br/>
              17. Par suite, eu égard à la gravité de ce comportement de la part d'un directeur général d'organisme d'habitat à loyer modéré, M. C... n'est pas fondé à soutenir que la sanction qui lui a été infligée n'est pas proportionnée aux fautes qui lui sont reprochées.<br/>
<br/>
              18. Il résulte de tout ce qui précède que M. C... n'est pas fondé à demander l'annulation de la sanction qu'il attaque ni à demander, par voie de conséquence, l'annulation de la décision du 25 mars 2020 du président de l'office public d'habitat de Saint-Claude le révoquant de son poste. <br/>
<br/>
              19. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante, la somme que demande, à ce titre, M. C.... Si la société La Maison pour tous, venant aux droits de l'office public de l'habitat de Saint-Claude, n'a pas la qualité de partie dans le litige introduit par M. C... contre la sanction du 28 février 2020, il n'y a pas lieu, dans les circonstances de l'espèce, en ce qui concerne le litige dirigé contre la décision du 25 mars 2020, de mettre à la charge de M. C... la somme que demande cette société au titre de ces dispositions.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
		Article 1er : La requête de M. C... est rejetée.<br/>
<br/>
Article 2 : Les conclusions de la société La Maison pour tous présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 3 : La présente décision sera notifiée à M. B... C..., à la société La maison pour tous et à la ministre de la transition écologique.<br/>
              Délibéré à l'issue de la séance du 17 décembre 2021 où siégeaient : Mme Christine Maugüé, présidente adjointe de la section du contentieux, présidant ; M. B... H..., M. Fabien Raynaud, présidents de chambre ; M. M... D..., Mme F... L..., M. E... J..., M. A... K... et Mme Bénédicte Fauvarque-Cosson, conseillers d'Etat et Mme Flavie Le Tallec, maître des requêtes en service extraordinaire-rapporteure. <br/>
<br/>
              Rendu le 29 décembre 2021.<br/>
<br/>
                 La présidente : <br/>
                 Signé : Mme Christine Maugüé<br/>
 		ae rapporteure : <br/>
      Signé : Mme Flavie Le Tallec<br/>
                 La secrétaire :<br/>
                 Signé : Mme I... G...<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-04-03-07-03 ACTES LÉGISLATIFS ET ADMINISTRATIFS. - VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. - PRINCIPES GÉNÉRAUX DU DROIT. - PRINCIPES INTÉRESSANT L'ACTION ADMINISTRATIVE. - RESPECT DES DROITS DE LA DÉFENSE. - PROPOSITION PAR L'ANCOLS D'UNE SANCTION À L'ENCONTRE DU DIRIGEANT D'UN ORGANISME DE LOGEMENT SOCIAL - OBLIGATION DE LUI COMMUNIQUER LE RAPPORT DÉFINITIF DE CONTRÔLE - ABSENCE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">38-04 LOGEMENT. - HABITATIONS À LOYER MODÉRÉ. - ANCOLS - PROPOSITION DE SANCTION À L'ENCONTRE DU DIRIGEANT D'UN ORGANISME DE LOGEMENT SOCIAL - OBLIGATION DE COMMUNIQUER À CE DIRIGEANT LE RAPPORT DÉFINITIF DE CONTRÔLE - ABSENCE [RJ1].
</SCT>
<ANA ID="9A"> 01-04-03-07-03 Aucune disposition ni aucun principe n'impose à l'Agence nationale de contrôle du logement social (ANCOLS), en sus des exigences liées aux droits de la défense, de notifier ce même rapport définitif à une personne physique, dirigeant ou membre du conseil d'administration, du conseil de surveillance ou du directoire de l'organisme contrôlé, pour lequel elle envisage de proposer une sanction, ni de mettre cette personne en mesure de présenter des observations sur le rapport définitif avant de proposer une sanction aux ministres compétents.</ANA>
<ANA ID="9B"> 38-04 Aucune disposition ni aucun principe n'impose à l'Agence nationale de contrôle du logement social (ANCOLS), en sus des exigences liées aux droits de la défense, de notifier ce même rapport définitif à une personne physique, dirigeant ou membre du conseil d'administration, du conseil de surveillance ou du directoire de l'organisme contrôlé, pour lequel elle envisage de proposer une sanction, ni de mettre cette personne en mesure de présenter des observations sur le rapport définitif avant de proposer une sanction aux ministres compétents.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Comp., s'agissant de l'obligation de communiquer ce rapport à l'organisme lui-même, CE, 26 avril 2018, SAEM Habiter à Yerres, n°s 409688 409703, T. pp. 533-761 ; CE, 16 juin 2021, OPH Drôme aménagement habitat, n°s 432682 436311, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
