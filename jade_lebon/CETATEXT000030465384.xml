<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030465384</ID>
<ANCIEN_ID>JG_L_2015_03_000000369431</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/46/53/CETATEXT000030465384.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 1ère SSR, 30/03/2015, 369431</TITRE>
<DATE_DEC>2015-03-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>369431</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 1ère SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ ; SCP DE NERVO, POUPET</AVOCATS>
<RAPPORTEUR>Mme Sophie-Justine Lieber</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier De Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2015:369431.20150330</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Procédure contentieuse antérieure<br/>
<br/>
              M. J...B...et plusieurs autres requérants, d'une part, et l'Association pour la protection des animaux sauvages (ASPAS), d'autre part, ont demandé au tribunal administratif de Besançon d'annuler pour excès de pouvoir les arrêtés des 8 et 21 janvier 2010 du préfet de la Haute-Saône délivrant à la société Eole-Res des permis de construire pour neuf éoliennes. <br/>
<br/>
              Par deux jugements nos 1000907 et 1000915 du 29 décembre 2011, le tribunal administratif de Besançon a annulé les arrêtés des 8 et 21 janvier 2010 du préfet de la Haute-Saône en tant qu'ils délivraient des permis de construire pour deux éoliennes et a rejeté le surplus des demandes tendant à l'annulation des permis de construire sept autres éoliennes.<br/>
<br/>
              Par un arrêt nos 12NC00392, 12NC00393, 12NC00456, 12NC00457 du 18 avril 2013, la cour administrative d'appel de Nancy, a, d'une part, à la demande de M. J...B...et autres et de l'ASPAS, annulé les jugements nos 1000907 et 1000915 du 29 décembre 2011 du tribunal administratif de Besançon, et annulé l'ensemble des permis de construire accordés par les arrêtés des 8 et 21 janvier 2010, et, d'autre part, rejeté les requêtes de la société Eole-Res et les conclusions incidentes du ministre de l'égalité des territoires et du logement, tendant à l'annulation des mêmes jugements en tant qu'ils annulaient les permis de construire relatifs à deux des éoliennes du projet ;<br/>
<br/>
              Procédure devant le Conseil d'Etat<br/>
<br/>
              1°, sous le n° 369431, par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 18 juin et 18 septembre 2013 et le 4 mars 2015 au secrétariat du contentieux du Conseil d'Etat, la société Eole-Res demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt nos 12NC00392, 12NC00393, 12NC00456, 12NC00457 du 18 avril 2013 de la cour administrative d'appel de Nancy ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses conclusions d'appel ; <br/>
<br/>
              3°) de mettre à la charge de M. J...B...et autres et de l'ASPAS une somme de 6 000 euros en application de l'article L. 761-1 du code de justice administrative, ainsi que les entiers dépens, y compris la contribution à l'aide juridique prévue à l'article R. 761-1 du même code. <br/>
<br/>
              2°, sous le n° 369637, par pourvoi, enregistré le 24 juin 2013 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'égalité des territoires et du logement demande au Conseil d'Etat d'annuler le même arrêt de la cour administrative d'appel de Nancy. <br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
<br/>
              - le code de l'aviation civile ;<br/>
<br/>
              - le code de l'urbanisme ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie-Justine Lieber, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de la société Eole-Res et à la SCP de Nervo, Poupet, avocat de M.B..., de l'ASPAS, de M. C...F..., de M. D... F..., de M.I..., de MmeG..., de M. A..., de M. H...et de M. E...;<br/>
<br/>
              - Vu la note en délibéré, enregistrée le 12 mars 2015, présentée sous les deux nos pour l'ASPAS et autres ; <br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que les pourvois de la société Eole-Res et du ministre de l'égalité des territoires et du logement sont dirigés contre le même arrêt ; qu'il y a lieu de les joindre pour statuer par une seule décision ;<br/>
<br/>
              2. Considérant, d'une part, que, devant les juridictions administratives et dans l'intérêt d'une bonne justice, le juge a toujours la faculté de rouvrir l'instruction, qu'il dirige, lorsqu'il est saisi d'une production postérieure à la clôture de celle-ci ; qu'il lui appartient, dans tous les cas, de prendre connaissance de cette production avant de rendre sa décision et de la viser ; que, s'il décide d'en tenir compte, il rouvre l'instruction et soumet au débat contradictoire les éléments contenus dans cette production qu'il doit, en outre, analyser ; que, dans le cas particulier où cette production contient l'exposé d'une circonstance de fait ou d'un élément de droit dont la partie qui l'invoque n'était pas en mesure de faire état avant la clôture de l'instruction et qui est susceptible d'exercer une influence sur le jugement de l'affaire, le juge doit alors en tenir compte, à peine d'irrégularité de sa décision ; <br/>
<br/>
              3. Considérant, d'autre part, que lorsqu'un permis de construire a été délivré en méconnaissance des dispositions législatives ou réglementaires relatives à l'utilisation du sol ou sans que soient respectées des formes ou formalités préalables à la délivrance des permis de construire, l'illégalité qui en résulte peut être régularisée par la délivrance d'un permis modificatif dès lors que celui-ci assure les respect des règles de fond applicables au projet en cause, répond aux exigences de forme ou a été précédé de l'exécution régulière de la ou des formalités qui avaient été omises ; que les irrégularités ainsi régularisées ne peuvent plus être utilement invoquées à l'appui d'un recours pour excès de pouvoir dirigé contre le permis initial ;<br/>
<br/>
              4. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, pour annuler les permis de construire attaqués, la cour administrative d'appel de Nancy s'est fondée sur le moyen tiré de ce que l'avis du ministre de l'aviation civile, exigé en application des dispositions combinées des articles R. 423-51 et R. 425-9 du code de l'urbanisme et R. 244-1 du code de l'aviation civile, avait été donné par un agent ne disposant d'aucune délégation régulière à cet effet ; que, toutefois, par une note en délibéré produite le 14 avril 2013, après la clôture de l'instruction intervenue le 25 mars 2013, trois jours francs avant l'audience du 28 mars 2013, mais avant la lecture de l'arrêt, intervenue le 18 avril 2013, la société Eole-Res a transmis à la cour un nouvel avis du ministre de l'aviation civile se substituant au précédent et trois permis de construire modificatifs en date du 10 avril 2013, ayant pour objet, compte tenu de cet avis, de régulariser l'illégalité mentionnée ci-dessus ; qu'il résulte de ce qui a été dit au point 3 que l'illégalité des permis de construire initiaux était de celles qui peuvent être régularisées par la délivrance d'un permis modificatif ; que ces circonstances nouvelles, dont la partie qui l'invoque n'était pas en mesure de faire état avant la clôture de l'instruction, étaient, dès lors, susceptibles d'exercer une influence sur le jugement de l'affaire ; qu'il suit de là qu'en s'abstenant d'en tenir compte et de rouvrir en conséquence l'instruction, la cour a statué au terme d'une procédure irrégulière ; que, par suite, sans qu'il soit besoin d'examiner les autres moyens des pourvois, la société Eole-Res et le ministre de l'égalité des territoires et du logement sont fondés à demander l'annulation de l'arrêt attaqué ;<br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de laisser la contribution à l'aide juridique à la charge de la société Eole-Res ; <br/>
<br/>
              6. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par la société Eole-Res au titre de l'article L. 761-1 du code de justice administrative ; que les mêmes dispositions font obstacle à ce qu'il soit fait droit aux conclusions présentées au même titre par l'ASPAS et par M. B...et autres ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nancy du 18 avril 2013 est annulé. <br/>
<br/>
Article 2 : L'affaire est renvoyée devant la cour administrative d'appel de Nancy.<br/>
<br/>
      Article 3 : Le surplus des conclusions de la société Eole-Res est rejeté. <br/>
<br/>
Article 4 : Les conclusions de l'ASPAS et autres tendant à l'application de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 5 : La présente décision sera notifiée à la société Eole-Res, à l'Association pour la protection des animaux sauvages (ASPAS), premier défendeur des pourvois, et à la ministre de l'égalité des territoires et du logement. Les autres défendeurs seront informés de la présente décision par la SCP de Nervo, Poupet, avocat au Conseil d'Etat et à la Cour de cassation, qui les représente devant le Conseil d'Etat.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-04-01-05 PROCÉDURE. INSTRUCTION. POUVOIRS GÉNÉRAUX D'INSTRUCTION DU JUGE. CLÔTURE DE L'INSTRUCTION. - PRODUCTION POSTÉRIEURE À LA CLÔTURE DE L'INSTRUCTION - PERMIS MODIFICATIF, INTERVENU EN RÉGULARISATION DU PERMIS INITIAL, QUE LA PARTIE N'ÉTAIT PAS EN MESURE DE PRODUIRE AVANT LA CLÔTURE DE L'INSTRUCTION - OBLIGATION DE RÉOUVERTURE DE L'INSTRUCTION - EXISTENCE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">68-06-04 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. POUVOIRS DU JUGE. - PRODUCTIONS APRÈS LA CLÔTURE DE L'INSTRUCTION - PERMIS MODIFICATIF, INTERVENU EN RÉGULARISATION DU PERMIS INITIAL, QUE LA PARTIE N'ÉTAIT PAS EN MESURE DE PRODUIRE AVANT LA CLÔTURE DE L'INSTRUCTION - OBLIGATION DE RÉOUVERTURE DE L'INSTRUCTION - EXISTENCE [RJ1].
</SCT>
<ANA ID="9A"> 54-04-01-05 Cour administrative d'appel ayant annulé un permis de construire pour un vice de procédure susceptible d'être régularisé par un permis modificatif. La production d'un tel permis modificatif après la clôture de l'instruction rend le moyen tiré du vice de procédure inopérant contre le permis initial [RJ2] et constitue, si la partie qui le produit n'était pas en mesure d'en faire état avant la clôture de l'instruction, une circonstance nouvelle susceptible d'exercer une influence sur le jugement de l'affaire. Obligation d'en tenir compte et de rouvrir en conséquence l'instruction.</ANA>
<ANA ID="9B"> 68-06-04 Cour administrative d'appel ayant annulé un permis de construire pour un vice de procédure susceptible d'être régularisé par un permis modificatif. La production d'un tel permis modificatif après la clôture de l'instruction rend le moyen tiré du vice de procédure inopérant contre le permis initial [RJ2] et constitue, si la partie qui le produit n'était pas en mesure d'en faire état avant la clôture de l'instruction, une circonstance nouvelle susceptible d'exercer une influence sur le jugement de l'affaire. Obligation d'en tenir compte et de rouvrir en conséquence l'instruction.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Cf. CE, Section, 5 décembre 2014, M. Lassus, n° 340943, p. 369.,,[RJ2]Cf. CE, 2 février 2004, SCI La Fontaine de Villiers, n° 238415, T. p. 914.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
