<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036965989</ID>
<ANCIEN_ID>JG_L_2018_05_000000405785</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/96/59/CETATEXT000036965989.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 5ème chambres réunies, 30/05/2018, 405785</TITRE>
<DATE_DEC>2018-05-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>405785</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ODENT, POULET</AVOCATS>
<RAPPORTEUR>M. Jean-Baptiste de Froment</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Julie Burguburu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:405785.20180530</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...a demandé au tribunal administratif de Montpellier d'annuler par excès de pouvoir l'arrêté du 17 octobre 2011 par lequel le préfet de l'Aude a délivré à la communauté d'agglomération du Carcassonnais l'autorisation prévue à l'article L. 214-3 du code de l'environnement pour réaliser des travaux de reprofilage du ruisseau du Régal relevant de la rubrique 3.1.2.0 de la nomenclature des opérations soumises à autorisation ou à déclaration en application des articles L. 214-1 à L. 214-3 du code de l'environnement. Par un jugement n° 1205035 du 18 novembre 2014, le tribunal administratif de Montpellier a annulé cet arrêté.<br/>
<br/>
              Par un arrêt n° 15MA00417 du 6 octobre 2016, la cour administrative d'appel de Marseille a rejeté l'appel du ministre de l'environnement, de l'énergie et de la mer dirigé contre ce jugement.  <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 7 décembre 2016 et 8 mars 2017 au secrétariat du contentieux du Conseil d'Etat, la ministre de l'environnement, de l'énergie et de la mer demande au Conseil d'Etat d'annuler cet arrêt. <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'environnement ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Baptiste de Froment, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Julie Burguburu, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Odent, Poulet, avocat de M.B....<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 17 octobre 2011, le préfet de l'Aude a délivré à la communauté d'agglomération du Carcassonnais l'autorisation prévue à l'article L. 214-3 du code de l'environnement pour réaliser des travaux de reprofilage du ruisseau du Régal relevant de la rubrique 3.1.2.0 de la nomenclature des opérations soumises à autorisation ou à déclaration en application des articles L. 214-1 à L. 214-3 du code de l'environnement ; que, par un jugement du 18 novembre 2014, le tribunal administratif de Montpellier a annulé cet arrêté ; que, par un arrêt du 6 octobre 2016, contre lequel le ministre chargé de l'environnement se pourvoit en cassation, la cour administrative d'appel de Marseille a confirmé ce jugement ;<br/>
<br/>
              2. Considérant, d'une part, qu'aux termes du I de l'article L. 214-3 du code de l'environnement, dans sa rédaction applicable au litige : " Sont soumis à autorisation de l'autorité administrative les installations, ouvrages, travaux et activités susceptibles de présenter des dangers pour la santé et la sécurité publique, de nuire au libre écoulement des eaux, de réduire la ressource en eau, d'accroître notablement le risque d'inondation, de porter gravement atteinte à la qualité ou à la diversité du milieu aquatique, notamment aux peuplements piscicoles. / Les prescriptions nécessaires à la protection des intérêts mentionnés à l'article L. 211-1, les moyens de surveillance, les modalités des contrôles techniques et les moyens d'intervention en cas d'incident ou d'accident sont fixés par l'arrêté d'autorisation et, éventuellement, par des actes complémentaires pris postérieurement. (...) " ;<br/>
<br/>
              3. Considérant, d'autre part, qu'aux termes du I de l'article L. 411-1 du code de l'environnement dans sa rédaction applicable au litige : " Lorsqu'un intérêt scientifique particulier ou que les nécessités de la préservation du patrimoine naturel justifient la conservation de sites d'intérêt géologique, d'habitats naturels, d'espèces animales non domestiques ou végétales non cultivées et de leurs habitats, sont interdits : / 1° La destruction ou l'enlèvement des oeufs ou des nids, la mutilation, la destruction, la capture ou l'enlèvement, la perturbation intentionnelle, la naturalisation d'animaux de ces espèces ou, qu'ils soient vivants ou morts, leur transport, leur colportage, leur utilisation, leur détention, leur mise en vente, leur vente ou leur achat ; / (...) / 3° La destruction, l'altération ou la dégradation de ces habitats naturels ou de ces habitats d'espèces ; / (...) " ; qu'aux termes de l'article L. 411-2 du même code dans sa rédaction alors en vigueur : " Un décret en Conseil d'Etat détermine les conditions dans lesquelles sont fixées : / 1° La liste limitative des habitats naturels, des espèces animales non domestiques ou végétales non cultivées ainsi que des sites d'intérêt géologique, y compris des types de cavités souterraines, ainsi protégés ; / 2° La durée et les modalités de mise en oeuvre des interdictions prises en application du I de l'article L. 411-1 ; / 3° La partie du territoire national sur laquelle elles s'appliquent, qui peut comprendre le domaine public maritime, les eaux intérieures et la mer territoriale ; / 4° La délivrance de dérogation aux interdictions mentionnées aux 1°, 2° et 3° de l'article L. 411-1, à condition qu'il n'existe pas d'autre solution satisfaisante et que la dérogation ne nuise pas au maintien, dans un état de conservation favorable, des populations des espèces concernées dans leur aire de répartition naturelle / (...) " ; qu'aux termes de l'article R. 411-1 du même code dans sa rédaction alors en vigueur : " Les listes des espèces animales non domestiques et des espèces végétales non cultivées faisant l'objet des interdictions définies par les articles L. 411-1 et L. 411-3 sont établies par arrêté conjoint du ministre chargé de la protection de la nature et soit du ministre chargé de l'agriculture, soit, lorsqu'il s'agit d'espèces marines, du ministre chargé des pêches maritimes. / (...) " ; qu'aux termes de l'article R. 411-6 du même code alors en vigueur : " Les dérogations définies au 4° de l'article L. 411-2 sont accordées par le préfet, sauf dans les cas prévus aux articles R. 411-7 et R. 411-8. " ; <br/>
<br/>
              4. Considérant qu'il résulte des dispositions précitées des articles L. 411-1 et L. 411-2 du code de l'environnement qu'elles organisaient, avant l'intervention de l'ordonnance du 26 janvier 2017 relative à l'autorisation environnementale, un régime juridique spécifique en vue de la protection du patrimoine naturel ; que toute dérogation aux interdictions édictées par l'article L. 411-1 devait faire l'objet d'une autorisation particulière, délivrée par le préfet ou, dans certains cas, par le ministre chargé de la protection de la nature ; que le titulaire de l'autorisation délivrée sur le fondement distinct de l'article L. 214-3 du code de l'environnement, au titre de la législation sur l'eau, était également tenu d'obtenir, en tant que de besoin, une telle dérogation au titre de la législation sur la protection du patrimoine naturel ; que si l'autorité administrative compétente pour délivrer l'autorisation au titre de l'article L. 214-3 du code de l'environnement avait connaissance, notamment au vu de l'étude d'impact jointe à la demande d'autorisation qui doit en principe faire apparaitre l'existence d'espèces protégées dans la zone concernée, des risques éventuels auxquels étaient exposées certaines espèces protégées, et pouvait alors alerter le pétitionnaire sur la nécessité de se conformer à la législation sur la protection du patrimoine naturel, en revanche, elle ne pouvait légalement subordonner la délivrance de l'autorisation sollicitée au titre de la police de l'eau au respect de cette législation sur la protection du patrimoine naturel ; qu'ainsi, en jugeant que l'autorité administrative, lorsqu'elle a délivré, à la date de la décision attaquée, une autorisation sur le fondement de l'article L. 214-3 du code de l'environnement devait veiller à ce que l'exécution des installations, ouvrages, travaux et activités concernés respecte les interdictions posées à l'article L. 411-1 de ce code, la cour administrative d'appel a commis une erreur de droit ; que, par suite et sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, le ministre est fondé à demander l'annulation de l'arrêt qu'il attaque ;<br/>
<br/>
              5. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 6 octobre 2016 de la cour administrative d'appel de Marseille est annulé.<br/>
<br/>
      Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Marseille.<br/>
<br/>
Article 3 : Les conclusions présentées M. B...au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
<br/>
Article 4 : La présente décision sera notifiée au ministre d'Etat, ministre de la transition écologique et solidaire et à M. A...B....<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">27 EAUX. - RÉGIME ANTÉRIEUR À L'AUTORISATION ENVIRONNEMENTALE (ART. L. 181-1 ET SUIVANTS DU CODE DE L'ENVIRONNEMENT) - INDÉPENDANCE DES LÉGISLATIONS - PORTÉE - FACULTÉ DE SUBORDONNER UNE AUTORISATION SOLLICITÉE AU TITRE DE LA POLICE DE L'EAU AU RESPECT DE LA LÉGISLATION SUR LA PROTECTION DU PATRIMOINE NATUREL - ABSENCE [RJ1] - FACULTÉ D'ALERTER LE PÉTITIONNAIRE D'UNE TELLE AUTORISATION SUR LA NÉCESSITÉ DE SE CONFORMER À LA LÉGISLATION SUR LA PROTECTION DU PATRIMOINE NATUREL - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">44-045-01 NATURE ET ENVIRONNEMENT. - RÉGIME ANTÉRIEUR À L'AUTORISATION ENVIRONNEMENTALE (ART. L. 181-1 ET SUIVANTS DU CODE DE L'ENVIRONNEMENT) - INDÉPENDANCE DES LÉGISLATIONS - PORTÉE - FACULTÉ DE SUBORDONNER UNE AUTORISATION SOLLICITÉE AU TITRE DE LA POLICE DE L'EAU AU RESPECT DE LA LÉGISLATION SUR LA PROTECTION DU PATRIMOINE NATUREL - ABSENCE [RJ1] - FACULTÉ D'ALERTER LE PÉTITIONNAIRE D'UNE TELLE AUTORISATION SUR LA NÉCESSITÉ DE SE CONFORMER À LA LÉGISLATION SUR LA PROTECTION DU PATRIMOINE NATUREL - EXISTENCE.
</SCT>
<ANA ID="9A"> 27 Les articles L. 411-1 et L. 411-2 du code de l'environnement organisaient, avant l'intervention de l'ordonnance n° 2017-80 du 26 janvier 2017 relative à l'autorisation environnementale, un régime juridique spécifique en vue de la protection du patrimoine naturel. Toute dérogation aux interdictions édictées par l'article L. 411-1 devait faire l'objet d'une autorisation particulière, délivrée par le préfet ou, dans certains cas, par le ministre chargé de la protection de la nature. Le titulaire de l'autorisation délivrée sur le fondement distinct de l'article L. 214-3 du code de l'environnement, au titre de la législation sur l'eau, était également tenu d'obtenir, en tant que de besoin, une telle dérogation au titre de la législation sur la protection du patrimoine naturel. Si l'autorité administrative compétente pour délivrer l'autorisation au titre de l'article L. 214-3 du code de l'environnement avait connaissance, notamment au vu de l'étude d'impact jointe à la demande d'autorisation qui doit en principe faire apparaître l'existence d'espèces protégées dans la zone concernée, des risques éventuels auxquels étaient exposées certaines espèces protégées, et pouvait alors alerter le pétitionnaire sur la nécessité de se conformer à la législation sur la protection du patrimoine naturel, en revanche, elle ne pouvait légalement subordonner la délivrance de l'autorisation sollicitée au titre de la police de l'eau au respect de cette législation sur la protection du patrimoine naturel.</ANA>
<ANA ID="9B"> 44-045-01 Les articles L. 411-1 et L. 411-2 du code de l'environnement organisaient, avant l'intervention de l'ordonnance n° 2017-80 du 26 janvier 2017 relative à l'autorisation environnementale, un régime juridique spécifique en vue de la protection du patrimoine naturel. Toute dérogation aux interdictions édictées par l'article L. 411-1 devait faire l'objet d'une autorisation particulière, délivrée par le préfet ou, dans certains cas, par le ministre chargé de la protection de la nature. Le titulaire de l'autorisation délivrée sur le fondement distinct de l'article L. 214-3 du code de l'environnement, au titre de la législation sur l'eau, était également tenu d'obtenir, en tant que de besoin, une telle dérogation au titre de la législation sur la protection du patrimoine naturel. Si l'autorité administrative compétente pour délivrer l'autorisation au titre de l'article L. 214-3 du code de l'environnement avait connaissance, notamment au vu de l'étude d'impact jointe à la demande d'autorisation qui doit en principe faire apparaître l'existence d'espèces protégées dans la zone concernée, des risques éventuels auxquels étaient exposées certaines espèces protégées, et pouvait alors alerter le pétitionnaire sur la nécessité de se conformer à la législation sur la protection du patrimoine naturel, en revanche, elle ne pouvait légalement subordonner la délivrance de l'autorisation sollicitée au titre de la police de l'eau au respect de cette législation sur la protection du patrimoine naturel.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Comp., s'agissant d'un acte réglementaire, CE, 3 juin 2013, Association interdépartementale et intercommunale pour la protection du lac de Sainte-Croix, de son environnement, des lacs, sites et villages du Verdon, n°s 334251 334483, T. pp. 708-717-736.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
