<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038351118</ID>
<ANCIEN_ID>JG_L_2019_04_000000426281</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/35/11/CETATEXT000038351118.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 05/04/2019, 426281</TITRE>
<DATE_DEC>2019-04-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>426281</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>M. Christian Fournier</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CECHR:2019:426281.20190405</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par un arrêt n° 16VE03323 du 13 décembre 2018, enregistré le 17 décembre 2018 au secrétariat du contentieux du Conseil d'Etat, la cour administrative d'appel de Versailles, avant de statuer sur la demande de M. B... tendant, d'une part, à l'annulation du jugement n° 1305609 du 19 septembre 2016 par lequel le tribunal administratif de Versailles a rejeté sa demande tendant à l'annulation des décisions du 1er juillet 2013 du conseil général des Yvelines refusant de reconnaître l'imputabilité au service de son état de santé et de prolonger son placement en congé de longue durée et à ce qu'il soit enjoint au département de statuer à nouveau sur sa demande et de reconnaître l'imputabilité au service de son état de santé en lui accordant une prolongation de son congé de longue durée et, d'autre part, à ce qu'il soit enjoint au département de statuer à nouveau sur sa demande dans un délai de quinze jours à compter de la notification de l'arrêt à intervenir, de reconnaître l'imputabilité au service de son état de santé et de lui accorder une prolongation de son congé de longue durée, a décidé, sur le fondement de l'article L. 113-1 du code de justice administrative, de transmettre le dossier de cette demande au Conseil d'Etat, en soumettant à son examen la question suivante :<br/>
<br/>
              Les dispositions de l'article 23 du décret n° 87-602 du 30 juillet 1987, qui ne prévoit aucun délai pour la présentation par un fonctionnaire territorial d'une demande de congé longue durée à raison d'une maladie contractée en service, doivent-elles être interprétées, à la lumière de la décision du Conseil d'Etat statuant au contentieux " Centre communal d'action sociale de Toulouse " du 18 mars 1996, n° 107065, comme étant les seules dispositions applicables aux agents de la fonction publique territoriale et doit-on considérer alors qu'aucun délai ne peut être opposé à une telle demande ' Doit-on au contraire procéder à une combinaison de ces dispositions avec celles de l'article 32 du décret n° 86-442 du 14 mars 1986, qui est visé par le décret n° 87-602 du 30 juillet 1987, et ainsi considérer qu'une demande d'imputabilité au service d'une maladie par un agent de la fonction publique territoriale doit être présentée, comme pour les agents de la fonction publique d'Etat, dans un délai de quatre ans suivant la date de la première constatation médicale de la maladie '<br/>
<br/>
              Des observations, enregistrées les 27 décembre 2018 et 11 février 2019, ont été présentées par M. B....<br/>
<br/>
              Des observations, enregistrées le 24 janvier 2019, ont été présentées par le département des Yvelines.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la loi n° 84-16 du 11 janvier 1984 ;<br/>
              - la loi n° 84-53 du 26 janvier 1984 :<br/>
              - le décret n° 86-442 du 14 mars 1986 ;<br/>
              - le décret n° 87-602 du 30 juillet 1987 ; <br/>
              - le code de justice administrative, notamment son article L. 113-1 ;<br/>
<br/>
<br/>
               Après avoir entendu en séance publique :<br/>
<br/>
- le rapport de M. Christian Fournier, maître des requêtes, <br/>
- les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
              - La parole ayant été donnée, avant et après les conclusions, à la SCP Foussard, Froger, avocat du département des Yvelines ;<br/>
<br/>
<br/>
<br/>
              REND L'AVIS SUIVANT<br/>
<br/>
              1. Aux termes, d'une part, de l'article 32 du décret du 14 mars 1986 relatif à la désignation des médecins agréés, à l'organisation des comités médicaux et des commissions de réforme, aux conditions d'aptitude physique pour l'admission aux emplois publics et au régime de congés de maladie des fonctionnaires, dans sa rédaction applicable en 2013 : " Lorsque le congé de longue durée est demandé pour une maladie contractée dans l'exercice des fonctions, le dossier est soumis à la commission de réforme. Ce dossier doit comprendre un rapport écrit du médecin chargé de la prévention attaché au service auquel appartient le fonctionnaire concerné. La demande tendant à ce que la maladie soit reconnue comme ayant été contractée dans l'exercice des fonctions doit être présentée dans les quatre ans qui suivent la date de la première constatation médicale de la maladie. La commission de réforme n'est toutefois pas consultée lorsque l'imputabilité au service d'une maladie ou d'un accident est reconnue par l'administration (...) ".<br/>
<br/>
              2. Aux termes, d'autre part, de l'article 23 du décret du 30 juillet 1987 pris pour l'application de la loi du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale et relatif à l'organisation des comités médicaux, aux conditions d'aptitude physique et au régime des congés de maladie des fonctionnaires territoriaux, dans sa rédaction applicable au litige : " Lorsque le congé de longue durée est demandé pour une maladie contractée en service, le dossier est soumis à la commission de réforme prévue par le décret n° 65-773 du 9 septembre 1965 susvisé ; le dossier doit comprendre un rapport écrit du médecin du service de médecine préventive attaché à la collectivité ou établissement auquel appartient le fonctionnaire concerné. Lorsque l'administration est amenée à se prononcer sur l'imputabilité au service d'une maladie ou d'un accident, elle peut, en tant que de besoin, consulter un médecin expert agréé. La commission de réforme n'est pas consultée lorsque l'imputabilité au service d'une maladie ou d'un accident est reconnue par l'administration. La commission de réforme peut, en tant que de besoin, demander à l'administration de lui communiquer les décisions reconnaissant l'imputabilité ".<br/>
<br/>
              3. Le décret du 14 mars 1986 a été pris pour l'application des articles 34 et 35 de la loi du 11 janvier 1984 portant dispositions statutaires relatives à la fonction publique de l'Etat. Ses dispositions, notamment celles de l'article 32 citées au point 1, ne s'appliquent qu'aux fonctionnaires régis par cette loi, à savoir les fonctionnaires de l'Etat.<br/>
<br/>
              4. Les fonctionnaires territoriaux sont régis, s'agissant de l'organisation des comités médicaux, des conditions d'aptitude physique et du régime des congés de maladie, par les dispositions du décret du 30 juillet 1987 pris pour l'application des articles 57 et 58 de la loi du 26 janvier 1984. Aucune disposition de ce décret ni aucun autre texte réglementaire ou principe général ne rend applicables aux fonctionnaires territoriaux les dispositions de l'article 32 du décret du 14 mars 1986 relatives au délai de quatre ans dans lequel la demande tendant à ce que la maladie soit reconnue comme ayant été contractée dans l'exercice des fonctions doit être présentée par le fonctionnaire. Ce délai de quatre ans ne peut, en conséquence, être opposé aux fonctionnaires territoriaux qui demandent, en application de l'article 23 du décret du 30 juillet 1987 cité au point 2, à ce que leur maladie soit reconnue comme ayant été contractée dans l'exercice de leurs fonctions.<br/>
<br/>
              5. Le présent avis sera notifié à la cour administrative d'appel de Versailles, à M. A... B..., au département des Yvelines, au ministre de l'intérieur et au ministre de l'action et des comptes publics. <br/>
<br/>
              Il sera publié au Journal officiel de la République française.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-05-04-02 FONCTIONNAIRES ET AGENTS PUBLICS. POSITIONS. CONGÉS. CONGÉS DE LONGUE DURÉE. - PLACEMENT EN CONGÉ DE LONGUE DURÉE DES FONCTIONNAIRES TERRITORIAUX (ART. 23 DU DÉCRET DU 30 JUILLET 1987) - APPLICATION DU DÉLAI DE QUATRE ANS POUR INTRODUIRE UNE DEMANDE TENDANT À CE QUE LA MALADIE SOIT RECONNUE COMME AYANT ÉTÉ CONTRACTÉE EN SERVICE (ART. 32 DU DÉCRET DU 14 MARS 1986) - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">36-07-01-03 FONCTIONNAIRES ET AGENTS PUBLICS. STATUTS, DROITS, OBLIGATIONS ET GARANTIES. STATUT GÉNÉRAL DES FONCTIONNAIRES DE L'ÉTAT ET DES COLLECTIVITÉS LOCALES. DISPOSITIONS STATUTAIRES RELATIVES À LA FONCTION PUBLIQUE TERRITORIALE (LOI DU 26 JANVIER 1984). - PLACEMENT EN CONGÉ DE LONGUE DURÉE DES FONCTIONNAIRES TERRITORIAUX (ART. 23 DU DÉCRET DU 30 JUILLET 1987) - APPLICATION DU DÉLAI DE QUATRE ANS POUR INTRODUIRE UNE DEMANDE TENDANT À CE QUE LA MALADIE SOIT RECONNUE COMME AYANT ÉTÉ CONTRACTÉE EN SERVICE (ART. 32 DU DÉCRET DU 14 MARS 1986) - ABSENCE.
</SCT>
<ANA ID="9A"> 36-05-04-02 Les fonctionnaires territoriaux sont régis, s'agissant de l'organisation des comités médicaux, des conditions d'aptitude physique et du régime des congés de maladie, par le décret n° 87-602 du 30 juillet 1987. Aucune disposition de ce décret ni aucun autre texte réglementaire ou principe général ne rend applicables aux fonctionnaires territoriaux les dispositions de l'article 32 du décret n° 86-442 du 14 mars 1986 relatives au délai de quatre ans dans lequel la demande tendant à ce que la maladie soit reconnue comme ayant été contractée dans l'exercice des fonctions doit être présentée par le fonctionnaire. Ce délai de quatre ans ne peut, en conséquence, être opposé aux fonctionnaires territoriaux qui demandent, en application de l'article 23 du décret du 30 juillet 1987 à ce que leur maladie soit reconnue comme ayant été contractée dans l'exercice de leurs fonctions.</ANA>
<ANA ID="9B"> 36-07-01-03 Les fonctionnaires territoriaux sont régis, s'agissant de l'organisation des comités médicaux, des conditions d'aptitude physique et du régime des congés de maladie, par le décret n° 87-602 du 30 juillet 1987. Aucune disposition de ce décret ni aucun autre texte réglementaire ou principe général ne rend applicables aux fonctionnaires territoriaux les dispositions de l'article 32 du décret n° 86-442 du 14 mars 1986 relatives au délai de quatre ans dans lequel la demande tendant à ce que la maladie soit reconnue comme ayant été contractée dans l'exercice des fonctions doit être présentée par le fonctionnaire. Ce délai de quatre ans ne peut, en conséquence, être opposé aux fonctionnaires territoriaux qui demandent, en application de l'article 23 du décret du 30 juillet 1987 à ce que leur maladie soit reconnue comme ayant été contractée dans l'exercice de leurs fonctions.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
