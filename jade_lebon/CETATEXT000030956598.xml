<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030956598</ID>
<ANCIEN_ID>JG_L_2015_07_000000372410</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/95/65/CETATEXT000030956598.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème / 4ème SSR, 27/07/2015, 372410</TITRE>
<DATE_DEC>2015-07-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>372410</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème / 4ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>BLONDEL</AVOCATS>
<RAPPORTEUR>M. Jean-Dominique Langlais</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:372410.20150727</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Fromagère de Charchigné a demandé au tribunal administratif de Nantes de condamner l'Etat à l'indemniser, en application des dispositions alors en vigueur de l'article L. 2216-3 du code général des collectivités territoriales, des préjudices ayant résulté pour elle du blocage de ses camions de collecte de lait par des attroupements intervenus entre le 21 et le 25 mai 2009 à Saint-Aignan-de-Couptrain (Mayenne). Par un jugement n° 1001215 du 24 décembre 2010, le tribunal administratif a jugé que la responsabilité de l'Etat était engagée à ce titre et l'a condamné à verser à la société requérante la somme de 12 766 euros.<br/>
<br/>
              Par un arrêt n° 11NT00628 du 3 février 2012, la cour administrative d'appel de Nantes, statuant sur l'appel de la société Fromagère de Charchigné, a jugé que la responsabilité de l'Etat était engagée et a ordonné une expertise afin que soit déterminé le préjudice de la société requérante. Par un arrêt n° 11NT00628 du 26 juillet 2013, elle a porté à 170 155,14 euros la somme mise à la charge de l'Etat et rejeté le surplus des conclusions de la société Fromagère de Charchigné.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 26 septembre et 20 décembre 2013 au secrétariat du contentieux du Conseil d'Etat, la société Fromagère de Charchigné demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'arrêt n° 11NT00628 du 26 juillet 2013 de la cour administrative d'appel de Nantes en tant qu'il rejette le surplus des conclusions de son appel ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire intégralement droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
              Vu :<br/>
<br/>
              - le code général des collectivités territoriales ;<br/>
<br/>
              - le code de la route ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Langlais, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Blondel, avocat de la société Fromagère de Charchigné ;<br/>
<br/>
<br/>
<br/>
<br/>Sur le pourvoi incident du ministre de l'intérieur : <br/>
<br/>
              1. Considérant qu'aux termes de l'article R. 821-1 du code de justice administrative : " Sauf dispositions contraires, le délai de recours en cassation est de deux mois... " ; qu'aucune disposition législative ou réglementaire ni aucune règle générale de procédure ne proroge le délai du pourvoi en cassation contre un arrêt avant dire droit jusqu'à l'expiration du délai de pourvoi en cassation contre l'arrêt définitif ; que l'arrêt avant dire droit du 3 février 2012 par lequel la cour administrative d'appel de Nantes a retenu que la responsabilité de l'Etat était engagée à l'égard de la société Fromagère de Charchigné sur le fondement de l'article L. 2216-3 du code général des collectivités territoriales n'a fait l'objet d'aucun pourvoi en cassation dans les deux mois suivant sa notification régulière aux parties ; qu'il en résulte que cet arrêt est devenu définitif ; que le pourvoi incident par lequel le ministre de l'intérieur conteste le principe de la responsabilité de l'Etat à l'égard de la société requérante retenu par l'arrêt avant dire droit du 3 février 2012 est, par suite, irrecevable ; <br/>
<br/>
              Sur le pourvoi de la société Fromagère de Charchigné : <br/>
<br/>
              2. Considérant que la société Fromagère de Charchigné a dû interrompre son activité en raison d'un délit d'entrave engageant la responsabilité de l'Etat au titre des dispositions de l'article L. 2216-3 du code général des collectivités territoriales ; que la réparation intégrale de son préjudice commercial supposait qu'elle fût replacée dans la situation qui aurait été la sienne si cette interruption ne s'était pas produite ; qu'en vue d'assurer cette réparation, il incombait aux juges du fond de lui accorder une indemnité correspondant aux pertes de recettes qu'elle avait subies, diminuées des charges qu'elle n'avait pas eu à exposer et augmentées, le cas échéant, des charges supplémentaires provoquées par l'interruption de son activité ; que l'octroi d'une indemnité ainsi déterminée assure la réparation du préjudice résultant de l'impossibilité de couvrir les charges fixes par des recettes d'exploitation et, le cas échéant, du préjudice résultant d'une perte de bénéfice ; <br/>
<br/>
              3. Considérant qu'il ressort de ses écritures que la société Fromagère de Charchigné avait demandé à la cour administrative d'appel de réparer les préjudices ayant résulté pour elle de l'impossibilité de couvrir ses charges fixes et de la perte de son bénéfice ; que l'arrêt attaqué énonce qu'elle ne saurait prétendre à réparation au titre de l'impossibilité de couvrir ses charges fixes, puis détermine l'indemnité mise à la charge de l'Etat par référence à la seule perte d'une marge dont il ne définit ni la nature ni les éléments de calcul ; qu'en évaluant ainsi la réparation à laquelle la société pouvait prétendre, la cour a méconnu le principe de réparation intégrale du préjudice ; que son arrêt doit, par suite, être annulé ; <br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à la société Fromagère de Charchigné au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : Le pourvoi incident du ministre de l'intérieur est rejeté.<br/>
Article 2 : L'arrêt de la cour administrative d'appel de Nantes du 26 juillet 2013 est annulé.<br/>
Article 3 : L'affaire est renvoyée à la cour administrative d'appel de Nantes.<br/>
Article 4 : L'Etat versera la somme de 3 000 euros à la société Fromagère de Charchigné au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 5 : La présente décision sera notifiée à la société Fromagère de Charchigné et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">60-04-03-02-01 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RÉPARATION. ÉVALUATION DU PRÉJUDICE. PRÉJUDICE MATÉRIEL. PERTE DE REVENUS. - INDEMNISATION D'UNE PERTE DE REVENUS COMMERCIAUX - INCLUSION - PRÉJUDICE RÉSULTANT DE L'IMPOSSIBILITÉ DE COUVRIR LES CHARGES FIXES.
</SCT>
<ANA ID="9A"> 60-04-03-02-01 En application du principe de réparation intégrale, la victime d'un préjudice de perte de recettes commerciales peut demander réparation de l'impossibilité dans laquelle elle s'est trouvée de couvrir les charges fixes de son exploitation.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
