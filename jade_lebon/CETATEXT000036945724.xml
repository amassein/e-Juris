<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036945724</ID>
<ANCIEN_ID>JG_L_2018_05_000000404382</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/94/57/CETATEXT000036945724.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème et 8ème chambres réunies, 25/05/2018, 404382</TITRE>
<DATE_DEC>2018-05-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>404382</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème et 8ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MONOD, COLIN, STOCLET</AVOCATS>
<RAPPORTEUR>M. Géraud Sajust de Bergues</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:404382.20180525</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire, un mémoire complémentaire, un mémoire en réplique et un nouveau mémoire, enregistrés les 11 octobre 2016, 11 janvier, 21 juin et 15 septembre 2017 au secrétariat du contentieux du Conseil d'Etat, la société OCEA demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision n° 16-DCC-120 du 28 juillet 2016 par laquelle l'Autorité de la concurrence a autorisé la création d'une entreprise commune de plein exercice entre les sociétés DCNS et Piriou ;<br/>
<br/>
              2°) de mettre à la charge de l'Autorité de la concurrence la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de commerce ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Géraud Sajust de Bergues, conseiller d'Etat, <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Monod, Colin, Stoclet, avocat de la société OCEA ;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. La société OCEA, qui est notamment spécialisée dans la construction de navires patrouilleurs en aluminium destinés à l'action de l'Etat en mer, demande l'annulation pour excès de pouvoir de la décision n° 16-DCC-120 du 28 juillet 2016 par laquelle l'Autorité de la concurrence a autorisé, sans condition, la création d'une entreprise commune de plein exercice entre les sociétés DCNS, devenue Naval Group, et Piriou. Cette opération de concentration au sens de l'article L. 430-1 du code de commerce, consistant en la transformation de leur entreprise commune Kership, créée en 2013, en entreprise commune de plein exercice, fait suite à la mise en commun des savoir-faire complémentaires de la société DCNS dans la conception de systèmes navals de défense et dans l'intégration de systèmes de communication susceptibles d'équiper des navires faiblement armés et de la société Piriou dans la construction de navires non armés d'une longueur inférieure à 95 mètres, conçus selon des standards civils. <br/>
<br/>
              Sur la légalité externe de la décision attaquée :<br/>
<br/>
              2. En vertu du point 222 des lignes directrices de l'Autorité de la concurrence relatives au contrôle des concentrations : " Lorsque l'opération le nécessite, par exemple lorsqu'elle intervient sur un marché qui n'a jamais été analysé par les autorités de concurrence ou dont l'analyse est ancienne, ou encore lorsqu'il apparaît qu'elle est susceptible de soulever des questions de concurrence, le service des concentrations réalise un test de marché. Pour ce faire, des questionnaires sont adressés aux principaux clients, fournisseurs et aux concurrents des entreprises concernées, et des réunions spécifiques peuvent être organisées (...) ".<br/>
<br/>
              3. Si l'Autorité de la concurrence était tenue de suivre, sauf circonstances particulières, la méthode d'analyse qu'elle s'était donnée au point 222 de ses lignes directrices, elle a, en l'espèce, pris en compte la pratique décisionnelle de la Commission européenne, qui était suffisamment récente, relative à la détermination des marchés de produits et des marchés géographiques en matière de construction navale militaire et considéré que, compte tenu de ses caractéristiques et de son contexte, l'opération de concentration, qui favorisait l'apparition d'un nouvel opérateur pour la construction de navires faiblement armés, ne soulevait pas de questions de concurrence. Elle a pu, dès lors, à bon droit considérer qu'elle disposait des informations nécessaires pour prendre sa décision sans devoir recourir à une enquête ou à un test de marché. <br/>
<br/>
              Sur la légalité interne de la décision attaquée :<br/>
<br/>
              En ce qui concerne le marché de la construction navale : <br/>
<br/>
              4. En premier lieu, l'Autorité de la concurrence n'a pas commis d'erreur d'appréciation en retenant comme marché de produit pertinent celui des petits bâtiments de guerre, dès lors, d'une part, qu'elle a tenu compte, conformément au point 347 de ses lignes directrices, de la pratique décisionnelle de la Commission européenne, qu'elle a correctement interprétée, laquelle distingue, en ce qui concerne les navires militaires, un marché des petits bâtiments de guerre, au sein duquel les corvettes, les bateaux de patrouille et autres navires de surface sont substituables du point de vue de la demande et susceptibles de remplir les mêmes missions, et, d'autre part, que l'activité de l'entreprise commune Kership consiste dans la fabrication selon des standards civils de navires de moins de 95 mètres à destination de l'action de l'Etat en mer, pouvant être utilisés pour des missions de nature civile, mais intégrant toujours un système de combat. <br/>
<br/>
              5. En deuxième lieu, si l'Autorité de la concurrence n'est pas compétente pour se prononcer sur les effets éventuels d'une opération de concentration sur un marché ne comprenant pas le territoire français, elle est, en principe, compétente pour apprécier les répercussions que pourraient avoir, sur le marché pertinent comprenant ce territoire, les effets produits par cette opération dans le reste du monde. <br/>
<br/>
              6. En conséquence, dès lors qu'elle avait défini le marché national français comme le marché géographique pertinent, au motif, conforme à la pratique décisionnelle de la Commission européenne, qu'il existait une industrie nationale capable de répondre à la demande du ministère de la défense, ce que la société OCEA ne conteste pas, l'Autorité de la concurrence n'a pas commis d'erreur d'appréciation en s'abstenant d'analyser les effets de l'opération de concentration sur le marché " reste du monde ".<br/>
<br/>
              7. De même, si la société OCEA soutient, au demeurant sans l'étayer, que l'opération de concentration aboutirait à verrouiller l'accès aux intrants nécessaires pour concurrencer l'entreprise commune Kership sur le marché à l'exportation, tels que les licences d'exportation délivrées par l'Etat, les assurances de crédit à l'exportation accordées par Bpifrance Assurance Export et les services d'intermédiation, fournis notamment par la société ODAS, pour négocier des marchés, il s'agirait d'obstacles à l'entrée sur le marché " reste du monde " et donc d'effets de l'opération de concentration que l'Autorité de la concurrence n'était pas compétente pour analyser. <br/>
<br/>
              8. Si la société OCEA soutient aussi que l'opération de concentration serait susceptible d'avoir, de manière indirecte, une influence substantielle sur le marché français en raison du développement des activités de l'entreprise commune Kership sur " le marché reste du monde ", elle n'apporte aucun élément de nature à permettre de l'apprécier. <br/>
<br/>
              En ce qui concerne le marché de la réparation et de la maintenance de navires non complexes : <br/>
<br/>
              9. En ce qui concerne les effets verticaux de la concentration, l'Autorité de la concurrence a relevé, d'une part, que, sur le marché aval de l'entretien périodique des navires non complexes, les sociétés DCNS et Piriou avaient des parts de marché globalement inférieures à 30 %, que leurs concurrents étaient nombreux et qu'elles ne disposaient pas d'un pouvoir de marché suffisant pour servir d'appui à un éventuel effet de levier. Elle a relevé, d'autre part, que, sur le marché amont de la construction des petits bâtiments de guerre, l'entreprise commune Kership, en collaboration avec ses sociétés mères, avait remporté deux des sept appels d'offres lancés depuis 2013, soit moins de 30 % des appels d'offre, qu'elle faisait face à un nombre important de concurrents et ne disposait pas à la date de l'opération d'un pouvoir de marché suffisant pour imposer à ses clients le recours à ses services de maintenance ou celui de ses sociétés mères.  La société Océa se borne à soutenir que l'Autorité de la concurrence aurait dû prendre en compte non la proportion des appels d'offre mais leur valeur qui était particulièrement importante, sans démontrer que cette méthode aurait établi que l'opération aurait de ce fait conduit à un effet de verrouillage anticoncurrentiel. De plus, la pratique décisionnelle de la Commission européenne n'exclut pas d'apprécier les parts de marché en pourcentage et non en valeur et l'Autorité de la concurrence pouvait retenir cette méthode dès lors que le nombre d'appels d'offre était limité et de valeur hétérogène. De plus, les parts de marché ne sont qu'un des éléments pris en compte par l'Autorité de la concurrence pour apprécier les effets verticaux. Par suite, le moyen tiré de ce que l'opération était de nature à porter atteinte à la concurrence par le biais d'effets verticaux doit être écarté.<br/>
<br/>
              10. Si la société OCEA soutient que l'Autorité de la concurrence a commis des erreurs d'appréciation, en ne tenant pas compte de ce que, étant détenue à 64 % par l'Etat et bénéficiant de commandes publiques sans mise en concurrence pour une part très significative de son activité, la société DCNS serait en mesure de permettre à l'entreprise commune Kership d'adopter une stratégie de prédation sur le marché des navires patrouilleurs, en pratiquant des prix très inférieurs au coût de revient de ses concurrents, ce qui, au surplus, serait susceptible de constituer une aide d'Etat illégale, le lien entre cette entreprise commune et la société DCNS préexistait à l'opération de concentration et, en tout état de cause, la société requérante n'assortit ces allégations, qui sont d'ailleurs sans rapport avec l'opération de concentration, d'aucun élément précis de nature à les étayer. Par suite, ce moyen doit être écarté.<br/>
<br/>
              11. Il résulte de tout ce qui précède, et sans qu'il soit besoin d'examiner la recevabilité de la requête, que la société OCEA n'est pas fondée à demander l'annulation de la décision de l'Autorité de la concurrence qu'elle attaque.  <br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              12. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que la somme demandée par la société OCEA soit mise à la charge de l'Autorité de la concurrence, qui n'est pas la partie perdante dans la présente instance. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de cette société le versement à la société Naval Group, d'une part, et à la société Piriou, d'autre part, de la somme de 3 000 euros au titre des mêmes dispositions.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la société OCEA est rejetée.<br/>
Article 2 : La société OCEA versera à la société Naval Group, d'une part, et à la société Piriou, d'autre part, la somme de 3000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la société OCEA, à l'Autorité de la concurrence, à la société Naval Group, à la société Piriou et au ministre de l'économie et des finances.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-01-05-03-03 ACTES LÉGISLATIFS ET ADMINISTRATIFS. DIFFÉRENTES CATÉGORIES D'ACTES. ACTES ADMINISTRATIFS - NOTION. INSTRUCTIONS ET CIRCULAIRES. DIRECTIVES ADMINISTRATIVES. - LIGNES DIRECTRICES ÉMISES PAR L'AUTORITÉ DE LA CONCURRENCE - CARACTÈRE CONTRAIGNANT DE CES LIGNES POUR ELLE-MÊME - EXISTENCE, SAUF CIRCONSTANCES PARTICULIÈRES [RJ1] - ESPÈCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">14-05-005 COMMERCE, INDUSTRIE, INTERVENTION ÉCONOMIQUE DE LA PUISSANCE PUBLIQUE. DÉFENSE DE LA CONCURRENCE. AUTORITÉ DE LA CONCURRENCE. - 1) LIGNES DIRECTRICES - CARACTÈRE CONTRAIGNANT DE CES LIGNES POUR ELLE-MÊME - EXISTENCE, SAUF CIRCONSTANCES PARTICULIÈRES - ESPÈCE - 2) COMPÉTENCE POUR APPRÉCIER LES EFFETS DANS LE RESTE DU MONDE D'UNE OPÉRATION DE CONCENTRATION SUR UN MARCHÉ PERTINENT COMPRENANT LE TERRITOIRE FRANÇAIS.
</SCT>
<ANA ID="9A"> 01-01-05-03-03 Si l'Autorité de la concurrence est tenue de suivre, sauf circonstances particulières, la méthode d'analyse qu'elle s'est donnée au point 222 de ses lignes directrices, elle a, en l'espèce, pris en compte la pratique décisionnelle de la Commission européenne, qui était assez récente, relative à la détermination des marchés de produits et des marchés géographiques en matière de construction navale militaire et considéré que, compte tenu de ses caractéristiques et de son contexte, l'opération de concentration, qui favorisait l'apparition d'un nouvel opérateur pour la construction de navires faiblement armés, ne soulevait pas de questions de concurrence. Elle a pu, dès lors, à bon droit considérer qu'elle disposait des informations nécessaires pour prendre sa décision sans devoir recourir à une enquête ou à un test de marché.</ANA>
<ANA ID="9B"> 14-05-005 1) Si l'Autorité de la concurrence est tenue de suivre, sauf circonstances particulières, la méthode d'analyse qu'elle s'est donnée au point 222 de ses lignes directrices, elle a, en l'espèce, pris en compte la pratique décisionnelle de la Commission européenne, qui était assez récente, relative à la détermination des marchés de produits et des marchés géographiques en matière de construction navale militaire et considéré que, compte tenu de ses caractéristiques et de son contexte, l'opération de concentration, qui favorisait l'apparition d'un nouvel opérateur pour la construction de navires faiblement armés, ne soulevait pas de questions de concurrence. Elle a pu, dès lors, à bon droit considérer qu'elle disposait des informations nécessaires pour prendre sa décision sans devoir recourir à une enquête ou à un test de marché.,,,2) Si l'Autorité de la concurrence n'est pas compétente pour se prononcer sur les effets éventuels d'une opération de concentration sur un marché ne comprenant pas le territoire français, elle est, en principe, compétente pour apprécier les répercussions que pourraient avoir, sur le marché pertinent comprenant ce territoire, les effets produits par cette opération dans le reste du monde.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 20 mars 2017, Région Aquitaine-Limousin-Poitou-Charentes, n° 401751, p. 99.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
