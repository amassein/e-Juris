<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037080606</ID>
<ANCIEN_ID>JG_L_2018_06_000000416325</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/08/06/CETATEXT000037080606.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 18/06/2018, 416325</TITRE>
<DATE_DEC>2018-06-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>416325</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Recours ds l'intérêt de la loi</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Louise Bréhier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Guillaume Odinet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:416325.20180618</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B...A...a demandé au tribunal administratif de Poitiers d'annuler pour excès de pouvoir l'arrêté du 2 mars 2015 par lequel le préfet de la Vienne a déclaré d'utilité publique le projet d'acquisition des voies desservant le hameau de Feuillebert par la commune de Romagne et l'acquisition des terrains nécessaires à la réalisation de ce projet et l'arrêté du 18 septembre 2015 par lequel le préfet de la Vienne a déclaré cessibles les immeubles nécessaires à la réalisation de ce projet. Par un jugement n° 1501185, 1601052 du 5 juillet 2017, le tribunal administratif a fait partiellement droit à ses demandes et annulé l'arrêté du 2 mars 2015 en tant qu'il concerne la partie centrale de la parcelle K 86 telle qu'elle est définie au point 9 du jugement et représentée en croisillons sur le plan parcellaire annexé au jugement ainsi que l'arrêté du 18 septembre 2015 en tant qu'il concerne la partie sud-ouest de la parcelle K 86 jusqu'à une ligne droite à tracer dans la continuation de la façade orientée au sud-ouest de la maison édifiée sur cette parcelle. <br/>
<br/>
              Par un recours et un nouveau mémoire, enregistrés les 6 décembre 2017 et 29 mai 2018 au secrétariat du contentieux du Conseil d'Etat, le ministre d'Etat, ministre de l'intérieur demande au Conseil d'Etat d'annuler ce jugement dans l'intérêt de la loi.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu le code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Louise Bréhier, auditrice, <br/>
<br/>
              - les conclusions de M. Guillaume Odinet, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'en vertu des principes généraux de procédure, un recours peut être formé dans l'intérêt de la loi par un ministre intéressé devant le Conseil d'Etat contre tout  jugement d'une juridiction administrative ayant acquis l'autorité de chose jugée, dès lors que ce jugement est devenu irrévocable ; que doit être regardé comme irrévocable tout jugement qui n'est plus susceptible d'appel ou de cassation ; <br/>
<br/>
              2.	Considérant que Mme B...A...a demandé au tribunal administratif de Poitiers d'annuler pour excès de pouvoir les arrêtés des 2 mars et 18 septembre 2015 par lesquels le préfet de la Vienne a déclaré d'utilité publique le projet d'acquisition des voies desservant le hameau de Feuillebert par la commune de Romagne et l'acquisition des terrains nécessaires à la réalisation de ce projet et cessibles les immeubles nécessaires à cette réalisation ; que, par un jugement du 5 juillet 2017, le tribunal administratif a fait partiellement droit à ses demandes ; que le ministre d'Etat, ministre de l'intérieur, demande l'annulation de ce jugement dans l'intérêt de la loi ; <br/>
<br/>
              3.	Considérant que pour contester, par la voie du recours dans l'intérêt de la loi, le jugement par lequel le tribunal administratif de Poitiers a partiellement annulé les arrêtés du préfet de la Vienne des 2 mars et 18 septembre 2015, le ministre d'Etat, ministre de l'intérieur, soutient que le tribunal administratif a, en premier lieu, méconnu les dispositions de l'article R. 611-1 du code de justice administrative en n'appelant pas en cause la commune de Romagne ; qu'un tel moyen, qui tend à contester la régularité de la procédure en l'espèce et, au demeurant, n'affecte pas la régularité de la procédure à l'égard du requérant, ne peut être utilement présenté à l'appui d'un recours dans l'intérêt de la loi ; que, en deuxième lieu, le ministre d'Etat, ministre de l'intérieur soutient que le tribunal administratif a commis une erreur de droit et entaché sa décision de contradiction de motifs en entendant contrôler que l'expropriant n'était pas en mesure de réaliser l'opération projetée dans des conditions équivalentes sans recourir à l'expropriation mais en jugeant, sans lien avec l'exercice de ce contrôle, que l'opération emportait une atteinte excessive à la propriété privée, au regard de son intérêt public limité et, en dernier lieu, commis une erreur de droit en s'abstenant de constater que l'opération était divisible avant de procéder à l'annulation partielle des arrêtés litigieux ; que de tels moyens, qui reviennent à remettre en cause l'appréciation d'espèce portée par le tribunal administratif sur l'utilité publique de l'opération, ne peuvent être utilement présentés à l'appui d'un recours dans l'intérêt de la loi ; que, par suite, le ministre n'est pas fondé à demander l'annulation dans l'intérêt de la loi du jugement qu'il conteste ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le recours du ministre d'Etat, ministre de l'intérieur est rejeté. <br/>
<br/>
Article 2 : La présente décision sera notifiée au ministre d'Etat, ministre de l'intérieur. Copie en sera adressée à Mme B...A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-08-07 PROCÉDURE. VOIES DE RECOURS. RECOURS DANS L'INTÉRÊT DE LA LOI. - RECEVABILITÉ - RECOURS POUVANT ÊTRE FORMÉ PAR LE MINISTRE INTÉRESSÉ CONTRE TOUT JUGEMENT AYANT ACQUIS L'AUTORITÉ DE LA CHOSE JUGÉ DÈS LORS QUE CE JUGEMENT EST DEVENU IRRÉVOCABLE [RJ1] - NOTION DE JUGEMENT IRRÉVOCABLE - JUGEMENT N'ÉTANT PLUS SUSCEPTIBLE D'APPEL OU DE CASSATION.
</SCT>
<ANA ID="9A"> 54-08-07 En vertu des principes généraux de procédure, un recours peut être formé dans l'intérêt de la loi par un ministre intéressé devant le Conseil d'Etat contre tout jugement d'une juridiction administrative ayant acquis l'autorité de chose jugée, dès lors que ce jugement est devenu irrévocable. Doit être regardé comme irrévocable tout jugement qui n'est plus susceptible d'appel ou de cassation.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., s'agissant des conditions de recevabilité du recours dans l'intérêt de la loi, CE, 11 avril 2018, Ministre de l'intérieur c/,,  n° 409648, à publier au Recueil. Comp., s'agissant des conditions de recevabilité de l'action en désaveu d'avocat, CE, 17 novembre 2010,,, n° 312594, p. 441.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
