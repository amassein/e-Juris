<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035245521</ID>
<ANCIEN_ID>JG_L_2017_07_000000397990</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/24/55/CETATEXT000035245521.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 1ère chambres réunies, 19/07/2017, 397990</TITRE>
<DATE_DEC>2017-07-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>397990</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS ; SCP OHL, VEXLIARD</AVOCATS>
<RAPPORTEUR>M. Jean-Baptiste de Froment</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Louis Dutheillet de Lamothe</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:397990.20170719</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 16 mars et 16 juin 2016, et 5 avril 2017, au secrétariat du contentieux du Conseil d'Etat, M. C...A...B...et la société Bryan A...and Co Limited demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler la décision du 11 janvier 2016 par laquelle la commission des sanctions de l'Autorité des marchés financiers a prononcé un blâme assorti d'une sanction pécuniaire de 70 000 euros à l'encontre de la société Bryan A...and Co Limited et un blâme assorti d'une sanction pécuniaire de 30 000 euros à l'encontre de M.A..., en sa qualité de représentant légal de cette société, et a ordonné la publication de sa décision sur le site internet de l'Autorité des marchés financiers ;<br/>
<br/>
              2°) de mettre à la charge de l'Autorité des marchés financiers la somme de 10 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la directive 2003/6/CE du Parlement européen et du Conseil ;<br/>
              - la directive 2004/72/CE de la Commission du 29 avril 2004 ; <br/>
              - le code monétaire et financier ;<br/>
              - le règlement général de l'Autorité des marchés financiers ; <br/>
              - la décision n° 397990 du 14 septembre 2016 par laquelle le Conseil d'Etat, statuant au contentieux, a jugé qu'il n'y avait pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par M. A...B...et la société BryanA... and Co Limited ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Baptiste de Froment, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Louis Dutheillet de Lamothe, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de la société Bryan A...et Co Limited et autre et à la SCP Ohl, Vexliard, avocat de l'Autorité des marchés financiers.<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que la société Bryan A...and Co Limited, prestataire de services d'investissement, a été mandatée par la société Novagali Pharma SA aux fins de préparer et exécuter son opération d'introduction en bourse ; que, par la décision attaquée du 11 janvier 2016, la commission des sanctions de l'Autorité des marchés financiers (AMF) a respectivement infligé un blâme assorti d'une sanction pécuniaire de 70 000 euros à la société Bryan A...and Co Limited et un blâme assorti d'une sanction pécuniaire de 30 000 euros à M. C...A...B..., en sa qualité de dirigeant de cette société, et décidé de publier ces sanctions de façon non anonymisée pour avoir manqué à leur obligation d'agir d'une manière honnête, loyale et professionnelle qui sert au mieux l'intérêt des clients et favorise l'intégrité du marché, en méconnaissance des dispositions des articles L. 533-1 du code monétaire et financier et 314-3 du règlement général de l'AMF ;<br/>
<br/>
              	2. Considérant qu'en vertu de l'article L. 621-15 du code monétaire et financier, lorsque le collège de l'AMF a décidé l'ouverture d'une procédure de sanction, il notifie les griefs aux personnes concernées et les transmet à la commission des sanctions, laquelle désigne un rapporteur parmi ses membres ; qu'en vertu de l'article R. 621-39 du même code, dans les cas où il estime que les griefs doivent être complétés, le rapporteur saisit le collège, qui statue sur sa demande ; qu'il résulte de ces dispositions que la commission des sanctions ne peut infliger une sanction que sur le fondement de griefs ayant été préalablement notifiés ; que la notification de griefs doit indiquer les principaux agissements qui sont reprochés à la personne mise en cause, ainsi que la nature des obligations méconnues, afin de lui permettre de se défendre en présentant ses observations ; que ni ces dispositions ni aucun principe ne s'opposent à ce que la commission se fonde sur des circonstances de fait qui ne figuraient pas dans la notification de griefs, dès lors qu'elles se rattachent aux griefs régulièrement notifiés ; <br/>
<br/>
              3.  Considérant qu'il résulte de l'instruction que, le 4 mars 2010, la société Novagali a confié à la société Bryan A...un mandat aux fins de préparer et d'exécuter son introduction en bourse par admission d'actions nouvelles aux négociations sur le compartiment C d'Euronext Paris pour un montant de quelque 25 millions d'euros ; que la mise en oeuvre de l'opération se révélant difficile, la société Bryan A...a pris la décision de souscrire directement des titres Novagali pour un montant d'un million d'euros ; que, parallèlement, la société Z a aussi souscrit des titres Novagali pour un montant également d'un million d'euros ; que, le 3 août 2010, la société Bryan A...a racheté les titres Novagali à la société Z, portant ainsi son engagement à 2 millions d'euros sur les 22 millions levés lors de l'introduction en bourse ; <br/>
<br/>
              4. Considérant que, dans la lettre de notification des griefs du 19 juin 2014, le président du collège de l'AMF indique qu'est susceptible d'être retenu à l'encontre des requérants le manquement à l'obligation d'agir d'une manière honnête, loyale et professionnelle qui sert au mieux l'intérêt des clients et favorise l'intégrité du marché ; qu'après avoir cité les articles L. 533-1 du code monétaire et financier et 314-3 du règlement général de l'AMF, applicables à ce manquement, la notification relève que la société Bryan A...est suspectée d'avoir, à l'occasion du service de placement des titres de la société Novagali, lors de son introduction en bourse le 21 juillet 2010, présenté un résultat d'allocation des titres Novagali " artificiellement augmenté et donc plus favorable à la réalité du placement effectué " et d'avoir ainsi " pu porter atteinte à l'intérêt de Novagali et à l'intégrité du marché " ; qu'il est ensuite précisément reproché à la société Bryan A...de n'avoir communiqué le résultat de l'allocation des titres " qu'à Novagali ", sans le porter à la connaissance " ni des personnes ayant souscrit à l'introduction en bourse, ni des actionnaires ayant acquis des titres Novagali sur le marché secondaire " ; qu'il ressort toutefois de la décision attaquée que, pour sanctionner les requérants, la commission des sanctions de l'AMF s'est fondée non pas sur un défaut d'information des personnes ayant souscrit à l'introduction en bourse et des actionnaires ayant acquis des titres, ainsi que cela avait été notifié à la société BryanA..., mais, à l'inverse, sur la circonstance que le résultat final de l'allocation des titres n'avait pas été présenté de façon sincère à la société Novagali, client de la société BryanA..., cette dernière ayant informée la société Novagali de la souscription opérée par la société Z mais pas du rachat, dans un second temps, de ces titres par elle-même ; que, par suite, le grief retenu ne peut, dans les circonstances de l'espèce, être regardé comme ayant été notifié aux requérants ; qu'il en résulte, sans qu'il soit besoin d'examiner les autres moyens de la requête, que M. C...A...B...et la société Bryan A...and Co Limited sont fondés, pour ce motif, à demander l'annulation de la décision qu'ils attaquent ; <br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'AMF la somme de 1 500 euros à verser tant à la société Bryan A...qu'à M. A... B...au titre des dispositions de l'article L. 761-1 du code de justice administrative ; qu'en revanche, ces mêmes dispositions font obstacle à ce qu'une somme soit mise à la charge des requérants, qui ne sont pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La décision du 11 janvier 2016 par laquelle la commission des sanctions de l'Autorité des marchés financiers a prononcé un blâme assorti d'une sanction pécuniaire de 70 000 euros à l'encontre de la société Bryan A...and Co Limited et un blâme assorti d'une sanction pécuniaire de 30 000 euros à l'encontre de M. A...B..., et a ordonné la publication de sa décision sur le site internet de l'Autorité des marchés financiers est annulée.<br/>
Article 2 : La présente décision sera publiée sur le site internet de l'AMF.<br/>
Article 3 : L'Autorité des marchés financiers versera à M. C...A...B...et la société Bryan A...une somme de 1 500 euros chacun au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions de l'Autorité des marchés financiers présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.  <br/>
Article 5 : La présente décision sera notifiée à M. C... A...B..., à la société Bryan A...and Co Limited et à l'Autorité des marchés financiers.<br/>
Copie en sera transmise au ministre de l'économie et des finances.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">13-01-02-01 CAPITAUX, MONNAIE, BANQUES. CAPITAUX. OPÉRATIONS DE BOURSE. AUTORITÉ DES MARCHÉS FINANCIERS. - COMMISSION DES SANCTIONS - EXERCICE DU POUVOIR DE SANCTION - 1) POSSIBILITÉ DE SE FONDER SUR DES GRIEFS NON PRÉALABLEMENT NOTIFIÉS - A) ABSENCE - B) NOTION DE GRIEF [RJ1] - 2) POSSIBILITÉ DE SE FONDER SUR DES CIRCONSTANCES DE FAIT NE FIGURANT PAS DANS LA NOTIFICATION DE GRIEFS - EXISTENCE - CONDITION.
</SCT>
<ANA ID="9A"> 13-01-02-01 1) a) La commission des sanctions de l'Autorité des marchés financiers (AMF) ne peut infliger une sanction que sur le fondement de griefs ayant été préalablement notifiés.... ,,b) La notification de griefs doit indiquer les principaux agissements qui sont reprochés à la personne mise en cause, ainsi que la nature des obligations méconnues, afin de lui permettre de se défendre en présentant ses observations.... ,,2) Ni les articles L. 621-15 et R. 621-39 du code monétaire et financier, ni aucun principe ne s'opposent à ce que la commission se fonde sur des circonstances de fait qui ne figuraient pas dans la notification de griefs, dès lors qu'elles se rattachent aux griefs régulièrement notifiés.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., en matière ordinale, CE, 15 décembre 2010,,, n° 329246, T. pp. 833-961.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
