<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042065736</ID>
<ANCIEN_ID>JG_L_2020_06_000000423036</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/06/57/CETATEXT000042065736.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 1ère chambres réunies, 29/06/2020, 423036</TITRE>
<DATE_DEC>2020-06-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>423036</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>Mme Françoise Tomé</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CECHR:2020:423036.20200629</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Le conseil départemental de la Gironde de l'ordre des chirurgiens-dentistes a porté plainte contre M. A... B... devant la chambre disciplinaire de première instance d'Aquitaine de l'ordre des chirurgiens-dentistes. Par une décision du 19 septembre 2016, la chambre disciplinaire a infligé à M. B... la sanction de l'interdiction d'exercer la profession de chirurgien-dentiste pendant une durée de deux mois, dont un mois assorti du sursis. <br/>
<br/>
              Par une décision du 6 juin 2018, la chambre disciplinaire nationale de l'ordre des chirurgiens-dentistes a rejeté l'appel de M. B....<br/>
<br/>
              Par un pourvoi, enregistré le 8 août 2018 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette décision ; <br/>
<br/>
              2°) de mettre à la charge du conseil départemental de la Gironde de l'ordre des chirurgiens-dentistes la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la santé publique ; <br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Françoise Tomé, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Raphaël Chambon, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat de M. B... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Le conseil départemental de la Gironde de l'ordre des chirurgiens-dentistes a porté plainte contre M. B..., chirurgien-dentiste, en lui reprochant que son site internet professionnel comporte des mentions prohibées par le code de déontologie et s'abstienne de préciser des informations relatives à la société d'exercice libéral dans le cadre de laquelle il exerce sa profession. Par une décision du 19 septembre 2016, la chambre disciplinaire de première instance d'Aquitaine de l'ordre des chirurgiens-dentistes a infligé à M. B... la sanction de l'interdiction d'exercer sa profession pendant une durée de deux mois, dont un mois assorti du sursis. M. B... se pourvoit en cassation contre la décision du 6 juin 2018 par laquelle la chambre disciplinaire nationale de l'ordre des chirurgiens-dentistes a rejeté son appel contre cette décision.<br/>
<br/>
              2. D'une part, aux termes de l'article R. 4113-2 du code de la santé publique, applicable aux sociétés d'exercice libéral des professions médicales mentionnées à l'article R. 4113-1 : " Les actes et documents destinés aux tiers, notamment les lettres, factures, annonces et publications diverses émanant d'une société mentionnée à l'article R. 4113-1 indiquent : / 1° Sa dénomination sociale, précédée ou suivie immédiatement, selon le cas : / a) Soit de la mention " société d'exercice libéral à responsabilité limitée " ou de la mention " SELARL " ; / b) Soit de la mention " société d'exercice libéral à forme anonyme " ou de la mention " SELAFA " ; / c) Soit de la mention " société d'exercice libéral en commandite par actions " ou de la mention " SELCA " ; / d) Soit de la mention " société d'exercice libéral par actions simplifiée " ou de la mention " SELAS " ; / 2° L'indication de la profession exercée par la société ; / 3° L'énonciation du montant de son capital social et de son siège social ; / 4° La mention de son inscription au tableau de l'ordre ". Il résulte des termes mêmes de ces dispositions que celles-ci sont applicables aux seuls actes et documents destinés aux tiers émis par une société d'exercice libéral. A ce titre, elles s'appliquent à son site internet.<br/>
<br/>
              3. D'autre part, aux termes de l'article R. 4127-216 du code de la santé publique : " Les seules indications que le chirurgien-dentiste est autorisé à mentionner sur ses imprimés professionnels, notamment ses feuilles d'ordonnances, notes d'honoraires et cartes professionnelles, sont : / 1° Ses nom, prénoms, adresses postale et électronique, numéros de téléphone et de télécopie, jours et heures de consultation et ses numéros de comptes bancaires ; / 2° Sa qualité et sa spécialité ; / 3° Les diplômes, titres et fonctions reconnus par le Conseil national de l'ordre ; / 4° Les distinctions honorifiques reconnues par la République française ; / 5° La mention de l'adhésion à une association agréée prévue à l'article 64 de la loi de finances pour 1977 n° 76-1232 du 29 décembre 1976 ; / 6° Sa situation vis-à-vis des organismes d'assurance maladie obligatoires ; / 7° S'il exerce en société civile professionnelle ou en société d'exercice libéral, les noms des chirurgiens-dentistes associés et, en ce qui concerne les sociétés d'exercice libéral, les mentions prévues à l'article R. 4113-2 et le numéro d'inscription au registre du commerce et des sociétés ". Ces dispositions se bornent à dresser la liste des mentions qu'un chirurgien-dentiste est autorisé à faire figurer sur ses " imprimés professionnels " et, notamment, pour leur application, sur son site internet. Elles n'imposent pas, en revanche, qu'il y fasse figurer l'ensemble des catégories d'informations énumérées du 1° au 7° de cet article. <br/>
<br/>
              4. Il s'ensuit que la chambre disciplinaire nationale de l'ordre des chirurgiens-dentistes, qui a relevé que le site internet professionnel en litige était consacré à M. B... et non à la société d'exercice libéral dans le cadre de laquelle il exerce, a commis une erreur de droit en jugeant que M. B... avait manqué à ses obligations déontologiques en ne faisant pas figurer sur celui-ci les informations exigées par l'article R. 4113-2 du code de la santé publique cité au point 2 pour les seules sociétés d'exercice libéral et qu'un chirurgien-dentiste peut, en application des dispositions également précitées de l'article, R. 4127-216 du code de la santé publique, mentionner sur ses imprimés professionnels sans y être tenu.<br/>
<br/>
              5. Il résulte de ce qui précède que M. B... est fondé à demander l'annulation de la décision qu'il attaque. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du conseil départemental de la Gironde de l'ordre des chirurgiens-dentistes une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.  <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La décision de la chambre disciplinaire nationale de l'ordre des chirurgiens-dentistes du 6 juin 2018 est annulée. <br/>
Article 2 : L'affaire est renvoyée à la chambre disciplinaire nationale de l'ordre des chirurgiens-dentistes.<br/>
Article 3 : Le conseil départemental de la Gironde de l'ordre des chirurgiens-dentistes versera à M. B... une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à M. A... B... et au conseil départemental de la Gironde de l'ordre des chirurgiens-dentistes.<br/>
Copie en sera adressée au Conseil national de l'ordre des chirurgiens-dentistes.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">02-02-04 AFFICHAGE ET PUBLICITÉ. SUPPORTS PUBLICITAIRES AUTRES QUE L`AFFICHAGE. - MENTIONS DEVANT FIGURER SUR LES DOCUMENTS DESTINÉS AUX TIERS ÉMIS PAR UNE SOCIÉTÉ D'EXERCICE LIBÉRAL DE LA MÉDECINE (ART. R. 4113-2 DU CSP) - APPLICATION AU SITE INTERNET [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">55-03-02 PROFESSIONS, CHARGES ET OFFICES. CONDITIONS D'EXERCICE DES PROFESSIONS. CHIRURGIENS-DENTISTES. - 1) MENTIONS DEVANT FIGURER SUR LES DOCUMENTS DESTINÉS AUX TIERS (ART. R. 4113-2 DU CSP) - A) APPLICATION AUX SEULS DOCUMENTS ÉMIS PAR UNE SOCIÉTÉ D'EXERCICE LIBÉRAL - B) APPLICATION AU SITE INTERNET DE CETTE SOCIÉTÉ [RJ1] - 2) MENTIONS POUVANT FIGURER SUR LES IMPRIMÉS PROFESSIONNELS (ART. R. 4127-216 DU CSP) - MENTIONS OBLIGATOIRES - ABSENCE.
</SCT>
<ANA ID="9A"> 02-02-04 Il résulte des termes mêmes de l'article R. 4113-2 du code de la santé publique (CSP) que celui-ci est applicable aux seuls actes et documents destinés aux tiers émis par une société d'exercice libéral (SEL). A ce titre, il s'applique à son site internet.</ANA>
<ANA ID="9B"> 55-03-02 1) a) Il résulte des termes mêmes de l'article R. 4113-2 du code de la santé publique (CSP) que celui-ci est applicable aux seuls actes et documents destinés aux tiers émis par une société d'exercice libéral (SEL).... ,,b) A ce titre, il s'applique à son site internet.,,,2) L'article R. 4127-216 du CSP se borne à dresser la liste des mentions qu'un chirurgien-dentiste est autorisé à faire figurer sur ses imprimés professionnels, et notamment, pour leur application, sur son site internet. Il n'impose pas, en revanche, qu'il y fasse figurer l'ensemble des catégories d'informations énumérées du 1° au 7° de cet article.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., s'agissant des indications mentionnées à l'article R. 4127-216, CE, 27 avril 2012, M.,, n° 348259, p. 177.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
