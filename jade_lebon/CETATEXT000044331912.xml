<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044331912</ID>
<ANCIEN_ID>JG_L_2021_11_000000443978</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/33/19/CETATEXT000044331912.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 5ème chambres réunies, 15/11/2021, 443978</TITRE>
<DATE_DEC>2021-11-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>443978</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI</AVOCATS>
<RAPPORTEUR>Mme Rozen Noguellou</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier  Fuchs</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:443978.20211115</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. G... D... et la société Aéronord ont demandé au tribunal administratif de Paris de condamner l'Etat à les indemniser à hauteur de 699 704,34 euros pour la société Aéronord et de 2 333 749,09 euros pour M. D..., au titre de la réparation des préjudices qu'ils estiment avoir subis à raison de la saisie, en Espagne, d'un hélicoptère leur appartenant, dans le cadre d'une procédure de commission rogatoire ordonnée par des magistrats français. Par un jugement n° 1808027 du 4 juillet 2019, le tribunal administratif a rejeté leur demande.<br/>
<br/>
              Par un arrêt n° 19PA02786 du 29 novembre 2018, la cour administrative d'appel de Paris a rejeté l'appel formé par M. D... et la société Aéronord contre ce jugement.  <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 10 septembre 2020, 10 décembre 2020 et 30 août 2021 au secrétariat du contentieux du Conseil d'Etat, M. D... et la société Aéronord demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit aux demandes qu'ils ont présentées devant la cour administrative d'appel de Paris ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne d'entraide judiciaire en matière pénale, signée à Strasbourg le 20 avril 1969, ensemble le décret n° 67-636 du 23 juillet 1967 décidant sa publication ;<br/>
              - le code de procédure pénale ; <br/>
              - le code de l'organisation judiciaire, notamment son article L. 141-1 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Rozen Noguellou, conseillère d'Etat,  <br/>
<br/>
              - les conclusions de M. Olivier Fuchs, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Spinosi, avocat de M. D... et autre ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la société Aéronord, créée en 2008 par M. D..., spécialisée dans la location, l'achat et la vente de matériel de transport aérien, a conclu un contrat de crédit-bail ayant pour objet un hélicoptère pour lequel M. D... s'est porté caution, et chargé la société Héli Nord de son exploitation. Cet hélicoptère a été loué à deux sociétés dont un représentant a été mis en examen pour des faits d'importation et de trafic de stupéfiants. A la suite d'une commission rogatoire internationale délivrée par le juge d'instruction, l'appareil a été saisi par les autorités espagnoles, le 19 octobre 2010, et placé en fourrière. La société Aéronord a demandé et obtenu de la chambre de l'instruction de la cour d'appel d'Aix en Provence, par un arrêt du 6 avril 2011, la restitution de l'appareil qui, du fait des conditions de sa conservation, était endommagé. Après qu'une expertise ordonnée par le juge des référés, déposée le 4 juillet 2013, a évalué le montant des réparations, M. D... et la société Aéronord ont engagé, devant les juridictions judiciaires, une action tendant à l'indemnisation par l'Etat du préjudice qu'ils estimaient avoir subi. Par un arrêt du 5 janvier 2016, la cour d'appel de Paris, confirmant le jugement du tribunal de grande instance de Paris du 21 janvier 2015, a rejeté leur requête, au motif que leur préjudice n'avait pas été causé par l'absence de transmission d'informations du juge d'instruction français, qui ne pouvait pas ordonner à la justice espagnole de mettre en œuvre les mesures prescrites par la société Heli Industries afin d'assurer la sauvegarde de l'hélicoptère. Par un arrêt du 18 janvier 2017, la première chambre civile de la Cour de cassation a rejeté le pourvoi formé par M. D... et la société Aéronord contre cet arrêt.  M. D... et la société Aéronord ont ensuite saisi la garde des sceaux, ministre de la justice le 5 février 2018, d'une demande indemnitaire et contesté, devant le juge administratif, le refus implicite qui leur a été opposé. Par un jugement du 4 juillet 2019, le tribunal administratif de Paris a rejeté leur demande. Par un arrêt du 9 juillet 2020, contre lequel M. D... et la société Aéronord se pourvoient en cassation, la cour administrative d'appel de Paris a rejeté leur appel contre ce jugement.<br/>
<br/>
              Sur l'action en responsabilité pour faute :<br/>
<br/>
              2. Si M. D... et la société Aéronord reprochent au magistrat de liaison en fonction à l'ambassade de France en Espagne d'avoir assuré un suivi insuffisant de la commission rogatoire délivrée par le juge d'instruction du tribunal de grande instance de Marseille, les modalités d'intervention de ce magistrat dans le suivi d'une commission rogatoire délivrée par un juge d'instruction relèvent, en tout état de cause, du fonctionnement du service public de la justice, et leur examen conduirait à porter une appréciation sur la marche même des services judiciaires. <br/>
<br/>
              3. Il résulte de ce qui précède que la cour administrative d'appel de Paris n'a pas commis d'erreur de droit en jugeant que le juge administratif était incompétent pour connaître de l'action en responsabilité pour faute du service public de la justice. <br/>
<br/>
              Sur l'action en responsabilité sans faute :<br/>
<br/>
              4. L'action fondée sur la responsabilité sans faute de l'État en raison du préjudice résultant d'une opération de police judiciaire relève de la compétence de la juridiction judiciaire. Si M. D... et la société Aéronord invoquent la responsabilité sans faute de l'Etat du fait de la mise en œuvre de la convention d'entraide judiciaire en matière pénale, signée à Strasbourg le 20 avril 1959 et entrée en vigueur dans l'ordre interne en vertu d'un décret du 23 juillet 1967, ils ne font état d'aucun préjudice trouvant dans cette convention un fondement distinct de l'information judiciaire dans le cadre de laquelle a été délivrée la commission rogatoire internationale ayant permis la saisie de l'appareil. La responsabilité résultant des conditions matérielles de la garde de l'appareil sur le territoire espagnol par les autorités espagnoles ne relève pas davantage de la juridiction administrative française.  <br/>
<br/>
              5. Il résulte de ce qui précède que le juge administratif est également incompétent pour connaître de l'action de M. D... et de la société Aéronord tendant à l'engagement de la responsabilité sans faute de l'Etat sur le fondement de la rupture de l'égalité devant les charges publiques. Ce motif, dont l'examen n'implique l'appréciation d'aucune circonstance de fait, doit être substitué à celui retenu par l'arrêt attaqué, dont il justifie le dispositif. <br/>
<br/>
              6. Il résulte de tout ce qui précède que le pourvoi de M. D... et de la société Aéronord doit être rejeté, y compris les conclusions présentées sur le fondement de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. D... et de la société Aéronord est rejeté.<br/>
Article 2 : Les conclusions présentées par M. D... et la société Aéronord sur le fondement de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à M. G... D..., à la société Aéronord, au garde des sceaux, ministre de la justice et au ministre de l'Europe et des affaires étrangères.<br/>
              Délibéré à l'issue de la séance du 8 octobre 2021 où siégeaient : Mme Christine Maugüé, présidente adjointe de la Section du contentieux, présidant ; M. A... H..., M. Fabien Raynaud, présidents de chambre ; Mme N... J..., M. L... C..., Mme F... K..., M. E... I..., M. Cyril Roger-Lacan, conseillers d'Etat et Mme Rozen Noguellou, conseillère d'Etat-rapporteure. <br/>
<br/>
              Rendu le 15 novembre 2021.<br/>
                 La présidente : <br/>
                 Signé : Mme Christine Maugüé<br/>
 		La rapporteure : <br/>
      Signé : Mme Rozen Noguellou<br/>
                 La secrétaire :<br/>
                 Signé : Mme M... B...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">17-03-02-05-01-02 COMPÉTENCE. - RÉPARTITION DES COMPÉTENCES ENTRE LES DEUX ORDRES DE JURIDICTION. - COMPÉTENCE DÉTERMINÉE PAR UN CRITÈRE JURISPRUDENTIEL. - RESPONSABILITÉ. - RESPONSABILITÉ EXTRA-CONTRACTUELLE. - COMPÉTENCE JUDICIAIRE. - MISE EN CAUSE DE LA RESPONSABILITÉ SANS FAUTE DE L'ETAT EN RAISON DU PRÉJUDICE RÉSULTANT D'UNE OPÉRATION DE POLICE JUDICIAIRE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">17-03-02-07-05-02 COMPÉTENCE. - RÉPARTITION DES COMPÉTENCES ENTRE LES DEUX ORDRES DE JURIDICTION. - COMPÉTENCE DÉTERMINÉE PAR UN CRITÈRE JURISPRUDENTIEL. - PROBLÈMES PARTICULIERS POSÉS PAR CERTAINES CATÉGORIES DE SERVICES PUBLICS. - SERVICE PUBLIC JUDICIAIRE. - FONCTIONNEMENT. - MISE EN CAUSE DE LA RESPONSABILITÉ SANS FAUTE DE L'ETAT EN RAISON DU PRÉJUDICE RÉSULTANT D'UNE OPÉRATION DE POLICE JUDICIAIRE [RJ1].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">37-02-02 JURIDICTIONS ADMINISTRATIVES ET JUDICIAIRES. - SERVICE PUBLIC DE LA JUSTICE. - FONCTIONNEMENT. - MISE EN CAUSE DE LA RESPONSABILITÉ SANS FAUTE DE L'ETAT EN RAISON DU PRÉJUDICE RÉSULTANT D'UNE OPÉRATION DE POLICE JUDICIAIRE [RJ1].
</SCT>
<ANA ID="9A"> 17-03-02-05-01-02 L'action fondée sur la responsabilité sans faute de l'État en raison du préjudice résultant d'une opération de police judiciaire relève de la compétence de la juridiction judiciaire.</ANA>
<ANA ID="9B"> 17-03-02-07-05-02 L'action fondée sur la responsabilité sans faute de l'État en raison du préjudice résultant d'une opération de police judiciaire relève de la compétence de la juridiction judiciaire.</ANA>
<ANA ID="9C"> 37-02-02 L'action fondée sur la responsabilité sans faute de l'État en raison du préjudice résultant d'une opération de police judiciaire relève de la compétence de la juridiction judiciaire.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr. TC, 8 février 2021, Garde des sceaux, ministre de la justice, c/ M. Rahmani, n° 4205, à publier au Recueil.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
