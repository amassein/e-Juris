<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036682863</ID>
<ANCIEN_ID>JG_L_2018_03_000000415675</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/68/28/CETATEXT000036682863.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 07/03/2018, 415675</TITRE>
<DATE_DEC>2018-03-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>415675</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>LE PRADO ; SCP PIWNICA, MOLINIE ; SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR>M. Thomas Odinot</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:415675.20180307</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
Procédure contentieuse antérieure <br/>
<br/>
              La société Biologie Nord Unilabs a demandé au juge des référés du tribunal administratif d'Amiens, sur le fondement de l'article L. 551-1 du code de justice administrative, d'ordonner la suspension de la passation par le centre hospitalier de Péronne du marché relatif à l'externalisation des examens de biologie médicale et du transport de prélèvements vers le lieu des analyses, d'annuler les décisions consécutives aux irrégularités qui entachent la procédure de publicité et de mise en concurrence de ce marché et notamment les décisions d'attribution du marché et de rejet des offres éventuellement notifiées aux candidats et d'enjoindre au centre hospitalier de Péronne de reprendre la procédure au stade de la publicité préalable. Par une ordonnance n° 1702823 du 31 octobre 2017, le juge des référés du tribunal administratif d'Amiens a annulé la procédure de passation du marché et enjoint au centre hospitalier de Péronne de reprendre la procédure de passation du marché en litige au stade de l'analyse des offres sauf s'il entendait renoncer à passer ce marché.<br/>
<br/>
Procédure contentieuse devant le Conseil d'Etat  <br/>
<br/>
              1° Sous le n° 415675, par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 14 et 29 novembre 2017 et le 9 février 2018 au secrétariat du contentieux du Conseil d'Etat, le centre hospitalier de Péronne demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de mettre à la charge de la société Biologie Nord Unilabs la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              2° Sous le n° 415716, par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 15 et 30 novembre 2017 et le 13 février 2018 au secrétariat du contentieux du Conseil d'Etat, la société Oxabio demande au Conseil d'Etat :<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de rejeter la demande de la société Biologie Nord Unilabs ;<br/>
<br/>
              3°) de mettre à la charge de la société Biologie Nord Unilabs la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la santé publique ;<br/>
              - l'ordonnance n° 2015-899 du 23 juillet 2015 ;<br/>
              - le décret n° 2016-360 du 25 mars 2016 ;<br/>
              - le code de justice administrative.<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Thomas Odinot, auditeur,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Le Prado, avocat du centre hospitalier de Péronne, à la SCP Matuchansky, Poupot, Valdelièvre, avocat de la société Biologie Nord Unilabs et à la SCP Piwnica, Molinié, avocat de la société Oxabio.<br/>
<br/>
<br/>
<br/>
              1. Considérant que les pourvois du centre hospitalier de Péronne et de la société Oxabio sont dirigés contre la même ordonnance ; qu'il y a lieu de les joindre pour statuer par une seule décision ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis au juge des référés que le centre hospitalier de Péronne a engagé une procédure d'appel d'offres ouvert pour l'attribution d'un marché ayant pour objet  des examens de biologie médicale et le transport des prélèvements vers le lieu d'analyse ; que la société Biologie Nord Unilabs, informée le 2 octobre 2017 que son offre était classée en deuxième position après celle de la société Oxabio, attributaire du marché, a demandé au juge des référés du tribunal administratif d'Amiens, sur le fondement de l'article L. 551-1 du code de justice administrative, d'annuler la procédure d'attribution du marché ; que, par une ordonnance du 31 octobre 2017, contre laquelle le centre hospitalier de Péronne et la société Oxabio se pourvoient en cassation, le juge des référés du tribunal administratif d'Amiens a fait droit à cette demande ;<br/>
<br/>
              3. Considérant qu'aux termes de l'article L. 551-1 du code de justice administrative : " Le président du tribunal administratif, ou le magistrat qu'il délègue, peut être saisi en cas de manquement aux obligations de publicité et de mise en concurrence auxquelles est soumise la passation par les pouvoirs adjudicateurs de contrats administratifs ayant pour objet l'exécution de travaux, la livraison de fournitures ou la prestation de services, avec une contrepartie économique constituée par un prix ou un droit d'exploitation (...) " ; <br/>
<br/>
              4. Considérant, d'une part, qu'aux termes de l'article 59 du décret du 25 mars 2016 relatif aux marchés publics : " I. - L'acheteur vérifie que les offres (...) sont régulières, acceptables et appropriées. Une offre irrégulière est une offre qui ne respecte pas les exigences formulées dans les documents de la consultation notamment parce qu'elle est incomplète, ou qui méconnaît la législation applicable notamment en matière sociale et environnementale. (...) . II. - Dans les procédures d'appel d'offres et les procédures adaptées sans négociation, les offres irrégulières, inappropriées ou inacceptables sont éliminées. " ;<br/>
<br/>
              5. Considérant, d'autre part, qu'aux termes de l'article L. 6211-16 du code de la santé publique, dans sa version applicable au litige : " Le prélèvement d'un échantillon biologique est réalisé dans l'une des zones déterminées en application du b du 2° de l'article L. 1434-9 d'implantation du laboratoire de biologie médicale, sauf dérogation pour des motifs de santé publique et dans des conditions déterminées par décret en Conseil d'Etat. " ; qu'aux termes de l'article R. 6211-12 du même code : " La dérogation prévue, pour des motifs de santé publique, à l'article L. 6211-16, selon lequel le prélèvement d'un échantillon biologique est effectué dans l'un des territoires de santé d'implantation du laboratoire de biologie médicale, s'applique aux cas suivants : (...) 4° Au prélèvement d'échantillon biologique effectué sur les patients hospitalisés en établissement de santé, lorsque la phase analytique de l'examen de biologie médicale est effectuée dans un laboratoire de biologie médicale qui, bien que situé dans un territoire de santé limitrophe, est plus proche de l'établissement de santé que tout autre laboratoire situé sur le même territoire de santé que l'établissement de santé (...) " ;<br/>
<br/>
              6. Considérant qu'il résulte des dispositions citées au point 5 que l'analyse d'un échantillon biologique prélevé dans un établissement de santé peut être effectuée dans un laboratoire situé dans un territoire de santé limitrophe du territoire dans lequel est situé l'établissement de santé, à condition que ce laboratoire soit plus proche de l'établissement de santé que les laboratoires situés sur le même territoire que cet établissement ; que, dans le cadre d'un marché public passé par un établissement de santé, la comparaison qui doit être faite pour apprécier si l'offre présentée par un candidat qui entend réaliser les analyses dans un laboratoire situé dans un territoire de santé limitrophe du territoire dans lequel est situé l'établissement de santé est régulière s'effectue avec les seuls laboratoires situés dans le même territoire que l'établissement de santé  dans lesquels les candidats qui ont présenté  une offre  régulière, acceptable et appropriée au sens de l'article 59 du décret du 25 mars 2016 cité au point 4 entendent réaliser les analyses ; <br/>
<br/>
              7. Considérant qu'il suit de là qu'en jugeant que cette comparaison devait être effectuée en prenant également en compte les laboratoires qui n'ont pas présenté d'offre dans le cadre du marché en litige, le juge des référés du tribunal administratif d'Amiens a commis une erreur de droit ; que, par suite et sans qu'il soit besoin d'examiner les autres moyens des pourvois, son ordonnance doit être annulée ;<br/>
<br/>
              8. Considérant que, dans les circonstances de l'espèce, il y a lieu, en application de l'article L. 821-2 du code de justice administrative, de régler l'affaire au titre de la procédure de référé engagée ;<br/>
<br/>
              9. Considérant qu'il résulte de l'article 4.2 du règlement de consultation des entreprises que les critères de sélection des offres sont dénommés et pondérés de la façon suivante : " 1.  " Impact social " : 35 % ;  2. " Offre financière (" Plateau technique " et tarifs examens non cotés) " : 30 % ;  3.  " Qualité de l'organisation et de la permanence des soins " : 30 % ; 4. " Déploiement " : 5 % ; que le critère n° 2 doit lui-même être  évalué en fonction des deux sous-critères suivants : le " forfait plateau technique ", pondéré à 65 %, et la " valeur des actes non côtés à la nomenclature des actes de biologie médicale ", pondérée à 35 % ;<br/>
<br/>
              10. Considérant que l'article 5.1 " Prix de la prestation " du cahier des clauses particulières stipule que : " Dans l'hypothèse où le candidat proposerait dans son offre l'utilisation de tout ou partie du plateau technique mis à sa disposition, il indiquera dans son offre financière le montant forfaitaire mensuel reversé au CHP. Le candidat intégrera dans son calcul outre la mise à disposition du matériel, les coûts induits liés à l'exploitation du dit plateau : coût locaux, prestation informatique, consommation d'énergie, consommation de l'eau, entretien. " ; qu'il résulte de ces stipulations que l'utilisation du plateau technique du centre hospitalier était facultative ; que, dans ces conditions, le sous-critère " forfait plateau technique ", qui était au demeurant ambigu, ne pouvait être régulièrement retenu par le centre hospitalier  pour l'appréciation de l'offre financière des candidats ;  <br/>
<br/>
              11. Considérant, par ailleurs, que, pour le sous-critère " valeur des actes non cotés à la nomenclature des actes de biologie médicale (NABM) " du critère " offre financière ", l'article 4.2 du règlement de consultation prévoit une méthode de notation qui a pour effet, dans l'hypothèse où un candidat proposerait, eu égard aux caractéristiques de ces prestations, de les réaliser gratuitement, d'attribuer une note nulle à tous les candidats qui proposeraient une facturation, quel qu'en soit le montant ; que, si un pouvoir adjudicateur définit librement la méthode de notation pour la mise en oeuvre des critères de sélection des offres, la méthode retenue en l'espèce ne peut, compte tenu du caractère très particulier des prestations demandées et du risque que le sous-critère en cause soit privé de l'essentiel de sa portée,  être regardée comme régulière ;  <br/>
<br/>
              12. Considérant qu'il résulte de ce qui précède que le centre hospitalier de Péronne a méconnu ses obligations de publicité et de mise en concurrence ; qu'eu égard à la nature du vice entachant la procédure de passation du contrat litigieux, la société Biologie Nord Unilabs  est fondée à en demander l'annulation et à demander que le centre hospitalier reprenne l'ensemble de la  procédure, sauf s'il entend renoncer à passer le marché ;  <br/>
<br/>
              13. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre solidairement à la charge du centre hospitalier de Péronne et de la société Oxabio la somme de 3 000 euros à verser à la société Biologie Nord Unilabs au titre des dispositions de l'article L. 761-1 du code de justice administrative ; qu'en revanche, ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de la société Biologie Nord Unilabs qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du 31 octobre 2017 du juge des référés du tribunal administratif d'Amiens est annulée.<br/>
Article 2 : La procédure de passation du marché relatif à des examens de biologie médicale et au transport des prélèvements vers le lieu d'analyse du centre hospitalier de Péronne est annulée.<br/>
Article 3 : Il est enjoint au centre hospitalier de Péronne, sauf s'il entend renoncer à passer le marché, de reprendre la procédure de passation.<br/>
Article 4 : Le centre hospitalier de Péronne et la société Oxabio verseront solidairement à la société Biologie Nord Unilabs une  somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 5 : Les conclusions présentées par le centre hospitalier de Péronne et la société Oxabio au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 6 : La présente décision sera notifiée au centre hospitalier de Péronne, à la société Oxabio et à la société Biologie Nord Unilabs.<br/>
Copie en sera adressée à la ministre des solidarités et de la santé et au ministre de l'économie et des finances.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-08-015-01 MARCHÉS ET CONTRATS ADMINISTRATIFS. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. PROCÉDURES D'URGENCE. - MARCHÉ PUBLIC D'UN EPS RELATIF À DES EXAMENS DE BIOLOGIE MÉDICALE - APPRÉCIATION DE LA RÉGULARITÉ DES OFFRES - CAS D'UN CANDIDAT DONT LE LABORATOIRE EST SITUÉ DANS UN TERRITOIRE DE SANTÉ LIMITROPHE DU TERRITOIRE DE L'EPS, EN APPLICATION DE LA DÉROGATION PRÉVUE AUX ARTICLES L. 6211-6 ET R. 6211-12 DU CSP - COMPARAISON DE SON OFFRE AVEC LES SEULS LABORATOIRES SITUÉS DANS LE MÊME TERRITOIRE QUE L'EPS DANS LESQUELS LES CANDIDATS QUI ONT PRÉSENTÉ UNE OFFRE RÉGULIÈRE, ACCEPTABLE ET APPROPRIÉE ENTENDENT RÉALISER LES ANALYSES.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">61-08-01 SANTÉ PUBLIQUE. DIVERS ÉTABLISSEMENTS À CARACTÈRE SANITAIRE. LABORATOIRES D'ANALYSES DE BIOLOGIE MÉDICALE. - MARCHÉ PUBLIC D'UN EPS RELATIF À DES EXAMENS DE BIOLOGIE MÉDICALE - APPRÉCIATION DE LA RÉGULARITÉ DES OFFRES - CAS D'UN CANDIDAT DONT LE LABORATOIRE EST SITUÉ DANS UN TERRITOIRE DE SANTÉ LIMITROPHE DU TERRITOIRE DE L'EPS, EN APPLICATION DE LA DÉROGATION PRÉVUE AUX ARTICLES L. 6211-6 ET R. 6211-12 DU CSP - COMPARAISON DE SON OFFRE AVEC LES SEULS LABORATOIRES SITUÉS DANS LE MÊME TERRITOIRE QUE L'EPS DANS LESQUELS LES CANDIDATS QUI ONT PRÉSENTÉ UNE OFFRE RÉGULIÈRE, ACCEPTABLE ET APPROPRIÉE ENTENDENT RÉALISER LES ANALYSES.
</SCT>
<ANA ID="9A"> 39-08-015-01 Il résulte des articles L. 6211-16 et R. 6211-12 du code de la santé publique (CSP) que l'analyse d'un échantillon biologique prélevé dans un établissement de santé peut être effectuée dans un laboratoire situé dans un territoire de santé limitrophe du territoire dans lequel est situé l'établissement de santé, à condition que ce laboratoire soit plus proche de l'établissement de santé que les laboratoires situés sur le même territoire que cet établissement. Dans le cadre d'un marché public passé par un établissement de santé, la comparaison qui doit être faite pour apprécier si l'offre présentée par un candidat qui entend réaliser les analyses dans un laboratoire situé dans un territoire de santé limitrophe du territoire dans lequel est situé l'établissement de santé est régulière s'effectue avec les seuls laboratoires situés dans le même territoire que l'établissement de santé dans lesquels les candidats qui ont présenté une offre régulière, acceptable et appropriée au sens de l'article 59 du décret du 25 mars 2016 entendent réaliser les analyses.</ANA>
<ANA ID="9B"> 61-08-01 Il résulte des articles L. 6211-16 et R. 6211-12 du code de la santé publique (CSP) que l'analyse d'un échantillon biologique prélevé dans un établissement de santé peut être effectuée dans un laboratoire situé dans un territoire de santé limitrophe du territoire dans lequel est situé l'établissement de santé, à condition que ce laboratoire soit plus proche de l'établissement de santé que les laboratoires situés sur le même territoire que cet établissement. Dans le cadre d'un marché public passé par un établissement public de santé (EPS), la comparaison qui doit être faite pour apprécier si l'offre présentée par un candidat qui entend réaliser les analyses dans un laboratoire situé dans un territoire de santé limitrophe du territoire dans lequel est situé l'établissement de santé est régulière s'effectue avec les seuls laboratoires situés dans le même territoire que l'établissement de santé dans lesquels les candidats qui ont présenté une offre régulière, acceptable et appropriée au sens de l'article 59 du décret du 25 mars 2016 entendent réaliser les analyses.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
