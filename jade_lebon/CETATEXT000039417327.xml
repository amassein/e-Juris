<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039417327</ID>
<ANCIEN_ID>JG_L_2019_11_000000417752</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/41/73/CETATEXT000039417327.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 22/11/2019, 417752</TITRE>
<DATE_DEC>2019-11-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>417752</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER ; SCP GASCHIGNARD</AVOCATS>
<RAPPORTEUR>M. Thomas Pez-Lavergne</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Mireille Le Corre</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:417752.20191122</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              La société Gom Propreté a demandé au tribunal administratif de Cergy-Pontoise de condamner l'établissement public de gestion du quartier d'affaires de La Défense à lui verser la somme de 616 298,38 euros en règlement de quatre factures émises entre mars et octobre 2013. Par un jugement n° 1403502 du 18 juin 2015, le tribunal administratif de Cergy-Pontoise a admis la compensation de la somme de 25 778,87 euros due par l'établissement public résultant d'une facture du 31 octobre 2013 avec la somme de 26 905,05 euros due par la société Gom Propreté résultant d'un titre de perception émis le 5 avril 2013 et a rejeté le surplus des conclusions de la demande.<br/>
<br/>
              Par un arrêt n° 15VE02739 du 30 novembre 2017, la cour administrative d'appel de Versailles a, sur appel de la société Gom Propreté, annulé ce jugement et condamné l'établissement public de gestion du quartier d'affaires de La Défense à verser à la société Gom Propreté la somme de 589 393,33 euros TTC, de laquelle il conviendra de déduire, le cas échéant, la provision de 25 778,87 euros qui lui a été accordée par l'ordonnance n° 1310291 du juge des référés du tribunal administratif de Cergy-Pontoise du 9 décembre 2014.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 30 janvier et 13 avril 2018 et le 17 octobre 2019 au secrétariat du contentieux du Conseil d'Etat, l'établissement Paris La Défense demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de la société Gom Propreté ;<br/>
<br/>
              3°) de mettre à la charge de la société Gom Propreté la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code des marchés publics ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Thomas Pez-Lavergne, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Mireille Le Corre, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Foussard, Froger, avocat de l'établissement Paris La Défense et à la SCP Gaschignard, avocat de la société APS Holding venant aux droits de la société Gom Propreté ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que l'établissement public de gestion du quartier d'affaires de La Défense a confié à la société Gom Propreté, par un acte d'engagement du 23 mai 2009, un marché à bons de commande pour des prestations de nettoiement des espaces publics du quartier d'affaires de La Défense. Estimant qu'une partie de ces prestations n'avait pas été effectuée le dimanche sur la dalle de La Défense et dans le centre commercial La Coupole, l'établissement public a émis le 5 juin 2013 un titre de perception d'un montant de 478 902,30 euros TTC qui a été annulé par un jugement du 18 juin 2015 du tribunal administratif de Cergy-Pontoise, devenu définitif. Il a également émis le 5 avril 2013 un titre de perception d'un montant de 26 905,05 euros TTC, correspondant au paiement de la redevance d'occupation domaniale due par la société Gom Propreté, dont la demande d'annulation a été rejetée par le même tribunal. Parallèlement, l'établissement public a refusé de régler les factures correspondant à quatre bons de commande, émis du 1er mars au 5 juin 2013, estimant qu'elles devaient être compensées avec ses propres créances vis-à-vis de la société Gom Propreté. Cette dernière a finalement présenté un mémoire de réclamation, reçu le 16 décembre 2013 par l'établissement public, qui a été rejeté par une décision du 31 janvier 2014. La société Gom Propreté a saisi le tribunal administratif de Cergy-Pontoise qui, par un jugement du 18 juin 2015, a rejeté comme irrecevables les conclusions relatives aux trois premières factures et a prononcé la compensation de la somme de 25 778,87 euros résultant de la quatrième facture de la société Gom Propreté avec la somme de 26 905,05 euros résultant du titre de perception émis par l'établissement public pour le paiement de la redevance d'occupation domaniale. Par un arrêt du 30 novembre 2017, contre lequel l'établissement Paris La Défense, venu aux droits de l'établissement public de gestion du quartier d'affaires de La Défense, se pourvoit en cassation, la cour administrative d'appel de Versailles a annulé ce jugement et condamné l'établissement public à verser à la société Gom Propreté la somme de 589 393,33 euros TTC, de laquelle il conviendra de déduire, le cas échéant, la provision de 25 778,87 euros qui lui a été accordée par une ordonnance du juge des référés du tribunal administratif de Cergy-Pontoise du 9 décembre 2014.<br/>
<br/>
              2. En premier lieu, aux termes de l'article 34.1 du cahier des clauses administratives générales applicables aux marchés publics de fournitures courantes et de services, notamment au marché en litige, approuvé par le décret du 27 mai 1977 : " Tout différend entre le titulaire et la personne responsable du marché doit faire l'objet de la part du titulaire d'un mémoire de réclamation qui doit être communiqué à la personne responsable du marché dans le délai de trente jours compté à partir du jour où le différend est apparu ".<br/>
<br/>
              3. L'apparition d'un différend, au sens des stipulations précitées, entre le titulaire du marché et l'acheteur, résulte, en principe, d'une prise de position écrite, explicite et non équivoque émanant de l'acheteur et faisant apparaître le désaccord. Elle peut également résulter du silence gardé par l'acheteur à la suite d'une mise en demeure adressée par le titulaire du marché l'invitant à prendre position sur le désaccord dans un certain délai. En revanche, en l'absence d'une telle mise en demeure, la seule circonstance qu'une personne publique ne s'acquitte pas, en temps utile, des factures qui lui sont adressées, sans refuser explicitement de les honorer, ne suffit pas à caractériser l'existence d'un différend au sens des stipulations précédemment citées.<br/>
<br/>
              4. En l'espèce, la cour administrative d'appel de Versailles a constaté que la société Gom Propreté, par un courrier du 7 août 2013, avait réclamé le paiement de factures émises les 31 mars, 30 avril et 31 mai 2013, dont le règlement était devenu exigible respectivement les 1er et 31 mai et 30 juin 2013, en notant que l'établissement public avait indiqué oralement qu'il entendait les " bloquer intégralement " et en faisant part de son intention de " contester immédiatement ", " si elle était avérée ", l'éventuelle compensation des sommes dues au titre de ces factures avec celles dues au titre de la redevance d'occupation domaniale. La cour a toutefois relevé le règlement, le 9 août 2013, par l'établissement public, postérieurement à ce courrier du 7 août 2013, de la facture du 31 mars 2013, d'un montant de 232 148,46 euros, à concurrence de la somme de 218 998,14 euros. Elle a considéré que ce règlement avait pu légitimement laisser croire à la société Gom Propreté que l'établissement public n'entendait pas refuser le paiement de ses factures. En jugeant ainsi que le courrier du 7 août 2013, qui ne révélait pas une prise de position écrite, explicite et non équivoque émanant de l'acheteur, ne caractérisait pas l'existence d'un différend au sens des stipulations précitées de l'article 34.1 du cahier des clauses administratives générales applicables aux marchés publics de fournitures courantes et de services, la cour administrative d'appel de Versailles s'est, sans erreur de droit ni dénaturation, livrée à une appréciation souveraine des faits de l'espèce. En en déduisant que le mémoire de réclamation, bien qu'adressé par la société Gom Propreté le 16 décembre 2013, soit plus de trente jours après ce courrier, n'était pas tardif, la cour n'a pas commis d'erreur de droit.<br/>
<br/>
              5. En second lieu, il ressort des pièces du dossier soumis aux juges du fond que l'établissement public a demandé qu'il soit procédé à la compensation de la créance de la société Gom Propreté avec celles qu'il estime détenir sur elle, liquidées et exigées par l'émission de deux titres de perception des 5 avril et 5 juin 2013. Ainsi qu'il a été dit au point 1, le titre de perception du 5 juin 2013 portant sur la somme de 478 902,30 euros TTC relative à des prestations inexécutées a été annulé par un jugement du tribunal administratif de Cergy-Pontoise, devenu définitif, au motif que l'établissement public a réglé les factures correspondant aux prestations de nettoyage qu'il a commandées sans émettre de réserves, et qu'en conséquence, ces prestations ont fait l'objet d'un paiement définitif qui n'est plus susceptible d'être remis en cause par les parties notamment lors de l'établissement du solde du marché. <br/>
<br/>
              6. Pour déduire de l'annulation de ce titre de perception par le tribunal administratif de Cergy-Pontoise que la somme sur laquelle il portait ne saurait être compensée avec la créance détenue par la société Gom Propreté à l'égard de l'établissement public, la cour administrative d'appel de Versailles a nécessairement retenu que le caractère définitif du règlement de cette somme avait éteint la créance correspondante de l'établissement public à l'égard de la société Gom Propreté, ce qui faisait obstacle à ce qu'elle puisse être compensée avec une autre créance. Dès lors, l'établissement Paris La Défense n'est pas fondé à soutenir que la cour a entaché son arrêt d'une erreur de droit ou l'a insuffisamment motivé, en refusant de procéder à la compensation demandée par l'établissement public au seul motif que le titre exécutoire du 5 juin 2013 correspondant à cette créance avait été annulé par le tribunal administratif de Cergy-Pontoise dans son jugement du 18 juin 2015. <br/>
<br/>
              7. Il résulte de ce qui précède que le pourvoi de l'établissement Paris La Défense doit être rejeté, y compris ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative. En revanche, il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'établissement Paris La Défense la somme de 3 000 euros à verser à la société APS Holding, au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de l'établissement Paris La Défense est rejeté.<br/>
Article 2 : L'établissement Paris La Défense versera à la société APS Holding une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à l'établissement Paris La Défense et à la société APS Holding.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-05 MARCHÉS ET CONTRATS ADMINISTRATIFS. EXÉCUTION FINANCIÈRE DU CONTRAT. - DIFFÉREND ENTRE LE TITULAIRE D'UN MARCHÉ DE SERVICES ET L'ACHETEUR DEVANT FAIRE L'OBJET D'UN MÉMOIRE EN RÉCLAMATION À PEINE D'IRRECEVABILITÉ DE LA SAISINE DU JUGE DU CONTRAT (ART. 34.1 DU CCAG MARCHÉS DE FOURNITURES COURANTES ET DE SERVICES, DÉCRET DU 27 MAI 1977) - 1) NOTION - PRISE DE POSITION ÉCRITE, EXPLICITE ET NON ÉQUIVOQUE ÉMANANT DE L'ACHETEUR ET FAISANT APPARAÎTRE LE DÉSACCORD [RJ1] - SILENCE GARDÉ PAR L'ACHETEUR À LA SUITE D'UNE MISE EN DEMEURE DE PRENDRE POSITION SUR LE DÉSACCORD - 2) CIRCONSTANCE QUE L'ACHETEUR NE S'ACQUITTE PAS, EN TEMPS UTILE, DES FACTURES QUI LUI SONT ADRESSÉES, SANS REFUSER EXPLICITEMENT DE LES HONORER - A) CIRCONSTANCE NE SUFFISANT PAS À CARACTÉRISER L'EXISTENCE D'UN DIFFÉREND - B) ESPÈCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">39-08-01 MARCHÉS ET CONTRATS ADMINISTRATIFS. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. RECEVABILITÉ. - DIFFÉREND ENTRE LE TITULAIRE D'UN MARCHÉ DE SERVICES ET L'ACHETEUR DEVANT FAIRE L'OBJET D'UN MÉMOIRE EN RÉCLAMATION À PEINE D'IRRECEVABILITÉ DE LA SAISINE DU JUGE DU CONTRAT (ART. 34.1 DU CCAG MARCHÉS DE FOURNITURES COURANTES ET DE SERVICES, DÉCRET DU 27 MAI 1977) - 1) NOTION - PRISE DE POSITION ÉCRITE, EXPLICITE ET NON ÉQUIVOQUE ÉMANANT DE L'ACHETEUR ET FAISANT APPARAÎTRE LE DÉSACCORD [RJ1] - SILENCE GARDÉ PAR L'ACHETEUR À LA SUITE D'UNE MISE EN DEMEURE DE PRENDRE POSITION SUR LE DÉSACCORD - 2) CIRCONSTANCE QUE L'ACHETEUR NE S'ACQUITTE PAS, EN TEMPS UTILE, DES FACTURES QUI LUI SONT ADRESSÉES, SANS REFUSER EXPLICITEMENT DE LES HONORER - A) CIRCONSTANCE NE SUFFISANT PAS À CARACTÉRISER L'EXISTENCE D'UN DIFFÉREND - B) ESPÈCE.
</SCT>
<ANA ID="9A"> 39-05 Article 34.1 du cahier des clauses administratives générales (CCAG) applicables aux marchés publics de fournitures courantes et de services, approuvé par le décret n° 77-699 du 27 mai 1977, prévoyant que : Tout différend entre le titulaire et la personne responsable du marché doit faire l'objet de la part du titulaire d'un mémoire de réclamation qui doit être communiqué à la personne responsable du marché dans le délai de trente jours compté à partir du jour où le différend est apparu.,,,1) L'apparition d'un différend, au sens de ces stipulations, entre le titulaire du marché et l'acheteur, résulte, en principe, d'une prise de position écrite, explicite et non équivoque émanant de l'acheteur et faisant apparaître le désaccord. Elle peut également résulter du silence gardé par l'acheteur à la suite d'une mise en demeure adressée par le titulaire du marché l'invitant à prendre position sur le désaccord dans un certain délai.... ,,2) a) En revanche, en l'absence d'une telle mise en demeure, la seule circonstance qu'une personne publique ne s'acquitte pas, en temps utile, des factures qui lui sont adressées, sans refuser explicitement de les honorer, ne suffit pas à caractériser l'existence d'un différend au sens des stipulations précédemment citées.,,,b) Cour administrative d'appel constatant que le titulaire du marché, par un courrier du 7 août 2013, avait réclamé le paiement de factures, dont le règlement était devenu exigible, en notant que l'acheteur avait indiqué oralement qu'il entendait les bloquer intégralement et en faisant part de son intention de contester immédiatement, si elle était avérée, l'éventuelle compensation des sommes dues au titre de ces factures avec celles dues au titre de la redevance d'occupation domaniale. Cour relevant toutefois le règlement, le 9 août 2013, par l'acheteur, postérieurement à ce courrier du 7 août 2013, de l'une de ces factures. Cour considérant que ce règlement avait pu légitimement laisser croire au titulaire que l'acheteur n'entendait pas refuser le paiement de ses factures.... ,,En jugeant ainsi que le courrier du 7 août 2013, qui ne révélait pas une prise de position écrite, explicite et non équivoque émanant de l'acheteur, ne caractérisait pas l'existence d'un différend au sens des stipulations précitées de l'article 34.1 du cahier des clauses administratives générales applicables aux marchés publics de fournitures courantes et de services, la cour administrative d'appel s'est, sans erreur de droit ni dénaturation, livrée à une appréciation souveraine des faits de l'espèce. En en déduisant que le mémoire de réclamation, bien qu'adressé par le titulaire le 16 décembre 2013, soit plus de trente jours après ce courrier, n'était pas tardif, la cour n'a pas commis d'erreur de droit.</ANA>
<ANA ID="9B"> 39-08-01 Article 34.1 du cahier des clauses administratives générales (CCAG) applicables aux marchés publics de fournitures courantes et de services, approuvé par le décret n° 77-699 du 27 mai 1977, prévoyant que : Tout différend entre le titulaire et la personne responsable du marché doit faire l'objet de la part du titulaire d'un mémoire de réclamation qui doit être communiqué à la personne responsable du marché dans le délai de trente jours compté à partir du jour où le différend est apparu.,,,1) L'apparition d'un différend, au sens de ces stipulations, entre le titulaire du marché et l'acheteur, résulte, en principe, d'une prise de position écrite, explicite et non équivoque émanant de l'acheteur et faisant apparaître le désaccord. Elle peut également résulter du silence gardé par l'acheteur à la suite d'une mise en demeure adressée par le titulaire du marché l'invitant à prendre position sur le désaccord dans un certain délai.... ,,2) a) En revanche, en l'absence d'une telle mise en demeure, la seule circonstance qu'une personne publique ne s'acquitte pas, en temps utile, des factures qui lui sont adressées, sans refuser explicitement de les honorer, ne suffit pas à caractériser l'existence d'un différend au sens des stipulations précédemment citées.,,,b) Cour administrative d'appel constatant que le titulaire du marché, par un courrier du 7 août 2013, avait réclamé le paiement de factures, dont le règlement était devenu exigible, en notant que l'acheteur avait indiqué oralement qu'il entendait les bloquer intégralement et en faisant part de son intention de contester immédiatement, si elle était avérée, l'éventuelle compensation des sommes dues au titre de ces factures avec celles dues au titre de la redevance d'occupation domaniale. Cour relevant toutefois le règlement, le 9 août 2013, par l'acheteur, postérieurement à ce courrier du 7 août 2013, de l'une de ces factures. Cour considérant que ce règlement avait pu légitimement laisser croire au titulaire que l'acheteur n'entendait pas refuser le paiement de ses factures.... ,,En jugeant ainsi que le courrier du 7 août 2013, qui ne révélait pas une prise de position écrite, explicite et non équivoque émanant de l'acheteur, ne caractérisait pas l'existence d'un différend au sens des stipulations précitées de l'article 34.1 du cahier des clauses administratives générales applicables aux marchés publics de fournitures courantes et de services, la cour administrative d'appel s'est, sans erreur de droit ni dénaturation, livrée à une appréciation souveraine des faits de l'espèce. En en déduisant que le mémoire de réclamation, bien qu'adressé par le titulaire le 16 décembre 2013, soit plus de trente jours après ce courrier, n'était pas tardif, la cour n'a pas commis d'erreur de droit.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 27 novembre 2019, Société SMA Environnement et autres, n° 422600, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
