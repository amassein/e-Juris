<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030860191</ID>
<ANCIEN_ID>JG_L_2015_07_000000390154</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/86/01/CETATEXT000030860191.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 1ère SSR, 08/07/2015, 390154</TITRE>
<DATE_DEC>2015-07-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>390154</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 1ère SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Clémence Olsina</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2015:390154.20150708</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Par un mémoire, un mémoire complémentaire et un nouveau mémoire, enregistrés les 13 mai, 29 mai  et 12 juin 2015 au secrétariat du contentieux du Conseil d'Etat, présentés en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958, M. B... demande au Conseil d'Etat, à l'appui de son recours pour excès de pouvoir tendant à l'annulation de la décision implicite de rejet née du silence gardé par le ministre de l'écologie, du développement durable et de l'énergie sur sa demande tendant à l'abrogation de l'arrêté du 19 janvier 2009 relatif aux dates de fermeture de la chasse aux oiseaux de passage et au gibier d'eau, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des dispositions du deuxième alinéa de l'article L. 424-2 du code de l'environnement. <br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
<br/>
              - la Constitution, notamment son Préambule et son article 61-1 ; <br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ; <br/>
              - la directive 79/409/CEE du 2 avril 1979 du Conseil ;<br/>
              - la directive n° 2009/147 CE du Parlement européen et du Conseil ; <br/>
              - le code de l'environnement ;  <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Clémence Olsina, auditeur,  <br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...) " ; qu'il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article 7 de la directive du 2 avril 1979 du Conseil concernant la conservation des oiseaux sauvages, dont les termes sont aujourd'hui repris par l'article 7 de la directive du 30 novembre 2009 : " 1. En raison de leur niveau de population, de leur distribution géographique et de leur taux de reproductivité dans l'ensemble de la Communauté, les espèces énumérées à l'annexe II peuvent faire l'objet d'actes de chasse dans le cadre de la législation nationale. Les États membres veillent à ce que la chasse de ces espèces ne compromette pas les efforts de conservation entrepris dans leur aire de distribution. / (...) 4. Les Etats membres s'assurent que la pratique de la chasse, y compris le cas échéant la fauconnerie, telle qu'elle découle de l'application des mesures nationales en vigueur, respecte les principes d'une utilisation raisonnée et d'une régulation équilibrée du point de vue écologique des espèces d'oiseaux concernées, et que cette pratique soit compatible, en ce qui concerne la population de ces espèces, notamment des espèces migratrices, avec les dispositions découlant de l'article 2. (...) / Lorsqu'il s'agit d'espèces migratrices, ils veillent en particulier à ce que les espèces auxquelles s'applique la législation sur la chasse ne soient pas chassées pendant leur période de reproduction et pendant leur trajet de retour vers leur lieu de nidification " ; qu'aux termes de l'article L. 424-2 du code de l'environnement : " Nul ne peut chasser en dehors des périodes d'ouverture de la chasse fixées par l'autorité administrative selon des conditions déterminées par décret en Conseil d'Etat. / Les oiseaux ne peuvent être chassés ni pendant la période nidicole ni pendant les différents stades de reproduction et de dépendance. Les oiseaux migrateurs ne peuvent en outre être chassés pendant leur trajet de retour vers leur lieu de nidification. / Toutefois, pour permettre, dans des conditions strictement contrôlées et de manière sélective, la capture, la détention ou toute autre exploitation judicieuse de certains oiseaux migrateurs terrestres et aquatiques en petites quantités, conformément aux dispositions de l'article L. 425-14, des dérogations peuvent être accordées (...) " ;<br/>
<br/>
              3. Considérant que M. B...soutient, d'une part, qu'en proscrivant la chasse des oiseaux migrateurs pendant leur trajet de retour vers leur lieu de nidification, les dispositions du deuxième alinéa de l'article L. 424-2 du code de l'environnement portent une atteinte disproportionnée au droit de chasse, qui est un attribut du droit de propriété garanti par l'article 2 de la Déclaration des droits de l'homme et du citoyen de 1789, et, d'autre part, que cette interdiction, qui ne s'applique pas pendant les migrations des oiseaux au départ de leur lieu de nidification, n'est pas justifiée par un motif d'intérêt général ; <br/>
<br/>
              4. Considérant qu'aux termes de l'article 88-1 de la Constitution : " La République participe à l'Union européenne constituée d'Etats qui ont choisi librement d'exercer en commun certaines de leurs compétences en vertu du traité sur l'Union européenne et du traité sur le fonctionnement de l'Union européenne, tels qu'ils résultent du traité signé à Lisbonne le 13 décembre 2007 " ; qu'il résulte de ces dispositions qu'en l'absence de mise en cause d'une règle ou d'un principe inhérent à l'identité constitutionnelle de la France, le Conseil constitutionnel n'est pas compétent pour contrôler la conformité aux droits et libertés que la Constitution garantit de dispositions législatives qui se bornent à tirer les conséquences nécessaires de dispositions inconditionnelles et précises d'une directive de l'Union européenne ; qu'en ce cas, il n'appartient qu'au juge de l'Union européenne, saisi le cas échéant à titre préjudiciel, de contrôler le respect par cette directive des droits fondamentaux garantis par l'article 6 du traité sur l'Union européenne ; qu'en l'espèce, les dispositions législatives critiquées, qui proscrivent la chasse des oiseaux migrateurs pendant leur trajet de retour vers leur lieu de nidification, se bornent à tirer les conséquences des dispositions précises et inconditionnelles du troisième alinéa du point 4 de l'article 7 de la directive du 30 novembre 2009 concernant la conservation des oiseaux sauvages et ne mettent en cause aucune règle ni aucun principe inhérents à l'identité constitutionnelle de la France ; qu'est sans incidence sur ce point la circonstance que la Convention internationale sur la protection des oiseaux, conclue à Paris le 18 octobre 1950, n'aurait été ratifiée que par un nombre limité d'Etats membres de l'Union européenne ; <br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que la question prioritaire de constitutionnalité soulevée, qui n'est pas nouvelle, ne présente pas un caractère sérieux ; qu'ainsi, sans qu'il soit besoin de renvoyer cette question au Conseil constitutionnel, le moyen tiré de ce que les dispositions du deuxième alinéa de l'article L. 424-2 du code de l'environnement portent atteinte aux droits et libertés garantis par la Constitution doit être écarté ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par M.B.... <br/>
Article 2 : La présente décision sera notifiée à M. A...B...et à la ministre l'écologie, du développement durable et de l'énergie. <br/>
Copie en sera adressée pour information au Conseil constitutionnel et au Premier ministre. <br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">15-03 COMMUNAUTÉS EUROPÉENNES ET UNION EUROPÉENNE. APPLICATION DU DROIT DE L'UNION EUROPÉENNE PAR LE JUGE ADMINISTRATIF FRANÇAIS. - QUESTION PRIORITAIRE DE CONSTITUTIONNALITÉ DIRIGÉE CONTRE UNE LOI TRANSPOSANT UNE DIRECTIVE DE L'UNION EUROPÉENNE - CRITÈRE DE LA QUESTION SÉRIEUSE - CAS D'UNE DISPOSITION LÉGISLATIVE TRANSPOSANT DES DISPOSITIONS INCONDITIONNELLES ET PRÉCISES D'UNE DIRECTIVE DE L'UNION EUROPÉENNE - ABSENCE DE CARACTÈRE SÉRIEUX DE LA QUESTION, SAUF MISE EN CAUSE DE L'IDENTITÉ CONSTITUTIONNELLE DE LA FRANCE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-10-05-04 PROCÉDURE. - CRITÈRE DE LA QUESTION SÉRIEUSE - CAS D'UNE DISPOSITION LÉGISLATIVE TRANSPOSANT DES DISPOSITIONS INCONDITIONNELLES ET PRÉCISES D'UNE DIRECTIVE DE L'UNION EUROPÉENNE - ABSENCE DE CARACTÈRE SÉRIEUX DE LA QUESTION, SAUF MISE EN CAUSE DE L'IDENTITÉ CONSTITUTIONNELLE DE LA FRANCE [RJ1].
</SCT>
<ANA ID="9A"> 15-03 Il résulte des dispositions de l'article 88-1 de la Constitution qu'en l'absence de mise en cause d'une règle ou d'un principe inhérent à l'identité constitutionnelle de la France, le Conseil constitutionnel n'est pas compétent pour contrôler la conformité aux droits et libertés que la Constitution garantit de dispositions législatives qui se bornent à tirer les conséquences nécessaires de dispositions inconditionnelles et précises d'une directive de l'Union européenne. En ce cas, il n'appartient qu'au juge de l'Union européenne, saisi le cas échéant à titre préjudiciel, de contrôler le respect par cette directive des droits fondamentaux garantis par l'article 6 du traité sur l'Union européenne. Dès lors, une question prioritiare de constitutionnalité contestant la conformité aux droits et libertés garantis par la Constitution de telles dispositions législatives n'est pas sérieuse, sauf s'il peut être sérieusement soutenu qu'elles mettent en cause une règle ou un principe inhérent à l'identité constitutionnelle de la France.</ANA>
<ANA ID="9B"> 54-10-05-04 Il résulte des dispositions de l'article 88-1 de la Constitution qu'en l'absence de mise en cause d'une règle ou d'un principe inhérent à l'identité constitutionnelle de la France, le Conseil constitutionnel n'est pas compétent pour contrôler la conformité aux droits et libertés que la Constitution garantit de dispositions législatives qui se bornent à tirer les conséquences nécessaires de dispositions inconditionnelles et précises d'une directive de l'Union européenne. En ce cas, il n'appartient qu'au juge de l'Union européenne, saisi le cas échéant à titre préjudiciel, de contrôler le respect par cette directive des droits fondamentaux garantis par l'article 6 du traité sur l'Union européenne. Dès lors, une question prioritiare de constitutionnalité contestant la conformité aux droits et libertés garantis par la Constitution de telles dispositions législatives n'est pas sérieuse, sauf s'il peut être sérieusement soutenu qu'elles mettent en cause une règle ou un principe inhérent à l'identité constitutionnelle de la France.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Cf. Cons. const. , 10 juin 2004, n° 2004-496 DC, loi pour la confiance dans l'économie numérique (LCEN) ; Cons. const.,27 juillet 2006, n° 2006-540 DC,  loi relative au droit d'auteur et aux droits voisins dans la société de l'information (DADVSI).</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
