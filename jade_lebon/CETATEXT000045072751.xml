<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000045072751</ID>
<ANCIEN_ID>JG_L_2022_01_000000440164</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/45/07/27/CETATEXT000045072751.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 1ère chambres réunies, 24/01/2022, 440164</TITRE>
<DATE_DEC>2022-01-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>440164</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE ; SCP DELAMARRE, JEHANNIN ; SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>Mme Marie Grosset</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2022:440164.20220124</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              D'une part, la commune de Guignen a demandé à la cour administrative d'appel de Nantes d'annuler pour excès de pouvoir l'avis de la Commission nationale d'aménagement commercial du 4 avril 2019 défavorable au projet de création par la société Guignen Dis II d'un hypermarché et d'un point permanent de retrait à l'enseigne E. Leclerc sur le territoire de la commune.<br/>
<br/>
              D'autre part, la SARL Guignen Dis II a demandé à la cour administrative d'appel de Nantes d'annuler pour excès de pouvoir la décision du 13 mai 2019 par laquelle le maire de Guignen a refusé de lui délivrer un permis de construire valant autorisation d'exploitation commerciale en vue de la création d'un hypermarché et d'un point permanent de retrait à l'enseigne E. Leclerc sur le territoire de la commune.<br/>
<br/>
              Par un arrêt n°s 19NT02099, 19NT02156 du 28 février 2020, la cour administrative d'appel de Nantes, joignant les deux requêtes, a, d'une part, annulé cet avis et cet arrêté, d'autre part, enjoint au maire de Guignen de statuer à nouveau sur la demande de permis de construire valant autorisation d'exploitation commerciale de la société Guignen Dis II, après nouvel examen du projet par la Commission nationale d'aménagement commercial, dans le délai de quatre mois à compter de la notification de son arrêt.<br/>
<br/>
              Par un pourvoi, trois nouveaux mémoires et un mémoire en réplique enregistrés les 20 avril, 24 juillet, 27 août et 8 septembre 2020 et le 3 février 2021 au secrétariat du contentieux du Conseil d'Etat, la société Année distribution, la société Govelomat, la société Guidis, la société Guijardy, la société Guivadis et la société Utilia demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter les requêtes de la commune de Guignen et de la société Guignen Dis II ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Guignen la somme de 5 600 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de commerce ;<br/>
              - le code de l'urbanisme ;<br/>
              - la loi n° 2014-626 du 18 juin 2014 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie Grosset, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de la société Année Distribution, de la société Govelomat, de la société Guidis, de la société Guijardy, de la société Guivadis et de la société Utilia, à la SCP Piwnica, Molinié, avocat de la société Guignen Dis II et à la SCP Delamarre, Jéhannin, avocat de la commune de Guignen ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la société Guignen Dis II a sollicité la délivrance d'un permis de construire valant autorisation d'exploitation commerciale en vue de la création d'un hypermarché à l'enseigne E. Leclerc d'une surface de vente de 2 500 m2 et d'un point permanent de retrait par la clientèle d'achats au détail, sur le territoire de la commune de Guignen. La commission départementale d'aménagement commercial d'Ille-et-Vilaine a émis un avis favorable au projet le 12 juillet 2018. Toutefois, sur recours des sociétés Année distribution, Govelomat, Guidis, Guijardy, Guivadis, qui exploitent des commerces alimentaires, et de la société Utilia, agence immobilière, la Commission nationale d'aménagement commercial a émis un avis défavorable le 8 novembre 2018. Le 28 janvier 2019, la commission départementale d'aménagement commercial a émis un avis favorable au projet modifié présenté par la société Guignen Dis II. Toutefois, la Commission nationale d'aménagement commercial, sur recours de la société Année distribution et autres, a émis un nouvel avis défavorable le 4 avril 2019, estimant que si l'intégration paysagère du projet avait été améliorée, la surface des espaces verts augmentée et le nombre de places de stationnement diminué, le projet aurait des effets négatifs sur les centres bourgs environnants et l'équilibre des implantations commerciales, en raison d'un dimensionnement excessif. Le maire de Guignen a, par arrêté du 13 mai 2019, refusé de délivrer à la société Guignen Dis II un permis de construire valant autorisation d'exploitation commerciale. Saisie, d'une part, d'une requête de la commune de Guignen tendant à l'annulation pour excès de pouvoir de l'avis du 4 avril 2019 et, d'autre part, d'une requête de la société Guignen Dis II tendant à l'annulation pour excès de pouvoir de l'arrêté du 13 mai 2019, la cour administrative d'appel de Nantes, par un arrêt du 28 février 2020, a annulé cet avis et cet arrêté et a enjoint au maire de Guignen de statuer à nouveau sur la demande de la société Guignen Dis II, après nouvel examen du projet par la Commission nationale d'aménagement commercial, dans le délai de quatre mois à compter de la notification de son arrêt. La société Année distribution, la société Govelomat, la société Guidis, la société Guijardy, la société Guivadis et la société Utilia se pourvoient en cassation contre cet arrêt. <br/>
<br/>
              Sur l'arrêt en tant qu'il a statué sur la requête de la commune de Guignen :<br/>
<br/>
              Sur le cadre juridique :<br/>
<br/>
              2. En premier lieu, aux termes de l'article L. 425-4 du code de l'urbanisme, dans sa rédaction issue de la loi du 18 juin 2014 relative à l'artisanat, au commerce et aux très petites entreprises : " Lorsque le projet est soumis à autorisation d'exploitation commerciale au sens de l'article L. 752-1 du code de commerce, le permis de construire tient lieu d'autorisation dès lors que la demande de permis a fait l'objet d'un avis favorable de la commission départementale d'aménagement commercial ou, le cas échéant, de la Commission nationale d'aménagement commercial (...) ". Aux termes de l'article L. 752-17 du code de commerce, dans sa version issue du même texte : " I.- Conformément à l'article L. 425-4 du code de l'urbanisme, le demandeur, le représentant de l'Etat dans le département, tout membre de la commission départementale d'aménagement commercial, tout professionnel dont l'activité, exercée dans les limites de la zone de chalandise définie pour chaque projet, est susceptible d'être affectée par le projet ou toute association les représentant peuvent, dans le délai d'un mois, introduire un recours devant la Commission nationale d'aménagement commercial contre l'avis de la commission départementale d'aménagement commercial. / La Commission nationale d'aménagement commercial émet un avis sur la conformité du projet aux critères énoncés à l'article L. 752-6 du présent code (...). / A peine d'irrecevabilité, la saisine de la commission nationale par les personnes mentionnées au premier alinéa du présent I est un préalable obligatoire au recours contentieux dirigé contre la décision de l'autorité administrative compétente pour délivrer le permis de construire. Le maire de la commune d'implantation du projet et le représentant de l'Etat dans le département ne sont pas tenus d'exercer ce recours préalable (...) ". Aux termes de l'article L. 422-1 du code de l'urbanisme : " L'autorité compétente pour délivrer le permis de construire, d'aménager ou de démolir et pour se prononcer sur un projet faisant l'objet d'une déclaration préalable est : a) Le maire, au nom de la commune, dans les communes qui se sont dotées d'un plan local d'urbanisme ou d'un document d'urbanisme en tenant lieu (...) ". Enfin, la même loi du 18 juin 2014 prévoit que la décision unique par laquelle l'autorité compétente octroie un permis de construire valant autorisation d'exploitation commerciale peut faire l'objet d'un recours pour excès de pouvoir, d'une part par les personnes mentionnées au I de l'article L. 752-17 du code de commerce, au nombre desquelles figurent notamment les professionnels dont l'activité, exercée dans les limites de la zone de chalandise définie pour le projet, est susceptible d'être affectée par celui-ci et, d'autre part, par les personnes mentionnées à l'article L. 600-1-2 du code de l'urbanisme, au nombre desquelles figurent notamment celles pour lesquelles la construction est de nature à affecter directement les conditions d'occupation, d'utilisation ou de jouissance du bien qu'elles détiennent ou occupent régulièrement. Pour chacune de ces deux catégories de requérants, l'article L. 600-1-4, introduit au code de l'urbanisme par la loi du 18 juin 2014, fixe des dispositions qui leur sont propres et prévoit en particulier que l'une et l'autre de ces catégories de requérants ne peuvent présenter des conclusions tendant à l'annulation de ce permis qu'en tant qu'il vaut autorisation d'exploitation commerciale pour la première, et en tant qu'il vaut autorisation de construire pour la seconde. <br/>
<br/>
              3. Il résulte des dispositions citées au point 2 que l'avis de la commission départementale d'aménagement commercial ou de la Commission nationale d'aménagement commercial a le caractère d'un acte préparatoire à la décision prise par l'autorité administrative sur la demande de permis de construire valant autorisation d'exploitation commerciale, seule décision susceptible de recours contentieux. Il en va ainsi que l'avis soit favorable ou qu'il soit défavorable. <br/>
<br/>
              4. Il résulte également des mêmes dispositions qu'alors même qu'un permis de construire tenant lieu d'autorisation d'exploitation commerciale en application des dispositions de l'article L. 425-4 du code de l'urbanisme ne peut être légalement délivré par le maire, au nom de la commune, que sur avis favorable de la commission départementale d'aménagement commercial compétente ou, le cas échéant, sur avis favorable de la Commission nationale d'aménagement commercial et qu'ainsi cet avis lie le maire s'agissant de l'autorisation d'exploitation commerciale sollicitée, la commune d'implantation du projet n'est pas recevable à demander l'annulation pour excès de pouvoir de cet avis, qui, comme il a été dit, a le caractère d'acte préparatoire à la décision prise sur la demande de permis de construire valant autorisation d'exploitation commerciale. Elle est en revanche recevable à contester, par la voie d'un recours pour excès de pouvoir, la décision qu'elle prend sur cette demande en tant seulement qu'elle se prononce sur l'autorisation d'exploitation commerciale sollicitée, pour autant qu'elle justifie d'un intérêt lui donnant qualité pour agir. <br/>
<br/>
              5. Il résulte de tout ce qui précède que les sociétés requérantes sont fondées à soutenir que la cour administrative d'appel de Nantes en tant qu'elle a statué sur la requête de la commune de Guignen et a prononcé l'annulation de l'avis défavorable du 4 avril 2019 de la Commission nationale d'aménagement commercial, acte insusceptible de recours, a entaché son arrêt d'une erreur de droit. <br/>
<br/>
              6. Par suite, et sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi dirigés contre cette partie de l'arrêt, l'arrêt de la cour administrative d'appel de Nantes doit, dans cette mesure, être annulé. <br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond, dans la mesure de la cassation prononcée au point 6, en application des dispositions de l'article L. 821-2 du code de justice administrative. <br/>
<br/>
              8. Il résulte de ce qui a été dit au point 5 que les conclusions, présentées par la commune de Guiguen, aux fins d'annulation pour excès de pouvoir de l'avis émis par la Commission nationale d'aménagement commercial le 4 avril 2019 sont irrecevables et ne peuvent qu'être rejetées. <br/>
<br/>
              Sur l'arrêt en tant qu'il a statué sur la requête de la société Guignen Dis II :<br/>
<br/>
              9. Il ressort des pièces de la procédure devant la cour administrative d'appel de Nantes que la société Année distribution et autres, qui avaient contesté devant la Commission nationale d'aménagement commercial l'avis favorable délivré par la commission départementale d'aménagement commercial, n'ont été ni mises en cause, ainsi qu'elles auraient dû l'être, ni représentées dans l'instance tendant à l'annulation de l'arrêté du 13 mai 2019 portant refus de permis de construire valant autorisation d'exploitation commerciale. Ces sociétés ne sont ainsi pas recevables à former un pourvoi en cassation contre l'arrêt en tant qu'il est relatif à la légalité de cet arrêté. L'arrêt étant toutefois dans cette mesure susceptible de préjudicier à leurs droits, leurs conclusions doivent, dans cette mesure, être regardées comme une requête en tierce opposition qu'il convient de renvoyer à la cour administrative d'appel de Nantes. <br/>
<br/>
              Sur les conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative<br/>
<br/>
              10. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par les requérantes à l'encontre de la commune de Guignen au titre de l'article L. 761-1 du code de justice administrative. Ces mêmes dispositions font obstacle à ce qu'il soit fait droit aux conclusions présentées à ce titre par la commune de Guignen à l'encontre des requérantes qui, dans cette partie du litige, ne sont les parties perdantes. <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nantes est annulé en tant qu'il statue sur la requête de la commune de Guignen.  <br/>
Article 2 : La requête de la commune de Guignen est rejetée. <br/>
Article 3 : La requête de la société Année Distribution et autres est renvoyée à la cour administrative d'appel de Nantes en tant qu'elle conteste l'arrêt de la cour administrative d'appel de Nantes du 28 février 2020 en tant qu'il a statué sur la requête de la société Guignen Dis II. <br/>
Article 4 : Les conclusions présentées, d'une part, par la commune de Guignen en cassation et, d'autre part, par la société Année Distribution, au titre de l'article L. 761-1 du code de justice administrative, sont rejetées.<br/>
Article 5 : La présente décision sera notifiée aux sociétés Année distribution, Govelomat, Guidis, Guijardy, Guivadis, Utilia, à la commune de Guignen, à la société Guignen Dis et au ministre de l'économie, des finances et de la relance. <br/>
              Délibéré à l'issue de la séance du 5 janvier 2022 où siégeaient : M. Rémy Schwartz, président adjoint de la section du contentieux, présidant ; Mme A... N..., Mme E... M..., présidentes de chambre ; M. K... H..., Mme J... L..., Mme B... G..., M. C... I... et Mme Carine Chevrier conseillers d'Etat et Mme Marie Grosset, maître des requêtes-rapporteure.<br/>
<br/>
              Rendu le 24 janvier 2022.<br/>
<br/>
<br/>
                 Le président : <br/>
                 Signé : M. Rémy Schwartz<br/>
 		La rapporteure : <br/>
      Signé : Mme Marie Grosset<br/>
                 La secrétaire :<br/>
                 Signé : Mme D... F...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">14-02-01-05 COMMERCE, INDUSTRIE, INTERVENTION ÉCONOMIQUE DE LA PUISSANCE PUBLIQUE. - RÉGLEMENTATION DES ACTIVITÉS ÉCONOMIQUES. - ACTIVITÉS SOUMISES À RÉGLEMENTATION. - AMÉNAGEMENT COMMERCIAL. - ACTE SUSCEPTIBLE D'UN RECOURS POUR EXCÈS DE POUVOIR DE LA COMMUNE D'IMPLANTATION DU PROJET - 1) AVIS DE LA CDAC OU DE LA CNAC - ABSENCE, ALORS MÊME QUE CET AVIS LIE LE MAIRE S'AGISSANT DE L'AUTORISATION D'EXPLOITATION COMMERCIALE [RJ1] - 2) DÉCISION PRISE SUR LA DEMANDE DE PERMIS DE CONSTRUIRE (ART. L. 425-4 DU CODE DE L'URBANISME) - A) EN TANT QU'ELLE SE PRONONCE SUR L'AUTORISATION DE CONSTRUIRE - ABSENCE - B) EN TANT QU'ELLE SE PRONONCE SUR L'AUTORISATION D'EXPLOITATION COMMERCIALE - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-01-01-01 PROCÉDURE. - INTRODUCTION DE L'INSTANCE. - DÉCISIONS POUVANT OU NON FAIRE L'OBJET D'UN RECOURS. - ACTES CONSTITUANT DES DÉCISIONS SUSCEPTIBLES DE RECOURS. - AMÉNAGEMENT COMMERCIAL - ACTE SUSCEPTIBLE D'UN RECOURS POUR EXCÈS DE POUVOIR DE LA COMMUNE D'IMPLANTATION DU PROJET - DÉCISION PRISE SUR LA DEMANDE DE PERMIS DE CONSTRUIRE (ART. L. 425-4 DU CODE DE L'URBANISME) -1) EN TANT QU'ELLE SE PRONONCE SUR L'AUTORISATION DE CONSTRUIRE - ABSENCE - 2) EN TANT QU'ELLE SE PRONONCE SUR L'AUTORISATION D'EXPLOITATION COMMERCIALE - EXISTENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-01-01-02-01 PROCÉDURE. - INTRODUCTION DE L'INSTANCE. - DÉCISIONS POUVANT OU NON FAIRE L'OBJET D'UN RECOURS. - ACTES NE CONSTITUANT PAS DES DÉCISIONS SUSCEPTIBLES DE RECOURS. - AVIS ET PROPOSITIONS. - AMÉNAGEMENT COMMERCIAL - ACTE SUSCEPTIBLE D'UN RECOURS POUR EXCÈS DE POUVOIR DE LA COMMUNE D'IMPLANTATION DU PROJET - AVIS DE LA CDAC OU DE LA CNAC - ABSENCE, ALORS MÊME QUE CET AVIS LIE LE MAIRE S'AGISSANT DE L'AUTORISATION D'EXPLOITATION COMMERCIALE [RJ1].
</SCT>
<ANA ID="9A"> 14-02-01-05 1) Il résulte des articles L. 425-4 et L. 600-1-4 du code de l'urbanisme et de l'article L. 752-17 du code de commerce dans leur rédaction issue de la loi n° 2014-626 du 18 juin 2014, ainsi que de l'article L. 422-1 du code de l'urbanisme, qu'alors même qu'un permis de construire tenant lieu d'autorisation d'exploitation commerciale en application de l'article L. 425-4 du code de l'urbanisme ne peut être légalement délivré par le maire, au nom de la commune, que sur avis favorable de la commission départementale d'aménagement commercial (CDAC) compétente ou, le cas échéant, sur avis favorable de la Commission nationale d'aménagement commercial (CNAC) et qu'ainsi cet avis lie le maire s'agissant de l'autorisation d'exploitation commerciale sollicitée, la commune d'implantation du projet n'est pas recevable à demander l'annulation pour excès de pouvoir de cet avis, qui a le caractère d'acte préparatoire à la décision prise sur la demande de permis de construire valant autorisation d'exploitation commerciale.......2) Elle est en revanche recevable à contester, par la voie d'un recours pour excès de pouvoir, la décision qu'elle prend sur cette demande en tant a) seulement b) qu'elle se prononce sur l'autorisation d'exploitation commerciale sollicitée, pour autant qu'elle justifie d'un intérêt lui donnant qualité pour agir.</ANA>
<ANA ID="9B"> 54-01-01-01 La commune d'implantation d'un projet ayant donné lieu à demande d'autorisation d'exploitation commerciale est recevable à contester, par la voie d'un recours pour excès de pouvoir, la décision qu'elle prend sur cette demande en tant 1) seulement 2) qu'elle se prononce sur l'autorisation d'exploitation commerciale sollicitée, pour autant qu'elle justifie d'un intérêt lui donnant qualité pour agir.</ANA>
<ANA ID="9C"> 54-01-01-02-01 Il résulte des articles L. 425-4 et L. 600-1-4 du code de l'urbanisme et de l'article L. 752-17 du code de commerce dans leur rédaction issue de la loi n° 2014-626 du 18 juin 2014, ainsi que de l'article L. 422-1 du code de l'urbanisme, qu'alors même qu'un permis de construire tenant lieu d'autorisation d'exploitation commerciale en application de l'article L. 425-4 du code de l'urbanisme ne peut être légalement délivré par le maire, au nom de la commune, que sur avis favorable de la commission départementale d'aménagement commercial (CDAC) compétente ou, le cas échéant, sur avis favorable de la Commission nationale d'aménagement commercial (CNAC) et qu'ainsi cet avis lie le maire s'agissant de l'autorisation d'exploitation commerciale sollicitée, la commune d'implantation du projet n'est pas recevable à demander l'annulation pour excès de pouvoir de cet avis, qui a le caractère d'acte préparatoire à la décision prise sur la demande de permis de construire valant autorisation d'exploitation commerciale.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., en précisant, CE, 25 mars 2020, Société Le Parc du Béarn, n° 409675, T. pp. 634-883. Comp. CE, 23 avril 1969, Prat et ville de Toulouse c/ Prat, n° 69476, p. 219.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
