<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028161253</ID>
<ANCIEN_ID>JG_L_2013_11_000000354931</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/16/12/CETATEXT000028161253.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 4ème sous-sections réunies, 06/11/2013, 354931, Publié au recueil Lebon</TITRE>
<DATE_DEC>2013-11-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>354931</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 4ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MONOD, COLIN ; SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>Mme Marie Gautier-Melleray</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:354931.20131106</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 16 décembre 2011 et 12 mars 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour Mme C...B..., veuveA..., demeurant... ; Mme  A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 09MA02793 du 17 octobre 2011 par lequel la cour administrative d'appel de Marseille a rejeté l'appel de M. et Mme A...contre le jugement n° 0703332 du tribunal administratif de Montpellier du 12 juin 2009 rejetant leur demande tendant à ce que la commune de Mauguio soit condamnée à réparer les dommages qu'ils ont subis en raison de la construction et de l'exploitation de la médiathèque municipale ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
              3°) de mettre à la charge de la commune de Mauguio le versement d'une somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi n° 68-1250 du 31 décembre 1968 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie Gautier-Melleray, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Monod, Colin, avocat de MmeB..., veuve A...et à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la commune de Mauguio ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que Mme B..., veuveA..., est propriétaire d'une maison située sur une place de la commune de Mauguio (Hérault) sur laquelle a été édifiée en 1995 une médiathèque municipale ; que M. et Mme A...ont recherché la responsabilité de la commune de Maugio, en invoquant les dommages subis du fait de cet ouvrage public ; que Mme A...se pourvoit en cassation contre l'arrêt du 17 octobre 2011 par lequel la cour administrative d'appel de Marseille a confirmé le jugement du 12 juin 2009 du tribunal administratif de Montpellier qui avait retenu l'exception de prescription quadriennale opposée par la commune à leurs conclusions indemnitaires ; <br/>
<br/>
              2. Considérant qu'aux termes du premier alinéa de l'article 1er de la loi du 31 décembre 1968 relative à la prescription des créances sur l'Etat, les départements, les communes et les établissements publics  : " Sont prescrites, au profit de l'État, des départements et des communes, sans préjudice des déchéances particulières édictées par la loi, et sous réserve des dispositions de la présente loi, toutes créances qui n'ont pas été payées dans un délai de quatre ans à partir du premier jour de l'année suivant celle au cours de laquelle les droits ont été acquis " ; <br/>
<br/>
              3. Considérant que, lorsque la responsabilité d'une personne publique est recherchée au titre d'un dommage causé à un tiers par un ouvrage public, les droits de créance invoqués par ce tiers en vue d'obtenir l'indemnisation de ses préjudices doivent être regardés comme acquis, au sens de ces dispositions, à la date à laquelle la réalité et l'étendue de ces préjudices ont été entièrement révélés, ces préjudices étant connus et pouvant être exactement mesurés ; que la créance indemnitaire relative à la réparation d'un préjudice présentant un caractère évolutif doit être rattachée à chacune des années au cours desquelles ce préjudice a été subi ;<br/>
<br/>
              4. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que le préjudice tenant à la perte de valeur vénale de la maison de M. et MmeA..., liée à une privation de vue et d'ensoleillement et à la réverbération des rayons solaires se reflétant sur les vitres de l'ouvrage public, était entièrement connu dans son existence et son étendue dès la mise en service de la médiathèque, en 1995 ; que, dès lors, en rattachant la totalité de ce chef de préjudice à l'année 1995 pour la computation du délai de prescription institué par l'article 1er de la loi du 31 décembre 1968, la cour administrative d'appel de Marseille n'a pas commis d'erreur de droit ; que, sur ce point, son arrêt n'est entaché ni d'insuffisance de motivation ni de dénaturation des faits de la cause ; <br/>
<br/>
              5. Considérant, en revanche, que le préjudice résultant des nuisances sonores liées au fonctionnement de la pompe à chaleur de la médiathèque était par nature susceptible d'évoluer dans le temps, en fonction des conditions d'utilisation de cette installation et des mesures susceptibles d'être prises pour en limiter les nuisances ; qu'en rattachant un tel préjudice dans son ensemble à la seule année 1995 et non à chacune des années durant lesquelles il a été subi par M. et Mme A...et en en déduisant que la prescription quadriennale était opposable aux conclusions tendant à l'indemnisation de ce préjudice, la cour administrative d'appel de Marseille a commis une erreur de droit ; que, dès lors, son arrêt doit être annulé en tant qu'il statue sur les conclusions d'appel de M. et Mme A...tendant à l'indemnisation du préjudice subi au titre des nuisances sonores qu'ils invoquent ; <br/>
<br/>
              6. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative, dans la mesure de la cassation prononcée ;<br/>
<br/>
              7. Considérant qu'il résulte de l'instruction que les nuisances sonores provoquées par le fonctionnement de la pompe à chaleur de la médiathèque ont été qualifiées de modérées par l'expertise réalisée le 6 avril 2007, l'expert relevant que la maison de M. et Mme A... est exposée à d'autres nuisances sonores, imputables notamment à la circulation des véhicules empruntant la voie située devant leur maison ; qu'il n'est pas établi que ces nuisances aient été aggravées par le remplacement de la pompe à chaleur de la médiathèque survenu en 2008 ; que, faute pour ces nuisances d'excéder les sujétions que les riverains doivent normalement supporter, le préjudice ainsi invoqué n'est pas de nature à engager la responsabilité de la commune ; que par suite, Mme A...n'est pas fondée à se plaindre de ce que, par son jugement du 12 juin 2009, le tribunal administratif de Montpellier a refusé de condamner la commune de Mauguio à réparer ce chef de préjudice ;<br/>
<br/>
              8. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la commune de Mauguio qui n'est pas, dans la présente instance, la partie perdante ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées au même titre par la commune ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt n° 09MA02793 de la cour administrative d'appel de Marseille du 17 octobre 2011 est annulé en tant qu'il statue sur les conclusions d'appel de M. et Mme A...tendant à l'indemnisation du préjudice subi au titre des nuisances sonores.<br/>
<br/>
Article 2 : Les conclusions présentées sur ce point par M. et Mme A...devant la cour administrative d'appel de Marseille et le surplus des conclusions du pourvoi de Mme A...sont rejetés.<br/>
<br/>
Article 3 : Les conclusions présentées par la commune de Mauguio au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 4 : La présente décision sera notifiée à MmeB..., Veuve A...et à la commune de Mauguio.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">18-04-02-04 COMPTABILITÉ PUBLIQUE ET BUDGET. DETTES DES COLLECTIVITÉS PUBLIQUES - PRESCRIPTION QUADRIENNALE. RÉGIME DE LA LOI DU 31 DÉCEMBRE 1968. POINT DE DÉPART DU DÉLAI. - DOMMAGE CAUSÉ À UN TIERS PAR UN OUVRAGE PUBLIC - 1) RÈGLE - DATE À LAQUELLE LA RÉALITÉ ET L'ÉTENDUE DU PRÉJUDICE ONT ÉTÉ ENTIÈREMENT RÉVÉLÉES - CAS PARTICULIER D'UN PRÉJUDICE ÉVOLUTIF [RJ1] - RATTACHEMENT DE LA CRÉANCE À CHACUNE DES ANNÉES AU COURS DESQUELLES LE PRÉJUDICE A ÉTÉ SUBI - 2) APPLICATION EN L'ESPÈCE - A) PRÉJUDICE SUBI DU FAIT DE L'ÉDIFICATION DE L'OUVRAGE - PRÉJUDICE ENTIÈREMENT CONNU DÈS LA MISE EN SERVICE - RATTACHEMENT À L'ANNÉE DE MISE EN SERVICE - B) PRÉJUDICE SUBI DU FAIT DE L'EXPLOITATION DE L'OUVRAGE - PRÉJUDICE ÉVOLUTIF - RATTACHEMENT À CHACUNE DES ANNÉES DURANT LESQUELLES IL A ÉTÉ SUBI.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">60-01-02-01-03-01-01 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. FAITS SUSCEPTIBLES OU NON D'OUVRIR UNE ACTION EN RESPONSABILITÉ. FONDEMENT DE LA RESPONSABILITÉ. RESPONSABILITÉ SANS FAUTE. RESPONSABILITÉ ENCOURUE DU FAIT DE L'EXÉCUTION, DE L'EXISTENCE OU DU FONCTIONNEMENT DE TRAVAUX OU D'OUVRAGES PUBLICS. VICTIMES AUTRES QUE LES USAGERS DE L'OUVRAGE PUBLIC. TIERS. - PRESCRIPTION QUADRIENNALE - POINT DE DÉPART DU DÉLAI - 1) RÈGLE - DATE À LAQUELLE LA RÉALITÉ ET L'ÉTENDUE DU PRÉJUDICE ONT ÉTÉ ENTIÈREMENT RÉVÉLÉES - CAS PARTICULIER D'UN PRÉJUDICE ÉVOLUTIF [RJ1] - RATTACHEMENT DE LA CRÉANCE À CHACUNE DES ANNÉES AU COURS DESQUELLES LE PRÉJUDICE A ÉTÉ SUBI - 2) APPLICATION EN L'ESPÈCE - A) PRÉJUDICE SUBI DU FAIT DE L'ÉDIFICATION DE L'OUVRAGE - PRÉJUDICE ENTIÈREMENT CONNU DÈS LA MISE EN SERVICE - RATTACHEMENT À L'ANNÉE DE MISE EN SERVICE - B) PRÉJUDICE SUBI DU FAIT DE L'EXPLOITATION DE L'OUVRAGE - PRÉJUDICE ÉVOLUTIF - RATTACHEMENT À CHACUNE DES ANNÉES DURANT LESQUELLES IL A ÉTÉ SUBI.
</SCT>
<ANA ID="9A"> 18-04-02-04 1) Lorsque la responsabilité d'une personne publique est recherchée, les droits de créance invoqués en vue d'obtenir l'indemnisation des préjudices doivent être regardés comme acquis, au sens des dispositions de l'article 1er de la loi n° 68-1250 du 31 décembre 1968, à la date à laquelle la réalité et l'étendue de ces préjudices ont été entièrement révélées, ces préjudices étant connus et pouvant être exactement mesurés. La créance indemnitaire relative à la réparation d'un préjudice présentant un caractère évolutif doit être rattachée à chacune des années au cours desquelles ce préjudice a été subi.,,,2) Préjudices tenant à des dommages subis par les propriétaires d'une maison d'habitation du fait de l'édification d'un ouvrage public.... ,,a) Le préjudice tenant à la perte de valeur vénale de la maison, liée à une privation de vue et d'ensoleillement et à la réverbération des rayons solaires se reflétant sur les vitres de l'ouvrage public, était entièrement connu dans son existence et son étendue dès la mise en service de ce dernier et se rattachait donc en totalité à l'année de cette mise en service.,,,b) En revanche, le préjudice résultant des nuisances sonores liées au fonctionnement de la pompe à chaleur de l'ouvrage était par nature susceptible d'évoluer dans le temps, en fonction des conditions d'utilisation de cette installation et des mesures susceptibles d'être prises pour en limiter les nuisances. Il devait donc être rattaché non pas, dans son ensemble, à la seule année de mise en service, mais à chacune des années durant lesquelles il a été subi.</ANA>
<ANA ID="9B"> 60-01-02-01-03-01-01 1) Lorsque la responsabilité d'une personne publique est recherchée, les droits de créance invoqués en vue d'obtenir l'indemnisation des préjudices doivent être regardés comme acquis, au sens des dispositions de l'article 1er de la loi n° 68-1250 du 31 décembre 1968, à la date à laquelle la réalité et l'étendue de ces préjudices ont été entièrement révélées, ces préjudices étant connus et pouvant être exactement mesurés. La créance indemnitaire relative à la réparation d'un préjudice présentant un caractère évolutif doit être rattachée à chacune des années au cours desquelles ce préjudice a été subi.,,,2) Préjudices tenant à des dommages subis par les propriétaires d'une maison d'habitation du fait de l'édification d'un ouvrage public.... ,,a) Le préjudice tenant à la perte de valeur vénale de la maison, liée à une privation de vue et d'ensoleillement et à la réverbération des rayons solaires se reflétant sur les vitres de l'ouvrage public, était entièrement connu dans son existence et son étendue dès la mise en service de ce dernier et se rattachait donc en totalité à l'année de cette mise en service.,,,b) En revanche, le préjudice résultant des nuisances sonores liées au fonctionnement de la pompe à chaleur de l'ouvrage était par nature susceptible d'évoluer dans le temps, en fonction des conditions d'utilisation de cette installation et des mesures susceptibles d'être prises pour en limiter les nuisances. Il devait donc être rattaché non pas, dans son ensemble, à la seule année de mise en service, mais à chacune des années durant lesquelles il a été subi.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Comp. CE, Section, 7 octobre 1966, Ville de Lagny, n° 64564, p. 528 et Ville de Bressuire, n° 61663, p. 529 ; CE, 10 mars 1972, Consorts Couzinet, n° 78595, p. 201.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
