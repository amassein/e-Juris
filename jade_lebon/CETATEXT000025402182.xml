<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025402182</ID>
<ANCIEN_ID>JG_L_2012_02_000000353150</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/40/21/CETATEXT000025402182.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème et 9ème sous-sections réunies, 20/02/2012, 353150</TITRE>
<DATE_DEC>2012-02-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>353150</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème et 9ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PEIGNOT, GARREAU, BAUER-VIOLAS ; SCP MONOD, COLIN</AVOCATS>
<RAPPORTEUR>M. Gilles Pellissier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Delphine Hedary</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:353150.20120220</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 5 et 20 octobre 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour le MINISTRE DE L'INTERIEUR, DE L'OUTRE-MER, DES COLLECTIVITES TERRITORIALES ET DE L'IMMIGRATION ; le ministre demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance du 16 septembre 2011 par laquelle le juge des référés du tribunal administratif de Paris, statuant sur le fondement des dispositions de l'article L. 521-1 du code de justice administrative, d'une part, a suspendu l'exécution de la décision du préfet de police du 1er août 2011 refusant l'admission de Mlle B...A...au séjour au titre de l'asile, d'autre part, lui a enjoint de procéder au réexamen de la demande d'admission au séjour de l'intéressée ;<br/>
<br/>
              2°) statuant en référé, de rejeter la demande de Mlle A...;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
<br/>
              Vu la loi n° 91-647 du 10 juillet 1991 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Gilles Pellissier, Maître des Requêtes,<br/>
<br/>
              - les observations de la SCP Peignot, Garreau, Bauer-Violas, avocat du MINISTRE DE L'INTERIEUR, DE L'OUTRE-MER, DES COLLECTIVITES TERRITORIALES ET DE L'IMMIGRATION et de la SCP Monod, Colin, avocat de Mlle A...,<br/>
<br/>
              - les conclusions de Mme Delphine Hedary, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Peignot, Garreau, Bauer-Violas, avocat du MINISTRE DE L'INTERIEUR, DE L'OUTRE-MER, DES COLLECTIVITES TERRITORIALES ET DE L'IMMIGRATION et à la SCP Monod, Colin, avocat de Mlle A... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ;<br/>
<br/>
              Considérant que l'urgence justifie que soit prononcée la suspension d'un acte administratif lorsque l'exécution de celui-ci porte atteinte, de manière suffisamment grave et immédiate, à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre ; qu'il appartient au juge des référés d'apprécier concrètement, compte tenu des justifications fournies par le demandeur, si les effets de l'acte litigieux sont de nature à caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, l'exécution de la décision soit suspendue ;<br/>
<br/>
              Considérant que MlleA..., se disant de nationalité sierra-léonaise ou nigériane, a présenté, après le rejet initial d'une première demande à la suite de laquelle une décision d'éloignement du territoire français avait été prise à son encontre, une nouvelle demande d'asile ; que, par décision du 1er août 2011, le préfet de police, estimant que cette demande n'avait pour but que de faire échec à la mesure d'éloignement, a, sur le fondement du 4° de l'article L. 741-4 du code de l'entrée et du séjour des étrangers et du droit d'asile, refusé de l'admettre au séjour et l'a informée que sa demande d'asile serait, par application de l'article L. 723-1 du même code, traitée selon la procédure prioritaire ; que, par une ordonnance du 16 septembre 2011 contre laquelle le MINISTRE DE L'INTERIEUR se pourvoit en cassation, le juge des référés du tribunal administratif de Paris a, sur le fondement de l'article L. 521-1 du code de justice administrative, suspendu l'exécution de cette décision ;<br/>
<br/>
              Considérant que l'article L. 742-6 du code de l'entrée et du séjour des étrangers et du droit d'asile prévoit que l'étranger présent sur le territoire français dont la demande d'asile entrant dans les cas visés aux 2° à 4° de l'article L. 741-4 de ce code est rejetée bénéficie du droit de se maintenir en France jusqu'à la décision de l'Office français de protection des réfugiés et apatrides (OFPRA) ; que, par suite, la décision par laquelle le préfet refuse, en application des dispositions des 2° à 4° de l'article L. 741-4 du code de l'entrée et du séjour des étrangers et du droit d'asile, d'admettre au séjour un demandeur d'asile, bien qu'elle ait pour conséquence de priver d'effet suspensif l'éventuel recours qu'il pourra former devant la Cour nationale du droit d'asile contre la décision de l'OFPRA si celle-ci rejette sa demande d'asile, n'a aucun effet propre sur son droit au séjour jusqu'à l'intervention de la décision de l'OFPRA ; qu'ainsi elle ne porte pas, par elle-même,  une atteinte suffisamment grave et immédiate à la situation du demandeur d'asile pour que la condition d'urgence soit, sauf circonstances particulières, toujours regardée comme remplie ; qu'en retenant une telle présomption et en jugeant que la condition tenant à l'urgence à suspendre l'exécution de la décision du préfet de police de Paris refusant, sur le fondement du 4° de l'article L. 741-4 du code de l'entrée et du séjour des étrangers et du droit d'asile, d'admettre au séjour MlleA..., devait être regardée comme remplie dès lors que le préfet de police ne faisait état d'aucune circonstance de nature à faire échec à cette présomption, le juge des référés du tribunal administratif de Paris a commis une erreur de droit ; que son ordonnance doit dès lors, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, être annulée ;<br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, en application des dispositions de l'article L. 821-2 du code de justice administrative, de régler l'affaire au titre de la procédure de référé engagée ;<br/>
<br/>
              Considérant que MlleA..., qui se borne à faire état de risques encourus en cas de retour au Nigeria, n'apporte aucun élément de nature à établir que l'exécution de la décision refusant de l'admettre au séjour porterait à ses intérêts une atteinte grave et immédiate justifiant la suspension de son exécution ; que, dès lors, la condition d'urgence posée par l'article L. 521-1 du code de justice administrative n'étant pas remplie, la demande présentée par Mlle A... devant le juge des référés du tribunal administratif de Paris tendant à ce que soit ordonnée la suspension de la décision du 1er août 2011 du préfet de police et à ce qu'il lui soit enjoint de réexaminer sa demande d'admission au séjour ne peut qu'être rejetée ; que les dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 font obstacle à ce qu'il soit fait droit aux conclusions présentées sur le fondement de ces dispositions par la SCP Monod - Colin, avocat de Mlle A...;<br/>
<br/>
<br/>
<br/>				D E C I D E  :<br/>
              				--------------<br/>
<br/>
Article 1er : L'ordonnance du juge des référés du tribunal administratif de Paris du 16 septembre 2011 est annulée.<br/>
<br/>
Article 2 : La demande présentée par Mlle A...devant le juge des référés du tribunal administratif de Paris ainsi que les conclusions présentées devant le Conseil d'Etat par son avocat sur le fondement de l'article 37 de la loi du 10 juillet 1991 sont rejetées.<br/>
<br/>
Article 3 : La présente décision sera notifiée au MINISTRE DE L'INTERIEUR, DE L'OUTRE-MER, DES COLLECTIVITES TERRITORIALES ET DE L'IMMIGRATION et à Mlle B...A....<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">095-02-04 - DÉCISION PRÉFECTORALE DE REFUS D'ADMISSION AU SÉJOUR D'UN DEMANDEUR D'ASILE EN APPLICATION DES 2° À 4° DE L'ART. L. 741-4 DU CESEDA - RÉFÉRÉ-SUSPENSION (ART. L. 521-1 DU CJA) - CONDITION D'URGENCE - PRÉSOMPTION - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">335-01-03 ÉTRANGERS. SÉJOUR DES ÉTRANGERS. REFUS DE SÉJOUR. - DÉCISION PRÉFECTORALE DE REFUS D'ADMISSION AU SÉJOUR D'UN DEMANDEUR D'ASILE EN APPLICATION DES 2° À 4° DE L'ART. L. 741-4 DU CESEDA - RÉFÉRÉ-SUSPENSION (ART. L. 521-1 DU CJA) - CONDITION D'URGENCE - PRÉSOMPTION - ABSENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-035-02-03-02 PROCÉDURE. PROCÉDURES INSTITUÉES PAR LA LOI DU 30 JUIN 2000. RÉFÉRÉ SUSPENSION (ART. L. 521-1 DU CODE DE JUSTICE ADMINISTRATIVE). CONDITIONS D'OCTROI DE LA SUSPENSION DEMANDÉE. URGENCE. - PRÉSOMPTION - ABSENCE - DÉCISION PRÉFECTORALE DE REFUS D'ADMISSION AU SÉJOUR D'UN DEMANDEUR D'ASILE EN APPLICATION DES 2° À 4° DE L'ART. L. 741-4 DU CESEDA.
</SCT>
<ANA ID="9A"> 095-02-04 Il résulte de l'article L. 742-6 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA) que la décision par laquelle le préfet refuse, en application des dispositions des 2° à 4° de l'article L. 741-4 du même code, d'admettre au séjour un demandeur d'asile, bien qu'elle ait pour conséquence de priver d'effet suspensif l'éventuel recours qu'il pourra former devant la Cour nationale du droit d'asile contre la décision de l'Office français de protection des réfugiés et apatrides (OFPRA) si celle-ci rejette sa demande d'asile, n'a aucun effet propre sur son droit au séjour jusqu'à l'intervention de la décision de l'OFPRA. Dès lors, une telle décision ne porte pas, par elle-même, une atteinte suffisamment grave et immédiate à la situation du demandeur d'asile pour que la condition d'urgence à laquelle est subordonné l'octroi d'une mesure de suspension en application de l'article L. 521-1 du code de justice administrative (CJA) soit, sauf circonstances particulières, toujours regardée comme remplie.</ANA>
<ANA ID="9B"> 335-01-03 Il résulte de l'article L. 742-6 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA) que la décision par laquelle le préfet refuse, en application des dispositions des 2° à 4° de l'article L. 741-4 du même code, d'admettre au séjour un demandeur d'asile, bien qu'elle ait pour conséquence de priver d'effet suspensif l'éventuel recours qu'il pourra former devant la Cour nationale du droit d'asile contre la décision de l'Office français de protection des réfugiés et apatrides (OFPRA) si celle-ci rejette sa demande d'asile, n'a aucun effet propre sur son droit au séjour jusqu'à l'intervention de la décision de l'OFPRA. Dès lors, une telle décision ne porte pas, par elle-même, une atteinte suffisamment grave et immédiate à la situation du demandeur d'asile pour que la condition d'urgence à laquelle est subordonné l'octroi d'une mesure de suspension en application de l'article L. 521-1 du code de justice administrative (CJA) soit, sauf circonstances particulières, toujours regardée comme remplie.</ANA>
<ANA ID="9C"> 54-035-02-03-02 Il résulte de l'article L. 742-6 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA) que la décision par laquelle le préfet refuse, en application des dispositions des 2° à 4° de l'article L. 741-4 du même code, d'admettre au séjour un demandeur d'asile, bien qu'elle ait pour conséquence de priver d'effet suspensif l'éventuel recours qu'il pourra former devant la Cour nationale du droit d'asile contre la décision de l'Office français de protection des réfugiés et apatrides (OFPRA) si celle-ci rejette sa demande d'asile, n'a aucun effet propre sur son droit au séjour jusqu'à l'intervention de la décision de l'OFPRA. Dès lors, une telle décision ne porte pas, par elle-même, une atteinte suffisamment grave et immédiate à la situation du demandeur d'asile pour que la condition d'urgence soit, sauf circonstances particulières, toujours regardée comme remplie.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
