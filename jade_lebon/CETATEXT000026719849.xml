<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026719849</ID>
<ANCIEN_ID>JG_L_2012_12_000000361287</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/71/98/CETATEXT000026719849.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème et 2ème sous-sections réunies, 03/12/2012, 361287</TITRE>
<DATE_DEC>2012-12-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>361287</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème et 2ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP COUTARD, MUNIER-APAIRE ; SCP BENABENT</AVOCATS>
<RAPPORTEUR>M. Fabrice Aubert</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:361287.20121203</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 24 juillet 2012 au secrétariat du contentieux du Conseil d'Etat, présenté pour le Syndicat mixte pour l'étude et le traitement des ordures ménagères (SETOM) de l'Eure, dont le siège est BP 110 à Evreux (27091) ; le SETOM de l'Eure demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'ordonnance n° 1201826 du 5 juillet 2012 par laquelle le juge des référés du tribunal administratif de Rouen, statuant en application de l'article L. 551-1 du code de justice administrative, a, à la demande de la société TP Tinel, mandataire du groupement solidaire formé avec la société Bec Frères, annulé la procédure lancée par le SETOM de l'Eure pour la passation d'un marché de substitution en vue de l'achèvement des travaux de création de casiers d'une installation de stockage de déchets ; <br/>
<br/>
              2°) statuant en référé, de rejeter la demande de la société TP Tinel ; <br/>
<br/>
              3°) de mettre à la charge de la société TP Tinel et de la société Bec Frères le versement de la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code des marchés publics ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Fabrice Aubert, Auditeur,  <br/>
<br/>
              - les observations de la SCP Coutard, Munier-Apaire, avocat du Syndicat mixte pour l'étude et le traitement des ordures ménagères (SETOM) de l'Eure, et de la SCP Bénabent, avocat de la société TP Tinel,<br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Coutard, Munier-Apaire, avocat du Syndicat mixte pour l'étude et le traitement des ordures ménagères (SETOM) de l'Eure, et à la SCP Bénabent, avocat de la société TP Tinel ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 551-1 du code de justice administrative : " Le président du tribunal administratif, ou le magistrat qu'il délègue, peut être saisi en cas de manquement aux obligations de publicité et de mise en concurrence auxquelles est soumise la passation par les pouvoirs adjudicateurs de contrats administratifs ayant pour objet l'exécution de travaux, la livraison de fournitures ou la prestation de services, avec une contrepartie économique constituée par un prix ou un droit d'exploitation, ou la délégation d'un service public (...) " ; qu'aux termes de l'article L. 551-2 de ce code : " I. Le juge peut ordonner à l'auteur du manquement de se conformer à ses obligations et suspendre l'exécution de toute décision qui se rapporte à la passation du contrat, sauf s'il estime, en considération de l'ensemble des intérêts susceptibles d'être lésés et notamment de l'intérêt public, que les conséquences négatives de ces mesures pourraient l'emporter sur leurs avantages. (...) " ; qu'aux termes de l'article L. 551-10 du même code : " Les personnes habilitées à engager les recours prévus aux articles L. 551-1 et L. 551-5 sont celles qui ont un intérêt à conclure le contrat et qui sont susceptibles d'être lésées par le manquement invoqué (...) " ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis au juge des référés que le Syndicat mixte pour l'étude et le traitement des ordures ménagères (SETOM) de l'Eure a conclu un marché public de travaux avec le groupement solidaire formé des sociétés TP Tinel, mandataire, et Bec Frères, pour la construction de " casiers " sur un site de traitement de déchets ; que le SETOM a résilié le marché aux frais et risques du groupement et a engagé une procédure, sur le fondement de l'article 28 du code des marchés publics, pour la passation d'un marché de substitution dans le but d'achever les travaux ; que l'offre du groupement sortant, qui s'est à nouveau porté candidat à l'attribution du marché de substitution, n'a pas été retenue ; que par l'ordonnance attaquée, le juge des référés du tribunal administratif de Rouen a fait droit aux conclusions présentées par la société TP Tinel en annulant dans son intégralité la procédure de passation de ce marché de substitution, au motif que le pouvoir adjudicateur avait indiqué, dans les documents de la consultation, mettre à disposition des candidats des matériaux entreposés sur le chantier, qui demeuraient la propriété de la société TP Tinel ; <br/>
<br/>
              3. Considérant qu'en cas de résiliation du marché, l'article 46.4 du cahier des clauses administratives générales applicable au litige soumis au juge des référés prévoit que : " Le maître d'ouvrage dispose du droit de racheter, en totalité ou partie: / les ouvrages provisoires utiles à l'exécution du marché ; / les matériaux approvisionnés, dans la limite où il en a besoin pour le chantier (...) " ; qu'aux termes de l'article 49.4 du même cahier : " (...) En cas de résiliation aux frais et risques de l'entrepreneur, il est passé un marché avec un autre entrepreneur pour l'achèvement des travaux. (...) Par exception aux dispositions du 42 de l'article 13, le décompte général du marché résilié ne sera notifié à l'entrepreneur qu'après règlement définitif du nouveau marché passé pour l'achèvement des travaux " ; qu'il résulte de ces dispositions, qui confèrent au maître d'ouvrage le droit d'acquérir les matériaux approvisionnés dont il a besoin pour le chantier, que le SETOM de l'Eure pouvait disposer de plein droit des matériaux d'isolation laissés sur le chantier par le groupement demandeur à l'issue de la résiliation du marché à ses frais et risques, traduisant ainsi sa décision de faire usage de son droit de rachat, alors même que le titulaire du marché résilié ne pouvait obtenir le paiement de ces matériaux que dans le cadre du règlement du marché résilié, intervenant après le règlement définitif du marché de substitution passé pour l'achèvement des travaux ; que, dès lors, en jugeant que constituait une information juridiquement inexacte et ainsi un manquement aux obligations de publicité et de mise en concurrence le fait pour le pouvoir adjudicateur d'indiquer aux candidats au marché de substitution qu'il mettrait à leur disposition les matériaux approvisionnés par le groupement sortant en se fondant sur ce que le rachat de ces matériaux n'était pas encore intervenu, sans prendre en considération  leur mise à disposition du pouvoir adjudicateur, de plein droit, en application des dispositions précitées, le juge des référés du tribunal administratif de Rouen a commis une erreur de droit ; que, par suite, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, son ordonnance doit être annulée ;<br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de statuer sur la demande en référé en application des dispositions de l'article L. 821-2 du code de justice administrative ; <br/>
<br/>
              5. Considérant, en premier lieu, que le moyen tiré par le groupement de ce que l'avis d'appel public à la concurrence n'aurait pas été publié dans le Bulletin officiel des annonces des marchés publics ou dans un journal habilité à recevoir des annonces légales, en méconnaissance des dispositions de l'article 40 du code des marchés publics, manque en fait, le marché ayant fait l'objet d'une publication dans le journal Paris Normandie, habilité à cette fin ; <br/>
<br/>
              6. Considérant, en deuxième lieu, qu'aux termes du 1° du I de l'article 80 du code des marchés publics : " Pour les marchés et accords-cadres passés selon une procédure formalisée autre que celle prévue au II de l'article 35, le pouvoir adjudicateur, dès qu'il a fait son choix pour une candidature ou une offre, notifie à tous les autres candidats le rejet de leur candidature ou de leur offre, en leur indiquant les motifs de ce rejet  (...) " ; que le moyen tiré de ce que le pouvoir adjudicateur aurait notifié au groupement le rejet de son offre sans exposer les motifs de ce rejet, en méconnaissance des dispositions précitées, doit être rejeté comme inopérant, ces dispositions n'étant pas applicables aux marchés passés, comme en l'espèce, en procédure dite " adaptée " sur le fondement de l'article 28 du même code ; <br/>
<br/>
              7. Considérant, en troisième lieu, qu'ainsi qu'il a été dit, que le SETOM de l'Eure n'a pas manqué à ses obligations de publicité et mise en concurrence en indiquant, dans les documents de la consultation, qu'il mettrait gratuitement à disposition des candidats des matériaux approvisionnés par le groupement sortant dans le cadre d'un marché résilié aux frais et risques des entrepreneurs, alors même qu'il n'aurait pas encore procédé au paiement dû au titre du rachat de ces matériaux ; qu'il résulte de tout ce qui précède que les conclusions présentées par la société TP Tinel doivent être rejetées ; <br/>
<br/>
              8. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge du SETOM de l'Eure, qui n'est pas la partie perdante dans la présente instance ; qu'en revanche, il y a lieu de mettre à la charge de la société TP Tinel et de la société Bec Frères le versement au SETOM de l'Eure de la somme globale de 4 500 euros ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du 5 juillet 2012 du juge des référés du tribunal administratif de Rouen est annulée. <br/>
Article 2 : La demande de la société TP Tinel devant le juge des référés du tribunal administratif de Rouen et ses conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 3 : La société TP Tinel et la société Bec Frères verseront solidairement au Syndicat mixte pour l'étude et le traitement des ordures ménagères (SETOM) de l'Eure la somme de 4 500 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée au Syndicat mixte pour l'étude et le traitement des ordures ménagères (SETOM) de l'Eure, à la société TP Tinel et à la société Bec Frères.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-04-02-02 MARCHÉS ET CONTRATS ADMINISTRATIFS. FIN DES CONTRATS. RÉSILIATION. EFFETS. - MARCHÉ DE TRAVAUX - DROIT DU MAÎTRE D'OUVRAGE DE DISPOSER DES MATÉRIAUX APPROVISIONNÉS - EXISTENCE (ART. 46.4 ET 49.4 DU CCAG TRAVAUX).
</SCT>
<ANA ID="9A"> 39-04-02-02 Il résulte des dispositions des articles 46.4 et 49.4 du cahier des clauses administratives générales (CCAG) travaux, qui confèrent au maître d'ouvrage le droit d'acquérir les matériaux approvisionnés dont il a besoin pour le chantier, que le maître d'ouvrage peut disposer de plein droit des matériaux laissés sur le chantier par le titulaire du marché à l'issue de la résiliation du marché à ses frais et risques, traduisant ainsi sa décision de faire usage de son droit de rachat, alors même que le titulaire du marché résilié ne pouvait obtenir le paiement de ces matériaux que dans le cadre du règlement du marché résilié, intervenant après le règlement définitif du marché de substitution passé pour l'achèvement des travaux.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
