<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038625568</ID>
<ANCIEN_ID>JG_L_2019_06_000000420861</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/62/55/CETATEXT000038625568.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 14/06/2019, 420861, Publié au recueil Lebon</TITRE>
<DATE_DEC>2019-06-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>420861</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SEVAUX, MATHONNET</AVOCATS>
<RAPPORTEUR>M. François Lelièvre</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Mireille Le Corre</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:420861.20190614</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A...B...a demandé au tribunal administratif de Marseille d'annuler l'arrêté du 16 septembre 2015 par lequel le préfet des Bouches-du-Rhône a refusé de lui délivrer un titre de séjour, lui a fait obligation de quitter le territoire français dans un délai de trente jours et a fixé le pays à destination duquel elle est susceptible d'être éloignée. Par un jugement n° 1702042 du 21 septembre 2017, le tribunal administratif de Marseille a rejeté sa demande. <br/>
<br/>
              Par une ordonnance n° 17MA05072 du 12 février 2018, le président de la 2ème chambre de la cour administrative d'appel de Marseille a rejeté l'appel formé par Mme B... contre ce jugement. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 23 mai et 20 août 2018 au secrétariat du contentieux du Conseil d'Etat, Mme B...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement de la somme de 3 000 euros à verser à la SCP Sevaux, Mathonnet, avocat de MmeB..., au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. François Lelièvre, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Mireille Le Corre, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Sevaux, Mathonnet, avocat de MmeB... ; <br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que, par un jugement du 21 septembre 2017, le tribunal administratif de Marseille a rejeté la demande de Mme B...tendant à l'annulation de l'arrêté du 16 septembre 2015 par lequel le préfet des Bouches-du-Rhône a refusé de lui délivrer un titre de séjour, lui a fait obligation de quitter le territoire français et a fixé le pays de destination. Par une ordonnance du 12 février 2018, le président de la 2ème chambre de la cour administrative d'appel de Marseille a rejeté l'appel formé par Mme B... comme manifestement irrecevable en application du 4° de l'article R. 222-1 du code de justice administrative, en l'absence de régularisation de la présentation des pièces jointes au moyen de l'application informatique mentionnée à l'article R. 414-1 du code de justice administrative, dénommée Télérecours, au motif que chacune des pièces n'avait pas été répertoriée par un signet distinct. Mme B...se pourvoit en cassation contre cette ordonnance. <br/>
<br/>
              Sur les conclusions aux fins de non-lieu :<br/>
<br/>
              2. La seule circonstance que, postérieurement à l'intervention de l'arrêté du 16 septembre 2015 portant refus de titre séjour, le préfet des Bouches-du-Rhône ait délivré à Mme B...une autorisation provisoire de séjour n'est pas de nature à rendre sans objet le pourvoi dirigé contre l'ordonnance qui a rejeté l'appel formé contre le jugement ayant rejeté les conclusions de l'intéressée tendant à l'annulation de cet arrêté.<br/>
<br/>
              Sur le pourvoi :<br/>
<br/>
              3. Aux termes de l'article R. 222-1 du code de justice administrative : " (...) les présidents de formation de jugement des tribunaux et des cours (...) peuvent, par ordonnance : / (...) 4° Rejeter les requêtes manifestement irrecevables, lorsque la juridiction n'est pas tenue d'inviter leur auteur à les régulariser ou qu'elles n'ont pas été régularisées à l'expiration du délai imparti par une demande en ce sens (...) / Les présidents des formations de jugement des cours peuvent (...), par ordonnance, rejeter (...) les requêtes dirigées contre des ordonnances prises en application des 1° à 5° du présent article (...) ". Aux termes de l'article R. 612-1 du même code : " Lorsque des conclusions sont entachées d'une irrecevabilité susceptible d'être couverte après l'expiration du délai de recours, la juridiction ne peut les rejeter en relevant d'office cette irrecevabilité qu'après avoir invité leur auteur à les régulariser. / (...) La demande de régularisation mentionne que, à défaut de régularisation, les conclusions pourront être rejetées comme irrecevables dès l'expiration du délai imparti qui, sauf urgence, ne peut être inférieur à quinze jours. La demande de régularisation tient lieu de l'information prévue à l'article R. 611-7 ". <br/>
<br/>
              4. Aux termes de l'article R. 412-2 du code de justice administrative : " Lorsque les parties joignent des pièces à l'appui de leurs requêtes et mémoires, elles en établissent simultanément un inventaire détaillé (...) ". L'article R. 414-1 du même code dispose que : " Lorsqu'elle est présentée par un avocat, un avocat au Conseil d'Etat et à la Cour de cassation, une personne morale de droit public autre qu'une commune de moins de 3 500 habitants ou un organisme de droit privé chargé de la gestion permanente d'un service public, la requête doit, à peine d'irrecevabilité, être adressée à la juridiction par voie électronique au moyen d'une application informatique dédiée accessible par le réseau internet. La même obligation est applicable aux autres mémoires du requérant (...) ". Aux termes de l'article R. 414-3 du même code, dans sa rédaction applicable au litige : " Par dérogation aux dispositions des articles R. 411-3, R. 411-4, R. 412-1 et R. 412-2, les requérants sont dispensés de produire des copies de leur requête et des pièces qui sont jointes à celle-ci et à leurs mémoires. / Les pièces jointes sont présentées conformément à l'inventaire qui en est dressé. / Lorsque le requérant transmet, à l'appui de sa requête, un fichier unique comprenant plusieurs pièces, chacune d'entre elles doit être répertoriée par un signet la désignant conformément à l'inventaire mentionné ci-dessus. S'il transmet un fichier par pièce, l'intitulé de chacun d'entre eux doit être conforme à cet inventaire. Le respect de ces obligations est prescrit à peine d'irrecevabilité de la requête. / Les mêmes obligations sont applicables aux autres mémoires du requérant, sous peine pour celui-ci, après invitation à régulariser non suivie d'effet, de voir ses écritures écartées des débats. / Si les caractéristiques de certaines pièces font obstacle à leur communication par voie électronique, ces pièces sont transmises sur support papier, dans les conditions prévues par l'article R. 412-2. L'inventaire des pièces transmis par voie électronique en fait mention ".<br/>
<br/>
              5. Les dispositions citées au point 4 relatives à la transmission de la requête et des pièces qui y sont jointes par voie électronique définissent un instrument et les conditions de son utilisation qui concourent à la qualité du service public de la justice rendu par les juridictions administratives et à la bonne administration de la justice. Elles ont pour finalité de permettre un accès uniformisé et rationalisé à chacun des éléments du dossier de la procédure, selon des modalités communes aux parties, aux auxiliaires de justice et aux juridictions. A cette fin, elles organisent la transmission par voie électronique des pièces jointes à la requête à partir de leur inventaire détaillé et font obligation à son auteur de les transmettre soit en un fichier unique, chacune d'entre elles devant alors être répertoriée par un signet la désignant, soit en les distinguant chacune par un fichier désigné, l'intitulé des signets ou des fichiers devant être conforme à l'inventaire qui accompagne la requête. Ces dispositions ne font pas obstacle, lorsque l'auteur de la requête entend transmettre un nombre important de pièces jointes constituant une série homogène eu égard à l'objet du litige, telles que des documents visant à établir la résidence en France d'un étranger au cours d'une année donnée, à ce qu'il les fasse parvenir à la juridiction en les regroupant dans un ou plusieurs fichiers sans répertorier individuellement chacune d'elles par un signet, à la condition que le référencement de ces fichiers ainsi que l'ordre de présentation, au sein de chacun d'eux, des pièces qu'ils regroupent soient conformes à l'énumération, figurant à l'inventaire, de toutes les pièces jointes à la requête.<br/>
<br/>
              6. Il résulte de ce qui précède qu'en jugeant que la requête d'appel formée par Mme B...méconnaissait les dispositions de l'article R. 414-3 du code de justice administrative au seul motif que le fichier unique joint à cette requête comportait des signets dont plusieurs renvoyaient chacun à plusieurs pièces, sans rechercher si ces pièces pouvaient faire l'objet d'une présentation groupée conformément à ce qui vient d'être dit, l'auteur de l'ordonnance attaquée a commis une erreur de droit. Ainsi, sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, Mme B...est fondée à demander l'annulation de l'ordonnance qu'elle attaque. <br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              Sur la recevabilité de la requête d'appel :<br/>
<br/>
              8. Si Mme B...pouvait, ainsi qu'il a été dit au point 5, regrouper dans un même fichier les pièces visant à établir sa résidence en France au cours d'une année donnée sans répertorier individuellement chacune d'elles par un signet, c'était à la condition d'énumérer toutes ces pièces dans l'inventaire détaillé qui accompagne la requête et de les regrouper en respectant l'ordre indiqué par cet inventaire. Or il ressort des pièces du dossier que l'inventaire qui accompagnait la requête d'appel de Mme B...ne comportait pas l'énumération des pièces regroupées par années de présence en France. Dans ces conditions, et quand bien même l'indication de ces pièces apparaissait dans la requête d'appel elle-même, les pièces jointes à la requête n'ont pas été présentées conformément aux exigences résultant de l'article R. 414-3 du code de justice administrative. La requête doit, par suite, être rejetée comme irrecevable.<br/>
<br/>
              Sur les frais du litige :<br/>
<br/>
              9. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mis à la charge de l'Etat, qui n'est pas la partie perdante dans la présente instance, le versement des sommes que demande, à ce titre, MmeB....<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du 12 février 2018 du président de la 2ème chambre de la cour administrative d'appel de Marseille est annulée.<br/>
Article 2 : La requête présentée par Mme B...devant la cour administrative d'appel de Marseille et les conclusions présentées devant le Conseil d'Etat au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à Mme A...B...et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-01-08 PROCÉDURE. INTRODUCTION DE L'INSTANCE. FORMES DE LA REQUÊTE. - PRÉSENTATION DE LA REQUÊTE PAR VOIE ÉLECTRONIQUE - FORMES IMPOSÉES À LA REQUÊTE ET AUX PIÈCES QUI Y SONT JOINTES (ART. R. 414-1 ET 414-3 DU CJA) - 1) PRINCIPE [RJ1] - 2) CAS OÙ LE REQUÉRANT ENTEND TRANSMETTRE UN NOMBRE IMPORTANT DE PIÈCES JOINTES CONSTITUANT UNE SÉRIE HOMOGÈNE - POSSIBILITÉ DE FAIRE PARVENIR CES PIÈCES EN LES REGROUPANT DANS UN OU PLUSIEURS FICHIERS SANS LES RÉPERTORIER INDIVIDUELLEMENT PAR UN SIGNET - EXISTENCE, À LA CONDITION D'ÉNUMÉRER TOUS CES FICHIERS ET PIÈCES DANS L'INVENTAIRE DÉTAILLÉ QUI ACCOMPAGNE LA REQUÊTE ET DE LES REGROUPER EN RESPECTANT L'ORDRE INDIQUÉ PAR CET INVENTAIRE [RJ2] - 3) ESPÈCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-07-01-03-02 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. QUESTIONS GÉNÉRALES. CONCLUSIONS. CONCLUSIONS IRRECEVABLES. - PRÉSENTATION DE LA REQUÊTE PAR VOIE ÉLECTRONIQUE - FORMES IMPOSÉES À LA REQUÊTE ET AUX PIÈCES QUI Y SONT JOINTES (ART. R. 414-1 ET R. 414-3 DU CJA) - 1) PRINCIPE [RJ1] - 2) ) CAS OÙ LE REQUÉRANT ENTEND TRANSMETTRE UN NOMBRE IMPORTANT DE PIÈCES JOINTES CONSTITUANT UNE SÉRIE HOMOGÈNE - PIÈCES REGROUPÉES DANS UN OU PLUSIEURS FICHIERS SANS ÊTRE RÉPERTORIÉES INDIVIDUELLEMENT PAR UN SIGNET - REQUÊTE IRRECEVABLE POUR CE MOTIF - ABSENCE, SOUS RÉSERVE QUE CES FICHIERS, ET EN LEUR SEIN LES PIÈCES, SOIENT PRÉSENTÉS CONFORMÉMENT À L'ÉNUMÉRATION, FIGURANT À L'INVENTAIRE, DE TOUTES LES PIÈCES JOINTES À LA REQUÊTE [RJ2] - 3) ESPÈCE.
</SCT>
<ANA ID="9A"> 54-01-08 1) Les articles R. 414-1 et R. 414-3 du code de justice administrative (CJA) relatifs à la transmission de la requête et des pièces qui y sont jointes par voie électronique définissent un instrument et les conditions de son utilisation qui concourent à la qualité du service public de la justice rendu par les juridictions administratives et à la bonne administration de la justice. Elles ont pour finalité de permettre un accès uniformisé et rationalisé à chacun des éléments du dossier de la procédure, selon des modalités communes aux parties, aux auxiliaires de justice et aux juridictions. A cette fin, elles organisent la transmission par voie électronique des pièces jointes à la requête à partir de leur inventaire détaillé et font obligation à son auteur de les transmettre soit en un fichier unique, chacune d'entre elles devant alors être répertoriée par un signet la désignant, soit en les distinguant chacune par un fichier désigné, l'intitulé des signets ou des fichiers devant être conforme à l'inventaire qui accompagne la requête.,,,2) Ces articles ne font pas obstacle, lorsque l'auteur de la requête entend transmettre un nombre important de pièces jointes constituant une série homogène eu égard à l'objet du litige, telles que des documents visant à établir la résidence en France d'un étranger au cours d'une année donnée, à ce qu'il les fasse parvenir à la juridiction en les regroupant dans un ou plusieurs fichiers sans répertorier individuellement chacune d'elles par un signet, à la condition que le référencement de ces fichiers ainsi que l'ordre de présentation, au sein de chacun d'eux, des pièces qu'ils regroupent soient conformes à l'énumération, figurant à l'inventaire, de toutes les pièces jointes à la requête.,,,3) Contentieux relatif à un refus de délivrance d'un titre de séjour. Si la requérante pouvait regrouper dans un même fichier les pièces visant à établir sa résidence en France au cours d'une année donnée sans répertorier individuellement chacune d'elles par un signet, c'était à la condition d'énumérer toutes ces pièces dans l'inventaire détaillé qui accompagne la requête et de les regrouper en respectant l'ordre indiqué par cet inventaire. Or il ressort des pièces du dossier que l'inventaire qui accompagnait sa requête d'appel ne comportait pas l'énumération des pièces regroupées par années de présence en France. Dans ces conditions, et quand bien même l'indication de ces pièces apparaissait dans la requête d'appel elle-même, les pièces jointes à la requête n'ont pas été présentées conformément aux exigences résultant de l'article R. 414-3 du CJA. La requête doit, par suite, être rejetée comme irrecevable.</ANA>
<ANA ID="9B"> 54-07-01-03-02 1) Les articles R. 414-1 et R. 414-3 du code de justice administrative (CJA) relatifs à la transmission de la requête et des pièces qui y sont jointes par voie électronique définissent un instrument et les conditions de son utilisation qui concourent à la qualité du service public de la justice rendu par les juridictions administratives et à la bonne administration de la justice. Elles ont pour finalité de permettre un accès uniformisé et rationalisé à chacun des éléments du dossier de la procédure, selon des modalités communes aux parties, aux auxiliaires de justice et aux juridictions. A cette fin, elles organisent la transmission par voie électronique des pièces jointes à la requête à partir de leur inventaire détaillé et font obligation à son auteur de les transmettre soit en un fichier unique, chacune d'entre elles devant alors être répertoriée par un signet la désignant, soit en les distinguant chacune par un fichier désigné, l'intitulé des signets ou des fichiers devant être conforme à l'inventaire qui accompagne la requête.,,,2) Ces articles ne font pas obstacle, lorsque l'auteur de la requête entend transmettre un nombre important de pièces jointes constituant une série homogène eu égard à l'objet du litige, telles que des documents visant à établir la résidence en France d'un étranger au cours d'une année donnée, à ce qu'il les fasse parvenir à la juridiction en les regroupant dans un ou plusieurs fichiers sans répertorier individuellement chacune d'elles par un signet, à la condition que le référencement de ces fichiers ainsi que l'ordre de présentation, au sein de chacun d'eux, des pièces qu'ils regroupent soient conformes à l'énumération, figurant à l'inventaire, de toutes les pièces jointes à la requête.,,,3) Contentieux relatif à un refus de délivrance d'un titre de séjour. Si la requérante pouvait regrouper dans un même fichier les pièces visant à établir sa résidence en France au cours d'une année donnée sans répertorier individuellement chacune d'elles par un signet, c'était à la condition d'énumérer toutes ces pièces dans l'inventaire détaillé qui accompagne la requête et de les regrouper en respectant l'ordre indiqué par cet inventaire. Or il ressort des pièces du dossier que l'inventaire qui accompagnait sa requête d'appel ne comportait pas l'énumération des pièces regroupées par années de présence en France. Dans ces conditions, et quand bien même l'indication de ces pièces apparaissait dans la requête d'appel elle-même, les pièces jointes à la requête n'ont pas été présentées conformément aux exigences résultant de l'article R. 414-3 du CJA. La requête doit, par suite, être rejetée comme irrecevable.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Section, 5 octobre 2018,,et autres, n° 418233, p. 367., ,[RJ2] Cf., en précisant, CE, 6 février 2019, SARL Attractive Fragrances et Cosmetics, n° 415582, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
