<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035774965</ID>
<ANCIEN_ID>JG_L_2017_10_000000394269</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/77/49/CETATEXT000035774965.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 11/10/2017, 394269</TITRE>
<DATE_DEC>2017-10-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>394269</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Jérôme Marchand-Arvier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:394269.20171011</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, un mémoire en réplique et un nouveau mémoire, enregistrés les 26 octobre 2015, 19 février et 24 mars 2016 au secrétariat du contentieux du Conseil d'Etat, M. A... B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la délibération du 19 mai 2015 du conseil dit " de l'article 7 " de l'Institut d'études politiques de Paris rejetant sa candidature au poste de professeur des universités en science politique et le courrier du 28 août 2015 par lequel le directeur de cet établissement a rejeté son recours contre cette délibération ;<br/>
<br/>
              2°) d'enjoindre à l'Institut d'études politiques de Paris de transmettre sa candidature au ministre en vue de sa nomination à ce poste, subsidiairement, de réunir de nouveau le conseil dit " de l'article 7 " pour réexaminer sa candidature ;<br/>
<br/>
              3°) de mettre à la charge de l'Institut d'études politiques de Paris la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'éducation ;<br/>
              - le décret n° 84-431 du 6 juin 1984 ;<br/>
              - le décret n° 85-497 du 10 mai 1985 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jérôme Marchand-Arvier, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>1.  Considérant qu'il ressort des pièces du dossier que M. B... a présenté sa candidature pour le recrutement au poste de professeur des universités en science politique, profil " relations internationales et analyse comparée des politiques étrangères ", à l'Institut d'études politiques de Paris ; que sa candidature a été la seule retenue par le comité de sélection ; que toutefois, par une délibération du 19 mai 2015, le conseil dit " de l'article 7 " de l'Institut d'études politiques de Paris a décidé de ne pas proposer au ministre la nomination de l'intéressé ; que M. B... demande l'annulation pour excès de pouvoir de cette délibération et de la décision du directeur de l'Institut d'études politiques de Paris du 28 août 2015 rejetant son recours gracieux ;<br/>
<br/>
              2.  Considérant que le dernier alinéa de l'article L. 717-1 du code de l'éducation, applicable à l'Institut d'études politiques de Paris en sa qualité de grand établissement d'enseignement supérieur, dispose que : " Lorsqu'un conseil académique n'a pas été créé, les compétences mentionnées aux articles (...) L. 952-6 à L. 952-9 sont exercées par les instances de l'établissement prévues par les décrets mentionnés au troisième alinéa " ; que ces décrets sont ceux qui fixent les règles particulières d'organisation et de fonctionnement de l'établissement intéressé ; qu'aux termes de l'article 7 du décret du 10 mai 1985 relatif à l'Institut d'études politiques de Paris : " Un conseil composé des enseignants-chercheurs affectés à l'établissement est consulté sur les recrutements et les nominations dans les conditions prévues par le décret n° 84-431 du 6 juin 1984 relatif au statut des enseignants-chercheurs de l'enseignement supérieur " ; qu'il résulte de ces dispositions que le conseil dit " de l'article 7 " dont elles prévoient l'existence exerce, au sein de l'Institut d'études politiques de Paris, en vertu des dispositions citées ci-dessus de l'article L. 717-1 du code de l'éducation, les compétences dévolues au conseil académique par les articles L. 952-6 à L. 952-9 du même code ;<br/>
<br/>
              3.  Considérant qu'il résulte des dispositions de l'article L. 952-6-1 du code de l'éducation que, pour le recrutement d'un enseignant-chercheur, le comité de sélection, après avoir dressé la liste des candidats qu'il souhaite entendre puis procédé à leur audition, choisit, en sa qualité de jury, ceux des candidats présentant des mérites, notamment scientifiques, suffisants, et, le cas échéant, les classe selon l'ordre de leurs mérites respectifs ; que, par un avis motivé unique portant sur l'ensemble des candidats, il transmet au conseil académique ou à l'instance qui en exerce les compétences la liste de ceux qu'il a retenus, ce conseil ne pouvant ensuite proposer au ministre chargé de l'enseignement supérieur la nomination d'un candidat non sélectionné par le comité ; que le conseil, siégeant dans une formation restreinte aux enseignants-chercheurs et personnels assimilés de rang au moins égal à celui de l'emploi à pourvoir, prend, au vu de la délibération du comité de sélection, une délibération propre par laquelle il établit sa proposition ; que, dans l'exercice de telles compétences, il incombe au conseil d'apprécier l'adéquation des candidatures au profil du poste et à la stratégie de l'établissement, sans remettre en cause l'appréciation des mérites scientifiques des candidats retenus par le comité de sélection ; <br/>
<br/>
              4. Considérant que les vices ayant, selon le requérant, entaché le compte rendu de la délibération attaquée du 19 mai 2015, par laquelle le conseil dit " de l'article 7 " s'est prononcé sur la candidature de M. B..., sont, en tout état de cause, sans incidence sur la régularité de cette délibération ; qu'est également sans incidence sur la régularité de cette délibération la circonstance que sa notification à M. B...mentionne, à tort, qu'elle peut faire l'objet d'un recours devant le tribunal administratif de Paris ;<br/>
<br/>
              5. Considérant que, pour estimer que la candidature de M. B...n'était pas en adéquation avec la stratégie de l'Institut d'études politiques de Paris et décider, par suite, de ne pas proposer sa candidature au ministre chargé de l'enseignement supérieur, le conseil dit " de l'article 7 " s'est fondé sur ce que, comme l'indique le document d'orientations stratégiques de l'Institut d'études politiques de Paris, l'établissement s'est fixé pour objectif de " contribuer au rayonnement de la recherche française en sciences humaines et sociales en Europe et dans le monde, et s'imposer comme une tête de pont dans les meilleurs réseaux internationaux, en recrutant des professeurs et des chercheurs de réputation internationale... " et a considéré que les travaux et l'expérience de M. B..., compte tenu notamment de ce que sa carrière s'était presqu'exclusivement déroulée en France et qu'il avait principalement publié ses travaux dans des revues et ouvrages d'éditeurs français, ne lui conféraient pas une notoriété internationale de nature à répondre à cet objectif de rayonnement international ; qu'en se fondant sur ce motif, qui ne remet pas en cause l'appréciation portée par le comité de sélection sur les mérites scientifiques du candidat, le conseil, dont la décision est suffisamment motivée, n'a pas excédé les pouvoirs qu'il tient de l'article L. 952-6-1 du code de l'éducation et n'a pas fait une inexacte application de ces dispositions ;<br/>
<br/>
              6.  Considérant, en quatrième lieu, que le détournement de pouvoir allégué n'est pas établi ;<br/>
<br/>
              7.  Considérant qu'il résulte de tout ce qui précède que M. B... n'est pas fondé à demander l'annulation de la délibération du 19 mai 2015 du conseil dit " de l'article 7 " de l'Institut d'études politiques de Paris ;<br/>
<br/>
              8. Considérant que, sans qu'il soit besoin de statuer sur la fin de non-recevoir opposée par l'Institut d'études politiques de Paris, M. B...n'est pas davantage fondé à demander l'annulation, par voie de conséquence, de la décision du 28 août 2015 rejetant son recours gracieux formé contre cette délibération ; que sa requête doit, par suite, être rejetée, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : La requête de M. B... est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. A... B..., à l'Institut d'études politiques de Paris et à la ministre de l'enseignement supérieur, de la recherche et de l'innovation.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">30-02-05-01-06-01-02 ENSEIGNEMENT ET RECHERCHE. QUESTIONS PROPRES AUX DIFFÉRENTES CATÉGORIES D'ENSEIGNEMENT. ENSEIGNEMENT SUPÉRIEUR ET GRANDES ÉCOLES. UNIVERSITÉS. GESTION DES UNIVERSITÉS. GESTION DU PERSONNEL. RECRUTEMENT. - RECRUTEMENT DES ENSEIGNANTS-CHERCHEURS À L'IEP DE PARIS - ORGANE EXERÇANT LES COMPÉTENCES DÉVOLUES AU CONSEIL ACADÉMIQUE (DERNIER ALINÉA DE L'ART. L. 717-1 DU CODE DE L'ÉDUCATION) - CONSEIL PRÉVU PAR L'ARTICLE 7 DU DÉCRET N° 85-497.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">30-02-05-03 ENSEIGNEMENT ET RECHERCHE. QUESTIONS PROPRES AUX DIFFÉRENTES CATÉGORIES D'ENSEIGNEMENT. ENSEIGNEMENT SUPÉRIEUR ET GRANDES ÉCOLES. INSTITUTS D'ÉTUDES POLITIQUES. - INSTITUT D'ÉTUDES POLITIQUES DE PARIS - RECRUTEMENT DES ENSEIGNANTS-CHERCHEURS - ORGANE EXERÇANT LES COMPÉTENCES DÉVOLUES AU CONSEIL ACADÉMIQUE (DERNIER ALINÉA DE L'ART. L. 717-1 DU CODE DE L'ÉDUCATION) - CONSEIL PRÉVU PAR L'ARTICLE 7 DU DÉCRET N° 85-497.
</SCT>
<ANA ID="9A"> 30-02-05-01-06-01-02 Le conseil composé des enseignants-chercheurs prévu par l'article 7 du décret n° 85-497 du 10 mai 1985 est celui qui exerce, au sein de l'Institut d'études politiques (IEP) de Paris, et en vertu de l'article L. 717-1 du code de l'éducation, les compétences dévolues au conseil académique par les articles L. 952-6 à L. 952-9 du même code.</ANA>
<ANA ID="9B"> 30-02-05-03 Le conseil composé des enseignants-chercheurs prévu par l'article 7 du décret n° 85-497 du 10 mai 1985 est celui qui exerce, au sein de l'Institut d'études politiques (IEP) de Paris, et en vertu de l'article L. 717-1 du code de l'éducation, les compétences dévolues au conseil académique par les articles L. 952-6 à L. 952-9 du même code.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
