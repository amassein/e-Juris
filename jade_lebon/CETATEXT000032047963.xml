<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032047963</ID>
<ANCIEN_ID>JG_L_2016_02_000000387931</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/04/79/CETATEXT000032047963.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème / 8ème SSR, 12/02/2016, 387931</TITRE>
<DATE_DEC>2016-02-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>387931</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème / 8ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Pierre Lombard</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2016:387931.20160212</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 12 février 2015 au secrétariat du contentieux du Conseil d'Etat, M. A...B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté du ministre de l'écologie, du développement durable et de l'énergie du 28 août 2014 encadrant la pêche professionnelle du bulot dans le secteur de la baie de Granville, ainsi que la décision implicite par laquelle ce ministre a rejeté sa demande tendant au retrait de l'arrêté litigieux ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - l'accord relatif à la pêche dans la baie de Granville entre la République française et le Royaume-Uni de Grande-Bretagne et d'Irlande du Nord, signé à Saint-Hélier le 4 juillet 2000, publié par le décret n° 2004-75 du 15 janvier 2004 ;   <br/>
              - le code rural et de la pêche maritime ;<br/>
              - le décret n° 2004-75 du 15 janvier 2004 ;<br/>
              - le décret n° 90-94 du 25 janvier 1990 ;<br/>
              - le décret n° 90-95 du 25 janvier 1990 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Pierre Lombard, auditeur,  <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article 2 de l'accord du 4 juillet 2000 entre la République française et le Royaume-Uni de Grande-Bretagne et d'Irlande du Nord relatif à la pêche dans la baie de Granville: " S'il apparaît (...) que la pêche d'une ressource halieutique est susceptible, dans un avenir prévisible, de devenir excessive et de mettre en cause l'existence de cette ressource ou l'équilibre économique de la pêche, la commission administrative mixte prévue à l'article 3 peut soumettre la pêche dans cette partie du Secteur ou la pêche de l'espèce considérée à un système d'autorisation (ci-après dénommé " permis de pêche ") " ; que, par une décision du 26 juin 2014, la commission administrative mixte instituée par l'article 3 du même accord a décidé de subordonner l'exercice de la pêche professionnelle au bulot dans le secteur couvert par le régime de gestion des ressources halieutiques issu de cet accord à la détention d'un permis de pêche ; que, pour rendre l'obligation ainsi édictée opposable en droit interne aux capitaines de navires professionnels de pêche, le ministre de l'écologie, du développement durable et de l'énergie, chargé des pêches maritimes, a pris le 28 août 2014 un arrêté encadrant la pêche professionnelle du bulot dans le secteur de la baie de Granville ; que M. B...demande l'annulation de cet arrêté ainsi que de la décision implicite par laquelle ce ministre a refusé de faire droit à sa demande tendant à son retrait ; <br/>
<br/>
              Sur le moyen tiré de l'incompétence du ministre chargé des pêches maritimes pour réglementer la pêche par les navires français dans les eaux territoriales jersiaises :<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 911-3 du code rural et de la pêche maritime, les dispositions du livre " Pêche maritime et aquaculture marine " de ce code " s'appliquent, en conformité avec les dispositions prévues par le traité sur le fonctionnement de l'Union européenne et dans le respect des engagements internationaux de la France, dans les zones sous juridiction ou sous souveraineté française, ainsi qu'en tout lieu aux ressortissants français et aux navires battant pavillon français dans le respect des accords internationaux et de la souveraineté des pays tiers. " ; qu'en vertu de l'article L. 921-1 du même code, l'exercice de la pêche maritime embarquée à titre professionnel peut être soumis à la délivrance d'autorisations ; qu'en vertu de ces dispositions, combinées aux décrets du 25 janvier 1990 pris pour l'application de l'article 3 du décret du 9 janvier 1852 modifié fixant les conditions générales d'exercice de la pêche maritime dans les eaux soumises à la réglementation communautaire de conservation et de gestion, dont les dispositions ont été codifiées au code rural et de la pêche maritime, le ministre chargé des pêches maritimes est compétent pour réglementer l'exercice de la pêche professionnelle notamment vis-à-vis des navires battant pavillon français pêchant dans des eaux placées sous la souveraineté ou la juridiction d'un Etat étranger et qui sont couvertes par un accord international conclu entre la France et cet Etat ; <br/>
<br/>
              3. Considérant que la pêche professionnelle par les navires battant pavillon français dans les eaux territoriales jersiaises du secteur de la baie de Granville, et ainsi hors eaux territoriales françaises, doit être exercée conformément aux stipulations de l'accord entre la France et le Royaume-Uni du 4 juillet 2000 ; qu'aux termes de l'article 2 de cet accord : " Les autorités jersiaises et françaises veillent chacune à ce que leur droit interne prévoie qu'un permis d'accès et un permis de pêche (si un tel permis a été délivré) soient présents à tout moment à bord d'un navire dans le Secteur et puissent être produits, aux fins d'inspection, aux autorités habilitées des Parties " ; qu'ainsi qu'il a été dit au point 1 la commission administrative mixte a décidé le 26 juin 2014 de soumettre la pêche professionnelle du bulot à la détention d'une licence de pêche, dans l'ensemble des espaces maritimes couverts par cet accord ; que si, aux termes de l'article 1er de l'arrêté attaqué " l'exercice de la pêche maritime professionnelle du bulot dans le secteur de la baie de Granville défini à l'article 1er de l'accord du 4 juillet 2000 (...) est subordonné à la détention d'une licence de pêche pour le bulot délivrée par l'une des autorités compétentes des parties à l'accord ", le ministre chargé des pêches maritimes s'est sur ce point borné à mettre en oeuvre, en droit interne, la décision de la commission administrative mixte du 26 juin 2014 ; que dès lors ce même ministre tenait de l'article L. 911-3 du code rural et de la pêche maritime le pouvoir de réglementer la pêche professionnelle du bulot par les navires battant pavillon français dans l'ensemble du secteur de la baie de Granville couvert par l'accord du 4 juillet 2000, y compris les eaux territoriales jersiaises ; que, par suite, le moyen tiré de ce que l'arrêté attaqué aurait été édicté par une autorité géographiquement incompétente doit être écarté ; <br/>
<br/>
              Sur l'exception tirée de l'illégalité de la décision du 26 juin 2014 de la commission administrative mixte instituée par l'accord entre le France et le Royaume-Uni du 4 juillet 2000 :<br/>
<br/>
              4. Considérant qu'à l'appui de ses conclusions dirigées contre l'arrêté litigieux, pris ainsi qu'il a été dit au point 3 aux fins d'introduire en droit interne la décision de réglementer l'exercice de la pêche professionnelle du bulot dans le secteur de la baie de Granville, prise le 26 juin 2014 par la commission administrative mixte, M. B...excipe de l'illégalité de cette décision ; que toutefois, ainsi qu'il a été dit au point 1, cette commission constitue une instance intergouvernementale créée par un accord international dont les décisions, qui ne sont pas détachables de l'exercice des relations diplomatiques, ne sont pas soumises au contrôle du juge administratif français ; que l'exception d'illégalité ainsi soulevée par M. B... doit par suite être rejetée comme portée devant une juridiction incompétente pour en connaître ;<br/>
<br/>
              Sur les autres moyens de la requête :<br/>
<br/>
              5. Considérant qu'aux termes de l'article 4 de l'accord entre la France et le Royaume-Uni du 4 juillet 2000 : " (...) les Parties adoptent d'un commun accord, en tant que de besoin, les règlements destinés à régir les activités de pêche dans le Secteur, conformément au principe de précaution mais tout en tenant compte de facteurs socio-économiques. (...) il appartient aux Parties d'adopter les mesures appropriées pour les mettre en oeuvre dans leur droit interne " ; qu'il résulte de ces stipulations et de ce qui est dit au point 1 qu'en édictant l'arrêté litigieux le ministre chargé des pêches maritimes s'est borné à tirer les conséquences de la décision de la commission administrative mixte du 26 juin 2014, ainsi qu'il était tenu de le faire ; que, par suite, le moyen tiré de ce que cet arrêté aurait été pris, non dans un but de préservation de la ressource au sens de l'article L. 911-2 du code rural et de la pêche maritime, mais exclusivement pour agir sur le cours auquel le bulot est commercialisé et serait entaché de détournement de pouvoir, ne peut qu'être écarté ;<br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que M. B...n'est pas fondé à demander l'annulation de l'arrêté qu'il attaque ; que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que la somme demandée par M. B...soit mise à la charge de l'Etat, qui n'est pas la partie perdante dans la présente instance ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
<br/>
Article 1er : La requête de M.B...  est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. A...B...et à la ministre de l'écologie, du développement durable et de l'énergie.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-01-03 ACTES LÉGISLATIFS ET ADMINISTRATIFS. DIFFÉRENTES CATÉGORIES D'ACTES. ACTES DE GOUVERNEMENT. - EXCEPTION D'ILLÉGALITÉ DIRIGÉE CONTRE UN ACTE DE GOUVERNEMENT - INCOMPÉTENCE DU JUGE ADMINISTRATIF.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-07-01-04-04 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. QUESTIONS GÉNÉRALES. MOYENS. EXCEPTION D'ILLÉGALITÉ. - EXCEPTION D'ILLÉGALITÉ DIRIGÉE CONTRE UN ACTE DE GOUVERNEMENT - INCOMPÉTENCE DU JUGE ADMINISTRATIF.
</SCT>
<ANA ID="9A"> 01-01-03 Le juge administratif n'est pas compétent pour connaître, par voie d'exception, de la légalité d'une décision prise par une instance intergouvernementale qui n'est pas détachable de l'exercice des relations diplomatiques et constitue par suite un acte de gouvernement.</ANA>
<ANA ID="9B"> 54-07-01-04-04 Le juge administratif n'est pas compétent pour connaître, par voie d'exception, de la légalité d'une décision prise par une instance intergouvernementale qui n'est pas détachable de l'exercice des relations diplomatiques et constitue par suite un acte de gouvernement.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
