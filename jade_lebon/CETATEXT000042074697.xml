<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042074697</ID>
<ANCIEN_ID>JG_L_2020_07_000000424289</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/07/46/CETATEXT000042074697.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 4ème chambres réunies, 01/07/2020, 424289</TITRE>
<DATE_DEC>2020-07-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>424289</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 4ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELAMARRE, JEHANNIN ; SCP GATINEAU, FATTACCINI, REBEYROL ; SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Arnaud Skzryerbak</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie Sirinelli</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:424289.20200701</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A... B... a demandé au tribunal administratif de Grenoble :<br/>
              - d'annuler la décision du 3 décembre 2016 de la caisse d'allocations familiales de l'Isère de récupérer un indu de 228,67 euros au titre de l'aide exceptionnelle de fin d'année pour 2015 et de la décharger de cette somme ;<br/>
              - d'annuler la décision du 19 décembre 2016 du président du conseil départemental de l'Isère de récupérer un indu de 9 934,16 euros au titre du revenu de solidarité active, de la décharger de cette somme et d'enjoindre au département de lui rembourser les sommes retenues sur ses prestations en vue du paiement de cette somme ;d'annuler la décision du 31 mars 2017 rejetant son recours contre la décision de récupération d'un indu d'aide personnalisée au logement, de la décharger du paiement du solde de la somme réclamée et d'enjoindre au département de lui rembourser les sommes retenues sur ses prestations ;<br/>
              - d'annuler la décision implicite rejetant son recours formé le 6 janvier 2017 contre les retenues effectuées par la caisse d'allocations familiales de l'Isère sur ses prestations pour le recouvrement d'indus de revenu de solidarité active et d'aide personnalisée au logement et d'enjoindre au département de l'Isère et à cette caisse de lui rembourser les sommes ainsi retenues.<br/>
<br/>
              Par un jugement n°s 1701658, 1701662, 1701669, 1703061 du 25 juillet 2018, le tribunal administratif de Grenoble a annulé la décision du 3 décembre 2016, déchargé Mme B... de l'indu d'aide exceptionnelle de fin d'année au titre de l'année 2015, sauf à ce que l'autorité administrative reprenne régulièrement une nouvelle décision de récupération de cet indu dans un délai de deux mois suivant la notification de ce jugement, et rejeté le surplus des conclusions des demandes de Mme B....<br/>
              Par un pourvoi et par un nouveau mémoire, enregistrés les 18 septembre 2018 et 14 janvier 2019 au secrétariat du contentieux du Conseil d'Etat, Mme A... B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses demandes ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat, de la caisse d'allocations familiales de l'Isère et du département de l'Isère la somme de 3 000 euros, à verser à la SCP Delamarre, Jehannin, son avocat, en application des articles 37 de la loi du 10 juillet 1991 et L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son article 62 ; <br/>
              - le code l'action sociale et des familles ; <br/>
              - le code de la sécurité sociale ; <br/>
              - la loi n° 2007-1786 du 19 décembre 2007 ;<br/>
              - l'arrêté du 5 mai 2014 fixant les conditions d'agrément des agents et des praticiens-conseils chargés du contrôle de l'application des législations de sécurité sociale ;<br/>
              - la décision n° 2019-789 QPC du 14 juin 2019 statuant sur la question prioritaire de constitutionnalité soulevée par Mme B... ; <br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Arnaud Skzryerbak, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Marie Sirinelli, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Delamarre, Jehannin, avocat de Mme B..., à la SCP Piwnica, Molinié, avocat du département de l'Isère et à la SCP Gatineau, Fattaccini, Rebeyrol, avocat de la caisse d'allocations familiales de l'Isère ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond qu'à la suite de déclarations divergentes sur sa situation de famille dans ses demandes de revenu de solidarité active et d'aides au logement, Mme B... a fait l'objet d'un contrôle de sa situation mené par la caisse d'allocations familiales de l'Isère. Le 21 novembre 2016, cette caisse a décidé de récupérer un indu de prestations sociales pour les mois de février 2015 à novembre 2016 au motif que Mme B... avait inexactement déclaré être séparée de son mari. Le président du conseil départemental de l'Isère a, sur recours de l'intéressée, confirmé la récupération de l'indu de revenu de solidarité active le 19 décembre 2016 et la caisse d'allocations familiales a rejeté le 31 mars 2017 le recours gracieux de Mme B... portant sur l'indu d'aide personnalisée au logement. Par un jugement du 25 juillet 2018 contre lequel Mme B... se pourvoit en cassation, le tribunal administratif de Grenoble a, notamment, rejeté les conclusions de cette dernière contre les décisions des 19 décembre 2016 et 31 mars 2017 et contre la décision implicite rejetant le recours par lequel elle contestait les retenues opérées sur les prestations versées par la caisse d'allocations familiales de l'Isère.<br/>
<br/>
              Sur le droit de communication :<br/>
<br/>
              2. Aux termes de l'article L. 262-16 du code de l'action sociale et des familles : " Le service du revenu de solidarité active est assuré, dans chaque département, par les caisses d'allocations familiales et, pour leurs ressortissants, par les caisses de mutualité sociale agricole ". Aux termes de l'article L. 262-40 de ce code : " (...) Les organismes chargés de son versement réalisent les contrôles relatifs au revenu de solidarité active selon les règles, procédures et moyens d'investigation applicables aux prestations de sécurité sociale. (...) ". A ce titre, l'article L. 114-19 du code de la sécurité sociale prévoit que le droit de communication permet à certains agents des organismes de sécurité sociale d'obtenir, auprès de personnes publiques et privées que l'article L. 114-20 du même code désigne par renvoi au livre des procédures fiscales, sans que le secret professionnel ne s'y oppose, les documents et informations nécessaires à l'exercice des missions de contrôle ou de recouvrement de prestations indûment versées qu'il définit. L'article L. 114-21 du code de la sécurité sociale dispose que l'organisme ayant usé de ce droit est tenu d'informer la personne à l'encontre de laquelle est prise la décision de supprimer le service d'une prestation ou de mettre des sommes en recouvrement " de la teneur et de l'origine des informations et documents obtenus auprès de tiers sur lesquels il s'est fondé pour prendre cette décision " et qu'il communique, avant la mise en recouvrement ou la suppression du service de la prestation, une copie de ces documents à la personne qui en fait la demande.<br/>
<br/>
              3. En premier lieu, saisi le 27 mars 2019 par le Conseil d'Etat de la question, posée par Mme B..., de la conformité aux droits et libertés garantis par la Constitution des articles L. 114-19, L. 114-20 et L. 114-21 du code de la sécurité sociale, le Conseil constitutionnel a, par sa décision n° 2019-789 QPC du 14 juin 2019, jugé que la question ne portait que sur les articles L. 114-20 et L. 114-21 de ce code dans leur rédaction, applicable au litige, résultant de la loi du 19 décembre 2007 de financement de la sécurité sociale pour 2008. Il a, d'une part, déclaré conforme à la Constitution l'article L. 114-21 du code de la sécurité sociale et, d'autre part, déclaré contraire à la Constitution les dispositions de l'article L. 114-20 de ce code, mais a jugé que les mesures prises sur leur fondement ne pouvaient être contestées sur le fondement de cette inconstitutionnalité. Il suit de là que Mme B... n'est pas fondée à se prévaloir de l'inconstitutionnalité des dispositions des articles L. 114-19, L. 114-20 et L. 114-21 du code de la sécurité sociale pour demander l'annulation du jugement qu'elle attaque.<br/>
<br/>
              4. En deuxième lieu, il résulte des dispositions de l'article L. 262-16 du code de l'action sociale et des familles, rappelées au point 2, que les caisses d'allocations familiales et les caisses de mutualité sociale agricole, chargées du service du revenu de solidarité active, réalisent les contrôles relatifs à cette prestation d'aide sociale selon les règles, procédures et moyens d'investigation applicables aux prestations de sécurité sociale, au nombre desquels figurent le droit de communication instauré par l'article L. 114-19 du code de la sécurité sociale au bénéfice des organismes de sécurité sociale pour contrôler la sincérité et l'exactitude des déclarations souscrites ou l'authenticité des pièces produites en vue de l'attribution et du paiement des prestations qu'ils servent, ainsi que les garanties procédurales s'attachant, en vertu de l'article L. 114-21 du même code, à l'exercice de ce droit par un organisme de sécurité sociale. Ces garanties ne trouvent cependant à s'appliquer que si, le droit de communication ayant été mis en oeuvre par l'organisme de contrôle, ce dernier s'est fondé, pour supprimer le service d'une prestation ou mettre des sommes en recouvrement, sur des informations ou documents obtenus auprès de tiers dans le cadre de ce droit de communication.<br/>
<br/>
              5. En l'espèce, pour écarter le moyen tiré de la méconnaissance de l'article L. 114-21 du code de la sécurité sociale, le tribunal administratif de Grenoble s'est fondé sur la seule circonstance, insuffisante au regard des exigences de ces dispositions, qu'il ressortait des mentions du rapport de contrôle établi le 21 avril 2017 que Mme B... avait été informée de son droit à avoir communication des documents obtenus de tiers si le contrôle aboutissait à un recouvrement ou à la suppression de la prestation. Toutefois, il ressort des pièces du dossier soumis au juge du fond que la caisse d'allocations familiales ne s'est pas fondée sur des informations ou documents obtenus dans le cadre du droit de communication pour réclamer à Mme B... le paiement des indus litigieux. Par suite, le moyen tiré par Mme B... de la méconnaissance de l'article L. 114-21 du code de la sécurité sociale était inopérant. Il convient de l'écarter pour ce motif, qui doit être substitué au motif retenu par le juge du fond, et Mme B... ne peut utilement soutenir que le tribunal aurait, dans cette mesure, entaché son jugement d'insuffisance de motivation et d'erreur de droit. <br/>
<br/>
              Sur l'habilitation, l'agrément et l'assermentation de l'agent chargé du contrôle :<br/>
<br/>
              6. Selon le premier alinéa de l'article L. 114-10 du code de la sécurité sociale, dans sa rédaction applicable au litige : " Les directeurs des organismes chargés de la gestion d'un régime obligatoire de sécurité sociale confient à des agents chargés du contrôle, assermentés et agréés dans des conditions définies par arrêté du ministre chargé de la sécurité sociale ou par arrêté du ministre chargé de l'agriculture, le soin de procéder à toutes vérifications ou enquêtes administratives concernant l'attribution des prestations (...) Ces agents ont qualité pour dresser des procès-verbaux faisant foi jusqu'à preuve du contraire ". Les conditions d'agrément des agents chargés du contrôle de l'application des législations de sécurité sociale ont été définies, en dernier lieu, par un arrêté du 5 mai 2014 de la ministre des affaires sociales et de la santé.<br/>
<br/>
              7. Il résulte de l'ensemble de ces dispositions que tant l'absence d'agrément que l'absence d'assermentation des agents de droit privé désignés par les caisses d'allocations familiales pour conduire des contrôles sur les déclarations des bénéficiaires du revenu de solidarité active sont de nature à affecter la validité des constatations des procès-verbaux qu'ils établissent à l'issue de ces contrôles et à faire ainsi obstacle à ce qu'elles constituent le fondement d'une décision déterminant pour l'avenir les droits de la personne contrôlée ou remettant en cause des paiements déjà effectués à son profit en ordonnant la récupération d'un indu.<br/>
<br/>
              8. Toutefois, il ressort en l'espèce des pièces du dossier soumis au juge du fond que le rapport de contrôle daté du 21 avril 2017 a été établi à l'occasion du contrôle effectué au domicile de Mme B... le 8 mars 2017, soit postérieurement à la décision de récupération d'indu litigieuse prise le 21 novembre 2016 par la caisse d'allocations familiales, confirmée le 19 décembre 2016 par le président du conseil départemental. Ainsi, les constatations de cet agent n'ayant en tout état de cause pu constituer le fondement de la décision ordonnant la récupération de l'indu de revenu de solidarité active litigieux, le moyen tiré par Mme B... de son absence d'habilitation, d'agrément ou d'assermentation était inopérant. Il convient de l'écarter pour ce motif, qui doit être substitué à celui retenu par le tribunal, et Mme B... ne peut utilement soutenir que le tribunal aurait méconnu son office ou aurait commis une erreur de droit sur ce point.<br/>
<br/>
              Sur l'examen du recours administratif préalable dirigé contre la récupération de l'indu de revenu de solidarité active :<br/>
<br/>
              9. En vertu du 1° du I de l'article L. 262-25 du code de l'action sociale et des familles, une convention, conclue entre le département et chacun des organismes payeurs mentionnés à l'article L. 262-16, précise en particulier les conditions dans lesquelles le revenu de solidarité active est servi et contrôlé. Le premier alinéa de l'article L. 262-47 du même code prévoit que : " Toute réclamation dirigée contre une décision relative au revenu de solidarité active fait l'objet, préalablement à l'exercice d'un recours contentieux, d'un recours administratif auprès du président du conseil départemental. Ce recours est, dans les conditions et limites prévues par la convention mentionnée à l'article L. 262-25, soumis pour avis à la commission de recours amiable qui connaît des réclamations relevant de l'article L. 142-1 du code de la sécurité sociale. (...) ". Enfin, aux termes du premier alinéa de l'article R. 262-89 du même code : " Sauf lorsque la convention mentionnée à l'article L. 262-25 en dispose autrement, ce recours est adressé par le président du conseil départemental pour avis à la commission de recours amiable mentionnée à l'article R. 142-1 du code de la sécurité sociale ". Il résulte de ces dispositions que la convention conclue entre le département et la caisse d'allocations familiales ne peut légalement prévoir qu'aucun recours administratif préalable dirigé contre une décision relative au revenu de solidarité active n'est soumis pour avis à la commission de recours amiable. <br/>
<br/>
              10. Toutefois, l'illégalité d'un acte administratif, qu'il soit ou non réglementaire, ne peut être utilement invoquée par voie d'exception à l'appui de conclusions dirigées contre une décision administrative ultérieure que si cette dernière a été prise pour son application ou y trouve sa base légale. En jugeant que la décision par laquelle le président du conseil départemental confirme la récupération par la caisse d'allocations familiales d'un indu de revenu de solidarité active ne constitue pas un acte pris pour l'application des dispositions de la convention conclue entre cette caisse et le département en application de l'article L. 262-25 du code de l'action sociale et des familles relatives à la saisine de la commission de recours amiable, lesquelles ne constituent pas davantage sa base légale, le tribunal n'a pas commis d'erreur de droit. Il n'a pas davantage commis d'erreur de droit en en déduisant que Mme B... ne pouvait utilement invoquer, par voie d'exception, l'illégalité de la convention conclue entre le département et la caisse d'allocations familiales de l'Isère, alors même que celle-ci ne pouvait légalement exclure la consultation de la commission de recours amiable sur toute réclamation dirigée contre une décision relative au revenu de solidarité active que ce soit.<br/>
<br/>
              Sur l'examen du recours administratif préalable dirigé contre la récupération de l'indu d'aide personnalisée au logement :<br/>
<br/>
              11. En jugeant qu'il ne résultait pas de l'instruction que le directeur de la caisse d'allocations familiales de l'Isère se serait estimé lié par l'avis de la commission de recours amiable et n'aurait pas procédé lui-même à l'examen de la situation de Mme B... avant de rejeter le recours par lequel celle-ci contestait l'indu d'aide personnalisée au logement dont le remboursement lui était réclamé, le tribunal n'a pas commis d'erreur de droit et a porté sur les pièces du dossier une appréciation souveraine exempte de dénaturation.<br/>
<br/>
              12. Il résulte de tout ce qui précède que Mme B... n'est pas fondée à demander l'annulation du jugement qu'elle attaque.<br/>
<br/>
              13. Les dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 font obstacle à ce qu'il soit fait droit aux conclusions présentées à ce titre par l'avocat de Mme B.... Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par le département et la caisse d'allocations familiales de l'Isère au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le pourvoi de Mme B... est rejeté.<br/>
Article 2 : Les conclusions présentées par le département de l'Isère et par la caisse d'allocations familiales de l'Isère au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à Mme A... B..., au département de l'Isère et à la caisse d'allocations familiales de l'Isère.<br/>
Copie en sera adressée à la ministre de la cohésion des territoires et des relations avec les collectivités territoriales.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">04-02-06 AIDE SOCIALE. DIFFÉRENTES FORMES D'AIDE SOCIALE. REVENU MINIMUM D'INSERTION (RMI). - RSA - CONVENTION ENTRE LE DÉPARTEMENT ET LA CAF - CONVENTION PRÉVOYANT QU'AUCUN RECOURS ADMINISTRATIF PRÉALABLE N'EST SOUMIS POUR AVIS À LA CRA (1ER AL. DE L'ART. L. 262-47 DU CASF) - ILLÉGALITÉ.
</SCT>
<ANA ID="9A"> 04-02-06 Il résulte des articles L. 262-25, L. 262-47 et R. 262-89 du code de l'action sociale et des familles (CASF) que la convention conclue entre le département et la caisse d'allocations familiales (CAF) ne peut légalement prévoir qu'aucun recours administratif préalable dirigé contre une décision relative au revenu de solidarité active (RSA) n'est soumis pour avis à la commission de recours amiable (CRA).</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
