<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034514948</ID>
<ANCIEN_ID>JG_L_2017_04_000000395867</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/51/49/CETATEXT000034514948.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 1ère chambres réunies, 28/04/2017, 395867</TITRE>
<DATE_DEC>2017-04-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>395867</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN ; SCP THOUIN-PALAT, BOUCARD</AVOCATS>
<RAPPORTEUR>Mme Laurence Franceschini</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:395867.20170428</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              1° Mme G...B..., Mme E...B..., Mme D...B..., Mme H...F...épouseB..., M. C...B...et M. A...B...ont demandé au tribunal administratif de Pau d'annuler pour excès de pouvoir le permis de construire délivré à la société ICB Investimmo Côte Basque le 2 décembre 2013 par la commune de Bayonne et la décision de rejet du recours gracieux du 6 mars 2014 ainsi que le permis de construire modificatif délivré le 8 avril 2014. Par un jugement n° 1400982 du 27 octobre 2015, le tribunal administratif de Pau a annulé le permis de construire délivré le 2 décembre 2013 ainsi que le rejet du recours gracieux contre ce permis et rejeté le surplus des conclusions.<br/>
<br/>
              Sous le n° 395867, par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 4 janvier et 4 avril 2016 au secrétariat du contentieux du Conseil d'Etat, la commune de Bayonne demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de Mme G...B...et autres.<br/>
<br/>
<br/>
              Sous le n° 396238, par ce pourvoi et un mémoire en réplique, enregistrés le 27 juillet 2016 et le 6 février 2017, au secrétariat du contentieux du Conseil d'Etat, la société ICB Investimmo Côte Basque demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement n° 1400982 du 27 octobre 2015 par lequel le tribunal administratif de Pau, sur la demande de Mme B...et autres, a annulé le permis de construire délivré le 2 décembre 2013 par la commune de Bayonne pour la construction de quatre bâtiments de quatre-vingt-seize logements ainsi que la décision de rejet du recours gracieux du 6 mars 2014 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de Mme B...et autres ;<br/>
<br/>
              3°) de mettre à la charge de Mme B...et autres la somme de 1 500 euros au titre de l'article L. 761- 1 du code de justice administrative. <br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Laurence Franceschini, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Thouin-Palat, Boucard, avocat de la commune de Bayonne, à la SCP Waquet, Farge, Hazan, avocat de MmeB..., et autres et à Me Occhipinti, avocat de la société ICB Investimmo Côte Basque.<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que les pourvois de la commune de Bayonne et de la société Investimmo Côte Basque sont dirigés contre le même jugement ; qu'il y a lieu de les joindre pour statuer par une seule décision ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces des dossiers soumis aux juges du fond qu'un permis de construire et un  permis de construire modificatif ont été délivrés par le maire de la commune de Bayonne à la société ICB Investimmo Côte Basque le 2 décembre 2013 et le 8 avril 2014 ; que Mme B...et autres ont saisi le tribunal administratif de Pau d'une demande d'annulation pour excès de pouvoir de ces permis ; qu'un autre permis modificatif a été délivré à cette société le 16 octobre 2015, postérieurement à l'audience publique tenue le 13 octobre 2015 par le tribunal administratif ; que, par un jugement du 27 octobre 2015, contre lequel la société ICB Investimmo Côte Basque et la commune de Bayonne se pourvoient en cassation, le tribunal administratif de Pau a annulé le permis de construire délivré le 2 décembre 2013 ; <br/>
<br/>
              3. Considérant que, devant les juridictions administratives et dans l'intérêt d'une bonne justice, le juge a toujours la faculté de rouvrir l'instruction, qu'il dirige, lorsqu'il est saisi d'une production postérieure à la clôture de celle-ci ; qu'il lui appartient, dans tous les cas, de prendre connaissance de cette production avant de rendre sa décision et de la viser ; que, s'il décide d'en tenir compte, il rouvre l'instruction et soumet au débat contradictoire les éléments contenus dans cette production qu'il doit, en outre, analyser ; que, dans le cas particulier où cette production contient l'exposé d'une circonstance de fait ou d'un élément de droit dont la partie qui l'invoque n'était pas en mesure de faire état avant la clôture de l'instruction et qui est susceptible d'exercer une influence sur le jugement de l'affaire, le juge doit en tenir compte, à peine d'irrégularité de sa décision ; que, lorsque le juge est saisi d'un recours dirigé contre un permis de construire et qu'est produit devant lui, postérieurement à la clôture de l'instruction, un permis modificatif qui a pour objet de modifier des éléments contestés du permis attaqué et qui ne pouvait être produit avant la clôture de l'instruction, il lui appartient, sauf si ce permis doit en réalité être regardé comme un nouveau permis, d'en tenir compte et de rouvrir en conséquence l'instruction ;<br/>
<br/>
              4. Considérant, en premier lieu, que, pour juger que la délivrance du permis de construire modificatif du 16 octobre 2015, produit avec une note en délibéré, ne constituait pas une circonstance nouvelle l'obligeant à rouvrir l'instruction, le tribunal administratif de Pau a d'abord estimé qu'une telle réouverture serait de nature à porter atteinte à la loyauté du procès et qu'en particulier, les pétitionnaires et les autorités compétentes pour délivrer les permis de construire ne sauraient trouver, dans la possibilité, pour le juge, de rouvrir une instruction qui a fait l'objet d'une clôture, un motif pour adapter les permis contestés, notamment en tenant compte des conclusions présentées au cours de l'audience publique par le rapporteur public ; qu'il a ainsi commis une erreur de droit, la circonstance liée au déroulement du procès qu'il a relevée étant sans incidence sur la possibilité de régulariser des vices affectant un permis initial par un permis modificatif ; <br/>
<br/>
              5. Considérant, en second lieu, qu'il résulte de ce qui a été dit au point 3 ci-dessus que le tribunal administratif de Pau a commis une erreur de droit en jugeant, après avoir relevé que les illégalités qui entachaient le permis en litige ne pouvaient, eu égard à leur nature et à leur ampleur, être régularisées par un permis de construire modificatif, que la délivrance du permis modificatif du 16 octobre 2015 ne constituait pas une circonstance nouvelle l'obligeant à rouvrir l'instruction ; <br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède, sans qu'il soit besoin d'examiner les autres moyens des pourvois, que le jugement attaqué doit être annulé ; <br/>
<br/>
              7. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de Mme B...et autres la somme totale de 1 500 euros à verser à la société ICB Investimmo Côte Basque au titre des dispositions de l'article L. 761-1 du code de justice administrative ; que ces même dispositions font obstacle à ce qu'une somme soit mise à la charge de la commune de Bayonne et de la société ICB Investimmo Côte Basque qui ne sont pas, dans la présente instance, les parties perdantes ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement n° 1400982 du 27 octobre 2015 du tribunal administratif de Pau est annulé.<br/>
Article 2 : Les affaires sont renvoyées devant le tribunal administratif de Pau.<br/>
Article 3 : Mme G...B...et autres verseront à la société ICB Investimmo Côte Basque une somme totale de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 4. : Les conclusions de Mme B...et autres présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 5 : La présente décision sera notifiée à la commune de Bayonne, à la société ICB Investimmo Côte Basque et à Mme G...B..., premier défendeur dénommé. Les autres défendeurs seront informés de la présente décision par la SCP Waquet, Farge, Hazan, avocat au Conseil d'Etat et à la Cour de cassation, qui les représente devant le Conseil d'Etat.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-04-01-05 PROCÉDURE. INSTRUCTION. POUVOIRS GÉNÉRAUX D'INSTRUCTION DU JUGE. CLÔTURE DE L'INSTRUCTION. - PRODUCTIONS POSTÉRIEURES À LA CLÔTURE DE L'INSTRUCTION [RJ1] - PERMIS MODIFICATIF QUI A POUR OBJET DE MODIFIER LES ÉLÉMENTS CONTESTÉS DU PERMIS ATTAQUÉ ET QUI NE POUVAIT ÊTRE PRODUIT AVANT LA CLÔTURE DE L'INSTRUCTION - OBLIGATION DE RÉOUVERTURE DE L'INSTRUCTION - EXISTENCE, SAUF SI CE PERMIS DOIT ÊTRE REGARDÉ COMME UN NOUVEAU PERMIS [RJ2].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">68-06-04 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. POUVOIRS DU JUGE. - PRODUCTIONS POSTÉRIEURES À LA CLÔTURE DE L'INSTRUCTION [RJ1] - PERMIS MODIFICATIF QUI A POUR OBJET DE MODIFIER LES ÉLÉMENTS CONTESTÉS DU PERMIS ATTAQUÉ ET QUI NE POUVAIT ÊTRE PRODUIT AVANT LA CLÔTURE DE L'INSTRUCTION - OBLIGATION DE RÉOUVERTURE DE L'INSTRUCTION - EXISTENCE, SAUF SI CE PERMIS DOIT ÊTRE REGARDÉ COMME UN NOUVEAU PERMIS [RJ2].
</SCT>
<ANA ID="9A"> 54-04-01-05 Lorsque le juge est saisi d'un recours dirigé contre un permis de construire et qu'est produit devant lui, postérieurement à la clôture de l'instruction, un permis modificatif qui a pour objet de modifier des éléments contestés du permis attaqué et qui ne pouvait être produit avant la clôture de l'instruction, il lui appartient, sauf si ce permis doit en réalité être regardé comme un nouveau permis, d'en tenir compte et de rouvrir en conséquence l'instruction.</ANA>
<ANA ID="9B"> 68-06-04 Lorsque le juge est saisi d'un recours dirigé contre un permis de construire et qu'est produit devant lui, postérieurement à la clôture de l'instruction, un permis modificatif qui a pour objet de modifier des éléments contestés du permis attaqué et qui ne pouvait être produit avant la clôture de l'instruction, il lui appartient, sauf si ce permis doit en réalité être regardé comme un nouveau permis, d'en tenir compte et de rouvrir en conséquence l'instruction.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Section, 5 décembre 2014, M.,, n° 340943, p. 369., ,[RJ2] Rappr., s'agissant de la production après la clôture de l'instruction d'un permis modificatif intervenu en régularisation du permis initial, CE, 30 mars 2015, Société Eole-Res et ministre de l'égalité des territoires et du logement, n°s 369431, 369637, T. pp. 811-927.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
