<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027328305</ID>
<ANCIEN_ID>JG_L_2013_04_000000342372</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/32/83/CETATEXT000027328305.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème SSR, 17/04/2013, 342372</TITRE>
<DATE_DEC>2013-04-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>342372</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>HAAS</AVOCATS>
<RAPPORTEUR>M. Tanneguy Larzul</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Delphine Hedary</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2013:342372.20130417</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 10 août 2010 au secrétariat du contentieux du Conseil d'Etat, présenté pour la société La Poste, dont le siège est 44, boulevard de Vaugirard à Paris (75757 Cedex 15) ; La Poste demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement n° 0813010 du 27 mai 2010 par lequel le tribunal administratif de Montreuil a annulé sa décision rejetant implicitement la demande de M. B... A...tendant à la communication des liste d'emplois repères du groupe A et de leurs plages de rémunération au 1er juillet 2007 et au 1er juillet 2008 et lui a enjoint de procéder à cette communication ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande présentée par M. A...devant le tribunal administratif de Montreuil ;<br/>
<br/>
              3°) de mettre à la charge de M. A...la somme de 2 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi n° 78-753 du 17 juillet 1978 ;<br/>
<br/>
              Vu la loi n° 90-568 du 2 juillet 1990 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Tanneguy Larzul, Conseiller d'Etat,  <br/>
<br/>
              - les observations de Me Haas, avocat de la société La Poste,<br/>
<br/>
              - les conclusions de Mme Delphine Hedary, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à Me Haas, avocat de la société La Poste ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes du deuxième alinéa de l'article 1er de la loi du 17 juillet 1978, dans sa rédaction applicable à la date de la décision attaquée : " Sont considérés comme documents administratifs, au sens des chapitres Ier, III et IV du présent titre, quels que soient leur date, leur lieu de conservation, leur forme et leur support, les documents élaborés ou détenus par l'Etat, les collectivités territoriales ainsi que par les autres personnes de droit public ou les personnes de droit privé chargées de la gestion d'un service public, dans le cadre de leur mission de service public. Constituent de tels documents notamment les dossiers, rapports, études, comptes rendus, procès-verbaux, statistiques, directives, instructions, circulaires, notes et réponses ministérielles, correspondances, avis, prévisions et décisions (...) " ; que, s'agissant des documents détenus par un organisme privé chargé d'une mission de service public qui exerce également une activité privée, seuls ceux qui présentent un lien suffisamment direct avec sa mission de service public peuvent être regardés comme des documents administratifs, communicables sous réserve des dispositions de l'article 6 et notamment des secrets protégés par la loi ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article 2 de la loi du 2 juillet 1990, dans sa rédaction applicable à la date de la décision attaquée : " La Poste et ses filiales constituent un groupe public qui remplit, dans les conditions définies par les textes qui régissent chacun de ses domaines d'activité, des missions d'intérêt général et exerce des activités concurrentielles / La Poste assure, dans les relations intérieures et internationales, le service public des envois postaux, qui comprend le service universel postal et notamment le service public du transport et de la distribution de la presse bénéficiant du régime spécifique prévu par le code des postes et des communications électroniques. Elle assure également, dans le respect des règles de concurrence, tout autre service de collecte, de tri, de transport et de distribution d'envois postaux, de courrier sous toutes ses formes, d'objets et de marchandises. (...) " ; <br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que La Poste a élaboré de nouveaux instruments de gestion de carrière pour l'ensemble de ses personnels d'encadrement supérieur ; qu'à cet effet, elle a établi des " listes d'emplois repères" assortis des écarts de rémunération correspondant à chacun de ces emplois ; qu'en jugeant, sur le fondement de ces dispositions, que les documents relatifs à ces listes d'emploi repères étaient des documents administratifs communicables, sans rechercher s'ils présentaient un lien suffisamment direct avec les missions de service public dont La Poste demeure chargée, ni s'assurer qu'aucun secret ne faisait obstacle à leur communication, le tribunal administratif a entaché son jugement d'une erreur de droit ; que, par suite, La Poste est fondée pour ce motif à en demander l'annulation, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi ;<br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ; <br/>
<br/>
              5. Considérant que les documents demandés sont relatifs aux règles applicables à des personnels dont une partie est affectée à l'organisation, la conduite et la mise en oeuvre des missions de service public dont La Poste est chargée ; qu'ils présentent avec ces missions un lien suffisamment direct pour être regardés comme des documents administratifs au sens de l'article 1er de la loi du 17 juillet 1978 ; <br/>
<br/>
              6. Considérant que les documents demandés mentionnent des listes d'emploi auxquels correspondent, pour chacun, des fourchettes très larges de rémunération allant du simple à plus du double ; que la généralité et le degré de précision assez faible de ces informations, qui ne portent ni sur les effectifs, ni sur la stratégie ou l'organisation de l'entreprise, ni ne suffisent à les révéler, ne permettent pas de regarder leur communication, contrairement ce que soutient La Poste, comme de nature à porter atteinte au secret industriel et commercial ;<br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que M. A...est fondé à demander l'annulation de la décision qu'il attaque ; qu'il n'y a cependant plus lieu de statuer sur ses conclusions tendant à ce qu'il soit enjoint à La Poste de lui communiquer les documents en cause, qui lui ont été communiqués en exécution du jugement attaqué ; <br/>
<br/>
              8. Considérant, enfin, que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de M. A...qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>				D E C I D E  :<br/>
              				--------------<br/>
<br/>
Article 1er : Le jugement du tribunal administratif de Montreuil du 27 mai 2010 est annulé.<br/>
<br/>
Article 2 : La décision de La Poste refusant la communication des documents relatifs aux " emplois repère " est annulée.<br/>
<br/>
Article 3 : Le surplus des conclusions de M. A...est rejeté.<br/>
Article 4 : Les conclusions de La Poste présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à La Poste et à M. B...A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">26-06-01-02-01 DROITS CIVILS ET INDIVIDUELS. ACCÈS AUX DOCUMENTS ADMINISTRATIFS. ACCÈS AUX DOCUMENTS ADMINISTRATIFS AU TITRE DE LA LOI DU 17 JUILLET 1978. DROIT À LA COMMUNICATION. NOTION DE DOCUMENT ADMINISTRATIF. - DOCUMENT ADMINISTRATIF ÉLABORÉ OU DÉTENU PAR UN ORGANISME PRIVÉ CHARGÉ D'UNE MISSION DE SERVICE PUBLIC EXERÇANT ÉGALEMENT UNE ACTIVITÉ PRIVÉE - 1) CRITÈRE - DOCUMENT PRÉSENTANT UN LIEN SUFFISAMMENT DIRECT AVEC LA MISSION DE SERVICE PUBLIC - 2) APPLICATION EN L'ESPÈCE - LA POSTE - DOCUMENT RELATIF AUX RÈGLES APPLICABLES À DES PERSONNELS EN PARTIE AFFECTÉS AUX MISSIONS DE SERVICE PUBLIC - INCLUSION.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">26-06-01-02-02 DROITS CIVILS ET INDIVIDUELS. ACCÈS AUX DOCUMENTS ADMINISTRATIFS. ACCÈS AUX DOCUMENTS ADMINISTRATIFS AU TITRE DE LA LOI DU 17 JUILLET 1978. DROIT À LA COMMUNICATION. DOCUMENTS ADMINISTRATIFS COMMUNICABLES. - LA POSTE - LISTES D'EMPLOIS REPÈRES ASSORTIS DES ÉCARTS DE RÉMUNÉRATION CORRESPONDANT À CHACUN DE CES EMPLOIS - INFORMATIONS COUVERTES PAR LE SECRET INDUSTRIEL ET COMMERCIAL - ABSENCE.
</SCT>
<ANA ID="9A"> 26-06-01-02-01 1) Parmi les documents détenus par un organisme privé chargé d'une mission de service public qui exerce également une activité privée, seuls ceux qui présentent un lien suffisamment direct avec la mission de service public peuvent être regardés comme des documents administratifs au sens de l'article 1er de la loi n° 78-753 du 17 juillet 1978.... ,,2) Les listes d'emplois repères assortis des écarts de rémunération correspondant à chacun de ces emplois, élaborées par La Poste au titre des nouveaux instruments de gestion de carrière pour l'ensemble de ses personnels d'encadrement supérieur, sont des documents relatifs aux règles applicables à des personnels dont une partie est affectée à l'organisation, la conduite et la mise en oeuvre des missions de service public dont La Poste est chargée. Ils présentent avec ces missions un lien suffisamment direct pour être regardés comme des documents administratifs au sens de l'article 1er de la loi du 17 juillet 1978.</ANA>
<ANA ID="9B"> 26-06-01-02-02 Les listes d'emplois repères élaborées par La Poste au titre des nouveaux instruments de gestion de carrière pour l'ensemble de ses personnels d'encadrement supérieur mentionnent des listes d'emploi auxquels correspondent, pour chacun, des fourchettes très larges de rémunération allant du simple à plus du double. La généralité et le degré de précision assez faible de ces informations, qui ne portent ni sur les effectifs, ni sur la stratégie ou l'organisation de l'entreprise, ni ne suffisent à les révéler, ne permettent pas de regarder leur communication comme de nature à porter atteinte au secret industriel et commercial.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
