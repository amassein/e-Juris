<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028686270</ID>
<ANCIEN_ID>JG_L_2014_03_000000359458</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/68/62/CETATEXT000028686270.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème et 5ème sous-sections réunies, 05/03/2014, 359458</TITRE>
<DATE_DEC>2014-03-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>359458</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème et 5ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARADUC, DUHAMEL, RAMEIX ; SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>M. David Moreau</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Gaëlle Dumortier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:359458.20140305</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 16 mai et 2 août 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour l'Agence pour l'enseignement français à l'étranger (AEFE), dont le siège est 19-21 rue du colonel Pierre Avia à Paris (75015), représentée par sa directrice ; l'AEFE demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 10PA04322 du 20 mars 2012 par lequel la cour administrative d'appel de Paris a rejeté sa requête tendant, d'une part, à l'annulation du jugement n° 0811279 du 25 juin 2010 par lequel le tribunal administratif de Paris a annulé sa décision du 20 juillet 2007 refusant l'inscription des deux enfants mineurs de M. B...A...au lycée Jean Renoir de Munich, d'autre part, au rejet de la demande de première instance de M. A... ;<br/>
<br/>
              2°) réglant au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de M. A...une somme de 4 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 18 février 2014, présentée pour M. A... ;<br/>
<br/>
              Vu le code de l'éducation ;<br/>
<br/>
              Vu le décret n° 93-1064 du 9 septembre 1993 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. David Moreau, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Baraduc, Duhamel, Rameix, avocat de l'Agence pour l'enseignement français à l'étranger (AEFE) et à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de M. A...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M.A..., ressortissant français installé à Munich, a demandé au proviseur du lycée français Jean Renoir de Munich, établissement scolaire français placé sous la gestion directe de l'Agence pour l'enseignement français à l'étranger (AEFE) en application de l'article L. 452-3 du code de l'éducation, d'inscrire ses deux enfants respectivement en classe maternelle et au cours préparatoire pour l'année scolaire 2007-2008 ; que le proviseur a informé M. A...qu'il ne pouvait, faute de places disponibles, réserver une suite favorable à cette demande ; que, par une décision du 20 juillet 2007, la directrice de l'AEFE a rejeté le recours administratif formé par M. A... contre cette décision ; que l'AEFE se pourvoit en cassation contre l'arrêt du 20 mars 2012 par lequel la cour administrative d'appel de Paris a rejeté sa requête tendant à l'annulation du jugement du 25 juin 2010 par lequel le tribunal administratif de Paris a annulé la décision du 20 juillet 2007 rejetant le recours administratif de M. A...; <br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 452-2 du code de l'éducation, dans sa rédaction en vigueur à la date du refus d'inscription opposé à M.A..., l'AEFE : " a pour objet : / 1° D'assurer, en faveur des enfants français établis hors de France, les missions de service public relatives à l'éducation ; (...) " ; qu'aux termes de l'article L. 451-1 du même code : " Des décrets en Conseil d'Etat fixent les conditions dans lesquelles les dispositions du présent code sont appliquées aux établissements scolaires français à l'étranger, compte tenu de leur situation particulière et des accords conclus avec des Etats étrangers " ; qu'aux termes de l'article L. 113-1 du même code : " Tout enfant doit pouvoir être accueilli, à l'âge de trois ans, dans une école maternelle ou une classe enfantine le plus près possible de son domicile, si sa famille en fait la demande " ; qu'aux termes de l'article L. 131-1 du même code : " L'instruction est obligatoire pour les enfants des deux sexes, français et étrangers, entre six ans et seize ans " ; que le décret du 9 septembre 1993 relatif aux établissements scolaires français à l'étranger en vigueur à la date de la décision contestée a rendu ces dispositions applicables aux établissements scolaires français à l'étranger sans prévoir de dérogation ;<br/>
<br/>
              3. Considérant que s'il résulte de ce qui précède que les dispositions des articles L. 113 1 et L. 131-1 du code de l'éducation sont applicables à l'AEFE s'agissant de la scolarisation des enfants français établis hors de France, aucun principe ni aucune disposition ne reconnaît à leurs parents le droit de choisir librement l'établissement devant être fréquenté par ces enfants ; que, par suite, en se bornant à retenir, pour juger que l'AEFE avait méconnu les dispositions précitées, que celle-ci avait refusé à M. A...une inscription de ses enfants au lycée Jean Renoir de Munich faute de places disponibles, sans rechercher si l'agence n'avait pas par ailleurs proposé aux demandeurs une solution de scolarisation de nature à assurer le respect de ces dispositions, la cour a commis une erreur de droit ; qu'ainsi, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, l'arrêt attaqué doit être annulé ;<br/>
<br/>
              4. Considérant que, dans les circonstances de l'espèce, il n'y a pas lieu de mettre à la charge de M. A...la somme que demande l'AEFE sur le fondement des  dispositions de l'article L. 761-1 du code de justice administrative ; que les mêmes dispositions font obstacle à ce qu'une somme soit mise à la charge de l'AEFE, qui n'est pas la partie perdante dans la présente instance, au titre des frais exposés par M. A...et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Paris du 20 mars 2012 est annulé.<br/>
Article 2 : L'affaire est renvoyée devant la cour administrative d'appel de Paris.<br/>
Article 3 : Le surplus des conclusions des parties est rejeté.<br/>
Article 4 : La présente décision sera notifiée à l'Agence pour l'enseignement français à l'étranger et à M. B...A....<br/>
Copie en sera adressée pour information au ministre des affaires étrangères et au ministre de l'éducation nationale.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">30-02-025 ENSEIGNEMENT ET RECHERCHE. QUESTIONS PROPRES AUX DIFFÉRENTES CATÉGORIES D'ENSEIGNEMENT. ÉTABLISSEMENTS D'ENSEIGNEMENT FRANÇAIS À L'ÉTRANGER (PREMIER ET SECOND DEGRÉ). - SCOLARISATION DES ENFANTS FRANÇAIS ÉTABLIS HORS DE FRANCE - OBLIGATION POUR L'AEFE DE PROPOSER UNE SOLUTION DE SCOLARISATION, EN CAS DE DEMANDE, À PARTIR DE L'ÂGE DE TROIS ANS DANS UNE ÉCOLE AU PLUS PRÈS DU DOMICILE - EXISTENCE - OBLIGATION DE GARANTIR L'INSTRUCTION OBLIGATOIRE ENTRE SIX ET SEIZE ANS - EXISTENCE - POSSIBILITÉ POUR LES PARENTS DE CHOISIR LIBREMENT LEUR ÉTABLISSEMENT - ABSENCE.
</SCT>
<ANA ID="9A"> 30-02-025 Les dispositions de l'article L. 113-1 du code de l'éducation, qui fondent l'obligation d'accueillir tout enfant à partir de l'âge de trois ans dans une école le plus près possible de son domicile si sa famille en fait la demande, et celles de l'article L. 131-1 du même code, qui rendent l'instruction obligatoire entre six et seize ans, sont applicables à l'Agence pour l'enseignement français à l'étranger (AEFE) s'agissant de la scolarisation des enfants français établis hors de France. S'il appartient à l'AEFE, saisie d'une demande de scolarisation, de proposer une solution de nature à assurer le respect de ces dispositions, aucun principe ni aucune disposition ne reconnaît toutefois aux parents le droit de choisir librement l'établissement devant être fréquenté par leurs enfants.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
