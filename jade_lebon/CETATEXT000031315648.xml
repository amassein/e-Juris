<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031315648</ID>
<ANCIEN_ID>JG_L_2015_10_000000390968</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/31/56/CETATEXT000031315648.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème / 2ème SSR, 14/10/2015, 390968</TITRE>
<DATE_DEC>2015-10-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>390968</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème / 2ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER ; SCP NICOLAY, DE LANOUVELLE, HANNOTIN ; SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Olivier Henrard</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:390968.20151014</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              La société RevetSens a demandé, le 22 avril 2015, au juge du référé précontractuel du tribunal administratif de Lille, d'annuler la procédure lancée par la région Nord-Pas-de-Calais en vue de la passation d'un marché portant sur la poursuite de la dématérialisation des " chéquiers livres région " et des " chéquiers équipements des apprentis " par la mise en place d'une carte multiservices " Génération Nord-Pas-de-Calais ".<br/>
<br/>
              Par une ordonnance n° 1503492 du 28 mai 2015, le juge du référé précontractuel du tribunal administratif de Lille a annulé la procédure de passation du marché litigieux.<br/>
<br/>
              1° Sous le n° 390968, par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 12 et 26 juin et 1er octobre 2015 au secrétariat du contentieux du Conseil d'Etat, la SA Applicam demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette ordonnance ; <br/>
<br/>
              2°) statuant en référé, de rejeter la demande de la société RevetSens ;<br/>
<br/>
              3°) de mettre à la charge de la société RevetSens la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              2° Sous le n° 391105, par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 17 juin et 2 juillet 2015 au secrétariat du contentieux du Conseil d'Etat, la région Nord-Pas-de-Calais demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler la même ordonnance ; <br/>
<br/>
              2°) statuant en référé, de rejeter la demande de la société RevetSens ;<br/>
<br/>
              3°) de mettre à la charge de la société RevetSens la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - la directive 2014/24/UE du Parlement européen et du Conseil du 26 février 2014 ;<br/>
              - le code des marchés publics ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Henrard, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Foussard, Froger, avocat de la SA Applicam , à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la région Nord-Pas-de-Calais et à la SCP Piwnica, Molinié, avocat de la société RevetSens ;<br/>
<br/>
              Vu les notes en délibéré, enregistrées le 5 octobre 2015, présentées par la SA Applicam ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 551-1 du code de justice administrative, dans sa rédaction applicable à la procédure suivie devant le tribunal administratif de Lille : " Le président du tribunal administratif, ou le magistrat qu'il délègue, peut être saisi en cas de manquement aux obligations de publicité et de mise en concurrence auxquelles est soumise la passation par les pouvoirs adjudicateurs de contrats administratifs ayant pour objet l'exécution de travaux, la livraison de fournitures ou la prestation de services, avec une contrepartie économique constituée par un prix ou un droit d'exploitation, la délégation d'un service public ou la sélection d'un actionnaire opérateur économique d'une société d'économie mixte à opération unique. / Le juge est saisi avant la conclusion du contrat. " ; que, selon le I de l'article L. 551-2 du même code : " Le juge peut ordonner à l'auteur du manquement de se conformer à ses obligations et suspendre l'exécution de toute décision qui se rapporte à la passation du contrat, sauf s'il estime, en considération de l'ensemble des intérêts susceptibles d'être lésés et notamment de l'intérêt public, que les conséquences négatives de ces mesures pourraient l'emporter sur leurs avantages. / Il peut, en outre, annuler les décisions qui se rapportent à la passation du contrat et supprimer les clauses ou prescriptions destinées à figurer dans le contrat et qui méconnaissent lesdites obligations. " ; qu'enfin, l'article L. 551-3 de ce code dispose que : " Le président du tribunal administratif ou son délégué statue en premier et dernier ressort en la forme des référés. " ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis au juge des référés que, le 6 février 2015, la région Nord-Pas-de-Calais a lancé une procédure d'appel d'offres ouvert en vue de la passation d'un marché à bon de commandes ayant pour objet la mise en place d'une carte dématérialisée " Génération Nord-Pas-de-Calais ", destinée à se substituer aux dispositifs existants des " chéquiers livres région " et " chéquiers équipements des apprentis " ; que, le 9 avril 2015, le président du conseil régional a informé la société RevetSens du rejet de son offre et de l'attribution du marché à la SA Applicam ; que la société RevetSens a saisi le juge du référé précontractuel du tribunal administratif de Lille, qui a annulé la procédure par une ordonnance du 28 mai 2015 ; que, par des pourvois qu'il y a lieu de joindre, la SA Applicam et la région Nord-Pas-de-Calais demandent l'annulation de cette ordonnance ;<br/>
<br/>
              3. Considérant que, pour annuler la procédure au motif que la région Nord-Pas-de-Calais s'était assuré la collaboration, comme assistant à la maîtrise d'ouvrage pour l'élaboration des pièces du marché litigieux et l'analyse des offres des candidats, de M.A..., ancien responsable de la SA Applicam, attributaire du marché, et qu'une telle circonstance était de nature à faire naître un doute légitime sur l'impartialité de cette procédure, le juge du référé précontractuel s'est fondé sur la définition du conflit d'intérêts posée par l'article 24 de la directive du Parlement européen et du Conseil du 26 février 2014 sur la passation des marchés publics ; qu'en statuant ainsi, alors qu'à la date à laquelle a été lancée la procédure litigieuse, cette directive n'avait pas été transposée, que son délai de transposition, dont le terme est fixé au 18 avril 2016, n'était pas expiré et que l'attribution du marché, laquelle présente le caractère d'une décision individuelle, ne pouvait être regardée comme de nature à compromettre sérieusement la réalisation du résultat prescrit pas cette directive, il a commis une erreur de droit ; qu'il suit de là que, sans qu'il soit besoin d'examiner les autres moyens des pourvois, l'ordonnance attaquée doit être annulée ;<br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de statuer sur la demande en référé présentée par la société RevetSens en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              5. Considérant qu'au nombre des principes généraux du droit qui s'imposent au pouvoir adjudicateur comme à toute autorité administrative figure le principe d'impartialité, dont la méconnaissance est constitutive d'un manquement aux obligations de publicité et de mise en concurrence ; qu'en l'espèce, il résulte de l'instruction que, d'une part, M.A..., chargé par la région d'une mission d'assistance à la maîtrise d'ouvrage pour le marché litigieux, a non seulement contribué à la rédaction du cahier des clauses techniques particulières mais aussi à l'analyse des offres des candidats aux côtés des services de la région et qu'il a ainsi été susceptible d'influencer l'issue de la procédure litigieuse ; que, d'autre part, M. A...a exercé des responsabilités importantes au sein de la SA Applicam, en qualité de directeur qualité puis de directeur des opérations et des projets, et qu'ayant occupé ces fonctions du mois de décembre 2001 au mois d'avril 2013, il n'avait donc quitté l'entreprise que moins de deux ans avant le lancement de la procédure litigieuse ; que s'il ne résulte pas de l'instruction que l'intéressé détiendrait encore des intérêts au sein de l'entreprise, le caractère encore très récent de leur collaboration, à un haut niveau de responsabilité, pouvait légitimement faire naître un doute sur la persistance de tels intérêts et par voie de conséquence sur l'impartialité de la procédure suivie par la région Nord-Pas-de-Calais ; qu'il était au demeurant loisible à la région, qui avait connaissance de la qualité d'ancien salarié de la SA Applicam de M.A..., de mettre en oeuvre, une fois connue la candidature de cette société, toute mesure en vue de lever ce doute légitime, par exemple en l'écartant de la procédure d'analyse des offres ; que, dans ces conditions, la région Nord-Pas-de-Calais a méconnu ses obligations de publicité et de mise en concurrence ; que, par suite, il y a lieu, sans qu'il soit besoin d'examiner les autres moyens de la demande, d'annuler la procédure contestée ;<br/>
<br/>
              6. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la société RevetSens qui n'est pas, dans la présente instance, la partie perdante ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par cette société au titre des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du 28 mai 2015 du juge des référés du tribunal administratif de Lille est annulée.<br/>
Article 2 : La procédure de passation du marché ayant pour objet la poursuite de la dématérialisation des " chéquiers livres région " et des " chéquiers équipement des apprentis " par la mise en place d'une carte multiservice " Génération NPDC ", lancée par la région Nord-Pas-de-Calais, est annulée.<br/>
Article 3 : Les conclusions de la SA Applicam, de la région Nord-Pas-de-Calais et de la société RevetSens présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à la SA Applicam, à la région Nord-Pas-de-Calais et à la société RevetSens.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-04-03 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. PRINCIPES GÉNÉRAUX DU DROIT. - PRINCIPE D'IMPARTIALITÉ - POUVOIR ADJUDICATEUR - CAS D'UNE PERSONNE AYANT PARTICIPÉ À LA PROCÉDURE D'ADJUDICATION EN ÉTANT SUSCEPTIBLE D'EXERCER UNE INFLUENCE SUR SON ISSUE, ALORS QU'ELLE OCCUPAIT PEU DE TEMPS AUPARAVANT UN EMPLOI À HAUT NIVEAU DE RESPONSABILITÉ DANS L'ENTREPRISE ATTRIBUTAIRE DU MARCHÉ - IRRÉGULARITÉ.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">39-02-005 MARCHÉS ET CONTRATS ADMINISTRATIFS. FORMATION DES CONTRATS ET MARCHÉS. FORMALITÉS DE PUBLICITÉ ET DE MISE EN CONCURRENCE. - OBLIGATION D'IMPARTIALITÉ DU POUVOIR ADJUDICATEUR - 1) EXISTENCE - 2) CAS D'UNE PERSONNE AYANT PARTICIPÉ À LA PROCÉDURE D'ADJUDICATION EN ÉTANT SUSCEPTIBLE D'EXERCER UNE INFLUENCE SUR SON ISSUE, ALORS QU'ELLE OCCUPAIT PEU DE TEMPS AUPARAVANT UN EMPLOI À HAUT NIVEAU DE RESPONSABILITÉ DANS L'ENTREPRISE ATTRIBUTAIRE DU MARCHÉ - IRRÉGULARITÉ.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">39-08-015-01 MARCHÉS ET CONTRATS ADMINISTRATIFS. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. PROCÉDURES D'URGENCE. - IMPARTIALITÉ DU POUVOIR ADJUDICATEUR - 1) CONTRÔLE PAR LE JUGE DU RÉFÉRÉ PRÉCONTRACTUEL - EXISTENCE - 2) CAS D'UNE PERSONNE AYANT PARTICIPÉ À LA PROCÉDURE D'ADJUDICATION EN ÉTANT SUSCEPTIBLE D'EXERCER UNE INFLUENCE SUR SON ISSUE, ALORS QU'ELLE OCCUPAIT PEU DE TEMPS AUPARAVANT UN EMPLOI À HAUT NIVEAU DE RESPONSABILITÉ DANS L'ENTREPRISE ATTRIBUTAIRE DU MARCHÉ - IRRÉGULARITÉ.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">54-03-05 PROCÉDURE. PROCÉDURES DE RÉFÉRÉ AUTRES QUE CELLES INSTITUÉES PAR LA LOI DU 30 JUIN 2000. PROCÉDURE PROPRE À LA PASSATION DES CONTRATS ET MARCHÉS. - IMPARTIALITÉ DU POUVOIR ADJUDICATEUR - 1) CONTRÔLE PAR LE JUGE DU RÉFÉRÉ PRÉCONTRACTUEL - EXISTENCE - 2) CAS D'UNE PERSONNE AYANT PARTICIPÉ À LA PROCÉDURE D'ADJUDICATION EN ÉTANT SUSCEPTIBLE D'EXERCER UNE INFLUENCE SUR SON ISSUE, ALORS QU'ELLE OCCUPAIT PEU DE TEMPS AUPARAVANT UN EMPLOI À HAUT NIVEAU DE RESPONSABILITÉ DANS L'ENTREPRISE ATTRIBUTAIRE DU MARCHÉ - IRRÉGULARITÉ.
</SCT>
<ANA ID="9A"> 01-04-03 Le principe d'impartialité, principe général du droit, s'impose au pouvoir adjudicateur comme à toute autorité administrative.... ,,Une personne a participé à la procédure d'adjudication d'un marché dans des conditions lui permettant d'influencer l'issue de la procédure litigieuse (contribution à la rédaction du cahier des clauses techniques particulières et à l'analyse des offres des candidats), alors qu'elle avait exercé des responsabilités importantes au sein de l'entreprise attributaire, jusqu'à moins de deux ans avant le lancement de cette procédure. Même si cette personne ne détient plus d'intérêts dans l'entreprise attributaire du marché, le caractère très récent de leur collaboration, à un haut niveau de responsabilité, pouvait légitimement faire naître un doute sur la persistance de tels intérêts et par voie de conséquence sur l'impartialité de la procédure suivie par le pouvoir adjudicateur.</ANA>
<ANA ID="9B"> 39-02-005 1) Le principe d'impartialité, principe général du droit, s'impose au pouvoir adjudicateur comme à toute autorité administrative. Sa méconnaissance est constitutive d'un manquement aux obligations de publicité et de mise en concurrence.,,,2) Une personne a participé à la procédure d'adjudication d'un marché dans des conditions lui permettant d'influencer l'issue de la procédure litigieuse (contribution à la rédaction du cahier des clauses techniques particulières et à l'analyse des offres des candidats), alors qu'elle avait exercé des responsabilités importantes au sein de l'entreprise attributaire, jusqu'à moins de deux ans avant le lancement de cette procédure. Même si cette personne ne détient plus d'intérêts dans l'entreprise attributaire du marché, le caractère très récent de leur collaboration, à un haut niveau de responsabilité, pouvait légitimement faire naître un doute sur la persistance de tels intérêts et par voie de conséquence sur l'impartialité de la procédure suivie par le pouvoir adjudicateur. Par suite, le pouvoir adjudicateur a méconnu ses obligations de publicité et de mise en concurrence.</ANA>
<ANA ID="9C"> 39-08-015-01 1) Le principe d'impartialité, principe général du droit, s'impose au pouvoir adjudicateur comme à toute autorité administrative. Sa méconnaissance est constitutive d'un manquement aux obligations de publicité et de mise en concurrence.,,,2) Une personne a participé à la procédure d'adjudication d'un marché dans des conditions lui permettant d'influencer l'issue de la procédure litigieuse (contribution à la rédaction du cahier des clauses techniques particulières et à l'analyse des offres des candidats), alors qu'elle avait exercé des responsabilités importantes au sein de l'entreprise attributaire, jusqu'à moins de deux ans avant le lancement de cette procédure. Même si cette personne ne détient plus d'intérêts dans l'entreprise attributaire du marché, le caractère très récent de leur collaboration, à un haut niveau de responsabilité, pouvait légitimement faire naître un doute sur la persistance de tels intérêts et par voie de conséquence sur l'impartialité de la procédure suivie par le pouvoir adjudicateur. Par suite, le pouvoir adjudicateur a méconnu ses obligations de publicité et de mise en concurrence.</ANA>
<ANA ID="9D"> 54-03-05 1) Le principe d'impartialité, principe général du droit, s'impose au pouvoir adjudicateur comme à toute autorité administrative. Sa méconnaissance est constitutive d'un manquement aux obligations de publicité et de mise en concurrence.,,,2) Une personne a participé à la procédure d'adjudication d'un marché dans des conditions lui permettant d'influencer l'issue de la procédure litigieuse (contribution à la rédaction du cahier des clauses techniques particulières et à l'analyse des offres des candidats), alors qu'elle avait exercé des responsabilités importantes au sein de l'entreprise attributaire, jusqu'à moins de deux ans avant le lancement de cette procédure. Même si cette personne ne détient plus d'intérêts dans l'entreprise attributaire du marché, le caractère très récent de leur collaboration, à un haut niveau de responsabilité, pouvait légitimement faire naître un doute sur la persistance de tels intérêts et par voie de conséquence sur l'impartialité de la procédure suivie par le pouvoir adjudicateur. Par suite, le pouvoir adjudicateur a méconnu ses obligations de publicité et de mise en concurrence.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
