<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044553293</ID>
<ANCIEN_ID>JG_L_2021_12_000000445969</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/55/32/CETATEXT000044553293.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 21/12/2021, 445969</TITRE>
<DATE_DEC>2021-12-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>445969</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>CABINET BRIARD</AVOCATS>
<RAPPORTEUR>M. Clément Tonon</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Philippe Ranquet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:445969.20211221</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. D... G..., M. B... G..., M. AC... AE..., M. AO... AE..., M. X... AI..., Mme AJ... AI..., M. N... S..., Mme AK... S..., M. Y... W..., Mme K... AE..., M. V... AD..., M. I... AD..., M. AQ... AU..., Mme J... W..., Mme AG... AX... W..., M. AT... Z..., M. H... AC..., Mme AB... AC..., Mme AL... AP... et M. E... AN... ont demandé au tribunal administratif d'Amiens d'annuler les opérations électorales qui se sont déroulées les 15 mars et 28 juin 2020 en vue de l'élection des conseillers municipaux de la commune de Villequier-Aumont (Aisne), d'annuler l'élection de M. Bruno Lelong et de certains de ses colistiers et de déclarer M. Bruno Lelong inéligible.<br/>
<br/>
              Par un jugement n° 2000969, 2001846 du 30 septembre 2020, le tribunal administratif d'Amiens a rejeté leurs protestations.<br/>
<br/>
              Par une requête, enregistrée le 5 novembre 2020 au secrétariat du contentieux du Conseil d'Etat, M. D... G... et M. B... G... demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) d'annuler les opérations électorales du 15 mars et du 28 juin 2020 à Villequier-Aumont.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code électoral ;<br/>
              - l'ordonnance n° 2020-305 du 25 mars 2020 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Clément Tonon, auditeur, <br/>
<br/>
              - les conclusions de M. Philippe Ranquet, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, après les conclusions, au cabinet Briard, avocat de Mme L... et autres ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1.	Lors des élections municipales qui ont eu lieu dans la commune de Villequier-Aumont (Aisne), commune de moins de mille habitants, les 15 mars et 28 juin 2020, deux listes se sont opposées, menées respectivement par M. D... G..., maire sortant, et par M. C... F.... À l'issue du premier tour des élections, qui s'est déroulé le 15 mars 2020, neuf candidats ayant obtenu plus de 191 voix ont été élus, dont M. Bruno Lelong, conseiller municipal sortant d'opposition. À l'issue du second tour, qui s'est déroulé le 28 juin 2020, six candidats ont été élus, dont M. C... F.... MM. Gaël et Rémy G... relèvent appel du jugement du 30 septembre 2020 par lequel le tribunal administratif d'Amiens a rejeté leur protestation tendant à l'annulation de ces opérations électorales.<br/>
<br/>
              2.	Dans son jugement, le tribunal administratif d'Amiens a omis de statuer sur trois griefs, tirés de la méconnaissance des règles de propagande électorale, des conditions de déroulement d'une séance du conseil de la communauté d'agglomération Chauny-Tergnier-La Fère et de l'altération de la sincérité du scrutin par le taux d'abstention imputable au contexte sanitaire. Dès lors, le jugement attaqué est entaché d'irrégularité et doit, pour ce motif, être annulé. Le délai imparti au tribunal administratif par l'article R. 120 du code électoral pour statuer sur la protestation de M. G... et autres étant expiré, il y a lieu pour le Conseil d'État de statuer immédiatement sur cette protestation. <br/>
<br/>
              Sur la recevabilité des protestations :<br/>
<br/>
              3.	MM. Gaël et Rémy G... ont formé deux protestations, enregistrées les 19 mars et 3 juillet 2020, dirigées respectivement contre les résultats du premier tour et contre ceux des deux tours de scrutin. Contrairement à ce que soutiennent M. F... et autres, ils ont ainsi demandé l'annulation de chacun des deux tours de scrutin dans le délai prescrit par l'article R. 119 du code électoral.<br/>
<br/>
              Sur les griefs soulevés dans le mémoire du 31 juillet 2020 :<br/>
<br/>
              4.	Les griefs mentionnés au point 2, invoqués pour la première fois par les requérants dans un mémoire enregistré le 31 juillet 2020 au greffe du tribunal, soit après l'expiration du délai de recours contre les opérations électorales, doivent être écartés comme tardifs.<br/>
<br/>
              Sur l'éligibilité de M. T... :<br/>
<br/>
              5.	Aux termes de l'article L. 231 du code électoral : " Ne peuvent être élus conseillers municipaux dans les communes situées dans le ressort où ils exercent ou ont exercé leurs fonctions depuis moins de six mois : (...) 6° Les comptables des deniers communaux agissant en qualité de fonctionnaire et les entrepreneurs de services municipaux ; ".<br/>
<br/>
              6.	Il résulte de l'instruction que l'entreprise Lelong-Vanacker, au sein de laquelle M. T... joue un rôle prédominant, a passé le 15 novembre 2011 avec la commune de Villequier-Aumont une convention en vue d'assurer le déneigement de la commune, renouvelée le 24 septembre 2018 et dénoncée par M. T... le 8 janvier 2020. Dans ces conditions, cette société participait à cette date au service municipal d'entretien de la voirie et M. T... avait, par suite, moins de six mois avant la date du premier tour de l'élection contestée, la qualité d'entrepreneur de service municipal au sens des dispositions de l'article L. 231 du code électoral, nonobstant la circonstance que cette société n'avait pas fourni de prestation depuis plus de six mois à la date du premier tour de scrutin, et qu'une seule facture d'un montant de 486 euros avait été émise au titre de cette convention en 2019. M. T... était, par suite, inéligible au conseil municipal de Villequier-Aumont à la date du scrutin.<br/>
<br/>
              Sur la régularité de l'établissement des listes électorales :<br/>
<br/>
              7.	Aux termes de l'article L. 18 du code électoral : " I. - Le maire vérifie si la demande d'inscription de l'électeur répond aux conditions mentionnées au I de l'article L. 11 ou aux articles L. 12 à L. 15-1. Il statue sur cette demande dans un délai de cinq jours à compter de son dépôt. / Le maire radie les électeurs qui ne remplissent plus aucune des conditions mentionnées au premier alinéa du présent I à l'issue d'une procédure contradictoire. / II.- Les décisions prises par le maire en application du I du présent article sont notifiées aux électeurs intéressés dans un délai de deux jours (...). / III. - Tout recours contentieux formé par l'électeur intéressé contre une décision prise au titre du présent article est précédé d'un recours administratif préalable, à peine d'irrecevabilité du recours contentieux. / Ce recours administratif préalable est formé dans un délai de cinq jours à compter de la notification de la décision prévue au II du présent article. Le recours est examiné par la commission mentionnée à l'article L. 19. (...) ". Aux termes de l'article L. 19 du même code : " I. - Dans chaque commune ou, à Paris, Marseille et Lyon, dans chaque arrondissement, une commission de contrôle statue sur les recours administratifs préalables prévus au III de l'article L. 18. / II. - La commission s'assure également de la régularité de la liste électorale. A cette fin, elle a accès à la liste des électeurs inscrits dans la commune extraite du répertoire électoral unique et permanent. / Elle peut, à la majorité de ses membres, au plus tard le vingt-et-unième jour avant chaque scrutin, réformer les décisions prévues au II de l'article L. 18 ou procéder à l'inscription ou à la radiation d'un électeur omis ou indûment inscrit. Lorsqu'elle radie un électeur, sa décision est soumise à une procédure contradictoire. / La décision de la commission est notifiée dans un délai de deux jours à l'électeur intéressé, au maire et à l'Institut national de la statistique et des études économiques. (...) IV. - Dans les communes de moins de 1 000 habitants, la commission est composée : / 1° D'un conseiller municipal pris dans l'ordre du tableau parmi les membres prêts à participer aux travaux de la commission, ou, à défaut, du plus jeune conseiller municipal. Le maire, les adjoints titulaires d'une délégation et les conseillers municipaux titulaires d'une délégation en matière d'inscription sur la liste électorale ne peuvent siéger au sein de la commission en application du présent 1° ; / 2° D'un délégué de l'administration désigné par le représentant de l'Etat dans le département ; / 3° D'un délégué désigné par le président du tribunal judiciaire. / Lorsqu'une délégation spéciale est nommée en application de l'article L. 2121-36 du code général des collectivités territoriales, le conseiller municipal mentionné au 1° du présent IV est remplacé par un membre de la délégation spéciale désigné par le représentant de l'Etat dans le département. / Les conseillers municipaux et les agents municipaux de la commune, de l'établissement public de coopération intercommunale ou des communes membres de celui-ci ne peuvent pas être désignés en application des 2° et 3° du présent IV. " Si le juge administratif n'est pas compétent pour statuer sur la régularité des inscriptions et radiations sur la liste électorale, il lui incombe en revanche de rechercher si des manœuvres dans l'établissement de la liste électorale ont altéré la sincérité du scrutin.<br/>
<br/>
              8.	En premier lieu, si les requérants soutiennent, d'une part, que la commission de contrôle des listes électorales, réunie environ trois jours avant le premier tour de scrutin, aurait remis au maire une liste comportant 40 radiations, dans des conditions empêchant tout recours contre les tableaux des additions et retranchements de la liste électorale, qui n'aurait pu être publiée, d'autre part, que certaines inscriptions sur la liste électorale prêteraient à réserve et enfin, que la liste d'émargement aurait été corrigée manuellement le jour du scrutin pour prendre en compte ces radiations, il ne résulte de l'instruction ni que la commission de contrôle des listes électorales se serait réunie tardivement, ni que cette commission aurait apporté des modifications aux inscriptions et radiations décidées par le maire, ni, par suite, que les corrections apportées aux listes d'émargement seraient liées à l'intervention de cette commission. Par suite, le grief tiré de ce que des irrégularités commises lors de l'établissement des listes électorales auraient altéré la sincérité du scrutin ne peut, en tout état de cause, qu'être écarté.<br/>
<br/>
              9.	En second lieu, la circonstance que M. T... ait été inéligible au moment où a siégé la commission de contrôle des listes électorales, au sein de laquelle il représentait la commune au titre de son mandat de conseiller municipal, ne saurait suffire à caractériser une manœuvre de nature à avoir altéré la sincérité du scrutin. <br/>
<br/>
              Sur la régularité des opérations électorales :<br/>
<br/>
              10.	En premier lieu, il ne résulte pas de l'instruction que la présence de M. T..., dont il résulte du point 6 qu'il était inéligible à la date du scrutin, sur la liste menée par M. F... ait été constitutive d'une manœuvre de nature à avoir altéré la sincérité du scrutin.<br/>
<br/>
              11.	En deuxième lieu, il résulte de l'instruction que Mme U... AW... a consenti le 9 mars 2020 à Mme AC... AR... une procuration valable jusqu'au 16 mars 2020. Cette procuration, dont il n'est pas soutenu qu'elle aurait été utilisée pour le second tour du scrutin, était régulière et valable pour le premier tour. Par suite, le grief tiré de ce que la prise en compte d'une procuration irrégulière aurait altéré la sincérité du scrutin ne peut qu'être écarté.<br/>
<br/>
              12.	En troisième lieu, aux termes de l'article L. 52-8 du code électoral, dans sa version applicable au litige : " Une personne physique peut verser un don à un candidat si elle est de nationalité française ou si elle réside en France. Les dons consentis par une personne physique dûment identifiée pour le financement de la campagne d'un ou plusieurs candidats lors des mêmes élections ne peuvent excéder 4 600 euros. / Les personnes morales, à l'exception des partis ou groupements politiques, ne peuvent participer au financement de la campagne électorale d'un candidat, ni en lui consentant des dons sous quelque forme que ce soit, ni en lui fournissant des biens, services ou autres avantages directs ou indirects à des prix inférieurs à ceux qui sont habituellement pratiqués (...) ". S'il résulte de l'instruction qu'entre les premier et second tours, les membres de la liste menée par M. F... ont procédé à une distribution gratuite de masques de protection, financés par des donateurs, auprès des habitants de Villequier-Aumont, il n'est ni établi, ni même soutenu que les dons en question émaneraient d'une personne morale en méconnaissance des dispositions précitées de L. 52-8 du code électoral ou méconnaitraient les conditions dans lesquelles une ou plusieurs personnes physiques peuvent faire un don. Par suite, le grief tiré de la méconnaissance des dispositions de l'article L. 52-8 du code électoral doit être écarté.<br/>
<br/>
              13.	Il résulte de tout ce qui précède qu'il y a lieu d'annuler uniquement l'élection de M. T... en qualité de conseiller municipal de la commune de Villequier-Aumont et de rejeter le surplus des protestations.<br/>
<br/>
              14.	Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit à la demande de M. F... et autres tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le jugement du 30 septembre 2020 du tribunal administratif d'Amiens est annulé.<br/>
Article 2 : L'élection de M. Bruno Lelong en qualité de conseiller municipal de la commune de Villequier-Aumont est annulée.<br/>
Article 3 : Le surplus des conclusions des protestations présentées par M. G... et autres est rejeté.<br/>
Article 4 : Les conclusions présentées par M. F... et autres au titre de l'article L. 761 1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée M. D... G..., premier requérant dénommé, à M. C... F..., représentant unique pour l'ensemble des défendeurs, et au ministre de l'intérieur.<br/>
<br/>
              Délibéré à l'issue de la séance du 1er décembre 2021 où siégeaient : Mme Christine Maugüé, présidente adjointe de la section du contentieux, présidant ; M. AF... AH..., M. Olivier Japiot, présidents de chambre ; M. M... AA..., Mme A... AM..., M. Q... AV..., M. R... AS..., M. Jean-Yves Ollier, conseillers d'Etat et M. Clément Tonon, auditeur-rapporteur. <br/>
<br/>
              Rendu le 21 décembre 2021.<br/>
<br/>
<br/>
                 La Présidente : <br/>
                 Signé : Mme Christine Maugüé<br/>
 		Le rapporteur : <br/>
      Signé : M. Clément Tonon<br/>
                 La secrétaire :<br/>
                 Signé : Mme O... P...<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">28-04-02-02-05 ÉLECTIONS ET RÉFÉRENDUM. - ÉLECTIONS MUNICIPALES. - ÉLIGIBILITÉ. - INÉLIGIBILITÉS. - ENTREPRENEURS DE SERVICES MUNICIPAUX. - ENTREPRISE TITULAIRE D'UNE CONVENTION L'ASSOCIANT AU SERVICE PUBLIC MUNICIPAL - CIRCONSTANCES SANS INCIDENCE - CARACTÈRE OCCASIONNEL DES PRESTATIONS FOURNIES - FAIBLE RÉMUNÉRATION À CE TITRE.
</SCT>
<ANA ID="9A"> 28-04-02-02-05 Entreprise, au sein de laquelle le candidat joue un rôle prédominant, ayant passé avec la commune une convention en vue d'assurer son déneigement. Convention ayant été dénoncée par l'intéressé moins de six mois avant la date du premier tour de l'élection. ......Cette entreprise participe jusqu'à la date de cette dénonciation au service municipal d'entretien de la voirie et l'intéressé a, par suite, moins de six mois avant la date du premier tour de l'élection contestée, la qualité d'entrepreneur de service municipal au sens de l'article L. 231 du code électoral.......Il en va ainsi alors même que cette entreprise n'a pas fourni de prestation depuis plus de six mois à la date du premier tour de scrutin, et qu'une seule facture d'un montant modique a été émise en exécution de la convention au titre de l'année précédente.......L'intéressé est, par suite, inéligible au conseil municipal à la date du scrutin.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
