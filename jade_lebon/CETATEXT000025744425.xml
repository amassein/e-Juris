<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025744425</ID>
<ANCIEN_ID>JG_L_2012_04_000000340954</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/74/44/CETATEXT000025744425.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème et 3ème sous-sections réunies, 24/04/2012, 340954</TITRE>
<DATE_DEC>2012-04-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>340954</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème et 3ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Jacques Arrighi de Casanova</PRESIDENT>
<AVOCATS>SCP BORE ET SALVE DE BRUNETON ; RICARD</AVOCATS>
<RAPPORTEUR>M. Guillaume Prévost</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Olléon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:340954.20120424</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 28 juin et 28 septembre 2010 au secrétariat du contentieux du Conseil d'Etat, présentés pour la SARL ANGLES HABITAT, dont le siège est 5 rue Edmond Rostand, Les Angles (30133), représentée par son gérant en exercice ; elle demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 08MA01173 du 23 avril 2010 par lequel la cour administrative d'appel de Marseille a annulé le jugement n° 0404179 du 7 décembre 2007 du tribunal administratif de Nîmes condamnant la commune de Pujaut à lui restituer la somme de 42 433,42 euros et a rejeté sa demande ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de la commune ; <br/>
<br/>
              3°) de mettre à la charge de la commune de Pujaut la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'urbanisme ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Guillaume Prévost, chargé des fonctions de Maître des Requêtes, <br/>
<br/>
              - les observations de la SCP Boré et Salve de Bruneton, avocat de la SARL ANGLES HABITAT et de Me Ricard, avocat de la commune de Pujaut, <br/>
<br/>
              - les conclusions de M. Laurent Olléon, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Boré et Salve de Bruneton, avocat de la SARL ANGLES HABITAT et à Me Ricard, avocat de la commune de Pujaut ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant qu'aux termes du premier alinéa de l'article L. 311-4 du code de l'urbanisme, inséré au chapitre I du titre Ier du livre III relatif aux zones d'aménagement concerté, dans sa rédaction applicable en 2001: " Il ne peut être mis à la charge de l'aménageur de la zone que le coût des équipements publics à réaliser pour répondre aux besoins des futurs habitants ou usagers des constructions à édifier dans la zone " ; qu'aux termes de l'article L. 332-6 du même code : " Les bénéficiaires d'autorisations de construire ne peuvent être tenus que des obligations suivantes : / (...) 3° La réalisation des équipements propres mentionnés à l'article L. 332-15 (...) " ; que ces dernières dispositions sont applicables aux lotisseurs en vertu de l'article L. 332-12 de ce code, dans sa rédaction applicable au litige ; que, selon le premier alinéa de l'article L. 332-30 du même code : " Les taxes et contributions de toute nature qui sont obtenues ou imposées en violation des dispositions des articles L. 311-4 et L. 332-6 sont réputées sans cause ; les sommes versées ou celles qui correspondent au coût de prestations fournies sont sujettes à répétition. L'action en répétition se prescrit par cinq ans à compter du dernier versement ou de l'obtention des prestations indûment exigées. " ; <br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par arrêté du 23 novembre 2001, le maire de la commune de Pujaut a délivré une autorisation de lotir à la SARL ANGLES HABITAT et a mis à sa charge la réalisation de travaux consistant à déplacer et à canaliser un fossé d'évacuation des eaux de ruissellement ; que la société a fait effectuer les travaux prescrits par l'arrêté pour un prix hors taxes de 42 433,42 euros ; <br/>
<br/>
              Considérant que, pour juger que les contributions ainsi obtenues de la société ne pouvaient être réputées sans cause et que l'action en répétition de l'indu ne relevait pas de la prescription prévue par les dispositions de l'article L. 332-30 du code de l'urbanisme, la cour a, par un motif qui, contrairement à ce qui est soutenu, ne présente pas un caractère surabondant, jugé que la réalisation de ces travaux était susceptible de se rattacher à la catégorie des équipements publics à réaliser pour répondre aux besoins des futurs habitants ou usagers des constructions à édifier dans une zone d'aménagement concerté au sens des dispositions de l'article L. 311-4 du code de l'urbanisme, alors qu'elle a également jugé, pour faire droit à la requête de la commune de Pujaut, que ces travaux devaient être regardés comme ayant le caractère d'un équipement propre au lotissement au sens des dispositions de l'article L. 332-6 du même code ; qu'en statuant ainsi et alors que les deux notions n'ont ni le même objet ni la même portée, elle a entaché son arrêt de contradiction de motifs ; que, par suite, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, la SARL ANGLES HABITAT est fondée à demander l'annulation de l'arrêt du 23 avril 2010 ;<br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              Sur la fin de non-recevoir opposée par la SARL ANGLES HABITAT :<br/>
<br/>
              Considérant qu'aux termes de l'article R. 811-2 du code de justice administrative : " Sauf disposition contraire, le délai d'appel est de deux mois (...) " ; que le jugement attaqué a été notifié le 22 janvier 2008 à la commune de Pujaut ; que la requête d'appel présentée par la commune a été enregistrée au greffe de la cour le 7 mars 2008 ; que, par suite, la fin de non-recevoir tirée de la tardiveté de la requête doit être écartée ;<br/>
<br/>
              Sur le bien-fondé de la contribution : <br/>
<br/>
              Considérant qu'aux termes de l'article L. 332-15 du code de l'urbanisme auquel renvoie l'article L. 332-6 : " L'autorité qui délivre l'autorisation de construire, d'aménager, ou de lotir exige, en tant que de besoin, du bénéficiaire de celle-ci la réalisation et le financement de tous travaux nécessaires à la viabilité et à l'équipement de la construction, du terrain aménagé ou du lotissement, notamment en ce qui concerne la voirie, l'alimentation en eau, gaz et électricité, les réseaux de télécommunication, l'évacuation et le traitement des eaux et matières usées, l'éclairage, les aires de stationnement, les espaces collectifs, les aires de jeux et les espaces plantés. / Les obligations imposées par l'alinéa ci-dessus s'étendent au branchement des équipements propres à l'opération sur les équipements publics qui existent au droit du terrain sur lequel ils sont implantés et notamment aux opérations réalisées à cet effet en empruntant des voies privées ou en usant de servitudes. (...) " ; <br/>
<br/>
              Considérant qu'il résulte de l'instruction qu'avant la réalisation des travaux prescrits par la commune, un fossé d'écoulement des eaux pluviales traversait les parcelles situées dans le périmètre du futur lotissement et permettait l'évacuation de ces eaux du bassin versant situé en amont ; que la situation géographique du terrain l'exposait en cas de forte pluie à des inondations provoquées par le débordement du fossé ; que la mise en place d'une canalisation sous le terrain et la suppression du fossé ont eu pour objet de permettre la constructibilité des parcelles comprises dans le terrain d'assiette du lotissement ; que, dès lors, alors même que la canalisation traversait seulement le terrain sans le desservir et qu'elle contribuait à l'évacuation des eaux pluviales en provenance de terrains situés en amont du projet, c'est à tort que le tribunal administratif de Nîmes a regardé ces travaux comme ne constituant pas un équipement propre au lotissement au sens des dispositions de l'article L. 332-6 du code de l'urbanisme ; que, par suite, alors que la société se borne, en défense en appel comme en demande en première instance, à soutenir que l'équipement en cause n'est pas un équipement propre et que le moyen tiré de la prescription de l'action en répétition de l'indu n'est pas fondé, la commune de Pujaut est fondée à soutenir que c'est à tort que, par le jugement attaqué, et sans qu'il soit besoin d'examiner la recevabilité de la demande de première instance, le tribunal administratif l'a condamnée à restituer à la SARL ANGLES HABITAT la somme de 42 433,42 euros, augmentée des intérêts légaux ; <br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la SARL ANGLES HABITAT la somme de 4 500 euros à verser à la commune de Pujaut, sur le fondement des dispositions de l'article L. 761-1 du code de justice administrative, au titre des frais exposés par elle tant en appel qu'en cassation ; que ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de la commune de Pujaut, qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : L'arrêt du 23 avril 2010 de la cour administrative d'appel de Marseille et le jugement du 7 décembre 2007 du tribunal administratif de Nîmes sont annulés.<br/>
<br/>
Article 2 : La demande de la SARL ANGLES HABITAT et le surplus des conclusions de son pourvoi sont rejetés.<br/>
<br/>
Article 3 : La SARL ANGLES HABITAT versera à la commune de Pujaut une somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la SARL ANGLES HABITAT et à la commune de Pujaut.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">68-02-04-02 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PROCÉDURES D'INTERVENTION FONCIÈRE. LOTISSEMENTS. AUTORISATION DE LOTIR. - EXIGENCE DE PARTICIPATION À LA RÉALISATION D'ÉQUIPEMENTS PUBLICS (ART. L. 332-6 ET SUIVANTS DU CODE DE L'URBANISME) - NOTION D'ÉQUIPEMENTS PROPRES (ART. L. 332-6 ET L. 332-15 DU MÊME CODE) - CANALISATION TRAVERSANT LE TERRAIN D'ASSIETTE D'UN LOTISSEMENT SANS LE SERVIR, MAIS MISE EN PLACE POUR PERMETTRE LA CONSTRUCTIBILITÉ DES PARCELLES - INCLUSION [RJ1].
</SCT>
<ANA ID="9A"> 68-02-04-02 Une canalisation mise en place pour assurer la constructibilité des parcelles, inondables, comprises dans le terrain d'assiette d'un lotissement constitue un équipement propre au lotissement au sens du 3° de l'article L. 332-6 et de l'article L. 332-15 du code de l'urbanisme, alors même qu'elle traverse le terrain d'assiette sans le desservir et qu'elle contribue à l'évacuation des eaux pluviales en provenance d'autres terrains situés en amont.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Cf. CE, 18 mars 1983, M. et Mme Plunian, n° 34130, p. 128.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
