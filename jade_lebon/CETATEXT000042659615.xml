<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042659615</ID>
<ANCIEN_ID>JG_L_2020_12_000000427616</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/65/96/CETATEXT000042659615.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 11/12/2020, 427616</TITRE>
<DATE_DEC>2020-12-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>427616</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>OCCHIPINTI</AVOCATS>
<RAPPORTEUR>M. Fabio Gennari</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie Roussel</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:427616.20201211</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Copra Méditerranée a demandé au tribunal administratif de Marseille de condamner la commune de Plan de Cuques à lui verser une indemnité de 6 763 680 euros toutes taxes comprises, avec intérêts au taux légal à compter du 9 décembre 2013, en réparation du préjudice subi du fait de la résiliation de la convention d'aménagement conclue le 13 février 1995 ou d'ordonner avant-dire droit, le cas échéant, une expertise afin d'évaluer son préjudice. Par un jugement n° 1402060 du 12 juillet 2017, le tribunal administratif de Marseille a rejeté sa demande. <br/>
<br/>
              Par un arrêt n° 17MA03902 du 26 novembre 2018, la cour administrative d'appel de Marseille a rejeté l'appel formé par la société Copra Méditerranée contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 3 février et 3 mai 2019 au secrétariat du contentieux du Conseil d'Etat, la société Copra Méditerranée demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt en tant qu'il a rejeté ses conclusions tendant à ce que soit engagée la responsabilité de la commune, en raison de la résiliation de la convention d'aménagement ;<br/>
<br/>
              2°) de mettre à la charge de la commune de Plan-de-Cuques la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative, l'ordonnance n° 2020-1402 du 18 novembre 2020 et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Fabio Gennari, auditeur,<br/>
<br/>
              - les conclusions de Mme Sophie Roussel, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à Me Occhipinti, avocat de la société Copra Méditerranée ; <br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1.	Il ressort des pièces du dossier soumis au juge du fond que la réalisation de la zone d'aménagement concerté (ZAC) dite " de SainteEuphémie " a été confiée à la société Euphémie, aux droits de laquelle est venue la société Copra Méditerranée, par une convention conclue le 13 février 1995 avec la commune de PlandeCuques. La convention prévoyait la réalisation de 94 logements en quatre tranches successives. La première des quatre tranches, correspondant à la zone D du programme, a été achevée en 2000. Les trois autres tranches, correspondant aux zones A, B et C, n'ont pas été réalisées.<br/>
<br/>
              2.	La société Copra Méditerranée a demandé au tribunal administratif de Marseille de condamner la commune de Plan-de-Cuques à l'indemniser du préjudice qu'elle estime avoir subi du fait de l'arrêt des travaux. Par un jugement du 12 juillet 2017, le tribunal administratif de Marseille a rejeté sa demande. Par un arrêt du 26 novembre 2018, la cour administrative d'appel de Marseille a rejeté l'appel formé par la société Copra Méditerranée contre ce jugement. La société requérante se pourvoit contre cet arrêt en tant qu'il a rejeté ses conclusions tendant à ce que soit engagée la responsabilité de la commune, en raison de la résiliation de la convention d'aménagement.<br/>
<br/>
              3.	En dehors du cas où elle est prononcée par le juge, la résiliation d'un contrat administratif résulte, en principe, d'une décision expresse de la personne publique cocontractante. Cependant, en l'absence de décision formelle de résiliation du contrat prise par la personne publique cocontractante, un contrat doit être regardé comme tacitement résilié lorsque, par son comportement, la personne publique doit être regardée comme ayant mis fin, de façon non équivoque, aux relations contractuelles. Les juges du fond apprécient souverainement, sous le seul contrôle d'une erreur de droit et d'une dénaturation des pièces du dossier par le juge de cassation, l'existence d'une résiliation tacite du contrat au vu de l'ensemble des circonstances de l'espèce, en particulier des démarches engagées par la personne publique pour satisfaire les besoins concernés par d'autres moyens, de la période durant laquelle la personne publique a cessé d'exécuter le contrat, compte tenu de sa durée et de son terme, ou encore de l'adoption d'une décision de la personne publique qui a pour effet de rendre impossible la poursuite de l'exécution du contrat ou de faire obstacle à l'exécution, par le cocontractant, de ses obligations contractuelles.<br/>
<br/>
              4.	Il ressort des énonciations de l'arrêt attaqué, d'une part, qu'aucun aménagement n'a eu lieu au sein de la ZAC dite " de SainteEuphémie " après l'achèvement, au cours de l'année 2000, de la première tranche du programme, correspondant à la zone D, une étude hydraulique réalisée postérieurement à la signature de la convention et confirmée au cours des années 2000 par plusieurs études complémentaires ayant mis en évidence l'existence d'un risque d'inondation des communes de Plan-de-Cuques, Marseille et Allauch, et, d'autre part, que, le 12 janvier 2012, la commune a informé la société, à la suite d'une demande de celle-ci relative à l'avancement du projet, de " l'arrêt de l'aménagement " pour le motif d'intérêt général représenté par le risque d'inondation, sans faire état d'une perspective de reprise de travaux ni de mesures envisagées afin de remédier au risque en cause. En jugeant, en dépit du contenu de ce courrier et de la durée durant laquelle la commune avait cessé d'exécuter le contrat, qu'aucune résiliation tacite de la convention d'aménagement conclue en vue de réaliser la zone d'aménagement concertée ne pouvait être caractérisée en l'espèce, la cour a dénaturé les pièces du dossier qui lui était soumis. Son arrêt doit, par suite, être annulé en tant qu'il statue sur la responsabilité de la commune du fait de la résiliation tacite de la convention pour un motif d'intérêt général.<br/>
<br/>
              5.	Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond dans la mesure de l'annulation prononcée, en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              6.	En vertu des règles générales applicables aux contrats administratifs, reprises sur ce point au 5° de l'article L. 6 du code de la commande publique, la personne publique cocontractante peut toujours, pour un motif d'intérêt général, résilier unilatéralement un tel contrat, sous réserve des droits éventuels à indemnité de son cocontractant.<br/>
<br/>
              7.	Aux termes de l'article 3 de la convention d'aménagement conclue entre la commune de Plan-de-Cuques et la société d'aménagement de Sainte-Euphémie en vue de l'aménagement de la zone d'aménagement concerté dite " de Sainte-Euphémie " : " L'aménagement et l'équipement de la zone pourront être exécutés en quatre tranches maximum, chacune d'elles comprenant les équipements publics de toute nature nécessaires aux constructions (...) ". En vertu des stipulations de l'article 9 de cette même convention : " L'aménageur et la commune devront chaque année, avant le 31 janvier : / a) arrêter ensemble le programme et l'échéancier des travaux (...) / b) décider, le cas échéant, le lancement d'une nouvelle tranche après avoir arrêté le programme et l'échéancier des travaux de cette tranche ; Toutefois le programme et l'échéancier des travaux de la première tranche sont annexés à la présente convention (...) / Le programme et le délai d'exécution des travaux des autres tranches sont donnés à titre indicatif et annexés à la présente convention (...) " ;  qu'aux termes également de son article 14, dans sa rédaction issue de l'avenant n° 1 à la convention d'aménagement, approuvé par délibération du 21 mai 1996 : " (...) La convention pourra être résolue à la demande d'une des deux parties pour ce qui concerne les tranches non encore engagées si, un an après la date fixée pour la fin d'une tranche par le dernier échéancier approuvé en application de l'article 9 ci-dessus, il n'a pas été arrêté un programme et un échéancier pour la tranche suivante. (...) / Toutefois, il est expressément convenu que le retard d'aménagement pour les tranches A - B - C résultant des dispositions techniques à prendre pour pallier le risque ci-dessus exposé, ne sera pas considéré comme un retard imputable à la commune ou à l'aménageur au sens des dispositions qui précèdent. Par ailleurs les travaux résultant de ces tranches décrits dans l'annexe II de la convention initiale ne pourront être engagés qu'une fois le risque évoqué définitivement réglé sur le plan technique, chacun des cotraitants ne pouvant demander à l'autre quoi que ce soit à ce titre ".<br/>
<br/>
              8.	Il résulte de ce qui a été dit plus haut que la convention a été tacitement résiliée par la commune pour un motif d'intérêt général après la réalisation de la première tranche des travaux correspondant à la zone D. Si cette résiliation est de nature à ouvrir droit à indemnité au bénéfice de la requérante, il résulte des stipulations précitées qu'en raison de la nécessité pour les parties de s'entendre expressément sur la poursuite du programme d'aménagement et la réalisation des tranches correspondant aux zones A, B et C, et alors, en tout état de cause, que les éléments produits par la société ne permettent pas d'établir que des dépenses auraient été engagées pour la réalisation de ces tranches, les préjudices invoqués par la société Copra Méditerranée résultant de leur non réalisation, y compris le manque à gagner, présentent un caractère purement éventuel. <br/>
<br/>
              9.	Il résulte de ce qui précède que la société n'est pas fondée à soutenir que c'est à tort que par le jugement attaqué, le tribunal administratif de Marseille a rejeté ses conclusions tendant à l'indemnisation des préjudices qu'elle aurait subis du fait de la résiliation tacite de la convention pour un motif d'intérêt général.<br/>
<br/>
              10.	Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la commune de Plan-de-Cuques.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Marseille du 26 novembre 2018 est annulé en tant qu'il statue sur la responsabilité de la commune du fait de la résiliation tacite de la convention pour un motif d'intérêt général.<br/>
<br/>
Article 2 : Les conclusions de la requête de la société Copra Méditerranée devant la cour administrative de Marseille tendant à l'indemnisation des préjudices qu'elle aurait subis du fait de la résiliation tacite de la convention pour un motif d'intérêt général ainsi que celles tendant à ce qu'une somme soit mise à la charge de la commune de Plan-de-Cuques au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 3 : La présente décision sera notifiée à la société Copra Méditerranée et à la commune de Plan-de-Cuques.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-04-02 MARCHÉS ET CONTRATS ADMINISTRATIFS. FIN DES CONTRATS. RÉSILIATION. - 1) FORME - A) PRINCIPE - DÉCISION EXPRESSE - B) EXCEPTION - COMPORTEMENT DE LA PERSONNE PUBLIQUE RÉVÉLANT QU'ELLE A MIS FIN AUX RELATIONS CONTRACTUELLES [RJ1] - 2) EXISTENCE D'UNE TELLE RÉSILIATION TACITE - A) APPRÉCIATION SOUVERAINE DES JUGES DU FOND - B) ELÉMENTS À PRENDRE EN CONSIDÉRATION.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-08-02-02-01-04 PROCÉDURE. VOIES DE RECOURS. CASSATION. CONTRÔLE DU JUGE DE CASSATION. BIEN-FONDÉ. DÉNATURATION. - EXISTENCE DE LA RÉSILIATION TACITE, PAR UNE PERSONNE PUBLIQUE, D'UN CONTRAT ADMINISTRATIF.
</SCT>
<ANA ID="9A"> 39-04-02 1) a) En dehors du cas où elle est prononcée par le juge, la résiliation d'un contrat administratif résulte, en principe, d'une décision expresse de la personne publique cocontractante.,,,b) Cependant, en l'absence de décision formelle de résiliation du contrat prise par la personne publique cocontractante, un contrat doit être regardé comme tacitement résilié lorsque, par son comportement, la personne publique doit être regardée comme ayant mis fin, de façon non équivoque, aux relations contractuelles.,,,2) a) Les juges du fond apprécient souverainement sous réserve de dénaturation l'existence d'une résiliation tacite du contrat b) au vu de l'ensemble des circonstances de l'espèce, en particulier des démarches engagées par la personne publique pour satisfaire les besoins concernés par d'autres moyens, de la période durant laquelle la personne publique a cessé d'exécuter le contrat, compte tenu de sa durée et de son terme, ou encore de l'adoption d'une décision de la personne publique qui a pour effet de rendre impossible la poursuite de l'exécution du contrat ou de faire obstacle à l'exécution, par le cocontractant, de ses obligations contractuelles.</ANA>
<ANA ID="9B"> 54-08-02-02-01-04 Les juges du fond apprécient souverainement, sous réserve de dénaturation, l'existence de la résiliation tacite d'un contrat administratif résultant du comportement de la personne publique.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 27 février 2019, Département de la Seine-Saint-Denis, n° 414114, pt. 5, aux Tables sur un autre point.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
