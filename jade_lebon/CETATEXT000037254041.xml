<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037254041</ID>
<ANCIEN_ID>JG_L_2018_07_000000418298</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/25/40/CETATEXT000037254041.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 5ème chambres réunies, 26/07/2018, 418298</TITRE>
<DATE_DEC>2018-07-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>418298</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MATUCHANSKY, POUPOT, VALDELIEVRE ; SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS</AVOCATS>
<RAPPORTEUR>Mme Laurence Franceschini</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Julie Burguburu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:418298.20180726</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Par un mémoire, enregistré le 6 juin 2018 au secrétariat du contentieux du Conseil d'Etat, présenté en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958, le conseil régional de l'ordre des architectes de Bretagne demande au Conseil d'Etat, à l'appui de son pourvoi contre l'arrêt n° 17NT00747 de la cour administrative d'appel de Nantes rejetant son appel contre le jugement n° 1403342 du tribunal administratif de Rennes en date du 30 décembre 2016 ayant rejeté sa demande tendant à l'annulation pour excès de pouvoir de l'arrêté du 22 mai 2014 par lequel le maire de Saint-Renan a délivré à M. B...A...un permis de construire, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des dispositions de l'article L. 600-1-2 du code de l'urbanisme.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              -	la Constitution, notamment son Préambule et son article 61-1 ;<br/>
              -	l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              -	le code de l'urbanisme ;<br/>
              -	la loi n° 77-2 du 3 janvier 1977 ;<br/>
              -	la loi n° 2014-366 du 24 mars 2014 ;<br/>
              -	le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Laurence Franceschini, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Julie Burguburu, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Matuchansky, Poupot, Valdelièvre, avocat du conseil régional de l'ordre des architectes de Bretagne et à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de la commune de Saint-Renan.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé, y compris pour la première fois en cassation, à l'occasion d'une instance devant le Conseil d'Etat (...) ". Il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              2. Aux termes de l'article L. 600-1-2 du code de l'urbanisme, créé par l'ordonnance du 18 juillet 2013 relative au contentieux de l'urbanisme, laquelle a été ratifiée par le 4° du IV de l'article 172 de la loi du 24 mars 2014 pour l'accès au logement et un urbanisme rénové : " Une personne autre que l'Etat, les collectivités territoriales ou leurs groupements ou une association n'est recevable à former un recours pour excès de pouvoir contre un permis de construire, de démolir ou d'aménager que si la construction, l'aménagement ou les travaux sont de nature à affecter directement les conditions d'occupation, d'utilisation ou de jouissance du bien qu'elle détient ou occupe régulièrement ou pour lequel elle bénéficie d'une promesse de vente, de bail, ou d'un contrat préliminaire mentionné à l'article L. 261-15 du code de la construction et de l'habitation ".<br/>
<br/>
              3. Le conseil régional de l'ordre des architectes de Bretagne soutient que ces dispositions, sur lesquelles s'est fondée la cour administrative d'appel pour juger qu'il ne justifiait pas d'un intérêt lui donnant qualité pour agir contre l'arrêté du maire de Saint-Renan ayant accordé un permis de construire à M.A..., méconnaissent le principe d'égalité des citoyens devant la loi ainsi que le droit des personnes intéressées d'exercer un recours effectif devant une juridiction, tels qu'ils sont garantis par les articles 6 et 16 de la Déclaration des droits de l'homme et du citoyen de 1789, en ce qu'elles feraient obstacle à ce que les instances régionales et nationales de l'ordre des architectes puissent déférer au juge un permis de construire délivré en méconnaissance de l'obligation de recourir à un architecte. <br/>
<br/>
              4. D'une part, l'article 3 de la loi du 3 janvier 1977 sur l'architecture dispose que : " Quiconque désire entreprendre des travaux soumis à une autorisation de construire doit faire appel à un architecte pour établir le projet architectural faisant l'objet de la demande de permis de construire ". Aux termes de l'article L. 431-1 du code de l'urbanisme : " (...) la demande de permis de construire ne peut être instruite que si la personne qui désire entreprendre des travaux soumis à une autorisation a fait appel à un architecte pour établir le projet architectural faisant l'objet de la demande de permis de construire ".<br/>
<br/>
              5. D'autre part, l'article 26 de la loi du 3 janvier 1977 sur l'architecture, dans sa rédaction issue de la loi du 17 mai 2011 de simplification et d'amélioration de la qualité du droit, dispose que : " Le conseil national et le conseil régional de l'ordre des architectes (...) ont qualité pour agir en justice en vue notamment de la protection du titre d'architecte et du respect des droits conférés et des obligations imposées aux architectes par les lois et règlements. En particulier, ils ont qualité pour agir sur toute question relative aux modalités d'exercice de la profession ainsi que pour assurer le respect de l'obligation de recourir à un architecte ".<br/>
<br/>
              6. Il résulte des dispositions qui viennent d'être citées qu'elles dérogent à la règle générale posée par l'article L. 600-1-2 du code de l'urbanisme en prévoyant que le conseil national et les conseils régionaux de l'ordre des architectes ont qualité pour agir contre un permis de construire délivré en méconnaissance de l'obligation de recourir à un architecte résultant de la loi. Par suite, le moyen tiré de ce que l'article L. 600-1-2 du code de l'urbanisme méconnaîtrait le principe d'égalité et le droit des personnes intéressées d'exercer un recours effectif devant une juridiction en ce qu'il priverait les instances régionales et nationales de l'ordre des architectes de la possibilité de saisir le juge d'un permis de construire délivré en méconnaissance de l'obligation de recourir à un architecte ne peut être regardé comme ayant un caractère sérieux.<br/>
<br/>
              7. Par suite, il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par le conseil régional de l'ordre des architectes de Bretagne, qui ne présente pas de caractère sérieux.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par le conseil régional des architectes de Bretagne. <br/>
<br/>
Article 2 : La présente décision sera notifiée au conseil régional des architectes de Bretagne et au ministre de la cohésion des territoires. <br/>
Copie en sera adressée au Conseil constitutionnel, au Premier ministre et à la garde des sceaux, ministre de la justice.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">09-01 ARTS ET LETTRES. ARCHITECTURE. - CONSEIL NATIONAL ET CONSEILS RÉGIONAUX DE L'ORDRE - INTÉRÊT POUR AGIR CONTRE UN PERMIS DE CONSTRUIRE DÉLIVRÉ EN MÉCONNAISSANCE DE L'OBLIGATION DE RECOURIR À UN ARCHITECTE - EXISTENCE (ART. 26 DE LA LOI N° 77-2 DU 3 JANVIER 1977, DÉROGEANT À L'ART. L. 600-1-2 DU CODE DE L'URBANISME).
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-01-04-02-01 PROCÉDURE. INTRODUCTION DE L'INSTANCE. INTÉRÊT POUR AGIR. EXISTENCE D'UN INTÉRÊT. INTÉRÊT LIÉ À UNE QUALITÉ PARTICULIÈRE. - CONSEIL NATIONAL ET CONSEILS RÉGIONAUX DE L'ORDRE DES ARCHITECTES - RECOURS CONTRE UN PERMIS DE CONSTRUIRE DÉLIVRÉ EN MÉCONNAISSANCE DE L'OBLIGATION DE RECOURIR À UN ARCHITECTE (ART. 26 DE LA LOI N° 77-2 DU 3 JANVIER 1977, DÉROGEANT À L'ART. L. 600-1-2 DU CODE DE L'URBANISME).
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">68-06-01-02 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. INTRODUCTION DE L'INSTANCE. INTÉRÊT À AGIR. - CONSEIL NATIONAL ET CONSEILS RÉGIONAUX DE L'ORDRE DES ARCHITECTES - RECOURS CONTRE UN PERMIS DE CONSTRUIRE DÉLIVRÉ EN MÉCONNAISSANCE DE L'OBLIGATION DE RECOURIR À UN ARCHITECTE (ART. 26 DE LA LOI N° 77-2 DU 3 JANVIER 1977, DÉROGEANT À L'ART. L. 600-1-2 DU CODE DE L'URBANISME).
</SCT>
<ANA ID="9A"> 09-01 Il résulte de l'article 26 de la loi n° 77-2 du 3 janvier 1977 sur l'architecture, dans sa rédaction issue de la loi n° 2011-525 du 17 mai 2011 de simplification et d'amélioration de la qualité du droit, que cet article déroge à la règle générale posée par l'article L. 600-1-2 du code de l'urbanisme en prévoyant que le conseil national et les conseils régionaux de l'ordre des architectes ont qualité pour agir contre un permis de construire délivré en méconnaissance de l'obligation de recourir à un architecte résultant de la loi.</ANA>
<ANA ID="9B"> 54-01-04-02-01 Il résulte de l'article 26 de la loi n° 77-2 du 3 janvier 1977 sur l'architecture, dans sa rédaction issue de la loi n° 2011-525 du 17 mai 2011 de simplification et d'amélioration de la qualité du droit, que cet article déroge à la règle générale posée par l'article L. 600-1-2 du code de l'urbanisme en prévoyant que le conseil national et les conseils régionaux de l'ordre des architectes ont qualité pour agir contre un permis de construire délivré en méconnaissance de l'obligation de recourir à un architecte résultant de la loi.</ANA>
<ANA ID="9C"> 68-06-01-02 Il résulte de l'article 26 de la loi n° 77-2 du 3 janvier 1977 sur l'architecture, dans sa rédaction issue de la loi n° 2011-525 du 17 mai 2011 de simplification et d'amélioration de la qualité du droit, que cet article déroge à la règle générale posée par l'article L. 600-1-2 du code de l'urbanisme en prévoyant que le conseil national et les conseils régionaux de l'ordre des architectes ont qualité pour agir contre un permis de construire délivré en méconnaissance de l'obligation de recourir à un architecte résultant de la loi.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
