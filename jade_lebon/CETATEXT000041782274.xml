<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041782274</ID>
<ANCIEN_ID>JG_L_2020_03_000000439674</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/78/22/CETATEXT000041782274.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, formation collégiale, 22/03/2020, 439674</TITRE>
<DATE_DEC>2020-03-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>439674</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés, formation collégiale</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MATUCHANSKY, POUPOT, VALDELIEVRE ; SCP THOUVENIN, COUDRAY, GREVY</AVOCATS>
<RAPPORTEUR>M. Nicolas Boulouis</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:439674.20200322</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 19 et 22 mars 2020 au secrétariat du contentieux du Conseil d'Etat, le syndicat Jeunes Médecins demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative : <br/>
<br/>
              1°) d'enjoindre au Premier ministre et au ministre des solidarités et de la santé de prononcer un confinement total de la population par la mise en place de mesures visant à : - l'interdiction totale de sortir de son lieu de confinement sauf autorisation délivrée par un médecin pour motif médical ; - l'arrêt des transports en commun ; - l'arrêt des activités professionnelles non vitales (alimentaire, eau et énergie, domaines régaliens) ; - l'instauration d'un ravitaillement de la population dans des conditions sanitaires visant à assurer la sécurité des personnels chargés de ce ravitaillement ;<br/>
<br/>
              2°) d'enjoindre au Premier Ministre et au ministre des solidarités et de la santé de prendre les mesures propres à assurer la production à échelle industrielle de tests de dépistage et de prendre les mesures réglementaires propres à assurer le dépistage des personnels médicaux. <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Il soutient que : <br/>
              - la condition d'urgence est remplie eu égard au caractère préoccupant de la situation française, à l'augmentation exponentielle du nombre de patients infectés par le Covid-19, aux déclarations du directeur général de la santé qui évoque un doublement des cas tous les jours et à la mention de l'urgence dans les visas du décret du 16 mars 2020 ;<br/>
              - il est porté une atteinte grave et manifestement illégale au droit au respect de la vie rappelé notamment par l'article 2 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - les Français, notamment les professionnels de santé, sont exposés au risque de propagation du virus du fait de l'insuffisance des mesures de confinement prononcées par le décret du 16 mars 2020 ;<br/>
              - les hôpitaux français risquent une saturation rapide de leurs services ;<br/>
              - le confinement total de la population est justifié face à la pandémie du Covid-19 dès lors que cette mesure constitue, en l'état de la lutte contre le virus, une stratégie thérapeutique qui fonctionne ;<br/>
              - il est nécessaire pour endiguer la progression du virus et permettre aux professionnels de santé de soigner les patients atteints dans les conditions les plus favorables possibles ;<br/>
              - la réalisation de tests de dépistage constitue une mesure nécessaire afin de dépister le plus grand nombre de citoyens et de limiter la propagation du virus.<br/>
<br/>
<br/>
              Par un mémoire en intervention et un nouveau mémoire, enregistrés les 21 et 22 mars 2020, l'InterSyndicale nationale des internes (" l'ISNI ") conclut à ce qu'il soit fait droit aux conclusions de la requête. L'ISNI soutient que :<br/>
              - son intervention est recevable ;<br/>
              - la condition d'urgence est remplie dès lors que, d'une part, la situation sanitaire de la France est proche de la saturation et, d'autre part, la propagation du virus se poursuit nonobstant les mesures prévues par le décret du 16 mars 2020 ;<br/>
              - l'insuffisance des mesures prévues par le décret du 16 mars 2020 porte une atteinte grave et manifestement illégale au droit au respect de la vie ;<br/>
              - il est nécessaire d'aplatir la courbe des personnes contaminées, d'une part, afin d'éviter de devoir recourir à la priorisation dans la délivrance des soins et, d'autre part, compte tenu de la pénurie de matériel ; <br/>
              - la carence de l'autorité publique dans la mise en oeuvre de mesures sanitaires est établie du fait de l'absence de mesures proactives et anticipées.<br/>
<br/>
              Par un mémoire en défense, enregistré le 21 mars 2020, le ministre des solidarités et de la santé conclut au rejet de la requête. Il soutient que ne peut être retenue aucune carence de l'autorité publique de nature à constituer une atteinte grave et manifestement illégale à une liberté fondamentale dès lors que les autorités administratives et sanitaires, tant nationales que locales, ont pris et continuent de prendre, compte tenu des connaissances et des projections scientifiques disponibles, les mesures appropriées et utiles pour éviter une saturation du système de santé et protéger les professionnels de santé comme l'ensemble de la population.<br/>
<br/>
              Par un mémoire en intervention, enregistré le 21 mars 2020, M. A... B... conclut, à titre principal, à ce qu'il soit fait droit aux conclusions de la requête et à ce que le Défenseur des droits soit invité à formuler des recommandations et, à titre subsidiaire, à ce que soit ordonné, d'une part, au Premier ministre de fournir, dans un délai de 48 heures à compter du prononcé de l'ordonnance, du gel hydroalcoolique et des masques pour l'ensemble des personnels médicaux et paramédicaux exerçant sur le territoire français, éventuellement sous astreinte et, d'autre part, toute mesure pour permettre un dépistage massif de la population française. Il soutient qu'il a intérêt à intervenir, que la condition d'urgence est remplie et qu'il est porté une atteinte grave et manifestement illégale au droit à la vie, au droit de mener une vie privée et familiale normale et au droit à la santé.<br/>
<br/>
              Par un mémoire en intervention, enregistré le 22 mars 2020, le Conseil national de l'Ordre des médecins conclut à ce qu'il soit fait droit aux conclusions de la requête. Il soutient qu'il a intérêt à intervenir et que les moyens de la requête sont fondés.<br/>
<br/>
              La requête a été communiquée au Premier ministre qui n'a pas produit d'observations.<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code de la santé publique ;<br/>
              - le décret n° 2020-260 du 16 mars 2020 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, le syndicat Jeunes Médecins, l'ISNI et le Conseil national de l'ordre des médecins, d'autre part, le Premier ministre et le ministre des solidarités et de la santé ; <br/>
<br/>
              Ont été entendus lors de l'audience publique du 22 mars 2020 à 11 heures : <br/>
<br/>
              - Me Coudray, avocat au Conseil d'Etat et à la Cour de cassation, avocat du syndicat Jeunes Médecins ;<br/>
<br/>
              - les représentants du syndicat Jeunes Médecins ;<br/>
<br/>
              - Me Poupot, avocat au Conseil d'Etat et à la Cour de cassation, avocat du Conseil national de l'Ordre des médecins ;<br/>
<br/>
              - les représentants de l'InterSyndicale nationale des internes ;<br/>
<br/>
              - les représentants du ministre des solidarités et de la santé ;<br/>
<br/>
              et à l'issue de laquelle l'instruction a été close.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
<br/>
              Sur les interventions <br/>
<br/>
              1. L'InterSyndicale Nationale des Internes (ISNI) et le Conseil National de l'Ordre des médecins (CNOM) justifient d'un intérêt suffisant pour intervenir au soutien de la requête du syndicat des jeunes médecins (SJM). Leur intervention est, par suite, recevable. Il en va de même de l'intervention de M. B.... <br/>
<br/>
              Sur le cadre juridique du litige :<br/>
<br/>
              2. D'une part, le Premier ministre peut, en vertu de ses pouvoirs propres, édicter des mesures de police applicables à l'ensemble du territoire, en particulier en cas de circonstances exceptionnelles, telle une épidémie avérée, comme celle de covid-19 que connaît actuellement la France. En outre, aux termes de l'article L. 3131-1 du code de la santé publique : " En cas de menace sanitaire grave appelant des mesures d'urgence, notamment en cas de menace d'épidémie, le ministre chargé de la santé peut, par arrêté motivé, prescrire dans l'intérêt de la santé publique toute mesure proportionnée aux risques courus et appropriée aux circonstances de temps et de lieu afin de prévenir et de limiter les conséquences des menaces possibles sur la santé de la population./Le ministre peut habiliter le représentant de l'Etat territorialement compétent à prendre toutes les mesures d'application de ces dispositions, y compris des mesures individuelles. (...). " Sur ces fondements ont été pris, le 16 mars 2020 un décret portant réglementation des déplacements dans le cadre de la lutte contre la propagation du virus covid-19 et à partir du 4 mars plusieurs arrêtés du ministre de la santé.  Enfin, le représentant de l'État dans le département et le maire disposent, dans les conditions et selon les modalités fixées en particulier par le code général des collectivités territoriales, du pouvoir d'adopter, dans le ressort du département ou de la commune, des mesures plus contraignantes permettant d'assurer la sûreté, la sécurité et la salubrité publiques, notamment en cas d'épidémie et compte tenu du contexte local. Par ailleurs, le Parlement a été saisi d'un projet de loi pour faire face à l'épidémie de covid-19 permettant l'instauration d'un état d'urgence sanitaire. <br/>
<br/>
              3. Dans cette situation, il appartient à ces différentes autorités de prendre, en vue de sauvegarder la santé de la population, toutes dispositions de nature à prévenir ou à limiter les effets de l'épidémie. Ces mesures, qui peuvent limiter l'exercice des droits et libertés fondamentaux, comme la liberté d'aller et venir, la liberté de réunion ou encore la liberté d'exercice d'une profession doivent, dans cette mesure, être nécessaires, adaptées et proportionnées à l'objectif de sauvegarde de la santé publique qu'elles poursuivent.  <br/>
<br/>
              4. D'autre part, aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ". <br/>
<br/>
              5. Le droit au respect de la vie, rappelé notamment par l'article 2 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, constitue une liberté fondamentale au sens des dispositions de l'article L. 521-2 du code de justice administrative. Lorsque l'action ou la carence de l'autorité publique crée un danger caractérisé et imminent pour la vie des personnes, portant ainsi une atteinte grave et manifestement illégale à cette liberté fondamentale, le juge des référés peut, au titre de la procédure particulière prévue par cet article, prescrire toutes les mesures de nature à faire cesser le danger résultant de cette action ou de cette carence. Toutefois, ce juge ne peut, au titre de cette procédure particulière, qu'ordonner les mesures d'urgence qui lui apparaissent de nature à sauvegarder, dans un délai de quarante-huit heures, la liberté fondamentale à laquelle il est porté une atteinte grave et manifestement illégale. Le caractère manifestement illégal de l'atteinte doit s'apprécier notamment en tenant compte des moyens dont dispose l'autorité administrative compétente et des mesures qu'elle a, dans ce cadre, déjà prises.<br/>
<br/>
              Sur les conclusions à fin d'injonction<br/>
<br/>
              6. Le syndicat des jeunes médecins soutient que les mesures de confinement ordonnées par le Premier ministre et le ministre de la santé afin de prévenir la propagation du covid-19 sont insuffisantes, en raison en particulier des exceptions qu'elles prévoient, font l'objet d'interprétations contradictoires et sont inégalement appliquées. La carence des autorités constitue ainsi, selon le requérant et les intervenants, une atteinte grave et manifestement illégale au droit à la vie et à la santé de la population, en particulier de l'ensemble des personnels soignants particulièrement exposés aux contaminations. Pour faire cesser cette atteinte, les intéressés demandent qu'il soit enjoint au Premier ministre et au ministre de la santé de décider l'interdiction totale de sortir de son lieu de confinement, sauf autorisation délivrée par un médecin pour motif médical, l'arrêt des transports en commun, l'arrêt des activités professionnelles non vitales et la mise en place d'un ravitaillement à domicile de la population dans des conditions sanitaires visant à assurer la sécurité des personnels chargés de ce ravitaillement. Il demande, en outre, que soient prises les mesures propres à assurer la production massive de tests de dépistage et permettre le dépistage de tous les professionnels de santé.  <br/>
<br/>
              En ce qui concerne les mesures de confinement total<br/>
<br/>
              7. Il résulte de l'instruction et des échanges qu'ainsi qu'il a été dit ci-dessus, le ministre de la santé a, par plusieurs arrêtés successifs, notamment interdit les rassemblements de plus de cent personnes, décidé la fermeture, sauf exceptions, des établissements recevant du public ainsi que des établissements d'accueil des enfants et des établissements d'enseignement scolaire et supérieur.   Le Premier ministre a, par le décret en date du 16 mars 2020, interdit jusqu'au 31 mars 2020 le déplacement de toute personne hors de son domicile, sous réserve d'exceptions limitatives, tenant à diverses nécessités, ainsi que tout regroupement avec la possibilité, pour le représentant de l'État dans le département d'adopter des mesures plus strictes si des circonstances locales l'exigent. Ce dispositif, régulièrement modifié, est susceptible d'être à nouveau adapté en fonction des circonstances, notamment, ainsi qu'il résulte des déclarations faites à l'audience, en fonction de l'avis que le conseil scientifique mis en place par le Gouvernement doit rendre lundi 23 mars sur la durée et l'étendue du confinement et pour la mise en oeuvre des dispositions législatives issues du projet de loi mentionné au point 2.  <br/>
<br/>
              8. Si un confinement total de la population dans certaines zones peut être envisagé, les mesures demandées au plan national ne peuvent, s'agissant en premier lieu du ravitaillement à domicile de la population, être adoptées, et organisées sur l'ensemble du territoire national, compte tenu des moyens dont l'administration dispose, sauf à risquer de graves ruptures d'approvisionnement qui seraient elles-mêmes dangereuses pour la protection de la vie et à retarder l'acheminement des matériels indispensables à cette protection. En outre, l'activité indispensable des personnels de santé ou aidants, des services de sécurité de l'exploitation des réseaux, ou encore des personnes participant à la production et à la distribution de l'alimentation rend nécessaire le maintien en fonctionnement, avec des cadences adaptées, des transports en commun, dont l'utilisation est restreinte aux occurrences énumérées par le décret du 16 mars 2020. Par ailleurs, la poursuite de ces diverses activités vitales dans des conditions de fonctionnement optimales est elle-même tributaire de l'activité d'autres secteurs ou professionnels qui directement ou indirectement leur sont indispensables, qu'il n'apparaît ainsi pas possible d'interrompre totalement. Par suite, il n'apparait pas que le Premier ministre ait fait preuve d'une carence grave et manifestement illégale en ne décidant pas un confinement total de la population sur l'ensemble du territoire selon les modalités demandées par le syndicat requérant. <br/>
<br/>
              En ce qui concerne le renforcement des mesures actuelles <br/>
<br/>
              9. En l'état actuel de l'épidémie, si l'économie générale des arrêtés ministériels et du décret du 16 mars 2020 ne révèle pas une telle carence, celle-ci est toutefois susceptible d'être caractérisée si leurs dispositions sont inexactement interprétées et leur non-respect inégalement ou insuffisamment sanctionné. <br/>
<br/>
              10. En premier lieu, les échanges ayant eu lieu au cours de l'audience font apparaitre l'ambiguïté de la portée de certaines dispositions, au regard en particulier de la teneur des messages d'alerte diffusés à la population.<br/>
<br/>
              11.  Il en va ainsi tout d'abord du 3° de l'article 1er du décret du 16 mars 2020 qui autorise, sans autre précision quant à leur degré d'urgence, les " déplacements pour motif de santé ".<br/>
<br/>
              12. La portée du 5° du même article qui permet les " déplacements brefs, à proximité du domicile, liés à l'activité physique individuelle des personnes, à l'exclusion de toute pratique sportive collective, et aux besoins des animaux de compagnie " apparait trop large, notamment en rendant possibles des pratiques sportives individuelles, telles le " jogging ". <br/>
<br/>
              13. Enfin, il en va de même du fonctionnement des marchés ouverts, sans autre limitation que l'interdiction des rassemblements de plus de cent personnes dont le maintien paraît autoriser dans certains cas des déplacements et des comportements contraires à la consigne générale.  <br/>
<br/>
              14. En deuxième lieu si le non-respect par la population des " gestes barrière " imposés par les autorités sanitaires et des interdictions de déplacement, alors qu'il appartient à chaque personne de contribuer ainsi à la non propagation du virus, ne saurait constituer une carence manifeste des pouvoirs publics, il appartient néanmoins à ces derniers de mettre en place les mesures d'organisation et de déploiement des forces de sécurité de nature à permettre de sanctionner sur l'ensemble du territoire les contrevenants aux arrêtés ministériels et au décret du 16 mars 2020. Il résulte, en outre, des déclarations faites à l'audience que des dispositions pénales plus sévères, pouvant aller jusqu'à des peines délictuelles, sont en cours d'adoption.  Il appartient également à ces mêmes autorités de s'assurer, dans les lieux recevant du public où continue de s'exercer une activité, du respect des " gestes barrière " et de la prise des mesures d'organisation indispensables.<br/>
<br/>
              15. En troisième lieu, dans le cadre du pouvoir qui leur a été reconnu par ce décret ou en vertu de leur pouvoir de police les représentants de l'Etat dans les départements comme les maires en vertu de leur pouvoir de police générale ont l'obligation d'adopter, lorsque de telles mesures seraient nécessaires des interdictions plus sévères lorsque les circonstances locales le justifient.   <br/>
<br/>
              16. Enfin, une information précise et claire du public sur les mesures prises et les sanctions encourues doit être régulièrement réitérée par l'ensemble des moyens à la disposition des autorités nationales et locales. <br/>
<br/>
              17. Il résulte de ce qui précède qu'il y a lieu d'enjoindre au Premier ministre et au ministre de la santé, de prendre dans les quarante-huit heures les mesures suivantes :<br/>
              - préciser la portée de la dérogation au confinement pour raison de santé ; <br/>
              - réexaminer le maintien de la dérogation pour " déplacements brefs à proximité du domicile " compte tenu des enjeux majeurs de santé publique et de la consigne de confinement ;<br/>
              - évaluer les risques pour la santé publique du maintien en fonctionnement des marchés ouverts, compte tenu de leur taille et de leur niveau de fréquentation.     <br/>
<br/>
              En ce qui concerne le dépistage<br/>
<br/>
              18. Il résulte des déclarations du ministre de la santé et de celles faites à l'audience d'une part que les autorités ont pris les dispositions avec l'ensemble des industriels en France et à l'étranger pour augmenter les capacités de tests dans les meilleurs délais, d'autre part que la limitation, à ce jour, des tests aux seuls personnels de santé présentant des symptômes du virus résulte, à ce jour, d'une insuffisante disponibilité des matériels. Les conclusions de la demande ne peuvent, par suite, sur ce point, eu égard aux pouvoirs que le juge des référés tient des dispositions de l'article L. 521-2 du code de justice administrative, qu'être rejetées.<br/>
<br/>
<br/>
<br/>ORDONNE :<br/>
--------------<br/>
Article 1er : Les interventions de l'InterSyndicale Nationale des Internes, du Conseil National de l'Ordre des médecins et de M. B... sont admises.<br/>
<br/>
Article 2 :   Il est enjoint au Premier ministre et au ministre de la santé, de prendre dans les quarante-huit heures les mesures suivantes :<br/>
            - préciser la portée de la dérogation au confinement pour raison de santé ; <br/>
            - réexaminer le maintien de la dérogation pour " déplacements brefs, à proximité du domicile " compte tenu des enjeux majeurs de santé publique et de la consigne de confinement ;<br/>
            - évaluer les risques pour la santé publique du maintien en fonctionnement des marchés ouverts, compte tenu de leur taille et de leur niveau de fréquentation. <br/>
<br/>
		Article 3 : Le surplus des conclusions de la requête est rejeté <br/>
<br/>
Article 4 : La présente ordonnance sera notifiée au syndicat Jeunes Médecins, à l'InterSyndicale nationale des internes, au Conseil national de l'ordre des médecins, à M. A... B..., au ministre des solidarités et de la santé et au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-035-03-03-01-02 PROCÉDURE. PROCÉDURES INSTITUÉES PAR LA LOI DU 30 JUIN 2000. RÉFÉRÉ TENDANT AU PRONONCÉ DE MESURES NÉCESSAIRES À LA SAUVEGARDE D'UNE LIBERTÉ FONDAMENTALE (ART. L. 521-2 DU CODE DE JUSTICE ADMINISTRATIVE). CONDITIONS D'OCTROI DE LA MESURE DEMANDÉE. ATTEINTE GRAVE ET MANIFESTEMENT ILLÉGALE À UNE LIBERTÉ FONDAMENTALE. ATTEINTE GRAVE ET MANIFESTEMENT ILLÉGALE. - CAS D'ÉPIDÉMIE AVÉRÉE - ATTEINTE GRAVE ET MANIFESTEMENT ILLÉGALE AUX DROITS AU RESPECT DE LA VIE ET À LA SANTÉ DE LA POPULATION - 1) ABSENCE D'ÉDICTION DE MESURES VISANT À LA MISE EN PLACE D'UN CONFINEMENT TOTAL DE LA POPULATION -- ABSENCE, AU REGARD DES MOYENS DONT DISPOSE L'ADMINISTRATION ET DE LA NÉCESSITÉ DE MAINTENIR CERTAINES ACTIVITÉS VITALES - 2) MESURES VISANT À RESTREINDRE LES DÉPLACEMENTS - A) EXISTENCE SI CES MESURES SONT INEXACTEMENT INTERPRÉTÉES ET LEUR NON-RESPECT INSUFFISAMMENT SANCTIONNÉ - B) CONSÉQUENCE - INJONCTIONS DE PRÉCISER, RÉEXAMINER OU ÉVALUER CERTAINES MESURES.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-035-03-04-01 PROCÉDURE. PROCÉDURES INSTITUÉES PAR LA LOI DU 30 JUIN 2000. RÉFÉRÉ TENDANT AU PRONONCÉ DE MESURES NÉCESSAIRES À LA SAUVEGARDE D'UNE LIBERTÉ FONDAMENTALE (ART. L. 521-2 DU CODE DE JUSTICE ADMINISTRATIVE). POUVOIRS ET DEVOIRS DU JUGE. MESURES SUSCEPTIBLES D'ÊTRE ORDONNÉES PAR LE JUGE DES RÉFÉRÉS. - CAS D'ÉPIDÉMIE AVÉRÉE - EDICTION DE MESURES VISANT À RESTREINDRE LES DÉPLACEMENTS - ATTEINTE GRAVE ET MANIFESTEMENT ILLÉGALE AUX DROITS AU RESPECT DE LA VIE ET À LA SANTÉ DE LA POPULATION - EXISTENCE SI CES MESURES SONT INEXACTEMENT INTERPRÉTÉES ET LEUR NON-RESPECT INSUFFISAMMENT SANCTIONNÉ - CONSÉQUENCE - INJONCTIONS DE PRÉCISER, RÉEXAMINER OU ÉVALUER CERTAINES MESURES.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">61-01-01-02 SANTÉ PUBLIQUE. PROTECTION GÉNÉRALE DE LA SANTÉ PUBLIQUE. POLICE ET RÉGLEMENTATION SANITAIRE. LUTTE CONTRE LES ÉPIDÉMIES. - CAS D'ÉPIDÉMIE AVÉRÉE - 1) COMPÉTENCE DES AUTORITÉS DE L'ETAT POUR PRENDRE DES MESURES DE NATURE À PRÉVENIR OU À LIMITER LES EFFETS DE L'ÉPIDÉMIE - A) COMPÉTENCE DU PM EN VERTU DE SES POUVOIRS PROPRES [RJ1], EN PARTICULIER EN CAS DE CIRCONSTANCES EXCEPTIONNELLES - B) COMPÉTENCE DU MINISTRE CHARGÉ DE LA SANTÉ SUR LE FONDEMENT DE L'ARTICLE L. 3131-1 DU CSP - 2) EXIGENCE DE PROPORTIONNALITÉ DE CES MESURES.
</SCT>
<ANA ID="9A"> 54-035-03-03-01-02 Requérant soutenant que les mesures de confinement ordonnées par le Premier ministre et le ministre de la santé afin de prévenir la propagation du covid-19 sont insuffisantes et que cette carence des autorités constitue ainsi une atteinte grave et manifestement illégale au droit à la vie et à la santé de la population, en particulier de l'ensemble des personnels soignants particulièrement exposés aux contaminations. Requérant demandant, pour faire cesser cette atteinte, qu'il soit enjoint au Premier ministre et au ministre de la santé de décider l'interdiction totale de sortir de son lieu de confinement, sauf autorisation délivrée par un médecin pour motif médical, l'arrêt des transports en commun, l'arrêt des activités professionnelles non vitales et la mise en place d'un ravitaillement à domicile de la population dans des conditions sanitaires visant à assurer la sécurité des personnels chargés de ce ravitaillement.,,,1) Si un confinement total de la population dans certaines zones peut être envisagé, les mesures demandées au plan national ne peuvent, s'agissant en premier lieu du ravitaillement à domicile de la population, être adoptées, et organisées sur l'ensemble du territoire national, compte tenu des moyens dont l'administration dispose, sauf à risquer de graves ruptures d'approvisionnement qui seraient elles-mêmes dangereuses pour la protection de la vie et à retarder l'acheminement des matériels indispensables à cette protection. En outre, l'activité indispensable des personnels de santé ou aidants, des services de sécurité de l'exploitation des réseaux, ou encore des personnes participant à la production et à la distribution de l'alimentation rend nécessaire le maintien en fonctionnement, avec des cadences adaptées, des transports en commun, dont l'utilisation est restreinte aux occurrences énumérées par le décret du 16 mars 2020. Par ailleurs, la poursuite de ces diverses activités vitales dans des conditions de fonctionnement optimales est elle-même tributaire de l'activité d'autres secteurs ou professionnels qui directement ou indirectement leur sont indispensables, qu'il n'apparaît ainsi pas possible d'interrompre totalement. Par suite, il n'apparait pas que le Premier ministre ait fait preuve d'une carence grave et manifestement illégale en ne décidant pas un confinement total de la population sur l'ensemble du territoire selon les modalités demandées par le syndicat requérant.... ,,2) a) En l'état actuel de l'épidémie, si l'économie générale des différents arrêtés par lesquels le ministre de la santé a interdit les rassemblements de plus de cent personnes, décidé la fermeture, sauf exceptions, des établissements recevant du public ainsi que des établissements d'accueil des enfants et des établissements d'enseignement scolaire et supérieur, et du décret n° 2020-260 du 16 mars 2020 par lequel le Premier ministre a interdit jusqu'au 31 mars 2020 le déplacement de toute personne hors de son domicile, sous réserve d'exceptions limitatives, tenant à diverses nécessités, ainsi que tout regroupement avec la possibilité, pour le représentant de l'État dans le département d'adopter des mesures plus strictes si des circonstances locales l'exigent, ne révèle pas une telle carence, celle-ci est toutefois susceptible d'être caractérisée si leurs dispositions sont inexactement interprétées et leur non-respect inégalement ou insuffisamment sanctionné....  ,,Les échanges ayant eu lieu au cours de l'audience font apparaitre l'ambiguïté de la portée de certaines dispositions, au regard en particulier de la teneur des messages d'alerte diffusés à la population.,,,Il en va ainsi tout d'abord du 3° de l'article 1er du décret du 16 mars 2020 qui autorise, sans autre précision quant à leur degré d'urgence, les déplacements pour motif de santé.... ,,La portée du 5° du même article qui permet les déplacements brefs, à proximité du domicile, liés à l'activité physique individuelle des personnes, à l'exclusion de toute pratique sportive collective, et aux besoins des animaux de compagnie apparait trop large, notamment en rendant possibles des pratiques sportives individuelles, telles le jogging.... ,,Enfin, il en va de même du fonctionnement des marchés ouverts, sans autre limitation que l'interdiction des rassemblements de plus de cent personnes dont le maintien paraît autoriser dans certains cas des déplacements et des comportements contraires à la consigne générale....   ,,b) Il résulte de ce qui précède qu'il y a lieu d'enjoindre au Premier ministre et au ministre de la santé, de prendre dans les quarante-huit heures les mesures suivantes :,,- préciser la portée de la dérogation au confinement pour raison de santé ;... ...- réexaminer le maintien de la dérogation pour déplacements brefs à proximité du domicile compte tenu des enjeux majeurs de santé publique et de la consigne de confinement ;,,- évaluer les risques pour la santé publique du maintien en fonctionnement des marchés ouverts, compte tenu de leur taille et de leur niveau de fréquentation.</ANA>
<ANA ID="9B"> 54-035-03-04-01 En l'état actuel de l'épidémie de Covid-19, si l'économie générale des différents arrêtés par lesquels le ministre de la santé a interdit les rassemblements de plus de cent personnes, décidé la fermeture, sauf exceptions, des établissements recevant du public ainsi que des établissements d'accueil des enfants et des établissements d'enseignement scolaire et supérieur, et du décret n° 2020-260 du 16 mars 2020 par lequel le Premier ministre a interdit jusqu'au 31 mars 2020 le déplacement de toute personne hors de son domicile, sous réserve d'exceptions limitatives, tenant à diverses nécessités, ainsi que tout regroupement avec la possibilité, pour le représentant de l'État dans le département d'adopter des mesures plus strictes si des circonstances locales l'exigent, ne révèle pas une telle carence, celle-ci est toutefois susceptible d'être caractérisée si leurs dispositions sont inexactement interprétées et leur non-respect inégalement ou insuffisamment sanctionné....  ,,Les échanges ayant eu lieu au cours de l'audience font apparaitre l'ambiguïté de la portée de certaines dispositions, au regard en particulier de la teneur des messages d'alerte diffusés à la population.,,,Il en va ainsi tout d'abord du 3° de l'article 1er du décret du 16 mars 2020 qui autorise, sans autre précision quant à leur degré d'urgence, les déplacements pour motif de santé.... ,,La portée du 5° du même article qui permet les déplacements brefs, à proximité du domicile, liés à l'activité physique individuelle des personnes, à l'exclusion de toute pratique sportive collective, et aux besoins des animaux de compagnie apparait trop large, notamment en rendant possibles des pratiques sportives individuelles, telles le jogging.... ,,Enfin, il en va de même du fonctionnement des marchés ouverts, sans autre limitation que l'interdiction des rassemblements de plus de cent personnes dont le maintien paraît autoriser dans certains cas des déplacements et des comportements contraires à la consigne générale....   ,,Il résulte de ce qui précède qu'il y a lieu d'enjoindre au Premier ministre et au ministre de la santé, de prendre dans les quarante-huit heures les mesures suivantes :,,- préciser la portée de la dérogation au confinement pour raison de santé ;... ...- réexaminer le maintien de la dérogation pour déplacements brefs à proximité du domicile compte tenu des enjeux majeurs de santé publique et de la consigne de confinement ;,,- évaluer les risques pour la santé publique du maintien en fonctionnement des marchés ouverts, compte tenu de leur taille et de leur niveau de fréquentation.</ANA>
<ANA ID="9C"> 61-01-01-02 1) a) Le Premier ministre peut, en vertu de ses pouvoirs propres, édicter des mesures de police applicables à l'ensemble du territoire, en particulier en cas de circonstances exceptionnelles, telle une épidémie avérée.... ,,b) En outre, en vertu de l'article L. 3131-1 du code de la santé publique (CSP), le ministre chargé de la santé peut, dans un tel cas, prescrire toute mesure proportionnée aux risques courus et appropriée aux circonstances de temps et de lieu afin de prévenir et de limiter les conséquences des menaces possibles sur la santé de la population (&#133;).,,,Dans cette situation, il appartient à ces autorités de prendre, en vue de sauvegarder la santé de la population, toutes dispositions de nature à prévenir ou à limiter les effets de l'épidémie.... ,,2) Ces mesures, qui peuvent limiter l'exercice des droits et libertés fondamentaux, comme la liberté d'aller et venir, la liberté de réunion ou encore la liberté d'exercice d'une profession doivent, dans cette mesure, être nécessaires, adaptées et proportionnées à l'objectif de sauvegarde de la santé publique qu'elles poursuivent.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE,  8 août 1919, Labonne, n° 56377, p. 737.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
