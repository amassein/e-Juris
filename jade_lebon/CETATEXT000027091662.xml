<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027091662</ID>
<ANCIEN_ID>JG_L_2013_02_000000363656</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/09/16/CETATEXT000027091662.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème / 2ème SSR, 20/02/2013, 363656</TITRE>
<DATE_DEC>2013-02-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>363656</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème / 2ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARTHELEMY, MATUCHANSKY, VEXLIARD</AVOCATS>
<RAPPORTEUR>M. Frédéric Dieu</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Bertrand Dacosta</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2013:363656.20130220</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 31 octobre et 15 novembre 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour la société Laboratoire Biomnis, dont le siège est 17/19 avenue Tony Garnier BP 7322 à Lyon Cedex 07 (69357) ; la société Laboratoire Biomnis demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 1217195 du 17 octobre 2012 par laquelle le juge des référés du tribunal administratif de Paris, statuant en application de l'article L. 551-1 du code de justice administrative sur la demande de l'Institut de Génétique Nantes Atlantique (IGNA), a annulé en totalité la procédure de passation du marché de prestations d'analyses de prélèvements biologiques effectués sur les individus en vue de l'enregistrement de leur profil génétique dans le fichier national automatisé des empreintes génétiques ; <br/>
<br/>
              2°) statuant en référé, de rejeter la demande de l'IGNA ; <br/>
<br/>
              3°) de mettre à la charge de l'IGNA le versement de la somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code des marchés publics ;<br/>
<br/>
              Vu le décret n° 97-109 du 6 février 1997 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Dieu, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Barthélemy, Matuchansky, Vexliard, avocat de la société Laboratoire Biomnis,<br/>
<br/>
              - les conclusions de M. Bertrand Dacosta, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Barthélemy, Matuchansky, Vexliard, avocat de la société Laboratoire Biomnis ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 551-1 du code de justice administrative : " Le président du tribunal administratif, ou le magistrat qu'il délègue, peut être saisi en cas de manquement aux obligations de publicité et de mise en concurrence auxquelles est soumise la passation par les pouvoirs adjudicateurs de contrats administratifs ayant pour objet l'exécution de travaux, la livraison de fournitures ou la prestation de services, avec une contrepartie économique constituée par un prix ou un droit d'exploitation, ou la délégation d'un service public (...) " ; qu'aux termes de l'article L. 551-2 de ce code : " I. Le juge peut ordonner à l'auteur du manquement de se conformer à ses obligations et suspendre l'exécution de toute décision qui se rapporte à la passation du contrat, sauf s'il estime, en considération de l'ensemble des intérêts susceptibles d'être lésés et notamment de l'intérêt public, que les conséquences négatives de ces mesures pourraient l'emporter sur leurs avantages. (...) " ; que, selon l'article L. 551-10 du même code : " Les personnes habilitées à engager les recours prévus aux articles L. 551-1 et L. 551-5 sont celles qui ont un intérêt à conclure le contrat et qui sont susceptibles d'être lésées par le manquement invoqué (...) " ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis au juge des référés du tribunal administratif de Paris que, par un avis du 8 juin 2012, le ministère de la justice a informé les treize laboratoires bénéficiant de l'agrément prévu par le décret du 6 février 1997 relatif aux conditions d'agrément des personnes habilitées à procéder à des identifications par empreintes génétiques dans le cadre d'une procédure judiciaire ou de la procédure extrajudiciaire d'identification des personnes décédées, du lancement d'une consultation, selon la procédure adaptée prévue par les articles 28 et 30 du code des marchés publics, en vue de la passation d'un marché ayant pour objet l'analyse de prélèvements biologiques effectués sur les individus aux fins d'enregistrement de leur profil génétique dans le Fichier national automatisé des empreintes génétiques (FNAEG) ; que le marché a été scindé en trois lots géographiques, les soumissionnaires ne pouvant, selon le règlement particulier de la consultation, se voir attribuer plus d'un lot ; qu'à l'issue de l'examen des offres, le lot n° 1, seul lot pour lequel l'Institut de Génétique Nantes Atlantique (IGNA) a déposé une offre, a été attribué à l'Institut national de la police scientifique tandis que le lot n° 3 a été attribué à la société Laboratoire Biomnis et que la procédure de passation du lot n° 2 a été déclarée sans suite ; que, par l'ordonnance attaquée, le juge des référés du tribunal administratif de Paris, statuant en application des articles L. 551-1 et suivants du code de justice administrative, a annulé l'ensemble de la procédure de passation du marché ;<br/>
<br/>
              3. Considérant qu'aux termes de l'article 5 du code des marchés publics : <br/>
" I. - La nature et l'étendue des besoins à satisfaire sont déterminées avec précision avant tout appel à la concurrence ou toute négociation non précédée d'un appel à la concurrence en prenant en compte des objectifs de développement durable. Le ou les marchés ou accords-cadres conclus par le pouvoir adjudicateur ont pour objet exclusif de répondre à ces besoins. II. - Le pouvoir adjudicateur détermine le niveau auquel les besoins sont évalués. Ce choix ne doit pas avoir pour effet de soustraire des marchés aux règles qui leur sont normalement applicables en vertu du présent code " ; qu'aux termes de l'article 10 du même code : " Afin de susciter la plus large concurrence, et sauf si l'objet du marché ne permet pas l'identification de prestations distinctes, le pouvoir adjudicateur passe le marché en lots séparés dans les conditions prévues par le III de l'article 27. A cette fin, il choisit librement le nombre de lots, en tenant notamment compte des caractéristiques techniques des prestations demandées, de la structure du secteur économique en cause et, le cas échéant, des règles applicables à certaines professions. Les candidatures et les offres sont examinées lot par lot. Les candidats ne peuvent présenter des offres variables selon le nombre de lots susceptibles d'être obtenus. Si plusieurs lots sont attribués à un même titulaire, il est toutefois possible de ne signer avec ce titulaire qu'un seul marché regroupant tous ces lots. (...) " ; <br/>
<br/>
              4. Considérant que, dans le cadre de ces dispositions et sans méconnaître aucune autre règle ni aucun principe issus du code des marchés publics, le pouvoir adjudicateur qui recourt à l'allotissement peut décider, afin de mieux assurer la satisfaction de ses besoins en s'adressant à une pluralité de cocontractants ou de favoriser l'émergence d'une plus grande concurrence, de limiter le nombre de lots qui pourra être attribué à chaque candidat, dès lors que ce nombre est indiqué dans les documents de la consultation ; que, dans l'hypothèse où le pouvoir adjudicateur autorise la présentation d'une candidature pour un nombre de lots supérieur à celui pouvant être attribué à un même candidat, les documents de la consultation doivent en outre indiquer les modalités d'attribution des lots, en les fondant sur des critères ou règles objectifs et non discriminatoires, lorsque l'application des critères de jugement des offres figurant dans ces mêmes documents conduirait à classer premier un candidat pour un nombre de lots supérieur au nombre de lots pouvant lui être attribués ; que lorsqu'il décide ainsi de limiter le nombre de lots qui pourra être attribué à chaque candidat, le pouvoir adjudicateur n'adopte pas un critère de jugement des offres au sens des dispositions de l'article 53 du code des marchés publics mais définit, dans le cadre de l'article 10 du code des marchés publics relatif à l'allotissement, les modalités d'attribution des lots du marché ;<br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède qu'en estimant que la limitation à un seul du nombre de lots susceptibles d'être attribués à chaque candidat devait être regardée comme un critère de jugement des offres soumis aux conditions prévues par l'article 53 du code des marchés publics, le juge des référés du tribunal administratif de Paris a commis une erreur de droit ; que, par suite et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, l'ordonnance attaquée doit être annulée ;<br/>
<br/>
              6. Considérant qu'il y a lieu, en application de l'article L. 821-2 du code de justice administrative, de régler l'affaire au titre de la procédure de référé engagée par l'Institut de Génétique Nantes Atlantique (IGNA) ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Sur les conclusions à fin d'annulation de la procédure d'attribution du lot n° 1 :<br/>
<br/>
              7. Considérant, en premier lieu, qu'aux termes du I de l'article 80 du code des marchés publics : " 1° Pour les marchés et accords-cadres passés selon une procédure formalisée autre que celle prévue au II de l'article 35, le pouvoir adjudicateur, dès qu'il a fait son choix pour une candidature ou une offre, notifie à tous les autres candidats le rejet de leur candidature ou de leur offre, en leur indiquant les motifs de ce rejet. / Cette notification précise le nom de l'attributaire et les motifs qui ont conduit au choix de son offre aux candidats ayant soumis une offre et à ceux n'ayant pas encore eu communication du rejet de leur candidature " ; qu'aux termes de l'article 83 du même code : " Le pouvoir adjudicateur communique à tout candidat écarté qui n'a pas été destinataire de la notification prévue au 1° du I de l'article 80 les motifs du rejet de sa candidature ou de son offre dans les quinze jours de la réception d'une demande écrite à cette fin. / Si le candidat a vu son offre écartée alors qu'elle n'était aux termes de l'article 35 ni inappropriée, ni irrégulière, ni inacceptable, le pouvoir adjudicateur est en outre tenu de lui communiquer les caractéristiques et les avantages relatifs de l'offre retenue ainsi que le nom du ou des attributaires du marché ou de l'accord-cadre " ;<br/>
<br/>
              8. Considérant que l'IGNA ne peut utilement se prévaloir des dispositions précitées du I-1° de l'article 80 du code des marchés publics relatives à la notification du rejet des candidatures et des offres, lesquelles ne sont pas applicables à un marché passé selon la procédure adaptée prévue par les articles 28 et 30 du code des marchés publics ; que l'absence de communication par le pouvoir adjudicateur de l'une des informations mentionnées par le deuxième alinéa des dispositions précitées de l'article 83 du code des marchés publics doit conduire le juge à enjoindre à ce dernier de communiquer les informations manquantes au candidat dont l'offre, bien que recevable, a été rejetée ; que cependant, il résulte de l'instruction que l'IGNA s'est vu communiquer par le ministère de la justice, par un courrier daté du 25 septembre 2012, le détail de la notation de son offre, le nom du candidat retenu, le montant de son offre et les notes qui lui ont été attribuées ainsi que des éléments de comparaison entre les deux offres ; que, par suite, l'IGNA n'est pas fondé à soutenir qu'il n'a pas été suffisamment informé des motifs du rejet de son offre et qu'ont été méconnues les dispositions de l'article 83 du code des marchés publics ;<br/>
<br/>
              9. Considérant, en second lieu, qu'il ne résulte pas davantage de l'instruction que le montant de l'offre de l'Institut national de police scientifique, de 1 109 947 euros, bien qu'il soit inférieur à l'estimation préalable du coût des prestations pour le lot n° 1, lequel s'élève selon le ministre à 1 356 264 euros, et qu'il soit sensiblement moins élevé que le montant de l'offre de l'IGNA, n'aurait pas été déterminé en prenant en compte l'ensemble des coûts directs et indirects de fourniture des prestations objet du contrat et que l'Institut national de police scientifique aurait ainsi bénéficié, pour présenter son offre, d'un avantage résultant des ressources ou des moyens qui lui sont attribués au titre de sa mission de service public ; que par suite, l'IGNA n'est pas fondé à soutenir qu'en ne vérifiant pas si le prix proposé par l'Institut national de police scientifique prenait en compte l'ensemble de ces coûts et en ne rejetant pas son offre comme anormalement basse, le ministre a méconnu le principe d'égalité entre les candidats et entaché sa décision d'une erreur manifeste d'appréciation ;<br/>
<br/>
<br/>
<br/>
<br/>
              Sur les autres conclusions à fin d'annulation :<br/>
<br/>
              10. Considérant, d'une part, qu'un candidat n'ayant pas présenté sa candidature pour l'attribution de l'ensemble des lots d'un marché, n'est, en principe, recevable à demander au juge du référé précontractuel que l'annulation de la procédure d'attribution du ou des lots pour lesquels il s'est porté candidat ; que cependant, l'IGNA, bien qu'ayant présenté une offre pour le seul lot n° 1, est recevable à demander l'annulation de la procédure de passation des autres lots de ce marché dès lors qu'en vertu des dispositions du règlement de la consultation interdisant à tout candidat de se voir attribuer plus d'un des trois lots du marché, il doit être regardé comme ayant été privé de la possibilité de présenter utilement sa candidature à l'ensemble des lots du marché ; que, toutefois, la procédure de passation du lot n° 2 ayant été déclarée sans suite avant la saisine du juge des référés, les conclusions de l'IGNA tendant à l'annulation de cette procédure sont irrecevables et doivent être rejetées ;<br/>
<br/>
              11. Considérant, d'autre part, qu'il résulte de l'instruction que le marché litigieux est divisé en trois lots géographiques correspondant à trois regroupements des ressorts de l'ensemble des cours d'appel du territoire national ; que l'article 3-2 du règlement particulier de la consultation prévoit que " Les soumissionnaires ont la faculté de répondre sur un, deux ou trois lots " tout en indiquant qu'il ne " sera attribué qu'un seul lot par soumissionnaire " ; que l'article 7 du même règlement précise que " l'attribution des lots se fera dans l'ordre d'importance de la volumétrie (lot n° 1, lot n° 3 et lot n° 2) " ;<br/>
<br/>
              12. Considérant qu'ainsi qu'il a été dit, les dispositions de l'article 53 du code des marchés publics ne faisaient pas obstacle à ce que le ministère de la justice décide, dans le cadre des dispositions précitées des articles 5 et 10 du code des marchés publics, de limiter à un seul le nombre de lots pouvant être attribué à chaque candidat ; que cette limitation a été portée à la connaissance des candidats dans le règlement particulier de la consultation ; qu'il résulte de l'instruction qu'une telle limitation vise à susciter l'émergence d'une plus grande concurrence dans le secteur des prestations d'identification par empreintes génétiques réalisées dans le cadre d'une procédure judiciaire ou extrajudiciaire et à assurer la sécurité des approvisionnements du ministère de la justice dans ce domaine, en permettant à plusieurs entreprises de disposer d'une compétence dans ce secteur afin que l'Etat puisse durablement être confronté à plusieurs opérateurs ; qu'elle est ainsi justifiée par la nature et l'étendue des besoins auquel le marché a pour objet de répondre ; qu'en prévoyant en outre dans le règlement de la consultation que, dans l'hypothèse où un candidat serait classé premier pour plusieurs lots différents tout en ne pouvant s'en voir attribuer qu'un seul, il lui serait attribué le lot le plus important, et en classant les trois lots du marché par ordre d'importance (lot n° 1 puis lot n° 3 et lot n° 2), le ministre de la justice a retenu une règle d'attribution des lots objective et non discriminatoire ; que, par suite, l'IGNA n'est pas fondé à soutenir qu'en limitant le nombre de lots susceptibles d'être attribués à chaque candidat et en retenant de telles modalités d'attribution des lots du marché, le ministre de la justice a manqué à ses obligations de publicité et de mise en concurrence ; que l'IGNA n'est pas non plus fondé à soutenir que la décision du ministre de limiter à un seul le nombre de lots susceptibles d'être attribués à un même candidat est entachée d'erreur manifeste d'appréciation ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              13. Considérant qu'il résulte de tout ce qui précède que la demande de l'IGNA doit être rejetée ; qu'il y a lieu, en application des dispositions de l'article L. 761-1 du code de justice administrative, de mettre à la charge de l'IGNA une somme de 3 000 euros à verser à la société Laboratoire Biomnis au titre des frais exposés par elle et non compris dans les dépens pour la procédure suivie devant le Conseil d'Etat ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du juge des référés du tribunal administratif de Paris du 17 octobre 2012 est annulée.<br/>
Article 2 : La demande présentée par l'Institut de Génétique Nantes Atlantique devant le tribunal administratif de Paris est rejetée.<br/>
Article 3 : L'Institut de Génétique Nantes Atlantique versera à la société Laboratoire Biomnis une somme de 3 000 euros en application des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la société Laboratoire Biomnis, à l'institut de Génétique Nantes Atlantique, à l'Institut national de police scientifique et à la Garde des sceaux, ministre de la justice.<br/>
Copie en sera adressée pour information au ministre de l'économie et des finances. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-02-02 MARCHÉS ET CONTRATS ADMINISTRATIFS. FORMATION DES CONTRATS ET MARCHÉS. MODE DE PASSATION DES CONTRATS. - ALLOTTISSEMENT (ART. 10 DU CODE DES MARCHÉS PUBLICS) - POSSIBILITÉ POUR LE POUVOIR ADJUDICATEUR DE LIMITER LE NOMBRE DE LOTS QUI POURRA ÊTRE ATTRIBUÉ À CHAQUE CANDIDAT - EXISTENCE.
</SCT>
<ANA ID="9A"> 39-02-02 Dans le cadre des dispositions de l'article 10 du code des marchés publics (CMP) et sans méconnaître aucune autre règle ni aucun autre principe issus du CMP, le pouvoir adjudicateur qui recourt à l'allotissement peut décider, afin d'assurer la satisfaction de ses besoins en s'adressant à une pluralité de cocontractants ou de susciter l'émergence d'une plus grande concurrence, de limiter le nombre de lots qui pourra être attribué à chaque candidat dès lors que ce nombre est indiqué dans les documents de la consultation.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
