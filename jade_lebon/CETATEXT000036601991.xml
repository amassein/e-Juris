<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036601991</ID>
<ANCIEN_ID>JG_L_2018_02_000000407880</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/60/19/CETATEXT000036601991.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 6ème chambres réunies, 14/02/2018, 407880</TITRE>
<DATE_DEC>2018-02-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>407880</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FABIANI, LUC-THALER, PINATEL</AVOCATS>
<RAPPORTEUR>M. Olivier Rousselle</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Laurence Marion</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:407880.20180214</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Cergy-Pontoise d'annuler la décision du 13 mai 2014 par laquelle le préfet du Val-d'Oise a refusé d'échanger son permis de conduire malien contre un permis de conduire français et la décision du 1er août 2014 par laquelle le préfet a rejeté son recours gracieux. Par un jugement n° 1409581 du 15 décembre 2016, le tribunal administratif de Cergy-Pontoise a rejeté sa demande.<br/>
<br/>
              Par un pourvoi, enregistré le 13 février 2017 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
<br/>
              - le code de la route ;<br/>
<br/>
              - l'arrêté du 12 janvier 2012 fixant les conditions de reconnaissance et d'échange des permis de conduire délivrés par les Etats n'appartenant ni à l'Union européenne, ni à l'Espace économique européen ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Rousselle, conseiller d'Etat, <br/>
<br/>
              - les conclusions de Mme Laurence Marion, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Fabiani, Luc-Thaler, Pinatel, avocat de M.B....<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. B... a demandé le 8 novembre 2013 à la préfecture du Val d'Oise la délivrance d'un permis de conduire français en échange d'un permis de conduire malien ; que le préfet du Val d'Oise a, par une décision du 13 mai 2014 , rejeté cette demande au motif que le titre malien produit n'était pas authentique, puis, par une décision du 1er août 2014, rejeté le recours gracieux de M.B... ; que le tribunal administratif de Cergy-Pontoise a, par un jugement du 15 décembre 2016, rejeté le recours pour excès de pouvoir de M. B... contre ces deux décisions ; que M. B...se pourvoit en cassation contre ce jugement ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article 7 de l'arrêté du 12 janvier 2012 fixant les conditions de reconnaissance et d'échange des permis de conduire délivrés par les Etats n'appartenant ni à l'Union européenne, ni à l'Espace économique européen : " Lorsque l'authenticité et la validité du titre sont établies lors du dépôt du dossier complet et sous réserve de satisfaire aux autres conditions prévues par le présent arrêté, le titre de conduite est échangé. / En cas de doute sur l'authenticité du titre dont l'échange est demandé, le préfet conserve le titre de conduite et fait procéder à son analyse, le cas échéant avec l'aide d'un service compétent, afin de s'assurer de son authenticité. Dans ce cas, une attestation de dépôt, sécurisée, est délivrée à son titulaire. Elle est valable pour une durée maximale de deux mois et est inscrite au fichier national du permis de conduire. Elle est retirée à l'issue de la procédure d'échange. / Si l'authenticité est confirmée, le titre de conduite peut être échangé sous réserve de satisfaire aux autres conditions. Si le caractère frauduleux est confirmé, l'échange n'a pas lieu et le titre est retiré par le préfet, qui saisit le procureur de la République en le lui transmettant. / Le préfet peut compléter son analyse en consultant l'autorité étrangère ayant délivré le titre afin de s'assurer des droits de conduite de son titulaire. Le titre de conduite est dès lors conservé par le préfet. La demande auprès des autorités étrangères est transmise, sous couvert du ministre des affaires étrangères, service de la valise diplomatique, au consulat de France compétent. Le consulat transmet au préfet la réponse de l'autorité étrangère. En l'absence de réponse dans un délai de six mois à compter de la saisine des autorités étrangères par le consulat compétent, l'échange du permis de conduire est refusé. Si l'autorité étrangère confirme l'absence de droits à conduire du titulaire, l'échange n'a pas lieu et le titre est retiré par le préfet qui saisit le procureur de la République en le lui transmettant " ;<br/>
<br/>
              3. Considérant qu'il résulte de ces dispositions qu'en cas de doute sur l'authenticité du titre dont l'échange est demandé, le préfet fait procéder à son analyse avec l'aide d'un service spécialisé en fraude documentaire et peut compléter son analyse en consultant par la voie diplomatique l'autorité étrangère qui a délivré le titre ; que l'intéressé peut, lors de l'instruction de sa demande par l'administration comme à l'appui d'un recours pour excès de pouvoir contre une décision refusant l'échange pour absence d'authenticité du titre, apporter la preuve de son authenticité par tout moyen présentant des garanties suffisantes ; que cette possibilité lui est ouverte y compris dans le cas où l'autorité étrangère, consultée par le préfet, n'a pas répondu ; que si des documents produits par l'intéressé et présentés comme des attestations de l'autorité étrangère ne peuvent être pris en considération que s'ils présentent eux-mêmes des garanties suffisantes d'authenticité, ils ne sauraient être écartés au seul motif qu'ils n'ont pas été transmis aux autorités françaises par la voie diplomatique ;<br/>
<br/>
              4. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, saisi par M. B...d'une demande d'échange de son permis de conduire malien contre un permis de conduire français, le préfet du Val d'Oise a demandé l'avis des services spécialisés dans la fraude documentaire, qui ont relevé une anomalie conduisant à douter de l'authenticité du titre, et a consulté par la voie diplomatique les autorités maliennes, qui n'ont pas répondu ; qu'à l'appui de son recours pour excès de pouvoir contre la décision du préfet de refuser l'échange, l'intéressé a produit devant le tribunal administratif de Cergy-Pontoise quatre documents présentés comme des attestations d'authenticité délivrées par les autorités maliennes ; qu'en écartant ces documents aux motifs que les autorités maliennes, consultées par le préfet, n'avaient pas répondu et que seules des attestations transmises aux autorités françaises par voie diplomatique étaient susceptibles d'établir l'authenticité d'un titre de conduite, le tribunal administratif a commis une erreur de droit ; que son jugement doit, par suite, être annulé ; <br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              6. Considérant qu'il ressort des pièces du dossier que M. B...a produit plusieurs correspondances entre le consulat général du Mali en France et le ministère de l'équipement et des transports de la République du Mali, qui ont transité par le ministère des affaires étrangères de la République du Mali, notamment une lettre en date du 28 décembre 2015 du directeur national des transports terrestres, maritimes et fluviaux qui atteste que le permis de conduire litigieux a été délivré le 22 juillet 2013 à M. A...B..., ainsi qu'une lettre du consul général du Mali en France confirmant l'authenticité de ce permis ; que ces attestations présentent des garanties suffisantes pour établir l'authenticité du permis et remettre en cause les conclusions du rapport du service spécialisé au vu duquel le préfet a refusé d'échanger le permis ; que, par suite, sans qu'il soit besoin d'examiner les autres moyens de la demande, M. B... est fondé à demander l'annulation des décisions des 13 mai et 1er août 2014 qu'il attaque ;<br/>
<br/>
              7. Considérant qu'aux termes de l'article L. 911-1 du code de justice administrative : " Lorsque sa décision implique nécessairement qu'une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public prenne une mesure d'exécution dans un sens déterminé, la juridiction, saisie de conclusions en ce sens, prescrit, par la même décision, cette mesure assortie, le cas échéant, d'un délai d'exécution " ; qu'il résulte des motifs de la présente décision que le permis de conduire malien dont M. B...a demandé l'échange contre un permis français présente les garanties requises d'authenticité ; qu'en réponse à une mesure d'instruction, l'administration n'a fait état d'aucun autre élément qui ferait obstacle à l'échange de ce permis contre un permis français eu égard aux dispositions applicables; qu'il y a lieu, par suite, d'ordonner au préfet du Val d'Oise de délivrer à l'intéressé un permis de conduire français dans un délai de deux mois à compter de la date à laquelle la présente décision lui sera notifiée ; que, dans les circonstances de l'espèce, il n'y a pas lieu d'assortir cette injonction d'une astreinte ;<br/>
<br/>
              8. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement à M. B...de la somme de 3 000 euros qu'il demande au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Cergy-Pontoise du 15 décembre 2016 est annulé.<br/>
<br/>
Article 2 : Les décisions du préfet du Val d'Oise des 13 mai  et 1er août 2014 sont annulées.<br/>
<br/>
Article 3 : Il est enjoint au préfet du Val d'Oise de délivrer un permis de conduire français à M. B... dans un délai de deux mois à compter de la date à laquelle il recevra notification de la présente décision.<br/>
<br/>
Article 4 : L'Etat versera à M. B...la somme de 3000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 5 : La présente décision sera notifiée à M. A...B..., au ministre d'Etat, ministre de l'intérieur et au préfet du Val d'Oise.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">49-04-01-04 POLICE. POLICE GÉNÉRALE. CIRCULATION ET STATIONNEMENT. PERMIS DE CONDUIRE. - ECHANGE D'UN PERMIS DE CONDUIRE ÉTRANGER CONTRE UN PERMIS DE CONDUIRE FRANÇAIS - FACULTÉ POUR LE PRÉFET DE CONSULTER PAR VOIE DIPLOMATIQUE L'AUTORITÉ ÉTRANGÈRE AYANT DÉLIVRÉ LE TITRE EN CAS DE DOUTE SUR SON AUTHENTICITÉ - MOYENS DE PREUVE APPORTÉS PAR LE DEMANDEUR LORS DE L'INSTRUCTION DE LA DEMANDE OU À L'APPUI D'UN RECOURS - TOUT MOYEN PRÉSENTANT DES GARANTIES SUFFISANTES, Y COMPRIS EN L'ABSENCE DE RÉPONSE DE L'AUTORITÉ ÉTRANGÈRE CONSULTÉE - CAS DE DOCUMENTS PRODUITS PAR L'INTÉRESSÉ ET PRÉSENTÉS COMME DES ATTESTATIONS DE L'AUTORITÉ ÉTRANGÈRE - PRISE EN COMPTE, DÈS LORS QU'ILS PRÉSENTENT DES GARANTIES SUFFISANTES, NONOBSTANT L'ABSENCE DE TRANSMISSION DE CES DOCUMENTS AUX AUTORITÉS FRANÇAISES PAR VOIE DIPLOMATIQUE.
</SCT>
<ANA ID="9A"> 49-04-01-04 Il résulte de l'article 7 de l'arrêté du 12 janvier 2012 fixant les conditions de reconnaissance et d'échange des permis de conduire délivrés par les Etats n'appartenant ni à l'Union européenne, ni à l'Espace économique européen qu'en cas de doute sur l'authenticité du titre dont l'échange est demandé, le préfet fait procéder à son analyse avec l'aide d'un service spécialisé en fraude documentaire et peut compléter son analyse en consultant par la voie diplomatique l'autorité étrangère qui a délivré le titre. L'intéressé peut, lors de l'instruction de sa demande par l'administration comme à l'appui d'un recours pour excès de pouvoir contre une décision refusant l'échange pour absence d'authenticité du titre, apporter la preuve de son authenticité par tout moyen présentant des garanties suffisantes. Cette possibilité lui est ouverte y compris dans le cas où l'autorité étrangère, consultée par le préfet, n'a pas répondu. Si des documents produits par l'intéressé et présentés comme des attestations de l'autorité étrangère ne peuvent être pris en considération que s'ils présentent eux-mêmes des garanties suffisantes d'authenticité, ils ne sauraient être écartés au seul motif qu'ils n'ont pas été transmis aux autorités françaises par la voie diplomatique.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
