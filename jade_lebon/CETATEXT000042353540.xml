<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042353540</ID>
<ANCIEN_ID>JG_L_2020_09_000000424360</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/35/35/CETATEXT000042353540.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 1ère chambres réunies, 21/09/2020, 424360</TITRE>
<DATE_DEC>2020-09-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>424360</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Céline Roux</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:424360.20200921</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés le 19 septembre 2018 et le 23 juillet 2019, M. A... B... et la société Les Essarteaux demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite, née le 27 août 2018, par laquelle le ministre de l'agriculture et de l'alimentation a rejeté leur demande d'abrogation des dispositions de l'article R. 242-93, des II et IV de l'article R. 242-95 et de l'article R. 242-102 du code rural et de la pêche maritime, dans leur rédaction issue du décret n° 2017-514 du 10 avril 2017 relatif à la réforme de l'ordre des vétérinaires ;<br/>
<br/>
              2°) d'enjoindre au ministre d'abroger les dispositions contestées dans un délai d'un mois sous astreinte de 500 euros par jour de retard ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code rural et de la pêche maritime ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 7 septembre 2020, présentée par M. B... et la société Les Essarteaux ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Céline Roux, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier que M. B..., vétérinaire, et la société d'exercice libéral à responsabilité limitée de vétérinaires Les Essarteaux demandent l'annulation pour excès de pouvoir de la décision implicite, née le 27 août 2018, par laquelle le ministre de l'agriculture et de l'alimentation a rejeté leur demande tendant à l'abrogation des dispositions de l'article R. 242-93, des II et IV de l'article R. 242-95 et de l'article R. 242-102 du code rural et de la pêche maritime, dans leur rédaction issue du décret du 10 avril 2017 relatif à la réforme de l'ordre des vétérinaires, lequel avait notamment pour objet de modifier l'organisation du régime disciplinaire au sein de cet ordre ainsi que la procédure applicable devant les chambres régionales de discipline. <br/>
<br/>
              Sur les conclusions à fin d'annulation :<br/>
<br/>
              2. L'article R. 242-93 du code rural et de la pêche maritime détermine les personnes ou autorités qui peuvent introduire une action disciplinaire contre un vétérinaire ou une société de vétérinaires, parmi lesquelles le président du conseil national de l'ordre des vétérinaires, le président du conseil régional de l'ordre dans le ressort duquel se trouve le domicile professionnel administratif du praticien poursuivi ainsi que le président d'un autre conseil régional de l'ordre, à la condition que celui-ci soit mandaté à cet effet par son conseil. Cet article dispose que " la plainte est adressée par lettre recommandée avec demande d'avis de réception au président du conseil régional de l'ordre du domicile professionnel administratif du vétérinaire poursuivi, qui la transmet au secrétaire général en charge du greffe de la chambre régionale de discipline ".<br/>
<br/>
              3. Il résulte de l'article R. 242-94 du code rural et de la pêche maritime qu'après réception de la plainte, le secrétaire général en charge du greffe de la chambre régionale de discipline notifie à la personne poursuivie, dans les meilleurs délais, les faits qui lui sont reprochés et l'informe qu'elle peut se faire assister d'un avocat ou d'un vétérinaire inscrit au tableau de l'ordre sous réserve qu'il ne soit pas conseiller ordinal. Un rapporteur est alors nommé par le président de la chambre régionale de discipline parmi les conseillers ordinaux du conseil régional dont dépend administrativement la personne poursuivie. Cette nomination est notifiée au plaignant, à la personne poursuivie et au président du conseil régional saisi de la plainte, lesquels peuvent, le cas échéant, demander la récusation du rapporteur ainsi désigné.<br/>
<br/>
              4. Aux termes de l'article R. 242-95 du code rural et de la pêche maritime : " I. - Le rapporteur conduit l'instruction, dans le respect des principes de contradiction et d'impartialité. / II. - Il engage sans délai une procédure de conciliation, sauf s'il dispose d'un procès-verbal constatant l'impossibilité de celle-ci, ou si le plaignant est un président de conseil de l'ordre, le préfet ou le procureur de la République. (...) ". En vertu du III du même article, le rapporteur, qui a qualité pour entendre les parties, recueillir tous témoignages et procéder à toutes constatations utiles à la manifestation de la vérité, rend un rapport qui " mentionne les diligences accomplies, les déclarations des parties, établit un exposé objectif des faits, et souligne les divergences entre les parties ". Aux termes du dernier alinéa du IV de ce même article : " Le rapporteur remet son rapport sur support papier et support dématérialisé au secrétaire général en charge du greffe de la chambre régionale de discipline qui le transmet au président de la chambre régionale de discipline et au président du conseil régional de l'ordre ".<br/>
<br/>
              5. Aux termes de l'article R. 242-102 du code rural et de la pêche maritime : " Le président de la chambre dirige les débats. La chambre entend le rapporteur en la lecture de son rapport. / L'auteur de la plainte est entendu ainsi que le président du conseil de l'ordre en ses demandes de peines disciplinaires. (...) ". Ce même article prévoit qu'après, le cas échéant, le recueil de témoignages, la personne poursuivie prend la parole en dernier. <br/>
<br/>
              6. En premier lieu, la circonstance que les dispositions de l'article R. 242-93 du code rural et de la pêche maritime ne prévoient pas que, pour engager une action disciplinaire contre un vétérinaire, le président du conseil national de l'ordre des vétérinaires ou le président du conseil régional de l'ordre dans le ressort duquel le vétérinaire a son domicile professionnel administratif doivent être mandatés à cet effet par leurs conseils respectifs est par elle-même dépourvue d'incidence sur le respect du principe des droits de la défense ou du droit à un procès équitable. Par suite, M. B... et la société Les Essarteaux ne peuvent utilement soutenir que ces dispositions seraient, dans cette mesure et à cet égard, illégales. <br/>
<br/>
              7. En deuxième lieu, il résulte des dispositions mentionnées aux points 2 et 5 que le président du conseil régional de l'ordre dans le ressort duquel se trouve le domicile professionnel administratif du vétérinaire poursuivi peut, d'une part, introduire une action disciplinaire contre un vétérinaire et qu'il est, d'autre part, entendu par la chambre régionale de discipline " en ses demandes de peines disciplinaires ". La seule circonstance que le président du conseil régional de l'ordre des vétérinaires puisse, en tant que garant des règles déontologiques de l'ordre des vétérinaires, à la fois déclencher une procédure disciplinaire en qualité de plaignant et demander à la juridiction disciplinaire, au cours de l'audience, le prononcé d'une sanction n'est pas, par elle-même, de nature à porter atteinte au principe des droits de la défense et au droit à un procès équitable, dès lors que le président du conseil régional de l'ordre ne siège pas au sein de la chambre régionale de discipline et que le vétérinaire poursuivi bénéficie des garanties rappelées en particulier aux points 2 à 5. <br/>
<br/>
              8. En troisième lieu, si, ainsi que le font valoir les requérants, les dispositions du II de l'article R. 242-95 écartent la procédure de conciliation si le plaignant est un président du conseil de l'ordre, eu égard à l'objet même de cette procédure qui a vocation à favoriser le règlement amiable de litiges entre confrères ou entre un client et un vétérinaire, et si les dispositions de l'article R. 242-102 prévoient que le président du conseil de l'ordre est entendu par la chambre régionale de discipline en ses demandes de peines disciplinaires, ces dispositions ne mettent nullement en cause, par elles-mêmes, le principe de la présomption d'innocence, garanti notamment par l'article 9 de la Déclaration des droits de l'homme et du citoyen et l'article 6 paragraphe 2 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales. <br/>
<br/>
              9. En quatrième lieu, il résulte des dispositions de l'article L. 242-5 du code rural et de la pêche maritime que la chambre régionale de discipline est présidée par un magistrat de l'ordre judiciaire, désigné par le premier président de la cour d'appel, et composée de conseillers ordinaux tirés au sort, qui ne peuvent être du ressort du même conseil régional de l'ordre que le vétérinaire poursuivi. Dans ces conditions, les requérants ne sont pas fondés à soutenir, en faisant valoir que le président du conseil régional ou national de l'ordre des vétérinaires disposerait d'une autorité morale de nature à influencer les membres de la chambre régionale de discipline, que les dispositions des articles R. 242-93 et R. 242-102 du code rural et de la pêche maritime méconnaîtraient les principes d'indépendance et d'impartialité de la juridiction ordinale, garantis notamment par l'article 6 paragraphe 1 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales. <br/>
<br/>
              10. En cinquième et dernier lieu, toutefois, il résulte des dispositions du dernier alinéa de l'article R. 242-95 du code rural et de la pêche maritime, aux termes desquelles " le rapporteur transmet son rapport (...) au secrétaire général en charge du greffe de la chambre régionale de discipline qui le transmet au président de la chambre régionale de discipline et au président du conseil régional de l'ordre ", et de celles de l'article R. 242-99 du même code, dont il résulte que le vétérinaire poursuivi n'a la possibilité de consulter le dossier d'instruction déposé au greffe de la chambre régionale de discipline, qui comprend notamment le rapport du rapporteur, qu'après avoir reçu la convocation prévue par ces dispositions, laquelle est susceptible de ne lui être communiquée, si le plus court délai est retenu, que quinze jours avant l'audience, que le président du conseil régional de l'ordre, alors qu'il peut avoir introduit l'action disciplinaire et qu'il est appelé à faire connaître à l'audience de la juridiction disciplinaire la sanction qui lui paraît devoir être prononcée à raison des faits reprochés, reçoit le rapport du rapporteur antérieurement à la communication de ce rapport aux autres parties et, en particulier, au vétérinaire poursuivi. Par suite, les requérants sont fondés à soutenir que les dispositions de l'article R. 242-95 du code rural et de la pêche maritime qui prévoient la transmission, à ce stade, du rapport au président du conseil régional de l'ordre, alors que les autres parties n'en disposent pas encore, méconnaissent le principe de l'égalité des armes et le droit à un procès équitable. La décision attaquée, en tant qu'elle refuse de faire droit à l'abrogation de ces dispositions dans cette mesure, doit, dès lors, être annulée.<br/>
<br/>
              11. Il résulte de ce qui précède que les requérants ne sont fondés à demander l'annulation pour excès de pouvoir de la décision qu'ils attaquent qu'en tant seulement qu'elle refuse de faire droit à la demande d'abrogation des dispositions du dernier alinéa du IV de l'article R. 242-95 du code rural et de la pêche maritime prévoyant la transmission du rapport au président du conseil régional de l'ordre.<br/>
<br/>
              Sur les conclusions à fins d'injonction :<br/>
<br/>
              12. L'annulation de la décision implicite attaquée en tant qu'elle a refusé d'abroger les dispositions du dernier alinéa du IV de l'article R. 242-95 du code rural et de la pêche maritime prévoyant la transmission du rapport au président du conseil régional de l'ordre implique nécessairement l'abrogation, dans cette mesure, de ces dispositions réglementaires. Il y a lieu d'enjoindre au Premier ministre d'y procéder dans un délai de six mois à compter de la notification de la présente décision. Dans les circonstances de l'espèce, il n'y a pas lieu d'assortir cette injonction d'une astreinte.<br/>
<br/>
              Sur les conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              13. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme globale de 3 000 euros, à verser à M. B... et à la société Les Essarteaux au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La décision implicite par laquelle le ministre de l'agriculture et de l'alimentation a rejeté la demande de M. B... et de la société Les Essarteaux est annulée en tant qu'elle refuse d'abroger les dispositions du dernier alinéa du IV de l'article R. 242-95 du code rural et de la pêche maritime qui prévoient la transmission du rapport au président du conseil régional de l'ordre.<br/>
Article 2 : Il est enjoint au Premier ministre d'abroger les dispositions du dernier alinéa du IV de l'article R. 242-95 du code rural et de la pêche maritime qui prévoient la transmission du rapport au président du conseil régional de l'ordre dans le délai de six mois à compter de la notification de la présente décision.<br/>
Article 3 : L'Etat versera à M. B... et la société Les Essarteaux une somme globale de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Le surplus des conclusions de la requête est rejeté. <br/>
Article 5 : La présente décision sera notifiée à M. A... B..., à la société Les Essarteaux, au ministre de l'agriculture et de l'alimentation et au Premier ministre.<br/>
Copie en sera adressée à la Section du rapport et des études du Conseil d'Etat.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">26-055-01-06-02 DROITS CIVILS ET INDIVIDUELS. CONVENTION EUROPÉENNE DES DROITS DE L'HOMME. DROITS GARANTIS PAR LA CONVENTION. DROIT À UN PROCÈS ÉQUITABLE (ART. 6). VIOLATION. - EXISTENCE - PROCÉDURE DEVANT LA CHAMBRE RÉGIONALE DE DISCIPLINE DE L'ORDRE DES VÉTÉRINAIRES - COMMUNICATION DU RAPPORT DU RAPPORTEUR AU PRÉSIDENT DU CONSEIL RÉGIONAL AVANT LES AUTRES PARTIES.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">55-04-01 PROFESSIONS, CHARGES ET OFFICES. DISCIPLINE PROFESSIONNELLE. PROCÉDURE DEVANT LES JURIDICTIONS ORDINALES. - ORDRE DES VÉTÉRINAIRES - PROCÉDURE DEVANT LA CHAMBRE RÉGIONALE DE DISCIPLINE - COMMUNICATION DU RAPPORT DU RAPPORTEUR AU PRÉSIDENT DU CONSEIL RÉGIONAL AVANT LES AUTRES PARTIES - MÉCONNAISSANCE DU PRINCIPE D'ÉGALITÉ DES ARMES ET DU DROIT À UN PROCÈS ÉQUITABLE - EXISTENCE.
</SCT>
<ANA ID="9A"> 26-055-01-06-02 Il résulte du dernier alinéa de l'article R. 242-95 du code rural et de la pêche maritime (CRPM) que le président du conseil régional de l'ordre des vétérinaires, alors qu'il peut avoir introduit l'action disciplinaire et qu'il est appelé à faire connaître à l'audience de la juridiction disciplinaire la sanction qui lui paraît devoir être prononcée à raison des faits reprochés, reçoit le rapport du rapporteur antérieurement à la communication de ce rapport aux autres parties et, en particulier, au vétérinaire poursuivi. Par suite, les dispositions de l'article R. 242-95 qui prévoient la transmission du rapport au président du conseil régional de l'ordre dès sa remise par le rapporteur, alors que les autres parties n'en disposent pas encore, méconnaissent le principe de l'égalité des armes et le droit à un procès équitable.</ANA>
<ANA ID="9B"> 55-04-01 Il résulte du dernier alinéa de l'article R. 242-95 du code rural et de la pêche maritime (CRPM) que le président du conseil régional de l'ordre des vétérinaires, alors qu'il peut avoir introduit l'action disciplinaire et qu'il est appelé à faire connaître à l'audience de la juridiction disciplinaire la sanction qui lui paraît devoir être prononcée à raison des faits reprochés, reçoit le rapport du rapporteur antérieurement à la communication de ce rapport aux autres parties et, en particulier, au vétérinaire poursuivi. Par suite, les dispositions de l'article R. 242-95 qui prévoient la transmission du rapport au président du conseil régional de l'ordre dès sa remise par le rapporteur, alors que les autres parties n'en disposent pas encore, méconnaissent le principe de l'égalité des armes et le droit à un procès équitable.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
