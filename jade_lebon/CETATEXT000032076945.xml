<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032076945</ID>
<ANCIEN_ID>JG_L_2016_02_000000383771</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/07/69/CETATEXT000032076945.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème / 1ère SSR, 17/02/2016, 383771, Publié au recueil Lebon</TITRE>
<DATE_DEC>2016-02-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>383771</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème / 1ère SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>M. Cyrille Beaufils</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2016:383771.20160217</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Smurfit Kappa Papier recyclé France a demandé au tribunal administratif de Limoges d'annuler, d'une part, la décision du 6 juillet 2009 rejetant son recours administratif préalable obligatoire contre l'arrêté du 31 octobre 2008 du ministre de l'écologie, du développement durable et de l'énergie en tant qu'il ramène, sur la période 2008-2012, le quota d'émissions de gaz à effet de serre qui lui est alloué, pour son installation de production de papier située à Saillat-sur-Vienne, de 367 210 à 179 185 tonnes de CO2, soit de 73 442 à 35 837 tonnes de CO2 par an, avec effet au 1er janvier 2010, et d'autre part, la décision du 1er février 2010 rejetant son recours administratif préalable obligatoire contre l'arrêté du 13 juillet 2009 du ministre de l'écologie, du développement durable et de l'énergie en tant qu'il ramène le quota annuel d'émissions de gaz à effet de serre, pour la même installation, de 73 442 à 35 837 tonnes de CO2 par an, pour les années 2010, 2011, 2012. Par deux jugements n° 0901687 et n° 1000550 du 9 février 2012, le tribunal administratif de Limoges a annulé ces deux décisions. <br/>
<br/>
              Par un arrêt nos 12BX00915, 12BX00916 du 17 juin 2014, la cour administrative d'appel de Bordeaux a rejeté l'appel formé contre ces deux jugements du tribunal administratif de Limoges par le ministre de l'écologie, du développement durable et de l'énergie. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 18 août et 19 novembre 2014 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'écologie, du développement durable et de l'énergie demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'environnement ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Cyrille Beaufils, auditeur,  <br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la société Smurfit Kappa Papier recyclé France ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que, sous réserve de dispositions législatives ou réglementaires contraires, et hors le cas où il est satisfait à une demande du bénéficiaire, l'administration ne peut retirer ou abroger une décision expresse individuelle créatrice de droits que dans le délai de quatre mois suivant l'intervention de cette décision et si elle est illégale ; <br/>
<br/>
              2. Considérant, d'une part, qu'aux termes de l'article L. 229-5 du code de l'environnement, dans sa rédaction applicable à la date des décisions contestées : " Les dispositions de la présente section s'appliquent aux installations classées rejetant un gaz à effet de serre dans l'atmosphère lorsqu'elles exercent une des activités dont la liste est fixée par décret en Conseil d'Etat (...) " ; qu'aux termes de l'article L. 229-7 du même code : " Un quota d'émission de gaz à effet de serre au sens de la présente section est une unité de compte représentative de l'émission de l'équivalent d'une tonne de dioxyde de carbone. / Pour chaque installation bénéficiant de l'autorisation d'émettre des gaz à effet de serre, l'Etat affecte à l'exploitant, pour une période déterminée, des quotas d'émission et lui délivre chaque année, au cours de cette période, une part des quotas qui lui ont été ainsi affectés. / La quantité de gaz à effet de serre émise par cette installation au cours d'une année civile est calculée ou mesurée et exprimée en tonnes de dioxyde de carbone. / A l'issue de chacune des années civiles de la période d'affectation, l'exploitant restitue à l'Etat sous peine des sanctions prévues à l'article L. 229-18 un nombre de quotas égal au total des émissions de gaz à effet de serre de ses installations " ; qu'aux termes de l'article L. 229-8 du même code : " I. - Les quotas d'émission de gaz à effet de serre sont affectés par l'Etat (...) par périodes de cinq ans, dans le cadre d'un plan national établi pour chaque période. (...) / IV. - Le plan répartit les quotas d'émission entre les différentes installations mentionnées à l'article L. 229-5. Cette répartition tient compte des possibilités techniques et économiques de réduction des émissions des activités bénéficiaires, des prévisions d'évolution de la production de ces activités, des mesures prises en vue de réduire les émissions de gaz à effet de serre avant l'établissement du système d'échange de quotas ainsi, le cas échéant, que de la concurrence d'activités situées dans des pays extérieurs à la Communauté européenne (...) " ; que l'article L. 229-11 du même code prévoit que : " L'autorité administrative notifie aux exploitants des installations autorisées à émettre des gaz à effet de serre le montant total des quotas d'émission affectés au titre de chaque période couverte par un plan et la quantité délivrée chaque année. / Un décret en Conseil d'Etat fixe (...) les règles de délivrance annuelle des quotas, les règles applicables en cas de changement d'exploitant ou de cessation ou de transfert d'activité ainsi que les conditions dans lesquelles les décisions d'affectation ou de délivrance et le plan national d'affectation des quotas prévu à l'article L. 229-8 peuvent être contestés " ; qu'aux termes de l'article L. 229-13 du même code : " Les quotas sont valables pendant la durée du plan au titre duquel ils sont affectés tant qu'ils ne sont pas utilisés (...) " ; que l'article R. 229-9 du code de l'environnement prévoit enfin, dans sa version applicable au litige, que : " Dès la publication du décret approuvant le plan national d'affectation des quotas, le ministre chargé de l'environnement fixe par arrêté la liste des exploitants auxquels sont affectés des quotas d'émission pour la période couverte par le plan. / L'arrêté précise, pour chaque installation, le montant total des quotas affectés ainsi que les quantités de quotas qui seront délivrées chaque année " ; <br/>
<br/>
              3. Considérant, d'autre part, que les articles R. 229-11 à R. 229-19 du code de l'environnement, dans leur rédaction en vigueur à la date des décisions litigieuses, fixent les conditions dans lesquelles les décisions d'affectation pluriannuelles de quotas peuvent être modifiées ou abrogées pour la durée de la période de référence restant à courir, notamment dans les cas de création d'une nouvelle installation classée entrant dans le champ du dispositif, de changement d'exploitant, de modification entraînant un changement notable de l'installation, de mise à l'arrêt et de variation d'activité exceptionnelle et imprévisible d'une installation ; que l'article R. 229-15 prévoit notamment qu'en cas d'arrêt de l'installation, il n'est plus délivré de quotas à l'exploitant au titre de cette installation, le solde des quotas affectés non encore délivrés venant abonder la réserve constituée en application de l'article L. 229-18 ;<br/>
<br/>
              4. Considérant qu'il résulte des dispositions citées ci-dessus que la décision par laquelle le ministre chargé de l'écologie retient un exploitant sur la liste des exploitants auxquels sont affectés des quotas d'émission de gaz à effet de serre et précise, pour chacune de ses installations, d'une part, le montant des quotas qui lui sont affectés pour l'ensemble de la période couverte par le plan national d'affectation des quotas, et d'autre part, les quantités de quotas qui lui seront délivrées chaque année, se borne à fixer un cadre prévisionnel qui a vocation à déterminer le montant annuel des quotas dont disposera l'exploitant et n'a pas le caractère d'une décision créatrice de droits au profit de ce dernier, au sens des principes rappelés au point 1, qui ne pourrait être abrogée au-delà du délai de quatre mois suivant son intervention ; qu'en revanche, les arrêtés annuels délivrant les quotas à l'exploitant constituent des décisions créatrices de droits ; que les principes rappelés au point 1, non plus que les dispositions rappelées ci-dessus ne s'opposent à ce que le ministre tienne compte, pour modifier la décision affectant les quotas pour l'ensemble de la période du plan, et à l'occasion des arrêtés annuels de délivrance des quotas, d'une erreur commise par l'exploitant dans les éléments factuels de la déclaration préalable ayant servi de base au calcul des quotas alloués pour la durée du plan et portant sur ses émissions lors de l'année de référence ;  <br/>
<br/>
              5. Considérant qu'il ressort des énonciations de l'arrêt attaqué que par arrêté du 31 mai 2007, le ministre chargé de l'écologie a fixé la liste des exploitants auxquels étaient affectés des quotas d'émission de gaz à effet de serre pour la période 2008-2012, les installations au titre desquelles ces quotas sont affectés et les montants desdits quotas ; qu'à ce titre, l'arrêté affectait à la société Smurfit Kappa Papier recyclé France, pour l'installation de production de papier exploitée à Saillat-sur-Vienne, un quota de 367 210 tonnes de CO2 pour l'ensemble de la période 2008-2012, soit 73 442 tonnes par an, sur le fondement des déclarations par l'exploitant des émissions de gaz à effet de serre de ses installations pour l'année 2002 ; que cette déclaration s'est révélée erronée, l'exploitant ayant pris en compte, dans le montant des émissions déclarées, les émissions résultant d'une unité de cogénération dont l'exploitation avait été confiée à une autre société déjà bénéficiaire de quotas à ce titre ; que par arrêté du 31 octobre 2008, le ministre chargé de l'écologie a, pour tenir compte de cette erreur, réduit les quotas affectés à la société Smurfit Kappa papier pour les porter à 35 837 tonnes annuelles, puis par un arrêté du 13 juillet 2009, abrogeant les deux arrêtés précédents, a révisé l'affectation des quotas en allouant ce nouveau montant annuel pour les années 2010 à 2012 ;  qu'il résulte de ce qui a été dit au point 4 qu'en jugeant que l'arrêté du 31 mai 2007 affectant les quotas à l'exploitant pour la période 2008-2012 avait le caractère d'une décision individuelle créatrice de droits et en en déduisant que le ministre chargé de l'écologie n'avait pu légalement, par les décisions litigieuses, modifier cet arrêté pour tirer les conséquences de l'erreur commise par l'exploitant dans sa déclaration d'émissions initiale, la cour a commis une erreur de droit ; que, par suite, le ministre de l'écologie, du développement durable et de l'énergie est fondé à demander l'annulation de l'arrêt qu'il attaque ;<br/>
<br/>
              6. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 17 juin 2014 de la cour administrative d'appel de Bordeaux est annulé. <br/>
<br/>
      Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Bordeaux. <br/>
<br/>
Article 3 : Les conclusions de la société Smurfit Kappa Papier recyclé France présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la ministre de l'environnement, de l'énergie et de la mer, chargée des relations internationales sur le climat et à la société Smurfit Kappa Papier recyclé France.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-01-06-02-01 ACTES LÉGISLATIFS ET ADMINISTRATIFS. DIFFÉRENTES CATÉGORIES D'ACTES. ACTES ADMINISTRATIFS - CLASSIFICATION. ACTES INDIVIDUELS OU COLLECTIFS. ACTES CRÉATEURS DE DROITS. - ARRÊTÉS ANNUELS DÉLIVRANT LES QUOTAS DE GAZ À EFFET DE SERRE À UN EXPLOITANT.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">01-01-06-02-02 ACTES LÉGISLATIFS ET ADMINISTRATIFS. DIFFÉRENTES CATÉGORIES D'ACTES. ACTES ADMINISTRATIFS - CLASSIFICATION. ACTES INDIVIDUELS OU COLLECTIFS. ACTES NON CRÉATEURS DE DROITS. - DÉCISION PAR LAQUELLE LE MINISTRE CHARGÉ DE L'ÉCOLOGIE AFFECTE DES QUOTAS POUR L'ENSEMBLE DE LA PÉRIODE COUVERTE PAR LE PLAN NATIONAL D'AFFECTATION DES QUOTAS.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">44-008-02-01 NATURE ET ENVIRONNEMENT. - ATTRIBUTION DES QUOTAS D'ÉMISSION DE GAZ À EFFET DE SERRE - ACTE CRÉATEUR DE DROIT - 1) DÉCISION PAR LAQUELLE LE MINISTRE CHARGÉ DE L'ÉCOLOGIE AFFECTE DES QUOTAS POUR L'ENSEMBLE DE LA PÉRIODE COUVERTE PAR LE PLAN NATIONAL D'AFFECTATION DES QUOTAS - ABSENCE - 2) ARRÊTÉS ANNUELS DÉLIVRANT LES QUOTAS DE GAZ À EFFET DE SERRE À UN EXPLOITANT - EXISTENCE - 3) CONSÉQUENCE - FACULTÉ DU MINISTRE DE TENIR COMPTE D'UNE ERREUR DANS LA DÉCLARATION PRÉALABLE POUR MODIFIER LA DÉCISION D'AFFECTATION POUR L'ENSEMBLE DE LA PÉRIODE ET À L'OCCASION DES ARRÊTÉS ANNUELS SUIVANTS.
</SCT>
<ANA ID="9A"> 01-01-06-02-01 La décision par laquelle le ministre chargé de l'écologie retient un exploitant sur la liste des exploitants auxquels sont affectés des quotas d'émission de gaz à effet de serre et précise, pour chacune de ses installations, d'une part, le montant des quotas qui lui sont affectés pour l'ensemble de la période couverte par le plan national d'affectation des quotas, et d'autre part, les quantités de quotas qui lui seront délivrées chaque année, se borne à fixer un cadre prévisionnel qui a vocation à déterminer le montant annuel des quotas dont disposera l'exploitant et n'a pas le caractère d'une décision créatrice de droits au profit de ce dernier.... ,,En revanche, les arrêtés annuels délivrant les quotas à l'exploitant constituent des décisions créatrices de droits.</ANA>
<ANA ID="9B"> 01-01-06-02-02 La décision par laquelle le ministre chargé de l'écologie retient un exploitant sur la liste des exploitants auxquels sont affectés des quotas d'émission de gaz à effet de serre et précise, pour chacune de ses installations, d'une part, le montant des quotas qui lui sont affectés pour l'ensemble de la période couverte par le plan national d'affectation des quotas, et d'autre part, les quantités de quotas qui lui seront délivrées chaque année, se borne à fixer un cadre prévisionnel qui a vocation à déterminer le montant annuel des quotas dont disposera l'exploitant et n'a pas le caractère d'une décision créatrice de droits au profit de ce dernier.... ,,En revanche, les arrêtés annuels délivrant les quotas à l'exploitant constituent des décisions créatrices de droits.</ANA>
<ANA ID="9C"> 44-008-02-01 1) La décision par laquelle le ministre chargé de l'écologie retient un exploitant sur la liste des exploitants auxquels sont affectés des quotas d'émission de gaz à effet de serre et précise, pour chacune de ses installations, d'une part, le montant des quotas qui lui sont affectés pour l'ensemble de la période couverte par le plan national d'affectation des quotas, et d'autre part, les quantités de quotas qui lui seront délivrées chaque année, se borne à fixer un cadre prévisionnel qui a vocation à déterminer le montant annuel des quotas dont disposera l'exploitant et n'a pas le caractère d'une décision créatrice de droits au profit de ce dernier.... ,,2) En revanche, les arrêtés annuels délivrant les quotas à l'exploitant constituent des décisions créatrices de droits.,,,3) Ni les principes régissant le retrait et l'abrogation des actes créateurs de droit, ni les articles L. 229-5 et suivants du code de l'environnement ne s'opposent à ce que le ministre tienne compte, pour modifier la décision affectant les quotas pour l'ensemble de la période du plan, et à l'occasion des arrêtés annuels de délivrance des quotas, d'une erreur commise par l'exploitant dans les éléments factuels de la déclaration préalable ayant servi de base au calcul des quotas alloués pour la durée du plan et portant sur ses émissions lors de l'année de référence.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
