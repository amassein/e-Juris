<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026480627</ID>
<ANCIEN_ID>JG_L_2012_10_000000357193</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/48/06/CETATEXT000026480627.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème et 10ème sous-sections réunies, 11/10/2012, 357193, Publié au recueil Lebon</TITRE>
<DATE_DEC>2012-10-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>357193</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème et 10ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE ; SCP BARADUC, DUHAMEL</AVOCATS>
<RAPPORTEUR>M. Philippe Josse</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Claire Legras</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:357193.20121011</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 29 février et 26 mars 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour la société Casino Guichard-Perrachon, dont le siège est 1, Esplanade de France, BP 306 à Saint-Etienne (42008 Cedex 2) ; la société Casino Guichard-Perrachon demande au Conseil d'Etat d'annuler, pour excès de pouvoir, l'avis n° 12-A-01 de l'Autorité de la concurrence du 11 janvier 2012 relatif à la situation concurrentielle dans le secteur de la distribution alimentaire à Paris ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 17 septembre 2012, présentée pour la société Casino Guichard-Perrachon ;<br/>
<br/>
              Vu la Constitution ;<br/>
<br/>
              Vu le code de commerce ;<br/>
<br/>
              Vu l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Philippe Josse, Conseiller d'Etat,  <br/>
<br/>
              - les observations de la SCP Baraduc, Duhamel, avocat de l'Autorité de la concurrence et de la SCP Piwnica, Molinié, avocat de la société Casino Guichard-Perrachon,<br/>
<br/>
              - les conclusions de Mme Claire Legras, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Baraduc, Duhamel, avocat de l'Autorité de la concurrence et à la SCP Piwnica, Molinié, avocat de la société Casino Guichard-Perrachon ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 462-1 du code de commerce : " L'Autorité de la concurrence peut être consultée par les commissions parlementaires sur les propositions de loi ainsi que sur toute question concernant la concurrence. / Elle donne son avis sur toute question de concurrence à la demande du Gouvernement. Elle peut également donner son avis sur les mêmes questions à la demande des collectivités territoriales, des organisations professionnelles et syndicales, des organisations de consommateurs agréées, des chambres d'agriculture, des chambres de métiers ou des chambres de commerce et d'industrie territoriales, de la Haute Autorité pour la diffusion des oeuvres et la protection des droits sur internet et des présidents des observatoires des prix et des revenus de Guadeloupe, de Guyane, de Martinique, de La Réunion, du Département de Mayotte et de Saint-Pierre-et-Miquelon, en ce qui concerne les intérêts dont ils ont la charge " ;<br/>
<br/>
              2. Considérant qu'il est loisible à l'Autorité de la concurrence, lorsqu'elle exerce la faculté d'émettre un avis que lui reconnaît l'article L. 462-4 du code de commerce, de faire toute préconisation relative à la question de concurrence qui est l'objet de son analyse, qu'elle s'adresse au législateur, aux ministres intéressés ou aux opérateurs économiques ; que les prises de position et recommandations qu'elle formule à cette occasion ne constituent pas des décisions faisant grief ; qu'il en irait toutefois différemment si elles revêtaient le caractère de dispositions générales et impératives ou de prescriptions individuelles dont l'Autorité pourrait ultérieurement censurer la méconnaissance ;<br/>
<br/>
              3. Considérant que, sur le fondement de ces dispositions, la ville de Paris a, le 8 février 2011, demandé l'avis de l'Autorité de la concurrence concernant la situation concurrentielle dans le secteur de la distribution alimentaire à Paris ; que l'Autorité a adopté cet avis le 11 janvier 2012 ; que ce document procède à l'analyse des caractéristiques de la demande puis de l'offre en matière de distribution alimentaire à Paris et à la description détaillée des groupes et enseignes présents dans la capitale ; qu'il propose ensuite des principes de définition des marchés pertinents pour le domaine étudié et analyse la structure du marché, ainsi que le comportement des opérateurs en matière de prix, de marges et d'ouverture de nouveaux magasins ; qu'il formule, enfin, des recommandations, dont certaines sont susceptibles d'être mises en oeuvre par le groupe Casino si ce dernier le souhaite, d'autres relèvent de la compétence de la ville de Paris et d'autres, enfin, nécessitent une intervention préalable du législateur ; que si l'avis souligne l'importance de la position occupée par le groupe Casino sur le marché de la distribution alimentaire à Paris, cette analyse ne comporte pas, en elle-même, d'appréciations susceptibles d'emporter des effets de droit ; que, si elle était ultérieurement reprise par l'Autorité de la concurrence ou par une autre autorité dans le cadre d'une procédure aboutissant à une décision faisant grief, elle pourrait, à cette occasion, faire l'objet d'un débat contentieux ; que, dès lors et quelle que soit l'ampleur de la publicité dont il a fait l'objet, cet avis n'a pas le caractère de décision susceptible de faire l'objet d'un recours pour excès de pouvoir ; que, par suite et sans qu'il soit besoin de prescrire l'enquête sollicitée ni de se prononcer sur le renvoi au Conseil constitutionnel de la question prioritaire de constitutionnalité tirée de ce que les dispositions du deuxième alinéa de l'article L. 462-1 du code de commerce porteraient atteinte aux droits et libertés garantis par la Constitution, la requête formée par la société Casino Guichard-Perrachon doit être rejetée ;<br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société Casino Guichard-Perrachon la somme de 3 000 euros à verser à l'Etat (Autorité de la concurrence) au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : La requête de la société Casino Guichard-Perrachon est rejetée.<br/>
Article 2 : La société Casino Guichard-Perrachon versera à l'Etat (Autorité de la concurrence) une somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la société Casino Guichard-Perrachon et à l'Autorité de la concurrence.<br/>
Copie en sera adressée au Conseil constitutionnel, au Premier ministre et au ministre de l'économie et des finances.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">14-05-005 COMMERCE, INDUSTRIE, INTERVENTION ÉCONOMIQUE DE LA PUISSANCE PUBLIQUE. DÉFENSE DE LA CONCURRENCE. AUTORITÉ DE LA CONCURRENCE. - AVIS ÉMIS EN VERTU DE L'ARTICLE L. 462-1 DU CODE DE COMMERCE - 1) PRISES DE POSITION ET RECOMMANDATIONS FORMULÉES À CETTE OCCASION - DÉCISIONS FAISANT GRIEF - ABSENCE, SAUF DISPOSITIONS GÉNÉRALES ET IMPÉRATIVES OU PRESCRIPTIONS INDIVIDUELLES DONT L'AUTORITÉ DE LA CONCURRENCE POURRAIT ULTÉRIEUREMENT CENSURER LA MÉCONNAISSANCE - 2) APPLICATION EN L'ESPÈCE - AVIS SUR LA SITUATION CONCURRENTIELLE DANS LE SECTEUR DE LA DISTRIBUTION ALIMENTAIRE À PARIS QUI, S'IL FORMULE DES RECOMMANDATIONS SUSCEPTIBLES D'ÊTRE MISES EN &#140;UVRE PAR UN GROUPE DE DISTRIBUTION EN PARTICULIER ET SOULIGNE L'IMPORTANCE DE LA POSITION OCCUPÉE PAR CE GROUPE SUR CE MARCHÉ, NE COMPORTE PAS, EN LUI-MÊME, D'APPRÉCIATIONS SUSCEPTIBLES D'EMPORTER DES EFFETS DE DROIT - CONSÉQUENCE - DÉCISION SUSCEPTIBLE DE FAIRE L'OBJET D'UN REP - ABSENCE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-01-01-02 PROCÉDURE. INTRODUCTION DE L'INSTANCE. DÉCISIONS POUVANT OU NON FAIRE L'OBJET D'UN RECOURS. ACTES NE CONSTITUANT PAS DES DÉCISIONS SUSCEPTIBLES DE RECOURS. - AVIS ÉMIS PAR L'AUTORITÉ DE LA CONCURRENCE EN VERTU DE L'ARTICLE L. 462-1 DU CODE DE COMMERCE - 1) PRISES DE POSITION ET RECOMMANDATIONS FORMULÉES À CETTE OCCASION - DÉCISIONS FAISANT GRIEF - ABSENCE, SAUF DISPOSITIONS GÉNÉRALES ET IMPÉRATIVES OU PRESCRIPTIONS INDIVIDUELLES DONT L'AUTORITÉ POURRAIT ULTÉRIEUREMENT CENSURER LA MÉCONNAISSANCE - 2) APPLICATION DE CES PRINCIPES EN L'ESPÈCE - AVIS SUR LA SITUATION CONCURRENTIELLE DANS LE SECTEUR DE LA DISTRIBUTION ALIMENTAIRE À PARIS QUI, S'IL FORMULE DES RECOMMANDATIONS SUSCEPTIBLES D'ÊTRE MISES EN &#140;UVRE PAR UN GROUPE DE DISTRIBUTION EN PARTICULIER ET SOULIGNE L'IMPORTANCE DE LA POSITION OCCUPÉE PAR CE GROUPE SUR CE MARCHÉ, NE COMPORTE PAS, EN LUI-MÊME, D'APPRÉCIATIONS SUSCEPTIBLES D'EMPORTER DES EFFETS DE DROIT - CONSÉQUENCE - DÉCISION SUSCEPTIBLE DE FAIRE L'OBJET D'UN REP - ABSENCE.
</SCT>
<ANA ID="9A"> 14-05-005 1) Il est loisible à l'Autorité de la concurrence, lorsqu'elle émet un avis en vertu de l'article L. 462-1 du code de commerce, de faire toute préconisation relative à la question de concurrence qui est l'objet de son analyse, qu'elle s'adresse au législateur, aux ministres intéressés ou aux opérateurs économiques. Les prises de position et recommandations qu'elle formule à cette occasion ne constituent pas des décisions faisant grief. Il en irait toutefois différemment si elles revêtaient le caractère de dispositions générales et impératives ou de prescriptions individuelles dont l'Autorité pourrait ultérieurement censurer la méconnaissance. 2) En l'espèce, l'avis adopté par l'Autorité de la concurrence sur la situation concurrentielle dans le secteur de la distribution alimentaire à Paris, s'il formule notamment des recommandations, dont certaines sont susceptibles d'être mises en oeuvre par un groupe de distribution en particulier si ce dernier le souhaite, et s'il souligne l'importance de la position occupée par ce groupe sur ce marché, ne comporte pas, en lui-même, d'appréciations susceptibles d'emporter des effets de droit. Par suite, cet avis n'a pas le caractère de décision susceptible de faire l'objet d'un recours pour excès de pouvoir (REP).</ANA>
<ANA ID="9B"> 54-01-01-02 1) Il est loisible à l'Autorité de la concurrence, lorsqu'elle émet un avis en vertu de l'article L. 462-1 du code de commerce, de faire toute préconisation relative à la question de concurrence qui est l'objet de son analyse, qu'elle s'adresse au législateur, aux ministres intéressés ou aux opérateurs économiques. Les prises de position et recommandations qu'elle formule à cette occasion ne constituent pas des décisions faisant grief. Il en irait toutefois différemment si elles revêtaient le caractère de dispositions générales et impératives ou de prescriptions individuelles dont l'Autorité pourrait ultérieurement censurer la méconnaissance. 2) En l'espèce, l'avis adopté par l'Autorité de la concurrence sur la situation concurrentielle dans le secteur de la distribution alimentaire à Paris, s'il formule notamment des recommandations, dont certaines sont susceptibles d'être mises en oeuvre par un groupe de distribution en particulier si ce dernier le souhaite, et s'il souligne l'importance de la position occupée par ce groupe sur ce marché, ne comporte pas, en lui-même, d'appréciations susceptibles d'emporter des effets de droit. Par suite, cet avis n'a pas le caractère de décision susceptible de faire l'objet d'un recours pour excès de pouvoir (REP).</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., pour un avis émis en vertu de l'article L. 462-4 du code de commerce, décision du même jour, Société ITM Entreprises et autres, n°s 346378 346444, à publier au Recueil.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
