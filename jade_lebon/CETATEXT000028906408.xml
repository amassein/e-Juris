<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028906408</ID>
<ANCIEN_ID>JG_L_2014_05_000000355580</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/90/64/CETATEXT000028906408.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème / 4ème SSR, 05/05/2014, 355580</TITRE>
<DATE_DEC>2014-05-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>355580</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème / 4ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>HAAS ; SCP BORE, SALVE DE BRUNETON</AVOCATS>
<RAPPORTEUR>M. Charles Touboul</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:355580.20140505</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et mémoire complémentaire, enregistrés les 5 janvier et 5 avril 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. B... A..., demeurant... ; M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement n° 0904849 du 25 mai 2011 du tribunal administratif de Lille rejetant sa demande tendant à l'annulation de la décision du directeur du centre hospitalier de Sambre-Avesnois refusant de réévaluer sa rémunération et à ce qu'il soit enjoint au centre hospitalier de procéder à la réévaluation de sa rémunération et d'en tirer les conséquences sur les indemnités dues au titre de la résiliation de son contrat de travail ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ; <br/>
<br/>
              3°) de mettre à la charge du centre hospitalier de Sambre-Avesnois la somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution ;<br/>
<br/>
              Vu la directive 99/70/CE du Conseil du 28 juin 1999 concernant l'accord-cadre CES, UNICE et CEEP sur le travail à durée déterminée ;<br/>
<br/>
              Vu l'arrêt de la Cour de justice de l'Union européenne n° C-268/06 du 15 avril 2008 ;<br/>
<br/>
              Vu le code de la santé publique ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Charles Touboul, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Haas, avocat de M. A...et à la SCP Boré, Salve de Bruneton, avocat du centre hospitalier de Sambre-Avesnois ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. A...a été recruté à compter du 16 mars 2009 comme praticien hospitalier sous contrat à durée déterminée par le centre hospitalier de Sambre-Avesnois ; que, lors d'un entretien le 25 mars 2009, M. A...a demandé au directeur de cet établissement de réévaluer sa rémunération afin de tenir compte de la durée des services qu'il avait accomplis auparavant dans des fonctions comparables ; que, par un courrier du même jour, le directeur a résilié son contrat ; que M. A...a demandé au tribunal administratif de Lille, d'une part, d'annuler la décision implicite de rejet née du silence gardé par le directeur du centre hospitalier sur sa demande de réévaluation de sa rémunération et, d'autre part, d'enjoindre à l'établissement de procéder à cette réévaluation et d'en tirer les conséquences  sur les indemnités dues au titre de la résiliation du contrat de travail ; que M. A...se pourvoit en cassation contre le jugement du  25 mai 2011 par lequel le tribunal administratif a rejeté l'ensemble de ces conclusions ;<br/>
<br/>
              Sur la régularité du jugement attaqué :<br/>
<br/>
              2. Considérant que si le tribunal n'a pas communiqué à M. A...le mémoire en réplique présenté par le centre hospitalier de Sambre-Avesnois le 5 mai 2011, cette circonstance est sans incidence sur la  régularité du jugement attaqué dont les motifs sont fondés exclusivement sur des faits et des moyens exposés dans des mémoires qui ont été communiqués à l'intéressé ;<br/>
<br/>
              3. Considérant qu'en relevant, pour écarter le moyen de M. A...tiré de ce que le refus du centre hospitalier de Sambre-Avesnois de prendre en compte son ancienneté pour le calcul de sa rémunération était contraire aux prescriptions de la directive 99/70/CE du Conseil du 28 juin 1999, que les conditions d'emploi mentionnées à la clause 4 de l'accord-cadre mis en oeuvre par cette directive n'incluaient pas les conditions de rémunération, le tribunal a suffisamment motivé son jugement ;<br/>
<br/>
              Sur le bien fondé du jugement :<br/>
<br/>
              4. Considérant que, pour soutenir que sa rémunération devait être réévaluée pour tenir compte de l'expérience qu'il avait acquise dans ses fonctions antérieures, M. A...s'est fondé devant les juges du fond sur les règles de l'accord-cadre sur le travail à durée déterminée conclu le 18 mars 1999 entre les organisations interprofessionnelles à vocation générale (CES, UNICE, CEEP) et mis en oeuvre par la directive 99/70/CE du Conseil du 28 juin 1999 ;<br/>
<br/>
              5. Considérant qu'aux termes de son article 1er, la directive du 28 juin 1999  " vise à mettre en oeuvre l'accord-cadre sur le travail à durée déterminée, figurant en annexe, conclu le 18 mars 1999 entre les organisations interprofessionnelles à vocation générale (CES, UNICE, CEEP) " ; qu'aux termes de la clause 4 de cet accord-cadre, annexé à la directive, relative au principe de non-discrimination " 1. Pour ce qui concerne les conditions d'emploi, les travailleurs à durée déterminée ne sont pas traités d'une manière moins favorable que les travailleurs à durée indéterminée comparables au seul motif qu'ils travaillent à durée déterminée, à moins qu'un traitement différent soit justifié par des raisons objectives. / 2. Lorsque c'est approprié, le principe du "pro rata temporis" s'applique. /  3. Les modalités d'application de la présente clause sont définies par les États membres, après consultation des partenaires sociaux, et/ou par les partenaires sociaux, compte tenu de la législation Communautaire et la législation, des conventions collectives et pratiques nationales./ 4. Les critères de périodes d'ancienneté relatifs à des conditions particulières d'emploi sont les mêmes pour les travailleurs à durée déterminée que pour les travailleurs à durée indéterminée, sauf lorsque des critères de périodes d'ancienneté différents sont justifiées par des raisons objectives " ; que le délai de transposition de cette directive expirait le 10 juillet 2001 ; que la clause 4 précitée, dont le contenu est précis et inconditionnel, n'avait pas été transposée à la date des décisions litigieuses et pouvait dès lors être utilement invoquée par le requérant devant le tribunal administratif à l'appui de sa demande ;<br/>
<br/>
              6. Considérant que, par son arrêt du 15 avril 2008 visé ci-dessus, la Cour de justice de l'Union européenne s'est prononcée sur la question de savoir si les conditions d'emploi, au sens de la clause 4 de l'accord-cadre, comprennent les conditions relatives aux rémunérations et aux pensions fixées par un contrat de travail ; qu'elle a dit pour droit que cette clause doit être interprétée en ce sens que les conditions d'emploi auxquelles elle se réfère incluent les conditions relatives aux rémunérations ainsi qu'aux pensions qui sont fonction de la relation d'emploi, à l'exclusion des conditions concernant les pensions découlant d'un régime légal de sécurité sociale ; que, dès lors, en jugeant que les conditions d'emploi au sens de cette clause n'incluaient pas les conditions de rémunération des travailleurs, le tribunal administratif de Lille a entaché son jugement d'une erreur de droit ; <br/>
<br/>
              7. Mais considérant que les stipulations précitées de l'accord cadre du 18 mars 1999 ne concernent que les différences de traitement entre les travailleurs à durée déterminée et les travailleurs à durée indéterminée, à raison même de la durée de la relation de travail ; que les dispositions de l'article R. 6152-416 du code de la santé publique relatives à la rémunération des praticiens contractuels ne prévoient aucune reprise d'ancienneté, qu'il s'agisse des praticiens sous contrat à durée déterminée ou des praticiens sous contrat à durée indéterminée ; que si les dispositions de l'article R. 6152-15 prévoient une reprise d'ancienneté pour les praticiens titulaires, la différence de traitement qui en résulte entre praticiens contractuels et praticiens titulaires est étrangère à la durée de la relation de travail ; qu'une telle différence de traitement n'entre pas dans le champ d'application de la clause 4 de l'accord-cadre ;<br/>
<br/>
              8. Considérant que ce motif, dont l'examen n'implique l'appréciation d'aucune circonstance de fait, doit être substitué au motif erroné en droit retenu par le jugement attaqué, dont il justifie le dispositif ; que, par suite, le pourvoi de M. A...doit être rejeté, y compris ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de M. A... une somme globale de 3 000 euros à verser au centre hospitalier de Sambre-Avesnois au titre des mêmes dispositions, pour la présente instance et pour l'instance devant le tribunal administratif de Lille ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
		Article 1er : Le pourvoi de M. A...est rejeté.<br/>
Article 2 : M. A... versera au centre hospitalier de Sambre-Avesnois la somme globale de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
Article 3 : La présente décision sera notifiée à M. B...A..., au centre hospitalier de Sambre-Avesnois et à la ministre des affaires sociales.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">15-03-03-01 COMMUNAUTÉS EUROPÉENNES ET UNION EUROPÉENNE. APPLICATION DU DROIT DE L'UNION EUROPÉENNE PAR LE JUGE ADMINISTRATIF FRANÇAIS. PRISE EN COMPTE DES ARRÊTS DE LA COUR DE JUSTICE. INTERPRÉTATION DU DROIT DE L'UNION. - ACCORD-CADRE SUR LE TRAVAIL À DURÉE DÉTERMINÉE MIS EN &#140;UVRE PAR LA DIRECTIVE DU CONSEIL DE L'UNION EUROPÉENNE DU 28 JUIN 1999 - CLAUSE 4 RELATIVE AUX CONDITIONS D'EMPLOI DES TRAVAILLEURS À DURÉE DÉTERMINÉE - NOTION DE CONDITIONS D'EMPLOI - INCLUSION - RÉMUNÉRATION [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">15-05-085 COMMUNAUTÉS EUROPÉENNES ET UNION EUROPÉENNE. RÈGLES APPLICABLES. - ACCORD-CADRE SUR LE TRAVAIL À DURÉE DÉTERMINÉE MIS EN &#140;UVRE PAR LA DIRECTIVE DU CONSEIL DE L'UNION EUROPÉENNE DU 28 JUIN 1999 - CLAUSE 4 RELATIVE AUX CONDITIONS D'EMPLOI DES TRAVAILLEURS À DURÉE DÉTERMINÉE - NOTION DE CONDITIONS D'EMPLOI - INCLUSION - RÉMUNÉRATION [RJ1].
</SCT>
<ANA ID="9A"> 15-03-03-01 Par son arrêt n° C-268/06 du 15 avril 2008, la Cour de justice de l'Union européenne a dit pour droit que la clause 4 de l'accord-cadre sur le travail à durée déterminée conclu le 18 mars 1999 entre les organisations interprofessionnelles à vocation générale (CES, UNICE, CEEP) et mis en oeuvre par la directive 1999/70/CE du Conseil de l'Union européenne du 28 juin 1999 doit être interprétée en ce sens que les conditions d'emploi auxquelles elle se réfère incluent les conditions relatives aux rémunérations ainsi qu'aux pensions qui sont fonction de la relation d'emploi, à l'exclusion des conditions concernant les pensions découlant d'un régime légal de sécurité sociale. En conséquence, erreur de droit à avoir estimé que les conditions d'emploi au sens de cette clause n'incluaient pas les conditions de rémunération des travailleurs.</ANA>
<ANA ID="9B"> 15-05-085 Par son arrêt n° C-268/06 du 15 avril 2008, la Cour de justice de l'Union européenne a dit pour droit que la clause 4 de l'accord-cadre sur le travail à durée déterminée conclu le 18 mars 1999 entre les organisations interprofessionnelles à vocation générale (CES, UNICE, CEEP) et mis en oeuvre par la directive 1999/70/CE du Conseil de l'Union européenne du 28 juin 1999 doit être interprétée en ce sens que les conditions d'emploi auxquelles elle se réfère incluent les conditions relatives aux rémunérations ainsi qu'aux pensions qui sont fonction de la relation d'emploi, à l'exclusion des conditions concernant les pensions découlant d'un régime légal de sécurité sociale. En conséquence, erreur de droit à avoir estimé que les conditions d'emploi au sens de cette clause n'incluaient pas les conditions de rémunération des travailleurs.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Ab. jur. CE, 15 juin 2005, Syndicat des personnels de l'administration centrale du ministère de la justice et de la légion d'honneur (CFDT), n° 254624, inédite au Recueil.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
