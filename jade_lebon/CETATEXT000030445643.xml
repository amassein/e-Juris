<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030445643</ID>
<ANCIEN_ID>JG_L_2015_03_000000373158</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/44/56/CETATEXT000030445643.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème / 5ème SSR, 18/03/2015, 373158</TITRE>
<DATE_DEC>2015-03-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>373158</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème / 5ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP RICHARD ; SCP BARTHELEMY, MATUCHANSKY, VEXLIARD, POUPOT</AVOCATS>
<RAPPORTEUR>Mme Florence Chaltiel-Terral</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Maud Vialettes</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:373158.20150318</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 6 novembre 2013 et 6 février 2014 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. B...A..., demeurant au ...; M. A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision n° 11452 du 6 septembre 2013 par laquelle la chambre disciplinaire nationale de l'ordre des médecins, réformant la décision n° 10.36.1581 du 20 septembre 2011 de la chambre disciplinaire de première instance des Pays de la Loire, statuant sur la plainte du conseil départemental de l'ordre des médecins de Loire-Atlantique lui ayant infligé un blâme, lui a infligé un avertissement ; <br/>
<br/>
              2°) de mettre à la charge du conseil départemental de l'ordre des médecins de Loire-Atlantique la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de la santé publique ; <br/>
<br/>
              Vu la loi n° 2009-879 du 21 juillet 2009 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Florence Chaltiel-Terral, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Richard, avocat de M. A...et à la SCP Barthélemy, Matuchansky, Vexliard, Poupot, avocat du Conseil national de l'ordre des médecins ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, dans le cadre d'un litige dont était saisi le tribunal de grande instance de Saint-Nazaire, le président de ce tribunal, a, par une ordonnance du 3 juillet 2007, ordonné une expertise sur l'état de santé de M. S. et désigné M.A..., médecin spécialisé en psychiatrie, pour réaliser cette expertise ; que, par une ordonnance en date du 1er juillet 2008, le magistrat chargé du suivi de l'expertise a, en raison du manque de diligence de M. A...dans l'exercice de sa mission d'expertise, prononcé la caducité de sa désignation et nommé un nouvel expert ; que, saisi par M. S. d'une plainte contre M. A...à raison de ces faits, le conseil départemental a, par une décision du 2 septembre 2010, engagé une procédure disciplinaire à l'encontre de l'intéressé ; que par une décision du 20 septembre 2011, la chambre disciplinaire de première instance des Pays de la Loire a infligé à M. A...un blâme ; que M. A...se pourvoit en cassation contre la décision du 6 septembre 2013 par laquelle la chambre disciplinaire nationale de l'ordre des médecins lui a infligé un avertissement ; <br/>
<br/>
              2. Considérant, en premier lieu, qu'aux termes de l'article L. 4124-2 du code de la santé publique, dans sa version en vigueur à la date des faits litigieux : " Les médecins, les chirurgiens-dentistes ou les sages-femmes chargés d'un service public et inscrits au tableau de l'ordre ne peuvent être traduits devant la chambre disciplinaire de première instance, à l'occasion des actes de leur fonction publique, que par le ministre chargé de la santé, le représentant de l'Etat dans le département, le procureur de la République ou, lorsque lesdits actes ont été réalisés dans un établissement public de santé, le directeur de l'agence régionale de l'hospitalisation " ; que l'article 62 de la loi du 21 juillet 2009 portant réforme de l'hôpital et relative aux patients, à la santé et aux territoires, entré en vigueur le 23 juillet 2009, a modifié ces dispositions pour remplacer, parmi les autorités ayant qualité pour saisir le juge disciplinaire, le directeur de l'agence régionale de l'hospitalisation par le directeur de l'agence régionale de santé, et pour y ajouter le Conseil national de l'ordre des médecins et le conseil départemental au tableau duquel le praticien est inscrit ; <br/>
<br/>
              3. Considérant que si, en matière d'édiction de sanction administrative, sont seuls punissables les fais constitutifs d'un manquement à des obligations définies par des dispositions législatives ou réglementaires en vigueur à la date où ces faits ont été commis, en revanche, et réserve faite du cas où il en serait disposé autrement, s'appliquent immédiatement les textes fixant les modalités des poursuites et les formes de la procédure à suivre, alors même qu'ils ouvrent une possibilité de réprimer des manquements commis avant leur entrée en vigueur ; que les dispositions rappelées ci-dessus de l'article 62 de la loi du 21 juillet 2009, qui désignent de nouvelles autorités ayant compétence pour saisir le juge disciplinaire, sont immédiatement applicables indépendamment de la date des faits faisant objet de la poursuite ; qu'est à cet égard sans incidence la circonstance que ces faits auraient été commis par un praticien chargé d'un service public dans l'exercice de sa fonction publique ; <br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède que, contrairement à ce que soutient M.A..., la chambre disciplinaire nationale n'a pas commis d'erreur de droit en jugeant que le conseil départemental était compétent pour engager une procédure disciplinaire à son encontre, alors même que les actes qui faisaient l'objet de la poursuite avaient été accomplis avant l'entrée en vigueur de l'article 62 de la loi du 21 juillet 2009 ;  <br/>
<br/>
              5. Considérant, en deuxième lieu, qu'il ressort des énonciations de la décision attaquée que la chambre disciplinaire nationale a retenu qu'avant d'engager des poursuites le conseil départemental de Loire-Atlantique, auteur de la plainte, s'était borné à manifester l'intention de prendre une décision de classement sans suite ; que par suite, le moyen tiré de ce que la chambre disciplinaire nationale aurait commis une erreur de droit en jugeant que des poursuites pouvaient être régulièrement engagées après une décision de classement sans suite est inopérant ;<br/>
<br/>
              6. Considérant, enfin, qu'en vertu de l'article R. 4127-31 du code de la santé publique, le médecin doit s'abstenir de tout acte de nature à déconsidérer la profession ; que la chambre disciplinaire a notamment retenu que M.A..., désigné le 3 juillet 2007 en qualité d'expert judiciaire, n'a organisé sa première mission d'expertise que le 15 février 2008 et n'avait pas remis de rapport le 1er juillet 2008, date à laquelle le magistrat chargé du suivi des expertises a prononcé la caducité de sa désignation et nommé un autre expert ; qu'en jugeant, par une décision suffisamment motivée sur ce point, que ces faits étaient constitutifs d'un manque de diligence de nature à déconsidérer la profession de médecin et, par suite, fautifs, la chambre disciplinaire nationale n'a pas inexactement qualifié les faits qui lui étaient soumis et n'a pas commis d'erreur de droit ; <br/>
<br/>
              7. Considérant qu'il résulte de tout ce qui précède que le pourvoi de M. A...doit être rejeté ;<br/>
<br/>
              8. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge du conseil départemental de l'ordre des médecins de Loire-Atlantique, qui n'est pas, dans la présente instance, la partie perdante ; <br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
Article 1er : Le pourvoi de M. A...est rejeté.<br/>
Article 2 : La présente décision sera notifiée à M. B...A...et au Conseil national de l'ordre des médecins. <br/>
Copie en sera adressée pour information au conseil départemental de l'ordre des médecins de Loire-Atlantique.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-07-06 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. POUVOIRS DU JUGE DISCIPLINAIRE. - ENGAGEMENT DES POURSUITES DISCIPLINAIRES - DISCIPLINE DES PROFESSIONS DE SANTÉ - MODIFICATION DES AUTORITÉS POUVANT ENGAGER LES POURSUITES - LOI DE PROCÉDURE IMMÉDIATEMENT APPLICABLE, Y COMPRIS POUR LES FAITS ANTÉRIEURS À CETTE MODIFICATION.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">55-04-01-01 PROFESSIONS, CHARGES ET OFFICES. DISCIPLINE PROFESSIONNELLE. PROCÉDURE DEVANT LES JURIDICTIONS ORDINALES. INTRODUCTION DE L'INSTANCE. - DISCIPLINE DES PROFESSIONS DE SANTÉ - MODIFICATION DES AUTORITÉS POUVANT ENGAGER LES POURSUITES - LOI DE PROCÉDURE IMMÉDIATEMENT APPLICABLE, Y COMPRIS POUR LES FAITS ANTÉRIEURS À CETTE MODIFICATION.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">59-02-02-01 RÉPRESSION. DOMAINE DE LA RÉPRESSION ADMINISTRATIVE RÉGIME DE LA SANCTION ADMINISTRATIVE. AUTORITÉS ADMINISTRATIVES TITULAIRES DU POUVOIR DE SANCTION. - MODIFICATION DES AUTORITÉS HABILITÉS À ENGAGER DES POURSUITES DISCIPLINAIRES - LOI DE PROCÉDURE IMMÉDIATEMENT APPLICABLE, Y COMPRIS POUR LES FAITS ANTÉRIEURS À CETTE MODIFICATION.
</SCT>
<ANA ID="9A"> 54-07-06 L'article 62 de la loi n° 2009-879 du 21 juillet 2009 a prévu que les poursuites disciplinaires contre un médecin chargé d'un service public pourraient désormais être engagées, non seulement par les autorités de l'Etat prévues antérieurement, mais aussi par le Conseil national de l'ordre des médecins et le conseil départemental au tableau duquel le praticien est inscrit.... ,,Si, en matière d'édiction de sanction administrative, sont seuls punissables les faits constitutifs d'un manquement à des obligations définies par des dispositions législatives ou réglementaires en vigueur à la date où ces faits ont été commis, en revanche, et réserve faite du cas où il en serait disposé autrement, s'appliquent immédiatement les textes fixant les modalités des poursuites et les formes de la procédure à suivre, alors même qu'ils ouvrent une possibilité de réprimer des manquements commis avant leur entrée en vigueur [RJ1]. Les dispositions de l'article 62 de la loi n° 2009-879 du 21 juillet 2009, qui désignent de nouvelles autorités ayant compétence pour saisir le juge disciplinaire, sont immédiatement applicables indépendamment de la date des faits faisant objet de la poursuite.</ANA>
<ANA ID="9B"> 55-04-01-01 L'article 62 de la loi n° 2009-879 du 21 juillet 2009 a prévu que les poursuites disciplinaires contre un médecin chargé d'un service public pourraient désormais être engagées, non seulement par les autorités de l'Etat prévues antérieurement, mais aussi par le Conseil national de l'ordre des médecins et le conseil départemental au tableau duquel le praticien est inscrit.... ,,Si, en matière d'édiction de sanction administrative, sont seuls punissables les faits constitutifs d'un manquement à des obligations définies par des dispositions législatives ou réglementaires en vigueur à la date où ces faits ont été commis, en revanche, et réserve faite du cas où il en serait disposé autrement, s'appliquent immédiatement les textes fixant les modalités des poursuites et les formes de la procédure à suivre, alors même qu'ils ouvrent une possibilité de réprimer des manquements commis avant leur entrée en vigueur. Les dispositions de l'article 62 de la loi n° 2009-879 du 21 juillet 2009, qui désignent de nouvelles autorités ayant compétence pour saisir le juge disciplinaire, sont immédiatement applicables indépendamment de la date des faits faisant objet de la poursuite.</ANA>
<ANA ID="9C"> 59-02-02-01 L'article 62 de la loi n° 2009-879 du 21 juillet 2009 a prévu que les poursuites disciplinaires contre un médecin chargé d'un service public pourraient désormais être engagées, non seulement par les autorités de l'Etat prévues antérieurement, mais aussi par le Conseil national de l'ordre des médecins et le conseil départemental au tableau duquel le praticien est inscrit.... ,,Si, en matière d'édiction de sanction administrative, sont seuls punissables les faits constitutifs d'un manquement à des obligations définies par des dispositions législatives ou réglementaires en vigueur à la date où ces faits ont été commis, en revanche, et réserve faite du cas où il en serait disposé autrement, s'appliquent immédiatement les textes fixant les modalités des poursuites et les formes de la procédure à suivre, alors même qu'ils ouvrent une possibilité de réprimer des manquements commis avant leur entrée en vigueur. Les dispositions de l'article 62 de la loi n° 2009-879 du 21 juillet 2009, qui désignent de nouvelles autorités ayant compétence pour saisir le juge disciplinaire, sont immédiatement applicables indépendamment de la date des faits faisant objet de la poursuite.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Cf. CE, Section, 17 novembre 2006, CNP Assurances, n° 276926, p. 473.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
