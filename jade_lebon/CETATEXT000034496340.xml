<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034496340</ID>
<ANCIEN_ID>JG_L_2017_04_000000394651</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/49/63/CETATEXT000034496340.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 26/04/2017, 394651</TITRE>
<DATE_DEC>2017-04-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>394651</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>BROUCHOT</AVOCATS>
<RAPPORTEUR>M. Luc Briand</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier Domino</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:394651.20170426</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 3 avril 2015 au greffe du tribunal administratif de Montpellier, M. D...et Mme C...ont demandé, d'une part, l'annulation pour excès de pouvoir de la décision implicite par laquelle le ministre de l'intérieur a rejeté leur demande tendant à l'abrogation de la circulaire n° INTD1237286C du 20 novembre 2012 relative à la décision judiciaire d'interdiction de sortie du territoire et à la mesure administrative conservatoire d'opposition à la sortie du territoire des mineurs, d'autre part, la condamnation de l'Etat à leur verser à chacun une indemnité de 50 000 euros en réparation du préjudice moral qu'ils estiment avoir subi du fait de la faute des services de police ayant permis la sortie du territoire de leur fille mineure et, enfin, à ce que soit mise à la charge de l'Etat une somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Par une ordonnance n° 1501964 du 11 juin 2015, le président du tribunal administratif de Montpellier a transmis cette demande au tribunal administratif de Montreuil en application de l'article R. 351-3 du code de justice administrative. <br/>
<br/>
              Par une ordonnance n° 1505229 du 5 novembre 2015, enregistrée au secrétariat du contentieux du Conseil d'Etat le 18 novembre 2015, le président du tribunal administratif de Montreuil a transmis au Conseil d'Etat cette demande et le nouveau mémoire produit par M. et MmeA..., enregistré le 22 octobre 2015 au greffe du tribunal administratif de Montreuil, en application des articles R. 341-3 et R. 341-4 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le règlement n° 562/2006/CE du Parlement européen et du Conseil du 15 mars 2006 ;<br/>
              - la loi n° 2016-731 du 3 juin 2016 ;<br/>
              - le décret n° 2016-1483 du 2 novembre 2016 ; <br/>
              - le code civil ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Luc Briand, maître des requêtes en service extraordinaire, <br/>
<br/>
              - les conclusions de M. Xavier Domino, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Brouchot, avocat de M. et Mme A...;  <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Sur les conclusions d'excès de pouvoir :<br/>
<br/>
              1.	Considérant que les conclusions d'excès de pouvoir présentées par M. et Mme A..., dirigées contre le refus opposé à leur demande d'abrogation de la circulaire interministérielle du 20 novembre 2012 relative aux décisions judiciaires d'interdiction de sortie du territoire et aux mesures administratives conservatoires d'opposition à la sortie du territoire des mineurs, laquelle avait abrogé les dispositions de la circulaire du ministre de l'intérieur du 11 mai 1990 exigeant des ressortissants français mineurs quittant seuls le territoire de détenir une autorisation parentale de sortie du territoire, doivent être regardées comme tendant, en réalité, à ce que soit institué à nouveau un dispositif exigeant des ressortissants français mineurs d'être munis d'une autorisation parentale pour quitter seuls le territoire français ; <br/>
<br/>
              2.	Considérant que, postérieurement à l'introduction de ces conclusions, l'article 49 de la loi du 3 juin 2016 renforçant la lutte contre le crime organisé, le terrorisme et leur financement et améliorant l'efficacité et les garanties de la procédure pénale a inséré au code civil un article 371-6 ainsi rédigé : " L'enfant quittant le territoire national sans être accompagné d'un titulaire de l'autorité parentale est muni d'une autorisation de sortie du territoire signée d'un titulaire de l'autorité parentale. / Un décret en Conseil d'Etat détermine les conditions d'application du présent article " ; qu'en application de ces dispositions, le décret du 2 novembre 2016 relatif à l'autorisation de sortie du territoire d'un mineur non accompagné par le titulaire de l'autorité parentale a prévu que l'autorisation de sortie du territoire est rédigée par un titulaire de l'autorité parentale au moyen d'un formulaire dont le modèle est fixé par arrêté et que le mineur doit joindre à cette autorisation la photocopie d'un document officiel, dont la liste est fixée par arrêté, justifiant de l'identité du titulaire de l'autorité parentale qui a signé l'autorisation ; que l'arrêté permettant l'application de ces dispositions a été pris le 13 décembre 2016 ;<br/>
<br/>
              3.	Considérant que ces dispositions législatives et réglementaires ont ainsi institué un dispositif exigeant des ressortissants français mineurs d'être munis d'une autorisation signée d'un titulaire de l'autorité parentale pour quitter seuls le territoire français ; que, dans ces conditions, les conclusions d'excès de pouvoir de M. et Mme A...ont perdu leur objet ; qu'il n'y a, dès lors, plus lieu d'y statuer ; <br/>
<br/>
              Sur les conclusions à fins d'indemnité :<br/>
<br/>
              4.	Considérant que M. et Mme A...demandent réparation du préjudice moral qu'ils estiment avoir subi du fait du départ, le 11 novembre 2013, de leur fille mineure, alors âgée de 17 ans, sur un vol qui a quitté l'aéroport de Paris-Orly à destination d'Istanbul, d'où elle a rejoint la Syrie ;<br/>
<br/>
              5.	Considérant qu'il résulte de l'instruction et n'est pas contesté que la jeune fille avait été inscrite sur le fichier des personnes recherchées après avoir quitté le domicile de ses parents le 5 juin 2013 ; qu'elle était toujours inscrite sur ce fichier le 11 novembre 2013, alors qu'elle a embarqué à l'aéroport de Paris-Orly sur un vol à destination d'Istanbul ; que les fonctionnaires en charge du contrôle des frontières à l'aéroport ne se sont pas opposés à cet embarquement, faute d'avoir consulté ou d'avoir correctement consulté le fichier des personnes recherchées, contrairement à ce que prescrit la circulaire du 20 novembre 2012, afin de s'assurer que la jeune fille ne faisait pas l'objet d'une interdiction judiciaire de sortie du territoire ou d'une opposition à sortie du territoire ; qu'en l'absence de circonstances particulières susceptibles de justifier l'allègement de la surveillance qui doit être normalement exercée sur le départ de mineurs du territoire national, et alors que le ministre n'établit pas que la jeune fille se serait livrée à des manoeuvres destinées à tromper la vigilance des services de contrôle des frontières, la négligence commise a été constitutive d'une faute qui a rendu possible la sortie du territoire de la jeune fille ; que cette faute est de nature à engager la responsabilité de l'Etat ; <br/>
<br/>
              6.	Considérant qu'il sera fait une juste appréciation du préjudice moral subi par M. et Mme A...en leur allouant une indemnité globale d'un montant de 15 000 euros ; <br/>
<br/>
              Sur les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              7.	Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement à M. et Mme A...d'une somme de 3 000 euros au titre des frais exposés et non compris dans les dépens ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Il n'y a pas lieu de statuer sur les conclusions à fin d'annulation de la requête de M. et MmeA....<br/>
<br/>
Article 2 : L'Etat est condamné à verser à M. et Mme A...une indemnité globale de 15 000 euros. <br/>
<br/>
Article 3 : L'Etat versera à M. et Mme A...une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à M. B...et Mme C...et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">49-05 POLICE. POLICES SPÉCIALES. - SORTIE DU TERRITOIRE - JEUNE FILLE MINEURE AYANT EMBARQUÉ À DESTINATION D'ISTANBUL ALORS QU'ELLE ÉTAIT INSCRITE SUR LE FICHIER DES PERSONNES RECHERCHÉES - FAUTE DES SERVICES DE CONTRÔLE DES FRONTIÈRES DE NATURE À ENGAGER LA RESPONSABILITÉ DE L'ETAT - EXISTENCE EN L'ESPÈCE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">60-02-03 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RESPONSABILITÉ EN RAISON DES DIFFÉRENTES ACTIVITÉS DES SERVICES PUBLICS. SERVICES DE POLICE. - SURVEILLANCE DE LA SORTIE DU TERRITOIRE - JEUNE FILLE MINEURE AYANT EMBARQUÉ À DESTINATION D'ISTANBUL ALORS QU'ELLE ÉTAIT INSCRITE SUR LE FICHIER DES PERSONNES RECHERCHÉES - FAUTE DES SERVICES DE CONTRÔLE DES FRONTIÈRES DE NATURE À ENGAGER LA RESPONSABILITÉ DE L'ETAT - EXISTENCE EN L'ESPÈCE [RJ1].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">60-04-03-04 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RÉPARATION. ÉVALUATION DU PRÉJUDICE. PRÉJUDICE MORAL. - PRÉJUDICE MORAL D'UN COUPLE DU FAIT DU DÉPART DE LEUR FILLE MINEURE À DESTINATION D'ISTANBUL, D'OÙ ELLE A REJOINT LA SYRIE, EN RAISON DE LA NÉGLIGENCE DES SERVICES DE CONTRÔLE DES FRONTIÈRES.
</SCT>
<ANA ID="9A"> 49-05 Jeune fille ayant embarqué à l'aéroport de Paris-Orly sur un vol à destination d'Istanbul alors qu'elle était inscrite sur le fichier des personnes recherchées. Les fonctionnaires en charge du contrôle des frontières à l'aéroport ne se sont pas opposés à cet embarquement, faute d'avoir consulté ou d'avoir correctement consulté le fichier des personnes recherchées, contrairement à ce que prescrivait une circulaire en vigueur, afin de s'assurer que la jeune fille ne faisait pas l'objet d'une interdiction judiciaire de sortie du territoire ou d'une opposition à sortie du territoire. En l'absence de circonstances particulières susceptibles de justifier l'allègement de la surveillance qui doit être normalement exercée sur le départ de mineurs du territoire national, et alors que le ministre n'établit pas que la jeune fille se serait livrée à des manoeuvres destinées à tromper la vigilance des services de contrôle des frontières, la négligence commise a été constitutive d'une faute qui a rendu possible la sortie du territoire de la jeune fille. Cette faute est de nature à engager la responsabilité de l'Etat.</ANA>
<ANA ID="9B"> 60-02-03 Jeune fille ayant embarqué à l'aéroport de Paris-Orly sur un vol à destination d'Istanbul alors qu'elle était inscrite sur le fichier des personnes recherchées. Les fonctionnaires en charge du contrôle des frontières à l'aéroport ne se sont pas opposés à cet embarquement, faute d'avoir consulté ou d'avoir correctement consulté le fichier des personnes recherchées, contrairement à ce que prescrivait une circulaire en vigueur, afin de s'assurer que la jeune fille ne faisait pas l'objet d'une interdiction judiciaire de sortie du territoire ou d'une opposition à sortie du territoire. En l'absence de circonstances particulières susceptibles de justifier l'allègement de la surveillance qui doit être normalement exercée sur le départ de mineurs du territoire national, et alors que le ministre n'établit pas que la jeune fille se serait livrée à des manoeuvres destinées à tromper la vigilance des services de contrôle des frontières, la négligence commise a été constitutive d'une faute qui a rendu possible la sortie du territoire de la jeune fille. Cette faute est de nature à engager la responsabilité de l'Etat.</ANA>
<ANA ID="9C"> 60-04-03-04 Jeune fille ayant embarqué à l'aéroport de Paris-Orly sur un vol à destination d'Istanbul, d'où elle a rejoint la Syrie, du fait d'une négligence des services de contrôle des frontières. Allocation d'une somme de 15 000 euros aux parents en réparation de leur préjudice moral.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Cf., sur l'existence d'un régime de faute simple, CE, 9 décembre 2015, M.,et Mme,, n° 386817, T. pp. 714-782-867.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
