<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030625066</ID>
<ANCIEN_ID>JG_L_2015_05_000000376079</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/62/50/CETATEXT000030625066.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème SSR, 22/05/2015, 376079</TITRE>
<DATE_DEC>2015-05-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>376079</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROUSSEAU, TAPIE ; FOUSSARD</AVOCATS>
<RAPPORTEUR>M. Stéphane Bouchard</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Bertrand Dacosta</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2015:376079.20150522</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Paris, d'une part, d'annuler pour excès de pouvoir l'arrêté du 2 juillet 2012 par lequel le maire de Paris a mis fin à son détachement dans un emploi fonctionnel de sous-directeur de la commune de Paris à compter du 19 juillet 2012 et l'a réintégré dans son corps d'origine et, d'autre part, d'enjoindre au maire de Paris de le réintégrer dans l'emploi de sous-directeur à compter du 19 juillet 2012 et de reconstituer sa carrière.<br/>
<br/>
              Par un jugement n° 1219746/2-3 du 26 septembre 2013 le magistrat désigné par le président du tribunal administratif de Paris a rejeté sa demande.<br/>
<br/>
              Par une ordonnance n° 13PA04412 du 11 février 2014 le président de la cour administrative d'appel de Paris a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, le pourvoi présenté à cette cour par M.A....<br/>
<br/>
              Par un pourvoi, enregistré le 4 décembre 2013 au greffe de la cour administrative d'appel de Paris et des mémoires complémentaires enregistrés les 25 mars et 20 mai 2014 au secrétariat du contentieux du Conseil d'Etat, M. A...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande présentée au tribunal administratif de Paris ; <br/>
<br/>
              3°) de mettre à la charge de la ville de Paris le versement de la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - la loi n° 84-53 du 26 janvier 1984 ;<br/>
              - le décret n° 77-187 du 1er mars 1977 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Stéphane Bouchard, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Bertrand Dacosta, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rousseau, Tapie, avocat de M. A...et à Me Foussard, avocat de la ville de Paris ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis au juge du fond que M. A..., administrateur de la ville de Paris, détaché dans l'emploi fonctionnel de sous-directeur des interventions sociales et de la santé au sein de la direction des ressources humaines de la ville de Paris a, par un arrêté du maire de Paris du 6 décembre 2010, été nommé, à compter du 15 novembre 2010, dans l'emploi de chargé de mission au sein de la direction du patrimoine et de l'architecture et " maintenu en détachement sur un emploi de sous-directeur " ; que, par un arrêté du 2 juillet 2012, le maire de Paris a mis fin, à compter du 19 juillet 2012, au détachement de M. A...dans un emploi de sous-directeur et a réintégré l'intéressé dans son corps d'origine ; que M. A...se pourvoit en cassation contre le jugement du tribunal administratif de Paris du 26 septembre 2013 ayant rejeté sa demande d'annulation pour excès de pouvoir de l'arrêté du 2 juillet 2012, ainsi que d'injonction aux fins de réintégration dans cet emploi de sous-directeur et de reconstitution de sa carrière ;<br/>
<br/>
              2. Considérant qu'il ressort des mentions du jugement attaqué que l'audience du tribunal administratif au cours de laquelle la demande de M. A...a été examinée a été publique ; que, le requérant, qui n'apporte aucun élément de nature à contredire cette mention, n'est pas fondé à soutenir que le jugement a été rendu au terme d'une procédure irrégulière ;<br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier soumis au juge du fond que l'arrêté du 6 décembre 2010 mentionné ci-dessus maintenant M. A...en détachement dans un emploi fonctionnel de sous-directeur, qui n'avait pas pour objet de pourvoir à un emploi vacant de sous-directeur et qui n'était pas accompagné de l'affectation dans les fonctions correspondantes, mais seulement de lui confier des fonctions de chargé de mission, constituait une nomination pour ordre, nulle et non avenue, à laquelle l'administration était tenue de mettre fin ; qu'ainsi les moyens soulevés par M. A...devant le juge du fond et dirigés contre la décision mettant fin à ce détachement étaient inopérants ; qu'il convient de les écarter pour ce motif, qui doit être substitué aux motifs retenus par le jugement attaqué ; <br/>
<br/>
              4. Considérant que le moyen tiré de ce que le jugement serait irrégulier au motif qu'il ne vise ni ne mentionne la loi du 11 juillet 1979 relative à la motivation des actes administratifs et à l'amélioration des relations entre l'administration et le public doit être écarté, en raison de la substitution de motif opérée au point 3 ;<br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède que M. A...n'est pas fondé à demander l'annulation de ce jugement ; <br/>
<br/>
              5. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que le versement d'une somme soit mis à la charge de la ville de Paris, qui n'est pas dans la présente instance la partie perdante ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par la ville de Paris au titre de ces mêmes dispositions ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. A...est rejeté.<br/>
Article 2 : Les conclusions de la ville de Paris présentées sur le fondement de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à M. B...A...et à la ville de Paris.<br/>
Copie en sera adressée pour information au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-03-03-02 FONCTIONNAIRES ET AGENTS PUBLICS. ENTRÉE EN SERVICE. NOMINATIONS. NOMINATION POUR ORDRE. - NOMINATION D'UN SOUS-DIRECTEUR POUR LUI CONFIER DES FONCTIONS DE CHARGÉ DE MISSION [RJ1] - MOYEN D'ORDRE PUBLIC [RJ2].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-07-01-04-01-02 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. QUESTIONS GÉNÉRALES. MOYENS. MOYENS D'ORDRE PUBLIC À SOULEVER D'OFFICE. EXISTENCE. - NOMINATION POUR ORDRE [RJ2] - ESPÈCE.
</SCT>
<ANA ID="9A"> 36-03-03-02 L'arrêté maintenant un fonctionnaire en détachement dans un emploi fonctionnel de sous-directeur, qui n'a pas pour objet de pourvoir à un emploi vacant de sous-directeur et qui n'est pas accompagné de l'affectation dans les fonctions correspondantes, mais seulement de lui confier des fonctions de chargé de mission, constitue une nomination pour ordre, nulle et non avenue, à laquelle l'administration est tenue de mettre fin. Le juge relève d'office cette irrégularité.</ANA>
<ANA ID="9B"> 54-07-01-04-01-02 Le juge relève d'office l'inexistence d'une nomination pour ordre. En l'espèce, l'arrêté maintenant un fonctionnaire en détachement dans un emploi fonctionnel de sous-directeur, qui n'a pas pour objet de pourvoir à un emploi vacant de sous-directeur et qui n'est pas accompagné de l'affectation dans les fonctions correspondantes, mais qui a seulement pour objet de lui confier des fonctions de chargé de mission, constitue une nomination pour ordre, nulle et non avenue, à laquelle l'administration est tenue de mettre fin.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Cf. CE, Section, 27 avril 1956, Sieur Egaze, n°s 85887  91215, p. 172 ; CE, Assemblée, 15 mai 1981, Sieur Maurice, n° 33041, p. 221., ,[RJ2]Cf. CE, 5 mai 1971, Préfet de Paris c/ Syndicat chrétien de la préfecture de la Seine et autres, n° 75655, p. 329.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
