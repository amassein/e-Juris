<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026474486</ID>
<ANCIEN_ID>JG_L_2012_10_000000360838</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/47/44/CETATEXT000026474486.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 1ère sous-sections réunies, 08/10/2012, 360838</TITRE>
<DATE_DEC>2012-10-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>360838</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 1ère sous-sections réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELAPORTE, BRIARD, TRICHET</AVOCATS>
<RAPPORTEUR>M. Jean-Baptiste de Froment</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:360838.20121008</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la requête, enregistrée le 9 juillet 2012 au secrétariat du contentieux du Conseil d'Etat, présentée pour la Fondation des oeuvres sociales de l'air, dont le siège est 5 bis avenue de la Porte de Sèvres à Paris (75015) ;  <br/>
<br/>
              Vu l'arrêt n° 64406 du 6 juillet 2012, enregistré le 9 juillet 2012 au secrétariat du Conseil d'Etat, par lequel la Cour des comptes, avant de statuer sur les suites à donner à l'instance ouverte par le réquisitoire du Procureur général qui l'a saisie de présomptions de gestion de fait des deniers de l'Etat à raison d'opérations exécutées par la Fondation des oeuvres sociales de l'air, a décidé, par application des dispositions de l'article 23-2 de l'ordonnance<br/>
n° 58-1067 du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel, de transmettre au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution des premier et quatrième alinéas de l'article 60-XI de la loi de finances n° 63-156 du 23 février 1963 ; <br/>
<br/>
              Vu le mémoire, enregistré le 22 mai 2012 au greffe de la Cour des comptes, présenté pour la Fondation des oeuvres sociales de l'air représentée par son président en exercice, dont le siège est au 5 bis, avenue de la Porte de Sèvres Paris (75015), en application de l'article 23-1 de l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
Vu les autres pièces du dossier ;<br/>
<br/>
Vu la Constitution, notamment son Préambule et son article 61-1 ;<br/>
<br/>
Vu l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
      Vu le code des juridictions financières ;<br/>
<br/>
Vu l'article 60-XI de la loi de finances n° 63-156 du 23 février 1963 ;<br/>
<br/>
Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Baptiste de Froment, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Delaporte, Briard, Trichet avocat de la Fondation des oeuvres sociales de l'air.	<br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Delaporte, Briard, Trichet avocat de la Fondation des oeuvres sociales de l'air ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il résulte des dispositions de l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que, lorsqu'une juridiction relevant du Conseil d'État lui a transmis, en application de l'article 23-2 de cette même ordonnance, la question de la conformité aux droits et libertés garantis par la Constitution d'une disposition législative, le Conseil constitutionnel est saisi de cette question de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changements de circonstances, et que la question soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              2. Considérant, en premier lieu, que le premier alinéa du XI de l'article 60 de la loi de finances du 23 février 1963 définit la gestion de fait en recettes ; que le quatrième alinéa du même paragraphe XI prévoit la possibilité de condamner les comptables de fait, dans le cas où ils n'ont pas fait l'objet pour les mêmes opérations des poursuites au titre du délit prévu et réprimé par l'article 433-12 du code pénal, aux amendes prévues par la loi ; que la Fondation des oeuvres sociales de l'air soutient que ces dispositions, en ce qu'elles ne définissent pas avec suffisamment de précision la notion de recette publique, méconnaissent le principe de légalité des délits et des peines garanti par l'article 8 de la Déclaration des droits de l'homme et du citoyen du 26 août 1789 ; que, toutefois, la question prioritaire de constitutionnalité est soulevée au cours de la première phase de la procédure de gestion de fait ; qu'alors même que cette procédure est une procédure unique, cette première phase constitue une instance autonome qui se conclut par une décision du juge des comptes visant uniquement à reconnaître l'existence d'obligations constitutives de gestion de fait et à assujettir le comptable de fait aux obligations qui incombent aux comptables publics, notamment l'obligation de rendre un compte ; que ce n'est que dans une phase ultérieure de la procédure, lors du jugement du compte, que la Cour des comptes sera amenée à se prononcer sur l'opportunité de sanctionner le comptable de fait et, éventuellement, à mettre en oeuvre le 4ème alinéa du XI de l'article 60 de la loi du 23 février 1963 ; que, dans ces conditions, les dispositions contestées du 4ème alinéa du XI ne sont pas applicables au litige ;<br/>
<br/>
<br/>
              3. Considérant, en second lieu, que le premier alinéa du XI de l'article 60 de la loi du 23 février 1960 n'a pour objet de définir ni un délit ou une peine, ni une sanction ; que par suite, la question tirée de ce que ces dispositions méconnaîtraient le principe de légalité des délits et des peines garanti par l'article 8 de la Déclaration de 1789, qui n'est pas nouvelle, ne présente pas un caractère sérieux ; <br/>
<br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède qu'il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité transmise par la Cour des comptes. <br/>
Article 2 : La présente décision sera notifiée à la Fondation des oeuvres sociales de l'air et au Premier ministre. Copie en sera adressée au Conseil constitutionnel, à la Cour des comptes et au ministre de l'économie et des finances.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">18-01-04-01 COMPTABILITÉ PUBLIQUE ET BUDGET. RÉGIME JURIDIQUE DES ORDONNATEURS ET DES COMPTABLES. JUGEMENT DES COMPTES. COUR DES COMPTES. - PROCÉDURE DE GESTION DE FAIT - PREMIÈRE PHASE (DÉCLARATION DE GESTION DE FAIT) - INSTANCE AUTONOME - CONSÉQUENCE - QPC DIRIGÉE CONTRE LES DISPOSITIONS DU 4ÈME ALINÉA DU XI DE L'ARTICLE 60 DE LA LOI DU 23 FÉVRIER 1963, SOULEVÉE AU COURS DE LA PREMIÈRE PHASE - APPLICABILITÉ AU LITIGE - CONDITION NON REMPLIE, DÈS LORS QUE CES DISPOSITIONS SONT SUSCEPTIBLES D'ÊTRE MISES EN OEUVRE SEULEMENT DANS UNE PHASE ULTÉRIEURE DE LA PROCÉDURE DE GESTION DE FAIT [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-10-05-01-03 PROCÉDURE. - QPC SOULEVÉE AU COURS DE LA PREMIÈRE PHASE DE LA PROCÉDURE DE GESTION DE FAIT CONTRE LES DISPOSITIONS DU 4ÈME ALINÉA DU XI DE L'ARTICLE 60 DE LA LOI DU 23 FÉVRIER 1963 PRÉVOYANT LA POSSIBILITÉ DE CONDAMNER LES COMPTABLES DE FAIT AUX AMENDES PRÉVUES PAR LA LOI - DISPOSITIONS SUSCEPTIBLES D'ÊTRE MISES EN OEUVRE SEULEMENT DANS UNE PHASE ULTÉRIEURE DE LA PROCÉDURE DE GESTION DE FAIT - CONSÉQUENCE - DISPOSITIONS NON APPLICABLES AU LITIGE [RJ1].
</SCT>
<ANA ID="9A"> 18-01-04-01 Alors même que la procédure de gestion de fait est une procédure unique, la première phase de cette procédure constitue une instance autonome qui se conclut par une décision du juge des comptes visant uniquement à reconnaître l'existence d'obligations constitutives de gestion de fait et à assujettir le comptable de fait aux obligations qui incombent aux comptables publics, notamment l'obligation de rendre un compte. Ce n'est que dans une phase ultérieure de la procédure, lors du jugement du compte, que la Cour des comptes sera amenée à se prononcer sur l'opportunité de sanctionner le comptable de fait et, éventuellement, à mettre en oeuvre le 4ème alinéa du XI de l'article 60 de la loi n° 63-156 du 23 février 1963 de finances pour 1963, qui prévoit la possibilité de condamner les comptables de fait, dans le cas où ils n'ont pas fait l'objet pour les mêmes opérations des poursuites au titre du délit prévu et réprimé par l'article 433-12 du code pénal, aux amendes prévues par la loi. Dans ces conditions, la question prioritaire de constitutionnalité (QPC) étant soulevée au cours de la première phase de la procédure de gestion de fait, les dispositions du 4ème alinéa du XI ne sont pas applicables au litige.</ANA>
<ANA ID="9B"> 54-10-05-01-03 Alors même que la procédure de gestion de fait est une procédure unique, la première phase de cette procédure constitue une instance autonome qui se conclut par une décision du juge des comptes visant uniquement à reconnaître l'existence d'obligations constitutives de gestion de fait et à assujettir le comptable de fait aux obligations qui incombent aux comptables publics, notamment l'obligation de rendre un compte. Ce n'est que dans une phase ultérieure de la procédure, lors du jugement du compte, que la Cour des comptes sera amenée à se prononcer sur l'opportunité de sanctionner le comptable de fait et, éventuellement, à mettre en oeuvre le 4ème alinéa du XI de l'article 60 de la loi n° 63-156 du 23 février 1963 de finances pour 1963, qui prévoit la possibilité de condamner les comptables de fait, dans le cas où ils n'ont pas fait l'objet pour les mêmes opérations des poursuites au titre du délit prévu et réprimé par l'article 433-12 du code pénal, aux amendes prévues par la loi. Dans ces conditions, la question prioritaire de constitutionnalité (QPC) étant soulevée au cours de la première phase de la procédure de gestion de fait, les dispositions du 4ème alinéa du XI ne sont pas applicables au litige.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., sur la notion d'instance en cours, dans l'hypothèse d'une question posée au moment de la mise en oeuvre d'un acte d'instruction, CE, 4 mars 2011, Clark, n° 344766, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
