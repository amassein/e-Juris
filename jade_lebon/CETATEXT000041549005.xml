<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041549005</ID>
<ANCIEN_ID>JG_L_2020_02_000000425451</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/54/90/CETATEXT000041549005.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 5ème chambres réunies, 05/02/2020, 425451</TITRE>
<DATE_DEC>2020-02-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>425451</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MARLANGE, DE LA BURGADE ; SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR>Mme Carine Chevrier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Louis Dutheillet de Lamothe</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2020:425451.20200205</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              L'association " Des évêques aux cordeliers ", la société civile d'exploitation agricole et forestière M... de Becourt, M. A... I..., M. C... F..., M. L... G..., Mme E... G..., M. D... H..., M. C... B..., Mme J... B... et M. K... M... ont demandé au tribunal administratif de Besançon d'annuler l'arrêté du 16 octobre 2014 par lequel le préfet de la Haute-Saône a délivré à la société Eole-Res une autorisation d'exploiter dix éoliennes sur les territoires des communes d'Andelarre, Baignes, Mont-le-Vernois et Rosey. Par un jugement no 1500635 du 23 mai 2017, le tribunal administratif a rejeté leur demande.<br/>
<br/>
              Par un arrêt n° 17NC01857 du 4 octobre 2018, la cour administrative d'appel de Nancy a rejeté l'appel formé par l'association " Des évêques aux cordeliers " et autres contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés le 19 novembre 2018, le 11 février et le 23 décembre 2019 au secrétariat du contentieux du Conseil d'Etat, l'association " Des évêques aux cordeliers ", la société civile d'exploitation agricole et forestière M... de Becourt, M. F..., M. et Mme G..., M. H..., M. et Mme B... et M. M... demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat et de la société Res la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la directive 2011/92/UE du Parlement européen et du Conseil du 13 décembre 2011 ;<br/>
              - le code de l'environnement ;<br/>
              - l'arrêté du 26 août 2011 relatif à la remise en état et à la constitution des garanties financières pour les installations de production d'électricité utilisant l'énergie mécanique du vent ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Carine Chevrier, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Louis Dutheillet de Lamothe, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Marlange, de la Burgade, avocat de l'association " Des évêques aux cordeliers " et autres et à la SCP Matuchansky, Poupot, Valdelièvre, avocat de la société Res ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 10 janvier 2020, présentée par la ministre de la transition écologique et solidaire ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 17 janvier 2020, présentée par la société Res ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que le préfet de la Haute-Saône, par un arrêté du 16 octobre 2014, a autorisé la société Eole Res, devenue Res, à exploiter un parc de dix éoliennes sur les territoires des communes d'Andelarre, Baignes, Mont-le-Vernois et Rosey. Le tribunal administratif de Besançon, par un jugement du 23 mai 2017, a rejeté la demande de l'association " Des évêques aux cordeliers " et autres tendant à l'annulation de cette autorisation d'exploiter. Par un arrêt du 4 octobre 2018, la cour administrative d'appel de Nancy a rejeté leur appel contre ce jugement. <br/>
<br/>
              2. Aux termes du paragraphe 1 de l'article 6 de la directive du 13 décembre 2011 concernant l'évaluation des incidences de certains projets publics et privés sur l'environnement : " Les États membres prennent les mesures nécessaires pour que les autorités susceptibles d'être concernées par le projet, en raison de leurs responsabilités spécifiques en matière d'environnement, aient la possibilité de donner leur avis sur les informations fournies par le maître d'ouvrage et sur la demande d'autorisation. À cet effet, les États membres désignent les autorités à consulter, d'une manière générale ou au cas par cas. (...) ". L'article L. 122-1 du code de l'environnement, pris pour la transposition des articles 2 et 6 de cette directive, dispose, dans sa rédaction applicable en l'espèce, que : " I. - Les projets de travaux, d'ouvrages ou d'aménagements publics et privés qui, par leur nature, leurs dimensions ou leur localisation sont susceptibles d'avoir des incidences notables sur l'environnement ou la santé humaine sont précédés d'une étude d'impact. (...) / III. - Dans le cas d'un projet relevant des catégories d'opérations soumises à étude d'impact, le dossier présentant le projet, comprenant l'étude d'impact et la demande d'autorisation, est transmis pour avis à l'autorité administrative de l'Etat compétente en matière d'environnement. (...). / IV.- La décision de l'autorité compétente qui autorise le pétitionnaire ou le maître d'ouvrage à réaliser le projet prend en considération l'étude d'impact, l'avis de l'autorité administrative de l'Etat compétente en matière d'environnement et le résultat de la consultation du public (...) ". En vertu du III de l'article R. 122-6 du même code, dans sa version issue du décret du 29 décembre 2011 portant réforme des études d'impact des projets de travaux, d'ouvrages ou d'aménagement, applicable au litige, l'autorité administrative de l'Etat compétente en matière d'environnement mentionnée à l'article L. 122-1, lorsqu'elle n'est ni le ministre chargé de l'environnement, dans les cas prévus au I de cet article, ni la formation compétente du Conseil général de l'environnement et du développement durable, dans les cas prévus au II de ce même article, est le préfet de la région sur le territoire de laquelle le projet de travaux, d'ouvrage ou d'aménagement doit être réalisé.<br/>
<br/>
              3. L'article 6 de la directive du 13 décembre 2011 a pour objet de garantir qu'une autorité compétente et objective en matière d'environnement soit en mesure de rendre un avis sur l'évaluation environnementale des projets susceptibles d'avoir des incidences notables sur l'environnement, avant leur approbation ou leur autorisation, afin de permettre la prise en compte de ces incidences. Eu égard à l'interprétation de l'article 6 de la directive du 27 juin 2001 donnée par la Cour de justice de l'Union européenne par son arrêt rendu le 20 octobre 2011 dans l'affaire C-474/10, il résulte clairement des dispositions de l'article 6 de la directive du 13 décembre 2011 que, si elles ne font pas obstacle à ce que l'autorité publique compétente pour autoriser un projet soit en même temps chargée de la consultation en matière environnementale, elles imposent cependant que, dans une telle situation, une séparation fonctionnelle soit organisée au sein de cette autorité, de manière à ce que l'entité administrative concernée dispose d'une autonomie réelle, impliquant notamment qu'elle soit pourvue de moyens administratifs et humains qui lui soient propres, et soit ainsi en mesure de remplir la mission de consultation qui lui est confiée en donnant un avis objectif sur le projet concerné.<br/>
<br/>
              4. Lorsque le préfet de région est l'autorité compétente pour autoriser le projet, en particulier lorsqu'il agit en sa qualité de préfet du département où se trouve le chef-lieu de la région, ou dans les cas où il est en charge de l'élaboration ou de la conduite du projet au niveau local, si la mission régionale d'autorité environnementale du Conseil général de l'environnement et du développement durable, définie par le décret du 2 octobre 2015 relatif au Conseil général de l'environnement et du développement durable et les articles R. 122-21 et R. 122-25 du code de l'environnement, peut être regardée comme disposant, à son égard, d'une autonomie réelle lui permettant de rendre un avis environnemental dans des conditions répondant aux exigences résultant de la directive, il n'en va pas de même des services placés sous son autorité hiérarchique, comme en particulier la direction régionale de l'environnement, de l'aménagement et du logement (DREAL). <br/>
<br/>
              5. Lorsque le projet est autorisé par un préfet de département autre que le préfet de région, l'avis rendu sur le projet par le préfet de région en tant qu'autorité environnementale doit, en principe, être regardé comme ayant été émis par une autorité disposant d'une autonomie réelle répondant aux exigences de l'article 6 de la directive du 13 décembre 2011, sauf dans le cas où c'est le même service qui a, à la fois, instruit la demande d'autorisation et préparé l'avis de l'autorité environnementale. En particulier, les exigences de la directive, tenant à ce que l'entité administrative appelée à rendre l'avis environnemental sur le projet dispose d'une autonomie réelle, impliquant notamment qu'elle soit pourvue de moyens administratifs et humains qui lui soient propres, ne peuvent être regardées comme satisfaites lorsque le projet a été instruit pour le compte du préfet de département par la direction régionale de l'environnement, de l'aménagement et du logement (DREAL) et que l'avis environnemental émis par le préfet de région a été préparé par la même direction, à moins que l'avis n'ait été préparé, au sein de cette direction, par le service mentionné à l'article R. 122-21 du code de l'environnement qui a spécialement pour rôle de préparer les avis des autorités environnementales.<br/>
<br/>
              6. Par suite, en jugeant que, par principe, il avait été répondu aux exigences de la directive dès lors que l'avis de l'autorité environnementale avait été émis par le préfet de région et que la décision attaquée avait été prise par le préfet de département, alors qu'il ressortait des pièces du dossier qui lui était soumis que la même unité territoriale de la direction régionale de l'environnement, de l'aménagement et du logement de Franche-Comté avait à la fois instruit la demande d'autorisation et préparé l'avis de l'autorité environnementale, la cour administrative d'appel a entaché son arrêt d'une erreur de droit. Dès lors, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, l'association " Des évêques aux cordeliers " et autres sont fondés à demander l'annulation de l'arrêt qu'ils attaquent.<br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser aux requérants au titre des dispositions de l'article L. 761-1 du code de justice administrative. Les dispositions de cet article font, en revanche, obstacle à ce qu'une somme soit mise à ce titre à la charge de l'association " Des évêques aux cordeliers " et autres, qui ne sont pas, dans la présente instance, les parties perdantes.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nancy du 4 octobre 2018 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nancy.<br/>
Article 3 : L'Etat versera à l'association " Des évêques aux cordeliers " et autres la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. Les conclusions présentées au même titre par la société Res sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à l'association " Des évêques aux cordeliers ", première dénommée pour l'ensemble des requérants, à la ministre de la transition écologique et solidaire et à la société Res.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">15-03-03-01 COMMUNAUTÉS EUROPÉENNES ET UNION EUROPÉENNE. APPLICATION DU DROIT DE L'UNION EUROPÉENNE PAR LE JUGE ADMINISTRATIF FRANÇAIS. PRISE EN COMPTE DES ARRÊTS DE LA COUR DE JUSTICE. INTERPRÉTATION DU DROIT DE L'UNION. - ARTICLE 6 DE LA DIRECTIVE 2011/92/UE DU 13 DÉCEMBRE 2011 - EXIGENCE D'AUTONOMIE DE L'AUTORITÉ APPELÉE À RENDRE UN AVIS SUR L'ÉVALUATION ENVIRONNEMENTALE D'UN PROJET [RJ1] - RESPECT DE CETTE EXIGENCE LORSQUE LE PRÉFET DE RÉGION EST L'AUTORITÉ COMPÉTENTE EN MATIÈRE D'ENVIRONNEMENT - 1) CAS DANS LEQUEL LE PROJET EST AUTORISÉ PAR LE PRÉFET DE RÉGION [RJ2] - 2) CAS DANS LEQUEL LE PROJET EST AUTORISÉ PAR UN PRÉFET DE DÉPARTEMENT AUTRE QUE LE PRÉFET DE RÉGION - A) EXISTENCE, SAUF SI LE MÊME SERVICE A INSTRUIT LA DEMANDE D'AUTORISATION ET PRÉPARÉ L'AVIS DE L'AUTORITÉ ENVIRONNEMENTALE - B) ILLUSTRATION - AVIS PRÉPARÉ PAR LA DREAL AYANT INSTRUIT LE PROJET - NON-CONFORMITÉ AVEC LA DIRECTIVE, SAUF SI L'AVIS EST PRÉPARÉ PAR LE SERVICE D'APPUI À LA MISSION RÉGIONALE D'AUTORITÉ ENVIRONNEMENTALE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">44-006-03-01 NATURE ET ENVIRONNEMENT. - ARTICLE 6 DE LA DIRECTIVE 2011/92/UE DU 13 DÉCEMBRE 2011 - EXIGENCE D'AUTONOMIE DE L'AUTORITÉ APPELÉE À RENDRE UN AVIS SUR L'ÉVALUATION ENVIRONNEMENTALE D'UN PROJET [RJ1] - RESPECT DE CETTE EXIGENCE LORSQUE LE PRÉFET DE RÉGION EST L'AUTORITÉ COMPÉTENTE EN MATIÈRE D'ENVIRONNEMENT - 1) CAS DANS LEQUEL LE PROJET EST AUTORISÉ PAR LE PRÉFET DE RÉGION [RJ2] - 2) CAS DANS LEQUEL LE PROJET EST AUTORISÉ PAR UN PRÉFET DE DÉPARTEMENT AUTRE QUE LE PRÉFET DE RÉGION - A) EXISTENCE, SAUF SI LE MÊME SERVICE A INSTRUIT LA DEMANDE D'AUTORISATION ET PRÉPARÉ L'AVIS DE L'AUTORITÉ ENVIRONNEMENTALE - B) ILLUSTRATION - AVIS PRÉPARÉ PAR LA DREAL AYANT INSTRUIT LE PROJET - NON-CONFORMITÉ AVEC LA DIRECTIVE, SAUF SI L'AVIS EST PRÉPARÉ PAR LE SERVICE D'APPUI À LA MISSION RÉGIONALE D'AUTORITÉ ENVIRONNEMENTALE.
</SCT>
<ANA ID="9A"> 15-03-03-01 Article 6 de la directive 2011/92/UE du 13 décembre 2011 imposant que, dans le cas où l'autorité publique compétente pour autoriser un projet est en même temps chargée de la consultation en matière environnementale, une séparation fonctionnelle soit organisée au sein de cette autorité, de manière à ce que l'entité administrative concernée dispose d'une autonomie réelle, impliquant notamment qu'elle soit pourvue de moyens administratifs et humains qui lui soient propres, et soit ainsi en mesure de remplir la mission de consultation qui lui est confiée en donnant un avis objectif sur le projet concerné.... ,,Article R. 122-6 du code de l'environnement, dans sa version issue du décret n° 2011-2019 du 29 décembre 2011, prévoyant qu'à l'exception des cas qu'il énumère, le préfet de la région sur le territoire de laquelle le projet doit être réalisé est l'autorité compétente en matière d'environnement.,,,1) Lorsque le préfet de région est l'autorité compétente pour autoriser le projet, en particulier lorsqu'il agit en sa qualité de préfet du département où se trouve le chef-lieu de la région, ou dans les cas où il est en charge de l'élaboration ou de la conduite du projet au niveau local, si la mission régionale d'autorité environnementale (MRAE) du Conseil général de l'environnement et du développement durable (CGEDD), définie par le décret n° 2015-1229 du 2 octobre 2015 et les articles R. 122-21 et R. 122-25 du code de l'environnement, peut être regardée comme disposant, à son égard, d'une autonomie réelle lui permettant de rendre un avis environnemental dans des conditions répondant aux exigences résultant de la directive, il n'en va pas de même des services placés sous son autorité hiérarchique, comme en particulier la direction régionale de l'environnement, de l'aménagement et du logement (DREAL).... ,,2) a) Lorsque le projet est autorisé par un préfet de département autre que le préfet de région, l'avis rendu sur le projet par le préfet de région en tant qu'autorité environnementale doit, en principe, être regardé comme ayant été émis par une autorité disposant d'une autonomie réelle répondant aux exigences de l'article 6 de la directive, sauf dans le cas où c'est le même service qui a, à la fois, instruit la demande d'autorisation et préparé l'avis de l'autorité environnementale.... ,,b) En particulier, les exigences de la directive, tenant à ce que l'entité administrative appelée à rendre l'avis environnemental sur le projet dispose d'une autonomie réelle, impliquant notamment qu'elle soit pourvue de moyens administratifs et humains qui lui soient propres, ne peuvent être regardées comme satisfaites lorsque le projet a été instruit pour le compte du préfet de département par la DREAL et que l'avis environnemental émis par le préfet de région a été préparé par la même direction, à moins que l'avis n'ait été préparé, au sein de cette direction, par le service mentionné à l'article R. 122-21 du code de l'environnement qui a spécialement pour rôle de préparer les avis des autorités environnementales.</ANA>
<ANA ID="9B"> 44-006-03-01 Article 6 de la directive 2011/92/UE du 13 décembre 2011 imposant que, dans le cas où l'autorité publique compétente pour autoriser un projet est en même temps chargée de la consultation en matière environnementale, une séparation fonctionnelle soit organisée au sein de cette autorité, de manière à ce que l'entité administrative concernée dispose d'une autonomie réelle, impliquant notamment qu'elle soit pourvue de moyens administratifs et humains qui lui soient propres, et soit ainsi en mesure de remplir la mission de consultation qui lui est confiée en donnant un avis objectif sur le projet concerné.... ,,Article R. 122-6 du code de l'environnement, dans sa version issue du décret n° 2011-2019 du 29 décembre 2011, prévoyant qu'à l'exception des cas qu'il énumère, le préfet de la région sur le territoire de laquelle le projet doit être réalisé est l'autorité compétente en matière d'environnement.,,,1) Lorsque le préfet de région est l'autorité compétente pour autoriser le projet, en particulier lorsqu'il agit en sa qualité de préfet du département où se trouve le chef-lieu de la région, ou dans les cas où il est en charge de l'élaboration ou de la conduite du projet au niveau local, si la mission régionale d'autorité environnementale (MRAE) du Conseil général de l'environnement et du développement durable (CGEDD), définie par le décret n° 2015-1229 du 2 octobre 2015 et les articles R. 122-21 et R. 122-25 du code de l'environnement, peut être regardée comme disposant, à son égard, d'une autonomie réelle lui permettant de rendre un avis environnemental dans des conditions répondant aux exigences résultant de la directive, il n'en va pas de même des services placés sous son autorité hiérarchique, comme en particulier la direction régionale de l'environnement, de l'aménagement et du logement (DREAL).... ,,2) a) Lorsque le projet est autorisé par un préfet de département autre que le préfet de région, l'avis rendu sur le projet par le préfet de région en tant qu'autorité environnementale doit, en principe, être regardé comme ayant été émis par une autorité disposant d'une autonomie réelle répondant aux exigences de l'article 6 de la directive, sauf dans le cas où c'est le même service qui a, à la fois, instruit la demande d'autorisation et préparé l'avis de l'autorité environnementale.... ,,b) En particulier, les exigences de la directive, tenant à ce que l'entité administrative appelée à rendre l'avis environnemental sur le projet dispose d'une autonomie réelle, impliquant notamment qu'elle soit pourvue de moyens administratifs et humains qui lui soient propres, ne peuvent être regardées comme satisfaites lorsque le projet a été instruit pour le compte du préfet de département par la DREAL et que l'avis environnemental émis par le préfet de région a été préparé par la même direction, à moins que l'avis n'ait été préparé, au sein de cette direction, par le service mentionné à l'article R. 122-21 du code de l'environnement qui a spécialement pour rôle de préparer les avis des autorités environnementales.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 6 décembre 2017, Association France Nature Environnement, n° 400559, T. pp. 499-691. Rappr., s'agissant de l'évaluation des plans et programmes prévue par la directive 2001/42/CE du 27 juin 2001, CJUE, 20 octobre 2011, Department of the Environment for Northern Ireland contre Seaport (NI) Ltd et autres, aff. C-474/10, Rec. p. I-10227.,,[RJ2] Cf., sur les conditions dans lesquelles l'exigence d'autonomie est dans ce cas respectée, CE, 20 septembre 2019, Ministre de la transition écologique et solidaire c/ Association  Sauvons le paradis  et autres, n° 428274, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
