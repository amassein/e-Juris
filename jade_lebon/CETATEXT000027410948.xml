<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027410948</ID>
<ANCIEN_ID>JG_L_2013_05_000000357112</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/41/09/CETATEXT000027410948.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 15/05/2013, 357112</TITRE>
<DATE_DEC>2013-05-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>357112</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MONOD, COLIN</AVOCATS>
<RAPPORTEUR>Mme Julia Beurton</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:357112.20130515</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 24 février 2012 au secrétariat du contentieux du Conseil d'Etat, présentée pour la société anonyme (SA) Laboratoire Sciencex, dont le siège est 1, rue Edmond Guillout à Paris (75015), représentée par son représentant légal ; la société requérante demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du Comité économique des produits de santé notifiée par courrier de son président en date du 27 décembre 2011, rejetant sa demande tendant à la modification du prix de la spécialité Nalgésic 300 mg ; <br/>
<br/>
              2°) d'enjoindre au Comité économique des produits de santé de réexaminer sa demande tendant à l'augmentation du prix de cette spécialité, dans le délai d'un mois à compter de la décision à venir ; <br/>
<br/>
              3°) de mettre à la charge du Comité économique des produits de santé la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu la note en délibéré, enregistrée le 8 avril 2013, présentée pour la S.A Laboratoire Sciencex ;<br/>
<br/>
              Vu le code de la sécurité sociale ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Julia Beurton, Auditeur,  <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Monod, Colin, avocat de la S. A. Laboratoire Sciencex ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 162-16-4 du code de la sécurité sociale : " Le prix de vente au public de chacun des médicaments mentionnés au premier alinéa de l'article L. 162-17 est fixé par convention entre l'entreprise exploitant le médicament et le Comité économique des produits de santé conformément à l'article L. 162-17-4 (...). La fixation de ce prix tient compte principalement de l'amélioration du service médical rendu apportée par le médicament, le cas échéant des résultats de l'évaluation médico-économique, des prix des médicaments à même visée thérapeutique, des volumes de vente prévus ou constatés ainsi que des conditions prévisibles et réelles d'utilisation du médicament. (...) " ;<br/>
<br/>
              2. Considérant que la société anonyme (S.A.) Laboratoire Sciencex a sollicité auprès du Comité économique des produits de santé (CEPS) la hausse du prix de la spécialité qu'elle exploite, dénommée Nalgésic 300 mg, laquelle est indiquée pour le traitement symptomatique des douleurs d'intensité légère à modérée, aux motifs, d'une part, de la faiblesse du prix de cette spécialité par rapport à la spécialité Ponstyl, d'autre part, de l'absence de marge dégagée pour la production de ce médicament ; que le CEPS, par décision du 27 décembre 2011, dont la société requérante demande l'annulation, a refusé de faire droit à sa demande au motif qu'il " existe d'autres alternatives qui ne sont pas plus coûteuses " pour la même indication thérapeutique ;<br/>
<br/>
              3. Considérant, en premier lieu, que, saisi d'une demande de hausse du prix d'une spécialité, il appartient au CEPS d'apprécier s'il y a lieu de procéder à la modification sollicitée au regard notamment des critères indiqués à l'article L. 162-16-4 du code de la sécurité sociale ; que le CEPS, après avoir examiné l'ensemble de ces critères, qui ne sont d'ailleurs pas exhaustifs, peut légalement refuser la demande qui lui est soumise en faisant usage d'un seul critère, justifiant à titre principal sa décision ; que, par suite, le moyen tiré de ce que le CEPS aurait commis une erreur de droit en se fondant sur l'existence d'alternatives thérapeutiques qui ne sont pas plus coûteuses pour refuser la demande de la société requérante ne peut qu'être écarté ;<br/>
<br/>
              4. Considérant, en deuxième lieu, que si la société requérante soutient que le CEPS a commis une erreur de droit en n'examinant pas si sa demande était justifiée au regard des coûts de fabrication de la spécialité Nalgésic 300 mg, elle ne peut utilement, en présence d'alternatives thérapeutiques, invoquer l'absence de marge commerciale due à une hausse de ses coûts généraux de production et fonder sa demande sur un tel motif ; <br/>
<br/>
              5. Considérant, en troisième lieu, que, pour justifier son refus d'augmenter le prix de la spécialité Nalgésic 300 mg, le CEPS a estimé, d'une part, que cette spécialité, à base de fénoprofène, répondait à un besoin thérapeutique couvert par d'autres spécialités, notamment celles à base d'ibuprofène, molécule qui appartient à la même famille chimique que le fénoprofène, d'autre part, que les spécialités à base d'ibuprofène sont vendues à un prix public toutes taxes comprises proche de celui de Nalgésic 300 mg et à un prix rapporté au dosage de molécule utilisé moins élevé, enfin que Nalgésic 300 mg ne présentait pas d'amélioration du service médical rendu par rapport à la spécialité Ibuprofène, qui lui est comparable ; qu'il ne ressort pas des pièces du dossier que le CEPS ait, ce faisant, commis une erreur manifeste d'appréciation ; <br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède que la S.A. Laboratoire Sciencex n'est pas fondée à demander l'annulation de la décision attaquée ; que ses conclusions présentées à fin d'injonction doivent, par voie de conséquence, être rejetées, ainsi que ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la S.A. Laboratoire Sciencex est rejetée.<br/>
Article 2 : La présente décision sera notifiée à la société anonyme Laboratoire Sciencex, au Comité économique des produits de santé et à la ministre des affaires sociales et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">61-04-01-022 SANTÉ PUBLIQUE. PHARMACIE. PRODUITS PHARMACEUTIQUES. - SPÉCIALITÉ POUR LAQUELLE IL EXISTE DES ALTERNATIVES THÉRAPEUTIQUES - PRISE EN COMPTE, DANS L'APPRÉCIATION DU PRIX, DES COÛTS SPÉCIFIQUES DE PRODUCTION - ABSENCE [RJ1].
</SCT>
<ANA ID="9A"> 61-04-01-022 Pour apprécier le prix de vente au public d'une spécialité pour laquelle il existe des alternatives thérapeutiques, le comité économique des produits de santé (CEPS) n'a pas à prendre en considération les coûts spécifiques de production invoqués par l'entreprise exploitant le médicament.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Comp., dans le cas d'un médicament dit  orphelin , CE, 20 mars 2013, Société Addmedica, n°s 356661 et autres, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
