<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030509794</ID>
<ANCIEN_ID>JG_L_2015_04_000000372195</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/50/97/CETATEXT000030509794.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème / 1ère SSR, 17/04/2015, 372195, Publié au recueil Lebon</TITRE>
<DATE_DEC>2015-04-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>372195</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème / 1ère SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP VINCENT, OHL</AVOCATS>
<RAPPORTEUR>Mme Clémence Olsina</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:372195.20150417</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 16 septembre 2013 au secrétariat du contentieux du Conseil d'Etat, présenté pour Mme A...B..., demeurant ...; Mme B...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'ordonnance n° 12VE03918 du 8 avril 2013 par laquelle le premier vice-président de la cour administrative d'appel de Versailles a rejeté sa requête tendant, d'une part, à l'annulation du jugement n° 1106215 du 19 janvier 2012 par lequel le tribunal administratif de Versailles a rejeté sa demande tendant à l'annulation pour excès de pouvoir de l'arrêté du 3 octobre 2011 par lequel le préfet des Yvelines a refusé de lui délivrer un certificat de résidence, l'a obligée à quitter le territoire français et a prononcé une interdiction de retour sur le territoire français d'une durée d'un an, d'autre part, à l'annulation pour excès de pouvoir de cet arrêté et à ce qu'il soit enjoint au préfet des Yvelines, à titre principal, de lui délivrer un certificat de résidence sur le fondement de l'article 6-5 de l'accord franco-algérien du 27 décembre 1968 dans le délai d'un mois, sous astreinte de 150 euros par jour de retard, et à titre subsidiaire, de réexaminer sa situation dans le délai d'un mois, sous astreinte de 150 euros par jour de retard ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'entrée et du séjour des étrangers et du droit d'asile ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Clémence Olsina, auditeur,  <br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Vincent, Ohl, avocat de Mme B...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes des dispositions du III de l'article L. 511-1 du code de l'entrée et du séjour des étrangers et du droit d'asile, dans leur version en vigueur à la date de la décision litigieuse : " L'autorité administrative peut, par une décision motivée, assortir l'obligation de quitter le territoire français d'une interdiction de retour sur le territoire français. / (...) Lorsque l'étranger ne faisant pas l'objet d'une interdiction de retour s'est maintenu sur le territoire au-delà du délai de départ volontaire, l'autorité administrative peut prononcer une interdiction de retour pour une durée maximale de deux ans à compter de sa notification. / Lorsqu'aucun délai de départ volontaire n'a été accordé à l'étranger obligé de quitter le territoire français, l'autorité administrative peut prononcer l'interdiction de retour pour une durée maximale de trois ans à compter de sa notification. / Lorsqu'un délai de départ volontaire a été accordé à l'étranger obligé de quitter le territoire français, l'autorité administrative peut prononcer l'interdiction de retour, prenant effet à l'expiration du délai, pour une durée maximale de deux ans à compter de sa notification. / Lorsque l'étranger faisant l'objet d'une interdiction de retour s'est maintenu sur le territoire au-delà du délai de départ volontaire ou alors qu'il était obligé de quitter sans délai le territoire français ou, ayant déféré à l'obligation de quitter le territoire français, y est revenu alors que l'interdiction de retour poursuit ses effets, l'autorité administrative peut prolonger cette mesure pour une durée maximale de deux ans. / L'interdiction de retour et sa durée sont décidées par l'autorité administrative en tenant compte de la durée de présence de l'étranger sur le territoire français, de la nature et de l'ancienneté de ses liens avec la France, de la circonstance qu'il a déjà fait l'objet ou non d'une mesure d'éloignement et de la menace pour l'ordre public que représente sa présence sur le territoire français (...) " ; <br/>
<br/>
              2. Considérant qu'il ressort des termes mêmes de ces dispositions que l'autorité compétente doit, pour décider de prononcer à l'encontre de l'étranger soumis à l'obligation de quitter le territoire français une interdiction de retour et en fixer la durée, tenir compte, dans le respect des principes constitutionnels, des principes généraux du droit et des règles résultant des engagements internationaux de la France, des quatre critères qu'elles énumèrent, sans pouvoir se limiter à ne prendre en compte que l'un ou plusieurs d'entre eux ; que la décision d'interdiction de retour doit comporter l'énoncé des considérations de droit et de fait qui en constituent le fondement, de sorte que son destinataire puisse à sa seule lecture en connaître les motifs ; que si cette motivation doit attester de la prise en compte par l'autorité compétente, au vu de la situation de l'intéressé, de l'ensemble des critères prévus par la loi, aucune règle n'impose que le principe et la durée de l'interdiction de retour fassent l'objet de motivations distinctes, ni que soit indiquée l'importance accordée à chaque critère ; <br/>
<br/>
              3. Considérant qu'il incombe ainsi à l'autorité compétente qui prend une décision d'interdiction de retour d'indiquer dans quel cas susceptible de justifier une telle mesure se trouve l'étranger ; qu'elle doit par ailleurs faire état des éléments de la situation de l'intéressé au vu desquels elle a arrêté, dans son principe et dans sa durée, sa décision, eu égard notamment à la durée de la présence de l'étranger sur le territoire français, à la nature et à l'ancienneté de ses liens avec la France et, le cas échéant, aux précédentes mesures d'éloignement dont il a fait l'objet ; qu'elle doit aussi, si elle estime que figure au nombre des motifs qui justifie sa décision une menace pour l'ordre public, indiquer les raisons pour lesquelles la présence de l'intéressé sur le territoire français doit, selon elle, être regardée comme une telle menace ; qu'en revanche, si, après prise en compte de ce critère, elle ne retient pas cette circonstance au nombre des motifs de sa décision, elle n'est pas tenue, à peine d'irrégularité, de le préciser expressément ; <br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède qu'en jugeant que l'arrêté du préfet des Yvelines du 3 octobre 2011 prononçant à l'encontre de Mme B...une interdiction de retour sur le territoire français d'une durée d'un an n'était pas, au seul motif  qu'il ne précisait pas que l'intéressée ne présentait aucune menace pour l'ordre public, entaché d'insuffisance de motivation, le vice-président de la cour administrative d'appel de Versailles n'a pas commis d'erreur de droit ; que MmeB..., qui soulève cet unique moyen, n'est, par suite, pas fondée à demander l'annulation de l'ordonnance attaquée  ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
      Article 1er : Le pourvoi de Mme B...est rejeté. <br/>
<br/>
Article 2 : La présente décision sera notifiée à Mme A...B...et au ministre de l'intérieur.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-03-01-02-02-02 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - FORME ET PROCÉDURE. QUESTIONS GÉNÉRALES. MOTIVATION. MOTIVATION SUFFISANTE. EXISTENCE. - INTERDICTION DE RETOUR EN FRANCE (ART. L. 511-1 DU CESEDA) - 1) MOTIFS DE L'INTERDICTION DE RETOUR - 2) MOTIVATION DE LA DÉCISION AU REGARD DES QUATRE CRITÈRES POSÉS PAR LA LOI - PRINCIPES GÉNÉRAUX [RJ1] - OBLIGATION D'ASSORTIR SYSTÉMATIQUEMENT LA DÉCISION D'UNE MOTIVATION SPÉCIFIQUE SUR LE CRITÈRE TIRÉ DE LA MENACE POUR L'ORDRE PUBLIC - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">335-03-01-02 ÉTRANGERS. OBLIGATION DE QUITTER LE TERRITOIRE FRANÇAIS (OQTF) ET RECONDUITE À LA FRONTIÈRE. LÉGALITÉ EXTERNE. MOTIVATION. - OQTF ASSORTIE D'UNE INTERDICTION DE RETOUR - 1) MOTIFS DE L'INTERDICTION DE RETOUR - 2) MOTIVATION DE LA DÉCISION AU REGARD DES QUATRE CRITÈRES POSÉS PAR LA LOI - PRINCIPES GÉNÉRAUX [RJ1] - OBLIGATION D'ASSORTIR SYSTÉMATIQUEMENT LA DÉCISION D'UNE MOTIVATION SPÉCIFIQUE SUR LE CRITÈRE TIRÉ DE LA MENACE POUR L'ORDRE PUBLIC - ABSENCE.
</SCT>
<ANA ID="9A"> 01-03-01-02-02-02 1) Il ressort des termes mêmes des dispositions du III de l'article L. 511-1 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA) que l'autorité compétente doit, pour décider de prononcer à l'encontre de l'étranger soumis à l'obligation de quitter le territoire français une interdiction de retour et en fixer la durée, tenir compte, dans le respect des principes constitutionnels, des principes généraux du droit et des règles résultant des engagements internationaux de la France, des quatre critères qu'elles énumèrent, sans pouvoir se limiter à ne prendre en compte que l'un ou plusieurs d'entre eux.... ,,2) La décision d'interdiction de retour doit comporter l'énoncé des considérations de droit et de fait qui en constituent le fondement, de sorte que son destinataire puisse à sa seule lecture en connaître les motifs. Si cette motivation doit attester de la prise en compte par l'autorité compétente, au vu de la situation de l'intéressé, de l'ensemble des critères prévus par la loi, aucune règle n'impose que le principe et la durée de l'interdiction de retour fassent l'objet de motivations distinctes, ni que soit indiquée l'importance accordée à chaque critère.,,,Il incombe ainsi à l'autorité compétente qui prend une décision d'interdiction de retour d'indiquer dans quel cas susceptible de justifier une telle mesure se trouve l'étranger. Elle doit par ailleurs faire état des éléments de la situation de l'intéressé au vu desquels elle a arrêté, dans son principe et dans sa durée, sa décision, eu égard notamment  à  la durée de la présence de l'étranger sur le territoire français, à la nature et à l'ancienneté de ses liens avec la France et, le cas échéant, aux précédentes mesures d'éloignement dont il a fait l'objet. Elle doit aussi, si elle estime que figure au nombre des motifs qui justifie sa décision une menace pour l'ordre public, indiquer les raisons pour lesquelles la présence de l'intéressé sur le territoire français doit, selon elle, être regardée comme une telle menace. En revanche, si, après prise en compte de ce critère, elle ne retient pas cette circonstance au nombre des motifs de sa décision, elle n'est pas tenue, à peine d'irrégularité, de le préciser expressément.</ANA>
<ANA ID="9B"> 335-03-01-02 1) Il ressort des termes mêmes des dispositions du III de l'article L. 511-1 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA) que l'autorité compétente doit, pour décider de prononcer à l'encontre de l'étranger soumis à l'obligation de quitter le territoire français une interdiction de retour et en fixer la durée, tenir compte, dans le respect des principes constitutionnels, des principes généraux du droit et des règles résultant des engagements internationaux de la France, des quatre critères qu'elles énumèrent, sans pouvoir se limiter à ne prendre en compte que l'un ou plusieurs d'entre eux.... ,,2) La décision d'interdiction de retour doit comporter l'énoncé des considérations de droit et de fait qui en constituent le fondement, de sorte que son destinataire puisse à sa seule lecture en connaître les motifs. Si cette motivation doit attester de la prise en compte par l'autorité compétente, au vu de la situation de l'intéressé, de l'ensemble des critères prévus par la loi, aucune règle n'impose que le principe et la durée de l'interdiction de retour fassent l'objet de motivations distinctes, ni que soit indiquée l'importance accordée à chaque critère.,,,Il incombe ainsi à l'autorité compétente qui prend une décision d'interdiction de retour d'indiquer dans quel cas susceptible de justifier une telle mesure se trouve l'étranger. Elle doit par ailleurs faire état des éléments de la situation de l'intéressé au vu desquels elle a arrêté, dans son principe et dans sa durée, sa décision, eu égard notamment  à  la durée de la présence de l'étranger sur le territoire français, à la nature et à l'ancienneté de ses liens avec la France et, le cas échéant, aux précédentes mesures d'éloignement dont il a fait l'objet. Elle doit aussi, si elle estime que figure au nombre des motifs qui justifie sa décision une menace pour l'ordre public, indiquer les raisons pour lesquelles la présence de l'intéressé sur le territoire français doit, selon elle, être regardée comme une telle menace. En revanche, si, après prise en compte de ce critère, elle ne retient pas cette circonstance au nombre des motifs de sa décision, elle n'est pas tenue, à peine d'irrégularité, de le préciser expressément.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., CE, 12 mars 2012, M.,, n° 354165, p. 83.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
