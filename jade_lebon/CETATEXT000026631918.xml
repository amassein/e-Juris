<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026631918</ID>
<ANCIEN_ID>JG_L_2012_11_000000342327</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/63/19/CETATEXT000026631918.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème / 1ère SSR, 14/11/2012, 342327</TITRE>
<DATE_DEC>2012-11-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>342327</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème / 1ère SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP POTIER DE LA VARDE, BUK LAMENT ; SCP LYON-CAEN, THIRIEZ ; GEORGES ; SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>M. Marc Pichon de Vendeuil</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier de Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:342327.20121114</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 9 août et 25 octobre 2010 au secrétariat du contentieux du Conseil d'Etat, présentés pour la commune de Mandelieu-la-Napoule, représentée par son maire ; la commune de Mandelieu-la-Napoule demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n°08MA03259-08MA03333-08MA03343-08MA03353 du 4 juin 2010 par lequel la cour administrative d'appel de Marseille a annulé, à la demande de la société Finareal, de Mme E...A..., de M. et Mme D...et de la SCI Nelson, les jugements du 22 mai 2008 du tribunal administratif de Nice et la délibération du 16 janvier 2006 du conseil municipal de la commune de Mandelieu-la-Napoule approuvant la révision du plan local d'urbanisme ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter les appels de la société Finareal, de MmeA..., de M. et Mme D...et de la SCI Nelson ;<br/>
<br/>
              3°) de mettre à la charge de la société Finareal le versement de la somme de 3.500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des collectivités territoriales, notamment son article L. 2121-12 ;<br/>
<br/>
              Vu le code de l'environnement ;<br/>
<br/>
              Vu le code de l'urbanisme, notamment son article L. 600-4-1 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Marc Pichon de Vendeuil, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les observations de Me Georges, avocat de la commune de Mandelieu-la Napoule et de la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la société Finareal,<br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à Me Georges, avocat de la commune de Mandelieu-la Napoule et à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la société Finareal ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par une délibération du 16 janvier 2006, le conseil municipal de Mandelieu-la-Napoule a approuvé la révision de son plan local d'urbanisme ; que, par quatre jugements du 22 mai 2008, le tribunal administratif de Nice a rejeté les demandes de la société Finareal, de MmeA..., de M. et Mme D...et de la SCI Nelson tendant à l'annulation de cette délibération, à l'exception de la demande de la société Finareal, à laquelle il a partiellement fait droit ; que, par un arrêt en date du 4 juin 2010, contre lequel la commune de Mandelieu-la-Napoule se pourvoit en cassation, la cour administrative d'appel de Marseille a annulé ces jugements et la délibération du 16 janvier 2006 ;<br/>
<br/>
              Sur l'intervention de la commune de Cannes :<br/>
<br/>
              2. Considérant que la commune de Cannes a intérêt au maintien de l'arrêt attaqué ; qu'ainsi, son intervention est recevable ;<br/>
<br/>
              Sur le bien-fondé de l'arrêt attaqué :<br/>
<br/>
              3. Considérant, en premier lieu, qu'aux termes de l'article L. 2121-12 du code général des collectivités territoriales : " Dans les communes de 3 500 habitants et plus, une note explicative de synthèse sur les affaires soumises à délibération doit être adressée avec la convocation aux membres du conseil municipal. " ;<br/>
<br/>
              4. Considérant qu'il résulte de ces dispositions que, dans les communes de 3 500 habitants et plus, la convocation aux réunions du conseil municipal doit être accompagnée d'une note explicative de synthèse portant sur chacun des points de l'ordre du jour ; que le défaut d'envoi de cette note ou son insuffisance entache d'irrégularité les délibérations prises, à moins que le maire n'ait fait parvenir aux membres du conseil municipal, en même temps que la convocation, les documents leur permettant de disposer d'une information adéquate pour exercer utilement leur mandat ; que cette obligation, qui doit être adaptée à la nature et à l'importance des affaires, doit permettre aux intéressés d'appréhender le contexte ainsi que de comprendre les motifs de fait et de droit des mesures envisagées et de mesurer les implications de leurs décisions ; qu'elle n'impose pas de joindre à la convocation adressée aux intéressés, à qui il est au demeurant loisible de solliciter des précisions ou explications conformément à l'article L. 2121-13 du même code, une justification détaillée du bien-fondé des propositions qui leur sont soumises ;<br/>
<br/>
              5. Considérant que la cour a relevé qu'il ressortait des pièces du dossier que la commune avait adressé aux membres du conseil municipal une note relative à la révision du plan local d'urbanisme, synthétisant les différentes étapes de sa procédure d'adoption, mentionnant l'avis favorable du commissaire enquêteur et proposant de tenir compte de certaines observations des personnes consultées à l'issue de l'enquête publique ; que cette note était notamment accompagnée d'un document portant sur les modifications pouvant être apportées au plan pour donner suite à ces différentes remarques, dont une rubrique mentionnait le projet de création d'un secteur Nx autorisant les affouillements et exhaussements de sol au titre des installations et travaux divers et proposait une modification du règlement du plan par l'adjonction d'un article N 2-8 ainsi rédigé : " Sont admis dans les seuls secteurs Nx/ - les dépôts de matériaux inertes à l'exclusion de toute structure technique destinée à l'accueil des déchets " ; qu'en jugeant que, en dépit de la communication de ces différents documents et nonobstant la très faible superficie, de quelques 3 hectares, du secteur dont s'agit, qui représentait moins d'un millième de celle de la commune, il n'avait pas été satisfait à l'obligation prescrite par l'article L. 2121-12, faute pour les membres du conseil municipal de connaître les motifs du choix de ce secteur et d'avoir disposé d'éléments permettant d'apprécier le bien-fondé de l'emplacement retenu pour ce site d'accueil de déchets inertes, la cour s'est méprise sur la portée cet article, qui n'imposait nullement de telles justifications ainsi qu'il a été dit ci-dessus, et a ainsi commis une erreur de droit ; <br/>
<br/>
              6. Considérant en revanche, en second lieu, qu'aux termes de l'article R. 123-19 du code de l'urbanisme : " Le projet de plan local d'urbanisme est soumis à l'enquête publique par le maire ou le président de l'établissement public de coopération intercommunale compétent dans les formes prévues par les articles R. 123-7 à R. 123-23 du code de l'environnement (...) " ; qu'aux termes de l'article R. 123-22 du code de l'environnement : " (...) Le commissaire enquêteur ou la commission d'enquête établit un rapport qui relate le déroulement de l'enquête et examine les observations recueillies. Le commissaire enquêteur ou la commission d'enquête consigne, dans un document séparé, ses conclusions motivées, en précisant si elles sont favorables ou non à l'opération (...) " ; qu'en relevant, pour juger que le commissaire enquêteur ne pouvait être regardé comme ayant formulé des conclusions motivées donnant son avis personnel sur le plan local d'urbanisme, que l'avis favorable rendu par l'intéressé à la fin de son rapport ne contenait pas, contrairement à ce que celui-ci énonçait, de réserves et qu'il ne se prononçait pas sur les avis émis par les personnes associées, et notamment celui du syndicat intercommunal suggérant la création d'une zone Nx, la cour a porté une appréciation souveraine sur les pièces du dossier, qui est exempte de dénaturation ;<br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que l'un des deux motifs retenus par la cour administrative d'appel de Marseille pour annuler la délibération du conseil municipal de la commune de Mandelieu-la-Napoule justifie légalement le dispositif de l'arrêt attaqué ; que, par suite, la commune de Mandelieu-la-Napoule n'est pas fondée à demander l'annulation de cet arrêt ;<br/>
<br/>
              Sur les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              8. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de la société Finareal, qui n'est pas, dans la présente instance, la partie perdante, la somme que demande la commune de Mandelieu-la-Napoule au titre des frais exposés par elle et non compris dans les dépens ; qu'en revanche, il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de  la commune de Mandelieu-la-Napoule la somme de 1.500 euros qui sera versée à la société Finareal au même titre ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E<br/>
                                     --------------<br/>
<br/>
		Article 1er  : L'intervention de la commune de Cannes est admise.<br/>
		Article 2 : Le pourvoi de la commune de Mandelieu-la-Napoule est rejeté.<br/>
Article 3 : La commune de Mandelieu-la-Napoule versera à la société Finareal une somme de 1.500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la commune de Mandelieu-la-Napoule, à la société Finareal, à Mme C...A..., à M. et Mme B...D..., à la SCI Nelson et à la commune de Cannes. <br/>
Copie en sera adressée pour information à la ministre de l'écologie, du développement durable et de l'énergie. <br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">135-02-01-02-01-01-01 COLLECTIVITÉS TERRITORIALES. COMMUNE. ORGANISATION DE LA COMMUNE. ORGANES DE LA COMMUNE. CONSEIL MUNICIPAL. FONCTIONNEMENT. CONVOCATION. - COMMUNE DE PLUS DE 3 500 HABITANTS - DÉFAUT D'ENVOI DE LA NOTE EXPLICATIVE DE SYNTHÈSE (ART. L. 2121-12 DU CGCT) - CONSÉQUENCE - PRINCIPE - IRRÉGULARITÉ DE LA DÉLIBÉRATION [RJ1] - EXCEPTION - ENVOI DE DOCUMENTS DÉLIVRANT UNE INFORMATION ADÉQUATE - MODALITÉS.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">135-02-01-02-01-03-01 COLLECTIVITÉS TERRITORIALES. COMMUNE. ORGANISATION DE LA COMMUNE. ORGANES DE LA COMMUNE. CONSEIL MUNICIPAL. DÉLIBÉRATIONS. DÉLIBÉRATIONS INTERVENUES À LA SUITE D'UNE PROCÉDURE IRRÉGULIÈRE. - COMMUNE DE PLUS DE 3 500 HABITANTS - DÉLIBÉRATIONS ADOPTÉES LORS D'UNE RÉUNION DU CONSEIL MUNICIPAL CONVOQUÉE SANS NOTE EXPLICATIVE DE SYNTHÈSE (ART. L. 2121-12 DU CGCT) [RJ1] - EXCEPTION - ENVOI DE DOCUMENTS DÉLIVRANT UNE INFORMATION ADÉQUATE - MODALITÉS.
</SCT>
<ANA ID="9A"> 135-02-01-02-01-01-01 Le défaut d'envoi, avec la convocation aux réunions du conseil municipal d'une commune de 3 500 habitants et plus, de la note explicative de synthèse portant sur chacun des points de l'ordre du jour prévue à l'article L. 2121-12 du code général des collectivités territoriales (CGCT) entache d'irrégularité les délibérations prises, à moins que le maire n'ait fait parvenir aux membres du conseil municipal, en même temps que la convocation, les documents leur permettant de disposer d'une information adéquate pour exercer utilement leur mandat. Cette obligation, qui doit être adaptée à la nature et à l'importance des affaires, doit permettre aux intéressés d'appréhender le contexte ainsi que de comprendre les motifs de fait et de droit des mesures envisagées et de mesurer les implications de leurs décisions. Elle n'impose pas de joindre à la convocation adressée aux intéressés une justification détaillée du bien-fondé des propositions qui leur sont soumises.</ANA>
<ANA ID="9B"> 135-02-01-02-01-03-01 Le défaut d'envoi, avec la convocation aux réunions du conseil municipal d'une commune de 3 500 habitants et plus, de la note explicative de synthèse portant sur chacun des points de l'ordre du jour prévue à l'article L. 2121-12 du code général des collectivités territoriales (CGCT) entache d'irrégularité les délibérations prises, à moins que le maire n'ait fait parvenir aux membres du conseil municipal, en même temps que la convocation, les documents leur permettant de disposer d'une information adéquate pour exercer utilement leur mandat. Cette obligation, qui doit être adaptée à la nature et à l'importance des affaires, doit permettre aux intéressés d'appréhender le contexte ainsi que de comprendre les motifs de fait et de droit des mesures envisagées et de mesurer les implications de leurs décisions. Elle n'impose pas de joindre à la convocation adressée aux intéressés une justification détaillée du bien-fondé des propositions qui leur sont soumises.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 12 juillet 1995, Commune de Fontenay-le-Fleury, n° 157092, T. p. 681 ; CE, 30 avril 1997, Commune de Sérignan, n° 158730, T. p. 699 ; CE, 30 décembre 2009, Commune du Cannet des Maures, n° 319942, T. pp. 911-990.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
