<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039230798</ID>
<ANCIEN_ID>JG_L_2019_10_000000420230</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/23/07/CETATEXT000039230798.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 16/10/2019, 420230, Publié au recueil Lebon</TITRE>
<DATE_DEC>2019-10-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>420230</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP POTIER DE LA VARDE, BUK LAMENT, ROBILLOT</AVOCATS>
<RAPPORTEUR>M. Bertrand Mathieu</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Guillaume Odinet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:420230.20191016</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B... F... née C... a demandé au tribunal administratif de Nouvelle-Calédonie d'annuler la décision du 12 mars 2012 par laquelle le D... national pour l'accès aux origines personnelles a refusé de lui communiquer l'identité de sa mère. Par un jugement n° 1500074 du 30 septembre 2015, le tribunal administratif de Nouvelle-Calédonie a rejeté sa demande. <br/>
<br/>
              Par un arrêt n° 15PA04869 du 30 janvier 2018, la cour administrative d'appel de Paris a rejeté l'appel formé par Mme B... F... contre ce jugement.  <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 30 avril et 27 juillet 2018 au secrétariat du contentieux du D... d'Etat, Mme F... demande au D... d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat, la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code civil ;<br/>
              - le code de l'action sociale et des familles ;<br/>
              - la loi du 27 juin 1904 sur le service des enfants assistés ;<br/>
              - l'acte dit " loi " n° 182 du 15 avril 1943 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bertrand Mathieu, conseiller d'Etat en service extraordinaire,<br/>
<br/>
              - les conclusions de M. Guillaume Odinet, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Potier de La Varde, Buk Lament, Robillot, avocat de Mme C... épouse F... ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1.	Il résulte des pièces du dossier soumis aux juges du fond que Mme B... F... née C... a été adoptée par M. E... C... et son épouse par jugement du 6 novembre 1952, quelques mois après sa naissance, déclarée le 11 juin 1952. Elle s'est adressée en septembre 2010 au D... national pour l'accès aux origines personnelles (CNAOP) pour tenter d'obtenir l'identité de ses parents biologiques. Par une décision du 12 mars 2012, le CNAOP a refusé de lui communiquer l'identité de sa mère biologique. Mme F... se pourvoit contre l'arrêt par lequel la cour administrative d'appel de Paris a rejeté l'appel qu'elle avait formé contre le jugement du 30 septembre 2015 du tribunal administratif de Nouvelle-Calédonie ayant rejeté sa demande d'annulation de cette décision.<br/>
<br/>
              2.	Aux termes de l'article L. 147-1 du code de l'action sociale et des familles " A... D... national, placé auprès du ministre chargé des affaires sociales, est chargé de faciliter, en liaison avec les départements et les collectivités d'outre-mer, l'accès aux origines personnelles ". Selon l'article L. 147-2 du même code, ce D... " reçoit : 1° La demande d'accès à la connaissance des origines de l'enfant formulée : / - s'il est majeur, par celui-ci ; (...) / 2° La déclaration de la mère ou, le cas échéant, du père de naissance par laquelle chacun d'entre eux autorise la levée du secret de sa propre identité ; (...) ". L'article L. 147-5 prévoit que pour répondre aux demandes dont il est saisi, ce D... recueille copie notamment " des éléments relatifs à l'identité : 1° De la femme qui a demandé le secret de son identité et de son admission lors de son accouchement dans un établissement de santé et, le cas échéant, de la personne qu'elle a désignée à cette occasion comme étant l'auteur de l'enfant (...) ". L'article L. 147-6 du même code dispose enfin que le D... communique aux personnes qui ont formulé et maintenu leur demande l'identité de la mère de naissance dans les cas qu'il énonce, notamment " s'il n'y a pas eu de manifestation expresse de [la volonté de la mère] de préserver le secret de son identité, après avoir vérifié sa volonté ".<br/>
<br/>
              3.	Il résulte de ces différentes dispositions que le CNAOP est tenu de refuser de satisfaire à la demande d'une personne, visant à connaître l'identité de la femme ayant accouché d'elle, lorsque cette dernière a manifesté la volonté de taire son identité lors de l'accouchement et a renouvelé expressément cette volonté en réponse à la demande de levée du secret.<br/>
<br/>
              4.	En premier lieu, pour juger que la décision de refus opposée à Mme F... par le CNAOP ne méconnaissait pas les dispositions citées au point 2, les juges d'appel ont estimé, après avoir cité les articles 8 et 9 de la loi du 27 juin 1904 relative au service des enfants assistés, d'une part, que les dispositions antérieures à l'entrée en vigueur du régime organisé par le code de l'action sociale et des familles décrit plus haut permettait à une mère de garder le secret sur son identité, d'autre part, que le CNAOP avait accompli les diligences prévues par les dispositions citées ci-dessus de ce code dont il ressortait la volonté expresse de la mère biologique de Mme F... de maintenir le secret. Si, en faisant application de la loi de 1904 alors que cette loi avait été abrogée par l'acte dit " loi " n° 182 du 15 avril 1943 relative à l'assistance à l'enfance, encore en vigueur à la date de l'accouchement, les juges d'appel, dont l'arrêt est suffisamment motivé, ont commis une erreur de droit, celle-ci est sans incidence sur le sens de leur décision dès lors que les dispositions de cet acte, en particulier les articles 6, 7, 11 et 39 organisaient la possibilité pour une mère de confier son enfant à des tiers en maintenant le secret de son identité. Il y a lieu, en conséquence, de remplacer, par une substitution de pur droit qui n'implique l'appréciation d'aucune circonstance de fait, le texte sur lequel la cour s'est fondée par l'acte dit " loi " du 15 avril 1943. Ainsi le moyen tiré de ce que la Cour administrative d'appel aurait commis une erreur de droit relative à la portée de la loi du 27 juin 1904 est inopérant. <br/>
<br/>
              5.	En second lieu, les dispositions précitées du code de l'action sociale et des familles organisent la possibilité de lever le secret de l'identité de la mère de naissance en permettant de solliciter la réversibilité du secret de son identité sous réserve de l'accord de celle-ci et définissent ainsi un équilibre entre le respect dû au droit à l'anonymat garanti à la mère lorsqu'elle a accouché et le souhait légitime de l'enfant né dans ces conditions de connaître ses origines. En estimant que Mme F..., dont il ressort des pièces du dossier soumis aux juges du fond qu'elle a pu disposer, hormis l'identité de sa mère biologique encore en vie, d'informations relatives à sa naissance recueillies par le CNAOP, n'était pas fondée à soutenir que les stipulations de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales avaient été méconnues, la cour administrative d'appel n'a pas commis d'erreur de qualification juridique.<br/>
<br/>
              6.	Il résulte de ce qui précède que Mme F... n'est pas fondée à demander l'annulation de l'arrêt attaqué. Ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er :  Le pourvoi de Mme F... est rejeté.<br/>
<br/>
Article 2 : La présente décision sera notifiée à Mme B... F... et à la ministre des solidarités et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">26-055-01-08 DROITS CIVILS ET INDIVIDUELS. CONVENTION EUROPÉENNE DES DROITS DE L'HOMME. DROITS GARANTIS PAR LA CONVENTION. DROIT AU RESPECT DE LA VIE PRIVÉE ET FAMILIALE (ART. 8). - CONDITIONS D'ACCÈS DE L'ENFANT AUX INFORMATIONS RELATIVES À UNE FEMME AYANT DEMANDÉ DE TAIRE SON IDENTITÉ LORS DE L'ACCOUCHEMENT (ART. L. 147-1 ET S. DU CASF) - MÉCONNAISSANCE - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">61-05 SANTÉ PUBLIQUE. BIOÉTHIQUE. - FEMME AYANT DEMANDÉ DE TAIRE SON IDENTITÉ LORS DE L'ACCOUCHEMENT ET AYANT RENOUVELÉ EXPRESSÉMENT CETTE VOLONTÉ EN RÉPONSE À LA DEMANDE DE LEVÉE DU SECRET - DEMANDE D'UNE PERSONNE VISANT À CONNAÎTRE L'IDENTITÉ DE LA FEMME AYANT ACCOUCHÉ D'ELLE 1) - RÉGIME ISSU DES DISPOSITIONS DU CASF - A) PORTÉE - CNAOP TENU DE REFUSER DE SATISFAIRE À CETTE DEMANDE - B) RÉGIME APPLICABLE AUX ACCOUCHEMENTS ANTÉRIEURS AUX DISPOSITIONS DU CASF - 2) COMPATIBILITÉ DE CE RÉGIME AVEC L'ART. 8 DE LA CONV. EDH - EXISTENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">61-09-001 SANTÉ PUBLIQUE. ADMINISTRATION DE LA SANTÉ. - CNAOP - FEMME AYANT DEMANDÉ DE TAIRE SON IDENTITÉ LORS DE L'ACCOUCHEMENT ET AYANT RENOUVELÉ EXPRESSÉMENT CETTE VOLONTÉ EN RÉPONSE À LA DEMANDE DE LEVÉE DU SECRET - DEMANDE D'UNE PERSONNE VISANT À CONNAÎTRE L'IDENTITÉ DE LA FEMME AYANT ACCOUCHÉ D'ELLE 1) - RÉGIME ISSU DES DISPOSITIONS DU CASF - A) PORTÉE - CNAOP TENU DE REFUSER DE SATISFAIRE À CETTE DEMANDE - B) RÉGIME APPLICABLE AUX ACCOUCHEMENTS ANTÉRIEURS AUX DISPOSITIONS DU CASF - 2) COMPATIBILITÉ DE CE RÉGIME AVEC L'ART. 8 DE LA CONV. EDH - EXISTENCE.
</SCT>
<ANA ID="9A"> 26-055-01-08 Les articles L. 147-1, L. 147-2, L. 147-5 et L. 147-6 du code de l'action sociale et des familles (CASF) organisent la possibilité de lever le secret de l'identité de la mère de naissance en permettant de solliciter la réversibilité du secret de son identité sous réserve de l'accord de celle-ci et définissent ainsi un équilibre entre le respect du au droit à l'anonymat garanti à la mère lorsqu'elle a accouché et le souhait légitime de l'enfant né dans ces conditions de connaître ses origines. En estimant que la requérante, dont il ressort des pièces du dossier soumis aux juges du fond qu'elle a pu disposer, hormis l'identité de sa mère biologique encore en vie, d'informations relatives à sa naissance recueillies par le Conseil national pour l'accès aux origines personnelles (CNAOP), n'était pas fondée à soutenir que les stipulations de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales (Conv EDH) avaient été méconnues, la cour administrative d'appel n'a pas commis d'erreur de qualification juridique.</ANA>
<ANA ID="9B"> 61-05 1) a) Il résulte des articles L. 147-1, L. 147-2, L. 147-5 et L. 147-6 du code de l'action sociale et des familles (CASF) que le Conseil national pour l'accès aux origines personnelles (CNAOP) est tenu de refuser de satisfaire à la demande, présentée par une personne, visant à connaître l'identité de la femme ayant accouché d'elle lorsque celle-ci a manifesté la volonté de taire son identité lors de l'accouchement et a renouvelé expressément cette volonté en réponse à la demande de levée du secret.,,,b) Pour juger que la décision de refus opposée à la requérante par le CNAOP ne méconnaissait pas ces dispositions, les juges d'appel ont estimé, après avoir cité les articles 8 et 9 de la loi du 27 juin 1904 relative au service des enfants assistés, d'une part que les dispositions antérieures à l'entrée en vigueur du régime organisé par le CASF permettait à une mère de garder le secret sur son identité, d'autre part que le CNAOP avait accompli les diligences prévues par les dispositions du CASF dont il ressortait la volonté expresse de la mère biologique de la requérante de maintenir le secret. Si, en faisant en application de la loi de 1904 alors que cette loi avait été abrogée par l'acte dit loi n° 182 du 15 avril 1943 relative à l'assistance à l'enfance, encore en vigueur à la date de l'accouchement, les juges d'appel, dont l'arrêt est suffisamment motivé, ont commis une erreur de droit, celle-ci est sans incidence sur le sens de leur décision dès lors que les dispositions de cet acte, en particulier les articles 6, 7, 11 et 39 organisaient la possibilité pour une mère de confier son enfant à des tiers en maintenant le secret de son identité. Il y a lieu, en conséquence, de remplacer, par une substitution de pur droit qui n'implique l'appréciation d'aucune circonstance de fait, le texte sur lequel la cour s'est fondée par l'acte dit loi du 15 avril 1943.,,,2) Ces dispositions du CASF organisent la possibilité de lever le secret de l'identité de la mère de naissance en permettant de solliciter la réversibilité du secret de son identité sous réserve de l'accord de celle-ci et définissent ainsi un équilibre entre le respect du au droit à l'anonymat garanti à la mère lorsqu'elle a accouché et le souhait légitime de l'enfant né dans ces conditions de connaître ses origines. En estimant que la requérante, dont il ressort des pièces du dossier soumis aux juges du fond qu'elle a pu disposer, hormis l'identité de sa mère biologique encore en vie, d'informations relatives à sa naissance recueillies par le CNAOP, n'était pas fondée à soutenir que les stipulations de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales (Conv EDH) avaient été méconnues, la cour administrative d'appel n'a pas commis d'erreur de qualification juridique.</ANA>
<ANA ID="9C"> 61-09-001 1) a) Il résulte des articles L. 147-1, L. 147-2, L. 147-5 et L. 147-6 du code de l'action sociale et des familles (CASF) que le Conseil national pour l'accès aux origines personnelles (CNAOP) est tenu de refuser de satisfaire à la demande, présentée par une personne, visant à connaître l'identité de la femme ayant accouché d'elle lorsque celle-ci a manifesté la volonté de taire son identité lors de l'accouchement et a renouvelé expressément cette volonté en réponse à la demande de levée du secret.,,,b) Pour juger que la décision de refus opposée à la requérante par le CNAOP ne méconnaissait pas ces dispositions, les juges d'appel ont estimé, après avoir cité les articles 8 et 9 de la loi du 27 juin 1904 relative au service des enfants assistés, d'une part que les dispositions antérieures à l'entrée en vigueur du régime organisé par le CASF permettait à une mère de garder le secret sur son identité, d'autre part que le CNAOP avait accompli les diligences prévues par les dispositions du CASF dont il ressortait la volonté expresse de la mère biologique de la requérante de maintenir le secret. Si, en faisant en application de la loi de 1904 alors que cette loi avait été abrogée par l'acte dit loi n° 182 du 15 avril 1943 relative à l'assistance à l'enfance, encore en vigueur à la date de l'accouchement, les juges d'appel, dont l'arrêt est suffisamment motivé, ont commis une erreur de droit, celle-ci est sans incidence sur le sens de leur décision dès lors que les dispositions de cet acte, en particulier les articles 6, 7, 11 et 39 organisaient la possibilité pour une mère de confier son enfant à des tiers en maintenant le secret de son identité. Il y a lieu, en conséquence, de remplacer, par une substitution de pur droit qui n'implique l'appréciation d'aucune circonstance de fait, le texte sur lequel la cour s'est fondée par l'acte dit loi du 15 avril 1943.,,,2) Ces dispositions du CASF organisent la possibilité de lever le secret de l'identité de la mère de naissance en permettant de solliciter la réversibilité du secret de son identité sous réserve de l'accord de celle-ci et définissent ainsi un équilibre entre le respect du au droit à l'anonymat garanti à la mère lorsqu'elle a accouché et le souhait légitime de l'enfant né dans ces conditions de connaître ses origines. En estimant que la requérante, dont il ressort des pièces du dossier soumis aux juges du fond qu'elle a pu disposer, hormis l'identité de sa mère biologique encore en vie, d'informations relatives à sa naissance recueillies par le CNAOP, n'était pas fondée à soutenir que les stipulations de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales (Conv EDH) avaient été méconnues, la cour administrative d'appel n'a pas commis d'erreur de qualification juridique.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
