<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028341080</ID>
<ANCIEN_ID>JG_L_2013_12_000000363690</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/34/10/CETATEXT000028341080.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  3ème sous-section jugeant seule, 17/12/2013, 363690</TITRE>
<DATE_DEC>2013-12-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>363690</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 3ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MONOD, COLIN, STOCLET ; SPINOSI</AVOCATS>
<RAPPORTEUR>M. Christian Fournier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:363690.20131217</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 2 novembre 2012 et 4 février 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour Mme B...A..., demeurant ... ; Mme A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 12BX01583 du 3 août 2012 par laquelle le président d'une chambre de la cour administrative d'appel de Bordeaux a rejeté l'appel qu'elle a interjeté du jugement n° 0801552 du 3 mai 2012 par lequel le tribunal administratif de Saint-Denis de La Réunion a rejeté sa demande tendant à la condamnation du centre communal d'action sociale (CCAS) de Saint-Joseph à lui verser la somme de 10 947 &#128; au titre d'arriérés de traitement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge du CCAS de Saint-Joseph la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Christian Fournier, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Spinosi, avocat de Mme B...A...et à la SCP Monod, Colin, avocat du centre communal d'action sociale de Saint Joseph ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article R. 222-1 du code de justice administrative : " (...) les présidents de formation de jugement (...) des cours peuvent, par ordonnance : (...) 4° Rejeter les requêtes manifestement irrecevables, lorsque la juridiction n'est pas tenue d'inviter leur auteur à les régulariser ou qu'elles n'ont pas été régularisées à l'expiration du délai imparti par une demande en ce sens ; (...) "  ; qu'aux termes de l'article R. 811-7 du même code : " Les appels ainsi que les mémoires déposés devant la cour administrative d'appel doivent être présentés, à peine d'irrecevabilité, par l'un des mandataires mentionnés à l'article R. 431-2 " ; qu'aux termes de l'article R. 431-2 du même code : " Les requêtes et les mémoires doivent, à peine d'irrecevabilité, être présentés soit par un avocat, soit par un avocat au Conseil d'Etat et à la Cour de cassation (...) " ; qu'aux termes de l'article R. 612-1 du même code :  " Lorsque des conclusions sont entachées d'une irrecevabilité susceptible d'être couverte après l'expiration du délai de recours, la juridiction ne peut les rejeter en relevant d'office cette irrecevabilité qu'après avoir invité leur auteur à les régulariser (...) " ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que Mme A...a interjeté appel du jugement du tribunal administratif qui lui a été notifié le <br/>
25 mai 2012 sans présenter sa requête par l'un des mandataires mentionnés à l'article R. 431-2 du code de justice administrative ; que la notification de ce jugement n'indiquant pas que la requête ne pouvait être présentée que par l'un de ces mandataires, l'intéressée a été invitée par le greffe de la cour administrative d'appel de Bordeaux, par courrier reçu le 9 juillet 2012, à régulariser sa requête dans un délai de quinze jours ; <br/>
<br/>
              3. Considérant, en premier lieu, qu'il résulte des dispositions mentionnées au point 1 que si une requête, qu'aucune disposition ne dispense du ministère d'avocat, n'a pas été présentée par l'un des mandataires mentionnés à l'article R. 431-2 du code de justice administrative et n'a pas été régularisée dans le délai imparti par la juridiction saisie dans la demande invitant l'auteur de la requête à la régulariser, le juge d'appel peut la rejeter sans attendre l'expiration du délai d'appel ;<br/>
<br/>
              4. Considérant, en deuxième lieu, qu'aux termes du troisième alinéa de l'article R. 612-1 du code de justice administrative : " La demande de régularisation mentionne que, à défaut de régularisation, les conclusions pourront être rejetées comme irrecevables dès l'expiration du délai imparti qui, sauf urgence, ne peut être inférieur à quinze jours (...) " ; que les délais supplémentaires de distance prévus à l'article R. 811-5 du même code s'ajoutent au délai d'appel mais non à un délai imparti par une juridiction, saisie d'une requête dans le délai de recours, pour la régularisation de celle-ci ; <br/>
<br/>
              5. Considérant, en troisième lieu, que si Mme A...fait état, devant le juge de cassation, d'une télécopie que l'avocat, qu'elle avait désigné après avoir reçu la demande de régularisation, aurait adressé le 23 juillet 2012 à la cour pour l'informer qu'il lui transmettrait dans les meilleurs délais une production aux fins de régulariser la procédure, cette télécopie, qui ne figure pas dans le dossier d'appel, n'a en tout état de cause été suivie d'aucune production adressée à la cour de la part de cet avocat ; <br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède qu'en relevant que la requête de Mme A...n'avait pas été régularisée par l'un des mandataires mentionnés à l'article R. 431-2 du code de justice administrative dans le délai de 15 jours imparti à la requérante, le président de chambre de la cour administrative d'appel de Bordeaux n'a pas dénaturé les pièces du dossier ; qu'il n'a méconnu, en jugeant que cette requête, qu'aucun texte ne dispensait de ministère d'avocat, était manifestement irrecevable, ni les dispositions du code de justice administrative citées aux points 1 et 4, ni les stipulations de l'article 6, paragraphe 1 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; que, dès lors, le pourvoi doit être rejeté, y compris ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de Mme A...le versement d'une somme au titre de ces dispositions ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de Mme A...est rejeté. <br/>
Article 2 : Les conclusions présentées par le centre communal d'action sociale de Saint Joseph au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à Mme B...A...et au centre communal d'action sociale de Saint-Joseph.<br/>
Copie en sera adressée pour information au ministre de l'intérieur.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-01-08-02 PROCÉDURE. INTRODUCTION DE L'INSTANCE. FORMES DE LA REQUÊTE. MINISTÈRE D'AVOCAT. - APPEL - REQUÊTE PRÉSENTÉE SANS MINISTÈRE D'AVOCAT ALORS QU'ELLE N'EST PAS DISPENSÉE DE CE MINISTÈRE - ABSENCE DE RÉGULARISATION DANS LE DÉLAI IMPARTI PAR LA JURIDICTION - CONSÉQUENCE - FACULTÉ DU JUGE D'APPEL DE REJETER LA REQUÊTE SANS ATTENDRE L'EXPIRATION DU DÉLAI D'APPEL - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-06-01 PROCÉDURE. JUGEMENTS. RÈGLES GÉNÉRALES DE PROCÉDURE. - APPEL - REQUÊTE PRÉSENTÉE SANS MINISTÈRE D'AVOCAT ALORS QU'ELLE N'EST PAS DISPENSÉE DE CE MINISTÈRE - DEMANDE DE RÉGULARISATION - 1) ABSENCE DE RÉGULARISATION DANS LE DÉLAI IMPARTI PAR LA JURIDICTION - CONSÉQUENCE - FACULTÉ DU JUGE D'APPEL DE REJETER LA REQUÊTE SANS ATTENDRE L'EXPIRATION DU DÉLAI D'APPEL - EXISTENCE - 2) DÉLAI IMPARTI PAR LA JURIDICTION POUR RÉGULARISER LA REQUÊTE (ART. R. 612-1 DU CJA) - AJOUT, LE CAS ÉCHÉANT, DU DÉLAI DE DISTANCE (ART. R. 811-5 DU CJA) - ABSENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-08-01 PROCÉDURE. VOIES DE RECOURS. APPEL. - REQUÊTE PRÉSENTÉE SANS MINISTÈRE D'AVOCAT ALORS QU'ELLE N'EST PAS DISPENSÉE DE CE MINISTÈRE - DEMANDE DE RÉGULARISATION - 1) ABSENCE DE RÉGULARISATION DANS LE DÉLAI IMPARTI PAR LA JURIDICTION - CONSÉQUENCE - FACULTÉ DU JUGE D'APPEL DE REJETER LA REQUÊTE SANS ATTENDRE L'EXPIRATION DU DÉLAI D'APPEL - EXISTENCE - 2) DÉLAI IMPARTI PAR LA JURIDICTION POUR RÉGULARISER LA REQUÊTE (ART. R. 612-1 DU CJA) - AJOUT, LE CAS ÉCHÉANT, DU DÉLAI DE DISTANCE (ART. R. 811-5 DU CJA) - ABSENCE.
</SCT>
<ANA ID="9A"> 54-01-08-02 Si une requête, qu'aucune disposition ne dispense du ministère d'avocat, n'a pas été présentée par l'un des mandataires mentionnés à l'article R. 431-2 du code de justice administrative et n'a pas été régularisée dans le délai imparti par la juridiction pour régulariser la requête, le juge d'appel peut la rejeter sans attendre l'expiration du délai d'appel.</ANA>
<ANA ID="9B"> 54-06-01 1) Si une requête, qu'aucune disposition ne dispense du ministère d'avocat, n'a pas été présentée par l'un des mandataires mentionnés à l'article R. 431-2 du code de justice administrative (CJA) et n'a pas été régularisée dans le délai imparti par la juridiction pour régulariser la requête, le juge d'appel peut la rejeter sans attendre l'expiration du délai d'appel.,,,2) Les délais supplémentaires de distance prévus à l'article R. 811-5 du CJA s'ajoutent au délai d'appel mais non à un délai imparti par une juridiction, saisie d'une requête dans le délai de recours, pour la régularisation de celle-ci.</ANA>
<ANA ID="9C"> 54-08-01 1) Si une requête, qu'aucune disposition ne dispense du ministère d'avocat, n'a pas été présentée par l'un des mandataires mentionnés à l'article R. 431-2 du code de justice administrative (CJA) et n'a pas été régularisée dans le délai imparti par la juridiction pour régulariser la requête, le juge d'appel peut la rejeter sans attendre l'expiration du délai d'appel.,,,2) Les délais supplémentaires de distance prévus à l'article R. 811-5 du CJA s'ajoutent au délai d'appel mais non à un délai imparti par une juridiction, saisie d'une requête dans le délai de recours, pour la régularisation de celle-ci.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
