<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030236203</ID>
<ANCIEN_ID>JG_L_2015_02_000000384302</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/23/62/CETATEXT000030236203.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème / 9ème SSR, 13/02/2015, 384302</TITRE>
<DATE_DEC>2015-02-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>384302</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème / 9ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Romain Godet</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:384302.20150213</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 8 et 30 septembre 2014 et le 23 décembre 2014 au secrétariat du contentieux du Conseil d'Etat, les communes de Punaauia, de Taiarapu-Est, de Bora-Bora et d'Arue demandent au Conseil d'Etat :<br/>
<br/>
              1°) de déclarer illégale la " loi du pays " de Polynésie française n° 2014/25 LP/APF du 29 juillet 2014 relative aux conditions d'admission au régime de solidarité (RST) et au contrôle de leur respect ;<br/>
<br/>
              2°) de mettre à la charge de la Polynésie française, pour chacune d'entre elles, la somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu :<br/>
              - les autres pièces du dossier ;<br/>
              - la Constitution, notamment son Préambule et son article 74 ;<br/>
              - la Convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - la Charte des droits fondamentaux de l'Union européenne ;<br/>
              - la loi organique n° 2004-192 du 27 février 2004 ;<br/>
              - la loi n° 78-17 du 6 janvier 1978 ;	<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Romain Godet, maître des requêtes en service extraordinaire, <br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat des communes de Punaauia, de Taiarapu-Est, de Bora Bora et d'Arue ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que l'assemblée de la Polynésie française a adopté le 29 juillet 2014, sur le fondement de l'article 140 de la loi organique du 27 février 2004, une " loi du pays " relative aux conditions d'admission au régime de solidarité territorial (RST) et au contrôle de leur respect ; que cette " loi du pays " a été publiée au Journal officiel de la Polynésie française, à titre d'information, le 8 août 2014 ; que, dans le cadre du contrôle juridictionnel spécifique défini au chapitre II du titre VI de cette loi organique, les communes de Punaauia, de Taiarapu-Est, de Bora-Bora et d'Arue ont saisi le Conseil d'Etat d'une requête tendant à ce que cette " loi du pays " soit déclarée illégale ;<br/>
<br/>
              Sur l'intervention de M. B...:<br/>
<br/>
              2. Considérant que M.B..., qui se borne à se prévaloir de sa qualité de bénéficiaire du régime de solidarité territorial sans en justifier, ne fait pas état d'un intérêt de nature à rendre son intervention recevable ;<br/>
<br/>
              Sur l'irrecevabilité du mémoire en défense de la Polynésie française :<br/>
<br/>
              3. Considérant, d'une part, que, par l'article 3 de l'arrêté n° 750 CM du 23 mai 2013 pris sur le fondement de l'article 92 de la loi organique du 27 février 2004, le conseil des ministres a délégué au président de la Polynésie française le pouvoir d'intenter ou de soutenir toute action au nom de la Polynésie française devant les juridictions de l'ordre administratif ; que, d'autre part, par l'article 3 de l'arrêté n° 686 PR du 19 septembre 2014 pris sur le fondement de l'article 96 de la même loi organique, le président du gouvernement a donné délégation de signature au secrétaire général du gouvernement, M. C...D..., à l'effet de signer en son nom tous mémoires déposés à l'occasion d'instances devant les juridictions de l'ordre administratif ; qu'il s'ensuit que les communes requérantes ne sont pas fondées à soutenir que le mémoire présenté par la Polynésie française serait irrecevable ;<br/>
<br/>
              Sur la légalité externe de la " loi du pays " attaquée :<br/>
<br/>
              4. Considérant, en premier lieu, qu'aux termes du 10° de l'article 14 de la loi organique du 27 février 2004, l'Etat est compétent dans les matières suivantes : " Règles relatives à l'administration, à l'organisation et aux compétences des communes, de leurs groupements et de leurs établissements publics ; coopération intercommunale ; contrôle des actes des communes, de leurs groupements et de leurs établissements publics ; régime comptable et financier et contrôle budgétaire de ces collectivités ; fonction publique communale ; domaine public communal ; dénombrement de la population ; " ; qu'il résulte du premier alinéa de l'article 140 de la même loi organique que : " Les actes de l'assemblée de la Polynésie française, dénommés "lois du pays", sur lesquels le Conseil d'État exerce un contrôle juridictionnel spécifique, sont ceux qui, relevant du domaine de la loi, soit ressortissent à la compétence de la Polynésie française en application de l'article 13, soit sont pris au titre de la participation de la Polynésie française à l'exercice des compétences de l'État dans les conditions prévues aux articles 31 à 36. " ; qu'en outre, en vertu du II de l'article 43 de cette même loi : " Dans les conditions définies par les actes prévus à l'article 140 dénommés "lois du pays" et la réglementation édictée par la Polynésie française, sous réserve du transfert des moyens nécessaires à l'exercice de ces compétences, les communes ou les établissements publics de coopération intercommunale peuvent intervenir dans les matières suivantes : (...) 2° Aide sociale ; (...) " ;<br/>
<br/>
              5. Considérant qu'il résulte de ces dispositions que l'aide sociale constitue une compétence dont l'exercice peut être partagé entre la Polynésie française et les communes ; que les conditions de cet exercice sont déterminées par une " loi du pays " ; qu'ainsi, en dépit des dispositions de l'article 14 de la loi organique, qui confie à l'Etat la compétence pour déterminer les principales règles applicables en matière d'administration communale, les conditions d'intervention des communes en matière d'aide sociale relèvent d'une " loi du pays " adoptée par l'assemblée de la Polynésie française ; qu'il s'ensuit que la " loi du pays " attaquée, qui modifie les conditions dans lesquelles les communes vérifient les dossiers de demande d'admission au régime de solidarité territorial avant de les transmettre avec avis motivé à la Polynésie française, n'est pas entachée d'incompétence ;<br/>
<br/>
              6. Considérant, en deuxième lieu, qu'à supposer que le rapport de la commission de la santé et du travail de l'assemblée de la Polynésie française n'ait pas été mis en distribution le 16 juillet 2014 mais le 18 juillet 2014, cette circonstance est sans incidence sur la régularité de la procédure d'adoption de la " loi du pays " adoptée le 29 juillet 2014, dès lors que les membres de l'assemblée ont été mis à même de prendre connaissance de ce rapport dans le respect du délai de douze jours, qui n'est pas un délai franc, prévu par l'article 130 de la loi organique du 27 février 2004 ; <br/>
<br/>
              7. Considérant, en troisième lieu, qu'en l'absence de détournement de procédure, il n'appartient pas au Conseil d'Etat, dans le cadre de l'article 176 de la loi organique du 27 février 2004, d'apprécier le bien-fondé de la déclaration d'urgence prononcée par le gouvernement ou l'assemblée de la Polynésie française en application du II de l'article 151 de cette même loi, qui a eu pour effet de ramener d'un mois à quinze jours le délai imparti au conseil économique, social et culturel de la Polynésie française pour donner son avis sur le projet de " loi du pays " ; <br/>
<br/>
              8. Considérant, en quatrième lieu, que l'avis rendu par le conseil économique, social et culturel est un avis qui ne s'impose pas à l'assemblée ; qu'ainsi, le moyen tiré de ce que l'assemblée n'aurait tenu aucun compte des observations formulées par ce conseil dans son avis ne peut qu'être écarté ; <br/>
<br/>
              9. Considérant, en cinquième lieu, que, si les dispositions du II de l'article 151 de la loi organique imposent que, lorsqu'il est saisi, le conseil économique, social et culturel le soit de l'ensemble des questions posées par un projet de " loi du pays " avant son adoption par le conseil des ministres de la Polynésie française, elles ne font pas obstacle à ce que des amendements, y compris d'origine gouvernementale, soient soumis au vote de l'assemblée dès lors que ces amendements ne sont pas dépourvus de tout lien avec le texte soumis à celle-ci ; qu'en l'espèce, il ressort de la comparaison du texte soumis au conseil économique, social et culturel et de celui qui a été soumis au vote de l'assemblée que les modifications qui ont été apportées au premier proviennent d'amendements adoptés par la commission de la santé et du travail de l'assemblée de la Polynésie française ; que, par suite, le moyen tiré de ce que ces modifications imposaient une nouvelle consultation du conseil économique, social et culturel doit être écarté ;<br/>
<br/>
              Sur la légalité interne de la " loi du pays " contestée :<br/>
<br/>
              10. Considérant, en premier lieu, qu'aux termes de l'article 48 de la loi organique du 27 février 2004 : " Les autorités de la Polynésie française peuvent déléguer aux maires ou aux présidents des établissements publics de coopération intercommunale les compétences pour prendre les mesures individuelles d'application des actes prévus à l'article 140 dénommés "lois du pays" et des réglementations édictées par ces autorités. / La délégation de compétences ne peut intervenir qu'avec l'accord du conseil municipal de la commune intéressée ou de l'assemblée délibérante de l'établissement public de coopération intercommunale intéressé et s'accompagne du transfert des moyens nécessaires à l'exercice des pouvoirs qui font l'objet de la délégation. " ; qu'en confiant aux communes la seule mission de vérifier et d'attester la véracité des éléments fournis par les personnes demandant le bénéfice du régime de solidarité territorial, sans leur attribuer le pouvoir de décider de l'admission de ces personnes au bénéfice de ce régime, l'article LP 4 de la " loi du pays " attaquée n'a pas délégué une compétence aux fins de prendre " des mesures individuelles d'application " au sens de l'article 48 précité ; que, par suite, le moyen tiré de la méconnaissance de cet article ne peut qu'être écarté ; <br/>
<br/>
              11. Considérant, en deuxième lieu, que les dispositions de la " loi du pays " attaquée, notamment ses articles LP 11 à LP 14, ne mettent pas en oeuvre le droit de l'Union européenne ; qu'il s'ensuit que le moyen tiré de ce que ces dispositions méconnaîtraient les stipulations de l'article 8 de la Charte des droits fondamentaux de l'Union européenne est inopérant ; que, si ces mêmes dispositions, en ce qu'elles autorisent des enquêtes et des contrôles, prévoient un droit de communication de documents au profit des services de la collectivité ainsi qu'un échange d'informations nominatives et rendent inopposable le secret professionnel à l'égard de certaines catégories de personnes, sont de nature à permettre une ingérence, au sens de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, dans l'exercice du droit au respect de la vie privée et familiale des personnes qui sollicitent le bénéfice du régime de solidarité territorial, cette ingérence est justifiée par des motifs d'intérêt général tirés du " bien-être économique du pays " et de la " prévention des infractions pénales ", au sens de cette stipulation ; qu'en outre, ces dispositions, d'une part, définissent avec précision l'objet et la finalité des mesures qu'elles instituent et, d'autre part, limitent le champ des personnes et services susceptibles de connaître des documents ainsi que des informations couvertes par le secret professionnel ; qu'en tout état de cause, les dispositions de la loi du 6 janvier 1978 s'appliquent de plein droit aux traitements de données à caractère personnel qui pourraient éventuellement être mis en oeuvre, ainsi que le prévoit d'ailleurs le 7° de l'article 7 de la loi organique du 27 février 2014 ; qu'il en résulte que le moyen tiré de la méconnaissance des stipulations de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales doit être écarté ;<br/>
<br/>
              12. Considérant, en dernier lieu, que l'article 35 de la loi organique dispose que les " lois du pays " peuvent comporter des " dispositions permettant aux fonctionnaires et agents assermentés des administrations et services publics de la Polynésie française, autres que ceux mentionnés à l'article 34, de rechercher et de constater les infractions aux actes prévus à l'article 140 dénommés "lois du pays", aux délibérations de l'assemblée de la Polynésie française et aux arrêtés réglementaires du conseil des ministres dont ces administrations et services publics sont spécialement chargés de contrôler la mise en oeuvre " ; qu'il n'impose pas que les " lois du pays " qui comportent de telles mesures réitèrent l'exigence, figurant au troisième alinéa de cet article, que les agents habilités à effectuer des visites aux fins de procéder à ces recherches et constatations ne puissent intervenir qu'" en présence d'un officier de police judiciaire requis à cet effet " ; qu'ainsi, à supposer que le droit de contrôle et d'enquête prévu par l'article LP 12 de la " loi du pays " attaquée puisse être regardé comme concourant à la recherche ou à la constatation de l'une des infractions prévues à l'article LP 18 de cette même " loi du pays ", le moyen tiré de ce que cet article LP 12 serait illégal au motif que la " loi du pays " ne mentionne pas l'obligation de présence d'un officier de police judiciaire requis à cet effet doit être écarté ;<br/>
<br/>
              13. Considérant qu'il résulte de tout ce qui précède que les communes requérantes ne sont pas fondées à demander que la " loi du pays " qu'ils attaquent soit déclarée illégale ;<br/>
<br/>
              14. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de la Polynésie française qui n'est pas, dans la présente instance, la partie perdante ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par le président de la Polynésie française au titre des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'intervention de M. B...est rejetée.<br/>
Article 2 : La requête présentée par les communes de Punaauia, de Taiarapu-Est, de Bora-Bora et d'Arue est rejetée.<br/>
Article 3 : Les conclusions présentées par le président de la Polynésie française au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à la commune de Punaauia, premier requérant dénommé, au président de la Polynésie française, au président de l'assemblée de la Polynésie française, à la ministre des outre-mer et à M. A...B.... Les autres requérants seront informés de la présente décision par la SCP Piwnica et Molinié, qui les représente devant le Conseil d'Etat.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">46-01-02-02 OUTRE-MER. DROIT APPLICABLE. STATUTS. POLYNÉSIE FRANÇAISE. - CONTRÔLE JURIDICTIONNEL SPÉCIFIQUE DES LOIS DU PAYS (ART. 176 DE LA LOI ORGANIQUE DU 27 FÉVRIER 2004) - RÉGULARITÉ DE LA PROCÉDURE D'ADOPTION - APPRÉCIATION PAR LE CONSEIL D'ETAT DU BIEN-FONDÉ DE LA DÉCLARATION D'URGENCE - ABSENCE.
</SCT>
<ANA ID="9A"> 46-01-02-02 En l'absence de détournement de procédure, il n'appartient pas au Conseil d'Etat, dans le cadre de l'article 176 de la loi organique n° 2004-192 du 27 février 2004, d'apprécier le bien-fondé de la déclaration d'urgence prononcée par le gouvernement ou l'assemblée de la Polynésie française en application du II de l'article 151 de cette même loi, qui a eu pour effet de ramener d'un mois à quinze jours le délai imparti au conseil économique, social et culturel de la Polynésie française pour donner son avis sur le projet de  loi du pays .</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
