<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029498131</ID>
<ANCIEN_ID>JG_L_2014_09_000000363252</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/49/81/CETATEXT000029498131.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème / 9ème SSR, 24/09/2014, 363252, Publié au recueil Lebon</TITRE>
<DATE_DEC>2014-09-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>363252</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème / 9ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DE CHAISEMARTIN, COURJON ; FOUSSARD</AVOCATS>
<RAPPORTEUR>M. Frédéric Bereyziat</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Delphine Hedary</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:363252.20140924</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
      Vu la procédure suivante :<br/>
<br/>
Procédure contentieuse antérieure<br/>
	La société Maxima a demandé au tribunal administratif de la Polynésie française d'annuler l'arrêté n° 968 CM pris le 23 juin 2010 par le conseil des ministres de la Polynésie française et relatif à l'agrément administratif des entreprises d'assurances. Par un jugement n° 1000355/1 du 26 octobre 2010, le tribunal a fait droit à cette demande.<br/>
	Par un arrêt n° 11PA00491 du 5 juillet 2012, la cour administrative d'appel de Paris a rejeté l'appel formé par la Polynésie française contre ce jugement.<br/>
Procédure devant le Conseil d'Etat <br/>
	Par un pourvoi, enregistré le 5 octobre 2012 au secrétariat du contentieux du Conseil d'Etat, et un mémoire en réplique, enregistré le 4 décembre 2013, la Polynésie française demande au Conseil d'Etat :<br/>
	1°) d'annuler l'arrêt n° 11PA00491 du 5 juillet 2012 de la cour administrative d'appel de Paris ;<br/>
	2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
	3°) de mettre à la charge de la société Maxima la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu :<br/>
              - les autres pièces du dossier ;<br/>
              - la loi organique n° 2004-192 du 27 février 2004, notamment ses articles 64, 89 à 91 et 102 ;<br/>
              - le code de justice administrative.<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Béreyziat, maître des requêtes,<br/>
<br/>
              - les conclusions de Mme Delphine Hedary, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP de Chaisemartin, Courjon, avocat de la présidence de la Polynésie française et à Me Foussard, avocat de la société Maxima ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que le conseil des ministres de la Polynésie française a pris, le 23 juin 2010, un arrêté fixant, d'une part, les conditions que devaient satisfaire les entreprises d'assurances souhaitant s'établir en Polynésie française afin d'obtenir l'agrément administratif préalable prévu par l'article L. 321-1 du code des assurances, dans sa rédaction applicable à ce territoire, d'autre part, les modalités de dépôt et d'examen des demandes d'agrément correspondantes ; que le tribunal administratif de la Polynésie française, saisi par la société Maxima et statuant par un jugement du 26 octobre 2010, a annulé cet arrêté, au motif qu'il avait été pris par une autorité incompétente ; que la Polynésie française se pourvoit en cassation contre l'arrêt du 5 juillet 2012 par lequel la cour administrative d'appel de Paris a rejeté l'appel dirigé contre ce jugement ;<br/>
<br/>
              2. Considérant, d'une part, qu'aux termes de l'article 11 de la loi organique du 27 février 2004 portant statut d'autonomie de la Polynésie française : " Les lois, ordonnances et décrets intervenus avant l'entrée en vigueur de la présente loi organique dans des matières qui relèvent désormais de la compétence des autorités de la Polynésie française peuvent être modifiés ou abrogés, en tant qu'ils s'appliquent à la Polynésie française, par les autorités de la Polynésie française selon les procédures prévues par la présente loi organique " ; qu'en vertu de ces dispositions, les règles applicables en Polynésie française dans le domaine d'une compétence transférée aux autorités de la Polynésie française sont celles qui la régissaient sur le territoire de la collectivité à la date d'entrée en vigueur de la loi organique, sous réserve qu'elles n'aient pas été postérieurement modifiées ou abrogées par les autorités compétentes de la Polynésie française ;<br/>
<br/>
              3. Considérant, d'autre part, qu'en vertu de l'article 102 de la même loi organique, toutes les matières qui sont de la compétence de la Polynésie française relèvent de l'assemblée de la Polynésie française, à l'exception de celles qui sont attribuées par la même loi au conseil des ministres ou au président de la Polynésie française ; qu'en vertu du troisième alinéa de l'article 89 de cette loi, le conseil des ministres prend les règlements nécessaires à la mise en oeuvre des actes prévus à l'article 140 dénommés " lois du pays " ainsi que des autres délibérations de l'assemblée de la Polynésie française ou de sa commission permanente ; <br/>
<br/>
              4. Considérant qu'il résulte de la combinaison des dispositions organiques citées ci-dessus que les règles qui régissaient, sur le territoire de la Polynésie française et à la date d'entrée en vigueur de la loi organique, le domaine d'une compétence transférée par cette loi aux autorités de la Polynésie française et qui n'ont pas été postérieurement modifiées ou abrogées par l'autorité désormais compétente, peuvent faire l'objet de mesures réglementaires d'application prises par le conseil des ministres sur le fondement du troisième alinéa de l'article 89 cité ci-dessus, quand bien même aucune " loi du pays " ni aucune autre délibération de l'assemblée de la Polynésie française ou de sa commission permanente n'aurait expressément habilité le conseil des ministres à cette fin ;<br/>
<br/>
              5. Considérant qu'il ressort des énonciations de l'arrêt attaqué que, pour juger l'arrêté du 23 juin 2010 pris par une autorité incompétente, la cour, après avoir vérifié que la réglementation des activités d'assurance ressortissait, en application de la loi organique, à la compétence des autorités de la Polynésie française, a notamment relevé qu'aucun acte dénommé " loi du pays " ni aucune délibération de l'assemblée de Polynésie française n'avait habilité le conseil des ministres à intervenir dans ce domaine ; qu'il résulte de ce qui vient d'être dit qu'en subordonnant à une telle habilitation l'exercice, par le conseil des ministres, du pouvoir réglementaire d'application qu'il tient directement des dispositions du troisième alinéa de l'article 89 de la loi organique, la cour a méconnu ces dispositions ; que, dès lors et sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, son arrêt doit être annulé ;<br/>
<br/>
              6. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la Polynésie française qui n'est pas, dans la présente instance, la partie perdante ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions que cette dernière présente au titre des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Paris du 5 juillet 2012 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Paris.<br/>
Article 3 : Le surplus des conclusions du pourvoi de la Polynésie française est rejeté.<br/>
Article 4 : Les conclusions présentées par la société anonyme Maxima au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à la Polynésie française et à la société anonyme Maxima.<br/>
Copie en sera adressée à la ministre des outre-mer.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">46-01-01-005 OUTRE-MER. DROIT APPLICABLE. GÉNÉRALITÉS. - POLYNÉSIE FRANÇAISE - LOI ORGANIQUE DU 27 FÉVRIER 2004 - TRANSFERT D'UNE COMPÉTENCE AUX AUTORITÉS DE LA POLYNÉSIE FRANÇAISE - 1) PRINCIPE - PERMANENCE DES RÈGLES QUI RÉGISSAIENT LE DOMAINE DE CETTE COMPÉTENCE TRANSFÉRÉE ET QUI N'ONT PAS ÉTÉ POSTÉRIEUREMENT MODIFIÉES OU ABROGÉES PAR L'AUTORITÉ DÉSORMAIS COMPÉTENTE - EXISTENCE [RJ1] - COMPÉTENCE DU CONSEIL DES MINISTRES POUR ÉDICTER DES MESURES RÉGLEMENTAIRES D'APPLICATION - EXISTENCE, SANS QU'UNE HABILITATION SOIT NÉCESSAIRE - 2) APPLICATION - RÉGLEMENTATION DES ACTIVITÉS D'ASSURANCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">46-01-02-02 OUTRE-MER. DROIT APPLICABLE. STATUTS. POLYNÉSIE FRANÇAISE. - LOI ORGANIQUE DU 27 FÉVRIER 2004 - RÉPARTITION DES COMPÉTENCES ENTRE L'ETAT ET LA POLYNÉSIE FRANÇAISE - TRANSFERT D'UNE COMPÉTENCE AUX AUTORITÉS DE LA POLYNÉSIE FRANÇAISE - 1) PRINCIPE - PERMANENCE DES RÈGLES QUI RÉGISSAIENT LE DOMAINE DE CETTE COMPÉTENCE TRANSFÉRÉE ET QUI N'ONT PAS ÉTÉ POSTÉRIEUREMENT MODIFIÉES OU ABROGÉES PAR L'AUTORITÉ DÉSORMAIS COMPÉTENTE - EXISTENCE [RJ1] - COMPÉTENCE DU CONSEIL DES MINISTRES POUR ÉDICTER DES MESURES RÉGLEMENTAIRES D'APPLICATION  - EXISTENCE, SANS QU'UNE HABILITATION SOIT NÉCESSAIRE - 2) APPLICATION - RÉGLEMENTATION DES ACTIVITÉS D'ASSURANCE.
</SCT>
<ANA ID="9A"> 46-01-01-005 1) Il résulte de la combinaison des dispositions des articles 11, 89, 102 et 140 de la loi organique n° 2004-192 du 27 février 2004 portant statut d'autonomie de la Polynésie française que les règles qui régissaient, sur le territoire de la Polynésie française et à la date d'entrée en vigueur de la loi organique, le domaine d'une compétence transférée par cette loi aux autorités de la Polynésie française et qui n'ont pas été postérieurement modifiées ou abrogées par l'autorité désormais compétente, peuvent faire l'objet de mesures réglementaires d'application prises par le conseil des ministres sur le fondement du troisième alinéa de l'article 89 de la loi organique, quand bien même aucune  loi du pays  ni aucune autre délibération de l'assemblée de la Polynésie française ou de sa commission permanente n'aurait expressément habilité le conseil des ministres à cette fin.,,,2) Le conseil des ministres a donc pu exercer dans le domaine de la réglementation des activités d'assurance, sans qu'un tel exercice soit subordonné à une habilitation à intervenir en cette matière, le pouvoir réglementaire d'application qu'il tient directement des dispositions du troisième alinéa de l'article 89 de la loi organique du 27 février 2004.</ANA>
<ANA ID="9B"> 46-01-02-02 1) Il résulte de la combinaison des dispositions des articles 11, 89, 102 et 140 de la loi organique n° 2004-192 du 27 février 2004 portant statut d'autonomie de la Polynésie française que les règles qui régissaient, sur le territoire de la Polynésie française et à la date d'entrée en vigueur de la loi organique, le domaine d'une compétence transférée par cette loi aux autorités de la Polynésie française et qui n'ont pas été postérieurement modifiées ou abrogées par l'autorité désormais compétente, peuvent faire l'objet de mesures réglementaires d'application prises par le conseil des ministres sur le fondement du troisième alinéa de l'article 89 de la loi organique, quand bien même aucune  loi du pays  ni aucune autre délibération de l'assemblée de la Polynésie française ou de sa commission permanente n'aurait expressément habilité le conseil des ministres à cette fin.,,,2) Le conseil des ministres a donc pu exercer dans le domaine de la réglementation des activités d'assurance, sans qu'un tel exercice soit subordonné à une habilitation à intervenir en cette matière, le pouvoir réglementaire d'application qu'il tient directement des dispositions du troisième alinéa de l'article 89 de la loi organique du 27 février 2004.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, avis, 12 mars 2010, Société Maxima, n° 333820, T. pp. 866-868.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
