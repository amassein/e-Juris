<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033308576</ID>
<ANCIEN_ID>JG_L_2016_10_000000391208</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/30/85/CETATEXT000033308576.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 21/10/2016, 391208</TITRE>
<DATE_DEC>2016-10-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>391208</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP COUTARD, MUNIER-APAIRE ; SCP MEIER-BOURDEAU, LECUYER</AVOCATS>
<RAPPORTEUR>Mme Isabelle Lemesle</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:391208.20161021</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              MM. C...et B...A...ont demandé au tribunal administratif de Strasbourg d'annuler pour excès de pouvoir l'arrêté préfectoral du 11 juillet 2007 par lequel le préfet de la Moselle a déclaré d'utilité publique le projet d'aménagement de la zone d'aménagement concerté (ZAC) du Sansonnet à Metz-devant-les-Ponts, ainsi que l'arrêté préfectoral du 10 décembre 2007 par lequel le préfet de la Moselle a déclaré cessibles les terrains nécessaires à la réalisation de cette zone. Par un jugement nos 0704793, 0704822, 0800735, 0800789, 0800791, 0800792 du 18 mai 2010 le tribunal administratif de Strasbourg a rejeté leur demande.<br/>
<br/>
              Par un arrêt n° 10NC01137, 10NC01160 du 9 juin 2011 la cour administrative d'appel de Nancy a rejeté l'appel formé MM. A...contre ce jugement. <br/>
<br/>
              Par une décision nos 351798, 351799 du 19 juin 2013, le Conseil d'Etat, statuant au contentieux, a annulé l'arrêt du 9 juin 2011 de la cour administrative d'appel de Nancy et lui a renvoyé l'affaire.<br/>
<br/>
              Par un arrêt n° 13NC01780 du 21 avril 2015, la cour administrative d'appel de Nancy, statuant sur renvoi, a rejeté l'appel de MM. A...contre le jugement du 18 mai 2010 du tribunal administratif de Strasbourg.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 22 juin 2015 et 22 septembre 2015 au secrétariat du contentieux du Conseil d'Etat, MM. A...demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'arrêt du 21 avril 2015 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel ;<br/>
<br/>
              3°) de mettre à la charge de l'établissement public foncier de Lorraine le versement d'une somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'expropriation pour cause d'utilité publique ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Isabelle Lemesle, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Coutard, Munier-Apaire, avocat de M. C...A...et de M. B...A...et à la SCP Meier-Bourdeau, Lecuyer, avocat de l'établissement public foncier de la métropole Lorraine ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que MM. C...et B...A...demandent l'annulation de l'arrêt du 21 avril 2015 par lequel la cour administrative d'appel de Nancy, statuant sur renvoi du Conseil d'Etat, a rejeté leur requête tendant à l'annulation du jugement du tribunal administratif de Strasbourg du 18 mai 2010 rejetant leur demande d'annulation pour excès de pouvoir des arrêtés du préfet de Moselle des 11 juillet 2007 et 10 décembre 2007 déclarant, pour le premier, d'utilité publique le projet d'aménagement de la zone d'aménagement concerté (ZAC) du Sansonnet à Metz et, pour le second, cessibles les terrains nécessaires en vue de la réalisation de cette zone ;<br/>
<br/>
              Sur l'arrêt attaqué en tant qu'il statue sur les conclusions dirigées contre l'arrêté portant déclaration d'utilité publique :<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 11-1 du code de l'expropriation pour cause d'utilité publique, dans sa rédaction applicable en l'espèce : " L'expropriation d'immeubles, en tout ou partie, ou de droits réels immobiliers, ne peut être prononcée qu'autant qu'elle aura été précédée d'une déclaration d'utilité publique intervenue à la suite d'une enquête et qu'il aura été procédé contradictoirement à la détermination des parcelles à exproprier, ainsi qu'à la recherche des propriétaires, des titulaires de droits réels et des autres intéressés./L'enquête préalable à la déclaration d'utilité publique est menée par un commissaire enquêteur ou une commission d'enquête dont les modalités de désignation et les pouvoirs sont définis par les dispositions du chapitre III du titre II du livre Ier du code de l'environnement(...) " ; que l'article R. 11-1 du même code précise que le commissaire enquêteur examine les observations consignées ou annexées aux registres et entend toutes personnes qu'il paraît utile de consulter ainsi que l'expropriant s'il le demande et qu'il rédige des conclusions motivées, en précisant si elles sont favorables ou non à l'opération ;<br/>
<br/>
              3. Considérant, en premier lieu, qu'il ressort des pièces du dossier soumis aux juges du fond que le commissaire enquêteur a émis un avis favorable au projet sous la réserve que trois parcelles en soient exclues et que sept propriétaires, dont les requérants, puissent obtenir du maître d'ouvrage la cession de terrains équipés, sous condition de participation financière au coût de l'ensemble de ces équipements ; que la cour a jugé, par une appréciation souveraine non arguée de dénaturation, que la réserve ainsi exprimée devait être regardée non comme une condition préalable subordonnant le caractère d'utilité publique du projet d'expropriation, mais comme une simple recommandation ; qu'elle a pu en déduire sans erreur de droit, après avoir relevé que le commissaire enquêteur n'avait remis en cause ni l'économie générale, ni l'utilité publique du projet, que l'avis qu'il avait rendu ne pouvait être regardé comme défavorable ;<br/>
<br/>
              4. Considérant, en second lieu, qu'il appartient au juge, lorsqu'il doit se prononcer sur le caractère d'utilité publique d'une opération nécessitant l'expropriation d'immeubles ou de droits réels immobiliers, de contrôler successivement que cette opération répond à une finalité d'intérêt général, que l'expropriant n'était pas en mesure de réaliser l'opération dans des conditions équivalentes sans recourir à l'expropriation, notamment en utilisant des biens se trouvant dans son patrimoine, et enfin, que les atteintes à la propriété privée, le coût financier et, le cas échéant, les inconvénients d'ordre social ou économique que comporte l'opération ne sont pas excessifs eu égard à l'intérêt qu'elle présente ; <br/>
<br/>
              5. Considérant que la cour a relevé qu'il ressortait des pièces du dossier, et notamment de la délibération du conseil municipal de la commune de Metz en date du 26 janvier 2006, que cette commune avait entendu subordonner la création de la ZAC du Sansonnet à la conservation par elle de la maîtrise foncière de l'ensemble des terrains en constituant l'emprise, afin d'en assurer un aménagement global et cohérent ; que la cour a jugé que le respect de cette condition nécessitait de faire abstraction des limites parcellaires, notamment en matière de voirie et d'aménagement foncier, de sorte que la collectivité n'aurait pas été en mesure de réaliser cette opération d'urbanisation du quartier de " Devant-les-ponts " par la construction de 300 à 400 logements, d'un établissement d'accueil pour personnes âgées ainsi que la réalisation de jardins familiaux et d'un parc urbain, qui répond à une finalité d'intérêt général, dans des conditions équivalentes sans recourir à l'expropriation ; qu'elle a pu, sans entacher son arrêt d'erreur de droit ni d'insuffisance de motivation, en déduire que l'opération d'expropriation projetée revêtait un caractère d'utilité publique alors même que les requérants auraient, lors de l'enquête publique, exprimé leur accord pour participer à l'aménagement de la ZAC en conservant la propriété de leur terrain ;<br/>
<br/>
              Sur l'arrêt attaqué en tant qu'il statue sur les conclusions dirigées contre l'arrêté de cessibilité :<br/>
<br/>
              6. Considérant que les requérants soutiennent que l'arrêt attaqué, qui a écarté une partie de leur argumentation par adoption des motifs des premiers juges, serait de ce fait entaché d'omission de réponse aux moyens qu'ils soulevaient pour la première fois en appel au soutien de leurs conclusions tendant à l'annulation de l'arrêté de cessibilité du 10 décembre 2007, tirés, d'une part, de ce que l'état parcellaire annexé à cet arrêté comprendrait des parcelles appartenant déjà à la collectivité expropriante, lesquelles ne pouvaient par suite légalement faire l'objet d'une procédure d'expropriation, d'autre part, de ce que ce même état parcellaire ne mentionnerait pas, à l'inverse, certaines des parcelles incluses dans le périmètre de l'opération déclarée d'utilité publique ; <br/>
<br/>
              7. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, dans son jugement du 18 mai 2010, le tribunal administratif de Strasbourg a jugé que si les requérants soutenaient que les parcelles constituant le chemin de la Corvée et la rue de la Folie, incluses dans le périmètre de la ZAC du Sansonnet, ne figuraient ni dans l'avis du commissaire enquêteur, ni dans l'état parcellaire annexé à l'arrêté de cessibilité, ils n'établissaient ni n'alléguaient que ces voies, qui constituent des emprises publiques, n'appartiendraient pas à la collectivité expropriante ni que, par suite, leur cession fût nécessaire ; que, cette réponse étant suffisante et n'appelant pas de nouvelles précisions en appel, la cour pouvait statuer sur ce point, sans entacher son arrêt d'insuffisance de motivation, par adoption des motifs des premiers juges ;<br/>
<br/>
              8. Considérant, en revanche, que la cour, qui n'a pas opposé d'irrecevabilité à ces conclusions, a ainsi qu'il est soutenu omis de répondre à l'argumentation soulevée pour la première fois devant elle par les requérants dans leur mémoire enregistré le 27 mars 2014, tirée de ce que certains immeubles mentionnés dans l'annexe de l'arrêté de cessibilité appartiendraient déjà à l'expropriant, qui les aurait précédemment acquis par voie amiable ; que son arrêt est, dans cette mesure, entaché d'irrégularité ; <br/>
<br/>
              9. Considérant qu'il résulte de ce tout ce qui précède que les consorts A...sont seulement fondés à demander l'annulation de l'arrêt attaqué en tant que celui-ci, statuant sur leurs conclusions tendant à l'annulation de l'arrêté du préfet de la Moselle du 10 décembre 2007 déclarant cessibles les terrains nécessaires en vue de la réalisation de la zone d'aménagement concerté du Sansonnet à Metz-devant-les-Ponts, concerne la cessibilité des immeubles dont il est soutenu qu'ils seraient déjà la propriété de l'expropriant ;<br/>
<br/>
              10. Considérant qu'aux termes du second alinéa de l'article L. 821-2 du code de justice administrative : " Lorsque l'affaire fait l'objet d'un second pourvoi en cassation, le Conseil d'Etat statue définitivement sur cette affaire " ; qu'il y a lieu, par suite, de régler l'affaire au fond dans la mesure indiquée au point 9 ;<br/>
<br/>
              11. Considérant qu'il ressort des écritures des requérants que ceux-ci ne font état d'aucune circonstance particulière de nature à leur conférer un intérêt leur donnant qualité pour demander l'annulation de l'arrêté du préfet de la Moselle du 10 décembre 2007 déclarant cessibles les terrains nécessaires en vue de la réalisation de la ZAC du Sansonnet, en tant qu'il concerne des terrains autres que ceux leur appartenant, alors, au surplus, qu'ils soutiennent que ces terrains auraient déjà été cédés à l'expropriant ; que leurs conclusions sont, dans cette mesure, irrecevables et ne peuvent qu'être rejetées ;<br/>
<br/>
              12. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de l'établissement public foncier de Lorraine (EPFL), qui n'est pas dans la présente espèce la partie perdante ; que, dans les circonstances de l'espèce, il n'y a pas lieu de mettre à la charge des consorts A...la somme que l'EPFL demande au titre de ces dispositions.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nancy du 21 avril 2015 est annulé en tant que, statuant sur les conclusions dirigées contre l'arrêté du préfet de Moselle du 10 décembre 2007, il concerne la cessibilité des immeubles dont il est soutenu qu'ils seraient déjà la propriété de l'établissement public foncier de Lorraine.<br/>
<br/>
Article 2 : Les conclusions présentées par les consorts A...devant la cour administrative d'appel de Nancy, relatives à l'arrêté du préfet de Moselle du 10 décembre 2007 en tant qu'il déclare cessibles des immeubles dont il est soutenu qu'ils seraient déjà la propriété de l'établissement public foncier de Lorraine, sont rejetées.<br/>
<br/>
Article 3 : Le surplus des conclusions des consorts A...est rejeté.<br/>
<br/>
Article 4 : Les conclusions présentées par l'établissement public foncier de Lorraine au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 5 : La présente décision sera notifiée à M. C...A..., M. B...A..., à l'établissement public foncier de Lorraine et au ministre de l'intérieur.<br/>
Copie en sera adressée à la ministre du logement et de l'habitat durable.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">34-02-03 EXPROPRIATION POUR CAUSE D'UTILITÉ PUBLIQUE. RÈGLES GÉNÉRALES DE LA PROCÉDURE NORMALE. ARRÊTÉ DE CESSIBILITÉ. - DIVISIBILITÉ - EXISTENCE - CONSÉQUENCE - INTÉRÊT POUR AGIR LIMITÉ, EN PRINCIPE, AUX PARCELLES DONT LE REQUÉRANT EST PROPRIÉTAIRE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-07-01-03-02-01 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. QUESTIONS GÉNÉRALES. CONCLUSIONS. CONCLUSIONS IRRECEVABLES. ACTES INDIVISIBLES. - ABSENCE - ARRÊTÉ DE CESSIBILITÉ - CONSÉQUENCE - INTÉRÊT POUR AGIR LIMITÉ, EN PRINCIPE, AUX PARCELLES DONT LE REQUÉRANT EST PROPRIÉTAIRE [RJ1].
</SCT>
<ANA ID="9A"> 34-02-03 En l'absence de circonstances particulières dont il ferait état, un requérant ne justifie pas d'un intérêt lui donnant qualité à demander l'annulation d'un arrêté de cessibilité en tant qu'il concerne des terrains autres que ceux lui appartenant.</ANA>
<ANA ID="9B"> 54-07-01-03-02-01 En l'absence de circonstances particulières dont il ferait état, un requérant ne justifie pas d'un intérêt lui donnant qualité à demander l'annulation d'un arrêté de cessibilité en tant qu'il concerne des terrains autres que ceux lui appartenant.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 22 juillet 1994, Mme,, n° 89570, aux Tables sur un autre point.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
