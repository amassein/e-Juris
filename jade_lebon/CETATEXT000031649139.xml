<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031649139</ID>
<ANCIEN_ID>JG_L_2015_12_000000381254</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/64/91/CETATEXT000031649139.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème / 9ème SSR, 18/12/2015, 381254</TITRE>
<DATE_DEC>2015-12-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>381254</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème / 9ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>CORLAY</AVOCATS>
<RAPPORTEUR>M. Jacques Reiller</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:381254.20151218</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 13 juin 2014 et 1er juin 2015 au secrétariat du contentieux du Conseil d'Etat, la SARL Aderanet, la SAS Arcandis, la SAS Abiova, la SAS Normbau France et la SASU Zalix Biométrie demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler, pour excès de pouvoir, la décision implicite de rejet née le 14 avril 2014 du silence gardé par la Commission nationale de l'informatique et des libertés en réponse à leur demande tendant à l'abrogation de la délibération n° 2012-322 du 20 septembre 2012 portant autorisation unique de mise en oeuvre de traitements reposant sur la reconnaissance du contour de la main et ayant pour finalités le contrôle d'accès ainsi que la restauration sur les lieux de travail (décision d'autorisation unique n° AU-007), ainsi que de la décision de la Commission du 27 mai 2014 rejetant explicitement cette demande ;<br/>
<br/>
              2°) d'enjoindre à la Commission nationale de l'informatique et des libertés d'abroger cette délibération, dans un délai de deux mois à compter de la décision à intervenir, sous astreinte d'au moins 150 euros par jour de retard ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement à chacune des requérantes d'une somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu :<br/>
              - la loi n° 78-17 du 6 janvier 1978 ;<br/>
              - le décret n° 2005-1309 du 20 octobre 2005 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jacques Reiller, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Corlay, avocat de la SARL Aderanet, la SAS Arcandis, la SAS Abiova, la SAS Normbau France et la SASU Zalix Biométrie ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier que, le 27 avril 2006, la Commission nationale de l'informatique et des libertés a adopté, sur le fondement du II de l'article 25 de la loi du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés, aux termes duquel : " (...) les traitements qui répondent à une même finalité, portent sur des catégories de données identiques et ont les mêmes destinataires ou catégories de destinataires peuvent être autorisés par une décision unique de la commission. Dans ce cas, le responsable de chaque traitement adresse à la commission un engagement de conformité de celui-ci à la description figurant dans l'autorisation ", la délibération n° 2006-101 portant autorisation unique de mise en oeuvre de dispositifs biométriques reposant sur la reconnaissance du contour de la main et ayant pour finalité le contrôle d'accès ainsi que la gestion des horaires et de la restauration sur les lieux de travail ; que cette délibération a été remplacée par la délibération n° 2012-322 du 20 septembre 2012, qui a exclu du champ de l'autorisation unique les traitements reposant sur un dispositif biométrique de reconnaissance de contour de la main ayant pour finalité le contrôle des horaires ; qu'après avoir demandé à la Commission l'abrogation de cette dernière délibération, la SARL Aderanet et les autres sociétés requérantes demandent au Conseil d'Etat l'annulation, pour excès de pouvoir, du refus opposé à cette demande, par une décision implicite du 14 avril 2014, puis par une décision expresse du 27 mai 2014 ; <br/>
<br/>
              2. Considérant que les sociétés requérantes soutiennent que le refus d'abroger la délibération du 20 septembre 2012 est illégal en raison des illégalités qui entachent cette délibération ; <br/>
<br/>
              3. Considérant, en premier lieu, que contrairement à ce qui est soutenu, la Commission nationale de l'informatique et des libertés n'a pas méconnu l'étendue de ses compétences en prenant la délibération litigieuse ; qu'en effet, elle s'est bornée à réduire le champ de l'autorisation unique qu'elle avait adoptée en 2006 sur le fondement du II de l'article 25 de la loi du 6 février 1978, en en excluant les dispositifs biométriques reposant sur la reconnaissance du contour de la main ayant pour finalité le contrôle des horaires ; que la délibération litigieuse n'a pas pour objet et ne saurait avoir pour effet d'interdire tout traitement automatisé de données à caractère personnel reposant sur un tel dispositif et poursuivant une telle finalité ; qu'elle a seulement pour conséquence que les responsables de tels traitements doivent désormais déposer auprès de la Commission une demande d'autorisation spécifique, en application du 8° de l'article 25 et de l'article 30 de la  loi du 6 janvier 1978 ; qu'en outre, la délibération a laissé aux responsables de traitements qui avaient effectué un engagement de conformité à l'autorisation unique avant le 12 octobre 2012, date de publication au Journal officiel de la délibération du 20 septembre 2012, et qui ne respectent plus les conditions fixées par cette dernière, un délai de cinq ans pour s'y conformer ; <br/>
<br/>
              4. Considérant, en deuxième lieu, que les sociétés requérantes ne sauraient utilement invoquer la méconnaissance de l'article 24 de la loi du 6 janvier 1978, aux termes duquel : " Pour les catégories les plus courantes de traitements de données à caractère personnel, dont la mise en oeuvre n'est pas susceptible de porter atteinte à la vie privée ou aux libertés, la Commission nationale de l'informatique et des libertés établit et publie, après avoir reçu le cas échéant les propositions formulées par les représentants des organismes publics et privés représentatifs, des normes destinées à simplifier l'obligation de déclaration " ; qu'en effet, cet article ne s'applique qu'aux traitements relevant du régime de la déclaration ; <br/>
<br/>
              5. Considérant qu'aux termes de l'article 6 de la loi du 6 janvier 1978 : " Un traitement ne peut porter que sur des données à caractère personnel qui satisfont aux conditions suivantes : /1° Les données sont collectées et traitées de manière loyale et licite ; /2° Elles sont collectées pour des finalités déterminées, explicites et légitimes et ne sont pas traitées ultérieurement de manière incompatible avec ces finalités. Toutefois, un traitement ultérieur de données à des fins statistiques ou à des fins de recherche scientifique ou historique est considéré comme compatible avec les finalités initiales de la collecte des données, s'il est réalisé dans le respect des principes et des procédures prévus au présent chapitre, au chapitre IV et à la section 1 du chapitre V ainsi qu'aux chapitres IX et X et s'il n'est pas utilisé pour prendre des décisions à l'égard des personnes concernées ; /3° Elles sont adéquates, pertinentes et non excessives au regard des finalités pour lesquelles elles sont collectées et de leurs traitements ultérieurs (...) " ; que, contrairement à ce qui est soutenu, la Commission nationale de l'informatique et des libertés n'a fait une inexacte application de ces dispositions ni en prenant la délibération du 20 septembre 2012 ni en refusant de l'abroger ; que, d'une part, ainsi que l'impliquent les dispositions précitées, la Commission a apprécié la proportionnalité des données collectées, qui sont en l'espèce de nature biométrique, à la finalité poursuivie par le traitement ; que, d'autre part, elle a pu légalement estimer que la mise en oeuvre des traitements reposant sur la reconnaissance du contour de la main et ayant pour seule finalité le contrôle de la gestion et du respect des horaires devait faire l'objet d'une autorisation spécifique, eu égard à la nature des données collectées, afin de lui permettre d'exercer, au cas par cas, le contrôle prévu par les dispositions précitées ;<br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède que, sans qu'il soit besoin de statuer sur les fins de non-recevoir opposées par la Commission nationale de l'informatique et des libertés, la requête de la SARL Aderanet et autres doit être rejetée, y compris ses conclusions aux fins d'injonction ainsi que celles présentées au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la SARL Aderanet et autres est rejetée.<br/>
Article 2 : La présente décision sera notifiée à la SARL Aderanet, à la SAS Arcandis, à la SAS Abiova, à la SAS Normbau France, à la SASU Zalix Biométrie et à la Commission nationale de l'informatique et des libertés.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">26-07-06 DROITS CIVILS ET INDIVIDUELS. - TRAITEMENTS AUTORISÉS PAR UNE DÉCISION UNIQUE (II DE L'ART. 25 DE LA LOI DU 6 JANVIER 1978) - SOUSTRACTION D'UN TRAITEMENT DU RÉGIME DE L'AUTORISATION UNIQUE - 1) CRITÈRES - 2) CONTRÔLE DU JUGE DE L'EXCÈS DE POUVOIR - CONTRÔLE ENTIER.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-07-02-03 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. CONTRÔLE DU JUGE DE L'EXCÈS DE POUVOIR. APPRÉCIATIONS SOUMISES À UN CONTRÔLE NORMAL. - DÉLIBÉRATION PAR LAQUELLE LA CNIL SOUSTRAIT UN TRAITEMENT DU RÉGIME DE L'AUTORISATION UNIQUE (II DE L'ART. 25 DE LA LOI DU 6 JANVIER 1978).
</SCT>
<ANA ID="9A"> 26-07-06 1) Pour déterminer si des traitements doivent être soustraits du régime de la décision unique prévu par le II de l'article 25 de la loi n° 78-17 du 6 janvier 1978, il appartient à la Commission nationale de l'informatique et des libertés (CNIL) d'apprécier la proportionnalité des données collectées à la finalité poursuivie par ces traitements et de rechercher si la mise en oeuvre des traitements doit faire l'objet d'une autorisation spécifique, eu égard à la nature des données collectées, lui permettant d'exercer, au cas par cas, le contrôle prévu par l'article 6 de la loi.,,,2) Le juge de l'excès de pouvoir exerce un contrôle entier sur la délibération par laquelle la Commission nationale de l'informatique et des libertés (CNIL) exclut certains traitements du champ de l'autorisation unique prévue par le II de l'article 25 de la loi du 6 janvier 1978, ainsi que sur le refus d'abroger cette délibération.</ANA>
<ANA ID="9B"> 54-07-02-03 Le juge de l'excès de pouvoir exerce un contrôle entier sur la délibération par laquelle la Commission nationale de l'informatique et des libertés (CNIL) exclut certains traitements du champ de l'autorisation unique prévue par le II de l'article 25 de la loi du 6 janvier 1978, ainsi que sur le refus d'abroger cette délibération.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
