<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031289333</ID>
<ANCIEN_ID>JG_L_2015_10_000000376466</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/28/93/CETATEXT000031289333.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème / 5ème SSR, 07/10/2015, 376466</TITRE>
<DATE_DEC>2015-10-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>376466</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème / 5ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROUSSEAU, TAPIE ; SCP CAPRON</AVOCATS>
<RAPPORTEUR>M. Benjamin de Maillard</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Maud Vialettes</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:376466.20151007</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La directrice départementale de la cohésion sociale et de la protection des populations de la Haute-Saône a porté plainte contre la SELARL Le loup blanc et M. B...A...devant la chambre de discipline du conseil régional de l'ordre des vétérinaires de Lorraine. Par une décision du 23 novembre 2012, la chambre de discipline du conseil de l'ordre des vétérinaires de Picardie, à qui le jugement de la plainte avait été attribué par la chambre supérieure de discipline du Conseil supérieur de l'ordre des vétérinaires, a infligé à la SELARL Le loup blanc et à M. A...la sanction d'interdiction temporaire d'exercer la profession vétérinaire pour une durée de trois mois sur l'ensemble du territoire national.<br/>
<br/>
              Par une décision du 20 janvier 2014, la chambre supérieure de discipline du Conseil supérieur de l'ordre des vétérinaires a, sur appel de la SELARL Le loup blanc et de M. A..., annulé cette décision en tant qu'elle prononçait une sanction à l'encontre de M. A... et rejeté l'appel en tant qu'il émanait de la SELARL Le loup blanc.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et de nouveaux mémoires, enregistrés les 18 mars, 18 juin et 5 août 2014 ainsi que les 27 janvier et 3 juin 2015 au secrétariat du contentieux du Conseil d'Etat, la SELARL Le loup blanc et M. A...demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette décision en tant qu'elle a infligé une sanction à la SELARL Le loup blanc ;<br/>
<br/>
              2°) de mettre à la charge du conseil régional de l'ordre des vétérinaires de Lorraine et du Conseil supérieur de l'ordre des vétérinaires la somme de 4 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administratif.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - la directive n° 2006/123/CE du 12 décembre 2006 ;<br/>
              - le code de la santé publique ;<br/>
              - le code rural et de la pêche maritime ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Benjamin de Maillard, auditeur, <br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Capron, avocat de M. A...et de la SELARL Le loup Blanc et à la SCP Rousseau, Tapie, avocat du Conseil supérieur de l'ordre des vétérinaires ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes des dispositions de l'article L. 242-1 du code rural et de la pêche maritime, dans sa rédaction alors en vigueur : " (...) II. - Les ordres régionaux sont institués dans chacune des circonscriptions régionales déterminées par un arrêté du ministre chargé de l'agriculture. Ils sont formés de tous les vétérinaires en exercice remplissant les conditions prévues à l'article L. 241-1 ainsi que des sociétés mentionnées au I de l'article L. 241-17. / Les membres des conseils régionaux de l'ordre sont élus par les vétérinaires mentionnés à l'article L. 241-1 et inscrits au tableau de l'ordre défini à l'article L. 242-4./ (...) Seuls les vétérinaires mentionnés à l'article L. 241-1 établis ou exerçant à titre principal en France sont électeurs et éligibles " ; qu'aux termes de l'article L. 242-4 du même code : " Le conseil régional de l'ordre tient à jour, chaque année et pour chaque département compris dans son ressort, le tableau des vétérinaires qui remplissent les conditions fixées à l'article L. 241-1 (...)./ L'inscription au tableau de l'ordre doit être demandée par les intéressés, agissant à titre personnel ou en qualité de membres d'une société, au conseil de l'ordre de la région dans laquelle ils se proposent d'exercer leur profession. La demande doit être accompagnée du diplôme, titre ou certificat permettant l'exercice de la profession vétérinaire ainsi que, le cas échéant, des statuts (...) " ; que, selon les dispositions de l'article L. 242-5, les membres de chaque chambre régionale de discipline sont choisis, à l'exception de son président, parmi les membres élus au conseil régional de l'ordre ; qu'enfin, aux termes de l'article R. 242-91 du même code : " Le vétérinaire qui cesse définitivement d'exercer sa profession sur le territoire national demande au conseil régional de l'ordre au tableau duquel il est inscrit de procéder à sa radiation (...) " ; <br/>
<br/>
              2. Considérant qu'il résulte de ces dispositions, dans leur rédaction alors applicable, que la qualité de membre d'un conseil régional de l'ordre des vétérinaires et, par suite, la qualité de membre d'une chambre régionale de discipline sont subordonnées à l'inscription au tableau de l'ordre des vétérinaires ; qu'un vétérinaire qui cesse d'exercer définitivement sa profession et doit alors être radié du tableau de l'ordre à sa demande ne peut plus siéger comme membre d'une chambre régionale de discipline à compter de la date de cette radiation ;<br/>
<br/>
              3. Considérant que, pour écarter le moyen tiré de ce que la chambre régionale de discipline était irrégulièrement composée du fait qu'un des vétérinaires qui y siégeait avait définitivement cessé d'exercer sa profession, la chambre supérieure de discipline a retenu que cette circonstance était sans influence sur la régularité de la composition de la formation disciplinaire ; qu'il résulte de ce qui a été dit ci-dessus qu'en statuant ainsi, sans rechercher si la cessation définitive de sa profession par l'intéressé n'avait pas entraîné, comme elle le devait, sa radiation du tableau de l'ordre, la chambre supérieure de discipline a commis une erreur de droit ; que, par suite et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, sa décision doit être annulée en tant qu'elle inflige une sanction à la SELARL Le loup blanc ;<br/>
<br/>
              4. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par les requérants au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : La décision de la chambre supérieure de discipline du Conseil supérieur de l'ordre des vétérinaires du 20 janvier 2014 est annulée en tant qu'elle inflige une sanction à la SELARL Le loup blanc.<br/>
Article 2 : L'affaire est renvoyée dans la limite de la cassation ainsi prononcée à la chambre supérieure de discipline du Conseil supérieur de l'ordre des vétérinaires.<br/>
Article 3 : Le surplus des conclusions du pourvoi de la SELARL Le loup blanc et de M. A... est rejeté. <br/>
Article 4 : La présente décision sera notifiée à la SELARL Le loup blanc, à M. B...A...et au Conseil supérieur de l'ordre des vétérinaires.<br/>
Copie en sera adressée au conseil régional de l'ordre des vétérinaires de Lorraine.<br/>
<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">55-01-02-025 PROFESSIONS, CHARGES ET OFFICES. ORDRES PROFESSIONNELS - ORGANISATION ET ATTRIBUTIONS NON DISCIPLINAIRES. QUESTIONS PROPRES À CHAQUE ORDRE PROFESSIONNEL. ORDRE DES VÉTÉRINAIRES. - JURIDICTIONS DISCIPLINAIRES DES VÉTÉRINAIRES - COMPOSITION - VÉTÉRINAIRES CESSANT DÉFINITIVEMENT D'EXERCER LEUR PROFESSION - FIN DE LEUR MISSION JURIDICTIONNELLE À COMPTER DE LEUR RADIATION DU TABLEAU DE L'ORDRE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">55-04-01-02 PROFESSIONS, CHARGES ET OFFICES. DISCIPLINE PROFESSIONNELLE. PROCÉDURE DEVANT LES JURIDICTIONS ORDINALES. JUGEMENTS. - COMPOSITION DES JURIDICTIONS DISCIPLINAIRES DES VÉTÉRINAIRES - VÉTÉRINAIRES CESSANT DÉFINITIVEMENT D'EXERCER LEUR PROFESSION - FIN DE LEUR MISSION JURIDICTIONNELLE À COMPTER DE LEUR RADIATION DU TABLEAU DE L'ORDRE.
</SCT>
<ANA ID="9A"> 55-01-02-025 La qualité de membre d'un conseil régional de l'ordre des vétérinaires et, par suite, la qualité de membre d'une chambre régionale de discipline sont subordonnées à l'inscription au tableau de l'ordre des vétérinaires. Un vétérinaire qui cesse d'exercer définitivement sa profession et doit alors être radié du tableau de l'ordre à sa demande, en vertu de l'article R. 242-91 du code rural et de la pêche maritime, ne peut plus siéger comme membre d'une chambre régionale de discipline à compter de la date de cette radiation. Erreur de droit à déduire l'irrégularité de la composition de la chambre régionale de discipline du seul fait qu'un des vétérinaires y siégeant a définitivement cessé son activité sans rechercher s'il a effectivement été radié du tableau de l'ordre.</ANA>
<ANA ID="9B"> 55-04-01-02 La qualité de membre d'un conseil régional de l'ordre des vétérinaires et, par suite, la qualité de membre d'une chambre régionale de discipline sont subordonnées à l'inscription au tableau de l'ordre des vétérinaires. Un vétérinaire qui cesse d'exercer définitivement sa profession et doit alors être radié du tableau de l'ordre à sa demande, en vertu de l'article R. 242-91 du code rural et de la pêche maritime, ne peut plus siéger comme membre d'une chambre régionale de discipline à compter de la date de cette radiation. Erreur de droit à déduire l'irrégularité de la composition de la chambre régionale de discipline du seul fait qu'un des vétérinaires y siégeant a définitivement cessé son activité sans rechercher s'il a effectivement été radié du tableau de l'ordre.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
