<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031938395</ID>
<ANCIEN_ID>JG_L_2016_01_000000388146</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/93/83/CETATEXT000031938395.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème SSR, 25/01/2016, 388146</TITRE>
<DATE_DEC>2016-01-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>388146</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DE CHAISEMARTIN, COURJON</AVOCATS>
<RAPPORTEUR>M. Vincent Montrieux</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2016:388146.20160125</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B...C..., de nationalité, algérienne, a demandé au tribunal administratif de Besançon d'annuler l'arrêté du préfet du Doubs du 23 mai 2013 lui refusant un certificat de résidence, lui faisant obligation de quitter le territoire français et fixant le pays de destination. Par un jugement n° 1300812 du 26 septembre 2013, le tribunal administratif de Besançon a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 14NC00649 du 16 octobre 2014, la cour administrative d'appel de Nancy a rejeté l'appel formé par Mme C...contre ce jugement. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire, et un mémoire en réplique, enregistrés les 19 février, 21 avril  et 2 octobre 2015 au secrétariat du contentieux du Conseil d'Etat, Mme C...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - la charte des droits fondamentaux de l'Union européenne ;<br/>
              - l'accord franco-algérien du 27 décembre 1968 modifié ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Vincent Montrieux, maître des requêtes en service extraordinaire,<br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP de Chaisemartin, Courjon, avocat de Mme C...;<br/>
<br/>
<br/>
<br/>1. Considérant, en premier lieu, qu'aux termes des stipulations de l'article 4 de l'accord franco-algérien du 27 décembre 1968 : " Les membres de la famille qui s'établissent en France sont mis en possession d'un certificat de résidence de même durée de validité que celui de la personne qu'ils rejoignent. sans préjudice des dispositions de l'article 9, l'admission sur le territoire français en vue de l'établissement des membres de famille d'un ressortissant algérien titulaire d'un certificat de résidence d'une durée de validité d'au moins un an, présent en France depuis au moins un an sauf cas de force majeure, et l'octroi du certificat de résidence sont subordonnés à la délivrance de l'autorisation de regroupement familial par l'autorité française compétente (....) " ; qu'aux termes du titre II du protocole annexé au même accord : " Les membres de la famille s'entendent du conjoint d'un ressortissant algérien (...) " ; qu'aux termes de l'article 7 du même accord : " Les dispositions du présent article et celles de l'article 7 bis fixent les conditions de délivrance du certificat de résidence aux ressortissants algériens autres que ceux visés à l'article 6 nouveau. (...) d) Les ressortissants algériens autorisés à séjourner en France au titre du regroupement familial, s'ils rejoignent un ressortissant algérien lui-même titulaire d'un certificat de résidence d'un an, reçoivent de plein droit un certificat de résidence de même durée de validité, renouvelable et portant la mention " vie privée et familiale de l'article 7 bis " ; qu'aux termes de l'article 7 bis du même accord : " Le certificat de résidence valable dix ans est délivré de plein droit sous réserve de la régularité du séjour pour ce qui concerne les catégories visées au a), au b), au c) et au g) : (...) d) Aux membres de la famille d'un ressortissant algérien titulaire d'un certificat de résidence valable dix ans qui sont autorisés à résider en France au titre du regroupement familial. " ; que ces stipulations régissent d'une manière complète les conditions dans lesquelles les ressortissants algériens peuvent être admis à séjourner en France et y exercer une activité professionnelle, les règles relatives à la nature des titres de séjour qui peuvent leur être délivrés, ainsi que les conditions dans lesquelles leurs conjoints et leurs enfants mineurs peuvent s'installer en France ; <br/>
<br/>
              2. Considérant que le regroupement familial, lorsqu'il est autorisé au profit du conjoint d'un ressortissant algérien résidant en France, a pour objet de rendre possible la vie commune des époux, ainsi qu'il résulte notamment des stipulations précitées de l'article 4 de l'accord franco-algérien ; que, par suite, en jugeant, après avoir relevé qu'il ressortait des pièces du dossier qui lui était soumis et qu'il n'était pas contesté que M. et Mme C...étaient séparés depuis le 22 mars 2013, soit depuis une date antérieure à la décision attaquée, que le préfet du Doubs n'avait pas entaché sa décision d'erreur de droit en rejetant la demande de certificat de résidence présentée par Mme A...épouse C...pour ce motif, la cour administrative d'appel, qui a suffisamment motivé son arrêt pour écarter le moyen tiré de ce que le préfet aurait été tenu de délivrer le titre sollicité, n'a pas commis d'erreur de droit ;<br/>
<br/>
              3. Considérant, en deuxième lieu, qu'en estimant que les documents produits par la requérante, postérieurs à la décision attaquée, ne permettaient pas d'établir la réalité des violences conjugales alléguées, la cour administrative d'appel a porté sur les faits qui lui étaient soumis  une appréciation souveraine exempte de dénaturation ; que, pour juger que la décision attaquée n'était entachée d'aucune erreur manifeste d'appréciation, elle a suffisamment répondu à l'argumentation de la requérante sur ce point ; <br/>
<br/>
              4. Considérant, en troisième lieu, que le moyen tiré de ce que la décision litigieuse aurait été prise en méconnaissance des stipulations de l'article 41 de la charte des droits fondamentaux de l'Union européenne est nouveau en cassation et ne peut dès lors qu'être écarté ;<br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que Mme C...n'est pas fondée à demander l'annulation de l'arrêt attaqué ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de Mme C...est rejeté.<br/>
Article 2 : La présente décision sera notifiée à Mme B...C...et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">335-01-01-02 ÉTRANGERS. SÉJOUR DES ÉTRANGERS. TEXTES APPLICABLES. CONVENTIONS INTERNATIONALES. - ACCORD FRANCO-ALGÉRIEN DU 27 DÉCEMBRE 1968 - CERTIFICAT DE RÉSIDENCE DÉLIVRÉ AU PROFIT DU CONJOINT D'UN RESSORTISSANT ALGÉRIEN AU TITRE DU REGROUPEMENT FAMILIAL - SÉPARATION POSTÉRIEURE À L'AUTORISATION DE REGROUPEMENT FAMILIAL - LÉGALITÉ DU REFUS D'OCTROI DU CERTIFICAT FONDÉ SUR L'ABSENCE DE VIE COMMUNE DES ÉPOUX - EXISTENCE [RJ1].
</SCT>
<ANA ID="9A"> 335-01-01-02 Le regroupement familial, lorsqu'il est autorisé au profit du conjoint d'un ressortissant algérien résidant en France, a pour objet de rendre possible la vie commune des époux, ainsi qu'il résulte notamment des stipulations de l'article 4 de l'accord franco-algérien. Par suite, le préfet peut rejeter la demande de certificat de résidence lorsque le demandeur est séparé de son conjoint depuis une date antérieure à la décision relative à la demande de certificat de résidence présenté par l'intéressé.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Comp., s'agissant du régime issu de l'ordonnance n° 45-2658 du 2 novembre 1945 dans sa rédaction issue de la loi n° 93 -1027 du 24 août 1993, CE, Avis, 16 juin 1995,,i, p. 249 ; et dans sa rédaction antérieure, CE, 30 juillet 1997, Ministre de l'intérieur,, n° 143045, T. p. 854-857.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
