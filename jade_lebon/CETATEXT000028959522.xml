<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028959522</ID>
<ANCIEN_ID>JG_L_2007_01_000000300428</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/95/95/CETATEXT000028959522.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 11/01/2007, 300428</TITRE>
<DATE_DEC>2007-01-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>300428</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2007:300428.20070111</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 8 janvier 2007 au secrétariat du contentieux du Conseil d'Etat, présentée pour Mme B...A..., demeurant...; Mme A...demande au juge des référés du Conseil d'Etat :<br/>
<br/>
              1°) de suspendre, sur le fondement de l'article L. 521-2 du code de justice administrative, la recommandation du Conseil supérieur de l'audiovisuel en date du 7 novembre 2006, ensemble son annexe intitulée " guide d'application de la recommandation relative à la campagne en vue de l'élection présidentielle de 2007 " ;<br/>
<br/>
              2°) d'enjoindre au Conseil supérieur de l'audiovisuel d'édicter, dans un délai de 48 heures à compter de la notification de l'ordonnance, une recommandation conforme au principe du caractère pluraliste de l'expression des courants de pensée et d'opinion ;<br/>
<br/>
<br/>
<br/>
              elle soutient que la condition d'urgence est remplie dès lors que la période dite " préliminaire " figurant dans la recommandation dont la suspension est demandée dure jusqu'à la fin du mois de février ; que la recommandation porte une atteinte grave à la liberté fondamentale que constitue le caractère pluraliste de l'expression des courants de pensée et d'opinion ; que l'atteinte ainsi portée à une liberté fondamentale est manifestement illégale en ce qu'en fixant le début de la période dite " préliminaire " à la date du 1er décembre 2006, le Conseil supérieur de l'audiovisuel a méconnu les dispositions combinées de l'article 3-III de la loi du 6 novembre 1962 relative à l'élection du Président de la République au suffrage universel et de l'article L. 52-4 du code électoral, qui imposaient de faire débuter la période à la date du 1er avril 2006, retenue par le législateur pour le calcul des dépenses engagées en vue de l'élection présidentielle ; qu'il sera nécessaire que le Conseil supérieur de l'audiovisuel enjoigne à tous les éditeurs de services de radio et de télévision de respecter l'équité en fixant un temps d'antenne minimal pour tous les candidats qui n'ont jamais été candidats à l'élection présidentielle ou qui n'appartiennent pas à une formation ayant présenté un candidat à l'élection présidentielle, puis en allouant à tous les autres candidats, en sus de ce temps minimal, un temps d'antenne calculé en fonction de leur résultat ou de celui de leur formation politique lors du scrutin présidentiel de 2002 ;<br/>
<br/>
              Vu la recommandation du Conseil supérieur de l'audiovisuel en date du 7 novembre 2006 ;<br/>
<br/>
<br/>
              Vu le mémoire en défense, enregistré le 10 janvier 2007, présenté par le Conseil supérieur de l'audiovisuel, qui conclut au rejet de la requête ; il soutient que la condition d'urgence n'est pas remplie, la requérante n'ayant introduit son recours que six semaines après l'entrée en vigueur de la recommandation contestée ; que l'illégalité grave et manifeste n'est pas caractérisée dès lors que la requérante n'a pas saisi le Conseil supérieur de l'audiovisuel depuis le 1er avril 2006 ; que par ailleurs le décret du 8 mars 2001 prévoit que la campagne électorale ne commence qu'à compter du deuxième lundi précédant le premier tour de l'élection présidentielle et aucune disposition n'impose au Conseil supérieur de l'audiovisuel, en dehors de cette période, d'adopter un dispositif spécifique ; qu'il est impossible d'avancer cette date eu égard aux difficultés rencontrées par les services audiovisuels pour identifier les candidats présumés ou déclarés ; que s'agissant du principe d'équité, aucune disposition n'impose une égalité de traitement entre les candidats en dehors de la campagne officielle ; qu'un tel principe d'équité s'inscrit dans une tradition républicaine et permet, tout en conciliant les principes à valeur constitutionnelle de pluralisme politique et de liberté de communication, de laisser aux rédactions une certaine souplesse ; que les conclusions aux fins d'injonction sont irrecevables car le Conseil supérieur de l'audiovisuel n'a pas le pouvoir d'imposer aux chaînes un temps d'antenne minimum pour les candidats ;<br/>
<br/>
              Vu, enregistrée le 11 janvier 2007, l'intervention présentée par l'association International alliance for right and transparency, dont le siège est 14 rue Braunkt à Strasbourg (67000), l'association pour une vraie démocratie de proximité, dont le siège est 21 rue des anciens combattants à Metz (57000) et la ligue des honnêtes gens, dont le siège est 1 route nationale à Longeville-les-Saint-Avold (57740) ; elles demandent que le juge des référés du Conseil d'Etat fasse droit aux conclusions de la requête n° 300428 et que l'affaire soit renvoyée pour cause de suspicion légitime, le Conseil d'Etat n'étant pas un tribunal indépendant et impartial ; que la recommandation du 7 novembre 2006 est contraire à l'article 21 de la déclaration universelle des droits de l'homme ;<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu la déclaration universelle des droits de l'homme du 10 décembre 1948 ;<br/>
<br/>
              Vu le code électoral ;<br/>
<br/>
              Vu la loi n°62-1292 du 6 novembre 1962 modifiée relative à l'élection du Président de la République au suffrage universel ;<br/>
<br/>
              Vu la loi n°86-1067 du 30 septembre 1986 modifiée relative à la liberté de communication, notamment ses articles 1er, 3-1, 13 et 16 ;<br/>
<br/>
              Vu le décret n°2001-213 du 8 mars 2001 modifié portant application de la loi n°62-1292 du 6 novembre 1962 relative à l'élection du Président de la République au suffrage universel ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part Mme B...A...et d'autre part, le Conseil supérieur de l'audiovisuel ;<br/>
<br/>
<br/>
              Vu le procès verbal de l'audience publique du jeudi 11 janvier 2007 à 10 heures au cours de laquelle ont été entendus :<br/>
<br/>
              - Me Nicolaÿ, avocat au Conseil d'Etat et à la Cour de cassation, avocat de Mme B...A... ;<br/>
              - Mme B...A... ;<br/>
              - les représentants du Conseil supérieur de l'audiovisuel ;<br/>
<br/>
<br/>
<br/>
              Sur l'intervention de l'association International alliance for right and transparency, de l'association pour une vraie démocratie de proximité et de la ligue des honnêtes gens :<br/>
<br/>
              Considérant qu'en l'état de l'instruction, il y a lieu d'admettre que l'association International alliance for right and transparency, l'association pour une vraie démocratie de proximité et la ligue des honnêtes gens ont intérêt à la suspension de la recommandation contestée ; que, par suite, leur intervention est recevable ; qu'en revanche, l'auteur d'une intervention, qui, en vertu de l'article R. 632-1 du code de justice administrative, ne peut retarder le jugement d'une affaire, ne saurait demander le renvoi de cette affaire pour cause de suspicion légitime, cette procédure n'étant au demeurant pas applicable aux affaires portées devant le Conseil d'Etat, qui n'a pas de juridiction supérieure à laquelle l'affaire pourrait être renvoyée ;<br/>
<br/>
              Sur la recommandation du Conseil supérieur de l'audiovisuel en date du 7 novembre 2006 :<br/>
<br/>
              Considérant qu'aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé  chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale..." ;<br/>
<br/>
              Considérant que, par sa recommandation du 7 novembre 2006 à l'ensemble des services de télévision et de radio en vue de l'élection présidentielle, le Conseil supérieur de l'audiovisuel a distingué, pour le traitement de l'actualité électorale, d'une part, une période préliminaire, courant du 1er décembre 2006 jusqu'à la veille de la publication de la liste des candidats établie par le Conseil constitutionnel, d'autre part, une période intermédiaire, courant du jour de la publication de la liste des candidats établie par le Conseil constitutionnel jusqu'à la veille de l'ouverture de la campagne, soit le dimanche 8 avril 2007, et enfin la période de campagne, courant du lundi 9 avril 2007 jusqu'au second tour de scrutin, le dimanche 6 mai 2007 ; que selon cette recommandation, les services de radio et de télévision doivent appliquer, pendant la période préliminaire, un principe d'équité pour le temps de parole et le temps d'antenne des candidats déclarés ou présumés, pendant la période intermédiaire, un principe d'égalité pour le temps de parole des candidats figurant sur la liste établie par le Conseil constitutionnel et un principe d'équité pour leur temps d'antenne, et pendant la période de campagne, un principe d'égalité pour le temps de parole et le temps d'antenne des candidats ;<br/>
<br/>
              Considérant, en premier lieu, que le premier alinéa de l'article 10 du décret du 8 mars 2001 portant application de la loi du 6 novembre 1962 relative à l'élection du Président de la République au suffrage universel fixe au deuxième lundi précédant le premier tour de scrutin l'ouverture de la campagne en vue de l'élection du Président de la République ; qu'aux termes de l'article 15 de ce décret : " A compter de la date de début de la campagne mentionnée à l'article 10 et jusqu'au tour de scrutin où l'élection est acquise, le principe d'égalité entre les candidats doit être respecté dans les programmes d'information des sociétés nationales de programme et des services de communication audiovisuelle autorisés ou concédés en ce qui concerne la reproduction ou les commentaires des déclarations et écrits des candidats et la présentation de leur personne... / Le Conseil supérieur de l'audiovisuel veille au respect des dispositions du présent article et des règles et recommandations qu'il édicte en application de l'article 16 de la loi du 30 septembre 1986 susvisée " ; qu'en vertu du second alinéa de l'article 16 de la loi du 30 septembre 1986 relative à la liberté de communication, le Conseil supérieur de l'audiovisuel adresse, pour la durée des campagnes électorales, des recommandations aux éditeurs des services de radio et de télévision autorisés ayant conclu une convention en vertu de cette loi ; qu'aux termes de l'article 1er de la loi du 30 septembre 1986 : " La communication au public par voie électronique est libre. / L'exercice de cette liberté ne peut être limité que dans la mesure requise, d'une part, par le respect de la dignité de la personne humaine, de la liberté et de la propriété d'autrui, du caractère pluraliste de l'expression des courants de pensée et d'opinion... " ; que l'article 3-1 de la même loi permet au Conseil supérieur de l'audiovisuel d'adresser aux éditeurs et distributeurs de services de radio et de télévision des recommandations relatives au respect des principes énoncés dans cette loi ; <br/>
<br/>
              Considérant qu'il résulte de ces dispositions que la période de campagne régie par les dispositions de l'article 15 du décret du 8 mars 2001 est celle qui est définie par l'article 10 du même décret ; que, contrairement à ce que soutient MmeA..., la durée des campagnes électorales visée à l'article 16 de la loi du 30 septembre 1986 ne se réfère pas à la période de prise en compte des dépenses électorales fixée par l'article L. 52-4 du code électoral, rendu applicable à l'élection du Président de la République par le II de l'article  3 de la loi du 6 novembre 1962 ; qu'ainsi le Conseil supérieur de l'audiovisuel n'était pas tenu de fixer le point de départ de la période préliminaire au 1er avril 2006 ; que le conseil, auquel il appartient, en vertu des articles 1er, 3 et 13 de la loi du 30 septembre 1986, de veiller à ce que les services de radio et de télévision respectent le principe d'équité de traitement entre candidats pendant la période précédant la campagne électorale officielle, n'a pas entaché sa recommandation d'une illégalité manifeste en fixant au 1er décembre 2006 le point de départ de la période préliminaire ; que par suite, si le principe du caractère pluraliste de l'expression des courants de pensée et d'opinion est une liberté fondamentale, la recommandation contestée n'est pas entachée, à son égard, d'une illégalité manifeste ;<br/>
<br/>
              Considérant, en deuxième lieu, qu'au soutien de sa demande d'injonction relative au respect de l'équité, Mme A...a contesté au cours de l'audience publique la légalité de la notion d'équité telle que définie par la recommandation attaquée ; que la recommandation du 7 novembre 2006 définit le principe d'équité entre les candidats, devant être respecté dans le traitement de l'actualité électorale, en fixant deux critères tirés respectivement de la représentativité des candidats et de leur capacité à manifester concrètement l'intention affirmée d'être candidat ; que, selon cette recommandation, la représentativité peut être évaluée en prenant en compte, en particulier, les résultats obtenus par le candidat ou la formation politique qui le soutient aux plus récentes élections ; qu'en retenant ces critères, et à supposer même que cette définition, qui suppose une marge d'appréciation, permette de tenir compte de la notoriété des candidats ou de l'écho recueilli par leur candidature, le Conseil supérieur de l'audiovisuel n'a pas entaché sa recommandation d'une illégalité manifeste ;<br/>
<br/>
              Considérant, en troisième lieu, que  les auteurs de l'intervention ne peuvent utilement se prévaloir  de la déclaration universelle des droits de l'homme, qui ne figure pas au nombre des textes diplomatiques ratifiés par la France dans les conditions fixées à l'article 55 de la Constitution ; <br/>
<br/>
              Considérant qu'il résulte de ce qui précède que la requête présentée par Mme A...doit être rejetée ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : L'intervention de l'association International alliance for right and transparency, de l'association pour une vraie démocratie et de la ligue des honnêtes gens est admise.<br/>
<br/>
Article 2 : Les conclusions présentées par ces associations aux fins de renvoi pour cause de suspicion légitime sont rejetées.<br/>
<br/>
Article 3  : La requête de Mme A...est rejetée.<br/>
<br/>
Article 4 : La présente ordonnance sera notifiée à Mme B...A..., au Conseil supérieur de l'audiovisuel, à l'association International alliance for right and transparency, à l'association pour une vraie démocratie de proximité et à la ligue des honnêtes gens..<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-035-03-03-01-01 PROCÉDURE. PROCÉDURES INSTITUÉES PAR LA LOI DU 30 JUIN 2000. RÉFÉRÉ TENDANT AU PRONONCÉ DE MESURES NÉCESSAIRES À LA SAUVEGARDE D'UNE LIBERTÉ FONDAMENTALE (ART. L. 521-2 DU CODE DE JUSTICE ADMINISTRATIVE). CONDITIONS D'OCTROI DE LA MESURE DEMANDÉE. ATTEINTE GRAVE ET MANIFESTEMENT ILLÉGALE À UNE LIBERTÉ FONDAMENTALE. LIBERTÉ FONDAMENTALE. - CARACTÈRE PLURALISTE DE L'EXPRESSION DES COURANTS DE PENSÉE ET D'OPINION - ATTEINTE GRAVE ET MANIFESTEMENT ILLÉGALE - ABSENCE - RECOMMANDATION DU CONSEIL SUPÉRIEUR DE L'AUDIOVISUEL FIXANT AU 1ER DÉCEMBRE 2006 LE POINT DE DÉPART DE LA PÉRIODE PRÉLIMINAIRE À LA CAMPAGNE POUR L'ÉLECTION PRÉSIDENTIELLE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">56-02-03 RADIO ET TÉLÉVISION. RÈGLES GÉNÉRALES. CAMPAGNES ET PROPAGANDE ÉLECTORALES. - ELECTION PRÉSIDENTIELLE - FIXATION PAR LE CONSEIL SUPÉRIEUR DE L'AUDIOVISUEL DU POINT DE DÉPART DE LA PÉRIODE PRÉLIMINAIRE - CONDITIONS.
</SCT>
<ANA ID="9A"> 54-035-03-03-01-01 Le Conseil supérieur de l'audiovisuel, auquel il appartient, en vertu des articles 1er, 3 et 13 de la loi du 30 septembre 1986, de veiller à ce que les services de radio et de télévision respectent le principe d'équité de traitement entre candidats pendant la période précédant la campagne électorale officielle, n'était pas tenu de fixer le point de départ de la période préliminaire au 1er avril 2006 dès lors que la durée des campagnes électorales visée à l'article 16 de la loi du 30 septembre 1986 ne se réfère pas à la période de prise en compte des dépenses électorales fixée par l'article L. 52-4 du code électoral, rendu applicable à l'élection du Président de la République par le II de l'article 3 de la loi du 6 novembre 1962. Par ailleurs, il n'a pas entaché sa recommandation d'une illégalité manifeste en fixant au 1er décembre 2006 le point de départ de la période préliminaire. Par suite, si le principe du caractère pluraliste de l'expression des courants de pensée et d'opinion est une liberté fondamentale, la recommandation contestée n'est pas entachée, à son égard, d'une illégalité manifeste.</ANA>
<ANA ID="9B"> 56-02-03 Le Conseil supérieur de l'audiovisuel, auquel il appartient, en vertu des articles 1er, 3 et 13 de la loi du 30 septembre 1986, de veiller à ce que les services de radio et de télévision respectent le principe d'équité de traitement entre candidats pendant la période précédant la campagne électorale officielle, n'était pas tenu de fixer le point de départ de la période préliminaire au 1er avril 2006 dès lors que la durée des campagnes électorales visée à l'article 16 de la loi du 30 septembre 1986 ne se réfère pas à la période de prise en compte des dépenses électorales fixée par l'article L. 52-4 du code électoral, rendu applicable à l'élection du Président de la République par le II de l'article 3 de la loi du 6 novembre 1962. Par ailleurs, il n'a pas entaché sa recommandation d'une illégalité manifeste en fixant au 1er décembre 2006 le point de départ de la période préliminaire.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
