<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000022330444</ID>
<ANCIEN_ID>JG_L_2010_05_000000330310</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/22/33/04/CETATEXT000022330444.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème et 7ème sous-sections réunies, 19/05/2010, 330310, Publié au recueil Lebon</TITRE>
<DATE_DEC>2010-05-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>330310</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème et 7ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Arrighi de Casanova</PRESIDENT>
<AVOCATS>SCP DELVOLVE, DELVOLVE</AVOCATS>
<RAPPORTEUR>Mme Maud  Vialettes</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Lenica Frédéric</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2010:330310.20100519</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le mémoire, enregistré le 1er mars 2010 au secrétariat du contentieux du Conseil d'Etat, présenté pour la COMMUNE DE BUC (78530) en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958 ; la COMMUNE DE BUC demande au Conseil d'Etat, à l'appui de sa requête tendant à l'annulation du décret n° 2009-615 du 3 juin 2009 fixant la liste des routes à grande circulation, en tant qu'il inclut dans cette liste la partie de la route départementale 938 (RD 938) entre Versailles et Toussus-le-Noble (Yvelines), de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution de l'article L. 110-3 du code de la route, dans sa rédaction issue de l'article 22 de la loi n° 2004-809 du 13 août 2004 ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution, notamment ses articles 61-1 et 62 ;<br/>
<br/>
              Vu l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
<br/>
              Vu le code de la route, notamment son article L. 110-3 dans sa rédaction issue de l'article 22 de la loi n° 2004-809 du 13 août 2004 ;<br/>
<br/>
              Vu la décision du Conseil constitutionnel n° 2004-503 DC du 12 août 2004 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Maud Vialettes, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Delvolvé, Delvolvé, avocat de la COMMUNE DE BUC, <br/>
<br/>
              - les conclusions de M. Frédéric Lenica, rapporteur public,<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Delvolvé, Delvolvé, avocat de la COMMUNE DE BUC ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant qu'aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...) " ; qu'il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et qu'elle soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              Considérant que, pour demander au Conseil d'Etat de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution de la seconde phrase du premier alinéa de l'article L. 110-3 du code de la route, dans sa rédaction issue de l'article 22 de la loi du 13 août 2004 relative aux libertés et responsabilités locales, la COMMUNE DE BUC soutient qu'en ce qu'elle prévoit que la liste des routes à grande circulation est fixée après consultation des collectivités et groupements propriétaires des voies, et non de toutes les collectivités que ces voies traversent, cette disposition porte atteinte à la libre administration des collectivités territoriales garantie par l'article 72 de la Constitution ;<br/>
<br/>
              Considérant, toutefois, que par la décision n° 2004-503-DC du 12 août 2004, le Conseil constitutionnel a, dans ses motifs et son dispositif, déclaré l'article 22 de la loi du 13 août 2004, dont est issu l'article L. 110-3 du code de la route, conforme à la Constitution ; qu'aucun changement de circonstances survenu depuis cette décision n'est de nature à justifier que la conformité de cette disposition à la Constitution soit à nouveau examinée par le Conseil constitutionnel ; qu'ainsi, et alors même que cette décision ne s'est pas expressément prononcée sur le moyen tiré de l'article 72 de la Constitution, ce moyen doit être écarté, sans qu'il soit besoin de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par la COMMUNE DE BUC.<br/>
Article 2 : La présente décision sera notifiée à la COMMUNE DE BUC, au Premier ministre et au ministre d'Etat, ministre de l'écologie, de l'énergie, du développement durable et de la mer.<br/>
Copie en sera adressée au Conseil constitutionnel.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-10-05-02-03 PROCÉDURE. - DISPOSITION DÉJÀ DÉCLARÉE CONFORME À LA CONSTITUTION - CIRCONSTANCE QUE LE CONSEIL CONSTITUTIONNEL N'AIT PAS EXPRESSÉMENT STATUÉ SUR LA CONFORMITÉ DE LA DISPOSITION À LA NORME CONSTITUTIONNELLE PRÉCISÉMENT INVOQUÉE PAR LE REQUÉRANT - CIRCONSTANCE NE CONDUISANT PAS AU RENVOI DE LA QUESTION, EN L'ABSENCE DE CHANGEMENT DE CIRCONSTANCES.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">71-01 VOIRIE. COMPOSITION ET CONSISTANCE. - LISTE DES ROUTES À GRANDE CIRCULATION - ELABORATION (ART. L. 110-3 DU CODE DE LA ROUTE, ISSU DE L'ARTICLE 22 DE LA LOI DU 13 AOÛT 2004) - QUESTION PRIORITAIRE DE CONSTITUTIONNALITÉ - PRINCIPE DE LIBRE ADMINISTRATION DES COLLECTIVITÉS TERRITORIALES - DISPOSITION LÉGISLATIVE DÉJÀ DÉCLARÉE CONFORME À LA CONSTITUTION - CONSÉQUENCE - ABSENCE DE RENVOI, EN L'ABSENCE DE CHANGEMENT DE CIRCONSTANCES.
</SCT>
<ANA ID="9A"> 54-10-05-02-03 Question de la conformité à l'article 72 de la Constitution de la seconde phrase du premier alinéa de l'article L. 110-3 du code de la route, dans sa rédaction issue de l'article 22 de la loi n° 2004-809 du 13 août 2004. Le Conseil constitutionnel a, par la décision n° 2004-503 DC du 12 août 2004, déclaré, dans ses motifs et son dispositif, l'article 22 conforme à la Constitution, mais sans statuer expressément sur la conformité de l'article à la norme constitutionnelle invoquée. Malgré cette dernière circonstance, la question n'est pas renvoyée au Conseil constitutionnel, aucun changement de circonstances survenu depuis cette décision n'étant de nature à justifier que la conformité de l'article 22 à la Constitution soit à nouveau examinée par le juge constitutionnel.</ANA>
<ANA ID="9B"> 71-01 Question de la conformité à l'article 72 de la Constitution de la seconde phrase du premier alinéa de l'article L. 110-3 du code de la route, dans sa rédaction issue de l'article 22 de la loi n° 2004-809 du 13 août 2004, aux termes de laquelle la liste des routes à grande circulation est fixée après consultation des collectivités et groupements propriétaires des voies. Requérant soutenant que le fait que la consultation ne soit pas ouverte à toutes les collectivités que ces voies traversent porte atteinte à la libre administration des collectivités territoriales garantie par l'article 72 de la Constitution. Mais le Conseil constitutionnel a, par la décision n° 2004-503 DC du 12 août 2004, déclaré, dans ses motifs et son dispositif, l'article 22 conforme à la Constitution, sans, cependant, statuer expressément sur la question de sa conformité à la norme constitutionnelle invoquée. Malgré cette dernière circonstance, la question n'est pas renvoyée au Conseil constitutionnel, aucun changement de circonstances survenu depuis cette décision n'étant de nature à justifier que la conformité de l'article 22 à la Constitution soit à nouveau examinée par le juge constitutionnel.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
