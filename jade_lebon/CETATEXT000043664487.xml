<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043664487</ID>
<ANCIEN_ID>JG_L_2021_06_000000439453</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/66/44/CETATEXT000043664487.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 4ème chambres réunies, 14/06/2021, 439453</TITRE>
<DATE_DEC>2021-06-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>439453</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BAUER-VIOLAS, FESCHOTTE-DESBOIS, SEBAGH ; SCP BUK LAMENT - ROBILLOT</AVOCATS>
<RAPPORTEUR>Mme Bénédicte Fauvarque-Cosson</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Villette</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:439453.20210614</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société civile immobilière des Sables a demandé au tribunal administratif de Nantes d'annuler pour excès de pouvoir la délibération du 11 mars 2016 par laquelle le conseil municipal de Pornic a approuvé la modification n° 1 de son plan local d'urbanisme, ainsi que la décision de rejet implicite de son recours gracieux. Par un jugement n° 1607523 du 26 juin 2018, le tribunal a rejeté cette demande.<br/>
<br/>
              Par un arrêt n° 18NT03209 du 10 janvier 2020, la cour administrative d'appel de Nantes a rejeté l'appel formé par la société des Sables contre ce jugement. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 10 mars et 18 août 2020 et le 29 avril 2021 au secrétariat du contentieux du Conseil d'Etat, la société des Sables demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
              3°) de mettre à la charge de la commune de Pornic la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme A... B..., conseillère d'Etat,  <br/>
<br/>
              - les conclusions de M. Vincent Villette, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Bauer-Violas, Feschotte-Desbois, Sebagh, avocat de la société des Sables et à la SCP Buk Lament - Robillot, avocat de la commune de Pornic ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que le conseil municipal de Pornic a approuvé, par une délibération du 11 mars 2016, la modification n° 1 du règlement de son plan local d'urbanisme. Elle a introduit, à l'article régissant le secteur Ub1, au sein de la zone urbaine U de la commune, l'interdiction, d'une part, de " toute construction à l'intérieur des cônes de vues figurant au plan de zonage " et, d'autre part, de " toute construction à l'intérieur des zones non aedificandi figurant au plan de zonage ". La parcelle cadastrée section BL n° 349 ainsi qu'une partie de la parcelle cadastrée section BL n° 347 que la société des Sables possède en front de mer sont devenues inconstructibles en raison de l'identification au plan de zonage d'un " cône de vue " dont l'objet est de préserver, depuis une rue perpendiculaire au rivage, une perspective sur le littoral, tandis qu'une grande partie de la parcelle section BL n° 349 a été intégrée dans une " zone non aedificandi ". La société requérante se pourvoit en cassation contre l'arrêt du 10 janvier 2020 par lequel la cour administrative d'appel de Nantes a confirmé le rejet par le tribunal administratif de Nantes, par un jugement du 26 juin 2018, de sa demande d'annulation de cette modification du règlement du plan local d'urbanisme de Pornic ainsi que du rejet de son recours gracieux. <br/>
<br/>
              2. Aux termes de l'article L. 151-19 du code de l'urbanisme : " Le règlement peut identifier et localiser les éléments de paysage et délimiter les quartiers, îlots, immeubles, espaces publics, monuments, sites et secteurs à protéger, à mettre en valeur ou à requalifier pour des motifs d'ordre culturel, historique ou architectural et définir, le cas échéant, les prescriptions de nature à assurer leur préservation ". Aux termes du premier alinéa de l'article L. 151-23 du même code : " Le règlement peut identifier et localiser les éléments de paysage et délimiter les sites et secteurs à protéger pour des motifs d'ordre écologique, notamment pour la préservation, le maintien ou la remise en état des continuités écologiques et définir, le cas échéant, les prescriptions de nature à assurer leur préservation (...) ".<br/>
<br/>
              3. L'un et l'autre de ces articles, issus de l'ancien article L. 123-1-5 du code de l'urbanisme, permettent au règlement d'un plan local d'urbanisme d'édicter des dispositions visant à protéger, mettre en valeur ou requalifier un élément du paysage dont l'intérêt le justifie. Le règlement peut notamment, à cette fin, instituer un cône de vue ou identifier un secteur en raison de ses caractéristiques particulières. La localisation de ce cône de vue ou de ce secteur, sa délimitation et les prescriptions le cas échéant définies, qui ne sauraient avoir de portée au-delà du territoire couvert par le plan, doivent être proportionnées et ne peuvent excéder ce qui est nécessaire à l'objectif recherché. Une interdiction de toute construction ne peut être imposée que s'il s'agit du seul moyen permettant d'atteindre l'objectif poursuivi.<br/>
<br/>
              4. Par suite, en jugeant que la commune de Pornic avait pu, dans le règlement de son plan local d'urbanisme, établir, d'une part, un cône de vue excluant toute construction et, d'autre part, une " zone non aedificandi ", qui interdit par nature toute construction, sans rechercher si ces interdictions, qui dérogent à la vocation d'une zone urbaine, constituaient, eu égard à l'ensemble des dispositifs existants, le seul moyen d'atteindre les objectifs recherchés, tels que relevés par les juges du fond, de valorisation des perspectives sur le littoral et de préservation de la frange littorale d'une urbanisation excessive, la cour a commis une erreur de droit. <br/>
<br/>
              5. Il résulte de ce qui précède, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que la société des Sables est fondée à demander l'annulation de l'arrêt qu'elle attaque.<br/>
<br/>
              6. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de la société des Sables, qui n'est pas la partie perdante dans la présente instance. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de Pornic une somme de 3 000 euros, à verser à cette société au titre des mêmes dispositions.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 10 janvier 2020 de la cour administrative d'appel de Nantes est annulé. <br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nantes.<br/>
Article 3 : La commune de Pornic versera à la société des Sables une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 4 : Les conclusions présentées par la commune de Pornic au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à la société civile immobilière des Sables et à la commune de Pornic.<br/>
Copie en sera adressée à la ministre de la cohésion des territoires et des relations avec les collectivités territoriales.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">68-01-01-01-03-01 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PLANS D'AMÉNAGEMENT ET D'URBANISME. PLANS D`OCCUPATION DES SOLS (POS) ET PLANS LOCAUX D'URBANISME (PLU). LÉGALITÉ DES PLANS. LÉGALITÉ INTERNE. PRESCRIPTIONS POUVANT LÉGALEMENT FIGURER DANS UN POS OU UN PLU. - PROTECTION DU PAYSAGE (ART. L. 151-19 ET L. 151-23 DU CODE DE L'URBANISME) - INSTITUTION D'UN CÔNE DE VUE OU D'UN SECTEUR ASSORTIS DE PRESCRIPTIONS, Y COMPRIS L'INCONSTRUCTIBILITÉ - LÉGALITÉ - CONDITION - PROPORTIONNALITÉ À L'OBJECTIF RECHERCHÉ.
</SCT>
<ANA ID="9A"> 68-01-01-01-03-01 Les articles L. 151-19 et L. 151-23 du code de l'urbanisme, issus de l'ancien article L. 123-1-5 de ce code, permettent l'un et l'autre au règlement d'un plan local d'urbanisme (PLU) d'édicter des dispositions visant à protéger, mettre en valeur ou requalifier un élément du paysage dont l'intérêt le justifie. Le règlement peut notamment, à cette fin, instituer un cône de vue ou identifier un secteur en raison de ses caractéristiques particulières.... ,,La localisation de ce cône de vue ou de ce secteur, sa délimitation et les prescriptions le cas échéant définies, qui ne sauraient avoir de portée au-delà du territoire couvert par le plan, doivent être proportionnées et ne peuvent excéder ce qui est nécessaire à l'objectif recherché. Une interdiction de toute construction ne peut être imposée que s'il s'agit du seul moyen permettant d'atteindre l'objectif poursuivi.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
