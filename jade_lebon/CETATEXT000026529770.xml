<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026529770</ID>
<ANCIEN_ID>JG_L_2012_10_000000325256</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/52/97/CETATEXT000026529770.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème et 10ème sous-sections réunies, 22/10/2012, 325256</TITRE>
<DATE_DEC>2012-10-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>325256</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème et 10ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LESOURD ; SCP DIDIER, PINET</AVOCATS>
<RAPPORTEUR>M. Matthieu Schlesinger</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Aladjidi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:325256.20121022</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 16 février et 15 mai 2009 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. Robert B, demeurant ... ; M. B demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement n°s 061142/066406 du 16 décembre 2008 par lequel le tribunal administratif de Montpellier a rejeté ses demandes tendant, d'une part, à la décharge de l'obligation de payer la somme de 3 455,06 euros correspondant au montant de quatre titres exécutoires émis à son encontre les 11 septembre 2002, 12 septembre 2003, 13 septembre 2004 et 15 septembre 2005 par le président de l'association syndicale autorisée du canal d'irrigation de Gignac au titre des taxes syndicales des années 2002 à 2005 et, d'autre part, à la décharge de l'obligation de payer la somme de 1 015,08 euros correspondant au montant du titre exécutoire émis à son encontre le 15 septembre 2006 par le président de l'association syndicale autorisée du canal d'irrigation de Gignac au titre des taxes syndicales de l'année 2006 ;<br/>
<br/>
              2°) de mettre à la charge de l'association syndicale autorisée du canal d'irrigation de Gignac le versement de la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi du 21 juin 1865 ;<br/>
<br/>
              Vu l'ordonnance n° 2004-632 du 1er juillet 2004 ;<br/>
<br/>
              Vu le décret du 18 décembre 1927 ;<br/>
<br/>
              Vu le décret n° 2006-504 du 3 mai 2006 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Matthieu Schlesinger, Auditeur,  <br/>
<br/>
              - les observations de la SCP Didier, Pinet, avocat de M. B et de la SCP Lesourd, avocat de l'association syndicale autorisée du canal d'irrigation de Gignac,<br/>
<br/>
              - les conclusions de M. Frédéric Aladjidi, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Didier, Pinet, avocat de M. B et à la SCP Lesourd, avocat de l'association syndicale autorisée du canal d'irrigation de Gignac ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. B est propriétaire de huit parcelles situées sur la commune de Gignac (Hérault) à l'intérieur du périmètre de l'association syndicale autorisée du canal d'irrigation de Gignac ; que, par titres exécutoires, des taxes syndicales ont été mises à sa charge au titre des années 2002 à 2006 ; que M. B se pourvoit en cassation contre le jugement du 16 décembre 2008 du tribunal administratif de Montpellier qui a rejeté sa demande de décharge de ces taxes ;<br/>
<br/>
              2. Considérant que, pour demander la décharge des taxes en litige, M. B soutenait notamment, devant le tribunal administratif de Montpellier, que l'article 25 du cahier des charges d'exploitation du canal d'irrigation de Gignac prévoyait la possibilité d'obtenir, dans certaines hypothèses, une réduction de ces taxes ; que le tribunal ne s'est pas prononcé sur ce moyen, qui n'était pas inopérant ; que, par suite, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, son jugement doit être annulé ;<br/>
<br/>
              3. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              4. Considérant, en premier lieu, qu'aux termes de l'article 5 du cahier des charges d'exploitation du canal de Gignac : " Le syndicat concessionnaire est tenu d'exécuter et d'entretenir à ses frais, risques et périls, tous les travaux destinés à amener et à distribuer les eaux en tête de chaque propriété à desservir. (...) " ; qu'aux termes de l'article 25 de ce cahier des charges : " L'insuffisance temporaire des eaux et la suspension temporaire du service, due à des accidents ou à la force majeure, seront constatées par l'administration et, dans  ce cas, elles ne donneront lieu à aucune réduction de redevance, à moins que l'insuffisance des eaux ou l'interruption du service dure plus de six mois. / Dans ce cas, si la quantité d'eau était diminuée de plus de moitié, il y aurait lieu à un dégrèvement de redevance proportionnel à la diminution de jouissance. / Il n'y aurait pas lieu à un dégrèvement si, en temps d'étiage, le volume d'eau était inférieur à celui qui est spécifié à l'article 3. Les quantités attribuées aux usagers se trouveraient alors réduites dans la même proportion. / La mise en chômage pour travaux de curage et d'entretien ne pourra non plus donner droit à aucune réduction. Les époques en seront fixées par le Préfet sur les propositions du syndicat. / En dehors des cas d'accidents, de force majeure, d'insuffisance de débit en étiage et de chômage, soumis aux conditions ci-dessus, les usagers pourront prétendre à dégrèvement de redevance, et le montant en sera réglé par les tribunaux compétents. " ;<br/>
<br/>
              5. Considérant que les taxes syndicales prélevées par les associations syndicales autorisées ont pour objet d'assurer la répartition entre les propriétaires, membres de l'association, des dépenses, essentiellement constituées par des frais de réalisation de travaux ou d'ouvrages et d'entretien de ceux-ci, qu'elles assument conformément à leur mission, de telle sorte que chaque propriété soit imposée en raison de l'intérêt qu'elle a à l'exécution de ces dépenses ; que, par suite, si le défaut d'accomplissement par une association syndicale de ses missions peut être de nature à entraîner la décharge de taxes syndicales, la circonstance qu'une telle association n'accomplirait qu'incomplètement ses missions ou les accomplirait de manière défectueuse, ne saurait, en principe, conduire à accorder la décharge des taxes syndicales réclamées à un membre de l'association ; que, toutefois, les membres d'une association syndicale autorisée peuvent se prévaloir, à l'appui d'une demande de décharge des taxes syndicales mises à leur charge, des dispositions d'un cahier des charges de l'association, lesquelles sont approuvées par l'autorité administrative et présentent un caractère réglementaire, lorsqu'elles prévoient des modalités spécifiques de dégrèvement dans des hypothèses où les missions de l'association sont accomplies de façon incomplète ou défectueuse ;<br/>
<br/>
              6. Considérant que les dispositions de l'article 25 du cahier des charges de l'association syndicale autorisée du canal de Gignac instituent, dans certaines hypothèses, un droit spécifique à dégrèvement des taxes syndicales ; que, toutefois, à supposer même établie la circonstance que le débit en eau ait été faible, il ne résulte pas de l'instruction que l'association syndicale aurait accompli ses obligations, à l'égard du requérant, de façon incomplète ou défectueuse ;<br/>
<br/>
              7. Considérant, en deuxième lieu, que pour demander la décharge des taxes en litige, M. B se prévaut également de l'absence de point de raccordement direct au réseau d'irrigation sur la parcelle 1033, qui lui appartient ; que, toutefois, s'il résulte de l'article 22 du cahier des charges qu'en cas de vente partielle ou de partage de parcelles, l'obligation de paiement de la redevance pour usage des eaux n'est supportée que par le propriétaire de la parcelle sur laquelle la prise d'eau est établie, une telle obligation s'apprécie en tenant compte de ce qu'est la propriété de l'acquéreur et de la configuration des lieux ; qu'il résulte de l'instruction que, si un point de livraison a été précédemment implanté sur la parcelle 1033 et déplacé à la demande de l'ancien propriétaire avant l'acquisition de la parcelle par M. B, la propriété de celui-ci, qui est composée de huit parcelles contiguës, comporte deux points de livraison ; que, dès lors, eu égard à la configuration des lieux, qui n'imposait pas l'installation d'un point de livraison supplémentaire, il appartenait au requérant de réaliser les travaux de raccordement nécessaires sur sa propriété ; qu'en outre, il résulte de l'instruction, notamment du rapport d'expertise ordonnée le 10 mars 2006 par le président du tribunal administratif de Montpellier, que l'association syndicale autorisée du canal d'irrigation de Gignac a rempli ses obligations relatives à l'entretien du canal dérivé de l'Hérault et de ses divers branchements, canaux et rigoles ; que M. B n'est dès lors  pas fondé à demander une décharge sur ce point ;<br/>
<br/>
              8. Considérant, en troisième lieu, que, contrairement à ce que soutient le requérant, il résulte de l'instruction que la parcelle 489 fait partie de sa propriété et est approvisionnée en eau par le canal ; qu'il ne saurait donc obtenir décharge des taxes syndicales en tant qu'elles tiennent compte de la superficie de cette parcelle ;<br/>
<br/>
              9. Considérant qu'il résulte de tout ce qui précède, sans qu'il soit besoin de statuer sur la fin de non-recevoir opposée par l'association syndicale, que M. B n'est pas fondé à demander la décharge des taxes syndicales mises à sa charge au titre des années 2002 à 2006 ;<br/>
<br/>
              10. Considérant qu'il y a lieu de mettre les frais et honoraires de l'expert, soit la somme de 4 169,64 euros, à la charge de M. B ;<br/>
<br/>
              11. Considérant que les dispositions de l'article L 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de l'association syndicale qui n'est pas, dans la présente instance, la partie perdante ; qu'en revanche, il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de M. B la somme de 2 000 euros à verser à l'association syndicale au titre des frais exposés par elle devant le tribunal administratif de Montpellier et devant le Conseil d'Etat et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le jugement du 18 décembre 2008 du tribunal administratif de Montpellier est annulé.<br/>
Article 2 : Les demandes présentées par M. B devant le tribunal administratif de Montpellier sont rejetées.<br/>
Article 3 : Les frais d'expertise, soit la somme de 4 169,64 euros, sont mis à la charge de M. B.<br/>
Article 4 : Les conclusions de M. B présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : M. B versera la somme de 2 000 euros à l'association syndicale autorisée du canal d'irrigation de Gignac en application des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 6 : La présente décision sera notifiée à M. Robert B et à l'association syndicale autorisée du canal d'irrigation de Gignac.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">11-01 ASSOCIATIONS SYNDICALES. QUESTIONS COMMUNES. - DEMANDE DE DÉGRÈVEMENT DE TAXES SYNDICALES FONDÉE SUR UN ACCOMPLISSEMENT INCOMPLET OU DÉFECTUEUX DE SES TÂCHES PAR UNE ASA - PRINCIPE - ABSENCE - EXCEPTION - CAS OÙ LES MEMBRES DE L'ASA PEUVENT SE PRÉVALOIR DES DISPOSITIONS D'UN CAHIER DES CHARGES DE L'ASA.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">11-01-03 ASSOCIATIONS SYNDICALES. QUESTIONS COMMUNES. RESSOURCES. - DEMANDE DE DÉGRÈVEMENT DE TAXES SYNDICALES FONDÉE SUR UN ACCOMPLISSEMENT INCOMPLET OU DÉFECTUEUX DE SES TÂCHES PAR UNE ASA - PRINCIPE - ABSENCE - EXCEPTION - CAS OÙ LES MEMBRES DE L'ASA PEUVENT SE PRÉVALOIR DES DISPOSITIONS D'UN CAHIER DES CHARGES DE L'ASA.
</SCT>
<ANA ID="9A"> 11-01 Les taxes syndicales prélevées par les associations syndicales autorisées (ASA) ont pour objet d'assurer la répartition entre les propriétaires, membres de l'association, des dépenses, essentiellement constituées par des frais de réalisation de travaux ou d'ouvrages et d'entretien de ceux-ci, qu'elles assument conformément à leur mission, de telle sorte que chaque propriété soit imposée en raison de l'intérêt qu'elle a à l'exécution de ces dépenses. Par suite, si le défaut d'accomplissement par une association syndicale de ses missions peut être de nature à entraîner la décharge de taxes syndicales, la circonstance qu'une telle association n'accomplirait qu'incomplètement ses missions ou les accomplirait de manière défectueuse, ne saurait, en principe, conduire à accorder la décharge des taxes syndicales réclamées à un membre de l'association. Toutefois, les membres d'une ASA peuvent se prévaloir, à l'appui d'une demande de décharge des taxes syndicales mises à leur charge, des dispositions d'un cahier des charges de l'association, lesquelles sont approuvées par l'autorité administrative et présentent un caractère réglementaire, lorsqu'elles prévoient des modalités spécifiques de dégrèvement dans des hypothèses où les missions de l'association sont accomplies de façon incomplète ou défectueuse.</ANA>
<ANA ID="9B"> 11-01-03 Les taxes syndicales prélevées par les associations syndicales autorisées (ASA) ont pour objet d'assurer la répartition entre les propriétaires, membres de l'association, des dépenses, essentiellement constituées par des frais de réalisation de travaux ou d'ouvrages et d'entretien de ceux-ci, qu'elles assument conformément à leur mission, de telle sorte que chaque propriété soit imposée en raison de l'intérêt qu'elle a à l'exécution de ces dépenses. Par suite, si le défaut d'accomplissement par une association syndicale de ses missions peut être de nature à entraîner la décharge de taxes syndicales, la circonstance qu'une telle association n'accomplirait qu'incomplètement ses missions ou les accomplirait de manière défectueuse, ne saurait, en principe, conduire à accorder la décharge des taxes syndicales réclamées à un membre de l'association. Toutefois, les membres d'une ASA peuvent se prévaloir, à l'appui d'une demande de décharge des taxes syndicales mises à leur charge, des dispositions d'un cahier des charges de l'association, lesquelles sont approuvées par l'autorité administrative et présentent un caractère réglementaire, lorsqu'elles prévoient des modalités spécifiques de dégrèvement dans des hypothèses où les missions de l'association sont accomplies de façon incomplète ou défectueuse.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
