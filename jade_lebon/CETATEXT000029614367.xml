<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029614367</ID>
<ANCIEN_ID>JG_L_2014_10_000000361315</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/61/43/CETATEXT000029614367.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème / 1ère SSR, 17/10/2014, 361315</TITRE>
<DATE_DEC>2014-10-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>361315</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème / 1ère SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP COUTARD, MUNIER-APAIRE ; SCP DIDIER, PINET</AVOCATS>
<RAPPORTEUR>M. Samuel Gillis</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:361315.20141017</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu l'ordonnance n° 1100683 du 12 juillet 2012, enregistrée le 24 juillet 2012 au secrétariat du Conseil d'Etat, par laquelle le président du tribunal administratif de Caen a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, la requête présentée à ce tribunal par le Comité de réflexion d'information et de lutte anti-nucléaire (CRILAN), dont le siège est 10, route d'Etang-Val à Pieux (50340), représenté par son président ;<br/>
<br/>
              Vu la requête, enregistrée le 28 mars 2011 au greffe du tribunal administratif de Caen, présentée par le Comité de réflexion d'information et de lutte anti-nucléaire (CRILAN) ; le CRILAN demande l'annulation de l'arrêté du 15 septembre 2010 du ministre de l'écologie, de l'énergie, du développement durable et de la mer et du ministre de l'économie, de l'industrie et l'emploi, portant homologation de la décision n° 2010-DC-0188 de l'autorité de sûreté nucléaire (ASN) du 7 juillet 2010 fixant les limites de rejets dans l'environnement des effluents liquides et gazeux pour l'exploitation des réacteurs "Flamanville 1" (INB n°108), "Flamanville 2" (INB n°109) et "Flamanville 3" (INB n°167) de la société Electricité de France et à que soit mise à la charge de l'Etat une somme de 2 000 euros en application de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution, notamment son Préambule ;<br/>
<br/>
              Vu la convention pour la protection du milieu marin de l'Atlantique du Nord-Est signée à Paris le 22 septembre 1992, publiée par le décret n° 2000-830 du 24 août 2000 ;<br/>
              Vu le traité instituant la Communauté européenne de l'énergie atomique ; <br/>
              Vu le code de l'environnement ;<br/>
<br/>
              Vu la loi n° 2006-686 du 13 juin 2006 ;<br/>
<br/>
              Vu le décret n° 95-540 du 4 mai 1995 ;<br/>
<br/>
              Vu le décret n° 2007-1557 du 2 novembre 2007 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Samuel Gillis, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Didier, Pinet, avocat du Comité de réflexion d'information et de lutte anti-nucléaire (CRILAN) et à la SCP Coutard, Munier-Apaire, avocat de la société Electricité de France ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'en vertu des deux premiers alinéas de l'article 29 de la loi du 13 juin 2006 relative à la transparence et à la sécurité en matière nucléaire, alors en vigueur, la création d'une installation nucléaire de base (INB) est soumise à autorisation délivrée après avis de l'Autorité de sûreté nucléaire (ASN) ; qu'aux termes du troisième alinéa de ce même article 29 : " Pour l'application de l'autorisation, l'Autorité de sûreté nucléaire définit, dans le respect des règles générales prévues à l'article L. 593-4, les prescriptions relatives à la conception, à la construction et à l'exploitation de l'installation qu'elle estime nécessaires à la protection des intérêts mentionnés à l'article L. 593-1. / Elle précise notamment, s'il y a lieu, les prescriptions relatives aux prélèvements d'eau de l'installation et aux substances radioactives issues de l'installation. Les prescriptions fixant les limites de rejets de l'installation dans l'environnement sont soumises à l'homologation du ministre chargé de la sûreté nucléaire. " ; que, par une première décision n° 2010-DC-188 du 7 juillet 2010, l'ASN a fixé, à la suite d'une demande présentée le 15 novembre 2006 par la société Electricité de France (EDF), les limites des rejets dans l'environnement des effluents liquides et gazeux pour l'exploitation des réacteurs " Flamanville 1 " (INB n° 108), " Flamanville 2 " (INB n° 109) et " Flamanville 3 " (INB n° 167, futur réacteur dit " EPR ") ; que cette décision a été homologuée par un arrêté du 15 septembre 2010 du ministre de l'écologie, de l'énergie, du développement durable et de la mer et du ministre de l'économie, de l'industrie et de l'emploi ; que, par une autre décision               n°2010-DC-0189 du 7 juillet 2010, l'ASN a fixé les prescriptions relatives aux modalités de prélèvement et de consommation d'eau et de rejets dans l'environnement des effluents liquides et gazeux pour l'exploitation de ces réacteurs ; que le comité de réflexion, d'information et de lutte anti-nucléaire (CRILAN) demande au juge du plein contentieux des installations nucléaires de base l'annulation de l'arrêté d'homologation du 15 septembre 2010 précité ; <br/>
              Sur la compétence du Conseil d'Etat : <br/>
<br/>
              2. Considérant qu'aux termes du 4° de l'article R. 311-1 du code de justice administrative : " Le Conseil d'Etat est compétent pour connaître en premier et dernier ressort: / (...) 4° Des recours dirigés contre les décisions prises par les organes des autorités suivantes, au titre de leur mission de contrôle ou de régulation : / (...) - l'Autorité de sûreté nucléaire (...) " ; que ces dispositions doivent être regardées comme donnant compétence au Conseil d'Etat pour connaître en premier et dernier ressort des recours dirigés tant contre les décisions prises par l'ASN au titre de sa mission de contrôle et de régulation que contre celles par lesquelles les ministres homologuent ces décisions ;  <br/>
<br/>
              Sur la légalité de la décision attaquée :<br/>
<br/>
              En ce qui concerne le moyen tiré de la méconnaissance de l'article 37 du traité instituant la Communauté européenne de l'énergie atomique :<br/>
<br/>
              3. Considérant que le CRILAN soutient que l'obligation prévue par l'article 37 du traité instituant la Communauté européenne de l'énergie atomique pour chaque État membre de fournir pour avis à la Commission européenne les données générales des projets de rejets d'effluents radioactifs a été méconnue, dès lors qu'il ressortirait des courriers adressés par la société EDF à l'ASN les 9 avril et 23 juillet 2009 que le dossier de demande d'autorisation a été modifié postérieurement à l'avis qui a été rendu par la Commission ; qu'il résulte toutefois de l'instruction que ces courriers répondent, sur des points d'ailleurs secondaires, aux observations formulées par l'ASN dans le cadre de l'instruction du projet et concernent non les rejets d'effluents radioactifs des installations mais les rejets d'effluents chimiques de l'unité de dessalement d'eau de mer de l'EPR ; que, par suite, le requérant n'est en tout état de cause pas fondé à soutenir que la consultation de la Commission européenne aurait été irrégulière ;<br/>
<br/>
              En ce qui concerne le moyen tiré de l'irrégularité de la consultation de la commission locale d'information (CLI) de Flamanville :<br/>
<br/>
              4. Considérant qu'aux termes du II de l'article 70 du décret du 2 novembre 2007 relatif aux INB et au contrôle, en matière de sûreté nucléaire, du transport de matières radioactives : " Les demandes d'autorisation ou de modification déposées en application du décret du 4 mai 1995 avant la publication du présent décret continuent à être instruites selon les procédures fixées par ce décret du 4 mai 1995, l'Autorité de sûreté nucléaire étant substituée à la direction générale de la sûreté nucléaire et de la radioprotection pour l'application de l'article 6 de ce décret. Les décisions sur ces demandes sont prises par l'Autorité de sûreté nucléaire selon les modalités définies aux IV, V VI et VII de l'article 18 du présent décret " ; qu'aux termes du II de l'article 18 du même décret : " Lorsque les prescriptions envisagées sont relatives aux prélèvements d'eau, aux rejets d'effluents dans le milieu ambiant et à la prévention ou à la limitation des nuisances de l'installation pour le public et l'environnement, l'Autorité de sûreté nucléaire transmet le projet de prescriptions assorti d'un rapport de présentation au préfet mentionné au I de l'article 13 et à la commission locale d'information. / Le préfet soumet le projet de prescriptions et le rapport de présentation au conseil départemental de l'environnement et des risques sanitaires et technologiques mentionné à l'article R. 1416-16 du code de la santé publique. (...) Le conseil départemental dispose d'un délai de trois mois pour émettre son avis, qui est transmis par le préfet à l'Autorité de sûreté nucléaire. / Dans le même délai, la commission locale d'information peut adresser ses observations à l'Autorité de sûreté nucléaire. " ; que le V de l'article 18 du même décret dispose : " Lorsque les prescriptions fixent les limites applicables aux rejets d'effluents de l'installation dans le milieu ambiant, l'Autorité de sûreté nucléaire transmet aux ministres chargés de la sûreté nucléaire, pour homologation dans les conditions définies à l'article 3, sa décision accompagnée du rapport de présentation et des avis recueillis en application du II. " ; <br/>
<br/>
              5. Considérant qu'il résulte de ces dispositions que les décisions prises sur les demandes déposées, comme en l'espèce, en application du décret du 4 mai 1995 avant la publication du décret du 2 novembre 2007 sont instruites selon les procédures prévues par le décret du 4 mai 1995 ; que le II de l'article 70 du décret du 2 novembre 2007 précité ne renvoie pas aux dispositions du II de l'article 18, qui prévoient la consultation de la commission locale d'information mais seulement aux IV, V, VI et VII de cet article ; que, par suite, l'administration n'était pas tenue de saisir la commission du projet litigieux ; qu'au surplus le document du 6 janvier 2010, intitulé " rapport du groupe d'experts scientifiques " de la commission, ne constitue pas un avis qu'aurait rendu cette commission, sans incidence étant la circonstance qu'il a été visé par la décision de l'ASN ; que le moyen tiré de ce que l'arrêté attaqué serait entaché d'illégalité, faute d'une consultation régulière de cette commission, ne peut par suite être accueilli ;<br/>
<br/>
              En ce qui concerne les moyens tirés des insuffisances du dossier de demande et de l'étude d'impact :<br/>
<br/>
              6. Considérant qu'aux termes de l'article 8 du décret du 4 mai 1995 relatif aux rejets d'effluents liquides et gazeux et aux prélèvements d'eau des INB, la demande d'autorisation de rejets d'effluents radioactifs comprend : " (...) 3° (...) Dans le cas des opérations de rejet (...) pour chaque installation, les différents types d'effluents à traiter et leur origine respective, leur quantité, leur composition, tant radioactive que chimique, leurs caractéristiques physiques, le procédé de traitement utilisé, les conditions dans lesquelles seront opérés les rejets dans le milieu récepteur ainsi que la composition des effluents à rejeter ; / 4° Un document indiquant, compte tenu des variations saisonnières et climatiques, les incidences de l'opération sur la ressource en eau, le milieu aquatique, l'écoulement, le niveau et la qualité des eaux, y compris de ruissellement, ainsi que sur chacun des éléments mentionnés à l'article 2 de la loi du 3 janvier 1992 susvisée, en fonction des procédés mis en oeuvre, des modalités d'exécution des travaux ou de l'activité, du fonctionnement des ouvrages ou installations, de la nature, de l'origine et du volume des eaux utilisées ou concernées. / S'il y a lieu, ce document indique également, compte tenu des variations saisonnières et climatiques, les incidences de l'opération sur la qualité de l'air, les odeurs, la santé ou la sécurité publique, la production agricole, la conservation des constructions et monuments, ou sur le caractère des sites, et plus généralement sur toutes les composantes de l'environnement. Les incidences indirectes, telles que les retombées d'aérosols ou de poussières ou leurs dépôts doivent également être indiquées. / Les transferts de radionucléides par les différents vecteurs, notamment les chaînes alimentaires et les sédiments aquatiques, sont évalués. Sont évalués les transferts de radionucléides par les différents vecteurs, notamment les chaînes alimentaires et les sédiments aquatiques, et font l'objet d'une estimation les doses auxquelles la population est soumise au niveau du groupe de référence./(...) Si ces informations sont données dans une étude d'impact, celle-ci remplace le document exigé dans le présent 4° ; / 5° Les moyens de surveillance prévus et, si l'opération présente un danger, les moyens d'intervention en cas d'incident ou d'accident ; / (...) Les études et documents prévus au présent article portent sur l'ensemble des installations ou équipements exploités ou projetés par le demandeur qui, par leur proximité ou leur connexité avec l'installation soumise à autorisation, sont de nature à participer aux incidences sur les eaux, le milieu aquatique ou l'atmosphère. " ; <br/>
<br/>
              7. Considérant, en premier lieu, que l'association requérante ne peut utilement exciper, pour critiquer le contenu du dossier de demande, de ce que les moyens de surveillance prévus seraient insuffisants ; <br/>
<br/>
              8. Considérant, en deuxième lieu, qu'il résulte de l'instruction que, contrairement à ce qui est soutenu par le CRILAN, la demande déposée par la société  EDF indique les moyens d'intervention prévus en cas d'incident ou d'accident ; <br/>
<br/>
              9. Considérant, en troisième lieu, que l'association requérante soutient que le dossier de demande méconnaît les dispositions du dernier alinéa de l'article 8 du décret du 4 mai 1995, selon lesquelles les études et documents prévus au même article portent sur l'ensemble des installations ou équipements exploités ou projetés par le demandeur qui, par leur proximité ou leur connexité avec l'installation soumise à autorisation, sont de nature à participer aux incidences sur les eaux, le milieu aquatique ou l'atmosphère ; qu'il résulte toutefois de l'instruction que les installations nucléaires de base de Paluel, Penly et Gravelines se situent respectivement à environ 180, 220 et 320 kilomètres du site de Flamanville et ne sauraient dès lors être regardées comme proches de celle de Flamanville ou connexes à celle-ci, au sens et pour l'application de ces dispositions ; <br/>
<br/>
              10. Considérant, en quatrième lieu, que l'étude d'impact traite des risques liés au rejet de tritium gazeux ou liquide dans l'environnement, notamment dans ses chapitres III.3 et IV.1 ; que le moyen tiré d'insuffisances de l'étude sur ce point n'est pas assorti de précisions permettant d'en apprécier le bien-fondé ;<br/>
<br/>
              11. Considérant, en dernier lieu, que l'association requérante soutient que l'étude d'impact serait insuffisante, faute d'analyser les conséquences du rejet dans l'environnement de substances chimiques nocives, susceptibles notamment de provoquer de graves lésions oculaires, provenant de l'utilisation de six tonnes par an de produits dits désincrustants nécessaires au fonctionnement de l'unité de dessalement d'eau de mer de l'EPR, qui doit traiter environ 430 000 m3 d'eau par an ; que l'insuffisance de l'étude d'impact est démontrée, selon elle, par la production d'une étude d'impact complémentaire détaillée traitant ce point, réalisée deux ans après l'enquête publique ; que, toutefois, la circonstance qu'a été réalisée ultérieurement une étude complémentaire afin de préciser certaines modalités d'exécution du projet ne révèle pas par elle-même une insuffisance du dossier de demande d'autorisation ou de l'étude d'impact ; que figurait dans le dossier soumis à enquête publique une annexe B-6c relative à l'unité de dessalement mentionnant l'utilisation de produits désincrustants et comportant une appréciation de la consommation, de la fréquence et des rejets de ces produits ; que, par suite, aucune insuffisance du dossier de demande ou de l'étude d'impact initiale ne peut être regardée en l'espèce comme établie ; <br/>
<br/>
              En ce qui concerne les moyens tirés de la méconnaissance du principe de précaution et des stipulations de la convention pour la protection du milieu marin de l'Atlantique du Nord-Est (dite " OSPAR "), s'agissant des rejets de tritium :<br/>
<br/>
              12. Considérant qu'aux termes de l'article 1er  de la Charte de l'environnement : " Chacun a le droit de vivre dans un environnement équilibré et respectueux de la santé " ; qu'aux termes de son article 5 : " Lorsque la réalisation d'un dommage, bien qu'incertaine en l'état des connaissances scientifiques, pourrait affecter de manière grave et irréversible l'environnement, les autorités publiques veillent, par application du principe de précaution et dans leurs domaines d'attributions, à la mise en oeuvre de procédures d'évaluation des risques et à l'adoption de mesures provisoires et proportionnées afin de parer à la réalisation du dommage " ; qu'il incombe à l'autorité administrative compétente en matière d'installations nucléaires de base de rechercher s'il existe des éléments circonstanciés de nature à accréditer l'hypothèse de risques de dommages graves et irréversibles pour l'environnement ou d'atteintes à l'environnement susceptibles de nuire de manière grave à la santé, qui justifieraient, en dépit des incertitudes subsistant quant à leur réalité et à leur portée en l'état des connaissances scientifiques, l'application du principe de précaution ; <br/>
<br/>
              13. Considérant que l'association requérante soutient que les décisions litigieuses méconnaîtraient le principe de précaution, en ce qu'elles autoriseraient une augmentation importante des limites des rejets de tritium sous forme gazeuse ou liquide (" eau tritiée "), en dépit des risques potentiels de cet isotope radioactif de l'hydrogène, notamment en cas de bio-accumulation dans l'organisme, que certaines études conduiraient selon elle à réévaluer ; qu'il résulte toutefois de l'instruction que ces limites maximales demeurent très inférieures à celles qui sont prévues par la réglementation sanitaire en vigueur; que l'augmentation des limites de rejet du tritium s'accompagne d'une diminution des rejets d'autres substances radioactives ; qu'en outre, les études ou documents les plus récents versés au dossier, notamment le livre blanc du tritium publié le 8 juillet 2010, qui été rédigé sur la base des réflexions des groupes de travail mis en place en 2008 par l'ASN, et les travaux de l'Institut de radioprotection et de sûreté nucléaire (IRSN), s'ils soulignent la nécessité de poursuivre les recherches, confirment, en l'état des connaissances scientifiques et compte tenu des mesures prises, l'absence de risques graves pour l'environnement ou la santé publique ; qu'il ne résulte pas de l'instruction que des circonstances nouvelles seraient de nature à remettre en cause l'appréciation portée sur ceux-ci ; qu'ainsi, l'administration n'a pas commis d'erreur d'appréciation dans l'évaluation des risques de l'installation ; <br/>
<br/>
              14. Considérant qu'aux termes du 1. de l'article 2 de la convention pour la protection du milieu marin de l'Atlantique du Nord-Est, signée à Paris le 22 septembre 1992, dont la ratification a été autorisée par la loi du 29 décembre 1997 et qui a été publiée par le décret du 24 août 2000 : " a) (...) les parties contractantes prennent toutes mesures possibles afin de prévenir et de supprimer la pollution, ainsi que les mesures nécessaires à la protection de la zone maritime contre les effets préjudiciables des activités  humaines (...) " ; qu'aux termes de son article 3 : " Les parties contractantes prennent individuellement et conjointement toutes les mesures possibles afin de prévenir et de supprimer la pollution provenant de sources telluriques conformément aux dispositions de la convention, en particulier dans les conditions prévues à l'annexe 1 " ; qu'il résulte de ce qui été dit au point précédent que l'association requérante n'est pas fondée à soutenir que, du fait des risques importants qui seraient liés aux rejets de substances dangereuses, l'arrêté attaqué méconnaîtrait les stipulations précitées ; <br/>
<br/>
              En ce qui concerne le moyen tiré de la méconnaissance de l'article 28 de la loi du 13 juin 2006 et de l'avant dernier alinéa du IV de l'article 18 du décret du 2 novembre 2007 :<br/>
<br/>
              15. Considérant que ce moyen n'est pas assorti de précisions permettant d'en apprécier le bien fondé ; <br/>
<br/>
              En ce qui concerne le moyen tiré de la méconnaissance de l'article 21 du décret du 4 mai 1995 et du 6° du IV de l'article 18 du décret du 2 novembre 2007 : <br/>
<br/>
              16. Considérant que le CRILAN soutient que les moyens nécessaires aux analyses et mesures utiles au contrôle de l'installation et à la surveillance de ses effets sur l'environnement et les prescriptions adoptées sur ce point par l'ASN seraient insuffisants au regard des exigences de l'article 21 du décret du 4 mai 1995 et du IV de l'article 18 du décret du 2 novembre 2007 ; qu'il critique, à l'appui de ce moyen, l'absence de mise en place d'un réseau d'instruments de mesure des températures dans le milieu naturel, pourtant réclamé par la commission d'enquête ; que, toutefois, la détermination des prescriptions en cause relève non de la décision de l'ASN homologuée par l'arrêté attaqué mais de la décision n° 2010-DC-0189 du 7 juillet 2010 précitée tendant, sur la base des meilleurs connaissances disponibles, à prévenir la pollution liée aux rejets et à protéger la zone maritime, qui n'est pas contestée par l'association requérante ; que le moyen mentionné ci-dessus doit donc, en tout état de cause, être écarté comme inopérant ; <br/>
<br/>
              17. Considérant qu'il résulte de tout ce qui précède, sans qu'il soit besoin de statuer sur la fin de non-recevoir opposée par la société EDF, que le CRILAN n'est pas fondé à demander l'annulation de la décision qu'il attaque ;<br/>
<br/>
              Sur les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              18. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante ; qu'il y a lieu, en revanche, de mettre à la charge de l'association requérante la somme de 3 000 euros à verser à Electricité de France au titre de ces mêmes dispositions ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de l'association Comité de réflexion, d'information et de lutte anti-nucléaire (CRILAN) est rejetée.<br/>
<br/>
Article 2 : Le CRILAN versera une somme de 3 000 euros à la société Electricité de France au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
Article 3 : La présente décision sera notifiée au Comité de réflexion, d'information et de lutte anti-nucléaire, à la société Electricité de France, au ministre de l'économie, de l'industrie et du numérique, au ministre de l'écologie, du développement durable et de l'énergie et à l'Autorité de sûreté nucléaire.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">17-05-02 COMPÉTENCE. COMPÉTENCE À L'INTÉRIEUR DE LA JURIDICTION ADMINISTRATIVE. COMPÉTENCE DU CONSEIL D'ETAT EN PREMIER ET DERNIER RESSORT. - EXISTENCE - RECOURS CONTRE UNE DÉCISION MINISTÉRIELLE HOMOLOGUANT UNE DÉCISION PRISE PAR L'ASN AU TITRE DE SA MISSION DE CONTRÔLE ET DE RÉGULATION.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">29-03-005 ENERGIE. INSTALLATIONS NUCLÉAIRES. - RECOURS CONTRE LES DÉCISIONS MINISTÉRIELLES D'HOMOLOGATION DES DÉCISIONS PRISES PAR L'ASN AU TITRE DE SA MISSION DE RÉGULATION ET DE CONTRÔLE - COMPÉTENCE DU CONSEIL D'ETAT EN PREMIER ET DERNIER RESSORT - EXISTENCE.
</SCT>
<ANA ID="9A"> 17-05-02 Les dispositions du 4° de l'article R. 311-1 du code de justice administrative doivent être regardées comme donnant compétence au Conseil d'Etat pour connaître en premier et dernier ressort des recours dirigés tant contre les décisions prises par l'Autorité de sûreté nucléaire (ASN) au titre de sa mission de contrôle et de régulation que contre celles par lesquelles les ministres homologuent ces décisions.</ANA>
<ANA ID="9B"> 29-03-005 Les dispositions du 4° de l'article R. 311-1 du code de justice administrative doivent être regardées comme donnant compétence au Conseil d'Etat pour connaître en premier et dernier ressort des recours dirigés tant contre les décisions prises par l'ASN au titre de sa mission de contrôle et de régulation que contre celles par lesquelles les ministres homologuent ces décisions.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
