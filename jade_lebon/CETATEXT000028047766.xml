<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028047766</ID>
<ANCIEN_ID>JG_L_2013_10_000000355289</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/04/77/CETATEXT000028047766.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 4ème SSR, 07/10/2013, 355289</TITRE>
<DATE_DEC>2013-10-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>355289</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 4ème SSR</FORMATION>
<TYPE_REC/>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GADIOU, CHEVALLIER</AVOCATS>
<RAPPORTEUR>M. Gérald Bégranger</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2013:355289.20131007</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 28 décembre 2011 au secrétariat du contentieux du Conseil d'Etat, présenté par le ministre du travail, de l'emploi et de la santé ; le ministre demande au Conseil d'Etat d'annuler le jugement n° 1100802-1100867 du 3 novembre 2011 par lequel le tribunal administratif de Rennes a annulé, d'une part, la décision du 25 janvier 2011 de la directrice générale du Centre national de gestion des praticiens hospitaliers et des personnels de direction de la fonction publique hospitalière rejetant la demande présentée par Mme B...A...tendant à sa réintégration au sein du centre hospitalier de Ploërmel et, d'autre part, l'arrêté du 22 février 2011 prononçant le maintien de Mme A...en position de disponibilité pour convenances personnelles ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de la santé publique ;<br/>
<br/>
              Vu le code du travail ;<br/>
<br/>
              Vu la loi n° 86-33 du 9 janvier 1986 ;<br/>
<br/>
              Vu le décret n° 88-976 du 13 octobre 1988 ;<br/>
<br/>
              Vu le décret n° 2005-921 du 2 août 2005 ;<br/>
<br/>
              Vu le décret n° 2007-704 du 4 mai 2007 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Gérald Bégranger, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gadiou, Chevallier, avocat de MmeA... ;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 14 avril 2008, la directrice générale du Centre national de gestion des praticiens hospitaliers et des personnels de direction de la fonction publique hospitalière a placé MmeA..., directrice adjointe au centre hospitalier de Ploërmel, en disponibilité pour convenances personnelles à compter du 1er janvier 2009, pour une durée d'un an  ; que, par un arrêté du 22 mars 2010, la directrice générale a maintenu l'intéressée dans cette position à compter du 1er janvier 2010 et pour la même durée ; que, par une lettre du 25 mai 2010, Mme A... a demandé à être réintégrée dès le 1er août 2010 ; qu'en l'absence de réponse, elle a demandé par lettre du 16 septembre 2010, à être réintégrée au 1er janvier 2011 ; qu'elle s'est portée candidate à un poste au centre hospitalier de Ploërmel ; que, par une lettre du 25 janvier 2011, la directrice générale du Centre national de gestion des praticiens hospitaliers et des personnels de direction de la fonction publique hospitalière l'a informée de l'impossibilité de la réintégrer dans cet établissement, faute de poste vacant, puis par une décision du 22 février 2011, l'a maintenue en disponibilité pour convenances personnelles, à compter du 1er janvier 2011 et jusqu'à sa réintégration ; que, par un jugement du 3 novembre 2011, le tribunal administratif de Rennes a annulé les décisions des 25 janvier et 22 février 2011 mentionnées ci-dessus ; que le ministre du travail, de l'emploi et de la santé  se pourvoit en cassation contre ce jugement ; <br/>
<br/>
              2. Considérant, d'une part, qu'aux termes de l'article 37 du décret du 13 octobre 1988 relatif au régime particulier de certaines positions des fonctionnaires hospitaliers, à l'intégration et à certaines modalités de mise à disposition : " Deux mois au moins avant l'expiration de la période de disponibilité en cours, le fonctionnaire doit solliciter soit le renouvellement de sa disponibilité soit sa réintégration. / (...) la réintégration est de droit à la première vacance lorsque la disponibilité n'a pas excédé trois ans. (...) / Le fonctionnaire qui ne peut être réintégré faute de poste vacant est maintenu en disponibilité jusqu'à sa réintégration et au plus tard jusqu'à ce que trois postes lui aient été proposés (...) " ; <br/>
<br/>
              3. Considérant, d'autre part, qu'aux termes de l'article 50-1 de la loi du 9 janvier 1986 portant dispositions statutaires relatives à la fonction publique hospitalière, dans sa rédaction alors applicable : " Les personnels de direction et les directeurs des soins des établissements mentionnés à l'article 2 peuvent être placés en recherche d'affectation auprès du Centre national de gestion (...) pour une durée maximale de deux ans. (...) " ; qu'aux termes de l'article 25-1 du décret du 2 août 2005 portant statut particulier des grades et emplois des personnels de direction des établissements mentionnés à l'article 2 (1° et 7°) de la loi n° 86-33 du 9 janvier 1986 portant dispositions statutaires relatives à la fonction publique hospitalière, dans sa rédaction alors en vigueur : " La recherche d'affectation est la situation dans laquelle les personnels de direction sont placés, compte tenu des nécessités du service, auprès du Centre national de gestion, soit sur leur demande, soit d'office, en vue de permettre leur adaptation ou leur reconversion professionnelle ou de favoriser la réorganisation ou la restructuration des structures hospitalières. / Le placement d'un fonctionnaire en recherche d'affectation est prononcé, après avis de la commission administrative paritaire nationale et pour une durée maximale de deux ans, par arrêté du directeur général du Centre national de gestion. (...) " ; <br/>
<br/>
              4. Considérant qu'il résulte de ces dispositions qu'un agent relevant des personnels de direction des établissements mentionnés à l'article 2 de la loi du 9 janvier 1986 ne peut être légalement placé en situation de recherche d'affectation qu'en vue de poursuivre  l'un des objectifs qu'elles mentionnent ; qu'ainsi, en jugeant qu'il appartenait au directeur général du Centre national de gestion des praticiens hospitaliers et des personnels de direction de la fonction publique hospitalière de placer Mme A...dans une telle situation, au seul motif qu'elle ne pouvait être réintégrée à l'issue de sa période de disponibilité pour convenances personnelles, le tribunal administratif de Rennes a commis une erreur de droit ; que, par suite, son jugement doit être annulé ;<br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              6. Considérant que les demandes de Mme A...enregistrées au greffe du tribunal administratif de Rennes sous les numéros 1100802 et 1100867 présentent à juger les mêmes questions ; qu'il y a lieu de les joindre pour statuer par une seule décision ;<br/>
<br/>
              Sur les conclusions aux fins d'annulation des décisions des 25 janvier et 22 février 2011 de la directrice générale du Centre national de gestion des praticiens hospitaliers et des personnels de direction de la fonction publique hospitalière :<br/>
<br/>
              7. Considérant qu'aux termes de l'article L. 6143-7 du code de la santé publique : " (...) Le directeur dispose d'un pouvoir de nomination dans l'établissement. Il propose au directeur général du Centre national de gestion la nomination des directeurs adjoints et des directeurs des soins. La commission administrative paritaire compétente émet un avis sur ces propositions (...) " ; qu'aux termes de l'article 2 du décret du 4 mai 2007 relatif à l'organisation et au fonctionnement du Centre national de gestion des praticiens hospitaliers et des personnels de direction de la fonction publique hospitalière : " Le directeur général du Centre national de gestion assure, au nom du ministre chargé de la santé, la gestion statutaire et le développement des ressources humaines des personnels de direction et des directeurs des soins de la fonction publique hospitalière (...) et, à ce titre : / 1° La nomination dans les corps des personnels de direction de la fonction publique hospitalière et les autres actes de gestion de leur carrière (...) " ; <br/>
<br/>
              8. Considérant qu'il ressort des pièces du dossier qu'un emploi de direction au centre hospitalier de Ploërmel, à pourvoir en avril 2011, a été inscrit sur une liste des postes offerts au concours à l'issue de la scolarité à l'Ecole nationale de la santé publique publiée au Journal officiel du 27 octobre 2010, postérieurement à la demande de réintégration présentée par MmeA... ; que, par ailleurs, deux vacances d'emplois dans le même établissement ont été publiées le 12 décembre 2010 puis retirées le 24 décembre, alors que Mme A...avait fait acte de candidature, sans que l'administration ait justifié des raisons de ce retrait ; que, dans ces conditions, il doit être regardé comme établi qu'au 1er janvier 2011, date à laquelle a pris fin la disponibilité de MmeA..., au moins un poste susceptible de lui être confié était vacant au centre hospitalier de Ploërmel ; que, aucun motif tiré des nécessités du service n'étant invoqué par l'administration, MmeA..., qui était placée en disponibilité pour convenances personnelles  depuis moins de trois ans et qui avait présenté au moins deux mois avant l'expiration de sa période de disponibilité une demande en ce sens, avait un droit à être réintégrée ; que, dans ces conditions, et alors même que le directeur du centre hospitalier de Ploërmel n'avait pas proposé sa nomination, la directrice générale du centre national de gestion des praticiens hospitaliers et des personnels de direction de la fonction publique hospitalière n'a pu légalement, par les décisions attaquées, refuser de réintégrer Mme A...au sein du centre hospitalier de Ploërmel et la maintenir en disponibilité pour convenances personnelles ; <br/>
<br/>
              9. Considérant qu'il résulte de ce qui précède que Mme A...est fondée à demander l'annulation des décisions des 25 janvier et 22 février 2011 de la directrice générale du Centre national de gestion des praticiens hospitaliers et des personnels de direction de la fonction publique hospitalière prises au nom du ministre chargé de la santé, qu'elle attaque ;<br/>
<br/>
              Sur les conclusions aux fins d'injonction :<br/>
<br/>
              10. Considérant qu'aux termes de l'article L. 911-1 du code de justice administrative : " lorsque sa décision implique nécessairement qu'une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public prenne une mesure d'exécution dans un sens déterminé, la juridiction, saisie de conclusions en ce sens, prescrit, par la même décision, cette mesure assortie, le cas échéant, d'un délai d'exécution " ;<br/>
<br/>
              11. Considérant que l'annulation des décisions des 25 janvier et 22 février 2011 mentionnées ci-dessus implique nécessairement que Mme A...soit réintégrée à la première vacance de poste, sauf motif tiré des nécessités du service ; qu'il y a lieu pour le Conseil d'Etat d'enjoindre à la directrice générale du Centre national de gestion des praticiens hospitaliers et des personnels de direction de la fonction publique hospitalière de prendre les mesures nécessaires en ce sens ;<br/>
<br/>
              Sur les conclusions tendant à ce qu'il soit enjoint au Centre national de gestion des praticiens hospitaliers et des personnels de direction de la fonction publique hospitalière de verser à Mme A...l'allocation d'assurance prévue à l'article L. 5424-1 du code du travail :<br/>
<br/>
              12. Considérant qu'aux termes de l'article L. 5424-1 du code du travail : " Ont droit à une allocation d'assurance (...) : 1° Les agents fonctionnaires et non fonctionnaires de l'Etat et de ses établissements publics administratifs, les agents titulaires des collectivités territoriales ainsi que les agents statutaires des autres établissements publics administratifs ainsi que les militaires " ;<br/>
<br/>
              13. Considérant que l'annulation prononcée par la présente décision n'implique pas nécessairement que, comme le demande la requérante, il soit enjoint à la directrice générale du Centre national de gestion des praticiens hospitaliers et des personnels de direction de la fonction publique hospitalière de lui verser l'allocation d'assurance prévue à l'article L. 5424-1 du code du travail ; que, par suite, les conclusions présentées à ce titre ne peuvent qu'être rejetées ;<br/>
<br/>
              Sur les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              14. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à MmeA..., au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement n° 1100802-1100867 du tribunal administratif de Rennes du 3 novembre 2011 et les décisions des 25 janvier et 22 février 2011 de la directrice générale du Centre national de gestion des praticiens hospitaliers et des personnels de direction de la fonction publique hospitalière sont annulés.<br/>
<br/>
Article 2 : Il est enjoint à la directrice générale du Centre national de gestion des praticiens hospitaliers et des personnels de direction de la fonction publique hospitalière de réintégrer Mme A...à la première vacance de poste, sauf motif tiré des nécessités du service. <br/>
<br/>
Article 3 : L'Etat versera à Mme A...une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
		Article 4 : Le surplus des conclusions de Mme A...est rejeté.<br/>
<br/>
Article 5 : La présente décision sera notifiée à la ministre des affaires sociales et de la santé, au Centre national de gestion des praticiens hospitaliers et des personnels de direction de la fonction publique hospitalière et à Mme B...A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-11 FONCTIONNAIRES ET AGENTS PUBLICS. DISPOSITIONS PROPRES AUX PERSONNELS HOSPITALIERS. - 1) RECHERCHE D'AFFECTATION (ART. 50-1 DE LA LOI DU 9 JANVIER 1986 ET 25-1 DU DÉCRET DU 2 AOÛT 2005) - CONDITION POUR PLACER UN AGENT DANS CETTE SITUATION - POURSUITE D'UN DES OBJECTIFS D'ADAPTATION OU DE RECONVERSION PROFESSIONNELLE DES PERSONNELS OU DE RÉORGANISATION OU DE RESTRUCTURATION DES STRUCTURES HOSPITALIÈRES - 2) DROIT À RÉINTÉGRATION APRÈS DISPONIBILITÉ POUR CONVENANCES PERSONNELLES - IMPLICATIONS - EXISTENCE D'UN POSTE VACANT SUSCEPTIBLE D'ÊTRE CONFIÉ À L'AGENT À LA DATE DE FIN DE LA DISPONIBILITÉ - POSSIBILITÉ POUR LE DIRECTEUR DU CENTRE NATIONAL DE GESTION DES PRATICIENS HOSPITALIERS ET DES PERSONNELS DE DIRECTION DE LA FONCTION PUBLIQUE HOSPITALIÈRE DE REFUSER LA RÉINTÉGRATION SANS MOTIF TIRÉ DES NÉCESSITÉS DU SERVICE - ABSENCE, Y COMPRIS EN L'ABSENCE DE PROPOSITION DE NOMINATION EN CE SENS DU DIRECTEUR D'ÉTABLISSEMENT.
</SCT>
<ANA ID="9A"> 36-11 1) Il résulte des dispositions des articles 50-1 de la loi n° 86-33 du 9 janvier 1986 et 25-1 du décret n° 2005-921 du 2 août 2005 qu'un agent relevant des personnels de direction des établissements publics de santé ne peut être légalement placé en situation de recherche d'affectation qu'en vue de poursuivre l'un des objectifs que ces dispositions mentionnent, à savoir soit son adaptation, soit sa reconversion professionnelle, soit la réorganisation ou la restructuration des structures hospitalières.,,,,2) Cas d'un agent ayant droit à réintégration après disponibilité pour convenances personnelles depuis moins de trois ans et qui a présenté au moins deux mois avant l'expiration de sa période de disponibilité une demande en ce sens. Dès lors qu'il est établi qu'à la date à laquelle a pris fin la disponibilité de l'intéressé au moins un poste susceptible de lui être confié était vacant, et en l'absence de tout motif tiré des nécessités du service, le directeur général du centre national de gestion des praticiens hospitaliers et des personnels de direction de la fonction publique hospitalière n'a pu légalement refuser la réintégration, alors même que le directeur du centre hospitalier dans lequel se trouvait le poste vacant n'avait pas proposé la nomination.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
