<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033936377</ID>
<ANCIEN_ID>JG_L_2017_01_000000404858</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/93/63/CETATEXT000033936377.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 27/01/2017, 404858</TITRE>
<DATE_DEC>2017-01-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>404858</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Pierre Lombard</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CECHR:2017:404858.20170127</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...C...a demandé au tribunal administratif de Lille d'annuler pour excès de pouvoir, d'une part, l'arrêté du maire de la commune de Marcq-en-Baroeul (Nord) du 11  janvier 2016 lui retirant ses délégations en qualité de quatrième adjoint et, d'autre part, la délibération du 27 janvier 2016 par laquelle le conseil municipal de cette commune s'est prononcé contre son maintien dans les fonctions d'adjoint au maire et a décidé de supprimer son poste d'adjoint. Par un jugement n° 1601679 du 28 octobre 2016, enregistré le 4 novembre 2016 au secrétariat du contentieux du Conseil d'Etat, le tribunal administratif de Lille, avant de statuer sur la requête de M. A...C...a décidé, en application des dispositions de l'article L. 113-1 du code de justice administrative, de transmettre le dossier de cette demande au Conseil d'Etat, en soumettant à son examen les questions suivantes :<br/>
              1°) Un adjoint au maire peut-il être qualifié de personne physique au sens de l'article L. 100-3 du code des relations entre le public et l'administration qui définit la notion de public, et par là même le champ d'application de ce code '<br/>
              2°) Dans l'affirmative, un retrait de délégation à un adjoint au maire doit-il s'analyser comme une décision prise en considération de la personne au sens de l'article L. 121-1 du code des relations entre le public et l'administration, alors même que cette notion n'a jusqu'ici été consacrée que s'agissant de décisions individuelles et que certaines d'entre elles, dont la décision de refus de titularisation en fin de stage ne revêtant pas le caractère d'une mesure disciplinaire (notamment arrêt du Conseil d'Etat du 3 décembre 2003, MmeB..., n° 236485), sont exemptées de procédure contradictoire préalable '<br/>
              3°) Le caractère règlementaire de la décision de retrait de délégation à un adjoint au maire, ou toute autre caractéristique propre à ce type de décision, fait-il obstacle à l'application d'une procédure contradictoire préalable '<br/>
              4°) Si la décision portant retrait de délégation doit être précédée d'une procédure contradictoire préalable, quelles en sont les modalités '<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des collectivités territoriales ;<br/>
              - le code des relations entre le public et l'administration ;<br/>
              - la loi n° 2013-1005 du 12 novembre 2013 ;<br/>
              - le code de justice administrative, notamment son article L. 113-1 ; <br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Pierre Lombard, auditeur, <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
REND L'AVIS SUIVANT<br/>
              1. D'une part, aux termes de l'article L. 100-1 du code des relations entre le public et l'administration : " Le présent code régit les relations entre le public et l'administration en l'absence de dispositions spéciales applicables. / Sauf dispositions contraires du présent code, celui-ci est applicable aux relations entre l'administration et ses agents ". Aux termes de l'article L. 100-3 du même code : " Au sens du présent code et sauf disposition contraire de celui-ci, on entend par : / 1° Administration : les administrations de l'Etat, les collectivités territoriales (...). / 2° Public : a) Toute personne physique ; (...) ". D'autre part, l'article L. 2122-18 du code général des collectivités territoriales dispose : " Le maire est seul chargé de l'administration, mais il peut, sous sa surveillance et sa responsabilité, déléguer par arrêté une partie de ses fonctions à un ou plusieurs de ses adjoints (...). / (...) Lorsque le maire a retiré les délégations qu'il avait données à un adjoint, le conseil municipal doit se prononcer sur le maintien de celui-ci dans ses fonctions ".<br/>
<br/>
              2. La décision par laquelle le maire rapporte la délégation qu'il a consentie à l'un de ses adjoints est une décision à caractère réglementaire qui a pour objet la répartition des compétences entre les différentes autorités municipales. Une telle décision ne relève pas du champ défini par les dispositions précitées du code des relations entre le public et l'administration.<br/>
<br/>
              3. Il en résulte que l'article L. 121-1 du code des relations entre le public et l'administration, qui prévoit qu'exception faite des cas où il est statué sur une demande, les décisions individuelles qui doivent être motivées en application de l'article L. 211-2 de ce code, ainsi que les décisions qui, bien que non mentionnées à cet article, sont prises en considération de la personne, sont soumises au respect d'une procédure contradictoire préalable, ne s'applique pas à la décision par laquelle le maire rapporte la délégation qu'il a consentie à l'un de ses adjoints.<br/>
<br/>
              Le présent avis sera notifié au tribunal administratif de Lille, à la commune de Marcq-en-Baroeul et au ministre de l'intérieur.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-01-06-01-01 ACTES LÉGISLATIFS ET ADMINISTRATIFS. DIFFÉRENTES CATÉGORIES D'ACTES. ACTES ADMINISTRATIFS - CLASSIFICATION. ACTES RÉGLEMENTAIRES. PRÉSENTENT CE CARACTÈRE. - DÉCISION PAR LAQUELLE LE MAIRE RAPPORTE LA DÉLÉGATION QU'IL A CONSENTIE À L'UN DE SES ADJOINTS.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">01-03-03-02 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - FORME ET PROCÉDURE. PROCÉDURE CONTRADICTOIRE. CARACTÈRE NON OBLIGATOIRE. - DÉCISION PAR LAQUELLE LE MAIRE RAPPORTE LA DÉLÉGATION QU'IL A CONSENTIE À L'UN DE SES ADJOINTS.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">135-02-01-02-02-03-04 COLLECTIVITÉS TERRITORIALES. COMMUNE. ORGANISATION DE LA COMMUNE. ORGANES DE LA COMMUNE. MAIRE ET ADJOINTS. POUVOIRS DU MAIRE. DÉLÉGATION DES POUVOIRS DU MAIRE. - DÉCISION PAR LAQUELLE LE MAIRE RAPPORTE LA DÉLÉGATION QU'IL A CONSENTIE À L'UN DE SES ADJOINTS - 1) DÉCISION À CARACTÈRE RÉGLEMENTAIRE - EXISTENCE - 2) DÉCISION SOUMISE AU RESPECT D'UNE PROCÉDURE CONTRADICTOIRE PRÉALABLE (ART. L. 121-1 DU CRPA) - ABSENCE.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">135-02-01-02-02-04 COLLECTIVITÉS TERRITORIALES. COMMUNE. ORGANISATION DE LA COMMUNE. ORGANES DE LA COMMUNE. MAIRE ET ADJOINTS. ADJOINTS. - DÉCISION PAR LAQUELLE LE MAIRE RAPPORTE LA DÉLÉGATION QU'IL A CONSENTIE À L'UN DE SES ADJOINTS - 1) DÉCISION À CARACTÈRE RÉGLEMENTAIRE - EXISTENCE - 2) DÉCISION SOUMISE AU RESPECT D'UNE PROCÉDURE CONTRADICTOIRE PRÉALABLE (ART. L. 121-1 DU CRPA) - ABSENCE.
</SCT>
<ANA ID="9A"> 01-01-06-01-01 La décision par laquelle le maire rapporte la délégation qu'il a consentie à l'un de ses adjoints sur le fondement de l'article L. 2122-18 du code général des collectivités territoriales (CGCT) est une décision à caractère réglementaire qui a pour objet la répartition des compétences entre les différentes autorités municipales. Une telle décision ne relève pas du champ d'application du code des relations entre le public et l'administration (CRPA) tel qu'il est défini par ses articles L. 100-1 et L. 100-3.</ANA>
<ANA ID="9B"> 01-03-03-02 La décision par laquelle le maire rapporte la délégation qu'il a consentie à l'un de ses adjoints sur le fondement de l'article L. 2122-18 du code général des collectivités territoriales (CGCT) est une décision à caractère réglementaire qui a pour objet la répartition des compétences entre les différentes autorités municipales. Une telle décision ne relève pas du champ d'application du code des relations entre le public et l'administration (CRPA) tel qu'il est défini par ses articles L. 100-1 et L. 100-3.... ,,Il en résulte que l'article L. 121-1 du CRPA, qui prévoit qu'exception faite des cas où il est statué sur une demande, les décisions individuelles qui doivent être motivées en application de l'article L. 211-2 du même code, ainsi que les décisions qui, bien que non mentionnées à cet article, sont prises en considération de la personne, sont soumises au respect d'une procédure contradictoire préalable, ne s'applique pas à la décision par laquelle le maire rapporte la délégation qu'il a consentie à l'un de ses adjoints.</ANA>
<ANA ID="9C"> 135-02-01-02-02-03-04 1) La décision par laquelle le maire rapporte la délégation qu'il a consentie à l'un de ses adjoints sur le fondement de l'article L. 2122-18 du code général des collectivités territoriales (CGCT) est une décision à caractère réglementaire qui a pour objet la répartition des compétences entre les différentes autorités municipales. Une telle décision ne relève pas du champ d'application du code des relations entre le public et l'administration (CRPA) tel qu'il est défini par ses articles L. 100-1 et L. 100-3.... ,,2) Il en résulte que l'article L. 121-1 du CRPA, qui prévoit qu'exception faite des cas où il est statué sur une demande, les décisions individuelles qui doivent être motivées en application de l'article L. 211-2 du même code, ainsi que les décisions qui, bien que non mentionnées à cet article, sont prises en considération de la personne, sont soumises au respect d'une procédure contradictoire préalable, ne s'applique pas à la décision par laquelle le maire rapporte la délégation qu'il a consentie à l'un de ses adjoints.</ANA>
<ANA ID="9D"> 135-02-01-02-02-04 1) La décision par laquelle le maire rapporte la délégation qu'il a consentie à l'un de ses adjoints sur le fondement de l'article L. 2122-18 du code général des collectivités territoriales (CGCT) est une décision à caractère réglementaire qui a pour objet la répartition des compétences entre les différentes autorités municipales. Une telle décision ne relève pas du champ d'application du code des relations entre le public et l'administration (CRPA) tel qu'il est défini par ses articles L. 100-1 et L. 100-3.... ,,2) Il en résulte que l'article L. 121-1 du CRPA, qui prévoit qu'exception faite des cas où il est statué sur une demande, les décisions individuelles qui doivent être motivées en application de l'article L. 211-2 du même code, ainsi que les décisions qui, bien que non mentionnées à cet article, sont prises en considération de la personne, sont soumises au respect d'une procédure contradictoire préalable, ne s'applique pas à la décision par laquelle le maire rapporte la délégation qu'il a consentie à l'un de ses adjoints.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
