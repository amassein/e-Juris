<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025955941</ID>
<ANCIEN_ID>JG_L_2012_06_000000339631</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/95/59/CETATEXT000025955941.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème et 5ème sous-sections réunies, 01/06/2012, 339631</TITRE>
<DATE_DEC>2012-06-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>339631</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème et 5ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Bernard Stirn</PRESIDENT>
<AVOCATS>SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>Mme Bethânia Gaschet</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Gaëlle Dumortier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:339631.20120601</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 19 mai et 16 août 2010 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. Jacques A, demeurant ... ; M. A demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler le jugement n° 0904511-5 du 22 juillet 2009 par lequel le tribunal administratif de Melun a rejeté sa demande tendant à ce qu'il soit enjoint au préfet du Val-de-Marne d'assurer son logement dans le délai d'un mois sous astreinte de 500 euros par jour de retard ; <br/>
<br/>
              2°) réglant l'affaire au fond, d'enjoindre à l'administration de lui octroyer un logement dans le délai d'un mois sous astreinte de 337 euros par mois de retard ; <br/>
<br/>
              3°) de mettre à la charge de l'ordre des médecins la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 10 mai 2012, présentée pour M. A ;<br/>
<br/>
              Vu le code de la construction et de l'habitation ; <br/>
<br/>
              Vu le décret n° 2002-120 du 30 janvier 2002 ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Bethânia Gaschet, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Lyon-Caen, Thiriez, avocat M. A, <br/>
<br/>
      - les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Lyon-Caen, Thiriez, avocat M. A, <br/>
<br/>
<br/>
<br/>
              Sans qu'il soit besoin d'examiner les autres moyens du pourvoi ; <br/>
<br/>
              Considérant qu'aux termes du II de l'article L. 441-2-3 du code de la construction et de l'habitation : "  (...) Dans un délai fixé par décret, la commission de médiation désigne les demandeurs qu'elle reconnaît prioritaires et auxquels un logement doit être attribué en urgence. Elle détermine pour chaque demandeur, en tenant compte de ses besoins et de ses capacités, les caractéristiques de ce logement (...) / La commission de médiation transmet au représentant de l'Etat dans le département la liste des demandeurs auxquels doit être attribué en urgence un logement / (...) Le représentant de l'Etat dans le département désigne chaque demandeur à un organisme bailleur disposant de logements correspondant à la demande (...) / En cas de refus de l'organisme de loger le demandeur, le représentant de l'Etat dans le département qui l'a désigné procède à l'attribution d'un logement correspondant aux besoins et aux capacités du demandeur sur ses droits de réservation. (...) / " ; qu'aux termes de l'article L. 441-2-3-1 du code de la construction et de l'habitation dans sa rédaction en vigueur à la date du jugement attaqué : " I.-Le demandeur qui a été reconnu par la commission de médiation comme prioritaire et comme devant être logé d'urgence et qui n'a pas reçu, dans un délai fixé par décret, une offre de logement tenant compte de ses besoins et de ses capacités peut introduire un recours devant la juridiction administrative tendant à ce que soit ordonné son logement ou son relogement. (...) / Le président du tribunal administratif ou le magistrat qu'il désigne, lorsqu'il constate que la demande a été reconnue comme prioritaire par la commission de médiation et doit être satisfaite d'urgence et que n'a pas été offert au demandeur un logement tenant compte de ses besoins et de ses capacités, ordonne le logement ou le relogement de celui-ci par l'Etat et peut assortir son injonction d'une astreinte. " ; qu'aux termes de l'article R. 441-14-1 du même code dans sa rédaction applicable à la date du jugement attaqué : " Peuvent être désignées par la commission comme prioritaires et devant être logées d'urgence en application du II de l'article L. 441-2-3 les personnes de bonne foi qui satisfont aux conditions réglementaires d'accès au logement social qui se trouvent dans l'une des situations suivantes : / (...) -être dépourvues de logement. (...) / (...) -être hébergées dans une structure d'hébergement de façon continue depuis plus de six mois ou logées dans un logement de transition depuis plus de dix-huit mois (...) ; / (...) Si la situation particulière du demandeur le justifie, la commission peut, par une décision spécialement motivée, désigner comme prioritaire une personne ne répondant qu'incomplètement aux caractéristiques définies ci-dessus." ; <br/>
<br/>
              Considérant qu'il résulte des dispositions précitées du code de la construction et de l'habitation que le juge, saisi sur le fondement de l'article L. 441-2-3-1de ce code, doit, s'il constate qu'un demandeur de logement a été reconnu par une commission de médiation comme prioritaire et devant être logé ou relogé d'urgence et que ne lui a pas été offert un logement tenant compte de ses besoins et de ses capacités définis par la commission, ordonner à l'administration de loger ou reloger l'intéressé, sauf si cette dernière apporte la preuve que l'urgence a complètement disparu ; que, d'une part, un hébergement dans un foyer ne saurait être regardé comme un logement tenant compte des besoins et capacités du demandeur au sens des dispositions précitées du I de l'article L. 441-2-3-1 du code de la construction et de l'habitation, d'autre part, la circonstance que, postérieurement à la décision de la commission de médiation le reconnaissant comme prioritaire et devant être logé ou relogé d'urgence, un demandeur de logement se trouve hébergé de façon temporaire dans une structure d'hébergement ou un logement de transition ne suffit pas à faire disparaître l'urgence qu'il y a à le reloger ; que, par suite, le tribunal administratif de Melun a commis une erreur de droit en se fondant sur la circonstance que M. A, reconnu par la commission de médiation de Créteil comme demandeur de logement prioritaire et devant être logé d'urgence au motif qu'il était dépourvu de logement, était, à la date du jugement attaqué, hébergé dans un foyer, pour juger qu'il n'y avait pas lieu d'enjoindre au préfet d'assurer son logement ; <br/>
<br/>
              Considérant qu'il résulte de ce qui précède que M. A est fondé à demander l'annulation du jugement attaqué ; <br/>
<br/>
              Considérant que les conclusions présentées pour M. A sur le fondement des dispositions de l'article L. 761-1 sont dirigées contre l'ordre des médecins, qui n'est pas partie dans la présente instance ; que, par suite, elles ne peuvent qu'être rejetées ;  <br/>
<br/>
<br/>
<br/>
<br/>
              			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : Le jugement du 22 juillet 2009 du tribunal administratif de Melun est annulé. <br/>
<br/>
            Article 2 : Le surplus des conclusions du pourvoi de M. A est rejeté. <br/>
<br/>
            Article 3 : L'affaire est renvoyée devant le tribunal administratif de Melun. <br/>
<br/>
Article 4 : La présente décision sera notifiée à M. Jacques A et à la ministre de l'égalité des territoires et du logement.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">38-07-01 LOGEMENT. - DEMANDEUR RECONNU COMME PRIORITAIRE ET DEVANT ÊTRE LOGÉ D'URGENCE PAR LA COMMISSION DE MÉDIATION - RECOURS DALO (ART. L. 441-2-3- 1) - 1) CONDITION TENANT À CE QUE LE DEMANDEUR NE SE SOIT PAS VU OFFRIR UN LOGEMENT TENANT COMPTE DE SES BESOINS ET CAPACITÉS - NOTION DE LOGEMENT TENANT COMPTE DES BESOINS ET CAPACITÉS DU DEMANDEUR - LOGEMENT DANS UN FOYER - EXCLUSION - 2) CIRCONSTANCE QUE LE DEMANDEUR SE TROUVE HÉBERGÉ DE FAÇON TEMPORAIRE DANS UNE STRUCTURE D'HÉBERGEMENT OU UN LOGEMENT DE TRANSITION - INCIDENCE SUR L'URGENCE À LE RELOGER - ABSENCE.
</SCT>
<ANA ID="9A"> 38-07-01 Le juge, saisi d'un recours DALO sur le fondement de l'article L. 441-2-3-1 du code de la construction et de l'habitation, doit, s'il constate qu'un demandeur de logement a été reconnu par une commission de médiation comme prioritaire et devant être logé ou relogé d'urgence et que ne lui a pas été offert un logement tenant compte de ses besoins et de ses capacités définis par la commission, ordonner à l'administration de loger ou reloger l'intéressé, sauf si cette dernière apporte la preuve que l'urgence a complètement disparu.,,1) Un hébergement dans un foyer ne saurait être regardé comme un logement tenant compte des besoins et capacités du demandeur au sens des dispositions du I de l'article L. 441-2-3-1 du code de la construction et de l'habitation.,,2) La circonstance que, postérieurement à la décision de la commission de médiation le reconnaissant comme prioritaire et devant être logé ou relogé d'urgence, un demandeur de logement se trouve hébergé de façon temporaire dans une structure d'hébergement ou un logement de transition ne suffit pas à faire disparaître l'urgence qu'il y a à le reloger.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
