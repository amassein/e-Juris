<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030853978</ID>
<ANCIEN_ID>JG_L_2015_07_000000386288</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/85/39/CETATEXT000030853978.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème / 2ème SSR, 01/07/2015, 386288</TITRE>
<DATE_DEC>2015-07-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>386288</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème / 2ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Vincent Montrieux</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Bertrand Dacosta</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:386288.20150701</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Montreuil d'annuler l'arrêté du 13 février 2013 par lequel le préfet de la Seine-Saint-Denis lui a refusé la délivrance d'un titre de séjour, l'a obligé à quitter le territoire français et a fixé le pays à destination duquel il sera éloigné. Par un jugement n° 1302674 du 18 novembre 2013, le tribunal administratif de Montreuil a annulé cet arrêté, enjoint au préfet de la Seine Saint Denis de procéder, dans un délai de trois mois à compter de la notification du jugement, à un nouvel examen de la situation de M. B..., et de lui délivrer, pendant cet examen, une autorisation provisoire de séjour.<br/>
<br/>
              Par un arrêt n° 13VE03826 du 23 septembre 2014, la cour administrative d'appel de Versailles  a rejeté l'appel du  ministre de l'intérieur contre ce jugement. <br/>
<br/>
              Par un pourvoi enregistré le 8 décembre 2014 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'intérieur demande au Conseil d'Etat d'annuler cet arrêt. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Vincent Montrieux, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Bertrand Dacosta, rapporteur public ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M.B..., de nationalité turque, entré en France le 3 avril 2011 selon ses déclarations, a vu sa demande d'asile rejetée par une décision de l'Office français de protection des réfugiés et apatrides (OFPRA) du 2 décembre 2011 ; que ce rejet a été confirmé par une décision du 16 avril 2012 de la Cour nationale du droit d'asile ; que, par un arrêté du 13 février 2013, le préfet de la Seine-Saint-Denis a refusé de lui délivrer un titre de séjour, a assorti ce refus d'une obligation de quitter le territoire français et a fixé le pays à destination duquel il sera renvoyé ; que, par l'arrêt attaqué du 23 septembre 2014, la cour administrative d'appel de Versailles a confirmé le jugement par lequel le tribunal administratif de Montreuil a annulé cet arrêté ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 742-3 du code de l'entrée et du séjour des étrangers et du droit d'asile : " L'étranger admis à séjourner en France bénéficie du droit de s'y maintenir jusqu'à la notification de la décision de l'Office français de protection des réfugiés et apatrides ou, si un recours a été formé, jusqu'à la notification de la décision de la Cour nationale du droit d'asile (...) " ; qu'aux termes de l'article R. 733-32 du même code : " Le secrétaire général de la cour notifie la décision de la cour au requérant par lettre recommandée avec demande d'avis de réception dans les conditions prévues au deuxième alinéa de l'article R. 213-3. Il la notifie également au directeur général de l'office. Il informe simultanément du caractère positif ou négatif de cette décision le préfet compétent et, à Paris, le préfet de police ainsi que le directeur de l'Office français de l'immigration et de l'intégration. / La cour communique au préfet compétent et, à Paris, au préfet de police, lorsque ceux-ci en font la demande, copie de l'avis de réception. (...) " ; <br/>
<br/>
              3. Considérant qu'il résulte de ces dispositions que l'étranger qui demande l'asile a le droit de séjourner sur le territoire national à ce titre jusqu'à ce que la décision rejetant sa demande lui ait été notifiée régulièrement par l'OFPRA ou, si un recours a été formé devant elle, par la Cour nationale du droit d'asile ; qu'en l'absence d'une telle notification, l'autorité administrative ne peut regarder l'étranger à qui l'asile a été refusé comme ne bénéficiant plus de son droit provisoire au séjour ou comme se maintenant irrégulièrement sur le territoire ; qu'en cas de contestation sur ce point, il appartient à l'autorité administrative de justifier que la décision de la Cour nationale du droit d'asile a été régulièrement notifiée à l'intéressé, le cas échéant en sollicitant la communication de la copie de l'avis de réception auprès de la cour ; <br/>
<br/>
              4. Considérant que la cour administrative d'appel de Versailles a relevé que le préfet de la Seine-Saint-Denis se bornait à produire devant elle le relevé des informations de la base de données " Telemofpra ", tenue par l'OFPRA et relative à l'état des procédures de demandes d'asile ; qu'en jugeant qu'il ne justifiait pas ainsi que la décision du 16 avril 2012 de la Cour nationale du droit d'asile avait été régulièrement notifiée à M.B..., elle n'a ni dénaturé les pièces du dossier qui lui était soumis ni commis d'erreur de droit ; que, par suite, le pourvoi du ministre de l'intérieur doit être rejeté ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi du ministre de l'intérieur est rejeté.<br/>
Article 2 : La présente décision sera notifiée au ministre de l'intérieur et à M. A...B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">095-04 - DÉCISION REJETANT LA DEMANDE D'ASILE - INCIDENCE SUR LE DROIT AU SÉJOUR - DATE D'EFFET - NOTIFICATION [RJ1] - MODALITÉS D'ADMINISTRATION DE LA PREUVE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">095-08 - REJET DE LA DEMANDE D'ASILE - INCIDENCE SUR LE DROIT AU SÉJOUR - DATE D'EFFET - NOTIFICATION [RJ1] - MODALITÉS D'ADMINISTRATION DE LA PREUVE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">335-01-03-02 ÉTRANGERS. SÉJOUR DES ÉTRANGERS. REFUS DE SÉJOUR. PROCÉDURE. - DATE D'EFFET DU REJET DE LA DEMANDE D'ASILE - NOTIFICATION [RJ1] - MODALITÉS D'ADMINISTRATION DE LA PREUVE.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">335-03-02-01-01 ÉTRANGERS. OBLIGATION DE QUITTER LE TERRITOIRE FRANÇAIS (OQTF) ET RECONDUITE À LA FRONTIÈRE. LÉGALITÉ INTERNE. ÉTRANGERS NE POUVANT FAIRE L`OBJET D`UNE OQTF OU D`UNE MESURE DE RECONDUITE. DEMANDEURS D'ASILE. - EXISTENCE - DEMANDE D'ASILE REJETÉE MAIS NON ENCORE NOTIFIÉE À L'INTÉRESSÉ [RJ1] - MODALITÉS D'ADMINISTRATION DE LA PREUVE DE LA NOTIFICATION.
</SCT>
<ANA ID="9A"> 095-04 Il résulte des articles L. 742-3 et R. 733-32 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA) que l'étranger qui demande l'asile a le droit de séjourner sur le territoire national à ce titre jusqu'à ce que la décision rejetant sa demande lui ait été notifiée régulièrement par l'Office français de protection des réfugiés et apatrides ou, si un recours a été formé devant elle, par la Cour nationale du droit d'asile (CNDA). En l'absence d'une telle notification, l'autorité administrative ne peut regarder l'étranger à qui l'asile a été refusé comme ne bénéficiant plus de son droit provisoire au séjour ou comme se maintenant irrégulièrement sur le territoire. En cas de contestation sur ce point, il appartient à l'autorité administrative de justifier que la décision de la Cour nationale du droit d'asile a été régulièrement notifiée à l'intéressé, le cas échéant en sollicitant la communication de la copie de l'avis de réception auprès de la cour.</ANA>
<ANA ID="9B"> 095-08 Il résulte des articles L. 742-3 et R. 733-32 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA) que l'étranger qui demande l'asile a le droit de séjourner sur le territoire national à ce titre jusqu'à ce que la décision rejetant sa demande lui ait été notifiée régulièrement par l'Office français de protection des réfugiés et apatrides ou, si un recours a été formé devant elle, par la Cour nationale du droit d'asile (CNDA). En l'absence d'une telle notification, l'autorité administrative ne peut regarder l'étranger à qui l'asile a été refusé comme ne bénéficiant plus de son droit provisoire au séjour ou comme se maintenant irrégulièrement sur le territoire. En cas de contestation sur ce point, il appartient à l'autorité administrative de justifier que la décision de la Cour nationale du droit d'asile a été régulièrement notifiée à l'intéressé, le cas échéant en sollicitant la communication de la copie de l'avis de réception auprès de la cour.</ANA>
<ANA ID="9C"> 335-01-03-02 Il résulte des articles L. 742-3 et R. 733-32 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA) que l'étranger qui demande l'asile a le droit de séjourner sur le territoire national à ce titre jusqu'à ce que la décision rejetant sa demande lui ait été notifiée régulièrement par l'Office français de protection des réfugiés et apatrides ou, si un recours a été formé devant elle, par la Cour nationale du droit d'asile (CNDA). En l'absence d'une telle notification, l'autorité administrative ne peut regarder l'étranger à qui l'asile a été refusé comme ne bénéficiant plus de son droit provisoire au séjour ou comme se maintenant irrégulièrement sur le territoire. En cas de contestation sur ce point, il appartient à l'autorité administrative de justifier que la décision de la Cour nationale du droit d'asile a été régulièrement notifiée à l'intéressé, le cas échéant en sollicitant la communication de la copie de l'avis de réception auprès de la cour.</ANA>
<ANA ID="9D"> 335-03-02-01-01 Il résulte des articles L. 742-3 et R. 733-32 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA) que l'étranger qui demande l'asile a le droit de séjourner sur le territoire national à ce titre jusqu'à ce que la décision rejetant sa demande lui ait été notifiée régulièrement par l'Office français de protection des réfugiés et apatrides ou, si un recours a été formé devant elle, par la Cour nationale du droit d'asile (CNDA). En l'absence d'une telle notification, l'autorité administrative ne peut regarder l'étranger à qui l'asile a été refusé comme ne bénéficiant plus de son droit provisoire au séjour ou comme se maintenant irrégulièrement sur le territoire. En cas de contestation sur ce point, il appartient à l'autorité administrative de justifier que la décision de la Cour nationale du droit d'asile a été régulièrement notifiée à l'intéressé, le cas échéant en sollicitant la communication de la copie de l'avis de réception auprès de la cour.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>Cf. CE, 7 mars 2008, Ministre de l'intérieur et de l'aménagement du territoire c/ Mounkaila, n° 289419, T. pp. 770-773-774-775.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
