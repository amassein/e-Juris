<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027091619</ID>
<ANCIEN_ID>JG_L_2013_02_000000345728</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/09/16/CETATEXT000027091619.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère / 6ème SSR, 20/02/2013, 345728</TITRE>
<DATE_DEC>2013-02-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>345728</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère / 6ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>RICARD ; SCP THOUIN-PALAT, BOUCARD ; SPINOSI</AVOCATS>
<RAPPORTEUR>M. Pascal Trouilly</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2013:345728.20130220</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 12 janvier et 12 avril 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour Mme B...E..., demeurant..., Mme A...E..., demeurant..., M. D... E...demeurant ...et M. C...E...demeurant..., ; les consorts E...demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 09NT02180 du 12 novembre 2010 par lequel la cour administrative d'appel de Nantes a confirmé le jugement n° 08-2586 du 30 juin 2009 par lequel le tribunal administratif de Caen a rejeté leur demande tendant à l'annulation de l'arrêté du 15 septembre 2008 par lequel le maire de Jullouville a délivré à la société Pozzo Promotion un permis de construire un ensemble immobilier de 22 logements ;<br/>
<br/>
              2°) de mettre à la charge de la commune de Jullouville et de la société Pozzo Promotion le versement de la somme de 4 000 euros chacun au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu le code de l'urbanisme, modifié notamment par l'ordonnance n° 2005-1527 du 8 décembre 2005 ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Pascal Trouilly, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Thouin-Palat, Boucard, avocat de Mme E...et autres, de Me Ricard, avocat de la commune de Jullouville et de Me Spinosi, avocat de la société Pozzo Promotion,<br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Thouin-Palat, Boucard, avocat de Mme E...et autres, à Me Ricard, avocat de la commune de Jullouville et à Me Spinosi, avocat de la société Pozzo Promotion ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant, en premier lieu, qu'aux termes de l'article L. 442-1 du code de l'urbanisme, dans sa rédaction issue de l'ordonnance du 8 décembre 2005 relative au permis de construire et aux autorisations d'urbanisme, applicable en l'espèce : " Constitue un lotissement l'opération d'aménagement qui a pour objet ou qui, sur une période de moins de dix ans, a eu pour effet la division, qu'elle soit en propriété ou en jouissance, qu'elle résulte de mutations à titre gratuit ou onéreux, de partage ou de locations, d'une ou de plusieurs propriétés foncières en vue de l'implantation de bâtiments " ; qu'aux termes de l'article L. 442-2 du même code, dans sa rédaction applicable en l'espèce : " Un décret en Conseil d'Etat précise, en fonction du nombre de terrains issus de la division, de la création de voies et d'équipements communs et de la localisation de l'opération, les cas dans lesquels la réalisation d'un lotissement doit être précédée d'un permis d'aménager " ; qu'aux termes de l'article L. 442-3 : " Les lotissements qui ne sont pas soumis à la délivrance d'un permis d'aménager doivent faire l'objet d'une déclaration préalable " ;<br/>
<br/>
              2. Considérant qu'une opération d'aménagement ayant pour effet la division en deux lots d'une propriété foncière est susceptible de constituer un lotissement, au sens de ces dispositions, s'il est prévu d'implanter des bâtiments sur l'un au moins de ces deux lots ; que, par suite, en jugeant que la cession d'une partie du terrain d'assiette n'ayant pas pour objet d'implanter un ou plusieurs bâtiments sur la partie cédée, l'opération ne constituait pas un lotissement au sens de l'article L. 442-1 du code de l'urbanisme, alors qu'il ressort des pièces du dossier qui lui était soumis que la parcelle conservée par la société propriétaire était destinée à l'implantation d'un ensemble immobilier, la cour administrative d'appel de Nantes a commis une erreur de droit ;<br/>
<br/>
              3. Considérant, toutefois, qu'il est constant que la division, résultant de la cession de l'une des parcelles, est intervenue postérieurement à la délivrance du permis de construire attaqué, le 15 septembre 2008 ; que, par suite, le moyen, soulevé devant les juges du fond, tiré de ce que la société pétitionnaire aurait dû solliciter l'attribution d'un permis d'aménager ou que le permis de construire ne pouvait être délivré en l'absence de la déclaration préalable prévue par l'article L. 442-3 du code de l'urbanisme, était inopérant ; que, dès lors, il y a lieu de substituer ce motif au motif erroné retenu par l'arrêt attaqué, dont il justifie sur ce point le dispositif ;<br/>
<br/>
              4. Considérant qu'en estimant que l'insertion dans l'environnement des constructions nouvelles autorisées par le permis de construire ne méconnaissait pas les prescriptions de l'article UA 11 du règlement du plan d'occupation des sols de la commune de Jullouville, la cour a porté sur les faits de l'espèce une appréciation souveraine, exempte de dénaturation ; <br/>
<br/>
              5. Considérant, enfin, qu'il ressort des pièces du dossier soumis aux juges du fond que le mur dont l'implantation était contestée par les consorts E...était prévu à l'alignement et que, par suite, le maire n'a pas fait application des dispositions dérogatoires de l'article UA 6 du règlement du plan d'occupation des sols, qui autorisent dans certaines hypothèses un recul des constructions par rapport à l'alignement, pour délivrer le permis litigieux ; que le moyen tiré de la méconnaissance des dispositions de l'article UA 6 étant ainsi inopérant, la cour n'a pas entaché son arrêt d'irrégularité en s'abstenant d'y répondre ; <br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède que les consorts E...ne sont pas fondés à demander l'annulation de l'arrêt qu'ils attaquent ; que leurs conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ne peuvent, en conséquence, qu'être rejetées ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à leur charge, solidairement, le versement tant à la commune de Jullouville qu'à la société Pozzo Promotion d'une somme de 2 000 euros au titre de ces mêmes dispositions ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi des consorts E...est rejeté.<br/>
Article 2 : Les consorts E...verseront solidairement, d'une part, à la commune de Jullouville, d'autre part, à la société Pozzo Promotion, une somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à Mme B...E..., premier requérant dénommé, à la commune de Jullouville et à la société Pozzo Promotion. Les autres requérants seront informés de la présente décision par la SCP Thouin-Palat, Boucard, avocat au Conseil d'Etat et à la Cour de cassation, qui les représente devant le Conseil d'Etat.<br/>
Copie en sera adressée à la ministre de l'égalité des territoires et du logement.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">68-02-04-01 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PROCÉDURES D'INTERVENTION FONCIÈRE. LOTISSEMENTS. OPÉRATIONS CONSTITUANT UN LOTISSEMENT. - DÉFINITION DU LOTISSEMENT ISSUE DE L'ORDONNANCE DU 8 DÉCEMBRE 2005.
</SCT>
<ANA ID="9A"> 68-02-04-01 Une opération d'aménagement ayant pour effet la division en deux lots d'une propriété foncière est susceptible de constituer un lotissement, au sens des dispositions des articles L. 442-1 à L. 442-3 du code de l'urbanisme, dans leur rédaction issue de l'ordonnance n° 2005-1527 du 8 décembre 2005 relative au permis de construire et aux autorisations d'urbanisme, s'il est prévu d'implanter des bâtiments sur l'un au moins de ces deux lots.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
