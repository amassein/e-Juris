<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028663272</ID>
<ANCIEN_ID>JG_L_2014_02_000000351202</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/66/32/CETATEXT000028663272.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère / 6ème SSR, 26/02/2014, 351202</TITRE>
<DATE_DEC>2014-02-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>351202</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère / 6ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN ; SCP GASCHIGNARD</AVOCATS>
<RAPPORTEUR>M. Pascal Trouilly</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:351202.20140226</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 25 juillet et 25 octobre 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour la société Gestion Camping Caravaning, dont le siège est Camping de l'Ile d'Or, à Saint-Raphaël (83700), et pour M. A...B..., demeurant à... ; la société Gestion Camping Caravaning et M. B... demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'arrêt n° 09MA02462 du 19 mai 2011 par lequel la cour administrative d'appel de Marseille a rejeté leur requête tendant à l'annulation du jugement du 16 avril 2009 par lequel le tribunal administratif de Nice a rejeté leur demande tendant à l'annulation pour excès de pouvoir de la délibération du 13 juin 2005 par laquelle le conseil municipal de la commune de Saint-Raphaël a approuvé le plan local d'urbanisme, en tant qu'elle crée un emplacement réservé sur la parcelle cadastrée BC n° 144 ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel ; <br/>
<br/>
              3°) de mettre à la charge de la commune de Saint-Raphaël la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des collectivités territoriales ; <br/>
<br/>
              Vu le code de l'urbanisme ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Pascal Trouilly, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la société Gestion Camping Caravaning et de M. B...et à la SCP Gaschignard, avocat de la commune de Saint-Raphaël ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que le conseil municipal de la commune de Saint-Raphaël a approuvé le 13 juin 2005, par révision de son plan d'occupation des sols, un plan local d'urbanisme créant, notamment, sur une parcelle de 3 800 m² appartenant à M. B...et exploitée par la société Gestion Camping Caravaning, un emplacement réservé en vue de l'" aménagement d'un espace public " ; que, par un jugement du 16 avril 2009, le tribunal administratif de Nice a rejeté la demande de M. B...et de cette société, tendant à l'annulation de la délibération du 13 juin 2005, en tant qu'elle a créé cet emplacement réservé ; que, par un arrêt du 19 mai 2011, la cour administrative d'appel de Marseille a confirmé ce jugement ; que M. B...et la société Gestion Camping Caravaning se pourvoient en cassation contre cet arrêt ; <br/>
<br/>
              2. Considérant, en premier lieu, qu'il est soutenu que la cour aurait entaché d'irrégularité son arrêt en omettant de répondre à deux moyens soulevés devant elle par M. C..., qui intervenait au soutien des conclusions de M. B...et de la société Gestion Camping Caravaning ; que, toutefois, d'une part, M. C...faisait valoir que l'arrêté du 29 mars 2001 du maire de Saint-Raphaël consentant une délégation à M.D..., adjoint au maire et signataire de l'arrêté du 21 février 2005 prescrivant l'ouverture de l'enquête publique préalable à l'approbation du nouveau document d'urbanisme, n'avait pas fait l'objet d'une " publicité complète ", faute de publication au recueil des actes administratifs de la commune, prévue par l'article L. 2122-29 du code général des collectivités territoriales ; que la cour a répondu à ce moyen, en estimant notamment que la publication dans le recueil des actes administratifs de la commune ne constituait pas l'unique mode de publicité des actes réglementaires ; que, d'autre part, M. C...soutenait que l'emplacement réservé en litige aurait dû être justifié dans le plan local d'urbanisme et mentionné dans le règlement de la zone UD ; que la cour, en relevant que la commune n'avait pas à justifier d'un projet précis et déjà élaboré, a répondu à la première branche de ce moyen ; que, par ailleurs, la seconde branche du moyen était inopérante, les emplacements réservés étant sans lien avec les dispositions réglementaires propres à chacune des zones définies par un plan local d'urbanisme ; que, par suite, la cour n'était pas tenue d'y répondre ; <br/>
<br/>
              3. Considérant, en deuxième lieu, que les arrêtés du maire consentant, en application des dispositions de l'article L. 2122-18 du code général des collectivités territoriales, des délégations aux adjoints doivent définir avec une précision suffisante les limites de ces délégations ; qu'en estimant que la délégation accordée le 29 mars 2001 par le maire de Saint-Raphaël à M. D...répondait à ces exigences en mentionnant toutes les questions d'urbanisme, la cour a suffisamment motivé son arrêt, n'a pas commis d'erreur de droit et n'a pas dénaturé les pièces du dossier ; que, par ailleurs, en précisant, pour  écarter le moyen tiré de ce que l'absence de  publication de cette délégation au recueil des actes administratifs de la commune rendait l'arrêté du 29 mars 2001 inopposable aux tiers, que la publication dans le recueil des actes administratifs de la commune ne constituait pas l'unique mode de publicité des actes réglementaires, la cour n'a pas davantage commis d'erreur de droit et n'a pas dénaturé les faits qui lui étaient soumis ;<br/>
<br/>
              4. Considérant, en troisième lieu, qu'aux termes du second alinéa de l'article L. 123-9 du code de l'urbanisme, dans sa rédaction alors en vigueur : " Le conseil municipal arrête le projet de plan local d'urbanisme. Celui-ci est alors soumis pour avis aux personnes publiques associées à son élaboration ainsi que, à leur demande, aux communes limitrophes, aux établissements publics de coopération intercommunale directement intéressés, ainsi qu'à l'établissement public chargé d'un schéma de cohérence territoriale dont la commune est limitrophe, lorsqu'elle n'est pas couverte par un tel schéma. Ces personnes donnent un avis dans les limites de leurs compétences propres, au plus tard trois mois après transmission du projet de plan ; à défaut, ces avis sont réputés favorables " ; qu'en vertu de l'article L. 123-7 du même code, les services de l'Etat peuvent être associés à l'élaboration du plan local d'urbanisme ; que selon l'article L. 123-10 du même code, le projet de plan local d'urbanisme est soumis à enquête publique, le dossier soumis à l'enquête comprenant, en annexe, les avis des personnes publiques consultées ; qu'il résulte de la combinaison de ces dispositions qu'il appartient à une commune souhaitant modifier son projet de plan local d'urbanisme avant l'ouverture de l'enquête publique, notamment pour tenir compte de l'avis rendu par une personne publique associée à son élaboration, de consulter à nouveau l'ensemble des personnes publiques associées, afin que le dossier soumis à l'enquête publique comporte des avis correspondant au projet modifié ; que, toutefois, l'omission de cette nouvelle consultation n'est de nature à vicier la procédure et à entacher d'illégalité la décision prise à l'issue de l'enquête publique que si elle a pu avoir pour effet de nuire à l'information du public ou si elle a été de nature à exercer une influence sur cette décision ; <br/>
<br/>
              5. Considérant que, pour écarter le moyen tiré de ce que la délibération attaquée avait été prise à l'issue d'une procédure irrégulière dès lors que le projet de plan local d'urbanisme avait été modifié le 14 février 2005, avant l'ouverture de l'enquête publique, pour tenir compte des observations du préfet du Var, sans être soumis de nouveau pour avis aux personnes publiques associées, la cour administrative d'appel de Marseille s'est fondée sur la circonstance que les appelants ne démontraient pas que les modifications demandées par le préfet étaient substantielles ; qu'il résulte de ce qui a été dit au point 4 qu'elle a ainsi commis une erreur de droit ; que, toutefois, l'absence de nouvelle consultation des personnes publiques associées ne peut, en tout état de cause, avoir d'incidence que sur la légalité des dispositions du plan local d'urbanisme, si elles sont divisibles de l'ensemble, qui ont été affectées par les modifications auxquelles il a été procédé après une première consultation ; qu'il ressort des pièces du dossier soumis à la cour que les modifications apportées n'affectaient ni le projet de plan local d'urbanisme dans son ensemble, ni la création de l'emplacement réservé n° 142, seule contestée par les requérants, ni des dispositions du plan qui en auraient été indivisibles ; que, par suite, le moyen soulevé devant la cour, tiré du défaut de nouvelle consultation des personnes publiques associées, était, en tout état de cause, insusceptible d'avoir une incidence sur la légalité des dispositions du plan contestées devant les juges du fond ; qu'il convient de l'écarter pour ce motif, qui doit être substitué au motif retenu par les juges du fond ;<br/>
<br/>
              6. Considérant, en dernier lieu, qu'aux termes de l'article L. 123-2 du code de l'urbanisme, dans sa rédaction applicable en l'espèce : " Dans les zones urbaines ou à urbaniser, le plan local d'urbanisme peut instituer des servitudes consistant : (...) c) A indiquer la localisation prévue et les caractéristiques des voies et ouvrages publics, ainsi que les installations d'intérêt général et les espaces verts à créer ou à modifier, en délimitant les terrains qui peuvent être concernés par ces équipements " ; qu'en jugeant que la création de l'emplacement réservé litigieux en vue de l'aménagement d'un espace public n'était entachée, compte tenu notamment de la situation de la parcelle, et même en l'absence de projet précis, ni d'erreur manifeste d'appréciation, ni de détournement de pouvoir, la cour a porté sur les faits qui lui étaient soumis une appréciation souveraine exempte de dénaturation ; <br/>
<br/>
              7. Considérant qu'il résulte de tout ce qui précède que M. B...et la société Gestion Camping Caravaning ne sont pas fondés à demander l'annulation de l'arrêt qu'ils attaquent ; que leurs conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ne peuvent, dès lors, qu'être rejetées ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de M. B...et de la société Gestion Camping Caravaning le versement à la commune de Saint-Raphaël d'une somme de 1 500 euros pour chacun, au titre de ces mêmes dispositions ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la société Gestion Camping Caravaning et de M. B...est rejeté. <br/>
Article 2 : M. B...et la société Gestion Camping Caravaning verseront à la commune de Saint-Raphaël une somme de 1 500 euros chacun au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée la société Gestion Camping Caravaning, à M. A...B...et à la commune de Saint-Raphaël.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-05-03 PROCÉDURE. INCIDENTS. INTERVENTION. - INTERVENTION EN DEMANDE - MOYEN TIRÉ EN CASSATION PAR L'APPELANT DE CE QUE LE JUGE D'APPEL AURAIT OMIS DE RÉPONDRE À UN MOYEN SOULEVÉ DANS CETTE INTERVENTION AU SOUTIEN DE SES CONCLUSIONS - OPÉRANCE - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-07-01-04-03 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. QUESTIONS GÉNÉRALES. MOYENS. MOYENS INOPÉRANTS. - ABSENCE - MOYEN TIRÉ EN CASSATION PAR L'APPELANT DE CE QUE LE JUGE D'APPEL AURAIT OMIS DE RÉPONDRE À UN MOYEN SOULEVÉ DANS UNE INTERVENTION AU SOUTIEN DE SES CONCLUSIONS.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">68-01-01-01-01-03 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PLANS D'AMÉNAGEMENT ET D'URBANISME. PLANS D`OCCUPATION DES SOLS (POS) ET PLANS LOCAUX D'URBANISME (PLU). LÉGALITÉ DES PLANS. PROCÉDURE D'ÉLABORATION. ADOPTION DU PROJET. - CONSULTATION DES PERSONNES PUBLIQUES ASSOCIÉES À L'ÉLABORATION DU PLAN (ART. L. 123-9 DU CODE DE L'URBANISME) - MODIFICATION DU PROJET POUR TENIR COMPTE, AVANT L'ENQUÊTE PUBLIQUE, D'UN AVIS ÉMIS LORS DE CETTE CONSULTATION - OBLIGATION DE CONSULTER À NOUVEAU L'ENSEMBLE DES PERSONNES PUBLIQUES SUR LE PROJET MODIFIÉ - 1) EXISTENCE - 2) OMISSION - CONSÉQUENCE - IRRÉGULARITÉ DE LA PROCÉDURE ET ILLÉGALITÉ DE LA DÉCISION PRISE À L'ISSUE DE L'ENQUÊTE PUBLIQUE - CONDITIONS - OMISSION AYANT NUI À L'INFORMATION DU PUBLIC OU ÉTÉ DE NATURE À EXERCER UNE INFLUENCE SUR LA DÉCISION.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">68-01-01-01-01-05 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PLANS D'AMÉNAGEMENT ET D'URBANISME. PLANS D`OCCUPATION DES SOLS (POS) ET PLANS LOCAUX D'URBANISME (PLU). LÉGALITÉ DES PLANS. PROCÉDURE D'ÉLABORATION. ENQUÊTE PUBLIQUE. - MODIFICATION DU PROJET POUR TENIR COMPTE, AVANT L'ENQUÊTE PUBLIQUE, D'UN AVIS ÉMIS LORS DE LA CONSULTATION DES PERSONNES PUBLIQUES ASSOCIÉES À L'ÉLABORATION DU PLAN (ART. L. 123-9 DU CODE DE L'URBANISME) - OBLIGATION DE CONSULTER À NOUVEAU L'ENSEMBLE DES PERSONNES PUBLIQUES SUR LE PROJET MODIFIÉ AFIN QUE LEURS AVIS FIGURENT DANS LE DOSSIER D'ENQUÊTE - 1) EXISTENCE - 2) OMISSION - CONSÉQUENCE - IRRÉGULARITÉ DE LA PROCÉDURE ET ILLÉGALITÉ DE LA DÉCISION PRISE À L'ISSUE DE L'ENQUÊTE PUBLIQUE - CONDITIONS - OMISSION AYANT NUI À L'INFORMATION DU PUBLIC OU ÉTÉ DE NATURE À EXERCER UNE INFLUENCE SUR LA DÉCISION.
</SCT>
<ANA ID="9A"> 54-05-03 Le moyen tiré par l'appelant de ce que le juge d'appel aurait omis de répondre à un moyen soulevé dans une intervention au soutien de ses conclusions est opérant devant le juge de cassation.</ANA>
<ANA ID="9B"> 54-07-01-04-03 Le moyen tiré par l'appelant de ce que le juge d'appel aurait omis de répondre à un moyen soulevé dans une intervention au soutien de ses conclusions est opérant devant le juge de cassation.</ANA>
<ANA ID="9C"> 68-01-01-01-01-03 1) Il appartient à une commune souhaitant modifier son projet de plan local d'urbanisme avant l'ouverture de l'enquête publique, notamment pour tenir compte de l'avis rendu par une personne publique associée à son élaboration, de consulter à nouveau l'ensemble des personnes publiques associées, afin que le dossier soumis à l'enquête publique comporte des avis correspondant au projet modifié.,,,2) Toutefois, l'omission de cette nouvelle consultation n'est de nature à vicier la procédure et à entacher d'illégalité la décision prise à l'issue de l'enquête publique que si elle a pu avoir pour effet de nuire à l'information du public ou si elle a été de nature à exercer une influence sur cette décision.</ANA>
<ANA ID="9D"> 68-01-01-01-01-05 1) Il appartient à une commune souhaitant modifier son projet de plan local d'urbanisme avant l'ouverture de l'enquête publique, notamment pour tenir compte de l'avis rendu par une personne publique associée à son élaboration, de consulter à nouveau l'ensemble des personnes publiques associées, afin que le dossier soumis à l'enquête publique comporte des avis correspondant au projet modifié.,,,2) Toutefois, l'omission de cette nouvelle consultation n'est de nature à vicier la procédure et à entacher d'illégalité la décision prise à l'issue de l'enquête publique que si elle a pu avoir pour effet de nuire à l'information du public ou si elle a été de nature à exercer une influence sur cette décision.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
