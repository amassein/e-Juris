<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037600030</ID>
<ANCIEN_ID>JG_L_2018_11_000000421302</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/60/00/CETATEXT000037600030.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 6ème chambres réunies, 09/11/2018, 421302</TITRE>
<DATE_DEC>2018-11-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>421302</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>M. Alain Seban</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:421302.20181109</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La SARL " Les Sept Monts Equitation " a demandé au juge des référés du tribunal administratif de Grenoble d'ordonner, sur le fondement de l'article L. 521-1 du code de justice administrative, la suspension de l'exécution de la décision du 12 février 2018 de l'inspecteur de la santé publique vétérinaire portant retrait de divers animaux de son centre équestre au titre de la protection animale. Par une ordonnance n° 1802302 du 22 mai 2018, le juge des référés a fait droit à cette demande.<br/>
<br/>
              Par un pourvoi, enregistré au secrétariat du contentieux du Conseil d'Etat le 7 juin 2018, le ministre de l'agriculture et de l'alimentation demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de rejeter la demande présentée par la SARL " Les Sept Monts Equitation ".<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - le code rural et de la pêche maritime ;<br/>
<br/>
              - le code de procédure pénale ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Alain Seban, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de la SARL " Les Sept Monts Equitation ".<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article L. 214-23 du code rural et de la pêche maritime : " I. - Pour l'exercice des inspections, des contrôles et des interventions de toute nature qu'implique l'exécution des mesures de protection des animaux prévues aux articles L. 214-3 à L. 214-18, L. 215-10 et L. 215-11, des règlements communautaires ayant le même objet et des textes pris pour leur application, les fonctionnaires et agents habilités à cet effet : / 1° Ont accès aux locaux et aux installations où se trouvent des animaux, à l'exclusion des domiciles et de la partie des locaux à usage de domicile, entre 8 heures et 20 heures ou en dehors de ces heures lorsque l'accès au public est autorisé ou lorsqu'une activité est en cours ; (...) / II. - Dans l'attente de la mesure judiciaire prévue à l'article 99-1 du code de procédure pénale, les agents qui sont mentionnés au I de l'article L. 205-1 et au I du présent article peuvent ordonner la saisie ou le retrait des animaux et, selon les circonstances de l'infraction et l'urgence de la situation, les confier à un tiers, notamment à une fondation ou à une association de protection animale reconnue d'utilité publique ou déclarée, pour une durée qui ne peut excéder trois mois ou les maintenir sous la garde du saisi. (...) " ; qu'aux termes des premier et dernier alinéas de l'article 99-1 du code de procédure pénale : " Lorsque, au cours d'une procédure judiciaire ou des contrôles mentionnés à l'article L. 214-23 du code rural et de la pêche maritime, il a été procédé à la saisie ou au retrait, à quelque titre que ce soit, d'un ou plusieurs animaux vivants, le procureur de la République près le tribunal de grande instance du lieu de l'infraction ou, lorsqu'il est saisi, le juge d'instruction peut placer l'animal dans un lieu de dépôt prévu à cet effet ou le confier à une fondation ou à une association de protection animale reconnue d'utilité publique ou déclarée. La décision mentionne le lieu de placement et vaut jusqu'à ce qu'il ait été statué sur l'infraction. / (...) / Lorsque, au cours de la procédure judiciaire, la conservation de l'animal saisi ou retiré n'est plus nécessaire à la manifestation de la vérité et que l'animal est susceptible de présenter un danger grave et immédiat pour les personnes ou les animaux domestiques, le procureur de la République ou le juge d'instruction lorsqu'il est saisi ordonne la remise de l'animal à l'autorité administrative afin que celle-ci mette en oeuvre les mesures prévues au II de l'article L. 211-11 du code rural et de la pêche maritime." ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis au juge des référés que la SARL " Les Sept Monts Equitation " exploite sur le territoire de la commune de Samoëns (Haute-Savoie), au lieu-dit " La Dent ", une activité de centre équestre, une activité de pension pour chevaux et une mini-ferme comprenant des caprins, ânes, lapins et volailles ; qu'à la suite de visites d'inspection réalisées les 10 janvier et 12 février 2018 par des agents de la direction départementale de la protection des populations, qui ont mis en évidence diverses infractions aux règles relatives à la détention des animaux, analogues à celles qui avaient été constatées lors d'une précédente visite en juin 2017, l'inspecteur de la santé publique vétérinaire a, par une décision prise sur le fondement de l'article L. 214-23 du code rural et de la pêche maritime, ordonné le retrait des animaux et les a confiés à la garde de tiers habilités ; que la SARL " Les Sept Monts Equitation " a demandé au juge des référés du tribunal administratif de Grenoble de prononcer, sur le fondement de l'article L. 521-1 du code de justice administrative, la suspension de l'exécution de cette décision ; que, par l'ordonnance attaquée du 22 mai 2018, le juge des référés a fait droit à cette demande ;<br/>
<br/>
              3. Considérant que la décision par laquelle, en application de l'article L. 214-23 du code rural et de la pêche maritime, l'autorité compétente décide, après la constatation d'une infraction réprimée par les articles L. 215-10 ou L. 215-11 du même code, de saisir ou de retirer des animaux et d'en confier la garde à un tiers " dans l'attente de la mesure judiciaire prévue à l'article 99-1 du code de procédure pénale ", a le caractère d'une mesure de police judiciaire dont la connaissance n'appartient qu'aux juridictions de l'ordre judiciaire ; qu'ainsi, faute de retenir que la demande dont il était saisi ne relevait manifestement pas de la compétence de la juridiction administrative, le juge des référés a commis une erreur de droit ; que son ordonnance doit, pour ce motif et sans qu'il soit besoin d'examiner les moyens du pourvoi, être annulée ;<br/>
<br/>
              4. Considérant qu'il y a lieu de faire application de l'article L. 821-1 du code de justice administrative et de régler l'affaire au titre de la procédure de référé ;<br/>
<br/>
              5. Considérant qu'ainsi qu'il a été dit au point 3, la demande présentée par la SARL " Les Sept Monts Equitation " devant le juge des référés du tribunal administratif de Grenoble ne relève manifestement pas de la compétence de la juridiction administrative et doit par suite, être rejetée comme portée devant un ordre de juridiction incompétent pour en connaître ;<br/>
<br/>
              6. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise, à ce titre, à la charge de l'Etat, qui n'est pas la partie perdante dans la présente instance ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du 22 mai 2018 du juge des référés du tribunal administratif de Grenoble est annulée.<br/>
<br/>
Article 2 : La demande présentée par la SARL " Les Sept Monts Equitation " devant le juge des référés du tribunal administratif de Grenoble est rejetée comme portée devant un ordre de juridiction incompétent pour en connaître.<br/>
<br/>
Article 3 : Les conclusions présentées par la SARL " Les Sept Monts Equitation " au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 4 : La présente décision sera notifiée au ministre de l'agriculture et la SARL " Les Sept Monts Equitation ".<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">03-05-03-01 AGRICULTURE ET FORÊTS. PRODUITS AGRICOLES. ÉLEVAGE ET PRODUITS DE L'ÉLEVAGE. ÉLEVAGE. - LITIGE RELATIF À LA SAISIE OU AU RETRAIT D'ANIMAUX SUITE À UNE MESURE D'INSPECTION ET DE CONTRÔLE EN VUE DE LEUR PROTECTION (II DE L'ART. L. 214-23 DU CRPM) - COMPÉTENCE DU JUGE JUDICIAIRE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">17-03-02-07-05-02 COMPÉTENCE. RÉPARTITION DES COMPÉTENCES ENTRE LES DEUX ORDRES DE JURIDICTION. COMPÉTENCE DÉTERMINÉE PAR UN CRITÈRE JURISPRUDENTIEL. PROBLÈMES PARTICULIERS POSÉS PAR CERTAINES CATÉGORIES DE SERVICES PUBLICS. SERVICE PUBLIC JUDICIAIRE. FONCTIONNEMENT. - LITIGE RELATIF À LA SAISIE OU AU RETRAIT D'ANIMAUX SUITE À UNE MESURE D'INSPECTION ET DE CONTRÔLE EN VUE DE LEUR PROTECTION (II DE L'ART. L. 214-23 DU CRPM) - COMPÉTENCE DU JUGE JUDICIAIRE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">49-01-02 POLICE. POLICE ADMINISTRATIVE ET JUDICIAIRE. NOTION DE POLICE JUDICIAIRE. - SAISIE OU RETRAIT D'ANIMAUX SUITE À UNE MESURE D'INSPECTION ET DE CONTRÔLE EN VUE DE LEUR PROTECTION (II DE L'ART. L. 214-23 DU CRPM) - MESURE DE POLICE JUDICIAIRE - INCLUSION.
</SCT>
<ANA ID="9A"> 03-05-03-01 La décision par laquelle, en application de l'article L. 214-23 du code rural et de la pêche maritime (CRPM), l'autorité compétente décide, après la constatation d'une infraction réprimée par les articles L. 215-10 ou L. 215-11 du même code, de saisir ou de retirer des animaux et d'en confier la garde à un tiers dans l'attente de la mesure judiciaire prévue à l'article 99-1 du code de procédure pénale, a le caractère d'une mesure de police judiciaire dont la connaissance n'appartient qu'aux juridictions de l'ordre judiciaire.</ANA>
<ANA ID="9B"> 17-03-02-07-05-02 La décision par laquelle, en application de l'article L. 214-23 du code rural et de la pêche maritime (CRPM), l'autorité compétente décide, après la constatation d'une infraction réprimée par les articles L. 215-10 ou L. 215-11 du même code, de saisir ou de retirer des animaux et d'en confier la garde à un tiers dans l'attente de la mesure judiciaire prévue à l'article 99-1 du code de procédure pénale, a le caractère d'une mesure de police judiciaire dont la connaissance n'appartient qu'aux juridictions de l'ordre judiciaire.</ANA>
<ANA ID="9C"> 49-01-02 La décision par laquelle, en application de l'article L. 214-23 du code rural et de la pêche maritime (CRPM), l'autorité compétente décide, après la constatation d'une infraction réprimée par les articles L. 215-10 ou L. 215-11 du même code, de saisir ou de retirer des animaux et d'en confier la garde à un tiers dans l'attente de la mesure judiciaire prévue à l'article 99-1 du code de procédure pénale, a le caractère d'une mesure de police judiciaire dont la connaissance n'appartient qu'aux juridictions de l'ordre judiciaire.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
