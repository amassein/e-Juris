<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025469046</ID>
<ANCIEN_ID>JG_L_2012_03_000000334575</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/46/90/CETATEXT000025469046.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème et 8ème sous-sections réunies, 09/03/2012, 334575, Publié au recueil Lebon</TITRE>
<DATE_DEC>2012-03-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>334575</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème et 8ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Christian Vigouroux</PRESIDENT>
<AVOCATS>SCP DIDIER, PINET ; SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>Mme Christine Allais</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Geffray</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:334575.20120309</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 14 décembre 2009 et 15 mars 2010 au secrétariat du contentieux du Conseil d'Etat, présentés pour la SCEA BARONNE GUICHARD, dont le siège est Château Siaurac à Néac (33500), la SCEA VIGNOBLES SILVESTRINI, dont le siège est lieu-dit Chereau à Lussac (33570), l'EARL VINS BEL, dont le siège est lieu-dit Le Poitou à Lussac (33570), M. Jean-Paul A, demeurant ..., la SCEA VIGNOBLES DANIEL YBERT, dont le siège est lieu-dit La Rose à Saint-Emilion (33330), la SOCIETE C. ESTRAGER et FILS, dont le siège est Château Fougeailles à Néac (33500), la SCEA VIGNOBLES MICHEL COUDROY, dont le siège est Maison Neuve à Montagne (33570), le GFA CHATEAU HAUT SURGET, dont le siège est lieu-dit Chevrol à Néac (33500), la SCEA VIGNOBLES TROCARD, dont le siège est 2 Les Petis Jays Ouest à Les Artigues de Lussac (33570) ; la SCEA BARONNE GUICHARD et autres demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2009-1237 du 14 octobre 2009 relatif aux appellations d'origine contrôlées " Pomerol ", " Coteaux de Die ", " Châtillon-en-Diois ", " Crémant de Limoux ", " Limoux ", " Crémant de Die " et " Clairette de Die " en tant que, en homologuant les dispositions du cahier des charges de l'appellation d'origine contrôlée (AOC) " Pomerol " relatives à l'aire de proximité immédiate de cette AOC, il retire à terme aux viticulteurs le droit d'effectuer les opérations de vinification sur le territoire d'autres communes que Pomerol ou à l'extérieur de la zone délimitée du territoire des communes de Libourne et de Lalande-de-Pomerol ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat une somme de 10 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et son premier protocole additionnel ;<br/>
<br/>
              Vu le règlement (CE) n° 479/2008 du Conseil du 29 avril 2008 portant organisation commune du marché vitivinicole ;<br/>
<br/>
              Vu le règlement (CE) n° 607/2009 de la Commission du 14 juillet 2009 fixant certaines modalités d'application du règlement (CE) n° 479/2008 du Conseil en ce qui concerne les appellations d'origine protégées et les indications géographiques protégées, les mentions traditionnelles, l'étiquetage et la présentation de certains produits du secteur vitivinicole ;<br/>
<br/>
              Vu le code de la consommation ;<br/>
<br/>
              Vu le code rural ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Christine Allais, chargée des fonctions de Maître des Requêtes,<br/>
<br/>
              - les observations de la SCP Waquet, Farge, Hazan, avocat de la SOCIETE SCEA BARONNE GUICHARD et autres et de la SCP Didier, Pinet, avocat de l'Institut national de l'origine et de la qualité, <br/>
<br/>
              - les conclusions de M. Edouard Geffray, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Waquet, Farge, Hazan, avocat de la SOCIETE SCEA BARONNE GUICHARD et autres et à la SCP Didier, Pinet, avocat de l'Institut national de l'origine et de la qualité, <br/>
<br/>
<br/>
<br/>
              Considérant que selon les définitions que donne l'article 34 du règlement (CE) n° 479/2008 du Conseil du 29 avril 2008, on entend par " appellation d'origine ", le nom d'une région, d'un lieu déterminé ou, dans des cas exceptionnels, d'un pays, qui sert à désigner un produit dont la qualité et les caractéristiques sont dues essentiellement ou exclusivement à un milieu géographique particulier et aux facteurs naturels et humains qui lui sont inhérents, qui, s'agissant d'un vin, est élaboré exclusivement à partir de raisins provenant de la zone géographique considérée et dont la production est limitée à la zone géographique désignée ; que l'article 6 du règlement (CE) n° 607/2009 de la Commission du 14 juillet 2009 dispose que, par dérogation à l'article 34 du règlement (CE) n° 479/2008, " sous réserve que le cahier des charges le prévoie, un produit bénéficiant d'une appellation d'origine protégée ou d'une indication géographique protégée peut être transformé en vin : / a) dans une zone à proximité immédiate de la zone délimitée... " ; <br/>
<br/>
              Considérant que le décret du 8 décembre 1936 relatif à l'appellation d'origine contrôlée (AOC) Pomerol, abrogé par le décret attaqué, définissait une aire géographique de récolte et se bornait à préciser, s'agissant de la vinification, qu'elle devait être conforme aux usages locaux ; qu'il ressort des pièces du dossier que de nombreux producteurs de vins bénéficiaires de l'AOC " Pomerol " procèdent aux opérations de vinification et d'élevage de leurs vins en dehors de l'aire géographique de production, en vertu d'usages anciens et, depuis 1998, d'autorisations délivrées à titre dérogatoire par l'Institut national de l'origine et de la qualité (INAO) ; que le cahier des charges homologué par le décret attaqué a utilisé la faculté ouverte par le règlement précité du 14 juillet 2009 pour délimiter une aire de proximité immédiate au sein de laquelle sont autorisés, à titre dérogatoire, la vinification et l'élevage des vins en dehors de l'aire géographique de production ; qu'une  telle délimitation doit être justifiée par des critères objectifs et rationnels  et n'introduire aucune différence de traitement entre producteurs qui ne corresponde à une différence de situation ou à un motif d'intérêt général en rapport avec les objectifs poursuivis ;<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier que seules deux parcelles où sont installés des chais, proches de l'aire géographique de l'AOC " Pomerol ", ont été incluses dans l'aire de proximité immédiate ; qu'ont été en revanche exclues de l'aire de proximité immédiate des parcelles, situées à des distances de l'ordre de 1 à 7 kilomètres de l'aire géographique, sur lesquelles sont situés les chais utilisés de longue date par les requérants pour procéder aux opérations de vinification des raisins qu'ils produisent à l'intérieur de l'aire géographique et à l'élevage des vins issus de ces raisins ; que le cahier des charges prévoit toutefois que les exploitants dont les chais sont implantés en dehors de l'aire de proximité immédiate pourront continuer à vinifier leurs raisins et élever leurs vins en dehors de cette aire jusqu'à la récolte de 2018 incluse pour la vinification et la récolte 2025 incluse pour l'élevage des vins ;<br/>
<br/>
              Considérant que si le ministre de l'agriculture et l'INAO motivent cette nouvelle délimitation, qui aura pour effet de retirer à des vignerons producteurs de vins bénéficiant depuis des décennies de l'AOC " Pomerol " la possibilité de procéder à la vinification et à l'élevage de leurs vins en dehors de l'aire géographique, par la nécessité de limiter le transport et la manipulation du vin afin de préserver sa qualité, il ne ressort pas des pièces du dossier que le transport sur les distances mentionnées ci-dessus des grappes de raisins, et non du vin, entre le lieu de la récolte et celui du chais de vinification aurait une incidence sur la qualité du vin produit, alors qu'il est par ailleurs constant que certains exploitants sont amenés à transporter leur vendange à l'intérieur de l'aire géographique de production sur des distances parfois plus longues que celles sur lesquelles les requérants transportent leur récolte ; qu'au regard du seul motif invoqué par le ministre de l'agriculture, les requérants sont fondés à soutenir que la délimitation ainsi retenue de l'aire dite de proximité immédiate induit une différence de traitement entre des exploitants qui bénéficiaient jusque là pour leur production de vin de l'AOC " Pomerol ", les uns conservant ce bénéfice parce que leurs chais sont implantés sur les parcelles comprises dans cette zone et les autres se voyant retirer ce bénéfice à terme, et qu'il n'apparaît pas que cette différence de traitement serait en rapport avec les objectifs du cahier des charges ; que, dès lors, et sans qu'il soit besoin d'examiner les autres moyens de leur requête, les requérants sont fondés à demander l'annulation du décret attaqué homologuant le cahier des charges de l'appellation d'origine contrôlée (AOC) " Pomerol ", en tant qu'il a homologué la délimitation de l'aire de proximité immédiate ; <br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement aux requérants d'une somme globale de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ; qu'en revanche, les dispositions de ce même article font obstacle à ce que soit mis à la charge des requérants le versement de la somme demandée par l'INAO au titre des frais exposés par lui et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le décret n° 2009-1237 du 14 octobre 2009 est annulé en tant qu'il homologue les dispositions du cahier des charges de l'appellation d'origine contrôlée (AOC) " Pomerol " délimitant l'aire de proximité immédiate de cette AOC. <br/>
<br/>
Article 2 : L'Etat versera une somme globale de 3 000 euros à la SCEA BARONNE GUICHARD, à la SCEA VIGNOBLES SILVESTRINI, à l'EARL VINS BEL, à M. Jean-Paul A, à la SCEA VIGNOBLES DANIEL YBERT, à la SOCIETE C. ESTRAGER et FILS, à la SCEA VIGNOBLES MICHEL COUDROY, au GFA CHATEAU HAUT SURGET et à la SCEA VIGNOBLES TROCARD au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : Les conclusions présentées par l'INAO au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à la SCEA BARONNE GUICHARD, premier requérant dénommé, au Premier ministre, au ministre de l'agriculture, de l'alimentation, de la pêche, de la ruralité et de l'aménagement du territoire, au ministre de l'économie, des finances et de l'industrie, à la ministre du budget, des comptes publics et de la réforme de l'Etat, porte-parole du gouvernement et à l'Institut national de l'origine et de la qualité. Les autres requérants seront informés de la présente décision par la SCP Waquet-Farge-Hazan, avocat au Conseil d'Etat et à la Cour de cassation, qui les représente devant le Conseil d'Etat.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-04-03-01 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. PRINCIPES GÉNÉRAUX DU DROIT. ÉGALITÉ DEVANT LA LOI. - DÉLIMITATION D'UNE AIRE DE PROXIMITÉ IMMÉDIATE - 1) OBLIGATION DE RESPECTER DES CRITÈRES OBJECTIFS ET RATIONNELS, ET DE RESPECTER LE PRINCIPE D'ÉGALITÉ - EXISTENCE [RJ1] - 2) MÉCONNAISSANCE DE CE PRINCIPE EN L'ESPÈCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">03-05-06-02 AGRICULTURE, CHASSE ET PÊCHE. PRODUITS AGRICOLES. VINS. CONTENTIEUX DES APPELLATIONS. - DÉLIMITATION D'UNE AIRE DE PROXIMITÉ IMMÉDIATE - 1) OBLIGATION DE RESPECTER DES CRITÈRES OBJECTIFS ET RATIONNELS, ET DE RESPECTER LE PRINCIPE D'ÉGALITÉ - EXISTENCE [RJ1] - 2) MÉCONNAISSANCE DE CE PRINCIPE EN L'ESPÈCE.
</SCT>
<ANA ID="9A"> 01-04-03-01 1) En utilisant pour la réglementation d'une appellation d'origine contrôlée la faculté, ouverte par l'article 6 du règlement (CE) n° 607/2009 de la Commission du 14 juillet 2009, de délimiter une aire de proximité immédiate au sein de laquelle sont autorisés, à titre dérogatoire, la vinification et l'élevage des vins en dehors de l'aire géographique de production, le pouvoir réglementaire doit procéder à une délimitation qui soit justifiée par des critères objectifs et rationnels et n'introduire aucune différence de traitement entre producteurs qui ne corresponde à une différence de situation ou à un motif d'intérêt général en rapport avec les objectifs poursuivis.,,2) En l'espèce, en excluant de l'aire de proximité immédiate des parcelles, situées à des distances de l'ordre de 1 à 7 kilomètres de l'aire géographique, sur lesquelles sont situés des chais utilisés de longue date pour procéder aux opérations de vinification des raisins qu'ils produisent à l'intérieur de l'aire géographique et à l'élevage des vins issus de ces raisins, et en justifiant cette exclusion par la nécessité de limiter le transport et la manipulation du vin afin de préserver sa qualité, alors qu'il ne ressort pas des pièces du dossier que le transport sur les distances mentionnées ci-dessus des grappes de raisins, et non du vin, entre le lieu de la récolte et celui du chais de vinification aurait une incidence sur la qualité du vin produit, et qu'il est par ailleurs constant que certains exploitants sont amenés à transporter leur vendange à l'intérieur de l'aire géographique de production sur des distances parfois plus longues que celles sur lesquelles les requérants transportent leur récolte, la délimitation retenue de l'aire dite de proximité immédiate induit une différence de traitement sans rapport avec les objectifs du cahier des charges.</ANA>
<ANA ID="9B"> 03-05-06-02 1) En utilisant pour la réglementation d'une appellation d'origine contrôlée la faculté, ouverte par l'article 6 du règlement (CE) n° 607/2009 de la Commission du 14 juillet 2009, de délimiter une aire de proximité immédiate au sein de laquelle sont autorisés, à titre dérogatoire, la vinification et l'élevage des vins en dehors de l'aire géographique de production, le pouvoir réglementaire doit procéder à une délimitation qui soit justifiée par des critères objectifs et rationnels et n'introduire aucune différence de traitement entre producteurs qui ne corresponde à une différence de situation ou à un motif d'intérêt général en rapport avec les objectifs poursuivis.,,2) En l'espèce, en excluant de l'aire de proximité immédiate des parcelles, situées à des distances de l'ordre de 1 à 7 kilomètres de l'aire géographique, sur lesquelles sont situés des chais utilisés de longue date pour procéder aux opérations de vinification des raisins qu'ils produisent à l'intérieur de l'aire géographique et à l'élevage des vins issus de ces raisins, et en justifiant cette exclusion par la nécessité de limiter le transport et la manipulation du vin afin de préserver sa qualité, alors qu'il ne ressort pas des pièces du dossier que le transport sur les distances mentionnées ci-dessus des grappes de raisins, et non du vin, entre le lieu de la récolte et celui du chais de vinification aurait une incidence sur la qualité du vin produit, et qu'il est par ailleurs constant que certains exploitants sont amenés à transporter leur vendange à l'intérieur de l'aire géographique de production sur des distances parfois plus longues que celles sur lesquelles les requérants transportent leur récolte, la délimitation retenue de l'aire dite de proximité immédiate induit une différence de traitement sans rapport avec les objectifs du cahier des charges.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. pour la délimitation des zones franches, CE, 19 mai 1999, Autour et autres, n° 185765, T. pp. 605-679-727 ; pour les AOC, CE, 6 mars 2002, M. et Mme Loulière, n° 226248, inédite au Recueil.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
