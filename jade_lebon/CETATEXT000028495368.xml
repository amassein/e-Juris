<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028495368</ID>
<ANCIEN_ID>JG_L_2014_01_000000357515</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/49/53/CETATEXT000028495368.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème et 5ème sous-sections réunies, 20/01/2014, 357515</TITRE>
<DATE_DEC>2014-01-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>357515</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème et 5ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP TIFFREAU, CORLAY, MARLANGE ; SCP GATINEAU, FATTACCINI ; SCP BARTHELEMY, MATUCHANSKY, VEXLIARD</AVOCATS>
<RAPPORTEUR>M. Christophe Eoche-Duval</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Gaëlle Dumortier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:357515.20140120</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 12 mars et 12 juin 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. B...A..., demeurant ... ; M. A...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision n° 164 du 19 octobre 2011 par laquelle le Conseil national de l'ordre des médecins, siégeant en formation restreinte, a rejeté sa demande tendant à l'annulation de la décision du 22 mars 2011 du conseil régional de l'ordre des médecins de Lorraine qui a rejeté son recours contre la décision du conseil départemental de l'ordre des médecins de Meurthe-et-Moselle du 20 octobre 2010, maintenant l'inscription au tableau de l'ordre de la société d'exercice libéral à responsabilité limitée (SELARL) de radiologie et d'imagerie médicale du 125 rue Saint Dizier, ainsi que de ces décisions ;<br/>
<br/>
              2°) d'ordonner la radiation du tableau de l'ordre de la SELARL de radiologie et d'imagerie médicale du 125 rue Saint Dizier ; <br/>
<br/>
              3°) de mettre à la charge de la SELARL de radiologie et d'imagerie médicale du 125 rue Saint Dizier la somme de 3 500 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu le code de la santé publique ;<br/>
<br/>
              Vu la loi n° 79-587 du 11 juillet 1979, modifiée notamment par la loi n° 2011-525 du 17 mai 2011 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Christophe Eoche-Duval, Conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gatineau, Fattaccini, avocat de M.A..., à la SCP Tiffreau, Corlay, Marlange, avocat de la SELARL de radiologie et d'imagerie médicale et la SCP Barthélemy, Matuchansky, Vexliard, avocat du Conseil national de l'ordre des médecins ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'en application des dispositions des articles R. 4113-4 et R. 4113-23 du code de la santé publique, la société d'exercice libéral à responsabilité limitée (SELARL) de radiologie et d'imagerie médicale du 125 rue Saint Dizier, qui exerce à Nancy, a soumis le 20 octobre 2010 au conseil départemental de l'ordre des médecins de Meurthe-et-Moselle, selon la procédure d'inscription au tableau de l'ordre, une modification de ses statuts pour exercer dans deux lieux distincts supplémentaires ; que cette modification statutaire a été approuvée par décision du conseil départemental du même jour ; que M.A..., qui exerce la radiologie à Neuves-Maisons, dans le même département, a formé contre cette décision des recours devant le conseil régional de l'ordre des médecins de Lorraine puis devant le Conseil national de l'ordre des médecins, qui a rejeté son recours par décision du 19 octobre 2011 ; <br/>
<br/>
              Sur les conclusions dirigées contre les décisions du conseil départemental de l'ordre des médecins de Meurthe-et-Moselle et du conseil régional de l'ordre des médecins de Lorraine :<br/>
<br/>
              2. Considérant que la décision du 19 octobre 2011, prise dans le cadre du mécanisme de recours prévu à l'article L. 4112-4 du code de la santé publique, s'est substituée aux décisions précédemment prises par le conseil départemental, puis par le conseil régional de l'ordre des médecins ; que, dès lors, les conclusions de M. A...dirigées contre ces deux décisions sont dépourvues d'objet et, par suite, irrecevables ;<br/>
<br/>
              Sur les conclusions dirigées contre la décision attaquée du conseil national en tant qu'elle approuve la modification des statuts pour l'exercice sur des sites distincts :<br/>
<br/>
              3. Considérant, en premier lieu, que les décisions rendues par le conseil national de l'ordre, lorsqu'il se prononce en matière d'inscription au tableau sur le recours prévu à l'article L. 4112-4 du code de la santé publique, sont des décisions administratives et non juridictionnelles ; que M. A...ne saurait donc utilement soutenir que l'article L. 9 du code de justice administrative et l'article 6 §1 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales impliquaient que le conseil national réponde à l'ensemble des griefs énoncés dans son recours administratif ; <br/>
<br/>
              4. Considérant, en deuxième lieu, que, s'il résulte du dernier alinéa ajouté à l'article 1er de la loi du 11 juillet 1979 par la loi du 17 mai 2011 que doivent être motivées les décisions qui " rejettent un recours administratif dont la présentation est obligatoire préalablement à tout recours contentieux en application d'une disposition législative ou réglementaire ", la décision attaquée, qui énonce les considérations de droit et de fait sur lesquelles elle se fonde est, contrairement à ce que soutient le requérant, suffisamment motivée ;  <br/>
<br/>
              5. Considérant, en troisième lieu, qu'aux termes de l'avant-dernier alinéa de l'article R. 4113-4 du code de la santé publique, applicable aux sociétés d'exercice libéral : " L'inscription ne peut être refusée que si les statuts ne sont pas conformes aux dispositions législatives et réglementaires en vigueur. Elle peut également être refusée dans le cas prévu à l'article L. 4113-11 ", lequel dispose que " (...) Le conseil de l'ordre peut refuser d'inscrire au tableau des candidats qui ont contracté des engagements incompatibles avec les règles de la profession ou susceptibles de priver le praticien de l'indépendance professionnelle nécessaire " ; qu'aux termes de l'article R. 4113-23 du même code dans sa rédaction applicable avant l'intervention du décret du 17 juillet 2012 : " L'activité d'une société d'exercice libéral de médecins ne peut s'effectuer que dans un lieu unique. Toutefois, par dérogation aux dispositions du code de déontologie médicale mentionnées à l'article R. 412- 85, la société peut exercer dans cinq lieux au maximum lorsque, d'une part, elle utilise des équipements implantés en des lieux différents ou met en oeuvre des techniques spécifiques et que, d'autre part, l'intérêt des malades le justifie (...) " ; <br/>
<br/>
              6. Considérant que, si ces dispositions impliquent que les instances compétentes de l'ordre vérifient que les statuts qui leur sont soumis aux fins d'ouverture d'un lieu d'exercice supplémentaire sont conformes aux dispositions législatives et réglementaires en vigueur, elles ne leur permettent pas de refuser l'inscription au tableau d'une société d'exercice libéral au motif que ses associés, à titre individuel, ou la personne morale ne respecteraient pas les dispositions des articles R. 4113-18 et R. 4127-1 du même code, lesquelles soumettent l'exercice de la profession médicale, par les associés comme par les sociétés d'exercice libéral, aux dispositions disciplinaires applicables à la profession ; que, par suite, le Conseil national de l'ordre des médecins n'a pas méconnu les règles relatives à l'inscription au tableau en se bornant, pour rejeter le recours de M.A..., à vérifier si la modification statutaire était conforme aux dispositions en vigueur ; qu'il ne ressort pas des pièces du dossier que la décision attaquée aurait été obtenue par la fraude ; <br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que les conclusions tendant à l'annulation de la décision attaquée en tant qu'elle approuve la modification des statuts de la SELARL pour l'exercice dans des sites distincts doivent être rejetées ; <br/>
<br/>
              Sur les conclusions dirigées contre la décision attaquée du conseil national en tant qu'elle rejette les " conclusions tendant à poursuivre au plan disciplinaire " les associés de la SELARL de radiologie et d'imagerie médicale du 125 rue Saint Dizier :<br/>
<br/>
              8. Considérant qu'aux termes de l'article R. 4126-1 du code de la santé publique : " L'action disciplinaire contre un médecin (...) ne peut être introduite devant la chambre disciplinaire de première instance que par l'une des personnes ou autorités suivantes : / 1° Le conseil national ou le conseil départemental de l'ordre au tableau duquel le praticien poursuivi est inscrit à la date de la saisine de la juridiction, agissant de leur propre initiative ou à la suite de plaintes, formées notamment par les patients, les organismes locaux d'assurance maladie obligatoires, les médecins-conseils chefs ou responsables du service du contrôle médical placé auprès d'une caisse ou d'un organisme de sécurité sociale, les associations de défense des droits des patients, des usagers du système de santé ou des personnes en situation de précarité, qu'ils transmettent, le cas échéant en s'y associant, dans le cadre de la procédure prévue à l'article L. 4123-2 (...) " que selon ce dernier article : " (...)Lorsqu'une plainte est portée devant le conseil départemental, son président en accuse réception à l'auteur, en informe le médecin (...) mis en cause et les convoque dans un délai d'un mois à compter de la date d'enregistrement de la plainte en vue d'une conciliation. En cas d'échec de celle-ci, il transmet la plainte à la chambre disciplinaire de première instance avec l'avis motivé du conseil dans un délai de trois mois à compter de la date d'enregistrement de la plainte, en s'y associant le cas échéant. " ; qu'aux termes de l'article R. 4113-18 du même code : " La société d'exercice libéral est soumise aux dispositions disciplinaires applicables à la profession. Elle ne peut faire l'objet de poursuites disciplinaires indépendamment de celles qui seraient intentées contre un ou plusieurs associés exerçant leur profession en son sein. "; <br/>
<br/>
              9. Considérant que, lorsque le Conseil national de l'ordre des médecins est saisi, même à l'occasion d'une procédure administrative et notamment du recours prévu à l'article L. 4112-4 du code de la santé publique, d'une plainte fondée sur des manquements allégués aux règles de déontologie, relevant des instances disciplinaires, il lui appartient soit de saisir lui-même la chambre disciplinaire de première instance compétente, soit de transmettre la plainte au conseil départemental de l'ordre compétent, afin que les suites appropriés soient données à cette plainte ; que, dès lors, le conseil national ne pouvait se borner à rejeter comme irrecevables les conclusions du requérant tendant à ce que des manquements disciplinaires qualifiés par lui d'" exercice illégal de la médecine ", mettant en cause les associés de la SELARL de radiologie et d'imagerie médicale du 125 rue Saint Dizier, soient sanctionnés ; que, par suite, M. A...est fondé à soutenir que le rejet de sa plainte, qui est divisible des autres dispositions de la décision attaquée, est entaché d'illégalité et doit être annulé ;<br/>
<br/>
              Sur les conclusions tendant à ce que le Conseil d'Etat ordonne la radiation du tableau de la SELARL de radiologie et d'imagerie médicale du 125 rue Saint Dizier :<br/>
<br/>
              10. Considérant que la présente décision n'implique pas, en tout état de cause, de faire droit aux conclusions à fin d'injonction présentées par le requérant ; <br/>
<br/>
              Sur les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              11. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées à ce titre par M. A...; que ces dispositions font obstacle à ce qu'il soit fait droit aux conclusions présentées au même titre par la SELARL de radiologie et d'imagerie médicale du 125 rue Saint Dizier et par le Conseil national de l'ordre des médecins ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La décision n° 164 du 19 octobre 2011 du Conseil national de l'ordre des médecins est annulée en tant qu'elle rejette la plainte de M.A....<br/>
Article 2 : Le surplus des conclusions de la requête de M. A...est rejeté. <br/>
Article 3 : Les conclusions de la SELARL de radiologie et d'imagerie médicale du 125 rue Saint Dizier et du Conseil national de l'ordre des médecins présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 4 : La présente décision sera notifiée à M. B...A..., à la SELARL de radiologie et d'imagerie médicale du 125 rue Saint Dizier et au Conseil national de l'ordre des médecins.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-03-01-02-01-01 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - FORME ET PROCÉDURE. QUESTIONS GÉNÉRALES. MOTIVATION. MOTIVATION OBLIGATOIRE. MOTIVATION OBLIGATOIRE EN VERTU DES ARTICLES 1 ET 2 DE LA LOI DU 11 JUILLET 1979. - DÉCISION REJETANT UN RAPO - INCLUSION - DÉCISION PRISE PAR LE CONSEIL NATIONAL DE L'ORDRE DES MÉDECINS REJETANT LE RECOURS D'UN TIERS FORMÉ À L'ENCONTRE D'UNE DÉCISION D'INSCRIPTION AU TABLEAU DE L'ORDRE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-01-02-01 PROCÉDURE. INTRODUCTION DE L'INSTANCE. LIAISON DE L'INSTANCE. RECOURS ADMINISTRATIF PRÉALABLE. - DÉCISION REJETANT UN RAPO - MOTIVATION OBLIGATOIRE - INCLUSION - DÉCISION PRISE PAR LE CONSEIL NATIONAL DE L'ORDRE DES MÉDECINS REJETANT LE RECOURS D'UN TIERS FORMÉ À L'ENCONTRE D'UNE DÉCISION D'INSCRIPTION AU TABLEAU DE L'ORDRE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">55-02-01-01 PROFESSIONS, CHARGES ET OFFICES. ACCÈS AUX PROFESSIONS. MÉDECINS. INSCRIPTION AU TABLEAU. - RECOURS ADMINISTRATIF PRÉVU À L'ARTICLE L. 4112-4 DU CODE DE LA SANTÉ PUBLIQUE - 1) DÉCISION PRISE PAR LE CONSEIL NATIONAL DE L'ORDRE DES MÉDECINS REJETANT LE RECOURS - MOTIVATION OBLIGATOIRE - EXISTENCE - 2) RECOURS ASSORTI DE CONCLUSIONS TENDANT À LA SANCTION DE MANQUEMENTS DISCIPLINAIRES - OBLIGATION POUR LE CONSEIL NATIONAL DE L'ORDRE DE TRANSMETTRE LA PLAINTE À L'INSTANCE COMPÉTENTE POUR EN CONNAÎTRE - EXISTENCE.
</SCT>
<ANA ID="9A"> 01-03-01-02-01-01 Les décisions prises par le Conseil national de l'ordre des médecins rejetant le recours d'un tiers formé à l'encontre d'une décision d'inscription au tableau de l'ordre doivent être motivées en vertu du dernier alinéa ajouté à l'article 1er de la loi n° 79-587 du 11 juillet 1979 par la loi n° 2011-525 du 17 mai 2011 aux termes duquel doivent être motivées les décisions qui rejettent un recours administratif dont la présentation est obligatoire préalablement à tout recours contentieux en application d'une disposition législative ou réglementaire (RAPO).</ANA>
<ANA ID="9B"> 54-01-02-01 Les décisions prises par le Conseil national de l'ordre des médecins rejetant le recours d'un tiers formé à l'encontre d'une décision d'inscription au tableau de l'ordre doivent être motivées en vertu du dernier alinéa ajouté à l'article 1er de la loi n° 79-587 du 11 juillet 1979 par la loi n° 2011-525 du 17 mai 2011 aux termes duquel doivent être motivées les décisions qui rejettent un recours administratif dont la présentation est obligatoire préalablement à tout recours contentieux en application d'une disposition législative ou réglementaire (RAPO).</ANA>
<ANA ID="9C"> 55-02-01-01 1) Les décisions prises par le Conseil national de l'ordre des médecins rejetant le recours d'un tiers formé à l'encontre d'une décision d'inscription au tableau de l'ordre doivent être motivées en vertu du dernier alinéa ajouté à l'article 1er de la loi n° 79-587 du 11 juillet 1979 par la loi n° 2011-525 du 17 mai 2011 aux termes duquel doivent être motivées les décisions qui rejettent un recours administratif dont la présentation est obligatoire préalablement à tout recours contentieux en application d'une disposition législative ou réglementaire (RAPO).,,,2) Lorsque le Conseil national de l'ordre des médecins est saisi, même à l'occasion d'une procédure administrative et notamment du recours prévu à l'article L. 4112-4 du code de la santé publique, d'une plainte fondée sur des manquements allégués aux règles de déontologie, relevant des instances disciplinaires, il lui appartient soit de saisir lui-même la chambre disciplinaire de première instance compétente, soit de transmettre la plainte au conseil départemental de l'ordre compétent, afin que les suites appropriés soient données à cette plainte. Il ne peut se borner à rejeter comme irrecevables des conclusions présentées dans le cadre du recours administratif tendant à ce que des manquements disciplinaires soient sanctionnés.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Comp., dans l'état du droit antérieur à la loi du 17 mai 2011, CE, 23 mars 2011,,, n° 337808, T. pp. 735-1129.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
