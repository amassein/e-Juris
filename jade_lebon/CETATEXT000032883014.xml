<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032883014</ID>
<ANCIEN_ID>JG_L_2016_07_000000392586</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/88/30/CETATEXT000032883014.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 11/07/2016, 392586, Publié au recueil Lebon</TITRE>
<DATE_DEC>2016-07-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>392586</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BORE, SALVE DE BRUNETON</AVOCATS>
<RAPPORTEUR>M. Jacques Reiller</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:392586.20160711</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              L'association Ethique et Liberté a demandé au tribunal administratif de Paris, d'une part, d'annuler pour excès de pouvoir la décision implicite par laquelle le Premier ministre a rejeté sa demande de communication des noms des fonctionnaires affectés au sein de la Mission interministérielle de vigilance et de lutte contre les dérives sectaires (MIVILUDES) dans le " Pôle économie budget ", le " Pôle sécurité ", le " Pôle parlementaire et relation avec la presse ", le " Pôle Santé ", le " Pôle diplomatique " et le " Pôle éducation-jeunesse ", les noms des fonctionnaires composant le comité exécutif de pilotage opérationnel et les noms des personnalités composant le conseil d'orientation de cette Mission en 2014, ainsi que le montant des indemnités perçues par ces personnalités, d'autre part, d'enjoindre au Premier ministre de lui communiquer ces documents, après occultation d'éventuelles mentions non communicables, dans le délai de huit jours à compter de la notification du jugement à intervenir, sous astreinte de 300 euros par jour de retard. <br/>
<br/>
              Par un jugement n° 1421383/5-1 du 11 juin 2015, le tribunal administratif de Paris a, d'une part, annulé la décision implicite du Premier ministre en tant que, par cette décision, il a refusé de communiquer à l'association Ethique et Liberté les noms des fonctionnaires affectés à la MIVILUDES dans le " Pôle économie budget ", le " Pôle sécurité ", le " Pôle parlementaire et relation avec la presse ", le " Pôle Santé ", le " Pôle diplomatique " et le " Pôle éducation-jeunesse ", les noms des fonctionnaires composant le comité exécutif de pilotage opérationnel ainsi que les noms des personnalités composant le conseil d'orientation de cette Mission en 2014, d'autre part, enjoint au Premier ministre de communiquer ces documents à l'association Ethique et Liberté ; il a rejeté le surplus des conclusions de la requête de l'association Ethique et Liberté.<br/>
<br/>
              Par un pourvoi enregistré au secrétariat du contentieux du Conseil d'Etat le 11 août 2015, le Premier ministre demande au Conseil d'Etat d'annuler ce jugement du tribunal administratif de Paris en tant qu'il a fait droit aux conclusions de l'association Ethique et Liberté.<br/>
<br/>
              Vu  les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - la loi n° 78-753 du 17 juillet 1978 ;<br/>
              - le décret n° 2002-1392 du 28 novembre 2002 ;<br/>
              - le décret n° 2005-1755 du 30 décembre 2005 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jacques Reiller, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boré, Salve de Bruneton, avocat de l'association Ethique et Liberté ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que le Premier ministre se pourvoit en cassation contre le jugement du 11 juin 2015 en tant que, par ce jugement, le tribunal administratif de Paris a, d'une part, annulé son refus de communiquer à l'association Ethique et Liberté les noms des personnalités composant le conseil d'orientation de la Mission interministérielle de vigilance et de lutte contre les dérives sectaires (MIVILUDES), ceux des fonctionnaires composant le comité exécutif de pilotage opérationnel ainsi que ceux des fonctionnaires affectés dans les six pôles de cette mission, le " Pôle économie budget ", le " Pôle sécurité ", le " Pôle parlementaire et relation avec la presse ", le " Pôle Santé ", le " Pôle diplomatique " et le " Pôle éducation-jeunesse " et, d'autre part, lui a enjoint de communiquer ces documents à l'association Ethique et Liberté ;<br/>
<br/>
              En ce qui concerne les noms des personnes composant le conseil d'orientation et le comité exécutif de pilotage opérationnel de la MIVILUDES :<br/>
<br/>
              2. Considérant que le premier alinéa de l'article 2 de la loi du 17 juillet 1978 portant diverses mesures d'amélioration des relations entre l'administration et le public et diverses dispositions d'ordre administratif, social et fiscal, aujourd'hui repris à l'article L. 311-1 du code des relations entre le public et l'administration, dispose que : " Sous réserve des dispositions de l'article 6, les autorités mentionnées à l'article 1er sont tenues de communiquer les documents administratifs qu'elles détiennent aux personnes qui en font la demande (...) " ; qu'aux termes du cinquième alinéa du même article : " Le droit à communication ne s'exerce plus lorsque les documents font l'objet d'une diffusion publique " ; <br/>
<br/>
              3. Considérant que les seuls documents administratifs comportant les éléments dont l'association a demandé communication sont les arrêtés fixant la composition du conseil d'orientation et du comité exécutif de pilotage opérationnel de la MIVILUDES ; qu'il est constant que ces arrêtés ont été publiés au Journal officiel de la République française ; que, dès lors, ils ont fait l'objet d'une diffusion publique au sens des dispositions citées au point 2 ci-dessus ; qu'il suit de là que le Premier ministre est fondé à soutenir que le tribunal administratif a entaché son jugement d'erreur de droit en estimant que ces documents étaient communicables alors qu'ayant fait l'objet d'une diffusion publique ils ne relevaient plus du champ d'application de l'obligation de communiquer résultant de l'article 2 de la loi du 17 juillet 1978 ; que ce moyen, qui est d'ordre public, peut être utilement présenté pour la première fois en cassation ;<br/>
<br/>
              En ce qui concerne les fonctionnaires affectés dans les six pôles de la MIVILUDES :<br/>
<br/>
              4. Considérant qu'aux termes de l'article 6 de la loi du 17 juillet 1978 : " I. Ne sont pas communicables : / les (...) documents administratifs dont la consultation ou la communication porterait atteinte : (...)/ d) A la sûreté de l'Etat, à la sécurité publique ou à la sécurité des personnes (...) " ; <br/>
<br/>
              5. Considérant qu'aux termes du décret du 28 novembre 2002 instituant une mission interministérielle de vigilance et de lutte contre les dérives sectaires, la MIVILUDES est chargée : " 1° D'observer et d'analyser le phénomène des mouvements à caractère sectaire dont les agissements sont attentatoires aux droits de l'homme et aux libertés fondamentales ou constituent une menace à l'ordre public ou sont contraires aux lois et règlements ; /2° De favoriser, dans le respect des libertés publiques, la coordination de l'action préventive et répressive des pouvoirs publics à l'encontre de ces agissements ; /3° De développer l'échange des informations entre les services publics sur les pratiques administratives dans le domaine de la lutte contre les dérives sectaires ; /4° De contribuer à l'information et à la formation des agents publics dans ce domaine ; /5° D'informer le public sur les risques, et le cas échéant les dangers, auxquels les dérives sectaires l'exposent et de faciliter la mise en oeuvre d'actions d'aide aux victimes de ces dérives ; 6° De participer aux travaux relatifs aux questions relevant de sa compétence menés par le ministère des affaires étrangères dans le champ international " ; qu'eu égard à la nature des missions ainsi définies de la MIVILUDES et aux responsabilités des fonctionnaires qui y sont affectés, le tribunal administratif de Paris a entaché son jugement d'inexacte qualification juridique des faits en jugeant que la divulgation de leur identité n'était pas de nature à porter atteinte à la sécurité publique ou à la sécurité des personnes et en en déduisant que les noms des fonctionnaires des six pôles de la mission étaient communicables ; <br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède que, sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, le Premier ministre est fondé à demander l'annulation du jugement qu'il attaque, en tant qu'il a fait droit aux conclusions de l'association Ethique et Liberté ; <br/>
<br/>
              7. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de faire application de l'article L. 821-2 du code de justice administrative et de régler l'affaire au fond dans la même mesure ;<br/>
<br/>
              8. Considérant qu'il résulte des motifs énoncés aux points 3 et 5 de la présente décision que l'association Ethique et Liberté n'est pas fondée à demander l'annulation de la décision par laquelle le Premier ministre a refusé de lui communiquer les noms des personnalités composant le conseil d'orientation de la Mission interministérielle de vigilance et de lutte contre les dérives sectaires, ceux des fonctionnaires composant le comité exécutif de pilotage opérationnel de cette Mission ainsi que ceux des fonctionnaires affectés dans les six pôles de cette même Mission, ni par suite à demander qu'il soit enjoint au Premier ministre de lui communiquer ces documents ;<br/>
<br/>
              9. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              ---------------<br/>
<br/>
Article 1er : Les articles 1er et 2 du jugement n° 1421383/5-1 du 11 juin 2015 du tribunal administratif de Paris sont annulés. <br/>
<br/>
Article 2 : Les conclusions de l'association Ethique et Liberté tendant à l'annulation du refus opposé par le Premier ministre à sa demande de communication des noms des personnalités composant le conseil d'orientation de la Mission interministérielle de vigilance et de lutte contre les dérives sectaires, de ceux des fonctionnaires composant le comité exécutif de pilotage opérationnel et de ceux des fonctionnaires affectés dans les six pôles de cette Mission, ainsi que ses conclusions tendant à ce qu'il soit enjoint au Premier ministre de lui communiquer ces documents, sont rejetées.<br/>
<br/>
Article 3 : Les conclusions présentées par l'association Ethique et Liberté au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 4 : La présente décision sera notifiée au Premier ministre et à l'association Ethique et Liberté.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">26-06-01-02-03 DROITS CIVILS ET INDIVIDUELS. ACCÈS AUX DOCUMENTS ADMINISTRATIFS. ACCÈS AUX DOCUMENTS ADMINISTRATIFS AU TITRE DE LA LOI DU 17 JUILLET 1978. DROIT À LA COMMUNICATION. DOCUMENTS ADMINISTRATIFS NON COMMUNICABLES. - NOMS DES FONCTIONNAIRES AFFECTÉS À LA MIVILUDES.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">26-06-01-04 DROITS CIVILS ET INDIVIDUELS. ACCÈS AUX DOCUMENTS ADMINISTRATIFS. ACCÈS AUX DOCUMENTS ADMINISTRATIFS AU TITRE DE LA LOI DU 17 JUILLET 1978. CONTENTIEUX. - MOYEN D'ORDRE PUBLIC - INCLUSION - MOYEN TIRÉ DE CE QUE DES DOCUMENTS NE RELÈVENT PAS DU CHAMP D'APPLICATION DE L'OBLIGATION DE COMMUNIQUER, DÈS LORS QU'ILS ONT FAIT L'OBJET D'UNE DIFFUSION PUBLIQUE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-07-01-04-01-02-01 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. QUESTIONS GÉNÉRALES. MOYENS. MOYENS D'ORDRE PUBLIC À SOULEVER D'OFFICE. EXISTENCE. CHAMP D'APPLICATION DE LA LOI. - MOYEN TIRÉ DE CE QUE DES DOCUMENTS NE RELÈVENT PAS DU CHAMP D'APPLICATION DE L'OBLIGATION DE COMMUNIQUER, DÈS LORS QU'ILS ONT FAIT L'OBJET D'UNE DIFFUSION PUBLIQUE.
</SCT>
<ANA ID="9A"> 26-06-01-02-03 Eu égard à la nature des missions de la mission interministérielle de vigilance et de lutte contre les dérives sectaires (MIVILUDES), définies par le décret n° 2002-1392 du 28 novembre 2002, et aux responsabilités des fonctionnaires qui y sont affectés, la divulgation de leur identité est de nature à porter atteinte à la sécurité publique ou à la sécurité des personnes au sens de l'article 6 de la loi n° 78-753 du 17 juillet 1978. Les noms des fonctionnaires affectés à cette mission ne sont donc pas communicables.</ANA>
<ANA ID="9B"> 26-06-01-04 Est d'ordre public le moyen tiré de ce que des documents ont fait l'objet d'une diffusion publique et ne relèvent donc plus du champ d'application de l'obligation de communiquer résultant de l'article 2 de la loi n° 78-753 du 17 juillet 1978.</ANA>
<ANA ID="9C"> 54-07-01-04-01-02-01 Est d'ordre public le moyen tiré de ce que des documents ont fait l'objet d'une diffusion publique et ne relèvent donc plus du champ d'application de l'obligation de communiquer résultant de l'article 2 de la loi n° 78-753 du 17 juillet 1978.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
