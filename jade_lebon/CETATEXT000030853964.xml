<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030853964</ID>
<ANCIEN_ID>JG_L_2015_07_000000381095</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/85/39/CETATEXT000030853964.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème / 2ème SSR, 01/07/2015, 381095</TITRE>
<DATE_DEC>2015-07-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>381095</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème / 2ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUIN-PALAT, BOUCARD ; SCP JEAN-PHILIPPE CASTON</AVOCATS>
<RAPPORTEUR>M. Stéphane Bouchard</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Bertrand Dacosta</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:381095.20150701</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société nouvelle d'entreprise générale du sud-ouest (SNEGSO) a demandé au tribunal administratif de Pau de condamner Habitat Sud Atlantic à lui verser la somme de 274 335,88 euros en réparation du préjudice né de son éviction irrégulière de l'appel d'offres ouvert pour la passation d'un marché public de travaux portant sur le programme de réaménagement d'une résidence.<br/>
<br/>
              Par un jugement n° 1101659 du 21 février 2013, le tribunal administratif de Pau a condamné Habitat Sud Atlantic au paiement à la SNEGSO de la somme de 268 386 euros HT assortie des intérêts au taux légal à compter du 1er juillet 2011.<br/>
<br/>
              Par un arrêt n°s 13BX01149, 13BX01574 du 8 avril 2014, la cour administrative d'appel de Bordeaux a, sur requêtes d'Habitat Sud Atlantic, annulé ce jugement, rejeté les conclusions indemnitaires de la SNEGSO et, décidé qu'il n'y avait pas lieu de statuer sur les conclusions tendant au sursis à l'exécution du même jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 10 juin et 10 septembre 2014 au secrétariat du contentieux du Conseil d'Etat, la SNEGSO demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler les articles 1er et 2 de l'arrêt du 8 avril 2014 de la cour administrative d'appel de Bordeaux ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la requête d'appel d'Habitat Sud Atlantic ;<br/>
<br/>
              3°) de mettre à la charge d'Habitat Sud Atlantic le versement de la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code des marchés publics ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Stéphane Bouchard, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Bertrand Dacosta, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Jean-Philippe Caston, avocat de la société nouvelle d'entreprise générale du sud-ouest, et à la SCP Thouin-Palat, Boucard, avocat de la société Habitat Sud Atlantic ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par un avis d'appel public à la concurrence publié le 1er décembre 2010, le groupement de commandes constitué de l'Office public de l'habitat de Bayonne et de la société HLM Habitat Sud Atlantic, aux droits desquels vient Habitat Sud Atlantic, a engagé une procédure d'appel d'offres ouvert pour la passation d'un marché unique de travaux portant sur le programme de réaménagement des pieds d'immeubles et de requalification des commerces, façades, celliers et entresols d'une résidence ; que, par un courrier du 22 mars 2011, la société nouvelle d'entreprise générale du sud-ouest (SNEGSO) a été informée du rejet de son offre ; que cette société se pourvoit en cassation contre l'arrêt du 8 avril 2014 de la cour administrative d'appel de Bordeaux en tant, d'une part, qu'il annule le jugement du 21 février 2013 par lequel le tribunal administratif de Pau a condamné Habitat Sud Atlantic à lui verser la somme de 268 386 euros, assortie des intérêts au taux légal en réparation du préjudice subi par cette société à raison de son éviction et, d'autre part, qu'il rejette ses conclusions indemnitaires ;<br/>
<br/>
              2. Considérant que le pouvoir adjudicateur définit librement la méthode de notation pour la mise en oeuvre de chacun des critères de sélection des offres qu'il a définis et rendus publics ; que, toutefois, une méthode de notation est entachée d'irrégularité si, en méconnaissance des principes fondamentaux d'égalité de traitement des candidats et de transparence des procédures, elle est par elle-même de nature à priver de leur portée les critères de sélection ou à neutraliser leur pondération et est, de ce fait, susceptible de conduire, pour la mise en oeuvre de chaque critère, à ce que la meilleure note ne soit pas attribuée à la meilleure offre ou, au regard de l'ensemble des critères pondérés, à ce que l'offre économiquement la plus avantageuse ne soit pas choisie ; qu'il en va ainsi alors même que la personne publique, qui n'y est pas tenue, aurait rendu publique, dans l'avis d'appel à concurrence ou les documents de la consultation, une telle méthode de notation ;<br/>
<br/>
              3. Considérant qu'en jugeant, après avoir relevé que le marché était un marché global divisé en dix lots techniques et que le pouvoir adjudicateur  avait décidé, pour la mise en oeuvre du critère du prix, de procéder à une notation lot par lot, avant de faire la moyenne arithmétique des différentes notes obtenues pour calculer une note globale, que la méthode de notation ainsi utilisée n'était pas entachée d'irrégularité, alors qu'il ressortait des pièces du dossier soumis à son examen que le calcul de la note globale ne permettait pas de tenir compte de la grande disparité des valeurs des différents lots ni, par suite, d'identifier l'offre dont le prix était effectivement le plus avantageux, la cour administrative d'appel a commis une erreur de droit ; qu'il s'ensuit, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que la SNEGSO est fondée à demander l'annulation des articles 1er et 2 de l'arrêt attaqué par lesquels la cour administrative d'appel de Bordeaux a annulé le jugement du tribunal administratif de Pau du 21 février 2013 et rejeté ses conclusions indemnitaires ;<br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge d'Habitat Sud Atlantic le versement à la SNEGSO de la somme de 3 000 euros  au titre des dispositions de l'article L. 761-1 du code de justice administrative ; que ces dispositions font obstacle à ce que le versement d'une somme soit mis à la charge de la SNEGSO, qui n'est pas la partie perdante dans la présente instance ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les articles 1er et 2 de l'arrêt du 8 avril 2014 de la cour administrative d'appel de Bordeaux sont annulés.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Bordeaux.<br/>
Article 3 : Habitat Sud Atlantic versera à la société nouvelle d'entreprise générale du sud-ouest la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions présentées par Habitat Sud Atlantic au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à la société nouvelle d'entreprise générale du sud-ouest et à Habitat Sud Atlantic.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-02-005 MARCHÉS ET CONTRATS ADMINISTRATIFS. FORMATION DES CONTRATS ET MARCHÉS. FORMALITÉS DE PUBLICITÉ ET DE MISE EN CONCURRENCE. - MÉTHODE DE NOTATION - IRRÉGULARITÉ D'UNE MÉTHODE PRIVANT DE LEUR PORTÉE LES CRITÈRES OU NEUTRALISANT LEUR PONDÉRATION [RJ1] - EXISTENCE EN L'ESPÈCE.
</SCT>
<ANA ID="9A"> 39-02-005 Marché global divisé en dix lots techniques, pour lequel le pouvoir adjudicateur a décidé, pour la mise en oeuvre du critère du prix, de procéder à une notation lot par lot, avant de faire la moyenne arithmétique des différentes notes obtenues pour calculer une note globale.... ,,Le calcul de la note globale ne permettant pas de tenir compte de la grande disparité des valeurs des différents lots ni, par suite, d'identifier l'offre dont le prix était effectivement le plus avantageux, cette méthode est entachée d'irrégularité.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., sur la grille d'analyse de la régularité d'une méthode de notation, CE, 3 novembre 2014, Commune de Belleville-sur-Loire, n° 373362, p. 323.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
