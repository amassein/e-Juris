<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025833599</ID>
<ANCIEN_ID>JG_L_2012_05_000000356209</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/83/35/CETATEXT000025833599.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème et 2ème sous-sections réunies, 09/05/2012, 356209</TITRE>
<DATE_DEC>2012-05-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>356209</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème et 2ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Christian Vigouroux</PRESIDENT>
<AVOCATS>FOUSSARD ; SCP GASCHIGNARD</AVOCATS>
<RAPPORTEUR>M. Jean-Dominique Nuttens</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Bertrand Dacosta</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:356209.20120509</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 27 janvier 2012 au secrétariat du contentieux du Conseil d'Etat, présenté pour la REGION CHAMPAGNE-ARDENNE, représentée par le président du conseil régional ; la REGION CHAMPAGNE-ARDENNE demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 1102080 du 12 janvier 2012 par laquelle le juge des référés du tribunal administratif de Châlons-en-Champagne, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, a, à la demande de la société ACE BTP, suspendu la mesure de résiliation de marché prononcée à son encontre au titre de la mission d'ordonnancement, pilotage et coordination (OPC) se rattachant à l'opération de restructuration et d'extension du lycée Decomble à Chaumont, par la société Quadri-Cités agissant en qualité de mandataire de la région ; <br/>
<br/>
              2°) statuant en référé, de rejeter la demande de la société ACE BTP ; <br/>
<br/>
              3°) de mettre à la charge de la société ACE BTP le versement de la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative et les entiers dépens, notamment les frais de contribution à l'aide juridique ;<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Nuttens, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les observations de Me Foussard, avocat de la REGION CHAMPAGNE-ARDENNE et de la SCP Gaschignard, avocat de la société ACE BTP, <br/>
<br/>
              - les conclusions de M. Bertrand Dacosta, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à Me Foussard, avocat de la REGION CHAMPAGNE-ARDENNE et à la SCP Gaschignard, avocat de la société ACE BTP ; <br/>
<br/>
<br/>
<br/>Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ; <br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis au juge des référés que la REGION CHAMPAGNE-ARDENNE a entrepris de restructurer et de rénover le lycée Decomble à Chaumont ; qu'elle a conclu avec  la société ACE BTP un marché en vue de la réalisation de la mission d'ordonnancement, pilotage et coordination pour un montant de 101 555 euros TTC, la mission étant scindée en quatre phases distinctes ; que, par courrier en date du 30 septembre 2011, le maître d'ouvrage délégué a notifié à la société ACE BTP l'arrêt de l'exécution des prestations à l'issue de la phase 1 et, par voie de conséquence, la résiliation du marché, en application de l'article 18 du cahier des clauses administratives générales - prestations intellectuelles applicable au marché ; que la REGION CHAMPAGNE-ARDENNE se pourvoit en cassation contre l'ordonnance du 12 janvier 2012 par laquelle le juge des référés du tribunal administratif de Châlons-en-Champagne a suspendu, à la demande de la société ACE BTP, l'exécution de la décision de résiliation du marché ;<br/>
<br/>
              Considérant qu'il incombe au juge des référés saisi, sur le fondement de l'article L. 521-1 du code de justice administrative, de conclusions tendant à la suspension d'une mesure de résiliation, après avoir vérifié que l'exécution du contrat n'est pas devenue sans objet, de prendre en compte, pour apprécier la condition d'urgence, d'une part, les atteintes graves et immédiates que la résiliation litigieuse est susceptible de porter à un intérêt public ou aux intérêts du requérant, notamment à la situation financière de ce dernier ou à l'exercice même de son activité, d'autre part, l'intérêt général ou l'intérêt de tiers, notamment du titulaire d'un nouveau contrat dont la conclusion aurait été rendue nécessaire par la résiliation litigieuse, qui peut s'attacher à l'exécution immédiate de la mesure de résiliation ;<br/>
<br/>
              Considérant que, pour juger que la condition d'urgence était remplie, le juge des référés du tribunal administratif de Châlons-en-Champagne a retenu une atteinte grave et immédiate à la situation financière de la requérante compte tenu de la perte de chiffre d'affaires occasionnée par la résiliation du marché ; qu'en statuant ainsi, en se limitant à la seule prise en compte de la perte de chiffre d'affaires occasionnée par la résiliation du marché sans se référer aux autres éléments d'activité de l'entreprise, et notamment à son chiffre d'affaires global, pour évaluer l'atteinte à sa situation financière, le juge des référés a commis une erreur de droit ; que, par suite, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, la REGION CHAMPAGNE-ARDENNE est fondée à demander l'annulation de l'ordonnance ;<br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au titre de la procédure de référé engagée par la société ACE BTP en application des dispositions de l'article L. 821-2 du code de justice administrative ; <br/>
<br/>
              Considérant, d'une part, qu'il ne résulte pas de l'instruction que l'exécution du marché soit devenue sans objet dès lors que la délibération par laquelle la région a décidé de mettre fin au marché prévoit la mise en oeuvre de nouvelles consultations pour la réalisation des prestations qui n'ont pu être exécutées à raison de la résiliation du marché litigieux ;  <br/>
<br/>
              Considérant, d'autre part, qu'il résulte de l'instruction que la perte de chiffre d'affaires hors taxes résultant de la résiliation du marché représente moins de 3 % du dernier chiffre d'affaires connu de la société requérante, cette perte  ayant au surplus vocation à être répartie sur deux exercices au moins ; que la société ACE BTP est confrontée à des impératifs normaux de reclassement des personnels en charge de l'exécution du marché résilié ; que la requérante, qui invoque l'atteinte à sa réputation professionnelle sans caractériser celle-ci au regard de circonstances particulières, ne justifie pas ainsi d'une atteinte grave et immédiate à ses intérêts, et, par suite, de l'urgence à ordonner la reprise des relations contractuelles ; <br/>
<br/>
              Considérant qu'il résulte de ce qui précède que la société ACE BTP n'est pas fondée à demander la suspension de l'exécution de la décision de résiliation du marché en vue de la reprise des relations contractuelles ; <br/>
<br/>
              Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de la REGION <br/>
CHAMPAGNE-ARDENNE, qui n'est pas, dans la présente instance, la partie perdante, la somme demandée par la société ACE BTP au titre des frais exposés par elle et non compris dans les dépens ; qu'il y a lieu, en revanche, de faire droit aux conclusions de la région tendant, d'une part, à ce que la société lui verse la somme de 4 500 euros au titre des mêmes dispositions et, d'autre part, à ce que soit également mise à la charge de cette société la contribution pour l'aide juridique acquittée par la région ; <br/>
<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              			---------------<br/>
<br/>
Article 1er : L'ordonnance du juge des référés du tribunal administratif de <br/>
Châlons-en-Champagne du 12 janvier 2012 est annulée.<br/>
Article 2 : La demande de la société ACE BTP présentée devant le juge des référés du tribunal administratif de Châlons-en-Champagne est rejetée.<br/>
Article 3 : La contribution pour l'aide juridique acquittée par la REGION <br/>
CHAMPAGNE-ARDENNE est mise à la charge de la société ACE BTP. <br/>
Article 4 : La société ACE BTP versera à la REGION CHAMPAGNE-ARDENNE une somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 5 : La présente décision sera notifiée à la REGION CHAMPAGNE-ARDENNE, à la société ACE BTP et à la société Quadri-Cités.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-04-02-04 MARCHÉS ET CONTRATS ADMINISTRATIFS. FIN DES CONTRATS. RÉSILIATION. POUVOIRS DU JUGE. - CONTESTATION DE LA VALIDITÉ D'UNE MESURE DE RÉSILIATION - DEMANDE DE SUSPENSION DE LA RÉSILIATION [RJ1] - OFFICE DU JUGE - APPRÉCIATION DE LA CONDITION D'URGENCE - NOTION D'ATTEINTE GRAVE ET IMMÉDIATE - PRISE EN COMPTE DE LA PERTE DE CHIFFRE D'AFFAIRES ENTRAÎNÉE PAR LA RÉSILIATION - MÉTHODE - OBLIGATION POUR LE JUGE DE RAPPORTER CETTE PERTE AUX AUTRES ÉLÉMENTS D'ACTIVITÉ DE L'ENTREPRISE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-035-02-03-02 PROCÉDURE. PROCÉDURES INSTITUÉES PAR LA LOI DU 30 JUIN 2000. RÉFÉRÉ SUSPENSION (ART. L. 521-1 DU CODE DE JUSTICE ADMINISTRATIVE). CONDITIONS D'OCTROI DE LA SUSPENSION DEMANDÉE. URGENCE. - DEMANDE DE SUSPENSION DE LA RÉSILIATION D'UN CONTRAT ADMINISTRATIF [RJ1] - NOTION D'ATTEINTE GRAVE ET IMMÉDIATE À LA SITUATION FINANCIÈRE DU CONCONTRACTANT - PRISE EN COMPTE DE LA PERTE DE CHIFFRE D'AFFAIRES ENTRAÎNÉE PAR LA RÉSILIATION - MÉTHODE - OBLIGATION POUR LE JUGE DE RAPPORTER CETTE PERTE AUX AUTRES ÉLÉMENTS D'ACTIVITÉ DE L'ENTREPRISE.
</SCT>
<ANA ID="9A"> 39-04-02-04 Pour retenir, au titre de l'appréciation de la condition d'urgence, une atteinte grave et immédiate à la situation financière d'un cocontractant de l'administration, le juge des référés saisi d'une demande de suspension de la résiliation d'un contrat administratif doit rapporter la perte de chiffre d'affaires entraînée par la résiliation du marché aux autres éléments d'activité de l'entreprise, notamment son chiffre d'affaires global.</ANA>
<ANA ID="9B"> 54-035-02-03-02 Pour retenir, au titre de l'appréciation de la condition d'urgence, une atteinte grave et immédiate à la situation financière d'un cocontractant de l'administration, le juge des référés saisi d'une demande de suspension de la résiliation d'un contrat administratif doit rapporter la perte de chiffre d'affaires entraînée par la résiliation du marché aux autres éléments d'activité de l'entreprise, notamment son chiffre d'affaires global.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Section, 21 mars 2011, Commune de Béziers, n° 304806, à publier au Recueil.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
