<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034940738</ID>
<ANCIEN_ID>JG_L_2017_06_000000401637</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/94/07/CETATEXT000034940738.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 14/06/2017, 401637</TITRE>
<DATE_DEC>2017-06-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>401637</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>DELAMARRE ; SCP MONOD, COLIN, STOCLET</AVOCATS>
<RAPPORTEUR>Mme Sandrine Vérité</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Jean Lessi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:401637.20170614</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...et Mme C...D...ont demandé au tribunal administratif de Montpellier :<br/>
              - à titre principal, d'annuler la décision du 7 octobre 2014 par laquelle le président du conseil général de l'Aude a confirmé la décision de récupération d'un indu de revenu de solidarité active au titre de la période de mars 2012 à janvier 2014, de prononcer la décharge de l'obligation de payer correspondante et d'enjoindre à la caisse d'allocations familiales de l'Aude de rembourser les sommes retenues ;<br/>
              - à titre subsidiaire, d'annuler la décision du même jour par laquelle le président du conseil général de l'Aude a refusé de leur accorder une remise gracieuse de l'indu ainsi mis à leur charge.<br/>
<br/>
              Par un jugement n° 1405597 du 22 mars 2016, le magistrat désigné par le président du tribunal administratif de Montpellier a rejeté leur demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 20 juillet et 19 octobre 2016 au secrétariat du contentieux du Conseil d'Etat, M. A...et Mme D... demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement du tribunal administratif de Montpellier du 22 mars 2016 ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur demande ;<br/>
<br/>
              3°) de mettre à la charge du département de l'Aude la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'action sociale et des familles ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sandrine Vérité, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Jean Lessi, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Delamarre, avocat de M. A...et MmeD..., et à la SCP Monod, Colin, Stoclet, avocat du département de l'Aude.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 262-3 du code de l'action sociale et des familles : " (...) L'ensemble des ressources du foyer, y compris celles qui sont mentionnées à l'article L. 132-1, est pris en compte pour le calcul du revenu de solidarité active, dans des conditions fixées par un décret en Conseil d'Etat qui détermine notamment : (...) 2° Les modalités d'évaluation des ressources (...) ". L'article L. 132-1 de ce code dispose que : " Il est tenu compte, pour l'appréciation des ressources des postulants à l'aide sociale, des revenus professionnels et autres et de la valeur en capital des biens non productifs de revenu, qui est évaluée dans les conditions fixées par voie réglementaire. (...) ". L'article R. 132-1 du même code prévoit, enfin, que : " Pour l'appréciation des ressources des postulants prévue à l'article L. 132-1, les biens non productifs de revenu, à l'exclusion de ceux constituant l'habitation principale du demandeur, sont considérés comme procurant un revenu annuel égal à 50 % de leur valeur locative s'il s'agit d'immeubles bâtis, à 80 % de cette valeur s'il s'agit de terrains non bâtis et à 3 % du montant des capitaux ".<br/>
<br/>
              2. Il résulte de ces dispositions que seules peuvent être évaluées sur la base forfaitaire prévue par les articles L. 132-1 et R. 132-1 du code de l'action sociale et des familles les ressources que l'allocataire est supposé pouvoir retirer de biens non productifs de revenu. Par suite, si les capitaux dont il dispose ont fait l'objet de placements productifs de revenus, seuls ces derniers peuvent être pris en compte, quand bien même le taux d'intérêt de ces placements serait inférieur au taux de 3 % prévu par l'article R. 132-1. La circonstance que l'allocataire n'aurait pas spontanément déclaré ces revenus est sans incidence sur l'application de ces dispositions. <br/>
<br/>
              3. En l'espèce, il ressort des pièces du dossier soumis au juge du fond que l'indu de revenu de solidarité active notifié à Mme D...et à M. A...a été calculé en appliquant un taux de 3 % aux sommes détenues par ceux-ci et par leur fils sur plusieurs livrets et plans d'épargne, qui n'avaient pas été déclarés. Il résulte de ce qui a été dit au point précédent qu'en jugeant que l'administration était fondée à appliquer ce taux forfaitaire, alors que ces livrets et plans d'épargne étaient productifs d'intérêts, à un taux d'ailleurs inférieur, le tribunal a commis une erreur de droit.<br/>
<br/>
              4. Il résulte de ce qui précède que M. A...et Mme D...sont fondés à demander l'annulation du jugement qu'ils attaquent. Le moyen retenu suffisant à entraîner cette annulation, il n'est pas nécessaire d'examiner les autres moyens de leur pourvoi.<br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du département de l'Aude une somme de 2 000 euros à verser à M. A...et Mme D...au titre de l'article L. 761-1 du code de justice administrative. En revanche, les dispositions de cet article font obstacle à ce qu'il soit fait droit aux conclusions présentées par le département de l'Aude au même titre.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Montpellier du 22 mars 2016 est annulé.<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Montpellier.<br/>
Article 3 : Le département de l'Aude versera une somme de 2 000 euros à M. A...et à Mme D... au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions du département de l'Aude présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à M. B...A...et Mme C...D..., au département de l'Aude et à la ministre des solidarités et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">04-02-06 AIDE SOCIALE. DIFFÉRENTES FORMES D'AIDE SOCIALE. REVENU MINIMUM D'INSERTION (RMI). - RSA - RESSOURCES PRISES EN COMPTE POUR LE CALCUL DE L'ALLOCATION - VALEUR EN CAPITAL DES BIENS NON PRODUCTIFS DE REVENU - CONDITIONS D'ÉVALUATION DU REVENU PROCURÉ - APPLICATION À DES BIENS PRODUCTIFS DE REVENU - ABSENCE, MÊME SI L'ALLOCATAIRE N'A PAS SPONTANÉMENT DÉCLARÉ CES REVENUS.
</SCT>
<ANA ID="9A"> 04-02-06 Seules peuvent être évaluées sur la base forfaitaire prévue par les articles L. 132-1 et R. 132-1 du code de l'action sociale et des familles les ressources que l'allocataire est supposé pouvoir retirer de biens non productifs de revenu. Par suite, si les capitaux dont il dispose ont fait l'objet de placements productifs de revenus, seuls ces revenus peuvent être pris en compte, quand bien même le taux d'intérêt de ces placements serait inférieur aux taux d'évaluation du revenu procuré par les capitaux fixé par l'article R. 132-1. La circonstance  que l'allocataire n'aurait pas spontanément déclaré ces revenus est sans incidence sur l'application de ces dispositions.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
