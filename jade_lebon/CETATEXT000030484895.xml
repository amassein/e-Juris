<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030484895</ID>
<ANCIEN_ID>JG_L_2015_04_000000378893</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/48/48/CETATEXT000030484895.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère / 6ème SSR, 15/04/2015, 378893</TITRE>
<DATE_DEC>2015-04-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>378893</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère / 6ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN ; SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>Mme Florence Marguerite</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:378893.20150415</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Strasbourg, d'une part, d'annuler la décision du 16 juin 2011 par laquelle les Hôpitaux universitaires de Strasbourg ont refusé de lui verser l'aide à la reprise ou la création d'entreprise (ARCE) et, d'autre part, de condamner les Hôpitaux universitaires de Strasbourg à lui verser la somme de 22 179,40 euros au titre de cette aide, avec intérêts au taux légal et capitalisation des intérêts. <br/>
<br/>
              Par un jugement n° 1105797 du 28 février 2014, le tribunal administratif de Strasbourg a annulé la décision des Hôpitaux universitaires de Strasbourg du 16 juin 2011 et renvoyé M. A...devant cet établissement pour le calcul et le versement, avec intérêts au taux légal et capitalisation des intérêts, de la somme due au titre de l'ARCE.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 28 avril 2014, 23 juillet 2014 et 23 mars 2015 au secrétariat du contentieux du Conseil d'Etat, les Hôpitaux universitaires de Strasbourg demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement du tribunal administratif de Strasbourg ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de M.A... ; <br/>
<br/>
              3°) de mettre à la charge de M. A...la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code du travail ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Florence Marguerite, maître des requêtes en service extraordinaire, <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat des Hôpitaux universitaires de Strasbourg et à la SCP Lyon-Caen, Thiriez, avocat de M. A...;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il résulte des dispositions de l'article L. 5422-1 du code du travail que les travailleurs involontairement privés d'emploi, aptes au travail et recherchant un emploi, qui satisfont à des conditions d'âge et d'activité antérieure, ont droit à l'allocation d'assurance ; que, selon les articles L. 5422-2 et L. 5422-3 du même code, l'allocation d'assurance est accordée pour des durées limitées qui tiennent compte de l'âge et des activités antérieures de l'intéressé et calculée soit en fonction de la rémunération antérieurement perçue dans la limite d'un plafond, soit en fonction de la rémunération ayant servi au calcul des contributions ; que l'article L. 5422-20 du même code prévoit que les mesures d'application des dispositions relatives à ce régime d'assurance font l'objet d'accords conclus entre les organisations représentatives d'employeurs et de salariés ; qu'aux termes de l'article L. 5424-1 du même code : " Ont droit à une allocation d'assurance dans les conditions prévues aux articles L. 5422-2 et L. 5422-3 : / 1° Les agents fonctionnaires et non fonctionnaires de l'Etat et de ses établissements publics administratifs, les agents titulaires des collectivités territoriales ainsi que les agents statutaires des autres établissements publics administratifs ainsi que les militaires ; / 2° Les agents non titulaires des collectivités territoriales et les agents non statutaires des établissements publics administratifs autres que ceux de l'Etat et ceux mentionnés au 4° ainsi que les agents non statutaires des groupements d'intérêt public (...) " ; que l'article L. 5424-2 du même code prévoit que les employeurs mentionnés à l'article L. 5424-1, sauf s'ils ont adhéré au régime d'assurance, assurent la charge et la gestion de l'allocation d'assurance ;<br/>
<br/>
              2. Considérant qu'aux termes du paragraphe 5 de l'article 2 de la convention du 19 février 2009 relative à l'indemnisation du chômage, agréée par un arrêté du ministre de l'économie, de l'industrie et de l'emploi du 30 mars 2009 : " Afin de faciliter le reclassement des allocataires ayant un projet de reprise ou de création d'entreprise, il est prévu une aide spécifique au reclassement attribuée dans les conditions définies par le règlement général ci-annexé, dénommée aide à la reprise ou à la création d'entreprise " ; qu'aux termes de l'article 34 du règlement général annexé à la convention du 19 février 2009, agréé par le même arrêté : " Une aide à la reprise ou à la création d'entreprise est attribuée à l'allocataire qui justifie de l'obtention de l'aide aux chômeurs créateurs d'entreprise (ACCRE) visée aux articles L. 5141-1, L. 5141-2 et L. 5141-5 du code du travail. Cette aide ne peut être servie simultanément avec l'incitation à la reprise d'emploi par le cumul d'une allocation d'aide au retour à l'emploi avec une rémunération visée aux articles 28 à 32. Le montant de l'aide est égal à la moitié du montant du reliquat des droits restants : - soit au jour de la création ou de la reprise d'entreprise ; - soit, si cette date est postérieure, à la date d'obtention de l'ACCRE. L'aide donne lieu à deux versements égaux : - le premier paiement intervient à la date à laquelle l'intéressé réunit l'ensemble des conditions d'attribution de l'aide ; - le second paiement intervient 6 mois après la date de création ou de reprise d'entreprise, sous réserve que l'intéressé exerce toujours l'activité au titre de laquelle l'aide a été accordée. La durée que représente le montant de l'aide versée est imputée sur le reliquat des droits restant au jour de la reprise ou de la création d'entreprise. Cette aide ne peut être attribuée qu'une seule fois par ouverture de droits (...) " ;<br/>
<br/>
              3. Considérant qu'il résulte des dispositions du code du travail citées ci-dessus que les agents des employeurs publics, mentionnés à l'article L. 5424-1 de ce code, assurant la charge et la gestion de l'allocation d'assurance, ont droit à l'allocation d'assurance mais ne peuvent prétendre au bénéfice des autres aides créées par les accords conclus entre les organisations représentatives d'employeurs et de salariés ;<br/>
<br/>
              4. Considérant que l'aide à la reprise et à la création d'entreprise (ARCE) prévue à l'article 34 du règlement général annexé à la convention du 19 février 2009 relative à l'indemnisation du chômage constitue une allocation spécifique dont la nature, les conditions d'octroi et les modalités de versement se distinguent de celles de l'allocation d'aide au retour à l'emploi (ARE) définie par l'article 1er du règlement général, qui est l'allocation d'assurance à laquelle ont droit les agents des employeurs publics mentionnés à l'article L. 5424-1 du code du travail ; que, par suite, le tribunal administratif de Strasbourg a commis une erreur de droit en jugeant que M.A..., qui avait exercé comme chef de clinique des universités, assistant des hôpitaux, au centre hospitalier universitaire de Strasbourg jusqu'au terme de son contrat le 31 octobre 2010, avait droit au versement de l'aide à la reprise ou à la création d'entreprise (ARCE) ;<br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, les Hôpitaux universitaires de Strasbourg sont fondés à demander l'annulation du jugement du tribunal administratif de Strasbourg ;<br/>
<br/>
              6. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge des Hôpitaux universitaires de Strasbourg ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de M. A...la somme que les Hôpitaux universitaires de Strasbourg demandent au titre des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Strasbourg du 28 février 2014 est annulé.<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Strasbourg.<br/>
Article 3 : Les conclusions des Hôpitaux universitaires de Strasbourg et de M. A...présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée aux Hôpitaux universitaires de Strasbourg et à M. B... A.... <br/>
Copie en sera adressée pour information à la ministre de la décentralisation et de la fonction publique.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-12-03-01 FONCTIONNAIRES ET AGENTS PUBLICS. AGENTS CONTRACTUELS ET TEMPORAIRES. FIN DU CONTRAT. LICENCIEMENT. - DROIT À L'ALLOCATION D'ASSURANCE CHÔMAGE (ART. L. 5424-1 DU CODE DU TRAVAIL) - PRESTATIONS CONCERNÉES - EXCLUSION - ARCE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">66-10-02 TRAVAIL ET EMPLOI. POLITIQUES DE L'EMPLOI. INDEMNISATION DES TRAVAILLEURS PRIVÉS D'EMPLOI. - PRESTATIONS SERVIES AUX TRAVAILLEURS PRIVÉS D'EMPLOI - ARCE - TRAVAILLEURS ÉLIGIBLES - EXCLUSION - AGENTS DES EMPLOYEURS PUBLICS N'AYANT PAS ADHÉRÉ À LA CONVENTION D'ASSURANCE-CHÔMAGE [RJ1].
</SCT>
<ANA ID="9A"> 36-12-03-01 L'aide à la reprise et à la création d'entreprise (ARCE) prévue à l'article 34 du règlement général annexé à la convention du 19 février 2009 relative à l'indemnisation du chômage constitue une allocation spécifique dont la nature, les conditions d'octroi et les modalités de versement se distinguent de celles de l'allocation d'aide au retour à l'emploi (ARE) définie par l'article 1er du règlement général, qui est l'allocation d'assurance à laquelle ont droit les agents des employeurs publics mentionnés à l'article L. 5424-1 du code du travail. Les agents concernés ne sont, par suite, pas éligibles à l'ARCE.</ANA>
<ANA ID="9B"> 66-10-02 L'aide à la reprise et à la création d'entreprise (ARCE) prévue à l'article 34 du règlement général annexé à la convention du 19 février 2009 relative à l'indemnisation du chômage constitue une allocation spécifique dont la nature, les conditions d'octroi et les modalités de versement se distinguent de celles de l'allocation d'aide au retour à l'emploi (ARE) définie par l'article 1er du règlement général, qui est l'allocation d'assurance à laquelle ont droit les agents des employeurs publics mentionnés à l'article L. 5424-1 du code du travail. Les agents concernés ne sont, par suite, pas éligibles à l'ARCE.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., pour l'allocation de formation-reclassement, CE, 12 mai 1999, Etablissement public départemental de soins, d'adaptation et d'éducation, n° 184550, T. pp. 861-1060.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
