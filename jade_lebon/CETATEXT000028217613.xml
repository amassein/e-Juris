<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028217613</ID>
<ANCIEN_ID>JG_L_2013_11_000000352615</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/21/76/CETATEXT000028217613.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème / 8ème SSR, 19/11/2013, 352615, Publié au recueil Lebon</TITRE>
<DATE_DEC>2013-11-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>352615</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème / 8ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN ; LE PRADO</AVOCATS>
<RAPPORTEUR>M. Guillaume Odinet</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:352615.20131119</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 12 septembre et 7 décembre 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour la société Credemlux International, dont le siège est 6/10-12, avenue Pasteur, BP 1301, à Luxembourg ; la société demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 10DA01222 du 12 mai 2011 par lequel la cour administrative d'appel de Douai a rejeté son appel contre le jugement n° 0504571 du 29 mai 2007 du tribunal administratif de Lille rejetant sa demande tendant à la condamnation de la commune de Pernes-en-Artois à lui verser la somme de 1 687 263,21 euros, augmentée des intérêts au taux légal, en réparation des préjudices qu'elle affirme avoir subis en raison de l'illégalité des délibérations du 10 septembre 1987 du conseil municipal de cette commune et du non respect d'engagements pris par cette dernière ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Pernes-en-Artois la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des collectivités territoriales ;<br/>
<br/>
              Vu la loi n° 68-1250 du 31 décembre 1968 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Guillaume Odinet, Auditeur, <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la Société Credemlux International et à Me Le Prado, avocat de la commune de Pernes-en-Artois ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la société Banco di Napoli International a repris à la société Idis Finances la créance correspondant à un prêt d'un montant de 3,3 millions de Deutsche Marks, consenti le 11 septembre 1987 par cette société à la SNC La Clarence, dont la commune de Pernes-en-Artois s'était portée caution solidaire ; qu'après que la SNC La Clarence s'est trouvée dans l'impossibilité d'honorer sa dette, la commune de Pernes-en-Artois a refusé de rembourser les sommes dues par cette société ; que, par un jugement du 29 mai 2007, le tribunal administratif de Lille a rejeté la demande de la société Banco di Napoli International, devenue la société Credemlux International, tendant à la condamnation de la commune de Pernes-en-Artois à lui payer une somme de 1 687 263,21 euros en réparation du préjudice, consistant dans l'impossibilité d'obtenir le remboursement de sa créance, qu'elle affirme avoir subi en raison de l'illégalité des délibérations du 10 septembre 1987 par lesquelles le conseil municipal de la commune de Pernes-en-Artois a décidé d'accorder la garantie de la commune au prêt contracté par la SNC La Clarence et qui ont été annulées pour excès de pouvoir ; que, par une décision du 23 juillet 2010, le Conseil d'Etat, statuant au contentieux, a annulé l'arrêt du 11 juin 2009 par lequel la cour administrative d'appel de Douai a rejeté l'appel que la société Credemlux a formé contre ce jugement ; que cette société se pourvoit en cassation contre l'arrêt du 12 mai 2011 par lequel la cour administrative d'appel de Douai, à laquelle l'affaire a été renvoyée, a de nouveau rejeté son appel ;<br/>
<br/>
              2. Considérant, en premier lieu, qu'aux termes de l'article 1er de la loi du 31 décembre 1968 relative à la prescription des créances sur l'Etat, les départements, les communes et les établissements publics : " Sont prescrites, au profit de l'Etat, des départements et des communes, sans préjudice des déchéances particulières édictées par la loi, et sous réserve des dispositions de la présente loi, toutes créances qui n'ont pas été payées dans un délai de quatre ans à partir du premier jour de l'année suivant celle au cours de laquelle les droits ont été acquis. / (...) " ; qu'aux termes de l'article 2 de la même loi : " La prescription est interrompue par : / Toute demande de paiement ou toute réclamation écrite adressée par un créancier à l'autorité administrative, dès lors que la demande ou la réclamation a trait au fait générateur, à l'existence, au montant ou au paiement de la créance, alors même que l'administration saisie n'est pas celle qui aura finalement la charge du règlement ; / Tout recours formé devant une juridiction, relatif au fait générateur, à l'existence, au montant ou au paiement de la créance, quel que soit l'auteur du recours et même si la juridiction saisie est incompétente pour en connaître, et si l'administration qui aura finalement la charge du règlement n'est pas partie à l'instance ; / (...) / Un nouveau délai de quatre ans court à compter du premier jour de l'année suivant celle au cours de laquelle a eu lieu l'interruption. Toutefois, si l'interruption résulte d'un recours juridictionnel, le nouveau délai court à partir du premier jour de l'année suivant celle au cours de laquelle la décision est passée en force de chose jugée. " ; que l'article 3 de cette loi dispose : " La prescription ne court ni contre le créancier qui ne peut agir, soit par lui-même ou par l'intermédiaire de son représentant légal, soit pour une cause de force majeure, ni contre celui qui peut être légitimement regardé comme ignorant l'existence de sa créance ou de la créance de celui qu'il représente légalement. " ;<br/>
<br/>
              3. Considérant que la cour administrative d'appel a jugé, par un motif non contesté devant le juge de cassation, que le fait générateur de la créance correspondant à la réparation du préjudice subi par la société Credemlux International du fait de l'illégalité des délibérations du 10 septembre 1987 était le jugement du 5 décembre 1989 par lequel le tribunal administratif de Lille avait annulé ces délibérations pour excès de pouvoir ; qu'en en déduisant que la créance de la société Credemlux International se rattachait à l'année 1989 et que la prescription de cette créance était ainsi acquise au profit de la commune au 1er janvier 1994, alors qu'il ressortait des pièces du dossier qui lui était soumis que la date de notification de ce jugement à la société Credemlux International n'était pas établie, la cour a commis une erreur de droit ; que son arrêt doit, pour ce motif, et sans qu'il soit besoin d'examiner l'autre moyen du pourvoi venant à l'appui des mêmes conclusions, être annulé en tant qu'il a statué sur la responsabilité de la commune de Pernes-en-Artois à raison de l'illégalité des délibérations du 10 septembre 1987 ;<br/>
<br/>
              4. Considérant, en second lieu, qu'il ressort des termes mêmes des écritures d'appel de la société Credemlux International, notamment des mémoires enregistrés les 29 novembre 2010 et 21 mars 2011 au greffe de la cour administrative d'appel, qu'elle entendait engager la responsabilité quasi-délictuelle de la commune de Pernes-en-Artois à raison de la promesse que celle-ci aurait faite, par les délibérations du 10 septembre 1987, de se porter caution solidaire de la SNC La Clarence et n'aurait pas tenue ; que dès lors, en estimant que la société Credemlux International recherchait la responsabilité de la commune à raison du <br/>
non-respect par celle-ci du contrat de cautionnement qu'elle avait conclu le 11 septembre 1987, la cour a inexactement interprété ses écritures ; que son arrêt doit, pour ce motif, et sans qu'il soit besoin d'examiner l'autre moyen du pourvoi venant à l'appui des mêmes conclusions, être annulé en tant qu'il a statué sur la responsabilité de la commune de Pernes-en-Artois à raison de la promesse non tenue de se porter caution de la SNC La Clarence ;<br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que la société Credemlux International est fondée à demander l'annulation de l'arrêt qu'elle attaque en tant qu'il a statué sur ses conclusions à fin de condamnation de la commune de Pernes-en-Artois à lui verser une somme de 1 687 263,21 euros au titre de l'engagement de sa responsabilité quasi-délictuelle ; <br/>
<br/>
              6. Considérant qu'aux termes du second alinéa de l'article L. 821-2 du code de justice administrative : " Lorsque l'affaire fait l'objet d'un second pourvoi en cassation, le Conseil d'Etat statue définitivement sur cette affaire " ; qu'il y a lieu, par suite, de régler l'affaire au fond dans la mesure de l'annulation prononcée ; <br/>
<br/>
              Sur l'exception de prescription quadriennale opposée par la commune de Pernes-en-Artois :<br/>
<br/>
              7. Considérant que le fait générateur de la créance de la société Credemlux International est constitué par l'adoption illégale des délibérations du 10 septembre 1987 décidant d'accorder la garantie de la commune à l'emprunt de la SNC La Clarence et autorisant le maire à intervenir au contrat de prêt correspondant ; que toutefois, compte tenu, d'une part, de la nature de l'illégalité, tenant à un défaut d'information du conseil municipal, qui se trouve à l'origine de la nullité de cet engagement, d'autre part, du comportement de la commune de Pernes-en-Artois, qui n'a pas remis en cause auprès de la société Banco di Napoli International la validité de sa décision d'accorder sa garantie à l'emprunt de la SNC La Clarence pendant ses cinq premières années d'exécution et, enfin, de la circonstance que la société Banco di Napoli International n'a pas été mise en cause dans l'instance introduite devant le tribunal administratif de Lille par le déféré préfectoral des délibérations du 10 septembre 1987 et n'a pas été destinataire du jugement du 5 décembre 1989 par lequel le tribunal a annulé ces délibérations, cette société doit être regardée comme ayant légitimement ignoré l'existence de sa créance au titre de la responsabilité quasi-délictuelle de la commune à raison de l'illégalité des délibérations jusqu'au 4 juin 1992, date à laquelle il est établi que la société Banco di Napoli International avait connaissance du jugement du 5 décembre 1989 ; <br/>
<br/>
              8. Considérant qu'il résulte de ce qui précède qu'en vertu des dispositions précitées des articles 1er et 3 de la loi du 31 décembre 1968, la créance de la société Banco di Napoli International n'était pas prescrite à la date du 3 novembre 1993 ; qu'en vertu de l'article 2 de la même loi, l'introduction à cette date, par la société, d'un recours en tierce opposition contre le jugement du 5 décembre 1989 a eu pour effet d'interrompre la prescription, dès lors que ce recours était relatif tant au fait générateur qu'à l'existence de sa créance ; qu'en vertu du même article, un nouveau délai de quatre ans courait à compter du 1er janvier 2002, la cour administrative d'appel de Douai ayant statué sur le recours de la société Banco di Napoli International par un arrêt du 26 juillet 2001 ; que ce délai a été de nouveau interrompu par le pourvoi en cassation formé par cette société ; qu'un nouveau délai de quatre ans courait donc à compter du 1er janvier 2004, le Conseil d'Etat ayant définitivement statué sur le recours de la société Banco di Napoli International par une décision du 30 juillet 2003 ; qu'ainsi, le 4 avril 2005, date à laquelle la société Banco di Napoli International, devenue société Credemlux International, a demandé à la commune de Pernes-en-Artois de l'indemniser du préjudice résultant pour elle de l'illégalité des délibérations du 10 septembre 1987, sa créance n'était pas prescrite ; <br/>
<br/>
              Sur le principe de la responsabilité :<br/>
<br/>
              9. Considérant qu'il résulte de l'instruction que les délibérations du 10 septembre 1987 ont été annulées pour excès de pouvoir ; que l'illégalité de ces délibérations constitue une faute de nature à engager la responsabilité de la commune de Pernes-en-Artois ; <br/>
<br/>
              10. Considérant que le contrat de cautionnement conclu le 11 septembre 1987 n'est pas l'accessoire d'un contrat de prêt de caractère administratif, dès lors notamment qu'il ne résulte pas de l'instruction, contrairement à ce que soutient la société requérante, que la SNC La Clarence aurait agi pour le compte de la commune de Pernes-en-Artois, qu'il n'a pas pour objet l'exécution d'une mission de service public et qu'il ne comporte aucune clause exorbitante du droit commun ; qu'il a, dès lors, le caractère d'un contrat de droit privé ;<br/>
<br/>
              11. Considérant qu'il résulte d'une jurisprudence établie de la Cour de cassation qu'un contrat de cautionnement de droit privé conclu par un maire sans que le conseil municipal ait, au préalable, décidé d'accorder la garantie de la commune et l'ait autorisé à intervenir à cette fin au contrat de prêt correspondant est entaché de nullité ; qu'il en va de même dans l'hypothèse où la délibération décidant d'accorder la garantie de la commune est annulée pour excès de pouvoir et ainsi réputée n'être jamais intervenue ; que, par suite, l'annulation pour excès de pouvoir des délibérations du 10 septembre 1987 décidant d'accorder la garantie de la commune et autorisant le maire de Pernes-en-Artois à conclure le contrat de cautionnement avec la société Idis Finances a eu pour effet d'entraîner la nullité de ce contrat ; qu'ainsi, l'illégalité des délibérations du 10 septembre 1987 a fait perdre à la société Credemlux International, qui a racheté la créance de la société Idis Finances, le bénéfice de la garantie accordée par la commune lorsque la SNC La Clarence s'est trouvée dans l'impossibilité d'honorer sa dette, soit le remboursement du principal de l'emprunt et de la majeure partie des intérêts ;<br/>
<br/>
              12. Considérant, toutefois, qu'en acceptant d'octroyer un prêt d'une valeur de 3,3 millions de Deutsche Marks à la SNC La Clarence, pour la réalisation d'un projet dont il résulte de l'instruction que la viabilité apparaissait, dès sa conception, douteuse, avec pour seule garantie la caution conclue par une commune qui, au vu de ses capacités financières, ne pouvait manifestement pas assumer la charge du remboursement du principal de l'emprunt et de ses intérêts, la société Idis Finances a commis une grave imprudence, que la société Banco di Napoli International, devenue société Credemlux International, a choisi d'assumer en lui rachetant sa créance ; <br/>
<br/>
              13. Considérant, par ailleurs, que la promesse de la commune de se porter caution de la SNC La Clarence ne se sépare pas de l'engagement formel pris par le conseil municipal d'accorder sa garantie ;<br/>
<br/>
              14. Considérant que, dans les circonstances de l'espèce, il sera fait une juste appréciation de la réparation due à la société Credemlux International en condamnant la commune de Pernes-en-Artois à la réparation de la moitié du préjudice qu'elle a subi ; <br/>
<br/>
              15. Considérant qu'il résulte de ce qui précède que la société Credemlux International est fondée à soutenir que c'est à tort que, par le jugement du 29 mai 2007, le tribunal administratif de Lille a refusé de faire droit à ses conclusions présentées sur le terrain de la responsabilité quasi-délictuelle de la commune de Pernes-en-Artois ;<br/>
<br/>
              Sur le préjudice :<br/>
<br/>
              16. Considérant que la société Credemlux International demande la condamnation de la commune à lui verser une somme de 1 687 263,21 euros ; que ce montant non contesté, qui correspond au seul principal de l'emprunt non remboursé, ne résulte pas d'une évaluation exagérée du préjudice de la société ; que la commune de Pernes-en-Artois doit, par suite, être condamnée à lui verser, compte tenu du partage de responsabilité, une somme de 843 631,60 euros ; que la société a droit, comme elle le demande, à ce que cette somme porte intérêts au taux légal à compter de la date de présentation de sa demande préalable à la commune, le 4 avril 2005 ; <br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              17. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de Pernes-en-Artois la somme de 5 000 euros à verser à la société Credemlux International au titre des frais exposés par elle tant devant le tribunal administratif de Lille que devant la cour administrative d'appel de Douai et le Conseil d'Etat et non compris dans les dépens ; qu'en revanche, les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de cette société qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Douai du 12 mai 2011 est annulé en tant qu'il a statué sur les conclusions de la société Credemlux International tendant à la condamnation de la commune de Pernes-en-Artois à lui verser une somme de 1 687 263,21 euros au titre de l'engagement de sa responsabilité quasi-délictuelle. <br/>
Article 2 : La commune de Pernes-en-Artois versera à la société Credemlux International une somme de 843 631,60 euros, portant intérêt à compter du 4 avril 2005. <br/>
Article 3 : Le jugement du tribunal administratif de Lille du 29 mai 2007 est réformé en ce qu'il a de contraire à la présente décision.<br/>
Article 4 : La commune de Pernes-en-Artois versera à la société Credemlux International une somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 5 : Le surplus des conclusions du pourvoi, de la requête d'appel et de la demande de première instance de la société Credemlux International et les conclusions présentées par la commune de Pernes-en-Artois en cassation, en appel et en première instance au titre de l'article L. 761-1 du code de justice administrative sont rejetés.<br/>
Article 6 : La présente décision sera notifiée à la société Credemlux International et à la commune de Pernes-en-Artois.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">18-04-02-04 COMPTABILITÉ PUBLIQUE ET BUDGET. DETTES DES COLLECTIVITÉS PUBLIQUES - PRESCRIPTION QUADRIENNALE. RÉGIME DE LA LOI DU 31 DÉCEMBRE 1968. POINT DE DÉPART DU DÉLAI. - ABSENCE DE DÉPART - IGNORANCE LÉGITIME DE LA CRÉANCE (ART. 3) - EXISTENCE EN L'ESPÈCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">18-04-02-05 COMPTABILITÉ PUBLIQUE ET BUDGET. DETTES DES COLLECTIVITÉS PUBLIQUES - PRESCRIPTION QUADRIENNALE. RÉGIME DE LA LOI DU 31 DÉCEMBRE 1968. INTERRUPTION DU COURS DU DÉLAI. - EXISTENCE - SOCIÉTÉ DEMANDANT RÉPARATION DU PRÉJUDICE QU'ELLE ESTIME AVOIR SUBI DU FAIT DE DÉLIBÉRATIONS ILLÉGALES D'UN CONSEIL MUNICIPAL - RECOURS EN TIERCE OPPOSITION CONTRE LE JUGEMENT PRONONÇANT L'ANNULATION DE CES DÉLIBÉRATIONS - POURVOI EN CASSATION CONTRE L'ARRÊT RENDU SUR L'APPEL FORMÉ CONTRE LE NOUVEAU JUGEMENT.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">18-04-02-06 COMPTABILITÉ PUBLIQUE ET BUDGET. DETTES DES COLLECTIVITÉS PUBLIQUES - PRESCRIPTION QUADRIENNALE. RÉGIME DE LA LOI DU 31 DÉCEMBRE 1968. SUSPENSION DU DÉLAI. - IGNORANCE LÉGITIME DE LA CRÉANCE (ART. 3) - EXISTENCE EN L'ESPÈCE.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">54-07-01-09 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. QUESTIONS GÉNÉRALES. QUESTION PRÉJUDICIELLE POSÉE PAR LE JUGE ADMINISTRATIF. - QUESTION PRÉJUDICIELLE À L'AUTORITÉ JUDICIAIRE - ABSENCE - CAS DANS LEQUEL IL APPARAÎT MANIFESTEMENT, AU VU D'UNE JURISPRUDENCE ÉTABLIE, QUE LA CONTESTATION PEUT ÊTRE ACCUEILLIE PAR LE JUGE SAISI AU PRINCIPAL [RJ1] - APPLICATION EN L'ESPÈCE - ACTION EN RESPONSABILITÉ - FAUTE RÉSULTANT DE L'ILLÉGALITÉ DE DÉLIBÉRATIONS, ANNULÉES POUR EXCÈS DE POUVOIR, PAR LESQUELLES UNE COMMUNE S'EST PORTÉE GARANTE D'UN PRÊT QUE LE DÉBITEUR N'A PU HONORER - INCIDENCE DE L'ILLÉGALITÉ SUR LE CONTRAT DE PRÊT DE DROIT PRIVÉ - NULLITÉ DU CONTRAT.
</SCT>
<ANA ID="9A"> 18-04-02-04 Société demandant réparation du préjudice qu'elle estime avoir subi du fait de l'illégalité des délibérations par lesquelles une commune s'est portée garante d'un prêt contracté auprès de cette société par un emprunteur s'étant trouvé dans l'impossibilité d'honorer sa dette.,,,Compte tenu, d'une part, de la nature de l'illégalité entachant ces délibérations, tenant au défaut d'information du conseil municipal, d'autre part, du comportement de la commune, qui n'a pas spontanément remis en cause son engagement au cours des premières années de son exécution et, enfin, de ce que cette société n'a été ni mise en cause dans l'instance ayant conduit au jugement d'annulation ni rendue destinataire de celui-ci, cette société doit être regardée comme ayant légitimement ignoré l'existence de sa créance au titre de la responsabilité quasi-délictuelle de la commune, dont le fait générateur est l'adoption illégale de ces délibérations, jusqu'à la date à laquelle il est établi qu'elle a finalement eu, plusieurs années après son prononcé, connaissance du jugement prononçant leur annulation.</ANA>
<ANA ID="9B"> 18-04-02-05 Société demandant réparation du préjudice qu'elle estime avoir subi du fait de l'illégalité des délibérations par lesquelles une commune s'est portée garante d'un prêt contracté auprès de cette société par un emprunteur s'étant trouvé dans l'impossibilité d'honorer sa dette.,,,Le recours en tierce opposition formé par cette société contre le jugement prononçant l'annulation de ces délibérations a eu pour effet d'interrompre la prescription dès lors qu'il était relatif tant au fait générateur qu'à l'existence de sa créance. Un nouveau délai de quatre ans a couru à compter du 1er janvier de l'année suivant celle au cours de laquelle il a été statué en appel sur le nouveau jugement. Ce délai a de nouveau été interrompu par le pourvoi en cassation formé par la société. Un nouveau délai a couru à compter du 1er janvier de l'année suivant celle au cours de laquelle le Conseil d'Etat a définitivement statué sur ce pourvoi.</ANA>
<ANA ID="9C"> 18-04-02-06 Société demandant réparation du préjudice qu'elle estime avoir subi du fait de l'illégalité des délibérations par lesquelles une commune s'est portée garante d'un prêt contracté auprès de cette société par un emprunteur s'étant trouvé dans l'impossibilité d'honorer sa dette.,,,Compte tenu, d'une part, de la nature de l'illégalité entachant ces délibérations, tenant au défaut d'information du conseil municipal, d'autre part, du comportement de la commune, qui n'a pas spontanément remis en cause son engagement au cours des premières années de son exécution et, enfin, de ce que cette société n'a été ni mise en cause dans l'instance ayant conduit au jugement d'annulation ni rendue destinataire de celui-ci, cette société doit être regardée comme ayant légitimement ignoré l'existence de sa créance au titre de la responsabilité quasi-délictuelle de la commune, dont le fait générateur est l'adoption illégale de ces délibérations, jusqu'à la date à laquelle il est établi qu'elle a finalement eu, plusieurs années après son prononcé, connaissance du jugement prononçant leur annulation.</ANA>
<ANA ID="9D"> 54-07-01-09 Société demandant réparation du préjudice qu'elle estime avoir subi du fait de l'illégalité des délibérations, annulées pour excès de pouvoir, par lesquelles une commune s'est portée garante d'un prêt contracté auprès de cette société, qui n'a pu honorer sa dette.... ,,Il résulte d'une jurisprudence établie de la Cour de cassation qu'un contrat de cautionnement de droit privé conclu par un maire sans que le conseil municipal ait, au préalable, décidé d'accorder la garantie de la commune et l'ait autorisé à intervenir à cette fin au contrat de prêt correspondant est entaché de nullité. Il en va de même dans l'hypothèse où la délibération décidant d'accorder la garantie de la commune est annulée pour excès de pouvoir et ainsi réputée n'être jamais intervenue. Par suite, l'annulation pour excès de pouvoir de délibérations décidant d'accorder la garantie de la commune et autorisant le maire à conclure avec une société privée un contrat de cautionnement qui, n'étant pas l'accessoire d'un contrat de prêt de caractère administratif et ne comportant pas de clause exorbitante du droit commun, a le caractère d'un contrat de droit privé, a eu pour effet d'entraîner la nullité de ce contrat.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Section, 23 mars 2012, Fédération Sud Santé Sociaux, n° 331805, p. 102.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
