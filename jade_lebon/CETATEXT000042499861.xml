<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042499861</ID>
<ANCIEN_ID>JG_L_2020_11_000000434519</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/49/98/CETATEXT000042499861.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 1ère chambres réunies, 04/11/2020, 434519</TITRE>
<DATE_DEC>2020-11-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>434519</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUIN-PALAT, BOUCARD ; SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>Mme Yaël Treille</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:434519.20201104</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              L'Union nationale des syndicats autonomes (UNSA) et l'Union fédérale de l'industrie et de la construction (UFIC - UNSA) ont demandé à la cour administrative d'appel de Paris d'annuler pour excès de pouvoir, d'une part, l'arrêté du 27 décembre 2017 par lequel la ministre du travail a fixé la liste des organisations syndicales représentatives dans le bâtiment et, d'autre part, l'arrêté du 25 juillet 2018 modifiant l'arrêté du 27 décembre 2017 par lequel la ministre du travail a fixé la liste des organisations syndicales représentatives dans le bâtiment. Par une deuxième requête, l'UNSA et l'UFIC-UNSA ont demandé à la cour administrative d'appel de Paris d'annuler pour excès de pouvoir cet arrêté du 25 juillet 2018. Enfin, par une troisième requête, la Fédération générale Force Ouvrière Construction (FO Construction) a demandé à la cour administrative d'appel de Paris d'annuler pour excès de pouvoir le même arrêté du 22 décembre 2017 ou subsidiairement, d'annuler ses articles 2 et 3. Par un arrêt n°s 18PA00635, 18PA00701, 18PA03181 du 12 juillet 2019, la cour administrative d'appel, après avoir joint ces trois requêtes, a annulé ces deux arrêtés.<br/>
<br/>
              1° Sous le n° 434519, par un pourvoi, enregistré le 11 septembre 2019 au secrétariat du contentieux du Conseil d'Etat, la ministre du travail demande au Conseil d'Etat d'annuler cet arrêt.<br/>
<br/>
              2° Sous le n°434573, par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 12 septembre et 11 décembre 2019 au secrétariat du contentieux du Conseil d'Etat, la Confédération française de l'encadrement-Confédération générale des cadres (CFE-CGC) et le syndicat national CFE-CGC BTP demandent au Conseil d'Etat : <br/>
              1°) d'annuler cet arrêt ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter les requêtes de l'UNSA et de l'UFIC-UNSA ; <br/>
              3°) de mettre solidairement à la charge de l'UNSA et de l'UFIC-UNSA la somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
			....................................................................................<br/>
<br/>
              3° Sous le n° 434577, par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 12 septembre et 12 novembre 2019 au secrétariat du contentieux du Conseil d'Etat, la Fédération FO Construction demande au Conseil d'Etat : <br/>
              1°) d'annuler cet arrêt en tant qu'il annule l'arrêté du 25 juillet 2018 ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la requête de l'UNSA et de l'UFIC-UNSA ; <br/>
<br/>
              3°) de mettre solidairement à la charge de l'UNSA et de l'UFIC-UNSA la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - le code du travail ; <br/>
              - le code de justice administrative ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 9 octobre 2020, présentée par la ministre du travail, de l'emploi et de l'insertion ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Yaël Treille, auditeur,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de la Fédération FO Construction, à la SCP Gatineau, Fattaccini, Rebeyrol, avocat de la Confédération française de l'encadrement-Confédération générale des cadres (CFE-CGC) et le syndicat national CFE-CGC BTP ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond qu'à la demande de plusieurs organisations syndicales, la ministre du travail a pris, le 22 décembre 2017, un arrêté " fixant la liste des organisations syndicales reconnues représentatives dans le bâtiment ". Son article 1er dresse la liste des organisations syndicales représentatives dans le " champ couvert par l'ensemble des conventions collectives du bâtiment " : la Confédération générale du travail (CGT), la Confédération générale du travail-Force ouvrière (CGT-FO), la Confédération française démocratique du travail (CFDT), la Confédération française des travailleurs chrétiens (CFTC) et la Confédération française de l'encadrement-Confédération générale des cadres (CFE-CGC). Son article 2 définit, pour ce même secteur, les audiences respectives de ces organisations pour la négociation des accords collectifs du travail. Enfin, l'article 3 procède, pour le même secteur, à la définition des audiences respectives de ces organisations syndicales pour la négociation des accords collectifs du travail ne concernant que les ouvriers, quelle que soit la taille de leur entreprise. Par un arrêté du 25 juillet 2018, la ministre du travail a modifié les articles 2 et 3 de son précédent arrêté. Par un arrêt du 12 juillet 2019, la cour administrative d'appel de Paris, saisie, tant par l'Union nationale des syndicats autonomes (UNSA) et l'Union fédérale UNSA de l'industrie et de la construction (UFIC-UNSA) que par la Fédération FO Construction, de requêtes tendant à l'annulation pour excès de pouvoir de ces deux arrêtés, les a annulés. Par trois pourvois, qu'il y a lieu de joindre, la ministre du travail, d'une part, la CFE-CGC, la CFE-CGC BTP, d'autre part, et la Fédération FO Construction, enfin, demandent au Conseil d'Etat d'annuler cet arrêt. <br/>
<br/>
              2. D'une part, aux termes de l'article L. 2121-1 du code du travail : " La représentativité des organisations syndicales est déterminée d'après les critères cumulatifs suivants : / 1° Le respect des valeurs républicaines ; / 2° L'indépendance ; / 3° La transparence financière ; / 4° Une ancienneté minimale de deux ans dans le champ professionnel et géographique couvrant le niveau de négociation (...); / 5° L'audience établie selon les niveaux de négociation conformément aux articles L. 2122-1, L. 2122-5, L. 2122-6 et L. 2122-9 ; / 6° L'influence, prioritairement caractérisée par l'activité et l'expérience ; / 7° Les effectifs d'adhérents et les cotisations ". <br/>
<br/>
              3. D'autre part, aux termes de l'article L. 2121-2 du code du travail : " S'il y a lieu de déterminer la représentativité d'un syndicat ou d'une organisation professionnelle autre que ceux affiliés à l'une des organisations représentatives au niveau national, l'autorité administrative diligente une enquête. / L'organisation intéressée fournit les éléments d'appréciation dont elle dispose ". Aux termes de l'article L. 2122-11 de ce code : " Après avis du Haut Conseil du dialogue social, le ministre chargé du travail arrête la liste des organisations syndicales reconnues représentatives par branche professionnelle (...) ". <br/>
<br/>
              4. Il résulte de l'ensemble des dispositions citées aux points 2 et 3 que, sans préjudice de l'application des règles d'appréciation de la représentativité des organisations syndicales propres aux accords interbranches ou aux accords de fusion de branches, le ministre chargé du travail est compétent pour, s'il y a lieu, arrêter, sous le contrôle du juge administratif, la liste des organisations syndicales représentatives et leurs audiences respectives dans un périmètre utile pour une négociation en cours ou à venir, y compris lorsque celui-ci ne correspond pas à une " branche professionnelle " au sens de l'article L. 2122-11 du code du travail.<br/>
<br/>
              5. Par suite, en jugeant que la ministre du travail n'était pas compétente pour édicter l'arrêté du 22 décembre 2017 fixant la liste des organisations syndicales reconnues représentatives dans le bâtiment et l'arrêté du 25 juillet 2018 l'ayant modifié au seul motif que ce secteur, couvert par plusieurs conventions collectives nationales et locales n'ayant pas fait l'objet, en application de l'article L. 2261-32 du code du travail, d'une fusion préalable de leurs champs d'application, ne serait pas une " branche professionnelle " au sens de l'article L. 2122-11 du code du travail, la cour administrative d'appel de Paris a commis une erreur de droit. La ministre du travail, la CFE-CGC, la CFE-CGC BTP et la Fédération FO Construction sont donc fondées, sans qu'il soit besoin de se prononcer sur les autres moyens de leurs pourvois, à demander l'annulation de l'arrêt qu'elles attaquent.  <br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              7. Les requêtes nos 434519, 434573 et 434577 présentent à juger des questions semblables et ont fait l'objet d'une instruction commune. Il y a lieu de les joindre pour statuer par une seule décision. <br/>
<br/>
              8. Une intervention ne peut être admise que si son auteur s'associe soit aux conclusions de l'appelant, soit à celles de son défendeur. Il ressort des mémoires en intervention présentés par la Confédération générale du travail (CGT) et la Fédération nationale des salariés de la construction-bois-ameublement (FNSCBA-CGT) que, d'une part, elles demandent d'enjoindre au ministre chargé du travail de verser aux débats les résultats et la méthode employée pour arriver aux pourcentages de l'audience publiés et, d'autre part, elles indiquent s'en rapporter à la justice. Par suite, ces interventions ne sont pas recevables.<br/>
<br/>
              Sur la légalité externe :<br/>
<br/>
              9. Il résulte de ce qui a été dit au point 4 que la ministre du travail était compétente pour prendre les arrêtés attaqués. Par suite, le moyen tiré de ce que la ministre du travail n'était pas compétente pour les édicter doit être écarté. <br/>
<br/>
              Sur la légalité interne : <br/>
<br/>
              10. L'arrêté du 22 décembre 2017 est relatif à la représentativité des organisations syndicales dans le secteur du bâtiment, lequel est défini, à son article 1er, comme le " champ couvert par l'ensemble des conventions collectives du bâtiment ". Il ressort des pièces du dossier qu'il a été établi au vu de l'ensemble des procès-verbaux d'élections professionnelles organisées par des entreprises du bâtiment, y compris des entreprises ultra marines, ayant déclaré relever principalement soit de l'une des quatre conventions collectives nationales du bâtiment - la convention collective nationale des ouvriers des entreprises employant jusqu'à dix salariés (n° 1596) ; la convention collective des ouvriers des entreprises employant plus de dix salariés (n° 1597) ; la convention collective nationale des employés, techniciens, et agents de maîtrise du bâtiment (n° 2609) ; la convention collective nationale des cadres du bâtiment (n° 2420) -  soit d'une convention collective locale du bâtiment. L'arrêté du 25 juillet 2018, qui ne modifie que les articles 2 et 3 de l'arrêté du 22 décembre 2017, procède des mêmes procès-verbaux, à l'exclusion de ceux relatifs à des élections professionnelles organisées par des entreprises situées outre-mer. <br/>
<br/>
              11. En premier lieu, il résulte de ce qui a été dit au point précédent quant au périmètre couvert par l'arrêté du 22 décembre 2017 et celui du 25 juillet 2018 que la seule circonstance que l'UNSA soit mentionnée comme organisation syndicale représentative par l'arrêté de la ministre du travail du 20 juillet 2017 fixant la liste des organisations syndicales représentatives dans le champ de la convention collective nationale concernant les ouvriers employés par les entreprises du bâtiment occupant jusqu'à dix salariés n'est pas de nature à établir, par elle-même, que les arrêtés du 22 décembre 2017 et du 25 juillet 2018 seraient illégaux en ce qu'ils ne mentionnent pas l'UNSA parmi les organisations syndicales représentatives au niveau de l'ensemble du secteur du bâtiment. L'UNSA et l'UFIC-UNSA ne sont donc pas fondées à soutenir que les arrêtés qu'elles attaquent sont illégaux pour ce motif. <br/>
<br/>
              12. En deuxième lieu, le moyen, présenté par la Fédération FO Construction, tiré de ce que l'arrêté du 22 décembre 2017 serait illégal en raison de ce qu'il attribue à la <br/>
CGT-FO une audience qui n'est pas celle qui correspond à l'addition de ses résultats dans le périmètre résultant des champs couverts par les quatre conventions collectives nationales du bâtiment, ne peut qu'être écarté compte tenu de ce que, comme il vient d'être dit, le périmètre couvert par l'arrêté du 22 décembre 2017 excède celui couvert par les quatre conventions collectives nationales du bâtiment.  <br/>
<br/>
              13. En troisième lieu, si la Fédération FO Construction soutient que les arrêtés en litige n'ont pu légalement prendre en compte les suffrages recueillis dans le cadre des élections professionnelles organisées dans des entreprises, couvertes par des conventions collectives locales dont l'intitulé comporte la mention " bâtiment et travaux publics ", le ministre chargé du travail indique, sans être contredit, que l'activité des entreprises ayant déclaré adhérer à de telles conventions relève, en réalité, du secteur du bâtiment. Ce moyen doit donc être écarté. <br/>
<br/>
              14. En dernier lieu, si les quatre conventions collectives nationales du bâtiment stipulent qu'elles ne s'appliquent pas outre-mer, l'arrêté du 22 décembre 2017 a pu légalement prendre en compte les suffrages recueillis dans le cadre des élections professionnelles des entreprises du bâtiment situées outre-mer, dès lors que son périmètre, tel qu'il le définit à son article 1er, couvre, outre celui des quatre conventions collectives nationales, celui des conventions collectives locales du bâtiment au nombre desquelles figurent des conventions régissant les entreprises du bâtiment ultramarines. Le dernier moyen présenté par la Fédération FO du bâtiment doit donc être écarté.  <br/>
<br/>
              15. Il résulte de tout ce qui précède que l'UNSA, l'UFIC-UNSA et la Fédération FO Construction ne sont pas fondées à demander l'annulation pour excès de pouvoir de l'arrêté du 22 décembre 2017 fixant la liste des organisations syndicales reconnues représentatives dans le bâtiment et s'agissant seulement de l'UNSA et de l'UFIC-UNSA, de l'arrêté du 25 juillet 2018. <br/>
<br/>
              16. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'il soit fait droit aux conclusions présentées en première instance par l'UNSA et autre ainsi que par la Fédération FO Construction en sa qualité de requérante. Dans les circonstances de l'espèce, il n'y a pas lieu de faire droit aux conclusions présentées au même titre par la CFE-CGC et la CFE-CGC BTP en première instance et en cassation, la Fédération FO Construction en cassation, la Confédération française des travailleurs chrétiens-Fédération nationale du bâtiment (TP CFTC) en première instance, la Fédération FO- construction et la CGT-FO en première instance en leur qualité de défendeur. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt n°s 18PA00635, 18PA00701, 18PA03181 de la cour administrative d'appel de Paris du 12 juillet 2019 est annulé. <br/>
Article 2 : Les interventions de la CGT et de la FNSCBA-CGT ne sont pas admises.<br/>
Article 3 : Les requêtes nos 434519, 434573 et 434577 sont rejetées. <br/>
Article 4 : Les conclusions présentées par les parties au titre de l'article L. 761-1 du code de justice administrative, en première instance et en cassation sont rejetées. <br/>
Article 5 : La présente décision sera notifiée à la Confédération française de l'encadrement-Confédération générale des cadres (CFE-CGC), au syndicat national CFE-CGC BTP, à l'Union nationale des syndicats autonomes (UNSA), à l'Union fédérale UNSA de l'industrie et de la construction, à la Fédération FO Construction, à la Confédération générale du travail (CGT) et à la Fédération nationale des salariés de la construction-bois-ameublement (FNSCBA-CGT), à la Confédération française des travailleurs chrétiens-Fédération nationale du bâtiment (TP CFTC) et à la ministre du travail, de l'emploi et de l'insertion.<br/>
Copie en sera adressée à la Confédération française démocratique du travail (CFDT). <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">66-02 TRAVAIL ET EMPLOI. CONVENTIONS COLLECTIVES. - COMPÉTENCE DU MINISTRE DU TRAVAIL POUR ARRÊTER LA LISTE DES ORGANISATIONS SYNDICALES REPRÉSENTATIVES ET LEURS AUDIENCES RESPECTIVES DANS UN PÉRIMÈTRE UTILE POUR UNE NÉGOCIATION EN COURS OU À VENIR - EXISTENCE, Y COMPRIS LORSQUE CELUI-CI NE CORRESPOND PAS À UNE BRANCHE PROFESSIONNELLE AU SENS DE L'ARTICLE L. 2122-11 DU CODE DU TRAVAIL [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">66-05-01 TRAVAIL ET EMPLOI. SYNDICATS. REPRÉSENTATIVITÉ. - COMPÉTENCE DU MINISTRE DU TRAVAIL POUR ARRÊTER LA LISTE DES ORGANISATIONS SYNDICALES REPRÉSENTATIVES ET LEURS AUDIENCES RESPECTIVES DANS UN PÉRIMÈTRE UTILE POUR UNE NÉGOCIATION EN COURS OU À VENIR - EXISTENCE, Y COMPRIS LORSQUE CELUI-CI NE CORRESPOND PAS À UNE BRANCHE PROFESSIONNELLE AU SENS DE L'ARTICLE L. 2122-11 DU CODE DU TRAVAIL [RJ1].
</SCT>
<ANA ID="9A"> 66-02 Il résulte des articles L. 2121-1, L. 2121-2 et L. 2122-11 du code du travail que, sans préjudice de l'application des règles d'appréciation de la représentativité des organisations syndicales propres aux accords interbranches ou aux accords de fusion de branches, le ministre chargé du travail est compétent pour, s'il y a lieu, arrêter, sous le contrôle du juge administratif, la liste des organisations syndicales représentatives et leurs audiences respectives dans un périmètre utile pour une négociation en cours ou à venir, y compris lorsque celui-ci ne correspond pas à une branche professionnelle au sens de l'article L. 2122-11 du code du travail.</ANA>
<ANA ID="9B"> 66-05-01 Il résulte des articles L. 2121-1, L. 2121-2 et L. 2122-11 du code du travail que, sans préjudice de l'application des règles d'appréciation de la représentativité des organisations syndicales propres aux accords interbranches ou aux accords de fusion de branches, le ministre chargé du travail est compétent pour, s'il y a lieu, arrêter, sous le contrôle du juge administratif, la liste des organisations syndicales représentatives et leurs audiences respectives dans un périmètre utile pour une négociation en cours ou à venir, y compris lorsque celui-ci ne correspond pas à une branche professionnelle au sens de l'article L. 2122-11 du code du travail.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., s'agissant des organisations professionnelles d'employeurs, CE, décision du même jour, Ministre du travail - Fédération française du bâtiment, n°s 434518 434574, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
