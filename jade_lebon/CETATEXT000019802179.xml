<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000019802179</ID>
<ANCIEN_ID>JG_L_2008_11_000000292948</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/19/80/21/CETATEXT000019802179.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème et 3ème sous-sections réunies, 19/11/2008, 292948, Publié au recueil Lebon</TITRE>
<DATE_DEC>2008-11-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>292948</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème et 3ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Stirn</PRESIDENT>
<AVOCATS>SCP LESOURD</AVOCATS>
<RAPPORTEUR>M. Vincent  Daumas</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Escaut Nathalie</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 28 avril et 17 août 2006 au secrétariat du contentieux du Conseil d'Etat, présentés pour la SOCIETE GETECOM, dont le siège est 24, rue de Suresnes à Paris (75008), représentée par son président-directeur général en exercice ; la SOCIETE GETECOM demande au Conseil d'Etat d'annuler l'arrêt du 3 février 2006 par lequel la cour administrative d'appel de Paris, d'une part, a rejeté sa requête tendant, en premier lieu, à l'annulation du jugement du 13 novembre 2003 du tribunal administratif de Paris rejetant partiellement ses demandes qui tendaient à la décharge des cotisations supplémentaires d'impôt sur les sociétés auxquelles elle a été assujettie au titre des exercices clos en 1990, 1991 et 1992 et, en second lieu, à la décharge de ces impositions et, d'autre part, faisant droit à l'appel formé par le ministre de l'économie, des finances et de l'industrie, a remis à sa charge l'intégralité des impositions supplémentaires contestées ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et son premier protocole additionnel ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu la loi n° 2004-1485 du 30 décembre 2004, notamment son article 43 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Vincent Daumas, Maître des Requêtes,<br/>
<br/>
              - les observations de la SCP Lesourd, avocat de la SOCIETE GETECOM, <br/>
<br/>
              - les conclusions de Mme Nathalie Escaut, Commissaire du gouvernement ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la SOCIETE GETECOM, qui exerçait une activité d'expertise comptable, a fait l'objet d'une vérification de comptabilité portant sur les exercices clos les 30 septembre des années 1990, 1991 et 1992, à la suite de laquelle l'administration fiscale lui a notifié plusieurs redressements en matière d'impôt sur les sociétés, procédant notamment de la remise en cause, d'une part, de la comptabilisation de certains honoraires en tant qu'avances reçues de ses clients, d'autre part, du montant porté en comptabilité d'honoraires dont la contrepartie consistait en des travaux qu'elle sous-traitait ; qu'après avoir vainement contesté les impositions supplémentaires découlant de ces redressements devant l'administration, la société a porté le litige devant le tribunal administratif de Paris qui, par un jugement du 13 novembre 2003, a partiellement fait droit à sa demande en prononçant, par application de la règle de correction symétrique des bilans, une décharge d'impôt sur les sociétés correspondant à une réduction des bases prises en compte au titre de l'année 1990 ; que la cour administrative d'appel de Paris a, d'une part, sur appel du ministre de l'économie, des finances et de l'industrie, remis cette somme à la charge de la société et, d'autre part, rejeté la requête de cette dernière dirigée contre la partie du jugement rejetant le surplus de ses conclusions ; que la SOCIETE GETECOM se pourvoit en cassation contre l'arrêt de la cour administrative d'appel en date du 3 février 2006 ;<br/>
<br/>
              Sur l'arrêt attaqué, en tant qu'il statue sur le recours du ministre de l'économie, des finances et de l'industrie :<br/>
<br/>
              Considérant qu'il appartient au juge d'appel, saisi par l'effet dévolutif, d'examiner, dans la limite des conclusions dont il est saisi, les moyens soulevés par l'appelant tant devant lui que devant les premiers juges, à l'exception, s'agissant de ces derniers, de ceux qui ont été expressément écartés par le jugement attaqué et qui ne sont pas repris en appel ; que dans le cas où le juge d'appel, dans les mêmes conditions, accueille l'un des moyens de l'appelant, il lui appartient également, avant de faire droit à ses conclusions, de répondre à tous les moyens opérants présentés par l'intimé devant les premiers juges, à la seule exception de ceux qu'il a expressément abandonnés ; qu'il en va ainsi même dans l'hypothèse où ces moyens ont été expressément écartés par le jugement attaqué et ne sont pas repris en appel par l'intimé ; <br/>
<br/>
              Considérant que la SOCIETE GETECOM avait présenté devant le tribunal administratif de Paris un moyen tiré de la méconnaissance de l'article L. 80 B du livre des procédures fiscales, auquel celui-ci avait expressément répondu ; que, pour faire droit aux conclusions du ministre, la cour administrative d'appel, après avoir censuré les motifs par lesquels les premiers juges avaient accueilli le moyen tiré de l'erreur commise par l'administration dans l'application de la correction symétrique des bilans, n'a pas répondu à ce moyen, qui n'était pas inopérant ; qu'il résulte de ce qui vient d'être dit qu'alors même que la société n'avait pas repris ce moyen dans ses écritures d'appel, la cour administrative d'appel était tenue d'y répondre ; qu'elle a, par suite, en omettant de le faire, entaché son arrêt d'irrégularité ; que la société est fondée, pour ce motif, à demander l'annulation des articles 2, 3 et 4 de l'arrêt attaqué ;<br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de faire application des dispositions de l'article L. 821-2 du code de justice administrative et de régler l'affaire au fond ;<br/>
<br/>
              Considérant qu'il résulte de l'instruction que la SOCIETE GETECOM a soustrait des honoraires perçus de ses clients, à raison des prestations d'expertise comptable qu'elle leur fournissait, une part correspondant, selon elle, à des travaux non encore exécutés à la clôture de l'exercice, soit un montant de 1 444 483 F (220 210,01 euros) au titre de l'exercice clos le 30 septembre 1990, premier exercice non prescrit ; que, par le jugement attaqué, le tribunal administratif de Paris a jugé que l'administration avait à bon droit réintégré cette somme au résultat imposable, dès lors que la société ne justifiait pas que cette déduction correspondait bien à des travaux non encore exécutés à la clôture de l'exercice ; que, toutefois, les premiers juges ont admis, sur demande de la société, de procéder sur ce redressement à la déduction d'une somme de 1 587 610 F (242 029,58 euros) représentant la sous-estimation de même nature affectant le bilan d'ouverture de l'exercice clos le 30 septembre 1990 ;<br/>
<br/>
              Considérant qu'aux termes des 1. et 2. de l'article 38 du code général des impôts, applicable en matière d'impôts sur les sociétés en vertu de l'article 209 du même code, le bénéfice imposable est le bénéfice net, lui-même (...) constitué par la différence entre les valeurs de l'actif net à la clôture et à l'ouverture de la période dont les résultats doivent servir de base à l'impôt (...). L'actif net s'entend de l'excédent des valeurs d'actif sur le total formé au passif par les créances des tiers, les amortissements et les provisions justifiées ; qu'aux termes de l'article 43 de la loi du 30 décembre 2004, portant loi de finances rectificative pour 2004 : I. - Le code général des impôts est ainsi modifié : / 1° Après le 4 de l'article 38, il est inséré un 4 bis ainsi rédigé : / 4 bis. Pour l'application des dispositions du 2, pour le calcul de la différence entre les valeurs de l'actif net à la clôture et à l'ouverture de l'exercice, l'actif net d'ouverture du premier exercice non prescrit déterminé, sauf dispositions particulières, conformément aux premier et deuxième alinéas de l'article L. 169 du livre des procédures fiscales ne peut être corrigé des omissions ou erreurs entraînant une sous-estimation ou surestimation de celui-ci. / Les dispositions du premier alinéa ne s'appliquent pas lorsque l'entreprise apporte la preuve que ces omissions ou erreurs sont intervenues plus de sept ans avant l'ouverture du premier exercice non prescrit. / Elles ne sont pas non plus applicables aux omissions ou erreurs qui résultent de dotations aux amortissements excessives au regard des usages mentionnés au 2° du 1 de l'article 39 déduites sur des exercices prescrits ou de la déduction au cours d'exercices prescrits de charges qui auraient dû venir en augmentation de l'actif immobilisé. / Les corrections des omissions ou erreurs mentionnées aux deuxième et troisième alinéas restent sans influence sur le résultat imposable lorsqu'elles affectent l'actif du bilan. Toutefois, elles ne sont prises en compte ni pour le calcul des amortissements ou des provisions, ni pour la détermination du résultat de cession. ; / (...) IV. - Sous réserve des décisions de justice passées en force de chose jugée et de l'application des dispositions des deuxième, troisième et quatrième alinéas du 4 bis de l'article 38 du code général des impôts, les impositions établies avant le 1er janvier 2005 (...) sont réputées régulières en tant qu'elles seraient contestées par le moyen tiré de ce que le contribuable avait la faculté de demander la correction des écritures du bilan d'ouverture du premier exercice non prescrit (...) ;<br/>
<br/>
              Considérant que la SOCIETE GETECOM soutient que le IV de l'article 43 de la loi du 30 décembre 2004 est incompatible avec l'article 1er du premier protocole additionnel à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, en tant que ces dispositions prévoient l'application du nouveau 4 bis de l'article 38 du code général des impôts aux litiges concernant des impositions établies avant le 1er janvier 2005 et n'ayant pas encore donné lieu à une décision de justice devenue définitive ; que, cependant, la société ne saurait prétendre au bénéfice de ces stipulations que si elle peut faire état de la propriété d'un bien qu'elles ont pour objet de protéger et à laquelle il aurait été porté atteinte ; qu'à défaut de créance certaine, l'espérance légitime d'obtenir la restitution d'une somme d'argent doit être regardée comme un bien au sens de ces stipulations ; que toutefois, si la société se prévaut en l'espèce de l'état du droit résultant de la décision n° 230169 du Conseil d'Etat, statuant au contentieux, du 7 juillet 2004, qui aurait dû conduire à la décharge d'une partie des impositions objets du présent litige dès lors qu'elle permettait au contribuable d'obtenir la correction des écritures du bilan d'ouverture du premier exercice non prescrit, cette décision est revenue rétroactivement, en ouvrant cette faculté au contribuable, sur une règle issue d'une jurisprudence ancienne et constante ; qu'en outre, il résulte de l'instruction que le gouvernement avait fait connaître, dès avant le dépôt le 17 novembre 2004 sur le bureau de l'Assemblée nationale du projet de loi ayant conduit à l'adoption des dispositions de l'article 43 de la loi du 30 décembre 2004, son intention de limiter les conséquences de la décision du Conseil d'Etat ; que dans ces conditions, la société ne saurait se fonder sur cette décision pour se prévaloir d'une espérance légitime d'obtenir le remboursement d'une partie des sommes qui font l'objet du présent litige ; qu'ainsi, elle ne peut utilement invoquer les stipulations de l'article 1er du premier protocole additionnel à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, dans le champ desquelles elle n'entre pas ;<br/>
<br/>
              Considérant que, conformément aux prescriptions du IV de l'article 43 de la loi du 30 décembre 2004  précitées, la SOCIETE GETECOM, qui ne se prévaut, en tout état de cause, d'aucune des exceptions mentionnées aux deuxième et troisième alinéas du 4 bis de l'article 38 du code général des impôts, n'avait pas la faculté de demander la correction des écritures du bilan d'ouverture du premier exercice non prescrit ; que, par suite, le ministre est fondé à soutenir que c'est à tort que les premiers juges ont réduit les bases assignées à la société au titre de l'année 1990 d'une somme de 1 587 610 F (242 029,58 euros) au motif d'une incorrecte application, par l'administration, de la correction symétrique des bilans ; <br/>
<br/>
              Considérant, toutefois, qu'il appartient au Conseil d'Etat, saisi de l'ensemble du litige par l'effet dévolutif de l'appel, d'examiner les autres moyens soulevés par la SOCIETE GETECOM devant le tribunal administratif de Paris et en appel ;<br/>
<br/>
              Considérant que, si la société entend se prévaloir, sur le fondement de l'article L. 80 B du livre des procédures fiscales, des mentions du rapport du vérificateur rédigé dans le cadre d'une précédente procédure de contrôle ayant porté sur les exercices clos entre 1979 et 1981, qui se serait conclue par un avis d'absence de redressement, il résulte des termes mêmes de ce rapport, qu'elle cite sans le produire mais qui ne sont pas contestés par l'administration, que le vérificateur n'a pas entendu prendre position expressément sur la valeur des pièces présentées par la société pour justifier les montants soustraits, au titre de travaux non encore exécutés à la clôture de l'exercice, des honoraires perçus au cours de celui-ci ; <br/>
<br/>
              Considérant qu'il y a lieu, par adoption des motifs retenus par les premiers juges, d'écarter les autres moyens articulés par la SOCIETE GETECOM, qui ne comportent aucun élément de fait ou de droit nouveau par rapport à l'argumentation développée devant le tribunal administratif de Paris ; <br/>
<br/>
              Considérant qu'il résulte de ce qui précède que le ministre est fondé à soutenir que c'est à tort que les premiers juges ont réduit les bases d'imposition à l'impôt sur les sociétés assignées à la SOCIETE GETECOM au titre de l'année 1990 à concurrence d'une somme de 1 587 610 F  (soit 242 029,58 euros) ; <br/>
<br/>
              Sur l'arrêt attaqué, en tant qu'il statue sur la requête de la SOCIETE GETECOM :<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que l'administration, au terme de la vérification de comptabilité à laquelle la société a été soumise, a constaté que celle-ci n'avait comptabilisé qu'une partie des honoraires payés par les clients d'un cabinet comptable à laquelle elle sous-traitait une partie de ses travaux ; que la société soutenait devant la cour administrative d'appel, pour justifier cette différence, qu'elle avait procédé à la déduction de diverses charges du montant brut des honoraires perçus, avant de porter le montant net de ces honoraires dans sa comptabilité ; qu'elle n'a toutefois produit devant la cour, à l'appui de ce moyen, aucune pièce de nature à établir la réalité de ses allégations ; que, par suite, la cour, en jugeant que la société ne produisait aucun justificatif, notamment comptable, des diverses charges qu'elle prétendait déduire, n'a pas entaché son arrêt de dénaturation et n'a pas commis d'erreur de droit par voie de conséquence, ainsi qu'il est soutenu, au regard des règles de dévolution de la charge de la preuve ou des dispositions du 1 de l'article 38 du code général des impôts ; que, par suite, la société n'est pas fondée à demander l'annulation de l'article 1er de l'arrêt attaqué, par lequel la cour a rejeté sa requête d'appel ;<br/>
<br/>
              Sur les conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              Considérant que ces dispositions font obstacle à ce que l'Etat, qui n'est pas, dans la présente instance, la partie perdante, verse à la SOCIETE GETECOM la somme que celle-ci demande au titre des frais exposés par elle, tant en cassation qu'en appel, et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : Les articles 2, 3 et 4 de l'arrêt de la cour administrative d'appel de Paris en date du 3 février 2006 sont annulés.<br/>
<br/>
Article 2 : Les articles 1 et 2 du jugement du tribunal administratif de Paris en date du 13 novembre 2003 sont annulés.<br/>
<br/>
Article 3 : Les conclusions de la société SOCIETE GETECOM tendant à la décharge du supplément d'impôt sur les sociétés mis à sa charge au titre de l'année 1990, correspondant à une réduction de ses bases d'impositions à hauteur d'une somme de 1 587 610 F  (242 029,58 euros), sont rejetées.<br/>
<br/>
Article 4 : Le surplus des conclusions présentées par la SOCIETE GETECOM devant le Conseil d'Etat, ainsi que ses conclusions d'appel tendant à l'application de l'article L. 761-1 du code de justice administrative, sont rejetés.<br/>
<br/>
Article 5 : La présente décision sera notifiée à la SOCIETE GETECOM et au ministre du budget, des comptes publics et de la fonction publique.<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">26-055-02-01 DROITS CIVILS ET INDIVIDUELS. CONVENTION EUROPÉENNE DES DROITS DE L'HOMME. DROITS GARANTIS PAR LES PROTOCOLES. DROIT AU RESPECT DE SES BIENS (ART. 1ER DU PREMIER PROTOCOLE ADDITIONNEL). - CHAMP D'APPLICATION - BIEN AU SENS DE CES STIPULATIONS - ESPÉRANCE LÉGITIME D'OBTENIR LA RECONNAISSANCE D'UNE CRÉANCE - A) INCLUSION [RJ1] - B) ABSENCE EN L'ESPÈCE - CONTRIBUABLE SE PRÉVALANT DE L'ÉTAT DU DROIT RÉSULTANT D'UNE DÉCISION DU CONSEIL D'ETAT, ALORS QUE CETTE DÉCISION OPÈRE UN REVIREMENT DE JURISPRUDENCE, QUE LE LÉGISLATEUR EST RAPIDEMENT INTERVENU POUR LA REMETTRE EN CAUSE RÉTROACTIVEMENT ET QUE LE GOUVERNEMENT AVAIT FAIT CONNAÎTRE, AVANT MÊME LE DÉPÔT DU PROJET DE LOI DEVANT LE PARLEMENT, SON INTENTION D'EN LIMITER LES CONSÉQUENCES [RJ2].
</SCT>
<ANA ID="9A"> 26-055-02-01 a) A défaut de créance certaine, l'espérance légitime d'obtenir la restitution d'une somme d'argent doit être regardée comme un bien au sens des stipulations de l'article 1er du premier protocole additionnel à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales. b) Supplément d'impôt mis à la charge d'un contribuable en conséquence de l'application du principe d'intangibilité du bilan d'ouverture du premier exercice non prescrit. Le contribuable se prévaut de l'état du droit résultant de la décision n° 230169 du Conseil d'Etat, statuant au contentieux, du 7 juillet 2004, qui aurait dû conduire à la décharge d'une partie des impositions objets du litige dès lors qu'elle lui permettait d'obtenir la correction des écritures du bilan d'ouverture du premier exercice non prescrit. Cependant, en ouvrant une telle possibilité, cette décision est revenue rétroactivement sur une règle issue d'une jurisprudence ancienne et constante, que le législateur a rétablie, y compris pour le passé, en adoptant les dispositions de l'article 43 de la loi n° 2004-1485 du 30 décembre 2004 de finances rectificative pour 2004. En outre, le gouvernement avait fait connaître, dès avant le dépôt le 17 novembre 2004 sur le bureau de l'Assemblée nationale du projet de loi, son intention de limiter les conséquences de la décision du Conseil d'Etat. Dans ces conditions, le contribuable ne saurait se fonder sur cette décision pour se prévaloir d'une espérance légitime d'obtenir le remboursement d'une partie des sommes en litige à laquelle les dispositions en question auraient porté atteinte. Il ne peut donc utilement invoquer à leur encontre les stipulations de l'article 1er du premier protocole additionnel à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, dans le champ desquelles il n'entre pas.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr. CEDH, 20 novembre 1995, Pressos Compania Naviera SA c/ Belgique, n° 17849/91, série A n° 332, § 31-32 ; s'agissant spécialement de la matière fiscale, CEDH, 16 avril 2002, SA Dangeville c/ France, n° 36677/97, Rec. 2002-III, RJF 7/02 n° 889, § 44-48., ,[RJ2] Rappr. CEDH, 28 septembre 2004, Kopecky c/ Slovaquie, n° 44912/98, Rec. 2004-IX, § 48-52.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
