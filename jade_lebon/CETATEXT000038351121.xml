<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038351121</ID>
<ANCIEN_ID>JG_L_2019_04_000000426820</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/35/11/CETATEXT000038351121.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 5ème chambres réunies, 08/04/2019, 426820</TITRE>
<DATE_DEC>2019-04-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>426820</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GOUZ-FITOUSSI ; SCP MARLANGE, DE LA BURGADE ; SCP SEVAUX, MATHONNET</AVOCATS>
<RAPPORTEUR>Mme Laure Durand-Viel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Louis Dutheillet de Lamothe</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:426820.20190408</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par un arrêté du 3 novembre 2015, le maire de Strasbourg a accordé à la société Frank immobilier un permis de construire pour l'édification de 7 bâtiments comportant 226 logements rue Jean Mentelin à Koenigshoffen. L'association " Koenigshoffen Demain " a présenté un recours gracieux contre cette décision, qui a été explicitement rejeté par le maire de Strasbourg le 29 janvier 2016. Elle a alors demandé au tribunal administratif de Strasbourg, d'une part, avant dire droit, d'ordonner à la commune de Strasbourg de produire divers documents et de faire une enquête à la barre, et d'autre part, d'annuler l'arrêté du 3 novembre 2015 ainsi que la décision du 29 janvier 2016 portant rejet de son recours gracieux. <br/>
<br/>
              Par un jugement n° 1601796 du 5 octobre 2017, le tribunal administratif de Strasbourg a rejeté ses demandes.<br/>
<br/>
              Par une décision n° 416122 du 21 novembre 2018, le Conseil d'Etat, statuant au contentieux, a annulé ce jugement et renvoyé l'affaire devant le tribunal administratif de Strasbourg.<br/>
<br/>
              Par une requête du 6 décembre 2018, l'association " Koenigshoffen Demain " a saisi le juge des référés du tribunal administratif de Strasbourg d'une demande tendant à ordonner, sur le fondement de l'article L. 521-1 du code de justice administrative, la suspension de l'arrêté portant permis de construire du 3 novembre 2015 du maire de Strasbourg et du rejet implicite de son recours gracieux.<br/>
<br/>
              Par une ordonnance n° 1807601 du 21 décembre 2018, le juge des référés du tribunal administratif de Strasbourg a rejeté cette requête.<br/>
<br/>
              Par un pourvoi et un mémoire en réplique, enregistrés les 4 janvier et 6 février 2019 au secrétariat du contentieux du Conseil d'Etat, l'association " Koenigshoffen Demain " demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) réglant l'affaire au titre de la procédure de référé, d'ordonner la suspension de l'arrêté portant permis de construire délivré par le maire de Strasbourg le 3 novembre 2015 à la société Frank Immobilier ; <br/>
<br/>
              3°) de mettre à la charge de la société Frank immobilier et de la commune de Strasbourg la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              -	le code de l'urbanisme ;<br/>
              -	le code de l'environnement ;<br/>
              -	la décision n° 416122 du Conseil d'Etat, statuant au contentieux, du 21 novembre 2018 ;<br/>
              -	le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Laure Durand-Viel, auditeur,  <br/>
<br/>
              - les conclusions de M. Louis Dutheillet de Lamothe, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gouz-Fitoussi, avocat de l'association " Koenigshoffen Demain ", à la SCP Sevaux, Mathonnet, avocat de la commune de Strasbourg et à la SCP Marlange, de la Burgade, avocat de la société Frank immobilier.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1.	Il ressort des pièces du dossier soumis au juge des référés que le maire de Strasbourg a accordé à la société Frank immobilier, le 3 novembre 2015, un permis de construire pour l'édification de 7 bâtiments comportant 226 logements sur un terrain sur lequel avait précédemment été exploitée une imprimerie, dans le quartier de Koenigshoffen. Elle a saisi le tribunal administratif de Strasbourg d'une demande tendant à l'annulation de cet arrêté. Le jugement du 5 octobre 2017 par lequel le tribunal administratif de Strasbourg a rejeté cette demande a été annulé par une décision n° 416122 du 21 novembre 2018 du Conseil d'Etat, statuant au contentieux, qui a renvoyé l'affaire au tribunal administratif de Strasbourg. Le 6 décembre 2018, l'association " Koenigshoffen Demain " a saisi le juge des référés du tribunal administratif de Strasbourg d'une demande tendant à la suspension de l'exécution de l'arrêté litigieux. Le juge des référés a rejeté sa demande par une ordonnance du 21 décembre 2018 contre laquelle l'association se pourvoit en cassation.<br/>
<br/>
              2.	En premier lieu, en l'absence de dispositions législatives ou réglementaires fixant les conditions dans lesquelles il doit être statué après l'annulation d'une décision de justice, ni le devoir d'impartialité qui s'impose à toute juridiction, ni aucune autre règle générale de procédure ne s'oppose à ce qu'un magistrat d'un tribunal administratif qui a siégé dans la formation de jugement ayant statué sur le recours formé contre une décision administrative par un jugement qui a été annulé par une décision du Conseil d'Etat ayant renvoyé l'affaire à ce même tribunal se prononce en qualité de juge des référés sur une demande tendant à la suspension de l'exécution de cette décision administrative. Ainsi, la circonstance que le magistrat du tribunal administratif de Strasbourg qui a statué en qualité de juge des référés, par l'ordonnance attaquée, sur la demande de l'association " Koenigshoffen demain " tendant à la suspension de l'exécution de l'arrêté du 3 novembre 2015 du maire de Strasbourg a, auparavant, siégé dans la formation de jugement ayant rejeté la requête de cette association tendant à l'annulation de cette décision, jugement qui a entretemps été annulé par le Conseil d'Etat, n'est pas de nature à entacher cette ordonnance d'irrégularité. Par suite, l'association requérante n'est pas fondée à soutenir que le juge des référés aurait statué dans des conditions qui méconnaissent les exigences découlant du principe d'impartialité.<br/>
<br/>
              3.	En deuxième lieu, il résulte des termes mêmes de la décision n° 416122 du 21 novembre 2018 du Conseil d'Etat qu'il s'est borné à censurer l'omission du tribunal de répondre dans son jugement au moyen tiré de l'insuffisance de l'étude d'impact alors qu'il n'était pas inopérant, sans pour autant prendre parti sur son mérite. Par suite, le moyen tiré de ce que le juge des référés aurait méconnu la décision du Conseil d'Etat et ainsi commis une erreur de droit en jugeant que le moyen tiré de l'insuffisance de l'étude d'impact n'était pas de nature à faire naître un doute sérieux sur la légalité de la décision attaquée, doit être écarté. <br/>
<br/>
              4.	Il résulte de ce qui précède que l'association " Koenigshoffen Demain " n'est pas fondée à demander l'annulation de l'ordonnance attaquée. Par suite, ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative doivent être rejetées. En revanche, il y a lieu, au titre des mêmes dispositions, de mettre à la charge de l'association " Koenigshoffen Demain " le versement d'une somme de 1 500 euros à la société Frank immobilier, d'une part, et à la commune de Strasbourg, d'autre part.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
      Article 1er : Le pourvoi de l'association " Koenigshoffen Demain " est rejeté.<br/>
<br/>
Article 2 : L'association " Koenigshoffen Demain " versera à la société Frank immobilier, d'une part, et à la commune de Strasbourg, d'autre part, une somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée à l'association " Koenigshoffen Demain ", à la société Frank immobilier et à la commune de Strasbourg.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">37-03-05 JURIDICTIONS ADMINISTRATIVES ET JUDICIAIRES. RÈGLES GÉNÉRALES DE PROCÉDURE. COMPOSITION DES JURIDICTIONS. - MAGISTRAT AYANT STATUÉ SUR LE RECOURS FORMÉ CONTRE UNE DÉCISION ADMINISTRATIVE PAR UN JUGEMENT QUI A ÉTÉ ANNULÉ PAR UNE DÉCISION DU CONSEIL D'ETAT AYANT RENVOYÉ L'AFFAIRE À CE MÊME TRIBUNAL - CIRCONSTANCE FAISANT OBSTACLE À CE QUE CE MAGISTRAT SE PRONONCE EN QUALITÉ DE JUGE DES RÉFÉRÉS SUR UNE DEMANDE DE SUSPENSION DE CETTE MÊME DÉCISION - ABSENCE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-06-03 PROCÉDURE. JUGEMENTS. COMPOSITION DE LA JURIDICTION. - MAGISTRAT AYANT STATUÉ SUR LE RECOURS FORMÉ CONTRE UNE DÉCISION ADMINISTRATIVE PAR UN JUGEMENT QUI A ÉTÉ ANNULÉ PAR UNE DÉCISION DU CONSEIL D'ETAT AYANT RENVOYÉ L'AFFAIRE À CE MÊME TRIBUNAL - CIRCONSTANCE FAISANT OBSTACLE À CE QUE CE MAGISTRAT SE PRONONCE EN QUALITÉ DE JUGE DES RÉFÉRÉS SUR UNE DEMANDE DE SUSPENSION DE CETTE MÊME DÉCISION - ABSENCE [RJ1].
</SCT>
<ANA ID="9A"> 37-03-05 En l'absence de dispositions législatives ou réglementaires fixant les conditions dans lesquelles il doit être statué après l'annulation d'une décision de justice, ni le devoir d'impartialité qui s'impose à toute juridiction, ni aucune autre règle générale de procédure ne s'oppose à ce qu'un magistrat d'un tribunal administratif qui a siégé dans la formation de jugement ayant statué sur le recours formé contre une décision administrative par un jugement qui a été annulé par une décision du Conseil d'Etat ayant renvoyé l'affaire à ce même tribunal se prononce en qualité de juge des référés sur une demande tendant à la suspension de l'exécution de cette décision administrative.</ANA>
<ANA ID="9B"> 54-06-03 En l'absence de dispositions législatives ou réglementaires fixant les conditions dans lesquelles il doit être statué après l'annulation d'une décision de justice, ni le devoir d'impartialité qui s'impose à toute juridiction, ni aucune autre règle générale de procédure ne s'oppose à ce qu'un magistrat d'un tribunal administratif qui a siégé dans la formation de jugement ayant statué sur le recours formé contre une décision administrative par un jugement qui a été annulé par une décision du Conseil d'Etat ayant renvoyé l'affaire à ce même tribunal se prononce en qualité de juge des référés sur une demande tendant à la suspension de l'exécution de cette décision administrative.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., s'agissant de juges délibérant à nouveau après renvoi en la même qualité, CE, Section, 11 février 2005, Commune de Meudon, n° 258102, p. 55.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
