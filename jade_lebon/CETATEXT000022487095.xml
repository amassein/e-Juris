<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000022487095</ID>
<ANCIEN_ID>JG_L_2010_07_000000337411</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/22/48/70/CETATEXT000022487095.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 07/07/2010, 337411, Publié au recueil Lebon</TITRE>
<DATE_DEC>2010-07-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>337411</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Stirn</PRESIDENT>
<AVOCATS/>
<RAPPORTEUR>M. Alexandre  Lallet</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Derepas Luc</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le jugement du 4 mars 2010, enregistré le 10 mars 2010 au secrétariat du contentieux du Conseil d'Etat, par lequel le tribunal administratif de Clermont-Ferrand, avant de statuer sur la demande de Mme Carole A tendant à l'annulation de la décision par laquelle le président du conseil général du Puy-de-Dôme a prononcé sa radiation du droit au revenu de solidarité active et a refusé de lui accorder la dérogation prévue à l'article L. 262-8 du code de l'action sociale et des familles, a décidé, par application des dispositions de l'article L. 113-1 du code de justice administrative, de transmettre le dossier de cette demande au Conseil d'Etat, en soumettant à son examen les questions suivantes :<br/>
<br/>
              1°) Le bénéficiaire du revenu de solidarité active exerçant une activité commerciale et radié en raison d'un dépassement du chiffre d'affaires prévu à l'article L. 262-7 du code de l'action sociale et des familles forme-t-il le recours préalable obligatoire prévu à l'article L. 262-47 du même code, lorsqu'il présente, dans le délai prévu à l'article R. 262-88 de ce code, un recours gracieux auprès du président du conseil général tendant à l'octroi de la dérogation prévue à l'article L. 262-8, ou la décision prise sur cette demande de dérogation constitue-t-elle elle-même une décision relative au revenu de solidarité active soumise à l'obligation de former un recours préalable obligatoire ' <br/>
<br/>
              2°) Au cas où la réponse à la première question amènerait à admettre la recevabilité de la demande, quelle est la nature du contrôle qu'exerce le juge administratif sur l'appréciation portée par le président du conseil général sur la  situation exceptionnelle au regard de l'insertion sociale et professionnelle  du demandeur au sens de cet article L. 262-8 '<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'action sociale et des familles ;<br/>
<br/>
              Vu la loi n° 2008-1249 du 1er décembre 2008 ;<br/>
<br/>
              Vu la loi n° 2009-1673 du 30 décembre 2009 ;<br/>
<br/>
              Vu le code de justice administrative, notamment son article L. 113-1 ;<br/>
<br/>
               Après avoir entendu en séance publique :<br/>
<br/>
- le rapport de M. Alexandre Lallet, Maître des Requêtes, <br/>
- les conclusions de M. Luc Derepas, rapporteur public ;<br/>
<br/>
<br/>
<br/>REND L'AVIS SUIVANT :<br/>
<br/>
<br/>
              1. Sur l'office du juge :<br/>
<br/>
              En confiant, par la loi du 1er décembre 2008 généralisant le revenu de solidarité active et réformant les politiques d'insertion, le contentieux né de la contestation des décisions relatives au revenu de solidarité active aux juridictions administratives de droit commun, le législateur n'a pas entendu que les recours portés devant ces dernières soient d'une autre nature que celle de recours de plein contentieux reconnu par la jurisprudence aux recours précédemment portés devant les commissions départementales d'aide sociale en matière de revenu minimum d'insertion. Au demeurant, le juge administratif de droit commun se trouve saisi, comme l'étaient les commissions départementales, de questions qui justifient, par leur nature, qu'il dispose de pouvoirs excédant ceux d'un juge de l'annulation pour excès de pouvoir. Il en résulte qu'il appartient au tribunal administratif, saisi d'une demande dirigée contre une décision suspendant le versement de l'allocation de revenu de solidarité active ou radiant l'intéressé de la liste des bénéficiaires de cette allocation, non seulement d'apprécier la légalité de cette décision, mais aussi de se prononcer sur les droits du demandeur à cette allocation jusqu'à la date à laquelle il statue, compte tenu de la situation de droit et de fait applicable au cours de cette période. <br/>
<br/>
              2. Sur la portée de l'obligation de présentation d'un recours administratif préalable obligatoire :<br/>
<br/>
              En vertu du premier alinéa de l'article L. 262-7 du code de l'action sociale et des familles, les travailleurs relevant du régime social des indépendants mentionné à l'article L. 611-1 du code de la sécurité sociale ne peuvent, en principe, bénéficier du revenu de solidarité active s'ils emploient, au titre de leur activité professionnelle, un ou plusieurs salariés, ou s'ils réalisent un chiffre d'affaires supérieur à un niveau fixé par décret. <br/>
<br/>
              Toutefois, il résulte de l'article L. 262-8 du même code, tant dans sa rédaction en vigueur que dans sa rédaction antérieure à la loi du 30 décembre 2009 de finances pour 2010, que le président du conseil général peut déroger, par une décision individuelle, à l'application des conditions fixées à l'article L. 262-7 de ce code  lorsque la situation exceptionnelle du demandeur au regard de son insertion sociale et professionnelle le justifie . <br/>
<br/>
              Aux termes de l'article L. 262-47 du code de l'action sociale et des familles :  Toute réclamation dirigée contre une décision relative au revenu de solidarité active fait l'objet, préalablement à l'exercice d'un recours contentieux, d'un recours administratif auprès du président du conseil général. Ce recours est, dans les conditions et limites prévues par la convention mentionnée à l'article L. 262-25, soumis pour avis à la commission de recours amiable qui connaît des réclamations relevant de l'article L. 142-1 du code de la sécurité sociale. (...) .<br/>
<br/>
              Ni les dispositions de l'article L. 262-8 de ce code rappelées ci-dessus, ni les dispositions réglementaires prises pour leur application, ne subordonnent le bénéfice de la dérogation que cet article prévoit à la présentation d'une demande expresse en ce sens ou à la condition que l'intéressé fasse valoir qu'il se trouve dans une situation exceptionnelle au sens de cet article. Dans ces conditions, et eu égard à l'objet de cette mesure, le président du conseil général qui décide de radier l'allocataire de la liste des bénéficiaires du revenu de solidarité active en application des dispositions de l'article L. 262-38 du code de l'action sociale et des familles au motif qu'il ne remplit pas les conditions mentionnées à l'article L. 262-7 du même code est réputé avoir également estimé, au vu des informations dont il disposait, qu'il n'y avait pas lieu d'y déroger sur le fondement de l'article L. 262-8 de ce code.<br/>
<br/>
              Il en résulte que, lorsqu'un allocataire du revenu de solidarité active a fait l'objet d'une décision de radiation de la liste des bénéficiaires de cette allocation au motif qu'il ne remplit pas les conditions posées par l'article L. 262-7 du code de l'action sociale et des familles et a présenté, dans le délai qui lui était imparti, un recours administratif contre cette décision en application de l'article L. 262-47 de ce code, il peut régulièrement saisir le juge administratif compétent d'un recours contentieux dirigé contre la décision confirmant la radiation, que son recours administratif ait été fondé sur les dispositions de l'article L. 262-7 ou sur celles de l'article L. 262-8. Il peut solliciter, dans le cadre de son recours contentieux, le bénéfice de la dérogation prévue à l'article L. 262-8, même s'il n'a pas adressé au président du conseil général une demande spécifique en ce sens ni mentionné cette dérogation dans son recours administratif. <br/>
<br/>
              3. Sur la nature du contrôle du juge administratif sur l'application de l'article L. 262-8 du code de l'action sociale et des familles :<br/>
<br/>
              Il résulte des termes mêmes, rappelés ci-dessus, de l'article L. 262-8 du code de l'action sociale et des familles que le législateur a entendu confier au président du conseil général un large pouvoir d'appréciation pour prendre la décision d'accorder ou de refuser la dérogation prévue à cet article. Par suite, l'appréciation que porte le président du conseil général sur le caractère exceptionnel ou non de la situation de l'intéressé au regard de son insertion sociale et professionnelle au sens de ces dispositions ne peut être censurée par le juge administratif qu'en cas d'erreur manifeste. <br/>
<br/>
              Le présent avis sera notifié au tribunal administratif de Clermont-Ferrand, à Mme Carole A, au département du Puy-de-Dôme et au ministre du travail, de la solidarité et de la fonction publique.<br/>
<br/>
              Il sera publié au Journal officiel de la République française.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">04-02 AIDE SOCIALE. DIFFÉRENTES FORMES D'AIDE SOCIALE. - REVENU DE SOLIDARITÉ ACTIVE (RSA) - 1) CONTESTATION DES DÉCISIONS DE SUSPENSION DU VERSEMENT DE L'ALLOCATION DE RSA OU DE RADIATION - RECOURS DE PLEIN CONTENTIEUX - CONSÉQUENCES [RJ1] - 2) RECOURS ADMINISTRATIF PRÉALABLE OBLIGATOIRE (ART. L. 262-47 DU CASF) - PORTÉE LORSQU'IL EST DIRIGÉ CONTRE UN REFUS DE DÉROGATION AUX CONDITIONS POSÉES PAR L'ARTICLE L. 262-7 DE CE CODE - 3) SITUATION EXCEPTIONNELLE DU DEMANDEUR JUSTIFIANT L'OCTROI DE LA DÉROGATION (ART. L. 262-8) - CONTRÔLE DU JUGE - CONTRÔLE RESTREINT.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-02-02-01 PROCÉDURE. DIVERSES SORTES DE RECOURS. RECOURS DE PLEIN CONTENTIEUX. RECOURS AYANT CE CARACTÈRE. - REVENU DE SOLIDARITÉ ACTIVE (RSA) - CONTESTATION DES DÉCISIONS DE SUSPENSION DU VERSEMENT DE L'ALLOCATION DE RSA OU DE RADIATION - RECOURS DE PLEIN CONTENTIEUX - CONSÉQUENCES [RJ1].
</SCT>
<ANA ID="9A"> 04-02 1) En confiant, par la loi n° 2008-1249 du 1er décembre 2008 généralisant le revenu de solidarité active (RSA) et réformant les politiques d'insertion, le contentieux né de la contestation des décisions relatives au revenu de solidarité active aux juridictions administratives de droit commun, le législateur n'a pas entendu que les recours portés devant ces dernières soient d'une autre nature que celle de recours de plein contentieux reconnu par la jurisprudence aux recours précédemment portés devant les commissions départementales d'aide sociale en matière de revenu minimum d'insertion. Au demeurant, le juge administratif de droit commun se trouve saisi, comme l'étaient les commissions départementales, de questions qui justifient, par leur nature, qu'il dispose de pouvoirs excédant ceux d'un juge de l'annulation pour excès de pouvoir. Il en résulte qu'il appartient au tribunal administratif, saisi d'une demande dirigée contre une décision suspendant le versement de l'allocation de revenu de solidarité active ou radiant l'intéressé de la liste des bénéficiaires de cette allocation, non seulement d'apprécier la légalité de cette décision, mais aussi de se prononcer sur les droits du demandeur à cette allocation jusqu'à la date à laquelle il statue, compte tenu de la situation de droit et de fait applicable au cours de cette période.... ...2) L'article L. 262-8 du code de l'action sociale et des familles (CASF) permet au président du conseil général de déroger aux conditions posées par l'article L. 262-7 de ce code à l'octroi du RSA lorsque la situation exceptionnelle du demandeur au regard de son insertion sociale et professionnelle le justifie. Lorsqu'un allocataire du RSA a fait l'objet d'une décision de radiation de la liste des bénéficiaires de cette allocation au motif qu'il ne remplit pas les conditions posées par l'article L. 262-7 du CASF et a présenté dans le délai imparti un recours administratif contre cette décision en application de l'article L. 262-47 de ce code, il peut régulièrement saisir le juge administratif compétent d'un recours contentieux dirigé contre la décision confirmant la radiation, que son recours administratif ait été fondé sur les dispositions de l'article L. 262-7 ou sur celles de l'article L. 262-8. Il peut solliciter, dans le cadre de son recours contentieux, le bénéfice de la dérogation prévue à l'article L. 262-8, même s'il n'a pas adressé au président du conseil général une demande spécifique en ce sens ni mentionné cette dérogation dans son recours administratif.... ...3) Le législateur ayant entendu confier au président du conseil général un large pouvoir d'appréciation pour déterminer si le demandeur se trouvait dans une situation exceptionnelle justifiant l'octroi de la dérogation prévue à l'article L. 262-8 du CASF, le juge ne censure que l'erreur manifeste d'appréciation sur ce point.</ANA>
<ANA ID="9B"> 54-02-02-01 En confiant, par la loi n° 2008-1249 du 1er décembre 2008 généralisant le revenu de solidarité active (RSA) et réformant les politiques d'insertion, le contentieux né de la contestation des décisions relatives au revenu de solidarité active aux juridictions administratives de droit commun, le législateur n'a pas entendu que les recours portés devant ces dernières soient d'une autre nature que celle de recours de plein contentieux reconnu par la jurisprudence aux recours précédemment portés devant les commissions départementales d'aide sociale en matière de revenu minimum d'insertion. Au demeurant, le juge administratif de droit commun se trouve saisi, comme l'étaient les commissions départementales, de questions qui justifient, par leur nature, qu'il dispose de pouvoirs excédant ceux d'un juge de l'annulation pour excès de pouvoir. Il en résulte qu'il appartient au tribunal administratif, saisi d'une demande dirigée contre une décision suspendant le versement de l'allocation de revenu de solidarité active ou radiant l'intéressé de la liste des bénéficiaires de cette allocation, non seulement d'apprécier la légalité de cette décision, mais aussi de se prononcer sur les droits du demandeur à cette allocation jusqu'à la date à laquelle il statue, compte tenu de la situation de droit et de fait applicable au cours de cette période.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr. à propos des commissions des droits et de l'autonomie des travailleurs handicapés, Avis, 6 avril 2007, Douwens Prats, n° 293238, p. 153.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
