<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042702009</ID>
<ANCIEN_ID>JG_L_2020_12_000000436461</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/70/20/CETATEXT000042702009.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 18/12/2020, 436461</TITRE>
<DATE_DEC>2020-12-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>436461</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DE NERVO, POUPET ; SCP L. POULET-ODENT</AVOCATS>
<RAPPORTEUR>M. Alexis Goin</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Mireille Le Corre</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:436461.20201218</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B... A... a demandé au tribunal administratif de Toulouse d'annuler la décision du 23 mai 2014 par laquelle le directeur général de la Caisse des dépôts et consignations a rejeté sa demande d'allocation temporaire d'invalidité et de constater l'irrégularité de l'expertise médicale réalisée le 9 juin 2015 et d'en ordonner une nouvelle afin d'évaluer son taux d'incapacité permanente partielle. Il a également demandé la condamnation de la communauté d'agglomération du Grand Cahors à lui verser la somme de 25 000 euros en réparation des préjudices résultant des fautes de son employeur et de sa maladie professionnelle. Par un jugement n°s 1403586, 1600855 du 3 octobre 2017, le tribunal administratif de Toulouse a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 17BX03713 du 2 décembre 2019, enregistré le 3 décembre suivant au secrétariat du contentieux du Conseil d'Etat, la cour administrative d'appel de Bordeaux a, d'une part, réformé ce jugement en condamnant la communauté d'agglomération du Grand Cahors à verser la somme de 4 500 euros à M. A... et, d'autre part, transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, les conclusions du pourvoi, enregistré le 29 novembre 2017 au greffe de cette cour, présenté par M. A..., dirigées contre ce jugement en tant qu'il a statué sur la décision du 23 mai 2014 du directeur général de la Caisse des dépôts et consignations et sur l'expertise médicale réalisée le 9 juin 2015. Par ce pourvoi et un nouveau mémoire, enregistré le 9 mars 2020, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) de mettre à la charge de la Caisse des dépôts et consignations la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code des communes ;<br/>
              - le code des pensions civiles et militaires de retraite ;<br/>
              - le code de la sécurité sociale ;<br/>
              - la loi n° 84-16 du 11 janvier 1984 ;<br/>
              - la loi n° 84-53 du 26 janvier 1984 ;<br/>
              - le décret n° 60-1089 du 6 octobre 1960 ;<br/>
              - le décret n° 68-756 du 13 août 1968 ;<br/>
              - le décret n° 2005-442 du 2 mai 2005 ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Alexis Goin, auditeur,  <br/>
<br/>
              - les conclusions de Mme Mireille Le Corre, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP de Nervo, Poupet, avocat de M. A... et à la SCP L. Poulet, Odent, avocat de la Caisse des dépôts et consignations ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. A... est employé depuis 2004 par la communauté d'agglomération du grand Cahors en qualité d'adjoint technique titulaire. Il a présenté en 2011 une baisse d'audition, reconnue imputable au service par un arrêté du président de cet établissement public en date du 5 mars 2012. Il a alors sollicité le bénéfice de l'allocation temporaire d'invalidité, qui lui a été refusé par une décision du 23 mai 2014 du directeur général de la Caisse des dépôts et consignations. Par le jugement attaqué du 3 octobre 2017, le tribunal administratif de Toulouse a rejeté sa demande tendant notamment à l'annulation de cette décision. Par un arrêt du 2 décembre 2019, la cour administrative d'appel de Bordeaux a transmis au Conseil d'Etat les conclusions du pourvoi présenté par M. A... contre ce jugement en tant qu'il a statué sur cette décision et sur la régularité de l'expertise médicale réalisée le 9 juin 2015.<br/>
<br/>
              2. En premier lieu, en jugeant que M. A... ne faisait état d'aucun élément précis permettant d'établir que l'expert désigné par le tribunal administratif ne lui aurait pas communiqué certaines pièces dans le cadre des opérations d'expertise, le tribunal, qui a suffisamment motivé son jugement, n'a pas dénaturé les pièces du dossier ni commis d'erreur de droit. <br/>
<br/>
              3. En second lieu, selon l'article L. 417-8 du code des communes, maintenu en vigueur et étendu à l'ensemble des agents concernés par la loi du 26 janvier 1984 relative à la fonction publique territoriale par le III de son article 119 : " Les communes et les établissements publics communaux et intercommunaux sont tenus d'allouer aux agents qui ont été atteints d'une invalidité résultant d'un accident de service ayant entraîné une incapacité permanente au moins égale à un taux minimum déterminé par l'autorité supérieure ou d'une maladie professionnelle une allocation temporaire d'invalidité cumulable avec le traitement, dans les mêmes conditions que pour les fonctionnaires de l'Etat ".<br/>
<br/>
              4. Aux termes de l'article 1er du décret du 6 octobre 1960 relatif à l'attribution de l'allocation temporaire d'invalidité des fonctionnaires de l'Etat : " L'allocation temporaire d'invalidité prévue à l'article 65 de la loi n° 84-16 du 11 janvier 1984 modifiée portant dispositions statutaires relatives à la fonction publique de l'Etat est attribuée aux agents maintenus en activité qui justifient d'une invalidité permanente résultant : / a) Soit d'un accident de service ayant entraîné une incapacité permanente d'un taux rémunérable au moins égal à 10 % ; / b) Soit de l'une des maladies d'origine professionnelle énumérées dans les tableaux mentionnés à l'article L. 461-2 du code de la sécurité sociale ; / c) Soit d'une maladie reconnue d'origine professionnelle dans les conditions prévues par les troisième et quatrième alinéas de l'article L. 461-1 du code de la sécurité sociale ; / dans ces cas, par dérogation aux règles prévues par cet article, le pouvoir de décision appartient en dernier ressort au ministre dont relève l'agent et au ministre chargé du budget ; / dans le cas mentionné au quatrième alinéa du même article, le taux d'incapacité permanente est celui prévu audit alinéa, mais, par dérogation aux règles auxquelles renvoie cet article, ce taux est apprécié par la commission de réforme mentionnée à l'article L. 31 du code des pensions civiles et militaires de retraite en prenant en compte le barème indicatif mentionné à l'article L. 28 du même code. (...) ".<br/>
<br/>
              5. Aux termes de l'article 1er du décret du 2 mai 2005 relatif à l'attribution de l'allocation temporaire d'invalidité aux fonctionnaires relevant de la fonction publique territoriale et de la fonction publique hospitalière : " L'allocation temporaire d'invalidité est accordée, dans les conditions fixées par le présent décret, aux fonctionnaires mentionnés à l'article 2 de la loi du 26 janvier 1984 susvisée (...) et qui sont affiliés à la Caisse nationale de retraites des agents des collectivités locales ". Selon l'article 2 du même décret : " L'allocation est attribuée aux fonctionnaires maintenus en activité qui justifient d'une invalidité permanente résultant : / a) Soit d'un accident de service ayant entraîné une incapacité permanente d'un taux au moins égal à 10 % ; / b) Soit de l'une des maladies d'origine professionnelle énumérées par les tableaux mentionnés à l'article L. 461-2 du code de la sécurité sociale ; / c) Soit d'une maladie reconnue d'origine professionnelle dans les conditions mentionnées aux alinéas 3 et 4 de l'article L.461-1 du code de la sécurité sociale, sous réserve des dispositions de l'article 6 du présent décret. / Les fonctionnaires justifiant se trouver dans les cas prévus aux b et c ne peuvent bénéficier de cette allocation que dans la mesure où l'affection contractée serait susceptible, s'ils relevaient du régime général de sécurité sociale, de leur ouvrir droit à une rente en application des dispositions du livre IV dudit code et de ses textes d'application ". Aux termes du premier alinéa de l'article 5 du même décret : " Le taux d'invalidité est déterminé compte tenu du barème indicatif prévu à l'article L. 28 du code des pensions civiles et militaires de retraite ". <br/>
<br/>
              6. Aux termes du premier alinéa de l'article L. 461-1 du code de la sécurité sociale : " Les dispositions du présent livre sont applicables aux maladies d'origine professionnelle sous réserve des dispositions du présent titre (...) ". Selon le premier alinéa de l'article L. 461-2 du même code : " Des tableaux annexés aux décrets énumèrent les manifestations morbides d'intoxications aiguës ou chroniques présentées par les travailleurs exposés d'une façon habituelle à l'action des agents nocifs mentionnés par lesdits tableaux, qui donnent, à titre indicatif, la liste des principaux travaux comportant la manipulation ou l'emploi de ces agents. Ces manifestations morbides sont présumées d'origine professionnelle ". Selon le deuxième alinéa de l'article R. 434-32 du même code : " Les barèmes indicatifs d'invalidité dont il est tenu compte pour la détermination du taux d'incapacité permanente d'une part en matière d'accidents du travail et d'autre part en matière de maladies professionnelles sont annexés au présent livre. Lorsque ce dernier barème ne comporte pas de référence à la lésion considérée, il est fait application du barème indicatif d'invalidité en matière d'accidents du travail ".<br/>
<br/>
              7. Il résulte de ces dispositions, en particulier de celles de l'article L. 417-8 du code des communes, qui prévoient que les agents entrant dans le champ de ses dispositions peuvent bénéficier d'une allocation temporaire d'invalidité dans les mêmes conditions que pour les fonctionnaires de l'Etat, que l'article 5 du décret du 2 mai 2005, relatif à l'attribution de l'allocation temporaire d'invalidité aux fonctionnaires relevant de la fonction publique territoriale et de la fonction publique hospitalière, doit être interprété à la lumière de l'article 1er du décret du 6 octobre 1960 applicable aux fonctionnaires de l'Etat. Celui-ci impose à l'administration de tenir compte du barème indicatif prévu à l'article L. 28 du code des pensions civiles et militaires de retraite dans la détermination de l'éligibilité à l'allocation temporaire d'invalidité aussi bien que dans le calcul de son montant. Par suite, l'administration, lorsqu'elle recherche si les fonctionnaires justifiant se trouver dans les cas prévus aux b et c de l'article 2 du décret du 2 mai 2005 remplissent les conditions mentionnées aux articles L. 461-1 et L. 461-2 du code de la sécurité sociale afin de déterminer leur éligibilité à l'allocation temporaire d'invalidité, doit se référer au barème indicatif prévu à l'article L. 28 du code des pensions civiles et militaires de retraite, et non aux barèmes indicatifs prévus à l'article R. 434-32 du code de la sécurité sociale. <br/>
<br/>
              8. Il suit de là que le tribunal administratif n'a pas méconnu le champ d'application de la loi, ni dénaturé les pièces du dossier en considérant que le barème indicatif prévu à l'article L. 28 du code des pensions civiles et militaires de retraite était applicable à la situation du requérant, qui sollicitait le bénéfice d'une allocation temporaire d'invalidité sur le fondement du c de l'article 2 du décret du 2 mai 2005. <br/>
<br/>
              9. Il résulte de ce qui précède que M. A... n'est pas fondé à demander l'annulation du jugement qu'il attaque. Son pourvoi ne peut dès lors qu'être rejeté, y compris ses conclusions au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. A... est rejeté.<br/>
Article 2 : La présente décision sera notifiée à M. B... A... et à la Caisse des dépôts et consignations.<br/>
Copie en sera adressée à la communauté d'agglomération du Grand Cahors.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-08-03-01 FONCTIONNAIRES ET AGENTS PUBLICS. RÉMUNÉRATION. INDEMNITÉS ET AVANTAGES DIVERS. ALLOCATION TEMPORAIRE D'INVALIDITÉ. - AGENTS MAINTENUS EN ACTIVITÉ QUI JUSTIFIENT D'UNE INVALIDITÉ PERMANENTE (ART. 1ER DU DÉCRET DU 6 OCTOBRE 1960) - PRISE EN COMPTE DU BARÈME INDICATIF PRÉVU À L'ARTICLE L. 28 DU CPCMR.
</SCT>
<ANA ID="9A"> 36-08-03-01 Il résulte de l'article 1er du décret n° 60-1089 du 6 octobre 1960, des articles 1er, 2 et du premier alinéa de l'article 5 du décret n° 2005-442 du 2 mai 2005, du premier alinéa de l'article L. 461-1, du premier alinéa de l'article L. 461-2, du deuxième alinéa de l'article R. 434-32 du code de la sécurité sociale (CSS) ainsi que, et en particulier, de l'article L. 417-8 du code des communes, qui prévoit que les agents entrant dans le champ de ses dispositions peuvent bénéficier d'une allocation temporaire d'invalidité dans les mêmes conditions que pour les fonctionnaires de l'Etat, que l'article 5 du décret du 2 mai 2005, relatif à l'attribution de l'allocation temporaire d'invalidité aux fonctionnaires relevant de la fonction publique territoriale et de la fonction publique hospitalière, doit être interprété à la lumière de l'article 1er du décret du 6 octobre 1960 applicable aux fonctionnaires de l'Etat. Celui-ci impose à l'administration de tenir compte du barème indicatif prévu à l'article L. 28 du code des pensions civiles et militaires de retraite (CPCMR) dans la détermination de l'éligibilité à l'allocation temporaire d'invalidité aussi bien que dans le calcul de son montant.,,,Par suite, l'administration, lorsqu'elle recherche si les fonctionnaires justifiant se trouver dans les cas prévus aux b et c de l'article 2 du décret du 2 mai 2005 remplissent les conditions mentionnées aux articles L. 461-1 et L. 461-2 du CSS afin de déterminer leur éligibilité à l'allocation temporaire d'invalidité, doit se référer au barème indicatif prévu à l'article L. 28 du CPCMR, et non aux barèmes indicatifs prévus à l'article R. 434-32 du CSS.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
