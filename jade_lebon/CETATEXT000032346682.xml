<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032346682</ID>
<ANCIEN_ID>JG_L_2016_03_000000383037</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/34/66/CETATEXT000032346682.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Section, 30/03/2016, 383037, Publié au recueil Lebon</TITRE>
<DATE_DEC>2016-03-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>383037</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Section</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER ; SCP ODENT, POULET</AVOCATS>
<RAPPORTEUR>M. Luc Briand</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier Domino</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESEC:2016:383037.20160330</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Bordeaux d'annuler pour excès de pouvoir les délibérations n° 2009/708 et n° 2009/709 du 6 novembre 2009 du conseil de la communauté urbaine de Bordeaux, par lesquelles ce conseil, d'une part, a arrêté, après concertation, le projet définitif de développement du réseau de transports en commun de la communauté urbaine et, d'autre part, a approuvé le principe de mesures d'aménagement en compensation des premiers effets du projet de développement du réseau. Par une ordonnance n° 1000306 du 2 octobre 2012, le président de la 1ère chambre du tribunal administratif de Bordeaux a rejeté sa demande. <br/>
<br/>
              Par un arrêt n° 12BX03066 du 3 juin 2014, la cour administrative d'appel de Bordeaux a rejeté l'appel formé par M. B...contre cette ordonnance.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 24 juillet et 17 septembre 2014 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de la communauté urbaine de Bordeaux la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Luc Briand, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Xavier Domino, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Odent, Poulet, avocat de M. B...et à la SCP Foussard, Froger, avocat de la communauté urbaine de Bordeaux ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des énonciations de l'arrêt attaqué que le conseil de la communauté urbaine de Bordeaux, par une délibération n° 2008/708 du 6 novembre 2009, a arrêté, à l'issue de la concertation prévue par l'article L. 300-2 du code de l'urbanisme, le dossier définitif du projet de développement du réseau de transports en commun de l'agglomération bordelaise, qui comporte la création d'une nouvelle ligne de tramway, l'extension des trois lignes existantes et la création du tram-train du Médoc ; que, par une délibération n° 2008/709 du même jour, le conseil de la communauté urbaine a prévu des mesures d'aménagement, en compensation des premiers effets du projet, en particulier de la suppression de places de stationnement du fait de la réalisation de la nouvelle ligne de tramway ; que le président de la 1ère chambre du tribunal administratif de Bordeaux a, par une ordonnance du 2 octobre 2012, rejeté le recours pour excès de pouvoir formé par M. B... contre ces délibérations ; que, par un arrêt du 3 juin 2014, la cour administrative d'appel de Bordeaux a rejeté l'appel formé par M. B...contre cette ordonnance ; que M. B... se pourvoit en cassation contre cet arrêt ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 300-2 du code de l'urbanisme dans sa rédaction alors en vigueur : " I - Le conseil municipal ou l'organe délibérant de l'établissement public de coopération intercommunale délibère sur les objectifs poursuivis et sur les modalités d'une concertation associant, pendant toute la durée de l'élaboration du projet, les habitants, les associations locales et les autres personnes concernées dont les représentants de la profession agricole, avant : [...] / c) Toute opération d'aménagement réalisée par la commune ou pour son compte lorsque, par son importance ou sa nature, cette opération modifie de façon substantielle le cadre de vie ou l'activité économique de la commune et qu'elle n'est pas située dans un secteur qui a déjà fait l'objet de cette délibération au titre du a) ou du b) ci-dessus. [...] / A l'issue de cette concertation, le maire en présente le bilan devant le conseil municipal qui en délibère. / Le dossier définitif du projet est alors arrêté par le conseil municipal et tenu à la disposition du public. [...] / II - Les autres personnes publiques ayant l'initiative d'opérations d'aménagement sont tenues aux mêmes obligations. Elles organisent la concertation dans des conditions fixées après avis de la commune. " ; <br/>
<br/>
              3. Considérant, en premier lieu, que la délibération par laquelle le conseil municipal ou l'organe délibérant de l'établissement public de coopération intercommunale arrête, en application des dispositions précitées, le dossier définitif d'un projet d'aménagement, ne permet pas, par elle-même, la réalisation des opérations d'aménagement, lesquelles ne pourront être engagées qu'à la suite de leur déclaration d'utilité publique ou d'une autre décision de les réaliser ; que cette délibération revêt le caractère d'une mesure préparatoire, insusceptible de faire l'objet d'un recours pour excès de pouvoir ; que, par suite, la cour administrative d'appel n'a pas commis d'erreur de droit en jugeant que M. B... n'était pas recevable à attaquer la délibération n° 2008/708 du 6 novembre 2009 par la voie du recours pour excès de pouvoir, même en se bornant à soulever des moyens tirés de vices dont, selon lui, la procédure de concertation ayant précédé l'adoption de cette délibération serait entachée ;<br/>
<br/>
              4. Considérant, en second lieu, que la délibération par laquelle le conseil municipal ou l'organe délibérant de l'établissement public de coopération intercommunale se borne à manifester son intention de prendre des mesures permettant de compenser les effets négatifs pour les riverains d'une opération d'aménagement revêt le caractère d'une simple déclaration de principe dépourvue par elle-même d'effets juridiques ; qu'elle ne constitue, dès lors, pas une décision susceptible de recours pour excès de pouvoir ; que, par suite, la cour, qui a exactement interprété la délibération n° 2009/709 en jugeant qu'elle revêtait le caractère d'une simple déclaration de principe, n'a pas commis d'erreur de droit en jugeant que M. B... n'était pas recevable à l'attaquer par la voie du recours pour excès de pouvoir ;<br/>
<br/>
              5. Considérant qu'il résulte de tout ce qui précède que M. B... n'est pas fondé à demander l'annulation de l'arrêt qu'il attaque ; que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mis à la charge de Bordeaux Métropole qui n'est pas, dans la présente instance, la partie perdante, le versement de la somme que M. B... demande au titre des frais exposés par lui et non compris dans les dépens ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de M. B... la somme que Bordeaux Métropole demande au même titre ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le pourvoi de M. B...est rejeté.<br/>
Article 2 : Les conclusions de Bordeaux Métropole présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à M. A...B...et à Bordeaux Métropole.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-01-01-02-02 PROCÉDURE. INTRODUCTION DE L'INSTANCE. DÉCISIONS POUVANT OU NON FAIRE L'OBJET D'UN RECOURS. ACTES NE CONSTITUANT PAS DES DÉCISIONS SUSCEPTIBLES DE RECOURS. MESURES PRÉPARATOIRES. - DÉLIBÉRATION PAR LAQUELLE LE CONSEIL MUNICIPAL OU L'ORGANE DÉLIBÉRANT DE L'EPCI ARRÊTE LE DOSSIER DÉFINITIF D'UN PROJET D'AMÉNAGEMENT (ART. L. 300-2 DU CODE DE L'URBANISME) [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">68-06-01 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. INTRODUCTION DE L'INSTANCE. - MESURES PRÉPARATOIRES INSUSCEPTIBLES DE RECOURS POUR EXCÈS DE POUVOIR - INCLUSION - DÉLIBÉRATION PAR LAQUELLE LE CONSEIL MUNICIPAL OU L'ORGANE DÉLIBÉRANT DE L'EPCI ARRÊTE LE DOSSIER DÉFINITIF D'UN PROJET D'AMÉNAGEMENT (ART. L. 300-2 DU CODE DE L'URBANISME) [RJ1].
</SCT>
<ANA ID="9A"> 54-01-01-02-02 La délibération par laquelle le conseil municipal ou l'organe délibérant de l'établissement public de coopération intercommunale (EPCI) arrête, en application de l'article L. 300-2 du code de l'urbanisme dans sa rédaction alors en vigueur, le dossier définitif d'un projet d'aménagement, ne permet pas, par elle-même, la réalisation des opérations d'aménagement, lesquelles ne pourront être engagées qu'à la suite de leur déclaration d'utilité publique ou d'une autre décision de les réaliser. Cette délibération revêt le caractère d'une mesure préparatoire, insusceptible de faire l'objet d'un recours pour excès de pouvoir, quand même celui-ci se bornerait à soulever des moyens tirés de vices dans la procédure de concertation ayant précédé l'adoption de la délibération.</ANA>
<ANA ID="9B"> 68-06-01 La délibération par laquelle le conseil municipal ou l'organe délibérant de l'établissement public de coopération intercommunale (EPCI) arrête, en application de l'article L. 300-2 du code de l'urbanisme dans sa rédaction alors en vigueur, le dossier définitif d'un projet d'aménagement, ne permet pas, par elle-même, la réalisation des opérations d'aménagement, lesquelles ne pourront être engagées qu'à la suite de leur déclaration d'utilité publique ou d'une autre décision de les réaliser. Cette délibération revêt le caractère d'une mesure préparatoire, insusceptible de faire l'objet d'un recours pour excès de pouvoir, quand même celui-ci se bornerait à soulever des moyens tirés de vices dans la procédure de concertation ayant précédé l'adoption de la délibération.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Comp., s'agissant de la décision arrêtant le principe et les modalités de réalisation d'un projet d'intérêt général, CE, Section, 30 octobre 1992, Ministre des affaires étrangères et secrétaire d'Etat aux grands travaux c/ Association de sauvegarde du site Alma Champ de Mars, n° 140220, p. 384 ; s'agissant de la délibération arrêtant le principe de la création d'un métro, CE, Section, 6 mai 1996, Association Aquitaine Alternatives, n° 121915, p. 144 ; s'agissant des actes par lesquels le maître d'ouvrage se prononce, après débat public, sur le principe et les modalités de poursuite du projet, CE, 28 décembre 2005, Association citoyenne intercommunale des populations concernées par le projet d'aéroport Notre-Dame des Landes, n° 267287, T. pp. 690-809-1007-1060-1142 ; s'agissant de la délibération sur le principe d'une délégation de service public, CE, 24 novembre 2010, Association fédération d'action régionale pour l'environnement et autres, n° 318342, T. pp. 603-848-886-892.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
