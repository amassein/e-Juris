<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034808216</ID>
<ANCIEN_ID>JG_L_2017_05_000000395017</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/80/82/CETATEXT000034808216.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 1ère chambres réunies, 24/05/2017, 395017</TITRE>
<DATE_DEC>2017-05-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>395017</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Cyrille Beaufils</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:395017.20170524</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par un jugement n° 2014-0004 du 13 juin 2004, la chambre régionale des comptes de Bretagne a constitué Mme A...B..., comptable du centre hospitalier Yves le Foll, à Saint-Brieuc, débitrice des sommes de 174 460,48 euros et de 206 388 euros à l'égard de cet établissement. <br/>
<br/>
              Par un arrêt n° 72702 du 15 octobre 2015, la Cour des Comptes a, sur la requête de MmeB..., d'une part, ramené la seconde somme mise à sa charge de 206 388 à 20 071,28 euros et réformé le jugement dans cette mesure, et, d'autre part, rejeté le surplus de ses conclusions. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code des juridictions financières ; <br/>
              - la loi n° 63-156 du 23 février 1963 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Cyrille Beaufils, auditeur,  <br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes du I de l'article 60 de la loi du 23 février 1963 de finances pour 1963, dans sa rédaction issue de l'article 90 de la loi du 28 décembre 2011 de finances rectificative pour 2011, qui définit les obligations qu'il incombe au comptable public de respecter, sous peine de voir sa responsabilité personnelle et pécuniaire engagée : " Outre la responsabilité attachée à leur qualité d'agent public, les comptables publics sont personnellement et pécuniairement responsables du recouvrement des recettes, du paiement des dépenses, de la garde et de la conservation des fonds et valeurs appartenant ou confiés aux différentes personnes morales de droit public dotées d'un comptable public, désignées ci-après par le terme d'organismes publics, du maniement des fonds et des mouvements de comptes de disponibilités, de la conservation des pièces justificatives des opérations et documents de comptabilité ainsi que de la tenue de la comptabilité du poste comptable qu'ils dirigent. / Les comptables publics sont personnellement et pécuniairement responsables des contrôles qu'ils sont tenus d'assurer en matière de recettes, de dépenses et de patrimoine dans les conditions prévues par le règlement général sur la comptabilité publique. / La responsabilité personnelle et pécuniaire prévue ci-dessus se trouve engagée dès lors qu'un déficit ou un manquant en monnaie ou en valeurs a été constaté, qu'une recette n'a pas été recouvrée, qu'une dépense a été irrégulièrement payée ou que, par le fait du comptable public, l'organisme public a dû procéder à l'indemnisation d'un autre organisme public ou d'un tiers ou a dû rétribuer un commis d'office pour produire les comptes. / (...) " ; qu'aux termes du VI de cet article : " La responsabilité personnelle et pécuniaire prévue au I est mise en jeu par le ministre dont relève le comptable, le ministre chargé du budget ou le juge des comptes dans les conditions qui suivent. Les ministres concernés peuvent déléguer cette compétence. / Lorsque le manquement du comptable aux obligations mentionnées au I n'a pas causé de préjudice financier à l'organisme public concerné, le juge des comptes peut l'obliger à s'acquitter d'une somme arrêtée, pour chaque exercice, en tenant compte des circonstances de l'espèce. Le montant maximal de cette somme est fixé par décret en Conseil d'Etat en fonction du niveau des garanties mentionnées au II. / Lorsque le manquement du comptable aux obligations mentionnées au I a causé un préjudice financier à l'organisme public concerné ou que, par le fait du comptable public, l'organisme public a dû procéder à l'indemnisation d'un autre organisme public ou d'un tiers ou a dû rétribuer un commis d'office pour produire les comptes, le comptable a l'obligation de verser immédiatement de ses deniers personnels la somme correspondante (...) " ; qu'aux termes du IX de ce même article : " Les comptables publics dont la responsabilité personnelle et pécuniaire a été mise en jeu dans les cas mentionnés au deuxième alinéa du VI ne peuvent obtenir du ministre chargé du budget la remise gracieuse des sommes mises à leur charge. / Les comptables publics dont la responsabilité personnelle et pécuniaire a été mise en jeu dans les cas mentionnés au troisième alinéa du même VI peuvent obtenir du ministre chargé du budget la remise gracieuse des sommes mises à leur charge. Hormis le cas de décès du comptable ou de respect par celui-ci, sous l'appréciation du juge des comptes, des règles de contrôle sélectif des dépenses, aucune remise gracieuse totale ne peut être accordée au comptable public dont la responsabilité personnelle et pécuniaire a été mise en jeu par le juge des comptes, le ministre chargé du budget étant dans l'obligation de laisser à la charge du comptable une somme au moins égale au double de la somme mentionnée au deuxième alinéa dudit VI. / En cas de remise gracieuse, les débets des comptables publics sont supportés par le budget de l'organisme intéressé. Toutefois, ils font l'objet d'une prise en charge par le budget de l'Etat dans les cas et conditions fixés par l'un des décrets prévus au XII. L'Etat est subrogé dans tous les droits des organismes publics à concurrence des sommes qu'il a prises en charge. " ;<br/>
<br/>
              2. Considérant qu'il résulte de ces dispositions, et notamment de celles du troisième alinéa du VI de l'article 60 auquel renvoie le IX du même article, que le comptable public dont la responsabilité personnelle et pécuniaire a été mise en jeu en raison d'un manquement ayant causé un préjudice financier à l'organisme public concerné est susceptible de se voir accorder la remise gracieuse des sommes mises à sa charge par le juge des comptes par décision du ministre chargé du budget ; qu'en dehors des cas de décès du comptable public ou du respect par celui-ci, sous l'appréciation du juge des comptes, des règles de contrôle sélectif des dépenses, aucune remise gracieuse totale n'est possible ; que le ministre chargé du budget est alors tenu de laisser à la charge du comptable, pour chaque manquement commis par celui-ci, une somme au moins égale au double de la somme mentionnée au deuxième alinéa du VI de l'article 60 de la loi du 23 février 1963 cité ci-dessus ; que lorsque, dans le cadre de son office tel que défini par le IX de ce même article, le juge des comptes s'est prononcé sur le point de savoir si la somme mise à la charge du comptable à raison de chaque manquement ayant causé un préjudice financier à l'organisme public est susceptible de faire l'objet d'une remise gracieuse totale du ministre ou si cette remise gracieuse est au contraire plafonnée dans les conditions rappelées ci-dessus, cette appréciation s'impose au ministre chargé du budget dans l'exercice de son pouvoir de remise gracieuse ; que les motifs du jugement qui portent sur cette appréciation constituent ainsi un élément de la décision juridictionnelle susceptible d'être discuté devant le juge d'appel ou de cassation, alors même que le jugement ou l'arrêt attaqué n'en n'a pas fait état dans son dispositif ; <br/>
<br/>
              3. Considérant qu'il résulte de ce qui précède qu'en ne rejetant pas comme irrecevables les conclusions présentées par Mme B...contre le jugement de la chambre régionale des comptes de Bretagne du 13 juin 2004 en tant qu'il indiquait que les sommes mises à sa charge n'étaient pas susceptibles de faire l'objet d'une remise gracieuse totale, la Cour des comptes n'a pas commis d'erreur de droit ; <br/>
<br/>
              4. Considérant que le parquet général près la Cour des comptes n'est pas fondé à demander l'annulation de l'arrêt qu'il attaque ; que son pourvoi ne peut, par suite, qu'être rejeté ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi du parquet général près la Cour des comptes est rejeté.<br/>
<br/>
Article 2 : La présente décision sera notifiée au parquet général près la Cour des comptes. <br/>
Copie en sera adressée à Mme A...B..., au ministre de l'action et des comptes publics et au centre hospitalier Yves Le Foll.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">18-01-03 COMPTABILITÉ PUBLIQUE ET BUDGET. RÉGIME JURIDIQUE DES ORDONNATEURS ET DES COMPTABLES. RESPONSABILITÉ. - MANQUEMENT DU COMPTABLE AYANT CAUSÉ UN PRÉJUDICE À LA COLLECTIVITÉ - OFFICE DU JUGE DES COMPTES - CAS OÙ LE JUGE INDIQUE LE MONTANT DE LA REMISE SUSCEPTIBLE D'ÊTRE CONSENTIE [RJ1] -1) APPRÉCIATION CONTRAIGNANT LE POUVOIR DE REMISE GRACIEUSE DU MINISTRE - EXISTENCE - 2) CONSÉQUENCE - RECEVABILITÉ DES CONCLUSIONS CONTESTANT CE MONTANT, ALORS-MÊME QUE LA DÉCISION JURIDICTIONNELLE ATTAQUÉE N'EN A PAS FAIT ÉTAT DANS SON DISPOSITIF.
</SCT>
<ANA ID="9A"> 18-01-03 Manquement du comptable ayant causé un préjudice à la collectivité.... ,,1) Lorsque, dans le cadre de son office tel que défini par le IX de l'article 60 de la loi n° 63-156 du 23 février 1963, le juge des comptes s'est prononcé sur le point de savoir si la somme mise à la charge du comptable à raison de chaque manquement ayant causé un préjudice financier à l'organisme public est susceptible de faire l'objet d'une remise gracieuse totale du ministre ou si cette remise gracieuse est au contraire plafonnée dans les conditions rappelées ci-dessus, cette appréciation s'impose au ministre chargé du budget dans l'exercice de son pouvoir de remise gracieuse.... ,,2) Les motifs du jugement qui portent sur cette appréciation constituent un élément de la décision juridictionnelle susceptible d'être discuté devant le juge d'appel ou de cassation, alors même que le jugement ou l'arrêt attaqué n'en n'a pas fait état dans son dispositif.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 27 mai 2015, Ministre délégué, chargé du budget c/ Parquet général près la Cour des comptes, n° 374708, T. p. 609.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
