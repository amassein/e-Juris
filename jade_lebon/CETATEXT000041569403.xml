<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041569403</ID>
<ANCIEN_ID>JG_L_2020_02_000000422754</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/56/94/CETATEXT000041569403.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 6ème chambres réunies, 12/02/2020, 422754</TITRE>
<DATE_DEC>2020-02-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>422754</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 6ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SEVAUX, MATHONNET ; SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>Mme Louise Cadin</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2020:422754.20200212</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme C... D... a demandé au tribunal administratif de Marseille de condamner l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales (ONIAM) à lui verser la somme de 520 059,67 euros en réparation du préjudice qu'elle estime avoir subi lors de sa prise en charge à l'Hôpital Nord de Marseille. Par un jugement n° 1306336 du 4 juillet 2016, le tribunal administratif a condamné l'ONIAM à verser à Mme D... la somme de 189 660,85 euros ainsi qu'une rente de 19,50 euros par jour, versée par trimestre échu.<br/>
<br/>
              Par un arrêt n° 16MA03550 du 31 mai 2018, la cour administrative d'appel de Marseille a, sur appel de l'ONIAM et appel incident de Mme D..., condamné l'ONIAM à verser la somme de 201 638 euros à Mme D... et porté la rente journalière à 23,70 euros. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 31 juillet et 1er octobre 2018 au secrétariat du contentieux du Conseil d'Etat, l'ONIAM demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de Mme D... la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la santé publique ;<br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme B... A..., auditrice,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Sevaux, Mathonnet, avocat de l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales et à la SCP Rocheteau, Uzan-Sarano, avocat de Mme D... ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que Mme D... a été victime d'un accident vasculaire cérébral liée à un cavernome, le 14 février 2007. Une intervention a été programmée à l'Hôpital Nord de Marseille, où elle était hospitalisée, pour réaliser l'ablation du cavernome. Toutefois, en raison d'une infection par le staphylocoque doré, l'opération prévue le 20 février 2007 a été repoussée au 28 février. Mme D..., estimant que cette infection bactérienne était de nature nosocomiale et que, ayant diminué les chances de succès de l'opération chirurgicale, elle était à l'origine des séquelles dont elle demeure atteinte, a saisi la commission régionale de conciliation et d'indemnisation de Provence-Alpes-Côte-d'Azur, qui a émis un avis favorable à son indemnisation au titre de la solidarité nationale. L'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales (ONIAM) a toutefois refusé, le 4 octobre 2012, d'adresser une offre d'indemnisation à Mme D.... <br/>
<br/>
              2. Par un jugement du 4 juillet 2016, le tribunal administratif a condamné l'ONIAM à verser à Mme D... la somme de 189 660,85 euros ainsi qu'une rente de 19,50 euros par jour. Par un arrêt du 31 mai 2018, la cour administrative d'appel de Marseille a, sur appel de l'ONIAM et appel incident de Mme D..., porté à 201 638 euros la somme à verser à Mme D... et à 23,70 euros par jour le montant de la rente journalière. L'ONIAM se pourvoit en cassation contre cet arrêt en tant qu'il rejette son appel et, par la voie du pourvoi incident, Mme D... en demande l'annulation en tant qu'il rejette le surplus de ses conclusions d'appel incident.<br/>
<br/>
              Sur le pourvoi principal de l'ONIAM :<br/>
<br/>
              2. Aux termes de l'article L. 1142-1-1 du code de la santé publique : " Sans préjudice des dispositions du septième alinéa de l'article L. 1142-17, ouvrent droit à réparation au titre de la solidarité nationale : / 1° Les dommages résultant d'infections nosocomiales dans les établissements, services ou organismes mentionnés au premier alinéa du I de l'article L. 1142-1 correspondant à un taux d'incapacité permanente supérieur à 25 % déterminé par référence au barème mentionné au II du même article, ainsi que les décès provoqués par ces infections nosocomiales (...) ". Ces dispositions, qui n'ont pas pour objet de définir les conditions dans lesquelles il est procédé à l'indemnisation du préjudice, mais de prévoir que les dommages résultant d'infections nosocomiales ayant entraîné une invalidité permanente d'un taux supérieur à 25 % ou le décès du patient peuvent être indemnisés au titre de la solidarité nationale, trouvent également à s'appliquer dans le cas où une infection nosocomiale a entraîné la perte d'une chance d'éviter de tels préjudices.<br/>
<br/>
              3. En premier lieu, en estimant que l'opération chirurgicale pratiquée sur Mme D... permet généralement une amélioration partielle ou totale de l'état de santé des patients ayant connu le même type d'accident ischémique, la cour a porté sur les pièces du dossier une appréciation souveraine, exempte de dénaturation. Elle a pu, sans erreur de droit, en déduire que, même s'il n'était pas certain que l'intervention chirurgicale aurait, en l'absence d'infection nosocomiale, amélioré davantage l'état de santé de Mme D..., cette infection avait néanmoins fait perdre à l'intéressée une chance d'amélioration de cet état de santé.<br/>
<br/>
              4. Pour l'application des dispositions de l'article L. 1142-1-1 du code de la santé publique dans l'hypothèse où une infection nosocomiale est à l'origine d'un préjudice constitué d'une perte de chance, le préjudice est indemnisé au titre de la solidarité nationale lorsque le taux d'atteinte permanente à l'intégrité du patient, calculé par la différence entre, d'une part, la capacité que l'intéressé aurait eu une très grande probabilité de récupérer grâce à l'intervention en l'absence de cette infection et, d'autre part, la capacité constatée après consolidation du préjudice résultant de l'infection, est supérieur à 25%.<br/>
<br/>
              5. Pour déterminer le taux d'incapacité permanente dont Mme D... reste atteinte, la cour a retenu que l'acte chirurgical qui était prévu avait une très grande probabilité, compte tenu du jeune âge de la patiente, de permettre sa guérison quasi-totale. En déterminant le taux d'atteinte permanente, conformément à la méthode décrite au point 4, par rapport à la capacité que Mme D... aurait très probablement pu récupérer grâce à l'intervention et en l'absence d'infection nosocomiale, la cour n'a pas commis d'erreur de droit.<br/>
<br/>
              6. Il résulte de ce qui précède que l'ONIAM n'est pas fondé à demander l'annulation de l'arrêt de la cour administrative de Marseille qu'il attaque.<br/>
<br/>
              Sur le pourvoi incident de Mme D... :<br/>
<br/>
              7. Lorsque le juge administratif indemnise dans le chef de la victime d'un dommage corporel la nécessité de recourir à l'aide d'une tierce personne, il détermine le montant de l'indemnité réparant ce préjudice en fonction des besoins de la victime et des dépenses nécessaires pour y pourvoir. Il doit à cette fin se fonder sur un taux horaire permettant, dans les circonstances de l'espèce, le recours à l'aide professionnelle d'une tierce personne d'un niveau de qualification adéquat, sans être lié par les débours effectifs dont la victime peut justifier. Il n'appartient notamment pas au juge, pour déterminer cette indemnisation, de tenir compte de la circonstance que l'aide a été ou pourrait être apportée par un membre de la famille ou un proche de la victime. <br/>
<br/>
              8. En tenant compte de la circonstance que l'assistance nécessaire à la jeune victime était assurée par un membre de sa famille pour écarter toute prise en compte des majorations de rémunération dues les dimanches et jours fériés, ainsi que des congés payés, la cour a commis une erreur de droit. Son arrêt doit par suite être annulé en tant qu'il statue sur l'indemnisation de l'assistance par une tierce personne d'une part pour la période antérieure à la consolidation de son état, fixée au 4 décembre 2008, et d'autre part pour la période postérieure à la consolidation jusqu'à la lecture de son arrêt.<br/>
<br/>
              9. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de Mme D..., qui n'est pas la partie perdante dans la présente instance, la somme que l'ONIAM demande à ce titre. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'ONIAM la somme de 3 000 euros à verser, au même titre, à Mme D....<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
		Article 1er : Le pourvoi de l'ONIAM est rejeté.<br/>
<br/>
Article 2 : L'arrêt de la cour administrative d'appel de Marseille est annulé en tant qu'il statue sur l'indemnisation de l'assistance par une tierce personne d'une part pour la période antérieure à la consolidation de son état, fixée au 4 décembre 2008, et d'autre part pour la période postérieure à la consolidation jusqu'à la lecture de son arrêt.<br/>
<br/>
Article 3 : L'affaire est renvoyée dans cette mesure à la cour administrative d'appel de Marseille.<br/>
Article 4 : L'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales versera une somme de 3 000 euros à Mme D... au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 5 : La présente décision sera notifiée à l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales et à Mme C... D....<br/>
Copie en sera adressée à la caisse primaire d'assurance maladie des Alpes-Maritimes, à la caisse primaire d'assurance maladie du Var et à la mutuelle des étudiants.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">60-01-05 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. FAITS SUSCEPTIBLES OU NON D'OUVRIR UNE ACTION EN RESPONSABILITÉ. RESPONSABILITÉ RÉGIE PAR DES TEXTES SPÉCIAUX. - RÉGIME D'INDEMNISATION AU TITRE DE LA SOLIDARITÉ NATIONALE (ART. L. 1142-1-1 DU CSP) - 1) APPLICABILITÉ DANS LE CAS OÙ UNE INFECTION NOSOCOMIALE A ENTRAÎNÉ LA PERTE D'UNE CHANCE D'ÉVITER DES PRÉJUDICES - EXISTENCE [RJ1] - 2) CONDITION - DOMMAGES CORRESPONDANT À UN TAUX D'ATTEINTE PERMANENTE À L'INTÉGRITÉ PHYSIQUE OU PSYCHIQUE SUPÉRIEUR À 25 % - MODALITÉS DE CALCUL DE CE TAUX - DIFFÉRENCE ENTRE LA CAPACITÉ QUE L'INTÉRESSÉ AURAIT EU UNE TRÈS GRANDE PROBABILITÉ DE RÉCUPÉRER GRÂCE À L'INTERVENTION EN L'ABSENCE DE CETTE INFECTION ET LA CAPACITÉ CONSTATÉE APRÈS CONSOLIDATION DU PRÉJUDICE RÉSULTANT DE L'INFECTION.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">60-02-01-01-005-02 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RESPONSABILITÉ EN RAISON DES DIFFÉRENTES ACTIVITÉS DES SERVICES PUBLICS. SERVICE PUBLIC DE SANTÉ. ÉTABLISSEMENTS PUBLICS D'HOSPITALISATION. RESPONSABILITÉ SANS FAUTE. ACTES MÉDICAUX. - RÉGIME D'INDEMNISATION AU TITRE DE LA SOLIDARITÉ NATIONALE (ART. L. 1142-1-1 DU CSP) - 1) APPLICABILITÉ DANS LE CAS OÙ UNE INFECTION NOSOCOMIALE A ENTRAÎNÉ LA PERTE D'UNE CHANCE D'ÉVITER DES PRÉJUDICES - EXISTENCE [RJ1] - 2) CONDITION - DOMMAGES CORRESPONDANT À UN TAUX D'ATTEINTE PERMANENTE À L'INTÉGRITÉ PHYSIQUE OU PSYCHIQUE SUPÉRIEUR À 25 % - MODALITÉS DE CALCUL DE CE TAUX - DIFFÉRENCE ENTRE LA CAPACITÉ QUE L'INTÉRESSÉ AURAIT EU UNE TRÈS GRANDE PROBABILITÉ DE RÉCUPÉRER GRÂCE À L'INTERVENTION EN L'ABSENCE DE CETTE INFECTION ET LA CAPACITÉ CONSTATÉE APRÈS CONSOLIDATION DU PRÉJUDICE RÉSULTANT DE L'INFECTION.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">60-04-03 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RÉPARATION. ÉVALUATION DU PRÉJUDICE. - RÉGIME D'INDEMNISATION AU TITRE DE LA SOLIDARITÉ NATIONALE (ART. L. 1142-1-1 DU CSP) - 1) APPLICABILITÉ DANS LE CAS OÙ UNE INFECTION NOSOCOMIALE A ENTRAÎNÉ LA PERTE D'UNE CHANCE D'ÉVITER DES PRÉJUDICES - EXISTENCE [RJ1] - 2) CONDITION - DOMMAGES CORRESPONDANT À UN TAUX D'ATTEINTE PERMANENTE À L'INTÉGRITÉ PHYSIQUE OU PSYCHIQUE SUPÉRIEUR À 25 % - MODALITÉS DE CALCUL DE CE TAUX - DIFFÉRENCE ENTRE LA CAPACITÉ QUE L'INTÉRESSÉ AURAIT EU UNE TRÈS GRANDE PROBABILITÉ DE RÉCUPÉRER GRÂCE À L'INTERVENTION EN L'ABSENCE DE CETTE INFECTION ET LA CAPACITÉ CONSTATÉE APRÈS CONSOLIDATION DU PRÉJUDICE RÉSULTANT DE L'INFECTION.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">60-04-04-01 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RÉPARATION. MODALITÉS DE LA RÉPARATION. SOLIDARITÉ. - RÉGIME D'INDEMNISATION AU TITRE DE LA SOLIDARITÉ NATIONALE (ART. L. 1142-1-1 DU CSP) - 1) APPLICABILITÉ DANS LE CAS OÙ UNE INFECTION NOSOCOMIALE A ENTRAÎNÉ LA PERTE D'UNE CHANCE D'ÉVITER DES PRÉJUDICES - EXISTENCE [RJ1] - 2) CONDITION - DOMMAGES CORRESPONDANT À UN TAUX D'ATTEINTE PERMANENTE À L'INTÉGRITÉ PHYSIQUE OU PSYCHIQUE SUPÉRIEUR À 25 % - MODALITÉS DE CALCUL DE CE TAUX - DIFFÉRENCE ENTRE LA CAPACITÉ QUE L'INTÉRESSÉ AURAIT EU UNE TRÈS GRANDE PROBABILITÉ DE RÉCUPÉRER GRÂCE À L'INTERVENTION EN L'ABSENCE DE CETTE INFECTION ET LA CAPACITÉ CONSTATÉE APRÈS CONSOLIDATION DU PRÉJUDICE RÉSULTANT DE L'INFECTION.
</SCT>
<ANA ID="9A"> 60-01-05 1) L'article L. 1142-1-1 du code de la santé publique (CSP), qui n'a pas pour objet de définir les conditions dans lesquelles il est procédé à l'indemnisation du préjudice, mais de prévoir que les dommages résultant d'infections nosocomiales ayant entraîné une invalidité permanente d'un taux supérieur à 25 % ou le décès du patient peuvent être indemnisés au titre de la solidarité nationale, trouve également à s'appliquer dans le cas où une infection nosocomiale a entraîné la perte d'une chance d'éviter de tels préjudices.,,,2) Pour l'application de cet article dans l'hypothèse où une infection nosocomiale est à l'origine d'un préjudice constitué d'une perte de chance, le préjudice est indemnisé au titre de la solidarité nationale lorsque le taux d'atteinte permanente à l'intégrité du patient, calculé par la différence entre, d'une part, la capacité que l'intéressé aurait eu une très grande probabilité de récupérer grâce à l'intervention en l'absence de cette infection et, d'autre part, la capacité constatée après consolidation du préjudice résultant de l'infection, est supérieur à 25%.</ANA>
<ANA ID="9B"> 60-02-01-01-005-02 1) L'article L. 1142-1-1 du code de la santé publique (CSP), qui n'a pas pour objet de définir les conditions dans lesquelles il est procédé à l'indemnisation du préjudice, mais de prévoir que les dommages résultant d'infections nosocomiales ayant entraîné une invalidité permanente d'un taux supérieur à 25 % ou le décès du patient peuvent être indemnisés au titre de la solidarité nationale, trouve également à s'appliquer dans le cas où une infection nosocomiale a entraîné la perte d'une chance d'éviter de tels préjudices.,,,2) Pour l'application de cet article dans l'hypothèse où une infection nosocomiale est à l'origine d'un préjudice constitué d'une perte de chance, le préjudice est indemnisé au titre de la solidarité nationale lorsque le taux d'atteinte permanente à l'intégrité du patient, calculé par la différence entre, d'une part, la capacité que l'intéressé aurait eu une très grande probabilité de récupérer grâce à l'intervention en l'absence de cette infection et, d'autre part, la capacité constatée après consolidation du préjudice résultant de l'infection, est supérieur à 25%.</ANA>
<ANA ID="9C"> 60-04-03 1) L'article L. 1142-1-1 du code de la santé publique (CSP), qui n'a pas pour objet de définir les conditions dans lesquelles il est procédé à l'indemnisation du préjudice, mais de prévoir que les dommages résultant d'infections nosocomiales ayant entraîné une invalidité permanente d'un taux supérieur à 25 % ou le décès du patient peuvent être indemnisés au titre de la solidarité nationale, trouve également à s'appliquer dans le cas où une infection nosocomiale a entraîné la perte d'une chance d'éviter de tels préjudices.,,,2) Pour l'application de cet article dans l'hypothèse où une infection nosocomiale est à l'origine d'un préjudice constitué d'une perte de chance, le préjudice est indemnisé au titre de la solidarité nationale lorsque le taux d'atteinte permanente à l'intégrité du patient, calculé par la différence entre, d'une part, la capacité que l'intéressé aurait eu une très grande probabilité de récupérer grâce à l'intervention en l'absence de cette infection et, d'autre part, la capacité constatée après consolidation du préjudice résultant de l'infection, est supérieur à 25%.</ANA>
<ANA ID="9D"> 60-04-04-01 1) L'article L. 1142-1-1 du code de la santé publique (CSP), qui n'a pas pour objet de définir les conditions dans lesquelles il est procédé à l'indemnisation du préjudice, mais de prévoir que les dommages résultant d'infections nosocomiales ayant entraîné une invalidité permanente d'un taux supérieur à 25 % ou le décès du patient peuvent être indemnisés au titre de la solidarité nationale, trouve également à s'appliquer dans le cas où une infection nosocomiale a entraîné la perte d'une chance d'éviter de tels préjudices.,,,2) Pour l'application de cet article dans l'hypothèse où une infection nosocomiale est à l'origine d'un préjudice constitué d'une perte de chance, le préjudice est indemnisé au titre de la solidarité nationale lorsque le taux d'atteinte permanente à l'intégrité du patient, calculé par la différence entre, d'une part, la capacité que l'intéressé aurait eu une très grande probabilité de récupérer grâce à l'intervention en l'absence de cette infection et, d'autre part, la capacité constatée après consolidation du préjudice résultant de l'infection, est supérieur à 25%.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 12 mars 2014, ONIAM c/ M. et Mme,, n° 359473, T. pp. 855-862.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
