<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042143151</ID>
<ANCIEN_ID>JG_L_2020_07_000000440149</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/14/31/CETATEXT000042143151.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 5ème chambres réunies, 22/07/2020, 440149</TITRE>
<DATE_DEC>2020-07-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>440149</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Catherine Moreau</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier  Fuchs</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:440149.20200722</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 17 avril 2020 au secrétariat du contentieux du Conseil d'Etat, M. A... B... et l'association de défense des libertés constitutionnelles demandent au Conseil d'Etat d'annuler l'article 3 du décret n° 2020-293 du 23 mars 2020 prescrivant les mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment ses articles 61-1 et 66 ; <br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - la loi organique n° 2020-365 du 30 mars 2020 ;<br/>
              - le code de la santé publique ;<br/>
              - la décision du Conseil constitutionnel n° 2020-800 DC du 11 mai 2020 ; <br/>
              - la décision du Conseil constitutionnel n° 2020-846/847/848 QPC du 26 juin 2020 ;<br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Catherine Moreau, conseiller d'Etat en service extraordinaire,  <br/>
              - les conclusions de M. Olivier Fuchs, rapporteur public ;<br/>
<br/>
              Vu les deux notes en délibéré, enregistrées le 3 juillet 2020 et la note en délibéré enregistrée le 21 juillet 2020, présentées par M. B... et l'association de défense des libertés constitutionnelles ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              Sur la question prioritaire de constitutionnalité : <br/>
<br/>
              1. Aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...) ". Il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              2. La loi du 23 mars 2020 d'urgence pour faire face à l'épidémie de covid-19 a introduit dans le code de la santé publique un article L. 3131-12 aux termes duquel " L'état d'urgence sanitaire peut être déclaré sur tout ou partie du territoire métropolitain ainsi que du territoire des collectivités régies par les articles 73 et 74 de la Constitution et de la Nouvelle-Calédonie en cas de catastrophe sanitaire mettant en péril, par sa nature et sa gravité, la santé de la population ". L'article L. 3131-15, issu de cette même loi, dispose que : " Dans les circonscriptions territoriales où l'état d'urgence sanitaire est déclaré, le Premier ministre peut, par décret réglementaire pris sur le rapport du ministre chargé de la santé, aux seules fins de garantir la santé publique : (...) 2° Interdire aux personnes de sortir de leur domicile, sous réserve des déplacements strictement indispensables aux besoins familiaux ou de santé ; (...). / Les mesures prescrites en application des 1° à 10° du présent article sont strictement proportionnées aux risques sanitaires encourus et appropriées aux circonstances de temps et de lieu. Il y est mis fin sans délai lorsqu'elles ne sont plus nécessaires ". <br/>
<br/>
              3. Les requérants soutiennent, en premier lieu, qu'en ne prévoyant pas l'intervention de l'autorité judiciaire aux fins de vérifier l'absence de caractère arbitraire de l'interdiction de sortie du domicile que peut décider le Premier ministre en application du 2° du I de l'article L. 3131-15, le législateur aurait méconnu la compétence de l'autorité judiciaire résultant de l'article 66 de la Constitution aux termes duquel " Nul ne peut être arbitrairement détenu. / L'autorité judiciaire, gardienne de la liberté individuelle, assure le respect de ce principe dans les conditions prévues par la loi ", alors que cette interdiction peut avoir pour effet d'imposer aux personnes concernées de rester à leur domicile pendant une durée pouvant excéder douze heures par vingt-quatre heures.<br/>
<br/>
              4. Cependant, si les dispositions contestées de l'article L. 3131-15 permettent au Premier ministre, dans les circonscriptions territoriales où l'état d'urgence sanitaire est déclaré et pour garantir la santé publique, d'interdire aux personnes de sortir de leur domicile, elles précisent que la mesure doit être strictement proportionnée aux risques sanitaires encourus et appropriée aux circonstances de temps et de lieu, qu'il y est mis fin sans délai lorsqu'elle n'est plus nécessaire et réservent expressément les déplacements indispensables aux besoins familiaux ou de santé. Les dispositions contestées donnent ainsi au Premier ministre, lorsque la situation l'exige et que les conditions posées sont remplies, la possibilité non d'interdire, par une mesure individuelle, à une personne déterminée de sortir de son domicile, mais de prendre un acte réglementaire à caractère général, ayant pour objet de viser un ensemble des personnes se trouvant dans une circonscription territoriale dans laquelle l'état d'urgence sanitaire est déclaré, et qui n'a d'autre but, conformément à l'objectif de valeur constitutionnelle de protection de la santé, que de protéger la santé de l'ensemble de la population en prévenant la propagation incontrôlée d'une épidémie. La contestation d'une telle mesure, eu égard à sa nature et à son objet, n'est pas au nombre de celles que l'article 66 de la Constitution réserve à la compétence de l'autorité judiciaire. <br/>
<br/>
              5. Les requérants soutiennent, en second lieu, que faute de prévoir un examen en temps utile par le juge administratif de la légalité de la mesure d'interdiction prévue par les dispositions litigieuses, ces dernières méconnaîtraient le droit à un recours juridictionnel effectif garanti par l'article 16 de la Déclaration des droits de l'homme et du citoyen de 1789.<br/>
<br/>
              6. Toutefois, dès lors que le décret réglementaire pris en application du 2° de l'article L.3131-15 du même code peut faire l'objet d'un recours pour excès de pouvoir devant le Conseil d'Etat ainsi, comme le rappellent les dispositions de l'article L. 3131-18 du code de la santé publique issues elles aussi de l'article 2 de la loi du 23 mars 2020, que des procédures de référé prévues par les articles L. 521-1 et L. 521-2 du code de justice administrative, les dispositions litigieuses ne peuvent être regardées comme méconnaissant le droit à un recours juridictionnel effectif. <br/>
<br/>
              7. Il résulte de ce qui précède que, sans qu'il soit besoin de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée, le moyen tiré de ce que le 2° du I de l'article L. 3131-15 du code de la santé publique porterait atteinte aux droits et libertés que la Constitution garantit doit être écarté. <br/>
<br/>
              Sur l'autre moyen de la requête : <br/>
<br/>
              8. Dès lors que l'interdiction de déplacement posée par l'article 3 du décret attaqué ne présente pas le caractère d'une mesure individuelle privative de liberté au sens de l'article 66 de la Constitution, il résulte de ce qui a été dit précédemment que ce décret n'avait pas à prévoir l'intervention de l'autorité judiciaire en vertu de l'article 66 de la Constitution.<br/>
<br/>
              9. Par suite, M. B... et l'association de défense des libertés constitutionnelles ne sont pas fondés à demander l'annulation pour excès de pouvoir des dispositions du décret qu'ils attaquent. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par M. B... et l'association de défense des libertés constitutionnelles.<br/>
Article 2 : La requête de M. B... et autre est rejetée.<br/>
Article 3 : La présente décision sera notifiée à M. A... B..., à l'association de défense des libertés constitutionnelles, au Premier ministre, au ministre des solidarités et de la santé et au garde des sceaux, ministre de la justice.<br/>
Copie en sera adressée au Conseil constitutionnel. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-04-005 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. CONSTITUTION ET PRINCIPES DE VALEUR CONSTITUTIONNELLE. - ARTICLE 66 DE LA CONSTITUTION - CHAMP D'APPLICATION - EXCLUSION - MESURES RÉGLEMENTAIRES DE CONFINEMENT POUVANT ÊTRE ORDONNÉES DANS LE CADRE DE L'ÉTAT D'URGENCE SANITAIRE (2° DU I DE L'ART. L. 3131-15 DU CSP) [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">17-03-02-08-01 COMPÉTENCE. RÉPARTITION DES COMPÉTENCES ENTRE LES DEUX ORDRES DE JURIDICTION. COMPÉTENCE DÉTERMINÉE PAR UN CRITÈRE JURISPRUDENTIEL. LIBERTÉ INDIVIDUELLE, PROPRIÉTÉ PRIVÉE ET ÉTAT DES PERSONNES. LIBERTÉ INDIVIDUELLE. - ARTICLE 66 DE LA CONSTITUTION - CHAMP D'APPLICATION - EXCLUSION - MESURES RÉGLEMENTAIRES DE CONFINEMENT POUVANT ÊTRE ORDONNÉES DANS LE CADRE DE L'ÉTAT D'URGENCE SANITAIRE (2° DU I DE L'ART. L. 3131-15 DU CSP) [RJ1].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">61-01-01-02 SANTÉ PUBLIQUE. PROTECTION GÉNÉRALE DE LA SANTÉ PUBLIQUE. POLICE ET RÉGLEMENTATION SANITAIRE. LUTTE CONTRE LES ÉPIDÉMIES. - ETAT D'URGENCE SANITAIRE - MESURES RÉGLEMENTAIRES DE CONFINEMENT (2° DU I DE L'ART. L. 3131-15 DU CSP) - COMPÉTENCE DE L'AUTORITÉ JUDICIAIRE (ART. 66 DE LA CONSTITUTION) - ABSENCE [RJ1].
</SCT>
<ANA ID="9A"> 01-04-005 Si le 2° du I de l'article L. 3131-15 du code de la santé public (CSP) permet au Premier ministre, dans les circonscriptions territoriales où l'état d'urgence sanitaire est déclaré et pour garantir la santé publique, d'interdire aux personnes de sortir de leur domicile, il précise que la mesure doit être strictement proportionnée aux risques sanitaires encourus et appropriée aux circonstances de temps et de lieu, qu'il y est mis fin sans délai lorsqu'elle n'est plus nécessaire et réserve expressément les déplacements indispensables aux besoins familiaux ou de santé.,,,Ces dispositions donnent ainsi au Premier ministre, lorsque la situation l'exige et que les conditions posées sont remplies, la possibilité non d'interdire, par une mesure individuelle, à une personne déterminée de sortir de son domicile, mais de prendre un acte réglementaire à caractère général, ayant pour objet de viser un ensemble des personnes se trouvant dans une circonscription territoriale dans laquelle l'état d'urgence sanitaire est déclaré, et qui n'a d'autre but, conformément à l'objectif de valeur constitutionnelle de protection de la santé, que de protéger la santé de l'ensemble de la population en prévenant la propagation incontrôlée d'une épidémie.... ,,La contestation d'une telle mesure, eu égard à sa nature et à son objet, n'est pas au nombre de celles que l'article 66 de la Constitution réserve à la compétence de l'autorité judiciaire.</ANA>
<ANA ID="9B"> 17-03-02-08-01 Si le 2° du I de l'article L. 3131-15 du code de la santé public (CSP) permet au Premier ministre, dans les circonscriptions territoriales où l'état d'urgence sanitaire est déclaré et pour garantir la santé publique, d'interdire aux personnes de sortir de leur domicile, il précise que la mesure doit être strictement proportionnée aux risques sanitaires encourus et appropriée aux circonstances de temps et de lieu, qu'il y est mis fin sans délai lorsqu'elle n'est plus nécessaire et réserve expressément les déplacements indispensables aux besoins familiaux ou de santé.,,,Ces dispositions donnent ainsi au Premier ministre, lorsque la situation l'exige et que les conditions posées sont remplies, la possibilité non d'interdire, par une mesure individuelle, à une personne déterminée de sortir de son domicile, mais de prendre un acte réglementaire à caractère général, ayant pour objet de viser un ensemble des personnes se trouvant dans une circonscription territoriale dans laquelle l'état d'urgence sanitaire est déclaré, et qui n'a d'autre but, conformément à l'objectif de valeur constitutionnelle de protection de la santé, que de protéger la santé de l'ensemble de la population en prévenant la propagation incontrôlée d'une épidémie.... ,,La contestation d'une telle mesure, eu égard à sa nature et à son objet, n'est pas au nombre de celles que l'article 66 de la Constitution réserve à la compétence de l'autorité judiciaire.</ANA>
<ANA ID="9C"> 61-01-01-02 Si le 2° du I de l'article L. 3131-15 du code de la santé public (CSP) permet au Premier ministre, dans les circonscriptions territoriales où l'état d'urgence sanitaire est déclaré et pour garantir la santé publique, d'interdire aux personnes de sortir de leur domicile, il précise que la mesure doit être strictement proportionnée aux risques sanitaires encourus et appropriée aux circonstances de temps et de lieu, qu'il y est mis fin sans délai lorsqu'elle n'est plus nécessaire et réserve expressément les déplacements indispensables aux besoins familiaux ou de santé.,,,Ces dispositions donnent ainsi au Premier ministre, lorsque la situation l'exige et que les conditions posées sont remplies, la possibilité non d'interdire, par une mesure individuelle, à une personne déterminée de sortir de son domicile, mais de prendre un acte réglementaire à caractère général, ayant pour objet de viser un ensemble des personnes se trouvant dans une circonscription territoriale dans laquelle l'état d'urgence sanitaire est déclaré, et qui n'a d'autre but, conformément à l'objectif de valeur constitutionnelle de protection de la santé, que de protéger la santé de l'ensemble de la population en prévenant la propagation incontrôlée d'une épidémie.... ,,La contestation d'une telle mesure, eu égard à sa nature et à son objet, n'est pas au nombre de celles que l'article 66 de la Constitution réserve à la compétence de l'autorité judiciaire.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Comp., s'agissant de la nécessité, dans certains cas, d'une intervention du juge judiciaire pour les mesures individuelles (mise en quarantaine, placement et maintien en isolement) prévues au 3° et 4° du I du même article, Cons. const., 11 mai 2020, n° 2020-800 DC, Loi prorogeant l'état d'urgence sanitaire et complétant ses dispositions.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
