<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039357530</ID>
<ANCIEN_ID>JG_L_2019_11_000000408514</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/35/75/CETATEXT000039357530.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 1ère chambres réunies, 08/11/2019, 408514</TITRE>
<DATE_DEC>2019-11-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>408514</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS</AVOCATS>
<RAPPORTEUR>M. Bruno Bachini</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:408514.20191108</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Mme B... A... a demandé au tribunal administratif de Grenoble d'annuler les décisions du 27 novembre 2012 et du 15 janvier 2013 par lesquelles le recteur de l'académie de Grenoble lui a refusé le bénéfice de l'allocation d'assurance liée à la perte d'emploi, d'enjoindre au recteur de réexaminer sa demande et de condamner l'Etat à lui verser une indemnité au titre des allocations non perçues. Par un jugement n° 1301394 du 25 février 2015, le tribunal administratif a annulé pour excès de pouvoir ces deux décisions, enjoint au recteur de réexaminer la demande de Mme A... et rejeté le surplus de ses conclusions. <br/>
<br/>
              Par une ordonnance n° 15LY01553 du 23 février 2017, enregistrée le 1er mars 2017 au secrétariat du contentieux du Conseil d'Etat, la cour administrative d'appel de Lyon a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, le recours, enregistré le 11 mai 2015, présenté par le ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche. Par ce recours, un mémoire en réplique, enregistré le 5 décembre 2016 au greffe de cette cour, et un nouveau mémoire, enregistré au secrétariat du contentieux du Conseil d'Etat le 27 juillet 2017, le ministre de l'éducation nationale demande au Conseil d'Etat :    <br/>
<br/>
              1°) d'annuler ce jugement en tant qu'il lui fait grief ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de Mme A....<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code du travail ;<br/>
              - la loi n° 2012-347 du 12 mars 2012 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bruno Bachini, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de Mme A....<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, le 27 juin 2012, le recteur de l'académie de Grenoble a proposé à Mme A..., professeur contractuel de l'enseignement secondaire ayant bénéficié de contrats à durée déterminée, à compter de 2000, pour assurer un enseignement en économie, gestion et communication dans divers établissements publics locaux d'enseignement, à l'occasion du renouvellement de son contrat en cours arrivant à échéance le 31 août 2012, la transformation de celui-ci en un contrat à durée indéterminée, en application des dispositions de l'article 8 de la loi du 12 mars 2012 relative à l'accès à l'emploi titulaire et à l'amélioration des conditions d'emploi des agents contractuels dans la fonction publique, à la lutte contre les discriminations et portant diverses dispositions relatives à la fonction publique. Ayant refusé cette transformation, elle a demandé, après la fin de son contrat, à bénéficier de l'allocation d'assurance pour perte d'emploi. Par des décisions du 27 novembre 2012 et du 15 janvier 2013, le recteur de l'académie de Grenoble a refusé de faire droit à cette demande. Par un jugement du 25 février 2015 contre lequel le ministre de l'éducation nationale se pourvoit en cassation, le tribunal administratif de Grenoble a, sur la demande de Mme A..., annulé ces décisions et enjoint au recteur de réexaminer sa demande.<br/>
<br/>
              2. Aux termes de l'article L. 5422-1 du code du travail dans sa version alors applicable : " Ont droit à l'allocation d'assurance les travailleurs involontairement privés d'emploi (...) ". Aux termes de l'article L. 5424-1 du même code dans sa version alors en vigueur : " Ont droit à une allocation d'assurance dans les conditions prévues aux articles L. 5422-2 et L. 5422-3 : / 1° Les agents fonctionnaires et non fonctionnaires de l'Etat et de ses établissements publics administratifs (...) ". Aux termes de l'article 8 de la loi du 12 mars 2012 : " A la date de publication de la présente loi, la transformation de son contrat en contrat à durée indéterminée est obligatoirement proposée à l'agent contractuel, employé par l'Etat (...) sur le fondement du dernier alinéa de l'article 3 ou des article 4 ou 6 de la loi n° 84-16 du 11 janvier 1984 (...) ". <br/>
<br/>
              3. L'agent mentionné à l'article L. 5424-1 du code du travail, qui refuse la transformation de son contrat à durée déterminée en contrat à durée indéterminée, ne peut être regardé comme involontairement privé d'emploi, à moins que ce refus soit fondé sur un motif légitime. Un tel motif peut être lié notamment à des considérations d'ordre personnel ou au fait que le contrat a été modifié de façon substantielle par l'employeur sans justification. <br/>
<br/>
              4. Il ressort des pièces du dossier soumis aux juges du fond que le contrat à durée indéterminée proposé à Mme A..., dont le contrat à durée déterminée, comme d'ailleurs tous les précédents contrats, prévoyait l'affectation dans un unique établissement qu'il désignait, stipulait que l'intéressée exercerait ses fonctions dans le ressort de l'académie de Grenoble et que son affectation serait déterminée et modifiée par le recteur compte tenu des besoins du service. Le tribunal administratif de Grenoble n'a pas inexactement qualifié les faits en jugeant que cette extension du périmètre au sein duquel l'intéressée était susceptible d'être, à l'avenir, appelée à exercer ses fonctions constituait une modification substantielle de son contrat. Toutefois, le tribunal administratif de Grenoble a inexactement qualifié les faits de l'espèce en estimant que la modification du contrat de Mme A... n'était pas justifiée par le recteur de l'académie de Grenoble et en en déduisant que le refus de l'intéressée reposait sur un motif légitime permettant de la regarder comme involontairement privée d'emploi, dès lors qu'il ressort des pièces du dossier soumis aux juges du fond que cette modification était nécessaire compte tenu des conditions d'emploi des professeurs sous contrat à durée indéterminée, lesquels ont vocation à enseigner dans l'ensemble des établissements du ressort de l'académie en fonction des besoins du service. <br/>
<br/>
              5. Il résulte de tout ce qui précède, sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, que le ministre de l'éducation nationale et de la jeunesse est fondé à demander l'annulation des articles 1er, 2 et 4 du jugement qu'il attaque. <br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application de l'article L. 821-2 du code de justice administrative. <br/>
<br/>
              7. En premier lieu, les stipulations de l'article 3 du contrat à durée indéterminée proposé à Mme A... prévoyant que celle-ci est chargée d'assurer un service d'enseignement en économie, gestion et communication et qu'elle pourra, en tant que de besoin, effectuer son service en documentation ou en éducation, ne peuvent être regardées comme une modification substantielle de son contrat. En deuxième lieu, il résulte de ce qui a été dit au point 4 que l'extension au ressort de l'ensemble de l'académie du périmètre d'exercice des fonctions de Mme A..., qui constitue une modification substantielle de son contrat par l'employeur, est justifiée. Dès lors, le refus de Mme A... de conclure le contrat à durée indéterminée qui lui a été proposé, motivé par la modification substantielle de son contrat, ne peut être regardé comme reposant sur un motif légitime. En conséquence, elle ne peut être regardée comme involontairement privée d'emploi et ne saurait, par suite, prétendre au bénéfice de l'allocation d'assurance pour perte d'emploi. Il s'ensuit que Mme A... n'est pas fondée à demander l'annulation des décisions du recteur de l'académie de Grenoble qu'elle attaque et ses conclusions aux fins d'injonction ne peuvent qu'être également rejetées.<br/>
<br/>
              8. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante.  <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les articles 1er, 2 et 4 du jugement du tribunal administratif de Grenoble du 25 février 2015 sont annulés.<br/>
Article 2 : La demande présentée par Mme A... devant le tribunal administratif de Grenoble est rejetée.<br/>
Article 3 : Les conclusions de Mme A... présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 4 : La présente décision sera notifiée à Mme B... A... et au ministre de l'éducation nationale et de la jeunesse.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-10-06-04 FONCTIONNAIRES ET AGENTS PUBLICS. CESSATION DE FONCTIONS. LICENCIEMENT. ALLOCATION POUR PERTE D'EMPLOI. - AGENT INVOLONTAIREMENT PRIVÉ D'EMPLOI (ART. L. 5422-1 DU CODE DU TRAVAIL) - 1) NOTION - AGENT DE L'ETAT REFUSANT LA TRANSFORMATION DE SON CDD EN CDI (ART. 8 DE LA LOI DU 12 MARS 2012) - EXCLUSION, SAUF REFUS FONDÉ SUR UN MOTIF LÉGITIME [RJ1] - 2) ESPÈCE - CDI PROPOSÉ PRÉVOYANT L'EXTENSION DU PÉRIMÈTRE GÉOGRAPHIQUE D'EXERCICE D'UN PROFESSEUR DE L'ENSEIGNEMENT SECONDAIRE - A) MODIFICATION SUBSTANTIELLE DU CONTRAT - EXISTENCE - MODIFICATION JUSTIFIÉE PAR LES BESOINS DU SERVICE - EXISTENCE - APPRÉCIATION DE CES CONDITIONS SOUMISE À UN CONTRÔLE DE QUALIFICATION JURIDIQUE DES FAITS DU JUGE DE CASSATION - B) CONSÉQUENCE - REFUS DE CONCLURE LE CDI NON FONDÉ SUR UN MOTIF LÉGITIME.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-08-02-02-01-02 PROCÉDURE. VOIES DE RECOURS. CASSATION. CONTRÔLE DU JUGE DE CASSATION. BIEN-FONDÉ. QUALIFICATION JURIDIQUE DES FAITS. - CARACTÈRE SUBSTANTIEL DE LA MODIFICATION D'UN CONTRAT DE TRAVAIL - CARACTÈRE JUSTIFIÉ DE CETTE MODIFICATION.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">66-10-02 TRAVAIL ET EMPLOI. POLITIQUES DE L'EMPLOI. INDEMNISATION DES TRAVAILLEURS PRIVÉS D'EMPLOI. - AGENT INVOLONTAIREMENT PRIVÉ D'EMPLOI (ART. L. 5422-1 DU CODE DU TRAVAIL) - 1) NOTION - AGENT DE L'ETAT REFUSANT LA TRANSFORMATION DE SON CDD EN CDI (ART. 8 DE LA LOI DU 12 MARS 2012) - EXCLUSION, SAUF REFUS FONDÉ SUR UN MOTIF LÉGITIME [RJ1] - 2) ESPÈCE - CDI PROPOSÉ PRÉVOYANT L'EXTENSION DU PÉRIMÈTRE GÉOGRAPHIQUE D'EXERCICE D'UN PROFESSEUR DE L'ENSEIGNEMENT SECONDAIRE - A) MODIFICATION SUBSTANTIELLE DU CONTRAT - EXISTENCE - MODIFICATION JUSTIFIÉE PAR LES BESOINS DU SERVICE - EXISTENCE - APPRÉCIATION DE CES CONDITIONS SOUMISE À UN CONTRÔLE DE QUALIFICATION JURIDIQUE DES FAITS DU JUGE DE CASSATION - B) CONSÉQUENCE - REFUS DE CONCLURE LE CDI NON FONDÉ SUR UN MOTIF LÉGITIME.
</SCT>
<ANA ID="9A"> 36-10-06-04 1) L'agent mentionné à l'article L. 5424-1 du code du travail, qui refuse la transformation de son contrat à durée déterminée (CDD) en contrat à durée indéterminée (CDI), ne peut être regardé comme involontairement privé d'emploi, à moins que ce refus soit fondé sur un motif légitime. Un tel motif peut être lié notamment à des considérations d'ordre personnel ou au fait que le contrat a été modifié de façon substantielle par l'employeur sans justification.... ,,2) Professeur contractuel de l'enseignement secondaire, dont le CDD prévoyait l'affectation dans un unique établissement, s'étant vue proposer un CDI qui stipule qu'elle exercerait ses fonctions dans le ressort de l'académie de Grenoble et que son affectation serait déterminée et modifiée par le recteur compte tenu des besoins du service.... ,,a) Le tribunal administratif n'a pas inexactement qualifié les faits en jugeant que cette extension du périmètre au sein duquel l'intéressée était susceptible d'être, à l'avenir, appelée à exercer ses fonctions constituait une modification substantielle de son contrat.... ,,Toutefois, le tribunal administratif a inexactement qualifié les faits de l'espèce en estimant que la modification du contrat de l'intéressée n'était pas justifiée par le recteur de l'académie de Grenoble et en en déduisant que le refus de l'intéressée reposait sur un motif légitime permettant de la regarder comme involontairement privée d'emploi, dès lors qu'il ressort des pièces du dossier que cette modification était nécessaire compte tenu des conditions d'emploi des professeurs sous CDI, lesquels ont vocation à enseigner dans l'ensemble des établissements du ressort de l'académie en fonction des besoins du service.... ,,b) Dès lors, le refus de l'intéressé de conclure le CDI qui lui a été proposé, motivé par la modification substantielle de son contrat, ne peut être regardé comme reposant sur un motif légitime. En conséquence, elle ne peut être regardé comme involontairement privée d'emploi et ne saurait, par suite, prétendre au bénéfice de l'allocation d'assurance pour perte d'emploi.</ANA>
<ANA ID="9B"> 54-08-02-02-01-02 Les appréciations portées, d'une part, sur le caractère substantiel de la modification d'un contrat de travail, d'autre part, sur caractère justifié de cette modification, caractères qui constituent les conditions pour que le refus de transformation du CDD d'un agent public en CDI, en application de l'article 8 de la loi n° 2012-347 du 12 mars 2012, soit regardé comme fondé sur un motif légitime et que cet agent puisse dès lors être regardé comme involontairement privé de d'emploi au sens de l'article L. 5422-1 du code du travail, font l'objet d'un contrôle de qualification juridique des faits de la part du juge de cassation.</ANA>
<ANA ID="9C"> 66-10-02 1) L'agent mentionné à l'article L. 5424-1 du code du travail, qui refuse la transformation de son contrat à durée déterminée (CDD) en contrat à durée indéterminée (CDI), ne peut être regardé comme involontairement privé d'emploi, à moins que ce refus soit fondé sur un motif légitime. Un tel motif peut être lié notamment à des considérations d'ordre personnel ou au fait que le contrat a été modifié de façon substantielle par l'employeur sans justification.... ,,2) Professeur contractuel de l'enseignement secondaire, dont le CDD prévoyait l'affectation dans un unique établissement, s'étant vue proposer un CDI qui stipule qu'elle exercerait ses fonctions dans le ressort de l'académie de Grenoble et que son affectation serait déterminée et modifiée par le recteur compte tenu des besoins du service.... ,,a) Le tribunal administratif n'a pas inexactement qualifié les faits en jugeant que cette extension du périmètre au sein duquel l'intéressée était susceptible d'être, à l'avenir, appelée à exercer ses fonctions constituait une modification substantielle de son contrat.... ,,Toutefois, le tribunal administratif a inexactement qualifié les faits de l'espèce en estimant que la modification du contrat de l'intéressée n'était pas justifiée par le recteur de l'académie de Grenoble et en en déduisant que le refus de l'intéressée reposait sur un motif légitime permettant de la regarder comme involontairement privée d'emploi, dès lors qu'il ressort des pièces du dossier que cette modification était nécessaire compte tenu des conditions d'emploi des professeurs sous CDI, lesquels ont vocation à enseigner dans l'ensemble des établissements du ressort de l'académie en fonction des besoins du service.... ,,b) Dès lors, le refus de l'intéressé de conclure le CDI qui lui a été proposé, motivé par la modification substantielle de son contrat, ne peut être regardé comme reposant sur un motif légitime. En conséquence, elle ne peut être regardé comme involontairement privée d'emploi et ne saurait, par suite, prétendre au bénéfice de l'allocation d'assurance pour perte d'emploi.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., s'agissant d'une démission, CE, 1er octobre 2001, Commune de Bouc-Bel-Air c/ Mme,, p. 451 ; s'agissant d'un refus de renouvellement de contrat, CE, 13 janvier 2003, Centre communal d'action sociale de Puyravault, n° 229251, T. pp. 837-1020.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
