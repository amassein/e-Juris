<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043574582</ID>
<ANCIEN_ID>JG_L_2021_05_000000433043</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/57/45/CETATEXT000043574582.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 5ème chambres réunies, 31/05/2021, 433043</TITRE>
<DATE_DEC>2021-05-31</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>433043</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BORE, SALVE DE BRUNETON, MEGRET</AVOCATS>
<RAPPORTEUR>Mme Airelle Niepce</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Stéphane Hoynck</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:433043.20210531</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La SARL MDC Hydro a demandé au tribunal administratif de Rouen d'annuler l'arrêté du préfet de l'Eure du 4 décembre 2012 constatant l'arrêt de l'exploitation de la centrale hydraulique dite du Val Anglier, située sur le territoire de la commune de Perriers-sur-Andelle, et précisant les conditions de sa gestion temporaire, ainsi que la décision implicite de rejet de son recours gracieux contre cet arrêté, du 7 février 2013. Par un jugement n° 1301246 du 25 novembre 2014, le tribunal administratif a rejeté ses demandes.<br/>
<br/>
              Par un arrêt n° 15DA00135 du 22 décembre 2016, la cour administrative d'appel de Douai a rejeté son appel formé contre ce jugement.<br/>
<br/>
              Par une décision n° 408663 du 22 octobre 2018, le Conseil d'Etat a annulé cet arrêt et renvoyé l'affaire devant la cour administrative d'appel de Douai. <br/>
<br/>
              Par un arrêt n° 18DA02139 du 28 mai 2019, la cour administrative d'appel de Douai a annulé les mots " qu'après la délivrance de l'autorisation d'exploiter " figurant à l'article 4 de l'arrêté attaqué, abrogé l'article 5 de ce même arrêté, réformé le jugement du tribunal administratif dans cette mesure et rejeté le surplus des conclusions de la SARL MDC Hydro. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 29 juillet et 29 octobre 2019 et 4 décembre 2020 au secrétariat du contentieux du Conseil d'Etat, la SARL MDC Hydro demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt en tant qu'il n'a fait que partiellement droit à sa demande de première instance ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'environnement ;<br/>
              - la loi n° 2017-227 du 24 février 2017 ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Airelle Niepce, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Stéphane Hoynck, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Boré, Salve de Bruneton, Mégret, avocat de la SARL MDC Hydro ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la SARL MDC Hydro a acquis en 2004 une centrale hydroélectrique, dite du Val Anglier, sur le territoire de la commune de Perriers-sur-Andelle, sur le cours d'eau l'Andelle, installation initialement autorisée par une ordonnance royale du 30 janvier 1839. Par un arrêté du 4 décembre 2012, le préfet de l'Eure a constaté l'arrêt de l'exploitation de la centrale depuis le 29 mars 2004, précisé les conditions de sa gestion temporaire et fixé les conditions de reprise de l'activité. Ainsi, aux termes de l'article 4 de cet arrêté : " La remise en service de la centrale (...) ne sera autorisée qu'après délivrance de l'autorisation d'exploiter et constat par le service de police de l'eau du respect des dispositions de l'article L. 432-6 du code de l'environnement et de la mise en conformité des installations à la continuité écologique (circulation piscicole des espèces migratrices et transit sédimentaire) ainsi que la mise en oeuvre de l'ensemble des dispositions concourant à la sécurité des personnes et des biens et nécessaires à l'exploitation de la centrale ".<br/>
<br/>
              2. La SARL MDC Hydro a demandé l'annulation de cet arrêté au tribunal administratif de Rouen puis à la cour administrative d'appel de Douai, qui ont rejeté ses demandes. Par une décision du 22 octobre 2018, le Conseil d'Etat, statuant au contentieux, a annulé l'arrêt de la cour et renvoyé l'affaire devant la cour administrative d'appel de Douai. Par un nouvel arrêt du 28 mai 2019, la cour a, d'une part, annulé les mots " qu'après délivrance de l'autorisation d'exploiter " figurant à l'article 4 de l'arrêté du 4 décembre 2012, abrogé l'article 5 du même arrêté et réformé le jugement du tribunal administratif de Rouen dans cette mesure et, d'autre part, rejeté le surplus des conclusions de la SARL MDC Hydro dirigées contre l'article 4 de l'arrêté préfectoral. La SARL MDC Hydro se pourvoit en cassation contre cet arrêt en tant qu'il rejette le surplus des conclusions de sa requête dirigées contre l'article 4 de cet arrêté.<br/>
<br/>
              3. Aux termes de l'article L. 214-17 du code de l'environnement dans sa rédaction applicable en l'espèce : "  I.- Après avis des conseils départementaux intéressés, des établissements publics territoriaux de bassin concernés, des comités de bassins et, en Corse, de l'Assemblée de Corse, l'autorité administrative établit, pour chaque bassin ou sous-bassin : / (...) / 2° Une liste de cours d'eau, parties de cours d'eau ou canaux dans lesquels il est nécessaire d'assurer le transport suffisant des sédiments et la circulation des poissons migrateurs. Tout ouvrage doit y être géré, entretenu et équipé selon des règles définies par l'autorité administrative, en concertation avec le propriétaire ou, à défaut, l'exploitant. / II.- Les listes visées aux 1° et 2° du I sont établies par arrêté de l'autorité administrative compétente, après étude de l'impact des classements sur les différents usages de l'eau visés à l'article L. 211-1. Elles sont mises à jour lors de la révision des schémas directeurs d'aménagement et de gestion des eaux pour tenir compte de l'évolution des connaissances et des enjeux propres aux différents usages. / III.- Les obligations résultant du I s'appliquent à la date de publication des listes. Celles découlant du 2° du I s'appliquent, à l'issue d'un délai de cinq ans après la publication des listes, aux ouvrages existants régulièrement installés. Lorsque les travaux permettant l'accomplissement des obligations résultant du 2° du I n'ont pu être réalisés dans ce délai, mais que le dossier relatif aux propositions d'aménagement ou de changement de modalités de gestion de l'ouvrage a été déposé auprès des services chargés de la police de l'eau, le propriétaire ou, à défaut, l'exploitant de l'ouvrage dispose d'un délai supplémentaire de cinq ans pour les réaliser. / Le cinquième alinéa de l'article 2 de la loi du 16 octobre 1919 relative à l'utilisation de l'énergie hydraulique et l'article L. 432-6 du présent code demeurent applicables jusqu'à ce que ces obligations y soient substituées, dans le délai prévu à l'alinéa précédent. A l'expiration du délai précité, et au plus tard le 1er janvier 2014, le cinquième alinéa de l'article 2 de la loi du 16 octobre 1919 précitée est supprimé et l'article L. 432-6 précité est abrogé. / Les obligations résultant du I du présent article n'ouvrent droit à indemnité que si elles font peser sur le propriétaire ou l'exploitant de l'ouvrage une charge spéciale et exorbitante. / (...) ". Par ailleurs, aux termes de l'article L. 214-18-1 du même code, qui résulte de l'article 15 de la loi du 24 février 2017 ratifiant les ordonnances du 27 juillet 2016 relative à l'autoconsommation d'électricité et du 3 août 2016 relative à la production d'électricité à partir d'énergies renouvelables et visant à adapter certaines dispositions relatives aux réseaux d'électricité et de gaz et aux énergies renouvelables : " Les moulins à eau équipés par leurs propriétaires, par des tiers délégués ou par des collectivités territoriales pour produire de l'électricité, régulièrement installés sur les cours d'eau, parties de cours d'eau ou canaux mentionnés au 2° du I de l'article L. 214-17, ne sont pas soumis aux règles définies par l'autorité administrative mentionnées au même 2°. Le présent article ne s'applique qu'aux moulins existant à la date de publication de la loi n° 2017-227 du 24 février 2017 ratifiant les ordonnances n° 2016-1019 du 27 juillet 2016 relative à l'autoconsommation d'électricité et n° 2016-1059 du 3 août 2016 relative à la production d'électricité à partir d'énergies renouvelables et visant à adapter certaines dispositions relatives aux réseaux d'électricité et de gaz et aux énergies renouvelables. "<br/>
<br/>
              4. Il résulte des dispositions de l'article L. 214-18-1 du code de l'environnement, telles qu'éclairées par les travaux préparatoires relatifs à la loi du 24 février 2017, qu'afin de préserver le patrimoine hydraulique que constituent les moulins à eau, le législateur a entendu exonérer l'ensemble des ouvrages pouvant recevoir cette qualification et bénéficiant d'un droit de prise d'eau fondé en titre ou d'une autorisation d'exploitation à la date de publication de la loi, des obligations mentionnées au 2° du I de l'article L. 214-17 du même code destinées à assurer la continuité écologique des cours d'eau. Les dispositions de l'article L. 214-18-1 du code de l'environnement ne peuvent ainsi être interprétées comme limitant le bénéfice de cette exonération aux seuls moulins hydrauliques mis en conformité avec ces obligations ou avec les obligations applicables antérieurement ayant le même objet.<br/>
<br/>
              5. Il ressort des énonciations de l'arrêt attaqué non contestées sur ce point en cassation que la centrale hydroélectrique dite du Val Anglier bénéficiait, du fait du droit d'usage de l'eau accordé par une ordonnance royale du 30 janvier 1839, d'un droit fondé en titre sur l'Andelle qui n'était pas abrogé à la date de publication de la loi du 24 février 2017. Pour juger que cette installation ainsi autorisée était, à la date de son arrêt, soumise aux obligations résultant du 2° du I de l'article L. 214-17 du code de l'environnement, la cour administrative d'appel a retenu que la dispense de ces obligations prévue par l'article L. 214-18-1 du même code n'était pas applicable aux exploitants de moulins hydrauliques antérieurement soumis à une obligation de mise en conformité en application de l'article L. 232-6 du code rural, devenu l'article L. 432-6 du code de l'environnement, désormais remplacé par les dispositions de l'article L. 214-7 du même code, qui n'auraient pas respecté le délai de cinq ans qui leur avait été octroyé par ces dispositions pour mettre en oeuvre cette obligation. En statuant ainsi, alors que cette circonstance était sans incidence sur l'application des dispositions de l'article L. 214-18-1 du code de l'environnement à la centrale hydroélectrique dite du Val Anglier, la cour a entaché son arrêt d'une erreur de droit.<br/>
<br/>
              6. Il résulte de ce qui précède que la SARL MDC Hydro est fondée à demander l'annulation de l'arrêt de la cour administrative d'appel de Douai qu'elle attaque en tant qu'il rejette le surplus des conclusions de sa requête dirigées contre l'article 4 de l'arrêté du préfet de l'Eure du 4 décembre 2012. <br/>
<br/>
              7. Aux termes du second alinéa de l'article L. 821-2 du code de justice administrative : " Lorsque l'affaire fait l'objet d'un second pourvoi en cassation, le Conseil d'Etat statue définitivement sur cette affaire. " Le Conseil d'Etat étant saisi, en l'espèce, d'un second pourvoi, il y a lieu de régler l'affaire au fond dans la mesure de la cassation prononcée au point précédent.<br/>
<br/>
              8. D'une part, ainsi qu'il a été dit au point 5, la centrale hydroélectrique dite du Val Anglier bénéficiait, du fait du droit d'usage de l'eau accordé par une ordonnance royale du 30 janvier 1839, d'un droit fondé en titre sur l'Andelle qui n'était pas abrogé au 25 février 2017, date de publication au Journal officiel de la loi du 24 février 2017 précitée. Il n'est par ailleurs pas contesté que cette installation constitue un moulin à eau au sens et pour l'application des dispositions de l'article L. 214-18-1 du code de l'environnement cité au point 3. D'autre part, il résulte de ce qui a été dit au point 4 que la seule circonstance que la SARL MDC Hydro n'ait pas mis ses installations en conformité avec les obligations découlant du 2° du I de l'article L. 214-17 du même code ou des dispositions qui étaient antérieurement applicables est sans incidence sur l'application des dispositions de l'article L. 214-18-1 à sa situation. <br/>
<br/>
              9. Par suite, la centrale hydroélectrique dite du Val Anglier doit être regardée comme un moulin à eau existant à la date de la publication de la loi du 24 février 2017 précitée au sens et pour l'application des dispositions de l'article L. 214-18-1 du code de l'environnement de sorte que, depuis cette date, aucune obligation fondée sur les seules dispositions du 2° du I de l'article L. 214-17 du même code ne peut lui être imposée.<br/>
<br/>
              10. Il résulte de tout ce qui précède que la société requérante est fondée à demander l'annulation des mots " constat par le service de police de l'eau du respect des dispositions de l'article L. 432-6 du code de l'environnement et de la mise en conformité des installations à la continuité écologique (circulation piscicole des espèces migratrices et transit sédimentaire) ainsi que " figurant à l'article 4 de l'arrêté du 4 décembre 2012 du préfet de l'Eure. <br/>
<br/>
              11. Dans les circonstances de l'espèce, il y a lieu de mettre à la charge de l'Etat, pour l'ensemble de la procédure, la somme de 3 000 euros à verser à la SARL MDC Hydro au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'arrêt du 28 mai 2019 de la cour administrative d'appel Douai est annulé en tant qu'il rejette le surplus des conclusions de la requête de la SARL MDC Hydro.<br/>
Article 2 : Les mots " constat par le service de police de l'eau du respect des dispositions de l'article L. 432-6 du code de l'environnement et de la mise en conformité des installations à la continuité écologique (circulation piscicole des espèces migratrices et transit sédimentaire) ainsi que " figurant à l'article 4 de l'arrêté du 4 décembre 2012 du préfet de l'Eure sont annulés.<br/>
Article 3 : Le jugement du 25 novembre 2014 du tribunal administratif de Rouen est réformé en ce qu'il a de contraire à la présente décision.<br/>
Article 4 : L'Etat versera à la SARL MDC Hydro une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 5 : La présente décision sera notifiée à la SARL MDC Hydro et à la ministre de la transition écologique.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">27-02-05 EAUX. OUVRAGES. MESURES PRISES POUR ASSURER LE LIBRE ÉCOULEMENT DES EAUX. - MOULIN À EAU - EXONÉRATION DES OBLIGATIONS DESTINÉES À ASSURER LA CONTINUITÉ ÉCOLOGIQUE DU COURS D'EAU (ART. L. 214-18-1 DU CODE DE L'ENVIRONNEMENT) - CHAMP D'APPLICATION - INCLUSION - OUVRAGE NON CONFORME À CES OBLIGATIONS À LA DATE D'ENTRÉE EN VIGUEUR DE L'EXONÉRATION [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">29-02 ENERGIE. ÉNERGIE HYDRAULIQUE. - MOULIN À EAU - EXONÉRATION DES OBLIGATIONS DESTINÉES À ASSURER LA CONTINUITÉ ÉCOLOGIQUE DES COURS D'EAU (ART. L. 214-18-1 DU CODE DE L'ENVIRONNEMENT) - CHAMP D'APPLICATION - INCLUSION - OUVRAGE NON CONFORME À CES OBLIGATIONS À LA DATE D'ENTRÉE EN VIGUEUR DE L'EXONÉRATION [RJ1].
</SCT>
<ANA ID="9A"> 27-02-05 Il résulte de l'article L. 214-18-1 du code de l'environnement, tel qu'éclairé par les travaux préparatoires de la loi n° 2017-227 du 24 février 2017 dont il est issu, qu'afin de préserver le patrimoine hydraulique que constituent les moulins à eau, le législateur a entendu exonérer l'ensemble des ouvrages pouvant recevoir cette qualification et bénéficiant d'un droit de prise d'eau fondé en titre ou d'une autorisation d'exploitation à la date de publication de la loi, des obligations mentionnées au 2° du I de l'article L. 214-17 du même code destinées à assurer la continuité écologique des cours d'eau.... ,,L'article L. 214-18-1 du code de l'environnement ne peut ainsi être interprété comme limitant le bénéfice de cette exonération aux seuls moulins hydrauliques mis en conformité avec ces obligations ou avec les obligations applicables antérieurement ayant le même objet.</ANA>
<ANA ID="9B"> 29-02 Il résulte de l'article L. 214-18-1 du code de l'environnement, tel qu'éclairé par les travaux préparatoires de la loi n° 2017-227 du 24 février 2017 dont il est issu, qu'afin de préserver le patrimoine hydraulique que constituent les moulins à eau, le législateur a entendu exonérer l'ensemble des ouvrages pouvant recevoir cette qualification et bénéficiant d'un droit de prise d'eau fondé en titre ou d'une autorisation d'exploitation à la date de publication de la loi, des obligations mentionnées au 2° du I de l'article L. 214-17 du même code destinées à assurer la continuité écologique des cours d'eau.... ,,L'article L. 214-18-1 du code de l'environnement ne peut ainsi être interprété comme limitant le bénéfice de cette exonération aux seuls moulins hydrauliques mis en conformité avec ces obligations ou avec les obligations applicables antérieurement ayant le même objet.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Comp., avant l'introduction de l'article L. 214-18-1 dans le code de l'environnement par la loi du 24 février 2017, CE, 22 octobre 2018, SARL Saint-Léon, n° 402480, T. p. 697.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
