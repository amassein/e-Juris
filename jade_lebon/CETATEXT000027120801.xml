<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027120801</ID>
<ANCIEN_ID>JG_L_2013_02_000000364751</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/12/08/CETATEXT000027120801.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème / 4ème SSR, 27/02/2013, 364751</TITRE>
<DATE_DEC>2013-02-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>364751</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème / 4ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE ; SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>Mme Leïla Derouich</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Fabienne Lambolez</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2013:364751.20130227</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et les mémoires complémentaires, enregistrés les 21, 24 et 28 décembre 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour la société Promogil, dont le siège est 37, rue de Coulanges à Sucy-en-brie (94370) ; la société Promogil demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'ordonnance n° 1207997 du 21 décembre 2012 par laquelle le juge des référés du tribunal administratif de Lyon, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, a rejeté sa demande tendant à la suspension de l'exécution de l'arrêté du 11 décembre 2012 du préfet du Rhône portant déclaration d'infection tuberculeuse des éléphants du parc zoologique de la Tête d'Or ; <br/>
<br/>
              2°) statuant en référé, de faire droit à sa demande de suspension de l'exécution de cet arrêté ;  <br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement d'une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 25 février 2013, présentée par le ministre de l'agriculture, de l'agroalimentaire et de la forêt ;<br/>
<br/>
              Vu la Constitution, notamment son Préambule et son article 61-1 ; <br/>
<br/>
              Vu l'ordonnance n° 58-1067 du 7 novembre 1958 ; <br/>
<br/>
              Vu le code rural et de la pêche maritime, notamment son article L. 223-8 ; <br/>
<br/>
              Vu la loi n° 79-587 du 11 juillet 1979 ; <br/>
<br/>
              Vu la loi n° 2000-321 du 12 avril 2000 ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Leïla Derouich, Auditeur,  <br/>
<br/>
              - les observations de la SCP Piwnica, Molinié, avocat de la société Promogil, et de la SCP Lyon-Caen-Thiriez, avocat de la commune de Lyon,<br/>
<br/>
              - les conclusions de Mme Fabienne Lambolez, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Piwnica, Molinié, avocat de la société Promogil, et à la SCP Lyon-Caen-Thiriez, avocat de la commune de Lyon ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis au juge des référés que la société Promogil, propriétaire de deux éléphants, a conclu en 1999 avec la commune de Lyon une convention de mise à disposition afin de les confier temporairement au parc animalier de la Tête d'Or ; qu'en 2010, la réalisation de tests pour le dépistage de la tuberculose a fait apparaître une suspicion quant à la contamination de ces animaux par le bacille de la tuberculose ; que le préfet du Rhône a prescrit à la commune de Lyon, par un courrier du 4 janvier 2011, de les tenir éloignés du public et de prendre des mesures de précaution à l'égard des soigneurs du parc zoologique ; qu'à la suite de la découverte de la contamination par le bacille de la tuberculose d'un troisième éléphant, mort en août 2012, qui avait occupé un enclos contigu à celui des deux éléphants de la société Promogil, le préfet a pris le 11 décembre 2012, sur le fondement des dispositions de l'article L. 223-8 du code rural et de la pêche maritime, un arrêté ordonnant, dans l'immédiat, des précautions renforcées pour éviter toute contamination puis, dans un délai d'un mois, l'abattage des deux animaux ; que la société Promogil, qui a saisi le tribunal administratif de Lyon d'un recours pour excès de pouvoir contre cet arrêté, se pourvoit en cassation contre l'ordonnance du 21 décembre 2012 par laquelle le juge des référés du tribunal administratif a rejeté sa demande tendant à ce que l'exécution en soit suspendue jusqu'à ce qu'il ait été statué sur ce recours ; qu'à l'occasion de son pourvoi, elle a présenté un mémoire tendant à ce que le Conseil d'Etat transmette au Conseil constitutionnel la question de la constitutionnalité des dispositions de l'article L. 223-8 du code rural et de la pêche maritime ;<br/>
<br/>
              2. Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision. / Lorsque la suspension est prononcée, il est statué sur la requête en annulation ou en réformation de la décision dans les meilleurs délais. La suspension prend fin au plus tard lorsqu'il est statué sur la requête en annulation ou en réformation de la décision " ; <br/>
<br/>
              3. Considérant que, pour juger qu'en dépit du caractère irréversible que revêtirait l'exécution de l'arrêté attaqué et de l'atteinte qu'elle porterait aux intérêts de la société Promogil, la condition d'urgence prévue par les dispositions précitées n'était pas remplie, le juge des référés du tribunal administratif de Lyon, après avoir relevé qu'il n'est pas possible d'établir de façon certaine que les animaux ne sont pas contaminés, s'est fondé sur l'intérêt s'attachant, du point de vue de la protection de la santé publique, à ce que la mesure reçoive immédiatement exécution ; qu'en se prononçant de la sorte, sans s'interroger sur l'efficacité des mesures d'isolement des animaux et de protection des soigneurs pour assurer la sécurité sanitaire dans l'attente du jugement par le tribunal administratif de la demande tendant à l'annulation de l'arrêté, le juge des référés n'a pas légalement justifié sa décision ; que, par suite, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, cette ordonnance doit être annulée ; <br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de faire application de l'article L. 821-2 du code de justice administrative et de régler l'affaire au titre de la procédure de référé engagée ;<br/>
<br/>
              Sur l'urgence  : <br/>
<br/>
              5. Considérant que l'article 1er de l'arrêté litigieux prévoit que les éléphants sont maintenus isolés tant du public que des animaux appartenant à des espèces sensibles à la tuberculose, dans un périmètre dont l'accès est limité aux soigneurs, que la protection individuelle de ces derniers est assurée, que des pédiluves sont installés à la limite du périmètre et que le matériel utilisé pour les soins donnés aux éléphants est à leur usage exclusif ; que l'article 2 définit les conditions de traitement des fumiers et autres effluents d'élevage provenant des installations où ils sont entretenus ; qu'à supposer que la société Promogil entende obtenir la suspension de l'exécution de ces articles, celle-ci n'est pas justifiée par l'urgence dès lors que les mesures qu'ils prescrivent ne portent pas atteinte aux intérêts de la société et sont nécessaires à la protection de la santé publique ; <br/>
<br/>
              6. Considérant que les articles 3 et 4 de l'arrêté ordonnent l'abattage des animaux dans un délai de trente jours, qui a été porté à soixante-dix jours par un arrêté modificatif du 9 janvier 2013, et en fixent les modalités ; que l'exécution de la mesure d'abattage entraînerait pour la société Promogil des préjudices économiques et moraux ; qu'il ne ressort pas des pièces du dossier que des mesures autres que l'abattage des animaux, et notamment celles prescrites par les articles 1er et 2 de l'arrêté litigieux, ne seraient pas de nature à permettre de prévenir efficacement les risques de contamination, au moins pendant la période nécessaire à l'examen par le tribunal administratif de Lyon de la demande tendant à l'annulation de cet arrêté, examen qui, en cas de suspension de son exécution, devra être conduit dans les meilleurs délais conformément aux dispositions du second alinéa de l'article L. 521-1 du code de justice administrative ; qu'eu égard au caractère irréversible que présenterait l'exécution de l'arrêté litigieux, et alors que la suspension de cette exécution dans l'attente de l'examen de l'affaire au fond n'apparaît pas inconciliable avec la protection de la santé publique, la condition d'urgence posée à l'article L. 521-1 du code de justice administrative doit être regardée comme remplie en ce qui concerne ces deux articles ; <br/>
<br/>
              Sur l'existence de moyens de nature à faire naître un doute sérieux quant à la légalité des articles 3 et 4 de l'arrêté litigieux : <br/>
<br/>
              En ce qui concerne la question prioritaire de constitutionnalité relative à l'article L. 223-8 du code rural et de la pêche maritime : <br/>
<br/>
              7. Considérant qu'aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé, y compris pour la première fois en cassation, à l'occasion d'une instance devant le Conseil d'Etat (...) " ; qu'il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              8. Considérant qu'aux termes de l'article L. 223-8 du code rural et de la pêche maritime : " Après la constatation d'une maladie classée parmi les dangers sanitaires de première catégorie ou parmi les dangers sanitaires de deuxième catégorie faisant l'objet d'une réglementation, le préfet statue sur les mesures à mettre en exécution dans le cas particulier. / Il prend, s'il est nécessaire, un arrêté portant déclaration d'infection remplaçant éventuellement un arrêté de mise sous surveillance. / Cette déclaration peut entraîner, dans le périmètre qu'elle détermine, l'application des mesures suivantes : / (...) 8° L'abattage des animaux malades ou contaminés ou des animaux ayant été exposés à la contagion, ainsi que des animaux suspects d'être infectés ou en lien avec des animaux infectés dans les conditions prévues par l'article L. 223-6 (...) " ; que ces dispositions, applicables au litige, n'ont pas déjà été déclarées conformes à la Constitution ;<br/>
<br/>
              9. Considérant que, pour contester la conformité aux droits et libertés garantis par la Constitution des dispositions de l'article L. 223-8 du code rural et de la pêche maritime, la société Promogil soutient qu'en confiant au préfet le pouvoir d'ordonner l'abattage d'animaux sans organiser une procédure contradictoire préalable et sans fixer les conditions d'indemnisation des propriétaires, le législateur a méconnu l'étendue de sa compétence ; qu'il en résulte, selon elle, une méconnaissance, d'une part, de l'article 16 de la Déclaration des droits de l'homme et du citoyen de 1789, d'autre part,  des articles 2 et 17 de la même Déclaration, dès lors que la loi n'assure pas la protection des droits de la défense et n'entoure pas des garanties nécessaires l'atteinte portée au droit de propriété par le régime administratif qu'elle institue ; <br/>
<br/>
              10. Considérant, en premier lieu, qu'aux termes de l'article 1er de la loi du 11 juillet 1979 relative à la motivation des actes administratifs et à l'amélioration des relations entre l'administration et le public : " Les personnes physiques ou morales ont le droit d'être informées sans délai des motifs des décisions administratives individuelles défavorables qui les concernent. / A cet effet, doivent être motivées les décisions qui :/- restreignent l'exercice des libertés publiques ou, de manière générale, constituent une mesure de police (...) " ; qu'aux termes de l'article 24 de la loi du 12 avril 2000 relative aux droits des citoyens dans leurs relations avec les administrations : " Exception faite des cas où il est statué sur une demande, les décisions individuelles qui doivent être motivées en application des articles 1er et 2 de la loi n° 79-587 du 11 juillet 1979 relative à la motivation des actes administratifs et à l'amélioration des relations entre l'administration et le public n'interviennent qu'après que la personne intéressée a été mise à même de présenter des observations écrites et, le cas échéant, sur sa demande, des observations orales. Cette personne peut se faire assister par un conseil ou représenter par un mandataire de son choix (...) " ; qu'en application de ces dispositions, les décisions, présentant le caractère de mesures de police, qui ordonnent l'abattage d'animaux sur le fondement de l'article L. 223-8 du code rural et de la pêche maritime, doivent être précédées d'une information du propriétaire destinée à lui permettre de présenter des observations sur les mesures que l'administration envisage de prendre ; que, dans ces conditions, le moyen tiré de ce que le législateur aurait omis de prévoir une procédure contradictoire préalable à l'intervention de ces décisions manque en fait ; que si la société reproche également au législateur de ne pas avoir ouvert aux propriétaires des animaux la possibilité de faire procéder à une contre-expertise relative à leur état sanitaire, aucune règle constitutionnelle ne lui imposait de le faire ;<br/>
<br/>
              11. Considérant, en second lieu, qu'aux termes des dispositions de l'article L. 221-2 du code rural et de la pêche maritime : " Des arrêtés conjoints du ministre chargé de l'agriculture et du ministre chargé de l'économie et des finances fixent les conditions d'indemnisation des propriétaires dont les animaux ont été abattus sur l'ordre de l'administration, ainsi que les conditions de la participation financière éventuelle de l'Etat aux autres frais obligatoirement entraînés par l'élimination des animaux. (...) " ; qu'en instituant, par ces dispositions, un régime d'indemnisation des propriétaires dont les animaux sont abattus sur décision de l'administration, le législateur a exercé pleinement la compétence que lui confie l'article 34 de la Constitution ; que la circonstance que le pouvoir réglementaire n'a pas défini les conditions de l'indemnisation pour toutes les catégories d'animaux est sans incidence sur la constitutionnalité des dispositions législatives critiquées ; que l'absence de disposition réglementaire pour une espèce animale ne fait d'ailleurs pas obstacle à ce que le propriétaire obtienne une indemnisation, notamment sur le terrain de la responsabilité sans faute de l'Etat ; <br/>
<br/>
              12. Considérant qu'il résulte de ce qui précède que la question prioritaire de constitutionnalité invoquée, qui n'est pas nouvelle, ne présente pas un caractère sérieux ; qu'il n'y a, par suite, pas lieu de la renvoyer au Conseil constitutionnel ;<br/>
<br/>
              En ce qui concerne les autres moyens invoqués à l'encontre des articles 3 et 4 de l'arrêté litigieux : <br/>
<br/>
              13. Considérant, en premier lieu, que, comme il a été dit ci-dessus au point 10, les mesures prises sur le fondement de l'article L. 223-8 du code rural et de la pêche maritime constituent des mesures de police qui doivent être motivées en vertu de l'article 1er de la loi du 11 juillet 1979 relative à la motivation des actes administratifs et à l'amélioration des relations entre l'administration et le public et qui ne peuvent, par suite, intervenir, eu égard aux dispositions de l'article 24 de la loi du 12 avril 2000 relative aux droits des citoyens dans leurs relations avec les administrations, qu'après que la personne intéressée par la mesure a été mise à même de présenter des observations écrites et, le cas échéant, sur sa demande, des observations orales ; que la société Promogil soutient, sans être sérieusement contredite, que l'adoption de l'arrêté litigieux n'a été précédée d'aucune procédure contradictoire à son égard, alors qu'il n'est pas établi ni même allégué que l'administration, qui était informée de sa qualité de propriétaire, aurait été dans l'impossibilité de joindre la société ou que l'urgence ou des circonstances exceptionnelles auraient fait obstacle à ce qu'une telle procédure soit mise en oeuvre ; que, dans ces conditions, le moyen tiré d'une méconnaissance des dispositions de l'article 24 de la loi du 12 avril 2000 est propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de cet arrêté ; <br/>
<br/>
              14. Considérant, en second lieu, que si, en application de l'article R. 223-4 du code rural et de la pêche maritime, les animaux ayant cohabité avec des animaux atteints de maladie réglementée sont présumés contaminés, le préfet dispose néanmoins, en l'absence de dispositions législatives ou réglementaires contraires, d'un pouvoir d'appréciation quant aux mesures à mettre en oeuvre sur le fondement de l'article L. 223-8 précité du code rural et de la pêche maritime ; qu'eu égard à l'ancienneté et au caractère peu probant des examens réalisés sur les éléphants de la société Promogil, à la possibilité technique d'effectuer des examens permettant d'approfondir les éléments de diagnostic sur l'état sanitaire de ces animaux, notamment sur leur caractère contagieux, à l'efficacité des mesures de protection autres que l'abattage qui ont été prises et peuvent être maintenues ou renforcées et à l'intérêt qui s'attache à préserver, dans une mesure compatible avec la prévention des risques pour la santé publique, l'existence d'animaux rares et protégés, le moyen tiré du caractère disproportionné de la mesure d'abattage des éléphants est, en l'état de l'instruction, propre à créer un doute sérieux quant à la légalité des articles 3 et 4 de l'arrêté ; <br/>
<br/>
              15. Considérant qu'il résulte de ce qui précède qu'il y a lieu de prononcer la suspension de l'exécution des articles 3 et 4 de l'arrêté du 11 décembre 2012 du préfet du Rhône ; <br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative : <br/>
<br/>
              16. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement à la société requérante de la somme de 3 000 euros qu'elle demande au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du tribunal administratif de Lyon du 21 décembre 2012 est annulée. <br/>
Article 2 : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par la société Promogil.<br/>
<br/>
Article 3 : L'exécution des articles 3 et 4 de l'arrêté du 11 décembre 2012 du préfet du Rhône est suspendue.<br/>
<br/>
Article 4 : L'Etat versera à la société Promogil une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
		Article 5 : Le surplus des conclusions de la société Promogil est rejeté. <br/>
Article 6 : La présente décision sera notifiée à la société Promogil, à la commune de Lyon et au ministre de l'agriculture, de l'agroalimentaire et de la forêt. <br/>
		Copie en sera adressée au Conseil constitutionnel et au Premier ministre. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-035-02-04 PROCÉDURE. PROCÉDURES INSTITUÉES PAR LA LOI DU 30 JUIN 2000. RÉFÉRÉ SUSPENSION (ART. L. 521-1 DU CODE DE JUSTICE ADMINISTRATIVE). POUVOIRS ET DEVOIRS DU JUGE. - QPC RELATIVE À LA BASE LÉGALE DE LA DÉCISION DONT LA SUSPENSION EST DEMANDÉE - EXAMEN DE LA QPC - EXAMEN SUBORDONNÉ AU RÈGLEMENT PRÉALABLE DE LA QUESTION DE L'URGENCE - EXISTENCE (SOL. IMPL.).
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-10-05 PROCÉDURE. - RÉFÉRÉ SUSPENSION (ART. L. 521-1 DU CJA) - QPC RELATIVE À LA BASE LÉGALE DE LA DÉCISION DONT LA SUSPENSION EST DEMANDÉE - EXAMEN DE LA QPC - EXAMEN SUBORDONNÉ AU RÈGLEMENT PRÉALABLE DE LA QUESTION DE L'URGENCE - EXISTENCE (SOL. IMPL.).
</SCT>
<ANA ID="9A"> 54-035-02-04 Saisi d'une question prioritaire de constitutionnalité (QPC) portant sur la base légale de la décision dont la suspension est demandée, le juge du référé suspension ne procède à son examen qu'après avoir réglé la question de l'urgence.</ANA>
<ANA ID="9B"> 54-10-05 Saisi d'une question prioritaire de constitutionnalité (QPC) portant sur la base légale de la décision dont la suspension est demandée, le juge du référé suspension (article L. 521-1 du code de justice administrative, CJA) ne procède à son examen qu'après avoir réglé la question de l'urgence.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
