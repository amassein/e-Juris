<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031427862</ID>
<ANCIEN_ID>JG_L_2015_11_000000374895</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/42/78/CETATEXT000031427862.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème / 8ème SSR, 04/11/2015, 374895</TITRE>
<DATE_DEC>2015-11-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>374895</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème / 8ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Christian Fournier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:374895.20151104</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              Vu 1°, sous le n° 374895, la requête, enregistrée le 24 janvier 2014 au secrétariat du contentieux du Conseil d'Etat, présentée par le Syndicat national des agents des phares et balises et sécurité maritime-CGT ; le syndicat demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler la décision implicite de rejet née du silence gardé par le Premier ministre sur sa demande tendant à ce que soit pris l'arrêté interministériel prévu par le décret n° 2013-435 du 27 mai 2013 relatif à l'attribution d'une allocation spécifique de cessation anticipée d'activité à certains fonctionnaires et agents non titulaires relevant du ministère chargé de la mer ;<br/>
<br/>
              2°) d'enjoindre au Premier ministre de prendre les mesures réglementaires qu'implique nécessairement l'application de ce décret dans un délai de 30 jours à compter de la notification de la décision à intervenir, sous astreinte de 500 euros par jour de retard ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu, 2°, sous le n° 385362, la requête, enregistrée le 27 octobre 2014 au secrétariat du contentieux du Conseil d'Etat, présentée par le Syndicat national des agents des phares et balises et sécurité maritime-CGT ; le syndicat demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler la note de gestion du 1er septembre 2014 prise par le ministre de l'écologie, du développement durable et de l'énergie pour l'application du décret n° 2013-435 du 27 mai 2013 relatif à l'attribution d'une allocation spécifique de cessation anticipée d'activité à certains fonctionnaires et agents non titulaires relevant du ministère chargé de la mer ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu la loi n° 2010-1657 du 29 décembre 2010 ; <br/>
<br/>
              Vu la loi n° 2013-1278 du 29 décembre 2013 ; <br/>
<br/>
              Vu le décret n° 2002-634 du 29 avril 2002 ;<br/>
<br/>
              Vu le décret n° 2013-435 du 27 mai 2013, modifié par le décret n° 2015-603 du 3 juin 2015 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Christian Fournier, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que les requêtes du Syndicat national des agents des phares et balises et sécurité maritime-CGT présentent à juger des questions semblables ; qu'il y a lieu de les joindre pour statuer par une seule décision ;<br/>
<br/>
              Sur les conclusions dirigées contre le refus de prendre l'arrêté interministériel prévu par le décret du 27 mai 2013 relatif à l'attribution d'une allocation spécifique de cessation anticipée d'activité à certains fonctionnaires et agents non titulaires relevant du ministère chargé de la mer et tendant à ce que soient prises ces mesures réglementaires ainsi que les mesures d'application de l'article 120 de la loi du 29 décembre 2013 de finances pour 2014 : <br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier que, d'une part, a été publié au Journal Officiel de la République française du 9 août 2014 l'arrêté du 1er août 2014 relatif à la liste des fonctions et des établissements ou parties d'établissements permettant l'attribution d'une allocation spécifique de cessation anticipée d'activité à certains fonctionnaires et agents non titulaires du ministère chargé de la mer, par lequel les ministres concernés ont complété, conformément au décret du 27 mai 2013 relatif à l'attribution d'une allocation spécifique de cessation anticipée d'activité à certains fonctionnaires et agents non titulaires relevant du ministère chargé de la mer, les mesures réglementaires qu'implique nécessairement l'application de l'article 157 de la loi du 29 décembre 2010 de finances pour 2011 ; que, d'autre part, le décret du 27 mai 2013 a été modifié par le décret du 3 juin 2015, publié au Journal Officiel de la République française du 5 juin 2015, pris pour l'application de l'article 120 de la loi de finances pour 2014 ; que, par suite, les conclusions de la requête du Syndicat national des agents de phares et balises de sécurité maritime-CGT tendant à l'annulation du rejet de sa demande tendant à ce que le Gouvernement prenne l'arrêté interministériel prévu par le décret du 27 mai 2013 et à ce que soit enjoint au Premier ministre de prendre ces mesures réglementaires ainsi que les mesures d'application de l'article 120 de la loi du 29 décembre 2013 de finances pour 2014 sont devenues sans objet ; qu'il n'y a, dès lors, pas lieu d'y statuer ;<br/>
<br/>
              Sur les conclusions tendant à annulation de la note de gestion du 1er septembre 2014 du ministre de l'écologie, du développement durable et de l'énergie pour l'application du décret du 27 mai 2013 relatif à l'attribution d'une allocation spécifique de cessation anticipée d'activité à certains fonctionnaires et agents non titulaires relevant du ministère chargé de la mer :<br/>
<br/>
              3. Considérant qu'aux termes de l'article 157 de la loi du 29 décembre 2010 de finances pour 2011 : " Les fonctionnaires et les agents non titulaires exerçant ou ayant exercé certaines fonctions dans des établissements ou parties d'établissement de construction ou de réparation navales du ministère chargé de la mer pendant les périodes au cours desquelles y étaient traités l'amiante ou des matériaux contenant de l'amiante peuvent demander à bénéficier d'une cessation anticipée d'activité et percevoir à ce titre une allocation spécifique. / Cette allocation ne peut se cumuler avec une pension civile de retraite./ La durée de la cessation anticipée d'activité est prise en compte pour la constitution et la liquidation des droits à pension des fonctionnaires qui sont exonérés du versement des retenues pour pension./ Un décret en Conseil d'Etat fixe les conditions d'application du présent article, notamment les conditions d'âge, de cessation d'activité ainsi que les modalités d'affiliation au régime de sécurité sociale et de cessation du régime selon l'âge de l'intéressé et ses droits à pension. " ; qu'aux termes de l'article 2 du décret du 27 mai 2013 : " La rémunération de référence, servant de base à la détermination du montant de l'allocation spécifique, est la moyenne des rémunérations brutes perçues par le fonctionnaire pendant les douze derniers mois de son activité sous réserve qu'elles présentent un caractère régulier et habituel, à l'exclusion de tout élément de rémunération lié à une affectation outre-mer ou à l'étranger et des indemnités ayant le caractère de remboursement de frais. (...) " ; <br/>
<br/>
              4. Considérant que le rachat de jours épargnés sur un compte d'épargne-temps dans la fonction publique constitue l'un des modes règlementaires d'utilisation de ce compte pour le nombre de jours épargnés au-delà de 20 jours comptabilisés ; qu'en excluant de façon générale les indemnités de rachat de jours épargnés sur un compte épargne-temps de la base de rémunération à prendre en compte pour le calcul de l'allocation spécifique de cessation anticipée, au motif qu'elles ne pourraient jamais être regardées comme une rémunération présentant un caractère régulier et habituel, le ministre a méconnu les dispositions citées au point 3 ;<br/>
<br/>
              5. Considérant que le surplus de la note attaquée se borne à commenter et à illustrer les dispositions réglementaires citées au point 3 ; que le moyen tiré de ce que le ministre de l'écologie, du développement durable et de l'énergie aurait pris des mesures réglementaires  ne peut qu'être écarté ;<br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que le Syndicat national des agents des phares et balises et sécurité maritime-CGT est seulement fondé à demander l'annulation pour excès de pouvoir de la note de gestion attaquée en tant qu'elle exclut les indemnités de rachat des jours épargnés sur un compte épargne-temps des éléments de rémunération à prendre en compte pour déterminer le montant de l'allocation spécifique de cessation d'activité ;<br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative : <br/>
<br/>
              7. Considérant qu'il y a lieu, dans les circonstances de l'espèce de mettre à la charge de l'Etat la somme de 6 000 euros à verser au Syndicat national des agents des phares et balises et sécurité maritime-CGT au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Il n'y a pas lieu de statuer sur les conclusions de la requête du Syndicat national des agents de phares et balises de sécurité maritime-CGT tendant, d'une part, à l'annulation du refus de prendre l'arrêté interministériel prévu par le décret du 27 mai 2013 relatif à l'attribution d'une allocation spécifique de cessation anticipée d'activité à certains fonctionnaires et agents non titulaires relevant du ministère chargé de la mer et, d'autre part, à ce que soit enjoint au Premier ministre de prendre ces mesures réglementaires ainsi que les mesures d'application de l'article 120 de la loi du 29 décembre 2013 de finances pour 2014. <br/>
Article 2 : La note de gestion du ministre de l'écologie, du développement durable et de l'énergie pour l'application du décret du 27 mai 2013 est annulée en tant qu'elle exclut les indemnités de rachat des jours épargnés sur un compte épargne-temps des éléments de rémunération à prendre en compte pour déterminer le montant de l'allocation spécifique de cessation d'activité.<br/>
Article 3 : L'Etat versera la somme de 6 000 euros au Syndicat national des agents de phares et balises de sécurité maritime-CGT au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Le surplus des conclusions de la  requête du Syndicat national des agents de phares et balises de sécurité maritime-CGT est rejeté.<br/>
Article 5 : La présente décision sera notifiée au Syndicat national des agents des phares et balises et sécurité maritime-CGT, au Premier ministre, à la ministre de l'écologie, du développement durable et de l'énergie, à la ministre des affaires sociales et de la santé et à la ministre de la réforme de l'Etat, de la décentralisation et de la fonction publique.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-08-03 FONCTIONNAIRES ET AGENTS PUBLICS. RÉMUNÉRATION. INDEMNITÉS ET AVANTAGES DIVERS. - VERSEMENT D'UNE ALLOCATION DE CESSATION ANTICIPÉE D'ACTIVITÉ AUX AGENTS AYANT EXERCÉ CERTAINES FONCTIONS DANS DES ÉTABLISSEMENTS RELEVANT DU MINISTÈRE CHARGÉ DE LA MER PENDANT LES PÉRIODES AU COURS DESQUELLES ÉTAIENT TRAITÉS L'AMIANTE OU DES MATÉRIAUX CONTENANT DE L'AMIANTE (ART. 157 DE LA LOI N° 2010-1657 DU 29 DÉCEMBRE 2010) - RÉMUNÉRATION DE RÉFÉRENCE SERVANT DE BASE À LA FIXATION DU MONTANT DE L'ALLOCATION (DÉCRET N° 2013-435 DU 27 MAI 2013) - NOTION DE RÉMUNÉRATION PRÉSENTANT UN CARACTÈRE RÉGULIER ET HABITUEL - INDEMNITÉS DE RACHAT DE JOURS ÉPARGNÉS SUR UN COMPTE ÉPARGNE-TEMPS.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">61-03 SANTÉ PUBLIQUE. LUTTE CONTRE LES FLÉAUX SOCIAUX. - VERSEMENT D'UNE ALLOCATION DE CESSATION ANTICIPÉE D'ACTIVITÉ AUX AGENTS AYANT EXERCÉ CERTAINES FONCTIONS DANS DES ÉTABLISSEMENTS RELEVANT DU MINISTÈRE CHARGÉ DE LA MER PENDANT LES PÉRIODES AU COURS DESQUELLES ÉTAIENT TRAITÉS L'AMIANTE OU DES MATÉRIAUX CONTENANT DE L'AMIANTE (ART. 157 DE LA LOI N° 2010-1657 DU 29 DÉCEMBRE 2010) - RÉMUNÉRATION DE RÉFÉRENCE SERVANT DE BASE À LA FIXATION DU MONTANT DE L'ALLOCATION (DÉCRET N° 2013-435 DU 27 MAI 2013) - NOTION DE RÉMUNÉRATION PRÉSENTANT UN CARACTÈRE RÉGULIER ET HABITUEL - INDEMNITÉS DE RACHAT DE JOURS ÉPARGNÉS SUR UN COMPTE ÉPARGNE-TEMPS.
</SCT>
<ANA ID="9A"> 36-08-03 Le rachat de jours épargnés sur un compte épargne-temps dans la fonction publique constitue l'un des modes réglementaires d'utilisation de ce compte pour le nombre de jours épargnés au-delà de 20 jours comptabilisés. Les indemnités de rachat de jours épargnés sur un compte épargne-temps ne peuvent donc être exclues de façon générale de la base de rémunération à prendre en compte pour le calcul de l'allocation spécifique de cessation anticipée, au motif qu'elles ne pourraient jamais être regardées comme une rémunération présentant un caractère régulier et habituel.</ANA>
<ANA ID="9B"> 61-03 Le rachat de jours épargnés sur un compte épargne-temps dans la fonction publique constitue l'un des modes réglementaires d'utilisation de ce compte pour le nombre de jours épargnés au-delà de 20 jours comptabilisés. Les indemnités de rachat de jours épargnés sur un compte épargne-temps ne peuvent donc être exclues de façon générale de la base de rémunération à prendre en compte pour le calcul de l'allocation spécifique de cessation anticipée, au motif qu'elles ne pourraient jamais être regardées comme une rémunération présentant un caractère régulier et habituel.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
