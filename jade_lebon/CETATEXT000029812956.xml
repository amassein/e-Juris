<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029812956</ID>
<ANCIEN_ID>JG_L_2014_11_000000365733</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/81/29/CETATEXT000029812956.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère / 6ème SSR, 28/11/2014, 365733</TITRE>
<DATE_DEC>2014-11-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>365733</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère / 6ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ ; SCP TIFFREAU, MARLANGE, DE LA BURGADE</AVOCATS>
<RAPPORTEUR>M. Rémi Decout-Paolini</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:365733.20141128</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire, le mémoire complémentaire et le nouveau mémoire, enregistrés les 4 février, 6 mai et 7 mai 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour le département de Tarn-et-Garonne, représenté par le président du conseil général ; le département demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 11BX01663 du 4 décembre 2012 par lequel la cour administrative d'appel de Bordeaux, à la demande de l'association Accueil familial en Tarn-et-Garonne, a annulé le jugement n° 0603181 du 10 mai 2011 du tribunal administratif de Toulouse rejetant la demande de cette association tendant à l'annulation de la décision du 13 juillet 2006 par laquelle le président du conseil général a refusé de saisir l'assemblée délibérante d'une demande d'abrogation de la délibération du 27 juin 2005 modifiant le règlement départemental d'aide sociale ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de l'association Accueil familial en Tarn-et-Garonne ;<br/>
<br/>
              3°) de mettre à la charge de l'association Accueil familial en Tarn-et-Garonne la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ainsi que la contribution à l'aide juridique mentionnée à l'article R. 761-1 du même code ;<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'action sociale et des familles ;<br/>
<br/>
              Vu la loi n° 91-647 du 10 juillet 1991 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Rémi Decout-Paolini, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat du département de Tarn-et-Garonne ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par une délibération du 27 juin 2005, le conseil général de Tarn-et-Garonne a modifié le règlement départemental d'aide sociale en ce qui concerne l'accueil en famille des personnes âgées, en décidant, d'une part, que l'allocation personnalisée d'autonomie (APA) allouée à un bénéficiaire accueilli en famille couvrirait, au titre des dépenses liées à la dépendance, les seuls " indemnités de sujétions particulières " et " frais spécifiques " et, d'autre part, que les dépenses couvertes par l'aide sociale à l'hébergement des personnes âgées hébergées en famille d'accueil seraient " limitées au coût le plus élevé d'un accueil en structure privée en Tarn-et-Garonne " et seraient constituées de l'" ensemble des frais " portés par les divers éléments du contrat conclu entre la personne accueillie et l'accueillant familial " à l'exception des compléments pour sujétion déjà couverts par l'APA " ; que, par un jugement du 10 mai 2011, le tribunal administratif de Toulouse a rejeté la demande de l'association Accueil familial en Tarn-et-Garonne tendant à l'annulation de la décision du 13 juillet 2006 par laquelle le président du conseil général a refusé de saisir l'assemblée délibérante d'une demande d'abrogation de la délibération du 27 juin 2005 ; que le département de Tarn-et-Garonne se pourvoit en cassation contre l'arrêt du 4 décembre 2012 par lequel la cour administrative d'appel de Bordeaux, faisant droit à la requête de l'association Accueil familial en Tarn-et-Garonne, a annulé le jugement du tribunal administratif de Toulouse du 10 mai 2011 ainsi que la décision du président du conseil général de Tarn-et-Garonne du 13 juillet 2006 et a enjoint à cette autorité de saisir l'assemblée délibérante du département d'une demande d'abrogation de la délibération du 27 juin 2005 ;<br/>
<br/>
              Sur les conclusions tendant à l'annulation de l'arrêt attaqué :<br/>
<br/>
              En ce qui concerne l'allocation personnalisée d'autonomie :<br/>
<br/>
              2. Considérant, d'une part, qu'aux termes de l'article L. 121-3 du code de l'action sociale et des familles : " Dans les conditions définies par la législation et la réglementation sociales, le conseil général adopte un règlement départemental d'aide sociale définissant les règles selon lesquelles sont accordées les prestations sociales relevant du département " ; qu'en vertu des articles L. 232-2 et suivants du même code, l'allocation personnalisée d'autonomie est au nombre des prestations légales d'aide sociale à la charge du département mentionnées à l'article L. 121-1 de ce code ; qu'aux termes du premier alinéa de l'article L. 121-4 du même code : " Le conseil général peut décider de conditions et de montants plus favorables que ceux prévus par les lois et règlements applicables aux prestations mentionnées à l'article L. 121-1. Le département assure la charge financière de ces décisions " ;<br/>
<br/>
              3. Considérant, d'autre part, que l'article L. 442-1 du code de l'action sociale et des familles dispose que les personnes accueillies à titre onéreux au domicile d'un accueillant familial concluent avec celui-ci un contrat précisant la nature ainsi que les conditions matérielles et financières de l'accueil et prévoyant notamment " 1° Une rémunération journalière des services rendus ainsi qu'une indemnité de congé (...) ; / 2° Le cas échéant, une indemnité en cas de sujétions particulières ; / 3° Une indemnité représentative des frais d'entretien courant de la personne accueillie ; / 4° Une indemnité représentative de mise à disposition de la ou des pièces réservées à la personne accueillie " ; que le premier alinéa de l'article L. 232-3 du même code dispose que : " Lorsque l'allocation personnalisée d'autonomie est accordée à une personne résidant à domicile, elle est affectée à la couverture des dépenses de toute nature relevant d'un plan d'aide élaboré par une équipe médico-sociale " ; que, pour l'application de cet article, sont considérées comme résidant à domicile, en vertu de l'article L. 232-5 du même code, les personnes accueillies au domicile d'un accueillant familial ; qu'aux termes de l'article R. 232-8 du même code : " L'allocation personnalisée d'autonomie est affectée à la couverture des dépenses de toute nature figurant dans le plan d'aide élaboré par l'équipe médico-sociale mentionnée à l'article L. 232-3. / Ces dépenses s'entendent notamment de la rémunération de l'intervenant à domicile, du règlement des frais d'accueil temporaire, avec ou sans hébergement, dans des établissements ou services autorisés à cet effet, du règlement des services rendus par les accueillants familiaux mentionnés à l'article L. 441-1 ainsi que des dépenses de transport, d'aides techniques, d'adaptation du logement et de toute autre dépense concourant à l'autonomie du bénéficiaire " ;<br/>
<br/>
              4. Considérant qu'il résulte de l'ensemble de ces dispositions que l'aide personnalisée d'autonomie à laquelle peut prétendre une personne accueillie au domicile d'un accueillant familial doit notamment couvrir, en application de l'article R. 232-8 du code de l'action sociale et des familles, les services rendus par les accueillants familiaux ; que, par suite, en jugeant que la délibération litigieuse du 27 juin 2005, limitant aux seuls " indemnités de sujétions particulières " et " frais spécifiques " la prise en charge par l'allocation personnalisée d'autonomie des dépenses liées à la dépendance, instaurait, en violation des dispositions de l'article L. 121-4 du même code, un régime moins favorable que celui prévu par l'article R. 232-8 et méconnaissait, par suite, les dispositions de cet article, la cour administrative d'appel de Bordeaux n'a entaché son arrêt ni de dénaturation ni d'erreur de droit ;<br/>
<br/>
              En ce qui concerne l'aide sociale à l'hébergement :<br/>
<br/>
              5. Considérant qu'en vertu des articles L. 231-1 et suivants du code de l'action sociale et des familles, l'aide sociale à l'hébergement est au nombre des prestations légales d'aide sociale à la charge du département mentionnées à l'article L. 121-1 de ce code ; qu'aux termes de l'article L. 231-4 du même code : " Toute personne âgée qui ne peut être utilement aidée à domicile peut être placée, si elle y consent, dans des conditions précisées par décret, soit chez des particuliers, soit dans un établissement de santé ou une maison de retraite publics, ou, à défaut, dans un établissement privé. / En cas de placement dans un établissement public ou un établissement privé, habilité par convention à recevoir des bénéficiaires de l'aide sociale, le plafond des ressources précisé à l'article L. 231-2 sera celui correspondant au montant de la dépense résultant dudit placement. Le prix de la journée dans ces établissements est fixé selon la réglementation en vigueur dans les établissements de santé " ; qu'aux termes de l'article R. 231-4 du même code, dans sa version alors applicable : " Le placement à titre onéreux chez un particulier au titre de l'aide sociale donne lieu à une prise en charge déterminée par la commission d'admission à l'aide sociale, compte tenu : / 1° D'un plafond constitué par la rémunération et les indemnités mentionnées aux 1° et 2° de l'article L. 442-1, le cas échéant selon la convention accompagnant l'habilitation à recevoir des bénéficiaires de l'aide sociale ; / 2° Des ressources de la personne accueillie, y compris celles résultant de l'obligation alimentaire (...) " ;<br/>
<br/>
              6. Considérant qu'il résulte de ces dispositions que, lorsqu'une personne âgée bénéficie d'un placement à titre onéreux chez un accueillant familial, la prise en charge à laquelle elle peut prétendre au titre de l'aide sociale à l'hébergement doit être déterminée en considération d'un plafond constitué par la rémunération des services rendus par l'accueillant, les indemnités de congé auquel ce dernier a droit et une indemnité éventuelle pour sujétions particulières, telles que prévues par le contrat conclu entre la personne âgée et l'accueillant en application de l'article L. 442-1 du code de l'action sociale et des familles, dans le respect, le cas échéant, du montant maximal de prise en charge préalablement fixé dans la convention conclue entre le département et l'accueillant familial et accompagnant l'habilitation de ce dernier, prévue à l'article L. 441-1 du même code, à recevoir des bénéficiaires de l'aide sociale ; que, par suite, en jugeant, par une motivation suffisante, que le conseil général de Tarn-et-Garonne ne pouvait, sans méconnaître l'article R. 231-4 de ce code, décider par la délibération litigieuse du 27 juin 2005 que la prise en charge par l'aide sociale des personnes âgées bénéficiant d'un accueil familial serait plafonnée au coût le plus élevé d'un accueil en structure privée dans le département, la cour administrative d'appel de Bordeaux n'a pas dénaturé les pièces du dossier ni commis d'erreur de droit ;<br/>
<br/>
              En ce qui concerne l'injonction adressée au président du conseil général :<br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède qu'en jugeant que son arrêt impliquait nécessairement que le président du conseil général de Tarn-et-Garonne saisisse l'assemblée délibérante d'une demande d'abrogation de la délibération du 27 juin 2005 et qu'il y avait lieu d'adresser à cette fin, sur le fondement de l'article L. 911-1 du code de justice administrative, une injonction au président du conseil général, la cour administrative d'appel de Bordeaux n'a pas commis d'erreur de droit ;<br/>
<br/>
              8. Considérant qu'il résulte de tout ce qui précède que le département de Tarn-et-Garonne n'est pas fondé à demander l'annulation de l'arrêt qu'il attaque ;<br/>
<br/>
              Sur les dépens :<br/>
<br/>
              9. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de laisser la contribution pour l'aide juridique à la charge du département de Tarn-et-Garonne ;<br/>
<br/>
              Sur les conclusions présentées au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 :<br/>
<br/>
              10. Considérant que ces dispositions font obstacle à ce qu'une somme soit mise à la charge de l'association Accueil familial en Tarn-et-Garonne, qui n'est pas, dans la présente instance, la partie perdante ; que cette association a obtenu le bénéfice de l'aide juridictionnelle ; que, par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ; qu'il y a lieu, dans les circonstances de l'espèce, et sous réserve que la SCP Tiffreau, Marlange, de la Burgade, avocat de l'association, renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge du département de Tarn-et-Garonne le versement à cette société de la somme de 3 000 euros ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi du département de Tarn-et-Garonne est rejeté.<br/>
Article 2 : La contribution pour l'aide juridique est laissée à la charge du département de Tarn-et-Garonne.<br/>
Article 3 : Le département de Tarn-et-Garonne versera à la SCP Tiffreau, Marlange, de la Burgade, avocat de l'association Accueil familial en Tarn-et-Garonne, la somme de 3 000 euros en application des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
Article 4 : La présente décision sera notifiée au département de Tarn-et-Garonne et à l'association Accueil familial en Tarn-et-Garonne.<br/>
Copie en sera adressée pour information à la ministre des affaires sociales, de la santé et des droits des femmes.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">04-02-03 AIDE SOCIALE. DIFFÉRENTES FORMES D'AIDE SOCIALE. AIDE SOCIALE AUX PERSONNES ÂGÉES. - AIDE SOCIALE À L'HÉBERGEMENT (ASH) - PERSONNE ACCUEILLIE AU DOMICILE D'UN ACCUEILLANT FAMILIAL - 1) RÈGLES DE DÉTERMINATION DU PLAFOND DE L'AIDE - ELÉMENTS PRIS EN COMPTE - RÉMUNÉRATION DES SERVICES RENDUS PAR L'ACCUEILLANT, INDEMNITÉS DE CONGÉ ET INDEMNITÉ POUR SUJÉTIONS PARTICULIÈRES, TELLES QUE PRÉVUES PAR LE CONTRAT ENTRE LA PERSONNE ÂGÉE ET L'ACCUEILLANT - LIMITE - MONTANT MAXIMAL FIXÉ PAR LA CONVENTION ENTRE LE DÉPARTEMENT ET L'ACCUEILLANT - 2) LÉGALITÉ D'UN RÈGLEMENT DÉPARTEMENTAL PLAFONNANT L'ASH AU COÛT LE PLUS ÉLEVÉ D'UN ACCUEIL EN STRUCTURE PRIVÉE DANS LE DÉPARTEMENT - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">04-02-03-03 AIDE SOCIALE. DIFFÉRENTES FORMES D'AIDE SOCIALE. AIDE SOCIALE AUX PERSONNES ÂGÉES. ALLOCATION PERSONNALISÉE D'AUTONOMIE. - PERSONNE ACCUEILLIE AU DOMICILE D'UN ACCUEILLANT FAMILIAL - PRISE EN CHARGE DES DÉPENSES LIÉES À LA DÉPENDANCE - POSSIBILITÉ POUR UN DÉPARTEMENT DE LIMITER LES DÉPENSES COUVERTES PAR L'APA AUX SEULS  FRAIS SPÉCIFIQUES  ET  INDEMNITÉS DE SUJÉTIONS PARTICULIÈRES  - ABSENCE.
</SCT>
<ANA ID="9A"> 04-02-03 1) Il résulte des dispositions des articles L. 231-4 et R. 231-4 du code de l'action sociale et des familles que, lorsqu'une personne âgée bénéficie d'un placement à titre onéreux chez un accueillant familial, la prise en charge à laquelle elle peut prétendre au titre de l'aide sociale à l'hébergement (ASH) doit être déterminée en considération d'un plafond constitué par la rémunération des services rendus par l'accueillant, par les indemnités de congé auquel ce dernier a droit et par une indemnité éventuelle pour sujétions particulières, telles que prévues par le contrat conclu entre la personne âgée et l'accueillant en application de l'article L. 442-1 du code, dans le respect, le cas échéant, du montant maximal de prise en charge préalablement fixé dans la convention conclue entre le département et l'accueillant familial et accompagnant l'habilitation de ce dernier, prévue à l'article L. 441-1 du même code, à recevoir des bénéficiaires de l'aide sociale.... ,,2) Par suite, un règlement départemental d'aide sociale ne peut légalement prévoir que la prise en charge par l'aide sociale des personnes âgées bénéficiant d'un accueil familial serait plafonnée au coût le plus élevé d'un accueil en structure privée dans le département.</ANA>
<ANA ID="9B"> 04-02-03-03 Il résulte des dispositions des articles L. 232-3, L. 232-5, L. 442-1 et R. 232-8 du code de l'action sociale et des familles que l'aide personnalisée d'autonomie (APA) à laquelle peut prétendre une personne accueillie au domicile d'un accueillant familial doit notamment couvrir les services rendus par les accueillants familiaux. Par suite, un règlement départemental d'aide sociale, qui ne peut comporter pour les prestations légales d'aide sociale que des dispositions plus favorables  à celles qui résultent des lois et règlements applicables, ne peut légalement limiter aux seuls  frais spécifiques  et  indemnités de sujétions particulières  la prise en charge, par l'APA allouée à un bénéficiaire accueilli en famille, des dépenses liées à la dépendance.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
