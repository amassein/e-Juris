<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043741054</ID>
<ANCIEN_ID>JG_L_2021_05_000000433660</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/74/10/CETATEXT000043741054.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 1ère chambres réunies, 27/05/2021, 433660</TITRE>
<DATE_DEC>2021-05-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>433660</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, TEXIDOR, PERIER ; SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>Mme Dorothée Pradines</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:433660.20210527</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. B... A... a demandé au tribunal administratif de Paris d'annuler pour excès de pouvoir la décision des directeurs de l'Institut national de la santé et de la recherche médicale (Inserm) et de l'Institut thématique multi-organisme (ITMO) cancer de l'Alliance nationale pour les sciences de la vie et de la santé du 22 octobre 2015 procédant au retrait de la décision du 6 juin 2015 lui accordant un financement dans le cadre du troisième plan cancer  en vue d'un projet de recherche post-doctoral, ainsi que la décision implicite rejetant son recours gracieux formé contre cette décision. Par un jugement n° 1606041 du 28 mars 2018, le tribunal administratif a annulé ces décisions et enjoint à l'Inserm de verser à M. A... la somme de 68 665 euros correspondant au montant du financement maximal demandé dans le cadre du troisième plan cancer.<br/>
<br/>
              Par un arrêt n°s 18PA01807, 18PA02506 du 25 juin 2019, la cour administrative d'appel de Paris a, sur appel de l'Inserm, annulé ce jugement et rejeté la demande de M. A... devant le tribunal administratif.  <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 19 août et 19 novembre 2019 et le 1er février 2021 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de l'Inserm ;<br/>
<br/>
              3°) de mettre à la charge de l'Inserm et de l'ITMO Cancer la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi n° 79-587 du 11 juillet 1979 ;<br/>
              - la loi n° 2000-321 du 12 avril 2000 ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Dorothée Pradines, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Célice, Texidor, Perier, avocat de M. A... et à la SCP Waquet, Farge, Hazan, avocat de l'Institut national de la santé et de la recherche médicale ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond qu'à la suite de l'appel à candidatures " soutien pour la formation à la recherche translationnelle en cancérologie - édition 2015 " dans le cadre du troisième plan cancer, M. B... A..., alors chef de clinique au sein du service d'hématologie des Hospices civils de Lyon, a sollicité un financement d'un montant de 68 665 euros à ce titre auprès de l'Institut thématique multi-organismes (ITMO) Cancer et de l'Institut national de la santé et de la recherche médicale (Inserm) afin de réaliser un projet de recherche post-doctoral. Par un courriel du 21 mai 2015, l'Inserm a informé M. A... que sa candidature avait été sélectionnée pour obtenir le financement demandé et l'a invité à confirmer son acceptation du soutien financier proposé, ce que l'intéressé a fait par un courriel à l'Inserm en date du même jour. Dans ce même courriel, M. A... a porté également à la connaissance de l'Inserm qu'il avait indiqué, lors de son audition par le comité d'évaluation scientifique de l'appel à candidatures, qu'il était également lauréat de la bourse Marie Sklodowska-Curie, financée par l'Union européenne. Par un courrier du 6 juin 2015, la directrice du département évaluation et suivi des programmes de l'Inserm et le directeur de l'ITMO Cancer ont notifié à M. A... l'octroi du financement demandé au titre du 3ème plan cancer. Toutefois, par un courriel du 18 juin 2015, l'Inserm a informé M. A... que le cumul de l'allocation que cet établissement lui avait attribuée au titre du plan cancer et de la bourse Marie Sklodowska-Curie n'était pas possible, et a incité l'intéressé à conserver le bénéfice de cette bourse en renonçant au financement obtenu dans le cadre du 3ème plan cancer. Par un courrier du 22 octobre 2015, l'Inserm et l'ITMO Cancer ont finalement fait savoir à M. A... que l'Inserm ne pouvait donner suite à la sélection de son projet ni au contrat correspondant à l'action du 3ème plan cancer, au motif que le bénéfice de la bourse Marie Sklodowska-Curie ne pouvait être cumulé avec un autre financement s'adressant au même bénéficiaire. M. A... a formé contre la décision du 22 octobre 2015 un recours gracieux, qui a fait l'objet d'une décision implicite de rejet. L'intéressé a contesté ces deux décisions devant le tribunal administratif de Paris, qui les a annulées par un jugement du 28 mars 2018. Sur appel de l'Inserm, la cour administrative d'appel de Paris a, par un arrêt du 25 juin 2019 contre lequel M. A... se pourvoit en cassation, annulé ce jugement et rejeté la demande de l'intéressé devant le tribunal administratif.<br/>
<br/>
              2. Sous réserve de dispositions législatives ou réglementaires contraires, et hors les hypothèses d'inexistence de la décision en question, de son obtention par fraude, ou de demande de son bénéficiaire, l'administration ne peut retirer une décision individuelle créatrice de droits, si elle est illégale, que dans le délai de quatre mois suivant la prise de cette décision. Une décision qui a pour objet l'attribution d'une subvention constitue un acte unilatéral qui crée des droits au profit de son bénéficiaire. Toutefois, de tels droits ne sont ainsi créés que dans la mesure où le bénéficiaire de la subvention respecte les conditions mises à son octroi, que ces conditions découlent des normes qui la régissent, qu'elles aient été fixées par la personne publique dans sa décision d'octroi, qu'elles aient fait l'objet d'une convention signée avec le bénéficiaire, ou encore qu'elles découlent implicitement mais nécessairement de l'objet même de la subvention. Il en résulte que les conditions mises à l'octroi d'une subvention sont fixées par la personne publique au plus tard à la date à laquelle cette subvention est octroyée.<br/>
<br/>
              3. Il ressort des pièces du dossier soumis aux juges du fond que par décision du 6 juin 2015, l'Institut national de la santé et de la recherche médicale (Inserm) a accordé à M. A... le financement qu'il avait sollicité au titre du 3ème plan cancer pour un projet post-doctoral, sans subordonner cette aide au respect d'une condition relative à l'impossibilité de cumuler plusieurs allocations ou bourses, quelles qu'elles soient. Il résulte de ce qui a été dit au point 2 que la cour administrative d'appel de Paris a, en conséquence, commis une erreur de droit en se fondant sur un courriel du 18 juin 2015 de l'Inserm informant M. A... de l'impossibilité de cumuler l'allocation que cet établissement lui avait attribuée au titre du plan cancer avec la bourse Marie Sklodowska-Curie, que l'intéressé avait par ailleurs obtenue pour son projet, dès lors que cette condition n'avait pas été posée lors de l'attribution de la subvention et alors qu'il n'était pas même soutenu devant les juges du fond qu'elle découlerait implicitement mais nécessairement de son objet. La cour ne pouvait pas plus se fonder, sans commettre une autre erreur de droit, sur ce que l'organisme gestionnaire de la bourse Marie Sklodowska-Curie, dont avait été attributaire M. A... pour son projet, aurait informé l'Inserm que le chercheur attributaire de cette bourse devait se consacrer à temps plein au projet soutenu et ne pouvait recevoir aucun autre revenu à ce titre, dès lors, ainsi qu'il vient d'être dit, qu'une telle règle ne figurait pas comme condition légale dans la décision d'attribution de la subvention accordée par l'Inserm ni ne découlerait de son objet même.<br/>
<br/>
              4. Il résulte de tout ce qui précède que l'arrêt contesté de la cour administrative d'appel de Paris doit être annulé, sans qu'il y ait lieu de se prononcer sur les autres moyens du pourvoi.<br/>
<br/>
              Sur les frais de l'instance :<br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Inserm une somme de 3 000 euros à verser à M. A..., au titre des dispositions de l'article L. 761-1 du code de justice administrative. Ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de M. A... qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Paris du 25 juin 2019 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Paris.<br/>
Article 3 : L'Inserm versera à M. A... une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions de l'Inserm présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à M. B... A... et à l'Institut national de la santé et de la recherche médicale.<br/>
Copie en sera adressée à la ministre de l'enseignement supérieur, de la recherche et de l'innovation et à l'Institut thématique multi-organisme cancer de l'Alliance nationale pour les sciences de la vie et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-01-06-02-01 ACTES LÉGISLATIFS ET ADMINISTRATIFS. DIFFÉRENTES CATÉGORIES D'ACTES. ACTES ADMINISTRATIFS - CLASSIFICATION. ACTES INDIVIDUELS OU COLLECTIFS. ACTES CRÉATEURS DE DROITS. - ATTRIBUTION D'UNE SUBVENTION PAR UNE PERSONNE PUBLIQUE - EXISTENCE [RJ1], DANS LA MESURE OÙ LE BÉNÉFICIAIRE RESPECTE LES CONDITIONS MISES À SON OCTROI [RJ2], LESQUELLES DOIVENT ÊTRE FIXÉES AU PLUS TARD À LA DATE À LAQUELLE CETTE SUBVENTION EST OCTROYÉE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">14-03-02 COMMERCE, INDUSTRIE, INTERVENTION ÉCONOMIQUE DE LA PUISSANCE PUBLIQUE. MESURES D'INCITATION. SUBVENTIONS. - ATTRIBUTION D'UNE SUBVENTION PAR UNE PERSONNE PUBLIQUE - EXISTENCE [RJ1], DANS LA MESURE OÙ LE BÉNÉFICIAIRE RESPECTE LES CONDITIONS MISES À SON OCTROI [RJ2], LESQUELLES DOIVENT ÊTRE FIXÉES AU PLUS TARD À LA DATE À LAQUELLE CETTE SUBVENTION EST OCTROYÉE.
</SCT>
<ANA ID="9A"> 01-01-06-02-01 Une décision qui a pour objet l'attribution d'une subvention constitue un acte unilatéral qui crée des droits au profit de son bénéficiaire.... ,,Toutefois, de tels droits ne sont ainsi créés que dans la mesure où le bénéficiaire de la subvention respecte les conditions mises à son octroi, que ces conditions découlent des normes qui la régissent, qu'elles aient été fixées par la personne publique dans sa décision d'octroi, qu'elles aient fait l'objet d'une convention signée avec le bénéficiaire, ou encore qu'elles découlent implicitement mais nécessairement de l'objet même de la subvention.,,,Il en résulte que les conditions mises à l'octroi d'une subvention sont fixées par la personne publique au plus tard à la date à laquelle cette subvention est octroyée.</ANA>
<ANA ID="9B"> 14-03-02 Une décision qui a pour objet l'attribution d'une subvention constitue un acte unilatéral qui crée des droits au profit de son bénéficiaire.... ,,Toutefois, de tels droits ne sont ainsi créés que dans la mesure où le bénéficiaire de la subvention respecte les conditions mises à son octroi, que ces conditions découlent des normes qui la régissent, qu'elles aient été fixées par la personne publique dans sa décision d'octroi, qu'elles aient fait l'objet d'une convention signée avec le bénéficiaire, ou encore qu'elles découlent implicitement mais nécessairement de l'objet même de la subvention.,,,Il en résulte que les conditions mises à l'octroi d'une subvention sont fixées par la personne publique au plus tard à la date à laquelle cette subvention est octroyée.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., sur le caractère unilatéral d'une décision d'octroi d'une subvention, qu'elle qu'en soit la forme, CE, 29 mai 2019, Société Royal cinéma - M. Romanello, n° 428040, p. 172,,[RJ2] Cf. E, 5 juillet 2010, Chambre de commerce et d'industrie de l'Indre, n° 308615, p. 238.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
