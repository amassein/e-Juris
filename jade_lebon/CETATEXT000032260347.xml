<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032260347</ID>
<ANCIEN_ID>JG_L_2016_03_000000389521</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/26/03/CETATEXT000032260347.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème / 3ème SSR, 16/03/2016, 389521</TITRE>
<DATE_DEC>2016-03-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>389521</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème / 3ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DIDIER, PINET</AVOCATS>
<RAPPORTEUR>Mme Emmanuelle Petitdemange</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2016:389521.20160316</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A...B...a demandé au tribunal administratif d'Orléans de prononcer la décharge de la cotisation de taxe d'habitation sur les logements vacants à laquelle elle a été assujettie au titre de l'année 2013. Par une ordonnance n° 1403400 du 9 décembre 2014, le président de la 3ème chambre du tribunal administratif d'Orléans a rejeté cette demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 16 avril et 17 juillet 2015 au secrétariat du contentieux du Conseil d'Etat, Mme B...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code civil, notamment son article 1316-4 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Emmanuelle Petitdemange, auditeur,  <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Didier, Pinet, avocat de Mme B...;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article R. 222-1 du code de justice administrative, dans sa rédaction applicable au litige : " Les présidents de tribunal administratif et de cour administrative d'appel, le vice-président du tribunal administratif de Paris et les présidents de formation de jugement des tribunaux et des cours peuvent, par ordonnance : / (...) 4° Rejeter les requêtes manifestement irrecevables, lorsque la juridiction n'est pas tenue d'inviter leur auteur à les régulariser ou qu'elles n'ont pas été régularisées à l'expiration du délai imparti par une demande en ce sens " ; qu'aux termes de l'article R. 431-8 du même code, dans sa rédaction applicable au litige : " Les parties non représentées devant un tribunal administratif qui ont leur résidence hors du territoire de la République doivent faire élection de domicile dans le ressort de ce tribunal " ; qu'aux termes de l'article R. 612-1 du même code : " Lorsque des conclusions sont entachées d'une irrecevabilité susceptible d'être couverte après l'expiration du délai de recours, la juridiction ne peut les rejeter en relevant d'office cette irrecevabilité qu'après avoir invité leur auteur à les régulariser. (...) La demande de régularisation mentionne que, à défaut de régularisation, les conclusions pourront être rejetées comme irrecevables dès l'expiration du délai imparti qui, sauf urgence, ne peut être inférieur à quinze jours. La demande de régularisation tient lieu de l'information prévue à l'article R. 611-7 " ; qu'en conséquence, une requête ne peut être rejetée comme manifestement irrecevable sur le fondement de l'article R. 222-1 du code de justice administrative au motif que le requérant a méconnu l'article R. 431-8 du même code en n'élisant pas domicile dans le ressort de la juridiction qu'il a saisie sans qu'il ait été au préalable invité à régulariser sa requête, conformément à l'article R. 612-1  ; que s'il procède à cette régularisation par courrier électronique sans utiliser l'application Télérecours ou sans apposer sa signature électronique, au sens de l'article 1316-4 du code civil,  le greffe de la juridiction est tenu de lui demander, sur le fondement de ce même article R. 612-1, de lui adresser un courrier postal portant sa signature et reprenant les éléments de son courrier électronique ; <br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis au juge du fond que Mme B...a, par une mémoire enregistré le 6 septembre 2014 au greffe du tribunal administratif d'Orléans, demandé la décharge de la cotisation de taxe d'habitation sur les logements vacants à laquelle elle a été assujettie au titre de l'année 2013 à raison d'un bien immobilier situé en Indre-et-Loire ; que, par lettre du 23 septembre 2014, le greffier en chef du tribunal administratif d'Orléans a invité la requérante à faire élection de domicile dans le ressort de ce tribunal en application de l'article R. 431-8 du code de justice administrative, dans un délai d'un mois suivant la réception de ce courrier ; que Mme B...a fait élection de domicile à une adresse située dans le ressort du tribunal administratif, par courrier électronique sans utiliser l'application Télérecours et sans apposer de signature électronique ; que ce courrier est parvenu au greffe du tribunal le 15 octobre 2014, qui en a accusé réception par la même voie le lendemain ; que, par ordonnance du 9 décembre 2014, le président de la 3ème chambre du tribunal administratif d'Orléans a rejeté sa demande comme manifestement irrecevable sur le fondement de l'article R. 222-1 du code de justice administrative au motif que Mme B...n'avait pas fait élection de domicile dans le ressort du tribunal administratif d'Orléans dans le délai d'un mois qui lui était imparti ; qu'en statuant ainsi, alors qu'il lui appartenait d'inviter, au préalable, la requérante à compléter son courrier électronique du 15 octobre 2014 par un courrier postal signé de sa main et confirmant l'adresse indiquée, le président de la 3ème chambre du tribunal administratif d'Orléans a commis une erreur de droit ; que, par suite, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, son ordonnance doit être annulée ;<br/>
<br/>
               3. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à MmeB..., au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>              D E C I D E :<br/>
                             --------------<br/>
<br/>
Article 1er : L'ordonnance du président de la 3ème chambre du tribunal administratif d'Orléans du 9 décembre 2014 est annulée. <br/>
<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif d'Orléans. <br/>
<br/>
Article 3 : L'Etat versera une somme de 3 000 euros à Mme B...au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à Mme A...B...et au ministre des finances et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-07-01-03-02 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. QUESTIONS GÉNÉRALES. CONCLUSIONS. CONCLUSIONS IRRECEVABLES. - OBLIGATION D'INVITER À RÉGULARISER (ART. R. 612-1 DU CJA) - PORTÉE - RÉGULARISATION EFFECTUÉE PAR COURRIER ÉLECTRONIQUE SANS SIGNATURE ÉLECTRONIQUE OU SANS UTILISER TÉLÉRECOURS - OBLIGATION DE DEMANDER UN COURRIER POSTAL SIGNÉ - EXISTENCE.
</SCT>
<ANA ID="9A"> 54-07-01-03-02 Lorsque la juridiction invite le requérant à régulariser sa requête en application de l'article R. 612-1 du code de justice administrative (CJA) et que celui-ci procède à cette régularisation par courrier électronique sans utiliser l'application Télérecours (art. R. 414-1 du CJA) ou sans apposer sa signature électronique, au sens de l'article 1316-4 du code civil, le greffe de la juridiction est tenu de lui demander, sur le fondement de ce même article R. 612-1, de lui adresser un courrier postal portant sa signature et reprenant les éléments de son courrier électronique.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
