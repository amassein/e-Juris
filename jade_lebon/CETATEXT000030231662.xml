<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030231662</ID>
<ANCIEN_ID>JG_L_2015_02_000000361433</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/23/16/CETATEXT000030231662.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème / 1ère SSR, 11/02/2015, 361433</TITRE>
<DATE_DEC>2015-02-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>361433</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème / 1ère SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN ; FOUSSARD</AVOCATS>
<RAPPORTEUR>M. Jean-Baptiste de Froment</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:361433.20150211</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 27 juillet et 26 octobre 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour la SCI Naq Gamma, dont le siège est route de Quimpérès à Tréflez (29430) ; la SCI Naq Gamma demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'arrêt n° 10NT01536 du 8 juin 2012 par lequel la cour administrative d'appel de Nantes a rejeté sa requête tendant à l'annulation, d'une part, du jugement n° 05 4622/05-2130/08-1017 du 12 mai 2010 du tribunal administratif de Rennes, en tant que ce dernier a rejeté sa demande tendant à l'annulation de la décision du 21 décembre 2007 par laquelle le maire de Tréflez (Finistère) a refusé de lui délivrer un permis de construire en vue de l'édification d'une maison d'habitation sur la parcelle cadastrée à la section AD, sous le n° 385, située au lieu-dit "Keremma", d'autre part, de cette décision ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
              3°) de mettre à la charge de la commune de Tréflez la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'urbanisme ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Baptiste de Froment, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la SCI Naq Gamma et à Me Foussard, avocat de la commune de Tréflez ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 20 mars 2006, le maire de Tréflez (Finistère) a opposé un sursis à statuer à la demande de permis de construire présentée par la SCI Naq Gamma en vue de l'édification d'une maison d'habitation sur une parcelle située sur le territoire de la commune, au motif que le projet de zonage du nouveau plan local d'urbanisme en cours d'élaboration classait la parcelle concernée en zone naturelle ; que, par un arrêté du 21 décembre 2007, le maire a refusé le permis de construire sollicité ; que, par un jugement du 12 mai 2010, le tribunal administratif de Rennes a notamment rejeté la demande de la SCI tendant à l'annulation de l'arrêté du 21 décembre 2007 ; que la SCI Naq Gamma se pourvoit en cassation contre l'arrêt du 8 juin 2012 par lequel la cour administrative de Nantes a rejeté son appel contre le jugement du 12 mai 2010 en tant qu'il a rejeté sa demande tendant à l'annulation de l'arrêté du 21 décembre 2007 du maire de Tréflez;<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 123-6 du code de l'urbanisme : " A compter de la publication de la délibération prescrivant l'élaboration d'un plan local d'urbanisme, l'autorité compétente peut décider de surseoir à statuer, dans les conditions et délai prévus à l'article L. 111-8, sur les demandes d'autorisation concernant des constructions, installations ou opérations qui seraient de nature à compromettre ou à rendre plus onéreuse l'exécution du futur plan. " ; qu'aux termes de l'article L. 111-8 du même code : " Le sursis à statuer doit être motivé et ne peut excéder deux ans. / (...) / A l'expiration du délai de validité du sursis à statuer, une décision doit, sur simple confirmation par l'intéressé de sa demande, être prise par l'autorité compétente chargée de la délivrance de l'autorisation, dans le délai de deux mois suivant cette confirmation. Cette confirmation peut intervenir au plus tard deux mois après l'expiration du délai de validité du sursis à statuer. Une décision définitive doit alors être prise par l'autorité compétente pour la délivrance de l'autorisation, dans un délai de deux mois suivant cette confirmation. A défaut de notification de la décision dans ce dernier délai, l'autorisation est considérée comme accordée dans les termes où elle avait été demandée. " ; qu'aux termes de l'article R. 424-9 du même code : " En cas de sursis à statuer, la décision indique en outre la durée du sursis et le délai dans lequel le demandeur pourra, en application du quatrième alinéa de l'article L. 111-8, confirmer sa demande. / En l'absence d'une telle indication, aucun délai n'est opposable au demandeur. " ; <br/>
<br/>
              3. Considérant, d'une part, que, eu égard à son objet, une décision de sursis à statuer prise sur le fondement de l'article L. 123-6 du code de l'urbanisme cesse de produire ses effets, quelle que soit la durée du sursis qu'elle indique, à la date où le plan local d'urbanisme dont l'élaboration ou la révision l'avait justifiée est adopté ; que, d'autre part, dans l'hypothèse où le plan local d'urbanisme dont l'élaboration ou la révision avait justifié la décision de sursis à statuer n'est pas adopté avant l'expiration du délai de validité du sursis, il résulte des dispositions citées au point précédent que, dès lors que la décision de sursis indiquait la durée du sursis et le délai dans lequel le demandeur pouvait confirmer sa demande, ce dernier dispose d'un délai de deux mois à compter de l'expiration du délai de validité du sursis pour confirmer sa demande ; que, dans le cas où le plan local d'urbanisme dont l'élaboration ou la révision avait justifié la décision de sursis à statuer est adopté avant l'expiration du délai indiqué par la décision de sursis, le demandeur dispose, pour confirmer sa demande, d'un délai qui court à compter de la date de l'adoption du plan local d'urbanisme et s'achève deux mois après l'expiration du délai qui lui avait été indiqué ;<br/>
<br/>
              4. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond et des énonciations de l'arrêt attaqué que le nouveau plan local d'urbanisme de la commune de Tréflez, dont l'élaboration avait justifié la décision de sursis à statuer du 20 mars 2006, a été adopté le 20 avril 2007 ; que, d'une part, il résulte de ce qui a été dit ci-dessus que l'adoption du plan local d'urbanisme en cause a mis un terme au sursis à statuer ; que, par suite, la société requérante est fondée à soutenir que la cour a commis une erreur de droit en jugeant que le délai de validité du sursis à statuer n'était pas expiré à cette date ; que, d'autre part, c'est à tort que la cour a écarté, pour ce motif, le moyen tiré de ce que la société était, à défaut d'avoir reçu de la part de la commune une réponse à la confirmation de sa demande, titulaire d'un permis de construire tacite, alors qu'il appartenait à la cour de rechercher si la société avait confirmé sa demande dans un délai conforme aux principes rappelés ci-dessus ; <br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que la SCI Naq Gamma est fondée à demander l'annulation de l'arrêt qu'elle attaque ;  <br/>
<br/>
              6. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              7. Considérant qu'il résulte de ce qui a été dit ci-dessus que dès lors que, ainsi qu'il ressort des pièces du dossier, la décision de sursis à statuer a fixé la durée du sursis à deux années et indiqué le délai dans lequel la société requérante pouvait confirmer sa demande, celle-ci disposait d'un délai qui courait à compter de la date d'adoption du plan local d'urbanisme et s'achevait deux mois après l'expiration du délai qui lui avait été initialement indiqué, soit le 20 mai 2008 ; qu'en confirmant sa demande par une lettre du 12 octobre 2007, reçue en mairie le 15 octobre 2007, la SCI Naq Gamma a respecté ce délai ; que le maire de Tréflez n'ayant pas répondu dans les deux mois suivant cette confirmation, la SCI Naq Gamma était titulaire d'un permis de construire tacite ; que, dès lors, la décision du 21 décembre 2007 doit être regardée comme ayant opéré le retrait de ce permis de construire ; que cette décision devait par suite être prise dans le respect de la procédure contradictoire prévue par les dispositions de l'article 24 de la loi du 12 avril 2000 relative aux droits des citoyens dans leurs relations avec les administrations ; que, dès lors, la SCI Naq Gamma est fondée à soutenir que c'est à tort que le tribunal administratif a jugé que le moyen tiré de la méconnaissance des dispositions de l'article 24 de la loi du 12 avril 2000 devait être écarté comme inopérant ; qu'il ressort des pièces du dossier que la décision du 21 décembre 2007 n'a été précédée d'aucune procédure contradictoire ; qu'ainsi, sans qu'il soit besoin d'examiner les autres moyens soulevés devant le tribunal administratif, la SCI Naq Gamma est fondée à demander l'annulation du jugement du tribunal administratif de Rennes du 12 mai 2010 en tant qu'il rejette sa demande d'annulation de l'arrêté du 21 décembre 2007 lui refusant le permis de construire demandé, ainsi que de cet arrêté ;<br/>
<br/>
              8. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de Tréflez la somme de 3 000 euros à verser la SCI Naq Gamma, au titre des dispositions de l'article L. 761-1 du code de justice administrative ; qu'en revanche, ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de la SCI Naq Gamma qui n'est pas, dans la présente instance, la partie perdante ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nantes est annulé.<br/>
<br/>
Article 2 : Le jugement du tribunal administratif de Rennes du 12 mai 2010 est annulé en tant qu'il rejette la demande de la SCI Naq Gamma tendant à l'annulation de la décision du 21 décembre 2007. <br/>
<br/>
      Article 3 : La décision du 21 décembre 2007 du maire de Tréflez est annulée.<br/>
<br/>
Article 4 : La commune de Tréflez versera à la SCI Naq Gamma une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 5 : Les conclusions de la commune de Tréflez présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
<br/>
Article 6 : La présente décision sera notifiée à la SCI Naq Gamma et à la commune de Treflez.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">68-03-025-01-02 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PERMIS DE CONSTRUIRE. NATURE DE LA DÉCISION. SURSIS À STATUER. DURÉE. - EXPIRATION - 1) DATE - ADOPTION DU PLAN LOCAL D'URBANISME, QUELLE QUE SOIT LA DATE INDIQUÉE DANS LA DÉCISION DE SURSIS - 2) EFFETS - PÉRIODE OUVERTE À L'INTÉRESSÉ POUR CONFIRMER SA DEMANDE - A) DANS LE CAS OÙ LE PLAN N'EST PAS ADOPTÉ AVANT LA DATE D'EXPIRATION INDIQUÉE - DEUX MOIS À COMPTER DE CETTE DATE - B) DANS LE CAS OÙ LE PLAN EST ADOPTÉ AVANT - DE L'ADOPTION DU PLAN JUSQU'À DEUX MOIS APRÈS LA DATE INDIQUÉE SUR LA DÉCISION DE SURSIS.
</SCT>
<ANA ID="9A"> 68-03-025-01-02 1) Eu égard à son objet, une décision de sursis à statuer prise sur le fondement de l'article L. 123-6 du code de l'urbanisme cesse de produire ses effets, quelle que soit la durée du sursis qu'elle indique, à la date où le plan local d'urbanisme dont l'élaboration ou la révision l'avait justifiée est adopté.... ,,2) a) Dans l'hypothèse où le plan local d'urbanisme dont l'élaboration ou la révision avait justifié la décision de sursis à statuer n'est pas adopté avant l'expiration du délai de validité du sursis, il résulte des dispositions des articles L. 123-6, L. 111-8 et R. 424-9 du même code que, dès lors que la décision de sursis indiquait la durée du sursis et le délai dans lequel le demandeur pouvait confirmer sa demande, ce dernier dispose d'un délai de deux mois à compter de l'expiration du délai de validité du sursis pour confirmer sa demande.,,,b) Dans le cas où le plan est adopté avant l'expiration du délai indiqué par la décision de sursis, le demandeur dispose, pour confirmer sa demande, d'un délai qui court à compter de la date de l'adoption du plan et s'achève deux mois après l'expiration du délai qui lui avait été indiqué.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
