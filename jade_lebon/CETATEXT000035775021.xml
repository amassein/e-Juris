<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035775021</ID>
<ANCIEN_ID>JG_L_2017_10_000000414148</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/77/50/CETATEXT000035775021.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 11/10/2017, 414148</TITRE>
<DATE_DEC>2017-10-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>414148</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MONOD, COLIN, STOCLET ; HAAS</AVOCATS>
<RAPPORTEUR>M. Vincent Villette</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:414148.20171011</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
Procédure contentieuse antérieure<br/>
<br/>
              M. C...A...et autres ont demandé au tribunal administratif de Caen d'annuler la décision du 11 janvier 2017 par laquelle la maire de Granville a fixé la liste des conseillers municipaux en exercice de ladite commune et d'enjoindre au préfet de la Manche de procéder aux élections d'un nouveau conseil municipal sans délai. Par un jugement n° 1700069 du 9 février 2017, le tribunal a, d'une part, annulé la décision par laquelle la maire de Granville a écarté le renoncement à siéger au conseil municipal de Mme E...et l'a appelée à siéger en remplacement d'un conseiller démissionnaire, d'autre part, enjoint à la maire de transmettre au préfet de la Manche la démission de MmeE..., enfin mis à la charge de la commune de Granville la somme de 1 500 euros à verser aux requérants au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Par une décision n° 408295 du 19 juillet 2017, le Conseil d'Etat statuant au contentieux a, sur appel formé par la commune de Granville, annulé ce jugement en tant qu'il avait mis à la charge de cette commune une somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative, rejeté les conclusions présentées à ce titre par M. A...et autres, et rejeté le surplus des conclusions d'appel de la commune.  <br/>
<br/>
Recours en tierce opposition<br/>
<br/>
              Par une requête en tierce opposition et un mémoire en réplique, enregistrés les 8 et 14 septembre 2017 au secrétariat du contentieux du Conseil d'Etat, Mme B...E...et autres demandent au Conseil d'Etat : <br/>
<br/>
              1°) de déclarer non avenue la décision du Conseil d'Etat statuant au contentieux du 19 juillet 2017 ; <br/>
<br/>
              2°) d'annuler le jugement du tribunal administratif de Caen du 9 février 2017 ; <br/>
<br/>
              3°) de rejeter la requête de M. A...et autres. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code électoral ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Vincent Villette, auditeur,  <br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Monod, Colin, Stoclet, avocat de Mme E...et autres et à Me Haas, avocat de M. C...A..., et autres ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 18 septembre 2017, présentée par M. A... et autres ; <br/>
<br/>
              Vu la note en délibéré, enregistrée le 19 septembre 2017, présentée par Mme E... et autres ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Mme E...et autres demandent au Conseil d'Etat de déclarer non avenue la décision du 19 juillet 2017 par laquelle le Conseil d'Etat, statuant au contentieux, a rejeté l'appel formé par la commune de Granville contre le jugement du 9 février 2017 par lequel le tribunal administratif de Caen a, d'une part, annulé la décision de la maire de Granville écartant le renoncement à siéger au conseil municipal de Mme E...et l'appelant à siéger en remplacement d'un conseiller démissionnaire, d'autre part, enjoint à la maire de transmettre au préfet de la Manche la démission de MmeE.... Ils soutiennent que cette décision juridictionnelle, en ce qu'elle conduit à constater que le conseil municipal de Granville a perdu, par l'effet des vacances survenues, le tiers de ses membres, impliquerait le renouvellement intégral du conseil municipal en application de l'article L. 270 du code électoral, et dans ces conditions préjudicie à leurs droits, dès lors notamment qu'ils siègent au sein de ce conseil municipal.<br/>
<br/>
              Sur la recevabilité de la tierce opposition :<br/>
<br/>
              2. Aux termes de l'article R. 832-1 du code de justice administrative : " Toute personne peut former tierce opposition à une décision juridictionnelle qui préjudicie à ses droits, dès lors que ni elle ni ceux qu'elles représentent n'ont été présents ou régulièrement appelés dans l'instance ayant abouti à cette décision ". Une tierce-opposition contre le jugement rendu par le tribunal administratif formée après qu'une partie a frappé ce jugement d'appel est irrecevable. La personne qui aurait eu qualité pour former tierce-opposition est dans ce cas recevable à intervenir dans la procédure d'appel ou, si elle n'a été ni présente ni représentée devant la juridiction d'appel, à former tierce-opposition contre l'arrêt rendu par celle-ci, s'il préjudicie à ses droits. La personne recevable à intervenir dans la procédure d'appel acquiert la qualité de partie dans cette instance. <br/>
<br/>
              3. Il ressort des pièces de la procédure que les requérants ont introduit leur requête en tierce opposition, devant le Conseil d'Etat après que celui-ci, statuant au contentieux, a, par une décision du 19 juillet 2017, rejeté comme irrecevable l'appel formé par la commune de Granville contre le jugement du tribunal administratif de Caen du 9 février 2017. Par suite, les requérants, qui siègent au conseil municipal et qui n'ont pas été présents ou régulièrement appelés dans cette instance, sont recevables à former tierce opposition à cette décision dès lors qu'elle rejette l'appel dirigé contre le jugement du tribunal administratif de Caen du 9 février 2017 qui préjudicie à leurs droits en étant susceptible de conduire au renouvellement intégral du conseil municipal. <br/>
<br/>
              Sur le bien-fondé de la tierce-opposition :<br/>
<br/>
              4. Aux termes de l'article L. 270 du code électoral : " Le candidat venant sur une liste immédiatement après le dernier élu est appelé à remplacer le conseiller municipal élu sur cette liste dont le siège devient vacant pour quelque cause que ce soit (...) ". L'article L. 2121-4 du code général des collectivités territoriales prévoit : " Les démissions des membres du conseil municipal sont adressées au maire. / La démission est définitive dès sa réception par le maire, qui en informe immédiatement le représentant de l'Etat dans le département ".<br/>
<br/>
              5. Le tribunal administratif de Caen, pour annuler la décision du 11 janvier 2017 par laquelle la maire de Granville a fixé, en y incluant MmeE..., la liste des conseillers municipaux en exercice de ladite commune, a estimé que la lettre par laquelle MmeE..., colistière de la liste " Granville avec vous ", avait renoncé à siéger, suite à la démission du conseil municipal de cinq conseillers, en remplacement de ces élus démissionnaires emportait démission définitive de l'intéressée dès sa réception par la maire.<br/>
<br/>
              6. Il résulte de l'instruction que la lettre que Mme E...a été invitée à signer afin de faire connaître son renoncement à siéger, qui a été rédigée antérieurement aux démissions et aux renoncements qui ont conduit à la vacance du siège en cause puis datée par un tiers postérieurement à sa signature, ne peut être regardée, dans les circonstances particulières de l'espèce, compte tenu notamment des conditions dans lesquelles le consentement de Mme E... a été recueilli, comme une démission au sens de l'article L. 2121-4 du code général des collectivités territoriales. Par suite, c'est à tort que le tribunal administratif s'est fondé sur la démission de Mme E...pour annuler la décision de la maire de Granville et pour enjoindre à cette dernière de transmettre au préfet de la Manche la démission de MmeE.... <br/>
<br/>
              7. Il appartient au Conseil d'Etat, saisi de l'ensemble du litige par l'effet dévolutif de l'appel, d'examiner l'autre moyen soulevé par M. A...et autres devant le tribunal administratif de Caen. Ce moyen, tiré de ce que la décision litigieuse serait entachée d'incompétence, faute de pouvoir identifier son signataire, manque en fait.  <br/>
<br/>
              8. Il résulte de ce qui précède que Mme E...et autres sont fondés à soutenir que c'est à tort que, par le jugement litigieux, le tribunal administratif de Caen a, d'une part, annulé la décision par laquelle la maire de Granville a écarté le renoncement à siéger au conseil municipal de Mme E...et l'a appelée à siéger en remplacement d'un conseiller démissionnaire, d'autre part, enjoint à la maire de transmettre au préfet de la Manche la démission de MmeE.... Il en résulte que le jugement du tribunal administratif de Caen du 9 février 2017 doit être annulé, que, par suite, les conclusions d'appel de la commune de Granville dirigées contre ce jugement sont devenues sans objet et que la décision du Conseil d'Etat du 19 juillet 2017 doit être déclarée non avenue.<br/>
<br/>
              9. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise, à ce titre, à la charge de Mme E...et autres qui ne sont pas, dans la présente instance, la partie perdante. Il n'y pas lieu de faire droit aux conclusions présentées au titre de ces mêmes dispositions, tant par la commune de Granville dans le cadre de son appel que par les défendeurs. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : La tierce-opposition formée par Mme E...et autres est admise.<br/>
Article 2 : La décision du Conseil d'Etat statuant au contentieux du 19 juillet 2017 est déclarée non avenue.<br/>
Article 3 : Le jugement du tribunal administratif de Caen du 9 février 2017 est annulé. <br/>
Article 4 : La demande de M. A...et autres présentée devant le tribunal administratif de Caen, ainsi que leurs conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative, sont rejetées. <br/>
Article 5 : Il n'y a pas lieu de statuer sur les conclusions d'appel de la commune de Granville dirigé contre le jugement du tribunal administratif de Caen du 9 février 2017.<br/>
Article 6 : Les conclusions de la commune de Granville présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative, sont rejetées.<br/>
Article 7 : La présente décision sera notifiée à Mme B...E..., première dénommée, et à M. C... A..., premier dénommé. Les autres requérants seront informés de la présente décision respectivement par la SCP Monod-Colin-Stoclet et par Maître D...Haas, avocats au Conseil d'Etat et à la Cour de cassation, qui les représentent devant le Conseil d'Etat.<br/>
Copie en sera transmise au ministre d'Etat, ministre de l'intérieur. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-08-04-01 PROCÉDURE. VOIES DE RECOURS. TIERCE-OPPOSITION. RECEVABILITÉ. - TIERCE-OPPOSITION FORMÉE PAR DES REQUÉRANTS CONTRE UNE DÉCISION REJETANT COMME IRRECEVABLE L'APPEL FORMÉ CONTRE UN JUGEMENT PRÉJUDICIANT À LEURS DROITS - RECEVABILITÉ - EXISTENCE [RJ1].
</SCT>
<ANA ID="9A"> 54-08-04-01 Lorsqu'un jugement a été frappé d'appel, la personne qui aurait eu qualité pour former tierce-opposition contre ce jugement est recevable à intervenir dans la procédure d'appel ou, si elle n'a été ni présente ni représentée devant la juridiction d'appel, à former tierce-opposition contre l'arrêt rendu par celle-ci, s'il préjudicie à ses droits, y compris lorsqu'il s'agit d'un arrêt de rejet pour irrecevabilité.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf, en précisant, CE, 2 juillet 2014, M.,et autres, n° 366150, T. pp. 820-832.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
