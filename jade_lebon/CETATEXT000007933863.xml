<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000007933863</ID>
<ANCIEN_ID>JGXLX1996X10X0000030031</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/07/93/38/CETATEXT000007933863.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'Etat, 2 / 6 SSR, du 30 octobre 1996, 130031, publié au recueil Lebon</TITRE>
<DATE_DEC>1996-10-30</DATE_DEC>
<JURIDICTION>Conseil d'Etat</JURIDICTION>
<NUMERO>130031</NUMERO>
<SOLUTION>Rejet</SOLUTION>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2 / 6 SSR</FORMATION>
<TYPE_REC>Recours pour excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Labetoulle</PRESIDENT>
<AVOCATS>SCP Guiguet, Bachellier, de la Varde, Avocat</AVOCATS>
<RAPPORTEUR>Mme de Margerie</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Abraham</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>    Vu la requête sommaire et le mémoire complémentaire enregistrés les 8 octobre 1991 et 10 février 1992 au secrétariat du contentieux du Conseil d'Etat présentés pour la société Henri X... dont le siège est 57, Grand'Rue à Munster (68140) agissant par son gérant M. Henri X... domicilié audit siège ; M. Henri X... demande au Conseil d'Etat :<br/>    1°) d'annuler le jugement du 6 août 1991 par lequel le tribunal administratif de Strasbourg a rejeté sa demande tendant à l'annulation de la délibération en date du 12 février 1987 par laquelle le conseil municipal de Munster a adopté le projet d'aménagement de la Grand'Rue ;<br/>    2°) d'annuler ladite décision pour excès de pouvoir ;<br/>    Vu les autres pièces du dossier ;<br/>    Vu le code l'urbanisme ;<br/>    Vu le code des communes ;<br/>    Vu le code des tribunaux administratifs et des cours administratives d'appel ;<br/>    Vu l'ordonnance n° 45-1708 du 31 juillet 1945, le décret n° 53-934 du 30 septembre 1953 et la loi n° 87-1127 du 31 décembre 1987 ;<br/>    Après avoir entendu en audience publique :<br/>    - le rapport de Mme de Margerie, Maître des Requêtes,<br/>    - les observations de la SCP Guiguet, Bachellier, de la Varde, avocat de la société Henri X...,<br/>    - les conclusions de M. Abraham, Commissaire du gouvernement ;<br/>
<br/>    Considérant que par délibération du 12 février 1987, la commune de Munster a adopté un projet d'aménagement de la Grand'Rue ; que cette délibération a été prise dans l'exercice des compétences du conseil municipal et dans un but d'intérêt général ; que M. X... n'invoque pas la méconnaissance de dispositions législatives ou réglementaires ; que s'il soutient que l'aménagement de la Grand'Rue aurait pour conséquence d'empêcher le stationnement de véhicules venant approvisionner son entrepôt situé sur la Grand'Rue, il ne ressort pas des pièces du dossier que la délibération attaquée serait entachée d'une erreur manifeste d'appréciation ;<br/>    Considérant que la circonstance qu'un autre riverain de la Grand'rue a été autorisé à étendre l'emprise de la terrasse de son hôtel sur le trottoir opposé est sans incidence sur les conditions dans lesquelles est desservi l'immeuble de M. X... et ne révèle pas, au détriment de celui-ci, une méconnaissance du principe d'égalité ;<br/>    Considérant qu'il résulte de ce qui précède que M. X... n'est pas fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Strasbourg a rejeté sa demande tendant à l'annulation de la délibération du conseil municipal de Munster en date du 12 février 1987 ;<br/>Article 1er : La requête de M. X... est rejetée.<br/>Article 2 : La présente décision sera notifiée à M. Henri X..., à la commune de Munster et au ministre de l'équipement, du logement, des transports et du tourisme.<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8AA" TYPE="PRINCIPAL">135-02-01-02-01-02-02 COLLECTIVITES TERRITORIALES - COMMUNE - ORGANISATION DE LA COMMUNE - ORGANES DE LA COMMUNE - CONSEIL MUNICIPAL - ATTRIBUTIONS - DECISIONS RELEVANT DE LA COMPETENCE DU CONSEIL MUNICIPAL -Décision trouvant son fondement dans les compétences générales du conseil - Délibération adoptant un projet d'aménagement d'une rue de la commune.</SCT>
<SCT ID="8BA" TYPE="PRINCIPAL">135-02-01-02-01-03 COLLECTIVITES TERRITORIALES - COMMUNE - ORGANISATION DE LA COMMUNE - ORGANES DE LA COMMUNE - CONSEIL MUNICIPAL - DELIBERATIONS -Décision trouvant son fondement dans les compétences générales du conseil - Délibération adoptant un projet d'aménagement d'une rue de la commune - Contrôle du juge - Contrôle restreint.</SCT>
<SCT ID="8CA" TYPE="PRINCIPAL">54-07-02-04 PROCEDURE - POUVOIRS ET DEVOIRS DU JUGE - CONTROLE DU JUGE DE L'EXCES DE POUVOIR - APPRECIATIONS SOUMISES A UN CONTROLE RESTREINT -Délibération d'un conseil municipal adoptant un projet d'aménagement d'une rue de la commune sur le fondement des compétences générales du conseil.</SCT>
<ANA ID="9AA">135-02-01-02-01-02-02, 135-02-01-02-01-03, 54-07-02-04        Délibération du conseil municipal de Munster adoptant un projet d'aménagement de la Grand'Rue. Cette délibération a été prise dans l'exercice des compétences du conseil municipal et dans un but d'intérêt général. Il n'est pas soutenu qu'elle méconnaîtrait des dispositions législatives ou réglementaires. Si le requérant allègue que l'aménagement projeté aurait pour conséquence d'empêcher le stationnement de véhicules venant approvisionner son entrepôt, il ne ressort pas des pièces du dossier que la délibération soit entachée d'une erreur manifeste d'appréciation.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
