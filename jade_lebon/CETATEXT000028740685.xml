<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028740685</ID>
<ANCIEN_ID>JG_L_2014_03_000000354596</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/74/06/CETATEXT000028740685.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème / 8ème SSR, 17/03/2014, 354596, Publié au recueil Lebon</TITRE>
<DATE_DEC>2014-03-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>354596</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème / 8ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>RICARD ; SCP GASCHIGNARD</AVOCATS>
<RAPPORTEUR>M. Guillaume Odinet</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Astrid Nicolazo de Barmon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:354596.20140317</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 5 décembre 2011 et 29 février 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour l'association des consommateurs de la Fontaulière, dont le siège est 11, rue Paul Fayette à Labegude (07200), représentée par son président ; l'association demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 10LY00581 du 6 octobre 2011 par lequel la cour administrative d'appel de Lyon a rejeté son appel du jugement n° 0800986 du 24 décembre 2009 par lequel le tribunal administratif de Lyon a rejeté sa demande tendant à l'annulation, d'une part, des délibérations du 12 décembre 2007 par lesquelles le comité syndical du syndicat des eaux de la basse-Ardèche a autorisé son président à signer avec la société Saur les contrats de délégation de service public de distribution d'eau potable et d'assainissement collectif, d'autre part, des décisions du président de ce syndicat de signer ces contrats, et à ce qu'il soit enjoint sous astreinte au syndicat, s'il ne peut obtenir la résolution amiable des contrats, de saisir le juge du contrat, afin que celui-ci prononce leur résolution ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
              3°) de mettre à la charge du syndicat des eaux de la basse-Ardèche la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Guillaume Odinet, Auditeur,  <br/>
<br/>
              - les conclusions de Mme Marie-Astrid Nicolazo de Barmon, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Ricard, avocat de l'association de consommateurs de la Fontauliere et à la SCP Gaschignard, avocat du syndicat des eaux de la basse-Ardèche ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des termes de l'arrêt attaqué que, pour juger que l'association des consommateurs de la Fontaulière ne justifiait pas d'un intérêt lui donnant qualité pour agir contre les délibérations du 12 décembre 2007 par lesquelles le syndicat des eaux de la basse-Ardèche (SEBA) a autorisé son président à signer les contrats de délégation des services publics de l'eau potable et de l'assainissement et contre les décisions du président de signer ces contrats, la cour administrative d'appel s'est fondée sur la seule circonstance que l'objet de cette association, tel que défini par ses statuts, ne précisait pas de ressort géographique, ce dont elle a déduit que l'association avait un champ d'action " national " et qu'elle n'était donc pas recevable à demander l'annulation d'actes administratifs ayant des effets " exclusivement locaux " ; qu'en statuant ainsi, alors qu'il lui appartenait, en l'absence de précisions sur le champ d'intervention de l'association dans les stipulations de ses statuts définissant son objet, d'apprécier son intérêt à agir contre les décisions qu'elle attaquait au regard de son champ d'intervention en prenant en compte les indications fournies sur ce point par les autres stipulations des statuts, notamment par le titre de l'association et les conditions d'adhésion, éclairées, le cas échéant, par d'autres pièces du dossier qui lui était soumis, la cour a commis une erreur de droit ; que son arrêt doit être, pour ce motif, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, annulé ;<br/>
<br/>
              2. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du SEBA la somme de 3 000 euros à verser à l'association requérante au titre des dispositions de l'article L. 761-1 du code de justice administrative ; qu'en revanche, ces dispositions font obstacle à ce qu'une somme soit mise à la charge de l'association des consommateurs de la Fontaulière, qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 6 octobre 2011 de la cour administrative d'appel de Lyon est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Lyon.<br/>
Article 3 : Le syndicat des eaux de la basse-Ardèche versera à l'association des consommateurs de la Fontaulière une somme de 3 000 euros au titre de l'article <br/>
L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions présentées par le syndicat des eaux de la basse-Ardèche au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à l'association des consommateurs de la Fontaulière et au syndicat des eaux de la basse-Ardèche.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">10-01-05-02 ASSOCIATIONS ET FONDATIONS. QUESTIONS COMMUNES. CONTENTIEUX. INTÉRÊT POUR AGIR. - INTÉRÊT POUR AGIR CONTRE UN ACTE - MODALITÉS D'APPRÉCIATION - CAS OÙ LES STIPULATIONS DES STATUTS DÉFINISSANT L'OBJET DE L'ASSOCIATION NE COMPORTENT PAS DE PRÉCISION SUR SON CHAMP D'INTERVENTION - PRISE EN COMPTE DES INDICATIONS FOURNIES SUR CE POINT PAR LES AUTRES STIPULATIONS DES STATUTS, ÉCLAIRÉES, LE CAS ÉCHÉANT, PAR D'AUTRES PIÈCES DU DOSSIER - EXISTENCE - CONSÉQUENCE - DÉDUCTION DE LA SEULE ABSENCE DE PRÉCISION SUR LE CHAMP D'INTERVENTION DANS L'OBJET DE L'ASSOCIATION TEL QUE DÉFINI PAR LES STATUTS QUE L'ASSOCIATION A UN CHAMP D'ACTION  NATIONAL  ET NE PEUT DEMANDER L'ANNULATION D'UN ACTE AYANT DES EFFETS  EXCLUSIVEMENT LOCAUX  - ABSENCE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-01-04 PROCÉDURE. INTRODUCTION DE L'INSTANCE. INTÉRÊT POUR AGIR. - ASSOCIATION - MODALITÉS D'APPRÉCIATION - CAS OÙ LES STIPULATIONS DES STATUTS DÉFINISSANT L'OBJET DE L'ASSOCIATION NE COMPORTENT PAS DE PRÉCISION SUR SON CHAMP D'INTERVENTION - PRISE EN COMPTE DES INDICATIONS FOURNIES SUR CE POINT PAR LES AUTRES STIPULATIONS DES STATUTS, ÉCLAIRÉES, LE CAS ÉCHÉANT, PAR D'AUTRES PIÈCES DU DOSSIER - EXISTENCE - CONSÉQUENCE - DÉDUCTION DE LA SEULE ABSENCE DE PRÉCISION SUR LE CHAMP D'INTERVENTION DANS L'OBJET DE L'ASSOCIATION TEL QUE DÉFINI PAR LES STATUTS QUE L'ASSOCIATION A UN CHAMP D'ACTION  NATIONAL  ET NE PEUT DEMANDER L'ANNULATION D'UN ACTE AYANT DES EFFETS  EXCLUSIVEMENT LOCAUX  - ABSENCE [RJ1].
</SCT>
<ANA ID="9A"> 10-01-05-02 Pour apprécier si une association justifie d'un intérêt lui donnant qualité pour agir contre un acte, il appartient au juge, en l'absence de précisions sur le champ d'intervention de l'association dans les stipulations de ses statuts définissant son objet, d'apprécier son intérêt à agir contre cet acte au regard de son champ d'intervention en prenant en compte les indications fournies sur ce point par les autres stipulations des statuts, notamment par le titre de l'association et les conditions d'adhésion, éclairées, le cas échéant, par d'autres pièces du dossier. Le juge ne saurait ainsi se fonder sur la seule circonstance que l'objet d'une association, tel que défini par ses statuts, ne précise pas de ressort géographique, pour en déduire que l'association a un champ d'action  national  et qu'elle n'est donc pas recevable à demander l'annulation d'actes administratifs ayant des effets  exclusivement locaux .</ANA>
<ANA ID="9B"> 54-01-04 Pour apprécier si une association justifie d'un intérêt lui donnant qualité pour agir contre un acte, il appartient au juge, en l'absence de précisions sur le champ d'intervention de l'association dans les stipulations de ses statuts définissant son objet, d'apprécier son intérêt à agir contre cet acte au regard de son champ d'intervention en prenant en compte les indications fournies sur ce point par les autres stipulations des statuts, notamment par le titre de l'association et les conditions d'adhésion, éclairées, le cas échéant, par d'autres pièces du dossier. Le juge ne saurait ainsi se fonder sur la seule circonstance que l'objet d'une association, tel que défini par ses statuts, ne précise pas de ressort géographique, pour en déduire que l'association a un champ d'action  national  et qu'elle n'est donc pas recevable à demander l'annulation d'actes administratifs ayant des effets  exclusivement locaux .</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Ab. jur. CE, 23 février 2004, Communauté de communes du pays loudunais, n° 250482, T. pp. 803-851 ; CE, 5 novembre 2004, Association Bretagne littoral environnement urbanisme Bleu, n° 264819, inédite au Recueil.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
