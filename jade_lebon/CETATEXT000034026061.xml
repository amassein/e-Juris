<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034026061</ID>
<ANCIEN_ID>JG_L_2017_02_000000388607</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/02/60/CETATEXT000034026061.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 4ème chambres réunies, 10/02/2017, 388607</TITRE>
<DATE_DEC>2017-02-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>388607</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>OCCHIPINTI</AVOCATS>
<RAPPORTEUR>M. Lionel Collet</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:388607.20170210</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B...A...a demandé au tribunal administratif de Marseille d'enjoindre sous astreinte au préfet des Bouches du Rhône de lui attribuer un logement dans un délai d'un mois à compter de la notification de son jugement. Par un jugement n° 1405258 du 27 octobre 2014,  le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire enregistrés les 10 mars et 10 juin 2015 au secrétariat du contentieux du Conseil d'Etat, Mme A...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement d'une somme de 3 500 euros à Me Occhipinti, qui déclare renoncer en ce cas à l'aide de l'Etat, en application des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - le code de la construction et de l'habitation ; <br/>
<br/>
              - la loi du 10 juillet 1991 relative à l'aide juridictionnelle ;<br/>
<br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Lionel Collet, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Occhipinti, avocat de MmeA....<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que Mme A...a été déclarée prioritaire et devant être relogée en urgence en application de l'article L. 441-2-3 du code de la construction et de l'habitation par une décision du 11 juillet 2013 de la commission de médiation des Bouches-du-Rhône ; qu'après avoir refusé un logement qui lui avait été proposé le 26 novembre 2013, Mme A...a saisi le tribunal administratif de Marseille afin qu'il ordonne au préfet de procéder à une nouvelle offre de logement ; que Mme A... se pourvoit en cassation contre le jugement du 27 octobre 2014 par lequel le tribunal administratif a rejeté sa demande ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 300-1 du code de la construction et de l'habitation : " Le droit à un logement décent et indépendant (...) est garanti par l'Etat à toute personne qui (...) n'est pas en mesure d'y accéder par ses propres moyens ou de s'y maintenir. / Ce droit s'exerce par un recours amiable puis, le cas échéant, par un recours contentieux dans les conditions et selon les modalités fixées par le présent article et les articles L. 441-2-3 et L. 441-2-3-1. " ; qu'aux termes du II de l'article L. 441-2-3 de ce code : " (...) Dans un délai fixé par décret, la commission de médiation désigne les demandeurs qu'elle reconnaît prioritaires et auxquels un logement doit être attribué en urgence. Elle détermine pour chaque demandeur, en tenant compte de ses besoins et de ses capacités, les caractéristiques de ce logement (...) / La commission de médiation transmet au représentant de l'Etat dans le département la liste des demandeurs auxquels doit être attribué en urgence un logement / (...) Le représentant de l'Etat dans le département désigne chaque demandeur à un organisme bailleur disposant de logements correspondant à la demande. En Ile-de-France, il peut aussi demander au représentant de l'Etat d'un autre département de procéder à une telle désignation. (...) / En cas de refus de l'organisme de loger le demandeur, le représentant de l'Etat dans le département qui l'a désigné procède à l'attribution d'un logement correspondant aux besoins et aux capacités du demandeur sur ses droits de réservation. (...) " ; qu'aux termes de l'article R*. 441-14-1 du même code : " (...) Peuvent être désignées par la commission comme prioritaires et devant être logées d'urgence en application du II de l'article L. 441-2-3 les personnes de bonne foi qui satisfont aux conditions réglementaires d'accès au logement social qui se trouvent dans l'une des situations prévues au même article et qui répondent aux caractéristiques suivantes : (...) / - être logées dans des locaux impropres à l'habitation, ou présentant un caractère insalubre ou dangereux (...) " ;<br/>
<br/>
              3. Considérant qu'en vertu des dispositions du premier alinéa du I de l'article L. 441-2-3-1 du code de la construction et de l'habitation, un demandeur qui a été reconnu par une commission de médiation comme prioritaire et comme devant être logé en urgence et qui n'a pas reçu, dans un délai fixé par décret, une offre de logement tenant compte de ses besoins et de ses capacités, peut introduire un recours devant la juridiction administrative tendant à ce que soit ordonné son logement ou son relogement ; que, lorsque le demandeur a refusé un logement qui lui avait été proposé à la suite de la décision de la commission, la juridiction ne peut adresser une injonction à l'administration que si l'offre ainsi rejetée n'était pas adaptée aux besoins et capacités de l'intéressé tels que définis par la commission ou si, bien que cette offre fût adaptée, le demandeur a fait état d'un motif impérieux de nature à justifier son refus ; que, dans ce cadre, l'existence, dans l'immeuble où est situé le logement proposé, d'une situation habituelle d'insécurité qui, du fait d'une vulnérabilité particulière du demandeur ou d'autres éléments liés à sa situation personnelle, crée des risques graves pour lui ou pour sa famille justifie un refus du logement proposé ; que le fait, pour le demandeur, d'avoir été victime d'une agression au cours de la visite du logement qui lui a été proposé est également susceptible de justifier un refus dès lors que, eu égard à sa nature et aux circonstances dans lesquelles elle est intervenue, elle suscite des craintes légitimes d'être exposé à une situation d'insécurité ; <br/>
<br/>
               4. Considérant qu'il ressort des pièces soumises au juge du fond que Mme A... a motivé le refus du logement qui lui avait été proposé par la circonstance qu'elle avait subi une agression, qu'elle a portée à la connaissance des services de police,  lors de la visite du logement le 29 novembre 2013 ; qu'en estimant que Mme A...ne pouvait valablement refuser ce logement pour un tel motif, sans rechercher si, eu égard à sa nature et aux circonstances dans lesquelles elle est intervenue, l'agression dont faisait état l'intéressée suscitait chez elle des craintes légitimes d'être exposée à une situation d'insécurité, le tribunal administratif de  Marseille a commis une erreur de droit ; qu'il y a lieu, par suite, d'annuler son jugement, sans qu'il soit besoin d'examiner l'autre moyen du pourvoi ;<br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 3 500 euros à verser à Me Occhipinti, avocat de MmeA..., en application des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991, sous réserve que Me Occhipinti renonce à percevoir la somme correspondant à la part contributive de l'Etat au titre de l'aide juridictionnelle ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
		Article 1er : Le jugement du tribunal administratif de Marseille est annulé.<br/>
<br/>
		Article 2 : L'affaire est renvoyée au tribunal administratif de Marseille.<br/>
<br/>
Article 3 : L'Etat versera à Me Occhipinti, avocat de MmeA..., une somme de 3 500 euros en application des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
<br/>
Article 4 : La présente décision sera notifiée à Mme B...A..., au ministre du logement et de l'habitat durable et au préfet des Bouches du Rhône.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">38-07-01 LOGEMENT. - DEMANDEUR RECONNU PRIORITAIRE AYANT REFUSÉ UNE PROPOSITION DE LOGEMENT - 1) POSSIBILITÉ POUR LE JUGE DU DALO DE PRONONCER UNE INJONCTION - CONDITIONS - OFFRE INADAPTÉE À SES BESOINS ET CAPACITÉS OU MOTIF IMPÉRIEUX [RJ1] - 2) APPLICATIONS [RJ2] - A) CAS D'UNE SITUATION INHABITUELLE D'INSÉCURITÉ DANS L'IMMEUBLE OÙ EST SITUÉ LE LOGEMENT - B) CAS OÙ LE DEMANDEUR EST VICTIME D'UNE AGRESSION AU COURS DE LA VISITE DU LOGEMENT.
</SCT>
<ANA ID="9A"> 38-07-01 1) Lorsque le demandeur a refusé un logement qui lui avait été proposé à la suite de la décision de la commission, la juridiction ne peut adresser une injonction à l'administration que si l'offre ainsi rejetée n'était pas adaptée aux besoins et capacités de l'intéressé tels que définis par la commission ou si, bien que cette offre fût adaptée, le demandeur a fait état d'un motif impérieux de nature à justifier son refus.,,,2) a) L'existence, dans l'immeuble où est situé le logement proposé, d'une situation habituelle d'insécurité qui, du fait d'une vulnérabilité particulière du demandeur ou d'autres éléments liés à sa situation personnelle, crée des risques graves pour lui ou pour sa famille justifie un refus du logement proposé.,,,b) Le fait, pour le demandeur, d'avoir été victime d'une agression au cours de la visite du logement qui lui a été proposé est également susceptible de justifier un refus dès lors que, eu égard à sa nature et aux circonstances dans lesquelles elle est intervenue, elle suscite des craintes légitimes d'être exposé à une situation d'insécurité.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Cf. CE, 4 novembre 2015, M.,, n° 374241, T. p. 743.,,[RJ2]Rappr. CE, 8 juillet 2016, Mme Bezzaouya, n° 381333, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
