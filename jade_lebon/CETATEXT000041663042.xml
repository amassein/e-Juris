<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041663042</ID>
<ANCIEN_ID>JG_L_2020_02_000000425743</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/66/30/CETATEXT000041663042.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 28/02/2020, 425743</TITRE>
<DATE_DEC>2020-02-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>425743</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOULLOCHE ; SCP COUTARD, MUNIER-APAIRE</AVOCATS>
<RAPPORTEUR>M. Paul Bernard</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Guillaume Odinet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:425743.20200228</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B... A... et M. C... A... ont demandé au tribunal administratif de Nancy de constater l'irrégularité de l'emprise résultant de la présence d'un transformateur électrique sur la parcelle dont ils sont propriétaires, située sur le territoire de la commune d'Errouville (Meurthe-et-Moselle) et d'enjoindre à ERDF de démolir ou de déplacer ce transformateur. <br/>
<br/>
              Par un jugement n° 1600206 du 30 mai 2017, le tribunal administratif a déclaré l'emprise irrégulière et enjoint à la société ENEDIS de déplacer le transformateur dans un délai de six mois, sauf à conclure une convention avec M. et Mme A... en vue d'établir une servitude. <br/>
<br/>
              Par un arrêt n° 17NC01858 du 19 juillet 2018, la cour administrative d'appel de Nancy a annulé ce jugement en tant qu'il a enjoint à la société ENEDIS de déplacer le transformateur dans un délai de six mois. <br/>
<br/>
              Par un pourvoi et un mémoire complémentaire, enregistrés les 27 novembre 2018 et 26 février 2019 au secrétariat du contentieux du Conseil d'Etat, M. et Mme A... demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de la société ENEDIS ; <br/>
<br/>
              3°) de mettre à la charge de la société ENEDIS la somme de 3 000 euros à verser à la SCP Boulloche, leur avocat, au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.  <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code civil ; <br/>
              - le code de l'énergie ;<br/>
              - le code de l'expropriation pour cause d'utilité publique ;<br/>
              - le code de l'urbanisme ; <br/>
              - la loi du 15 juin 1906 sur les distributions d'énergie ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le décret n° 67-886 du 6 octobre 1967 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Paul Bernard, maître des requêtes, <br/>
<br/>
              - les conclusions de M. Guillaume Odinet, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boulloche, avocat de M. et Mme A..., et à la SCP Coutard, Munier-Apaire, avocat de la société Enedis ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1.	Il ressort des énonciations de l'arrêt attaqué que, le 18 octobre 2010, M. et Mme A... ont acheté une parcelle cadastrée section AC n° 63 à Errouville (Meurthe-et-Moselle) pour y construire une maison d'habitation. Le 13 mars 2015, ils ont demandé à la société ERDF, aux droits de laquelle est venue la société ENEDIS, de déplacer un transformateur situé sur leur terrain. A la suite de la décision implicite de rejet qui leur a été opposée, ils ont saisi le tribunal administratif de Nancy d'une demande tendant à ce que le tribunal constate l'irrégularité de l'emprise résultant de la présence du transformateur sur leur terrain, à ce qu'il enjoigne à la société ENEDIS de démolir cet ouvrage et à ce qu'il condamne la société à leur verser 5 000 euros en réparation des préjudices subis. Par un jugement du 30 mai 2017, le tribunal administratif a déclaré l'emprise irrégulière, a enjoint à la société ENEDIS de déplacer le transformateur dans un délai de six mois sauf à conclure une convention avec M. et Mme A... et a condamné la société à leur verser une somme de 500 euros en réparation de leurs préjudices. Par un arrêt du 19 juillet 2018, contre lequel M. et Mme A... se pourvoient en cassation, la cour administrative d'appel de Nancy a annulé ce jugement en tant qu'il a enjoint à la société ENEDIS de déplacer le transformateur.<br/>
<br/>
              2.	Lorsque le juge administratif est saisi d'une demande d'exécution d'une décision juridictionnelle qui juge qu'un ouvrage public a été implanté de façon irrégulière, il lui appartient, pour déterminer, en fonction de la situation de droit et de fait existant à la date à laquelle il statue, si l'exécution de cette décision implique qu'il ordonne le déplacement de cet ouvrage, de rechercher, d'abord, si, eu égard notamment aux motifs de la décision, une régularisation appropriée est possible ; que, dans la négative, il lui revient ensuite de prendre en considération, d'une part, les inconvénients que la présence de l'ouvrage entraîne pour les divers intérêts publics ou privés en présence, notamment, le cas échéant, pour le propriétaire du terrain d'assiette de l'ouvrage, d'autre part, les conséquences de l'enlèvement pour l'intérêt général, et d'apprécier, en rapprochant ces éléments, si cet enlèvement n'entraîne pas une atteinte excessive à l'intérêt général. <br/>
<br/>
              3.	Pour juger qu'en dépit de l'implantation irrégulière du transformateur électrique litigieux sur le terrain de M. et Mme A..., il n'y avait pas lieu d'enjoindre à la société ENEDIS de déplacer cet ouvrage, la cour administrative d'appel s'est fondée sur la circonstance qu'une régularisation appropriée était possible, dès lors que la société ENEDIS pouvait, compte tenu de l'intérêt général qui s'attachait à cet ouvrage, le faire déclarer d'utilité publique et obtenir ainsi la propriété de son terrain d'assiette par voie d'expropriation. En se bornant à déduire l'existence d'une telle possibilité de régularisation de l'intérêt général qui s'attache à l'ouvrage public en cause, sans rechercher si une procédure d'expropriation avait été envisagée et était susceptible d'aboutir, la cour a commis une erreur de droit. Il suit de là que, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, son arrêt doit être annulé. <br/>
<br/>
              4.	Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société ENEDIS une somme de 3 000 euros à verser la SCP Boulloche, avocat de M. et Mme A..., au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.  <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'arrêt du 19 juillet 2018 de la cour administrative d'appel de Nancy est annulé. <br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nancy. <br/>
<br/>
Article 3 : La société ENEDIS versera à la SCP Boulloche, avocat de M. et Mme A..., une somme de 3 000 euros, au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991, sous réserve qu'elle renonce à la part contributive de l'Etat.<br/>
<br/>
Article 4 : La présente décision sera notifiée à Mme B... A..., à M. C... A... et à la société ENEDIS.         <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-06-07-008 PROCÉDURE. JUGEMENTS. EXÉCUTION DES JUGEMENTS. PRESCRIPTION D'UNE MESURE D'EXÉCUTION. - IMPLANTATION IRRÉGULIÈRE D'UN OUVRAGE PUBLIC - PRESCRIPTION PAR LE JUGE DE LA DÉMOLITION DE L'OUVRAGE - CONDITIONS [RJ1] - APPRÉCIATION PAR LE JUGE DE L'EXISTENCE D'UNE POSSIBILITÉ DE RÉGULARISATION - JUGE TENU DE VÉRIFIER QUE LA RÉGULARISATION ÉTAIT ENVISAGÉE ET SUSCEPTIBLE D'ABOUTIR.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">67-05 TRAVAUX PUBLICS. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. - IMPLANTATION IRRÉGULIÈRE D'UN OUVRAGE PUBLIC - PRESCRIPTION PAR LE JUGE DE LA DÉMOLITION DE L'OUVRAGE - CONDITIONS [RJ1] - APPRÉCIATION PAR LE JUGE DE L'EXISTENCE D'UNE POSSIBILITÉ DE RÉGULARISATION - JUGE TENU DE VÉRIFIER QUE LA RÉGULARISATION ÉTAIT ENVISAGÉE ET SUSCEPTIBLE D'ABOUTIR.
</SCT>
<ANA ID="9A"> 54-06-07-008 Le juge ne peut déduire le caractère régularisable d'un ouvrage public irrégulièrement implanté, qui fait obstacle à ce que soit ordonnée sa démolition, de la seule possibilité pour son propriétaire, compte tenu de l'intérêt général qui s'attache à l'ouvrage en cause, de le faire déclarer d'utilité publique et d'obtenir ainsi la propriété de son terrain d'assiette par voie d'expropriation, mais est tenu de rechercher si une procédure d'expropriation avait été envisagée et était susceptible d'aboutir.</ANA>
<ANA ID="9B"> 67-05 Le juge ne peut déduire le caractère régularisable d'un ouvrage public irrégulièrement implanté, qui fait obstacle à ce que soit ordonnée sa démolition, de la seule possibilité pour son propriétaire, compte tenu de l'intérêt général qui s'attache à l'ouvrage en cause, de le faire déclarer d'utilité publique et d'obtenir ainsi la propriété de son terrain d'assiette par voie d'expropriation, mais est tenu de rechercher si une procédure d'expropriation avait été envisagée et était susceptible d'aboutir.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., sur ces conditions, CE, Section, 29 janvier 2003, Syndicat départemental de l'électricité et du gaz des Alpes-Maritimes et commune de Clans, n° 245239, p. 21 ; CE, 29 novembre 2019, M.,, n° 410689, à publier au Recueil.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
