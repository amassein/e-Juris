<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038227979</ID>
<ANCIEN_ID>JG_L_2019_03_000000418170</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/22/79/CETATEXT000038227979.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 5ème chambres réunies, 13/03/2019, 418170</TITRE>
<DATE_DEC>2019-03-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>418170</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 5ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BORE, SALVE DE BRUNETON, MEGRET</AVOCATS>
<RAPPORTEUR>Mme Laurence Franceschini</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Louis Dutheillet de Lamothe</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:418170.20190313</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La commune de Villeneuve-le-Comte (A...-et-Marne) a demandé au tribunal administratif de Melun de condamner l'Etat à lui verser la somme de 55 311,86 euros, augmentée des intérêts à compter du 5 décembre 2013 et de la capitalisation des intérêts à compter du 5 décembre 2014 en réparation du préjudice subi du fait des carences du commissaire-enquêteur lors de la procédure d'enquête préalable à l'approbation de la révision de son plan local d'urbanisme. Par un jugement n° 1401470 du 4 février 2016, le tribunal administratif a rejeté cette demande.<br/>
<br/>
              Par un arrêt n° 16PA00853 du 14 décembre 2017, la cour administrative d'appel de Paris a rejeté l'appel formé par la commune de Villeneuve-le-Comte contre ce jugement.  <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés le 14 février 2018, le 14 mai 2018 et le 11 février 2019 au secrétariat du contentieux du Conseil d'Etat, la commune de Villeneuve-le-Comte demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'environnement ;<br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Laurence Franceschini, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Louis Dutheillet de Lamothe, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boré, Salve de Bruneton, Mégret, avocat de la commune de Villeneuve-le-Comte ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1.	Il ressort des pièces du dossier soumis aux juges du fond que, par un jugement du 27 novembre 2013, le tribunal administratif de Melun a annulé la délibération du 27 février 2012 par laquelle le conseil municipal de Villeneuve-le-Comte avait approuvé son plan local d'urbanisme en raison des irrégularités commises par le commissaire-enquêteur dans l'examen des observations recueillies pendant l'enquête publique ainsi que dans la présentation de ses conclusions. La commune de Villeneuve-le-Comte a alors saisi le préfet de A...-et-Marne d'une demande tendant à la réparation du préjudice qu'elle estime avoir subi en raison des fautes commises par le commissaire-enquêteur. Par un arrêt du 14 décembre 2017 contre lequel la commune de Villeneuve-le-Comte se pourvoit en cassation, la cour administrative d'appel de Paris a confirmé le rejet prononcé par le tribunal administratif de Melun de ses conclusions tendant à la condamnation de l'Etat à lui verser une somme de 55 311,86 euros en réparation de ce préjudice. <br/>
<br/>
              2. Aux termes de l'article L. 123-6 du code de l'urbanisme, dans sa rédaction alors applicable : " (...) le plan local d'urbanisme est élaboré à l'initiative et sous la responsabilité de la commune (...). ". L'article L. 123-10 du même code, dans sa rédaction alors en vigueur dispose que : " Le projet de plan local d'urbanisme est soumis à enquête publique réalisée conformément au chapitre III du titre II du livre Ier du code de l'environnement par (...) le maire. (...) ". Aux termes de l'article R. 123-19 du code de l'urbanisme, alors en vigueur : " Le projet de plan local d'urbanisme est soumis à l'enquête publique (...) par le maire dans les formes prévues par les articles R. 123-7 à R. 123-23 du code de l'environnement. Toutefois, (...) le maire exerce les compétences attribuées au préfet par les articles R. 123-7, R. 123-8, R. 123-13, R. 123-14, R. 123-18 et R. 123-20 à R. 123-23 de ce code. (...). " L'article L. 123-3 du code de l'environnement, alors en vigueur, dispose que l'enquête publique  " a pour objet d'informer le public et de recueillir ses appréciations, suggestions et contre-propositions, postérieurement à l'étude d'impact lorsque celle-ci est requise, afin de permettre à l'autorité compétente de disposer de tous éléments nécessaires à son information ". Aux termes de l'article L. 123-4 du code de l'environnement dans sa rédaction applicable au litige : " L'enquête mentionnée à l'article L. 123-1 est conduite, selon la nature et l'importance des opérations, par un commissaire enquêteur (...) désignés par le président du tribunal administratif ou le membre du tribunal délégué par lui à cette fin  (...) " sur la saisine du maire lorsque l'enquête publique concerne un projet de plan local d'urbanisme communal, conformément aux dispositions combinées des articles R. 123-8 du code de l'environnement et R. 123-19 du code de l'urbanisme dans leur rédaction applicable au litige. En vertu de l'article L. 123-14 du code de l'environnement dans sa rédaction alors en vigueur " Le maître d'ouvrage prend en charge les frais de l'enquête, notamment l'indemnisation des commissaires enquêteurs (...) ainsi que les frais qui sont entraînés par la mise à la disposition du commissaire enquêteur (...) des moyens matériels nécessaires à l'organisation et au déroulement de la procédure d'enquête. / Saisi d'une demande en ce sens par le commissaire enquêteur (...), le président du tribunal administratif ou le magistrat qu'il désigne à cet effet ordonne le versement par le maître d'ouvrage d'une provision dont il définit le montant. L'enquête publique ne peut être ouverte qu'après le versement de cette provision. / Un décret en Conseil d'Etat détermine les conditions dans lesquelles, aux fins de garantir l'indépendance des commissaires enquêteurs (...), sont fixées les règles d'indemnisation de ceux-ci et les modalités de versement par les maîtres d'ouvrage des sommes correspondantes aux intéressés. " Aux termes de l'article R. 123-10 du code de l'environnement dans sa rédaction applicable au litige : " Les commissaires enquêteurs (...) ont droit à une indemnité, à la charge du maître d'ouvrage, qui comprend des vacations et le remboursement des frais qu'ils engagent pour l'accomplissement de leur mission. / Le président du tribunal administratif qui a désigné le commissaire enquêteur (...), ou le membre du tribunal délégué par lui à cet effet, détermine le nombre de vacations allouées au commissaire enquêteur sur la base du nombre d'heures que le commissaire enquêteur déclare avoir consacrées à l'enquête, en tenant compte des difficultés de l'enquête ainsi que de la nature et de la qualité du travail fourni par celui-ci. (...). " Enfin, conformément aux dispositions combinées des articles R. 123-22 du code de l'environnement et R. 123-19 du code de l'urbanisme dans leur rédaction alors en vigueur, le commissaire enquêteur transmet au maire le dossier de l'enquête avec son rapport et ses conclusions motivées dans un délai d'un mois à compter de la date de clôture de l'enquête. <br/>
<br/>
              3. Il résulte de ces dispositions que le plan local d'urbanisme soumis à enquête publique est élaboré à l'initiative et sous la responsabilité de la commune. La mission du commissaire-enquêteur consiste à établir un rapport adressé au maire relatant le déroulement de l'enquête et examinant les observations recueillies et à consigner, dans un document séparé, ses conclusions motivées, en précisant si elles sont favorables ou non au projet. Le commissaire enquêteur, qui conduit ainsi une enquête à caractère local, destinée à permettre non seulement aux habitants de la commune de prendre une connaissance complète du projet et de présenter leurs observations, suggestions et contre-propositions, mais également à l'autorité compétente de disposer de tous les éléments nécessaires à son information et ainsi de l'éclairer dans ses choix, doit être regardé comme exerçant sa mission au titre d'une procédure conduite par la commune. Si le commissaire enquêteur est susceptible de prendre en compte tous les éléments révélés par l'enquête publique, y compris ceux qui ne concernent pas directement la commune, il n'en exerce pas pour autant sa mission, comme le soutient la commune requérante, au nom et pour le compte de l'Etat. Le fait que la commune ne puisse ni procéder elle-même à sa désignation ni décider du montant de sa rémunération est destiné à garantir l'indépendance du commissaire enquêteur ainsi que son impartialité à l'égard de la commune, qui assume la charge des frais d'enquête, notamment le versement de son indemnité. Si, à la date des faits en cause, aucune procédure n'était prévue pour permettre au maire, constatant une irrégularité dans le rapport ou les conclusions du commissaire enquêteur, d'en saisir le président du tribunal administratif, il lui appartenait en revanche de ne pas donner suite à une procédure entachée d'irrégularités et d'en tirer les conséquences en demandant soit au commissaire enquêteur de corriger ces irrégularités soit de mettre en oeuvre une nouvelle procédure en saisissant à nouveau le président du tribunal administratif pour qu'il procède à la désignation d'un nouveau commissaire enquêteur.<br/>
<br/>
              4. Par suite, en jugeant que la responsabilité de l'Etat ne pouvait être engagée en raison des irrégularités commises par le commissaire enquêteur lors de la mission qu'il a réalisée dans le cadre de l'élaboration du projet de plan local d'urbanisme de la commune, la cour administrative d'appel de Paris n'a pas commis d'erreur de droit. En jugeant que la circonstance que l'adoption du plan local d'urbanisme de la commune de Villeneuve-le-Comte serait une condition préalable à la réalisation du projet de Village-nature, classé par l'Etat " opération d'intérêt national " et " projet d'intérêt général ", ne permettait pas davantage que la responsabilité de l'Etat soit poursuivie en raison des fautes commises par le commissaire enquêteur lors de sa mission sur le projet de plan local d'urbanisme communal, la cour administrative d'appel de Paris n'a pas inexactement qualifié les faits qui lui étaient soumis et ne les a pas dénaturés. <br/>
<br/>
              5. Il résulte de tout ce qui précède que le pourvoi de la commune de Villeneuve-le-Comte doit être rejeté, y compris ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la commune de Villeneuve-le-Comte est rejeté.<br/>
Article 2 : La présente décision sera notifiée à la commune de Villeneuve-le-Comte et au ministre d'Etat, ministre de la transition écologique et solidaire.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">44-006-05-06 NATURE ET ENVIRONNEMENT. - 1) MISSION DU COMMISSAIRE ENQUÊTEUR - EXERCICE POUR LE COMPTE DE LA COMMUNE - 2) OBLIGATION POUR LE MAIRE DE NE PAS DONNER SUITE À UNE PROCÉDURE ENTACHÉE D'IRRÉGULARITÉS COMMISES PAR LE COMMISSAIRE ENQUÊTEUR - 3) RESPONSABILITÉ DE L'ETAT DU FAIT DE CES IRRÉGULARITÉS - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">60-03-02-02-01 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. PROBLÈMES D'IMPUTABILITÉ. PERSONNES RESPONSABLES. ÉTAT OU AUTRES COLLECTIVITÉS PUBLIQUES. ÉTAT OU COMMUNE. - IRRÉGULARITÉS COMMISES PAR LE COMMISSAIRE ENQUÊTEUR DANS LE CADRE DE L'ÉLABORATION DU PROJET DE PLAN LOCAL D'URBANISME - IMPUTABILITÉ À L'ETAT - ABSENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">68-01-01-01-01-05 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PLANS D'AMÉNAGEMENT ET D'URBANISME. PLANS D`OCCUPATION DES SOLS (POS) ET PLANS LOCAUX D'URBANISME (PLU). LÉGALITÉ DES PLANS. PROCÉDURE D'ÉLABORATION. ENQUÊTE PUBLIQUE. - 1) MISSION DU COMMISSAIRE ENQUÊTEUR - EXERCICE POUR LE COMPTE DE LA COMMUNE - 2) OBLIGATION POUR LE MAIRE DE NE PAS DONNER SUITE À UNE PROCÉDURE ENTACHÉE D'IRRÉGULARITÉS COMMISES PAR LE COMMISSAIRE ENQUÊTEUR - 3) RESPONSABILITÉ DE L'ETAT DU FAIT DE CES IRRÉGULARITÉS - ABSENCE.
</SCT>
<ANA ID="9A"> 44-006-05-06 1) Il résulte des articles L. 123-6, L. 123-10 et R. 123-19 du code de l'urbanisme et des articles L.123-3, L. 123-4, L. 123-14, R. 123-10 et R. 123-22 du code de l'environnement que le plan local d'urbanisme soumis à enquête publique est élaboré à l'initiative et sous la responsabilité de la commune. Eu égard aux caractéristiques et aux finalités de sa mission, le commissaire enquêteur doit être regardé comme l'exerçant au titre d'une procédure conduite par la commune.... ...2) Si, à la date des faits en cause, aucune procédure n'était prévue pour permettre au maire, constatant une irrégularité dans le rapport ou les conclusions du commissaire enquêteur, d'en saisir le président du tribunal administratif, il lui appartenait en revanche de ne pas donner suite à une procédure entachée d'irrégularités et d'en tirer les conséquences en demandant soit au commissaire enquêteur de corriger ces irrégularités soit de mettre en oeuvre une nouvelle procédure en saisissant à nouveau le président du tribunal administratif pour qu'il procède à la désignation d'un nouveau commissaire enquêteur.,,3) Ne commet pas d'erreur de droit la cour administrative d'appel qui juge que la responsabilité de l'Etat ne pouvait être engagée en raison des irrégularités commises par le commissaire enquêteur lors de la mission qu'il a réalisée dans le cadre de l'élaboration du projet de plan local d'urbanisme de la commune.</ANA>
<ANA ID="9B"> 60-03-02-02-01 Il résulte des articles L. 123-6, L. 123-10 et R. 123-19 du code de l'urbanisme et des articles L.123-3, L. 123-4, L. 123-14, R. 123-10 et R. 123-22 du code de l'environnement que le plan local d'urbanisme soumis à enquête publique est élaboré à l'initiative et sous la responsabilité de la commune. Eu égard aux caractéristiques et aux finalités de sa mission, le commissaire enquêteur doit être regardé comme l'exerçant au titre d'une procédure conduite par la commune. Si, à la date des faits en cause, aucune procédure n'était prévue pour permettre au maire, constatant une irrégularité dans le rapport ou les conclusions du commissaire enquêteur, d'en saisir le président du tribunal administratif, il lui appartenait en revanche de ne pas donner suite à une procédure entachée d'irrégularités et d'en tirer les conséquences en demandant soit au commissaire enquêteur de corriger ces irrégularités soit de mettre en oeuvre une nouvelle procédure en saisissant à nouveau le président du tribunal administratif pour qu'il procède à la désignation d'un nouveau commissaire enquêteur.,,Par suite, ne commet pas d'erreur de droit la cour administrative d'appel qui juge que la responsabilité de l'Etat ne pouvait être engagée en raison des irrégularités commises par le commissaire enquêteur lors de la mission qu'il a réalisée dans le cadre de l'élaboration du projet de plan local d'urbanisme de la commune.</ANA>
<ANA ID="9C"> 68-01-01-01-01-05 1) Il résulte des articles L. 123-6, L. 123-10 et R. 123-19 du code de l'urbanisme et des articles L.123-3, L. 123-4, L. 123-14, R. 123-10 et R. 123-22 du code de l'environnement que le plan local d'urbanisme soumis à enquête publique est élaboré à l'initiative et sous la responsabilité de la commune. Eu égard aux caractéristiques et aux finalités de sa mission, le commissaire enquêteur doit être regardé comme l'exerçant au titre d'une procédure conduite par la commune.... ...2) Si, à la date des faits en cause, aucune procédure n'était prévue pour permettre au maire, constatant une irrégularité dans le rapport ou les conclusions du commissaire enquêteur, d'en saisir le président du tribunal administratif, il lui appartenait en revanche de ne pas donner suite à une procédure entachée d'irrégularités et d'en tirer les conséquences en demandant soit au commissaire enquêteur de corriger ces irrégularités soit de mettre en oeuvre une nouvelle procédure en saisissant à nouveau le président du tribunal administratif pour qu'il procède à la désignation d'un nouveau commissaire enquêteur.,,3) Ne commet pas d'erreur de droit la cour administrative d'appel qui juge que la responsabilité de l'Etat ne pouvait être engagée en raison des irrégularités commises par le commissaire enquêteur lors de la mission qu'il a réalisée dans le cadre de l'élaboration du projet de plan local d'urbanisme de la commune.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
