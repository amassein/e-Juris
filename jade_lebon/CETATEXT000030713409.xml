<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030713409</ID>
<ANCIEN_ID>JG_L_2015_06_000000374303</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/71/34/CETATEXT000030713409.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème / 5ème SSR, 08/06/2015, 374303</TITRE>
<DATE_DEC>2015-06-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>374303</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème / 5ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MASSE-DESSEN, THOUVENIN, COUDRAY ; SCP CELICE, BLANCPAIN, SOLTNER, TEXIDOR</AVOCATS>
<RAPPORTEUR>M. Benjamin de Maillard</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Maud Vialettes</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:374303.20150608</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 30 décembre 2013 et 31 mars 2014 au secrétariat du contentieux du Conseil d'Etat, présentés pour Mme B...A..., demeurant ... ; Mme A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 12VE00771 du 9 avril 2013 par lequel la cour administrative d'appel de Versailles a, à la demande de la société KPMG, d'une part, annulé le jugement n° 0904170 du 22 décembre 2011 du tribunal administratif de Versailles annulant les décisions du 7 août 2008 de l'inspecteur du travail des Hauts-de-Seine autorisant sa mise en retraite et du 20 février 2009 du ministre chargé du travail rejetant son recours hiérarchique, et, d'autre part, rejeté sa demande de première instance ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de la société KPMG ;<br/>
<br/>
              3°) de mettre à la charge de la société KPMG une somme de 3 500 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code du travail ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Benjamin de Maillard, auditeur,  <br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Masse-Dessen, Thouvenin, Coudray, avocat de Mme A...et à la SCP Célice, Blancpain, Soltner, Texidor, avocat de la société KPMG ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis au juge du fond que la société KPMG a saisi l'autorité administrative d'une demande de mise en retraite de MmeA..., salariée protégée ; qu'une décision implicite de refus est née le 27 juillet 2008 du silence gardé par l'administration sur cette demande ; que, par une décision du 7 août 2008 faisant suite à l'enquête contradictoire conduite par l'inspecteur du travail de la 16ème section des Hauts-de-Seine, l'inspecteur du travail assurant par intérim le remplacement de ce dernier a retiré ce refus et accordé l'autorisation demandée ; que, par une décision du 20 février 2009, le ministre du travail, de l'emploi et de la solidarité a rejeté le recours hiérarchique introduit par Mme A...contre la décision de l'inspecteur du travail ; que par l'arrêt attaqué, la cour administrative d'appel de Versailles a, après avoir annulé le jugement du 22 décembre 2011 du tribunal administratif de Versailles, rejeté la demande d'annulation des décisions des 7 août 2008 et 20 février 2009 présentée par Mme A...; <br/>
<br/>
              Sur la régularité de l'arrêt attaqué :<br/>
<br/>
              2. Considérant que si, au cours de l'instance d'appel devant la cour administrative d'appel de Versailles, le mémoire en réplique du 11 juin 2012 de la société KPMG n'a pas été communiqué à la partie en défense, il ressort des pièces du dossier soumis aux juges du fond que, contrairement à ce que soutient MmeA..., ce mémoire ne contenait aucun moyen ou élément nouveau sur lesquels la cour se serait fondée pour prendre sa décision ; que, Mme A...n'est, par suite, pas fondée à soutenir que l'arrêt qu'elle attaque est intervenu à l'issue d'une procédure irrégulière ;<br/>
<br/>
              Sur le bien-fondé de l'arrêt attaqué :<br/>
<br/>
              3. Considérant qu'en vertu des dispositions du code du travail, les salariés légalement investis de fonctions représentatives bénéficient, dans l'intérêt de l'ensemble des travailleurs qu'ils représentent, d'une protection exceptionnelle ; qu'aux termes des dispositions de l'article L. 1237-5 de ce code : " La mise à la retraite s'entend de la possibilité donnée à l'employeur de rompre le contrat de travail d'un salarié ayant atteint l'âge mentionné au 1° de l'article L. 351-8 du code de la sécurité sociale. (...) " ; que, dans le cas où la demande de rupture du contrat de travail d'un salarié protégé est motivée par la survenance de l'âge, il appartient à l'autorité administrative de vérifier, sous le contrôle du juge de l'excès de pouvoir, d'une part, que la mesure envisagée n'est pas en rapport avec les fonctions représentatives exercées ou l'appartenance syndicale de l'intéressé et, d'autre part, que les conditions légales de mise à la retraite sont remplies ; que l'autorité administrative a également la faculté de refuser l'autorisation sollicitée pour des motifs d'intérêt général, sous réserve qu'une atteinte excessive ne soit pas portée à l'un ou l'autre des intérêts en présence ;<br/>
<br/>
              4. Considérant qu'à l'effet de concourir à la mise en oeuvre de la protection rappelée ci-dessus, les articles R. 2421-4 et R. 2421-1 du code du travail disposent que l'inspecteur du travail saisi d'une demande d'autorisation de licenciement d'un salarié protégé au titre d'un ou plusieurs mandats représentatifs procède à une enquête contradictoire ; que cette enquête ainsi que la décision prise à son issue relèvent de l'inspecteur du travail ayant compétence pour statuer sur la demande d'autorisation ; que la cessation de fonctions de l'inspecteur du travail ayant conduit l'enquête contradictoire n'impose pas, par elle-même, que l'inspecteur du travail assurant son remplacement ou lui ayant succédé procède à une nouvelle enquête contradictoire avant de statuer sur la demande,  même s'il lui est toujours loisible de le faire ;<br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que l'inspecteur du travail qui assurait l'intérim de l'inspecteur du travail de la 16ème section des Hauts-de-Seine pouvait se fonder sur l'enquête contradictoire réalisée par ce dernier pour statuer, ainsi qu'il l'a fait, sur la demande d'autorisation de licenciement déposée par la société KPMG ; que, par suite, la cour administrative d'appel n'a, en tout état de cause, pas commis d'erreur de droit en ne relevant pas d'office que l'auteur de la décision attaquée ne pouvait pas prendre cette décision faute d'avoir lui-même procédé, au préalable, à l'enquête contradictoire ; <br/>
<br/>
              6. Considérant que, pour répondre au moyen selon lequel le caractère implicite du refus du 27 juillet 2008 aurait fait, par lui-même, obstacle à son retrait par l'administration, la cour a pu, sans erreur de droit, juger qu'il appartenait, au contraire, à l'administration, dès lors qu'elle estimait devoir légalement autoriser la mise à la retraite de l'intéressée, de retirer dans le délai du recours contentieux, pour un motif de légalité, la décision implicite de refus précédemment prise ; qu'est sans incidence sur le bien-fondé de la réponse apportée par la cour au moyen soulevé par Mme A...la circonstance qu'elle a, à titre surabondant, retenu à tort que l'illégalité de la décision du 27 juillet 2008 se déduisait nécessairement de la légalité de l'autorisation du 7 août 2008 ;<br/>
<br/>
              7. Considérant que c'est par une appréciation souveraine, qui n'est pas entachée de dénaturation, que la cour a estimé qu'il ne ressortait pas des pièces du dossier que l'inspecteur du travail avait, au cours de son enquête contradictoire, été destinataire d'un document, relatif aux embauches compensant les départs en retraite, qu'il aurait omis de communiquer à Mme A...; <br/>
<br/>
              8. Considérant qu'il résulte de tout ce qui précède, et sans qu'il soit besoin de statuer sur la fin de non-recevoir opposée par la société KPMG, que Mme A...n'est pas fondée à demander l'annulation de l'arrêt attaqué ; <br/>
<br/>
              9. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise, à ce titre, à la charge de la société KPMG, qui n'est pas, dans la présente instance, la partie perdante ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées à ce même titre par cette société ;<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : Le pourvoi de Mme A...est rejeté.<br/>
<br/>
Article 2 : Les conclusions de la société KPMG présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 3 : La présente décision sera notifiée à Mme B...A..., à la société KPMG et au ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-03-03-03 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - FORME ET PROCÉDURE. PROCÉDURE CONTRADICTOIRE. MODALITÉS. - AUTORISATION DE LICENCIEMENT D'UN SALARIÉ PROTÉGÉ - ENQUÊTE CONTRADICTOIRE PRÉALABLE - CESSATION DE FONCTION DE L'INSPECTEUR DU TRAVAIL AYANT PROCÉDÉ À L'ENQUÊTE CONTRADICTOIRE - OBLIGATION DU SUCCESSEUR DE PROCÉDER À UNE NOUVELLE ENQUÊTE - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">66-07-01-03-02-01 TRAVAIL ET EMPLOI. LICENCIEMENTS. AUTORISATION ADMINISTRATIVE - SALARIÉS PROTÉGÉS. MODALITÉS DE DÉLIVRANCE OU DE REFUS DE L'AUTORISATION. MODALITÉS D'INSTRUCTION DE LA DEMANDE. ENQUÊTE CONTRADICTOIRE. - AUTORISATION DE LICENCIEMENT D'UN SALARIÉ PROTÉGÉ - ENQUÊTE CONTRADICTOIRE PRÉALABLE - CESSATION DE FONCTION DE L'INSPECTEUR DU TRAVAIL AYANT PROCÉDÉ À L'ENQUÊTE CONTRADICTOIRE - OBLIGATION DU SUCCESSEUR DE PROCÉDER À UNE NOUVELLE ENQUÊTE - ABSENCE.
</SCT>
<ANA ID="9A"> 01-03-03-03 Enquête contradictoire préalable à la décision de l'inspecteur du travail. La cessation de fonctions de l'inspecteur du travail initialement compétent pour statuer sur une demande d'autorisation et ayant conduit l'enquête contradictoire n'impose pas, par elle-même, que l'inspecteur du travail assurant son remplacement ou lui ayant succédé procède à une nouvelle enquête contradictoire avant de statuer sur la demande,  même s'il lui est toujours loisible de le faire.</ANA>
<ANA ID="9B"> 66-07-01-03-02-01 Enquête contradictoire préalable à la décision de l'inspecteur du travail statuant sur une demande d'autorisation de licencier un salarié protégé. La cessation de fonctions de l'inspecteur du travail initialement compétent pour statuer sur une demande d'autorisation et ayant conduit l'enquête contradictoire n'impose pas, par elle-même, que l'inspecteur du travail assurant son remplacement ou lui ayant succédé procède à une nouvelle enquête contradictoire avant de statuer sur la demande,  même s'il lui est toujours loisible de le faire.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
