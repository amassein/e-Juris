<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000024736674</ID>
<ANCIEN_ID>JG_L_2011_10_000000326492</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/24/73/66/CETATEXT000024736674.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Assemblée, 26/10/2011, 326492, Publié au recueil Lebon</TITRE>
<DATE_DEC>2011-10-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>326492</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Assemblée</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP TIFFREAU, CORLAY, MARLANGE ; SCP PEIGNOT, GARREAU ; SCP WAQUET, FARGE, HAZAN ; SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>Mme Constance Rivière</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier de Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CEASS:2011:326492.20111026</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 26 mars 2009 et 26 juin 2009 au secrétariat du contentieux du Conseil d'Etat, présentés pour la COMMUNE DE SAINT-DENIS, représentée par son maire ; la COMMUNE DE SAINT-DENIS demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 07VE01770 - 07VE01773 - 07VE01776 du 15 janvier 2009 par lequel la cour administrative d'appel de Versailles a rejeté ses requêtes tendant à l'annulation des jugements n° 0611566 - 0611690 - 0611695 du 31 mai 2007 par lesquels le tribunal administratif de Cergy-Pontoise a, statuant sur la demande de la société Bouygues Telecom, de la société Orange France et de la Société française de radiotéléphone (SFR), annulé l'arrêté municipal du 14 septembre 2006 interdisant l'installation des antennes de téléphonie mobile dans un rayon de 100 mètres autour des crèches, des établissements scolaires ou recevant un public mineur et des résidences de personnes âgées ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses requêtes d'appel ;<br/>
<br/>
              3°) de mettre à la charge des sociétés Orange France, Bouygues Telecom et SFR le versement de la somme de 2 000 euros chacune en application de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution et notamment la Charte de l'environnement à laquelle renvoie son Préambule ; <br/>
<br/>
              Vu la recommandation n° 1999/519/CE du 12 juillet 1999 du Conseil de l'Union européenne ; <br/>
<br/>
              Vu le code général des collectivités territoriales ;<br/>
<br/>
              Vu le code des postes et des communications électroniques ;<br/>
<br/>
              Vu le décret n° 2002-775 du 3 mai 2002 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Constance Rivière, Maître des Requêtes-rapporteur ;<br/>
<br/>
              - les observations de la SCP Waquet, Farge, Hazan, avocat de la COMMUNE DE SAINT-DENIS, de la SCP Tiffreau, Corlay, Marlange, avocat de la société Orange France, de la SCP Peignot, Garreau, avocat de la société Bouygues Telecom et de la SCP Piwnica, Molinié, avocat de la Société française du radiotéléphone ;<br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Waquet, Farge, Hazan, avocat de la COMMUNE DE SAINT-DENIS, à la SCP Tiffreau, Corlay, Marlange, avocat de la société Orange France, à la SCP Peignot, Garreau, avocat de la société Bouygues Telecom et à la SCP Piwnica, Molinié, avocat de la Société française du radiotéléphone ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que le maire de la commune de Saint-Denis, se fondant notamment sur le principe de précaution, a, par arrêté en date du 14 septembre 2006, interdit sur le territoire de la commune l'installation d'antennes de téléphonie mobile dans un rayon de 100 mètres autour des crèches, des établissements scolaires ou recevant un public mineur et des résidences de personnes âgées, de manière temporaire, jusqu'à la mise en place d'une charte entre les opérateurs de réseaux de communications électroniques et la communauté de communes de la Plaine Commune ; que, sur recours des sociétés Orange France, Bouygues Telecom et SFR, cet arrêté a été annulé par un jugement du tribunal administratif de Cergy-Pontoise en date du 31 mai 2007, confirmé le 15 janvier 2009 par un arrêt de la cour administrative d'appel de Versailles ; que la COMMUNE DE SAINT-DENIS se pourvoit en cassation contre cet arrêt ;<br/>
<br/>
              Considérant, en premier lieu, qu'en vertu du I de l'article L. 32-1 du code des postes et des communications électroniques, les activités de communications électroniques, si elles s'exercent librement, doivent respecter les autorisations prévues au titre II de ce code (" Ressources et police "), notamment celles relatives à l'utilisation des fréquences radioélectriques et l'implantation des stations radioélectriques de toute nature ; qu'en vertu du II de ce même article, le ministre chargé des communications électroniques et l'Autorité de régulation des communications électroniques et des postes (ARCEP) veillent notamment, dans le cadre de leurs attributions respectives, au respect de l'ordre public par les exploitants de réseaux de communications électroniques ainsi qu'à la gestion efficace des fréquences radioélectriques ; qu'en vertu de l'article L. 42-1 du même code, les autorisations d'utilisation des fréquences radioélectriques attribuées par l'ARCEP précisent les conditions techniques nécessaires " pour limiter l'exposition du public aux champs électromagnétiques " ; que l'article L. 43 du code donne mission à l'Agence nationale des fréquences (ANFR), établissement public administratif de l'Etat, notamment de coordonner " l'implantation sur le territoire national des stations radioélectriques de toute nature ", en autorisant ces implantations, et de veiller " au respect des valeurs limites d'exposition du public aux champs électromagnétiques " définies, en application de l'article L. 34-9-1 du même code, par le décret n° 2002-775 du 3 mai 2002, qui a repris les valeurs limites fixées par la recommandation du 12 juillet 1999 du Conseil de l'Union européenne relative à la limitation de l'exposition du public aux champs électromagnétiques (de 0 Hz à 300 Ghz) ; que ce décret impose à tout exploitant d'un réseau de communications électroniques de s'assurer que le niveau d'exposition du public aux champs électromagnétiques émis par les équipements et installations de son réseau respecte les valeurs limites définies en annexe ; qu'en particulier, il résulte de l'article 5 de ce décret que tout exploitant doit justifier, sur demande de l'ARCEP ou de l'ANFR, des actions engagées pour s'assurer, au sein des établissements scolaires, des crèches ou des établissements de soins situés dans un rayon de cent mètres à partir de l'équipement ou de l'installation, que l'exposition du public aux champs électromagnétiques est aussi faible que possible, tout en préservant la qualité du service rendu ; qu'en application des articles R. 20-44-10 et suivants du code, l'ANFR peut diligenter des vérifications sur place effectuées par des organismes répondant à des exigences de qualités fixées par décret et selon un protocole de mesure déterminé par arrêté ministériel ; qu'enfin, en vertu de l'article L. 96-1 du code, l'exploitant d'une installation radioélectrique sur le territoire d'une commune est tenu de transmettre au maire " sur sa demande, un dossier établissant l'état des lieux de cette ou de ces installations " ;<br/>
<br/>
              Considérant qu'il résulte de ces dispositions que le législateur a organisé une police spéciale des communications électroniques confiée à l'Etat ; qu'afin d'assurer, sur l'ensemble du territoire national et conformément au droit de l'Union européenne, d'une part, un niveau élevé et uniforme de protection de la santé publique contre les effets des ondes électromagnétiques émises par les réseaux de communications électroniques, qui sont identiques sur tout le territoire, d'autre part, un fonctionnement optimal de ces réseaux notamment par une couverture complète de ce territoire, le  législateur a confié aux seules autorités qu'il a désignées, c'est-à-dire au ministre chargé des communications électroniques, à l'ARCEP et à l'ANFR, le soin de déterminer, de manière complète, les modalités d'implantation des stations radioélectriques sur l'ensemble du territoire ainsi que les mesures  de protection du public contre les effets des ondes qu'elles émettent ; que les pouvoirs de police spéciale ainsi attribués aux autorités nationales, qui reposent sur un niveau d'expertise et peuvent être assortis de garanties indisponibles au plan local, sont conférés à chacune de ces autorités, notamment pour veiller, dans le cadre de leurs compétences respectives, à la limitation de l'exposition du public aux champs électromagnétiques et à la protection de la santé publique ; que, dans ces conditions, si le législateur a prévu par ailleurs que le maire serait informé à sa demande de l'état des installations radioélectriques exploitées sur le territoire de la commune et si les articles L. 2212-1 et L. 2212-2 du code général des collectivités territoriales habilitent le maire à prendre les mesures de police générale nécessaires au bon ordre, à la sûreté, à la sécurité et à la salubrité publiques, celui-ci ne saurait, sans porter atteinte aux pouvoirs de police spéciale conférés aux autorités de l'Etat, adopter sur le territoire de la commune une réglementation portant sur l'implantation des antennes relais de téléphonie mobile et destinée à protéger le public contre les effets des ondes émises par ces antennes ;<br/>
<br/>
              Considérant, en second lieu, qu'aux termes de l'article 5 de la Charte de l'environnement, à laquelle le Préambule de la Constitution fait référence en vertu de la loi constitutionnelle du 1er mars 2005 : " Lorsque la réalisation d'un dommage, bien qu'incertaine en l'état des connaissances scientifiques, pourrait affecter de manière grave et irréversible l'environnement, les autorités publiques veillent, par application du principe de précaution et dans leurs domaines d'attributions, à la mise en oeuvre de procédures d'évaluation des risques et à l'adoption de mesures provisoires et proportionnées afin de parer à la réalisation du dommage " ; qu'il résulte de ces dispositions que le principe de précaution, s'il est applicable à toute autorité publique dans ses domaines d'attributions, ne saurait avoir ni pour objet ni pour effet de permettre à une autorité publique d'excéder son champ de compétence et d'intervenir en dehors de ses domaines d'attributions ; que, par conséquent, la circonstance que les valeurs limites d'exposition du public aux champs électromagnétiques fixées au niveau national ne prendraient pas suffisamment en compte les exigences posées par le principe de précaution n'habilite pas davantage les maires à adopter une réglementation locale portant sur l'implantation des antennes relais de téléphonie mobile et destinée à protéger le public contre les effets des ondes émises par ces antennes  ;<br/>
<br/>
              Considérant qu'il résulte de tout ce qui précède qu'en ne relevant pas l'incompétence du maire pour adopter, au titre de ses pouvoirs de police générale, y compris en se fondant sur le principe de précaution, un arrêté portant sur l'implantation des antennes relais de téléphonie mobile dans la commune et destiné à protéger le public contre les effets des ondes émises par ces antennes, la cour administrative d'appel de Versailles a entaché son arrêt d'une erreur de droit ; que, dès lors, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, l'arrêt attaqué doit être annulé ; <br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              Considérant qu'ainsi qu'il a été dit ci-dessus, le maire ne peut, ni au titre de ses pouvoirs de police générale ni en se fondant sur le principe de précaution, adopter une réglementation portant sur l'implantation des antennes relais de téléphonie mobile et destinée à protéger le public contre les effets des ondes émises par ces antennes ; que le maire de la COMMUNE DE SAINT-DENIS ne pouvait, par conséquent, pas légalement édicter une telle réglementation sur le territoire de la commune ;<br/>
<br/>
              Considérant que, compte-tenu de ce qui précède, la négociation en cours d'une charte entre la communauté de communes de la Plaine Commune et les opérateurs de téléphonie mobile ne pouvait pas non plus être utilement invoquée par le maire de la COMMUNE DE SAINT-DENIS pour justifier l'adoption de l'arrêté attaqué ;<br/>
<br/>
              Considérant qu'il résulte de ce qui précède que la COMMUNE DE SAINT-DENIS n'est pas fondée à se plaindre de ce que, par le jugement attaqué, le tribunal administratif de Cergy-Pontoise a annulé l'arrêté en date du 14 septembre 2006 ; <br/>
<br/>
              Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mis à la charge des sociétés Orange France, Bouygues Telecom et SFR, qui ne sont pas, dans la présente instance, les parties perdantes, le versement d'une somme au titre des frais exposés par la COMMUNE DE SAINT-DENIS et non compris dans les dépens ; qu'il y a lieu, en revanche, de mettre à la charge de la COMMUNE DE SAINT-DENIS le versement à la société Bouygues Telecom et à la société Orange France de la même somme de 4 000 euros chacune au titre des frais exposés devant le Conseil d'Etat et devant la cour administrative d'appel de Versailles et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
        D E C I D E :<br/>
                                                                         --------------<br/>
Article 1er : L'arrêt du 15 janvier 2009 de la cour administrative d'appel de Versailles est annulé.<br/>
Article 2 : Les requêtes de la COMMUNE DE SAINT-DENIS devant la cour administrative d'appel de Versailles ainsi que le surplus des conclusions de son pourvoi sont rejetés. <br/>
<br/>
Article 3 : La COMMUNE DE SAINT-DENIS versera à la société Orange France et à la société Bouygues Telecom la même somme de 4 000 euros chacune au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la COMMUNE DE SAINT-DENIS, à la Société française du radiotéléphone, à la société Orange France et à la société Bouygues Telecom.<br/>
<br/>
Une copie en sera adressée au ministre de l'écologie, du développement durable, des transports et du logement, au ministre de l'économie, des finances et de l'industrie et au ministre du travail, de l'emploi et de la santé. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">44-005-05 NATURE ET ENVIRONNEMENT. - INCIDENCE SUR LA RÉPARTITION DES COMPÉTENCES ENTRE LES AUTORITÉS PUBLIQUES - ABSENCE [RJ2].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">49-04 POLICE ADMINISTRATIVE. POLICE GÉNÉRALE. - POUVOIRS DE POLICE GÉNÉRALE DU MAIRE - ARTICULATION AVEC LA POLICE SPÉCIALE DES COMMUNICATIONS ÉLECTRONIQUES CONFÉRÉE AUX AUTORITÉS DE L'ETAT PAR LA LOI - CONSÉQUENCES [RJ1].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">49-05 POLICE ADMINISTRATIVE. POLICES SPÉCIALES. - POLICE SPÉCIALE DES COMMUNICATIONS ÉLECTRONIQUES - 1) POLICE SPÉCIALE CONFIÉE PAR LE LÉGISLATEUR AUX AUTORITÉS DE L'ETAT - 2) CONSÉQUENCES SUR LES COMPÉTENCES DES MAIRES AU TITRE DE LEUR POUVOIR DE POLICE GÉNÉRALE [RJ1].
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">51 POLICE ADMINISTRATIVE. POLICES SPÉCIALES. - POLICE SPÉCIALE DES COMMUNICATIONS ÉLECTRONIQUES - 1) POLICE SPÉCIALE CONFIÉE PAR LE LÉGISLATEUR AUX AUTORITÉS DE L'ETAT - 2) CONSÉQUENCES SUR LES COMPÉTENCES DES MAIRES AU TITRE DE LEUR POUVOIR DE POLICE GÉNÉRALE [RJ1].
</SCT>
<ANA ID="9A"> 44-005-05 Il résulte des dispositions de l'article 5 de la Charte de l'environnement que le principe de précaution, s'il est applicable à toute autorité publique dans ses domaines d'attributions, ne saurait avoir ni pour objet ni pour effet de permettre à une autorité publique d'excéder son champ de compétence et d'intervenir en dehors de ses domaines d'attributions.</ANA>
<ANA ID="9B"> 49-04 Par les dispositions figurant aux articles L. 32-1, L. 34-9-1, L. 34-9-2, L. 42-1 et L. 43 du code des postes et communications électroniques, le législateur a organisé de manière complète une police spéciale des communications électroniques confiée à l'Etat. Afin d'assurer, sur l'ensemble du territoire national et conformément au droit de l'Union européenne, d'une part, un niveau élevé et uniforme de protection de la santé publique contre les effets des ondes électromagnétiques émises par les réseaux de communications électroniques, qui sont identiques sur tout le territoire, d'autre part, un fonctionnement optimal de ces réseaux notamment par une couverture complète de ce territoire, le législateur a confié aux seules autorités qu'il a désignées le soin de déterminer, de manière complète, les modalités d'implantation des stations radioélectriques sur l'ensemble du territoire ainsi que les mesures  de protection du public contre les effets des ondes qu'elles émettent.,,Les pouvoirs de police spéciale ainsi attribués aux autorités nationales, qui reposent sur un niveau d'expertise et peuvent être assortis de garanties indisponibles au plan local, sont conférés à chacune de ces autorités, notamment pour veiller, dans le cadre de leurs compétences respectives, à la limitation de l'exposition du public aux champs électromagnétiques et à la protection de la santé publique. Si le législateur a par ailleurs prévu que le maire serait informé, à sa demande, de l'état des installations radioélectriques exploitées sur le territoire de la commune, et si les articles L. 2212-1 et L. 2212-2 du code général des collectivités territoriales habilitent le maire à prendre les mesures de police générale nécessaires au bon ordre, à la sûreté, à la sécurité et à la salubrité publiques, celui-ci ne saurait, sans porter atteinte aux pouvoirs de police spéciale conférés aux autorités de l'Etat, adopter sur le territoire de la commune, une réglementation relative à l'implantation des antennes relais de téléphonie mobile et destinée à protéger le public contre les effets des ondes émises par ces antennes.</ANA>
<ANA ID="9C"> 49-05 1) Par les dispositions figurant aux articles L. 32-1, L. 34-9-1, L. 34-9-2, L. 42-1 et L. 43 du code des postes et communications électroniques, le législateur a organisé de manière complète une police spéciale des communications électroniques confiée à l'Etat. Afin d'assurer, sur l'ensemble du territoire national et conformément au droit de l'Union européenne, d'une part, un niveau élevé et uniforme de protection de la santé publique contre les effets des ondes électromagnétiques émises par les réseaux de communications électroniques, qui sont identiques sur tout le territoire, d'autre part, un fonctionnement optimal de ces réseaux notamment par une couverture complète de ce territoire, le législateur a confié aux seules autorités qu'il a désignées le soin de déterminer, de manière complète, les modalités d'implantation des stations radioélectriques sur l'ensemble du territoire ainsi que les mesures  de protection du public contre les effets des ondes qu'elles émettent.,,Les pouvoirs de police spéciale ainsi attribués aux autorités nationales, qui reposent sur un niveau d'expertise et peuvent être assortis de garanties indisponibles au plan local, sont conférés à chacune de ces autorités, notamment pour veiller, dans le cadre de leurs compétences respectives, à la limitation de l'exposition du public aux champs électromagnétiques et à la protection de la santé publique.,,2) Si le législateur a par ailleurs prévu que le maire serait informé, à sa demande, de l'état des installations radioélectriques exploitées sur le territoire de la commune, et si les articles L. 2212-1 et L. 2212-2 du code général des collectivités territoriales habilitent le maire à prendre les mesures de police générale nécessaires au bon ordre, à la sûreté, à la sécurité et à la salubrité publiques, celui-ci ne saurait, sans porter atteinte aux pouvoirs de police spéciale conférés aux autorités de l'Etat, adopter sur le territoire de la commune, une réglementation relative à l'implantation des antennes relais de téléphonie mobile et destinée à protéger le public contre les effets des ondes émises par ces antennes.</ANA>
<ANA ID="9D"> 51 1) Par les dispositions figurant aux articles L. 32-1, L. 34-9-1, L. 34-9-2, L. 42-1 et L. 43 du code des postes et communications électroniques, le législateur a organisé de manière complète une police spéciale des communications électroniques confiée à l'Etat. Afin d'assurer, sur l'ensemble du territoire national et conformément au droit de l'Union européenne, d'une part, un niveau élevé et uniforme de protection de la santé publique contre les effets des ondes électromagnétiques émises par les réseaux de communications électroniques, qui sont identiques sur tout le territoire, d'autre part, un fonctionnement optimal de ces réseaux notamment par une couverture complète de ce territoire, le législateur a confié aux seules autorités qu'il a désignées le soin de déterminer, de manière complète, les modalités d'implantation des stations radioélectriques sur l'ensemble du territoire ainsi que les mesures  de protection du public contre les effets des ondes qu'elles émettent.,,Les pouvoirs de police spéciale ainsi attribués aux autorités nationales, qui reposent sur un niveau d'expertise et peuvent être assortis de garanties indisponibles au plan local, sont conférés à chacune de ces autorités, notamment pour veiller, dans le cadre de leurs compétences respectives, à la limitation de l'exposition du public aux champs électromagnétiques et à la protection de la santé publique.,,2) Si le législateur a par ailleurs prévu que le maire serait informé, à sa demande, de l'état des installations radioélectriques exploitées sur le territoire de la commune, et si les articles L. 2212-1 et L. 2212-2 du code général des collectivités territoriales habilitent le maire à prendre les mesures de police générale nécessaires au bon ordre, à la sûreté, à la sécurité et à la salubrité publiques, celui-ci ne saurait, sans porter atteinte aux pouvoirs de police spéciale conférés aux autorités de l'Etat, adopter sur le territoire de la commune, une réglementation relative à l'implantation des antennes relais de téléphonie mobile et destinée à protéger le public contre les effets des ondes émises par ces antennes.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Voir aussi décisions du même jour, CE, Assemblée, 26 octobre 2011, Société française de radiotéléphone, n°s 341767 341768 et Commune des Pennes-Mirabeau, n° 329904.,,[RJ2] Rappr., Cons. Const., 19 juin 2008, décision n° 2008-564 DC, loi relative aux organismes génétiquement modifiés.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
