<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039258803</ID>
<ANCIEN_ID>JG_L_2019_09_000000428508</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/25/88/CETATEXT000039258803.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 4ème chambres réunies, 25/09/2019, 428508</TITRE>
<DATE_DEC>2019-09-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>428508</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SEVAUX, MATHONNET ; SCP GATINEAU, FATTACCINI</AVOCATS>
<RAPPORTEUR>M. Damien Pons</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:428508.20190925</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Le comité central d'entreprise de l'Agence nationale pour la formation professionnelle des adultes (AFPA) a demandé au juge des référés du tribunal administratif de Montreuil, sur le fondement de l'article L. 521-3 du code de justice administrative, d'ordonner au conseil d'administration de l'AFPA, d'une part, d'engager la négociation d'un contrat d'objectifs et de performance avec la ministre du travail et le ministre de l'économie et des finances afin de fixer les objectifs de politique publique assignés à cette agence, le pilotage stratégique, ses objectifs et ses priorités ainsi que les modalités selon lesquelles ceux-ci sont suivis et établis par l'Etat, d'autre part, de suspendre toute décision susceptible de dépendre du pilotage stratégique, des objectifs et des priorités déterminés par le contrat d'objectifs et de performances jusqu'au terme des négociations entre l'Etat et l'agence. Par une ordonnance n° 1900871 du 12 février 2019, le juge des référés du tribunal administratif de Montreuil a rejeté cette demande. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 28 février, 15 mars et 17 juin 2019 au secrétariat du contentieux du Conseil d'Etat, le comité central d'entreprise de l'AFPA demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'AFPA la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code du travail ;<br/>
              - l'ordonnance n° 2016-1519 du 10 novembre 2016 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Damien Pons, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Sevaux, Mathonnet, avocat du comité central d'entreprise de l'Agence nationale pour la formation professionnelle des adultes et à la SCP Gatineau, Fattaccini, avocat de l'Agence nationale pour la formation professionnelle des adultes ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. L'Agence nationale pour la formation professionnelle des adultes (AFPA) est, en vertu de l'article L. 5315-1 du code du travail, dans sa rédaction issue de l'ordonnance du 10 novembre 2016 entrée en vigueur le 1er janvier 2017, un établissement public de l'Etat à caractère industriel et commercial, contribuant au service public de l'emploi, qui a été substitué par l'article 3 de l'ordonnance du 10 novembre 2016 à l'Association nationale pour la formation professionnelle des adultes. Il ressort des pièces du dossier soumis au juge des référés du tribunal administratif de Montreuil qu'un projet de réorganisation et un projet de plan de sauvegarde de l'emploi ont été présentés au comité central d'entreprise de l'AFPA le 7 novembre 2018, le projet de réorganisation conduisant à envisager la création de 603 emplois, la modification de 454 emplois et la suppression de 1 541 emplois avant le 31 mars 2020. Saisi par le comité central d'entreprise d'une demande tendant, sur le fondement de l'article L. 521-3 du code de justice administrative, à ce qu'il enjoigne au conseil d'administration de l'AFPA d'engager la négociation d'un contrat d'objectifs et de performance avec le ministre du travail et le ministre de l'économie et suspende toutes décisions susceptibles d'en dépendre jusqu'au terme des négociations entre l'Etat et l'AFPA, le juge des référés l'a, par une ordonnance rendue en application de l'article L. 522-3 du code de justice administrative, rejetée comme portée devant un ordre de juridiction incompétent pour en connaître. Le comité central d'entreprise de l'AFPA se pourvoit en cassation contre cette ordonnance.<br/>
<br/>
              2. Ainsi qu'il a été dit, l'Agence nationale pour la formation professionnelle des adultes est, en vertu de l'article L. 5315-1 du code du travail, un établissement public de l'Etat. L'article R. 5315-1 du même code précise qu'il est placé sous la tutelle conjointe des ministres chargés de l'emploi, de la formation professionnelle et du budget. L'article R. 5315-3 du même code dispose que : " Le conseil d'administration (...) délibère notamment sur : / 1° Les orientations annuelles et pluriannuelles, notamment celles prévues dans le contrat d'objectifs et de performance signé entre l'Etat et l'établissement public, représenté, sur son autorisation, par le président et le directeur général (...) ". <br/>
<br/>
              3. Il résulte des dispositions précitées que le " contrat d'objectifs et de performance " a notamment pour objet de formaliser les relations entre un établissement public de l'Etat et son autorité de tutelle. Un tel acte relève, par nature, de la compétence du juge administratif. En jugeant que la demande du comité central d'entreprise de l'AFPA, tendant à ce qu'il soit enjoint à l'établissement d'engager avec l'Etat l'élaboration d'un tel document, était manifestement insusceptible de se rattacher à un litige relevant de la juridiction administrative, le juge des référés a commis une erreur de droit. Le requérant est par suite fondé, sans qu'il soit besoin d'examiner l'autre moyen de son pourvoi, à demander, pour ce motif, l'annulation de l'ordonnance qu'il attaque.<br/>
<br/>
              4. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au titre de la procédure de référé engagée, en application de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              5. Aux termes de l'article L. 511-1 du code de justice administrative : " Le juge des référés statue par des mesures qui présentent un caractère provisoire. Il n'est pas saisi du principal et se prononce dans les meilleurs délais ". Aux termes de l'article L. 521-3 de ce même code : " En cas d'urgence et sur simple requête qui sera recevable même en l'absence de décision administrative préalable, le juge des référés peut ordonner toutes autres mesures utiles sans faire obstacle à l'exécution d'aucune décision administrative ". Il résulte de ces dispositions que, saisi d'une demande présentée sur ce fondement qui n'est pas manifestement insusceptible de se rattacher à un litige relevant de la compétence du juge administratif, le juge des référés peut prescrire, à des fins conservatoires ou à titre provisoire, toutes mesures que l'urgence justifie, notamment sous forme d'injonctions adressées à l'administration, à la condition que ces mesures soient utiles et ne se heurtent à aucune contestation sérieuse. En raison du caractère subsidiaire du référé régi par l'article L. 521-3, le juge saisi sur ce fondement ne peut prescrire les mesures qui lui sont demandées lorsque leurs effets pourraient être obtenus par les procédures de référé régies par les articles L. 521-1 et L 521-2. Si le juge des référés, saisi sur le fondement de l'article L. 521-3, ne saurait faire obstacle à l'exécution d'une décision administrative, même celle refusant la mesure demandée, à moins qu'il ne s'agisse de prévenir un péril grave, la circonstance qu'une décision administrative refusant la mesure demandée au juge des référés intervienne postérieurement à sa saisine ne saurait faire obstacle à ce qu'il fasse usage des pouvoirs qu'il tient de l'article L. 521-3.<br/>
<br/>
              6. Si le comité central d'entreprise de l'AFPA fait valoir que la conclusion entre l'Etat et l'AFPA d'un contrat d'objectifs et de performance serait particulièrement nécessaire pour que la stratégie et les objectifs de l'établissement soient, dans le contexte de sa réorganisation mentionné au point 1, déterminés avec précision, l'engagement par un établissement public de " négociations " avec son autorité de tutelle en vue de la conclusion d'un contrat d'objectifs et de performance ne relève pas des mesures conservatoires ou provisoires que le juge des référés est susceptible d'ordonner sur le fondement des dispositions précitées de l'article L 521-3 du code de justice administrative. Au surplus, une telle demande serait, en vertu des articles L. 1233-57-5 et L. 1235-7-1 du code du travail, irrecevable si, destinée à obtenir de l'employeur des éléments d'information, elle était présentée au juge des référés dans le cadre de la consultation du comité d'entreprise, prévue par l'article L. 1233-30 du même code, sur un projet de licenciement collectif donnant lieu à l'établissement d'un plan de sauvegarde de l'emploi.<br/>
<br/>
              7. Il en résulte que les conclusions présentées par le requérant ne peuvent qu'être rejetées. Il en va de même, par suite, de celles qu'il présente au titre de l'article L. 761-1 du code de justice administrative. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge du comité central d'entreprise de l'AFPA la somme que demande l'AFPA au titre des mêmes dispositions.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du juge des référés du tribunal administratif de Montreuil du 12 février 2019 est annulée. <br/>
<br/>
Article 2 : La demande présentée par le comité central d'entreprise de l'Agence nationale pour la formation professionnelle des adultes devant le juge des référés du tribunal administratif de Montreuil est rejetée.<br/>
<br/>
Article 3 : Les conclusions présentées par les parties au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 4 : La présente décision sera notifiée au comité central d'entreprise de l'Agence nationale pour la formation professionnelle des adultes et à l'Agence nationale pour la formation professionnelle des adultes.<br/>
Copie en sera adressée à la ministre du travail.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">17-03 COMPÉTENCE. RÉPARTITION DES COMPÉTENCES ENTRE LES DEUX ORDRES DE JURIDICTION. - CONTRAT D'OBJECTIFS ET DE PERFORMANCE (ART. R. 5315-3 DU CODE DU TRAVAIL) - ACTE RELEVANT PAR NATURE DE LA COMPÉTENCE DU JUGE ADMINISTRATIF.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-035-04 PROCÉDURE. PROCÉDURES INSTITUÉES PAR LA LOI DU 30 JUIN 2000. RÉFÉRÉ TENDANT AU PRONONCÉ DE TOUTES MESURES UTILES (ART. L. 521-3 DU CODE DE JUSTICE ADMINISTRATIVE). - MESURES QUE LE JUGE DES RÉFÉRÉS MESURES UTILES EST SUSCEPTIBLE D'ORDONNER - ENGAGEMENT PAR UN ÉTABLISSEMENT PUBLIC DE NÉGOCIATIONS AVEC SON AUTORITÉ DE TUTELLE EN VUE DE LA CONCLUSION D'UN CONTRAT D'OBJECTIFS ET DE PERFORMANCE - EXCLUSION.
</SCT>
<ANA ID="9A"> 17-03 Il résulte de l'article R. 5315-3 du code du travail que le contrat d'objectifs et de performance a notamment pour objet de formaliser les relations entre un établissement public de l'Etat et son autorité de tutelle. Un tel acte relève, par nature, de la compétence du juge administratif.</ANA>
<ANA ID="9B"> 54-035-04 Si le comité central d'entreprise de l'Agence nationale pour la formation professionnelle des adultes (AFPA) fait valoir que la conclusion entre l'Etat et l'AFPA d'un contrat d'objectifs et de performance serait particulièrement nécessaire pour que la stratégie et les objectifs de l'établissement soient, dans un contexte de réorganisation de cet établissement public, déterminés avec précision, l'engagement par un établissement public de négociations avec son autorité de tutelle en vue de la conclusion d'un contrat d'objectifs et de performance ne relève pas des mesures conservatoires ou provisoires que le juge des référés est susceptible d'ordonner sur le fondement des dispositions précitées de l'article L 521-3 du code de justice administrative (CJA).</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
