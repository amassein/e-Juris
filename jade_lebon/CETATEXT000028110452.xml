<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028110452</ID>
<ANCIEN_ID>JG_L_2013_10_000000352561</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/11/04/CETATEXT000028110452.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère / 6ème SSR, 23/10/2013, 352561</TITRE>
<DATE_DEC>2013-10-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>352561</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère / 6ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARTHELEMY, MATUCHANSKY, VEXLIARD ; DELAMARRE</AVOCATS>
<RAPPORTEUR>M. Jean Lessi</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2013:352561.20131023</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la requête sommaire et le mémoire complémentaire, enregistrés les 9 septembre et 9 décembre 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour la Fédération des entreprises de boulangeries et pâtisseries françaises, dont le siège est 34, quai de Loire à Paris (75019), représentée par son président, pour la société New Providence, dont le siège est 1, place Ernest Delibes à Marseille (13008), représentée par son gérant, et pour la société COBAP, dont le siège est 2, boulevard du Redon à Marseille (13009), représentée par son gérant ; les requérantes demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite par laquelle le ministre du travail, de l'emploi et de la santé a rejeté leur demande tendant à l'abrogation de l'arrêté du 14 décembre 1996 du préfet des Bouches-du-Rhône ordonnant la fermeture hebdomadaire de tous les établissements et annexes d'établissements du département dont la vente au détail de pain et viennoiseries constitue l'activité unique ou l'une des deux activités principales ;<br/>
<br/>
              2°) d'enjoindre au ministre chargé du travail de procéder à l'abrogation de l'arrêté litigieux dans un délai d'un mois à compter de la notification de la décision à intervenir ou, à titre subsidiaire, de procéder à une consultation de l'ensemble des professionnels intéressés dans un délai de trois mois, dans les deux cas sous astreinte de 5 000 euros par jour de retard ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat une somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code du travail ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean Lessi,  <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Barthélemy, Matuchansky, Vexliard, avocat de la Fédération des entreprises de boulangeries et pâtisseries françaises, de la société New Providence et de la société COBAP ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article L. 3132-29 du code du travail, reprenant les dispositions du premier alinéa de l'ancien article L. 221-17 : " Lorsqu'un accord est intervenu entre les organisations syndicales de salariés et les organisations d'employeurs d'une profession et d'une zone géographique déterminées sur les conditions dans lesquelles le repos hebdomadaire est donné aux salariés, le préfet peut, par arrêté, sur la demande des syndicats intéressés, ordonner la fermeture au public des établissements de la profession ou de la zone géographique concernée pendant toute la durée de ce repos. Ces dispositions ne s'appliquent pas aux activités dont les modalités de fonctionnement et de paiement sont automatisées " ; que la fermeture au public des établissements d'une profession ne peut légalement être ordonnée sur la base d'un accord syndical que dans la mesure où cet accord correspond pour la profession à la volonté de la majorité indiscutable de tous ceux qui exercent cette profession à titre principal ou accessoire et dont l'établissement ou partie de celui-ci est susceptible d'être fermé ; qu'aux termes de l'article R. 3132-22 du même code : " Lorsqu'un arrêté préfectoral de fermeture au public, pris en application de l'article L. 3132-29, concerne des établissements concourant d'une façon directe à l'approvisionnement de la population en denrées alimentaires, il peut être abrogé ou modifié par le ministre chargé du travail après consultation des organisations professionnelles intéressées. / Cette décision ne peut intervenir qu'après l'expiration d'un délai de six mois à compter de la mise en application de l'arrêté préfectoral " ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier que le préfet des Bouches-du-Rhône a, par un arrêté du 4 décembre 1996 intervenu à la suite d'un accord conclu le 1er septembre 1996 entre certains syndicats d'employeurs et de travailleurs concernés, prescrit la fermeture, un jour par semaine, de tous les établissements ou annexes d'établissements implantés dans le département dont la vente au détail de pain et viennoiseries constitue l'activité unique ou l'une des deux activités principales ; que la Fédération des entreprises de boulangeries et pâtisseries françaises et autres demandent l'annulation de la décision implicite par laquelle le ministre du travail, de la solidarité et de la fonction publique a refusé d'abroger cet arrêté ;<br/>
<br/>
              Sur la légalité de la décision attaquée :<br/>
<br/>
              3. Considérant que l'autorité compétente, saisie d'une demande tendant à  l'abrogation d'un règlement illégal, est tenue d'y déférer, soit que ce règlement ait été illégal dès sa signature, soit que l'illégalité résulte de circonstances de droit ou de fait postérieures à cette date ;<br/>
<br/>
              4. Considérant que, comme le soutiennent la Fédération des entreprises de boulangeries et pâtisseries françaises et autres, les établissements ou annexes d'établissement ayant pour activité accessoire la vente au détail de pain et viennoiseries n'entrent pas dans les prévisions de l'arrêté litigieux, alors qu'il est constant qu'ils se trouvent placés, sur ce marché, en concurrence directe avec les établissements concernés par l'obligation de fermeture hebdomadaire et que cette obligation, si elle leur était étendue, ne concernerait que la partie de ces établissements ayant cette activité ; que le ministre ne fait valoir aucun élément de nature à justifier cette différence de traitement au regard de l'objectif de préservation des conditions du libre jeu de la concurrence entre établissements exerçant une même profession poursuivi par l'article L. 3132-29 ; que, dans ces conditions, les requérantes sont fondées à soutenir que le préfet a fait une inexacte application de ces dispositions et que l'arrêté litigieux était, pour ce motif, illégal dès sa signature ; <br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que, sans qu'il soit besoin d'examiner les autres moyens de la requête, la Fédération des entreprises de boulangeries et pâtisseries françaises et autres sont fondées à demander l'annulation de la décision qu'elles attaquent ;<br/>
<br/>
              Sur les conclusions à fin d'injonction :<br/>
<br/>
              6. Considérant qu'aux termes de l'article L. 911-2 du code de justice administrative : " Lorsque sa décision implique nécessairement qu'une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public prenne à nouveau une décision après une nouvelle instruction, la juridiction, saisie de conclusions en ce sens, prescrit, par la même décision juridictionnelle, que cette nouvelle décision doit intervenir dans un délai déterminé "<br/>
<br/>
              7. Considérant que l'annulation de la décision refusant d'abroger l'arrêté du 4 décembre 1996 du préfet des Bouches-du-Rhône implique seulement que la demande d'abrogation présentée par la Fédération des entreprises de boulangeries et pâtisseries française, la société New Providence et la société COBAP soit réexaminée, afin que le ministre remédie à l'illégalité dont cet arrêté est entaché, soit en l'abrogeant, soit en en modifiant le champ d'application de manière à le rendre conforme aux exigences rappelées au point 4 ; qu'il y a lieu, par suite, d'enjoindre au ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social de procéder à ce réexamen dans un délai de trois mois à compter de la notification de la présente décision ; que, dans les circonstances de l'espèce, il n'y a pas lieu d'assortir cette injonction d'une astreinte ;<br/>
<br/>
              Sur l'application de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              8. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme globale de 3 000 euros à verser à la Fédération des entreprises de boulangeries et pâtisseries françaises et autres au titre des dispositions de cet article ; qu'en revanche, ces dispositions font obstacle à ce que soit mise à la charge de la Fédération des entreprises de boulangeries et pâtisseries françaises, de la société New Providence et de la société COBAP, qui ne sont pas les parties perdantes, la somme que l'Union départementale des syndicats des maîtres-artisans boulangers des Bouches-du-Rhône demande au même titre ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La décision implicite par laquelle ministre du travail, de la solidarité et de la fonction publique a refusé d'abroger l'arrêté du 4 décembre 1996 du préfet des Bouches-du-Rhône est annulée.<br/>
Article 2 : Il est enjoint au ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social de procéder au réexamen de la demande de la Fédération des entreprises de boulangeries et pâtisseries française, de la société New Providence et de la société COBAP dans un délai de trois mois à compter de la notification de la présente décision.<br/>
Article 3 : L'Etat versera à la Fédération des entreprises de boulangeries et pâtisseries françaises et autres une somme globale de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions de l'Union départementale des syndicats des maîtres-artisans boulangers des Bouches-du-Rhône présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : Le surplus des conclusions de la requête de la Fédération des entreprises de boulangeries et pâtisseries françaises et autres est rejeté.<br/>
Article 6 : La présente décision sera notifiée à la Fédération des entreprises de boulangeries et pâtisseries françaises, à la société New Providence, à la société COBAP, au syndicat CGT du personnel des boulangeries pâtisseries artisanales des Bouches-du-Rhône, à l'Union départementale des syndicats des maîtres artisans boulangers des Bouches-du-Rhône et au ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social.<br/>
Copie en sera adressée pour information au syndicat général agroalimentaire CFDT des Bouches-du-Rhône, à l'Union départementale CFTC des Bouches-du-Rhône et à l'Union départementale FO des Bouches-du-Rhône. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">66-03-02-02 TRAVAIL ET EMPLOI. CONDITIONS DE TRAVAIL. REPOS HEBDOMADAIRE. FERMETURE HEBDOMADAIRE DES ÉTABLISSEMENTS. - ARRÊTÉ PRÉFECTORAL ORDONNANT LA FERMETURE DES COMMERCES LORSQU'UN ACCORD EST INTERVENU ENTRE SYNDICATS D'EMPLOYEURS ET DE TRAVAILLEURS DANS UNE PROFESSION DÉTERMINÉE (ANC. ART. L. 221-17, DÉSORMAIS L. 3132-29 DU CODE DU TRAVAIL) - FERMETURE NE CONCERNANT QUE LES ÉTABLISSEMENTS EXERÇANT L'ACTIVITÉ CONCERNÉE À TITRE PRINCIPAL, À L'EXCLUSION DES ÉTABLISSEMENTS L'EXERÇANT À TITRE ACCESSOIRE - 1) ILLÉGALITÉ - EXISTENCE - 2) REFUS DU MINISTRE D'ABROGER L'ARRÊTÉ - ANNULATION - INJONCTION - RÉEXAMEN DE LA DEMANDE D'ABROGATION.
</SCT>
<ANA ID="9A"> 66-03-02-02 Arrêté ordonnant la fermeture de commerces exerçant une activité à titre principal, mais pas des commerces exerçant la même activité à titre accessoire, alors que ces derniers se trouvent placés, sur le marché en cause, en concurrence directe avec les établissements concernés par l'obligation de fermeture hebdomadaire et que cette obligation, si elle leur était étendue, ne concernerait que la partie de ces établissements ayant cette activité.... ,,1) Dès lors que l'administration ne fait valoir aucun élément de nature à justifier cette différence de traitement au regard de l'objectif de préservation des conditions du libre jeu de la concurrence entre établissements exerçant une même profession poursuivi par l'article L. 3132-29 du code du travail, annulation de l'arrêté litigieux qui méconnait ces dispositions.,,,2) L'annulation pour ce motif de la décision refusant d'abroger un tel arrêté implique seulement que la demande d'abrogation soit réexaminée, afin que le ministre remédie à l'illégalité dont l'arrêté est entaché, soit en l'abrogeant, soit en en modifiant le champ d'application de manière à le rendre conforme aux exigences rappelées ci-dessus.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
