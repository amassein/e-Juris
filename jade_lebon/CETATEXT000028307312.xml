<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028307312</ID>
<ANCIEN_ID>JG_L_2013_12_000000369460</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/30/73/CETATEXT000028307312.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème / 2ème SSR, 06/12/2013, 369460</TITRE>
<DATE_DEC>2013-12-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>369460</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème / 2ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOULLOCHE</AVOCATS>
<RAPPORTEUR>Mme Natacha Chicot</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:369460.20131206</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 18 juin et 3 juillet 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour la société Michel Beauvais et associés, dont le siège est 3 rue Charles Weiss à Paris (75015), la société Ion Cindea, dont le siège est 2 rue Georges Raveneau BP 1119 à Fort-de-France (97248), la société Asco BTP, dont le siège est 56 cité Saint Georges à Schoelcher (97200), la société Acra Architecture, dont le siège est Immeuble Panorama boulevard de la Marne à Fort-de-France (97200), et la société Lorenzo Architecture, dont le siège est 60 rue du Professeur Camille Roy Quartier Dillon à Fort-de-France (97200) ; la société Michel Beauvais et associés et autres demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 13BX01044 du 31 mai 2013 par laquelle le juge des référés de la cour administrative d'appel de Bordeaux a rejeté leur demande tendant à l'annulation de l'alinéa 1 de l'article 1er de l'ordonnance n° 1201122 du 22 mars 2013 par laquelle le juge des référés du tribunal administratif de Fort-de-France, saisi d'une demande tendant à ce que soit précisée la portée de l'expertise ordonnée par les ordonnances nos 0901210, 1000349, 1000388, 1000578, 1000747, 1100614, 1100900, 1100986, 1200005 et 1200050, a indiqué que " le protocole d'accord et l'avenant ne font pas obstacle à ce que la mission de l'expert puisse porter sur des éléments de fait inclus dans le champ de ces accords, mais dont la prise en compte peut avoir un effet sur la solution de questions situées hors champ d'application de ces actes, car non tranchées par ces derniers " ;<br/>
<br/>
              2°) de mettre à la charge du syndicat inter-hospitalier de Mangot-Vulcin le versement d'une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ainsi que le montant de la contribution pour l'aide juridique ;<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Natacha Chicot, Auditeur,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boulloche, avocat de la société Michel Beauvais et associés, de la société Ion Cindea, de la société Asco BTP, de la société Acra Architecture et de la société Lorenzo Architecture ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges des référés, que le juge des référés du tribunal administratif de Fort-de-France a ordonné en 2009 la réalisation d'une expertise relative à la construction de la nouvelle cité hospitalière de Mangot-Vulcin au Lamentin, dont l'étendue a été modifiée à plusieurs reprises, et en a confié la charge à M. A... B...; que la société Michel Beauvais et associés et autres se pourvoient en cassation contre l'ordonnance du 31 mai 2013 par laquelle le juge des référés de la cour administrative d'appel de Bordeaux a rejeté leur requête tendant à l'annulation de l'article 1er de l'ordonnance du 22 mars 2013 par laquelle le juge des référés du tribunal administratif de Fort-de-France a fait droit à la demande, présentée par M.B..., tendant à ce que soit précisée la portée de cette expertise ;<br/>
<br/>
              2. Considérant, en premier lieu, qu'aux termes de l'article R. 532-3 du code de justice administrative : " Le juge des référés peut, à la demande de l'une des parties formée dans le délai de deux mois qui suit la première réunion d'expertise, ou à la demande de l'expert formée à tout moment, étendre l'expertise à des personnes autres que les parties initialement désignées par l'ordonnance, ou mettre hors de cause une ou plusieurs des parties ainsi désignées. / Il peut, dans les mêmes conditions, étendre la mission de l'expertise à l'examen de questions techniques qui se révélerait indispensable à la bonne exécution de cette mission, ou, à l'inverse, réduire l'étendue de la mission si certaines des recherches envisagées apparaissent inutiles. " ; <br/>
<br/>
              3. Considérant que les dispositions précitées de l'article R. 532-3 du code de justice administrative, qui permettent à l'expert de demander au juge des référés la modification du périmètre de l'expertise dont il a la charge, lui permettent aussi de demander des précisions sur le contenu de sa mission ; que de telles demandes, présentées par l'expert, qui n'est pas une partie à l'instance qui a conduit à ce que soit ordonnée l'expertise, sont dispensées du ministère d'avocat ; que les requérantes ne sont, par suite, pas fondées à soutenir que le juge des référés de la cour administrative d'appel aurait commis des erreurs de droit, d'une part, en jugeant que l'expert était recevable à saisir le juge des référés d'une demande de précisions sur l'expertise dont il a été chargé, d'autre part, en n'exigeant pas qu'il présente cette demande par le ministère d'un avocat ; <br/>
<br/>
              4. Considérant, en second lieu, que le juge des référés de la cour administrative d'appel a, par une ordonnance suffisamment motivée, confirmé les précisions apportées par le juge des référés du tribunal administratif de Fort-de-France ; que si les requérantes soutiennent qu'il aurait commis une erreur de droit en permettant à l'expert de connaître les causes du protocole d'accord et de son avenant, signés respectivement les 28 novembre 2005 et 5 février 2007, elles n'apportent, à l'appui de ce moyen, aucune précision permettant d'en apprécier le bien fondé ;<br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que le pourvoi de la société Michel Beauvais et associés et autres doit être rejeté ; qu'il en va de même, par voie de conséquence, de leurs conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative ; qu'il y a également lieu, dans les circonstances de l'espèce, de laisser la contribution pour l'aide juridique à la charge des sociétés requérantes ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi des sociétés Michel Beauvais et associés, Ion Cindea, Asco BTP, Acra Architecture et Lorenzo Architecture est rejeté.<br/>
Article 2 : La contribution pour l'aide juridique est laissée à la charge des sociétés Michel Beauvais et associés, Ion Cindea, Asco BTP, Acra Architecture et Lorenzo Architecture.<br/>
Article 3 : La présente décision sera notifiée aux sociétés Michel Beauvais et associés, Ion Cindea, Asco BTP, Acra Architecture et Lorenzo Architecture.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-03-011 PROCÉDURE. PROCÉDURES DE RÉFÉRÉ AUTRES QUE CELLES INSTITUÉES PAR LA LOI DU 30 JUIN 2000. RÉFÉRÉ TENDANT AU PRONONCÉ D'UNE MESURE D'EXPERTISE OU D'INSTRUCTION. - PORTÉE DES DISPOSITIONS DE L'ARTICLE R. 532-3 DU CJA - 1) FACULTÉ DE L'EXPERT DE DEMANDER AU JUGE DES RÉFÉRÉS LA MODIFICATION DU PÉRIMÈTRE DE L'EXPERTISE - EXISTENCE - FACULTÉ DE L'EXPERT DE DEMANDER DES PRÉCISIONS SUR LE CONTENU DE SA MISSION - EXISTENCE - 2) DEMANDE PRÉSENTÉE PAR L'EXPERT DANS CE CADRE - MINISTÈRE D'AVOCAT OBLIGATOIRE - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-04-02-02 PROCÉDURE. INSTRUCTION. MOYENS D'INVESTIGATION. EXPERTISE. - PORTÉE DES DISPOSITIONS DE L'ARTICLE R. 532-3 DU CJA - 1) FACULTÉ DE L'EXPERT DE DEMANDER AU JUGE DES RÉFÉRÉS LA MODIFICATION DU PÉRIMÈTRE DE L'EXPERTISE - EXISTENCE - FACULTÉ DE L'EXPERT DE DEMANDER DES PRÉCISIONS SUR LE CONTENU DE SA MISSION - EXISTENCE - 2) DEMANDE PRÉSENTÉE PAR L'EXPERT DANS CE CADRE - MINISTÈRE D'AVOCAT OBLIGATOIRE - ABSENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-04-02-02-01-03 PROCÉDURE. INSTRUCTION. MOYENS D'INVESTIGATION. EXPERTISE. RECOURS À L'EXPERTISE. MISSION DE L'EXPERT. - PORTÉE DES DISPOSITIONS DE L'ARTICLE R. 532-3 DU CJA - 1) FACULTÉ DE L'EXPERT DE DEMANDER AU JUGE DES RÉFÉRÉS LA MODIFICATION DU PÉRIMÈTRE DE L'EXPERTISE - EXISTENCE - FACULTÉ DE L'EXPERT DE DEMANDER DES PRÉCISIONS SUR LE CONTENU DE SA MISSION - EXISTENCE - 2) DEMANDE PRÉSENTÉE PAR L'EXPERT DANS CE CADRE - MINISTÈRE D'AVOCAT OBLIGATOIRE - ABSENCE.
</SCT>
<ANA ID="9A"> 54-03-011 1) Les dispositions de l'article R. 532-3 du code de justice administrative (CJA), qui permettent à l'expert de demander au juge des référés la modification du périmètre de l'expertise dont il a la charge, lui permettent aussi de demander des précisions sur le contenu de sa mission.... ,,2) De telles demandes, présentées par l'expert, qui n'est pas une partie à l'instance qui a conduit à ce que soit ordonnée l'expertise, sont dispensées du ministère d'avocat.</ANA>
<ANA ID="9B"> 54-04-02-02 1) Les dispositions de l'article R. 532-3 du code de justice administrative (CJA), qui permettent à l'expert de demander au juge des référés la modification du périmètre de l'expertise dont il a la charge, lui permettent aussi de demander des précisions sur le contenu de sa mission.... ,,2) De telles demandes, présentées par l'expert, qui n'est pas une partie à l'instance qui a conduit à ce que soit ordonnée l'expertise, sont dispensées du ministère d'avocat.</ANA>
<ANA ID="9C"> 54-04-02-02-01-03 1) Les dispositions de l'article R. 532-3 du code de justice administrative (CJA), qui permettent à l'expert de demander au juge des référés la modification du périmètre de l'expertise dont il a la charge, lui permettent aussi de demander des précisions sur le contenu de sa mission.... ,,2) De telles demandes, présentées par l'expert, qui n'est pas une partie à l'instance qui a conduit à ce que soit ordonnée l'expertise, sont dispensées du ministère d'avocat.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
