<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037625000</ID>
<ANCIEN_ID>JG_L_2018_11_000000413017</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/62/50/CETATEXT000037625000.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 19/11/2018, 413017</TITRE>
<DATE_DEC>2018-11-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>413017</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>HAAS ; SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT>M. Olivier Henrard</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:413017.20181119</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La commune d'Hyères a demandé au tribunal administratif de Nice, qui a renvoyé le jugement de l'affaire au tribunal administratif de Toulon, de condamner solidairement l'Etat et la société Verdino Constructions à lui verser la somme de 254 030,40 euros correspondant au montant estimé de la réparation des désordres constatés sur le quai d'avitaillement du port Saint-Pierre, assortie des intérêts au taux légal. Par un jugement n° 0705856 du 15 octobre 2009, le tribunal administratif de Toulon a rejeté cette demande. <br/>
<br/>
              Par un arrêt n° 09MA04654 du 16 juillet 2012, la cour administrative d'appel de Marseille a, sur appel de la commune d'Hyères, annulé ce jugement, déclaré l'Etat et la société Verdino Constructions solidairement responsables du préjudice résultant pour la commune d'Hyères des désordres affectant le quai d'avitaillement du port et ordonné, avant de statuer sur les conclusions indemnitaires de la commune d'Hyères, une expertise aux fins notamment de déterminer les causes et origines de ces désordres et d'évaluer la nature et le coût des travaux nécessaires pour y remédier.<br/>
<br/>
              Par un arrêt n° 09MA04654 du 22 décembre 2014, la cour administrative d'appel de Marseille a ordonné une nouvelle expertise aux fins notamment d'évaluer le montant des travaux de reprise.<br/>
<br/>
              Par un arrêt n° 09MA04654 du 12 juin 2017, la cour administrative d'appel de Marseille a, en premier lieu, condamné solidairement la société Travaux du Midi Var, venant aux droits de la société Verdino Constructions, et l'Etat à verser la somme de 686 036,94 euros TTC à la commune d'Hyères assortie des intérêts au taux légal à compter du 6 novembre 2007, en deuxième lieu, mis à sa charge solidaire avec l'Etat les frais d'expertise, taxés et liquidés à la somme totale de 71 291,32 euros, en troisième lieu, condamné l'Etat à garantir la société Travaux du Midi Var à hauteur de 25 % des sommes qui lui seront demandées en exécution des articles 1er et 2 de l'arrêt de la cour, et, en dernier lieu, rejeté le surplus des conclusions des parties.  <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 2 août et 3 novembre 2017 au secrétariat du contentieux du Conseil d'Etat, la société Travaux du Midi Var demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt du 12 juin 2017 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de la commune d'Hyères ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat et de la commune d'Hyères la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code des marchés publics ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean Sirinelli, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Olivier Henrard, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Haas, avocat de la société Travaux du Midi Var, et à la SCP Lyon-Caen, Thiriez, avocat de la commune d'Hyères.<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, dans le cadre de l'extension du quai d'avitaillement du port Saint-Pierre, la commune d'Hyères a confié, en février 1992, une mission de maîtrise d'oeuvre à la direction départementale de l'équipement du Var et a conclu un marché de travaux publics avec la société Verdino Constructions ; que les travaux ont été réceptionnés sans réserve le 10 août 1992 ; que, des désordres étant survenus en 2001, la commune a saisi le tribunal administratif de Nice de conclusions tendant à la condamnation solidaire de l'Etat et de la société Verdino Constructions au titre de la garantie décennale ; que, par un jugement du 15 octobre 2009, le tribunal administratif de Toulon, auquel le tribunal administratif de Nice avait renvoyé le jugement de l'affaire, a rejeté la demande de la commune ; que, par un arrêt du 16 juillet 2012, la cour administrative d'appel de Marseille a annulé ce jugement, a retenu la responsabilité solidaire de l'Etat et de la société Verdino Constructions du fait des désordres affectant le quai d'avitaillement et le rendant impropre à sa destination et a ordonné une expertise ; que, par l'arrêt attaqué du 12 juin 2017, la cour a condamné solidairement l'Etat et la société Travaux du Midi Var, venant aux droits de la société Verdino Constructions, à verser à la commune d'Hyères la somme de 686 036,94 euros TTC et mis à leur charge la somme de 71 291,32 euros au titre des frais d'expertise ; que la cour a également condamné l'Etat à garantir la société Travaux du Midi Var à hauteur de 25 % de ces sommes ; que la société Travaux du Midi Var se pourvoit en cassation contre cet arrêt ; que le ministre d'Etat, ministre de la transition écologique et solidaire, conclut, par la voie du pourvoi provoqué, à son annulation en tant qu'il condamne solidairement l'Etat à indemniser la commune d'Hyères ;<br/>
<br/>
              Sur le pourvoi principal :<br/>
<br/>
              En ce qui concerne la régularité de l'arrêt attaqué :<br/>
<br/>
              2. Considérant, en premier lieu, que la circonstance qu'un mémoire n'a pas été visé dans une décision juridictionnelle ne peut être utilement invoquée pour contester cette décision que par la partie qui l'a produit ; qu'ainsi, la société Travaux du Midi Var ne saurait utilement se prévaloir, en tout état de cause, de la circonstance que la cour administrative d'appel n'a pas visé un mémoire du 2 juillet 2012 produit par la commune d'Hyères antérieurement à l'arrêt du 16 juillet 2012 ;<br/>
<br/>
              3. Considérant, en second lieu, que la société Travaux du Midi Var n'est pas fondée, en tout état de cause, à soutenir que la cour a entaché son arrêt d'irrégularité en ne visant qu'un des deux mémoires qu'elle a produits le 24 mai 2017, postérieurement à la clôture de l'instruction, dès lors qu'il ressort des pièces du dossier que son second mémoire, qui a été enregistré au greffe de la cour moins de deux heures après le premier, dont il reprend et enrichit la substance, avait vocation à s'y substituer ;<br/>
<br/>
              En ce qui concerne le bien-fondé de l'arrêt attaqué :<br/>
<br/>
              4. Considérant que, pour ne condamner l'Etat à garantir la société Travaux du Midi Var qu'à hauteur de 25 % du montant de leur condamnation solidaire, la cour administrative d'appel de Marseille a relevé que l'insuffisance de la surveillance exercée par le maître d'oeuvre sur les travaux réalisés par la société Verdino Constructions relatifs à l'assise du quai n'était pas constitutive d'une faute caractérisée d'une gravité suffisante de nature à engager la responsabilité de l'Etat ; qu'en subordonnant ainsi l'engagement de la responsabilité du maître d'oeuvre dans le cadre de sa mission de surveillance de l'exécution du marché à l'existence d'une faute caractérisée d'une gravité suffisante, alors qu'il lui appartenait seulement de rechercher si le comportement du maître d'oeuvre présentait un caractère fautif eu égard à la portée de son intervention compte tenu des propres obligations des autres constructeurs, la cour administrative d'appel de Marseille a commis une erreur de droit ; <br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que la société Travaux du Midi Var est fondée à demander l'annulation de l'arrêt qu'elle attaque en tant seulement qu'il a statué, par son article 3, sur ses conclusions d'appel en garantie dirigées contre l'Etat ;<br/>
<br/>
              Sur le pourvoi provoqué de l'Etat :<br/>
<br/>
              6. Considérant qu'un pourvoi provoqué est recevable, dès lors que le pourvoi principal est accueilli, que les conclusions ne soulèvent pas un litige distinct et que la décision rendue sur le pourvoi principal est susceptible d'aggraver la situation de l'auteur du pourvoi provoqué ; que, d'une part, les conclusions de l'Etat tendant à l'annulation de l'arrêt attaqué en tant qu'il prononce sa condamnation solidaire ne soulèvent pas un litige distinct du pourvoi principal ; que, d'autre part, l'annulation de l'arrêt en tant qu'il statue sur les conclusions d'appel en garantie présentées par la société Travaux du Midi Var est susceptible d'aggraver la situation de l'Etat ; que, par suite, le pourvoi provoqué de l'Etat est recevable ;<br/>
<br/>
              7. Considérant, en premier lieu, que, pour le motif rappelé au point 2, le ministre d'Etat, ministre de la transition écologique et solidaire, ne peut, en tout état de cause, utilement invoquer la circonstance que la cour n'aurait pas visé un mémoire produit par la société Travaux du Midi Var ;<br/>
<br/>
              8. Considérant, en deuxième lieu, qu'il ressort des pièces du dossier que, le 2 août 2016, le ministre de l'environnement, de l'énergie et de la mer a été invité à produire des observations sur le rapport déposé le 27 juillet 2017 par l'expert désigné à la suite de l'arrêt de la cour administrative d'appel du 22 décembre 2014 ; que l'Etat n'a pas produit de nouvelles écritures, alors même que la commune d'Hyères, à la suite du dépôt du rapport de l'expert, avait porté ses conclusions indemnitaires au montant retenu par celui-ci ; que la société Travaux du Midi Var, pour sa part, n'a produit un mémoire que postérieurement à la clôture de l'instruction ; que, d'une part, dans ces conditions, le ministre d'Etat, ministre de la transition écologique et solidaire n'est pas fondé à soutenir que la cour administrative d'appel, qui a suffisamment motivé son arrêt sur ce point, aurait méconnu la portée des écritures de l'Etat et de la société des Travaux du Midi Var en relevant, après s'être appropriée les conclusions du rapport d'expertise, que le montant fixé par celui-ci n'avait été contesté par eux ; que, d'autre part, la cour, en estimant que le coût de certains travaux, au motif allégué qu'ils auraient été constitutifs d'une plus-value pour la commune d'Hyères, ne devait pas être déduit du montant retenu par l'expert ,a porté une appréciation souveraine exempte de dénaturation sur les pièces dont elle était saisie ; qu'elle n'a pas davantage, ce faisant, commis une erreur de droit ou méconnu son office ;<br/>
<br/>
              9. Considérant, en troisième lieu, qu'il ressort des pièces du dossier que, dans le cadre de sa requête introductive d'instance en date du 6 novembre 2007, la commune d'Hyères a demandé la condamnation solidaire de l'Etat et de la société Verdino Construction à lui verser une somme de 254 030,40 euros assortie des intérêts au taux légal ; qu'elle a porté ses conclusions indemnitaires, pour les mêmes chefs de préjudice, à la somme de 686 036,94 euros à la suite du dépôt, le 27 juillet 2017, du rapport d'expertise par son dernier mémoire enregistré au greffe de la cour administrative d'appel de Marseille le 23 février 2017, sans réitérer expressément ses conclusions tendant à l'application des intérêts au taux légal ; que, toutefois, dès lors que la commune avait assorti sa demande initiale de telles conclusions et qu'elle avait précisé, dans son mémoire du 23 février 2017, ne renoncer nullement au bénéfice de ses précédentes écritures, le moyen tiré de ce que la cour administrative d'appel aurait statué " ultra petita " en condamnant l'Etat et la société Travaux du Midi Var au versement des intérêts au taux légal sur l'intégralité de la somme 686 036,94 euros ne peut qu'être écarté ;<br/>
<br/>
              10. Considérant qu'il résulte de ce qui précède que le pourvoi provoqué de l'Etat doit être rejeté ;<br/>
<br/>
              Sur les conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              11. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement tant à la société Travaux du Midi Var qu'à la commune d'Hyères de la somme de 3 000 euros en application de l'article L. 761-1 du code de justice administrative ; que les dispositions de cet article font obstacle à ce que soient accueillies les conclusions présentées au même titre par la société Travaux du Midi Var à l'encontre de la commune d'Hyères ainsi que celles présentées par la commune à l'encontre de la société Travaux du Midi Var ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'article 3 de l'arrêt n° 09MA04654 de la cour administrative d'appel de Marseille du 12 juin 2017 est annulé.<br/>
Article 2 : L'affaire est renvoyée dans cette mesure à la cour administrative d'appel de Marseille. <br/>
Article 3 : Le pourvoi provoqué de l'Etat est rejeté.<br/>
Article 4 : L'Etat versera à la société Travaux du Midi Var et à la commune d'Hyères une somme de 3 000 euros chacun au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 5 : Le surplus des conclusions du pourvoi de la société Travaux du Midi Var ainsi que les conclusions de la commune d'Hyères sont rejetées.<br/>
Article 6 : La présente décision sera notifiée à la société Travaux du Midi Var, à la commune d'Hyères et au ministre d'Etat, ministre de la transition écologique et solidaire.<br/>
Copie en sera adressée à la société Géologie Informatique Appliquée Ingénierie, à M B...A...et à la société études de génie civil et d'équipements.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-06 MARCHÉS ET CONTRATS ADMINISTRATIFS. RAPPORTS ENTRE L'ARCHITECTE, L'ENTREPRENEUR ET LE MAÎTRE DE L'OUVRAGE. - RESPONSABILITÉ DU MAÎTRE D'OEUVRE ENVERS L'ENTREPRENEUR [RJ1], À RAISON DU DÉFAUT DE SURVEILLANCE DE L'EXÉCUTION DU MARCHÉ - RÉGIME DE FAUTE SIMPLE - EXISTENCE [RJ2] - PRISE EN COMPTE DE LA PORTÉE DE L'INTERVENTION DU MAÎTRE D'OEUVRE, COMPTE TENU DES OBLIGATIONS PROPRES DES AUTRES CONSTRUCTEURS - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">39-06-01-06 MARCHÉS ET CONTRATS ADMINISTRATIFS. RAPPORTS ENTRE L'ARCHITECTE, L'ENTREPRENEUR ET LE MAÎTRE DE L'OUVRAGE. RESPONSABILITÉ DES CONSTRUCTEURS À L'ÉGARD DU MAÎTRE DE L'OUVRAGE. ACTIONS EN GARANTIE. - ACTION EN GARANTIE DE L'ENTREPRENEUR CONTRE LE MAÎTRE D'OEUVRE [RJ1], À RAISON DU DÉFAUT DE SURVEILLANCE DE L'EXÉCUTION DU MARCHÉ - RÉGIME DE FAUTE SIMPLE - EXISTENCE [RJ2] - PRISE EN COMPTE DE LA PORTÉE DE L'INTERVENTION DU MAÎTRE D'OEUVRE, COMPTE TENU DES OBLIGATIONS PROPRES DES AUTRES CONSTRUCTEURS - EXISTENCE.
</SCT>
<ANA ID="9A"> 39-06 Commet une erreur de droit la cour qui subordonne l'engagement de la responsabilité d'un maître d'oeuvre, dans le cadre de sa mission de surveillance de l'exécution du marché, à l'existence d'une faute caractérisée d'une gravité suffisante, alors qu'il lui appartient seulement de rechercher si le comportement du maître d'oeuvre présente un caractère fautif, eu égard à la portée de son intervention compte tenu des propres obligations des autres constructeurs.</ANA>
<ANA ID="9B"> 39-06-01-06 Commet une erreur de droit la cour qui subordonne l'engagement de la responsabilité d'un maître d'oeuvre, dans le cadre de sa mission de surveillance de l'exécution du marché, à l'existence d'une faute caractérisée d'une gravité suffisante, alors qu'il lui appartient seulement de rechercher si le comportement du maître d'oeuvre présente un caractère fautif, eu égard à la portée de son intervention compte tenu des propres obligations des autres constructeurs.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., TC, 20 novembre 1961,,c/,, n° 1751, p. 882 ; CE, 5 juin 1985,,, n° 49873, T. pp. 543-687-688.,,[RJ2] Ab. jur., sur ce point, CE, Section, 21 octobre 1966,,, n° 61615, p. 562 ; CE, 26 mai 1982, Ville de Chamonix-Mont-Blanc, n° 16488, T. pp. 671-672-673-749 ; CE, 21 décembre 1983, Société méditerranéenne de bâtiments industrialisés, n° 21648, T. p. 786. Rappr., s'agissant de la responsabilité du maître d'oeuvre, CE, 28 mai 1975,,, n° 91870, T. pp. 1134-1137 ; CE, 10 février 1990,  S.A. Spie-Batignolles et,, n° 74315, T. p. 882 ; CE, 17 décembre 1990, Commune de Mours, n° 67044, T. p. 870 ; s'agissant de la responsabilité du maître d'ouvrage, CE, 27 mai 1998, Société Dodin, n° 149830, T. p. 1209.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
