<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038625583</ID>
<ANCIEN_ID>JG_L_2019_06_000000424326</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/62/55/CETATEXT000038625583.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 14/06/2019, 424326, Publié au recueil Lebon</TITRE>
<DATE_DEC>2019-06-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>424326</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Cécile Renault</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Mireille Le Corre</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:424326.20190614</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 18 septembre 2018 et 15 janvier 2019 au secrétariat du contentieux du Conseil d'Etat, M. D...C...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret du 3 août 2018 portant nomination du président du conseil d'administration de l'Ecole polytechnique ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son article 13 ;<br/>
              - le code de la défense ;<br/>
              - le code de l'éducation ;<br/>
              - le code des relations entre le public et l'administration ;<br/>
              - le décret n° 2009-63 du 16 janvier 2009 ;<br/>
              - le décret n° 2015-1176 du 24 septembre 2015 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Cécile Renault, auditrice,  <br/>
<br/>
              - les conclusions de Mme Mireille Le Corre, rapporteur public ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 755-1 du code de l'éducation, auquel renvoie l'article L. 3411-1 du code de la défense : " L'Ecole polytechnique constitue un établissement public doté de la personnalité civile et de l'autonomie financière, placé sous la tutelle du ministre chargé de la défense. / L'administration de l'école est assurée par un conseil d'administration et le président de ce conseil. Un officier général assure, sous l'autorité du président du conseil d'administration, la direction générale et le commandement militaire de l'école. / Un décret en Conseil d'Etat précise la répartition des pouvoirs et des responsabilités entre le conseil d'administration et son président. Il fixe également les règles relatives à l'organisation et au régime administratif et financier de l'école, qui est soumise, sauf dérogation prévue par le même décret, aux dispositions réglementaires concernant l'administration et le contrôle financier des établissements publics à caractère administratif dotés de l'autonomie financière ".<br/>
<br/>
              2. Aux termes de l'article 13 de la Constitution : " Le Président de la République signe les ordonnances et les décrets délibérés en Conseil des ministres. / Il nomme aux emplois civils et militaires de l'Etat (...) ". L'article 14 du décret du 24 septembre 2015 relatif à l'organisation et au régime administratif et financier de l'Ecole polytechnique dispose que : " Le président du conseil d'administration est nommé par décret en conseil des ministres, sur proposition du ministre de la défense, pour une durée de cinq ans renouvelable. Il est choisi, après appel public à candidatures, publié au Journal officiel, parmi les personnalités justifiant d'une compétence scientifique dans les domaines d'activité de l'école ou ayant une expérience de l'enseignement supérieur ou de la recherche ".<br/>
<br/>
              3. Afin de pourvoir aux fonctions de président du conseil d'administration de l'Ecole polytechnique, la ministre des armées a publié un avis de vacance d'emploi au Journal officiel de la République française le 22 février 2018. M.C..., qui s'est porté candidat à ces fonctions, demande l'annulation pour excès de pouvoir du décret du 3 août 2018 par lequel le Président de la République a nommé M. A...B...en qualité de président du conseil d'administration de l'Ecole polytechnique. <br/>
<br/>
              4. En premier lieu, il ressort des pièces du dossier que la nomination du président du conseil d'administration de l'Ecole polytechnique, décidée par le décret contesté, a été précédée d'un examen des candidatures par un cabinet de recrutement, qui en a transmis une synthèse à un comité de sélection. Ce comité a auditionné certains des candidats puis a transmis son avis à la ministre des armées. <br/>
<br/>
              5. Aucune disposition législative ou réglementaire, ni aucun principe, ne faisait obstacle à ce que la ministre décidât de confier à un comité qu'elle instituait la mission d'analyser les candidatures, dès lors qu'elle ne s'estimait pas liée par l'avis de ce dernier. A cet égard, quels que soient les termes du message adressé au requérant par un conseiller au cabinet de la ministre, il ne ressort pas des pièces du dossier que la ministre se serait crue liée par l'avis émis par le comité de sélection. <br/>
<br/>
              6. En outre, si le requérant fait valoir que le cabinet de recrutement a été choisi par l'Ecole polytechnique, alors que l'un des candidats était le président sortant de l'école, et s'il relève la participation au comité de sélection du vice-président du conseil général de l'économie, de l'industrie, de l'énergie et des technologies et d'une personne s'étant entretenue avec le candidat retenu dans le cadre de l'élaboration d'un rapport intitulé " Pour un Institut Polytechnique de France ", il ne ressort pas des pièces du dossier que les avis émis par le cabinet de recrutement et le comité seraient intervenus pour des motifs étrangers à la valeur respective des candidats, révélant de la part des personnes visées un manque d'impartialité.<br/>
<br/>
              7. En deuxième lieu, il appartenait, en vertu des dispositions citées au point 2, au ministre de la défense de proposer au Président de la République la nomination, par décret délibéré en conseil des ministres, du candidat qu'il retenait, sans que M. C...puisse utilement soutenir que le conseil des ministres aurait dû recevoir communication de la liste de l'ensemble des candidats aux fonctions de président du conseil d'administration de l'Ecole polytechnique. <br/>
<br/>
              8. En troisième lieu, si le requérant soutient que sa demande de communication des motifs ayant conduit au choix du candidat retenu est restée sans réponse, le rejet de sa candidature n'avait pas à être motivé, dès lors que la nomination en cause ne saurait être regardée comme un avantage dont l'attribution constituerait un droit au sens de l'article L. 211-2 du code des relations entre le public et l'administration.<br/>
<br/>
              9. En dernier lieu, il résulte des dispositions de l'article 14 du décret du 24 septembre 2015, citées au point 2, que le président du conseil d'administration de l'Ecole polytechnique est choisi parmi les personnalités justifiant d'une compétence scientifique dans les domaines d'activité de l'école ou parmi les personnalités ayant une expérience de l'enseignement supérieur ou de la recherche. Il ressort des pièces du dossier que M.B..., ancien élève de l'Ecole polytechnique et de Télécom ParisTech, diplômé de l'Institut européen d'administration des affaires (INSEAD), membre du conseil d'école de Télécom ParisTech, de " l'international advisory board " de l'Ecole supérieure des sciences économiques et commerciales (ESSEC) et du comité des affaires stratégiques de l'école d'affaires publiques de l'Institut d'études politiques de Paris, satisfait aux conditions mises par l'article 14 du décret du 24 septembre 2015 à la nomination du président du conseil d'administration de l'Ecole polytechnique. Le moyen tiré de ce que le décret attaqué aurait été pris en méconnaissance des exigences de cet article ne peut, dès lors, qu'être écarté.<br/>
<br/>
              10. Il résulte de tout ce qui précède que M. C...n'est pas fondé à demander l'annulation du décret qu'il attaque. Les conclusions qu'il a présentées au titre de l'article L. 761-1 du code de justice administrative ne peuvent, en conséquence, qu'être rejetées.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. C... est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. D... C..., à la ministre des armées et au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">30-02-05-05 ENSEIGNEMENT ET RECHERCHE. QUESTIONS PROPRES AUX DIFFÉRENTES CATÉGORIES D'ENSEIGNEMENT. ENSEIGNEMENT SUPÉRIEUR ET GRANDES ÉCOLES. GRANDES ÉCOLES. - NOMINATION DU PRÉSIDENT DU CONSEIL D'ADMINISTRATION DE L'ECOLE POLYTECHNIQUE - CONTRÔLE DU JUGE DE L'EXCÈS DE POUVOIR - CONTRÔLE NORMAL [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-07-02-03 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. CONTRÔLE DU JUGE DE L'EXCÈS DE POUVOIR. APPRÉCIATIONS SOUMISES À UN CONTRÔLE NORMAL. - NOMINATION DU PRÉSIDENT DU CONSEIL D'ADMINISTRATION DE L'ECOLE POLYTECHNIQUE [RJ1].
</SCT>
<ANA ID="9A"> 30-02-05-05 Le juge de l'excès de pouvoir exerce un contrôle normal sur le respect, par le décret portant nomination du président du conseil d'administration de l'Ecole polytechnique, des conditions fixées par le décret n° 2015-1176 du 24 septembre 2015.</ANA>
<ANA ID="9B"> 54-07-02-03 Le juge de l'excès de pouvoir exerce un contrôle normal sur le respect, par le décret portant nomination du président du conseil d'administration de l'Ecole polytechnique, des conditions fixées par le décret n° 2015-1176 du 24 septembre 2015.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Ab. jur., s'agissant du contrôle restreint du juge sur des nominations subordonnées à des conditions fixées par des textes, CE, 19 décembre 2007, Commission de recherche et d'information indépendantes sur la radioactivité, n° 300451, p. 518.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
