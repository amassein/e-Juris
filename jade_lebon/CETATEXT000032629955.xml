<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032629955</ID>
<ANCIEN_ID>JG_L_2015_07_000000374114</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/62/99/CETATEXT000032629955.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 4ème SSR, 22/07/2015, 374114</TITRE>
<DATE_DEC>2015-07-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>374114</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 4ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARADUC, DUHAMEL, RAMEIX ; SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR>Mme Manon Perrière</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Fabienne Lambolez</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2015:374114.20150722</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              1° Sous le numéro 374 114, par une requête sommaire, un mémoire de désistement conditionnel, un mémoire complémentaire et un mémoire en réplique, enregistrés le 20 décembre 2013, et les 20 janvier, 20 mars et 7 août 2014 au secrétariat du contentieux du Conseil d'Etat, le Syndicat interprofessionnel des radios et télévisions indépendantes (SIRTI) demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir la délibération du Conseil supérieur de l'audiovisuel (CSA) du 11 décembre 2013 relative à la fixation des règles permettant de déterminer la somme des populations desservies par un service de radio autorisé en mode analogique par voie hertzienne terrestre ;<br/>
<br/>
              2°) de mettre à la charge du CSA la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              2° Sous le numéro 374183, par une requête sommaire et un mémoire complémentaire, enregistrés les 23 décembre 2013 et 21 mars 2014 au secrétariat du contentieux du Conseil d'Etat, la société Vortex demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la délibération du Conseil supérieur de l'audiovisuel (CSA) du 11 décembre 2013 relative à la fixation des règles permettant de déterminer la somme des populations desservies par un service de radio autorisé en mode analogique par voie hertzienne terrestre ;<br/>
<br/>
              2°) de mettre à la charge du CSA la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              3° Sous le numéro 383 009, par une requête sommaire et un mémoire complémentaire, enregistrés les 23 juillet et 23 octobre 2014 au secrétariat du contentieux du Conseil d'Etat, le Syndicat interprofessionnel des radios et télévisions indépendantes (SIRTI) demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 21 mai 2014 par laquelle le CSA a rejeté sa demande tendant au retrait de la délibération relative à la fixation des règles permettant de déterminer la somme des populations desservies par un service de radio autorisé en mode analogique par voie hertzienne terrestre ;<br/>
<br/>
              2°) de mettre à la charge du CSA la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu :<br/>
<br/>
              - la loi n° 86-1067 du 30 septembre 1986 ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Manon Perrière, auditeur,  <br/>
<br/>
              - les conclusions de Mme Fabienne Lambolez, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Spinosi, Sureau, avocat du Syndicat interprofessionnel des radios et télévisions indépendantes et à la SCP Baraduc, Duhamel, Rameix, avocat du Conseil supérieur de l'audiovisuel ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes du premier alinéa de l'article 29 de loi du 30 septembre 1986 : " Sous réserve des dispositions de l'article 26 de la présente loi, l'usage des fréquences pour la diffusion de services de radio par voie hertzienne terrestre est autorisé par le Conseil supérieur de l'audiovisuel dans les conditions prévues au présent article. / (...) / Le conseil accorde les autorisations en appréciant l'intérêt de chaque projet pour le public, au regard des impératifs prioritaires que sont la sauvegarde du pluralisme des courants d'expression socio-culturels, la diversification des opérateurs, et la nécessité d'éviter les abus de position dominante ainsi que les pratiques entravant le libre exercice de la concurrence. / (...) " ; qu'aux termes du premier alinéa de l'article 41 de la même loi : " Une même personne physique ou morale ne peut, sur le fondement d'autorisations relatives à l'usage de fréquences dont elle est titulaire pour la diffusion d'un ou de plusieurs services de radio par voie hertzienne terrestre en mode analogique, ou par le moyen d'un programme qu'elle fournit à d'autres titulaires d'autorisation par voie hertzienne terrestre en mode analogique, disposer en droit ou en fait de plusieurs réseaux que dans la mesure où la somme des populations recensées dans les zones desservies par ces différents réseaux n'excède pas 150 millions d'habitants. " ; qu'il appartient au CSA, lors de l'attribution des autorisations d'usage de fréquences, de s'assurer que ces dispositions ne sont pas méconnues ; <br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier que, pour déterminer les populations desservies par chaque service de radio autorisé en mode analogique par voie hertzienne terrestre afin de vérifier le respect du plafond de couverture de la population par un même opérateur titulaire d'autorisations d'usage de fréquences pour un ou plusieurs services de radio, le CSA prend principalement en compte les quatre paramètres suivants : les dernières données démographiques consolidées authentifiées à la suite d'un recensement, les données topographiques relatives au territoire français, un modèle de propagation et, enfin, un seuil de réception ; que, s'agissant de ce dernier paramètre, le CSA a utilisé le seuil de 60 dB V/m (décibel micro-volt par mètre) à compter du milieu des années 1990 jusqu'en 2010 ; qu'à compter de 2011, il a modifié ce paramètre ; que par délibération du 11 décembre 2013, il a énoncé et rendu publics les éléments sur lesquels repose la nouvelle méthode de calcul qu'il utilise depuis 2011 ; que le Syndicat interprofessionnel des radios et télévisions indépendantes (SIRTI) demande, sous le n° 374114, l'annulation pour excès de pouvoir de cette délibération et, sous le n° 383009, celle de la décision du 21 mai 2014 par laquelle le CSA a rejeté le recours gracieux dont il l'avait saisi ; que la société Vortex demande, sous le n° 374183, l'annulation pour excès de pouvoir de la même délibération ; que les requêtes du SIRTI et de la société Vortex étant dirigées contre la même délibération et présentant à juger les mêmes questions, il y a lieu de les joindre pour statuer par une seule décision ; <br/>
<br/>
              Sur l'intérêt pour agir de la société Vortex : <br/>
<br/>
              3. Considérant que cette société, qui édite un service radiophonique et sollicite l'attribution de fréquences dans le cadre d'appels aux candidatures auxquels répondent également des groupes de communication dont les réseaux desservent une population importante, justifie d'un intérêt lui donnant qualité pour demander l'annulation pour excès de pouvoir de la délibération du 11 décembre 2013 ; que la fin de non-recevoir opposée par le CSA à sa requête doit par suite être écartée ; <br/>
<br/>
              Sur la compétence du CSA pour adopter la délibération litigieuse :<br/>
<br/>
              4. Considérant qu'en l'absence de dispositions législatives ou réglementaires fixant des règles pour déterminer la population desservie par un service de radio, il appartient au CSA de définir les modalités qu'il entend mettre en oeuvre, en l'état des moyens techniques disponibles, afin d'évaluer cette population ; que, par suite, le moyen tiré de ce que la délibération litigieuse serait entachée d'incompétence doit être écarté ;<br/>
<br/>
              Sur les autres moyens des requêtes :<br/>
<br/>
              5. Considérant qu'aux termes de l'article 1er de la délibération du 11 décembre 2013 du CSA contestée : " Pour contrôler le respect des dispositions du premier alinéa de l'article 41 de la loi n° 86-1067 du 30 septembre 1986, le Conseil détermine la somme des populations couvertes par les éditeurs de services de radio autorisés en mode analogique par voie hertzienne terrestre. / Pour ce faire, il se fonde, d'une part, sur les chiffres de population, authentifiés par décret pris en application de la loi relative à la démocratie de proximité et, d'autre part, sur les paramètres techniques figurant en annexe. " ; qu'aux termes de l'annexe de cette même délibération : " La méthode de calcul pour les services diffusés en modulation de fréquence est fondée sur les éléments suivants : (...) Seuil de réception : 54 dB V/m [décibel micro-volt par mètre] à 10m/sol ; Prise en compte des 4 principaux brouilleurs constants / Modèle de propagation : Modèle statistique de la recommandation de l'Union internationale des télécommunications (UIT-P 1546 " Méthode de prévision de la propagation point à zone pour les services de Terre entre 30 MHz et 3 000 MHz ") " ; que les requérants critiquent, d'une part, le modèle de propagation retenu et, d'autre part, le paramètre du seuil de réception qui combine un seuil de 54 dB V/m à 10m/sol avec la prise en compte des quatre principaux brouilleurs constants ; qu'ils soutiennent que ce paramètre ne respecte pas le principe d'intelligibilité de la norme et conduit à sous-évaluer la population desservie par les services de radio, faisant obstacle au respect des dispositions de l'article 41 de la loi du 30 septembre 1986 et de l'objectif de diversification des opérateurs mentionné à l'article 29 de la même loi ;   <br/>
<br/>
              6. Considérant que, pour vérifier le respect des dispositions de l'article 41 de la loi du 30 septembre 1986 et en l'absence de dispositions législatives ou réglementaires fixant des règles pour la détermination des populations couvertes par les services de radio, le CSA doit recourir à une méthode de calcul permettant d'approcher au plus près la population effectivement desservie par chaque service, en tenant compte des phénomènes de brouillage faisant obstacle à la réception ; que si la complexité de l'évaluation et les limites des moyens techniques disponibles rendent inévitables certaines approximations, la méthode adoptée doit permettre de détecter tout dépassement du seuil de 150 millions d'habitants sans risquer de faire apparaître un dépassement fictif de ce seuil ; <br/>
<br/>
              7. Considérant qu'il ressort des précisions apportées par le CSA que, alors que la méthode utilisée jusqu'en 2010 ferait apparaître en 2013 un dépassement du seuil par le groupe desservant le plus grand nombre de personnes, le groupe NRJ, avec une population totale desservie au travers de ses différents réseaux de 151 millions d'habitants, celle qu'il a retenue dans la délibération attaquée conduit à évaluer la population couverte par ce groupe en 2013 à 117,5 millions d'habitants ; que le CSA fait valoir que les paramètres sur lesquels reposait la méthode précédemment utilisée entraînaient, du fait d'une prise en compte très insuffisante des phénomènes de brouillage, une forte surévaluation des populations couvertes, qu'il importait de corriger dès lors que les résultats obtenus par certains groupes approchaient du seuil fixé par la loi ; que l'instance de régulation précise que les paramètres de la nouvelle méthode permettent une estimation plus exacte des populations couvertes et que les approximations qu'elle comporte peuvent seulement conduire à une surévaluation et en aucun cas à une sous-évaluation ; <br/>
<br/>
              8. Considérant, en premier lieu, que la délibération attaquée ne définit aucune norme qui s'imposerait à des tiers mais expose la méthode que le CSA emploie pour calculer la somme des populations desservies par un service de radio ; que, dans ces conditions, la circonstance qu'elle n'est pas compréhensible à la simple lecture et qu'elle se borne à indiquer les paramètres sur lesquels repose la méthode employée, sans la décrire de manière exhaustive, n'est pas de nature à justifier l'annulation de cette délibération ; qu'au surplus, le CSA a, en réponse à la mesure supplémentaire d'instruction, apporté des éclaircissements sur la notion de " quatre principaux brouilleurs constants " ; <br/>
<br/>
              9. Considérant, en deuxième lieu, que si la société requérante soutient qu'un autre modèle de propagation, le modèle déterministe, permettrait de mieux appréhender les brouillages en zone de montagne, il ressort des pièces du dossier que le modèle statistique de propagation retenue par le CSA est recommandé par l'Union internationale des télécommunications (UIT) ; <br/>
<br/>
              10. Considérant, en troisième lieu, que, alors que les recommandations de l'UIT préconisent de retenir trois seuils de réception différents, respectivement 54 dB V/m en zone rurale, 66 dB V/m en zone péri-urbaine et 74 dB V/m en zone urbaine dense, le CSA indique ne pas être en mesure de retenir trois seuils différents selon les zones, sans préciser les raisons de cette impossibilité ; que la société Vortex fait valoir que la méthode a été définie en fonction des limites de l'outil de calcul et non en paramétrant un outil qui permettrait de calculer de façon plus exacte la population desservie, ce qui selon elle ne se heurterait à aucune impossibilité ; qu'il n'est pas possible, en l'état de l'instruction, de déterminer si le nouveau seuil de réception fixé à 54dB V/m pour l'ensemble du territoire, contre 60 dB V/m dans la méthode précédente et qui permet de ne plus sous-estimer la population couverte en zone rurale, conduit systématiquement à une surévaluation sensible de la population desservie dans les zones urbaines et urbaines denses, ainsi que l'affirme en défense le CSA ; <br/>
<br/>
              11. Considérant, en quatrième lieu, qu'afin de tenir compte, dans la nouvelle méthode de calcul, des brouillages qui font obstacle à ce qu'une partie des habitants d'une zone reçoivent une radio qui y est émise, le CSA a introduit une nouvelle variable, celle des " quatre principaux brouilleurs constants ", alors que la précédente méthode de calcul prenait en compte les brouillages de manière forfaitaire ; que l'identification du brouillage constant de la réception d'un service par l'émission d'un autre service est conforme aux préconisations de l'UIT ; qu'il résulte des précisions apportées par le CSA que le logiciel utilisé pour procéder au calcul de la population desservie tient compte de la répartition de la population dans chaque zone et sélectionne automatiquement, pour chaque émetteur de chaque zone, les quatre sources de brouillage constant qui sont la cause des nuisances les plus significatives pour les fréquences émises par cet émetteur ; qu'il recourt à cet effet à deux paramètres, à savoir le champ reçu depuis l'émetteur potentiellement brouilleur en fonction du modèle de propagation et le " rapport de protection ", calculé conformément à la recommandation 412-9 de l'UIT ; que le logiciel exclut des chiffres de la population desservie par l'émetteur considéré la partie de la population affectée par ces nuisances ; que, toutefois, la société Vortex fait valoir que la méthode retenue pour évaluer le brouillage, préconisée par l'UIT dans un but de planification et non de mesure de la réception réelle, retient des valeurs qui majorent quelque peu les brouillages et conduisent, en conséquence, à une sous-évaluation de la population effectivement desservie ; que le logiciel de calcul utilisé par le CSA, dont les requérants font valoir qu'il serait défaillant, ne permet pas de délivrer des calculs intermédiaires, émetteur par émetteur, puis zone par zone, mais seulement des résultats globaux ; qu'il n'est pas possible, en l'état de l'instruction, de déterminer avec quelle fiabilité cet outil de calcul prend en compte l'existence d'émetteurs susceptibles de brouiller les fréquences étudiées, en particulier dans les zones densément peuplées où la marge d'erreur est plus importante ; <br/>
<br/>
              12. Considérant qu'aux termes de l'article R. 625-2 du code de justice administrative : " Lorsqu'une question technique ne requiert pas d'investigations complexes, la formation de jugement peut charger la personne qu'elle commet de lui fournir un avis sur les points qu'elle détermine. Elle peut, à cet effet, désigner une personne figurant sur l'un des tableaux établis en application de l'article R. 221-9. Elle peut, le cas échéant, désigner toute autre personne de son choix. Le consultant, à qui le dossier de l'instance n'est pas remis, n'a pas à opérer en respectant une procédure contradictoire à l'égard des parties. / L'avis est consigné par écrit. Il est communiqué aux parties par la juridiction. / Les dispositions des articles R. 621-3 à R. 621-6, R. 621-10 à R. 621-12-1 et R. 621-14 sont applicables aux avis techniques " ; que les arguments des requérants rappelés aux points 9, 10 et 11, tendant à établir que la méthode retenue par le CSA conduirait à une sous-évaluation des populations desservies soulèvent une question technique au sens de ces dispositions ; qu'il y a lieu, avant de statuer sur les requêtes du SIRTI et de la société Vortex de demander un avis sur ces points ;<br/>
<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : Il sera, avant de statuer sur les requêtes du SIRTI et de la société Vortex, demandé à M. B...un avis technique portant sur la fiabilité des paramètres retenus par le CSA dans sa délibération du 11 décembre 2013 en ce qui concerne le modèle de propagation, le seuil de réception et la prise en compte des quatre principaux brouilleurs constants, sur la possibilité de disposer d'instruments de mesure plus fiables et sur le point de savoir si, comme le soutient le CSA, la méthode qu'il a définie peut seulement conduire à surévaluer les populations desservies par les services de radio. <br/>
Article 2 : M. A...prêtera serment par écrit ; l'avis sera déposé au secrétariat du contentieux dans le délai de trois mois suivant la prestation de serment.<br/>
Article 3 : Les frais relatifs à l'avis sont réservés pour y être statué en fin d'instance.<br/>
Article 4 : La présente décision sera notifiée au Syndicat interprofessionnel des radios et télévisions indépendantes, à la société Vortex et au Conseil supérieur de l'audiovisuel.<br/>
Copie en sera adressée pour information à la ministre de la culture et de la communication. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-04-02-08 PROCÉDURE. INSTRUCTION. MOYENS D'INVESTIGATION. - QUESTION TECHNIQUE QUI NE REQUIERT PAS D'INVESTIGATIONS COMPLEXES AU SENS DE L'ARTICLE R. 625-2 DU CJA - QUESTION PORTANT SUR LES BIAIS ÉVENTUELS DE LA MÉTHODE RETENUE PAR LE CSA POUR DÉTERMINER LES POPULATIONS DESSERVIES PAR UN SERVICE DE RADIO.
</SCT>
<ANA ID="9A"> 54-04-02-08 La question de savoir si la méthode retenue par le Conseil supérieur de l'audiovisuel (CSA) pour déterminer les populations desservies par un service de radio autorisé en mode analogique par voie hertzienne terrestre, afin de vérifier le respect du plafond de couverture de la population par un même opérateur titulaire d'autorisations d'usage de fréquences prévu au premier alinéa de l'article 41 de la loi n° 86-1067 du 30 septembre 1986, conduirait à une sous-évaluation des populations desservies, soulève une question technique au sens des dispositions de l'article R. 625-2 du code de justice administrative (CJA).</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
