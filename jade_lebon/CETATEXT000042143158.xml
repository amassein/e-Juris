<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042143158</ID>
<ANCIEN_ID>JG_L_2020_07_000000440764</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/14/31/CETATEXT000042143158.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 22/07/2020, 440764, Publié au recueil Lebon</TITRE>
<DATE_DEC>2020-07-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>440764</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DE CHAISEMARTIN, DOUMIC-SEILLER</AVOCATS>
<RAPPORTEUR>M. Pierre Ramain</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:440764.20200722</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 21 mai 2020 au secrétariat du contentieux du Conseil d'Etat, M. H... D..., M. B... E..., M. F... C... et M. G... A... demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'acte de promulgation de la " loi du pays " n° 2020-11 du 21 avril 2020 ;<br/>
<br/>
              2°) d'annuler la " loi du pays " n° 2020-11 du 21 avril 2020 sur la prévention et la gestion des menaces sanitaires graves et des situations d'urgence ;  <br/>
<br/>
              3°) de mettre à la charge de la Polynésie française la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son article 74 ; <br/>
              - la loi organique n°2004-192 du 27 février 2004 ;<br/>
              - le code de la santé publique ; <br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Pierre Ramain, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP de Chaisemartin, Doumic-Seiller, avocat de l'assemblée de la Polynésie française et de la présidence de la Polynésie française ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. L'assemblée de la Polynésie française a adopté le 17 avril 2020, sur le fondement de l'article 140 de la loi organique du 27 février 2004 portant statut d'autonomie de la Polynésie française, la délibération n° 2020-4 LP/APF sur la prévention et la gestion des menaces sanitaires graves et des situations d'urgence. Le président de la Polynésie française a promulgué cette " loi du pays " sous le n° 2020-11 le 21 avril 2020. M. D..., M. E..., M. C... et M. A... ont saisi le Conseil d'Etat d'une requête tendant à l'annulation de l'acte de promulgation de la " loi du pays " n° 2020-11 du 21 avril 2020 et de la " loi du pays " elle-même.<br/>
<br/>
              Sur l'exercice des recours :<br/>
<br/>
              2. De façon générale, l'article 176 de la loi organique du 27 février 2004 portant statut d'autonomie de la Polynésie française dispose que : " (...) II. - A l'expiration de la période de huit jours suivant l'adoption d'un acte prévu à l'article 140 dénommé "loi du pays" ou au lendemain du vote intervenu à l'issue de la nouvelle lecture prévue à l'article 143, l'acte prévu à l'article 140 dénommé " loi du pays " est publié au Journal officiel de la Polynésie française à titre d'information pour permettre aux personnes physiques ou morales, dans le délai d'un mois à compter de cette publication, de déférer cet acte au Conseil d'Etat./ Le recours des personnes physiques ou morales est recevable si elles justifient d'un intérêt à agir./ Dès sa saisine, le greffe du Conseil d'Etat en informe le président de la Polynésie française avant l'expiration du délai de dix jours prévu à l'article 178./ III. - Le Conseil d'Etat se prononce sur la conformité des actes prévus à l'article 140 dénommés "lois du pays" au regard de la Constitution, des lois organiques, des engagements internationaux et des principes généraux du droit. Il se prononce sur l'ensemble des moyens de la requête qu'il estime susceptibles de fonder l'annulation, en l'état du dossier. La procédure contentieuse applicable au contrôle juridictionnel spécifique de ces actes est celle applicable en matière de recours pour excès de pouvoir devant le Conseil d'Etat. / Les actes prévus à l'article 140 dénommés " lois du pays " ne peuvent plus être contestés par voie d'action devant aucune autre juridiction ". L'article 177 prévoit que : " I.- Le Conseil d'Etat se prononce dans les trois mois de sa saisine. Sa décision est publiée au Journal officiel de la République française et au Journal officiel de la Polynésie française. / Si le Conseil d'Etat constate qu'un acte prévu à l'article 140 dénommé "loi du pays" contient une disposition contraire à la Constitution, aux lois organiques, ou aux engagements internationaux ou aux principes généraux du droit, et inséparable de l'ensemble de l'acte, celle-ci ne peut être promulguée. / Si le Conseil d'Etat décide qu'un acte prévu à l'article 140 dénommé " loi du pays " contient une disposition contraire à la Constitution, aux lois organiques ou aux engagements internationaux, ou aux principes généraux du droit, sans constater en même temps que cette disposition est inséparable de l'acte, seule cette dernière disposition ne peut être promulguée. / II.-A l'expiration du délai de trois mois mentionné au premier alinéa du I du présent article, le président de la Polynésie française peut promulguer l'acte prévu à l'article 140 dénommé " loi du pays ", dans les conditions mentionnées au second alinéa de l'article 178. Le Conseil d'Etat reste toutefois saisi des recours formés contre l'acte. /Dans ce cas, lorsque l'acte contient une disposition contraire à la Constitution, aux lois organiques, aux engagements internationaux ou aux principes généraux du droit, et inséparable de l'ensemble de l'acte, le Conseil d'Etat en prononce l'annulation totale./ Si le Conseil d'Etat estime qu'une disposition est contraire à la Constitution, aux lois organiques, aux engagements internationaux ou aux principes généraux du droit, sans constater en même temps que cette disposition est inséparable de l'acte, il prononce l'annulation de cette seule disposition.". Aux termes de l'article 178, " A l'expiration du délai d'un mois mentionné au II de l'article 176 pour saisir le Conseil d'Etat ou à la suite de la publication au Journal officiel de la Polynésie française de la décision de ce conseil constatant la conformité totale ou partielle de l'acte prévu à l'article 140 dénommé " loi du pays " aux normes mentionnées au deuxième alinéa du I de l'article 177, le président de la Polynésie française dispose d'un délai de dix jours pour le promulguer, sous les réserves énoncées aux troisième et quatrième alinéas du I dudit article./ Il transmet l'acte de promulgation au haut-commissaire. L'acte prévu à l'article 140 dénommé " loi du pays " est publié, pour information, au Journal officiel de la République française ". Aux termes de l'article 180, " Sans préjudice de l'article 180-1, les actes prévus à l'article 140 dénommés " lois du pays " ne sont susceptibles d'aucun recours par voie d'action après leur promulgation ". <br/>
<br/>
              3. Pour sa part, l'article 180-1 de la loi organique du 27 février 2004 dispose que " Par dérogation au premier alinéa des I et II de l'article 176 et au premier alinéa des articles 178 et 180, les actes dénommés " lois du pays " relatifs aux impôts et taxes peuvent faire l'objet d'un recours devant le Conseil d'Etat à compter de la publication de leur acte de promulgation. " Aux termes de l'article 180-2 de la même loi, " Les actes prévus à l'article 140 dénommés " lois du pays " relatifs aux impôts et taxes sont publiés au Journal officiel de la Polynésie française et promulgués par le président de la Polynésie française au plus tard le lendemain de leur adoption. / Le président de la Polynésie française transmet l'acte de promulgation au haut-commissaire de la République. " Selon l'article 180-3 : " (...) II. _ A compter de la publication de l'acte de promulgation, les personnes physiques ou morales justifiant d'un intérêt à agir disposent d'un délai d'un mois pour déférer l'acte dénommé " loi du pays " relatif aux impôts et taxes au Conseil d'Etat ". L'article 180-4 prévoit que " Le Conseil d'Etat se prononce dans un délai de trois mois à compter de sa saisine. Il annule toute disposition contraire à la Constitution, aux lois organiques, aux engagements internationaux ou aux principes généraux du droit. "<br/>
<br/>
              4. Il résulte des dispositions citées aux points 2 et 3 que les actes dits " lois du pays " qui ne sont pas relatifs aux impôts et aux taxes ne peuvent, en principe, pas faire l'objet d'un recours par voie d'action après leur promulgation par le président de la Polynésie française. Il en va toutefois différemment quand l'acte dit " loi du pays " a été prématurément promulgué, que cette promulgation intervienne avant l'expiration du délai d'un mois prévu au premier alinéa de l'article 178 de la loi organique ou, si le Conseil d'Etat a été saisi, avant l'expiration du délai de trois mois prévu au I de l'article 177. <br/>
<br/>
              5. En cas de promulgation prématurée, si le Conseil d'Etat est saisi d'un recours dirigé seulement contre l'acte de promulgation, lequel peut être contesté au motif qu'il méconnait les exigences qui découlent de l'article 177 de la loi organique ou qu'il est entaché d'un vice propre, et si le Conseil d'Etat prononce l'annulation de cet acte, la " loi du pays " cesse d'être exécutoire et la publication qui a été faite de la " loi du pays " promulguée vaut publication pour information, ouvrant le délai de recours par voie d'action prévu par les dispositions citées au point 2 de l'article 176 de la loi organique. <br/>
<br/>
              6. Si, en cas de promulgation prématurée, le Conseil d'Etat est simultanément saisi de conclusions dirigées contre l'acte de promulgation et contre la " loi du pays " promulguée et s'il annule l'acte de promulgation, le recours dirigé contre la " loi du pays " est alors regardé comme un recours tendant à déclarer non conforme au bloc de légalité défini au III de l'article 176 de la loi organique la délibération adoptée par l'assemblée de la Polynésie française. S'il rejette les conclusions dirigées contre l'acte de promulgation, le recours dirigé contre la " loi du pays " présente le caractère d'un recours en annulation. <br/>
<br/>
              7. Enfin, si le Conseil d'Etat n'est saisi, dans le délai d'un mois suivant la publication de la " loi du pays " prématurément promulguée, que d'un recours par voie d'action contre la " loi du pays ", ce recours présente le caractère d'un recours en annulation. Il appartient alors au Conseil d'Etat d'annuler les dispositions de la " loi du pays " qu'il juge contraires au bloc de légalité voire, si ces dispositions ne sont pas séparables des autres dispositions de l'acte, d'en prononcer l'annulation totale.<br/>
<br/>
              8. Il ressort des pièces du dossier que la délibération n°2020-11 LP/APF sur la prévention et la gestion des menaces sanitaires graves et des situations d'urgence a été adoptée le 17 avril 2020 par l'assemblée de la Polynésie française. Le président de la Polynésie française a promulgué la " loi du pays " n° 2020-11 qui en procède dès le 21 avril 2020, laquelle a été publiée le même jour au Journal officiel de la Polynésie française. MM. D..., E..., C... et A... ont saisi le Conseil d'Etat d'une requête tendant à l'annulation de l'acte de promulgation de la " loi du pays " n° 2020-11 du 21 avril 2020 et de la " loi du pays " elle-même.<br/>
<br/>
              Sur la légalité de l'acte de promulgation attaqué : <br/>
<br/>
              9. Par l'acte contesté, le président de la Polynésie française a promulgué la " loi du pays " résultant de la délibération n° 2020-11 LP/APF sur la prévention et la gestion des menaces sanitaires graves et des situations d'urgence dès le 21 avril 2020, sans respecter ni la mesure de publicité ni le délai fixé par l'article 176 de la loi organique du 27 février 2004. <br/>
<br/>
              10. Toutefois, en raison des circonstances exceptionnelles résultant de l'épidémie de covid-19 sur le territoire français et dans le monde, lesquelles ont conduit à l'adoption de la loi du 23 mars 2020 d'urgence pour faire face à l'épidémie de covid-19 et à la déclaration de l'état d'urgence sanitaire à compter du 24 mars 2020 sur l'ensemble du territoire national, des difficultés particulières de prise en charge sanitaire dans les îles de la Polynésie française et de l'urgence qui s'attache à la possibilité, pour les autorités de la collectivité, de prendre les mesures propres à préserver la santé publique, la promulgation prématurée de la " loi du pays " contestée, par l'acte attaqué, laquelle, ainsi qu'il a été dit, ne prive pas les personnes intéressées de la possibilité d'exercer un recours, ne peut, dans les circonstances particulières de l'espèce, être tenue pour illégale. <br/>
<br/>
              11. Les requérants ne sont, dès lors, pas fondés à demander, pour ce motif, l'annulation de l'acte de promulgation qu'ils attaquent. <br/>
<br/>
              Sur la légalité de " loi du pays " attaquée : <br/>
<br/>
              12. Il résulte de ce qui a été dit au point 6 que les conclusions des requérants dirigées contre la " loi du pays " sur la prévention et la gestion des menaces sanitaires graves et des situations d'urgence doivent être regardées comme tendant à l'annulation de cette " loi du pays ".<br/>
<br/>
              13. L'article 13 de la loi organique du 27 février 2004 portant statut d'autonomie de la Polynésie française dispose que " les autorités de la Polynésie française sont compétentes dans toutes les matières qui ne sont pas dévolues à l'Etat par l'article 14 ". Aux termes de l'article 14 de cette même loi, " les autorités de l'Etat son compétentes sans les seules matières suivantes : (...) 6° Sécurité et ordre publics, notamment maintien de l'ordre ; prohibitions à l'importation et à l'exportation qui relèvent de l'ordre public et des engagements internationaux ratifiés par la France ; réglementation des fréquences radioélectriques ; préparation des mesures de sauvegarde, élaboration et mise en oeuvre des plans opérationnels et des moyens de secours nécessaires pour faire face aux risques majeurs et aux catastrophes ; coordination et réquisition des moyens concourant à la sécurité civile (...) ". Il ressort de ces dispositions que si l'Etat est compétent en matière de préparation des mesures de sauvegarde, d'élaboration et de mise en oeuvre des plans opérationnels et des moyens de secours nécessaires pour faire face aux risques majeurs et aux catastrophes, les questions de santé publique relèvent de la compétence de la Polynésie française. <br/>
<br/>
              14. L'article unique de la " loi du pays " attaquée dispose qu'" en cas de menace grave ou de crise sanitaire grave appelant des mesures d'urgence, le conseil des ministres peut, par arrêté motivé, prescrire dans l'intérêt de la santé publique toute mesure réglementaire proportionnée aux risques courus et appropriés aux circonstances de temps et de lieu afin de prévenir et de limiter les conséquences possibles sur la santé de la population de Polynésie française. / Les mesures réglementaires prises par le conseil des ministres font immédiatement l'objet d'une information du représentant de l'Etat en Polynésie française. Elles ne portent pas atteinte à la confidentialité des données recueillies à l'égard des tiers. / Le Président de la Polynésie française informe le représentant de l'Etat des actions entreprises et des résultats obtenus en application de la présente loi du pays ". <br/>
<br/>
              15. Contrairement à ce que soutiennent les requérants, ces dispositions, qui ont pour objet de permettre au gouvernement de la Polynésie française de prendre par arrêté des mesures sanitaires permettant de prévenir et de limiter les conséquences possibles sur la santé de la population polynésienne d'une crise sanitaire grave, ne se rattachent pas, pour l'application de la loi organique du 27 février 2004, à la sécurité civile mais à la santé publique. Elles entrent ainsi dans le champ de compétence de la Polynésie française. Par suite, doit être écarté le moyen tiré de ce que la " loi du pays " attaquée méconnaîtrait la répartition des compétences entre l'Etat et la Polynésie française résultant des dispositions des articles 13 et 14 de la loi organique du 27 février 2004 portant statut d'autonomie de la Polynésie française. <br/>
<br/>
              16. Il résulte de tout ce qui précède, sans qu'il soit besoin de se prononcer sur les fins de non-recevoir opposées en défense, que les requérants ne sont fondés ni à demander l'annulation de l'acte par lequel le président de la Polynésie française a promulgué la " loi du pays " n° 2020-11 du 21 avril 2020, ni à demander l'annulation de cette " loi du pays ". <br/>
<br/>
              17. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la Polynésie française et à l'assemblée de la Polynésie française, qui ne sont pas, dans la présente instance, les parties perdantes. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées au titre de ces dispositions par la Polynésie française et l'assemblée de la Polynésie française.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête de M. D... et autres est rejetée. <br/>
Article 2 : Les conclusions présentées par la Polynésie française et l'assemblée de la Polynésie française au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à M. H... D..., premier dénommé pour l'ensemble des requérants, au président de la Polynésie française, au président de l'assemblée de la Polynésie française et au haut-commissaire de la République en Polynésie française. <br/>
Copie en sera adressée au ministre des outre-mer.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">46-01-02-02 OUTRE-MER. DROIT APPLICABLE. STATUTS. POLYNÉSIE FRANÇAISE. - LOI DU PAYS (LOI ORGANIQUE DU 27 FÉVRIER 2004) - I) RECOURS PAR VOIE D'ACTION APRÈS LA PROMULGATION - 1) PRINCIPE - EXCLUSION [RJ1] - 2) EXCEPTION - LOI DU PAYS PROMULGUÉE PRÉMATURÉMENT - A) RECOURS CONTRE LE SEUL ACTE DE PROMULGATION - CONSÉQUENCE D'UNE ANNULATION [RJ2] - LOI DU PAYS CESSANT D'ÊTRE EXÉCUTOIRE ET PUBLICATION OUVRANT LE DÉLAI DE RECOURS EN DÉCLARATION D'ILLÉGALITÉ (ART. 176 DE LA LOI ORGANIQUE) - B) RECOURS CONTRE L'ACTE DE PROMULGATION ET LA LOI DU PAYS - I) CAS D'ANNULATION DE L'ACTE DE PROMULGATION - RECOURS CONTRE LA LOI DU PAYS REGARDÉ COMME UN RECOURS EN DÉCLARATION D'ILLÉGALITÉ - II) ABSENCE D'ANNULATION DE L'ACTE DE PROMULGATION - RECOURS EN ANNULATION - C) RECOURS CONTRE LA SEULE LOI DU PAYS - RECOURS EN ANNULATION - II) CIRCONSTANCES EXCEPTIONNELLES JUSTIFIANT LA PROMULGATION PRÉMATURÉE D'UNE LOI DU PAYS - EXISTENCE, EN L'ESPÈCE [RJ3] - III) COMPÉTENCE DE LA POLYNÉSIE FRANÇAISE - INCLUSION - MESURES PERMETTANT DE PRÉVENIR ET DE LIMITER LES EFFETS D'UNE CRISE SANITAIRE GRAVE SUR LA SANTÉ DE LA POPULATION POLYNÉSIENNE.
</SCT>
<ANA ID="9A"> 46-01-02-02 I) 1) Il résulte des articles 176, 177, 178, 180, 180-1 et 180-2 de la loi organique n° 2004-192 du 27 février 2004 que les actes dits lois du pays qui ne sont pas relatifs aux impôts et aux taxes ne peuvent, en principe, pas faire l'objet d'un recours par voie d'action après leur promulgation par le président de la Polynésie française.,,,2) Il en va toutefois différemment quand l'acte dit loi du pays a été prématurément promulgué, que cette promulgation intervienne avant l'expiration du délai d'un mois prévu au premier alinéa de l'article 178 de la loi organique ou, si le Conseil d'Etat a été saisi, avant l'expiration du délai de trois mois prévu au I de l'article 177.,,,a) En cas de promulgation prématurée, si le Conseil d'Etat est saisi d'un recours dirigé seulement contre l'acte de promulgation, lequel peut être contesté au motif qu'il méconnait les exigences qui découlent de l'article 177 de la loi organique ou qu'il est entaché d'un vice propre, et si le Conseil d'Etat prononce l'annulation de cet acte, la loi du pays cesse d'être exécutoire et la publication qui a été faite de la loi du pays promulguée vaut publication pour information, ouvrant le délai de recours par voie d'action prévu par l'article 176 de la loi organique.,,,b) i) Si, en cas de promulgation prématurée, le Conseil d'Etat est simultanément saisi de conclusions dirigées contre l'acte de promulgation et contre la loi du pays promulguée et s'il annule l'acte de promulgation, le recours dirigé contre la loi du pays est alors regardé comme un recours tendant à déclarer non conforme au bloc de légalité défini au III de l'article 176 de la loi organique la délibération adoptée par l'assemblée de la Polynésie française.... ,,ii) S'il rejette les conclusions dirigées contre l'acte de promulgation, le recours dirigé contre la loi du pays présente le caractère d'un recours en annulation.... ,,c) Enfin, si le Conseil d'Etat n'est saisi, dans le délai d'un mois suivant la publication de la loi du pays prématurément promulguée, que d'un recours par voie d'action contre la loi du pays, ce recours présente le caractère d'un recours en annulation. Il appartient alors au Conseil d'Etat d'annuler les dispositions de la loi du pays qu'il juge contraires au bloc de légalité voire, si ces dispositions ne sont pas séparables des autres dispositions de l'acte, d'en prononcer l'annulation totale.... ,,II) Le président de la Polynésie française a promulgué la loi du pays résultant de la délibération n° 2020-11 LP/APF sur la prévention et la gestion des menaces sanitaires graves et des situations d'urgence sans respecter ni la mesure de publicité ni le délai fixé par l'article 176 de la loi organique du 27 février 2004.... ,,Toutefois, en raison des circonstances exceptionnelles résultant de l'épidémie de covid-19 sur le territoire français et dans le monde, lesquelles ont conduit à l'adoption de la loi du 23 mars 2020 d'urgence pour faire face à l'épidémie de covid-19 et à la déclaration de l'état d'urgence sanitaire à compter du 24 mars 2020 sur l'ensemble du territoire national, des difficultés particulières de prise en charge sanitaire dans les îles de la Polynésie française et de l'urgence qui s'attache à la possibilité, pour les autorités de la collectivité, de prendre les mesures propres à préserver la santé publique, la promulgation prématurée de la loi du pays contestée, par l'acte attaqué, laquelle ne prive pas les personnes intéressées de la possibilité d'exercer un recours, ne peut, dans les circonstances particulières de l'espèce, être tenue pour illégale....  ,,III) Il ressort des articles 13 et 14 de la loi organique du 27 février 2004 que si l'Etat est compétent en matière de préparation des mesures de sauvegarde, d'élaboration et de mise en oeuvre des plans opérationnels et des moyens de secours nécessaires pour faire face aux risques majeurs et aux catastrophes, les questions de santé publique relèvent de la compétence de la Polynésie française.,,,Loi du pays disposant qu'en cas de menace grave ou de crise sanitaire grave appelant des mesures d'urgence, le conseil des ministres peut, par arrêté motivé, prescrire dans l'intérêt de la santé publique toute mesure réglementaire proportionnée aux risques courus et appropriés aux circonstances de temps et de lieu afin de prévenir et de limiter les conséquences possibles sur la santé de la population de Polynésie française.,,,Ces dispositions, qui ont pour objet de permettre au gouvernement de la Polynésie française de prendre par arrêté des mesures sanitaires permettant de prévenir et de limiter les conséquences possibles sur la santé de la population polynésienne d'une crise sanitaire grave, ne se rattachent pas, pour l'application de la loi organique du 27 février 2004, à la sécurité civile mais à la santé publique. Elles entrent ainsi dans le champ de compétence de la Polynésie française.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 7 novembre 2018,,et,, n° 420284 et s., T. p. 796 ; CE, 13 mars 2019, Toumaniantz et syndicat de la fonction publique, n° 426435, T. p. 861.,,[RJ2] Rappr., sur l'absence d'incidence de l'illégalité de l'acte de promulgation sur la légalité de la loi du pays promulguée, CE, 5 décembre 2008, Flosse et autres, n° 320412, T. p. 826.,,[RJ3] Rappr., s'agissant du respect des règles de procédure prévues par l'article 46 de la Constitution, Cons. const., 26 mars 2020, n° 2020-799 DC, Loi organique d'urgence pour faire face à l'épidémie de covid-19.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
