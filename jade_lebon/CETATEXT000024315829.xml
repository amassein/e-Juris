<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000024315829</ID>
<ANCIEN_ID>JG_L_2011_06_000000335072</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/24/31/58/CETATEXT000024315829.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème et 9ème sous-sections réunies, 29/06/2011, 335072, Publié au recueil Lebon</TITRE>
<DATE_DEC>2011-06-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>335072</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème et 9ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP POTIER DE LA VARDE, BUK LAMENT</AVOCATS>
<RAPPORTEUR>M. Gilles Pellissier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Delphine Hedary</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2011:335072.20110629</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 28 décembre 2009 et 29 mars 2010 au secrétariat du contentieux du Conseil d'Etat, présentés pour Mme B...A..., demeurant..., ; Mme A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 08BX00083 du 19 octobre 2009 par lequel la cour administrative d'appel de Bordeaux a rejeté sa requête tendant à l'annulation du jugement du 7 novembre 2007 du tribunal administratif de Toulouse rejetant ses conclusions en annulation de la décision du 5 octobre 2006 par laquelle la directrice des archives de France a refusé de l'autoriser à consulter à titre dérogatoire le dossier complet d'accusation ayant abouti à l'arrêt n° 279 rendu par la chambre civique de la cour de justice de Toulouse le 6 juillet 1946 ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code du patrimoine ;<br/>
<br/>
              Vu le décret n° 79-1038 du 3 décembre 1979 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Gilles Pellissier, Maître des Requêtes,<br/>
<br/>
              - les observations de la SCP Potier de la Varde, Buk Lament, avocat de Mme A...,<br/>
<br/>
              - les conclusions de Mme Delphine Hedary, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Potier de la Varde, Buk Lament, avocat de MmeA... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que Mme A...a sollicité le 21 avril 2006 du service des archives départementales de la Haute-Garonne l'autorisation de consulter à titre dérogatoire le dossier complet d'accusation de son père ayant abouti à l'arrêt n° 279 rendu par la chambre civique de la cour de justice de Toulouse le 6 juillet 1946 ; qu'après avoir recueilli l'avis du procureur général près la cour d'appel de Toulouse, qui était l'autorité qui avait effectué le versement du dossier, et en raison de son sens négatif, la directrice des archives de France a, par décision du 11 avril 2006, rejeté la demande de MmeA... ; qu'après avoir saisi la Commission d'accès aux documents administratifs, qui a rendu un avis favorable à la communication de ces documents, Mme A... a renouvelé sa demande ; que le procureur général près la cour d'appel de Toulouse ayant maintenu son opposition à la consultation demandée, la directrice des archives de France a, par décision du 5 octobre 2006, renouvelé son refus ; que Mme A...se pourvoit en cassation contre l'arrêt du 19 octobre 2009 par lequel la cour administrative d'appel de Bordeaux a rejeté l'appel qu'elle avait interjeté contre le jugement du tribunal administratif de Toulouse du 7 novembre 2007 ayant rejeté ses conclusions en annulation de cette décision ;<br/>
<br/>
              Considérant qu'aux termes de l'article L. 213-2 du code du patrimoine, dans sa rédaction applicable à la date de la décision attaquée : " Le délai au-delà duquel les documents d'archives publiques peuvent être librement consultés est porté à (...) / c) Cent ans à compter de la date de l'acte ou de la clôture du dossier pour les documents relatifs aux affaires portées devant les juridictions (...) " ; que l'article L. 213-3 du même code disposait que : " Sous réserve, en ce qui concerne les minutes des notaires, des dispositions de l'article 23 de la loi du 25 ventôse An XI, l'administration des archives peut autoriser la consultation des documents d'archives publiques avant l'expiration des délais prévus au troisième alinéa de l'article L. 213-1 et à l'article L. 213-2 (...) " ; qu'aux termes de l'article 2 du décret du 3 décembre 1979 relatif à la communication des documents d'archives publiques : " Toute demande de dérogation aux conditions de communicabilité des documents d'archives publiques est soumise au ministre chargé de la culture (direction des archives de France) qui statue, après accord de l'autorité qui a effectué le versement ou qui assure la conservation des archives (...) " ; que si le refus de l'accord de l'autorité qui a effectué le versement ou qui assure la conservation des archives, qui s'impose à l'autorité compétente pour statuer sur la demande d'autorisation, ne constitue pas une décision susceptible de recours, des moyens relatifs à sa régularité et son bien-fondé peuvent, quel que soit le sens de la décision prise par l'autorité compétente pour statuer sur la demande d'autorisation, être invoqués devant le juge saisi de cette décision ;<br/>
<br/>
              Considérant, en premier lieu, que la cour administrative d'appel, qui disposait du pouvoir de se faire communiquer les archives demandées, sans les soumettre au contradictoire, afin de fonder son appréciation des conséquences de leur communication, n'était pas tenue de motiver son arrêt sur l'usage qu'elle faisait de ce pouvoir d'instruction ; qu'ainsi le moyen tiré de ce qu'elle aurait insuffisamment motivé son arrêt sur ce point ne peut qu'être écarté ;<br/>
<br/>
              Considérant, en deuxième lieu, que, pour motiver sa décision, la directrice des archives de France a expressément repris les motifs de l'avis négatif du procureur général près la cour d'appel de Toulouse, fondé sur les risques d'atteinte aux secrets protégés par la loi et en particulier au secret de la vie privée ; qu'en estimant, par une appréciation souveraine, que la décision attaquée était ainsi suffisamment motivée, la cour administrative d'appel n'a pas dénaturé les pièces du dossier ; <br/>
<br/>
              Considérant, en troisième lieu, que la consultation anticipée d'archives publiques ne peut être autorisée, en application des dispositions des articles L. 213-1, L. 213-3 et L. 213-4 du code du patrimoine, que si la satisfaction de l'intérêt légitime de celui qui en fait la demande ne conduit pas à porter une atteinte excessive aux intérêts que la loi a entendu protéger ;<br/>
<br/>
              Considérant qu'il ressort des motifs de l'arrêt attaqué que, contrairement à ce que soutient la requérante, la cour administrative d'appel a tenu compte, pour apprécier le bien-fondé du refus d'autoriser Mme A...à consulter les documents archivés, de l'ensemble des intérêts en présence, c'est-à-dire des risques d'atteinte aux secrets protégés par la loi, d'une part, et des intérêts légitimes du demandeur, d'autre part ; qu'ainsi le moyen tiré de ce qu'elle aurait commis une erreur de droit en ne mettant pas en balance les intérêts en présence et en ne tenant pas compte des intérêts légitimes de la requérante ne peut qu'être écarté ;<br/>
<br/>
              Considérant qu'en jugeant que cette autorité n'avait pas commis d'erreur manifeste d'appréciation en estimant que, malgré la légitimité de la demande, la communication de ces documents présentait des risques excessifs d'atteinte aux secrets protégés par la loi, en particulier au secret de la vie privée, la cour administrative d'appel a porté sur les faits, qu'elle n'a pas dénaturés, une appréciation souveraine insusceptible d'être discutée devant le juge de cassation ;<br/>
<br/>
              Considérant qu'il résulte de ce qui précède que Mme A...n'est pas fondée à demander l'annulation de l'arrêt de la cour administrative d'appel de Bordeaux ; que son pourvoi ne peut, par suite, qu'être rejeté y compris, par voie de conséquence, ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>				D E C I D E  :<br/>
              				--------------<br/>
<br/>
Article 1er : Le pourvoi de Mme A...est rejeté.<br/>
Article 2 : La présente décision sera notifiée à Mme B...A...et au ministre de la culture et de la communication.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">26-06-03 DROITS CIVILS ET INDIVIDUELS. ACCÈS AUX DOCUMENTS ADMINISTRATIFS. DROIT D'ACCÈS ET DE VÉRIFICATION SUR UN FONDEMENT AUTRE QUE CELUI DES LOIS DU 17 JUILLET 1978 ET DU 6 JANVIER 1978. - CONTENTIEUX DE LA LÉGISLATION SUR L'ACCÈS AUX ARCHIVES - 1) APPEL - EXISTENCE (SOL. IMPL.) - 2) POUVOIRS D'INSTRUCTION DU JUGE - COMMUNICATION DES ARCHIVES DEMANDÉES - EXISTENCE - SOUMISSION AU CONTRADICTOIRE - ABSENCE [RJ1] - MOTIVATION DE L'USAGE DE CE POUVOIR D'INSTRUCTION - ABSENCE - 3) CRITÈRES D'EXAMEN DES DEMANDES D'ACCÈS ANTICIPÉ AUX ARCHIVES - 4) CONTRÔLE DU JUGE DE L'EXCÈS DE POUVOIR - CONTRÔLE DE L'ERREUR DE DROIT SUR L'APPRÉCIATION DES INTÉRÊTS EN CAUSE - CONTRÔLE DE L'ERREUR MANIFESTE D'APPRÉCIATION SUR LA PONDÉRATION DE CES INTÉRÊTS.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-04-01-03 PROCÉDURE. INSTRUCTION. POUVOIRS GÉNÉRAUX D'INSTRUCTION DU JUGE. PRODUCTION ORDONNÉE. - CONTENTIEUX DE LA LÉGISLATION SUR L'ACCÈS AUX ARCHIVES - POUVOIRS D'INSTRUCTION DU JUGE - COMMUNICATION DES ARCHIVES DEMANDÉES - EXISTENCE - SOUMISSION AU CONTRADICTOIRE - ABSENCE [RJ1] - MOTIVATION DE L'USAGE DE CE POUVOIR D'INSTRUCTION - ABSENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-04-03-01 PROCÉDURE. INSTRUCTION. CARACTÈRE CONTRADICTOIRE DE LA PROCÉDURE. COMMUNICATION DES MÉMOIRES ET PIÈCES. - CONTENTIEUX DE LA LÉGISLATION SUR L'ACCÈS AUX ARCHIVES - POUVOIRS D'INSTRUCTION DU JUGE - COMMUNICATION DES ARCHIVES DEMANDÉES - EXISTENCE - SOUMISSION AU CONTRADICTOIRE - ABSENCE [RJ1] - MOTIVATION DE L'USAGE DE CE POUVOIR D'INSTRUCTION - ABSENCE.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">54-07-02 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. CONTRÔLE DU JUGE DE L'EXCÈS DE POUVOIR. - CONTENTIEUX DE LA LÉGISLATION SUR L'ACCÈS AUX ARCHIVES - CONTRÔLE DE L'ERREUR DE DROIT SUR L'APPRÉCIATION DES INTÉRÊTS EN CAUSE - CONTRÔLE DE L'ERREUR MANIFESTE D'APPRÉCIATION SUR LA PONDÉRATION DE CES INTÉRÊTS.
</SCT>
<SCT ID="8E" TYPE="PRINCIPAL">54-08-01-01 PROCÉDURE. VOIES DE RECOURS. APPEL. RECEVABILITÉ. - EXISTENCE - CONTENTIEUX DE LA LÉGISLATION SUR L'ACCÈS AUX ARCHIVES (SOL. IMPL.).
</SCT>
<ANA ID="9A"> 26-06-03 1) La voie de l'appel est ouverte dans le contentieux de la législation sur l'accès aux archives.... ...2) Le juge dispose du pouvoir de se faire communiquer les archives demandées, sans les soumettre au contradictoire, afin de fonder son appréciation des conséquences de leur communication, et n'est pas tenu de motiver son arrêt sur l'usage qu'il fait de ce pouvoir d'instruction.... ...3) La consultation anticipée d'archives publiques ne peut être autorisée, en application des dispositions des articles L. 213-1, L. 213-3 et L. 213-4 du code du patrimoine, que si la satisfaction de l'intérêt légitime de celui qui en fait la demande ne conduit pas à porter une atteinte excessive aux intérêts que la loi a entendu protéger.... ...4) Le juge de l'excès de pouvoir exerce un contrôle de l'erreur de droit sur l'appréciation des intérêts en présence. Il exerce un contrôle de l'erreur manifeste d'appréciation sur la pondération de ces intérêts.</ANA>
<ANA ID="9B"> 54-04-01-03 Le juge dispose du pouvoir de se faire communiquer les archives demandées, sans les soumettre au contradictoire, afin de fonder son appréciation des conséquences de leur communication, et n'est pas tenu de motiver son arrêt sur l'usage qu'il fait de ce pouvoir d'instruction.</ANA>
<ANA ID="9C"> 54-04-03-01 Le juge dispose du pouvoir de se faire communiquer les archives demandées, sans les soumettre au contradictoire, afin de fonder son appréciation des conséquences de leur communication, et n'est pas tenu de motiver son arrêt sur l'usage qu'il fait de ce pouvoir d'instruction.</ANA>
<ANA ID="9D"> 54-07-02 La consultation anticipée d'archives publiques ne peut être autorisée, en application des dispositions des articles L. 213-1, L. 213-3 et L. 213-4 du code du patrimoine, que si la satisfaction de l'intérêt légitime de celui qui en fait la demande ne conduit pas à porter une atteinte excessive aux intérêts que la loi a entendu protéger. Le juge de l'excès de pouvoir exerce un contrôle de l'erreur de droit sur l'appréciation des intérêts en présence. Il exerce un contrôle de l'erreur manifeste d'appréciation sur la pondération de ces intérêts.</ANA>
<ANA ID="9E"> 54-08-01-01 La voie de l'appel est ouverte dans le contentieux de la législation sur l'accès aux archives.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr. CE, Section, 23 décembre 1988, Banque de France c/ Huberschwiller, n° 95310, p. 464.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
