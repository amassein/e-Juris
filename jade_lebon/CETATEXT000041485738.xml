<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041485738</ID>
<ANCIEN_ID>JG_L_2020_01_000000426569</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/48/57/CETATEXT000041485738.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 27/01/2020, 426569</TITRE>
<DATE_DEC>2020-01-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>426569</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>HAAS ; SCP THOUIN-PALAT, BOUCARD</AVOCATS>
<RAPPORTEUR>M. Marc Firoud</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:426569.20200127</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B... A... a demandé au tribunal administratif de Cergy-Pontoise d'annuler l'arrêté du 7 avril 2015 du maire de Beaumont-sur-Oise prononçant son exclusion temporaire pour une durée de deux jours. Par un jugement n° 1504136 du 11 octobre 2016, ce tribunal a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 16VE03117 du 25 octobre 2018, la cour administrative d'appel de Versailles a rejeté l'appel formé par Mme A... contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 26 décembre 2018 et 27 mars 2019 au secrétariat du contentieux du Conseil d'Etat, Mme A... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Beaumont-sur-Oise la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - la loi n° 84-53 du 26 janvier 1984 ;<br/>
              - le décret n° 85-565 du 30 mai 1985 ;<br/>
              - le code de justice administrative. <br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Marc Firoud, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Haas, avocat de Mme A... et à la SCP Thouin-Palat, Boucard, avocat de la commune de Beaumont-sur-Oise ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que Mme A..., adjointe administrative territoriale de 2ème classe employée par la commune de Beaumont-sur-Oise, représentante du personnel au comité technique, a demandé au tribunal administratif de Cergy-Pontoise d'annuler la décision prise, le 7 avril 2015, par le maire de Beaumont-sur-Oise, prononçant à son encontre la sanction d'exclusion temporaire de fonctions de deux jours. Par un jugement du 11 octobre 2016, le tribunal administratif de Cergy-Pontoise a rejeté sa demande. Par un arrêt du 25 octobre 2018, contre lequel elle se pourvoit en cassation, la cour administrative d'appel de Versailles a rejeté son appel.<br/>
<br/>
              2. Si les agents publics qui exercent des fonctions syndicales bénéficient de la liberté d'expression particulière qu'exigent l'exercice de leur mandat et la défense des intérêts des personnels qu'ils représentent, cette liberté doit être conciliée avec le respect de leurs obligations déontologiques. En particulier, des propos ou un comportement agressifs à l'égard d'un supérieur hiérarchique ou d'un autre agent sont susceptibles, alors même qu'ils ne seraient pas constitutifs d'une infraction pénale, d'avoir le caractère d'une faute de nature à justifier une sanction disciplinaire.<br/>
<br/>
              3. La cour a relevé que, au cours d'une réunion du comité technique de la commune de Beaumont-sur-Oise, Mme A... avait eu un comportement et tenu des propos particulièrement irrespectueux et agressifs à l'égard la directrice générale des services, présente en qualité d'experte. Il résulte de ce qui a été dit ci-dessus qu'en jugeant que ces propos et ce comportement étaient susceptibles de justifier, même s'ils étaient le fait d'une représentante du personnel dans le cadre de l'exercice de son mandat et alors même qu'ils ne caractériseraient pas une infraction pénale, une sanction disciplinaire, la cour n'a pas commis d'erreur de droit.<br/>
<br/>
              4. Par ailleurs, en retenant l'existence, dans les circonstances de l'espèce qui lui était soumise, d'une faute de nature à justifier une sanction disciplinaire, la cour, dont l'arrêt est suffisamment motivé, n'a pas commis d'erreur de qualification juridique des faits.   <br/>
<br/>
              5. Il résulte de ce qui précède que Mme A... n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque.<br/>
<br/>
              6. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la commune de Beaumont-sur-Oise qui n'est pas, dans la présente instance, la partie perdante. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par la commune au titre des mêmes dispositions.  <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de Mme A... est rejeté.<br/>
Article 2 : Les conclusions de la commune de Beaumont-sur-Oise présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à Mme B... A... et à la commune de Beaumont-sur-Oise. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-07-09 FONCTIONNAIRES ET AGENTS PUBLICS. STATUTS, DROITS, OBLIGATIONS ET GARANTIES. DROIT SYNDICAL. - AGENTS PUBLICS EXERÇANT DES FONCTIONS SYNDICALES - EXIGENCE DE CONCILIATION ENTRE LEUR LIBERTÉ D'EXPRESSION ET LE RESPECT DE LEURS OBLIGATIONS DÉONTOLOGIQUES - CONSÉQUENCE - PROPOS AGRESSIFS À L'ÉGARD D'UN SUPÉRIEUR HIÉRARCHIQUE - FAITS SUSCEPTIBLES DE JUSTIFIER UNE SANCTION DISCIPLINAIRE, ALORS MÊME QU'ILS NE SERAIENT PAS CONSTITUTIFS D'UNE INFRACTION PÉNALE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">36-07-11-01 FONCTIONNAIRES ET AGENTS PUBLICS. STATUTS, DROITS, OBLIGATIONS ET GARANTIES. OBLIGATIONS DES FONCTIONNAIRES. DEVOIR DE RÉSERVE. - AGENT PUBLIC EXERÇANT DES FONCTIONS SYNDICALES AYANT TENU DES PROPOS AGRESSIFS À L'ÉGARD D'UN SUPÉRIEUR HIÉRARCHIQUE - FAITS SUSCEPTIBLES D'AVOIR LE CARACTÈRE D'UNE FAUTE DE NATURE À JUSTIFIER UNE SANCTION DISCIPLINAIRE, ALORS MÊME QU'ILS NE SERAIENT PAS CONSTITUTIFS D'UNE INFRACTION PÉNALE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">36-09-03-01 FONCTIONNAIRES ET AGENTS PUBLICS. DISCIPLINE. MOTIFS. FAITS DE NATURE À JUSTIFIER UNE SANCTION. - PROPOS AGRESSIFS TENUS PAR UN AGENT PUBLIC EXERÇANT DES FONCTIONS SYNDICALES À L'ÉGARD D'UN SUPÉRIEUR HIÉRARCHIQUE - EXISTENCE, ALORS MÊME QUE CES PROPOS NE SERAIENT PAS CONSTITUTIFS D'UNE INFRACTION PÉNALE.
</SCT>
<ANA ID="9A"> 36-07-09 Si les agents publics qui exercent des fonctions syndicales bénéficient de la liberté d'expression particulière qu'exigent l'exercice de leur mandat et la défense des intérêts des personnels qu'ils représentent, cette liberté doit être conciliée avec le respect de leurs obligations déontologiques. En particulier, des propos ou un comportement agressifs à l'égard d'un supérieur hiérarchique ou d'un autre agent sont susceptibles, alors même qu'ils ne seraient pas constitutifs d'une infraction pénale, d'avoir le caractère d'une faute de nature à justifier une sanction disciplinaire.</ANA>
<ANA ID="9B"> 36-07-11-01 Si les agents publics qui exercent des fonctions syndicales bénéficient de la liberté d'expression particulière qu'exigent l'exercice de leur mandat et la défense des intérêts des personnels qu'ils représentent, cette liberté doit être conciliée avec le respect de leurs obligations déontologiques. En particulier, des propos ou un comportement agressifs à l'égard d'un supérieur hiérarchique ou d'un autre agent sont susceptibles, alors même qu'ils ne seraient pas constitutifs d'une infraction pénale, d'avoir le caractère d'une faute de nature à justifier une sanction disciplinaire.</ANA>
<ANA ID="9C"> 36-09-03-01 Si les agents publics qui exercent des fonctions syndicales bénéficient de la liberté d'expression particulière qu'exigent l'exercice de leur mandat et la défense des intérêts des personnels qu'ils représentent, cette liberté doit être conciliée avec le respect de leurs obligations déontologiques. En particulier, des propos ou un comportement agressifs à l'égard d'un supérieur hiérarchique ou d'un autre agent sont susceptibles, alors même qu'ils ne seraient pas constitutifs d'une infraction pénale, d'avoir le caractère d'une faute de nature à justifier une sanction disciplinaire.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
