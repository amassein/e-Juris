<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000024736687</ID>
<ANCIEN_ID>JG_L_2011_10_000000332011</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/24/73/66/CETATEXT000024736687.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème et 5ème sous-sections réunies, 26/10/2011, 332011</TITRE>
<DATE_DEC>2011-10-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>332011</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème et 5ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Jacques Arrighi de Casanova</PRESIDENT>
<AVOCATS>SCP BARADUC, DUHAMEL ; SCP POTIER DE LA VARDE, BUK LAMENT</AVOCATS>
<RAPPORTEUR>M. Louis Dutheillet de Lamothe</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Gaëlle Dumortier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2011:332011.20111026</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 15 septembre et 9 novembre 2009 au secrétariat du contentieux du Conseil d'Etat, présentés pour l'UNIVERSITE DE PAU ET DES PAYS DE L'ADOUR, dont le siège est avenue de l'Université, à Pau, Cedex B. P. 576 (64012) ; l'université requérante demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 08BX01339 du 15 juillet 2009 par lequel la cour administrative d'appel de Bordeaux a rejeté sa requête tendant à l'annulation du jugement n° 0600542 du 11 mars 2008 du tribunal administratif de Pau annulant la décision du 25 janvier 2006 de son président qui avait rejeté la demande d'inscription de Mme Françoise A en qualité de candidate à l'obtention du diplôme d'habilitation à diriger des recherches ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de Mme A une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'éducation ; <br/>
<br/>
              Vu l'arrêté du 23 novembre 1988 relatif à l'habilitation à diriger des recherches ;<br/>
<br/>
              Vu l'arrêté du 25 avril 2002 relatif aux études doctorales ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Louis Dutheillet de Lamothe, Auditeur,  <br/>
<br/>
              - les observations de la SCP Baraduc, Duhamel, avocat de l'UNIVERSITE DE PAU ET DES PAYS DE L'ADOUR et de la SCP Potier de la Varde, Buk Lament, avocat de Mme A, <br/>
<br/>
              - les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Baraduc, Duhamel, avocat de l'UNIVERSITE DE PAU ET DES PAYS DE L'ADOUR et à la SCP Potier de la Varde, Buk Lament, avocat de Mme A, <br/>
<br/>
<br/>
<br/>
              Considérant qu'aux termes du dernier alinéa de l'article L. 612-7 du code de l'éducation : "  L'aptitude à diriger des recherches est sanctionnée par une habilitation délivrée dans des conditions fixées par arrêté du ministre chargé de l'enseignement supérieur " ; qu'aux termes de l'article 1er de l'arrêté du 23 novembre 1988, pris pour l'application de ces dispositions : " L'habilitation à diriger des recherches sanctionne la reconnaissance du haut niveau scientifique du candidat, du caractère original de sa démarche dans le domaine de la science, de son aptitude à maîtriser une stratégie de recherche dans un domaine scientifique ou technologique suffisamment large et de sa capacité à encadrer de jeunes chercheurs (...) " ; que, selon l'article 3 du même arrêté : " Les candidats doivent être titulaires : / d'un diplôme de doctorat ou / d'un diplôme de docteur permettant l'exercice de la médecine, de l'odontologie, de la pharmacie et de la médecine vétérinaire et d'un diplôme d'études approfondies,/ ou justifier d'un diplôme, de travaux ou d'une expérience d'un niveau équivalent au doctorat . (...) Les demandes d'inscription sont examinées par le président ou le directeur de l'établissement, qui statue sur proposition du conseil scientifique siégeant en formation restreinte aux personnalités habilitées à diriger des recherches et après avis du directeur de recherche si le candidat en a un. " ; qu'en vertu du quatrième alinéa de l'article 11 de l'arrêté du 25 avril 2002 relatif aux études doctorales, alors en vigueur, les fonctions de directeur ou codirecteur de thèse peuvent, notamment, être exercées par des " personnalités choisies en raison de leur compétence scientifique par le chef d'établissement, sur proposition du directeur de l'école doctorale et après avis du conseil scientifique de l'établissement. " ; <br/>
<br/>
              Considérant que les candidats à l'habilitation à diriger des recherches doivent être autorisés à se présenter à cet examen par une décision du président ou du directeur de l'établissement qui statue sur proposition du conseil scientifique siégeant en formation restreinte aux personnalités habilités à diriger des recherches ; qu'il résulte des dispositions de l'arrêté du 23 novembre 1988 citées ci-dessus que, s'il appartient au jury d'apprécier définitivement la valeur des candidats en vue de leur habilitation à diriger des recherches, il revient au conseil scientifique, qui n'agit pas dans ce cadre en qualité de jury, de vérifier si  le candidat, au regard, d'une part, des conditions posées par l'article 3 de cet arrêté, d'autre part, de son niveau scientifique, de la qualité de ses recherches et de sa capacité à encadrer de jeunes chercheurs, remplit les conditions requises pour être inscrit à l'examen conduisant à la délivrance de l'habilitation à diriger des recherches ; qu'en particulier, le conseil scientifique, pour refuser de proposer l'inscription à cet examen, peut prendre en compte, notamment, le fait que le candidat n'a pas suffisamment encadré ou co-encadré de travaux de thèses, comme le lui permet, à certaines conditions, le quatrième alinéa de l'article 11 de l'arrêté du 25 avril 2002 ; <br/>
<br/>
              Considérant que, pour confirmer le jugement du tribunal administratif de Pau annulant la décision du 25 janvier 2006 du président de l'UNIVERSITE DE PAU ET DES PAYS DE L'ADOUR qui avait rejeté la demande d'inscription de Mme A à l'examen conduisant à la délivrance du diplôme d'habilitation à diriger des recherches, la cour administrative d'appel de Bordeaux a jugé que le conseil scientifique de cette université ne pouvait légalement, pour refuser de proposer cette inscription, se fonder sur le caractère limité de son activité de co-encadrement de thèses ; qu'en statuant ainsi, alors que la capacité d'encadrement des jeunes chercheurs figure au nombre des critères que le conseil scientifique peut prendre en considération, la cour a commis une erreur de droit ; que par suite, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, l'UNIVERSITE DE PAU ET DES PAYS DE L'ADOUR est fondée à demander l'annulation de l'arrêt attaqué ; <br/>
<br/>
              Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'il soit fait droit aux conclusions présentées par Mme A au titre des frais exposés par elle et non compris dans les dépens ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées au même titre par l'UNIVERSITE DE PAU ET DES PAYS DE L'ADOUR ; <br/>
<br/>
<br/>
<br/>
<br/>
              			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : L'arrêt du 15 juillet 2009 de la cour administrative d'appel de Bordeaux est annulé. <br/>
<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Bordeaux. <br/>
<br/>
Article 3 : Le surplus des conclusions du pourvoi est rejeté. <br/>
<br/>
Article 4 : Les conclusions présentées par Mme A au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
<br/>
Article 5 : La présente décision sera notifiée à l'UNIVERSITE DE PAU ET DES PAYS DE L'ADOUR et à Mme Françoise A.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">30-01-04 ENSEIGNEMENT ET RECHERCHE. QUESTIONS GÉNÉRALES. EXAMENS ET CONCOURS. - HABILITATION À DIRIGER DES RECHERCHES (ART. L. 612-7 DU CODE DE L'ÉDUCATION) - AUTORISATION D'INSCRIPTION À L'EXAMEN - PROPOSITION DU CONSEIL SCIENTIFIQUE - CRITÈRES À PRENDRE EN COMPTE POUR PROPOSER OU REFUSER L'INSCRIPTION.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">30-02-05-01 ENSEIGNEMENT ET RECHERCHE. QUESTIONS PROPRES AUX DIFFÉRENTES CATÉGORIES D'ENSEIGNEMENT. ENSEIGNEMENT SUPÉRIEUR ET GRANDES ÉCOLES. UNIVERSITÉS. - HABILITATION À DIRIGER DES RECHERCHES (ART. L. 612-7 DU CODE DE L'ÉDUCATION) - AUTORISATION D'INSCRIPTION À L'EXAMEN - PROPOSITION DU CONSEIL SCIENTIFIQUE - CRITÈRES À PRENDRE EN COMPTE POUR PROPOSER OU REFUSER L'INSCRIPTION.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">36-03-02-01 FONCTIONNAIRES ET AGENTS PUBLICS. ENTRÉE EN SERVICE. CONCOURS ET EXAMENS PROFESSIONNELS. ADMISSION À CONCOURIR. - HABILITATION À DIRIGER DES RECHERCHES (ART. L. 612-7 DU CODE DE L'ÉDUCATION) - AUTORISATION D'INSCRIPTION À L'EXAMEN - PROPOSITION DU CONSEIL SCIENTIFIQUE - CRITÈRES À PRENDRE EN COMPTE POUR PROPOSER OU REFUSER L'INSCRIPTION.
</SCT>
<ANA ID="9A"> 30-01-04 Les candidats à l'habilitation à diriger des recherches doivent être autorisés à se présenter à cet examen par une décision du président ou du directeur de l'établissement qui statue sur proposition du conseil scientifique siégeant en formation restreinte aux personnalités habilitées à diriger des recherches. Il revient au conseil scientifique, qui n'agit pas dans ce cadre en qualité de jury, de vérifier si le candidat peut être inscrit à l'examen au regard, d'une part, des conditions de diplôme posées par arrêté du ministre chargé de l'enseignement supérieur et, d'autre part, de son niveau scientifique, de la qualité de ses recherches et de sa capacité à encadrer de jeunes chercheurs. Il peut notamment refuser l'inscription au motif que le candidat n'a pas suffisamment encadré ou co-encadré de travaux de thèses.</ANA>
<ANA ID="9B"> 30-02-05-01 Les candidats à l'habilitation à diriger des recherches doivent être autorisés à se présenter à cet examen par une décision du président ou du directeur de l'établissement qui statue sur proposition du conseil scientifique siégeant en formation restreinte aux personnalités habilitées à diriger des recherches. Il revient au conseil scientifique, qui n'agit pas dans ce cadre en qualité de jury, de vérifier si le candidat peut être inscrit à l'examen au regard, d'une part, des conditions de diplôme posées par arrêté du ministre chargé de l'enseignement supérieur et, d'autre part, de son niveau scientifique, de la qualité de ses recherches et de sa capacité à encadrer de jeunes chercheurs. Il peut notamment refuser l'inscription au motif que le candidat n'a pas suffisamment encadré ou co-encadré de travaux de thèses.</ANA>
<ANA ID="9C"> 36-03-02-01 Les candidats à l'habilitation à diriger des recherches doivent être autorisés à se présenter à cet examen par une décision du président ou du directeur de l'établissement qui statue sur proposition du conseil scientifique siégeant en formation restreinte aux personnalités habilitées à diriger des recherches. Il revient au conseil scientifique, qui n'agit pas dans ce cadre en qualité de jury, de vérifier si le candidat peut être inscrit à l'examen au regard, d'une part, des conditions de diplôme posées par arrêté du ministre chargé de l'enseignement supérieur et, d'autre part, de son niveau scientifique, de la qualité de ses recherches et de sa capacité à encadrer de jeunes chercheurs. Il peut notamment refuser l'inscription au motif que le candidat n'a pas suffisamment encadré ou co-encadré de travaux de thèses.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
