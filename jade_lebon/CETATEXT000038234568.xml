<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038234568</ID>
<ANCIEN_ID>JG_L_2019_03_000000415366</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/23/45/CETATEXT000038234568.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 4ème chambres réunies, 15/03/2019, 415366</TITRE>
<DATE_DEC>2019-03-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>415366</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 4ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS ; SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>M. Arnaud Skzryerbak</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:415366.20190315</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme C...B...-D... a demandé au tribunal administratif de Cergy-Pontoise, d'une part, d'annuler la décision du 22 septembre 2014, confirmée le 17 octobre suivant, par laquelle le maire de Gennevilliers a refusé de lui verser rétroactivement l'allocation aux parents d'enfants handicapés pour la période allant du 1er mars 2012 au 1er juin 2014 et réduit le montant de cette allocation à compter du 1er octobre 2014 et, d'autre part, de condamner la commune de Gennevilliers à lui verser la somme de 5 000 euros en réparation du préjudice qu'elle estime avoir subi du fait de cette décision. Par un jugement n° 1412398 du 1er février 2016, le tribunal administratif de Cergy-Pontoise a rejeté cette demande.<br/>
<br/>
              Par une ordonnance n° 16VE00938 du 30 octobre 2017, enregistrée le 31 octobre 2017 au secrétariat du contentieux du Conseil d'Etat, le président de la cour administrative d'appel de Versailles a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, le pourvoi, enregistré le 30 mars 2016 au greffe de cette cour, présenté par Mme B...-D.... Par ce pourvoi et par un nouveau mémoire, enregistré le 5 mars 2018 au secrétariat du contentieux du Conseil d'Etat, Mme B...-D... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ; <br/>
<br/>
              3°) de mettre à la charge de la commune de Gennevilliers la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Arnaud Skzryerbak, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de Mme B...-D... et à la SCP Foussard, Froger, avocat de la commune de Gennevilliers.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article 9 de la loi du 13 juillet 1983 portant droits et obligations des fonctionnaires : " (...) L'action sociale, collective ou individuelle, vise à améliorer les conditions de vie des agents publics et de leurs familles, notamment dans les domaines de la restauration, du logement, de l'enfance et des loisirs, ainsi qu'à les aider à faire face à des situations difficiles. / (...) Les prestations d'action sociale, individuelles ou collectives, sont distinctes de la rémunération visée à l'article 20 de la présente loi et sont attribuées indépendamment du grade, de l'emploi ou de la manière de servir ". <br/>
<br/>
              2. Il ressort des pièces du dossier soumis aux juges du fond qu'en application de ces dispositions, le conseil municipal de Gennevilliers a, par une délibération du 29 septembre 2004, institué au bénéfice des agents de la commune une allocation aux parents d'enfants handicapés. M. A...B..., agent communal, percevait cette allocation lorsque la résidence de ses enfants a été fixée chez leur mère, Mme C...B...-D..., lors du divorce prononcé le 1er octobre 2012. Mme B...-D... a demandé à la commune de Gennevilliers, par courrier du 22 août 2013, que l'allocation aux parents d'enfants handicapés lui soit désormais versée, plutôt qu'à son ancien conjoint qui avait continué de la percevoir. Le maire de la commune a fait droit à cette demande à compter du 1er juillet 2014 mais, par une décision du 22 septembre 2014, il a révisé à la baisse le montant de l'allocation et a refusé d'en faire rétroagir l'octroi à la date de séparation des époux. Par un jugement du 1er février 2016, le tribunal administratif de Cergy-Pontoise a rejeté la demande de Mme B...-D... tendant à l'annulation de cette décision et à la condamnation de la commune de Gennevilliers à l'indemniser du préjudice qui en serait résulté. Par une ordonnance du 30 octobre 2017, le président de la cour administrative d'appel de Versailles a transmis au Conseil d'Etat le pourvoi de Mme B...-D... contre ce jugement.<br/>
<br/>
              Sur la compétence du Conseil d'Etat : <br/>
<br/>
              3. Aux termes de l'article R. 811-1 du code de justice administrative : " (...) le tribunal administratif statue en premier et dernier ressort : / 1° Sur les litiges relatifs aux prestations, allocations ou droits attribués au titre de l'aide ou de l'action sociale (...) ". Il résulte du 8° du même article que ces dispositions s'appliquent aux actions indemnitaires, quel que soit le montant des indemnités demandées.<br/>
<br/>
              4. Les prestations d'action sociale facultative instituées au bénéfice des agents publics en application de l'article 9 de la loi du 13 juillet 1983 constituent des prestations attribuées au titre de l'action sociale au sens de l'article R. 811-1 du code de justice administrative. Par suite, les litiges relatifs à ces prestations sont au nombre de ceux sur lesquels le tribunal administratif statue en premier et dernier ressort. Dès lors, la requête de Mme B... -D... dirigée contre le jugement du tribunal administratif de Cergy-Pontoise du 1er février 2016 doit être regardée comme un pourvoi en cassation, relevant de la compétence du Conseil d'Etat.<br/>
<br/>
              Sur la régularité du jugement attaqué : <br/>
<br/>
              5. Lorsque, par suite des indications erronées portées sur la notification d'un jugement rendu en premier et dernier ressort, un requérant a saisi la cour administrative d'appel et que le président de celle-ci a transmis son recours au Conseil d'Etat en application de l'article R. 351-2 du code de justice administrative, le délai de deux mois à l'issue duquel le requérant n'est plus recevable à invoquer une cause juridique distincte court à compter soit de la date à laquelle un avocat au Conseil d'Etat et à la Cour de cassation se constitue pour le requérant, soit, s'il y a été invité avant cette constitution, de la réception, par le requérant, de l'invitation à faire régulariser son pourvoi par un avocat au Conseil d'Etat et à la Cour de cassation en application des articles R. 612-1 et R. 821-3 du même code. <br/>
<br/>
              6. Le jugement du 1er février 2016, rendu en premier et dernier ressort par le tribunal administratif de Cergy-Pontoise, a été notifié à Mme B...-D... le 8 février 2016 avec l'indication erronée d'une voie de recours devant la cour administrative d'appel de Versailles. A l'appui de son pourvoi enregistré le 30 mars 2016 au greffe de la cour, Mme B... -D... n'a invoqué que des moyens contestant le bien-fondé du jugement. Après que la cour administrative d'appel a transmis le dossier au Conseil d'Etat, un avocat au Conseil d'Etat et à la Cour de cassation a déclaré le 27 novembre 2017 se constituer pour Mme B... -D.... Le mémoire dans lequel Mme B...-D... a soulevé le moyen tiré de ce que le tribunal a méconnu le principe du caractère contradictoire de la procédure en ne sollicitant pas les observations des parties sur le moyen, relevé d'office, tiré de la méconnaissance des règles fixées par la délibération du conseil municipal de la commune de Gennevilliers du 29 septembre 2004 a été enregistré le 5 mars 2018, soit plus de deux mois après cette constitution. Par suite, la commune de Gennevilliers est fondée à soutenir que ce moyen contestant la régularité du jugement n'est pas recevable.<br/>
<br/>
              Sur le bien-fondé du jugement attaqué :<br/>
<br/>
              7. En premier lieu, l'allocation aux parents d'enfants handicapés prévue par la délibération du conseil municipal de Gennevilliers du 29 septembre 2004 étant une prestation instituée en application de l'article 9 de la loi du 13 juillet 1983, la circonstance que le bénéfice en soit réservé aux agents communaux, sans pouvoir être versée, du chef de l'agent communal, à son ancien conjoint lorsque ce dernier a la charge de leurs enfants, ne caractérise pas une différence de traitement constitutive d'une méconnaissance du principe d'égalité. Par suite, Mme B... -D... n'est pas fondée à soutenir que le tribunal aurait commis pour ce motif une erreur de droit en jugeant que la délibération du conseil municipal de Gennevilliers du 29 septembre 2004 ne permettait pas le versement de l'allocation aux parents d'enfants handicapés à une personne qui n'a pas la qualité d'agent de la commune.<br/>
<br/>
              8. En second lieu, en relevant d'office que Mme B...-D..., qui n'avait pas la qualité d'agent communal, n'entrait pas dans le champ d'application de la délibération du conseil municipal de Gennevilliers du 29 septembre 2004, le tribunal n'a pas commis d'erreur de droit. <br/>
<br/>
              9. Il résulte de ce qui précède que Mme B...-D... n'est pas fondée à demander l'annulation du jugement qu'elle attaque. <br/>
<br/>
              10. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la commune de Gennevilliers, qui n'est pas la partie perdante dans la présente instance. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de Mme B...-D... la somme que la commune de Gennevilliers demande au titre des mêmes dispositions.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de Mme B...-D... est rejeté.<br/>
Article 2 : Les conclusions de la commune de Gennevilliers présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à Mme C...B...-D... et à la commune de Gennevilliers.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">17-05-012 COMPÉTENCE. COMPÉTENCE À L'INTÉRIEUR DE LA JURIDICTION ADMINISTRATIVE. COMPÉTENCE EN PREMIER ET DERNIER RESSORT DES TRIBUNAUX ADMINISTRATIFS. - INCLUSION - LITIGES RELATIFS AUX PRESTATIONS D'ACTION SOCIALE FACULTATIVE INSTITUÉES AU BÉNÉFICE DES AGENTS PUBLICS.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">17-05-015 COMPÉTENCE. COMPÉTENCE À L'INTÉRIEUR DE LA JURIDICTION ADMINISTRATIVE. COMPÉTENCE D'APPEL DES COURS ADMINISTRATIVES D'APPEL. - EXCLUSION - LITIGES RELATIFS AUX PRESTATIONS D'ACTION SOCIALE FACULTATIVE INSTITUÉES AU BÉNÉFICE DES AGENTS PUBLICS.
</SCT>
<ANA ID="9A"> 17-05-012 Les prestations d'action sociale facultative instituées au bénéfice des agents publics en application de l'article 9 de la loi n° 83-634 du 13 juillet 1983 constituent des prestations attribuées au titre de l'action sociale au sens de l'article R. 811-1 du code de justice administrative. Par suite, les litiges relatifs à ces prestations sont au nombre de ceux sur lesquels le tribunal administratif statue en premier et dernier ressort.</ANA>
<ANA ID="9B"> 17-05-015 Les prestations d'action sociale facultative instituées au bénéfice des agents publics en application de l'article 9 de la loi n° 83-634 du 13 juillet 1983 constituent des prestations attribuées au titre de l'action sociale au sens de l'article R. 811-1 du code de justice administrative. Par suite, les litiges relatifs à ces prestations sont au nombre de ceux sur lesquels le tribunal administratif statue en premier et dernier ressort.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
