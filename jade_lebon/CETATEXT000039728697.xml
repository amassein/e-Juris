<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039728697</ID>
<ANCIEN_ID>JG_L_2019_12_000000420231</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/72/86/CETATEXT000039728697.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 4ème chambres réunies, 31/12/2019, 420231</TITRE>
<DATE_DEC>2019-12-31</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>420231</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 4ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROUSSEAU, TAPIE ; LE PRADO</AVOCATS>
<RAPPORTEUR>M. Arnaud Skzryerbak</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie Sirinelli</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:420231.20191231</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A... B... a demandé au tribunal administratif de Paris d'ordonner une expertise médicale en vue d'obtenir tous les éléments utiles sur les préjudices qu'il estime avoir subis du fait de la prise du Mediator et de condamner solidairement l'Etat et l'Agence nationale de sécurité du médicament et des produits de santé à lui verser une provision à valoir sur l'indemnisation de ses préjudices. <br/>
<br/>
              Par un jugement avant-dire droit n° 1315853 du 7 août 2014, le tribunal administratif de Paris a rejeté les conclusions de la demande dirigées contre l'Agence nationale de sécurité du médicament et des produits de santé, a déclaré l'Etat responsable des conséquences dommageables éventuelles pour M. B... de la prise du Mediator à partir du 7 juillet 1999, a ordonné une expertise et a rejeté la demande de provision. Par un jugement n° 1315853 du 23 octobre 2015, le tribunal administratif de Paris a rejeté les conclusions indemnitaires de M. B.... <br/>
<br/>
              Par un arrêt n°s 14PA04080, 15PA04750 du 28 février 2018, la cour administrative d'appel de Paris a rejeté l'appel formé par M. B... contre le jugement du tribunal administratif de Paris du 23 octobre 2015 et prononcé un non-lieu à statuer sur l'appel formé par le ministre des affaires sociales, de la santé et des droits des femmes contre le jugement avant-dire droit du 7 août 2014.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 30 avril et 26 juillet 2018 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code civil ;<br/>
              - le code de la santé publique ;<br/>
              - la loi n° 2011-900 du 29 juillet 2011 ;<br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Arnaud Skzryerbak, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Marie Sirinelli, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rousseau, Tapie, avocat de M. B... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. B... a demandé au tribunal administratif de Paris de condamner l'Etat et l'Agence nationale de sécurité du médicament et des produits de santé à l'indemniser des préjudices qu'il estime avoir subis du fait de son exposition au benfluorex, principe actif du Mediator, par lequel il a été traité de janvier 2006 à novembre 2009. Par un jugement avant-dire droit du 7 août 2014, le tribunal a déclaré l'Etat responsable des conséquences dommageables éventuelles pour M. B... de la prise du Mediator et ordonné à sa demande une expertise en vue d'établir l'existence d'un lien de causalité entre la pathologie dont il souffre et son exposition à ce médicament et d'apprécier l'étendue des préjudices subis. A l'invitation du vice-président du tribunal, l'expert qu'il avait désigné le 24 septembre 2014 a déposé un rapport de carence au vu duquel le tribunal a rejeté la demande de M. B... par un jugement du 23 octobre 2015. Par un arrêt du 28 février 2018, la cour administrative d'appel de Paris a rejeté l'appel formé par M. B... contre le jugement du 23 octobre 2015 et prononcé un non-lieu à statuer sur l'appel formé par le ministre chargé de la santé contre le jugement du 7 août 2014. Par son pourvoi, M. B... doit être regardé comme demandant l'annulation de cet arrêt en tant que, par son article 1er, il rejette son appel.<br/>
<br/>
              Sur le non-lieu :<br/>
<br/>
              2. Aux termes de l'article L. 1142-24-1 du code de la santé publique, issu de l'article 57 de la loi du 29 juillet 2011 de finances rectificative pour 2011 : " Sans préjudice des actions qui peuvent être exercées conformément au droit commun, la réparation intégrale des préjudices imputables au benfluorex est assurée dans les conditions prévues par la présente section ". Aux termes de l'article L. 1142-24-2 du même code : " Toute personne s'estimant victime d'un déficit fonctionnel imputable au benfluorex ou, le cas échéant, son représentant légal ou ses ayants droit peut saisir l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales en vue d'obtenir la réparation des préjudices en résultant (...) ". La demande est examinée par un collège d'experts placé auprès de l'office qui procède à toute investigation utile à son instruction et diligente, le cas échéant, une expertise. L'article L. 1142-24-6 du code de la santé publique prévoit que les personnes considérées comme responsables par le collège d'experts ou leurs assureurs adressent à la victime ou à ses ayants droit une offre d'indemnisation visant à la réparation intégrale des préjudices subis. Il précise que " sont applicables à cette offre les deuxième à huitième alinéas de l'article L. 1142-14 ". En vertu du sixième alinéa de cet article, l'acceptation de l'offre de l'assureur vaut transaction au sens de l'article 2044 du code civil. Enfin, aux termes de l'article L 1142-24-8 du code de la santé publique : " Les indemnisations accordées en application de la présente section ne peuvent se cumuler avec celles accordées, le cas échéant, en application des articles L. 1142-14, L. 1142-15, L. 1142-17, L. 1142-20 et L. 1142-21, ni avec les indemnités de toute nature reçues ou à recevoir d'autres débiteurs du chef des mêmes préjudices ".<br/>
<br/>
              3. Il ne résulte pas de ces dispositions que l'acceptation d'une offre d'indemnisation présentée sur le fondement de l'article L. 1142-24-6 du code de la santé publique rende sans objet une action intentée devant le juge administratif pour rechercher la responsabilité de l'Etat à raison des mêmes préjudices.<br/>
<br/>
              4. Par suite, la circonstance que le requérant a accepté l'offre d'indemnisation que lui a adressée la société les Laboratoires Servier sur le fondement de l'article L. 1142-24-6 du code de la santé publique, qui est inférieure au montant des indemnités réclamées devant la cour administrative d'appel de Paris, ne prive pas d'objet le présent pourvoi.<br/>
<br/>
              Sur le bien-fondé de l'arrêt attaqué :<br/>
<br/>
              En ce qui concerne la régularité de la procédure de première instance :<br/>
<br/>
              5. Aux termes de l'article R. 621-1 du code de justice administrative : " La juridiction peut, soit d'office, soit sur la demande des parties ou de l'une d'elles, ordonner, avant dire droit, qu'il soit procédé à une expertise sur les points déterminés par sa décision (...) ". Il appartient au demandeur qui engage une action en responsabilité à l'encontre de l'administration d'apporter tous éléments de nature à établir devant le juge l'existence d'une faute, la réalité du préjudice subi et l'existence d'un lien direct de causalité entre la faute et ce préjudice. Il incombe alors, en principe, au juge de statuer au vu des pièces du dossier, le cas échéant après avoir demandé aux parties les éléments complémentaires qu'il juge nécessaires à son appréciation. Il ne lui revient d'ordonner une expertise que lorsqu'il n'est pas en mesure de se prononcer au vu des pièces et éléments qu'il a recueillis et que l'expertise présente ainsi un caractère utile.<br/>
<br/>
              6. En premier lieu, aux termes de l'article R. 621-7-1 du même code : " Les parties doivent remettre sans délai à l'expert tous documents que celui-ci estime nécessaires à l'accomplissement de sa mission. / En cas de carence des parties, l'expert en informe le président de la juridiction qui, après avoir provoqué les observations écrites de la partie récalcitrante, peut ordonner la production des documents, s'il y a lieu sous astreinte, autoriser l'expert à passer outre, ou à déposer son rapport en l'état. / Le président peut en outre examiner les problèmes posés par cette carence lors de la séance prévue à l'article R. 621-8-1. / La juridiction tire les conséquences du défaut de communication des documents à l'expert ". Aux termes de l'article R. 621-9 du même code : " Le rapport est déposé au greffe en deux exemplaires. Des copies sont notifiées par l'expert aux parties intéressées. (...) / Les parties sont invitées par le greffe de la juridiction à fournir leurs observations dans le délai d'un mois ; une prorogation de délai peut être accordée ". Enfin, en vertu de l'article R. 621-1-1 de ce code, le président de la juridiction peut désigner au sein de sa juridiction un magistrat chargé des questions d'expertise et du suivi des opérations d'expertise, en lui déléguant notamment les attributions mentionnées à l'article R. 621-7-1.<br/>
<br/>
              7. D'une part, si le rapport de l'expert, alors même qu'il se bornerait à constater la carence de l'une des parties, doit être notifié aux parties pour qu'elles puissent fournir leur observations, dans les conditions prévues par l'article R. 621-9 du code de justice administrative, avant que le juge ne statue, il ne résulte en revanche ni des dispositions de l'article R. 621-7-1 du code de justice administrative ni d'aucun principe que le président de la juridiction ou le magistrat qu'il a désigné devrait, en dehors du cas où il ordonne la production de documents, provoquer les observations écrites de la partie récalcitrante avant que l'expert, faute d'être en mesure d'accomplir sa mission du fait de la carence de cette partie, dépose son rapport en l'état. Par suite, M. B... n'est pas fondé à soutenir que la cour aurait commis une erreur de droit en jugeant que la circonstance que ses observations n'avaient pas été sollicitées préalablement au dépôt par l'expert d'un rapport de carence n'avait pas entaché d'irrégularité la procédure de première instance.  <br/>
<br/>
              8. D'autre part, en estimant que l'expert désigné par le tribunal n'avait pu accomplir sa mission en raison de la carence de M. B... et que l'expertise n'avait pas eu lieu de son fait, la cour s'est livrée à une appréciation souveraine des faits de l'espèce qui est exempte de dénaturation.<br/>
<br/>
              9. En second lieu, il résulte de ce qui précède que le tribunal pouvait tirer les conséquences de la carence de M. B... à participer aux opérations d'expertise qu'il avait ordonnées à sa demande. Par suite, la cour n'a pas commis d'erreur de droit ni dénaturé les faits dont elle était saisie en jugeant que le tribunal avait pu régulièrement s'abstenir de surseoir à statuer dans l'attente du rapport du collège d'experts placé auprès de l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales, mentionné à l'article L. 1142-24-4 du code de la santé publique, alors même qu'il ne disposait pas de tous les éléments nécessaires à la solution du litige qui lui était soumis. C'est également sans erreur de droit qu'elle a jugé que le tribunal n'avait pas entaché son jugement d'irrégularité ni méconnu le droit au procès équitable, garanti notamment par les stipulations de l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, en ne donnant pas à M. B... la possibilité de produire ce rapport au soutien de ses prétentions.  <br/>
<br/>
              En ce qui concerne la responsabilité de l'Etat :<br/>
<br/>
              10. En premier lieu, si la cour a relevé le caractère minime des troubles invoqués par M. B..., elle a ainsi entendu caractériser leur absence de lien de causalité avec la prise de Mediator. Par suite, M. B... n'est pas fondé à soutenir que la cour, qui n'a pas dénaturé les faits dont elle était saisie, aurait méconnu le principe de réparation intégrale du préjudice en subordonnant son indemnisation à un seuil minimal de gravité.<br/>
<br/>
              11. En deuxième lieu, c'est au terme d'une appréciation souveraine, exempte de dénaturation, des pièces du dossier, notamment des comptes rendus de consultations médicales produits par M. B..., que la cour a estimé, par un arrêt qui est suffisamment motivé sur ce point, que le lien de causalité entre les pathologies dont souffre le requérant et son exposition au Mediator n'était pas établi.<br/>
<br/>
              12. En dernier lieu, en jugeant qu'en dépit de la valvulopathie, minime et non évolutive, qu'il présentait, M. B..., qui se bornait pour le surplus à se prévaloir de données générales relatives aux risques liés à l'exposition au Mediator, ne justifiait pas personnellement de l'existence d'un préjudice certain lié à la crainte de développer une pathologie grave après la prise de Mediator, la cour n'a pas dénaturé les pièces du dossier qui lui était soumis.<br/>
<br/>
              13. Il résulte de tout ce qui précède que M. B... n'est pas fondé à demander l'annulation de l'article 1er de l'arrêt qu'il attaque.<br/>
<br/>
              Sur les frais liés au litige :<br/>
<br/>
              14. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le pourvoi de M. B... est rejeté.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. A... B... et à la ministre des solidarités et de la santé.<br/>
Copie en sera adressée à l'Agence nationale de sécurité du médicament et des produits de santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-04-02-02-01 PROCÉDURE. INSTRUCTION. MOYENS D'INVESTIGATION. EXPERTISE. RECOURS À L'EXPERTISE. - DÉPÔT PAR L'EXPERT D'UN RAPPORT DE CARENCE - NOTIFICATION AUX PARTIES AVANT QUE LE JUGE STATUE - EXISTENCE (ART. R. 621-9 DU CJA) - JUGE TENU DE PROVOQUER LES OBSERVATIONS ÉCRITES DES PARTIES AVANT LE DÉPÔT DU RAPPORT - ABSENCE, EN DEHORS DU CAS DE DÉFAUT DE PRODUCTION DE DOCUMENTS PRÉVU À L'ARTICLE R. 621-7-1 DU CJA.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">60-02-01 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RESPONSABILITÉ EN RAISON DES DIFFÉRENTES ACTIVITÉS DES SERVICES PUBLICS. SERVICE PUBLIC DE SANTÉ. - PROCÉDURE NON CONTENTIEUSE D'INDEMNISATION DES VICTIMES DU BENFLUOREX (ART. L. 1142-24-1 ET S. DU CSP) - ACCEPTATION DE L'OFFRE D'INDEMNISATION FORMULÉE PAR L'EXPLOITANT DU MÉDICAMENT - ACCEPTATION PRIVANT D'OBJET UNE ACTION EN RESPONSABILITÉ CONTRE L'ETAT À RAISON DES MÊMES PRÉJUDICES - ABSENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">60-04-04 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RÉPARATION. MODALITÉS DE LA RÉPARATION. - PROCÉDURE NON CONTENTIEUSE D'INDEMNISATION DES VICTIMES DU BENFLUOREX (ART. L. 1142-24-1 ET S. DU CSP) - ACCEPTATION DE L'OFFRE D'INDEMNISATION FORMULÉE PAR L'EXPLOITANT DU MÉDICAMENT - ACCEPTATION PRIVANT D'OBJET UNE ACTION EN RESPONSABILITÉ CONTRE L'ETAT À RAISON DES MÊMES PRÉJUDICES - ABSENCE.
</SCT>
<ANA ID="9A"> 54-04-02-02-01 Si le rapport de l'expert, alors même qu'il se bornerait à constater la carence de l'une des parties, doit être notifié aux parties pour qu'elles puissent fournir leur observations, dans les conditions prévues par l'article R. 621-9 du code de justice administrative (CJA), avant que le juge ne statue, il ne résulte en revanche ni des dispositions de l'article R. 621-7-1 du CJA ni d'aucun principe que le président de la juridiction ou le magistrat qu'il a désigné devrait, en dehors du cas où il ordonne la production de documents, provoquer les observations écrites de la partie récalcitrante avant que l'expert, faute d'être en mesure d'accomplir sa mission du fait de la carence de cette partie, dépose son rapport en l'état.</ANA>
<ANA ID="9B"> 60-02-01 Il ne résulte pas des dispositions du code de la santé publique instituant la procédure non contentieuse d'indemnisation des victimes du benfluorex (art. L. 1142-24-1 et s. de ce code) que l'acceptation d'une offre d'indemnisation présentée sur le fondement de l'article L. 1142-24-6 du même code rende sans objet une action intentée devant le juge administratif pour rechercher la responsabilité de l'Etat à raison des mêmes préjudices.</ANA>
<ANA ID="9C"> 60-04-04 Il ne résulte pas des dispositions du code de la santé publique instituant la procédure non contentieuse d'indemnisation des victimes du benfluorex (art. L. 1142-24-1 et s. de ce code) que l'acceptation d'une offre d'indemnisation présentée sur le fondement de l'article L. 1142-24-6 du même code rende sans objet une action intentée devant le juge administratif pour rechercher la responsabilité de l'Etat à raison des mêmes préjudices.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
