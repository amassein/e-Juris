<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028822751</ID>
<ANCIEN_ID>JG_L_2014_04_000000360902</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/82/27/CETATEXT000028822751.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème / 1ère SSR, 03/04/2014, 360902</TITRE>
<DATE_DEC>2014-04-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>360902</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème / 1ère SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO ; SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>M. Didier Ribes</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier de Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:360902.20140403</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 10 juillet et 9 octobre 2012 au secrétariat du contentieux du Conseil d'État, présentés pour la commune de Bonifacio, représentée par son maire ; la commune de Bonifacio demande au Conseil d'État :<br/>
<br/>
              1°) d'annuler l'arrêt n° 10MA03021 du 16 mai 2012 par lequel la cour administrative d'appel de Marseille a rejeté sa requête tendant, d'une part, à l'annulation du jugement n° 0900531 du 12 mai 2010 par lequel le tribunal administratif de Bastia a annulé, à la demande de M. A...B..., l'arrêté de son maire refusant de lui délivrer un permis de construire, d'autre part, au rejet de la demande de M. B...;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de M. B...le versement de la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des collectivités territoriales ;<br/>
<br/>
              Vu le code de l'urbanisme ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Didier Ribes, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de la commune de Bonifacio et à la SCP Waquet, Farge, Hazan, avocat de M. B...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 30 mars 2009, le maire de Bonifacio a refusé de délivrer à M. A...B...un permis de construire dix maisons individuelles, d'une superficie hors oeuvre nette d'environ 116 m2 chacune, sur un terrain dont il est propriétaire, situé en secteur ULB 3 du plan local d'urbanisme, au lieu-dit Chioso della Marina ; que, par un jugement du 12 mai 2010, le tribunal administratif de Bastia a annulé, à la demande de M.B..., cet arrêté, au motif que l'intéressé était titulaire d'un permis de construire tacite ; que, par un arrêt du 16 mai 2012, la cour administrative d'appel de Marseille a rejeté l'appel de la commune de Bonifacio contre ce jugement, en jugeant que le maire avait estimé, à tort, que le projet en cause ne constituait pas un hameau nouveau intégré à l'environnement ; que la commune de Bonifacio se pourvoit en cassation contre cet arrêt ; que M.B..., par la voie du pourvoi incident, conteste ce même arrêt en ce qu'il ne juge pas qu'il était titulaire d'un permis tacite ;<br/>
<br/>
<br/>
              Sur le pourvoi de la commune de Bonifacio :<br/>
<br/>
              2. Considérant qu'aux termes des dispositions du I de l'article L. 146-4 du code de l'urbanisme : " L'extension de l'urbanisation doit se réaliser soit en continuité avec les agglomérations et villages existants, soit en hameaux nouveaux intégrés à l'environnement. (...) " ; qu'un permis de construire ne peut être délivré sur le fondement de ces dispositions pour la réalisation d'une construction qui n'est pas en continuité avec les agglomérations et villages existants qu'à la condition que le projet soit conforme à la destination d'une zone délimitée par le document local d'urbanisme, dans laquelle celui-ci prévoit la possibilité d'une extension de l'urbanisation de faible ampleur intégrée à l'environnement par la réalisation d'un petit nombre de constructions de faible importance, proches les unes des autres et formant un ensemble dont les caractéristiques et l'organisation s'inscrivent dans les traditions locales ; <br/>
<br/>
              3. Considérant qu'en se bornant à relever que les  constructions projetées étaient organisées autour d'un espace commun constitué par une rue centrale et une petite place, pour en déduire que le maire n'avait pu légalement refuser d'autoriser leur construction au motif qu'elles ne constituaient pas un " hameau nouveau " au sens des dispositions de l'article L. 146-4 du code de l'urbanisme, sans rechercher si le projet en cause se situait dans une zone destinée, par le plan local d'urbanisme de la commune de Bonifacio, à accueillir  un " hameau nouveau ", c'est-à-dire un ensemble défini comme il a été dit au point 2 ci-dessus, la cour a entaché son arrêt d'une erreur de droit ; que, par suite, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, la commune de Bonifacio est fondée à demander l'annulation de l'arrêt attaqué ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Sur le pourvoi incident de M. B...:<br/>
<br/>
              4. Considérant que si M. B...demande l'annulation de l'arrêt attaqué en tant qu'il n'a pas retenu le moyen tiré de l'existence d'un permis de construire tacite pour fonder l'annulation de l'arrêté contesté, ces conclusions, qui ne sont pas dirigées contre le dispositif de l'arrêt mais contre ses motifs, ne sont, en tout état de cause, pas recevables ;<br/>
<br/>
<br/>
              Sur les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              5. Considérant que ces dispositions font obstacle à ce qu'une somme soit mise à la charge de la commune de Bonifacio qui n'est pas, dans la présente instance, la partie perdante ; qu'il y a lieu, en revanche, dans les circonstances de l'espèce, de mettre à la charge de M. B...la somme de 3 000 euros à verser à la commune de Bonifacio, au titre des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Marseille du 16 mai 2012 est annulé.<br/>
<br/>
      Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Marseille.<br/>
<br/>
Article 3 : M. B...versera à la commune de Bonifacio la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
      Article 4 : Le pourvoi incident de M. B...est rejeté.<br/>
<br/>
Article 5 : Les conclusions  présentées par M. B...au titre des dispositions de l'article        L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 6 : La présente décision sera notifiée à la commune de Bonifacio et à M. A...B....   Copie en sera transmise pour information à la ministre de l'égalité des territoires et du logement.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">68-001-01-02-03 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. RÈGLES GÉNÉRALES D'UTILISATION DU SOL. RÈGLES GÉNÉRALES DE L'URBANISME. PRESCRIPTIONS D'AMÉNAGEMENT ET D'URBANISME. RÉGIME ISSU DE LA LOI DU 3 JANVIER 1986 SUR LE LITTORAL. - RÈGLES APPLICABLES À L'EXTENSION DE L'URBANISATION (I DE L'ART. L. 146-4 DU CODE DE L'URBANISME) - NOTION DE HAMEAU NOUVEAU INTÉGRÉ À L'ENVIRONNEMENT - DÉFINITION.
</SCT>
<ANA ID="9A"> 68-001-01-02-03 Un permis ne peut être délivré sur le fondement des dispositions du premier alinéa du I de l'article L. 146-4 du code de l'urbanisme pour la réalisation d'une construction qui n'est pas en continuité avec les agglomérations et villages existants qu'à la condition que le projet soit conforme à la destination d'une zone délimitée par le document local d'urbanisme, dans laquelle celui-ci prévoit la possibilité d'une extension de l'urbanisation de faible ampleur intégrée à l'environnement par la réalisation d'un petit nombre de constructions de faible importance, proches les unes des autres et formant un ensemble dont les caractéristiques et l'organisation s'inscrivent dans les traditions locales.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
