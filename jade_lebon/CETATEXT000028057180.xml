<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028057180</ID>
<ANCIEN_ID>JG_L_2013_10_000000359219</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/05/71/CETATEXT000028057180.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème / 7ème SSR, 10/10/2013, 359219, Publié au recueil Lebon</TITRE>
<DATE_DEC>2013-10-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>359219</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème / 7ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SPINOSI</AVOCATS>
<RAPPORTEUR>M. David Gaudillère</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Damien Botteghi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:359219.20131010</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 7 mai et 12 juillet 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour la Fédération française de gymnastique, représentée par son président, dont le siège est 7 ter, cour des Petites Ecuries à Paris (75010) ; la fédération demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite par laquelle le ministre chargé des sports a refusé d'abroger le point 2.2.2.2.1. de l'annexe I-5 du code du sport, pris en application de l'article R. 131-3 de ce code ; <br/>
<br/>
              2°) d'enjoindre, au besoin sous astreinte, au ministre chargé des sports d'abroger ces dispositions dans un délai d'un mois à compter de la décision à intervenir ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros en application des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution, modifiée notamment par les lois constitutionnelles n° 99-569 du 8 juillet 1999 et n° 2008-724 du 23 juillet 2008 ;<br/>
<br/>
              Vu le code du sport ;<br/>
<br/>
              Vu la loi n° 84-610 du 16 juillet 1984 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. David Gaudillère, Maître des requêtes,<br/>
<br/>
              - les conclusions de M. Damien Botteghi, Rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Spinosi, avocat de la Fédération française de gymnastique ;<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant que l'autorité compétente, saisie d'une demande tendant à l'abrogation d'un règlement illégal, est tenue d'y déférer, soit que ce règlement ait été illégal dès la date de sa signature, soit que l'illégalité résulte de circonstances de droit ou de fait postérieures à cette date ; que, toutefois, cette autorité ne saurait être tenue d'accueillir une telle demande dans le cas où l'illégalité du règlement a cessé, en raison d'un changement de circonstances, à la date à laquelle elle se prononce ;<br/>
<br/>
              2.	Considérant qu'aux termes du III de l'article 16 de la loi du 16 juillet 1984 relative à l'organisation et à la promotion des activités physiques et sportives, codifié à l'article L. 131-8 du code du sport par l'effet de l'ordonnance du 23 mai 2006 relative à la partie législative de ce code : " Un agrément peut être délivré par le ministre chargé des sports aux fédérations qui, en vue de participer à l'exécution d'une mission de service public, ont adopté des statuts comportant certaines dispositions obligatoires et un règlement disciplinaire conforme à un règlement type. / Les dispositions obligatoires des statuts et le règlement disciplinaire type sont définis par décret en Conseil d'Etat pris après avis du Comité national olympique et sportif français " ; que le décret du 7 janvier 2004, pris pour l'application de ces dispositions a défini, en application de cet article, les dispositions obligatoires des statuts des fédérations sportives agréées ; qu'au nombre des dispositions obligatoires issues de ce décret figurent celles du point 2.2.2.2.1. des statuts types des fédérations sportives agréées, désormais codifiées au point 2.2.2.2.1. de l'annexe I-5 du code du sport, en vertu desquelles ces statuts doivent préciser " que la représentation des femmes est garantie au sein de la ou des instances dirigeantes en leur attribuant un nombre de sièges en proportion du nombre de licenciées éligibles " ;<br/>
<br/>
              3.	Considérant que la Fédération française de gymnastique a demandé au ministre des sports l'abrogation des dispositions du point 2.2.2.2.1. de l'annexe I-5 du code du sport, issues du décret du 7 janvier 2004 ; qu'elle a saisi le Conseil d'Etat d'un recours tendant à l'annulation pour excès de pouvoir de la décision implicite par laquelle il a été refusé de faire droit à sa demande d'abrogation ;<br/>
<br/>
              4.	Considérant que l'article 1er de la Déclaration de 1789 proclame : " Les hommes naissent et demeurent.libres et égaux en droits Les distinctions sociales ne peuvent être fondées que sur l'utilité commune " ; que l'alinéa 3 du Préambule de la Constitution du 27 octobre 1946 précise que " La loi garantit à la femme, dans tous les domaines, des droits égaux à ceux de l'homme " ; qu'en vertu de l'article 1er de la Constitution : " La France (libres et égaux en droits) assure l'égalité devant la loi de tous les citoyens sans distinction d'origine, de race ou de religion " ; que si, aux termes du cinquième alinéa de l'article 3 de la Constitution, dans sa rédaction issue de la loi constitutionnelle du 8 juillet 1999 : " La loi favorise l'égal accès des femmes et des hommes aux mandats électoraux et fonctions électives ", ces dispositions ne s'appliquaient qu'à des mandats et des fonctions politiques ; que toutefois, l'article 1er de la loi constitutionnelle du 23 juillet 2008 a abrogé cet alinéa et ajouté à l'article 1er de la Constitution un second alinéa aux termes duquel : " La loi favorise l'égal accès des femmes et des hommes aux mandats électoraux et fonctions électives, ainsi qu'aux responsabilités professionnelles et sociales " ; qu'il résulte de ces dernières dispositions, éclairées par les travaux parlementaires qui ont précédé leur adoption, que leur objet est de combiner le principe constitutionnel d'égalité, tel qu'interprété par le Conseil constitutionnel, notamment dans sa décision n° 2006-533 DC du 16 mars 2006, et l'objectif d'égal accès des femmes et des hommes aux mandats électoraux et fonctions électives, ainsi qu'aux responsabilités professionnelles et sociales ;<br/>
<br/>
              5.	Considérant que si le principe constitutionnel d'égalité ne fait pas obstacle à la recherche d'un accès équilibré des femmes et des hommes aux responsabilités, il interdit, réserve faite de dispositions constitutionnelles particulières, de faire prévaloir la considération du sexe sur celle des capacités et de l'utilité commune ; qu'ainsi, avant l'adoption de la loi constitutionnelle du 23 juillet 2008, le principe constitutionnel d'égalité excluait que la composition des organes dirigeants des personnes morales de droit privé, comme les fédérations sportives, soit régie par des règles contraignantes fondées sur le sexe des personnes appelées à y siéger ; que si, ainsi qu'il a été dit, le second alinéa désormais ajouté à l'article 1er de la Constitution a pour objet de combiner ce principe et l'objectif d'égal accès des femmes et des hommes aux responsabilités professionnelles et sociales, il résulte également de ces dispositions que le législateur est seul compétent, tant dans les matières définies notamment par l'article 34 de la Constitution que dans celles relevant du pouvoir réglementaire en application de l'article 37, pour adopter les règles destinées à favoriser l'égal accès des femmes et des hommes aux mandats, fonctions et responsabilités mentionnés à l'article 1er de la Constitution ; qu'il appartient seulement au Premier ministre, en vertu de l'article 21 de la Constitution et sous réserve de la compétence conférée au Président de la République par son article 13, de prendre les dispositions d'application de ces mesures législatives ;<br/>
<br/>
              6.	Considérant, d'une part, que les dispositions contestées du point 2.2.2.2.1. des statuts types des fédérations sportives agréées, issues du décret du 7 janvier 2004, ne se bornent pas à fixer un objectif de représentation équilibrée entre les femmes et les hommes au sein des instances dirigeantes des fédérations agréées, mais imposent le respect d'une proportion déterminée entre les hommes et les femmes au sein de ces instances, précisément fixée en proportion du nombre de licenciés de chaque sexe ; que ces dispositions étaient ainsi contraires au principe constitutionnel d'égalité devant la loi, à la date à laquelle elles ont été édictées ;<br/>
<br/>
              7.	Considérant, d'autre part, qu'en l'absence de toute disposition législative applicable aux fédérations sportives agréées, fixant les règles destinées à favoriser l'égal accès des femmes et des hommes aux instances dirigeantes de ces fédérations, les dispositions du second alinéa de l'article 1er de la Constitution dans sa rédaction issue de la loi constitutionnelle du 23 juillet 2008 ne peuvent, par elles-mêmes, avoir eu pour effet de rendre légales les dispositions du point 2.2.2.2.1. des statuts types des fédérations sportives agréées, figurant à l'annexe I-5 du code du sport ; <br/>
<br/>
              8.	Considérant qu'il résulte de ce qui précède que la Fédération française de gymnastique est fondée à soutenir que les dispositions du point 2.2.2.2.1. de l'annexe I-5 du code du sport sont entachées d'illégalité et, sans qu'il soit besoin d'examiner les autres moyens de la requête, à demander l'annulation pour excès de pouvoir de la décision implicite refusant de les abroger ; que cette annulation implique nécessairement l'abrogation des dispositions réglementaires dont l'illégalité a été constatée ; qu'il y a lieu pour le Conseil d'Etat d'ordonner cette mesure dans un délai de trois mois à compter de la notification de la présente décision, sans qu'il soit nécessaire d'assortir cette injonction d'une astreinte ; <br/>
<br/>
              9.	Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à la Fédération française de gymnastique au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
                --------------<br/>
<br/>
Article 1er : La décision implicite refusant l'abrogation du point 2.2.2.2.1. de l'annexe I-5 du code du sport est annulée.<br/>
<br/>
Article 2 : Il est enjoint au Premier ministre d'abroger le point 2.2.2.2.1. de l'annexe I 5 du code du sport dans le délai de trois mois à compter de la notification de la présente décision.<br/>
<br/>
Article 3 : L'Etat versera une somme de 3 000 euros à la Fédération française de gymnastique au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la Fédération française de gymnastique, à la ministre des sports, de la jeunesse, de l'éducation populaire et de la vie associative et au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-015-03-01-02 ACTES LÉGISLATIFS ET ADMINISTRATIFS. - PRISE EN COMPTE DU CRITÈRE DU SEXE DANS L'ACCÈS AUX RESPONSABILITÉS PROFESSIONNELLES - 1) AVANT L'ENTRÉE EN VIGUEUR DE LA LOI CONSTITUTIONNELLE DU 23 JUILLET 2008 - PRINCIPE CONSTITUTIONNEL D'ÉGALITÉ - PORTÉE - INTERDICTION, SAUF DISPOSITIONS CONSTITUTIONNELLES PARTICULIÈRES, DE FAIRE PRÉVALOIR LA CONSIDÉRATION DU SEXE SUR CELLE DES CAPACITÉS ET DE L'UTILITÉ COMMUNE [RJ3] - 2) APRÈS L'ENTRÉE EN VIGUEUR DE LA LOI CONSTITUTIONNELLE - A) COMBINAISON DU PRINCIPE D'ÉGALITÉ ET DE L'OBJECTIF D'ÉGAL ACCÈS DES FEMMES ET DES HOMMES AUX MANDATS ÉLECTORAUX ET FONCTIONS ÉLECTIVES AINSI QU'AUX RESPONSABILITÉS PROFESSIONNELLES ET SOCIALES - B) COMPÉTENCE EXCLUSIVE DU LÉGISLATEUR POUR ADOPTER LES RÈGLES DESTINÉES À FAVORISER CET ÉGAL ACCÈS - EXISTENCE - COMPÉTENCE DU POUVOIR RÉGLEMENTAIRE POUR PRENDRE LES DISPOSITIONS D'APPLICATION - EXISTENCE [RJ2] - 3) CONSÉQUENCES EN L'ESPÈCE - STATUTS TYPES DES FÉDÉRATIONS SPORTIVES IMPOSANT LE RESPECT D'UNE CERTAINE PROPORTION ENTRE HOMMES ET FEMMES AU SEIN DE LEURS INSTANCES DIRIGEANTES, ISSUS DU DÉCRET DU 7 JANVIER 2004 ET DONT L'ABROGATION A ÉTÉ REFUSÉE EN 2012 - A) ILLÉGALITÉ DÈS L'ORIGINE EN RAISON DE LA MÉCONNAISSANCE DU PRINCIPE D'ÉGALITÉ - B) ILLÉGALITÉ À LA DATE DU REFUS D'ABROGATION EN L'ABSENCE DE DISPOSITIONS LÉGISLATIVES FIXANT DES RÈGLES FAVORISANT L'ÉGAL ACCÈS AU SEIN DE CES INSTANCES.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">01-02-01 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - COMPÉTENCE. LOI ET RÈGLEMENT. - PRISE EN COMPTE DU CRITÈRE DU SEXE DANS L'ACCÈS AUX RESPONSABILITÉS PROFESSIONNELLES - 1) AVANT L'ENTRÉE EN VIGUEUR DE LA LOI CONSTITUTIONNELLE DU 23 JUILLET 2008 - PRINCIPE CONSTITUTIONNEL D'ÉGALITÉ - PORTÉE - INTERDICTION, SAUF DISPOSITIONS CONSTITUTIONNELLES PARTICULIÈRES, DE FAIRE PRÉVALOIR LA CONSIDÉRATION DU SEXE SUR CELLE DES CAPACITÉS ET DE L'UTILITÉ COMMUNE [RJ3] - 2) APRÈS L'ENTRÉE EN VIGUEUR DE LA LOI CONSTITUTIONNELLE - A) COMBINAISON DU PRINCIPE D'ÉGALITÉ ET DE L'OBJECTIF D'ÉGAL ACCÈS DES FEMMES ET DES HOMMES AUX MANDATS ÉLECTORAUX ET FONCTIONS ÉLECTIVES AINSI QU'AUX RESPONSABILITÉS PROFESSIONNELLES ET SOCIALES - B) COMPÉTENCE EXCLUSIVE DU LÉGISLATEUR POUR ADOPTER LES RÈGLES DESTINÉES À FAVORISER CET ÉGAL ACCÈS - EXISTENCE - COMPÉTENCE DU POUVOIR RÉGLEMENTAIRE POUR PRENDRE LES DISPOSITIONS D'APPLICATION - EXISTENCE [RJ2] - 3) CONSÉQUENCES EN L'ESPÈCE - STATUTS TYPES DES FÉDÉRATIONS SPORTIVES IMPOSANT LE RESPECT D'UNE CERTAINE PROPORTION ENTRE HOMMES ET FEMMES AU SEIN DE LEURS INSTANCES DIRIGEANTES, ISSUS DU DÉCRET DU 7 JANVIER 2004 ET DONT L'ABROGATION A ÉTÉ REFUSÉE EN 2012 - A) ILLÉGALITÉ DÈS L'ORIGINE EN RAISON DE LA MÉCONNAISSANCE DU PRINCIPE D'ÉGALITÉ - B) ILLÉGALITÉ À LA DATE DU REFUS D'ABROGATION EN L'ABSENCE DE DISPOSITIONS LÉGISLATIVES FIXANT DES RÈGLES FAVORISANT L'ÉGAL ACCÈS AU SEIN DE CES INSTANCES.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">01-04-03-07-06 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. PRINCIPES GÉNÉRAUX DU DROIT. PRINCIPES INTÉRESSANT L'ACTION ADMINISTRATIVE. OBLIGATION D'ABROGER UN RÈGLEMENT ILLÉGAL. - OBLIGATION D'ABROGER UN RÈGLEMENT ILLÉGAL [RJ1] - CAS PARTICULIER - CAS OÙ L'ILLÉGALITÉ DU RÈGLEMENT A CESSÉ À LA DATE À LAQUELLE L'ADMINISTRATION SE PRONONCE - ABSENCE D'OBLIGATION.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">01-09-02-01 ACTES LÉGISLATIFS ET ADMINISTRATIFS. DISPARITION DE L'ACTE. ABROGATION. ABROGATION DES ACTES RÉGLEMENTAIRES. - OBLIGATION D'ABROGER UN RÈGLEMENT ILLÉGAL [RJ1] - CAS PARTICULIER - CAS OÙ L'ILLÉGALITÉ DU RÈGLEMENT A CESSÉ À LA DATE À LAQUELLE L'ADMINISTRATION SE PRONONCE - ABSENCE D'OBLIGATION.
</SCT>
<ANA ID="9A"> 01-015-03-01-02 1) Si le principe constitutionnel d'égalité ne fait pas obstacle à la recherche d'un accès équilibré des femmes et des hommes aux responsabilités, il interdit, réserve faite de dispositions constitutionnelles particulières, de faire prévaloir la considération du sexe sur celle des capacités et de l'utilité commune. Ainsi, avant l'adoption de la loi constitutionnelle n° 2008-724 du 23 juillet 2008, le principe constitutionnel d'égalité excluait que la composition des organes dirigeants des personnes morales de droit privé, comme les fédérations sportives, soit régie par des règles contraignantes fondées sur le sexe des personnes appelées à y siéger,,,2) a) Le second alinéa ajouté à l'article 1er de la Constitution par la loi constitutionnelle du 23 juillet 2008 a pour objet de combiner ce principe, tel qu'interprété par le Conseil constitutionnel, notamment dans sa décision n° 2006-533 DC du 16 mars 2006, et l'objectif d'égal accès des femmes et des hommes aux responsabilités professionnelles et sociales.... ,,b) Il résulte également de ces dispositions que le législateur est seul compétent, tant dans les matières définies notamment par l'article 34 de la Constitution que dans celles relevant du pouvoir réglementaire en application de l'article 37, pour adopter les règles destinées à favoriser l'égal accès des femmes et des hommes aux mandats, fonctions et responsabilités mentionnés à l'article 1er de la Constitution. Il appartient seulement au Premier ministre, en vertu de l'article 21 de la Constitution et sous réserve de la compétence conférée au Président de la République par son article 13, de prendre les dispositions d'application de ces mesures législatives.,,,3)  a) Les dispositions du point 2.2.2.2.1 des statuts types des fédérations sportives agréées, issues du décret n° 2004-22 du 7 janvier 2004, ne se bornent pas à fixer un objectif de représentation équilibrée entre les femmes et les hommes au sein des instances dirigeantes des fédérations agréées, mais imposent le respect d'une proportion déterminée entre les hommes et les femmes au sein de ces instances, précisément fixée en proportion du nombre de licenciés de chaque sexe. Ces dispositions étaient ainsi contraires au principe constitutionnel d'égalité devant la loi à la date à laquelle elles ont été édictées.,,,b) En l'absence de toute disposition législative applicable aux fédérations sportives agréées, fixant les règles destinées à favoriser l'égal accès des femmes et des hommes aux instances dirigeantes de ces fédérations, les dispositions du second alinéa de l'article 1er de la Constitution dans sa rédaction issue de la loi constitutionnelle du 23 juillet 2008 ne peuvent, par elles-mêmes, avoir eu pour effet de rendre légales les dispositions de ce même point 2.2.2.2.1.</ANA>
<ANA ID="9B"> 01-02-01 1) Si le principe constitutionnel d'égalité ne fait pas obstacle à la recherche d'un accès équilibré des femmes et des hommes aux responsabilités, il interdit, réserve faite de dispositions constitutionnelles particulières, de faire prévaloir la considération du sexe sur celle des capacités et de l'utilité commune. Ainsi, avant l'adoption de la loi constitutionnelle n° 2008-724 du 23 juillet 2008, le principe constitutionnel d'égalité excluait que la composition des organes dirigeants des personnes morales de droit privé, comme les fédérations sportives, soit régie par des règles contraignantes fondées sur le sexe des personnes appelées à y siéger,,,2) a) Le second alinéa ajouté à l'article 1er de la Constitution par la loi constitutionnelle du 23 juillet 2008 a pour objet de combiner ce principe, tel qu'interprété par le Conseil constitutionnel, notamment dans sa décision n° 2006-533 DC du 16 mars 2006, et l'objectif d'égal accès des femmes et des hommes aux responsabilités professionnelles et sociales.... ,,b) Il résulte également de ces dispositions que le législateur est seul compétent, tant dans les matières définies notamment par l'article 34 de la Constitution que dans celles relevant du pouvoir réglementaire en application de l'article 37, pour adopter les règles destinées à favoriser l'égal accès des femmes et des hommes aux mandats, fonctions et responsabilités mentionnés à l'article 1er de la Constitution. Il appartient seulement au Premier ministre, en vertu de l'article 21 de la Constitution et sous réserve de la compétence conférée au Président de la République par son article 13, de prendre les dispositions d'application de ces mesures législatives.,,,3)  a) Les dispositions du point 2.2.2.2.1 des statuts types des fédérations sportives agréées, issues du décret n° 2004-22 du 7 janvier 2004, ne se bornent pas à fixer un objectif de représentation équilibrée entre les femmes et les hommes au sein des instances dirigeantes des fédérations agréées, mais imposent le respect d'une proportion déterminée entre les hommes et les femmes au sein de ces instances, précisément fixée en proportion du nombre de licenciés de chaque sexe. Ces dispositions étaient ainsi contraires au principe constitutionnel d'égalité devant la loi à la date à laquelle elles ont été édictées.,,,b) En l'absence de toute disposition législative applicable aux fédérations sportives agréées, fixant les règles destinées à favoriser l'égal accès des femmes et des hommes aux instances dirigeantes de ces fédérations, les dispositions du second alinéa de l'article 1er de la Constitution dans sa rédaction issue de la loi constitutionnelle du 23 juillet 2008 ne peuvent, par elles-mêmes, avoir eu pour effet de rendre légales les dispositions de ce même point 2.2.2.2.1.</ANA>
<ANA ID="9C"> 01-04-03-07-06 L'autorité compétente, saisie d'une demande tendant à l'abrogation d'un règlement illégal, est tenue d'y déférer, soit que ce règlement ait été illégal dès la date de sa signature, soit que l'illégalité résulte de circonstances de droit ou de fait postérieures à cette date. Toutefois, cette autorité ne saurait être tenue d'accueillir une telle demande dans le cas où l'illégalité du règlement a cessé, en raison d'un changement de circonstances, à la date à laquelle elle se prononce.</ANA>
<ANA ID="9D"> 01-09-02-01 L'autorité compétente, saisie d'une demande tendant à l'abrogation d'un règlement illégal, est tenue d'y déférer, soit que ce règlement ait été illégal dès la date de sa signature, soit que l'illégalité résulte de circonstances de droit ou de fait postérieures à cette date. Toutefois, cette autorité ne saurait être tenue d'accueillir une telle demande dans le cas où l'illégalité du règlement a cessé, en raison d'un changement de circonstances, à la date à laquelle elle se prononce.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Assemblée, 3 février 1989, Compagnie Alitalia, n° 74052, p. 44.,,[RJ2] Cf. CE, Assemblée, 7 mai 2013, Fédération CFTC de l'agriculture, n° 362280, p. 119.,,[RJ3] Rappr. Cons. const., 16 mars 2006, n° 2006-533 DC.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
