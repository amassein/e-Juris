<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039351214</ID>
<ANCIEN_ID>JG_L_2019_11_000000409330</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/35/12/CETATEXT000039351214.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 07/11/2019, 409330</TITRE>
<DATE_DEC>2019-11-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>409330</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>BALAT ; SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>Mme Cécile Isidoro</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Cytermann</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:409330.20191107</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. A... B... a demandé au tribunal administratif de Châlons-en-Champagne de condamner le service départemental d'incendie et de secours des Ardennes à lui verser la somme de 118 448 euros en réparation des préjudices qu'il a subis du fait d'un accident de service intervenu le 30 décembre 2010. <br/>
<br/>
              Par un jugement n° 1402456 du 26 avril 2016, le tribunal administratif de Châlons-en-Champagne a condamné le service départemental d'incendie et de secours des Ardennes à verser à M. B... une somme de 18 000 euros.<br/>
<br/>
              Par un arrêt n° 16NC01254, 16NC01751 du 26 janvier 2017, la cour administrative d'appel de Nancy a, sur appel du service départemental d'incendie et de secours des Ardennes, annulé ce jugement et rejeté la demande présentée par M. B... devant le tribunal administratif. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés le 28 mars et le 28 juin 2017 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel du service départemental d'incendie et de secours ;<br/>
<br/>
              3°) de mettre à la charge du service départemental d'incendie et de secours la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi n° 91-1389 du 31 décembre 1991 ;  <br/>
              - la loi n° 96-370 du 3 mai 1996 ; <br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Cécile Isidoro, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Laurent Cytermann, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Balat, avocat de M. B... et à la SCP Piwnica, Molinié, avocat du service départemental d'incendie et de secours des Ardennes ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. B..., sapeur-pompier volontaire au service départemental d'incendie et de secours des Ardennes depuis le 1er décembre 1999, s'est blessé au genou à l'occasion d'une chute sur la chaussée enneigée le 30 octobre 1999, au retour d'une intervention. Cet accident, ainsi qu'une rechute du 10 mars 2011, qui ont été reconnus imputables au service, lui ont ouvert droit au régime d'indemnisation des sapeurs-pompiers volontaires institué par la loi du 31 décembre 1991 relative à la protection sociale des sapeurs-pompiers volontaires en cas d'accident survenu ou de maladie contractée en service. Depuis le 2 avril 2012, M. B... bénéficie ainsi d'une allocation d'invalidité versée par la Caisse des dépôts et consignations au taux de 20 %, porté à 28 % le 2 avril 2015 en raison d'une aggravation des séquelles. M. B... a saisi le tribunal administratif de Châlons-en-Champagne d'une demande tendant à être indemnisé de l'intégralité des préjudices qu'il estime subir du fait de son invalidité, chiffrée à 118 448 euros. Estimant par ailleurs avoir été évincé du service en raison de son handicap, M. B... a également recherché la responsabilité du service départemental d'incendie et de secours des Ardennes en raison de cette éviction. Par un jugement du 26 avril 2016, le tribunal a écarté l'existence d'une faute du service départemental d'incendie et de secours des Ardennes. Il a, en revanche, condamné ce service à verser à M. B..., sur le terrain de la responsabilité sans faute, une indemnité complémentaire de 18 000 euros. Le service départemental d'incendie et de secours des Ardennes a relevé appel de ce jugement devant la cour administrative d'appel de Nancy qui, par un arrêt du 26 janvier 2017, a annulé le jugement du tribunal administratif de Châlons-en-Champagne, rejeté la demande présentée par M. B... devant ce tribunal ainsi que ses conclusions d'appel. M. B... se pourvoit en cassation contre cet arrêt. <br/>
<br/>
              2. Aux termes de l'article 1-5 de la loi du 3 mai 1996 relative au développement du volontariat dans les corps de sapeurs-pompiers : " Une protection sociale particulière est garantie au sapeur-pompier volontaire par la loi n° 91-1389 du 31 décembre 1991 relative à la protection sociale des sapeurs-pompiers volontaires en cas d'accident survenu ou de maladie contractée en service ". Aux termes de l'article 1er de la loi du 31 décembre 1991 relative à la protection sociale des sapeurs-pompiers volontaires en cas d'accident survenu ou de maladie contractée en service : " Le sapeur-pompier volontaire victime d'un accident survenu ou atteint d'une maladie contractée en service ou à l'occasion du service a droit, dans les conditions prévues par la présente loi : 1° Sa vie durant, à la gratuité des frais médicaux, chirurgicaux, pharmaceutiques et accessoires ainsi que des frais de transport, d'hospitalisation et d'appareillage et, d'une façon générale, des frais de traitement, de réadaptation fonctionnelle et de rééducation professionnelle directement entraînés par cet accident ou cette maladie ; 2° A une indemnité journalière compensant la perte de revenus qu'il subit pendant la période d'incapacité temporaire de travail ; 3° A une allocation ou une rente en cas d'invalidité permanente. En outre, il ouvre droit pour ses ayants cause aux prestations prévues par la présente loi (...) ". L'article 20 de la même loi dispose que : " Aucun avantage supplémentaire ne peut être accordé par les collectivités locales et leurs établissements publics pour l'indemnisation des risques couverts par la présente loi. La présente loi s'applique à tous les sapeurs-pompiers volontaires, quel que soit le service dont ils dépendent ". <br/>
<br/>
              3. Ces dispositions déterminent forfaitairement la réparation à laquelle les sapeurs-pompiers volontaires victimes d'un accident de service ou d'une maladie professionnelle peuvent prétendre, au titre des préjudices liés aux pertes de revenus et à l'incidence professionnelle résultant de l'incapacité physique causée par cet accident ou cette maladie. Les dispositions du c de l'article 20 de la loi du 31 décembre 1991, éclairées par les travaux préparatoires de la loi du 31 juillet 1962 de finances rectificative pour 1962, desquelles elles sont issues, se bornent à exclure l'attribution d'avantages supplémentaires par les collectivités locales et leurs établissements publics au titre de cette réparation forfaitaire. Elles ne font, en revanche, pas obstacle à ce que le sapeur-pompier volontaire qui subit, du fait de l'invalidité ou de la maladie, des préjudices patrimoniaux d'une autre nature ou des préjudices personnels obtienne de la personne publique auprès de laquelle il est engagé, même en l'absence de faute de celle-ci, une indemnité complémentaire réparant ces chefs de préjudice, ni à ce qu'une action de droit commun pouvant aboutir à la réparation intégrale de l'ensemble du dommage soit engagée contre la personne publique, dans le cas notamment où l'accident ou la maladie serait imputable à une faute de nature à engager la responsabilité de cette personne ou à l'état d'un ouvrage public dont l'entretien lui incombait. <br/>
<br/>
              4. Par suite, en jugeant que les dispositions de l'article 20 de la loi du 31 décembre 1991 faisaient obstacle à ce que le sapeur-pompier volontaire obtienne, en l'absence de faute de nature à engager la responsabilité de droit commun du service départemental d'incendie et de secours qui l'emploie, une indemnité complémentaire pour les risques d'accident de service et de maladie professionnelle couverts par la loi, y compris lorsqu'il a subi des préjudices patrimoniaux d'une autre nature que ceux qui sont visés par la loi ou des préjudices personnels, la cour administrative d'appel de Nancy a entaché son arrêt d'erreur de droit. Il y a, dès lors, lieu de l'annuler, sans qu'il soit besoin d'examiner les autres moyens du pourvoi. <br/>
<br/>
              5. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de M. B... qui n'est pas, dans la présente instance, la partie perdante. Il y a lieu en revanche, dans les circonstances de l'espèce, de mettre à la charge du service départemental d'incendie et de secours des Ardennes la somme de 3 000 euros à verser à M. B... au titre des mêmes dispositions.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nancy du 26 janvier 2017 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nancy. <br/>
Article 3 : Le service départemental d'incendie et de secours des Ardennes versera la somme de 3 000 euros à M. B... au titre des dispositions de l'article L. 761-1 du code de justice administrative <br/>
Article 4 : Les conclusions du service départemental d'incendie et de secours des Ardennes présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 5 : La présente décision sera notifiée à M.  A... B... et au service départemental d'incendie et de secours des Ardennes.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">60-02-06-01 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RESPONSABILITÉ EN RAISON DES DIFFÉRENTES ACTIVITÉS DES SERVICES PUBLICS. SERVICES PUBLICS COMMUNAUX. SERVICE PUBLIC DE LUTTE CONTRE L'INCENDIE. - SAPEURS-POMPIERS VOLONTAIRES (SPV) - INDEMNISATION FORFAITAIRE DES SPV VICTIMES D'UN ACCIDENT DE SERVICE OU D'UNE MALADIE PROFESSIONNELLE [RJ1] - 1) PORTÉE - RÉPARATION DES PRÉJUDICES LIÉS AUX PERTES DE REVENUS ET À L'INCIDENCE PROFESSIONNELLE DE L'INCAPACITÉ PHYSIQUE - 2) CONSÉQUENCES - A) POSSIBILITÉ POUR LES COLLECTIVITÉS LOCALES D'ATTRIBUER DES AVANTAGES SUPPLÉMENTAIRES À CE TITRE - ABSENCE - A) POSSIBILITÉ, POUR LE SPV VICTIME, D'OBTENIR UNE INDEMNISATION COMPLÉMENTAIRE POUR D'AUTRES CHEFS DE PRÉJUDICE - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">60-04-04-05 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RÉPARATION. MODALITÉS DE LA RÉPARATION. CARACTÈRE FORFAITAIRE DE LA PENSION. - SAPEURS-POMPIERS VOLONTAIRES (SPV) - INDEMNISATION FORFAITAIRE DES SPV VICTIMES D'UN ACCIDENT DE SERVICE OU D'UNE MALADIE PROFESSIONNELLE [RJ1] - 1) PORTÉE - RÉPARATION DES PRÉJUDICES LIÉS AUX PERTES DE REVENUS ET À L'INCIDENCE PROFESSIONNELLE DE L'INCAPACITÉ PHYSIQUE - 2) CONSÉQUENCES - I) POSSIBILITÉ POUR LES COLLECTIVITÉS LOCALES D'ATTRIBUER DES AVANTAGES SUPPLÉMENTAIRES À CE TITRE - ABSENCE - II) POSSIBILITÉ, POUR LE SPV VICTIME, D'OBTENIR UNE INDEMNISATION COMPLÉMENTAIRE POUR D'AUTRES CHEFS DE PRÉJUDICE - EXISTENCE.
</SCT>
<ANA ID="9A"> 60-02-06-01 1) L'article 1-5 de la loi n° 96-370 du 3 mai 1996 et les articles 1er et 20 de la loi n° 91-1389 du 31 décembre 1991 déterminent forfaitairement la réparation à laquelle les sapeurs-pompiers volontaires victimes d'un accident de service ou d'une maladie professionnelle peuvent prétendre, au titre des préjudices liés aux pertes de revenus et à l'incidence professionnelle résultant de l'incapacité physique causée par cet accident ou cette maladie.... ,,2) a) Le c de l'article 20 de la loi du 31 décembre 1991, éclairé par les travaux préparatoires de la loi n° 62-873 du 31 juillet 1962 dont il est issu, se borne à exclure l'attribution d'avantages supplémentaires par les collectivités locales et leurs établissements publics au titre de cette réparation forfaitaire.... ,,b) Il ne fait, en revanche, pas obstacle à ce que le sapeur-pompier volontaire qui subit, du fait de l'invalidité ou de la maladie, des préjudices patrimoniaux d'une autre nature ou des préjudices personnels obtienne de la personne publique auprès de laquelle il est engagé, même en l'absence de faute de celle-ci, une indemnité complémentaire réparant ces chefs de préjudice, ni à ce qu'une action de droit commun pouvant aboutir à la réparation intégrale de l'ensemble du dommage soit engagée contre la personne publique, dans le cas notamment où l'accident ou la maladie serait imputable à une faute de nature à engager la responsabilité de cette personne ou à l'état d'un ouvrage public dont l'entretien lui incombait.</ANA>
<ANA ID="9B"> 60-04-04-05 1) L'article 1-5 de la loi n° 96-370 du 3 mai 1996 et les articles 1er et 20 de la loi n° 91-1389 du 31 décembre 1991 déterminent forfaitairement la réparation à laquelle les sapeurs-pompiers volontaires victimes d'un accident de service ou d'une maladie professionnelle peuvent prétendre, au titre des préjudices liés aux pertes de revenus et à l'incidence professionnelle résultant de l'incapacité physique causée par cet accident ou cette maladie.... ,,2) a) Le c de l'article 20 de la loi du 31 décembre 1991, éclairé par les travaux préparatoires de la loi n° 62-873 du 31 juillet 1962 dont il est issu, se borne à exclure l'attribution d'avantages supplémentaires par les collectivités locales et leurs établissements publics au titre de cette réparation forfaitaire.... ,,b) Il ne fait, en revanche, pas obstacle à ce que le sapeur-pompier volontaire qui subit, du fait de l'invalidité ou de la maladie, des préjudices patrimoniaux d'une autre nature ou des préjudices personnels obtienne de la personne publique auprès de laquelle il est engagé, même en l'absence de faute de celle-ci, une indemnité complémentaire réparant ces chefs de préjudice, ni à ce qu'une action de droit commun pouvant aboutir à la réparation intégrale de l'ensemble du dommage soit engagée contre la personne publique, dans le cas notamment où l'accident ou la maladie serait imputable à une faute de nature à engager la responsabilité de cette personne ou à l'état d'un ouvrage public dont l'entretien lui incombait.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., s'agissant de la pension forfaitaire d'invalidité des agents publics, CE, Assemblée, 4 juillet 2003,,, n° 211106, p. 323 (dont la solution a été abandonnée, en ce qu'elle inclut la totalité des préjudices patrimoniaux dans l'objet de ces prestations et non uniquement les pertes de revenus et l'incidence professionnelle de l'invalidité, par CE, 16 décembre 2013, Centre hospitalier de Royan, n° 353798, T. pp. 729-730-840).</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
