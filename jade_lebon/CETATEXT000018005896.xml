<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000018005896</ID>
<ANCIEN_ID>JG_L_2007_03_000000304053</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/18/00/58/CETATEXT000018005896.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 30/03/2007, 304053</TITRE>
<DATE_DEC>2007-03-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>304053</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>BLONDEL ; SCP TIFFREAU</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2007:304053.20070330</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 26 mars 2007 au secrétariat du contentieux du Conseil d'Etat, présentée pour la VILLE DE LYON, représentée par son maire en exercice, domicilié... ; la VILLE DE LYON demande au juge des référés du Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance du 15 mars 2007 par laquelle le juge des référés du tribunal administratif de Lyon, statuant sur le fondement de l'article L. 521-2 a suspendu l'exécution de la décision par laquelle le maire de Lyon a refusé de louer une salle municipale à l'association locale pour le culte des Témoins de Jéhovah de Lyon Lafayette et a enjoint au maire de louer cette salle ou une salle équivalente à cette association pour la soirée du 2 avril 2007 ; <br/>
<br/>
              2°) statuant au titre de la procédure de référé, de rejeter la demande présentée par l'association locale pour le culte des Témoins de Jéhovah de Lyon Lafayette comme irrecevable et, subsidiairement, comme mal fondée ;<br/>
<br/>
              3°) de mettre à la charge de l'association locale pour le culte des Témoins de Jéhovah de Lyon Lafayette le versement d'une somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              elle soutient que le juge des référés ne pouvait, sans erreur de droit, enjoindre à la ville de mettre la salle à la disposition de l'association, une telle mesure n'ayant aucun caractère provisoire ; qu'il a dénaturé les conclusions dont il était saisi et statué ultra petita dès lors que l'association ne demandait ni la suspension de la décision ni éventuellement la location d'une autre salle ; qu'il a statué sur des conclusions irrecevables dès lors que la demande portait sur l'exécution d'une mesure n'ayant pas un caractère provisoire ; subsidiairement, que l'ordonnance est entachée d'insuffisance de motivation dès lors qu'elle ne précise pas en quoi les conditions de l'article L. 521-2 sont réunies ; que la condition d'urgence n'est pas remplie dès lors que l'association, à qui une décision de refus implicite avait été opposée dès le 8 janvier 2007, pouvait accéder à une autre salle ; qu'il n'y a pas d'atteinte grave à une liberté fondamentale pour la même raison ; que le refus de la ville ne peut être regardé comme portant une atteinte illégale à une liberté fondamentale dès lors que la collectivité, qui pratique des tarifs de location de salles inférieurs à ceux du marché, avait compétence liée pour refuser de subventionner une association qui a un caractère cultuel, en application de la loi de 1905 sur la séparation des Eglises et de l'Etat ; qu'en tout état de cause l'illégalité ne saurait être regardée comme manifeste ;<br/>
<br/>
<br/>
<br/>
              Vu l'ordonnance attaquée ;<br/>
              Vu, enregistrés le 28 mars 2007, les mémoires présentés pour l'association locale pour le culte des Témoins de Jéhovah de Lyon Lafayette ; l'association conclut au rejet de la requête et à ce que soit mise à la charge de la VILLE DE LYON une somme de 4 800 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
              elle soutient que le juge n'a nullement méconnu les termes de la demande qui lui était présentée ; que l'article L. 521-2 du code de justice administrative permet au juge de prononcer une injonction indépendamment d'une mesure de suspension ; qu'il appartient au juge des référés d'exercer ses pouvoirs pour assurer l'exercice effectif des libertés fondamentales ; que la ville a entendu s'opposer à la demande parce qu'elle a estimé que l'association n'était pas au nombre de celle à qui une salle pouvait être louée ; que le caractère d'association cultuelle de la défenderesse ne pouvait faire obstacle à ce qu'une salle municipale lui soit louée ; que la condition d'urgence est satisfaite dès lors que l'association, qui a présenté sa demande dès le 8 novembre 2006, sans obtenir de réponse, entend célébrer une fête religieuse le 2 avril ; qu'aucun motif d'ordre public ne justifie le refus ; que la ville ne justifie pas davantage que le tarif de location des salles municipales est inférieur au tarif des autres salles ; que la location d'une salle ne saurait être regardée comme une subvention ; que la décision de refus méconnaît les principes constitutionnels de la liberté d'association et de la liberté de réunion et celui d'égalité devant le service public ; que l'association a cherché d'autres salles et c'est faute d'en trouver qu'elle a maintenu la demande qu'elle a adressée à la VILLE DE LYON ;<br/>
              Vu, enregistré le 29 mars 2007, le mémoire en réplique présenté pour la VILLE DE LYON qui conclut aux mêmes fins que sa requête et par les mêmes moyens, et en outre par les moyens que l'association locale pour le culte des Témoins de Jéhovah Lyon Lafayette a mené sans succès deux demandes de location de salle et qu'elle disposait du temps nécessaire aux dates auxquelles elle était en possession de décisions implicites de refus pour rechercher une autre salle ; qu'elle n'établit pas avoir réalisé une telle démarche ; que la condition d'urgence n'est donc pas remplie ; que les tarifs de location des salles municipales sont très inférieurs à ceux prévus pour des spectacles ou des manifestations publiques payantes ; que les salles municipales ne peuvent devenir le lieu de manifestations à caractère religieux sans méconnaître le principe de laïcité ;	<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
              Vu la Constitution ;<br/>
              Vu le code général des collectivités territoriales, notamment son article L. 2144-3 ;<br/>
              Vu la loi du 9 décembre 1905, notamment son article 2 ;<br/>
              Vu le code de justice administrative ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, la VILLE DE LYON, d'autre part, l'association locale pour le culte des Témoins de Jéhovah Lyon Lafayette ;<br/>
              Vu le procès-verbal de l'audience publique du jeudi 29 mars 2007 à 15 heures au cours de laquelle ont été entendus :<br/>
              - Me Tiffreau, avocat au Conseil d'Etat et à la Cour de cassation, avocat de la VILLE DE LYON ;<br/>
<br/>
              - Me BLONDEL, avocat au Conseil d'Etat et à la Cour de cassation, avocat de l'association locale pour le culte des Témoins de Jéhovah Lyon Lafayette ;<br/>
<br/>
<br/>
<br/>
<br/>
              Sur la régularité de l'ordonnance :<br/>
              Considérant qu'aux termes de l'article L. 511-1 du code de justice administrative : " Le juge des référés statue par des mesures qui présentent un caractère provisoire (...). " ; qu'aux termes de l'article L. 521-2 du même code : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public (...) aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures " ; qu'il résulte de ces dispositions que les décisions prises par le juge des référés n'ont, en principe, qu'un caractère provisoire ; qu'il lui appartient ainsi, lorsqu'il est saisi sur le fondement de l'article L. 521-2 du code de justice administrative et qu'il constate une atteinte grave et manifestement illégale portée par une personne morale de droit public à une liberté fondamentale, de prendre les mesures provisoires qui sont de nature à faire disparaître les effets de cette atteinte ; que, toutefois, lorsqu'aucune mesure de caractère provisoire n'est susceptible de satisfaire cette exigence, en particulier lorsque les délais dans lesquels il est saisi ou lorsque la nature de l'atteinte y font obstacle, il peut enjoindre à la personne qui en est l'auteur de prendre toute disposition de nature à sauvegarder l'exercice effectif de la liberté fondamentale en cause ; qu'il en va ainsi notamment lorsque l'atteinte résulte d'une interdiction dont les effets sont eux-mêmes provisoires ou limités dans le temps ;<br/>
<br/>
              Considérant que la VILLE DE LYON relève appel de l'ordonnance, en date du 15 mars 2007, par laquelle le juge des référés du tribunal administratif de Lyon a, d'une part, suspendu l'exécution de la décision par laquelle le maire de Lyon a opposé un refus à la demande de location de la salle municipale Victor Hugo présentée par l'association locale pour le culte des Témoins de Jéhovah Lyon Lafayette, d'autre part enjoint à la ville de mettre à la disposition de l'association, au jour qu'elle avait demandé, le 2 avril 2007, dans le but de permettre à ses adhérents de célébrer une fête religieuse, une salle municipale ; que si le juge a ainsi enjoint à la ville de prendre une disposition qui n'avait pas de caractère provisoire, la nature de l'interdiction opposée à l'association et ses effets permettaient au juge des référés, pour sauvegarder la liberté de réunion dont il a décidé qu'il y était gravement porté atteinte de manière manifestement illégale, d'enjoindre au maire d'autoriser l'association à louer une salle municipale aux jour et heure qu'elle avait sollicités ; que, par suite, contrairement à ce que soutient la VILLE DE LYON, l'ordonnance dont elle relève appel ne s'est pas prononcée sur des conclusions irrecevables et n'a pas méconnu la portée des dispositions de l'article L. 521-2 du code de justice administrative ;<br/>
<br/>
              Considérant que l'association locale pour le culte des Témoins de Jéhovah Lyon Lafayette s'est bornée à demander au juge des référés du tribunal d'enjoindre à la VILLE DE LYON de l'autoriser à louer une salle municipale pour la célébration prévue le 2 avril ; que, toutefois, le refus exprès adressé par l'adjoint au maire, en réponse à l'association, indiquait qu'il ne serait pas donné de suite favorable à sa demande ; qu'ainsi, en décidant que l'injonction adressée à la ville de louer une salle municipale impliquait que soit au préalable suspendue sa décision de refus, alors même que cette suspension n'avait pas été demandée, le juge des référés, à qui il incombait d'ordonner toutes mesures nécessaires pour la sauvegarde des libertés auxquelles il était porté atteinte, n'a pas, contrairement à ce que soutient la ville, statué ultra petita ;<br/>
<br/>
              Considérant qu'après avoir indiqué les motifs pour lesquels il décidait que la condition d'urgence propre aux dispositions de l'article L. 521-2 était satisfaite, le juge des référés a pris soin de relever que la VILLE DE LYON n'établissait pas que sa décision de refus de location de salle était justifiée par un motif d'ordre public et que le prix plus favorable que les tarifs des salles privées auquel serait soumise l'association ne saurait être regardée comme une subvention à un culte prohibée par la loi du 9 décembre 1905 ; qu'il en a déduit qu'une atteinte grave et manifestement illégale a été portée aux libertés d'association et de réunion ; qu'il a ainsi suffisamment motivé sa décision ; <br/>
<br/>
<br/>
              Sur la décision de refus : <br/>
              Considérant qu'aux termes de l'article L. 2144-3 du code général des collectivités territoriales : " Des locaux communaux peuvent être utilisés par les associations, syndicats ou partis politiques qui en font la demande./ Le maire détermine les conditions dans lesquelles ces locaux peuvent être utilisés, compte tenu des nécessités de l'administration des propriétés communales, du fonctionnement des services et du maintien de l'ordre public. (...) " ;<br/>
<br/>
              Considérant que, ainsi l'a relevé le juge des référés, l'association locale pour le culte des Témoins de Jéhovah Lyon-Lafayette a sollicité la location de la salle Victor Hugo pour le 2 avril 2007, de 18h30 à 22h30, dès le 8 novembre 2006 ; que si elle était ainsi titulaire d'une décision de refus le 9 janvier 2007, elle a, d'une part, cherché à connaître les motifs de ce refus, d'autre part, tenté de louer une autre salle, ainsi qu'il a été précisé à l'audience ; que, dans ces conditions, alors qu'il lui a été répondu par lettre du 23 février qu'il ne lui serait pas donné satisfaction, le juge des référés du tribunal administratif a pu, sans erreur de droit, décidé que la condition d'urgence particulière exigée par les dispositions de l'article L. 521-2 était satisfaite ; qu'il a pu aussi juger que le refus opposé à l'association, d'ailleurs consécutif à d'autres refus de même nature opposés à des associations identiques et annulés précédemment par le juge administratif, portait une atteinte grave et manifestement illégale à la liberté de réunion, qui est une liberté fondamentale, dès lors que la VILLE DE LYON ne faisait état d'aucune menace à l'ordre public, mais seulement de considérations générales relatives au caractère sectaire de l'association, ni d'aucun motif tiré des nécessités de l'administration des propriétés communales ou du fonctionnement des services, ;<br/>
<br/>
              Considérant que si l'article 2 de la loi du 9 décembre 1905 prohibe les subventions des cultes par les collectivités publiques, et si l'association locale pour le culte des Témoins de Jéhovah Lyon-Lafayette doit être regardée comme une association cultuelle, le prix acquitté par cette association pour la location de la salle ne saurait être regardé comme une subvention de la ville au motif que les tarifs des salles municipales seraient plus avantageux que ceux des salles privées, dès lors que la VILLE DE LYON n'établit pas, en tout état de cause, que l'association avait la possibilité de louer une salle privée au jour et aux heures qu'elle avait déterminés ; que la crainte, purement éventuelle, que les salles municipales soient l'objet de sollicitations répétées pour des manifestations à but religieux ne saurait davantage justifier légalement le refus de la ville ; <br/>
<br/>
              Considérant qu'il résulte de ce qui précède que la VILLE DE LYON n'est pas fondée à soutenir que c'est à tort que, par l'ordonnance attaquée, le juge des référés du tribunal administratif de Lyon a suspendu l'exécution de sa décision de refus et enjoint au maire de louer la salle Victor Hugo ou une autre salle municipale équivalente le 2 avril 2007 de 18h30 à 22h30 ;<br/>
<br/>
<br/>
              Sur les conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative :<br/>
              Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de l'association locale pour le culte des Témoins de Jéhovah Lyon Lafayette, qui n'est pas la partie perdante, la somme que demande la VILLE DE LYON au  titre des frais exposés par elle et non compris dans les dépens ; qu'il y a lieu, en revanche, de mettre à la charge de la VILLE DE LYON la somme de 5 000 euros que demande l'association locale pour le culte des Témoins de Jéhovah Lyon Lafayette au titre des mêmes frais ;<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de VILLE DE LYON est rejetée.<br/>
Article 2 : La VILLE DE LYON versera à l'association locale pour le culte des Témoins de Jéhovah de Lyon Lafayette la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente ordonnance sera notifiée à la VILLE DE LYON et à l'association locale pour le culte des Témoins de Jéhovah de Lyon Lafayette.<br/>
Copie en sera adressée pour information au ministre de l'intérieur et de l'aménagement du territoire.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-035-03-03-01-02 PROCÉDURE. PROCÉDURES INSTITUÉES PAR LA LOI DU 30 JUIN 2000. RÉFÉRÉ TENDANT AU PRONONCÉ DE MESURES NÉCESSAIRES À LA SAUVEGARDE D'UNE LIBERTÉ FONDAMENTALE (ART. L. 521-2 DU CODE DE JUSTICE ADMINISTRATIVE). CONDITIONS D'OCTROI DE LA MESURE DEMANDÉE. ATTEINTE GRAVE ET MANIFESTEMENT ILLÉGALE À UNE LIBERTÉ FONDAMENTALE. ATTEINTE GRAVE ET MANIFESTEMENT ILLÉGALE. - EXISTENCE - LIBERTÉ DE RÉUNION - DÉCISION REFUSANT LA LOCATION D'UNE SALLE MUNICIPALE À UNE ASSOCIATION CULTUELLE [RJ2].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-035-03-04-01 PROCÉDURE. PROCÉDURES INSTITUÉES PAR LA LOI DU 30 JUIN 2000. RÉFÉRÉ TENDANT AU PRONONCÉ DE MESURES NÉCESSAIRES À LA SAUVEGARDE D'UNE LIBERTÉ FONDAMENTALE (ART. L. 521-2 DU CODE DE JUSTICE ADMINISTRATIVE). POUVOIRS ET DEVOIRS DU JUGE. MESURES SUSCEPTIBLES D'ÊTRE ORDONNÉES PAR LE JUGE DES RÉFÉRÉS. - PRINCIPE - MESURE PRÉSENTANT UN CARACTÈRE PROVISOIRE [RJ1] - EXCEPTION - NÉCESSITÉ DE PRENDRE UNE MESURE DÉFINITIVE POUR SAUVEGARDER LA LIBERTÉ FONDAMENTALE À LAQUELLE IL EST PORTÉ ATTEINTE.
</SCT>
<ANA ID="9A"> 54-035-03-03-01-02 Le refus opposé à une association cultuelle de lui accorder la location d'une salle municipale, surtout lorsqu'il est consécutif à d'autres refus de même nature opposés à des associations identiques et annulés précédemment par le juge administratif, porte une atteinte grave et manifestement illégale à la liberté de réunion, qui est une liberté fondamentale, dès lors que la commune ne fait état d'aucune menace à l'ordre public, mais seulement de considérations générales relatives au caractère sectaire de l'association, ni d'aucun motif tiré des nécessités de l'administration des propriétés communales ou du fonctionnement des services.</ANA>
<ANA ID="9B"> 54-035-03-04-01 Demande tendant à l'annulation de l'ordonnance par laquelle le juge des référés du tribunal administratif a, d'une part, suspendu l'exécution de la décision par laquelle le maire a opposé un refus à la demande de location d'une salle municipale présentée par une association cultuelle, d'autre part, enjoint à la ville de mettre une salle municipale à la disposition de l'association, au jour qu'elle avait demandé, dans le but de permettre à ses adhérents de célébrer une fête religieuse. Si le juge des référés a ainsi enjoint à la ville de prendre une disposition qui n'avait pas de caractère provisoire, la nature de l'interdiction opposée à l'association et ses effets le lui permettaient, dès lors que, compte-tenu des circonstances, elle constituait la seule mesure propre à sauvegarder la liberté de réunion dont il a décidé qu'il y était gravement porté atteinte de manière manifestement illégale. Par suite, le juge des référés ne s'est pas prononcé sur des conclusions irrecevables et n'a pas méconnu la portée des dispositions de l'article L. 521-2 du code de justice administrative.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Comp. JRCE, 1er mars 2001, Paturel, n° 230794, T. p. 1134.,,[RJ2] Rappr., pour une décision refusant la tenue d'une réunion d'un parti politique, JRCE, 19 août 2002, Front National et Institut de formation des élus locaux (IFOREL), n° 249666, p. 311.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
