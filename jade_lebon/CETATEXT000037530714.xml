<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037530714</ID>
<ANCIEN_ID>JG_L_2018_10_000000403417</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/53/07/CETATEXT000037530714.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 5ème chambres réunies, 25/10/2018, 403417</TITRE>
<DATE_DEC>2018-10-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>403417</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP COUTARD, MUNIER-APAIRE</AVOCATS>
<RAPPORTEUR>Mme Laure Durand-Viel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Louis Dutheillet de Lamothe</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:403417.20181025</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A...B...a demandé au tribunal administratif de Cergy-Pontoise, d'une part, d'annuler pour excès de pouvoir la décision du 16 avril 2014 par laquelle le préfet des Hauts-de-Seine a rejeté sa demande visant à exonérer une personne majeure protégée de participation au financement de la mesure de protection, ainsi que les décisions du même préfet, en date des 27 mai et 16 juillet 2014, rejetant ses recours gracieux, d'autre part, d'enjoindre à ce préfet de lui verser, dans le délai d'un mois à compter de la notification du jugement, la somme de 3 417,27 euros assortie des intérêts au taux légal à compter du 7 avril 2014, au titre du financement de la mesure.<br/>
<br/>
              Par un jugement n° 1411109 du 11 juillet 2016, le tribunal administratif de Cergy-Pontoise a rejeté sa demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 9 septembre et 9 décembre 2016 au secrétariat du contentieux du Conseil d'État, Mme A... B...demande au Conseil d'État :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 200 euros en application de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code civil ;<br/>
              - le code de l'action sociale et des familles ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Laure Durand-Viel, auditeur,  <br/>
<br/>
              - les conclusions de M. Louis Dutheillet de Lamothe, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Coutard, Munier-Apaire, avocat de MmeB....<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1.	Il ressort des pièces du dossier soumis au juge du fond que Mme A... B...a été chargée, en qualité de mandataire judiciaire, de la protection de Mme E. R., placée sous sauvegarde de justice puis curatelle renforcée, du 26 avril 2013 au 20 décembre 2013. Mme B...a demandé, le 7 avril 2014, une exonération de participation de l'intéressée au financement de la mesure de protection, sur le fondement des dispositions de l'article R. 471-5-3 du code de l'action sociale et des familles, au motif que cette personne se trouvait dans une situation de surendettement. Sa demande a été rejetée par le préfet des Hauts-de-Seine par une décision du 16 avril 2014. Ses recours gracieux ont été également rejetés, par décisions des 27 mai et 16 juillet 2014. Mme B...a demandé au tribunal administratif de Cergy-Pontoise d'annuler pour excès de pouvoir ces décisions et d'enjoindre au préfet de lui verser la somme de 3 417,27 euros, avec intérêts, au titre du financement de la mesure. Par un jugement du 11 juillet 2016, contre lequel Mme B... se pourvoit en cassation, le tribunal administratif a rejeté sa demande. <br/>
<br/>
              2.	Les deuxième et troisième alinéas de l'article 419 du code civil disposent que : " Si la mesure judiciaire de protection est exercée par un mandataire judiciaire à la protection des majeurs, son financement est à la charge totale ou partielle de la personne protégée en fonction de ses ressources et selon les modalités prévues par le code de l'action sociale et des familles. / Lorsque le financement de la mesure ne peut être intégralement assuré par la personne protégée, il est pris en charge par la collectivité publique, selon des modalités de calcul communes à tous les mandataires judiciaires à la protection des majeurs et tenant compte des conditions de mise en oeuvre de la mesure, quelles que soient les sources de financement. Ces modalités sont fixées par décret ". Le premier alinéa de l'article L. 471-5 du code de l'action sociale et des familles prévoit que : " Le coût des mesures exercées par les mandataires judiciaires à la protection des majeurs et ordonnées par l'autorité judiciaire au titre du mandat spécial auquel il peut être recouru dans le cadre de la sauvegarde de justice ou au titre de la curatelle, de la tutelle ou de la mesure d'accompagnement judiciaire est à la charge totale ou partielle de la personne protégée en fonction de ses ressources. Lorsqu'il n'est pas intégralement supporté par la personne protégée, il est pris en charge dans les conditions fixées par les articles L. 361-1, L. 472-3 et L. 472-9. ". Pour l'application de ces dispositions, les articles R. 471-5, R. 471-5-1 et R. 471-5-2 du même code précisent les ressources prises en compte pour la détermination du montant de la participation de la personne protégée, les modalités de versement de cette participation, le seuil de ressources en-deçà duquel aucune participation n'est due, ainsi que le montant de cette participation au-delà. Le même code prévoit en outre, en son article R. 471-5-3, dans sa rédaction applicable au litige, que : " Le préfet peut accorder, à titre exceptionnel, temporaire, une exonération d'une partie ou de l'ensemble de la participation de la personne protégée, en raison de difficultés particulières liées à l'existence de dettes contractées par la personne protégée avant l'ouverture d'une mesure de protection juridique des majeurs ou à la nécessité de faire face à des dépenses impératives. / Le montant de la participation faisant l'objet de l'exonération est pris en charge dans les conditions prévues à la seconde phrase du premier alinéa de l'article L. 471-5. ".<br/>
<br/>
              3.	En premier lieu, aux termes de l'article R. 222-13 du code de justice administrative : " Le président du tribunal administratif ou le magistrat qu'il désigne à cette fin et ayant atteint au moins le grade de premier conseiller ou ayant une ancienneté minimale de deux ans statue en audience publique et après audition du rapporteur public, sous réserve de l'application de l'article R. 732-1-1 : / 1° Sur les litiges relatifs aux prestations, allocations ou droits attribués au titre de l'aide ou de l'action sociale, du logement ou en faveur des travailleurs privés d'emploi, mentionnés à l'article R. 772-5 (... ). ". Aux termes de l'article R. 772-5 du même code : " Sont présentées, instruites et jugées selon les dispositions du présent code, sous réserve des dispositions du présent chapitre, les requêtes relatives aux prestations, allocations ou droits attribués au titre de l'aide ou de l'action sociale, du logement ou en faveur des travailleurs privés d'emploi, sans préjudice des dispositions du chapitre VIII s'agissant du contentieux du droit au logement défini à l'article R. 778-1. ". Il résulte des dispositions précitées des articles L. 471-5 et R. 471-5-3 du code de l'action sociale et des familles que l'exonération et la prise en charge correspondante demandées sur le fondement de l'article R. 471-5-3 doivent être regardées comme une " prestation attribuée au titre de l'action sociale ", au sens des dispositions précitées de l'article R. 222-13 du code de justice administrative. Par suite, le moyen tiré de ce que le tribunal administratif aurait entaché son jugement d'irrégularité en statuant en formation de juge unique doit être écarté.<br/>
<br/>
              4.	En deuxième lieu, l'article R. 471-5-3 du code de l'action sociale et des familles donne compétence au préfet pour accorder une exonération de la participation de la personne protégée aux dépenses afférentes aux mesures de protection judiciaire la concernant. Le tribunal administratif, qui a relevé, sans être contesté sur ce point, que le signataire des décisions litigieuses bénéficiait d'une délégation de signature du préfet régulièrement publiée du 15 janvier 2014, n'était pas tenu de rechercher si ce signataire avait été chargé de l'intérim du directeur départemental de la cohésion sociale. Par suite, le tribunal administratif n'a pas entaché son jugement d'insuffisance de motivation en s'abstenant de répondre à ce moyen, qui était inopérant. Il n'a pas non plus commis d'erreur de droit ou de dénaturation des pièces du dossier en écartant le moyen tiré de l'incompétence du signataire des décisions au motif que l'arrêté préfectoral du 15 janvier 2014 donnait au signataire une large délégation s'agissant des décisions relatives à la protection juridique des majeurs et à la rémunération des mandataires judiciaires. <br/>
<br/>
              6. En troisième et dernier lieu, il résulte des dispositions des articles 419 du code civil et L. 471-5 du code de l'action sociale et des familles rappelées ci-dessus que la personne protégée assume le coût de sa protection en fonction de ses ressources et que, si ces dernières sont insuffisantes, ce coût est pris en charge par la collectivité publique. En application de ces dispositions, les articles R. 471-5-1 et R. 471-5-2 de ce code déterminent le montant de la participation de la personne protégée au financement du coût de la mesure de protection et le montant dont elle est exonérée, qui est pris en charge par la collectivité publique. En outre, l'article R. 471-5-3 du même code permet au préfet, dans des cas exceptionnels et à titre temporaire, de décider la prise en charge par la collectivité publique, totalement ou partiellement, des sommes restant en principe à la charge de la personne protégée, en cas de dettes contractées avant l'ouverture de la mesure de protection ou de nécessité de faire face à certaines dépenses impératives. En vertu du deuxième alinéa de ce dernier article, cette prise en charge est financée par les mêmes crédits que ceux assurant la prise en charge obligatoire par la collectivité publique en application des articles R. 471-5-1 et R. 471-5-2. Eu égard à leurs termes et à leur objet, qui est de permettre, à titre exceptionnel et en raison de circonstances particulières, une prise en charge temporaire totale ou partielle par la collectivité publique des dépenses incombant au majeur protégé, ces dispositions ne peuvent être regardées comme instituant un droit que pourrait revendiquer toute personne confrontée aux difficultés mentionnées à l'article R. 471-5-4. Dès lors, le préfet peut légalement se fonder, pour refuser d'accorder le bénéfice de cette mesure, sur des motifs tirés de l'insuffisance des crédits disponibles, sous le contrôle du juge administratif à qui il appartient de censurer une décision de refus en cas d'erreur de fait ou de droit, d'erreur manifeste d'appréciation ou de détournement de pouvoir. Par suite, le tribunal administratif n'a pas commis d'erreur de droit en jugeant que les dispositions précitées ne rendaient pas obligatoire la prise en charge par la collectivité publique des dépenses afférentes aux mesures de protection judiciaire en cause et en écartant, par voie de conséquence, le moyen tiré de ce que la requérante remplissait les conditions légales ouvrant droit à cet avantage. Dans ces conditions, il n'a pas davantage commis d'erreur de droit en jugeant que le préfet avait légalement pu se fonder, pour refuser le bénéfice de cette mesure, sur un motif tiré de l'absence de crédits disponibles. <br/>
<br/>
              7. Il résulte de ce tout qui précède que Mme B...n'est pas fondée à demander l'annulation du jugement qu'elle attaque. Il y a lieu, par suite, de rejeter son pourvoi, y compris les conclusions présentées sur le fondement de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
      Article 1er : Le pourvoi de Mme B...est rejeté.<br/>
<br/>
Article 2 : La présente décision sera notifiée à Mme A...B...et à la ministre des solidarités et de la santé. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-05-01-03 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - MOTIFS. POUVOIRS ET OBLIGATIONS DE L'ADMINISTRATION. COMPÉTENCE LIÉE. - MESURES DE PROTECTION DES PERSONNES MAJEURES - PRISE EN CHARGE DU COÛT DES MESURES EXERCÉES PAR LES MANDATAIRES JUDICIAIRES - POUVOIR RECONNU AU PRÉFET, À TITRE EXCEPTIONNEL ET TEMPORAIRE, DE PRENDRE EN CHARGE LE COÛT DE CES MESURES, EN PRINCIPE À LA CHARGE DE LA PERSONNE PROTÉGÉE, EN CAS DE DETTES CONTRACTÉES AVANT LA MESURE DE PROTECTION OU DE NÉCESSITÉ DE FAIRE FACE À CERTAINES DÉPENSES IMPÉRATIVES (ART. R. 471-5-3 CASF) - COMPÉTENCE LIÉE DU PRÉFET POUR FAIRE USAGE DE CE POUVOIR LORSQUE LA PERSONNE PROTÉGÉE EST CONFRONTÉE À DE TELLES DIFFICULTÉS - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">26-01-04 DROITS CIVILS ET INDIVIDUELS. ÉTAT DES PERSONNES. QUESTIONS DIVERSES RELATIVES À L`ÉTAT DES PERSONNES. - MESURES DE PROTECTION DES PERSONNES MAJEURES - PRISE EN CHARGE DU COÛT DES MESURES EXERCÉES PAR LES MANDATAIRES JUDICIAIRES - 1) PRINCIPE - PRISE EN CHARGE PAR LA PERSONNE PROTÉGÉE OU, SI SES RESSOURCES SONT INSUFFISANTES, PAR LA COLLECTIVITÉ PUBLIQUE EN TOTALITÉ OU EN PARTIE (ART. 419 DU CODE CIVIL ET 1ER AL. DE L'ART. L. 471-5 DU CASF) - 2) FACULTÉ RECONNUE AU PRÉFET, À TITRE EXCEPTIONNEL ET TEMPORAIRE, DE PRENDRE EN CHARGE LE COÛT DE CES MESURES EN CAS DE DETTES CONTRACTÉES AVANT LA MESURE DE PROTECTION OU DE NÉCESSITÉ DE FAIRE FACE À CERTAINES DÉPENSES IMPÉRATIVES (ART. R. 471-5-3 CASF) - OBLIGATION DE FAIRE USAGE DE CETTE FACULTÉ À L'ÉGARD DES PERSONNES CONFRONTÉES À DE TELLES DIFFICULTÉS - ABSENCE - POSSIBILITÉ DE REFUSER LA PRISE EN CHARGE EN RAISON DE L'INSUFFISANCE DES CRÉDITS DISPONIBLES - EXISTENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-07-02-04 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. CONTRÔLE DU JUGE DE L'EXCÈS DE POUVOIR. APPRÉCIATIONS SOUMISES À UN CONTRÔLE RESTREINT. - MESURES DE PROTECTION DES PERSONNES MAJEURES - PRISE EN CHARGE DU COÛT DES MESURES EXERCÉES PAR LES MANDATAIRES JUDICIAIRES - REFUS DU PRÉFET DE PRENDRE EN CHARGE, À TITRE EXCEPTIONNEL ET TEMPORAIRE, LE COÛT DE CES MESURES EN CAS DE DETTES CONTRACTÉES AVANT L'OUVERTURE DE LA MESURE DE PROTECTION OU DE NÉCESSITÉ DE FAIRE FACE À CERTAINES DÉPENSES IMPÉRATIVES (ART. R. 471-5-3 CASF).
</SCT>
<ANA ID="9A"> 01-05-01-03 L'article R. 471-5-3 du code de l'action sociale et des familles (CASF) permet au préfet, dans des cas exceptionnels et à titre temporaire, de décider la prise en charge par la collectivité publique, totalement ou partiellement, des sommes restant en principe à la charge de la personne protégée, en cas de dettes contractées avant l'ouverture de la mesure de protection ou de nécessité de faire face à certaines dépenses impératives.,,Eu égard à leurs termes et à leur objet, qui est de permettre, à titre exceptionnel et en raison de circonstances particulières, une prise en charge temporaire totale ou partielle par la collectivité publique des dépenses incombant au majeur protégé, ces dispositions ne peuvent être regardées comme instituant un droit que pourrait revendiquer toute personne confrontée aux difficultés mentionnées à l'article R. 471-5-3.... ...Dès lors, le préfet peut légalement se fonder, pour refuser d'accorder le bénéfice de cette mesure, sur des motifs tirés de l'insuffisance des crédits disponibles, sous le contrôle du juge administratif à qui il appartient de censurer une décision de refus en cas d'erreur de fait ou de droit, d'erreur manifeste d'appréciation ou de détournement de pouvoir.</ANA>
<ANA ID="9B"> 26-01-04 1) Il résulte de l'article 419 du code civil et du premier alinéa de l'article L. 471-5 du code de l'action sociale et des familles (CASF) que la personne protégée assume le coût de sa protection en fonction de ses ressources et que, si ces dernières sont insuffisantes, ce coût est pris en charge par la collectivité publique. En application de ces dispositions, les articles R. 471-5-1 et R. 471-5-2 de ce code déterminent le montant de la participation de la personne protégée au financement du coût de la mesure de protection et le montant dont elle est exonérée, qui est pris en charge par la collectivité publique.... ,,2) L'article R. 471-5-3 du CASF permet par ailleurs au préfet, dans des cas exceptionnels et à titre temporaire, de décider la prise en charge par la collectivité publique, totalement ou partiellement, des sommes restant en principe à la charge de la personne protégée, en cas de dettes contractées avant l'ouverture de la mesure de protection ou de nécessité de faire face à certaines dépenses impératives. En vertu du deuxième alinéa de ce dernier article, cette prise en charge est financée par les mêmes crédits que ceux assurant la prise en charge obligatoire par la collectivité publique en application des articles R. 471-5-1 et R. 471-5-2.... ...Eu égard à leurs termes et à leur objet, qui est de permettre, à titre exceptionnel et en raison de circonstances particulières, une prise en charge temporaire totale ou partielle par la collectivité publique des dépenses incombant au majeur protégé, ces dispositions ne peuvent être regardées comme instituant un droit que pourrait revendiquer toute personne confrontée aux difficultés mentionnées à l'article R. 471-5-3.... ...Dès lors, le préfet peut légalement se fonder, pour refuser d'accorder le bénéfice de cette mesure, sur des motifs tirés de l'insuffisance des crédits disponibles, sous le contrôle du juge administratif à qui il appartient de censurer une décision de refus en cas d'erreur de fait ou de droit, d'erreur manifeste d'appréciation ou de détournement de pouvoir.</ANA>
<ANA ID="9C"> 54-07-02-04 L'article R. 471-5-3 du code de l'action sociale et des familles (CASF) permet au préfet, dans des cas exceptionnels et à titre temporaire, de décider la prise en charge par la collectivité publique, totalement ou partiellement, des sommes restant en principe à la charge de la personne protégée, en cas de dettes contractées avant l'ouverture de la mesure de protection ou de nécessité de faire face à certaines dépenses impératives.... ...Eu égard à leurs termes et à leur objet, qui est de permettre, à titre exceptionnel et en raison de circonstances particulières, une prise en charge temporaire totale ou partielle par la collectivité publique des dépenses incombant au majeur protégé, ces dispositions ne peuvent être regardées comme instituant un droit que pourrait revendiquer toute personne confrontée aux difficultés mentionnées à l'article R. 471-5-3.... ...Dès lors, le préfet peut légalement se fonder, pour refuser d'accorder le bénéfice de cette mesure, sur des motifs tirés de l'insuffisance des crédits disponibles, sous le contrôle du juge administratif à qui il appartient de censurer une décision de refus en cas d'erreur de fait ou de droit, d'erreur manifeste d'appréciation ou de détournement de pouvoir.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
