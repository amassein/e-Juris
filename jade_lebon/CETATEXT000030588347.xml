<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030588347</ID>
<ANCIEN_ID>JG_L_2015_05_000000360662</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/58/83/CETATEXT000030588347.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème / 8ème SSR, 12/05/2015, 360662</TITRE>
<DATE_DEC>2015-05-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>360662</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème / 8ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>HAAS ; CARBONNIER</AVOCATS>
<RAPPORTEUR>M. Christophe Pourreau</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:360662.20150512</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Le 13 octobre 2010, M. A... B...a demandé au tribunal administratif de Montpellier d'annuler la décision du maire de la commune de Bassan de le maintenir dans un emploi non adapté, d'enjoindre au maire de l'affecter dans un emploi adapté et de condamner la commune à lui verser une première indemnité en réparation des pertes financières consécutives à son placement en congé de longue durée et une seconde indemnité en réparation du préjudice moral subi.<br/>
<br/>
              Par un jugement n° 1004510 du 7 mars 2012, le tribunal administratif de Montpellier a condamné la commune à verser à M. B... la somme de 300 euros en réparation du préjudice moral subi et rejeté le surplus des conclusions de sa demande.<br/>
<br/>
              Par une ordonnance n° 12MA01931 du 15 juin 2012, enregistrée le 2 juillet 2012 au secrétariat du contentieux du Conseil d'Etat, le président de la cour administrative d'appel de Marseille a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, le pourvoi, enregistré le 14 mai 2012 au greffe de cette cour, présenté par la commune de Bassan. Par ce pourvoi et par un mémoire complémentaire et un mémoire en réplique, enregistrés les 13 décembre 2012 et 22 août 2013 au secrétariat du contentieux du Conseil d'Etat, la commune de Bassan demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement du tribunal administratif de Montpellier du 7 mars 2012 en tant qu'il a partiellement fait droit aux conclusions indemnitaires de M. B... ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de M. B... ;<br/>
<br/>
              3°) de mettre à la charge de M. B... la somme de 3 000 euros au titre de l'article L. 761-1 du code justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la loi n° 84-53 du 26 janvier 1984 ;<br/>
              - le décret n° 87-602 du 30 juillet 1987 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Christophe Pourreau, maître des requêtes,<br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Haas, avocat de la commune de Bassan et à Me Carbonnier, avocat de M. A...B...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant, en premier lieu, que, lorsque, par suite des indications erronées portées sur la notification d'un jugement rendu en premier et dernier ressort, un requérant a formé un appel devant la cour administrative d'appel et qu'après que le président de celle-ci a transmis son recours au Conseil d'Etat en application de l'article R. 351-2 du code de justice administrative, le requérant a été invité à faire régulariser son pourvoi par un avocat au Conseil d'Etat et à la Cour de cassation en application des articles R. 612-1 et R. 821-3 du même code, le délai de deux mois à l'issue duquel le requérant n'est plus recevable à invoquer une cause juridique distincte court à compter de la réception par celui-ci de cette demande de régularisation ;<br/>
<br/>
              2. Considérant que le jugement du 7 mars 2012, rendu en premier et dernier ressort par le tribunal administratif de Montpellier, a été notifié à la commune de Bassan le 16 mars 2012 avec l'indication erronée d'une voie de recours devant la cour administrative d'appel de Marseille ; qu'à l'appui de son appel du 14 mai 2012 contre ce jugement, la commune n'a invoqué que des moyens contestant le bien-fondé du jugement ; que, le 8 août 2012, après que la cour administrative d'appel eut transmis le dossier au Conseil d'Etat, la commune a été invitée à faire régulariser son pourvoi par un avocat au Conseil d'Etat et à la Cour de cassation, ce qu'elle a fait le 6 septembre 2012 ; que le mémoire complémentaire dans lequel la commune a invoqué, devant le Conseil d'Etat, le moyen tiré de ce que le tribunal administratif n'a pas statué sur la fin de non-recevoir qu'elle avait opposée dans son mémoire en défense a été présenté le 13 décembre 2012, soit plus de deux mois après la réception de la demande de régularisation ; que ce moyen, qui relève d'une cause juridique distincte, est, par suite, irrecevable ;<br/>
<br/>
              3. Considérant, en deuxième lieu, qu'en vertu de l'article 57 de la loi du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale, tout fonctionnaire atteint d'une maladie le mettant dans l'impossibilité d'exercer ses fonctions a droit, sous conditions, à des congés de maladie, de longue maladie et de longue durée ; qu'aux termes de l'article 31 du décret du 30 juillet 1987 pris pour l'application de cette loi et relatif à l'organisation des comités médicaux, aux conditions d'aptitude physique et au régime des congés de maladie des fonctionnaires territoriaux : " Le bénéficiaire d'un congé de longue maladie ou de longue durée ne peut reprendre ses fonctions à l'expiration ou au cours dudit congé que s'il est reconnu apte après examen par un spécialiste agréé et avis favorable du comité médical compétent. (...) " ; qu'aux termes de l'article 32 du même décret : " Si, au vu de l'avis du comité médical compétent et éventuellement de celui du comité médical supérieur, dans le cas où l'autorité territoriale ou l'intéressé jugent utile de le provoquer, le fonctionnaire est reconnu apte à exercer ses fonctions, il reprend celles-ci dans les conditions fixées à l'article 33 ci-dessous. / Si, au vu des avis prévus ci-dessus, le fonctionnaire est reconnu inapte à exercer ses fonctions, le congé continue à courir ou, s'il était au terme d'une période, est renouvelé. Il en est ainsi jusqu'au moment où le fonctionnaire sollicite l'octroi de l'ultime période de congé rétribuée à laquelle il peut prétendre. (...) " ; qu'aux termes de l'article 33 du même décret : " Le comité médical, consulté sur l'aptitude d'un fonctionnaire territorial mis en congé de longue maladie ou de longue durée à reprendre l'exercice de ses fonctions, peut formuler des recommandations sur les conditions d'emploi de l'intéressé sans qu'il puisse porter atteinte à sa situation administrative. / (...) Si l'intéressé bénéficie d'un aménagement des conditions de son travail, le comité médical, après avis du service de médecine professionnelle et préventive, est appelé de nouveau, à l'expiration de périodes successives d'une durée comprise entre trois et six mois, à formuler des recommandations auprès de l'autorité territoriale sur l'opportunité du maintien ou de la modification de ces aménagements. (...) " ;<br/>
<br/>
              4. Considérant qu'il résulte de ces dispositions que, lorsque le comité médical compétent déclare qu'un fonctionnaire territorial bénéficiant d'un congé de longue maladie ou de longue durée est apte à reprendre ses fonctions à condition que son poste soit adapté à son état physique, il appartient à l'autorité territoriale de rechercher si un poste ainsi adapté peut être proposé au fonctionnaire ; que, si l'autorité territoriale ne peut pas lui proposer un tel poste, le congé se poursuit ou est renouvelé, jusqu'à ce que le fonctionnaire ait épuisé ses droits à congé pour raison de maladie ou ait été déclaré définitivement inapte à exercer ses fonctions ; que, par suite, en jugeant qu'il incombait à la commune de Bassan, dès lors que le poste adapté qu'elle avait proposé à M. A... B...avait été estimé incompatible avec l'état de santé de l'intéressé, de lui proposer un autre emploi adapté ou d'établir qu'elle était dans l'impossibilité de le faire, le tribunal administratif n'a pas commis d'erreur de droit ;<br/>
<br/>
              5. Considérant, en troisième lieu, que le tribunal administratif n'a pas dénaturé les pièces du dossier en jugeant que la commune de Bassan n'avait pas établi qu'elle était dans l'impossibilité de proposer à M. B... un poste adapté ;<br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède que la commune de Bassan n'est pas fondée à demander l'annulation du jugement attaqué ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à sa charge la somme de 3 000 euros à verser à M. B... au titre des dispositions de l'article L. 761-1 du code de justice administrative ; que ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de M. B..., qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la commune de Bassan est rejeté.<br/>
<br/>
Article 2 : La commune de Bassan versera à M. B... une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée à la commune de Bassan et à M. A... B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-05-04-02 FONCTIONNAIRES ET AGENTS PUBLICS. POSITIONS. CONGÉS. CONGÉS DE LONGUE DURÉE. - FONCTIONNAIRE TERRITORIAL EN CONGÉ DE LONGUE MALADIE OU DE LONGUE DURÉE DÉCLARÉ APTE À REPRENDRE SES FONCTIONS SUR UN POSTE ADAPTÉ - OBLIGATION POUR L'AUTORITÉ TERRITORIALE DE RECHERCHER SI UN POSTE ADAPTÉ PEUT ÊTRE PROPOSÉ - EXISTENCE - EN CAS D'IMPOSSIBILITÉ, CONGÉ JUSQU'À ÉPUISEMENT DES DROITS OU DÉCLARATION D'INAPTITUDE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">36-07-01-03 FONCTIONNAIRES ET AGENTS PUBLICS. STATUTS, DROITS, OBLIGATIONS ET GARANTIES. STATUT GÉNÉRAL DES FONCTIONNAIRES DE L'ÉTAT ET DES COLLECTIVITÉS LOCALES. DISPOSITIONS STATUTAIRES RELATIVES À LA FONCTION PUBLIQUE TERRITORIALE (LOI DU 26 JANVIER 1984). - FONCTIONNAIRE TERRITORIAL EN CONGÉ DE LONGUE MALADIE OU DE LONGUE DURÉE DÉCLARÉ APTE À REPRENDRE SES FONCTIONS SUR UN POSTE ADAPTÉ - OBLIGATION POUR L'AUTORITÉ TERRITORIALE DE RECHERCHER SI UN POSTE ADAPTÉ PEUT ÊTRE PROPOSÉ - EXISTENCE - EN CAS D'IMPOSSIBILITÉ, CONGÉ JUSQU'À ÉPUISEMENT DES DROITS OU DÉCLARATION D'INAPTITUDE.
</SCT>
<ANA ID="9A"> 36-05-04-02 Il résulte des dispositions de l'article 57 de la loi n° 84-53 du 26 janvier 1984 et des articles 31, 32 et 33 du décret n° 87-602 du 30 juillet 1987 que, lorsque le comité médical compétent déclare qu'un fonctionnaire territorial bénéficiant d'un congé de longue maladie ou de longue durée est apte à reprendre ses fonctions à condition que son poste soit adapté à son état physique, il appartient à l'autorité territoriale de rechercher si un poste ainsi adapté peut être proposé au fonctionnaire. Si l'autorité territoriale ne peut pas lui proposer un tel poste, le congé se poursuit ou est renouvelé, jusqu'à ce que le fonctionnaire ait épuisé ses droits à congé pour raison de maladie ou ait été déclaré définitivement inapte à exercer ses fonctions.</ANA>
<ANA ID="9B"> 36-07-01-03 Il résulte des dispositions de l'article 57 de la loi n° 84-53 du 26 janvier 1984 et des articles 31, 32 et 33 du décret n° 87-602 du 30 juillet 1987 que, lorsque le comité médical compétent déclare qu'un fonctionnaire territorial bénéficiant d'un congé de longue maladie ou de longue durée est apte à reprendre ses fonctions à condition que son poste soit adapté à son état physique, il appartient à l'autorité territoriale de rechercher si un poste ainsi adapté peut être proposé au fonctionnaire. Si l'autorité territoriale ne peut pas lui proposer un tel poste, le congé se poursuit ou est renouvelé, jusqu'à ce que le fonctionnaire ait épuisé ses droits à congé pour raison de maladie ou ait été déclaré définitivement inapte à exercer ses fonctions.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
