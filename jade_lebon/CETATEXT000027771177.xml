<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027771177</ID>
<ANCIEN_ID>JG_L_2013_07_000000347777</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/77/11/CETATEXT000027771177.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 4ème sous-sections réunies, 25/07/2013, 347777</TITRE>
<DATE_DEC>2013-07-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>347777</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 4ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BORE, SALVE DE BRUNETON</AVOCATS>
<RAPPORTEUR>Mme Domitille Duval-Arnould</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:347777.20130725</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 24 mars et 24 juin 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. A...B...agissant en son nom propre et en qualité de représentant légal de Mlle C...B..., Mme E... B...et Mlle D...B..., demeurant... ; les consorts B...demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 08MA01111 du 14 décembre 2010 par lequel la cour administrative d'appel de Marseille a rejeté leur appel contre le jugement n°0504701 du tribunal administratif de Marseille du 27 décembre 2007 rejetant leurs conclusions tendant à ce que l'Etat soit condamné à les indemniser du préjudice qu'ils ont subi du fait de la vaccination obligatoire administrée le 18 septembre 1989 à Mlle C...B... ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement d'une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
	Vu le code de la santé publique ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Domitille Duval-Arnould, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boré, Salve de Bruneton, avocat de Mlle C...B... ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que l'enfant SophieB..., alors âgée de 5 mois, a reçu le 18 septembre 1989 une injection du vaccin Tétracoq comprenant quatre valences dont trois correspondant à des valences obligatoires ; qu'à la suite de cette vaccination, elle a présenté des convulsions ainsi qu'une hémiparésie gauche et est demeurée atteinte d'une incapacité permanente partielle de 85% ; que ses parents, M. et Mme B..., ont recherché la responsabilité de l'Etat au titre des dommages causés par les vaccinations obligatoires ; que leur demande préalable tendant à l'indemnisation des préjudices subis a été rejetée ; que M. A...B..., agissant en son nom propre et en qualité de tuteur de SophieB..., Mme E...B...ainsi que Melle DéborahB..., soeur de Sophie, ont saisi le tribunal administratif de Marseille de demandes d'indemnisation que celui-ci a rejetées par un jugement en date du 27 décembre 2007 ; que les consorts B...se pourvoient en cassation contre l'arrêt du 14 décembre 2010 par lequel la cour administrative d'appel de Marseille a rejeté leur appel contre ce jugement ;  <br/>
<br/>
              2. Considérant qu'aux termes du premier alinéa de l'article L. 3111-9 du code de la santé publique, dans sa rédaction alors applicable : " Sans préjudice des actions qui pourraient être exercées conformément au droit commun, la réparation d'un dommage imputable directement à une vaccination obligatoire pratiquée dans les conditions mentionnées au présent chapitre est supportée par l'Etat " ; qu'il incombe aux juges du fond de déterminer, au vu des éléments apportés par les requérants, s'il existe un faisceau d'éléments de nature à établir ou faire présumer l'origine vaccinale du dommage ; que, dans le cas d'un vaccin associant des valences obligatoires et des valences facultatives, la responsabilité de l'Etat ne peut être écartée que s'il est démontré que les troubles sont exclusivement imputables à une valence facultative et si cette valence n'était pas systématiquement associée aux valences obligatoires dans les vaccins disponibles ; <br/>
<br/>
              3. Considérant que, pour écarter la responsabilité de l'Etat, les juges du fond ont relevé qu'il résultait de l'instruction, et notamment des différents avis médicaux versés au dossier qui leur était soumis, que n'était pas rapportée la preuve d'un lien de causalité direct entre l'apparition des troubles dont est atteinte Sophie B...et les trois valences obligatoires du vaccin Tétracoq qui lui a été administré ; qu'il résulte de ce qui a été dit ci-dessus qu'en se fondant sur ce seul motif,  la cour a commis une erreur de droit ; que, par suite, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, les consorts B...sont fondés à demander l'annulation de l'arrêt attaqué ; <br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement d'une somme de 3 000 euros à verser aux consorts B...au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Marseille n° 08MA01111 du 14 décembre 2010 est annulé.<br/>
<br/>
		Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Marseille.<br/>
<br/>
Article 3 : L'Etat versera aux consorts B...une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à M. A...B..., à Mme E...B..., à Mlle D...B..., au ministre du travail de l'emploi et de la santé et à la caisse primaire d'assurance maladie des Bouches du Rhône.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">60-02-01-03 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RESPONSABILITÉ EN RAISON DES DIFFÉRENTES ACTIVITÉS DES SERVICES PUBLICS. SERVICE PUBLIC DE SANTÉ. SERVICE DES VACCINATIONS. - ARTICLE L. 3111-9 DU CODE DE LA SANTÉ PUBLIQUE - CHAMP D'APPLICATION - VACCIN POLYVALENT COMPORTANT AU MOINS UNE VALENCE CORRESPONDANT À UNE VACCINATION OBLIGATOIRE - INCLUSION - EXCEPTION - DÉMONSTRATION QUE LES TROUBLES SONT IMPUTABLES À L'UNE DES VALENCES FACULTATIVES ET ABSENCE D'ASSOCIATION SYSTÉMATIQUE DE CETTE VALENCE AUX VALENCES OBLIGATOIRES DANS LES VACCINS DISPONIBLES [RJ1].
</SCT>
<ANA ID="9A"> 60-02-01-03 Dès lors qu'un vaccin comporte au moins une valence correspondant à une vaccination obligatoire, la responsabilité de l'Etat au titre de l'article L. 3111-9 du code de la santé publique (prévoyant que la réparation d'un dommage imputable directement à une vaccination obligatoire est supportée par l'Etat) ne peut être écartée que s'il est démontré que les troubles sont exclusivement imputables à une valence facultative et si cette valence n'était pas systématiquement associée aux valences obligatoires dans les vaccins disponibles.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., pour le principe que cette décision vient confirmer et préciser, CE, 24 avril 2012, Ministre de la santé et des sports c/ Chelhi et Mme Dion, n° 327915, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
