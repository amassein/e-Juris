<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038227993</ID>
<ANCIEN_ID>JG_L_2019_03_000000418994</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/22/79/CETATEXT000038227993.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 5ème chambres réunies, 13/03/2019, 418994</TITRE>
<DATE_DEC>2019-03-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>418994</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Cyrille Beaufils</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Louis Dutheillet de Lamothe</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2019:418994.20190313</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              1° Sous le n° 418994, par une requête et un mémoire en réplique, enregistrés les 14 mars 2018 et 11 février 2019 au secrétariat du contentieux du Conseil d'Etat, l'association Alsace nature demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2018-36 du 22 janvier 2018 prorogeant les effets du décret du 23 janvier 2008 déclarant d'utilité publique et urgents les travaux de l'autoroute A 355, grand contournement ouest de Strasbourg, entre le noeud autoroutier A 4-A 35 et le noeud autoroutier A 352-A 35 dans le département du Bas-Rhin ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              2° Sous le n° 419239, par une requête enregistrée le 23 mars 2018 au secrétariat du contentieux du Conseil d'Etat, la commune de Kolbsheim demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2018-36 du 22 janvier 2018 prorogeant les effets du décret du 23 janvier 2008 déclarant d'utilité publique et urgents les travaux de l'autoroute A 355, grand contournement ouest de Strasbourg, entre le noeud autoroutier A 4-A 35 et le noeud autoroutier A 352-A 35 dans le département du Bas-Rhin ;<br/>
<br/>
<br/>
<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - le code civil ;<br/>
              - le code de l'environnement ; <br/>
              - le code de l'expropriation pour cause d'utilité publique ;<br/>
              - le code des transports ; <br/>
              - la loi n° 2016-1087 du 8 août 2016 ;<br/>
              - l'ordonnance n° 2014-1345 du 6 novembre 2014 ;<br/>
              - le décret du 23 janvier 2008 déclarant d'utilité publique et urgents les travaux de construction de l'autoroute A 355, grand contournement ouest de Strasbourg, entre le noeud autoroutier A 4-A 35 (communes de Hoerdt, Geudertheim, Brumath, Reichstett et Vendenheim) et le noeud autoroutier A 352-A 35 (communes de Duppigheim, Duttlenheim et Innenheim);<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Cyrille Beaufils, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Louis Dutheillet de Lamothe, rapporteur public.<br/>
<br/>
              Vu la note en délibéré, enregistrée le 19 février 2018, présentée par l'association Alsace nature.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1.	Par un décret du 23 janvier 2008, le Premier ministre a déclaré d'utilité publique et urgents les travaux de l'autoroute A 355, dite " grand contournement ouest de Strasbourg ", et fixé à dix ans à compter de la publication du décret le délai pour réaliser les expropriations nécessaires. Un décret du 22 janvier 2018 a prorogé ce délai jusqu'au 22 janvier 2026. L'association Alsace nature et la commune de Kolbsheim demandent l'annulation pour excès de pouvoir de ce décret par deux requêtes qu'il y a lieu de joindre pour statuer par une seule décision.<br/>
<br/>
              2.	L'article L. 1 du code de l'expropriation pour cause d'utilité publique dispose que : " L'expropriation, en tout ou partie, d'immeubles ou de droits réels immobiliers ne peut être prononcée qu'à la condition qu'elle réponde à une utilité publique préalablement et formellement constatée à la suite d'une enquête (...) ". Son article L. 121-2 prévoit que : " L'acte déclarant l'utilité publique ou la décision refusant de la déclarer intervient au plus tard un an après la clôture de l'enquête préalable. (...) ". Aux termes du premier alinéa de son article L. 121-4 : " L'acte déclarant l'utilité publique précise le délai accordé pour réaliser l'expropriation. Il ne peut excéder cinq ans, si la déclaration d'utilité publique n'est pas prononcée par décret en Conseil d'Etat en application de l'article L. 121-1. ". Enfin, aux termes de l'article L. 121-5 du même code : " Un acte pris dans la même forme peut proroger une fois les effets de la déclaration d'utilité publique pour une durée au plus égale à la durée initialement fixée, lorsque celle-ci n'est pas supérieure à cinq ans. Cette prorogation peut être accordée sans nouvelle enquête préalable, en l'absence de circonstances nouvelles. / Toute autre prorogation ne peut être prononcée que par décret en Conseil d'Etat. "<br/>
<br/>
              3.	En premier lieu, il ne ressort pas des pièces des dossiers que le Premier ministre se serait cru lié par l'engagement, mentionné dans le cahier des charges du contrat de concession du projet, de proroger, à la demande du concessionnaire, le délai ouvert pour les expropriations et aurait ainsi renoncé à exercer son pouvoir d'appréciation sur la nécessité d'une telle prorogation. Le moyen tiré de ce qu'il aurait, pour ce motif, méconnu l'étendue de sa compétence doit donc être écarté.<br/>
<br/>
              4. En deuxième lieu, il résulte des dispositions citées au point 2 que l'autorité compétente peut proroger les effets d'un acte déclaratif d'utilité publique, sauf si l'opération n'est plus susceptible d'être légalement réalisée en raison de l'évolution du droit applicable ou s'il apparaît que le projet a perdu son caractère d'utilité publique par suite d'un changement des circonstances de fait. Cette prorogation peut être décidée sans procéder à une nouvelle enquête publique, alors même que le contexte dans lequel s'inscrit l'opération aurait connu des évolutions significatives, sauf si les caractéristiques du projet sont substantiellement modifiées. A cet égard, une augmentation de son coût dans des proportions de nature à en affecter l'économie générale doit être regardée comme une modification substantielle. <br/>
<br/>
              5. Les modifications apportées au projet depuis la déclaration d'utilité publique, qui consistent notamment en des rectifications du tracé, en l'abandon de la possibilité d'élargissement de l'infrastructure à deux fois trois voies, en la création d'un pôle d'échange multimodal et en la reconfiguration de l'échangeur nord ne peuvent être regardées, en l'espèce, comme des modifications substantielles des caractéristiques du projet. L'évolution du coût du projet, qui est de l'ordre de 12 % hors inflation, ne peut être regardée comme affectant son économie générale. En outre, les requérants, qui ne soutiennent pas que le projet a perdu son utilité publique, ne peuvent utilement exciper, pour soutenir que la prorogation ne pouvait être décidée sans une nouvelle enquête publique, de ce que le contexte aurait connu des évolutions significatives depuis l'intervention de l'acte déclaratif d'utilité publique. Par suite, il se déduit de l'application des principes rappelés au point précédent que le moyen tiré de ce que le décret de prorogation attaqué serait intervenu en méconnaissance du II de l'article L. 121-5 du code de l'expropriation pour cause d'utilité publique, faute qu'ait été organisée une nouvelle enquête publique, doit être écarté. <br/>
<br/>
              6. En troisième lieu, par son article 2, le décret du 23 janvier 2008 a prescrit que les expropriations nécessaires devaient être réalisées dans un délai de dix ans à compter de sa publication. Ce décret a été publié le 24 janvier 2008. Compte tenu des conditions d'entrée en vigueur de ce texte telles qu'elles résultent de l'article 1er du code civil, le délai de dix ans mentionné ci-dessus n'était pas expiré le 22 janvier 2018, date à laquelle est intervenu le décret attaqué. Le moyen tiré de ce que les effets de la déclaration d'utilité publique du 23 janvier 2008 ne pouvaient légalement être prorogés au motif que celle-ci était caduque doit par suite être écarté.<br/>
<br/>
              7. En dernier lieu, les requérantes ne sauraient utilement exciper de la méconnaissance des dispositions de l'article L. 122-2 du code de l'expropriation pour cause d'utilité publique, qui imposent que la déclaration d'utilité publique comporte les mesures prévues au I de l'article L. 122-1-1 du code de l'environnement destinées à éviter, réduire ou compenser les incidences négatives notables du projet sur l'environnement, dès lors que le décret attaqué n'est pas une déclaration d'utilité publique mais la simple prorogation d'un tel acte. <br/>
<br/>
              8. Il résulte de tout ce qui précède que l'association Alsace nature et la commune de Kolbsheim ne sont pas fondées à demander l'annulation du décret qu'elles attaquent. Par suite, sans qu'il soit besoin de se prononcer sur leur recevabilité, leurs requêtes doivent être rejetées, y compris leurs conclusions présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les requêtes de l'association Alsace nature et la commune de Kolbsheim sont rejetées. <br/>
<br/>
Article 2 : La présente décision sera notifiée à l'association Alsace nature, à la commune de Kolbsheim et au ministre d'Etat, ministre de la transition écologique et solidaire.<br/>
      Copie en sera adressée au Premier ministre. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">34-02-02-03 EXPROPRIATION POUR CAUSE D'UTILITÉ PUBLIQUE. RÈGLES GÉNÉRALES DE LA PROCÉDURE NORMALE. ACTE DÉCLARATIF D'UTILITÉ PUBLIQUE. PROROGATION. - 1) CONDITIONS [RJ1] - 2) NÉCESSITÉ D'UNE NOUVELLE ENQUÊTE PUBLIQUE - ABSENCE, SAUF SI LES CARACTÉRISTIQUES DU PROJET SONT SUBSTANTIELLEMENT MODIFIÉES [RJ2].
</SCT>
<ANA ID="9A"> 34-02-02-03 1) Il résulte des articles L. 1, L. 121-2, L. 121-4 et L. 121-5 du code de l'expropriation pour cause d'utilité publique que l'autorité compétente peut proroger les effets d'un acte déclaratif d'utilité publique, sauf si l'opération n'est plus susceptible d'être légalement réalisée en raison de l'évolution du droit applicable ou s'il apparaît que le projet a perdu son caractère d'utilité publique par suite d'un changement des circonstances de fait.... ...2) Cette prorogation peut être décidée sans procéder à une nouvelle enquête publique, alors même que le contexte dans lequel s'inscrit l'opération aurait connu des évolutions significatives, sauf si les caractéristiques du projet sont substantiellement modifiées. A cet égard, une augmentation de son coût dans des proportions de nature à en affecter l'économie générale doit être regardée comme une modification substantielle.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Section, 25 mai 1979, Mme,, p. 239.,,[RJ2] Cf. CE, 26 septembre 2001, M.,et autres, n° 220921, T. p. 1001 ; CE, 25 juin 2003, Union départementale vie et nature des Alpes de Haute Provence et autres, n° 240040, T. p. 817.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
