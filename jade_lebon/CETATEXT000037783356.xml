<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037783356</ID>
<ANCIEN_ID>JG_L_2018_12_000000418821</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/78/33/CETATEXT000037783356.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 07/12/2018, 418821</TITRE>
<DATE_DEC>2018-12-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>418821</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Louise Bréhier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie Roussel</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:418821.20181207</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 6 mars et 9 juillet 2018 au secrétariat du contentieux du Conseil d'Etat, M. B...A...et le Front des patriotes républicains demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision par laquelle le ministre de l'intérieur a attribué la nuance politique " EXD " (extrême-droite) aux sept candidats présentés par le Front des patriotes républicains lors des élections législatives de juin 2017, la décision du 2 juin 2017 par laquelle le ministre de l'intérieur a rejeté leur demande de rectification de la nuance politique attribuée à ces candidats ainsi que la décision du 16 août 2017 par laquelle le ministre de l'intérieur a rejeté leur recours hiérarchique contre ces décisions ; <br/>
<br/>
              2°) d'enjoindre au ministre de l'intérieur d'attribuer à ces sept candidats la nuance politique " DVD " (divers droite) ; <br/>
<br/>
              3°) d'enjoindre au ministre de l'intérieur de publier ces nouvelles nuances politiques, notamment par la mise à jour des résultats des élections législatives sur le site www.interieur.gouv.fr, dans un délai de deux jours à compter de la décision à intervenir et sous astreinte de 300 euros par jour de retard ; <br/>
<br/>
              4°) d'enjoindre au ministre de l'intérieur d'attribuer au candidat du Front des patriotes républicains à l'élection partielle d'avril 2018 dans la 5e circonscription des Français de l'étranger, M. B...A..., la nuance politique " DVD " (divers droite) ; <br/>
<br/>
              5°) de mettre à la charge de l'Etat une somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier :<br/>
<br/>
              Vu : <br/>
              - la loi n° 78-17 du 6 janvier 1978 ;<br/>
              - le décret n° 2014-1479 du 9 décembre 2014 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Louise Bréhier, auditrice, <br/>
<br/>
              - les conclusions de Mme Sophie Roussel, rapporteur public,<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant que le décret du 9 décembre 2014 relatif à la mise en oeuvre de deux traitements automatisés de données à caractère personnel dénommés " Application élection " et " Répertoire national des élus " prévoit que le ministre de l'intérieur établit une  grille des nuances politiques pour l'enregistrement des résultats de l'élection ; que selon l'article 5 de ce décret, figure parmi les données à caractère personnel et les informations enregistrées portant sur les candidats la nuance politique attribuée à chaque candidat par l'administration ; que l'article 9 du même décret dispose que " les droits d'accès et de rectification prévus par les articles 39 et 40 de la loi du 6 janvier 1978 s'exercent auprès de l'autorité administrative qui a enregistré la candidature " et " qu'au moment du dépôt de sa candidature, le candidat est informé :/ 1° de la grille des nuances politiques retenue pour l'enregistrement des résultats de l'élection ;/ 2° Du fait qu'il peut avoir accès au classement qui lui est affecté et en demander la rectification, conformément à l'article 39 de la loi du 6 janvier 1978 (...) " ;<br/>
<br/>
              2.	Considérant qu'il ressort des pièces du dossier que les candidats du Front des patriotes républicains ont été classés dans la rubrique " extrême droite " de la grille des nuances politiques publiée le 23 mai 2017 en vue des élections législatives de juin 2017 ; qu'en vertu des dispositions précitées de l'article 9 du décret du 9 décembre 2014, M. A...et le Front des patriotes républicains ont demandé au ministre de l'intérieur de rectifier la nuance politique attribuée aux candidats de ce parti politique pour les classer dans la rubrique " divers droite " ; que le ministre a rejeté leur demande par une décision du 2 juin 2017 ; que, dans le dernier état de leurs écritures, M. A...et le Front des patriotes républicains demandent l'annulation des rejets de leurs demandes tendant à ce que la nuance " divers droite " soit attribuée aux candidats du Front des patriotes républicains ; <br/>
<br/>
              3.	Considérant qu'en vertu de l'article L. 311-1 du code de justice administrative, les tribunaux administratifs " sont, en premier ressort, juges de droit commun du contentieux administratif, sous réserve des compétences que l'objet du litige ou l'intérêt d'une bonne administration de la justice conduisent à attribuer à une autre juridiction administrative " ; qu'aux termes de l'article R. 311-1 du même code : " Le Conseil d'Etat est compétent pour connaître en premier et dernier ressort :/ (...) 2° Des recours dirigés contre les actes réglementaires des ministres et des autres autorités à compétence nationale et contre leurs circulaires et instructions de portée générale " ; <br/>
<br/>
              4.	Considérant que si la décision par laquelle le ministre de l'intérieur établit une " grille des nuances politiques " pour l'enregistrement des résultats d'une élection présente un caractère réglementaire, la décision par laquelle l'autorité administrative qui a enregistré sa candidature attribue à un candidat une nuance politique parmi celles figurant dans cette grille ne présente pas ce caractère ; <br/>
<br/>
              5.	Considérant que ni les dispositions de l'article R. 311-1 du code de justice administrative ni aucune autre disposition ne donnent compétence au Conseil d'Etat pour connaître en premier ressort des conclusions à fin d'annulation présentées par M. A...et le Front des patriotes républicains ; qu'il y a lieu, en application de l'article R. 351-1 du même code, d' attribuer le jugement de ces conclusions, ensemble les conclusions à fin d'injonction et celles tendant à l'application des dispositions de l'article L 761-1 du code de justice administrative, au tribunal administratif de Paris, compétent pour en connaître en vertu de l'article R. 312-1 de ce code ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le jugement de la requête de M. A...et du Front des patriotes républicains est renvoyé au tribunal administratif de Paris. <br/>
<br/>
Article 2 : La présente décision sera notifiée à M. B...A...et au ministre de l'intérieur.  <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-01-06-01 ACTES LÉGISLATIFS ET ADMINISTRATIFS. DIFFÉRENTES CATÉGORIES D'ACTES. ACTES ADMINISTRATIFS - CLASSIFICATION. ACTES RÉGLEMENTAIRES. - DÉCISION PAR LAQUELLE LE MINISTRE DE L'INTÉRIEUR ÉTABLIT UNE GRILLE DES NUANCES POLITIQUES [RJ1] POUR L'ENREGISTREMENT DES RÉSULTATS D'UNE ÉLECTION - EXISTENCE - DÉCISION PAR LAQUELLE L'AUTORITÉ ADMINISTRATIVE QUI A ENREGISTRÉ LA CANDIDATURE ATTRIBUE AU CANDIDAT EN QUESTION UNE NUANCE POLITIQUE PARMI CELLES FIGURANT DANS CETTE GRILLE - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">17-05-01-01-01 COMPÉTENCE. COMPÉTENCE À L'INTÉRIEUR DE LA JURIDICTION ADMINISTRATIVE. COMPÉTENCE EN PREMIER RESSORT DES TRIBUNAUX ADMINISTRATIFS. COMPÉTENCE MATÉRIELLE. ACTES NON RÉGLEMENTAIRES. - DÉCISION PAR LAQUELLE L'AUTORITÉ ADMINISTRATIVE QUI A ENREGISTRÉ LA CANDIDATURE ATTRIBUE AU CANDIDAT EN QUESTION UNE NUANCE POLITIQUE PARMI CELLES FIGURANT DANS UNE GRILLE DES NUANCES POLITIQUES [RJ1] ÉTABLIE PAR LE MINISTRE DE L'INTÉRIEUR.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">28-005 ÉLECTIONS ET RÉFÉRENDUM. DISPOSITIONS GÉNÉRALES APPLICABLES AUX ÉLECTIONS. - GRILLE DES NUANCES POLITIQUES [RJ1] POUR L'ENREGISTREMENT DES RÉSULTATS (DÉCRET N° 2014-1479 DU 9 DÉCEMBRE 2014) - DÉCISION DU MINISTRE DE L'INTÉRIEUR L'ÉTABLISSANT - ACTE PRÉSENTANT UN CARACTÈRE RÉGLEMENTAIRE - DÉCISION PAR LAQUELLE L'AUTORITÉ ADMINISTRATIVE QUI A ENREGISTRÉ LA CANDIDATURE ATTRIBUE AU CANDIDAT EN QUESTION UNE NUANCE PARMI CELLES FIGURANT DANS CETTE GRILLE - ACTE NE PRÉSENTANT PAS UN CARACTÈRE RÉGLEMENTAIRE.
</SCT>
<ANA ID="9A"> 01-01-06-01 Si la décision par laquelle le ministre de l'intérieur établit une grille des nuances politiques pour l'enregistrement des résultats d'une élection présente un caractère réglementaire, la décision par laquelle l'autorité administrative qui a enregistré la candidature attribue au candidat en question une nuance politique parmi celles figurant dans cette grille ne présente pas ce caractère.</ANA>
<ANA ID="9B"> 17-05-01-01-01 Si la décision par laquelle le ministre de l'intérieur établit une grille des nuances politiques pour l'enregistrement des résultats d'une élection présente un caractère réglementaire, la décision par laquelle l'autorité administrative qui a enregistré la candidature attribue au candidat en question une nuance politique parmi celles figurant dans cette grille ne présente pas ce caractère. Ni les dispositions de l'article R. 311-1 du code de justice administrative (CJA) ni aucune autre disposition ne donnent compétence au Conseil d'Etat pour en connaître en premier ressort.</ANA>
<ANA ID="9C"> 28-005 Si la décision par laquelle le ministre de l'intérieur établit une grille des nuances politiques pour l'enregistrement des résultats d'une élection présente un caractère réglementaire, la décision par laquelle l'autorité administrative qui a enregistré la candidature attribue au candidat en question une nuance politique parmi celles figurant dans cette grille ne présente pas ce caractère. Ni les dispositions de l'article R. 311-1 du code de justice administrative (CJA) ni aucune autre disposition ne donnent compétence au Conseil d'Etat pour en connaître en premier ressort.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr. CE, 2 avril 2003, Parti des travailleurs et M.,, n° 246993, T. pp. 790-938.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
