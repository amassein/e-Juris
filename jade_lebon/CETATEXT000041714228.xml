<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041714228</ID>
<ANCIEN_ID>JG_L_2020_03_000000422704</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/71/42/CETATEXT000041714228.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 5ème chambres réunies, 11/03/2020, 422704</TITRE>
<DATE_DEC>2020-03-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>422704</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BORE, SALVE DE BRUNETON, MEGRET ; SCP DIDIER, PINET</AVOCATS>
<RAPPORTEUR>M. Didier Ribes</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Louis Dutheillet de Lamothe</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2020:422704.20200311</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La fédération départementale de pêche et de protection du milieu aquatique de l'Isère et l'association agréée de pêche et de protection du milieu aquatique du Valbonnais " La Truite de La Bonne " ont demandé au tribunal administratif de Grenoble d'annuler l'arrêté du 6 mai 2013 du préfet de l'Isère valant règlement d'eau relatif à l'exploitation d'un aménagement hydroélectrique sur la rivière de La Bonne à Valjouffrey au bénéfice de la société Valhydrau. Par un jugement n° 1403798 du 4 octobre 2016, le tribunal administratif a rejeté leur demande.<br/>
<br/>
              Par un arrêt n° 16LY04051 du 29 mai 2018, la cour administrative d'appel de Lyon a, sur appel de la fédération départementale de pêche et de protection du milieu aquatique de l'Isère et de l'association agréée de pêche et de protection du milieu aquatique du Valbonnais " La Truite de La Bonne ", annulé ce jugement ainsi que l'arrêté préfectoral du 6 mai 2013.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un nouveau mémoire, enregistrés les 30 juillet et 31 octobre 2018 et le 27 janvier 2020 au secrétariat du contentieux du Conseil d'État, la société Valhydrau demande au Conseil d'État :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses conclusions d'appel ;<br/>
<br/>
              3°) de mettre à la charge de la fédération départementale de pêche et de protection du milieu aquatique de l'Isère et de l'association agréée de pêche et de protection du milieu aquatique du Valbonnais " La Truite de La Bonne " la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'environnement ;<br/>
              - la loi n° 2006-1772 du 30 décembre 2016 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Didier Ribes, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Louis Dutheillet de Lamothe, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boré, Salve de Bruneton, Mégret, avocat de la société Valhydrau et à la SCP Didier, Pinet, avocat de la fédération départementale de pêche et de protection du milieu aquatique de l'Isère et autre ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 6 mai 2013, le préfet de l'Isère a autorisé la société Valhydrau à disposer, pour une durée de trente-cinq ans, de l'énergie de la rivière La Bonne et a défini le règlement d'eau relatif à l'exploitation d'une centrale hydroélectrique en dérivation de la rivière. Par un jugement du 4 octobre 2016, le tribunal administratif de Grenoble a rejeté la demande d'annulation de cet arrêté présentée par la fédération départementale de pêche et de protection du milieu aquatique de l'Isère et par l'association agréée de pêche et de protection du milieu aquatique du Valbonnais " La Truite de La Bonne ". Sur appel de ces associations, la cour administrative d'appel de Lyon a, par un arrêt du 29 mai 2018, annulé le jugement du tribunal administratif ainsi que l'arrêté du 6 mai 2013. La société Valhydrau se pourvoit en cassation contre cet arrêt.<br/>
<br/>
              Sur le cadre juridique du litige :<br/>
<br/>
              2. Il résulte des dispositions de l'article L. 212-1 du code de l'environnement que le schéma directeur d'aménagement et de gestion des eaux (SDAGE), d'une part, fixe, pour chaque bassin ou groupement de bassins, les objectifs de qualité et de quantité des eaux ainsi que les orientations permettant d'assurer une gestion équilibrée et durable de la ressource en eau et, d'autre part, détermine à cette fin les aménagements et les dispositions nécessaires. En outre, lorsque cela apparaît nécessaire pour respecter ses orientations et ses objectifs, le SDAGE peut être complété, pour un périmètre géographique donné, par un schéma d'aménagement et de gestion des eaux (SAGE) qui doit être compatible avec lui et qui comporte, en vertu de l'article L. 212-5-1 du code de l'environnement, d'une part, un plan d'aménagement et de gestion durable de la ressource en eau et des milieux aquatiques (PAGD) et, d'autre part, un règlement, qui peut prévoir les obligations définies au II de cet article. En vertu du XI de l'article L. 212-1 et de l'article L. 212-5-2 du code de l'environnement, les décisions administratives prises dans le domaine de l'eau doivent être compatibles avec le SDAGE et avec le PAGD du SAGE. En revanche, les décisions administratives prises au titre de la police de l'eau en application des articles L. 214-1 et suivants du code de l'environnement doivent être conformes au règlement du SAGE et à ses documents cartographiques, dès lors que les installations, ouvrages, travaux et activités en cause sont situés sur un territoire couvert par un tel document.<br/>
<br/>
              3. Aux termes de l'article L. 212-10 du code de l'environnement, issu de la loi du 30 décembre 2006 sur l'eau et les milieux aquatiques, dans sa version alors en vigueur : " I. - Un projet de schéma d'aménagement et de gestion des eaux arrêté par la commission locale de l'eau à la date de publication du décret prévu à l'article L. 212-11 peut être approuvé selon la procédure prévue par les dispositions législatives et réglementaires antérieures pendant un délai de trois ans à compter de cette même date. Le schéma approuvé constitue le plan d'aménagement et de gestion durable de la ressource défini au I de l'article L. 212-5-1. / II. - Les schémas d'aménagement et de gestion des eaux approuvés à la date de promulgation de la loi n° 2006-1772 du 30 décembre 2006 précitée ou en application du I du présent article sont complétés dans un délai de six ans à compter de la promulgation de ladite loi par le règlement prévu au II de l'article L. 212-5-1, approuvé selon la procédure fixée par l'article L. 212-6 ". Le décret auquel se réfère le I de l'article L. 212-10 a été publié le 14 août 2007.<br/>
<br/>
              4. Les dispositions de l'article L. 212-10 du code de l'environnement ont pour objet de permettre, dans les conditions et limites qu'elles prévoient, que les SAGE déjà approuvés ou en cours d'élaboration lors de la promulgation de la loi du 30 décembre 2006 relèvent du régime prévu par cette loi pour les futurs SAGE. Il ne résulte ni des dispositions du II de l'article L. 212-10 ni d'aucune autre disposition qu'un SAGE approuvé conformément au I de cet article et constituant dès lors un PAGD cesserait d'être applicable faute d'avoir été complété, dans le délai prévu au II du même article, par l'adoption d'un règlement.<br/>
<br/>
              Sur le pourvoi :<br/>
<br/>
              5. Il ressort des pièces du dossier soumis aux juges du fond que le projet de SAGE du Drac et de la Romanche a été adopté par la commission locale de l'eau le 27 mars 2007 et approuvé par un arrêté interpréfectoral du 13 août 2010, dans le délai prévu par les dispositions du I de l'article L. 212-10 du code l'environnement, mais n'a pas été complété, dans le délai prévu au II de cet article, par un règlement. Conformément à ce qui a été dit au point précédent, il a valeur, à compter de son entrée en vigueur, de PAGD pour les sous-bassins considérés, les décisions administratives prises dans le domaine de l'eau étant donc soumises à une obligation de compatibilité à son égard, ainsi que cela a été dit au point 2. <br/>
<br/>
              6. Pour apprécier cette compatibilité, il appartient au juge administratif de rechercher, dans le cadre d'une analyse globale le conduisant à se placer à l'échelle du territoire pertinent pour apprécier les effets du projet sur la gestion des eaux, si l'autorisation ne contrarie pas les objectifs et les orientations fixés par le schéma, en tenant compte de leur degré de précision, sans rechercher l'adéquation de l'autorisation au regard de chaque orientation ou objectif particulier.<br/>
<br/>
              7. Il ressort des pièces du dossier soumis aux juges du fond que l'objectif n° 8 du SAGE du Drac et de la Romanche prévoit que les secteurs à préserver de nouveaux aménagements hydroélectriques sont définis dans l'attente de l'approbation du SDAGE, son point 1.c. précisant que, en attendant, tout nouvel aménagement hydroélectrique est interdit sur le sous-bassin versant de La Bonne. <br/>
<br/>
              8. Il ressort des énonciations de l'arrêt attaqué que la cour s'est fondée, pour annuler l'arrêté du 6 mai 2013 autorisant l'exploitation d'une centrale hydroélectrique sur le sous-bassin versant de La Bonne, sur la seule interdiction de tout nouvel aménagement énoncée par le point 1.c. de l'objectif n° 8 du SAGE du Drac et de la Romanche. En se fondant sur la non adéquation de l'arrêté litigieux avec un objectif particulier du SAGE et non sur une analyse globale à l'échelle du territoire pertinent et au regard de l'ensemble des objectifs et orientations fixés par le schéma, la cour a entaché son arrêt d'une erreur de droit.<br/>
<br/>
              9. Il résulte de ce qui précède que la société Valhydrau est fondée à demander l'annulation de l'arrêt qu'elle attaque. Le moyen retenu suffisant à entraîner cette annulation, il n'est pas nécessaire de se prononcer sur les autres moyens du pourvoi.<br/>
<br/>
              10. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la société Valhydrau qui n'est pas, dans la présente instance, la partie perdante. En revanche, il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la fédération départementale de pêche et de protection du milieu aquatique de l'Isère et de l'association agréée de pêche et de protection du milieu aquatique du Valbonnais " La Truite de La Bonne " le versement à la société Valhydrau de la somme globale de 3 000 euros au titre des mêmes dispositions.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'arrêt du 29 mai 2018 de la cour administrative d'appel de Lyon est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Lyon.<br/>
Article 3 : La fédération départementale de pêche et de protection du milieu aquatique de l'Isère et l'association agréée de pêche et de protection du milieu aquatique du Valbonnais " La Truite de La Bonne " verseront à la société Valhydrau une somme globale de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions présentées par la fédération départementale de pêche et de protection du milieu aquatique de l'Isère et l'association agréée de pêche et de protection du milieu aquatique du Valbonnais " La Truite de La Bonne " au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à la société Valhydrau, à la fédération départementale de pêche et de protection du milieu aquatique de l'Isère, à l'association agréée de pêche et de protection du milieu aquatique du Valbonnais " La Truite de La Bonne " et à la ministre de la transition écologique et solidaire.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">27-05-05 EAUX. GESTION DE LA RESSOURCE EN EAU. SCHÉMAS DIRECTEURS ET SCHÉMAS D'AMÉNAGEMENT ET DE GESTION DES EAUX. - MODIFICATION DU RÉGIME DES SAGE PAR LA LOI DU 30 DÉCEMBRE 2006 - SAGE APPROUVÉS ANTÉRIEUREMENT - SAGE DEMEURANT APPLICABLES, MÊME S'ILS N'ONT PAS ÉTÉ COMPLÉTÉS PAR UN RÈGLEMENT DANS LE DÉLAI DE SIX ANS PRÉVU À L'ARTICLE L. 212-10 DU CODE DE L'ENVIRONNEMENT.
</SCT>
<ANA ID="9A"> 27-05-05 L'article L. 212-10 du code de l'environnement a pour objet de permettre, dans les conditions et limites qu'il prévoit, que les schémas d'aménagement et de gestion des eaux (SAGE) déjà approuvés ou en cours d'élaboration lors de la promulgation de la loi n° 2006-1772 du 30 décembre 2006 relèvent du régime prévu par cette loi pour les futurs SAGE. Il ne résulte ni du II de l'article L. 212-10 ni d'aucune autre disposition qu'un SAGE approuvé conformément au I de cet article et constituant dès lors un plan d'aménagement et de gestion durable (PAGD) de la ressource en eau et des milieux aquatiques cesserait d'être applicable faute d'avoir été complété par l'adoption d'un règlement, dans le délai de six ans à compter de la promulgation de la loi du 30 décembre 2006 prévu au II du même article.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
