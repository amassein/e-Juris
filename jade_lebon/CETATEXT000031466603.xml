<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031466603</ID>
<ANCIEN_ID>JG_L_2015_11_000000372531</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/46/66/CETATEXT000031466603.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 1ère SSR, 09/11/2015, 372531, Publié au recueil Lebon</TITRE>
<DATE_DEC>2015-11-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>372531</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 1ère SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN ; SCP GASCHIGNARD</AVOCATS>
<RAPPORTEUR>M. Jean-Baptiste de Froment</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier de Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2015:372531.20151109</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...et cinquante autres requérants ont demandé au tribunal administratif de Bastia d'annuler pour excès de pouvoir la délibération du 30 juillet 2009 par laquelle le conseil municipal de Porto-Vecchio a approuvé son plan local d'urbanisme. Par un jugement nos 0900860 et autres numéros du 20 mai 2011, le tribunal administratif a fait droit à leur demande.<br/>
<br/>
              Par un arrêt n° 11MA02797 du 30 juillet 2013, la cour administrative d'appel de Marseille a rejeté l'appel de la commune de Porto-Vecchio formé contre ce jugement. <br/>
<br/>
              Par un pourvoi sommaire et des mémoires complémentaires, enregistrés les 1er octobre 2013, 2 et 13 janvier 2014 au secrétariat du contentieux du Conseil d'Etat, la commune de Porto-Vecchio demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge des parties défenderesses une somme de 12 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              -	la Constitution, notamment son article 62 ;<br/>
              -	le code de l'urbanisme ;<br/>
              -	la loi n° 2002-92 du 22 janvier 2002 ;<br/>
              -	le décret n° 92-129 du 7 février 1992 ;<br/>
              -	la décision du Conseil constitutionnel n° 2000-436 DC du 7 décembre 2000 ;<br/>
              -	le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Baptiste de Froment, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gaschignard, avocat de la commune de Porto-Vecchio et à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de L'EURL " Objectif résidence sud-Corse et autres " ; <br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que par un jugement du 20 mai 2011, le tribunal administratif de Bastia, saisi de trente demandes, a annulé la délibération du 30 juillet 2009 par laquelle le conseil municipal de la commune de Porto-Vecchio a approuvé son plan local d'urbanisme ; que cette commune se pourvoit en cassation contre l'arrêt du 30 juillet 2013 par lequel la cour administrative d'appel de Marseille a rejeté l'appel qu'elle a formé contre ce jugement ;<br/>
<br/>
              En ce qui concerne l'application de l'article L. 121-1 du code de l'urbanisme ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 121-1 du code de l'urbanisme, dans sa rédaction applicable à l'espèce : " Les schémas de cohérence territoriale, les plans locaux d'urbanisme et les cartes communales déterminent les conditions permettant d'assurer :  / 1° L'équilibre entre le renouvellement urbain, un développement urbain maîtrisé, le développement de l'espace rural, d'une part, et la préservation des espaces affectés aux activités agricoles et forestières et la protection des espaces naturels et des paysages, d'autre part, en respectant les objectifs du développement durable (...)  " ; que, par sa décision n° 2000-436 DC du 7 décembre 2000, le Conseil constitutionnel a jugé que ces dispositions n'étaient pas contraires aux articles 34 et 72 de la Constitution sous réserve qu'elles soient interprétées comme imposant seulement aux auteurs des documents d'urbanisme d'y faire figurer des mesures tendant à la réalisation des objectifs qu'elles énoncent et que, en conséquence, le juge administratif exerce un simple contrôle de compatibilité entre les règles fixées par ces documents et les dispositions précitées de l'article L. 121-1 du code l'urbanisme ; qu'en jugeant que le déséquilibre que le plan local d'urbanisme de la commune, notamment en raison de la très forte concentration urbaine qu'il autorisait sur tout le littoral de Porto-Vecchio, instaurait " entre le développement urbain de la commune et la préservation des espaces naturels et des paysages " était d'une ampleur telle que ce plan portait atteinte aux exigences résultant des dispositions de l'article L. 121-1 du code de l'urbanisme, la cour s'est livrée à un contrôle de la compatibilité du plan local d'urbanisme aux dispositions de cet article L. 121-1 ; qu'ainsi, elle n'a pas commis d'erreur de droit ;  <br/>
<br/>
              En ce qui concerne l'application des dispositions du code de l'urbanisme particulières au littoral : <br/>
<br/>
              3. Considérant que le premier alinéa de l'article L. 111-1-1 du code de l'urbanisme, dans sa rédaction alors en vigueur, dispose que des directives territoriales d'aménagement peuvent préciser, sur les parties du territoire qu'elles couvrent, " les modalités d'application (...) adaptées aux particularités géographiques locales " des dispositions particulières au littoral figurant aux articles L. 146-1 et suivants du code de l'urbanisme ; que ces dispositions sont reprises au quatrième alinéa de l'article L. 146-1 du code de l'urbanisme, selon lequel " les directives territoriales d'aménagement prévues à l'article L. 111-1-1 peuvent préciser les modalités d'application du présent chapitre (...) " ; qu'en vertu du dernier alinéa de l'article L. 111-1-1, dans sa rédaction alors en vigueur, " Les plans locaux d'urbanisme (...) doivent être compatibles avec les orientations des schémas de cohérence territoriale et des schémas de secteur. En l'absence de ces schémas, ils doivent être compatibles avec les directives territoriales d'aménagement (...). En l'absence de ces documents, ils doivent être compatibles avec les dispositions particulières aux zones de montagne et au littoral des articles L. 145-1 et suivants et L. 146-1 et suivants " ; qu'il résulte de la combinaison de ces dispositions que les auteurs des plans locaux d'urbanisme doivent s'assurer que les partis d'urbanisme présidant à l'élaboration de ces documents sont compatibles avec les directives territoriales d'aménagement ou, en leur absence, avec les dispositions du code de l'urbanisme particulières, notamment, au littoral ; qu'en outre, en l'absence de document local d'urbanisme légalement applicable, il appartient à l'autorité administrative chargée de se prononcer sur une demande d'autorisation d'occupation ou d'utilisation du sol mentionnée au dernier alinéa de l'article L. 146-1 du code de l'urbanisme, de s'assurer, sous le contrôle du juge de l'excès de pouvoir, de la conformité du projet soit, lorsque le territoire de la commune est couvert par une directive territoriale d'aménagement ou par un document en tenant lieu, avec les éventuelles prescriptions édictées par ce document d'urbanisme, sous réserve que les dispositions qu'il comporte sur les modalités d'application des dispositions des articles L. 146-1 et suivants du code de l'urbanisme soient, d'une part, suffisamment précises et, d'autre part, compatibles avec ces mêmes dispositions, soit, dans le cas contraire, avec les dispositions du code de l'urbanisme particulières au littoral ; <br/>
<br/>
              Sur l'application par la cour du I de l'article L. 146-4 du code de l'urbanisme :<br/>
<br/>
              4. Considérant qu'en vertu des dispositions du I de l'article L. 146-4 du code de l'urbanisme, l'extension de l'urbanisation doit se réaliser, dans les communes littorales, soit en continuité avec les agglomérations et villages existants, soit en hameaux nouveaux intégrés à l'environnement ; qu'il résulte de ces dispositions que les constructions peuvent être autorisées dans les communes littorales en continuité avec les agglomérations et villages existants, c'est-à-dire avec les zones déjà urbanisées caractérisées par un nombre et une densité significatifs de constructions, mais que, en revanche, aucune construction ne peut être autorisée, même en continuité avec d'autres, dans les zones d'urbanisation diffuse éloignées de ces agglomérations et villages ;<br/>
<br/>
              5. Considérant cependant que le schéma d'aménagement de la Corse, approuvé par décret en Conseil d'Etat du 7 février 1992, demeuré applicable à la date du document d'urbanisme attaqué et jusqu'à l'entrée en vigueur du plan d'aménagement et de développement durable de la Corse, ainsi que le prévoit l'article 13 de la loi du 22 janvier 2002 relative à la Corse, a valeur de schéma de mise en valeur de la mer en vertu de l'article L. 144-2 du code de l'urbanisme ; qu'il prescrit que l'urbanisation du littoral demeure limitée ; que, pour en prévenir la dispersion, il privilégie la densification des zones urbaines existantes et la structuration des " espaces péri-urbains ", en prévoyant, d'une part, que les extensions, lorsqu'elles sont nécessaires, s'opèrent dans la continuité des " centres urbains existants ", d'autre part, que les hameaux nouveaux demeurent l'exception ; que de telles prescriptions apportent des précisions relatives aux modalités d'application des dispositions du I de l'article L. 146-4 du code l'urbanisme et sont compatibles avec elles ; que la compatibilité du plan local d'urbanisme de Porto-Vecchio doit ainsi s'apprécier au regard des dispositions du schéma d'aménagement de la Corse ; <br/>
<br/>
              6. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que les zones UH et AUH du règlement du PLU correspondent toutes à l'urbanisation de hameaux traditionnels, dont certains, comme Muratello, Ceccia et Precojo sont de taille relativement importante et assez densément urbanisés ; que toutefois, en jugeant que ces hameaux ne sauraient être regardés comme des " centres urbains " et que le classement en zone UH des parcelles situées en continuité de ces hameaux n'était pas compatible avec les dispositions du schéma d'aménagement de la Corse, la cour n'a pas entaché son arrêt d'insuffisance de motivation et s'est livrée à une appréciation souveraine des pièces du dossier qui n'est pas entachée de dénaturation ;<br/>
<br/>
              7. Considérant que c'est également par une motivation suffisante et au terme d'une appréciation souveraine des pièces du dossier exempte de dénaturation, que la cour a estimé que le classement en zone constructible des terrains se trouvant soit à proximité d'espaces naturels, soit au sein de zones d'urbanisation diffuse, n'était pas compatible avec les dispositions du schéma d'aménagement de la Corse ;<br/>
<br/>
              Sur l'application par la cour du III de l'article L. 146-4 du code de l'urbanisme :<br/>
<br/>
              8. Considérant qu'il résulte des dispositions du III de l'article L. 146-4 du code de l'urbanisme qu'en dehors des espaces urbanisés, les constructions ou installations sont interdites sur une bande littorale de cent mètres à compter de la limite haute du rivage ; que ces dispositions sont seules applicables sur le territoire de la commune de Porto-Vecchio, dès lors que le schéma d'aménagement de la Corse n'y apporte aucune précision ni aucun complément ;<br/>
<br/>
              9. Considérant qu'en jugeant que, s'agissant des zones de Cala d'Oro et Pieteraggio, la seule présence d'un lotissement ne permettait pas de regarder celles-ci comme constituant des " espaces urbanisés " au sens de ces dispositions et que les secteurs de Faciasata, Punta d'Oro, Cala d'Oro et Santa Giulia ne devaient pas être regardés comme des " espaces urbanisés ", la cour n'a commis aucune erreur de droit et s'est livrée à une appréciation souveraine des pièces du dossier qui n'est pas entachée de dénaturation ;<br/>
<br/>
              Sur l'application par la cour du I de l'article L. 145-3 du code de l'urbanisme :  <br/>
<br/>
              10. Considérant qu'aux termes du I de l'article L. 145-3 du code de l'urbanisme : " Les terres nécessaires au maintien et au développement des activités agricoles, pastorales et forestières sont préservées. La nécessité de préserver ces terres s'apprécie au regard de leur rôle et de leur place dans les systèmes d'exploitation locaux. Sont également pris en compte leur situation par rapport au siège de l'exploitation, leur relief, leur pente et leur exposition. Les constructions nécessaires à ces activités ainsi que les équipements sportifs liés notamment à la pratique du ski et de la randonnée peuvent y être autorisés. Peuvent être également autorisées, par arrêté préfectoral, après avis de la commission départementale de la préservation des espaces naturels, agricoles et forestiers et de la commission départementale compétente en matière de nature, de paysages et de sites, dans un objectif de protection et de mise en valeur du patrimoine montagnard, la restauration ou la reconstruction d'anciens chalets d'alpage ou de bâtiments d'estive, ainsi que les extensions limitées de chalets d'alpage ou de bâtiments d'estive existants lorsque la destination est liée à une activité professionnelle saisonnière " ; que ces dispositions sont seules applicables sur le territoire de la commune de Porto-Vecchio, dès lors que le schéma d'aménagement de la Corse n'y apporte aucune précision ni aucun complément ;<br/>
<br/>
              11. Considérant que si les prescriptions de l'article L. 145-3 du code de l'urbanisme ne sauraient être regardées comme interdisant de classer, dans un plan local d'urbanisme, des terres agricoles dans des zones réservées à des activités économiques autres que l'agriculture ou l'habitat, elles impliquent de n'admettre l'urbanisation de ces terres que pour satisfaire des besoins justifiés et dans une mesure compatible avec le maintien et le développement des activités agricoles, pastorales et forestières ; que pour annuler, pour un motif d'incompatibilité avec les dispositions mentionnées ci-dessus, le classement par le plan local d'urbanisme attaqué en terres non agricoles des zones UE, UEi, UF, AUDa, AUDAi et AU1R, situées dans le secteur de Brellinga, des zones AUD, AUDi, UF, AUL1 du secteur d'Arutoli, de la zone AUD et d'une partie de la zone Uda situées à Campiccicoli, de la zone AU1L de Riolo Ponte et des zones AU1R et UL de Georges Ville, la cour a relevé qu'elles recouvrent des terres caractérisées par leurs potentialités agricoles, séparées des zones urbanisées, dont la majorité d'entre elles est d'ailleurs exploitée, notamment pour l'élevage bovin allaitant ; que ce faisant, la cour n'a entaché son arrêt ni d'insuffisance de motivation, ni de dénaturation des pièces du dossier ; <br/>
<br/>
              12. Considérant que la cour a également  jugé que le classement en zone Ng d'une vaste étendue naturelle, en partie utilisée pour l'élevage ovin, tel que prévu dans le cadre du projet d'extension du golf de Lezza dans le sud du hameau de Precojo, compromettait le maintien et le développement des activités agricoles, pastorales et forestières, protégés par l'article L. 145-3 ; que cette appréciation relève du pouvoir souverain des juges du fond et n'est sur ce point, en dépit de l'intérêt de l'opération pour le rayonnement touristique de la commune, entachée d'aucune dénaturation ; <br/>
<br/>
              13. Considérant qu'il résulte de tout ce qui précède que la commune de Porto-Vecchio  n'est pas fondée à demander l'annulation de l'arrêt attaqué ;<br/>
<br/>
              14. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de Porto-Vecchio le versement d'une somme de 1 000 euros au titre de l'article L. 761-1 du code de justice administrative, respectivement à la copropriété de la résidence " Santa Giulia Palace ", à la SCI " Bocca di l'Oro " et à l'EURL " Objectif résidence sud-Corse "; qu'en revanche, ces mêmes dispositions font obstacle à ce que le versement de la somme demandée par la commune de Porto-Vecchio soit mis à la charge de ces dernières, qui ne sont pas, dans la présente instance, les parties perdantes ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la commune de Porto-Vecchio est rejeté. <br/>
<br/>
Article 2 : La commune de Porto-Vecchio versera à la copropriété de la résidence " Santa Giulia Palace ", à la SCI " Bocca di l'Oro " et à l'EURL " Objectif résidence sud-Corse " chacune une somme de 1 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée à la commune de Porto-Vecchio, à la copropriété de la résidence " Santa Giulia Palace ", à la SCI " Bocca di l'Oro " et à l'EURL " Objectif résidence sud-Corse ".<br/>
Une copie en sera adressée pour information à M. B...A...et aux quarante sept autres défendeurs.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">68-001-01-02-03 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. RÈGLES GÉNÉRALES D'UTILISATION DU SOL. RÈGLES GÉNÉRALES DE L'URBANISME. PRESCRIPTIONS D'AMÉNAGEMENT ET D'URBANISME. RÉGIME ISSU DE LA LOI DU 3 JANVIER 1986 SUR LE LITTORAL. - 1) OBLIGATION DE COMPATIBILITÉ DES PLU, EN L'ABSENCE DE DIRECTIVES TERRITORIALES D'AMÉNAGEMENT - 2) ARTICULATION DES NORMES AUXQUELLES UNE AUTORISATION D'URBANISME DOIT ÊTRE CONFORME [RJ1] - 3) I DE L'ARTICLE L. 146-4 DU CODE DE L'URBANISME - PORTÉE [RJ2].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">68-001-01-035 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. RÈGLES GÉNÉRALES D'UTILISATION DU SOL. RÈGLES GÉNÉRALES DE L'URBANISME. DIRECTIVES TERRITORIALES D'AMÉNAGEMENT. - 1) COMPATIBILITÉ DES PLU AVEC LES DTA OU, EN LEUR ABSENCE, AVEC LES DISPOSITIONS PARTICULIÈRES DU CODE DE L'URBANISME - 2) ARTICULATION DES NORMES AUXQUELLES UNE AUTORISATION D'URBANISME DOIT ÊTRE CONFORME [RJ1].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">68-01-002 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PLANS D'AMÉNAGEMENT ET D'URBANISME. DISPOSITIONS COMMUNES À DIFFÉRENTS DOCUMENTS D'URBANISME. - ARTICLE L. 121-1 DU CODE DE L'URBANISME - PORTÉE [RJ3].
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">68-01-01-01-03 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PLANS D'AMÉNAGEMENT ET D'URBANISME. PLANS D`OCCUPATION DES SOLS (POS) ET PLANS LOCAUX D'URBANISME (PLU). LÉGALITÉ DES PLANS. LÉGALITÉ INTERNE. - COMPATIBILITÉ DES PLU AVEC LES DTA OU, EN LEUR ABSENCE, AVEC LES DISPOSITIONS PARTICULIÈRES DU CODE DE L'URBANISME.
</SCT>
<ANA ID="9A"> 68-001-01-02-03 1) Il résulte de la combinaison des articles L. 111-1-1 et L. 146-1 du code de l'urbanisme que les auteurs des plans locaux d'urbanisme (PLU) doivent s'assurer que les partis d'urbanisme présidant à l'élaboration de ces documents sont compatibles avec les directives territoriales d'aménagement (DTA) ou, en leur absence, avec les dispositions particulières, notamment, au littoral.,,,2) En l'absence de document local d'urbanisme légalement applicable, il appartient à l'autorité administrative chargée de se prononcer sur une demande d'autorisation d'occupation ou d'utilisation du sol mentionnée au dernier alinéa de l'article L. 146-1 du code de l'urbanisme de s'assurer, sous le contrôle du juge de l'excès de pouvoir, de la conformité du projet soit, lorsque le territoire de la commune est couvert par une directive territoriale d'aménagement ou par un document en tenant lieu, avec les éventuelles prescriptions édictées par ce document d'urbanisme, sous réserve que les dispositions qu'il comporte sur les modalités d'application des articles L. 146-1 et suivants du code de l'urbanisme soient, d'une part, suffisamment précises et, d'autre part, compatibles avec ces mêmes articles, soit, dans le cas contraire, avec les dispositions du code de l'urbanisme particulières au littoral.,,,3) Il résulte du I de l'article L. 146-4 du code de l'urbanisme que les constructions peuvent être autorisées dans les communes littorales en continuité avec les agglomérations et villages existants, c'est-à-dire avec les zones déjà urbanisées caractérisées par un nombre et une densité significatifs de constructions, mais que, en revanche, aucune construction ne peut être autorisée, même en continuité avec d'autres, dans les zones d'urbanisation diffuse éloignées de ces agglomérations et villages.</ANA>
<ANA ID="9B"> 68-001-01-035 1) Il résulte de la combinaison des articles L. 111-1-1 et L. 146-1 du code de l'urbanisme que les auteurs des plans locaux d'urbanisme (PLU) doivent s'assurer que les partis d'urbanisme présidant à l'élaboration de ces documents sont compatibles avec les directives territoriales d'aménagement (DTA) ou, en leur absence, avec les dispositions particulières, notamment, au littoral.,,,2) En l'absence de document local d'urbanisme légalement applicable, il appartient à l'autorité administrative chargée de se prononcer sur une demande d'autorisation d'occupation ou d'utilisation du sol mentionnée au dernier alinéa de l'article L. 146-1 du code de l'urbanisme de s'assurer, sous le contrôle du juge de l'excès de pouvoir, de la conformité du projet soit, lorsque le territoire de la commune est couvert par une directive territoriale d'aménagement ou par un document en tenant lieu, avec les éventuelles prescriptions édictées par ce document d'urbanisme, sous réserve que les dispositions qu'il comporte sur les modalités d'application des articles L. 146-1 et suivants du code de l'urbanisme soient, d'une part, suffisamment précises et, d'autre part, compatibles avec ces mêmes articles, soit, dans le cas contraire, avec les dispositions du code de l'urbanisme particulières au littoral.</ANA>
<ANA ID="9C"> 68-01-002 En application de la décision n° 2000-436 DC du Conseil constitutionnel du 7 décembre 2000, les dispositions de l'article L. 121-1 du code de l'urbanisme n'imposent aux auteurs des documents d'urbanisme qu'elles mentionnent que d'y faire figurer des mesures tendant à la réalisation des objectifs qu'elles énoncent.,,,En conséquence, et en application de la même décision, le juge administratif exerce un simple contrôle de compatibilité entre les règles fixées par ces documents et les dispositions de l'article L. 121-1 du code de l'urbanisme.</ANA>
<ANA ID="9D"> 68-01-01-01-03 Il résulte de la combinaison des articles L. 111-1-1 et L. 146-1 du code de l'urbanisme que les auteurs des plans locaux d'urbanisme (PLU) doivent s'assurer que les partis d'urbanisme présidant à l'élaboration de ces documents sont compatibles avec les directives territoriales d'aménagement (DTA) ou, en leur absence, avec les dispositions particulières, notamment, au littoral.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Cf. CE, Section, 16 juillet 2010, Ministre d'Etat, ministre de l'écologie, de l'énergie, du développement durable et de la mer, en charge des technologies vertes et des négociations sur le climat c/ société Les Casuccie, n° 313768, p. 317. Solution abandonnée, en ce qu'elle implique un a contrario dans l'hypothèse où un document local d'urbanisme est légalement applicable, par CE, Section, 31 mars 2017, SARL Savoie Lac Investissements, n° 392186, A.,,[RJ2]Cf. CE, 27 septembre 2006, Commune du Lavandou, n° 275924, aux Tables sur un autre point.,,[RJ3]Cf. CE, 15 mai 2013, Commune de Gurmençon, n° 340554, aux Tables sur un autre point.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
