<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028770812</ID>
<ANCIEN_ID>JG_L_2014_03_000000356142</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/77/08/CETATEXT000028770812.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère / 6ème SSR, 24/03/2014, 356142</TITRE>
<DATE_DEC>2014-03-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>356142</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère / 6ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN ; SCP FABIANI, LUC-THALER</AVOCATS>
<RAPPORTEUR>M. Pascal Trouilly</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:356142.20140324</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 25 janvier et 26 avril 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour la commune du Luc-en-Provence, représentée par son maire ; la commune du Luc-en-Provence demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'arrêt n° 10MA00065 du 24 novembre 2011 par lequel la cour administrative d'appel de Marseille a, à la demande de M. B...A..., d'une part, annulé le jugement n° 0702285 du 7 décembre 2009 par lequel le tribunal administratif de Toulon a rejeté la demande de celui-ci tendant à l'annulation pour excès de pouvoir de la décision du 12 février 2007 par laquelle son maire a retiré l'arrêté du 20 octobre 2006 lui accordant un permis de construire et, d'autre part, annulé pour excès de pouvoir cette décision ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de M.A... ; <br/>
<br/>
              3°) de mettre à la charge de M. A...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'urbanisme ; <br/>
<br/>
              Vu la loi n° 2000-321 du 12 avril 2000 ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Pascal Trouilly, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la commune du Luc-en-Provence et à la SCP Fabiani, Luc-Thaler, avocat de M.A... ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que le maire de la commune du Luc-en-Provence a délivré le 20 octobre 2006 à M. A...un permis de construire en vue de l'édification d'une maison individuelle ; que le préfet du Var a demandé au maire de procéder au retrait de cet arrêté, au motif que le plan de prévention des risques d'inondation en cours d'élaboration classait le terrain d'assiette en zone de fort aléa, ce qui le rendait inconstructible ; que le maire a retiré le permis de construire le 12 février 2007, en se fondant sur les dispositions de l'article R. 111-2 du code de l'urbanisme ; que, par un arrêt du 24 novembre 2011, la cour administrative d'appel de Marseille a annulé le jugement du 6 novembre 2009 du tribunal administratif de Toulon et la décision de retrait du maire, en estimant que les dispositions de l'article 24 de la loi du 12 avril 2000 relative aux droits des citoyens dans leurs relations avec les administrations avaient été méconnues ; que la commune du Luc-en-Provence se pourvoit en cassation contre cet arrêt ;<br/>
<br/>
              2. Considérant qu'aux termes du premier alinéa de l'article 24 de la loi du 12 avril 2000 : " Exception faite des cas où il est statué sur une demande, les décisions individuelles qui doivent être motivées en application des articles 1er et 2 de la loi n° 79-587 du 11 juillet 1979 relative à la motivation des actes administratifs et à l'amélioration des relations entre l'administration et le public n'interviennent qu'après que la personne intéressée a été mise à même de présenter des observations écrites et, le cas échéant, sur sa demande, des observations orales (...) " ; qu'il résulte de ces dispositions qu'il appartient à l'autorité administrative compétente pour adopter une décision individuelle entrant dans leur champ de mettre elle-même la personne intéressée en mesure de présenter des observations ; que dans l'hypothèse où un maire envisage de retirer un permis de construire, la notification au bénéficiaire de ce permis d'un recours administratif formé par un tiers ou par le préfet agissant dans le cadre du contrôle de légalité contre ce permis ne saurait tenir lieu du respect, par le maire, de la procédure prévue par les dispositions précitées ;<br/>
<br/>
              3. Considérant que la cour a relevé que si M. A...avait été destinataire du recours gracieux formé par le sous-préfet de Draguignan, qui lui avait été notifié en application des dispositions de l'article R. 600-1 du code de l'urbanisme, et s'il avait présenté à la commune des observations répondant à ce recours, le maire de la commune ne l'avait pas lui-même informé de son intention de procéder au retrait du permis de construire et ne l'avait pas mis à même de présenter ses observations sur son projet ; qu'elle a pu, sans erreur de droit, en déduire que la décision de retrait du maire avait été prise à l'issue d'une procédure irrégulière ; <br/>
<br/>
              4. Considérant, toutefois, qu'un vice affectant le déroulement d'une procédure administrative préalable n'est de nature à entacher d'illégalité la décision prise que s'il ressort des pièces du dossier qu'il a été susceptible d'exercer, en l'espèce, une influence sur le sens de la décision prise ou qu'il a privé les intéressés d'une garantie ; que le respect, par l'autorité administrative compétente, de la procédure prévue par les dispositions de l'article 24 de la loi du 12 avril 2000, constitue une garantie pour le titulaire du permis que le maire envisage de retirer ; que la décision de retrait prise par le maire est ainsi illégale s'il ressort de l'ensemble des circonstances de l'espèce que le titulaire du permis a été effectivement privé de cette garantie ;<br/>
<br/>
              5. Considérant que la cour a jugé que la méconnaissance des dispositions de l'article 24 de la loi du 12 avril 2000 par le maire du Luc-en-Provence entachait d'illégalité la décision de retrait, sans rechercher si, dans les circonstances particulières de l'espèce qui lui était soumise, et compte tenu, en particulier, des observations que M. A...avait adressées à la commune et qui, ainsi qu'il ressortait des pièces du dossier qui lui était soumis, portaient  notamment sur le motif même qui a conduit le maire à procéder au retrait du permis de construire, l'intéressé avait été effectivement privé de la garantie prévue par la loi ; qu'elle a ainsi commis une erreur de droit ; que son arrêt doit, par suite, être annulé ; <br/>
<br/>
              6. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de M. A...le versement à la commune du Luc-en-Provence de la somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative ; que ces dispositions font en revanche obstacle à ce qu'il soit fait droit aux conclusions présentées, au même titre, par M. A... ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 24 novembre 2011 de la cour administrative d'appel de Marseille est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Marseille.<br/>
Article 3 : M. A...versera à la commune du Luc-en-Provence la somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions présentées par M. A...au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à la commune du Luc-en-Provence et à M. B...A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-03-03-03 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - FORME ET PROCÉDURE. PROCÉDURE CONTRADICTOIRE. MODALITÉS. - PROCÉDURE PRÉVUE À L'ARTICLE 24 DE LA LOI DU 12 AVRIL 2000 - 1) OBLIGATION POUR L'AUTORITÉ COMPÉTENTE DE METTRE ELLE-MÊME LA PERSONNE INTÉRESSÉE EN MESURE DE PRÉSENTER SES OBSERVATIONS - EXISTENCE - NOTION - NOTIFICATION AU BÉNÉFICIAIRE D'UN PERMIS DE CONSTRUIRE D'UN RECOURS ADMINISTRATIF FORMÉ PAR UN TIERS OU LE PRÉFET - EXCLUSION - 2) GARANTIE AU SENS DE LA JURISPRUDENCE  DANTHONY  [RJ1] - EXISTENCE - CONSÉQUENCES.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">68-03-04-05 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PERMIS DE CONSTRUIRE. RÉGIME D'UTILISATION DU PERMIS. RETRAIT DU PERMIS. - PROCÉDURE CONTRADICTOIRE PRÉALABLE PRÉVUE À L'ARTICLE 24 DE LA LOI DU 12 AVRIL 2000 - 1) OBLIGATION POUR L'AUTORITÉ COMPÉTENTE DE METTRE ELLE-MÊME LA PERSONNE INTÉRESSÉE EN MESURE DE PRÉSENTER SES OBSERVATIONS - EXISTENCE - NOTION - NOTIFICATION AU BÉNÉFICIAIRE D'UN PERMIS DE CONSTRUIRE D'UN RECOURS ADMINISTRATIF FORMÉ PAR UN TIERS OU LE PRÉFET - EXCLUSION - 2) GARANTIE AU SENS DE LA JURISPRUDENCE  DANTHONY  [RJ1] - EXISTENCE - CONSÉQUENCES.
</SCT>
<ANA ID="9A"> 01-03-03-03 1) Il résulte de l'article 24 de la loi n° 2000-321 du 12 avril 2000 qu'il appartient à l'autorité administrative compétente pour adopter une décision individuelle entrant dans son champ de mettre elle-même la personne intéressée en mesure de présenter des observations. Dans l'hypothèse où l'autorité administrative envisage de retirer un permis de construire, la notification au bénéficiaire de ce permis d'un recours administratif formé par un tiers ou par le préfet agissant dans le cadre du contrôle de légalité contre ce permis ne saurait tenir lieu du respect, par l'autorité administrative, de cette procédure contradictoire.,,,2) Le respect, par l'autorité administrative compétente, de la procédure contradictoire prévue par les dispositions de l'article 24 de la loi du 12 avril 2000, constitue une garantie pour le titulaire du permis qu'elle entend retirer. La décision de retrait est illégale s'il ressort de l'ensemble des circonstances de l'espèce que le bénéficiaire a été effectivement privé de cette garantie.</ANA>
<ANA ID="9B"> 68-03-04-05 1) Il résulte de l'article 24 de la loi n° 2000-321 du 12 avril 2000 qu'il appartient à l'autorité administrative compétente pour adopter une décision individuelle entrant dans son champ de mettre elle-même la personne intéressée en mesure de présenter des observations. Dans l'hypothèse où l'autorité administrative envisage de retirer un permis de construire, la notification au bénéficiaire de ce permis d'un recours administratif formé par un tiers ou par le préfet agissant dans le cadre du contrôle de légalité contre ce permis ne saurait tenir lieu du respect, par l'autorité administrative, de cette procédure contradictoire.,,,2) Le respect, par l'autorité administrative compétente, de la procédure contradictoire prévue par les dispositions de l'article 24 de la loi du 12 avril 2000, constitue une garantie pour le titulaire du permis qu'elle entend retirer. La décision de retrait est illégale s'il ressort de l'ensemble des circonstances de l'espèce que le bénéficiaire a été effectivement privé de cette garantie.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Assemblée, 23 décembre 2011, Danthony et autres, n° 335033, p. 649.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
