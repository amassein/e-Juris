<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028528263</ID>
<ANCIEN_ID>JG_L_2014_01_000000374163</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/52/82/CETATEXT000028528263.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème / 5ème SSR, 24/01/2014, 374163, Publié au recueil Lebon</TITRE>
<DATE_DEC>2014-01-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>374163</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème / 5ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROGER, SEVAUX, MATHONNET</AVOCATS>
<RAPPORTEUR>M. Louis Dutheillet de Lamothe</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Gaëlle Dumortier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:374163.20140124</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu l'ordonnance n° 1312059 du 20 décembre 2013 par laquelle le président du tribunal administratif de Montreuil a transmis au président de la section du contentieux du Conseil d'État, en application de l'article R. 351-3 du code de justice administrative, la demande du comité d'entreprise de la société Ricoh France, dont le siège est situé parc Tertiaire SILIC, 9, avenue Robert Schuman à Rungis, représenté par son secrétaire ; <br/>
<br/>
              Vu la demande, enregistrée au greffe du tribunal administratif de Montreuil le 11 décembre 2013, présentée par le comité d'entreprise de la société Ricoh France et tendant, d'une part, à l'annulation pour excès de pouvoir de la décision du 6 décembre 2013 homologuant l'acte unilatéral présenté par la société Ricoh France en application de l'article L. 1233-24-4 du code du travail en vue du licenciement collectif de certains salariés, d'autre part, à ce qu'il soit enjoint au directeur régional des entreprises, de la concurrence, du travail et de l'emploi d'Ile-de-France de solliciter de la société Ricoh France la reprise de la procédure d'information et de consultation et de présenter un nouvel acte unilatéral ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu le code du travail ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Louis Dutheillet de Lamothe, Auditeur, <br/>
<br/>
              - les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Roger, Sevaux, Mathonnet, avocat du comité d'entreprise de la société Ricoh France ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que l'article R. 351-3 du code de justice administrative dispose : " Lorsqu'une cour administrative d'appel ou un tribunal administratif est saisi de conclusions qu'il estime relever de la compétence d'une juridiction administrative autre que le Conseil d'Etat, son président, ou le magistrat qu'il délègue, transmet sans délai le dossier à la juridiction qu'il estime compétente. / Toutefois, en cas de difficultés particulières, il peut transmettre sans délai le dossier au président de la section du contentieux du Conseil d'Etat qui règle la question de compétence et attribue le jugement de tout ou partie de l'affaire à la juridiction qu'il déclare compétente. " ; <br/>
<br/>
              2. Considérant, d'une part, qu'aux termes du premier alinéa de l'article R. 312-10 du code de justice administrative : " Les litiges relatifs aux législations régissant les activités professionnelles, notamment les professions libérales, les activités agricoles, commerciales et industrielles, la réglementation des prix, la réglementation du travail, ainsi que la protection ou la représentation des salariés, ceux concernant les sanctions administratives intervenues en application de ces législations relèvent, lorsque la décision attaquée n'a pas un caractère réglementaire, de la compétence du tribunal administratif dans le ressort duquel se trouve soit l'établissement ou l'exploitation dont l'activité est à l'origine du litige, soit le lieu d'exercice de la profession. " ; <br/>
<br/>
              3. Considérant, d'autre part, qu'en vertu des dispositions des articles L. 1233-24-1 à L. 1233-24-4 du code du travail, le licenciement économique d'au moins dix salariés pendant une même période de trente jours dans une entreprise d'au moins cinquante salariés ne peut intervenir qu'après la conclusion d'un accord collectif portant sur le contenu du plan de sauvegarde de l'emploi, sur les modalités d'information et de consultation du comité d'entreprise, sur le nombre, l'ordre et le calendrier des licenciements et sur les mesures de formation, d'adaptation et de reclassement à mettre en oeuvre, ou, à défaut d'accord collectif portant sur l'ensemble de ces éléments, après l'élaboration par l'employeur d'un document contenant les mêmes informations ; que l'article L. 1233-57-1 du code du travail dispose que cet accord collectif ou ce document de l'employeur est transmis à l'autorité administrative, qui valide l'accord ou homologue le document de l'employeur s'il respecte les dispositions législatives, réglementaires et conventionnelles applicables ; <br/>
<br/>
              4. Considérant que les décisions de validation ou d'homologation mentionnées à l'article L. 1233-57-1 du code du travail, qui n'ont pas un caractère réglementaire, sont relatives à l'application de la réglementation du travail et doivent, par suite, être contestées devant le tribunal administratif compétent, déterminé conformément à la règle édictée par  l'article R. 312-10 du code de justice administrative ; qu'en application de cet article, lorsque l'accord collectif ou le document de l'employeur relatif au projet de licenciement collectif en cause identifie le ou les établissements auxquels sont rattachés les emplois dont la suppression est envisagée et que ces établissements sont situés dans le ressort d'un même tribunal administratif, ce tribunal est compétent pour connaître d'un recours pour excès de pouvoir dirigé contre la décision administrative validant l'accord collectif ou homologuant le document de l'employeur; que, dans tous les autres cas, il y a lieu d'estimer que " l'établissement (...) à l'origine du litige " au sens de l'article R. 312-10 du code de justice administrative est l'entreprise elle-même et que le tribunal compétent est celui dans le ressort duquel se trouve le siège de cette entreprise ; <br/>
<br/>
              5. Considérant qu'en l'espèce, la requête du comité d'entreprise de la société Ricoh France, dont le siège est situé à Rungis, tend à l'annulation de la décision du 6 décembre 2013 par laquelle le directeur régional des entreprises, de la concurrence, du travail et de l'emploi d'Ile-de-France a homologué le document présenté par l'employeur relatif à un projet de licenciement économique ; que, si le document de l'employeur mentionne les sites auxquels sont rattachés les emplois supprimés, il n'identifie pas les établissements concernés ; que, par suite, le tribunal administratif compétent pour statuer sur les conclusions d'annulation pour excès de pouvoir et sur les conclusions accessoires de la demande est le tribunal administratif de Melun, dans le ressort duquel se trouve la commune de Rungis, lieu du siège de l'entreprise ; que le jugement de la demande du comité d'entreprise de la société Ricoh France doit, dès lors, être attribué à ce tribunal, auquel il incombe, en vertu de l'article L. 1235-7-1 du code du travail, de statuer dans un délai de trois mois à compter de la réception du dossier ; <br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				---------------<br/>
Article 1er : Le jugement de la demande du comité d'entreprise de la société Ricoh France est attribué au tribunal administratif de Melun. <br/>
<br/>
Article 2 : La présente décision sera notifiée au comité d'entreprise de la société Ricoh France, à la société Ricoh France et au ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">17-05-01-02 COMPÉTENCE. COMPÉTENCE À L'INTÉRIEUR DE LA JURIDICTION ADMINISTRATIVE. COMPÉTENCE EN PREMIER RESSORT DES TRIBUNAUX ADMINISTRATIFS. COMPÉTENCE TERRITORIALE. - TRIBUNAL ADMINISTRATIF COMPÉTENT POUR CONNAÎTRE DU RECOURS DIRIGÉ CONTRE UNE DÉCISION DE VALIDATION OU D'HOMOLOGATION DE L'ACCORD COLLECTIF OU DU DOCUMENT DE L'EMPLOYEUR PRÉVU EN CAS DE LICENCIEMENT ÉCONOMIQUE (ART. L. 1233-24-1 ET L. 1233-24-4 DU CODE DU TRAVAIL) - 1) DÉTERMINATION - APPLICABILITÉ DE L'ARTICLE R. 312-10 DU CJA - EXISTENCE - CONSÉQUENCE - A) CAS OÙ L'ACCORD OU LE DOCUMENT IDENTIFIE LES ÉTABLISSEMENTS CONCERNÉS ET OÙ CES ÉTABLISSEMENTS SE TROUVENT DANS LE RESSORT D'UN SEUL TRIBUNAL - TRIBUNAL COMPÉTENT POUR CE RESSORT - B) AUTRES CAS - TRIBUNAL DU SIÈGE DE L'ENTREPRISE - C) ESPÈCE - DOCUMENT DE L'EMPLOYEUR MENTIONNANT DES SITES SANS IDENTIFIER D'ÉTABLISSEMENT - COMPÉTENCE DU TRIBUNAL DU SIÈGE - 2) POINT DE DÉPART DU DÉLAI DE TROIS MOIS, IMPARTI AU TRIBUNAL ADMINISTRATIF POUR STATUER À PEINE DE DESSAISISSEMENT, EN CAS DE TRANSMISSION DU DOSSIER PAR UN AUTRE TRIBUNAL QUI S'EST ESTIMÉ INCOMPÉTENT POUR EN CONNAÎTRE (ART. R. 351-3 DU CJA) - DATE DE RÉCEPTION DU DOSSIER PAR LE SECOND TRIBUNAL.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-07-01-08 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. QUESTIONS GÉNÉRALES. RENVOI DE CONCLUSIONS À LA JURIDICTION COMPÉTENTE. - TRANSMISSION OU ATTRIBUTION À UN SECOND TRIBUNAL ADMINISTRATIF DE CONCLUSIONS TENDANT À L'ANNULATION D'UNE DÉCISION DE VALIDATION OU D'HOMOLOGATION DE L'ACCORD COLLECTIF OU DU DOCUMENT DE L'EMPLOYEUR EN CAS DE LICENCIEMENT ÉCONOMIQUE PORTÉES INITIALEMENT DEVANT UN TRIBUNAL ADMINISTRATIF QUI S'EST ESTIMÉ INCOMPÉTENT POUR EN CONNAÎTRE - POINT DE DÉPART DU DÉLAI DE TROIS MOIS IMPARTI PAR LE CODE DU TRAVAIL POUR STATUER À PEINE DE DESSAISISSEMENT - DATE DE RÉCEPTION DU DOSSIER PAR LE SECOND TRIBUNAL.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">66-07 TRAVAIL ET EMPLOI. LICENCIEMENTS. - CONTENTIEUX DES DÉCISIONS DE VALIDATION OU D'HOMOLOGATION DE L'ACCORD COLLECTIF OU DU DOCUMENT DE L'EMPLOYEUR EN CAS DE LICENCIEMENT ÉCONOMIQUE (ART. L. 1233-24-1 ET L. 1233-24-4 DU CODE DU TRAVAIL) - 1) DÉTERMINATION DU TRIBUNAL ADMINISTRATIF TERRITORIALEMENT COMPÉTENT - A) CAS OÙ L'ACCORD OU LE DOCUMENT IDENTIFIE LES ÉTABLISSEMENTS CONCERNÉS ET OÙ CES ÉTABLISSEMENTS SE TROUVENT DANS LE RESSORT D'UN SEUL TRIBUNAL - TRIBUNAL COMPÉTENT POUR CE RESSORT - B) AUTRES CAS - TRIBUNAL DU SIÈGE DE L'ENTREPRISE - 2) POINT DE DÉPART DU DÉLAI DE TROIS MOIS, IMPARTI PAR LE CODE DU TRAVAIL À PEINE DE DESSAISISSEMENT POUR STATUER, EN CAS DE TRANSMISSION DU DOSSIER PAR UN AUTRE TRIBUNAL QUI S'EST ESTIMÉ INCOMPÉTENT POUR EN CONNAÎTRE (ART. R. 351-3 DU CJA) - RÉCEPTION DU DOSSIER.
</SCT>
<ANA ID="9A"> 17-05-01-02 1) Les décisions, qui n'ont pas un caractère réglementaire, par lesquelles l'autorité administrative valide ou homologue l'accord collectif ou le document de l'employeur, portant notamment sur le contenu du plan de sauvegarde de l'emploi, prévus par les articles L. 1233-24-1 à L. 1233-24-4 du code du travail en cas de licenciement économique d'au moins dix salariés pendant une même période de trente jours dans une entreprise d'au moins cinquante salariés, sont relatives à la réglementation du travail et doivent dès lors être contestées devant le tribunal administratif compétent déterminé conformément à la règle édictée par l'article R. 312-10 du code de justice administrative (CJA).,,,a) En application de cet article, lorsque l'accord collectif ou le document de l'employeur relatif au projet de licenciement collectif en cause identifie le ou les établissements auxquels sont rattachés les emplois dont la suppression est envisagée et que ces établissements sont situés dans le ressort d'un même tribunal administratif, ce tribunal est compétent pour connaître du recours pour excès de pouvoir dirigé contre la décision de validation ou d'homologation.,,,b) Dans tous les autres cas, il y a lieu d'estimer que  l'établissement (&#133;) à l'origine du litige  au sens de l'article R. 312-10 du CJA est l'entreprise elle-même et que le tribunal compétent est celui dans le ressort duquel se trouve le siège de l'entreprise.,,,c) En l'espèce, document unilatéral de l'employeur se bornant à mentionner les sites auxquels sont rattachés les emplois supprimés, sans identifier les établissements concernés. Il s'ensuit que le tribunal administratif compétent pour statuer sur les conclusions d'annulation pour excès de pouvoir et sur les conclusions accessoires de la demande est le tribunal dans le ressort duquel se trouve la commune d'implantation du siège de l'entreprise.,,,2) Lorsque, en application des dispositions de l'article R. 351-3 du CJA, un tribunal administratif saisi de conclusions tendant à l'annulation d'une décision de validation ou d'homologation qu'il estime relever de la compétence d'un autre tribunal, transmet la requête à cet autre tribunal ou au président de la section du contentieux du Conseil d'Etat afin que celui-ci en attribue le jugement au tribunal qu'il déclarera compétent, le délai de trois mois imparti au tribunal administratif pour statuer à peine de dessaisissement par l'article L. 1235-7-1 du code du travail court à compter de la réception du dossier par cette dernière juridiction.</ANA>
<ANA ID="9B"> 54-07-01-08 Lorsque, en application des dispositions de l'article R. 351-3 du CJA, un tribunal administratif saisi de conclusions tendant à l'annulation d'une décision de validation ou d'homologation de l'accord collectif ou du document de l'employeur, portant notamment sur le contenu du plan de sauvegarde de l'emploi, prévus pour certains licenciements économiques par les articles L. 1233-24-1 à L. 1233-24-4 du code du travail, qu'il estime relever de la compétence d'un autre tribunal, transmet la requête à cet autre tribunal ou au président de la section du contentieux du Conseil d'Etat afin que celui-ci en attribue le jugement au tribunal qu'il déclarera compétent, le délai de trois mois imparti pour statuer à peine de dessaisissement par l'article L. 1235-7-1 du code du travail court à compter de la réception du dossier par cette dernière juridiction.</ANA>
<ANA ID="9C"> 66-07 1) Les décisions, qui n'ont pas un caractère réglementaire, par lesquelles l'autorité administrative valide ou homologue l'accord collectif ou le document de l'employeur, portant notamment sur le contenu du plan de sauvegarde de l'emploi, prévus par les articles L. 1233-24-1 à L. 1233-24-4 du code du travail en cas de licenciement économique d'au moins dix salariés pendant une même période de trente jours dans une entreprise d'au moins cinquante salariés, sont relatives à la réglementation du travail et doivent dès lors être contestées devant le tribunal administratif compétent déterminé conformément à la règle édictée par l'article R. 312-10 du code de justice administrative (CJA).,,,a) En application de cet article, lorsque l'accord collectif ou le document de l'employeur relatif au projet de licenciement collectif en cause identifie le ou les établissements auxquels sont rattachés les emplois dont la suppression est envisagée et que ces établissements sont situés dans le ressort d'un même tribunal administratif, ce tribunal est compétent pour connaître du recours pour excès de pouvoir dirigé contre la décision de validation ou d'homologation.,,,b) Dans tous les autres cas, il y a lieu d'estimer que l'établissement (&#133;) à l'origine du litige au sens de l'article R. 312-10 du CJA est l'entreprise elle-même et que le tribunal compétent est celui dans le ressort duquel se trouve le siège de l'entreprise.,,,c) En l'espèce, document unilatéral de l'employeur se bornant à mentionner les sites auxquels sont rattachés les emplois supprimés, sans identifier les établissements concernés. Il s'ensuit que le tribunal administratif compétent pour statuer sur les conclusions d'annulation pour excès de pouvoir et sur les conclusions accessoires de la demande est le tribunal dans le ressort duquel se trouve la commune d'implantation du siège de l'entreprise.,,,2) Lorsque, en application des dispositions de l'article R. 351-3 du CJA, un tribunal administratif saisi de conclusions tendant à l'annulation d'une décision de validation ou d'homologation qu'il estime relever de la compétence d'un autre tribunal, transmet la requête à cet autre tribunal ou au président de la section du contentieux du Conseil d'Etat afin que celui-ci en attribue le jugement au tribunal qu'il déclarera compétent, le délai de trois mois imparti au tribunal administratif pour statuer à peine de dessaisissement par l'article L. 1235-7-1 du code du travail court à compter de la réception du dossier par cette dernière juridiction.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
