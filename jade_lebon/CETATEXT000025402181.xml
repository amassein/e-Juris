<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025402181</ID>
<ANCIEN_ID>JG_L_2012_02_000000353134</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/40/21/CETATEXT000025402181.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème et 9ème sous-sections réunies, 20/02/2012, 353134</TITRE>
<DATE_DEC>2012-02-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>353134</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème et 9ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Philippe Martin</PRESIDENT>
<AVOCATS>SCP BARADUC, DUHAMEL</AVOCATS>
<RAPPORTEUR>M. Nicolas Labrune</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Delphine Hedary</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:353134.20120220</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 4 octobre 2011 au secrétariat du contentieux du Conseil d'Etat, présenté pour M. Ariste A, demeurant ... ; M. A demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 1114629/9 du 15 septembre 2011 par laquelle le juge des référés du tribunal administratif de Paris a rejeté sa demande tendant à ce qu'il soit enjoint, sur le fondement de l'article L. 521-3 du code de justice administrative, au centre hospitalier Sainte-Anne de lui communiquer les comptes-rendus de ses hospitalisations du 14 au 20 août 2008 et du 9 au 15 septembre 2008 ;<br/>
<br/>
              2°) statuant en référé, de faire droit à sa demande d'injonction et d'enjoindre en outre au centre hospitalier Sainte-Anne de lui communiquer son scanner du 10 juillet 2008 et son compte-rendu de consultation du 12 août 2008, le tout sous astreinte de 500 euros par jour de retard ;<br/>
<br/>
              3°) de mettre à la charge du centre hospitalier Sainte-Anne la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Nicolas Labrune, Auditeur,<br/>
<br/>
              - les observations de la SCP Baraduc, Duhamel, avocat de M. A,<br/>
<br/>
              - les conclusions de Mme Delphine Hedary, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Baraduc, Duhamel, avocat de M. A ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant qu'il ressort des pièces du dossier soumis au juge des référés que M. A a saisi le 6 juillet 2010 le tribunal administratif de Paris d'une demande tendant à ce que le centre hospitalier Sainte-Anne soit condamné à lui verser la somme de 239 000 euros en réparation des préjudices subis à l'occasion de ses hospitalisations, à ce qu'un nouvel expert soit désigné et à ce qu'il soit enjoint au centre hospitalier Sainte-Anne de lui communiquer l'intégralité de son dossier médical ; que, parallèlement à ce litige, M. A a saisi le juge des référés du tribunal administratif de Paris le 25 juin 2011 d'une requête tendant à ce qu'il soit enjoint, sur le fondement de l'article L. 521-3 du code de justice administrative, au centre hospitalier Sainte-Anne de lui communiquer les comptes-rendus de ses hospitalisations du 14 au 20 août 2008 et du 9 au 15 septembre 2008 ; que le juge des référés du tribunal administratif de Paris a rejeté cette requête par une ordonnance du 15 septembre 2011 au motif que la condition d'urgence de la mesure demandée, posée par l'article L. 521-3 du code de justice administrative, ne pouvait être regardée comme satisfaite dès lors que la demande portait sur la communication de pièces utiles à un litige et que le tribunal administratif qui en était saisi s'était déjà prononcé sur le fond ; que M. A se pourvoit en cassation contre cette ordonnance ;<br/>
<br/>
              Considérant qu'aux termes de l'article L. 521-3 du code de justice administrative : " En cas d'urgence et sur simple requête qui sera recevable même en l'absence de décision administrative préalable, le juge des référés peut ordonner toutes (...) mesures utiles sans faire obstacle à l'exécution d'aucune décision administrative " ; qu'en application de ces dispositions, le juge des référés peut prescrire, notamment, la communication des pièces ou informations mettant à même le demandeur de former un recours ;<br/>
              Considérant toutefois que lorsqu'un tel recours a déjà été formé, une demande présentée au juge des référés portant sur la communication de pièces utiles à la solution du litige est dépourvue d'utilité jusqu'à ce qu'il soit statué définitivement sur le litige, après épuisement, le cas échéant, des voies de recours, ordinaires et extraordinaires, dès lors qu'il appartient au juge saisi du litige, à quelque titre que ce soit, de faire usage des pouvoirs généraux d'instruction qui lui sont dévolus pour ordonner, le cas échéant, les communications qui lui paraissent nécessaires à la solution du litige ;<br/>
<br/>
              Considérant qu'à la date où le juge des référés a statué, M. A estimait que l'urgence à obtenir communication des pièces qu'il demandait résultait de la nécessité pour lui d'en disposer pour augmenter ses chances de relever efficacement appel du jugement du tribunal administratif de Paris statuant sur le litige indemnitaire qui l'opposait au centre hospitalier Sainte-Anne ; qu'il incombait à M. A de solliciter du juge d'appel qu'il aurait saisi la mesure de communication demandée ; que sa demande présentée au juge des référés était ainsi dépourvue d'utilité et n'avait aucun caractère d'urgence ; que, par suite, en rejetant cette demande, le juge des référés n'a pas commis d'erreur de droit ;<br/>
<br/>
              Considérant qu'il résulte de ce qui précède que M. A n'est pas fondé à demander l'annulation de l'ordonnance attaquée ;<br/>
<br/>
              Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge du centre hospitalier Sainte-Anne qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>				D E C I D E  :<br/>
              				--------------<br/>
<br/>
Article 1er : Le pourvoi de M. A est rejeté.<br/>
Article 2 : La présente décision sera notifiée à M. Ariste A et au centre hospitalier Sainte-Anne.<br/>
Copie en sera adressée pour information au ministre du travail, de l'emploi et de la santé.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-035-04-03 PROCÉDURE. PROCÉDURES INSTITUÉES PAR LA LOI DU 30 JUIN 2000. RÉFÉRÉ TENDANT AU PRONONCÉ DE TOUTES AUTRES MESURES UTILES (ART. L. 521-3 DU CODE DE JUSTICE ADMINISTRATIVE). CONDITIONS D'OCTROI DE LA MESURE DEMANDÉE. - DEMANDE DE COMMUNICATION DE PIÈCES UTILES À LA SOLUTION D'UN LITIGE - RECOURS DÉJÀ FORMÉ DEVANT LES JUGES DU FOND - CONSÉQUENCE DES POUVOIRS GÉNÉRAUX D'INSTRUCTION DU JUGE SAISI DU LITIGE POUR ORDONNER LES COMMUNICATIONS NÉCESSAIRES - ABSENCE D'UTILITÉ ET D'URGENCE DE LA DEMANDE PRÉSENTÉE AU JUGE DES RÉFÉRÉS [RJ1].
</SCT>
<ANA ID="9A"> 54-035-04-03 Si le juge des référés peut, en application des dispositions de l'article L. 521-3 du code de justice administrative (CJA), prescrire la communication des pièces ou informations mettant à même le demandeur de former un recours, en revanche, dès lors qu'un tel recours a déjà été formé, une demande présentée au juge des référés portant sur la communication de pièces utiles à la solution du litige est dépourvue d'utilité jusqu'à ce qu'il soit statué définitivement sur le litige, après épuisement, le cas échéant, des voies de recours. En effet, il appartient au juge saisi du litige, à quelque titre que ce soit, de faire usage des pouvoirs généraux d'instruction qui lui sont dévolus pour ordonner, le cas échéant, les communications qui lui paraissent nécessaires à la solution. Par suite, une demande présentée par un requérant au juge des référés pour obtenir communication de pièces aux fins d'augmenter ses chances de relever efficacement appel d'un jugement est dépourvue d'utilité et n'a aucun caractère d'urgence, dès lors qu'il incombait à l'intéressé de solliciter du juge d'appel qu'il aurait saisi la mesure de communication demandée.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr. CE, 3 mars 2008, Ministre de la défense c/ Commune d'Aiguines, n° 308275, T. p. 962 ; JRCE, 1er avril 2005, Potolot, n° 279177, inédite au Recueil. Cf., avant la réforme des procédures d'urgence : CE, 11 octobre 1989, Wade, n° 106414, T. p. 849 ; CE, 28 septembre 1994, Mazeron, n° 146681, T. p. 1111.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
