<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030559657</ID>
<ANCIEN_ID>JG_L_2015_05_000000375882</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/55/96/CETATEXT000030559657.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère / 6ème SSR, 07/05/2015, 375882, Publié au recueil Lebon</TITRE>
<DATE_DEC>2015-05-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>375882</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère / 6ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>M. Yannick Faure</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:375882.20150507</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire et un mémoire complémentaire, enregistrés les 27 février et 26 mai 2014 au secrétariat du contentieux du Conseil d'Etat, l'Association des comédiens et intervenants audiovisuels (ACIA) et M. A...B...demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté du ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social du 24 décembre 2013 portant extension du titre III de la convention collective nationale de la production cinématographique (n° 3097) en tant qu'il porte extension des stipulations du sous-titre II du titre III de cette convention ; <br/>
<br/>
              2°) à titre subsidiaire, de surseoir à statuer jusqu'à ce que l'autorité judiciaire se soit prononcée sur la validité des stipulations du sous-titre II du titre III de cette convention ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              le code du travail ;<br/>
              le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Yannick Faure, auditeur,  <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat du Syndicat français des artistes-interprètes ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Sur les interventions du Syndicat national des acteurs de complément cinéma et télévision CGT et de l'Union des syndicats nationaux de l'audiovisuel CFTC :<br/>
<br/>
              1. Considérant que l'Union des syndicats nationaux de l'audiovisuel CFTC et le Syndicat national des acteurs de complément cinéma et télévision CGT justifient d'un intérêt suffisant à l'annulation de la décision attaquée ; qu'ainsi, leur intervention à l'appui de la requête formée par l'Association des comédiens et intervenants audiovisuels et par M. B...est recevable ;<br/>
<br/>
              Sur la légalité de l'arrêté du 24 décembre 2013 :<br/>
<br/>
              2. Considérant, en premier lieu, qu'aux termes du premier alinéa de l'article L. 2261-16 du code du travail : " Le ministre chargé du travail peut également, conformément à la procédure d'extension prévue à la sous-section 3, rendre obligatoires, par arrêté, les avenants ou annexes à une convention ou à un accord étendu " ; qu'il résulte de ces dispositions que le ministre chargé du travail ne peut rendre obligatoire, par arrêté, un avenant ou une annexe à une convention ou à un accord que si cette convention ou cet accord a été étendu ;<br/>
<br/>
              3. Considérant qu'en raison des effets qui s'y attachent, l'annulation pour excès de pouvoir d'un acte administratif, qu'il soit ou non réglementaire, emporte, lorsque le juge est saisi de conclusions recevables, l'annulation par voie de conséquence des décisions administratives consécutives qui n'auraient pu légalement être prises en l'absence de l'acte annulé ou qui sont en l'espèce intervenues en raison de l'acte annulé ; qu'il incombe au juge de l'excès de pouvoir, lorsqu'il est saisi de conclusions recevables dirigées contre de telles décisions consécutives, de prononcer leur annulation par voie de conséquence, le cas échéant en relevant d'office un tel moyen, qui découle de l'autorité absolue de chose jugée qui s'attache à l'annulation du premier acte ;<br/>
<br/>
              4. Considérant que la requête tend à l'annulation de l'arrêté du ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social du 24 décembre 2013 portant extension du titre III de la convention collective nationale de la production cinématographique, en tant qu'il porte extension des dispositions du sous-titre II de ce titre III ; que, par une décision du 24 février 2015, le Conseil d'Etat, statuant au contentieux a annulé l'arrêté du ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social du 1er juillet 2013 portant extension de la convention collective nationale de la production cinématographique, qui ne comportait, à la date de son extension, que les titres I et II ;<br/>
<br/>
              5. Considérant qu'il résulte des dispositions de l'article L. 2261-16 du code du travail que l'arrêté attaqué n'aurait pu légalement être pris en l'absence de l'acte annulé ; que, par voie de conséquence de cette annulation, l'arrêté du 24 décembre 2013 portant extension du titre III de la convention collective nationale de la production cinématographique doit également être annulé en tant qu'il porte extension du sous-titre II de ce titre III ;<br/>
<br/>
              6. Considérant, en deuxième lieu, qu'aux termes de l'article L. 3121-1 du code du travail : " La durée du travail effectif est le temps pendant lequel le salarié est à la disposition de l'employeur et se conforme à ses directives sans pouvoir vaquer librement à des occupations personnelles " ;<br/>
<br/>
              7. Considérant que l'article 4.1.2 du sous-titre II du titre III de la convention collective nationale de la production cinématographique prévoit que, pour les tournages en décors naturels nécessitant cinquante acteurs de complément ou plus, un temps d'émargement pouvant aller jusqu'à trente minutes à partir de l'heure de la convocation ne sera pas décompté comme temps de travail effectif ; qu'il ressort des stipulations de la convention que, pendant le temps d'émargement, qui est la conséquence de l'organisation imposée par l'employeur, les acteurs de complément sont à la disposition de celui-ci et se conforment à ses directives, sans pouvoir vaquer librement à des occupations personnelles ; qu'il résulte d'une jurisprudence établie de la Cour de cassation que, dans une telle hypothèse, la période en cause fait partie de la durée du travail effectif ; qu'il apparaît ainsi manifestement que le temps d'émargement prévu par la convention doit être inclus dans la durée du travail effectif ; que, par suite, la contestation relative à la validité de la convention sur ce point peut être accueillie par le Conseil d'Etat, saisi de la légalité de l'arrêté prononçant l'extension de son titre III ;<br/>
<br/>
              8. Considérant, en troisième lieu, qu'il résulte d'une jurisprudence établie de la Cour de cassation qu'une convention collective ne peut prévoir de différences de traitement entre salariés relevant de la même catégorie professionnelle et placés dans la même situation au regard de l'avantage qu'elle prévoit que si ces différences reposent sur des raisons objectives dont le juge doit contrôler la réalité et la pertinence ;<br/>
<br/>
              9. Considérant que l'annexe III.2 au sous-titre II du titre III de la convention collective litigieuse prévoit un barème de salaires minimaux garantis aux acteurs de complément et un barème d'indemnités applicable aux seuls " films se tournant à Paris et sa banlieue contenue dans un rayon inférieur ou égal à 40 km autour de la ville ainsi qu'à Marseille, Lyon, Bordeaux Nice, Lille, Nantes et leurs banlieues respectives contenues dans un rayon inférieur ou égal à 25 kilomètres autour de ces villes " ; qu'il ne ressort pas des pièces du dossier que les acteurs de complément seraient placés dans une situation différente, au regard de la rémunération, selon le lieu du tournage tel qu'il est défini par la convention collective ni que la différence de traitement prévue reposerait sur des raisons objectives pertinentes ; que, par suite, il apparaît manifestement que la contestation relative à la validité de la convention sur ce point peut également être accueillie par le Conseil d'Etat, saisi de la légalité de l'arrêté prononçant l'extension de son titre III ;<br/>
<br/>
              10. Considérant qu'il résulte de tout ce qui précède que, sans qu'il soit besoin d'examiner les autres moyens de la requête, les requérants sont fondés à demander l'annulation de l'arrêté du 24 décembre 2013 portant extension du titre III de la convention collective nationale de la production cinématographique en tant qu'il porte extension du sous-titre II de son titre III ;<br/>
<br/>
              Sur les conséquences de l'illégalité de l'arrêté attaqué :<br/>
<br/>
              11. Considérant qu'eu égard, d'une part, aux conséquences susceptibles de résulter de l'annulation de cet arrêté en tant qu'il étend le deuxième alinéa de l'article 4.1.2 du sous-titre II du titre III de la convention collective nationale de la production cinématographique et, d'autre part, à la nature de l'illégalité constatée ci-dessus, il n'y a pas lieu, dans les circonstances de l'espèce, de limiter les effets de l'annulation sur ce point ;<br/>
<br/>
              12. Considérant, en revanche, que l'annulation rétroactive de l'arrêté attaqué en tant qu'il étend les autres stipulations du sous-titre II du titre III de la convention collective nationale de la production cinématographique serait à l'origine de graves incertitudes pour la rémunération des salariés des entreprises non affiliées aux syndicats d'employeurs signataires de la convention et de son avenant et aurait, dans les circonstances de l'espèce, des conséquences manifestement excessives ; que, dès lors, compte tenu de ces effets et des motifs de l'annulation prononcée, il y a lieu de prévoir qu'en tant qu'il étend les clauses du sous-titre II du titre III de la convention collective nationale de la production cinématographique autres que celles du deuxième alinéa de son article 4.1.2, et sous réserve des actions contentieuses engagées à la date de la présente décision contre les actes pris sur son fondement, les effets de l'arrêté du 24 décembre 2013 antérieurs au 11 avril 2015, date d'entrée en vigueur de l'arrêté du 31 mars 2015 par lequel le ministre chargé du travail a de nouveau procédé à l'extension de la convention collective nationale de la production cinématographique, y compris son titre III, devront être regardés comme définitifs ; <br/>
<br/>
              Sur les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              13. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme globale de 3 000 euros à verser à l'Association des comédiens et intervenants audiovisuels et à M. B...au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les interventions de l'Union des syndicats nationaux de l'audiovisuel CFTC et du Syndicat national des acteurs de complément cinéma et télévision CGT sont admises. <br/>
Article 2 : L'arrêté du 24 décembre 2013 portant extension du titre III de la convention collective nationale de la production cinématographique est annulé en tant qu'il porte extension du sous-titre II de ce titre III.<br/>
Article 3 : Sous réserve des actions contentieuses engagées à la date de la présente décision contre les actes pris sur son fondement, les effets produits antérieurement au 11 avril 2015 par l'arrêté du 24 décembre 2013, en tant qu'il étend les clauses du sous-titre II du titre III de la convention collective nationale de la production cinématographique autres que celles du deuxième alinéa de son article 4.1.2, sont regardés comme définitifs.<br/>
Article 4 : L'Etat versera à l'Association des comédiens et intervenants audiovisuels et à M. B... une somme globale de 3 000 euros en application des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 5 : La présente décision sera notifiée à l'Association des comédiens et intervenants audiovisuels (ACIA), à M. A...B..., au Syndicat national des acteurs de complément cinéma et télévision CGT, à l'Union des syndicats nationaux de l'audiovisuel CFTC et au ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social.<br/>
Copie en sera adressée à l'Association des producteurs indépendants, au Syndicat national des techniciens et travailleurs de la  production  cinématographique  et de  télévision, au Syndicat français des artistes-interprètes CGT, à la Fédération CFE-CGC de la culture, de la communication et du spectacle, à la Fédération communication CFTC, à l'Association française des producteurs de films, à l'Association des producteurs de cinéma, au Syndicat des producteurs indépendants et à l'Union des producteurs de films.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">17-04-02-02 COMPÉTENCE. COMPÉTENCES CONCURRENTES DES DEUX ORDRES DE JURIDICTION. CONTENTIEUX DE L'APPRÉCIATION DE LA LÉGALITÉ. CAS OÙ UNE QUESTION PRÉJUDICIELLE NE S'IMPOSE PAS. - LITIGE RELATIF À L'ARRÊTÉ D'EXTENSION D'UNE CONVENTION COLLECTIVE - HYPOTHÈSE DANS LAQUELLE IL APPARAÎT MANIFESTEMENT, AU VU D'UNE JURISPRUDENCE ÉTABLIE, QUE LA CONTESTATION DE LA CONVENTION PEUT ÊTRE ACCUEILLIE PAR LE JUGE SAISI AU PRINCIPAL [RJ1] - 1) DURÉE DU TRAVAIL EFFECTIF - NOTION - 2) DIFFÉRENCE DE TRAITEMENT ENTRE SALARIÉS - CONDITIONS.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-05-03-01 PROCÉDURE. INCIDENTS. INTERVENTION. RECEVABILITÉ. - EXISTENCE - SYNDICAT SIGNATAIRE D'UNE CONVENTION COLLECTIVE INTERVENANT À L'APPUI DE LA DEMANDE D'ANNULATION DE L'ARRÊTÉ D'EXTENSION DE CETTE CONVENTION.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-07-01-09 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. QUESTIONS GÉNÉRALES. QUESTION PRÉJUDICIELLE POSÉE PAR LE JUGE ADMINISTRATIF. - LITIGE RELATIF À L'ARRÊTÉ D'EXTENSION D'UNE CONVENTION COLLECTIVE - HYPOTHÈSE DANS LAQUELLE IL APPARAÎT MANIFESTEMENT, AU VU D'UNE JURISPRUDENCE ÉTABLIE, QUE LA CONTESTATION DE LA CONVENTION PEUT ÊTRE ACCUEILLIE PAR LE JUGE SAISI AU PRINCIPAL [RJ1] - 1) DURÉE DU TRAVAIL EFFECTIF - NOTION - 2) DIFFÉRENCE DE TRAITEMENT ENTRE SALARIÉS - CONDITIONS.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">54-07-023 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. - LITIGE RELATIF À L'ARRÊTÉ D'EXTENSION D'UNE CONVENTION COLLECTIVE - MODULATION DANS LE TEMPS DES EFFETS DE L'ANNULATION, EU ÉGARD À SES EFFETS ET À SES MOTIFS - 1) ABSENCE EN CE QUI CONCERNE L'EXTENSION DE CERTAINES STIPULATIONS - 2) EXISTENCE EN CE QUI CONCERNE L'EXTENSION D'AUTRES STIPULATIONS.
</SCT>
<SCT ID="8E" TYPE="PRINCIPAL">66-02-02 TRAVAIL ET EMPLOI. CONVENTIONS COLLECTIVES. EXTENSION DES CONVENTIONS COLLECTIVES. - RECOURS DIRIGÉ CONTRE UN ARRÊTÉ D'EXTENSION - SYNDICAT SIGNATAIRE DE LA CONVENTION - INTÉRÊT À INTERVENIR À L'APPUI DU RECOURS - EXISTENCE.
</SCT>
<SCT ID="8F" TYPE="PRINCIPAL">66-02-02-035 TRAVAIL ET EMPLOI. CONVENTIONS COLLECTIVES. EXTENSION DES CONVENTIONS COLLECTIVES. CONDITION DE LÉGALITÉ DE L'EXTENSION TENANT À LA VALIDITÉ DE LA CONVENTION. - 1) HYPOTHÈSE DANS LAQUELLE IL APPARAÎT MANIFESTEMENT, AU VU D'UNE JURISPRUDENCE ÉTABLIE, QUE LA CONTESTATION DE LA CONVENTION PEUT ÊTRE ACCUEILLIE PAR LE JUGE SAISI AU PRINCIPAL [RJ1] - A) DURÉE DU TRAVAIL EFFECTIF - NOTION - B) DIFFÉRENCE DE TRAITEMENT ENTRE SALARIÉS - CONDITIONS - 2) MODULATION DANS LE TEMPS DES EFFETS DE L'ANNULATION DE L'ARRÊTÉ D'EXTENSION, EU ÉGARD AUX EFFETS ET AUX MOTIFS DE L'ANNULATION - A) ABSENCE EN CE QUI CONCERNE L'EXTENSION DE CERTAINES STIPULATIONS - B) EXISTENCE EN CE QUI CONCERNE L'EXTENSION D'AUTRES STIPULATIONS.
</SCT>
<ANA ID="9A"> 17-04-02-02 1) Convention collective prévoyant que, dans certains cas, un temps d'émargement pouvant aller jusqu'à trente minutes à partir de l'heure de la convocation ne sera pas décompté comme temps de travail effectif. Il ressort des stipulations de la convention que, pendant le temps d'émargement, qui est la conséquence de l'organisation imposée par l'employeur, les salariés concernés sont à la disposition de celui-ci et se conforment à ses directives, sans pouvoir vaquer librement à des occupations personnelles. Il résulte d'une jurisprudence établie de la Cour de cassation que, dans une telle hypothèse, la période en cause fait partie de la durée du travail effectif. Il apparaît ainsi manifestement que le temps d'émargement prévu par la convention doit être inclus dans la durée du travail effectif.  Par suite, la contestation relative à la validité de la convention sur ce point peut être accueillie par le Conseil d'Etat, saisi de la légalité de l'arrêté prononçant l'extension.,,,2) Il résulte d'une jurisprudence établie de la Cour de cassation qu'une convention collective ne peut prévoir de différences de traitement entre salariés relevant de la même catégorie professionnelle et placés dans la même situation au regard de l'avantage qu'elle prévoit que si ces différences reposent sur des raisons objectives dont le juge doit contrôler la réalité et la pertinence.</ANA>
<ANA ID="9B"> 54-05-03-01 Un syndicat est recevable à intervenir à l'appui d'une requête dirigée contre l'arrêté d'extension d'une convention collective dont il est signataire.</ANA>
<ANA ID="9C"> 54-07-01-09 1) Convention collective prévoyant que, dans certains cas, un temps d'émargement pouvant aller jusqu'à trente minutes à partir de l'heure de la convocation ne sera pas décompté comme temps de travail effectif. Il ressort des stipulations de la convention que, pendant le temps d'émargement, qui est la conséquence de l'organisation imposée par l'employeur, les salariés concernés sont à la disposition de celui-ci et se conforment à ses directives, sans pouvoir vaquer librement à des occupations personnelles. Il résulte d'une jurisprudence établie de la Cour de cassation que, dans une telle hypothèse, la période en cause fait partie de la durée du travail effectif. Il apparaît ainsi manifestement que le temps d'émargement prévu par la convention doit être inclus dans la durée du travail effectif.  Par suite, la contestation relative à la validité de la convention sur ce point peut être accueillie par le Conseil d'Etat, saisi de la légalité de l'arrêté prononçant l'extension.,,,2) Il résulte d'une jurisprudence établie de la Cour de cassation qu'une convention collective ne peut prévoir de différences de traitement entre salariés relevant de la même catégorie professionnelle et placés dans la même situation au regard de l'avantage qu'elle prévoit que si ces différences reposent sur des raisons objectives dont le juge doit contrôler la réalité et la pertinence.</ANA>
<ANA ID="9D"> 54-07-023 1) Stipulations d'une convention collective prévoyant illégalement que, dans certains cas, un temps d'émargement pouvant aller jusqu'à trente minutes à partir de l'heure de la convocation ne sera pas décompté comme temps de travail effectif. Eu égard, d'une part, aux conséquences susceptibles de résulter de l'annulation de l'arrêté d'extension en tant qu'il étend ces stipulations et, d'autre part, à la nature de l'illégalité constatée, il n'y a pas lieu, dans les circonstances de l'espèce, de limiter les effets de l'annulation de l'arrêté d'extension sur ce point.,,,2) Stipulations de la même convention collective prévoyant illégalement une différence de traitement des salariés en matière de rémunération. L'annulation rétroactive de l'arrêté d'extension en tant qu'il étend ces stipulations serait à l'origine de graves incertitudes pour la rémunération des salariés des entreprises non affiliées aux syndicats d'employeurs signataires de la convention et aurait, dans les circonstances de l'espèce, des conséquences manifestement excessives. Dès lors, compte tenu de ces effets et des motifs de l'annulation prononcée, il y a lieu de prévoir qu'en tant qu'il étend ces stipulations, et sous réserve des actions contentieuses engagées, à la date de la décision du Conseil d'Etat, contre les actes pris sur son fondement, les effets passés de l'arrêté d'extension devront être regardés comme définitifs.</ANA>
<ANA ID="9E"> 66-02-02 Un syndicat est recevable à intervenir à l'appui d'une requête dirigée contre l'arrêté d'extension d'une convention collective dont il est signataire.</ANA>
<ANA ID="9F"> 66-02-02-035 1) a) Convention collective prévoyant que, dans certains cas, un temps d'émargement pouvant aller jusqu'à trente minutes à partir de l'heure de la convocation ne sera pas décompté comme temps de travail effectif. Il ressort des stipulations de la convention que, pendant le temps d'émargement, qui est la conséquence de l'organisation imposée par l'employeur, les salariés concernés sont à la disposition de celui-ci et se conforment à ses directives, sans pouvoir vaquer librement à des occupations personnelles. Il résulte d'une jurisprudence établie de la Cour de cassation que, dans une telle hypothèse, la période en cause fait partie de la durée du travail effectif. Il apparaît ainsi manifestement que le temps d'émargement prévu par la convention doit être inclus dans la durée du travail effectif.  Par suite, la contestation relative à la validité de la convention sur ce point peut être accueillie par le Conseil d'Etat, saisi de la légalité de l'arrêté prononçant l'extension.,,,b) Il résulte d'une jurisprudence établie de la Cour de cassation qu'une convention collective ne peut prévoir de différences de traitement entre salariés relevant de la même catégorie professionnelle et placés dans la même situation au regard de l'avantage qu'elle prévoit que si ces différences reposent sur des raisons objectives dont le juge doit contrôler la réalité et la pertinence.,,,2) a) Stipulations de la convention collective prévoyant illégalement que, dans certains cas, un temps d'émargement pouvant aller jusqu'à trente minutes à partir de l'heure de la convocation ne sera pas décompté comme temps de travail effectif. Eu égard, d'une part, aux conséquences susceptibles de résulter de l'annulation de l'arrêté d'extension en tant qu'il étend ces stipulations et, d'autre part, à la nature de l'illégalité constatée, il n'y a pas lieu, dans les circonstances de l'espèce, de limiter les effets de l'annulation de l'arrêté d'extension sur ce point.,,,b) Stipulations de la même convention collective prévoyant illégalement une différence de traitement des salariés en matière de rémunération. L'annulation rétroactive de l'arrêté d'extension en tant qu'il étend ces stipulations serait à l'origine de graves incertitudes pour la rémunération des salariés des entreprises non affiliées aux syndicats d'employeurs signataires de la convention et aurait, dans les circonstances de l'espèce, des conséquences manifestement excessives. Dès lors, compte tenu de ces effets et des motifs de l'annulation prononcée, il y a lieu de prévoir qu'en tant qu'il étend ces stipulations, et sous réserve des actions contentieuses engagées, à la date de la décision du Conseil d'Etat, contre les actes pris sur son fondement, les effets passés de l'arrêté d'extension devront être regardés comme définitifs.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Section, 23 mars 2012, Fédération Sud Santé Sociaux, n° 331805, p. 102.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
