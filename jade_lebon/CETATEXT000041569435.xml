<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041569435</ID>
<ANCIEN_ID>JG_L_2020_02_000000428983</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/56/94/CETATEXT000041569435.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 6ème chambres réunies, 12/02/2020, 428983</TITRE>
<DATE_DEC>2020-02-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>428983</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Olivier Rousselle</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2020:428983.20200212</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A... B... a demandé au tribunal administratif de Lille d'annuler pour excès de pouvoir la décision du 14 mars 2018 par laquelle le ministre de l'intérieur a refusé de reconnaître la validité du permis de conduire qui lui a été délivré par les autorités belges, d'annuler la décision 48SI du 5 janvier 2009 par laquelle le ministre de l'intérieur a invalidé son permis de conduire français ainsi que les décisions de retrait de points consécutives aux infractions des 31 mai 2003, 1er avril 2004, 15 novembre 2004, 24 novembre 2004, 27 mai 2005, 14 novembre 2005, 8 décembre 2005, 2 août 2006, 26 décembre 2006, 7 juillet 2007 et 24 juin 2008, d'enjoindre au ministre de l'intérieur de régulariser sa situation et d'admettre la validité de son permis belge et à défaut de lui restituer les 12 points retirés sur son permis français. Par un jugement n°1802567 du 29 janvier 2019, le tribunal administratif de Lille a annulé la décision du 14 mars 2018 et rejeté le surplus de sa demande.<br/>
<br/>
              Par un pourvoi, enregistré le 18 mars 2019 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'intérieur demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement en tant qu'il annule la décision du 14 mars 2018 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter dans cette mesure la demande de M. B....<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la directive 91/439/CEE du Conseil du 29 juillet 1991 ;<br/>
              - le code de la route ;<br/>
              - l'arrêté du 12 janvier 2012 fixant les conditions de reconnaissance et d'échange des permis de conduire délivrés par les Etats n'appartenant ni à l'Union européenne, ni à l'Espace économique européen ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Rousselle, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par une décision du 5 janvier 2009, le ministre de l'intérieur a procédé au retrait de quatre points du permis de conduire délivré par les autorités françaises à M. B... et l'a informé de la perte de validité de ce permis pour solde de points nul. M. B..., ayant établi sa résidence en Belgique, a toutefois obtenu des autorités belges l'échange de ce permis français contre un permis de conduire belge le 8 juillet 2014. Par une décision du 14 mars 2018, le ministre de l'intérieur, saisi d'une demande à cette fin présentée par M. B..., a refusé de reconnaître ce permis belge sur le territoire français. Le ministre se pourvoit en cassation contre le jugement du 29 janvier 2019 du tribunal administratif de Lille en tant que, par ce jugement, le tribunal a, sur la demande de M. B..., annulé la décision du 14 mars 2018 refusant de reconnaître le permis délivré par les autorités belges.<br/>
<br/>
              2. D'une part, le paragraphe 1 de l'article 8 de la directive du Conseil du 29 juillet 1991 relative au permis de conduire, en vigueur à la date à laquelle M. B... a obtenu son permis de conduire belge, prévoyait que : " Dans le cas où le titulaire d'un permis de conduire en cours de validité délivré par un Etat membre a établi sa résidence normale dans un autre Etat membre, il peut demander l'échange de son permis contre un permis équivalent ; il appartient à l'Etat membre qui procède à l'échange de vérifier, le cas échéant, si le permis présenté est effectivement en cours de validité ". Selon le paragraphe 2 du même article 8 : " (...) l'Etat membre de résidence normale peut appliquer au titulaire d'un permis de conduire délivré par un autre Etat membre ses dispositions nationales concernant la restriction, la suspension, le retrait ou l'annulation du droit de conduire (...) ". Enfin, aux termes du paragraphe 4 du même article 8 : " Un Etat membre peut refuser de reconnaître, à une personne faisant l'objet sur son territoire d'une des mesures visées au paragraphe 2, la validité de tout permis de conduire établi par un autre Etat membre ".<br/>
<br/>
              3. D'autre part, l'article L. 223-5 du code de la route dispose que : " En cas de retrait de la totalité des points, l'intéressé reçoit de l'autorité administrative l'injonction de remettre son permis de conduire au préfet de son département de résidence et perd le droit de conduire un véhicule. / Il ne peut obtenir un nouveau permis de conduire avant l'expiration d'un délai de six mois à compter de la date de remise de son permis au préfet (...). Ce délai est porté à un an lorsqu'un nouveau retrait de la totalité des points intervient dans un délai de cinq ans suivant le précédent (...) ". <br/>
<br/>
              4. Enfin, aux termes du premier alinéa de l'article R. 222-1 du code de la route : " Tout permis de conduire national régulièrement délivré par un Etat membre de l'Union européenne ou partie à l'accord sur l'Espace économique européen est reconnu en France sous réserve d'être en cours de validité ". <br/>
<br/>
              5. Si, en vertu de ces dernières dispositions, un permis délivré régulièrement par un autre Etat membre de l'Union européenne ou partie à l'accord sur l'Espace économique européen doit, en principe, être reconnu en France, ces dispositions ne sauraient imposer aux autorités françaises de reconnaître en France un tel permis dans le cas où il a été délivré par l'autre Etat par voie d'échange avec un permis français qui n'était plus valide à la date à laquelle il a été échangé, notamment en raison d'un retrait de points.<br/>
<br/>
              6. Par suite, en jugeant qu'à raison de sa validité en Belgique, le permis belge délivré à M. B... devait être reconnu par les autorités françaises, alors qu'il ressortait des pièces du dossier soumis aux juges du fond et n'était d'ailleurs pas contesté que ce permis avait été délivré par les autorités belges en échange d'un permis français qui avait perdu sa validité à la suite de la perte de tous ses points, le tribunal administratif a commis une erreur de droit. Le ministre de l'intérieur est, par suite, fondé à demander l'annulation de l'article 1er du jugement qu'il attaque.<br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond, dans la mesure de la cassation prononcée, par application des dispositions de l'article L. 821-1 du code de justice administrative, et de statuer sur les conclusions de M. B... dirigées contre la décision du 14 mars 2018.<br/>
<br/>
              8. Il ressort des pièces du dossier que le permis français de M. B... avait perdu sa validité après le retrait de ses derniers points le 5 janvier 2009 et que c'est en échange de ce permis pourtant dépourvu de validité que les autorités belges lui ont délivré un permis de conduire belge le 8 juillet 2014. Par suite, il résulte de ce qui a été dit au point 5 que le ministre de l'intérieur a pu légalement refuser la reconnaissance, sur le territoire français, du permis de conduire belge détenu par M. B.... Ce dernier, qui ne peut utilement invoquer la circonstance que la décision du 5 janvier 2009 ne lui aurait pas été régulièrement notifiée, n'est, par suite, pas fondé à demander l'annulation de la décision qu'il attaque.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'article 1er du jugement du tribunal administratif de Lille du 29 janvier 2019 est annulé.<br/>
<br/>
Article 2 : Les conclusions de M. B... présentées devant le tribunal administratif de Lille tendant à l'annulation de la décision du ministre de l'intérieur du 14 mars 2018 sont rejetées.<br/>
<br/>
Article 3 : La présente décision sera notifiée à M. A... B... et au ministre de l'intérieur.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">49-04-01-04 POLICE. POLICE GÉNÉRALE. CIRCULATION ET STATIONNEMENT. PERMIS DE CONDUIRE. - OBLIGATION DE RECONNAISSANCE DES PERMIS DE CONDUIRE ÉTRANGERS (ART. R. 222-1 DU CODE DE LA ROUTE) - CAS D'UN PERMIS ÉTRANGER DÉLIVRÉ PAR VOIE D'ÉCHANGE AVEC UN PERMIS FRANÇAIS INVALIDE - ABSENCE.
</SCT>
<ANA ID="9A"> 49-04-01-04 Si, en vertu de l'article R. 222-1 du code de la route, un permis délivré régulièrement par un autre Etat membre de l'Union européenne ou partie à l'accord sur l'Espace économique européen doit, en principe, être reconnu en France, ces dispositions ne sauraient imposer aux autorités françaises de reconnaître en France un tel permis dans le cas où il a été délivré par l'autre Etat par voie d'échange avec un permis français qui n'était plus valide à la date à laquelle il a été échangé, notamment en raison d'un retrait de points.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
