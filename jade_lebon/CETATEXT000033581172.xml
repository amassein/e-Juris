<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033581172</ID>
<ANCIEN_ID>JG_L_2016_12_000000390327</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/58/11/CETATEXT000033581172.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 6ème chambres réunies, 07/12/2016, 390327</TITRE>
<DATE_DEC>2016-12-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>390327</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GATINEAU, FATTACCINI ; SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>M. Frédéric Puigserver</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CECHR:2016:390327.20161207</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Meubles Gimazane a demandé au tribunal administratif de Caen d'annuler pour excès de pouvoir l'arrêté du préfet de la Manche du 9 avril 2009 relatif à la fermeture hebdomadaire des établissements d'ameublement. Par un jugement n° 0901252 du 30 juin 2010, le tribunal d'administratif de Caen a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 14NT00840 du 20 mars 2015, rendu sur renvoi du Conseil d'Etat, la cour administrative d'appel de Nantes a rejeté l'appel formé par la société Meubles Gimazane contre ce jugement du 30 juin 2010.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire rectificatif et un mémoire complémentaire, enregistrés les 20 mai, 21 mai et 19 août 2015 au secrétariat du contentieux du Conseil d'Etat, la société Meubles Gimazane demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt de la cour administrative d'appel de Nantes du 20 mars 2015 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code du travail ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Puigserver, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Foussard, Froger, avocat de la société Meubles Gimazane et à la chambre régionale du négoce de l'ameublement et de l'équipement de la maison de Basse-Normandie.<br/>
<br/>
              Vu la note en délibéré, enregistrée le 5 décembre 2016, présentée par le ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 9 avril 2009, pris sur le fondement de l'article L. 3132-29 du code du travail, au vu d'un accord conclu le 8 décembre 2008 et modifié par avenant du 5 février 2009 entre la chambre régionale de l'ameublement et de l'équipement de la maison de Basse-Normandie et cinq organisations syndicales de salariés, le préfet de la Manche a ordonné la fermeture, dans l'ensemble du département, pendant quarante-sept dimanches par an, des commerces relevant de la convention collective de l'ameublement et a renvoyé la détermination des cinq dimanches travaillés par an à une décision de la chambre régionale, prise après consultation des professionnels concernés ; que la société Meubles Gimazane demande l'annulation de  l'arrêt du 20 mars 2015 par lequel la cour administrative d'appel de Nantes a confirmé le jugement du tribunal administratif de Caen du 30 juin 2010 qui avait rejeté sa demande tendant à l'annulation de cet arrêté préfectoral ;<br/>
<br/>
              2. Considérant que Me A...et la SELARL Bruno Cambon, respectivement administrateur judiciaire et mandataire judiciaire à la procédure de redressement judiciaire de la société Meubles Gimazane, justifient d'un intérêt suffisant à l'annulation de l'arrêt attaqué ; qu'ainsi, leur intervention est recevable ;<br/>
<br/>
              3. Considérant qu'aux termes de l'article L. 3132-29 du code du travail, dans sa rédaction applicable à la date de l'arrêté attaqué : " Lorsqu'un accord est intervenu entre les organisations syndicales de salariés et les organisations d'employeurs d'une profession et d'une zone géographique déterminées sur les conditions dans lesquelles le repos hebdomadaire est donné aux salariés, le préfet peut, par arrêté, sur la demande des syndicats intéressés, ordonner la fermeture au public des établissements de la profession ou de la zone géographique concernée pendant toute la durée de ce repos. Ces dispositions ne s'appliquent pas aux activités dont les modalités de fonctionnement et de paiement sont automatisées " ; que la fermeture au public des établissements d'une profession ne peut légalement être ordonnée sur la base d'un accord syndical que dans la mesure où cet accord correspond pour la profession à la volonté de la majorité indiscutable de tous ceux qui exercent cette profession à titre principal ou accessoire dans la zone géographique considérée et dont l'établissement ou une partie de celui-ci est susceptible d'être fermé ; que l'existence de cette majorité est vérifiée lorsque les entreprises adhérentes à la ou aux organisations d'employeurs qui ont signé l'accord ou s'y sont déclarées expressément favorables exploitent la majorité des établissements intéressés ou que la consultation de l'ensemble des entreprises concernées a montré que l'accord recueillait l'assentiment d'un nombre d'entreprises correspondant à la majorité des établissements intéressés ;<br/>
<br/>
              4. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond qu'à la suite de la loi du 3 janvier 2008 pour le développement de la concurrence au service des consommateurs, qui avait ajouté les " établissements de commerce de détail d'ameublement " à la liste des catégories d'établissements admis de plein droit à donner le repos hebdomadaire par roulement, et avant de conclure un accord avec les organisations de salariés sur les conditions d'une fermeture hebdomadaire dominicale, la chambre régionale de l'ameublement et de l'équipement de la maison de Basse-Normandie, affiliée à la Fédération française du négoce de l'ameublement et de l'équipement de la maison, et seule organisation professionnelle localement représentative de ce secteur professionnel, a jugé utile de réaliser une enquête auprès des entreprises concernées et leur a, à cet effet, adressé un questionnaire portant sur le principe d'une fermeture dominicale assortie d'une ouverture quelques dimanches par an ; que, pour estimer que l'accord conclu reflétait la majorité indiscutable des professionnels du secteur, la cour administrative d'appel de Nantes a pris en compte la majorité des avis exprimés par les établissements ayant leur siège dans le département de La Manche, lors de l'enquête réalisée à l'initiative de la chambre régionale, et non, compte tenu du nombre des établissements qui n'avaient pas répondu à cette enquête, la majorité de ceux qui exerçaient la profession dans le département ; que, ce faisant, la cour a commis une erreur de droit ; que, par suite, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, la société requérante est fondée à demander l'annulation de l'arrêt de la cour administrative d'appel de Nantes qu'elle attaque ;<br/>
<br/>
              5. Considérant qu'aux termes du second alinéa de l'article L. 821-2 du code de justice administrative : " Lorsque l'affaire fait l'objet d'un second pourvoi en cassation, le Conseil d'Etat statue définitivement sur cette affaire " ; qu'il y a lieu, par suite, de régler l'affaire au fond ;<br/>
<br/>
              6. Considérant, en premier lieu, qu'il ressort des pièces du dossier que, pour la réalisation, à l'initiative de la chambre régionale de l'ameublement et de l'équipement de la maison de Basse-Normandie, de l'enquête mentionnée au point 4, auprès des quarante-et-un établissements d'ameublement du département de la Manche, le courrier du 19 août 2008 qui leur a été adressé, contenant un questionnaire, indiquait que l'absence de réponse de leur part vaudrait acceptation des décisions qui seraient prises par la chambre régionale ; que vingt établissements ont répondu au questionnaire distribué, seize s'étant prononcé en faveur de la fermeture la plupart des dimanches de l'année et quatre ayant exprimé un avis défavorable ; que, compte tenu des modalités de consultation ainsi définies, permettant à tous les établissements exerçant la profession dans le département d'exprimer leur position compte tenu des conséquences clairement indiquées de leur abstention, et alors qu'il n'est pas contesté que tous ces établissements ont reçu le courrier du 19 août 2008 accompagné du questionnaire, les vingt-et-un établissements n'ayant pas répondu doivent être regardés, dans les circonstances de l'espèce, comme ayant exprimé un avis favorable ; qu'ainsi, la condition de majorité indiscutable en faveur de l'accord, qui s'établissait à vingt-et-un, était satisfaite ;<br/>
<br/>
              7. Considérant, en deuxième lieu, qu'aux termes du premier alinéa de l'article R. 2131-1 du code du travail : " Les statuts du syndicat sont déposés à la mairie de la localité où le syndicat est établi " ; que la circonstance que la chambre régionale de l'ameublement et de l'équipement de la maison de Basse-Normandie, qui avait, lors de sa constitution en 2004, déposé ses statuts auprès de la mairie de Caen, localité où elle était établie, ait ensuite transféré son siège dans la commune de Cormelles-Le-Royal et n'y ait déposé ses nouveaux statuts qu'en 2010, n'est pas de nature à établir qu'elle n'aurait pas été régulièrement constituée lors de la conclusion de l'accord du 8 décembre 2008 et ne disposait pas de la capacité juridique pour conclure cet accord ;<br/>
<br/>
              8. Considérant, en troisième lieu, que le principe d'égalité n'oblige pas à traiter différemment des personnes se trouvant dans des situations différentes ; que, dès lors, l'arrêté attaqué, qui ne distingue pas, pour la fixation des dates des cinq dimanches travaillés, selon que les entreprises comptent ou non plusieurs établissements, ne porte par lui-même aucune atteinte au principe d'égalité ; <br/>
<br/>
              9. Considérant, en dernier lieu, que le détournement de pouvoir allégué n'est pas établi ;<br/>
<br/>
              10. Considérant qu'il résulte de ce qui précède que la société Meubles Gimazane n'est pas fondée à soutenir que c'est à tort que le tribunal administratif de Caen a rejeté ses conclusions tendant à l'annulation de l'arrêté du préfet de la Manche du 9 avril 2009 ;<br/>
<br/>
              11. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'il soit fait droit aux conclusions de la société Meubles Gimazane présentées à ce titre ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions de la chambre régionale du négoce de l'ameublement et de l'équipement de la maison de Basse-Normandie présentées au même titre ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'intervention de Me A...et de la SELARL Bruno Cambon est admise.<br/>
Article 2 : L'arrêt de la cour administrative d'appel de Nantes du 20 mars 2015 est annulé. <br/>
Article 3 : La requête présentée par la société Meubles Gimazane devant la cour administrative d'appel de Nantes est rejetée.<br/>
Article 4 : Les conclusions de la société Meubles Gimazane et de la chambre régionale du négoce de l'ameublement et de l'équipement de la maison de Basse-Normandie présentées devant le Conseil d'Etat au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à la société Meubles Gimazane, à la chambre régionale du négoce de l'ameublement et de l'équipement de la maison de Basse-Normandie, à  MeA..., à la SELARL Bruno Cambon et à la ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social<br/>
Copie en sera adressée aux unions régionales Basse-Normandie de la CFTC, de la CFE-CGC, de la CFDT, de la CGT et de la CGT-Force ouvrière.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">66-03-02-02 TRAVAIL ET EMPLOI. CONDITIONS DE TRAVAIL. REPOS HEBDOMADAIRE. FERMETURE HEBDOMADAIRE DES ÉTABLISSEMENTS. - FERMETURE DES ÉTABLISSEMENTS D'UNE PROFESSION DANS UN SECTEUR GÉOGRAPHIQUE PAR ARRÊTÉ PRÉFECTORAL (ART. L. 3132-29 DU CODE DU TRAVAIL) - 1) CONDITION - ACCORD CORRESPONDANT À LA VOLONTÉ DE LA MAJORITÉ DES ÉTABLISSEMENTS [RJ1] - 2) POSSIBILITÉ DE S'ASSURER DE CETTE MAJORITÉ PAR UN QUESTIONNAIRE ENVOYÉ AUX ÉTABLISSEMENTS ET PRÉVOYANT QUE L'ABSENCE DE RÉPONSE VAUT ASSENTIMENT - EXISTENCE EN L'ESPÈCE.
</SCT>
<ANA ID="9A"> 66-03-02-02 1) Il résulte de l'article L. 3132-29 du code du travail que la fermeture au public des établissements d'une profession ne peut légalement être ordonnée, par arrêté préfectoral, sur la base d'un accord syndical que dans la mesure où cet accord correspond pour la profession à la volonté de la majorité indiscutable de tous ceux qui exercent cette profession à titre principal ou accessoire dans la zone géographique considérée et dont l'établissement ou une partie de celui-ci est susceptible d'être fermé. L'existence de cette majorité est vérifiée lorsque les entreprises adhérentes à la ou aux organisations d'employeurs qui ont signé l'accord ou s'y sont déclarées expressément favorables exploitent la majorité des établissements intéressés ou que la consultation de l'ensemble des entreprises concernées a montré que l'accord recueillait l'assentiment d'un nombre d'entreprises correspondant à la majorité des établissements intéressés.... ,,2) En l'espèce, la seule organisation professionnelle localement représentative des employeurs dans un secteur professionnel, avant de conclure un accord avec les organisations de salariés, a adressé un questionnaire portant sur le principe d'une fermeture dominicale assortie d'une ouverture quelques dimanches par an, alors que le secteur en cause est admis de plein droit à donner le repos hebdomadaire par roulement. Le questionnaire indiquait que l'absence de réponse vaudrait acceptation des décisions prises par la chambre régionale. 16 établissements ont répondu favorablement, 4 défavorablement et 21 n'ont pas répondu. Compte tenu de ces modalités de consultation, permettant à tous les établissements d'exprimer leur position en mentionnant clairement les conséquences de leur abstention, et alors qu'il n'est pas contesté que tous ces établissements ont reçu le courrier, les 21 établissements n'ayant pas répondu doivent être regardés, dans les circonstances particulières de l'espèce, comme ayant exprimé un avis favorable. Ainsi, la condition de majorité indiscutable en faveur de l'accord, qui s'établissait à vingt-et-un, était satisfaite.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Section, 12 novembre 1938, Sieur Milhac, p. 841 ; CE, 14 avril 1976, Chambre syndicale nationale du commerce et de la réparation automobile (CSNCRA) et Labrousse, n° 94387, p.203.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
