<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029621943</ID>
<ANCIEN_ID>JG_L_2014_10_000000368904</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/62/19/CETATEXT000029621943.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème / 4ème SSR, 22/10/2014, 368904, Publié au recueil Lebon</TITRE>
<DATE_DEC>2014-10-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>368904</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème / 4ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>LE PRADO ; SCP GATINEAU, FATTACCINI ; SCP DIDIER, PINET</AVOCATS>
<RAPPORTEUR>Mme Leïla Derouich</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:368904.20141022</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 29 mai et 28 août 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour le centre hospitalier de Dinan, dont le siège est rue Chateaubriand BP 56 à Dinan (22100), représenté par son directeur en exercice ; le centre hospitalier demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 12NT00297 du 28 mars 2013 par lequel la cour administrative d'appel de Nantes a rejeté son appel contre les jugements avant-dire-droit des 2 octobre 2008 et 30 décembre 2010 du tribunal administratif de Rennes ordonnant un complément d'expertise médicale ainsi qu'une expertise comptable afin de déterminer le montant des indemnités et d'évaluer les frais futurs dus en réparation des préjudices subis par les consorts B...en raison des fautes commises par le centre hospitalier lors de la naissance du jeune C...;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ainsi que le premier protocole additionnel à cette convention ;  <br/>
<br/>
              Vu le code de la santé publique ;<br/>
<br/>
              Vu le code de la sécurité sociale ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Leïla Derouich, auditeur,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Le Prado, avocat du centre hospitalier de Dinan, à la SCP Didier, Pinet, avocat de M. et Mme B...et à la SCP Gatineau, Fattaccini, avocat de la caisse primaire d'assurance maladie d'Ille et Vilaine ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que C...B..., né le 2 mars 1984 au centre hospitalier de Dinan, est demeuré atteint d'un grave handicap lié aux conditions dans lesquelles l'accouchement s'est déroulé ; que, par un jugement du 30 décembre 1992, le tribunal administratif de Rennes a estimé que les médecins avaient commis une faute en tardant à pratiquer une césarienne en dépit de signes de souffrance foetale et que ce retard avait " fait perdre à l'enfant les chances de récupération qui pouvaient exister " ; que le tribunal a déclaré le centre hospitalier " entièrement responsable des conséquences du retard à l'extraction de l'enfant " et ordonné une expertise afin d'évaluer ces conséquences ; que, par un jugement du 20 avril 1994, il a, d'une part, en attendant la date à laquelle l'indemnité définitive pourrait être fixée, à l'âge de dix-huit ans, alloué à l'enfant une rente annuelle de 200 000 F, et, d'autre part, attribué des indemnités à ses parents au titre de leurs préjudices propres et à la caisse de mutualité sociale agricole d'Ille-et-Vilaine au titre de ses débours ; que, saisi après la majorité de C...B...d'une action tendant à une indemnisation définitive, le même tribunal administratif a, par un jugement du 2 octobre 2008, ordonné une expertise afin de déterminer les préjudices subis par l'enfant depuis sa naissance ainsi que les versements dont il avait bénéficié ; que, par un jugement du 30 décembre 2010, le tribunal a constaté que l'expertise ne s'était pas déroulée dans des conditions régulières s'agissant de la détermination des sommes versées, ordonné qu'elle soit reprise sur ce point et rejeté une demande du centre hospitalier tendant à ce que son objet soit étendu à la détermination de l'ampleur de la perte de chance ayant résulté de la réalisation tardive de la césarienne ; que le centre hospitalier général de Dinan se pourvoit en cassation contre l'arrêt du 28 mars 2013 par lequel la cour administrative d'appel de Nantes a rejeté son appel contre les jugements des 2 octobre 2008 et 30 décembre 2010 ;<br/>
<br/>
              2. Considérant que, dans le cas où la faute commise lors de la prise en charge ou le traitement d'un patient dans un établissement public hospitalier a compromis ses chances d'obtenir une amélioration de son état de santé ou d'échapper à son aggravation, le préjudice résultant directement de la faute commise par l'établissement et qui doit être intégralement réparé n'est pas le dommage corporel constaté mais la perte de chance d'éviter la survenue de ce dommage ; que la réparation qui incombe à l'hôpital doit alors être évaluée à une fraction du dommage corporel déterminée en fonction de l'ampleur de la chance perdue ; qu'alors même que cette règle de réparation a été dégagée par une jurisprudence postérieure au jugement du 30 décembre 1992, devenu définitif, constatant que le retard apporté à la réalisation d'une césarienne a fait perdre à C...B...ses chances de récupération et déclarant le centre hospitalier de Dinan entièrement responsable des conséquences de ce retard, et au jugement du 20 avril 1994, également devenu définitif, mettant à la charge de l'établissement le versement à l'enfant d'une rente annuelle de 200 000 F dans l'attente de son indemnisation définitive à l'âge de dix-huit ans, elle doit être mise en oeuvre pour procéder à cette indemnisation définitive ; qu'en estimant que l'autorité s'attachant au jugement du 30 décembre 1992 implique que l'intégralité du dommage résultant pour C...B...du handicap dont il est atteint soit regardée comme la conséquence directe du retard apporté à la réalisation d'une césarienne et soit, par suite, prise en charge par le centre hospitalier de Dinan, et en en déduisant qu'il n'y avait pas lieu d'étendre l'expertise à la détermination de l'ampleur de la chance de récupération perdue du fait de ce retard, la cour administrative d'appel de Nantes a commis une erreur de droit ; <br/>
<br/>
              3. Considérant, toutefois, que les requérants sont fondés à se prévaloir des  stipulation de l'article 1er du premier protocole additionnel à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, pour soutenir que la détermination du préjudice indemnisable en fonction de l'ampleur de la chance perdue ne saurait avoir pour effet, en l'espèce, afin de préserver le droit au respect des biens de C...B..., de remettre en cause les sommes versées sous forme de rente par le centre hospitalier de Dinan en application du jugement du 20 avril 1994, qui doivent être regardées comme définitivement acquises et ne pourront être déduites de l'évaluation faite de ses droits ; <br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède que l'arrêt de la cour administrative d'appel de Nantes du 28 mars 2013 doit être annulé ; que les dispositions de l'article L. 761-1 du code de justice administrative  font obstacle à ce que soient mises à la charge du centre hospitalier de Dinan les sommes que demandent M. et Mme  B...et la caisse primaire d'assurance maladie d'Ille et Vilaine au titre des frais exposés par eux et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nantes du 28 mars 2013 est annulé.<br/>
<br/>
		Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nantes.<br/>
<br/>
Article 3 : Les conclusions de M. et Mme B...et de la  caisse primaire d'assurance maladie d'Ille-et-Vilaine présentées au titre de l'article L. 761-1 du code de justice sont rejetées.<br/>
<br/>
Article 4 : La présente décision sera notifiée au centre hospitalier de Dinan, à Mme D... B...et M. A...B..., à la caisse primaire d'assurance maladie d'Ille-et-Vilaine, à la caisse de mutualité sociale agricole d'Ille-et-Vilaine et au département d'Ille-et-Vilaine.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-07-01 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. QUESTIONS GÉNÉRALES. - APPLICATION D'UNE NOUVELLE RÈGLE JURISPRUDENTIELLE AUX INSTANCES EN COURS - 1) EXISTENCE EN PRINCIPE - 2) LIMITE - ATTEINTE, EN L'ESPÈCE, À UN BIEN PROTÉGÉ PAR LE 1ER PROTOCOLE ADDITIONNEL À LA CEDH.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">60-04-03 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RÉPARATION. ÉVALUATION DU PRÉJUDICE. - RESPONSABILITÉ POUR FAUTE MÉDICALE - FAUTE AYANT COMPROMIS LES CHANCES D'OBTENIR UNE AMÉLIORATION DE L'ÉTAT DE SANTÉ DU PATIENT OU D'ÉCHAPPER À SON AGGRAVATION - 1) RÉPARATION DE LA PERTE DE CHANCE D'ÉVITER LE DOMMAGE - APPLICATION DE LA DÉCISION OPÉRANT LE REVIREMENT DE JURISPRUDENCE AUX INSTANCES EN COURS - EXISTENCE EN PRINCIPE, SANS QU'Y FASSE OBSTACLE LE FAIT QU'UN JUGEMENT AIT DÉFINITIVEMENT RECONNU LA FAUTE MÉDICALE ANTÉRIEUREMENT AU REVIREMENT DE JURISPRUDENCE DÈS LORS QUE CE JUGEMENT N'AVAIT PAS PROCÉDÉ À L'ÉVALUATION DU PRÉJUDICE [RJ1] - 2) LIMITE - ATTEINTE, EN L'ESPÈCE, À UN BIEN PROTÉGÉ PAR LE 1ER PROTOCOLE ADDITIONNEL À LA CEDH [RJ2].
</SCT>
<ANA ID="9A"> 54-07-01 1) Il appartient en principe au juge administratif de faire application d'une règle jurisprudentielle nouvelle à l'ensemble des litiges, quelle que soit la date des faits qui leur ont donné naissance.... ,,En l'espèce, il y a lieu d'appliquer la règle jurisprudentielle selon laquelle, lorsque la faute commise lors de la prise en charge ou le traitement d'un patient dans un établissement public hospitalier a compromis ses chances d'obtenir une amélioration de son état de santé ou d'échapper à son aggravation, le préjudice résultant directement de la faute commise par l'établissement et qui doit être intégralement réparé n'est pas le dommage corporel constaté mais la perte de chance d'éviter la survenue de ce dommage, et ce alors même qu'en l'espèce un jugement de 1992 antérieur au revirement de jurisprudence et devenu définitif avait constaté qu'un retard apporté à la réalisation d'une césarienne avait fait perdre à la victime ses chances de récupération et déclaré l'établissement public hospitalier entièrement responsable des conséquences de ce retard. L'autorité de ce jugement implique que l'intégralité du dommage soit regardée comme la conséquence directe du retard fautif mais ne fait pas obstacle à l'application de la règle nouvelle d'évaluation du dommage.... ,,2) Cependant, les stipulations de l'article 1er du premier protocole additionnel à la convention EDH font obstacle à ce que soient remises en cause les sommes déjà versées, en attendant la fixation définitive de l'indemnité, calculées à l'époque sur la base de la règle jurisprudentielle antérieure prévoyant la réparation intégrale du dommage corporel. En l'espèce, les sommes versées au titre de la rente allouée par un jugement de 1994 en attendant la date à laquelle l'indemnité définitive pourrait être fixée, qui doivent être regardées comme définitivement acquises, ne pourront être déduites de l'évaluation faite des droits de la victime à réparation d'une perte de chance.</ANA>
<ANA ID="9B"> 60-04-03 1) Il appartient en principe au juge administratif de faire application d'une règle jurisprudentielle nouvelle à l'ensemble des litiges, quelle que soit la date des faits qui leur ont donné naissance.... ,,En l'espèce, il y a lieu d'appliquer la règle jurisprudentielle selon laquelle, lorsque la faute commise lors de la prise en charge ou le traitement d'un patient dans un établissement public hospitalier a compromis ses chances d'obtenir une amélioration de son état de santé ou d'échapper à son aggravation, le préjudice résultant directement de la faute commise par l'établissement et qui doit être intégralement réparé n'est pas le dommage corporel constaté mais la perte de chance d'éviter la survenue de ce dommage, et ce alors même qu'en l'espèce un jugement de 1992 antérieur au revirement de jurisprudence et devenu définitif avait constaté qu'un retard apporté à la réalisation d'une césarienne a fait perdre à la victime ses chances de récupération et déclaré l'établissement public hospitalier entièrement responsable des conséquences de ce retard. L'autorité de ce jugement implique que l'intégralité du dommage soit regardée comme la conséquence directe du retard fautif mais ne fait pas obstacle à l'application de la règle nouvelle d'évaluation du dommage.... ,,2) Cependant, les stipulations de l'article 1er du premier protocole additionnel à la convention EDH font obstacle à ce que soit remises en cause les sommes déjà versées, en attendant la fixation définitive de l'indemnité, calculées à l'époque sur la base de la règle jurisprudentielle antérieure prévoyant la réparation intégrale du dommage corporel. En l'espèce, les sommes versées au titre de la rente allouée par un jugement de 1994 en attendant la date à laquelle l'indemnité définitive pourrait être fixée, qui doivent être regardées comme définitivement acquises, ne pourront être déduites de l'évaluation faite des droits de la victime à réparation d'une perte de chance.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf CE, 2 septembre 2009, Assistance publique de Marseille, n° 297013, T. p. 917. Comp., pour une espèce où l'article 1er du premier protocole de la convention EDH n'était pas invoqué, CE, 15 mars 2013, M. Moret, n° 337496, inédit au Recueil., ,[RJ2] Comp., s'agissant des règles générales de l'indemnisation définitive du préjudice d'un enfant né atteinte d'une informité à la suite d'une faute d'un établissement public d'hospitalisation lors de l'accouchement CE, 11 mai 2007, M. Bouvard et autres, n° 281702, p. 204.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
