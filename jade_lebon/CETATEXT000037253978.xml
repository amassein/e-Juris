<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037253978</ID>
<ANCIEN_ID>JG_L_2018_07_000000411870</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/25/39/CETATEXT000037253978.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 26/07/2018, 411870</TITRE>
<DATE_DEC>2018-07-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>411870</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>M. Pierre Lombard</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:411870.20180726</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B...A...a demandé au tribunal administratif de Nîmes d'annuler la décision implicite par laquelle le recteur de l'académie d'Aix-Marseille a rejeté sa demande du 12 juin 2012, d'enjoindre au recteur d'établir de nouveaux états de service prenant en compte la réduction de service d'une heure dite " heure de laboratoire " pour les années 2007-2008 à 2013-2014 et de condamner l'Etat à lui verser les sommes de 7 492,88 euros au titre de son préjudice matériel et de 2 000 euros au titre de son préjudice moral, ainsi que les intérêts de droit à compter du 1er septembre 2007 et la capitalisation de ces intérêts.<br/>
<br/>
              Par un jugement n° 1301044 du 18 juin 2015, le tribunal administratif de Nîmes a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 15MA03286 du 28 avril 2017, la cour administrative d'appel de Marseille a, après avoir annulé ce jugement " en tant qu'il [était] contraire à ses article 1er et 2 ", annulé la décision par laquelle le recteur de l'académie d'Aix Marseille a refusé de calculer la rémunération de Mme A...en tenant compte d'une " heure de laboratoire " du mois de septembre 2007 au mois d'août 2014, et condamné l'Etat à lui verser la somme de 7 498,88 euros.<br/>
<br/>
              Par un pourvoi et un mémoire en réplique, enregistrés le 27 juin 2017 et le 21 juin 2018 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'éducation nationale demande au Conseil d'Etat d'annuler cet arrêt.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'éducation ;<br/>
              - le décret n° 50-581 du 25 mai 1950 ;<br/>
              - le décret n° 60-745 du 28 juillet 1960 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Pierre Lombard, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de Mme A...;<br/>
<br/>
              Vu la note en délibéré, enregistrée  le 13 juillet 2018, présentée par Mme A... ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que MmeA..., enseignant contractuel dans la matière des sciences de la vie et de la terre au collège-lycée de l'Immaculée Conception, établissement d'enseignement privé sous contrat avec l'Etat, a saisi le recteur de l'académie d'Aix Marseille d'une demande tendant à l'attribution, dans la dotation horaire académique, d'une heure de décharge de service dite " heure de laboratoire " et au paiement de cette heure pour les années précédentes. Elle a demandé au tribunal administratif de Nîmes d'annuler la décision implicite de rejet de sa demande et de condamner l'Etat au paiement des heures supplémentaires accomplies au titre de la décharge de service qu'elle réclamait pour les années scolaires 2007-2008 à 2013-2014. Par un jugement du 18 juin 2015, le tribunal administratif a rejeté sa demande. Par un arrêt du 28 avril 2017, la cour administrative d'appel de Marseille a partiellement annulé ce jugement et fait droit à la demande de Mme A...tendant à ce que l'Etat soit condamné au paiement des heures supplémentaires accomplies au titre de la décharge de service dite " heure de laboratoire " pour les années 2007-2008 à 2013-2014. Le ministre chargé de l'éducation nationale se pourvoit en cassation contre cet arrêt.<br/>
<br/>
              2. D'une part, aux termes de l'article L. 442-5 du code de l'éducation : " Les établissements d'enseignement privés du premier et du second degré peuvent demander à passer avec l'Etat un contrat d'association à l'enseignement public (...). Dans les classes faisant l'objet du contrat, l'enseignement est dispensé selon les règles et programmes de l'enseignement public. Il est confié, en accord avec la direction de l'établissement, soit à des maîtres de l'enseignement public, soit à des maîtres liés à l'Etat par contrat. Ces derniers, en leur qualité d'agent public, ne sont pas, au titre des fonctions pour lesquelles ils sont employés et rémunérés par l'Etat, liés par un contrat de travail à l'établissement au sein duquel l'enseignement leur est confié (...) ". En vertu des dispositions, alors applicables, de l'article 8 du décret du 25 mai 1950 portant règlement d'administration publique pour la fixation des maximums de service hebdomadaire du personnel enseignant des établissements d'enseignement du second degré, dans les établissements où n'existe ni professeur attaché au laboratoire ni agent de service affecté au laboratoire, le maximum de service des professeurs qui donnent au moins huit heures d'enseignement en sciences physiques ou en sciences naturelles est abaissé d'une heure.<br/>
<br/>
              3. D'autre part, aux termes de l'article 10 du décret du 28 juillet 1960 relatif aux conditions financières de fonctionnement (personnel et matériel) des classes sous contrat d'association, dont les dispositions ont été, depuis le 29 décembre 2008, codifiées à l'article R. 914-85 du code de l'éducation : " Les heures supplémentaires assurées sur autorisation de l'autorité académique pour les enseignements compris dans les programmes de l'enseignement public sont payées au taux en vigueur pour le personnel correspondant de l'enseignement public dans les mêmes conditions que la rémunération principale. / Les autorités académiques peuvent autoriser le paiement d'heures de suppléance et, à titre exceptionnel dans la limite de 10 % des heures d'enseignement données dans l'ensemble des classes sous contrat d'un établissement, le paiement d'heures d'enseignement partiel. Les services partiels d'enseignement, inférieurs à un demi-service, assurés par les maîtres chargés des fonctions de direction d'établissement et de formation sont également inclus dans la limite de ces 10 %. / Ces heures [peuvent] être assurées, à la demande du chef d'établissement et sur autorisation de l'autorité académique, par des maîtres appartenant au secteur privé de l'établissement, par du personnel chargé à titre principal de fonctions de surveillance, d'administration ou de direction ou par toutes autres personnes dès lors que celles-ci possèdent les titres requis des maîtres auxiliaires des établissements d'enseignement public. Elles [sont] rémunérées au taux correspondant aux titres des intéressés ".<br/>
<br/>
              4. Il résulte de ces dispositions que l'Etat est tenu de prendre en charge la rémunération à laquelle ont droit, après service fait, les enseignants des établissements privés sous contrat et qui comprend les mêmes éléments que celle des enseignants de l'enseignement public ainsi que les avantages et indemnités dont ceux-ci bénéficient. Cette obligation trouve à s'appliquer à l'égard des enseignants qui bénéficient de décharges d'activité. Toutefois, il n'appartient pas à l'Etat de prendre en charge la rémunération des heures supplémentaires effectuées, au-delà des obligations de service, à la demande du directeur d'un établissement d'enseignement privé dès lors qu'elles n'ont pas fait l'objet d'une autorisation de l'autorité académique.<br/>
<br/>
              5. Il ressort des pièces du dossier soumis aux juges du fond que, dès lors que, au sein de l'établissement où elle enseignait, aucun personnel n'était attaché au laboratoire, Mme A... bénéficiait d'une heure de décharge d'activité, dite heure " de laboratoire ", réduisant ainsi ses obligations d'enseignement à 17 heures hebdomadaires. A la demande du chef de l'établissement de l'Immaculée Conception, elle a réalisé, sur la période en litige, un service d'enseignement de 18 heures ou 18 heures 30 hebdomadaires selon les années scolaires, dépassant par conséquent son obligation de service. Ce dépassement de son obligation de service correspondait ainsi à la réalisation d'heures supplémentaires effectuées à la demande du chef de l'établissement dans lequel elle enseignait sans que la réalisation de ces heures ait été autorisée par l'autorité académique. Il résulte ce qui a été dit au point 4 ci-dessus qu'il n'appartient pas à l'Etat de prendre en charge la rémunération de telles heures supplémentaires. Par suite, en jugeant que ces heures supplémentaires correspondaient à des heures " de laboratoire " qu'il appartenait à l'Etat de payer et en condamnant, par voie de conséquence, l'Etat à les payer pour les années 2007-2008 à 2013-2014, la cour a commis une erreur de droit. Le ministre est fondé, dès lors, à demander l'annulation de l'arrêt attaqué. Par suite, le pourvoi incident de Mme A... à est devenu sans objet et il n'y a pas lieu d'y statuer.<br/>
<br/>
              6. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante. <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Marseille du 28 avril 2017 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Marseille.<br/>
Article 3 : Il n'y a pas lieu de statuer sur le pourvoi incident de Mme A...demandant l'annulation de l'arrêt de la cour administrative d'appel de Marseille du 28 avril 2017 en tant qu'elle a a omis de statuer sur ses conclusions tendant à ce que l'Etat soit condamné à lui verser la somme de 2 000 euros au titre de son préjudice moral. <br/>
Article 4 : Les conclusions de Mme A...présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée au ministre de l'éducation nationale et à Mme B...A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">30-02-07-01 ENSEIGNEMENT ET RECHERCHE. QUESTIONS PROPRES AUX DIFFÉRENTES CATÉGORIES D'ENSEIGNEMENT. ÉTABLISSEMENTS D'ENSEIGNEMENT PRIVÉS. PERSONNEL. - PRISE EN CHARGE PAR L'ETAT DE LA RÉMUNÉRATION DES ENSEIGNANTS DES ÉTABLISSEMENTS D'ENSEIGNEMENT PRIVÉS SOUS CONTRAT (ART. L. 442-5 DU CODE DE L'ÉDUCATION) - HEURES SUPPLÉMENTAIRES (ART. R. 914-85 DU MÊME CODE) - HEURES EFFECTUÉES AU-DELÀ DES OBLIGATIONS DE SERVICE À LA DEMANDE DU DIRECTEUR, N'AYANT PAS FAIT L'OBJET D'UNE AUTORISATION DE L'AUTORITÉ ACADÉMIQUE - EXCLUSION.
</SCT>
<ANA ID="9A"> 30-02-07-01 Il résulte de l'article L. 442-5 du code de l'éducation et de l'article 10 du décret n° 60-745 du 28 juillet 1960 relatif aux conditions financières de fonctionnement (personnel et matériel) des classes sous contrat d'association, dont les dispositions sont désormais codifiées à l'article R. 914-85 du code de l'éducation, que l'Etat est tenu de prendre en charge la rémunération à laquelle ont droit, après service fait, les enseignants des établissements privés sous contrat et qui comprend les mêmes éléments que celle des enseignants de l'enseignement public ainsi que les avantages et indemnités dont ceux-ci bénéficient. Cette obligation trouve à s'appliquer à l'égard des enseignants qui bénéficient de décharges d'activité. Toutefois, il n'appartient pas à l'Etat de prendre en charge la rémunération des heures supplémentaires effectuées, au-delà des obligations de service, à la demande du directeur d'un établissement d'enseignement privé dès lors qu'elles n'ont pas fait l'objet d'une autorisation de l'autorité académique.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
