<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043546729</ID>
<ANCIEN_ID>JG_L_2021_05_000000439199</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/54/67/CETATEXT000043546729.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 6ème chambres réunies, 27/05/2021, 439199</TITRE>
<DATE_DEC>2021-05-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>439199</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 6ème chambres réunies</FORMATION>
<TYPE_REC/>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DE NERVO, POUPET ; SCP GOUZ-FITOUSSI</AVOCATS>
<RAPPORTEUR>Mme Flavie Le Tallec</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:439199.20210527</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B... A... a demandé au tribunal administratif de Nancy de condamner l'Agence nationale des titres sécurisés (ANTS) à lui verser la somme de 4 667,83 euros en réparation des préjudices qu'il estime avoir subis. Par un jugement n° 1803208 du 30 décembre 2019, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire, un mémoire en réplique et un nouveau mémoire, enregistrés les 28 février et 24 août 2020 et les 19 février et 23 mars 2021 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Agence nationale des titres sécurisés la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la route ;<br/>
              - le code des relations entre le public et l'administration ;<br/>
              - le décret n° 2007-240 du 22 février 2007 ;<br/>
              - l'arrêté du 9 février 2009 relatif aux modalités d'immatriculation des véhicules ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Flavie Le Tallec, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP de Nervo, Poupet, avocat de M. A... et à la SCP Gouz-Fitoussi, avocat de l'Agence nationale des titres sécurisés ;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. L'article R. 322-5 du code de la route prévoit que l'acquéreur d'un véhicule déjà immatriculé doit, s'il veut le maintenir en circulation, faire établir, dans le mois qui suit l'acquisition, un certificat d'immatriculation à son nom en adressant sa demande au ministre de l'intérieur, soit directement par voie électronique, soit par l'intermédiaire d'un professionnel habilité. L'article 11 de l'arrêté du 9 février 2009 relatif aux modalités d'immatriculation des véhicules dispose, dans sa version applicable, que l'acquéreur qui adresse sa demande directement au ministre de l'intérieur doit s'authentifier sur le site internet de l'Agence nationale des titres sécurisés (ANTS) et qu'à l'issue du processus d'instruction de sa demande, il obtient un numéro de dossier, un accusé d'enregistrement et un certificat provisoire d'immatriculation.<br/>
<br/>
              2. Il ressort des pièces du dossier soumis aux juges du fond qu'ayant acquis un véhicule d'occasion le 14 décembre 2017, M. A... a adressé par la voie électronique, le 8 janvier 2018, dans le délai d'un mois qui lui était imparti, une demande de certificat d'immatriculation à son nom en se connectant au site internet de l'ANTS. L'accusé de réception qui lui a été retourné le 15 janvier 2018 mentionnait que sa demande avait été transmise pour traitement au service concerné. Après plusieurs semaines d'attente et diverses démarches, M. A... a constaté, le 19 avril 2018, que le site de l'ANTS sur lequel il avait enregistré sa demande mentionnait qu'aucune demande n'était en cours à son nom. Il a alors sollicité le certificat d'immatriculation par l'intermédiaire d'un professionnel habilité et l'a finalement obtenu le 7 mai 2018.<br/>
<br/>
              3. M. A..., après avoir demandé à l'ANTS de l'indemniser des préjudices qu'il estimait avoir subis du fait du retard mis à la délivrance de son titre, a saisi le tribunal administratif de Nancy d'une action indemnitaire tendant à ce que l'agence soit condamnée à lui verser, à ce titre, une somme de 4 667,83 euros, Il se pourvoit en cassation contre le jugement du 30 décembre 2019 par lequel le tribunal administratif de Nancy a rejeté sa demande comme étant mal dirigée, au motif que, l'instruction et la délivrance d'un certificat d'immatriculation relevant de la compétence du ministre de l'intérieur, seule la responsabilité de l'Etat était susceptible d'être engagée.<br/>
<br/>
              4. L'Agence nationale des titres sécurisés, établissement public national à caractère administratif placé sous la tutelle du ministre de l'intérieur, a pour mission, en vertu de l'article 2 du décret du 22 février 2007 l'ayant créée, dans sa version applicable au litige, de répondre aux besoins des administrations de l'Etat de conception, de gestion et de production de titres sécurisés, qui sont des documents délivrés par l'Etat et faisant l'objet d'une procédure d'édition et de contrôle sécurisée, ainsi que de la transmission des données qui leur sont associées. Selon les dispositions de cet article, l'agence est notamment chargée de : " 1° Assurer ou faire assurer, le développement, la maintenance et l'évolution des systèmes, des équipements et des réseaux informatiques permettant la gestion des titres sécurisés  / 2° Assurer ou faire assurer, la mise en oeuvre de services en ligne, de moyens d'identification électronique et de transmissions de données associée à la délivrance et à la gestion des titres sécurisés ; / 3° Procéder, pour le compte des administrations de l'Etat, aux achats des titres sécurisés ; / 4° Acquérir et mettre à disposition des administrations intéressées les matériels et équipements nécessaires à la gestion et au contrôle de l'authenticité et de la validité des titres sécurisés et en assurer la maintenance ; / 5° Mettre en oeuvre des actions d'information et de communication dans son domaine d'activité ; / 6° Développer et mettre en oeuvre des plates-formes d'échanges sécurisés des données dans le cadre du 1° et 2° ci-dessus ". Le même article dispose, en outre, d'une part, que l'agence accomplit sa mission dans le respect des orientations générales arrêtées par l'Etat en matière de titres sécurisés et dans le cadre de la coopération européenne et internationale et, d'autre part, que sa mission exclut l'instruction des demandes et la délivrance des titres. Cet article prévoit également que les modalités d'intervention pour le compte d'une administration de l'Etat sont précisées dans une convention. Enfin, selon la convention-cadre signée le 27 février 2017 entre le ministère de l'intérieur et l'agence, cette dernière est chargée d'assurer un soutien aux usagers par le centre de contacts citoyens et par des équipes de support.<br/>
<br/>
              5. Il résulte de l'ensemble de ces dispositions que, lorsqu'un usager demande à l'Etat la délivrance d'un titre sécurisé pour lequel l'Agence nationale des titres sécurisés exerce ses missions et qu'il doit, en conséquence, s'enregistrer sur la plate-forme de cet établissement public, les dysfonctionnements ou retards qui peuvent survenir à l'occasion des différentes étapes au cours desquelles, successivement, les données sont transmises par l'agence aux services de l'Etat, ceux-ci instruisent la demande et, si le titre est octroyé, l'agence assure son édition et son acheminement, tout en ayant en charge, tout au long du processus, un soutien à l'usager, peuvent avoir différentes causes, qui sont susceptibles d'engager, selon le cas, la responsabilité de l'agence ou celle de l'Etat mais dont l'usager n'est pas en mesure d'identifier l'auteur. <br/>
<br/>
              6. Par suite, lorsqu'un usager adresse une réclamation préalable à l'Agence nationale des titres sécurisés afin d'obtenir la réparation de préjudices qu'il estime avoir subis en raison de dysfonctionnements ou de retards lors de la délivrance, par cette agence, d'un titre sécurisé, cette réclamation doit être regardée comme adressée à la fois à l'agence et à l'Etat. Conformément aux dispositions de l'article L. 114-2 du code des relations entre le public et l'administration, cette réclamation doit être transmise par l'agence à l'autorité compétente de l'Etat, laquelle, en l'absence de réponse expresse de sa part, est réputée, en vertu de l'article L. 231-4 du même code, l'avoir implicitement rejetée à l'expiration du délai de deux mois suivant sa réception par l'agence.<br/>
<br/>
              7. En outre, il appartient au juge administratif, saisi d'une action indemnitaire de l'usager après le rejet d'une telle réclamation, de regarder des conclusions tendant à l'obtention de dommages et intérêts de la part de l'Agence nationale des titres sécurisés comme étant également dirigées contre l'Etat et de communiquer la requête tant à l'agence qu'à l'autorité compétente de l'Etat. <br/>
<br/>
              8. Il résulte de ce qui précède que M. A... est fondé à soutenir que la requête par laquelle il demandait au tribunal administratif de Nancy de condamner l'Agence nationale des titres sécurisés à l'indemniser des préjudices qu'il estimait avoir subis du fait du retard mis à traiter sa demande de certificat d'immatriculation aurait dû être regardée par le tribunal comme étant également dirigée contre l'Etat. Il est, par suite, fondé à demander l'annulation du jugement qu'il attaque.<br/>
<br/>
              9. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Agence nationale des titres sécurisés la somme de 3 000 euros à verser à M. A..., au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Nancy du 30 décembre 2019 est annulé. <br/>
<br/>
		Article 2 : L'affaire est renvoyée au tribunal administratif de Nancy. <br/>
<br/>
Article 3 : L'Agence nationale des titres sécurisés versera à M. A... la somme de 3 000 euros, au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à M. B... A..., à l'Agence nationale des titres sécurisés et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-04-03-01 PROCÉDURE. INSTRUCTION. CARACTÈRE CONTRADICTOIRE DE LA PROCÉDURE. COMMUNICATION DES MÉMOIRES ET PIÈCES. - DYSFONCTIONNEMENT DE L'ANTS - RÉGIME DES RÉCLAMATIONS PRÉALABLES ET DEMANDES INDEMNITAIRES [RJ1] - 1) RÉCLAMATION ADRESSÉE À L'ANTS SEULE - RÉCLAMATION DEVANT ÊTRE REGARDÉE COMME ADRESSÉE ÉGALEMENT À L'ETAT - CONSÉQUENCES - 2) RECOURS INDEMNITAIRE DIRIGÉ CONTRE L'ANTS SEULE - RECOURS DEVANT ÊTRE REGARDÉ COMME DIRIGÉ ÉGALEMENT CONTRE L'ETAT - CONSÉQUENCES.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-07-01-03-01 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. QUESTIONS GÉNÉRALES. CONCLUSIONS. INTERPRÉTATION DE LA REQUÊTE. - DYSFONCTIONNEMENT DE L'ANTS - RÉGIME DES RÉCLAMATIONS PRÉALABLES ET DEMANDES INDEMNITAIRES [RJ1] - 1) RÉCLAMATION ADRESSÉE À L'ANTS SEULE - RÉCLAMATION DEVANT ÊTRE REGARDÉE COMME ADRESSÉE ÉGALEMENT À L'ETAT - CONSÉQUENCES - 2) RECOURS INDEMNITAIRE DIRIGÉ CONTRE L'ANTS SEULE - RECOURS DEVANT ÊTRE REGARDÉ COMME DIRIGÉ ÉGALEMENT CONTRE L'ETAT - CONSÉQUENCES.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">60-03-02-02-04 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. PROBLÈMES D'IMPUTABILITÉ. PERSONNES RESPONSABLES. ÉTAT OU AUTRES COLLECTIVITÉS PUBLIQUES. ÉTAT OU ÉTABLISSEMENT PUBLIC. - DYSFONCTIONNEMENT DE L'ANTS - RÉGIME DES RÉCLAMATIONS PRÉALABLES ET DEMANDES INDEMNITAIRES [RJ1] - 1) RÉCLAMATION ADRESSÉE À L'ANTS SEULE - RÉCLAMATION DEVANT ÊTRE REGARDÉE COMME ADRESSÉE ÉGALEMENT À L'ETAT - CONSÉQUENCES - 2) RECOURS INDEMNITAIRE DIRIGÉ CONTRE L'ANTS SEULE - RECOURS DEVANT ÊTRE REGARDÉ COMME DIRIGÉ ÉGALEMENT CONTRE L'ETAT - CONSÉQUENCES.
</SCT>
<ANA ID="9A"> 54-04-03-01 Lorsqu'un usager demande à l'Etat la délivrance d'un titre sécurisé pour lequel l'Agence nationale des titres sécurisés (ANTS) exerce ses missions et qu'il doit, en conséquence, s'enregistrer sur la plate-forme de cet établissement public, les dysfonctionnements ou retards qui peuvent survenir à l'occasion des différentes étapes au cours desquelles, successivement, les données sont transmises par l'agence aux services de l'Etat, ceux-ci instruisent la demande et, si le titre est octroyé, l'agence assure son édition et son acheminement, tout en ayant en charge, tout au long du processus, un soutien à l'usager, peuvent avoir différentes causes, qui sont susceptibles d'engager, selon le cas, la responsabilité de l'agence ou celle de l'Etat mais dont l'usager n'est pas en mesure d'identifier l'auteur.,,,1) Par suite, lorsqu'un usager adresse une réclamation préalable à l'ANTS afin d'obtenir la réparation de préjudices qu'il estime avoir subis en raison de dysfonctionnements ou de retards lors de la délivrance, par cette agence, d'un titre sécurisé, cette réclamation doit être regardée comme adressée à la fois à l'agence et à l'Etat.,,,Conformément aux dispositions de l'article L. 114-2 du code des relations entre le public et l'administration (CRPA), cette réclamation doit être transmise par l'agence à l'autorité compétente de l'Etat, laquelle, en l'absence de réponse expresse de sa part, est réputée, en vertu de l'article L. 231-4 du même code, l'avoir implicitement rejetée à l'expiration du délai de deux mois suivant sa réception par l'agence.,,,2) En outre, il appartient au juge administratif, saisi d'une action indemnitaire de l'usager après le rejet d'une telle réclamation, de regarder des conclusions tendant à l'obtention de dommages et intérêts de la part de l'ANTS comme étant également dirigées contre l'Etat et de communiquer la requête tant à l'agence qu'à l'autorité compétente de l'Etat.</ANA>
<ANA ID="9B"> 54-07-01-03-01 Lorsqu'un usager demande à l'Etat la délivrance d'un titre sécurisé pour lequel l'Agence nationale des titres sécurisés (ANTS) exerce ses missions et qu'il doit, en conséquence, s'enregistrer sur la plate-forme de cet établissement public, les dysfonctionnements ou retards qui peuvent survenir à l'occasion des différentes étapes au cours desquelles, successivement, les données sont transmises par l'agence aux services de l'Etat, ceux-ci instruisent la demande et, si le titre est octroyé, l'agence assure son édition et son acheminement, tout en ayant en charge, tout au long du processus, un soutien à l'usager, peuvent avoir différentes causes, qui sont susceptibles d'engager, selon le cas, la responsabilité de l'agence ou celle de l'Etat mais dont l'usager n'est pas en mesure d'identifier l'auteur.,,,1) Par suite, lorsqu'un usager adresse une réclamation préalable à l'ANTS afin d'obtenir la réparation de préjudices qu'il estime avoir subis en raison de dysfonctionnements ou de retards lors de la délivrance, par cette agence, d'un titre sécurisé, cette réclamation doit être regardée comme adressée à la fois à l'agence et à l'Etat.,,,Conformément aux dispositions de l'article L. 114-2 du code des relations entre le public et l'administration (CRPA), cette réclamation doit être transmise par l'agence à l'autorité compétente de l'Etat, laquelle, en l'absence de réponse expresse de sa part, est réputée, en vertu de l'article L. 231-4 du même code, l'avoir implicitement rejetée à l'expiration du délai de deux mois suivant sa réception par l'agence.,,,2) En outre, il appartient au juge administratif, saisi d'une action indemnitaire de l'usager après le rejet d'une telle réclamation, de regarder des conclusions tendant à l'obtention de dommages et intérêts de la part de l'ANTS comme étant également dirigées contre l'Etat et de communiquer la requête tant à l'agence qu'à l'autorité compétente de l'Etat.</ANA>
<ANA ID="9C"> 60-03-02-02-04 Lorsqu'un usager demande à l'Etat la délivrance d'un titre sécurisé pour lequel l'Agence nationale des titres sécurisés (ANTS) exerce ses missions et qu'il doit, en conséquence, s'enregistrer sur la plate-forme de cet établissement public, les dysfonctionnements ou retards qui peuvent survenir à l'occasion des différentes étapes au cours desquelles, successivement, les données sont transmises par l'agence aux services de l'Etat, ceux-ci instruisent la demande et, si le titre est octroyé, l'agence assure son édition et son acheminement, tout en ayant en charge, tout au long du processus, un soutien à l'usager, peuvent avoir différentes causes, qui sont susceptibles d'engager, selon le cas, la responsabilité de l'agence ou celle de l'Etat mais dont l'usager n'est pas en mesure d'identifier l'auteur.,,,1) Par suite, lorsqu'un usager adresse une réclamation préalable à l'ANTS afin d'obtenir la réparation de préjudices qu'il estime avoir subis en raison de dysfonctionnements ou de retards lors de la délivrance, par cette agence, d'un titre sécurisé, cette réclamation doit être regardée comme adressée à la fois à l'agence et à l'Etat.,,,Conformément aux dispositions de l'article L. 114-2 du code des relations entre le public et l'administration (CRPA), cette réclamation doit être transmise par l'agence à l'autorité compétente de l'Etat, laquelle, en l'absence de réponse expresse de sa part, est réputée, en vertu de l'article L. 231-4 du même code, l'avoir implicitement rejetée à l'expiration du délai de deux mois suivant sa réception par l'agence.,,,2) En outre, il appartient au juge administratif, saisi d'une action indemnitaire de l'usager après le rejet d'une telle réclamation, de regarder des conclusions tendant à l'obtention de dommages et intérêts de la part de l'ANTS comme étant également dirigées contre l'Etat et de communiquer la requête tant à l'agence qu'à l'autorité compétente de l'Etat.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., s'agissant de Pôle Emploi, CE, 28 mai 2018, Mme,, n° 405448, p. 227 ; s'agissant d'une ARS, CE, 26 février 2020, Société Thessalie, n° 422344, T. pp. 601-990.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
