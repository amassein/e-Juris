<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034330381</ID>
<ANCIEN_ID>JG_L_2017_03_000000401069</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/33/03/CETATEXT000034330381.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 31/03/2017, 401069</TITRE>
<DATE_DEC>2017-03-31</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>401069</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELVOLVE ET TRICHET</AVOCATS>
<RAPPORTEUR>M. Clément Malverti</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Béatrice Bourgeois-Machureau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:401069.20170331</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme C...A...B...a demandé au tribunal administratif d'Orléans d'annuler la décision du 9 juin 2015 par laquelle le directeur des ressources humaines du groupe Orange a confirmé la décision du 30 janvier 2015 refusant de lui attribuer une augmentation individuelle de rémunération liée à la gestion prévisionnelle des emplois et des compétences dans le cadre de l'accord salarial Orange SA 2014. Par une ordonnance n° 1502683 du 30 décembre 2015, le président de la 1ère chambre du tribunal administratif d'Orléans a rejeté cette demande comme portée devant un ordre de juridiction incompétent pour en connaître.<br/>
<br/>
              Par une ordonnance n° 16NT00712 du 3 mai 2016, le président de la 4ème chambre de la cour administrative d'appel de Nantes a rejeté l'appel formé par la société Orange SA contre cette ordonnance.<br/>
<br/>
              Par un pourvoi, enregistré le 29 juin 2016 au secrétariat du contentieux du Conseil d'Etat, la société Orange SA demande au Conseil d'Etat d'annuler cette l'ordonnance du président de la 4ème chambre de la cour administrative d'appel de Nantes.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code du travail ;<br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - la loi n° 84-16 du 11 janvier 1984 ;<br/>
              - la loi n° 90-568 du 2 juillet 1990 ;<br/>
              - le code justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Clément Malverti, auditeur, <br/>
<br/>
              - les conclusions de Mme Béatrice Bourgeois-Machureau, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Delvolvé et Trichet, avocat de la société Orange SA ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant que, dès lors qu'elles ne portent pas sur des dispositions régissant l'organisation du service public, les contestations portant sur la légalité ou l'application et la dénonciation d'une convention collective ou d'un accord d'entreprise conclu en application des dispositions du livre II du code du travail relèvent de la compétence du juge judiciaire, sauf loi contraire et sous réserve qu'il ne soit pas manifeste, au vu d'une jurisprudence établie, qu'elles ne peuvent pas être accueillies ; que le juge administratif est, en revanche, seul compétent pour connaître des contestations relatives à l'application de ces conventions et accords à la situation individuelle des fonctionnaires, tels que les personnels de droit public de la société Orange SA et de La Poste qui sont régis, en vertu de l'article 29 de la loi du 2 juillet 1990, par des statuts particuliers pris en application des lois du 13 juillet 1983 portant droits et obligations des fonctionnaires et du 11 janvier 1984 portant dispositions statutaires relatives à la fonction publique de l'Etat ;<br/>
<br/>
              2.	Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la requête introduite devant le tribunal administratif d'Orléans par Mme A...B..., fonctionnaire en activité au sein de la société Orange SA, tendait à l'annulation de la décision par laquelle le directeur des ressources humaines du groupe Orange avait refusé de lui attribuer une augmentation individuelle liée à la gestion prévisionnelle des emplois et compétences en application des stipulations de l'article 7 de l'accord salarial pour l'année 2014 conclu le 18 avril 2014 ; qu'eu égard à la qualité de fonctionnaire de l'intéressée, cette contestation ressortit à la compétence de la juridiction administrative, alors même que la décision litigieuse a été prise en application des stipulations d'un accord d'entreprise, conclu sur le fondement des dispositions du livre II du code du travail et qui ne régit pas l'organisation du service public ; que, dès lors, le président de la 4ème chambre de la cour administrative d'appel de Nantes a commis une erreur de droit en jugeant que la demande présentée par Mme A...B...devant le tribunal administratif d'Orléans ne ressortissait pas à la compétence de la juridiction administrative ; que la société Orange SA est, par suite, fondée à demander l'annulation de l'ordonnance qu'elle attaque ;<br/>
<br/>
              3.	Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ; <br/>
<br/>
              4.	Considérant que, pour les motifs indiqués ci-dessus, la demande formée par Mme A...B...devant le tribunal administratif d'Orléans ressortit à la compétence de la juridiction administrative ; que, par suite, la société Orange SA est fondée à soutenir que c'est à tort que, par l'ordonnance attaquée, le président de la 1ère chambre du tribunal administratif d'Orléans a rejeté cette demande comme formée devant un ordre de juridiction incompétent pour en connaître ; <br/>
<br/>
              5.	Considérant que l'affaire n'est pas en état ; qu'il y a lieu de la renvoyer au tribunal administratif d'Orléans pour qu'il statue à nouveau sur la demande de Mme A...B...;<br/>
<br/>
              6.	Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées en appel par la société Orange au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'ordonnance du 3 mai 2016 du président de la 4ème chambre de la cour administrative d'appel de Nantes est annulée.<br/>
<br/>
Article 2 : L'ordonnance du 30 décembre 2015 du président de la 1ère chambre du tribunal administratif d'Orléans est annulée.<br/>
Article 3 : L'affaire est renvoyée au tribunal administratif d'Orléans.<br/>
<br/>
Article 4 : Le surplus des conclusions de la requête d'appel de la société Orange SA est rejeté.<br/>
<br/>
Article 5 : La présente décision sera notifiée à la société Orange SA et à Mme C...A...B....<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">17-03-02-03-01-01 COMPÉTENCE. RÉPARTITION DES COMPÉTENCES ENTRE LES DEUX ORDRES DE JURIDICTION. COMPÉTENCE DÉTERMINÉE PAR UN CRITÈRE JURISPRUDENTIEL. CONTRATS. CONTRATS DE DROIT PRIVÉ. CONTRATS CONCLUS ENTRE PERSONNES PRIVÉES. - CONVENTIONS COLLECTIVES ET ACCORDS COLLECTIFS PASSÉS PAR DES ÉTABLISSEMENTS PUBLICS INDUSTRIELS ET COMMERCIAUX OU DES ENTREPRISES À STATUTS [RJ1] - 1) CONTESTATIONS PORTANT SUR LA LÉGALITÉ OU L'APPLICATION ET LA DÉNONCIATION - COMPÉTENCE DU JUGE JUDICIAIRE, SAUF MESURE D'ORGANISATION DU SERVICE OU LOI CONTRAIRE - 2) CONTESTATIONS RELATIVES À L'APPLICATION DES CONVENTIONS COLLECTIVES ET ACCORDS D'ENTREPRISE À LA SITUATION INDIVIDUELLE DE FONCTIONNAIRES - COMPÉTENCE DU JUGE ADMINISTRATIF.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">17-03-02-04 COMPÉTENCE. RÉPARTITION DES COMPÉTENCES ENTRE LES DEUX ORDRES DE JURIDICTION. COMPÉTENCE DÉTERMINÉE PAR UN CRITÈRE JURISPRUDENTIEL. PERSONNEL. - CONTESTATIONS RELATIVES À L'APPLICATION DES CONVENTIONS COLLECTIVES ET ACCORDS D'ENTREPRISE À LA SITUATION INDIVIDUELLE DE FONCTIONNAIRES - COMPÉTENCE DU JUGE ADMINISTRATIF [RJ1].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">66-02 TRAVAIL ET EMPLOI. CONVENTIONS COLLECTIVES. - CONVENTIONS COLLECTIVES ET ACCORDS COLLECTIFS PASSÉS PAR DES ÉTABLISSEMENTS PUBLICS INDUSTRIELS ET COMMERCIAUX OU DES ENTREPRISES À STATUTS [RJ1] - 1) CONTESTATIONS PORTANT SUR LA LÉGALITÉ OU L'APPLICATION ET LA DÉNONCIATION - COMPÉTENCE DU JUGE JUDICIAIRE, SAUF MESURE D'ORGANISATION DU SERVICE OU LOI CONTRAIRE - 2) CONTESTATIONS RELATIVES À L'APPLICATION DES CONVENTIONS COLLECTIVES ET ACCORDS D'ENTREPRISE À LA SITUATION INDIVIDUELLE DE FONCTIONNAIRES - COMPÉTENCE DU JUGE ADMINISTRATIF.
</SCT>
<ANA ID="9A"> 17-03-02-03-01-01 1) Dès lors qu'elles ne portent pas sur des dispositions régissant l'organisation du service public, les contestations portant sur la légalité ou l'application et la dénonciation d'une convention collective ou d'un accord d'entreprise conclu en application des dispositions du livre II du code du travail relèvent de la compétence du juge judiciaire, sauf loi contraire et sous réserve qu'il ne soit pas manifeste, au vu d'une jurisprudence établie, qu'elles ne peuvent pas être accueillies.,,,2) Le juge administratif est, en revanche, seul compétent pour connaître des contestations relatives à l'application de ces conventions et accords à la situation individuelle des fonctionnaires, tels que les personnels de droit public de la société Orange SA et de La Poste, qui sont régis, en vertu de l'article 29 de la loi n° 90-568 du 2 juillet 1990, par des statuts particuliers pris en application des lois n° 83-634 du 13 juillet 1983 et n° 84-16 du 11 janvier 1984.</ANA>
<ANA ID="9B"> 17-03-02-04 Le juge administratif est seul compétent pour connaître des contestations relatives à l'application des conventions collectives et accords d'entreprise à la situation individuelle des fonctionnaires, tels que les personnels de droit public de la société Orange SA et de La Poste, qui sont régis, en vertu de l'article 29 de la loi n° 90-568 du 2 juillet 1990, par des statuts particuliers pris en application des lois n° 83-634 du 13 juillet 1983 et n° 84-16 du 11 janvier 1984.</ANA>
<ANA ID="9C"> 66-02 1) Dès lors qu'elles ne portent pas sur des dispositions régissant l'organisation du service public, les contestations portant sur la légalité ou l'application et la dénonciation d'une convention collective ou d'un accord d'entreprise conclu en application des dispositions du livre II du code du travail relèvent de la compétence du juge judiciaire, sauf loi contraire et sous réserve qu'il ne soit pas manifeste, au vu d'une jurisprudence établie, qu'elles ne peuvent pas être accueillies.,,,2) Le juge administratif est, en revanche, seul compétent pour connaître des contestations relatives à l'application de ces conventions et accords à la situation individuelle des fonctionnaires, tels que les personnels de droit public de la société Orange SA et de La Poste, qui sont régis, en vertu de l'article 29 de la loi n° 90-568 du 2 juillet 1990, par des statuts particuliers pris en application des lois n° 83-634 du 13 juillet 1983 et n° 84-16 du 11 janvier 1984.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Cf. TC, 15 décembre 2008, Voisin c/ RATP, n° 3662, p. 563 ; décision du même jour, Kim c/ Etablissement français du sang, n° 3652, T. pp. 647-950.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
