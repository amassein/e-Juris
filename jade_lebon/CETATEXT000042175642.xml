<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042175642</ID>
<ANCIEN_ID>JG_L_2020_07_000000423420</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/17/56/CETATEXT000042175642.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 29/07/2020, 423420</TITRE>
<DATE_DEC>2020-07-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>423420</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>GOLDMAN ; SCP BAUER-VIOLAS, FESCHOTTE-DESBOIS, SEBAGH</AVOCATS>
<RAPPORTEUR>Mme Pauline Berne</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Gabrielle Merloz</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:423420.20200729</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Mme B... A... a demandé au tribunal administratif d'Orléans d'annuler la décision du 26 mai 2014 par laquelle le président du service départemental d'incendie et de secours (SDIS) du Loiret a mis fin à l'attribution à son profit d'un logement de fonction en caserne et la décision du 22 septembre 2014 par laquelle le directeur du SDIS a refusé de lui attribuer le bénéfice de l'indemnité de logement. Par un jugement n° 1404349 du 24 mai 2016, le tribunal administratif d'Orléans a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 16NT02363 du 21 juin 2018, la cour administrative d'appel de Nantes a rejeté l'appel formé par Mme A... contre ce jugement en tant qu'il a rejeté ses conclusions dirigées contre la décision du 22 septembre 2014 du directeur du SDIS du Loiret lui refusant le bénéfice de l'indemnité de logement. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 20 août et 19 novembre 2018 au secrétariat du contentieux du Conseil d'Etat, Mme A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge du SDIS du Loiret la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi n° 83-634 du 13 juillet 1983 ; <br/>
              - la loi n° 84-53 du 26 janvier 1984 ;<br/>
              - le décret n° 90-850 du 25 septembre 1990 ;<br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Pauline Berne, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Marie-Gabrielle Merloz, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Goldman, avocat de Mme A... et à la SCP Bauer-Violas, Feschotte-Desbois, Sebagh, avocat du service départemental d'incendie et de secours du Loiret ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que Mme A..., caporal des sapeurs-pompiers du service départemental d'incendie et de secours (SDIS) du Loiret a, par lettre du 12 octobre 2012, informé sa hiérarchie de son souhait de quitter le logement de fonction en caserne qui était mis à sa disposition depuis 2010, ce dont il lui a été donné acte par arrêté du 26 mai 2014. Par lettre du 25 juillet 2014, Mme A... a sollicité le bénéfice de l'indemnité de logement instituée par l'article 6-6 du décret du 25 septembre 1990 portant dispositions communes à l'ensemble des sapeurs-pompiers professionnels. Par une décision du 22 septembre 2014, le directeur du SDIS a rejeté sa demande aux motifs que son conjoint, également sapeur-pompier du SDIS, avec qui elle résidait, percevait déjà cette indemnité. Par un jugement du 24 mai 2016, le tribunal administratif d'Orléans a notamment rejeté la demande d'annulation, présentée par Mme A..., de cette décision du 22 septembre 2014. Mme A... se pourvoit en cassation contre l'arrêt du 21 juin 2018 par lequel la cour administrative d'appel de Nantes a rejeté l'appel qu'elle a formé, dans cette mesure, contre ce jugement.<br/>
<br/>
              2. Aux termes de l'article 5 du décret du 25 septembre 1990 portant dispositions communes à l'ensemble des sapeurs-pompiers professionnels : " Les sapeurs-pompiers professionnels ont droit au logement en caserne dans la limite des locaux disponibles (...). Les sapeurs-pompiers professionnels peuvent également être logés à l'extérieur des casernements par nécessité absolue de service ". Aux termes de l'article 6-1 du même décret : " Le régime indemnitaire des sapeurs-pompiers professionnels est fixé par le conseil d'administration du service départemental d'incendie et de secours dans les limites déterminées aux articles suivants " et aux termes de l'article 6-2 : " (...) Le président du conseil d'administration détermine le taux individuel applicable à chaque sapeur-pompier professionnel ". Aux termes de l'article 6-6 de ce même décret : " Les sapeurs-pompiers professionnels non logés peuvent percevoir une indemnité de logement égale au maximum à 10 % du traitement augmenté de l'indemnité de résidence. Aucun officier, sous-officier ou gradé ne peut percevoir, à ce titre, une indemnité supérieure au double de l'indemnité d'un sapeur, 1er échelon. "<br/>
<br/>
              3. Il résulte de ces dispositions que le conseil d'administration du SDIS est compétent pour fixer le régime indemnitaire des sapeurs-pompiers professionnels, et notamment d'instaurer, dans les limites fixées à l'article 6-6 du décret, une indemnité de logement au bénéfice des sapeurs-pompiers professionnels qui ne sont pas logés, et que seule la détermination du taux individuel applicable à chaque sapeur-pompier de ce régime indemnitaire relève de la compétence du président du conseil d'administration du SDIS. Dans l'hypothèse où est instaurée une indemnité de logement, les dispositions de l'article 6-6 du décret du 25 septembre 1990 impliquent qu'elle doive être attribuée aux sapeurs-pompiers non logés par le service, que cette situation résulte de la décision du service ou du choix du sapeur-pompier. <br/>
<br/>
              4. Pris sur le fondement de ces dispositions, le règlement intérieur du SDIS du Loiret, approuvé par la délibération n° 2009-A7 de son conseil d'administration, prévoit à son article 64 que " bénéficient de l'indemnité de logement, les sapeurs-pompiers (professionnels, titulaires ou contractuels) non logés par le service dans les conditions prévues par l'article 6.6 du décret n° 90-850 du 25 septembre 1990 ". Cette disposition instaure ainsi une indemnité de logement ouverte, conformément au décret du 25 septembre 1990 et ainsi qu'il a été dit au point 3, à tous les sapeurs-pompiers non logés dans les limites de montant prévues à l'article 6-6 du décret. Par suite, en jugeant que cet article 64 du règlement intérieur du SDIS du Loiret n'avait ni pour objet ni pour effet de remettre en cause le caractère facultatif de l'attribution de l'indemnité de logement telle qu'elle est envisagée par le décret du 25 septembre 1990, la cour administrative d'appel a commis une erreur de droit. <br/>
<br/>
              5. Si le SDIS du Loiret demande, devant le Conseil d'Etat, que soit substitué au motif erroné retenu par les juges d'appel le motif tiré de ce que l'indemnité de logement ne serait que subsidiaire par rapport au droit au logement en caserne et que Mme A... ne pouvait prétendre à son bénéfice dès lors qu'elle avait renoncé à son logement en caserne, le bénéfice de l'indemnité de logement est, ainsi qu'il a été dit au point 3, ouvert à tous les sapeurs-pompiers non logés, quel que soit le motif de cette situation, dès lors que le conseil d'administration du SDIS en a décidé l'institution.<br/>
<br/>
              6. Il résulte de ce qui précède que, sans qu'il soit besoin de se prononcer sur l'autre moyen du pourvoi, Mme A... est fondée à demander l'annulation de l'arrêt de la cour administrative d'appel de Nantes qu'elle attaque. <br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du SDIS du Loiret le versement de la somme de 3 000 euros à Mme A... au titre de l'article L. 761-1 du code de justice administrative. Les dispositions de cet article font, en revanche, obstacle à ce qu'une somme soit mise à la charge de Mme A... qui n'est pas, dans la présente instance, la partie perdante. <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nantes est annulé.<br/>
<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nantes. <br/>
<br/>
Article 3 : Le service départemental d'incendie et de secours du Loiret versera à Mme A... une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : Les conclusions du service départemental d'incendie et de secours du Loiret présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
<br/>
Article 5: La présente décision sera notifiée Mme B... A... et au service départemental d'incendie et de secours du Loiret.<br/>
Copie en sera adressée au ministre de l'intérieur.<br/>
<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">135-01-04-02-03 COLLECTIVITÉS TERRITORIALES. DISPOSITIONS GÉNÉRALES. SERVICES PUBLICS LOCAUX. DISPOSITIONS PARTICULIÈRES. SERVICES D'INCENDIE ET SECOURS. - INDEMNITÉ DE LOGEMENT (ART. 6-6 DU DÉCRET DU 25 SEPTEMBRE 1990) - BÉNÉFICIAIRES - SAPEURS-POMPIERS NON LOGÉS PAR LE SDIS, QUEL QU'EN SOIT LE MOTIF.
</SCT>
<ANA ID="9A"> 135-01-04-02-03 Il résulte des articles 5, 6-1, 6-2 et 6-6 du décret n° 90-850 du 25 septembre 1990 que le conseil d'administration du service départemental d'incendie et de secours (SDIS) est compétent pour fixer le régime indemnitaire des sapeurs-pompiers professionnels, et notamment d'instaurer, dans les limites fixées à l'article 6-6 du décret, une indemnité de logement au bénéfice des sapeurs-pompiers professionnels qui ne sont pas logés, et que seule la détermination du taux individuel applicable à chaque sapeur-pompier de ce régime indemnitaire relève de la compétence du président du conseil d'administration du SDIS. Dans l'hypothèse où est instaurée une indemnité de logement, les dispositions de l'article 6-6 du décret du 25 septembre 1990 implique qu'elle doive être attribuée aux sapeurs-pompiers non logés par le service, que cette situation résulte de la décision du service ou du choix du sapeur-pompier.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
