<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031427877</ID>
<ANCIEN_ID>JG_L_2015_11_000000383303</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/42/78/CETATEXT000031427877.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème / 8ème SSR, 04/11/2015, 383303</TITRE>
<DATE_DEC>2015-11-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>383303</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème / 8ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOUTET, HOURDEAUX</AVOCATS>
<RAPPORTEUR>M. Christian Fournier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:383303.20151104</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Mme B...A...a demandé au tribunal administratif de Strasbourg l'annulation des titres exécutoires n° 2008/756 et n° 2008/758 émis le 14 mai 2008 à son encontre par l'Agence unique de paiement pour un montant total de 10 311,30 euros. Par un jugement n° 0804856 du 4 avril 2013, le tribunal administratif de Strasbourg a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 13NC01080 du 2 juin 2014, la cour administrative d'appel de Nancy a annulé le jugement n° 0804856 du 4 avril 2013 du tribunal administratif de Strasbourg ainsi que les titres exécutoires n° 2008/756 et n° 2008/758 émis le 14 mai 2008 par l'Agence unique de paiement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 31 juillet et 28 octobre 2014 au secrétariat du contentieux du Conseil d'Etat, l'Agence de services et de paiement demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de Mme A... ;<br/>
<br/>
              3°) de mettre à la charge de Mme A...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le règlement (CE) n° 1782/2003 du Conseil du 29 septembre 2003 ;<br/>
              - le règlement (CE) n° 796/2004 de la Commission du 21 avril 2004 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Christian Fournier, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boutet, Hourdeaux, avocat de l'Agence de services et de paiement  ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que Mme B...A...a déposé le 13 mai 2006 auprès des services de la direction départementale de l'agriculture et de la forêt (DDAF) de la Moselle un dossier de demande d'aide communautaire au titre de la campagne 2006 pour une surface totale de 68,08 hectares ; que, le 15 octobre 2007, un contrôle administratif a révélé qu'à la date de cette demande d'aide, une surface de 60,48 hectares, correspondant à une partie des surfaces au titre desquelles Mme A... avait déposé sa demande, avait déjà fait l'objet d'une demande d'aide communautaire par le gérant de la SCEA Saint-Blaise, dont Mme A...était associée ; que, le 10 janvier 2008, le préfet de la Moselle a signifié à Mme A...qu'elle ne pouvait déclarer personnellement la surface de 60,48 hectares et a transmis un rapport à l'Agence unique de paiement (AUP) aux fins de recouvrement des sommes indûment versées ; que l'Agence de services et de paiement, venue aux droits de l'AUP, se pourvoit en cassation contre l'arrêt du 2 juin 2014 par lequel la cour administrative d'appel de Nancy a annulé le jugement du 4 avril 2013 du tribunal administratif de Strasbourg rejetant la demande de Mme A...tendant à l'annulation des deux titres de perception émis à son encontre le 14 mai 2008 par l'AUP pour un montant total de 10 311,30 euros ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article 74 du règlement (CE) n° 796/2004 de la Commission du 21 avril 2004, pris pour l'application du règlement (CE) n° 1782/2003 du Conseil du 29 septembre 2003 établissant des règles communes de soutien direct dans le cadre de la politique agricole commune et établissant certains régimes de soutien en faveur des agriculteurs : " 1. Aux fins du présent article, on entend par : / a) " transfert d'une exploitation ", une opération de vente ou de location ou tout type de transaction semblable ayant pour objet les unités de production concernées, / b) " cédant ", l'agriculteur dont l'exploitation est transférée à un autre agriculteur, / c) " repreneur " l'agriculteur à qui l'exploitation est transférée / 2. Si une exploitation est transférée en totalité par un agriculteur à un autre après l'introduction d'une demande d'aide et avant que toutes les conditions d'octroi de l'aide n'aient été remplies, aucune aide n'est accordée au cédant pour l'exploitation transférée. / 3. L'aide demandée par le cédant est octroyée au repreneur pourvu : / a) qu'au terme d'une période à déterminer par les Etats membres, le repreneur informe l'autorité compétente du transfert et demande le paiement de l'aide, / b) que le repreneur fournisse toutes les pièces exigées par l'autorité compétente, / c) que toutes les conditions d'octroi de l'aide soient remplies en ce qui concerne l'exploitation transférée. / 4. Une fois que le repreneur a informé l'autorité compétente et demandé le paiement de l'aide conformément au paragraphe 3 point a) : / a) tous les droits et obligations du cédant résultant du rapport de droit généré par la demande d'aide entre le cédant et l'autorité compétente sont attribués au repreneur ; / (...) / 5. Si une demande d'aide est déposée après réalisation des actions nécessaires à l'octroi de l'aide et qu'une exploitation est transférée en totalité par un agriculteur à un autre après le début de ces actions mais avant que toutes les conditions d'octroi de l'aide n'aient été remplies, l'aide peut être accordée au repreneur pour autant que les conditions prévues au paragraphe 3, points a) et b), soient respectées. (...) " ; <br/>
<br/>
              3. Considérant qu'en jugeant que l'octroi des aides agricoles liées à la surface instituées par les règlements communautaires n'est subordonné qu'à la justification de l'exploitation effective des parcelles au titre desquelles l'aide est demandée, y compris lorsque ces parcelles ont fait l'objet d'un transfert entre agriculteurs, alors que les dispositions précitées prévoient, dans un tel cas, des obligations d'information de l'autorité compétente, la cour administrative d'appel a commis une erreur de droit ; que, par suite, l'Agence de services et de paiement est fondée, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, à demander l'annulation de l'arrêt qu'elle attaque ;<br/>
<br/>
              4. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de Mme A...la somme que demande l'Agence de services et de paiement au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt en date du 2 juin 2014 de la cour administrative d'appel de Nancy est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nancy.<br/>
Article 3 : Les conclusions présentées par l'Agence de services et de paiement au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à l'Agence de services et de paiement et à Mme B...A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">03-03-06 AGRICULTURE ET FORÊTS. EXPLOITATIONS AGRICOLES. AIDES DE L'UNION EUROPÉENNE. - AIDES AGRICOLES LIÉES À LA SURFACE - OCTROI SUBORDONNÉ À LA SEULE JUSTIFICATION DE L'EXPLOITATION EFFECTIVE DES PARCELLES AU TITRE DESQUELLES L'AIDE EST DEMANDÉE - ABSENCE.
</SCT>
<ANA ID="9A"> 03-03-06 Commet une erreur de droit une cour qui juge que l'octroi des aides agricoles liées à la surface instituées par les règlements communautaires n'est subordonné qu'à la justification de l'exploitation effective des parcelles au titre desquelles l'aide est demandée, y compris lorsque ces parcelles ont fait l'objet d'un transfert entre agriculteurs, alors que l'article 74 du règlement (CE) n° 796/2004 de la Commission du 21 avril 2004, pris pour l'application du règlement (CE) n° 1782/2003 du Conseil du 29 septembre 2003, prévoit, dans un tel cas, des obligations d'information de l'autorité compétente.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
