<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025744412</ID>
<ANCIEN_ID>JG_L_2012_04_000000327915</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/74/44/CETATEXT000025744412.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 4ème sous-sections réunies, 24/04/2012, 327915</TITRE>
<DATE_DEC>2012-04-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>327915</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 4ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Philippe Martin</PRESIDENT>
<AVOCATS>SCP DEFRENOIS, LEVIS ; SCP BARADUC, DUHAMEL</AVOCATS>
<RAPPORTEUR>M. Philippe Ranquet</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Jean-Philippe Thiellay</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:327915.20120424</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi, enregistré le 12 mai 2009 au secrétariat du contentieux du Conseil d'Etat, présenté par le MINISTRE DE LA SANTE ET DES SPORTS ; le ministre demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 07PA00866 du 4 mars 2009 par lequel la cour administrative d'appel de Paris, d'une part, a annulé sur appel de M. Djilani A et Mme Sylvie B le jugement n° 0201714 du 19 décembre 2006 du tribunal administratif de Paris rejetant leur demande tendant à la condamnation de l'Etat à leur verser diverses indemnités en réparation des préjudices résultant de la vaccination de leur fille Inès le 5 septembre 1995, d'autre part, a condamné l'Etat à leur verser une rente annuelle de 12 000 euros, une rente de 524 euros par jour due au prorata du nombre de nuits que leur fille aura passées au domicile familial et un capital de 373 395,43 euros, et, enfin, a ordonné avant dire droit une expertise afin de déterminer, pour le surplus, l'étendue des préjudices ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de M. A et Mme B ;<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de la santé publique ;<br/>
<br/>
              Vu le code de la sécurité sociale ;<br/>
<br/>
              Vu la loi n° 91-647 du 10 juillet 1991 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Philippe Ranquet, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Defrenois, Levis, avocat de M. Djilani A et de Mme Sylvie B et de la SCP Baraduc, Duhamel, avocat de la caisse de mutualité sociale agricole d'Ile de France, <br/>
<br/>
              - les conclusions de M. Jean-Philippe Thiellay, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Defrenois, Levis, avocat de M. Djilani A et de Mme Sylvie B et à la SCP Baraduc, Duhamel, avocat de la caisse de mutualité sociale agricole d'Ile de France ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la jeune Inès A, alors âgée de cinq mois, a reçu le 5 septembre 1995 une injection du vaccin Pentacoq, comportant cinq valences dont trois correspondant à des vaccinations obligatoires ; qu'elle a présenté, à partir du 12 septembre, des troubles graves qui ont évolué en une rhombomyélite et l'ont laissée atteinte de lourdes séquelles neurologiques ; que ses parents, M. Djilani A et Mme Sylvie B, ont demandé à l'Etat, au titre des dommages causés par les vaccinations obligatoires, l'indemnisation des préjudices en résultant pour elle ainsi que de leurs préjudices propres, qui leur a été refusée ; que par l'arrêt du 4 mars 2009 contre lequel le MINISTRE DE LA SANTE ET DES SPORTS se pourvoit en cassation, la cour administrative d'appel de Paris a annulé le jugement du 19 décembre 2006 du tribunal administratif de Paris rejetant leur demande indemnitaire et a condamné l'Etat à leur verser diverses sommes ainsi qu'ordonné avant dire droit une expertise afin de déterminer, pour le surplus, l'étendue des préjudices ; que la caisse de mutualité sociale agricole d'Ile-de-France, par la voie du pourvoi incident, demande l'annulation du même arrêt seulement en tant que la cour a statué sur les préjudices sans tenir compte de ses droits ;<br/>
<br/>
              Sur le pourvoi principal :<br/>
<br/>
              Considérant qu'aux termes du premier alinéa de l'article L. 3111-9 du code de la santé publique, dans sa rédaction applicable en l'espèce, antérieure à l'entrée en vigueur de la loi du 9 août 2004 relative à la politique de santé publique : " Sans préjudice des actions qui pourraient être exercées conformément au droit commun, la réparation d'un dommage imputable directement à une vaccination obligatoire pratiquée dans les conditions mentionnées au présent chapitre est supportée par l'Etat. " ;<br/>
<br/>
              Considérant que la cour administrative d'appel a relevé, par une appréciation souveraine exempte de dénaturation, d'une part, que le rapport de l'expert désigné dans le cadre de la procédure devant la commission de règlement amiable des accidents vaccinaux retient deux causes possibles à la rhombomyélite dont la jeune Inès A a été atteinte, soit une infection virale sans rapport avec la vaccination, soit un phénomène immuno-inflammatoire déclenché par cette dernière, aucun agent viral n'ayant toutefois été retrouvé dans les prélèvements effectués sur l'enfant et, d'autre part, que si le rapport de l'expert désigné par le tribunal administratif conclut à l'origine virale de l'affection, cette conclusion est sérieusement contredite par des éléments produits par M. A et Mme B, notamment l'avis d'un spécialiste en virologie, consulté sur la pertinence de ce dernier rapport d'expertise, avis dont il ressort que cette conclusion procède d'une interprétation erronée de ses propres travaux ; que la cour a également relevé, sans commettre davantage de dénaturation, que la maladie est apparue dans un très bref délai après l'injection du vaccin alors que l'enfant était auparavant en bonne santé ;<br/>
<br/>
              Considérant qu'en se fondant sur ce faisceau d'éléments, qui était de nature à faire présumer en l'espèce l'origine vaccinale de la rhombomyélite, la cour a pu, sans commettre d'erreur de droit ni de qualification juridique, regarder le dommage comme imputable à l'administration du vaccin Pentacoq ; que, dès lors que ce vaccin comporte au moins une valence correspondant à une vaccination obligatoire et qu'il n'était pas démontré que les troubles seraient exclusivement imputables à l'une de ses valences facultatives, la cour n'a pas davantage commis d'erreur de droit en jugeant que le dommage entrait dans les prévisions de l'article L. 3111-9 du code de la santé publique ;<br/>
<br/>
              Considérant que le MINISTRE DE LA SANTE ET DES SPORTS n'est, par suite, pas fondé à demander l'annulation de l'arrêt, suffisamment motivé, de la cour administrative d'appel de Paris ;<br/>
<br/>
              Sur le pourvoi incident de la caisse de mutualité sociale agricole d'Ile-de-France :<br/>
<br/>
              Considérant qu'aux termes de l'article R. 611-3 du code de justice administrative : " (...) il est procédé aux notifications de la requête (...) au moyen de lettres remises contre signature ou de tout autre dispositif permettant d'attester la date de réception (...) " ; que, selon la caisse de mutualité sociale agricole d'Ile-de-France, la requête d'appel de M. A et Mme B ne lui a pas été communiquée ; que le dossier ne contient ni un avis attestant que la caisse ait reçu cette requête, ni aucun autre élément susceptible de l'établir ; que dans ces conditions et sans qu'il soit besoin d'examiner les autres moyens de son pourvoi incident, la caisse est fondée à soutenir que l'arrêt attaqué a été rendu au terme d'une procédure irrégulière et à en demander, pour ce motif, l'annulation en tant qu'il statue sur les préjudices et dans la mesure où sont concernés les postes de préjudice au titre desquels elle fait valoir des débours, c'est-à-dire les dépenses de santé et les frais liés au handicap ;<br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler dans cette mesure l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative, en statuant sur les conclusions indemnitaires de M. A et Mme B et sur celles de la caisse de mutualité sociale agricole d'Ile-de-France, qui sont recevables alors même que la caisse n'avait pas chiffré les conclusions de la demande qu'elle avait présentée devant le tribunal administratif, dès lors qu'aucune fin de non-recevoir ne lui a été opposée sur ce point par le défendeur en première instance et que le tribunal administratif ne l'a pas invitée à préciser le montant de la condamnation qu'elle sollicitait ;<br/>
<br/>
              Sur les dépenses de santé :<br/>
<br/>
              Considérant que les dépenses médicales, pharmaceutiques et d'appareillage que la caisse justifie avoir supportées s'élèvent au montant de 47 412,18 euros, qu'il y a lieu de mettre à la charge de l'Etat ;<br/>
<br/>
              Sur les frais liés au handicap :<br/>
<br/>
              Considérant que les frais afférents au placement de la jeune Inès A en institution spécialisée que la caisse justifie avoir supportés jusqu'au 31 mai 2011 s'élèvent au montant de 2 729 636,63 euros ; qu'il résulte de l'instruction que l'état de complète dépendance dans lequel se trouve l'enfant a entraîné jusqu'au 31 décembre 2008, pour les périodes où elle n'était pas hébergée en institution, un besoin en assistance d'une tierce personne et des frais divers qu'il y a lieu d'évaluer au montant, justifié par M. et Mme A et non sérieusement contesté, de 323 395,43 euros ; qu'il y lieu de mettre ces montants à la charge de l'Etat ;<br/>
<br/>
              Considérant que si le juge n'est pas en mesure de déterminer lorsqu'il se prononce si l'enfant sera placé dans une institution spécialisée ou s'il sera hébergé au domicile de sa famille, il lui appartient d'accorder à l'enfant une rente trimestrielle couvrant les frais de son maintien au domicile familial, en fixant un taux quotidien et en précisant que la rente sera versée au prorata du nombre de nuits que l'enfant aura passées à ce domicile au cours du trimestre considéré ; que les autres chefs de préjudice demeurés à la charge de l'enfant doivent être indemnisés par ailleurs, sous la forme soit d'un capital, soit d'une rente distincte ; que le juge doit condamner le responsable du dommage à rembourser à l'organisme de sécurité sociale qui aura assumé la charge du placement de l'enfant dans une institution spécialisée, le remboursement, sur justificatifs, des frais qu'il justifiera avoir exposés de ce fait ; qu'en cas de refus du centre hospitalier, il appartiendra à la caisse de faire usage des voies de droit permettant d'obtenir l'exécution des décisions de la justice administrative ;<br/>
<br/>
              Considérant qu'il sera fait une juste appréciation des frais afférents au maintien d'Inès A au domicile de ses parents, eu égard notamment à la nécessité de l'assistance d'une tierce personne, en attribuant à l'enfant, depuis le 1er janvier 2009, une rente calculée sur la base d'un taux quotidien dont le montant, fixé à 200 euros à cette date, sera revalorisé par la suite par application des coefficients prévus à l'article L. 434-17 du code de la sécurité sociale ; que cette rente, versée par trimestres échus, sera due au prorata du nombre de nuits que l'enfant aura passées au domicile familial ; que l'Etat doit également rembourser à la caisse, sur justificatifs, toutes les sommes qu'elle a exposées depuis le 1er juin 2011, et exposera à l'avenir, au titre du placement de l'enfant en établissement spécialisé ;<br/>
<br/>
              Considérant que les sommes accordées ci-dessus au titre des frais liés au handicap s'entendent sans préjudice de celles allouées pour l'aménagement du domicile et l'acquisition d'un nouveau logement par l'arrêt définitif rendu le 29 juillet 2011 par la cour administrative d'appel de Paris après dépôt du rapport de l'expertise ordonnée avant dire droit par l'arrêt attaqué ;<br/>
<br/>
              Sur les intérêts et la capitalisation des intérêts :<br/>
<br/>
              Considérant que les sommes dues en capital à M. A et Mme B, en leur qualité de responsables légaux de leur fille, porteront intérêts au taux légal à compter du 2 octobre 1998, date de leur demande indemnitaire préalable ; que les arrérages de la rente qui leur est due porteront intérêts au taux légal à compter de leur date d'échéance respective et jusqu'à leur paiement ; qu'ils ont demandé la capitalisation des intérêts le 6 février 2002 ; que les intérêts échus à cette date, ainsi qu'à chaque échéance annuelle ultérieure, seront capitalisés pour produire eux-mêmes intérêts ;<br/>
<br/>
              Sur l'application des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 :<br/>
<br/>
              Considérant que M. A et Mme B ayant obtenu le bénéfice de l'aide juridictionnelle devant le Conseil d'Etat, leur avocat peut se prévaloir de ces dispositions ; qu'il y a lieu, dans les circonstances de l'espèce, et sous réserve que la SCP Defrenois, Levis, avocat de M. A et Mme B, renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à la SCP Defrenois, Levis ; qu'il y a également lieu, en application des dispositions de l'article L. 761-1 du code de justice administrative, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à la caisse de mutualité sociale agricole d'Ile-de-France au titre des frais exposés par elle devant le Conseil d'Etat et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 4 mars 2009 de la cour administrative d'appel de Paris est annulé en tant qu'il statue sur les préjudices en ce qui concerne les dépenses de santé et les frais liés au handicap.<br/>
<br/>
Article 2 : L'Etat est condamné à verser à M. A et à Mme B, en leur qualité de représentants légaux de leur fille Inès, en sus des sommes mises à sa charge par l'arrêt du 29 juillet 2011 de la cour administrative d'appel de Paris, la somme de 323 395,43 euros, assortie des intérêts au taux légal à compter du 2 octobre 1998 et, à compter du 1er janvier 2009, par trimestre échu, une rente due au prorata du nombre de nuits que l'enfant aura passées au domicile familial dont le montant quotidien, fixé à cette date à 200 euros, sera revalorisé par la suite par application des coefficients prévus à l'article L. 434-17 du code de la sécurité sociale et dont les arrérages porteront intérêts au taux légal à compter de leur date d'échéance respective et jusqu'à leur paiement. Les intérêts échus au 6 février 2002, ainsi qu'à chaque échéance annuelle ultérieure, seront capitalisés pour produire eux-mêmes intérêts.<br/>
<br/>
Article 3 : L'Etat est condamné à verser à la caisse de mutualité sociale agricole d'Ile-de-France la somme de 2 777 048,81 euros ainsi qu'à lui rembourser, sur présentation des justificatifs à la fin de chaque trimestre échu, les frais exposés par elle au titre des frais de placement d'Inès A en institution spécialisée à compter du 1er juin 2011.<br/>
<br/>
Article 4 : Le jugement du 19 décembre 2006 du tribunal administratif de Paris est réformé en ce qu'il a de contraire à la présente décision.<br/>
<br/>
Article 5 : L'Etat versera la somme de 3 000 euros à la SCP Defrenois, Levis, avocat de M. A et Mme B, en application des dispositions du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat, et celle de 3 000 euros à la caisse de mutualité sociale agricole d'Ile-de-France au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 6 : Le pourvoi du MINISTRE DE LA SANTE ET DES SPORTS et le surplus des conclusions de la requête de M. A et Mme B devant la cour administrative d'appel de Paris sont rejetés.<br/>
<br/>
Article 7 : La présente décision sera notifiée au ministre du travail, de l'emploi et de la santé, à M. Djilani A, à Mme Sylvie B et à la caisse de mutualité sociale agricole d'Ile-de-France.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">60-02-01-03 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RESPONSABILITÉ EN RAISON DES DIFFÉRENTES ACTIVITÉS DES SERVICES PUBLICS. SERVICE PUBLIC DE SANTÉ. SERVICE DES VACCINATIONS. - ART. L. 3111-9 DU CODE DE LA SANTÉ PUBLIQUE - CHAMP D'APPLICATION - VACCIN POLYVALENT COMPORTANT AU MOINS UNE VALENCE CORRESPONDANT À UNE VACCINATION OBLIGATOIRE - INCLUSION - CONDITIONS - ABSENCE DE DÉMONSTRATION QUE LES TROUBLES SERAIENT IMPUTABLES À L'UNE DES VALENCES FACULTATIVES.
</SCT>
<ANA ID="9A"> 60-02-01-03 Dès lors qu'un vaccin comporte au moins une valence correspondant à une vaccination obligatoire et qu'il n'est pas démontré que les troubles seraient exclusivement imputables à l'une de ses valences facultatives, le dommage entre dans les prévisions de l'article L. 3111-9 du code de la santé publique prévoyant que la réparation d'un dommage imputable directement à une vaccination obligatoire est supportée par l'Etat.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
