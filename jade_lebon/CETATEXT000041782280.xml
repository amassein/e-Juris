<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041782280</ID>
<ANCIEN_ID>JG_L_2020_04_000000422802</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/78/22/CETATEXT000041782280.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 5ème chambres réunies, 03/04/2020, 422802</TITRE>
<DATE_DEC>2020-04-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>422802</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER ; SCP DE CHAISEMARTIN, DOUMIC-SEILLER ; SCP MELKA - PRIGENT</AVOCATS>
<RAPPORTEUR>Mme Coralie Albumazard</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Stéphane Hoynck</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:422802.20200403</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Le syndicat des copropriétaires de l'ensemble immobilier " Univers 21 " et Mme B... D..., d'une part, et le syndicat des copropriétaires du 53 rue de Montreuil, d'autre part, ont demandé au tribunal administratif de Paris d'annuler pour excès de pouvoir les arrêtés des 4 juillet et 6 décembre 2016 par lesquels la maire de Paris a accordé à M. C... A... un permis de construire et un permis de construire modificatif pour la réalisation d'une maison individuelle au 53 rue de Montreuil, dans le onzième arrondissement de Paris, ainsi que la décision implicite de rejet de leur recours gracieux. Par un jugement n° 1613535/4-2, 1622023/4-2 du 1er juin 2018, le tribunal administratif a fait droit à leurs demandes.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un autre mémoire, enregistrés les 1er août, 2 novembre et 31 décembre 2018 au secrétariat du contentieux du Conseil d'Etat, la Ville de Paris demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter les requêtes ;<br/>
<br/>
              3°) de mettre à la charge solidaire du syndicat des copropriétaires de l'ensemble immobilier Univers 21, de Mme D... et du syndicat des copropriétaires du 53 rue de Montreuil la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Coralie Albumazard, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Stéphane Hoynck, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Foussard, Froger, avocat de la Ville de Paris, à la SCP Melka - Prigent, avocat du syndicat des copropriétaires de l'ensemble immobilier " Univers 21 " et autre, et à la SCP de Chaisemartin, Doumic-Seiller, avocat du syndicat des copropriétaires du 53 rue de Montreuil ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces des dossiers soumis aux juges du fond que le syndicat des copropriétaires de l'ensemble immobilier " Univers 21 " et Mme B... D..., d'une part, et le syndicat des copropriétaires du 53, rue de Montreuil, d'autre part, ont demandé au tribunal administratif de Paris d'annuler pour excès de pouvoir les arrêtés du 4 juillet et du 6 décembre 2016 par lesquels la maire de Paris a délivré à M. C... A... un permis de construire et un permis de construire modificatif pour la construction, en fond de parcelle, d'une maison individuelle d'un étage sur un niveau de sous-sol au 53, rue de Montreuil, dans le onzième arrondissement de Paris. Par un jugement du 1er juin 2018 contre lequel la Ville de Paris se pourvoit en cassation, le tribunal administratif de Paris a annulé ces deux arrêtés.<br/>
<br/>
              2. Aux termes de l'article L. 600-4-1 du code de justice administrative : " Lorsqu'elle annule pour excès de pouvoir un acte intervenu en matière d'urbanisme ou en ordonne la suspension, la juridiction administrative se prononce sur l'ensemble des moyens de la requête qu'elle estime susceptibles de fonder l'annulation ou la suspension, en l'état du dossier ".<br/>
<br/>
              3. Saisi d'un pourvoi dirigé contre une décision juridictionnelle reposant sur plusieurs motifs dont l'un est erroné, le juge de cassation, à qui il n'appartient pas de rechercher si la juridiction aurait pris la même décision en se fondant uniquement sur les autres motifs, doit, hormis le cas où ce motif erroné présenterait un caractère surabondant, accueillir le pourvoi. Il en va cependant autrement lorsque la décision juridictionnelle attaquée prononce l'annulation pour excès de pouvoir d'un acte administratif, dans la mesure où l'un quelconque des moyens retenus par le juge du fond peut suffire alors à justifier son dispositif d'annulation. En pareille hypothèse, et sous réserve du cas où la décision qui lui est déférée aurait été rendue dans des conditions irrégulières, il appartient au juge de cassation, si l'un des moyens reconnus comme fondés par cette décision en justifie légalement le dispositif, sauf à ce que le vice qui affecte l'acte soit susceptible de faire l'objet d'une mesure de régularisation, en particulier en application des articles L. 600-5 ou L. 600-5-1 du code de l'urbanisme, de rejeter le pourvoi. Toutefois, en raison de l'autorité de chose jugée qui s'attache aux motifs constituant le soutien nécessaire du dispositif de la décision juridictionnelle déférée, le juge de cassation ne saurait, sauf à méconnaître son office, prononcer ce rejet sans avoir, au préalable, censuré celui ou ceux de ces motifs qui étaient erronés. Eu égard à l'objet des dispositions précitées de l'article L. 600-4-1 du code de l'urbanisme, cette règle trouve en particulier à s'appliquer lorsque la pluralité des motifs du juge du fond découle de l'obligation qui lui est faite de se prononcer sur l'ensemble des moyens susceptibles de fonder l'annulation.<br/>
<br/>
              4. Pour annuler les arrêtés litigieux, le tribunal administratif, après avoir admis l'intérêt à agir des requérants, a retenu quatre motifs d'illégalité, tirés de la fraude entachant le permis de construire modificatif, du caractère lacunaire de la notice architecturale prévue à l'article R. 431-8 du code de l'urbanisme, de l'erreur de fait entachant d'irrégularité l'avis de l'architecte des bâtiments de France et de la méconnaissance des dispositions de l'article UG 13.1.2 du règlement du plan local d'urbanisme de la Ville de Paris.<br/>
<br/>
              5. En premier lieu, l'article L. 600-1-2 du code de l'urbanisme, dans sa version applicable, dispose que : " Une personne autre que l'Etat, les collectivités territoriales ou leurs groupements ou une association n'est recevable à former un recours pour excès de pouvoir contre un permis de construire, de démolir ou d'aménager que si la construction, l'aménagement ou les travaux sont de nature à affecter directement les conditions d'occupation, d'utilisation ou de jouissance du bien qu'elle détient ou occupe régulièrement ou pour lequel elle bénéficie d'une promesse de vente, de bail, ou d'un contrat préliminaire mentionné à l'article L. 261-15 du code de la construction et de l'habitation ".  Il résulte de ces dispositions qu'il appartient à tout requérant qui saisit le juge administratif d'un recours pour excès de pouvoir tendant à l'annulation d'un permis de construire, de démolir ou d'aménager, de préciser l'atteinte qu'il invoque pour justifier d'un intérêt lui donnant qualité pour agir, en faisant état de tous éléments suffisamment précis et étayés de nature à établir que cette atteinte est susceptible d'affecter directement les conditions d'occupation, d'utilisation ou de jouissance de son bien. Il appartient au défendeur, s'il entend contester l'intérêt à agir du requérant, d'apporter tous éléments de nature à établir que les atteintes alléguées sont dépourvues de réalité. Le juge de l'excès de pouvoir apprécie la recevabilité de la requête au vu des éléments ainsi versés au dossier par les parties, en écartant le cas échéant les allégations qu'il jugerait insuffisamment étayées mais sans pour autant exiger de l'auteur du recours qu'il apporte la preuve du caractère certain des atteintes qu'il invoque au soutien de la recevabilité de celui-ci. Eu égard à sa situation particulière, le voisin immédiat justifie, en principe, d'un intérêt à agir lorsqu'il fait état devant le juge, qui statue au vu de l'ensemble des pièces du dossier, d'éléments relatifs à la nature, à l'importance ou à la localisation du projet de construction.<br/>
<br/>
              6. Il ressort des pièces du dossier soumis aux juges du fond que le terrain d'assiette de la construction litigieuse se situe dans l'angle nord-est de la parcelle 112, laquelle jouxte la parcelle 107 sur laquelle est implanté l'ensemble immobilier " Univers 21 " au sein duquel se trouve, notamment, l'appartement de Mme D.... En jugeant que cette dernière, qui faisait en particulier état de vues directes depuis l'immeuble sur le terrain d'assiette de la construction litigieuse sans être sérieusement contredite en défense, se prévalait d'un intérêt lui donnant qualité à agir contre les décisions attaquées, le tribunal administratif de Paris n'a commis aucune erreur de qualification juridique.  <br/>
<br/>
              7. En second lieu, l'article UG 13 du règlement du plan local d'urbanisme de la Ville de Paris définit des prescriptions applicables aux espaces libres de constructions et aux plantations " afin de préserver le paysage urbain parisien, d'améliorer la qualité de vie des habitants, de sauvegarder et développer le biotope ". Le 1° de l'article UG 13.1.2 de ce règlement dispose notamment : " 1° - Disposition générales : / Sur tout terrain dont la profondeur est supérieure à celle de la bande Z, les espaces libres, situés ou non dans la bande Z, doivent présenter une surface au sol au moins égale à 50 % de la superficie S correspondant à la partie du terrain située hors de la bande Z. / Les espaces libres doivent comprendre : / a - une surface au moins égale à 20 % de la superficie S, obligatoirement en pleine terre ; / b - une surface complémentaire au moins égale à : / - 10 % de la superficie S sur les terrains situés dans le Secteur de mise en valeur du végétal, / - 15 % de la superficie S sur les terrains situés dans le Secteur de renforcement du végétal. / Cette surface complémentaire doit être réalisée prioritairement en pleine terre. A défaut, elle peut être remplacée par une Surface végétalisée pondérée de même valeur minimale (...) ". Les dispositions générales applicables au territoire couvert par le plan local d'urbanisme définissent la bande Z comme la bande d'une largeur de 15 mètres délimitée à compter, notamment, de l'alignement de la voie publique ou de la limite de fait de la voie privée. Ces mêmes dispositions définissent les espaces libres, sauf spécification contraire, comme " les espaces hors voie libres de constructions en élévation (à l'exception des équipements et des serres de production agricole, des composteurs et des aires couvertes de stationnement des vélos) ", à l'exclusion des surfaces surplombées par des éléments de construction. Il résulte de ces dispositions que, pour le calcul du quota d'espaces libres de 50 % prévu par le 1° de l'article UG 13.1.2 précité, doit être prise en compte la surface de l'ensemble des espaces situés au niveau du sol dès lors qu'ils sont vierges de toute construction.<br/>
<br/>
              8. Pour juger que les arrêtés litigieux méconnaissaient les dispositions de l'article UG 13.1.2 du règlement du plan local d'urbanisme, le tribunal administratif a relevé, par une analyse non contestée en cassation, que, dans l'état initial du terrain, la surface d'espaces libres de la parcelle sur laquelle devait être édifiée la construction litigieuse était de 404 m2, soit une surface inférieure à celle qui devrait résulter de l'application des dispositions précitées. Il a ensuite jugé que, en créant une nouvelle surface d'emprise au sol, le projet litigieux aggravait la non-conformité du bâti initial aux dispositions précitées, dès lors qu'il résultait des pièces des dossiers qui lui étaient soumis que le terrain d'assiette du projet était un espace libre de construction en élévation. En statuant ainsi, par une appréciation souveraine exempte de dénaturation, le tribunal administratif a fait une exacte application des dispositions du règlement du plan local d'urbanisme précitées et n'a pas entaché son jugement d'erreur de droit.<br/>
<br/>
              9. Ce motif, qui n'est pas susceptible de faire l'objet d'une mesure de régularisation en application des articles L. 600-5 ou L. 600-5-1 du code de l'urbanisme, suffit à justifier légalement le dispositif du jugement du tribunal administratif de Paris. Par suite, la Ville de Paris n'est pas fondée à demander l'annulation de ce jugement.<br/>
<br/>
              10. Toutefois, ainsi qu'il a été dit plus haut, le juge de cassation ne saurait prononcer le rejet du pourvoi sans avoir, au préalable, censuré celui ou ceux de ces motifs retenus par les juges du fond qui étaient erronés.<br/>
<br/>
              11. En premier lieu, aux termes de l'article R. 423-1 du code de l'urbanisme : " Les demandes de permis de construire, d'aménager ou de démolir et les déclarations préalables sont adressées par pli recommandé avec demande d'avis de réception ou déposées à la mairie de la commune dans laquelle les travaux sont envisagés : / a) Soit par le ou les propriétaires du ou des terrains, leur mandataire ou par une ou plusieurs personnes attestant être autorisées par eux à exécuter les travaux ; / b) Soit, en cas d'indivision, par un ou plusieurs co-indivisaires ou leur mandataire ; / c) Soit par une personne ayant qualité pour bénéficier de l'expropriation pour cause d'utilité publique ". Aux termes du dernier alinéa de l'article R. 431-5 du même code : " La demande comporte également l'attestation du ou des demandeurs qu'ils remplissent les conditions définies à l'article R. 423-1 pour déposer une demande de permis ". En vertu de l'article R. 431-4 du même code, le dossier est réputé complet lorsqu'il comprend les informations et pièces limitativement énumérées aux articles R. 431-5 à R. 431-33-1, aucune autre information ou pièce ne pouvant être exigée par l'autorité compétente. Par ailleurs, comme le rappelle le dernier alinéa de l'article A. 428-4 du même code, le permis est délivré sous réserve du droit des tiers, il vérifie la conformité du projet aux règles et servitudes d'urbanisme, il ne vérifie pas si le projet respecte les autres réglementations et les règles de droit privé. Toute personne s'estimant lésée par la méconnaissance du droit de propriété ou d'autres dispositions de droit privé peut donc faire valoir ses droits en saisissant les tribunaux civils, même si le permis respecte les règles d'urbanisme.<br/>
<br/>
              12. Il résulte de ces dispositions que, sous réserve de la fraude, le pétitionnaire qui fournit l'attestation prévue à l'article R. 431-5 du code de l'urbanisme selon laquelle il remplit les conditions fixées par l'article R. 423-1 du même code doit être regardé comme ayant qualité pour présenter sa demande. Il résulte également de ces dispositions qu'une demande d'autorisation d'urbanisme concernant un terrain soumis au régime juridique de la copropriété peut être régulièrement présentée par son propriétaire, son mandataire ou par une ou plusieurs personnes attestant être autorisées par lui à exécuter les travaux, alors même que la réalisation de ces travaux serait subordonnée à l'autorisation de l'assemblée générale de la copropriété, une contestation sur ce point ne pouvant être portée, le cas échéant, que devant le juge judiciaire. Une telle contestation ne saurait, par elle-même, caractériser une fraude du pétitionnaire entachant d'irrégularité la demande d'autorisation d'urbanisme.<br/>
<br/>
              13. Par le jugement attaqué, le tribunal administratif a jugé qu'en attestant de sa qualité pour déposer sa demande de permis de construire modificatif, alors même que l'introduction d'un recours gracieux et d'une requête par le syndicat des copropriétaires de l'ensemble immobilier " Univers 21 " et par Mme D... l'avait alerté sur la nécessité d'obtenir au préalable l'autorisation de l'assemblée générale des copropriétaires, M. A... s'était livré à une manoeuvre frauduleuse entachant d'irrégularité le permis de construire modificatif qui lui a été délivré. Il résulte de ce qui a été dit au point 12 ci-dessus qu'en statuant ainsi, le tribunal administratif a entaché son jugement d'une erreur de droit.<br/>
<br/>
              14. En second lieu, aux termes de l'article R. 425-30 du code de l'urbanisme : " Lorsque le projet est situé dans un site inscrit, la demande de permis ou la déclaration préalable tient lieu de la déclaration exigée par l'article L. 341-1 du code de l'environnement. Les travaux ne peuvent être entrepris avant l'expiration d'un délai de quatre mois à compter du dépôt de la demande ou de la déclaration. / La décision prise sur la demande de permis ou sur la déclaration préalable intervient après consultation de l'architecte des Bâtiments de France ".<br/>
<br/>
              15. Par le jugement attaqué, le tribunal administratif a jugé qu'en mentionnant que le projet litigieux portait sur une " construction créant moins de 20 m2 " alors qu'il ressortait des autres pièces du dossier que celui-ci créerait une surface de 165,86 m2, l'avis de l'architecte des bâtiments de France, bien qu'il comportât en visa les références du dossier de permis de construire qui lui avait été transmis, était affecté d'une erreur de fait entachant d'irrégularité le permis de construire délivré à M. A.... En statuant ainsi, alors qu'il ressortait des termes mêmes de cet avis que la mention erronée de la surface de plancher créée procédait d'une simple erreur de plume, le tribunal administratif a dénaturé les pièces des dossiers qui lui étaient soumis.<br/>
<br/>
              16. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge du syndicat des copropriétaires de l'ensemble immobilier " Univers 21 ", de Mme D... et du syndicat des copropriétaires du 53, rue de Montreuil, qui ne sont pas, dans la présente instance, la partie perdante. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la Ville de Paris la somme de 3 000 euros à verser, d'une part, au syndicat des copropriétaires du 53, rue de Montreuil, et, d'autre part, au syndicat des copropriétaires de l'ensemble immobilier " Univers 21 " et à Mme D....<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la Ville de Paris est rejeté.<br/>
Article 2 : La Ville de Paris versera une somme de 3 000 euros au syndicat des copropriétaires du 53, rue de Montreuil et une somme globale de 3 000 euros au syndicat des copropriétaires de l'ensemble immobilier " Univers 21 " et à Mme D... au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la Ville de Paris, au syndicat des copropriétaires de l'ensemble immobilier " Univers 21 ", à Mme B... D... et au syndicat des copropriétaires du 53 rue de Montreuil.<br/>
Copie en sera adressée à M. C... A....<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">68-03-02-02 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PERMIS DE CONSTRUIRE. PROCÉDURE D'ATTRIBUTION. INSTRUCTION DE LA DEMANDE. - FOURNITURE PAR LE DEMANDEUR DE L'ATTESTATION SUIVANT LAQUELLE IL REMPLIT LES CONDITIONS DÉFINIES À L'ARTICLE R. 423-1 DU CODE DE L'URBANISME [RJ1] - COPROPRIÉTÉ - QUALITÉ DU MANDATAIRE POUR PRÉSENTER LA DEMANDE ALORS MÊME QUE LA RÉALISATION DES TRAVAUX SERAIT SUBORDONNÉE À L'AUTORISATION DE L'ASSEMBLÉE GÉNÉRALE - EXISTENCE - EXISTENCE D'UNE CONTESTATION SUR UNE TELLE AUTORISATION SUFFISANT À CARACTÉRISER UNE FRAUDE [RJ2] - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">68-03-03-005 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PERMIS DE CONSTRUIRE. LÉGALITÉ INTERNE DU PERMIS DE CONSTRUIRE. RÈGLES NON PRISES EN COMPTE LORS DE LA DÉLIVRANCE DU PERMIS DE CONSTRUIRE. - FOURNITURE PAR LE DEMANDEUR DE L'ATTESTATION SUIVANT LAQUELLE IL REMPLIT LES CONDITIONS DÉFINIES À L'ARTICLE R. 423-1 DU CODE DE L'URBANISME [RJ1] - COPROPRIÉTÉ - QUALITÉ DU MANDATAIRE POUR PRÉSENTER LA DEMANDE ALORS MÊME QUE LA RÉALISATION DES TRAVAUX SERAIT SUBORDONNÉE À L'AUTORISATION DE L'ASSEMBLÉE GÉNÉRALE - EXISTENCE - EXISTENCE D'UNE CONTESTATION SUR UNE TELLE AUTORISATION SUFFISANT À CARACTÉRISER UNE FRAUDE [RJ2] - ABSENCE.
</SCT>
<ANA ID="9A"> 68-03-02-02 Il résulte des articles R. 423-1, R. 431-4 et R. 431-5 du code de l'urbanisme que, sous réserve de la fraude, le pétitionnaire qui fournit l'attestation prévue à l'article R. 431-5 selon laquelle il remplit les conditions fixées par l'article R. 423-1 doit être regardé comme ayant qualité pour présenter sa demande.... ,,Il en résulte également qu'une demande d'autorisation d'urbanisme concernant un terrain soumis au régime juridique de la copropriété peut être régulièrement présentée par son propriétaire, son mandataire ou par une ou plusieurs personnes attestant être autorisées par lui à exécuter les travaux, alors même que la réalisation de ces travaux serait subordonnée à l'autorisation de l'assemblée générale de copropriété, une contestation sur ce point ne pouvant être portée, le cas échéant, que devant le juge judiciaire.... ,,Une telle contestation ne saurait, par elle-même, caractériser une fraude du pétitionnaire entachant d'irrégularité la demande d'autorisation d'urbanisme.</ANA>
<ANA ID="9B"> 68-03-03-005 Il résulte des articles R. 423-1, R. 431-4 et R. 431-5 du code de l'urbanisme que, sous réserve de la fraude, le pétitionnaire qui fournit l'attestation prévue à l'article R. 431-5 selon laquelle il remplit les conditions fixées par l'article R. 423-1 doit être regardé comme ayant qualité pour présenter sa demande.... ,,Il en résulte également qu'une demande d'autorisation d'urbanisme concernant un terrain soumis au régime juridique de la copropriété peut être régulièrement présentée par son propriétaire, son mandataire ou par une ou plusieurs personnes attestant être autorisées par lui à exécuter les travaux, alors même que la réalisation de ces travaux serait subordonnée à l'autorisation de l'assemblée générale de copropriété, une contestation sur ce point ne pouvant être portée, le cas échéant, que devant le juge judiciaire.... ,,Une telle contestation ne saurait, par elle-même, caractériser une fraude du pétitionnaire entachant d'irrégularité la demande d'autorisation d'urbanisme.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., sur la portée de cette attestation, CE, Section, 19 juin 2015, Commune de Salbris, n° 368667, p. 211.,,[RJ2] Cf., sur l'obligation pour l'administration de refuser le permis lorsqu'elle a connaissance de l'existence d'une fraude sur la qualité du pétitionnaire, CE, 23 mars 2015,,, n° 348261, p.117.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
