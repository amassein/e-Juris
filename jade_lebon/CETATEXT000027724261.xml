<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027724261</ID>
<ANCIEN_ID>JG_L_2013_07_000000343554</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/72/42/CETATEXT000027724261.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème et 5ème sous-sections réunies, 17/07/2013, 343554</TITRE>
<DATE_DEC>2013-07-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>343554</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème et 5ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Bruno Bachini</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Keller</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:343554.20130717</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu, 1° sous le n° 343554, la requête sommaire et le mémoire complémentaire, enregistrés les 27 septembre et 22 octobre 2010 au secrétariat du contentieux du Conseil d'Etat, présentés par M. A... B..., demeurant... ; M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'évaluation dont il a fait l'objet le 12 juillet 2010 ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat une somme de 1 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu, 2° sous le n° 344148, la requête, enregistrée le 3 novembre 2010 au secrétariat du contentieux du Conseil d'Etat, présentée pour M. A... B..., demeurant... ; M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret du 30 septembre 2010 par lequel le Président de la République a mis fin à ses fonctions d'ambassadeur, représentant permanent de la France auprès du Conseil de l'Europe à Strasbourg, et nommé son successeur ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat une somme de 5 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 27 juin 2013, présentée par M.B... ; <br/>
<br/>
              Vu la loi du 22 avril 1905, notamment son article 65 ;<br/>
<br/>
              Vu la loi n° 79-587 du 11 juillet 1979 ;<br/>
<br/>
              Vu la loi n° 83-634 du 13 juillet 1983 ;<br/>
<br/>
              Vu la loi n° 84-16 du 11 janvier 1984 ;<br/>
<br/>
              Vu le décret n° 69-222 du 6 mars 1969 ;<br/>
<br/>
              Vu le décret n° 85-779 du 24 juillet 1985 ; <br/>
<br/>
              Vu le décret n° 2002-682 du 29 avril 2002 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bruno Bachini, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de M. Rémi Keller, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Barthélémy, Matuchansky, Vexliard, avocat de M.B... ; <br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que les requêtes visées ci-dessus sont relatives à la situation individuelle d'un même fonctionnaire nommé par décret du Président de la République ; que la requête enregistrée sous le n° 344148 relève de la compétence en premier et dernier ressort du Conseil d'Etat en vertu du 1° de l'article R. 311-1 du code de justice administrative tandis que la requête enregistrée sous le n° 343554 présente à juger des questions connexes ; qu'il y a lieu de joindre ces requêtes pour statuer par une seule décision ; <br/>
<br/>
              Sur la requête n° 343554 :<br/>
<br/>
              2. Considérant que, par cette requête, M. B... demande l'annulation pour excès de pouvoir de l'évaluation, dite à 360°, dont il a fait l'objet le 12 juillet 2010 en sa qualité d'ambassadeur, représentant permanent de la France auprès du Conseil de l'Europe à Strasbourg, et dont il a reçu notification par un message électronique le 10 août  suivant ; qu'il ressort des pièces du dossier que ce dispositif d'évaluation a été institué à compter de l'année 2006 par des circulaires annuelles du ministre des affaires étrangères et européennes ; que ce dispositif consiste à mettre en regard une auto-évaluation de l'ambassadeur avec les évaluations réalisées, d'une part, par ses collaborateurs, d'autre part, par son directeur géographique, le résultat des diverses fiches d'évaluation faisant ensuite l'objet d'une synthèse par un collège d'évaluateurs centraux, sous forme de schémas et d'un diagramme accompagnés d'un commentaire littéral sur la manière de servir de l'agent évalué ; <br/>
<br/>
              3. Considérant, en premier lieu, qu'aux termes de l'article 1er du décret du 24 juillet 1985 portant application de l'article 25 de la loi n° 84-16 du 11 janvier 1984 fixant les emplois supérieurs pour lesquels la nomination est laissée à la décision du gouvernement, figure au nombre de ces emplois, en ce qui concerne tant la nomination que la cessation de fonctions, celui de " chef titulaire d'une mission diplomatique ayant rang d'ambassadeur " ; qu'il résulte des dispositions des articles 2 et 3 de la même loi du 11 janvier 1984 que les personnes nommées à ces emplois à la discrétion du gouvernement, qui peuvent être des fonctionnaires ou des non-fonctionnaires, ne se trouvent pas, en cette qualité, titularisées dans un grade de la hiérarchie des administrations centrales ou des services déconcentrés de l'Etat ; que, par suite, si dans le cas où ces personnes ont la qualité de fonctionnaire, leur nomination dans un tel emploi ne dispense pas l'administration du corps dont elles relèvent de l'obligation, lorsqu'elle n'est pas exclue par le statut particulier de ce corps, de leur délivrer les notes et appréciations générales exprimant leur valeur professionnelle mentionnées aux articles 17 de la loi du 13 juillet 1983 et 55 de la loi du 11 janvier 1984, le ministre dont dépend l'emploi auquel les intéressés sont nommés est compétent pour instituer, dans le cadre de ses pouvoirs de chef de service, un dispositif spécifique d'évaluation, sous réserve que ce dispositif n'ait ni pour objet ni ne soit susceptible d'avoir pour effet d'entraîner des conséquences sur la situation statutaire de l'agent concerné dans son corps d'origine ;<br/>
<br/>
              4. Considérant qu'en l'espèce, il ne ressort pas des pièces du dossier, et n'est d'ailleurs pas allégué, que l'évaluation contestée, dont M. B...a fait l'objet dans son emploi d'ambassadeur au titre de l'année 2010 et qui, selon les circulaires qui l'ont instituée, constituait un simple élément permettant au ministre des affaires étrangères de s'assurer des capacités de l'agent évalué à exercer des fonctions de haute responsabilité dans un poste diplomatique, ait pu avoir pour objet ou pour effet de constituer un élément de l'évaluation ou de la notation statutaires mentionnées aux III et IV de l'article 7 du décret du 6 mars 1969 relatif au statut particulier des agents diplomatiques et consulaires, auxquelles l'intéressé restait soumis en sa qualité de ministre plénipotentiaire de 2ème classe, ni, à plus forte raison, d'en tenir lieu ; qu'une telle évaluation n'ayant dès lors revêtu aucun caractère statutaire, le requérant n'est pas fondé à soutenir que le ministre n'a pu légalement la mettre en oeuvre sur le fondement de ces circulaires, ni ne peut utilement invoquer la méconnaissance de la règle prévue par les articles 55 de la loi du 11 janvier 1984 et 6 du décret du 29 avril 2002, alors en vigueur, relatif aux conditions générales d'évaluation, de notation et d'avancement des fonctionnaires de l'Etat, au motif que celle-ci n'émanait pas de son chef de service ; <br/>
<br/>
              5. Considérant, en deuxième lieu, que l'évaluation contestée, qui est au demeurant motivée, n'était soumise à aucune obligation particulière en ce sens, ni par la loi du 11 juillet 1979, ni par aucun autre texte ; que le moyen tiré d'un vice de forme à ce titre doit donc être écarté ;<br/>
<br/>
              6. Considérant, en troisième lieu, qu'eu égard au processus de mise en regard, décrit ci-dessus, mené au titre de la procédure d'évaluation dite à 360°, M. B...a eu communication des éléments dont il devait avoir connaissance dans le cadre de cette évaluation et été mis à même de formuler ses observations ; <br/>
<br/>
              7. Considérant, en quatrième lieu, que l'évaluation contestée, qui, ainsi qu'il a été dit aux points 3 et 4, n'est pas dépourvue de base légale, ne révèle pas l'intention d'infliger à l'intéressé une sanction déguisée ; qu'il ne ressort pas davantage des pièces du dossier que cette évaluation repose sur des faits matériellement inexacts ou soit entachée d'erreur manifeste d'appréciation ;  <br/>
<br/>
              8. Considérant qu'il résulte de tout ce qui précède que M. B...n'est pas fondé à demander l'annulation de l'évaluation dont il a fait l'objet ; <br/>
<br/>
              Sur la requête n° 344148 :<br/>
<br/>
              9. Considérant que, par cette requête, M. B...demande l'annulation pour excès de pouvoir du décret du 30 septembre 2010 par lequel le Président de la République a mis fin à ses fonctions d'ambassadeur, représentant permanent de la France auprès du Conseil de l'Europe, et a nommé son successeur ;<br/>
<br/>
              10. Considérant, en premier lieu, qu'il résulte de ce qui a été dit plus haut que M. B...n'est, en tout état de cause, pas fondé à se prévaloir de l'illégalité de l'évaluation, dite à 360°, dont il a fait l'objet au titre de l'année 2010, à l'encontre de cette décision ;<br/>
<br/>
              11. Considérant, en deuxième lieu, qu'il ressort des pièces du dossier que la décision de mettre fin aux fonctions de M. B... a été prise au vu d'un rapport d'inspection daté du 17 septembre 2010 faisant état, d'une part, de son comportement, qualifié d'inacceptable, à l'égard du personnel féminin de la Représentation permanente, et notamment de ses agissements à l'encontre d'une agente contractuelle décrits comme constitutifs d'un acharnement particulier ayant entraîné une détérioration de la santé physique et psychologique de cette dernière, d'autre part, des conséquences, qualifiées de déplorables, de ce comportement sur l'image du ministère et de la France vis-à-vis de l'extérieur ; que ce rapport recommandait qu'il fût mis fin immédiatement aux fonctions de l'intéressé et émettait de sérieuses réserves sur l'opportunité de lui confier, à l'avenir, des missions d'autorité et de nouvelles fonctions d'encadrement ; qu'alors même que la décision de mettre fin aux fonctions de M. B...a ainsi été prise en considération de son comportement, elle a eu pour seul objet de préserver l'intérêt du service ; que, dès lors, celui-ci, nonobstant les circonstances qu'il a fait l'objet ultérieurement de poursuites disciplinaires à raison de ce comportement et que  l'administration l'a laissé sans affectation jusqu'à la prise d'effet de la sanction de la mise à la retraite d'office qui lui a été infligée par la suite, n'est pas fondé à soutenir que la mesure litigieuse aurait constitué une sanction disciplinaire déguisée ; qu'il ne peut, par suite, utilement soutenir que le décret attaqué aurait dû, pour ce motif, être motivé en application des articles 1er de la loi du 11 juillet 1979 et 19 de la loi du 13 juillet 1983 et être pris au terme de la procédure applicable en matière disciplinaire ;<br/>
<br/>
              12. Considérant, en troisième lieu, que s'il résulte de ce qui a été dit ci-dessus que le Président de la République peut, à tout moment, décider de mettre fin aux fonctions d'un ambassadeur, cette cessation de fonctions, même si elle est dépourvue de caractère disciplinaire, constitue, sauf si elle est la conséquence d'une nouvelle réglementation de l'emploi en cause, une mesure prise en considération de la personne ; qu'elle doit, dès lors, être précédée de la formalité instituée par l'article 65 de la loi du 22 avril 1905 ;<br/>
<br/>
              13. Considérant qu'il ressort des pièces du dossier qu'après avoir été destinataire, le 20 septembre 2010, du rapport d'inspection mentionné ci-dessus, M. B...a été informé par un courrier du directeur général de l'administration et de la modernisation du ministère des affaires étrangères et européennes du 24 septembre 2010, remis le jour-même à son domicile par coursier, de l'intention du Président de la République de mettre un terme à ses fonctions de représentant permanent de la France auprès du Conseil de l'Europe et de la possibilité, d'une part, de consulter son dossier administratif, d'autre part, de formuler des observations dans un délai de trois jours suivant la réception du courrier en cause ; que l'intéressé a présenté ses observations par une lettre du 25 septembre et a consulté son dossier administratif le 28 septembre ; que, dans ces conditions, celui-ci doit être regardé comme ayant été mis à même de demander utilement la communication de son dossier et de faire connaître ses observations à l'autorité compétente sur la mesure de retrait d'emploi envisagée ;<br/>
<br/>
              14. Considérant, en quatrième lieu, qu'il ne ressort pas des pièces du dossier que la décision attaquée ait été prise au vu de faits matériellement inexacts ; <br/>
<br/>
              15. Considérant, en cinquième lieu, que l'emploi de chef titulaire d'une mission diplomatique ayant rang d'ambassadeur est, ainsi qu'il a été dit ci-dessus, au nombre des emplois supérieurs laissés à la décision du gouvernement en ce qui concerne tant la nomination que la cessation de fonctions ; que, par suite, M. B...ne peut utilement se prévaloir de ce que ses insuffisances professionnelles n'auraient pas été de nature à justifier le retrait de ses fonctions de représentant permanent de la France auprès du Conseil de l'Europe ;<br/>
<br/>
              16. Considérant, en sixième lieu, que si M. B...soutient que la procédure ayant conduit à l'écarter de ces fonctions avait, en réalité, pour seul but de permettre la nomination de son successeur, le détournement de pouvoir ainsi allégué n'est pas établi ;<br/>
<br/>
              17. Considérant qu'il résulte de tout ce qui précède que le requérant n'est pas fondé à demander l'annulation du décret attaqué ;<br/>
<br/>
              Sur les conclusions de M. B...présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative : <br/>
<br/>
              18. Considérant que ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans les présentes instances, la partie perdante ; <br/>
<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : Les requêtes de M. B... sont rejetées.<br/>
Article 2 : La présente décision sera notifiée à M. A... B..., à M. C...D..., au Premier ministre, au ministre des affaires étrangères et à la ministre de la réforme de l'Etat, de la décentralisation et de la fonction publique.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-06-01 FONCTIONNAIRES ET AGENTS PUBLICS. NOTATION ET AVANCEMENT. NOTATION. - FONCTIONNAIRE OCCUPANT UN EMPLOI À LA DÉCISION DU GOUVERNEMENT - POSSIBILITÉ POUR LE CHEF DE SERVICE DE METTRE EN PLACE UN DISPOSITIF SPÉCIFIQUE D'ÉVALUATION - EXISTENCE, À CONDITION QUE CE DISPOSITIF NE SOIT PAS EXCLUSIF DE LA NOTATION STATUTAIRE ET SOIT SANS CONSÉQUENCES SUR LA SITUATION STATUTAIRE DE L'AGENT ÉVALUÉ.
</SCT>
<ANA ID="9A"> 36-06-01 Si la nomination d'un fonctionnaire dans un emploi à la décision du gouvernement ne dispense pas l'administration du corps dont il relève de l'obligation, lorsqu'elle n'est pas exclue par le statut particulier de ce corps, de lui délivrer les notes et appréciations générales exprimant sa valeur professionnelle mentionnées aux articles 17 de la loi n° 83-634 du 13 juillet 1983 et 55 de la loi n° 84-16  du 11 janvier 1984, cette nomination ne fait pas obstacle à ce que le ministre dont dépend l'emploi auquel les intéressés sont nommés institue, dans le cadre de ses pouvoirs de chef de service, un dispositif spécifique d'évaluation, sous réserve que ce dispositif n'ait ni pour objet ni ne soit susceptible d'avoir pour effet d'entraîner des conséquences sur la situation statutaire de l'agent concerné dans son corps d'origine.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
