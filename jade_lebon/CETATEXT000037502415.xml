<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037502415</ID>
<ANCIEN_ID>JG_L_2018_10_000000409579</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/50/24/CETATEXT000037502415.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 4ème chambres réunies, 05/10/2018, 409579</TITRE>
<DATE_DEC>2018-10-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>409579</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP POTIER DE LA VARDE, BUK LAMENT, ROBILLOT</AVOCATS>
<RAPPORTEUR>M. Frédéric Pacoud</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:409579.20181005</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a formé opposition devant le tribunal administratif de Nancy à la contrainte émise le 12 août 2016 par la directrice de la caisse d'allocations familiales des Vosges pour le recouvrement d'un trop-perçu de revenu de solidarité active d'un montant de 308,06 euros au titre de la période du 1er juin au 31 juillet 2013. Par une ordonnance n° 1602726 du 16 novembre 2016, le président du tribunal administratif de Nancy a rejeté sa demande.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un nouveau mémoire, enregistrés les 6 avril, 7 juillet et 23 octobre 2017 au secrétariat du contentieux du Conseil d'État, M. B... demande au Conseil d'État : <br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ; <br/>
<br/>
              3°) de mettre à la charge de la caisse d'allocations familiales des Vosges la somme de 3 000 euros à verser à la SCP Potier de la Varde, Buk Lament, Robillot, son avocat, au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'action sociale et des familles ;<br/>
              - le code de procédure civile ;<br/>
              - le code de la sécurité sociale ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Pacoud, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Potier de la Varde, Buk Lament, Robillot, avocat de M.B.ou par lettre recommandée avec demande d'avis de réception adressée au secrétariat dudit tribunal dans les quinze jours à compter de la signification)<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que la caisse d'allocations familiales des Vosges a, le 12 août 2016, émis une contrainte à l'encontre de M. B... pour la récupération d'une somme totale de 1 119,95 euros, correspondant pour 811,89 euros à un indu d'allocation de logement sociale, relevant du contentieux général de la sécurité sociale, et pour 308,06 euros à un indu de revenu de solidarité active, relevant de la compétence de la juridiction administrative. Cette contrainte, qui mentionnait la possibilité de former opposition, " dans les quinze jours à compter (...) de sa notification ", devant le tribunal des affaires de sécurité sociale de Pau s'agissant de la première de ces prestations et devant le tribunal administratif de Nancy s'agissant de la seconde, a été notifiée par une lettre recommandée avec avis de réception que M. B... a reçue le 18 août 2016. M. B...a formé opposition à cette contrainte en tant qu'elle porte sur l'indu de revenu de solidarité active par une requête expédiée le 1er septembre 2016 et enregistrée le 5 septembre 2016 au greffe du tribunal administratif de Nancy. Par une ordonnance du 16 novembre 2016, le président du tribunal administratif de Nancy a rejeté cette requête comme tardive et, par suite, manifestement irrecevable, en application du 4° de l'article R. 222-1 du code de justice administrative.<br/>
<br/>
              2. Aux termes du septième alinéa de l'article L. 262-46 du code de l'action sociale et des familles : " L'article L. 161-1-5 [du code de la sécurité sociale] est applicable pour le recouvrement des sommes indûment versées au titre du revenu de solidarité active ". Cet article L. 161-1-5 du code de la sécurité sociale dispose que : " Pour le recouvrement d'une prestation indûment versée (...), le directeur d'un organisme de sécurité sociale peut, dans les délais et selon les conditions fixés par voie réglementaire, délivrer une contrainte qui, à défaut d'opposition du débiteur devant la juridiction compétente, comporte tous les effets d'un jugement et confère notamment le bénéfice de l'hypothèque judiciaire ". Enfin, aux termes du troisième alinéa de l'article R. 133-3 du même code, dans sa rédaction applicable à la procédure devant le tribunal administratif de Nancy : " Le débiteur peut former opposition par inscription au secrétariat du tribunal compétent dans le ressort duquel il est domicilié.ou par lettre recommandée avec demande d'avis de réception adressée au secrétariat dudit tribunal dans les quinze jours à compter de la signification) (... ".<br/>
<br/>
              3. Sauf texte contraire, les délais de recours devant les juridictions administratives sont, en principe, des délais francs, leur  premier jour étant le lendemain du jour de leur déclenchement et leur dernier jour étant le lendemain du jour de leur échéance, et les recours doivent être enregistrés au greffe de la juridiction avant l'expiration du délai. Toutefois, il résulte des dispositions précitées de l'article R. 133-3 du code de la sécurité sociale, applicables également au contentieux général de la sécurité sociale, qui relève des juridictions judiciaires, que, ainsi que cela est le cas devant ces juridictions en vertu des articles 642 et 668 du code de procédure civile, l'opposition à contrainte doit seulement être " adressée " à la juridiction compétente, c'est-à-dire expédiée en cas d'envoi postal, avant le terme du délai de quinze jours à compter de la signification de la contrainte, qui n'est pas un délai franc mais est seulement susceptible de prorogation jusqu'au premier jour ouvrable suivant s'il expire normalement un samedi, un dimanche ou un jour férié ou chômé. <br/>
<br/>
              4. Il ressort des pièces du dossier soumis au juge du fond que M. B...a accusé réception de la contrainte litigieuse le 18 août 2016. Par suite, le délai de quinze jours prévu par l'article R. 133-3 du code de la sécurité sociale expirait le vendredi 2 septembre à minuit. Il suit de là que le pli contenant l'opposition à contrainte de M.B..., confié au bureau de poste de Saint-Jean-de-Luz le jeudi 1er septembre en vue de son envoi par lettre recommandée avec avis de réception, a été expédié avant le terme de ce délai. Dès lors, en jugeant que les conclusions de M. B...à fin d'opposition à la contrainte décernée le 12 août 2016 étaient tardives au motif qu'elles avaient été enregistrées le 5 septembre 2016 et que le pli expédié le 1er septembre ne pouvait plus parvenir en temps utile au tribunal compte tenu du délai normal d'acheminement du courrier, le président du  tribunal administratif de Nancy a commis une erreur de droit. Par suite, sans qu'il soit besoin de statuer sur la recevabilité des observations présentées par la caisse d'allocations familiales des Vosges, son ordonnance doit être annulée.<br/>
<br/>
              5. Aux termes du premier alinéa de l'article L. 821-2 du code de justice administrative : " S'il prononce l'annulation d'une décision d'une juridiction administrative statuant en dernier ressort, le Conseil d'Etat peut soit renvoyer l'affaire devant la même juridiction statuant, sauf impossibilité tenant à la nature de la juridiction, dans une autre formation, soit renvoyer l'affaire devant une autre juridiction de même nature, soit régler l'affaire au fond si l'intérêt d'une bonne administration de la justice le justifie ".<br/>
<br/>
              6. Il ressort des pièces du dossier soumis au juge du fond que M. B...résidait, à la date à laquelle il a formé opposition à la contrainte du 12 août 2016, comme d'ailleurs à la date de la présente décision, à Saint-Jean-de-Luz. Par suite, eu égard aux dispositions de l'article R. 133-3 du code de la sécurité sociale citées au point 2, il y a lieu de renvoyer l'affaire au tribunal administratif de Pau.<br/>
<br/>
              7. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par M. B...au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : L'ordonnance du président du tribunal administratif de Nancy du 16 novembre 2016 est annulée.<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Pau.<br/>
Article 3 : Le surplus des conclusions du pourvoi est rejeté.<br/>
Article 4 : La présente décision sera notifiée à M. A...B..., au département des Vosges et au président du tribunal administratif de Pau. <br/>
Copie en sera adressée à la ministre des solidarités et de la santé, à la caisse d'allocations familiales des Vosges et au tribunal administratif de Nancy.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">04-02-06 AIDE SOCIALE. DIFFÉRENTES FORMES D'AIDE SOCIALE. REVENU MINIMUM D'INSERTION (RMI). - RSA - OPPOSITION AUX CONTRAINTES DÉLIVRÉES EN VUE DE LA RÉPÉTITION D'INDUS (ART. L. 262-46 DU CASF ET L. 165-1-5 DU CSS) - DÉLAI DE QUINZE JOURS (ART. R. 133-3 DU CSS) - MODALITÉS DE DÉCOMPTE DE CE DÉLAI [RJ1], QUI N'EST PAS UN DÉLAI FRANC.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-01-07 PROCÉDURE. INTRODUCTION DE L'INSTANCE. DÉLAIS. - DÉLAI DE QUINZE JOURS (ART. R. 133-3 DU CSS) POUR FORMER OPPOSITION AUX CONTRAINTES DÉLIVRÉES EN VUE DE LA RÉPÉTITION D'INDUS DE REVENU DE SOLIDARITÉ ACTIVE (ART. L. 262-46 DU CASF ET L. 165-1-5 DU CSS) - MODALITÉS DE DÉCOMPTE DE CE DÉLAI [RJ1], QUI N'EST PAS UN DÉLAI FRANC.
</SCT>
<ANA ID="9A"> 04-02-06 Sauf texte contraire, les délais de recours devant les juridictions administratives sont, en principe, des délais francs, leur premier jour étant le lendemain du jour de leur déclenchement et leur dernier jour étant le lendemain du jour de leur échéance, et les recours doivent être enregistrés au greffe de la juridiction avant l'expiration du délai.... ...Toutefois, il résulte des dispositions précitées de l'article R. 133-3 du code de la sécurité sociale (CSS), applicables également au contentieux général de la sécurité sociale, qui relève des juridictions judiciaires, que, ainsi que cela est le cas devant ces juridictions en vertu des articles 642 et 668 du code de procédure civile (CPC), l'opposition à contrainte doit seulement être « adressée » à la juridiction compétente, c'est-à-dire expédiée en cas d'envoi postal, avant le terme du délai de quinze jours à compter de la signification de la contrainte, qui n'est pas un délai franc mais est seulement susceptible de prorogation jusqu'au premier jour ouvrable suivant s'il expire normalement un samedi, un dimanche ou un jour férié ou chômé.</ANA>
<ANA ID="9B"> 54-01-07 Sauf texte contraire, les délais de recours devant les juridictions administratives sont, en principe, des délais francs, leur premier jour étant le lendemain du jour de leur déclenchement et leur dernier jour étant le lendemain du jour de leur échéance, et les recours doivent être enregistrés au greffe de la juridiction avant l'expiration du délai.... ...Toutefois, il résulte des dispositions précitées de l'article R. 133-3 du code de la sécurité sociale (CSS), applicables également au contentieux général de la sécurité sociale, qui relève des juridictions judiciaires, que, ainsi que cela est le cas devant ces juridictions en vertu des articles 642 et 668 du code de procédure civile (CPC), l'opposition à contrainte doit seulement être « adressée » à la juridiction compétente, c'est-à-dire expédiée en cas d'envoi postal, avant le terme du délai de quinze jours à compter de la signification de la contrainte, qui n'est pas un délai franc mais est seulement susceptible de prorogation jusqu'au premier jour ouvrable suivant s'il expire normalement un samedi, un dimanche ou un jour férié ou chômé.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr. Cass. civ. 2e, 2 décembre 2004,,c/ SCP Chaudet et Brebion, n° 03-12.466, Bull. civ. II, n° 511 ; Cass. civ. 2e, 21 février 2008,,c/ Union de recouvrement des cotisations de sécurité sociale et d'allocations familiales d'Indre-et-Loire, n° 06-20.614, inédit au Bulletin.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
