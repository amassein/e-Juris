<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042671451</ID>
<ANCIEN_ID>JG_L_2020_12_000000435910</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/67/14/CETATEXT000042671451.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 10/12/2020, 435910</TITRE>
<DATE_DEC>2020-12-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>435910</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MELKA - PRIGENT ; SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>M. Bruno Delsol</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:435910.20201210</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. C... A... B... a demandé à la Cour nationale du droit d'asile d'annuler la décision du 14 février 2019 par laquelle l'Office français de protection des réfugiés et apatrides (OFPRA) a rejeté sa demande d'asile. Par une décision n° 19013195 du 11 octobre 2019, la Cour nationale du droit d'asile a rejeté sa demande. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 7 novembre 2019 et 13 mars 2020 au secrétariat du contentieux du Conseil d'Etat, M. A... B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette décision ;<br/>
<br/>
              2°) de mettre à la charge de l'OFPRA la somme de 3 000 euros à verser à la SCP Melka, Prigent, son avocat, au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu :<br/>
              - la convention de Genève du 28 juillet 1951 relative au statut des réfugiés et le protocole signé à New York le 31 janvier 1967 ;<br/>
              - les règlements (UE) n° 603/2013 et n° 604/2013 du Parlement européen et du Conseil du 26 juin 2013 ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bruno Delsol, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Melka - Prigent, avocat de M. A... B... et à la SCP Foussard, Froger, avocat de l'office français de protection des refugies et apatrides ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis à la Cour nationale du droit d'asile que l'Office français de protection des réfugiés et apatrides (OFPRA) a rejeté la demande d'asile présentée par M. A... B..., ressortissant somalien, par une décision du 14 février 2019 retenant que les déclarations de l'intéressé ne permettaient pas de considérer fondées les craintes de persécution alléguées. Par une décision du 11 octobre 2019, la Cour nationale du droit d'asile a rejeté le recours de M. A... B... contre cette décision en se fondant sur le motif que les autorités italiennes lui avaient reconnu le bénéfice de la protection subsidiaire le 4 décembre 2015. M. A... B... se pourvoit en cassation contre cette décision. <br/>
<br/>
              2. Aux termes de l'article R. 733-16 du code de l'entrée et du séjour des étrangers et du droit d'asile : " La formation de jugement ne peut se fonder sur des éléments d'information extérieurs au dossier relatifs à des circonstances de fait propres au demandeur d'asile ou spécifiques à son récit, sans en avoir préalablement informé les parties. / Les parties sont préalablement informées lorsque la formation de jugement est susceptible de fonder sa décision sur un moyen soulevé d'office, notamment celui tiré de ce que le demandeur relèverait de l'une des clauses d'exclusion figurant aux sections D, E et F de l'article 1er de la convention de Genève du 28 juillet 1951 relative au statut des réfugiés ou à l'article L. 712-2 du code de l'entrée et du séjour des étrangers et du droit d'asile. / Un délai est fixé aux parties pour déposer leurs observations, sans qu'y fasse obstacle la clôture de l'instruction écrite ". Eu égard tant à la nature des motifs susceptibles de fonder le refus d'examiner le bien-fondé d'une demande d'asile présentée pour la première fois en France qu'aux effets susceptibles de s'y attacher pour celui qui remplirait par ailleurs les conditions requises pour obtenir le bénéfice du statut de réfugié ou de la protection subsidiaire, la Cour nationale du droit d'asile ne peut opposer au demandeur la circonstance qu'il bénéficierait d'une protection dans un autre Etat membre de l'Union européenne alors que l'OFPRA n'a pas retenu ce motif dans sa décision et ne l'a pas invoqué en défense devant elle, qu'après avoir mis l'intéressé à même de s'en expliquer préalablement à la tenue de l'audience. <br/>
<br/>
              3. Il ressort des énonciations de la décision attaquée que, pour rejeter la demande de M. A... B..., la Cour nationale du droit d'asile s'est fondée sur le motif tiré de ce que les autorités italiennes lui auraient reconnu le bénéfice de la protection subsidiaire par une décision en date du 4 décembre 2015. Or, il ressort des pièces du dossier de la procédure suivie devant la Cour que, d'une part, l'OFPRA, qui n'avait pas rejeté la demande de M. A... B... pour ce motif, n'avait pas invoqué ce moyen dans ses écritures en défense et que, d'autre part, M. A... B... n'a pas été mis à même de s'expliquer sur ce motif, que la Cour lui a au demeurant opposé au seul vu d'un courrier électronique de la préfecture de Loire-Atlantique. En statuant ainsi sans avoir mis en oeuvre les dispositions de l'article R. 733-16 du code de l'entrée et du séjour des étrangers et du droit d'asile, la Cour nationale du droit d'asile a entaché sa décision d'irrégularité. <br/>
<br/>
              4. Il résulte de ce qui précède, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, que M. A... B... est fondé à demander l'annulation de la décision qu'il attaque.<br/>
<br/>
              5. M. A... B... a obtenu le bénéfice de l'aide juridictionnelle. Par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. Il y a lieu, dans les circonstances de l'espèce, et sous réserve que la SCP Melka, Prigent, avocat de M. A... B..., renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de l'OFPRA la somme de 3 000 euros à verser à cette société.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : La décision du 11 octobre 2019 de la Cour nationale du droit d'asile est annulée.<br/>
Article 2 : L'affaire est renvoyée devant la Cour nationale du droit d'asile.<br/>
Article 3 : L'OFPRA versera à la SCP Melka, Prigent une somme de 3 000 euros au titre de l'article 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
Article 4 : La présente décision sera notifiée à M. C... A... B... et à l'Office français de protection des réfugiés et apatrides. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">095-08-02-03-02 - MOYEN TIRÉ DE CE QUE LE DEMANDEUR BÉNÉFICIE D'UNE PROTECTION DANS UN AUTRE ETAT MEMBRE DE L'UNION EUROPÉENNE - MOYEN DEVANT ÊTRE SOUMIS AU CONTRADICTOIRE AVANT L'AUDIENCE (ART. R. 733-16 DU CESEDA) [RJ1].
</SCT>
<ANA ID="9A"> 095-08-02-03-02 Eu égard tant à la nature des motifs susceptibles de fonder le refus d'examiner le bien-fondé d'une demande d'asile présentée pour la première fois en France qu'aux effets susceptibles de s'y attacher pour celui qui remplirait par ailleurs les conditions requises pour obtenir le bénéfice du statut de réfugié ou de la protection subsidiaire, la Cour nationale du droit d'asile (CNDA) ne peut, sans méconnaître l'article R. 733-16 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA), opposer au demandeur la circonstance qu'il bénéficierait d'une protection dans un autre Etat membre de l'Union européenne alors que l'Office français de protection des réfugiés et apatrides (OFPRA) n'a pas retenu ce motif dans sa décision et ne l'a pas invoqué en défense devant elle, qu'après avoir mis l'intéressé à même de s'en expliquer préalablement à la tenue de l'audience.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., s'agissant d'un moyen tiré de ce que l'intéressé peut se prévaloir de la protection d'un autre pays dont il a également la nationalité, CE, 11 mai 2016, M.,et autres, n° 390351, T. p. 648.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
