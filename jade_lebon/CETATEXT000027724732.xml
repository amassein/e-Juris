<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027724732</ID>
<ANCIEN_ID>JG_L_2013_07_000000365671</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/72/47/CETATEXT000027724732.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 1ère sous-sections réunies, 17/07/2013, 365671</TITRE>
<DATE_DEC>2013-07-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>365671</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 1ère sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>Mme Sophie Roussel</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:365671.20130717</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et les mémoires complémentaires, enregistrés les 1er, 15 février et 3 juin 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour la société Hess Oil France, dont le siège est Le Centorial, 16/18, rue du 4 Septembre à Paris (75002) ; la société Hess Oil France demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'ordonnance n° 1202149 du 17 janvier 2013 par laquelle le juge des référés du tribunal administratif de Châlons-en-Champagne, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, a rejeté sa demande tendant à la suspension de la décision du 14 mai 2012 par laquelle le ministre de l'écologie a implicitement refusé la prolongation pour quatre années du permis de recherches de mines d'hydrocarbures liquides ou gazeux délivré le 27 juillet 2007, dénommé " permis de Mairy ", jusqu'à ce qu'il soit statué au fond sur la légalité de cette décision et à ce qu'il soit enjoint au même ministre, à titre principal, d'accorder cette prolongation dans un délai de quinze jours à compter de l'ordonnance susvisée, sous astreinte de 10 000 euros par jour de retard, et à titre subsidiaire, de réexaminer la demande formée le 14 avril 2011 ; <br/>
<br/>
              2°) réglant l'affaire au titre de la procédure de référé engagée, de suspendre la décision du 14 mai 2012 et d'enjoindre aux ministres chargés des mines, à titre principal, d'accorder la prolongation du permis délivré le 27 juillet 2007 dans un délai de quinze jours à compter de la décision à intervenir, sous astreinte de 10 000 euros par jour de retard, ou, à titre subsidiaire, de réexaminer la demande de prolongation de ce permis, dans un délai de quinze jours à compter de la décision à intervenir, sous astreinte de 10 000 euros par jour de retard ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 27 juin 2013, présentée pour la société Hess Oil France ; <br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu le code minier ; <br/>
<br/>
              Vu l'ordonnance n° 2011-91 du 20 janvier 2011 ; <br/>
<br/>
              Vu le décret n° 2006-648 du 2 juin 2006 ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie Roussel, Auditeur,  <br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat de la société Hess Oil France ;<br/>
<br/>
<br/>
<br/>
<br/>
              	1. Considérant qu'aux termes de l'article L. 142-1 du code minier : " La validité d'un permis exclusif de recherches peut être prolongée à deux reprises, chaque fois de cinq ans au plus, sans nouvelle mise en concurrence. / Chacune de ces prolongations est de droit, soit pour une durée au moins égale à trois ans, soit pour la durée de validité précédente si cette dernière est inférieure à trois ans, lorsque le titulaire a satisfait à ses obligations et souscrit dans la demande de prolongation un engagement financier au moins égal à l'engagement financier souscrit pour la période de validité précédente, au prorata de la durée de validité et de la superficie sollicitées " ; qu'aux termes de l'article L. 142-6 du même code : " Au cas où, à la date d'expiration de la période de validité en cours, il n'a pas été statué sur la demande de prolongation, le titulaire du permis reste seul autorisé, jusqu'à l'intervention d'une décision explicite de l'autorité administrative, à poursuivre ses travaux dans les limites du ou des périmètres sur lesquels porte la demande de prolongation " ; qu'aux termes de l'article 49 du décret du 2 juin 2006 relatif aux titres miniers et aux titres de stockage souterrain, dans sa version issue de l'ordonnance du 20 janvier 2011 portant codification de la partie législative du code minier : " (...). / Le silence gardé pendant plus de deux ans par le ministre chargé des mines sur la demande de prolongation d'une concession et pendant plus de quinze mois sur la demande de prolongation d'un permis de recherches vaut décision de rejet " ; <br/>
<br/>
              2. Considérant qu'il résulte de la combinaison de ces dispositions que, lorsque le titulaire d'un permis exclusif de recherches de mines souhaite prolonger la validité de ce permis, il lui appartient de saisir le ministre chargé des mines d'une demande de prolongation de la validité de ce titre dans les conditions précisées à l'article 46 du décret du 2 juin 2006 ; qu'en vertu de l'article L. 142-1 du code minier, la prolongation du titre est de droit dès lors que le titulaire a satisfait à deux exigences, tirées, d'une part, du respect par ce dernier des obligations visées à l'article L. 122-1 du code pour préserver les intérêts mentionnés à l'article L. 161-1 et aux articles L. 161-1 et L. 163-1 à L. 163-9, d'autre part, de la souscription dans la demande de prolongation d'un engagement financier au moins égal à l'engagement financier souscrit pour la période de validité précédente, au prorata de la durée de validité et de la superficie sollicitées ; qu'à l'expiration d'un délai de quinze mois après la saisine du ministre, le silence gardé par celui-ci fait naître une décision implicite de rejet de la demande de prolongation ; que, dans le cas où la validité du titre arrive à échéance alors qu'il n'a pas encore été statué sur la demande de prolongation du permis, le titulaire du permis est autorisé, en vertu de l'article L. 142-6 du code, à poursuivre ses travaux dans les limites du ou des périmètres sur lesquels porte la demande de prolongation, nonobstant l'intervention d'une décision implicite de rejet de sa demande de prolongation, seule l'intervention d'une décision explicite de rejet pouvant alors mettre fin à la possibilité qui lui est reconnue ;<br/>
<br/>
              3. Considérant qu'il résulte de ce qui précède qu'en jugeant que l'article L. 142-6 du code dérogeait à la règle prévue par les dispositions de l'article 49 du décret du 2 juin 2006 citées ci-dessus et faisait obstacle à la naissance d'une décision implicite de rejet de la demande de prolongation pour quatre années de la validité du permis exclusif de recherches minières d'hydrocarbures liquides ou gazeux sur une partie du territoire du département de la Marne, dit " permis de Mairy " en date du 14 février 2011, le juge des référés du tribunal administratif de Châlons-en-Champagne a commis une erreur de droit ;<br/>
<br/>
              4. Considérant, par suite, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que la société requérante est fondée à demander l'annulation de l'ordonnance attaquée ; <br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de statuer sur la demande en référé en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
<br/>
              En ce qui concerne les fins de non-recevoir opposées par la ministre de l'écologie, du développement durable et de l'énergie : <br/>
<br/>
              6. Considérant, en premier lieu, qu'ainsi qu'il a été dit au point 3, une décision implicite de rejet est née le 14 mai 2012 du silence gardé par la ministre pendant plus de quinze mois sur la demande de prolongation du permis dit " de Mairy " ; que, par suite, la fin de non-recevoir opposée par la ministre tirée de l'absence de décision ne peut qu'être écartée ; <br/>
<br/>
              7. Considérant, en second lieu, que si la société Hess Oil France n'est pas le titulaire originel du permis exclusif de recherche dit " de Mairy ", cette société a néanmoins engagé d'importants investissements en vue de la mutation à son profit de ce permis ; qu'ainsi, elle justifie d'un intérêt suffisant lui donnant qualité pour demander la suspension de l'exécution de la décision implicite du 14 mai 2012 ; <br/>
<br/>
<br/>
              En ce qui concerne l'urgence : <br/>
<br/>
              8. Considérant qu'aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision (...) " ; <br/>
<br/>
              9. Considérant qu'il résulte de ces dispositions que la condition d'urgence à laquelle est subordonné le prononcé d'une mesure de suspension doit être regardée comme remplie lorsque la décision contestée préjudicie de manière suffisamment grave et immédiate à la situation du requérant et aux intérêts qu'il entend défendre ; qu'il en va ainsi, alors même que cette décision n'aurait un objet ou des répercussions que purement financiers et qu'en cas d'annulation, ses effets pourraient être effacés par une réparation pécuniaire ; qu'il appartient au juge des référés, saisi d'une demande tendant à la suspension d'une telle décision, d'apprécier concrètement, compte tenu des justifications fournies par le requérant, si les effets de celle-ci sur la situation de ce dernier, ou, le cas échéant, sur les personnes concernées, sont de nature à caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, l'exécution de la décision soit suspendue ;<br/>
<br/>
              10. Considérant que, pour demander la suspension de l'exécution de la décision implicite du 14 mai 2012 refusant la prolongation pour quatre années du " permis de Mairy ", la société Hess Oil France fait valoir, pour justifier de la condition d'urgence,  que la décision lui cause un préjudice économique et financier, la tardiveté d'une prolongation du permis pouvant affecter son utilité et remettre en cause les importants investissements réalisés ; <br/>
<br/>
              11. Considérant qu'il résulte de l'instruction, d'une part, que la société Hess Oil France fait valoir, sans être contredite, qu'outre les engagements financiers significatifs pris en vue d'apporter la preuve à l'administration de ses capacités financières, elle a engagé d'importants investissements en vue de l'exploration du périmètre du " permis de Mairy ", soit 3,4 millions de dollars américains depuis le 25 juin 2010, sans avoir pu réaliser de forage d'exploration compte tenu de l'incertitude affectant l'obtention d'un titre l'autorisant de façon pérenne à effectuer les travaux de recherches ; que les sommes ainsi investies risquent d'avoir été engagées en pure perte ; qu'il ne résulte pas de l'instruction que les investissements auraient été réalisés dans des conditions imprudentes ; qu'ainsi, la décision attaquée préjudicie de manière grave et immédiate à la situation économique et financière de la société ; que, d'autre part, la ministre de l'écologie ne fait valoir aucun motif d'intérêt général de nature à faire obstacle à la suspension sollicitée ; que, par suite, la condition d'urgence énoncée à l'article L. 521-1 du code de justice administrative doit être regardée comme remplie ;<br/>
<br/>
<br/>
              En ce qui concerne l'existence de moyens propres à faire naître un doute sérieux quant à la légalité de la décision dont la suspension est demandée : <br/>
<br/>
              12. Considérant que le moyen tiré de l'erreur manifeste d'appréciation du ministre à avoir estimé que les conditions auxquelles est subordonnée, en vertu de l'article L. 142-1 du code minier cité ci-dessus, la prolongation du permis exclusif de recherche n'étaient pas remplies, est de nature, en l'état de l'instruction, à créer un doute sérieux quant à la légalité de la décision attaquée ;<br/>
<br/>
              13. Considérant qu'il résulte de ce qui précède que la société Hess Oil est fondée à demander la suspension de l'exécution de la décision du 14 mai 2012 ;<br/>
<br/>
<br/>
              En ce qui concerne les conclusions à fin d'injonction et d'astreinte : <br/>
<br/>
              14. Considérant qu'aux termes de l'article L. 911-2 du code de justice administrative : "Lorsque sa décision implique nécessairement qu'une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public prenne à nouveau une décision après une nouvelle instruction, la juridiction, saisie de conclusions en ce sens, prescrit, par la même décision juridictionnelle, que cette nouvelle décision doit intervenir dans un délai déterminé" ;<br/>
<br/>
              15. Considérant que la présente décision implique seulement que la ministre réexamine la demande de la société Hess Oil France ; qu'il y a lieu de lui enjoindre de procéder à ce réexamen dans un délai de deux mois à compter de la notification de la décision, sans qu'il y ait toutefois lieu d'assortir cette injonction d'une astreinte ;<br/>
<br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative : <br/>
<br/>
              16. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à la société Hess Oil France au titre des dispositions de l'article L. 761-1 du code de justice administrative ;  <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du juge des référés du tribunal administratif de Châlons-en-Champagne du 17 janvier 2013 est annulée. <br/>
Article 2 : L'exécution de la décision implicite de rejet née le 14 mai 2012 du silence gardé  par la ministre de l'écologie, du développement durable et de l'énergie pendant plus de quinze mois sur la demande de prolongation du permis exclusif de recherche dit " de Mairy " est suspendue. <br/>
Article 3 : Il est enjoint au ministre de l'écologie, du développement durable et de l'énergie de procéder, dans le délai de deux mois à compter de la notification de la présente décision, au réexamen de la demande de prolongation du permis dit " de Mairy ". <br/>
Article 4 : L'Etat versera à la société Hess Oil France une somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
Article 5 : La présente décision sera notifiée à la société Hess Oil France, au ministre de l'écologie, du développement durable et de l'énergie et au ministre du redressement productif. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">40-01-01 MINES ET CARRIÈRES. MINES. RECHERCHE DES MINES. - DEMANDE DE PROLONGATION DE VALIDITÉ D'UN PERMIS EXCLUSIF DE RECHERCHES DE MINES - SILENCE GARDÉ PAR L'ADMINISTRATION AU TERME DU DÉLAI DE 15 MOIS IMPARTI - EFFET - DÉCISION IMPLICITE DE REJET - LIMITE - TITULAIRE AUTORISÉ À POURSUIVRE LES TRAVAUX (ART. L. 142-6 DU CODE MINIER), SEULE UNE DÉCISION EXPLICITE POUVANT METTRE FIN À CETTE POSSIBILITÉ.
</SCT>
<ANA ID="9A"> 40-01-01 Il résulte de la combinaison des dispositions des articles L. 142-1 et L. 142-6 du code minier ainsi que de l'article 49 du décret n° 2006-648 du 2 juin 2006 relatif aux titres miniers et aux titres de stockage souterrain, dans sa version issue de l'ordonnance n° 2011-91 du 20 janvier 2011 portant codification de la partie législative du code minier, que lorsque le titulaire d'un permis exclusif de recherches de mines souhaite prolonger la validité de ce permis, il lui appartient de saisir le ministre chargé des mines d'une demande de prolongation de la validité de ce titre dans les conditions précisées à l'article 46 du décret du 2 juin 2006.,,,En vertu de l'article L. 142-1 du code minier, la prolongation du titre est de droit dès lors que le titulaire a satisfait à deux exigences, tirées, d'une part, du respect par ce dernier des obligations visées à l'article L. 122-1 du code pour préserver les intérêts mentionnés à l'article L. 161-1 et aux articles L. 161-1 et L. 163-1 à L. 163-9, d'autre part, de la souscription dans la demande de prolongation d'un engagement financier au moins égal à l'engagement financier souscrit pour la période de validité précédente, au prorata de la durée de validité et de la superficie sollicitées.,,,A l'expiration d'un délai de quinze mois après la saisine du ministre, le silence gardé par celui-ci fait naître une décision implicite de rejet de la demande de prolongation. Dans le cas où la validité du titre arrive à échéance alors qu'il n'a pas encore été statué sur la demande de prolongation du permis, le titulaire du permis est autorisé, en vertu de l'article L. 142-6 du code, à poursuivre ses travaux dans les limites du ou des périmètres sur lesquels porte la demande de prolongation, nonobstant l'intervention d'une décision implicite de rejet de sa demande de prolongation, seule l'intervention d'une décision explicite de rejet pouvant alors mettre fin à la possibilité qui lui est reconnue.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
