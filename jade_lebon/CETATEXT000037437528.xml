<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037437528</ID>
<ANCIEN_ID>JG_L_2018_09_000000407856</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/43/75/CETATEXT000037437528.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème et 1ère chambres réunies, 26/09/2018, 407856</TITRE>
<DATE_DEC>2018-09-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>407856</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème et 1ère chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN ; SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR>Mme Françoise Tomé</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:407856.20180926</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu les procédures suivantes :<br/>
<br/>
              Procédure contentieuse antérieure<br/>
<br/>
              Le conseil départemental de la Ville de Paris de l'ordre des médecins a porté plainte contre M. B... A...devant la chambre disciplinaire de première instance d'Ile-de-France de l'ordre des médecins. Par une décision du 21 mai 2015, la chambre disciplinaire a infligé à M. A...la sanction de l'interdiction d'exercer la médecine pendant deux ans, dont vingt et un mois assortis du sursis. <br/>
<br/>
              Par une décision du 13 décembre 2016, la chambre disciplinaire nationale de l'ordre des médecins a, sur appels du conseil départemental de la Ville de Paris de l'ordre des médecins et de M.A..., infligé à M. A...la sanction de l'interdiction d'exercer la médecine pendant deux ans, dont un an assorti du sursis et décidé que la sanction prendrait effet à compter du 1er mai 2017.<br/>
<br/>
              Procédures devant le Conseil d'Etat :<br/>
<br/>
              1° Sous le n° 407856, par un pourvoi, un mémoire complémentaire, un mémoire en réplique et un nouveau mémoire, enregistrés les 13 février, 15 mai et 19 octobre  2017 et le 29 janvier 2018 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette décision ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
              3°) de mettre à la charge du conseil départemental de la Ville de Paris de l'ordre des médecins la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              2° Sous le n° 410550, par une requête, un mémoire en réplique et un nouveau mémoire, enregistrés au secrétariat du contentieux du Conseil d'Etat les 15 mai, 19 octobre 2017et le 29 janvier 2018, M. A...demande au Conseil d'Etat de suspendre l'exécution de cette même décision. Il soutient qu'elle entraîne pour lui des conséquences difficilement réparables pour faire face à ses charges et pour son avenir professionnel mais aussi du fait de son état de santé. Il soulève, au fond, les mêmes moyens que sous le n° 407856.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Françoise Tomé, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de M. A...et à la SCP Matuchansky, Poupot, Valdelièvre, avocat du conseil départemental de la ville de Paris de l'ordre des médecins et du conseil national de l'ordre des médecins ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M.A..., médecin spécialisé en chirurgie plastique, reconstructrice et esthétique, a fait l'objet de plaintes par le conseil départemental de la Ville de Paris de l'ordre des médecins, du fait, d'une part, de sa participation à des émissions pour lesquelles il a accepté d'être filmé pendant des consultations et des interventions chirurgicales, dont il a lui-même commenté le succès sur son " profil facebook " professionnel, notamment dans des épisodes de l'émission " la conciergerie de Jeremstar ", reprises sur différents sites internet, et dans l'émission " Tellement vrai - les chirurgiens de la beauté ", diffusée sur la chaîne de télévision NRJ 12 et sur Youtube, et, d'autre part, de citations de ses propos dans un article du magazine " Public ", portant en une de couverture un titre relatif à une vedette de la téléréalité " Nabila, la vérité sur ses seins ", dans lequel il est présenté comme le " chirurgien esthétique des stars "  ; que M. A...se pourvoit en cassation contre la décision du 13 décembre 2016 par laquelle la chambre disciplinaire nationale de l'ordre des médecins lui a infligé la sanction de l'interdiction du droit d'exercer la médecine pendant deux ans ; que, par une requête qu'il y a lieu de joindre à son pourvoi pour statuer par une seule décision, M. A...demande également qu'il soit sursis à l'exécution de cette sanction ; <br/>
<br/>
              Sur le pourvoi :<br/>
<br/>
              2. Considérant, en premier lieu, qu'aux termes de l'article R. 4127-4 du code de la santé publique : " Le secret professionnel institué dans l'intérêt des patients s'impose à tout médecin dans les conditions établies par la loi. / Le secret couvre tout ce qui est venu à la connaissance du médecin dans l'exercice de sa profession, c'est-à-dire non seulement ce qui lui a été confié, mais aussi ce qu'il a vu, entendu ou compris " ; <br/>
<br/>
              3. Considérant qu'en estimant que l'épisode de la série " Les chirurgiens de la beauté " diffusé sur la chaîne " NRJ 12 " permettait l'identification de la patiente opérée par M. A..., la chambre disciplinaire nationale n'a pas dénaturé les pièces du dossier qui lui était soumis ; qu'en estimant que, malgré les démentis de M. A...à la suite de la sortie du magazine " Public ", il n'avait pas contesté être le médecin de la personne dont l'état de santé et l'identité étaient ainsi divulgués, la chambre disciplinaire nationale ne s'est pas fondée sur des faits matériellement inexacts et a porté sur ces faits une appréciation souveraine, exempte de dénaturation ;<br/>
<br/>
              4. Considérant qu'en déduisant de ces constatations que,  alors même que les patientes concernées auraient, par leur participation à ce type d'émissions ou leur consentement à l'article de presse mentionné ci-dessus, sciemment recherché la médiatisation et consenti à la révélation de leur identité,  le concours apporté par M.A... à la divulgation de l'identité de patientes à l'occasion d'émissions ou d'articles était constitutif d'une méconnaissance des dispositions, citées ci-dessus, de l'article R. 4127-4 du code de la santé publique, qui prohibent la violation du secret médical, la chambre disciplinaire nationale a, par une décision qui est suffisamment motivée sur ce point, exactement qualifié les faits qui lui étaient soumis et n'a pas commis d'erreur de droit ;<br/>
<br/>
              5. Considérant, en deuxième lieu, qu'aux termes de l'article R. 4127-19 du code de la santé publique : " La médecine ne doit pas être pratiquée comme un commerce./ Sont interdits tous procédés directs ou indirects de publicité et notamment tout aménagement ou signalisation donnant aux locaux une apparence commerciale " ; qu'aux termes de l'article R. 4127-20 du même code : " Le médecin doit veiller à l'usage qui est fait de son nom, de sa qualité ou de ses déclarations./ Il ne doit pas tolérer que les organismes, publics ou privés, où il exerce ou auxquels il prête son concours utilisent à des fins publicitaires son nom ou son activité professionnelle " ; <br/>
<br/>
              6. Considérant que le moyen tiré de ce que les dispositions citées ci-dessus seraient contraires aux stipulations de l'article 56 du traité sur le fonctionnement de l'Union européenne n'a pas été soulevé devant le juge du fond et n'est pas d'ordre public ; qu'il est, par suite, inopérant en cassation ;<br/>
<br/>
              7. Considérant qu'en estimant que ni l'épisode de la série " Les chirurgiens de la beauté " diffusé sur la chaîne " NRJ 12 " ni les émissions diffusées sur le site internet du " blogueur Jeremstar " consacrées à des opérations de chirurgie esthétique réalisées sur des vedettes de " téléréalité " n'avaient un caractère purement informatif et qu'elles revêtaient, au moins pour partie, un caractère publicitaire, la chambre disciplinaire nationale ne s'est pas fondée sur des faits matériellement inexacts et a porté sur les faits qui lui étaient soumis une appréciation souveraine, exempte de dénaturation ; <br/>
<br/>
              8. Considérant qu'en jugeant que la participation de M. A...à ces émissions ainsi que les commentaires qu'il en diffusait sur les réseaux " Facebook " et " Twitter ", étaient constitutifs d'une méconnaissance des dispositions, citées ci-dessus, des articles R. 4127-19 et R. 4127-20 du code de la santé publique, qui prohibent l'utilisation de procédés publicitaires, la chambre disciplinaire nationale a, par une décision qui est suffisamment motivée sur ce point, exactement qualifié les faits qui lui étaient soumis et n'a pas commis d'erreur de droit ;<br/>
              9. Considérant, en troisième lieu, qu'aux termes de l'article R. 4127-31 du code la santé publique : " Tout médecin doit s'abstenir, même en dehors de l'exercice de sa profession, de tout acte de nature à déconsidérer celle-ci " ;<br/>
<br/>
              10. Considérant qu'en jugeant que la façon dont M. A...avait, tant par sa participation aux émissions mentionnées ci-dessus que par de nombreux messages sur les réseaux sociaux, vanté sa pratique médicale et " mis en scène " sa vie privée et professionnelle, était constitutive d'une méconnaissance des dispositions, citées ci-dessus, de l'article R. 4127-31 du code de la santé publique, qui prohibent les actes de nature à déconsidérer la profession de médecin, la chambre disciplinaire nationale a exactement qualifié les faits qui lui étaient soumis et n'a pas commis d'erreur de droit ;<br/>
<br/>
              11. Considérant, enfin, que si le choix de la sanction relève de l'appréciation des juges du fond au vu de l'ensemble des circonstances de l'espèce, il appartient au juge de cassation de vérifier que la sanction retenue n'est pas hors de proportion avec la faute commise et qu'elle a pu dès lors être légalement prise ; que la chambre disciplinaire nationale de l'ordre des médecins a pu légalement estimer que, en dépit de l'inexpérience revendiquée par M. A...à l'égard du " tourbillon médiatique " dans lequel il aurait été entraîné, les comportements qui lui étaient  reprochés justifiaient, eu égard à leur nature et à leur gravité, la sanction prononcée ;<br/>
<br/>
              12. Considérant qu'il résulte de tout ce qui précède que M. A...n'est pas fondé à demander l'annulation de la décision qu'il attaque ; que son pourvoi doit être rejeté, y compris, par voie de conséquences, ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
              Sur la requête aux fins de sursis à exécution :<br/>
<br/>
              13. Considérant que, par la présente décision, le Conseil d'Etat rejette le pourvoi formé par M. A...contre la décision de la chambre disciplinaire nationale de l'ordre des médecins du 13 décembre 2016 ; que, par suite, les conclusions de la requête de M. A...tendant à ce qu'il soit sursis à l'exécution de cette même décision de la chambre disciplinaire nationale sont devenues sans objet ; qu'il n'y a plus lieu d'y statuer ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. A...est rejeté. <br/>
Article 2 : Il n'y a pas lieu de statuer sur la requête de sursis à exécution de M.A....<br/>
Article 3 : La présente décision sera notifiée à M. B... A...et au Conseil national de l'ordre des médecins.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">26-03-10 DROITS CIVILS ET INDIVIDUELS. LIBERTÉS PUBLIQUES ET LIBERTÉS DE LA PERSONNE. SECRET DE LA VIE PRIVÉE. - SECRET MÉDICAL (ART. R. 4127-4 DU CSP) - VIOLATION - CONCOURS APPORTÉ PAR LE MÉDECIN À LA DIVULGATION DE L'IDENTITÉ DE PATIENTS, QUAND BIEN MÊME ILS AURAIENT SCIEMMENT RECHERCHÉ LA MÉDIATISATION ET CONSENTI À LA RÉVÉLATION DE LEUR IDENTITÉ [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">55-04-02-01-01 PROFESSIONS, CHARGES ET OFFICES. DISCIPLINE PROFESSIONNELLE. SANCTIONS. FAITS DE NATURE À JUSTIFIER UNE SANCTION. MÉDECINS. - VIOLATION DU SECRET MÉDICAL (ART. R. 4127-4 DU CSP) - CONCOURS APPORTÉ À LA DIVULGATION DE L'IDENTITÉ DE PATIENTS, QUAND BIEN MÊME ILS AURAIENT SCIEMMENT RECHERCHÉ LA MÉDIATISATION ET CONSENTI À LA RÉVÉLATION DE LEUR IDENTITÉ [RJ1].
</SCT>
<ANA ID="9A"> 26-03-10 Alors même que des patients auraient, par leur participation à des émissions télévisées ou leur consentement à un article de presse, sciemment recherché la médiatisation et consenti à la révélation de leur identité, le concours apporté par le médecin à la divulgation de leur identité à l'occasion d'émissions ou d'articles est constitutif d'une méconnaissance de l'article R. 4127-4 du code de la santé publique (CSP), qui prohibe la violation du secret médical.</ANA>
<ANA ID="9B"> 55-04-02-01-01 Alors même que des patients auraient, par leur participation à des émissions télévisées ou leur consentement à un article de presse, sciemment recherché la médiatisation et consenti à la révélation de leur identité, le concours apporté par le médecin à la divulgation de leur identité à l'occasion d'émissions ou d'articles est constitutif d'une méconnaissance de l'article R. 4127-4 du code de la santé publique (CSP), qui prohibe la violation du secret médical.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 28 mai 1999, M.,, n° 189057, p. 159, CE, 29 décembre 2000, M.,, n° 211240, p. 676.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
