<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028161261</ID>
<ANCIEN_ID>JG_L_2013_11_000000359501</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/16/12/CETATEXT000028161261.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 4ème sous-sections réunies, 06/11/2013, 359501</TITRE>
<DATE_DEC>2013-11-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>359501</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 4ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MASSE-DESSEN, THOUVENIN, COUDRAY ; SCP BARTHELEMY, MATUCHANSKY, VEXLIARD</AVOCATS>
<RAPPORTEUR>M. Olivier Rousselle</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:359501.20131106</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 18 mai et 20 août 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. B... A..., demeurant... ; M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement n°1100930 et 1101421 du 28 février 2012 par lequel le tribunal administratif de Dijon a rejeté sa demande tendant, d'une part, à l'annulation de la décision du 26 août 2010 du directeur du centre hospitalier de Nevers refusant de modifier ses plannings de travail des mois d'août et septembre 2010 et à la condamnation du centre hospitalier à lui verser la somme de 3 000 euros en réparation du préjudice subi du fait de ce refus, d'autre part, à l'annulation de la décision du 23 mars 2011 du directeur du centre hospitalier refusant de modifier sa fiche de notation pour 2010 et, enfin, à ce qu'il soit enjoint au centre hospitalier de reprendre une décision concernant sa situation dans un délai de deux semaines à compter du prononcé du jugement, sous astreinte de 50 euros par jour de retard ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ; <br/>
<br/>
              3°) de mettre à la charge du centre hospitalier de Nevers le versement d'une somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi n° 83-634 du 13 juillet 1983 ;<br/>
<br/>
              Vu la loi n° 86-33 du 9 janvier 1986 ;<br/>
<br/>
              Vu le décret n° 2002-9 du 4 janvier 2002 ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Rousselle, Conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Masse-Dessen, Thouvenin, Coudray, avocat de M. A...et à la SCP Barthélemy, Matuchansky, Vexliard, avocat du centre hospitalier de Nevers ;<br/>
<br/>
<br/>
<br/>
              1. Considérant que, par une décision du 26 août 2010, le directeur du centre hospitalier de Nevers a rejeté une demande de M.A..., ouvrier professionnel qualifié, tendant à la modification de ses plannings de travail pour les mois d'août et septembre 2010 ; que, par une décision du 23 mars 2011, il a rejeté une demande de M. A...tendant à la modification de sa notation pour 2010 ; que l'intéressé a demandé au tribunal administratif de Dijon d'annuler ces deux décisions et de condamner le centre hospitalier de Nevers à réparer le préjudice qu'il estimait avoir subi du fait de la première ; qu'il se pourvoit en cassation contre le jugement du 28 février 2012 par lequel le tribunal administratif a rejeté l'ensemble de ses conclusions ;<br/>
<br/>
              Sur le jugement en tant qu'il a rejeté la demande d'annulation de la décision du 26 août 2010 :<br/>
<br/>
              2. Considérant qu'aux termes de l'article 6 du décret du 4 janvier 2002 : " L'organisation du travail doit respecter les garanties ci-après définies (...) Les agents bénéficient d'un repos quotidien de 12 heures consécutives minimum et d'un repos hebdomadaire de 36 heures consécutives minimum. / Le nombre de jours de repos est fixé à 4 jours pour 2 semaines, deux d'entre eux, au moins, devant être consécutifs, dont un dimanche. " ; qu'aux termes du premier alinéa de l'article 9 du même décret : " Le travail est organisé selon des périodes de référence dénommées cycles de travail définis par service ou par fonctions et arrêtés par le chef d'établissement après avis du comité technique d'établissement ou du comité technique " ; <br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que le cycle de travail de douze heures consécutives avec alternance entre travail de jour et de nuit, qui était appliqué à l'ensemble du service de sécurité incendie du centre hospitalier de Nevers, à la suite d'un accord approuvé en comité technique d'établissement le 16 avril 2002, avait pour effet, pour certains agents travaillant de nuit, comme M. A..., de ne pas leur permettre de bénéficier d'un dimanche complet de repos tous les quinze jours, en méconnaissance des dispositions précitées de l'article 6 du décret du 4 janvier 2002 qui fixent le nombre de jours de repos à quatre pour deux semaines, deux d'entre eux, au moins, devant être consécutifs, dont un dimanche ; que les dispositions de l'article 9 du même décret, qui confient au chef d'établissement le soin d'arrêter des cycles de travail, après avis du comité technique d'établissement ou du comité technique, ne l'autorisent pas à déroger aux règles édictées par ailleurs par le décret et notamment à celles prévues à son article 6 ; que, dès lors, le tribunal administratif a commis une erreur de droit en rejetant la demande de M. A...au  motif que le cycle de travail arrêté par le chef d'établissement résultait d'un accord approuvé en comité technique d'établissement ; <br/>
<br/>
              Sur le jugement en tant qu'il a rejeté la demande d'annulation de la décision du 23 mars 2011 :<br/>
<br/>
              4. Considérant, en premier lieu, que si les actes administratifs doivent être pris selon les formes et conformément aux procédures prévues par les lois et règlements, un vice affectant le déroulement d'une procédure administrative préalable, suivie à titre obligatoire ou facultatif, n'est de nature à entacher d'illégalité la décision prise que s'il ressort des pièces du dossier qu'il a été susceptible d'exercer, en l'espèce, une influence sur le sens de la décision prise ou qu'il a privé les intéressés d'une garantie ; <br/>
<br/>
              5. Considérant qu'en estimant que la présence, lors de la réunion du 10 mars 2011 de la commission administrative paritaire, de la directrice-adjointe des ressources humaines du centre hospitalier, chargée d'assurer le secrétariat de la  commission, et son intervention dans les débats relatifs à la notation de M. A..., n'avaient pas été, en l'espèce, susceptibles d'exercer une influence sur le sens de la décision du 23 mars 2011, qui a maintenu sa notation pour 2010, et n'avaient pas privé l'intéressé d'une garantie, le tribunal administratif n'a pas, contrairement à ce que soutient le pourvoi, dénaturé les pièces du dossier ;  <br/>
<br/>
              6. Considérant, en second lieu, qu'en retenant, pour juger que la décision maintenant la notation de M. A...n'était pas fondée sur des faits matériellement inexacts et ne résultait pas d'un détournement de pouvoir, qu'il ressortait des pièces du dossier, notamment d'appréciations de ses supérieurs et du procès-verbal d'une réunion de la commission administrative paritaire, que l'intéressé rencontrait des difficultés dans ses relations de travail au sein du service, le tribunal administratif n'a pas dénaturé les éléments qui lui étaient soumis  ; <br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que M. A...n'est fondé à demander l'annulation du jugement du tribunal administratif de Dijon du 28 février 2012 qu'en tant qu'il a rejeté sa demande tendant à l'annulation de la décision du 26 août 2010 du directeur du centre hospitalier de Nevers et à la réparation de ses conséquences dommageables ;<br/>
<br/>
              8. Considérant qu'il y a lieu de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative, dans la mesure de la cassation prononcée ; <br/>
<br/>
              Sur la demande d'annulation de la décision du 26 août 2010 : <br/>
<br/>
              9. Considérant qu'il résulte de ce qui a été dit ci-dessus que le cycle de travail qui était appliqué à l'ensemble du service de sécurité incendie du centre hospitalier entraînait dans le cas de M. A...la méconnaissance des prescriptions de l'article 6 du décret du 4 janvier 2002 ; qu'il suit de là que M. A...est fondé à demander l'annulation de la décision du 26 août 2010 par laquelle le directeur du centre hospitalier de Nevers a refusé de modifier ses plannings pour les mois d'août et septembre 2010  ; <br/>
<br/>
              Sur les conclusions à fin d'injonction et d'astreinte :<br/>
<br/>
              10. Considérant qu'il ne résulte pas de l'instruction que l'annulation de la décision du 26 août 2010, relative aux plannings de M. A...pour les moins d'août et septembre 2010, entraîne pour le centre hospitalier de Nevers, à la date de la présente décision, l'obligation de prendre une mesure dans un sens déterminé ou de prendre une nouvelle décision ; que, dès lors, les conclusions à fin d'injonction doivent être rejetées ;  <br/>
<br/>
              Sur les conclusions indemnitaires :<br/>
<br/>
              11. Considérant que M. A...demande la réparation du préjudice que l'organisation irrégulière du travail en août et septembre 2010 lui a causé, notamment dans sa vie familiale ; qu'il sera fait une juste appréciation des troubles dans les conditions d'existence subis de ce fait par l'intéressé en condamnant le centre hospitalier de Nevers à lui verser une indemnité de 500 euros ;<br/>
<br/>
              Sur les conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              12. Considérant qu'il y a lieu, en application de ces dispositions, de mettre à la charge du centre hospitalier de Nevers une somme de 3 000 euros au titre des frais exposés par M. A...et non compris dans les dépens, en première instance et devant le Conseil d'Etat ; qu'en revanche, ces dispositions  font obstacle à ce qu'une somme soit mise à la charge de M. A... qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Dijon du 28 février 2012 est annulé en tant qu'il a rejeté la demande n° 1100930 tendant à l'annulation de la décision du 26 août 2010 du directeur du centre hospitalier de Nevers et à la réparation de ses conséquences dommageables.<br/>
<br/>
Article 2 : La décision du 26 août 2010 du directeur du centre hospitalier de Nevers est annulée.<br/>
<br/>
Article 3 : Le centre hospitalier de Nevers est condamné à verser à M. A...la somme de 500 euros.<br/>
<br/>
Article 4 : Le centre hospitalier de Nevers versera à M. A...la somme de 3 000 euros au titre de l'article L.761-1 du code de justice administrative.<br/>
<br/>
Article 5 : Le surplus des conclusions de M. A...et du centre hospitalier de Nevers est rejeté.<br/>
<br/>
Article 6 : La présente décision sera notifiée à M. A...et au centre hospitalier de Nevers.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-03-01 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - FORME ET PROCÉDURE. QUESTIONS GÉNÉRALES. - APPLICATION DE LA JURISPRUDENCE DITE DANTHONY [RJ1] - APPRÉCIATIONS D'ESPÈCE PORTÉES PAR LES JUGES DU FOND SUR LA PRIVATION OU NON D'UNE GARANTIE PAR LE VICE DE PROCÉDURE ET L'INFLUENCE OU NON DE CE DERNIER SUR LE SENS DE LA DÉCISION - APPRÉCIATIONS SOUVERAINES - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">36-07-01-04 FONCTIONNAIRES ET AGENTS PUBLICS. STATUTS, DROITS, OBLIGATIONS ET GARANTIES. STATUT GÉNÉRAL DES FONCTIONNAIRES DE L'ÉTAT ET DES COLLECTIVITÉS LOCALES. DISPOSITIONS STATUTAIRES RELATIVES À LA FONCTION PUBLIQUE HOSPITALIÈRE (LOI DU 9 JANVIER 1986). - TEMPS DE TRAVAIL (DÉCRET DU 4 JANVIER 2002) - COMPÉTENCE DU CHEF D'ÉTABLISSEMENT POUR DÉFINIR DES CYCLES DE TRAVAIL (ART. 9) - PORTÉE - HABILITATION À DÉROGER AUX RÈGLES RELATIVES AU TEMPS DE TRAVAIL ÉDICTÉES PAR AILLEURS PAR LE DÉCRET - ABSENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-08-02-02-01-03 PROCÉDURE. VOIES DE RECOURS. CASSATION. CONTRÔLE DU JUGE DE CASSATION. BIEN-FONDÉ. APPRÉCIATION SOUVERAINE DES JUGES DU FOND. - EXISTENCE - APPLICATION PAR LES JUGES DU FOND DE LA JURISPRUDENCE DITE DANTHONY [RJ1] - APPRÉCIATIONS D'ESPÈCE PORTÉES SUR LA PRIVATION OU NON D'UNE GARANTIE PAR LE VICE DE PROCÉDURE ET L'INFLUENCE OU NON DE CE DERNIER SUR LE SENS DE LA DÉCISION.
</SCT>
<ANA ID="9A"> 01-03-01 Relèvent de l'appréciation souveraine des juges du fond les questions de savoir, dans le cadre de l'application de la jurisprudence dite Danthony, si un vice de procédure a, en l'espèce, privé l'intéressé d'une garantie et s'il a, en l'espèce, exercé une influence sur le sens de la décision.</ANA>
<ANA ID="9B"> 36-07-01-04 Les dispositions de l'article 9 du décret n° 2002-9 du 4 janvier 2002, qui confient au chef d'établissement le soin d'arrêter des cycles de travail, après avis du comité technique d'établissement ou du comité technique, ne l'autorisent pas à déroger aux règles édictées par ailleurs par le décret et notamment à celles prévues à son article 6, relatives à la durée de travail effectif hebdomadaire maximale ainsi qu'aux repos quotidien et hebdomadaire.</ANA>
<ANA ID="9C"> 54-08-02-02-01-03 Relèvent de l'appréciation souveraine des juges du fond les questions de savoir, dans le cadre de l'application de la jurisprudence dite Danthony, si un vice de procédure a, en l'espèce, privé l'intéressé d'une garantie et s'il a, en l'espèce, exercé une influence sur le sens de la décision.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Assemblée, 23 décembre 2011, Danthony et autres, n° 335033, p. 649.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
