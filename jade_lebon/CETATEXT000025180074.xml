<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025180074</ID>
<ANCIEN_ID>JG_L_2012_01_000000352122</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/18/00/CETATEXT000025180074.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème et 2ème sous-sections réunies, 16/01/2012, 352122</TITRE>
<DATE_DEC>2012-01-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>352122</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème et 2ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Jacques Arrighi de Casanova</PRESIDENT>
<AVOCATS>SCP CELICE, BLANCPAIN, SOLTNER ; SCP ODENT, POULET ; SCP FABIANI, LUC-THALER</AVOCATS>
<RAPPORTEUR>M. Jean-Dominique Nuttens</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Boulouis</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:352122.20120116</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 23 août 2011 au secrétariat du contentieux du Conseil d'Etat, présenté pour la COMMUNE DU CHATEAU D'OLERON, représentée par son maire ; la commune demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler les articles 1er et 3 de l'arrêt n° 10BX02681 du 21 juin 2011 par lequel la cour administrative d'appel de Bordeaux a rejeté sa requête tendant à la réformation de l'ordonnance n° 1001363 du 8 octobre 2010 du juge des référés du tribunal administratif de Poitiers qui a, d'une part, rejeté sa demande tendant à la condamnation solidaire de l'Etat, de la société Semen TP et de la société Géotec à lui payer une provision d'un montant de 4 000 000 d'euros en réparation du préjudice subi à raison de la mauvaise exécution des travaux d'amélioration et de réaménagement du port ostréicole du Paté, d'autre part, désigné un expert chargé d'évaluer ses préjudices ; <br/>
<br/>
              2°) statuant en référé, de faire droit à sa demande de provision ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement de la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Nuttens, chargé des fonctions de Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Fabiani, Luc-Thaler, avocat de la COMMUNE DU CHATEAU D'OLERON, de la SCP Célice, Blancpain, Soltner, avocat de la société Semen TP et de la SCP Odent, Poulet, avocat de la société Géotec, <br/>
<br/>
              - les conclusions de M. Nicolas Boulouis, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Fabiani, Luc-Thaler, avocat de la COMMUNE DU CHATEAU D'OLERON, à la SCP Célice, Blancpain, Soltner, avocat de la société Semen TP et à la SCP Odent, Poulet, avocat de la société Géotec ;<br/>
<br/>
<br/>
<br/>Considérant qu'aux termes de l'article R. 541-1 du code de justice administrative : " Le juge des référés peut, même en l'absence d'une demande au fond, accorder une provision au créancier qui l'a saisi lorsque l'existence de l'obligation n'est pas sérieusement contestable (...) " ;<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis au juge des référés que la COMMUNE DU CHATEAU D'OLERON a entrepris de réaménager le port ostréicole du Paté par agrandissement et approfondissement du bassin et création d'un chenal d'accès vers la baie sud ; que la commune a confié la maîtrise d'ouvrage déléguée du projet à la société d'économie mixte pour le développement de l'Aunis et de la Saintonge ; qu'un marché de maîtrise d'oeuvre a été passé avec l'Etat et un marché conclu avec la société Semen TP pour la réalisation du rideau de palplanches de l'enceinte du bassin ; que le maître d'ouvrage délégué a prononcé le 1er août 2005, avec effet au 22 juillet 2005, la réception des travaux, sous deux réserves relatives à l'exécution du scellement des chevêtres en tête de quai et à la remise en état des terrains et des lieux ; qu'en mars 2006 d'importants désordres sont apparus sur le rideau de palplanches ; qu'après la levée des réserves mentionnées dans la décision de réception du 1er août 2005, le maître d'ouvrage délégué a prononcé la réception sans réserve de l'ouvrage le 7 septembre 2006 ; que la COMMUNE DU CHATEAU D'OLERON se pourvoit en cassation contre l'arrêt du 21 juin 2011 par lequel la cour administrative d'appel de Bordeaux a rejeté sa demande de réformation de l'ordonnance du juge des référés du tribunal administratif de Poitiers du 8 octobre 2010 refusant de faire droit à sa demande tendant à ce que l'Etat, en qualité de maître d'oeuvre, la société Géotec, chargée d'études géologiques préalables aux travaux, et la société Semen TP, chargée de la réalisation du rideau de palplanches, soient condamnées solidairement à lui verser une provision de 4 000 000 d'euros au titre de leur responsabilité décennale ;<br/>
<br/>
              Considérant que la réception est l'acte par lequel le maître de l'ouvrage déclare accepter l'ouvrage avec ou sans réserve et qu'elle met fin aux rapports contractuels entre le maître de l'ouvrage et les constructeurs en ce qui concerne la réalisation de l'ouvrage ; qu'en l'absence de stipulations particulières prévues par les documents contractuels, lorsque la réception est prononcée avec réserves, les rapports contractuels entre le maître de l'ouvrage et les constructeurs ne se poursuivent qu'au titre des travaux ou des parties de l'ouvrage ayant fait l'objet des réserves ;<br/>
<br/>
              Considérant qu'il résulte de ce qui précède qu'en jugeant que la créance de la COMMUNE DU CHATEAU D'OLERON au titre de la responsabilité décennale des constructeurs était sérieusement contestable au motif que les désordres affectant l'ouvrage étaient apparents le 7 septembre 2006, date de sa réception sans réserve, sans rechercher si les réserves mentionnées dans le procès-verbal de réception du 1er août 2005 portaient sur les travaux ou parties d'ouvrages affectés par les désordres, la cour administrative d'appel de Bordeaux a commis une erreur de droit ; que, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, la commune est fondée à demander, pour ce motif, l'annulation de l'article 1er et de l'article 3, en tant qu'il rejette ses conclusions, de l'arrêt du 21 juin 2011 par lequel la cour administrative d'appel a rejeté son appel contre l'ordonnance du 8 octobre 2010 du juge des référés du tribunal administratif de Poitiers ;<br/>
<br/>
              Considérant qu'il y a lieu de statuer sur l'appel de la commune en application de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              Sur la demande de provision :<br/>
<br/>
              En ce qui concerne le principe de la responsabilité des constructeurs :<br/>
<br/>
              Considérant qu'ainsi qu'il a été dit, la réception de l'ouvrage prononcée le 1er août 2005, avec effet au 22 juillet 2005, était assortie de réserves portant respectivement sur la remise en l'état des terrains et des lieux et sur l'exécution du scellement des chevêtres en tête de quai ; que cette réception a donc mis fin aux rapports contractuels entre la commune et les constructeurs, sauf en ce qui concerne les obligations de remise en l'état et les chevêtres en tête de quai ; que les désordres apparus ultérieurement, affectant le rideau de palplanches du bassin, sont dénués de tout lien avec les travaux pour lesquels les relations contractuelles n'avaient pas pris fin ; qu'il n'est pas contesté que ces désordres, affectant la consistance même du bassin et son fonctionnement, étaient de nature à rendre l'ouvrage impropre à sa destination ; que la responsabilité des constructeurs est donc, dans ces conditions, nécessairement susceptible d'être engagée sur le fondement de la garantie décennale ;<br/>
<br/>
              Considérant qu'il résulte de l'instruction, notamment des rapports de l'expert désigné par ordonnances des 12 juillet 2007 et 8 octobre 2010 du juge des référés du tribunal administratif de Poitiers, que les désordres ont pour cause l'insuffisance des études de sol, les erreurs affectant les études d'exécution des palplanches, les erreurs commises dans la mise en place des palplanches, enfin l'insuffisance du contrôle d'exécution du marché ; qu'il apparaît ainsi que le maître d'oeuvre a fait preuve de manquements graves à ses obligations conduisant à lui imputer une responsabilité essentielle dans la survenance des désordres ; que les constructeurs n'apportent pas d'éléments permettant de remettre en cause sérieusement les conclusions de l'expert et, par la même, leur responsabilité, à l'exclusion de celle du maître d'ouvrage délégué et de celle de l'entreprise chargée du terrassement ; que, notamment, si le ministre dont les services ont assuré une mission de maîtrise d'oeuvre invoque la modification apportée par le maître de l'ouvrage en cours d'opération et le retard dans la mise en eau du bassin, ces circonstances sont sans rapport avec les désordres affectant l'ouvrage ;<br/>
<br/>
              Considérant qu'il résulte de ce qui précède que l'obligation de payer dont se prévaut la COMMUNE DU CHATEAU D'OLERON à l'égard de l'Etat, de la société Semen TP et de la société Géotec au titre de la responsabilité décennale des constructeurs n'est, dans son principe, pas sérieusement contestable ; <br/>
<br/>
<br/>
<br/>
              En ce qui concerne le montant de la provision :<br/>
<br/>
              Considérant que le complément d'expertise résultant de l'ordonnance du 8 octobre 2010 évalue à 3 935 410,20 euros le préjudice total subi par la commune, lequel n'est pas dans sa globalité remis en cause ; que le montant des travaux de restauration du bassin accidenté et des honoraires du maître d'oeuvre, du bureau de contrôle et du maître d'ouvrage délégué est évalué à 2 953 394 euros, sur la base du montant des marchés passés par la commune et que cette somme n'est pas elle-même contestée en son montant global ; <br/>
<br/>
              Considérant, toutefois, que si l'expert note que les travaux figurant dans le marché Eiffage sont strictement nécessaires à la réalisation satisfaisante de la restauration, certaines des prestations prévues sont susceptibles d'apporter une amélioration à l'ouvrage ; que, de même, la commune ne fournit aucun élément susceptible d'étayer l'évaluation faite par l'expert du préjudice correspondant aux " dépenses diverses engagées par le maître d'ouvrage ", évalué à 382 016,37 euros ; qu'elle n'apporte pas davantage d'éléments à l'appui de l'évaluation du préjudice correspondant à la perte d'exploitation du bassin, estimé à 600 000 euros en prenant pour hypothèse une occupation complète du port pendant les cinq années correspondant au retard de sa mise en service ; qu'il en résulte, en l'état de l'instruction et du montant des seuls travaux, non sérieusement contesté, engagés pour mettre fin aux désordres, qu'il y a lieu de fixer à 2 500 000 euros le montant de la provision au versement de laquelle les constructeurs doivent être solidairement condamnés ;<br/>
<br/>
              En ce qui concerne les appels en garantie :<br/>
<br/>
              Considérant que le partage des responsabilités résultant des rapports d'expertise n'est pas sérieusement contestable et que la responsabilité de l'Etat, en sa qualité de maître d'oeuvre, est manifestement engagée à titre principal ; que dans ces conditions, et compte tenu de la part prise par les entreprises dans la réalisation des travaux, il y a lieu, au titre du versement de la provision, de condamner respectivement l'Etat à garantir la société Géotec à hauteur de 70 % de la condamnation solidaire, la société Semen TP à garantir l'Etat et la société Géotec à hauteur de 20 % de la condamnation solidaire, enfin, la société Géotec à garantir l'Etat à hauteur de 10 % de la condamnation solidaire ;<br/>
<br/>
              Considérant qu'il résulte de tout ce qui précède que la COMMUNE DU CHATEAU D'OLERON est fondée à demander la réformation de l'ordonnance du juge des référés du tribunal administratif de Poitiers en ce qu'elle a de contraire à la présente décision ;<br/>
<br/>
              Sur les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              Considérant que ces dispositions font obstacle à ce que soit mis à la charge de la COMMUNE DU CHATEAU D'OLERON qui n'est pas, dans la présente instance, la partie perdante, le versement d'une somme au titre des frais exposés par les sociétés Semen TP et Géotec et non compris dans les dépens ; qu'il y a lieu en revanche, sur le fondement des mêmes dispositions, de mettre à la charge respectivement de l'Etat et des sociétés Semen TP et Géotec le versement à la COMMUNE DU CHATEAU D'OLERON de la somme de 1 500 euros chacun ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'article 1er et l'article 3, en tant qu'il rejette les conclusions de la COMMUNE DU CHATEAU D'OLERON, de l'arrêt de la cour administrative d'appel de Bordeaux du 21 juin 2011 sont annulés.<br/>
Article 2 : L'Etat, la société Semen TP et la société Géotec sont condamnés à verser solidairement une provision de 2 500 000 euros à la COMMUNE DU CHATEAU D'OLERON. <br/>
Article 3 : L'Etat et la société Géotec se garantiront mutuellement, à hauteur respectivement de 70 % et 10 % de la condamnation prononcée à leur encontre par la présente décision ; la société Semen TP garantira l'Etat et la société Géotec à hauteur de 20 % de la condamnation prononcée par la présente décision.<br/>
Article 4 : L'ordonnance du juge des référés du tribunal administratif de Poitiers du 8 octobre 2010 est réformée en ce qu'elle a de contraire à la présente décision.<br/>
Article 5 : L'Etat, la société Semen TP et la société Géotec verseront chacun à la COMMUNE DU CHATEAU D'OLERON une somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 6 : Les conclusions présentées par les sociétés Semen TP et Géotec au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 7 : La présente décision sera notifiée à la COMMUNE DU CHATEAU D'OLERON, à la ministre de l'écologie, du développement durable, des transports et du logement, à la société Semen TP et à la société Géotec.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-04 MARCHÉS ET CONTRATS ADMINISTRATIFS. FIN DES CONTRATS. - RÉCEPTION DE L'OUVRAGE - RÉSERVES - DIVISIBILITÉ - CONSÉQUENCE - POURSUITE DES RAPPORTS CONTRACTUELS AU SEUL TITRE DES TRAVAUX OU PARTIES DE L'OUVRAGE AYANT FAIT L'OBJET DES RÉSERVES.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">39-06-01-04 MARCHÉS ET CONTRATS ADMINISTRATIFS. RAPPORTS ENTRE L'ARCHITECTE, L'ENTREPRENEUR ET LE MAÎTRE DE L'OUVRAGE. RESPONSABILITÉ DES CONSTRUCTEURS À L'ÉGARD DU MAÎTRE DE L'OUVRAGE. RESPONSABILITÉ DÉCENNALE. - RÉCEPTION DE L'OUVRAGE - RÉSERVES - DIVISIBILITÉ - CONSÉQUENCE - POURSUITE DES RAPPORTS CONTRACTUELS AU SEUL TITRE DES TRAVAUX OU PARTIES DE L'OUVRAGE AYANT FAIT L'OBJET DES RÉSERVES.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">39-06-01-06 MARCHÉS ET CONTRATS ADMINISTRATIFS. RAPPORTS ENTRE L'ARCHITECTE, L'ENTREPRENEUR ET LE MAÎTRE DE L'OUVRAGE. RESPONSABILITÉ DES CONSTRUCTEURS À L'ÉGARD DU MAÎTRE DE L'OUVRAGE. ACTIONS EN GARANTIE. - LITIGE NOUÉ ENTRE LE MAÎTRE D'OUVRAGE ET LES CONSTRUCTEURS - RÉFÉRÉ-PROVISION INTRODUIT PAR LE MAÎTRE D'OUVRAGE - POSSIBILITÉ POUR LES CONSTRUCTEURS DE PRÉSENTER DES CONCLUSIONS À FIN DE GARANTIE - EXISTENCE (SOL. IMPL.).
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">54-03-015 PROCÉDURE. PROCÉDURES D'URGENCE. RÉFÉRÉ-PROVISION. - POSSIBILITÉ DE PRÉSENTER DES CONCLUSIONS À FIN DE GARANTIE - EXISTENCE (SOL. IMPL.).
</SCT>
<ANA ID="9A"> 39-04 En l'absence de stipulations particulières prévues par les documents contractuels, lorsque la réception de l'ouvrage est prononcée avec réserves, les rapports contractuels entre le maître de l'ouvrage et les constructeurs ne se poursuivent qu'au titre des travaux ou des parties de l'ouvrage ayant fait l'objet des réserves.</ANA>
<ANA ID="9B"> 39-06-01-04 En l'absence de stipulations particulières prévues par les documents contractuels, lorsque la réception de l'ouvrage est prononcée avec réserves, les rapports contractuels entre le maître de l'ouvrage et les constructeurs ne se poursuivent qu'au titre des travaux ou des parties de l'ouvrage ayant fait l'objet des réserves.</ANA>
<ANA ID="9C"> 39-06-01-06 Litige noué entre le maître d'ouvrage et les constructeurs. Des appels en garantie peuvent être présentés par les constructeurs devant le juge du référé-provision saisi par le maître d'ouvrage (sol. impl.). En l'espèce, le juge du référé-provision y fait droit, après avoir constaté que le partage des responsabilités n'est pas sérieusement contestable.</ANA>
<ANA ID="9D"> 54-03-015 Des appels en garantie peuvent être présentés devant le juge du référé-provision (sol. impl.). En l'espèce, le juge du référé-provision y fait droit, après avoir constaté que le partage des responsabilités n'est pas sérieusement contestable.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
