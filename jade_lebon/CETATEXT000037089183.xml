<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037089183</ID>
<ANCIEN_ID>JG_L_2018_06_000000410838</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/08/91/CETATEXT000037089183.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 6ème chambres réunies, 20/06/2018, 410838</TITRE>
<DATE_DEC>2018-06-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>410838</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>M. Olivier Rousselle</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:410838.20180620</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Melun d'annuler pour excès de pouvoir la décision du 5 mars 2015 par laquelle le préfet du Val-de-Marne a refusé de procéder à l'échange de son permis de conduire algérien contre un titre de conduite français et  d'enjoindre au préfet de procéder à cet échange dans un délai de 15 jours sous astreinte ou, à titre subsidiaire, de réexaminer sa demande. Par un jugement n° 1502909 du 24 mars 2017, le tribunal administratif de Melun a rejeté sa demande.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 24 mai et 24 août 2017 et le 5 avril 2018 au secrétariat du contentieux du Conseil d'Etat, M. A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
<br/>
              - le code de la route ;<br/>
<br/>
              - l'arrêté du 12 janvier 2012 fixant les conditions de reconnaissance et d'échange des permis de conduire délivrés par les Etats n'appartenant ni à l'Union européenne, ni à l'Espace économique européen ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Rousselle, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de M.A....<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis au juge du fond que M. A..., de nationalité française et algérienne, a obtenu son permis de conduire algérien le 13 mars 2014 et a sollicité le 5 juin 2014 l'échange de ce permis contre un permis français ; que le préfet du Val-de-Marne lui a opposé un refus le 5 mars 2015 ; que l'intéressé se pourvoit en cassation contre le jugement du 24 mars 2017 par lequel le tribunal administratif de Melun a rejeté le recours pour excès de pouvoir qu'il avait formé contre cette décision ;<br/>
<br/>
              2. Considérant qu'aux termes de  l'article R. 222-3 du code de la route, dans sa rédaction applicable à la date de la décision attaquée : " Tout permis de conduire national, en cours de validité, délivré par un Etat ni membre de la Communauté européenne, ni partie à l'accord sur l'Espace économique européen, peut être reconnu en France jusqu'à l'expiration d'un délai d'un an après l'acquisition de la résidence normale de son titulaire. Pendant ce délai, il peut être échangé contre le permis français, sans que son titulaire soit tenu de subir les examens prévus au premier alinéa de l'article D. 221-3. Les conditions de cette reconnaissance et de cet échange sont définies par arrêté du ministre chargé des transports, après avis du ministre de la justice, du ministre de l'intérieur et du ministre chargé des affaires étrangères. Au terme de ce délai, ce permis n'est plus reconnu et son titulaire perd tout droit de conduire un véhicule pour la conduite duquel le permis de conduire est exigé " ; qu'aux termes de l'article 5 de l'arrêté du 12 janvier 2012 susvisé pris pour l'application de ces dispositions : " I. - Pour être échangé contre un titre français, tout permis de conduire délivré par un Etat n'appartenant ni à l'Union européenne, ni à l'Espace économique européen doit répondre aux conditions suivantes : / A. Avoir été délivré au nom de l'Etat dans le ressort duquel le conducteur avait alors sa résidence normale, sous réserve qu'il existe un accord de réciprocité entre la France et cet Etat conformément à l'article R. 222-1 du code de la route. (...) / II. - En outre, son titulaire doit :/ (...) D. Apporter la preuve de sa résidence normale au sens du quatrième alinéa de l'article R. 222-1 sur le territoire de l'Etat de délivrance, lors de celle-ci, en fournissant tout document approprié présentant des garanties d'authenticité. Les ressortissants étrangers qui détiennent uniquement la nationalité de l'Etat du permis demandé à l'échange ne sont pas soumis à cette condition./ Entre autres documents permettant d'établir la réalité de cette résidence normale, il sera tenu compte, pour les Français, de la présentation d'un certificat d'inscription ou de radiation sur le registre des Français établis hors de France délivré par le consulat français territorialement compétent, ou, pour les ressortissants étrangers ne possédant pas la nationalité de l'Etat de délivrance, d'un certificat équivalent, délivré par les services consulaires compétents, rédigé en langue française ou, si nécessaire, accompagné d'une traduction officielle en français./ Pour les ressortissants français qui possèdent également la nationalité de l'Etat qui a délivré le permis de conduire présenté pour échange, la preuve de cette résidence normale, à défaut de pouvoir être apportée par les documents susmentionnés, sera établie par tout document suffisamment probant et présentant des garanties d'authenticité. (...) " ; qu'aux termes du quatrième alinéa de l'article R. 222-1 du code de la route, dans sa rédaction alors applicable : " (...) On entend par "résidence normale" le lieu où une personne demeure habituellement, c'est-à-dire pendant au moins 185 jours par année civile, en raison d'attaches personnelles ou d'attaches professionnelles " ;<br/>
<br/>
              3. Considérant qu'il résulte de ces dispositions qu'un Français possédant également la nationalité de l'Etat qui lui a délivré un permis de conduire dont il demande l'échange doit établir que ce titre lui a été délivré au cours d'une période où il avait sa résidence normale dans cet Etat ; que cette condition ne peut normalement être regardée comme remplie que si le permis a été obtenu au cours d'une année pendant laquelle l'intéressé a résidé, en raison d'attaches personnelles ou professionnelles, pendant au moins 185 jours dans le pays de délivrance ; que, toutefois, elle doit également être regardée comme remplie si le permis a été obtenu au cours d'une période de résidence dans ce pays précédant ou suivant immédiatement une année pendant laquelle il a résidé pendant au moins 185 jours ; que la preuve de la résidence normale peut être apportée par tout document probant et présentant des garanties d'authenticité ; <br/>
<br/>
              4. Considérant qu'il ressort des pièces du dossier soumis au juge du fond que M. A..., qui avait obtenu son permis de conduire algérien le 13 mars 2014, indiquait avoir séjourné en Algérie du 17 novembre 2013 au 19 mai 2014 ; qu'à supposer exacte cette allégation, la condition de résidence normale ne pouvait être regardée comme remplie faute d'une résidence d'au moins 185 jours pendant la même année civile ; qu'en jugeant que le préfet avait légalement refusé pour ce motif l'échange sollicité, le tribunal administratif, dont le jugement est suffisamment motivé, n'a pas commis d'erreur de droit et s'est livré à une appréciation souveraine des pièces du dossier qui n'est pas entachée de dénaturation ; <br/>
<br/>
              5. Considérant qu'en mentionnant que le préfet avait relevé dans sa décision l'absence de présentation d'un certificat  d'inscription ou de radiation sur le registre des Français établis hors de France délivré par le consulat français territorialement compétent, le tribunal n'a pas subordonné à la production d'un tel certificat  la preuve de la résidence normale dans le pays de délivrance du permis mais s'est borné à faire état de l'une des énonciations de la décision attaquée ; que le moyen tiré de ce qu'il aurait commis une erreur de droit doit, par suite, être écarté ;<br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que M. A...n'est pas fondé à demander l'annulation du jugement du 24 mars 2017 par lequel le tribunal administratif de Melun a rejeté sa demande ; <br/>
<br/>
              7. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de l'Etat, qui n'est pas dans la présente instance la partie perdante, la somme que demande M. A...au titre des frais exposés par lui et non compris dans les dépens ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
		Article 1er : Le pourvoi de M. A...est rejeté.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. B...A...et au ministre d'Etat, ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">49-04-01-04 POLICE. POLICE GÉNÉRALE. CIRCULATION ET STATIONNEMENT. PERMIS DE CONDUIRE. - ECHANGE D'UN PERMIS DE CONDUIRE DÉLIVRÉ PAR UN ETAT N'APPARTENANT NI À L'UE NI À L'EEE CONTRE UN PERMIS FRANÇAIS - CONDITION - PERMIS DÉLIVRÉ AU COURS D'UNE PÉRIODE OÙ L'INTÉRESSÉ AVAIT SA RÉSIDENCE NORMALE DANS CET ETAT - 1) NOTION DE RÉSIDENCE NORMALE - ANNÉE PENDANT LAQUELLE L'INTÉRESSÉ A RÉSIDÉ PENDANT AU MOINS 185 JOURS DANS LE PAYS DE DÉLIVRANCE - 2) PERMIS OBTENU AU COURS D'UNE PÉRIODE DE RÉSIDENCE DANS CE PAYS PRÉCÉDANT OU SUIVANT IMMÉDIATEMENT UNE ANNÉE PENDANT LAQUELLE IL A RÉSIDÉ DANS CE PAYS PENDANT AU MOINS 185 JOURS - CONDITION REMPLIE - 3) PREUVE DE LA RÉSIDENCE NORMALE.
</SCT>
<ANA ID="9A"> 49-04-01-04 Il résulte des articles R. 222-1 et R. 222-3 du code de la route ainsi que de l'article 5 de l'arrêté du 12 janvier 2012 fixant les conditions de reconnaissance et d'échange des permis de conduire délivrés par les Etats n'appartenant ni à l'Union européenne (UE), ni à l'Espace économique européen (EEE) qu'un Français possédant également la nationalité de l'Etat qui lui a délivré un permis de conduire dont il demande l'échange doit établir que ce titre lui a été délivré au cours d'une période où il avait sa résidence normale dans cet Etat.... ,,1) Cette condition ne peut normalement être regardée comme remplie que si le permis a été obtenu au cours d'une année pendant laquelle l'intéressé a résidé, en raison d'attaches personnelles ou professionnelles, pendant au moins 185 jours dans le pays de délivrance.... ,,2) Toutefois, elle doit également être regardée comme remplie si le permis a été obtenu au cours d'une période de résidence dans ce pays précédant ou suivant immédiatement une année pendant laquelle il a résidé pendant au moins 185 jours.... ,,3) La preuve de la résidence normale peut être apportée par tout document probant et présentant des garanties d'authenticité.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
