<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030956572</ID>
<ANCIEN_ID>JG_L_2015_07_000000370414</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/95/65/CETATEXT000030956572.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème / 4ème SSR, 27/07/2015, 370414</TITRE>
<DATE_DEC>2015-07-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>370414</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème / 4ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE ; SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>M. Jean-Dominique Langlais</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:370414.20150727</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a saisi le tribunal administratif de Rouen de demandes tendant, d'une part, à la condamnation de l'établissement d'hébergement pour personnes âgées dépendantes (EHPAD) de Beuzeville à l'indemniser des préjudices ayant résulté du maintien de la mesure de suspension de ses fonctions entre le 19 décembre 2008 et le 11 mars 2010 et au versement de son traitement pendant cette période, d'autre part, à l'annulation de la décision du 11 mars 2010 du directeur de l'établissement prononçant sa révocation, à l'indemnisation des préjudices ayant résulté de cette décision et à sa réintégration. Par un jugement n° 1001289, 1001292 du 6 juillet 2011, le tribunal administratif a condamné l'EHPAD de Beuzeville à lui verser la somme de 2 500 euros du fait de la prolongation anormale de la mesure de suspension le concernant et a rejeté le surplus de ses conclusions.<br/>
<br/>
              Par un arrêt n° 11DA01507 du 14 mai 2013, la cour administrative d'appel de Douai a, à la demande de M.B..., partiellement annulé ce jugement, annulé la décision du 11 mars 2010 prononçant sa révocation, enjoint à l'EHPAD de Beuzeville de le réintégrer dans son emploi ou dans un emploi équivalent, avec reconstitution de sa carrière et de ses droits sociaux à compter du 11 mars 2010, et porté à 25 000 euros l'indemnité mise à la charge de l'établissement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 22 juillet et 22 octobre 2013 au secrétariat du contentieux du Conseil d'Etat, l'EHPAD de Beuzeville demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de M. B...;<br/>
<br/>
              3°) de mettre à la charge de M. B...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
              Vu :<br/>
<br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
<br/>
              - la loi n° 86-33 du 9 janvier 1986 ;<br/>
<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Langlais, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat de l'établissement d'hébergement de personnes âgées dépendantes de Beuzeville  et à la SCP Waquet, Farge, Hazan, avocat de M. B...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond qu'une procédure disciplinaire a été engagée en 2001 par l'établissement d'hébergement de personnes âgées dépendantes (EHPAD) de Beuzeville à l'encontre de M.B..., qui exerçait depuis 1980 les fonctions de directeur de cet établissement ; que l'intéressé a été suspendu de ses fonctions par une décision du 24 février 2001 en raison de l'engagement d'une procédure pénale à son encontre ; qu'une ordonnance de non-lieu du 19 décembre 2008 a mis un terme à cette procédure pénale ; que, par une décision du 11 mars 2010, le nouveau directeur de l'EHPAD a prononcé contre M. B...la sanction de la révocation ; que, par un arrêt du 14 mai 2013, la cour administrative d'appel de Douai a annulé cette décision, enjoint à l'EHPAD de Beuzeville de réintégrer M. B... dans son emploi ou un emploi équivalent et condamné l'établissement à lui verser une indemnité de 25 000 euros ; que l'EHPAD se pourvoit en cassation contre cet arrêt ; que M. B...demande, par la voie d'un pourvoi incident, l'annulation de l'arrêt en tant qu'il exclut de l'indemnisation accordée la réparation du préjudice moral né de la prolongation de sa suspension au-delà du 19 décembre 2008 ;<br/>
<br/>
              Sur les conclusions de l'EHPAD dirigées contre l'arrêt en tant qu'il annule la décision de révocation du 11 mars 2010 et le condamne à verser 10 000 euros à M. B... en réparation du préjudice moral ayant résulté de cette révocation :<br/>
<br/>
              2. Considérant qu'aux termes de l'article 81 de la loi du 9 janvier 1986 portant dispositions statutaires relatives à la fonction publique hospitalière : " Les sanctions disciplinaires sont réparties en quatre groupes : (...) / Troisième groupe : / La rétrogradation, l'exclusion temporaire de fonctions pour une durée de trois mois à deux ans ; / Quatrième groupe : / La mise à la retraite d'office, la révocation " ;<br/>
<br/>
              3. Considérant qu'il appartient au juge de l'excès de pouvoir, saisi de moyens en ce sens, de rechercher si les faits reprochés à un agent public ayant fait l'objet d'une sanction disciplinaire constituent des fautes de nature à justifier une sanction et si la sanction retenue est proportionnée à la gravité de ces fautes ; <br/>
<br/>
              4. Considérant que la constatation et la caractérisation des faits reprochés à l'agent relèvent, dès lors qu'elles sont exemptes de dénaturation, du pouvoir souverain des juges du fond ; que le caractère fautif de ces faits est susceptible de faire l'objet d'un contrôle de qualification juridique de la part du juge de cassation ; que l'appréciation du caractère proportionné de la sanction au regard de la gravité des fautes commises relève, pour sa part, de l'appréciation des juges du fond et n'est susceptible d'être remise en cause par le juge de cassation que dans le cas où la solution qu'ils ont retenue quant au choix, par l'administration, de la sanction est hors de proportion avec les fautes commises ; <br/>
<br/>
              5. Considérant, en premier lieu, que la cour administrative d'appel de Douai a regardé comme établi que M. B...s'était, dans l'exercice de ses fonctions de directeur de l'établissement, abstenu de mettre fin à divers manquements professionnels dans la tenue des dossiers administratifs et dans les soins prodigués aux pensionnaires et avait demandé au personnel placé sous son autorité de signer une pétition en sa faveur après l'engagement des poursuites à son encontre ; que la cour a estimé que ces agissements étaient de nature à justifier une sanction disciplinaire ; qu'en ne retenant pas d'autres agissements invoqués à l'encontre de l'intéressé, elle s'est livrée à une appréciation souveraine des faits qui lui étaient soumis, sans les dénaturer, et n'a pas insuffisamment motivé son arrêt sur ce point ; <br/>
<br/>
              6. Considérant, en deuxième lieu, qu'en retenant que la sanction de la révocation, qui relève du quatrième groupe en vertu des dispositions de l'article 81 de la loi du 9 janvier 1986, était disproportionnée par rapport aux fautes commises, la cour, qui a suffisamment motivé l'appréciation ainsi portée, a retenu une solution qui ne fait pas obstacle à ce que soit infligée à M.B..., en cas de reprise de la procédure disciplinaire, une des sanctions moins sévères prévues à cet article ; qu'il ne ressort pas des pièces du dossier soumis aux juges du fond que les sanctions susceptibles d'être infligées par l'administration sans méconnaître l'autorité de la chose jugée seraient toutes, en raison de leur caractère insuffisant, hors de proportion avec les fautes commises ; <br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que l'EHPAD de Beuzeville n'est pas fondé à demander l'annulation de l'arrêt en tant qu'il annule la mesure de révocation ni, par voie de conséquence, en tant qu'il le condamne à réparer les préjudices ayant résulté de cette sanction ;<br/>
<br/>
              Sur les conclusions du pourvoi principal et du pourvoi incident dirigées contre l'arrêt en ce qu'il majore l'indemnité due par l'EHPAD au titre du préjudice subi du fait du maintien de la mesure de suspension :<br/>
<br/>
              8. Considérant qu'aux termes des dispositions de l'article 30 de la loi du 13 juillet 1983 portant droits et obligations des fonctionnaires : " En cas de faute grave commise par un fonctionnaire, qu'il s'agisse d'un manquement à ses obligations professionnelles ou d'une infraction de droit commun, l'auteur de cette faute peut être suspendu par l'autorité ayant pouvoir disciplinaire qui saisit, sans délai, le conseil de discipline. Le fonctionnaire suspendu conserve son traitement, l'indemnité de résidence, le supplément familial de traitement et les prestations familiales obligatoires. Sa situation doit être définitivement réglée dans le délai de quatre mois. Si, à l'expiration de ce délai, aucune décision n'a été prise par l'autorité ayant pouvoir disciplinaire, l'intéressé, sauf s'il est l'objet de poursuites pénales, est rétabli dans ses fonctions. Le fonctionnaire qui, en raison de poursuites pénales, n'est pas rétabli dans ses fonctions peut subir une retenue qui ne peut être supérieure à la moitié de la rémunération mentionnée à l'alinéa précédent. Il continue, néanmoins, à percevoir la totalité des suppléments pour charges de famille " ;<br/>
<br/>
              9. Considérant, en premier lieu, que les demandes présentées par M. B... devant le tribunal administratif de Rouen comportaient des conclusions tendant à la condamnation de l'EHPAD de Beuzeville à réparer son préjudice moral ainsi que le préjudice financier ayant résulté de la prolongation de sa suspension et de la privation de son traitement du 19 décembre 2008, date du non-lieu prononcé par le juge pénal, au 11 mars 2010, date de sa révocation ; qu'en estimant que les premiers juges, qui lui avaient accordé une indemnité de 2 500 euros au titre du seul préjudice moral subi du fait de cette prolongation, avaient omis de statuer sur les conclusions de M. B...tendant à la réparation de son préjudice financier, la cour ne s'est pas méprise sur la portée du jugement qui lui était déféré ;<br/>
<br/>
              10. Considérant, en deuxième lieu, qu'en estimant que le maintien de la suspension de M. B...pendant plus de quatorze mois après l'ordonnance de non-lieu du 19 décembre 2008 constituait une faute de nature à engager la responsabilité de l'EHPAD de Beuzeville à son égard, la cour a exactement qualifié les faits qui lui étaient soumis ;<br/>
<br/>
              11. Considérant qu'il résulte de ce qui précède que l'EHPAD de Beuzeville n'est pas fondé à demander l'annulation de l'arrêt attaqué en tant qu'il le condamne à indemniser à hauteur de 15 000 euros le préjudice subi par M. B...au titre de la prolongation illégale de la mesure de suspension ; <br/>
<br/>
              12. Mais considérant que le jugement du tribunal administratif condamnait l'établissement à verser à M. B...une indemnité de 2 500 euros, tous intérêts compris, au titre du préjudice moral né de la prolongation de la mesure de suspension ; que, statuant sur le seul appel de l'intéressé, la cour n'était pas saisie de conclusions remettant en cause cette indemnisation ; qu'après avoir annulé le jugement en tant qu'il omettait de statuer sur le préjudice financier résultant de la prolongation de la suspension, la cour a évalué ce préjudice à 15 000 euros ; qu'elle a par ailleurs mis à la charge de l'établissement le versement à l'intéressé d'une indemnité de 10 000 euros en réparation du préjudice ayant résulté de sa révocation ; qu'eu égard à son objet, la somme totale de 25 000 euros ainsi allouée à l'intéressé devait s'ajouter à la somme de 2 500 euros qu'il avait obtenue en première instance à un autre titre ; qu'en énonçant à l'article 4 de son arrêt que " l'indemnité que l'EHPAD de Beuzeville a été condamné à verser à M. B... est portée de la somme de 2 500 euros à la somme de 25 000 euros ", la cour a entaché son arrêt d'une contradiction entre les motifs et le dispositif ; que M. B...est, par suite, fondé à en demander l'annulation en tant qu'il limite à cette dernière somme l'indemnité qui lui est due ;<br/>
<br/>
              13. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler, dans cette mesure, l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              14. Considérant que l'indemnité de 10 000 euros que la cour a, dans les motifs de son arrêt, condamné l'EHPAD de Beuzeville à verser à M. B...au titre du préjudice moral né de sa révocation et l'indemnité de 15 000 euros qu'elle a condamné cet établissement à lui verser au titre du préjudice financier né de la prolongation de sa suspension entre le 19 décembre 2008 et le 11 mars 2010 s'ajoutent à l'indemnité, devenue définitive, de 2 500 euros tous intérêts compris que le tribunal a condamné le même EHPAD à verser à M. B... ; que le montant total de l'indemnité due à ce dernier doit, dès lors, être porté à 27 500 euros ;<br/>
<br/>
              15. Considérant que M. B...a droit aux intérêts sur la somme de 25 000 euros qu'il a obtenue en appel à compter de la date de réception de sa demande devant le tribunal administratif de Rouen, soit le 28 avril 2010 ;<br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              16. Considérant que les dispositions de l'article L. 761-1 font obstacle à ce qu'une somme soit mise à ce titre à la charge de M.B..., qui n'est pas, dans la présente instance, la partie perdante ; qu'en revanche, il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'EHPAD de Beuzeville le versement d'une somme de 2 500 euros à la SCP Waquet, Farge, Hazan, avocat de M. B..., en application des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat ; <br/>
<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : Le pourvoi de l'EHPAD de Beuzeville est rejeté.<br/>
Article 2 : L'article 4 de l'arrêt de la cour administrative d'appel de Douai du 14 mai 2013 est annulé.<br/>
Article 3 : L'indemnité que l'EHPAD de Beuzeville a été condamné à verser à M. B... est portée de la somme de 25 000 euros à la somme de 27 500 euros. La somme de 25 000 euros allouée à l'intéressé en appel est assortie des intérêts au taux légal à compter du 28 avril 2010.<br/>
Article 4 : L'EHPAD de Beuzeville versera à la SCP Waquet, Farge, Hazan une somme de 2 500 euros en application des dispositions du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
Article 5 : Le présent arrêt sera notifié à M. A...B...et à l'établissement d'hébergement pour personnes âgées dépendantes (EHPAD) de Beuzeville.<br/>
Copie en sera adressée pour information à l'Agence régionale de santé de Haute-Normandie.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-08-02-02-01 PROCÉDURE. VOIES DE RECOURS. CASSATION. CONTRÔLE DU JUGE DE CASSATION. BIEN-FONDÉ. - VÉRIFICATION DE CE QUE LA SOLUTION RETENUE PAR LES JUGES DU FOND QUANT AU CHOIX D'UNE SANCTION N'EST PAS HORS DE PROPORTION AVEC LES FAUTES COMMISES [RJ1] - APPLICATION AU CAS OÙ LE JUGE DU FOND A ANNULÉ POUR EXCÈS DE POUVOIR UNE SANCTION DISCIPLINAIRE INFLIGÉE À UN AGENT PUBLIC - MODALITÉS DU CONTRÔLE.
</SCT>
<ANA ID="9A"> 54-08-02-02-01 Lorsque le juge du fond a annulé pour excès de pouvoir une sanction disciplinaire en raison de sa sévérité excessive au regard des faits retenus à l'encontre de l'intéressé, il appartient au juge de cassation, saisi d'un moyen contestant cette appréciation portée par le juge du fond, de vérifier que les sanctions moins sévères que l'administration pourrait prononcer, en cas de reprise de la procédure disciplinaire, sans méconnaître l'autorité de la chose jugée, ne sont pas toutes, en raison de leur sévérité insuffisante, hors de proportion avec les faits reprochés.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Cf. CE, 27 février 2015, La Poste, n°s 376598 381828, à publier au recueil.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
