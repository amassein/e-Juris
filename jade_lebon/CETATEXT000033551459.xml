<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033551459</ID>
<ANCIEN_ID>JG_L_2016_12_000000394178</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/55/14/CETATEXT000033551459.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 5ème chambres réunies, 05/12/2016, 394178, Publié au recueil Lebon</TITRE>
<DATE_DEC>2016-12-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>394178</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GATINEAU, FATTACCINI</AVOCATS>
<RAPPORTEUR>M. Jean-François de Montgolfier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:394178.20161205</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Sud Travaux a demandé au tribunal administratif de Nîmes d'annuler pour excès de pouvoir la décision du 30 mars 2012 par laquelle l'inspecteur du travail de l'unité territoriale du Gard a exigé qu'elle retire certaines dispositions de son règlement intérieur. Par un jugement n° 1201512 du 27 mars 2014, le tribunal administratif a annulé cette décision.<br/>
<br/>
              Par un arrêt n° 14MA02413 du 21 août 2015, la cour administrative d'appel de Marseille a, sur appel du ministre du travail, de l'emploi de la formation professionnelle et du dialogue social, annulé ce jugement et rejeté la demande de la société Sud Travaux.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 21 octobre 2015 et 21 janvier 2016 au secrétariat du contentieux du Conseil d'Etat, la société SOGEA SUD, venant aux droits de la société Sud Travaux, demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel du ministre ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code du travail ;<br/>
              - le code de la santé publique ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-François de Montgolfier, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gatineau, Fattaccini, avocat de la société SOGEA SUD ;<br/>
<br/>
<br/>
<br/>
<br/>1.  Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la société Sud Travaux, qui exerçait une activité de construction dans le domaine du bâtiment a, en application de l'article L. 1321-4 du code du travail, communiqué à l'inspecteur du travail un projet de règlement intérieur comportant notamment, en son article 3.5 relatif aux " boissons alcoolisées et drogues ", les dispositions suivantes : " a) Mesures générales applicables à l'ensemble du personnel / L'introduction, la distribution et la consommation d'alcool et de drogue sont strictement interdites dans les locaux de l'entreprise et ses dépendances. Il est interdit de pénétrer ou de demeurer dans l'établissement en état d'ivresse ou sous l'emprise de drogue. Tout manquement à l'une de ces obligations est de nature à justifier une sanction pouvant aller jusqu'au licenciement. Dans certaines circonstances particulières (...) l'employeur pourra accorder des dérogations en matière de consommation modérée d'alcool et en fixera les modalités. / b) Mesures particulières applicables aux postes hypersensibles drogue et alcool. / L'employeur a identifié, en collaboration avec le médecin du travail, et avec la collaboration et la consultation des délégués du personnel, les postes dits hypersensibles. Les salariés affectés à ces postes de travail font l'objet des mesures suivantes : (...) Contrôle aléatoire effectué sur les lieux de travail / La mise en oeuvre d'une politique de prévention efficace justifie de veiller à ce que les salariés qui occupent des postes hypersensibles drogue et alcool ne soient pas, pendant l'exécution de leur travail, en état d'ébriété et/ou sous l'emprise de produits stupéfiants. / Aussi la Direction s'autorise à organiser des contrôles aléatoires afin de vérifier si les salariés ne sont pas sous l'emprise d'alcool et/ou de drogue. La Direction pourra ainsi imposer : / (...) - s'agissant de la drogue, le contrôle sera effectué par un test salivaire permettant le dépistage simultané de six substances prohibées. Le test ne permet pas d'identifier précisément la catégorie de drogue qui a été consommée par le salarié mais simplement d'établir qu'il y a bien eu consommation de drogue. (...) / Les tests devront être pratiqués par un supérieur hiérarchique qui aura reçu une information appropriée sur la manière d'administrer les tests concernés et d'en lire les résultats. A ce titre, il devra respecter scrupuleusement la notice d'utilisation rédigée par le fournisseur, s'assurer que le test de dépistage se trouve en parfait état (validité et conservation) et veiller à éviter toute circonstance susceptible d'en fausser le résultat. / Avant d'être soumises au test de dépistage, la ou les personnes concernées devront être préalablement informées que celui-ci ne pourra être effectué : /  qu'avec l'accord de la personne contrôlée ; la personne chargée du contrôle devra préciser toutefois qu'en cas de refus, le salarié s'expose à une sanction disciplinaire pouvant aller jusqu'au licenciement. / (...) Les salariés soumis au contrôle auront la faculté de demander une contre expertise médicale qui devra être effectuée dans les plus brefs délais. (...) / Dans l'hypothèse d'un contrôle positif, le salarié pourra faire l'objet d'une sanction disciplinaire pouvant aller jusqu'au licenciement. / c) Mesures particulières applicables à l'ensemble des salariés / Pour l'ensemble des postes de travail, la Direction pourra être amenée à effectuer des contrôles selon les modalités et garanties définies au paragraphe 3.5 b), lorsqu'il apparaîtra que le comportement des salariés laissera présumer un état d'ébriété ou une consommation de drogue. Dans l'hypothèse d'un résultat positif, le salarié pourra faire l'objet d'une sanction disciplinaire pouvant aller jusqu'au licenciement. " ; que, par une décision du 20 mars 2012, l'inspectrice du travail de l'unité territoriale du Gard a exigé que soient retirées de ce règlement intérieur, d'une part, au point b) de l'article 3.5, la phrase : " Les tests devront être pratiqués par un supérieur hiérarchique qui aura reçu une information appropriée sur la manière d'administrer les tests concernés et d'en lire les résultats " et, d'autre part, aux points b) et c) du même article, en tant qu'elles portent sur le contrôle de la consommation de drogue, les phrases : " Dans l'hypothèse d'un résultat positif, le salarié pourra faire l'objet d'une sanction disciplinaire pouvant aller jusqu'au licenciement " ; que, par un arrêt du 21 août 2015, contre lequel la société SOGEA SUD, venant aux droits de la société Sud Travaux, se pourvoit en cassation, la cour administrative d'appel de Marseille a, sur appel du ministre chargé du travail, annulé le jugement du tribunal administratif de Nîmes annulant la décision de l'inspecteur du travail du 30 mars 2012 et rejeté la demande d'annulation de cette décision présentée par la société Sud Travaux devant le tribunal administratif ;<br/>
<br/>
              2.  Considérant qu'un test salivaire de détection immédiate de produits stupéfiants, tel que celui qui est prévu par le règlement intérieur qui figure dans les pièces du dossier soumis aux juges du fond, a pour seul objet de révéler, par une lecture instantanée, l'existence d'une consommation récente de substance stupéfiante ; qu'il ne revêt pas, par suite, le caractère d'un examen de biologie médicale au sens des dispositions de l'article L. 6211-1 du code de la santé publique et n'est donc pas au nombre des actes qui, en vertu des dispositions de son article L. 6211-7, doivent être réalisés par un biologiste médical ou sous sa responsabilité ; que, n'ayant pas pour objet d'apprécier l'aptitude médicale des salariés à exercer leur emploi, sa mise en oeuvre ne requiert pas l'intervention d'un médecin du travail ; qu'enfin, aucune autre règle ni aucun  principe ne réservent le recueil d'un échantillon de salive à une profession médicale ; qu'ainsi, en jugeant que, dès lors qu'il impliquait un recueil de salive, le test de dépistage prévu par les dispositions litigieuses du règlement intérieur ne pouvait pas être pratiqué par un supérieur hiérarchique, la cour administrative d'appel de Marseille a commis une erreur de droit ; que, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, la société SOGEA SUD est fondée à demander l'annulation de l'arrêt qu'elle attaque ;<br/>
<br/>
              3.  Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              4.  Considérant qu'aux termes de l'article L. 1121-1 du code du travail : " Nul ne peut apporter aux droits des personnes et aux libertés individuelles et collectives de restrictions qui ne seraient pas justifiées par la nature de la tâche à accomplir ni proportionnées au but recherché " ; que son article L. 1321-1 dispose : " Le règlement intérieur est un document écrit par lequel l'employeur fixe exclusivement : / 1° Les mesures d'application de la réglementation en matière de santé et de sécurité dans l'entreprise ou l'établissement, notamment les instructions prévues à l'article L. 4122-1 ; / (...) 3° Les règles générales et permanentes relatives à la discipline, notamment la nature et l'échelle des sanctions que peut prendre l'employeur " ; qu'aux termes de l'article L. 1321-3 de ce code : " Le règlement intérieur ne peut contenir : (...) 2° Des dispositions apportant aux droits des personnes et aux libertés individuelles et collectives des restrictions qui ne seraient pas justifiées par la nature de la tâche à accomplir ni proportionnées au but recherché " ; qu'enfin, l'article L. 4121-1 du même code dispose que : " L'employeur prend les mesures nécessaires pour assurer la sécurité et protéger la santé physique et mentale des travailleurs " ;<br/>
<br/>
              5.  Considérant, en premier lieu, que, ainsi qu'il a été dit au point 2 ci-dessus, aucune règle ni aucun principe n'imposent l'intervention d'un professionnel de santé pour procéder au recueil de salive et lire le résultat du test de dépistage ; que, par ailleurs, si les résultats de ce test ne sont pas couverts par le secret médical, l'employeur et le supérieur hiérarchique désigné pour le mettre en oeuvre sont tenus au secret professionnel sur son résultat ;<br/>
<br/>
              6.  Considérant, en deuxième  lieu, que s'il ressort des pièces du dossier qu'en l'état des techniques disponibles, les tests salivaires de détection de substances stupéfiantes présentent des risques d'erreur, le règlement intérieur litigieux reconnaît aux salariés ayant fait l'objet d'un test  positif le droit d'obtenir une contre expertise médicale, laquelle doit être à la charge de l'employeur ; que, par ailleurs, si le contrôle de la consommation de drogues n'est pas aussi précis que le contrôle d'alcoolémie, dès lors qu'il se borne à établir la consommation récente de produits stupéfiants, sans apporter la preuve que le salarié est encore sous l'emprise de la drogue et n'est pas apte à exercer son emploi, le règlement litigieux réserve les contrôles aléatoires de consommation de substances stupéfiantes aux seuls postes dits " hypersensibles drogue et alcool ", pour lesquels l'emprise de la drogue constitue un danger particulièrement élevé pour le salarié et pour les tiers ; que, compte tenu de ce risque particulier, de l'obligation qui incombe à l'employeur, en vertu des dispositions précitées de l'article L. 4121-1 du code du travail, d'assurer la sécurité et la santé des salariés dans l'entreprise, de l'obligation, rappelée au point 5 ci-dessus, pour l'employeur et le supérieur hiérarchique qui pratique le test de respecter le secret professionnel sur ses résultats, et en l'absence d'une autre méthode qui permettrait d'établir directement l'incidence d'une consommation de drogue sur l'aptitude à effectuer une tâche, les dispositions du règlement intérieur litigieux, qui permettent à l'employeur d'effectuer lui-même le contrôle des salariés affectés à des postes dits " hypersensibles drogue et alcool " et de sanctionner ceux des contrôles qui se révéleraient positifs, ne portent pas aux droits des personnes et aux libertés individuelles et collectives une atteinte disproportionnée par rapport au but recherché ; qu'ainsi, elles ne méconnaissent pas les dispositions citées ci-dessus des articles L. 1121-1 et L. 1321-3 du code du travail ; <br/>
<br/>
              7.  Considérant, en troisième lieu, qu'à l'appui de son appel, le ministre du travail ne conteste pas l'appréciation qui a conduit le tribunal administratif à annuler la décision de l'inspecteur du travail en tant qu'elle exige le retrait des dispositions du c) de l'article 3.5 du règlement intérieur relatives  aux mesures particulières applicables à l'ensemble des salariés ;  <br/>
<br/>
              8.  Considérant qu'il résulte de tout ce qui précède que la ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social n'est pas fondée à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Nîmes a annulé la décision du 30 mars 2012 de l'inspectrice du travail de l'unité territoriale du Gard ;<br/>
<br/>
              9.  Considérant que, dans les circonstances de l'espèce, il y a lieu de mettre à la charge de l'Etat une somme de 3 000 euros à verser à la société SOGEA SUD au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Marseille du 21 août 2015 est annulé.<br/>
Article 2 : L'appel formé par le ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social contre le jugement du tribunal administratif de Nîmes du 27 mars 2014 est rejeté.<br/>
Article 3 : L'Etat versera à la société SOGEA SUD la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la société SOGEA SUD et à la ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">55-03 PROFESSIONS, CHARGES ET OFFICES. CONDITIONS D'EXERCICE DES PROFESSIONS. - PROFESSIONS MÉDICALES - MONOPOLE DE RÉALISATION DES TESTS SALIVAIRES DE DÉTECTION DES PRODUITS STUPÉFIANTS - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">66-03-01 TRAVAIL ET EMPLOI. CONDITIONS DE TRAVAIL. RÈGLEMENT INTÉRIEUR. - RÈGLEMENT INTÉRIEUR PRÉVOYANT DES TESTS SALIVAIRES DE DÉTECTION DE DROGUES - 1) OBLIGATION DE RECOURIR À UN PROFESSIONNEL DE SANTÉ - ABSENCE - 2) PROPORTIONNALITÉ DE L'ATTEINTE AUX DROITS DES SALARIÉS - EXISTENCE EN L'ESPÈCE.
</SCT>
<ANA ID="9A"> 55-03 Un test salivaire de détection immédiate de produits stupéfiants a pour seul objet de révéler, par une lecture instantanée, l'existence d'une consommation récente de substance stupéfiante. Il ne revêt pas, par suite, le caractère d'un examen de biologie médicale au sens des dispositions de l'article L. 6211-1 du code de la santé publique et n'est donc pas au nombre des actes qui, en vertu des dispositions de son article L. 6211-7, doivent être réalisés par un biologiste médical ou sous sa responsabilité. N'ayant pas pour objet d'apprécier l'aptitude médicale des salariés à exercer leur emploi, sa mise en oeuvre ne requiert pas l'intervention d'un médecin du travail. Enfin, aucune autre règle ni aucun  principe ne réservent le recueil d'un échantillon de salive à une profession médicale.</ANA>
<ANA ID="9B"> 66-03-01 Règlement intérieur prévoyant, dans certaines conditions, des tests salivaires de détection de produits stupéfiants réalisés par le supérieur hiérarchique.... ,,1) Un test salivaire de détection immédiate de produits stupéfiants a pour seul objet de révéler, par une lecture instantanée, l'existence d'une consommation récente de substance stupéfiante. Il ne revêt pas, par suite, le caractère d'un examen de biologie médicale au sens des dispositions de l'article L. 6211-1 du code de la santé publique et n'est donc pas au nombre des actes qui, en vertu des dispositions de son article L. 6211-7, doivent être réalisés par un biologiste médical ou sous sa responsabilité. N'ayant pas pour objet d'apprécier l'aptitude médicale des salariés à exercer leur emploi, sa mise en oeuvre ne requiert pas l'intervention d'un médecin du travail. Enfin, aucune autre règle ni aucun  principe ne réservent le recueil d'un échantillon de salive à une profession médicale.... ,,2) Si, en l'état des techniques disponibles, les tests salivaires de détection de substances stupéfiantes présentent des risques d'erreur, le règlement intérieur litigieux reconnaît aux salariés ayant fait l'objet d'un test positif le droit d'obtenir une contre-expertise médicale, laquelle doit être à la charge de l'employeur. Par ailleurs, si le contrôle de la consommation de drogues se borne à établir la consommation récente de produits stupéfiants, sans apporter la preuve que le salarié est encore sous l'emprise de la drogue et n'est pas apte à exercer son emploi, le règlement litigieux réserve les contrôles aléatoires de consommation de substances stupéfiantes aux seuls postes dits hypersensibles drogue et alcool, pour lesquels l'emprise de la drogue constitue un danger particulièrement élevé pour le salarié ou pour les tiers. Compte tenu de ce risque particulier, de l'obligation qui incombe à l'employeur, en vertu de l'article L. 4121-1 du code du travail, d'assurer la sécurité et la santé des salariés dans l'entreprise, de l'obligation pour l'employeur et le supérieur hiérarchique qui pratique le test de respecter le secret professionnel sur ses résultats, et en l'absence d'une autre méthode qui permettrait d'établir directement l'incidence d'une consommation de drogue sur l'aptitude à effectuer une tâche, les dispositions du règlement intérieur litigieux, qui permettent à l'employeur d'effectuer lui-même le contrôle des salariés affectés à des postes dits hypersensibles drogue et alcool et de sanctionner ceux des contrôles qui se révéleraient positifs, ne portent pas aux droits des personnes et aux libertés individuelles et collectives une atteinte disproportionnée par rapport au but recherché et ne méconnaissent ainsi pas les articles L. 1121-1 et L. 1321-3 du code du travail.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
