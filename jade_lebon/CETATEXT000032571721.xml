<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032571721</ID>
<ANCIEN_ID>JG_L_2016_05_000000386122</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/57/17/CETATEXT000032571721.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 6ème chambres réunies, 20/05/2016, 386122</TITRE>
<DATE_DEC>2016-05-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>386122</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Frédéric Puigserver</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Jean Lessi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:386122.20160520</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 1er décembre 2014, 2 mars 2015 et 11 décembre 2015 au secrétariat du contentieux du Conseil d'Etat, la société Laboratoires Alcon demande au Conseil d'Etat d'annuler pour excès de pouvoir la décision du 2 juin 2014 par laquelle le ministre des finances et des comptes publics et le ministre des affaires sociales et de la santé ont refusé d'inscrire la spécialité Jetrea sur la liste des spécialités pharmaceutiques mentionnée à l'article L. 162-22-7 du code de la sécurité sociale, ensemble la décision implicite de rejet de son recours gracieux et la décision expresse du 26 novembre 2014 confirmant le refus d'inscription.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la directive 89/105/CEE du Conseil du 21 décembre 1988 ;<br/>
              - le code de la santé publique ;<br/>
              - le code de la sécurité sociale ;<br/>
              - le décret n° 2005-850 du 27 juillet 2005 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Puigserver, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Jean Lessi, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 162-22-7 du code de la sécurité sociale, dans sa rédaction applicable au litige : " I.- L'Etat fixe la liste des spécialités pharmaceutiques bénéficiant d'une autorisation de mise sur le marché dispensées aux patients hospitalisés dans les établissements de santé mentionnés à l'article L. 162-22-6 qui peuvent être prises en charge, sur présentation des factures, par les régimes obligatoires d'assurance maladie en sus des prestations d'hospitalisation mentionnées au 1° du même article (...). Cette liste précise les seules indications thérapeutiques ouvrant droit à la prise en charge des médicaments en sus des prestations d'hospitalisation mentionnées à l'article L. 162-22-6 (...) ", lesquelles sont prises en charge dans le cadre de forfaits ; qu'aux termes de l'article R. 162-42-7 du même code, alors en vigueur : " La liste des spécialités pharmaceutiques et les conditions de prise en charge des produits et prestations mentionnés à l'article L. 162-22-7 sont fixées par arrêté des ministres chargés de la santé et de la sécurité sociale sur recommandation du conseil de l'hospitalisation " ;<br/>
<br/>
              2. Considérant que, par une décision du 2 juin 2014, prise sur une recommandation du conseil de l'hospitalisation du 21 mars 2014, le ministre des finances et des comptes publics et le ministre des affaires sociales et de la santé ont refusé d'inscrire sur la liste des spécialités pharmaceutiques prévue à l'article L. 162-22-7 du code de la sécurité sociale la spécialité Jetrea, commercialisée par la société Laboratoires Alcon et indiquée, selon l'autorisation de mise sur le marché obtenue le 13 mars 2013, chez les adultes atteints de traction vitréo-maculaire isolée ou associée à un trou maculaire d'un diamètre inférieur à 400 microns ; qu'après avoir rejeté implicitement le recours gracieux dont la société les avait saisis par un courrier du 30 juillet 2014, les ministres compétents, au vu d'une nouvelle recommandation du conseil de l'hospitalisation du 10 octobre 2014, ont réitéré, par une décision du 26 novembre 2014, le refus d'inscription de la spécialité sur la liste en cause ; que les conclusions de la requête dirigées contre la décision implicite de rejet du recours gracieux ainsi formé doivent être regardées comme dirigées contre la décision du 26 novembre 2014, qui s'y est substituée, par laquelle les ministres se sont prononcés expressément sur les prétentions de la société ;<br/>
<br/>
              Sur la légalité de la décision du 2 juin 2014 :<br/>
<br/>
              3. Considérant, en premier lieu, qu'il résulte des dispositions du décret du 27 juillet 2005 relatif aux délégations de signature des membres du Gouvernement que le sous-directeur du financement du système de soins à la direction de la sécurité sociale avait, en vertu de sa nomination par un arrêté du 30 août 2013 publié au Journal officiel du 1er septembre suivant, et du fait des attributions de la sous-direction placée sous son autorité, définies par un arrêté du 3 janvier 2006, qualité pour signer la décision attaquée au nom du ministre des finances et des comptes publics et du ministre des affaires sociales et de la santé, dont les attributions avaient été définies par des décrets du 16 avril 2014 ; que, conjointement avec ce même sous-directeur, le sous-directeur du pilotage de la performance des acteurs de l'offre de soins à la direction générale de l'offre de soins, avait, en vertu de sa nomination par un arrêté du 28 juin 2013 publié au Journal officiel du 30 juin suivant, et du fait des attributions de la sous-direction placée sous son autorité, définies par un arrêté du 7 mai 2014, qualité pour signer la décision attaquée au nom du ministre des affaires sociales et de la santé ; qu'ainsi, le moyen tiré de l'incompétence de ses signataires doit être écarté ;<br/>
<br/>
              4. Considérant, en deuxième lieu, d'une part, qu'aux termes de l'article L. 1451-1 du code de la santé publique, dans sa rédaction applicable au litige : " I.- Les membres des commissions et conseils siégeant auprès des ministres chargés de la santé et de la sécurité sociale (...) sont tenus, lors de leur prise de fonctions, d'établir une déclaration d'intérêts. / Cette déclaration (...) mentionne les liens d'intérêts de toute nature, directs ou par personne interposée, que le déclarant a, ou qu'il a eus pendant les cinq années précédant sa prise de fonctions, avec des entreprises, des établissements ou des organismes dont les activités, les techniques et les produits entrent dans le champ de compétence (...) de l'organe consultatif dont il est membre ainsi qu'avec les sociétés ou organismes de conseil intervenant dans les mêmes secteurs. / Elle est rendue publique. Elle est actualisée à l'initiative de l'intéressé (...) " ; qu'aux termes de l'article R. 1451-1 du même code : " I.- En application du I de l'article L. 1451-1, les personnes suivantes remettent la déclaration d'intérêts prévue par les dispositions de cet article au ministre (...) auprès duquel ils exercent leurs fonctions ou remplissent une mission : / 1° Les membres des commissions et conseils siégeant auprès des ministres chargés de la santé et de la sécurité sociale et auxquels la loi ou le règlement confie la mission (...) d'émettre des recommandations (...) sur des questions de santé publique (...) " ; que ces dispositions sont applicables au conseil de l'hospitalisation, placé, en vertu de l'article L. 162-21-2 du code de la sécurité sociale, auprès des ministres chargés de la santé et de la sécurité sociale et chargé, en vertu du 6° de l'article R. 162-22 du même code, d'émettre une recommandation portant sur l'inscription d'une spécialité sur la liste mentionné à l'article L. 162-22-7 de ce code ;<br/>
<br/>
              5. Considérant que si le retard dans la souscription ou l'absence de publication de certaines déclarations d'intérêts ne révèlent pas, par eux-mêmes, une méconnaissance du principe d'impartialité, il appartient, en revanche, aux ministres chargés de la santé et de la sécurité sociale, auprès de qui est placé le conseil de l'hospitalisation, pour celles des personnes dont la déclaration obligatoire d'intérêts échapperait ainsi au débat contradictoire, de verser au dossier l'ensemble des éléments permettant au juge de s'assurer, après transmission aux parties, de l'absence ou de l'existence de liens d'intérêts et d'apprécier, le cas échéant, si ces liens sont de nature à révéler des conflits d'intérêts ; que la société requérante ayant fait valoir que toutes les déclarations requises n'avaient pas été rendues publiques, le ministre des affaires sociales, de la santé et des droits des femmes a produit au cours de l'instruction l'ensemble des déclarations d'intérêts souscrites par les membres du conseil de l'hospitalisation ayant siégé lors de sa réunion du 21 mars 2014 ; que si ces déclarations, dont l'objet est de retracer les éventuels liens d'intérêts des personnes concernées durant l'exercice de leurs fonctions et les cinq années précédentes, portent, pour la plupart, des dates postérieures de quelques mois à la date de la réunion du conseil de l'hospitalisation, cette circonstance n'est, par elle-même, pas susceptible de révéler l'existence de conflits d'intérêts ; que la société ne produit par ailleurs, à l'appui de ses allégations et au vu des déclarations produites, aucun élément susceptible d'établir l'existence de liens d'intérêts mettant en cause l'impartialité des membres du conseil à la date à laquelle ils ont siégé ; que, dès lors, le moyen tiré de ce que l'absence de dépôt de certaines déclarations entacherait la procédure d'irrégularité doit être écarté ;<br/>
<br/>
              6. Considérant, d'autre part, qu'en vertu de l'article 1er de l'arrêté du 7 mai 2007 pris en application de l'article L. 162-21-2 du code de la sécurité sociale et fixant la composition et les modalités de fonctionnement du conseil de l'hospitalisation, dans sa rédaction applicable au litige, ce conseil compte neuf membres ; qu'aux termes de l'article 2 de cet arrêté : " (...) Le conseil ne peut délibérer valablement que si la moitié au moins de ses membres sont présents. Lorsque le quorum n'est pas atteint, le conseil se réunit à nouveau dans les huit jours et la délibération n'est pas soumise à l'obligation de quorum (...) " ;<br/>
<br/>
              7. Considérant qu'il ressort des pièces du dossier, notamment du procès-verbal établi à la suite de la réunion du conseil de l'hospitalisation du 21 mars 2014, que huit de ses membres étaient présents ; que la condition de quorum prévue par les dispositions précitées de l'article 2 de l'arrêté du 7 mai 2007 étant ainsi satisfaite, le conseil a pu valablement délibérer ; que le moyen tiré de la méconnaissance des règles de quorum doit, par suite, être écarté ;<br/>
<br/>
              8. Considérant, en troisième lieu, qu'il ressort des pièces du dossier que la décision attaquée, qui reprend la recommandation du conseil de l'hospitalisation du 21 mars 2014 et se réfère à l'avis rendu le 4 décembre 2013 par la commission de la transparence de la Haute Autorité de santé, refuse l'inscription de la spécialité Jetrea sur la liste mentionnée à l'article L. 162-22-7 du code de la sécurité sociale au motif que, d'une part, le service médical rendu par cette spécialité est insuffisant pour une partie de la population cible de l'autorisation de mise sur le marché, soit les personnes nécessitant d'emblée une intervention chirurgicale dénommée vitrectomie, et, d'autre part, l'amélioration du service médical rendu est mineure pour l'autre partie de cette population, constituée des patients ne nécessitant pas d'emblée une vitrectomie, le taux de recours à cette intervention à l'échéance de six mois n'étant pas réduit par l'administration de la spécialité en cause ; qu'en retenant ce motif, distinguant différentes composantes de la population cible de l'autorisation de mise sur le marché, selon que les personnes nécessitent ou non d'emblée une vitrectomie, mais portant sur l'ensemble de cette population, les ministres n'ont pas commis d'erreur de droit et n'ont pas dénaturé les termes de l'avis de la commission de la transparence ; <br/>
<br/>
              9. Considérant, en quatrième lieu, qu'il ressort de l'avis de la commission de la transparence de la Haute Autorité de santé du 4 décembre 2013, sur lequel se fonde, ainsi qu'il a été dit, la décision attaquée, que pour les personnes ne nécessitant pas d'emblée une vitrectomie et pour lesquelles la spécialité est utilisée comme traitement de première intention, si le service médical rendu de la spécialité Jetrea est important, compte tenu notamment de la nature de la pathologie, du caractère curatif du traitement et de l'absence d'alternative thérapeutique médicamenteuse, en revanche, l'amélioration du service médical rendu est mineure, dès lors que son efficacité est modeste et qu'il n'est pas démontré que son administration réduise le recours à la vitrectomie ; que, pour refuser l'inscription de la spécialité Jetrea sur la liste mentionnée à l'article L. 162-22-7 du code de la sécurité sociale, les ministres pouvaient légalement se fonder sur le niveau mineur de l'amélioration du service médical rendu de la spécialité ainsi que sur l'existence d'un traitement de même visée thérapeutique prescrit en deuxième intention dans le traitement de la pathologie en cause, ce traitement, à savoir la vitrectomie, fût-il chirurgical et non médicamenteux ; que, ce faisant, ils n'ont pas commis d'erreur de droit ;<br/>
<br/>
              10. Considérant, en dernier lieu, qu'il ne ressort pas des pièces du dossier qu'en refusant d'inscrire la spécialité Jetrea sur la liste mentionnée à l'article L. 162-22-7 du code de la sécurité sociale, pour les motifs mentionnés aux points 8 et 9, les ministres auraient privé certains patients de la possibilité de bénéficier d'un meilleur traitement, moins coûteux pour l'assurance maladie, et auraient entaché leur décision d'une erreur manifeste d'appréciation ; que, pour les mêmes motifs, le moyen tiré de ce que cette décision serait contraire au principe d'égal accès aux soins doit être écarté ;<br/>
<br/>
              11. Considérant qu'il résulte de ce qui précède que la société Laboratoires Alcon n'est pas fondée à demander l'annulation de la décision attaquée du 2 juin 2014 ;<br/>
<br/>
              Sur la légalité de la décision du 26 novembre 2014 :<br/>
<br/>
              12. Considérant qu'en prenant la décision attaquée du 26 novembre 2014, par laquelle ils ont rejeté, après une nouvelle instruction de la demande, le recours gracieux formé par la société requérante contre la décision du 2 juin 2014, les ministres compétents ne peuvent être regardés comme ayant entendu retirer ou modifier leur décision initiale et n'ont pas eu à se prononcer au vu de circonstances de fait ou de droit nouvelles, et ce alors même qu'ils ont cru devoir solliciter un nouvel avis du conseil de l'hospitalisation ; que la société requérante n'étant pas fondée à demander l'annulation de la décision du 2 juin 2014, ses conclusions dirigées contre la décision du 26 novembre 2014, qui se borne à la confirmer sur recours gracieux, doivent être également rejetées, sans qu'elle puisse utilement se prévaloir des vices propres dont cette seconde décision serait entachée ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la société Laboratoires Alcon est rejetée.<br/>
Article 2 : La présente décision sera notifiée à la société Laboratoires Alcon, au ministre des finances et des comptes publics et à la ministre des affaires sociales et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-04-02 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. LOI. - RETARD OU NON PUBLICATION DES DÉCLARATIONS D'INTÉRÊTS DE MEMBRES D'UN COLLÈGE - VICE ENTACHANT D'ILLÉGALITÉ PAR LUI-MÊME LA DÉLIBÉRATION - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">01-04-03 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. PRINCIPES GÉNÉRAUX DU DROIT. - PRINCIPE D'IMPARTIALITÉ - RETARD OU NON PUBLICATION DES DÉCLARATIONS D'INTÉRÊTS DE MEMBRES D'UN COLLÈGE - MÉCONNAISSANCE DU PRINCIPE D'IMPARTIALITÉ - 1) ABSENCE PAR SOI-MÊME - 2) APPRÉCIATION PAR LE JUGE [RJ1].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">01-04-03-07 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. PRINCIPES GÉNÉRAUX DU DROIT. PRINCIPES INTÉRESSANT L'ACTION ADMINISTRATIVE. - DÉCISION SE BORNANT À REJETER UN RECOURS GRACIEUX, EN L'ABSENCE DE CIRCONSTANCES DE FAIT OU DE DROIT NOUVELLES [RJ3] - MOYENS CRITIQUANT LES VICES PROPRES DE CETTE DÉCISION - INOPÉRANCE DÈS LORS QUE LES CONCLUSIONS CONTRE LA DÉCISION INITIALE SONT REJETÉES [RJ2].
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">54-07-01-04-03 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. QUESTIONS GÉNÉRALES. MOYENS. MOYENS INOPÉRANTS. - DÉCISION SE BORNANT À REJETER UN RECOURS GRACIEUX, EN L'ABSENCE DE CIRCONSTANCES DE FAIT OU DE DROIT NOUVELLES [RJ3] - MOYENS CRITIQUANT LES VICES PROPRES DE CETTE DÉCISION - INOPÉRANCE DÈS LORS QUE LES CONCLUSIONS CONTRE LA DÉCISION INITIALE SONT REJETÉES [RJ2].
</SCT>
<SCT ID="8E" TYPE="PRINCIPAL">62-04-01 SÉCURITÉ SOCIALE. PRESTATIONS. PRESTATIONS D'ASSURANCE MALADIE. - CONSULTATION DU CONSEIL DE L'HOSPITALISATION - RETARD OU NON PUBLICATION DES DÉCLARATIONS D'INTÉRÊTS DES MEMBRES - MÉCONNAISSANCE DU PRINCIPE D'IMPARTIALITÉ - ABSENCE PAR SOI-MÊME, SI DES ÉLÉMENTS MONTRANT L'ABSENCE DE CONFLIT D'INTÉRÊTS SONT PRODUITS [RJ1].
</SCT>
<ANA ID="9A"> 01-04-02 Contestation d'une recommandation du conseil de l'hospitalisation au regard du principe d'impartialité. Le retard dans la souscription ou l'absence de publication de certaines déclarations d'intérêts de membres du conseil ayant siégé lors de l'adoption de cette recommandation n'entachent pas, par eux-mêmes, d'illégalité cette recommandation.</ANA>
<ANA ID="9B"> 01-04-03 Contestation d'une recommandation du conseil de l'hospitalisation au regard du principe d'impartialité.... ,,1) Le retard dans la souscription ou l'absence de publication de certaines déclarations d'intérêts de membres du conseil ne révèlent pas, par eux-mêmes, une méconnaissance du principe d'impartialité.,,,2) Dans une telle hypothèse, il appartient, en revanche, aux ministres chargés de la santé et de la sécurité sociale, auprès de qui est placé le conseil de l'hospitalisation, pour celles des personnes dont la déclaration obligatoire d'intérêts échapperait ainsi au débat contradictoire, de verser au dossier l'ensemble des éléments permettant au juge de s'assurer, après transmission aux parties, de l'absence ou de l'existence de liens d'intérêts et d'apprécier, le cas échéant, si ces liens sont de nature à révéler des conflits d'intérêts. La circonstance que des déclarations d'intérêts ainsi versées au débat contradictoire soient postérieures de quelques mois à la date de la réunion du conseil de l'hospitalisation n'est, par elle-même, pas susceptible de révéler l'existence de conflits d'intérêts.</ANA>
<ANA ID="9C"> 01-04-03-07 Requérant attaquant une décision de refus prise après une consultation obligatoire et le rejet de son recours gracieux.... ,,Dès lors que le ministre, par sa décision de rejet du recours gracieux, ne peut être regardé comme ayant entendu retirer ou modifier sa décision initiale, et ce alors même qu'il a cru devoir solliciter un nouvel avis de l'organisme consultatif, qu'il n'a pas eu à se prononcer au vu de circonstances de fait ou de droit nouvelles et que le requérant n'est pas fondé à demander l'annulation de la décision initiale, des conclusions tendant à l'annulation de la décision rejetant le recours gracieux doivent être également rejetées, sans qu'il puisse utilement se prévaloir des vices propres dont cette seconde décision serait entachée.</ANA>
<ANA ID="9D"> 54-07-01-04-03 Requérant attaquant une décision de refus prise après une consultation obligatoire et le rejet de son recours gracieux.... ,,Dès lors que le ministre, par sa décision de rejet du recours gracieux, ne peut être regardé comme ayant entendu retirer ou modifier sa décision initiale, et ce alors même qu'il a cru devoir solliciter un nouvel avis de l'organisme consultatif, qu'il n'a pas eu à se prononcer au vu de circonstances de fait ou de droit nouvelles et que le requérant n'est pas fondé à demander l'annulation de la décision initiale, des conclusions tendant à l'annulation de la décision rejetant le recours gracieux doivent être également rejetées, sans qu'il puisse utilement se prévaloir des vices propres dont cette seconde décision serait entachée.</ANA>
<ANA ID="9E"> 62-04-01 Contestation d'une recommandation du conseil de l'hospitalisation au regard du principe d'impartialité. Si le retard dans la souscription ou l'absence de publication de certaines déclarations d'intérêts de membres du conseil ne révèlent pas, par eux-mêmes, une méconnaissance du principe d'impartialité, il appartient, en revanche, aux ministres chargés de la santé et de la sécurité sociale, auprès de qui est placé le conseil de l'hospitalisation, pour celles des personnes dont la déclaration obligatoire d'intérêts échapperait ainsi au débat contradictoire, de verser au dossier l'ensemble des éléments permettant au juge de s'assurer, après transmission aux parties, de l'absence ou de l'existence de liens d'intérêts et d'apprécier, le cas échéant, si ces liens sont de nature à révéler des conflits d'intérêts. La circonstance que des déclarations d'intérêts ainsi versées au débat contradictoire soient postérieures de quelques mois à la date de la réunion du conseil de l'hospitalisation n'est, par elle-même, pas susceptible de révéler l'existence de conflits d'intérêts.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. sol. contr., dans un cas où aucun élément n'est produit, CE, 27 avril 2011, Association pour une formation médicale indépendante (FORMINDEP), n° 334396, p. 168., ,[RJ2] Comp., dans un cas où la décision initiale est illégale, pour un recours hiérarchique, CE, Section, 1er octobre 1954, Mme Bonnetblanc, p. 491 ; pour un recours gracieux, CE, Section, 6 juillet 1990, Clinique Les Martinets, n° 77546, p. 202, aux Tables sur un autre point., ,[RJ3] Cf., s'agissant d'un recours gracieux contre une décision non créatrice de droits, CE, Section, 6 juillet 1990, Clinique Les Martinets, n° 77546, p. 202.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
