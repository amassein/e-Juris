<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036233158</ID>
<ANCIEN_ID>JG_L_2017_12_000000403458</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/23/31/CETATEXT000036233158.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 4ème chambres réunies, 18/12/2017, 403458</TITRE>
<DATE_DEC>2017-12-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>403458</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 4ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP RICHARD ; SCP DE NERVO, POUPET</AVOCATS>
<RAPPORTEUR>M. Florian Roussel</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Laurence Marion</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:403458.20171218</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Mme A...B...a demandé au tribunal administratif de Grenoble d'annuler le titre exécutoire n° H 0016784 émis le 3 octobre 2011 par le directeur du centre hospitalier d'Aix-les-Bains la constituant débitrice d'une somme de 12 436,08 euros et de la décharger de l'obligation de payer cette somme. Par un jugement n° 1106009 du 23 septembre 2014, le tribunal administratif a fait droit à sa demande.<br/>
<br/>
              Par un arrêt n° 14LY03485 du 12 juillet 2016, la cour administrative d'appel de Lyon a, sur appel du centre hospitalier d'Aix-les-Bains, annulé ce jugement et rejeté la demande présentée par Mme B...devant le tribunal administratif.  <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 12 septembre et 12 décembre 2016 au secrétariat du contentieux du Conseil d'Etat, Mme B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge du centre hospitalier d'Aix-les-Bains la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
<br/>
              - le code de la santé publique ;<br/>
<br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
<br/>
              - le décret n° 91-966 du 20 septembre 1991 ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Florian Roussel, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Laurence Marion, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Richard, avocat de Mme B...et à la SCP de Nervo, Poupet, avocat du centre hospitalier d'Aix-les-Bains.<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que Mme B... a été nommée, par arrêté du 1er février 2008, praticien hospitalier à temps plein au centre hospitalier d'Aix-les-Bains, où elle exerçait les fonctions de chef du service des urgences ; que, s'étant engagée lors de sa nomination, puis à nouveau en février 2011, à ne pas exercer d'activité libérale parallèlement à son activité hospitalière, elle percevait l'indemnité d'engagement de service public exclusif prévue par l'article D. 6152-23-1 du code de la santé publique ; qu'ayant appris qu'elle avait été recrutée par un contrat à temps plein à durée indéterminée par ERDF - GDF à compter du 6 avril 2009, le centre hospitalier d'Aix-les-Bains lui a notamment demandé de rembourser l'indemnité d'engagement de service public exclusif perçue du 1er avril 2009 au 31 août 2011, soit un montant de 12 346,08 euros, et a émis un titre exécutoire à cette fin, le 3 octobre 2011 ; que, par une requête enregistrée le 9 novembre 2011, Mme B...a demandé au tribunal administratif de Grenoble d'annuler le titre exécutoire et de la décharger de l'obligation de payer la somme demandée ; que par un jugement du 23 septembre 2014, le tribunal a fait droit à sa demande ; que saisie par le centre hospitalier, la cour administrative d'appel de Lyon a, par un arrêt du 12 juillet 2016, annulé ce jugement et rejeté la demande de première instance ; que Mme B...se pourvoit en cassation contre cet arrêt ;<br/>
<br/>
              2. Considérant, d'une part, qu'aux termes des dispositions alors en vigueur de l'article 25 de la loi du 13 juillet 1983, rendu applicable aux praticiens hospitaliers par l'article L. 6152-4 du code de la santé publique : " I.- Les fonctionnaires et agents non titulaires de droit public consacrent l'intégralité de leur activité professionnelle aux tâches qui leur sont confiées. Ils ne peuvent exercer à titre professionnel une activité privée lucrative de quelque nature que ce soit. / (...) / Les fonctionnaires et agents non titulaires de droit public peuvent toutefois être autorisés à exercer, dans des conditions fixées par décret en Conseil d'Etat, à titre accessoire, une activité, lucrative ou non, auprès d'une personne ou d'un organisme public ou privé, dès lors que cette activité est compatible avec les fonctions qui leur sont confiées et n'affecte pas leur exercice. (...) V.- Sans préjudice de l'application de l'article 432-12 du code pénal, la violation du présent article donne lieu au reversement des sommes indûment perçues, par voie de retenue sur le traitement. " ; qu'en vertu de l'article R. 6152-24 du code de la santé publique, les praticiens ne peuvent percevoir des rémunérations s'ajoutant aux émoluments, indemnités et allocations versés au titre de leur activité de praticien hospitalier que pour les activités et dans les conditions prévues par cet article ; <br/>
<br/>
              3. Considérant, d'autre part, qu'en vertu des dispositions de l'article L. 6154-1 du code de la santé publique, les praticiens hospitaliers statutaires à temps plein sont autorisés à exercer une activité libérale au sein de leur établissement  dans les conditions prévues aux articles L. 6154-2 à L. 6154-7 ; que le 6° de l'article D. 6152-23-1 dispose que figure au nombre des indemnités et allocations que peuvent percevoir les praticiens hospitaliers " une indemnité d'engagement de service public exclusif versée aux praticiens qui s'engagent, pour une période de trois ans renouvelable, à ne pas exercer une activité libérale telle que prévue à l'article L. 6154-1 " ;<br/>
<br/>
              4. Considérant qu'il résulte de ces dispositions que si l'exercice par un praticien hospitalier d'une activité professionnelle non autorisée en application des dispositions de l'article R. 6152-24 du code de la santé publique donne lieu au reversement des sommes perçues au titre de cette activité par voie de retenue sur le traitement, le reversement de l'indemnité d'engagement de service public exclusif ne peut être légalement opéré que si l'intéressé a méconnu son engagement de ne pas exercer une activité libérale au sein de son établissement ;<br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède qu'en jugeant que l'exercice concomitant par Mme B...de son activité de praticien hospitalier et d'une activité salariée rémunérée à temps plein pour une autre personne morale justifiait le reversement au centre hospitalier des sommes perçues au titre de l'indemnité d'engagement de service public exclusif au cours de la période de cumul non autorisé d'activités, alors qu'elle relevait que l'intéressée n'avait pas méconnu son engagement de ne pas exercer d'activité libérale au sein de son établissement, la cour administrative d'appel de Lyon a commis une erreur de droit ; que Mme B...est, dès lors, fondée à demander l'annulation de l'arrêt attaqué ;<br/>
<br/>
              6. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de MmeB..., qui n'est pas la partie perdante dans la présente instance, la somme que le centre hospitalier d'Aix-les-Bains demande au titre des frais exposés par lui et non compris dans les dépens ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du centre hospitalier d'Aix-les-Bains une somme de 3 000 euros, à verser à Mme B...au même titre ; <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Lyon du 12 juillet 2016 est annulé.<br/>
<br/>
		Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Lyon.<br/>
<br/>
Article 3 : Le centre hospitalier d'Aix-les-Bains versera à Mme B...une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : Les conclusions présentées par le centre hospitalier d'Aix-les-bains au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 5 : La présente décision sera notifiée à Mme A...B...et au centre hospitalier d'Aix-les-Bains.<br/>
		Copie en sera adressée à la ministre des solidarités et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-11-01-03 FONCTIONNAIRES ET AGENTS PUBLICS. DISPOSITIONS PROPRES AUX PERSONNELS HOSPITALIERS. PERSONNEL MÉDICAL. PRATICIENS À TEMPS PLEIN. - OBLIGATION DE REVERSEMENT DES SOMMES PERÇUES PAR UN PRATICIEN HOSPITALIER EN MÉCONNAISSANCE DES RÈGLES DE CUMUL - REVERSEMENT DE L'INDEMNITÉ D'ENGAGEMENT DE SERVICE EXCLUSIF - CONDITION - NON RESPECT PAR LE PRATICIEN DE SON ENGAGEMENT DE NE PAS EXERCER UNE ACTIVITÉ LIBÉRALE AU SEIN DE L'ÉTABLISSEMENT.
</SCT>
<ANA ID="9A"> 36-11-01-03 Il résulte de l'article 25 de la loi n° 83-634 du 13 juillet 1983, dans sa rédaction antérieure à la loi n° 2016-483, rendu applicable aux praticiens hospitaliers par l'article L. 6152-4 du code de la santé publique (CSP), et des articles L. 6154-1 et R. 6152-24 et du 6° de l'article D. 6152-23-1 du CSP que si l'exercice par un praticien hospitalier d'une activité professionnelle non autorisée en application des dispositions de l'article R. 6152-24 du CSP donne lieu au reversement des sommes perçues au titre de cette activité par voie de retenue sur le traitement, le reversement de l'indemnité d'engagement de service public exclusif ne peut être légalement opéré que si l'intéressé a méconnu son engagement de ne pas exercer une activité libérale au sein de son établissement.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
