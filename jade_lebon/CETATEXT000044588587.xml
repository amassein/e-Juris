<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044588587</ID>
<ANCIEN_ID>JG_L_2021_12_000000433620</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/58/85/CETATEXT000044588587.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 6ème chambres réunies, 27/12/2021, 433620</TITRE>
<DATE_DEC>2021-12-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>433620</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 6ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, TEXIDOR, PERIER ; SCP LYON-CAEN, THIRIEZ ; SCP DUHAMEL - RAMEIX - GURY- MAITRE</AVOCATS>
<RAPPORTEUR>M. François Charmont</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:433620.20211227</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B... A... a porté plainte contre Mme D... C... devant la chambre de discipline du conseil central de la section D de l'ordre des pharmaciens. Par une décision du 25 septembre 2017, la chambre de discipline a infligé à Mme C... la sanction de l'interdiction temporaire d'exercer la pharmacie pendant une durée de trois semaines.<br/>
<br/>
              Par une décision du 19 juin 2019, la chambre de discipline du Conseil national de l'ordre des pharmaciens a, sur appel de Mme C..., annulé cette décision et rejeté la plainte.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 16 août et 18 novembre 2019 et le 17 juillet 2020 au secrétariat du contentieux du Conseil d'Etat, Mme A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette décision ;<br/>
<br/>
              2°) de mettre à la charge de Mme C... la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la santé publique ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. François Charmont, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteure publique.<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Duhamel - Rameix - Gury - Maître, avocat de Mme A..., à la SCP Célice, Texidor, Perier, avocat du Conseil national de l'ordre des pharmaciens et à la SCP Lyon-Caen, Thiriez, avocat de Mme C....<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis à la chambre de discipline du Conseil national de l'ordre des pharmaciens que Mme C..., pharmacienne alors employée dans l'officine de Mme A..., a, dans le courant de l'année 2015, saisi le conseil de prud'hommes de Créteil d'une demande de résiliation de son contrat de travail, à l'appui de laquelle elle a produit des copies d'ordonnances et de feuilles de soins de certains clients de l'officine. Mme A... se pourvoit en cassation contre la décision du 19 juin 2019 par laquelle la chambre de discipline du Conseil national de l'ordre des pharmaciens a annulé, sur appel de Mme C..., la décision du 25 septembre 2017 par laquelle la chambre de discipline du conseil central de la section D de cet ordre avait, à la suite d'une plainte de sa part, infligé à Mme C... la sanction d'interdiction temporaire d'exercer la pharmacie pour une durée de trois semaines.<br/>
<br/>
              2. Aux termes de l'article R. 4235-5 du code de la santé publique : " Le secret professionnel s'impose à tous les pharmaciens dans les conditions établies par la loi. / Tout pharmacien doit en outre veiller à ce que ses collaborateurs soient informés de leurs obligations en matière de secret professionnel et à ce qu'ils s'y conforment ".<br/>
<br/>
              3. Il résulte des termes mêmes de la décision attaquée que, pour annuler la décision du 25 septembre 2017 de la chambre de discipline de première instance et rejeter la plainte de Mme A..., la chambre de discipline du Conseil national de l'ordre des pharmaciens a jugé que la production par Mme C..., devant le conseil de prud'hommes, de documents nominatifs couverts par le secret médical ne méconnaissait pas l'obligation de secret rappelée par les dispositions de l'article R. 4235-5 du code de la santé publique cité ci-dessus, dès lors que ces documents avaient été anonymisés en cours d'instance devant le conseil de prud'hommes et que leur divulgation s'était opérée dans le cadre d'une instance judiciaire, à l'égard de personnes elles-mêmes soumises au secret professionnel.<br/>
<br/>
              4. En statuant ainsi, alors que la circonstance que des documents soient produits dans le cadre d'une instance judiciaire n'a pas, par elle-même, pour effet de soustraire la partie qui les divulgue au respect du secret médical et qu'il appartenait ainsi à la chambre de discipline de rechercher si cette absence d'anonymisation de pièces couvertes par le secret médical était, dans le cadre de l'instance en cause devant le conseil de prud'hommes, strictement nécessaire à la défense de ses droits par l'intéressée, la chambre de discipline a commis une erreur de droit.<br/>
<br/>
              5. Il résulte de tout ce qui précède que Mme A... est, sans qu'il soit besoin de se prononcer sur les autres moyens de son pourvoi, fondée à demander l'annulation de la décision de la chambre de discipline du Conseil national de l'ordre des pharmaciens qu'elle attaque.<br/>
<br/>
              6. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de Mme A..., qui n'est pas, dans la présente instance, la partie perdante, la somme que demande, à ce titre, Mme C.... Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de Mme C... la somme demandée par Mme A... au titre de ces mêmes dispositions.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : La décision du 19 juin 2019 de la chambre de discipline du Conseil national de l'ordre des pharmaciens est annulée.<br/>
Article 2 : L'affaire est renvoyée devant la chambre de discipline du Conseil national de l'ordre des pharmaciens.<br/>
Article 3 : Le surplus des conclusions de Mme A..., présenté au titre de l'article L. 761-1 du code de justice administrative, est rejeté.<br/>
Article 4 : Les conclusions présentées par Mme C... au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à Mme B... A..., à Mme D... C... et au Conseil national de l'ordre des pharmaciens.<br/>
		Copie en sera adressée au ministre des solidarités et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">26-03-10 DROITS CIVILS ET INDIVIDUELS. - LIBERTÉS PUBLIQUES ET LIBERTÉS DE LA PERSONNE. - SECRET DE LA VIE PRIVÉE. - SECRET MÉDICAL - VIOLATION - PRODUCTION DE DOCUMENTS NON ANONYMISÉS COUVERTS PAR CE SECRET DANS LE CADRE D'UNE INSTANCE JUDICIAIRE - EXISTENCE [RJ1], SAUF SI L'ABSENCE D'ANONYMISATION EST STRICTEMENT NÉCESSAIRE À LA DÉFENSE DE SES DROITS PAR L'INTÉRESSÉ [RJ2].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">55-03-04-03 PROFESSIONS, CHARGES ET OFFICES. - CONDITIONS D'EXERCICE DES PROFESSIONS. - PHARMACIENS. - RÈGLES DIVERSES S'IMPOSANT AUX PHARMACIENS DANS L'EXERCICE DE LEUR PROFESSION. - SECRET MÉDICAL - VIOLATION - PRODUCTION DE DOCUMENTS NON ANONYMISÉS COUVERTS PAR CE SECRET DANS LE CADRE D'UNE INSTANCE JUDICIAIRE - EXISTENCE [RJ1], SAUF SI L'ABSENCE D'ANONYMISATION EST STRICTEMENT NÉCESSAIRE À LA DÉFENSE DE SES DROITS PAR L'INTÉRESSÉ [RJ2].
</SCT>
<ANA ID="9A"> 26-03-10 La circonstance que des documents soient produits dans le cadre d'une instance judiciaire, à l'égard de personnes elles-mêmes soumises au secret professionnel, n'a pas, par elle-même, pour effet de soustraire la partie qui les divulgue au respect du secret médical.......Dès lors, la production par un pharmacien, devant le conseil de prud'hommes à l'occasion d'un litige l'opposant à son employeur, de documents nominatifs couverts par le secret médical méconnait l'obligation de secret rappelée par l'article R. 4235-5 du code de la santé publique (CSP), sauf si l'absence d'anonymisation de ces pièces est, dans le cadre de l'instance en cause, strictement nécessaire à la défense de ses droits par l'intéressé.</ANA>
<ANA ID="9B"> 55-03-04-03 La circonstance que des documents soient produits dans le cadre d'une instance judiciaire, à l'égard de personnes elles-mêmes soumises au secret professionnel, n'a pas, par elle-même, pour effet de soustraire la partie qui les divulgue au respect du secret médical.......Dès lors, la production par un pharmacien, devant le conseil de prud'hommes à l'occasion d'un litige l'opposant à son employeur, de documents nominatifs couverts par le secret médical méconnait l'obligation de secret rappelée par l'article R. 4235-5 du code de la santé publique (CSP), sauf si l'absence d'anonymisation de ces pièces est, dans le cadre de l'instance en cause, strictement nécessaire à la défense de ses droits par l'intéressé.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 23 avril 1997, M. Levy, n° 169977, p. 162 ; CE, 13 janvier 1999, M. Lardennois, n° 177913, T. pp. 638-784-1002...[RJ2] Rappr. Cass. crim, 11 mai 2004, n° 03-80.254, Bull. crim. 2004, n° 113 ; Cour. EDH, 13 mai 2008, N.N. et T.A. c. Belgique, n° 65097/01.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
