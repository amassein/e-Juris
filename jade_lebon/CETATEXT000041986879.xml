<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041986879</ID>
<ANCIEN_ID>JG_L_2020_06_000000431179</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/98/68/CETATEXT000041986879.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 10/06/2020, 431179</TITRE>
<DATE_DEC>2020-06-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>431179</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT>Mme Mireille Le Corre</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:431179.20200610</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B... A... a demandé au tribunal administratif de Montpellier d'annuler l'arrêté du 28 juillet 2015 par lequel le préfet de l'Hérault l'a obligé à quitter le territoire français sans délai. Par un jugement n° 1505775 du 3 novembre 2015, le tribunal administratif de Montpellier a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 16MA02527 du 28 mars 2019, la cour administrative d'appel de Marseille a rejeté l'appel formé par M. A... contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 28 mai et 29 août 2019 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution ; <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - la charte des droits fondamentaux de l'Union européenne ; <br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ; <br/>
              - le décret n° 2016-1458 du 28 octobre 2016 ;<br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean Sirinelli, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Mireille Le Corre, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Spinosi, Sureau, avocat de M. A... ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              Sur l'intervention de la section française de l'Observatoire international des prisons :<br/>
<br/>
              1. La section française de l'Observatoire international des prisons justifie d'un intérêt suffisant à l'annulation de l'arrêt. Ainsi son intervention est recevable.<br/>
<br/>
              Sur le pourvoi :<br/>
<br/>
              2. Il ressort des pièces du dossier soumis aux juges du fond que M. A..., né le 16 avril 1982, de nationalité nigériane, a demandé l'annulation de l'arrêté en date du 28 juillet 2015 par lequel le préfet de l'Hérault l'a obligé à quitter le territoire français sans délai à destination de son pays d'origine ou bien dans tout pays dans lequel il serait légalement admissible.<br/>
<br/>
              3. Aux termes de l'article R. 421-5 du code de justice administrative : " Les délais de recours contre une décision administrative ne sont opposables qu'à la condition d'avoir été mentionnés, ainsi que les voies de recours, dans la notification de la décision ". Aux termes de l'article R. 776-1 du même code : " Sont présentées, instruites et jugées selon les dispositions de l'article L. 512-1 du code de l'entrée et du séjour des étrangers et du droit d'asile et celles du présent code, sous réserve des dispositions du présent chapitre, les requêtes dirigées contre : / 1° Les décisions portant obligation de quitter le territoire français, prévues au I de l'article L. 511-1 et à l'article L. 511-3-1 du code de l'entrée et du séjour des étrangers et du droit d'asile, ainsi que les décisions relatives au séjour notifiées avec les décisions portant obligation de quitter le territoire français ; / 2° Les décisions relatives au délai de départ volontaire prévues au II de l'article L. 511-1 du même code ; / 3° Les interdictions de retour sur le territoire français prévues au III du même article et les interdictions de circulation sur le territoire français prévues à l'article L. 511-3-2 du même code ; / 4° Les décisions fixant le pays de renvoi prévues à l'article L. 513-3 du même code ; / (...) / 6° Les décisions d'assignation à résidence prévues aux articles L. 561-2, L. 744-9-1 et L. 571-4 du même code. / Sont instruites et jugées dans les mêmes conditions les conclusions tendant à l'annulation d'une autre mesure d'éloignement prévue au livre V du code de l'entrée et du séjour des étrangers et du droit d'asile, à l'exception des arrêtés d'expulsion, présentées en cas de placement en rétention administrative, en cas de détention ou dans le cadre d'une requête dirigée contre la décision d'assignation à résidence prise au titre de cette mesure. / Sont instruites et jugées dans les mêmes conditions les conclusions présentées dans le cadre des requêtes dirigées contre les décisions portant obligation de quitter le territoire français mentionnées au 1° du présent article, sur le fondement de l'article L. 743-3 du code de l'entrée et du séjour des étrangers et du droit d'asile, tendant à la suspension de l'exécution de ces mesures d'éloignement ". <br/>
<br/>
              4. Par ailleurs, l'article L. 512-2 du code d'entrée et de séjour des étrangers en France et du droit d'asile dispose : " Dès notification de l'obligation de quitter le territoire français, l'étranger auquel aucun délai de départ volontaire n'a été accordé est mis en mesure, dans les meilleurs délais, d'avertir un conseil, son consulat ou une personne de son choix. L'étranger est informé qu'il peut recevoir communication des principaux éléments des décisions qui lui sont notifiées en application de l'article L. 511-1. Ces éléments lui sont alors communiqués dans une langue qu'il comprend ou dont il est raisonnable de supposer qu'il la comprend ".<br/>
<br/>
              5. Enfin, d'une part, aux termes de l'article R. 776-19 du code de justice administrative : " Si, au moment de la notification d'une décision mentionnée à l'article R. 776-1, l'étranger est retenu par l'autorité administrative, sa requête peut valablement être déposée, dans le délai de recours contentieux, auprès de ladite autorité administrative ". D'autre part, il résulte des dispositions combinées des articles R. 776-29 et R. 776-31 du même code, issues du décret du 28 octobre 2016 pris pour l'application du titre II de la loi du 7 mars 2016 relative au droit des étrangers en France, que les étrangers ayant reçu notification d'une décision mentionnée à l'article R. 776-1 du code alors qu'ils sont en détention ont la faculté de déposer leur requête, dans le délai de recours contentieux, auprès du chef de l'établissement pénitentiaire.<br/>
<br/>
              6. Pour rendre opposable le délai de recours contentieux, l'administration est tenue, en application de l'article R. 421-5 du code de justice administrative, de faire figurer dans la notification de ses décisions la mention des délais et voies de recours contentieux ainsi que les délais des recours administratifs préalables obligatoires. Elle n'est en principe pas tenue d'ajouter d'autres indications, notamment les délais de distance, la possibilité de former des recours gracieux et hiérarchiques facultatifs ou la possibilité de former une demande d'aide juridictionnelle. Si des indications supplémentaires sont toutefois ajoutées, ces dernières ne doivent pas faire naître d'ambiguïtés de nature à induire en erreur les destinataires des décisions dans des conditions telles qu'ils pourraient se trouver privés du droit à un recours effectif.<br/>
<br/>
              7. En cas de rétention ou de détention, lorsque l'étranger entend contester une décision prise sur le fondement du code de l'entrée et du séjour des étrangers et du droit d'asile pour laquelle celui-ci a prévu un délai de recours bref, notamment lorsqu'il entend contester une décision portant obligation de quitter le territoire sans délai, la circonstance que sa requête ait été adressée, dans le délai de recours, à l'administration chargée de la rétention ou au chef d'établissement pénitentiaire, fait obstacle à ce qu'elle soit regardée comme tardive, alors même qu'elle ne parviendrait au greffe du tribunal administratif qu'après l'expiration de ce délai de recours.<br/>
<br/>
              8. Jusqu'à l'entrée en vigueur des dispositions mentionnées au point 5 ci-dessus, l'administration n'était pas tenue de faire figurer, dans la notification à un étranger retenu ou détenu d'une décision prise sur le fondement du code de l'entrée et du séjour des étrangers et du droit d'asile pour laquelle celui-ci a prévu un délai de recours bref, notamment d'une décision portant obligation de quitter le territoire sans délai, pour laquelle l'article L. 512-1 de ce code prévoit un délai de recours de quarante-huit heures, la possibilité de déposer une requête contre cette décision, dans le délai de recours, auprès de l'administration chargée de la rétention ou du chef d'établissement pénitentiaire. Les obligations prévues par les dispositions citées au point 4 ci-dessus de l'article L512-2 du code de l'entrée et du séjour des étrangers et du droit d'asile, qui n'ont pas pour objet de définir les conditions de régularité de la notification, sont à cet égard sans incidence. En revanche, depuis l'entrée en vigueur des dispositions mentionnées au point 5, notamment, pour les étrangers détenus, des dispositions du décret du 28 octobre 2016 précité, il incombe à l'administration, pour les décisions présentant les caractéristiques mentionnées ci-dessus, de faire figurer, dans leur notification à un étranger retenu ou détenu, la possibilité de déposer sa requête dans le délai de recours contentieux auprès de l'administration chargée de la rétention ou du chef de l'établissement pénitentiaire. <br/>
<br/>
              9. Il ressort des énonciations de l'arrêt attaqué que la notification à M. A..., alors incarcéré, de l'arrêté attaqué du 28 juillet 2015 est antérieure à l'entrée en vigueur des dispositions du code de justice administrative issues du décret du 28 octobre 2016. Il s'ensuit que la cour administrative d'appel de Marseille n'a pas commis d'erreur de droit en jugeant qu'à cette date, l'administration n'était pas tenue de faire figurer dans la notification la faculté déjà mentionnée pour que le délai de recours contentieux soit opposable à l'intéressé.<br/>
<br/>
              10. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'intervention de la section française de l'Observatoire international des prisons est admise.<br/>
Article 2 : Le pourvoi de M. A... est rejeté.<br/>
Article 3 : La présente décision sera notifiée à M. B... A..., à la section française de l'Observatoire international des prisons et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">335-03-03 ÉTRANGERS. OBLIGATION DE QUITTER LE TERRITOIRE FRANÇAIS (OQTF) ET RECONDUITE À LA FRONTIÈRE. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. - DÉCISIONS ASSORTIES D'UN DÉLAI DE RECOURS BREF - RECOURS DE L'ÉTRANGER RETENU OU DÉTENU - 1) INTERRUPTION DU DÉLAI DE RECOURS CONTENTIEUX PAR UNE REQUÊTE ADRESSÉE À L'AUTORITÉ ADMINISTRATIVE - EXISTENCE - 2) OBLIGATION DE MENTIONNER L'EXISTENCE DE CE MODE D'INTRODUCTION DE LA REQUÊTE [RJ1] - A) AVANT L'ENTRÉE EN VIGUEUR DES DISPOSITIONS PRÉVOYANT CE MODE D'INTRODUCTION - ABSENCE - B) APRÈS LEUR ENTRÉE EN VIGUEUR - EXISTENCE.
</SCT>
<ANA ID="9A"> 335-03-03 1) En cas de rétention ou de détention, lorsque l'étranger entend contester une décision prise sur le fondement du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA) pour laquelle celui-ci a prévu un délai de recours bref, notamment lorsqu'il entend contester une décision portant obligation de quitter le territoire (OQTF) sans délai, la circonstance que sa requête ait été adressée, dans le délai de recours, à l'administration chargée de la rétention ou au chef d'établissement pénitentiaire, fait obstacle à ce qu'elle soit regardée comme tardive, alors même qu'elle ne parviendrait au greffe du tribunal administratif qu'après l'expiration de ce délai de recours.,,,2) Jusqu'à l'entrée en vigueur des articles R. 776-19, R. 776-29 et R. 776-31 du code de justice administrative (CJA), l'administration n'était pas tenue de faire figurer, dans la notification à un étranger retenu ou détenu d'une décision prise sur le fondement du CESEDA pour laquelle celui-ci a prévu un délai de recours bref, notamment d'une décision portant OQTF sans délai, pour laquelle l'article L. 512-1 de ce code prévoit un délai de recours de quarante-huit heures, la possibilité de déposer une requête contre cette décision, dans le délai de recours, auprès de l'administration chargée de la rétention ou du chef d'établissement pénitentiaire. Les obligations prévues par l'article L512-2 du CESEDA, qui n'ont pas pour objet de définir les conditions de régularité de la notification, sont à cet égard sans incidence.... ,,b) En revanche, depuis l'entrée en vigueur des articles R. 776-19, R. 776-29 et R. 776-31 du CJA, notamment, pour les étrangers détenus, des dispositions issues du décret n° 2016-1458 du 28 octobre 2016 précité, il incombe à l'administration, pour les décisions présentant les caractéristiques mentionnées ci-dessus, de faire figurer, dans leur notification à un étranger retenu ou détenu, la possibilité de déposer sa requête dans le délai de recours contentieux auprès de l'administration chargée de la rétention ou du chef de l'établissement pénitentiaire.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., sur les conséquences de la mention des voies et délais de recours sur l'opposabilité du délai de recours contentieux, CE, 16 octobre 2017, M.,, n° 411169, T. pp. 633-724.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
