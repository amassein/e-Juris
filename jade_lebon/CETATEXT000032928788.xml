<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032928788</ID>
<ANCIEN_ID>JG_L_2016_07_000000374114</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/92/87/CETATEXT000032928788.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 4ème chambres réunies, 22/07/2016, 374114</TITRE>
<DATE_DEC>2016-07-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>374114</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARADUC, DUHAMEL, RAMEIX ; SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR>Mme M. Perrière</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Laurence Marion</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:374114.20160722</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une décision du 22 juillet 2015, le Conseil d'Etat statuant au contentieux a, avant-dire droit sur les requêtes n° 374114 et 383009 du Syndicat interprofessionnel des radios et des télévisions indépendantes (SIRTI) et sur la requête n° 374183 de la société Vortex tendant à l'annulation pour excès de pouvoir de la délibération du Conseil supérieur de l'audiovisuel (CSA) du 11 décembre 2013 relative à la fixation des règles permettant de déterminer la somme des populations desservies par un service de radio autorisé en mode analogique par voie hertzienne terrestre ainsi que de la décision du 21 mai 2014 par laquelle le CSA a rejeté la demande du SIRTI tendant au retrait de cette délibération, demandé à M. B..., expert agréé par la Cour de cassation, en application de l'article R. 625-2 du code de justice administrative, un avis technique portant sur la fiabilité des paramètres retenus par la délibération en ce qui concerne le modèle de propagation, le seuil de réception et la prise en compte des quatre principaux brouilleurs constants, sur la possibilité de disposer d'instruments de mesure plus fiables et sur le point de savoir si la méthode définie par le CSA peut seulement conduire à surévaluer les populations desservies par les services de radio.<br/>
<br/>
              M. B... a remis son avis technique le 20 janvier 2016. <br/>
<br/>
              Les parties ont été invitées à faire connaître au Conseil d'Etat quelles seraient les conséquences d'une annulation rétroactive de la délibération. <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
<br/>
              - la loi du n° 86-1067 du 30 septembre 1986 ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Manon Perrière, auditeur,  <br/>
<br/>
              - les conclusions de Mme Laurence Marion, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Spinosi, Sureau, avocat du syndicat interprofessionnel des radios et télévisions indépendantes et à la SCP Baraduc, Duhamel, Rameix, avocat du Conseil supérieur de l'audiovisuel ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 22 juin 2016, présentée par le CSA ; <br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes du premier alinéa de l'article 41 de la loi du 30 septembre 1986 : " Une même personne physique ou morale ne peut, sur le fondement d'autorisations relatives à l'usage de fréquences dont elle est titulaire pour la diffusion d'un ou de plusieurs services de radio par voie hertzienne terrestre en mode analogique, ou par le moyen d'un programme qu'elle fournit à d'autres titulaires d'autorisation par voie hertzienne terrestre en mode analogique, disposer en droit ou en fait de plusieurs réseaux que dans la mesure où la somme des populations recensées dans les zones desservies par ces différents réseaux n'excède pas 150 millions d'habitants " ; <br/>
<br/>
              2. Considérant qu'au sens de ces dispositions législatives, la zone desservie par un service de radio est celle dans laquelle ce service peut être reçu dans des conditions satisfaisantes au moyen des postes de réception d'utilisation courante ; que, dès lors que le constat d'un dépassement du seuil de 150 millions d'habitants fait obstacle à la délivrance de nouvelles autorisations à l'opérateur concerné et conduit, par suite, à restreindre la liberté d'entreprendre et la liberté de la communication audiovisuelle, une zone ne peut être regardée comme desservie que s'il est établi avec une certitude raisonnable que le service considéré y est reçu dans de telles conditions ; qu'en l'absence de dispositions réglementaires précisant les conditions d'application de la loi, il appartient au Conseil supérieur de l'audiovisuel (CSA) de définir une méthode afin de déterminer les zones desservies ; <br/>
<br/>
              3. Considérant qu'aux termes de l'article 1er de la délibération du 11 décembre 2013 par laquelle le CSA a énoncé et rendu publics les éléments sur lesquels repose la méthode de calcul qu'il utilise depuis 2011 pour déterminer la population couverte par les services de radio, dont le SIRTI et la société Vortex demandent l'annulation : " Pour contrôler le respect des dispositions du premier alinéa de l'article 41 de la loi n° 86-1067 du 30 septembre 1986, le Conseil détermine la somme des populations couvertes par les éditeurs de services de radio autorisés en mode analogique par voie hertzienne terrestre. / Pour ce faire, il se fonde, d'une part, sur les chiffres de population, authentifiés par décret pris en application de la loi relative à la démocratie de proximité et, d'autre part, sur les paramètres techniques figurant en annexe " ; qu'aux termes de l'annexe de cette même délibération : " La méthode de calcul pour les services diffusés en modulation de fréquence est fondée sur les éléments suivants :/ (...) Seuil de réception : 54 dBµV/m [décibel micro-volt par mètre] à 10m/sol ; Prise en compte des 4 principaux brouilleurs constants / Modèle de propagation : Modèle statistique de la recommandation de l'Union internationale des télécommunications (UIT-P 1546 " Méthode de prévision de la propagation point à zone pour les services de Terre entre 30 MHz et 3 000 MHz3") " ; que les requérants soutiennent que les critères retenus dans la délibération du 11 décembre 2013, en particulier ceux du seuil de réception et celui des quatre principaux brouilleurs constants, ne permettent pas de refléter la réalité de la couverture des services radiophoniques mais conduisent, au contraire, à la réduire artificiellement ; <br/>
<br/>
              4. Considérant que, par la décision du 22 juillet 2015, le Conseil d'Etat statuant au contentieux a, d'une part, admis l'intérêt à agir de la société Vortex et écarté le moyen tiré de l'incompétence du CSA pour déterminer la méthode de calcul fixée par la délibération attaquée ainsi que le moyen tiré de ce que cette délibération ne respecterait pas le principe d'intelligibilité de la norme ; que par cette décision, il a, d'autre part, sur le fondement de l'article R. 625-2 du code de justice administrative, demandé, avant-dire droit, à M. A... un avis technique portant sur la fiabilité des paramètres retenus par le CSA dans sa délibération du 11 décembre 2013 en ce qui concerne le modèle de propagation, le seuil de réception et la prise en compte des quatre principaux brouilleurs constants, sur la possibilité de disposer d'instruments de mesure plus fiables et sur le point de savoir si, comme le soutient le CSA, la méthode qu'il a définie peut seulement conduire à surévaluer les populations desservies par les services de radio ; que l'avis technique a été déposé le 22 janvier 2016 ; <br/>
<br/>
              5. Considérant que la méthode retenue par le CSA ne s'appuie pas sur une mesure effective de la réception mais recourt à une modélisation destinée à approcher cette réalité ; que cette méthode consiste, dans un premier temps, à déterminer, grâce à un modèle de propagation des ondes, la puissance avec laquelle le signal diffusé par l'émetteur d'un service radiophonique donné est reçu en chaque point du territoire ; que le secteur où la puissance du signal reçu à 10 mètres du sol excède un certain seuil, fixé à 54 dBµV/m pour les services diffusés en modulation de fréquence, est pris en considération pour la délimitation de la zone desservie ; que ce secteur est toutefois réduit afin de tenir compte du brouillage du signal résultant de la présence d'autres émetteurs, diffusant d'autres services ; qu'en raison de la complexité du calcul, seuls les quatre principaux brouilleurs sont pris en compte pour effectuer cette correction ; <br/>
<br/>
              6. Considérant qu'il ressort des pièces du dossier que le modèle de propagation des ondes auquel la délibération attaquée se réfère est celui préconisé par l'Union internationale des télécommunications (UIT) pour les besoins de la planification des fréquences ; que le seuil de 54 dBµV/m est celui au-dessus duquel, selon l'UIT, une qualité de service satisfaisante est assurée en zone rurale, en présence de brouillages causés par des installations industrielles et domestiques et pour une réception stéréophonique ; que des seuils plus élevés, respectivement fixés à 66 et 74 dBµV/m, sont retenus en zone urbaine et urbaine dense pour tenir compte de l'existence d'obstacles physiques à la réception ; que, dans le cadre des opérations de planification, les autorités compétentes doivent faire en sorte que, dans le secteur où sa puissance excède les seuils ainsi déterminés, le signal d'un service donné ne soit pas brouillé par les signaux d'autres services ; <br/>
<br/>
              7. Considérant que le CSA fait valoir que, pour l'évaluation des populations desservies par les services de radio en modulation de fréquence, la référence à un seuil unique de réception est imposée par les limites des outils informatiques disponibles, que le choix du seuil de 54 dBµV/m, qui est celui au-dessus duquel une qualité de service satisfaisante est assurée en zone rurale, conduit, dans les zones urbaines et urbaines denses, à regarder comme desservis des secteurs où une réception effective n'est pas garantie et que la prise en compte des quatre principaux brouilleurs constants a pour objet de corriger ce biais afin de cerner la zone desservie ; que, selon l'instance de régulation, cette méthode pourrait conduire à une certaine surévaluation de la population couverte mais ne risque pas de faire apparaître un dépassement fictif du seuil de 150 millions d'habitants fixé par le législateur ;<br/>
<br/>
              8. Considérant que l'avis technique souligne notamment que, dans la méthode définie par l'UIT pour les besoins de la planification des fréquences, le signal du service considéré est mesuré à 10 mètres du sol, alors que la réception des services de radio en modulation de fréquence au moyen d'antennes collectives situées sur le toit a fortement régressé, que l'UIT retient qu'en l'absence de brouillages causés par des installations industrielles et domestiques une qualité de service acceptable est assurée au-dessus de 48 dBµV/m en mode stéréophonique et de 34 dBµV/m en mode monophonique, sans distinguer entre les zones rurales et urbaines, et que les brouillages peuvent être atténués par l'utilisation d'antennes directionnelles ; que l'auteur de l'avis indique également que, compte tenu des limites de l'outil informatique qu'il utilise, le CSA détermine les quatre principaux brouilleurs constants du service considéré en fonction de l'intensité du brouillage mesurée non en tout point du territoire mais au site de l'émetteur du service, alors que cet émetteur peut être implanté en dehors de la partie la plus peuplée de la zone de desserte ; qu'il relève que la méthode mise en oeuvre vise à déterminer les secteurs certainement desservis par le service considéré et conduit, par suite, à ne pas prendre en considération des secteurs où il n'est pas exclu que ce service soit reçu dans des conditions acceptables, sans que cela puisse être regardé comme certain ; qu'il mentionne, enfin, la possibilité de recourir à une méthode " coopérative " consistant à recueillir auprès des opérateurs et des auditeurs des indications relatives à la réception d'un service en différents points du territoire ; <br/>
<br/>
              9. Considérant, toutefois, qu'ainsi qu'il a été dit au point 2, une zone ne peut être regardée comme desservie par un service de radio, au sens de l'article 41 de la loi du 30 septembre 1986, que s'il est établi avec une certitude raisonnable que ce service y est reçu dans des conditions satisfaisantes ; que, dans la définition de la méthode mise en oeuvre afin de déterminer cette zone, le CSA a dû tenir compte des contraintes inhérentes aux outils disponibles à la date de sa délibération, de l'ampleur des investissements qu'aurait impliqué le développement de nouveaux outils et du gain qu'ils auraient permis en termes de fiabilité des résultats ; qu'en choisissant de se référer au modèle de propagation et aux seuils de réception fixés par l'UIT, il a adopté des critères objectifs, adaptés à l'objet de la méthode et de nature à conférer à celle-ci un caractère opérationnel et reproductible ; que la mise en oeuvre systématique de relevés effectués sur place n'était pas raisonnablement envisageable ; que la prise en compte de données transmises par les opérateurs et les auditeurs, mentionnée par l'avis technique, aurait soulevé des difficultés importantes relatives à la fiabilité de ces données ; qu'il ne ressort pas des pièces du dossier qu'en l'état des moyens techniques disponibles à la date de la délibération attaquée, le CSA ait commis une erreur manifeste d'appréciation en estimant que la prise en compte du territoire où le seuil de 54 dBµV/m est dépassé, corrigée pour tenir compte des quatre principaux brouilleurs constants, permettait de déterminer la zone desservie par un service de radio en modulation de fréquence, sans risquer de faire apparaître un dépassement fictif du seuil de desserte fixé par la loi ; que, ce faisant, il n'a méconnu ni les principes de pluralisme et de diversification des opérateurs, ni l'obligation légale qui lui incombe d'éviter les abus de position dominante ainsi que les pratiques restreignant la concurrence ; <br/>
<br/>
              10. Considérant que le détournement de pouvoir allégué n'est pas établi ;<br/>
<br/>
              11. Considérant qu'il résulte de ce qui précède que le SIRTI et la société Vortex ne sont pas fondés à demander l'annulation de la délibération du 11 décembre 2013 et de la décision du 21 mai 2014 ; que, par voie de conséquence, leurs conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative ne peuvent qu'être rejetées ;<br/>
<br/>
              Sur les dépens :<br/>
<br/>
              12. Considérant que, dans les circonstances de l'espèce, il y a lieu de mettre les frais afférents à l'avis technique demandé par le Conseil d'Etat à la charge du SIRTI et de la société Vortex ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Les requêtes du SIRTI et de la société Vortex sont rejetées.<br/>
<br/>
Article 2 : Les dépens sont mis à la charge du SIRTI et de la société Vortex.<br/>
Article 3 : La présente décision sera notifiée au Syndicat interprofessionnel des radios et télévisions indépendantes, à la société Vortex et au Conseil supérieur de l'audiovisuel.<br/>
Copie en sera adressée à la ministre de la culture et de la communication.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-07-02-04 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. CONTRÔLE DU JUGE DE L'EXCÈS DE POUVOIR. APPRÉCIATIONS SOUMISES À UN CONTRÔLE RESTREINT. - DÉFINITION DE LA MÉTHODE DE DÉTERMINATION DES ZONES DESSERVIES PAR UN SERVICE RADIOPHONIQUE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">56-04-01 RADIO ET TÉLÉVISION. SERVICES PRIVÉS DE RADIO ET DE TÉLÉVISION. SERVICES DE RADIO. - LIMITE AU CUMUL DE DIFFUSION DE SERVICES DE RADIO HERTZIENNE PAR UNE MÊME PERSONNE (ART. 41 DE LA LOI DU 30 SEPTEMBRE 1986) - 1) MODALITÉS D'APPLICATION - DÉFINITION PAR LE CSA D'UNE MÉTHODE POUR ÉVALUER LES ZONES DESSERVIES PAR UN SERVICE RADIOPHONIQUE [RJ1] - 2) CONTRÔLE DU JUGE SUR LA DÉFINITION DE CETTE MÉTHODE - CONTRÔLE RESTREINT.
</SCT>
<ANA ID="9A"> 54-07-02-04 Le juge de l'excès de pouvoir exerce un contrôle de l'erreur manifeste d'appréciation sur les choix techniques faits par le Conseil supérieur de l'audiovisuel (CSA) pour définir une méthode permettant de déterminer les zones desservies par un service radiophonique pour l'application de l'article 41 de la loi n° 86-1067 du 30 septembre 1986.</ANA>
<ANA ID="9B"> 56-04-01 Limite au cumul de diffusion de services de radio hertzienne par une même personne (art. 41 de la loi n° 86-1067 du 30 septembre 1986).... ,,1) Au sens de ces dispositions législatives, la zone desservie par un service de radio est celle dans laquelle ce service peut être reçu dans des conditions satisfaisantes au moyen des postes de réception d'utilisation courante. Dès lors que le constat d'un dépassement du seuil de 150 millions d'habitants fait obstacle à la délivrance de nouvelles autorisations à l'opérateur concerné et conduit, par suite, à restreindre la liberté d'entreprendre et la liberté de la communication audiovisuelle, une zone ne peut être regardée comme desservie que s'il est établi avec une certitude raisonnable que le service considéré y est reçu dans de telles conditions. En l'absence de dispositions réglementaires précisant les conditions d'application de la loi, il appartient au Conseil supérieur de l'audiovisuel (CSA) de définir une méthode afin de déterminer les zones desservies.,,,2) Le juge de l'excès de pouvoir exerce un contrôle de l'erreur manifeste d'appréciation sur les choix techniques du CSA dans la définition de cette méthode.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 22 juillet 2015, SIRTI et Vortex, n° 374114 374183 383009, aux Tables sur un autre point.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
