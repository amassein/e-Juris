<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034367614</ID>
<ANCIEN_ID>JG_L_2017_03_000000395506</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/36/76/CETATEXT000034367614.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 5ème chambres réunies, 31/03/2017, 395506</TITRE>
<DATE_DEC>2017-03-31</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>395506</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Sophie Baron</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:395506.20170331</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 22 décembre 2015 au secrétariat du contentieux du Conseil d'Etat, l'association SOS Education demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2015-1351 du 26 octobre 2015 modifiant les dispositions du code de l'éducation relatives à la préparation aux examens des voies générale, professionnelle et technologique des lycées et à la délivrance du baccalauréat ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'éducation ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie Baron, maître des requêtes en service extraordinaire, <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              Sur la fin de non-recevoir soulevée par la ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche :<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que la présidente de l'association SOS Education a produit la délibération par laquelle son conseil d'administration l'autorise à agir en justice ; que, par suite, la fin de non-recevoir soulevée par la ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche, tirée du défaut de qualité pour agir de la présidente de l'association en l'absence d'un tel mandat, doit être écartée ; <br/>
<br/>
              Sur les conclusions de la requête relatives aux conditions de redoublement :<br/>
<br/>
              2. Considérant que les dispositions des articles D. 331-42, D. 331-61, D. 341-20 et D. 341-39 du code de l'éducation, dans leur rédaction issue des dispositions du décret attaqué, prévoient que tout élève ayant échoué à l'examen du baccalauréat général ou technologique, du brevet de technicien, du brevet de technicien supérieur, du certificat d'aptitude professionnelle, du brevet de technicien supérieur agricole, du brevet de technicien agricole, du brevet d'études professionnelles agricoles ou du certificat d'aptitude professionnelle agricole " se voit offrir, à la rentrée scolaire qui suit cet échec, en vue de préparer cet examen, le droit à une nouvelle inscription dans l'établissement dont il est issu, le cas échéant selon des modalités adaptées au niveau des connaissances et compétences qu'il a acquises dans les matières d'enseignement correspondant aux épreuves de l'examen " ; <br/>
<br/>
              3. Considérant que ces dispositions ont pour objet de permettre d'adapter le contenu de l'enseignement dispensé au candidat à l'un des diplômes mentionnés au point 2, lorsqu'il a échoué à une précédente session de l'examen et entend conserver une ou plusieurs notes obtenues lors de cette session ; que si elles sont susceptibles de conduire, dans certaines situations, à ce que le candidat concerné soit dispensé de certains enseignements, cette circonstance n'est pas en elle-même constitutive d'une méconnaissance du principe de continuité éducative posé par les dispositions de l'article L. 311-1 du code de l'éducation ; <br/>
<br/>
<br/>
              Sur les conclusions de la requête relatives à la " mention " portée par les diplômes de baccalauréat :<br/>
<br/>
<br/>
              4. Considérant qu'en vertu des articles D. 334-11 et D. 336-11 du code de l'éducation, les diplômes des baccalauréats général et technologique se voient attribuer les mentions " assez bien ", " bien " ou " très bien " en fonction de la moyenne des notes obtenues par le candidat à l'issue des épreuves dites " du premier groupe " ; que la mention " assez bien " est attribuée aux candidats ayant obtenu une note moyenne au moins égale à 12 et inférieure à 14 ; que la mention " bien " est attribuée aux candidats ayant obtenu une note moyenne au moins égale à 14 et inférieure à 16 ; que la mention " très bien " est attribuée aux candidats ayant obtenu une note moyenne au moins égale à 16 ; <br/>
<br/>
              5. Considérant qu'aux termes des dispositions de l'article D. 334-13 du code de l'éducation, dans sa rédaction issue de l'article 6 du décret attaqué : " Les candidats au baccalauréat général peuvent conserver, après un échec à l'examen, sur leur demande et pour chacune des épreuves du premier groupe, dans la limite des cinq sessions suivant la première session à laquelle ils se sont présentés, le bénéfice des notes égales ou supérieures à 10 qu'ils ont obtenues à ces épreuves. Ils ne subissent alors que les autres épreuves. / Les dispositions du premier alinéa ne s'appliquent qu'aux candidats qui se présentent dans la même série que celle où ils ont obtenu des notes dont ils demandent à conserver le bénéfice, à l'exception de règles particulières définies par arrêté ministériel. / Le renoncement à un bénéfice de notes lors d'une session est définitif et seules les notes obtenues ultérieurement sont prises en compte pour l'attribution du diplôme. / Pour ces candidats, à chaque session, le calcul de la moyenne pour l'admission s'effectue sur la base des notes conservées et des notes obtenues aux épreuves nouvellement subies " ; que les dispositions de l'article D. 336-13 du même code, dans leur rédaction issue de l'article 8 du même décret, introduisent des règles identiques pour les candidats à l'examen du baccalauréat technologique ;<br/>
<br/>
              6. Considérant, d'une part, qu'en introduisant les dispositions citées ci-dessus, le décret attaqué du 26 octobre 2015 étend à l'ensemble des candidats à l'examen des baccalauréats général et technologique la faculté de report, dans la limite de cinq sessions dans la même série, des notes égales ou supérieures à 10 obtenues lors d'une épreuve du premier groupe, faculté qui était jusque-là réservée à certaines catégories particulières de candidats (candidats non scolarisés, salariés, stagiaires de la formation professionnelle continue, demandeurs d'emplois, sportifs de haut niveau, ou enfin scolarisés à l'école de danse de l'Opéra national de Paris) ; que, d'autre part, les articles 6 et 7 du décret contesté, en ne reprenant pas les derniers alinéas des articles D. 334-13 et D. 336-13 du code de l'éducation dans leur rédaction antérieure, aux termes desquels "Aucune mention ne peut être attribuée aux candidats qui ont demandé à conserver le bénéfice de notes (...) ", et l'article 7 du même décret, en supprimant à l'article D. 336-11 la référence à ces dispositions, permettent que des mentions soient désormais attribuées, dans les conditions rappelées au point 4, aux candidats ayant obtenu leur diplôme de baccalauréat en opérant un tel report de notes obtenues au cours de sessions antérieures ; que, toutefois, le décret maintient inchangées les dispositions des articles D. 334-8 et D. 336-8 du code de l'éducation en vertu desquelles les candidats qui obtiennent leur baccalauréat général ou technologique en une seule session, mais après avoir dû se présenter au second groupe d'épreuves (" oraux de rattrapage "), ne peuvent prétendre à aucune mention ; <br/>
<br/>
              7. Considérant qu'en permettant ainsi d'attribuer des mentions à des candidats qui ont obtenu leur diplôme de baccalauréat au terme de plusieurs sessions dans la même série, alors que les candidats qui obtiennent leur baccalauréat en une seule session, mais après avoir dû se présenter au second groupe d'épreuves ne peuvent prétendre à aucune mention, le décret attaqué introduit, entre ces deux catégories de candidats au baccalauréat, une différence de traitement qui n'est pas justifiée par une différence de situation en rapport avec l'objet de cette réglementation ; que si la faculté d'attribuer une mention aux candidats qui obtiennent leur baccalauréat en plusieurs sessions peut être justifiée par le motif d'intérêt général de lutte contre le " décrochage scolaire ", un tel motif n'est pas, contrairement à ce que soutient la ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche, de nature à justifier cette différence de traitement dans l'attribution des mentions ; que, par suite, l'association requérante est fondée à soutenir que ces dispositions méconnaissent le principe d'égalité ; <br/>
<br/>
              8. Considérant qu'il résulte de tout ce qui précède, sans qu'il soit besoin d'examiner l'autre moyen de la requête dirigé contre le décret en tant qu'il porte sur l'attribution des mentions, que l'association SOS Education, est seulement fondée à demander l'annulation de l'article 7 du décret attaqué ainsi que de ses articles 6 et 8 en tant qu'ils  ne reprennent pas les derniers alinéas des articles D. 334-13 et D. 336-13 du code de l'éducation dans leurs rédactions antérieures à ce décret ;<br/>
<br/>
              9. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros que demande l'association SOS Education au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : L'article 7 du décret du 26 octobre 2015 et ses articles 6 et 8 en tant qu'ils ne reprennent pas les derniers alinéas des articles D. 334-13 et D. 336-13 du code de l'éducation dans leurs rédactions antérieures à ce décret, sont annulés.<br/>
Article 2 : L'Etat versera à l'association SOS Education une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 3 : le surplus des conclusions de la requête de l'association SOS Education est rejeté.<br/>
Article 4 : La présente décision sera notifiée à l'association SOS Education, au Premier ministre et à la ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-04-03-01 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. PRINCIPES GÉNÉRAUX DU DROIT. ÉGALITÉ DEVANT LA LOI. - BACCALAURÉAT - MENTION ATTRIBUÉE AUX CANDIDATS AYANT OBTENU LEUR BACCALAURÉAT AU TERME DE PLUSIEURS SESSIONS DANS LA MÊME SÉRIE EN CONSERVANT LE BÉNÉFICE DE CERTAINES NOTES - DIFFÉRENCE DE TRAITEMENT AVEC LES CANDIDATS AYANT OBTENU LEUR DIPLÔME EN UNE SEULE SESSION APRÈS S'ÊTRE PRÉSENTÉS AUX ÉPREUVES DU SECOND GROUPE - 1) DIFFÉRENCE DE TRAITEMENT JUSTIFIÉE PAR UNE DIFFÉRENCE DE SITUATION EN RAPPORT AVEC L'OBJET DE LA RÉGLEMENTATION - ABSENCE - 2) DIFFÉRENCE DE TRAITEMENT JUSTIFIÉE PAR UN MOTIF D'INTÉRÊT GÉNÉRAL - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">30-01-04 ENSEIGNEMENT ET RECHERCHE. QUESTIONS GÉNÉRALES. EXAMENS ET CONCOURS. - BACCALAURÉAT - MENTION ATTRIBUÉE AUX CANDIDATS AYANT OBTENU LEUR BACCALAURÉAT AU TERME DE PLUSIEURS SESSIONS DANS LA MÊME SÉRIE EN CONSERVANT LE BÉNÉFICE DE CERTAINES NOTES - DIFFÉRENCE DE TRAITEMENT AVEC LES CANDIDATS AYANT OBTENU LEUR DIPLÔME EN UNE SEULE SESSION APRÈS S'ÊTRE PRÉSENTÉS AUX ÉPREUVES DU SECOND GROUPE - MÉCONNAISSANCE DU PRINCIPE D'ÉGALITÉ - EXISTENCE.
</SCT>
<ANA ID="9A"> 01-04-03-01 Décret n° 2015-1531 du 26 octobre 2015 étendant à l'ensemble des candidats à l'examen des baccalauréats général et technologique la faculté de report, dans la limite de cinq sessions dans la même série, des notes égales ou supérieures à 10 obtenues lors d'une épreuve du premier groupe, sans reprendre la disposition interdisant l'attribution de mentions aux candidats ayant demandé à conserver le bénéfice de leurs notes.... ,,1) Ce décret introduit, entre les candidats ayant obtenu leur baccalauréat au terme de plusieurs sessions dans la même série en conservant le bénéfice de certaines notes et les candidats ayant obtenu leur diplôme en une seule session après s'être présentés aux épreuves du second groupe (oraux de rattrapage) qui ne peuvent prétendre à aucune mention une différence de traitement qui n'est pas justifiée par une différence de situation en rapport avec l'objet de cette réglementation.... ,,2) Si la faculté d'attribuer une mention aux candidats qui obtiennent leur baccalauréat en plusieurs sessions peut être justifiée par le motif d'intérêt général de lutte contre le décrochage scolaire, un tel motif n'est pas de nature à justifier cette différence de traitement dans l'attribution des mentions.</ANA>
<ANA ID="9B"> 30-01-04 Décret n° 2015-1531 du 26 octobre 2015 étendant à l'ensemble des candidats à l'examen des baccalauréats général et technologique la faculté de report, dans la limite de cinq sessions dans la même série, des notes égales ou supérieures à 10 obtenues lors d'une épreuve du premier groupe, sans reprendre la disposition interdisant l'attribution de mentions aux candidats ayant demandé à conserver le bénéfice de leurs notes.... ,,Ce décret introduit, entre les candidats ayant obtenu leur baccalauréat au terme de plusieurs sessions dans la même série en conservant le bénéfice de certaines notes et les candidats ayant obtenu leur diplôme en une seule session après s'être présentés aux épreuves du second groupe (oraux de rattrapage) qui ne peuvent prétendre à aucune mention une différence de traitement qui n'est pas justifiée par une différence de situation en rapport avec l'objet de cette réglementation. Si la faculté d'attribuer une mention aux candidats qui obtiennent leur baccalauréat en plusieurs sessions peut être justifiée par le motif d'intérêt général de lutte contre le décrochage scolaire, un tel motif n'est pas de nature à justifier cette différence de traitement dans l'attribution des mentions.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
