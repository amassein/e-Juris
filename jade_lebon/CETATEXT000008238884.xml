<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000008238884</ID>
<ANCIEN_ID>JG_L_2006_11_000000273877</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/08/23/88/CETATEXT000008238884.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 4ème sous-sections réunies, 29/11/2006, 273877</TITRE>
<DATE_DEC>2006-11-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>273877</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 4ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>Mme Hagelsteen</PRESIDENT>
<AVOCATS>CARBONNIER ; LE PRADO</AVOCATS>
<RAPPORTEUR>M. Damien  Botteghi</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Chauvaux</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 4 novembre et 18 novembre 2004 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. Pierre A, demeurant ... ; M. A demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance du 20 octobre 2004 par laquelle le président de la cour administrative d'appel de Douai a rejeté sa requête tendant à l'annulation de l'ordonnance du 23 septembre 2004 du juge des référés du tribunal administratif d'Amiens ayant rejeté sa demande d'expertise aux fins de déterminer les causes du malaise cardiaque survenu au centre hospitalier général d'Abbeville après son admission le 9 avril 1992 et d'évaluer les éventuels préjudices subis ;<br/>
<br/>
              2°) statuant au fond, d'ordonner une expertise aux fins notamment de déterminer les conditions de son admission au centre hospitalier général d'Abbeville, de dire s'il a reçu les informations dont le centre hospitalier aurait dû être dépositaire sur la nature des soins prodigués, de dire si les diagnostics posés, les médications administrées et les soins dispensés l'ont été dans les règles de l'art, de déterminer les causes des malaises survenus, ainsi que les circonstances et les conditions de son admission dans le service de réanimation polyvalente du centre hospitalier universitaire d'Amiens, d'indiquer l'évolution de son état dans ce service, de dire si son état est consolidé, de déterminer les séquelles en relation avec les fautes commises, et d'évaluer l'ITT, l'IPT, l'IPP ainsi que les chefs de préjudice personnels ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Damien Botteghi, Auditeur,<br/>
<br/>
              - les observations de Me Carbonnier, avocat de M. A et de Me Le Prado, avocat du centre hospitalier général d'Abbeville, <br/>
<br/>
              - les conclusions de M. Didier Chauvaux, Commissaire du gouvernement ;<br/>
<br/>
<br/>
<br/>
              Considérant que, par un jugement avant-dire droit du 24 mars 1998, le tribunal administratif d'Amiens a, à la demande de M. A, ordonné une expertise aux fins de déterminer si le centre hospitalier général d'Abbeville pouvait être déclaré responsable de l'état de santé de l'intéressé que celui-ci impute aux conditions de son hospitalisation dans cet établissement en avril 1992 ; que, malgré plusieurs rappels et l'octroi d'un délai supplémentaire de trois mois, le requérant ne s'est jamais présenté aux convocations de l'expert ; que, par un jugement du 4 avril 2000 confirmé en appel par la cour administrative d'appel de Douai le 17 septembre 2002, les conclusions de M. A tendant à l'engagement de la responsabilité du centre hospitalier ont été rejetées ; que l'intéressé a saisi à nouveau le juge administratif d'une demande d'expertise aux mêmes fins que précédemment ; que, par ordonnance du 27 septembre 2004, le juge des référés du tribunal administratif d'Amiens a estimé que l'autorité relative de la chose jugée attachée aux décisions précitées du 4 avril 2000 et du 17 septembre 2002 rendait inutile toute nouvelle demande d'expertise ; que le président de la cour administrative de Douai, par une ordonnance en date du 20 octobre 2004 contre laquelle M. A se pourvoit en cassation, a confirmé cette ordonnance ; <br/>
<br/>
              Considérant qu'aux termes de l'article R. 532-1 du code de justice administrative : « Le juge des référés peut, sur simple requête et même en l'absence de décision administrative préalable, prescrire toute mesure utile d'expertise ou d'instruction (...) » ; qu'en vertu de l'article R. 611-8 du même code : « Lorsqu'il apparaît au vu de la requête que la solution de l'affaire est d'ores et déjà certaine, le président du tribunal administratif ou le président de la formation de jugement ou, à la cour administrative d'appel, le président de la chambre ou, au Conseil d'Etat, le président de la sous-section peut décider qu'il n'y a pas lieu à instruction. » ;<br/>
<br/>
              Considérant, en premier lieu, que le moyen tiré de ce que l'ordonnance attaquée, qui est suffisamment motivée et répond à l'ensemble des conclusions du requérant, serait irrégulière faute de comporter la signature du président de la cour administrative d'appel de Douai, manque en fait ;  <br/>
<br/>
              Considérant, en second lieu, qu'en rejetant par son arrêt du 17 septembre 2002 les conclusions du requérant tendant à ce que le centre hospitalier général d'Abbeville soit déclaré responsable des préjudices qu'il estime avoir subis du fait des fautes commises lors de son hospitalisation, au motif que l'intéressé, en raison de sa carence lors de plusieurs convocations fixées en vue de l'expertise médicale ordonnée à sa demande par le tribunal administratif, n'apportait pas d'élément permettant de statuer, la cour administrative d'appel de Douai a statué au fond ; que des demandes d'indemnisation des préjudices causés par un même événement relèvent d'une même cause juridique si elles sont fondées sur une faute que l'administration aurait commise ; qu'ainsi, l'autorité relative de la chose jugée s'opposait à ce que M. A pût introduire une nouvelle action en responsabilité à l'encontre de l'établissement public en vue d'obtenir la réparation des mêmes préjudices résultant de son hospitalisation, même en invoquant un défaut d'information sur les risques encourus, dès lors que celui-ci se rattache à une faute de l'établissement ; que, par suite, le président de la cour administrative d'appel de Douai n'a pas commis d'erreur de droit en jugeant que la demande d'expertise sollicitée par M. A ne présentait pas le caractère d'utilité exigée par l'article R. 532-1 du code de justice administrative, et en faisant, par conséquence, application des dispositions de l'article R. 611-8 du même code ; <br/>
<br/>
              Considérant qu'il résulte de ce qui précède que la requête de M. A tendant à l'annulation de l'ordonnance du 20 octobre 2004 du président de la cour administrative d'appel de Douai doit être rejetée ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
		Article 1er : La requête de M. A est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. Pierre A, au directeur du centre hospitalier général d'Abbeville et au ministre de la santé et des solidarités.<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-06-06-01 PROCÉDURE. JUGEMENTS. CHOSE JUGÉE. CHOSE JUGÉE PAR LA JURIDICTION ADMINISTRATIVE. - AUTORITÉ RELATIVE DE LA CHOSE JUGÉE - REJET DÉFINITIF AU FOND D'UNE DEMANDE D'INDEMNISATION D'UN PRÉJUDICE SUR LE TERRAIN DE LA FAUTE FAISANT OBSTACLE À LA PRÉSENTATION D'UNE NOUVELLE DEMANDE D'INDEMNISATION DU MÊME PRÉJUDICE PAR L'INVOCATION D'UNE AUTRE FAUTE DE LA PERSONNE PUBLIQUE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">60-01-02-02 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. FAITS SUSCEPTIBLES OU NON D'OUVRIR UNE ACTION EN RESPONSABILITÉ. FONDEMENT DE LA RESPONSABILITÉ. RESPONSABILITÉ POUR FAUTE. - DEMANDES INDEMNITAIRES CONCERNANT UN MÊME PRÉJUDICE ET METTANT EN CAUSE DES FAUTES DE L'ADMINISTRATION - CAUSE JURIDIQUE UNIQUE - CONSÉQUENCE - AUTORITÉ RELATIVE DE LA CHOSE JUGÉE PAR LE REJET DÉFINITIF AU FOND D'UNE DEMANDE D'INDEMNISATION D'UN PRÉJUDICE SUR LE TERRAIN DE LA FAUTE FAISANT OBSTACLE À LA PRÉSENTATION D'UNE NOUVELLE DEMANDE D'INDEMNISATION DU MÊME PRÉJUDICE PAR L'INVOCATION D'UNE AUTRE FAUTE DE LA PERSONNE PUBLIQUE [RJ1].
</SCT>
<ANA ID="9A"> 54-06-06-01 Des demandes d'indemnisation des préjudices causés par un même événement relèvent d'une même cause juridique si elles sont fondées sur une faute que l'administration aurait commise. Ainsi, l'autorité relative de la chose jugée attachée à un jugement rejetant définitivement au fond une demande indemnitaire présentée sur le terrain de la responsabilité pour faute d'une personne publique s'oppose à ce que le requérant puisse introduire une nouvelle action en responsabilité à l'encontre de cette personne publique en vue d'obtenir la réparation des mêmes préjudices dès lors qu'il invoque une faute de cette personne.</ANA>
<ANA ID="9B"> 60-01-02-02 Des demandes d'indemnisation des préjudices causés par un même événement relèvent d'une même cause juridique si elles sont fondées sur une faute que l'administration aurait commise. Ainsi, l'autorité relative de la chose jugée attachée à un jugement rejetant définitivement au fond une demande indemnitaire présentée sur le terrain de la responsabilité pour faute d'une personne publique s'oppose à ce que le requérant puisse introduire une nouvelle action en responsabilité à l'encontre de cette personne publique en vue d'obtenir la réparation des mêmes préjudices dès lors qu'il invoque une faute de cette personne.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr. 16 février 1979, Mallisson, T. p. 652 ; 1er octobre 2004, Association départementale d'éducation et de prévention spécialisée (ADEPS), T. p. 850.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
