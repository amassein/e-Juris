<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036157810</ID>
<ANCIEN_ID>JG_L_2017_12_000000397363</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/15/78/CETATEXT000036157810.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 1ère chambres réunies, 06/12/2017, 397363</TITRE>
<DATE_DEC>2017-12-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>397363</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Mireille Le Corre</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Louis Dutheillet de Lamothe</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:397363.20171206</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, un mémoire en réplique et un nouveau mémoire, enregistrés les 26 février 2016, 10 mars 2017 et 7 novembre 2017 au secrétariat du contentieux du Conseil d'Etat, Mme C...B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'avis non conforme émis par le Conseil supérieur de la magistrature le 25 novembre 2015 sur sa nomination en tant que président de la chambre de l'instruction de la cour d'appel de Nouméa ;<br/>
<br/>
              2°) d'enjoindre à l'Etat, à titre principal, de procéder à sa nomination en qualité de président de la chambre de l'instruction de la cour d'appel de Nouméa et, à titre subsidiaire, d'enjoindre au garde des sceaux, ministre de la justice, de procéder au réexamen de la proposition de nomination la concernant ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative, ainsi que les entiers dépens.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - l'ordonnance n° 58-1270 portant loi organique du 22 décembre 1958 ;<br/>
              - la loi organique n° 94-100 du 5 février 1994 ; <br/>
              - la loi n° 79-587 du 11 juillet 1979 ;<br/>
              - le décret n° 94-199 du 9 mars 1994 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Mireille Le Corre, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Louis Dutheillet de Lamothe, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier que MmeB..., magistrate du siège, s'est portée candidate, en vue des affectations de magistrats devant intervenir en janvier 2016, au poste de président de la chambre de l'instruction de la cour d'appel de Nouméa ; que le garde des sceaux, ministre de la justice a proposé sa nomination sur ce poste au Conseil supérieur de la magistrature ; que le Conseil supérieur de la magistrature a toutefois émis, le 25 novembre 2015, un avis défavorable à sa nomination sur ce poste, fondé sur le motif tiré de ce que " l'un des observants présente un dossier de meilleure qualité " ; que Mme B...demande l'annulation pour excès de pouvoir de cet avis ;<br/>
<br/>
              Sur la légalité externe de l'avis attaqué :<br/>
<br/>
              2. Considérant, en premier lieu, qu'aux termes de l'article 27-1 de l'ordonnance du 22 décembre 1958 portant loi organique relative au statut de la magistrature : " Le projet de nomination à une fonction du premier ou du second grade et la liste des candidats à cette fonction sont communiqués pour les postes du siège ou pour ceux du parquet à la formation compétente du Conseil supérieur de la magistrature. / Ce projet de nomination est adressé aux chefs de la Cour de cassation, aux chefs des cours d'appel et des tribunaux supérieurs d'appel, à l'inspecteur général, chef de l'inspection générale de la justice, ainsi qu'aux directeurs et chefs de service de l'administration centrale du ministère de la justice, qui en assurent la diffusion auprès des magistrats en activité dans leur juridiction, dans le ressort de leur juridiction ou de leurs services. Ce document est adressé aux syndicats représentatifs de magistrats et, sur leur demande, aux magistrats placés dans une autre position que celle de l'activité. / Toute observation d'un candidat relative à un projet de nomination est adressée au garde des sceaux, ministre de la justice, et au Conseil supérieur de la magistrature. (...) " ; que l'article 13 de la loi organique du 5 février 1994 sur le Conseil supérieur de la magistrature dispose que chacune des formations du Conseil supérieur de la magistrature se réunit sur convocation de son président ; qu'aux termes de l'article 17 de la même loi organique : " Les propositions du ministre de la justice sont transmises au Conseil supérieur avec la liste des candidats pour chacun des postes concernés. / Le rapporteur a accès au dossier des magistrats candidats. Il peut demander au ministre de la justice toutes précisions utiles. Ces précisions et les observations éventuelles du magistrat intéressé sont versées dans le dossier de ce dernier. / Sur proposition du rapporteur, le Conseil supérieur peut remettre au ministre de la justice les observations qu'il estime utiles sur le contenu du dossier examiné. / (...) " ; qu'aux termes de l'article 35 du décret du 9 mars 1994 relatif au Conseil supérieur de la magistrature : " L'ordre du jour des séances est arrêté par le président de chaque formation et communiqué au ministre de la justice. / L'ordre du jour des séances au cours desquelles sont examinées les propositions de nominations formulées par le ministre de la justice est arrêté huit jours avant la date à laquelle elles se tiennent. Le président de chaque formation inscrit à l'ordre du jour de chacune de ces séances les propositions de nomination transmises à cette fin par le ministre de la justice. Le président peut, à la demande du ministre de la justice, retirer de l'ordre du jour une ou plusieurs de ses propositions. / Une copie de l'ordre du jour est annexée à la convocation adressée aux membres du conseil supérieur " ; que l'article 36 du même décret dispose que : " Chaque formation du conseil supérieur peut, pour préparer ses travaux, se réunir à l'initiative de son président qui peut inviter le directeur des services judiciaires ou toutes personnes dont la présence lui paraît nécessaire à assister à ces réunions " ; que l'article 38 du même décret dispose que : " Pour les nominations sur proposition du garde des sceaux, ministre de la justice, le rapporteur de la formation compétente du conseil supérieur prend connaissance au ministère de la justice des dossiers des magistrats figurant sur la liste mentionnée au premier alinéa de l'article 17 de la loi organique du 5 février 1994 susvisée. (....) " ; qu'aux termes de l'article 38-1 du même décret : " Dans les cas visés aux articles 37 et 38, les dossiers des magistrats, lorsqu'ils sont disponibles sous forme dématérialisée, sont consultés par les membres du conseil supérieur par voie électronique " ; qu'enfin, aux termes de l'article 41 du même décret : " L'ordre du jour des séances du conseil supérieur est arrêté par le président de chaque formation et est communiqué au ministre de la justice. / Le texte de l'ordre du jour est également annexé à la convocation adressée aux membres du conseil " ;<br/>
<br/>
              3. Considérant qu'il résulte de ces dispositions que l'objet de la règle prévue à l'article 35 du décret du 9 mars 1994 imposant que l'ordre du jour des séances du Conseil supérieur de la magistrature soit arrêté huit jours avant qu'elles ne se tiennent est de donner la faculté au garde des sceaux, ministre de la justice, de demander au président de la formation concernée de retirer de l'ordre du jour une ou plusieurs des propositions soumises au Conseil supérieur de la magistrature ; qu'il est, par suite, loisible au garde des sceaux, ministre de la justice, de renoncer unilatéralement à ce délai ; qu'il ressort du procès-verbal de la séance du Conseil supérieur de la magistrature au cours de laquelle a été examinée la proposition de nomination de MmeB..., que le représentant du garde des sceaux, ministre de la justice, a indiqué renoncer à ce délai de huit jours ; qu'il résulte, par ailleurs, des dispositions rappelées au point précédent que les membres du Conseil supérieur de la magistrature ont connaissance des projets de nomination des magistrats à compter de la diffusion du document mentionné à l'article 27-1 de l'ordonnance organique, dit circulaire de " transparence " ; qu'ils ont également connaissance des observations émises à la suite de cette diffusion ; que les propositions du garde des sceaux, ministre de la justice, sont examinées par les membres du Conseil supérieur de la magistrature et le rapporteur désigné, selon les modalités définies aux articles 36 et suivants du décret du 9 mars 1994, en lien avec les services de la chancellerie, en amont de la réunion au cours de laquelle il est formellement statué sur les propositions de nominations formulées par le garde des sceaux, ministre de la justice ; qu'au cours de cette phase préparatoire, les membres du Conseil supérieur de la magistrature sont ainsi mis à même de prendre connaissance des dossiers des intéressés et des observations éventuellement formulées par d'autres candidats, en sollicitant les compléments d'information qui leur paraîtraient nécessaires ; qu'en l'espèce, le projet de nomination concernant Mme B...a ainsi été diffusé le 5 novembre 2015 ; que, par suite, la seule circonstance que l'ordre du jour, qui a été adressé avec leur convocation aux membres du Conseil supérieur de la magistrature, n'ait été arrêté que deux jours avant la séance n'est pas de nature à entacher l'avis attaqué d'irrégularité ; <br/>
<br/>
              4. Considérant, en deuxième lieu, qu'aux termes de l'article 14 de la loi organique du 5 février 1994 : " En cas d'empêchement, le premier président de la Cour de cassation et le procureur général près ladite cour peuvent être suppléés respectivement par le magistrat visé au 1° de l'article 1er et par le magistrat visé au 1° de l article 2. / Pour délibérer valablement lorsqu'elles siègent en matière disciplinaire, la formation compétente à l'égard des magistrats du siège et celle compétente à l'égard des magistrats du parquet comprennent, outre le président de séance, au moins sept de leurs membres. Dans les autres matières, chaque formation du Conseil supérieur délibère valablement si elle comprend, outre le président de séance, au moins huit de ses membres. / Les propositions et avis de chacune des formations du Conseil supérieur sont formulés à la majorité des voix " ; que, contrairement à ce qui est soutenu, il ressort des pièces du dossier et notamment du procès-verbal de la séance du 25 novembre 2015 que le quorum a été atteint et que l'avis a été adopté à la majorité, ainsi que l'exigeaient les dispositions rappelées ci-dessus ; <br/>
<br/>
              5. Considérant, en troisième lieu, que l'avis défavorable donné par le Conseil supérieur de la magistrature à une proposition de nomination d'un magistrat du siège n'est pas au nombre des décisions individuelles refusant à l'intéressé un avantage auquel il a droit qui, en application de l'article 1er de la loi du 11 juillet 1979 relative à la motivation des actes administratifs et à l'amélioration des relations entre l'administration et le public, alors en vigueur, doivent être motivées ; qu'au demeurant, ainsi qu'il a été dit au point 1, la décision litigieuse énonce le motif ayant conduit le Conseil supérieur de la magistrature à émettre un avis défavorable à la proposition de nomination de Mme B...; <br/>
<br/>
              Sur la légalité interne de l'avis attaqué : <br/>
<br/>
              6. Considérant qu'aux termes de l'article 28 de l'ordonnance du 22 décembre 1958, dans sa rédaction applicable au litige : " Les décrets de nomination aux fonctions de président d'un tribunal de grande instance ou d'un tribunal de première instance ou de conseiller référendaire à la Cour de cassation sont pris par le Président de la République sur proposition de la formation compétente du Conseil supérieur de la magistrature. / Les décrets portant promotion de grade ou nomination aux fonctions de magistrat autres que celles mentionnées à l'alinéa précédent sont pris par le Président de la République sur proposition du garde des sceaux, ministre de la justice, après avis conforme de la formation compétente du Conseil supérieur de la magistrature pour ce qui concerne les magistrats du siège et après avis de la formation compétente du Conseil supérieur pour ce qui concerne les magistrats du parquet. (...) " ; <br/>
<br/>
              7. Considérant qu'il résulte des dispositions combinées des articles 27-1 et 28 de l'ordonnance du 22 décembre 1958 et de l'article 17 de la loi organique du 5 février 1994, citées aux points 2 et 6, que le garde des sceaux, ministre de la justice, transmet au Conseil supérieur de la magistrature sa proposition de nomination d'un magistrat du siège sur un poste déterminé ainsi que la liste des autres magistrats s'étant portés candidats sur le même poste et leurs observations éventuelles ; que le Conseil supérieur de la magistrature peut, dans l'appréciation qu'il porte sur cette proposition, au vu du dossier du candidat proposé, et compte tenu, le cas échéant, des observations formulées par d'autres candidats, émettre un avis défavorable s'il lui apparaît soit que la candidature proposée est inadéquate au regard des aptitudes de l'intéressé, des exigences déontologiques, des besoins de l'institution judiciaire et des caractéristiques du poste concerné, soit qu'une autre candidature est plus adéquate au regard de ces critères ;<br/>
<br/>
              8. Considérant qu'il ressort des éléments versés au dossier par le secrétaire général du Conseil supérieur de la magistrature, à la suite du supplément d'instruction réalisé par la 6ème chambre de la section du contentieux du Conseil d'Etat, qui éclairent les motifs pour lesquels la nomination de Mme B...au poste qu'elle sollicitait a donné lieu à un avis défavorable, que le Conseil supérieur de la magistrature a estimé que M.A..., qui s'était porté candidat en même temps que l'intéressée au poste de président de la chambre de l'instruction de la cour d'appel de Nouméa et avait présenté des observations, disposait d'un dossier de meilleure qualité pour ce poste ; qu'en décidant, au vu notamment des évaluations et des parcours professionnels de ces deux magistrats, d'émettre un avis défavorable à la nomination de Mme B..., le Conseil supérieur de la magistrature n'a pas porté une appréciation manifestement erronée au regard des profils de ces candidats ;<br/>
<br/>
              9. Considérant qu'il résulte de tout ce qui précède que Mme B...n'est pas fondée à demander l'annulation de l'avis qu'elle attaque ; que ses conclusions aux fins d'injonction doivent, en conséquence, être rejetées ;<br/>
<br/>
              10. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise, à ce titre, à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de Mme B...est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à Mme C...B...et au garde des sceaux, ministre de la justice.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">37-04-02-005 JURIDICTIONS ADMINISTRATIVES ET JUDICIAIRES. MAGISTRATS ET AUXILIAIRES DE LA JUSTICE. MAGISTRATS DE L'ORDRE JUDICIAIRE. NOMINATION. - NOMINATION D'UN MAGISTRAT DU SIÈGE À UNE FONCTION DU PREMIER OU DU SECOND GRADE (ART. 27-1 DE L'ORDONNANCE DU 22 DÉCEMBRE 1958) - AVIS DU CSM SUR LA PROPOSITION DU GARDE DES SCEAUX - 1) POSSIBILITÉ POUR LE CSM DE PRENDRE EN COMPTE LES CANDIDATURES AUTRES QUE CELLE PROPOSÉE - EXISTENCE - 2) CONTRÔLE DU CONSEIL D'ETAT SUR CET AVIS - CONTRÔLE RESTREINT [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-07-02-04 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. CONTRÔLE DU JUGE DE L'EXCÈS DE POUVOIR. APPRÉCIATIONS SOUMISES À UN CONTRÔLE RESTREINT. - AVIS DU CSM SUR LA NOMINATION D'UN MAGISTRAT DU SIÈGE À UNE FONCTION DU PREMIER OU DU SECOND GRADE (ART. 27-1 DE L'ORDONNANCE DU 22 DÉCEMBRE 1958) [RJ1].
</SCT>
<ANA ID="9A"> 37-04-02-005 1) Il résulte des articles 27-1 et 28 de l'ordonnance n° 58-1270 du 22 décembre 1958 portant loi organique du 22 décembre 1958 et de l'article 17 de la loi organique n° 94-100 du 5 février 1994 sur le Conseil supérieur de la magistrature (CSM), que le garde des sceaux, ministre de la justice, transmet au CSM sa proposition de nomination d'un magistrat du siège sur un poste déterminé ainsi que la liste des autres magistrats s'étant portés candidats sur le même poste et leurs observations éventuelles. Le CSM peut, dans l'appréciation qu'il porte sur cette proposition, au vu du dossier du candidat proposé, et compte tenu, le cas échéant, des observations formulées par d'autres candidats, émettre un avis défavorable s'il lui apparaît soit que la candidature proposée est inadéquate au regard des aptitudes de l'intéressé, des exigences déontologiques, des besoins de l'institution judiciaire et des caractéristiques du poste concerné, soit qu'une autre candidature est plus adéquate au regard de ces critères.,,,2) Le Conseil d'Etat exerce sur cet avis un contrôle restreint à l'erreur manifeste d'appréciation.</ANA>
<ANA ID="9B"> 54-07-02-04 Le Conseil d'Etat exerce un contrôle restreint à l'erreur manifeste d'appréciation sur l'avis que rend le Conseil supérieur de la magistrature (CSM) sur la candidature d'un magistrat du siège à une fonction du premier ou second grade proposée par le garde des sceaux en application des articles 27-1 et 28 de l'ordonnance n° 58-1270 du 22 décembre 1958 portant loi organique du 22 décembre 1958 et de l'article 17 de la loi organique n° 94-100 du 5 février 1994 sur le Conseil supérieur de la magistrature.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., s'agissant du contrôle exercé par le Conseil d'Etat sur l'avis du CSM rendu sur la candidature d'un magistrat placé se prévalant du bénéfice de l'article 3-1 de l'ordonnance du 22 décembre 1958, CE, 8 juin 2016, Assemblée,,n°s 382736, 386701, p. 236.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
