<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025469081</ID>
<ANCIEN_ID>JG_L_2012_03_000000355009</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/46/90/CETATEXT000025469081.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème et 3ème sous-sections réunies, 07/03/2012, 355009</TITRE>
<DATE_DEC>2012-03-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>355009</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème et 3ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Christian Vigouroux</PRESIDENT>
<AVOCATS>SCP DELAPORTE, BRIARD, TRICHET</AVOCATS>
<RAPPORTEUR>Mme Paquita Morellet-Steiner</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Nathalie Escaut</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:355009.20120307</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi, enregistré le 19 décembre 2011 au secrétariat du contentieux du Conseil d'Etat, présenté pour M. Antoine A demeurant ... ; M. A demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'ordonnance n° 1103375 du 16 décembre 2011 par laquelle le juge des référés du tribunal administratif de Toulon, statuant sur le fondement de l'article L. 522-3 du code de justice administrative, a rejeté sa demande présentée sur le fondement de l'article L. 521-2 du même code et tendant à ce qu'il enjoigne au préfet du Var de faire cesser les opérations de destruction des installations de l'établissement " La voile rouge " situé sur la plage de Pampelonne, de lui restituer le mobilier qu'il a fait retirer et placer dans un garde-meuble et à ce que soit mise à la charge de l'Etat la somme de 5 000 euros en application des dispositions de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
              2°) statuant en référé, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros, en application de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution, notamment son Préambule et son article 61-1 ;<br/>
<br/>
              Vu l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
<br/>
              Vu le code général de la propriété des personnes publiques, notamment son article L. 2132-3 ; <br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Paquita Morellet-Steiner, Maître des Requêtes,<br/>
<br/>
              - les observations de la SCP Delaporte, Briard et Trichet, avocat de M. A,<br/>
<br/>
              - les conclusions de Mme Paquita Morellet-Steiner, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Delaporte, Briard et Trichet, avocat de M. A ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant, en premier lieu, que, par ordonnance du 16 décembre 2011, le juge des référés du tribunal administratif de Toulon a, d'une part, refusé de transmettre au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution des dispositions du premier alinéa de l'article L. 2132-3 du code général de la propriété des personnes publiques qui lui était soumise par M. A et, d'autre part, a, par la même ordonnance, rejeté la demande qu'il lui avait présentée sur le fondement de l'article L. 521-2 du code de justice administrative ; que M. A conteste cette ordonnance par un pourvoi en cassation formé devant le Conseil d'Etat le 19 décembre 2011 ; qu'en second lieu, il demande au Conseil d'Etat, par un mémoire intitulé " question prioritaire de constitutionnalité " et de nouveaux mémoires enregistrés respectivement le 19 décembre 2011 et les 19 janvier et 3 février 2012, de transmettre au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des dispositions du premier alinéa de l'article L. 2132-3 du code général de la propriété des personnes publiques; <br/>
<br/>
              Sur le mémoire intitulé " question prioritaire de constitutionnalité " :<br/>
<br/>
              Considérant, en premier lieu, qu'à l'appui du mémoire intitulé " question prioritaire de constitutionnalité ", enregistré le 19 décembre 2011, par lequel M. A entend seulement, sur le fondement de l'article L. 23-5 de l'ordonnance du 7 novembre 1958, soumettre au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution des dispositions du premier alinéa de l'article L. 2132-3 du code général de la propriété des personnes publiques et qui ne constitue pas une contestation du refus de transmission par le juge des référés de cette question, M. A soutient que ces dispositions méconnaissent le droit de propriété garanti par les articles 2 et 17 de la Déclaration des droits de l'homme et du citoyen de 1789, dès lors que les atteintes au droit de propriété qu'elles autorisent, sous la forme d'expulsion ou de destruction de constructions régulièrement autorisées et sur lesquelles l'occupant possède des droits réels, voire de confiscation des biens, ne répondent pas à la double condition d'être justifiées par une nécessité publique et d'être assorties d'une juste et préalable indemnité ; que ces mêmes dispositions en ce qu'elles sont source d'insécurité juridique pour les occupants dont les constructions ont été régulièrement autorisées portent atteinte à la garantie des droits proclamée par l'article 16 de la Déclaration des droits de l'homme et du citoyen de 1789 ; qu'il ne peut être fait droit à cette demande dès lors que la question prioritaire de constitutionnalité ainsi posée invoque les mêmes questions, par les mêmes moyens, que celle soumise au juge des référés du tribunal administratif de Toulon ;  <br/>
<br/>
              Considérant, en second lieu, que M. A a soulevé dans ses mémoires ultérieurs des moyens de constitutionnalité différents de ceux présentés dans le mémoire du 19 décembre 2011 ;<br/>
<br/>
              Considérant qu'il résulte des dispositions de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que celui-ci est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              Considérant qu'aux termes du premier alinéa de l'article L. 2132-3 du code général de la propriété des personnes publiques : " Nul ne peut bâtir sur le domaine public maritime ou y réaliser quelque aménagement ou quelque ouvrage que ce soit sous peine de leur démolition, de confiscation des matériaux et d'amende." ; <br/>
<br/>
              Considérant, d'une part, que M. A soutient qu'en ne prévoyant aucune mesure d'information ou de notification au propriétaire des biens de la date de leur démolition ou de leur confiscation, le législateur a méconnu le droit à un recours effectif et au respect du principe du contradictoire garantis par l'article 16 de la Déclaration des droits de l'homme et du citoyen ainsi que l'article 34 de la Constitution ; <br/>
<br/>
              Considérant que les dispositions contestées définissent les infractions propres au domaine public maritime naturel dont la constatation justifie que les autorités chargées de la conservation de ce domaine engagent, après avoir cité le contrevenant à comparaître, des poursuites conformément à la procédure de contravention de grande voirie prévue par les articles L. 774-1 à L. 774-13 du code de justice administrative ; que, dans le cadre de cette procédure, le contrevenant peut être condamné par le juge, au titre de l'action publique, à une sanction pénale consistant en une amende ainsi que, au titre de l'action domaniale, et à la demande de l'administration, à remettre lui-même les lieux en état en procédant à la destruction des ouvrages construits ou maintenus illégalement sur la dépendance domaniale ou à l'enlèvement des installations ; que si le contrevenant n'exécute pas les travaux dans le délai prévu par le jugement ou l'arrêt, l'administration peut y faire procéder d'office, si la loi le prévoit ou si le juge l'a autorisée à le faire ; que ces dispositions, qui font dépendre l'exécution des mesures de remise en l'état du domaine de l'accomplissement régulier d'une procédure juridictionnelle préalable, assurent, non seulement pour le contrevenant mais aussi pour tous occupants de son fait y compris le cas échéant pour le propriétaire, la garantie de leur droit à un recours effectif et l'information sur les obligations leur incombant ainsi que des pouvoirs d'exécution dont dispose l'administration ; que, par suite, M. A n'est pas fondé à soutenir que les dispositions contestées sont contraires à l'article 16 de la Déclaration des droits de l'homme et du citoyen de 1789 et que le législateur aurait méconnu l'étendue de sa compétence ; <br/>
<br/>
              Considérant, d'autre part, que M. A expose que le préfet du Var a fait préalablement enlever, pour les déposer dans un garde-meuble, les mobiliers qui se trouvaient à l'intérieur des installations dont il a demandé la démolition en exécution du jugement du 25 mars 2011 du tribunal administratif de Toulon ordonnant la libération des dépendances du domaine public maritime occupées sans titre ainsi que, le cas échéant d'office, la démolition des installations implantées irrégulièrement sur ces dépendances et alors que le contrevenant n'avait pas procédé de lui-même à la remise en état dans le délai imparti ; qu'il soutient qu'en ne prévoyant aucune possibilité de revendiquer ces biens qui auraient été ainsi confisqués en application des dispositions précitées de l'article L. 2132-3 du code général de la propriété des personnes publiques, le législateur a méconnu le droit de propriété garanti par les articles 2 et 17 de la Déclaration des droits de l'homme et du citoyen de 1789 ainsi que l'article 34 de la Constitution ; <br/>
<br/>
              Considérant que les dispositions contestées qui autorisent, en vue de prévenir ou d'empêcher la construction illégale d'ouvrages ou d'aménagements sur le domaine public maritime, les autorités chargées de la conservation de ce domaine à confisquer des matériaux, n'ont pour objet ni de permettre la saisie d'objets tels du petit mobilier de plage ou du matériel de baignade, ni d'interdire à leur propriétaire d'en obtenir restitution quand ils ont été déplacés pour permettre la démolition de l'installation irrégulière ; que, par suite, M. A n'est pas fondé à soutenir que les dispositions contestées seraient contraires aux articles 2 et 17 de la Déclaration des droits de l'homme et du citoyen de 1789 et que le législateur aurait méconnu l'étendue de sa compétence ; <br/>
<br/>
              Considérant, enfin, que M. A soutient qu'en permettant qu'une personne étrangère à l'infraction supporte, en voyant ses biens détruits ou confisqués, les conséquences des mesures prononcées à l'encontre d'une autre personne, les dispositions de l'article L. 2132-3 du code général des propriétés des personnes publiques méconnaissent le principe de personnalité des peines protégé par les articles 8 et 9 de la Déclaration des droits de l'homme et du citoyen de 1789 ainsi que l'article 34 de la Constitution ; que, toutefois, ce principe qui ne trouve à s'appliquer qu'en matière répressive ne peut être invoqué pour contester les mesures prises au titre de l'action domaniale et par lesquelles, les autorités publiques chargées de la protection du domaine public naturel assurent sa remise en l'état, dès lors qu'une telle action ne présente pas de caractère répressif ; que, par suite, le législateur n'a pas méconnu l'étendue de sa compétence en ne prévoyant pas d'encadrer l'exécution des décisions prises en application de cet article ; <br/>
<br/>
              Considérant qu'il résulte de ce qui précède que la question soulevée, qui n'est pas nouvelle, ne présente pas un caractère sérieux ; qu'il n'y a pas lieu en conséquence de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée ; <br/>
<br/>
              Sur le pourvoi en cassation :<br/>
<br/>
              Considérant qu'aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux " ;<br/>
<br/>
<br/>
              Considérant, que, pour demander l'annulation de l'ordonnance qu'il attaque, par laquelle le juge des référés du tribunal administratif de Toulon a refusé de transmettre au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution des dispositions du premier alinéa de l'article L. 2132-3 du code général de la propriété des personnes publiques, M. A soutient, par son mémoire enregistré le 3 février 2012, que la question de constitutionnalité invoquée devant le juge des référés portait sur des dispositions méconnaissant le droit de propriété garanti par les articles 2 et 17 de la Déclaration des droits de l'homme et du citoyen de 1789 ainsi que la garantie des droits proclamée par l'article 16 de la même Déclaration ; que M. A soutient en outre que le juge des référés, en refusant d'enjoindre d'interrompre les opérations de destruction de son installation a dénaturé les pièces du dossier et insuffisamment motivé son ordonnance en se fondant, en application de l'article L.521-2 du code de justice administrative, sur la circonstance que les opérations de démolition avaient un caractère " définitif " pour juger que la condition d'urgence n'était pas satisfaite, alors que la démolition du bâtiment principal n'était que partielle et que les travaux étaient encore en cours au moment où il a statué ; qu'il a dénaturé les faits en estimant, pour écarter l'existence du fait de ces opérations d'une atteinte grave et manifestement illégale à la liberté d'entreprendre, que l'établissement avait été exploité au cours de l'été 2011 par la SARL Tom Tea et non par lui-même, alors qu'il ressortait des pièces du dossier qu'il était l'unique associé et gérant de cette société constituée sous forme d'entreprise unipersonnelle à responsabilité limitée ; qu'il a commis une erreur de droit en jugeant que la mise en garde-meubles des objets mobiliers de l'établissement ne portait pas une atteinte grave et illégale à son droit de propriété sur ces biens, alors qu'il en était propriétaire indivis et n'en avait plus la libre disposition ; que le juge des référés a commis une erreur de droit en jugeant, après avoir relevé que cet établissement était situé sur le domaine public maritime, que les opérations de démolition des constructions ne portaient pas une atteinte grave et illégale à son droit de propriété, alors même que ces constructions avaient fait l'objet d'un permis de construire, dès lors que ce permis relève de la police de l'urbanisme et ne saurait conférer un droit de propriété ; <br/>
<br/>
              Considérant qu'aucun de ces moyens n'est de nature à permettre l'admission du pourvoi ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
       --------------<br/>
Article 1er : Il n'y a pas lieu de transmettre au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des dispositions du premier alinéa de l'article L. 2132-3 du code général de la propriété des personnes publiques.<br/>
Article 2 : Le pourvoi de M. A n'est pas admis. <br/>
Article 3 : La présente décision sera notifiée à M. A, au Premier ministre, ministre de l'écologie, du développement durable, des transports et du logement.<br/>
Copie en sera adressée au Conseil constitutionnel. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">24-01-03-01 DOMAINE. DOMAINE PUBLIC. PROTECTION DU DOMAINE. CONTRAVENTIONS DE GRANDE VOIRIE. - ARTICLE L. 2132-3 DU CG3P RELATIF À LA PROTECTION DU DOMAINE PUBLIC MARITIME - QPC - ABSENCE DE CARACTÈRE SÉRIEUX - 1) MÉCONNAISSANCE DU PRINCIPE DU CONTRADICTOIRE ET DU DROIT AU RECOURS - ABSENCE, DÈS LORS QUE LES GARANTIES PRÉVUES VALENT ÉGALEMENT POUR LES ÉVENTUELS PROPRIÉTAIRES - 2) MÉCONNAISSANCE DU DROIT DE PROPRIÉTÉ - ABSENCE - 3) MÉCONNAISSANCE DU PRINCIPE DE LA PERSONNALITÉ DES PEINES - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-10-02 PROCÉDURE. - QUESTION POSÉE DEVANT UNE JURIDICTION QUI A REFUSÉ SA TRANSMISSION AU CONSEIL D'ETAT - QUESTION SOULEVÉE À NOUVEAU DEVANT LE JUGE SAISI DU RECOURS CONTRE LA DÉCISION DE REFUS DE TRANSMISSION - RECEVABILITÉ, DÈS LORS QUE SONT SOULEVÉS DES MOYENS NOUVEAUX [RJ1] - APPLICATION AU CAS D'UNE ORDONNANCE DE TRI.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-10-05-04-02 PROCÉDURE. - ARTICLE L. 2132-3 DU CG3P RELATIF À LA PROTECTION DU DOMAINE PUBLIC MARITIME - QPC - ABSENCE DE CARACTÈRE SÉRIEUX - 1) MÉCONNAISSANCE DU PRINCIPE DU CONTRADICTOIRE ET DU DROIT AU RECOURS - ABSENCE, DÈS LORS QUE LES GARANTIES PRÉVUES VALENT ÉGALEMENT POUR LES ÉVENTUELS PROPRIÉTAIRES - 2) MÉCONNAISSANCE DU DROIT DE PROPRIÉTÉ - ABSENCE - 3) MÉCONNAISSANCE DU PRINCIPE DE LA PERSONNALITÉ DES PEINES - ABSENCE.
</SCT>
<ANA ID="9A"> 24-01-03-01 Contestation par une QPC des dispositions de l'article L. 2132-3 du code général de la propriété des personnes publiques (CG3P) relatif à la protection du domaine public maritime. 1) Les articles L. 774-1 à L. 774-13 du code de justice administrative, qui définissent la procédure de contravention de grande voirie applicable en cas de constat d'une infraction à l'article L. 2132-3 du CG3P assurent, non seulement pour le contrevenant mais aussi pour tous occupants de son fait y compris le cas échéant pour le propriétaire, la garantie de leur droit à un recours effectif et l'information sur les obligations leur incombant ainsi que des pouvoirs d'exécution dont dispose l'administration. 2) Les dispositions contestées n'ont pour objet ni de permettre la saisie d'objets, ni d'interdire à leur propriétaire d'en obtenir restitution quand ils ont été déplacés pour permettre la démolition de l'installation irrégulière. Absence de méconnaissance du droit de propriété. 3) Le principe de la personnalité des peines, qui ne trouve à s'appliquer qu'en matière répressive, ne peut être invoqué pour contester les mesures prises au titre de l'action domaniale et par lesquelles les autorités publiques chargées de la protection du domaine public naturel assurent sa remise en l'état, dès lors qu'une telle action ne présente pas de caractère répressif.</ANA>
<ANA ID="9B"> 54-10-02 Les règles de recevabilité posées dans la décision Région Centre (CE, 1er février 2012, n°351795) valent dans le cas où est contestée une ordonnance de tri prise par un juge des référés sur le fondement de l'article L. 522-3 du code de justice administrative.</ANA>
<ANA ID="9C"> 54-10-05-04-02 Contestation par une QPC des dispositions de l'article L. 2132-3 du code général de la propriété des personnes publiques (CG3P) relatif à la protection du domaine public maritime. 1) Les articles L. 774-1 à L. 774-13 du code de justice administrative, qui définissent la procédure de contravention de grande voirie applicable en cas de constat d'une infraction à l'article L. 2132-3 du CG3P assurent, non seulement pour le contrevenant mais aussi pour tous occupants de son fait y compris le cas échéant pour le propriétaire, la garantie de leur droit à un recours effectif et l'information sur les obligations leur incombant ainsi que des pouvoirs d'exécution dont dispose l'administration. 2) Les dispositions contestées n'ont pour objet ni de permettre la saisie d'objets, ni d'interdire à leur propriétaire d'en obtenir restitution quand ils ont été déplacés pour permettre la démolition de l'installation irrégulière. Absence de méconnaissance du droit de propriété. 3) Le principe de la personnalité des peines, qui ne trouve à s'appliquer qu'en matière répressive, ne peut être invoqué pour contester les mesures prises au titre de l'action domaniale et par lesquelles les autorités publiques chargées de la protection du domaine public naturel assurent sa remise en l'état, dès lors qu'une telle action ne présente pas de caractère répressif.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 1er février 2011, SARL Prototype Technique Industrie (Prototech), n° 342536, à publier au Recueil, et CE, 1er février 2012, Région Centre, n° 351795, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
