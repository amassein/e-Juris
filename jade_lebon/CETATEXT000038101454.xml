<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038101454</ID>
<ANCIEN_ID>JG_L_2019_02_000000415582</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/10/14/CETATEXT000038101454.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 06/02/2019, 415582</TITRE>
<DATE_DEC>2019-02-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>415582</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GHESTIN</AVOCATS>
<RAPPORTEUR>M. Vincent Ploquin-Duchefdelaville</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Karin Ciavaldini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:415582.20190206</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société à responsabilité limitée Attractive Fragrances et Cosmetics a demandé au tribunal administratif de Montreuil de la décharger, d'une part, des rappels de taxe sur la valeur ajoutée mis à sa charge pour les années 2010, 2011, 2012 et 2013, ainsi que des pénalités correspondantes et, d'autre part, de la cotisation supplémentaire d'impôt sur les sociétés à laquelle elle a été assujettie au titre de l'exercice clos en 2010, ainsi que des pénalités correspondantes. Par un jugement n° 1508025 du 4 avril 2017, ce tribunal , en premier lieu, a dit qu'il n'y avait pas lieu de statuer sur ses conclusions à hauteur des sommes de 3 512 euros, 464 euros, 629 euros et 766 euros, ainsi que des pénalités correspondantes, en deuxième lieu, lui a accordé la décharge des rappels de taxe sur la valeur ajoutée à hauteur de 24 761, 28 euros au titre de l'année 2010, de 1 501,09 euros au titre de l'année 2011, et de 147,21 euros au titre de la période correspondant à l'année 2013, ainsi que des pénalités correspondantes, en troisième lieu, a rejeté le surplus des conclusions de sa demande. <br/>
<br/>
              Par une ordonnance n° 17VE01590 du 21 septembre 2017, le président de la 1ère chambre de la cour administrative d'appel de Versailles a rejeté l'appel qu'elle avait formé contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 10 novembre 2017 et 12 février 2018 au secrétariat du contentieux du Conseil d'Etat, la société Attractive Fragrances et Cosmetics demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Vincent Ploquin-Duchefdelaville, auditeur,  <br/>
<br/>
              - les conclusions de Mme Karin Ciavaldini, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Ghestin, avocat de la société Attractive Fragrances et Cosmetics.<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. La société Attractive Fragrances et Cosmetics a demandé au tribunal administratif de Montreuil de prononcer, d'une part, la décharge des rappels de taxe sur la valeur ajoutée mis à sa charge pour les années 2010, 2011, 2012, et 2013, ainsi que des pénalités correspondantes et, d'autre part, la décharge de la cotisation supplémentaire d'impôt sur les sociétés à laquelle elle a été assujettie au titre de l'exercice clos en 2010, ainsi que des pénalités correspondantes. Par un jugement du 4 avril 2017, ce tribunal n'y a que partiellement fait droit. La société se pourvoit en cassation contre l'ordonnance rendue le 21 septembre 2017 par laquelle le président de la 1ère chambre de la cour administrative d'appel de Versailles a rejeté sa requête d'appel en application du 4° de l'article R. 222-1 du code de justice administrative. <br/>
<br/>
              2. Aux termes de l'article R. 222-1 du code de justice administrative : " (...) les présidents de formation de jugement des tribunaux et des cours (...) peuvent, par ordonnance : / (...) 4° Rejeter les requêtes manifestement irrecevables, lorsque la juridiction n'est pas tenue d'inviter leur auteur à les régulariser ou qu'elles n'ont pas été régularisées à l'expiration du délai imparti par une demande en ce sens ; / (...) les présidents des formations de jugement des cours peuvent (...), par ordonnance, rejeter (...), les requêtes dirigées contre des ordonnances prises en application des 1° à 5° du présent article (...) ". Aux termes de l'article R. 612-1 du même code : " Lorsque des conclusions sont entachées d'une irrecevabilité susceptible d'être couverte après l'expiration du délai de recours, la juridiction ne peut les rejeter en relevant d'office cette irrecevabilité qu'après avoir invité leur auteur à les régulariser. / (...) La demande de régularisation mentionne que, à défaut de régularisation, les conclusions pourront être rejetées comme irrecevables dès l'expiration du délai imparti qui, sauf urgence, ne peut être inférieur à quinze jours. La demande de régularisation tient lieu de l'information prévue à l'article R. 611-7 ".<br/>
<br/>
              3. Aux termes de l'article R. 414-1 du même code : " Lorsqu'elle est présentée par un avocat, un avocat au Conseil d'Etat et à la Cour de cassation, une personne morale de droit public autre qu'une commune de moins de 3 500 habitants ou un organisme de droit privé chargé de la gestion permanente d'un service public, la requête doit, à peine d'irrecevabilité, être adressée à la juridiction par voie électronique au moyen d'une application informatique dédiée accessible par le réseau internet. La même obligation est applicable aux autres mémoires du requérant (...) ". Aux termes de l'article R. 414-3 du même code, dans sa  rédaction applicable au litige : " Par dérogation aux dispositions des articles R. 411-3, R. 411-4, R. 412-1 et R. 412-2, les requérants sont dispensés de produire des copies de leur requête et des pièces qui sont jointes à celle-ci et à leurs mémoires. / Les pièces jointes sont présentées conformément à l'inventaire qui en est dressé. / Lorsque le requérant transmet, à l'appui de sa requête, un fichier unique comprenant plusieurs pièces, chacune d'entre elles doit être répertoriée par un signet la désignant conformément à l'inventaire mentionné ci-dessus. S'il transmet un fichier par pièce, l'intitulé de chacun d'entre eux doit être conforme à cet inventaire. Le respect de ces obligations est prescrit à peine d'irrecevabilité de la requête. / Les mêmes obligations sont applicables aux autres mémoires du requérant, sous peine pour celui-ci, après invitation à régulariser non suivie d'effet, de voir ses écritures écartées des débats. / Si les caractéristiques de certaines pièces font obstacle à leur communication par voie électronique, ces pièces sont transmises sur support papier, dans les conditions prévues par l'article R. 412-2. L'inventaire des pièces transmis par voie électronique en fait mention ".<br/>
<br/>
              4. Les dispositions citées au point 3 relatives à la transmission de la requête et des pièces qui y sont jointes par voie électronique définissent un instrument et les conditions de son utilisation qui concourent à la qualité du service public de la justice rendu par les juridictions administratives et à la bonne administration de la justice. Elles ont pour finalité de permettre un accès uniformisé et rationalisé à chacun des éléments du dossier de la procédure, selon des modalités communes aux parties, aux auxiliaires de justice et aux juridictions. A cette fin, elles organisent la transmission par voie électronique des pièces jointes à la requête à partir de leur inventaire détaillé et font obligation à son auteur de les transmettre soit en un fichier unique, chacune d'entre elles devant alors être répertoriée par un signet la désignant, soit en les distinguant chacune par un fichier désigné, l'intitulé des signets ou des fichiers devant être conforme à l'inventaire qui accompagne la requête. Ces dispositions ne font pas obstacle, lorsque l'auteur de la requête entend transmettre un nombre important de pièces jointes constituant une série homogène, telles que des factures, à ce qu'il les fasse parvenir à la juridiction en les regroupant dans un ou plusieurs fichiers sans répertorier individuellement chacune d'elles par un signet, à la condition que le référencement de ces fichiers ainsi que la numération, au sein de chacun d'eux, des pièces qu'ils regroupent soient conformes à l'inventaire.<br/>
<br/>
              5. Il résulte de ce qui précède qu'en jugeant que la requête d'appel formée par la société requérante méconnaissait les dispositions de l'article R. 414-3 du code de justice administrative rappelées au point 3 au seul motif que les fichiers joints à cette requête contenaient plusieurs pièces non répertoriées par des signets les désignant conformément à l'inventaire produit, sans rechercher si ces pièces pouvaient faire l'objet d'une présentation groupée conformément à ce qui est dit au point 4, le président de la 1ère chambre de la cour administrative d'appel de Versailles a entaché son ordonnance d'une erreur de droit. Ainsi, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, la société requérante est fondée à en demander l'annulation.<br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement à la société requérante de la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>                 D E C I D E :<br/>
                               --------------<br/>
<br/>
Article 1er : L'ordonnance du 21 septembre 2017 du président de la 1ère chambre de la cour administrative d'appel de Versailles est annulée.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Versailles.<br/>
Article 3 : L'Etat versera à la société Attractive Fragrances et Cosmetics la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.	 <br/>
<br/>
Article 4 : La présente décision sera notifiée à la société à responsabilité limitée Attractive Fragrances et Cosmetics et au ministre de l'action et des comptes publics.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-01-08 PROCÉDURE. INTRODUCTION DE L'INSTANCE. FORMES DE LA REQUÊTE. - PRÉSENTATION DE LA REQUÊTE PAR VOIE ÉLECTRONIQUE - FORMES IMPOSÉES À LA REQUÊTE ET AUX PIÈCES QUI Y SONT JOINTES (ART. R. 414-1 ET 414-3 DU CJA) - 1) PRINCIPE [RJ1] - 2) CAS OÙ LE REQUÉRANT ENTEND TRANSMETTRE UN NOMBRE IMPORTANT DE PIÈCES JOINTES CONSTITUANT UNE SÉRIE HOMOGÈNE - POSSIBILITÉ DE FAIRE PARVENIR CES PIÈCES EN LES REGROUPANT DANS UN OU PLUSIEURS FICHIERS SANS LES RÉPERTORIER INDIVIDUELLEMENT PAR UN SIGNET - EXISTENCE, SOUS RÉSERVE QUE CES PIÈCES ET FICHIERS SOIENT PRÉSENTÉS CONFORMÉMENT À L'INVENTAIRE ACCOMPAGNANT LA REQUÊTE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-07-01-03-02 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. QUESTIONS GÉNÉRALES. CONCLUSIONS. CONCLUSIONS IRRECEVABLES. - PRÉSENTATION DE LA REQUÊTE PAR VOIE ÉLECTRONIQUE - FORMES IMPOSÉES À LA REQUÊTE ET AUX PIÈCES QUI Y SONT JOINTES (ART. R. 414-1 ET R. 414-3 DU CJA) - 1) PRINCIPE [RJ1] - 2) CAS OÙ LE REQUÉRANT ENTEND TRANSMETTRE UN NOMBRE IMPORTANT DE PIÈCES JOINTES CONSTITUANT UNE SÉRIE HOMOGÈNE - PIÈCES REGROUPÉES DANS UN OU PLUSIEURS FICHIERS SANS ÊTRE RÉPERTORIÉES INDIVIDUELLEMENT PAR UN SIGNET - REQUÊTE IRRECEVABLE POUR CE MOTIF - ABSENCE, SOUS RÉSERVE, QUE CES PIÈCES ET FICHIERS SOIENT PRÉSENTÉS CONFORMÉMENT À L'INVENTAIRE ACCOMPAGNANT LA REQUÊTE.
</SCT>
<ANA ID="9A"> 54-01-08 1) Les articles R. 414-1 et R. 414-3 du code de justice administrative (CJA) relatifs à la transmission de la requête et des pièces qui y sont jointes par voie électronique définissent un instrument et les conditions de son utilisation qui concourent à la qualité du service public de la justice rendu par les juridictions administratives et à la bonne administration de la justice. Ils ont pour finalité de permettre un accès uniformisé et rationalisé à chacun des éléments du dossier de la procédure, selon des modalités communes aux parties, aux auxiliaires de justice et aux juridictions. A cette fin, ils organisent la transmission par voie électronique des pièces jointes à la requête à partir de leur inventaire détaillé et font obligation à son auteur de les transmettre soit en un fichier unique, chacune d'entre elles devant alors être répertoriée par un signet la désignant, soit en les distinguant chacune par un fichier désigné, l'intitulé des signets ou des fichiers devant être conforme à l'inventaire qui accompagne la requête.,,2) Ces articles ne font pas obstacle, lorsque l'auteur de la requête entend transmettre un nombre important de pièces jointes constituant une série homogène, telles que des factures, à ce qu'il les fasse parvenir à la juridiction en les regroupant dans un ou plusieurs fichiers sans répertorier individuellement chacune d'elles par un signet, à la condition que le référencement de ces fichiers ainsi que la numérotation, au sein de chacun d'eux, des pièces qu'ils regroupent soient conformes à l'inventaire.</ANA>
<ANA ID="9B"> 54-07-01-03-02 1) Les articles R. 414-1 et R. 414-3 du code de justice administrative (CJA) relatifs à la transmission de la requête et des pièces qui y sont jointes par voie électronique définissent un instrument et les conditions de son utilisation qui concourent à la qualité du service public de la justice rendu par les juridictions administratives et à la bonne administration de la justice. Ils ont pour finalité de permettre un accès uniformisé et rationalisé à chacun des éléments du dossier de la procédure, selon des modalités communes aux parties, aux auxiliaires de justice et aux juridictions. A cette fin, ils organisent la transmission par voie électronique des pièces jointes à la requête à partir de leur inventaire détaillé et font obligation à son auteur de les transmettre soit en un fichier unique, chacune d'entre elles devant alors être répertoriée par un signet la désignant, soit en les distinguant chacune par un fichier désigné, l'intitulé des signets ou des fichiers devant être conforme à l'inventaire qui accompagne la requête.,,2) Ces articles ne font pas obstacle, lorsque l'auteur de la requête entend transmettre un nombre important de pièces jointes constituant une série homogène, telles que des factures, à ce qu'il les fasse parvenir à la juridiction en les regroupant dans un ou plusieurs fichiers sans répertorier individuellement chacune d'elles par un signet, à la condition que le référencement de ces fichiers ainsi que la numérotation, au sein de chacun d'eux, des pièces qu'ils regroupent soient conformes à l'inventaire.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Section, 5 octobre 2018,,, n° 418233, à publier au Recueil.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
