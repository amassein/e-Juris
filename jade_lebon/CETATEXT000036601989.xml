<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036601989</ID>
<ANCIEN_ID>JG_L_2018_02_000000407124</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/60/19/CETATEXT000036601989.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 6ème chambres réunies, 14/02/2018, 407124</TITRE>
<DATE_DEC>2018-02-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>407124</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 6ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP POTIER DE LA VARDE, BUK LAMENT, ROBILLOT ; SCP BRIARD</AVOCATS>
<RAPPORTEUR>M. Alain Seban</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Laurence Marion</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:407124.20180214</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Cergy-Pontoise d'annuler la décision du 23 août 2016 par laquelle la commission d'attribution du bailleur social Logirep n'a pas retenu sa candidature pour un logement sis 1 allée de la Boulangère à Sarcelles. Par une ordonnance n° 1609543 du 18 octobre 2016, le président de la 9ème chambre de ce tribunal a rejeté sa demande.<br/>
<br/>
              Par un pourvoi, un nouveau mémoire et un mémoire en réplique, enregistrés les 23 et 27 janvier et le 18 octobre 2017 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande et d'enjoindre, à titre principal, à la société Logirep de lui attribuer le logement de type T4 qui lui avait été proposé sis 1, allée de la Boulangère à Sarcelles et à titre subsidiaire, au préfet du Val d'Oise d'assurer son relogement dans un délai de 15 jours à compter de la notification de la décision à intervenir, sous astreinte de 700 euros par jour de retard ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat une somme de 3 000 euros au titre de l'article 37 de la loi du 10 juillet 1991 et de l'article L. 761-1 du code de justice administrative, à verser à la SCP Potier de la Varde, Buk Lament, Robillot, renonçant à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
- le code de la construction et de l'habitation ;<br/>
- la loi n° 91-647 du 10 juillet 1991 ;<br/>
- le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Alain Seban, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Laurence Marion, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Potier de la Varde, Buk Lament, Robillot, avocat de M. A...et à la SCP Briard, avocat de la société Logirep.<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis au juge du fond que M.A..., demandeur de logement social depuis 2007,  a été reconnu prioritaire au titre de l'article L. 441-2-3-1 du code de la construction et de l'habitation par la commission de médiation du département de Paris le 4 juin 2010 ; que, par une ordonnance du 6 avril 2011, le magistrat désigné par le président du tribunal administratif de Paris a enjoint au préfet de la région d'Ile-de-France, préfet de Paris, de procéder à son relogement sous astreinte à compter du 1er juin 2011 ; que le 3 août 2016, les services de la préfecture du Val-d'Oise lui ont proposé un logement de type T4 de 88 m² situé 1 allée de la Boulangère à Sarcelles ; que, toutefois, par une décision du 23 août 2016, la commission d'attribution de l'organisme bailleur, la société Logirep, a rejeté la candidature de M. A...au motif que le logement n'était pas adapté à la composition de sa famille ; que l'intéressé a alors saisi le tribunal administratif d'un recours en excès de pouvoir tendant à l'annulation de cette décision ; que, par une ordonnance du 18 octobre 2016, le président de la 9ème chambre du tribunal administratif de Cergy-Pontoise a rejeté ce recours comme irrecevable ; que M. A...se pourvoit en cassation au Conseil d'Etat contre cette ordonnance ;<br/>
<br/>
              2. Considérant que l'article L. 300-1 du code de la construction et de l'habitation garantit à toute personne résidant sur le territoire français de façon régulière et dans des conditions de permanence définies par décret en Conseil d'Etat " le droit à un logement décent et indépendant " ; que, pour assurer l'effectivité de ce droit, l'article L. 441-2-3 du même code crée des commissions de médiation qui peuvent être saisies, sous certaines conditions, par toute personne qui n'est pas en mesure d'accéder à un logement décent et indépendant ; que le demandeur reconnu comme prioritaire par la commission de médiation doit se voir proposer, selon le cas, un logement ou un hébergement répondant à ses besoins et à ses capacités ; qu'à défaut d'une telle proposition dans un certain délai, l'article L. 441-2-3-1 permet au demandeur reconnu comme prioritaire d'exercer un recours spécial devant le tribunal administratif, qui peut ordonner à l'Etat, au besoin sous astreinte, son logement ou relogement ou son accueil en structure d'hébergement ; qu'en vertu des dispositions de l'article R. 778-2 du code de justice administrative, ce recours doit être exercé dans un délai de quatre mois à compter de l'expiration du délai dont le préfet disposait pour exécuter la décision de la commission de médiation ;<br/>
<br/>
              3. Considérant qu'il résulte des dispositions rappelées ci-dessus que le recours spécial destiné aux demandeurs reconnus comme prioritaires par la commission de médiation est seul ouvert pour obtenir l'exécution de la décision de cette commission ; que, lorsque la commission d'attribution d'un organisme de logement social auquel un demandeur a été désigné par le préfet, le cas échéant après injonction du tribunal administratif, oppose un refus, il est loisible à celui-ci de saisir, le cas échéant pour la seconde fois, le tribunal administratif d'un tel recours,  afin qu'il ordonne au préfet, si celui-ci s'est abstenu de le faire, de faire usage des pouvoirs qu'il tient des dispositions du II de l'article L. 441-2-3 du code de la construction et de l'habitation, en cas de refus de l'organisme de logement social de loger le demandeur, en vue de procéder à l'attribution d'un logement correspondant à ses besoins et à ses capacités, les dispositions de l'article L. 441-2-3-1 du même code faisant peser sur l'Etat, désigné comme garant du droit au logement opposable, une obligation de résultat ; que, toutefois, le demandeur peut aussi  saisir le tribunal administratif d'une demande d'annulation pour excès de pouvoir  de la décision par laquelle la commission d'attribution de l'organisme de logement social lui a refusé l'attribution d'un logement ; qu'en effet, cette demande, qui ne tend pas à faire exécuter par l'Etat la décision de la commission de médiation reconnaissant l'intéressé comme prioritaire et devant être relogé en urgence, est détachable de la procédure engagée par ailleurs pour obtenir l'exécution de cette décision ; qu'ainsi c'est au prix d'une erreur de droit que, par l'ordonnance attaquée, le tribunal administratif a rejeté comme irrecevable la demande de M. A...tendant à l'annulation pour excès de pouvoir de la décision du 23 août 2016 par laquelle la commission d'attribution de la société Logirep a rejeté sa candidature; que la circonstance que le 3 mai 2017, postérieurement à l'introduction du pourvoi, la société Logirep a fait au requérant une proposition pour un logement de type F3, dont il n'est d'ailleurs ni établi, ni même allégué, qu'elle aurait été acceptée par l'intéressé, ne saurait priver d'objet le pourvoi dirigé contre cette ordonnance ; que, par suite, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, M. A...est fondé à en demander l'annulation ;<br/>
<br/>
              4. Considérant que M. A...a obtenu le bénéfice de l'aide juridictionnelle ; que, par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ; qu'il y a lieu, dans les circonstances de l'espèce, et sous réserve que la Société civile professionnelle (SCP) Potier de La Varde, Buk Lament, Robillot, avocat de M.A..., renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de l'Etat la somme de 1 500 euros à verser à cette société ; que les dispositions de l'article L. 761-1 du code de justice administrative font, en revanche, obstacle à ce qu'une somme soit mise, à ce titre, à la charge de M.A..., qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du président de la 9ème chambre du tribunal administratif de Cergy-Pontoise est annulée.<br/>
Article 2 : L'affaire est renvoyée devant le tribunal administratif de Cergy-Pontoise.<br/>
Article 3 : L'Etat versera à la SCP Potier de La Varde, Buk Lament, Robillot, avocat de M. A..., une somme de 1 500 euros en application des dispositions du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
Article 4 : La présente décision sera notifiée à M. A...et à la société Logirep.<br/>
Copie pour information en sera adressée au ministre de la cohésion des territoires. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">38-07-01 LOGEMENT. - DEMANDEUR RECONNU PRIORITAIRE PAR LA COMMISSION DE MÉDIATION - REFUS DE LA COMMISSION D'ATTRIBUTION D'UN OLS DE LUI ATTRIBUER UN LOGEMENT - RECOURS OUVERTS AU DEMANDEUR - 1) SAISINE DU JUGE DALO (ART. L. 441-2-3-1 DU CCH) AFIN D'ORDONNER AU PRÉFET DE FAIRE USAGE DE SES POUVOIRS POUR OBTENIR L'EXÉCUTION DE LA DÉCISION DE LA COMMISSION DE MÉDIATION - EXISTENCE - 2) RECOURS POUR EXCÈS DE POUVOIR CONTRE LE REFUS DE LA COMMISSION D'ATTRIBUTION DE L'OLS - EXISTENCE.
</SCT>
<ANA ID="9A"> 38-07-01 1) Il résulte des articles L. 300-1, L. 441-2-3 et L. 441-2-3-1 du code la construction et de l'habitation (CCH) que le recours spécial destiné aux demandeurs reconnus comme prioritaires par la commission de médiation est seul ouvert pour obtenir l'exécution de la décision de cette commission. Lorsque la commission d'attribution d'un organisme de logement social (OLS) désigné par le préfet, le cas échéant après injonction du tribunal administratif, oppose un refus, il est loisible au demandeur de saisir, le cas échéant pour la seconde fois, le tribunal administratif d'un tel recours, afin qu'il ordonne au préfet, si celui-ci s'est abstenu de le faire, de faire usage des pouvoirs qu'il tient des dispositions du II de l'article L. 441-2-3 du CCH, en cas de refus de l'OLS de loger le demandeur, en vue de procéder à l'attribution d'un logement correspondant à ses besoins et à ses capacités, les dispositions de l'article L. 441-2-3-1 du même code faisant peser sur l'Etat, désigné comme garant du droit au logement opposable, une obligation de résultat.... ,,2) Le demandeur peut aussi saisir le tribunal administratif d'une demande d'annulation pour excès de pouvoir de la décision par laquelle la commission d'attribution de l'organisme de logement social lui a refusé l'attribution d'un logement. En effet, cette demande, qui ne tend pas à faire exécuter par l'Etat la décision de la commission de médiation reconnaissant l'intéressé comme prioritaire et devant être relogé en urgence, est détachable de la procédure engagée par ailleurs pour obtenir l'exécution de cette décision.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
