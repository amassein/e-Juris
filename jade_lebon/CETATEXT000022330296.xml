<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000022330296</ID>
<ANCIEN_ID>JG_L_2010_05_000000306354</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/22/33/02/CETATEXT000022330296.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 4ème sous-sections réunies, 26/05/2010, 306354</TITRE>
<DATE_DEC>2010-05-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>306354</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 4ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Vigouroux</PRESIDENT>
<AVOCATS>SCP CELICE, BLANCPAIN, SOLTNER ; SCP BARADUC, DUHAMEL ; SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>M. Philippe  Ranquet</RAPPORTEUR>
<COMMISSAIRE_GVT/>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 8 juin et 10 septembre 2007 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. et Mme Jean-Claude A, demeurant au ... ; M. et Mme A demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt du 18 juillet 2006 par lequel la cour administrative d'appel de Bordeaux a rejeté leur requête tendant à l'annulation du jugement du 6 février 2003 du tribunal administratif de Limoges rejetant leur demande de condamnation du centre hospitalier de Guéret à leur verser, en qualité de représentants légaux de leurs enfants mineurs et en leur nom propre, diverses sommes en réparation des conséquences dommageables des conditions de prise en charge de la naissance de leur fils Anthony B le 27 novembre 1994 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur requête d'appel et d'assortir les indemnités à la charge du centre hospitalier des intérêts de droit capitalisés ;<br/>
<br/>
              3°) de mettre à la charge du centre hospitalier de Guéret la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de la sécurité sociale ;<br/>
<br/>
              Vu la loi n° 2006-1640 du 21 décembre 2006 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Philippe Ranquet, Auditeur,<br/>
<br/>
              - les observations de la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de M. et Mme A, de la SCP Célice, Blancpain, Soltner, avocat du centre hospitalier de Guéret et de la SCP Baraduc, Duhamel, avocat de la caisse de mutualité sociale agricole de la Creuse, <br/>
<br/>
              - les conclusions de Mme Catherine de Salins, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de M. et Mme A, à la SCP Célice, Blancpain, Soltner, avocat du centre hospitalier de Guéret et à la SCP Baraduc, Duhamel, avocat de la caisse de mutualité sociale agricole de la Creuse ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que Mme A, prise en charge au centre hospitalier de Guéret, y a donné naissance par césarienne, dans la nuit du 26 au 27 novembre 1994, à son second enfant Anthony B qui est atteint depuis lors d'une infirmité motrice cérébrale majeure le rendant totalement dépendant ; que par l'arrêt du 18 juillet 2006 contre lequel M. et Mme A se pourvoient en cassation, la cour administrative d'appel de Bordeaux a rejeté leur requête tendant à l'annulation du jugement du 6 février 2003 du tribunal administratif de Limoges rejetant leur demande tendant à la condamnation du centre hospitalier de Guéret à les indemniser, en leur nom propre et en leur qualité de représentants légaux de leurs deux enfants, des conséquences dommageables de cette infirmité ; que la caisse de mutualité sociale agricole de la Creuse, à laquelle les requérants sont affiliés et qui a vu de même ses demandes indemnitaires rejetées, conclut également, dans le mémoire qu'elle a produit en réponse à la communication du pourvoi, à l'annulation de l'arrêt ;<br/>
<br/>
              Considérant que dans le cas où la faute commise lors de la prise en charge ou du traitement d'un patient dans un établissement public hospitalier a compromis ses chances d'obtenir une amélioration de son état de santé ou d'échapper à son aggravation, le préjudice résultant directement de la faute commise par l'établissement et qui doit être intégralement réparé n'est pas le dommage corporel constaté, mais la perte de chance d'éviter que ce dommage soit advenu ; que la réparation qui incombe à l'hôpital doit alors être évaluée à une fraction du dommage corporel déterminée en fonction de l'ampleur de la chance perdue ;<br/>
<br/>
              Considérant qu'après avoir relevé, d'une part, que la survenue de ralentissements du rythme cardiaque foetal à 4 heures 15 le 27 novembre 1994, dès lors qu'elle était associée à la coloration teintée du liquide amniotique observée la veille vers 23 heures à la rupture de la poche des eaux, était le signe d'une souffrance foetale justifiant de prendre immédiatement la décision d'extraire l'enfant et, d'autre part, que l'obstétricien n'avait été appelé par la sage-femme qu'à 5 heures 15 et n'avait pris la décision de pratiquer une césarienne qu'à 5 heures 30, la cour administrative d'appel a jugé que le retard dans la prise de cette décision était constitutif d'une faute de nature à engager la responsabilité du centre hospitalier ; qu'elle a, toutefois, également relevé qu'il était impossible d'affirmer que les lésions auraient pour origine les souffrances foetales subies au cours du travail ou que les séquelles auraient été moins sévères si l'extraction avait été réalisée plus rapidement, de sorte que l'existence d'un lien de causalité entre l'état de l'enfant et la faute commise ne pouvait être regardée comme établie, et a rejeté, pour ce motif, les demandes d'indemnisation ; qu'en tirant une telle conséquence de la circonstance qu'il n'était pas certain que le dommage ne serait pas advenu en l'absence de retard fautif, sans rechercher si, au moment où la décision appropriée à son état aurait dû être prise, l'enfant avait une chance, que le retard lui aurait fait perdre, d'échapper aux séquelles dont il est atteint, la cour administrative d'appel de Bordeaux a, ainsi que le soutient la caisse de mutualité sociale agricole de la Creuse, commis une erreur de droit ; qu'il y a par suite lieu, sans qu'il soit besoin d'examiner les moyens du pourvoi de M. et Mme A, d'annuler son arrêt du 18 juillet 2006 ;<br/>
<br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de faire application de l'article L. 821-2 du code de justice administrative et de régler l'affaire au fond ;<br/>
<br/>
              Sur la responsabilité du centre hospitalier de Guéret :<br/>
<br/>
              Considérant qu'il résulte de l'instruction, et notamment des expertises ordonnées en référé et avant dire droit par le tribunal administratif de Limoges, que la couleur teintée du liquide amniotique à la rupture de la poche des eaux ne révélait pas, à elle seule, une souffrance foetale aiguë imposant l'extraction de l'enfant, mais qu'ainsi qu'il a été dit ci-dessus, les autres signes de souffrance apparus à partir de 4 heures 15 auraient dû conduire la sage-femme à appeler immédiatement l'obstétricien de garde afin qu'il procède à l'extraction ou, en cas d'impossibilité, qu'il décide de pratiquer une césarienne en urgence ; que le retard d'une heure à appeler l'obstétricien constitue une faute dans l'organisation et le fonctionnement du service public hospitalier ;<br/>
<br/>
              Considérant qu'il résulte également de l'instruction qu'en cas de souffrance foetale aiguë, tout retard dans l'extraction de l'enfant est susceptible de contribuer à l'apparition ou à l'aggravation de séquelles cérébrales ; que s'il n'est pas certain, en l'espèce, que le dommage ne serait pas advenu en l'absence du retard fautif, il n'est pas davantage établi avec certitude que les lésions étaient déjà irréversiblement acquises dans leur totalité quand la décision de pratiquer la césarienne aurait dû être prise, ni que le délai de 35 minutes qui aurait en toute hypothèse séparé cette décision de l'extraction de l'enfant aurait suffi à l'apparition des mêmes lésions ; que dans ces conditions, le retard fautif a fait perdre à Anthony B une chance d'éviter tout ou partie des séquelles dont il est resté atteint ; qu'eu égard à l'importante probabilité qu'avait la souffrance installée à 4 h 15 d'évoluer, même prise en charge à temps, vers de telles séquelles, il y a lieu d'évaluer l'ampleur de cette perte de chance à 30 % et de mettre à la charge du centre hospitalier de Guéret la réparation de cette fraction du dommage corporel ; que M. et Mme A sont, par suite, fondés à soutenir que c'est à tort que le tribunal administratif de Limoges a rejeté les demandes indemnitaires dont il était saisi ;<br/>
<br/>
              Sur le préjudice d'Anthony B :<br/>
<br/>
              En ce qui concerne le recours exercé par la caisse de mutualité sociale agricole de la Creuse :<br/>
<br/>
              Considérant que la caisse de mutualité sociale agricole de la Creuse, qui demande, dans l'hypothèse où le centre hospitalier de Guéret serait responsable des préjudices subis par Anthony B, le remboursement de frais exposés par elle à raison de ces préjudices, dont elle indique les montants, doit être regardée comme exerçant, par subrogation à la victime, le recours prévu à l'article L. 376-1 du code de la sécurité sociale ; que la fin de non recevoir opposée par le centre hospitalier et tirée de ce que la caisse n'aurait présenté aucune conclusion indemnitaire doit, par suite, être rejetée ; qu'il y a lieu de statuer poste par poste sur les droits respectifs de la victime et de la caisse en application des dispositions de cet article telles qu'elles ont été modifiées par la loi n° 2006-1640 du 21 décembre 2006, qui s'appliquent à la réparation des dommages résultant d'évènements antérieurs à la date d'entrée en vigueur de cette loi dès lors que, comme en l'espèce, le montant de l'indemnité due à la victime n'a pas été définitivement fixé avant cette date, et en tenant compte de la fraction du dommage dont l'établissement hospitalier est responsable ; que pour chacun de ces postes, l'indemnité à la charge du centre hospitalier de Guéret est égale, ainsi qu'il a été dit ci-dessus, à 30 % du dommage ;<br/>
<br/>
              En ce qui concerne les préjudices à caractère patrimonial :<br/>
<br/>
              Quant aux dépenses de santé :<br/>
<br/>
              Considérant que, si la caisse de mutualité sociale agricole de la Creuse n'apporte pas de justification précise pour l'ensemble des sommes dont elle demande remboursement au titre des dépenses de santé, il résulte de l'instruction que ses demandes doivent être regardées comme justifiées, en ce qui concerne les frais qu'elle a exposés jusqu'à la date de la présente décision, à hauteur de 10 000 euros pour les soins délivrés à Anthony B et de 20 000 euros pour la fourniture ou la location d'appareillage, et en ce qui concerne les frais qu'elle devra exposer à l'avenir, à 516 euros par an au titre des soins, auxquels s'ajoute un montant annuel de 721,76 euros au titre d'hospitalisations de jour jusqu'à ce qu'Anthony B atteigne l'âge de vingt ans, et à 1 500 euros par an au titre de l'appareillage ; qu'il y a en outre lieu de regarder les sommes qu'elles a supportées au titre de l'intervention, depuis 2005, d'un service d'éducation spécialisée et de soins à domicile, comme consistant pour la moitié, soit 70 000 euros, en des dépenses de santé ; que M. et Mme A n'établissent en revanche par aucun justificatif que des frais d'appareillage soient restés à la charge de la victime ; que, dans ces conditions, la perte de chance de subir le dommage étant estimée à 30 %, il y a lieu d'accorder à la seule caisse de mutualité sociale agricole de la Creuse, au titre des frais exposés jusqu'à la date de la présente décision, une somme égale à 30 % de 100 000 euros, soit 30 000 euros, et au titre des frais futurs, une rente annuelle dont le montant, fixé à la même date à 30 % de 2 737,76 euros, soit 821,33 euros jusqu'à ce qu'Anthony B atteigne l'âge de vingt ans et à 30 % de 2 016 euros, soit 604,80 euros pour les années ultérieures, sera revalorisé par la suite par application des coefficients prévus à l'article L. 434-17 du code de la sécurité sociale ; <br/>
<br/>
              Quant aux frais liés au handicap :<br/>
<br/>
              Considérant qu'il résulte de l'instruction que l'infirmité motrice totale et l'état de complète dépendance dans lesquels se trouve Anthony B rendent nécessaires l'assistance constante d'une tierce personne ; qu'il a été constamment hébergé et pris en charge au domicile familial dans des conditions imposant d'importants aménagements au logement familial et l'acquisition d'un véhicule adapté, et est susceptible de l'être à l'avenir ; que depuis 2005, ainsi qu'il a été dit ci-dessus, intervient également auprès de lui un service d'éducation spécialisée et de soins à domicile dont la charge incombe à l'assurance maladie, et qu'il ne peut être exclu qu'à l'avenir, son état requière le placement dans une institution spécialisée ;<br/>
<br/>
              Considérant qu'il sera fait une juste appréciation du préjudice résultant des besoins d'aménagement du logement et d'acquisition d'un véhicule en l'évaluant en capital, en l'absence de justification plus précise du montant des dépenses nécessaires, à 60 000 euros ; qu'il sera fait une juste appréciation des besoins en assistance d'une tierce personne à domicile en les évaluant, compte tenu du salaire minimum interprofessionnel de croissance (SMIC) horaire brut augmenté des charges sociales, à 348 000 euros pour la période écoulée de la naissance d'Anthony B à la date de la présente décision ; que la moitié de la somme supportée par la caisse de mutualité sociale agricole de la Creuse au titre de la prise en charge de l'enfant par le service d'éducation spécialisée et de soins à domicile, soit 70 000 euros, doit être regardée comme relevant des frais liés au handicap ; qu'à la date de la présente décision, le poste de préjudice des frais liés au handicap doit ainsi être évalué à un montant de 478 000 euros, dont la réparation incombe au centre hospitalier de Guéret pour la fraction de 30 % correspondant à l'ampleur de la chance perdue, soit 143 400 euros ; que les dépenses supportées par les représentants légaux d'Anthony B étant supérieures à ce montant, qui doit être attribué par préférence à la victime conformément aux dispositions de l'article L. 376-1 du code de la sécurité sociale, il y a dès lors lieu de le leur allouer en totalité ;<br/>
<br/>
              Considérant que si le juge n'est pas en mesure de déterminer lorsqu'il se prononce si l'enfant sera placé dans une institution spécialisée ou s'il sera hébergé au domicile de sa famille, il lui appartient d'accorder à l'enfant une rente trimestrielle couvrant les frais de son maintien au domicile familial, en fixant un taux quotidien et en précisant que la rente sera versée au prorata du nombre de nuits que l'enfant aura passées à ce domicile au cours du trimestre considéré ; que les autres chefs de préjudice demeurés à la charge de l'enfant doivent être indemnisés par ailleurs, sous la forme soit d'un capital, soit d'une rente distincte ; que le juge doit condamner le responsable du dommage à rembourser à l'organisme de sécurité sociale qui aura assumé la charge du placement de l'enfant dans une institution spécialisée, sur justificatifs, les frais qu'il justifiera avoir exposés de ce fait ; qu'en cas de refus du centre hospitalier, il appartiendra à la caisse de faire usage des voies de droit permettant d'obtenir l'exécution des décisions de la justice administrative ;<br/>
<br/>
              Considérant qu'il résulte de ce qui précède que l'indemnité due par le centre hospitalier de Guéret, à compter de la présente décision, au titre des frais liés au handicap sera égale, à chaque trimestre échu, à 30 % de la somme, d'une part, d'un montant représentatif de sa prise en charge à domicile déterminé sur la base d'un taux quotidien, qu'il y a lieu de fixer à 66 euros à la date de la présente décision et de revaloriser par la suite par application des coefficients prévus à l'article L. 434-17 du code de la sécurité sociale, et qui sera retenu au prorata du nombre de nuits passées au domicile familial au cours du trimestre, et d'autre part, des sommes que la caisse de mutualité sociale agricole de la Creuse établira, sur justificatifs, avoir exposées pour la même période ; que l'indemnité ainsi calculée devant être attribuée par préférence à la victime conformément aux dispositions de l'article L. 376-1 du code de la sécurité sociale, elle sera intégralement versée aux représentants légaux d'Anthony B tant qu'elle sera inférieure au montant représentatif de la prise en charge à domicile pour le trimestre en cause ; qu'en revanche, lorsqu'elle dépassera ce montant, celui-ci sera versé aux intéressés et le solde à la caisse de mutualité sociale agricole de la Creuse ;<br/>
<br/>
              En ce qui concerne les préjudices à caractère personnel :<br/>
<br/>
              Considérant qu'il sera fait une juste appréciation des troubles de toute nature subis par Anthony B à raison d'un déficit fonctionnel permanent évalué, par les experts commis en référé par le tribunal administratif de Limoges, à 100 %, de ses souffrances physiques et morales et de son préjudice esthétique en les évaluant à 600 000 euros ; que ses représentants légaux ont ainsi droit au versement d'un capital égal à 30 % de cette somme, soit 180 000 euros ;<br/>
<br/>
              En ce qui concerne les autres demandes de la caisse de mutualité sociale agricole de la Creuse :<br/>
<br/>
              Considérant que la caisse justifie verser aux parent d'Anthony B, depuis 1997, l'allocation d'éducation spéciale, et soutient qu'elle devra verser à la victime, à compter de 2014, l'allocation aux adultes handicapés ; que de telles allocations n'ont toutefois pas pour objet la réparation d'un accident et n'ouvrent pas droit à recours de la part des organismes de sécurité sociale sur le fondement de l'article L. 376-1 du code de la sécurité sociale ; que si la même caisse réclame, par ailleurs, des sommes au titre de l'incapacité permanente dont est atteint Anthony B, il n'est ni établi, ni même allégué qu'elle verse ou devra verser une prestation de ce chef ;<br/>
<br/>
              Sur le préjudice des parents et du frère d'Anthony B :<br/>
<br/>
              Considérant qu'il sera fait une juste appréciation des troubles dans les conditions d'existence et du préjudice moral subis par les parents et le frère aîné d'Anthony B à raison du handicap dont il est atteint en les évaluant à 40 000 euros pour ses parents et 15 000 euros pour son frère ; qu'il y a ainsi lieu, compte tenu de la fraction de 30 % définie plus haut, de condamner le centre hospitalier de Guéret à leur verser, respectivement, 12 000 euros chacun et 4 500 euros ;<br/>
<br/>
              Sur les intérêts et intérêts des intérêts :<br/>
<br/>
              Considérant que les consorts A ont droit aux intérêts au taux légal sur les sommes qui leur sont dues en capital par le centre hospitalier de Guéret à compter de leur demande indemnitaire du 19 juin 1997 ; qu'ils ont demandé la capitalisation des intérêts le 17 août 2001 ; qu'il y a lieu de faire droit à cette demande tant à cette date qu'à chaque échéance annuelle ultérieure ;<br/>
<br/>
              Sur les frais d'expertise :<br/>
<br/>
              Considérant qu'il y lieu de mettre les frais de l'expertise ordonnée en référé par le tribunal administratif de Limoges et de celle ordonnée avant dire droit par le même tribunal, taxés et liquidés pour un montant total de 1 682,74 euros, à la charge du centre hospitalier de Guéret ;<br/>
<br/>
              Sur l'application des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              Considérant que ces dispositions font obstacle à ce que soit mise à la charge de M. et Mme A, qui ne sont pas la partie perdante dans la présente instance, la somme que le centre hospitalier de Guéret demande sur leur fondement ; qu'il y a en revanche lieu, dans les circonstances de l'espèce, de mettre à la charge de ce centre hospitalier une somme de 9 000 euros au titre des frais exposés par M. et Mme A devant le tribunal administratif, la cour administrative d'appel et le Conseil d'Etat et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 18 juillet 2006 de la cour administrative d'appel de Bordeaux et le jugement du 6 février 2003 du tribunal administratif de Limoges sont annulés.<br/>
<br/>
Article 2 : Le centre hospitalier de Guéret est condamné à verser à M. et Mme A, en leur qualité de représentants légaux de leur fils Anthony, la somme de 323 400 euros, en leur nom propre, la somme de 12 000 euros chacun, et en leur qualité de représentants légaux de leur fils Romain, la somme de 4 500 euros. Ces sommes porteront intérêts légaux à compter du 19 juin 1997. Les intérêts échus le 17 août 2001 seront capitalisés à cette date puis à chaque échéance annuelle ultérieure à compter de cette date pour produire eux mêmes intérêts.<br/>
<br/>
Article 3 : Le centre hospitalier de Guéret est condamné à verser à la caisse de mutualité sociale agricole de la Creuse une somme de 30 000 euros et, à compter de la date de la présente décision et par trimestre échu, une rente dont le montant annuel, fixé à la même date à 821,33 euros jusqu'à ce qu'Anthony B atteigne l'âge de vingt ans et à  604,80 euros pour les années ultérieures, sera revalorisé par application des coefficients prévus à l'article L. 434-17 du code de la sécurité sociale.<br/>
<br/>
Article 4 : Le centre hospitalier de Guéret est condamné à verser, à compter de la date de la présente décision et par trimestre échu, une indemnité au titre des frais liés au handicap égale à 30 % de la somme, d'une part, d'un montant représentatif de la prise en charge à domicile d'Anthony B déterminé sur la base d'un taux quotidien fixé à 66 euros à la même date et revalorisé par la suite par application des coefficients prévus à l'article L. 434-17 du code de la sécurité sociale, retenu au prorata du nombre de nuits passées au domicile familial au cours du trimestre, et d'autre part, des sommes que la caisse de mutualité sociale agricole de la Creuse établira, sur justificatifs, avoir exposées au titre de ce poste de préjudice pour la même période. L'indemnité ainsi calculée sera intégralement versée aux représentants légaux d'Anthony B tant qu'elle sera inférieure au montant représentatif de la prise en charge à domicile pour le trimestre en cause. Lorsqu'elle dépassera ce montant, celui-ci sera versé aux intéressés et le solde à la caisse de mutualité sociale agricole de la Creuse.<br/>
<br/>
Article 5 : Les frais d'expertise, taxés à la somme de 1 682,74 euros, sont mis à la charge du centre hospitalier de Guéret.<br/>
<br/>
Article 6 : Le centre hospitalier de Guéret versera à M. et Mme A la somme de 9 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 7 : Le surplus des conclusions des requêtes de M. et Mme A et de la caisse de mutualité sociale agricole de la Creuse devant la cour administrative d'appel de Bordeaux, ainsi que les conclusions du centre hospitalier de Guéret au titre de l'article L. 761-1 du code de justice administrative sont rejetés.<br/>
<br/>
Article 8 : La présente décision sera notifiée à M. et Mme Jean-Claude A, au centre hospitalier de Guéret et à la caisse de mutualité sociale agricole de la Creuse.<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">60-02-01-01-02 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RESPONSABILITÉ EN RAISON DES DIFFÉRENTES ACTIVITÉS DES SERVICES PUBLICS. SERVICE PUBLIC DE SANTÉ. ÉTABLISSEMENTS PUBLICS D'HOSPITALISATION. RESPONSABILITÉ POUR FAUTE MÉDICALE : ACTES MÉDICAUX. - PRISE EN CHARGE DE LA NAISSANCE - RETARD FAUTIF - PORTÉE - PERTE DE CHANCE D'ÉVITER LES SÉQUELLES QUI SONT ADVENUES - 1) EXISTENCE COMPTE TENU DES DONNÉES DE L'ESPÈCE [RJ1] - 2) IMPORTANCE - CHANCE PERDUE ÉVALUÉE À 30%.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">60-04-03 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RÉPARATION. ÉVALUATION DU PRÉJUDICE. - PRISE EN CHARGE DE LA NAISSANCE - RETARD FAUTIF - PORTÉE - PERTE DE CHANCE D'ÉVITER LES SÉQUELLES QUI SONT ADVENUES - 1) EXISTENCE COMPTE TENU DES DONNÉES DE L'ESPÈCE [RJ1] - 2) IMPORTANCE - CHANCE PERDUE ÉVALUÉE À 30%.
</SCT>
<ANA ID="9A"> 60-02-01-01-02 Retard d'une heure à appeler l'obstétricien dans un cas de souffrance foetale aiguë constituant une faute dans l'organisation et le fonctionnement du service public hospitalier. 1) Il n'est pas certain que le dommage ne serait pas advenu en l'absence du retard fautif, tout retard dans l'extraction de l'enfant étant susceptible de contribuer à l'apparition ou à l'aggravation de séquelles cérébrales. Il n'est pas pour autant davantage établi avec certitude que les lésions étaient déjà irréversiblement acquises dans leur totalité quand la décision de pratiquer la césarienne aurait dû être prise, ni que le délai de 35 minutes qui aurait en toute hypothèse séparé cette décision de l'extraction de l'enfant aurait suffi à l'apparition des mêmes lésions. Dans ces conditions, la responsabilité du centre hospitalier à raison du retard fautif est engagée à raison de la perte de chance du patient d'éviter tout ou partie des séquelles dont il est resté atteint. 2) Eu égard à l'importante probabilité qu'avait la souffrance d'origine d'évoluer, même prise en charge à temps, vers les séquelles dont est atteint l'enfant, il y a lieu d'évaluer l'ampleur de cette perte de chance à 30 %.</ANA>
<ANA ID="9B"> 60-04-03 Retard d'une heure à appeler l'obstétricien dans un cas de souffrance foetale aiguë constituant une faute dans l'organisation et le fonctionnement du service public hospitalier. 1) Il n'est pas certain que le dommage ne serait pas advenu en l'absence du retard fautif, tout retard dans l'extraction de l'enfant étant susceptible de contribuer à l'apparition ou à l'aggravation de séquelles cérébrales. Il n'est pas pour autant davantage établi avec certitude que les lésions étaient déjà irréversiblement acquises dans leur totalité quand la décision de pratiquer la césarienne aurait dû être prise, ni que le délai de 35 minutes qui aurait en toute hypothèse séparé cette décision de l'extraction de l'enfant aurait suffi à l'apparition des mêmes lésions. Dans ces conditions, la responsabilité du centre hospitalier à raison du retard fautif est engagée à raison de la perte de chance du patient d'éviter tout ou partie des séquelles dont il est resté atteint. 2) Eu égard à l'importante probabilité qu'avait la souffrance d'origine d'évoluer, même prise en charge à temps, vers les séquelles dont est atteint l'enfant, il y a lieu d'évaluer l'ampleur de cette perte de chance à 30 %.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. Section, 21 décembre 2007, Centre hospitalier de Vienne, n° 289328, p. 546.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
