<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034823504</ID>
<ANCIEN_ID>JG_L_2017_05_000000410833</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/82/35/CETATEXT000034823504.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, formation collégiale, 29/05/2017, 410833, Publié au recueil Lebon</TITRE>
<DATE_DEC>2017-05-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>410833</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés, formation collégiale</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN ; SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. le Pdt. Bernard Stirn</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2017:410833.20170529</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
              Par une requête, enregistrée le 24 mai 2017 au secrétariat du contentieux du Conseil d'Etat, l'association " En marche ! " demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'ordonner la suspension de l'exécution de la décision n° 2017-254 du Conseil supérieur de l'audiovisuel du 23 mai 2017 fixant la durée des émissions de la campagne électorale en vue des élections législatives des 11 et 18 juin 2017 ;<br/>
<br/>
              2°) d'enjoindre au Conseil supérieur de l'audiovisuel de fixer à un niveau qui ne saurait être inférieur à celui des deux principaux partis et groupements représentés par des groupes parlementaires de l'Assemblée nationale la durée d'émission mise à la disposition de l'association " En marche ! " dans le cadre de la campagne en vue des élections législatives ;<br/>
<br/>
              3°) d'enjoindre au Conseil supérieur de l'audiovisuel de veiller à ce que l'équité de traitement des partis et des groupements politiques présentant des candidats à l'élection législative soit assurée par les services de télévision et radiodiffusion sonore ; <br/>
<br/>
              4°) de mettre à la charge du Conseil supérieur de l'audiovisuel la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              L'association " En marche ! " soutient que : <br/>
              - la condition d'urgence posée par l'article L. 521-2 du code de justice administrative est remplie dès lors que la campagne électorale est ouverte depuis le 22 mai 2017 ;<br/>
              - la décision attaquée porte une atteinte grave et manifestement illégale au respect du caractère pluraliste de l'expression des courants de pensée et d'opinion, qui constitue une liberté fondamentale en période électorale ;<br/>
              - elle a soulevé par un mémoire distinct une question prioritaire de constitutionnalité dirigée contre les dispositions de l'article L. 167-1 du code électoral, qui méconnaissent les articles 3 et 4 de la Constitution ainsi que les articles 6 et 11 de la Déclaration des droits de l'homme et du citoyen ;<br/>
              - les dispositions de l'article L. 167-1 du code électoral sont incompatibles avec les stipulations de l'article 3 du premier protocole additionnel à la Convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, des articles 10 et 14 de cette convention et de l'article 25 du Pacte international relatif aux droits civiques et politiques, dès lors qu'elles permettent aux partis représentés par des groupes parlementaires à l'Assemblée nationale de disposer ensemble de trois heures d'émission avant le premier tour de scrutin et d'une heure et demie entre les deux tours, alors que chacun des autres partis présentant au moins 75 candidats ne peuvent disposer que de sept minutes avant le premier tour et cinq minutes entre les deux tours, et ce quels que soient les résultats obtenus par les différents partis lors de l'élection présidentielle qui a précédé immédiatement les élections législatives ;<br/>
              - en toute hypothèse, l'application de ces dispositions législatives dans la situation résultant de l'élection présidentielle des 27 avril et 7 mai 2017 porte une atteinte grave et manifestement illégale à son droit de concourir à des élections libres, garanti par les stipulations mentionnées ci-dessus, en ne lui permettant de disposer que d'une durée d'émission très inférieure à celles allouées à ses principaux concurrents, alors que les résultats de l'élection présidentielle établissent sa représentativité et qu'elle présente des candidats en vue de soutenir l'action du Président de la République. <br/>
<br/>
              Par un mémoire distinct, enregistré le 24 mai 2017, présenté en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958, l'association " En marche ! " demande au juge des référés du Conseil d'Etat de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution de l'article L. 167-1 du code électoral. Elle soutient que cet article est applicable au litige et n'a jamais été déclaré conforme à la Constitution et qu'il méconnaît les articles 3 et 4 de la Constitution ainsi que les articles 6 et 11 de la Déclaration des droits de l'homme et du citoyen.<br/>
<br/>
              Par mémoire en défense et un mémoire complémentaire, enregistrés les 24 et 26 mai 2017, le président du groupe parlementaire Les Républicains conclut au rejet de la requête. Il soutient que la requête ne relève pas de la compétence de la juridiction administrative, qu'elle n'est pas justifiée par l'urgence et que les moyens soulevés par l'association requérante ne sont pas fondés. <br/>
<br/>
              Par un mémoire en défense, enregistré le 25 mai 2017, le président du groupe parlementaire Radical, républicain, démocrate et progressiste conclut au rejet de la requête. Il soutient que les moyens soulevés par l'association requérante ne sont pas fondés. <br/>
<br/>
              Par un mémoire en défense, enregistré le 26 mai 2017, le président du groupe parlementaire Gauche démocrate et républicaine conclut au rejet de la requête. Il soutient que les moyens soulevés par l'association requérante ne sont pas fondés. <br/>
<br/>
              Par un mémoire en défense et un mémoire complémentaire, enregistrés le 26 mai 2017, le président du groupe Union des démocrates et indépendants, conclut au rejet de la requête. Il soutient que la requête ne relève pas de la compétence de la juridiction administrative, qu'elle n'est pas justifiée par l'urgence et que les moyens soulevés par l'association requérante ne sont pas fondés. <br/>
<br/>
              Par un mémoire en défense, enregistré le 26 mai 2017, le Conseil supérieur de l'audiovisuel conclut au rejet de la requête. Il soutient que les moyens soulevés par l'association requérante ne sont pas fondés.<br/>
<br/>
              Par des observations en défense, enregistrées le 27 mai 2017, le ministre d'Etat, ministre de l'intérieur, fait valoir que la question prioritaire de constitutionnalité soulevée par l'association requérante n'est pas nouvelle. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son préambule et ses articles 3, 4 et 61-1 ;<br/>
              - le code électoral ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, l'association " En marche ! ", d'autre part, le Conseil supérieur de l'audiovisuel, le Premier ministre, le ministre d'Etat, ministre de l'intérieur, la ministre de la culture, le président du groupe parlementaire socialiste, écologiste et républicain, le président du groupe parlementaire Les Républicains, le président du groupe parlementaire Radical, républicain, démocrate et progressiste, le président du groupe parlementaire Gauche démocrate et républicaine et le président du groupe parlementaire Union des démocrates et indépendants ; <br/>
<br/>
              Vu le procès-verbal de l'audience publique du lundi 29 mai 2017 à 10 heures au cours de laquelle ont été entendus : <br/>
<br/>
              - Me Piwnica, avocat au Conseil d'Etat et à la Cour de cassation, avocat de l'association " En marche ! " ;<br/>
<br/>
- les représentants du Conseil supérieur de l'audiovisuel ; <br/>
<br/>
              - Me Hazan, avocat au Conseil d'Etat et à la Cour de cassation, avocat des présidents des groupes parlementaires Les Républicains et Union des démocrates et indépendants ;<br/>
              et à l'issue de laquelle le juge des référés a clos l'instruction ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ". <br/>
<br/>
              2. Aux termes de l'article L. 167-1 du code électoral : " I. - Les partis et groupements peuvent utiliser les antennes du service public de radiodiffusion et de télévision pour leur campagne en vue des élections législatives. Chaque émission est diffusée par les sociétés nationales de télévision et de radiodiffusion sonore. / II. - Pour le premier tour de scrutin, une durée d'émission de trois heures est mise à la disposition des partis et groupements représentés par des groupes parlementaires de l'Assemblée nationale. / Cette durée est divisée en deux séries égales, l'une étant affectée aux groupes qui appartiennent à la majorité, l'autre à ceux qui ne lui appartiennent pas. / Le temps attribué à chaque groupement ou parti dans le cadre de chacune de ces séries d'émissions est déterminé par accord entre les présidents des groupes intéressés. A défaut d'accord amiable, la répartition est fixée par les membres composant le bureau de l'Assemblée nationale sortante, en tenant compte notamment de l'importance respective de ces groupes ; pour cette délibération, le bureau est complété par les présidents de groupe. / Les émissions précédant le deuxième tour de scrutin ont une durée d'une heure trente : elles sont réparties entre les mêmes partis et groupements et selon les mêmes proportions. / III. - Tout parti ou groupement politique qui n'est pas représenté par des groupes parlementaires de l'Assemblée nationale a accès, à sa demande, aux émissions du service public de la communication audiovisuelle pour une durée de sept minutes au premier tour et de cinq minutes au second, dès lors qu'au moins soixante-quinze candidats ont indiqué, dans leur déclaration de candidature, s'y rattacher pour l'application de la procédure prévue par le deuxième alinéa de l'article 9 de la loi n° 88-277 du 11 mars 1988 relative à la transparence financière de la vie politique. / L'habilitation est donnée à ces partis ou groupements dans des conditions qui seront fixées par décret. / IV. - Les conditions de production, de programmation et de diffusion des émissions sont fixées, après consultation des conseils d'administration des sociétés nationales de télévision et de radiodiffusion, par le conseil supérieur de l'audiovisuel. / V. - En ce qui concerne les émissions destinées à être reçues hors métropole, le conseil supérieur de l'audiovisuel tient compte des délais d'acheminement et des différences d'heures. / VI.- Les dépenses liées à la campagne audiovisuelle officielle sont à la charge de l'Etat. ". <br/>
<br/>
              3. Le 23 mai 2017, le Conseil supérieur de l'audiovisuel (CSA) a, sur le fondement de ces dispositions législatives, pris une décision " fixant la durée des émissions de la campagne électorale en vue des élections législatives de juin 2017 ", qui rappelle la durée totale d'émission allouée à chaque parti et précise le nombre et la durée des émissions et le nombre maximal d'émissions originales. L'article 1er de cette décision concerne les émissions prévues au II de l'article L. 167-1 du code électoral. Il mentionne, conformément à la décision des présidents des groupes parlementaires de l'Assemblée nationale, une durée totale d'émission de 80 minutes au premier tour et 40 minutes au second pour le Parti socialiste, de 69 minutes et 19 secondes au premier tour et 34 minutes et 40 secondes au second pour Les Républicains, de 5 minutes et 5 secondes au premier tour et 7 minutes 32 secondes au second pour l'Union des démocrates et indépendants, de 10 minutes au premier tour et 5 minutes au second pour le Parti radical de gauche et de 5 minutes et 36 secondes au premier tour et 2 minutes et 48 secondes au second pour le Parti communiste français. L'article 2 de la décision mentionne, quant à lui, une durée totale d'émission de 7 minutes au premier tour et de 5 minutes au second pour chacun des partis ou groupements politiques habilités en application des dispositions du III de l'article L. 167-1. <br/>
<br/>
              4. L'association " En marche ! ", qui, relevant de l'article 2 de la décision, peut seulement prétendre à une durée totale d'émission de 7 minutes au premier tour et 5 minutes au second, soutient que ces durées, alors même qu'elles ont été fixées conformément aux dispositions de l'article L. 167-1 du code électoral, portent une atteinte grave et manifestement illégale à plusieurs libertés fondamentales. Elle demande au Conseil d'Etat d'ordonner, sur le fondement de l'article L. 521-2 précité du code de justice administrative, des mesures propres à mettre fin à cette atteinte. A l'appui de cette demande, elle soulève notamment une question prioritaire de constitutionnalité dirigée contre l'article L. 167-1 du code électoral. <br/>
<br/>
              5. L'atteinte alléguée par l'association requérante résulterait de la décision du Conseil supérieur de l'audiovisuel, autorité compétente, en vertu des dispositions du IV de l'article L. 167-1 du code électoral, pour définir les conditions de programmation et de diffusion des émissions de la campagne électorale en vue des élections législatives, de faire application des dispositions des II et III du même article, alors qu'elles seraient contraires à des normes supérieures à la loi. Une telle décision revêt un caractère administratif. Dans ces conditions, contrairement à ce que soutiennent les présidents des groupes parlementaires Les Républicains et l'Union des démocrates et indépendants, la requête ressortit à la compétence de la juridiction administrative.<br/>
<br/>
              Sur la question prioritaire de constitutionnalité :<br/>
<br/>
              6. Aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...) ". Il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances et que la question soit nouvelle ou présente un caractère sérieux. <br/>
<br/>
              7. D'une part, les dispositions de l'article L. 167-1 du code électoral sont applicables au litige et n'ont pas été déclarées conformes à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel. D'autre part, les moyens tirés de ce que ces dispositions, compte tenu notamment des évolutions intervenues dans les circonstances de droit et de fait depuis la date de leur édiction, sont susceptibles de porter atteinte à l'expression pluraliste des opinions et à la participation équitable des partis et groupements politiques à la vie démocratique de la Nation, garanties par le troisième alinéa de l'article 4 de la Constitution, au principe d'égalité devant le suffrage découlant de l'article 3 de la Constitution et de l'article 6 de la Déclaration des droits de l'homme et du citoyen de 1789 et à la liberté d'expression garantie par l'article 11 de cette déclaration présentent un caractère sérieux. Dans ces conditions, il y a lieu de transmettre au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par l'association requérante.<br/>
<br/>
              Sur la requête en référé :<br/>
<br/>
              8. L'article 23-3 de l'ordonnance du 7 novembre 1958 prévoit qu'une juridiction saisie d'une question prioritaire de constitutionnalité " peut prendre les mesures provisoires ou conservatoires nécessaires " et qu'elle peut statuer " sans attendre la décision relative à la question prioritaire de constitutionnalité si la loi ou le règlement prévoit qu'elle statue dans un délai déterminé ou en urgence ".<br/>
<br/>
              9. Eu égard à la date de la transmission de la question prioritaire de constitutionnalité au Conseil constitutionnel et au calendrier prévisionnel d'examen de cette question indiqué par le Conseil constitutionnel aux parties, qui est compatible avec la remise en cause des durées d'émission mentionnées dans la décision litigieuse, il n'y a lieu ni d'examiner immédiatement les autres moyens de la requête, tirés de la violation d'engagements internationaux de la France, ni d'ordonner une mesure conservatoire. Il est, dès lors, sursis à statuer sur le surplus de la requête en référé dans l'attente de la décision du Conseil constitutionnel.<br/>
<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : La question prioritaire de constitutionnalité soulevée par l'association " En marche ! " est transmise au Conseil constitutionnel.<br/>
Article 2 : Dans l'attente de la décision du Conseil constitutionnel, il est sursis à statuer sur le surplus de la requête de l'association " En marche ! ".<br/>
Article 3 : La présente ordonnance sera notifiée à l'association " En marche ! ", au Conseil supérieur de l'audiovisuel, au Premier ministre, au ministre d'Etat, ministre de l'intérieur, à la ministre de la culture, au président du groupe parlementaire socialiste, écologiste et républicain, au président du groupe parlementaire Les Républicains, au président du groupe parlementaire Radical, républicain, démocrate et progressiste, au président du groupe parlementaire Gauche démocrate et républicaine et au président du groupe parlementaire Union des démocrates et indépendants. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">28-005-02 ÉLECTIONS ET RÉFÉRENDUM. DISPOSITIONS GÉNÉRALES APPLICABLES AUX ÉLECTIONS. CAMPAGNE ET PROPAGANDE ÉLECTORALES. - CAMPAGNE OFFICIELLE - DÉCISION DU CSA FIXANT LA DURÉE DES ÉMISSIONS (ART. L. 167-1 DU CODE ÉLECTORAL) - RÉFÉRÉ-LIBERTÉ CONTRE CETTE DÉCISION ASSORTI D'UNE QPC CONTRE L'ARTICLE L. 167-1 DU CODE ÉLECTORAL - OFFICE DU JUGE DU RÉFÉRÉ-LIBERTÉ EN CAS DE RENVOI DE LA QUESTION AU CONSEIL CONSTITUTIONNEL [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">28-02-02 ÉLECTIONS ET RÉFÉRENDUM. ÉLECTIONS LÉGISLATIVES. OPÉRATIONS PRÉLIMINAIRES À L`ÉLECTION AUTRES QUE L`ENREGISTREMENT DES CANDIDATURES. - CAMPAGNE ÉLECTORALE - DÉCISION DU CSA FIXANT LA DURÉE DES ÉMISSIONS (ART. L. 167-1 DU CODE ÉLECTORAL) - RÉFÉRÉ-LIBERTÉ CONTRE CETTE DÉCISION ASSORTI D'UNE QPC CONTRE L'ARTICLE L. 167-1 DU CODE ÉLECTORAL - OFFICE DU JUGE DU RÉFÉRÉ-LIBERTÉ EN CAS DE RENVOI DE LA QUESTION AU CONSEIL CONSTITUTIONNEL [RJ1].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-035-03-04 PROCÉDURE. PROCÉDURES INSTITUÉES PAR LA LOI DU 30 JUIN 2000. RÉFÉRÉ TENDANT AU PRONONCÉ DE MESURES NÉCESSAIRES À LA SAUVEGARDE D'UNE LIBERTÉ FONDAMENTALE (ART. L. 521-2 DU CODE DE JUSTICE ADMINISTRATIVE). POUVOIRS ET DEVOIRS DU JUGE. - HYPOTHÈSE OÙ LE JUGE DES RÉFÉRÉS EST SAISI D'UNE QPC - OFFICE DU JUGE DU RÉFÉRÉ-LIBERTÉ EN CAS DE RENVOI DE LA QPC AU CONSEIL CONSTITUTIONNEL [RJ1] - MESURE CONSERVATOIRE - ABSENCE, COMPTE TENU  DE LA DATE DE TRANSMISSION DE LA QPC ET DU CALENDRIER D'EXAMEN DE CELLE-CI INDIQUÉ PAR LE CONSEIL CONSTITUTIONNEL AUX PARTIES - CONSÉQUENCE - SURSIS À STATUER, DANS L'ATTENTE DE LA DÉCISION DU CONSEIL CONSTITUTIONNEL.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">54-10 PROCÉDURE. - QPC POSÉE DEVANT LE JUGE DU RÉFÉRÉ-LIBERTÉ (ART. L. 521-2 DU CJA) - OFFICE EN CAS DE RENVOI DE LA QPC AU CONSEIL CONSTITUTIONNEL [RJ1] - MESURE CONSERVATOIRE - ABSENCE, COMPTE TENU  DE LA DATE DE TRANSMISSION DE LA QPC ET DU CALENDRIER D'EXAMEN DE CELLE-CI INDIQUÉ PAR LE CONSEIL CONSTITUTIONNEL AUX PARTIES - CONSÉQUENCE - SURSIS À STATUER, DANS L'ATTENTE DE LA DÉCISION DU CONSEIL CONSTITUTIONNEL.
</SCT>
<ANA ID="9A"> 28-005-02 Référé-liberté tendant notamment à la suspension de l'exécution de la décision du Conseil supérieur de l'audiovisuel (CSA) fixant la durée des émissions de la campagne en vue des élections législatives, assorti d'une question prioritaire de constitutionnalité (QPC) dirigée contre l'article L. 167-1 du code électoral.... ,,Eu égard à la date de la transmission de la QPC au Conseil constitutionnel et au calendrier prévisionnel d'examen de cette question indiqué par le Conseil constitutionnel aux parties, qui est compatible avec la remise en cause des durées d'émission mentionnées dans la décision litigieuse, il n'y a lieu ni d'examiner immédiatement les autres moyens de la requête, tirés de la violation d'engagements internationaux de la France, ni d'ordonner une mesure conservatoire sur le fondement de l'article 23-3 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel. Il est, dès lors, sursis à statuer sur le surplus de la requête en référé dans l'attente de la décision du Conseil constitutionnel.</ANA>
<ANA ID="9B"> 28-02-02 Référé-liberté tendant notamment à la suspension de l'exécution de la décision du Conseil supérieur de l'audiovisuel (CSA) fixant la durée des émissions de la campagne en vue des élections législatives, assorti d'une question prioritaire de constitutionnalité (QPC) dirigée contre l'article L. 167-1 du code électoral.... ,,Eu égard à la date de la transmission de la QPC au Conseil constitutionnel et au calendrier prévisionnel d'examen de cette question indiqué par le Conseil constitutionnel aux parties, qui est compatible avec la remise en cause des durées d'émission mentionnées dans la décision litigieuse, il n'y a lieu ni d'examiner immédiatement les autres moyens de la requête, tirés de la violation d'engagements internationaux de la France, ni d'ordonner une mesure conservatoire sur le fondement de l'article 23-3 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel. Il est, dès lors, sursis à statuer sur le surplus de la requête en référé dans l'attente de la décision du Conseil constitutionnel.</ANA>
<ANA ID="9C"> 54-035-03-04 Référé-liberté tendant notamment à la suspension de l'exécution de la décision du Conseil supérieur de l'audiovisuel (CSA) fixant la durée des émissions de la campagne en vue des élections législatives, assorti d'une question prioritaire de constitutionnalité (QPC) dirigée contre l'article L. 167-1 du code électoral.... ,,Eu égard à la date de la transmission de la QPC au Conseil constitutionnel et au calendrier prévisionnel d'examen de cette question indiqué par le Conseil constitutionnel aux parties, qui est compatible avec la remise en cause des durées d'émission mentionnées dans la décision litigieuse, il n'y a lieu ni d'examiner immédiatement les autres moyens de la requête, tirés de la violation d'engagements internationaux de la France, ni d'ordonner une mesure conservatoire sur le fondement de l'article 23-3 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel. Il est, dès lors, sursis à statuer sur le surplus de la requête en référé dans l'attente de la décision du Conseil constitutionnel.</ANA>
<ANA ID="9D"> 54-10 Référé-liberté tendant notamment à la suspension de l'exécution de la décision du Conseil supérieur de l'audiovisuel (CSA) fixant la durée des émissions de la campagne en vue des élections législatives, assorti d'une question prioritaire de constitutionnalité (QPC) dirigée contre l'article L. 167-1 du code électoral.... ,,Eu égard à la date de la transmission de la QPC au Conseil constitutionnel et au calendrier prévisionnel d'examen de cette question indiqué par le Conseil constitutionnel aux parties, qui est compatible avec la remise en cause des durées d'émission mentionnées dans la décision litigieuse, il n'y a lieu ni d'examiner immédiatement les autres moyens de la requête, tirés de la violation d'engagements internationaux de la France, ni d'ordonner une mesure conservatoire sur le fondement de l'article 23-3 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel. Il est, dès lors, sursis à statuer sur le surplus de la requête en référé dans l'attente de la décision du Conseil constitutionnel.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Section, 11 décembre 2015,,c/ Premier ministre, n° 395009, p. 438.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
