<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028411797</ID>
<ANCIEN_ID>JG_L_2013_12_000000352901</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/41/17/CETATEXT000028411797.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 30/12/2013, 352901</TITRE>
<DATE_DEC>2013-12-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>352901</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, BLANCPAIN, SOLTNER ; SCP GATINEAU, FATTACCINI</AVOCATS>
<RAPPORTEUR>M. Pascal Trouilly</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:352901.20131230</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 23 septembre et 21 octobre 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour l'Union des syndicats de l'immobilier (UNIS) dont le siège est 60, rue Saint-Lazare à Paris (75009), représentée par son président ; l'UNIS demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté du ministre du travail, de l'emploi et de la santé du 13 juillet 2011 portant extension d'avenants à la convention collective nationale de l'immobilier ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le traité sur le fonctionnement de l'Union européenne, notamment son article 267 ;<br/>
<br/>
              Vu le code de la sécurité sociale ; <br/>
<br/>
              Vu le code du travail ; <br/>
<br/>
              Vu la décision n° 2013-672 DC du 13 juin 2013 du Conseil constitutionnel ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Pascal Trouilly, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Blancpain, Soltner, avocat de l'Union des syndicats de l'immobilier et à la SCP Gatineau, Fattaccini, avocat de la Fédération des sociétés immobilières et foncières  et autres ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'en vertu de l'article L. 911-1 du code de la  sécurité sociale, les " garanties collectives dont bénéficient les salariés ", qui ont notamment pour objet, aux termes de l'article L. 911-2 du même code, de prévoir " la couverture (...) des risques portant atteinte à l'intégrité physique de la personne ou liés à la maternité " en complément de celles qui résultent de l'organisation de la sécurité sociale, peuvent notamment être déterminées par voie de conventions ou d'accords collectifs ; qu'en vertu de l'article L. 911-3 du même code, ces accords peuvent être étendus ; que la légalité d'un arrêté ministériel prononçant l'extension d'un accord collectif relatif à un régime de prévoyance complémentaire des salariés ou d'un avenant à celui-ci est nécessairement subordonnée à la validité de la  convention ou de l'avenant en cause ;<br/>
<br/>
              2. Considérant que, par l'arrêté du 13 juillet 2011 dont l'Union des syndicats de l'immobilier (UNIS) demande l'annulation, le ministre du travail, de l'emploi et de la santé a étendu l'avenant n° 48 du 23 novembre 2010 et les avenants n° 49 et 50 du 17 mai 2011 à la convention collective nationale de l'immobilier, qui instituent pour l'ensemble des salariés de la branche un régime obligatoire de prévoyance couvrant les risques décès, incapacité de travail et invalidité et un régime obligatoire de remboursement de frais de santé ; que par son article 17, l'avenant n° 48 désigne, pour une période de trois ans, l'Institution de prévoyance de groupe Mornay (IPGM) en tant qu'unique organisme assureur des garanties de ces deux régimes ;<br/>
<br/>
              Sur la compétence du signataire de l'arrêté :<br/>
<br/>
              3. Considérant qu'aux termes de l'article L. 911-3 du code de la sécurité sociale : " Les dispositions du titre III du livre Ier du code du travail sont applicables aux conventions et accords collectifs mentionnés à l'article L. 911-1. Toutefois, lorsque les accords ont pour objet exclusif la détermination des garanties mentionnées à l'article L. 911-2, leur extension aux salariés, aux anciens salariés, à leurs ayants droit et aux employeurs compris dans leur champ d'application est décidée par arrêté du ministre chargé de la sécurité sociale et du ministre chargé du budget, après avis motivé d'une commission dont la composition est fixée par décret " ; que les avenants n° 48, 49 et 50, étendus par l'arrêté attaqué, ont été conclus dans le cadre de la  convention collective nationale de l'immobilier du 9 septembre 1988, dont ils modifient l'article 26, et créent une annexe à cette convention, consacrée aux " régimes de prévoyance et de remboursement de frais de santé " ; qu'ainsi, les avenants en cause, qui s'incorporent à la convention collective nationale, n'ont pas pour " objet exclusif " la détermination des garanties mentionnées à l'article L. 911-2 ; que, dès lors, le ministre chargé du travail avait compétence pour décider de leur extension, alors même que la commission des accords de retraite et de prévoyance mentionnée à l'article L. 911-3 a été saisie préalablement pour avis, à titre facultatif ; que, par suite, le moyen tiré de l'incompétence du signataire de l'arrêté attaqué doit être écarté ; <br/>
<br/>
              Sur les conditions de conclusion des avenants : <br/>
<br/>
              En ce qui concerne la représentativité des organisations d'employeurs signataires : <br/>
<br/>
              4. Considérant qu'aux termes de l'article L. 2261-19 du code du travail : " Pour pouvoir être étendus, la convention de branche ou l'accord professionnel ou interprofessionnel, leurs avenants ou annexes, doivent avoir été négociés et conclus en commission paritaire. / Cette commission est composée de représentants des organisations syndicales d'employeurs et de salariés représentatives dans le champ d'application considéré " ; qu'il résulte de ces dispositions qu'un accord ne peut être étendu à l'ensemble des entreprises comprises dans son champ d'application que s'il a été négocié et conclu par les organisations représentatives dans ce champ d'application ; que la circonstance qu'un accord a été en outre signé par une ou des organisations non représentatives ne fait pas, en revanche, légalement obstacle à son extension ; <br/>
<br/>
              5. Considérant que l'UNIS soutient qu'aucune des trois organisations d'employeurs signataires des avenants n° 48, 49 et 50, à savoir la Fédération des sociétés immobilières et foncières (FSIF), le Syndicat national des résidences de tourisme (SNRT) et le Syndicat national des professionnels immobiliers (SNPI), n'est représentative dans la branche de l'immobilier ; <br/>
<br/>
              6. Considérant qu'aux termes de l'article L. 2121-1 du code du travail : " La représentativité des organisations syndicales est déterminée d'après les critères cumulatifs suivants : 1° Le respect des valeurs républicaines ; 2° L'indépendance ; 3° La transparence financière ; 4° Une ancienneté minimale de deux ans dans le champ professionnel et géographique couvrant le niveau de négociation. (....) ; 5° L'audience établie selon les niveaux de négociation (...) ; 6° L'influence, prioritairement caractérisée par l'activité et l'expérience ; 7° Les effectifs d'adhérents et les cotisations " ; que ces dispositions s'appliquent tant aux organisations d'employeurs qu'aux syndicats de salariés ; que si l'article L. 2121-2 du code du travail prévoit que " S'il y a lieu de déterminer la représentativité d'un syndicat ou d'une organisation professionnelle autre que ceux affiliés à l'une des organisations représentatives au niveau national, l'autorité administrative diligente une enquête. / L'organisation intéressée fournit les éléments d'appréciation dont elle dispose", la circonstance qu'aucune demande d'enquête de représentativité n'ait été formée avant l'adoption d'un arrêté d'extension n'est pas, contrairement à ce que soutient le ministre chargé du travail, de nature à faire obstacle à la contestation, à l'appui d'un recours tendant à l'annulation pour excès de pouvoir d'un tel arrêté, de la représentativité d'une organisation ayant conclu une convention collective ou un accord collectif, ni à faire présumer de cette représentativité ; <br/>
<br/>
              7. Considérant que si le Syndicat national des professionnels immobiliers (SNPI) compte un représentant dans le collège adhérent du conseil d'administration d'une institution de prévoyance, cette participation, conforme aux dispositions de l'article R. 931-3-1 du code de la sécurité sociale, ne peut, à elle seule, être regardée comme l'empêchant d'assurer effectivement la défense des intérêts professionnels qu'il entend représenter et, par suite, comme le privant d'indépendance ; qu'eu égard au nombre des entreprises qui y sont adhérentes et au nombre de salariés de ces entreprises, rapportés au nombre total des entreprises et des salariés de la branche de l'immobilier, ainsi que de l'activité et de l'expérience de cette organisation professionnelle, et alors qu'il n'est pas soutenu que les autres critères de représentativité ne seraient pas satisfaits, le moyen tiré de ce que le SNPI ne pourrait être regardé comme une organisation représentative de la branche de l'immobilier doit être écarté ; que, par suite, la requérante n'est pas fondée à soutenir qu'aucune des organisations d'employeurs signataires des avenants n° 48, 49 et 50 ne serait  représentative dans la branche de l'immobilier ;<br/>
<br/>
              En ce qui concerne les conditions de négociation  des avenants :<br/>
<br/>
              8. Considérant que, conformément aux dispositions de l'article L. 2261-19 du code du travail, les avenants litigieux ont été négociés et conclus au cours de plusieurs séances de la commission paritaire auxquelles toutes les organisations syndicales ont été convoquées ; que les principales caractéristiques des régimes complémentaires de prévoyance et de santé envisagés ont été exposées au cours de ces réunions et les différentes versions successives des projets d'avenant discutées ; que, dans ces conditions, ni la circonstance que le SNPI aurait pris contact de manière unilatérale, avant le début de la négociation des avenants en commission paritaire, avec l'institution de prévoyance dont il était déjà adhérent et qu'il a proposé ensuite aux autres organisations représentatives de désigner cette institution en tant que gestionnaire unique de régime de prévoyance, sans signaler qu'il en était déjà adhérent, ni celle que certaines des questions des autres organisations n'auraient pas reçu de réponse et qu'un exemplaire du contrat d'adhésion à cette institution n'aurait pas été présenté lors de cette négociation n'étaient de nature, en l'espèce, à faire légalement obstacle à l'extension de ces avenants ;  <br/>
<br/>
              Sur le moyen tiré de ce que le ministre ne pouvait légalement étendre un accord en l'absence de mesures transitoires permettant l'adaptation ou la dénonciation de contrats de prévoyance déjà souscrits par certaines entreprises de la branche : <br/>
<br/>
              9. Considérant qu'en vertu de l'article L. 912-1 du code de la sécurité sociale, en vigueur à la date de l'arrêté d'extension : " Lorsque les accords professionnels ou interprofessionnels mentionnés à l'article L. 911-1 prévoient une mutualisation des risques dont ils organisent la couverture auprès d'un ou plusieurs organismes mentionnés à l'article 1er de la loi n° 89-1009 du 31 décembre 1989 renforçant les garanties offertes aux personnes assurées contre certains risques ou d'une ou plusieurs institutions mentionnées à l'article L. 370-1 du code des assurances, auxquels adhèrent alors obligatoirement les entreprises relevant du champ d'application de ces accords " et qu'ils " s'appliquent à une entreprise qui, antérieurement à leur date d'effet, a adhéré ou souscrit un contrat auprès d'un organisme différent de celui prévu par les accords pour garantir les mêmes risques à un niveau équivalent, les dispositions du second alinéa de l'article L. 132-23 du code du travail sont applicables " ; que selon cet article, devenu l'article L. 2253-2 du code du travail : " Lorsqu'une convention de branche ou un accord professionnel ou interprofessionnel vient à s'appliquer dans l'entreprise postérieurement à la conclusion de conventions ou d'accords d'entreprise ou d'établissement négociés conformément au présent livre, les stipulations de ces derniers sont adaptées en conséquence " ; qu'en vertu de l'article 18 de l'accord annexé à la convention collective nationale de l'immobilier, dans sa rédaction issue de l'avenant n° 49, les entreprises relevant du champ d'application de cette convention ou y ayant adhéré ont l'obligation d'adhérer, à compter du premier jour du mois suivant la publication de l'arrêté d'extension, aux contrats de base obligatoire de prévoyance et de frais de santé proposés par l'organisme assureur désigné, en application de l'article L. 912-1 du code de la sécurité sociale, à l'article 17 de l'accord, à savoir l'institution de prévoyance de groupe Mornay (IPGM) ; que, par exception, les entreprises ayant souscrit, antérieurement à cette même date, un contrat de prévoyance ou de remboursement de frais de santé effectif au profit de l'ensemble de leur personnel, couvrant les mêmes risques à un niveau strictement supérieur, peuvent ne pas rejoindre l'organisme assureur désigné à l'article 17 ; que ces stipulations conduisent ainsi les entreprises ayant déjà souscrit un contrat de prévoyance ou de santé ne prévoyant pas pour l'ensemble des salariés de garanties d'un niveau strictement supérieur, soit à dénoncer ce contrat, sauf à payer pour leurs salariés une double cotisation, soit à renégocier le contrat, avant le premier jour suivant la publication de l'arrêté d'extension, afin de prévoir un niveau de protection supérieur à celui offert par l'organisme désigné ;<br/>
<br/>
              10. Considérant que l'UNIS soutient que le ministre ne pouvait légalement procéder, eu égard au principe de sécurité juridique, à l'extension des avenants n° 48 et 49, faute pour ceux-ci de prévoir les mesures transitoires nécessaires à l'adaptation ou à la dénonciation des contrats en cours ; que, toutefois, il ressort des pièces du dossier que les organisations d'employeurs ont pu informer les entreprises de la branche, dès la signature de l'avenant n° 49, le 17 mars 2011, des obligations qui seraient mises à leur charge à compter du premier jour du mois suivant la publication de l'arrêté d'extension ; que cet arrêté d'extension a été publié au Journal officiel de la République française le 23 juillet 2011 ; que, dans ces conditions, le principe de sécurité juridique n'a pas été méconnu ;  <br/>
<br/>
              Sur le moyen tiré de ce que le champ d'application de l'accord recouperait celui de la convention collective nationale de retraite et de prévoyance des cadres du 14 mars 1947 :<br/>
<br/>
              11. Considérant que le ministre chargé du travail, saisi d'une demande d'extension d'un accord de prévoyance collective, doit rechercher si le champ d'application professionnel de cet accord n'est pas compris dans le champ d'une autre convention ou accord collectif étendu par arrêté, compte tenu, le cas échéant, de l'objet respectif des stipulations étendues ou à étendre ; que, lorsqu'il apparaît que les champs d'application définis par les textes en  cause se recoupent, il lui appartient, préalablement à l'extension projetée, soit d'exclure du champ de l'extension envisagée les activités économiques ou les catégories de salariés déjà couvertes par la convention ou l'accord collectif précédemment étendu, soit d'abroger l'arrêté d'extension de cette convention ou de cet accord collectif, en tant qu'il s'applique à ces activités ou ces catégories ;<br/>
<br/>
              12. Considérant que l'article 7 de la convention collective nationale de retraite et de prévoyance des cadres du 14 mars 1947 comporte des clauses spécifiques pour les voyageurs représentants placiers en matière de prévoyance, en particulier, s'agissant de ceux dont la rémunération dépasse le plafond de la sécurité sociale mais qui n'ont pas le statut de cadre, la désignation d'un organisme, l'Institution nationale de prévoyance des représentants, auquel des cotisations doivent obligatoirement être versées pour la constitution d'avantages en matière de prévoyance ; que l'avenant n° 48, dont il ressort clairement de ses clauses qu'il inclut dans son champ d'application les voyageurs représentants placiers, prévoit le versement des cotisations du régime de prévoyance complémentaire qu'il instaure à l'Institution de prévoyance de groupe Mornay ; que, dans ces conditions, l'UNIS est fondée à soutenir que le ministre du travail ne pouvait étendre l'avenant n° 48 sans exclure de son champ d'application les voyageurs représentants placiers entrant dans le champ de l'article 7 de la convention collective nationale de retraite et de prévoyance des cadres du 14 mars 1947 ; que l'arrêté attaqué doit être, dans cette mesure, annulé ;<br/>
<br/>
              Sur le moyen tiré de ce que l'organisme gestionnaire du régime de prévoyance a été choisi sans mise en concurrence préalable :<br/>
<br/>
              13. Considérant que si, contrairement à ce que soutient l'UNIS, aucune règle ni aucun principe de droit interne n'imposait que la désignation de l'IGPM en tant qu'unique organisme gestionnaire des régimes institués par les avenants litigieux soit précédée d'un appel d'offre, l'UNIS invoque également la méconnaissance de l'obligation de transparence, qui découle du droit de l'Union européenne ; qu'en particulier, par son arrêt du 3 juin 2010 rendu dans l'affaire C-203/08, la Cour de justice de l'Union européenne a qualifié l'obligation de transparence de condition préalable obligatoire du droit d'un Etat membre d'attribuer à un opérateur le droit exclusif d'exercer une activité économique, quel que soit le mode de sélection de cet opérateur ; que l'IPGM, bien que n'ayant pas de but lucratif et agissant sur le fondement du principe de solidarité, doit être regardée comme une entreprise exerçant une activité économique, qui a été choisie par les partenaires sociaux parmi d'autres entreprises avec lesquelles elle est en concurrence sur le marché des services de prévoyance qu'elle propose ; que la réponse au moyen soulevé dépend de la question de savoir si le respect de cette obligation de transparence est une condition préalable obligatoire à l'extension, par un Etat membre, à l'ensemble des entreprises d'une branche, d'un accord collectif confiant à un unique opérateur, choisi par les partenaires sociaux, la gestion d'un régime de prévoyance complémentaire obligatoire institué au profit des salariés ; que cette question est déterminante pour la solution du litige que doit trancher le Conseil d'Etat ; qu'elle présente une difficulté sérieuse ; qu'il y a lieu, par suite, d'en saisir la Cour de justice de l'Union européenne en application de l'article 267 du traité sur le fonctionnement de l'Union européenne et, jusqu'à ce que celle-ci se soit prononcée, de surseoir à statuer sur le surplus des conclusions de la requête de l'UNIS ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêté du ministre du travail, de l'emploi et de la santé du 13 juillet 2011 est annulé en tant qu'il étend l'avenant n° 48 à la convention collective nationale de l'immobilier sans exclure du champ de cette extension les voyageurs représentants placiers entrant dans le champ de l'article 7 de la convention collective nationale de retraite et de prévoyance des cadres du 14 mars 1947.<br/>
Article 2 : Il est sursis à statuer sur le surplus des conclusions de la requête présentée par l'Union des syndicats de l'immobilier et sur les conclusions des autres parties présentées au titre de l'article L. 761-1 du code de justice administrative, jusqu'à ce que la Cour de justice de l'Union européenne se soit prononcée sur la question suivante : le respect de l'obligation de transparence qui découle de l'article 56 du traité sur le fonctionnement de l'Union européenne est-il une condition préalable obligatoire à l'extension, par un Etat membre, à l'ensemble des entreprises d'une branche, d'un accord collectif confiant à un unique opérateur, choisi par les partenaires sociaux, la gestion d'un régime de prévoyance complémentaire obligatoire institué au profit des salariés '  <br/>
Article 3 : La présente décision sera notifiée à l'Union des syndicats de l'immobilier (UNIS), au ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social, au Syndicat national des résidences de tourisme (SNRT) et au président de la Cour de justice de l'Union européenne. Les autres défendeurs représentés devant le Conseil d'Etat par la SCP Gatineau, Fattaccini, avocat au Conseil d'Etat et à la Cour de cassation, seront informés de la présente décision par celle-ci.<br/>
Copie en sera adressée au Premier ministre, à la Fédération des entreprises publiques locales, à la Fédération nationale des agents immobiliers, au Syndicat CFTC des commerces, du service et de la force de vente, à la Fédération des personnels du commerce, de la distribution et des services CGT et à la Fédération des services CFDT.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-04-03-07 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. PRINCIPES GÉNÉRAUX DU DROIT. PRINCIPES INTÉRESSANT L'ACTION ADMINISTRATIVE. - PRINCIPE DE SÉCURITÉ JURIDIQUE - OBLIGATION POUR L'AUTORITÉ INVESTIE DU POUVOIR RÉGLEMENTAIRE D'ÉDICTER, POUR DES MOTIFS DE SÉCURITÉ JURIDIQUE, LES MESURES TRANSITOIRES QU'IMPLIQUE, S'IL Y A LIEU, UNE RÉGLEMENTATION NOUVELLE - APPLICATION À L'ARRÊTÉ D'EXTENSION D'UN ACCORD COLLECTIF - LÉGALITÉ DE L'ARRÊTÉ SUBORDONNÉE AU RESPECT, PAR LES STIPULATIONS DE L'ACCORD, DU PRINCIPE DE SÉCURITÉ JURIDIQUE - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">66-02-02-035 TRAVAIL ET EMPLOI. CONVENTIONS COLLECTIVES. EXTENSION DES CONVENTIONS COLLECTIVES. CONDITION DE LÉGALITÉ DE L'EXTENSION TENANT À LA VALIDITÉ DE LA CONVENTION. - PRINCIPE DE SÉCURITÉ JURIDIQUE - OBLIGATION POUR L'AUTORITÉ INVESTIE DU POUVOIR RÉGLEMENTAIRE D'ÉDICTER, POUR DES MOTIFS DE SÉCURITÉ JURIDIQUE, LES MESURES TRANSITOIRES QU'IMPLIQUE, S'IL Y A LIEU, UNE RÉGLEMENTATION NOUVELLE - CONSÉQUENCE - LÉGALITÉ DE L'ARRÊTÉ SUBORDONNÉE AU RESPECT, PAR LES STIPULATIONS DE L'ACCORD, DU PRINCIPE DE SÉCURITÉ JURIDIQUE - EXISTENCE.
</SCT>
<ANA ID="9A"> 01-04-03-07 Il peut être utilement soutenu, à l'appui d'un recours pour excès de pouvoir dirigé contre l'arrêté d'extension d'un accord collectif, que l'accord ne comporte pas en lui-même les mesures transitoires qu'impliquent, pour des motifs de sécurité juridique, ses stipulations.</ANA>
<ANA ID="9B"> 66-02-02-035 Il peut être utilement soutenu, à l'appui d'un recours pour excès de pouvoir dirigé contre l'arrêté d'extension d'un accord collectif, que l'accord ne comporte pas en lui-même les mesures transitoires qu'impliquent, pour des motifs de sécurité juridique, ses stipulations.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
