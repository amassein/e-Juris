<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030459154</ID>
<ANCIEN_ID>JG_L_2015_03_000000387322</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/45/91/CETATEXT000030459154.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème SSR, 30/03/2015, 387322</TITRE>
<DATE_DEC>2015-03-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>387322</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème SSR</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN ; FOUSSARD ; BOUTHORS</AVOCATS>
<RAPPORTEUR>Mme Angélique Delorme</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2015:387322.20150330</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<p>Vu la procédure suivante :<br clear="none"/>
<br clear="none"/>
M. A...B...et la société civile immobilière Tour Triangle ont, chacun, saisi le tribunal administratif de Paris d'une demande et le préfet de la région Ile-de-France, préfet de Paris, d'un déféré tendant à l'annulation de la décision du 17 novembre 2014 par laquelle le Conseil de Paris a refusé d'adopter le projet de délibération n° 2014DU1117 relatif au " déclassement de l'assiette de la Tour Triangle et signature d'une promesse de bail et d'un bail à construction relatif à la réalisation de la Tour Triangle ".<br clear="none"/>
<br clear="none"/>
A l'appui des observations qu'elle a produites dans ces instances,<br clear="none"/>
Mme C...D... a demandé, par un mémoire distinct enregistré le<br clear="none"/>
19 décembre 2014, en application de l'article 23-1 de l'ordonnance n° 58-1067 du 7 novembre 1958, que soit transmise au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des deuxième et troisième alinéas de l'article L. 2121-21 du code général des collectivités territoriales.<br clear="none"/>
<br clear="none"/>
Par une ordonnance n° 1428029, 1429131, 1430471/2-1 du 21 janvier 2015, enregistrée le 22 janvier 2015 au secrétariat du contentieux du Conseil d'Etat, le tribunal administratif de Paris a transmis au Conseil d'Etat, en application de l'article 23-2 de l'ordonnance du 7 novembre 1958, la question prioritaire de constitutionnalité soulevée par Mme D... .<br clear="none"/>
<br clear="none"/>
Dans la question prioritaire de constitutionnalité transmise et dans ses nouveaux mémoires, enregistrés au secrétariat du contentieux du Conseil d'Etat les 25 février et 5 mars 2015, Mme D... soutient que les deuxième et troisième alinéas de l'article L. 2121-21 du code général des collectivités territoriales, applicables au litige, méconnaissent les principes constitutionnels d'égalité et d'égalité devant la loi, garantis par les articles 1er et 6 de la Déclaration des droits de l'homme et du citoyen, et le principe constitutionnel selon lequel la société a le droit de demander à tout agent public de rendre des comptes de son administration, garanti par l'article 15 de cette même Déclaration.<br clear="none"/>
<br clear="none"/>
<br clear="none"/>
Vu les autres pièces du dossier ;<br clear="none"/>
<br clear="none"/>
Vu :<br clear="none"/>
- la Constitution, notamment son Préambule et son article 61-1 ;<br clear="none"/>
- l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br clear="none"/>
- le code général des collectivités territoriales ;<br clear="none"/>
- le code de justice administrative ;<br clear="none"/>
<br clear="none"/>
Après avoir entendu en séance publique :<br clear="none"/>
<br clear="none"/>
- le rapport de Mme Angélique Delorme, auditeur,<br clear="none"/>
<br clear="none"/>
- les conclusions de M. Vincent Daumas, rapporteur public ;<br clear="none"/>
<br clear="none"/>
La parole ayant été donnée, avant et après les conclusions, à Me Bouthors, avocat de Mme C...D...-morizet, la SCP Waquet, Farge, Hazan, avocat de M. A...B...et à Me Foussard, avocat de la Ville de Paris ;<br clear="none"/>
<br clear="none"/>
</p>
<p>1. Considérant qu'il résulte des dispositions de l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que, lorsqu'une juridiction relevant du Conseil d'Etat a transmis à ce dernier, en application de l'article 23-2 de cette même ordonnance, la question de la conformité à la Constitution d'une disposition législative, le Conseil constitutionnel est saisi de cette question de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ;<br clear="none"/>
<br clear="none"/>
2. Considérant que, eu égard aux conséquences susceptibles d'en résulter quant au règlement du litige tel que déterminé par les conclusions des parties, seules ces dernières sont recevables à soulever une question prioritaire de constitutionnalité ; que doit être regardée comme une partie à l'instance, ayant à ce titre qualité pour soulever une telle question, la personne qui a été invitée par la juridiction à présenter des observations et qui, si elle ne l'avait pas été, aurait eu qualité pour former tierce opposition contre cette décision ;<br clear="none"/>
<br clear="none"/>
3. Considérant qu'il ressort du dossier transmis par le tribunal administratif de Paris qu'afin d'assurer le caractère contradictoire de la procédure, Mme D... , présidente du groupe UMP au conseil de Paris a été invitée, comme les autres présidents de groupe, à présenter des observations sur les demandes et le déféré préfectoral tendant à l'annulation d'une décision par laquelle le Conseil de Paris a refusé d'adopter un projet de délibération que lui soumettait l'exécutif de cette collectivité territoriale, relatif au déclassement de l'assiette du projet de " Tour Triangle " et à la signature d'une promesse de bail et d'un bail à construction permettant la réalisation de ce projet ; que, dans les circonstances de l'espèce et alors que Mme D... s'était exprimée en sa qualité de présidente de groupe, dans le débat ayant précédé le vote sur ce projet de délibération, contre la position défendue par le maire et en faveur du rejet de projet, elle aurait qualité pour former tierce opposition contre un jugement qui annulerait la décision en cause sans qu'elle ait été présente ni représentée à l'instance ; qu'elle doit, dès lors, être regardée comme ayant la qualité de défendeur dans l'instance ayant donné lieu à la présente question prioritaire de constitutionnalité ; que, par suite, cette question est recevable ;<br clear="none"/>
<br clear="none"/>
4. Considérant qu'aux termes des trois premiers alinéas de l'article L. 2121-21 du code général des collectivités territoriales, relatif au fonctionnement du conseil municipal : " Le vote a lieu au scrutin public à la demande du quart des membres présents. Le registre des délibérations comporte le nom des votants et l'indication du sens de leur vote. / Il est voté au scrutin secret : / 1° Soit lorsqu'un tiers des membres présents le réclame ; (...) " ;<br clear="none"/>
<br clear="none"/>
5. Considérant que les dispositions des deuxième et troisième alinéas de l'article L. 2121-21 du code général des collectivités territoriales sont applicables au litige dont est saisi le tribunal administratif de Paris ; qu'elles n'ont pas déjà été déclarées conformes à la Constitution par le Conseil constitutionnel ; que le moyen tiré de ce qu'en prévoyant que le vote au scrutin secret est de droit lorsqu'un tiers des membres présents le réclame, ces dispositions méconnaîtraient le droit de demander des comptes aux agents publics énoncé à l'article 15 de la Déclaration des droits de l'homme et du citoyen soulève une question relative à la portée de cette disposition constitutionnelle qui, sans qu'il soit besoin pour le Conseil d'Etat d'examiner le caractère sérieux du moyen invoqué, sur ce fondement, à l'encontre des dispositions législatives en cause, doit être regardée comme nouvelle au sens de l'article 23-4 de l'ordonnance du 7 novembre 1958 ; qu'ainsi, il y a lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée ;<br clear="none"/>
<br clear="none"/>
<br clear="none"/>
<br clear="none"/>
<br clear="none"/>
D E C I D E :<br clear="none"/>
--------------<br clear="none"/>
Article 1er : La question de la conformité à la Constitution des deuxième et troisième alinéas de l'article L. 2121-21 du code général des collectivités territoriales est renvoyée au Conseil constitutionnel.<br clear="none"/>
Article 2 : La présente décision sera notifiée à Mme C...D... , à la Ville de Paris, à M. A...B..., au préfet de la région Ile-de-France, préfet de Paris, à la société Tour Triangle et au ministre de l'intérieur.<br clear="none"/>
Copie en sera adressée au Premier ministre et au tribunal administratif de Paris.<br clear="none"/>
</p>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-04-03 PROCÉDURE. INSTRUCTION. CARACTÈRE CONTRADICTOIRE DE LA PROCÉDURE. - PARTIE - NOTION - INCLUSION - PERSONNE QUI AURAIT EU QUALITÉ POUR FORMER TIERCE OPPOSITION SI ELLE N'AVAIT PAS ÉTÉ INVITÉE À PRÉSENTER DES OBSERVATIONS [RJ2].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-06-05-11 PROCÉDURE. JUGEMENTS. FRAIS ET DÉPENS. REMBOURSEMENT DES FRAIS NON COMPRIS DANS LES DÉPENS. - PARTIE - NOTION - INCLUSION - PERSONNE QUI AURAIT EU QUALITÉ POUR FORMER TIERCE OPPOSITION SI ELLE N'AVAIT PAS ÉTÉ INVITÉE À PRÉSENTER DES OBSERVATIONS [RJ2].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-08-04 PROCÉDURE. VOIES DE RECOURS. TIERCE-OPPOSITION. - PERSONNE QUI AURAIT EU QUALITÉ POUR FORMER TIERCE OPPOSITION SI ELLE N'AVAIT PAS ÉTÉ INVITÉE À PRÉSENTER DES OBSERVATIONS - QUALITÉ DE PARTIE - EXISTENCE [RJ2].
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">54-10-02 PROCÉDURE. - 1) QPC TRANSMISE AU CONSEIL D'ETAT - CONTRÔLE PAR LE CONSEIL D'ETAT DE LA RECEVABILITÉ DE LA QPC DEVANT LA JURIDICTION L'AYANT TRANSMISE - EXISTENCE (SOL. IMPL.) - 2) RECEVABILITÉ À SOULEVER UNE QPC RÉSERVÉE AUX SEULES PARTIES - OBSERVATEUR IRRECEVABLE À SOULEVER DE SA PROPRE INITIATIVE UNE QPC NON INVOQUÉE PAR L'UNE DES PARTIES [RJ1] - 3) PARTIE - NOTION - INCLUSION - PERSONNE QUI AURAIT EU QUALITÉ POUR FORMER TIERCE OPPOSITION SI ELLE N'AVAIT PAS ÉTÉ INVITÉE À PRÉSENTER DES OBSERVATIONS [RJ2].
</SCT>
<ANA ID="9A"> 54-04-03 Doit être regardée comme une partie à l'instance la personne qui a été invitée par la juridiction à présenter des observations et qui, si elle ne l'avait pas été, aurait eu qualité pour former tierce opposition contre cette décision.</ANA>
<ANA ID="9B"> 54-06-05-11 Doit être regardée comme une partie à l'instance la personne qui a été invitée par la juridiction à présenter des observations et qui, si elle ne l'avait pas été, aurait eu qualité pour former tierce opposition contre cette décision.</ANA>
<ANA ID="9C"> 54-08-04 Doit être regardée comme une partie à l'instance la personne qui a été invitée par la juridiction à présenter des observations et qui, si elle ne l'avait pas été, aurait eu qualité pour former tierce opposition contre cette décision.</ANA>
<ANA ID="9D"> 54-10-02 1) Le Conseil d'Etat contrôle la recevabilité de la question prioritaire de constitutionnalité  (QPC) soulevée devant la juridiction qui la lui a transmise.,,,2) Eu égard aux conséquences susceptibles d'en résulter quant au règlement du litige tel que déterminé par les conclusions des parties, seules ces dernières sont recevables à soulever une question prioritaire de constitutionnalité. Un observateur n'est donc, comme un intervenant, pas recevable à soulever de sa propre initiative une QPC qui n'aurait pas été invoquée par l'une des parties.,,,3) Doit être regardée comme une partie à l'instance, ayant à ce titre qualité pour soulever une telle question, la personne qui a été invitée par la juridiction à présenter des observations et qui, si elle ne l'avait pas été, aurait eu qualité pour former tierce opposition contre cette décision.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr. CE, 22 février 2013, Zoia, n°356245, T. p. 774-812.,,[RJ2] Cf. CE, 26 janvier 2011, Association de défense contre la déviation au nord de Maisse et commune de Courdimanche-sur-Essonne, n°307317, T. p. 1104-1110-1189. Rappr. CE, 10 janvier 2005, Association Quercy-Périgord contre le projet d'aéroport de Brive-Souillac et ses nuisances, n°265838, T. p. 1052 ; CE, 24 février 2015, Association des producteurs de cinéma et autres et Fédération communication conseil culture F3C, n°370629 371732, à mentionner aux Tables.


</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
