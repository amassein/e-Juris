<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026207097</ID>
<ANCIEN_ID>JG_L_2012_06_000000344646</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/20/70/CETATEXT000026207097.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 1ère sous-sections réunies, 20/06/2012, 344646</TITRE>
<DATE_DEC>2012-06-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>344646</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 1ère sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Philippe Martin</PRESIDENT>
<AVOCATS>SCP COUTARD, MUNIER-APAIRE ; LE PRADO ; RICARD</AVOCATS>
<RAPPORTEUR>Mme Nadia Bergouniou-Gournay</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Cyril Roger-Lacan</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:344646.20120620</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 1er décembre 2010 et 28 février 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. André D, demeurant ..., Mme Marie E, demeurant ..., le COMITE DE QUARTIER DE CAMPANIER, dont le siège est situé 19 impasse des Lilas à Nîmes (30900), M. Alain F, demeurant ..., M. Alain B, demeurant ..., M. Philippe G, demeurant ..., Mme Marie-Hélène H, demeurant ..., M. Daniel C, demeurant ... et M. Rémy A, demeurant ... ; M. D et les autres requérants demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler le jugement n° 0902293 du 1er octobre 2010 par lequel le tribunal administratif de Nîmes a rejeté leur requête tendant à l'annulation de la décision du 30 juin 2009 par laquelle le maire de Nîmes ne s'est pas opposé aux travaux déclarés par la société Orange France en vue de l'édification d'un pylône et d'armoires techniques sur un terrain situé 84, impasse des Dreux à Nîmes ;<br/>
<br/>
              2°) réglant l'affaire au fond, d'annuler la décision du 30 juin 2009 et d'enjoindre au maire de Nîmes de statuer à nouveau sur la demande de la Société Orange France et de s'y opposer ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Nîmes le versement d'une somme de 4. 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
Vu les autres pièces du dossier ;<br/>
Vu le code de l'urbanisme ;<br/>
Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Nadia Bergouniou-Gournay, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les observations de Me Le Prado, avocat de M. D et autres, de la SCP Coutard, Munier-Apaire, avocat de la société Orange France et de Me Ricard, avocat de la commune de Nîmes, <br/>
<br/>
              - les conclusions de M. Cyril Roger-Lacan, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à Me Le Prado, avocat de M. D et autres, à la SCP Coutard, Munier-Apaire, avocat de la société Orange France et à Me Ricard, avocat de la commune de Nîmes ;<br/>
<br/>
<br/>
<br/>
<br/>
              Sans qu'il soit besoin d'examiner les autres moyens du pourvoi ;<br/>
<br/>
              Considérant qu'aux termes de l'article L. 421-1 du code de l'urbanisme : " Les constructions, même ne comportant pas de fondations, doivent être précédées de la délivrance d'un permis de construire (...) " ; qu'aux termes de l'article L. 421-4 du même code : " Un décret en Conseil d'Etat arrête la liste des constructions, aménagements, installations et travaux qui, en raison de leurs dimensions, de leur nature ou de leur localisation, ne justifient pas l'exigence d'un permis et font l'objet d'une déclaration préalable (...) " ; que selon l'article L. 421-5 du même code, un décret en Conseil d'Etat arrête la liste des constructions, aménagements, installations et travaux qui, par dérogation aux dispositions des articles L. 421-1 à L. 421-4, sont dispensés de toute formalité au titre de ce code en raison, notamment, de leur très faible importance ; <br/>
<br/>
              Considérant qu'en vertu de l'article R. 421-1 du code de l'urbanisme, les constructions nouvelles doivent être précédées de la délivrance d'un permis de construire à l'exception des constructions mentionnées aux articles R. 421-2 à R. 421-8, qui sont dispensées de toute formalité au titre du code de l'urbanisme, et des constructions mentionnées aux articles R. 421-9 à R. 421-12, qui doivent faire l'objet d'une déclaration préalable ; que selon le a) de l'article R. 421-2 du même code, dans sa rédaction applicable à la date de la décision contestée, les constructions nouvelles dont la hauteur au-dessus du sol est inférieure à douze mètres et qui n'ont pas pour effet de créer de surface de plancher ou qui ont pour effet de créer une surface hors oeuvre brute inférieure ou égale à deux mètres carrés sont dispensées, en dehors des secteurs sauvegardés et des sites classés, de toute formalité au titre du code de l'urbanisme, en raison de leur nature ou de leur très faible importance ; qu'en vertu du a) de l'article R. 421-9 du même code, dans sa rédaction applicable à la date de la décision contestée, doivent faire l'objet d'une déclaration préalable, en dehors des secteurs sauvegardés et des sites classés, les constructions nouvelles n'étant pas dispensées de toute formalité au titre du code qui ont "  pour effet de créer une surface hors oeuvre brute supérieure à deux mètres carrés et inférieure ou égale à vingt mètres carrés " ; qu'en vertu des dispositions du  c) du même article, sont également soumises à autorisation préalable les constructions "  dont la hauteur au-dessus du sol est supérieure à douze mètres et qui n'ont pas pour effet de créer de surface hors oeuvre brute ou qui ont pour effet de créer une surface hors oeuvre brute inférieure ou égale à deux mètres carrés ", ces dernières dispositions n'étant pas applicables aux éoliennes et aux ouvrages de production d'électricité à partir de l'énergie solaire ; <br/>
<br/>
              Considérant qu'aux termes du premier alinéa de l'article R. 112-2 du code de l'urbanisme, dans sa rédaction applicable à la date de la décision contestée : " La surface de plancher hors oeuvre brute d'une construction est égale à la somme des surfaces de plancher de chaque niveau de la construction " ; <br/>
<br/>
              Considérant qu'il résulte de la combinaison des dispositions qui précèdent que les antennes relais de téléphonie mobile dont la hauteur est supérieure à douze mètres et dont les installations techniques nécessaires à leur fonctionnement entraînent la création d'une surface hors oeuvre brute de plus de deux mètres carrés n'entrent pas, dès lors qu'elles constituent entre elles un ensemble fonctionnel indissociable,  dans le champ des exceptions prévues au a) et au c) de l'article R. 421-9 du code de l'urbanisme et doivent faire l'objet d'un permis de construire en vertu des articles L. 421-1 et R. 421-1 du même code ;<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la société Orange France a déposé une seule déclaration préalable à la mairie de Nîmes le 20 mars 2009 en vue de construire une antenne relais de téléphonie mobile composée, d'une part, d'un pylône de radiotéléphonie d'une hauteur de 18 mètres reposant sur une dalle enterrée d'une surface de 9 mètres carrés, et, d'autre part, d'installations techniques sur une dalle de béton clôturée de palissades en bois d'une surface de 10,5 mètres carrés ; que les surfaces de plancher cumulées du pylône et du local technique sont créatrices d'une surface hors oeuvre brute supérieure à deux mètres carrés ;  qu'en jugeant que la construction projetée relevait du régime de la déclaration préalable, aux motifs que la construction du pylône pouvait être dissociée de celle du local technique, que le pylône relevait du c) de l'article R. 421-9 du code de l'urbanisme, que le local technique relevait du a) du même article et que les dispositions de l'article R. 421-9 ne font pas obstacle à ce que, par une même déclaration préalable, l'autorité compétente autorise plusieurs constructions sur le fondement d'alinéas différents de cet article, sans rechercher s'il existait un lien fonctionnel entre les deux ouvrages leur conférant le caractère d'une seule construction pour l'application des dispositions du c ) de l'article R. 421-9 du code de l'urbanisme, le tribunal administratif de Nîmes a commis une erreur de droit ; que, dès lors, M. D ET AUTRES sont fondés à demander, pour ce motif, l'annulation du jugement qu'ils attaquent ;<br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de Nîmes une somme globale de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ; que ces dispositions font en revanche obstacle à ce que soient mises à la charge de M. D ET AUTRES qui ne sont pas, dans la présente instance, la partie perdante, les sommes demandées à ce titre par la commune de Nîmes et la société Orange France ;<br/>
<br/>
<br/>
<br/>
<br/>
              			D E C I D E :<br/>
              			--------------<br/>
Article 1er : Le jugement du tribunal administratif de Nîmes du 1er octobre 2010 est annulé.<br/>
<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Nîmes.<br/>
<br/>
Article 3 : La commune de Nîmes versera à M. D ET AUTRES une somme globale de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
 Article 4 : Les conclusions présentées par la commune de Nîmes et la société Orange France au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 5 : La présente décision sera notifiée à M. André D, premier demandeur, à la commune de Nîmes et à la société Orange France.<br/>
 Les autres requérants seront informés de la présente décision par Me Le Prado, avocat au Conseil d'Etat et à la Cour de cassation, qui les représente devant le Conseil d'Etat.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">51-02-01 POSTES ET COMMUNICATIONS ÉLECTRONIQUES. COMMUNICATIONS ÉLECTRONIQUES. TÉLÉPHONE. - TRAVAUX DE CONSTRUCTION D'UNE ANTENNE-RELAIS DE TÉLÉPHONIE MOBILE ET INSTALLATIONS TECHNIQUES NÉCESSAIRES À SON FONCTIONNEMENT - ENSEMBLE FONCTIONNEL INDISSOCIABLE - CONSÉQUENCES - NÉCESSITÉ D'UN PERMIS DE CONSTRUIRE EN CAS DE HAUTEUR DE PLUS DE DOUZE MÈTRES ET DE CRÉATION D'UNE SHOB DE PLUS DE 2 M².
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">68-03-01-01 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PERMIS DE CONSTRUIRE. TRAVAUX SOUMIS AU PERMIS. PRÉSENTENT CE CARACTÈRE. - TRAVAUX DE CONSTRUCTION D'UNE ANTENNE-RELAIS DE TÉLÉPHONIE MOBILE ET INSTALLATIONS TECHNIQUES NÉCESSAIRES À SON FONCTIONNEMENT - ENSEMBLE FONCTIONNEL INDISSOCIABLE - CONSÉQUENCES - NÉCESSITÉ D'UN PERMIS DE CONSTRUIRE EN CAS DE HAUTEUR DE PLUS DE DOUZE MÈTRES ET DE CRÉATION D'UNE SHOB DE PLUS DE 2 M².
</SCT>
<ANA ID="9A"> 51-02-01 Il résulte de la combinaison des dispositions de l'article R. 421-1 du code de l'urbanisme, du a de l'article R. 421-2 et du a et du c de l'article R. 421-9 du même code, que les antennes relais de téléphonie mobile dont la hauteur est supérieure à douze mètres et dont les installations techniques nécessaires à leur fonctionnement entraînent la création d'une surface hors oeuvre brute (SHOB) de plus de deux mètres carrés n'entrent pas, dès lors qu'elles constituent entre elles un ensemble fonctionnel indissociable, dans le champ des exceptions prévues au a et au c de l'article R. 421-9 et doivent faire l'objet d'un permis de construire en vertu des articles L. 421-1 et R. 421-1.</ANA>
<ANA ID="9B"> 68-03-01-01 Il résulte de la combinaison des dispositions de l'article R. 421-1 du code de l'urbanisme, du a de l'article R. 421-2 et du a et du c de l'article R. 421-9 du même code, que les antennes relais de téléphonie mobile dont la hauteur est supérieure à douze mètres et dont les installations techniques nécessaires à leur fonctionnement entraînent la création d'une surface hors oeuvre brute (SHOB) de plus de deux mètres carrés n'entrent pas, dès lors qu'elles constituent entre elles un ensemble fonctionnel indissociable, dans le champ des exceptions prévues au a et au c de l'article R. 421-9 et doivent faire l'objet d'un permis de construire en vertu des articles L. 421-1 et R. 421-1.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
