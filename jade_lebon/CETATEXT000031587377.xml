<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031587377</ID>
<ANCIEN_ID>JG_L_2015_12_000000378325</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/58/73/CETATEXT000031587377.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème SSR, 07/12/2015, 378325, Publié au recueil Lebon</TITRE>
<DATE_DEC>2015-12-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>378325</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MASSE-DESSEN, THOUVENIN, COUDRAY</AVOCATS>
<RAPPORTEUR>Mme Charline Nicolas</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2015:378325.20151207</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B...A...a saisi le tribunal administratif de Dijon d'une demande tendant à l'annulation de la décision du 20 décembre 2011 du ministre de la défense rejetant sa demande, présentée en qualité d'ayant droit, tendant au bénéfice de la loi n° 2010-2 du 5 janvier 2010 relative à la reconnaissance et à l'indemnisation des victimes des essais nucléaires français. Par un jugement n° 1200330 du 15 janvier 2013, le tribunal a rejeté sa demande. <br/>
<br/>
              Par un arrêt n° 13LY00269 du 20 février 2014, la cour administrative d'appel de Lyon a rejeté l'appel formé par Mme A...contre ce jugement. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire, un mémoire en réplique et un nouveau mémoire, enregistrés les 22 avril et 21 juillet 2014, 25 mars et 15 octobre 2015 au secrétariat du contentieux du Conseil d'Etat, Mme A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses conclusions d'appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement de la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la loi n° 2010-2 du 5 janvier 2010 ; <br/>
              - le décret n° 2010-653 du 11 juin 2010 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Charline Nicolas, auditeur,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Masse-Dessen, Thouvenin, Coudray, avocat de Mme A...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article 1er de la loi du 5 janvier 2010 relative à la reconnaissance et à l'indemnisation des victimes des essais nucléaires français modifiée : " Toute personne souffrant d'une maladie radio-induite résultant d'une exposition à des rayonnements ionisants dus aux essais nucléaires français et inscrite sur une liste fixée par décret en Conseil d'Etat conformément aux travaux reconnus par la communauté scientifique internationale peut obtenir réparation intégrale de son préjudice dans les conditions prévues par la présente loi. Si la personne est décédée, la demande de réparation peut être présentée par ses ayants droit " ; que l'article 2 de cette même loi définit les conditions de temps et de lieu de séjour ou de résidence que le demandeur doit remplir ; qu'aux termes du I de l'article 4 de cette même loi, dans sa version applicable au litige : " Les demandes d'indemnisation sont soumises à un comité d'indemnisation (...) " et qu'aux termes du II de ce même article : " Ce comité examine si les conditions de l'indemnisation sont réunies. Lorsqu'elles le sont, l'intéressé bénéficie d'une présomption de causalité à moins qu'au regard de la nature de la maladie et des conditions de son exposition le risque attribuable aux essais nucléaires puisse être considéré comme négligeable. Le comité le justifie auprès de l'intéressé (...) " ; qu'aux termes de l'article 7 du décret du 11 juin 2010 pris pour l'application de la loi du 5 janvier 2010, dans sa rédaction alors applicable : " (...) Le comité d'indemnisation détermine la méthode qu'il retient pour formuler sa recommandation au ministre en s'appuyant sur les méthodologies recommandées par l'Agence internationale de l'énergie atomique. (...) " ; <br/>
<br/>
              2. Considérant qu'il résulte de ces dispositions que le législateur a entendu faire bénéficier toute personne souffrant d'une maladie radio-induite ayant résidé ou séjourné, durant des périodes déterminées, dans des zones géographiques situées en Polynésie française et en Algérie, d'une présomption de causalité aux fins d'indemnisation du préjudice subi en raison de l'exposition aux rayonnements ionisants due aux essais nucléaires ; que, toutefois, cette présomption peut être renversée lorsqu'il est établi que le risque attribuable aux essais nucléaires, apprécié tant au regard de la nature de la maladie que des conditions particulières d'exposition du demandeur, est négligeable ; qu'à ce titre, l'appréciation du risque peut notamment prendre en compte le délai de latence de la maladie, le sexe du demandeur, son âge à la date du diagnostic, sa localisation géographique au moment des tirs, les fonctions qu'il exerçait effectivement, ses conditions d'affectation, ainsi que, le cas échéant, les missions de son unité au moment des tirs ; <br/>
<br/>
              3. Considérant que le calcul de la dose reçue de rayonnements ionisants constitue l'un des éléments sur lequel l'autorité chargée d'examiner la demande peut se fonder afin d'évaluer le risque attribuable aux essais nucléaires ; que si, pour ce calcul, l'autorité peut utiliser les résultats des mesures de surveillance de la contamination tant interne qu'externe des personnes exposées, qu'il s'agisse de mesures individuelles ou collectives en ce qui concerne la contamination externe, il lui appartient de vérifier, avant d'utiliser ces résultats, que les mesures de surveillance de la contamination interne et externe ont, chacune, été suffisantes au regard des conditions concrètes d'exposition de l'intéressé, et sont ainsi de nature à établir si le risque attribuable aux essais nucléaires était négligeable ; qu'en l'absence de mesures de surveillance de la contamination interne ou externe et en l'absence de données relatives au cas des personnes se trouvant dans une situation comparable à celle du demandeur du point de vue du lieu et de la date de séjour, il appartient à cette même autorité de vérifier si, au regard des conditions concrètes d'exposition de l'intéressé précisées ci-dessus, de telles mesures auraient été nécessaires ; que si tel est le cas, l'administration ne peut être regardée comme rapportant la preuve de ce que le risque attribuable aux essais nucléaires doit être regardé comme négligeable et la présomption de causalité ne peut être renversée ;<br/>
<br/>
              4. Considérant, en premier lieu, qu'il ressort des pièces du dossier soumis aux juges du fond que la méthode retenue par le comité d'indemnisation des victimes des essais nucléaires (CIVEN) pour évaluer le risque attribuable aux essais nucléaires, s'appuie sur une pluralité de critères, recommandés par l'Agence internationale de l'énergie atomique, notamment sur les conditions particulières d'exposition de l'intéressé, et qui, pour le calcul de la dose reçue de rayonnements ionisants, prend en compte, au titre de la contamination externe, les résultats de mesures de surveillance individuelles ou collectives disponibles ou en leur absence, la dose équivalente à la valeur du seuil de détection des dosimètres individuels pour chaque mois de présence basée sur des données de surveillance radiologique atmosphérique permanente effectuée dans les centres d'essais nucléaires, et, au titre de la contamination interne, les résultats des mesures individuelles de surveillance, ou en leur absence, les résultats des mesures de surveillance d'individus placés dans des situations comparables ; que ces critères, ainsi qu'il a été dit ci-dessus aux points 2 et 3, ne méconnaissent pas les dispositions de la loi et permettent, sur ce fondement, d'établir, le cas échéant, le caractère négligeable du risque attribuable aux essais nucléaires dans la survenue de la maladie dont souffre l'intéressé ; que, par suite, la cour administrative d'appel de Lyon n'a pas commis les erreurs de droit que lui reproche le pourvoi et s'est livrée à une appréciation souveraine des pièces du dossier qui n'est pas entachée de dénaturation en écartant le moyen tiré de ce que la méthode utilisée par le CIVEN ne permettait pas de caractériser l'existence d'un risque négligeable attribuable aux essais nucléaires ; <br/>
<br/>
              5. Considérant, en deuxième lieu, que la cour administrative d'appel de Lyon s'est fondée, pour rejeter la demande d'indemnisation de MmeA..., sur le délai de latence de l'affection dont a souffert M.A..., les conditions d'exposition de ce dernier, notamment ses missions, ses conditions de séjour, la localisation au moment des tirs et les missions du navire sur lequel il était affecté ainsi que le résultat de relevés dosimétriques disponibles et, en l'absence de suivi spécifique de contamination interne, sur la circonstance que ces conditions d'exposition ne nécessitaient pas un tel suivi ; qu'elle a, se faisant, ni commis d'erreur de droit ni entaché son arrêt d'une contradiction de motifs ou d'insuffisance de motivation ; <br/>
<br/>
              6. Considérant, en troisième lieu, qu'une fois le caractère négligeable du risque attribuable aux essais nucléaires établi, la présomption de causalité instituée par les dispositions citées ci-dessus est renversée ; que la cour administrative d'appel de Lyon, ayant estimé établi le caractère négligeable du risque, a pu, dès lors, sans contradiction de motifs ni erreur de droit et sans méconnaître son office, rejeter la demande d'indemnisation présentée par la requérante, à défaut pour celle-ci d'établir un lien de causalité direct et certain entre l'exposition aux rayonnements ionisants et la maladie de son époux ; <br/>
<br/>
              7. Considérant qu'il résulte de tout ce qui précède que le pourvoi de Mme A... doit être rejeté ; <br/>
<br/>
              8. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de Mme A...est rejeté. <br/>
Article 2 : La présente décision sera notifiée à Mme B...A...et au ministre de la défense.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">08-20 ARMÉES ET DÉFENSE. - INDEMNISATION DES VICTIMES DES ESSAIS NUCLÉAIRES - PRÉSOMPTION DE CAUSALITÉ À L'ÉGARD DES VICTIMES DE MALADIES RÉSULTANT D'UNE EXPOSITION À DES RAYONNEMENTS IONISANTS - CONDITIONS DE RENVERSEMENT DE LA PRÉSOMPTION [RJ1].
</SCT>
<ANA ID="9A"> 08-20 Il résulte des articles 1er, 2 et 4 de la loi n° 2010-2 du 5 janvier 2010 que le législateur a entendu faire bénéficier toute personne souffrant d'une maladie radio-induite ayant résidé ou séjourné, durant des périodes déterminées, dans des zones géographiques situées en Polynésie française et en Algérie, d'une présomption de causalité aux fins d'indemnisation du préjudice subi en raison de l'exposition aux rayonnements ionisants due aux essais nucléaires. Toutefois, cette présomption peut être renversée lorsqu'il est établi que le risque attribuable aux essais nucléaires, apprécié tant au regard de la nature de la maladie que des conditions particulières d'exposition du demandeur, est négligeable.... ,,A ce titre, l'appréciation du risque peut notamment prendre en compte le délai de latence de la maladie, le sexe du demandeur, son âge à la date du diagnostic, sa localisation géographique au moment des tirs, les fonctions qu'il exerçait effectivement, ses conditions d'affectation, ainsi que, le cas échéant, les missions de son unité au moment des tirs.,,,Le calcul de la dose reçue de rayonnements ionisants constitue l'un des éléments sur lequel l'autorité chargée d'examiner la demande peut se fonder afin d'évaluer le risque attribuable aux essais nucléaires. Pour ce calcul, l'autorité peut utiliser les résultats des mesures de surveillance de la contamination tant interne qu'externe des personnes exposées, qu'il s'agisse de mesures individuelles ou collectives en ce qui concerne la contamination externe. Il lui appartient toutefois de vérifier, avant d'utiliser ces résultats, que les mesures de surveillance de la contamination interne et externe ont, chacune, été suffisantes au regard des conditions concrètes d'exposition de l'intéressé, et sont ainsi de nature à établir si le risque attribuable aux essais nucléaires était négligeable. En l'absence de mesures de surveillance de la contamination interne ou externe et en l'absence de données relatives au cas des personnes se trouvant dans une situation comparable à celle du demandeur du point de vue du lieu et de la date de séjour, il appartient à cette même autorité de vérifier si, au regard des conditions concrètes d'exposition de l'intéressé précisées ci-dessus, de telles mesures auraient été nécessaires. Si tel est le cas, l'administration ne peut être regardée comme rapportant la preuve de ce que le risque attribuable aux essais nucléaires doit être regardé comme négligeable et la présomption de causalité ne peut être renversée.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Cf. décisions du même jour, Mme Bertrand, n° 378323, inédite au Recueil, et Ministre de la défense, n° 386980, inédite au Recueil.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
