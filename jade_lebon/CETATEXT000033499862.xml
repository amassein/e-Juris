<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033499862</ID>
<ANCIEN_ID>JG_L_2016_11_000000389423</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/49/98/CETATEXT000033499862.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 28/11/2016, 389423</TITRE>
<DATE_DEC>2016-11-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>389423</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP POTIER DE LA VARDE, BUK LAMENT, ROBILLOT ; SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>Mme Isabelle Lemesle</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:389423.20161128</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire complémentaire, enregistrés le 13 avril 2016 et le 13 juillet 2016 au secrétariat du contentieux du Conseil d'Etat, l'association Carrefour demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite de rejet né du silence gardé par le Premier ministre sur sa demande tendant à l'abrogation du décret du 27 mars 2006 approuvant la modification des statuts de l'association reconnue d'utilité publique " Foyer Saint-Joseph " et la dissolution de celle-ci, et abrogeant le décret portant reconnaissance de cette association comme établissement d'utilité publique ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts ;<br/>
              - le code civil local ;<br/>
              - le décret n° 85-1304 du 9 décembre 1985 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Isabelle Lemesle, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Potier de la Varde, Buk Lament, avocat de l'association Carrefour ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Par une délibération du 26 novembre 1997, l'association dite " Foyer Saint-Joseph ", dont le siège est à Metz et qui a été reconnue établissement d'utilité publique par un décret impérial du 8 janvier 1868, a décidé de modifier ses statuts de sorte qu'en cas de dissolution, l'actif net de l'association soit dévolu non plus au bureau de bienfaisance ou d'aide sociale de la ville de Metz mais à la Fédération des oeuvres de charité du diocèse de Metz-Caritas dont elle était adhérente, au motif que les buts de cette dernière étaient complémentaires des siens, notamment par l'aide apportée à l'insertion des jeunes en difficulté. Par une délibération du 7 décembre 2000, le " Foyer Saint-Joseph " a décidé de sa dissolution. Un décret en Conseil d'Etat du 27 mars 2006, approuve dans son article 1er cette modification statutaire et dans son article 2 cette dissolution, abroge dans son article 3 le décret impérial du 8 janvier 1868, autorise dans son article 4 le président de la Fédération des oeuvres de charité du diocèse de Metz-Caritas à accepter la dévolution de l'actif net résultant de la liquidation du Foyer Saint-Joseph et, dans son article 5, déclare que cette dévolution présente le caractère de bienfaisance prévu au 4 de l'article 795 du code général des impôts et qu'elle intervient, au regard de l'article 1039 du même code, dans un intérêt général et de bonne administration, avec maintien de l'affectation des biens dévolus au même objet. <br/>
<br/>
              2. Il ressort des pièces du dossier que le Foyer Saint-Joseph, pour un loyer symbolique, a donné à bail à l'association Carrefour, qui a pour objet l'insertion sociale et professionnelle des jeunes, un ensemble immobilier, situé au 6, rue Marchant à Metz, pour une durée de 31 ans à compter du 1er septembre 1977, à charge pour elle d'y poursuivre une mission d'accueil de jeunes orphelins. A la suite de la dévolution intervenue le 1er janvier 2007 sur le fondement du décret mentionné au point 1, ce bail a été prorogé à titre gratuit par la Fédération des oeuvres de charité du diocèse de Metz-Caritas jusqu'au 31 août 2009, puis jusqu'au 31 août 2010. L'association Carrefour, qui depuis lors s'est maintenue dans les lieux sans droit ni titre, a fait appel du jugement du 14 août 2014 par lequel le tribunal de grande instance de Metz a rejeté sa demande tendant à ce qu'elle soit reconnue comme étant venue aux droits du Foyer Saint-Joseph, en lieu et place de la Fédération des oeuvres de charité du diocèse de Metz-Caritas. Elle a demandé à la cour d'appel de surseoir à statuer dans l'attente de la présente décision. L'association Carrefour demande au Conseil d'Etat l'annulation de la décision implicite de rejet née du silence gardé par le Premier ministre sur sa demande, reçue le 12 décembre 2014, tendant à l'abrogation du décret du 27 mars 2006.<br/>
<br/>
              Sur la recevabilité de la demande d'abrogation :<br/>
<br/>
              3. L'association Carrefour, qui a pour objet l'insertion sociale et professionnelle des jeunes, ne justifie pas d'un intérêt lui donnant qualité pour demander l'abrogation des articles 1er, 2, 3, 4 et 5 du décret du 27 mars 2006, qui respectivement, comme mentionné au point 1, approuvent la modification statutaire de l'association " Foyer Saint-Joseph ", ainsi que sa dissolution, abrogent le décret impérial du 8 janvier 1868 conférant à celle-ci la qualité d'établissement d'utilité publique, autorisent le président de la  Fédération des oeuvres de charité du diocèse de Metz-Caritas à accepter la dévolution de l'actif net résultant de la liquidation du Foyer Saint-Joseph et déclarent que cette dévolution présente le caractère de bienfaisance prévu au 4 de l'article 795 du code général des impôts, alors même que comme il a été dit au point 2, à cette date, elle était occupant sans droit ni titre des locaux qui lui avaient initialement été donnés à bail par l'association " Foyer Saint-Joseph ". <br/>
<br/>
              4. Il résulte de ce qui précède que l'association Carrefour n'est pas recevable à demander l'annulation de la décision implicite par laquelle le Premier ministre a rejeté sa demande tendant à l'abrogation du décret en Conseil d'Etat du 27 mars 2006 approuvant la modification des statuts d'une association reconnue d'utilité publique et la dissolution de celle-ci et abrogeant le décret portant reconnaissance de cette association comme établissement d'utilité publique. Il s'ensuit que ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent qu'être rejetées. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'association Carrefour la somme de 3 000 euros que demande la Fédération des oeuvres de charité du diocèse de Metz-Caritas au titre de ces dispositions.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : La requête de l'association Carrefour est rejetée.<br/>
<br/>
Article 2 : L'association Carrefour versera à la Fédération des oeuvres de charité du diocèse de Metz-Caritas  la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée à l'association Carrefour et à la Fédération des oeuvres de charité du diocèse de Metz-Caritas.<br/>
Copie en sera adressée au Premier ministre.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">10-02-03-07-03 ASSOCIATIONS ET FONDATIONS. RÉGIME JURIDIQUE DES DIFFÉRENTES ASSOCIATIONS. ASSOCIATIONS RECONNUES D'UTILITÉ PUBLIQUE. DISPARITION. DÉVOLUTION DES BIENS. - CONTESTATION DU DÉCRET AUTORISANT UNE ASSOCIATION À ACCEPTER LA DÉVOLUTION DE L'ACTIF D'UNE ASSOCIATION RECONNUE D'UTILITÉ PUBLIQUE DISSOUTE - INTÉRÊT POUR AGIR - OCCUPANT D'UN BIEN FAISANT PARTIE DE CET ACTIF - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-01-04-01 PROCÉDURE. INTRODUCTION DE L'INSTANCE. INTÉRÊT POUR AGIR. ABSENCE D'INTÉRÊT. - DÉCRET AUTORISANT UNE ASSOCIATION À ACCEPTER LA DÉVOLUTION DE L'ACTIF D'UNE ASSOCIATION RECONNUE D'UTILITÉ PUBLIQUE DISSOUTE - OCCUPANT D'UN BIEN FAISANT PARTIE DE CET ACTIF.
</SCT>
<ANA ID="9A"> 10-02-03-07-03 Le locataire, devenu occupant sans titre, d'un bien ne justifie pas à ce titre d'un intérêt lui donnant qualité pour agir contre le décret autorisant une association à accepter la dévolution de l'actif net qui résulte de la dissolution d'une association reconnue d'utilité publique et qui inclut le bien occupé.</ANA>
<ANA ID="9B"> 54-01-04-01 Le locataire, devenu occupant sans titre, d'un bien ne justifie pas à ce titre d'un intérêt lui donnant qualité pour agir contre le décret autorisant une association à accepter la dévolution de l'actif net qui résulte de la dissolution d'une association reconnue d'utilité publique et qui inclut le bien occupé.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
