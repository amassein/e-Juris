<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030853967</ID>
<ANCIEN_ID>JG_L_2015_07_000000383613</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/85/39/CETATEXT000030853967.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème / 2ème SSR, 01/07/2015, 383613</TITRE>
<DATE_DEC>2015-07-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>383613</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème / 2ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>BALAT ; SCP DELAPORTE, BRIARD, TRICHET</AVOCATS>
<RAPPORTEUR>Mme Charline Nicolas</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Bertrand Dacosta</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:383613.20150701</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Sud terrassement a demandé au tribunal administratif de Nice de condamner la communauté de communes des terres de Siagne à l'indemniser à hauteur de 94 034,30 euros au titre de sujétions imprévues dans l'exécution d'un marché de travaux. <br/>
<br/>
              Par un jugement n° 0902701 du 16 décembre 2011, le tribunal administratif de Nice a rejeté cette demande.<br/>
<br/>
              Par un arrêt n° 12MA00617 du 10 juin 2014, la cour administrative d'appel de Marseille a, sur appel de la société Sud terrassement, annulé ce jugement et condamné la communauté de communes des terres de Siagne à verser à la société la somme de 94 034,30 euros assortie des intérêts au taux légal à compter du 28 novembre 2008.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 11 août et 12 novembre 2014 au secrétariat du contentieux du Conseil d'Etat, la régie des eaux du canal de Belletrud (RECB), venue aux droits de la communauté de communes des terres de Siagne, demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de la société Sud terrassement ; <br/>
<br/>
              3°) de mettre à la charge de la société Sud terrassement le versement de la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code des marchés publics ; <br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Charline Nicolas, auditeur,  <br/>
<br/>
              - les conclusions de M. Bertrand Dacosta, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Delaporte, Briard, Trichet, avocat de la régie des eaux du canal de Belletrud, et à Me Balat, avocat de la société Sud terrassement ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par un marché à forfait d'un montant de 695 940 euros HT conclu le 7 mars 2006, le syndicat intercommunal des cinq communes de l'eau et de l'assainissement a confié à la société Degrémont la construction d'une unité de séchage solaire des boues de la station d'épuration de Peymeinade ; qu'aux fins d'effectuer des travaux de terrassement, la société Dégrémont a conclu un contrat de sous-traitance d'un montant de 156 000 euros HT avec la société Sud terrassement ; que le maître d'ouvrage a agréé ce sous-traitant et accepté ses conditions de paiement ; que la société Sud terrassement a saisi le tribunal administratif de Nice d'une demande tendant à la condamnation de la communauté de communes des terres de Siagne, venue aux droits du syndicat intercommunal des cinq communes de l'eau et de l'assainissement, à lui payer la somme de 94 034,30 euros correspondant au surcoût des travaux qu'elle a supporté, les sols s'étant révélés d'une nature différente de celle qu'avait analysée l'étude des sols qui avait été réalisée préalablement à la conclusion du marché ; que, par un jugement du 16 décembre 2011, le tribunal administratif de Nice a rejeté cette demande ; que la cour administrative d'appel de Marseille a, par un arrêt du 10 juin 2014, annulé ce jugement et condamné la communauté de communes des terres de Siagne à payer à la société Sud terrassement la somme de 94 034,30 euros TTC au titre des sujétions imprévues ; que la régie des eaux du canal de Belletrud (RECB), venue aux droits de la communauté de communes des terres de Siagne, se pourvoit en cassation contre cet arrêt ; <br/>
<br/>
              2. Considérant que, même si un marché public a été conclu à prix forfaitaire, son titulaire a droit à être indemnisé pour les dépenses exposées en raison de sujétions imprévues, c'est-à-dire de sujétions présentant un caractère exceptionnel et imprévisible et dont la cause est extérieure aux parties, si ces sujétions ont eu pour effet de bouleverser l'économie générale du marché ; qu'un sous-traitant bénéficiant du paiement direct des prestations sous-traitées a également droit à ce paiement direct pour les dépenses résultant pour lui de sujétions imprévues qui ont bouleversé l'économie générale du marché ; <br/>
<br/>
              3. Considérant que, pour accorder la somme de 94 034,30 euros TTC à la société Sud terrassement, sous-traitante de la société Degrémont, titulaire du marché en cause, la cour s'est fondée sur la circonstance qu'elle avait dû faire face à des sujétions imprévues qui avaient eu pour effet de bouleverser l'économie générale du marché ; que, toutefois, pour apprécier si des sujétions imprévues apparues pendant l'exécution d'une partie sous-traitée d'un marché ont entraîné un bouleversement de l'économie générale de ce marché, il convient de comparer le montant des dépenses résultant de ces sujétions au montant total du marché et non au montant de la partie sous-traitée ; que la cour a inexactement qualifié les faits qui lui étaient soumis en jugeant que les dépenses occasionnées en l'espèce, d'un montant estimé par elle à 78 624 euros HT et 94 034 euros TTC, soit 11,3 % du montant total de 695 940 euros HT, avaient bouleversé l'économie générale du marché ; que, par suite, son arrêt doit être annulé ; <br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ; <br/>
<br/>
              5. Considérant que le montant des dépenses auxquelles la société Sud terrassement soutient avoir dû faire face pour l'exécution de son contrat de sous-traitance du fait de sujétions imprévues est évalué par elle à 94 034 euros TTC, soit 11,3 % du marché conclu entre le maître d'ouvrage et le titulaire du marché ; que ces dépenses ne peuvent donc être regardées comme ayant bouleversé l'économie générale du marché ; que la société n'est ainsi pas fondée à se plaindre de ce que, par le jugement attaqué, le tribunal administratif de Nice a rejeté sa demande ;  <br/>
<br/>
              6. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que le versement d'une somme soit mis à la charge de la RECB qui n'est pas, dans la présente instance, la partie perdante ; qu'il y a lieu, en revanche, de mettre à la charge de la société Sud terrassement le versement de la somme de 3 000 euros à la RECB au titre de ces mêmes dispositions ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Marseille du 10 juin 2014 est annulé. <br/>
Article 2 : La requête présentée par la société Sud terrassement devant la cour administrative d'appel de Marseille est rejetée. <br/>
Article 3 : La société Sud terrassement versera à la régie des eaux du canal de Belletrud une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions présentées par la société Sud terrassement au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à la régie des eaux du canal de Belletrud et à la société Sud terrassement.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-03-01-02-03 MARCHÉS ET CONTRATS ADMINISTRATIFS. EXÉCUTION TECHNIQUE DU CONTRAT. CONDITIONS D'EXÉCUTION DES ENGAGEMENTS CONTRACTUELS EN L'ABSENCE D'ALÉAS. MARCHÉS. SOUS-TRAITANCE. - MARCHÉ PUBLIC À FORFAIT - DROIT À INDEMNISATION DU SOUS-TRAITANT EN CAS DE SUJÉTIONS IMPRÉVUES - EXISTENCE - SEUIL DE DÉCLENCHEMENT - BOULEVERSEMENT DE L'ÉCONOMIE GÉNÉRALE DU MARCHÉ - MODALITÉS D'APPRÉCIATION - PRISE EN COMPTE DU MONTANT TOTAL DU MARCHÉ.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">39-05-01-02-01 MARCHÉS ET CONTRATS ADMINISTRATIFS. EXÉCUTION FINANCIÈRE DU CONTRAT. RÉMUNÉRATION DU CO-CONTRACTANT. INDEMNITÉS. TRAVAUX SUPPLÉMENTAIRES. - MARCHÉ PUBLIC À FORFAIT - DROIT À INDEMNISATION DU SOUS-TRAITANT EN CAS DE SUJÉTIONS IMPRÉVUES - EXISTENCE - SEUIL DE DÉCLENCHEMENT - BOULEVERSEMENT DE L'ÉCONOMIE GÉNÉRALE DU MARCHÉ - MODALITÉS D'APPRÉCIATION - PRISE EN COMPTE DU MONTANT TOTAL DU MARCHÉ.
</SCT>
<ANA ID="9A"> 39-03-01-02-03 Même si un marché public a été conclu à prix forfaitaire, son titulaire a droit à être indemnisé pour les dépenses exposées en raison de sujétions imprévues, c'est-à-dire de sujétions présentant un caractère exceptionnel et imprévisible et dont la cause est extérieure aux parties, si ces sujétions ont eu pour effet de bouleverser l'économie générale du marché. Un sous-traitant bénéficiant du paiement direct des prestations sous-traitées a également droit à ce paiement direct pour les dépenses résultant pour lui de sujétions imprévues qui ont bouleversé l'économie générale du marché.,,,Pour apprécier si des sujétions imprévues apparues pendant l'exécution d'une partie sous-traitée d'un marché ont entraîné un bouleversement de l'économie générale de ce marché, il convient de comparer le montant des dépenses résultant de ces sujétions au montant total du marché et non au montant de la partie sous-traitée.</ANA>
<ANA ID="9B"> 39-05-01-02-01 Même si un marché public a été conclu à prix forfaitaire, son titulaire a droit à être indemnisé pour les dépenses exposées en raison de sujétions imprévues, c'est-à-dire de sujétions présentant un caractère exceptionnel et imprévisible et dont la cause est extérieure aux parties, si ces sujétions ont eu pour effet de bouleverser l'économie générale du marché. Un sous-traitant bénéficiant du paiement direct des prestations sous-traitées a également droit à ce paiement direct pour les dépenses résultant pour lui de sujétions imprévues qui ont bouleversé l'économie générale du marché.,,,Pour apprécier si des sujétions imprévues apparues pendant l'exécution d'une partie sous-traitée d'un marché ont entraîné un bouleversement de l'économie générale de ce marché, il convient de comparer le montant des dépenses résultant de ces sujétions au montant total du marché et non au montant de la partie sous-traitée.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
