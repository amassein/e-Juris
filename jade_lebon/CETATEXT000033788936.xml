<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033788936</ID>
<ANCIEN_ID>JG_L_2016_12_000000386536</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/78/89/CETATEXT000033788936.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 4ème chambres réunies, 30/12/2016, 386536</TITRE>
<DATE_DEC>2016-12-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>386536</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 4ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DIDIER, PINET</AVOCATS>
<RAPPORTEUR>M. Florian Roussel</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CECHR:2016:386536.20161230</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Covea risks, en sa qualité d'assureur, subrogé dans les droits de la SA Petillon auto, a demandé au tribunal administratif de Cergy-Pontoise de condamner l'Etat à lui verser la somme de 2 401 580,34 euros, avec intérêts au taux légal et capitalisation des intérêts, en réparation du préjudice que cette société estime avoir subi du fait de l'incendie, le 26 novembre 2007, du garage situé au 6 avenue des Erables à Villiers-le-Bel. Par un jugement n° 1008768 du 5 avril 2012, le tribunal administratif de Cergy-Pontoise a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 12VE02119 du 2 octobre 2014, la cour administrative d'appel de Versailles a rejeté l'appel formé  par la société Covea risks contre ce jugement.  <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 17 décembre 2014 et 18 mars 2015 au secrétariat du contentieux du Conseil d'Etat, la société Covea risks demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
<br/>
              - le code général des collectivités territoriales ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Florian Roussel, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Didier, Pinet, avocat de la Société Covea Risks SA.<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond qu'un garage automobile situé à Villiers-le-Bel et exploité par la société Petillon auto ayant été incendié le 26 novembre 2007, la société Covéa risks, assureur subrogé dans les droits de l'exploitant, a recherché la responsabilité de l'Etat sur le fondement des dispositions de l'article L. 2216-3 du code général des collectivités territoriales ; que sa demande a été rejetée par un jugement du 5 avril 2012 du tribunal administratif de Cergy-Pontoise, confirmé par un arrêt du 2 octobre 2014 de la cour administrative d'appel de Versailles, contre lequel elle se pourvoit en cassation ;<br/>
<br/>
              2. Considérant qu'aux termes du premier alinéa de l'article L. 2216-3 du code général des collectivités territoriales, applicable au litige porté devant les juges du fond et désormais repris à l'article L. 211-10 du code de la sécurité intérieure : " L'Etat est civilement responsable des dégâts et dommages résultant des crimes et délits commis, à force ouverte ou par violence, par des attroupements ou rassemblements armés ou non armés, soit contre les personnes, soit contre les biens " ; <br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, le 26 novembre 2007, vers 17 heures,  deux adolescents ont péri à la suite d'une collision entre leur mini-moto et un véhicule de police, à Villiers-le-Bel ; que pendant l'intervention des secours, une foule très hostile d'habitants du quartier s'est regroupée sur les lieux de l'accident et a pris à parti les forces de police ; que plusieurs centaines de personnes se sont ensuite dirigées vers la caserne des sapeurs-pompiers, où les corps des adolescents avaient été déposés, avant de redescendre l'avenue des Erables et de s'attaquer aux commerces situés à proximité, parmi lesquels le garage de la société Petillon, qui a été incendié vers 19 heures ; que si la cour administrative d'appel a pu relever, par une appréciation souveraine des faits qui lui étaient soumis, d'une part, que les auteurs des dégradations avaient utilisé des moyens de communication ainsi que des cocktails Molotov et des battes de base-ball et qu'ils avaient formé des groupes mobiles, d'autre part, qu'un restaurant de la même commune avait fait l'objet d'une attaque une heure avant le décès des deux adolescents, elle a commis une erreur de qualification juridique en déduisant de ces éléments que l'incendie n'était pas le fait d'un attroupement ou d'un rassemblement au sens des dispositions précitées de l'article L. 2216-3 du code général des collectivités territoriales, dès lors qu'il ressortait des pièces du dossier qui lui était soumis que cet incendie avait été provoqué par des personnes qui étaient au nombre de celles qui s'étaient spontanément rassemblées, peu de temps auparavant, pour manifester leur émotion après le décès des deux adolescents et que, par ailleurs, l'attaque du restaurant était sans rapport avec cette manifestation ;  que, par suite, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, son arrêt doit être annulé ;<br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement à la société Covea risks de la somme de 3 000 euros au titre des frais exposés par elle et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Versailles du 2 octobre 2014 est annulé. <br/>
<br/>
		Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Versailles. <br/>
Article 3 : L'Etat versera à la société Covea risks une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la société Covea risks et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">60-01-05-01 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. FAITS SUSCEPTIBLES OU NON D'OUVRIR UNE ACTION EN RESPONSABILITÉ. RESPONSABILITÉ RÉGIE PAR DES TEXTES SPÉCIAUX. ATTROUPEMENTS ET RASSEMBLEMENTS (ART. L. 2216-3 DU CGCT). - NOTION - CAS D'UN INCENDIE AYANT FAIT L'OBJET D'UNE ORGANISATION IMPLIQUANT MOYENS DE COMMUNICATION ET COCKTAILS MOLOTOV MAIS S'INSCRIVANT DANS LE PROLONGEMENT D'UN RASSEMBLEMENT SPONTANÉ - INCLUSION [RJ1].
</SCT>
<ANA ID="9A"> 60-01-05-01 Cas d'un rassemblement d'une foule très hostile à la suite du décès de deux adolescents ayant péri dans une collision avec un véhicule de police, suivi du déplacement de plusieurs centaines de personnes vers l'endroit où les corps avaient été déposés puis dans une avenue de la commune où un garage a été incendié. Bien que, d'une part, les auteurs des dégradations aient utilisé des moyens de communication ainsi que des cocktails Molotov et des battes de base-ball et qu'ils aient formé des groupes mobiles, et alors que, d'autre part, un restaurant de la même commune avait fait l'objet d'une attaque une heure avant le décès des deux adolescents, cet incendie a été le fait d'un attroupement ou rassemblement au sens de l'article L. 2216-3 du code général des collectivités territoriales (devenu art. L. 211-10 du code de la sécurité intérieure) dès lors qu'il a été provoqué par des personnes qui étaient au nombre de celles qui s'étaient spontanément rassemblées, peu de temps auparavant, pour manifester leur émotion après le décès des deux adolescents et que l'attaque du restaurant était sans rapport avec cette manifestation.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Comp., pour le cas d'un blocage prémédité organisé en dehors d'un rassemblement spontané, décision du même jour, Sté Générali IARD et autres, n° 389835, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
