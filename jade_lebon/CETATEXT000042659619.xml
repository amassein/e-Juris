<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042659619</ID>
<ANCIEN_ID>JG_L_2020_12_000000427744</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/65/96/CETATEXT000042659619.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 11/12/2020, 427744, Publié au recueil Lebon</TITRE>
<DATE_DEC>2020-12-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>427744</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>M. Fabio Gennari</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie Roussel</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:427744.20201211</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Air France a demandé au tribunal administratif de Paris d'annuler la décision du 17 mai 2016 par laquelle le ministre de l'intérieur lui a infligé une amende de 5 000 euros en application de l'article L. 625-1 du code de l'entrée et du séjour des étrangers et du droit d'asile. <br/>
<br/>
              Par un jugement n° 1611046 du 3 octobre 2017, le tribunal administratif de Paris a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 17PA03680 du 10 décembre 2018, la cour administrative d'appel de Paris a rejeté l'appel formé par la société Air France contre ce jugement. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 6 février et 3 mai 2019 et le 29 octobre 2020 au secrétariat du contentieux du Conseil d'Etat, la société Air France demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses conclusions d'appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - la décision n° 2019-810 QPC du Conseil constitutionnel du 25 octobre 2019 ; <br/>
              - le code de justice administrative, l'ordonnance n° 2020-1402 du 18 novembre 2020 et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - Le rapport de M. Fabio Gennari, auditeur,<br/>
<br/>
              - Les conclusions de Mme Sophie Roussel, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de la société Air France ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1.	Aux termes de l'article L. 625-1 du code de l'entrée et du séjour des étrangers et du droit d'asile, dans sa version applicable au litige : " Est punie d'une amende d'un montant maximum de 5 000 euros l'entreprise de transport aérien ou maritime qui débarque sur le territoire français, en provenance d'un Etat avec lequel ne s'applique pas l'acquis de Schengen, un étranger non ressortissant d'un Etat de l'Union européenne et démuni du document de voyage et, le cas échéant, du visa requis par la loi ou l'accord international qui lui est applicable en raison de sa nationalité (...) ". En vertu de l'article L. 625-5 du même code, dans sa rédaction alors en vigueur, déclarée conforme à la Constitution par le Conseil constitutionnel dans sa décision n° 2019-810 QPC du 25 octobre 2019 : " Les amendes prévues aux articles L. 625-1 et L. 625-4 ne sont pas infligées : (...) 2° Lorsque l'entreprise de transport établit que les documents requis lui ont été présentés au moment de l'embarquement et qu'ils ne comportaient pas d'élément d'irrégularité manifeste ". <br/>
<br/>
              2.	Il ressort des pièces du dossier soumis aux juges du fond que, par une décision en date du 17 mai 2016, le ministre de l'intérieur a infligé à la société Air France une amende de 5 000 euros, sur le fondement de ces dispositions, pour avoir, le 6 juillet 2015, débarqué sur le territoire français une personne en provenance de Ouagadougou, titulaire d'un passeport de la République démocratique du Congo qui s'est révélé contrefait. Par un jugement du 3 octobre 2017, le tribunal administratif de Paris a rejeté la demande d'annulation de cette sanction présentée par la société Air France. Cette société se pourvoit en cassation contre l'arrêt du 10 décembre 2018 par lequel la cour administrative d'appel de Paris a rejeté son appel contre ce jugement.<br/>
<br/>
              3.	Il appartient au juge administratif, saisi d'un recours de pleine juridiction contre la décision infligeant une amende sur le fondement des articles L. 625-1 et L. 625-5 du code de l'entrée et du séjour des étrangers et du droit d'asile, de statuer sur le bien-fondé de la sanction attaquée et de réduire, le cas échéant, le montant de l'amende infligée en tenant compte de l'ensemble des circonstances de l'espèce. <br/>
<br/>
              4.	La constatation et la caractérisation de l'irrégularité qu'il est reproché au transporteur de ne pas avoir décelée relèvent, dès lors qu'elles sont exemptes de dénaturation, du pouvoir souverain des juges du fond. Si le caractère manifeste de ces irrégularités, dont l'absence de détection constitue un manquement du transporteur à ses obligations de contrôle de nature à justifier le prononcé d'une amende, est susceptible de faire l'objet d'un contrôle de qualification juridique de la part du juge de cassation, l'appréciation du caractère proportionné de la sanction au regard de la gravité des manquements constatés et des circonstances de l'espèce relève, pour sa part, de l'appréciation des juges du fond et n'est susceptible d'être remise en cause par le juge de cassation que dans le cas où la solution retenue est hors de proportion.<br/>
<br/>
              5.	Il résulte des dispositions précitées des articles L. 625-1 et L. 625-5 du code de l'entrée et du séjour des étrangers et du droit d'asile que les irrégularités manifestes qu'il appartient au transporteur de déceler sous peine d'amende lors, au moment de l'embarquement, du contrôle des documents requis, sont celles susceptibles d'apparaître à l'occasion d'un examen normalement attentif de ces documents par un agent du transporteur et que le transporteur peut être sanctionné alors même que l'irrégularité manifeste n'a pas été détectée par les autorités publiques compétentes pour délivrer les documents. Par suite, en jugeant que la circonstance que l'irrégularité retenue était passée inaperçue du service ayant apposé un visa Schengen sur le passeport n'était pas de nature à faire obstacle au prononcé d'une sanction, la cour administrative d'appel de Paris n'a pas commis d'erreur de droit. Par ailleurs, en estimant, par une appréciation souveraine exempte de dénaturation, que la mention " date d'expiration du passeport " recelait une faute aisément décelable à l'oeil nu par le personnel d'embarquement et en en déduisant, compte tenu de ce que cette anomalie portait sur une mention essentielle dans le contrôle de la validité du titre de voyage, que l'irrégularité constatée devait être regardée comme manifeste et donc comme justifiant le prononcé d'une sanction, la cour n'a pas entaché son arrêt d'une erreur de droit ni d'inexacte qualification juridique des faits. <br/>
<br/>
              6.	En jugeant toutefois que ces faits justifiaient que soit infligé à la société Air France le montant maximal de l'amende encourue, alors que le passeport présenté par l'étranger comportait un visa Schengen qui avait été apposé par les autorités compétentes et dont la validité n'était pas contestée, la cour a retenu une solution, quant au choix, par le ministre, du montant de la sanction, hors de proportion avec les manquements constatés. <br/>
<br/>
              7.	La société Air France est fondée à demander, pour ce motif, l'annulation de l'arrêt qu'elle attaque.<br/>
<br/>
              8.	Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond, en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              9.	En premier lieu, l'absence alléguée au dossier de la procédure contradictoire préalable à la sanction de l'original du passeport qui s'est révélé falsifié n'était pas de nature à priver la société Air France de la possibilité de faire valoir utilement ses observations dès lors que l'anomalie litigieuse était aisément décelable même sur la copie du document et que cette copie n'en a pas accentué le caractère manifeste.<br/>
<br/>
              10.	En second lieu, il résulte de ce qui a été dit au point 5 que les faits reprochés à la société Air France étaient de nature à justifier le prononcé d'une amende sur le fondement des dispositions précitées des articles L. 625-1 et L. 625-5 du code de l'entrée et du séjour des étrangers et du droit d'asile, sans qu'ait d'incidence sur ce point le fait que l'irrégularité manifeste entachant le passeport aurait échappé au service ayant apposé un visa Schengen sur le passeport, à supposer que cette irrégularité affectait le document qui lui avait été présenté. Si la société Air France a manqué à ses obligations de contrôle, cette circonstance devait toutefois être prise en compte pour fixer le montant de l'amende. Il y a lieu, dans ces circonstances particulières, de réduire le montant de l'amende infligée à 3 000 euros.<br/>
<br/>
              11.	Il résulte de ce qui précède que la société Air France est seulement fondée à demander la décharge partielle de l'amende infligée et la réformation du jugement attaqué sur ce point.<br/>
<br/>
              12.	Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par la société Air France au titre de l'article L. 761-1 du code de justice administrative devant le Conseil d'Etat et la cour administrative d'appel de Paris.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Paris du 10 décembre 2018 est annulé.<br/>
<br/>
Article 2 : Le montant de l'amende infligée à la société Air France est ramené de 5 000 euros à 3 000 euros.<br/>
<br/>
Article 3 : Le jugement du tribunal administratif de Paris du 3 octobre 2017 est réformé en ce qu'il a de contraire à la présente décision.<br/>
<br/>
Article 4 : Le surplus des conclusions de la société Air France présentées devant la cour administrative d'appel de Paris et le Conseil d'Etat est rejeté.<br/>
<br/>
Article 5 : La présente décision sera notifiée à la société Air France et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">335-005-01 ÉTRANGERS. ENTRÉE EN FRANCE. VISAS. - SANCTION D'UNE ENTREPRISE DE TRANSPORT AYANT DÉBARQUÉ EN FRANCE UN ÉTRANGER DÉPOURVU DES DOCUMENT DE VOYAGE ET VISA REQUIS (ART. L. 625-1 ET L. 625-5 DU CESEDA) - 1) CONDITIONS [RJ1] - A) CARACTÈRE DE LA FAUTE - DÉFAILLANCE À DÉCELER DES IRRÉGULARITÉS SUSCEPTIBLES D'APPARAÎTRE À L'OCCASION D'UN EXAMEN NORMALEMENT ATTENTIF - B) CIRCONSTANCE EXONÉRATOIRE - IRRÉGULARITÉ NON DÉTECTÉE PAR LES AUTORITÉS PUBLIQUES COMPÉTENTES POUR LA DÉLIVRANCE DES DOCUMENTS - ABSENCE - 2) CONTRÔLE DU JUGE DE CASSATION [RJ2] - A) SUR LA CONSTATATION DE L'IRRÉGULARITÉ - DÉNATURATION - B) SUR SON CARACTÈRE MANIFESTE - QUALIFICATION JURIDIQUE - C) SUR LE CARACTÈRE PROPORTIONNÉ DE LA SANCTION - VÉRIFICATION DE CE QUE LA SOLUTION RETENUE PAR LES JUGES DU FOND QUANT AU CHOIX DE LA SANCTION N'EST PAS HORS DE PROPORTION AVEC LES FAUTES COMMISES.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-08-02-02-01 PROCÉDURE. VOIES DE RECOURS. CASSATION. CONTRÔLE DU JUGE DE CASSATION. BIEN-FONDÉ. - DÉCISION DES JUGES DU FOND STATUANT SUR UNE SANCTION PROFESSIONNELLE [RJ2] - 1) SUR LA CONSTATATION DE L'IRRÉGULARITÉ - DÉNATURATION - 2) SUR SON CARACTÈRE MANIFESTE - QUALIFICATION JURIDIQUE - 3) SUR LE CARACTÈRE PROPORTIONNÉ DE LA SANCTION - VÉRIFICATION DE CE QUE LA SOLUTION RETENUE PAR LES JUGES DU FOND QUANT AU CHOIX DE LA SANCTION N'EST PAS HORS DE PROPORTION AVEC LES FAUTES COMMISES.
</SCT>
<ANA ID="9A"> 335-005-01 1) a) Les irrégularités manifestes qu'il appartient au transporteur de déceler, sous peine d'amende en application des articles L. 625-1 et L. 625-5 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA), lors, au moment de l'embarquement, du contrôle des documents requis, sont celles susceptibles d'apparaître à l'occasion d'un examen normalement attentif de ces documents par un agent du transporteur.,,,b) Le transporteur peut être sanctionné alors même que l'irrégularité manifeste n'a pas été détectée par les autorités publiques compétentes pour délivrer les documents.,,,2) Il appartient au juge administratif, saisi d'un recours de pleine juridiction contre la décision infligeant une amende sur le fondement des articles L. 625-1 et L. 625-5 du CESEDA, de statuer sur le bien-fondé de la sanction attaquée et de réduire, le cas échéant, le montant de l'amende infligée en tenant compte de l'ensemble des circonstances de l'espèce.,,,a) La constatation et la caractérisation de l'irrégularité qu'il est reproché au transporteur de ne pas avoir décelée relèvent, dès lors qu'elles sont exemptes de dénaturation, du pouvoir souverain des juges du fond.,,,b) Le caractère manifeste de ces irrégularités, dont l'absence de détection constitue un manquement du transporteur à ses obligations de contrôle de nature à justifier le prononcé d'une amende, est susceptible de faire l'objet d'un contrôle de qualification juridique de la part du juge de cassation.,,,c) L'appréciation du caractère proportionné de la sanction au regard de la gravité des manquements constatés et des circonstances de l'espèce relève de l'appréciation des juges du fond et n'est susceptible d'être remise en cause par le juge de cassation que dans le cas où la solution retenue est hors de proportion.</ANA>
<ANA ID="9B"> 54-08-02-02-01 Il appartient au juge administratif, saisi d'un recours de pleine juridiction contre la décision infligeant une amende sur le fondement des articles L. 625-1 et L. 625-5 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA), de statuer sur le bien-fondé de la sanction attaquée et de réduire, le cas échéant, le montant de l'amende infligée en tenant compte de l'ensemble des circonstances de l'espèce.,,,1) La constatation et la caractérisation de l'irrégularité qu'il est reproché au transporteur de ne pas avoir décelée relèvent, dès lors qu'elles sont exemptes de dénaturation, du pouvoir souverain des juges du fond.,,,2) Le caractère manifeste de ces irrégularités, dont l'absence de détection constitue un manquement du transporteur à ses obligations de contrôle de nature à justifier le prononcé d'une amende, est susceptible de faire l'objet d'un contrôle de qualification juridique de la part du juge de cassation.,,,3) L'appréciation du caractère proportionné de la sanction au regard de la gravité des manquements constatés et des circonstances de l'espèce relève de l'appréciation des juges du fond et n'est susceptible d'être remise en cause par le juge de cassation que dans le cas où la solution retenue est hors de proportion.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., Cons. const., 25 octobre 2019, n° 2019-810 QPC.,,[RJ2] Rappr., s'agissant des sanctions prononcées par le juge disciplinaire, CE, Assemblée, 30 décembre 2014, M.,, n° 381245, p. 443 ; s'agissant des décisions du juge de droit commun statuant sur des sanctions disciplinaires, CE, 27 février 2015, La Poste, n° 376598, 381828, p. 64.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
