<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025833600</ID>
<ANCIEN_ID>JG_L_2012_05_000000356455</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/83/36/CETATEXT000025833600.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème et 2ème sous-sections réunies, 09/05/2012, 356455</TITRE>
<DATE_DEC>2012-05-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>356455</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème et 2ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GASCHIGNARD ; SCP GATINEAU, FATTACCINI</AVOCATS>
<RAPPORTEUR>M. Frédéric Dieu</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Bertrand Dacosta</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:356455.20120509</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi et le mémoire complémentaire, enregistrés les 3 et 20 février 2012 au secrétariat du contentieux du Conseil d'Etat, présenté pour la COMMUNE DE SAINT-BENOIT, représentée par son maire ; la COMMUNE DE SAINT-BENOIT demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 1102922 du 18 janvier 2012 par laquelle le juge des référés du tribunal administratif de Poitiers, statuant en application de l'article L. 551-1 du code de justice administrative, a annulé, sur la demande de la SAS Penaud Frères, la procédure de passation du marché de fournitures de vêtements de travail et d'équipements de protection individuelle à destination des agents de la commune de Saint-Benoît ;<br/>
<br/>
              2°) de mettre à la charge de la société Penaud Frères une somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu l'ordonnance attaquée ;<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code des marchés publics ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Dieu, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Gaschignard, avocat de la COMMUNE DE SAINT-BENOIT et de la SCP Gatineau, Fattaccini, avocat de la société Penaud Frères ; <br/>
<br/>
              - les conclusions de M. Bertrand Dacosta, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Gaschignard, avocat de la COMMUNE DE SAINT-BENOIT et à la SCP Gatineau, Fattaccini, avocat de la société Penaud Frères ;<br/>
<br/>
<br/>Considérant qu'il résulte des dispositions de l'article L. 551-1 du code de justice administrative que le président du tribunal administratif ou le magistrat qu'il délègue peut être saisi, avant la conclusion d'un contrat de commande publique ou de délégation de service public, d'un manquement, par le pouvoir adjudicateur, à ses obligations de publicité et de mise en concurrence ; qu'aux termes de l'article L. 551-2 du même code : " Le juge peut ordonner à l'auteur du manquement de se conformer à ses obligations et suspendre l'exécution de toute décision qui se rapporte à la passation du contrat, sauf s'il estime, en considération de l'ensemble des intérêts susceptibles d'être lésés et notamment de l'intérêt public, que les conséquences négatives de ces mesures pourraient l'emporter sur leurs avantages. / Il peut, en outre, annuler les décisions qui se rapportent à la passation du contrat et supprimer les clauses ou prescriptions destinées à figurer dans le contrat et qui méconnaissent lesdites obligations " ;<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis au juge des référés que, par un avis d'appel public à la concurrence publié le 24 octobre 2011, la COMMUNE DE SAINT-BENOIT a lancé une procédure d'appel d'offres en vue de l'attribution d'un marché à bons de commande ayant pour objet la fourniture de vêtements de travail et d'équipements de protection individuelle pour ses agents ; que quatre sociétés ont présenté des offres, parmi lesquelles la société Penaud Frères et la société Vet'work ; que, par courrier daté du 14 décembre 2011, la COMMUNE DE SAINT-BENOIT a informé la société Penaud Frères que son offre n'avait pas été retenue et que le marché avait été attribué à la société Vet'work ; que, par l'ordonnance attaquée, le juge des référés du tribunal administratif de Poitiers, saisi par la société Penaud Frères, a, sur le fondement de l'article L. 551-1 du code de justice administrative, annulé la procédure de passation de ce marché à compter de l'examen des candidatures ;<br/>
<br/>
              Considérant qu'aux termes de l'article 45 du code des marchés publics, applicable au marché litigieux : " - I. - Le pouvoir adjudicateur ne peut exiger des candidats que des renseignements ou documents permettant d'évaluer leur expérience, leurs capacités professionnelles, techniques et financières ainsi que des documents relatifs aux pouvoirs des personnes habilitées à les engager (...) III. (...) Si le candidat est objectivement dans l'impossibilité de produire, pour justifier de sa capacité financière, l'un des renseignements ou documents prévus par l'arrêté mentionné au I et demandés par le pouvoir adjudicateur, il peut prouver sa capacité par tout autre document considéré comme équivalent par le pouvoir adjudicateur. " ; qu'aux termes de l'article 52 du code des marchés publics, également applicable au marché litigieux : " I - (...) Les candidatures (...) sont examinées au regard des niveaux de capacités professionnelles, techniques et financières mentionnées dans l'avis d'appel public à la concurrence, ou, s'il s'agit d'une procédure dispensée de l'envoi d'un tel avis, dans le règlement de la consultation. Les candidatures qui ne satisfont pas à ces niveaux de capacité sont éliminées. / L'absence de références relatives à l'exécution de marchés de même nature ne peut justifier l'élimination d'un candidat et ne dispense pas le pouvoir adjudicateur d'examiner les capacités professionnelles, techniques et financières des candidats. " ;<br/>
<br/>
              Considérant qu'il résulte de ces dispositions que s'il est loisible à l'acheteur public d'exiger la détention, par les candidats à l'attribution d'un marché public, de documents comptables et de références de nature à attester de leurs capacités, il doit néanmoins, lorsque cette exigence  a pour effet de restreindre l'accès au marché à des entreprises de création récente, permettre aux candidats qui sont dans l'impossibilité objective de produire les documents et renseignements exigés par le règlement de la consultation, de justifier de leurs capacités financières et de leurs références professionnelles par tout autre moyen ; <br/>
<br/>
              Considérant que, pour annuler la procédure de passation litigieuse, le juge des référés du tribunal administratif de Poitiers a jugé que la COMMUNE DE SAINT-BENOIT avait méconnu le règlement de la consultation et ses obligations de mise en concurrence en retenant la candidature de la société Vet'work, alors que cette société n'avait pu fournir les déclarations de chiffre d'affaires des trois derniers exercices et les références des prestations similaires exécutées au cours des trois dernières années, qui étaient exigées des candidats par le règlement de la consultation pour justifier de leurs capacités professionnelles, techniques et financières sans que puisse être prise en considération la circonstance qu'elle était de création récente ; qu'il résulte de ce qui précède qu'en jugeant ainsi que la mention, dans le règlement de la consultation, de ces seuls documents et renseignements imposait au pouvoir adjudicateur de rejeter la candidature des entreprises de création récente qui n'avaient pu les fournir, alors qu'il lui incombait, en application des articles 45 et 52 du code des marchés publics, de permettre aux entreprises de création récente de justifier de leurs capacités financières, techniques et références professionnelles par tout autre moyen, le juge des référés du tribunal administratif de Poitiers a commis une erreur de droit ; que, par suite et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, la COMMUNE DE SAINT-BENOIT est fondée à demander l'annulation de l'ordonnance attaquée ;<br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au titre de la procédure de référé engagée par la société Penaud Frères, dans la mesure de l'annulation prononcée, en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              Sur les fins de non-recevoir opposées par la COMMUNE DE SAINT-BENOIT :<br/>
<br/>
              Considérant, en premier lieu, que la société Penaud Frères ayant acquitté la contribution pour l'aide juridique prévue par les dispositions de l'article 1635 bis Q du code général des impôts, la fin de non recevoir tirée de ce que sa demande serait irrecevable en application des dispositions de l'article R. 411-2 du code de justice administrative ne peut qu'être écartée ;<br/>
<br/>
              Considérant, en deuxième lieu, qu'il résulte de l'instruction que la demande de la société Penaud Frères tendant à la suspension de la procédure de passation du marché est présentée sur le fondement des dispositions de l'article L. 551-1 du code de justice administrative et non sur le fondement des dispositions de l'article L. 521-1 du même code ; que, par suite, la fin de non recevoir tirée de la " confusion " des conclusions présentées par la société Penaud Frères doit être écartée ;<br/>
<br/>
              Considérant, en dernier lieu, que les dispositions de l'article R. 551-1 du code de justice administrative, prévues dans l'intérêt de l'auteur du référé en vue d'éviter que le marché contesté ne soit prématurément signé par le pouvoir adjudicateur resté dans l'ignorance de l'introduction d'un recours, ne sont pas prescrites à peine d'irrecevabilité de ce recours ; qu'il suit de là que la fin de non recevoir opposée par la COMMUNE DE SAINT-BENOIT et tirée de ce qu'en l'absence de notification de son recours, la demande de la société Penaud Frères est irrecevable, doit être écartée ;<br/>
<br/>
              Sur les conclusions dirigées contre la procédure de passation :<br/>
<br/>
              Considérant, d'une part, que si le règlement de la consultation, exigeait des candidats la production d'une déclaration concernant le chiffre d'affaires global au cours des trois derniers exercices disponibles et d'une liste des principales fournitures livrées au cours des trois dernières années, il appartenait à la COMMUNE DE SAINT-BENOIT, ainsi qu'il vient d'être dit, de permettre aux entreprises de création récente telle que la société Vet'work de justifier de leurs capacités financières et  techniques et de leurs références  professionnelles par tout autre moyen ; <br/>
<br/>
              Considérant toutefois, d'autre part, que, pour justifier de sa capacité financière, la société Vet'work s'est bornée à produire une " attestation de bonne tenue de compte " rédigée sur papier sans en-tête par son conseiller bancaire et indiquant seulement que les comptes bancaires de la société fonctionnaient normalement, qu'ils n'avaient fait l'objet d'aucun incident de paiement et que la société était à jour de ses engagements contractés auprès de l'établissement bancaire ; qu'une telle attestation ne pouvant suffire à établir la capacité financière de la société Vet'work à exécuter le marché, cette dernière ne pouvait être regardée par la COMMUNE DE SAINT-BENOIT comme ayant justifié de sa capacité financière ; que, par suite, la COMMUNE DE SAINT-BENOIT ne pouvait, sans entacher sa décision d'illégalité et manquer à ses obligations de mise en concurrence, retenir la candidature de la société Vet'work ; que ce manquement est susceptible d'avoir lésé la société Penaud Frères, dont il ne résulte pas de l'instruction que sa candidature était irrégulière pour un motif étranger à ce manquement, dès lors que celui-ci était susceptible de permettre, et a d'ailleurs permis, à la société Vet'work, qui n'aurait pas disposé des garanties financières requises pour exécuter le marché, d'être retenue ; que, dès lors, la procédure de passation litigieuse doit être annulée à compter de l'examen des offres ; qu'il y a lieu d'enjoindre à la COMMUNE DE SAINT-BENOIT, si elle entend conclure le marché, de la reprendre à ce stade, au regard des motifs de la présente décision ;<br/>
<br/>
              Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de la société Penaud Frères, qui n'est pas, dans la présente instance, la partie perdante, la somme que demande la COMMUNE DE SAINT-BENOIT au titre des frais exposés par elle et non compris dans les dépens ; qu'il y a lieu en revanche, dans les circonstances de l'espèce, de mettre à la charge de cette dernière une somme de 4 500 euros à verser à la société Penaud Frères pour la procédure suivie devant le Conseil d'Etat et le tribunal administratif de Poitiers ; qu'il y a lieu enfin de rejeter la demande présentée par la société Vet'work au même titre ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du 18 janvier 2012 du juge des référés du tribunal administratif de Poitiers est annulée.<br/>
<br/>
Article 2 : La procédure de passation du marché ayant pour objet la fourniture de vêtements de travail et d'équipements de protection individuelle pour les agents de la COMMUNE DE SAINT-BENOIT est annulée à compter de l'examen des offres.<br/>
<br/>
Article 3 : Il est enjoint à la COMMUNE DE SAINT-BENOIT, si elle entend poursuivre la procédure, de la reprendre au stade de l'examen des offres.<br/>
<br/>
Article 4 : La COMMUNE DE SAINT-BENOIT versera une somme de 4 500 euros à la société Penaud Frères en application des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 5 : Les conclusions présentées par la COMMUNE DE SAINT-BENOIT et la société Vet'work en application des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 6 : La présente décision sera notifiée à la COMMUNE DE SAINT-BENOIT, à la société Penaud Frères et à la société Vet'Work.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-02-005 MARCHÉS ET CONTRATS ADMINISTRATIFS. FORMATION DES CONTRATS ET MARCHÉS. FORMALITÉS DE PUBLICITÉ ET DE MISE EN CONCURRENCE. - EXAMEN DES CANDIDATURES - EVALUATION DES CAPACITÉS PROFESSIONNELLES, TECHNIQUES ET FINANCIÈRES DES CANDIDATS (ART. 45 DU CODE DES MARCHÉS PUBLICS) - POSSIBILITÉ D'EXIGER DES DOCUMENTS COMPTABLES - EXISTENCE - EXCEPTION - CAS OÙ CETTE EXIGENCE A POUR EFFET DE RESTREINDRE L'ACCÈS AU MARCHÉ À DES ENTREPRISES DE CRÉATION RÉCENTE - CONSÉQUENCE - OBLIGATION DE PERMETTRE À CES ENTREPRISES DE JUSTIFIER DE LEURS CAPACITÉS PAR TOUT AUTRE MOYEN [RJ1].
</SCT>
<ANA ID="9A"> 39-02-005 S'il est loisible à l'acheteur public d'exiger la détention, par les candidats à l'attribution d'un marché public, de documents comptables et de références de nature à attester de leurs capacités financières, techniques et professionnelles, il doit néanmoins, lorsque cette exigence a pour effet de restreindre l'accès des entreprises de création récente au marché, permettre aux candidats qui sont dans l'incapacité objective de produire les documents et renseignements exigés par le règlement de la consultation de justifier de leurs capacités par tout autre moyen.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Comp. 10 mai 2006, Société Bronzo, n° 281976, p. 240.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
