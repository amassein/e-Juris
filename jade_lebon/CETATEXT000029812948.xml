<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029812948</ID>
<ANCIEN_ID>JG_L_2014_11_000000363146</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/81/29/CETATEXT000029812948.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 4ème SSR, 28/11/2014, 363146, Publié au recueil Lebon</TITRE>
<DATE_DEC>2014-11-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>363146</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 4ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOUTET, HOURDEAUX</AVOCATS>
<RAPPORTEUR>Mme Leïla Derouich</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2014:363146.20141128</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la requête sommaire et le mémoire complémentaire, enregistrés les 1er octobre et 31 décembre 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour la société NRJ réseau, dont le siège est 22, rue Boileau à Paris (75016), représentée par son président directeur général ; la société NRJ réseau demande au Conseil d'Etat d'annuler la décision du 28 février 2012 par laquelle le Conseil supérieur de l'audiovisuel l'a mise en demeure de respecter les conventions fixant les obligations particulières des services radiophoniques "NRJ Beauvais" et "NRJ Pays de Bray" et la décision du 27 juillet 2012 par laquelle le Conseil supérieur a refusé de l'autoriser à pratiquer une syndication totale de ses programmes depuis Beauvais ;  <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi n° 86-1067 du 30 septembre 1986 ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Leïla Derouich, auditeur,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boutet, Hourdeaux, avocat de la société NRJ RESEAU ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article 28 de la loi du 30 septembre 1986 relative à la liberté de communication, dans sa rédaction en vigueur aux dates des décisions attaquées : " La délivrance des autorisations d'usage de la ressource radioélectrique pour chaque nouveau service diffusé par voie hertzienne terrestre autre que ceux exploités par les sociétés nationales de programme, est subordonnée à la conclusion d'une convention passée entre le Conseil supérieur de l'audiovisuel au nom de l'Etat et la personne qui demande l'autorisation. Dans le respect de l'honnêteté et du pluralisme de l'information et des programmes et des règles générales fixées en application de la présente loi et notamment de son article 27, cette convention fixe les règles particulières applicables au service, compte tenu de l'étendue de la zone desservie, de la part du service dans le marché publicitaire, du respect de l'égalité de traitement entre les différents services et des conditions de concurrence propres à chacun d'eux, ainsi que du développement de la radio et de la télévision numériques de terre. /  La convention porte notamment sur un ou plusieurs des points suivants : / 1° La durée et les caractéristiques générales du programme propre ; (...) La convention mentionnée au premier alinéa définit également les prérogatives et notamment les pénalités contractuelles dont dispose le Conseil supérieur de l'audiovisuel pour assurer le respect des obligations conventionnelles " ; <br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier que, par une décision du 24 juillet 2007, le Conseil supérieur de l'audiovisuel (CSA) a autorisé la société NRJ réseau à exploiter, dans la zone de Beauvais, le service radiophonique " NRJ Beauvais " ; que, par une décision du 5 avril 2011, il a autorisé la même société à exploiter, dans la zone de Gournay-en-Bray, le service " NRJ Pays de Bray " ; qu'il résulte des conventions fixant, en application de l'article 28 précité de la loi du 30 septembre 1986, les règles particulières applicables à ces deux services qu'ils diffusent une émission musicale interactive identique d'une durée de quatre heures par jour produite par NRJ Beauvais et que chacun doit en outre réaliser et diffuser dans sa zone d'exploitation un programme spécifique d'informations locales d'une durée quotidienne d'environ vingt minutes ; que, par une décision du 28 février 2012, le CSA, estimant que la société NRJ réseau ne respectait pas cette dernière obligation, l'a mise en demeure de s'y conformer dans un délai d'un mois ; que, par un courrier du 19 avril 2012, la société a demandé à être autorisée à diffuser dans la zone de Gournay-en-Bray un programme en tout point identique au programme diffusé à Beauvais ; que le CSA lui a opposé un refus le 27 juillet 2012 ; que la société demande l'annulation des décisions des 28 février et 27 juillet 2012 ; <br/>
<br/>
              Sur la mise en demeure du 28 février 2012 : <br/>
<br/>
              3. Considérant, en premier lieu, que la décision par laquelle le CSA met le titulaire de l'autorisation d'exploiter un service audiovisuel en demeure de se conformer à ses obligations, qui n'inflige pas une sanction, n'a pas à être précédée d'une procédure contradictoire ; qu'ainsi le moyen tiré de ce que la mise en demeure litigieuse serait illégale faute pour le CSA d'avoir préalablement recueilli les observations de la société NRJ réseau ne saurait être accueilli ; <br/>
<br/>
              4. Considérant, en deuxième lieu, que la décision attaquée énonce les éléments de droit et de fait sur lesquels le CSA s'est fondé pour prononcer la mise en demeure ; que, par suite, le moyen tiré de ce que la décision du 28 février 2012 serait insuffisamment motivée doit être écarté ;<br/>
<br/>
              5. Considérant, en dernier lieu, qu'il est constant que, contrairement aux prescriptions des conventions définissant leurs obligations particulières, les services " NRJ Beauvais " et " NRJ Pays-de-Bray " diffusaient des programmes locaux identiques ; que, dès lors, le CSA a estimé à bon droit que la société NRJ réseau ne respectait pas certaines prescriptions des conventions relatives à ces services et a pu légalement la mettre en demeure de respecter ses obligations en réalisant des programmes locaux distincts pour ces deux zones ; <br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que la société NRJ réseau n'est pas fondée à demander l'annulation de la mise en demeure du 28 février 2012 ni du rejet de son recours gracieux contre cette mesure ; <br/>
<br/>
              Sur le refus d'autoriser la société NRJ réseau à diffuser un programme local identique dans les zones de Beauvais et de Gournay-en-Bray : <br/>
<br/>
              7. Considérant que les dispositions précitées de l'article 28 de la loi du 30 septembre 1986, qui subordonnent la délivrance des autorisations d'usage de la ressource radioélectrique pour la diffusion d'un service diffusé par voie hertzienne terrestre à la conclusion entre le CSA et la personne qui demande l'autorisation d'une convention fixant les règles particulières applicables au service, ne font pas obstacle à ce que les conventions ainsi conclues fassent l'objet de modifications à la demande du titulaire de l'autorisation ; que, saisi par le titulaire d'une autorisation d'exploiter un service radiophonique d'une demande tendant à ce que la convention afférente à ce service soit modifiée en ce qui concerne le programme à diffuser, le CSA est tenu de la rejeter dans le cas où la modification sollicitée revêt, du fait de son objet ou de son ampleur, un caractère substantiel ; que, si tel n'est pas le cas, il appartient au CSA, sous le contrôle du juge, d'apprécier si l'intérêt du public lui permet, au regard des impératifs prioritaires mentionnés à l'article 29 de la loi du 30 septembre 1986, notamment de la sauvegarde du pluralisme des courants d'expression socio-culturels, et compte tenu, en particulier, des critères mentionnés aux 4° et 6° du même article, d'accepter de modifier la convention ; <br/>
<br/>
              8. Considérant qu'en demandant à être autorisée à diffuser dans les zones de Gournay-en-Bray et de Beauvais des programmes en tout point identiques, la société NRJ réseau a sollicité une modification des programmes prévus par les conventions définissant les règles particulières applicables aux services " NRJ Pays de Bray " et " NRJ Beauvais ", qui prévoient la diffusion par chacun de ces services d'un programme d'informations locales spécifique à sa zone de diffusion ; que la modification ainsi sollicitée ne conduisait pas à la suppression de toute information d'intérêt local, mais seulement à la diffusion, dans les deux zones, distantes d'une trentaine de kilomètres, d'un même programme d'informations locales, et était d'ailleurs justifiée, par la société demanderesse, par la difficulté de produire un programme local quotidien spécifique à la zone de Gournay-en-Bray, qui compte moins de 8 000 habitants  ; qu'elle ne pouvait ainsi être regardée comme revêtant un caractère substantiel et comme étant, pour ce motif,  au nombre de celles que le CSA était tenu de refuser ; que, par ailleurs, cette modification, qui conservait aux deux services une dimension locale, ne portait pas atteinte à l'impératif de sauvegarde du pluralisme ; que le CSA a commis une erreur d'appréciation sur ce point ; que, par suite, sans qu'il soit besoin d'examiner les autres moyens de la requête, la société NRJ réseau est fondée à soutenir que le CSA a inexactement appliqué la loi en rejetant sa demande ; que la décision attaquée doit ainsi être annulée ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
		Article 1er : La décision du CSA du 27 juillet 2012 est annulée. <br/>
<br/>
		Article 2 : Le surplus des conclusions de la société NRJ réseau est rejeté. <br/>
<br/>
Article 3 : La présente décision sera notifiée à la société NRJ réseau et au Conseil supérieur de l'audiovisuel. <br/>
Copie en sera adressée pour information à la ministre de la culture et de la communication.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">56-01 RADIO ET TÉLÉVISION. CONSEIL SUPÉRIEUR DE L'AUDIOVISUEL. - AUTORISATION D'UN SERVICE DIFFUSÉ PAR VOIE HERTZIENNE TERRESTRE - POSSIBILITÉ DE MODIFIER LE CONTENU DES PROGRAMMES FIXÉ PAR LA CONVENTION - EXISTENCE - CONDITIONS - MODIFICATION NON SUBSTANTIELLE CONFORME À L'INTÉRÊT DU PUBLIC [RJ1].
</SCT>
<ANA ID="9A"> 56-01 Les dispositions de l'article 28 de la loi n°86-1067 du 30 septembre 1986, qui subordonnent la délivrance des autorisations d'usage de la ressource radioélectrique pour la diffusion d'un service diffusé par voie hertzienne terrestre à la conclusion entre le Conseil supérieur de l'audiovisuel (CSA) et la personne qui demande l'autorisation d'une convention fixant les règles particulières applicables au service, ne font pas obstacle à ce que les conventions ainsi conclues fassent l'objet de modifications à la demande du titulaire de l'autorisation. Saisi par le titulaire d'une autorisation d'exploiter un service radiophonique d'une demande tendant à ce que la convention afférente à ce service soit modifiée en ce qui concerne le programme à diffuser, le CSA est tenu de la rejeter dans le cas où la modification sollicitée revêt, du fait de son objet ou de son ampleur, un caractère substantiel. Si tel n'est pas le cas, il appartient au CSA, sous le contrôle du juge, d'apprécier si l'intérêt du public lui permet, au regard des impératifs prioritaires mentionnés à l'article 29 de la loi du 30 septembre 1986, notamment de la sauvegarde du pluralisme des courants d'expression socio-culturels, et compte tenu, en particulier, des critères mentionnés aux 4° et 6° du même article, d'accepter de modifier la convention.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Comp. s'agissant non d'une modification du contenu des programmes fixé par la convention mais de la vérification, au regard des dispositions de l'article 42-3 de la loi du 30 septembre 1986, de la légalité de l'agrément donné par le CSA à la cession de l'intégralité du capital d'une société titulaire d'une autorisation de radiodiffusion, CE, assemblée, 23 décembre 2013, Société Métropole Télévision M6, n° 363978, p. 328 ; CE, 11 avril 2014, Syndicat des réseaux radiophoniques nationaux, n° 348972, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
