<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043351180</ID>
<ANCIEN_ID>JG_L_2021_04_000000433162</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/35/11/CETATEXT000043351180.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 4ème chambres réunies, 07/04/2021, 433162</TITRE>
<DATE_DEC>2021-04-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>433162</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROUSSEAU, TAPIE</AVOCATS>
<RAPPORTEUR>M. Frédéric Pacoud</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie Sirinelli</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:433162.20210407</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 31 juillet et 31 octobre 2019 et le 9 juin 2020 au secrétariat du contentieux du Conseil d'Etat, la société Teofarma demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 27 juin 2019, notifiée le 10 juillet 2019, par laquelle le Comité économique des produits de santé a rejeté sa demande de modification du prix des spécialités Alepsal 50 mg (phénobarbital, caféine anhydre) comprimés (B/30), Alepsal 100 mg (phénobarbital, caféine anhydre) comprimés (B/30), Alepsal 15 mg (phénobarbital, caféine anhydre) comprimés (B/30) et Alepsal 150 mg (phénobarbital, caféine anhydre), comprimés (B/30) ; <br/>
<br/>
              2°) d'enjoindre au Comité économique des produits de santé, sous astreinte de 500 euros par jour de retard, de faire droit à sa demande de modification du prix des spécialités en litige ou, subsidiairement, de se prononcer de nouveau sur celle-ci dans un délai d'un mois à compter de la notification de la décision du Conseil d'Etat ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la directive 89/105/CEE du Conseil du 21 décembre 1988 ;<br/>
              - le code des relations entre le public et l'administration ;<br/>
              - le code de la sécurité sociale ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Pacoud, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Marie Sirinelli, rapporteure publique ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Rousseau, Tapie, avocat de la société Teofarma ;<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du I de l'article L. 162-16-4 du code de la sécurité sociale, dans sa rédaction en vigueur à la date de la décision attaquée : " Le prix de vente au public de chacun des médicaments mentionnés au premier alinéa de l'article L. 162-17 est fixé par convention entre l'entreprise exploitant le médicament et le Comité économique des produits de santé conformément à l'article L. 162-17-4 ou, à défaut, par décision du comité, sauf opposition conjointe des ministres concernés qui arrêtent dans ce cas le prix dans un délai de quinze jours après la décision du comité. La fixation de ce prix tient compte principalement de l'amélioration du service médical rendu par le médicament, le cas échéant des résultats de l'évaluation médico-économique, des prix des médicaments à même visée thérapeutique, des volumes de vente prévus ou constatés ainsi que des conditions prévisibles et réelles d'utilisation du médicament. (...) ". <br/>
<br/>
              2. Il ressort des pièces du dossier que la société Teofarma a sollicité du Comité économique des produits de santé la hausse du prix des spécialités antiépileptiques qu'elle exploite, dénommées Alepsal 15 milligrammes (phénobarbital, caféine anhydre) comprimés (B/30), Alepsal 50 milligrammes (phénobarbital, caféine anhydre) comprimés (B/30), Alepsal 100 milligrammes (phénobarbital, caféine anhydre) comprimés (B/30) et Alepsal 150 milligrammes (phénobarbital, caféine anhydre) comprimés (B/30), en faisant valoir la perte financière engendrée par l'exploitation de ces médicaments. Par une décision délibérée le 27 juin 2019 et notifiée le 10 juillet 2019, dont la société requérante demande l'annulation pour excès de pouvoir, le Comité économique des produits de santé a refusé de faire droit à cette demande. <br/>
<br/>
              Sur la légalité externe de la décision attaquée :<br/>
<br/>
              3. En premier lieu, le premier alinéa de l'article D. 162-2-5 du code de la sécurité sociale dispose que : " Le Comité économique des produits de santé se réunit sur convocation de son président. Le président fixe l'ordre du jour des séances. Les délibérations du Comité économique des produits de santé ne sont valables que si au moins six de ses membres ayant voix délibérative sont présents ". Il ressort des pièces du dossier que, d'une part, une convocation à la séance du 27 juin 2019, assortie de l'ordre du jour, a été adressée par courriel aux membres du Comité économique des produits de santé six jours avant cette date, sans que les allégations insuffisamment étayées de la société requérante permettent de mettre en doute la réception de ce courriel par l'ensemble de ses destinataires. Il en ressort, d'autre part, que neuf membres ayant voix délibérative étaient présents lors de cette réunion. Par suite, la société requérante n'est pas fondée à soutenir que les membres du Comité économique des produits de santé n'ont pas été régulièrement convoqués à la réunion au cours de laquelle a été prise la décision attaquée et que la condition de quorum posée par l'article précité n'était pas remplie.<br/>
<br/>
              4. En deuxième lieu, il résulte des dispositions de l'article L. 162-16-4 du code de la sécurité sociale citées au point 1 que les conventions et décisions fixant le prix de vente au public de chacun des médicaments mentionnés au premier alinéa de l'article L. 162-17 relèvent de la compétence du collège des membres du Comité économique des produits de santé. En outre, d'une part, le premier alinéa de l'article L. 212-1 du code des relations entre le public et l'administration, applicable aux actes réglementaires en vertu de l'article L. 200-1 du même code, prévoit que : " Toute décision prise par une administration comporte la signature de son auteur ainsi que la mention, en caractères lisibles, du prénom, du nom et de la qualité de celuici ". S'agissant d'un organisme collégial, il est satisfait à ces exigences dès lors que la décision prise comporte la signature de son président, accompagnée des mentions prévues par cet article. D'autre part, les troisième et quatrième alinéas de l'article D. 162-2-5 du code de la sécurité sociale prévoient que le président du Comité économique des produits de santé signe les conventions passées et les décisions prises, notamment, en application de l'article L. 162164 de ce code et qu'en cas d'absence ou d'empêchement du président, ses pouvoirs sont exercés, pour les missions définies à cet article, par le vice-président chargé du médicament, le président ou le vice-président authentifiant ainsi les conventions et décisions par leur signature.<br/>
<br/>
              5. Il ressort des pièces du dossier que la décision attaquée été adoptée par une délibération du collège du 27 juin 2019, dont le relevé a été signé par M. B... A..., vice-président chargé du médicament, ainsi qu'il y était habilité par sa seule nomination en cette qualité, et que sa signature est accompagnée de la mention de son prénom, de son nom et de sa qualité. Par suite, les moyens tirés de ce que la décision attaquée aurait été prise par une autorité incompétente et serait irrégulière, faute de comporter les mentions requises, doivent être écartés, la circonstance que le courrier du 10 juillet 2019 portant la décision à la connaissance de la société requérante ne comportait pas lui-même ces mentions étant inopérante. <br/>
<br/>
              6. En dernier lieu, aux termes de l'article 3 de la directive 89/105/CEE du Conseil du 21 décembre 1988 concernant la transparence des mesures régissant la fixation des prix des médicaments à usage humain et leur inclusion dans le champ d'application des systèmes nationaux d'assurance-maladie : " (...) les dispositions suivantes s'appliquent lorsqu'une augmentation du prix d'un médicament n'est autorisée qu'après l'obtention d'une autorisation préalable des autorités compétentes : / (...) 2. Lorsque les autorités compétentes décident de ne pas autoriser, en totalité ou en partie, l'augmentation de prix demandée, la décision comporte un exposé des motifs fondé sur des critères objectifs et vérifiables (...) ". Pour assurer la transposition de ces dispositions, l'article R. 163-14 du code de la sécurité sociale prévoit que les décisions portant refus de modification du prix d'un médicament sont communiquées à l'entreprise avec la mention de leurs motifs.<br/>
<br/>
              7. Il résulte des termes mêmes du courrier du 10 juillet 2019 portant la décision attaquée à la connaissance de la société requérante que le Comité économique des produits de santé a refusé de faire droit à sa demande d'augmentation du prix des spécialités qu'elle commercialise aux motifs, d'une part, que ces spécialités n'apportaient pas d'amélioration du service médical rendu et, d'autre part, s'agissant des spécialités Alepsal 50 milligrammes et Alepsal 100 milligrammes, que leur prix fabricant hors taxes était plus élevé que celui de leurs comparateurs Gardenal 50 milligrammes et Gardenal 100 milligrammes et, s'agissant des spécialités Alepsal 15 milligrammes et Alepsal 150 milligrammes, pouvant être regardées comme des spécialités répondant à un besoin thérapeutique non couvert par une autre spécialité moins coûteuse, que les conditions d'exploitation de ces spécialités présentées par la société requérante, permettant d'analyser leur prix de revient industriel, ne justifiaient pas la hausse de leur prix. En indiquant les composantes du prix de revient industriel qu'il retenait, le comité a exposé avec une précision suffisante les éléments sur lesquels il se fondait et en mentionnant, au titre du critère des conditions d'utilisation du médicament, l'existence d'alternatives thérapeutiques efficaces et mieux tolérées, il s'est prononcé de façon suffisamment intelligible. Par suite, la société requérante n'est pas fondée à soutenir que la décision du Comité économique des produits de santé n'aurait pas été assortie d'une motivation conforme aux exigences énoncées au point 6.<br/>
<br/>
              Sur la légalité interne de la décision attaquée :<br/>
<br/>
              8. Sous réserve des cas dans lesquels l'évolution du prix de la spécialité remboursable a été prévue par convention avec l'entreprise exploitant le médicament, il appartient au Comité économique des produits de santé, saisi d'une demande en ce sens de l'entreprise, d'apprécier s'il y a lieu de procéder à la modification de prix sollicitée au regard notamment des critères mentionnés à l'article L. 162-16-4 du code de la sécurité sociale cité au point 1. <br/>
<br/>
              En ce qui concerne les spécialités Alepsal 50 milligrammes et Alepsal 100 milligrammes :<br/>
<br/>
              9. Il résulte des dispositions du 1° de l'article R. 163-5 du code de la sécurité sociale que ne peuvent être inscrits sur la liste des médicaments remboursables par l'assurance maladie prévue au premier alinéa de l'article L. 162-17 de ce code les médicaments qui n'apportent ni amélioration du service médical rendu appréciée par la commission de la transparence de la Haute Autorité de santé, ni économie dans le coût du traitement médicamenteux. Il suit de là que le Comité économique des produits de santé est, en particulier, fondé à refuser l'augmentation du prix d'un médicament n'apportant pas d'amélioration du service médical rendu lorsqu'il en résulterait un coût du traitement par ce médicament supérieur à celui des alternatives thérapeutiques existantes. Pour la mise en oeuvre des dispositions de l'article L. 162-16-4 du code de la sécurité sociale, il incombe au Comité économique des produits de santé, notamment, de déterminer dans chaque cas, sur la base de critères objectifs et vérifiables, la méthode de comparaison des prix des médicaments à même visée thérapeutique la plus adaptée aux caractéristiques des spécialités en cause, en tenant compte des conditions réelles et prévisibles d'utilisation de cellesci. En outre, eu égard à la lettre même de l'article L. 162-16-4 du code de la sécurité sociale et à l'objectif, poursuivi par ces dispositions, de maîtrise du coût des spécialités remboursées par l'assurance maladie, la comparaison doit porter, en principe, sur le prix de vente au public.<br/>
<br/>
              10. En premier lieu, il ressort des pièces du dossier que, pour refuser à la société Teofarma l'augmentation du prix de la spécialité Alepsal 100 au motif qu'elle est plus coûteuse que les médicaments à même visée thérapeutique, le Comité économique des produits de santé a comparé le prix fabricant hors taxes par boîte de cette spécialité, de 1,55 euros, avec celui de la spécialité Gardenal 100, de 1,26 euros, alors que la première est conditionnée par boîtes de 30 comprimés, conduisant à un coût de 0,052 euros par comprimé, et la seconde par boîte de 20 comprimés, soit 0,063 euros par comprimé. Le comité, qui ne fait valoir aucun élément tenant aux caractéristiques des spécialités en cause, dans leurs conditions réelles et prévisibles d'utilisation, ne justifie pas le choix de se fonder sur le coût par unité de conditionnement plutôt que sur celui du traitement journalier pour comparer le prix de spécialités destinées au traitement de fond d'une affection chronique. Il suit de là qu'en regardant la spécialité Alepsal 100 comme plus coûteuse, le comité a, en l'absence de l'invocation d'une différence de posologie entre les deux spécialités, fondé sa décision sur des faits matériellement inexacts. Par suite, la société requérante est fondée à soutenir que la décision qu'elle attaque est illégale pour ce motif en tant qu'elle porte sur la spécialité Alepsal 100.<br/>
<br/>
              11. En second lieu, si la société Teofarma fait valoir que le Comité économique des produits de santé a comparé les prix fabricant hors taxes des spécialités Alepsal 50 et Gardenal 50, au lieu de leur prix de vente au public, elle ne conteste pas que ce choix est, en l'espèce, resté sans incidence sur le niveau comparé des prix des deux spécialités, plus élevé pour Alepsal 50. En l'absence d'amélioration du service médical rendu apportée par la spécialité, laquelle n'est pas davantage contestée par la société requérante, le comité pouvait se fonder sur cette seule circonstance pour rejeter sa demande. La société requérante n'est, dès lors, pas fondée à soutenir que la décision qu'elle attaque serait illégale en tant qu'elle porte sur la spécialité Alepsal 50.<br/>
<br/>
              En ce qui concerne les spécialités Alepsal 15 milligrammes et Alepsal 150 milligrammes :<br/>
<br/>
              12. Aux termes de l'article L. 162-17-4 du code de la sécurité sociale, dans sa rédaction applicable à la décision attaquée : " En application des orientations qu'il reçoit annuellement des ministres compétents, le Comité économique des produits de santé peut conclure avec des entreprises ou groupes d'entreprises des conventions d'une durée maximum de quatre années relatives à un ou à des médicaments visés (...) au premier et deuxième alinéas l'article L. 162-17. (...) Ces conventions, dont le cadre peut être précisé par un accord conclu avec un ou plusieurs syndicats représentatifs des entreprises concernées, déterminent les relations entre le comité et chaque entreprise, et notamment : / 1° Le prix mentionné à l'article L. 162-16-5 de ces médicaments (...) et, le cas échéant, l'évolution de ce prix, notamment en fonction des volumes de vente ". L'article 16 de l'accord-cadre conclu en application de ces dispositions le 31 décembre 2015 entre le Comité économique des produits de santé et le syndicat Les entreprises du médicament, prorogé par des avenants des 7 décembre 2018 et 18 décembre 2019, envisage l'hypothèse dans laquelle " pour une spécialité répondant à un besoin thérapeutique qui n'est couvert par aucune autre spécialité moins coûteuse, l'entreprise qui l'exploite demande une hausse du prix justifiée par les conditions financières d'exploitation de cette spécialité ", en précisant qu'il est alors tenu compte du coût des obligations liées aux normes environnementales ou à la lutte contre la contrefaçon. Lorsqu'il apprécie le coût d'une telle spécialité, le Comité économique des produits de santé doit prendre en considération les dépenses que le laboratoire doit nécessairement exposer pour la mise sur le marché de la spécialité. <br/>
<br/>
              13. Il ressort des motifs de la décision attaquée que le Comité économique des produits de santé a estimé que les spécialités Alepsal 15 et Alepsal 150 répondaient à un besoin thérapeutique non couvert par une autre spécialité moins coûteuse, mais que leurs conditions d'exploitation ne justifiaient pas une hausse de prix. Pour apprécier à cette fin le prix de revient industriel du médicament, il a retenu le coût " produit fini " et une part de l'amortissement - sur deux ans en droit italien - des frais d'acquisition des droits de propriété industrielle, mais non les coûts de transport, de prestation logistique, d'information et de promotion, non plus que les redevances à l'URSSAF et les frais d'enregistrement, ce dont il a déduit un coût inférieur au prix fabricant hors taxe en vigueur. Il résulte de ce qui a été dit ci-dessus que si le Comité économique des produits de santé était fondé à écarter les coûts d'information et de promotion, ainsi que la part de l'amortissement qui ne s'imposait pas à la société Teofarma mais résultait de son choix de faire application de règles fiscales avantageuses, il a en revanche entaché sa décision d'illégalité en refusant de tenir compte, dans le calcul du prix de revient industriel des spécialités Alepsal 15 et Alepsal 150, des redevances à l'URSSAF, des frais d'enregistrement ainsi que, dans la seule mesure où ils ne résultaient pas de la stratégie choisie par l'industriel, des coûts de transport et de logistique, alors que ces dépenses devaient nécessairement être exposées par la société Teofarma pour la mise sur le marché de ces spécialités. <br/>
<br/>
              14. Il résulte de tout ce qui précède que la société Teofarma est fondée à demander l'annulation de la décision qu'elle attaque en tant qu'elle rejette sa demande de hausse du prix des spécialités Alepsal 15, Alepsal 100 et Alepsal 150. Les moyens retenus suffisant à entraîner cette annulation, il n'est pas nécessaire d'examiner les autres moyens de la requête soulevés à l'appui des mêmes conclusions.<br/>
<br/>
              Sur les conclusions à fin d'injonction et d'astreinte :<br/>
<br/>
              15. L'exécution de la présente décision implique nécessairement que la demande de la société Teofarma tendant à la hausse du prix des spécialités Alepsal 15, Alepsal 100 et Alepsal 150 soit réexaminée. Il y a lieu, par suite, d'enjoindre au Comité économique des produits de santé de procéder à ce réexamen dans un délai de deux mois à compter de la notification de la présente décision. Dans les circonstances de l'espèce, il n'y a pas lieu d'assortir cette injonction de l'astreinte demandée par la société requérante.<br/>
<br/>
              Sur les frais liés au litige :<br/>
<br/>
              16. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 2 000 euros à verser à la société Teofarma au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : La décision du Comité économique des produits de santé du 27 juin 2019 est annulée en tant qu'elle rejette la demande de la société Teofarma tendant à la hausse du prix des spécialités Alepsal 15 milligrammes, Alepsal 100 milligrammes et Alepsal 150 milligrammes.<br/>
Article 2 : Il est enjoint au Comité économique des produits de santé de réexaminer la demande de la société Teofarma tendant à la hausse du prix des spécialités Alepsal 15 milligrammes, Alepsal 100 milligrammes et Alepsal 150 milligrammes dans un délai de deux mois à compter de la notification de la présente décision. <br/>
Article 3 : L'Etat versera à la société Teofarma une somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Le surplus des conclusions de la requête de la société Teofarma est rejeté.<br/>
Article 5 : La présente décision sera notifiée à la société Teofarma et au Comité économique des produits de santé.<br/>
Copie en sera adressée au ministre des solidarités et de la santé et à la section du rapport et des études.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">61-04-01-022 SANTÉ PUBLIQUE. PHARMACIE. PRODUITS PHARMACEUTIQUES. - FIXATION DES PRIX DES MÉDICAMENTS REMBOURSABLES (ART. L. 162-17-4 DU CSS) - ACCORD CADRE PRÉVOYANT POUR UN MÉDICAMENT DIT ORPHELIN UNE HAUSSE DU PRIX JUSTIFIÉE PAR LES CONDITIONS FINANCIÈRES D'EXPLOITATION DU MÉDICAMENT - 1) NOTION - DÉPENSES QUE LE LABORATOIRE DOIT NÉCESSAIREMENT EXPOSER POUR LA MISE SUR LE MARCHÉ DE LA SPÉCIALITÉ [RJ1] - 2) CHAMP - A) EXCLUSION - COÛTS D'INFORMATION ET DE PROMOTION - PART DE L'AMORTISSEMENT RÉSULTANT DU CHOIX DE FAIRE APPLICATION DE RÈGLES FISCALES AVANTAGEUSES - B) INCLUSION - REDEVANCES À L'URSSAF - FRAIS D'ENREGISTREMENT - COÛTS DE TRANSPORT ET DE LOGISTIQUE, DANS LA SEULE MESURE OÙ ILS NE RÉSULTENT PAS DE LA STRATÉGIE CHOISIE PAR L'INDUSTRIEL.
</SCT>
<ANA ID="9A"> 61-04-01-022 Accord-cadre, conclu en application de l'article L. 162-17-4 du code de la sécurité sociale (CSS), envisageant l'hypothèse dans laquelle pour une spécialité répondant à un besoin thérapeutique qui n'est couvert par aucune autre spécialité moins coûteuse, l'entreprise qui l'exploite demande une hausse du prix justifiée par les conditions financières d'exploitation de cette spécialité, en précisant qu'il est alors tenu compte du coût des obligations liées aux normes environnementales ou à la lutte contre la contrefaçon.,,,1) Lorsqu'il apprécie le coût d'une telle spécialité, le Comité économique des produits de santé (CEPS) doit prendre en considération les dépenses que le laboratoire doit nécessairement exposer pour la mise sur le marché de la spécialité.,,,2) a) Si le CEPS était fondé à écarter les coûts d'information et de promotion, ainsi que la part de l'amortissement qui ne s'imposait pas à la société mais résultait de son choix de faire application de règles fiscales avantageuses, b) il a en revanche entaché sa décision d'illégalité en refusant de tenir compte, dans le calcul du prix de revient industriel des spécialités, des redevances à l'URSSAF, des frais d'enregistrement ainsi que, dans la seule mesure où ils ne résultaient pas de la stratégie choisie par l'industriel, des coûts de transport et de logistique, alors que ces dépenses devaient nécessairement être exposées par la société pour la mise sur le marché de ces spécialités.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., sur la prise en compte des coûts tenant aux dépenses de recherche et développement que le laboratoire doit nécessairement exposer pour la mise sur le marché de la spécialité, CE, 20 mars 2013, Société Addmedica, n°s 356661 et autres, T. pp. 777-848.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
