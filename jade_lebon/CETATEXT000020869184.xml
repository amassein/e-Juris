<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000020869184</ID>
<ANCIEN_ID>JG_L_2009_06_000000298641</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/20/86/91/CETATEXT000020869184.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème et 2ème sous-sections réunies, 05/06/2009, 298641</TITRE>
<DATE_DEC>2009-06-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>298641</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème et 2ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Vigouroux</PRESIDENT>
<AVOCATS>SCP CELICE, BLANCPAIN, SOLTNER ; SCP VIER, BARTHELEMY, MATUCHANSKY</AVOCATS>
<RAPPORTEUR>M. Alban de Nervaux</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Dacosta Bertrand</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 8 novembre 2006 et 8 février 2007 au secrétariat du contentieux du Conseil d'Etat, présentés pour la SOCIETE AVENANCE-ENSEIGNEMENT ET SANTE, venant aux droits de la société Générale de restauration, dont le siège est 61-69 rue de Bercy à Paris (75012) ; la SOCIETE AVENANCE-ENSEIGNEMENT ET SANTE demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt du 14 septembre 2006 par lequel la cour administrative d'appel de Versailles a rejeté sa requête tendant à l'annulation du jugement du tribunal administratif de Versailles du 4 octobre 2004 en tant que celui-ci a rejeté ses demandes tendant à la condamnation de la commune de Draveil à réparer les conséquences dommageables de la résiliation de l'affermage du service de restauration scolaire et municipale et au paiement de factures impayées ;<br/>
<br/>
              2°) réglant l'affaire au fond, de condamner la commune de Draveil à lui verser les sommes de 191 767,10 euros au titre de prestations impayées et de 2 213 411 euros en réparation du préjudice subi du fait de la résiliation du contrat d'affermage, ainsi que les intérêts au taux légal et leur capitalisation ;  <br/>
<br/>
              3°) de mettre à la charge de la commune de Draveil la somme de 9 000 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Alban de Nervaux, Auditeur,  <br/>
<br/>
              - les observations de la SCP Célice, Blancpain, Soltner, avocat de la SOCIETE AVENANCE-ENSEIGNEMENT ET SANTE et de la SCP Vier, Barthélemy, Matuchansky, avocat de la commune de Draveil, <br/>
<br/>
              - les conclusions de M. Bertrand Dacosta, rapporteur public ;<br/>
<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Célice, Blancpain, Soltner, avocat de la SOCIETE AVENANCE-ENSEIGNEMENT ET SANTE et à la SCP Vier, Barthélemy, Matuchansky, avocat de la commune de Draveil ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que par un contrat conclu le 10 septembre 1990, la commune de Draveil a confié à la société Générale de restauration, aux droits de laquelle vient la SOCIETE AVENANCE-ENSEIGNEMENT ET SANTE, le service de restauration scolaire et municipale ; que par un avenant conclu le 15 janvier 1993, la commune de Draveil a confié à la société le même service pour une durée de quinze ans et, en outre, la réalisation de travaux de réaménagement des points de distribution ; que par une délibération du 26 juin 2000, la commune de Draveil a prononcé la résiliation du contrat pour un motif d'intérêt général ; que la société a alors saisi le tribunal administratif de Versailles de demandes tendant, notamment, à l'annulation de cette décision de résiliation, à la condamnation de la commune de Draveil à réparer les conséquences dommageables de la résiliation et au paiement des factures impayées ; que la commune de Draveil a, pour sa part, présenté des conclusions indemnitaires reconventionnelles ; que par un jugement du 4 octobre 2004, le tribunal administratif de Versailles a constaté la nullité du contrat conclu entre les parties ; qu'il a, par suite, jugé qu'il n'y avait pas lieu de statuer sur la demande d'annulation de la décision de résiliation prise par la commune et rejeté les demandes de paiement et les demandes indemnitaires formulées sur un fondement contractuel ; que le tribunal a dans le même jugement, ordonné une mesure d'expertise destinée à évaluer les dépenses utiles exposées par la société au profit de la commune pour l'exécution du contrat déclaré nul, le préjudice correspondant aux bénéfices dont elle s'est trouvé privée à raison de cette nullité ainsi que de chiffrer certains éléments dont la commune serait en droit de demander le remboursement sur le fondement de l'enrichissement sans cause ; que par un arrêt du 14 septembre 2006, contre lequel la SOCIETE AVENANCE-ENSEIGNEMENT ET SANTE se pourvoit en cassation, la cour administrative d'appel de Versailles a, d'une part, rejeté les conclusions de la  requête d'appel présentée par cette société à l'encontre du jugement du tribunal administratif de Versailles en tant qu'il s'est fondé sur la nullité du contrat pour rejeter ses demandes indemnitaires, d'autre part, rejeté les conclusions de la SOCIETE AVENANCE-ENSEIGNEMENT ET SANTE tendant notamment à contester la part de responsabilité laissée à sa charge par le tribunal administratif ; <br/>
<br/>
              Sur l'arrêt en tant qu'il a confirmé la nullité du contrat :<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que si la rémunération du co-contractant de la commune de Draveil était assurée principalement par des redevances qui, selon les articles 39-2 et 41 de la convention, étaient perçues directement auprès des usagers des restaurants scolaires et municipaux, des centres de loisirs maternels et des personnes âgées, les parties s'engageaient également sur un prix unitaire des repas, fixé initialement sur la base de 303 000 repas par an, comprenant les charges afférentes aux prestations fournies, dont le coût des investissements, ainsi que la rémunération du prestataire et que, dans les cas où le nombre de repas commandés au cours d'un exercice s'avèrerait inférieur ou supérieur de plus de 5 % par rapport à la base de référence retenue, le prix unitaire des repas fournis sur l'exercice écoulé serait réajusté par la rectification de divers postes composant ce prix, en fonction du nombre de repas effectivement commandés ; que la commune de Draveil, à laquelle, en application de l'article 41 du contrat, la société Générale de restauration facturait directement le prix des repas des usagers du centre aéré, prenait en charge, sous forme de subventions, la différence entre les redevances perçues auprès des usagers et le prix des repas fixé selon les modalités décrites ci-dessus ; que le nombre d'usagers, constitués pour l'essentiel d'enfants des centres aérés et des écoles ainsi que de personnes âgées vivant en maisons de retraite, n'était pas, en l'espèce, susceptible de diminuer de manière substantielle d'une année sur l'autre, ainsi que le confirme l'évolution du nombre de repas servis au cours des années 1993 à 2000 ; que dans ces conditions, la cour, dont l'arrêt est suffisamment motivé, n'a pas commis d'erreur de droit ni d'erreur de qualification juridique en jugeant que la rémunération du co-contractant de la commune, en l'absence de réel risque d'exploitation, ne pouvait être regardée comme étant substantiellement assurée par les résultats de l'exploitation et que le contrat était en conséquence constitutif d'un marché public et non d'une délégation de service public ; qu'elle n'a pas non plus entaché son arrêt d'une contradiction de motifs en jugeant que nonobstant le versement de redevances par les usagers du services, le contrat s'analysait comme un marché public ; que si la cour a relevé qu'en cas de défaut de paiement par les usagers de leurs redevances, l'article 39-2 du contrat permettait à la commune de Draveil de prendre à sa charge ces redevances et d'être subrogée aux droits de la société envers ceux-ci, elle n'a pas ce faisant dénaturé les pièces du dossier ; <br/>
<br/>
              Sur l'arrêt en tant qu'il a rejeté les conclusions de la SOCIETE AVENANCE-ENSEIGNEMENT ET SANTE en tant qu'elles tendaient à contester la part de responsabilité laissée à sa charge par le tribunal administratif :<br/>
<br/>
              Sans qu'il soit besoin d'examiner les autres moyens dirigés contre cette partie de l'arrêt ; <br/>
<br/>
              Considérant qu'il ressort des motifs du jugement du 4 octobre 2004 du tribunal administratif de Versailles que celui-ci avait estimé que la part de responsabilité de la ville au titre du dommage résultant de la nullité du contrat devait être évaluée à 60 % et celle de la société à hauteur de 40 % ; que si ce partage de responsabilité n'a pas été repris par le tribunal administratif dans le dispositif de son arrêt, il n'était en tout état de cause pas étranger au fondement de l'expertise prescrite par le tribunal administratif afin d'évaluer les dépenses utiles exposées par la société au profit de la commune pour l'exécution du contrat déclaré nul, le préjudice correspondant aux bénéfices dont elle s'est trouvé privée à raison de cette nullité ainsi que de chiffrer certains éléments dont la commune serait en droit de demander le remboursement sur le fondement de l'enrichissement sans cause ; qu'ainsi, et alors même qu'il demeurait loisible à la société de contester un tel partage à l'occasion d'un appel formé à l'encontre du jugement rendu après expertise, la cour administrative d'appel, qui a méconnu son office, a commis une erreur de droit  en jugeant que la SOCIETE AVENANCE ENSEIGNEMENT ET SANTE n'était pas recevable à contester le partage de responsabilité effectué par le tribunal administratif dans les motifs de son jugement ; <br/>
<br/>
              Considérant qu'il résulte de ce qui précède que la SOCIETE AVENANCE ENSEIGNEMENT ET SANTE est fondée à demander l'annulation de l'arrêt attaqué en tant seulement qu'il l'a jugée irrecevable à contester la part de responsabilité laissée à sa charge par le tribunal administratif de Versailles ; <br/>
<br/>
              Sur l'application des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              Considérant que dans les circonstances de l'espèce, il n'y a pas lieu faire application de ces dispositions ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 14 septembre 2006 de la cour administrative de Versailles est annulé en tant qu'il a jugé la SOCIETE AVENANCE-ENSEIGNEMENT ET SANTE irrecevable à contester la part de responsabilité laissée à sa charge par le jugement du 4 octobre 2004 du tribunal administratif de Versailles.<br/>
Article 2 : Le surplus des conclusions du pourvoi ainsi que les conclusions présentées par la commune de Draveil au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 3 : L'affaire est renvoyée devant la cour administrative d'appel de Versailles. <br/>
Article 4 : La présente décision sera notifiée à SOCIETE AVENANCE-ENSEIGNEMENT ET SANTE et à la commune de Draveil.<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-01-03-02 MARCHÉS ET CONTRATS ADMINISTRATIFS. NOTION DE CONTRAT ADMINISTRATIF. DIVERSES SORTES DE CONTRATS. MARCHÉS. - EXISTENCE - CONTRAT CONCLU ENTRE UNE COMMUNE ET UNE SOCIÉTÉ POUR ASSURER LE SERVICE DE RESTAURATION SCOLAIRE ET  MUNICIPALE - CONTRAT SANS RISQUE RÉEL D'EXPLOITATION [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">39-01-03-03 MARCHÉS ET CONTRATS ADMINISTRATIFS. NOTION DE CONTRAT ADMINISTRATIF. DIVERSES SORTES DE CONTRATS. DÉLÉGATIONS DE SERVICE PUBLIC. - ABSENCE - CONTRAT CONCLU ENTRE UNE COMMUNE ET UNE SOCIÉTÉ POUR ASSURER LE SERVICE DE RESTAURATION SCOLAIRE ET  MUNICIPALE - CONTRAT SANS RISQUE RÉEL D'EXPLOITATION [RJ1].
</SCT>
<ANA ID="9A"> 39-01-03-02 Un contrat conclu par une commune avec une entreprise spécialisée pour assurer le service de restauration scolaire et municipale est un marché public et non une délégation de service public dès lors que la rémunération du cocontractant de la commune, en l'absence de réel risque d'exploitation et compte tenu des compensations et garanties fournies à l'entreprise par la commune, ne peut être regardée comme étant substantiellement assurée par les résultats de l'exploitation.</ANA>
<ANA ID="9B"> 39-01-03-03 Un contrat conclu par une commune avec une entreprise spécialisée pour assurer le service de restauration scolaire et municipale est un marché public et non une délégation de service public dès lors que la rémunération du cocontractant de la commune, en l'absence de réel risque d'exploitation et compte tenu des compensations et garanties fournies à l'entreprise par la commune, ne peut être regardée comme étant substantiellement assurée par les résultats de l'exploitation.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. sol. contr. 7 novembre 2008, Département de la Vendée, n° 291794, à publier aux Tables du Recueil. Rappr. 15 avril 1996, Préfet des Bouches-du-Rhône, n° 168325, p. 137 ; 30 juin 1999, Syndicat mixte du traitement des ordures ménagères centre ouest, n° 198147, p. 229.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
