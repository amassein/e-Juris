<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026454646</ID>
<ANCIEN_ID>JG_L_2012_10_000000349281</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/45/46/CETATEXT000026454646.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème et 2ème sous-sections réunies, 03/10/2012, 349281</TITRE>
<DATE_DEC>2012-10-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>349281</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème et 2ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DE CHAISEMARTIN, COURJON ; SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>M. Fabrice Aubert</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:349281.20121003</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 16 mai et 3 août 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour la société Valterra, dont le siège est au 52-56 rue Carvès à Montrouge (92120) et pour la société Champagne épandage, dont le siège est au 41 rue de Champagne à Vitry La Ville (51240) ; la société Valterra, venant aux droits de la société Traitement-Valorisation-Décontamination, et la société Champagne épandage demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 10NC00836 du 17 mars 2011 par lequel la cour administrative d'appel de Nancy a, à la demande de la communauté d'agglomération Reims Métropole, annulé le jugement n° 0601461 du 25 mars 2010 par lequel le tribunal administratif de Châlons-en-Champagne a condamné la communauté d'agglomération à verser à la société <br/>
Traitement-Valorisation-Décontamination une somme de 145 000 euros et à la société Champagne épandage une somme de 249 514,42 euros avec intérêts et capitalisation, en réparation des préjudices subis du fait du non respect des stipulations du marché ayant pour objet l'enlèvement et la valorisation agricole des boues produites par la station d'épuration de Saint Brice Courcelles ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de la communauté d'agglomération Reims Métropole ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code des marchés publics ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Fabrice Aubert, Auditeur,  <br/>
<br/>
              - les observations de la SCP de Chaisemartin, Courjon, avocat de la société Champagne épandage et de la société Valterra et de la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la communauté d'agglomération Reims Métropole,<br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP de Chaisemartin, Courjon, avocat de la société Champagne épandage et de la société Valterra et à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la communauté d'agglomération Reims Métropole ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la communauté d'agglomération Reims Métropole a conclu, le 18 novembre 2002, un marché à bons de commande avec la société Champagne épandage et la société Traitement-Valorisation-Décontamination, aux droits de laquelle vient la société Valterra, pour l'enlèvement et la valorisation des boues produites par une station d'épuration ; que par jugement du 25 mars 2010, le tribunal administratif de Châlons-en-Champagne a condamné la communauté d'agglomération à verser, d'une part, une somme de 145 000 euros à la société Champagne épandage en compensation de son manque à gagner du fait de l'absence de commandes à hauteur du montant minimum du marché et, d'autre part, une somme de 249 514,42 euros à la société <br/>
Traitement-Valorisation-Décontamination en indemnisation de son manque à gagner et de la détérioration de son matériel résultant de la mauvaise qualité des boues d'épandage ; que par l'arrêt attaqué du 17 mars 2011, la cour administrative d'appel de Nancy a fait droit à l'appel de la communauté d'agglomération en annulant le jugement de première instance et en rejetant les demandes indemnitaires des sociétés ; <br/>
<br/>
              2. Considérant, en premier lieu, que la cour pouvait, sans entacher son arrêt d'irrégularité, se borner, dans l'analyse des deux mémoires en défense produits devant elle par les sociétés, à relever que ces dernières faisaient valoir que les moyens de la requête d'appel de la communauté d'agglomération n'étaient pas fondés, dès lors que ces mémoires se limitaient à la réfutation des moyens présentés par la collectivité requérante ; <br/>
<br/>
              3. Considérant, en deuxième lieu, qu'aux termes de l'article 34.1 du cahier des clauses administratives générales applicable aux marchés de fournitures courantes et services (CCAG-FCS) dans sa rédaction en vigueur à la conclusion du contrat : " Tout différend entre le titulaire et la personne responsable du marché doit faire l'objet de la part du titulaire d'un mémoire de réclamation qui doit être communiqué à la personne responsable du marché dans le délai de trente jours compté à partir du jour où le différend est apparu. " ; que pour rejeter, après évocation, la demande des sociétés, la cour administrative d'appel de Nancy a jugé que le courrier du 24 juillet 2006 par lequel elles ont demandé à la communauté d'agglomération Reims Métropole le paiement de la somme de 364 245 euros en indemnisation de la marge bénéficiaire manquée du fait du minima de commande non atteint et de la somme de 39 591 euros en raison de la détérioration de leur équipement résultant de la mauvaise qualité des boues, ne pouvait, en l'absence d'indication sur la base de calcul de ces sommes, être regardé comme un mémoire en réclamation au sens de l'article 34.1 du CCAG-FCS précité ; que ce faisant, la cour a écarté les moyens en défense des sociétés tirés de ce que ce cahier des clauses administratives générales n'impose pas la motivation des mémoires en réclamation et de ce qu'en l'espèce, le courrier du 24 juillet 2006 exposait le différend avec suffisamment de précision ; que, par suite, la cour n'a pas entaché son arrêt d'insuffisance de motivation ; <br/>
<br/>
              4. Considérant, en troisième lieu, qu'un mémoire du titulaire du marché ne peut être regardé comme une réclamation au sens de l'article 34.1 du CCAG-FCS que s'il comporte l'énoncé d'un différend et expose, de façon précise et détaillée, les chefs de la contestation en indiquant, d'une part, les montants des sommes dont le paiement est demandé et, d'autre part, les motifs de ces demandes, notamment les bases de calcul des sommes réclamées ; qu'ainsi, la cour administrative d'appel de Nancy n'a pas commis d'erreur de droit en jugeant que les mémoires en réclamations adressés par le titulaire du marché doivent présenter une contestation motivée du décompte incluant notamment la base de calcul de créances dont se prévaut le titulaire ; que par suite, la cour n'a pas entaché son arrêt de contradiction de motifs en estimant, par une appréciation exempte de dénaturation, que le courrier du 24 juillet 2006 n'était pas constitutif d'un mémoire en réclamation, faute de présenter les bases de calcul de la réclamation, tout en relevant que ce courrier comportait l'objet de cette réclamation ainsi que son montant global ; <br/>
<br/>
              5. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mis à la charge de la communauté d'agglomération Reims Métropole le versement de la somme demandée par les sociétés ; qu'en revanche il y a lieu de faire droit aux conclusions de la communauté d'agglomération Reims Métropole sur le fondement de ces dispositions et de mettre à la charge des sociétés Valterra et Champagne épandage le versement de la somme de 3 000 euros à la communauté d'agglomération ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la société Valterra et de la société Champagne épandage est rejeté. <br/>
Article 2 : La société Valterra et la société Champagne épandage verseront solidairement à la communauté d'agglomération Reims Métropole la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la société Valterra, à la société Champagne épandage et à la communauté d'agglomération Reims Métropole.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-06-04-01 PROCÉDURE. JUGEMENTS. RÉDACTION DES JUGEMENTS. VISAS. - VISA DES MÉMOIRES EN DÉFENSE - CAS DES MÉMOIRES SE BORNANT À RÉFUTER LES MOYENS DE LA REQUÊTE - POSSIBILITÉ D'UNE ANALYSE SYNTHÉTIQUE - EXISTENCE.
</SCT>
<ANA ID="9A"> 54-06-04-01 Un mémoire en défense se limitant à la réfutation des moyens présentés par le requérant peut être régulièrement visé et analysé par l'indication synthétique que ce mémoire fait valoir qu'aucun des moyens du requérant n'est fondé.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
