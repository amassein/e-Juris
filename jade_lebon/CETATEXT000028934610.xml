<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028934610</ID>
<ANCIEN_ID>JG_L_2014_05_000000354634</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/93/46/CETATEXT000028934610.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère / 6ème SSR, 14/05/2014, 354634</TITRE>
<DATE_DEC>2014-05-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>354634</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère / 6ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DIDIER, PINET ; SCP GATINEAU, FATTACCINI</AVOCATS>
<RAPPORTEUR>Mme Julia Beurton</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:354634.20140514</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 6 décembre 2011 et 6 mars 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour la fédération nationale CGT des personnels des organismes sociaux, dont le siège est 263, rue de Paris à Montreuil (93515), représentée par son secrétaire général ; la fédération requérante demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté du 15 septembre 2011 du ministre du budget, des comptes publics et de la réforme de l'Etat portant création de l'union pour le recouvrement des cotisations de sécurité sociale et d'allocations familiales de Midi-Pyrénées ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution, notamment les articles 34 et 37 ;<br/>
<br/>
              Vu le code de la sécurité sociale ;<br/>
<br/>
              Vu le code du travail ; <br/>
<br/>
              Vu le décret n° 2010-1149 du 25 novembre 2010 ;<br/>
<br/>
              Vu le décret n° 2010-1451 du 25 novembre 2010 ;<br/>
<br/>
              Vu le décret n° 2012-769 du 24 mai 2012 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Julia Beurton, Auditeur,  <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Didier, Pinet, avocat de la fédération nationale CGT des personnels des organismes sociaux et à la SCP Gatineau, Fattaccini, avocat de l'agence centrale des organismes de sécurité sociale ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que le ministre du budget, des comptes publics et de la réforme de l'Etat a, par arrêté du 15 septembre 2011, créé l'union pour le recouvrement des cotisations de sécurité sociale et d'allocations familiales de Midi-Pyrénées, dont la circonscription correspond à l'ensemble de la région, dissous les unions des huit départements de cette région et transféré leurs biens, droits et obligations à l'union nouvellement créée ; que la fédération nationale CGT des personnels des organismes sociaux demande l'annulation pour excès de pouvoir de cet arrêté ;  <br/>
<br/>
              Sur la demande tendant à ce que les écritures du ministre des affaires sociales et de la santé soient écartées des débats :<br/>
<br/>
              2. Considérant que le directeur de la sécurité sociale, agissant par délégation du ministre de l'économie et des finances, a déclaré faire siennes les écritures présentées par délégation du ministre des affaires sociales et de la santé ; que, par suite, la fédération requérante n'est pas fondée à soutenir que les écritures en défense n'auraient pas été valablement présentées au nom de l'Etat ; <br/>
<br/>
              Sur l'intervention de l'agence centrale des organismes de sécurité sociale :<br/>
<br/>
              3. Considérant que l'agence centrale des organismes de sécurité sociale a intérêt au maintien de l'arrêté attaqué ; qu'ainsi, son intervention est recevable ; <br/>
<br/>
              Sur la légalité de l'arrêté attaqué :<br/>
<br/>
              4. Considérant qu'aux termes de l'article D. 213-1 du code de la sécurité sociale, dans sa rédaction issue du décret du 8 septembre 2011 portant création de conseils départementaux au sein des unions de recouvrement des cotisations de sécurité sociale et d'allocations familiales dont la circonscription est régionale : " La circonscription territoriale d'une union pour le recouvrement des cotisations de sécurité sociale et d'allocations familiales est départementale ou régionale. Elle est fixée, ainsi que le siège de l'union, par arrêté du ministre chargé de la sécurité sociale " ;<br/>
<br/>
              En ce qui concerne la compétence du pouvoir réglementaire :<br/>
<br/>
              5. Considérant qu'aux termes de l'article 34 de la Constitution : " (...) La loi détermine les principes fondamentaux (...) de la sécurité sociale (...) " ; que si l'administration des organismes de sécurité sociale, dotés de la personnalité morale, par des représentants des employeurs et des salariés constitue l'un des principes fondamentaux de la sécurité sociale, ce principe doit être apprécié dans le cadre des limitations de portée générale qui y ont été introduites pour permettre certaines interventions jugées nécessaires de la puissance publique, en raison du caractère des activités assumées par ces organismes ; qu'en particulier, il appartient au pouvoir réglementaire de déterminer l'organisation administrative de ce service public et de délimiter les circonscriptions territoriales des organismes de sécurité sociale ; que, par suite, le pouvoir réglementaire était compétent pour prévoir la possibilité de déterminer, par arrêté ministériel, la circonscription et le siège des unions pour le recouvrement des cotisations de sécurité sociale et d'allocations familiales et de procéder, le cas échéant, à la fusion de deux ou plusieurs unions ; que, dès lors, la fédération requérante n'est pas fondée à soutenir que l'article D. 213-1 du code de la sécurité sociale, de même que l'arrêté attaqué, portant création de l'union pour le recouvrement des cotisations de sécurité sociale et d'allocations familiales de Midi-Pyrénées par fusion des huit unions existant dans les départements de la région, seraient entachés d'incompétence ; <br/>
<br/>
              En ce qui concerne la compétence du ministre du budget :<br/>
<br/>
              6. Considérant, en premier lieu, que la fédération requérante ne peut utilement se prévaloir des dispositions de l'article 2 du décret du 12 mai 1960 relatif à l'organisation et au fonctionnement de la sécurité sociale, qui ont été abrogées par le décret du 17 décembre 1985 relatif au code de la sécurité sociale (partie Législative et partie Décrets en Conseil d'Etat) ; que, contrairement à ce qu'elle soutient, l'article R. 112-1 du code de la sécurité sociale charge de l'application de l'ensemble des dispositions législatives et réglementaires relatives à la sécurité sociale non plus le ministre du travail mais celui chargé de la sécurité sociale ; qu'en tout état de cause, l'article D. 213-1 du code de la sécurité sociale donne compétence au ministre chargé de la sécurité sociale pour fixer la circonscription et le siège des unions pour le recouvrement des cotisations de sécurité sociale et d'allocations familiales ;  <br/>
<br/>
              7. Considérant qu'en vertu des attributions qui lui étaient confiées par le 3° de l'article 1er du décret n° 2010-1449 du 25 novembre 2010, le ministre du travail, de l'emploi et de la santé était compétent pour préparer et mettre en oeuvre " les règles relatives aux régimes et à la gestion des organismes de sécurité sociale ainsi qu'aux régimes complémentaires en matière d'accidents du travail et de maladies professionnelles, d'assurance vieillesse et d'assurance maladie et maternité " ; que le ministre du budget, des comptes publics, de la fonction publique et de la réforme de l'Etat était, pour sa part, en vertu du décret n° 2010-1451 du 25 novembre 2010 relatif à ses attributions, " responsable de l'équilibre général des comptes sociaux et des mesures de financement de la protection sociale " ; qu'il résulte de ces dispositions que le ministre du budget, des comptes publics, de la fonction publique et de la réforme de l'Etat devait être regardé comme seul " chargé de la sécurité sociale " au sens de l'article D. 213-1 du code de la sécurité sociale ; que, par suite, la fédération requérante n'est pas fondée à soutenir que l'arrêté attaqué serait entaché d'incompétence faute d'avoir été également signé par le ministre du travail, de l'emploi et de la santé ; <br/>
<br/>
              En ce qui concerne la régularité de la procédure :<br/>
<br/>
              8. Considérant, en premier lieu, qu'en vertu des dispositions de l'article R. 200-1 du code de la sécurité sociale, il revenait au " ministre chargé de la sécurité sociale " de saisir pour avis l'agence centrale des organismes de sécurité sociale ; qu'il résulte de ce qui a été dit ci-dessus que le moyen tiré de ce que l'agence centrale des organismes de sécurité sociale aurait été irrégulièrement consultée, faute d'avoir été saisie par le ministre du travail, de l'emploi et de la santé, doit, en tout état de cause, être écarté ; <br/>
<br/>
              9. Considérant, en deuxième lieu, qu'aux termes de l'article L. 2323-19 du code du travail : " Le comité d'entreprise est informé et consulté sur les modifications de l'organisation économique ou juridique de l'entreprise, notamment en cas de fusion (...) / L'employeur indique les motifs des modifications projetées et consulte le comité d'entreprise sur les mesures envisagées à l'égard des salariés lorsque ces modifications comportent des conséquences pour ceux-ci (...) " ; qu'en application de ces dispositions, les unions pour le recouvrement des cotisations de sécurité sociale et d'allocations familiales de la région Midi-Pyrénées devaient être mises en mesure de saisir leurs comités d'entreprise respectifs, afin de les consulter préalablement à l'édiction de l'arrêté attaqué sur les modalités de la fusion envisagée, en leur laissant un délai suffisant pour se prononcer ; <br/>
<br/>
              10. Considérant qu'il ressort des pièces du dossier que la délégation unique du personnel de l'union pour le recouvrement des cotisations de sécurité sociale et d'allocations familiales du Lot et les comités d'entreprise des sept autres unions concernées par l'arrêté attaqué ont été saisis préalablement à son édiction ; que la circonstance que les organisations syndicales aient, après discussion sur les principales questions soulevées par le projet de régionalisation envisagé, refusé d'exprimer un avis n'est pas de nature à entacher d'irrégularité la procédure de consultation ainsi engagée ; que la fédération requérante n'est, par suite, pas fondée à soutenir que l'arrêté attaqué serait entaché d'illégalité faute de consultation des comités d'entreprise des unions de recouvrement des cotisations de sécurité sociale et d'allocations familiales de la région Midi-Pyrénées ;<br/>
<br/>
              11. Considérant, en troisième lieu, qu'aux termes de l'article L. 4612-8 du code du travail : " Le comité d'hygiène, de sécurité et des conditions de travail est consulté avant toute décision d'aménagement important modifiant les conditions de santé et de sécurité ou les conditions de travail et, notamment, avant toute transformation importante des postes de travail découlant (...) d'un changement (...) de l'organisation du travail (...) " ; qu'il ressort des pièces du dossier que chaque directeur des unions de recouvrement des cotisations de sécurité sociale et d'allocations familiales de la région Midi-Pyrénées a été mis à même de procéder à la consultation du comité d'hygiène, de sécurité et des conditions de travail de l'union sur la fusion envisagée ; que, par suite, le moyen tiré de ce que l'arrêté attaqué serait irrégulier en l'absence de consultation de ces comités doit, en tout état de cause, être écarté ; <br/>
<br/>
              12. Considérant, en dernier lieu, que la circonstance que les conseils d'administration des unions pour le recouvrement des cotisations de sécurité sociale et d'allocations familiales des départements de la région Midi-Pyrénées aient rendu un avis sur le projet d'arrêté avant que les comités d'entreprise et les comités d'hygiène, de sécurité et des conditions de travail compétents se soient prononcés est sans incidence sur la légalité de l'arrêté attaqué ;<br/>
<br/>
              13. Considérant qu'il résulte de tout ce qui précède, sans qu'il soit besoin de se prononcer sur la recevabilité de sa requête, que la fédération nationale CGT des personnels des organismes sociaux n'est pas fondée à demander l'annulation de l'arrêté qu'elle attaque ;<br/>
<br/>
              Sur l'application des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              14. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante ; que l'agence centrale des organismes de sécurité sociale, intervenante en défense, n'étant pas partie à l'instance, ces dispositions font également obstacle à ce qu'il soit fait droit aux conclusions que celle-ci présente au même titre ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'intervention de l'agence centrale des organismes de sécurité sociale est admise. <br/>
Article 2 : La requête de la fédération nationale CGT des personnels des organismes sociaux est rejetée.<br/>
Article 3 : Les conclusions de l'agence centrale des organismes de sécurité sociale présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 4 : La présente décision sera notifiée à la fédération nationale CGT des personnels des organismes sociaux, à l'agence centrale des organismes de sécurité sociale et au ministre des finances et des comptes publics. <br/>
Copie en sera adressée pour information à la ministre des affaires sociales.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-02-01-03-17 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - COMPÉTENCE. LOI ET RÈGLEMENT. ARTICLES 34 ET 37 DE LA CONSTITUTION - MESURES RELEVANT DU DOMAINE DU RÈGLEMENT. MESURES NE PORTANT PAS ATTEINTE AUX PRINCIPES FONDAMENTAUX DE LA SÉCURITÉ SOCIALE. - ORGANISATION ADMINISTRATIVE DE LA SÉCURITÉ SOCIALE ET DÉLIMITATION DES CIRCONSCRIPTIONS TERRITORIALES DES ORGANISMES DE SÉCURITÉ SOCIALE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">62-01 SÉCURITÉ SOCIALE. ORGANISATION DE LA SÉCURITÉ SOCIALE. - ORGANISATION ADMINISTRATIVE DE LA SÉCURITÉ SOCIALE ET DÉLIMITATION DES CIRCONSCRIPTIONS TERRITORIALES DES ORGANISMES DE SÉCURITÉ SOCIALE - COMPÉTENCE DU POUVOIR RÉGLEMENTAIRE (ART. 34 DE LA CONSTITUTION) - EXISTENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">62-01-01-01-04-01 SÉCURITÉ SOCIALE. ORGANISATION DE LA SÉCURITÉ SOCIALE. RÉGIME DE SALARIÉS. RÉGIME GÉNÉRAL. RECOUVREMENT. URSSAF. - COMPÉTENCE DU POUVOIR RÉGLEMENTAIRE POUR PRÉVOIR LA POSSIBILITÉ DE DÉTERMINER, PAR ARRÊTÉ MINISTÉRIEL, LA CIRCONSCRIPTION ET LE SIÈGE DES URSSAF ET PROCÉDER, LE CAS ÉCHÉANT, À LA FUSION DE DEUX OU PLUSIEURS UNIONS - EXISTENCE [RJ1].
</SCT>
<ANA ID="9A"> 01-02-01-03-17 Si l'administration des organismes de sécurité sociale, dotés de la personnalité morale, par des représentants des employeurs et des salariés, constitue l'un des principes fondamentaux de la sécurité sociale, ce principe doit être apprécié dans le cadre des limitations de portée générale qui y ont été introduites pour permettre certaines interventions jugées nécessaires de la puissance publique, en raison du caractère des activités assumées par ces organismes. En particulier, il appartient au pouvoir réglementaire de déterminer l'organisation administrative de ce service public et de délimiter les circonscriptions territoriales des organismes de sécurité sociale. Par suite, le pouvoir réglementaire est compétent pour prévoir la possibilité de déterminer, par arrêté ministériel, la circonscription et le siège des unions pour le recouvrement des cotisations de sécurité sociale et d'allocations familiales et de procéder, le cas échéant, à la fusion de deux ou plusieurs unions.</ANA>
<ANA ID="9B"> 62-01 Si l'administration des organismes de sécurité sociale, dotés de la personnalité morale, par des représentants des employeurs et des salariés, constitue l'un des principes fondamentaux de la sécurité sociale, ce principe doit être apprécié dans le cadre des limitations de portée générale qui y ont été introduites pour permettre certaines interventions jugées nécessaires de la puissance publique, en raison du caractère des activités assumées par ces organismes. En particulier, il appartient au pouvoir réglementaire de déterminer l'organisation administrative de ce service public et de délimiter les circonscriptions territoriales des organismes de sécurité sociale. Par suite, le pouvoir réglementaire est compétent pour prévoir la possibilité de déterminer, par arrêté ministériel, la circonscription et le siège des unions pour le recouvrement des cotisations de sécurité sociale et d'allocations familiales et de procéder, le cas échéant, à la fusion de deux ou plusieurs unions.</ANA>
<ANA ID="9C"> 62-01-01-01-04-01 Si l'administration des organismes de sécurité sociale, dotés de la personnalité morale, par des représentants des employeurs et des salariés, constitue l'un des principes fondamentaux de la sécurité sociale, ce principe doit être apprécié dans le cadre des limitations de portée générale qui y ont été introduites pour permettre certaines interventions jugées nécessaires de la puissance publique, en raison du caractère des activités assumées par ces organismes. En particulier, il appartient au pouvoir réglementaire de déterminer l'organisation administrative de ce service public et de délimiter les circonscriptions territoriales des organismes de sécurité sociale. Par suite, le pouvoir réglementaire est compétent pour prévoir la possibilité de déterminer, par arrêté ministériel, la circonscription et le siège des unions pour le recouvrement des cotisations de sécurité sociale et d'allocations familiales (URSSAF) et procéder, le cas échéant, à la fusion de deux ou plusieurs unions.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., pour les caisses d'allocations familiales, décision du même jour, CE, 14 mai 2014, Commune de Vienne, n° 355988, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
