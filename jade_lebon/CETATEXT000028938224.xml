<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028938224</ID>
<ANCIEN_ID>JG_L_2007_02_000000276863</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/93/82/CETATEXT000028938224.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère / 6ème SSR, 26/02/2007, 276863</TITRE>
<DATE_DEC>2007-02-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>276863</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère / 6ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>FOUSSARD ; RICARD</AVOCATS>
<RAPPORTEUR>M. Marc  Bénassy</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Devys</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2007:276863.20070226</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 24 janvier 2005 au secrétariat du contentieux du Conseil d'Etat, présentée pour l'AGENCE NATIONALE POUR L'EMPLOI (ANPE), dont le siège est 4, rue Galilée, Le Galilée à Noisy-le-Grand cedex (93198) ; l'AGENCE NATIONALE POUR L'EMPLOI demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance du 29 novembre 2004 par laquelle le président de la cour administrative d'appel de Nancy a rejeté sa requête tendant à l'annulation du jugement du 13 juillet 2004 par lequel le tribunal administratif de Châlons-en-Champagne a annulé la décision du 27 mai 2003 du directeur régional de l'Agence nationale pour l'emploi de Champagne-Ardenne ayant mis fin au contrat de Mme B...A...à compter du 11 mai 2003 et a ordonné la réintégration de celle-ci à compter de cette date ;<br/>
<br/>
              2°) réglant l'affaire au fond, d'annuler le jugement du tribunal administratif de Châlons-en-Champagne et de rejeter la demande présentée par Mme A... devant ce tribunal ;<br/>
<br/>
              3°) de mettre à la charge de Mme A... le versement de la somme de 2 500 euros en application des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code du travail ;<br/>
<br/>
              Vu la loi n° 83-634 du 13 juillet 1983 ;<br/>
<br/>
              Vu la loi n° 84-16 du 11 janvier 1984 ;<br/>
<br/>
              Vu le décret n° 86-83 du 17 janvier 1986 ;<br/>
<br/>
              Vu le décret n° 90-543 du 29 juin 1990 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Marc Bénassy, chargé des fonctions de Maître des requêtes, <br/>
<br/>
              - les observations de Me Foussard, avocat de l'AGENCE NATIONALE POUR L'EMPLOI (ANPE) et de Me Ricard, avocat de Mme A..., <br/>
<br/>
              - les conclusions de M. Christophe Devys, Commissaire du gouvernement ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Sans qu'il soit besoin d'examiner les autres moyens de la requête :<br/>
<br/>
              Considérant qu'aux termes de l'article R. 811-7 du code de justice administrative, sont dispensées de ministère d'avocat : " Les requêtes dirigées contre les décisions des tribunaux administratifs statuant sur des recours pour excès de pouvoir formés par les fonctionnaires ou agents de l'Etat et des autres personnes ou collectivités publiques (...) contre les actes relatifs à leur situation personnelle " ;<br/>
<br/>
              Considérant que le jugement du tribunal administratif de Châlons-en-Champagne en date du 13 juillet 2004 dont il a été fait appel, annulant à la demande de l'intéressée la décision du 27 mai 2003 par laquelle le directeur régional de l'Agence nationale pour l'emploi (ANPE) de Champagne-Ardenne a mis fin au contrat de Mme A... à compter du 11 mai 2003, statue sur un recours pour excès de pouvoir, alors même que l'annulation qu'il prononce est assortie d'une injonction faite à l'agence de réintégrer Mme A... dans ses fonctions à compter de la même date ; que, par suite, en estimant que l'appel formé par l'ANPE n'était pas au nombre des requêtes dispensées de ministère d'avocat en vertu de l'article R. 811-7 du code de justice administrative et que, par voie de conséquence, cet appel présenté sans le ministère d'avocat était irrecevable, le président de la cour administrative d'appel de Nancy a commis une erreur de droit ; que son ordonnance en date du 29 novembre 2004 doit, dès lors, être annulée ;<br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en vertu de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              Considérant que le comité médical du département de la Haute-Marne a constaté le 9 janvier 2003 l'inaptitude définitive et absolue de Mme A...à reprendre ses fonctions de conseiller à l'emploi à l'ANPE, à l'issue du congé de longue maladie de trois ans dont elle avait bénéficié ; que le 23 mai 2003, le directeur régional de l'ANPE de Champagne-Ardenne a licencié MmeA... ;<br/>
<br/>
              Considérant qu'aux termes de l'article 63 de la loi du 11 janvier 1984 : " Lorsque les fonctionnaires sont reconnus, par suite d'altération de leur état physique, inaptes à l'exercice de leurs fonctions, le poste de travail auquel ils sont affectés est adapté à leur état physique. Lorsque l'adaptation du poste de travail n'est pas possible, ces fonctionnaires peuvent être reclassés dans des emplois d'un autre corps s'ils ont été déclarés en mesure de remplir les fonctions correspondantes. (...) Un décret en Conseil d'Etat détermine les conditions dans lesquelles le reclassement, qui est subordonné à la présentation d'une demande de l'intéressé, peut intervenir " ; qu'aux termes de l'article 56 du décret du 29 juin 1990 fixant le statut applicable aux agents de l'Agence nationale pour l'emploi : " Le reclassement des agents lié à une inaptitude à l'exercice de leurs fonctions s'opère après consultation de la commission administrative paritaire compétente siégeant en la matière et avis du médecin de prévention selon des modalités définies par décision du directeur général " ; que la décision du directeur général de l'ANPE du 19 décembre 1992 énonce que " le reclassement est subordonné à la présentation d'une demande par l'intéressé (...). " ;<br/>
<br/>
              Considérant qu'il résulte du principe général du droit dont s'inspirent tant les dispositions du code du travail relatives à la situation des salariés qui, pour des raisons médicales, ne peuvent plus occuper leur emploi que les règles statutaires applicables dans ce cas aux fonctionnaires, que, lorsqu'il a été médicalement constaté qu'un salarié se trouve de manière définitive atteint d'une inaptitude physique à occuper son emploi, il appartient à l'employeur de le reclasser dans un autre emploi et, en cas d'impossibilité, de prononcer, dans les conditions prévues pour l'intéressé, son licenciement ; que ce principe est applicable en particulier aux agents contractuels de droit public, catégorie à laquelle appartient Mme A... ; que les dispositions législatives précitées, en subordonnant le reclassement à la présentation d'une demande par l'intéressé, ont pour objet d'interdire à l'employeur d'imposer un tel reclassement, mais ne le dispensent pas d'inviter l'intéressé à formuler une telle demande ; <br/>
<br/>
              Considérant qu'il ressort du dossier que Mme A...n'a pas été invitée à présenter une demande de reclassement avant que ne soit prise la décision de licenciement litigieuse ; que cette décision n'a donc pas été régulièrement prise ; que par suite l'ANPE n'est pas fondée à se plaindre que, par le jugement attaqué, le tribunal administratif de Châlons-en-Champagne a annulé la décision du 27 mai 2003 de son directeur régional de Champagne-Ardenne licenciant Mme A...et lui a enjoint de réintégrer celle-ci dans un délai de deux mois ;<br/>
<br/>
              Considérant, enfin, que dans les circonstances de l'espèce, il y a lieu de mettre à la charge de l'AGENCE NATIONALE POUR L'EMPLOI la somme que demande Mme A... au titre des dispositions de l'article L. 761-1 du code de justice administrative ; que ces mêmes dispositions font obstacle à ce que soit mis à la charge de Mme A..., qui n'est pas la partie perdante dans la présente instance, le versement de la somme que l'AGENCE NATIONALE POUR L'EMPLOI demande au même titre ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du président de la cour administrative d'appel de Nancy en date du 29 novembre 2004 est annulée.<br/>
Article 2 : Les conclusions présentées par l'AGENCE NATIONALE POUR L'EMPLOI devant la cour administrative d'appel de Nancy et le surplus des conclusions présentées par cette agence devant le Conseil d'Etat sont rejetés.<br/>
Article 3 : L'AGENCE NATIONALE POUR L'EMPLOI versera à Mme A...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à l'AGENCE NATIONALE POUR L'EMPLOI, à Mme B...A...et au ministre de l'emploi, de la cohésion sociale et du logement.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-04-03-08 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. PRINCIPES GÉNÉRAUX DU DROIT. RECONNAISSANCE DE DROITS SOCIAUX FONDAMENTAUX. - OBLIGATION POUR L'EMPLOYEUR DE RECLASSER UN SALARIÉ ATTEINT DE MANIÈRE DÉFINITIVE D'UNE INAPTITUDE À EXERCER SON EMPLOI ET, EN CAS D'IMPOSSIBILITÉ, DE PRONONCER SON LICENCIEMENT [RJ1] - CHAMP D'APPLICATION - INCLUSION - AGENTS CONTRACTUELS DE DROIT PUBLIC.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">36-04-01 FONCTIONNAIRES ET AGENTS PUBLICS. CHANGEMENT DE CADRES, RECLASSEMENTS, INTÉGRATIONS. QUESTIONS D'ORDRE GÉNÉRAL. - OBLIGATION POUR L'EMPLOYEUR DE RECLASSER UN SALARIÉ ATTEINT DE MANIÈRE DÉFINITIVE D'UNE INAPTITUDE À EXERCER SON EMPLOI ET, EN CAS D'IMPOSSIBILITÉ, DE PRONONCER SON LICENCIEMENT [RJ1] - A) OBLIGATION RÉSULTANT D'UN PRINCIPE GÉNÉRAL DU DROIT - CHAMP D'APPLICATION - INCLUSION - AGENTS CONTRACTUELS DE DROIT PUBLIC - B) PORTÉE - OBLIGATION POUR L'EMPLOYEUR D'INVITER L'INTÉRESSÉ À FORMULER UNE DEMANDE DE RECLASSEMENT (ART. 63 DE LA LOI DU 11 JANVIER 1984).
</SCT>
<ANA ID="9A"> 01-04-03-08 Il résulte du principe général du droit dont s'inspirent tant les dispositions du code du travail relatives à la situation des salariés qui, pour des raisons médicales, ne peuvent plus occuper leur emploi que les règles statutaires applicables dans ce cas aux fonctionnaires que, lorsqu'il a été médicalement constaté qu'un salarié se trouve de manière définitive atteint d'une inaptitude physique à occuper son emploi, il appartient à l'employeur de le reclasser dans un autre emploi et, en cas d'impossibilité, de prononcer, dans les conditions prévues pour l'intéressé, son licenciement. Ce principe est applicable en particulier aux agents contractuels de droit public.</ANA>
<ANA ID="9B"> 36-04-01 a) Il résulte du principe général du droit dont s'inspirent tant les dispositions du code du travail relatives à la situation des salariés qui, pour des raisons médicales, ne peuvent plus occuper leur emploi que les règles statutaires applicables dans ce cas aux fonctionnaires que, lorsqu'il a été médicalement constaté qu'un salarié se trouve de manière définitive atteint d'une inaptitude physique à occuper son emploi, il appartient à l'employeur de le reclasser dans un autre emploi et, en cas d'impossibilité, de prononcer, dans les conditions prévues pour l'intéressé, son licenciement. Ce principe est applicable en particulier aux agents contractuels de droit public.... ...b) Les dispositions de l'article 63 de la loi du 11 janvier 1984, en subordonnant le reclassement à la présentation d'une demande par l'intéressé, ont pour objet d'interdire à l'employeur d'imposer un tel reclassement, mais ne le dispensent pas d'inviter l'intéressé à formuler une telle demande.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. 2 octobre 2002, Chambre de commerce et d'industrie de Meurthe-et-Moselle, n° 227868, p. 319.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
