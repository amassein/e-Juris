<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032772233</ID>
<ANCIEN_ID>JG_L_2016_06_000000395474</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/77/22/CETATEXT000032772233.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 6ème chambres réunies, 22/06/2016, 395474</TITRE>
<DATE_DEC>2016-06-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>395474</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 6ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Frédéric Puigserver</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Jean Lessi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:395474.20160622</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Par une protestation et un nouveau mémoire, enregistrés les 22 décembre 2015 et 27 janvier 2016 au secrétariat du contentieux du Conseil d'Etat, M. H...J..., M. E... A...et Mme I...B...demandent au Conseil d'Etat d'annuler les opérations électorales qui se sont déroulées le 13 décembre 2015 dans la commune de Chanoy (Haute-Marne) en vue de la désignation des membres du conseil régional de la région Alsace, Champagne-Ardenne et Lorraine.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code électoral ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Puigserver, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Jean Lessi, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 338 du code électoral : " Les conseillers régionaux sont élus dans chaque région au scrutin de liste à deux tours sans adjonction ni suppression de noms et sans modification de l'ordre de présentation. Chaque liste est constituée d'autant de sections qu'il y a de départements dans la région. / (...) / Si aucune liste n'a recueilli la majorité absolue des suffrages exprimés au premier tour, il est procédé à un second tour. Il est attribué à la liste qui a obtenu le plus de voix un nombre de sièges égal au quart du nombre des sièges à pourvoir, arrondi à l'entier supérieur. (...) Cette attribution opérée, les autres sièges sont répartis entre toutes les listes à la représentation proportionnelle suivant la règle de la plus forte moyenne, sous réserve de l'application du quatrième alinéa ci-après. / Les listes qui n'ont pas obtenu au moins 5 % des suffrages exprimés ne sont pas admises à la répartition des sièges (...) ". L'article L. 338-1 du même code dispose que : " Les sièges attribués à chaque liste en application de l'article L. 338 sont répartis entre les sections départementales qui la composent au prorata des voix obtenues par la liste dans chaque département. Cette attribution opérée, les sièges restant à attribuer sont répartis entre les sections départementales selon la règle de la plus forte moyenne. (...) / Les sièges sont attribués aux candidats dans l'ordre de présentation sur chaque section départementale. (...) ".<br/>
<br/>
              2. A l'issue du second tour des élections régionales dans la région Alsace, Champagne-Ardenne et Lorraine, le 13 décembre 2015, les listes conduites par MM. L...F..., C...K...et G...D...ont obtenu, respectivement, 339 756, 790 166 et 1 060 059 voix et se sont vu attribuer, en application des dispositions précitées, respectivement dix-neuf sièges, quarante-six sièges et cent quatre sièges. <br/>
<br/>
              3. D'une part, aux termes de l'article R. 52 du code électoral : " Le bureau se prononce provisoirement sur les difficultés qui s'élèvent touchant les opérations électorales. / Ses décisions sont motivées. Toutes les réclamations et décisions sont inscrites au procès-verbal, les pièces qui s'y rapportent y sont annexées après avoir été paraphées par les membres du bureau. / Pendant toute la durée des opérations de vote, le procès-verbal est tenu à la disposition des membres du bureau, candidats, remplaçants et délégués des candidats, électeurs du bureau et personnes chargées du contrôle des opérations, qui peuvent y porter leurs observations ou réclamations ". Aux termes de l'article R. 66 du même code : " Une fois les opérations de lecture et de pointage terminées, les scrutateurs remettent au bureau les feuilles de pointage signées par eux, en même temps que les bulletins, enveloppes électorales et enveloppes de centaine dont la régularité leur a paru douteuse, ou a été contestée par des électeurs ou par les délégués des candidats ". Enfin, aux termes de l'article R. 68 de ce code : " Les pièces fournies à l'appui des réclamations et des décisions prises par le bureau, ainsi que les feuilles de pointage sont jointes au procès-verbal. / Les bulletins autres que ceux qui, en application de la législation en vigueur, doivent être annexés au procès-verbal sont détruits en présence des électeurs ". Il résulte de ces dispositions que dans l'hypothèse où est portée au procès-verbal une réclamation portant sur la validité de certains bulletins de vote, désignés avec une précision suffisante, qui ont été pris en considération dans le décompte des voix, il appartient au bureau de vote de statuer sur cette difficulté, en faisant mention de sa décision, motivée, au procès-verbal et en y annexant les bulletins contestés, en vue de permettre, le cas échéant, au juge de l'élection d'exercer son contrôle. En revanche, lorsqu'une réclamation porte, non sur certains bulletins précisément identifiables, mais sur la régularité de l'ensemble des opérations de dépouillement et qu'un nouveau décompte est demandé, il appartient au bureau de vote, après avoir apprécié le sérieux de la contestation, de procéder à un nouveau décompte ou de mentionner au procès-verbal les motifs de son refus, puis de détruire en présence des électeurs l'ensemble des bulletins autres que ceux qui ont été exclus du résultat du dépouillement.<br/>
<br/>
              4. D'autre part, aux termes de l'article R. 66-2 du code électoral : " Sont nuls et n'entrent pas en compte dans le résultat du dépouillement : (...) 5° Les bulletins imprimés d'un modèle différent de ceux qui ont été produits par les candidats ou qui comportent une mention manuscrite ; / 6° Les circulaires utilisées comme bulletin (...) ".<br/>
<br/>
              5. Il résulte de l'instruction qu'à l'issue des opérations électorales qui se sont déroulées dans l'unique bureau de vote de la commune de Chanoy, ont été portées au procès-verbal les mentions selon lesquelles, d'une part, le verso des bulletins de vote n'avait pas été vérifié lors du dépouillement et, d'autre part, deux documents de propagande avaient été comptabilisés comme bulletins de vote. Toutefois, aucun bulletin de vote n'a été annexé au procès-verbal ni conservé. <br/>
<br/>
              6. Dans leur protestation, M.J..., M. A...et Mme B...soutiennent qu'ont été comptabilisés à tort deux bulletins comportant des noms rayés au verso et deux circulaires utilisées comme bulletins de vote. D'une part,  faute d'annexion au procès-verbal des deux documents de propagande dont la prise en compte dans les résultats du scrutin avait été contestée et qui étaient identifiés avec une précision suffisante dans les observations portées au procès-verbal, le juge n'est pas en mesure de vérifier l'appréciation portée sur la validité de ces deux suffrages, alors que des suffrages exprimés au moyen de circulaires auraient dû être tenus pour nuls en application de l'article R. 66-2 du code électoral. D'autre part, en l'absence de réclamation au procès-verbal portant de façon précise sur des bulletins qui auraient comporté des mentions manuscrites, c'est à bon droit que tous les autres bulletins ont été détruits en application de l'article R. 68 du code électoral et leur validité ne peut plus être utilement contestée devant le juge de l'élection.<br/>
<br/>
              7. Il y a lieu, dès lors, pour apprécier l'influence de l'irrégularité invoquée sur le résultat de l'élection, selon le mode de scrutin prévu par les articles L. 338 et L. 338-1 du code électoral, de soustraire successivement deux suffrages du total de ceux qui ont obtenus par chacune des trois listes de candidats en présence au second tour de scrutin, en diminuant de deux unités le total des suffrages exprimés. Ce retranchement hypothétique ne remet en cause ni l'attribution, en application de l'article L. 338 du code électoral, des quarante-trois premiers sièges et de soixante sièges supplémentaires à liste conduite par M.D..., ni celle de quarante-cinq et dix-neuf sièges aux listes conduites respectivement par M. K...et par M.F..., ni celle des avant-dernier et dernier sièges aux listes conduites respectivement par M. D...et par M. K.... Ainsi, et sans qu'il y ait lieu de rechercher, par des calculs hypothétiques, si la répartition entre sections départementales des sièges obtenus par chaque liste, en application de l'article L. 338-1, s'en trouve affectée, l'élection des membres du conseil régional de la région Alsace, Champagne-Ardenne et Lorraine n'est pas remise en cause.<br/>
<br/>
              8. Il résulte de tout ce qui précède que M.J..., M. A...et Mme B...ne sont pas fondés à demander l'annulation des opérations électorales qu'ils contestent.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La protestation de M.J..., de M. A...et de Mme B...est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. H...J..., à M. L...F...et au ministre de l'intérieur.<br/>
Copie en sera adressée à la commission nationale des comptes de campagne et des financements politiques.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">28-005-03 ÉLECTIONS ET RÉFÉRENDUM. DISPOSITIONS GÉNÉRALES APPLICABLES AUX ÉLECTIONS. OPÉRATIONS ÉLECTORALES. - DÉPOUILLEMENT - RÉCLAMATIONS PORTÉES AU PROCÈS-VERBAL - OFFICE DU BUREAU DE VOTE - 1) CAS D'UNE RÉCLAMATION PORTANT SUR LA VALIDITÉ DE CERTAINS BULLETINS DE VOTE - 2) CAS D'UNE RÉCLAMATION PORTANT SUR LA RÉGULARITÉ DE L'ENSEMBLE DES OPÉRATIONS DE DÉPOUILLEMENT ET DE DEMANDE D'UN NOUVEAU DÉCOMPTE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">28-08-05-03-02 ÉLECTIONS ET RÉFÉRENDUM. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. POUVOIRS DU JUGE. CONSÉQUENCES TIRÉES PAR LE JUGE DES IRRÉGULARITÉS. RECTIFICATION DES RÉSULTATS ÉLECTORAUX. - ELECTIONS RÉGIONALES - APPRÉCIATION DE L'INFLUENCE DE LA PRISE EN COMPTE IRRÉGULIÈRE DE SUFFRAGES INVALIDES - APPRÉCIATION SUR LA RÉPARTITION DES SIÈGES ENTRE LISTES, SANS PRISE EN COMPTE DE LA RÉPARTITION ENTRE SECTIONS DÉPARTEMENTALES.
</SCT>
<ANA ID="9A"> 28-005-03 1) Il résulte des articles R. 52, R. 66 et R. 68 du code électoral que dans l'hypothèse où est portée au procès-verbal une réclamation portant sur la validité de certains bulletins de vote, désignés avec une précision suffisante, qui ont été pris en considération dans le décompte des voix, il appartient au bureau de vote de statuer sur cette difficulté, en faisant mention de sa décision, motivée, au procès-verbal et en y annexant les bulletins contestés, en vue de permettre, le cas échéant, au juge de l'élection d'exercer son contrôle.,,,2) En revanche, lorsqu'une réclamation porte, non sur certains bulletins précisément identifiables, mais sur la régularité de l'ensemble des opérations de dépouillement et qu'un nouveau décompte est demandé, il appartient au bureau de vote, après avoir apprécié le sérieux de la contestation, de procéder à un nouveau décompte ou de mentionner au procès verbal les motifs de son refus, puis de détruire en présence des électeurs l'ensemble des bulletins autres que ceux qui ont été exclus du résultat du dépouillement.</ANA>
<ANA ID="9B"> 28-08-05-03-02 Prise en compte irrégulière, dans les résultats du second tour de scrutin, de deux suffrages qui auraient dû être tenus pour nuls.,,,Il y a lieu, pour apprécier l'influence de l'irrégularité sur le résultat de l'élection, selon le mode de scrutin prévu par les articles L. 338 et L. 338-1 du code électoral, de soustraire successivement deux suffrages du total de ceux obtenus par chacune des trois listes de candidats en présence au second tour de scrutin, en diminuant de deux unités le total des suffrages exprimés.... ,,Lorsque ce retranchement hypothétique ne remet en cause ni l'attribution de la prime majoritaire à la première liste, ni la répartition des sièges entre les listes, l'élection des membres du conseil régional n'est pas remise en cause, sans qu'il y ait lieu de rechercher, par des calculs hypothétiques, si la répartition entre sections départementales des sièges obtenus par chaque liste s'en trouve affectée.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
