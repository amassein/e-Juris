<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026537602</ID>
<ANCIEN_ID>JG_L_2012_10_000000346947</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/53/76/CETATEXT000026537602.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 26/10/2012, 346947</TITRE>
<DATE_DEC>2012-10-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>346947</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MONOD, COLIN ; SCP GADIOU, CHEVALLIER</AVOCATS>
<RAPPORTEUR>M. Rémi Decout-Paolini</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:346947.20121026</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 23 février et 23 mai 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour Mme A...B..., demeurant..., " ; Mme B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 09VE02243 du 2 décembre 2010 par lequel la cour administrative d'appel de Versailles a rejeté sa requête tendant à l'annulation du jugement n° 0703547 du 5 mai 2009 par lequel le tribunal administratif de Versailles a rejeté sa demande tendant à l'annulation de la décision du 25 septembre 2006 du maire de Dourdan d'exercer le droit de préemption sur la parcelle non bâtie cadastrée AE 6, sise au lieu-dit " Les Pierres Aigues " ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Dourdan le versement de la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu le code de l'expropriation pour cause d'utilité publique ;<br/>
<br/>
              Vu le code de l'urbanisme ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Rémi Decout-Paolini, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Monod, Colin, avocat de Mme B...et de la SCP Gadiou, Chevallier, avocat de la commune de Dourdan,<br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Monod, Colin, avocat de Mme B... et de la SCP Gadiou, Chevallier, avocat de la commune de Dourdan ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par une décision du 25 septembre 2006, le maire de Dourdan a préempté une parcelle incluse dans la zone d'aménagement différé (ZAD) du " Bois Bréant ", créée par arrêté préfectoral du 5 mai 1997 ; que Mme B..., acquéreur évincé, se pourvoit en cassation contre l'arrêt par lequel, en appel d'un  jugement du 5 mai 2009 du tribunal administratif de Versailles, la cour administrative d'appel de Versailles a rejeté sa demande d'annulation de cette décision de préemption ;<br/>
<br/>
              Sur la régularité de l'arrêt attaqué :<br/>
<br/>
              2. Considérant que Mme B...n'avait pas soulevé, devant les juges d'appel, de moyen tiré du défaut de publicité de la délibération par laquelle le conseil municipal de Dourdan a délégué au maire de cette ville l'exercice des droits de préemption prévus par le code de l'urbanisme ; que la cour n'a, dès lors, pas entaché son arrêt d'une insuffisance de motivation en se bornant à indiquer que la requérante n'était pas fondée à soutenir que la décision de préemption aurait été prise par une autorité incompétente ;<br/>
<br/>
              3. Considérant que la cour a estimé que Mme B...n'était pas recevable à exciper de l'illégalité de l'arrêté préfectoral du 5 mai 1997 créant la zone d'aménagement différé ; qu'elle n'était, dès lors, pas tenue de répondre au moyen par lequel Mme B... contestait la légalité de cet arrêté préfectoral ;<br/>
<br/>
              Sur le bien-fondé de l'arrêt attaqué :<br/>
<br/>
              4. Considérant qu'aux termes du premier alinéa de l'article L. 210-1 du code de l'urbanisme : " Les droits de préemption (...) sont exercés en vue de la réalisation, dans l'intérêt général, des actions ou opérations répondant aux objets définis à l'article L. 300-1 (...) ou pour constituer des réserves foncières en vue de permettre la réalisation desdites actions ou opérations d'aménagement " ; qu'aux termes de l'article L. 212-2 du même code, dans sa version alors applicable : " Dans les zones d'aménagement différé, un droit de préemption, qui peut être exercé pendant une période de quatorze ans à compter de la publication de l'acte qui a créé la zone, (...) est ouvert à une collectivité publique ou à un établissement public y ayant vocation (...) / L'acte créant la zone désigne le titulaire du droit de préemption " ;<br/>
<br/>
              5. Considérant qu'il résulte de ces dispositions que l'acte de création d'une zone d'aménagement différé, qui rend applicable au sein de cette zone les dispositions du code de l'urbanisme qui régissent l'exercice du droit de préemption, constitue une base légale des décisions de préemption prises dans son périmètre ; que l'illégalité de cet acte est, par suite, susceptible d'être utilement invoquée au soutien de conclusions dirigées contre une décision de préemption ; que toutefois cet acte, qui ne revêt pas un caractère réglementaire, ne forme pas avec les décisions individuelles de préemption prises dans la zone une opération administrative unique comportant un lien tel que les illégalités qui l'affecteraient pourraient, alors même qu'il aurait acquis un caractère définitif, être régulièrement invoquées par la voie de l'exception ;<br/>
<br/>
              6. Considérant, par suite, qu'en jugeant que Mme B...n'était pas recevable à soulever, à l'appui de sa demande d'annulation de la décision de préemption du 25 septembre 2006, l'illégalité de l'arrêté préfectoral du 5 mai 1997 créant la ZAD du " Bois Bréant ", devenu définitif, la cour n'a pas commis d'erreur de droit ;<br/>
<br/>
              7. Considérant qu'aux termes du deuxième alinéa de l'article L. 210-1 du code de l'urbanisme, dans sa version alors applicable : " Toute décision de préemption doit mentionner l'objet pour lequel ce droit est exercé. Toutefois, lorsque le droit de préemption est exercé à des fins de réserves foncières dans le cadre d'une zone d'aménagement différé, la décision peut se référer aux motivations générales mentionnées dans l'acte créant la zone " ; <br/>
<br/>
              8. Considérant qu'en estimant que la commune de Dourdan avait satisfait à l'obligation de motivation fixée par l'article L. 210-1 du code de l'urbanisme en motivant la décision de préemption attaquée par le rappel de l'opération d'aménagement justifiant la constitution de réserves foncières dans la ZAD du " Bois Bréant " ainsi que par la mention des objectifs généraux de cette opération, mais sans faire état d'une opération propre au bien préempté, la cour n'a pas commis d'erreur de droit et n'a pas dénaturé les pièces du dossier ;<br/>
<br/>
              9. Considérant qu'en jugeant que le titulaire du droit de préemption n'a pas à justifier d'un projet précis d'action ou d'opération d'aménagement, la cour n'a pas commis d'erreur de droit ; qu'elle n'a pas non plus dénaturé les pièces du dossier en estimant que la commune justifiait, à la date de la décision de préemption litigieuse, de la réalité d'un projet de développement d'activités économiques répondant à l'objectif figurant dans la motivation de cette décision ;<br/>
<br/>
              10. Considérant qu'il résulte de tout ce qui précède que Mme B...n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque ; que son pourvoi doit, par suite, être rejeté, y compris les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ; qu'il y a lieu, en revanche, dans les circonstances de l'espèce, de mettre à la charge de MmeB..., sur le fondement des mêmes dispositions, le versement à la commune de Dourdan de la somme de 3 000 euros ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de Mme B...est rejeté.<br/>
Article 2 : Mme B...versera à la commune de Dourdan une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à Mme A...B...et à la commune de Dourdan.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-01-06-03-01 ACTES LÉGISLATIFS ET ADMINISTRATIFS. DIFFÉRENTES CATÉGORIES D'ACTES. ACTES ADMINISTRATIFS - CLASSIFICATION. OPÉRATIONS COMPLEXES. ABSENCE. - PRÉEMPTION RÉALISÉE DANS LE PÉRIMÈTRE D'UNE ZAD - ACTE DE CRÉATION DE LA ZAD ET DÉCISION DE PRÉEMPTION - CONSÉQUENCE - IMPOSSIBILITÉ DE CONTESTER L'ACTE DE CRÉATION DE LA ZAD S'IL EST DEVENU DÉFINITIF.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-07-01-04-04-02 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. QUESTIONS GÉNÉRALES. MOYENS. EXCEPTION D'ILLÉGALITÉ. RECEVABILITÉ. - PRÉEMPTION RÉALISÉE DANS LE PÉRIMÈTRE D'UNE ZAD - 1) POSSIBILITÉ D'INVOQUER PAR LA VOIE DE L'EXCEPTION L'ILLÉGALITÉ DE L'ACTE DE CRÉATION DE LA ZAD AU SOUTIEN DE CONCLUSIONS DIRIGÉES CONTRE LA DÉCISION DE PRÉEMPTION - EXISTENCE, DÈS LORS QUE L'ACTE DE CRÉATION DE LA ZAD EST UNE BASE LÉGALE DE LA PRÉEMPTION [RJ1] - 2) OPÉRATION COMPLEXE - ABSENCE - CONSÉQUENCE - IMPOSSIBILITÉ DE CONTESTER L'ACTE DE CRÉATION DE LA ZAD S'IL EST DEVENU DÉFINITIF.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-07-01-04-04-02-01 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. QUESTIONS GÉNÉRALES. MOYENS. EXCEPTION D'ILLÉGALITÉ. RECEVABILITÉ. OPÉRATIONS COMPLEXES. - PRÉEMPTION RÉALISÉE DANS LE PÉRIMÈTRE D'UNE ZAD - ACTE DE CRÉATION DE LA ZAD ET DÉCISION DE PRÉEMPTION - OPÉRATION COMPLEXE - ABSENCE - CONSÉQUENCE - IMPOSSIBILITÉ DE CONTESTER L'ACTE DE CRÉATION DE LA ZAD S'IL EST DEVENU DÉFINITIF.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">68-02-01-01-02 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PROCÉDURES D'INTERVENTION FONCIÈRE. PRÉEMPTION ET RÉSERVES FONCIÈRES. DROITS DE PRÉEMPTION. ZONES D'AMÉNAGEMENT DIFFÉRÉ. - PRÉEMPTION RÉALISÉE DANS LE PÉRIMÈTRE D'UNE ZAD - 1) POSSIBILITÉ D'INVOQUER PAR LA VOIE DE L'EXCEPTION L'ILLÉGALITÉ DE L'ACTE DE CRÉATION DE LA ZAD AU SOUTIEN DE CONCLUSIONS DIRIGÉES CONTRE LA DÉCISION DE PRÉEMPTION - EXISTENCE, DÈS LORS QUE L'ACTE DE CRÉATION DE LA ZAD EST UNE BASE LÉGALE DE LA PRÉEMPTION [RJ1] - 2) OPÉRATION COMPLEXE - ABSENCE - CONSÉQUENCE - IMPOSSIBILITÉ DE CONTESTER L'ACTE DE CRÉATION DE LA ZAD S'IL EST DEVENU DÉFINITIF.
</SCT>
<SCT ID="8E" TYPE="PRINCIPAL">68-06-04-01 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. POUVOIRS DU JUGE. MOYENS. - PRÉEMPTION RÉALISÉE DANS LE PÉRIMÈTRE D'UNE ZAD - 1) POSSIBILITÉ D'INVOQUER PAR LA VOIE DE L'EXCEPTION L'ILLÉGALITÉ DE L'ACTE DE CRÉATION DE LA ZAD AU SOUTIEN DE CONCLUSIONS DIRIGÉES CONTRE LA DÉCISION DE PRÉEMPTION - EXISTENCE, DÈS LORS QUE L'ACTE DE CRÉATION DE LA ZAD EST UNE BASE LÉGALE DE LA PRÉEMPTION [RJ1] - 2) OPÉRATION COMPLEXE - ABSENCE - CONSÉQUENCE - IMPOSSIBILITÉ DE CONTESTER L'ACTE DE CRÉATION DE LA ZAD S'IL EST DEVENU DÉFINITIF.
</SCT>
<ANA ID="9A"> 01-01-06-03-01 L'acte de création d'une zone d'aménagement différé (ZAD), qui rend applicable au sein de cette zone les dispositions du code de l'urbanisme qui régissent l'exercice du droit de préemption, constitue une base légale des décisions de préemption prises dans son périmètre. L'illégalité de cet acte est, par suite, susceptible d'être utilement invoquée au soutien de conclusions dirigées contre une décision de préemption. Toutefois, cet acte, qui ne revêt pas un caractère réglementaire, ne forme pas avec les décisions individuelles de préemption prises dans la zone une opération administrative unique comportant un lien tel que les illégalités qui l'affecteraient pourraient, alors même qu'il aurait acquis un caractère définitif, être régulièrement invoquées par la voie de l'exception si aucun recours n'a été formé avant l'expiration des délais de recours.</ANA>
<ANA ID="9B"> 54-07-01-04-04-02 L'acte de création d'une zone d'aménagement différé, qui rend applicable au sein de cette zone les dispositions du code de l'urbanisme qui régissent l'exercice du droit de préemption, constitue une base légale des décisions de préemption prises dans son périmètre. 1) L'illégalité de cet acte est, par suite, susceptible d'être utilement invoquée au soutien de conclusions dirigées contre une décision de préemption. 2) Toutefois, cet acte, qui ne revêt pas un caractère réglementaire, ne forme pas avec les décisions individuelles de préemption prises dans la zone une opération administrative unique comportant un lien tel que les illégalités qui l'affecteraient pourraient, alors même qu'il aurait acquis un caractère définitif, être régulièrement invoquées par la voie de l'exception si aucun recours n'a été formé avant l'expiration des délais de recours.</ANA>
<ANA ID="9C"> 54-07-01-04-04-02-01 L'acte de création d'une zone d'aménagement différé (ZAD), qui rend applicable au sein de cette zone les dispositions du code de l'urbanisme qui régissent l'exercice du droit de préemption, constitue une base légale des décisions de préemption prises dans son périmètre. L'illégalité de cet acte est, par suite, susceptible d'être utilement invoquée au soutien de conclusions dirigées contre une décision de préemption. Toutefois, cet acte, qui ne revêt pas un caractère réglementaire, ne forme pas avec les décisions individuelles de préemption prises dans la zone une opération administrative unique comportant un lien tel que les illégalités qui l'affecteraient pourraient, alors même qu'il aurait acquis un caractère définitif, être régulièrement invoquées par la voie de l'exception si aucun recours n'a été formé avant l'expiration des délais de recours.</ANA>
<ANA ID="9D"> 68-02-01-01-02 L'acte de création d'une zone d'aménagement différé, qui rend applicable au sein de cette zone les dispositions du code de l'urbanisme qui régissent l'exercice du droit de préemption, constitue une base légale des décisions de préemption prises dans son périmètre. 1) L'illégalité de cet acte est, par suite, susceptible d'être utilement invoquée au soutien de conclusions dirigées contre une décision de préemption. 2) Toutefois, cet acte, qui ne revêt pas un caractère réglementaire, ne forme pas avec les décisions individuelles de préemption prises dans la zone une opération administrative unique comportant un lien tel que les illégalités qui l'affecteraient pourraient, alors même qu'il aurait acquis un caractère définitif, être régulièrement invoquées par la voie de l'exception si aucun recours n'a été formé avant l'expiration des délais de recours.</ANA>
<ANA ID="9E"> 68-06-04-01 L'acte de création d'une zone d'aménagement différé, qui rend applicable au sein de cette zone les dispositions du code de l'urbanisme qui régissent l'exercice du droit de préemption, constitue une base légale des décisions de préemption prises dans son périmètre. 1) L'illégalité de cet acte est, par suite, susceptible d'être utilement invoquée au soutien de conclusions dirigées contre une décision de préemption. 2) Toutefois, cet acte, qui ne revêt pas un caractère réglementaire, ne forme pas avec les décisions individuelles de préemption prises dans la zone une opération administrative unique comportant un lien tel que les illégalités qui l'affecteraient pourraient, alors même qu'il aurait acquis un caractère définitif, être régulièrement invoquées par la voie de l'exception si aucun recours n'a été formé avant l'expiration des délais de recours.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Section, 11 juillet 2011, Société d'équipement du département de Maine-et-Loire Sodemel et ministre de l'intérieur, de l'outre-mer, des collectivités territoriales et de l'immigration, n°s 320735 320854, p. 346.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
