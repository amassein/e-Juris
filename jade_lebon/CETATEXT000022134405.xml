<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000022134405</ID>
<ANCIEN_ID>JG_L_2010_04_000000336753</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/22/13/44/CETATEXT000022134405.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 14/04/2010, 336753, Publié au recueil Lebon</TITRE>
<DATE_DEC>2010-04-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>336753</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Contentieux des pensions</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Martin</PRESIDENT>
<AVOCATS>SCP LYON-CAEN, FABIANI, THIRIEZ</AVOCATS>
<RAPPORTEUR>M. Jean  Lessi</RAPPORTEUR>
<COMMISSAIRE_GVT>Mlle Courrèges Anne</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2010:336753.20100414</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le mémoire, enregistré le 1er mars 2010 au secrétariat du contentieux du Conseil d'Etat, présenté pour Mme Khedidja A et M. Mokhtar A, demeurant ..., en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958 ; les requérants demandent au Conseil d'Etat, à l'appui de leur pourvoi tendant à l'annulation du jugement du 12 mars 2009 par lequel le tribunal administratif de Nantes a rejeté leur demande d'annulation de la décision implicite du ministre de la défense refusant de revaloriser leurs pensions militaires d'ayant cause à compter du 3 juillet 1962, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des articles 71 de la loi n° 59-1454 du 26 décembre 1959 portant loi de finances pour 1960, 26 de la loi n° 81-734 du 3 août 1981 de finances rectificative pour 1981, 68 de la loi n° 2002-1576 du 30 décembre 2002 de finances rectificative pour 2002 et 100 de la loi n° 2006-1666 du 21 décembre 2006 de finances pour 2007 ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Déclaration des droits de l'homme et du citoyen du 26 août 1789, notamment ses articles 6 et 16 ;<br/>
<br/>
              Vu la Constitution, notamment son Préambule et son article 61-1 ;<br/>
<br/>
              Vu l'ordonnance n° 58-1067 du 7 novembre 1958 ; <br/>
<br/>
              Vu la loi n° 59-1454 du 26 décembre 1959 ; <br/>
<br/>
              Vu la loi n° 81-734 du 3 août 1981 ; <br/>
<br/>
              Vu la loi n° 2002-1576 du 30 décembre 2002 ; <br/>
<br/>
              Vu la loi n° 2006-1666 du 21 décembre 2006 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean Lessi, Auditeur,  <br/>
<br/>
              - les observations de la SCP Lyon-Caen, Fabiani, Thiriez, avocat de Mme Khedidja A et M. Mokhtar A,<br/>
<br/>
              - les conclusions de Mlle Anne Courrèges, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Lyon-Caen, Fabiani, Thiriez, avocat de Mme Khedidja A et M. Mokhtar A ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant qu'aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé, y compris pour la première fois en cassation, à l'occasion d'une instance devant le Conseil d'Etat (...) " ; qu'il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et qu'elle soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              Considérant, en premier lieu, que l'article 71 de la loi du 26 décembre 1959 a pour objet de cristalliser les pensions, rentes ou allocations viagères servies aux ressortissants des pays ou territoires ayant appartenu à l'Union française ou à la Communauté ou ayant été placés sous le protectorat ou sous la tutelle de la France, à l'exclusion toutefois des ressortissants algériens dont la situation est régie, sur ce point, par les seules dispositions de l'article 26 de la loi du 3 août 1981 ; que le litige soulevé par Mme Khedidja A et M. Mokhtar A, tous deux ressortissants algériens, a trait à la revalorisation de la pension temporaire d'orphelin concédée à M. A ainsi que de la pension militaire de retraite d'ayant cause concédée à Mme A par un arrêté du 23 novembre 1958 ; que l'article 71 de la loi du 26 décembre 1959 contesté au regard de la Constitution n'est, par conséquent, pas applicable au présent litige ;<br/>
<br/>
              Considérant, en deuxième lieu, que les articles 26 de la loi du 3 août 1981 et 68 de la loi du 30 décembre 2002 sont applicables au présent litige au sens et pour l'application de l'article 23-5 de l'ordonnance du 7 novembre 1958 ; que ces dispositions n'ont pas déjà été déclarées conformes à la Constitution par le Conseil constitutionnel ; que le moyen tiré de ce qu'elles portent atteinte aux droits et libertés garantis par la Constitution, et notamment au principe d'égalité, soulève une question présentant un caractère sérieux ; qu'ainsi, il y a lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée ;<br/>
<br/>
              Considérant, enfin, que l'article 100 de la loi du 21 décembre 2006 a pour objet de décristalliser complètement, à compter du 1er janvier 2007, d'une part, la valeur du point de base des retraites du combattant et des pensions militaires d'invalidité servies aux ressortissants des anciens territoires placés sous souveraineté française et, d'autre part, les indices servant au calcul de ces mêmes prestations ; qu'ainsi qu'il a été dit ci-dessus, le litige soulevé par M. et Mme A a trait à la revalorisation de la pension temporaire d'orphelin ainsi que de la pension militaire de retraite d'ayant cause qui leur ont été respectivement concédées, lesquelles ne sont pas au nombre des prestations régies par la disposition contestée ; que, toutefois, les requérants soutiennent précisément que cette disposition, dont ils demandent le bénéfice, porte atteinte au principe d'égalité en ce qu'elle ne s'applique pas à la catégorie de prestations constituant l'objet du litige ; que, dans ces conditions, l'article 100 de la loi du 21 décembre 2006 doit être regardé comme applicable au litige au sens et pour l'application de l'article 23-5 de l'ordonnance du 7 novembre 1958 ; que ces dispositions n'ont pas déjà été déclarées conformes à la Constitution par le Conseil constitutionnel ; que le moyen tiré de ce qu'elles portent atteinte aux droits et libertés garantis par la Constitution, et notamment au principe d'égalité, soulève une question présentant un caractère sérieux ; qu'ainsi, il y a lieu, sur ce point également, de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question de la conformité à la Constitution de l'article 71 de la loi n° 59-1454 du 26 décembre 1959 portant loi de finances pour 1960.<br/>
Article 2 : Les questions de la conformité à la Constitution des articles 26 de la loi n° 81-734 du 3 août 1981 de finances rectificative pour 1981, 68 de la loi n° 2002-1576 du 30 décembre 2002 de finances rectificative pour 2002 et 100 de la loi n° 2006-1666 du 21 décembre 2006 de finances pour 2007 sont renvoyées au Conseil constitutionnel.<br/>
Article 3 : Il est sursis à statuer sur le pourvoi de Mme Khedidja A et M. Mokhtar A jusqu'à ce que le Conseil constitutionnel ait tranché les questions de constitutionnalité ainsi soulevées.<br/>
Article 4 : La présente décision sera notifiée à Mme Khedidja A, à M. Mokhtar A, au Premier ministre, au ministre de la défense et au ministre du budget, des comptes publics et de la réforme de l'Etat.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-10-05-01-02 PROCÉDURE. - DISPOSITION CRITIQUÉE AU REGARD DU PRINCIPE D'ÉGALITÉ EN TANT QU'ELLE NE S'APPLIQUE PAS À LA SITUATION À L'ORIGINE DU LITIGE.
</SCT>
<ANA ID="9A"> 54-10-05-01-02 Contestation de la constitutionnalité de l'article 100 de la loi n° 2006-1666 du 21 décembre 2006, qui a pour objet de décristalliser complètement, à compter du 1er janvier 2007, d'une part, la valeur du point de base des retraites du combattant et des pensions militaires d'invalidité servies aux ressortissants des anciens territoires placés sous souveraineté française et, d'autre part, les indices servant au calcul de ces mêmes prestations. Le litige à l'occasion duquel la question prioritaire de constitutionnalité est soulevée a trait à la revalorisation de la pension temporaire d'orphelin ainsi que de la pension militaire de retraite d'ayant cause qui ont été respectivement concédées aux deux requérants, lesquelles ne sont pas au nombre des prestations régies par la disposition contestée. Toutefois, les requérants soutiennent précisément que cette dernière disposition, dont ils demandent le bénéfice, porte atteinte au principe d'égalité en ce qu'elle ne s'applique pas à la catégorie de prestations constituant l'objet du litige. Dans ces conditions, l'article 100 de la loi du 21 décembre 2006 doit être regardé comme applicable au litige au sens et pour l'application de l'article 23-5 de l'ordonnance du 7 novembre 1958.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
