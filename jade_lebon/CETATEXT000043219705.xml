<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043219705</ID>
<ANCIEN_ID>JG_L_2021_03_000000438859</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/21/97/CETATEXT000043219705.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 04/03/2021, 438859</TITRE>
<DATE_DEC>2021-03-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>438859</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SARL DIDIER-PINET ; SCP BUK LAMENT - ROBILLOT</AVOCATS>
<RAPPORTEUR>M. Alexis Goin</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Marc Pichon de Vendeuil</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:438859.20210304</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Edenred France a demandé au juge des référés du tribunal administratif de Lyon, sur le fondement des dispositions de l'article L. 551-1 du code de justice administrative, d'annuler la procédure engagée par le département de la Loire pour la passation d'un accord-cadre s'agissant des lots nos 2, 3, 5 et 6 portant sur l'émission et la distribution de chèques emploi service, de titres-restaurants et de chèques cadeaux. <br/>
<br/>
              Par une ordonnance n° 2000411 du 4 février 2020, le juge des référés du tribunal administratif de Lyon a annulé les procédures de passation de ces lots.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 19 février et 5 mars 2020 au secrétariat du contentieux du Conseil d'Etat, le département de la Loire demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ; <br/>
<br/>
              2°) de mettre à la charge de la société Edenred France la somme de 3 600 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la commande publique ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ; <br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Alexis Goin, auditeur,  <br/>
<br/>
              - les conclusions de M. Marc Pichon de Vendeuil, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SARL Didier-Pinet, avocat du département de la Loire et à la SCP Buk Lament, Robillot, avocat de la société Edenred France ; <br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du référé précontractuel du tribunal administratif de Lyon que le département de la Loire, après avoir lancé la passation du lot n° 1 d'une consultation sous forme d'accord-cadre ayant pour objet l'émission et la distribution de chèques emploi-service universels préfinancés pour l'allocation personnalisée d'autonomie et la prestation de compensation du handicap, laquelle n'est pas en litige, a lancé, selon une procédure sans publicité ni mise en concurrence préalables, la passation des lots nos 2 à 6 de cet accord-cadre. Les lots nos 2 et 3 ont pour objet l'émission et la distribution de chèques emploi-service universels respectivement pour les agents placés dans diverses situations, dont le congé de maladie, et pour les agents handicapés, le lot n° 4 les chèques emploi-service universels " garde d'enfants " pour les agents du département, le lot n° 5 l'émission et la distribution de titres-restaurants pour ces agents, et le lot n° 6 l'émission et la distribution de titres cadeaux offerts au personnel du département. Par courriers des 24 et 26 décembre 2019, la société Edenred France a été invitée par le département à présenter une offre pour chacun des lots n°s 2 à 6. Par courrier du 9 janvier 2020, la société Edenred France a informé le département de la Loire qu'elle ne souhaitait pas présenter d'offre. Par l'ordonnance attaquée, le juge du référé précontractuel du tribunal administratif de Lyon, saisi par la société Edenred France sur le fondement de l'article L. 5511 du code de justice administrative, a annulé les procédures de passation des lots nos 2, 3, 5 et 6.<br/>
<br/>
              2. En premier lieu, aux termes de l'article L. 1111-1 du code de la commande publique : " Un marché est un contrat conclu par un ou plusieurs acheteurs soumis au présent code avec un ou plusieurs opérateurs économiques, pour répondre à leurs besoins en matière de travaux, de fournitures ou de services, en contrepartie d'un prix ou de tout équivalent ". Aux termes de l'article L. 1121-1 du même code : " Un contrat de concession est un contrat par lequel une ou plusieurs autorités concédantes soumises au présent code confient l'exécution de travaux ou la gestion d'un service à un ou plusieurs opérateurs économiques, à qui est transféré un risque lié à l'exploitation de l'ouvrage ou du service, en contrepartie soit du droit d'exploiter l'ouvrage ou le service qui fait l'objet du contrat, soit de ce droit assorti d'un prix. / La part de risque transférée au concessionnaire implique une réelle exposition aux aléas du marché, de sorte que toute perte potentielle supportée par le concessionnaire ne doit pas être purement théorique ou négligeable. Le concessionnaire assume le risque d'exploitation lorsque, dans des conditions d'exploitation normales, il n'est pas assuré d'amortir les investissements ou les coûts, liés à l'exploitation de l'ouvrage ou du service, qu'il a supportés ". Il résulte de ces dispositions qu'un contrat par lequel un acheteur public confie l'exécution de travaux ou la gestion d'un service à un ou plusieurs opérateurs économiques ne constitue un contrat de concession que s'il transfère un risque réel lié à l'exploitation de l'ouvrage ou du service et si le transfert de ce risque trouve sa contrepartie, au moins partiellement, dans le droit d'exploiter l'ouvrage ou le service. Le risque d'exploitation est constitué par le fait de ne pas être assuré d'amortir les investissements ou les coûts liés à l'exploitation du service.<br/>
<br/>
              3. Il ressort des pièces du dossier soumis au juge des référés du tribunal administratif de Lyon que le contrat faisant l'objet de la procédure de passation en litige a pour objet l'émission et la distribution de titres de paiement à destination des agents du département de la Loire. Si les stipulations du projet de contrat ne font pas obstacle à ce que, sous réserve des dispositions législatives et réglementaires encadrant chaque catégorie de titre préfinancé, le cocontractant qui projette d'exécuter le service prélève une commission à l'occasion du remboursement des titres aux personnes physiques ou morales les ayant acceptés en paiement ou place les sommes versées par le département durant le laps de temps précédant leur remboursement, le coût de l'émission des titres et de leur distribution est intégralement payé par le département et le cocontractant bénéficie, à titre de dépôt, des fonds nécessaires pour verser leur contre-valeur aux personnes physiques ou morales auprès desquelles les titres seront utilisés. Il résulte de ce qui précède que le cocontractant ne supporte aucun risque d'exploitation. Dans ces conditions, le contrat en litige ne revêt pas le caractère d'un contrat de concession, mais celui d'un marché public. Il s'ensuit que le juge du référé précontractuel du tribunal administratif de Lyon n'a pas méconnu le champ d'application de la loi en faisant application des dispositions du code de la commande publique relatives au calcul de la valeur estimée des marchés publics.<br/>
<br/>
              4. En deuxième lieu, selon l'article R. 2121-1 du code de la commande publique : " L'acheteur procède au calcul de la valeur estimée du besoin sur la base du montant total hors taxes du ou des marchés envisagés ". Aux termes de l'article R. 2121-3 du même code : " La valeur du besoin à prendre en compte est celle estimée au moment de l'envoi de l'avis d'appel à la concurrence ou, en l'absence d'un tel avis, au moment où l'acheteur lance la consultation ". Aux termes de l'article R. 2121-4 du même code : " L'acheteur ne peut se soustraire à l'application du présent livre en scindant ses achats ou en utilisant des modalités de calcul de la valeur estimée du besoin autres que celles qui y sont prévues ". Selon l'article R. 2121-6 du même code : " Pour les marchés de fourniture ou de services, la valeur estimée du besoin est déterminée, quels que soient le nombre d'opérateurs économiques auquel il est fait appel et le nombre de marchés à passer, en prenant en compte la valeur totale des fournitures ou des services qui peuvent être considérés comme homogènes soit en raison de leurs caractéristiques propres, soit parce qu'ils constituent une unité fonctionnelle ". Aux termes de l'article R. 21218 du même code : " Pour les accords-cadres et les systèmes d'acquisition dynamiques définis à l'article L. 2125-1, la valeur estimée du besoin est déterminée en prenant en compte la valeur maximale estimée de l'ensemble des marchés à passer ou des bons de commande à émettre pendant la durée totale de l'accord-cadre ou du système d'acquisition dynamique. / Lorsque l'accord-cadre ne fixe pas de maximum, sa valeur estimée est réputée excéder les seuils de procédure formalisée ".<br/>
<br/>
              5. Pour l'application de ces dispositions à un marché de titres de paiement, l'acheteur doit prendre en compte, outre les frais de gestion versés par le pouvoir adjudicateur, la valeur faciale des titres susceptibles d'être émis pour son exécution, somme que le pouvoir adjudicateur doit payer à son cocontractant en contrepartie des titres mis à sa disposition. Dès lors, en jugeant qu'il appartenait à l'acheteur public d'établir le montant d'un marché de titres de paiement en prenant en compte la valeur faciale totale des titres susceptibles d'être émis pour son exécution, augmentée d'une évaluation sincère des frais de gestion prévisibles, le juge du référé précontractuel du tribunal administratif de Lyon n'a pas commis d'erreur de droit. <br/>
<br/>
              6. En troisième lieu, il appartient au juge des référés, saisi en vertu des dispositions de l'article L. 551-1 du code de justice administrative, de rechercher si l'entreprise qui le saisit se prévaut de manquements qui, eu égard à leur portée et au stade de la procédure auquel ils se rapportent, sont susceptibles de l'avoir lésée ou risquent de la léser, fût-ce de manière indirecte en avantageant une entreprise concurrente.<br/>
<br/>
              7. Il ressort des pièces du dossier soumis au juge du référé précontractuel du tribunal administratif de Lyon que la société Edenred France a été dissuadée de présenter une offre par l'irrégularité dont elle considérait que la procédure était entachée, qui conduisait à ce que la passation des lots en litige soit dispensée de formalités de publicité et de mise en concurrence. En estimant, par une ordonnance qui est suffisamment motivée, que la société Edenred France était susceptible d'être lésée par ce manquement, alors même qu'elle avait été invitée à se porter candidate par le département de la Loire, le juge des référés a exactement qualifié les faits qui lui étaient soumis.<br/>
<br/>
              8. Il résulte de ce qui précède que le département de la Loire n'est pas fondé à demander l'annulation de l'ordonnance qu'il attaque.<br/>
<br/>
              9. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du département de la Loire la somme de 3 000 euros à verser à la société Edenred France au titre de l'article L. 761-1 du code de justice administrative. Les mêmes dispositions font obstacle à ce qu'une somme soit mise à la charge de la société Edenred France, qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi du département de la Loire est rejeté.<br/>
Article 2 : Le département de la Loire versera à la société Edenred France une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 3 : La présente décision sera notifiée au département de la Loire, à la société Edenred France, et au ministre de l'économie, des finances et de la relance.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-02-005 MARCHÉS ET CONTRATS ADMINISTRATIFS. FORMATION DES CONTRATS ET MARCHÉS. FORMALITÉS DE PUBLICITÉ ET DE MISE EN CONCURRENCE. - CALCUL DE LA VALEUR ESTIMÉE DU BESOIN - CAS PARTICULIER D'UN MARCHÉ DE TITRES DE PAIEMENT.
</SCT>
<ANA ID="9A"> 39-02-005 Pour le calcul de la valeur estimée de son besoin s'agissant d'un marché de titres de paiement, l'acheteur doit prendre en compte, outre les frais de gestion versés par le pouvoir adjudicateur, la valeur faciale des titres susceptibles d'être émis pour son exécution, somme que le pouvoir adjudicateur doit payer à son cocontractant en contrepartie des titres mis à sa disposition.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
