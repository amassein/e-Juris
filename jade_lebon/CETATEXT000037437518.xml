<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037437518</ID>
<ANCIEN_ID>JG_L_2018_09_000000404777</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/43/75/CETATEXT000037437518.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème et 1ère chambres réunies, 26/09/2018, 404777</TITRE>
<DATE_DEC>2018-09-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>404777</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème et 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUVENIN, COUDRAY, GREVY</AVOCATS>
<RAPPORTEUR>M. Olivier  Fuchs</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:404777.20180926</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              1° Sous le n° 404777, par une requête sommaire, un mémoire complémentaire et trois nouveaux mémoires, enregistrés le 31 octobre 2016, les 31 janvier, 30 août et 12 décembre 2017 et le 5 septembre 2018 au secrétariat du contentieux du Conseil d'Etat, la Fédération des syndicats généraux de l'éducation nationale et de la recherche publique (SGEN-CFDT) demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2016-1173 du 29 août 2016 modifiant le décret n° 2009-464 du 23 avril 2009 relatif aux doctorants contractuels des établissements publics administratifs d'enseignement supérieur ou de recherche ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              2° Sous le n° 404780, par une requête sommaire, un mémoire complémentaire et un nouveau mémoire, enregistrés les 31 octobre 2016, 31 janvier 2017 et 5 septembre 2018 au secrétariat du contentieux du Conseil d'Etat, la Fédération des syndicats généraux de l'Education nationale et de la recherche publique (SGEN - CFDT) demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté du 29 août 2016 fixant le montant de la rémunération du doctorant contractuel ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - le code de l'éducation ;<br/>
              - la loi n° 84-16 du 11 janvier 1984 ;<br/>
              - le décret n° 2011-184 du 15 février 2011 ;<br/>
              - le décret n° 2014-1092 du 26 septembre 2014 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Fuchs, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Thouvenin, Coudray, Grevy, avocat de la Fédération des syndicats généraux de l'éducation nationale et de la recherche publique (SGEN-CFDT) ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 7 septembre 2018, présentée par la Fédération des syndicats généraux de l'éducation nationale et de la recherche publique (SGEN-CFDT) ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que la requête par laquelle la Fédération des syndicats généraux de l'éducation nationale et de la recherche publique (SGEN - CFDT) demande l'annulation du décret du 29 août 2016 modifiant le décret n° 2009-464 du 23 avril 2009 relatif aux doctorants contractuels des établissements publics administratifs d'enseignement supérieur ou de recherche et la requête par laquelle la même fédération demande l'annulation, par voie de conséquence de l'illégalité de ce décret, de l'arrêté du 29 août 2016 fixant le montant de la rémunération du doctorant contractuel, présentent à juger les mêmes questions ; qu'il y a lieu de les joindre pour statuer par une seule décision ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article 34 du décret du 15 février 2011 relatif aux comités techniques dans les administrations et les établissements publics de l'Etat : " Les comités techniques sont consultés (...) sur les questions et projets de textes relatifs : 1° A l'organisation et au fonctionnement des administrations, établissements ou services ; 2° A la gestion prévisionnelle des effectifs, des emplois et des compétences ; 3° Aux règles statutaires et aux règles relatives à l'échelonnement indiciaire (...) " ; qu'aux termes de l'article 45 du même décret : " L'acte portant convocation du comité technique fixe l'ordre du jour de la séance. (...) Le président du comité, à son initiative ou à la demande de membres titulaires du comité, peut convoquer des experts afin qu'ils soient entendus sur un point inscrit à l'ordre du jour. Les experts ne peuvent assister, à l'exclusion du vote, qu'à la partie des débats relative aux questions pour lesquelles leur présence a été demandée " ; qu'il résulte de ces dernières dispositions que le droit à l'assistance d'experts est reconnu, dans les conditions qu'elles prévoient, aux membres titulaires des comités techniques ; <br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier que, lors de la réunion du 7 juillet 2016 du comité technique ministériel de l'enseignement supérieur et de la recherche au cours de laquelle a été examiné le projet de décret litigieux, l'expert convoqué, à la demande du syndicat CFDT, sur le fondement des dispositions de l'article 45 du décret du 15 février 2011 citées ci-dessus, a été présent et a pris la parole au cours de la discussion générale sur le projet de texte ; qu'en revanche, à la demande de la présidente du comité, il a quitté la salle au moment de la discussion des amendements présentés sur le projet de texte par les organisations syndicales et n'a ainsi pris la parole ni lors de l'examen de chacun de ces amendements, ni avant le vote final du comité sur le projet de décret ; que la fédération requérante soutient qu'en raison de cette circonstance, le droit à l'assistance d'un expert a été méconnu et que cette méconnaissance entache d'irrégularité la procédure de consultation ;<br/>
<br/>
              4. Considérant, toutefois, que si, pour la mise en oeuvre du droit mentionné au point 2 ci-dessus, il y avait lieu d'entendre l'expert convoqué à la demande du syndicat CFDT sur l'ensemble du projet de texte et sur les amendements qui ont été, en l'espèce, débattus, il ressort des pièces du dossier que cet expert a pu, au cours de la discussion générale qui a eu lieu pendant la première partie de la séance, exposer de manière complète les risques et inconvénients que comportaient, à ses yeux, certaines dispositions du projet de texte et préciser celles qu'il jugeait bon de retirer ou d'amender ; qu'ainsi, alors même que, ainsi que le soutient la fédération requérante, son absence, pendant la seconde partie de la séance, aurait, pour l'examen des amendements  mis en discussion, privé certains membres du comité d'explications utiles, une telle absence, d'ailleurs motivée notamment par le souci d'éviter que l'intéressé ne soit tenu, en vertu des dispositions citées au point 2, de quitter la salle à vingt-huit reprises lors de chacun des votes sur les vingt-huit amendements, n'a pas fait obstacle, en l'espèce, à ce que le comité se prononce en toute connaissance de cause sur le projet de texte qui lui était  soumis ; que la fédération requérante n'est, par suite, pas fondée à soutenir que le décret attaqué aurait été adopté au vu d'un avis rendu dans des conditions irrégulières ;<br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que la fédération  requérante n'est pas fondée à demander l'annulation du décret qu'elle attaque ; qu'elle n'est, par suite, pas davantage fondée à demander l'annulation, par voie de conséquence,  de l'arrêté du 29 août 2016 ; <br/>
<br/>
              6. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de l'Etat, qui n'est pas la partie perdante dans la présente instance, la somme que demande à ce titre la fédération requérante ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les requêtes de la Fédération des syndicats généraux de l'Education nationale et de la recherche publique (SGEN - CFDT) sont rejetées.<br/>
Article 2 : La présente décision sera notifiée à la Fédération des syndicats généraux de l'Education nationale et de la recherche publique (SGEN - CFDT), à la ministre de l'enseignement supérieur, de la recherche et de l'innovation, au ministre de l'action et des comptes publics et au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-07-06-05 FONCTIONNAIRES ET AGENTS PUBLICS. STATUTS, DROITS, OBLIGATIONS ET GARANTIES. COMITÉS TECHNIQUES PARITAIRES. PROCÉDURE. - ASSISTANCE D'EXPERTS - 1) PRINCIPE - DROIT RECONNU AUX MEMBRES TITULAIRES, DANS LES CONDITIONS PRÉVUES PAR LE DÉCRET DU 15 FÉVRIER 2011 - 2) MISE EN OEUVRE LORS DE L'EXAMEN D'UN PROJET DE TEXTE - A) PRINCIPE - B) HYPOTHÈSE DE PRÉSENCE DE L'EXPERT AU COURS DE LA DISCUSSION GÉNÉRALE, MAIS NON LORS DE L'EXAMEN DES AMENDEMENTS - IRRÉGULARITÉ - ABSENCE, DANS LES CIRCONSTANCES DE L'ESPÈCE.
</SCT>
<ANA ID="9A"> 36-07-06-05 1) Il résulte de l'article 45 du décret n° 2011-184 du 15 février 2011 relatif aux comités techniques dans les administrations et les établissements publics de l'Etat que le droit à l'assistance d'experts est reconnu, dans les conditions qu'elles prévoient, aux membres titulaires des comités techniques.,,2) a) Pour la mise en oeuvre de ce droit, il y a lieu d'entendre l'expert sur l'ensemble du projet de texte et sur les amendements qui ont été débattus.... ...b) Expert ayant pu, au cours de la discussion générale, exposer de manière complète les risques et inconvénients que comportaient, à ses yeux, certaines dispositions du projet de texte et préciser celles qu'il jugeait bon de retirer ou d'amender. Alors même que son absence aurait, pour l'examen des amendements mis en discussion, privé certains membres du comité d'explications utiles, une telle absence, d'ailleurs motivée notamment par le souci d'éviter que l'intéressé ne soit tenu de quitter la salle à vingt-huit reprises lors de chacun des votes sur les vingt-huit amendements, n'a pas fait obstacle, en l'espèce, à ce que le comité se prononce en toute connaissance de cause sur le projet de texte qui lui était  soumis.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
