<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031861187</ID>
<ANCIEN_ID>JG_L_2015_12_000000376527</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/86/11/CETATEXT000031861187.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème / 4ème SSR, 23/12/2015, 376527</TITRE>
<DATE_DEC>2015-12-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>376527</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème / 4ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARTHELEMY, MATUCHANSKY, VEXLIARD, POUPOT ; SCP BOUTET, HOURDEAUX</AVOCATS>
<RAPPORTEUR>Mme Marie Gautier-Melleray</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:376527.20151223</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La commune de Montereau-Fault-Yonne a saisi le tribunal administratif de Melun d'une demande tendant à la condamnation de la société Axa France Iard à lui verser une somme de 250 899 euros, correspondant au montant des travaux de reprise nécessaires à la réparation des désordres constatés sur l'immeuble de la maison des services publics. Par un jugement n° 0903224 du 3 novembre 2011, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 12PA00052 du 20 janvier 2014, la cour administrative d'appel de Paris a rejeté l'appel formé par la commune contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique enregistrés les 20 mars 2014, 23 juin 2014 et 7 décembre 2015 au secrétariat du contentieux du Conseil d'Etat, la commune de Montereau-Fault-Yonne demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses conclusions d'appel ;<br/>
<br/>
              3°) de mettre à la charge de la société Axa France Iard la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - le code des assurances ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie Gautier-Melleray, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Barthélemy, Matuchansky, Vexliard, Poupot, avocat de la commune de Montereau-Fault-Yonne et à la SCP Boutet, Hourdeaux, avocat de la société Axa France Iard ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la commune de Montereau-Fault-Yonne a constaté au cours de l'année 2006 divers désordres affectant sa maison des services publics, bâtiment réceptionné sans réserve en 1999 et pour la construction duquel elle avait souscrit le 21 décembre 1998 un contrat d'assurance dit " dommages-ouvrage " avec la société Axa ; que la commune a adressé une déclaration de sinistre à son assureur en septembre 2006 ; que la société Axa a, par un même courrier du 16 novembre 2006, adressé à la commune le rapport de l'expert commis par ses soins et porté à sa connaissance son refus de garantir les désordres constatés ; que le 7 février 2007, la commune a réclamé à la société Axa le versement de la somme de 250 899 euros en réparation des désordres constatés ; qu'elle a ensuite demandé au tribunal administratif de Melun de condamner la société à lui verser cette somme ; que sa demande a été rejetée par un jugement du 3 novembre 2011, confirmé par un arrêt du 20 janvier 2014 de la cour administrative d'appel de Paris contre lequel elle se pourvoit en cassation ;<br/>
<br/>
              2. Considérant qu'aux termes des deux premiers alinéas de l'article L. 242-1 du code des assurances : " Toute personne physique ou morale qui, agissant en qualité de propriétaire de l'ouvrage, de vendeur ou de mandataire du propriétaire de l'ouvrage, fait réaliser des travaux de bâtiment, doit souscrire avant l'ouverture du chantier, pour son compte ou pour celui des propriétaires successifs, une assurance garantissant, en dehors de toute recherche des responsabilités, le paiement de la totalité des travaux de réparation des dommages de la nature de ceux dont sont responsables les constructeurs au sens de l'article 1792-1, les fabricants et importateurs ou le contrôleur technique sur le fondement de l'article 1792 du code civil. / Toutefois, l'obligation prévue au premier alinéa ci-dessus ne s'applique ni aux personnes morales de droit public ni aux personnes morales exerçant une activité dont l'importance dépasse les seuils mentionnés au dernier alinéa de l'article L. 111-6, lorsque ces personnes font réaliser pour leur compte des travaux de bâtiment pour un usage autre que l'habitation " ; qu'aux termes de l'article L. 243-8 du même code, issu de la loi du 4 janvier 1978 : " Tout contrat d'assurance souscrit par une personne assujettie à l'obligation d'assurance en vertu du présent titre est, nonobstant toute clause contraire, réputé comporter des garanties au moins équivalentes à celles figurant dans les clauses types prévues par l'article L. 310-7 du présent code " ; qu'aux termes de l'article L. 111-4 du même code, dans sa rédaction issue de la loi du 4 janvier 1994, reprenant une disposition qui figurait antérieurement à l'article L. 310-7 : " L'autorité administrative peut imposer l'usage de clauses types de contrats " ; qu'aux termes de l'article A. 243-1 du même code : " Tout contrat d'assurance souscrit pour l'application du titre IV du livre II du présent code doit obligatoirement comporter les clauses figurant : (...) / A l'annexe II au présent article, en ce qui concerne l'assurance de dommages. / Toute autre clause du contrat ne peut avoir pour effet d'altérer d'une quelconque manière le contenu ou la portée de ces clauses, sauf si elle s'applique exclusivement à des garanties plus larges que celles prévues par le titre IV du livre II du présent code " ; qu'aux termes de l'annexe II de l'article A. 243-1 du même code, dans sa rédaction applicable au litige porté devant les juges du fond : " ... B. - Obligations de l'assureur en cas de sinistre (...) 2° Rapport préliminaire, mise en jeu des garanties, mesures conservatoires : / a) dans un délai maximum de soixante jours courant à compter de la réception de la déclaration du sinistre réputée constituée, l'assureur, (...) sur le vu du rapport préliminaire établi par l'expert et préalablement communiqué à l'assuré, notifie à celui-ci sa décision quant au principe de la mise en jeu des garanties du contrat " ;<br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la demande indemnitaire présentée par la commune devant le tribunal administratif était fondée sur la circonstance que la société Axa avait méconnu les dispositions combinées des articles L. 242-1 et A. 243-1 du code des assurances en communiquant le rapport préliminaire de l'expert simultanément et non préalablement à sa décision refusant de garantir les désordres constatés ; que, pour rejeter cette demande, le tribunal a estimé que les dispositions invoquées n'étaient pas applicables au litige dès lors que, s'agissant de la construction par une personne morale de droit public d'un immeuble qui était, fût-ce pour partie, à usage d'habitation, l'assurance n'avait pas été souscrite à titre obligatoire ; que la commune ayant soutenu en appel que le contrat contenait une clause prévoyant que la société Axa lui notifierait le rapport d'expertise préalablement à sa décision relative à la prise en charge du dommage, la cour administrative d'appel, par l'arrêt attaqué, a estimé qu'elle n'était pas recevable à invoquer pour la première fois en appel la responsabilité contractuelle de l'assureur, cause juridique distincte de celle sur laquelle elle avait fondé sa demande de première instance ;<br/>
<br/>
              4. Considérant toutefois que, dès lors que les dispositions de l'article A. 243-1 du code des assurances imposent de faire figurer, dans les contrats mentionnés à l'article L. 242-1 du même code, une clause prévoyant la communication préalable du rapport d'expert, l'action intentée par la commune devant le tribunal administratif pouvait viser la réparation des préjudices résultant soit de la violation d'une clause qui figurait dans le contrat en application de ces dispositions, soit de la méconnaissance de garanties que le contrat était réputé selon elle comporter en application des dispositions de l'article L. 243-8 du code ; que, dans les deux cas, la demande tendait à mettre en jeu la responsabilité contractuelle de l'assureur ; qu'ainsi, en retenant que le moyen invoqué devant elle, tiré de l'existence dans le contrat de clauses prévoyant la notification préalable du rapport de l'expert, se rattachait à une cause juridique nouvelle en appel, la cour administrative d'appel de Paris a commis une erreur de droit ; que son arrêt doit, par suite et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, être annulé ;<br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société Axa France Iard la somme de 3 000 euros à verser à la commune de Montereau-Fault-Yonne, au titre des dispositions de l'article L. 761-1 du code de justice administrative ; que ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de la commune de Montereau-Fault-Yonne, qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Paris du 20 janvier 2014 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Paris.<br/>
Article 3 : La société Axa France Iard versera une somme de 3 000 euros à la commune de Montereau-Fault-Yonne au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions présentées par la société Axa France Iard au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées<br/>
Article 5 : La présente décision sera notifiée à commune de Montereau-Fault-Yonne et à la société Axa France Iard.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-08-04-01-01 MARCHÉS ET CONTRATS ADMINISTRATIFS. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. VOIES DE RECOURS. APPEL. MOYENS RECEVABLES EN APPEL. - CONTESTATIONS RELEVANT DE LA MÊME CAUSE JURIDIQUE - ACTION TENDANT À LA RÉPARATION DES PRÉJUDICES RÉSULTANT DE LA VIOLATION D'UNE CLAUSE FIGURANT DANS LE CONTRAT EN VERTU D'UNE OBLIGATION TEXTUELLE ET ACTION TENDANT À LA RÉPARATION DES PRÉJUDICES RÉSULTANT DE LA VIOLATION DE GARANTIES QUE LE CONTRAT EST RÉPUTÉ COMPORTER EN VERTU DE CETTE OBLIGATION.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-08-01-03 PROCÉDURE. VOIES DE RECOURS. APPEL. MOYENS RECEVABLES EN APPEL. - CONTESTATIONS RELEVANT DE LA MÊME CAUSE JURIDIQUE - ACTION TENDANT À LA RÉPARATION DES PRÉJUDICES RÉSULTANT DE LA VIOLATION D'UNE CLAUSE FIGURANT DANS LE CONTRAT EN VERTU D'UNE OBLIGATION TEXTUELLE ET ACTION TENDANT À LA RÉPARATION DES PRÉJUDICES RÉSULTANT DE LA VIOLATION DE GARANTIES QUE LE CONTRAT EST RÉPUTÉ COMPORTER EN VERTU DE CETTE OBLIGATION.
</SCT>
<ANA ID="9A"> 39-08-04-01-01 Lorsqu'une disposition impose de faire figurer certaines clauses dans des contrats, l'action d'une victime tendant à la réparation des préjudices résultant de la violation d'une clause figurant dans le contrat en application de cette disposition et son action tendant à la réparation des préjudices résultant de la méconnaissance de garanties que le contrat était réputé comporter en vertu de cette disposition tendent toutes deux à mettre en jeu la responsabilité contractuelle de son cocontractant. Elles procèdent donc d'une même cause juridique.</ANA>
<ANA ID="9B"> 54-08-01-03 Lorsqu'une disposition impose de faire figurer certaines clauses dans des contrats, l'action d'une victime tendant à la réparation des préjudices résultant de la violation d'une clause figurant dans le contrat en application de cette disposition et son action tendant à la réparation des préjudices résultant de la méconnaissance de garanties que le contrat était réputé comporter en vertu de cette disposition tendent toutes deux à mettre en jeu la responsabilité contractuelle de son cocontractant. Elles procèdent donc d'une même cause juridique.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
