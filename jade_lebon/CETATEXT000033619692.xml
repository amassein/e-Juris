<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033619692</ID>
<ANCIEN_ID>JG_L_2016_12_000000389141</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/61/96/CETATEXT000033619692.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 15/12/2016, 389141</TITRE>
<DATE_DEC>2016-12-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>389141</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOULLOCHE ; SCP ODENT, POULET</AVOCATS>
<RAPPORTEUR>Mme Karin Ciavaldini</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:389141.20161215</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Poitiers d'annuler la décision du 15 octobre 2010 par laquelle le maire de la commune de Saint-Denis d'Oléron (Charente-Maritime) a prononcé la résiliation du contrat de " garantie annuelle d'usage de poste d'amarrage " dont il bénéficiait dans le port de la commune. Par un jugement n° 1003374 du 12 décembre 2012, le tribunal administratif a rejeté cette demande.<br/>
<br/>
              Par un arrêt n° 13BX00454 du 5 février 2015, la cour administrative d'appel de Bordeaux a, sur appel de M.B..., annulé la décision du 15 octobre 2010 et réformé le jugement du tribunal administratif de Poitiers en tant qu'il était contraire à son arrêt.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 1er avril et 23 juin 2015 au secrétariat du contentieux du Conseil d'Etat, la commune de Saint-Denis d'Oléron demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de M.B....<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Karin Ciavaldini, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Odent, Poulet, avocat de la commune de Saint-Denis d'Oléron et à la SCP Boulloche, avocat de M. B....<br/>
<br/>
<br/>1. Il ressort des pièces du dossier soumis aux juges du fond que M. B...a conclu en 1993 avec la commune de Saint-Denis d'Oléron (Charente-Maritime) un contrat de " garantie annuelle d'usage de poste d'amarrage " aux termes duquel il était autorisé à faire stationner le bateau de plaisance dont il était propriétaire dans le port de cette commune à la place n° 16 du ponton I. Par un courrier du 15 octobre 2010, le maire de la commune a signifié à M. B...la résiliation de ce contrat sur le fondement de l'article 49 du règlement du port, en raison de son comportement envers un professionnel travaillant dans le port, cette résiliation prenant effet à la réception du courrier. Par courrier du 22 octobre 2010, M. B... a formé un recours gracieux contre cette mesure, qui a été rejeté par un courrier du maire du 30 octobre 2010. Le 27 décembre 2010, M. B...a saisi le tribunal administratif de Poitiers d'une demande tendant à l'annulation de la décision du 15 octobre 2010. Par un jugement du 12 décembre 2012, le tribunal administratif a rejeté cette demande. La commune de Saint-Denis d'Oléron se pourvoit en cassation contre l'arrêt du 5 février 2015 par lequel la cour administrative d'appel de Bordeaux, faisant droit à l'appel formé par M.B..., a annulé la décision du 15 octobre 2010.<br/>
<br/>
              2. Le juge du contrat, saisi par une partie d'un litige relatif à une mesure d'exécution d'un contrat, peut seulement, en principe, rechercher si cette mesure est intervenue dans des conditions de nature à ouvrir droit à indemnité. Toutefois, une partie à un contrat administratif peut, eu égard à la portée d'une telle mesure d'exécution, former devant le juge du contrat un recours de plein contentieux contestant la validité de la résiliation de ce contrat et tendant à la reprise des relations contractuelles. Elle doit exercer ce recours, y compris si le contrat en cause est relatif à des travaux publics, dans un délai de deux mois à compter de la date à laquelle elle a été informée de la mesure de résiliation. De telles conclusions peuvent être assorties d'une demande tendant, sur le fondement des dispositions de l'article L. 521-1 du code de justice administrative, à la suspension de l'exécution de la résiliation, afin que les relations contractuelles soient provisoirement reprises. <br/>
<br/>
              3. Eu égard aux particularités de ce recours contentieux, à l'étendue des pouvoirs de pleine juridiction dont le juge du contrat dispose et qui peut le conduire, si les conditions en sont satisfaites, à ordonner la reprise des relations contractuelles ainsi qu'à l'intervention du juge des référés pour prendre des mesures provisoires en ce sens, l'exercice d'un recours administratif pour contester cette mesure, s'il est toujours loisible au cocontractant d'y recourir, ne peut avoir pour effet d'interrompre le délai de recours contentieux. Il en va ainsi quel que soit le motif de résiliation du contrat et notamment lorsque cette résiliation est intervenue en raison des fautes commises par le cocontractant.<br/>
<br/>
              4. La demande de M. B...tendant à l'annulation du courrier du 15 octobre 2010 par lequel la commune de Saint-Denis d'Oléron l'informait de la résiliation du contrat dont il bénéficiait devait être regardée comme un recours de plein contentieux contestant la validité de cette résiliation et tendant à la reprise des relations contractuelles. Il ressortait du dossier soumis à la cour que M. B...avait saisi le tribunal administratif le 27 décembre 2010, soit après l'expiration du délai de deux mois à compter de la date à laquelle il avait été informé de la mesure, qui était au plus tard le 22 octobre 2010, date à laquelle il avait formé un recours gracieux. Il résulte de ce qui est dit aux points 2 et 3 que la demande adressée au tribunal administratif était tardive et, par suite, irrecevable. En ne relevant pas d'office cette irrecevabilité, qui ressortait des pièces du dossier qui lui était soumis, et en faisant droit à la demande de M.B..., la cour a entaché son arrêt d'irrégularité. Sans qu'il soit besoin d'examiner les moyens du pourvoi, la commune de Saint-Denis d'Oléron est fondée à demander l'annulation de l'arrêt qu'elle attaque.<br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              6. Pour rejeter la demande de M.B..., le tribunal administratif a jugé que le maire était en situation de compétence liée pour lui notifier la résiliation du contrat en cause qui avait été prononcée par le conseil municipal de la commune dans le cadre de ses compétences. Il en a déduit que les moyens invoqués par M. B...contre la décision lui notifiant cette résiliation étaient inopérants. Il ne ressortait ni des termes de cette décision, ni d'aucune autre pièce du dossier que le maire s'était fondé sur ce qu'il s'estimait en situation de compétence liée pour notifier à M. B...la résiliation du contrat. Dès lors, en se fondant lui-même sur ce motif sans avoir préalablement informé les parties de son intention de le relever d'office, le tribunal a entaché son jugement d'irrégularité. Il y a lieu, par suite, d'annuler ce jugement et de statuer par la voie de l'évocation sur la demande de M. B.... <br/>
<br/>
              7. Ainsi qu'il a été dit au point 4, la demande de M. B...devant le tribunal était irrecevable et ne peut, par suite, qu'être rejetée. <br/>
<br/>
              8. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise, à ce titre, à la charge de la commune de Saint-Denis d'Oléron, qui n'est pas, dans la présente instance, la partie perdante. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de M. B...le versement à la commune de Saint-Denis d'Oléron de la somme de 2 000 euros au titre des frais exposés par cette dernière devant la cour administrative d'appel et le tribunal administratif.<br/>
<br/>
<br/>
<br/>                   D E C I D E :<br/>
                                  --------------<br/>
<br/>
Article 1er : L'arrêt du 5 février 2015 de la cour administrative d'appel de Bordeaux et le jugement du 12 décembre 2012 du tribunal administratif de Poitiers sont annulés.<br/>
<br/>
Article 2 : La demande de M. B...devant le tribunal administratif de Poitiers est rejetée. <br/>
<br/>
Article 3 : Les conclusions présentées par M. B...devant le Conseil d'Etat et la cour administrative d'appel de Bordeaux au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
<br/>
Article 4 : M. B...versera à la commune de Saint-Denis d'Oléron la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 5 : La présente décision sera notifiée à la commune de Saint-Denis d'Oléron et à M. A... B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">37-03-02-01 JURIDICTIONS ADMINISTRATIVES ET JUDICIAIRES. RÈGLES GÉNÉRALES DE PROCÉDURE. INSTRUCTION. CARACTÈRE CONTRADICTOIRE DE LA PROCÉDURE. - JUGE SE FONDANT SUR LA SITUATION DE COMPÉTENCE LIÉE [RJ3] DANS LAQUELLE SE TROUVE L'ADMINISTRATION SANS QUE LA CIRCONSTANCE QUE CETTE DERNIÈRE ESTIMAIT ÊTRE DANS UNE TELLE SITUATION RESSORTE DES MOTIFS DE LA DÉCISION ATTAQUÉE OU DES PIÈCES DU DOSSIER - OBLIGATION DE COMMUNIQUER UN MOP (ART. R. 611-7 DU CJA) - EXISTENCE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">39-04-02 MARCHÉS ET CONTRATS ADMINISTRATIFS. FIN DES CONTRATS. RÉSILIATION. - APPLICATION AUX INSTANCES EN COURS DE LA JURISPRUDENCE SARL PROMOTION DE LA RESTAURATION TOURISTIQUE (PRORESTO) [RJ2] - EXISTENCE (SOL. IMPL.).
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-01-07-04-01 PROCÉDURE. INTRODUCTION DE L'INSTANCE. DÉLAIS. INTERRUPTION ET PROLONGATION DES DÉLAIS. INTERRUPTION PAR UN RECOURS ADMINISTRATIF PRÉALABLE. - APPLICATION AUX INSTANCES EN COURS DE LA JURISPRUDENCE SARL PROMOTION DE LA RESTAURATION TOURISTIQUE (PRORESTO) [RJ2] - EXISTENCE (SOL. IMPL.).
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">54-04-03-02 PROCÉDURE. INSTRUCTION. CARACTÈRE CONTRADICTOIRE DE LA PROCÉDURE. COMMUNICATION DES MOYENS D'ORDRE PUBLIC. - JUGE SE FONDANT SUR LA SITUATION DE COMPÉTENCE LIÉE [RJ3] DANS LAQUELLE SE TROUVE L'ADMINISTRATION SANS QUE LA CIRCONSTANCE QUE CETTE DERNIÈRE ESTIMAIT ÊTRE DANS UNE TELLE SITUATION RESSORTE DES MOTIFS DE LA DÉCISION ATTAQUÉE OU DES PIÈCES DU DOSSIER - OBLIGATION DE COMMUNIQUER UN MOP (ART. R. 611-7 DU CJA) - EXISTENCE [RJ1].
</SCT>
<SCT ID="8E" TYPE="PRINCIPAL">54-07-01-04-01-02 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. QUESTIONS GÉNÉRALES. MOYENS. MOYENS D'ORDRE PUBLIC À SOULEVER D'OFFICE. EXISTENCE. - JUGE SE FONDANT SUR LA SITUATION DE COMPÉTENCE LIÉE [RJ3] DANS LAQUELLE SE TROUVE L'ADMINISTRATION SANS QUE LA CIRCONSTANCE QUE CETTE DERNIÈRE ESTIMAIT ÊTRE DANS UNE TELLE SITUATION RESSORTE DES MOTIFS DE LA DÉCISION ATTAQUÉE OU DES PIÈCES DU DOSSIER - OBLIGATION DE COMMUNIQUER UN MOP (ART. R. 611-7 DU CJA) - EXISTENCE [RJ1].
</SCT>
<ANA ID="9A"> 37-03-02-01 Le juge ne peut se fonder, sans inviter les parties à présenter leurs observations, sur la situation de compétence liée dans laquelle se trouve l'administration s'il ne ressort ni des termes de la décision attaquée, ni des pièces du dossier que l'administration estimait être dans une telle situation.</ANA>
<ANA ID="9B"> 39-04-02 La jurisprudence SARL Promotion de la restauration touristique (PRORESTO), selon laquelle l'exercice d'un recours administratif pour contester la mesure de résiliation d'un contrat, s'il est toujours loisible au cocontractant d'y recourir, ne peut avoir pour effet d'interrompre le délai de recours contentieux, quel que soit le motif de résiliation du contrat et notamment lorsque cette résiliation est intervenue en raison des fautes commises par le cocontractant, s'applique aux instances en cours à la date de cette décision.</ANA>
<ANA ID="9C"> 54-01-07-04-01 La jurisprudence SARL Promotion de la restauration touristique (PRORESTO), selon laquelle l'exercice d'un recours administratif pour contester la mesure de résiliation d'un contrat, s'il est toujours loisible au cocontractant d'y recourir, ne peut avoir pour effet d'interrompre le délai de recours contentieux, quel que soit le motif de résiliation du contrat et notamment lorsque cette résiliation est intervenue en raison des fautes commises par le cocontractant, s'applique aux instances en cours à la date de cette décision.</ANA>
<ANA ID="9D"> 54-04-03-02 Le juge ne peut se fonder, sans inviter les parties à présenter leurs observations, sur la situation de compétence liée dans laquelle se trouve l'administration s'il ne ressort ni des termes de la décision attaquée, ni des pièces du dossier que l'administration estimait être dans une telle situation.</ANA>
<ANA ID="9E"> 54-07-01-04-01-02 Le juge ne peut se fonder, sans inviter les parties à présenter leurs observations, sur la situation de compétence liée dans laquelle se trouve l'administration s'il ne ressort ni des termes de la décision attaquée, ni des pièces du dossier que l'administration estimait être dans une telle situation.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. sol. contr. CE, 22 octobre 2014, Mme Guessas, n° 364000, T. pp. 729-800-817.,,[RJ2] CE, 30 mai 2012, SARL Promotion de la restauration touristique (PRORESTO), n° 357151, p. 237.,,[RJ3] Cf. CE, Section, 3 février 1999, Montaignac, n° 149722, p. 7.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
