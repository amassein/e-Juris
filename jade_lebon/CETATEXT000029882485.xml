<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029882485</ID>
<ANCIEN_ID>JG_L_2014_12_000000362663</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/88/24/CETATEXT000029882485.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème / 5ème SSR, 10/12/2014, 362663</TITRE>
<DATE_DEC>2014-12-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>362663</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème / 5ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FABIANI, LUC-THALER ; SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR>M. Bruno Bachini</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Gaëlle Dumortier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:362663.20141210</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 11 septembre et 10 décembre 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour l'association Service Interentreprises de Santé au Travail (SIST), dont le siège est situé 1 avenue du Forum, à Narbonne (11000), représentée par son président en exercice ; l'association requérante demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 11MA01556 du 10 juillet 2012 par lequel la cour administrative d'appel de Marseille a rejeté sa requête tendant à l'annulation du jugement n° 0903289 du tribunal administratif de Montpellier du 22 février 2011 rejetant sa demande tendant à l'annulation pour excès de pouvoir, d'une part, de la décision de l'inspecteur du travail du 22 décembre 2008 lui refusant l'autorisation de licencier Mme B...A..., d'autre part, de la décision du ministre du travail, des relations sociales, de la famille, de la solidarité et de la ville du 18 juin 2009 rejetant son recours hiérarchique formé contre cette décision de l'inspecteur du travail ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel et d'enjoindre au ministre d'instruire à nouveau sa demande d'autorisation de licenciement dans un délai d'un mois à compter de la notification de l'arrêt à venir, sous peine d'astreinte de 200 euros par jour de retard ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code du travail ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bruno Bachini, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Fabiani, Luc-Thaler, avocat de l'association Service Interentreprises de Santé au Travail (SIST) et à la SCP Spinosi, Sureau, avocat de Mme A...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que, par une décision du 22 décembre 2008, l'inspecteur du travail a refusé d'autoriser l'association Service Interentreprises de Santé au Travail (SIST) à licencier pour faute MmeA..., médecin du travail employée par cette association qui lui reprochait des agissements considérés comme constitutifs de harcèlement moral à l'encontre de trois secrétaires médicales ; que, par une décision du 18 juin 2009, le ministre du travail, des relations sociales, de la famille, de la solidarité et de la ville a confirmé ce refus en rejetant le recours hiérarchique formé par l'association contre la décision de l'inspecteur ; que, par un arrêt du 10 juillet 2012 contre lequel cette association se pourvoit en cassation, la cour administrative d'appel de Marseille a rejeté l'appel formé par cette dernière contre le jugement du tribunal administratif de Montpellier du 22 février 2011 rejetant sa demande tendant à l'annulation pour excès de pouvoir de ces deux décisions ; <br/>
<br/>
              2. Considérant qu'en vertu des dispositions du code du travail, et notamment de ses articles L. 4623-5, R. 4623-20 et R. 4623-21, les médecins du travail bénéficient, dans l'intérêt de l'ensemble des salariés qu'ils suivent, d'une protection exceptionnelle ; que lorsque le licenciement d'un de ces médecins est envisagé, ce licenciement ne doit pas être en rapport avec les fonctions normalement exercées par ce dernier ; que dans le cas où la demande de licenciement est motivée par un comportement fautif, il appartient à l'inspecteur du travail saisi et, le cas échéant, au ministre compétent de rechercher, sous le contrôle du juge de l'excès de pouvoir, si les faits reprochés au médecin du travail sont d'une gravité suffisante pour justifier le licenciement, compte tenu de l'ensemble des règles applicables au contrat de travail de l'intéressé et des exigences propres à l'exécution normale de la mission dont il est investi ;  <br/>
<br/>
              3. Considérant qu'aux termes de l'article L. 1152-1 du code du travail : " Aucun salarié ne doit subir les agissements répétés de harcèlement moral qui ont pour objet ou pour effet une dégradation de ses conditions de travail susceptible de porter atteinte à ses droits et à sa dignité, d'altérer sa santé physique ou mentale ou de compromettre son avenir professionnel " ; qu'aux termes de l'article L. 1152-5 du même code : " Tout salarié ayant procédé à des agissements de harcèlement moral est passible d'une sanction disciplinaire " ; qu'aux termes de l'article L. 1154-1 du même code : " Lorsque survient un litige relatif à l'application des articles L. 1152-1 à L. 1152-3 (...) le salarié établit des faits qui permettent de présumer l'existence d'un harcèlement. / Au vu de ces éléments, il incombe à la partie défenderesse de prouver que ces agissements ne sont pas constitutifs d'un tel harcèlement et que sa décision est justifiée par des éléments objectifs étrangers à tout harcèlement. / Le juge forme sa conviction après avoir ordonné, en cas de besoin, toutes les mesures d'instruction qu'il estime utiles " ;<br/>
<br/>
              4. Considérant, d'une part, qu'il résulte des termes mêmes des dispositions de l'article L. 1154-1 du code du travail mentionnées ci-dessus que le régime particulier de preuve qu'elles prévoient au bénéfice du salarié s'estimant victime de harcèlement moral n'est pas applicable lorsque survient un litige, auquel ce dernier n'est pas partie, opposant un employeur à l'un de ses salariés auquel il est reproché d'être l'auteur de tels faits ; que, dès lors, la cour administrative d'appel de Marseille a méconnu le champ d'application de la loi en se fondant sur ces dispositions pour rechercher si l'intéressée s'était livrée, en l'espèce, à des agissements de harcèlement moral ;<br/>
<br/>
              5. Considérant, d'autre part, qu'il résulte des dispositions mêmes de l'article L. 1152-1 du code du travail mentionnées ci-dessus que le harcèlement moral se caractérise par des agissements répétés ayant pour effet une dégradation des conditions de travail susceptible de porter atteinte aux droits et à la dignité du salarié, d'altérer sa santé ou de compromettre son avenir professionnel ; qu'il s'en déduit que, pour apprécier si des agissements sont constitutifs d'un harcèlement moral, l'inspecteur du travail doit, sous le contrôle du juge administratif, tenir compte des comportements respectifs du salarié auquel il est reproché d'avoir exercé de tels agissements et du salarié susceptible d'en être victime, indépendamment du comportement de l'employeur ; qu'il appartient, en revanche, à l'inspecteur du travail, lorsqu'il estime, par l'appréciation ainsi portée, qu'un comportement de harcèlement moral est caractérisé, de prendre en compte le comportement de l'employeur pour apprécier si la faute résultant d'un tel comportement est d'une gravité suffisante pour justifier un licenciement ; qu'il s'ensuit que la cour administrative d'appel de Marseille a commis une erreur de droit en tenant compte, pour écarter en l'espèce la qualification de harcèlement, du système de management mis en place par l'employeur ainsi que de l'inaction prolongée de ce dernier face aux agissements de sa salariée ; <br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que l'association SIST est fondée à demander l'annulation de l'arrêt attaqué ;<br/>
<br/>
              7. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à l'association requérante au titre des dispositions de l'article L. 761-1 du code de justice administrative ; qu'en revanche, ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de cette dernière, qui n'est pas, dans la présente instance, la partie perdante ; <br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Marseille du 10 juillet 2012 est annulé.<br/>
<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Marseille.<br/>
<br/>
Article 3 : L'Etat versera à l'association Service Interentreprises de Santé au Travail une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
Article 4 : Les conclusions de Mme A...présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
<br/>
Article 5 : La présente décision sera notifiée à l'association Service Interentreprises de Santé au Travail, à Mme B...A...et au ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-04-04 PROCÉDURE. INSTRUCTION. PREUVE. - CONTENTIEUX DU LICENCIEMENT DES SALARIÉS PROTÉGÉS - HARCÈLEMENT MORAL - RÉGIME PARTICULIER DE PREUVE PRÉVU À L'ARTICLE L. 1154-1 DU CODE DU TRAVAIL - CHAMP D'APPLICATION - EXCLUSION - LITIGES ENTRE L'EMPLOYEUR ET L'AUTEUR DE TELS FAITS [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">66-07-01-04-02 TRAVAIL ET EMPLOI. LICENCIEMENTS. AUTORISATION ADMINISTRATIVE - SALARIÉS PROTÉGÉS. CONDITIONS DE FOND DE L'AUTORISATION OU DU REFUS D'AUTORISATION. LICENCIEMENT POUR FAUTE. - HARCÈLEMENT MORAL - NOTION - PRISE EN COMPTE DU COMPORTEMENT DE L'EMPLOYEUR - AU STADE DE LA QUALIFICATION DE HARCÈLEMENT MORAL DES AGISSEMENTS DU SALARIÉ - ABSENCE - AU STADE DE L'APPRÉCIATION DE LA GRAVITÉ DE LA FAUTE - EXISTENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">66-07-01-05 TRAVAIL ET EMPLOI. LICENCIEMENTS. AUTORISATION ADMINISTRATIVE - SALARIÉS PROTÉGÉS. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. - HARCÈLEMENT MORAL - RÉGIME PARTICULIER DE PREUVE PRÉVU À L'ARTICLE L. 1154-1 DU CODE DU TRAVAIL - CHAMP D'APPLICATION - EXCLUSION - LITIGES ENTRE L'EMPLOYEUR ET L'AUTEUR DE TELS FAITS [RJ1].
</SCT>
<ANA ID="9A"> 54-04-04 Il résulte des termes mêmes des dispositions de l'article L. 1154-1 du code du travail que le régime particulier de preuve qu'elles prévoient au bénéfice du salarié s'estimant victime de harcèlement moral n'est pas applicable lorsque survient un litige, auquel ce dernier n'est pas partie, opposant un employeur à l'un de ses salariés auquel il est reproché d'être l'auteur de tels faits.</ANA>
<ANA ID="9B"> 66-07-01-04-02 Il résulte des dispositions de l'article L. 1152-1 du code du travail que le harcèlement moral se caractérise par des agissements répétés ayant pour effet une dégradation des conditions de travail susceptible de porter atteinte aux droits et à la dignité du salarié, d'altérer sa santé ou de compromettre son avenir professionnel. Il s'en déduit que, pour apprécier si des agissements sont constitutifs d'un harcèlement moral, l'inspecteur du travail doit, sous le contrôle du juge administratif, tenir compte des comportements respectifs du salarié auquel il est reproché d'avoir exercé de tels agissements et du salarié susceptible d'en être victime, indépendamment du comportement de l'employeur. Il appartient, en revanche, à l'inspecteur du travail,  lorsqu'il estime, par l'appréciation ainsi portée,  qu'un comportement de harcèlement moral est caractérisé, de prendre en compte le comportement de l'employeur pour apprécier si la faute résultant d'un tel comportement est d'une gravité suffisante pour justifier un licenciement.</ANA>
<ANA ID="9C"> 66-07-01-05 Il résulte des termes mêmes des dispositions de l'article L. 1154-1 du code du travail que le régime particulier de preuve qu'elles prévoient au bénéfice du salarié s'estimant victime de harcèlement moral n'est pas applicable lorsque survient un litige, auquel ce dernier n'est pas partie, opposant un employeur à l'un de ses salariés auquel il est reproché d'être l'auteur de tels faits.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. Cass. Soc., 7 février 2012, société Terreal n° 10-17393, Bull. 2012 V n° 56.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
