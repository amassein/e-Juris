<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026152436</ID>
<ANCIEN_ID>JG_L_2012_07_000000356427</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/15/24/CETATEXT000026152436.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 4ème sous-sections réunies, 06/07/2012, 356427</TITRE>
<DATE_DEC>2012-07-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>356427</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 4ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Jacques Arrighi de Casanova</PRESIDENT>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>Mme Anissia Morel</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:356427.20120706</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 3 et 20 février 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. Clément A, demeurant ... ; M. A demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 1201023/9 du 19 janvier 2012 par laquelle le juge des référés du tribunal administratif de Paris, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, a rejeté sa demande tendant à la suspension de l'exécution de l'arrêté du 24 octobre 2011 du ministre de l'intérieur prononçant sa révocation ; <br/>
<br/>
              2°) statuant en référé, de faire droit à sa demande de suspension ;<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des impôts ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Anissia Morel, Auditeur, <br/>
<br/>
              - les observations de la SCP Piwnica, Molinié avocat de M. A ;<br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Piwnica, Molinié avocat de M. A ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que, par un arrêté du 24 octobre 2011, le ministre de l'intérieur a prononcé la révocation de M. Clément A, gardien de la paix ; que l'intéressé a introduit devant le tribunal administratif de Paris un recours pour excès de pouvoir contre cet arrêté et une demande en référé tendant à ce que son exécution soit suspendue en application de l'article L. 521-1 du code de justice administrative ; qu'il se pourvoit en cassation contre l'ordonnance du 19 janvier 2012 par laquelle le juge des référés de ce tribunal, statuant dans les conditions prévues à l'article L. 522-3 du même code, a rejeté la demande de suspension comme entachée d'une irrecevabilité manifeste, faute pour le requérant de s'être acquitté de la contribution pour l'aide juridique prévue par les dispositions de l'article 1635 bis Q du code général des impôts ; <br/>
<br/>
              2. Considérant, d'une part, que l'article L. 522-3 du code de justice administrative permet au juge des référés de rejeter sans procédure contradictoire et sans tenir d'audience publique, notamment, les demandes manifestement irrecevables ;<br/>
<br/>
              3. Considérant, d'autre part, qu'il résulte de l'article 1635 bis Q du code général des impôts qu'une contribution pour l'aide juridique de 35 euros est due par la partie qui introduit une instance, notamment devant une juridiction administrative, et qu'elle est exigible lors de l'introduction de l'instance ; que, selon l'article R. 411-2 du code de justice administrative, lorsque cette contribution est due et n'a pas été acquittée, la requête est irrecevable, cette irrecevabilité étant toutefois susceptible d'être couverte après l'expiration du délai de recours ; qu'aux termes du second alinéa de l'article R. 411-2-1 du même code : " La contribution n'est due qu'une seule fois lorsqu'un même requérant introduit une demande au fond portant sur les mêmes faits qu'une demande de référé présentée accessoirement et fondée sur le titre III du livre V du présent code ", lequel est relatif au juge des référés ordonnant un constat ou une mesure d'instruction ; qu'il résulte de la combinaison de ces dispositions qu'une demande en référé présentée sur le fondement de l'article L. 521-1 de ce code, en vue de la suspension de l'exécution d'une décision administrative, doit, à peine d'irrecevabilité, donner lieu au paiement de la contribution pour l'aide juridique ; <br/>
<br/>
              4. Considérant que, si l'article R. 612-1 du code de justice administrative prévoit que le juge ne peut relever d'office une irrecevabilité susceptible d'être couverte après l'expiration du délai de recours qu'après avoir procédé à une demande de régularisation, l'article R. 522-2 écarte l'application de ces dispositions devant le juge des référés statuant en urgence ; que, dès lors, contrairement à ce que soutient M. A, le juge des référés du tribunal administratif de Paris n'a pas entaché sa décision d'irrégularité en ne l'invitant pas à acquitter la contribution pour l'aide juridique ; qu'en rejetant sa demande dans les conditions prévues à l'article L. 522-3 du code de justice administrative, au motif qu'elle était entachée d'une irrecevabilité manifeste, il n'a pas commis d'erreur de droit ; <br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que M. A n'est pas fondé à demander l'annulation de l'ordonnance qu'il attaque ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
		Article 1er : Le pourvoi de M. A est rejeté.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. Clément A et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-01-08-05 PROCÉDURE. INTRODUCTION DE L'INSTANCE. FORMES DE LA REQUÊTE. DROIT DE TIMBRE. - CONTRIBUTION POUR L'AIDE JURIDIQUE - 1) OBLIGATION D'ACQUITTER CETTE CONTRIBUTION - DEMANDE DE SUSPENSION - EXISTENCE [RJ1] - 2) CONTRIBUTION NON ACQUITTÉE - CONSÉQUENCE - POSSIBILITÉ POUR LE JUGE DES RÉFÉRÉS STATUANT EN URGENCE DE REJETER LA DEMANDE POUR IRRECEVABILITÉ MANIFESTE SANS INVITATION PRÉALABLE À RÉGULARISATION - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-035-01-02 PROCÉDURE. PROCÉDURES INSTITUÉES PAR LA LOI DU 30 JUIN 2000. QUESTIONS COMMUNES. RECEVABILITÉ. - 1) CONDITION - PAIEMENT DE LA CONTRIBUTION POUR L'AIDE JURIDIQUE [RJ1] - 2) CONTRIBUTION NON ACQUITTÉE - CONSÉQUENCE - POSSIBILITÉ POUR LE JUGE DES RÉFÉRÉS STATUANT EN URGENCE DE REJETER LA DEMANDE POUR IRRECEVABILITÉ MANIFESTE SANS INVITATION PRÉALABLE À RÉGULARISATION - EXISTENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-035-02-02 PROCÉDURE. PROCÉDURES INSTITUÉES PAR LA LOI DU 30 JUIN 2000. RÉFÉRÉ SUSPENSION (ART. L. 521-1 DU CODE DE JUSTICE ADMINISTRATIVE). RECEVABILITÉ. - 1) CONDITION - PAIEMENT DE LA CONTRIBUTION POUR L'AIDE JURIDIQUE [RJ1] - 2) CONTRIBUTION NON ACQUITTÉE - CONSÉQUENCE - POSSIBILITÉ POUR LE JUGE DES RÉFÉRÉS DE REJETER LA DEMANDE DE SUSPENSION POUR IRRECEVABILITÉ MANIFESTE SANS INVITATION PRÉALABLE À RÉGULARISATION - EXISTENCE.
</SCT>
<ANA ID="9A"> 54-01-08-05 1) Il résulte de la combinaison des dispositions de l'article 1635 bis Q du code général des impôts, de l'article R. 411-2 et de l'article R. 411-2-1 du code de justice administrative (CJA) qu'une demande en référé présentée sur le fondement de l'article L. 521-1 de ce code, en vue de la suspension de l'exécution d'une décision administrative, doit, à peine d'irrecevabilité, donner lieu au paiement de la contribution pour l'aide juridique. 2) Si l'article R. 612-1 du CJA prévoit que le juge ne peut relever d'office une irrecevabilité susceptible d'être couverte après l'expiration du délai de recours qu'après avoir procédé à une demande de régularisation, l'article R. 522-2 écarte l'application de ces dispositions devant le juge des référés statuant en urgence. Dès lors, le juge des référés n'entache pas sa décision d'irrégularité en n'invitant pas à acquitter la contribution pour l'aide juridique et ne commet pas d'erreur de droit en rejetant dans les conditions prévues à l'article L. 522-3 du CJA, au motif qu'elle était entachée d'une irrecevabilité manifeste, une demande de suspension faute pour le requérant de s'être acquitté de cette contribution.</ANA>
<ANA ID="9B"> 54-035-01-02 1) Il résulte de la combinaison des dispositions de l'article 1635 bis Q du code général des impôts, de l'article R. 411-2 et de l'article R. 411-2-1 du code de justice administrative (CJA) qu'une demande en référé présentée sur le fondement de l'article L. 521-1 de ce code, en vue de la suspension de l'exécution d'une décision administrative, doit, à peine d'irrecevabilité, donner lieu au paiement de la contribution pour l'aide juridique. 2) Si l'article R. 612-1 du CJA prévoit que le juge ne peut relever d'office une irrecevabilité susceptible d'être couverte après l'expiration du délai de recours qu'après avoir procédé à une demande de régularisation, l'article R. 522-2 écarte l'application de ces dispositions devant le juge des référés statuant en urgence. Dès lors, le juge des référés n'entache pas sa décision d'irrégularité en n'invitant pas à acquitter la contribution pour l'aide juridique et ne commet pas d'erreur de droit en rejetant dans les conditions prévues à l'article L. 522-3 du CJA, au motif qu'elle était entachée d'une irrecevabilité manifeste, une demande de suspension faute pour le requérant de s'être acquitté de cette contribution.</ANA>
<ANA ID="9C"> 54-035-02-02 1) Il résulte de la combinaison des dispositions de l'article 1635 bis Q du code général des impôts, de l'article R. 411-2 et de l'article R. 411-2-1 du code de justice administrative (CJA) qu'une demande en référé présentée sur le fondement de l'article L. 521-1 de ce code, en vue de la suspension de l'exécution d'une décision administrative, doit, à peine d'irrecevabilité, donner lieu au paiement de la contribution pour l'aide juridique. 2) Si l'article R. 612-1 du CJA prévoit que le juge ne peut relever d'office une irrecevabilité susceptible d'être couverte après l'expiration du délai de recours qu'après avoir procédé à une demande de régularisation, l'article R. 522-2 écarte l'application de ces dispositions devant le juge des référés statuant en urgence. Dès lors, le juge des référés n'entache pas sa décision d'irrégularité en n'invitant pas à acquitter la contribution pour l'aide juridique et ne commet pas d'erreur de droit en rejetant dans les conditions prévues à l'article L. 522-3 du CJA, au motif qu'elle était entachée d'une irrecevabilité manifeste, une demande de suspension faute pour le requérant de s'être acquitté de cette contribution.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, juge des référés, 16 novembre 2011, Union des syndicats de l'immobilier (UNIS), n° 353341, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
