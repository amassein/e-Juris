<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027198411</ID>
<ANCIEN_ID>JG_L_2013_03_000000347558</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/19/84/CETATEXT000027198411.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 1ère sous-sections réunies, 20/03/2013, 347558</TITRE>
<DATE_DEC>2013-03-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>347558</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 1ère sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Didier Ribes</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:347558.20130320</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 17 mars 2011 au secrétariat du contentieux du Conseil d'État, présenté par le Parquet général près la Cour des comptes, qui demande au Conseil d'État :<br/>
<br/>
              1°) d'annuler l'arrêt n° 59746 du 26 janvier 2011 par lequel la Cour des comptes, statuant sur un réquisitoire du ministère public, a déchargé l'agent comptable du groupement d'intérêt public " Transport sanitaire par hélicoptère en Île-de-France (TSH IF) " de sa gestion pour l'exercice 2004 ;<br/>
<br/>
              2°) de renvoyer l'affaire devant la Cour des comptes ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code des juridictions financières ;<br/>
<br/>
              Vu la loi n° 63-156 du 23 février 1963 ;<br/>
<br/>
              Vu le décret n° 62-1587 du 29 décembre 1962 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Didier Ribes, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes du I de l'article 60 de la loi de finances du 23 février 1963, dans sa rédaction alors applicable : " Quel que soit le lieu où ils exercent leurs fonctions, les comptables publics sont personnellement et pécuniairement responsables du recouvrement des recettes, du paiement des dépenses, de la garde et de la conservation des fonds et valeurs appartenant ou confiés à l'État, aux collectivités locales et aux établissements publics nationaux ou locaux, du maniement des fonds et des mouvements de comptes de disponibilités, de la conservation des pièces justificatives des opérations et documents de comptabilité ainsi que de la tenue de la comptabilité du poste comptable qu'ils dirigent. / Les comptables publics sont personnellement et pécuniairement responsables des contrôles qu'ils sont tenus d'assurer en matière de recettes, de dépenses et de patrimoine dans les conditions prévues par le règlement général sur la comptabilité publique. (...) " ; que le IV du même article précise que : " La responsabilité pécuniaire prévue ci-dessus se trouve engagée dès lors qu'un déficit ou un manquant en deniers ou en valeurs a été constaté, qu'une recette n'a pas été recouvrée, qu'une dépense a été irrégulièrement payée ou que, par la faute du comptable public, l'organisme public a dû procéder à l'indemnisation d'un autre organisme public ou d'un tiers. " ; que l'article 12 du décret du 29 décembre 1962 portant règlement général sur la comptabilité publique, alors en vigueur, dispose que : " Les comptables sont tenus d'exercer : (...) B. - En matière de dépenses, le contrôle : (...) de la disponibilité des crédits ; (...) " ;<br/>
<br/>
              2. Considérant qu'il appartient au juge des comptes de vérifier que le comptable public s'est livré aux différents contrôles qu'il doit réaliser notamment pour assurer le paiement régulier des dépenses ; que dans l'exercice de cette fonction juridictionnelle, il doit s'abstenir de toute appréciation du comportement personnel du comptable intéressé et ne peut fonder ses décisions que sur les éléments matériels des comptes et les règles applicables ; qu'il résulte des dispositions citées ci-dessus qu'aucune circonstance, tenant notamment au fonctionnement du service public, n'est de nature à justifier, en l'absence de disposition spécifique permettant le mandatement ou le paiement de dépenses avant le vote du budget primitif, le paiement par un comptable d'une dépense malgré l'absence de budget exécutoire et, par suite, à faire obstacle à l'engagement de sa responsabilité personnelle et pécuniaire en raison de ce paiement irrégulier ;<br/>
<br/>
              3. Considérant que le Procureur général près la Cour des comptes a saisi cette dernière d'une présomption de charge à l'encontre de M. B...A..., agent comptable du groupement d'intérêt public " Transport sanitaire par hélicoptère en Île-de-France (TSH IF) " en 2004, pour avoir payé le 4 mars 2004 une somme de 10 295 euros à la société SAS Hélicap en règlement d'une facture relative à des transports aériens de personnes au cours de la période du 1er au 31 janvier 2004, alors que le budget du groupement d'intérêt public pour cette même année n'avait pas encore été voté ; que par l'arrêt attaqué du 26 janvier 2011, la Cour des comptes a déchargé M. A...de sa gestion pour l'année 2004 ;<br/>
<br/>
              4. Considérant qu'il ressort des énonciations de l'arrêt attaqué que M. A...a, en l'absence de budget exécutoire, effectué un paiement en application de la procédure dite des douzièmes provisoires prévue par le guide méthodologique des groupements d'intérêt public élaboré par la direction générale de la comptabilité publique ; que l'arrêt relève que si aucun texte de portée normative n'autorisait pour les groupements d'intérêt public le paiement de dépenses indispensables à leur fonctionnement en l'absence d'adoption du budget avant le commencement de l'exercice, M. A...a pris la responsabilité de procéder au paiement litigieux, après s'être assuré que l'organisme disposait de la trésorerie nécessaire, dans le seul but d'assurer la continuité d'un service d'urgence de transport héliporté de malades, dès lors qu'un non-paiement découlant du retard d'adoption du budget aurait eu pour conséquence l'interruption de ce service ; qu'en refusant de prononcer un débet au motif que la suspension du paiement par le comptable aurait conduit à l'interruption du service, alors qu'aucune circonstance ne pouvait justifier un tel paiement en l'absence de disposition le permettant, la Cour a commis une erreur de droit ;<br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que le Parquet général près la Cour des comptes est fondé à demander l'annulation de l'arrêt attaqué ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la Cour des comptes du 26 janvier 2011 est annulé.<br/>
<br/>
      Article 2 : L'affaire est renvoyée à la Cour des comptes.<br/>
<br/>
Article 3 : La présente décision sera notifiée au Parquet général près la Cour des comptes et à M. B... A.... <br/>
Copie en sera adressée au groupement d'intérêt public " Transport sanitaire par hélicoptère en Île-de-France (TSH IF) " et au ministre de l'économie et des finances.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">18-01-04 COMPTABILITÉ PUBLIQUE ET BUDGET. RÉGIME JURIDIQUE DES ORDONNATEURS ET DES COMPTABLES. JUGEMENT DES COMPTES. - OFFICE DU JUGE DES COMPTES - 1) PRINCIPE - PRISE EN COMPTE DES SEULS ÉLÉMENTS MATÉRIELS DES COMPTES ET DES RÈGLES APPLICABLES - APPRÉCIATION DU COMPORTEMENT DU COMPTABLE - ABSENCE - CONSÉQUENCE - PAIEMENT PAR UN COMPTABLE D'UNE DÉPENSE MALGRÉ L'ABSENCE DE BUDGET EXÉCUTOIRE EN L'ABSENCE DE DISPOSITION SPÉCIFIQUE PERMETTANT LE MANDATEMENT OU LE PAIEMENT DE DÉPENSES AVANT LE VOTE DU BUDGET PRIMITIF - IRRÉGULARITÉ DE NATURE À ENGAGER LA RESPONSABILITÉ DU COMPTABLE - EXISTENCE - FONCTIONNEMENT DU SERVICE PUBLIC - MOTIF DE NATURE À Y FAIRE OBSTACLE - ABSENCE - 2) ESPÈCE - COMPTABLE D'UN GIP AYANT, EN L'ABSENCE DE BUDGET EXÉCUTOIRE, EFFECTUÉ UN PAIEMENT DANS LE BUT D'ASSURER LA CONTINUITÉ D'UN SERVICE D'URGENCE DE TRANSPORT HÉLIPORTÉ DE MALADES, ALORS QU'AUCUNE DISPOSITION APPLICABLE AUX GIP NE PERMETTAIT LE PAIEMENT INDISPENSABLE À LEUR FONCTIONNEMENT DANS UN TEL CAS - MISE EN DÉBET.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">33-03 ÉTABLISSEMENTS PUBLICS ET GROUPEMENTS D'INTÉRÊT PUBLIC. GROUPEMENTS D'INTÉRÊT PUBLIC. - COMPTABILITÉ DES GIP - COMPTABLE D'UN GIP AYANT, EN L'ABSENCE DE BUDGET EXÉCUTOIRE, EFFECTUÉ UN PAIEMENT DANS LE BUT D'ASSURER LA CONTINUITÉ D'UN SERVICE D'URGENCE DE TRANSPORT HÉLIPORTÉ DE MALADES, ALORS QU'AUCUNE DISPOSITION APPLICABLE AUX GIP NE PERMETTAIT LE PAIEMENT INDISPENSABLE À LEUR FONCTIONNEMENT DANS UN TEL CAS - MISE EN DÉBET.
</SCT>
<ANA ID="9A"> 18-01-04 1) Il appartient au juge des comptes de vérifier que le comptable public s'est livré aux différents contrôles qu'il doit réaliser notamment pour assurer le paiement régulier des dépenses. Dans l'exercice de cette fonction juridictionnelle, il doit s'abstenir de toute appréciation du comportement personnel du comptable intéressé et ne peut fonder ses décisions que sur les éléments matériels des comptes et les règles applicables. Il résulte des dispositions du I et du IV de l'article 60 de la loi n° 63-156 de finances du 23 février 1963 et de l'article 12 du décret n° 62-1587 du 29 décembre 1962 portant règlement général sur la comptabilité publique, qu'aucune circonstance, tenant notamment au fonctionnement du service public, n'est de nature à justifier, en l'absence de disposition spécifique permettant le mandatement ou le paiement de dépenses avant le vote du budget primitif, le paiement par un comptable d'une dépense malgré l'absence de budget exécutoire et, par suite, à faire obstacle à l'engagement de sa responsabilité personnelle et pécuniaire en raison de ce paiement irrégulier.,,,2) En l'espèce, erreur de droit de la Cour des comptes à avoir refusé de prononcer un débet à l'encontre du comptable d'un groupement d'intérêt public (GIP) ayant, en l'absence de budget exécutoire, effectué un paiement en application de la procédure dite des douzièmes provisoires prévue par le guide méthodologique des GIP élaboré par la direction générale de la comptabilité publique, dans le but d'assurer la continuité d'un service d'urgence de transport héliporté de malades, alors qu'aucun texte de portée normative n'autorisait pour les GIP le paiement de dépenses indispensables à leur fonctionnement en l'absence d'adoption du budget avant le commencement de l'exercice.</ANA>
<ANA ID="9B"> 33-03 Le juge des comptes commet une erreur de droit en refusant de prononcer un débet à l'encontre du comptable d'un groupement d'intérêt public (GIP) ayant, en l'absence de budget exécutoire, effectué un paiement en application de la procédure dite des douzièmes provisoires prévue par le guide méthodologique des GIP élaboré par la direction générale de la comptabilité publique, dans le but d'assurer la continuité d'un service d'urgence de transport héliporté de malades, alors qu'aucun texte de portée normative n'autorisait pour les GIP le paiement de dépenses indispensables à leur fonctionnement en l'absence d'adoption du budget avant le commencement de l'exercice.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
