<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043100593</ID>
<ANCIEN_ID>JG_L_2021_02_000000436759</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/10/05/CETATEXT000043100593.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 05/02/2021, 436759</TITRE>
<DATE_DEC>2021-02-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>436759</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER ; SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>Mme Myriam Benlolo Carabot</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Domingo</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:436759.20210205</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
       M. A... C... a demandé à la Cour nationale du droit d'asile d'annuler la décision du 13 novembre 2018 par laquelle l'Office français de protection des réfugiés et apatrides (OFPRA) a déclaré irrecevable sa demande d'asile. <br/>
<br/>
       Par une décision n° 19003499 du 30 septembre 2019, la Cour nationale du droit d'asile a annulé cette décision et renvoyé la demande d'asile de M. C... à l'OFPRA. <br/>
<br/>
       Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 16 décembre 2019 et 16 mars 2020 au secrétariat du contentieux du Conseil d'Etat, l'OFPRA demande au Conseil d'Etat :<br/>
<br/>
       1°) d'annuler cette décision ;<br/>
<br/>
       2°) de renvoyer l'affaire devant la Cour nationale du droit d'asile. <br/>
<br/>
<br/>
<br/>
       Vu les autres pièces du dossier ;<br/>
<br/>
       Vu : <br/>
       - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
       - le code de justice administrative et le décret n° 2020-1406 du décret du 18 novembre 2020 ;<br/>
<br/>
<br/>
<br/>
       Après avoir entendu en séance publique :<br/>
<br/>
       - le rapport de Mme B... D..., maître des requêtes en service extraordinaire,  <br/>
<br/>
       - les conclusions de M. Laurent Domingo, rapporteur public ;<br/>
<br/>
       La parole ayant été donnée, avant et après les conclusions, à la SCP Foussard, Froger, avocat de l'OFPRA et à la SCP Rocheteau, Uzan-Sarano, avocat de M. A... C... ;<br/>
<br/>
<br/>
<br/>
       Considérant ce qui suit : <br/>
<br/>
       1. Il ressort des pièces du dossier soumis au juge du fond que par une décision du 13 novembre 2018, l'Office français de protection des réfugiés et apatrides (OFPRA) a déclaré irrecevable la demande d'asile de Monsieur A... C... sur le fondement de l'article L. 723-11 du code de l'entrée et du séjour des étrangers et du droit d'asile, au motif que le demandeur bénéficie d'une protection effective au titre de l'asile dans un Etat membre de l'Union européenne. L'OFPRA se pourvoit en cassation contre la décision du 30 septembre 2019 par laquelle la Cour nationale du droit d'asile a annulé cette décision et lui a renvoyé l'examen de la demande d'asile de M. C....<br/>
<br/>
       2. Aux termes de l'article L. 723-11 du code de l'entrée et du séjour des étrangers et du droit d'asile : " L''office peut prendre une décision d'irrecevabilité écrite et motivée, sans vérifier si les conditions d'octroi de l'asile sont réunies, dans les cas suivants :1° Lorsque le demandeur bénéficie d'une protection effective au titre de l'asile dans un Etat membre de l'Union européenne ;/2° Lorsque le demandeur bénéficie du statut de réfugié et d'une protection effective dans un Etat tiers et y est effectivement réadmissible (...) ".<br/>
<br/>
       3. Le recours contre une décision de l'OFPRA devant la Cour nationale du droit d'asile, pour être recevable, doit être rédigé en langue française et les pièces et documents produits au cours de l'instruction doivent également être rédigés en cette langue ou accompagnés de leur traduction. Si la Cour nationale du droit d'asile n'entache pas, ainsi, sa décision d'irrégularité en refusant de tenir compte de pièces ou documents qui ne seraient pas rédigés en langue française ni accompagnés de leur traduction, aucun texte ni aucune règle de procédure n'interdit à la Cour nationale du droit d'asile d'en tenir compte dès lors que l'utilisation de tels documents ou éléments ne fait pas obstacle à l'exercice par le juge de cassation du contrôle qui lui incombe. Elle peut, dans les mêmes conditions, tenir compte d'éléments d'information générale librement accessibles au public qui ne seraient pas rédigés en langue française. Elle n'est toutefois pas tenue de faire usage de son pouvoir d'instruction pour demander aux parties de produire la traduction d'une pièce ou document rédigé en langue étrangère.<br/>
       4. S'agissant cependant de documents administratifs présentés comme émanant d'une autorité d'un Etat membre de l'Union européenne et dont l'OFPRA doit tenir compte pour fonder sa décision, en application notamment des dispositions précitées de l'article L. 723-11 du code de l'entrée et du séjour des étrangers et du droit d'asile, il appartient à la Cour nationale du droit d'asile, si elle estime ne pouvoir prendre en considération la version originale de ce document rédigé en langue étrangère, de faire usage de ses pouvoirs d'instruction pour en demander une traduction.  <br/>
       5. Il ressort des pièces soumis au juge du fond que l'OFPRA, pour justifier sa décision prise sur le fondement de l'article L. 723-11 précité, a produit devant la Cour nationale du droit d'asile deux documents établissant, selon ses affirmations, que l'intéressé avait obtenu la protection subsidiaire en Italie, dont l'un émanant des autorités italiennes, rédigé en anglais. En jugeant que ce document, dont elle avait constaté le caractère officiel en relevant qu'il émanait d'une autorité d'un Etat membre de l'Union européenne et dont elle avait ainsi nécessairement admis qu'il était susceptible de revêtir un caractère probant pour établir la réalité de la protection subsidiaire obtenue en Italie par M. C..., ne pouvait à lui seul permettre de corroborer les affirmations de l'Office, au seul motif qu'il n'était pas traduit en langue française et ne pouvait en conséquence être utilement exploité, alors que dans de telles circonstances, elle ne pouvait refuser d'en tenir compte sans faire usage de ses pouvoirs d'instruction pour demander une traduction à verser au dossier, la Cour nationale du droit d'asile s'est méprise sur son office et a commis une erreur de droit. Il y a lieu dès lors, et sans qu'il soit besoin de statuer sur les autres moyens du pourvoi, d'annuler sa décision.<br/>
       6. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'il soit fait droit aux conclusions présentées à ce titre par M. C....<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
       --------------<br/>
<br/>
Article 1er : La décision de la Cour nationale du droit d'asile en date du 30 septembre 2019 est annulée.<br/>
Article 2 : L'affaire est renvoyée devant la Cour nationale du droit d'asile.<br/>
Article 3 : Les conclusions de M. C... présentes au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à l'Office français de protection des réfugiés et apatrides et à M. A... C.... <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">095-08-01-06-01-01 - RECOURS CONTRE UNE DÉCISION DE L'OFPRA [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">095-08-02-01 - RÉDACTION EN LANGUE FRANÇAISE DES PIÈCES SOUMISES AU JUGE - 1) PRINCIPE - A) EXIGENCE [RJ1] - B) PORTÉE DES PIÈCES EN LANGUE ÉTRANGÈRE - I) OBLIGATION D'EN TENIR COMPTE - ABSENCE [RJ2] - II) POSSIBILITÉ D'EN TENIR COMPTE - EXISTENCE [RJ3] - C) PORTÉE DES ÉLÉMENTS D'INFORMATION GÉNÉRALE LIBREMENT ACCESSIBLES AU PUBLIC ET RÉDIGÉS EN LANGUE ÉTRANGÈRE - POSSIBILITÉ D'EN TENIR COMPTE - EXISTENCE [RJ4] - D) OBLIGATION DE DEMANDER UNE TRADUCTION - ABSENCE [RJ5] - 2) EXCEPTION - DOCUMENT ADMINISTRATIF PRÉSENTÉ COMME ÉMANANT D'UNE AUTORITÉ D'UN ETAT MEMBRE DE L'UE - RÉDACTION EN LANGUE ÉTRANGÈRE FAISANT MATÉRIELLEMENT OBSTACLE À LEUR PRISE EN CONSIDÉRATION - A) OBLIGATION DE RÉCLAMER UNE TRADUCTION - EXISTENCE - B) POSSIBILITÉ DE NE PAS EN TENIR COMPTE - ABSENCE.
</SCT>
<ANA ID="9A"> 095-08-01-06-01-01 Le recours contre une décision de l'Office français de protection des réfugiés et apatrides (OFPRA) devant la Cour nationale du droit d'asile (CNDA), pour être recevable, doit être rédigé en langue française.</ANA>
<ANA ID="9B"> 095-08-02-01 1) a) Le recours contre une décision de l'Office français de protection des réfugiés et apatrides (OFPRA) devant la Cour nationale du droit d'asile (CNDA), pour être recevable, doit être rédigé en langue française et les pièces et documents produits au cours de l'instruction doivent également être rédigés en cette langue ou accompagnés de leur traduction.,,,b) i) La CNDA n'entache pas, ainsi, sa décision d'irrégularité en refusant de tenir compte de pièces ou documents qui ne seraient pas rédigés en langue française ni accompagnés de leur traduction.... ,,ii) Aucun texte ni aucune règle de procédure n'interdit à la CNDA de tenir compte de pièces ou documents qui ne seraient pas rédigés en langue française ni accompagnés de leur traduction dès lors que l'utilisation de tels documents ou éléments ne fait pas obstacle à l'exercice par le juge de cassation du contrôle qui lui incombe.,,,c) Elle peut, dans les mêmes conditions, tenir compte d'éléments d'information générale librement accessibles au public qui ne seraient pas rédigés en langue française.,,,d) Elle n'est toutefois pas tenue de faire usage de son pouvoir d'instruction pour demander aux parties de produire la traduction d'une pièce ou document rédigé en langue étrangère.,,,2) a) S'agissant cependant de documents administratifs présentés comme émanant d'une autorité d'un Etat membre de l'Union européenne (UE) et dont l'OFPRA doit tenir compte pour fonder sa décision, en application notamment de l'article L. 723-11 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA), il appartient à la CNDA, si elle estime ne pouvoir prendre en considération la version originale de ce document rédigé en langue étrangère, de faire usage de ses pouvoirs d'instruction pour en demander une traduction.... ,,b) Elle ne peut refuser d'en tenir compte sans faire usage de ses pouvoirs d'instruction pour demander une traduction à verser au dossier.,,,Un document, dont la CNDA a constaté le caractère officiel en relevant qu'il émanait d'une autorité d'un Etat membre de l'UE et dont elle a ainsi nécessairement admis qu'il était susceptible de revêtir un caractère probant pour établir la réalité de la protection subsidiaire obtenue dans cet Etat membre par un demandeur d'asile, ne saurait être écarté au seul motif qu'il n'était pas traduit en langue française et ne pouvait en conséquence être utilement exploité.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., sur l'exigence de rédaction des requêtes en langue française, CE, Section, 22 novembre 1985,,, n° 65105, p. 333.,[RJ2] Cf. CE, 24 octobre 1984,,, n° 50581, p. 335.,,[RJ3] Cf. CE, Section, 15 décembre 2000, SA Polyclad Europe, n° 194696, p. 622 ; CE, 17 décembre 2010,,, n° 306174, T. pp. 638-736-752-833-893.,,[RJ4] Cf. CE, 30 décembre 2014, M.,, n° 371502, T. pp. 525-526.,,[RJ5] Cf. CE, 27 février 1987,,, n° 62851, T. p. 734 ; CE, 22 mars 1989, Mlle,, n° 83959, p. 99.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
