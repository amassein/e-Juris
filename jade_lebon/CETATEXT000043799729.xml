<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043799729</ID>
<ANCIEN_ID>JG_L_2021_07_000000435621</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/79/97/CETATEXT000043799729.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 6ème chambres réunies, 16/07/2021, 435621</TITRE>
<DATE_DEC>2021-07-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>435621</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SEVAUX, MATHONNET</AVOCATS>
<RAPPORTEUR>Mme Pearl Nguyên Duy</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:435621.20210716</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Mme A... B... a demandé à la commission du contentieux du stationnement payant de la décharger du montant de 35 euros mis à sa charge par l'avis de paiement du forfait de post-stationnement émis le 25 janvier 2018 par la commune de Strasbourg. Par une décision n° 18003106 du 13 août 2019, le magistrat désigné par le président de la commission a prononcé la décharge sollicitée.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 28 octobre 2019 et 28 janvier 2020 au secrétariat du contentieux du Conseil d'Etat, la commune de Strasbourg demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette décision ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de Mme B... ;<br/>
<br/>
              3°) de mettre à la charge de Mme B... la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des collectivités territoriales ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Pearl Nguyên Duy, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Sevaux, Mathonnet, avocat de la commune de Strasbourg ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que la commune de Strasbourg a mis à la charge de Mme B... le paiement d'un forfait de post-stationnement de 35 euros, au motif que cette dernière aurait omis de payer une redevance due pour le stationnement de son véhicule. La commune se pourvoit en cassation contre la décision du 13 août 2019 par laquelle la commission du contentieux du stationnement payant a déchargé Mme B... de cette somme.<br/>
<br/>
              2. Aux termes du I de l'article L. 2333-87 du code général des collectivités territoriales : " I.- (...) le conseil municipal ou l'organe délibérant de l'établissement public de coopération intercommunale (...), peut instituer une redevance de stationnement, compatible avec les dispositions du plan de déplacements urbains, s'il existe. / La délibération institutive établit : / 1° Le barème tarifaire de paiement immédiat de la redevance, applicable lorsque la redevance correspondant à la totalité de la période de stationnement est réglée par le conducteur du véhicule dès le début du stationnement ; / 2° Le tarif du forfait de post-stationnement, applicable lorsque la redevance correspondant à la totalité de la période de stationnement n'est pas réglée dès le début du stationnement ou est insuffisamment réglée (...) ". L'article R. 2333-120-3 du même code dispose que : " Le paiement immédiat de la redevance de stationnement donne lieu à la délivrance d'un justificatif imprimé ou transmis par voie dématérialisée. Ce justificatif comporte les informations suivantes : / a) La date et l'heure d'impression ou de transmission du justificatif ; / b) La date et l'heure de fin de la période du stationnement payé immédiatement ;/ c) Le montant de la redevance de stationnement payé ;/ d) Le barème tarifaire appliqué dans la zone de stationnement ; / e) Le rappel de la règle : "Le forfait est dû en cas de paiement insuffisant" ; / f) Lorsque le justificatif est délivré sous forme d'un imprimé, la prescription suivante : "A placer à l'avant du véhicule, bien lisible de l'extérieur" ". <br/>
<br/>
              3. Il résulte des dispositions ci-dessus que le conducteur qui procède au paiement immédiat de la redevance de stationnement se voit remettre un justificatif, imprimé ou transmis par voie électronique, qui permet d'établir qu'il s'est acquitté de la redevance et comporte à cette fin plusieurs informations introduites par lui. Si ce conducteur se voit néanmoins mettre à sa charge le paiement d'un forfait de post-stationnement, il peut ainsi, pour en obtenir la décharge par l'exercice d'un recours administratif ou, le cas échéant, d'un recours contentieux devant la commission du contentieux du stationnement payant, établir par la production de ce justificatif qu'il a procédé au paiement immédiat de la redevance de stationnement. Il lui est également loisible d'apporter cette preuve du paiement immédiat de sa redevance par tout moyen, en particulier lorsque le justificatif remis au moment du paiement immédiat de la redevance comporte, en raison d'une erreur commise par lui, des renseignements incomplets ou inexacts. Dans ce dernier cas, il est également loisible à la commune d'apporter, le cas échéant, des éléments susceptibles d'établir que le caractère incomplet ou inexact de ces renseignements résulte d'une fraude du conducteur. <br/>
<br/>
              4. Par suite en jugeant, pour décharger Mme B... de l'obligation de payer le forfait de post-stationnement mis à sa charge par la commune de Strasbourg, que, bien qu'elle n'ait pas saisi correctement le numéro de la plaque d'immatriculation de son véhicule dans le dispositif permettant l'émission de son justificatif de stationnement, elle apportait néanmoins la preuve qu'elle s'était acquittée de la redevance due pour le stationnement de son véhicule, et alors que la commune n'avait ni établi ni même allégué que cette erreur résulterait d'une fraude, la commission du contentieux du stationnement payant n'a pas commis d'erreur de droit. <br/>
<br/>
              5. Il résulte de tout ce qui précède que la commune de Strasbourg n'est pas fondée à demander l'annulation de la décision qu'elle attaque. Son pourvoi doit, par suite, être rejeté, y compris par voie de conséquence ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				 --------------<br/>
<br/>
		Article 1er : Le pourvoi de la commune de Strasbourg est rejeté.<br/>
<br/>
Article 2 : La présente décision sera notifiée à la commune de Strasbourg et à Mme A... B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">135-02-03-02-04-02-03 COLLECTIVITÉS TERRITORIALES. COMMUNE. ATTRIBUTIONS. POLICE. POLICE DE LA CIRCULATION ET DU STATIONNEMENT. RÉGLEMENTATION DU STATIONNEMENT. STATIONNEMENT PAYANT. - PAIEMENT IMMÉDIAT DE LA REDEVANCE DE STATIONNEMENT (ART. R. 2333-120-3 DU CGCT) - PREUVE PAR TOUT MOYEN - CAS OÙ LE JUSTIFICATIF COMPORTE DES RENSEIGNEMENTS INCOMPLETS OU INEXACTS.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">49-04-01-02-03 POLICE. POLICE GÉNÉRALE. CIRCULATION ET STATIONNEMENT. RÉGLEMENTATION DU STATIONNEMENT. STATIONNEMENT PAYANT. - PAIEMENT IMMÉDIAT DE LA REDEVANCE DE STATIONNEMENT (ART. R. 2333-120-3 DU CGCT) - PREUVE PAR TOUT MOYEN - CAS OÙ LE JUSTIFICATIF COMPORTE DES RENSEIGNEMENTS INCOMPLETS OU INEXACTS.
</SCT>
<ANA ID="9A"> 135-02-03-02-04-02-03 Il résulte du I de l'article L. 2333-87 et de l'article R. 2333-120-3 du code général des collectivités territoriales (CGCT) que le conducteur qui procède au paiement immédiat de la redevance de stationnement se voit remettre un justificatif, imprimé ou transmis par voie électronique, qui permet d'établir qu'il s'est acquitté de la redevance et comporte à cette fin plusieurs informations introduites par lui. Si ce conducteur se voit néanmoins mettre à sa charge le paiement d'un forfait de post-stationnement, il peut ainsi, pour en obtenir la décharge par l'exercice d'un recours administratif ou, le cas échéant, d'un recours contentieux devant la commission du contentieux du stationnement payant (CCSP), établir par la production de ce justificatif qu'il a procédé au paiement immédiat de la redevance de stationnement.... ,,Il lui est également loisible d'apporter cette preuve du paiement immédiat de sa redevance par tout moyen, en particulier lorsque le justificatif remis au moment du paiement immédiat de la redevance comporte, en raison d'une erreur commise par lui, des renseignements incomplets ou inexacts. Dans ce dernier cas, il est également loisible à la commune de se forger sa conviction au vu de l'ensemble des éléments dont elle dispose, notamment s'ils sont susceptibles d'établir que le caractère incomplet ou inexact de ces renseignements résulte d'une fraude du conducteur.</ANA>
<ANA ID="9B"> 49-04-01-02-03 Il résulte du I de l'article L. 2333-87 et de l'article R. 2333-120-3 du code général des collectivités territoriales (CGCT) que le conducteur qui procède au paiement immédiat de la redevance de stationnement se voit remettre un justificatif, imprimé ou transmis par voie électronique, qui permet d'établir qu'il s'est acquitté de la redevance et comporte à cette fin plusieurs informations introduites par lui. Si ce conducteur se voit néanmoins mettre à sa charge le paiement d'un forfait de post-stationnement, il peut ainsi, pour en obtenir la décharge par l'exercice d'un recours administratif ou, le cas échéant, d'un recours contentieux devant la commission du contentieux du stationnement payant (CCSP), établir par la production de ce justificatif qu'il a procédé au paiement immédiat de la redevance de stationnement.... ,,Il lui est également loisible d'apporter cette preuve du paiement immédiat de sa redevance par tout moyen, en particulier lorsque le justificatif remis au moment du paiement immédiat de la redevance comporte, en raison d'une erreur commise par lui, des renseignements incomplets ou inexacts. Dans ce dernier cas, il est également loisible à la commune de se forger sa conviction au vu de l'ensemble des éléments dont elle dispose, notamment s'ils sont susceptibles d'établir que le caractère incomplet ou inexact de ces renseignements résulte d'une fraude du conducteur.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
