<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041663058</ID>
<ANCIEN_ID>JG_L_2020_02_000000428422</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/66/30/CETATEXT000041663058.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 28/02/2020, 428422</TITRE>
<DATE_DEC>2020-02-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>428422</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUVENIN, COUDRAY, GREVY</AVOCATS>
<RAPPORTEUR>M. Sébastien Gauthier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Guillaume Odinet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:428422.20200228</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire complémentaire, enregistrés les 26 février et 27 mai 2019 au secrétariat du contentieux du Conseil d'Etat, l'Union fédérale CFDT des cheminots et des activités complémentaires (UFCAC-CFDT) demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'article 5 du décret n° 2018-1242 du 26 décembre 2018 relatif au transfert des contrats de travail des salariés en cas de changement d'attributaire d'un contrat de service public de transport ferroviaire de voyageurs ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code des transports ;<br/>
              - le code du travail ;<br/>
              - la loi n° 2018-515 du 27 juin 2018 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Sébastien Gauthier, maître des requêtes en service extraordinaire, <br/>
<br/>
              - les conclusions de M. Guillaume Odinet, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Thouvenin, Coudray, Grevy, avocat de l'Union fédérale CFDT des cheminots et des activités complémentaires (UFCAC-CFDT) ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1.	Aux termes du premier alinéa de l'article L. 2101-1 du code des transports, dans sa rédaction applicable à la date du décret attaqué : " La SNCF, SNCF Réseau et SNCF Mobilités constituent le groupe public ferroviaire au sein du système ferroviaire national ". Selon le I de l'article L. 2101-2 du même code : " I.- La société nationale SNCF et les sociétés relevant des activités exercées au 31 décembre 2019 par le groupe public ferroviaire mentionné à l'article L. 2101-1 dans sa rédaction antérieure à la loi n° 2018-515 du 27 juin 2018 pour un nouveau pacte ferroviaire emploient des salariés régis par un statut particulier élaboré dans des conditions fixées par décret en Conseil d'État et des salariés sous le régime des conventions collectives ".<br/>
<br/>
              2.	Aux termes du I de l'article L. 2121-26 du même code : " Les salariés employés par le groupe public mentionné à l'article L. 2101-1 dont le contrat de travail se poursuit auprès d'un nouvel attributaire bénéficient des garanties suivantes : / 1° Le niveau de leur rémunération ne peut être inférieur au montant annuel, pour une durée de travail équivalente, correspondant à l'ensemble des éléments de rémunération, comprenant la rémunération fixe, les primes, indemnités, allocations et gratifications, versés lors des douze mois précédant la date de changement effectif d'employeur, hors éléments exceptionnels. Ce montant correspond au montant net de cotisations salariales. Un décret en Conseil d'Etat détermine les conditions d'application du présent 1° ".<br/>
<br/>
              3.	Pour l'application de ces dispositions, le II et le III de l'article 5 du décret attaqué énumèrent les éléments de rémunération des salariés du groupe public ferroviaire régis par le statut particulier mentionné à l'article L. 2101-2 du code des transports ou placés sous le régime des conventions collectives. Le syndicat requérant demande l'annulation de ces dispositions en tant qu'elles n'incluent pas " l'allocation familiale supplémentaire " parmi ces éléments.<br/>
<br/>
              4.	En prévoyant, dans le cas d'un changement d'attributaire d'un contrat de service public ferroviaire, une garantie de rémunération pour les salariés dont le contrat de travail se poursuit auprès du nouvel attributaire, le législateur a entendu, ainsi qu'il résulte des dispositions mêmes de l'article L. 2121-26 du code des transports et des débats parlementaires relatifs à la loi du 27 juin 2018 pour un nouveau pacte ferroviaire de laquelle ces dispositions sont issues, que soient pris en compte tous les éléments de rémunération, en particulier les allocations lorsque celles-ci, de caractère non exceptionnel, doivent être regardées comme faisant partie de la rémunération du salarié. Si les " directives " générales arrêtées par la SNCF tant pour les salariés du groupe public ferroviaire régis par le statut particulier mentionné à l'article L. 2101-2 du code des transports que pour ceux qui sont placés sous le régime des conventions collectives ne mentionnent pas l'allocation familiale supplémentaire dans la définition de la rémunération mensuelle des agents, la directive n° GRH00649 du 3 avril 2018, relative à cette allocation, dispose qu'elle est un " élément mensuel de rémunération propre à la SNCF ". Cette allocation est, selon ces dispositions, " versée sur la paie du salarié " et son montant est, à partir de deux enfants, fonction de la qualification du salarié ou de sa rémunération. Elle constitue ainsi, en raison de ses caractéristiques, non une prestation sociale mais un élément de la rémunération du salarié de caractère non exceptionnel. L'UFCAC-CFDT est dès lors fondée à soutenir, sans qu'il soit besoin d'examiner les autres moyens de sa requête, qu'en n'incluant pas cette allocation dans les énumérations des II et III de l'article 5, le Premier ministre a méconnu le I de l'article L. 2121-26 du code des transports et à demander, dans cette mesure, l'annulation de ces dispositions.   <br/>
<br/>
              5.	Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement à l'Union fédérale CFDT des cheminots et des activités complémentaires (UFCAC-CFDT) d'une somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Les II et III de l'article 5 du décret n° 2018-1242 du 26 décembre 2018 sont annulés en tant qu'ils ne mentionnent pas l'allocation familiale supplémentaire parmi les éléments de rémunération.<br/>
<br/>
Article 2 : L'Etat versera à l'Union fédérale CFDT des cheminots et des activités complémentaires (UFCAC-CFDT) une somme de 3 500 euros en application de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée à l'Union fédérale CFDT des cheminots et des activités complémentaires (UFCAC-CFDT) et à la ministre de la transition écologique et solidaire.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">65-01-02 TRANSPORTS. TRANSPORTS FERROVIAIRES. OPÉRATEURS DE TRANSPORTS FERROVIAIRES. - CHANGEMENT D'ATTRIBUTAIRE D'UN CONTRAT DE SERVICE PUBLIC DE TRANSPORT FERROVIAIRE DE VOYAGEURS (ART. L. 2121-20 ET S. DU CODE DES TRANSPORTS) - ELÉMENTS DE RÉMUNÉRATION GARANTIS AUX SALARIÉS DONT LE CONTRAT DE TRAVAIL SE POURSUIT AUPRÈS DU NOUVEL ATTRIBUTAIRE (I DE L'ART. L. 2121-26 DU MÊME CODE) - INCLUSION - ALLOCATION FAMILIALE SUPPLÉMENTAIRE VERSÉE AUX SALARIÉS DE LA SNCF (DIRECTIVE DU 3 AVRIL 2018).
</SCT>
<ANA ID="9A"> 65-01-02 En prévoyant, dans le cas d'un changement d'attributaire d'un contrat de service public ferroviaire, une garantie de rémunération pour les salariés dont le contrat de travail se poursuit auprès du nouvel attributaire, le législateur a entendu, ainsi qu'il résulte des dispositions mêmes de l'article L. 2121-26 du code des transports et des débats parlementaires relatifs à la loi du 27 juin 2018 pour un nouveau pacte ferroviaire de laquelle ces dispositions sont issues, que soient pris en compte l'ensemble des éléments de rémunération, en particulier les allocations lorsque celles-ci, de caractère non exceptionnel, doivent être regardées comme faisant partie de la rémunération du salarié. Si les directives générales arrêtées par la SNCF tant pour les salariés du groupe public ferroviaire régis par le statut particulier mentionné à l'article L. 2101-2 du code des transports que pour ceux qui sont placés sous le régime des conventions collectives ne mentionnent pas les allocations familiales supplémentaires dans la définition de la rémunération mensuelle des agents, la directive n° GRH00649 du 3 avril 2018, relative à cette allocation, dispose qu'elle est un élément mensuel de rémunération propre à la SNCF. Cette allocation est, selon ces dispositions, versée sur la paie du salarié et son montant est, à partir de deux enfants, fonction de la qualification du salarié ou de sa rémunération. Elle constitue ainsi, en raison de ses caractéristiques, non une prestation sociale mais un élément de la rémunération du salarié de caractère non exceptionnel. Dès lors, cette allocation doit être incluse dans les éléments de rémunération garantis en vertu du I de l'article L. 2121-26 du code des transports aux salariés dont le contrat de travail se poursuit auprès du nouvel attributaire.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
