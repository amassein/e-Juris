<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034797220</ID>
<ANCIEN_ID>JG_L_2017_05_000000397577</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/79/72/CETATEXT000034797220.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 19/05/2017, 397577</TITRE>
<DATE_DEC>2017-05-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>397577</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FABIANI, LUC-THALER, PINATEL ; SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>Mme Marie-Anne Lévêque</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier Henrard</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:397577.20170519</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Marseille d'annuler la décision du 7 juillet 2011 par laquelle le président de la chambre de métiers et de l'artisanat des Alpes de Haute-Provence (CMA-AHP) l'a licencié sans indemnité et de condamner la chambre à lui verser la somme de 48 000 euros en réparation des préjudices subis. Par un jugement n° 1105547 du 10 avril 2014, le tribunal administratif de Marseille a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 14MA02545 du 22 décembre 2015, la cour administrative d'appel de Marseille a, sur appel de M.A..., annulé la décision en litige en tant qu'elle refuse l'octroi à M. A...d'une indemnité de licenciement et rejeté le surplus de ses conclusions.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 2 mars et 2 juin 2016 au secrétariat du contentieux du Conseil d'Etat, M. A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit au surplus de ses conclusions d'appel ;<br/>
<br/>
              3°) de mettre à la charge de la CMA-AHP la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la loi n° 52-1311 du 10 décembre 1952 ;<br/>
              - le statut du personnel administratif des chambres de métiers et de l'artisanat ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie-Anne Lévêque, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Olivier Henrard, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Fabiani, Luc-Thaler, Pinatel, avocat de M.A..., et à la SCP Lyon-Caen, Thiriez, avocat de la chambre de métiers et de l'artisanat des Alpes de Haute-Provence.<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des énonciations de l'arrêt attaqué que M.A..., recruté comme enseignant par la chambre de métiers et de l'artisanat des Alpes de Haute-Provence en 1992, a été licencié, sans indemnité, le 7 juillet 2011 en raison de son inaptitude physique à l'exercice de ses fonctions ; que, par jugement du 10 avril 2014, le tribunal administratif de Marseille a rejeté sa demande tendant à l'annulation de cette décision de licenciement ; que M. A...se pourvoit en cassation contre l'arrêt du 22 décembre 2015 par lequel la cour administrative d'appel de Marseille n'a annulé cette décision qu'en tant qu'elle ne lui accorde pas d'indemnité de licenciement ; que son pourvoi doit être regardé comme dirigé uniquement contre la partie de l'arrêt attaqué lui faisant grief ;<br/>
<br/>
              2. Considérant, d'une part, qu'il résulte d'un principe général du droit, dont s'inspirent tant les dispositions du code du travail relatives à la situation des salariés qui, pour des raisons médicales, ne peuvent plus occuper leur emploi que les règles statutaires applicables dans ce cas aux fonctionnaires, que, lorsqu'il a été médicalement constaté qu'un salarié se trouve, de manière définitive, atteint d'une inaptitude physique à occuper son emploi, il incombe à l'employeur public, avant de pouvoir prononcer son licenciement, de chercher à reclasser l'intéressé ; que la mise en oeuvre de ce principe implique que l'employeur propose à ce dernier un emploi compatible avec son état de santé et aussi équivalent que possible avec l'emploi précédemment occupé ou, à défaut d'un tel emploi, tout autre emploi si l'intéressé l'accepte ; que, dans le cas où le reclassement s'avère impossible, faute d'emploi vacant, ou si l'intéressé refuse la proposition qui lui est faite, il appartient à l'employeur de prononcer, dans les conditions applicables à l'intéressé, son licenciement ; que ce principe est applicable, en particulier, aux agents titulaires de droit public des chambres de métiers ;<br/>
<br/>
<br/>
              3. Considérant, d'autre part, qu'aux termes de l'article 40 du statut du personnel administratif des chambres de métiers : " Le licenciement résulte : / (...) - du fait que l'agent cesse de remplir une des conditions spécifiées à l'article 7 notamment au regard de son aptitude physique (art. 48-III) ; (...) " ; qu'aux termes du III de l'article 48 du même statut " (...) L'agent qui (...) fait l'objet d'un avis d'inaptitude définitive à l'emploi occupé établi par le médecin du travail (...), peut être reclassé sur un emploi susceptible de lui correspondre ou licencié pour inaptitude physique ou, s'il en remplit les conditions, admis à la retraite. (...) / L'agent est, le cas échéant, reclassé dans son nouvel emploi à un niveau équivalent de classement et de durée de présence dans l'échelon. / En cas de litige sur le point de savoir si l'emploi offert au titre du reclassement correspond ou non aux aptitudes de l'agent, la commission paritaire locale (...) est appelée à émettre un avis conformément aux dispositions du même article. / Dans le cas où la commission détermine que l'emploi offert au titre du reclassement correspond aux aptitudes de l'agent et que l'agent refuse l'emploi offert, celui-ci est licencié sans indemnité. / Dans le cas où la commission détermine que l'emploi offert au titre du reclassement ne correspond pas aux aptitudes de l'agent et que l'agent refuse l'emploi offert, celui-ci est licencié et bénéficie des indemnités de licenciement selon les modalités définies à l'article 44-I-3 " ;<br/>
<br/>
              4. Considérant qu'il résulte de ces dispositions, interprétées à la lumière du principe général du droit rappelé au point 2, que, quel que soit l'avis de la commission paritaire locale sur le caractère approprié de l'emploi proposé, au titre du reclassement, à un agent de chambre de métiers déclaré inapte, pour des raisons médicales, à exercer ses fonctions, la chambre est tenue, sous le contrôle du juge administratif, de lui proposer un emploi compatible avec son état de santé et aussi équivalent que possible avec l'emploi précédemment occupé ou, à défaut d'un tel emploi, tout autre emploi si l'intéressé l'accepte ; qu'il suit de là qu'en jugeant que la circonstance que l'emploi offert au titre du reclassement ne correspondrait pas aux aptitudes de l'agent a seulement pour effet d'ouvrir à celui-ci le droit de percevoir l'indemnité de licenciement et ne peut conduire, sauf à contester la légalité des dispositions du III de l'article 48 du statut du personnel des chambres de métiers, au constat de l'inexécution par la chambre de métiers concernée de son obligation de reclassement et, par suite, à l'illégalité de la décision de licenciement elle-même, la cour a commis une erreur de droit ; que, par suite et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, son arrêt doit être annulé en tant qu'il rejette les conclusions de M. A...tendant à l'annulation de la décision du 7 juillet 2011 prononçant son licenciement ;<br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la CMA-AHP une somme de 3 000 euros à verser à M. A...au titre de l'article L. 761-1 du code de justice administrative ; qu'en revanche, ces dispositions font obstacle à ce qu'une somme soit mise, à ce titre, à la charge de M.A..., qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Marseille du 22 décembre 2015 est annulé en tant qu'il rejette les conclusions de M. A...tendant à l'annulation de la décision du 7 juillet 2011 prononçant son licenciement.<br/>
Article 2 : L'affaire est renvoyée, dans cette mesure, à la cour administrative d'appel de Marseille.<br/>
Article 3 : La chambre de métiers et de l'artisanat des Alpes de Haute-Provence versera une somme de 3 000 euros à M. A...au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions présentées par la chambre de métiers et de l'artisanat des Alpes de Haute-Provence tendant à l'application de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à M. B...A...et à la chambre de métiers et de l'artisanat des Alpes de Haute-Provence.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-04-03-08 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. PRINCIPES GÉNÉRAUX DU DROIT. RECONNAISSANCE DE DROITS SOCIAUX FONDAMENTAUX. - OBLIGATION POUR L'EMPLOYEUR DE CHERCHER À RECLASSER UN SALARIÉ ATTEINT DE MANIÈRE DÉFINITIVE D'UNE INAPTITUDE À EXERCER SON EMPLOI AVANT DE POUVOIR PRONONCER SON LICENCIEMENT [RJ1] - PORTÉE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">14-06-02-03 COMMERCE, INDUSTRIE, INTERVENTION ÉCONOMIQUE DE LA PUISSANCE PUBLIQUE. ORGANISATION PROFESSIONNELLE DES ACTIVITÉS ÉCONOMIQUES. CHAMBRES DES MÉTIERS. PERSONNEL. - PRINCIPE GÉNÉRAL DU DROIT - OBLIGATION POUR L'EMPLOYEUR DE CHERCHER À RECLASSER UN SALARIÉ ATTEINT DE MANIÈRE DÉFINITIVE D'UNE INAPTITUDE À EXERCER SON EMPLOI AVANT DE POUVOIR PRONONCER SON LICENCIEMENT [RJ1] - 1) PORTÉE - 2) APPLICATION AUX AGENTS TITULAIRES DE DROIT PUBLIC DES CHAMBRES DE MÉTIERS - EXISTENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">36-10-06-02 FONCTIONNAIRES ET AGENTS PUBLICS. CESSATION DE FONCTIONS. LICENCIEMENT. AUXILIAIRES, AGENTS CONTRACTUELS ET TEMPORAIRES. - OBLIGATION POUR L'EMPLOYEUR DE CHERCHER À RECLASSER UN SALARIÉ ATTEINT DE MANIÈRE DÉFINITIVE D'UNE INAPTITUDE À EXERCER SON EMPLOI AVANT DE POUVOIR PRONONCER SON LICENCIEMENT [RJ1] - EXISTENCE.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">36-12-02 FONCTIONNAIRES ET AGENTS PUBLICS. AGENTS CONTRACTUELS ET TEMPORAIRES. EXÉCUTION DU CONTRAT. - OBLIGATION POUR L'EMPLOYEUR DE CHERCHER À RECLASSER UN SALARIÉ ATTEINT DE MANIÈRE DÉFINITIVE D'UNE INAPTITUDE À EXERCER SON EMPLOI AVANT DE POUVOIR PRONONCER SON LICENCIEMENT [RJ1] - EXISTENCE.
</SCT>
<SCT ID="8E" TYPE="PRINCIPAL">36-12-03-01 FONCTIONNAIRES ET AGENTS PUBLICS. AGENTS CONTRACTUELS ET TEMPORAIRES. FIN DU CONTRAT. LICENCIEMENT. - OBLIGATION POUR L'EMPLOYEUR DE CHERCHER À RECLASSER UN SALARIÉ ATTEINT DE MANIÈRE DÉFINITIVE D'UNE INAPTITUDE À EXERCER SON EMPLOI AVANT DE POUVOIR PRONONCER SON LICENCIEMENT [RJ1] - EXISTENCE.
</SCT>
<ANA ID="9A"> 01-04-03-08 Il résulte d'un principe général du droit, dont s'inspirent tant les dispositions du code du travail relatives à la situation des salariés qui, pour des raisons médicales, ne peuvent plus occuper leur emploi que les règles statutaires applicables dans ce cas aux fonctionnaires, que, lorsqu'il a été médicalement constaté qu'un salarié se trouve, de manière définitive, atteint d'une inaptitude physique à occuper son emploi, il incombe à l'employeur public, avant de pouvoir prononcer son licenciement, de chercher à reclasser l'intéressé. La mise en oeuvre de ce principe implique que l'employeur propose à ce dernier un emploi compatible avec son état de santé et aussi équivalent que possible à l'emploi précédemment occupé ou, à défaut d'un tel emploi, tout autre emploi si l'intéressé l'accepte. Dans le cas où le reclassement s'avère impossible, faute d'emploi vacant, ou si l'intéressé refuse la proposition qui lui est faite, il appartient à l'employeur de prononcer, dans les conditions applicables à l'intéressé, son licenciement.</ANA>
<ANA ID="9B"> 14-06-02-03 1) Il résulte d'un principe général du droit, dont s'inspirent tant les dispositions du code du travail relatives à la situation des salariés qui, pour des raisons médicales, ne peuvent plus occuper leur emploi que les règles statutaires applicables dans ce cas aux fonctionnaires, que, lorsqu'il a été médicalement constaté qu'un salarié se trouve, de manière définitive, atteint d'une inaptitude physique à occuper son emploi, il incombe à l'employeur public, avant de pouvoir prononcer son licenciement, de chercher à reclasser l'intéressé. La mise en oeuvre de ce principe implique que l'employeur propose à ce dernier un emploi compatible avec son état de santé et aussi équivalent que possible à l'emploi précédemment occupé ou, à défaut d'un tel emploi, tout autre emploi si l'intéressé l'accepte. Dans le cas où le reclassement s'avère impossible, faute d'emploi vacant, ou si l'intéressé refuse la proposition qui lui est faite, il appartient à l'employeur de prononcer, dans les conditions applicables à l'intéressé, son licenciement.,,,2) Ce principe est applicable, en particulier, aux agents titulaires de droit public des chambres de métiers.</ANA>
<ANA ID="9C"> 36-10-06-02 Il résulte d'un principe général du droit, dont s'inspirent tant les dispositions du code du travail relatives à la situation des salariés qui, pour des raisons médicales, ne peuvent plus occuper leur emploi que les règles statutaires applicables dans ce cas aux fonctionnaires, que, lorsqu'il a été médicalement constaté qu'un salarié se trouve, de manière définitive, atteint d'une inaptitude physique à occuper son emploi, il incombe à l'employeur public, avant de pouvoir prononcer son licenciement, de chercher à reclasser l'intéressé. La mise en oeuvre de ce principe implique que l'employeur propose à ce dernier un emploi compatible avec son état de santé et aussi équivalent que possible à l'emploi précédemment occupé ou, à défaut d'un tel emploi, tout autre emploi si l'intéressé l'accepte. Dans le cas où le reclassement s'avère impossible, faute d'emploi vacant, ou si l'intéressé refuse la proposition qui lui est faite, il appartient à l'employeur de prononcer, dans les conditions applicables à l'intéressé, son licenciement.</ANA>
<ANA ID="9D"> 36-12-02 Il résulte d'un principe général du droit, dont s'inspirent tant les dispositions du code du travail relatives à la situation des salariés qui, pour des raisons médicales, ne peuvent plus occuper leur emploi que les règles statutaires applicables dans ce cas aux fonctionnaires, que, lorsqu'il a été médicalement constaté qu'un salarié se trouve, de manière définitive, atteint d'une inaptitude physique à occuper son emploi, il incombe à l'employeur public, avant de pouvoir prononcer son licenciement, de chercher à reclasser l'intéressé. La mise en oeuvre de ce principe implique que l'employeur propose à ce dernier un emploi compatible avec son état de santé et aussi équivalent que possible à l'emploi précédemment occupé ou, à défaut d'un tel emploi, tout autre emploi si l'intéressé l'accepte. Dans le cas où le reclassement s'avère impossible, faute d'emploi vacant, ou si l'intéressé refuse la proposition qui lui est faite, il appartient à l'employeur de prononcer, dans les conditions applicables à l'intéressé, son licenciement.</ANA>
<ANA ID="9E"> 36-12-03-01 Il résulte d'un principe général du droit, dont s'inspirent tant les dispositions du code du travail relatives à la situation des salariés qui, pour des raisons médicales, ne peuvent plus occuper leur emploi que les règles statutaires applicables dans ce cas aux fonctionnaires, que, lorsqu'il a été médicalement constaté qu'un salarié se trouve, de manière définitive, atteint d'une inaptitude physique à occuper son emploi, il incombe à l'employeur public, avant de pouvoir prononcer son licenciement, de chercher à reclasser l'intéressé. La mise en oeuvre de ce principe implique que l'employeur propose à ce dernier un emploi compatible avec son état de santé et aussi équivalent que possible à l'emploi précédemment occupé ou, à défaut d'un tel emploi, tout autre emploi si l'intéressé l'accepte. Dans le cas où le reclassement s'avère impossible, faute d'emploi vacant, ou si l'intéressé refuse la proposition qui lui est faite, il appartient à l'employeur de prononcer, dans les conditions applicables à l'intéressé, son licenciement.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., en précisant, CE, 2 octobre 2002, Chambre de commerce et d'industrie de Meurthe-et-Moselle, n° 227868, p. 319. Rappr., s'agissant du reclassement des contractuels dont le contrat n'est pas régularisé, CE, Section, 31 décembre 2008,,, n° 283256, p. 481 ; s'agissant des contractuels dont l'emploi est supprimé, CE, Section, avis, 25 septembre 2013, Mme,, n° 365139, p. 223.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
