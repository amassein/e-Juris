<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033889694</ID>
<ANCIEN_ID>JG_L_2017_01_000000386799</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/88/96/CETATEXT000033889694.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 13/01/2017, 386799, Publié au recueil Lebon</TITRE>
<DATE_DEC>2017-01-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>386799</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>Mme Pauline Jolivet</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:386799.20170113</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par un jugement n° 1001545/5-3 du 10 avril 2013, le tribunal administratif de Paris a rejeté la requête de M. A...B...tendant, d'une part, à l'annulation de la décision implicite par laquelle le ministre chargé du budget a rejeté sa demande d'indemnisation préalable présentée par lettre du 25 novembre 2008 et, d'autre part, à la condamnation de l'Etat à lui verser la somme de 91 066 169 euros en réparation de son préjudice.<br/>
<br/>
              Par un arrêt n° 13PA02359 du 30 octobre 2014, la cour administrative d'appel de Paris a rejeté l'appel formé par M. B...contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 30 décembre 2014, 30 mars 2015 et 13 janvier 2016 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code des douanes ;<br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - l'arrêté du 18 avril 1957 portant fixation des modalités d'application de l'article 391 du code des douanes relatif à la répartition des produits des amendes et confiscations ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Pauline Jolivet, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat de M. A...B...;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. A...B...a été enregistré en 1988 comme informateur susceptible d'être rémunéré par la direction nationale du renseignement et des enquêtes douanières et, qu'à ce titre, il a fourni des informations ayant permis l'arrestation de plusieurs trafiquants ainsi que la saisie de quantités importantes de produits stupéfiants et des moyens de locomotion ayant servi à leurs transports. Cependant, il a été condamné, d'une part, le 23 décembre 1996 en Grande-Bretagne à quatre ans d'emprisonnement du chef de " conspiration dans le but de faire entrer du cannabis sur le territoire anglais " et, d'autre part, le 5 août 1997 par la cour provinciale du Nouveau-Brunswick (Canada) à la réclusion criminelle à perpétuité pour " complot d'importation de stupéfiants " après avoir plaidé coupable. Le 10 juillet 1998, dans le cadre de l'exécution de sa peine, il a été transféré en France où, par jugement du 22 janvier 1999, le tribunal de grande instance de Bobigny a dit que la peine applicable pour l'infraction correspondante en France est celle de vingt ans d'emprisonnement. Il a bénéficié d'une libération conditionnelle le 26 mai 2005. Il a présenté au ministre du budget, des comptes publics et de la fonction publique, le 25 novembre 2008, une demande d'indemnisation des préjudices qui résulteraient des fautes commises par les services des douanes et ayant abouti à son incarcération. Il a contesté le refus implicite du ministre devant le tribunal administratif de Paris et demandé la condamnation de l'Etat à lui verser la somme de 91 066 169 euros en réparation des divers préjudices qu'il allègue avoir subis à raison de ces fautes alléguées. M. B...se pourvoit en cassation à l'encontre de l'arrêt du 30 octobre 2014 par lequel la cour administrative d'appel de Paris a rejeté son appel contre le jugement du tribunal administratif de Paris du 10 avril 2013 rejetant sa demande.	<br/>
<br/>
              Sur le préjudice consécutif au refus de l'octroi de la protection fonctionnelle : <br/>
<br/>
              2. Aux termes de l'article 2 de l'arrêté du 18 avril 1957 du secrétaire d'Etat au budget portant fixation des modalités d'application de l'article 391 du code des douanes relatif à la répartition des produits des amendes et confiscations, dans sa version applicable au litige : " Toute personne, étrangère aux administrations publiques, qui a fourni au service des douanes des renseignements ou avis sur la fraude reçoit une part, susceptible d'atteindre le tiers du produit disponible de l'affaire considérée dans le cas où ses renseignements ou avis ont amené directement la découverte de la fraude. ".  Il résulte de ces dispositions que le pouvoir réglementaire a entendu permettre la rémunération de la participation ponctuelle au service public des douanes consistant, pour une personne, à fournir spontanément ou à la demande de l'administration des renseignements susceptibles de favoriser la découverte d'une fraude. Ainsi, une personne qui apporte, dans ces conditions, son concours au service des douanes prend part personnellement, dans cette mesure, à une mission de service public. A ce titre, elle doit être regardée comme possédant la qualité de collaborateur occasionnel du service public.<br/>
<br/>
              3. Il résulte d'un principe général du droit que, lorsqu'un agent public est mis en cause par un tiers à raison de ses fonctions, il incombe à la collectivité dont il dépend de le couvrir des condamnations civiles prononcées contre lui, dans la mesure où une faute personnelle détachable du service ne lui est pas imputable, de lui accorder sa protection dans le cas où il fait l'objet de poursuites pénales, sauf s'il a commis une faute personnelle, et, à moins qu'un motif d'intérêt général ne s'y oppose, de le protéger contre les menaces, violences, voies de fait, injures, diffamations ou outrages dont il est l'objet. Ce principe général du droit s'étend à toute personne à laquelle la qualité de collaborateur occasionnel du service public est reconnue. <br/>
<br/>
              4. Il ressort des pièces du dossier soumis aux juges du fond, ainsi qu'il a été dit au point 1, que M. B...a été, en sa qualité d'" aviseur des douanes ", c'est-à-dire d'informateur du service des douanes rémunéré pour les informations transmises sur le fondement des dispositions précitées, un collaborateur occasionnel du service public mais qu'il a néanmoins été condamné pour trafic de stupéfiants par les juridictions anglaises et canadiennes. La cour a souverainement apprécié les faits dont elle était saisie, sans les dénaturer, en estimant que si l'implication croissante de M. B...dans un réseau de trafiquants de drogue a été encouragée à l'origine par l'administration des douanes, les faits pour lesquels il avait été condamné étaient dépourvus de tout lien avec les fonctions exercées en sa qualité d'informateur de l'administration des douanes et étaient donc détachables du service. En déduisant qu'ils étaient constitutifs d'une faute personnelle de l'intéressé et que, dès lors, l'administration n'avait pas commis de faute de nature à engager la responsabilité de l'Etat en refusant de lui octroyer à ce titre le bénéfice de la protection fonctionnelle, la cour n'a pas commis d'erreur de droit ni d'erreur de qualification juridique des faits.<br/>
<br/>
              Sur les autres préjudices :<br/>
<br/>
              5. En premier lieu, pour rejeter les conclusions indemnitaires présentées par M. B... tendant à la réparation du préjudice consécutif à l'insuffisance des rémunérations perçues en contrepartie des informations qu'il avait fournies au service des douanes, la cour administrative d'appel a relevé que l'intéressé n'apportait aucun élément sérieux à l'appui de ses allégations. En tirant de telles conséquences des interprétations portées tant par les premiers juges que par elle sur les écritures du requérant, la cour n'a entaché son arrêt ni d'insuffisance de motivation ni d'erreur de droit. Ce faisant, elle n'a pas davantage méconnu les exigences résultant de l'article 6§1 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales.<br/>
<br/>
              6. En second lieu, il ressort des pièces du dossier soumis aux juges du fond que la cour administrative d'appel n'a pas entaché son arrêt de dénaturation en jugeant que l'administration n'avait pas pris auprès de M. B...d'engagement relatif à une minoration de peine et ne pouvait pas, dès lors, voir sa responsabilité engagée du fait d'une promesse non tenue.<br/>
<br/>
              7. Il résulte de tout ce qui précède que M. B...n'est pas fondé à demander l'annulation de l'arrêt qu'il attaque. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mis à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante, le versement à M. B...d'une somme euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
Article 1er : Le pourvoi de M. B...est rejeté. <br/>
Article 2 : La présente décision sera notifiée à M. A...B...et au ministre l'économie et des finances. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-04-03-07-04 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. PRINCIPES GÉNÉRAUX DU DROIT. PRINCIPES INTÉRESSANT L'ACTION ADMINISTRATIVE. GARANTIES DIVERSES ACCORDÉES AUX AGENTS PUBLICS. - PROTECTION FONCTIONNELLE - 1) CHAMP D'APPLICATION [RJ1] - COLLABORATEURS OCCASIONNELS DU SERVICE PUBLIC - INCLUSION - 2) CONDITION D'OCTROI - ABSENCE DE FAUTE PERSONNELLE - CONDITION NON REMPLIE EN L'ESPÈCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">36-07-10-005 FONCTIONNAIRES ET AGENTS PUBLICS. STATUTS, DROITS, OBLIGATIONS ET GARANTIES. GARANTIES ET AVANTAGES DIVERS. PROTECTION CONTRE LES ATTAQUES. - PROTECTION FONCTIONNELLE - 1) CHAMP D'APPLICATION [RJ1] - COLLABORATEURS OCCASIONNELS DU SERVICE PUBLIC - INCLUSION - 2) CONDITION D'OCTROI - ABSENCE DE FAUTE PERSONNELLE - CONDITION NON REMPLIE EN L'ESPÈCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">60-01-02-01-02-02 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. FAITS SUSCEPTIBLES OU NON D'OUVRIR UNE ACTION EN RESPONSABILITÉ. FONDEMENT DE LA RESPONSABILITÉ. RESPONSABILITÉ SANS FAUTE. RESPONSABILITÉ FONDÉE SUR LE RISQUE CRÉÉ PAR CERTAINES ACTIVITÉS DE PUISSANCE PUBLIQUE. RESPONSABILITÉ FONDÉE SUR L'OBLIGATION DE GARANTIR LES COLLABORATEURS DES SERVICES PUBLICS CONTRE LES RISQUES QUE LEUR FAIT COURIR LEUR PARTICIPATION À L'EXÉCUTION DU SERVICE. - COLLABORATEURS OCCASIONNEL DU SERVICE PUBLIC - AVISEURS DES DOUANES - INCLUSION.
</SCT>
<ANA ID="9A"> 01-04-03-07-04 1) Il résulte d'un principe général du droit que, lorsqu'un agent public est mis en cause par un tiers à raison de ses fonctions, il incombe à la collectivité dont il dépend de le couvrir des condamnations civiles prononcées contre lui, dans la mesure où une faute personnelle détachable du service ne lui est pas imputable, de lui accorder sa protection dans le cas où il fait l'objet de poursuites pénales, sauf s'il a commis une faute personnelle, et, à moins qu'un motif d'intérêt général ne s'y oppose, de le protéger contre les menaces, violences, voies de fait, injures, diffamations ou outrages dont il est l'objet. Ce principe général du droit s'étend à toute personne à laquelle la qualité de collaborateur occasionnel du service public est reconnue.... ,,2) Requérant ayant été collaborateur du service public en sa qualité d'aviseur des douanes et ayant été condamné pour trafic de stupéfiants par les juridictions anglaise et canadienne. La cour a souverainement apprécié les faits dont elle était saisie, sans les dénaturer, en estimant que si l'implication croissante de l'intéressé dans un réseau de trafiquants de drogue a été encouragée à l'origine par l'administration des douanes, les faits pour lesquels il avait été condamné étaient dépourvus de tout lien avec les fonctions exercées en sa qualité d' informateur de l'administration des douanes et étaient donc détachables du service. En en déduisant qu'ils étaient constitutifs d'une faute personnelle de l'intéressé et que, dès lors,  l'administration n'avait pas commis de faute de nature à engager la responsabilité de l'Etat en refusant de lui octroyer à ce titre le bénéfice de la protection fonctionnelle, la cour n'a pas commis d'erreur de droit ni d'erreur de qualification juridique des faits.</ANA>
<ANA ID="9B"> 36-07-10-005 1) Il résulte d'un principe général du droit que, lorsqu'un agent public est mis en cause par un tiers à raison de ses fonctions, il incombe à la collectivité dont il dépend de le couvrir des condamnations civiles prononcées contre lui, dans la mesure où une faute personnelle détachable du service ne lui est pas imputable, de lui accorder sa protection dans le cas où il fait l'objet de poursuites pénales, sauf s'il a commis une faute personnelle, et, à moins qu'un motif d'intérêt général ne s'y oppose, de le protéger contre les menaces, violences, voies de fait, injures, diffamations ou outrages dont il est l'objet. Ce principe général du droit s'étend à toute personne à laquelle la qualité de collaborateur occasionnel du service public est reconnue.... ,,2) Requérant ayant été collaborateur du service public en sa qualité d'aviseur des douanes et ayant été condamné pour trafic de stupéfiants par les juridictions anglaise et canadienne. La cour a souverainement apprécié les faits dont elle était saisie, sans les dénaturer, en estimant que si l'implication croissante de l'intéressé dans un réseau de trafiquants de drogue a été encouragée à l'origine par l'administration des douanes, les faits pour lesquels il avait été condamné étaient dépourvus de tout lien avec les fonctions exercées en sa qualité d' informateur de l'administration des douanes et étaient donc détachables du service. En en déduisant qu'ils étaient constitutifs d'une faute personnelle de l'intéressé et que, dès lors,  l'administration n'avait pas commis de faute de nature à engager la responsabilité de l'Etat en refusant de lui octroyer à ce titre le bénéfice de la protection fonctionnelle, la cour n'a pas commis d'erreur de droit ni d'erreur de qualification juridique des faits.</ANA>
<ANA ID="9C"> 60-01-02-01-02-02 Il résulte de l'article 2 de l'arrêté du 18 avril 1957 du secrétaire d'Etat au budget portant fixation des modalités d'application de l'article 391 du code des douanes relatif à la répartition des produits des amendes et confiscations que le pouvoir réglementaire a entendu permettre la rémunération de la participation ponctuelle au service public des douanes consistant, pour une personne, à fournir spontanément ou à la demande de l'administration des renseignements susceptibles de favoriser la découverte d'une fraude. Ainsi, une personne qui apporte, dans ces conditions, son concours au service des douanes prend part personnellement, dans cette mesure, à une mission de service public [RJ2]. A ce titre, elle doit être regardée comme possédant la qualité de collaborateur occasionnel du service public.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Section, 8 juin 2011, Farré, n° 312700, p. 270.,,[RJ2] Cf., sur la notion de mission de service public, Assemblée, 22 novembre 1946, Commune de Saint-Priest-la-Plaine, n° 74725, p. 279 ; Section, 25 septembre 1970, Commune de Batz-sur-mer et Mme Veuve Tesson, n°s 73707 73727, p. 540.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
