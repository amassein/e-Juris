<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030296273</ID>
<ANCIEN_ID>JG_L_2015_02_000000384847</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/29/62/CETATEXT000030296273.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème / 7ème SSR, 27/02/2015, 384847</TITRE>
<DATE_DEC>2015-02-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>384847</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème / 7ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROUSSEAU, TAPIE</AVOCATS>
<RAPPORTEUR>M. Yves Doutriaux</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier Domino</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:384847.20150227</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 29 septembre et 27 octobre 2014 au secrétariat du contentieux du Conseil d'Etat, présentés pour Mme A...B..., demeurant ... ; Mme B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la lettre du 20 juin 2014 par laquelle le président de l'Agence française de lutte contre le dopage (AFLD) lui a notifié un troisième avertissement pour manquement aux obligations de localisation et le rejet, le 29 juillet 2014 par le comité d'experts pour la localisation, de sa demande de révision à titre gracieux ;<br/>
<br/>
              2°) de mettre à la charge de l'AFLD la somme de 3 500 euros en application des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 11 février 2015, présentée pour Mme B... ;<br/>
<br/>
              Vu le code du sport ; <br/>
<br/>
              Vu la délibération n° 54 rectifiée des 12 juillet et 18 octobre 2007 de l'Agence française de lutte contre le dopage portant modalités de transmission et de gestion des informations de localisation des sportifs faisant l'objet de contrôles individualisés et de sanctions en cas de manquement ;<br/>
<br/>
              Vu la délibération n° 138 du 5 novembre 2009 de l'Agence française de lutte contre le dopage portant modalités de gestion de manquements présumés aux obligations de localisation des sportifs faisant l'objet de contrôles individualisés ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Yves Doutriaux, conseiller d'Etat,<br/>
<br/>
              - les conclusions de M. Xavier Domino, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rousseau, Tapie, avocat de Mme B...; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'aux termes du I de l'article L. 232-5 du code du sport : " L'Agence française de lutte contre le dopage, autorité publique indépendante dotée de la personnalité morale, définit et met en oeuvre les actions de lutte contre le dopage (...) / A cet effet : / (...) 3° Pour les sportifs soumis à l'obligation de localisation mentionnée à l'article L. 232-15, elle diligente les contrôles dans les conditions prévues aux articles L. 232-12 à L. 232-16 : / (...) d) Hors des manifestations sportives mentionnées à l'article L. 230-3, et hors des périodes d'entraînement y préparant ; (...) " ; qu'aux termes de l'article L. 232-15 du même code : " Sont tenus de fournir des renseignements précis et actualisés sur leur localisation permettant la réalisation de contrôles mentionnés à l'article L. 232-5 les sportifs, constituant le groupe cible, désignés pour une année par l'Agence française de lutte contre le dopage (...) " ; qu'aux termes du II de l'article L. 232-17 de ce code : " Les manquements aux obligations de localisation prévues par l'article L. 232-15 sont (...) passibles des sanctions administratives prévues par les articles L. 232-21 à L. 232-23 " ; qu'en vertu des dispositions de l'article L. 232-21, le sportif qui a manqué à ses obligations de localisation est passible de sanctions disciplinaires de la part de la fédération sportive auprès de laquelle il est licencié ; que l'AFLD peut également infliger des sanctions disciplinaires, dans les conditions prévues à l'article L. 232-22, notamment en cas de carence de la fédération ; <br/>
<br/>
              2.	Considérant qu'aux termes de l'article 3 de la délibération n° 54 rectifiée des 12 juillet et 18 octobre 2007 du collège de l'Agence portant modalités de transmission et de gestion des informations de localisation des sportifs faisant l'objet de contrôles individualisés et de sanctions en cas de manquement : " Tout sportif désigné par le directeur des contrôles de l'agence pour faire l' objet de contrôles individualisés doit indiquer, pour chaque jour, un créneau horaire d' une heure, durant lequel il est susceptible de faire l'objet d'un ou de plusieurs contrôles individualisés par l'Agence française de lutte contre le dopage, en application de l'article L. 232-15 du code du sport (...) " ; qu'aux termes de l'article 9 de la même délibération : " Les manquements aux obligations de transmission d'informations relatives à la localisation des sportifs appartenant au groupe cible de l' agence sont : / (...) -l'absence du sportif durant le créneau d'une heure à l'adresse ou sur le lieu indiqués par lui pour la réalisation de contrôles individualisés. Le préleveur missionné à cet effet constate le manquement du sportif à l'issue de l' absence de celui- ci, à l' adresse sur le lieu indiqué, pendant une période continue de trente minutes durant le créneau horaire (...) " ; qu'aux termes de l'article 10 de la même délibération : " Après avoir procédé aux vérifications relatives à la qualification de manquement aux obligations de localisation, l'agence notifie au sportif un avertissement, par lettre recommandée avec demande d'avis de réception (...) " ; qu'enfin, selon l'article 13 de cette délibération : " Si le sportif commet trois manquements mentionnés à l'article 9 pendant une période de dix-huit mois consécutifs, l'agence transmet à la fédération compétente un constat d'infraction, pour l'application de la sanction prévue par l'article 36 du règlement disciplinaire type des fédérations sportives agréées relatif à la lutte contre le dopage humain, annexé à l'article R. 232-86 du code du sport " ;<br/>
<br/>
              3.	Considérant qu'il ressort des pièces du dossier que Mme B...a été désignée, par une délibération du 27 septembre 2012 du collège de l'Agence française de lutte contre le dopage, parmi les sportifs appartenant au groupe " cible " prévu par l'article L. 232-15 du code du sport, astreints à une obligation de localisation à l'effet de permettre des contrôles inopinés relatifs à la lutte contre le dopage ; qu'elle a été désignée à nouveau dans le groupe " cible " par des délibérations du 31 janvier 2013 et du 23 janvier 2014 ; qu'elle a fait l'objet, le 29 mars 2013 puis le 25 février 2014, de deux avertissements pour avoir manqué aux obligations lui incombant à ce titre ; que, sur la base des informations de localisation qu'elle avait fournies, l'Agence a décidé de la soumettre à un  contrôle individualisé, le 10 juin 2014 ; qu'après que le préleveur mandaté à cette fin eut constaté l'absence de Mme B...lors de ce contrôle inopiné, le président de l'Agence a notifié à l'intéressée, en application de l'article 10 de la délibération n° 54 rectifiée des 12 juillet et 18 octobre 2007 du collège de l'Agence, un troisième avertissement pour manquement aux obligations de localisation, par lettre du 20 juin 2014 ; que la demande de révision à titre gracieux formée par MmeB..., sur le fondement de la délibération du 5 novembre 2009 de l'Agence portant modalités de gestion de manquements présumés aux obligations de localisation des sportifs faisant l'objet de contrôles individualisés, a été rejetée le 29 juillet 2014, conformément à l'avis négatif rendu le 25 juillet 2014 par le comité d'experts pour la localisation ; <br/>
<br/>
              4.	Considérant que la lettre du 20 juin 2014 par laquelle le président de l'Agence française de lutte contre le dopage, constatant un troisième manquement aux obligations de localisation dans le délai prévu par l'article 13 de la délibération n° 54 des 12 juillet et 18 octobre 2007, a saisi, conformément aux dispositions de cet article, la fédération dont relève Mme B...aux fins d'engagement de poursuites disciplinaires ne constitue que le premier acte de la procédure pouvant conduire au prononcé d'une sanction à l'égard de l'intéressée ; que, présentant ainsi le caractère d'une mesure préparatoire, elle ne constitue pas par elle-même une décision susceptible de faire l'objet d'un recours pour excès de pouvoir ; qu'il en est de même en ce qui concerne la lettre rejetant la demande de révision à titre gracieux formée sur le fondement de la délibération du 5 novembre 2009 ; <br/>
<br/>
              5.	Considérant qu'il résulte de ce qui précède que la requête de Mme B... n'est pas recevable et ne peut qu'être rejetée, y compris ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête de Mme B...est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à Mme A...B...et à l'Agence française de lutte contre le dopage. <br/>
              Copie en sera adressée pour information au ministre de la ville, de la jeunesse et des sports. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-01-01-02-02 PROCÉDURE. INTRODUCTION DE L'INSTANCE. DÉCISIONS POUVANT OU NON FAIRE L'OBJET D'UN RECOURS. ACTES NE CONSTITUANT PAS DES DÉCISIONS SUSCEPTIBLES DE RECOURS. MESURES PRÉPARATOIRES. - SAISINE PAR L'AFLD D'UNE FÉDÉRATION SPORTIVE AUX FINS D'ENGAGEMENT DE POURSUITES DISCIPLINAIRES POUR MANQUEMENT D'UN SPORTIF À SES OBLIGATIONS DE LOCALISATION [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">63-05-05 SPORTS ET JEUX. SPORTS. - SAISINE PAR L'AFLD D'UNE FÉDÉRATION SPORTIVE AUX FINS D'ENGAGEMENT DE POURSUITES DISCIPLINAIRES POUR MANQUEMENT D'UN SPORTIF À SES OBLIGATIONS DE LOCALISATION [RJ1].
</SCT>
<ANA ID="9A"> 54-01-01-02-02 La délibération n° 54 des 12 juillet et 18 octobre 2007 de l'Agence française de lutte contre le dopage (AFLD) prévoit que chaque manquement aux obligations de localisation auxquelles sont soumis, en vertu de l'article L. 232-15 du code du sport, les sportifs du groupe cible, donne lieu à un avertissement. Au troisième manquement aux obligations de localisation, l'agence transmet à la fédération compétente un constat d'infraction en vue de l'engagement d'une procédure disciplinaire.... ,,La lettre par laquelle le président de l'AFLD constatant un troisième manquement saisit la fédération dont relève le sportif en cause aux fins d'engagement de poursuites disciplinaires ne constitue que le premier acte de la procédure pouvant conduire au prononcé d'une sanction à l'égard de l'intéressé. Il s'agit donc d'une mesure préparatoire insusceptible de faire l'objet d'un recours pour excès de pouvoir.</ANA>
<ANA ID="9B"> 63-05-05 La délibération n° 54 des 12 juillet et 18 octobre 2007 de l'Agence française de lutte contre le dopage (AFLD) prévoit que chaque manquement aux obligations de localisation auxquelles sont soumis, en vertu de l'article L. 232-15 du code du sport, les sportifs du groupe cible, donne lieu à un avertissement. Au troisième manquement aux obligations de localisation, l'agence transmet à la fédération compétente un constat d'infraction en vue de l'engagement d'une procédure disciplinaire.... ,,La lettre par laquelle le président de l'AFLD constatant un troisième manquement saisit la fédération dont relève le sportif en cause aux fins d'engagement de poursuites disciplinaires ne constitue que le premier acte de la procédure pouvant conduire au prononcé d'une sanction à l'égard de l'intéressé. Il s'agit donc d'une mesure préparatoire insusceptible de faire l'objet d'un recours pour excès de pouvoir.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Rappr. CE, 28 juillet 2004, M.,, n° 262851, p. 355.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
