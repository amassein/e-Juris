<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033695590</ID>
<ANCIEN_ID>JG_L_2016_12_000000392230</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/69/55/CETATEXT000033695590.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 5ème chambres réunies, 23/12/2016, 392230</TITRE>
<DATE_DEC>2016-12-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>392230</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER ; BALAT ; SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR>M. Pierre-François Mourier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:392230.20161223</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme D...A...a porté plainte contre M. B... C...devant la chambre disciplinaire de première instance d'Ile-de-France de l'ordre des médecins. Le conseil départemental de la Ville de Paris de l'ordre des médecins s'est associé à la plainte. Par une décision du 16 décembre 2013, la chambre disciplinaire de première instance a rejeté la plainte.<br/>
<br/>
              Par une décision du 10 juillet 2015, la chambre disciplinaire nationale de l'ordre des médecins a, sur appel de MmeA..., annulé cette décision et infligé à M. C... la sanction d'interdiction d'exercer la médecine pendant trois mois dont deux assortis du sursis.<br/>
<br/>
              Par un pourvoi et deux mémoires en réplique, enregistrés au secrétariat du contentieux du Conseil d'Etat les 31 juillet 2015 et 1er juin et 4 juillet 2016, M. C... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette décision ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de Mme A... ;<br/>
<br/>
              3°) de mettre à la charge de Mme A...et du conseil départemental de la Ville de Paris de l'ordre des médecins la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la santé publique ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Pierre-François Mourier, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Foussard, Froger, avocat de M. C...et à Me Balat, avocat de Mme A...;<br/>
<br/>
<br/>
<br/>
<br/>1.  Considérant qu'il ressort des pièces du dossier soumis aux juges du fond qu'après avoir reçu Mme A... en consultation à plusieurs reprises dans le cadre de son activité libérale à l'hôpital Henri Mondor, M. C..., professeur des universités-praticien hospitalier, alors chef de service en chirurgie plastique dans cet établissement public, l'a opérée les 8 mars 2010 et 21 juin 2011 dans le cadre du service public hospitalier ; qu'à la suite de ces interventions, Mme A... a porté plainte contre M. C... devant la chambre disciplinaire de première instance d'Ile-de-France de l'ordre des médecins ; que le conseil départemental de la Ville de Paris de l'ordre des médecins s'est associé à la plainte ; que, par une décision du 16 décembre 2013, la chambre disciplinaire de première instance a rejeté la plainte ; que, saisie du seul appel de Mme A..., la chambre disciplinaire nationale de l'ordre des médecins a, par la décision attaquée, annulé la décision de la chambre disciplinaire de première instance et prononcé à l'encontre de M. C... la sanction d'interdiction d'exercer la médecine pour une durée de trois mois, dont deux assortis du sursis ; <br/>
<br/>
              2.  Considérant qu'aux termes du premier alinéa de l'article L. 4124-2 du code de la santé publique : " Les médecins (...) chargés d'un service public et inscrits au tableau de l'ordre ne peuvent être traduits devant la chambre disciplinaire de première instance, à l'occasion des actes de leur fonction publique, que par le ministre chargé de la santé, le représentant de l'Etat dans le département, le directeur général de l'agence régionale de santé, le procureur de la République, le conseil national ou le conseil départemental au tableau duquel le praticien est inscrit " ; qu'il en résulte que, lorsque l'auteur d'une plainte dirigée contre un praticien chargé d'un service public n'est pas au nombre des personnes limitativement énumérées par cet article, cette plainte n'est recevable qu'en tant qu'elle se rapporte à des actes qui n'ont pas été accomplis par ce praticien à l'occasion de sa fonction publique ; <br/>
<br/>
              3.  Considérant qu'il ressort des termes mêmes de la décision attaquée que, pour infliger à M. C...la sanction litigieuse, la chambre disciplinaire nationale s'est fondée, d'une part sur le grief tiré de ce que les incertitudes quant à la nature de l'intervention chirurgicale effectivement réalisée le 8 mai 2010 ne permettaient pas d'établir que Mme A...y avait donné un consentement éclairé et, d'autre part, sur le grief tiré de ce qu'après cette intervention chirurgicale, M. C... ne l'avait pas informée de ce qu'il avait réalisé, lors de l'opération, un curage ganglionnaire et une transfusion sanguine ; <br/>
<br/>
              4. Considérant que ces griefs, qui mettent en cause, d'une part le recueil du consentement d'un patient à une intervention chirurgicale et, d'autre part, la façon dont le praticien a rendu compte à ce patient du déroulement de l'intervention, sont relatifs à des actes qui, par leur nature, doivent être regardés, pour l'application de l'article L. 4124-2 du code de la santé publique, comme accomplis dans le même cadre que l'opération chirurgicale à laquelle ils se rapportent ;<br/>
<br/>
              5. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, ainsi qu'il a été dit, l'intervention en litige a été effectuée par M. C...dans le cadre du service public hospitalier ; qu'elle revêtait, par suite, le caractère d'un acte de sa fonction publique ; que par suite, alors même que M. C...avait, avant cette intervention, reçu sa patiente en consultation préopératoire dans le cadre de son activité libérale, il résulte de ce qui a été dit ci-dessus que les omissions reprochées dans le recueil du consentement relatif à cette opération et dans l'information de sa patiente sur les actes effectués lors de la même opération ont le caractère d'actes accomplis à l'occasion de sa fonction publique ;<br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que la plainte de MmeA..., qui soulevait des griefs relatifs, d'une part, à des actes commis lors des consultations préopératoires en secteur privé et, d'autre part, à un défaut de consentement et à un défaut d'information sur l'opération effectuée dans le cadre du service public, était irrecevable en tant qu'elle soulevait ce second ensemble de griefs ; qu'en ne relevant pas d'office cette irrecevabilité et en se fondant, alors qu'elle n'était saisie que du seul appel de MmeA..., sur ces griefs pour prononcer la sanction, la chambre disciplinaire nationale a entaché sa décision d'erreur de droit ; que, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, M. C... est fondé à en demander l'annulation ;<br/>
<br/>
              7. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de M. C... qui n'est pas la partie perdante dans la présente instance ; qu'elles font également obstacle à ce qu'une somme soit mise à la charge du conseil départemental de la Ville de Paris de l'ordre des médecins qui n'a pas la qualité de partie dans la présente instance ; qu'enfin, il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de Mme A...la somme que M. C... demande au titre de ces mêmes dispositions ;<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : La décision de la chambre disciplinaire nationale de l'ordre des médecins du 10 juillet 2015 est annulée.<br/>
Article 2 : L'affaire est renvoyée devant la chambre disciplinaire nationale de l'ordre des médecins.<br/>
Article 3 : Les conclusions présentées par M. C... et par Mme A...au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à M. B... C...et à Mme D...A....<br/>
Copie en sera adressée au Conseil national de l'ordre des médecins.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">55-04-01-01 PROFESSIONS, CHARGES ET OFFICES. DISCIPLINE PROFESSIONNELLE. PROCÉDURE DEVANT LES JURIDICTIONS ORDINALES. INTRODUCTION DE L'INSTANCE. - RECEVABILITÉ - PLAINTE PORTANT SUR DES MANQUEMENTS COMMIS PAR UN PRATICIEN HOSPITALIER DANS L'EXERCICE DE SES FONCTIONS PUBLIQUES - RECEVABILITÉ LIMITÉE AUX PLAINTES INTRODUITES PAR LES PERSONNES VISÉES À L'ARTICLE L. 4124-2 DU CSP - CAS D'UN PRATICIEN RECEVANT LE PATIENT EN CONSULTATION PRÉOPÉRATOIRE DANS LE CADRE DE SON ACTIVITÉ LIBÉRALE ET L'OPÉRANT DANS LE CADRE DU SERVICE PUBLIC.
</SCT>
<ANA ID="9A"> 55-04-01-01 Des omissions dans le recueil du consentement et dans l'information du patient sur les actes effectués lors d'une opération chirurgicale réalisée dans le cadre du service public hospitalier sont des actes accomplis à l'occasion d'un service public pour lesquels le praticien hospitalier ne peut être poursuivi qu'à la suite d'une plainte formée par l'une des autorités mentionnées à l'article L. 4124-2 du code de la santé publique (CSP), alors même que le praticien a, avant l'intervention, reçu le patient en consultation préopératoire dans le cadre de son activité libérale.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
