<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032950602</ID>
<ANCIEN_ID>JG_L_2016_07_000000399942</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/95/06/CETATEXT000032950602.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 22/07/2016, 399942</TITRE>
<DATE_DEC>2016-07-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>399942</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC/>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MEIER-BOURDEAU, LECUYER</AVOCATS>
<RAPPORTEUR>M. Vincent Villette</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:399942.20160722</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              L'association Non aux éoliennes entre Noirmoutier et Yeu a demandé au tribunal administratif de Paris d'annuler la décision implicite par laquelle la ministre de l'écologie et du développement durable a rejeté sa demande tendant à la communication de l'ensemble des documents rédigés par la Commission de régulation de l'énergie pour le choix de l'entreprise retenue dans le cadre de l'appel d'offres n° 2013/S 054-088441, l'offre remise par la société Eoliennes en Mer de Vendée et l'ensemble des documents, décisions et études réalisés préalablement à l'engagement de la procédure d'appel d'offre. <br/>
<br/>
              Par un jugement n° 1503802/5-2 du 7 avril 2016, le tribunal administratif de Paris a annulé la décision implicite attaquée et enjoint à la commission de régulation de l'énergie de communiquer à l'association requérante l'offre remise par la société Eoliennes en Mer de Vendée ainsi que l'ensemble des documents rédigés par la Commission de régulation de l'énergie dans le cadre du choix de l'entreprise retenue. <br/>
<br/>
              Par une requête, enregistrée le 20 mai 2016 au secrétariat du contentieux du Conseil d'Etat, la société Eoliennes en mer îles d'Yeu et de Noirmoutier, qui était intervenue en défense dans l'instance devant le tribunal administratif, demande au Conseil d'Etat d'ordonner qu'il soit sursis à l'exécution de ce jugement.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi n° 78-573 du 17 juillet 1978 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Vincent Villette, auditeur,  <br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Meier-Bourdeau, Lecuyer, avocat de la société Eoliennes en mer îles d'Yeu et de Noirmoutier ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. La personne qui, devant le tribunal administratif statuant en premier et dernier ressort, est régulièrement intervenue en défense, n'est recevable à se pourvoir en cassation contre le jugement rendu contrairement aux conclusions de son intervention que lorsqu'elle aurait eu qualité, à défaut d'intervention de sa part, pour faire tierce-opposition contre le jugement. Il s'ensuit que la société Eoliennes en mer îles d'Yeu et de Noirmoutier, qui était intervenue en défense dans l'instance devant le tribunal administratif de Paris, est recevable à se pourvoir en cassation contre le jugement du 7 avril 2016 dès lors qu'elle justifie d'un droit auquel ce jugement a préjudicié. En effet, le tribunal a enjoint à la commission de régulation de l'énergie de communiquer à l'association Non aux éoliennes entre Noirmoutier et Yeu des documents qui concernent directement la requérante, laquelle a en outre été privée de la possibilité de faire valoir qu'ils pourraient comporter des secrets protégés par la loi. Dès lors que son pourvoi est recevable, la société requérante est également recevable à demander qu'il soit sursis à l'exécution du jugement. <br/>
<br/>
              2. Aux termes de l'article R. 821-5 du code de justice administrative, la formation de jugement peut, à la demande de l'auteur d'un pourvoi en cassation : " ordonner qu'il soit sursis à l'exécution d'une décision juridictionnelle rendue en dernier ressort si cette décision risque d'entraîner des conséquences difficilement réparables et si les moyens invoqués paraissent, en l'état de l'instruction, sérieux et de nature à justifier, outre l'annulation de la décision juridictionnelle, l'infirmation de la solution retenue par les juges du fond (...) ".<br/>
<br/>
              3. L'exécution du jugement du 7 avril 2016 implique la communication à l'association Non aux éoliennes entre Noirmoutier et Yeu des documents litigieux, dont le refus de communication constitue l'objet même du litige. Cette communication, indépendamment du contenu des documents en cause, revêtirait un caractère irréversible. Dans ces conditions, la condition tenant au risque que le jugement entraîne des conséquences difficilement réparables doit être regardée comme remplie.<br/>
<br/>
              4. Toutefois, aucun des moyens soulevés n'est de nature à justifier, outre l'annulation du jugement du 7 avril 2016 du tribunal administratif de Paris, l'infirmation de la solution retenue par les juges du fond. La seconde des deux conditions posées par l'article R. 821-5 du code de justice administrative n'est donc pas remplie.<br/>
<br/>
              5. Il résulte de ce qui précède que les conclusions de la société Eoliennes en mer îles d'Yeu et de Noirmoutier aux fins de sursis à exécution du jugement du 7 avril 2016 doivent être rejetées.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la société Eoliennes en mer îles d'Yeu et de Noirmoutier est rejetée.<br/>
Article 2 : La présente décision sera notifiée à la société Eoliennes en mer îles d'Yeu et de Noirmoutier, à l'association Non aux éoliennes entre Noirmoutier et Yeu, à la Commission de régulation de l'énergie et à la ministre de l'environnement, de l'énergie et de la mer. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">26-06-01-04 DROITS CIVILS ET INDIVIDUELS. ACCÈS AUX DOCUMENTS ADMINISTRATIFS. ACCÈS AUX DOCUMENTS ADMINISTRATIFS AU TITRE DE LA LOI DU 17 JUILLET 1978. CONTENTIEUX. - CAS OÙ UN JUGEMENT ENJOINT À UNE ADMINISTRATION DE COMMUNIQUER À UN REQUÉRANT DES DOCUMENTS ADMINISTRATIFS - 1) JUGEMENT PRÉJUDICIANT À UN DROIT DE LA PERSONNE, NON MISE EN CAUSE, DIRECTEMENT CONCERNÉE PAR CES DOCUMENTS - QUALITÉ DE CETTE PERSONNE À FAIRE TIERCE OPPOSITION - 2) RISQUE QUE LE JUGEMENT ENTRAÎNE DES CONSÉQUENCES DIFFICILEMENT RÉPARABLES - CONDITION DU SURSIS À EXÉCUTION REMPLIE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-03-06-02 PROCÉDURE. PROCÉDURES DE RÉFÉRÉ AUTRES QUE CELLES INSTITUÉES PAR LA LOI DU 30 JUIN 2000. - CONDITION TENANT AU RISQUE QUE LA DÉCISION ENTRAINE DES CONSÉQUENCES DIFFICILEMENT RÉPARABLES - CONDITION REMPLIE - COMMUNICATION DE DOCUMENTS DONT LE REFUS DE COMMUNICATION CONSTITUE L'OBJET MÊME DU LITIGE, INDÉPENDAMMENT DU CONTENU DES DOCUMENTS.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-08-04-01-01 PROCÉDURE. VOIES DE RECOURS. TIERCE-OPPOSITION. RECEVABILITÉ. NOTION DE DROIT LÉSÉ. - EXISTENCE - PERSONNE, NON MISE EN CAUSE, DIRECTEMENT CONCERNÉE PAR DES DOCUMENTS ADMINISTRATIFS DONT LA COMMUNICATION EST ORDONNÉE.
</SCT>
<ANA ID="9A"> 26-06-01-04 1) Le jugement par lequel un tribunal administratif a enjoint à une administration de communiquer à un requérant des documents qui concernent directement un tiers n'ayant pas été mis en cause, lequel a en outre été privé de la possibilité de faire valoir que les documents pourraient comporter des secrets protégés par la loi, préjudicie à un droit de ce tiers. Celui-ci a donc qualité pour faire tierce opposition contre le jugement.,,,2) La communication à un requérant, en exécution d'un jugement d'un tribunal administratif, de documents dont le refus de communication constitue l'objet même du litige, indépendamment même du contenu des documents en cause, revêtirait un caractère irréversible. Dans ces conditions, la condition du sursis à exécution tenant au risque que le jugement entraîne des conséquences difficilement réparables doit être regardée comme remplie.</ANA>
<ANA ID="9B"> 54-03-06-02 La communication à un requérant, en exécution d'un jugement d'un tribunal administratif, de documents dont le refus de communication constitue l'objet même du litige, indépendamment même du contenu des documents en cause, revêtirait un caractère irréversible. Dans ces conditions, la condition du sursis à exécution tenant au risque que le jugement entraîne des conséquences difficilement réparables doit être regardée comme remplie.</ANA>
<ANA ID="9C"> 54-08-04-01-01 Le jugement par lequel un tribunal administratif a enjoint à une administration de communiquer à un requérant des documents qui concernent directement un tiers n'ayant pas été mis en cause, lequel a en outre été privé de la possibilité de faire valoir que les documents pourraient comporter des secrets protégés par la loi, préjudicie à un droit de ce tiers. Celui-ci a donc qualité pour faire tierce opposition contre le jugement.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
