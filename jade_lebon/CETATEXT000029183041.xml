<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029183041</ID>
<ANCIEN_ID>JG_L_2014_07_000000367179</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/18/30/CETATEXT000029183041.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème / 5ème SSR, 02/07/2014, 367179, Publié au recueil Lebon</TITRE>
<DATE_DEC>2014-07-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>367179</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème / 5ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Louis Dutheillet de Lamothe</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Keller</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:367179.20140702</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu 1°, sous le n° 367179, la requête, enregistrée le 26 mars 2013, présentée par l'association autonome des parents d'élèves de l'école Emile Glay (AAPEEG), dont le siège est 19 rue des Glaises, à Montigny-lès-Cormeilles (95370), représentée par sa présidente ; cette association demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2013-77 du 24 janvier 2013 relatif à l'organisation du temps scolaire dans les écoles maternelles et élémentaires ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 1 000 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
              Vu 2°, sous le n° 367190, la requête sommaire et le mémoire complémentaire, enregistrés les 26 mars et 19 juin 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour la Fédération Sud Education, dont le siège est 17 boulevard de la Libération, à Saint-Denis (93200), représentée par son représentant légal en exercice ; la Fédération Sud Education demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2013-77 du 24 janvier 2013 relatif à l'organisation du temps scolaire dans les écoles maternelles et élémentaires ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu la Constitution ; <br/>
<br/>
              Vu le code de l'éducation ; <br/>
<br/>
              Vu la loi n° 84-16 du 11 janvier 1984 ; <br/>
<br/>
              Vu le décret n° 82-453 du 28 mai 1982 ; <br/>
<br/>
              Vu le décret n° 2011-184 du 15 février 2011 ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Louis Dutheillet de Lamothe, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Rémi Keller, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de la Fédération Sud Education ;<br/>
<br/>
<br/>
<br/>
<br/>Sur la légalité externe du décret attaqué :<br/>
<br/>
              En ce qui concerne la compétence de l'auteur du décret attaqué : <br/>
<br/>
              1. Considérant que l'article 34 de la Constitution prévoit que la loi détermine les principes fondamentaux de l'enseignement, de la libre administration des collectivités territoriales, de leurs compétences et de leurs ressources ; que l'article 37 de la Constitution dispose que : " Les matières autres que celles qui sont du domaine de la loi ont un caractère réglementaire " ; <br/>
<br/>
              2. Considérant que le décret attaqué modifie l'organisation de la semaine scolaire des écoles maternelles et élémentaires, régie par les articles D. 521-10 à D. 521-13 du code de l'éducation ; qu'il prévoit que cette semaine comporte vingt-quatre heures d'enseignement, réparties sur neuf demi-journées et que le conseil d'école, la commune ou l'établissement public de coopération intercommunale peut transmettre un projet d'organisation de la semaine scolaire au directeur académique des services de l'éducation nationale qui est chargé, par délégation du recteur d'académie, d'arrêter l'organisation de la semaine scolaire dans chaque école du département dont il a la charge ; qu'aux termes du deuxième alinéa de l'article D. 521-10 du code de l'éducation, dans sa rédaction issue du décret attaqué : " Les heures d'enseignement sont organisées les lundi, mardi, jeudi et vendredi et le mercredi matin, à raison de cinq heures trente maximum par jour et de trois heures trente maximum par demi-journée " ; que le décret attaqué précise que les élèves peuvent bénéficier d'activités pédagogiques complémentaires, arrêtées par l'inspecteur de l'éducation nationale et destinées à aider les élèves rencontrant des difficultés, à accompagner le travail personnel des élèves ou à les encadrer dans le cadre d'une activité prévue par le projet d'école ; qu'aux termes du deuxième alinéa de l'article D. 521-12 du code de l'éducation, dans sa rédaction issue du décret attaqué : " Le directeur académique des services de l'éducation nationale peut donner son accord à une dérogation aux dispositions du deuxième alinéa de l'article D. 521-10 lorsqu'elle est justifiée par les particularités du projet éducatif territorial et que l'organisation proposée présente des garanties pédagogiques suffisantes " ; <br/>
<br/>
              3. Considérant qu'aucune disposition constitutionnelle ou législative ne rend les collectivités territoriales compétentes pour organiser la répartition hebdomadaire des enseignements scolaires délivrés dans les écoles maternelles et élémentaires ; que, par suite, cette compétence ressortit à l'Etat et le pouvoir réglementaire est, contrairement à ce qui est soutenu, compétent pour fixer l'organisation de la semaine scolaire dans ces écoles, dans le cadre des principes fondamentaux d'organisation des enseignements fixés par le législateur ; que, si l'association requérante soutient également que le décret attaqué créerait incompétemment une charge supplémentaire pour les communes, qui devraient organiser des activités périscolaires, une telle obligation ne résulte pas des dispositions du décret attaqué, qui ne régit pas ces activités et n'est donc pas entaché du vice d'incompétence allégué ; <br/>
<br/>
              En ce qui concerne la forme du décret attaqué : <br/>
<br/>
              4. Considérant que le moyen tiré de ce que le décret attaqué aurait dû être revêtu des contreseings du ministre de l'économie, des finances et du commerce extérieur, du ministre délégué auprès du ministre de l'économie, des finances et du commerce extérieur chargé du budget, du ministre des sports, de la jeunesse, de l'éducation populaire et de la vie associative, et du ministre des affaires sociales et de la santé est dépourvu des précisions permettant d'en apprécier le bien-fondé dès lors que l'association requérante ne précise pas les actes d'application du décret attaqué que ces ministres seraient appelés à prendre ; <br/>
<br/>
              En ce qui concerne la procédure d'édiction du décret attaqué : <br/>
<br/>
              5. Considérant, en premier lieu, que le décret attaqué, qui modifie ou abroge les articles D. 521-10 à D. 521-15 du code de l'éducation, n'avait pas, contrairement à ce qui est soutenu, à être précédé, à ce titre, de la consultation du Conseil d'Etat dès lors que, si ces articles ont été créés par le décret en Conseil d'Etat n° 2009-553 du 15 mai 2009 relatif aux dispositions réglementaires du livre V du code de l'éducation, l'article 1er de ce décret prévoit que les articles qu'il identifie par un " D " dans le code de l'éducation peuvent être modifiés par décret ; <br/>
<br/>
              6. Considérant que, de même, le décret attaqué n'avait pas à être précédé de la consultation du Conseil d'Etat pour modifier l'article D. 411-2 du code de l'éducation dès lors que cet article a été créé par le décret en Conseil d'Etat n° 2008-263 du 14 mars 2008 relatif aux dispositions réglementaires du livre IV du code de l'éducation, dont l'article 1er prévoit que : " Les articles identifiés par un " R " correspondent aux dispositions relevant d'un décret en Conseil d'Etat, ceux identifiés par un " D " correspondent aux dispositions relevant d'un décret " ; que, contrairement à ce qui est soutenu, la modification d'un article identifié par un " D " par un décret en Conseil d'Etat ne fait pas obstacle à la possibilité de modifier ultérieurement cet article par décret, sauf dans l'hypothèse où un décret en Conseil d'Etat a entendu mettre fin à cette possibilité en identifiant l'article en cause par un " R " ; que, dès lors, le fait que l'article D. 411-2 du code de l'éducation ait été modifié par un décret en Conseil d'Etat avant sa modification par le décret attaqué ne faisait pas obstacle à ce que cet article soit modifié par le décret attaqué sans qu'ait été consulté le Conseil d'Etat, dès lors que cet article demeure identifié en " D " dans le code de l'éducation ; <br/>
<br/>
              7. Considérant, en second lieu, que les articles 15 et 16 de la loi du 11 janvier 1984 prévoient, respectivement et dans leurs rédactions issues des lois n° 2013-1168 du 18 décembre 2013 et n° 2010-751 du 5 juillet 2010, que, dans toutes les administrations de l'Etat et dans les établissements publics ne présentant pas un caractère industriel et commercial, les comités techniques " connaissent des questions relatives à l'organisation et au fonctionnement des services " et les comités d'hygiène de sécurité et des conditions de travail ont " pour mission de contribuer à la protection de la santé physique et mentale et de la sécurité des agents dans leur travail, à l'amélioration des conditions de travail et de veiller à l'observation des prescriptions légales prises en ces matières " ; <br/>
<br/>
              8. Considérant que l'article 34 du décret du 15 février 2011, pris pour l'application de l'article 15 de la loi du 11 janvier 1984, énumère les questions et projets de textes sur lesquels les comités techniques sont obligatoirement consultés, qui incluent ceux relatifs à l'organisation et au fonctionnement des administrations, établissements ou services ; qu'en vertu du même article, d'une part, les comités techniques ne sont consultés sur les questions relatives à l'hygiène, à la sécurité et aux conditions de travail que lorsqu'aucun comité d'hygiène de sécurité et de conditions de travail n'est placé auprès d'eux, d'autre part, les comités techniques bénéficient du concours du comité d'hygiène, peuvent le saisir de toute question et examinent toute question que lui soumet le comité d'hygiène ; <br/>
<br/>
              9. Considérant que l'article 47 du décret du 28 mai 1982 pris pour l'application de l'article 16 de la loi du 11 janvier 1984, précise, dans sa rédaction issue du décret n° 2011-774 du 28 juin 2011, que les comités d'hygiène, de sécurité et des conditions de travail exercent leurs missions " sous réserve des compétences des comités techniques " ; que le 1° de l'article 57 du même décret prévoit que le comité d'hygiène, de sécurité et des conditions de travail est notamment consulté " sur les projets d'aménagement importants modifiant les conditions de santé et de sécurité ou les conditions de travail (...) " ; <br/>
<br/>
              10. Considérant qu'il résulte de ces dispositions qu'une question ou un projet de disposition ne doit être soumis à la consultation du comité d'hygiène, de sécurité et des conditions de travail que si le comité technique ne doit pas lui-même être consulté sur la question ou le projet de disposition en cause ; que le comité d'hygiène, de sécurité et des conditions de travail ne doit ainsi être saisi que d'une question ou projet de disposition concernant exclusivement la santé, la sécurité ou les conditions de travail ; qu'en revanche, lorsqu'une question ou un projet de disposition concerne ces matières et l'une des matières énumérées à l'article 34 du décret du 15 février 2011, seul le comité technique doit être obligatoirement consulté ; que, ce comité peut, le cas échéant, saisir le comité d'hygiène, de sécurité et des conditions de travail de toute question qu'il juge utile de lui soumettre ; qu'en outre, l'administration a toujours la faculté de consulter le comité d'hygiène, de sécurité et des conditions de travail ; <br/>
<br/>
              11. Considérant qu'en l'espèce, le décret attaqué modifie l'organisation du service de l'enseignement dans les écoles maternelles et élémentaires et a été soumis pour avis au comité technique ministériel de l'éducation nationale ; que, si la modification de la semaine scolaire dans ces écoles a des incidences sur les conditions de travail des agents qui y sont affectés, cette circonstance n'impliquait pas, en vertu de ce qui vient d'être dit, que le comité d'hygiène, de sécurité et des conditions de travail fût consulté avant son édiction ; <br/>
<br/>
              Sur la légalité interne du décret attaqué : <br/>
<br/>
              12. Considérant, en premier lieu, que l'association requérante soutient que le décret attaqué méconnaît le principe d'égal accès à l'éducation en permettant qu'il soit dérogé aux dispositions précitées du deuxième alinéa de l'article D. 521-10 du code de l'éducation, dans sa rédaction issue du décret attaquée ; que, toutefois, cette dérogation ne peut intervenir, en vertu de l'article D. 521-12 du code de l'éducation, dans sa rédaction issue du décret attaquée que " lorsqu'elle est justifiée par les particularités du projet éducatif territorial et que l'organisation proposées présente des garanties pédagogiques suffisantes " ; que, dès lors, la disposition critiquée n'autorise la dérogation que lorsque les particularités liées à l'école et à son projet éducatif la justifient et ne méconnaît pas le principe constitutionnel d'égalité ; <br/>
<br/>
              13. Considérant, en deuxième lieu, que, dès lors, ainsi qu'il a été dit au point 3, que le décret attaqué ne crée pas une obligation pour les collectivités territoriales d'organiser des activités périscolaires complétant la journée de travail des élèves, les moyens tirés de ce que cette obligation méconnaîtrait le principe constitutionnel d'égalité, le principe d'indivisibilité de la République française et le principe de libre administration des collectivités territoriales ne peuvent qu'être rejetés ; que les moyens tirés du défaut de clarté, d'accessibilité et d'intelligibilité soulevés par l'association requérante sont également dirigés contre les dispositions du décret, qui créerait une telle obligation, et ne peuvent également qu'être rejetés ;  <br/>
<br/>
              14. Considérant, en troisième lieu, qu'ainsi qu'il a été dit au point 3, l'Etat étant compétent pour fixer l'organisation de la semaine scolaire dans les écoles maternelles et élémentaires, le décret attaqué pouvait sans porter atteinte à la libre administration des collectivités territoriales prévoir que les dérogations aux règles d'organisation qu'il fixe sont arrêtées par le directeur académique des services de l'éducation nationale et non par la commune d'implantation de l'école ; <br/>
<br/>
              15. Considérant, en quatrième lieu, que, dès lors que les dispositions édictées par le décret attaqué ressortissaient à la compétence du pouvoir réglementaire à la date de leur édiction, le moyen tiré de ce que l'auteur du décret attaqué aurait commis une erreur de droit faute d'avoir attendu la promulgation de la loi du 8 juillet 2013 dont le décret attaqué constituerait un acte d'application ne peut qu'être écarté ; <br/>
<br/>
              16. Considérant, en dernier lieu, que le détournement de pouvoir, tiré de ce que l'édiction du décret attaquée aurait pour fin de décharger l'Etat de certaines dépenses et de sa responsabilité d'encadrement des élèves, n'est pas établi ; <br/>
<br/>
              17. Considérant qu'il résulte de tout ce qui précède que les deux requêtes doivent être rejetées, y compris leurs conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : Les requêtes de l'association autonome des parents d'élèves de l'école Emile Glay et de la Fédération Sud Education sont rejetées. <br/>
Article 2 : La présente décision sera notifiée à l'association autonome des parents d'élèves de l'école Emile Glay, à la Fédération Sud Education, au Premier ministre et au ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-03-02-02-01-01-02 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - FORME ET PROCÉDURE. PROCÉDURE CONSULTATIVE. CONSULTATION OBLIGATOIRE. - MODIFICATION D'UN ARTICLE CODIFIÉ EN  D  CRÉÉ OU MODIFIÉ PAR UN DÉCRET EN CONSEIL D'ETAT.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">36-07-06-03 FONCTIONNAIRES ET AGENTS PUBLICS. STATUTS, DROITS, OBLIGATIONS ET GARANTIES. COMITÉS TECHNIQUES PARITAIRES. CONSULTATION OBLIGATOIRE. - EFFET - ABSENCE D'OBLIGATION DE CONSULTER LE CHSCT.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">36-07-065 FONCTIONNAIRES ET AGENTS PUBLICS. STATUTS, DROITS, OBLIGATIONS ET GARANTIES. COMITÉS D'HYGIÈNE ET DE SÉCURITÉ. - CONSULTATION OBLIGATOIRE DU CHSCT - CONDITIONS - INCLUSION - QUESTION OU PROJET CONCERNANT EXCLUSIVEMENT LA SANTÉ, LA SÉCURITÉ OU LES CONDITIONS DE TRAVAIL ET NE DEVANT DONC PAS, PAR AILLEURS, ÊTRE SOUMIS POUR AVIS AU COMITÉ TECHNIQUE.
</SCT>
<ANA ID="9A"> 01-03-02-02-01-01-02 La modification d'un article identifié par un  D  par un décret en Conseil d'Etat ne fait pas obstacle à la possibilité de modifier ultérieurement cet article par décret, sauf dans l'hypothèse où un décret en Conseil d'Etat a entre-temps entendu mettre fin à cette possibilité en identifiant l'article en cause par un  R .</ANA>
<ANA ID="9B"> 36-07-06-03 Il résulte des articles 15 et 16 de la loi n° 84-16 du 11 janvier 1984, des articles 47 et 57 du décret n° 82-453 du 28 mai 1982 et de l'article 34 du décret n° 2011-184 du 15 février 2011 qu'une question ou un projet de disposition ne doit être soumis à la consultation du comité d'hygiène, de sécurité et des conditions de travail (CHSCT) que si le comité technique ne doit pas lui-même être consulté sur la question ou le projet de disposition en cause.... ,,Le CHSCT ne doit ainsi être saisi que d'une question ou d'un projet de disposition concernant exclusivement la santé, la sécurité ou les conditions de travail. En revanche, lorsqu'une question ou un projet de disposition concerne ces matières et l'une des matières énumérées à l'article 34 du décret du 15 février 2011, seul le comité technique doit être obligatoirement consulté. Ce comité peut, le cas échéant, saisir le CHSCT de toute question qu'il juge utile de lui soumettre. En outre, l'administration a toujours la faculté de consulter le CHSCT.</ANA>
<ANA ID="9C"> 36-07-065 Il résulte des articles 15 et 16 de la loi n° 84-16 du 11 janvier 1984, des articles 47 et 57 du décret n° 82-453 du 28 mai 1982 et de l'article 34 du décret n° 2011-184 du 15 février 2011 qu'une question ou un projet de disposition ne doit être soumis à la consultation du comité d'hygiène, de sécurité et des conditions de travail (CHSCT) que si le comité technique ne doit pas lui-même être consulté sur la question ou le projet de disposition en cause.... ,,Le CHSCT ne doit ainsi être saisi que d'une question ou d'un projet de disposition concernant exclusivement la santé, la sécurité ou les conditions de travail. En revanche, lorsqu'une question ou un projet de disposition concerne ces matières et l'une des matières énumérées à l'article 34 du décret du 15 février 2011, seul le comité technique doit être obligatoirement consulté. Ce comité peut, le cas échéant, saisir le CHSCT de toute question qu'il juge utile de lui soumettre. En outre, l'administration a toujours la faculté de consulter le CHSCT.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
