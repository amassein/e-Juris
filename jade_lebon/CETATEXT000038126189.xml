<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038126189</ID>
<ANCIEN_ID>JG_L_2019_01_000000424258</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/12/61/CETATEXT000038126189.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 25/01/2019, 424258</TITRE>
<DATE_DEC>2019-01-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>424258</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DIDIER, PINET ; SCP MEIER-BOURDEAU, LECUYER</AVOCATS>
<RAPPORTEUR>Mme Cécile Renault</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:424258.20190125</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B...A...a demandé au juge des référés du tribunal administratif de Bordeaux d'ordonner, sur le fondement de l'article L. 521-1 du code de justice administrative, la suspension de l'exécution de la décision du 4 juillet 2018 par laquelle le président-directeur général du Centre national de la recherche scientifique a décidé sa mutation dans l'intérêt du service à la fédération des sciences archéologiques de Bordeaux à compter du 1er septembre 2018. <br/>
<br/>
              Par une ordonnance n° 1803600 du 31 août 2018, le juge des référés du tribunal administratif de Bordeaux a rejeté cette demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 17 septembre et 2 octobre 2018 au secrétariat du contentieux du Conseil d'Etat, Mme A...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette ordonnance ; <br/>
<br/>
              2°) de mettre à la charge du Centre national de la recherche scientifique la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code des relations entre le public et l'administration ;<br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Cécile Renault, auditeur,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Didier, Pinet, avocat de Mme A...et à la SCP Meier-Bourdeau, Lecuyer, avocat du Centre national de la recherche scientifique.<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge des référés du tribunal administratif de Bordeaux que MmeA..., assistante ingénieure du Centre national de la recherche scientifique (CNRS), était affectée au Centre Jacques Berque pour les études en sciences humaines et sociales à Rabat au Maroc, depuis le 1er mai 2010. Le 4 juillet 2018, le président-directeur général du CNRS a décidé sa mutation dans l'intérêt du service à la fédération des sciences archéologiques de Bordeaux à compter du 1er septembre 2018. Par l'ordonnance attaquée, le juge des référés du tribunal administratif de Bordeaux, saisi sur le fondement de l'article L. 521-1 du code de justice administrative, a rejeté la demande de l'intéressée tendant à la suspension de l'exécution de cette décision. <br/>
<br/>
              Sur la régularité de l'ordonnance attaquée :<br/>
<br/>
              2. Aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". Aux termes de l'article L. 522-1 du même code : " Le juge des référés statue au terme d'une procédure contradictoire écrite ou orale. Lorsqu'il lui est demandé de prononcer les mesures visées aux articles L. 521-1 et L. 521-2, de les modifier ou d'y mettre fin, il informe sans délai les parties de la date et de l'heure de l'audience publique. (...) ". Aux termes de l'article R. 522-6 du même code : " Lorsque le juge des référés est saisi d'une demande fondée sur les dispositions de l'article L. 521-1 (...), les parties sont convoquées sans délai et par tous moyens à l'audience ". Aux termes du deuxième alinéa de l'article R. 711-2 du même code : " L'avis d'audience reproduit les dispositions des articles R. 731-3 et R. 732-1-1 (...) ". Aux termes de l'article R. 731-3 du code : " A l'issue de l'audience, toute partie à l'instance peut adresser au président de la formation de jugement une note en délibéré ".<br/>
<br/>
              3. Il résulte de ces dispositions qu'il appartient au juge des référés saisi sur le fondement des dispositions de l'article L. 521-1 du code de justice administrative, compte tenu des caractéristiques de cette procédure, d'aviser les parties de la date de l'audience par tous moyens utiles, sans que s'appliquent les règles fixées par l'article R. 711-2 du code de justice administrative. Par suite, le moyen tiré de ce que l'ordonnance attaquée aurait été rendue à l'issue d'une procédure irrégulière au motif que l'avis d'audience qu'elle a reçu ne reproduisait pas les dispositions de l'article R. 731-3 du code de justice administrative et aurait ainsi méconnu les dispositions du deuxième alinéa de l'article R. 711-2 du même code ne peut qu'être écarté.<br/>
<br/>
              Sur le bien-fondé de l'ordonnance attaquée :<br/>
<br/>
              4. En jugeant que n'étaient pas de nature à faire naître un doute sérieux quant à la légalité de la décision attaquée les moyens tirés, d'une part, de l'insuffisante motivation de celle-ci et, d'autre part, de la méconnaissance des dispositions de l'article 6 quinquies de la loi du 13 juillet 1983 portant droits et obligations des fonctionnaires, le juge des référés n'a pas commis d'erreur de droit et s'est livré à une appréciation souveraine des faits exempte de dénaturation.<br/>
<br/>
              5. Il résulte de ce qui précède que Mme A...n'est pas fondée à demander l'annulation de l'ordonnance qu'elle attaque.<br/>
<br/>
              Sur les conclusions relatives aux frais d'instance :<br/>
<br/>
              6. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de Mme A...le versement d'une somme au CNRS au titre de l'article L. 761-1 du code de justice administrative. Ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge du CNRS qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de Mme A...est rejeté.<br/>
Article 2 : Les conclusions présentées par le CNRS au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée Mme B...A...et au Centre national de la recherche scientifique.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-035-02-04 PROCÉDURE. PROCÉDURES INSTITUÉES PAR LA LOI DU 30 JUIN 2000. RÉFÉRÉ SUSPENSION (ART. L. 521-1 DU CODE DE JUSTICE ADMINISTRATIVE). POUVOIRS ET DEVOIRS DU JUGE. - OBLIGATION D'AVISER LES PARTIES DE LA DATE DE L'AUDIENCE PAR TOUS MOYENS UTILES - APPLICATION DE L'ARTICLE R. 711-2 DU CJA - ABSENCE [RJ1] - CONSÉQUENCE - AVIS D'AUDIENCE NE REPRODUISANT PAS L'ARTICLE R. 731-3 DU CJA - IRRÉGULARITÉ - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-06-02-01 PROCÉDURE. JUGEMENTS. TENUE DES AUDIENCES. AVIS D'AUDIENCE. - RÉFÉRÉ-SUSPENSION - OBLIGATION D'AVISER LES PARTIES DE LA DATE DE L'AUDIENCE PAR TOUS MOYENS UTILES - APPLICATION DE L'ARTICLE R. 711-2 DU CJA - ABSENCE [RJ1] - CONSÉQUENCE - AVIS D'AUDIENCE NE REPRODUISANT PAS L'ARTICLE R. 731-3 DU CJA - IRRÉGULARITÉ - ABSENCE.
</SCT>
<ANA ID="9A"> 54-035-02-04 Il résulte des articles L. 521-1, L. 522-1, R. 522-6, R. 711-2 et R. 731-3 du code de justice administrative (CJA) qu'il appartient au juge des référés saisi sur le fondement de l'article L. 521-1 de ce code, compte tenu des caractéristiques de cette procédure, d'aviser les parties de la date de l'audience par tous moyens utiles, sans que s'appliquent les règles fixées par l'article R. 711-2 de ce code.... ...Par suite, un moyen tiré de ce qu'une ordonnance aurait été rendue à l'issue d'une procédure irrégulière au motif que l'avis d'audience ne reproduisait pas l'article R. 731-3 du CJA, et aurait ainsi méconnu le deuxième alinéa de l'article R. 711-2 du même code, ne peut qu'être écarté.</ANA>
<ANA ID="9B"> 54-06-02-01 Il résulte des articles L. 521-1, L. 522-1, R. 522-6, R. 711-2 et R. 731-3 du code de justice administrative (CJA) qu'il appartient au juge des référés saisi sur le fondement de l'article L. 521-1 de ce code, compte tenu des caractéristiques de cette procédure, d'aviser les parties de la date de l'audience par tous moyens utiles, sans que s'appliquent les règles fixées par l'article R. 711-2 de ce code.... ...Par suite, un moyen tiré de ce qu'une ordonnance aurait été rendue à l'issue d'une procédure irrégulière au motif que l'avis d'audience ne reproduisait pas l'article R. 731-3 du CJA, et aurait ainsi méconnu le deuxième alinéa de l'article R. 711-2 du même code, ne peut qu'être écarté.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., s'agissant du référé mesures utiles (art. L. 521-3 du CJA), CE, 15 mars 2004, Société Dauphin Adshel, n° 259803, T. pp. 829-840-853.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
