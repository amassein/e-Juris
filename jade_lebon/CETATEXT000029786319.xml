<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029786319</ID>
<ANCIEN_ID>JG_L_2014_11_000000384353</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/78/63/CETATEXT000029786319.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème - 10ème SSR, 21/11/2014, 384353</TITRE>
<DATE_DEC>2014-11-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>384353</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème - 10ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO ; SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR>M. Laurent Olléon</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Aladjidi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2014:384353.20141121</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Mutuelle des Transports Assurances a demandé au Conseil d'Etat d'annuler la décision du 10 juillet 2014 par laquelle l'Autorité de contrôle prudentiel et de résolution a engagé à son encontre la procédure de transfert d'office de son portefeuille de contrats, bulletins ou adhésions.<br/>
<br/>
              A l'appui de sa requête elle a, en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958, demandé au Conseil d'Etat, par un mémoire distinct et un mémoire en réplique enregistrés les 9 septembre et 22 octobre 2014, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des dispositions du 8° du I de l'article L. 612-33 du code monétaire et financier. Elle soutient que ces dispositions, applicables au litige, portent atteinte aux principes d'égalité devant les charges publiques, garanti par l'article 13 de la Déclaration des droits de l'homme et du citoyen, de liberté contractuelle et de liberté d'entreprendre, garantis par son article 4, et au droit de propriété, garanti par son article 17.<br/>
<br/>
              Par deux mémoires en défense, enregistrés les 25 septembre et 3 novembre 2014, l'Autorité de contrôle prudentiel et de résolution conclut qu'il n'y a pas lieu de transmettre au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée et à ce que la somme de 3 000 euros soit mise à la charge de la société requérante au titre de l'article L. 761-1 du code de justice administrative. Elle soutient, en premier lieu, que la question prioritaire de constitutionnalité est irrecevable, dès lors qu'elle est soulevée à l'appui d'une requête elle-même irrecevable, car formée contre une décision portant ouverture d'une procédure de transfert d'office de portefeuilles, laquelle présente un caractère préparatoire et est, par suite, insusceptible de recours. Elle soutient, en second lieu, que la question soulevée ne présente aucun caractère sérieux dès lors, d'une part, que le transfert litigieux n'implique aucune privation du droit de propriété au sens de l'article 17 de la Déclaration du 26 août 1789 et que, d'autre part, les atteintes aux droits et libertés qui sont  invoquées sont justifiées par un objectif d'intérêt général et proportionnées à celui-ci, compte tenu des garanties qui entourent la procédure de transfert d'office. <br/>
<br/>
<br/>
<br/>
              Vu :<br/>
              - les autres pièces du dossier ;<br/>
              - la Constitution, notamment son Préambule et son article 61-1 ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code monétaire et financier, notamment son article L. 612-33 ;<br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Laurent Olléon, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Frédéric Aladjidi, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Spinosi, Sureau, avocat de la société Mutuelle des Transports Assurances et à la SCP Rocheteau, Uzan-Sarano, avocat de l'Autorité de contrôle prudentiel et de résolution ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 14 novembre 2014, présentée pour l'Autorité de contrôle prudentiel et de résolution.<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé, y compris pour la première fois en cassation, à l'occasion d'une instance devant le Conseil d'Etat (...) " ; qu'il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et qu'elle soit nouvelle ou présente un caractère sérieux ; <br/>
<br/>
              Sur la fin de non recevoir opposée par l'Autorité de contrôle prudentiel et de résolution :<br/>
<br/>
              2. Considérant que l'Autorité de contrôle prudentiel et de résolution soutient que la question prioritaire de constitutionnalité soulevée par la société requérante est irrecevable, au motif qu'elle est présentée à l'appui d'un recours pour excès de pouvoir lui-même irrecevable, car dirigé contre une décision préparatoire insusceptible de recours ; que, toutefois, le Conseil d'Etat n'est pas tenu, lorsqu'à l'appui d'une requête est soulevée devant lui une question prioritaire de constitutionnalité, sur laquelle il lui incombe de se prononcer dans un délai de trois mois, de statuer au préalable sur la recevabilité de cette requête ; que, par suite, la fin de non recevoir opposée par l'Autorité de contrôle prudentiel et de résolution doit être rejetée ;<br/>
<br/>
              Sur la question prioritaire de constitutionnalité :<br/>
<br/>
              3. Considérant qu'aux termes de l'article L. 612-33 du code monétaire et financier : " I. - Lorsque la solvabilité ou la liquidité d'une personne soumise au contrôle de l'Autorité ou lorsque les intérêts de ses clients, assurés, adhérents ou bénéficiaires, sont compromis ou susceptibles de l'être (...), l'Autorité de contrôle prudentiel et de résolution prend les mesures conservatoires nécessaires. / Elle peut, à ce titre :(...) 8° Prononcer le transfert d'office de tout ou partie du portefeuille des contrats d'assurance ou de règlements ou de bulletins d'adhésion à des contrats ou règlements des personnes mentionnées aux 1°, 3° et 5° du B du I de l'article L. 612-2 ainsi que tout ou partie d'un portefeuille de crédits ou de dépôts d'un établissement de crédit ; " ; <br/>
<br/>
              4. Considérant que le 8° du I de l'article L. 612-33 du code monétaire et financier est applicable au présent litige au sens et pour l'application de l'article 23-5 de l'ordonnance du 7 novembre 1958 ; que cette disposition n'a pas déjà été déclarée conforme à la Constitution par le Conseil constitutionnel ; que le moyen tiré de ce qu'elle porte atteinte aux droits et libertés garantis par la Constitution, notamment à la liberté d'entreprendre, à la liberté contractuelle et au droit de propriété garantis par la Déclaration des droits de l'homme et du citoyen soulève une question présentant un caractère sérieux ; qu'ainsi, il y a lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée ;<br/>
<br/>
              Sur les conclusions de l'Autorité de contrôle prudentiel et de résolution présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              5. Considérant que ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de la société Mutuelle des Transports Assurances qui n'est pas, dans la présente instance, la partie perdante ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : La question de la conformité à la Constitution des dispositions du 8° du I de l'article L. 612-33 du code monétaire et financier est renvoyée au Conseil constitutionnel.<br/>
Article 2 : Il est sursis à statuer sur la requête de la société Mutuelle des Transports Assurances jusqu'à ce que le Conseil constitutionnel ait tranché la question de constitutionnalité ainsi soulevée. <br/>
Article 3 : Les conclusions présentées par l'Autorité de contrôle prudentiel et de résolution au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à la société Mutuelle des Transports Assurances et à l'Autorité de contrôle prudentiel et de résolution. <br/>
Copie en sera adressée au Premier ministre et au ministre des finances et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-10-05 PROCÉDURE. - OBLIGATION DE STATUER SUR LA RECEVABILITÉ DE LA REQUÊTE AVANT DE SE PRONONCER SUR LA QPC PRÉSENTÉE À SON SOUTIEN - ABSENCE [RJ1].
</SCT>
<ANA ID="9A"> 54-10-05 Le Conseil d'Etat n'est pas tenu, lorsqu'à l'appui d'une requête est soulevée devant lui une question prioritaire de constitutionnalité (QPC), sur laquelle il lui incombe de se prononcer dans un délai de trois mois, de statuer au préalable sur la recevabilité de cette requête.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 28 septembre 2011, Société Alsass et autres, n° 349820, T. pp. 786-790-1063-1114.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
