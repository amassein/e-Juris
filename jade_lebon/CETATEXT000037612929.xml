<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037612929</ID>
<ANCIEN_ID>JG_L_2018_11_000000409936</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/61/29/CETATEXT000037612929.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 14/11/2018, 409936</TITRE>
<DATE_DEC>2018-11-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>409936</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Jacques Reiller</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Anne Iljic</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:409936.20181114</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
              Le syndicat Action et Démocratie a demandé au tribunal administratif de Montpellier, d'une part, d'annuler pour excès de pouvoir la décision implicite par laquelle la ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche a confirmé son refus de lui communiquer la liste des personnels déchargés de tout ou partie de leur service au titre de l'enveloppe des décharges de service attribuée à l'organisation syndicale Confédération syndicale de l'Éducation nationale-Fédération générale autonome des fonctionnaires (CSEN-FGAF) pour l'année scolaire 2014-2015, d'autre part de condamner l'Etat à lui verser la somme de 15 000 euros au titre des dommages et intérêts, assortis des intérêts moratoires.<br/>
<br/>
              Par un jugement n° 1612086/5-3 du 22 février 2017, le tribunal administratif de Paris, saisi de cette requête par une ordonnance n° 1501147 du 28 juillet 2016 du président du tribunal administratif de Montpellier, a annulé la décision attaquée et rejeté le surplus des conclusions de la requête. <br/>
<br/>
              Par un pourvoi, enregistré au secrétariat du contentieux du Conseil d'Etat le 21 avril 2017, la ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche demande au Conseil d'Etat d'annuler l'article 1er de ce jugement du tribunal administratif de Paris.<br/>
<br/>
<br/>
<br/>
              Vu  les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la loi n° 78-753 du 17 juillet 1978 ;<br/>
              - le décret n° 82-447 du 28 mai 1982 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jacques Reiller, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Anne Iljic, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. La ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche se pourvoit en cassation contre le jugement du 22 février 2017 en tant que, par ce jugement, le tribunal administratif de Paris a annulé son refus de communiquer au syndicat Action et Démocratie la liste des personnels déchargés de tout ou partie de leur service au titre de l'enveloppe des décharges de service attribuée à l'organisation syndicale Confédération syndicale de l'Éducation nationale-Fédération générale autonome des fonctionnaires (CSEN-FGAF) pour l'année scolaire 2014-2015.<br/>
<br/>
              2. En premier lieu, le premier alinéa de l'article 2 de la loi du 17 juillet 1978 portant diverses mesures d'amélioration des relations entre l'administration et le public et diverses dispositions d'ordre administratif, social et fiscal, aujourd'hui repris à l'article L. 311-1 du code des relations entre le public et l'administration, dispose que : " Sous réserve des dispositions de l'article 6, les autorités mentionnées à l'article 1er sont tenues de communiquer les documents administratifs qu'elles détiennent aux personnes qui en font la demande (...) ". Aux termes de l'article 6 de cette loi, repris à l'article L. 311-6 du même code : " II.- Ne sont communicables qu'à l'intéressé les documents administratifs : -dont la communication porterait atteinte à la protection de la vie privée (...). ".<br/>
<br/>
              3. En second lieu, aux termes du deuxième alinéa de l'article 11 du décret du 28 mai 1982 relatif à l'exercice du droit syndical dans la fonction publique : " Des autorisations spéciales d'absence ou des décharges d'activité de service peuvent être accordées, dans les conditions définies aux articles 13, 15 et 16 ci-après, aux agents chargés d'un mandat syndical afin de leur permettre de remplir les obligations résultant de ce mandat. ". Et aux termes de l'article 16 du même décret : " I.- Un crédit de temps syndical, utilisable sous forme de décharges de service ou de crédits d'heure selon les besoins de l'activité syndicale, est déterminé, au sein de chaque département ministériel, à l'issue du renouvellement  général des comités techniques. (...) / VI. - Les organisations syndicales désignent librement parmi leurs représentants les bénéficiaires de crédits de temps syndical./  Les décharges de service sont exprimées sous forme d'une quotité annuelle de temps de travail. Les crédits d'heures sont utilisés sous forme d'autorisations d'absence d'une demi-journée minimum. La liste nominative des bénéficiaires des crédits de temps syndical sollicités sous forme de décharges d'activité de service est communiquée par les organisations syndicales concernées au ministre ou au chef de service intéressé. Est par ailleurs mentionnée la part des crédits de temps syndical destinée à être utilisée sous forme de crédits d'heures ".<br/>
<br/>
              4. Il ressort de ces dispositions que les organisations syndicales ne peuvent désigner comme bénéficiaires de crédits de temps syndical sous forme de décharges d'activité de service, que des agents qui, titulaires d'un mandat syndical, se sont déjà portés volontaires pour assumer publiquement des responsabilités dans l'intérêt des organisations auxquelles ils adhèrent. Dans ces conditions, les exigences de la protection de la vie privée que garantit la loi du 17 juillet 1978 ne sauraient faire obstacle à ce que la liste nominative de ces bénéficiaires, dont l'appartenance syndicale est publique, soit considérée comme un document administratif communicable au sens du code des relations entre le public et l'administration. Il résulte de ce qui précède que la ministre n'était donc pas fondée à demander l'annulation du jugement par lequel le tribunal administratif de Paris a annulé sa décision de refus de communiquer au syndicat Action et Démocratie la liste des personnels déchargés de tout ou partie de leur service au titre de l'enveloppe des décharges de service attribuée à l'organisation syndicale CSEN-FGAF pour l'année scolaire 2014-2015. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le pourvoi de la ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche est rejeté. <br/>
Article 2 : La présente décision sera notifiée au ministre de l'éducation nationale et de la jeunesse et au syndicat Action et Démocratie.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">26-06-01-02-02 DROITS CIVILS ET INDIVIDUELS. ACCÈS AUX DOCUMENTS ADMINISTRATIFS. ACCÈS AUX DOCUMENTS ADMINISTRATIFS AU TITRE DE LA LOI DU 17 JUILLET 1978. DROIT À LA COMMUNICATION. DOCUMENTS ADMINISTRATIFS COMMUNICABLES. - LISTE NOMINATIVE DES BÉNÉFICIAIRES DE CRÉDITS DE TEMPS SYNDICAL SOUS FORME DE DÉCHARGES D'ACTIVITÉ DE SERVICE (2E AL. DE L'ART. 11 ET ART. 16 DU DÉCRET DU 28 MAI 1982) - INCLUSION.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">36-07-09 FONCTIONNAIRES ET AGENTS PUBLICS. STATUTS, DROITS, OBLIGATIONS ET GARANTIES. DROIT SYNDICAL. - CRÉDITS DE TEMPS SYNDICAL SOUS FORME DE DÉCHARGES D'ACTIVITÉ DE SERVICE (2E AL. DE L'ART. 11 ET ART. 16 DU DÉCRET DU 28 MAI 1982) - LISTE NOMINATIVE DES BÉNÉFICIAIRES - DOCUMENT ADMINISTRATIF COMMUNICABLE AU SENS DU CRPA - EXISTENCE.
</SCT>
<ANA ID="9A"> 26-06-01-02-02 Il ressort du deuxième alinéa de l'article 11 et de l'article 16 du décret n° 82-447 du 28 mai 1982 que les organisations syndicales ne peuvent désigner comme bénéficiaires de crédits de temps syndical sous forme de décharges d'activité de service, que des agents qui, titulaires d'un mandat syndical, se sont déjà portés volontaires pour assumer publiquement des responsabilités dans l'intérêt des organisations auxquelles ils adhèrent.... ...Dans ces conditions, les exigences de la protection de la vie privée que garantit la loi n° 78-753 du 17 juillet 1978, reprise sur ce point à l'article L. 311-6 du code des relations entre le public et l'administration (CRPA), ne sauraient faire obstacle à ce que la liste nominative de ces bénéficiaires, dont l'appartenance syndicale est publique, soit considérée comme un document administratif communicable.</ANA>
<ANA ID="9B"> 36-07-09 Il ressort du deuxième alinéa de l'article 11 et de l'article 16 du décret n° 82-447 du 28 mai 1982 que les organisations syndicales ne peuvent désigner comme bénéficiaires de crédits de temps syndical sous forme de décharges d'activité de service, que des agents qui, titulaires d'un mandat syndical, se sont déjà portés volontaires pour assumer publiquement des responsabilités dans l'intérêt des organisations auxquelles ils adhèrent.... ...Dans ces conditions, les exigences de la protection de la vie privée que garantit la loi n° 78-753 du 17 juillet 1978, reprise sur ce point à l'article L. 311-6 du code des relations entre le public et l'administration (CRPA), ne sauraient faire obstacle à ce que la liste nominative de ces bénéficiaires, dont l'appartenance syndicale est publique, soit considérée comme un document administratif communicable au sens du CRPA.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
