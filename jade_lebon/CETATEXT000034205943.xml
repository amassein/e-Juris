<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034205943</ID>
<ANCIEN_ID>JG_L_2017_03_000000387060</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/20/59/CETATEXT000034205943.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 5ème chambres réunies, 15/03/2017, 387060</TITRE>
<DATE_DEC>2017-03-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>387060</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>Mme Pauline Pannier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:387060.20170315</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 12 janvier 2015, 9 mars 2015 et 3 février 2017 au secrétariat du contentieux du Conseil d'Etat, le syndicat professionnel Armateurs de France demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté du 3 novembre 2014 du ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social et du ministre de l'écologie, du développement durable et de l'énergie portant extension de la convention collective des personnels navigants officiers des entreprises de transport et services maritimes, en tant qu'il émet deux réserves relatives, d'une part à la période d'essai et, d'autre part, aux modalités de calcul de l'indemnité de licenciement ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat une somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code civil ;<br/>
              - le code des transports ;<br/>
              - le code du travail ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Pauline Pannier, maître des requêtes, <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat du syndicat Armateurs de France ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 28 février 2017, présentée par la ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 2261-25 du code du travail, lorsqu'il est saisi d'une demande tendant à l'extension de stipulations d'une convention ou d'un accord collectif : " Le ministre chargé du travail peut exclure de l'extension, après avis motivé de la Commission nationale de la négociation collective, les clauses qui seraient en contradiction avec des dispositions légales. / Il peut également exclure les clauses pouvant être distraites de la convention ou de l'accord sans en modifier l'économie, mais ne répondant pas à la situation de la branche ou des branches dans le champ d'application considéré. / Il peut, dans les mêmes conditions, étendre, sous réserve de l'application des dispositions légales, les clauses incomplètes au regard de ces dispositions " ;<br/>
<br/>
              2. Considérant que, sur le fondement du troisième alinéa l'article L. 2261-25 du code du travail cité ci-dessus, le ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social et le ministre de l'écologie, du développement durable et de l'énergie, ont, par arrêté du 3 novembre 2014, procédé, sous certaines réserves, à l'extension de la convention collective des personnels navigants officiers des entreprises de transport et services maritimes du 19 novembre 2012 ; que le syndicat professionnel Armateurs de France demande l'annulation pour excès de pouvoir de cet arrêté en tant qu'il émet des réserves portant, d'une part, sur l'avant-dernier alinéa de l'article 3.4 " Période d'essai " du titre III " Contrat de travail" et, d'autre part, sur l'article 3.6.3.1 du titre III " Contrat de travail " et les annexes 2a et 2b de la convention ;<br/>
<br/>
              Sur la réserve relative à la période d'essai :<br/>
<br/>
              3. Considérant qu'aux termes de l'article L. 1221-19 du code du travail, applicable aux marins salariés des entreprises d'armement maritime en vertu des dispositions de l'article L. 5541-1 et des articles L. 5542-15 à L. 5542-17 du code des transports : " Le contrat de travail à durée indéterminée peut comporter une période d'essai (...) " ; qu'aux termes de l'article L. 1221-21 du code du travail : " La période d'essai peut être renouvelée une fois si un accord de branche étendu le prévoit. Cet accord fixe les conditions et les durées de renouvellement " ; qu'en application de ces dispositions, le cinquième alinéa de l'article 3.4. de la convention collective du 19 novembre 2012 stipule que la période d'essai des officiers engagés en contrat à durée déterminée est de " 4 mois d'embarquement effectif éventuellement renouvelables une fois pour la même durée (...) " et son avant-dernier alinéa stipule que : " Le renouvellement de la période d'essai sera notifié par écrit avant l'expiration du délai de 4 mois " ; que, toutefois, aucune stipulation de la convention ne prévoit l'accord exprès du salarié à la décision de renouvellement prise par son employeur ;<br/>
<br/>
              4. Considérant que, selon une jurisprudence établie de la Cour de cassation, il résulte des dispositions de l'article 1134 du code civil, devenu l'article 1103 de ce code, que le renouvellement de l'essai ne peut résulter que d'un accord exprès des parties intervenu au cours de la période initiale ; que, dès lors, les ministres ont fait une exacte application des dispositions de l'article L. 2261-25 du code du travail rappelées au point 1 en étendant, " sous réserve du respect de l'accord exprès de la partie à laquelle il est proposé un renouvellement de la période d'essai ", l'avant-dernier alinéa de l'article 3.4. de la convention ; <br/>
<br/>
              Sur la réserve relative à l'indemnité de licenciement :<br/>
<br/>
              5. Considérant qu'aux termes de l'article L. 2251-1 du code du travail : " Une convention ou un accord peut comporter des stipulations plus favorables aux salariés que les dispositions légales en vigueur. Ils ne peuvent déroger aux dispositions qui revêtent un caractère d'ordre public " ; qu'aux termes de l'article R. 1234-4 du même code relatif au montant de l'indemnité de licenciement : " Le salaire à prendre en considération pour le calcul de l'indemnité de licenciement est, selon la formule la plus avantageuse pour le salarié : / 1° Soit le douzième de la rémunération des douze derniers mois précédant le licenciement ; / 2° Soit le tiers des trois derniers mois. Dans ce cas, toute prime ou gratification de caractère annuel ou exceptionnel, versée au salarié pendant cette période, n'est prise en compte que dans la limite d'un montant calculé à due proportion " ; qu'il résulte de ces dispositions que les conventions et accords collectifs ne peuvent légalement priver les salariés licenciés du versement d'une indemnité de licenciement au moins égale à celle qui est prévue aux articles L. 1234-9 et R. 1234-4 du code du travail ;<br/>
<br/>
              6. Considérant que les annexes 2a et 2b de la convention collective du 19 novembre 2012 stipulent que : " Le salaire de référence est désormais égal au douzième de la rémunération brute des 12 derniers mois, ou les 3 derniers mois si cela est plus favorable, précédant la date de notification du licenciement (ou du départ à la retraite) comprenant tous les éléments fixes et récurrents hors primes (notamment prime de fin d'année) et indemnité de nourriture " ; qu'il résulte clairement de ces stipulations que, dans certaines hypothèses, le montant de l'indemnité conventionnelle de licenciement, dont le taux et l'assiette sont différents de ceux retenus par les dispositions citées au point 5, est susceptible d'être inférieur à celui de l'indemnité légale de licenciement ; que, dès lors, les ministres ont fait une exacte application des dispositions de l'article L. 2261-25 du code du travail en étendant ces stipulations " sous réserve des articles L. 1234-9 et R. 1234-4 du code du travail, de telle sorte que le montant de l'indemnité conventionnelle de licenciement ne soit en aucun cas inférieur à celui de l'indemnité légale de licenciement " ; <br/>
<br/>
              7. Considérant qu'il résulte de tout ce qui précède que le syndicat requérant n'est pas fondé à demander l'annulation de l'arrêté attaqué ; que ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être également rejetées ;<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : La requête du syndicat Armateurs de France est rejetée.<br/>
Article 2 : La présente décision sera notifiée au syndicat Armateurs de France et à la ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social.<br/>
Copie en sera adressée à la ministre de l'environnement, de l'énergie et de la mer, à la fédération des officiers de la marine marchande UGICT-CGT, à la fédération de l'équipement, des transports et des services CGT-FO, à l'union fédérale maritime CFDT, au syndicat national des cadres navigants de la marine marchande CFE-CGC et au syndicat national CFTC des personnels navigants et sédentaires.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">66-02-02-02 TRAVAIL ET EMPLOI. CONVENTIONS COLLECTIVES. EXTENSION DES CONVENTIONS COLLECTIVES. POUVOIRS DU MINISTRE. - ARRÊTÉ D'EXTENSION D'UNE CONVENTION COLLECTIVE - FACULTÉ POUR LE MINISTRE D'ÉTENDRE, SOUS RÉSERVE DE L'APPLICATION DES DISPOSITIONS LÉGALES, LES CLAUSES INCOMPLÈTES AU REGARD DE CES DISPOSITIONS (ART. L. 2261-25 DU CODE DU TRAVAIL) - 1) RÉSERVE D'UNE JURISPRUDENCE ÉTABLIE DE LA COUR DE CASSATION RELATIVE AU RENOUVELLEMENT DE LA PÉRIODE D'ESSAI - LÉGALITÉ - 2) RÉSERVE DE DISPOSITIONS DU CODE DU TRAVAIL RELATIVES À L'INDEMNITÉ CONVENTIONNELLE DE LICENCIEMENT - LÉGALITÉ.
</SCT>
<ANA ID="9A"> 66-02-02-02 Le ministre chargé du travail peut étendre, sous réserve de l'application des dispositions légales, les clauses incomplètes d'une convention ou d'un accord collectif au regard de ces dispositions, après avis motivé de la Commission nationale de la négociation collective (art. L. 2261-25 du code du travail).... ,,1) Les ministres ont fait une exacte application de l'article L. 2261-25 du code du travail en étendant les stipulations de la convention nationale collective des personnels navigants officiers des entreprises de transport et services maritimes du 19 novembre 2012 relatives aux conditions de renouvellement de la période d'essai sous réserve, conformément à une jurisprudence établie de la Cour de cassation, de l'accord exprès de la partie à laquelle il est proposé un renouvellement de la période d'essai.... ,,2) Les ministres ont fait une exacte application de cet article en étendant les stipulations de la même convention relatives au montant de l'indemnité conventionnelle de licenciement sous réserve de l'application des articles L. 1234-9 et R. 1234-4 du code du travail, de telle sorte que le montant de l'indemnité conventionnelle de licenciement ne soit en aucun cas inférieur à celui de l'indemnité légale de licenciement.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
