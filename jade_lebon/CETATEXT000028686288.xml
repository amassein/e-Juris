<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028686288</ID>
<ANCIEN_ID>JG_L_2014_03_000000367233</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/68/62/CETATEXT000028686288.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème et 7ème sous-sections réunies, 05/03/2014, 367233</TITRE>
<DATE_DEC>2014-03-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>367233</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème et 7ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Sophie-Caroline de Margerie</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier Domino</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:367233.20140305</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 2 avril et 5 juin 2013 au secrétariat du contentieux du Conseil d'État, présentés par le département du Bas-Rhin, représenté par son président ; le département du Bas-Rhin demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision de la Société des autoroutes du Nord et de l'Est de la France (SANEF) fixant les tarifs de péages autoroutiers entrés en vigueur le 1er février 2013 pour la dernière section de l'autoroute A4 avant Strasbourg, la décision du ministre chargé du développement durable homologuant ces tarifs, ainsi que les clauses réglementaires " politique tarifaire et commerciale " visées au point 4 du contrat de plan Etat-SANEF pour 2010-2014 ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat et de la SANEF le versement de la somme de 3 000 &#128; au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de la voirie routière ;<br/>
<br/>
              Vu le décret n° 95-81 du 24 janvier 1995 ;<br/>
<br/>
              Vu le décret n° 2009-1102 du 8 septembre 2009 ;<br/>
<br/>
              Vu le cahier des charges de la concession passée entre l'Etat et la Société des autoroutes du Nord et de l'Est de la France (SANEF) approuvé par le décret du 29 octobre 1990 modifié, notamment son article 25 tel que modifié par le onzième avenant à la convention de concession approuvé par le décret n° 2012-1063 du 17 septembre 2012 ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie-Caroline de Margerie, Conseiller d'Etat, <br/>
<br/>
              - les conclusions de M. Xavier Domino, Rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant que le département du Bas-Rhin demande l'annulation de la décision de la Société des autoroutes du Nord et de l'Est de la France fixant les tarifs de péages autoroutiers entrés en vigueur le 1er février 2013 pour la dernière section de l'autoroute A4 avant Strasbourg, ainsi que de la décision du ministre de l'écologie, du développement durable et de l'énergie homologuant ces tarifs et des clauses " politique tarifaire et commerciale " mentionnées au point 4 du contrat de plan entre l'Etat et la  Société des Autoroutes du Nord et de l'Est de la France pour 2010-2014 ;<br/>
<br/>
              Sur la légalité externe des décisions attaquées :<br/>
<br/>
              2.	Considérant qu'aux termes de l'article 1er du décret du 24 janvier 1995 relatif aux péages autoroutiers, pris sur le fondement des dispositions aujourd'hui codifiées à l'article L. 410-2 du code de commerce : " Les tarifs de péages autoroutiers sont fixés chaque année par les  sociétés concessionnaires d'autoroutes dans les conditions définies ci-après. / Le cahier des charges de la société concessionnaire prévu par l'article L. 122-4 du code de la voirie routière définit les règles de fixation des tarifs de péages (...) / Le contrat de plan, conclu pour une durée maximale de cinq années renouvelable entre l'Etat et la société concessionnaire, fixe les modalités d'évolution des tarifs de péages pendant la période considérée " ; qu'aux termes de l'article 2 du même décret : " Les tarifs de péages fixés comme il est dit à l'article précédent sont applicables à l'expiration d'un délai d'un mois après leur dépôt auprès du ministre chargé de l'économie et auprès du ministre chargé  de l'équipement (...) " ; qu'enfin l'article 3 dispose que : " Jusqu'à la conclusion d'un contrat de plan conforme aux dispositions de l'article 1er ci-dessus, les tarifs de péages sont fixés par arrêté conjoint du ministre chargé de l'économie et du ministre chargé de l'équipement, après consultation de la société concessionnaire concernée (...) " ; <br/>
<br/>
              3. Considérant, en premier lieu, qu'il ressort des pièces du dossier que, contrairement à ce qui est soutenu, l'Etat et la Société des autoroutes du Nord et de l'Est de la France avaient, à la date où ont été fixés les tarifs de péages entrés en vigueur le 1er février 2013 pour la section d'autoroute A4 en cause, signé un contrat de plan pour 2010-2014 qui comporte, en son point 4, des clauses " politique tarifaire et commerciale " fixant un taux d'évolution des tarifs de péages ; que, dès lors, la Société des autoroutes du Nord et de l'Est de la France, régulièrement représentée par son directeur général, était compétente pour fixer ces tarifs de péages, en application des dispositions précitées des articles 1er et 2 du décret du 24 janvier 1995 ;<br/>
<br/>
              4. Considérant, en deuxième lieu, que l'acte d'homologation de ces tarifs résulte de la décision implicite d'approbation du ministre chargé de l'économie et du ministre chargé de l'équipement intervenue un mois après le dépôt des tarifs par la société concessionnaire, soit le 20 janvier 2013 ; qu'ainsi la procédure suivie en application de l'article 2 du décret précité a été régulière ;<br/>
<br/>
              Sur la légalité interne des décisions attaquées :<br/>
<br/>
              5. Considérant qu'il ressort des pièces du dossier que les trajets effectués entre la barrière de péage de Schwindratzheim et la sortie de Reichstett, qui se trouve en limite de la concession, sont facturés à l'usager 1,20 &#128; sur une base forfaitaire, quelle que soit la distance parcourue ; que, compte tenu de la longueur de la portion d'autoroute en cause, qui n'est que de 19 km, et du faible montant du tarif pratiqué, le caractère forfaitaire du péage est d'ampleur limitée ; que la localisation de la barrière de péage à Schwindratzheim et non aux abords immédiats de Strasbourg, qui a conduit à un système de " péage ouvert ", trouve sa justification dans des motifs d'intérêt général de fluidité du trafic et de rationalisation de l'exploitation de l'autoroute ; qu'ainsi, le moyen tiré de ce que la tarification forfaitaire pratiquée pour la section de l'autoroute A4 en cause serait contraire au principe d'égalité des usagers devant le service public et ne respecterait pas la règle de proportionnalité entre le montant du tarif et la valeur du service rendu ne peut qu'être écarté ;<br/>
<br/>
              6. Considérant que les clauses " politique tarifaire et commerciale " mentionnées au point 4 du contrat de plan entre l'Etat et la Société des autoroutes du Nord et de l'Est de la France pour 2010-2014 n'avaient pas à définir avec davantage de précision le mode de calcul des tarifs, lesquels sont accessibles au public sur le site internet de la société ;<br/>
<br/>
              7. Considérant qu'il résulte de tout ce qui précède, et sans qu'il soit besoin de statuer sur la fin de non-recevoir opposée par la Société des autoroutes du Nord et de l'Est de la France, que le département du Bas-Rhin n'est pas fondé à demander l'annulation des décisions attaquées ; <br/>
<br/>
              Sur les conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              8. Considérant que ces dispositions font obstacle à ce que soit mise à la charge de la Société des autoroutes du Nord et de l'Est de la France, qui n'est pas, dans la présente instance, la partie perdante, la somme que demande à ce titre le département du Bas-Rhin ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par la Société des autoroutes du Nord et de l'Est de la France au titre de ces mêmes dispositions ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête du département du Bas-Rhin est rejetée.<br/>
<br/>
Article 2 : Les conclusions de la Société des autoroutes du Nord et de l'Est de la France présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
<br/>
Article 3 : La présente décision sera notifiée au département du Bas-Rhin, à la Société des autoroutes du Nord et de l'Est de la France et au ministre de l'écologie, du développement durable et de l'énergie.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">65-02 TRANSPORTS. TRANSPORTS ROUTIERS. - AUTOROUTES - FIXATION DU TARIF DES PÉAGES - PÉAGE DIT  OUVERT  AVEC TARIFICATION FORFAITAIRE - LÉGALITÉ D'UNE TELLE TARIFICATION AU REGARD DU PRINCIPE D'ÉGALITÉ ET DE LA RÈGLE DE PROPORTIONNALITÉ - CONDITIONS - AMPLEUR LIMITÉE ET JUSTIFICATION PAR UN MOTIF D'INTÉRÊT GÉNÉRAL.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">71-02-04-03 VOIRIE. RÉGIME JURIDIQUE DE LA VOIRIE. DROITS ET OBLIGATIONS DES RIVERAINS ET USAGERS. PÉAGES. - AUTOROUTES - PÉAGE DIT  OUVERT  AVEC TARIFICATION FORFAITAIRE - LÉGALITÉ D'UNE TELLE TARIFICATION AU REGARD DU PRINCIPE D'ÉGALITÉ ET DE LA RÈGLE DE PROPORTIONNALITÉ - CONDITIONS - AMPLEUR LIMITÉE ET JUSTIFICATION PAR UN MOTIF D'INTÉRÊT GÉNÉRAL.
</SCT>
<ANA ID="9A"> 65-02 Le principe d'égalité des usagers devant le service public et la règle de proportionnalité entre le montant du tarif et la valeur du service rendu ne font pas obstacle à ce que soit pratiquée, sur une portion d'autoroute, une tarification forfaitaire, dès lors, d'une part, que compte tenu de la longueur d'autoroute en cause et du faible montant du tarif pratiqué, le caractère forfaitaire du péage est d'ampleur limitée et, d'autre part, que la localisation de la barrière de péage conduisant à un système de  péage ouvert  trouve sa justification dans des motifs d'intérêt général de fluidité du trafic et de rationalisation de l'exploitation de l'autoroute.</ANA>
<ANA ID="9B"> 71-02-04-03 Le principe d'égalité des usagers devant le service public et la règle de proportionnalité entre le montant du tarif et la valeur du service rendu ne font pas obstacle à ce que soit pratiquée, sur une portion d'autoroute, une tarification forfaitaire, dès lors, d'une part, que compte tenu de la longueur d'autoroute en cause et du faible montant du tarif pratiqué, le caractère forfaitaire du péage est d'ampleur limitée et, d'autre part, que la localisation de la barrière de péage conduisant à un système de  péage ouvert  trouve sa justification dans des motifs d'intérêt général de fluidité du trafic et de rationalisation de l'exploitation de l'autoroute.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
