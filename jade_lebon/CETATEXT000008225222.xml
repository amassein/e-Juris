<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000008225222</ID>
<ANCIEN_ID>JG_L_2005_12_000000276589</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/08/22/52/CETATEXT000008225222.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème et 7ème sous-sections réunies, 14/12/2005, 276589</TITRE>
<DATE_DEC>2005-12-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>276589</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème et 7ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Stirn</PRESIDENT>
<AVOCATS>RICARD</AVOCATS>
<RAPPORTEUR>Mme Sophie-Caroline de Margerie</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Prada Bordenave</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés le 13 janvier et 24 juin 2005 au secrétariat du contentieux du Conseil d'Etat, présentée pour M. René X, demeurant ...; M. X demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret en date du 15 décembre 2004 par lequel le Premier ministre a accordé son extradition aux autorités allemandes en vue de l'exécution d'un mandat d'arrêt délivré le 28 novembre 2003 par le tribunal d'instance de Sarrebrück, modifié le 27 mai 2004 par ce même tribunal, pour des faits de vol aggravé et escroquerie ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros en application des dispositions de l'article L. 761-1 du code de justice administrative ;  <br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu la Constitution, notamment son article 55 ;<br/>
              Vu la convention européenne d'extradition du 13 décembre 1957 ;<br/>
<br/>
              Vu la convention d'application de l'accord de Schengen du 14 juin 1985, signée le 19 juin 1990 ;<br/>
<br/>
              Vu le code pénal ;<br/>
<br/>
              Vu le code de procédure pénale ;<br/>
<br/>
              Vu la loi n° 79-587 du 11 juillet 1979, modifiée ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie-Caroline de Margerie, Conseiller d'Etat,  <br/>
<br/>
              - les observations de Me Ricard, avocat de M. X, <br/>
<br/>
              - les conclusions de Mme Emmanuelle Prada Bordenave, Commissaire du gouvernement ;<br/>
<br/>
<br/>
<br/>
<br/>
              Sur la légalité externe :<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier que, contrairement à ce que soutient le requérant, le décret attaqué a été signé par le Premier ministre et contresigné par le garde des sceaux, ministre de la justice ;<br/>
<br/>
              Considérant qu'après avoir visé la demande d'extradition présentée par les autorités allemandes en vue de l'exécution d'un mandat d'arrêt délivré le 28 novembre 2003 par le tribunal d'instance de Sarrebrück, modifié le 27 mai 2004 par ce même tribunal, pour des faits de vol aggravé et escroquerie, ainsi que l'avis émis par la chambre de l'instruction de la cour d'appel d'Aix-en-Provence et avoir indiqué les faits reprochés à M. X, le décret attaqué énonce que ces faits répondent aux exigences de l'article 61 de la convention d'application de l'accord de Schengen du 14 juin 1985, signée le 19 juin 1990, sont punissables en droit français, ne sont pas prescrits, n'ont pas un caractère politique et que la demande d'extradition n'a pas été présentée aux fins de poursuivre l'intéressé en raison de ses opinions politiques ; qu'ainsi, il satisfait aux exigences de motivation posées à l'article 3 de la loi du 11 juillet 1979 relative à la motivation des actes administratifs et à l'amélioration des relations entre l'administration et le public ;<br/>
<br/>
              Considérant qu'aux termes du 2 de l'article 12 de la convention européenne d'extradition : « Il sera produit à l'appui de la requête : a) L'original ou l'expédition authentique soit d'une décision de condamnation exécutoire, soit d'un mandat d'arrêt ou de tout autre acte ayant la même force, délivré dans les formes prescrites par la loi de la Partie requérante ; b) Un exposé des faits pour lesquels l'extradition est demandée. Le temps et le lieu de leur perpétration, leur qualification légale et les références aux dispositions légales qui leur sont applicables seront indiqués le plus exactement possible ; et c) Une copie des dispositions légales applicables ou, si cela n'est pas possible, une déclaration sur le droit applicable, ainsi que le signalement aussi précis que possible de l'individu réclamé ou tous autres renseignements de nature à déterminer son identité et sa nationalité » ; qu'il ressort des pièces du dossier que la demande d'extradition était accompagnée de toutes les pièces requises par ces stipulations ; qu'ainsi, le moyen tiré de la méconnaissance des stipulations du 2 de l'article 12 de la convention européenne d'extradition du 13 décembre 1957 doit être écarté ; <br/>
<br/>
              Sur la légalité interne :<br/>
<br/>
              Considérant que, si l'article 696-4 du code de procédure pénale prévoit en son 3° que l'extradition n'est pas accordée pour un crime ou un délit commis en France, ces dispositions ne sauraient prévaloir sur les stipulations de la convention européenne d'extradition, qui a une autorité supérieure à celle de la loi en vertu des prescriptions de l'article 55 de la Constitution et de celles de l'article 696 du code de procédure pénale ; que l'article 7 de la convention européenne d'extradition, aux termes duquel « la Partie requise pourra refuser d'extrader l'individu réclamé à raison d'une infraction qui, selon la législation, a été commise en tout ou en partie sur son territoire », implique que les autorités d'un Etat signataire de cette convention puissent accorder l'extradition d'un étranger pour des faits commis sur le territoire de cet Etat ; qu'il ne ressort pas des pièces du dossier que le Gouvernement ait commis une erreur manifeste d'appréciation en s'abstenant de faire usage de cette faculté concernant l'extradition de M. X pour des faits commis en France ;<br/>
<br/>
              Considérant que si, aux termes du paragraphe 1er de l'article 2 de la convention européenne d'extradition, tel qu'il résulte des réserves exprimées par le Gouvernement français : « Donneront lieu à extradition les faits punis par les lois de la partie requérante et de la partie requise d'une peine privative de liberté.... d'un maximum d'au moins deux ans ou d'une peine plus sévère », il résulte de l'article 61 de la convention d'application de l'accord de Schengen signée le 19 juin 1990, qui complète entre les Etats parties la convention européenne d'extradition en vertu de son article 59, que « la République française s'engage à extrader, à la demande de l'une des Parties contractantes, les personnes poursuivies pour des faits punis par la législation française d'une peine ou d'une mesure de sûreté privative de liberté d'un maximum d'au moins deux ans et par la loi de la Partie contractante requérante d'une peine ou d'une mesure de sûreté privative de liberté d'un maximum d'au moins un an » ; que les faits de vol aggravé et d'escroquerie dont M. X se serait rendu coupable sont punis en droit français de cinq ans d'emprisonnement et en droit allemande respectivement de dix ans et de cinq ans d'emprisonnement ; qu'ainsi, l'ensemble de ces faits pouvait légalement donner lieu à extradition en application des stipulations précitées de la convention d'application de l'accord de Schengen signée le 19 juin 1990 ;<br/>
<br/>
              Considérant que si M. X soutient qu'il est de nationalité française, il n'apporte aucun élément à l'appui de ses allégations ; qu'en l'absence de contestation sérieuse, il n'y a pas lieu à renvoi préjudiciel devant le juge de la nationalité ;<br/>
<br/>
              Considérant qu'il résulte de tout ce qui précède que M. X n'est pas fondé à demander l'annulation du décret du 15 décembre 2004 accordant son extradition aux autorités allemandes ;<br/>
<br/>
              Sur les conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              Considérant que ces dispositions font obstacle à ce que l'Etat, qui n'est pas la partie perdante dans la présente instance, verse à M. X la somme que celui-ci demande pour les frais exposés par lui et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. X est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. René X et au garde des sceaux, ministre de la justice.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">335-04-03 ÉTRANGERS. EXTRADITION. DÉCRET D'EXTRADITION. - RECOURS EN EXCÈS DE POUVOIR CONTRE UN TEL DÉCRET - CIRCONSTANCE QUE L'ETAT REQUÉRANT A RENONCÉ À SA DEMANDE D'EXTRADITION NE LE PRIVANT PAS D'OBJET (SOL. IMPL.) [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-05-05-01 PROCÉDURE. INCIDENTS. NON-LIEU. ABSENCE. - RECOURS EN EXCÈS DE POUVOIR DIRIGÉ CONTRE UN DÉCRET D'EXTRADITION - CIRCONSTANCE QUE L'ETAT REQUÉRANT A RENONCÉ À SA DEMANDE D'EXTRADITION (SOL. IMPL.) [RJ1].
</SCT>
<ANA ID="9A"> 335-04-03 La circonstance que l'Etat requérant a renoncé à demander l'extradition de l'intéressé ne rend pas sans objet le recours en excès de pouvoir dirigé par ce dernier contre le décret accordant son extradition.</ANA>
<ANA ID="9B"> 54-05-05-01 La circonstance que l'Etat requérant a renoncé à demander l'extradition de l'intéressé ne rend pas sans objet le recours en excès de pouvoir dirigé par ce dernier contre le décret accordant son extradition.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Comp., en cas de retrait du décret, 29 juillet 1994, Von Lintel, p. 1123 ; et, compte tenu des circonstances de l'espèce, 30 mai 2005, Pirvulescu, p. 234.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
