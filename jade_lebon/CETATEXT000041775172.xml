<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041775172</ID>
<ANCIEN_ID>JG_L_2020_03_000000431175</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/77/51/CETATEXT000041775172.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 27/03/2020, 431175</TITRE>
<DATE_DEC>2020-03-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>431175</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>CABINET BRIARD</AVOCATS>
<RAPPORTEUR>M. Guillaume de LA TAILLE LOLAINVILLE</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Karin Ciavaldini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:431175.20200327</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu les procédures suivantes :<br/>
<br/>
              1° Sous le n° 431175, par une décision du 13 décembre 2019, le Conseil d'Etat, statuant au contentieux, a prononcé l'admission des conclusions du pourvoi de Mme A... C... dirigées contre l'arrêt n° 17DA00522 du 25 mars 2019 de la cour administrative d'appel de Douai, d'une part en tant que cet arrêt a, sur l'appel du ministre de l'action et des comptes publics, remis à la charge de l'intéressée l'obligation de payer la somme de 4 363,69 euros procédant d'une mise en demeure tenant lieu de commandement, émise le 17 août 2012, pour le recouvrement de cotisations de taxe foncière, en droits et majorations, relatives aux années 2003 et 2004, puis annulé le jugement n° 1606496 du 2 février 2017 du tribunal administratif de Lille en ce qu'il avait de contraire, et d'autre part, en tant que cet arrêt a rejeté l'appel formé par Mme C... contre le même jugement dans la mesure où celui-ci avait rejeté sa demande de décharge de l'obligation de payer la somme de 531 euros procédant de mises en demeure tenant lieu de commandement, également émises le 17 août 2012, pour le recouvrement de cotisations de taxe d'habitation, en droits et majorations, relatives aux années 2004 à 2007.<br/>
<br/>
<br/>
              2° Sous le n° 431178, par une décision du 13 décembre 2019, le Conseil d'Etat, statuant au contentieux, a prononcé l'admission des conclusions du pourvoi présentées par Mme C... en qualité d'héritière de la succession de M. B... C..., dirigées contre l'arrêt n° 17DA00525 du 25 mars 2019 de la cour administrative d'appel de Douai en tant que cet arrêt a, sur l'appel du ministre de l'action et des comptes publics, remis à la charge de cette succession la somme de 1 160 euros procédant d'une mise en demeure tenant lieu de commandement, émise le 17 août 2012, pour le recouvrement de cotisations de taxe d'habitation, en droits et majorations, relatives aux années 2002 et 2003, puis annulé le jugement n° 1606497 du 2 février 2017 du tribunal administratif de Lille en ce qu'il avait de contraire.<br/>
<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Guillaume de La Taille Lolainville, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Karin Ciavaldini, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, au Cabinet Briard, avocat de Mme C... ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, le 17 août 2012, des mises en demeure tenant lieu de commandements de payer ont été émises à l'encontre des époux A... et Alfredo C... en vue du recouvrement de diverses créances fiscales. Par deux jugements nos 1606496 et 1606497 du 2 février 2017, le tribunal administratif de Lille a accueilli en partie les demandes, dont Mme C..., d'une part, et M. C..., d'autre part, l'avaient saisi, tendant à la décharge des obligations de payer résultant de ces actes de poursuite. Faisant droit aux appels du ministre de l'action et des comptes publics, la cour administrative d'appel de Douai a annulé ces jugements en tant qu'ils étaient partiellement favorables aux contribuables et les a confirmés pour le surplus par deux arrêts nos 17DA00522 et 17DA00525 du 25 mars 2019.<br/>
<br/>
              2. Statuant au contentieux le 13 décembre 2019 sur le pourvoi n° 431175 présenté par Mme C... contre l'arrêt la concernant à titre personnel, le Conseil d'Etat a refusé d'en admettre les conclusions, à l'exception de celles qui tendent à l'annulation de cet arrêt en tant, d'une part, qu'il rétablit l'obligation de payer la somme de 4 363,69 euros correspondant à des cotisations de taxe foncière relatives aux années 2003 et 2004 et, d'autre part, qu'il confirme l'obligation de payer la somme de 531 euros correspondant à des cotisations de taxe d'habitation relatives aux années 2004 à 2007. Statuant au contentieux le même jour sur le pourvoi n° 431178 présenté par Mme C... contre l'arrêt la concernant en qualité d'héritière de la succession de M. C..., le Conseil d'Etat a refusé d'en admettre les conclusions, à l'exception de celles qui sont dirigées contre cet arrêt en tant qu'il rétablit l'obligation de payer la somme de 1 160 euros correspondant à des cotisations de taxe d'habitation relatives aux années 2002 et 2003.<br/>
<br/>
              3. Ces affaires présentent à juger des questions semblables. Il y a lieu de les joindre pour statuer par une seule décision.<br/>
<br/>
              Sur les pourvois dirigés contre les arrêts de la cour administrative d'appel de Douai :<br/>
<br/>
              4. Le 4° de l'article R. 811-1 du code de justice administrative prévoit que le tribunal administratif statue en premier et dernier ressort " sur les litiges relatifs aux impôts locaux et à la contribution à l'audiovisuel public, à l'exception des litiges relatifs à la contribution économique territoriale ". Il résulte de ces dispositions que la compétence de premier et dernier ressort du tribunal administratif s'étend, dans les litiges relatifs aux impôts locaux, aussi bien aux demandes relatives à l'assiette de ces prélèvements qu'aux demandes relatives à leur recouvrement. Par ailleurs, il résulte des dispositions des articles R. 351-2 et R. 351-4 du même code que, sauf à rejeter les conclusions comme entachées d'une irrecevabilité manifeste insusceptible d'être couverte en cours d'instance, à constater qu'il n'y a pas lieu de statuer sur tout ou partie des conclusions ou à rejeter la requête en se fondant sur l'irrecevabilité manifeste de la demande de première instance, une cour administrative d'appel saisie de conclusions qui relèvent de la compétence du Conseil d'Etat doit les lui transmettre sans délai.<br/>
<br/>
              5. En se prononçant, par ses jugements du 2 février 2017, sur les conclusions de Mme et de M. C... tendant à la décharge d'obligations de payer des cotisations de taxe foncière et de taxe d'habitation, le tribunal administratif de Lille a statué en premier et dernier ressort. Par suite, en tant que, par ses arrêts du 25 mars 2019, elle a statué à son tour sur le bien-fondé de ces obligations de payer, la cour administrative d'appel de Douai a méconnu le champ de sa compétence d'appel. Sans qu'il soit besoin de se prononcer sur les moyens des pourvois, il y a donc lieu d'annuler dans cette mesure les arrêts attaqués. Les conclusions que le ministre de l'action et des comptes publics et Mme C... ont présentées devant la cour administrative d'appel de Douai, dirigées contre les jugements, statuant en premier et dernier ressort, en tant qu'ils leur font grief, doivent être regardées comme des pourvois en cassation. <br/>
<br/>
              6. Aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux. " Cette procédure est applicable à tout pourvoi en cassation dont le Conseil d'Etat est saisi. Elle est, par suite, applicable aux conclusions contre un jugement ayant statué en premier et dernier ressort sur lesquelles une cour administrative d'appel a statué et qui doivent être regardées, après l'annulation de l'arrêt de la cour, comme des conclusions de cassation. <br/>
<br/>
              Sur les pourvois dirigés contre le jugement n° 1606496 :<br/>
<br/>
              7. En premier lieu, les conclusions que le ministre de l'action et des comptes publics a présentées devant la cour administrative d'appel de Douai et qui sont dirigées contre le jugement n° 1606496 du tribunal administratif de Lille du 2 février 2017 en tant que ce jugement a déchargé Mme C... de l'obligation de payer des cotisations de taxe foncière constituent, ainsi qu'il a été dit ci-dessus, un pourvoi en cassation. Pour demander l'annulation du jugement, le ministre soutient que le tribunal administratif de Lille a méconnu l'article L. 274 du livre des procédures fiscales en jugeant que la prescription était acquise lorsque le comptable public a, le 17 août 2012, émis les commandements de payer en litige, dès lors que le délai de prescription avait été interrompu par le dépôt d'un dossier de surendettement auprès de la Banque de France et par l'émission régulière, depuis la mise en recouvrement des impositions, de divers actes de poursuite. Ce moyen n'est pas de nature à permettre l'admission de ce pourvoi.<br/>
<br/>
              8. En second lieu, les conclusions que Mme C... a présentées le 6 novembre 2017 dans son mémoire en défense devant la cour administrative d'appel de Douai, et qui sont dirigées contre ce même jugement n° 1606496 du 2 février 2017 en tant que ce jugement a rejeté sa demande en décharge de l'obligation de payer la somme de 531 euros correspondant à des cotisations de taxe d'habitation relatives aux années 2004 à 2007 constituent également un pourvoi en cassation.<br/>
<br/>
              9. Cependant si Mme C... doit être regardée comme ayant entendu introduire un pourvoi incident, un tel pourvoi ne saurait être admis, dès lors que la présente décision refuse d'admettre le pourvoi du ministre dirigé contre le jugement. Si Mme C... doit être regardée comme ayant entendu introduire un pourvoi principal, un tel pourvoi ne saurait davantage être admis. En effet, en vertu du premier alinéa de l'article R. 821-1 du code de justice administrative, le délai de recours en cassation est de deux mois. La notification à Mme C... du jugement, certes erronée en ce qu'elle a prétendu la voie de l'appel ouverte contre l'ensemble des éléments du dispositif, n'a pas été en revanche de nature à induire l'intéressée en erreur sur ce délai. Par suite, le délai dont Mme C... disposait pour former un pourvoi principal à l'encontre du jugement, qui a commencé à courir à la date de sa notification le 15 février 2017, a expiré le lundi 17 avril suivant. Dès lors, un pourvoi principal de Mme C..., serait tardif et, pour ce motif, irrecevable.<br/>
<br/>
              10. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par Mme C... au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Sur le pourvoi dirigé contre le jugement n° 1606497 :<br/>
<br/>
              11. Les conclusions que le ministre de l'action et des comptes publics a présentées devant la cour administrative d'appel de Douai et qui sont dirigées contre le jugement n° 1606497 du tribunal administratif de Lille du 2 février 2017 en tant que celui-ci a déchargé M. C... de l'obligation de payer des cotisations de taxe d'habitation constituent un pourvoi en cassation. Pour demander l'annulation du jugement, le ministre soutient que le tribunal administratif de Lille a :<br/>
              - méconnu l'article R. 281-3-1 du livre des procédures fiscales en accueillant le moyen tiré de la prescription de l'action en recouvrement dès lors que, les mises en demeure du 17 août 2012 ayant été précédées par l'émission, le 6 juillet 2011, d'un commandement aux fins de saisie immobilière, ce moyen était irrecevable ; <br/>
              - méconnu l'article L. 274 du livre des procédures fiscales en jugeant que la prescription était acquise lorsque le comptable public a, le 17 août 2012, émis les commandements de payer en litige, dès lors que le délai de prescription avait été interrompu par une reconnaissance de dette, par le dépôt d'un dossier de surendettement auprès de la Banque de France et par l'émission régulière, depuis la mise en recouvrement des impositions, de divers actes de poursuite.<br/>
<br/>
              12. Eu égard aux moyens soulevés ou susceptibles d'être relevés d'office, il y a lieu d'admettre les conclusions de ce pourvoi.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt n° 17DA00522 du 25 mars 2019 de la cour administrative d'appel de Douai est annulé en tant qu'il remet à la charge de Mme C... l'obligation de payer la somme de 4 363,69 euros procédant d'une mise en demeure tenant lieu de commandement, émise le 17 août 2012, pour le recouvrement de cotisations de taxe foncière, en droits et majorations, relatives aux années 2003 et 2004, et en tant qu'il  annule le jugement n° 1606496 du 2 février 2017 du tribunal administratif de Lille en ce qu'il avait de contraire.<br/>
Article 2 : L'arrêt n° 17DA00522 du 25 mars 2019 de la cour administrative d'appel de Douai est annulé en tant qu'il rejette l'appel formé par Mme C... contre le jugement n° 1606496 du 2 février 2017 du tribunal administratif de Lille dans la mesure où ce jugement avait rejeté la demande de décharge de l'obligation de payer la somme de 531 euros procédant de mises en demeure tenant lieu de commandements, émises le 17 août 2012, pour le recouvrement de cotisations de taxe d'habitation, en droits et majorations, relatives aux années 2004 à 2007.<br/>
 Article 3 : L'arrêt n° 17DA00525 du 25 mars 2019 de la cour administrative d'appel de Douai est annulé en tant qu'il  remet à la charge de la succession de M. C... la somme de 1 160 euros procédant d'une mise en demeure tenant lieu de commandement, émise le 17 août 2012, pour le recouvrement de cotisations de taxe d'habitation, en droits et majorations, relatives aux années 2002 et 2003, et en tant qu'il  annule le jugement n° 1606497 du 2 février 2017 du tribunal administratif de Lille en ce qu'il avait de contraire.<br/>
Article 4 : Le pourvoi du ministre de l'action et des comptes publics dirigé contre le jugement n° 1606496 du tribunal administratif de Lille du 2 février 2017 en tant qu'il a déchargé Mme C... de l'obligation de payer des cotisations de taxe foncière n'est pas admis.<br/>
Article 5 : Le pourvoi de Mme C... dirigé contre le jugement n° 1606496 du tribunal administratif de Lille du 2 février 2017 en tant qu'il a confirmé son obligation de payer des cotisations de taxe d'habitation n'est pas admis.<br/>
Article 6 : Les conclusions présentées par Mme C... sous le n° 431175 au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 7 : Le pourvoi du ministre de l'action et des comptes publics dirigé contre le jugement n° 1606497 du tribunal administratif de Lille du 2 février 2017 en tant qu'il a déchargé M. C... de l'obligation de payer des cotisations de taxe d'habitation est admis.<br/>
Article 8 : La présente décision sera notifiée à Mme A... C... et au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-08-02-03-01 PROCÉDURE. VOIES DE RECOURS. CASSATION. POUVOIRS DU JUGE DE CASSATION. ADMISSION DES POURVOIS EN CASSATION. - CONCLUSIONS CONTRE UN JUGEMENT AYANT STATUÉ EN PREMIER ET DERNIER RESSORT SUR LESQUELLES UNE COUR ADMINISTRATIVE D'APPEL A STATUÉ - POSSIBILITÉ DE METTRE EN &#140;UVRE LA PAPC APRÈS ANNULATION DE L'ARRÊT - EXISTENCE [RJ1].
</SCT>
<ANA ID="9A"> 54-08-02-03-01 La procédure d'admission des pourvois en cassation (PAPC), instituée par l'article L. 822-1 du code de justice administrative (CJA) est applicable à tout pourvoi en cassation dont le Conseil d'Etat est saisi. Elle est, par suite, applicable aux conclusions contre un jugement ayant statué en premier et dernier ressort sur lesquelles une cour administrative d'appel a statué et qui doivent être regardées, après l'annulation de l'arrêt de la cour, comme des conclusions de cassation.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., en précisant, CE, Section, 15 février 2019, Mme,, n° 416590, à publier au Rec. Ab. jur. CE, 29 janvier 2007,,, n° 284113, T. pp. 767-1048.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
