<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035803953</ID>
<ANCIEN_ID>JG_L_2017_10_000000397031</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/80/39/CETATEXT000035803953.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 4ème chambres réunies, 13/10/2017, 397031</TITRE>
<DATE_DEC>2017-10-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>397031</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 4ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP POTIER DE LA VARDE, BUK LAMENT, ROBILLOT ; HAAS</AVOCATS>
<RAPPORTEUR>M. Florian Roussel</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Laurence Marion</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:397031.20171013</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. et Mme A...B...ont demandé au tribunal administratif de Toulon, d'une part, de condamner solidairement la commune de Six-Fours-les-Plages et l'Etat à leur verser la somme de 1 182 652, 64 euros en vue de la remise en état naturel du terrain dont ils sont propriétaires sur le territoire de cette commune et sur lequel ont été effectués des dépôts illicites de déchets et, d'autre part, d'enjoindre à ces mêmes autorités de mettre en oeuvre les mesures propres à empêcher de tels dépôts et d'engager des poursuites contre leurs auteurs lorsque ceux-ci sont identifiés. Par un jugement n° 1101062 du 6 décembre 2013, le tribunal administratif de Toulon a rejeté leur demande.<br/>
<br/>
              Par un arrêt n° 14MA00600 du 15 décembre 2015, la cour administrative d'appel de Marseille a annulé ce jugement pour irrégularité puis évoqué et rejeté la demande de M. et MmeB....<br/>
<br/>
              Par un pourvoi sommaire et deux mémoires complémentaires, enregistrés les 16 février, 17 mai et 22 septembre 2016 au secrétariat du contentieux du Conseil d'Etat, M. et Mme B...demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat et de la commune de Six-Fours-les-Plages la somme de 8 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
<br/>
              - la directive 2006/12/CE du 5 avril 2006 ;<br/>
<br/>
              - le code de l'environnement ;<br/>
<br/>
              - le code général des collectivités territoriales ;<br/>
<br/>
              - le code forestier ;<br/>
<br/>
              - la loi n° 2003-699 du 30 juillet 2003 ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Florian Roussel, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Laurence Marion, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Potier de la Varde, Buk Lament, Robillot, avocat de M. et Mme B...et à Me Haas, avocat de la commune de Six-Fours-les-Plages.<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que les époux B...ont acquis en 1988 un terrain boisé de trois hectares, situé sur le territoire de la commune de Six-Fours-les-Plages (Var), dans le site classé du Cap Sicié ; que, depuis lors, des dépôts illicites de déchets en quantité très importante, principalement des matériaux de construction, ont été constatés sur cette parcelle ; que, s'estimant victimes d'une carence du maire et du préfet dans l'exercice de leurs pouvoirs de police, les époux B...ont recherché la responsabilité de la commune et de l'Etat devant le tribunal administratif de Toulon ; que le tribunal a rejeté leur demande par un jugement du 6 décembre 2013, dont ils ont relevé appel ; que, par un arrêt du 15 décembre 2015, la cour administrative d'appel de Marseille a annulé ce jugement pour irrégularité, puis évoqué et rejeté la demande de première instance ; que le pourvoi des époux B...doit être regardé comme tendant à l'annulation de l'article 2 de cet arrêt, qui rejette leur demande de première instance ainsi que le surplus de leurs conclusions d'appel ;<br/>
<br/>
              Sur la responsabilité de l'Etat :<br/>
<br/>
              2. Considérant qu'en soulevant pour la première fois en appel le moyen tiré de ce que le préfet du Var avait commis une faute de nature à engager la responsabilité de l'Etat en ne faisant pas usage des pouvoirs qui lui sont dévolus par les articles L. 162-11 à 162-16 du code de l'environnement ainsi que par l'article L. 414-5 de ce même code, les époux B...n'ont modifié ni l'objet de leur demande de première instance, qui tendait à la réparation des conséquences dommageables de l'inaction du préfet, ni la cause juridique de cette demande, fondée sur la responsabilité pour faute ; que, par suite, en s'estimant saisie d'une demande nouvelle irrecevable, la cour a entaché son arrêt d'une erreur de droit ; <br/>
<br/>
              Sur la responsabilité de la commune de Six-Fours-les-Plages :<br/>
<br/>
              En ce qui concerne les déchets déposés avant l'année 2009 : <br/>
<br/>
              3. Considérant qu'aux termes du premier alinéa de l'article L. 541-2 du code de l'environnement, dans sa rédaction issue de la loi du 30 juillet 2003 relative à la prévention des risques technologiques et à la réparation des dommages : " Toute personne qui produit ou détient des déchets dans des conditions de nature à produire des effets nocifs sur le sol, la flore et la faune, à dégrader les sites ou les paysages, à polluer l'air ou les eaux, à engendrer des bruits et des odeurs et, d'une façon générale, à porter atteinte à la santé de l'homme et à l'environnement, est tenue d'en assurer ou d'en faire assurer l'élimination conformément aux dispositions du présent chapitre, dans des conditions propres à éviter lesdits effets " ; que sont responsables des déchets au sens de ces dispositions, interprétées à la lumière des dispositions des articles 1er, 8 et 17 de la directive du Parlement européen et du Conseil du 5 avril 2006 relative aux déchets, les seuls producteurs ou autres détenteurs des déchets ; qu'en l'absence de tout producteur ou de tout autre détenteur connu, le propriétaire du terrain sur lequel ont été déposés des déchets peut être regardé comme leur détenteur au sens de l'article L. 541-2 du code de l'environnement, notamment s'il a fait preuve de négligence à l'égard d'abandons sur son terrain ; <br/>
<br/>
              4. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, dans leurs écritures de première instance et d'appel, les époux B...soutenaient qu'ils ne pouvaient être regardés comme les détenteurs des déchets entreposés sur leur parcelle au sens des dispositions précitées dès lors que leur producteur pouvait être identifié ; qu'ils faisaient notamment valoir que l'essentiel des déchets entreposés sur leur terrain provenait de l'activité illégale de l'entreprise Masséna, en relevant en particulier que celle-ci avait procédé en 2005, pour le compte de la commune de Six-Fours-les-Plages, à la démolition d'une passerelle dont des morceaux avaient été retrouvés sur leur parcelle ; qu'en énonçant " qu'il est constant que les déchets litigieux déposés sur le terrain des époux B...avant l'année 2009 (...) ne proviennent pas de producteurs ou autres détenteurs connus ", alors que les requérants élevaient sur ce point une contestation expresse et précise, la cour s'est méprise sur la portée de leurs écritures ;<br/>
<br/>
              En ce qui concerne les déchets déposés au cours de l'année 2009 : <br/>
<br/>
              5. Considérant qu'aux termes de l'article L. 541-3 du code de l'environnement, dans sa rédaction issue de la loi du 30 juillet 2003 : " En cas de pollution des sols, de risque de pollution des sols, ou au cas où des déchets sont abandonnés, déposés ou traités contrairement aux prescriptions du présent chapitre et des règlements pris pour leur application, l'autorité titulaire du pouvoir de police peut, après mise en demeure, assurer d'office l'exécution des travaux nécessaires aux frais du responsable (...) " ; qu'il résulte de ces dispositions que l'autorité investie des pouvoirs de police municipale doit prendre les mesures nécessaires pour assurer l'élimination des déchets dont l'abandon, le dépôt ou le traitement présente des dangers pour l'environnement ; <br/>
<br/>
              6. Considérant que, s'agissant des déchets déposés dans le courant de l'année 2009, la cour a retenu que le refus de l'autorité titulaire du pouvoir de police de faire usage des pouvoirs que lui confère l'article L. 541-3 du code de l'environnement n'est illégal que s'il est entaché d'une erreur manifeste d'appréciation au regard de la gravité de l'atteinte portée à l'environnement et qu'en l'espèce le maire de Six-Fours-les-Plages n'avait pas commis d'illégalité et n'avait, par suite, pas engagé la responsabilité de la commune, en s'abstenant d'assurer aux frais des intéressés l'enlèvement des déchets dont les producteurs avaient pu être identifiés ; qu'en se bornant à rechercher si l'abstention du maire était entachée d'erreur manifeste, alors qu'il lui appartenait d'exercer un plein contrôle sur le respect de l'obligation définie au point 5 ci-dessus, la cour a entaché son arrêt d'une erreur de droit ; <br/>
<br/>
              7. Considérant qu'il résulte de tout ce qui précède, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que l'arrêt attaqué doit être annulé en tant qu'il rejette la demande présentée par M. et Mme B...devant le tribunal administratif de Toulon, tant contre l'Etat que contre la commune de Six-Fours-les-Plages ;<br/>
<br/>
              8. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de Six-Fours-les-Plages et de l'Etat le versement à M. et Mme B...d'une somme de 1 500 euros chacun au titre de l'article L. 761-1 du code de justice administrative ; que ces dispositions font obstacle à ce qu'une somme soit mise à la charge des épouxB..., qui ne sont pas, dans la présente instance, la partie perdante ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'article 2 de l'arrêt n° 14MA00600 de la cour administrative d'appel de Marseille du 15 décembre 2015 est annulé.<br/>
<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Marseille dans la limite de la cassation prononcée. <br/>
<br/>
Article 3 : L'Etat et la commune de Six-Fours-les-Plages verseront à M. et Mme B...une somme de 1 500 euros chacun au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : Les conclusions présentées par la commune de Six-Fours-les-Plages au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 5 : La présente décision sera notifiée à M. et Mme A...B..., à la commune de Six-Fours-les-Plages et au ministre de la transition écologique et solidaire.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">44-035-05 NATURE ET ENVIRONNEMENT. - POUVOIRS DE POLICE (ART. L. 541-3 DU CODE DE L'ENVIRONNEMENT) - REFUS DE L'AUTORITÉ COMPÉTENTE D'EN FAIRE USAGE - ETENDUE DU CONTRÔLE DU JUGE - CONTRÔLE NORMAL.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">49-05 POLICE. POLICES SPÉCIALES. - DÉCHETS (ART. L. 541-3 DU CODE DE L'ENVIRONNEMENT) - CONTRÔLE DU JUGE SUR LE REFUS DE L'AUTORITÉ DE POLICE COMPÉTENTE DE FAIRE USAGE DE SES POUVOIRS - CONTRÔLE NORMAL.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-07-02-03 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. CONTRÔLE DU JUGE DE L'EXCÈS DE POUVOIR. APPRÉCIATIONS SOUMISES À UN CONTRÔLE NORMAL. - POUVOIRS DE POLICE EN MATIÈRE DE DÉCHETS (ART. L. 541-3 DU CODE DE L'ENVIRONNEMENT) - REFUS DE L'AUTORITÉ COMPÉTENTE D'EN FAIRE USAGE.
</SCT>
<ANA ID="9A"> 44-035-05 Il appartient au juge d'exercer un plein contrôle sur le respect de l'obligation incombant à l'autorité investie de pouvoir de police municipale de prendre les mesures nécessaires pour assurer l'élimination des déchets dont l'abandon, le dépôt ou le traitement présente des dangers pour l'environnement.</ANA>
<ANA ID="9B"> 49-05 Il appartient au juge d'exercer un plein contrôle sur le respect de l'obligation incombant à l'autorité investie de pouvoir de police municipale de prendre les mesures nécessaires pour assurer l'élimination des déchets dont l'abandon, le dépôt ou le traitement présente des dangers pour l'environnement.</ANA>
<ANA ID="9C"> 54-07-02-03 Il appartient au juge d'exercer un plein contrôle sur le respect de l'obligation incombant à l'autorité investie de pouvoir de police municipale de prendre les mesures nécessaires pour assurer l'élimination des déchets dont l'abandon, le dépôt ou le traitement présente des dangers pour l'environnement.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
