<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041938563</ID>
<ANCIEN_ID>JG_L_2020_05_000000422956</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/93/85/CETATEXT000041938563.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 1ère chambres réunies, 29/05/2020, 422956</TITRE>
<DATE_DEC>2020-05-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>422956</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP RICHARD</AVOCATS>
<RAPPORTEUR>Mme Yaël Treille</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:422956.20200529</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Le conseil départemental du Bas-Rhin de l'ordre des chirurgiens-dentistes a porté plainte contre M. A... B... devant la chambre disciplinaire de première instance de l'ordre des chirurgiens-dentistes d'Alsace. Par une décision du 27 octobre 2016, la chambre disciplinaire de première instance lui a infligé la sanction de l'avertissement. <br/>
<br/>
              Par une décision du 29 mars 2018, la chambre disciplinaire nationale de l'ordre des chirurgiens-dentistes a rejeté l'appel de M. B... contre cette décision. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 6 août et 6 novembre 2018 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette décision ; <br/>
<br/>
              2°) de mettre à la charge du conseil départemental du Bas-Rhin de l'ordre des chirurgiens-dentistes la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la santé publique ; <br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 modifiée ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Yaël Treille, auditeur,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Richard, avocat de M. B....<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par une décision du 13 octobre 2016, la chambre disciplinaire de première instance de la région Alsace de l'ordre des chirurgiens-dentistes a, sur la plainte du conseil départemental du Bas-Rhin de l'ordre des chirurgiens-dentistes, infligé à M. B..., chirurgien-dentiste salarié d'un centre de santé de Strasbourg géré par la Mutuelle générale de l'éducation nationale (MGEN), la sanction de l'avertissement, pour n'avoir pas assuré une garde de soins dentaires le 8 mai 2016. Par une décision du 29 mars 2018, la chambre disciplinaire nationale de l'ordre des chirurgiens-dentistes a rejeté l'appel formé par M. B... contre cette décision. M. B... se pourvoit en cassation contre cette dernière décision. <br/>
<br/>
              2. D'une part, aux termes de l'article R. 6315-7 du code de la santé publique : " Une permanence des soins dentaires, assurée par les chirurgiens-dentistes libéraux, les chirurgiens-dentistes collaborateurs et les chirurgiens-dentistes salariés des centres de santé, est organisée dans chaque département les dimanches et jours fériés. Les chirurgiens-dentistes y participent dans le cadre de leur obligation déontologique prévue à l'article R. 4127-245 ". Aux termes de l'article R. 6315-9 de ce code : " Pour chaque secteur, un tableau de permanence est établi pour une durée minimale de trois mois par le conseil départemental de l'ordre des chirurgiens-dentistes. Il précise le nom et le lieu de dispensation des actes de chaque chirurgien-dentiste sous réserve des exemptions prévues à l'article R. 4127-245 (...) ". <br/>
<br/>
              3. D'autre part, aux termes de l'article R. 4127-245 du code de la santé publique : " Il est du devoir de tout chirurgien-dentiste de prêter son concours aux mesures prises en vue d'assurer la permanence des soins et la protection de la santé. Sa participation au service de garde est obligatoire. Toutefois, des exemptions peuvent être accordées par le conseil départemental de l'ordre, compte tenu de l'âge, de l'état de santé et, éventuellement, de la spécialisation du praticien ". <br/>
<br/>
              4. Il résulte de l'ensemble de ces dispositions qu'un chirurgien-dentiste ne peut, sans méconnaître ses obligations déontologiques, s'abstenir délibérément de participer à la permanence des soins dentaires organisée par le conseil départemental de l'ordre des chirurgiens-dentistes, lorsqu'il ne bénéficie pas d'une exemption accordée sur le fondement de l'article R. 4127-245 du code de la santé publique.<br/>
<br/>
              5. En jugeant que, pour n'avoir pas assuré la garde du 8 mai 2016 pour laquelle il figurait sur le tableau de permanence, M. B... avait commis une faute déontologique, alors qu'il ressortait des pièces du dossier qui lui était soumis que son employeur avait refusé de mettre à sa disposition les moyens propres à lui permettre d'assurer effectivement sa garde dans le centre de santé où il exerce comme salarié et qu'il en avait informé par avance, plusieurs fois, le conseil départemental du Bas-Rhin de l'ordre des chirurgiens-dentistes en vue qu'une solution puisse être trouvée et que, dès lors, il ne pouvait être regardé comme s'étant délibérément abstenu de participer à la permanence des soins dentaires, la chambre disciplinaire nationale de l'ordre des chirurgiens-dentistes a inexactement qualifié les faits qui lui étaient soumis. Par suite, M. B... est fondé à demander l'annulation de la décision qu'il attaque.  <br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du conseil départemental du Bas-Rhin de l'ordre des chirurgiens-dentistes une somme de 3 000 euros à verser à M. B... au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : La décision de la chambre disciplinaire nationale de l'ordre des chirurgiens-dentistes du 29 mars 2018 est annulée. <br/>
Article 2 : L'affaire est renvoyée à la chambre disciplinaire nationale de l'ordre des chirurgiens-dentistes<br/>
Article 3 : Le conseil départemental du Bas-Rhin de l'ordre des chirurgiens-dentistes versera à M. B... une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 4 : La présente décision sera notifiée à M. A... B... et au conseil départemental du Bas-Rhin de l'ordre des chirurgiens-dentistes. <br/>
Copie en sera adressée au Conseil national de l'ordre des chirurgiens-dentistes. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">55-04-02-02-02 PROFESSIONS, CHARGES ET OFFICES. DISCIPLINE PROFESSIONNELLE. SANCTIONS. FAITS N'ÉTANT PAS DE NATURE À JUSTIFIER UNE SANCTION. CHIRURGIENS-DENTISTES. - CHIRURGIEN-DENTISTE SALARIÉ N'AYANT PAS ASSURÉ UNE GARDE ALORS QUE SON EMPLOYEUR LUI A REFUSÉ LES MOYENS NÉCESSAIRES ET QU'IL EN A INFORMÉ PAR AVANCE LE CONSEIL DE L'ORDRE [RJ1].
</SCT>
<ANA ID="9A"> 55-04-02-02-02 Chirurgien-dentiste salarié n'ayant pas assuré une garde pour laquelle il figurait sur le tableau de permanence alors que son employeur a refusé de mettre à sa disposition les moyens propres à lui permettre d'assurer effectivement sa garde dans le centre de santé où il exerce comme salarié et qu'il en a informé par avance, plusieurs fois, le conseil départemental de l'ordre en vue qu'une solution puisse être trouvée.,,,L'intéressé ne peut être regardé comme s'étant délibérément abstenu de participer à la permanence des soins dentaires et comme ayant ainsi commis une faute déontologique.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Comp., s'agissant d'un chirurgien-dentiste ayant refusé, sans motif valable, de participer au tour de garde, CE, 8 novembre 1993, M.,, n° 126599, T. pp. 560-992-1001-1038.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
