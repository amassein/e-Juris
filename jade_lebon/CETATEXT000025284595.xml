<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025284595</ID>
<ANCIEN_ID>JG_L_2012_02_000000338665</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/28/45/CETATEXT000025284595.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème et 3ème sous-sections réunies, 01/02/2012, 338665</TITRE>
<DATE_DEC>2012-02-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>338665</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème et 3ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Christian Vigouroux</PRESIDENT>
<AVOCATS>SCP COUTARD, MUNIER-APAIRE ; FOUSSARD</AVOCATS>
<RAPPORTEUR>Mme Marie-Astrid Nicolazo de Barmon</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Nathalie Escaut</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:338665.20120201</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
<br/>
              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 15 avril et 9 juillet 2010 au secrétariat du contentieux du Conseil d'Etat, présentés pour la SA RTE EDF TRANSPORT, dont le siège est 119 rue des Trois Fontanots à Nanterre (92024) ; la SA RTE EDF TRANSPORT demande au Conseil d'Etat :<br/>
<br/>
               1°) d'annuler l'article 3 de l'arrêt n° 07PA01825, 07PA01856 du 12 février 2010 par lequel la cour administrative d'appel de Paris, après avoir annulé le jugement n° 0319941 et 0500261 du 16 mars 2007 du tribunal administratif de Paris et la décision rectifiée du 27 mai 2003 du directeur général de l'Etablissement public pour l'aménagement de la Défense en tant qu'elle impose à la SA RTE EDF TRANSPORT, venant aux droits du Réseau de Transport d'Electricité de France, de supporter à ses frais les modifications de son réseau quand celles-ci sont imposées par d'autres occupants pour le fonctionnement de leur service public, a rejeté le surplus de ses conclusions présentées devant le tribunal administratif et la cour administrative d'appel de Paris ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa requête d'appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etablissement public pour l'aménagement de La Défense la somme de 3 000 euros en application des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi du 15 juillet 1906 sur les distributions d'énergie électrique ;<br/>
<br/>
              Vu le décret n° 58-815 du 9 septembre 1958 ;<br/>
<br/>
              Vu le décret n° 2005-1676 du 27 décembre 2005 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie-Astrid Nicolazo de Barmon, Auditeur,  <br/>
<br/>
              - les observations de la SCP Coutard, Munier-Apaire, avocat de la SA RTE EDF TRANSPORT et de Me Foussard, avocat de la société Etablissement public pour l'aménagement de la région Ile-de-France (EPAD), <br/>
<br/>
              - les conclusions de Mme Nathalie Escaut, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Coutard, Munier-Apaire, avocat de la SA RTE EDF TRANSPORT et à Me Foussard, avocat de l'Etablissement public d'aménagement de La Défense Seine Arche ; <br/>
<br/>
<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par décision du 27 mai 2003, modifiée par une décision rectificative du 17 novembre 2004, l'établissement public pour l'aménagement de La Défense (EPAD) a accordé au service Réseau de Transport d'Electricité de France (RTE) une autorisation d'occupation temporaire du domaine public et notamment des galeries techniques et autres ouvrages souterrains empruntés par le réseau de transport et de distribution d'électricité ; que l'article 6-1 de cette autorisation prévoyait le versement d'une redevance en contrepartie du droit d'occuper le domaine public de l'établissement ; que son article 8-2 précisait les conditions dans lesquelles RTE devrait procéder à la modification des éléments de son réseau à la demande de l'EPAD ; que, par un jugement du 16 mars 2007, le tribunal administratif de Paris, faisant partiellement droit à la demande de la SA RTE EDF TRANSPORT, venant aux droits de RTE, a annulé la décision rectifiée de l'EPAD du 27 mai 2003 en tant, d'une part, qu'elle soumettait à redevance les 700 mètres linéaires du réseau de transport d'électricité situé hors des galeries techniques au motif que l'EPAD n'établissait pas être propriétaire de ce réseau, quand bien même il serait implanté dans son périmètre d'intervention et, d'autre part, qu'elle imposait à RTE de supporter à ses frais les modifications de son réseau imposées par d'autres occupants du domaine pour le fonctionnement de leur service public ; que la SA RTE EDF TRANSPORT et l'EPAD ont fait appel de ce jugement ; que la cour administrative d'appel de Paris, après en avoir prononcé l'annulation, a, par l'article 2 de son arrêt du 12 février 2010, confirmé l'annulation partielle de l'article 8-2 de l'autorisation du 27 mai 2003 en tant qu'il imposait à RTE de supporter à ses frais les modifications de son réseau imposées par d'autres occupants du domaine et, par son article 3, rejeté le surplus des conclusions présentées par les requérants tant en première instance qu'en appel ; que la SA RTE EDF TRANSPORT se pourvoit en cassation contre l'article 3 de cet arrêt ; que, par la voie du pourvoi incident, l'établissement public d'aménagement de La Défense Seine Arche, venant aux droits de l'EPAD, demande l'annulation de son article 2 ; <br/>
<br/>
              Sur le pourvoi de la SA RTE EDF TRANSPORT :<br/>
<br/>
              Considérant, en premier lieu, qu'aux termes de l'article R. 222-29 du code de justice administrative, dans sa rédaction alors en vigueur : " La formation de jugement ou le président de la cour peuvent, à tout moment de la procédure, décider d'inscrire une affaire au rôle de la cour statuant en formation plénière. " ; qu'aux termes de l'article R. 611-7 du même code : " Lorsque la décision lui paraît susceptible d'être fondée sur un moyen relevé d'office, le président de la formation de jugement (...) en informe les parties avant la séance de jugement et fixe le délai dans lequel elles peuvent, sans qu'y fasse obstacle la clôture éventuelle de l'instruction, présenter leurs observations sur le moyen communiqué. " ; <br/>
<br/>
              Considérant que, par lettre du 19 janvier 2010, le président de la cour administrative d'appel de Paris, en application de l'article R. 222-29 du code de justice administrative, a informé les parties du renvoi de l'affaire devant la formation plénière de la cour ; que si le président de la cour a précisé dans cette lettre les questions justifiant l'examen de l'affaire par cette formation de jugement, il n'a ce faisant énoncé aucun moyen d'ordre public sur lequel la société requérante aurait dû, conformément à l'article R. 611-7 du code de justice administrative, être invitée à présenter ses observations, ni soulevé aucune question nouvelle pouvant justifier que les parties fussent invitées à prendre position ; que, par suite, la SA RTE EDF TRANSPORT n'est pas fondée à soutenir que la procédure suivie devant la cour aurait méconnu les dispositions de cet article ou le principe du caractère contradictoire de la procédure, faute d'avoir été invitée dans cette lettre à produire ses observations sur les motifs du renvoi de l'affaire en formation plénière ; <br/>
<br/>
              Considérant, en deuxième lieu, qu'aux termes de l'article R. 613-3 du code de justice administrative : " Les mémoires produits après la clôture de l'instruction ne donnent pas lieu à communication et ne sont pas examinés par la juridiction. (...) " ; que lorsque, postérieurement à la clôture de l'instruction, le juge est saisi d'un mémoire émanant de l'une des parties à l'instance, et conformément au principe selon lequel devant les juridictions administratives, le juge dirige l'instruction, il lui appartient, dans tous les cas, d'en prendre connaissance avant de rendre sa décision, ainsi que de le viser sans l'analyser ; que s'il a toujours la faculté, dans l'intérêt d'une bonne justice, d'en tenir compte, après l'avoir, cette fois, analysé, il n'est tenu de le faire, à peine d'irrégularité de sa décision, que si ce mémoire contient l'exposé d'une circonstance de fait dont la partie qui l'invoque n'était pas en mesure de faire état avant la clôture de l'instruction écrite et que le juge ne pourrait ignorer sans fonder sa décision sur des faits matériellement inexacts, soit d'une circonstance nouvelle ou que le juge devrait relever d'office ; que dans tous les cas où il est amené à tenir compte de ce mémoire, à l'exception de l'hypothèse dans laquelle il se fonde sur un moyen qu'il devait relever d'office, il doit le soumettre au débat contradictoire ; <br/>
<br/>
              Considérant que l'EPAD a produit des observations enregistrées au greffe de la cour le 4 février 2010, la veille de l'audience, postérieurement à la clôture de l'instruction intervenue, en l'absence d'ordonnance du président de la formation de jugement, trois jours francs avant la date de l'audience ; que si l'EPAD s'efforçait dans ce mémoire d'étayer son argumentation relative à l'appartenance des installations hors galeries techniques à son domaine public, cette question était en débat depuis le début de l'instance devant le juge d'appel ; que l'EPAD avait développé dans ses précédentes écritures, notamment dans sa note en délibéré du 18 novembre 2008, les motifs retenus par la cour et tirés de ce qu'il avait, dans le cadre de sa mission, construit les voies en litige, sises dans son périmètre d'intervention, et qu'il pouvait dès lors délivrer des autorisations d'occupation sur cette partie du domaine public ; que, par suite, la cour n'était pas tenue de communiquer le mémoire du 4 février 2010 à la SA RTE EDF TRANSPORT à peine d'irrégularité de sa décision ; qu'il résulte de ce qui précède que la cour n'a pas méconnu le principe du caractère contradictoire de la procédure ; <br/>
<br/>
              Considérant, en troisième lieu, d'une part, qu'aux termes de l'article 1er du décret du 9 septembre 1958 créant un établissement public pour l'aménagement de la région dite de " La Défense " dans le département de la Seine : " Il est créé un établissement public de caractère industriel et commercial chargé de procéder à toutes opérations de nature à faciliter la réalisation du projet d'aménagement de la région dite de " La Défense " et notamment : de procéder à l'acquisition, au besoin par voie d'expropriation, des immeubles bâtis ou non bâtis nécessaires à la réalisation des travaux d'aménagement et d'équipement et éventuellement de construction de logements prévus au projet d'aménagement susvisé ; de procéder, dans les conditions prévues aux articles 141 et suivants du code de l'urbanisme et de l'habitation, à la cession des immeubles acquis en vue de leur affectation conforme aux prévisions du projet d'aménagement " ; <br/>
<br/>
              Considérant, d'autre part, qu'aux termes de l'article 10 la loi du 15 juin 1906 sur les distributions d'énergie électrique, alors applicable : " La concession confère à l'entrepreneur le droit d'exécuter sur les voies publiques et leurs dépendances tous travaux nécessaires à l'établissement et à l'entretien des ouvrages en se conformant aux conditions du cahier des charges, des règlements de voirie, et des règlements d'administration publique (...) " ;<br/>
<br/>
              Considérant que toute occupation privative du domaine public est en principe subordonnée à la délivrance d'une autorisation et au paiement d'une redevance ; que si la loi du 15 juin 1906 a eu pour objet de conférer à titre permanent aux entreprises concessionnaires du réseau de distribution et de transport d'électricité le droit d'occuper sans autorisation les voies publiques afin d'y réaliser leur mission de service public, ses dispositions n'instaurent pas une dérogation au principe du paiement d'une redevance pour l'occupation du domaine public ; que, dès lors, en jugeant qu'elles avaient conféré à Electricité de France le droit d'exécuter sur les voies publiques du quartier de La Défense tous travaux nécessaires à l'établissement et à l'entretien des ouvrages de transport d'électricité mais n'avaient ni pour objet ni pour effet d'attribuer à la société requérante le droit d'occuper à titre gratuit les ouvrages enterrés sous ces voies, la cour n'a pas commis d'erreur de droit ; que si elle s'est référée, de manière inexacte, aux " galeries techniques enterrées " et non aux ouvrages hors galeries techniques, mentionnés par la société requérante à l'appui de son moyen, cette erreur est sans incidence sur le bien-fondé des motifs de l'arrêt attaqué ;<br/>
<br/>
              Considérant, en quatrième lieu, qu'en l'absence de dispositions contraires, il appartient à l'autorité chargée de la gestion du domaine public en cause, qu'elle en soit ou non le propriétaire, d'octroyer les permissions d'occupation et de fixer les conditions auxquelles elle entend subordonner leur délivrance et, à ce titre, de déterminer le tarif des redevances en tenant compte des avantages de toute nature que le permissionnaire est susceptible de retirer de l'occupation du domaine public ; que le décret précité du 9 septembre 1958 a entendu confier à l'EPAD non seulement un rôle d'aménageur mais également une mission de gestionnaire des dépendances du domaine public sises dans son périmètre d'intervention jusqu'à leur éventuelle cession ou au transfert de leur gestion à des tiers ; qu'ainsi, quelle que soit la collectivité propriétaire des biens en litige, l'EPAD était compétent pour délivrer les autorisations d'occupation du domaine dont il était le gestionnaire et fixer les modalités de la redevance due par les occupants de ce domaine ; <br/>
<br/>
              Considérant que, s'agissant des ouvrages hors galeries techniques par lesquels transite le réseau électrique de RTE, situés, d'une part, entre l'avenue Gambetta et la rue Guynemer et passant sous le boulevard circulaire et la rue Ségoffin, d'autre part, entre la galerie Pont de Neuilly et la galerie Alsace et passant sous la rue Louis Blanc, la cour a relevé que ces voies, sises dans le périmètre d'intervention de l'EPAD, avaient été construites par lui, dans le cadre de la mission qui lui avait été confiée par le décret du 9 septembre 1958, de faciliter la réalisation du projet d'aménagement du quartier de La Défense ; qu'en en déduisant qu'à ce titre, et tant qu'il n'était pas établi que la gestion de tout ou partie de ces ouvrages aurait été dévolue à une autre collectivité publique, l'EPAD devait être regardé comme ayant le droit d'y délivrer des autorisations d'occupation domaniale, moyennant redevance, dans les mêmes conditions que pour son domaine propre, la cour n'a pas commis d'erreur de droit, dès lors, d'une part, qu'elle n'a pas inversé la charge de la preuve au détriment de la SA RTE EDF TRANSPORT mais s'est prononcée au vu des éléments fournis par l'une et l'autre partie et, d'autre part, qu'elle n'a pas déduit le droit pour l'EPAD de délivrer des autorisations d'occupation domaniale d'une présomption d'appartenance des dépendances domaniales en cause à son domaine propre ; que la cour a pu se borner à constater que l'EPAD avait, en sa seule qualité d'aménageur, acquis la qualité de gestionnaire de ces dépendances, sans être tenue de déterminer quelle collectivité était propriétaire des voies litigieuses ; que par suite, la cour, qui n'a pas dénaturé les écritures de la SA RTE EDF TRANSPORT, a suffisamment motivé son arrêt ; <br/>
<br/>
              Considérant, en cinquième lieu, qu'ainsi qu'il a été dit ci-dessus, il appartient à l'autorité gestionnaire du domaine public de fixer, tant dans l'intérêt du domaine que dans l'intérêt général, les conditions de délivrance des permissions d'occupation et à ce titre de déterminer le tarif des redevances en tenant compte des avantages de toute nature que le permissionnaire est susceptible de retirer de l'occupation du domaine public ; que ces règles trouvent à s'appliquer, même en l'absence de toute réglementation particulière, à l'établissement public autorisé à délivrer les permissions d'occupation sur le domaine public dont la gestion lui a été confiée ; <br/>
<br/>
              Considérant qu'en limitant son contrôle, outre à l'erreur de droit au regard des critères rappelés ci-dessus, à l'erreur manifeste d'appréciation qu'aurait commise l'EPAD dans la détermination du montant de la redevance d'occupation domaniale mise à la charge de RTE par l'article 6-1 de l'autorisation du 27 mai 2003, la cour n'a pas commis d'erreur de droit ; que la cour, qui n'était pas tenue de répondre à l'argument de la société requérante tiré de ce que cette redevance était supérieure à celle qui peut être exigée des opérateurs de communications électroniques en vertu du décret du 27 décembre 2005, au demeurant entré en vigueur postérieurement à l'autorisation attaquée, a suffisamment motivé son arrêt ; qu'en estimant d'une part que le montant de la redevance litigieuse tenait compte des avantages spécifiques que procurent à l'occupant la localisation des galeries techniques dans le quartier d'affaires de La Défense et la qualité particulière de l'aménagement de ces galeries, visitables, éclairées, équipées de pompes de relevage, de détecteurs d'incendie, d'issues de secours balisées et dotées d'un poste central de sécurité fonctionnant vingt-quatre heures sur vingt-quatre et d'un logiciel de gestion technique centralisée d'électricité, qui sont ainsi spécialement étudiées pour la desserte des fluides et adaptées aux contraintes particulières imposées par la nature du réseau d'électricité, d'autre part que la circonstance qu'une faible partie du réseau concerné ne dispose pas de la qualité d'aménagement des galeries techniques n'était pas de nature à remettre en cause la très grande importance de l'avantage global offert à RTE en termes de localisation, d'accessibilité, de sécurité et d'économie du coût de construction de son réseau, et en déduisant de ces constats que la décision de l'EPAD de fixer à 9,53 euros par mètre linéaire le montant de la redevance n'était pas entachée d'une erreur manifeste d'appréciation, la cour s'est livrée à une appréciation souveraine exempte de dénaturation ; <br/>
<br/>
              Considérant qu'il résulte de tout ce qui précède que le pourvoi de la SA RTE EDF TRANSPORT doit être rejeté ; <br/>
<br/>
              Sur le pourvoi incident de l'EPAD :<br/>
<br/>
              Considérant que le titulaire d'une autorisation d'occupation temporaire du domaine public doit supporter sans indemnité la charge résultant du déplacement et de la modification de ses ouvrages lorsque ce déplacement ou cette modification sont la conséquence de travaux entrepris dans l'intérêt du domaine public occupé, en vue d'en faciliter ou d'en améliorer la gestion conformément à sa destination ; qu'à l'inverse, lorsque les travaux n'ont pas eu pour seul objet l'intérêt de ce domaine et alors même qu'ils présenteraient, dans leur ensemble, un caractère d'utilité générale, ou seraient nécessaires au bon fonctionnement d'un service public assuré par un autre occupant du domaine, le permissionnaire est fondé à demander le remboursement de ses dépenses à concurrence de la somme correspondant aux travaux exécutés dans un intérêt autre que celui du domaine qu'il occupe ; <br/>
<br/>
              Considérant que la cour a souverainement relevé que l'article 8-2 de la décision du 27 mai 2003 pouvait conduire à mettre à la charge de RTE les travaux de modification de son réseau rendus nécessaires par les travaux d'aménagement entrepris à l'initiative d'autres occupants pour le fonctionnement du service public dont ils ont la charge ; qu'en en déduisant que ces stipulations n'étaient pas édictées dans l'intérêt exclusif du domaine mais pour celui d'autres occupants, et devaient dès lors, dans cette mesure, être annulées, la cour n'a pas commis d'erreur de droit ; que l'EPAD n'est, par suite, pas fondé à demander l'annulation de l'article 2 de l'arrêt attaqué ; <br/>
<br/>
              Sur les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de l'Etablissement public d'aménagement de La Défense Seine Arche, qui n'est pas, dans la présente instance, la partie perdante ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par cet établissement au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>		D E C I D E :<br/>
              		--------------<br/>
<br/>
Article 1er : Le pourvoi de la SA RTE EDF TRANSPORT et le pourvoi incident de l'Etablissement public d'aménagement de La Défense Seine Arche sont rejetés. <br/>
<br/>
Article 2 : La présente décision sera notifiée à la SA RTE EDF TRANSPORT et à l'Etablissement public d'aménagement de La Défense Seine Arche.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">24-01-02-01-01-04 DOMAINE. DOMAINE PUBLIC. RÉGIME. OCCUPATION. UTILISATIONS PRIVATIVES DU DOMAINE. REDEVANCES. - 1) ARTICLE 10 DE LA LOI DU 15 JUIN 1906 CONFÉRANT AUX CONCESSIONNAIRES DU RÉSEAU DE DISTRIBUTION ET DE TRANSPORT D'ÉLECTRICITÉ LE DROIT D'OCCUPER SANS AUTORISATION LES VOIES PUBLIQUES AFIN D'Y RÉALISER LEUR MISSION DE SERVICE PUBLIC - DÉROGATION AU PRINCIPE DU PAIEMENT D'UNE REDEVANCE POUR L'OCCUPATION DU DOMAINE PUBLIC - ABSENCE - CONSÉQUENCE - DROIT DU CONCESSIONNAIRE D'OCCUPER À TITRE GRATUIT LES OUVRAGES ENTERRÉS SOUS LES VOIES PUBLIQUES DU QUARTIER DE LA DÉFENSE - ABSENCE - 2) EPAD - RÔLE DE GESTIONNAIRE DES DÉPENDANCES DU DOMAINE PUBLIC SISES SUR SON PÉRIMÈTRE D'INTERVENTION - CONSÉQUENCE - COMPÉTENCE POUR DÉLIVRER LES AUTORISATIONS D'OCCUPATION ET FIXER LES MODALITÉS DE LA REDEVANCE DUE PAR LES OCCUPANTS [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">29-04 ENERGIE. LIGNES ÉLECTRIQUES. - ARTICLE 10 DE LA LOI DU 15 JUIN 1906 CONFÉRANT AUX CONCESSIONNAIRES DU RÉSEAU DE DISTRIBUTION ET DE TRANSPORT D'ÉLECTRICITÉ LE DROIT D'OCCUPER SANS AUTORISATION LES VOIES PUBLIQUES AFIN D'Y RÉALISER LEUR MISSION DE SERVICE PUBLIC - DÉROGATION AU PRINCIPE DU PAIEMENT D'UNE REDEVANCE POUR L'OCCUPATION DU DOMAINE PUBLIC - ABSENCE - CONSÉQUENCE - DROIT DU CONCESSIONNAIRE D'OCCUPER À TITRE GRATUIT LES OUVRAGES ENTERRÉS SOUS LES VOIES PUBLIQUES DU QUARTIER DE LA DÉFENSE - ABSENCE.
</SCT>
<ANA ID="9A"> 24-01-02-01-01-04 1) Si l'article 10 de la loi du 15 juin 1906 sur les distributions d'énergie électrique, désormais repris à l'article L. 323-1 du code de l'énergie, a eu pour objet de conférer à titre permanent aux entreprises concessionnaires du réseau de distribution et de transport d'électricité le droit d'occuper sans autorisation les voies publiques afin d'y réaliser leur mission de service public, ces dispositions n'instaurent pas une dérogation au principe du paiement d'une redevance pour l'occupation du domaine public. Dès lors, elles n'ont ni pour objet ni pour effet d'attribuer à la société RTE EDF Transport le droit d'occuper à titre gratuit les ouvrages enterrés sous les voies publiques du quartier de La Défense.,,2) Le décret n° 58-815 du 9 septembre 1958 créant un établissement public pour l'aménagement de la région dite de « La Défense » dans le département de la Seine a entendu confier à l'établissement public pour l'aménagement de La Défense (EPAD) non seulement un rôle d'aménageur, mais également une mission de gestionnaire des dépendances du domaine public sises dans son périmètre d'intervention jusqu'à leur éventuelle cession ou au transfert de leur gestion à des tiers. Ainsi, quelle que soit la collectivité propriétaire, l'EPAD est compétent dans ces limites pour délivrer les autorisations d'occupation du domaine dont il est le gestionnaire et fixer les modalités de la redevance due par les occupants de ce domaine.</ANA>
<ANA ID="9B"> 29-04 Si l'article 10 de la loi du 15 juin 1906 sur les distributions d'énergie électrique, désormais repris à l'article L. 323-1 du code de l'énergie, a eu pour objet de conférer à titre permanent aux entreprises concessionnaires du réseau de distribution et de transport d'électricité le droit d'occuper sans autorisation les voies publiques afin d'y réaliser leur mission de service public, ces dispositions n'instaurent pas une dérogation au principe du paiement d'une redevance pour l'occupation du domaine public. Dès lors, elles n'ont ni pour objet ni pour effet d'attribuer à la société RTE EDF Transport le droit d'occuper à titre gratuit les ouvrages enterrés sous les voies publiques du quartier de La Défense.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., pour la compétence, sauf dispositions contraires, du gestionnaire non propriétaire du domaine pour fixer les redevances dues par les occupants de ce domaine, CE, 8 juillet 1996, Merie, n° 121520, p. 272 ; CE, 10 juin 2010, Société des autoroutes Esterel-Côte-d'Azur-Provence-Alpes, n° 305136, T. p 762. Rappr. CE, 15 avril 2011, Société nationale des chemins de fer français, n° 308014, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
