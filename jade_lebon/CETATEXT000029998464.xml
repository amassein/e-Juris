<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029998464</ID>
<ANCIEN_ID>JG_L_2014_12_000000381391</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/99/84/CETATEXT000029998464.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème / 10ème SSR, 30/12/2014, 381391</TITRE>
<DATE_DEC>2014-12-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>381391</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème / 10ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP COUTARD, MUNIER-APAIRE ; SCP BARADUC, DUHAMEL, RAMEIX</AVOCATS>
<RAPPORTEUR>M. Bastien Lignereux</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Aladjidi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:381391.20141230</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
Procédure contentieuse antérieure<br/>
<br/>
              La société Or vert a demandé au juge des référés du tribunal administratif de Pau, d'une part, de suspendre, sur le fondement des dispositions de l'article L. 521-1 du code de justice administrative, l'exécution des décisions des 19 novembre 2012, 6 février 2013 et 27 janvier 2014 par lesquelles la société Electricité de France (EDF) a refusé la conclusion d'un contrat de rachat de l'électricité qu'elle produit aux tarifs fixés par l'arrêté du 12 janvier 2010 et, d'autre part, d'enjoindre à la société EDF de conclure ce contrat d'achat sous astreinte de 500 euros par jour de retard. <br/>
<br/>
              Par une ordonnance n° 1400917 du 26 mai 2014, le juge des référés du tribunal administratif de Pau a prononcé la suspension de l'exécution de la décision du 27 janvier 2014, enjoint à la société EDF de réexaminer la demande de la société requérante et rejeté le surplus des conclusions de la requête.<br/>
<br/>
Procédure devant le Conseil d'Etat<br/>
<br/>
              Par un pourvoi, un mémoire en réplique et un nouveau mémoire, enregistrés les 18 juin, 9 septembre et 21 novembre 2014 au secrétariat du contentieux du Conseil d'Etat, la société EDF demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 1400917 du 26 mai 2014 du juge des référés du tribunal administratif de Pau en tant qu'elle a fait droit aux conclusions de la société Or vert ;<br/>
<br/>
              2°) statuant en référé, de rejeter le surplus des conclusions de la demande présentée par la société Or vert ;<br/>
<br/>
              3°) de mettre à la charge de la société Or vert la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu :<br/>
              - les autres pièces du dossier ;<br/>
              - le code civil ;<br/>
              - le code de l'énergie ;<br/>
              - le décret n° 2010-1510 du 9 décembre 2010 ;<br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bastien Lignereux, auditeur,  <br/>
<br/>
              - les conclusions de M. Frédéric Aladjidi, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Coutard, Munier-Apaire, avocat de la société EDF et à la SCP Baraduc, Duhamel, Rameix, avocat de la société Or vert ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision. (...) " ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article 1er du décret du 9 décembre 2010 suspendant l'obligation d'achat de l'électricité produite par certaines installations utilisant l'énergie radiative du soleil : " L'obligation de conclure un contrat d'achat de l'électricité produite par les installations mentionnées au 3° de l'article 2 du décret du 6 décembre 2000 susvisé est suspendue pour une durée de trois mois courant à compter de l'entrée en vigueur du présent décret. Aucune nouvelle demande ne peut être déposée durant la période de suspension " ; qu'aux termes de l'article 3 de ce décret : " Les dispositions de l'article 1er ne s'appliquent pas aux installations de production d'électricité issue de l'énergie radiative du soleil dont le producteur a notifié au gestionnaire de réseau, avant le 2 décembre 2010, son acceptation de la proposition technique et financière de raccordement au réseau " ; qu'aux termes de l'article 4  du même décret : " Le bénéfice de l'obligation d'achat au titre de l'article 3 est subordonné à la mise en service de l'installation dans un délai de dix-huit mois à compter de la notification de l'acceptation de la proposition technique et financière de raccordement au réseau ou, lorsque cette notification est antérieure de plus de neuf mois à la date d'entrée en vigueur du présent décret, à la mise en service de l'installation dans les neuf mois suivant cette date. / Les délais mentionnés au premier alinéa sont prolongés lorsque la mise en service de l'installation est retardée du fait des délais nécessaires à la réalisation des travaux de raccordement et à condition que l'installation ait été achevée dans les délais prévus au premier alinéa. La mise en service de l'installation doit, dans tous les cas, intervenir au plus tard deux mois après la fin des travaux de raccordement. / La date de mise en service de l'installation correspond à la date de mise en service de son raccordement au réseau " ; qu'enfin, aux termes de son article 5 : " A l'issue de la période de suspension mentionnée à l'article 1er, les demandes suspendues devront faire l'objet d'une nouvelle demande complète de raccordement au réseau pour bénéficier d'un contrat d'obligation d'achat " ;<br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier soumis au juge des référés du tribunal administratif de Pau que, le 24 août 2010, la société Electricité réseau distribution France (ERDF) a adressé à la société Or vert, en réponse à sa demande, une proposition technique et financière de raccordement de son installation de production d'énergie photovoltaïque au réseau, que la société Or vert a retournée signée à la société ERDF le 13 septembre suivant ; que, par une lettre du 29 décembre 2010, la société ERDF, alors que la demande de la société Or vert entrait dans le champ d'application de la dérogation prévue par l'article 3 du décret du 9 décembre 2010, l'a informée qu'elle ne pouvait plus donner suite à sa demande de raccordement ; qu'après avoir saisi, le 7 février 2012, le comité de règlement des différends et des sanctions (CORDIS) de la Commission de régulation de l'énergie, la société Or vert a conclu avec la société ERDF, le 11 juin suivant, une transaction par laquelle cette dernière, reconnaissant qu'elle avait à tort fait application de l'article 1er du décret à la demande de la société Or vert, acceptait de réintégrer son projet à son rang dans la " file d'attente " de raccordement au réseau, la société Or vert se désistant alors de sa demande de règlement du différend présentée au CORDIS ; que, par des décisions du 19 novembre 2012, 6 février 2013 et 27 janvier 2014, la société EDF a néanmoins refusé de faire droit à la demande de conclusion d'un contrat d'achat de l'électricité produite par la société Or vert, l'invitant à présenter une nouvelle demande ; que, par l'ordonnance attaquée du 26 mai 2014, le juge des référés du tribunal administratif de Pau a suspendu l'exécution de la décision du 27 janvier 2014 aux motifs que, la notification de l'acceptation de la proposition technique de raccordement d'ERDF répondant aux conditions posées par l'article 3 du décret et la mise en service de l'installation ayant eu lieu dans le délai de dix-huit mois prévu à l'article 4 du même décret, qui courait à compter du 11 juin 2012, date de la conclusion de la transaction entre la société Or vert et ERDF, laquelle devait être regardée comme le point de départ de ce délai, le moyen tiré de ce que la société Or vert était en droit de bénéficier d'un contrat de rachat d'électricité dans les conditions prévues par l'arrêté du 12 janvier 2010 était propre, en l'état de l'instruction, à créer un doute sérieux sur la légalité du refus de rachat opposé à cette société ; <br/>
<br/>
              4. Considérant que les dispositions de l'article 4 du décret du 9 décembre 2010, citées au point 2, subordonnent le bénéfice de l'obligation d'achat, pour les installations dont le producteur a notifié son acceptation de la proposition technique et financière de raccordement au réseau au gestionnaire de réseau avant le 2 décembre 2010, à la mise en service de l'installation dans un délai de dix-huit mois à compter de cette notification, lorsque celle-ci n'a pas été antérieure de plus de neuf mois à la date d'entrée en vigueur de ce décret, le délai de dix-huit mois étant prolongé lorsque la mise en service de l'installation a été retardée du fait des délais nécessaires à la réalisation des travaux de raccordement, l'installation devant, en tout état de cause, être achevée dans ce délai et sa mise en service intervenir au plus tard deux mois après la fin de ces travaux ; que, dans l'hypothèse où le gestionnaire de réseau a, en méconnaissance des dispositions précitées de l'article 3 de ce décret, décidé de sortir un projet de la " file d'attente " de raccordement alors qu'il entrait dans le champ d'application des dispositions de cet article, cette circonstance ne saurait ouvrir droit au bénéfice de l'obligation d'achat lorsque la condition d'achèvement de l'installation dans le délai de dix-huit mois n'a pas été satisfaite ; que la transaction conclue entre les sociétés ERDF et Or vert, à laquelle la société EDF n'est pas partie, ne saurait produire d'effets à l'égard de celle-ci et est, dès lors, quelles que soient ses stipulations, sans incidence sur la computation de ce délai de dix-huit mois ; que, par suite, en regardant cette transaction comme le point de ce délai, le juge des référés du tribunal administratif a commis une erreur de droit ; qu'EDF est ainsi fondée, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, à demander l'annulation de l'ordonnance attaquée en tant qu'elle a fait droit aux conclusions de la société Or vert ;<br/>
<br/>
              5. Considérant qu'il y a lieu, en application des dispositions de l'article L. 821-2 du code de justice administrative, de régler, dans la mesure de la cassation prononcée, l'affaire au titre de la procédure de référé engagée par la société Or vert ;<br/>
<br/>
              6. Considérant que ni le moyen tiré de ce que le refus litigieux aurait été pris par une autorité incompétente, ni le moyen tiré de ce que cette décision n'aurait pas été suffisamment motivée au regard de l'article 1er de la loi du 11 juillet 1979 relative à la motivation des actes administratifs et à l'amélioration des relations entre l'administration et le public, ni, eu égard à ce qui a été dit au point 4, le moyen tiré de ce que, le délai de dix-huit prévu à l'article 4 du décret du 9 décembre 2010 courant à compter de la date de la transaction passée entre ERDF et la société Or vert, cette dernière était en droit d'obtenir le bénéfice d'une obligation de rachat, ne sont propres, en l'état de l'instruction, à créer un doute sérieux sur la légalité du refus opposé à la société Or Vert ; qu'ainsi, l'une des conditions auxquelles les dispositions de l'article L. 521-1 du code de justice administrative subordonnent la suspension de l'exécution d'une décision administrative n'est pas remplie en l'espèce ; que, dès lors, sans qu'il soit besoin de se prononcer sur la condition d'urgence, la société Or vert n'est pas fondée à demander la suspension de l'exécution de la décision du 27 janvier 2014 ; que, par suite, ses conclusions aux fins d'injonction et d'astreinte ne peuvent qu'être rejetées ;<br/>
<br/>
              7. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de la société EDF, qui n'est pas, dans la présente instance, la partie perdante ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par la société EDF au titre de ces dispositions ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
Article 1er : L'ordonnance du juge des référés du tribunal administratif de Pau du 26 mai 2014 est annulée en tant qu'elle a fait droit aux conclusions de la société Or vert.<br/>
Article 2 : Le surplus des conclusions de la demande présentée par la société Or vert devant le juge des référés du tribunal administratif de Pau est rejeté.<br/>
Article 3 : Le surplus des conclusions du pourvoi de la société EDF est rejeté.<br/>
Article 4 : La présente décision sera notifiée à la société EDF et à la société Or vert.<br/>
Copie en sera adressée pour information aux ministres de l'écologie, du développement durable et de l'énergie et de l'économie, de l'industrie et du numérique, ainsi qu'à la société Electricité réseau distribution France.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">29-036 ENERGIE. - BÉNÉFICE DE L'OBLIGATION D'ACHAT DE L'ÉLECTRICITÉ PRODUITE PAR CERTAINES INSTALLATIONS PHOTOVOLTAÏQUES - 1) CONDITION - MISE EN SERVICE DE L'INSTALLATION DANS UN DÉLAI DE DIX-HUIT MOIS À COMPTER DE LA NOTIFICATION PAR LE PRODUCTEUR AU GESTIONNAIRE DE RÉSEAU DE SON ACCEPTATION DE LA PROPOSITION TECHNIQUE ET FINANCIÈRE DE RACCORDEMENT AU RÉSEAU (ART. 4 DU DÉCRET N° 2010-1510 DU 9 DÉCEMBRE 2010) - 2) CAS OÙ LE GESTIONNAIRE DE RÉSEAU A ILLÉGALEMENT DÉCIDÉ DE SORTIR UN PROJET DE LA  FILE D'ATTENTE  DE RACCORDEMENT - CIRCONSTANCE SANS INCIDENCE SUR LE RESPECT DE LA CONDITION  D'ACHÈVEMENT DE L'INSTALLATION DANS LE DÉLAI DE DIX-HUIT MOIS - 3) TRANSACTION ENTRE LE GESTIONNAIRE DE RÉSEAU ET LE PORTEUR DU PROJET - INCIDENCE SUR LA COMPUTATION DE CE DÉLAI DE DIX-HUIT MOIS - ABSENCE.
</SCT>
<ANA ID="9A"> 29-036 1) Les dispositions de l'article 4 du décret n° 2010-1510 du 9 décembre 2010 (suspendant l'obligation d'achat de l'électricité produite par certaines installations utilisant l'énergie radiative du soleil) subordonnent le bénéfice de l'obligation d'achat, pour les installations dont le producteur a notifié son acceptation de la proposition technique et financière de raccordement au réseau au gestionnaire de réseau avant le 2 décembre 2010, à la mise en service de l'installation dans un délai de dix-huit mois à compter de cette notification, lorsque celle-ci n'a pas été antérieure de plus de neuf mois à la date d'entrée en vigueur de ce décret, le délai de dix-huit mois étant prolongé lorsque la mise en service de l'installation a été retardée du fait des délais nécessaires à la réalisation des travaux de raccordement, l'installation devant, en tout état de cause, être achevée dans ce délai et sa mise en service intervenir au plus tard deux mois après la fin de ces travaux.,,,2) Dans l'hypothèse où le gestionnaire de réseau a, en méconnaissance des dispositions de l'article 3 de ce décret, décidé de sortir un projet de la  file d'attente  de raccordement alors qu'il entrait dans le champ d'application des dispositions de cet article, cette circonstance ne saurait ouvrir droit au bénéfice de l'obligation d'achat lorsque la condition d'achèvement de l'installation dans le délai de dix-huit mois n'a pas été satisfaite.... ,,3) Une transaction conclue entre la société gestionnaire de réseau et la société porteuse du projet, à laquelle la société EDF n'est pas partie, ne saurait produire d'effets à l'égard de celle-ci et est, dès lors, quelles que soient ses stipulations, sans incidence sur la computation de ce délai de dix-huit mois.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
