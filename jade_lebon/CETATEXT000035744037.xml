<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035744037</ID>
<ANCIEN_ID>JG_L_2017_10_000000404749</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/74/40/CETATEXT000035744037.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 04/10/2017, 404749</TITRE>
<DATE_DEC>2017-10-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>404749</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MONOD, COLIN, STOCLET</AVOCATS>
<RAPPORTEUR>Mme Cécile Barrois de Sarigny</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier Domino</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:404749.20171004</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. D...A...a demandé au tribunal administratif de Paris de réformer la décision du 17 décembre 2014 par laquelle la Commission nationale des comptes de campagne et des financements politiques a approuvé après réformation le compte de campagne qu'il avait déposé au titre de l'élection des représentants au Parlement européen du 25 mai 2014 dans la circonscription Outre-mer et a fixé le montant du remboursement dû par l'État. Par un jugement n° 1502877 du 16 octobre 2015, le tribunal administratif de Paris a réformé cette décision de la Commission nationale des comptes de campagne et des financements politiques.<br/>
<br/>
              M. B...C...a demandé au tribunal administratif de Paris de réformer la décision du 17 décembre 2014 par laquelle la Commission nationale des comptes de campagne et des financements politiques a approuvé après réformation le compte de campagne qu'il avait déposé au titre de l'élection des représentants au Parlement européen du 25 mai 2014 dans la circonscription Ouest et a fixé le montant du remboursement dû par l'État. Par un jugement n° 1502878 du 16 octobre 2015, le tribunal administratif de Paris a réformé cette décision de la Commission nationale des comptes de campagne et des financements politiques.<br/>
<br/>
              Par un arrêt n°s 15PA4538, 15PA04539, 15PA04672, 15PA04673, 15PA04674, 15PA04675, 15PA04823 du 29 septembre 2016, la cour administrative d'appel de Paris, saisie par la voie de l'appel principal par la Commission nationale des comptes de campagne et des financements politiques et par la voie de l'appel incident par MM. A...etC..., a :<br/>
              - s'agissant de M.A..., retranché des dépenses de son compte de campagne les sommes de 288 euros et 2 400 euros et réintégré une somme de 3 054 euros et, en outre, réduit à 500 euros le montant de la sanction de réduction du remboursement forfaitaire dû par l'Etat ; <br/>
              - s'agissant de M.C..., retranché des dépenses de son compte de campagne la somme de 288 euros et, en outre, réduit à 500 euros le montant de la sanction de réduction du remboursement forfaitaire dû par l'Etat. <br/>
<br/>
              Par un pourvoi et un mémoire en réplique, enregistrés les 28 octobre 2016 et 9 février 2017 au secrétariat du contentieux du Conseil d'Etat, la Commission nationale des comptes de campagne et des financements politiques demande au Conseil d'Etat de réformer cet arrêt, d'une part, en tant qu'il a confirmé le jugement n° 1502877 du tribunal administratif de Paris du 16 octobre 2015 en ce qu'il a réintégré au compte de campagne de M. A...la somme de 11 159 euros correspondant à la quote-part de frais d'impression pour des documents non acheminés dans la circonscription et, d'autre part, en tant qu'il a confirmé le jugement du tribunal administratif de Paris n° 1502878 du 16 octobre 2015 en ce qu'il a réintégré au compte de campagne de M. C...la somme de 3 115 euros correspondant aux frais afférents à une réunion publique annulée. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code électoral ;<br/>
              - la loi n° 77-729 du 7 juillet 1977 ; <br/>
              - le décret n° 2009-370 du 1er avril 2009 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Cécile Barrois de Sarigny, maître des requêtes, <br/>
<br/>
              - les conclusions de M. Xavier Domino, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Monod, Colin, Stoclet, avocat de M. A...et de M. C...; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'il ressort des énonciations de l'arrêt attaqué que la Commission nationale des comptes de campagne et des financements politiques a, par deux décisions du 17 décembre 2014, approuvé après réformation les comptes de campagne déposés par M. C...et M.A..., candidats tête de liste à l'élection des représentants au Parlement européen qui s'est déroulée le 25 mai 2014, respectivement pour les circonscriptions Ouest et Outre-mer, et fixé le montant du remboursement de leurs dépenses de campagne qui leur était dû par l'Etat ; que le tribunal administratif de Paris, saisi par M. C...et M.A..., a réformé ces décisions de la Commission nationale des comptes de campagne et des financements politiques du 17 décembre 2014 par deux jugements du 16 octobre 2015 ; que, saisie en appel par la Commission nationale des comptes de campagne et des financements politiques et par la voie de l'appel incident par M. A...et M.C..., la cour administrative d'appel de Paris a, par un arrêt du 29 septembre 2016, réformé ces jugements ; que la Commission nationale des comptes de campagne et des financements politiques demande l'annulation de cet arrêt, d'une part, en tant qu'il a confirmé le jugement du tribunal administratif de Paris du 16 octobre 2015 en ce qu'il a réintégré dans le compte de M. A...la somme de 11 159 euros correspondant à des frais d'impression pour des documents électoraux non acheminés dans la circonscription, d'autre part, en tant qu'il a confirmé le jugement du tribunal administratif de Paris du 16 octobre 2015 en ce qu'il a réintégré au compte de campagne de M. C...la somme de 3 115 euros correspondant aux frais afférents à une réunion publique annulée ; <br/>
<br/>
              2.	Considérant qu'aux termes de l'article L. 52-15 du code électoral, rendu applicable comme les autres dispositions du titre Ier du livre Ier de ce code, à l'élection des représentants au Parlement européen par l'article 2 de la loi du 7 juillet 1977 relative à l'élection des représentants au Parlement européen : " La Commission nationale des comptes de campagne et des financements politiques approuve et, après procédure contradictoire, rejette ou réforme les comptes de campagne. Elle arrête le montant du remboursement forfaitaire prévu à l'article L. 52-11-1 " ; qu'aux termes de l'article L. 52-11-1 du même code : " Les dépenses électorales des candidats aux élections auxquelles l'article L. 52-4 est applicable font l'objet d'un remboursement forfaitaire de la part de l'Etat égal à 47,5 % de leur plafond de dépenses. Ce remboursement ne peut excéder le montant des dépenses réglées sur l'apport personnel des candidats et retracées dans leur compte de campagne " ; que les dépenses électorales susceptibles de faire l'objet de ce remboursement sont définies à l'article L. 52-12 du code électoral comme étant " l'ensemble des dépenses engagées ou effectuées en vue de l'élection, hors celles de la campagne officielle " par le candidat ou pour son compte au cours de la période mentionnée à l'article L. 52-4 du même code ; <br/>
<br/>
              3.	Considérant que les dépenses électorales susceptibles de faire l'objet, en application de l'article L. 52-11-1 du code électoral, d'un remboursement forfaitaire de la part de l'Etat sont celles dont la finalité est l'obtention des suffrages des électeurs ; que les dépenses qui, bien qu'engagées pendant la campagne par le candidat tête de liste ou par ses colistiers, n'ont pas cette finalité ne peuvent ouvrir droit au remboursement forfaitaire de l'Etat ;<br/>
<br/>
              4.	Considérant, en premier lieu, que les dépenses liées à l'organisation d'une réunion publique dans la circonscription électorale ont pour finalité l'expression des suffrages des électeurs ; qu'elles présentent, par suite, le caractère d'une dépense électorale, au sens de l'article L. 52-12 du code électoral, quand bien même, sauf manoeuvre, la réunion publique ne se tiendrait finalement pas pour quelque motif que ce soit ; que, pour juger que le tribunal administratif avait à bon droit réintégré dans le compte de campagne de M. C...une somme de 3 115 euros correspondant aux frais occasionnés par l'annulation d'une réunion publique dont l'organisation avait été remise en cause au cours de la campagne, la cour administrative d'appel de Paris a retenu, après avoir relevé que cet incident avait été isolé et que l'annulation ne revêtait pas le caractère d'une manoeuvre, que la circonstance que la réunion ne s'était finalement pas tenue n'avait pas pour effet de priver la dépense correspondante de son caractère de dépense électorale ; qu'en statuant ainsi, la cour administrative d'appel de Paris n'a pas commis d'erreur de droit ; <br/>
<br/>
              5. Considérant, en second lieu, que si des dépenses liées à l'impression de documents de propagande électorale présentent, en principe, le caractère de dépenses électorales, au sens de l'article L. 52-12 du code électoral, c'est à la condition que les dépenses en cause soient exposées en vue de la distribution des documents en cause dans la circonscription électorale du candidat qui les inscrit sur son compte de campagne ; que, par suite, la cour administrative d'appel a commis une erreur de droit en jugeant que devait être réintégré dans le compte de campagne de M.A..., candidat tête de liste dans la circonscription Outre-mer, une somme de 11 159 euros correspondant à la quote-part facturée par un parti politique au titre des frais d'impression de documents de propagande mutualisés avec les listes soutenues par ce parti se présentant dans les autres circonscriptions électorales, alors qu'elle relevait souverainement que les documents en cause n'étaient pas destinés à la circonscription Outre-mer en raison du coût du transport et des délais prévisibles de leur acheminement ; <br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que la Commission nationale des comptes de campagne et des financements politiques n'est fondée à demander l'annulation de l'arrêt qu'elle attaque qu'en tant qu'il a rejeté sa requête dirigée contre la réintégration, décidée par le tribunal administratif, de la somme de 11 159 euros dans le compte de campagne de M. A...; <br/>
<br/>
              7. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler, dans cette mesure, l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              8. Considérant qu'il résulte de l'instruction que les documents de propagande dont les frais d'impression ont été mutualisés entre les différentes circonscriptions et refacturés à M. A...par le parti politique UDI à hauteur de 11 159 euros, n'étaient pas destinés à être expédiés dans la circonscription Outre-mer et n'ont, dès lors, pas été utilisés dans cette circonscription ; que les dépenses correspondantes ne peuvent, par suite, être regardées comme ayant eu pour finalité l'obtention des suffrages des électeurs dans la circonscription où se présentait M. A...; qu'elles ne peuvent, en conséquence, être regardées comme des dépenses électorales, au sens de l'article L. 52-12 du code électoral, susceptibles d'être inscrites au compte de campagne déposé par M. A...; que la Commission nationale des comptes de campagne et des financements politiques est, dès lors, fondée à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Paris a réformé sa décision du 17 décembre 2014 en tant qu'elle excluait du remboursement de M. A...par l'Etat la somme de 11 159 euros ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
                                        --------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Paris du 29 septembre 2016 est annulé en tant qu'il a rejeté l'appel de la Commission nationale des comptes de campagne et des financements politiques dirigé contre le jugement n° 1502877 du 16 octobre 2015 du tribunal administratif de Paris réformant la décision de la Commission du 17 décembre 2014 en tant qu'elle n'intégrait pas au compte de campagne de M. A...la somme de 11 159 euros correspondant à des frais d'impression de documents de propagande électorale. <br/>
<br/>
Article 2 : Le jugement n° 1502877 du 16 octobre 2015 du tribunal administratif de Paris est annulé en tant qu'il a réintégré dans le compte de campagne de M. A...la somme de 11 159 euros correspondant à des frais d'impression de documents de propagande électorale.<br/>
<br/>
Article 3 : Les conclusions présentées par M. A...devant le tribunal administratif de Paris tendant à la réformation de la décision du 17 décembre 2014 de la Commission nationale des comptes de campagne sont rejetées en tant qu'elles tendent à ce que la somme de 11 159 euros soit réintégrée dans son compte de campagne.<br/>
<br/>
Article 4 : Le surplus des conclusions du pourvoi de la Commission nationale des comptes de campagne et des financements politiques est rejeté.<br/>
<br/>
Article 5 : La présente décision sera notifiée à M. D...A..., à M. B...C...et à la Commission nationale des comptes de campagne et des financements politiques.<br/>
Copie en sera adressée au ministre d'Etat, ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">28-005-04-02-04 ÉLECTIONS ET RÉFÉRENDUM. DISPOSITIONS GÉNÉRALES APPLICABLES AUX ÉLECTIONS. FINANCEMENT ET PLAFONNEMENT DES DÉPENSES ÉLECTORALES. COMPTE DE CAMPAGNE. DÉPENSES. - DÉPENSES POUVANT FAIRE L'OBJET DU REMBOURSEMENT FORFAITAIRE DE L'ETAT (ART. L. 52-11-1 DU CODE ÉLECTORAL) - 1) DÉPENSES LIÉES À L'ORGANISATION D'UNE RÉUNION PUBLIQUE ANNULÉE - INCLUSION - 2) DÉPENSES LIÉES À L'IMPRESSION DE DOCUMENTS DE PROPAGANDE ÉLECTORALE - A) PRINCIPE - INCLUSION - B) EXCLUSION EN L'ESPÈCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">28-023-03 ÉLECTIONS ET RÉFÉRENDUM. ÉLECTIONS AU PARLEMENT EUROPÉEN. CAMPAGNE ET PROPAGANDE ÉLECTORALES. - DÉPENSES POUVANT FAIRE L'OBJET DU REMBOURSEMENT FORFAITAIRE DE L'ETAT (ART. L. 52-11-1 DU CODE ÉLECTORAL) - 1) DÉPENSES LIÉES À L'ORGANISATION D'UNE RÉUNION PUBLIQUE ANNULÉE - INCLUSION - 2) DÉPENSES LIÉES À L'IMPRESSION DE DOCUMENTS DE PROPAGANDE ÉLECTORALE - A) PRINCIPE - INCLUSION - B) EXCLUSION EN L'ESPÈCE.
</SCT>
<ANA ID="9A"> 28-005-04-02-04 1) Les dépenses liées à l'organisation d'une réunion publique dans la circonscription électorale ont pour finalité l'expression des suffrages des électeurs. Elles présentent, par suite, le caractère d'une dépense électorale, au sens de l'article L. 52-12 du code électoral, quand bien même, sauf manoeuvre, la réunion publique ne se tiendrait finalement pas pour quelque motif que ce soit.... ,,2) a) Si les dépenses liées à l'impression de documents de propagande électorale présentent, en principe, le caractère de dépenses électorales, au sens de l'article L. 52-12 du code électoral, c'est à la condition que les dépenses en cause soient exposées en vue de la distribution de ces documents dans la circonscription électorale du candidat qui les inscrit sur son compte de campagne.,,,b) Erreur de droit à avoir jugé que devait être réintégré dans le compte de campagne d'un candidat aux élections européennes, tête de liste dans la circonscription Outre-mer, une somme correspondant à la quote-part facturée par un parti politique au titre des frais d'impression de documents de propagande mutualisés avec les listes soutenues par ce parti se présentant dans les autres circonscriptions électorales, après avoir relevé souverainement que les documents en cause n'étaient pas destinés à la circonscription Outre-mer en raison du coût du transport et des délais prévisibles de leur acheminement.</ANA>
<ANA ID="9B"> 28-023-03 1) Les dépenses liées à l'organisation d'une réunion publique dans la circonscription électorale ont pour finalité l'expression des suffrages des électeurs. Elles présentent, par suite, le caractère d'une dépense électorale, au sens de l'article L. 52-12 du code électoral, quand bien même, sauf manoeuvre, la réunion publique ne se tiendrait finalement pas pour quelque motif que ce soit.... ,,2) a) Si les dépenses liées à l'impression de documents de propagande électorale présentent, en principe, le caractère de dépenses électorales, au sens de l'article L. 52-12 du code électoral, c'est à la condition que les dépenses en cause soient exposées en vue de la distribution de ces documents dans la circonscription électorale du candidat qui les inscrit sur son compte de campagne.,,,b) Erreur de droit à avoir jugé que devait être réintégré dans le compte de campagne d'un candidat aux élections européennes, tête de liste dans la circonscription Outre-mer, une somme correspondant à la quote-part facturée par un parti politique au titre des frais d'impression de documents de propagande mutualisés avec les listes soutenues par ce parti se présentant dans les autres circonscriptions électorales, après avoir relevé souverainement que les documents en cause n'étaient pas destinés à la circonscription Outre-mer en raison du coût du transport et des délais prévisibles de leur acheminement.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
