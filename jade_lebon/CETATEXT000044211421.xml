<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044211421</ID>
<ANCIEN_ID>JG_L_2021_10_000000441415</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/21/14/CETATEXT000044211421.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 14/10/2021, 441415</TITRE>
<DATE_DEC>2021-10-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>441415</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP KRIVINE, VIAUD ; CABINET ROUSSEAU ET TAPIE</AVOCATS>
<RAPPORTEUR>M. David Moreau</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Arnaud Skzryerbak</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:441415.20211014</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              M. H... G... a demandé au tribunal administratif de Cergy-Pontoise d'annuler l'arrêté du 5 juin 2019 par lequel le maire de Puteaux a délivré un permis de construire à la SCI JNH Holding, sur un terrain sis 14, rue de l'Oasis à Puteaux, ainsi que la décision rejetant son recours gracieux du 29 septembre 2019. Par une ordonnance n° 1914931 du 17 avril 2020, prise sur le fondement de l'article R. 222-1 du code de justice administrative, la présidente de la 1ère chambre du tribunal administratif de Cergy-Pontoise a rejeté cette demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 24 juin et 24 septembre 2020 au secrétariat du contentieux du Conseil d'Etat, M. G... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de mettre à la charge de la commune de Puteaux et de la SCI JNH Holding la somme de 3 500 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
        Vu les autres pièces du dossier ;<br/>
<br/>
        Vu : <br/>
        - le code de l'urbanisme ;<br/>
        - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. David Moreau, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Arnaud Skzryerbak, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Viaud, Krivine, avocat de M. G... et au cabinet Rousseau et Tapie, avocat de la SCI JNH Holding ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Aux termes de l'article R. 612-1 du code de justice administrative : " Lorsque des conclusions sont entachées d'une irrecevabilité susceptible d'être couverte après l'expiration du délai de recours, la juridiction ne peut les rejeter en relevant d'office cette irrecevabilité qu'après avoir invité leur auteur à les régulariser. / (...) / La demande de régularisation mentionne que, à défaut de régularisation, les conclusions pourront être rejetées comme irrecevables dès l'expiration du délai imparti qui, sauf urgence, ne peut être inférieur à quinze jours (...) ". Aux termes de l'article R. 222-1 du même code : " Les présidents de tribunal administratif et de cour administrative d'appel, les premiers vice-présidents des tribunaux et des cours, le vice-président du tribunal administratif de Paris, les présidents de formation de jugement des tribunaux et des cours et les magistrats ayant une ancienneté minimale de deux ans et ayant atteint au moins le grade de premier conseiller désignés à cet effet par le président de leur juridiction peuvent, par ordonnance : / (...) 4° Rejeter les requêtes manifestement irrecevables, lorsque la juridiction n'est pas tenue d'inviter leur auteur à les régulariser ou qu'elles n'ont pas été régularisées à l'expiration du délai imparti par une demande en ce sens (...) ". Aux termes de l'article L. 600-1-2 du code de l'urbanisme : " Une personne autre que l'Etat, les collectivités territoriales ou leurs groupements ou une association n'est recevable à former un recours pour excès de pouvoir contre une décision relative à l'occupation ou à l'utilisation du sol régie par le présent code que si la construction, l'aménagement ou le projet autorisé sont de nature à affecter directement les conditions d'occupation, d'utilisation ou de jouissance du bien qu'elle détient ou occupe régulièrement ou pour lequel elle bénéficie d'une promesse de vente, de bail, ou d'un contrat préliminaire mentionné à l'article L. 261-15 du code de la construction et de l'habitation ".<br/>
<br/>
              2. Les requêtes manifestement irrecevables qui peuvent être rejetées par ordonnance en application des dispositions de l'article R. 222-1 citées au point 1 sont, tout d'abord, celles dont l'irrecevabilité ne peut en aucun cas être couverte, ensuite, celles qui ne peuvent être régularisées que jusqu'à l'expiration du délai de recours, si ce délai est expiré et, enfin, celles qui ont donné lieu à une invitation à régulariser, si le délai que la juridiction avait imparti au requérant à cette fin, en l'informant des conséquences qu'emporte un défaut de régularisation comme l'exige l'article R. 612-1 du code de justice administrative, est expiré.<br/>
<br/>
              3. Pour rejeter comme manifestement irrecevable, par une ordonnance prise sur le fondement du 4° de l'article R. 222-1 du code de justice administrative, la demande de M. G... tendant à l'annulation du permis de construire délivré le 5 juin 2019 par le maire de Puteaux à la SCI JNH Holding, la présidente de la 1ère chambre du tribunal administratif de Cergy-Pontoise a retenu que le demandeur ne justifiait pas d'un intérêt pour agir. Il résulte de ce qui a été dit au point précédent qu'en statuant ainsi sans avoir au préalable invité le requérant à régulariser sa requête en apportant les précisions permettant d'en apprécier la recevabilité au regard des exigences de l'article L. 600-1-2 du code de l'urbanisme et sans l'avoir informé des conséquences qu'emporterait un défaut de régularisation dans le délai imparti comme l'exige l'article R. 612-1 du code de justice administrative, l'auteur de l'ordonnance attaquée a commis une erreur de droit.<br/>
<br/>
              4. Il résulte de ce qui précède que, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, M. G... est fondé à demander l'annulation de l'ordonnance attaquée.<br/>
<br/>
              5. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de M. G..., qui n'est pas la partie perdante dans la présente instance.  Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de Puteaux la somme de 3 000 euros à verser à M. G... au titre des dispositions de l'article L. 761-1 du code de justice administrative. Il n'y a pas lieu en revanche, dans les circonstances de l'espèce, de mettre une somme à la charge de la société JNH Holding à verser à M. G... au titre des mêmes dispositions.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance n° 1914931 du 17 avril 2020 de la présidente de la 1ère chambre du tribunal administratif de Cergy-Pontoise est annulée.<br/>
Article 2 : L'affaire est renvoyée devant le tribunal administratif de Cergy-Pontoise.<br/>
Article 3 : La commune de Puteaux versera une somme de 3 000 euros à M. G... au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Le surplus des conclusions de M. G... présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative est rejeté.<br/>
Article 5 : Les conclusions de la société JNH Holding présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 6 : La présente décision sera notifiée à M. H... G..., à la commune de Puteaux et à la SCI JNH Holding.<br/>
                   Délibéré à l'issue de la séance du 22 septembre 2021 où siégeaient : M. Christophe Chantepy, président adjoint de la section du contentieux, présidant ; M. I... F..., M. Frédéric Aladjidi, présidents de chambre, Mme K... B..., M. L... C..., Mme A... M..., M. D... E..., M. François Weil, conseillers d'Etat et M. David Moreau maître des requêtes-rapporteur. <br/>
                          Rendu le 14 octobre 2021.<br/>
                                                  Le président : <br/>
                                                  Signé : M. Christophe Chantepy<br/>
Le rapporteur : <br/>
Signé : M. David Moreau<br/>
La secrétaire :<br/>
Signé : Mme J... N...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-06-03 PROCÉDURE. - JUGEMENTS. - COMPOSITION DE LA JURIDICTION. - REJET PAR ORDONNANCE D'UNE REQUÊTE MANIFESTEMENT IRRECEVABLE (4° DE L'ART. R. 222-1 DU CJA) - INCLUSION - REQUÉRANT NE JUSTIFIANT PAS SUFFISAMMENT DE SON INTÉRÊT À AGIR CONTRE UNE AUTORISATION D'URBANISME [RJ1] - CONDITIONS [RJ2].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-07-01-07 PROCÉDURE. - POUVOIRS ET DEVOIRS DU JUGE. - QUESTIONS GÉNÉRALES. - DEVOIRS DU JUGE. - REJET PAR ORDONNANCE D'UNE REQUÊTE MANIFESTEMENT IRRECEVABLE (4° DE L'ART. R. 222-1 DU CJA) - INCLUSION - REQUÉRANT NE JUSTIFIANT PAS SUFFISAMMENT DE SON INTÉRÊT À AGIR CONTRE UNE AUTORISATION D'URBANISME [RJ1] - CONDITIONS [RJ2].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">68-06-01-02 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. - RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. - INTRODUCTION DE L'INSTANCE. - INTÉRÊT À AGIR. - REQUÉRANT NE JUSTIFIANT PAS SUFFISAMMENT DE SON INTÉRÊT À AGIR CONTRE UNE AUTORISATION D'URBANISME - POSSIBILITÉ DE REJETER LA REQUÊTE PAR ORDONNANCE COMME MANIFESTEMENT IRRECEVABLE (4° DE L'ART. R. 222-1 DU CJA) - EXISTENCE [RJ1] - CONDITIONS [RJ2].
</SCT>
<ANA ID="9A"> 54-06-03 Un recours pour excès de pouvoir contre une décision relative à l'occupation ou à l'utilisation du sol ne peut être rejeté comme manifestement irrecevable pour défaut d'intérêt pour agir, par une ordonnance prise sur le fondement du 4° de l'article R. 222-1 du code de justice administrative (CJA), sans avoir au préalable invité le requérant à régulariser sa requête en apportant les précisions permettant d'en apprécier la recevabilité au regard des exigences de l'article L. 600-1-2 du code de l'urbanisme et sans l'avoir informé des conséquences qu'emporterait un défaut de régularisation dans le délai imparti comme l'exige l'article R. 612-1 du CJA.</ANA>
<ANA ID="9B"> 54-07-01-07 Un recours pour excès de pouvoir contre une décision relative à l'occupation ou à l'utilisation du sol ne peut être rejeté comme manifestement irrecevable pour défaut d'intérêt pour agir, par une ordonnance prise sur le fondement du 4° de l'article R. 222-1 du code de justice administrative (CJA), sans avoir au préalable invité le requérant à régulariser sa requête en apportant les précisions permettant d'en apprécier la recevabilité au regard des exigences de l'article L. 600-1-2 du code de l'urbanisme et sans l'avoir informé des conséquences qu'emporterait un défaut de régularisation dans le délai imparti comme l'exige l'article R. 612-1 du CJA.</ANA>
<ANA ID="9C"> 68-06-01-02 Un recours pour excès de pouvoir contre une décision relative à l'occupation ou à l'utilisation du sol ne peut être rejeté comme manifestement irrecevable pour défaut d'intérêt pour agir, par une ordonnance prise sur le fondement du 4° de l'article R. 222-1 du code de justice administrative (CJA), sans avoir au préalable invité le requérant à régulariser sa requête en apportant les précisions permettant d'en apprécier la recevabilité au regard des exigences de l'article L. 600-1-2 du code de l'urbanisme et sans l'avoir informé des conséquences qu'emporterait un défaut de régularisation dans le délai imparti comme l'exige l'article R. 612-1 du CJA.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 10 février 2016, M. et Mme Peyret et M. et Mme Vivier, n° 387507, T. pp. 891-996. Rappr., s'agissant de l'appréciation de l'intérêt pour agir en contentieux de l'urbanisme, CE, 10 juin 2015, M. Brodelle et Mme Gino, n° 386121, p. 192....[RJ2] Cf., sur l'obligation d'inviter à régulariser lorsque le motif d'irrecevabilité peut l'être, y compris quand une fin de non-recevoir est soulevée en défense, CE, 14 octobre 2015, M. et Mme Godrant, n° 374850, T. pp. 819-830. Rappr., s'agissant d'une irrecevabilité pour défaut de notification du recours (art. R. 600-1 du code de l'urbanisme), CE, 13 juillet 2011, Mme Cassan, n° 314093, T. p. 1199. Comp., sur la faculté de rejeter sans invitation à régulariser par une décision prise après audience publique, lorsqu'une fin de non-recevoir a été soulevée en défense, CE, 14 novembre 2011, M. Alloune, n° 334764, T. p. 1084.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
