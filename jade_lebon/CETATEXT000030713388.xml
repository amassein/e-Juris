<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030713388</ID>
<ANCIEN_ID>JG_L_2015_06_000000368605</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/71/33/CETATEXT000030713388.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 5ème SSR, 08/06/2015, 368605</TITRE>
<DATE_DEC>2015-06-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>368605</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 5ème SSR</FORMATION>
<TYPE_REC>Contentieux des pensions</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BENABENT, JEHANNIN</AVOCATS>
<RAPPORTEUR>M. David Moreau</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Maud Vialettes</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2015:368605.20150608</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal des pensions de Toulon d'annuler l'arrêté du ministre de la défense du 3 décembre 2007 fixant sa pension militaire d'invalidité au taux de 100 % + 30° pour la période allant du 3 novembre 2007 au 23 septembre 2008.<br/>
<br/>
              Par un jugement du 27 janvier 2011, le tribunal des pensions de Toulon a annulé cet arrêté et décidé que M. B...bénéficierait d'un taux d'invalidité de 100 % + 38° pour la période considérée.<br/>
<br/>
              Par un arrêt n° 11/00032 du 12 décembre 2012, la cour régionale des pensions d'Aix-en-Provence, statuant sur l'appel du préfet de la région Provence-Alpes-Côte-d'Azur, a annulé ce jugement et rejeté la demande M. B...tendant à l'annulation de l'arrêté du 3 décembre 2007.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 17 mai et 14 août 2013 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel du préfet de la région Provence-Alpes-Côte-d'Azur ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code des pensions militaires d'invalidité et des victimes de la guerre ;<br/>
              - le décret n° 59-327 du 20 février 1959 ;<br/>
              - le guide-barème annexé au décret du 29 mai 1919 modifié déterminant les règles et barèmes pour la classification des infirmités d'après leur gravité en vue de la concession des pensions accordées par le code des pensions militaires d'invalidité et des victimes de la guerre ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. David Moreau, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Bénabent, Jehannin, avocat de M. B...;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. B...est titulaire d'une pension militaire d'invalidité qui, par un arrêté du 3 avril 2006, a été fixée pour la période allant du 3 avril 2004 au 2 novembre 2007 au taux de 100 % pour l'infirmité dénommée " insuffisance cardiaque par cardiopathie mitro-aortique " augmenté de 38 degrés de surpension pour plusieurs infirmités supplémentaires, dont des " séquelles de plaie de la main gauche chez un droitier " et des " troubles trophonévrotiques cicatriciels de la main gauche " à caractère définitif résultant d'une blessure reçue en service commandé le 28 février 1957, ainsi qu'une " limitation des mouvements de l'épaule gauche " ayant résulté d'une intervention chirurgicale nécessitée en 2002 par son insuffisance cardiaque ; que, toutefois, par un arrêté du 3 décembre 2007, le ministre de la défense a ramené la surpension de M. B...à 30 degrés pour la période allant du 3 novembre 2007 au 23 septembre 2008 au motif que la limitation des mouvements de l'épaule gauche trouvait son siège sur le même membre que les deux infirmités affectant sa main gauche et qu'il y avait donc lieu d'écrêter la somme des majorations correspondant aux trois infirmités afin que la surpension y afférente n'excède pas celle qui aurait été due pour la perte totale du membre supérieur gauche ;<br/>
<br/>
              2. Considérant que par un jugement du 27 janvier 2011, le tribunal des pensions de Toulon a annulé cet arrêté et accordé à M. B...une surpension de 38 degrés ; que par un arrêt du 12 décembre 2012, la cour régionale des pensions d'Aix-en-Provence a annulé ce jugement et rejeté la demande de M. B...tendant à l'annulation de l'arrêté du 3 décembre 2007 ; que M. B...se pourvoit en cassation contre cet arrêt ;<br/>
<br/>
              3. Considérant, en premier lieu, que si le dossier soumis aux juges du fond ne contient pas l'accusé de réception de la notification au préfet de région Provence-Alpes-Côte-d'Azur du jugement du tribunal des pensions de Toulon, il ressort de ce même dossier que ce jugement a été reçu en préfecture le 31 janvier 2011 ; que, par suite, M. B...n'est, en tout état de cause, pas fondé à soutenir que la requête d'appel du préfet de Provence-Alpes-Côte-d'Azur enregistrée le 30 mars 2011 serait tardive ;<br/>
<br/>
              4. Considérant, en deuxième lieu, qu'aux termes de l'article L. 16 du code des pensions militaires d'invalidité et des victimes de la guerre : " Dans le cas d'infirmités multiples dont l'une entraîne l'invalidité absolue, il est accordé, en sus de la pension maxima, pour tenir compte de l'infirmité ou des infirmités supplémentaires, par degré d'invalidité de 10 %, un complément de pension calculé sur la base de l'indice de pension 16 tel qu'il est défini à l'article L. 8 bis du présent code. / Si, à l'infirmité la plus grave, s'ajoutent deux ou plus de deux infirmités supplémentaires, la somme des degrés d'invalidité est calculée en accordant à chacune des blessures supplémentaires la majoration prévue à l'article L. 14. / La majoration susvisée est accordée dans la limite de 100 degrés de surpension. Les infirmités classées après celle qui permet, compte tenu de la majoration correspondant à son rang, de franchir ladite limite sont affectées d'une majoration dont la valeur ne peut être supérieure au pourcentage de l'invalidité résultant de l'infirmité temporaire ou définitive à laquelle elle se rattache (...) " ; qu'aux termes du 3ème alinéa de l'article L. 14 du même code : " (...) les degrés d'invalidité de chacune des infirmités supplémentaires sont élevés d'une, de deux ou de trois catégories, soit de 5, 10, 15 %, et ainsi de suite, suivant qu'elles occupent les deuxième, troisième, quatrième rangs dans la série décroissante de leur gravité " ; que, pour l'application de ces dispositions, les infirmités autres que celle entraînant une invalidité absolue siégeant sur un même membre au sens du guide-barème prévu par l'article L. 9 du même code ne peuvent ouvrir droit à une surpension excédant celle qu'aurait procurée la perte totale du membre considéré ;<br/>
<br/>
              5. Considérant qu'étant affecté, ainsi qu'il a été dit ci-dessus, d'une insuffisance cardiaque entraînant l'invalidité absolue, M. B...relève pour le calcul de sa pension de l'article L. 16 du code des pensions militaires d'invalidité et des victimes de la guerre ; qu'il résulte de ce qui a été dit au point précédent que la cour régionale des pensions d'Aix-en-Provence, après avoir exactement retenu, au regard du guide-barème, que les infirmités " séquelles de plaie de la main gauche chez un droitier ", " troubles trophonévrotiques cicatriciels de la main gauche " et " limitation des mouvements de l'épaule gauche " trouvaient leur siège sur un même membre, n'a pas commis d'erreur de droit en jugeant que le cumul de ces infirmités ne pouvait ouvrir droit pour le requérant à une surpension supérieure à celle correspondant à la perte de ce membre ; <br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède que le pourvoi de M. B...doit être rejeté, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
      --------------<br/>
Article 1er : Le pourvoi de M. B...est rejeté.<br/>
Article 2 : La présente décision sera notifiée à M. A...B...et au ministre de la défense.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">48-01-03-02 PENSIONS. PENSIONS MILITAIRES D'INVALIDITÉ ET DES VICTIMES DE GUERRE. CARACTÈRE DES PENSIONS CONCÉDÉES. CALCUL DU TAUX DE LA PENSION. - INFIRMITÉS MULTIPLES DONT UNE ENTRAÎNE L'INVALIDITÉ ABSOLUE - COMPLÉMENT DE PENSION AU TITRE DES AUTRES INFIRMITÉS - CAS OÙ PLUSIEURS DE CES INFIRMITÉS SIÈGENT SUR UN MÊME MEMBRE - LIMITE - PENSION CORRESPONDANT À LA PERTE TOTALE DE CE MEMBRE [RJ1].
</SCT>
<ANA ID="9A"> 48-01-03-02 L'article L. 16 du code des pensions militaires d'invalidité et des victimes de la guerre (CPMI) prévoit qu'en cas d'infirmités multiples dont l'une entraîne l'invalidité absolue, il est accordé un complément de pension en sus de la pension maxima, pour tenir compte de l'infirmité ou des infirmités supplémentaires.  Pour l'application de ces dispositions, les infirmités autres que celle entraînant une invalidité absolue siégeant sur un même membre au sens du guide-barème prévu par l'article L. 9 du même code ne peuvent ouvrir droit à un complément de pension excédant celle qu'aurait procurée la perte totale du membre considéré.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Cf. CE, Commission spéciale de cassation des pensions (CSCP), 22 mars 1968, Ministre de la défense c/ M. Fardeau,  p. 203.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
