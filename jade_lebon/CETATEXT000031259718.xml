<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031259718</ID>
<ANCIEN_ID>JG_L_2015_09_000000375730</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/25/97/CETATEXT000031259718.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème / 8ème SSR, 30/09/2015, 375730</TITRE>
<DATE_DEC>2015-09-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>375730</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème / 8ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MONOD, COLIN, STOCLET</AVOCATS>
<RAPPORTEUR>M. Christian Fournier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:375730.20150930</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Le préfet des Pyrénées-Atlantiques a demandé au tribunal administratif de Pau d'annuler le contrat à durée indéterminée en date du 3 mai 2012 par lequel le président de la communauté d'agglomération Côte Basque-Adour a engagé M. A... en qualité de directeur général des services techniques sur le fondement de l'article 47 de la loi du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale.<br/>
<br/>
              Par un jugement n° 1201677 du 20 décembre 2012, le tribunal administratif de Pau a annulé ce contrat.<br/>
<br/>
              Par un arrêt n° 13BX00624 en date du 23 décembre 2013, la cour administrative d'appel de Bordeaux a rejeté l'appel formé par la communauté d'agglomération Côte Basque-Adour contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 24 février et 11 avril 2014 au secrétariat du contentieux du Conseil d'Etat, la communauté d'agglomération Côte Basque-Adour demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu :<br/>
              - les autres pièces du dossier ;<br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - la loi n° 84-53 du 26 janvier 1984 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Christian Fournier, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Monod, Colin, Stoclet, avocat de la communauté d'agglomération Cote Basque-Adour ;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que le président de la communauté d'agglomération Côte Basque-Adour a recruté, par contrat à durée indéterminée en date du 3 mai 2012, M. A... pour occuper, à compter du 1er juin 2012, l'emploi fonctionnel de directeur général des services techniques ; que, sur déféré du préfet des Pyrénées-Atlantiques, le tribunal administratif de Pau a, par un jugement du 20 décembre 2012, annulé ce contrat au motif qu'il ne pouvait être conclu pour une durée indéterminée ; que la communauté d'agglomération Côte Basque-Adour se pourvoit en cassation contre l'arrêt du 23 décembre 2013 par lequel la cour administrative d'appel de Bordeaux a rejeté l'appel qu'elle a interjeté de ce jugement ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article 3 de la loi du 13 juillet 1983 portant droits et obligations des fonctionnaires : " Sauf dérogation prévue par une disposition législative, les emplois civils permanents de l'Etat, des régions, des départements, des communes et de leurs établissements publics à caractère administratif sont, à l'exception de ceux réservés aux magistrats de l'ordre judiciaire et aux fonctionnaires des assemblées parlementaires, occupés soit par des fonctionnaires régis par le présent titre, soit par des fonctionnaires des assemblées parlementaires, des magistrats de l'ordre judiciaire ou des militaires dans les conditions prévues par leur statut. " ; qu'aux termes de l'article 2 de la loi du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale : " Les dispositions de la présente loi s'appliquent aux personnes qui, régies par le titre Ier du statut général des fonctionnaires de l'Etat et des collectivités territoriales, ont été nommées dans un emploi permanent et titularisées dans un grade de la hiérarchie administrative des communes, des départements, des régions ou des établissements publics en relevant (...). " ; qu'aux termes de l'article 3-3 de la même loi, dans sa rédaction issue de la loi du 12 mars 2012 : " Par dérogation au principe énoncé à l'article 3 de la loi n° 83-634 du 13 juillet 1983 précitée et sous réserve de l'article 34 de la présente loi, des emplois permanents peuvent être occupés de manière permanente par des agents contractuels dans les cas suivants : 1° Lorsqu'il n'existe pas de cadre d'emplois de fonctionnaires susceptibles d'assurer les fonctions correspondantes ; 2° Pour les emplois du niveau de la catégorie A lorsque les besoins des services ou la nature des fonctions le justifient et sous réserve qu'aucun fonctionnaire n'ait pu être recruté dans les conditions prévues par la présente loi ; (...) Les agents ainsi recrutés sont engagés par contrat à durée déterminée d'une durée maximale de trois ans. Ces contrats sont renouvelables par reconduction expresse, dans la limite d'une durée maximale de six ans. Si, à l'issue de cette durée, ces contrats sont reconduits, ils ne peuvent l'être que par décision expresse et pour une durée indéterminée. " ; qu'aux termes de l'article 3-4 de cette même loi : " II. - Tout contrat conclu ou renouvelé pour pourvoir un emploi permanent en application de l'article 3-3 avec un agent qui justifie d'une durée de services publics effectifs de six ans au moins sur des fonctions relevant de la même catégorie hiérarchique est conclu  pour une durée indéterminée. La durée de six ans mentionnée au premier alinéa du présent II est comptabilisée au titre de l'ensemble des services accomplis auprès de la même collectivité ou du même établissement dans des emplois occupés sur le fondement des articles 3 à 3-3. Elle inclut, en outre, les services effectués au titre du deuxième alinéa de l'article 25 s'ils l'ont été auprès de la collectivité ou de l'établissement l'ayant ensuite recruté par contrat. (...) " ; qu'aux termes de l'article 41 de la même loi : " Lorsqu'un emploi permanent est créé ou devient vacant, l'autorité territoriale en informe le centre de gestion compétent qui assure la publicité de cette création ou de cette vacance, à l'exception des emplois susceptibles d'être pourvus exclusivement par voie d'avancement de grade./ (...) / L'autorité territoriale pourvoit l'emploi créé ou vacant en nommant l'un des candidats inscrits sur une liste d'aptitude établie en application de l'article 44 ou l'un des fonctionnaires qui s'est déclaré candidat par voie de mutation, de détachement, d'intégration directe ou, le cas échéant et dans les conditions fixées par chaque statut particulier, par voie de promotion interne et d'avancement de grade. " ; qu'aux termes de l'article 47 de la même loi, dans sa rédaction issue de la loi du 13 août 2004 : " Par dérogation à l'article 41, peuvent être pourvus par la voie du recrutement direct, dans les conditions de diplômes ou de capacités fixées par décret en Conseil d'Etat, les emplois suivants : (...) Directeur général des services et directeur général des services techniques des communes de plus de 80 000 habitants et des établissements publics de coopération intercommunale à fiscalité propre de plus de 80 000 habitants ; (...) " ;<br/>
<br/>
              3. Considérant que les dispositions citées  ci-dessus de l'article 47 de la loi du 26 janvier 1984 autorisent le recrutement direct, sans publicité de la création ou de la vacance de l'emploi dont il s'agit ni concours, de fonctionnaires ou d'agents non titulaires, pour occuper les emplois fonctionnels dont elles dressent la liste ; que ces dispositions, qui ne fixent pas la durée des contrats de recrutement qui peuvent être proposés dans ce cadre, doivent être regardées comme dérogeant aux dispositions des articles 3-3 et 3-4 de la loi du 26 janvier 1984 qui régissent la durée des contrats conclus par les collectivités et établissements publics territoriaux en vue du recrutement des agents non titulaires pour occuper des emplois permanents ; qu'il en résulte que le recrutement d'un agent non titulaire, sur le fondement des dispositions de l'article 47 de la loi du 26 janvier 1984, peut donner lieu à un contrat à durée déterminée ou à durée indéterminée ; que, dès lors, en jugeant que les dispositions de cet article ne pouvaient être interprétées comme autorisant la conclusion d'un contrat à durée indéterminée en dehors des hypothèses prévues par les articles 3-3 et 3-4 précités de la loi du 26 janvier 1984, la cour administrative d'appel de Bordeaux a commis une erreur de droit ; que, par suite et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, la communauté d'agglomération Côte Basque-Adour est fondée à demander l'annulation de l'arrêt qu'elle attaque ;<br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              5. Considérant qu'il résulte de ce qui a été dit au point 3 ci-dessus que la communauté d'agglomération Côte Basque-Adour est fondée à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Pau a jugé que les dispositions de l'article 47 de la loi du 26 janvier 1984 ne permettent pas que le contrat de recrutement d'un agent non titulaire pour occuper les fonctions de directeur général des services techniques d'une communauté d'agglomération de plus de 80 000 habitants soit conclu pour une durée indéterminée et a annulé, pour ce motif, sur déféré préfectoral, le contrat d'engagement de M. A... ; qu'en l'absence d'autre moyens soulevés par le préfet, il y lieu de rejeter son déféré ;<br/>
<br/>
              6. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat, pour l'ensemble de la procédure, la somme de 4 000 euros qui sera versée à la communauté d'agglomération Côte Basque-Adour, au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 23 décembre 2013 de la cour administrative d'appel de Bordeaux et le jugement du 20 décembre 2012 du tribunal administratif de Pau sont annulés.<br/>
Article 2 : Le déféré du préfet des Pyrénées-Atlantiques devant le tribunal administratif de Pau est rejeté.<br/>
Article 3 : Une somme de 4 000 euros sera versée par l'Etat à la communauté d'agglomération Côte Basque-Adour au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la communauté d'agglomération Côte Basque-Adour et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-07-01-03 FONCTIONNAIRES ET AGENTS PUBLICS. STATUTS, DROITS, OBLIGATIONS ET GARANTIES. STATUT GÉNÉRAL DES FONCTIONNAIRES DE L'ÉTAT ET DES COLLECTIVITÉS LOCALES. DISPOSITIONS STATUTAIRES RELATIVES À LA FONCTION PUBLIQUE TERRITORIALE (LOI DU 26 JANVIER 1984). - RECRUTEMENT D'UN AGENT NON TITULAIRE SUR UN EMPLOI FONCTIONNEL (ART. 47 DE LA LOI DU 16 JANVIER 1984) - FACULTÉ DE RECOURIR À UN CDD OU UN CDI - EXISTENCE.
</SCT>
<ANA ID="9A"> 36-07-01-03 L'article 47 de la loi n° 84-53 du 26 janvier 1984 autorise le recrutement direct, sans publicité de la création ou de la vacance de l'emploi dont il s'agit ni concours, de fonctionnaires ou d'agents non titulaires pour occuper les emplois fonctionnels dont il dresse la liste. Cet article, qui ne fixe pas la durée des contrats de recrutement pouvant être proposés dans ce cadre, déroge aux articles 3-3 et 3-4 de la même loi qui régissent la durée des contrats conclus par les collectivités et établissements publics territoriaux en vue du recrutement des agents non titulaires pour occuper des emplois permanents. Il en résulte que le recrutement d'un agent non titulaire sur le fondement de l'article 47 de la loi du 26 janvier 1984 peut donner lieu à un contrat à durée déterminée ou à durée indéterminée.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
