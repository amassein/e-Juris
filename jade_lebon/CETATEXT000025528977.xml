<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025528977</ID>
<ANCIEN_ID>JG_L_2012_03_000000354114</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/52/89/CETATEXT000025528977.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème et 8ème sous-sections réunies, 09/03/2012, 354114, Publié au recueil Lebon</TITRE>
<DATE_DEC>2012-03-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>354114</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème et 8ème sous-sections réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Christian Fournier</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CESSR:2012:354114.20120309</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le jugement n° 0906432 du 2 novembre 2011, enregistré le 17 novembre 2011 au secrétariat du contentieux du Conseil d'Etat, par lequel le tribunal administratif de Lyon, avant de statuer sur la demande de la COMMUNE DE MIONNAY tendant, d'une part, à l'annulation du titre exécutoire d'un montant de 7 462,80 euros émis à son encontre le 14 septembre 2009 par le maire de la commune de Mézériat en vue d'obtenir le remboursement des dépenses engagées pour la formation obligatoire de Mlle , attachée territoriale mutée de Mézériat à Mionnay et, d'autre part, à la décharge, à titre principal, de la somme de 7 642,80 euros et, à titre subsidiaire, de la somme de 4 670,88 euros, a décidé, par application des dispositions de l'article L. 113-1 du code de justice administrative, de transmettre le dossier de cette requête au Conseil d'Etat, en soumettant à son examen les questions suivantes : <br/>
<br/>
              1°) L'accord devant intervenir entre les deux collectivités sur le montant de l'indemnité due en remboursement, par la collectivité territoriale d'accueil, des frais de formation de l'agent muté peut-il, en stipulant une indemnité nulle, légalement permettre à la collectivité d'origine de renoncer au versement de toute indemnité '<br/>
<br/>
              2°) Dans l'affirmative, un tel accord doit-il être nécessairement explicite ou peut-il être implicitement révélé, notamment, par l'absence de prise de position formelle des collectivités concernées au cours de leurs pourparlers relatifs à la mutation ou tout autre indice susceptible de corroborer l'existence d'une telle renonciation '<br/>
<br/>
              3°) La constatation d'un désaccord sur le montant de l'indemnité, qui permet à la collectivité d'origine de la fixer selon les modalités prévues à l'article 51 de la loi du 26 janvier 1984, est-elle enfermée dans un délai déterminé, notamment au regard de la date de prise d'effet de la mutation de l'agent, et dans quelle mesure est-elle susceptible de se heurter à l'existence antérieure d'une renonciation explicite ou éventuellement implicite de cette collectivité à la percevoir '<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi n° 68-1250 du 31 décembre 1968 ;<br/>
<br/>
              Vu la loi n° 84-53 du 26 janvier 1984 ;<br/>
<br/>
              Vu la loi n° 84-594 du 12 juillet 1984 ;<br/>
<br/>
              Vu le code de justice administrative, notamment ses articles L. 113-1 et R. 222-1 ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Christian Fournier, Maître des Requêtes,<br/>
<br/>
              - les conclusions de M. Edouard Geffray, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>REND L'AVIS SUIVANT <br/>
              Aux termes des dispositions de l'article L. 113-1 du code de justice administrative : " Avant de statuer sur une requête soulevant une question de droit nouvelle, présentant une difficulté sérieuse et se posant dans de nombreux litiges, le tribunal administratif ou la cour administrative d'appel peut, par une décision qui n'est susceptible d'aucun recours, transmettre le dossier de l'affaire au Conseil d'Etat, qui examine dans un  délai de trois mois la question soulevée. Il est sursis à toute décision au fond jusqu'à un avis du Conseil d'Etat ou, à défaut, jusqu'à  l'expiration de ce délai " ;<br/>
<br/>
              Aux termes de l'article 51 de la loi du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique : " Les mutations sont prononcées par l'autorité territoriale d'accueil. Sauf accord entre cette autorité et l'autorité qui emploie le fonctionnaire, la mutation prend effet à l'expiration du délai de préavis mentionné à l'article 14 bis du titre Ier du statut général. / Lorsque la mutation intervient dans les trois années qui suivent la titularisation de l'agent, la collectivité territoriale ou l'établissement public d'accueil verse à la collectivité territoriale ou à l'établissement public d'origine une indemnité au titre, d'une part, de la rémunération perçue par l'agent pendant le temps de formation obligatoire prévu au 1° de l'article 1er de la loi n° 84-594 du 12 juillet 1984 précitée et, d'autre part, le cas échéant, du coût de toute formation complémentaire suivie par l'agent au cours de ces trois années. A défaut d'accord sur le montant de cette indemnité, la collectivité territoriale ou l'établissement public d'accueil rembourse la totalité des dépenses engagées par la collectivité territoriale ou l'établissement public d'origine " ;<br/>
<br/>
              Ces dispositions confèrent à la collectivité territoriale ou à l'établissement public d'origine d'un fonctionnaire muté dans une autre collectivité ou un autre établissement public, lorsque la mutation intervient dans les trois années qui suivent la titularisation de ce fonctionnaire, une créance sur la collectivité territoriale ou l'établissement public d'accueil dont l'assiette est constituée, en principe, par la rémunération perçue par l'agent pendant le temps de formation obligatoire prévu au 1° de l'article 1er de la loi du 12 juillet 1984 et, le cas échéant, par le coût de toute formation complémentaire suivie par l'agent au cours de ces trois années. Pour la collectivité territoriale ou l'établissement public d'accueil, la dépense prévue par ces dispositions présente un caractère obligatoire.<br/>
<br/>
              Si l'article 51 de la loi du 26 janvier 1984 prévoit que les collectivités territoriales ou établissements publics d'origine et d'accueil peuvent s'accorder pour fixer le montant de l'indemnité à un niveau inférieur à celui qui résulterait de l'application des dispositions qu'il prévoit, dont il ne peut être exclu par principe qu'il puisse être arrêté à un montant nul, l'accord doit être explicite. Il doit indiquer les raisons pour lesquelles le montant de l'indemnité arrêté conjointement est inférieur au montant total des dépenses engagées à ce titre par la collectivité ou l'établissement d'origine.<br/>
<br/>
              La créance d'indemnité prévue par cet article prend naissance à la date d'effet de la mutation du fonctionnaire, quels que soient son montant et ses modalités de fixation. Le législateur n'a enfermé l'exercice de l'action en recouvrement de cette créance, en l'absence d'accord intervenu entre les collectivité territoriales ou établissements publics d'accueil et d'origine sur le montant de l'indemnité, dans aucun délai particulier. La collectivité territoriale ou l'établissement public d'accueil serait toutefois, le cas échéant, en droit d'opposer à la collectivité territoriale ou à l'établissement public d'origine la prescription quadriennale de la créance sur le fondement des dispositions de la loi du 31 décembre 1968 relative à la prescription des créances sur l'Etat, les départements, les communes et les établissements publics.<br/>
<br/>
              Le présent avis sera notifié au tribunal administratif de Lyon, à la COMMUNE DE MIONNAY et à la commune de Mézériat.<br/>
<br/>
              Il sera publié au Journal officiel de la République française.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-07-01-03 FONCTIONNAIRES ET AGENTS PUBLICS. STATUTS, DROITS, OBLIGATIONS ET GARANTIES. STATUT GÉNÉRAL DES FONCTIONNAIRES DE L'ÉTAT ET DES COLLECTIVITÉS LOCALES. DISPOSITIONS STATUTAIRES RELATIVES À LA FONCTION PUBLIQUE TERRITORIALE (LOI DU 26 JANVIER 1984). - MUTATION - PRINCIPE DU REMBOURSEMENT PAR LA COLLECTIVITÉ D'ACCUEIL DES FRAIS DE FORMATION À LA COLLECTIVITÉ D'ORIGINE (ART. 51) - 1) POSSIBILITÉ DE PRÉVOIR UN REMBOURSEMENT INFÉRIEUR OU NUL - EXISTENCE - 2) MODALITÉS - ACCORD OBLIGATOIREMENT EXPLICITE - 3) DÉLAI DE PRESCRIPTION PARTICULIER - ABSENCE.
</SCT>
<ANA ID="9A"> 36-07-01-03 Pour l'application de l'article 51 de la loi du 26 janvier 1984, qui pose le principe, en cas de mutation d'un fonctionnaire, du remboursement par la collectivité d'accueil des frais de formation à la collectivité d'origine :... ...1) Les collectivités territoriales ou établissements publics d'origine et d'accueil peuvent s'accorder pour fixer le montant de l'indemnité à un niveau inférieur à celui normalement dû, dont il ne peut être exclu par principe qu'il puisse être arrêté à un montant nul.,,2) Dans ces cas, l'accord doit être explicite et doit indiquer les raisons pour lesquelles le montant de l'indemnité arrêté conjointement est inférieur au montant total des dépenses engagées à ce titre par la collectivité ou l'établissement d'origine.,,3) La loi n'a enfermé l'exercice de l'action en recouvrement des créances résultant de ce remboursement, en l'absence d'accord, dans aucun délai particulier. La collectivité territoriale ou l'établissement public d'accueil serait toutefois, le cas échéant, en droit d'opposer à la collectivité territoriale ou à l'établissement public d'origine la prescription quadriennale de la créance sur le fondement des dispositions de la loi du 31 décembre 1968 relative à la prescription des créances sur l'Etat, les départements, les communes et les établissements publics.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
