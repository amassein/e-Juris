<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036682860</ID>
<ANCIEN_ID>JG_L_2018_03_000000415125</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/68/28/CETATEXT000036682860.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 07/03/2018, 415125</TITRE>
<DATE_DEC>2018-03-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>415125</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUVENIN, COUDRAY, GREVY ; SCP DELAMARRE, JEHANNIN</AVOCATS>
<RAPPORTEUR>Mme Cécile RENAULT</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:415125.20180307</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au juge des référés du tribunal administratif de Paris d'ordonner, sur le fondement des dispositions de l'article L. 521-1 du code de justice administrative, la suspension de la décision du 31 mai 2017 de la bourse du travail de Paris prononçant son licenciement, jusqu'à ce qu'il soit statué au fond sur la légalité de cette décision. <br/>
<br/>
              Par une ordonnance n° 1714228 du 3 octobre 2017, le juge des référés du tribunal administratif de Paris a suspendu l'exécution de cette décision.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 19 octobre et 3 novembre 2017 et le 12 janvier 2018 au secrétariat du contentieux du Conseil d'Etat, la bourse du travail de Paris demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de rejeter la demande de M. B...;<br/>
<br/>
              3°) de mettre à la charge de M. B...la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi n° 84-53 du 26 janvier 1984 ;<br/>
              - le décret n° 70-301 du 3 avril 1970 ;<br/>
              - le décret n° 88-145 du 15 février 1988 ;<br/>
              - le décret n° 94-415 du 24 mai 1994 ;<br/>
              - le code de justice administrative.<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Marc Pichon de Vendeuil, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Thouvenin, Coudray, Grevy, avocat de la bourse du travail de Paris et à la SCP Delamarre, Jéhannin, avocat de M. B...;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis au juge des référés du tribunal administratif de Paris que M. B..., recruté par la bourse du travail de Paris sous contrat à durée indéterminée à temps partiel conclu le 1er décembre 2008, pour assurer l'emploi de conseiller en droit du travail, a fait l'objet, le 31 mai 2017, d'une décision de licenciement pour faute grave ; qu'il a demandé au juge des référés du tribunal administratif la suspension des effets de cette décision jusqu'à ce qu'il soit statué au fond sur sa légalité ; que, par une ordonnance du 3 octobre 2017, le juge des référés a suspendu l'exécution de cette décision ;<br/>
<br/>
              Sur la qualité d'agent public de M. B...:<br/>
<br/>
              2. Considérant que les personnels non statutaires travaillant pour le compte d'un service public à caractère administratif géré par une personne publique sont des agents de droit public, quel que soit leur emploi ;<br/>
<br/>
              3. Considérant qu'il résulte des dispositions du décret du 3 avril 1970 portant réforme du statut de la bourse du travail de Paris que celle-ci est " un établissement public de caractère municipal doté de la personnalité morale " ; qu'elle a pour objet de concourir à la promotion économique et sociale des travailleurs, notamment par l'organisation d'activités d'enseignement et la fourniture de services de consultation ou d'information  ; qu'eu égard à son objet, aux modalités de son organisation et de son fonctionnement et à l'origine de ses ressources, principalement assurées par des subventions inscrites au budget de la ville de Paris, la bourse du travail de Paris doit être regardée comme exerçant une mission de  service public à caractère administratif ; qu'il suit de là qu'en jugeant que M.B..., employé en qualité de conseiller en droit du travail par la bourse du travail de Paris, était un agent contractuel de droit public, le juge des référés du tribunal administratif de Paris n'a pas commis d'erreur de droit ;<br/>
<br/>
<br/>
              Sur la suspension de l'exécution de la décision de licenciement :<br/>
<br/>
              4. Considérant qu'aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision (...) " ;<br/>
              5. Considérant, d'une part, que l'urgence justifie que soit prononcée la suspension d'un acte administratif lorsque l'exécution de celui-ci porte atteinte, de manière suffisamment grave et immédiate, à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre ; qu'il appartient au juge des référés d'apprécier concrètement, compte tenu des justifications fournies par le requérant, si les effets de l'acte litigieux sont de nature à caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, l'exécution de la décision soit suspendue ; qu'il lui appartient également, l'urgence s'appréciant objectivement et compte tenu de l'ensemble des circonstances de chaque espèce, de faire apparaître dans sa décision tous les éléments qui, eu égard notamment à l'argumentation des parties, l'ont conduit à considérer que la suspension demandée revêtait un caractère d'urgence ;<br/>
<br/>
              6. Considérant que la procédure instaurée par l'article L. 521-1 du code de justice administrative ne subordonne la saisine du juge des référés au respect d'aucun délai mais seulement à ce que l'urgence, qui peut apparaître après que la décision contestée a commencé à produire ses effets, soit justifiée à la date de la saisine ; <br/>
<br/>
              7. Considérant que c'est par une appréciation souveraine exempte de dénaturation que le juge des référés du tribunal administratif de Paris a relevé que la décision de licenciement du 31 mai 2017, sans préavis ni indemnité de licenciement, avait pour conséquence de priver M. B...de la majeure partie de ses revenus et que la seule circonstance que l'intéressé n'ait pas engagé de démarche auprès de Pôle emploi et n'ait introduit sa requête en référé que plusieurs mois après l'intervention de la décision de licenciement n'était pas, dans les circonstances de l'espèce, de nature à priver la suspension demandée de son caractère d'urgence ;<br/>
<br/>
              8. Considérant, d'autre part, qu'aux termes de l'article 2 de la loi du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale : " Les dispositions de la présente loi s'appliquent aux personnes qui, régies par le titre Ier du statut général des fonctionnaires de l'État et des collectivités territoriales, ont été nommées dans un emploi permanent et titularisées dans un grade de la hiérarchie administrative des communes, des départements, des régions ou des établissements publics en relevant, à l'exception des agents comptables des caisses de crédit municipal (...) " ; qu'aux termes de l'article 1er du décret du 15 février 1988 pris pour l'application de l'article 136 de la loi du 26 janvier 1984 modifiée portant dispositions statutaires relatives à la fonction publique territoriale et relatif aux agents non titulaires de la fonction publique territoriale : " Les dispositions du présent décret s'appliquent aux agents contractuels de droit public des collectivités et des établissements mentionnés à l'article 2 de la loi du 26 janvier 1984 susvisée qui sont recrutés ou employés dans les conditions définies aux articles 3, 3-1, 3-2, 3-3, 47, 110 et 110-1 de la loi du 26 janvier 1984 précitée, ou qui sont maintenus en fonctions en application du deuxième ou du troisième alinéa de l'article 136, de l'article 139 ou de l'article 139 bis de la même loi " ; qu'aux termes de l'article 37 du même décret : " Le pouvoir disciplinaire appartient à l'autorité territoriale ayant le pouvoir de procéder au recrutement. / L'agent contractuel à l'encontre duquel une procédure disciplinaire est engagée a droit à la communication de l'intégralité de son dossier individuel et de tous les documents annexes et à l'assistance de défenseurs de son choix. L'autorité territoriale doit informer l'intéressé de son droit à communication du dossier " ;<br/>
<br/>
              9. Considérant qu'il résulte de ces dispositions que l'obligation d'informer les agents publics du droit à communication de leur dossier prévue par le décret du 15 février 1988 s'applique aux agents publics contractuels de la bourse du travail de Paris ; que, dès lors, le juge des référés du tribunal administratif de Paris n'a pas commis d'erreur de droit en regardant comme de nature à créer, en l'état de l'instruction, un doute sérieux quant à la légalité du licenciement de M. B...le moyen tiré de ce que celui-ci, en méconnaissance des dispositions de l'article 37 du décret du 15 février 1988, n'a, à aucun moment de la procédure de licenciement pour faute grave dont il faisait l'objet, été mis à même de demander la  communication de son dossier individuel ;<br/>
<br/>
              10. Considérant qu'il résulte de tout ce qui précède que la bourse du travail de Paris n'est pas fondée à demander l'annulation de l'ordonnance qu'elle attaque ; que ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent, en conséquence, qu'être rejetées ;<br/>
<br/>
              11. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la bourse du travail de Paris la somme de 4 000 euros à verser à M. B...au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la bourse du travail de Paris est rejeté.<br/>
Article 2 : La bourse du travail de Paris versera la somme de 4 000 euros à M. B...au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la bourse de travail de Paris et à M. A...B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">33-01-03-01 ÉTABLISSEMENTS PUBLICS ET GROUPEMENTS D'INTÉRÊT PUBLIC. NOTION D'ÉTABLISSEMENT PUBLIC. CARACTÈRE DE L'ÉTABLISSEMENT. CARACTÈRE ADMINISTRATIF. - BOURSE DU TRAVAIL DE PARIS.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">33-02-06-01-01 ÉTABLISSEMENTS PUBLICS ET GROUPEMENTS D'INTÉRÊT PUBLIC. RÉGIME JURIDIQUE DES ÉTABLISSEMENTS PUBLICS. PERSONNEL. QUALITÉ. AGENT PUBLIC. - CONSEILLER EN DROIT DU TRAVAIL EMPLOYÉ PAR LA BOURSE DU TRAVAIL DE PARIS, ÉTABLISSEMENT PUBLIC ADMINISTRATIF.
</SCT>
<ANA ID="9A"> 33-01-03-01 ll résulte du décret n° 70-301 du 3 avril 1970 que la bourse du travail de Paris est un établissement public de caractère municipal doté de la personnalité morale. Elle a pour objet de concourir à la promotion économique et sociale des travailleurs, notamment par l'organisation d'activités d'enseignement et la fourniture de services de consultation ou d'information. Eu égard à son objet, aux modalités de son organisation et de son fonctionnement et à l'origine de ses ressources, principalement assurées par des subventions inscrites au budget de la ville de Paris, la bourse du travail de Paris doit être regardée comme exerçant une mission de service public à caractère administratif.</ANA>
<ANA ID="9B"> 33-02-06-01-01 ll résulte du décret n°70-301 du 3 avril 1970 que la bourse du travail de Paris est un établissement public de caractère municipal doté de la personnalité morale. Elle a pour objet de concourir à la promotion économique et sociale des travailleurs, notamment par l'organisation d'activités d'enseignement et la fourniture de services de consultation ou d'information. Eu égard à son objet, aux modalités de son organisation et de son fonctionnement et à l'origine de ses ressources, principalement assurées par des subventions inscrites au budget de la ville de Paris, la bourse du travail de Paris doit être regardée comme exerçant une mission de  service public à caractère administratif. Il suit de là que l'intéressé, employé en qualité de conseiller en droit du travail par la bourse du travail de Paris, est un agent contractuel de droit public.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
