<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026230139</ID>
<ANCIEN_ID>JG_L_2012_07_000000357824</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/23/01/CETATEXT000026230139.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème et 9ème sous-sections réunies, 27/07/2012, 357824</TITRE>
<DATE_DEC>2012-07-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>357824</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème et 9ème sous-sections réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARTHELEMY, MATUCHANSKY, VEXLIARD ; SCP ANCEL, COUTURIER-HELLER, MEIER-BOURDEAU</AVOCATS>
<RAPPORTEUR>M. Nicolas Labrune</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CESSR:2012:357824.20120727</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le jugement du 15 mars 2012, enregistré le 22 mars 2012 au secrétariat du contentieux du Conseil d'Etat, par lequel le tribunal administratif de Nouvelle-Calédonie, avant de statuer sur la demande de M. Philippe B tendant à l'annulation de la délibération de l'assemblée de la province Sud n° 37-2011/APS du 9 novembre 2011 relative aux zones de restructuration de l'habitat spontané, a transmis, en application des dispositions de l'article 205 de la loi organique du 19 mars 1999, le dossier de cette demande au Conseil d'Etat, en soumettant à son examen la question de savoir quelle est l'autorité compétente, de la Nouvelle-Calédonie, la province ou la commune, pour instituer des zones de restructuration de l'habitat spontané ;<br/>
<br/>
              Vu les observations, enregistrées le 4 mai 2012, présentées par le ministre des outre-mer ;<br/>
<br/>
              Vu les observations, enregistrées le 14 mai 2012, présentées pour le gouvernement de la Nouvelle-Calédonie ;<br/>
<br/>
              Vu les observations, enregistrées le 6 juillet 2012, présentées pour la province Sud de Nouvelle-Calédonie ;<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution ;<br/>
<br/>
              Vu la loi organique n° 99-209 du 19 mars 1999 relative à la Nouvelle-Calédonie ;<br/>
<br/>
              Vu le code des communes de la Nouvelle-Calédonie ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Nicolas Labrune, Auditeur,<br/>
<br/>
              - les observations de la SCP Ancel, Couturier-Heller, Meier-Bourdeau avocat du gouvernement de la Nouvelle-Calédonie et de la SCP Barthélemy, Matuchansky, Vexliard avocat de l'assemblée de la province Sud,<br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Ancel, Couturier-Heller, Meier-Bourdeau avocat du gouvernement de la Nouvelle-Calédonie et à la SCP Barthélemy, Matuchansky, Vexliard avocat de l'assemblée de la province Sud ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>			REND L'AVIS SUIVANT :<br/>
<br/>
              1. Aux termes de l'article 204 de la loi organique du 19 mars 1999 relative à la Nouvelle-Calédonie : " I. Les actes (...) de l'assemblée de province, de son bureau et de son président mentionnés au II sont exécutoires de plein droit dès qu'il a été procédé à leur publication au Journal officiel de la Nouvelle-Calédonie ou à leur notification aux intéressés, ainsi qu'à leur transmission au haut-commissaire ou à son représentant dans la province, (...) par le président de l'assemblée de province. (...) / II. Sont soumis aux dispositions du I les actes suivants : (...) / D. Pour les assemblées de province : (...) / 1° Leurs délibérations ou les décisions prises par délégation de l'assemblée en application de l'article 168 ; (...) ". Aux termes de l'article 205 de la même loi : " Lorsque le tribunal administratif est saisi d'un recours pour excès de pouvoir dirigé contre les actes mentionnés aux (...) 1° à 3° du D du II de l'article 204 et que ce recours est fondé sur un moyen sérieux invoquant l'inexacte application de la répartition des compétences entre l'Etat, la Nouvelle-Calédonie, les provinces et les communes ou que ce moyen est soulevé d'office, il transmet le dossier sans délai pour avis au Conseil d'Etat, par un jugement qui n'est susceptible d'aucun recours. Le Conseil d'Etat examine la question soulevée dans un délai de trois mois et il est sursis à toute décision sur le fond jusqu'à son avis ou, à défaut, jusqu'à l'expiration de ce délai. Le tribunal administratif statue dans un délai de deux mois à compter de la publication de l'avis au Journal officiel de la Nouvelle-Calédonie ou de l'expiration du délai imparti au Conseil d'Etat. ".<br/>
<br/>
              2. En application de ces dispositions, le tribunal administratif de Nouvelle-Calédonie a sursis à toute décision au fond sur la demande de M. B tendant à l'annulation de la délibération de l'assemblée de la province Sud n° 37-2011/APS du 9 novembre 2011 relative aux zones de restructuration de l'habitat spontané et a transmis le dossier au Conseil d'Etat en lui posant la question de la légalité de cette délibération au regard de la répartition des compétences entre la Nouvelle-Calédonie, la province et les communes pour créer un nouveau dispositif d'aménagement à disposition de la province et des communes de la province Sud.<br/>
<br/>
              3. La délibération dont il est demandé au Conseil d'Etat d'apprécier si elle relève des compétences de la province ou de celles de la Nouvelle-Calédonie ou des communes donne, aux termes de son article 1er , à la province Sud et aux communes de la province Sud la possibilité de créer, dans les zones constituées de " locaux ou installations, à usage d'habitation, édifiés sans droit ni titre sur la propriété de personnes publiques ou privées et présentant un caractère d'insalubrité ", dénommées zones d'habitat spontané, des " zones de restructuration de l'habitat spontané ", définies comme des " zones d'aménagement " à l'intérieur desquelles la province ou les communes " décident d'intervenir afin d'améliorer les conditions de vie des habitants par la réalisation d'aménagements ou d'équipements publics dans un but d'intérêt général ". Le même article prévoit que " dans le cadre de ces interventions, la province ou les communes peuvent engager des actions de nature à : réaliser des équipements collectifs ; protéger l'environnement immédiat par le traitement des eaux résiduaires ; améliorer l'insertion du quartier dans son environnement ; permettre l'accès à des services publics ". Ces zones peuvent être instituées " sur tout type de zone " et localisées sur des terrains appartenant à la collectivité à l'initiative de l'aménagement, en voie d'acquisition ou d'expropriation ou sur des terrains privés sous réserve de l'accord du propriétaire. Les procédures de création, suppression et modification de ces zones sont décrites ainsi que les modalités de division parcellaire qui résultent de leur institution.<br/>
<br/>
              4. M. B soutient devant le tribunal administratif de la Nouvelle-Calédonie que cette délibération méconnaît d'une part la compétence de la Nouvelle-Calédonie pour adopter les principes directeurs du droit de l'urbanisme, d'autre part la compétence des communes pour prendre en charge les services publics de distribution d'eau potable, d'assainissement collectif et de collecte des ordures ménagères.<br/>
<br/>
              5. Aux termes de l'article 20 de la loi organique du 19 mars 1999 relative à la Nouvelle-Calédonie : " Chaque province est compétente dans toutes les matières qui ne sont pas dévolues à l'Etat ou à la Nouvelle-Calédonie par la présente loi, ou aux communes par la législation applicable en Nouvelle-Calédonie. " Aux termes de l'article 22 de la même loi : " La Nouvelle-Calédonie est compétente dans les matières suivantes : (...) / 21° Principes directeurs du droit de l'urbanisme ; (...) ". Ces principes directeurs doivent s'entendre non comme correspondant aux principes fondamentaux dont la détermination est réservée au législateur par l'article 34 de la Constitution, ni aux normes adoptées en métropole par le législateur, mais comme les principes relatifs à l'urbanisme et concernant, sur le fond et quant à la procédure, l'encadrement des atteintes au droit de propriété, la détermination des compétences et la garantie de la cohésion territoriale. Constituent donc des principes directeurs du droit de l'urbanisme, au sens de ces dispositions, les règles générales relatives à l'utilisation du sol, aux documents d'urbanisme, aux procédures d'aménagement et au droit de préemption, ainsi que celles relatives à la détermination des autorités compétentes pour élaborer et approuver les documents d'urbanisme, conduire les procédures d'aménagement, délivrer les autorisations d'urbanisme et exercer le droit de préemption. Font également partie de ces principes directeurs les règles générales régissant l'exercice de ces compétences. En ce qui concerne plus précisément les procédures d'aménagement, relèvent des principes directeurs la fixation des finalités des actions ou opérations d'aménagement, la définition de leur champ d'application, la détermination des autorités compétentes pour les mettre en oeuvre, des procédures et modalités auxquelles cette mise en oeuvre est soumise ainsi que des pouvoirs qui peuvent y être exercés.<br/>
<br/>
              6. L'institution, par la délibération litigieuse, d'une nouvelle procédure d'aménagement telle que les " zones de restructuration de l'habitat spontané ", la détermination de son contenu, de ses effets, notamment sur les zonages résultant des documents locaux d'urbanisme en vigueur, et des collectivités qui peuvent la mettre en oeuvre ainsi que des modalités de cette mise en oeuvre, relèvent des principes directeurs du droit de l'urbanisme, alors même que son application ne porterait pas nécessairement atteinte au droit de propriété et qu'elle tendrait à la réalisation d'opérations temporaires. Par conséquent, la province Sud n'était pas compétente pour instituer une telle procédure d'aménagement, alors même qu'elle est compétente en matière de logement social, dès lors que la nature sociale des objectifs que cette procédure est susceptible de poursuivre est sans incidence sur le fait que sa création relève des principes directeurs du droit de l'urbanisme. Enfin, la circonstance alléguée que de telles zones ne seraient susceptibles d'être instituées que dans la province Sud, compte tenu de ce que l'habitat spontané ne s'est développé que sur son territoire, est sans incidence sur les règles de répartition des compétences.<br/>
<br/>
              7. L'incompétence de la province pour instituer une procédure d'aménagement du territoire rend sans objet la question de savoir si, ce faisant, la province a porté atteinte aux compétences des communes.<br/>
<br/>
              Le présent avis sera notifié au président du tribunal administratif de Nouvelle-Calédonie, à M. B, au président de l'assemblée de la province Sud de la Nouvelle-Calédonie, au président du gouvernement de la Nouvelle-Calédonie, au haut-commissaire de la République en Nouvelle-Calédonie et au ministre des outre-mer.<br/>
<br/>
              Il sera publié au Journal officiel de la Nouvelle-Calédonie.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">46-01-02-01 OUTRE-MER. DROIT APPLICABLE DANS LES COLLECTIVITÉS D'OUTRE-MER ET EN NOUVELLE-CALÉDONIE. STATUT DES COLLECTIVITÉS D'OUTRE-MER ET DE LA NOUVELLE-CALÉDONIE. NOUVELLE-CALÉDONIE. - LOI ORGANIQUE DU 19 MARS 1999 - ARTICLE 22 CONFÉRANT COMPÉTENCE À LA NOUVELLE-CALÉDONIE POUR LES PRINCIPES DIRECTEURS DU DROIT DE L'URBANISME - NOTION DE PRINCIPES DIRECTEURS [RJ1].
</SCT>
<ANA ID="9A"> 46-01-02-01 Aux termes de l'article 20 de la loi organique n° 99-209 du 19 mars 1999 relative à la Nouvelle-Calédonie : « Chaque province est compétente dans toutes les matières qui ne sont pas dévolues à l'Etat ou à la Nouvelle-Calédonie par la présente loi, ou aux communes par la législation applicable en Nouvelle-Calédonie. » Aux termes de l'article 22 de la même loi : « La Nouvelle-Calédonie est compétente dans les matières suivantes : (&#133;) / 21° Principes directeurs du droit de l'urbanisme (&#133;) ».... ...Ces principes directeurs doivent s'entendre non comme correspondant aux principes fondamentaux dont la détermination est réservée au législateur par l'article 34 de la Constitution, ni aux normes adoptées en métropole par le législateur, mais comme les principes relatifs à l'urbanisme et concernant, sur le fond et quant à la procédure, l'encadrement des atteintes au droit de propriété, la détermination des compétences et la garantie de la cohésion territoriale. Constituent donc des principes directeurs du droit de l'urbanisme, au sens de ces dispositions, les règles générales relatives à l'utilisation du sol, aux documents d'urbanisme, aux procédures d'aménagement et au droit de préemption, ainsi que celles relatives à la détermination des autorités compétentes pour élaborer et approuver les documents d'urbanisme, conduire les procédures d'aménagement, délivrer les autorisations d'urbanisme et exercer le droit de préemption. Font également partie de ces principes directeurs les règles générales régissant l'exercice de ces compétences. En ce qui concerne plus précisément les procédures d'aménagement, relèvent des principes directeurs la fixation des finalités des actions ou opérations d'aménagement, la définition de leur champ d'application, la détermination des autorités compétentes pour les mettre en oeuvre, des procédures et modalités auxquelles cette mise en oeuvre est soumise ainsi que des pouvoirs qui peuvent y être exercés.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr. CE, Section des travaux publics, avis, 18 mai 2010, n° 383819, Rapport public 2011 p. 383.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
