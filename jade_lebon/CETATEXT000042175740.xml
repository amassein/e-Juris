<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042175740</ID>
<ANCIEN_ID>JG_L_2020_07_000000437283</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/17/57/CETATEXT000042175740.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 29/07/2020, 437283</TITRE>
<DATE_DEC>2020-07-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>437283</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>CABINET MUNIER-APAIRE ; SCP BOUTET-HOURDEAUX</AVOCATS>
<RAPPORTEUR>Mme Pauline Berne</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Gabrielle Merloz</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:437283.20200729</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Les communes de Salses-le-Château, Duilhac sous Peyrepertuse, Soulatge, Tuchan, Paziols et Montgaillard ont demandé au juge des référés du tribunal administratif de Montpellier, sur le fondement de l'article L. 521-1 du code de justice administrative, de suspendre l'exécution de l'arrêté du préfet de l'Aude et du préfet des Pyrénées-Orientales en date du 25 novembre 2019 autorisant l'extension des compétences supplémentaires de la communauté de communes Corbières Salanque Méditerranée à l'eau et l'assainissement des eaux usées à compter du 1er janvier 2020. <br/>
<br/>
              Par une ordonnance n° 1906501 du 18 décembre 2019, le juge des référés du tribunal administratif de Montpellier a rejeté cette demande. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 2 et 17 janvier 2020 au secrétariat du contentieux du Conseil d'Etat, la commune de Salses-le-Château et la commune de Duilhac-sous-Peyrepertuse demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) réglant l'affaire en référé, de faire droit à leur demande ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 500 euros sur le fondement de l'article L. 761-1 du code de justice administrative.  <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des collectivités territoriales ;<br/>
              - la loi n° 2015-991 du 7 août 2015 ;<br/>
              - la loi n° 2018-702 du 3 août 2018 ;<br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Pauline Berne, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Marie-Gabrielle Merloz, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, au cabinet Munier-Apaire, avocat de la commune de Salses-le-Château et de la commune de Duilhac-sous-Peyrepertuse et à la SCP Boutet-Hourdeaux, avocat de la communauté de communes Corbieres Salanque Méditerranée ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge des référés que, par des délibérations s'opposant au transfert obligatoire au 1er janvier 2020 des compétences de l'eau et de l'assainissement des eaux usées à la communauté de communes Corbières Salanque Méditerranée dont elles sont membres, les communes de Salses-le-Château, Duilhac-sous-Peyrepertuse, Fontjoncouse, Tuchan, Paziols, Montgaillard et Soulatge se sont opposées à ce transfert, avant le 1er juillet 2019, dans les conditions prévues par la loi du 3 août 2018 relative à la mise en oeuvre du transfert des compétences eau et assainissement aux communautés de communes, repoussant ainsi au 1er janvier 2026 l'exercice obligatoire de ces compétences par la communauté de communes. Par délibération du 22 juillet 2019, la communauté de communes a néanmoins approuvé leur transfert au titre des compétences facultatives susceptibles d'être exercées en application des dispositions de l'article L. 5211-17 du code général des collectivités territoriales et, par arrêté interpréfectoral du 25 novembre 2019, le préfet de l'Aude et le préfet des Pyrénées Orientales ont étendu les compétences de la communauté de communes à l'eau et l'assainissement à compter du 1er janvier 2020. Les communes de Salses-le-Château, Duilhac-sous-Peyrepertuse, Soulatge, Tuchan, Paziols et Montgaillard ont demandé l'annulation de cet arrêté au tribunal administratif de Montpellier ainsi que la suspension de son exécution, sur le fondement de l'article L. 521-1 du code de justice administrative. Le juge des référés du tribunal administratif de Montpellier a rejeté cette demande par une ordonnance du 18 décembre 2019 contre laquelle les communes de Salses-le-Château et Duilhac-sous-Peyrepertuse se pourvoient en cassation.  <br/>
<br/>
              2. Aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision (...) ". <br/>
<br/>
              3. Selon les dispositions générales de l'article L. 5211-17 du code général des collectivités territoriales : " Les communes membres d'un établissement public de coopération intercommunale peuvent à tout moment transférer, en tout ou partie, à ce dernier, certaines de leurs compétences dont le transfert n'est pas prévu par la loi (...) ainsi que les biens, équipements ou services publics nécessaires à leur exercice. / Ces transferts sont décidés par délibérations concordantes de l'organe délibérant et des conseils municipaux se prononçant dans les conditions de majorité requise pour la création de l'établissement public de coopération intercommunale (...). Le transfert de compétences est prononcé par arrêté du ou des représentants de l'Etat dans le ou les départements intéressés ". <br/>
<br/>
              4. Les dispositions particulières du IV de l'article 64 de la loi du 7 août 2015 portant nouvelle organisation territoriale de la République ont toutefois prévu le transfert obligatoire au 1er janvier 2020 des compétences eau et assainissement aux communautés de communes. Cependant, aux termes de l'article 1er de la loi du 3 août 2018 relative à la mise en oeuvre du transfert des compétences eau et assainissement aux communautés de communes : " Les communes membres d'une communauté de communes qui n'exerce pas, à la date de la publication de la présente loi, à titre optionnel ou facultatif, les compétences relatives à l'eau ou à l'assainissement peuvent s'opposer au transfert obligatoire, résultant du IV de l'article 64 de la loi n° 2015-991 du 7 août 2015 portant nouvelle organisation territoriale de la République, de ces deux compétences, ou de l'une d'entre elles, à la communauté de communes si, avant le 1er juillet 2019, au moins 25 % des communes membres de la communauté de communes représentant au moins 20 % de la population délibèrent en ce sens. En ce cas, le transfert de compétences prend effet le 1er janvier 2026. (...) Si, après le 1er janvier 2020, une communauté de communes n'exerce pas les compétences relatives à l'eau et à l'assainissement ou l'une d'entre elles, l'organe délibérant de la communauté de communes peut également, à tout moment, se prononcer par un vote sur l'exercice de plein droit d'une ou de ces compétences par la communauté. Les communes membres peuvent toutefois s'opposer à cette délibération, dans les trois mois, dans les conditions prévues au premier alinéa ". <br/>
<br/>
              5. Il résulte des dispositions spéciales de la loi du 3 août 2018, citées au point 4, que lorsque au moins 25 % des communes membres d'une communauté de communes représentant au moins 20 % de la population s'opposent, avant le 1er juillet 2019, au transfert obligatoire des compétences eau et assainissement à la communauté de communes au 1er janvier 2020, de sorte que ce transfert obligatoire est reporté au 1er janvier 2026, les dispositions générales de l'article L. 5211-17, relatives aux transferts facultatifs de compétences, qui renvoient notamment aux conditions de majorité requise pour la création de l'établissement public de coopération intercommunale, ne peuvent recevoir application entre le 1er juillet 2019 et le 1er janvier 2020. Après cette dernière date, ces dispositions générales ne peuvent recevoir application qu'à la condition que ne s'y opposent pas, dans les trois mois, au moins 25 % des communes représentant au moins 20 % de la population. <br/>
<br/>
              6. Par suite, en jugeant que n'était pas de nature à créer un doute sérieux quant à la légalité de l'arrêté du 25 novembre 2019, pris par les préfets de l'Aude et des Pyrénées-Orientales sur le fondement de l'article L. 5211-17 du code général des collectivités territoriales et étendant à l'eau et à l'assainissement les compétences de la communauté de communes Corbières Salanque Méditerranée après que l'organe délibérant de cette communauté de communes eut décidé ce transfert par délibération du 22 juillet 2019, le moyen tiré de ce que des communes, dans une proportion satisfaisant aux conditions prévues par l'article 1er de la loi du 3 août 2018, s'étaient opposées, avant le 1er juillet 2019, au transfert obligatoire de ces compétences au 1er janvier 2020, le juge des référés du tribunal administratif de Montpellier a commis une erreur de droit. Les communes requérantes sont, dès lors, fondées à demander l'annulation de l'ordonnance qu'elles attaquent.<br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au titre de la procédure de référé engagée, en application des dispositions de l'article L. 821-2 du code de justice administrative. <br/>
<br/>
              En ce qui concerne la condition d'urgence :<br/>
<br/>
              8. Lorsqu'un arrêté préfectoral a pour objet de modifier la répartition des compétences entre une collectivité territoriale et un groupement de collectivités territoriales ou entre deux groupements de collectivités territoriales, la condition d'urgence à laquelle est subordonnée l'octroi d'une mesure de suspension doit être regardée, en principe et eu égard à la nature de cette décision, comme remplie. <br/>
<br/>
              9. Eu égard à l'objet de l'arrêté contesté, qui modifie la répartition des compétences entre la communauté de communes Corbières Salanque Méditerranée et ses communes membres, la condition d'urgence doit être regardée en l'espèce comme remplie. Les circonstances alléguées en défense, tirées de l'insuffisance manifeste du service d'eau potable et d'assainissement dans le périmètre communautaire et de l'absence de capacités en la matière de plusieurs communes, ne sont pas de nature à écarter cette présomption. <br/>
<br/>
              En ce qui concerne la condition tenant à l'existence d'un moyen de nature à créer un doute sérieux quant à la légalité de la décision attaquée :<br/>
<br/>
              10. Il résulte de ce qui a été dit au point 5 que, dès lors que s'est exprimée avant le 1er juillet 2019 une opposition de communes membres de la communauté de communes dans les conditions prévues par l'article 1er de la loi du 3 août 2018, avec pour effet de repousser au 1er janvier 2026 le transfert obligatoire des compétences eau et assainissement à la communauté de communes Corbières Salanque Méditerranée, le moyen tiré de ce que le recours aux dispositions de l'article L. 5211-17 du code général des collectivités territoriales entre le 1er juillet 2019 et le 1er janvier 2020 en vue d'un transfert de ces mêmes compétences au 1er janvier 2020 n'était pas légalement possible est de nature, en l'état de l'instruction, à créer un doute sérieux quant à la légalité de l'arrêté contesté. <br/>
<br/>
              11. Il résulte de ce qui précède que les communes de Salses-le-Château et de Duilhac-sous-Peyrepertuse sont fondées à demander la suspension de l'exécution de l'arrêté du préfet de l'Aude et du préfet des Pyrénées orientales en date du 25 novembre 2019 autorisant l'extension des compétences supplémentaires de la communauté de communes Corbières Salanque Méditerranée à l'eau et à l'assainissement à compter du 1er janvier 2020. <br/>
<br/>
              12. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement de la somme de 3 000 euros aux communes de Salses-le-Château et de Duilhac-sous-Peyrepertuse au titre de l'article L. 761-1 du code de justice administrative. Les dispositions de cet article font obstacle à ce qu'il soit fait droit à la demande présentée sur leur fondement par la communauté de communes Corbières Salanque Méditerranée. <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'ordonnance du 18 décembre 2019 du juge des référés du tribunal administratif de Montpellier est annulée.<br/>
<br/>
Article 2 : L'exécution de l'arrêté du 25 novembre 2019 des préfets de l'Aude et des Pyrénées-Orientales autorisant l'extension des compétences supplémentaires de la communauté de communes Corbières Salanque Méditerranée à l'eau et à l'assainissement à compter du 1er janvier 2020 est suspendue.<br/>
<br/>
Article 3 : L'Etat versera aux communes de Salses-le-Château et de Duilhac-sous-Peyrepertuse la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : Les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative par la communauté de communes Corbières Salanque Méditerranée sont rejetées.<br/>
<br/>
Article 5 : La présente décision sera notifiée aux communes de Salses-le-Château et de Duilhac-sous-Peyrepertuse, au ministre de l'intérieur, à la ministre de la cohésion des territoires et des relations avec les collectivités territoriales et à la communauté de communes Corbières Salanque Méditerranée. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">135-02-03-03 COLLECTIVITÉS TERRITORIALES. COMMUNE. ATTRIBUTIONS. SERVICES COMMUNAUX. - TRANSFERT OBLIGATOIRE DES COMPÉTENCES EAU ET ASSAINISSEMENT AUX COMMUNAUTÉS DE COMMUNES AU 1ER JANVIER 2020 - REPORT EN CAS D'OPPOSITION D'UNE PARTIE DES COMMUNES MEMBRES (ART. 1ER DE LA LOI DU 3 AOÛT 2018) - APPLICATION DES RÈGLES RELATIVES AUX TRANSFERTS FACULTATIFS DE COMPÉTENCES (ART. L. 5211-17 DU CGCT) ENTRE LE 1ER JUILLET 2019 ET LE 1ER JANVIER 2020 - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">135-05-01-05 COLLECTIVITÉS TERRITORIALES. COOPÉRATION. ÉTABLISSEMENTS PUBLICS DE COOPÉRATION INTERCOMMUNALE - QUESTIONS GÉNÉRALES. COMMUNAUTÉS DE COMMUNES. - TRANSFERT OBLIGATOIRE DES COMPÉTENCES EAU ET ASSAINISSEMENT AUX COMMUNAUTÉS DE COMMUNES AU 1ER JANVIER 2020 - REPORT EN CAS D'OPPOSITION D'UNE PARTIE DES COMMUNES MEMBRES (ART. 1ER DE LA LOI DU 3 AOÛT 2018) - APPLICATION DES RÈGLES RELATIVES AUX TRANSFERTS FACULTATIFS DE COMPÉTENCES (ART. L. 5211-17 DU CGCT) ENTRE LE 1ER JUILLET 2019 ET LE 1ER JANVIER 2020 - ABSENCE.
</SCT>
<ANA ID="9A"> 135-02-03-03 Il résulte des dispositions spéciales de l'article 1er de la loi n° 2018-702 du 3 août 2018 que lorsque au moins 25 % des communes membres d'une communauté de communes représentant au moins 20 % de la population s'opposent, avant le 1er juillet 2019, au transfert obligatoire des compétences eau et assainissement à la communauté de communes au 1er janvier 2020, de sorte que ce transfert obligatoire est reporté au 1er janvier 2026, les dispositions générales de l'article L. 5211-17 du code général des collectivités territoriales (CGCT), relatives aux transferts facultatifs de compétences, qui renvoient notamment aux conditions de majorité requise pour la création de l'établissement public de coopération intercommunale, ne peuvent recevoir application entre le 1er juillet 2019 et le 1er janvier 2020. Après cette dernière date, ces dispositions générales ne peuvent recevoir application qu'à la condition que ne s'y opposent pas, dans les trois mois, au moins 25 % des communes représentant au moins 20 % de la population.</ANA>
<ANA ID="9B"> 135-05-01-05 Il résulte des dispositions spéciales de l'article 1er de la loi n° 2018-702 du 3 août 2018 que lorsque au moins 25 % des communes membres d'une communauté de communes représentant au moins 20 % de la population s'opposent, avant le 1er juillet 2019, au transfert obligatoire des compétences eau et assainissement à la communauté de communes au 1er janvier 2020, de sorte que ce transfert obligatoire est reporté au 1er janvier 2026, les dispositions générales de l'article L. 5211-17 du code général des collectivités territoriales (CGCT), relatives aux transferts facultatifs de compétences, qui renvoient notamment aux conditions de majorité requise pour la création de l'établissement public de coopération intercommunale, ne peuvent recevoir application entre le 1er juillet 2019 et le 1er janvier 2020. Après cette dernière date, ces dispositions générales ne peuvent recevoir application qu'à la condition que ne s'y opposent pas, dans les trois mois, au moins 25 % des communes représentant au moins 20 % de la population.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
