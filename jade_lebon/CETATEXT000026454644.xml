<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026454644</ID>
<ANCIEN_ID>JG_L_2012_10_000000348476</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/45/46/CETATEXT000026454644.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème et 2ème sous-sections réunies, 03/10/2012, 348476</TITRE>
<DATE_DEC>2012-10-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>348476</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème et 2ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARTHELEMY, MATUCHANSKY, VEXLIARD ; FOUSSARD</AVOCATS>
<RAPPORTEUR>M. Fabrice Aubert</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:348476.20121003</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 14 avril et 13 juillet 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour la société Eiffage travaux publics Méditerranée, dont le siège est au 5 rue de Copenhague à Vitrolles (13127) ; la société Eiffage travaux publics Méditerranée demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 08MA02493 du 14 février 2011, par lequel la cour administrative d'appel de Marseille a rejeté sa requête tendant, d'une part, à l'annulation du jugement n° 0503205 du 11 mars 2008 du tribunal administratif de Marseille rejetant sa requête tendant à ce que le département des Bouches-du-Rhône soit condamné à lui verser la somme de 354 842,13 euros avec intérêts à compter du 20 juin 2003 et capitalisation, en règlement d'un marché public à bons de commande et, d'autre part, à ce que cette somme soit mise à la charge du département des Bouches-du-Rhône ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
              3°) de mettre à la charge du département des Bouches-du-Rhône le versement de la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code des marchés publics ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Fabrice Aubert, Auditeur,  <br/>
<br/>
              - les observations de la SCP Barthélemy, Matuchansky, Vexliard, avocat de la société Eiffage travaux publics Méditerranée et de Me Foussard, avocat du département des Bouches-du-Rhône,<br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Barthélemy, Matuchansky, Vexliard, avocat de la société Eiffage travaux publics Méditerranée et à Me Foussard, avocat du département des Bouches-du-Rhône ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que le département des Bouches-du-Rhône a confié, par un marché public à bons de commande, signé le 27 juin 2001, la réalisation et la pose d'enrobés sur les routes départementales à la société Appia 13, aux droits de laquelle est venue la société Eiffage travaux publics Méditerranée ; que pour la commande de travaux exécutés entre juillet 2001 et juillet 2002, la société a transmis ses factures au département en signalant, dès le 20 septembre 2001, avoir dû réaliser des travaux supplémentaires de découpe et rabotage, de nettoyage et d'évacuation de matériaux hors des chantiers, pour lesquels le marché n'avait pas prévu de prix unitaire dans le bordereau de prix ; que le département, qui a fixé un prix unitaire pour ces prestations par un ordre de service daté du 23 juillet 2002, a procédé au règlement de la commande en refusant de rémunérer les travaux supplémentaires réalisés par la société antérieurement à cet ordre de service ; que par un  arrêt du 14 février 2011, contre lequel la société Eiffage travaux publics Méditerranée se pourvoit en cassation, la cour administrative d'appel de Marseille a confirmé le jugement du 11 mars 2008 par lequel le tribunal administratif de Marseille a rejeté la demande de la société tendant à la condamnation du département à lui verser la somme de 354 842,13 euros au titre de l'indemnisation des travaux supplémentaires litigieux ; <br/>
<br/>
              2. Considérant que le cocontractant de l'administration peut demander à être indemnisé, sur la base du contrat, des travaux supplémentaires réalisés sans ordre de service, dès lors que ces travaux ont été indispensables à l'exécution du contrat dans les règles de l'art ;<br/>
<br/>
              3. Considérant, d'une part, qu'aux termes de l'article 3-3.6 du cahier des clauses administratives particulières du marché : " (...) par dérogation aux articles 13 et 13 bis du cahier des clauses administratives générales, les comptes seront réglés dans les conditions fixées ci-après : 3-3.6.1 - Remise de la facture. L'entreprise ayant exécuté la commande établit une facture précisant les sommes auxquelles elle prétend du fait de l'exécution du marché. (...) / 3-3.6.2 - Acceptation de la facture. Le chef du service études et travaux de la DR ou de la subdivision territoriale DDE 13 concernée accepte ou rectifie la facture et arrête le montant de la somme à régler. Celui-ci est notifié à l'entreprise si la facture a été modifiée. Passé un délai de trente jours à compter de cette notification, l'entreprise est réputée par son silence avoir accepté ce montant. / 3-3.6.3 - Paiements partiels définitifs. Le paiement de l'ensemble d'une commande est considéré comme paiement définitif. / (...) / 3-3.6.5 - Etablissement de la facture. La facture fera apparaître en tant que de besoin : (...) - Les prix unitaires hors-taxes correspondants avec les numéros indiqués au bordereau de prix unitaires. (...)  " ; que, d'autre part, aux termes de l'article 9-2.1 du même cahier : " Par dérogation aux articles 41 et 42 du CCAG, les opérations de réception font l'objet des dispositions suivantes : 9-2.1 : A l'achèvement des travaux relatifs à une commande, la personne responsable du marché (...) procède, en présence de l'entrepreneur, aux opérations de réception " ;<br/>
<br/>
              4. Considérant, en premier lieu, que, contrairement à ce que soutient la société, ni l'article 14 du cahier des clauses administratives générales applicable au marché, qui ne concerne que les travaux supplémentaires réalisés sur ordre de service notifié à l'entreprise, ni l'article 3.3.6.5 précité du cahier des clauses administratives particulières, qui ne prévoit qu'en tant que de besoin, que les factures transmises par le cocontractant fassent référence aux prix unitaires du bordereau de prix, n'imposaient que le département fixât un prix unitaire pour les travaux supplémentaires nécessaires à la finition du chantier et à la propreté des lieux préalablement à l'établissement des factures correspondantes ; que, par suite, la cour administrative d'appel de Marseille n'a pas commis d'erreur de droit en jugeant qu'il appartenait à la société Appia 13, aux droits desquels est venue la société Eiffage travaux publics Méditerranée, si elle entendait obtenir le paiement de ces travaux supplémentaires, qu'elle avait effectués sans ordre de service, de transmettre les factures correspondantes au département en application de l'article 3-3.6.1 du cahier des clauses administratives particulières, sans attendre l'émission d'un nouveau bordereau de prix unitaires ;<br/>
<br/>
              5. Considérant, en deuxième lieu, que chaque commande d'un marché de travaux à bons de commande donne lieu à des prestations propres pouvant faire l'objet d'une réception et d'un règlement dès leur réalisation ; que, par suite, sauf à ce que le contrat renvoie le règlement définitif de l'ensemble des commandes au terme du marché, chaque commande de travaux peut donner lieu à un règlement définitif qui ne saurait donc être regardé comme un règlement partiel définitif interdit par le deuxième alinéa de l'article 92 du code des marchés publics aux termes duquel :  " Les marchés de travaux ne donnent pas lieu à des règlements partiels définitifs " ; qu'ainsi l'article 3-3.6.3 précité du cahier des clauses particulières du marché pouvait, sans contrevenir à cette disposition, prévoir, dans le cadre d'un marché de travaux à bons de commande, que soit considéré comme définitif le paiement de l'ensemble d'une commande ; que, par suite, la cour n'a pas commis d'erreur de droit en jugeant que la société requérante n'était pas fondée à demander la condamnation du département au paiement de travaux exécutés au titre d'une commande ayant fait l'objet d'un paiement définitif ; <br/>
<br/>
              6. Considérant, en troisième lieu, que les travaux supplémentaires réalisés sans ordre de service ne sont indemnisables que sur la base du contrat ; que, par suite, la cour, qui a suffisamment motivé son arrêt, n'a pas commis d'erreur de droit ni méconnu les règles d'indemnisation des travaux supplémentaires en jugeant que la société Eiffage travaux publics Méditerranée ne pouvait fonder sa demande sur un autre fondement que le contrat, ni en alléguant l'enrichissement sans cause du département, ni en invoquant sa responsabilité fautive à avoir tardé à établir un bordereau de prix complémentaire ;<br/>
<br/>
              7. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mis à la charge du département des Bouches-du-Rhône qui n'est pas, dans la présente instance, la partie perdante, le versement de la somme demandée par la société Eiffage travaux publics Méditerranée ; qu'en revanche il y a lieu de faire droit aux conclusions du département des Bouches-du-Rhône sur le fondement de ces dispositions et de mettre à la charge de la société Eiffage travaux publics Méditerranée le versement de la somme de 3 000 euros ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la société Eiffage travaux publics Méditerranée est rejeté.<br/>
Article 2 : La société Eiffage travaux publics Méditerranée versera une somme de 3 000 euros au département des Bouches-du-Rhône au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
Article 3 : La présente décision sera notifiée à la société Eiffage travaux publics Méditerranée et au département des Bouches-du-Rhône.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-05-02-01 MARCHÉS ET CONTRATS ADMINISTRATIFS. EXÉCUTION FINANCIÈRE DU CONTRAT. RÈGLEMENT DES MARCHÉS. DÉCOMPTE GÉNÉRAL ET DÉFINITIF. - MARCHÉS DE TRAVAUX À BONS DE COMMANDE - POSSIBILITÉ D'ÉTABLIR UN DÉCOMPTE DÉFINITIF À L'ISSUE DE CHAQUE COMMANDE - EXISTENCE.
</SCT>
<ANA ID="9A"> 39-05-02-01 Chaque commande d'un marché de travaux à bons de commande donne lieu à des prestations propres pouvant faire l'objet d'une réception et d'un règlement dès leur réalisation. Par suite, sauf à ce que le contrat renvoie le règlement définitif de l'ensemble des commandes au terme du marché, chaque commande de travaux peut donner lieu à un règlement définitif qui ne saurait donc être regardé comme un règlement partiel définitif interdit par le deuxième alinéa de l'article 92 du code des marchés publics.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
