<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038227966</ID>
<ANCIEN_ID>JG_L_2019_03_000000414930</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/22/79/CETATEXT000038227966.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 5ème chambres réunies, 13/03/2019, 414930</TITRE>
<DATE_DEC>2019-03-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>414930</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Laure Durand-Viel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Louis Dutheillet de Lamothe</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:414930.20190313</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 9 octobre 2017 et 23 avril 2018 au secrétariat du contentieux du Conseil d'Etat, l'association France Nature Environnement demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir, d'une part, le décret n° 2017-626 du 25 avril 2017 relatif aux procédures destinées à assurer l'information et la participation du public à l'élaboration de certaines décisions susceptibles d'avoir une incidence sur l'environnement et modifiant diverses dispositions relatives à l'évaluation environnementale de certains projets, plans et programmes et, d'autre part, la décision implicite du ministre de la transition écologique et solidaire du 21 août 2017 rejetant sa demande tendant à l'abrogation de l'article R. 121-2 du code de l'environnement en ce qu'il prévoit des seuils financiers pour déclencher la procédure de débat public ;<br/>
<br/>
              2°) à titre subsidiaire, de surseoir à statuer et de saisir la Cour de justice de l'Union européenne à titre préjudiciel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761 1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention sur l'accès à l'information, la participation du public au processus décisionnel et l'accès à la justice en matière d'environnement, faite à Aarhus le 25 juin 1998 ;<br/>
              - le traité sur le fonctionnement de l'Union européenne ;<br/>
              - la directive 2001/42/CE du Parlement européen et du Conseil du 27 juin 2001 relative à l'évaluation des incidences de certains plans et programmes sur l'environnement ;<br/>
              - la directive n° 2011/92/UE du Parlement européen et du Conseil du 13 décembre 2011 concernant l'évaluation des incidences de certains projets publics et privés sur l'environnement ;<br/>
              - le code de l'environnement ;<br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Laure Durand-Viel, auditeur,  <br/>
<br/>
              - les conclusions de M. Louis Dutheillet de Lamothe, rapporteur public.<br/>
<br/>
              Vu la note en délibéré, enregistrée le 17 février 2019, présentée par l'association France Nature Environnement ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1.	L'association France Nature Environnement demande l'annulation pour excès de pouvoir, d'une part, du décret du 25 avril 2017 relatif aux procédures destinées à assurer l'information et la participation du public à l'élaboration de certaines décisions susceptibles d'avoir une incidence sur l'environnement et modifiant diverses dispositions relatives à l'évaluation environnementale de certains projets, plans et programmes et, d'autre part, du rejet implicite du ministre de la transition écologique et solidaire né du silence gardé sur sa demande en date du 20 juin 2017 tendant à l'abrogation de l'article R. 121-2 du code de l'environnement en ce qu'il fait dépendre de seuils économiques ou financiers l'obligation de mettre en oeuvre une procédure de concertation préalable.<br/>
<br/>
              Sur la soumission des projets ayant une incidence sur l'environnement à une procédure de participation préalable du public :<br/>
<br/>
              2.	Aux termes du 2 de l'article 6 de la directive du 27 juin 2001 relative à l'évaluation des incidences de certains plans et programmes sur l'environnement : " Une possibilité réelle est donnée, à un stade précoce, aux autorités visées au paragraphe 3 et au public visé au paragraphe 4 d'exprimer, dans des délais suffisants, leur avis sur le projet de plan ou de programme et le rapport sur les incidences environnementales avant que le plan ou le programme ne soit adopté ou soumis à la procédure législative ". Aux termes de l'article 6 de la directive du 13 décembre 2011 concernant l'évaluation des incidences de certains projets publics et privés sur l'environnement, qui reprend sur ce point les dispositions de la directive du Conseil du 27 juin 1985, dans leur rédaction issue de la directive du 26 mai 2003 prévoyant la participation du public lors de l'élaboration de certains plans et programmes relatifs à l'environnement : " 2. À un stade précoce des procédures décisionnelles en matière d'environnement visées à l'article 2, paragraphe 2, et au plus tard dès que ces informations peuvent raisonnablement être fournies, les informations suivantes sont communiquées au public (...) : / a) la demande d'autorisation ; (...) / d) la nature des décisions possibles ou, lorsqu'il existe, le projet de décision ; (...) / 4. A un stade précoce de la procédure, le public concerné se voit donner des possibilités effectives de participer au processus décisionnel en matière d'environnement visé à l'article 2, paragraphe 2, et, à cet effet, il est habilité à adresser des observations et des avis, lorsque toutes les options sont envisageables, à l'autorité ou aux autorités compétentes avant que la décision concernant la demande d'autorisation ne soit prise ".<br/>
<br/>
              3.	En vertu des articles L. 121-1-A et L. 123-1-A du code de l'environnement, créés par l'ordonnance du 3 août 2016 portant réforme des procédures destinées à assurer l'information et la participation du public à l'élaboration de certaines décisions susceptibles d'avoir une incidence sur l'environnement, les dispositions du chapitre Ier du titre II du livre Ier de ce code s'appliquent à la participation du public préalable au dépôt de la demande d'autorisation d'un projet ou pendant la phase initiale d'élaboration d'un plan ou d'un programme, tandis que les dispositions du chapitre III, relatif à la participation du public aux décisions ayant une incidence sur l'environnement, définissent les modalités de participation du public après le dépôt de la demande d'autorisation des projets ou avant la phase finale de l'adoption ou de l'approbation des plans ou programmes, cette participation prenant en général la forme d'une enquête publique. <br/>
<br/>
              4.	D'une part, en vertu de l'article L. 123-1 du même code : " L'enquête publique a pour objet d'assurer l'information et la participation du public ainsi que la prise en compte des intérêts des tiers lors de l'élaboration des décisions susceptibles d'affecter l'environnement mentionnées à l'article L. 123-2. Les observations et propositions parvenues pendant le délai de l'enquête sont prises en considération par le maître d'ouvrage et par l'autorité compétente pour prendre la décision. " L'enquête publique permet de présenter au public concerné le projet soumis à autorisation ou le projet de plan ou programme, l'article R. 123-8 précisant la composition du dossier soumis à l'enquête, qui comporte notamment, suivant les cas, l'étude d'impact ou le rapport sur les incidences environnementales. Au vu des conclusions du commissaire enquêteur ou de la commission d'enquête, la personne responsable du projet, plan ou programme garde la faculté de lui apporter tous changements, le II de l'article L. 123-14 du même code lui imposant de demander à l'autorité organisatrice d'ouvrir une enquête complémentaire lorsque ces changements modifient l'économie générale du projet. La personne publique compétente pour délivrer l'autorisation demeure libre de l'accorder ou non, sous le contrôle du juge administratif.<br/>
<br/>
              5.	D'autre part, il résulte des articles L. 121-1, L. 121-8 et L. 121-15-1 du même code que la participation préalable du public a pour objet de débattre de l'opportunité, des objectifs et des caractéristiques principales du projet ou des objectifs et des principales orientations du plan ou programme, des enjeux socio-économiques qui s'y attachent, des impacts significatifs sur l'environnement et l'aménagement du territoire, ainsi que des solutions alternatives, y compris, pour un projet, son absence de mise en oeuvre. Il résulte de l'article L. 121-1-A du code de l'environnement, dans sa rédaction issue de l'ordonnance du 3 août 2016, pour l'application de laquelle a été pris le décret attaqué, que la participation préalable prend la forme d'un débat public ou d'une concertation préalable relevant de la compétence de la Commission nationale du débat public en vertu de l'article L. 121-8, ou d'une concertation préalable organisée soit par le maître d'ouvrage ou la personne publique responsable du plan ou programme en application du I de l'article L. 121-17, soit à la demande de l'autorité compétente pour approuver le plan ou programme ou autoriser le projet en application du II du même article, soit dans le cadre du droit d'initiative, prévu au III du même article, ouvert au public pour demander au représentant de l'Etat l'organisation d'une concertation préalable. La Commission nationale du débat public est saisie à titre systématique, en vertu du I de l'article L. 121-8 du même code, des projets qui " par leur nature, leurs caractéristiques techniques ou leur coût prévisionnel, tel qu'il peut être évalué lors de la phase d'élaboration, répondent à des critères ou excèdent des seuils fixés par décret en Conseil d'Etat " et à titre facultatif, en vertu du II du même article, des " projets appartenant aux catégories définies en application du I mais dont le coût prévisionnel est d'un montant inférieur au seuil fixé en application du I, et qui répondent à des critères techniques ou excèdent des seuils fixés par décret en Conseil d'Etat pour chaque nature de projet ", le maître d'ouvrage étant alors tenu, s'il décide de ne pas la saisir, d'organiser lui-même une concertation. Elle est également saisie, en vertu du IV du même article, des plans et programmes de niveau national faisant l'objet d'une évaluation environnementale. L'article R. 121-2 du code de l'environnement, dans sa rédaction issue du décret attaqué comme dans sa rédaction antérieure, dresse la liste des catégories d'opérations relatives aux projets dont cette commission est saisie à titre systématique en application du I de l'article L. 121-8 et de celles dont elle est saisie à titre facultatif en application du II de l'article L. 121-8, en retenant des seuils et critères correspondant, dans certains cas, au coût de l'opération. Par ailleurs, l'article L. 121-17-1 du même code prévoit l'ouverture systématique du droit d'initiative pour les projets, plans et programmes ne relevant pas du champ de compétence de cette commission, sous réserve, pour les projets, qu'ils dépassent un seuil exprimé en termes de montant de dépenses prévisionnelles ou de subventions publiques à l'investissement et fixé par décret en Conseil d'Etat. Le I de l'article R. 121-25 du code de l'environnement, dans sa rédaction issue du décret attaqué, fixe ces seuils de dépenses prévisionnelles et de subventions publiques à l'investissement. <br/>
<br/>
              6.	Il résulte clairement de ce qui a été dit au point 2 que les directives du 27 juin 2001 et du 13 décembre 2011 prévoient la mise en place d'une procédure de participation du public à un stade où le projet, plan ou programme est défini de façon suffisamment précise pour permettre au public concerné d'exprimer son avis au vu, notamment, du rapport sur les incidences environnementales ou, dans le cas d'un projet, de l'étude d'impact. Les dispositions du chapitre III du titre II du livre Ier du code de l'environnement rappelées au point 4 définissant les modalités de participation du public après le dépôt de la demande d'autorisation des projets ou après qu'un projet de plan ou programme a été élaboré ont pour objet, conformément à l'intention du législateur, d'assurer la transposition des objectifs de ces directives. En revanche, celles du chapitre Ier rappelées au point 5 traitent de la concertation préalable, organisée avant le dépôt de la demande d'autorisation d'un projet ou pendant la phase d'élaboration d'un projet de plan ou d'un programme à un stade où le projet, plan ou programme n'est pas encore assez défini pour faire l'objet d'une évaluation environnementale ou d'un rapport sur les incidences environnementales conformes aux exigences de ces directives. Dès lors, ces dispositions ne peuvent utilement être critiquées au regard des objectifs des directives du 27 juin 2001 et du 13 décembre 2011 mentionnés ci-dessus. Par suite, le moyen tiré de ce que les dispositions du décret attaqué et de l'ordonnance du 3 août 2016 méconnaîtraient ces objectifs, faute de prévoir l'obligation de mettre en oeuvre une procédure de concertation préalable, doit être écarté comme inopérant, de même que le moyen tiré de ce que ce décret ne soumet pas à concertation préalable obligatoire certaines procédures relatives à l'évolution de documents d'urbanisme. Enfin, les stipulations de la convention d'Aarhus énoncées au 4 de son article 6, qui prévoient que " chaque Partie prend des dispositions pour que la participation du public commence au début de la procédure, c'est-à-dire lorsque toutes les options et solutions sont encore possibles et que le public peut exercer une réelle influence. ", ne créent pas de droits dont les particuliers pourraient directement se prévaloir. Par suite, le moyen tiré de leur méconnaissance doit, en tout état de cause, être écarté. <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Sur le cumul des fonctions de garant et de commissaire enquêteur : <br/>
<br/>
              7.	Le deuxième alinéa de l'article L. 123-4 du code de l'environnement prévoit : " (...) Dans le cas où une concertation préalable s'est tenue sous l'égide d'un garant conformément aux articles L. 121-16 à L. 121-21, le président du tribunal administratif peut désigner ce garant en qualité de commissaire enquêteur si ce dernier est inscrit sur l'une des listes d'aptitude de commissaire enquêteur. (...) ". Il résulte de ces dispositions que la possibilité, pour le garant désigné pour établir le bilan de la concertation préalable, d'être nommé en qualité de commissaire enquêteur au stade de l'enquête publique sur la même opération résulte de la loi. Le moyen tiré de ce que l'article 4 du décret attaqué, en n'excluant pas une telle possibilité, méconnaîtrait le principe de l'indépendance du commissaire enquêteur, revient à contester devant le Conseil d'Etat la conformité de la loi elle-même à la Constitution. Il ne peut être utilement soulevé en dehors de la procédure prévue à l'article 61-1 de la Constitution.<br/>
<br/>
              Sur la désignation de l'autorité environnementale : <br/>
<br/>
              8.	Il résulte des dispositions de l'article 6 de la directive du 13 décembre 2011 que, si elles ne font pas obstacle à ce que l'autorité publique compétente pour autoriser un projet ou en assurer la maîtrise d'ouvrage soit en même temps chargée de la consultation en matière environnementale, elles imposent cependant que, dans une telle situation, une séparation fonctionnelle soit organisée au sein de cette autorité, de manière à ce qu'une entité administrative, interne à celle-ci, dispose d'une autonomie réelle, impliquant notamment qu'elle soit pourvue de moyens administratifs et humains qui lui sont propres, et soit ainsi en mesure de remplir la mission de consultation qui lui est confiée et de donner un avis objectif sur le projet concerné. Le 4° de l'article 3 du décret attaqué, qui modifie le III de l'article R. 122-6 du code de l'environnement, a cependant maintenu, au IV du même article, la désignation du préfet de la région sur le territoire de laquelle le projet de travaux, d'ouvrage ou d'aménagement doit être réalisé en qualité d'autorité compétente de l'Etat en matière d'environnement, pour tous les projets autres que ceux pour lesquels une autre autorité est désignée par les I, II et III du même article. Ni le décret attaqué, ni aucune autre disposition n'ont prévu de dispositif propre à garantir que, dans les cas où le préfet de région est compétent pour autoriser le projet, en particulier lorsqu'il agit en sa qualité de préfet du département où se trouve le chef-lieu de la région ou dans les cas où il est en charge de l'élaboration ou de la conduite du projet au niveau local, la compétence consultative en matière environnementale soit exercée par une entité interne disposant d'une autonomie réelle à son égard. Les dispositions du 4° de l'article 3 du décret attaqué ont ainsi méconnu les exigences découlant du paragraphe 1 de l'article 6 de la directive du 13 décembre 2011. Elles doivent donc être annulées en tant que l'article R. 122-6 du code de l'environnement qu'elles modifient conserve au préfet de région la compétence pour procéder à l'évaluation environnementale de certains projets.<br/>
<br/>
              9.	Il résulte de tout ce qui précède, sans qu'il soit besoin de saisir la Cour de justice de l'Union européenne d'une question préjudicielle, que l'association France Nature Environnement n'est fondée à demander l'annulation que du 4° de l'article 3 du décret du 25 avril 2017 en tant qu'il maintient, au IV de l'article R. 122-6 du code de l'environnement, la désignation du préfet de région en qualité d'autorité compétente de l'Etat en matière d'environnement.<br/>
<br/>
              10.	Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions de l'association France Nature Environnement au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le 4° de l'article 3 du décret du 25 avril 2017 est annulé en tant qu'il maintient, au IV de l'article R. 122-6 du code de l'environnement, la désignation du préfet de région en qualité d'autorité compétente de l'Etat en matière d'environnement.<br/>
<br/>
Article 2 : Le surplus des conclusions de la requête de l'association France Nature Environnement est rejeté.<br/>
<br/>
Article 3 : La présente décision sera notifiée à l'association France Nature Environnement, au ministre d'Etat, ministre de la transition écologique et solidaire et au ministre de la cohésion des territoires. <br/>
      Copie en sera adressée au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">15-05-10 COMMUNAUTÉS EUROPÉENNES ET UNION EUROPÉENNE. RÈGLES APPLICABLES. ENVIRONNEMENT. - DIRECTIVE 2001/42/CE DU 27 JUIN 2001 - DIRECTIVE 2011/92/UE DU 13 DÉCEMBRE 2011 - INVOCABILITÉ CONTRE DES DISPOSITIONS RELATIVES À LA PROCÉDURE DE CONCERTATION PRÉALABLE - ABSENCE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">44-006 NATURE ET ENVIRONNEMENT. - DIRECTIVE 2001/42/CE DU 27 JUIN 2001 - DIRECTIVE 2011/92/UE DU 13 DÉCEMBRE 2011 - INVOCABILITÉ CONTRE DES DISPOSITIONS RELATIVES À LA PROCÉDURE DE CONCERTATION PRÉALABLE - ABSENCE [RJ1].
</SCT>
<ANA ID="9A"> 15-05-10 Les directives 2001/42/CE du 27 juin 2001 et 2011/92/UE du 13 décembre 2011 prévoient la mise en place d'une procédure de participation du public à un stade où le projet, plan ou programme est défini de façon suffisamment précise pour permettre au public concerné d'exprimer son avis au vu, notamment, du rapport sur les incidences environnementales ou, dans le cas d'un projet, de l'étude d'impact. Les dispositions du chapitre III du titre II du livre Ier du code de l'environnement définissant les modalités de participation du public après le dépôt de la demande d'autorisation des projets ou après qu'un projet de plan ou programme a été élaboré ont pour objet, conformément à l'intention du législateur, d'assurer la transposition des objectifs de ces directives.... ...En revanche, celles du chapitre Ier traitent de la concertation préalable, organisée avant le dépôt de la demande d'autorisation d'un projet ou pendant la phase d'élaboration d'un projet de plan ou d'un programme à un stade où le projet, plan ou programme n'est pas encore assez défini pour faire l'objet d'une évaluation environnementale ou d'un rapport sur les incidences environnementales conformes aux exigences de ces directives. Dès lors, elles ne peuvent utilement être critiquées au regard des objectifs des directives du 27 juin 2001 et du 13 décembre 2011.</ANA>
<ANA ID="9B"> 44-006 Les directives 2001/42/CE du 27 juin 2001 et 2011/92/UE du 13 décembre 2011 prévoient la mise en place d'une procédure de participation du public à un stade où le projet, plan ou programme est défini de façon suffisamment précise pour permettre au public concerné d'exprimer son avis au vu, notamment, du rapport sur les incidences environnementales ou, dans le cas d'un projet, de l'étude d'impact. Les dispositions du chapitre III du titre II du livre Ier du code de l'environnement définissant les modalités de participation du public après le dépôt de la demande d'autorisation des projets ou après qu'un projet de plan ou programme a été élaboré ont pour objet, conformément à l'intention du législateur, d'assurer la transposition des objectifs de ces directives.... ...En revanche, celles du chapitre Ier traitent de la concertation préalable, organisée avant le dépôt de la demande d'autorisation d'un projet ou pendant la phase d'élaboration d'un projet de plan ou d'un programme à un stade où le projet, plan ou programme n'est pas encore assez défini pour faire l'objet d'une évaluation environnementale ou d'un rapport sur les incidences environnementales conformes aux exigences de ces directives. Dès lors, elles ne peuvent utilement être critiquées au regard des objectifs des directives du 27 juin 2001 et du 13 décembre 2011.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 9 décembre 2011, Réseau sortir du nucléaire, n° 324294, T. pp. 830-946-1032-1033.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
