<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042143108</ID>
<ANCIEN_ID>JG_L_2020_07_000000431299</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/14/31/CETATEXT000042143108.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 6ème chambres réunies, 22/07/2020, 431299</TITRE>
<DATE_DEC>2020-07-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>431299</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 6ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ZRIBI, TEXIER</AVOCATS>
<RAPPORTEUR>M. François Charmont</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:431299.20200722</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B... A... a demandé au tribunal administratif de Strasbourg d'annuler pour excès de pouvoir la décision du 31 juillet 2018 par laquelle la préfète de la Loire-Atlantique a rejeté sa demande d'échange de permis de conduite afghan contre un permis de conduire français. Par un jugement n° 1806135 du 8 février 2019, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 3 juin et 3 septembre 2019 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 2 700 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention de Genève du 28 juillet 1951 relative au statut des réfugiés ;<br/>
              - le code de la route ; <br/>
              - la loi n° 91-647 du 10 juillet 1991 ; <br/>
              - l'arrêté du 12 janvier 2012 fixant les conditions de reconnaissance et d'échange des permis de conduire délivrés par les Etats n'appartenant ni à l'Union européenne, ni à l'Espace économique européen ; <br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 modifiée ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. François Charmont, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Zribi et Texier, avocat de M. A....<br/>
<br/>
              Vu la note en délibéré, enregistrée le 8 juillet 2020, présentée par M. A... ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. A..., ressortissant afghan ayant la qualité de réfugié, a sollicité l'échange de son permis de conduire afghan contre un permis français. Il se pourvoit en cassation contre le jugement du 8 février 2019 par lequel le tribunal administratif de Strasbourg a rejeté sa demande d'annulation de la décision du 31 juillet 2018 par laquelle la préfète de la Loire-Atlantique lui a refusé l'échange sollicité.<br/>
<br/>
              Sur le droit applicable :<br/>
<br/>
              2. L'article R. 222-3 du code de la route dispose que : " Tout permis de conduire national, en cours de validité, délivré par un Etat ni membre de l'Union européenne, ni partie à l'accord sur l'Espace économique européen, peut être reconnu en France jusqu'à l'expiration d'un délai d'un an après l'acquisition de la résidence normale de son titulaire. (...) Les conditions de cette reconnaissance et de cet échange sont définies par arrêté du ministre chargé de la sécurité routière, après avis du ministre de la justice et du ministre chargé des affaires étrangères (...) ". Pour l'application de ces dispositions, l'article 7 de l'arrêté du 12 janvier 2012 fixant les conditions de reconnaissance et d'échange des permis de conduire délivrés par les Etats n'appartenant ni à l'Union européenne, ni à l'Espace économique européen dispose, dans sa rédaction applicable au litige : " A. - Avant tout échange, l'autorité administrative compétente s'assure de l'authenticité du titre de conduite et, en cas de doute, de la validité des droits. / B. - Pour vérifier l'authenticité du titre de conduite, l'autorité administrative compétente sollicite, le cas échéant, l'aide d'un service spécialisé dans la détection de la fraude documentaire. / C. - Si l'authenticité du titre de conduite est établie, celui-ci peut être échangé sous réserve de satisfaire aux autres conditions. / D. - Néanmoins, quand bien même l'authenticité du titre de conduite est établie, l'autorité administrative compétente peut, avant de se prononcer sur la demande d'échange, en cas de doute selon les informations dont elle dispose, consulter l'autorité étrangère ayant délivré le titre afin de s'assurer des droits de conduite de son titulaire. (...) / E. - Si le caractère frauduleux du titre est établi, l'échange n'a pas lieu et le titre est retiré par l'autorité administrative compétente, qui saisit le procureur de la République en le lui transmettant ".<br/>
<br/>
              3. Lorsque la personne qui demande, sur le fondement des dispositions citées ci-dessus, l'échange d'un permis de conduire délivré par un Etat ni membre de l'Union européenne, ni partie à l'accord sur l'Espace économique européen, a la qualité de réfugié en raison des craintes de persécution de la part des autorités de cet Etat, les dispositions citées ci-dessus doivent être appliquées en tenant compte des stipulations de l'article 25 de la convention de Genève du 28 juillet 1951 relative au statut des réfugiés, aux termes desquelles : " 1. Lorsque l'exercice d'un droit par un réfugié nécessiterait normalement le concours d'autorités étrangères auxquelles il ne peut recourir, les Etats contractants sur le territoire desquels il réside veilleront à ce que ce concours lui soit fourni, soit par leurs propres autorités, soit par une autorité internationale. / 2. La ou les autorités visées au paragraphe 1er délivreront ou feront délivrer sous leur contrôle, aux réfugiés, les documents ou les certificats qui normalement seraient délivrés à un étranger par ses autorités nationales ou par leur intermédiaire. / 3. Les documents ou certificats ainsi délivrés remplaceront les actes officiels délivrés à des étrangers par leurs autorités nationales ou par leur intermédiaire, et feront foi jusqu'à preuve du contraire (...) ". Il en résulte que trois cas peuvent alors se présenter.<br/>
<br/>
              4. En premier lieu, si, après avoir le cas échéant saisi le service spécialisé dans la détection de la fraude documentaire placé auprès du ministre de l'intérieur aux fins qu'il se prononce sur l'authenticité du titre de conduite étranger, l'autorité compétente estime que cette authenticité est établie sans que subsiste, par ailleurs, de doute sur la validité des droits à conduire de son titulaire, l'échange ne peut être légalement refusé, dès lors que ses autres conditions sont satisfaites.<br/>
<br/>
              5. En deuxième lieu, si, après avoir le cas échéant saisi le service spécialisé dans la détection de la fraude documentaire, l'autorité compétente estime que le caractère falsifié du titre de conduite est établi, elle rejette la demande d'échange de permis de conduire, sans être tenue de mettre préalablement en mesure l'intéressé, alors même qu'il a le statut de réfugié, de lui soumettre des éléments de nature à établir l'authenticité de son titre ou la validité de ses droits à conduire. <br/>
<br/>
              6. Enfin  si, après avoir saisi le service spécialisé dans la détection de la fraude documentaire déjà mentionné, l'autorité compétente conserve un doute sur l'authenticité du titre de conduite ou si elle conserve un doute sur la validité des droits à conduire du demandeur, il lui appartient, faute de pouvoir se fonder sur une consultation des autorités du pays à l'égard duquel le demandeur a obtenu le statut de réfugié, de mettre ce dernier en mesure de lui soumettre tous éléments de nature à faire regarder l'authenticité de son titre ou la validité de ses droits à conduire comme suffisamment établis et d'apprécier ces éléments en tenant compte de sa situation particulière. L'administration ne peut légalement refuser l'échange sans avoir invité le demandeur à fournir de tels éléments. Si, à l'issue de cette procédure, le doute persiste, l'échange ne peut légalement avoir lieu.<br/>
<br/>
              Sur le bien-fondé du jugement attaqué :<br/>
<br/>
              7. Il ressort des pièces du dossier soumis au juge du fond que, pour rejeter la demande d'échange de permis de conduire présentée par M. A..., la préfète de la Loire-Atlantique s'est fondée sur ce que le caractère falsifié du titre de conduite afghan produit par l'intéressé était établi, compte tenu de la comparaison effectuée entre ce document et les spécimens détenus dans la base documentaire du service spécialisé dans la fraude documentaire du ministère de l'intérieur.<br/>
<br/>
              8. Par suite, le tribunal administratif a pu, sans erreur de droit, juger que la préfète de la Loire-Atlantique n'était pas tenue d'inviter préalablement l'intéressé, alors même qu'il a la qualité de réfugié, à fournir des éléments permettant de tenir pour suffisamment établi que le document qu'il présentait l'habilitait à conduire dans son pays d'origine.<br/>
<br/>
              9. Il résulte de tout ce qui précède que M. A... n'est pas fondé à demander l'annulation du jugement qu'il attaque. Son pourvoi doit, par suite, être rejeté, y compris, par voie de conséquence, ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
		Article 1er : Le pourvoi de M. A... est rejeté. <br/>
<br/>
Article 2 : La présente décision sera notifiée à M. B... A... et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-04-01 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. TRAITÉS ET DROIT DÉRIVÉ. - CONVENTION DE GENÈVE DU 28 JUILLET 1951 RELATIVE AU STATUT DES RÉFUGIÉS - RECONNAISSANCE DE LA QUALITÉ DE RÉFUGIÉ - CONSÉQUENCE - ECHANGE D'UN PERMIS DE CONDUIRE ÉTRANGER CONTRE UN PERMIS DE CONDUIRE FRANÇAIS - CAS POSSIBLES - 1) AUTHENTICITÉ DU PERMIS ÉTRANGER ÉTABLIE ET ABSENCE DE DOUTE SUR LES DROITS À CONDUIRE - DÉLIVRANCE D'UN PERMIS FRANÇAIS - 2) FALSIFICATION DU PERMIS ÉTRANGER ÉTABLIE - REFUS DE DÉLIVRANCE SANS CONTRADICTOIRE - 3) DOUTES SUR L'AUTHENTICITÉ - MISE EN &#140;UVRE DU CONTRADICTOIRE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">095-05 - ECHANGE DE PERMIS DE CONDUIRE - CAS POSSIBLES - 1) AUTHENTICITÉ DU PERMIS ÉTRANGER ÉTABLIE ET ABSENCE DE DOUTE SUR LES DROITS À CONDUIRE - DÉLIVRANCE D'UN PERMIS FRANÇAIS - 2) FALSIFICATION DU PERMIS ÉTRANGER ÉTABLIE - REFUS DE DÉLIVRANCE SANS CONTRADICTOIRE - 3) DOUTES SUR L'AUTHENTICITÉ - MISE EN &#140;UVRE DU CONTRADICTOIRE [RJ1].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">49-04-01-04-01 POLICE. POLICE GÉNÉRALE. CIRCULATION ET STATIONNEMENT. PERMIS DE CONDUIRE. DÉLIVRANCE. - ECHANGE D'UN PERMIS DE CONDUIRE ÉTRANGER CONTRE UN PERMIS FRANÇAIS - ETRANGER S'ÉTANT VU RECONNAÎTRE LA QUALITÉ DE RÉFUGIÉ - CAS POSSIBLES - 1) AUTHENTICITÉ DU PERMIS ÉTRANGER ÉTABLIE ET ABSENCE DE DOUTE SUR LES DROITS À CONDUIRE - DÉLIVRANCE D'UN PERMIS FRANÇAIS - 2) FALSIFICATION DU PERMIS ÉTRANGER ÉTABLIE - REFUS DE DÉLIVRANCE SANS CONTRADICTOIRE - 3) DOUTES SUR L'AUTHENTICITÉ - MISE EN &#140;UVRE DU CONTRADICTOIRE [RJ1].
</SCT>
<ANA ID="9A"> 01-04-01 Lorsque la personne qui demande, sur le fondement de l'article R. 222-3 du code de la route et de l'article 7 de l'arrêté du 12 janvier 2012 fixant les conditions de reconnaissance et d'échange des permis de conduire, l'échange d'un permis de conduire délivré par un Etat ni membre de l'Union européenne, ni partie à l'accord sur l'Espace économique européen, a la qualité de réfugié en raison des craintes de persécution de la part des autorités de cet Etat, ces dispositions doivent être appliquées en tenant compte des stipulations de l'article 25 de la convention de Genève du 28 juillet 1951 relative au statut des réfugiés.,,,1) En premier lieu, si, après avoir le cas échéant saisi le service spécialisé dans la détection de la fraude documentaire placé auprès du ministre de l'intérieur aux fins qu'il se prononce sur l'authenticité du titre de conduite étranger, l'autorité compétente estime que cette authenticité est établie sans que subsiste, par ailleurs, de doute sur la validité des droits à conduire de son titulaire, l'échange ne peut être légalement refusé, dès lors que ses autres conditions sont satisfaites.,,,2) En deuxième lieu, si, après avoir le cas échéant saisi le service spécialisé dans la détection de la fraude documentaire, l'autorité compétente estime que le caractère falsifié du titre de conduite est établi, elle rejette la demande d'échange de permis de conduire, sans être tenue de mettre préalablement en mesure l'intéressé, alors même qu'il a le statut de réfugié, de lui soumettre des éléments de nature à établir l'authenticité de son titre ou la validité de ses droits à conduire.,,,3) Enfin si, après avoir saisi le service spécialisé dans la détection de la fraude documentaire déjà mentionné, l'autorité compétente conserve un doute sur l'authenticité du titre de conduite ou si elle conserve un doute sur la validité des droits à conduire du demandeur, il lui appartient, faute de pouvoir se fonder sur une consultation des autorités du pays à l'égard duquel le demandeur a obtenu le statut de réfugié, de mettre ce dernier en mesure de lui soumettre tous éléments de nature à faire regarder l'authenticité de son titre ou la validité de ses droits à conduire comme suffisamment établis et d'apprécier ces éléments en tenant compte de sa situation particulière. L'administration ne peut légalement refuser l'échange sans avoir invité le demandeur à fournir de tels éléments. Si, à l'issue de cette procédure, le doute persiste, l'échange ne peut légalement avoir lieu.</ANA>
<ANA ID="9B"> 095-05 Lorsque la personne qui demande, sur le fondement de l'article R. 222-3 du code de la route et de l'article 7 de l'arrêté du 12 janvier 2012 fixant les conditions de reconnaissance et d'échange des permis de conduire, l'échange d'un permis de conduire délivré par un Etat ni membre de l'Union européenne, ni partie à l'accord sur l'Espace économique européen, a la qualité de réfugié en raison des craintes de persécution de la part des autorités de cet Etat, ces dispositions doivent être appliquées en tenant compte des stipulations de l'article 25 de la convention de Genève du 28 juillet 1951 relative au statut des réfugiés.,,,1) En premier lieu, si, après avoir le cas échéant saisi le service spécialisé dans la détection de la fraude documentaire placé auprès du ministre de l'intérieur aux fins qu'il se prononce sur l'authenticité du titre de conduite étranger, l'autorité compétente estime que cette authenticité est établie sans que subsiste, par ailleurs, de doute sur la validité des droits à conduire de son titulaire, l'échange ne peut être légalement refusé, dès lors que ses autres conditions sont satisfaites.,,,2) En deuxième lieu, si, après avoir le cas échéant saisi le service spécialisé dans la détection de la fraude documentaire, l'autorité compétente estime que le caractère falsifié du titre de conduite est établi, elle rejette la demande d'échange de permis de conduire, sans être tenue de mettre préalablement en mesure l'intéressé, alors même qu'il a le statut de réfugié, de lui soumettre des éléments de nature à établir l'authenticité de son titre ou la validité de ses droits à conduire.,,,3) Enfin si, après avoir saisi le service spécialisé dans la détection de la fraude documentaire déjà mentionné, l'autorité compétente conserve un doute sur l'authenticité du titre de conduite ou si elle conserve un doute sur la validité des droits à conduire du demandeur, il lui appartient, faute de pouvoir se fonder sur une consultation des autorités du pays à l'égard duquel le demandeur a obtenu le statut de réfugié, de mettre ce dernier en mesure de lui soumettre tous éléments de nature à faire regarder l'authenticité de son titre ou la validité de ses droits à conduire comme suffisamment établis et d'apprécier ces éléments en tenant compte de sa situation particulière. L'administration ne peut légalement refuser l'échange sans avoir invité le demandeur à fournir de tels éléments. Si, à l'issue de cette procédure, le doute persiste, l'échange ne peut légalement avoir lieu.</ANA>
<ANA ID="9C"> 49-04-01-04-01 Lorsque la personne qui demande, sur le fondement de l'article R. 222-3 du code de la route et de l'article 7 de l'arrêté du 12 janvier 2012 fixant les conditions de reconnaissance et d'échange des permis de conduire, l'échange d'un permis de conduire délivré par un Etat ni membre de l'Union européenne, ni partie à l'accord sur l'Espace économique européen, a la qualité de réfugié en raison des craintes de persécution de la part des autorités de cet Etat, ces dispositions doivent être appliquées en tenant compte des stipulations de l'article 25 de la convention de Genève du 28 juillet 1951 relative au statut des réfugiés.,,,1) En premier lieu, si, après avoir le cas échéant saisi le service spécialisé dans la détection de la fraude documentaire placé auprès du ministre de l'intérieur aux fins qu'il se prononce sur l'authenticité du titre de conduite étranger, l'autorité compétente estime que cette authenticité est établie sans que subsiste, par ailleurs, de doute sur la validité des droits à conduire de son titulaire, l'échange ne peut être légalement refusé, dès lors que ses autres conditions sont satisfaites.,,,2) En deuxième lieu, si, après avoir le cas échéant saisi le service spécialisé dans la détection de la fraude documentaire, l'autorité compétente estime que le caractère falsifié du titre de conduite est établi, elle rejette la demande d'échange de permis de conduire, sans être tenue de mettre préalablement en mesure l'intéressé, alors même qu'il a le statut de réfugié, de lui soumettre des éléments de nature à établir l'authenticité de son titre ou la validité de ses droits à conduire.,,,3) Enfin si, après avoir saisi le service spécialisé dans la détection de la fraude documentaire déjà mentionné, l'autorité compétente conserve un doute sur l'authenticité du titre de conduite ou si elle conserve un doute sur la validité des droits à conduire du demandeur, il lui appartient, faute de pouvoir se fonder sur une consultation des autorités du pays à l'égard duquel le demandeur a obtenu le statut de réfugié, de mettre ce dernier en mesure de lui soumettre tous éléments de nature à faire regarder l'authenticité de son titre ou la validité de ses droits à conduire comme suffisamment établis et d'apprécier ces éléments en tenant compte de sa situation particulière. L'administration ne peut légalement refuser l'échange sans avoir invité le demandeur à fournir de tels éléments. Si, à l'issue de cette procédure, le doute persiste, l'échange ne peut légalement avoir lieu.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 20 février 2019, M.,, n° 413625, T. pp. 537-582-874.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
