<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030559651</ID>
<ANCIEN_ID>JG_L_2015_05_000000366933</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/55/96/CETATEXT000030559651.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère / 6ème SSR, 07/05/2015, 366933</TITRE>
<DATE_DEC>2015-05-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>366933</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère / 6ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>M. Philippe Combettes</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:366933.20150507</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
Procédure contentieuse antérieure<br/>
<br/>
              La clinique ophtalmologique Thiers a demandé au tribunal administratif de Bordeaux, d'une part, d'annuler la sanction financière d'un montant de 44 872 euros prise par la délibération du 9 septembre 2008 de la commission exécutive de l'agence régionale de l'hospitalisation de l'Aquitaine et notifiée le 31 octobre 2008 et, d'autre part, d'enjoindre à l'administration de lui restituer les sommes en litige, assorties des intérêts au taux légal. Par un jugement n° 0806044 du 11 mai 2011, le tribunal administratif de Bordeaux a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 11BX01836 du 15 janvier 2013, la cour administrative d'appel de Bordeaux a, à la demande de la clinique ophtalmologique Thiers, annulé ce jugement du 11 mai 2011 et la décision notifiée le 31 octobre 2008 et enjoint à l'Etat et à l'agence régionale de santé d'Aquitaine de restituer à la clinique les sommes en litige, assorties des intérêts au taux légal.<br/>
<br/>
Procédure devant le Conseil d'Etat<br/>
<br/>
              Par un pourvoi, un nouveau mémoire et un mémoire en réplique, enregistrés les 18 mars 2013, 9 juillet 2013 et 19 mars 2014 au secrétariat du contentieux du Conseil d'Etat, le ministre des affaires sociales et de la santé demande au Conseil d'Etat d'annuler cet arrêt de la cour administrative d'appel de Bordeaux du 15 janvier 2013.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la sécurité sociale ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Philippe Combettes, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la clinique ophtalmologique Thiers ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par un courrier du 31 octobre 2008, l'agence régionale de l'hospitalisation d'Aquitaine a notifié à la clinique ophtalmologique Thiers une sanction financière de 44 872 euros, prise sur le fondement de l'article L. 162-22-18 du code de la sécurité sociale, à la suite d'un contrôle interne de la tarification de son activité diligenté par ses services ; que, par un jugement du 11 mai 2011, le tribunal administratif de Bordeaux a rejeté la demande de la clinique ophtalmologique Thiers tendant à l'annulation de cette décision  ; que, par un arrêt du 15 janvier 2013 contre lequel le ministre des affaires sociales et de la santé se pourvoit en cassation, la cour administrative d'appel de Bordeaux a annulé le jugement du tribunal administratif de Bordeaux et la décision notifiée le 31 octobre 2008, au motif que celle-ci avait été prise à l'issue d'une procédure irrégulière et était insuffisamment motivée ; <br/>
<br/>
              2. Considérant que l'article L. 162-22-18 du code de la sécurité sociale, dans sa rédaction applicable à la date de la décision attaquée, dispose que : " Les établissements de santé sont passibles, après qu'ils ont été mis en demeure de présenter leurs observations, d'une sanction financière en cas de manquement aux règles de facturation fixées en application des dispositions de l'article L. 162-22-6, d'erreur de codage ou d'absence de réalisation d'une prestation facturée.  / Cette sanction est prise par la commission exécutive mentionnée à l'article L. 6115-2 du code de la santé publique, à la suite d'un contrôle réalisé sur pièces et sur place par les médecins inspecteurs de santé publique ou les praticiens-conseils des organismes d'assurance maladie mentionnés au sixième alinéa de l'article L. 1112-1 du même code en application du programme de contrôle régional établi par ladite commission. Elle est notifiée à l'établissement. / (...) Les modalités d'application du présent article sont définies par décret en Conseil d'Etat " ; qu'en vertu des dispositions de l'article R. 162-42-10 du code de la sécurité sociale, dans leur rédaction applicable à la décision attaquée : " L'agence régionale de l'hospitalisation informe l'établissement de santé de l'engagement du contrôle réalisé en application de l'article L. 162-22-18 par tout moyen permettant de déterminer la date de réception. Elle précise les activités, prestations ou ensemble de séjours ainsi que la période sur lesquels porte le contrôle, le nom et la qualité des personnes chargées du contrôle et la date à laquelle il commence. / Le contrôle porte sur tout ou partie de l'activité de l'établissement et peut être réalisé sur la base d'un échantillon tiré au sort. / L'établissement est tenu de fournir ou de tenir à disposition des personnes chargées du contrôle l'ensemble des documents qu'elles demandent. Les personnes chargées du contrôle exercent leur mission dans les conditions prévues à l'article R. 166-1. / A l'issue du contrôle, les personnes chargées du contrôle communiquent à l'établissement de santé par tout moyen permettant de déterminer la date de réception, un rapport qu'elles datent et signent mentionnant la période, l'objet, la durée et les résultats du contrôle et, le cas échéant, la méconnaissance par l'établissement de santé des obligations définies à l'alinéa précédent. / A compter de la réception de ce rapport, l'établissement dispose d'un délai de quinze jours pour faire connaître, le cas échéant, ses observations. A l'expiration de ce délai, les personnes chargées du contrôle transmettent à l'unité de coordination le rapport de contrôle accompagné, s'il y a lieu, de la réponse de l'établissement (...) " ; qu'aux termes de l'article R. 162-42-13 du même code, dans sa rédaction applicable à la décision en litige : " La sanction envisagée et les motifs le justifiant sont notifiés à l'établissement par tout moyen permettant de déterminer la date de réception. L'établissement dispose d'un délai d'un mois pour présenter ses observations. Au terme de ce délai, la commission exécutive prononce la sanction, la notifie à l'établissement par tout moyen permettant de déterminer la date de réception et lui indique le délai et les modalités de paiement des sommes en cause (...) " ; <br/>
<br/>
<br/>
              3. Considérant, en premier lieu, qu'il résulte des dispositions de l'article R. 162-42-10 du code de la sécurité sociale citées ci-dessus qu'à l'issue du contrôle, l'établissement de santé doit avoir communication du rapport mentionnant les résultats du contrôle et pouvoir faire connaître, le cas échéant, ses observations ; qu'à cette fin, le rapport doit permettre à l'établissement d'identifier les actes pour lesquels il lui est reproché d'avoir méconnu une règle de facturation, fait une erreur de codage ou facturé une prestation non réalisée, ainsi que la règle méconnue, l'erreur relevée ou la prestation non réalisée ;<br/>
<br/>
              4. Considérant que le 10° du I de l'article 5 de l'arrêté du 5 mars 2006 relatif à la classification et à la prise en charge des prestations d'hospitalisation prévoit que lorsque le patient est pris en charge moins d'une journée, à l'exception des cas où il est pris en charge dans un service d'urgence et des cas où la prestation nécessite l'utilisation du secteur opératoire du fait de la nature de l'acte, un " groupe homogène de séjour " ne peut être facturé " que dans les cas où sont réalisés des actes qui nécessitent : / - une admission dans une structure d'hospitalisation individualisée (...) disposant de moyens en locaux, en matériel et en personnel, et notamment des équipements adaptés pour répondre aux risques potentiels des actes réalisés ; / - un environnement respectant les conditions de fonctionnement relatives à la pratique de l'anesthésie ou la prise en charge par une équipe paramédicale et médicale dont la coordination est assurée par un médecin ; / - l'utilisation d'un lit ou d'une place pour une durée nécessaire à la réalisation de l'acte ou justifiée par l'état de santé du patient " ; qu'en jugeant que la mention, par le rapport de contrôle transmis à la clinique ophtalmologique Thiers le 19 juillet 2007, de ce que les conditions de facturation du 10° de l'article 5 de l'arrêté du 5 mars 2006 n'étaient pas remplies, ne satisfaisait pas aux exigences de l'article R. 162-42-10 du code de la sécurité sociale, alors qu'elle lui permettait de connaître la règle de facturation dont la méconnaissance lui était reprochée et de faire connaître utilement ses observations sur ce point, la cour a commis une erreur de droit ;<br/>
<br/>
              5. Considérant, en second lieu, qu'il résulte des dispositions de l'article R. 162-42-13 du code de la sécurité sociale citées au point 2 que la sanction envisagée et les motifs la justifiant doivent être notifiés à l'établissement de santé pour que celui-ci soit à même de présenter ses observations ; que, toutefois, en jugeant que les droits de la défense avaient été méconnus au motif que la lettre du 9 juillet 2008 par laquelle le directeur de l'agence régionale de l'hospitalisation notifiait à la clinique la sanction envisagée à son encontre se bornait à rappeler, en annexant la liste des dossiers contrôlés, que le rapport de contrôle faisait apparaître des manquements aux règles de facturation fixées en application de l'article L. 162-22-6 du code de la sécurité sociale, sans rechercher si, compte tenu du déroulement de l'ensemble de la procédure, notamment des échanges intervenus au moment du contrôle et entre la transmission du rapport de contrôle et la notification de la sanction envisagée, l'établissement avait été mis à même de se défendre utilement, la cour a commis une erreur  de droit ;<br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que le ministre des affaires sociales et de la santé est fondé à demander l'annulation de l'arrêt qu'il attaque ; <br/>
<br/>
              7. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Bordeaux du 15 janvier 2013 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Bordeaux. <br/>
Article 3 : Les conclusions de la clinique ophtalmologique Thiers présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à la ministre des affaires sociales, de la santé et des droits des femmes et à la clinique ophtalmologique Thiers.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">61-06-02-01 SANTÉ PUBLIQUE. ÉTABLISSEMENTS PUBLICS DE SANTÉ. FONCTIONNEMENT. FINANCEMENT. - SANCTIONS FINANCIÈRES EN CAS DE MANQUEMENT AUX RÈGLES DE FACTURATION, D'ERREUR DE CODAGE OU D'ABSENCE DE RÉALISATION D'UNE PRESTATION FACTURÉE (ART. L. 162-22-18 DU CSS) - PROCÉDURE - OBLIGATION DE NOTIFIER À L'ÉTABLISSEMENT LA SANCTION ENVISAGÉE ET LES MOTIFS LA JUSTIFIANT - APPRÉCIATION DU RESPECT DES DROITS DE LA DÉFENSE AU REGARD DU DÉROULEMENT DE L'ENSEMBLE DE LA PROCÉDURE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">61-07-02-04 SANTÉ PUBLIQUE. ÉTABLISSEMENTS PRIVÉS DE SANTÉ. PARTICIPATION DES ÉTABLISSEMENTS PRIVÉS AU SERVICE PUBLIC HOSPITALIER. FINANCEMENT. - SANCTIONS FINANCIÈRES EN CAS DE MANQUEMENT AUX RÈGLES DE FACTURATION, D'ERREUR DE CODAGE OU D'ABSENCE DE RÉALISATION D'UNE PRESTATION FACTURÉE (ART. L. 162-22-18 DU CSS) - PROCÉDURE - OBLIGATION DE NOTIFIER À L'ÉTABLISSEMENT LA SANCTION ENVISAGÉE ET LES MOTIFS LA JUSTIFIANT - APPRÉCIATION DU RESPECT DES DROITS DE LA DÉFENSE AU REGARD DU DÉROULEMENT DE L'ENSEMBLE DE LA PROCÉDURE.
</SCT>
<ANA ID="9A"> 61-06-02-01 Régime des sanctions financières dont sont passibles les établissements de santé en cas de manquement aux règles de facturation fixées en application des dispositions de l'article L. 162-22-6 du code de la sécurité sociale (CSS), d'erreur de codage ou d'absence de réalisation d'une prestation facturée (article L. 162-22-18 du CSS).,,,Il résulte des dispositions de l'article R. 162 42-13 du CSS que la sanction envisagée et les motifs la justifiant doivent être notifiés à l'établissement de santé pour que celui-ci soit à même de présenter ses observations.,,,Une cour administrative d'appel commet une erreur de droit en jugeant que les droits de la défense ont été méconnus au motif que la lettre par laquelle le directeur de l'agence régionale de l'hospitalisation notifiait à une clinique la sanction envisagée à son encontre se bornait à rappeler, en annexant la liste des dossiers contrôlés, que le rapport de contrôle faisait apparaître des manquements aux règles de facturation fixées en application de l'article L. 162-22-6 du CSS, sans rechercher si, compte tenu du déroulement de l'ensemble de la procédure, notamment des échanges intervenus au moment du contrôle et entre la transmission du rapport de contrôle et la notification de la sanction envisagée, l'établissement avait été mis à même de se défendre utilement.</ANA>
<ANA ID="9B"> 61-07-02-04 Régime des sanctions financières dont sont passibles les établissements de santé en cas de manquement aux règles de facturation fixées en application des dispositions de l'article L. 162-22-6 du code de la sécurité sociale (CSS), d'erreur de codage ou d'absence de réalisation d'une prestation facturée (article L. 162-22-18 du CSS).,,,Il résulte des dispositions de l'article R. 162 42-13 du CSS que la sanction envisagée et les motifs la justifiant doivent être notifiés à l'établissement de santé pour que celui-ci soit à même de présenter ses observations.,,,Une cour administrative d'appel commet une erreur de droit en jugeant que les droits de la défense ont été méconnus au motif que la lettre par laquelle le directeur de l'agence régionale de l'hospitalisation notifiait à une clinique la sanction envisagée à son encontre se bornait à rappeler, en annexant la liste des dossiers contrôlés, que le rapport de contrôle faisait apparaître des manquements aux règles de facturation fixées en application de l'article L. 162-22-6 du CSS, sans rechercher si, compte tenu du déroulement de l'ensemble de la procédure, notamment des échanges intervenus au moment du contrôle et entre la transmission du rapport de contrôle et la notification de la sanction envisagée, l'établissement avait été mis à même de se défendre utilement.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
