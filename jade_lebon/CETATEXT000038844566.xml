<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038844566</ID>
<ANCIEN_ID>JG_L_2019_06_000000410876</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/84/45/CETATEXT000038844566.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 5ème chambres réunies, 17/06/2019, 410876, Publié au recueil Lebon</TITRE>
<DATE_DEC>2019-06-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>410876</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 5ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LEDUC, VIGAND ; SCP BORE, SALVE DE BRUNETON, MEGRET</AVOCATS>
<RAPPORTEUR>Mme Airelle Niepce</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Louis Dutheillet de Lamothe</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:410876.20190617</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par un jugement n° 2015-0008 du 21 octobre 2015, la chambre territoriale des comptes de la Polynésie française, statuant sur les comptes de Mme A...B..., comptable de la commune de Papeete, l'a constituée débitrice envers cette commune de la somme de 667 116 772 francs CFP pour défaut de diligence dans le recouvrement de 20 590 titres de recettes pris en charge au cours des exercices 1981 à 2009.<br/>
<br/>
              Par un arrêt n° S 2017-0388 du 23 mars 2017, la Cour des comptes a infirmé le jugement de la chambre territoriale de la Polynésie française en tant qu'il l'a constituée débitrice de 19 185 titres de créance émis par la commune de Papeete entre 1981 et 2008, représentant un montant de 615 343 594 francs CFP ainsi que de 28 titre émis en 2009, représentant un montant de 4 193 986 francs CFP, et rejeté le surplus de la requête de MmeB....<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 24 mai et 24 août 2017 au secrétariat du contentieux du Conseil d'Etat, la commune de Papeete demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de MmeB..., une somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des collectivités territoriales ;<br/>
              - le code des juridictions financières ;<br/>
              - la loi n° 63-156 du 23 février 1963 ;<br/>
              - le décret n° 2008-228 du 5 mars 2008 ;<br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Airelle Niepce, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Louis Dutheillet de Lamothe, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la Scp Leduc, Vigand, avocat de la commune de Papeete et à la SCP Boré, Salve de Bruneton, Mégret, avocat de MmeB....<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1.	Il ressort des énonciations de l'arrêt attaqué et des pièces du dossier soumis aux juges du fond que MmeB..., comptable public, a pris ses fonctions en tant que responsable de la trésorerie des îles du Vent, des Australes et des Archipels, en Polynésie française, le 3 janvier 2011 et les a exercées jusqu'au 31 décembre 2013. Le 27 décembre 2011, elle a régulièrement formulé des réserves sur la gestion de ses prédécesseurs pour 253 040 titres de créance représentant un montant de 1 147 799 205 francs CFP, s'agissant de la commune de Papeete. A la suite d'un contrôle diligenté en février 2015, la chambre territoriale des comptes de Polynésie française a, par un jugement du 21 octobre 2015, notamment constitué Mme B...débitrice pour 20 590 titres de créance émis par cette commune entre 1981 et 2009, représentant un montant total de 667 116 772 francs CFP, portant en particulier sur des créances relatives à des taxes d'enlèvement des ordures ménagères dues par des particuliers ou par des petites et moyennes entreprises, pour des montants unitaires assez faibles. Sur appel de MmeB..., la Cour des comptes a, par un arrêt du 23 mars 2017, infirmé le jugement de la chambre territoriale des comptes en tant qu'il l'a constituée débitrice de 19 185 titres de créance émis par la commune de Papeete entre 1981 et 2008, représentant un montant de 615 343 594 francs CFP ainsi que de 28 titre émis en 2009, représentant un montant de 4 193 986 francs CFP. La commune de Papeete se pourvoit en cassation contre cet arrêt. Eu égard aux moyens soulevés, son pourvoi doit être regardé comme ne tendant à l'annulation de l'arrêt attaqué qu'en tant que celui-ci conduit à décharger Mme B...de 19 185 titres de créance émis par la commune de Papeete entre 1981 et 2008, pour un montant de 615 343 594 francs CFP.<br/>
<br/>
              2.	Aux termes du I de l'article 60 de la loi du 23 février 1963 de finances pour 1963, dans sa rédaction applicable en l'espèce : " I. - Outre la responsabilité attachée à leur qualité d'agent public, les comptables publics sont personnellement et pécuniairement responsables du recouvrement des recettes, du paiement des dépenses, de la garde et de la conservation des fonds et valeurs appartenant ou confiés aux différentes personnes morales de droit public dotées d'un comptable public, désignées ci-après par le terme d'organismes publics, du maniement des fonds et des mouvements de comptes de disponibilités, de la conservation des pièces justificatives des opérations et documents de comptabilité ainsi que de la tenue de la comptabilité du poste comptable qu'ils dirigent. / Les comptables publics sont personnellement et pécuniairement responsables des contrôles qu'ils sont tenus d'assurer en matière de recettes, de dépenses et de patrimoine dans les conditions prévues par le règlement général sur la comptabilité publique. / La responsabilité personnelle et pécuniaire prévue ci-dessus se trouve engagée dès lors qu'un déficit ou un manquant en monnaie ou en valeurs a été constaté, qu'une recette n'a pas été recouvrée, qu'une dépense a été irrégulièrement payée ou que, par le fait du comptable public, l'organisme public a dû procéder à l'indemnisation d'un autre organisme public ou d'un tiers ou a dû rétribuer un commis d'office pour produire les comptes. / (...) ". Aux termes du III du même article : " III - La responsabilité pécuniaire des comptables publics s'étend à toutes les opérations du poste comptable qu'ils dirigent depuis la date de leur installation jusqu'à la date de cessation des fonctions. / Cette responsabilité s'étend aux opérations des comptables publics placés sous leur autorité et à celles des régisseurs et, dans la limite des contrôles qu'ils sont tenus d'exercer, aux opérations des comptables publics et des correspondants centralisées dans leur comptabilité ainsi qu'aux actes des comptables de fait, s'ils ont eu connaissance de ces actes et ne les ont pas signalés à leurs supérieurs hiérarchiques. / Elle ne peut être mise en jeu à raison de la gestion de leurs prédécesseurs que pour les opérations prises en charge sans réserve lors de la remise de service ou qui n'auraient pas été contestées par le comptable entrant, dans un délai fixé par l'un des décrets prévus au paragraphe XII ci-après. / (...) ". Aux termes du VI du même article : " VI. - La responsabilité personnelle et pécuniaire prévue au I est mise en jeu par le ministre dont relève le comptable, le ministre chargé du budget ou le juge des comptes dans les conditions qui suivent. Les ministres concernés peuvent déléguer cette compétence. / Lorsque le manquement du comptable aux obligations mentionnées au I n'a pas causé de préjudice financier à l'organisme public concerné, le juge des comptes peut l'obliger à s'acquitter d'une somme arrêtée, pour chaque exercice, en tenant compte des circonstances de l'espèce. Le montant maximal de cette somme est fixé par décret en Conseil d'Etat en fonction du niveau des garanties mentionnées au II. / Lorsque le manquement du comptable aux obligations mentionnées au I a causé un préjudice financier à l'organisme public concerné ou que, par le fait du comptable public, l'organisme public a dû procéder à l'indemnisation d'un autre organisme public ou d'un tiers ou a dû rétribuer un commis d'office pour produire les comptes, le comptable a l'obligation de verser immédiatement de ses deniers personnels la somme correspondante. / (...) ". Par ailleurs, aux termes de l'article 21 du décret du 5 mars 2008 relatif à la constatation et à l'apurement des débets des comptables publics et assimilés : " Le délai mentionné au troisième alinéa du paragraphe III de l'article 60 de la loi du 23 février 1963 susvisé est fixé à six mois. / Ce délai peut être prorogé par décision du ministre chargé du budget. ".<br/>
<br/>
              3.	La responsabilité d'un comptable public ne peut être recherchée pour les actes de son prédécesseur sur lesquels il a valablement émis des réserves lors de la remise de service ou dans les délais fixés par la réglementation en vigueur. A cet égard, il appartient, le cas échéant, au juge des comptes de se prononcer sur la régularité et le bien-fondé des réserves émises par le comptable entrant, lesquelles doivent être précises. Si ces conditions sont remplies, le comptable sortant demeure alors seul responsable des opérations en cause. Doivent être regardées comme fondées des réserves concernant des titres relatifs à des créances non prescrites mais dont le recouvrement apparaît manifestement compromis à la date de prise de fonctions, en dépit des diligences auxquelles le comptable pourrait raisonnablement se livrer. Pour apprécier le caractère manifestement compromis du recouvrement de créances, le juge des comptes peut tenir compte, notamment, de la nature et du nombre des créances, des caractéristiques des débiteurs concernés ainsi que de la date de prescription. <br/>
<br/>
              4.	Pour décharger partiellement de sa responsabilité MmeB..., comptable entrant, la Cour des comptes a jugé que le nombre de créances concernées par les réserves régulièrement émises par l'intéressée le 27 décembre 2011, leur ancienneté et les caractéristiques de leurs débiteurs étaient tels que ces réserves devaient être regardées comme justifiées dès lors que le recouvrement des créances concernées, compte tenu des délais de prescription, était manifestement compromis à la date de sa prise de fonction. Ce faisant, la cour, qui n'a pas entaché son arrêt d'une contradiction de motifs, n'a pas méconnu les règles exposées au point précédent. Les moyens d'erreur de droit soulevés par la commune requérante doivent être par suite écartés. <br/>
<br/>
              5.	Il résulte de ce qui précède que la Commune de Papeete n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque. Par suite, ses conclusions relatives aux frais de l'instance ne peuvent qu'être rejetées. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
		Article 1er : Le pourvoi de la commune de Papeete est rejeté.<br/>
Article 2 : La présente décision sera notifiée à la commune de Papeete, à Mme A...B..., au ministre de l'action et des comptes publics et au parquet général près la Cour des comptes.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">18-01-03 COMPTABILITÉ PUBLIQUE ET BUDGET. RÉGIME JURIDIQUE DES ORDONNATEURS ET DES COMPTABLES. RESPONSABILITÉ. - IRRESPONSABILITÉ PÉCUNIAIRE D'UN COMPTABLE PUBLIC POUR LES ACTES DE SON PRÉDÉCESSEUR SUR LESQUELS IL A VALABLEMENT ÉMIS DES RÉSERVES (III DE L'ART. 60 DE LA LOI DU 23 FÉVRIER 1963) - 1) OBLIGATION POUR LE JUGE DES COMPTES DE SE PRONONCER SUR LA RÉGULARITÉ ET LE BIEN-FONDÉ DES RÉSERVES [RJ1] - 2) RÉSERVES CONCERNANT DES TITRES RELATIFS À DES CRÉANCES NON PRESCRITES MAIS DONT LE RECOUVREMENT APPARAÎT MANIFESTEMENT COMPROMIS À LA DATE DE PRISE DE FONCTIONS, EN DÉPIT DES DILIGENCES AUXQUELLES LE COMPTABLE POURRAIT RAISONNABLEMENT SE LIVRER - RÉSERVES DEVANT ÊTRE REGARDÉES COMME FONDÉES - MODALITÉS D'APPRÉCIATION - 3) ESPÈCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">18-01-04 COMPTABILITÉ PUBLIQUE ET BUDGET. RÉGIME JURIDIQUE DES ORDONNATEURS ET DES COMPTABLES. JUGEMENT DES COMPTES. - IRRESPONSABILITÉ PÉCUNIAIRE D'UN COMPTABLE PUBLIC POUR LES ACTES DE SON PRÉDÉCESSEUR SUR LESQUELS IL A VALABLEMENT ÉMIS DES RÉSERVES (III DE L'ART. 60 DE LA LOI DU 23 FÉVRIER 1963) - 1) OBLIGATION POUR LE JUGE DES COMPTES DE SE PRONONCER SUR LA RÉGULARITÉ ET LE BIEN-FONDÉ DES RÉSERVES [RJ1] - 2) RÉSERVES CONCERNANT DES TITRES RELATIFS À DES CRÉANCES NON PRESCRITES MAIS DONT LE RECOUVREMENT APPARAÎT MANIFESTEMENT COMPROMIS À LA DATE DE PRISE DE FONCTIONS, EN DÉPIT DES DILIGENCES AUXQUELLES LE COMPTABLE POURRAIT RAISONNABLEMENT SE LIVRER - RÉSERVES DEVANT ÊTRE REGARDÉES COMME FONDÉES - MODALITÉS D'APPRÉCIATION - 3) ESPÈCE.
</SCT>
<ANA ID="9A"> 18-01-03 1) La responsabilité d'un comptable public ne peut être recherchée pour les actes de son prédécesseur sur lesquels il a valablement émis des réserves lors de la remise de service ou dans les délais fixés par la réglementation en vigueur. A cet égard, il appartient, le cas échéant, au juge des comptes de se prononcer sur la régularité et le bien-fondé des réserves émises par le comptable entrant, lesquelles doivent être précises. Si ces conditions sont remplies, le comptable sortant demeure alors seul responsable des opérations en cause.... ,,2) Doivent être regardées comme fondées des réserves concernant des titres relatifs à des créances non prescrites mais dont le recouvrement apparaît manifestement compromis à la date de prise de fonctions, en dépit des diligences auxquelles le comptable pourrait raisonnablement se livrer. Pour apprécier le caractère manifestement compromis du recouvrement de créances, le juge des comptes peut tenir compte, notamment, de la nature et du nombre des créances, des caractéristiques des débiteurs concernés ainsi que de la date de prescription.,,,3) Pour décharger partiellement de sa responsabilité le comptable entrant, la Cour des comptes a jugé que le nombre de créances concernées par les réserves régulièrement émises par l'intéressée, leur ancienneté et les caractéristiques de leurs débiteurs étaient tels que ces réserves devaient être regardées comme justifiées dès lors que le recouvrement des créances concernées, compte tenu des délais de prescription, était manifestement compromis à la date de sa prise de fonction. Ce faisant, la cour n'a pas méconnu les règles précédemment exposées.</ANA>
<ANA ID="9B"> 18-01-04 1) La responsabilité d'un comptable public ne peut être recherchée pour les actes de son prédécesseur sur lesquels il a valablement émis des réserves lors de la remise de service ou dans les délais fixés par la réglementation en vigueur. A cet égard, il appartient, le cas échéant, au juge des comptes de se prononcer sur la régularité et le bien-fondé des réserves émises par le comptable entrant, lesquelles doivent être précises. Si ces conditions sont remplies, le comptable sortant demeure alors seul responsable des opérations en cause.... ,,2) Doivent être regardées comme fondées des réserves concernant des titres relatifs à des créances non prescrites mais dont le recouvrement apparaît manifestement compromis à la date de prise de fonctions, en dépit des diligences auxquelles le comptable pourrait raisonnablement se livrer. Pour apprécier le caractère manifestement compromis du recouvrement de créances, le juge des comptes peut tenir compte, notamment, de la nature et du nombre des créances, des caractéristiques des débiteurs concernés ainsi que de la date de prescription.,,,3) Pour décharger partiellement de sa responsabilité le comptable entrant, la Cour des comptes a jugé que le nombre de créances concernées par les réserves régulièrement émises par l'intéressée, leur ancienneté et les caractéristiques de leurs débiteurs étaient tels que ces réserves devaient être regardées comme justifiées dès lors que le recouvrement des créances concernées, compte tenu des délais de prescription, était manifestement compromis à la date de sa prise de fonction. Ce faisant, la cour n'a pas méconnu les règles précédemment exposées.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., sur l'obligation pour le juge des comptes de vérifier l'existence des réserves, CE, 17 novembre 1999,,et ministre de l'économie et des finances, n°s 181886 182622, T. pp. 720-722.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
