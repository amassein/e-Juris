<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026618673</ID>
<ANCIEN_ID>JG_L_2012_11_000000345749</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/61/86/CETATEXT000026618673.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 12/11/2012, 345749</TITRE>
<DATE_DEC>2012-11-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>345749</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MASSE-DESSEN, THOUVENIN ; SCP CELICE, BLANCPAIN, SOLTNER</AVOCATS>
<RAPPORTEUR>M. Rémi Decout-Paolini</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:345749.20121112</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 13 janvier et 11 avril 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour le Comité d'hygiène et de sécurité et des conditions de travail de l'établissement Michelin de Golbey, dont le siège est 6, rue Xay à Golbey (88192), représenté par son secrétaire et le Syndicat Sud Michelin, dont le siège est 8, rue Gabriel Péri à Clermont-Ferrand (63000), représenté par son secrétaire ; le Comité d'hygiène et de sécurité et des conditions de travail de l'établissement Michelin de Golbey et le Syndicat Sud Michelin demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 09NC01004 - 09NC01148 du 15 novembre 2010 par lequel la cour administrative d'appel de Nancy a annulé le jugement n° 0800751 du 2 juin 2009 par lequel le tribunal administratif de Nancy a annulé la décision du ministre du travail, des relations sociales et de la solidarité en date du 28 janvier 2008 refusant d'inscrire l'établissement Michelin situé à Golbey sur la liste des établissements ouvrant droit au dispositif de cessation anticipée des travailleurs de l'amiante ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter les appels du ministre du travail, des relations sociales et de la solidarité et de la Manufacture française de pneumatiques Michelin ;<br/>
<br/>
              3°) de mettre solidairement à la charge de l'Etat et de la Manufacture française de pneumatiques Michelin le versement de la somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu la loi n° 98-1194 du 23 décembre 1998 ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Rémi Decout-Paolini, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Masse-Dessen, Thouvenin, avocat du Comité d'hygiène et de sécurité et des conditions de travail de l'établissement Michelin de Golbey et du Syndicat Sud Michelin et de la SCP Célice, Blancpain, Soltner, avocat de la Manufacture française de pneumatiques Michelin,<br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Masse-Dessen, Thouvenin, avocat du Comité d'hygiène et de sécurité et des conditions de travail de l'établissement Michelin de Golbey et du Syndicat Sud Michelin et à la SCP Célice, Blancpain, Soltner, avocat de la Manufacture française de pneumatiques Michelin ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article 41 de la loi du 23 décembre 1998 visée ci-dessus, dans sa rédaction applicable en l'espèce : " I. - Une allocation de cessation anticipée d'activité est versée aux salariés et anciens salariés des établissements de fabrication de matériaux contenant de l'amiante, des établissements de flocage et de calorifugeage à l'amiante ou de construction et de réparation navales, sous réserve qu'ils cessent toute activité professionnelle, lorsqu'ils remplissent les conditions suivantes : / 1° Travailler ou avoir travaillé dans un des établissements mentionnés ci-dessus et figurant sur une liste établie par arrêté des ministres chargés du travail, de la sécurité sociale et du budget, pendant la période où y étaient fabriqués ou traités l'amiante ou des matériaux contenant de l'amiante. L'exercice des activités de fabrication de matériaux contenant de l'amiante, de flocage et de calorifugeage à l'amiante de l'établissement doit présenter un caractère significatif " ;<br/>
<br/>
              2. Considérant qu'il résulte de ces dispositions que peuvent seuls être légalement inscrits sur la liste qu'elles prévoient les établissements dans lesquels les opérations de calorifugeage ou de flocage à l'amiante ont, compte tenu notamment de leur fréquence et de la proportion de salariés qui y ont été affectés, représenté sur la période en cause une part significative de l'activité de ces établissements ; que les opérations de calorifugeage à l'amiante doivent, pour l'application de ces dispositions, s'entendre des interventions qui ont pour but d'utiliser l'amiante à des fins d'isolation thermique ; que ne sauraient par suite ouvrir droit à l'allocation prévue par ce texte les utilisations de l'amiante à des fins autres que l'isolation thermique, alors même que, par l'effet de ses propriétés intrinsèques, l'amiante ainsi utilisée assurerait également une isolation thermique ; <br/>
<br/>
              3. Considérant qu'il résulte de ce qui précède que la cour administrative d'appel de Nancy a pu, après avoir souverainement estimé, sans entacher son appréciation de dénaturation, que les salariés de l'établissement de la Manufacture française des pneumatiques Michelin situé à Golbey avaient utilisé de l'amiante pour assurer l'étanchéité aux fuites de gaz de fours de chauffage de fils d'acier, déduire sans erreur de droit que de telles interventions ne constituaient pas des opérations de calorifugeage au sens des dispositions citées ci-dessus de l'article 41 de la loi du 23 décembre 1998 ; <br/>
<br/>
              4. Considérant que, pour les mêmes raisons, la cour administrative d'appel a pu, sans erreur de droit, estimer que l'entretien des éléments amiantés servant à l'isolation phonique des machines d'assemblage utilisées par le même établissement n'était pas constitutif d'opérations de calorifugeage au sens de cette même loi ; qu'est à cet égard sans incidence la circonstance, au demeurant souverainement appréciée par les juges du fond, que ces éléments amiantés n'auraient pas été de petite taille ; <br/>
<br/>
              5. Considérant enfin que, sur la base de la caractérisation des opérations de calorifugeage à l'amiante à laquelle elle avait ainsi procédé, la cour administrative d'appel de Nancy a pu, sans dénaturer les pièces du dossier, estimer que les opérations de maintenance des appareils de l'établissement de Golbey qui étaient assimilables à un calorifugeage à l'amiante ne concernaient que 124 machines sur un total de 1 451 ; qu'en l'absence de toute discussion, dans le dossier qui lui était soumis, de l'importance comparée des opérations de maintenance propres à chacune des différentes machines de l'établissement, la cour a pu en déduire sans erreur de droit que les opérations de calorifugeage à l'amiante ne représentaient pas, sur la période en cause, une part significative de l'activité de l'établissement ;<br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède que le pourvoi du Comité d'hygiène et de sécurité et des conditions de travail de l'établissement Michelin de Golbey et du Syndicat Sud Michelin doit être rejeté, y compris ses conclusions relatives au bénéfice des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi du Comité d'hygiène et de sécurité et des conditions de travail de l'établissement Michelin de Golbey et du Syndicat Sud Michelin est rejeté.<br/>
Article 2 : La présente décision sera notifiée au Comité d'hygiène et de sécurité et des conditions de travail de l'établissement Michelin de Golbey, au Syndicat Sud Michelin, à la Manufacture française de pneumatiques Michelin et au ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">61-03 SANTÉ PUBLIQUE. LUTTE CONTRE LES FLÉAUX SOCIAUX. - VERSEMENT D'UNE ALLOCATION DE CESSATION ANTICIPÉE D'ACTIVITÉ AUX SALARIÉS ET ANCIENS SALARIÉS DES ÉTABLISSEMENTS DE FABRICATION DE MATÉRIAUX CONTENANT DE L'AMIANTE, DANS LESQUELS LES OPÉRATIONS LIÉES À L'AMIANTE REPRÉSENTAIENT UNE PART SIGNIFICATIVE DE L'ACTIVITÉ (ART. 41 DE LA LOI N° 98-1194 DU 23 DÉCEMBRE 1998) - NOTION D'OPÉRATION DE CALORIFUGEAGE - CHAMP D'APPLICATION - UTILISATION DE L'AMIANTE À DES FINS AUTRES QUE L'ISOLATION THERMIQUE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">66-03 TRAVAIL ET EMPLOI. CONDITIONS DE TRAVAIL. - VERSEMENT D'UNE ALLOCATION DE CESSATION ANTICIPÉE D'ACTIVITÉ AUX SALARIÉS ET ANCIENS SALARIÉS DES ÉTABLISSEMENTS DE FABRICATION DE MATÉRIAUX CONTENANT DE L'AMIANTE, DANS LESQUELS LES OPÉRATIONS LIÉES À L'AMIANTE REPRÉSENTAIENT UNE PART SIGNIFICATIVE DE L'ACTIVITÉ (ART. 41 DE LA LOI N° 98-1194 DU 23 DÉCEMBRE 1998) - NOTION D'OPÉRATION DE CALORIFUGEAGE - CHAMP D'APPLICATION - UTILISATION DE L'AMIANTE À DES FINS AUTRES QUE L'ISOLATION THERMIQUE.
</SCT>
<ANA ID="9A"> 61-03 Il résulte des dispositions de l'article 41 de la loi n° 98-1194 du 23 décembre 1998 que peuvent seuls être légalement inscrits sur la liste qu'elles prévoient les établissements dans lesquels les opérations de calorifugeage ou de flocage à l'amiante ont, compte tenu notamment de leur fréquence et de la proportion de salariés qui y ont été affectés, représenté sur la période en cause une part significative de l'activité de ces établissements. Les opérations de calorifugeage à l'amiante doivent, pour l'application de ces dispositions, s'entendre des interventions qui ont pour but d'utiliser l'amiante à des fins d'isolation thermique. Ne sauraient par suite ouvrir droit à l'allocation prévue par ce texte les utilisations de l'amiante à des fins autres que l'isolation thermique, par exemple à des fins d'isolation phonique ou aux fins de colmater des fuites de gaz, alors même que, par l'effet de ses propriétés intrinsèques, l'amiante ainsi utilisée assurerait également une isolation thermique.</ANA>
<ANA ID="9B"> 66-03 Il résulte des dispositions de l'article 41 de la loi n° 98-1194 du 23 décembre 1998 que peuvent seuls être légalement inscrits sur la liste qu'elles prévoient les établissements dans lesquels les opérations de calorifugeage ou de flocage à l'amiante ont, compte tenu notamment de leur fréquence et de la proportion de salariés qui y ont été affectés, représenté sur la période en cause une part significative de l'activité de ces établissements. Les opérations de calorifugeage à l'amiante doivent, pour l'application de ces dispositions, s'entendre des interventions qui ont pour but d'utiliser l'amiante à des fins d'isolation thermique. Ne sauraient par suite ouvrir droit à l'allocation prévue par ce texte les utilisations de l'amiante à des fins autres que l'isolation thermique, par exemple à des fins d'isolation phonique ou aux fins de colmater des fuites de gaz, alors même que, par l'effet de ses propriétés intrinsèques, l'amiante ainsi utilisée assurerait également une isolation thermique.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
