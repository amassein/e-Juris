<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029812962</ID>
<ANCIEN_ID>JG_L_2014_11_000000366154</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/81/29/CETATEXT000029812962.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème / 4ème SSR, 28/11/2014, 366154, Publié au recueil Lebon</TITRE>
<DATE_DEC>2014-11-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>366154</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème / 4ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROGER, SEVAUX, MATHONNET ; LE PRADO ; SCP BARADUC, DUHAMEL, RAMEIX ; RICARD</AVOCATS>
<RAPPORTEUR>M. Olivier Rousselle</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:366154.20141128</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 18 février et 21 mai 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour l'Office national de l'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales (ONIAM), dont le siège est 36 avenue du Général de Gaulle Tour Gallieni II à Bagnolet cedex (93175) ;  l'ONIAM demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 11BX02376 du 18 décembre 2012 par lequel la cour administrative d'appel de Bordeaux, réformant le jugement n° 0901353 du 7 juillet 2011 du tribunal administratif de Poitiers, l'a condamné à verser à M. B...A...la somme de 100 673,50 euros avec les intérêts au taux légal à compter du 27 mars 2009 et a rejeté son action récursoire dirigée contre le centre hospitalier de Saintes ; <br/>
<br/>
              2°) réglant l'affaire au fond, de réformer le jugement du 7 juillet 2011 du tribunal administratif de Poitiers en ramenant à 71 466,06 euros l'indemnité accordée à M. A...et de condamner le centre hospitalier de Saintes à en assumer la charge finale et à verser la somme de 1 400 euros au titre de l'expertise demandée par la CRCI de Poitou-Charentes ; <br/>
<br/>
              3°) de mettre à la charge du centre hospitalier de Saintes le versement d'une somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de la santé publique ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Rousselle, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Roger, Sevaux, Mathonnet, avocat de l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales, à Me Ricard, avocat de M.A..., à Me Le Prado, avocat du centre hospitalier de Saintes et à la SCP Baraduc, Duhamel, Rameix, avocat de la caisse de mutualité sociale agricole de la Charente-Maritime ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond qu'après avoir subi le 7 avril 2005 au centre hospitalier de Saintes une opération de la prostate, M. A... a présenté à partir du 14 avril une infection urinaire qui a été traitée par une antibiothérapie ; qu'il a dû être à nouveau hospitalisé le 27 avril 2005 en raison d'une dégradation de son état de santé s'accompagnant d'un syndrome infectieux ; que le 30 avril 2005, une endocardite aortique à staphylocoque doré multi-résistant avec infarctus cérébelleux a été diagnostiquée ; que M. A...a dû subir le 4 juin suivant dans un hôpital de Bordeaux une intervention; que, par un jugement du 7 juillet 2011, le tribunal administratif de Poitiers lui a accordé, en application des dispositions de l'article L. 1142-1-1 du code de la santé publique, une indemnité de 110 566,66 euros, à la charge de l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales (ONIAM) et a rejeté l'action récursoire présentée par cet office contre le centre hospitalier de Saintes ainsi que la demande de la caisse de mutualité sociale agricole (CMSA) des Charentes tendant au remboursement de ses frais ; que, par un arrêt du 18 décembre 2012, la cour administrative d'appel a ramené à 100 673,50 euros l'indemnité mise à la charge de l'ONIAM et confirmé le rejet de son action récursoire et de la demande de la caisse ; que l'ONIAM et la CMSA des Charentes se pourvoient en cassation contre cet arrêt ; qu'eu égard aux moyens qu'ils soulèvent, qui sont tous relatifs à la responsabilité du centre hospitalier de Saintes, ils doivent être regardés comme demandant l'annulation de l'arrêt qu'ils attaquent en tant qu'il rejette l'action récursoire de l'office et la demande de la caisse  ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 1142-1-1 du code de la santé publique dans sa rédaction applicable au litige : " Sans préjudice des dispositions du septième alinéa de l'article L. 1142-17, ouvrent droit à réparation au titre de la solidarité nationale : 1° Les dommages résultant d'infections nosocomiales dans les établissements, services ou organismes mentionnés au premier alinéa du I de l'article L. 1142-1 correspondant à un taux d'incapacité permanente supérieur à 25 % déterminé par référence au barème mentionné au II du même article, ainsi que les décès provoqués par ces infections nosocomiales (...) " ; qu'aux termes de l'article L. 1142-21, dans sa rédaction applicable au litige : " Lorsque la juridiction compétente, saisie d'une demande d'indemnisation des conséquences dommageables d'actes de prévention, de diagnostic ou de soins dans un établissement de santé, estime que les dommages subis sont indemnisables au titre du II de l'article L. 1142-1 ou au titre de l'article L. 1142-1-1, l'office est appelé en la cause s'il ne l'avait pas été initialement. Il devient défendeur en la procédure. / Lorsqu'il résulte de la décision du juge que l'office indemnise la victime ou ses ayants droit au titre de l'article L. 1142-1-1, celui-ci ne peut exercer une action récursoire contre le professionnel, l'établissement de santé, le service ou l'organisme concerné ou son assureur, sauf en cas de faute établie à l'origine du dommage, notamment le manquement caractérisé aux obligations posées par la réglementation en matière de lutte contre les infections nosocomiales (...) " ;<br/>
<br/>
              3. Considérant, en premier lieu, que la cour administrative d'appel a jugé que l'existence d'un manquement caractérisé du centre hospitalier de Saintes aux obligations prévues par la réglementation en matière de lutte contre les infections nosocomiales n'était pas établie, en relevant  notamment que la seule affirmation de M. A...selon laquelle un infirmier avait manipulé à mains nues sa sonde urinaire n'était pas suffisante à cet égard ; qu'en s'abstenant d'exiger du centre hospitalier qu'il verse au dossier des documents relatifs à la prévention des infections nosocomiales dans l'établissement, la cour, dont l'arrêt n'est pas entaché sur ce point d'insuffisance de motivation, a souverainement apprécié l'utilité d'une telle mesure d'instruction et n'a pas commis d'erreur de droit ; <br/>
<br/>
              4. Considérant, en deuxième lieu, que, pour juger que le retard avec lequel l'endocardite avait été diagnostiquée ne résultait pas d'une faute médicale, la cour a relevé que si, selon l'expert, l'existence d'une pathologie valvulaire notée dans le bilan pré-anesthésique " aurait pu faire évoquer une endocardite ", la survenue de cette pathologie constituait un événement rare et difficile à diagnostiquer ; qu'en statuant ainsi, elle n'a ni dénaturé les pièces du dossier, ni commis d'erreur de qualification juridique ; <br/>
<br/>
              5. Considérant, enfin, qu'en prévoyant, par les dispositions citées ci-dessus de l'article L. 1142-21 du code de la santé publique, que l'ONIAM, condamné, en application de l'article L. 1142-1-1 du même code, à réparer les conséquences d'une infection nosocomiale ayant entraîné une incapacité permanente supérieure à 25 % ou le décès de la victime, peut exercer une action récursoire contre le professionnel, l'établissement de santé, le service ou l'organisme concerné ou son assureur " en cas de faute établie à l'origine du dommage ", le législateur n'a pas entendu exclure l'exercice de cette action lorsqu'une faute établie a entraîné la perte d'une chance d'éviter l'infection nosocomiale ou d'en limiter les conséquences ; qu'ainsi, la cour administrative d'appel a commis une erreur de droit en jugeant, pour écarter la possibilité pour l'ONIAM d'exercer une action récursoire en se prévalant de ce que le centre hospitalier n'avait pas informé M. A...des risques d'infection nosocomiale que comportait l'intervention qui lui était proposée, qu'une telle faute, à la supposer établie, n'aurait pas constitué la cause directe de l'infection nosocomiale mais pouvait seulement avoir fait perdre au patient une chance de l'éviter en refusant l'intervention ;<br/>
<br/>
              6. Considérant, toutefois, que le législateur n'a pas entendu permettre à l'office, dans le cadre de son action récursoire dirigée contre l'établissement de santé, de se prévaloir de la méconnaissance du droit que l'article L. 1111-2 du code de la santé publique reconnaît aux patients d'être informés des risques des traitements qui leur sont proposés ; qu'il y a lieu de substituer ce motif, qui n'appelle l'appréciation d'aucune circonstance de fait, à celui sur lequel repose l'arrêt attaqué, dont il justifie sur ce point le dispositif ; <br/>
<br/>
              7. Considérant qu'il résulte de tout ce qui précède que l'ONIAM et la CMSA des Charentes ne sont pas fondés à demander l'annulation de l'arrêt de la cour administrative d'appel de Bordeaux du 18 décembre 2012 ;<br/>
<br/>
              8. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'ONIAM, au titre de l'article L. 761-1 du code de justice administrative, le versement à M. A...de la somme de 3 000 euros ; que ces dispositions font obstacle à ce que soient mises à la charge du centre hospitalier de Saintes, qui n'est pas la partie perdante dans la présente instance,  la somme que demandent  à ce titre l'ONIAM et la CMSA de la Charente-Maritime ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les pourvois de l'ONIAM et de la CMSA des Charentes sont rejetés.<br/>
<br/>
Article 2 : L'ONIAM versera une somme de 3 000 euros à M. A...au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée à l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales, au centre hospitalier de Saintes, à M. B...A...et à la caisse de mutualité sociale agricole des Charentes.<br/>
Copie en sera adressée pour information à la ministre des affaires sociales, de la santé et des droits des femmes.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">60-02-01-01-005-02 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RESPONSABILITÉ EN RAISON DES DIFFÉRENTES ACTIVITÉS DES SERVICES PUBLICS. SERVICE PUBLIC DE SANTÉ. ÉTABLISSEMENTS PUBLICS D'HOSPITALISATION. RESPONSABILITÉ SANS FAUTE. ACTES MÉDICAUX. - INDEMNISATION EN MATIÈRE D'INFECTIONS NOSOCOMIALES PAR L'ONIAM - ACTION RÉCURSOIRE DE L'ONIAM PRÉVUE PAR L'ARTICLE L. 1142-21 DU CODE DE LA SANTÉ PUBLIQUE - 1) POSSIBILITÉ D'INVOQUER DES FAUTES AYANT SEULEMENT FAIT PERDRE UNE CHANCE D'ÉVITER L'INFECTION - EXISTENCE EN PRINCIPE - 2) EXCEPTION - POSSIBILITÉ D'INVOQUER UN MANQUEMENT À L'OBLIGATION D'INFORMATION DU PATIENT - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">60-02-01-01-02 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RESPONSABILITÉ EN RAISON DES DIFFÉRENTES ACTIVITÉS DES SERVICES PUBLICS. SERVICE PUBLIC DE SANTÉ. ÉTABLISSEMENTS PUBLICS D'HOSPITALISATION. RESPONSABILITÉ POUR FAUTE MÉDICALE : ACTES MÉDICAUX. - INDEMNISATION EN MATIÈRE D'INFECTIONS NOSOCOMIALES PAR L'ONIAM - ACTION RÉCURSOIRE DE L'ONIAM PRÉVUE PAR L'ARTICLE L. 1142-21 DU CODE DE LA SANTÉ PUBLIQUE - 1) POSSIBILITÉ D'INVOQUER DES FAUTES AYANT SEULEMENT FAIT PERDRE UNE CHANCE D'ÉVITER L'INFECTION - EXISTENCE EN PRINCIPE - 2) EXCEPTION - POSSIBILITÉ D'INVOQUER UN MANQUEMENT À L'OBLIGATION D'INFORMATION DU PATIENT - ABSENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">60-04-04-01 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RÉPARATION. MODALITÉS DE LA RÉPARATION. SOLIDARITÉ. - INDEMNISATION EN MATIÈRE D'INFECTIONS NOSOCOMIALES PAR L'ONIAM - ACTION RÉCURSOIRE DE L'ONIAM PRÉVUE PAR L'ARTICLE L. 1142-21 DU CODE DE LA SANTÉ PUBLIQUE - 1) POSSIBILITÉ D'INVOQUER DES FAUTES AYANT SEULEMENT FAIT PERDRE UNE CHANCE D'ÉVITER L'INFECTION - EXISTENCE EN PRINCIPE - 2) EXCEPTION - POSSIBILITÉ D'INVOQUER UN MANQUEMENT À L'OBLIGATION D'INFORMATION DU PATIENT - ABSENCE.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">60-05-02 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RECOURS OUVERTS AUX DÉBITEURS DE L'INDEMNITÉ, AUX ASSUREURS DE LA VICTIME ET AUX CAISSES DE SÉCURITÉ SOCIALE. ACTION RÉCURSOIRE. - INDEMNISATION EN MATIÈRE D'INFECTIONS NOSOCOMIALES PAR L'ONIAM - ACTION RÉCURSOIRE DE L'ONIAM PRÉVUE PAR L'ARTICLE L. 1142-21 DU CODE DE LA SANTÉ PUBLIQUE - 1) POSSIBILITÉ D'INVOQUER DES FAUTES AYANT SEULEMENT FAIT PERDRE UNE CHANCE D'ÉVITER L'INFECTION - EXISTENCE EN PRINCIPE - 2) EXCEPTION - POSSIBILITÉ D'INVOQUER UN MANQUEMENT À L'OBLIGATION D'INFORMATION DU PATIENT - ABSENCE.
</SCT>
<ANA ID="9A"> 60-02-01-01-005-02 1) Les dispositions de l'article L. 1142-21 du code de la santé publique prévoient que l'office d'indemnisation des accidents médicaux (ONIAM), condamné, en application de l'article L. 1142-1-1 du même code, à réparer les conséquences d'une infection nosocomiale ayant entraîné une incapacité permanente supérieure à 25 % ou le décès de la victime, peut exercer une action récursoire contre le professionnel, l'établissement de santé, le service ou l'organisme concerné ou son assureur en cas de faute établie à l'origine du dommage.... ,,Le législateur n'a pas entendu exclure l'exercice de cette action lorsqu'une faute établie a entraîné la perte d'une chance d'éviter l'infection nosocomiale ou d'en limiter les conséquences....  ,,2) Toutefois, le législateur n'a pas entendu permettre à l'office, dans le cadre de son action récursoire dirigée contre l'établissement de santé, de se prévaloir de la méconnaissance du droit que l'article L. 1111-2 du code de la santé publique reconnaît aux patients d'être informés des risques des traitements qui leur sont proposés.</ANA>
<ANA ID="9B"> 60-02-01-01-02 1) Les dispositions de l'article L. 1142-21 du code de la santé publique prévoient que l'office d'indemnisation des accidents médicaux (ONIAM), condamné, en application de l'article L. 1142-1-1 du même code, à réparer les conséquences d'une infection nosocomiale ayant entraîné une incapacité permanente supérieure à 25 % ou le décès de la victime, peut exercer une action récursoire contre le professionnel, l'établissement de santé, le service ou l'organisme concerné ou son assureur en cas de faute établie à l'origine du dommage.... ,,Le législateur n'a pas entendu exclure l'exercice de cette action lorsqu'une faute établie a entraîné la perte d'une chance d'éviter l'infection nosocomiale ou d'en limiter les conséquences....  ,,2) Toutefois, le législateur n'a pas entendu permettre à l'office, dans le cadre de son action récursoire dirigée contre l'établissement de santé, de se prévaloir de la méconnaissance du droit que l'article L. 1111-2 du code de la santé publique reconnaît aux patients d'être informés des risques des traitements qui leur sont proposés.</ANA>
<ANA ID="9C"> 60-04-04-01 1) Les dispositions de l'article L. 1142-21 du code de la santé publique prévoient que l'office d'indemnisation des accidents médicaux (ONIAM), condamné, en application de l'article L. 1142-1-1 du même code, à réparer les conséquences d'une infection nosocomiale ayant entraîné une incapacité permanente supérieure à 25 % ou le décès de la victime, peut exercer une action récursoire contre le professionnel, l'établissement de santé, le service ou l'organisme concerné ou son assureur en cas de faute établie à l'origine du dommage.... ,,Le législateur n'a pas entendu exclure l'exercice de cette action lorsqu'une faute établie a entraîné la perte d'une chance d'éviter l'infection nosocomiale ou d'en limiter les conséquences....  ,,2) Toutefois, le législateur n'a pas entendu permettre à l'office, dans le cadre de son action récursoire dirigée contre l'établissement de santé, de se prévaloir de la méconnaissance du droit que l'article L. 1111-2 du code de la santé publique reconnaît aux patients d'être informés des risques des traitements qui leur sont proposés.</ANA>
<ANA ID="9D"> 60-05-02 1) Les dispositions de l'article L. 1142-21 du code de la santé publique prévoient que l'office d'indemnisation des accidents médicaux (ONIAM), condamné, en application de l'article L. 1142-1-1 du même code, à réparer les conséquences d'une infection nosocomiale ayant entraîné une incapacité permanente supérieure à 25 % ou le décès de la victime, peut exercer une action récursoire contre le professionnel, l'établissement de santé, le service ou l'organisme concerné ou son assureur en cas de faute établie à l'origine du dommage.... ,,Le législateur n'a pas entendu exclure l'exercice de cette action lorsqu'une faute établie a entraîné la perte d'une chance d'éviter l'infection nosocomiale ou d'en limiter les conséquences....  ,,2) Toutefois, le législateur n'a pas entendu permettre à l'office, dans le cadre de son action récursoire dirigée contre l'établissement de santé, de se prévaloir de la méconnaissance du droit que l'article L. 1111-2 du code de la santé publique reconnaît aux patients d'être informés des risques des traitements qui leur sont proposés.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
