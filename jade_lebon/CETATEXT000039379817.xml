<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039379817</ID>
<ANCIEN_ID>JG_L_2019_11_000000422938</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/37/98/CETATEXT000039379817.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 6ème chambres réunies, 13/11/2019, 422938</TITRE>
<DATE_DEC>2019-11-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>422938</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MARLANGE, DE LA BURGADE</AVOCATS>
<RAPPORTEUR>Mme Louise Cadin</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2019:422938.20191113</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. D... B... a demandé au tribunal administratif de Versailles d'annuler pour excès de pouvoir la décision du 18 juillet 2016 par laquelle la préfète de l'Essonne a rejeté sa demande d'échange de permis de conduire burkinabé contre un permis de conduire français et de lui enjoindre de procéder à cet échange dans un délai d'un mois. Par une ordonnance n° 1605271 du 18 mai 2018, la présidente du tribunal administratif a donné acte du désistement de M. B... de sa demande par application de l'article R. 612-5-1 du code de justice administrative. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 6 août et 29 octobre 2018 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros à verser à la SCP Marlange, de La Burgade au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme C... A..., auditrice,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Marlange, de la Burgade, avocat de M. B... ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Aux termes de l'article R. 612-5-1 du même code : " Lorsque l'état du dossier permet de s'interroger sur l'intérêt que la requête conserve pour son auteur, le président de la formation de jugement (...) peut inviter le requérant à confirmer expressément le maintien de ses conclusions. La demande qui lui est adressée mentionne que, à défaut de réception de cette confirmation à l'expiration du délai fixé, qui ne peut être inférieur à un mois, il sera réputé s'être désisté de l'ensemble de ses conclusions ". <br/>
<br/>
              2. En vertu de l'article R. 431-1 du code de justice administrative, lorsqu'une partie est représentée devant le tribunal administratif par un avocat, " les actes de procédure, à l'exception de la notification de la décision prévue aux articles R. 751-3 et suivants, ne sont accomplis qu'à l'égard de ce mandataire ". <br/>
<br/>
              3. Par ailleurs, la loi du 10 juillet 1991 relative à l'aide juridique prévoit, en son article 2, que les personnes physiques dont les ressources sont insuffisantes pour faire valoir leurs droits en justice peuvent bénéficier d'une aide juridictionnelle. En vertu de l'article 25 de cette loi, le bénéficiaire de l'aide juridictionnelle a droit à l'assistance d'un avocat choisi par lui ou, à défaut, désigné par le bâtonnier de l'ordre des avocats. <br/>
<br/>
              4. En premier lieu, il résulte de l'article R. 431-1 du code de justice administrative cité au point 2 que lorsqu'une partie est représentée devant le tribunal administratif ou la cour administrative d'appel par un des mandataires mentionnés à l'article R. 431-2 du même code, c'est-à-dire par un avocat ou par un avocat au Conseil d'Etat et à la Cour de cassation, les actes de procédure, à l'exception de la notification de la décision prévue aux articles R. 751-3 et suivants, ne sont accomplis qu'à l'égard de ce mandataire. Il s'ensuit que l'invitation à confirmer expressément le maintien des conclusions d'une requête en application de l'article R. 612-5-1 du code de justice administrative doit être adressée à ce mandataire. Il en va ainsi même lorsque le requérant bénéfice d'un avocat au titre de l'aide juridictionnelle. En l'absence de réponse de l'avocat agissant au titre de l'aide juridictionnelle à l'invitation qui lui a été adressée en application de l'article R. 612-5-1, le requérant est réputé s'être désisté de sa demande, sans qu'il y ait lieu pour la juridiction ni de mettre en demeure l'avocat de répondre à l'invitation qui lui a été adressée, ni d'informer le requérant de ce que l'avocat n'a pas répondu à cette invitation. <br/>
<br/>
              5. Il ressort des pièces du dossier soumis aux juges du fond que M. B... a demandé au tribunal administratif de Versailles l'annulation de la décision du 18 juillet 2016 par laquelle la préfète de l'Essonne a rejeté sa demande d'échange de permis de conduire burkinabé contre un permis de conduire français. Par une décision du 18 octobre 2016, le bureau de l'aide juridictionnelle du tribunal de grande instance de Versailles lui a accordé le bénéfice de l'aide juridictionnelle prévue par la loi du 10 juillet 1991. Le 6 avril 2018, le tribunal administratif a, sur le fondement de l'article R. 612-5-1 du code de justice administrative, adressé à l'avocat agissant au titre de l'aide juridictionnelle une invitation à confirmer expressément le maintien de ses conclusions, dans un délai d'un mois. En l'absence de réponse de cet avocat, la présidente du tribunal administratif a, par l'ordonnance attaquée, donné acte du désistement de M. B.... Il résulte de ce qui a été dit au point 4 que, contrairement à ce que soutient le requérant, ni la circonstance que le tribunal n'a pas mis son avocat en demeure de répondre à l'invitation ni celle qu'il ne l'a pas informé de ce que l'avocat n'avait pas répondu à la demande de la juridiction n'entachent d'irrégularité l'ordonnance attaquée. <br/>
<br/>
              6. En second lieu, les dispositions de l'article R. 612-5-1 du code de justice administrative, prises dans l'objectif de bonne administration de la justice, prévoient, à peine d'irrégularité de la décision constatant le désistement, que la partie concernée doit être expressément invitée à maintenir ses conclusions, doit disposer d'un délai d'au moins un mois pour y procéder et doit être préalablement et régulièrement informée du délai dont elle dispose et des conséquences d'une abstention de sa part. Dans ces conditions, M. B... n'est pas fondé à soutenir que ces dispositions méconnaissent le droit à un recours juridictionnel effectif garanti par les stipulations des articles 6 et 13 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, la circonstance que le requérant bénéficie de l'aide juridictionnelle étant, à cet égard, sans incidence.<br/>
<br/>
              7. Il résulte de tout ce qui précède que M. B... n'est pas fondé à demander l'annulation de l'ordonnance qu'il attaque. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>
              D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le pourvoi de M. B... est rejeté.<br/>
Article 2 : La présente décision sera notifiée à M. D... B... et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">26-055-01-13 DROITS CIVILS ET INDIVIDUELS. CONVENTION EUROPÉENNE DES DROITS DE L'HOMME. DROITS GARANTIS PAR LA CONVENTION. DROIT À UN RECOURS EFFECTIF (ART. 13). - DÉSISTEMENT D'OFFICE D'UN REQUÉRANT N'AYANT PAS RÉPONDU, À L'EXPIRATION DU DÉLAI IMPARTI, À UNE DEMANDE DU JUGE L'INVITANT EXPRESSÉMENT À CONFIRMER LE MAINTIEN DE SES CONCLUSIONS (ART. R. 612-5-1 DU CJA) - MÉCONNAISSANCE - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-01-05 PROCÉDURE. INTRODUCTION DE L'INSTANCE. QUALITÉ POUR AGIR. - REPRÉSENTATION PAR UN MANDATAIRE - 1) PRINCIPE - ACTES DE PROCÉDURE ACCOMPLIS À L'ÉGARD DU SEUL MANDATAIRE (ART. R. 431-1 DU CJA) [RJ1] - 2) APPLICATION - INVITATION À CONFIRMER, SOUS PEINE DE DÉSISTEMENT D'OFFICE, LE MAINTIEN DES CONCLUSIONS D'UNE REQUÊTE (ART. R. 612-5-1 DU CJA) - INVITATION DEVANT ÊTRE ADRESSÉE AU SEUL MANDATAIRE DU REQUÉRANT [RJ2], MÊME LORSQUE LE REQUÉRANT BÉNÉFICE D'UN AVOCAT AU TITRE DE L'AIDE JURIDICTIONNELLE - REQUÉRANT RÉPUTÉ S'ÊTRE DÉSISTÉ DE SA DEMANDE EN CAS D'ABSENCE DE RÉPONSE DE CET AVOCAT - EXISTENCE, SANS QU'IL Y AIT LIEU NI DE METTRE EN DEMEURE L'AVOCAT DE RÉPONDRE À L'INVITATION, NI D'INFORMER LE REQUÉRANT DE CE QUE L'AVOCAT N'Y A PAS RÉPONDU [RJ3].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-01-08-02 PROCÉDURE. INTRODUCTION DE L'INSTANCE. FORMES DE LA REQUÊTE. MINISTÈRE D'AVOCAT. - REPRÉSENTATION PAR UN MANDATAIRE - 1) PRINCIPE - ACTES DE PROCÉDURE ACCOMPLIS À L'ÉGARD DU SEUL MANDATAIRE [RJ1] - 2) APPLICATION - INVITATION À CONFIRMER, SOUS PEINE DE DÉSISTEMENT D'OFFICE, LE MAINTIEN DES CONCLUSIONS D'UNE REQUÊTE (ART. R. 612-5-1 DU CJA) - INVITATION DEVANT ÊTRE ADRESSÉE AU SEUL MANDATAIRE DU REQUÉRANT [RJ2], MÊME LORSQUE LE REQUÉRANT BÉNÉFICE D'UN AVOCAT AU TITRE DE L'AIDE JURIDICTIONNELLE - REQUÉRANT RÉPUTÉ S'ÊTRE DÉSISTÉ DE SA DEMANDE EN CAS D'ABSENCE DE RÉPONSE DE CET AVOCAT - EXISTENCE, SANS QU'IL Y AIT LIEU NI DE METTRE EN DEMEURE L'AVOCAT DE RÉPONDRE À L'INVITATION, NI D'INFORMER LE REQUÉRANT DE CE QUE L'AVOCAT N'Y A PAS RÉPONDU [RJ3].
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">54-05-04-03 PROCÉDURE. INCIDENTS. DÉSISTEMENT. DÉSISTEMENT D'OFFICE. - INVITATION À CONFIRMER, SOUS PEINE DE DÉSISTEMENT D'OFFICE, LE MAINTIEN DES CONCLUSIONS D'UNE REQUÊTE (ART. R. 612-5-1 DU CJA) - 1) INVITATION DEVANT ÊTRE ADRESSÉE AU SEUL MANDATAIRE DU REQUÉRANT [RJ2], MÊME LORSQUE LE REQUÉRANT BÉNÉFICE D'UN AVOCAT AU TITRE DE L'AIDE JURIDICTIONNELLE - REQUÉRANT RÉPUTÉ S'ÊTRE DÉSISTÉ DE SA DEMANDE EN CAS D'ABSENCE DE RÉPONSE DE CET AVOCAT - EXISTENCE, SANS QU'IL Y AIT LIEU NI DE METTRE EN DEMEURE L'AVOCAT DE RÉPONDRE À L'INVITATION, NI D'INFORMER LE REQUÉRANT DE CE QUE L'AVOCAT N'Y A PAS RÉPONDU [RJ3] - 2) MÉCONNAISSANCE DU DROIT À UN RECOURS JURIDICTIONNEL EFFECTIF (ART. 6 ET 13 DE LA CONV. EDH) - ABSENCE.
</SCT>
<ANA ID="9A"> 26-055-01-13 Les dispositions de l'article R. 612-5-1 du code de justice administrative (CJA), prises dans l'objectif de bonne administration de la justice, prévoient, à peine d'irrégularité de la décision constatant le désistement, que la partie concernée doit être expressément invitée à maintenir ses conclusions, doit disposer d'un délai d'au moins un mois pour y procéder et doit être préalablement et régulièrement informée du délai dont elle dispose et des conséquences d'une abstention de sa part. Dans ces conditions, le requérant n'est pas fondé à soutenir que ces dispositions méconnaissent le droit à un recours juridictionnel effectif garanti par les stipulations des articles 6 et 13 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales (Conv. EDH), la circonstance que le requérant bénéficie de l'aide juridictionnelle étant, à cet égard, sans incidence.</ANA>
<ANA ID="9B"> 54-01-05 1) Il résulte de l'article R. 431 1 du code de justice administrative (CJA) que lorsqu'une partie est représentée devant le tribunal administratif ou la cour administrative d'appel par un des mandataires mentionnés à l'article R. 431-2 du même code, c'est-à-dire par un avocat ou par un avocat au Conseil d'Etat et à la Cour de cassation, les actes de procédure, à l'exception de la notification de la décision prévue aux articles R. 751-3 et suivants, ne sont accomplis qu'à l'égard de ce mandataire.... ,,2) Il s'ensuit que l'invitation à confirmer expressément le maintien des conclusions d'une requête en application de l'article R. 612-5-1 du CJA doit être adressée à ce mandataire. Il en va ainsi même lorsque le requérant bénéfice d'un avocat au titre de l'aide juridictionnelle. En l'absence de réponse de l'avocat agissant au titre de l'aide juridictionnelle à l'invitation qui lui a été adressée en application de l'article R. 612-5-1, le requérant est réputé s'être désisté de sa demande, sans qu'il y ait lieu pour la juridiction ni de mettre en demeure l'avocat de répondre à l'invitation qui lui a été adressée, ni d'informer le requérant de ce que l'avocat n'a pas répondu à cette invitation.</ANA>
<ANA ID="9C"> 54-01-08-02 1) Il résulte de l'article R. 431-1 du code de justice administrative (CJA) que lorsqu'une partie est représentée devant le tribunal administratif ou la cour administrative d'appel par un des mandataires mentionnés à l'article R. 431-2 du même code, c'est-à-dire par un avocat ou par un avocat au Conseil d'Etat et à la Cour de cassation, les actes de procédure, à l'exception de la notification de la décision prévue aux articles R. 751-3 et suivants, ne sont accomplis qu'à l'égard de ce mandataire.... ,,2) Il s'ensuit que l'invitation à confirmer expressément le maintien des conclusions d'une requête en application de l'article R. 612-5-1 du CJA doit être adressée à ce mandataire. Il en va ainsi même lorsque le requérant bénéfice d'un avocat au titre de l'aide juridictionnelle. En l'absence de réponse de l'avocat agissant au titre de l'aide juridictionnelle à l'invitation qui lui a été adressée en application de l'article R. 612-5-1, le requérant est réputé s'être désisté de sa demande, sans qu'il y ait lieu pour la juridiction ni de mettre en demeure l'avocat de répondre à l'invitation qui lui a été adressée, ni d'informer le requérant de ce que l'avocat n'a pas répondu à cette invitation.</ANA>
<ANA ID="9D"> 54-05-04-03 1) Il résulte de l'article R. 431-1 du code de justice administrative (CJA) que lorsqu'une partie est représentée devant le tribunal administratif ou la cour administrative d'appel par un des mandataires mentionnés à l'article R. 431-2 du même code, c'est-à-dire par un avocat ou par un avocat au Conseil d'Etat et à la Cour de cassation, les actes de procédure, à l'exception de la notification de la décision prévue aux articles R. 751-3 et suivants, ne sont accomplis qu'à l'égard de ce mandataire.... ,,Il s'ensuit que l'invitation à confirmer expressément le maintien des conclusions d'une requête en application de l'article R. 612-5-1 du CJA doit être adressée à ce mandataire. Il en va ainsi même lorsque le requérant bénéfice d'un avocat au titre de l'aide juridictionnelle. En l'absence de réponse de l'avocat agissant au titre de l'aide juridictionnelle à l'invitation qui lui a été adressée en application de l'article R. 612-5-1, le requérant est réputé s'être désisté de sa demande, sans qu'il y ait lieu pour la juridiction ni de mettre en demeure l'avocat de répondre à l'invitation qui lui a été adressée, ni d'informer le requérant de ce que l'avocat n'a pas répondu à cette invitation.,,,2) Les dispositions de l'article R. 612-5-1 du CJA, prises dans l'objectif de bonne administration de la justice, prévoient, à peine d'irrégularité de la décision constatant le désistement, que la partie concernée doit être expressément invitée à maintenir ses conclusions, doit disposer d'un délai d'au moins un mois pour y procéder et doit être préalablement et régulièrement informée du délai dont elle dispose et des conséquences d'une abstention de sa part. Dans ces conditions, le requérant n'est pas fondé à soutenir que ces dispositions méconnaissent le droit à un recours juridictionnel effectif garanti par les stipulations des articles 6 et 13 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, la circonstance que le requérant bénéficie de l'aide juridictionnelle étant, à cet égard, sans incidence.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 21 février 2000, Ville d'Annecy, n° 196405, p. 67.,,[RJ2] Cf. décision du même jour, Mme,, n° 417855, à mentionner aux Tables.,,[RJ3] Comp., en cas de carence de l'avocat, CE, 28 décembre 2012, M.,, n° 348472, T. p. 927 ; CE, 28 novembre 2008, M.,, n° 292772, p. 444.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
