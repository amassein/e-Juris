<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000018008061</ID>
<ANCIEN_ID>JG_L_2007_12_000000300922</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/18/00/80/CETATEXT000018008061.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 03/12/2007, 300922</TITRE>
<DATE_DEC>2007-12-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>300922</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Martin</PRESIDENT>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE ; SCP BACHELLIER, POTIER DE LA VARDE</AVOCATS>
<RAPPORTEUR>M. Jean-Jacques de Peretti</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Derepas</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 24 janvier 2007 au secrétariat du contentieux du Conseil d'Etat, présentée pour M. Gaspard B, demeurant ... ; M. B demande au Conseil d'Etat :
              
              1°) d'annuler la décision du 22 décembre 2006 par laquelle le tribunal administratif de Nice a refusé de l'autoriser à exercer aux lieu et place de la commune de Solliès&#143;Pont les actions tendant à se constituer partie civile à l'encontre de M. Fabrice A et à engager la responsabilité personnelle de l'auteur du permis de lotir modificatif du 31 octobre 2001 ;
              
              2°) de l'autoriser à exercer ces actions en justice au nom de la commune de Solliès&#143;Pont ;
              
              3°) de mettre à la charge de la commune de Solliès&#143;Pont le versement de la somme de 4 000 euros au titre de  l'article L. 761&#143;1 du code de justice administrative ;
     
	
              Vu les autres pièces du dossier ;
              
              Vu le code de procédure pénale ;
              
              Vu le code général des collectivités territoriales;
              
              Vu le code de justice administrative ;

     
              Après avoir entendu en séance publique :
              
              - le rapport de M. Jean-Jacques de Peretti, Conseiller d'Etat,  
              
              - les observations de la SCP Nicolaÿ, de Lanouvelle, avocat de M. B et de la SCP Bachellier, Potier de la Varde, avocat de la commune de Solliès-Pont, 
              
              - les conclusions de M. Luc Derepas, Commissaire du gouvernement ;
              
              
              
     
     <br/>Considérant qu'aux termes de l'article L. 2132&#143;5 du code général des collectivités territoriales : « Tout contribuable inscrit au rôle de la  commune a le droit d'exercer, tant en demande qu'en défense, à ses frais et risques, avec l'autorisation du tribunal administratif, les actions qu'il croit appartenir à la commune, et que celle-ci, préalablement appelée à en délibérer, a refusé ou négligé d'exercer » ; qu'il appartient au tribunal administratif statuant comme autorité administrative, et au Conseil d'Etat, saisi d'un recours de pleine juridiction dirigé contre la décision du tribunal administratif, lorsqu'ils examinent une demande présentée par un contribuable sur le fondement de ces dispositions, de vérifier, sans se substituer au juge de l'action, et au vu des éléments qui leur sont fournis, que l'action envisagée appartient à la commune, qu'elle présente un intérêt matériel suffisant pour celle&#143;ci et qu'elle a une chance de succès ;
              
              Considérant que M. B a notamment demandé au tribunal administratif de Nice, d'une part, de l'autoriser à déposer plainte avec constitution de partie civile pour le compte de la commune de Solliès&#143;Pont à l'encontre de M. A, alors chef du service de l'urbanisme dans cette commune, du fait de la transformation illégale d'un bâtiment agricole, situé dans le quartier des Bancaous, en local à usage d'habitation et au titre d'infractions au code de l'urbanisme qu'il aurait commises au regard d'une habitation située dans le quartier de La Jonquière et, d'autre part, de l'autoriser à engager une action en responsabilité pour faute personnelle contre l'auteur réel ou apparent d'un permis de lotir modificatif qui aurait été délivré frauduleusement le 31 octobre 2001 ; que, par la décision attaquée du 22 décembre 2006, le tribunal administratif de Nice a rejeté cette demande au motif que les actions envisagées ne présentaient pas d'intérêt suffisant pour la commune de Solliès&#143;Pont ni d'ailleurs de chance raisonnable de succès ;
              
              Sur le moyen tiré de ce que la décision attaquée ne serait pas suffisamment motivée :
              
              Considérant que la décision attaquée énonce les considérations de droit et de fait sur lesquelles elle se fonde ; que le moyen tiré de ce qu'elle ne serait pas suffisamment motivée doit, dès lors, être écarté ;
              
              Au fond :
              
              En ce qui concerne l'action tendant à mettre en cause la responsabilité pour faute personnelle de l'auteur d'un permis de lotir modificatif :
              
              Considérant que les dispositions citées plus haut de l'article L. 2132&#143;5 du code général des collectivités territoriales visent les seules actions en justice appartenant à la commune et que celle-ci refuse ou néglige d'exercer ; que, lorsqu'une collectivité publique estime avoir subi un préjudice en raison de la faute personnelle d'un de ses agents, il lui appartient d'émettre directement, si elle s'y croit fondée, un titre exécutoire à l'effet de fixer le montant des sommes qu'elle estime lui être dues par cet agent, à charge pour ce dernier, s'il conteste son obligation, d'en saisir la juridiction administrative, seule compétente pour en connaître dès lors que les rapports entre une collectivité publique et ses agents sont des rapports de droit public ; qu'il suit de là qu'une action en justice tendant à mettre en cause la responsabilité d'un agent de la commune ne saurait être regardée comme une action appartenant à celle-ci, au sens de l'article L. 2132&#143;5 du code général des collectivités territoriales ;  
              
              Considérant qu'il résulte de ce qui précède que M. B ne saurait être autorisé à engager au nom de la commune, sur le fondement de ces dispositions, une action tendant à mettre en cause la responsabilité pour faute personnelle de l'auteur d'une autorisation de lotir ;
              
              En ce qui concerne les infractions au code de l'urbanisme imputées à un agent communal :
              
              Considérant qu'à l'appui de sa demande d'autorisation, M. B fait valoir que le dépôt d'une plainte avec constitution de partie civile permettrait d'obtenir la démolition des constructions irrégulières et la réparation des préjudices résultant pour la commune des participations d'urbanisme impayées ainsi que de la soustraction d'une parcelle de 5 000 m2 à sa vocation agricole ; 
              
              Considérant, toutefois, qu'il ne résulte pas de l'instruction que la démolition des constructions litigieuses présenterait pour la commune un intérêt matériel, seul susceptible de justifier l'action d'un contribuable sur le fondement des dispositions citées plus haut ; qu'il n'est pas davantage établi que le fait d'avoir soustrait 5 000 m2 de terrain réservé à l'agriculture par la transformation d'un bâtiment agricole en local à usage d'habitation ait causé un  préjudice matériel à la commune ; qu'enfin, s'agissant du préjudice résultant, selon M. B, des participations d'urbanisme impayées, le requérant ne fournit aucun élément permettant d'en apprécier la réalité ;
              
              Considérant qu'il résulte de tout ce qui précède que M. B n'est pas fondé à demander l'annulation de la décision par laquelle le tribunal administratif de Nice a refusé de l'autoriser à engager les actions qu'il envisageait d'exercer au nom de la commune de Solliès&#143;Pont ;
              
              Sur les conclusions tendant à l'application des dispositions de l'article L. 761&#143;1 du code de justice administrative : 
              
              Considérant que ces dispositions font obstacle à ce que soit mise à la charge de la commune de Solliès&#143;Pont, qui n'est pas la partie perdante dans la présente instance, la somme que demande M. B ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions que la commune présente au même titre ;
              
     
     <br/>D E C I D E :
--------------
Article 1er : La requête de M. B est rejetée.
Article 2 : Les conclusions présentées par la commune de Solliès&#143;Pont au titre de l'article L. 761&#143;1 du code de justice administrative sont rejetées.
Article 3 : La présente décision sera notifiée à M. Gaspard B, à M. Fabrice A et à la commune de Solliès&#143;Pont.
Copie en sera adressée pour information au ministre de l'intérieur, de l'outre-mer et des collectivités territoriales.
                 
                 <br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">COLLECTIVITÉS TERRITORIALES. COMMUNE. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. EXERCICE PAR UN CONTRIBUABLE DES ACTIONS APPARTENANT À LA COMMUNE. CONDITIONS DE FOND. - ACTION QUE LA COMMUNE AURAIT ÉTÉ RECEVABLE À EXERCER PAR LA VOIE CONTENTIEUSE - INCLUSION - DÉPÔT DE PLAINTE AVEC CONSTITUTION DE PARTIE CIVILE [RJ1].
</SCT>
<ANA ID="9A">La demande d'un contribuable présentée en application de l'article L. 2132-5 du code général des collectivités territoriales peut avoir pour objet un dépôt de plainte avec constitution civile.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
