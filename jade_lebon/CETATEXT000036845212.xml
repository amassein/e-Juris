<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036845212</ID>
<ANCIEN_ID>JG_L_2018_04_000000402714</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/84/52/CETATEXT000036845212.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 4ème chambres réunies, 06/04/2018, 402714</TITRE>
<DATE_DEC>2018-04-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>402714</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN ; SCP BARADUC, DUHAMEL, RAMEIX</AVOCATS>
<RAPPORTEUR>Mme Dorothée Pradines</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Charles Touboul</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:402714.20180406</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              L'association Nature, aménagement réfléchi, territoire, environnement, culture sauvegardés (NARTECS) a demandé au tribunal administratif de Strasbourg d'annuler pour excès de pouvoir l'arrêté du 3 décembre 2010 par lequel un permis de construire a été délivré à la société Loisium Alsace SAS en vue de la création d'un hôtel et d'un centre d'événements du vin à Voegtlinshoffen (Haut-Rhin). Par un jugement n° 1100547 du 4 novembre 2014, le tribunal administratif de Strasbourg a rejeté la demande de l'association NARTECS.<br/>
<br/>
              Par un arrêt n° 15NC01873 du 10 mars 2016, la cour administrative d'appel de Nancy a rejeté l'appel formé par l'association NARTECS contre le jugement du tribunal administratif de Strasbourg.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 22 août et 22 novembre 2016 au secrétariat du contentieux du Conseil d'Etat, l'association NARTECS demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Voegtlinshoffen et de la société Loisium Alsace le versement de la somme de 3 000 euros à la SCP Nicolaÿ, de Lanouvelle, Hannotin,  son avocat, au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des collectivités territoriales ;<br/>
              - le code de l'urbanisme ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Dorothée Pradines, auditeur, <br/>
<br/>
              - les conclusions de M. Charles Touboul, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, l'association Nature, aménagement réfléchi, territoire, environnement, culture sauvegardés et à la SCP Baraduc, Duhamel, Rameix, avocat de la commune de Voegtlinshoffen ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 21 mars 2018, présentée par l'association NARTECS ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 26 mars 2018, présentée par la commune de Voegtlinshoffen ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges de fond qu'à la suite de l'annulation, par un jugement devenu définitif du tribunal administratif de Strasbourg du 23 juin 2010, du permis de construire délivré le 5 septembre 2009 par le maire de la commune de Voegtlinshoffen à la société Loisium Alsace SAS, au motif que le maire était intéressé à sa délivrance, un nouveau permis de construire un hôtel et un centre d'événements du vin a été délivré à cette société, par un arrêté du 3 décembre 2010 signé de M. Michel Descamps, conseiller municipal spécialement habilité à cet effet par le conseil municipal, en application de l'article L. 422-7 du code de l'urbanisme. Par un jugement du 4 novembre 2014, le tribunal administratif de Strasbourg a rejeté la demande de l'association Nature, aménagement réfléchi, territoire, environnement, culture sauvegardés (NARTECS) tendant à l'annulation de cet arrêté du 3 décembre 2010. Par un arrêt du 10 mars 2016, la cour administrative d'appel de Nancy, après avoir jugé que M. Descamps n'avait pas compétence pour délivrer le permis de construire en litige mais que le permis de construire modificatif délivré par le maire le 9 février 2016, en cours d'instance, avait régularisé ce vice d'incompétence, a annulé le jugement du 4 novembre 2014 en tant seulement qu'il mettait une somme à la charge de l'association NARTECS au titre de l'article L. 761-1 du code de justice administrative et a rejeté le surplus des conclusions de l'appel formé par cette association. L'association NARTECS doit, en conséquence, être regardée comme demandant l'annulation de l'arrêt de la cour administrative d'appel de Nancy en tant que, par son article 3, il a rejeté le surplus de ses conclusions.  <br/>
<br/>
              Sur la régularité de l'arrêt attaqué :<br/>
<br/>
              2. D'une part, aux termes de l'article L. 600-5-1 du code de l'urbanisme : " Le juge administratif qui, saisi de conclusions dirigées contre un permis de construire, de démolir ou d'aménager, estime, après avoir constaté que les autres moyens ne sont pas fondés, qu'un vice entraînant l'illégalité de cet acte est susceptible d'être régularisé par un permis modificatif peut, après avoir invité les parties à présenter leurs observations, surseoir à statuer jusqu'à l'expiration du délai qu'il fixe pour cette régularisation. Si un tel permis modificatif est notifié dans ce délai au juge, celui-ci statue après avoir invité les parties à présenter leurs observations ".<br/>
<br/>
              3. D'autre part, aux termes de l'article L. 5 du code de justice administrative, dans sa rédaction applicable à la procédure devant la cour : " L'instruction des affaires est contradictoire. Les exigences de la contradiction sont adaptées à celles de l'urgence ". Aux termes du premier alinéa de l'article R. 613-2 du même code : " Si le président de la formation de jugement n'a pas pris une ordonnance de clôture, l'instruction est close trois jours francs avant la date de l'audience indiquée dans l'avis d'audience prévu à l'article R. 711-2. Cet avis le mentionne ". Enfin, aux termes du premier alinéa de l'article R. 613-4 de ce code : " Le président de la formation de jugement peut rouvrir l'instruction par une décision qui n'est pas motivée et ne peut faire l'objet d'aucun recours. Cette décision est notifiée dans les mêmes formes que l'ordonnance de clôture ".<br/>
<br/>
              4. Lorsque, pour les besoins de l'instruction, il invite les parties à produire des observations, le juge administratif doit leur laisser un délai suffisant à cette fin, en tenant compte de l'objet des observations demandées. Lorsque l'affaire est déjà inscrite au rôle d'une audience, il lui incombe, si le respect de cette obligation l'exige, soit de rayer l'affaire du rôle, soit de différer la clôture de l'instruction prévue de plein droit, en application de l'article R. 613-2 du code de justice administrative, trois jours francs avant la date de l'audience, en indiquant aux parties quand l'instruction sera close, cette clôture pouvant être reportée au plus tard à la date de l'audience, soit après que les parties ou leurs mandataires ont formulé leurs observations orales, soit, si ces parties sont absentes ou ne sont pas représentées, après l'appel de leur affaire.   <br/>
<br/>
              5. Il ressort des pièces du dossier qu'après que la cour eut invité les parties à faire part de leurs observations en application des dispositions précitées de l'article L. 600-5-1 du code de l'urbanisme afin de permettre la régularisation du vice d'incompétence soulevé contre le permis attaqué, la commune de Voegtlinshoffen a produit, le mercredi 10 février 2016, un permis modificatif supposé régulariser ce vice. Le conseil de l'association requérante a eu connaissance du mémoire de la commune, accompagné du permis modificatif, le jour même à 16 h 30, ainsi qu'en témoignent les mentions de l'application Télérecours. Si une ordonnance de réouverture de l'instruction a été notifiée simultanément, elle ne précisait pas la date de la nouvelle clôture, de sorte que la clôture est intervenue de plein droit le dimanche 14 février à minuit, l'audience étant prévue le 18 février. Eu égard à la nouveauté du document produit par la commune et à l'incidence qu'il était susceptible d'avoir sur l'issue du litige, l'association NARTECS est fondée à soutenir qu'en lui laissant un délai de quatre jours, dont deux jours ouvrés, pour produire ses observations sur la régularisation dont la commune se prévalait, alors même qu'elle avait sollicité le 12 février le report de la date d'audience, la cour a statué au terme d'une procédure irrégulière.<br/>
<br/>
              6. Il résulte de ce qui précède, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que l'article 3 de l'arrêt de la cour administrative d'appel de Nancy doit être annulé.<br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              Sur la recevabilité des conclusions tendant à l'annulation de l'arrêté du 9 février 2016 :<br/>
<br/>
              8. Dans le cas où l'administration lui transmet spontanément un permis modificatif en vue de la régularisation d'un vice de nature à entraîner l'annulation du permis attaqué, le juge peut prendre en considération ce nouvel acte sans être tenu de surseoir à statuer, dès lors qu'il a préalablement invité les parties à présenter leurs observations sur la question de savoir s'il permet une régularisation en application de l'article L. 600-5-1 du code de l'urbanisme. À cette occasion, il appartient à la partie qui poursuit l'annulation du permis initial, si elle s'y croit fondée, de contester la légalité du permis modificatif, ce qu'elle peut faire utilement par des moyens propres et au motif que le permis initial n'était pas régularisable.<br/>
<br/>
              9. En l'espèce, le maire de la commune de Voegtlinshoffen a pris le 9 février 2016, au cours de l'instance d'appel, un permis de construire modificatif en vue de régulariser le permis attaqué par l'association NARTECS et la commune a transmis cet acte à la cour administrative d'appel de Nancy avant que celle-ci ne statue, avant-dire droit ou à titre définitif, sur le permis litigieux. Il résulte de ce qui a été dit ci-dessus que la commune n'est pas fondée à soutenir que l'association NARTECS serait irrecevable à demander, pour la première fois devant le juge d'appel, l'annulation du permis modificatif du 9 février 2016, ni que celui-ci serait devenu définitif faute d'avoir fait l'objet d'un recours pour excès de pouvoir devant le tribunal administratif.   <br/>
<br/>
              Sur la légalité de l'arrêté du 9 février 2016 :<br/>
<br/>
              10. Aux termes de l'article L. 422-1 du code de l'urbanisme : " L'autorité compétente pour délivrer le permis de construire, d'aménager ou de démolir et pour se prononcer sur un projet faisant l'objet d'une déclaration préalable est : / a) Le maire, au nom de la commune, dans les communes qui se sont dotées d'un plan local d'urbanisme ou d'un document d'urbanisme en tenant lieu (...) ". Aux termes de l'article L. 422-7 du même code : " Si le maire ou le président de l'établissement public de coopération intercommunale est intéressé au projet faisant l'objet de la demande de permis ou de la déclaration préalable, soit en son nom personnel, soit comme mandataire, le conseil municipal de la commune ou l'organe délibérant de l'établissement public désigne un autre de ses membres pour prendre la décision ". Si le maire est, en vertu des dispositions précitées de l'article L 422-1 du code de l'urbanisme, compétent pour délivrer une autorisation d'urbanisme, une telle autorisation peut également être compétemment délivrée, réserve faite des délégations accordées dans les conditions prévues par le code général des collectivités territoriales ou de l'application des règles de suppléance, par un membre du conseil municipal légalement désigné par celui-ci en application de l'article  L. 422-7 du même code au motif que le maire peut être légitimement regardé comme intéressé au projet devant faire l'objet de l'autorisation.<br/>
<br/>
              11. En l'espèce, ainsi qu'il a été dit au point 1, le tribunal administratif de Strasbourg a, par un jugement devenu définitif du 23 juin 2010, annulé pour excès de pouvoir le permis de construire délivré le 5 septembre 2009 par le maire de la commune de Voegtlinshoffen à la société Loisium Alsace SAS en vue de la réalisation d'un hôtel et d'un centre d'événements du vin, au motif que M. B...A..., maire de la commune, devait être regardé comme intéressé à la délivrance de l'autorisation litigieuse au sens de l'article L. 422-7 du code de l'urbanisme. En l'absence de changement dans les  circonstances de droit ou de fait, l'association NARTECS est fondée à soutenir que l'arrêté du 9 février 2016, par lequel M. A...a accordé un permis de régularisation à la même société pour le même projet, a été pris en méconnaissance de l'autorité absolue de chose jugée qui s'attache tant au dispositif  du jugement du 23 juin 2010 qu'au motif qui en constitue le support nécessaire. Cet arrêté doit, par suite, être annulé, sans qu'il soit besoin d'examiner les autres moyens soulevés contre lui.    <br/>
<br/>
              Sur la légalité de l'arrêté du 3 décembre 2010 :<br/>
<br/>
              En ce qui concerne l'incompétence du signataire de l'arrêté du 3 décembre 2010 : <br/>
<br/>
              12. Le maire de la commune devant être regardé comme intéressé au projet pour lequel l'autorisation d'urbanisme était demandée, l'association requérante n'est pas fondée à soutenir que M. Descamps, conseiller municipal désigné par le conseil municipal en application de l'article L. 422-7 du code de l'urbanisme, n'avait pas compétence pour signer le permis délivré le 3 décembre 2010 à la même société et pour le même projet.<br/>
<br/>
              En ce qui concerne l'exception d'illégalité de la délibération du 28 mai 2009 portant révision du plan d'occupation des sols de la commune :<br/>
<br/>
              13. En premier lieu, il est loisible à l'autorité compétente de modifier le plan d'occupation des sols après l'enquête publique, sous réserve, d'une part, que ne soit pas remise en cause l'économie générale du projet et, d'autre part, que cette modification procède de l'enquête publique. <br/>
<br/>
              14. À ce titre, le conseil municipal peut prendre en considération des observations faites, au cours de l'enquête publique, par des personnes propriétaires de parcelles sur le territoire de la commune ou intéressées pour d'autres motifs aux règles d'urbanisme envisagées, dès lors qu'il n'en résulte ni atteinte à l'intérêt général, ni méconnaissance des règles d'urbanisme, ni remise en cause de l'économie générale du projet. Ainsi, contrairement à ce que soutient l'association requérante, et en l'absence de manoeuvre destinée à fausser l'objet même de l'enquête publique, la circonstance que des modifications aient, en l'espèce, été apportées au projet de révision du plan d'occupation des sols de la commune de Voegtlinshoffen, qui avait pour objet de classer en zone constructible les terrains d'assiette du projet de la société Loisium Alsace SAS, à la suite d'observations formulées le dernier jour de l'enquête publique par une personne intéressée par ce projet, est, par elle-même, sans incidence sur la légalité de la révision adoptée. <br/>
<br/>
              15. En second lieu, si la convention par laquelle une autorité investie d'un pouvoir réglementaire prend l'engagement de faire usage de ce pouvoir dans un sens déterminé a un objet illicite, un acte réglementaire adopté après la signature d'une telle convention n'est pas illégal de ce seul fait, si, ayant  été pris dans le but d'intérêt général pour lequel le pouvoir réglementaire a été conféré à cette autorité et non pour la mise en oeuvre de la convention,  il ne procède d'aucun détournement de pouvoir.<br/>
<br/>
              16. Il ressort des pièces du dossier qu'aux termes d'une convention conclue le 26 mai 2008 avec la société Loisium Alsace SAS, la commune s'est engagée à " procéder à ses frais aux études nécessaires à la révision simplifiée de son plan d'occupation des sols afin d'adapter les règles de celui-ci pour rendre possible la réalisation du projet " vini-viticole hôtelier que souhaitait développer la société ainsi qu'à acquérir les terrains de l'assiette foncière du projet pour les rétrocéder au promoteur et à réaliser la desserte routière. Par la même convention, le promoteur s'est engagé à réaliser son projet, à rembourser le coût net de la desserte routière et " à proposer, en tant que condition essentielle à la réalisation du projet, à la clientèle du complexe, tout au long de l'exploitation de celui-ci, au moins 50 % de vins provenant des deux communes de Voegtlinshoffem et Obermorschwihr ", sélectionnés sous l'égide d'un comité devant comprendre des représentants des deux communes et des instances représentatives de la profession et " sur une base qualitative et d'une représentation équitable des différents producteurs ou entreprises (coopératives) concernées ". À supposer que la commune puisse être ainsi regardée comme s'étant, de façon illicite, engagée par convention à modifier le classement des terrains d'assiette du projet dans le zonage de son plan d'occupation des sols, il ressort en l'espèce des pièces du dossier, notamment du rapport de présentation de ce document, que la révision du plan d'occupation des sols de la commune a été adoptée, par une délibération du 28 mai 2009, dans le but d'intérêt général de favoriser le développement économique et touristique local et non pour respecter une obligation de mettre en oeuvre la convention du 26 mai 2008. La délibération du 28 mai 2009 n'était donc pas entachée de détournement de pouvoir et l'association NARTECS n'est pas fondée à soutenir qu'elle était illégale. <br/>
<br/>
              17. Il en résulte que le moyen tiré de ce que le permis de construire litigieux aurait été délivré sous l'empire d'un document d'urbanisme illégal et méconnaîtrait les dispositions du plan d'occupation des sols applicables antérieurement à la délibération du 28 mai 2009 doit être écarté.<br/>
<br/>
              18. Il résulte de tout ce qui précède que l'association NARTECS n'est pas fondée à soutenir que c'est à tort que, par le jugement attaqué, qui, en tout état de cause, n'est pas entaché de contradiction de motifs, le tribunal administratif de Strasbourg a rejeté sa demande tendant à l'annulation de l'arrêté du 3 décembre 2010.<br/>
<br/>
              Sur les frais liés au litige :<br/>
<br/>
              19. Les dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 font obstacle à ce qu'une somme soit mise, à ce titre, à la charge de la commune de Voegtlinshoffen et de la société Loisium Alsace, qui ne sont pas, dans la présente instance, les parties perdantes. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par la commune de Voegtlinshoffen au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
                             --------------<br/>
<br/>
Article 1er : L'article 3 de l'arrêt de la cour administrative d'appel de Nancy du 10 mars 2016 est annulé.<br/>
Article 2 : L'arrêté du maire de Voegtlinshoffen du 9 février 2016 est annulé.<br/>
Article 3 : Les conclusions de la requête présentée par l'association NARTECS devant la cour administrative d'appel de Nancy dirigées contre le jugement du tribunal administratif de Strasbourg du 4 novembre 2014 en tant qu'il a rejeté ses conclusions tendant à l'annulation de l'arrêté du 3 décembre 2010 sont rejetées.<br/>
Article 4 : Les conclusions de l'association NARTECS présentées au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 sont rejetées.<br/>
Article 5 : Les conclusions de la commune de Voegtlinshoffen présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 6 : La présente décision sera notifiée à l'association Nature, aménagement réfléchi, territoire, environnement, culture sauvegardés et à la commune de Voegtlinshoffen.<br/>
         Copie en sera adressée à la société Loisium Alsace SAS.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">135-02-01-02-02-03-01 COLLECTIVITÉS TERRITORIALES. COMMUNE. ORGANISATION DE LA COMMUNE. ORGANES DE LA COMMUNE. MAIRE ET ADJOINTS. POUVOIRS DU MAIRE. ATTRIBUTIONS EXERCÉES AU NOM DE LA COMMUNE. - AUTORITÉ COMPÉTENTE POUR DÉLIVRER LES AUTORISATIONS D'URBANISME (ART. L. 422-1 DU CODE DE L'URBANISME) - CAS DANS LEQUEL LE MAIRE PEUT ÊTRE LÉGITIMEMENT REGARDÉ COMME INTÉRESSÉ AU PROJET DEVANT FAIRE L'OBJET D'UNE AUTORISATION - MEMBRE DU CONSEIL MUNICIPAL LÉGALEMENT DÉSIGNÉ PAR CELUI-CI (ART. L. 422-7 DU CGCT), RÉSERVE FAITE DES DÉLÉGATIONS ACCORDÉES OU DE RÈGLES DE SUPPLÉANCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">39-04-01 MARCHÉS ET CONTRATS ADMINISTRATIFS. FIN DES CONTRATS. NULLITÉ. - CONTRAT PAR LEQUEL UNE AUTORITÉ INVESTIE D'UN POUVOIR RÉGLEMENTAIRE S'ENGAGE À EN USER DANS UN CERTAIN SENS - OBJET ILLICITE - EXISTENCE - CONSÉQUENCE - ILLÉGALITÉ DE L'ACTE RÉGLEMENTAIRE ADOPTÉ APRÈS LA SIGNATURE DE CE CONTRAT - ABSENCE DE CE SEUL FAIT, S'IL NE PROCÈDE D'AUCUN DÉTOURNEMENT DE POUVOIR [RJ3].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-04-01-02 PROCÉDURE. INSTRUCTION. POUVOIRS GÉNÉRAUX D'INSTRUCTION DU JUGE. DÉLAIS D'INSTRUCTION. - PRODUCTION D'OBSERVATIONS À LA DEMANDE DU JUGE - 1) OBLIGATION POUR LE JUGE DE LAISSER AUX PARTIES UN DÉLAI SUFFISANT - CAS D'UNE AFFAIRE DÉJÀ INSCRITE AU RÔLE D'UNE AUDIENCE - OBLIGATION DE RAYER L'AFFAIRE OU DE DIFFÉRER LA CLÔTURE DE L'INSTRUCTION AU PLUS TARD JUSQU'À LA DATE D'AUDIENCE, SOUS RÉSERVE D'EN INFORMER LES PARTIES - 2) ESPÈCE - PRODUCTION D'OBSERVATIONS EN APPLICATION DE L'ARTICLE L. 600-5-1 DU CODE DE L'URBANISME - DÉLAI INSUFFISANT.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">68-03-03 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PERMIS DE CONSTRUIRE. LÉGALITÉ INTERNE DU PERMIS DE CONSTRUIRE. - 1) RÉGULARISATION D'UN VICE ENTRAÎNANT L'ILLÉGALITÉ D'UN PERMIS DE CONSTRUIRE, DE DÉMOLIR OU D'AMÉNAGER (ART. L. 600-1-5 DU CODE DE L'URBANISME) - CAS D'UNE TRANSMISSION SPONTANÉE PAR L'ADMINISTRATION D'UN PERMIS MODIFICATIF - FACULTÉ POUR LE JUGE DE LE PRENDRE EN CONSIDÉRATION SANS SURSEOIR À STATUER - EXISTENCE DÈS LORS QU'IL A INVITÉ LES PARTIES À PRODUIRE LEURS OBSERVATIONS SUR LA RÉGULARISATION [RJ1] - FACULTÉ POUR LA PARTIE DEMANDANT L'ANNULATION DU PERMIS INITIAL DE CONTESTER LA LÉGALITÉ DU PERMIS MODIFICATIF PAR DES MOYENS PROPRES ET AU MOTIF QU'IL N'EST PAS RÉGULARISABLE - EXISTENCE [RJ2] - 2) AUTORITÉ COMPÉTENTE POUR DÉLIVRER LES AUTORISATIONS D'URBANISME (ART. L. 422-1 DU CODE DE L'URBANISME) - CAS DANS LEQUEL LE MAIRE PEUT ÊTRE LÉGITIMEMENT REGARDÉ COMME INTÉRESSÉ AU PROJET DEVANT FAIRE L'OBJET D'UNE AUTORISATION (ART. L. 422-7 DU MÊME CODE) - MEMBRE DU CONSEIL MUNICIPAL LÉGALEMENT DÉSIGNÉ PAR CELUI-CI, RÉSERVE FAITE DES DÉLÉGATIONS ACCORDÉES OU DE RÈGLES DE SUPPLÉANCE.
</SCT>
<ANA ID="9A"> 135-02-01-02-02-03-01 Si le maire est, en vertu de l'article L. 422-1 du code de l'urbanisme, compétent pour délivrer une autorisation d'urbanisme, une telle autorisation peut également être compétemment délivrée, réserve faite des délégations accordées dans les conditions prévues par le code général des collectivités territoriales ou de l'application des règles de suppléance, par un membre du conseil municipal légalement désigné par celui-ci en application de l'article  L. 422-7 du même code au motif que le maire peut être légitimement regardé comme intéressé au projet devant faire l'objet de l'autorisation.</ANA>
<ANA ID="9B"> 39-04-01 Si la convention par laquelle une autorité investie d'un pouvoir réglementaire prend l'engagement de faire usage de ce pouvoir dans un sens déterminé a un objet illicite, un acte réglementaire adopté après la signature d'une telle convention n'est pas illégal de ce seul fait, si, ayant  été pris dans le but d'intérêt général pour lequel le pouvoir réglementaire a été conféré à cette autorité et non pour la mise en oeuvre de la convention, il ne procède d'aucun détournement de pouvoir.</ANA>
<ANA ID="9C"> 54-04-01-02 1) Lorsque, pour les besoins de l'instruction, il invite les parties à produire des observations, le juge administratif doit leur laisser un délai suffisant à cette fin, en tenant compte de l'objet des observations demandées. Lorsque l'affaire est déjà inscrite au rôle d'une audience, il lui incombe, si le respect de cette obligation l'exige, soit de rayer l'affaire du rôle, soit de différer la clôture de l'instruction prévue de plein droit, en application de l'article R. 613-2 du code de justice administrative (CJA), trois jours francs avant la date de l'audience, en indiquant aux parties quand l'instruction sera close, cette clôture pouvant être reportée au plus tard à la date de l'audience, soit après que les parties ou leurs mandataires ont formulé leurs observations orales, soit, si ces parties sont absentes ou ne sont pas représentées, après l'appel de leur affaire.,,,2) Cour ayant invité les parties à faire part de leurs observations en application de l'article L. 600-5-1 du code de l'urbanisme afin de permettre la régularisation du vice d'incompétence soulevé contre le permis attaqué. Commune ayant produit un permis modificatif supposé régulariser ce vice, dont l'association a eu connaissance le jour même.,,,Eu égard à la nouveauté du document produit par la commune et à l'incidence qu'il était susceptible d'avoir sur l'issue du litige, l'association requérante est fondée à soutenir qu'en lui laissant un délai de quatre jours, dont deux jours ouvrés, à compter de la production du permis modificatif pour produire ses observations sur la régularisation dont la commune se prévalait, alors même qu'elle avait sollicité deux jours après cette production le report de la date d'audience, la cour a statué au terme d'une procédure irrégulière.</ANA>
<ANA ID="9D"> 68-03-03 1) Dans le cas où l'administration lui transmet spontanément un permis modificatif en vue de la régularisation d'un vice de nature à entraîner l'annulation du permis attaqué, le juge peut prendre en considération ce nouvel acte sans être tenu de surseoir à statuer, dès lors qu'il a préalablement invité les parties à présenter leurs observations sur la question de savoir s'il permet une régularisation en application de l'article L. 600-5-1 du code de l'urbanisme. À cette occasion, il appartient à la partie qui poursuit l'annulation du permis initial, si elle s'y croit fondée, de contester la légalité du permis modificatif, ce qu'elle peut faire utilement par des moyens propres et au motif que le permis initial n'était pas régularisable.,,,2) Si le maire est, en vertu de l'article L 422-1 du code de l'urbanisme, compétent pour délivrer une autorisation d'urbanisme, une telle autorisation peut également être compétemment délivrée, réserve faite des délégations accordées dans les conditions prévues par le code général des collectivités territoriales ou de l'application des règles de suppléance, par un membre du conseil municipal légalement désigné par celui-ci en application de l'article  L. 422-7 du même code au motif que le maire peut être légitimement regardé comme intéressé au projet devant faire l'objet de l'autorisation.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., s'agissant de la procédure de régularisation sur le fondement de l'article L. 600-9 du code de l'urbanisme, CE, Section, 22 décembre 2017, Commune de Sempy, n° 395963, à publier au Recueil., ,[RJ2] Cf. CE, 19 juin 2017, Syndicat des copropriétaires de la résidence Butte Stendhal et autres, n° 398531, à mentionner aux Tables.,,[RJ3] Rappr. CE, 9 juillet 2015, Football Club des Girondins de Bordeaux et autres, n°s 375542 375543, p. 239.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
