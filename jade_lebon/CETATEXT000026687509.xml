<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026687509</ID>
<ANCIEN_ID>JG_L_2012_11_000000354108</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/68/75/CETATEXT000026687509.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème et 7ème sous-sections réunies, 26/11/2012, 354108, Publié au recueil Lebon</TITRE>
<DATE_DEC>2012-11-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>354108</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème et 7ème sous-sections réunies</FORMATION>
<TYPE_REC/>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN ; SCP DELVOLVE, DELVOLVE</AVOCATS>
<RAPPORTEUR>Mme Airelle Niepce</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Béatrice Bourgeois-Machureau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:354108.20121126</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 16 novembre 2011 et 15 février 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour Mme Brigitte B, demeurant ... ; Mme B demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement n° 0809428 du 15 septembre 2011 par lequel le tribunal administratif de Cergy-Pontoise a rejeté sa demande tendant à l'annulation pour excès de pouvoir de la décision du 2 juin 2008 par laquelle le directeur des ressources humaines de l'agence Ile-de-France Ouest de France Télécom a refusé de la réintégrer dans le département des Pyrénées-Orientales à l'issue de sa disponibilité pour convenance personnelle et ne lui a proposé qu'un emploi à Cergy pour être réintégrée, ainsi que de la décision du 10 juillet 2008 rejetant son recours gracieux, et à ce qu'il soit enjoint à France Télécom de lui proposer un emploi dans le département des Pyrénées-Orientales ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ; <br/>
<br/>
              3°) de mettre à la charge de France Télécom le versement de la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi n° 83-634 du 13 juillet 1983 ;<br/>
<br/>
              Vu la loi n° 84-16 du 11 janvier 1984 ;<br/>
<br/>
              Vu la loi n° 90-568 du 2 juillet 1990 ;<br/>
<br/>
              Vu le décret n° 85-986 du 16 septembre 1985 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Airelle Niepce, Maître des requêtes en service extraordinaire, <br/>
<br/>
              - les observations de la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de Mme Brigitte B, et de la SCP Delvolvé, Delvolvé, avocat de France Télécom,<br/>
<br/>
              - les conclusions de Mme Béatrice Bourgeois-Machureau, Rapporteur public,<br/>
<br/>
              La parole ayant à nouveau été donnée à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de Mme Brigitte B, et à la SCP Delvolvé, Delvolvé, avocat de France Télécom ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que, pour rejeter le recours pour excès de pouvoir formé par Mme B à l'encontre de la décision de France Télécom ayant refusé de lui proposer un emploi dans le département des Pyrénées-Orientales pour y être réintégrée à l'issue de la période de disponibilité dans laquelle elle avait été placée, en application de l'article 47 du décret du 16 septembre 1985 relatif au régime particulier de certaines positions des fonctionnaires de l'Etat, pour suivre son conjoint affecté dans ce département, le tribunal administratif de Cergy-Pontoise s'est notamment fondé sur le motif qu'il " appartient à Mme B, demandeur dans la présente affaire, de prouver que (...) des postes étaient disponibles dans ce département " et que l'intéressée " n'apporte pas la preuve qui lui incombe qu'à la date de la décision attaquée des postes étaient disponibles " ; <br/>
<br/>
              2. Mais considérant qu'il appartient au juge de l'excès de pouvoir de former sa conviction sur les points en litige au vu des éléments versés au dossier par les parties ; que s'il peut écarter des allégations qu'il jugerait insuffisamment étayées, il ne saurait exiger de l'auteur du recours que ce dernier apporte la preuve des faits qu'il avance ; que, le cas échéant, il revient au juge, avant de se prononcer sur une requête assortie d'allégations sérieuses non démenties par les éléments produits par l'administration en défense, de mettre en oeuvre ses pouvoirs généraux d'instruction des requêtes et de prendre toutes mesures propres à lui procurer, par les voies de droit, les éléments de nature à lui permettre de former sa conviction, en particulier en exigeant de l'administration compétente la production de tout document susceptible de permettre de vérifier les allégations du demandeur ;<br/>
<br/>
              3. Considérant, dès lors, qu'en faisant supporter à Mme B la charge de la preuve de l'existence d'emplois vacants dans le département dans lequel elle demandait à être réintégrée, le tribunal administratif de Cergy-Pontoise a commis une erreur de droit ; que, par suite, Mme B est fondée, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, à demander l'annulation du jugement attaqué ;<br/>
<br/>
              4. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de Mme B qui n'est pas, dans la présente instance, la partie perdante ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de France Télécom une somme de 3 000 euros à verser à Mme B au titre des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le jugement du tribunal administratif de Cergy-Pontoise du 15 septembre 2011 est annulé.<br/>
<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Cergy-Pontoise.<br/>
<br/>
Article 3 : La société France Télécom versera à Mme B la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : Les conclusions de France Télécom tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
<br/>
Article 5 : La présente décision sera notifiée à Mme Brigitte B et à France Télécom.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-04-01 PROCÉDURE. INSTRUCTION. POUVOIRS GÉNÉRAUX D'INSTRUCTION DU JUGE. - EXCÈS DE POUVOIR - FORMATION DE LA CONVICTION DU JUGE - A) CHARGE DE LA PREUVE - ABSENCE - B) MISE EN &#140;UVRE DES POUVOIRS D'INSTRUCTION DU JUGE - CONDITIONS.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-04-04 PROCÉDURE. INSTRUCTION. PREUVE. - EXCÈS DE POUVOIR - FORMATION DE LA CONVICTION DU JUGE - A) CHARGE DE LA PREUVE - ABSENCE - B) MISE EN &#140;UVRE DES POUVOIRS D'INSTRUCTION DU JUGE - CONDITIONS.
</SCT>
<ANA ID="9A"> 54-04-01 Il appartient au juge de l'excès de pouvoir de former sa conviction sur les points en litige au vu des éléments versés au dossier par les parties. a) S'il peut écarter des allégations qu'il jugerait insuffisamment étayées, il ne saurait exiger de l'auteur du recours que ce dernier apporte la preuve des faits qu'il avance. b) Le cas échéant, il revient au juge, avant de se prononcer sur une requête assortie d'allégations sérieuses non démenties par les éléments produits par l'administration en défense, de mettre en oeuvre ses pouvoirs généraux d'instruction des requêtes et de prendre toutes mesures propres à lui procurer, par les voies de droit, les éléments de nature à lui permettre de former sa conviction, en particulier en exigeant de l'administration compétente la production de tout document susceptible de permettre de vérifier les allégations du demandeur.</ANA>
<ANA ID="9B"> 54-04-04 Il appartient au juge de l'excès de pouvoir de former sa conviction sur les points en litige au vu des éléments versés au dossier par les parties. a) S'il peut écarter des allégations qu'il jugerait insuffisamment étayées, il ne saurait exiger de l'auteur du recours que ce dernier apporte la preuve des faits qu'il avance. b) Le cas échéant, il revient au juge, avant de se prononcer sur une requête assortie d'allégations sérieuses non démenties par les éléments produits par l'administration en défense, de mettre en oeuvre ses pouvoirs généraux d'instruction des requêtes et de prendre toutes mesures propres à lui procurer, par les voies de droit, les éléments de nature à lui permettre de former sa conviction, en particulier en exigeant de l'administration compétente la production de tout document susceptible de permettre de vérifier les allégations du demandeur.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
