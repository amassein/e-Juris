<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027064731</ID>
<ANCIEN_ID>JG_L_2013_02_000000349738</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/06/47/CETATEXT000027064731.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 1ère sous-sections réunies, 13/02/2013, 349738</TITRE>
<DATE_DEC>2013-02-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>349738</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 1ère sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MONOD, COLIN</AVOCATS>
<RAPPORTEUR>Mme Sophie Roussel</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:349738.20130213</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 30 mai 2011 au secrétariat du contentieux du Conseil d'Etat, présenté par le ministre de l'intérieur, de l'outre mer, des collectivités locales et de l'immigration ; le ministre demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 09MA02258 du 22 mars 2011 par lequel la cour administrative d'appel de Marseille a, sur la demande de Mme A...B..., d'une part, annulé le jugement n° 0701973 du 8 avril 2009 du tribunal administratif de Montpellier, la décision du 19 janvier 2006 du préfet de l'Hérault refusant de renouveler son titre de séjour et la décision implicite rejetant son recours hiérarchique du 9 mars 2006, d'autre part, enjoint au préfet de l'Hérault de délivrer à Mme B...un titre de séjour temporaire portant la mention "vie privée et familiale", <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la requête de Mme B...devant la cour administrative d'appel de Marseille ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention européenne des droits de l'homme et des libertés fondamentales,<br/>
<br/>
              Vu le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
<br/>
              Vu la loi n° 91-647 du 10 juillet 1991 ;<br/>
<br/>
              Vu l'arrêté du 8 juillet 1999 relatif aux conditions d'établissement des avis médicaux concernant les étrangers malades prévus à l'article 7-5 du décret n° 46-1574 du 30 juin 1946 modifié ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie Roussel, Auditeur,  <br/>
<br/>
              - les observations de la SCP Monod, Colin, avocat de MmeB...,<br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Monod, Colin, avocat de Mme B... ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 19 janvier 2006, le préfet de l'Hérault a rejeté la demande d'admission au séjour en France présentée par Mme A...B..., ressortissante marocaine ; que, par un arrêt du 22 mars 2011, la cour administrative d'appel de Marseille a annulé, à la demande de MmeB..., le jugement du tribunal administratif de Montpellier du 8 avril 2009 et la décision du 19 janvier 2006 du préfet de l'Hérault refusant de renouveler son titre de séjour ; que le ministre de l'intérieur se pourvoit en cassation contre cet arrêt ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 313-11 du code de l'entrée et du séjour des étrangers et du droit d'asile, dans sa rédaction applicable au litige : " Sauf si sa présence constitue une menace pour l'ordre public, la carte de séjour temporaire portant la mention " vie privée et familiale " est délivrée de plein droit:/ (...) 11° A l'étranger résidant habituellement en France dont l'état de santé nécessite une prise en charge médicale dont le défaut pourrait entraîner pour lui des conséquences d'une exceptionnelle gravité, sous réserve qu'il ne puisse effectivement bénéficier d'un traitement approprié dans le pays dont il est originaire. La décision de délivrer la carte de séjour est prise par l'autorité administrative, après avis du médecin inspecteur de santé publique compétent au regard du lieu de résidence de l'intéressé ou, à Paris, du médecin, chef du service médical de la préfecture de police " ; qu'aux termes de l'article 4 de l'arrêté du 8 juillet 1999 relatif aux conditions d'établissement des avis médicaux concernant les étrangers malades prévus à l'article 7-5 du décret n° 46-1574 du 30 juin 1946 modifié : "(...) le médecin inspecteur de santé publique de la direction départementale des affaires sanitaires et sociales émet un avis précisant:/ - si l'état de santé de l'étranger nécessite ou non une prise en charge médicale ;/- si le défaut de cette prise en charge peut ou non entraîner des conséquences d'une exceptionnelle gravité sur son état de santé ;/- si l'intéressé peut effectivement ou non bénéficier d'un traitement approprié dans le pays dont il est originaire ;/- et la durée prévisible du traitement./ Il indique, en outre, si l'état de santé de l'étranger lui permet de voyager sans risque vers son pays de renvoi " ; <br/>
<br/>
              3. Considérant que ces dispositions ont pour objet de permettre au préfet, auquel il incombe de prendre en considération les modalités d'exécution d'une éventuelle mesure d'éloignement dès le stade de l'examen de la demande de titre de séjour présentée sur le fondement des dispositions rappelées ci-dessus du 11° de l'article L. 313-11 du code de l'entrée et du séjour des étrangers et du droit d'asile, de disposer d'une information complète sur l'état de santé d'un étranger malade, y compris sur sa capacité à voyager sans risque à destination de son pays d'origine ; que l'absence de l'indication prévue à l'article 4 de l'arrêté du 8 juillet 1999 quant à la possibilité pour un étranger malade de voyager sans risque vers son pays d'origine ne met pas l'autorité préfectorale à même de se prononcer de manière éclairée sur la situation de  cet étranger ; que, par suite, sauf s'il ressort des autres éléments du dossier que l'état de santé de l'étranger malade ne suscite pas d'interrogation sur sa capacité à supporter le voyage vers son pays d'origine, l'omission de l'indication en cause entache d'irrégularité la procédure suivie et partant affecte la légalité de l'arrêté pris à sa suite ;<br/>
<br/>
              4. Considérant que, pour annuler le jugement du tribunal administratif de Montpellier en date du 8 avril 2009 et l'arrêté du préfet de l'Hérault du 19 janvier 2006 refusant à Mme B...la délivrance d'un titre de séjour sur le fondement du 11° de l'article L. 313-11 du code de l'entrée et du séjour des étrangers et du droit d'asile, la cour administrative d'appel de Marseille s'est fondée sur le fait que l'avis rendu par le médecin inspecteur de santé publique ne comportait pas d'indication sur la possibilité pour l'intéressée de voyager sans risque vers son pays d'origine, alors qu'il ne ressortait pas des pièces du dossier soumis aux juges du fond que son état de santé lui permettait de supporter un tel voyage ; qu'ainsi, contrairement à ce que soutient le ministre de l'intérieur, en retenant que l'omission dont était entaché l'avis médical pouvait être utilement invoquée pour contester la légalité du refus de titre de séjour et en retenant qu'en l'espèce, le refus de titre de séjour avait été pris au vu d'une procédure irrégulière et était entaché d'illégalité, la cour administrative d'appel n'a pas commis d'erreur de droit ; que, contrairement à ce que soutient également le ministre, la cour n'a, en tout état de cause, pas commis d'erreur de droit ni dénaturé les faits de l'espèce en retenant, au surplus, que le préfet s'était à tort approprié l'avis du médecin inspecteur en ce qui concerne l'absence de gravité exceptionnelle des conséquences d'un défaut de prise en charge de la pathologie dont était atteinte Mme B... ;<br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que le ministre de l'intérieur n'est pas fondé à demander l'annulation de l'arrêt qu'il attaque ;<br/>
<br/>
              6. Considérant que Mme B...a obtenu le bénéfice de l'aide juridictionnelle ; que, par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ; qu'il y a lieu, dans les circonstances de l'espèce, et sous réserve que la SCP Monod-Colin, avocat de MmeB..., renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de l'Etat la somme de 2 000 euros à verser à cette société ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi du ministre de l'intérieur est rejeté.<br/>
<br/>
Article 2 : L'Etat versera à la SCP Alain Monod-Bertrand Colin, avocat de MmeB..., une somme de 2 000 euros en application des dispositions du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
<br/>
Article 3 : La présente décision sera notifiée au ministre de l'intérieur et à Madame A...B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">335-01-02-02-01 ÉTRANGERS. SÉJOUR DES ÉTRANGERS. AUTORISATION DE SÉJOUR. OCTROI DU TITRE DE SÉJOUR. DÉLIVRANCE DE PLEIN DROIT. - ETRANGER MALADE - DÉLIVRANCE DE PLEIN DROIT D'UNE CARTE VIE PRIVÉE ET FAMILIALE (11° DE L'ART. L. 313-11 DU CESEDA) - NÉCESSITÉ, POUR LE PRÉFET, DE PRENDRE EN CONSIDÉRATION LES MODALITÉS D'EXÉCUTION D'UNE ÉVENTUELLE MESURE D'ÉLOIGNEMENT DÈS L'EXAMEN DE LA DEMANDE - CONSÉQUENCE - ABSENCE, DANS L'AVIS DU MÉDECIN INSPECTEUR, D'INDICATION RELATIVE À LA CAPACITÉ DE L'ÉTRANGER À VOYAGER SANS RISQUE VERS SON PAYS D'ORIGINE (ART. DE L'ARRÊTÉ DU 8 JUILLET 1999) - IRRÉGULARITÉ DE LA PROCÉDURE D'EXAMEN DE LA DEMANDE, SAUF ABSENCE DE DOUTE SUR LA CAPACITÉ DE L'ÉTRANGER À SUPPORTER LE VOYAGE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">335-01-03-01 ÉTRANGERS. SÉJOUR DES ÉTRANGERS. REFUS DE SÉJOUR. QUESTIONS GÉNÉRALES. - REFUS DE DÉLIVRANCE D'UNE CARTE VIE PRIVÉE ET FAMILIALE À UN ÉTRANGER MALADE (11° DE L'ART. L. 313-11 DU CESEDA) - ABSENCE, DANS L'AVIS DU MÉDECIN INSPECTEUR, D'INDICATION APPORTÉE AU PRÉFET SUR LA CAPACITÉ DE L'ÉTRANGER À VOYAGER SANS RISQUE VERS SON PAYS D'ORIGINE (ART. DE L'ARRÊTÉ DU 8 JUILLET 1999) - CONSÉQUENCE - ILLÉGALITÉ DU REFUS DE TITRE, SAUF ABSENCE DE DOUTE SUR LA CAPACITÉ DE L'ÉTRANGER À SUPPORTER LE VOYAGE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-07-01-04-03 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. QUESTIONS GÉNÉRALES. MOYENS. MOYENS INOPÉRANTS. - ABSENCE - CONTESTATION D'UN REFUS DE TITRE DE SÉJOUR VIE PRIVÉE ET FAMILIALE À UN ÉTRANGER MALADE - MOYEN TIRÉ DE L'ABSENCE D'INDICATION, SUR L'AVIS MÉDICAL DU MÉDECIN INSPECTEUR, DE LA CAPACITÉ DE L'ÉTRANGER À SUPPORTER LE VOYAGE EN CAS D'ÉLOIGNEMENT.
</SCT>
<ANA ID="9A"> 335-01-02-02-01 Les dispositions de l'article 4 de l'arrêté du 8 juillet 1999 relatif aux conditions d'établissement des avis médicaux concernant les étrangers malades prévus à l'article 7-5 du décret n° 46-1574 du 30 juin 1946 modifié ont pour objet de permettre au préfet, auquel il incombe de prendre en considération les modalités d'exécution d'une éventuelle mesure d'éloignement dès le stade de l'examen d'une demande de titre de séjour vie privée et familiale présentée sur le fondement des dispositions du 11° de l'article L. 313-11 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA) par un étranger malade, de disposer d'une information complète sur l'état de santé de l'étranger, y compris sur sa capacité à voyager sans risque à destination de son pays d'origine. L'absence de l'indication prévue à l'article 4 de l'arrêté du 8 juillet 1999 relative à la possibilité pour l'étranger de voyager sans risque vers son pays d'origine ne met pas, en principe, l'autorité préfectorale à même de se prononcer de manière éclairée sur la situation de l'étranger. Par suite, sauf s'il ressort des autres éléments du dossier que l'état de santé de l'étranger malade ne suscite pas d'interrogation sur sa capacité à supporter le voyage vers son pays d'origine, l'omission de l'indication en cause entache d'irrégularité la procédure suivie et partant affecte la légalité de l'arrêté pris à sa suite.</ANA>
<ANA ID="9B"> 335-01-03-01 Les dispositions de l'article 4 de l'arrêté du 8 juillet 1999 relatif aux conditions d'établissement des avis médicaux concernant les étrangers malades prévus à l'article 7-5 du décret n° 46-1574 du 30 juin 1946 modifié ont pour objet de permettre au préfet, auquel il incombe de prendre en considération les modalités d'exécution d'une éventuelle mesure d'éloignement dès le stade de l'examen d'une demande de titre de séjour vie privée et familiale présentée sur le fondement des dispositions du 11° de l'article L. 313-11 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA) par un étranger malade, de disposer d'une information complète sur l'état de santé de l'étranger, y compris sur sa capacité à voyager sans risque à destination de son pays d'origine. L'absence de l'indication prévue à l'article 4 de l'arrêté du 8 juillet 1999 relative à la possibilité pour l'étranger de voyager sans risque vers son pays d'origine ne met pas, en principe, l'autorité préfectorale à même de se prononcer de manière éclairée sur la situation de l'étranger. Par suite, sauf s'il ressort des autres éléments du dossier que l'état de santé de l'étranger malade ne suscite pas d'interrogation sur sa capacité à supporter le voyage vers son pays d'origine, l'omission de l'indication en cause entache d'irrégularité la procédure suivie et partant affecte la légalité de l'arrêté pris à sa suite.</ANA>
<ANA ID="9C"> 54-07-01-04-03 Le moyen tiré de ce que l'avis médical du médecin inspecteur ne comporterait pas l'indication, prévue à l'article 4 de l'arrêté du 8 juillet 1999 relatif aux conditions d'établissement des avis médicaux concernant les étrangers malades, relative à la possibilité pour l'étranger de voyager sans risque vers son pays d'origine, est opérant à l'encontre du refus de délivrance à un étranger malade d'un titre de séjour vie privée et familiale sur le fondement des dispositions du 11° de l'article L. 313-11 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA).</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
