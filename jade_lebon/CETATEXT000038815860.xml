<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038815860</ID>
<ANCIEN_ID>JG_L_2019_07_000000428552</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/81/58/CETATEXT000038815860.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 4ème chambres réunies, 24/07/2019, 428552</TITRE>
<DATE_DEC>2019-07-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>428552</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ ; SCP FABIANI, LUC-THALER, PINATEL</AVOCATS>
<RAPPORTEUR>M. Thibaut Félix</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Charles Touboul</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:428552.20190724</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au juge des référés du tribunal administratif de Lille, sur le fondement de l'article L. 521-1 du code de justice administrative, de suspendre l'exécution de la décision du 10 octobre 2018 par laquelle le directeur général de l'établissement public foncier Nord Pas-de-Calais a exercé le droit de préemption urbain sur un ensemble immobilier situé 56 rue de Lille à Estaires. Par une ordonnance n° 1810600 du 10 décembre 2018, le juge des référés du tribunal administratif de Lille a suspendu l'exécution de cette décision en tant qu'elle permet à l'établissement public foncier Nord Pas-de-Calais de prendre possession du bien et d'en disposer ou d'en user dans des conditions qui rendraient la préemption irréversible.<br/>
<br/>
              La société civile immobilière Madeleine a demandé au juge des référés du tribunal administratif de Lille de réformer l'ordonnance n° 1810600 du 10 décembre 2018 et de suspendre l'exécution, en tous ses effets, de la décision du 10 octobre 2018 du directeur général de l'établissement public foncier Nord Pas-de-Calais. Par une ordonnance n° 1811929 du 15 février 2019, cette demande a été rejetée. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un nouveau mémoire, enregistrés les 1er mars, 18 mars et 17 avril 2019 au secrétariat du contentieux du Conseil d'Etat, la SCI Madeleine demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance du 15 février 2019 ;<br/>
<br/>
              2°) statuant en référé, de faire droit à sa demande ; <br/>
<br/>
              3°) de mettre à la charge de l'établissement public foncier Nord Pas-de-Calais la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Thibaut Félix, auditeur,  <br/>
<br/>
              - les conclusions de M. Charles Touboul, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de la SCI Madeleine et à la SCP Fabiani, Luc-Thaler, Pinatel, avocat de la société établissement public foncier Nord Pas-de-Calais ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge des référés du tribunal administratif de Lille que la SCI Madeleine a conclu avec M. B...A..., le 1er août 2018, un compromis de vente portant sur un ensemble immobilier situé 56, rue de Lille, à Estaires, dont elle est propriétaire. Par une décision du 10 octobre 2018, le directeur général de l'établissement public foncier Nord Pas-de-Calais a exercé sur cet immeuble, par délégation de la commune d'Estaires, le droit de préemption urbain. M. A...a demandé au tribunal administratif de Lille d'annuler pour excès de pouvoir cette décision et au juge des référés de ce tribunal d'en suspendre l'exécution. Par une ordonnance du 10 décembre 2018, le juge des référés du tribunal administratif de Lille en a suspendu l'exécution, en tant seulement que cette décision permet à l'établissement public foncier Nord Pas-de-Calais de prendre possession du bien et d'en disposer ou d'en user dans des conditions qui rendraient la préemption irréversible. La SCI Madeleine se pourvoit en cassation contre l'ordonnance du 15 février 2019 par laquelle le juge des référés a rejeté sa demande tendant à la réformation de l'ordonnance du 10 décembre 2018 et à la suspension de l'exécution de tous les effets de la décision du 10 octobre 2018 du directeur général de l'établissement public foncier Nord Pas-de-Calais.<br/>
<br/>
              2. Aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". Aux termes de l'article L. 521-4 du même code : " Saisi par toute personne intéressée, le juge des référés peut, à tout moment, au vu d'un élément nouveau, modifier les mesures qu'il avait ordonnées ou y mettre fin ".<br/>
<br/>
              3. Lorsqu'il est saisi d'un recours pour excès de pouvoir contre une décision de préemption, le tribunal administratif doit appeler dans l'instance la personne publique qui a exercé le droit de préemption ainsi que, sauf à ce que l'un ou l'autre soit lui-même l'auteur du recours, l'acquéreur évincé et le vendeur du bien préempté. Il en va de même lorsque le juge des référés de ce tribunal est saisi d'une demande de suspension de l'exécution d'une telle décision. Si le défaut d'une telle communication n'affecte pas la régularité du jugement, il est toutefois loisible à l'acquéreur évincé ou au vendeur, si le juge des référés a ordonné la suspension de l'exécution de la décision de préemption ou de certains de ses effets, de le saisir d'une demande tendant à ce qu'il modifie les mesures qu'il a ordonnées ou y mette fin, dans les conditions prévues par l'article L. 521-4 du code de justice administrative.<br/>
<br/>
              4. La saisine du juge des référés, par toute personne intéressée, sur le fondement de l'article L. 521-4 du code de justice administrative, n'est pas subordonnée à l'introduction d'une requête en annulation ou en réformation de la décision initiale, cette exigence ne s'imposant qu'à l'auteur d'une demande, présentée sur le fondement de l'article L. 521-1 du même code, tendant à la suspension de l'exécution d'une décision administrative.<br/>
<br/>
              5. Par suite, en jugeant que la SCI Madeleine n'était pas recevable à demander au juge des référés du tribunal administratif de Lille de modifier les mesures ordonnées par son ordonnance du 10 décembre 2018, au seul motif qu'elle n'avait pas préalablement introduit de requête à fin d'annulation de la décision du 10 octobre 2018 du directeur général de l'établissement public foncier Nord Pas-de-Calais, le juge des référés du tribunal administratif de Lille a commis une erreur de droit. <br/>
<br/>
              6. Il en résulte que la SCI Madeleine est fondée à demander l'annulation de l'ordonnance qu'elle attaque. <br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au titre de la procédure de référé engagée, en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              8. Il résulte de l'instruction que, par décision du 10 octobre 2018, le directeur général de l'établissement public foncier Nord Pas-de-Calais a exercé le droit de préemption urbain sur le bien mis en vente par la SCI Madeleine aux prix et conditions mentionnés par la déclaration d'intention d'aliéner. Toutefois, à la date du 10 décembre 2018 à laquelle l'exécution de cette décision a été suspendue par ordonnance du juge des référés du tribunal administratif de Lille, le transfert de propriété n'était pas encore intervenu, faute que soient remplies les deux conditions tenant à l'intervention du paiement et de l'acte authentique, exigées par l'article L. 213-14 du code de l'urbanisme. La SCI Madeleine fait valoir que la suspension prononcée par le juge des référés, en tant seulement que la décision de préemption permet à l'établissement public foncier de prendre possession du bien et d'en disposer ou d'en user dans des conditions qui rendraient irréversible cette décision, lui interdit, jusqu'au jugement de la requête au fond, de vendre l'immeuble pour lequel elle avait trouvé un acquéreur, tant à ce dernier qu'à l'établissement public foncier, cette suspension étant regardée comme un obstacle au paiement au sens de l'article L. 213-14 du code de l'urbanisme. Elle demande au juge des référés, sur le fondement de l'article L. 521-4 du code de justice administrative, ainsi qu'elle est recevable à le faire en sa qualité de personne intéressée, de modifier les mesures prononcées et de suspendre l'exécution de la décision du 10 octobre 2018 en tous ses effets pour, notamment, permettre la vente du bien à M.A....<br/>
<br/>
              Sur l'existence de moyens propres à créer un doute sérieux sur la légalité de la décision de préemption :<br/>
<br/>
              9. Par son ordonnance du 10 décembre 2018, le juge des référés du tribunal administratif de Lille a retenu qu'était propre à créer un doute sérieux quant à la légalité de la décision litigieuse le moyen tiré de ce que la demande de communication de documents formée par l'établissement public foncier Nord Pas-de-Calais le 6 septembre 2018, alors que le droit de préemption ne lui avait pas encore été délégué pour certaines des parcelles considérées, n'avait pu suspendre, en application de l'article L. 213-2 du code de l'urbanisme, le délai de deux mois pendant lequel pouvait être exercé le droit de préemption, de sorte que le bien ne pouvait plus être légalement préempté à la date du 10 octobre 2018. Si l'établissement public foncier Nord Pas-de-Calais soutient que ce moyen n'est pas de nature à créer un doute sérieux quant à la légalité de la décision de préemption litigieuse, aucun élément ne conduit à remettre en cause cette appréciation.<br/>
<br/>
              10. La SCI Madeleine soutient également que la décision du 10 octobre 2018 par laquelle le maire d'Estaires a délégué à l'établissement public foncier le droit de préemption sur ces parcelles, reçue en préfecture le 11 octobre suivant, n'était pas entrée en vigueur lorsque l'établissement public foncier a pris la décision de préemption en litige le 10 octobre. Ce moyen paraît, en l'état de l'instruction, également propre à créer un doute sérieux quant à la légalité de cette décision. <br/>
<br/>
              11. Pour l'application de l'article L. 600-4-1 du code de l'urbanisme, en l'état du dossier soumis au Conseil d'Etat, aucun autre moyen n'est susceptible d'entraîner la suspension de l'exécution de la décision attaquée.<br/>
<br/>
              Sur la portée de la suspension prononcée :<br/>
<br/>
              12. Aux termes de l'article L. 213-14 du code de l'urbanisme, dans sa rédaction issue de la loi du 24 mars 2014 pour l'accès au logement et un urbanisme rénové : " En cas d'acquisition d'un bien par voie de préemption (...), le transfert de propriété intervient à la plus tardive des dates auxquelles seront intervenus le paiement et l'acte authentique. / Le prix d'acquisition est payé ou, en cas d'obstacle au paiement, consigné dans les quatre mois qui suivent soit la décision d'acquérir le bien au prix indiqué par le vendeur ou accepté par lui, soit la décision définitive de la juridiction compétente en matière d'expropriation, soit la date de l'acte ou du jugement d'adjudication. / En cas de non-respect du délai prévu au deuxième alinéa du présent article, le vendeur peut aliéner librement son bien (...) ".<br/>
<br/>
              13. Lorsque le juge des référés prend, sur le fondement des dispositions de l'article L. 521-1 du code de justice administrative, une mesure de suspension de l'exécution d'une décision de préemption après l'intervention du transfert de propriété au profit de la collectivité publique qui a exercé le droit de préemption, cette suspension a pour effet, ainsi qu'il doit en principe le préciser, d'empêcher cette collectivité de disposer du bien ainsi que d'en user dans des conditions qui rendraient difficilement réversible la décision de préemption, sous réserve cependant qu'à cette date la collectivité n'en ait pas déjà disposé - par exemple par la revente du bien à un tiers - de telle sorte que ces mesures seraient devenues sans objet. <br/>
<br/>
              14. Lorsque le juge des référés prend une mesure de suspension de l'exécution d'une décision de préemption avant l'intervention du transfert de propriété, comme en l'espèce, faute que soient remplies les deux conditions mentionnées par l'article L. 213-14 du code de l'urbanisme, cette suspension a en principe pour effet de faire obstacle au transfert de propriété du bien préempté au bénéfice de cette collectivité et à la prise de possession du bien. Toutefois, le juge des référés, qui doit prendre en considération les incidences de la suspension pour l'ensemble des personnes intéressées, tout en préservant les intérêts du futur propriétaire, quel qu'il soit, peut notamment suspendre la décision de préemption en tant seulement qu'elle permet à la collectivité publique de disposer du bien et d'en user dans des conditions qui rendraient difficilement réversible la décision de préemption, en précisant alors que son ordonnance ne fait pas obstacle à la signature de l'acte authentique et au paiement du prix d'acquisition, ou au contraire la suspendre en tant qu'elle fait obstacle à la vente au bénéfice de l'acquéreur initial, à ses risques et périls et, le cas échéant, sous les mêmes réserves relatives à la disposition et à l'usage du bien.<br/>
<br/>
              15. La SCI Madeleine demande que la décision de préemption soit suspendue dans tous ses effets, en faisant valoir que le bien préempté constitue son seul patrimoine, qu'elle n'en tire plus aucune recette depuis la fin du premier trimestre 2017, compte tenu du départ de son dernier locataire, et qu'elle doit assumer les réparations et l'entretien du bâtiment ainsi que le coût de la taxe foncière jusqu'au jugement de l'affaire au fond, sans pouvoir percevoir le montant de la vente avant cette date. Toutefois, elle n'apporte pas d'éléments de nature à établir que les incidences financières de la suspension ordonnée seraient d'une importance telle qu'elles justifieraient de permettre, avant même que le tribunal administratif de Lille se soit prononcé au fond sur la demande d'annulation de la décision de préemption en litige, la vente du bien préempté à M.A.... De même, les besoins de son activité invoqués par M.A..., qui pourraient être satisfaits dans le cadre d'un bail précaire, ne justifient pas que cette vente puisse être faite avant le jugement au fond. <br/>
<br/>
              16. Par suite, la SCI Madeleine et M. A...ne sont pas fondés à demander la modification des mesures ordonnées par l'ordonnance du juge des référés du tribunal administratif de Lille du 10 décembre 2018.  <br/>
<br/>
              17. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de la SCI Madeleine la somme que l'établissement public foncier Nord Pas-de-Calais demande au titre des frais exposés par lui tant devant le Conseil d'Etat que devant le juge des référés du tribunal administratif de Lille. Les dispositions de l'article L. 761-1 font obstacle à ce qu'il soit fait droit aux conclusions de la SCI Madeleine et de M. A...présentées au même titre.<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : L'ordonnance du 15 février 2019 du juge des référés du tribunal administratif de Lille est annulée.<br/>
Article 2 : La demande présentée par la SCI Madeleine devant le juge des référés du tribunal administratif de Lille est rejetée. <br/>
Article 3 : Les conclusions présentées par la SCI Madeleine, par M. A...et par l'établissement public foncier Nord Pas-de-Calais au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à la société civile immobilière Madeleine, à l'établissement public foncier Nord Pas-de-Calais et M. B...A.... <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-035-01-04 PROCÉDURE. PROCÉDURES INSTITUÉES PAR LA LOI DU 30 JUIN 2000. QUESTIONS COMMUNES. CONCLUSIONS SUSCEPTIBLES D'ÊTRE PRÉSENTÉES DANS LE CADRE DE L'INSTANCE EN RÉFÉRÉ. - JUGE SAISI D'UN RÉFÉRÉ-SUSPENSION CONTRE UNE DÉCISION DE PRÉEMPTION - POSSIBILITÉ POUR LE VENDEUR N'AYANT PAS ÉTÉ APPELÉ DANS L'INSTANCE, EN CAS DE SUSPENSION DE LA DÉCISION DE PRÉEMPTION, DE SAISIR LE JUGE DES RÉFÉRÉS D'UNE DEMANDE DE MODIFICATION DES MESURES ORDONNÉES (ART. L. 521-4 DU CJA) - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-04 PROCÉDURE. INSTRUCTION. - JUGE SAISI D'UN REP OU D'UN RÉFÉRÉ-SUSPENSION CONTRE UNE DÉCISION DE PRÉEMPTION - 1) OBLIGATION D'APPELER DANS L'INSTANCE LE VENDEUR DU BIEN PRÉEMPTÉ - EXISTENCE - 2) CONSÉQUENCE EN CAS DE MÉCONNAISSANCE DE CETTE OBLIGATION - INFLUENCE SUR LA RÉGULARITÉ DE LA DÉCISION JURIDICTIONNELLE - ABSENCE - POSSIBILITÉ POUR LE VENDEUR, EN CAS DE SUSPENSION DE LA DÉCISION DE PRÉEMPTION, DE SAISIR LE JUGE DES RÉFÉRÉS D'UNE DEMANDE DE MODIFICATION DES MESURES ORDONNÉES (ART. L. 521-4 DU CJA) - EXISTENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-07-01-07 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. QUESTIONS GÉNÉRALES. DEVOIRS DU JUGE. - JUGE SAISI D'UN REP OU D'UN RÉFÉRÉ-SUSPENSION CONTRE UNE DÉCISION DE PRÉEMPTION - 1) OBLIGATION D'APPELER DANS L'INSTANCE LE VENDEUR DU BIEN PRÉEMPTÉ - EXISTENCE - 2) CONSÉQUENCE EN CAS DE MÉCONNAISSANCE DE CETTE OBLIGATION - INFLUENCE SUR LA RÉGULARITÉ DE LA DÉCISION JURIDICTIONNELLE - ABSENCE - POSSIBILITÉ POUR LE VENDEUR, EN CAS DE SUSPENSION DE LA DÉCISION DE PRÉEMPTION, DE SAISIR LE JUGE DES RÉFÉRÉS D'UNE DEMANDE DE MODIFICATION DES MESURES ORDONNÉES (ART. L. 521-4 DU CJA) - EXISTENCE.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">68-02-01-01 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PROCÉDURES D'INTERVENTION FONCIÈRE. PRÉEMPTION ET RÉSERVES FONCIÈRES. DROITS DE PRÉEMPTION. - JUGE SAISI D'UN REP OU D'UN RÉFÉRÉ-SUSPENSION CONTRE UNE DÉCISION DE PRÉEMPTION - 1) OBLIGATION D'APPELER DANS L'INSTANCE LE VENDEUR DU BIEN PRÉEMPTÉ - EXISTENCE - 2) CONSÉQUENCE EN CAS DE MÉCONNAISSANCE DE CETTE OBLIGATION - INFLUENCE SUR LA RÉGULARITÉ DE LA DÉCISION JURIDICTIONNELLE - ABSENCE - POSSIBILITÉ POUR LE VENDEUR, EN CAS DE SUSPENSION DE LA DÉCISION DE PRÉEMPTION, DE SAISIR LE JUGE DES RÉFÉRÉS D'UNE DEMANDE DE MODIFICATION DES MESURES ORDONNÉES (ART. L. 521-4 DU CJA) - EXISTENCE.
</SCT>
<ANA ID="9A"> 54-035-01-04 Lorsqu'il est saisi d'un recours pour excès de pouvoir contre une décision de préemption, le tribunal administratif doit appeler dans l'instance la personne publique qui a exercé le droit de préemption ainsi que, sauf à ce que l'un ou l'autre soit lui-même l'auteur du recours, l'acquéreur évincé et le vendeur du bien préempté. Il en va de même lorsque le juge des référés de ce tribunal est saisi d'une demande de suspension de l'exécution d'une telle décision.... ...Si l'absence de cette communication est sans influence sur la régularité du jugement, il est toutefois loisible à l'acquéreur évincé ou au vendeur, si le juge des référés a ordonné la suspension de l'exécution de la décision de préemption ou de certains de ses effets, de le saisir d'une demande tendant à ce qu'il modifie les mesures qu'il a ordonnées ou y mette fin, dans les conditions prévues par l'article L. 521-4 du code de justice administrative (CJA).</ANA>
<ANA ID="9B"> 54-04 1) Lorsqu'il est saisi d'un recours pour excès de pouvoir contre une décision de préemption, le tribunal administratif doit appeler dans l'instance la personne publique qui a exercé le droit de préemption ainsi que, sauf à ce que l'un ou l'autre soit lui-même l'auteur du recours, l'acquéreur évincé et le vendeur du bien préempté. Il en va de même lorsque le juge des référés de ce tribunal est saisi d'une demande de suspension de l'exécution d'une telle décision.... ,,2) Si l'absence de cette communication est sans influence sur la régularité du jugement, il est toutefois loisible à l'acquéreur évincé ou au vendeur, si le juge des référés a ordonné la suspension de l'exécution de la décision de préemption ou de certains de ses effets, de le saisir d'une demande tendant à ce qu'il modifie les mesures qu'il a ordonnées ou y mette fin, dans les conditions prévues par l'article L. 521-4 du code de justice administrative (CJA).</ANA>
<ANA ID="9C"> 54-07-01-07 1) Lorsqu'il est saisi d'un recours pour excès de pouvoir contre une décision de préemption, le tribunal administratif doit appeler dans l'instance la personne publique qui a exercé le droit de préemption ainsi que, sauf à ce que l'un ou l'autre soit lui-même l'auteur du recours, l'acquéreur évincé et le vendeur du bien préempté. Il en va de même lorsque le juge des référés de ce tribunal est saisi d'une demande de suspension de l'exécution d'une telle décision.... ,,2) Si l'absence de cette communication est sans influence sur la régularité du jugement, il est toutefois loisible à l'acquéreur évincé ou au vendeur, si le juge des référés a ordonné la suspension de l'exécution de la décision de préemption ou de certains de ses effets, de le saisir d'une demande tendant à ce qu'il modifie les mesures qu'il a ordonnées ou y mette fin, dans les conditions prévues par l'article L. 521-4 du code de justice administrative (CJA).</ANA>
<ANA ID="9D"> 68-02-01-01 1) Lorsqu'il est saisi d'un recours pour excès de pouvoir contre une décision de préemption, le tribunal administratif doit appeler dans l'instance la personne publique qui a exercé le droit de préemption ainsi que, sauf à ce que l'un ou l'autre soit lui-même l'auteur du recours, l'acquéreur évincé et le vendeur du bien préempté. Il en va de même lorsque le juge des référés de ce tribunal est saisi d'une demande de suspension de l'exécution d'une telle décision.... ,,2) Si l'absence de cette communication est sans influence sur la régularité du jugement, il est toutefois loisible à l'acquéreur évincé ou au vendeur, si le juge des référés a ordonné la suspension de l'exécution de la décision de préemption ou de certains de ses effets, de le saisir d'une demande tendant à ce qu'il modifie les mesures qu'il a ordonnées ou y mette fin, dans les conditions prévues par l'article L. 521-4 du code de justice administrative (CJA).</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
