<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034158715</ID>
<ANCIEN_ID>JG_L_2017_03_000000406387</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/15/87/CETATEXT000034158715.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 08/03/2017, 406387</TITRE>
<DATE_DEC>2017-03-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>406387</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>CORLAY ; SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Stéphane Hoynck</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:406387.20170308</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              Procédure contentieuse antérieure : <br/>
<br/>
              L'association Promouvoir, M. et Mme G...A..., Mme E...F..., Mme B...H..., Mme D...C...et l'association Action pour la dignité humaine ont demandé au juge des référés du tribunal administratif de Paris de suspendre, sur le fondement de l'article L. 521-1 du code de justice administrative, l'exécution de la décision 29 septembre 2016 par laquelle la ministre de la culture et de la communication a délivré un visa d'exploitation au film " Sausage Party " assorti d'une interdiction de représentation aux mineurs de douze ans, à titre principal, en totalité et, à titre subsidiaire, en tant que ce visa n'interdit pas le film aux moins de seize ans et, à titre infiniment subsidiaire, en tant que ce visa n'est pas accompagné d'un avertissement.<br/>
<br/>
              L'association Juristes Pour l'Enfance a demandé au juge des référés du même tribunal :<br/>
<br/>
              1°) de suspendre, sur le fondement des dispositions de l'article L. 521-1 du code de justice administrative, l'exécution des décisions par lesquelles la ministre de la culture et de la communication a délivré deux visas d'exploitation au film " Sausage Party " assortis d'une interdiction de représentation aux mineurs de douze ans, un pour la version originale et un pour la version française, en tant que les décisions n'assortissent pas les visas d'une interdiction aux mineurs de seize ans ; <br/>
              2°) de mettre à la charge de l'Etat la somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Par une ordonnance n°s 1620779/9, 1620839/9 du 14 décembre 2016, le juge des référés du tribunal administratif a rejeté les deux demandes.<br/>
<br/>
              Procédures devant le Conseil d'État :<br/>
<br/>
              1° Sous le n° 406387, par un pourvoi sommaire et un mémoire complémentaire, enregistrés au secrétariat du contentieux du Conseil d'Etat les 28 décembre 2016 et 11 janvier 2017, l'association Juristes Pour l'Enfance demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette ordonnance ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande de première instance ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat une somme de 3500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
              2° Sous le n° 406524, par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 2 et 18 janvier et 24 février 2017, au secrétariat du contentieux du Conseil d'Etat, l'association Promouvoir et l'association Action pour la Dignité humaine demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette ordonnance ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leurs conclusions de première instance;<br/>
<br/>
              3°) de mettre à la charge de l'Etat une somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - la convention internationale relative aux droits de l'enfant, signée à New-York le 26 janvier 1990 ;<br/>
              - le code du cinéma et de l'image animée ;<br/>
              - le code de l'action sociale et des familles ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Stéphane Hoynck, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Corlay, avocat de l'association Juristes pour l'Enfance, à la SCP Piwnica, Molinié, avocat de la ministre de la culture et de la communication et à la SCP Gaschignard, avocat de l'Association Promouvoir  et de l'Association Action Pour La Dignité Humaine ;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Les pourvois de l'association Juristes Pour l'Enfance et des associations Promouvoir et Action pour la Dignité Humaine sont dirigés contre la même ordonnance. Il y a lieu de les joindre pour statuer par une seule décision.<br/>
<br/>
              2. Les associations requérantes ont demandé au juge des référés du tribunal administratif de Paris de suspendre l'exécution des décisions par lesquelles la ministre de la culture et de la communication a délivré deux visas d'exploitation au film " Sausage Party " assortis d'une interdiction de représentation aux mineurs de douze ans, un pour la version originale et un pour la version française. Elles se pourvoient contre l'ordonnance du 14 décembre 2016 rejetant leurs demandes. <br/>
<br/>
              En ce qui concerne le défaut de visionnage, par la commission de classification, de la version française du film : <br/>
<br/>
              3. Le premier alinéa de l'article R. 211-2 du code du cinéma et de l'image animée dispose que : " L'exploitation d'une oeuvre ou d'un document doublé en langue française est subordonnée à la délivrance d'un visa d'exploitation cinématographique distinct de celui délivré pour l'exploitation de l'oeuvre ou du document dans la version originale ". L'article R. 211-3 du même code prévoit qu'à l'appui d'une demande de visa d'exploitation cinématographique est remis " le découpage dialogué sous sa forme intégrale et définitive " ainsi que, s'agissant d'une demande de visa d'une oeuvre ou d'un document étranger en version originale : " (...)  a) Le texte et la traduction juxtalinéaire en français du titre ou du dialogue et, le cas échéant, des sous-titres de la version originale ; / b) Le texte des sous-titres français de la version exploitée en France ". <br/>
<br/>
              4. En application des articles R. 211-4 à R. 211-6 du code du cinéma et de l'image animée, les comités de classification mentionnés à l'article R. 211-27 visionnent les oeuvres ou les documents, en vue d'établir un rapport au président de la commission de classification mentionnée à l'article R. 211-29 du même code. Lorsque deux au moins des membres du comité de classification proposent une interdiction particulière de représentation, accompagnée ou non d'un avertissement, ou le refus de visa, le président de la commission de classification inscrit l'oeuvre ou le document à l'ordre du jour de celle-ci. L'article R. 211-7 du même code dispose que : " Saisie par son président dans les conditions prévues à l'article R. 211-6, la commission de classification visionne les oeuvres ou documents, en vue de rendre un avis au ministre chargé de la culture ". <br/>
<br/>
              5. Il résulte de ces dispositions que l'exploitation d'une oeuvre à la fois en version originale et en version doublée en langue française nécessite la délivrance d'un visa d'exploitation pour chaque format. Dans pareille hypothèse, la commission de classification, si elle est consultée, doit rendre un avis de nature à éclairer le ministre chargé de la culture sur chacun des visas qu'il doit délivrer. Dès lors, ainsi que le prévoient les dispositions précitées, qu'elle dispose, lors du visionnage de l'oeuvre en version originale, du découpage dialogué sous sa forme intégrale et définitive en français, la commission n'est pas tenue, à peine d'irrégularité de la procédure, de visionner séparément chacun des formats soumis à son avis.<br/>
<br/>
              6. Il ne ressort cependant pas des pièces du dossier soumis au juge des référés que la commission de classification, lorsqu'elle a visionné le film " Sausage party " en version originale, ait disposé du découpage dialogué de la version doublée en français. Ce faisant, et alors qu'il n'est pas soutenu qu'elle aurait visionné le film en version doublée en français, elle n'a pas été mise à même d'apprécier les spécificités de la version doublée par rapport à la version originale sous-titrée. Dans ces conditions, le juge des référés a, eu égard à son office, commis une erreur de droit en estimant que la circonstance que la commission de classification aurait visionné seulement la version originale du film pour rendre son avis sur chacun des formats faisant l'objet d'une demande de visa, ne paraissait pas, en l'état de l'instruction, susceptible d'avoir exercé une influence sur le sens de la décision prise ou d'avoir privé les intéressés d'une garantie. Les associations requérantes sont, par suite, fondées à demander l'annulation de l'ordonnance attaquée en tant qu'elle rejette leur demande de suspension du visa accordé à la version doublée en français du film " Sausage party ".<br/>
<br/>
              En ce qui concerne l'absence d'avertissement accompagnant la délivrance du visa concernant la version originale sous-titrée du film : <br/>
<br/>
              7. L'article R. 211-13 du code du cinéma et de l'image animée dispose, dans sa rédaction applicable aux visas litigieux : " Sans préjudice de la mesure de classification qui accompagne sa délivrance, le visa d'exploitation cinématographique peut être assorti d'un avertissement, destiné à l'information du spectateur, portant sur le contenu ou les particularités de l'oeuvre ou du document concerné ". Pour écarter le moyen tiré de ce que le visa litigieux devait être assorti d'un tel avertissement, le juge des référés a estimé que le public était suffisamment informé du contenu du film et des éléments qu'il comporte susceptibles de choquer les plus jeunes du fait, d'une part, de l'interdiction aux moins de douze ans, exceptionnelle s'agissant d'un film d'animation, d'autre part, des conditions de diffusion du film, en particulier en raison de la nature du titre et de l'affiche du film ainsi que du contenu de la bande annonce diffusée avant sa sortie. En se fondant sur de tels éléments pour juger que l'information du spectateur sur les particularités de l'oeuvre n'exigeait pas que le visa délivré à la version originale sous-titrée du film soit assorti d'un avertissement, le juge des référés a, eu égard à son office, commis une nouvelle erreur de droit. <br/>
<br/>
              8. Il résulte de ce qui précède que, sans qu'il soit besoin d'examiner les autres moyens de leurs pourvois, les associations requérantes sont fondées, à demander l'annulation de l'ordonnance attaquée.<br/>
<br/>
              9. Il y a lieu, dans les circonstances de l'espèce, de régler les affaires au titre de la procédure de référé, en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              10. Il ne ressort pas des pièces du dossier que le film litigieux ferait encore, à la date de la présente ordonnance, l'objet de diffusion en salle. Dans ces conditions, les faits de l'espèce ne caractérisent plus une situation d'urgence, au sens des dispositions de l'article L. 521-1 du code de justice administrative. Il résulte de ce qui précède que les conclusions des requêtes tendant à la suspension de l'exécution des visas d'exploitation attribués au film " Sausage party " ne peuvent qu'être rejetées ainsi que, par voie de conséquence, celles présentées, par les associations requérantes, au titre de l'article L. 761-1 du code de justice administrative. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées au même titre par la ministre de la culture.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du juge des référés du tribunal administratif de Paris du 14 décembre 2016 est annulée.<br/>
Article 2 : Les demandes présentées par les associations Juristes Pour l'Enfance, Promouvoir et Action pour la Dignité Humaine devant le juge des référés du tribunal administratif de Paris et le surplus de leurs conclusions présentées devant le Conseil d'Etat sont rejetés.<br/>
Article 3 : Les conclusions de la ministre de la culture présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 4 : La présente décision sera notifiée aux associations Juristes Pour l'Enfance, Promouvoir et Action pour la Dignité Humaine et à la ministre de la culture et de la communication.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">09-05-01 ARTS ET LETTRES. CINÉMA. - MESURES DE CLASSIFICATION ACCOMPAGNANT UN VISA D'EXPLOITATION CINÉMATOGRAPHIQUE (ART. R. 211-12 DU CODE DU CINÉMA ET DE L'IMAGE ANIMÉE) - 1) EXPLOITATION D'UNE OEUVRE EN VERSION ORIGINALE ET EN VERSION DOUBLÉE EN LANGUE FRANÇAISE - NÉCESSITÉ DE DÉLIVRANCE D'UN VISA D'EXPLOITATION POUR CHAQUE FORMAT - POSSIBILITÉ POUR LA COMMISSION DE CLASSIFICATION DE NE PAS VISIONNER SÉPARÉMENT CHACUN DES FORMATS DÈS LORS QU'ELLE DISPOSE DU DÉCOUPAGE DIALOGUÉ SOUS SA FORME INTÉGRALE ET DÉFINITIVE EN LANGUE FRANÇAISE - EXISTENCE - 2) NÉCESSITÉ D'ASSORTIR LE VISA D'EXPLOITATION D'UN AVERTISSEMENT (ART. R. 211-13 DU CODE DU CINÉMA ET DE L'IMAGE ANIMÉE) - APPRÉCIATION - ELÉMENTS À PRENDRE EN COMPTE - INTERDICTION DU FILM AUX MOINS DE DOUZE ANS ET CONDITIONS DE DIFFUSION DE CE FILM - ABSENCE.
</SCT>
<ANA ID="9A"> 09-05-01 1) Il résulte des articles R. 211-2 à R. 211-7 du code du cinéma et de l'image animée que l'exploitation d'une oeuvre à la fois en version originale et en version doublée en langue française nécessite la délivrance d'un visa d'exploitation pour chaque format. Dans pareille hypothèse, la commission de classification, si elle est consultée, doit rendre un avis de nature à éclairer le ministre chargé de la culture sur chacun des visas qu'il doit délivrer. Dès lors qu'elle dispose, lors du visionnage de l'oeuvre en version originale, du découpage dialogué sous sa forme intégrale et définitive en français, la commission n'est pas tenue, à peine d'irrégularité de la procédure, de visionner séparément chacun des formats soumis à son avis.,,,2) Juge ayant estimé, pour écarter le moyen tiré de ce que le visa d'exploitation d'un film devait être assorti de l'avertissement prévu à l'article R. 211-13 du code du cinéma et de l'image animée, que le public était suffisamment informé du contenu du film et des éléments qu'il comportait susceptibles de choquer les plus jeunes du fait, d'une part, de l'interdiction aux moins de douze ans, exceptionnelle s'agissant d'un film d'animation, d'autre part, des conditions de diffusion du film, en particulier en raison de la nature du titre et de l'affiche du film ainsi que du contenu de la bande annonce diffusée avant sa sortie. Erreur de droit à s'être fondé sur de tels éléments pour juger que l'information du spectateur sur les particularités de l'oeuvre n'exigeait pas que le visa délivré à la version originale sous-titrée du film soit assorti d'un avertissement.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
