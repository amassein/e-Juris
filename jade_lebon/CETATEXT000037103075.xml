<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037103075</ID>
<ANCIEN_ID>JG_L_2018_06_000000412970</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/10/30/CETATEXT000037103075.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 5ème chambres réunies, 25/06/2018, 412970</TITRE>
<DATE_DEC>2018-06-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>412970</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GADIOU, CHEVALLIER</AVOCATS>
<RAPPORTEUR>M. Jean-Baptiste de Froment</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Julie Burguburu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:412970.20180625</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              M. B...A...a demandé au juge des référés du tribunal administratif de Paris, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, de suspendre l'exécution de la décision du garde des sceaux, ministre de la justice, du 18 avril 2017, rejetant ses demandes de nomination dans un des offices notariaux à créer au titre de la loi n° 2015-990 du 6 août 2015. Par une ordonnance n° 1710155/9 du 13 juillet 2017, le juge des référés a fait droit à cette demande. <br/>
<br/>
              Par un pourvoi, enregistré au secrétariat du contentieux du Conseil d'Etat le 31 juillet 2017, la garde des sceaux, ministre de la justice, demande au Conseil d'Etat d'annuler cette ordonnance.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              -	la loi n° 91-647 du 10 juillet 1991 ;<br/>
              -	la loi n° 2015-990 du 6 août 2015 ;<br/>
              -	le décret n° 71-942 du 26 novembre 1971 ;<br/>
              -	le décret n° 73-609 du 5 juillet 1973 ; <br/>
              -	le décret n° 2016-661 du 20 mai 2016 ;<br/>
              -	l'arrêté du 16 septembre 2016 pris en application de l'article 52 de la loi n° 2015-990 du 6 août 2015 pour la croissance, l'activité et l'égalité des chances économiques ;<br/>
              -	le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Baptiste de Froment, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Julie Burguburu, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gadiou, Chevallier, avocat de M.A....<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision. ".<br/>
<br/>
              2. Le ministre de la justice se pourvoit en cassation contre l'ordonnance du 13 juillet 2017 par laquelle le juge des référés du tribunal administratif de Paris a suspendu, sur le fondement des dispositions citées ci-dessus, l'exécution de la décision du 18 avril 2017 par laquelle il a rejeté les demandes de M. A...tendant à ce qu'il soit nommé dans un des offices notariaux à créer en application de la loi du 6 août 2015 pour lesquels il s'était porté candidat.<br/>
<br/>
              Sur la compétence du tribunal administratif :<br/>
<br/>
              3. Aux termes de l'article 52 de la loi du 6 août 2015 pour la croissance, l'activité et l'égalité des chances économiques : " I. - Les notaires (...) peuvent librement s'installer dans les zones où l'implantation d'offices apparaît utile pour renforcer la proximité ou l'offre de services. / Ces zones sont déterminées par une carte établie conjointement par les ministres de la justice et de l'économie, sur proposition de l'Autorité de la concurrence en application de l'article L. 462-4-1 du code de commerce. (...) / A cet effet, cette carte identifie les secteurs dans lesquels, pour renforcer la proximité ou l'offre de services, la création de nouveaux offices de notaire (...) apparaît utile. / (...) / II. Dans les zones mentionnées au I, lorsque le demandeur remplit les conditions de nationalité, d'aptitude, d'honorabilité, d'expérience et d'assurance requises pour être nommé en qualité de notaire (...), le ministre de la justice le nomme titulaire de l'office de notaire (...) créé. (...) ". Aux termes de l'article 3 du décret du 5 juillet 1973 relatif à la formation professionnelle dans le notariat et aux conditions d'accès aux fonctions de notaire, dans sa rédaction issue du décret du 20 mai 2016 relatif aux officiers publics et ministériels : " Nul ne peut être notaire s'il ne remplit les conditions suivantes : / 1° Etre français ou ressortissant d'un autre Etat membre de l'Union européenne ou d'un autre Etat partie à l'accord sur l'Espace économique européen ; / 2° N'avoir pas été l'auteur de faits contraires à l'honneur et à la probité ; / 3° N'avoir pas été l'auteur d'agissements de même nature ayant donné lieu à mise à la retraite d'office ou à une sanction disciplinaire ou administrative de destitution, radiation, révocation, retrait d'agrément ou d'autorisation ; / 4° N'avoir pas été frappé de faillite personnelle ou de l'interdiction prévue à l'article L. 653-8 du code de commerce ; / 5° Avoir obtenu un diplôme national de master en droit ou l'un des diplômes admis en dispense pour l'exercice de la profession de notaire par arrêté conjoint du garde des sceaux, ministre de la justice, et du ministre chargé de l'enseignement supérieur ; / 6° Etre titulaire du diplôme de notaire ou du diplôme supérieur de notariat. ". Aux termes du premier alinéa de l'article 49 du même décret : " Peuvent demander leur nomination sur un office à créer les personnes qui remplissent les conditions générales d'aptitude aux fonctions de notaire. ". Par un arrêté du 16 septembre 2016, les ministres de l'économie et des finances et de la justice ont établi la carte prévue à l'article 52 précité de la loi du 6 août 2015, qui comporte deux cent quarante-sept zones dans lesquelles la création d'offices de notaire apparaît utile pour renforcer la proximité ou l'offre de services et ils ont fixé, pour chacune de ces zones, une recommandation sur le nombre d'offices notariaux à créer pour les années 2016-2018.<br/>
<br/>
              4. Il résulte des dispositions citées ci-dessus qu'il incombe au ministre de la justice de nommer titulaire d'un office à créer le demandeur qui remplit les conditions générales d'aptitude aux fonctions de notaire précisées par l'article 3 du décret du 5 juillet 1973 relatif à la formation professionnelle dans le notariat et aux conditions d'accès aux fonctions de notaire et, au contraire, de rejeter la demande lorsque le candidat ne remplit pas ces conditions. La décision par laquelle le ministre rejette une candidature au motif que le candidat ne remplit pas les conditions générales d'aptitude aux fonctions, qui ne porte pas sur le principe de la création de l'office pour lequel l'intéressé a déposé sa candidature, mais sur l'appréciation de l'aptitude du demandeur aux fonctions de notaire, constitue un acte individuel. Les recours contre une telle décision, qui n'est pas au nombre de celles dont le Conseil d'Etat est compétent pour connaître en premier et dernier ressort, relèvent de la compétence en premier ressort du tribunal administratif. Par suite, contrairement à ce que soutient le ministre, le juge des référés du tribunal administratif n'a commis aucune erreur de droit en statuant sur la demande de M. A...tendant à la suspension de l'exécution de sa décision de rejet de ses demandes de nomination dans un des offices notariaux à créer.<br/>
<br/>
              Sur la condition d'urgence :<br/>
<br/>
              5. L'urgence justifie que soit prononcée la suspension d'un acte administratif lorsque l'exécution de celui-ci porte atteinte, de manière suffisamment grave et immédiate, à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre. Il appartient au juge des référés d'apprécier concrètement, compte tenu des justifications fournies par le requérant, si les effets de l'acte litigieux sont de nature à caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, l'exécution de la décision soit suspendue. Il lui appartient également, l'urgence s'appréciant objectivement et compte tenu de l'ensemble des circonstances de chaque espèce, de faire apparaître dans sa décision tous les éléments qui, eu égard notamment à l'argumentation des parties, l'ont conduit à considérer que la suspension demandée revêtait un caractère d'urgence.<br/>
<br/>
              6. Il ressort des énonciations de l'ordonnance attaquée que, pour juger que la condition d'urgence posée par l'article L. 521-1 du code de justice administrative était remplie, le juge des référés a relevé, en premier lieu, que la décision dont la suspension était demandée avait pour effet de priver M.A..., qui rencontrait des difficultés matérielles dans l'exercice de sa profession d'avocat à Paris, de la possibilité d'être nommé en qualité de notaire dans un des offices à créer dans les zones de Nevers et d'Evreux alors que, après le tirage au sort effectué pour déterminer l'ordre d'examen des candidatures reçues pour la nomination dans ces nouveaux offices, sa candidature était classée première pour un office à créer dans la zone de Nevers et deuxième pour un office à créer dans la zone d'Evreux, en deuxième lieu, qu'à la date à laquelle il statuait, les nominations de notaires dans ces zones étaient imminentes, et, enfin, que l'éventuelle remise en cause, après plusieurs mois de fonctionnement des offices créés, de la nomination de leurs titulaires, qui pourrait intervenir si le juge de l'excès de pouvoir jugeait illégale la décision litigieuse, serait de nature à porter une atteinte plus grande à l'intérêt général que celle qui pourrait résulter de la suspension de la décision attaquée. En se prononçant ainsi, le juge des référés a porté sur les faits de l'espèce une appréciation souveraine exempte de dénaturation.<br/>
<br/>
              Sur l'existence de moyens propres à faire naître un doute sérieux sur la légalité de la décision litigieuse : <br/>
<br/>
              7. Par l'ordonnance attaquée, le juge des référés a jugé propres à créer, en l'état de l'instruction, un doute sérieux sur la légalité de la décision du 18 avril 2017, les moyens tirés d'un défaut de motivation, de la méconnaissance du principe du contradictoire et d'une erreur d'appréciation.<br/>
<br/>
              8. En premier lieu, il résulte de l'article 3 du décret du 5 juillet 1973 cité au point 3 de la présente décision que nul ne peut être notaire s'il ne remplit pas, notamment, la condition de n'avoir pas été l'auteur de faits contraires à l'honneur et à la probité. Lorsqu'il vérifie le respect de cette condition, il appartient au ministre de la justice d'apprécier, sous le contrôle du juge de l'excès de pouvoir, si l'intéressé a commis des faits contraires à l'honneur et à la probité qui sont, compte tenu notamment de leur nature, de leur gravité, de leur ancienneté ainsi que du comportement postérieur de l'intéressé, susceptibles de justifier légalement un refus de nomination. Dès lors, en jugeant que, eu égard au caractère isolé et ancien des faits reprochés à l'intéressé, le moyen tiré de l'erreur d'appréciation au regard des dispositions du 2° de l'article 3 du décret du 5 juillet 1973, était propre à créer, en l'état de l'instruction, un doute sérieux sur la légalité de la décision du 18 avril 2017, le juge des référés, qui n'a pas entaché sur ce point son ordonnance de dénaturation et qui a pris à bon droit en compte le fait que le juge de l'excès de pouvoir exercerait un entier contrôle sur l'appréciation du ministre, n'a pas, eu égard à son office, commis d'erreur de droit.  <br/>
<br/>
              9. En deuxième lieu, aux termes de l'article L. 121-1 du code des relations entre le public et l'administration : " Exception faite des cas où il est statué sur une demande, les décisions individuelles qui doivent être motivées en application de l'article L. 211-2, ainsi que les décisions qui, bien que non mentionnées à cet article, sont prises en considération de la personne, sont soumises au respect d'une procédure contradictoire préalable. ". Il ressort des pièces du dossier soumis au juge des référés que si la décision du 18 avril 2017 fait suite à une demande de M.A..., elle est fondée sur des motifs tenant à ce que l'intéressé aurait commis des faits contraires à l'honneur et à la probité qui, étant relatifs au comportement personnel de M. A..., ne pouvaient lui être opposés sans qu'il ait été préalablement mis en mesure de présenter ses observations. Par suite, en jugeant propre à créer, en l'état de l'instruction, un doute sérieux sur la légalité de la décision du 18 avril 2017 le moyen tiré de la méconnaissance du principe du contradictoire au regard de l'article L. 121-1 de ce même code, le juge des référés n'a pas, eu égard à son office, commis d'erreur de droit.<br/>
<br/>
              10. En dernier lieu, aux termes de l'article L. 211-2 du code des relations entre le public et l'administration : " Les personnes physiques ou morales ont le droit d'être informées sans délai des motifs des décisions administratives individuelles défavorables qui les concernent. A cet effet, doivent être motivées les décisions qui : / (...) Refusent un avantage dont l'attribution constitue un droit pour les personnes qui remplissent les conditions légales pour l'obtenir ; (...) " ; aux termes de l'article L. 211-5 du même code : " La motivation exigée par le présent chapitre doit être écrite et comporter l'énoncé des considérations de droit et de fait qui constituent le fondement de la décision. ". Il résulte des dispositions citées au point 3 que, dès lors que le demandeur remplit les conditions énumérées à l'article 3 du décret du 5 juillet 1973, il dispose d'un droit à être nommé titulaire de l'office à créer pour lequel il est candidat, sous réserve de l'ordre d'examen de sa candidature tel qu'il est déterminé par sa date d'enregistrement ou par un tirage au sort. Une décision de refus de nomination, au motif que le candidat ne remplit pas ces conditions, doit dès lors, en application des dispositions des articles L. 211-2 et L. 211-5 du code des relations entre le public et l'administration, être motivée. Par suite, en jugeant qu'était propre à créer, en l'état de l'instruction, un doute sérieux sur la légalité de la décision du 18 avril 2017, le moyen tiré de ce que, ne précisant pas les éléments sur lesquels s'était fondé le ministre pour estimer que M. A...ne remplissait pas les conditions générales d'aptitude, elle n'était pas motivée, le juge des référés n'a pas, eu égard à son office, commis d'erreur de droit.<br/>
<br/>
              11. Il résulte de tout ce qui précède que le ministre de la justice, n'est pas fondé à demander l'annulation de l'ordonnance qu'il attaque. Par suite, son pourvoi doit être rejeté.<br/>
<br/>
              Sur les conclusions présentées au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 :<br/>
<br/>
              12. M. A...a obtenu le bénéfice de l'aide juridictionnelle. Par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. Il y a lieu, dans les circonstances de l'espèce, et sous réserve que la SCP Gadiou Chevallier, avocat de M.A..., renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à la SCP Gadiou Chevallier. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le pourvoi de la garde des sceaux, ministre de la justice est rejeté.<br/>
<br/>
Article 2 : L'Etat versera à la SCP Gadiou Chevallier, avocat de M.A..., une somme de 3 000 euros en application des dispositions du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat. <br/>
<br/>
Article 3 : La présente décision sera notifiée à la garde des sceaux, ministre de la justice. <br/>
      Copie en sera adressée pour information à M. B...A.... <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-01-06-02 ACTES LÉGISLATIFS ET ADMINISTRATIFS. DIFFÉRENTES CATÉGORIES D'ACTES. ACTES ADMINISTRATIFS - CLASSIFICATION. ACTES INDIVIDUELS OU COLLECTIFS. - NOTAIRES - NOMINATION DANS LES ZONES DITES D'INSTALLATION LIBRE - DÉCISION PAR LAQUELLE LE MINISTRE DE LA JUSTICE REJETTE UNE CANDIDATURE AU MOTIF QUE LE CANDIDAT NE REMPLIT PAS LES CONDITIONS GÉNÉRALES D'APTITUDE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">01-03-01-02-01-01-04 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - FORME ET PROCÉDURE. QUESTIONS GÉNÉRALES. MOTIVATION. MOTIVATION OBLIGATOIRE. MOTIVATION OBLIGATOIRE EN VERTU DES ARTICLES 1 ET 2 DE LA LOI DU 11 JUILLET 1979. DÉCISION REFUSANT UN AVANTAGE DONT L'ATTRIBUTION CONSTITUE UN DROIT. - NOTAIRES - NOMINATION DANS LES ZONES DITES D'INSTALLATION LIBRE - DÉCISION PAR LAQUELLE LE MINISTRE DE LA JUSTICE REJETTE UNE CANDIDATURE AU MOTIF QUE LE CANDIDAT NE REMPLIT PAS LES CONDITIONS GÉNÉRALES D'APTITUDE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">17-05-01-01-01 COMPÉTENCE. COMPÉTENCE À L'INTÉRIEUR DE LA JURIDICTION ADMINISTRATIVE. COMPÉTENCE EN PREMIER RESSORT DES TRIBUNAUX ADMINISTRATIFS. COMPÉTENCE MATÉRIELLE. ACTES NON RÉGLEMENTAIRES. - NOTAIRES - NOMINATION DANS LES ZONES DITES D'INSTALLATION LIBRE - DÉCISION PAR LAQUELLE LE MINISTRE DE LA JUSTICE REJETTE UNE CANDIDATURE AU MOTIF QUE LE CANDIDAT NE REMPLIT PAS LES CONDITIONS GÉNÉRALES D'APTITUDE.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">55-02 PROFESSIONS, CHARGES ET OFFICES. ACCÈS AUX PROFESSIONS. - NOTAIRES - NOMINATION DANS LES ZONES DITES D'INSTALLATION LIBRE - DÉCISION PAR LAQUELLE LE MINISTRE DE LA JUSTICE REJETTE UNE CANDIDATURE AU MOTIF QUE LE CANDIDAT NE REMPLIT PAS LES CONDITIONS GÉNÉRALES D'APTITUDE - 1) ACTE INDIVIDUEL - EXISTENCE - CONSÉQUENCE - COMPÉTENCE DU TA EN PREMIER RESSORT POUR CONNAÎTRE D'UN RECOURS CONTRE CETTE DÉCISION - 2) CONDITION TENANT À L'ABSENCE DE COMMISSION DE FAITS CONTRAIRES À L'HONNEUR ET À LA PROBITÉ - PORTÉE - 3) OBLIGATION DE MOTIVATION - EXISTENCE.
</SCT>
<ANA ID="9A"> 01-01-06-02 Il résulte des I et II de l'article 52 de la loi n° 2015-990 du 6 août 2015 pour la croissance, l'activité et l'égalité des chances économiques, de l'article 3 et du premier alinéa de l'article 49 du décret n° 73-609 du 5 juillet 1973 relatif à la formation professionnelle dans le notariat et aux conditions d'accès aux fonctions de notaire qu'il incombe au ministre de la justice de nommer titulaire d'un office à créer le demandeur qui remplit les conditions générales d'aptitude aux fonctions de notaire précisées par l'article 3 du décret du 5 juillet 1973 et, au contraire, de rejeter la demande lorsque le candidat ne remplit pas ces conditions. La décision par laquelle le ministre rejette une candidature au motif que le candidat ne remplit pas les conditions générales d'aptitude aux fonctions, qui ne porte pas sur le principe de la création de l'office pour lequel l'intéressé a déposé sa candidature mais sur l'appréciation de l'aptitude du demandeur aux fonctions de notaire, constitue un acte individuel.</ANA>
<ANA ID="9B"> 01-03-01-02-01-01-04 Une décision de refus de nomination, au motif que le candidat ne remplit pas les conditions énumérées à l'article 3 du décret n° 73-609 du 5 juillet 1973, doit, en application des articles L. 211-2 et L. 211-5 du code des relations entre le public et l'administration (CRPA), être motivée.</ANA>
<ANA ID="9C"> 17-05-01-01-01 Il résulte des I et II de l'article 52 de la loi n° 2015-990 du 6 août 2015 pour la croissance, l'activité et l'égalité des chances économiques, de l'article 3 et du premier alinéa de l'article 49 du décret n° 73-609 du 5 juillet 1973 relatif à la formation professionnelle dans le notariat et aux conditions d'accès aux fonctions de notaire qu'il incombe au ministre de la justice de nommer titulaire d'un office à créer le demandeur qui remplit les conditions générales d'aptitude aux fonctions de notaire précisées par l'article 3 du décret du 5 juillet 1973 et, au contraire, de rejeter la demande lorsque le candidat ne remplit pas ces conditions. La décision par laquelle le ministre rejette une candidature au motif que le candidat ne remplit pas les conditions générales d'aptitude aux fonctions, qui ne porte pas sur le principe de la création de l'office pour lequel l'intéressé a déposé sa candidature mais sur l'appréciation de l'aptitude du demandeur aux fonctions de notaire, constitue un acte individuel. Les recours contre une telle décision, qui n'est pas au nombre de celles dont le Conseil d'Etat est compétent pour connaître en premier et dernier ressort, relèvent de la compétence en premier ressort du tribunal administratif.</ANA>
<ANA ID="9D"> 55-02 1) Il résulte des I et II de l'article 52 de la loi n° 2015-990 du 6 août 2015 pour la croissance, l'activité et l'égalité des chances économiques, de l'article 3 et du premier alinéa de l'article 49 du décret n° 73-609 du 5 juillet 1973 relatif à la formation professionnelle dans le notariat et aux conditions d'accès aux fonctions de notaire qu'il incombe au ministre de la justice de nommer titulaire d'un office à créer le demandeur qui remplit les conditions générales d'aptitude aux fonctions de notaire précisées par l'article 3 du décret du 5 juillet 1973 et, au contraire, de rejeter la demande lorsque le candidat ne remplit pas ces conditions. La décision par laquelle le ministre rejette une candidature au motif que le candidat ne remplit pas les conditions générales d'aptitude aux fonctions, qui ne porte pas sur le principe de la création de l'office pour lequel l'intéressé a déposé sa candidature mais sur l'appréciation de l'aptitude du demandeur aux fonctions de notaire, constitue un acte individuel. Les recours contre une telle décision, qui n'est pas au nombre de celles dont le Conseil d'Etat est compétent pour connaître en premier et dernier ressort, relèvent de la compétence en premier ressort du tribunal administratif.,,,2) Lorsqu'il vérifie le respect de la condition de n'avoir pas été l'auteur de faits contraires à l'honneur et à la probité, il appartient au ministre de la justice d'apprécier, sous le contrôle du juge de l'excès de pouvoir, si l'intéressé a commis des faits contraires à l'honneur et à la probité qui sont, compte tenu notamment de leur nature, de leur gravité, de leur ancienneté ainsi que du comportement postérieur de l'intéressé, susceptibles de justifier légalement un refus de nomination.,,,3) Une décision de refus de nomination, au motif que le candidat ne remplit pas les conditions énumérées à l'article 3 du décret du 5 juillet 1973, doit, en application des articles L. 211-2 et L. 211-5 du code des relations entre le public et l'administration (CRPA), être motivée.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
