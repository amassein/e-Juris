<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034026111</ID>
<ANCIEN_ID>JG_L_2017_02_000000404291</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/02/61/CETATEXT000034026111.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème - 10ème chambres réunies, 10/02/2017, 404291</TITRE>
<DATE_DEC>2017-02-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>404291</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème - 10ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LE GRIEL</AVOCATS>
<RAPPORTEUR>M. Bastien Lignereux</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Astrid Nicolazo de Barmon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:404291.20170210</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Pimiento Music a demandé au tribunal administratif de Paris de prononcer la décharge des retenues à la source prélevées sur les dividendes qu'elle a versés à la société Nerthus Invest au cours des exercices clos en 2008 et 2009 et des pénalités correspondantes. Par un jugement n° 1404040 du 4 mars 2015, le tribunal administratif de Paris a partiellement fait droit à cette demande.<br/>
<br/>
              Par une requête, enregistrée le 29 juillet 2016 au greffe de la cour administrative d'appel de Paris, par ailleurs saisie d'un appel contre ce jugement, la société Pimiento Music a demandé au juge des référés de cette cour de suspendre, sur le fondement des dispositions de l'article L. 521-1 du code de justice administrative, la mise en recouvrement des impositions et pénalités en litige, qui ont fait l'objet d'avis de mise en recouvrement le 17 juin et d'une mise en demeure de payer le 30 juin 2016. Par une ordonnance n° 16PA02465 du 27 septembre 2016, le juge des référés de la cour administrative d'appel de Paris a rejeté cette demande.<br/>
<br/>
              Par un pourvoi, un nouveau mémoire et un mémoire en réplique, enregistrés les 11 et 20 octobre 2016 et le 6 décembre 2016 au secrétariat du contentieux du Conseil d'Etat, la société Pimiento Music demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande de suspension ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bastien Lignereux, auditeur, <br/>
<br/>
              - les conclusions de Mme Marie-Astrid Nicolazo de Barmon, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Le Griel, avocat de la société Pimiento Music ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision. (...) ".<br/>
<br/>
              2. Il ressort des pièces du dossier soumis au juge des référés de la cour administrative d'appel de Paris que, par un jugement du 4 mars 2015, le tribunal administratif de Paris a rejeté la demande de la société Pimiento Music tendant à la décharge des retenues à la source prélevées sur les dividendes qu'elle a versés à la société Nerthus Invest au cours des exercices clos en 2008 et 2009 et des pénalités correspondantes. Après que ces impositions et pénalités ont fait l'objet, en juin 2016, d'un avis de mise en recouvrement et d'une mise en demeure de payer, la société Pimiento Music a assorti son appel contre ce jugement d'une demande, présentée sur le fondement de l'article L. 521-1 du code de justice administrative, tendant à la suspension de cette mise en recouvrement. Par une ordonnance du 27 septembre 2016, le juge des référés de la cour administrative d'appel de Paris a rejeté cette demande au motif qu'elle ne remplissait pas la condition d'urgence à laquelle est subordonné le prononcé d'une telle mesure.<br/>
<br/>
              3. Le contribuable qui a saisi le juge de l'impôt de conclusions tendant à la décharge d'une imposition à laquelle il a été assujetti est recevable à demander au juge des référés, sur le fondement des dispositions précitées de l'article L. 521-1 du code de justice administrative, la suspension de la mise en recouvrement de l'imposition, dès lors que celle-ci est exigible. Le prononcé de cette suspension est subordonné à la double condition, d'une part, qu'il soit fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux sur la régularité de la procédure d'imposition ou sur le bien-fondé de l'imposition et, d'autre part, que l'urgence justifie la mesure de suspension sollicitée. Pour vérifier si la condition d'urgence est satisfaite, le juge des référés doit apprécier la gravité des conséquences que pourraient entraîner, à brève échéance, l'obligation de payer sans délai l'imposition ou les mesures mises en oeuvre ou susceptibles de l'être pour son recouvrement, eu égard aux capacités du contribuable à acquitter les sommes qui lui sont demandées.<br/>
<br/>
              4. Pour juger que la demande présentée par la société Pimiento Music ne remplissait pas la condition d'urgence à laquelle l'article L. 521-1 du code de justice administrative subordonne la mesure de suspension, le juge des référés de la cour administrative d'appel de Paris a relevé, sans dénaturer les faits, que la requête d'appel de la société était inscrite au rôle d'une audience du 20 octobre 2016, soit moins d'un mois après l'ordonnance qu'il a rendue, et était dès lors susceptible d'être jugée à une date rapprochée. En se fondant sur cette circonstance, et en estimant nécessairement qu'au vu des éléments apportés à l'appui de la demande de suspension, dont il ressortait que le montant des disponibilités de la société requérante était supérieur à celui des impositions et pénalités mises en recouvrement, aucun préjudice irréversible n'était susceptible de résulter de leur recouvrement avant le jugement de la requête au fond, le juge des référés n'a pas commis d'erreur de droit.<br/>
<br/>
              5. Il résulte de ce qui précède que la société Pimiento Music n'est pas fondée à demander l'annulation de l'ordonnance qu'elle attaque. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la société Pimiento Music est rejeté.<br/>
Article 2 : La présente décision sera notifiée à la société Pimiento Music et au ministre de l'économie et des finances.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-035-02-03-02 PROCÉDURE. PROCÉDURES INSTITUÉES PAR LA LOI DU 30 JUIN 2000. RÉFÉRÉ SUSPENSION (ART. L. 521-1 DU CODE DE JUSTICE ADMINISTRATIVE). CONDITIONS D'OCTROI DE LA SUSPENSION DEMANDÉE. URGENCE. - POSSIBILITÉ DE TENIR COMPTE DE L'INTERVENTION PROCHAINE D'UN JUGEMENT AU FOND - EXISTENCE.
</SCT>
<ANA ID="9A"> 54-035-02-03-02 Juge des référés ayant relevé, pour juger que la demande présentée par une société tendant à la suspension de la mise en recouvrement d'impositions ne remplissait pas la condition d'urgence prévue à l'article L. 521-1 du code de justice administrative, que la requête d'appel de la société était inscrite au rôle d'une audience devant se tenir moins d'un mois après la date de son ordonnance et était dès lors susceptible d'être jugée à une date rapprochée. En se fondant sur cette circonstance, et en estimant nécessairement qu'au vu des éléments apportés à l'appui de la demande de suspension, dont il ressortait que le montant des disponibilités de la société requérante était supérieur à celui des impositions et pénalités mises en recouvrement, aucun préjudice irréversible n'était susceptible de résulter de leur recouvrement avant le jugement de la requête au fond, le juge des référés n'a pas commis d'erreur de droit.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
