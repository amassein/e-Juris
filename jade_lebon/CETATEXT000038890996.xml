<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038890996</ID>
<ANCIEN_ID>JG_L_2019_07_000000415427</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/89/09/CETATEXT000038890996.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 4ème chambres réunies, 10/07/2019, 415427</TITRE>
<DATE_DEC>2019-07-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>415427</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 4ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CAPRON ; SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>Mme Bénédicte Fauvarque-Cosson</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:415427.20190710</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A...B...a demandé au tribunal administratif de Pau, d'une part, d'annuler la mise en demeure prise par la caisse d'allocations familiales des Landes le 22 décembre 2015 pour le paiement de la somme de 13 233,55 euros correspondant à des indus de revenu de solidarité active, d'aide exceptionnelle de fin d'année et d'aide personnalisée au logement, ainsi que la décision implicite de rejet de son recours gracieux formé le 5 janvier 2016, et, d'autre part, de la décharger de l'obligation de payer cette somme. Par un jugement n° 1601743 du 18 octobre 2017, le tribunal administratif de Pau a rejeté sa demande.<br/>
<br/>
              Par un pourvoi et un nouveau mémoire, enregistrés les 3 novembre 2017 et 15 mars 2018 au secrétariat du contentieux du Conseil d'État, Mme B...demande au Conseil d'État : <br/>
<br/>
              1°) d'annuler ce jugement ; <br/>
<br/>
              2°) de mettre à la charge de l'État la somme de 2 500 euros, à verser à la SCP Capron, son avocat, au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'action sociale et des familles ;<br/>
              - le code de la sécurité sociale ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le décret n° 2013-1294 du 30 décembre 2013 ;<br/>
              - le décret n° 2014-1709 du 30 décembre 2014 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Bénédicte Fauvarque-Cosson, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Capron, avocat de Mme B...;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que Mme B...a bénéficié du revenu de solidarité active à compter du mois de juin 2009 en qualité de personne isolée avec deux enfants à charge. A la suite d'un contrôle sur place, réalisé le 17 février 2015, la caisse d'allocations familiales des Landes a estimé qu'elle vivait en concubinage et a pris des décisions de récupération d'indus de plusieurs prestations dont elle bénéficiait, le 28 mai 2015, à hauteur de 8 688,23 euros, au titre du revenu de solidarité active et de l'aide personnalisée au logement pour la période du 1er juin 2013 au 31 mai 2015, le 10 juin 2015, à hauteur de 228,67 euros, au titre de l'aide exceptionnelle de fin d'année 2014, et le 21 juillet 2015, après levée de la prescription biennale, à hauteur de 4 393,17 euros au titre du revenu de solidarité active et de l'aide personnalisée au logement pour la période du 1er juillet 2012 au 31 mai 2013 et à hauteur de 228,67 euros au titre de l'aide exceptionnelle de fin d'année 2013. Le 22 décembre 2015, la caisse d'allocations familiales des Landes a mis Mme B... en demeure de rembourser des indus de revenu de solidarité active d'un montant de 12 746,40 euros pour la période de juillet 2012 à mai 2015, d'aides exceptionnelles de fin d'année d'un montant de 457,34 euros au titre de 2013 et 2014 et le solde, s'élevant à 29,81 euros, d'un indu d'aide personnalisée au logement pour la période de janvier à mai 2015, soit un montant total de 13 233,55 euros. Mme B...se pourvoit en cassation contre le jugement du 18 octobre 2017 par lequel le tribunal a rejeté sa demande tendant à l'annulation de cette mise en demeure et du rejet implicite de son recours préalable contre cet acte.<br/>
<br/>
              2. D'une part, aux termes de l'article L. 262-46 du code de l'action sociale et des familles : " Tout paiement indu de revenu de solidarité active est récupéré par l'organisme chargé du service de celui-ci ainsi que, dans les conditions définies au présent article, par les collectivités débitrices du revenu de solidarité active. / Toute réclamation dirigée contre une décision de récupération de l'indu, le dépôt d'une demande de remise ou de réduction de créance ainsi que les recours administratifs et contentieux, y compris en appel, contre les décisions prises sur ces réclamations et demandes ont un caractère suspensif. / Sauf si le bénéficiaire opte pour le remboursement de l'indu en une seule fois, l'organisme mentionné au premier alinéa procède au recouvrement de tout paiement indu de revenu de solidarité active par retenues sur les montants à échoir. / (...) / L'article L. 161-1-5 du code de la sécurité sociale est applicable pour le recouvrement des sommes indûment versées au titre du revenu de solidarité active. (...) ". En outre, un versement indu de l'aide exceptionnelle attribuée à un allocataire du revenu de solidarité active au titre de cette allocation doit être regardé comme relevant des " sommes indûment versées au titre du revenu de solidarité active " au sens de ces dispositions.<br/>
<br/>
              3. D'autre part, il résulte des dispositions des articles L. 351-11 et L. 351-14 du code de la construction et de l'habitation qu'en cas de sommes indûment payées au titre de l'aide personnalisée au logement, l'organisme payeur prend une décision de récupération, soumise à recours administratif préalable obligatoire devant le directeur de l'organisme payeur statuant après avis de la commission de recours amiable, puis que l'indu peut, sous certaines conditions, être récupéré par retenue sur les échéances à venir de cette allocation ou de certaines autres prestations sociales. Aux termes du neuvième alinéa de cet article : " L'article L. 161-1-5 du code de la sécurité sociale est applicable pour le recouvrement des sommes indûment versées ".<br/>
<br/>
              4. Aux termes de l'article L. 161-1-5 du code de la sécurité sociale : " Pour le recouvrement d'une prestation indûment versée (...), le directeur d'un organisme de sécurité sociale peut, dans les délais et selon les conditions fixés par voie réglementaire, délivrer une contrainte qui, à défaut d'opposition du débiteur devant la juridiction compétente, comporte tous les effets d'un jugement et confère notamment le bénéfice de l'hypothèque judiciaire ". Selon le second alinéa de l'article R. 133-9-2 du même code, à l'expiration du délai de deux mois qui suit la décision de récupération ou notification de payer, ou après notification d'une décision de rejet du recours préalable obligatoire exercé par l'allocataire : " (...) le directeur de l'organisme créancier compétent, en cas de refus du débiteur de payer, lui adresse par tout moyen permettant de rapporter la preuve de sa date de réception une mise en demeure de payer dans le délai d'un mois qui comporte le motif, la nature et le montant des sommes demeurant... ". Enfin, aux termes de l'article R. 133-3 du même code, dans sa rédaction applicable au litige : " Si la mise en demeure (...) reste sans effet au terme du délai d'un mois à compter de sa notification, le directeur de l'organisme créancier peut décerner la contrainte (...) mentionnée à l'article L. 161-1-5. La contrainte est signifiée au débiteur par acte d'huissier de justice ou par lettre recommandée avec demande d'avis de réception. A peine de nullité, l'acte d'huissier ou la lettre recommandée mentionne la référence de la contrainte et son montant, le délai dans lequel l'opposition doit être formée, l'adresse du tribunal compétent et les formes requises pour sa saisine. / (...) / Le débiteur peut former opposition par inscription au secrétariat du tribunal compétent dans le ressort duquel il est domicilié... ".<br/>
<br/>
              5. Il résulte de ces dispositions que lorsqu'il constate un indu de revenu de solidarité active, d'aide exceptionnelle de fin d'année ou d'aide personnalisée au logement, l'organisme chargé du service de la prestation ou de l'aide doit prendre une décision de récupération d'indu, motivée et notifiée au bénéficiaire de l'allocation, qui lui réclame le remboursement de la somme due et, le cas échéant, l'informe des modalités selon lesquelles cet indu pourra être récupéré par retenues sur les prestations à venir. Cette décision, qui fait grief, peut être contestée devant le tribunal administratif, après l'exercice, s'agissant du revenu de solidarité active et de l'aide personnalisée au logement, d'un recours administratif préalable obligatoire. En l'absence de recours dans un délai de deux mois ou en cas de rejet de celui-ci, et sauf à ce que l'indu ait été remboursé, ait été récupéré par retenues sur les prestations à venir ou ait fait l'objet d'un titre exécutoire émis par l'ordonnateur de la personne publique pour le compte de laquelle la prestation est servie, l'organisme peut mettre l'allocataire en demeure de payer dans le délai d'un mois, puis, si cette mise en demeure reste sans effet dans ce délai, décerner une contrainte, laquelle est susceptible d'opposition devant le tribunal administratif dans le délai de quinze jours. Il suit de là qu'une telle mise en demeure, intervenant après la notification de la décision de récupération de l'indu, constitue un acte préparatoire à la contrainte qui pourra être émise si l'allocataire ne rembourse pas la somme due. Si l'allocataire peut utilement se prévaloir, à l'appui d'une opposition à contrainte, de l'irrégularité de la mise en demeure qui lui a été adressée, celle-ci ne présente pas, en revanche, le caractère d'une décision susceptible de recours.<br/>
<br/>
              6. Il suit de là que la demande présentée par Mme B...devant le tribunal administratif de Pau, tendant à l'annulation de la mise en demeure de la caisse d'allocations familiales des Landes du 22 décembre 2015, était irrecevable. Ce motif, qui n'appelle l'appréciation d'aucune circonstance de fait, doit être substitué à ceux retenus par le jugement du 18 octobre 2017, dont il justifie légalement le dispositif.<br/>
<br/>
              7. Par suite, sans qu'il soit besoin d'examiner les moyens de son pourvoi, les conclusions de Mme B...tendant à l'annulation du jugement du tribunal administratif de Pau qu'elle attaque doivent être rejetées.<br/>
<br/>
              8. Les dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 font obstacle à ce qu'il soit fait droit aux conclusions présentées sur le fondement de ces dispositions par la SCP Capron, avocat de MmeB.réclamées, la date du ou des versements indus donnant lieu à recouvrement, les voies et délais de recours et le motif qui, le cas échéant, a conduit à rejeter totalement ou partiellement les observations présentées Dans les circonstances de l'espèce, il n'y a pas lieu de faire droit aux conclusions du département des Landes présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : Le pourvoi de Mme B...est rejeté.<br/>
Article 2 : Les conclusions du département des Landes présentées au titre de l'article L. 761-1 du code de justice sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à Mme A...B..., au département des Landes, à la ministre des solidarités et de la santé et à la ministre de la cohésion des territoires et des relations avec les collectivités territoriales.<br/>
Copie en sera adressée à la caisse d'allocations familiales des Landes.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-01-05-02-02 ACTES LÉGISLATIFS ET ADMINISTRATIFS. DIFFÉRENTES CATÉGORIES D'ACTES. ACTES ADMINISTRATIFS - NOTION. ACTES À CARACTÈRE DE DÉCISION. ACTES NE PRÉSENTANT PAS CE CARACTÈRE. - INDU DE RSA, D'AIDE EXCEPTIONNELLE DE FIN D'ANNÉE OU D'APL - MISE EN DEMEURE DE PAYER ADRESSÉE À L'ALLOCATAIRE - INCLUSION.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">04-04 AIDE SOCIALE. CONTENTIEUX DE L'AIDE SOCIALE ET DE LA TARIFICATION. - INDU DE RSA, D'AIDE EXCEPTIONNELLE DE FIN D'ANNÉE OU D'APL - MISE EN DEMEURE DE PAYER ADRESSÉE À L'ALLOCATAIRE - MESURE PRÉPARATOIRE, INSUSCEPTIBLE DE RECOURS.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-01-01-02-02 PROCÉDURE. INTRODUCTION DE L'INSTANCE. DÉCISIONS POUVANT OU NON FAIRE L'OBJET D'UN RECOURS. ACTES NE CONSTITUANT PAS DES DÉCISIONS SUSCEPTIBLES DE RECOURS. MESURES PRÉPARATOIRES. - INDU DE RSA, D'AIDE EXCEPTIONNELLE DE FIN D'ANNÉE OU D'APL - MISE EN DEMEURE DE PAYER ADRESSÉE À L'ALLOCATAIRE - INCLUSION.
</SCT>
<ANA ID="9A"> 01-01-05-02-02 Il résulte des articles L. 262-46 du code de l'action sociale et des familles (CASF), L. 351-11 et L. 351-14 du code de la construction et de l'habitation (CCH) et L. 161-1-5, R. 133-9-2 et R. 133-3 du code de la sécurité sociale (CSS) que lorsqu'il constate un indu de revenu de solidarité active (RSA), d'aide exceptionnelle de fin d'année ou d'aide personnalisée au logement (APL), l'organisme chargé du service de la prestation ou de l'aide doit prendre une décision de récupération d'indu, motivée et notifiée au bénéficiaire de l'allocation, qui lui réclame le remboursement de la somme due et, le cas échéant, l'informe des modalités selon lesquelles cet indu pourra être récupéré par retenues sur les prestations à venir. Cette décision, qui fait grief, peut être contestée devant le tribunal administratif, après l'exercice, s'agissant du RSA et de l'APL, d'un recours administratif préalable obligatoire.... ,,En l'absence de recours dans un délai de deux mois ou en cas de rejet de celui-ci, et sauf à ce que l'indu ait été remboursé, ait été récupéré par retenues sur les prestations à venir ou ait fait l'objet d'un titre exécutoire émis par l'ordonnateur de la personne publique pour le compte de laquelle la prestation est servie, l'organisme peut mettre l'allocataire en demeure de payer dans le délai d'un mois, puis, si cette mise en demeure reste sans effet dans ce délai, décerner une contrainte, laquelle est susceptible d'opposition devant le tribunal administratif dans le délai de quinze jours. Il suit de là qu'une telle mise en demeure, intervenant après la notification de la décision de récupération de l'indu, constitue un acte préparatoire à la contrainte qui pourra être émise si l'allocataire ne rembourse pas la somme due. Si l'allocataire peut utilement se prévaloir, à l'appui d'une opposition à contrainte, de l'irrégularité de la mise en demeure qui lui a été adressée, celle-ci ne présente pas, en revanche, le caractère d'une décision susceptible de recours.</ANA>
<ANA ID="9B"> 04-04 Il résulte des articles L. 262-46 du code de l'action sociale et des familles (CASF), L. 351-11 et L. 351-14 du code de la construction et de l'habitation (CCH) et L. 161-1-5, R. 133-9-2 et R. 133-3 du code de la sécurité sociale (CSS) que lorsqu'il constate un indu de revenu de solidarité active (RSA), d'aide exceptionnelle de fin d'année ou d'aide personnalisée au logement (APL), l'organisme chargé du service de la prestation ou de l'aide doit prendre une décision de récupération d'indu, motivée et notifiée au bénéficiaire de l'allocation, qui lui réclame le remboursement de la somme due et, le cas échéant, l'informe des modalités selon lesquelles cet indu pourra être récupéré par retenues sur les prestations à venir. Cette décision, qui fait grief, peut être contestée devant le tribunal administratif, après l'exercice, s'agissant du RSA et de l'APL, d'un recours administratif préalable obligatoire.... ,,En l'absence de recours dans un délai de deux mois ou en cas de rejet de celui-ci, et sauf à ce que l'indu ait été remboursé, ait été récupéré par retenues sur les prestations à venir ou ait fait l'objet d'un titre exécutoire émis par l'ordonnateur de la personne publique pour le compte de laquelle la prestation est servie, l'organisme peut mettre l'allocataire en demeure de payer dans le délai d'un mois, puis, si cette mise en demeure reste sans effet dans ce délai, décerner une contrainte, laquelle est susceptible d'opposition devant le tribunal administratif dans le délai de quinze jours. Il suit de là qu'une telle mise en demeure, intervenant après la notification de la décision de récupération de l'indu, constitue un acte préparatoire à la contrainte qui pourra être émise si l'allocataire ne rembourse pas la somme due. Si l'allocataire peut utilement se prévaloir, à l'appui d'une opposition à contrainte, de l'irrégularité de la mise en demeure qui lui a été adressée, celle-ci ne présente pas, en revanche, le caractère d'une décision susceptible de recours.</ANA>
<ANA ID="9C"> 54-01-01-02-02 Il résulte des articles L. 262-46 du code de l'action sociale et des familles (CASF), L. 351-11 et L. 351-14 du code de la construction et de l'habitation (CCH) et L. 161-1-5, R. 133-9-2 et R. 133-3 du code de la sécurité sociale (CSS) que lorsqu'il constate un indu de revenu de solidarité active (RSA), d'aide exceptionnelle de fin d'année ou d'aide personnalisée au logement (APL), l'organisme chargé du service de la prestation ou de l'aide doit prendre une décision de récupération d'indu, motivée et notifiée au bénéficiaire de l'allocation, qui lui réclame le remboursement de la somme due et, le cas échéant, l'informe des modalités selon lesquelles cet indu pourra être récupéré par retenues sur les prestations à venir. Cette décision, qui fait grief, peut être contestée devant le tribunal administratif, après l'exercice, s'agissant du RSA et de l'APL, d'un recours administratif préalable obligatoire.... ,,En l'absence de recours dans un délai de deux mois ou en cas de rejet de celui-ci, et sauf à ce que l'indu ait été remboursé, ait été récupéré par retenues sur les prestations à venir ou ait fait l'objet d'un titre exécutoire émis par l'ordonnateur de la personne publique pour le compte de laquelle la prestation est servie, l'organisme peut mettre l'allocataire en demeure de payer dans le délai d'un mois, puis, si cette mise en demeure reste sans effet dans ce délai, décerner une contrainte, laquelle est susceptible d'opposition devant le tribunal administratif dans le délai de quinze jours. Il suit de là qu'une telle mise en demeure, intervenant après la notification de la décision de récupération de l'indu, constitue un acte préparatoire à la contrainte qui pourra être émise si l'allocataire ne rembourse pas la somme due. Si l'allocataire peut utilement se prévaloir, à l'appui d'une opposition à contrainte, de l'irrégularité de la mise en demeure qui lui a été adressée, celle-ci ne présente pas, en revanche, le caractère d'une décision susceptible de recours.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
