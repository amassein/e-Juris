<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041763182</ID>
<ANCIEN_ID>JG_L_2020_03_000000434228</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/76/31/CETATEXT000041763182.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 27/03/2020, 434228</TITRE>
<DATE_DEC>2020-03-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>434228</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP COUTARD, MUNIER-APAIRE</AVOCATS>
<RAPPORTEUR>Mme Cécile Renault</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Mireille Le Corre</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:434228.20200327</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. et Mme C... E... ont demandé au tribunal administratif de Marseille d'enjoindre à la ville de Marseille d'exécuter les travaux d'insonorisation de la salle de spectacle " Espace Julien " ordonnés par les jugements de ce tribunal n° 0304788 du 12 décembre 2006 et du 23 février 2009, de procéder à la liquidation provisoire de l'astreinte prononcée par le jugement n° 1003648 du tribunal du 2 novembre 2010 pour un montant de 770 700 euros et de porter le taux de l'astreinte définitive à 1 000 euros par jour de retard. Par un jugement n° 1604032 du 22 mai 2018, le tribunal administratif de Marseille a rejeté leur demande.<br/>
<br/>
              Par un arrêt n° 18MA02928 du 20 décembre 2018, la cour administrative d'appel de Marseille a, sur appel des époux E..., annulé ce jugement et condamné la ville de Marseille à verser la somme de 135 000 euros à l'Etat et la somme de 15 000 euros aux époux E... au titre de la liquidation provisoire de l'astreinte prononcée par le jugement du 2 novembre 2010 du tribunal administratif de Marseille, puis rejeté le surplus de leurs conclusions.  <br/>
<br/>
              Par un arrêt n° 18MA02928 du 11 juillet 2019, la cour administrative d'appel de Marseille a procédé d'office à une nouvelle liquidation provisoire de cette astreinte en condamnant la ville de Marseille à verser la somme de 15 000 euros à l'Etat.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un nouveau mémoire, enregistrés les 4 septembre et 18 novembre 2019 et 6 février 2020 au secrétariat du contentieux du Conseil d'Etat, la ville de Marseille demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'arrêt n° 18MA02928 du 11 juillet 2019 ;<br/>
<br/>
              2°) de mettre à la charge des époux E... la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des collectivités territoriales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme A... D..., auditrice,  <br/>
<br/>
              - les conclusions de Mme Mireille Le Corre, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Coutard, Munier-Apaire, avocat de la ville de Marseille ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par un jugement du 11 juin 2002, le tribunal administratif de Marseille a condamné la ville de Marseille à verser à M. et Mme E... la somme de 15 000 euros en réparation du préjudice causé par les nuisances sonores résultant de l'exploitation de la salle de spectacles dénommée " Espace Julien ", située cours Julien. Par un jugement du 12 décembre 2006, devenu définitif, le tribunal leur a alloué, au même titre, la somme de 10 000 euros, a annulé le refus implicite de la ville de Marseille de réaliser des travaux d'insonorisation de la salle de spectacles et enjoint à la ville d'effectuer les travaux préconisés par le rapport d'expertise déposé le 6 mai 1998, avant le 30 septembre 2007, sous astreinte de 100 euros par jour de retard. Par un jugement du 23 février 2009, le tribunal a, d'une part, liquidé l'astreinte à la somme de 39 500 euros et, d'autre part, enjoint à la ville de Marseille de réaliser les travaux utiles pour mettre la salle aux normes acoustiques, et notamment ceux recommandés par le rapport d'expertise déposé le 17 septembre 2008, avant le 30 novembre 2009, sous astreinte de 500 euros par jour de retard. Par un jugement du 2 novembre 2010, devenu définitif, le tribunal a liquidé l'astreinte prononcée par le jugement du 23 février 2009 à la somme de 50 550 euros et a enjoint à la ville d'exécuter les jugements du 12 décembre 2006 et du 23 février 2009 avant le 28 février 2011, sous une nouvelle astreinte de 300 euros par jour de retard. Par un jugement du 22 mai 2018, le tribunal a rejeté la demande des époux E... tendant au prononcé d'une nouvelle injonction, à la liquidation de l'astreinte prononcée le 2 novembre 2010 et à ce que le taux de cette astreinte soit porté à la somme de 1 000 euros par jour de retard. Par un arrêt du 20 décembre 2018, la cour administrative d'appel de Marseille a annulé ce jugement, prononcé la liquidation provisoire de l'astreinte fixée par le jugement du 2 novembre 2010 en condamnant la ville de Marseille à verser la somme de 135 000 euros à l'Etat et la somme de 15 000 euros aux époux E... pour la période du 1er mars 2011 au 20 décembre 2018 inclus, puis rejeté le surplus des conclusions de ces derniers. Par un courrier du 17 avril 2019, la cour administrative d'appel de Marseille a invité la ville à justifier de l'exécution des travaux d'insonorisation prescrits par les jugements du tribunal administratif de Marseille des 12 décembre 2006 et 23 février 2009. Par un arrêt du 11 juillet 2019, contre lequel la ville de Marseille se pourvoit en cassation, la cour administrative d'appel a procédé d'office à une nouvelle liquidation provisoire de l'astreinte fixée par le jugement du 2 novembre 2010, en condamnant la ville à verser la somme de 15 000 euros à l'Etat pour la période du 21 décembre 2018 au 11 juillet 2019 inclus. <br/>
<br/>
              2. Aux termes de l'article L. 911-7 du code de justice administrative : " En cas d'inexécution totale ou partielle ou d'exécution tardive, la juridiction procède à la liquidation de l'astreinte qu'elle avait prononcée. / Sauf s'il est établi que l'inexécution de la décision provient d'un cas fortuit ou de force majeure, la juridiction ne peut modifier le taux de l'astreinte définitive lors de sa liquidation. / Elle peut modérer ou supprimer l'astreinte provisoire, même en cas d'inexécution constatée ". Aux termes de l'article R. 921-7 du même code : " Lorsqu'à la date d'effet de l'astreinte prononcée par le tribunal administratif ou la cour administrative d'appel, cette juridiction constate, d'office ou sur la saisine de la partie intéressée que les mesures d'exécution qu'elle avait prescrites n'ont pas été prises, elle procède à la liquidation de l'astreinte dans les conditions prévues aux articles L. 911-6 à L. 911-8 ". Il résulte de ces dispositions qu'il appartient à la juridiction qui a prononcé une astreinte ou qui l'a modifiée de la liquider. Par suite, lorsque la cour administrative d'appel, saisi d'un appel contre le jugement du tribunal administratif ayant rejeté la demande de liquidation provisoire d'une astreinte que ce tribunal avait précédemment prononcée, se borne à prononcer une liquidation provisoire de l'astreinte sans en modifier le taux pour l'avenir, seul le tribunal est compétent pour procéder, d'office ou à la demande d'une partie, à une nouvelle liquidation de cette astreinte.<br/>
<br/>
              3. Il suit de là que la cour administrative d'appel de Marseille n'était pas compétente pour procéder, par l'arrêt attaqué, à la liquidation provisoire de l'astreinte prononcée par le jugement du tribunal administratif de Marseille du 2 novembre 2010, dès lors que, dans son précédent arrêt du 20 décembre 2018, elle s'était bornée à procéder à une liquidation provisoire de cette astreinte, en en modérant le montant pour la période du 1er mars 2011 au 20 décembre 2018, sans en modifier le taux pour l'avenir.<br/>
<br/>
              4. Il résulte de ce qui précède, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que la ville de Marseille est fondée à demander l'annulation de l'arrêt qu'elle attaque.<br/>
<br/>
              5. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge des époux E... la somme que demande la ville de Marseille au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 11 juillet 2019 de la cour administrative d'appel de Marseille est annulé.<br/>
Article 2 : Les conclusions présentées par la ville de Marseille au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à la ville de Marseille.<br/>
Copie en sera adressée à M. C... E... et à Mme B... E....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-06-07-01-04 PROCÉDURE. JUGEMENTS. EXÉCUTION DES JUGEMENTS. ASTREINTE. LIQUIDATION DE L'ASTREINTE. - COMPÉTENCE EXCLUSIVE DE LA JURIDICTION POUR LIQUIDER L'ASTREINTE QU'ELLE A PRÉCÉDEMMENT PRONONCÉE - CONSÉQUENCE - INCOMPÉTENCE DU JUGE D'APPEL, QUI A PRONONCÉ UNE LIQUIDATION PROVISOIRE D'UNE ASTREINTE PRONONCÉE PAR LE PREMIER JUGE SANS EN MODIFIER LE TAUX, POUR PROCÉDER À UNE NOUVELLE LIQUIDATION DE CETTE ASTREINTE.
</SCT>
<ANA ID="9A"> 54-06-07-01-04 Il résulte des articles L. 911-7 et R. 921-7 du code de justice administrative qu'il appartient à la juridiction qui a prononcé une astreinte ou qui l'a modifiée de la liquider. Par suite, lorsque la cour administrative d'appel, saisi d'un appel contre le jugement du tribunal administratif ayant rejeté la demande de liquidation provisoire d'une astreinte que ce tribunal avait précédemment prononcée, se borne à prononcer une liquidation provisoire de l'astreinte sans en modifier le taux pour l'avenir, seul le tribunal est compétent pour procéder, d'office ou à la demande d'une partie, à une nouvelle liquidation de cette astreinte.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
