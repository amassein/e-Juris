<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031861230</ID>
<ANCIEN_ID>JG_L_2015_12_000000380768</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/86/12/CETATEXT000031861230.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème / 4ème SSR, 23/12/2015, 380768</TITRE>
<DATE_DEC>2015-12-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>380768</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème / 4ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE ; SCP GATINEAU, FATTACCINI</AVOCATS>
<RAPPORTEUR>Mme Manon Perrière</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:380768.20151223</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Le comité de défense du bois des Rochottes et de ses riverains et M. B...A..., d'une part, l'association Le Varne, d'autre part, ont demandé au tribunal administratif de Dijon d'annuler pour excès de pouvoir l'arrêté du 25 mars 2010 par lequel le préfet de l'Yonne a autorisé, au profit de la société " La Provençale ", la distraction du régime forestier pour un terrain situé dans ce bois. Par jugement n° 1002721, 1002775 du 31 janvier 2012, le tribunal administratif a joint ces demandes et les a rejetées. <br/>
<br/>
              Par un arrêt n° 12LY01026 du 18 mars 2014, la cour administrative d'appel de Lyon a fait droit à leurs requêtes d'appels et annulé ce jugement ainsi que l'arrêté préfectoral. <br/>
<br/>
              Par un pourvoi, enregistré le 28 mai 2014 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'agriculture, de l'agroalimentaire et de la forêt demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel du comité de défense des bois des Rochottes et de ses riverains, de M. A...et de l'association Le Varne.<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - le code forestier ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Manon Perrière, auditeur,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat du comité de défense du bois des Rochottes et de ses riverains et à la SCP Gatineau, Fattaccini, avocat de la société La Provençale ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par arrêté du 25 mars 2010, le préfet de l'Yonne a autorisé, au bénéfice de la société anonyme " La Provençale " qui y exploitait une carrière, la distraction du régime forestier de parcelles de bois situées sur le territoire de la commune de Courson-les-Carrières et appartenant en indivision à cette commune et à la commune de Fontenailles ; que, par un jugement du 31 janvier 2012, le tribunal administratif de Dijon a rejeté les demandes présentées par le comité de défense du Bois des Rochottes et de ses riverains et M. A...et par l'association Le Varne tendant à l'annulation pour excès de pouvoir de cet arrêté ; que le ministre de l'agriculture se pourvoit en cassation contre l'arrêt du 18 mars 2014 par lequel la cour administrative d'appel de Lyon, faisant droit à l'appel des demandeurs de première instance, a annulé ce jugement ainsi que l'arrêté préfectoral ;<br/>
<br/>
              Sur le mémoire " en intervention " de la société " La Provençale " :<br/>
<br/>
              2. Considérant que la société " La Provençale ", partie à l'instance devant la cour administrative d'appel de Lyon, avait qualité pour se pourvoir en cassation contre l'arrêt attaqué ; que, dès lors, sa prétendue intervention ne peut être regardée que comme un pourvoi en cassation ; que ce pourvoi n'a été enregistré au secrétariat de la section du contentieux du Conseil d'Etat que le 26 mai 2015, plus de deux mois après la date à laquelle l'arrêt attaqué avait été notifié à la société ; qu'il est, dès lors, tardif et, par suite, irrecevable ;<br/>
<br/>
              Sur le pourvoi du ministre de l'agriculture, de l'agroalimentaire et de la forêt :<br/>
<br/>
              3. Considérant, d'une part, qu'aux termes du premier alinéa de l'article L. 141-1 du code forestier, dans sa rédaction en vigueur à la date de l'arrêté litigieux, dont les dispositions ont été ultérieurement reprises à l'article L. 214-3 du nouveau code forestier : " L'application du régime forestier des bois et forêts susceptibles d'aménagement, d'exploitation régulière ou de reconstitution, et des terrains à boiser appartenant aux régions, aux départements, communes ou sections de communes, établissements publics, établissements d'utilité publique, sociétés mutualistes et caisses d'épargne, est prononcée par l'autorité administrative, le représentant de la collectivité ou personne morale intéressée entendu. En cas de désaccord, la décision est prise par arrêté ministériel " ; qu'aux termes de l'article R. 141-5 du même code, dans sa rédaction alors applicable, ultérieurement repris à l'article R. 214-2 du nouveau code forestier : " L'application du régime forestier prévue par l'article L. 141-1 est prononcée par le préfet sur la proposition de l'Office national des forêts, après avis de la collectivité ou personne morale propriétaire. / En cas de désaccord entre la collectivité ou personne morale intéressée et l'Office national des forêts, l'application du régime forestier est prononcée par arrêté du ministre chargé des forêts après avis des ministres intéressés " ;<br/>
<br/>
              4. Considérant, d'autre part, qu'aux termes du premier alinéa de l'article L. 143-2 du code forestier alors applicable, repris à l'article L. 214-5 du nouveau code : " Tout changement dans le mode d'exploitation ou l'aménagement des terrains relevant du régime forestier appartenant aux collectivités ou personnes morales mentionnées à l'article L. 141-1 fait l'objet d'une décision de l'autorité administrative après avis du représentant de la collectivité ou de la personne morale intéressée " ; qu'aux termes du premier alinéa de l'article R. 143-8 du code forestier alors applicable, repris à l'article R. 214-19 du nouveau code : " La décision prévue au premier alinéa de l'article L. 143-2 est prise par le préfet de région après consultation de l'Office national des forêts et avis de la collectivité ou personne morale propriétaire " ;<br/>
<br/>
              5. Considérant qu'en principe l'autorité administrative compétente pour modifier, abroger ou retirer un acte administratif est celle qui, à la date de la modification, de l'abrogation ou du retrait, est compétente pour prendre cet acte et, le cas échéant, s'il s'agit d'un acte individuel, son supérieur hiérarchique ; que la distraction de parcelles boisées du régime forestier s'analyse comme l'abrogation de l'acte par lequel ces parcelles avaient été soumises à ce régime et non comme un changement dans le mode d'exploitation ou l'aménagement des parcelles au sens des dispositions citées au point 4 ; que, dans le silence du code forestier sur l'autorité compétente pour prononcer la distraction, il résulte des dispositions citées au point 3 que cet acte entre dans les attributions du préfet lorsqu'il recueille l'accord tant de l'Office national des forêts que de  la collectivité ou personne morale intéressée, et dans celles du ministre chargé des forêts si cette condition n'est pas remplie ; <br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède qu'en jugeant que les dispositions des articles L. 141-1 et R. 141-5 du code forestier applicables à la date de l'arrêté préfectoral litigieux ne pouvaient suffire à fonder la compétence du préfet pour prononcer la distraction de ce régime, alors qu'il ressortait des pièces du dossier qui lui était soumis que l'Office national des forêts avait émis un avis favorable à la demande de distraction présentée par les communes, la cour a commis une erreur de droit ; que, dès lors, sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, son arrêt doit être annulé ;<br/>
<br/>
              7. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que la somme que le comité de défense du Bois des Rochottes et de ses riverains, M. A... et l'association Le Varne demandent sur leur fondement soit mise à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : Le pourvoi de la société " La Provençale " est rejeté.<br/>
Article 2 : L'arrêt de la cour administrative d'appel de Lyon du 18 mars 2014 est annulé.<br/>
Article 3 : L'affaire est renvoyée devant la cour administrative d'appel de Lyon.<br/>
Article 4 : Les conclusions du comité de défense du Bois des Rochottes et de ses riverains, de M. A... et de l'association Le Varne présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 5 : La présente décision sera notifiée au ministre de l'agriculture, de l'agroalimentaire et de la forêt, à la société " La Provençale ", au comité de défense du Bois des Rochottes et de ses riverains, à M. B... A...et à l'association Le Varne.<br/>
Copie en sera adressée aux communes de Courson-les-Carrières et de Fontenailles.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">03-06-01 AGRICULTURE ET FORÊTS. BOIS ET FORÊTS. GESTION DES FORÊTS. - DISTRACTION DE PARCELLES BOISÉES DU RÉGIME FORESTIER - PORTÉE - AUTORITÉ COMPÉTENTE [RJ1].
</SCT>
<ANA ID="9A"> 03-06-01 La distraction de parcelles boisées du régime forestier s'analyse comme l'abrogation de l'acte par lequel ces parcelles avaient été soumises à ce régime et non comme un changement dans le mode d'exploitation ou l'aménagement des parcelles au sens de l'article L. 143-2 du code forestier, repris à l'article L. 214-5 du nouveau code.,,,Dans le silence du code forestier sur l'autorité compétente pour prononcer la distraction, il résulte des articles L. 141-1 et R. 141-5 du code forestier, repris aux articles L. 214-3 et R. 214-2 du nouveau code, que cet acte entre dans les attributions du préfet lorsqu'il recueille l'accord tant de l'Office national des forêts (ONF) que de la collectivité ou personne morale intéressée, et dans celles du ministre chargé des forêts si cette condition n'est pas remplie.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Ab. jur. CE, 6 juillet 1988, Commune de Saumos, n°s 67156, 71576, aux Tables sur un autre point. Cf., sur la détermination de l'autorité compétente, CE, Section, 30 septembre 2005, Ilouane, n° 280605, p. 402.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
