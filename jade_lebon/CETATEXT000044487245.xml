<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044487245</ID>
<ANCIEN_ID>JG_L_2021_12_000000450241</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/48/72/CETATEXT000044487245.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 13/12/2021, 450241</TITRE>
<DATE_DEC>2021-12-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>450241</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE ; SCP CELICE, TEXIDOR, PERIER</AVOCATS>
<RAPPORTEUR>M. Bruno Delsol</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Arnaud Skzryerbak</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:450241.20211213</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              La société Ocean's Dream Resort a demandé au tribunal administratif de<br/>
Saint-Barthélemy d'annuler la délibération n° 2017-186 CE du 23 février 2017 par laquelle le conseil exécutif de la collectivité de Saint-Barthélemy a délivré à la société Almosnino un permis de construire pour la démolition et la reconstruction d'une résidence particulière à Gustavia. Par un jugement n° 1700024 du 22 février 2018, le tribunal administratif de Saint-Barthélemy a rejeté sa demande. <br/>
<br/>
              Par un arrêt n° 18BX01646 du 29 décembre 2020, la cour administrative d'appel de Bordeaux a rejeté l'appel formé par la société Ocean's Dream Resort contre ce jugement. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 1er mars, 1er juin et 22 novembre 2021 au secrétariat du contentieux du Conseil d'Etat, la société Ocean's Dream Resort demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ; <br/>
<br/>
              2°) de mettre à la charge de la collectivité de Saint-Barthélemy et de la société Almosnino la somme de 4 000 euros chacune au titre de l'article L. 761-1 du code de justice administrative.  <br/>
<br/>
<br/>
<br/>
                          Vu les autres pièces du dossier ;<br/>
<br/>
                          Vu :<br/>
                          - le code de l'urbanisme ; <br/>
                          - le code de l'urbanisme de Saint-Barthélemy applicable au litige ; <br/>
                          - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bruno Delsol, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Arnaud Skzryerbak, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Célice, Texidor, Perier, avocat de la Société Ocean's Dream Resort et à la SCP Piwnica, Molinié, avocat de la SCI Almosnino ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par une délibération du 23 février 2017, le conseil exécutif de la collectivité de Saint-Barthélemy a accordé à la société Almosnino un permis de construire pour une résidence particulière. La société Ocean's Dream Resort a demandé au tribunal administratif de Saint-Barthélemy d'annuler cette délibération. Elle se pourvoit en cassation contre l'arrêt en date du 29 décembre 2020 par lequel la cour administrative d'appel de Bordeaux a rejeté son appel contre le jugement du tribunal administratif de Saint-Barthélemy en date du 22 février 2018 rejetant sa demande comme irrecevable pour défaut d'intérêt pour agir.<br/>
<br/>
              2. L'article L. 600-1-3 du code de l'urbanisme dispose que : " Sauf pour le requérant à justifier de circonstances particulières, l'intérêt pour agir contre un permis de construire (...) s'apprécie à la date d'affichage en mairie de la demande du pétitionnaire ". <br/>
<br/>
              3. En premier lieu, il ressort des pièces du dossier soumis aux juges du fond que si le certificat produit par la collectivité de Saint-Barthélemy pour justifier de l'affichage en mairie de la demande de permis de construire déposée par la société Almosnino le 17 octobre 2016 ne précisait pas si le document affiché indiquait la surface et la hauteur de la construction projetée, ainsi que le prévoit l'article 75 du code de l'urbanisme de Saint-Barthélemy alors en vigueur, ce certificat était accompagné de l'avis de dépôt qui comportait ces mentions. Il s'ensuit qu'en jugeant que la demande de permis de construire de la société Almosnino avait fait l'objet d'un affichage régulier, la cour n'a en tout état de cause ni méconnu les règles d'administration de la preuve, ni dénaturé les pièces du dossier. <br/>
<br/>
              4. En second lieu, il ressort des pièces du dossier soumis aux juges du fond que la société Ocean's Dream Resort est devenue propriétaire d'un terrain voisin de la parcelle objet du permis de construire dont elle demande l'annulation postérieurement à sa délivrance à la société Almosnino. Si elle soutenait, d'une part, que son recours n'avait pour seul but que de mener à bien son propre projet et de préserver ses intérêts, à l'exclusion de toute intention malveillante, et, d'autre part, que la société Almosnino aurait entretenu la confusion en continuant à afficher sur son terrain des autorisations caduques ou retirées, la cour, par un arrêt qui est suffisamment motivé, n'a pas commis d'erreur de droit, ni d'erreur de qualification juridique des faits, en jugeant que ces circonstances ne sauraient avoir le caractère de circonstances particulières, au sens de l'article L. 600-1-3 du code de l'urbanisme cité au point 2, justifiant que son intérêt pour agir contre le permis attaqué ne soit pas apprécié à la date d'affichage de la demande de permis de construire.<br/>
<br/>
              5. Il résulte de tout ce qui précède que la société Ocean's Dream Resort n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque. Ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ne peuvent par suite qu'être rejetées. Il y a lieu, dans les circonstances de l'espèce de mettre à la charge de la société Ocean's Dream Resort une somme de 3 000 euros à verser à la société Almosnino au titre de ces mêmes dispositions.  <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la société Ocean's Dream Resort est rejeté. <br/>
Article 2 : La société Ocean's Dream Resort versera à la société Almosnino une somme de 3 000  euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 3 : La présente décision sera notifiée à la société Ocean's Dream Resort, à la collectivité de Saint-Barthélemy et à la société Almosnino.<br/>
Copie en sera adressée au ministre des outre-mer. <br/>
<br/>
Délibéré à l'issue de la séance du 29 novembre 2021 où siégeaient : M. Rémy Schwartz, président adjoint de la section du contentieux présidant ; M. G... F...,<br/>
M. Frédéric Aladjidi, présidents de chambre ; Mme I... B..., M. J... C...,<br/>
Mme A... K..., M. D... E..., M. Alain Seban, conseillers d'Etat et M. Bruno Delsol, conseiller d'Etat-rapporteur.<br/>
<br/>
              Rendu le 13 décembre 2021<br/>
<br/>
                 Le Président : <br/>
                 Signé : M. Rémy Schwartz<br/>
 		Le rapporteur : <br/>
      Signé : M. Bruno Delsol<br/>
                 Le secrétaire :<br/>
                 Signé : Mme H... L...<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-01-04-02-01 PROCÉDURE. - INTRODUCTION DE L'INSTANCE. - INTÉRÊT POUR AGIR. - EXISTENCE D'UN INTÉRÊT. - INTÉRÊT LIÉ À UNE QUALITÉ PARTICULIÈRE. - RECOURS CONTRE UNE AUTORISATION D'URBANISME - REQUÉRANT AYANT ACQUIS LA QUALITÉ DE VOISIN [RJ1] POSTÉRIEUREMENT À L'AFFICHAGE EN MAIRIE - 1) CIRCONSTANCES PARTICULIÈRES JUSTIFIANT QU'IL SOIT DÉROGÉ À LA RÈGLE D'APPRÉCIATION DE L'INTÉRÊT À AGIR À LA DATE DE CET AFFICHAGE (ART. L. 600-1-3 DU CODE DE L'URBANISME) - EXCLUSION - BONNE FOI - 2) CONSÉQUENCE EN L'ESPÈCE - IRRECEVABILITÉ DU RECOURS [RJ2].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">68-06-01-02 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. - RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. - INTRODUCTION DE L'INSTANCE. - INTÉRÊT À AGIR. - RECOURS CONTRE UNE AUTORISATION D'URBANISME - REQUÉRANT AYANT ACQUIS LA QUALITÉ DE VOISIN [RJ1] POSTÉRIEUREMENT À L'AFFICHAGE EN MAIRIE - 1) CIRCONSTANCES PARTICULIÈRES JUSTIFIANT QU'IL SOIT DÉROGÉ À LA RÈGLE D'APPRÉCIATION DE L'INTÉRÊT À AGIR À LA DATE DE CET AFFICHAGE (ART. L. 600-1-3 DU CODE DE L'URBANISME) - EXCLUSION - BONNE FOI - 2) CONSÉQUENCE EN L'ESPÈCE - IRRECEVABILITÉ DU RECOURS [RJ2].
</SCT>
<ANA ID="9A"> 54-01-04-02-01 Société devenue, postérieurement à la délivrance du permis de construire dont elle demande l'annulation, propriétaire d'un terrain voisin. Permis ayant été régulièrement affiché en mairie.......1) Société soutenant, d'une part, que son recours n'a pour seul but que de mener à bien son propre projet et de préserver ses intérêts, à l'exclusion de toute intention malveillante, et, d'autre part, que le pétitionnaire aurait entretenu la confusion en continuant à afficher sur son terrain des autorisations caduques ou retirées....Ces circonstances ne sauraient avoir le caractère de circonstances particulières, au sens de l'article L. 600-1-3 du code de l'urbanisme, justifiant que l'intérêt pour agir contre le permis attaqué ne soit pas apprécié à la date d'affichage de la demande de permis de construire.......2) Il en résulte que la demande de la société est irrecevable pour défaut d'intérêt à agir.</ANA>
<ANA ID="9B"> 68-06-01-02 Société devenue, postérieurement à la délivrance du permis de construire dont elle demande l'annulation, propriétaire d'un terrain voisin. Permis ayant été régulièrement affiché en mairie.......1) Société soutenant, d'une part, que son recours n'a pour seul but que de mener à bien son propre projet et de préserver ses intérêts, à l'exclusion de toute intention malveillante, et, d'autre part, que le pétitionnaire aurait entretenu la confusion en continuant à afficher sur son terrain des autorisations caduques ou retirées.......Ces circonstances ne sauraient avoir le caractère de circonstances particulières, au sens de l'article L. 600-1-3 du code de l'urbanisme, justifiant que l'intérêt pour agir contre le permis attaqué ne soit pas apprécié à la date d'affichage de la demande de permis de construire.......2) Il en résulte que la demande de la société est irrecevable pour défaut d'intérêt à agir.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., s'agissant de la présomption d'intérêt pour agir du voisin sous l'empire de l'article L. 600-1-2 du code de l'urbanisme, CE, 13 avril 2016, M. Bartolomei, n° 389798, p. 135....[RJ2] Comp., avant l'entrée en vigueur de l'article L. 600-1-3 du code de l'urbanisme, CE, 29 novembre 1999, Epoux Boulanger, n° 182214, T. p. 941.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
