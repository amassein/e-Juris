<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000024911141</ID>
<ANCIEN_ID>JG_L_2011_12_000000343104</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/24/91/11/CETATEXT000024911141.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 02/12/2011, 343104</TITRE>
<DATE_DEC>2011-12-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>343104</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ ; SCP DELAPORTE, BRIARD, TRICHET</AVOCATS>
<RAPPORTEUR>Mme Dominique Versini-Monod</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Claire Landais</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2011:343104.20111202</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 8 septembre et 8 décembre 2010 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. Pascal A, demeurant ... ; M. A demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 09PA01762 du 8 juillet 2010 par lequel la cour administrative d'appel de Paris a, à la demande de la commune d'Alfortville, annulé le jugement n° 0703757/4 du 22 janvier 2009 par lequel le tribunal administratif de Melun a annulé la décision du 10 mai 2007 du maire de cette commune exerçant le droit de préemption urbain sur un bien situé 69, rue Edouard-Vaillant à Alfortville ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de la commune d'Alfortville ;<br/>
<br/>
              3°) de mettre à la charge de la commune d'Alfortville le versement de la somme de 4 200 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'urbanisme ;<br/>
<br/>
              Vu la loi n° 75-1351 du 31 décembre 1975, notamment son article 10-1 ;<br/>
<br/>
              Vu la loi n° 2006-685 du 13 juin 2006 ;<br/>
<br/>
              Vu le décret n° 2009-14 du 7 janvier 2009 ; <br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Dominique Versini-Monod, Conseiller d'Etat,  <br/>
<br/>
              - les observations de la SCP Delaporte, Briard, Trichet, avocat de M. A et de la SCP Lyon-Caen, Thiriez, avocat de la commune d'Alfortville, <br/>
<br/>
              - les conclusions de Mme Claire Landais, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Delaporte, Briard, Trichet, avocat de M. A et à la SCP Lyon-Caen, Thiriez, avocat de la commune d'Alfortville ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Sur la régularité de l'arrêt attaqué :<br/>
<br/>
              Considérant que la mention de la décision par laquelle la cour administrative d'appel de Paris a été désignée parmi les juridictions pouvant appliquer la procédure expérimentale prévue à l'article 2 du décret du 7 janvier 2009 n'est pas au nombre de celles que l'arrêt attaqué devait comporter en application des dispositions de l'article R. 741-2 du code de justice administrative ; que, dès lors, le moyen tiré de l'absence d'une telle mention ne peut qu'être écarté ;<br/>
<br/>
              Sur le bien-fondé de l'arrêt attaqué : <br/>
<br/>
              Considérant, d'une part, que le I de l'article 1er de loi du 13 juin 2006 relative au droit de préemption et à la protection des locataires en cas de vente d'un immeuble a inséré dans la loi du 31 décembre 1975 relative à la protection des occupants de locaux à usage d'habitation un article 10-1, selon lequel : " I. - A. - Préalablement à la conclusion de la vente, dans sa totalité et en une seule fois, d'un immeuble à usage d'habitation (...) de plus de dix logements au profit d'un acquéreur ne s'engageant pas à proroger les contrats de bail à usage d'habitation en cours à la date de la conclusion de la vente afin de permettre à chaque locataire ou occupant de bonne foi de disposer du logement qu'il occupe pour une durée de six ans à compter de la signature de l'acte authentique de vente (...) le bailleur doit faire connaître (...) à chacun des locataires ou occupants de bonne foi l'indication du prix et des conditions de la vente, dans sa totalité et en une seule fois, de l'immeuble ainsi que l'indication du prix et des conditions de la vente pour le local qu'il occupe (...). / B. - Préalablement à la conclusion de la vente mentionnée au premier alinéa du A, le bailleur communique au maire de la commune sur le territoire de laquelle est situé l'immeuble le prix et les conditions de la vente de l'immeuble dans sa totalité et en une seule fois. Lorsque l'immeuble est soumis à l'un des droits de préemption institués par les chapitres Ier et II du titre Ier du livre II du code de l'urbanisme, la déclaration préalable faite au titre de l'article L. 213-2 du même code vaut communication au sens du présent article. / II.- Les dispositions du I ne sont pas applicables en cas d'exercice de l'un des droits de préemption institués par le titre Ier du livre II du code de l'urbanisme ou lorsque la vente intervient entre parents ou alliés jusqu'au quatrième degré inclus (...) " ; <br/>
<br/>
              Considérant, d'autre part, qu'aux termes de l'article L. 210-2 introduit dans le code de l'urbanisme par le II du même article 1er de la loi du 13 juin 2006 : " En cas de vente d'un immeuble à usage d'habitation, la commune peut faire usage de son droit de préemption pour assurer le maintien dans les lieux des locataires " ; <br/>
<br/>
              Considérant qu'il résulte des termes mêmes de ces dernières dispositions que le motif de préemption qu'elles instituent au profit des communes détentrices d'un droit de préemption peut s'appliquer à tout immeuble à usage d'habitation, et non pas seulement aux immeubles de plus de dix logements visés par l'article 10-1 de la loi du 31 décembre 1975 ; que, par suite, la cour administrative d'appel de Paris n'a pas commis d'erreur de droit en jugeant, par l'arrêt attaqué, que la commune d'Alfortville avait légalement pu fonder la décision de préemption en litige sur le fait qu'elle entendait assurer, conformément à l'article L. 210-2 du code de l'urbanisme, le maintien des locataires dans les lieux, alors même que l'immeuble préempté ne comportait que huit logements et ne relevait ainsi pas du champ d'application de l'article 10-1 de la loi du 31 décembre 1975 ; <br/>
<br/>
              Considérant que, ainsi qu'il a été dit ci-dessus, l'applicabilité à l'ensemble des immeubles à usages d'habitation du motif de préemption prévu par l'article L. 210-1 du code de l'urbanisme résulte des termes mêmes de cet article ; que la cour n'était, dès lors, pas tenue de motiver davantage sa décision au regard des développements, même très circonstanciés, consacrés sur ce point par le requérant aux travaux préparatoires de la loi du 13 juin 2006 ; qu'elle n'a pas, ce faisant, dénaturé les termes de la requête qui lui était soumise ni entaché son arrêt d'insuffisance de motivation ;  <br/>
<br/>
              Considérant, enfin, qu'en estimant qu'il n'existait aucun désaccord de la commune d'Alfortville sur le prix mentionné dans la déclaration d'intention d'aliéner, la cour a porté sur les faits de l'espèce une appréciation souveraine qui n'est pas entachée de dénaturation ; <br/>
<br/>
              Considérant qu'il résulte de tout ce qui précède que le pourvoi de M. A doit être rejeté, y compris ses conclusions au titre de l'article L. 761-1 du code de justice administrative ; que, dans les circonstances de l'espèce, il y a lieu de mettre à sa charge la somme de 3 000 euros à verser à la commune d'Alfortville au titre des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. A est rejeté.<br/>
Article 2 : M. A versera à la commune d'Alfortville une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 3 : La présente décision sera notifiée à M. Pascal A, à la commune d'Alfortville et à la ministre de l'écologie, du développement durable, des transports et du logement.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">68-02-01-01 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PROCÉDURES D'INTERVENTION FONCIÈRE. PRÉEMPTION ET RÉSERVES FONCIÈRES. DROITS DE PRÉEMPTION. - EXERCICE PAR UNE COMMUNE DE SON DROIT DE PRÉEMPTION POUR ASSURER LE MAINTIEN DANS LES LIEUX DES LOCATAIRES (ART. L. 210-2 DU CODE DE L'URBANISME) - CHAMP D'APPLICATION - TOUT IMMEUBLE À USAGE D'HABITATION, Y COMPRIS DE MOINS DE DIX LOGEMENTS.
</SCT>
<ANA ID="9A"> 68-02-01-01 Il résulte des termes mêmes des dispositions de l'article L. 210-2, introduit dans le code de l'urbanisme par le II de l'article 1er de la loi n° 2006-685 du 13 juin 2006, que le motif de préemption (maintien dans les lieux des locataires) qu'elles instituent au profit des communes détentrices d'un droit de préemption peut s'appliquer à tout immeuble à usage d'habitation, et non pas seulement aux immeubles de plus de dix logements visés par l'article 10-1 de la loi n° 75-1351 du 31 décembre 1975.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
