<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032928899</ID>
<ANCIEN_ID>JG_L_2016_07_000000397237</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/92/88/CETATEXT000032928899.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 22/07/2016, 397237</TITRE>
<DATE_DEC>2016-07-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>397237</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR>M. Grégory Rzepski</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier Henrard</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:397237.20160722</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              En application de l'article L. 52-15 du code électoral, la Commission nationale des comptes de campagne et des financements politiques (CNCCFP) a saisi le tribunal administratif de Bordeaux de la décision du 28 octobre 2015 par laquelle elle a rejeté le compte de campagne de M. A...D... et Mme C...B..., candidats aux élections départementales qui se sont déroulées les 22 et 29 mars 2015 dans le canton de Lavardac (Lot-et-Garonne).<br/>
<br/>
              Par un jugement n° 1505050 du 25 janvier 2016, le tribunal administratif de Bordeaux a déclaré M. D...et Mme B...inéligibles pour une durée de deux ans.<br/>
<br/>
              Par une requête et un mémoire complémentaire, enregistrés les 23 février et 23 mars 2016 au secrétariat du contentieux du Conseil d'Etat, M. D...et Mme B...demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) de rejeter la saisine de la Commission nationale des comptes de campagne et des financements politiques ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code électoral ;<br/>
              - la loi n° 2015-29 du 16 janvier 2015 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Grégory Rzepski, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Olivier Henrard, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Matuchansky, Poupot, Valdelièvre, avocat de M. D...et de Mme B...;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il résulte de l'instruction que, par une décision du 28 octobre 2015, la Commission nationale des comptes de campagne et des financements politiques a rejeté le compte de campagne déposé par M. D...et MmeB..., binôme candidat aux élections départementales qui se sont déroulées les 22 et 29 mars 2015 dans le canton de Lavardac (Lot-et-Garonne), au motif que ce compte n'a pas été présenté par un membre de l'ordre des experts-comptables et des comptables agréés et que les candidats ont payé directement, après désignation de leur mandataire, 1 142 euros de dépenses, soit 24,98 % du montant total des dépenses et 10,7 % du plafond des dépenses ; que, saisi par la Commission nationale des comptes de campagne et des financements politiques en application de l'article L. 52-15 du code électoral, le tribunal administratif de Bordeaux, par un jugement du 25 janvier 2016, a déclaré M. D...et Mme B...inéligibles pour une durée de deux ans ; que les intéressés font appel de ce jugement ;<br/>
<br/>
              2. Considérant qu'en application de l'article L. 52-15 du même code, lorsque la Commission nationale des comptes de campagne et des financements politiques " a constaté que le compte de campagne n'a pas été déposé dans le délai prescrit, si le compte a été rejeté ou si, le cas échéant après réformation, il fait apparaître un dépassement du plafond des dépenses électorales, la commission saisit le juge de l'élection (...) " ; <br/>
<br/>
              Sur la régularité du jugement attaqué :<br/>
<br/>
              3. Considérant qu'il résulte des dispositions des articles L. 191, L. 210-1 et L. 221 du code électoral dans leur version issue de la loi du 16 janvier 2015 que le législateur a instauré un mode de scrutin majoritaire binominal à deux tours sans panachage ni vote préférentiel, afin d'assurer la parité au sein des conseils départementaux, et a retenu le principe de solidarité des candidats d'un même binôme ; que cette solidarité conduit à ce que les membres d'un même binôme soient tous les deux déclarés inéligibles en cas de méconnaissance des règles relatives au financement des campagnes électorales ; que, par suite, elle impose que chaque membre soit mis en cause devant le juge de l'élection lorsque celui-ci se trouve saisi par la Commission nationale des comptes de campagne et des financements politiques en application de l'article L. 52-15 du code électoral ; <br/>
<br/>
              4. Considérant qu'il résulte des dispositions de l'article R. 611-1 du code de justice administrative que le tribunal administratif est tenu d'ordonner la communication aux parties des requêtes introductives d'instance sous la forme de copies qui leur sont notifiées ; qu'il résulte de l'article R. 611-3 du même code que la requête doit être notifiée au moyen de lettres remises contre signature ou de tout autre dispositif permettant d'attester la date de réception ; qu'il résulte de l'instruction que si le tribunal administratif de Bordeaux pouvait régulièrement envoyer la saisine de la Commission nationale des comptes de campagne et des financements politiques à l'adresse commune indiquée par M. D...et Mme B...lors de leur envoi du compte de campagne à la commission, le jugement a été rendu au terme d'une procédure irrégulière en l'absence de toute preuve de cet envoi à chacun des membres du binôme ; qu'il y a lieu, par suite, d'en prononcer l'annulation ; <br/>
<br/>
              5. Considérant que le délai imparti au tribunal administratif par l'article R. 114 du code électoral pour statuer sur la saisine de la Commission nationale des comptes de campagne et des financements politiques est expiré ; que, dès lors, il y a lieu pour le Conseil d'Etat de statuer immédiatement sur cette saisine ; <br/>
<br/>
              Sur la procédure devant la commission :<br/>
<br/>
              6. Considérant qu'il résulte des dispositions de l'article L. 52-15 du code électoral citées au point 2. que la procédure par laquelle la Commission nationale des comptes de campagne et des financements politiques approuve, rejette ou réforme les comptes de campagne des candidats aux élections revêt un caractère contradictoire ; qu'il incombe, à ce titre, à la commission d'informer les candidats des motifs pour lesquels elle envisage de rejeter leur compte ; <br/>
<br/>
              7. Considérant qu'il résulte de l'instruction qu'à la suite du dépôt de leur compte de campagne, la commission a adressé une lettre recommandée à M. D...et MmeB..., le 27 juillet 2015, pour les informer que l'absence de présentation de leur compte de campagne par un membre de l'ordre des experts comptables et des comptables agréés, en méconnaissance des dispositions de l'article L. 52-12 du code électoral, et le paiement direct de dépenses par les candidats après la désignation de leur mandataire, contrairement aux dispositions de l'article L. 52-4 du code électoral, étaient des irrégularités susceptibles d'entraîner le rejet de leur compte de campagne ; que contrairement à ce qui est soutenu, Mme B... a bien reçu le courrier de la commission, envoyé par lettre recommandée, ainsi que la lettre de relance adressée le 19 août suivant ; que par suite la commission a satisfait aux exigences de la procédure contradictoire régie par l'article L. 52-15 du code électoral ; que le moyen tiré de ce que le juge de l'élection n'aurait pas, pour ce motif, été régulièrement saisi par la Commission nationale des comptes de campagne et des financements politiques ne peut donc qu'être écarté ;<br/>
<br/>
<br/>
              Sur le bien-fondé du rejet du compte de campagne :<br/>
<br/>
              8. Considérant que lorsque la Commission nationale des comptes de campagne et des financements politiques, après avoir rejeté le compte d'un candidat, saisit régulièrement le juge de l'élection, cette saisine conduit nécessairement le juge, avant de rechercher s'il y a lieu ou non de prononcer l'inéligibilité du candidat et, s'il s'agit d'un candidat proclamé élu, d'annuler son élection ou de le déclarer démissionnaire d'office, à apprécier si le compte de campagne a été rejeté à bon droit par la commission ; <br/>
<br/>
              9. Considérant qu'aux termes de l'article L. 52-4 du code électoral : " Tout candidat à une élection déclare un mandataire conformément aux articles L. 52-5 et L. 52-6 au plus tard à la date à laquelle sa candidature est enregistrée. Ce mandataire peut être une association de financement électoral, ou une personne physique dénommée " le mandataire financier ". Un même mandataire ne peut être commun à plusieurs candidats. Le mandataire recueille, pendant l'année précédant le premier jour du mois de l'élection et jusqu'à la date du dépôt du compte de campagne du candidat, les fonds destinés au financement de la campagne. Il règle les dépenses engagées en vue de l'élection et antérieures à la date du tour de scrutin où elle a été acquise, à l'exception des dépenses prises en charge par un parti ou groupement politique. Les dépenses antérieures à sa désignation payées directement par le candidat ou à son profit, ou par l'un des membres d'un binôme de candidats ou au profit de ce membre, font l'objet d'un remboursement par le mandataire et figurent dans son compte bancaire ou postal (...) " ; qu'aux termes de l'article L. 52-12 du même code: " Chaque candidat ou candidat tête de liste soumis au plafonnement prévu à l'article L. 52-11 et qui a obtenu au moins 1 % des suffrages exprimés est tenu d'établir un compte de campagne retraçant, selon leur origine, l'ensemble des recettes perçues et, selon leur nature, l'ensemble des dépenses engagées ou effectuées en vue de l'élection, hors celles de la campagne officielle par lui même ou pour son compte, au cours de la période mentionnée à l'article L. 52-4. (...) / Au plus tard avant 18 heures le dixième vendredi suivant le premier tour de scrutin, chaque candidat ou candidat tête de liste présent au premier tour dépose à la Commission nationale des comptes de campagne et des financements politiques son compte de campagne et ses annexes accompagné des justificatifs de ses recettes ainsi que des factures, devis et autres documents de nature à établir le montant des dépenses payées ou engagées par le candidat ou pour son compte. Le compte de campagne est présenté par un membre de l'ordre des experts comptables et des comptables agréés ; celui-ci met le compte de campagne en état d'examen et s'assure de la présence des pièces justificatives requises. Cette présentation n'est pas nécessaire lorsque aucune dépense ou recette ne figure au compte de campagne. Dans ce cas, le mandataire établit une attestation d'absence de dépense et de recette. (...) " ;<br/>
<br/>
              10. Considérant, d'une part, qu'il résulte de l'instruction et qu'il n'est d'ailleurs pas contesté que le compte de campagne de M. D...et de MmeB..., qui ont engagé des dépenses et des recettes, n'a pas été déposé par un membre de l'ordre des experts-comptables et des comptables agréés en méconnaissance des exigences posées par les dispositions précitées de l'article L. 52-12 du code électoral ; que ce motif justifie le rejet du compte de campagne ; <br/>
<br/>
              11. Considérant, d'autre part, que si, par dérogation à la formalité substantielle que constitue l'obligation de recourir à un mandataire pour toute dépense effectuée en vue de sa campagne, le règlement direct de menues dépenses par le candidat peut être admis, ce n'est qu'à la double condition que leur montant, en vertu des dispositions de l'article L. 52-4 du code électoral, c'est à dire prenant en compte non seulement les dépenses intervenues après la désignation du mandataire financier mais aussi celles réglées avant cette désignation et qui n'auraient pas fait l'objet d'un remboursement par le mandataire, soit faible par rapport au total des dépenses du compte de campagne et négligeable au regard du plafond de dépenses autorisées fixé par l'article L. 52-11 du code électoral ; qu'il résulte de l'instruction que M. D...et Mme B...ont payé directement, après désignation de leur mandataire, diverses factures d'un montant total de 1 142 euros ; que le total des dépenses ainsi acquittées en méconnaissance des dispositions précitées de l'article L. 52-4 du code électoral représente près de 25 % du montant total des dépenses et 10,7 % du plafond des dépenses alors autorisées pour le canton de Lavardac ; qu'un tel montant ne peut être regardé comme faible au regard du total des dépenses du compte de campagne et négligeable au regard du plafond de dépenses autorisées ; que ce motif justifie également à lui seul le rejet du compte de campagne de M. D...et de Mme B... ;<br/>
<br/>
              12. Considérant que, dans ces conditions, c'est à bon droit que la Commission nationale des comptes de campagne et des financements politiques a rejeté leur compte de campagne et a saisi le juge électoral ;<br/>
<br/>
              Sur l'inéligibilité de M. D...et de Mme B...:<br/>
<br/>
              13. Considérant qu'aux termes de l'article L. 118-3 du code électoral : " Saisi par la commission instituée par l'article L. 52-14, le juge de l'élection peut déclarer inéligible le candidat dont le compte de campagne, le cas échéant après réformation, fait apparaître un dépassement du plafond des dépenses électorales. / Saisi dans les mêmes conditions, le juge de l'élection peut déclarer inéligible le candidat qui n'a pas déposé son compte de campagne dans les conditions et le délai prescrits à l'article L. 52-12. / Il prononce également l'inéligibilité du candidat dont le compte de campagne a été rejeté à bon droit en cas de volonté de fraude ou de manquement d'une particulière gravité aux règles relatives au financement des campagnes électorales. /... " ; <br/>
              En ce qui concerne l'absence de dépôt du compte dans les conditions prescrites à l'article L. 52-12 :<br/>
<br/>
              14. Considérant que pour apprécier s'il y a lieu de déclarer inéligible, sur le fondement du deuxième alinéa de l'article L. 118-3 du code électoral cité ci-dessus,  un candidat qui n'a pas déposé son compte de campagne dans les conditions et le délai prescrits à l'article L. 52-12, il appartient au juge de l'élection de tenir compte de la nature de la règle méconnue, du caractère délibéré ou non du manquement, de l'existence éventuelle d'autres motifs d'irrégularité du compte et du montant des sommes en cause ;<br/>
<br/>
              15. Considérant qu'il résulte de l'instruction que le compte de campagne de M. D... et Mme B...n'a pas été déposé devant la Commission  des comptes de campagne et des financements politiques par un membre de l'ordre des experts comptables et des comptables agréés, alors pourtant que la commission les avait invités à régulariser cette omission dès le 27 juillet 2015 ; que compte tenu du caractère substantiel de la formalité qu'ils ont méconnue, laquelle est destinée à assurer un contrôle effectif des comptes de campagne par la commission dans le délai limité qui lui est imparti, et de l'ensemble des circonstances de l'espèce, le manquement aux dispositions de l'article L. 52-12 constaté par la Commission nationale des comptes de campagne et des financements politiques est de nature à justifier le prononcé d'une inéligibilité ;<br/>
<br/>
              En ce qui concerne les dépenses effectuées directement :<br/>
<br/>
              16. Considérant que, pour déterminer si un manquement est d'une particulière gravité au sens des dispositions du troisième alinéa de l'article L. 118-3 cité ci dessus, il incombe au juge de l'élection d'apprécier, d'une part, s'il s'agit d'un manquement caractérisé à une règle substantielle relative au financement des campagnes électorales, d'autre part, s'il présente un caractère délibéré ; qu'en cas de manquement aux dispositions de l'article L. 52-4 du code électoral, il incombe, en outre, au juge de tenir compte de l'existence éventuelle d'autres motifs d'irrégularité du compte, du montant des sommes en cause ainsi que de l'ensemble des circonstances de l'espèce ;<br/>
<br/>
              17. Considérant qu'il résulte de l'instruction, que, en l'espèce, les dépenses électorales réglées sans recourir au mandataire financier, contrairement aux dispositions de l'article L. 52-4 du code électoral dont les prescriptions, dépourvues d'ambiguïté, présentent un caractère substantiel, se sont élevées à 1 142 euros ; que le montant global de ces dépenses directement acquittées, relatives à des frais de réception, qui ne sont ni faibles par rapport au total des dépenses du compte de campagne, ni négligeables au regard du plafond de dépenses autorisées, n'est pas demeuré limité ; qu'en outre, ainsi qu'il a été dit au point 15, le compte de campagne de M. D...et Mme B...fait apparaître une autre irrégularité ; qu'il résulte de ce qui précède que le manquement commis par M. D...et Mme B...doit être qualifié de manquement d'une particulière gravité, au sens et pour l'application de l'article L. 118 3 du code électoral, de nature à justifier aussi le prononcé d'une inéligibilité ;<br/>
<br/>
              18. Considérant qu'il résulte de tout ce qui précède que, dans les circonstances de l'espèce, eu égard notamment au caractère substantiel des règles méconnues, au caractère délibéré de ces manquements, à leur cumul et à l'absence de toute justification de la part des candidats mis en cause, il y a lieu, en application des dispositions de l'article L. 118-3 du code électoral, de déclarer M. D...et Mme B...inéligibles pendant dix-huit mois à compter de la date de la présente décision ;<br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              19. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par M. D...et Mme B...au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Bordeaux du 25 janvier 2016 est annulé.<br/>
Article 2 : Le compte de campagne de M. D...et de Mme B...a été rejeté à bon droit.<br/>
Article 3 : M. D...et Mme B...sont déclarés inéligibles pour une durée de dix-huit mois.<br/>
Article 4 : Les conclusions de M. D...et Mme B...tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à M. A...D..., à Mme C...B...et à la Commission nationale des comptes de campagne et des financements politiques.<br/>
Copie en sera adressée au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">28-08-02 ÉLECTIONS ET RÉFÉRENDUM. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. INSTRUCTION. - ELECTION DES CONSEILS DÉPARTEMENTAUX - SAISINE DU JUGE PAR LA CNCCFP APRÈS EXAMEN DU COMPTE DE CAMPAGNE D'UN BINÔME DE CANDIDATS - 1) OBLIGATION DE METTRE EN CAUSE LES DEUX MEMBRES DU BINÔME - 2) POSSIBILITÉ D'UTILISER L'ADRESSE COMMUNE INDIQUÉE À LA CNCCFP PAR LES CANDIDATS - EXISTENCE.
</SCT>
<ANA ID="9A"> 28-08-02 1) Par la loi n° 2015-29 du 16 janvier 2015, le législateur a instauré un mode de scrutin majoritaire binominal à deux tours sans panachage ni vote préférentiel, afin d'assurer la parité au sein des conseils départementaux, et a retenu le principe de solidarité des candidats d'un même binôme, qui conduit à ce que les membres d'un même binôme soient tous les deux déclarés inéligibles en cas de méconnaissance des règles relatives au financement des campagnes électorales. Par suite, cette solidarité impose que chaque membre soit mis en cause devant le juge de l'élection lorsque celui-ci se trouve saisi par la Commission nationale des comptes de campagne et des financements politiques (CNCCFP) en application de l'article L. 52-15 du code électoral.,,,2) Un tribunal administratif ne commet pas d'irrégularité en envoyant la saisine de la CNCCFP à l'adresse commune indiquée par le binôme de candidats lors de leur envoi du compte de campagne à la commission. Il doit, en vertu de l'article R. 611-3 du code de justice administrative, notifier cette saisine au moyen de lettres remises contre signature ou de tout autre dispositif permettant d'attester la date de réception.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
