<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025210364</ID>
<ANCIEN_ID>JG_L_2012_01_000000348725</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/21/03/CETATEXT000025210364.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème et 2ème sous-sections réunies, 23/01/2012, 348725</TITRE>
<DATE_DEC>2012-01-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>348725</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème et 2ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>FOUSSARD ; SCP COUTARD, MUNIER-APAIRE</AVOCATS>
<RAPPORTEUR>M. Fabrice Aubert</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Boulouis</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:348725.20120123</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi et le mémoire complémentaire, enregistrés les 22 avril et 24 juin 2011 au secrétariat du contentieux du Conseil d'Etat, présenté pour le DEPARTEMENT DES BOUCHES-DU-RHÔNE, représenté par le président du conseil général ; le département demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 07MA04439 du 21 février 2011 de la cour administrative d'appel de Marseille, en tant que, après avoir rejeté l'appel principal de la société France Assist tendant à l'annulation du jugement n° 0402587 du 19 septembre 2007 du tribunal administratif de Marseille, il a rejeté les conclusions de son appel incident dirigées contre ce jugement, en ce qu'il l'a condamné à verser à cette société une somme de 310 000 euros en réparation du préjudice subi du fait de l'absence de commande à hauteur du montant minimum fixé pour la première année d'exécution d'un marché de services, ainsi que ses conclusions tendant à ce qu'il soit enjoint à cette société de lui rembourser les sommes déjà payées en exécution de ce jugement ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit aux conclusions de son appel incident et à ses conclusions à fin d'injonction ; <br/>
<br/>
              3°) de mettre à la charge de la société France Assist le versement de la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code des marchés publics ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Fabrice Aubert, Auditeur,  <br/>
<br/>
              - les observations de Me Foussard, avocat du DEPARTEMENT DES BOUCHES-DU-RHÔNE et de la SCP Coutard, Munier-Apaire, avocat de la société France Assist, <br/>
<br/>
              - les conclusions de M. Nicolas Boulouis, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à Me Foussard, avocat du DEPARTEMENT DES BOUCHES-DU-RHÔNE et à la SCP Coutard, Munier-Apaire, avocat de la société France Assist ;<br/>
<br/>
<br/>
<br/>Considérant qu'il ressort des pièces du dossier soumis au juge du fond que le DEPARTEMENT DES BOUCHES-DU-RHÔNE a conclu le 11 juillet 2002 avec la société France Assist un marché à bons de commande portant sur la mise en place d'un service de <br/>
télé-accompagnement des personnes âgées ; que le contrat prévoyait un montant minimal annuel de prestations de 1 050 000 euros pour la première année d'exécution et a été reconduit pour une seconde année avec un montant minimum annuel de commandes fixé à 1 070 000 euros ; que, le montant des commandes effectivement passées ayant été inférieur aux niveaux ainsi fixés pour chacune de ces périodes, la société France Assist a saisi le juge administratif d'une demande tendant à la condamnation du département à lui verser la différence entre ces sommes et les montants minimaux fixés par le marché ; que la cour administrative d'appel de Marseille, après avoir annulé le jugement du 19 septembre 2007 du tribunal administratif de Marseille en tant seulement qu'il condamne le département à indemniser la société de son manque à gagner au titre de la deuxième année du marché, a confirmé ce jugement en tant qu'il condamne le département à verser à la société France Assist une somme de 310 000 euros au titre de la première année d'exécution du marché ; que le DEPARTEMENT DES BOUCHES-DU-RHÔNE se pourvoit en cassation contre cet arrêt en tant qu'il rejette les conclusions de son appel incident tendant, d'une part, à l'annulation du jugement de première instance en tant que ce dernier le condamne au titre de la première année du marché et, d'autre part, à ce qu'il soit enjoint à la société France Assist de lui rembourser les sommes déjà versées en exécution de ce jugement ; <br/>
<br/>
              Considérant, en premier lieu, qu'aux termes de l'article 34.1 du cahier des clauses administratives générales relatif aux marchés publics de fournitures courantes et de services, dans sa rédaction applicable au litige : " Tout différend entre le titulaire et la personne responsable du marché doit faire l'objet de la part du titulaire d'un mémoire de réclamation qui doit être communiqué à la personne responsable du marché dans le délai de trente jours compté à partir du jour où le différend est apparu " ; que l'article 2.4 de ce même cahier, fixant les modalités de décompte des délais, dispose d'autre part que : " 2.41. Tout délai imparti dans le marché à la personne publique ou à la personne responsable du marché ou au titulaire, commence à courir le lendemain du jour où s'est produit le fait qui sert de point de départ à ce délai. / 2.42. Lorsque le délai est fixé en jours, il s'entend en jours de calendrier et il expire à la fin du dernier jour de la durée prévue. " ; que, par suite, après avoir estimé que la société France Assist avait, en mettant le département en demeure de prendre position sur sa rémunération au titre de la première année du marché avant le 17 décembre 2003, fait naître le différend à cette date, la cour administrative d'appel de Marseille ne pouvait, sans commettre d'erreur de droit, juger que le mémoire en réclamation de la société reçu le lundi 19 janvier 2004 par le département, soit après l'expiration, le vendredi 16 janvier à minuit, du délai de trente jours prévu au cahier des clauses administratives générales, n'était pas tardif ; que, dès lors et sans qu'il soit besoin d'examiner les autres moyens du pourvoi soulevés à ce titre, son arrêt doit être annulé en tant qu'il statue sur la rémunération de la société France Assist pour la première année d'exécution du contrat ;<br/>
<br/>
              Considérant, en deuxième lieu, que pour rejeter les conclusions à fin d'injonction présentées par le DEPARTEMENT DES BOUCHES-DU-RHÔNE, la cour administrative d'appel de Marseille a relevé d'office leur irrecevabilité, sans que celle-ci ait fait l'objet de l'information préalable des parties prévue à l'article R. 611-7 du code de justice administrative ; que, dès lors, le département est également fondé à demander l'annulation de l'arrêt attaqué sur ce point ;<br/>
<br/>
              Considérant qu'il y a lieu, en application de l'article L. 821-2 du code de justice administrative, de régler l'affaire au fond dans les limites de la cassation prononcée ;<br/>
<br/>
              Considérant, en premier lieu, que la société France Assist a, ainsi qu'il a été dit, adressé au département une mise en demeure fixant au 17 décembre 2003 la date au-delà de laquelle elle considèrerait l'absence de réponse à sa réclamation comme signifiant un rejet de celle-ci ; que cette date doit donc être regardée comme étant celle de la naissance du différend l'opposant au département au sujet de sa rémunération pour la première année du marché ; qu'il résulte de ce qui a été dit plus haut que le mémoire de réclamation parvenu à son destinataire le 19 janvier 2004, était tardif ; que, par suite, les conclusions de la société France Assist dirigées contre le refus du département de l'indemniser de son manque à gagner pour la première année d'exécution du marché étaient irrecevables ; que le département est, par suite, fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Marseille l'a condamné à verser à la société France Assist la somme de 310 000 euros au titre de la première période du marché ; <br/>
<br/>
              Considérant, en second lieu, qu'il résulte des dispositions de l'article L. 11 du code de justice administrative que les décisions des juridictions administratives sont exécutoires ; que, lorsque le juge d'appel infirme une condamnation prononcée en première instance, sa décision, dont l'expédition notifiée aux parties est revêtue de la formule exécutoire prévue à l'article R. 751-1 du code de justice administrative, permet par elle-même d'obtenir, au besoin d'office, le remboursement de sommes déjà versées en vertu de cette condamnation ; qu'ainsi les conclusions du DEPARTEMENT DES BOUCHES-DU-RHÔNE tendant à ce qu'il soit enjoint à la société France Assist de lui rembourser les sommes versées en exécution du jugement du 19 septembre 2007 du tribunal administratif de Marseille, infirmé en appel, sont sans objet ; qu'elles ne peuvent, par suite, qu'être rejetées ; <br/>
<br/>
<br/>
              Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mis à la charge du DEPARTEMENT DES BOUCHES-DU-RHÔNE qui n'est pas, dans la présente instance, la partie perdante, le versement à la société France Assist de la somme qu'elle demande au titre des frais exposés par elle et non compris dans les dépens ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions du département au titre des mêmes dispositions ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 21 février 2011 de la cour administrative d'appel de Marseille est annulé, en tant qu'il statue sur la rémunération de la société France Assist au titre de la première année d'exécution du marché et sur les conclusions à fin d'injonction du DEPARTEMENT DES BOUCHES-DU-RHÔNE.<br/>
Article 2 : Le jugement du 19 septembre 2007 du tribunal administratif de Marseille est annulé, en tant qu'il condamne le DEPARTEMENT DES BOUCHES-DU-RHÔNE à verser à la société France Assist la somme de 310 000 euros.<br/>
Article 3 : Le surplus des conclusions du DEPARTEMENT DES BOUCHES-DU-RHÔNE devant le Conseil d'Etat et devant la cour administrative d'appel de Marseille est rejeté. <br/>
Article 4 : Les conclusions de la société France Assist au titre de l'article L. 761-1 du code de justice administrative et les conclusions de sa demande devant le tribunal administratif de Marseille au titre de la première année du marché sont rejetées. <br/>
Article 5 : La présente décision sera notifiée au DEPARTEMENT DES <br/>
BOUCHES-DU-RHÔNE et à la société France Assist.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39 NATURE ET ENVIRONNEMENT. - DIFFÉREND ENTRE LE TITULAIRE ET LA PERSONNE RESPONSABLE DU MARCHÉ (ART. 34. 1 DU CCAG FOURNITURES COURANTES ET SERVICES) - NOTION.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-06-07 PROCÉDURE. JUGEMENTS. EXÉCUTION DES JUGEMENTS. - CARACTÈRE EXÉCUTOIRE DES JUGEMENTS (ART. L. 11 DU CJA) - CONSÉQUENCE - ARRÊT INFIRMANT EN APPEL UN JUGEMENT PRONONÇANT UNE CONDAMNATION - DÉCISION SUFFISANTE POUR OBTENIR LE REMBOURSEMENT.
</SCT>
<ANA ID="9A"> 39 Aux termes de l'article 34.1 du cahier des clauses administratives générales relatif aux marchés publics de fournitures courantes et de services : « Tout différend entre le titulaire et la personne responsable du marché doit faire l'objet de la part du titulaire d'un mémoire de réclamation qui doit être communiqué à la personne responsable du marché dans le délai de trente jours compté à partir du jour où le différend est apparu ».,,Titulaire d'un marché ayant adressé à la personne responsable du marché une mise en demeure fixant la date au-delà de laquelle elle considèrerait l'absence de réponse à sa réclamation comme signifiant un rejet de celle-ci. Cette date doit être regardée comme étant celle de la naissance du différend entre les parties au sens de l'article 34.1 du CCAG.</ANA>
<ANA ID="9B"> 54-06-07 Il résulte des dispositions de l'article L. 11 du code de justice administrative (CJA) que les décisions des juridictions administratives sont exécutoires. Lorsque le juge d'appel infirme une condamnation prononcée en première instance, sa décision, dont l'expédition notifiée aux parties est revêtue de la formule exécutoire prévue à l'article R. 751-1 du CJA, permet par elle-même d'obtenir, au besoin d'office, le remboursement de sommes déjà versées en vertu de cette condamnation.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
