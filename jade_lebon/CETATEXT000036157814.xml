<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036157814</ID>
<ANCIEN_ID>JG_L_2017_12_000000398537</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/15/78/CETATEXT000036157814.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 1ère chambres réunies, 06/12/2017, 398537</TITRE>
<DATE_DEC>2017-12-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>398537</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN ; SCP GASCHIGNARD</AVOCATS>
<RAPPORTEUR>Mme Mireille Le Corre</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Louis Dutheillet de Lamothe</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:398537.20171206</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. et Mme B...et l'association Environnement et paysage en Haute-Bretagne ont demandé au tribunal administratif de Rennes d'annuler pour excès de pouvoir l'arrêté du 4 octobre 2012 par lequel le maire de Bazouges-la-Pérouse (Ille-et-Vilaine) a accordé à M. C...un permis de construire en vue de l'édification d'un bâtiment d'élevage de porcs destiné à regrouper sur un seul site ses installations. Par un jugement n° 1205051 du 19 décembre 2014, le tribunal administratif de Rennes a fait droit à leur demande.<br/>
<br/>
              Par un arrêt nos 15NT00328, 15NT00571 du 5 février 2016, la cour administrative d'appel de Nantes a, sur l'appel de M. C...et de la commune de Bazouges-la-Pérouse, annulé ce jugement et rejeté la demande.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 5 avril et 5 juillet 2016 et le 11 juillet 2017 au secrétariat du contentieux du Conseil d'Etat, M. et Mme B...demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de M. C...et de la commune de Bazouges-la-Pérouse ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Bazouges-la-Pérouse et de M. C... la somme de 3 000 euros chacun au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              -	le code de l'environnement ; <br/>
              -	le code de l'urbanisme ;<br/>
              -	le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Mireille Le Corre, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Louis Dutheillet de Lamothe, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de M. et Mme B...et à la SCP Gaschignard, avocat de la commune de Bazouges-la-Pérouse.<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que par un arrêté du 4 octobre 2012 le maire de Bazouges-la-Pérouse (Ille-et-Vilaine) a délivré à M. C... un permis de construire en vue de l'édification d'un bâtiment lui permettant de procéder à un regroupement sur le même site de ses installations d'élevage porcin ; qu'à la demande de M. et Mme B...et de l'association environnement et paysage en Haute-Bretagne, le tribunal administratif de Rennes a, par un jugement du 19 décembre 2014, annulé pour excès de pouvoir ce permis de construire ; que, par un arrêt du 5 février 2016, contre lequel M. et Mme B...se pourvoient en cassation, la cour administrative d'appel de Nantes a annulé ce jugement et rejeté leur demande ;<br/>
<br/>
              2. Considérant, en premier lieu, qu'il est constant que les constructions antérieures à la construction faisant l'objet du permis de construire litigieux étaient distinctes de celle-ci ; que leur régularité était, par suite, sans incidence sur la légalité de ce permis ; qu'il convient, pour ce motif, qui doit être substitué au motif retenu par la cour au considérant 9 de son arrêt, d'écarter le moyen soumis aux juges du fond tiré de ce que le permis de construire ne pouvait être légalement accordé que s'il permettait également la régularisation des constructions antérieures ; qu'il en résulte que les requérants ne peuvent utilement se fonder sur ce que l'arrêt attaqué serait entaché, sur ce point, d'insuffisance de motivation et de dénaturation de leurs écritures ;<br/>
<br/>
              3. Considérant, en deuxième lieu, qu'en relevant que les intéressés soutenaient que le projet litigieux comportait un merlon, en méconnaissance des dispositions de l'article NDb 1 du règlement du plan d'occupation des sols prohibant tout exhaussement au sein de la zone ND, puis en écartant ce moyen aux motifs que ce merlon avait été édifié antérieurement à la décision litigieuse et qu'à supposer son édification irrégulière, une telle circonstance était en l'espèce sans incidence sur la légalité de l'autorisation de construire litigieuse, laquelle ne prévoyait pas d'exhaussement, la cour n'a pas dénaturé leurs écritures et a suffisamment motivé son arrêt au regard de la teneur de l'argumentation qui lui était soumise ;<br/>
<br/>
              4. Considérant, en troisième lieu, qu'aux termes de l'article R. 431-16 du code de l'urbanisme, dans sa rédaction applicable au litige : " Le dossier joint à la demande de permis de construire comprend en outre, selon les cas : / a) L'étude d'impact, lorsqu'elle est prévue en application du code de l'environnement, ou la décision de l'autorité administrative de l'Etat compétente en matière d'environnement dispensant le demandeur de réaliser une étude d'impact ; (...) " ;  qu'il résulte de ces dispositions que l'obligation de joindre l'étude d'impact au dossier de demande de permis de construire ne concerne que les cas où l'étude d'impact est exigée en vertu des dispositions du code de l'environnement pour des projets soumis à autorisation en application du code de l'urbanisme ; qu'il suit de là qu'en écartant, sur ce fondement, le moyen tiré de l'illégalité externe du permis de construire litigieux tiré de l'absence d'étude d'impact, la cour n'a pas commis d'erreur de droit ; <br/>
<br/>
              5. Considérant, en quatrième lieu, qu'aux termes de l'article R. 111-15 du code de l'urbanisme, dans sa rédaction alors en vigueur : " Le permis ou la décision prise sur la déclaration préalable doit respecter les préoccupations d'environnement définies aux articles L. 110-1 et L. 110-2 du code de l'environnement. Le projet peut n'être accepté que sous réserve de l'observation de prescriptions spéciales si, par son importance, sa situation ou sa destination, il est de nature à avoir des conséquences dommageables pour l'environnement " ; qu'il résulte de ces dispositions qu'elles ne permettent pas à l'autorité administrative de refuser un permis de construire, mais seulement de l'accorder sous réserve du respect de prescriptions spéciales relevant de la police de l'urbanisme, telles que celles relatives à l'implantation ou aux caractéristiques des bâtiments et de leurs abords, si le projet de construction est de nature à avoir des conséquences dommageables pour l'environnement ; qu'à ce titre, s'il n'appartient pas à cette autorité d'assortir le permis de construire délivré pour une installation classée de prescriptions relatives à son exploitation et aux nuisances qu'elle est susceptible d'occasionner, il lui incombe, en revanche, le cas échéant, de tenir compte des prescriptions édictées au titre de la police des installations classées ou susceptibles de l'être ;<br/>
<br/>
              6. Considérant que les requérants ont soutenu devant la cour que le permis litigieux était entaché d'une erreur manifeste d'appréciation au regard des dispositions de l'article R. 111-15 du code de l'urbanisme, faute de comporter des prescriptions spéciales destinées à limiter les incidences du projet sur l'environnement ; qu'il résulte toutefois de ce qui a été dit au point précédent que, ayant relevé qu'il ressortait des pièces du dossier qu'une demande d'autorisation de regroupement d'installations d'élevage au titre de la police des installations classées pour la protection de l'environnement était en cours d'instruction devant l'autorité compétente à la date de délivrance du permis litigieux, la cour n'a entaché son arrêt d'aucune erreur de droit dans l'application des règles rappelées au point 5 en jugeant que les requérants n'étaient pas fondés à se prévaloir, pour contester la légalité de ce permis au regard des dispositions de l'article R. 111-15 du code de l'urbanisme, de la circonstance, qui concernait l'exploitation de l'installation, que l'augmentation du nombre de porcs présents sur le site génèrerait des nuisances supplémentaires, notamment en ce qui concerne le volume du lisier et la teneur en nitrates des milieux aquatiques ; <br/>
<br/>
              7. Considérant, en cinquième et dernier lieu, que pour écarter le moyen tiré d'une violation des dispositions de l'article R. 111-21 du code de l'urbanisme et de l'article NDb 11 du règlement du plan d'occupation des sols de la commune, la cour a relevé qu'il ressortait des pièces du dossier que le projet litigieux procédait à l'extension d'une installation d'élevage existante, située dans une zone du plan d'occupation des sols définie comme constituant un espace naturel agricole et dans un cadre rural constitué essentiellement de terres agricoles, que plusieurs autres exploitations agricoles étaient également situées dans cette zone et à proximité du château de La Ballue, que le dossier de demande de permis de construire n'occultait pas la présence d'un monument protégé situé dans le périmètre de co-visibilité, que M. C...avait fait édifier en 2011 un merlon afin de protéger des vues son exploitation et que l'architecte des bâtiments de France avait émis un avis favorable assorti de prescriptions destinées à atténuer la perception de l'ouvrage à partir des terrasses-jardins du château, notamment un bardage en bois de teinte sombre, lequel n'était pas entaché d'erreur manifeste d'appréciation ; que la cour s'est ainsi fondée implicitement sur ce que la commune n'avait pas fait une inexacte application de l'article NDb 11 du règlement du plan d'occupation de la commune, la mention relative à l'erreur manifeste d'appréciation ne concernant que l'avis émis par l'architecte des bâtiments de France ; que, ce faisant, la cour, qui a porté sur les pièces du dossier qui lui était soumis une appréciation souveraine exempte de dénaturation, n'a pas commis d'erreur de droit ; <br/>
<br/>
              8. Considérant qu'il résulte de tout ce qui précède que M. et Mme B...ne sont pas fondés à demander l'annulation de l'arrêt qu'ils attaquent ; <br/>
<br/>
              9. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de M. et Mme B...la somme de 3 000 euros à verser à la commune de Bazouges-la-Pérouse au titre de l'article L. 761-1 du code de justice administrative ; qu'en revanche, ces dispositions font, en tout état de cause, obstacle à ce qu'une somme soit mise, à ce titre, à la charge de la commune et de M.C..., qui ne sont pas, dans la présente instance, la partie perdante ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
	Article 1er : Le pourvoi de M. et Mme B...est rejeté.<br/>
Article 2 : M. et Mme B...verseront une somme de 3 000 euros à la commune de Bazouges-la-Pérouse au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : Les conclusions présentées par l'association Environnement et paysage en Haute-Bretagne et Ille-et-Vilaine au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
<br/>
Article 4 : La présente décision sera notifiée à M. et MmeB..., à la commune de Bazouges-la-Pérouse, à M. A...C...et à l'association Environnement et paysage en Haute-Bretagne.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">44-02-02 NATURE ET ENVIRONNEMENT. INSTALLATIONS CLASSÉES POUR LA PROTECTION DE L'ENVIRONNEMENT. RÉGIME JURIDIQUE. - RESPECT DES PRÉOCCUPATIONS D'ENVIRONNEMENT PAR L'AUTORITÉ COMPÉTENTE POUR DÉLIVRER UN PERMIS DE CONSTRUIRE (ART. R. 111-15 REPRIS À L'ART. R. 111-26 DU CODE DE L'URBANISME) - PORTÉE - POSSIBILITÉ D'ASSORTIR LE PERMIS DE PRESCRIPTIONS RELATIVES À L'EXPLOITATION OU AUX NUISANCES SUSCEPTIBLES D'ÊTRE GÉNÉRÉES PAR UNE IPCE - ABSENCE - OBLIGATION DE TENIR COMPTE DES PRESCRIPTIONS ÉDICTÉES AU TITRE DE LA POLICE DES INSTALLATIONS CLASSÉES OU SUSCEPTIBLES DE L'ÊTRE - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">68-03 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PERMIS DE CONSTRUIRE. - RESPECT DES PRÉOCCUPATIONS D'ENVIRONNEMENT (ART. R. 111-15 REPRIS À L'ART. R. 111-26 DU CODE DE L'URBANISME) - PORTÉE - MOTIF DE REFUS DU PERMIS DE CONSTRUIRE - ABSENCE - POSSIBILITÉ DE L'ACCORDER SOUS RESERVE DU RESPECT DE PRESCRIPTIONS SPÉCIALES RELEVANT DE LA POLICE DE L'URBANISME - EXISTENCE [RJ1] - POSSIBILITÉ DE L'ASSORTIR DE PRESCRIPTIONS RELATIVES À L'EXPLOITATION OU AUX NUISANCES SUSCEPTIBLES D'ÊTRE GÉNÉRÉES PAR UNE IPCE - ABSENCE - OBLIGATION DE TENIR COMPTE DES PRESCRIPTIONS ÉDICTÉES AU TITRE DE LA POLICE DES INSTALLATIONS CLASSÉES OU SUSCEPTIBLES DE L'ÊTRE - EXISTENCE.
</SCT>
<ANA ID="9A"> 44-02-02 L'article R. 111-15 du code de l'urbanisme, repris à l'article R. 111-26 du même code, ne permet pas à l'autorité administrative de refuser un permis de construire, mais seulement de l'accorder sous réserve du respect de prescriptions spéciales relevant de la police de l'urbanisme, telles que celles relatives à l'implantation ou aux caractéristiques des bâtiments et de leurs abords, si le projet de construction est de nature à avoir des conséquences dommageables pour l'environnement. A ce titre, s'il n'appartient pas à cette autorité d'assortir le permis de construire délivré pour une installation classée de prescriptions relatives à son exploitation et aux nuisances qu'elle est susceptible d'occasionner, il lui incombe, en revanche, le cas échéant, de tenir compte des prescriptions édictées au titre de la police des installations classées ou susceptibles de l'être.</ANA>
<ANA ID="9B"> 68-03 L'article R. 111-15 du code de l'urbanisme, repris à l'article R. 111-26 du même code, ne permet pas à l'autorité administrative de refuser un permis de construire, mais seulement de l'accorder sous réserve du respect de prescriptions spéciales relevant de la police de l'urbanisme, telles que celles relatives à l'implantation ou aux caractéristiques des bâtiments et de leurs abords, si le projet de construction est de nature à avoir des conséquences dommageables pour l'environnement. A ce titre, s'il n'appartient pas à cette autorité d'assortir le permis de construire délivré pour une installation classée de prescriptions relatives à son exploitation et aux nuisances qu'elle est susceptible d'occasionner, il lui incombe, en revanche, le cas échéant, de tenir compte des prescriptions édictées au titre de la police des installations classées ou susceptibles de l'être.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 7 février 2003, Société civile d'exploitation agricole Le Haras d'Achères II, n° 220215, inédite au Recueil.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
