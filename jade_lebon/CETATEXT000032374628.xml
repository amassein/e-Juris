<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032374628</ID>
<ANCIEN_ID>JG_L_2016_03_000000377945</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/37/46/CETATEXT000032374628.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème / 9ème SSR, 30/03/2016, 377945</TITRE>
<DATE_DEC>2016-03-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>377945</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème / 9ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>Mme Pauline Jolivet</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2016:377945.20160330</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...C...a demandé au tribunal administratif de Marseille, d'une part, d'annuler pour excès de pouvoir la décision du 7 juillet 2009 par laquelle le directeur du département des recherches archéologiques subaquatiques et sous-marines du ministère de la culture et de la communication a refusé d'enregistrer sa déclaration relative à la découverte de l'épave du navire " Thésée " et d'en délivrer récépissé ainsi que la décision du 30 juillet 2009 par laquelle cette même autorité a rejeté son recours gracieux contre cette première décision, d'autre part, d'enjoindre au directeur du département des recherches archéologiques subaquatiques et sous-marines de réexaminer cette déclaration. Par un jugement n° 0906281 du 15 décembre 2011, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 12MA00672 du 17 février 2014, la cour administrative d'appel de Marseille, sur appel de M.C..., a, d'une part, annulé le jugement du tribunal administratif de Marseille du 15 décembre 2011 ainsi que les décisions des 7 juillet 2009 et 30 juillet 2009 du directeur du département des recherches archéologiques subaquatiques et sous-marines et, d'autre part, enjoint à cette autorité d'enregistrer la déclaration déposée par M. C...et de lui en délivrer récépissé. <br/>
<br/>
              Par un pourvoi, enregistré le 17 avril 2014 au secrétariat du contentieux du Conseil d'Etat, la ministre de la culture et de la communication demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de M.C.soumises à l'appréciation du ministre<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code du patrimoine ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Pauline Jolivet, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de M. B...C...;<br/>
<br/>
<br/>
              Vu la note en délibéré, enregistrée le 21 mars 2016, présentée par la ministre de la culture et de la communication ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 532-3 du code du patrimoine : " Toute personne qui découvre un bien culturel maritime est tenue de le laisser en place et de ne pas y porter atteinte. / Elle doit, dans les quarante-huit heures de la découverte ou de l'arrivée au premier port, en faire la déclaration à l'autorité administrative. " ; qu'il résulte de l'article R. 232-2 de ce code que cette déclaration de découverte doit indiquer le lieu de la découverte et la nature de ce bien culturel ; qu'aux termes de l'article L. 532-7 du même code : " Nul ne peut procéder à des prospections à l'aide de matériels spécialisés permettant d'établir la localisation d'un bien culturel maritime, à des fouilles ou à des sondages sans en avoir, au préalable, obtenu l'autorisation administrative délivrée en fonction de la qualification du demandeur ainsi que de la nature et des modalités de la recherche. / Tout déplacement d'un bien ou tout prélèvement sur celui-ci est soumis, dans les mêmes conditions, à l'obtention préalable d'une autorisation administrative. " ; qu'aux termes de l'article L. 544-5 du même code : " Le fait, pour toute personne, d'enfreindre les obligations de déclaration prévues au deuxième alinéa de l'article L. 532-3 ou à l'article L. 532-4 est puni d'une amende de 3 750 euros. / Est puni de la même peine le fait, pour toute personne, d'avoir fait auprès de l'autorité publique une fausse déclaration quant au lieu et à la composition du gisement sur lequel l'objet déclaré a été découvert. " ; qu'aux termes de l'article L. 544-6 du même code : " Le fait, pour toute personne, d'avoir fait des prospections, des sondages, des prélèvements ou des fouilles sur des biens culturels maritimes ou d'avoir procédé à un déplacement de ces biens ou à un prélèvement sur ceux-ci en infraction aux dispositions du premier alinéa de l'article L. 532-3 ou des articles L. 532-7 et L. 532-8 est puni d'une amende de 7 500 euros. " ;<br/>
<br/>
              2. Considérant qu'il résulte de la combinaison de ces dispositions que l'obligation de déclaration d'un bien culturel maritime prévue à l'article L. 532-3 du code du patrimoine, et dont le défaut ou le caractère faux est sanctionné d'une peine d'amende prévue à l'article L. 544-5 de ce code, s'applique à toute découverte et en toutes circonstances ; qu'ainsi, elle s'applique même lorsque la découverte n'intervient pas dans le cadre d'une autorisation délivrée par l'autorité administrative en application de l'article L. 532-7 du même code et quand bien même cette découverte résulterait de prospections à l'aide de matériels spécialisés permettant d'établir la localisation d'un bien culturel maritime, de fouilles ou de sondages sans qu'ait été demandée ou obtenue une telle autorisation, la prospection sans l'autorisation requise étant sanctionnée par une peine d'amende en application de l'article L. 544-6 précité ; que la circonstance que la déclaration ait pour conséquence de reconnaître la qualité d'inventeur à la personne qui y procède ne dépend pas de l'appréciation de la légalité des conditions de la découverte et est sans incidence sur celle-ci ; que la reconnaissance de cette qualité d'inventeur ne confère par elle-même aucun droit à l'intéressé et notamment pas à l'allocation des récompenses légalement prévues qui demeurent... ; qu'ainsi, lorsqu'elle est saisie d'une déclaration de découverte n'intervenant pas dans le cadre d'une autorisation délivrée en application de l'article L. 532-7 du code du patrimoine, l'autorité administrative est tenue de l'enregistrer et d'en donner récépissé à son auteur, dès lors qu'elle comporte les indications mentionnées à l'article R. 532-2 du même code, sans préjudice des poursuites pénales prévues en cas de  prospection sans autorisation ; <br/>
<br/>
              3. Considérant qu'il résulte des pièces du dossier soumis aux juges du fond qu'une autorisation de prospection a été délivrée, sur le fondement de l'article L. 532-7 du code du patrimoine, à M.A..., afin de procéder, en qualité de responsable scientifique, à une opération archéologique sous-marine de prospection au large du Croisic, sur le plateau de l'Artimon ; que M. C...a rejoint l'équipe constituée à cette fin en y apportant ses compétences de plongeur et en mettant son bateau à disposition ; qu'à la suite d'un désaccord avec le reste de l'équipe sur la délimitation de la zone de recherche, M. C...a mené ses propres recherches qui l'ont conduit à localiser, hors de la zone de prospection pour laquelle l'autorisation administrative avait été délivrée et à l'aide d'un matériel de détection électronique, une épave sous-marine susceptible de constituer le navire " Thésée " ; que le directeur du département des recherches archéologiques subaquatiques et sous-marines a refusé d'enregistrer la déclaration déposée par M. C...aux motifs que la découverte était liée à la campagne de prospection menée par l'équipe dirigée par M.A..., seul titulaire d'une autorisation administrative, et qu'il avait procédé à des recherches à l'aide de matériel de détection électronique, sans y être autorisé ; <br/>
<br/>
              4. Considérant qu'en jugeant, pour annuler le jugement du tribunal administratif de Marseille ayant rejeté la demande d'annulation de la décision de refus d'enregistrement de la déclaration de M.C..., que l'obligation de déclaration prévue à l'article L. 532-3 du code du patrimoine s'appliquait à la découverte effectuée par M.C..., alors même qu'elle était intervenue en contravention avec les exigences de l'article L. 532-7 du même code et, qu'en conséquence, l'autorité administrative compétente ne pouvait ni refuser d'enregistrer la déclaration de découverte de ce bien culturel maritime ni s'opposer à la délivrance du récépissé, la cour administrative d'appel n'a pas commis d'erreur de droit ; qu'il en résulte que le ministre n'est pas fondé à demander l'annulation de l'arrêt qu'il attaque ;<br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de faire droit aux conclusions de M. C...et de mettre à la charge de l'Etat la somme de 3 000 euros sur le fondement de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er: Le pourvoi de la ministre de la culture et de la communication est rejeté. <br/>
<br/>
Article 2 : L'Etat versera une somme de 3 000 euros à M. C...au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée à la ministre de la culture et de la communication et à M. B...C.soumises à l'appréciation du ministre<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">09 ARTS ET LETTRES. - BIENS CULTURELS MARITIMES - RÉGIME DE DÉCLARATION DES DÉCOUVERTES (ART. L. 532-3 DU CODE DU PATRIMOINE) - 1) PORTÉE DE L'OBLIGATION DE DÉCLARATION - TOUTE DÉCOUVERTE, Y COMPRIS DANS LE CADRE DE PROSPECTIONS ILLÉGALES - CONSÉQUENCE - OBLIGATION D'ENREGISTRER LA DÉCOUVERTE - EXISTENCE - 2) ESPÈCE.
</SCT>
<ANA ID="9A"> 09 1) L'obligation de déclaration d'un bien culturel maritime prévue à l'article L. 532-3 du code du patrimoine, dont le défaut ou le caractère faux est sanctionné d'une peine d'amende prévue à l'article L. 544-5 de ce code, s'applique à toute découverte et en toutes circonstances. Ainsi, elle s'applique même lorsque la découverte n'intervient pas dans le cadre d'une autorisation délivrée par l'autorité administrative en application de l'article L. 532-7 du même code et quand bien même cette découverte résulterait de prospections à l'aide de matériels spécialisés permettant d'établir la localisation d'un bien culturel maritime, de fouilles ou de sondages sans qu'ait été demandée ou obtenue une telle autorisation, la prospection sans l'autorisation requise étant sanctionnée par une peine d'amende en application de l'article L. 544-6.... ,,La circonstance que la déclaration ait pour conséquence de reconnaître la qualité d'inventeur à la personne qui y procède ne dépend pas de l'appréciation de la légalité des conditions de la découverte  et est sans incidence sur celle-ci. La reconnaissance de cette qualité d'inventeur ne confère par elle-même aucun droit à l'intéressé et notamment pas à l'allocation des récompenses légalement prévues qui demeurent soumises à l'appréciation du ministre.,,,Ainsi, lorsqu'elle est saisie d'une déclaration de découverte n'intervenant pas dans le cadre d'une autorisation délivrée en application de l'article L. 532-7 du code du patrimoine, l'autorité administrative est tenue de l'enregistrer et d'en donner récépissé à son auteur, dès lors qu'elle comporte les indications mentionnées à l'article R. 532-2 du même code, sans préjudice des poursuites pénales prévues en cas de  prospection sans autorisation.... ,,2) Autorisation de prospection donnée à une personne en qualité de responsable scientifique. Membre de l'équipe ayant, à la suite d'un désaccord, mené ses propres recherches en dehors de la zone de prospection et localisé une épave sous-marine. Alors même que cette découverte était intervenue en contravention avec les exigences de l'article L. 532-7 du code du patrimoine, l'autorité administrative ne pouvait ni refuser d'enregistrer la déclaration de découverte de ce bien culturel maritime ni s'opposer à la délivrance du récépissé.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
