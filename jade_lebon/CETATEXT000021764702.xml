<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000021764702</ID>
<ANCIEN_ID>JG_L_2010_01_000000313247</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/21/76/47/CETATEXT000021764702.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème et 3ème sous-sections réunies, 27/01/2010, 313247</TITRE>
<DATE_DEC>2010-01-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>313247</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème et 3ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Vigouroux</PRESIDENT>
<AVOCATS>SCP PIWNICA, MOLINIE ; SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>M. Marc  El Nouchi</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olléon Laurent</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2010:313247.20100127</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 12 février et 9 mai 2008 au secrétariat du contentieux du Conseil d'Etat, présentés pour la COMMUNE DE MAZAYES-BASSES, représentée par son maire, domicilié en cette qualité à l'Hôtel de ville sis place de l'Hôtel de ville (63230) ; la COMMUNE DE MAZAYES-BASSES demande au Conseil d'Etat :<br/>
<br/>
               1°) d'annuler l'arrêt du 11 décembre 2007 par lequel la cour administrative d'appel de Lyon, faisant partiellement droit à la requête de M. Alain A, a, d'une part, annulé le jugement du 22 mars 2005 du tribunal administratif de Clermont-Ferrand rejetant la demande de ce dernier tendant à l'annulation des délibérations du conseil municipal de la COMMUNE DE MAYAZES-BASSES en date du 30 octobre 2003 par lesquelles ce conseil municipal a décidé du déclassement d'un bâtiment anciennement à usage d'école, puis de mairie, et sa vente pour le prix de 53 357 euros, et d'autre part, annulé ces délibérations ;<br/>
<br/>
               2°) réglant l'affaire au fond, de rejeter la demande présentée par M. A devant le tribunal administratif de Clermont-Ferrand ;<br/>
<br/>
              3°) de mettre à la charge de M. A la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des collectivités territoriales ;<br/>
<br/>
              Vu le code de l'éducation nationale ;<br/>
<br/>
              Vu la loi du 30 octobre 1886 portant sur l'organisation de l'enseignement primaire ;<br/>
<br/>
              Vu la loi n° 83-663 du 22 juillet 1983 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Marc El Nouchi, Maître des Requêtes, <br/>
<br/>
              - les observations de la SCP Piwnica, Molinié, avocat de la COMMUNE DE MAZAYES-BASSES et de la SCP Waquet, Farge, Hazan, avocat de M. A, <br/>
<br/>
              - les conclusions de M. Laurent Olléon, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Piwnica, Molinié, avocat de la COMMUNE DE MAZAYES-BASSES et à la SCP Waquet, Farge, Hazan, avocat de M. A ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par deux délibérations en date du 30 octobre 2003, le conseil municipal de la COMMUNE DE MAZAYES-BASSES a, d'une part, déclassé du domaine public communal un bâtiment sis sur son territoire et qui a abrité successivement l'école du village jusqu'en 1952, puis les services de la mairie jusqu'en 1988, avant d'être pour partie utilisé comme local associatif et pour partie loué à titre de logement, et, d'autre part, autorisé la cession de ce bâtiment pour la somme de 53 357 euros, prioritairement au locataire en place à cette date ; que s'estimant irrégulièrement évincé de cette procédure d'acquisition, M. A a déféré ces deux délibérations à la censure du tribunal administratif de Clermont-Ferrand, lequel a rejeté sa demande par un jugement du 22 mars 2005 ; que la COMMUNE DE MAZAYES-BASSES se pourvoit en cassation contre l'arrêt du 11 décembre 2007 par lequel la cour administrative d'appel de Lyon, faisant partiellement droit à la requête de M. A, a annulé ce jugement ainsi que les délibérations attaquées ;<br/>
<br/>
              Sans qu'il soit besoin d'examiner l'autre moyen du pourvoi ;<br/>
<br/>
              Considérant qu'aux termes de l'article L. 2121-30 du code général des collectivités territoriales, auquel renvoie l'article L. 212-1 du code de l'éducation, et qui est issu du I de l'article 13 de la loi du 22 juillet 1983 complétant la loi n° 83-8 du 7 janvier 1983 relative à la répartition des compétences entre les communes, les départements, les régions et l'Etat : "Le conseil municipal décide de la création et de l'implantation des écoles et classes élémentaires et maternelles d'enseignement public après avis du représentant de l'Etat dans le département" ; qu'il résulte de ces dispositions  que les communes ne peuvent prendre les décisions de désaffectation des biens affectés aux besoins du service public des écoles élémentaires et maternelles dont elles sont propriétaires sans avoir recueilli au préalable l'avis du représentant de l'Etat ; que, toutefois, avant l'entrée en vigueur de ces dispositions, était applicable l'article 13 de la loi du 30 octobre 1886 portant sur l'organisation de l'enseignement primaire qui disposait, dans des dispositions en vigueur en 1952, que : "Le conseil départemental de l'instruction publique, après avoir pris l'avis des conseils municipaux, détermine, sous réserve de l'approbation du ministre, le nombre, la nature et le siège des écoles primaires publiques de tout degré qu'il y a lieu d'établir ou de maintenir dans chaque commune ainsi que le nombre des maîtres qui y sont attachés" ; <br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis aux juges du fond, et qu'il est constant, que depuis 1952 le bâtiment en cause n'est plus affecté au service public des écoles élémentaires ; que si la désaffectation en 1952 de ce bâtiment du service public des écoles, en vue de son affectation au service public municipal, devait à cette date être soumise à la procédure prévue par l'article 13 de la loi du 30 octobre 1886 précité, ce bâtiment, que cette procédure ait ou non été alors mise en oeuvre, n'était plus, lorsque le conseil municipal a décidé, par la délibération du 30 octobre 2003, de procéder à son déclassement du domaine public municipal, affecté au service public des écoles ; que, dès lors, le conseil municipal pouvait, par cette délibération, procéder à son déclassement sans mettre préalablement en oeuvre la procédure de désaffectation du service public des écoles désormais prévue par les dispositions de l'article L. 2121-30 du code général des collectivités territoriales précité ; que, dès lors, en jugeant que cette délibération était illégale au motif que le déclassement était intervenu sans la consultation préalable du représentant de l'Etat prévue par ces dernières dispositions, la cour administrative d'appel de Lyon a commis une erreur de droit ; que l'arrêt attaqué doit, dès lors, être annulé ;   <br/>
<br/>
              Considérant que dans les circonstances de l'espèce, il y a lieu, par application des dispositions de l'article L. 821-2 du code de justice administrative, de régler l'affaire au fond ;<br/>
<br/>
              Considérant, en premier lieu, qu'ainsi qu'il a été dit, le conseil municipal a pu légalement procéder, par la délibération attaquée, au déclassement du bâtiment litigieux en vue de son aliénation, sans recueillir l'avis du représentant de l'Etat, dès lors que ce bâtiment n'était plus affecté au service public des écoles ; que, par suite, les moyens tirés de ce que cette décision de déclassement, serait, pour ce motif, entachée d'une erreur de droit ou d'une erreur d'appréciation, doivent être écartés ;   <br/>
<br/>
              Considérant, en deuxième lieu, qu'aucune disposition législative ou réglementaire ne faisait obstacle à ce que la commune, qui a manifesté son souhait de favoriser l'habitat permanent sur son territoire, privilégie la recherche d'une transaction de gré à gré avec le locataire en place, en fixant de surcroît un prix de vente supérieur à la fourchette d'appréciation de la valeur de l'immeuble établie par le service des domaines en juin 2003 ; que si M. A fait valoir qu'il aurait été susceptible de présenter une offre d'un montant supérieur, cette circonstance ne suffit pas à entacher d'irrégularité la délibération fixant les modalités de vente, la commune n'étant, en tout état de cause, pas tenue de réaliser la vente au profit du mieux offrant ; que, par suite, le moyen tiré par M. A de l'irrégularité de la délibération du 30 octobre 2003, en tant que celle-ci a invité le maire à proposer au locataire d'acquérir l'immeuble au prix de 53 357 euros, ne peut qu'être écarté ; que le détournement de pouvoir allégué n'est pas établi ;<br/>
<br/>
              Considérant, en troisième lieu, qu'il résulte de l'instruction que la commune requérante n'a pas donné suite à la procédure de vente qu'elle a engagée, dans l'attente de la conclusion du présent litige ; que, par suite, les conclusions de M. A tendant à ce qu'il soit ordonné à la commune d'engager une procédure de résolution de la vente ne peuvent, en tout état de cause, qu'être rejetées ; <br/>
<br/>
              Considérant qu'il résulte de ce qui précède que M. A n'est pas fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Clermont-Ferrand a rejeté sa demande ;<br/>
<br/>
              Sur les conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de la COMMUNE DE MAZAYES-BASSES qui n'est pas, dans la présente instance, la partie perdante, une somme au titre des frais exposés par M. A et non compris dans les dépens ; qu'il y a lieu, dans les circonstances de l'espèce, de faire application des mêmes dispositions et de mettre à la charge de ce dernier le versement à la COMMUNE DE MAZAYES-BASSES de la somme de 3 000 euros ;<br/>
<br/>
<br/>
<br/>
<br/>
              			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : L'arrêt en date du 11 décembre 2007 de la cour administrative d'appel de Lyon est annulé.<br/>
<br/>
Article 2 : La requête présentée par M. A devant la cour administrative d'appel de Lyon et le surplus de ses conclusions devant le Conseil d'Etat sont rejetés.<br/>
<br/>
Article 3 : M. A versera à la COMMUNE DE MAZAYES-BASSES la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la COMMUNE DE MAZAYES-BASSES et à M. Alain A.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">24-01-02-02 DOMAINE. DOMAINE PUBLIC. RÉGIME. CHANGEMENT D'AFFECTATION. - SERVICE PUBLIC SCOLAIRE - PROCÉDURE (ART. L. 2121-30 DU CGCT) - MÉCONNAISSANCE - CONSÉQUENCE SUR LA LÉGALITÉ DE LA DÉCISION DE DÉCLASSEMENT - ABSENCE DÈS LORS QU'À LA DATE DE CETTE DERNIÈRE, LE BIEN N'ÉTAIT PLUS AFFECTÉ À CE SERVICE PUBLIC.
</SCT>
<ANA ID="9A"> 24-01-02-02 Bâtiment du domaine public communal ayant fait l'objet en 1952 d'une désaffectation du service public des écoles. Alors même qu'à cette époque une telle désaffectation devait être soumise à la procédure prévue par l'article 13 de la loi du 30 octobre 1886, le bâtiment n'était plus, lors de son déclassement en 2003, affecté à ce service public. Le déclassement en cause n'était donc pas subordonné au respect de la procédure de désaffectation du service public scolaire désormais prévue par les dispositions de l'article L. 2121-30 du code général des collectivités territoriales (CGCT), issues de la loi n° 83-663 du 22 juillet 1983.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
