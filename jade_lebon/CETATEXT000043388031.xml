<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043388031</ID>
<ANCIEN_ID>JG_L_2021_04_000000434611</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/38/80/CETATEXT000043388031.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 1ère chambres réunies, 16/04/2021, 434611</TITRE>
<DATE_DEC>2021-04-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>434611</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>Mme Yaël Treille</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:434611.20210416</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Le Syndicat national des techniciens et travailleurs de la production cinématographique et de télévision (SNTPCT) a demandé à la cour administrative d'appel de Paris, d'une part, d'enjoindre au ministre du travail de lui communiquer les résultats du scrutin des élections des délégués du personnel qui ont eu lieu entre le 1er janvier 2013 et le 31 décembre 2016, tels qu'ils ont été enregistrés auprès du Haut Conseil au dialogue social (HCDS) en indiquant le code NAF de ces différentes entreprises, ainsi que la liste des entreprises de moins de onze salariés qui y ont participé et les résultats de la consultation dans chacune d'entre elles, d'autre part, d'annuler pour excès de pouvoir l'arrêté du 21 juillet 2017 de la ministre du travail fixant la liste des organisations syndicales reconnues représentatives dans la convention collective nationale de la production cinématographique. Par un arrêt n° 17PA03162 du 12 juillet 2019, la cour administrative d'appel a annulé l'article 2 de l'arrêté du 21 juillet 2017 et rejeté le surplus des conclusions de la requête. <br/>
<br/>
              Par un pourvoi, enregistré le 16 septembre 2019 au secrétariat du contentieux du Conseil d'Etat, la ministre du travail demande au Conseil d'Etat d'annuler cet arrêt. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code du travail ; <br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme A... B..., auditrice,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat du Syndicat national des techniciens et travailleurs de la production cinématographique et de télévision ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que par un arrêté du 21 juillet 2017, pris en application de l'article L. 2122-11 du code du travail, la ministre du travail a fixé la liste des organisations syndicales reconnues représentatives dans le champ de la convention collective nationale de la production cinématographique (n° IDCC [identifiant de convention collective] 3097). Cet arrêté reconnaît, en son article 1er, le Syndicat national des techniciens et travailleurs de la production cinématographique et de télévision (SNTPCT), la Confédération générale du travail (CGT) et la Confédération française démocratique du travail (CFDT) représentatives dans le champ de cette convention collective. Il fixe, en son article 2, pour la négociation des accords collectifs prévue à l'article L. 2232-6 du code du travail, le poids respectif de ces trois organisations syndicales, soit 44,77 % (SNTPCT), 30,91 % (CGT) et 24,32 % (CFDT). Le SNTPCT a demandé à la cour administrative d'appel de Paris l'annulation pour excès de pouvoir de cet arrêté en faisant valoir qu'il résulte de la prise en compte de suffrages électoraux qui n'auraient pas dû être pris en compte, notamment parce qu'ils émanent d'entreprises ne relevant pas de cette branche professionnelle. Par un arrêt du 12 juillet 2019, contre lequel la ministre du travail se pourvoit en cassation, la cour administrative d'appel a annulé l'article 2 de cet arrêté. <br/>
<br/>
              2. Aux termes de l'article L. 2122-5 du code du travail dans sa rédaction alors applicable : " Dans les branches professionnelles, sont représentatives les organisations syndicales qui : / 1° Satisfont aux critères de l'article L. 2121-1 ; / 2° Disposent d'une implantation territoriale équilibrée au sein de la branche ; / 3° Ont recueilli au moins 8 % des suffrages exprimés résultant de l'addition au niveau de la branche, d'une part, des suffrages exprimés au premier tour des dernières élections des titulaires aux comités d'entreprise ou de la délégation unique du personnel ou, à défaut, des délégués du personnel, quel que soit le nombre de votants, et, d'autre part, des suffrages exprimés au scrutin concernant les entreprises de moins de onze salariés dans les conditions prévues aux articles L. 2122-10-1 et suivants. (...) ". Aux termes de l'article L. 2122-11 du même code : " Après avis du Haut Conseil du dialogue social, le ministre chargé du travail arrête la liste des organisations syndicales reconnues représentatives par branche professionnelle et des organisations syndicales reconnues représentatives au niveau national et interprofessionnel en application des articles L. 2122-5 à L. 2122-10.  (...) ". Aux termes de l'article L. 2122-12 du même code : " Un décret détermine les modalités de recueil et de consolidation des résultats aux élections professionnelles pour l'application du présent chapitre. ". Aux termes de l'article D. 2122-6 du même code : " Le système de centralisation des résultats des élections professionnelles mentionnées aux articles L. 2122-5 à L. 2122-10 afin de mesurer l'audience des organisations syndicales doit : / a) Garantir la confidentialité et l'intégrité des données recueillies et traitées ; / b) Permettre de s'assurer, par des contrôles réguliers, de la fiabilité et de l'exhaustivité des données recueillies et consolidées (...) ". <br/>
<br/>
              3. Pour l'application des dispositions rappelées au point 2, l'audience des organisations syndicales par branche professionnelle est mesurée en se fondant sur les suffrages exprimés à l'occasion des élections professionnelles grâce à un système de centralisation des résultats dont les caractéristiques sont fixées par l'article D. 2122-6 du code du travail. A cette fin, l'article D. 2122-7 du même code prévoit que les procès-verbaux de ces élections sont transmis par les employeurs ou leurs représentants au prestataire agissant pour le compte du ministre chargé du travail. Si le ministre chargé du travail, à qui il incombe ainsi d'assurer cette centralisation, est fondé, pour assurer la fiabilité des données requise, pour l'établissement des mesures d'audience, à écarter les procès-verbaux dont les données ne sont pas exploitables en raison des anomalies qu'ils comportent, il lui appartient de veiller, sous le contrôle du juge de l'excès de pouvoir, à ce que les traitements opérés à ce titre ne remettent pas en cause, eu égard notamment au nombre des procès-verbaux concernés, l'exhaustivité nécessaire à l'établissement de ces mêmes mesures d'audience. <br/>
<br/>
              4. Dans le cadre de ces traitements, le ministre chargé du travail ne saurait se substituer à l'entreprise et corriger, sans son accord exprès, le numéro d'identifiant de convention collective mentionné dans le procès-verbal des opérations électorales lorsque celui-ci ne correspond à aucun numéro existant. <br/>
<br/>
              5. Il résulte de ce qui a été dit ci-dessus qu'en jugeant qu'en présence de procès-verbaux d'opérations électorales mentionnant des identifiants de convention collective qui n'existent pas, la ministre du travail ne pouvait légalement y substituer un autre numéro d'identifiant de convention collective sans recueillir au préalable l'accord exprès de l'entreprise concernée, la cour n'a pas commis d'erreur de droit. <br/>
<br/>
              6. Il résulte de tout ce qui précède que le pourvoi de la ministre du travail doit être rejeté. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser au Syndicat national des techniciens et travailleurs de la production cinématographique au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le pourvoi de la ministre du travail est rejeté.  <br/>
Article 2 : L'Etat versera au Syndicat national des techniciens et travailleurs de la production cinématographique la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée au Syndicat national des techniciens et travailleurs de la production cinématographique et de télévision (SNTPCT) et à la ministre du travail, de l'emploi et de l'insertion. <br/>
Copie en sera adressée à la Confédération générale du travail (CGT) et à la Confédération française démocratique du travail (CFDT).<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">66-05-01 TRAVAIL ET EMPLOI. SYNDICATS. REPRÉSENTATIVITÉ. - FIXATION DE LA LISTE DES ORGANISATIONS SYNDICALES RECONNUES REPRÉSENTATIVES ET DE LEURS AUDIENCES RESPECTIVES (ART. L. 2122-5 ET L. 2122-11 DU CODE DU TRAVAIL) - OFFICE DU MINISTRE CHARGÉ DU TRAVAIL - INCLUSION - TRAITEMENT DES PROCÈS-VERBAUX NON EXPLOITABLES EN RAISON DE LEURS ANOMALIES [RJ1] - PORTÉE - FACULTÉ DE CORRIGER D'OFFICE UN NUMÉRO D'IDENTIFIANT DE CONVENTION COLLECTIVE LORSQUE CELUI-CI NE CORRESPOND À AUCUN NUMÉRO EXISTANT - ABSENCE.
</SCT>
<ANA ID="9A"> 66-05-01 Pour l'application des articles L. 2122-5, L. 2122-11 et L. 2122-12 du code du travail, l'audience des organisations syndicales par branche professionnelle est mesurée en se fondant sur les suffrages exprimés à l'occasion des élections professionnelles grâce à un système de centralisation des résultats dont les caractéristiques sont fixées par l'article D. 2122-6 de ce code. A cette fin, l'article D. 2122-7 du même code prévoit que les procès-verbaux de ces élections sont transmis par les employeurs ou leurs représentants au prestataire agissant pour le compte du ministre chargé du travail. Si le ministre chargé du travail, à qui il incombe ainsi d'assurer cette centralisation, est fondé, pour assurer la fiabilité des données requise pour l'établissement des mesures d'audience, à écarter les procès-verbaux dont les données ne sont pas exploitables en raison des anomalies qu'ils comportent, il lui appartient de veiller, sous le contrôle du juge de l'excès de pouvoir, à ce que les traitements opérés à ce titre ne remettent pas en cause, eu égard notamment au nombre des procès-verbaux concernés, l'exhaustivité nécessaire à l'établissement de ces mêmes mesures d'audience.,,,Dans le cadre de ces traitements, le ministre chargé du travail ne saurait se substituer à l'entreprise et corriger, sans son accord exprès, le numéro d'identifiant de convention collective mentionné dans le procès-verbal des opérations électorales lorsque celui-ci ne correspond à aucun numéro existant.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., s'agissant de l'office du ministre du travail, CE, 30 décembre 2015, Confédération générale du travail - Force ouvrière (CGT- FO), n° 387420, p. 489.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
