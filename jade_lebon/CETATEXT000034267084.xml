<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034267084</ID>
<ANCIEN_ID>JG_L_2017_03_000000396630</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/26/70/CETATEXT000034267084.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 20/03/2017, 396630</TITRE>
<DATE_DEC>2017-03-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>396630</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Stéphane Hoynck</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:396630.20170320</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Par une requête, enregistrée le 1er février 2016 au secrétariat du contentieux du Conseil d'Etat, la société nouvelle du nouvel économiste demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la circulaire de la ministre de la culture et de la communication du 3 décembre 2015 relative aux modalités d'inscription des journaux autorisés à publier des annonces judiciaires et légales dans tous les départements et dans les collectivités d'outre-mer ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la directive 2006/123/CE du Parlement européen et du Conseil du 12 décembre 2006 ;<br/>
              - la loi n°55-4 du 4 janvier 1955 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Stéphane Hoynck, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Aux termes de l'article 1er de la loi du 4 janvier 1955 concernant les annonces judiciaires et légales : " Dans chaque département, et sauf pour les annonces devant paraître au Journal officiel de la République française ou à ses annexes, les annonces exigées par les lois et décrets seront insérées, à peine de nullité de l'insertion, nonobstant les dispositions contraires de ces lois et décrets, dans l'un des journaux, au choix des parties, remplissant les conditions prévues à l'article 2. / A compter du 1er janvier 2013, l'impression des annonces relatives aux sociétés et fonds de commerce publiées dans les journaux remplissant les conditions prévues au même article 2 est complétée par une insertion dans une base de données numérique centrale, dans des conditions définies par décret en Conseil d'Etat ". Aux termes de l'article 2 de cette loi : " Tous les journaux d'information générale, judiciaire ou technique, inscrits à la commission paritaire des publications et agences de presse, et ne consacrant pas en conséquence à la publicité plus des deux tiers de leur surface et justifiant une vente effective par abonnements, dépositaires ou vendeurs, sont inscrits de droit sur la liste prévue ci-dessous sous les conditions suivantes :/ 1° Paraître depuis plus de six mois au moins une fois par semaine ; / 2° Etre publiés dans le département ou comporter pour le département une édition au moins hebdomadaire ; / 3° Justifier d'une diffusion atteignant le minimum fixé par décret, en fonction de l'importance de la population du département ou de ses arrondissements. / La liste des journaux susceptibles de recevoir les annonces légales soit dans tout le département, soit dans un ou plusieurs de ses arrondissements est fixée chaque année au mois de décembre pour l'année suivante, par arrêté du préfet. (...) ". <br/>
<br/>
              2. Il ressort de ces dispositions, éclairées par leurs travaux préparatoires, que le législateur a entendu assurer que les annonces judiciaires et légales fassent l'objet, dans chaque département, d'une mesure de publicité sur un support de presse local ou comportant une édition locale en vue d'assurer une information appropriée de ces annonces, et en tenant compte de la diversité de l'offre de presse selon les départements. Pour ce faire, il a fixé au 2° de son article 2 des critères tenant au contenu rédactionnel de la publication, qui peut soit être principalement consacré aux informations d'intérêt local dans le département pour chacun de ses numéros, soit proposer un contenu rédactionnel au moins hebdomadaire relatif à des informations présentant un même intérêt. Contrairement à ce que soutient la société requérante, aucun de ces deux critères ne peut être interprété comme tendant simplement à s'assurer que les journaux habilités sont diffusés dans le département, la vérification d'une diffusion suffisante étant au demeurant assurée par la condition posée au 3° du même article.  <br/>
<br/>
              3. Si la société requérante fait reproche à la circulaire litigieuse d'avoir interprété la condition posée au 2°, tenant à ce qu'un tel journal comporte pour le département une édition au moins hebdomadaire, comme signifiant que le titre présente " quelle que soit la forme éditoriale retenue, un volume suffisant d'informations régulièrement dédiées à ce département ", il résulte de ce qui a été dit au point 2 que ce moyen ne peut qu'être écarté, la circulaire n'ayant ni donné une interprétation erronée de cette disposition législative qu'elle commente, ni ajouté de façon incompétente des critères nouveaux. La circonstance invoquée par la société requérante que la circulaire ne préciserait pas ce que serait " un volume suffisant d'informations " est sans incidence sur sa légalité. <br/>
<br/>
              4. Aux termes de l'article 9 de la directive 2006/123/CE du Parlement européen et du Conseil du 12 décembre 2006 relative aux services dans le marché intérieur : " 1. Les Etats membres ne peuvent subordonner l'accès à une activité de services et son exercice à un régime d'autorisation que si les conditions suivantes sont réunies:/ a) le régime d'autorisation n'est pas discriminatoire à l'égard du prestataire visé ; / b) la nécessité d'un régime d'autorisation est justifiée par une raison impérieuse d'intérêt général ; / c) l'objectif poursuivi ne peut pas être réalisé par une mesure moins contraignante, notamment parce qu'un contrôle a posteriori interviendrait trop tardivement pour avoir une efficacité réelle (...) ". L'article 10 de la même directive dispose que : " 1. Les régimes d'autorisation doivent reposer sur des critères qui encadrent l'exercice du pouvoir d'appréciation des autorités compétentes afin que celui-ci ne soit pas utilisé de manière arbitraire. / 2 Les critères visés au paragraphe 1 sont : (...) / c) proportionnels à cet objectif d'intérêt général / d) clairs et non ambigus;/ e) objectifs ; (...) g) transparents et accessibles (...)".<br/>
<br/>
              5. L'obligation qui figure au 2° de l'article 2 de la loi du 4 janvier 1955, que réitère la circulaire litigieuse, répond à l'objectif rappelé au point 2 de la présente décision de s'assurer que les annonces judiciaires et légales fassent l'objet d'une information appropriée par des médias localement identifiés. Contrairement à ce que soutient la société requérante, les critères fixés à cette fin, qui sont proportionnés à l'objectif poursuivi, clairs et transparents, ne sauraient être regardées comme incompatibles avec les articles 9 et 10 de la directive.<br/>
<br/>
              6. Il résulte de tout ce qui précède que la société nouvelle du nouvel économiste n'est pas fondée à demander l'annulation de la circulaire attaquée. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la société nouvelle du nouvel économiste est rejetée.<br/>
Article 2 : La présente décision sera notifiée à la société nouvelle du nouvel économiste et à la ministre de la culture et de la communication.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">53 PRESSE. - ANNONCES JUDICIAIRES ET LÉGALES - JOURNAUX POUVANT PUBLIER DE TELLES ANNONCES - PUBLICATION DÉPARTEMENTALE OU COMPORTANT DANS LE DÉPARTEMENT AU MOINS UNE ÉDITION HEBDOMADAIRE (ART. 2 DE LA LOI DU 4 JANVIER 1955) - NOTION - PUBLICATION PRINCIPALEMENT CONSACRÉE AUX INFORMATIONS D'INTÉRÊT LOCAL DANS LE DÉPARTEMENT OU PROPOSANT UN CONTENU RÉDACTIONNEL AU MOINS HEBDOMADAIRE RELATIF À DES INFORMATIONS PRÉSENTANT UN MÊME INTÉRÊT.
</SCT>
<ANA ID="9A"> 53 Il ressort des articles 1er et 2 de la loi n°55-4 du 4 janvier 1955 concernant les annonces judiciaires et légales, éclairés par les travaux parlementaires, que le législateur a entendu assurer que les annonces judiciaires et légales fassent l'objet dans chaque département d'une mesure de publicité dans un support de presse local ou comportant une édition locale en vue d'assurer une information appropriée de ces annonces, et en tenant compte de la diversité de l'offre de presse selon les départements. Pour ce faire, il a fixé au 2° de son article 2 des critères tenant au contenu rédactionnel de la publication, qui peut soit être principalement consacré aux informations d'intérêt local dans le département pour chacun de ses numéros, soit proposer un contenu rédactionnel au moins hebdomadaire relatif à des informations présentant un même intérêt.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
