<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027111134</ID>
<ANCIEN_ID>JG_L_2013_02_000000363581</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/11/11/CETATEXT000027111134.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème et 7ème sous-sections réunies, 01/02/2013, 363581</TITRE>
<DATE_DEC>2013-02-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>363581</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème et 7ème sous-sections réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Sophie-Caroline de Margerie</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CESSR:2013:363581.20130201</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le jugement n° 1204184 du 25 octobre 2012, enregistré le 29 octobre 2012 au secrétariat du contentieux du Conseil d'Etat, par lequel le tribunal administratif de Marseille, avant de statuer sur la demande de M. A...C...tendant, d'une part, à l'annulation de l'arrêté du 6 avril 2012 par lequel le préfet des Bouches-du-Rhône a rejeté sa demande d'admission au séjour, lui a fait obligation de quitter le territoire français et a fixé le pays de destination, d'autre part, à ce qu'il soit enjoint au préfet de lui délivrer un certificat de résidence ou, à défaut, de réexaminer sa situation et de lui délivrer une autorisation provisoire de séjour, sous astreinte de 150 euros par jour de retard à compter d'un délai de quinze jours suivant la notification du jugement, a décidé, par application des dispositions de l'article L. 113-1 du code de justice administrative, de transmettre le dossier de cette demande au Conseil d'Etat, en soumettant à son examen les questions suivantes :<br/>
<br/>
              1°) Un étranger, qui a sollicité son admission au séjour au titre de l'article L. 741-1 du code de l'entrée et du séjour des étrangers en France, peut-il utilement invoquer, à l'appui de conclusions aux fins d'annulation dirigées contre la décision de refus de titre que lui a opposée le préfet après intervention de la décision de l'Office français de protection des réfugiés et apatrides et, le cas échéant, de celle de la Cour nationale du droit d'asile rejetant sa demande d'asile, la méconnaissance des dispositions du dernier alinéa de l'article R. 741-2 du code précité qui sont relatives aux garanties accordées aux demandeurs d'asile pour l'instruction de leur demande ' <br/>
<br/>
              2°) En cas de réponse positive à la précédente question, le vice de procédure tenant à l'insuffisance de l'information délivrée au regard des dispositions précitées de l'article R. 741-2 du code de l'entrée et du séjour des étrangers en France revêt-il un caractère substantiel, compte tenu notamment des garanties offertes lors de la procédure d'instruction de sa demande d'asile devant les instances compétentes en la matière ou, éventuellement, des griefs précis articulés par le requérant tendant à démontrer que la privation d'une ou des informations prévues à l'article R. 741-2 précité l'aurait pénalisé au cours de cette procédure ' Dans l'affirmative, quelles sont celles de ces informations dont l'omission doit être regardée comme privant l'intéressé d'une garantie dont l'absence emporte nécessairement l'illégalité de la décision de refus de séjour attaquée '<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la directive 2005/85/CE du Conseil du 1er décembre 2005 ;<br/>
<br/>
              Vu le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
<br/>
              Vu le code de justice administrative, notamment son article L. 113-1 ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie-Caroline de Margerie, Conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Damien Botteghi, Rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              REND L'AVIS SUIVANT <br/>
<br/>
<br/>
              1. Le code de l'entrée et du séjour des étrangers et du droit d'asile organise la procédure d'examen des demandes d'asile susceptibles d'être présentées par des étrangers, présents sur le territoire français sans être déjà admis à y séjourner, en distinguant les étapes suivantes.<br/>
<br/>
              Sauf à ce que l'admission au séjour soit refusée pour l'un des motifs énumérés par l'article L. 741-4 du code, l'étranger qui demande l'asile est, en vertu de l'article R. 742-1, mis en possession d'une autorisation provisoire de séjour d'une validité d'un mois dans un délai de quinze jours après avoir fourni les pièces exigées par l'article R. 741-2. Il lui appartient alors de formuler sa demande d'asile, en saisissant l'Office français de protection des réfugiés et apatrides d'un dossier complet dans le délai de vingt et un jours prévu par l'article R. 723-1 à compter de la délivrance de l'autorisation provisoire de séjour, délai à l'expiration duquel sa demande peut être rejetée par l'office comme irrecevable. Conformément à l'article R. 742-2, sur présentation de la lettre de l'Office français de protection des réfugiés et apatrides attestant de l'enregistrement de la demande d'asile, l'étranger est mis en possession d'un récépissé valant autorisation provisoire de séjour d'une durée de validité de trois mois renouvelable jusqu'à la notification de la décision de l'Office français de protection des réfugiés et apatrides. <br/>
<br/>
              Dans le cas où la décision prise par l'Office français de protection des réfugiés et apatrides sur la demande d'asile est négative, l'étranger dispose d'un recours devant la Cour nationale du droit d'asile, qui doit être exercé dans un délai d'un mois. Conformément aux dispositions de l'article R. 742-3, sur présentation de l'accusé de réception d'un recours devant la Cour nationale du droit d'asile, le demandeur d'asile obtient le renouvellement du récépissé de la demande d'asile visé à l'article R. 742-2 d'une durée de validité de trois mois renouvelable jusqu'à la notification de la décision de la cour.<br/>
<br/>
              Selon l'article L. 742-7, l'étranger auquel la reconnaissance de la qualité de réfugié a été définitivement refusée et qui n'est pas autorisé à demeurer sur le territoire à un autre titre, doit quitter le territoire français sous peine de faire l'objet d'une mesure d'éloignement.<br/>
<br/>
              Ainsi, l'examen d'une demande de séjour au titre de l'asile peut conduire successivement à l'intervention d'une décision du préfet sur l'admission provisoire au séjour en France pour permettre l'examen de la demande d'asile, puis d'une décision de l'Office français de protection des réfugiés et apatrides, puis, le cas échéant, d'une décision de la Cour nationale du droit d'asile et, enfin, d'une décision du préfet statuant sur le séjour en France, le cas échéant à un autre titre que l'asile. <br/>
<br/>
              2. En vertu de l'article R. 741-2 du code de l'entrée et du séjour des étrangers en France, l'étranger présent sur le territoire français qui, n'étant pas déjà admis à séjourner en France, sollicite son admission au séjour au titre de l'asile, est informé par les services de la préfecture des pièces à fournir en vue de cette admission et doit se voir remettre un document d'information sur ses droits et sur les obligations qu'il doit respecter, ainsi que sur les organisations susceptibles de lui procurer une assistance juridique, de l'aider ou de l'informer sur les conditions d'accueil offertes aux demandeurs d'asile. Cette information doit être faite dans une langue dont il est raisonnable de penser que l'intéressé la comprend. <br/>
<br/>
              Ces dispositions ont été adoptées pour assurer la transposition en droit français des objectifs fixés par l'article 10 de la directive 2005/85/CE du Conseil du 1er décembre 2005 relative à des normes minimales concernant la procédure d'octroi et de retrait du statut de réfugié dans les Etats membres, lesquels précisent que les informations en cause sont communiquées aux demandeurs d'asile " à temps " pour leur permettre d'exercer leurs droits et de se conformer aux obligations qui leur sont imposées par les autorités en vue du traitement de leur demande. <br/>
<br/>
              3. Eu égard à l'objet de ce document d'information sur les droits et obligations des demandeurs d'asile, sur les organisations susceptibles de leur procurer une assistance juridique, de les aider ou de les informer sur les conditions d'accueil qui peuvent leur être proposées, la remise de ce document doit intervenir au début de la procédure d'examen des demandes d'asile, ainsi que le prévoit l'article R. 741-2 du code de l'entrée et du séjour des étrangers et du droit d'asile, pour permettre aux intéressés de présenter utilement leur demande aux autorités compétentes, dans le respect notamment des délais prévus.<br/>
<br/>
              Le défaut de remise de ce document à ce stade est ainsi de nature à faire obstacle au déclenchement du délai de vingt et un jours prévu par l'article R. 723-1 pour saisir l'Office français de protection des réfugiés et apatrides.<br/>
<br/>
              En revanche, il ne peut être utilement invoqué à l'appui d'un recours mettant en cause la légalité de la décision par laquelle le préfet statue, en fin de procédure, après intervention de l'Office français de protection des réfugiés et apatrides et, le cas échéant, après celle de la Cour nationale du droit d'asile, sur le séjour en France au titre de l'asile ou à un autre titre. <br/>
<br/>
              Compte tenu de cette réponse, il n'y a pas lieu de répondre aux autres questions posées par le tribunal administratif de Marseille.<br/>
<br/>
<br/>
<br/>
<br/>
              Le présent avis sera notifié au tribunal administratif de Marseille, à M. A...C...et au ministre de l'intérieur.<br/>
<br/>
              Il sera publié au Journal officiel de la République française.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">095-02-02 - DÉFAUT DE REMISE À L'ÉTRANGER DU DOCUMENT D'INFORMATION PRÉVU À L'ARTICLE R. 741-2 DU CESEDA - 1) CONSÉQUENCE - DÉCLENCHEMENT DU DÉLAI DE 21 JOURS PRÉVU PAR L'ARTICLE R. 723-1 POUR SAISIR L'OFPRA - ABSENCE - 2) INVOCABILITÉ À L'ENCONTRE DE LA DÉCISION DE REFUS DE SÉJOUR PRISE PAR LE PRÉFET APRÈS REJET DE LA DEMANDE D'ASILE PAR L'OFPRA, CONFIRMÉ LE CAS ÉCHÉANT PAR LA CNDA - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">095-02-05-01 - DÉLAI DE 21 JOURS POSÉ PAR L'ARTICLE R. 723-1 DU CESEDA - DÉFAUT DE REMISE AU DEMANDEUR DU DOCUMENT D'INFORMATION PRÉVU À L'ARTICLE R. 741-2 DU CESEDA PAR LES SERVICES DE LA PRÉFECTURE - CONSÉQUENCE - DÉCLENCHEMENT DU DÉLAI - ABSENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">335-01-03 ÉTRANGERS. SÉJOUR DES ÉTRANGERS. REFUS DE SÉJOUR. - REFUS OPPOSÉ À LA SUITE DU REJET D'UNE DEMANDE D'ASILE PAR L'OFPRA, CONFIRMÉ LE CAS ÉCHÉANT PAR LA CNDA - RECOURS - MOYEN TIRÉ DE DU DÉFAUT DE REMISE À L'INTÉRESSÉ DU DOCUMENT D'INFORMATION PRÉVU À L'ARTICLE R. 741-2 DU CESEDA AU DÉBUT DE LA PROCÉDURE D'EXAMEN DE SA DEMANDE D'ASILE - OPÉRANCE - ABSENCE.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">54-07-01-04-03 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. QUESTIONS GÉNÉRALES. MOYENS. MOYENS INOPÉRANTS. - CONTESTATION DU REFUS DE SÉJOUR OPPOSÉ PAR LE PRÉFET À UN DEMANDEUR D'ASILE APRÈS REJET DE SA DEMANDE PAR L'OFPRA, CONFIRMÉ LE CAS ÉCHÉANT PAR LA CNDA - MOYEN TIRÉ DU DÉFAUT DE REMISE À L'INTÉRESSÉ DU DOCUMENT D'INFORMATION PRÉVU À L'ARTICLE R. 741-2 DU CESEDA AU DÉBUT DE LA PROCÉDURE D'EXAMEN DE SA DEMANDE D'ASILE.
</SCT>
<ANA ID="9A"> 095-02-02 L'article R. 741-2 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA) prévoit qu'un document d'information est remis l'étranger qui sollicite son admission au séjour au titre de l'asile au début de la procédure d'examen de sa demande d'asile. 1) Le défaut de remise de ce document à ce stade est de nature à faire obstacle au déclenchement du délai de vingt et un jours à compter de la remise de l'autorisation provisoire de séjour, prévu par l'article R. 723-1 du CESEDA pour saisir l'Office français de protection des réfugiés et apatrides (OFPRA). 2) Le défaut de remise de ce document ne peut être utilement invoqué à l'appui d'un recours mettant en cause la légalité de la décision par laquelle le préfet statue, en fin de procédure, après intervention de l'OFPRA et, le cas échéant, après celle de la Cour nationale du droit d'asile (CNDA), sur le séjour en France au titre de l'asile ou à un autre titre.</ANA>
<ANA ID="9B"> 095-02-05-01 L'article R. 741-2 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA) prévoit qu'un document d'information est remis l'étranger qui sollicite son admission au séjour au titre de l'asile au début de la procédure d'examen de sa demande d'asile. Le défaut de remise de ce document à ce stade est de nature à faire obstacle au déclenchement du délai de vingt et un jours à compter de la remise de l'autorisation provisoire de séjour, prévu par l'article R. 723-1 du CESEDA pour saisir l'Office français de protection des réfugiés et apatrides (OFPRA).</ANA>
<ANA ID="9C"> 335-01-03 Un étranger qui a sollicité son admission au séjour au titre de l'asile ne peut pas utilement invoquer, à l'appui d'un recours mettant en cause la légalité de la décision par laquelle le préfet statue, après intervention de l'Office français de protection des réfugiés et apatrides (OFPRA) et, le cas échéant, après celle de la Cour nationale du droit d'asile (CNDA), sur le séjour en France au titre de l'asile ou à un autre titre, l'irrégularité tenant au défaut de remise, au début de la procédure d'examen de sa demande d'asile, du document d'information prévu à l'article R. 741-2 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA).</ANA>
<ANA ID="9D"> 54-07-01-04-03 Un étranger qui a sollicité son admission au séjour au titre de l'asile ne peut pas utilement invoquer, à l'appui d'un recours mettant en cause la légalité de la décision par laquelle le préfet statue, après intervention de l'Office français de protection des réfugiés et apatrides (OFPRA) et, le cas échéant, après celle de la Cour nationale du droit d'asile (CNDA), sur le séjour en France au titre de l'asile ou à un autre titre, l'irrégularité tenant au défaut de remise, au début de la procédure d'examen de sa demande d'asile , du document d'information prévu à l'article R. 741-2 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA).</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
