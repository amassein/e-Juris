<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028323672</ID>
<ANCIEN_ID>JG_L_2013_12_000000349541</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/32/36/CETATEXT000028323672.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème / 1ère SSR, 13/12/2013, 349541</TITRE>
<DATE_DEC>2013-12-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>349541</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème / 1ère SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELAPORTE, BRIARD, TRICHET</AVOCATS>
<RAPPORTEUR>M. Didier Ribes</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier de Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:349541.20131213</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 25 mai 2011 au secrétariat du contentieux du Conseil d'État, présenté par le ministre de l'écologie, du développement durable, des transports et du logement ; le ministre demande au Conseil d'État :<br/>
<br/>
              1°) d'annuler l'arrêt n° 09MA00510 du 17 mars 2011 par lequel la cour administrative d'appel de Marseille a, d'une part, annulé, à la demande de la société Résidence Porte des Neiges, le jugement n° 0703817 du 25 novembre 2008 par lequel le tribunal administratif de Montpellier a annulé, sur déféré du préfet des Pyrénées-Orientales, la délibération du 9 mars 2007 du conseil municipal de Porta créant la zone d'aménagement concerté de Porta dite " Porte des Neiges ", d'autre part, rejeté le déféré du préfet des Pyrénées-Orientales ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de la société Résidence Porte des Neiges ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la directive 92/43/CEE du Conseil du 21 mai 1992 ;<br/>
<br/>
              Vu le code de l'environnement ;<br/>
<br/>
              Vu le code de l'urbanisme ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Didier Ribes, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Delaporte, Briard, Trichet, avocat de la société Résidence Porte des Neiges ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des énonciations de l'arrêt attaqué que, par une décision du 22 décembre 2003, la Commission européenne a arrêté, sur proposition du gouvernement français, en application de la directive du 21 mai 1992 concernant la conservation des habitats naturels ainsi que de la faune et de la flore sauvages, la liste des sites d'importance communautaire pour la région biogéographique alpine et a inscrit sur cette liste le site " Capcir, Carlit et Campcardos " (FR 9101471) ; que, par une délibération du 9 mars 2007, le conseil municipal de Porta (Pyrénées-Orientales) a décidé la création de la zone d'aménagement concerté de la " Porte des Neiges ", destinée à l'aménagement d'une station touristique de montagne ; que la zone d'aménagement concerté se situe à l'intérieur du périmètre couvert par le site d'importance communautaire " Capcir, Carlit et Campcardos " ; que, par un jugement du 25 novembre 2008, le tribunal administratif de Montpellier a annulé, sur déféré du préfet des Pyrénées-Orientales, la délibération du conseil municipal de Porta ; que par un arrêt du 17 mars 2011, contre lequel le ministre de l'écologie, du développement durable, des transports et du logement se pourvoit en cassation, la cour administrative d'appel de Marseille a annulé le jugement du tribunal administratif de Montpellier et rejeté le déféré préfectoral ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 414-4 du code de l'environnement, adopté pour la transposition de la directive du 21 mai 1992 et applicable au site concerné par le projet litigieux, qui est au nombre des sites inscrits par la Commission européenne sur la liste des sites d'importance communautaire : " I. - Les programmes ou projets de travaux, d'ouvrage ou d'aménagement soumis à un régime d'autorisation ou d'approbation administrative, et dont la réalisation est de nature à affecter de façon notable un site Natura 2000, font l'objet d'une évaluation de leurs incidences au regard des objectifs de conservation du site. Pour ceux de ces programmes qui sont prévus par des dispositions législatives et réglementaires et qui ne sont pas soumis à étude d'impact, l'évaluation est conduite selon la procédure prévue aux articles           L. 122-4 et suivants du présent code. Les travaux, ouvrages ou aménagements prévus par les contrats Natura 2000 sont dispensés de la procédure d'évaluation mentionnée à l'alinéa précédent. / II. - L'autorité compétente ne peut autoriser ou approuver un programme ou projet mentionné au premier alinéa du I s'il résulte de l'évaluation que sa réalisation porte atteinte à l'état de conservation du site. / III. - Toutefois, lorsqu'il n'existe pas d'autre solution que la réalisation d'un programme ou projet qui est de nature à porter atteinte à l'état de conservation du site, l'autorité compétente peut donner son accord pour des raisons impératives d'intérêt public. Dans ce cas, elle s'assure que des mesures compensatoires sont prises pour maintenir la cohérence globale du réseau Natura 2000. Ces mesures compensatoires sont à la charge du bénéficiaire des travaux, de l'ouvrage ou de l'aménagement. La Commission européenne en est tenue informée. / IV. - Lorsque le site abrite un type d'habitat naturel ou une espèce prioritaires qui figurent, au titre de la protection renforcée dont ils bénéficient, sur des listes arrêtées dans des conditions fixées par décret en Conseil d'Etat, l'accord mentionné au III ne peut être donné que pour des motifs liés à la santé ou à la sécurité publique ou tirés des avantages importants procurés à l'environnement ou, après avis de la Commission européenne, pour d'autres raisons impératives d'intérêt public " ; <br/>
<br/>
              3. Considérant, d'une part, qu'il ressort de ces dispositions que l'évaluation des incidences d'un projet doit être réalisée au regard des différents objectifs de conservation du site d'intérêt communautaire concerné ; qu'une telle évaluation ne saurait se fonder sur le seul rapport entre la superficie d'habitats naturels affectée et la superficie du site lui-même ; que, dès lors, en se fondant sur le caractère très limité des espaces affectés par le projet par rapport à la superficie totale du site d'intérêt communautaire pour apprécier si la réalisation de la zone d'aménagement concerté était de nature à porter atteinte à l'état de conservation du site concerné, la cour a méconnu les dispositions de l'article L. 414-4 du code de l'environnement ;<br/>
<br/>
              4. Considérant, d'autre part, qu'il doit être tenu compte, pour évaluer les incidences d'un projet sur l'état de conservation d'un site d'importance communautaire, des mesures, prévues par le projet, de nature à supprimer ou réduire les effets dommageables de celui-ci sur le site ; qu'en revanche, il n'y a pas lieu de tenir compte, à ce stade, des mesures compensatoires envisagées, le cas échéant, dans l'étude d'incidences, si le projet répond aux conditions posées par le III de l'article L. 414-4 ; que, dès lors, en tenant compte non seulement des mesures de réduction des impacts sur le milieu naturel prévues dans l'étude d'impact du projet de zone d'aménagement concerté mais également des mesures compensatoires qui y étaient envisagées, pour juger que le préfet ne démontrait pas que le projet serait de nature à porter atteinte à l'état de conservation du site concerné, la cour a méconnu les dispositions de l'article L. 414-4 du code de l'environnement ;<br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que le ministre de l'écologie, du développement durable, des transports et du logement est fondé à demander l'annulation de l'arrêt attaqué ; <br/>
<br/>
              6. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'État, qui n'est pas, dans la présente instance, la partie perdante ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Marseille du 17 mars 2011 est annulé.<br/>
<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Marseille.<br/>
Article 3 : Les conclusions de la société Résidence Porte des Neiges tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à la ministre du logement et de l'égalité des territoires, au ministre de l'écologie, du développement durable et de l'énergie et à la société Résidence Porte des Neiges.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">44-045-04 NATURE ET ENVIRONNEMENT. - PROJETS DONT LA RÉALISATION EST DE NATURE À AFFECTER DE FAÇON NOTABLE UN SITE D'IMPORTANCE COMMUNAUTAIRE (ART. L. 414-4 DU CODE DE L'ENVIRONNEMENT) - EVALUATION DES INCIDENCES SUR L'ÉTAT DE CONSERVATION DU SITE - PRISE EN COMPTE DES MESURES, PRÉVUES PAR LE PROJET, DE NATURE À SUPPRIMER OU RÉDUIRE SES EFFETS DOMMAGEABLES - EXISTENCE - PRISE EN COMPTE, À CE STADE, DES MESURES COMPENSATOIRES ENVISAGÉES, LE CAS ÉCHÉANT, DANS LE CADRE DU III DE L'ARTICLE L. 414-4 DU CODE - ABSENCE.
</SCT>
<ANA ID="9A"> 44-045-04 Il doit être tenu compte, pour évaluer les incidences d'un projet sur l'état de conservation d'un site d'importance communautaire, des mesures, prévues par le projet, de nature à supprimer ou réduire les effets dommageables de celui-ci sur le site. En revanche, il n'y a pas lieu de tenir compte, à ce stade, des mesures compensatoires envisagées, le cas échéant, dans l'étude d'incidences, si le projet répond aux conditions posées par le III de l'article L. 414-4 du code de l'environnement.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
