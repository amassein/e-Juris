<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026555861</ID>
<ANCIEN_ID>JG_L_2012_10_000000347199</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/55/58/CETATEXT000026555861.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 4ème sous-sections réunies, 29/10/2012, 347199</TITRE>
<DATE_DEC>2012-10-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>347199</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 4ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ORTSCHEIDT ; SCP CELICE, BLANCPAIN, SOLTNER</AVOCATS>
<RAPPORTEUR>M. Jean-Dominique Langlais</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Fabienne Lambolez</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:347199.20121029</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 3 mars et 31 mai 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour Mme Géraldine B, demeurant ... ; Mme B demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'arrêt n° 08MA01964 du 4 janvier 2011 par lequel la cour administrative d'appel de Marseille a, d'une part, annulé les articles 1er, 2 et 3 du jugement n° 0404559 du 24 janvier 2008 du tribunal administratif de Montpellier condamnant le centre hospitalier de Carcassonne à verser 43 000 euros à Mme B et 47 642,07 euros à la caisse primaire d'assurance maladie de l'Aude en réparation des conséquences dommageables d'une opération subie le 11 avril 2001 par Mme B et, d'autre part, rejeté le surplus des conclusions du centre hospitalier de Carcassonne ainsi que les conclusions incidentes présentées par Mme B et par la caisse primaire d'assurance maladie de l'Aude ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel du centre hospitalier de Carcassonne et de faire droit à son appel incident et, subsidiairement, d'ordonner une expertise ;<br/>
<br/>
              3°) de mettre à la charge du centre hospitalier de Carcassonne le versement d'une somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
	Vu le code de la sécurité sociale ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Langlais, Maître des Requêtes, <br/>
<br/>
              - les observations de la SCP Célice, Blancpain, Soltner, avocat du centre hospitalier de Carcassonne et de la SCP Ortscheidt, avocat de Mme B ;<br/>
<br/>
              - les conclusions de Mme Fabienne Lambolez, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Célice, Blancpain, Soltner, avocat du centre hospitalier de Carcassonne et à la SCP Ortscheidt, avocat de Mme B ;<br/>
<br/>
<br/>
<br/>
<br/>
              Sans qu'il soit besoin d'examiner les autres moyens du pourvoi ;<br/>
<br/>
              1. Considérant qu'aux termes de l'article R. 711-2 du code de justice administrative : " Toute partie est avertie, par une notification faite par lettre recommandée avec demande d'avis de réception ou par la voie administrative mentionnée à l'article R. 611-4, du jour où l'affaire sera appelée à l'audience. (...) L'avertissement est donné sept jours au moins avant l'audience. Toutefois, en cas d'urgence, ce délai peut être réduit à deux jours par une décision expresse du président de la formation de jugement qui est mentionnée sur l'avis d'audience. (...) " ; qu'aux termes de l'article R. 431-1 du même code, applicable devant la cour administrative d'appel en vertu de l'article R. 811-13 : " Lorsqu'une partie est représentée devant le tribunal administratif par un des mandataires mentionnés à l'article R. 431-2, les actes de procédure, à l'exception de la notification de la décision prévue aux articles R. 751-3 et suivants, ne sont accomplis qu'à l'égard de ce mandataire " ; que lorsque l'avis d'audience, régulièrement notifié au seul avocat, n'a pu lui être remis en raison d'un changement d'adresse et a été retourné au greffe de la juridiction, il appartient à celle-ci, en cas d'insuccès de nouvelles tentatives pour joindre l'avocat, d'avertir le requérant de la date de l'audience, personnellement et par tous moyens ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier transmis par la cour administrative d'appel de Marseille qu'à la suite du retour d'un avis d'audience envoyé à l'avocat de Mme B à une adresse où celui-ci n'exerçait plus, le greffe a, comme il lui appartenait de le faire, vérifié son adresse et procédé à l'envoi d'un nouvel avis d'audience ; que le pli contenant cet avis, expédié le 4 novembre 2010, a été présenté chez cet avocat et lui a été remis le 15 novembre ; qu'à la date de l'audience de la cour, qui s'est tenue le 16 novembre et à laquelle Mme B n'était ni présente, ni représentée, un délai de sept jours francs ne s'était pas écoulé depuis cet avertissement ; que la circonstance que cet avocat n'avait pas averti le greffe de son changement d'adresse est, de même que la durée d'acheminement du pli contenant le second avis d'audience, sans incidence sur cette irrégularité, qui doit entraîner la cassation de l'arrêt ; <br/>
<br/>
              3. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge du centre hospitalier de Carcassonne la somme que demande Mme B au titre des dispositions de l'article L. 761-1 du code de justice administrative ; que ces dispositions font obstacle à ce qu'une somme soit mise à la charge de Mme B, qui n'est pas, dans la présente instance, la partie perdante ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Marseille du 4 janvier 2011 est annulé. <br/>
<br/>
		Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Marseille.<br/>
<br/>
		Article 3 : Le surplus des conclusions de Mme B est rejeté.<br/>
<br/>
Article 4 : Les conclusions présentées par le centre hospitalier de Carcassonne au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 5 : La présente décision sera notifiée à Mme Géraldine B, au centre hospitalier de Carcassonne, à la caisse primaire d'assurance maladie de l'Aude et à la ministre des affaires sociales et de la santé.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-06-02-01 PROCÉDURE. JUGEMENTS. TENUE DES AUDIENCES. AVIS D'AUDIENCE. - CARACTÈRE CONTRADICTOIRE DE LA PROCÉDURE - CONVOCATION À L'AUDIENCE D'APPEL - CONVOCATION ADRESSÉE À L'AVOCAT NE LUI ÉTANT PAS PARVENUE - OBLIGATION POUR LA JURIDICTION DE PRÉVENIR PERSONNELLEMENT LE REQUÉRANT [RJ1].
</SCT>
<ANA ID="9A"> 54-06-02-01 Lorsque l'avis d'audience, régulièrement notifié au seul avocat, n'a pu lui être remis en raison d'un changement d'adresse et a été retourné au greffe de la juridiction, il appartient à celle-ci, en cas d'insuccès de nouvelles tentatives pour joindre l'avocat, d'avertir le requérant de la date de l'audience, personnellement et par tous moyens.,,La circonstance que l'avocat n'avait pas averti le greffe de son changement d'adresse est, de même que la durée d'acheminement du pli contenant le second avis d'audience, sans incidence sur l'exigence du respect de cette obligation.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 15 juillet 2004, Mayné, n° 248680, T. p. 831 ; CE, 9 avril 1975, Leffad, n° 91083, T. p. 1208.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
