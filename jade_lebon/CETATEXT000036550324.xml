<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036550324</ID>
<ANCIEN_ID>JG_L_2018_01_000000415512</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/55/03/CETATEXT000036550324.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 26/01/2018, 415512</TITRE>
<DATE_DEC>2018-01-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>415512</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MARLANGE, DE LA BURGADE</AVOCATS>
<RAPPORTEUR>M. Vincent Uher</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:415512.20180126</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société à responsabilité limitée Consus France, à l'appui de sa requête tendant à l'annulation du jugement n° 1310609 du 28 novembre 2014 du tribunal administratif de Paris rejetant sa demande de décharge des rappels de taxe sur la valeur ajoutée mis à sa charge pour la période du 1er janvier 2008 au 30 septembre 2009 et des pénalités correspondantes, a produit deux mémoires, enregistrés les 24 avril et 1er août 2017 au greffe de la cour administrative d'appel de Paris, en application de l'article 23-1 de l'ordonnance n° 58-1067 du 7 novembre 1958, par lesquels elle soulève une question prioritaire de constitutionnalité.<br/>
<br/>
              Par une ordonnance n° 15PA00456 du 6 novembre 2017, enregistrée le 7 novembre 2017 au secrétariat du contentieux du Conseil d'Etat, le président de la 5ème chambre de cette cour, avant qu'il soit statué sur la requête d'appel de la société Consus France, a décidé, par application des dispositions de l'article 23-2 de l'ordonnance du 7 novembre 1958, de transmettre au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution des dispositions du I de l'article 256 et des I et II de l'article 271 du code général des impôts.<br/>
<br/>
              Dans la question prioritaire de constitutionnalité transmise et dans un mémoire enregistré le 7 décembre 2017 au secrétariat du contentieux du Conseil d'Etat, la société Consus France soutient que les dispositions du I de l'article 256 et des I et II de l'article 271 du code général des impôts, applicables au litige, méconnaissent les principes de légalité, de proportionnalité et d'individualisation des peines, garantis par l'article 8 de la Déclaration des droits de l'homme et du citoyen, le principe d'application de la loi pénale plus douce qui découle du principe de nécessité des peines garanti par ce même article 8, le principe de présomption d'innocence garanti par l'article 9 de la Déclaration, le principe d'égalité devant la loi consacré par son article 6, le principe d'égalité devant les charges publiques énoncé par son article 13, le principe de responsabilité qui découle de ses articles 4 et 15 et le principe de garantie des droits protégé par son article 16.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule et ses articles 61-1 et 88-1 ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - la directive 77/388/CEE du Conseil du 17 mai 1977 ;<br/>
              - la directive 2006/112/CE du Conseil du 28 novembre 2006 ;<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - la loi n° 92-677 du 17 juillet 1992, notamment son article 1er ;<br/>
              - la loi n° 2002-1576 du 30 décembre 2002, notamment son article 17 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Vincent Uher, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Marlange, de la Burgade, avocat de la société Consus France.<br/>
<br/>
              Vu la note en délibéré, enregistrée le 15 janvier 2018, présentée par la société Consus France.<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il résulte des dispositions de l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que, lorsqu'une juridiction relevant du Conseil d'Etat a transmis à ce dernier, en application de l'article 23-2 de cette même ordonnance, la question de la conformité à la Constitution d'une disposition législative, le Conseil constitutionnel est saisi de cette question de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement de circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              2. La société Consus France soutient que les dispositions du I de l'article 256 et des I et II de l'article 271 du code général des impôts, dans la portée que leur confère une interprétation jurisprudentielle constante selon laquelle le bénéfice du droit à déduction de taxe sur la valeur ajoutée doit être refusé à un assujetti lorsqu'il est établi, au vu d'éléments objectifs, que celui-ci savait ou aurait dû savoir que, par l'opération invoquée pour fonder ce droit, il participait à une fraude à la taxe sur la valeur ajoutée commise dans le cadre d'une chaîne de livraisons ou de prestations, méconnaissent les principes de légalité, de proportionnalité et d'individualisation des peines, garantis par l'article 8 de la Déclaration des droits de l'homme et du citoyen, le principe d'application de la loi pénale plus douce qui découle du principe de nécessité des peines garanti par ce même article 8, le principe de présomption d'innocence garanti par l'article 9 de la Déclaration, le principe d'égalité devant la loi consacré par son article 6, le principe d'égalité devant les charges publiques énoncé par son article 13, le principe de responsabilité qui découle de ses articles 4 et 15 et le principe de garantie des droits protégé par son article 16.<br/>
<br/>
              3. Aux termes du I de l'article 256 du code général des impôts, dans sa rédaction issue de l'article 1er de la loi du 17 juillet 1992 portant mise en oeuvre par la République française de la directive du Conseil des communautés européennes (C. E. E.) n° 91-680 complétant le système commun de la taxe sur la valeur ajoutée et modifiant, en vue de la suppression des contrôles aux frontières, la directive (C. E. E.) n° 77-388 et de la directive (C. E. E.) n° 92-12 relative au régime général, à la détention, à la circulation et au contrôle des produits soumis à accise : " Sont soumises à la taxe sur la valeur ajoutée les livraisons de biens et les prestations de services effectuées à titre onéreux par un assujetti agissant en tant que tel ". Aux termes des I et II de l'article 271 du même code, dans leur rédaction issue de l'article 17 de la loi du 30 décembre 2002 de finances rectificative pour 2002 : " I. 1. La taxe sur la valeur ajoutée qui a grevé les éléments du prix d'une opération imposable est déductible de la taxe sur la valeur ajoutée applicable à cette opération. / (...) II. 1. Dans la mesure où les biens et les services sont utilisés pour les besoins de leurs opérations imposables, et à la condition que ces opérations ouvrent droit à déduction, la taxe dont les redevables peuvent opérer la déduction est, selon le cas : / a) Celle qui figure sur les factures établies conformément aux dispositions de l'article 289 et si la taxe pouvait légalement figurer sur lesdites factures (...) ". <br/>
<br/>
              4. Considérant que les dispositions du I de l'article 256, relatives au champ d'application de la taxe sur la valeur ajoutée, et celles du I et du a) du 1 du II de l'article 271, relatives au droit à déduction de cette taxe et aux conditions de cette déduction en ce qui concerne les livraisons de biens et les prestations de services, sont applicables au litige. <br/>
<br/>
              5. Si le Conseil constitutionnel a jugé, par sa décision 78-100 DC du 29 décembre 1978 que les dispositions de la loi de finances rectificative pour 1978, desquelles sont issues les dispositions du I de l'article 256 du code général des impôts, n'étaient pas contraires à la Constitution, il a relevé, dans les motifs de cette décision, que la critique dont il était saisi  ne portait que sur le respect de la procédure parlementaire et a seulement jugé que les dispositions de l'article 42 de l'ordonnance du 2 janvier 1959 relative aux lois de finances, alors en vigueur, n'avaient pas été méconnues. Dès lors, la condition que les dispositions contestées du I de l'article 256 du code général des impôts n'aient pas déjà été déclarées conformes à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel doit être regardée comme remplie.  Cette condition est par ailleurs remplie pour les dispositions du I et du a) du 1 du II de l'article 271 du code. <br/>
<br/>
              6. Aux termes de l'article 88-1 de la Constitution : " La République participe à l'Union européenne constituée d'Etats qui ont choisi librement d'exercer en commun certaines de leurs compétences en vertu du traité sur l'Union européenne et du traité sur le fonctionnement de l'Union européenne, tels qu'ils résultent du traité signé à Lisbonne le 13 décembre 2007 ". Le Conseil constitutionnel juge qu'en l'absence de mise en cause d'une règle ou d'un principe inhérent à l'identité constitutionnelle de la France, il n'est pas compétent pour contrôler la conformité aux droits et libertés que la Constitution garantit de dispositions législatives qui se bornent à tirer les conséquences nécessaires de dispositions inconditionnelles et précises d'une directive de l'Union européenne et qu'en ce cas, il n'appartient qu'au juge de l'Union européenne, saisi le cas échéant à titre préjudiciel, de contrôler le respect par cette directive des droits fondamentaux garantis par l'article 6 du traité sur l'Union européenne. Il suit de là qu'en l'absence de mise en cause, à l'occasion d'une question prioritaire de constitutionnalité relative à des dispositions législatives se bornant à tirer les conséquences nécessaires de dispositions précises et inconditionnelles d'une directive de l'Union européenne, d'une règle ou d'un principe inhérent à l'identité constitutionnelle de la France, une telle question n'est pas au nombre de celles qu'il appartient au Conseil d'Etat de transmettre au Conseil constitutionnel sur le fondement de l'article 23-4 de l'ordonnance précitée.<br/>
<br/>
              7. Selon une jurisprudence constante de la Cour de justice de l'Union européenne, rappelée notamment par son arrêt du 18 décembre 2014, Staatssecretaris van Financiën c/ Schoenimport " Italmoda " Mariano Previti vof et Turbu.com BV, Turbu.com Mobile Phone's BV (C-131/13, 163/13 et 164/13), il résulte des dispositions de l'article 17 de la sixième directive 77/388/CEE du 17 mai 1977, reprises en substance à l'article 168 de la directive 2006/112/CE du 28 novembre 2006, que le bénéfice du droit à déduction de taxe sur la valeur ajoutée doit être refusé à un assujetti lorsqu'il est établi, au vu d'éléments objectifs, que celui-ci savait ou aurait dû savoir que, par l'opération invoquée pour fonder ce droit, il participait à une fraude à la taxe sur la valeur ajoutée commise dans le cadre d'une chaîne de livraisons ou de prestations.<br/>
<br/>
              8. Les règles inconditionnelles et précises rappelées au point précédent, inhérentes au système de taxe sur la valeur ajoutée, s'appliquent en droit interne sur le fondement des dispositions du I et du a) du 1 du II de l'article 271 du code général des impôts citées au point 3, prises pour assurer la transposition de l'article 17 de la sixième directive. De même, les dispositions du I de l'article 256 du code général des impôts ont été prises pour la transposition de l'article 2 de cette même directive.<br/>
<br/>
              9. Enfin, la question prioritaire de constitutionnalité soulevée par la société Consus France, qui invoque les principes de légalité, de proportionnalité et d'individualisation des peines, le principe d'application de la loi pénale plus douce qui découle du principe de nécessité des peines, le principe de présomption d'innocence, le principe d'égalité devant la loi, le principe d'égalité devant les charges publiques, le principe de responsabilité et le principe de garantie des droits, ne met en cause aucune règle ou principe inhérent à l'identité constitutionnelle de la France. Par suite, cette question est dépourvue de caractère sérieux.<br/>
<br/>
              10. Il résulte de tout ce qui précède qu'il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>                  D E C I D E :<br/>
                                 --------------<br/>
<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité transmise par le président de la 5ème chambre de la cour administrative d'appel de Paris.<br/>
<br/>
Article 2 : La présente décision sera notifiée à la société à responsabilité limitée Consus France, au ministre de l'action et des comptes publics et au président de la cour administrative d'appel de Paris.<br/>
Copie en sera adressée au Conseil constitutionnel et au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-10-05-02-02 PROCÉDURE. - DISPOSITION DÉCLARÉE CONFORME À LA CONSTITUTION PAR LE CONSEIL CONSTITUTIONNEL DANS LES MOTIFS D'UNE DÉCISION - MOTIFS DE CETTE DÉCISION NE SE PRONONÇANT QUE SUR LE RESPECT DE LA PROCÉDURE PARLEMENTAIRE.
</SCT>
<ANA ID="9A"> 54-10-05-02-02 Si le Conseil constitutionnel a jugé, par sa décision n° 78-100 DC du 29 décembre 1978 que les dispositions de la loi de finances rectificative pour 1978, desquelles sont issues les dispositions du I de l'article 256 du code général des impôts (CGI), n'étaient pas contraires à la Constitution, il a relevé, dans les motifs de cette décision, que la critique dont il était saisi ne portait que sur le respect de la procédure parlementaire et a seulement jugé que les dispositions de l'article 42 de l'ordonnance du 2 janvier 1959 relative aux lois de finances, alors en vigueur, n'avaient pas été méconnues. Dès lors, la condition que les dispositions contestées du I de l'article 256 du (CGI) n'aient pas déjà été déclarées conformes à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel doit être regardée comme remplie.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
