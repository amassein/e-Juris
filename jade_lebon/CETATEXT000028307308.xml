<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028307308</ID>
<ANCIEN_ID>JG_L_2013_12_000000365155</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/30/73/CETATEXT000028307308.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Section, 06/12/2013, 365155, Publié au recueil Lebon</TITRE>
<DATE_DEC>2013-12-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>365155</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Section</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE ; SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>M. François Lelièvre</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Bertrand Dacosta</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESEC:2013:365155.20131206</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 14 janvier 2013 et 15 avril 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour la commune d'Ajaccio, représentée par son maire ; la commune d'Ajaccio demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 10MA00996 du 9 novembre 2012 par lequel la cour administrative d'appel de Marseille a, à la demande de Mme A..., réformé le jugement du tribunal administratif de Bastia du 7 janvier 2010 et condamné la commune d'Ajaccio à lui payer la somme de 22 286 euros, assortie des intérêts au taux légal à compter du 9 mars 2009, ces intérêts devant être capitalisés au 9 mars 2010 et à chaque échéance annuelle à compter de cette date ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la requête d'appel de Mme A... ; <br/>
<br/>
              3°) de mettre à la charge de Mme A... le versement de la somme de 3 600 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 27 novembre 2013, présentée pour la commune d'Ajaccio ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 28 novembre 2013, présentée pour Mme A... ;<br/>
<br/>
              Vu la loi n° 84-16 du 11 janvier 1984 ;<br/>
<br/>
              Vu le décret n° 85-986 du 16 septembre 1985 ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. François Lelièvre, Maître des Requêtes, <br/>
<br/>
              - les conclusions de M. Bertrand Dacosta, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de la commune d'Ajaccio et à la SCP Piwnica, Molinié, avocat de Mme A... ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par un jugement du 30 mars 2006 devenu définitif, le tribunal administratif de Bastia a annulé l'arrêté du 11 octobre 2004 par lequel le maire de la commune d'Ajaccio avait mis un terme au détachement de Mme A... dans les services de cette commune ; que Mme A... a alors demandé au même tribunal de lui accorder, en réparation de cette éviction illégale, l'indemnité que lui refusait la commune d'Ajaccio ; que, par le présent pourvoi, la commune d'Ajaccio demande l'annulation de l'arrêt du 9 novembre 2012 par lequel la cour administrative de Marseille, statuant sur cette demande indemnitaire en appel du jugement du 7 janvier 2010 du tribunal administratif de Bastia, a porté la condamnation de la commune d'Ajaccio à une somme de 22 286 euros, qui comprend notamment 19 000 euros au titre du préjudice subi par Mme A... du fait de la perte de primes liées à l'exercice effectif des fonctions qui étaient les siennes à la commune d'Ajaccio, ainsi que 3 000 euros au titre de son préjudice moral ;<br/>
<br/>
              Sur la régularité de l'arrêt attaqué :<br/>
<br/>
              2. Considérant que, pour statuer sur les droits à indemnités de Mme A..., la cour administrative d'appel de Marseille n'a fait application d'aucune disposition particulière de la loi du 11 janvier 1984 portant dispositions statutaires relatives à la fonction publique de l'Etat ou du décret du 16 septembre 1985 relatif au régime particulier de certaines positions des fonctionnaires de l'Etat ; que, par suite, le moyen tiré de ce qu'elle aurait fait application d'un texte qu'elle n'a ni visé ni mentionné dans les motifs de son arrêt doit être écarté ;<br/>
<br/>
              3. Considérant que, contrairement à ce que soutient la commune d'Ajaccio, la cour n'a pas estimé que la période d'éviction illégale avait couru jusqu'au mois de novembre 2007 ; que, par suite, le moyen tiré de ce que l'arrêt attaqué serait entaché d'un défaut de motivation, faute de justifier une telle durée, ne peut qu'être écarté ; <br/>
<br/>
              4. Considérant que la cour a pu, sans entacher son arrêt d'insuffisance de motivation, juger que Mme A... aurait eu, si elle était demeurée en poste, une chance sérieuse de se voir attribuer certaines indemnités liées à son emploi, sans répondre à l'argumentation, au demeurant dépourvue de toute précision, par laquelle la commune d'Ajaccio alléguait que l'emploi en question avait été supprimé après le départ de l'intéressée ; <br/>
<br/>
              Sur le bien-fondé de l'arrêt attaqué :<br/>
<br/>
              5. Considérant que la cour administrative d'appel de Marseille ne s'est pas méprise sur la portée de la requête d'appel de Mme A... en jugeant qu'elle ne se bornait pas à la seule reproduction littérale de son argumentation de première instance et que, par suite, elle était recevable ;<br/>
<br/>
              6. Considérant que la circonstance que l'arrêté du 11 octobre 2004 par lequel le maire d'Ajaccio a mis fin au détachement de Mme A... a été annulé pour un vice touchant à sa légalité externe ne faisait pas, par elle-même, obstacle à ce que l'intéressée soit indemnisée du préjudice résultant de son éviction illégale ; que la cour a, en tout état de cause, jugé qu'il résultait de l'instruction que cet arrêté n'était pas justifié par l'intérêt du service ; qu'elle n'a ainsi ni fait peser sur la commune d'Ajaccio la charge d'établir la légalité interne de l'arrêté du 11 octobre 2004, ni par suite commis d'erreur de droit ; qu'elle n'a pas davantage entaché sur ce point son arrêt d'une dénaturation des pièces du dossier ; <br/>
<br/>
              7. Considérant que si, dans les motifs d'un jugement du 13 mai 2008, le tribunal administratif de Bastia a jugé, en liquidant l'astreinte prononcée par son précédent jugement ordonnant la réintégration de Mme A... dans les services de la commune d'Ajaccio, que la commune d'Ajaccio avait entièrement exécuté les obligations qui résultaient pour elle de l'annulation de l'arrêté du 11 octobre 2004, ce jugement portait sur un litige ayant un objet différent du présent litige indemnitaire soumis à la cour administrative d'appel ; que, par suite, la commune d'Ajaccio n'est pas fondée à soutenir que l'arrêt attaqué aurait été pris en méconnaissance de l'autorité de la chose jugée par le jugement du 13 mai 2008 ;<br/>
<br/>
              8. Considérant qu'en vertu des principes généraux qui régissent la responsabilité de la puissance publique, un agent public irrégulièrement évincé a droit à la réparation intégrale du préjudice qu'il a effectivement subi du fait de la mesure illégalement prise à son encontre ; que sont ainsi indemnisables les préjudices de toute nature avec lesquels l'illégalité commise présente, compte tenu de l'importance respective de cette illégalité et des fautes relevées à l'encontre de l'intéressé, un lien direct de causalité ; que, pour l'évaluation du montant de l'indemnité due, doit être prise en compte la perte du traitement ainsi que celle des primes et indemnités dont l'intéressé avait, pour la période en cause, une chance sérieuse de bénéficier, à l'exception de celles qui, eu égard à leur nature, à leur objet et aux conditions dans lesquelles elles sont versées, sont seulement destinées à compenser des frais, charges ou contraintes liés à l'exercice effectif des fonctions ; qu'enfin, il y a lieu de déduire, le cas échéant, le montant des rémunérations que l'agent a pu se procurer par son travail au cours de la période d'éviction ;<br/>
<br/>
              9. Considérant que ni l'indemnité d'exercice des missions de préfecture ni l'indemnité forfaitaire pour travaux supplémentaires n'ont pour objet de compenser des frais, charges ou contraintes liés à l'exercice effectif des fonctions ; que la cour administrative d'appel de Marseille n'a donc pas commis d'erreur de droit en recherchant, pour évaluer le montant de la somme due à Mme A..., si celle-ci aurait eu, en l'absence de la décision qui a mis fin illégalement à son détachement, une chance sérieuse de continuer à bénéficier de ces indemnités, au taux qu'elle percevait avant cette mesure ;<br/>
<br/>
              10. Considérant qu'en estimant que le préjudice moral subi par Mme A... du fait de la décision illégale du maire d'Ajaccio s'élevait à la somme de 3 000 euros, la cour administrative d'appel de Marseille s'est livrée à une appréciation souveraine des faits de l'espèce, exempte de dénaturation ;<br/>
<br/>
              11. Considérant qu'il résulte de tout ce qui précède que la commune d'Ajaccio n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque ; que Mme A... n'étant, par suite, pas la partie perdante dans la présente instance, les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à sa charge au titre des frais exposés par la commune d'Ajaccio et non compris dans les dépens ; qu'il y a lieu, en revanche, dans les circonstances de l'espèce, de mettre à la charge de la commune d'Ajaccio le versement à Mme A... de la somme de 3 000 euros au même titre ;<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              			--------------<br/>
<br/>
		Article 1er : Le pourvoi de la commune d'Ajaccio est rejeté.<br/>
Article 2 : La commune d'Ajaccio versera à Mme A... une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la commune d'Ajaccio et à Mme B...A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-08-01 FONCTIONNAIRES ET AGENTS PUBLICS. RÉMUNÉRATION. QUESTIONS D'ORDRE GÉNÉRAL. - PERTE DE RÉMUNÉRATION LIÉE À UNE ÉVICTION ILLÉGALE DU SERVICE - MODALITÉS DE RÉPARATION INTÉGRALE DU PRÉJUDICE EFFECTIVEMENT SUBI [RJ1] - INCLUSION DANS LE PRÉJUDICE INDEMNISABLE - TRAITEMENT - EXISTENCE - PRIMES ET INDEMNITÉS DONT L'INTÉRESSÉ AVAIT UNE CHANCE SÉRIEUSE DE BÉNÉFICIER - EXISTENCE, À L'EXCEPTION DE CELLES QUI SONT SEULEMENT DESTINÉES À COMPENSER DES FRAIS, CHARGES OU CONTRAINTES LIÉS À L'EXERCICE EFFECTIF DES FONCTIONS [RJ2] - DÉDUCTION DU PRÉJUDICE INDEMNISABLE - MONTANT DES RÉMUNÉRATIONS QUE L'AGENT A PU SE PROCURER PAR SON TRAVAIL AU COURS DE LA PÉRIODE D'ÉVICTION.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">36-08-03 FONCTIONNAIRES ET AGENTS PUBLICS. RÉMUNÉRATION. INDEMNITÉS ET AVANTAGES DIVERS. - PRISE EN COMPTE DANS L'ÉVALUATION DE L'INDEMNITÉ ALLOUÉE EN CAS D'ÉVICTION ILLÉGALE DU SERVICE [RJ1] - PRIMES ET INDEMNITÉS DONT L'INTÉRESSÉ AVAIT UNE CHANCE SÉRIEUSE DE BÉNÉFICIER - EXISTENCE, À L'EXCEPTION DE CELLES QUI SONT SEULEMENT DESTINÉES À COMPENSER DES FRAIS, CHARGES OU CONTRAINTES LIÉS À L'EXERCICE EFFECTIF DES FONCTIONS [RJ2].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">60-04-03-02-01-03 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RÉPARATION. ÉVALUATION DU PRÉJUDICE. PRÉJUDICE MATÉRIEL. PERTE DE REVENUS. PRÉJUDICE MATÉRIEL SUBI PAR DES AGENTS PUBLICS. - EVICTION ILLÉGALE DU SERVICE - RÉPARATION INTÉGRALE DU PRÉJUDICE EFFECTIVEMENT SUBI [RJ1] - MODALITÉS - INCLUSION DANS LE PRÉJUDICE INDEMNISABLE - TRAITEMENT - EXISTENCE - PRIMES ET INDEMNITÉS DONT L'INTÉRESSÉ AVAIT UNE CHANCE SÉRIEUSE DE BÉNÉFICIER  - EXISTENCE, À L'EXCEPTION DE CELLES QUI SONT SEULEMENT DESTINÉES À COMPENSER DES FRAIS, CHARGES OU CONTRAINTES LIÉS À L'EXERCICE EFFECTIF DES FONCTIONS [RJ2] - DÉDUCTION DU PRÉJUDICE INDEMNISABLE - MONTANT DES RÉMUNÉRATIONS QUE L'AGENT A PU SE PROCURER PAR SON TRAVAIL AU COURS DE LA PÉRIODE D'ÉVICTION.
</SCT>
<ANA ID="9A"> 36-08-01 En vertu des principes généraux qui régissent la responsabilité de la puissance publique, un agent public irrégulièrement évincé a droit à la réparation intégrale du préjudice qu'il a effectivement subi du fait de la mesure illégalement prise à son encontre. Sont ainsi indemnisables les préjudices de toute nature avec lesquels l'illégalité commise présente, compte tenu de l'importance respective de cette illégalité et des fautes relevées à l'encontre de l'intéressé, un lien direct de causalité.,,,Pour l'évaluation du montant de l'indemnité due, doit être prise en compte la perte du traitement ainsi que celle des primes et indemnités dont l'intéressé avait, pour la période en cause, une chance sérieuse de bénéficier, à l'exception de celles qui, eu égard à leur nature, à leur objet et aux conditions dans lesquelles elles sont versées, sont seulement destinées à compenser les frais, charges ou contraintes liés à l'exercice effectif des fonctions. Enfin, il y a lieu de déduire, le cas échéant, le montant des rémunérations que l'agent a pu se procurer par son travail au cours de la période d'éviction.</ANA>
<ANA ID="9B"> 36-08-03 Pour l'évaluation du montant de l'indemnité due à un agent public au titre du préjudice subi du fait d'une éviction illégale du service, doit notamment être prise en compte la perte des primes et indemnités dont l'intéressé avait, pour la période en cause, une chance sérieuse de bénéficier, à l'exception de celles qui, eu égard à leur nature, à leur objet et aux conditions dans lesquelles elles sont versées, sont seulement destinées à compenser les frais, charges ou contraintes liés à l'exercice effectif des fonctions.</ANA>
<ANA ID="9C"> 60-04-03-02-01-03 En vertu des principes généraux qui régissent la responsabilité de la puissance publique, un agent public irrégulièrement évincé a droit à la réparation intégrale du préjudice qu'il a effectivement subi du fait de la mesure illégalement prise à son encontre. Sont ainsi indemnisables les préjudices de toute nature avec lesquels l'illégalité commise présente, compte tenu de l'importance respective de cette illégalité et des fautes relevées à l'encontre de l'intéressé, un lien direct de causalité.,,,Pour l'évaluation du montant de l'indemnité due, doit être prise en compte la perte du traitement ainsi que celle des primes et indemnités dont l'intéressé avait, pour la période en cause, une chance sérieuse de bénéficier, à l'exception de celles qui, eu égard à leur nature, à leur objet et aux conditions dans lesquelles elles sont versées, sont seulement destinées à compenser les frais, charges ou contraintes liés à l'exercice effectif des fonctions. Enfin, il y a lieu de déduire, le cas échéant, le montant des rémunérations que l'agent a pu se procurer par son travail au cours de la période d'éviction.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., sur le principe d'une indemnisation en cas d'éviction illégale du service, CE, Assemblée, 7 avril 1933, Deberles, n° 4711, p. 439.,,[RJ2] Cf. CE, 24 mai 1933, Fraissé, n° 17828, p. 560 ; CE, 18 juillet 2008, Stilinovic, n° 304962, p. 306. Comp., pour l'exclusion de principe des primes et indemnités liées à l'exercice effectif des fonctions, sans qu'il soit besoin de rechercher si elles sont seulement destinées à compenser les frais, charges ou contraintes liés à cet exercice, CE, 7 novembre 1969, Sieur Vidal, n° 73698, p. 481 ; CE, Section, 24 juin 1977, Dame Deleuse, n°s 93480 93481 93482, p. 294. Rappr., s'agissant de la rémunération des agents publics en décharge syndicale, CE, Section, 27 juillet 2012, Bourdois, n° 344801, p. 316.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
