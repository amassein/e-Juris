<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026141370</ID>
<ANCIEN_ID>JG_L_2012_07_000000341533</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/14/13/CETATEXT000026141370.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 04/07/2012, 341533, Publié au recueil Lebon</TITRE>
<DATE_DEC>2012-07-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>341533</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Philippe Martin</PRESIDENT>
<AVOCATS/>
<RAPPORTEUR>M. Franck Le Morvan</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Claire Landais</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:341533.20120704</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 13 juillet 2010 au secrétariat du contentieux du Conseil d'Etat, présentée par la confédération française pour la promotion sociale des aveugles et des amblyopes, dont le siège est 58, avenue Bosquet à Paris (75007), représentée par son président ; la confédération française pour la promotion sociale des aveugles et des amblyopes demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision implicite née du silence gardé par le Premier ministre sur sa demande tendant à l'abrogation de l'article 1er du décret n° 2005-1591 du 19 décembre 2005 relatif à la prestation de compensation à domicile pour les personnes handicapées, modifié par le décret n° 2006-1311 du 25 octobre 2006, imposant une condition d'âge dans l'octroi de la prestation de compensation du handicap ;<br/>
<br/>
              2°) d'enjoindre au Premier ministre d'abroger ces dispositions dans un délai de deux mois, sous astreinte de 150 euros par jour de retard ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement de la somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
              4°) de condamner l'Etat aux entiers dépens ;<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et le douzième protocole additionnel à cette convention ;<br/>
<br/>
              Vu la charte sociale européenne révisée, signée à Strasbourg le 3 mai 1996 ;<br/>
<br/>
              Vu la Charte des droits fondamentaux de l'Union européenne ;<br/>
<br/>
              Vu la convention relative aux droits des personnes handicapées, signée à New York le 30 mars 2007 ;<br/>
<br/>
              Vu la directive 2000/78/CE du 27 novembre 2000 ;<br/>
<br/>
              Vu le code de l'action sociale et des familles ;<br/>
<br/>
              Vu loi n° 2005-102 du 11 février 2005 ;<br/>
<br/>
              Vu le décret n° 2005-1591 du 19 décembre 2005  modifié par le décret n° 2006-1311 du 25 octobre 2006 ;<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Franck Le Morvan, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Claire Landais, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 245-1 du code de l'action sociale et des familles, issu de l'article 12 de la loi n° 2005-102 du 11 février 2005 : " I. Toute personne handicapée résidant de façon stable et régulière en France métropolitaine, dans les départements mentionnés à l'article L. 751-1 du code de la sécurité sociale ou à Saint-Pierre-et-Miquelon, dont l'âge est inférieur à une limite fixée par décret et dont le handicap répond à des critères définis par décret prenant notamment en compte la nature et l'importance des besoins de compensation au regard de son projet de vie, a droit à une prestation de compensation qui a le caractère d'une prestation en nature qui peut être versée, selon le choix du bénéficiaire, en nature ou en espèces. (...) / II. Peuvent également prétendre au bénéfice de cette prestation : / 1° Les personnes d'un âge supérieur à la limite mentionnée au I mais dont le handicap répondait, avant cet âge limite, aux critères mentionnés audit I, sous réserve de solliciter cette prestation avant un âge fixé par décret ; / 2° Les personnes d'un âge supérieur à la limite mentionnée au I mais qui exercent une activité professionnelle au-delà de cet âge et dont le handicap répond aux critères mentionnés audit I (...) " ; qu'aux termes de l'article 13 de la loi du 11 février 2005 : " (...) Dans un délai maximum de cinq ans, les dispositions de la présente loi opérant une distinction entre les personnes handicapées en fonction de critères d'âge en matière de compensation du handicap et de prise en charge des frais d'hébergement en établissements sociaux et médico-sociaux seront supprimées " ;<br/>
<br/>
              2. Considérant que la requête de la confédération française pour la promotion sociale des aveugles et des amblyopes doit être interprétée comme tendant à l'annulation du refus implicite opposé par le Premier ministre à sa demande tendant à l'abrogation de l'article D. 245-3 du code de l'action sociale et des familles, tel qu'issu de l'article 1er du décret n° 2005-1591 du 19 décembre 2005 modifié par le décret n° 2006-1311 du 25 octobre 2006, qui dispose : " La limite d'âge maximale pour solliciter la prestation de compensation est fixée à soixante ans. Toutefois, les personnes dont le handicap répondait avant l'âge de soixante ans aux critères du I de l'article L. 245-1 peuvent solliciter la prestation jusqu'à soixante-quinze ans. / Cette limite d'âge ne s'applique pas aux bénéficiaires de l'allocation compensatrice optant pour le bénéfice de la prestation de compensation en application de l'article 95 de la loi n° 2005-102 du 11 février 2005 pour l'égalité des droits et des chances, la participation et la citoyenneté des personnes handicapées " ;<br/>
<br/>
              3. Considérant, en premier lieu, que l'article 13 de la loi du 11 février 2005 est dépourvu de toute portée normative et qu'en particulier, il ne ressort pas des travaux parlementaires produits par l'association requérante qu'il ait eu pour objet et encore moins pour effet de rendre caduques au terme d'un délai de cinq ans les dispositions de l'article L. 245-1 du code de l'action sociale et des familles prévoyant un âge limite pour le droit à la prestation de compensation du handicap ; que les moyens tirés de ce que l'article D. 245-3 du même code serait devenu contraire à la loi par suite de l'expiration de ce délai et violerait le principe constitutionnel d'égalité devant la loi doivent par suite, et en tout état de cause, être écartés ;<br/>
<br/>
              4. Considérant, en deuxième lieu, que les principes généraux du droit de l'Union européenne ne trouvent à s'appliquer dans l'ordre juridique national que dans le cas où la situation juridique dont a à connaître le juge administratif français est régie par le droit de l'Union européenne ; que le moyen tiré de la violation du principe général de non-discrimination en fonction de l'âge, en tant que principe général du droit de l'Union européenne, doit donc être écarté dès lors que la prestation de compensation du handicap n'est pas régie par le droit de l'Union ;<br/>
<br/>
              5. Considérant, en troisième lieu, qu'aux termes de l'article 51 de la Charte des droits fondamentaux de l'Union européenne : " 1. Les dispositions de la présente Charte s'adressent aux institutions, organes et organismes de l'Union dans le respect du principe de subsidiarité, ainsi qu'aux Etats membres uniquement lorsqu'ils mettent en oeuvre le droit de l'Union. (...) " ; que le moyen tiré de la méconnaissance de l'article 21 de cette Charte ne peut être accueilli, dès lors que le décret contesté et les dispositions législatives pour l'application desquelles il est pris ne mettent pas en oeuvre le droit de l'Union ; <br/>
<br/>
              6. Considérant, en quatrième lieu, que doivent également être écartés le moyen tiré de la violation de l'article 1er de la directive 2000/78/CE du 27 novembre 2000 portant création d'un cadre général en faveur de l'égalité de traitement en matière d'emploi et de travail, dont l'article 3 dispose qu'elle ne s'applique pas aux versements de toute nature effectués par les régimes publics ou assimilés, y compris les régimes publics de sécurité sociale ou de protection sociale, et celui tiré de la violation du douzième protocole additionnel à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, qui n'a pas été ratifié par la France ; <br/>
<br/>
              7. Considérant, en cinquième lieu, que par l'article 15 de la charte sociale européenne révisée, signée à Strasbourg le 3 mai 1996, les Etats signataires s'engagent " à  prendre les mesures nécessaires pour fournir aux personnes handicapées une orientation, une éducation et une formation professionnelle dans le cadre du droit commun chaque fois que possible ", " à favoriser leur accès à l'emploi par toute mesure susceptible d'encourager les employeurs à embaucher et à maintenir en activité des personnes handicapées " et " à favoriser leur pleine intégration et participation à la vie sociale, notamment par des mesures (...) visant à surmonter des obstacles à la communication et à la mobilité " ; que ces stipulations, qui requièrent l'intervention d'actes complémentaires pour produire des effets à l'égard des particuliers, ont pour objet exclusif de régir les relations entre Etats ; que, par suite, l'association requérante ne peut utilement s'en prévaloir ;<br/>
<br/>
              8. Considérant, en sixième lieu, que l'association requérante se prévaut du paragraphe 3 de l'article 5 de la convention relative aux droits des personnes handicapées, signée à New York le 30 mars 2007, selon lequel : " Afin de promouvoir l'égalité et d'éliminer la discrimination, les Etats Parties prennent toutes les mesures appropriées pour faire en sorte que des aménagements raisonnables soient apportés " ; que toutefois ces stipulations, qui requièrent l'intervention d'actes complémentaires pour produire des effets à l'égard des particuliers, ne peuvent utilement être invoquées ; qu'il en est de même des stipulations de l'article 19 de la même convention, par lesquelles les Etats signataires s'engagent à prendre " des mesures efficaces et appropriées " pour faciliter l'autonomie de vie des personnes handicapées et leur inclusion dans la société ; qu'enfin, les moyens tirés de la violation des articles 27 et 29 de la même convention ne sont pas assortis de précisions suffisantes pour en apprécier le bien-fondé ;<br/>
<br/>
              9. Considérant qu'il résulte de tout ce qui précède, et sans qu'il soit besoin de statuer sur la fin de non-recevoir opposée par le ministre des solidarités et de la cohésion sociale, que les conclusions de la confédération française pour la promotion sociale des aveugles et des amblyopes doivent être rejetées, y compris celles présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative et celles relatives aux dépens ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la confédération française pour la promotion sociale des aveugles et des amblyopes est rejetée.<br/>
Article 2 : La présente décision sera notifiée à la confédération française pour la promotion sociale des aveugles et des amblyopes, au Premier ministre et à la ministre des affaires sociales et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-01-02-01 ACTES LÉGISLATIFS ET ADMINISTRATIFS. DIFFÉRENTES CATÉGORIES D'ACTES. ACCORDS INTERNATIONAUX. APPLICABILITÉ. - EFFET DIRECT [RJ2] - 1) CHARTE SOCIALE EUROPÉENNE - ARTICLE 15 - ABSENCE - 2) ARTICLES 5§3 ET 19 DE LA CONVENTION RELATIVE AUX DROITS DES PERSONNES HANDICAPÉES - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">15-05-001 COMMUNAUTÉS EUROPÉENNES ET UNION EUROPÉENNE. RÈGLES APPLICABLES. - INVOCABILITÉ - INVOCABILITÉ LIMITÉE À L'ENCONTRE DES SEULES DISPOSITIONS METTANT EN &#140;UVRE LE DROIT DE L'UNION EUROPÉENNE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">15-05-002 COMMUNAUTÉS EUROPÉENNES ET UNION EUROPÉENNE. RÈGLES APPLICABLES. - CONDITION D'INVOCABILITÉ - SITUATION RÉGIE PAR LE DROIT DE L'UNION EUROPÉENNE [RJ1].
</SCT>
<ANA ID="9A"> 01-01-02-01 1) Les stipulations de l'article 15 de la charte sociale européenne révisée, par lesquelles les Etats signataires s'engagent à prendre les mesures nécessaires pour fournir aux personnes handicapées une orientation, une éducation et une formation professionnelle dans le cadre du droit commun chaque fois que possible, à favoriser leur accès à l'emploi par toute mesure susceptible d'encourager les employeurs à embaucher et à maintenir en activité des personnes handicapées et à favoriser leur pleine intégration et participation à la vie sociale, notamment par des mesures (&#133;) visant à surmonter des obstacles à la communication et à la mobilité, requièrent l'intervention d'actes complémentaires pour produire des effets à l'égard des particuliers et ont pour objet exclusif de régir les relations entre Etats. Par suite, absence d'invocabilité.,,2) Les stipulations du paragraphe 3 de l'article 5 de la convention relative aux droits des personnes handicapées (selon lequel : Afin de promouvoir l'égalité et d'éliminer la discrimination, les Etats Parties prennent toutes les mesures appropriées pour faire en sorte que des aménagements raisonnables soient apportés) et de l'article 19 de la même convention (par lesquelles les Etats signataires s'engagent à prendre des mesures efficaces et appropriées pour faciliter l'autonomie de vie des personnes handicapées et leur inclusion dans la société) requièrent l'intervention d'actes complémentaires pour produire des effets à l'égard des particuliers, et ne peuvent par suite utilement être invoquées. Par suite, absence d'invocabilité.</ANA>
<ANA ID="9B"> 15-05-001 Ainsi que le précise l'article 51 de la Charte des droits fondamentaux de l'Union européenne, le moyen tiré de la méconnaissance de la Charte ne peut être utilement invoqué que si les dispositions contestées mettent en oeuvre le droit de l'Union.</ANA>
<ANA ID="9C"> 15-05-002 Les principes généraux du droit de l'Union européenne ne trouvent à s'appliquer dans l'ordre juridique national que dans le cas où la situation juridique dont a à connaître le juge administratif français est régie par le droit de l'Union européenne.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 3 décembre 2001, Syndicat national de l'industrie pharmaceutique et autres, n° 226514, p. 790.,,[RJ2] Cf. CE, Assemblée, 11 avril 2012, Groupe d'information et de soutien des immigrés (GISTI) et autre, n° 322326, à publier au Recueil.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
