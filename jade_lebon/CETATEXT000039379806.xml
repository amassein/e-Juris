<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039379806</ID>
<ANCIEN_ID>JG_L_2019_11_000000419067</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/37/98/CETATEXT000039379806.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 5ème chambres réunies, 13/11/2019, 419067</TITRE>
<DATE_DEC>2019-11-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>419067</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO ; HAAS</AVOCATS>
<RAPPORTEUR>Mme Laurence Franceschini</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Stéphane Hoynck</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2019:419067.20191113</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B... A... a demandé au tribunal administratif de Nantes d'annuler pour excès de pouvoir l'arrêté du 26 août 2013 par lequel le maire de l'Ile d'Yeu s'est opposé à sa déclaration préalable de travaux pour la réfection d'un bâtiment à usage de hangar situé sur la parcelle cadastrée section AD n° 1 route des Anglais. Par un jugement n° 1308272 du 15 mai 2016, le tribunal administratif de Nantes a fait droit à cette demande.<br/>
<br/>
              Par un arrêt n° 16NT01496 du 19 janvier 2018, la cour administrative d'appel de Nantes a, sur appel de la commune de l'Ile d'Yeu, après avoir annulé ce jugement, rejeté la demande de M. A....<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 19 mars et 18 juin 2018 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses conclusions d'appel ;<br/>
<br/>
              3°) de mettre à la charge de la commune de l'Ile d'Yeu la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'environnement ;<br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Laurence Franceschini, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Stéphane Hoynck, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de M. A... et à Me Haas, avocat de la commune de l'Ile d'Yeu ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, dans le cadre de l'instruction de la demande de déclaration préalable de travaux de M. A... en vue de la réfection de son hangar, le maire de l'Ile d'Yeu lui a demandé la communication de pièces complémentaires comprenant, d'une part, une notice de présentation des matériaux utilisés et des modalités d'exécution des travaux et, d'autre part, un dossier d'évaluation des incidences du projet sur un site Natura 2000. Le 29 juin 2013, M. A... a transmis les pièces complémentaires demandées. Par un arrêté du 26 août 2013, le maire de l'Ile d'Yeu s'est opposé à la déclaration préalable de travaux déposée par M. A.... Par un jugement du 15 mars 2016, le tribunal administratif de Nantes a annulé cet arrêté. Par un arrêt du 19 janvier 2018 contre lequel M. A... se pourvoit en cassation, la cour administrative d'appel de Nantes a annulé ce jugement et rejeté sa demande. <br/>
<br/>
              2. En premier lieu, aux termes de l'article R. 425-17 du code de l'urbanisme : " Lorsque le projet est situé dans un site classé ou en instance de classement, la décision prise sur la demande de permis ou sur la déclaration préalable ne peut intervenir qu'avec l'accord exprès prévu par les articles L. 341-7 et L. 341-10 du code de l'environnement : / a) Cet accord est donné par le préfet ou, le cas échéant, le directeur de l'établissement public du parc national dans les conditions prévues par l'article R. 341-10 du code de l'environnement, après avis de l'architecte des Bâtiments de France, lorsque le projet fait l'objet d'une déclaration préalable (...). ". Aux termes de l'article R. 421-1 de ce code dans sa rédaction alors en vigueur : " Les constructions nouvelles doivent être précédées de la délivrance d'un permis de construire, à l'exception : / a) Des constructions mentionnées aux articles R. 421-2 à R. 421-8-1 qui sont dispensées de toute formalité au titre du code de l'urbanisme ; / b) Des constructions mentionnées aux articles R. 421-9 à R. 421-12 qui doivent faire l'objet d'une déclaration préalable. ".<br/>
<br/>
              3. Il ressort des pièces du dossier soumis aux juges du fond, notamment du rapport d'expertise établi le 31 octobre 2012, du procès-verbal de constat d'huissier du 11 février 2013 ainsi que des photographies annexées à ce procès-verbal, que le bâtiment est dans un état de délabrement tel que les travaux litigieux, qui consistent en la mise en oeuvre d'une couverture en plaques ondulées, la reprise du pignon endommagé identique à l'état initial, la mise en place d'une porte métallique façade sud et la mise en place d'un volet en bois sur l'ouverture de la façade Est, doivent être regardés comme une reconstruction soumise à permis de construire en vertu des dispositions de l'article R. 421-1 du code de l'urbanisme citées au point 2. En déduisant de ce qui précède que le maire s'était à bon droit opposé à la déclaration préalable de travaux déposée par M. A..., la cour, qui n'a pas inexactement qualifié les faits et ne les a pas dénaturés, n'a pas commis d'erreur de droit. Il suit de là que les moyens dirigés contre l'autre motif de refus retenu par la cour, tiré de ce que le maire devait également refuser le projet dès lors qu'il n'avait pas fait l'objet d'un accord exprès du préfet alors qu'il est situé dans un site Natura 2000, motif qui est surabondant, sont inopérants. <br/>
<br/>
              4. En second lieu, aux termes de l'article R. 423-23 du code de l'urbanisme : " Le délai d'instruction de droit commun est de : /a) Un mois pour les déclarations préalables ; (...). Aux termes de l'article R. 424-1 de ce code dans sa rédaction en vigueur : " A défaut de notification d'une décision expresse dans le délai d'instruction déterminé comme il est dit à la section IV du chapitre III ci-dessus, le silence gardé par l'autorité compétente vaut, selon les cas : / a) Décision de non-opposition à la déclaration préalable (...). ". En vertu de l'article R. 423-22 de ce code : " Pour l'application de la présente section, le dossier est réputé complet si l'autorité compétente n'a pas, dans le délai d'un mois à compter du dépôt du dossier en mairie, notifié au demandeur ou au déclarant la liste des pièces manquantes dans les conditions prévues par les articles R. 423-38 et R. 423-41. ". Aux termes de l'article R. 423-38 du code de l'urbanisme : " Lorsque le dossier ne comprend pas les pièces exigées en application du présent livre, l'autorité compétente, dans le délai d'un mois à compter de la réception ou du dépôt du dossier à la mairie, adresse au demandeur ou à l'auteur de la déclaration une lettre recommandée avec demande d'avis de réception ou, dans le cas prévu par l'article R. 423-48, un échange électronique, indiquant, de façon exhaustive, les pièces manquantes. ". Selon l'article R. 423-39 de ce code : " L'envoi prévu à l'article R. 423-38 précise : / a) Que les pièces manquantes doivent être adressées à la mairie dans le délai de trois mois à compter de sa réception ; / b) Qu'à défaut de production de l'ensemble des pièces manquantes dans ce délai, la demande fera l'objet d'une décision tacite de rejet en cas de demande de permis ou d'une décision tacite d'opposition en cas de déclaration ; / c) Que le délai d'instruction commencera à courir à compter de la réception des pièces manquantes par la mairie. ". Par ailleurs, en vertu de l'article R. 431-36 du code de l'urbanisme dans sa version en vigueur : " Le dossier joint à la déclaration (...) est complété, s'il y a lieu, par les documents mentionnés aux a et b de l'article R*431-10, aux articles R*431-14 et R*431-15, aux b et g de l'article R*431-16 et aux articles R*431-21, R*431-25 et R*431-31 à R*431-33. (...). " Aux termes de l'article R. 431-16 du code de l'urbanisme dans sa version applicable au litige : " Le dossier joint à la demande de permis de construire comprend en outre, selon les cas :  / (...) b) Le dossier d'évaluation des incidences du projet sur un site Natura 2000 prévu à l'article R. 414-23 du code de l'environnement, dans le cas où le projet doit faire l'objet d'une telle évaluation en application de l'article L. 414-4 de ce code. ". <br/>
<br/>
              5. Il résulte de ces dispositions qu'une décision de non-opposition à déclaration préalable naît un mois après le dépôt de celle-ci, en l'absence de notification expresse de l'administration ou d'une demande de pièces complémentaires. En cas de demande de pièces complémentaires, ce délai est interrompu, à la condition toutefois que cette demande intervienne dans le délai d'un mois et qu'elle porte sur l'une des pièces limitativement énumérées par le code de l'urbanisme. Si cette demande de pièces complémentaires tend à la production d'une pièce qui ne peut être requise, elle est de nature à entacher d'illégalité la décision tacite d'opposition prise en application de l'article R. 423-39 du code de l'urbanisme, sans que cette illégalité ait pour effet de rendre le pétitionnaire titulaire d'une décision implicite de non-opposition. <br/>
<br/>
              6. Dans le cas où le pétitionnaire, en réponse à la demande de pièces complémentaires, a fourni une pièce qui a été indûment demandée car ne figurant pas sur la liste limitative des pièces prévue par les dispositions des articles R. 431-36 et R. 431-16 du code de l'urbanisme citées au point 4, cette irrégularité n'est pas, par elle-même, de nature à entraîner l'illégalité de la décision de l'autorité administrative refusant de faire droit à la demande d'autorisation. Toutefois, l'autorisation d'urbanisme n'ayant d'autre objet que d'autoriser la construction conforme aux plans et indications fournis par le pétitionnaire et l'autorité administrative n'ayant, par suite, pas à vérifier l'exactitude des déclarations du demandeur relatives à la consistance de son projet à moins qu'elles ne soient contredites par les autres éléments du dossier joint à la demande tels que limitativement définis par les dispositions précitées, l'administration ne peut légalement refuser l'autorisation demandée en se fondant sur la consistance du projet au vu d'une pièce ne relevant pas de cette liste limitative.<br/>
<br/>
              7. En l'espèce, il ressort des pièces du dossier soumis aux juges du fond que, pour prendre la décision d'opposition à la déclaration préalable de M. A..., l'administration ne s'est pas fondée sur les pièces complémentaires demandées par l'autorité administrative, qui consistaient dans la fourniture d'une notice faisant apparaître les matériaux utilisés et les modalités d'exécution du projet ainsi que le dossier d'évaluation des incidences prévu à l'article R. 414-3 du code de l'environnement. Il s'en suit que c'est sans erreur de droit et sans avoir inexactement qualifié les faits de l'espèce que la cour, qui a suffisamment motivé son arrêt, n'a pas accueilli les moyens tirés de ce que les pièces complémentaires en cause auraient été demandées illégalement.<br/>
<br/>
              8. Il résulte de ce qui précède que le pourvoi de M. A... doit être rejeté. <br/>
<br/>
              9. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de M. A... la somme de 3 000 euros à verser à la commune de l'Ile d'Yeu au titre des dispositions de l'article L. 761-1 du code de justice administrative. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la commune de l'Ile d'Yeu qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le pourvoi de M. A... est rejeté. <br/>
Article 2 : M. A... versera à la commune de l'Ile d'Yeu une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 3 : La présente décision sera notifiée à M. A... et à la commune de l'Ile d'Yeu.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">68-03-02-02 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PERMIS DE CONSTRUIRE. PROCÉDURE D'ATTRIBUTION. INSTRUCTION DE LA DEMANDE. - PÉTITIONNAIRE AYANT FOURNI, À LA DEMANDE DE L'ADMINISTRATION, DES PIÈCES NON PRÉVUES DANS LA LISTE LIMITATIVE DE CELLES DEVANT FIGURER AU DOSSIER - CONSÉQUENCES - 1) ILLÉGALITÉ, POUR CE SEUL MOTIF, DU REFUS DE LA DEMANDE D'AUTORISATION - ABSENCE - 2) ILLÉGALITÉ LORSQUE CE REFUS EST FONDÉ SUR LA CONSISTANCE DU PROJET TELLE QUE RÉVÉLÉE PAR UNE TELLE PIÈCE - EXISTENCE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">68-03-03-005 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PERMIS DE CONSTRUIRE. LÉGALITÉ INTERNE DU PERMIS DE CONSTRUIRE. RÈGLES NON PRISES EN COMPTE LORS DE LA DÉLIVRANCE DU PERMIS DE CONSTRUIRE. - PÉTITIONNAIRE AYANT FOURNI, À LA DEMANDE DE L'ADMINISTRATION, DES PIÈCES NON PRÉVUES DANS LA LISTE LIMITATIVE DE CELLES DEVANT FIGURER AU DOSSIER - CONSÉQUENCES - 1) ILLÉGALITÉ, POUR CE SEUL MOTIF, DU REFUS DE LA DEMANDE D'AUTORISATION - ABSENCE - 2) ILLÉGALITÉ LORSQUE CE REFUS EST FONDÉ SUR LA CONSISTANCE DU PROJET TELLE QUE RÉVÉLÉE PAR UNE TELLE PIÈCE - EXISTENCE [RJ1].
</SCT>
<ANA ID="9A"> 68-03-02-02 1) Dans le cas où le pétitionnaire, en réponse à la demande de pièces complémentaires, a fourni une pièce qui a été indûment demandée car ne figurant pas sur la liste limitative des pièces prévue par les articles R. 431-36 et R. 431-16 du code de l'urbanisme, cette irrégularité n'est pas, par elle-même, de nature à entraîner l'illégalité de la décision de l'autorité administrative refusant de faire droit à la demande d'autorisation.... ,,2) Toutefois, l'autorisation d'urbanisme n'ayant d'autre objet que d'autoriser la construction conforme aux plans et indications fournis par le pétitionnaire et l'autorité administrative n'ayant, par suite, pas à vérifier l'exactitude des déclarations du demandeur relatives à la consistance de son projet à moins qu'elles ne soient contredites par les autres éléments du dossier joint à la demande tels que limitativement définis par les articles précités, l'administration ne peut légalement refuser l'autorisation demandée en se fondant sur la consistance du projet au vu d'une pièce ne relevant pas de cette liste limitative.</ANA>
<ANA ID="9B"> 68-03-03-005 1) Dans le cas où le pétitionnaire, en réponse à la demande de pièces complémentaires, a fourni une pièce qui a été indûment demandée car ne figurant pas sur la liste limitative des pièces prévue par les articles R. 431-36 et R. 431-16 du code de l'urbanisme, cette irrégularité n'est pas, par elle-même, de nature à entraîner l'illégalité de la décision de l'autorité administrative refusant de faire droit à la demande d'autorisation.... ,,2) Toutefois, l'autorisation d'urbanisme n'ayant d'autre objet que d'autoriser la construction conforme aux plans et indications fournis par le pétitionnaire et l'autorité administrative n'ayant, par suite, pas à vérifier l'exactitude des déclarations du demandeur relatives à la consistance de son projet à moins qu'elles ne soient contredites par les autres éléments du dossier joint à la demande tels que limitativement définis par les articles précités, l'administration ne peut légalement refuser l'autorisation demandée en se fondant sur la consistance du projet au vu d'une pièce ne relevant pas de cette liste limitative.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
