<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000023632382</ID>
<ANCIEN_ID>JG_L_2011_02_000000334022</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/23/63/23/CETATEXT000023632382.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 23/02/2011, 334022</TITRE>
<DATE_DEC>2011-02-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>334022</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PEIGNOT, GARREAU</AVOCATS>
<RAPPORTEUR>M. Pascal  Trouilly</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Vialettes Maud</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2011:334022.20110223</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 20 novembre 2009, présentée par l'ASSOCIATION LA CIMADE, dont le siège est 64, rue Clisson à Paris (75013), représentée par son président, le GROUPE D'INFORMATION ET DE SOUTIEN AUX IMMIGRES (GISTI), dont le siège est 3, villa Marcès à Paris (75011), représenté par son président, et la FEDERATION NATIONALE DES ASSOCIATIONS D'ACCUEIL ET DE REINSERTION SOCIALE (FNARS), dont le siège est 76, rue du Faubourg Saint-Denis à Paris (75010), représentée par son président ; l'ASSOCIATION LA CIMADE et autres demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir certaines dispositions des points I.2.1, II.2.3  et II.2.4 de la circulaire du 24 juillet 2008 du ministre de l'immigration, de l'intégration, de l'identité nationale et du développement solidaire relative aux missions des centres d'accueil pour demandeurs d'asile et aux modalités de pilotage du dispositif national d'accueil ;<br/>
<br/>
              2°) d'enjoindre au Gouvernement d'adopter de nouvelles instructions conformes aux dispositions réglementaires en vigueur ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement de la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'action sociale et des familles ; <br/>
<br/>
              Vu le décret n° 2008-1281 du 8 décembre 2008 ; <br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Pascal Trouilly, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Peignot, Garreau, avocat du ministre de l'intérieur, de l'outre-mer, des collectivités territoriales et de l'immigration, <br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Peignot, Garreau, avocat du ministre de l'intérieur, de l'outre-mer, des collectivités territoriales et de l'immigration ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant qu'aux termes de l'article 1er du décret du 8 décembre 2008 relatif aux conditions de publication des instructions et circulaires : " Les circulaires et instructions adressées par les ministres aux services et établissements de l'Etat sont tenues à la disposition du public sur un site internet relevant du Premier ministre.  (...) Une circulaire ou une instruction qui ne figure pas sur le site mentionné au précédent alinéa n'est pas applicable. Les services ne peuvent en aucun cas s'en prévaloir à l'égard des administrés. / Cette publicité se fait sans préjudice des autres formes de publication éventuellement applicables à ces actes " ; qu'aux termes de l'article 2 du même décret : " L'article 1er prend effet à compter du 1er mai 2009. / Les circulaires et instructions déjà signées sont réputées abrogées si elles ne sont pas reprises sur le site mentionné à l'article 1er. / Les dispositions du précédent alinéa ne s'appliquent pas aux circulaires et instructions publiées avant le 1er mai 2009 dont la loi permet à un administré de se prévaloir " ; qu'il ressort des pièces du dossier et n'est pas contesté par l'administration que la circulaire du 24 juillet 2008 du ministre de l'immigration, de l'intégration, de l'identité nationale et du développement solidaire relative aux missions des centres d'accueil pour demandeurs d'asile et aux modalités de pilotage du dispositif national d'accueil n'avait pas été reprise, à la date du 1er mai 2009, sur le site internet créé en application des dispositions de l'article 1er du décret du 8 décembre 2008 citées ci-dessus ; que, par suite, cette circulaire doit, conformément à l'article 2 du même décret, être regardée comme abrogée à compter du 1er mai 2009 ; que sa mise en ligne sur ce même site à une date postérieure au 1er mai 2009 n'a pas eu pour effet de la remettre en vigueur ; que, par suite, la requête de l'ASSOCIATION LA CIMADE et autres, enregistrée le 20 novembre 2009, tend à l'annulation de dispositions qui étaient déjà abrogées à la date où elle a été introduite ; que ces conclusions, dont les associations requérantes ne se sont pas désistées, contrairement à ce que soutient le ministre de l'intérieur, de l'outre-mer, des collectivités locales et de l'immigration, sont, par suite, irrecevables et doivent être rejetées ; que les conclusions de ces associations présentées au titre de l'article L. 761-1 du code de justice administrative ne peuvent également, par voie de conséquence, qu'être rejetées ; qu'enfin, il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge des requérantes la somme demandée par l'Etat au titre des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de l'ASSOCIATION LA CIMADE et autres est rejetée.<br/>
Article 2 : Les conclusions de l'Etat tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à l'ASSOCIATION LA CIMADE, au GROUPE D'INFORMATION ET DE SOUTIEN AUX IMMIGRES (GISTI), à la FEDERATION NATIONALE DES ASSOCIATIONS D'ACCUEIL ET DE REINSERTION SOCIALE (FNARS) et au ministre de l'intérieur, de l'outre-mer, des collectivités territoriales et de l'immigration.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-01-05-03 ACTES LÉGISLATIFS ET ADMINISTRATIFS. DIFFÉRENTES CATÉGORIES D'ACTES. ACTES ADMINISTRATIFS - NOTION. INSTRUCTIONS ET CIRCULAIRES. - APPLICABILITÉ - CONDITION DE PUBLICATION (DÉCRET DU 8 DÉCEMBRE 2008) - ABSENCE, AU 1ER MAI 2009, DE REPRISE SUR LE SITE INTERNET RELEVANT DU PREMIER MINISTRE D'UNE CIRCULAIRE SIGNÉE ANTÉRIEUREMENT - CONSÉQUENCE - ABROGATION.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">01-07-02-035 ACTES LÉGISLATIFS ET ADMINISTRATIFS. PROMULGATION - PUBLICATION - NOTIFICATION. PUBLICATION. EFFETS D'UN DÉFAUT DE PUBLICATION. - ABSENCE, AU 1ER MAI 2009, DE REPRISE SUR LE SITE INTERNET RELEVANT DU PREMIER MINISTRE D'UNE CIRCULAIRE SIGNÉE ANTÉRIEUREMENT - CONSÉQUENCE - ABROGATION.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">01-09-02-02 ACTES LÉGISLATIFS ET ADMINISTRATIFS. DISPARITION DE L'ACTE. ABROGATION. ABROGATION DES ACTES NON RÉGLEMENTAIRES. - CIRCULAIRE NON REPRISE SUR LE SITE INTERNET DU PREMIER MINISTRE (ART. 1 ET 2 DU DÉCRET DU 8 DÉCEMBRE 2008) - CONSÉQUENCE - CIRCULAIRE RÉPUTÉE ABROGÉE [RJ1].
</SCT>
<ANA ID="9A"> 01-01-05-03 En application des dispositions de l'article 2 du décret n° 2008-1281 du 8 décembre 2008, une circulaire signée avant le 1er mai 2009 et ne figurant pas sur le site relevant du Premier ministre créé en application de l'article 1er de ce décret, est réputée abrogée. Sa mise en ligne sur ce même site à une date postérieure au 1er mai 2009 ne saurait avoir pour effet de la remettre en vigueur.</ANA>
<ANA ID="9B"> 01-07-02-035 En application des dispositions de l'article 2 du décret n° 2008-1281 du 8 décembre 2008, une circulaire signée avant le 1er mai 2009 et ne figurant pas sur le site relevant du Premier ministre créé en application de l'article 1er de ce décret, est réputée abrogée. Sa mise en ligne sur ce même site à une date postérieure au 1er mai 2009 ne saurait avoir pour effet de la remettre en vigueur.</ANA>
<ANA ID="9C"> 01-09-02-02 En application des dispositions de l'article 2 du décret du 8 décembre 2008, une circulaire signée avant le 1er mai 2009 et ne figurant pas sur le site relevant du Premier ministre créé en application de l'article 1er de ce décret, est réputée abrogée. Sa mise en ligne sur ce même site à une date postérieure au 1er mai 2009 ne saurait avoir pour effet de la remettre en vigueur.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., s'agissant d'une instruction non écrite devant être regardée comme abrogée, CE, 16 avril 2010, Azelvandre, n° 279817, T. p. 626.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
