<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028569842</ID>
<ANCIEN_ID>JG_L_2014_02_000000364561</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/56/98/CETATEXT000028569842.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème / 3ème SSR, 05/02/2014, 364561, Publié au recueil Lebon</TITRE>
<DATE_DEC>2014-02-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>364561</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème / 3ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>BALAT</AVOCATS>
<RAPPORTEUR>M. Maxime Boutron</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:364561.20140205</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 17 décembre 2012 et 18 mars 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour l'établissement public Voies navigables de France, dont le siège est 175, rue Ludovic Boutieux BP 820, à Béthune (62408) ; Voies navigables de France demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'arrêt n° 11LY02134 du 11 octobre 2012 par lequel la cour administrative d'appel de Lyon a rejeté sa requête tendant, d'une part, à l'annulation du jugement n° 1004061 du 21 juin 2011 du tribunal administratif de Grenoble en tant qu'il a décidé que la moitié de l'astreinte prononcée à l'encontre de la société Cardinal Shipping, liquidée à hauteur de la somme de 32 250 euros pour la période comprise entre le 31 janvier et le 2 septembre 2010, serait versée à l'Etat, d'autre part, à la condamnation de la société Cardinal Shipping à lui verser la totalité de l'astreinte ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code des transports ;<br/>
<br/>
              Vu l'article 124 de la loi n° 90-1168 du 29 décembre 1990 ;<br/>
<br/>
              Vu la loi n° 91-1385 du 31 décembre 1991 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Maxime Boutron, Auditeur ; <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Balat, avocat de Voies navigables de France ;<br/>
<br/>
<br/>
<br/>
              1. Considérant, d'une part, que, lorsqu'il qualifie de contravention de grande voirie des faits d'occupation irrégulière d'une dépendance du domaine public, il appartient au juge administratif, saisi d'un procès-verbal accompagné ou non de conclusions de l'administration tendant à l'évacuation de cette dépendance, d'enjoindre au contrevenant de libérer sans délai le domaine public et, s'il l'estime nécessaire et au besoin d'office, de prononcer une astreinte ;<br/>
<br/>
              2. Considérant, d'autre part, que si, en vertu de l'article L. 911-8 du code de justice administrative, le juge administratif peut décider qu'une part de l'astreinte qu'il prononce ne sera pas versée au requérant mais sera affectée au budget de l'Etat, il résulte de l'ensemble des dispositions du chapitre premier du titre I du livre IX du code de justice administrative, dans lequel elle est insérée, que cette disposition ne s'applique qu'aux astreintes que, depuis la loi du 16 juillet 1980 en ce qui concerne le Conseil d'Etat, et celle du 8 février 1995 en ce qui concerne les tribunaux administratifs et les cours administratives d'appel, ces juridictions peuvent prononcer à l'encontre d'une personne morale de droit public ou d'un organisme privé chargé de la gestion d'un service public ; que la  possibilité, ouverte par l'article L. 911-8 du code de justice administrative, de ne pas verser la totalité de l'astreinte à la victime de l'inexécution ne saurait en revanche s'appliquer lorsque le juge administratif, saisi par l'administration en vue de mettre fin à l'occupation irrégulière d'une dépendance du domaine public, fait application du principe général selon lequel les juges ont la faculté de prononcer une astreinte en vue de l'exécution de leurs décisions ;<br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par un jugement du 23 février 2009, le tribunal administratif de Grenoble a prononcé une astreinte de 150 euros par jour de retard à l'encontre de la société Cardinal Shipping si elle ne justifiait pas avoir, dans le délai d'un mois suivant la notification du jugement, libéré le domaine public fluvial qu'occupait sans autorisation un navire dont elle était l'armateur ; que, par un jugement du 21 juin 2011, le tribunal, ayant constaté que le jugement du 23 février 2009 n'avait pas été exécuté, a liquidé l'astreinte à hauteur de la somme de 32 250 euros et décidé qu'elle serait versée pour moitié à l'établissement public Voies navigables de France et pour moitié à l'Etat ; que Voies navigables de France se pourvoit en cassation contre l'arrêt de la cour administrative d'appel de Lyon du 11 octobre 2012 qui a rejeté son appel dirigé contre ce jugement en tant qu'il a décidé qu'une partie de l'astreinte serait versée à l'Etat ;<br/>
<br/>
              4. Considérant qu'il résulte de ce qui a été dit ci-dessus qu'en jugeant qu'une partie de l'astreinte prononcée à l'encontre de la société Cardinal shipping, qui n'est ni une personne morale de droit public, ni un organisme privé chargé de la gestion d'un service public, pouvait être affectée au budget de l'Etat, la cour administrative d'appel de Lyon a commis une erreur de droit ; que, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, son arrêt doit, pour ce motif, être annulé ;<br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que c'est à tort que le tribunal administratif de Grenoble, faisant application des dispositions de l'article L. 911-8 du même code, a partiellement rejeté les conclusions de Voies navigables de France tendant à ce que l'astreinte en cause soit liquidée en totalité à son profit ; que, par suite, l'astreinte liquidée à hauteur de 32 250 euros, doit être versée à cet établissement public ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Lyon du 11 octobre 2012 est annulé.<br/>
<br/>
Article 2 : L'astreinte liquidée par le jugement du tribunal administratif de Grenoble du 21 juin 2011 est attribuée en totalité à Voies navigables de France.<br/>
<br/>
Article 3 : Le jugement du tribunal administratif de Grenoble du 21 juin 2011 est réformé en ce qu'il a de contraire à la présente décision.<br/>
<br/>
Article 4 : La présente décision sera notifiée à l'Etablissement public Voies navigables de France et à la société Cardinal shipping. <br/>
Copie en sera adressée au ministre de l'écologie, du développement durable et de l'énergie.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">24-01-03-01-04-015 DOMAINE. DOMAINE PUBLIC. PROTECTION DU DOMAINE. CONTRAVENTIONS DE GRANDE VOIRIE. POURSUITES. PROCÉDURE DEVANT LE JUGE ADMINISTRATIF. - POUVOIRS DU JUGE - OCCUPATION IRRÉGULIÈRE QUALIFIÉE DE CONTRAVENTION DE GRANDE VOIRIE - INJONCTION DE LIBÉRER SANS DÉLAI LE DOMAINE PUBLIC - 1) FACULTÉ DE PRONONCER UNE ASTREINTE - EXISTENCE, AU BESOIN D'OFFICE [RJ1] - 2) DISPOSITIONS DE L'ARTICLE L. 911-8 DU CJA PERMETTANT AU JUGE DE DÉCIDER QU'UNE PART DE L'ASTREINTE QU'IL PRONONCE NE SERA PAS VERSÉE AU REQUÉRANT MAIS SERA AFFECTÉE AU BUDGET DE L'ETAT - APPLICABILITÉ AUX SEULES ASTREINTES PRONONCÉES À L'ENCONTRE D'UNE PERSONNE MORALE DE DROIT PUBLIC OU D'UN ORGANISME PRIVÉ CHARGÉ DE LA GESTION D'UN SERVICE PUBLIC - EXISTENCE - APPLICABILITÉ AUX ASTREINTES PRONONCÉES PAR LE JUGE ADMINISTRATIF SAISI PAR L'ADMINISTRATION EN VUE DE METTRE FIN À L'OCCUPATION IRRÉGULIÈRE D'UNE DÉPENDANCE DU DOMAINE PUBLIC - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">24-01-03-02 DOMAINE. DOMAINE PUBLIC. PROTECTION DU DOMAINE. PROTECTION CONTRE LES OCCUPATIONS IRRÉGULIÈRES. - OCCUPATION IRRÉGULIÈRE QUALIFIÉE DE CONTRAVENTION DE GRANDE VOIRIE - POUVOIRS DU JUGE - INJONCTION DE LIBÉRER SANS DÉLAI LE DOMAINE PUBLIC - 1) FACULTÉ DE PRONONCER UNE ASTREINTE - EXISTENCE, AU BESOIN D'OFFICE [RJ1] - 2) DISPOSITIONS DE L'ARTICLE L. 911-8 DU CJA PERMETTANT AU JUGE DE DÉCIDER QU'UNE PART DE L'ASTREINTE QU'IL PRONONCE NE SERA PAS VERSÉE AU REQUÉRANT MAIS SERA AFFECTÉE AU BUDGET DE L'ETAT - APPLICABILITÉ AUX SEULES ASTREINTES PRONONCÉES À L'ENCONTRE D'UNE PERSONNE MORALE DE DROIT PUBLIC OU D'UN ORGANISME PRIVÉ CHARGÉ DE LA GESTION D'UN SERVICE PUBLIC - EXISTENCE - APPLICABILITÉ AUX ASTREINTES PRONONCÉES PAR LE JUGE DE LA CONTRAVENTION DE GRANDE VOIRIE, EN VERTU PRINCIPE GÉNÉRAL SELON LEQUEL LES JUGES ONT LA FACULTÉ DE PRONONCER UNE ASTREINTE EN VUE DE L'EXÉCUTION DE LEURS DÉCISIONS - ABSENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-06-07-01 PROCÉDURE. JUGEMENTS. EXÉCUTION DES JUGEMENTS. ASTREINTE. - JUGE DE LA CONTRAVENTION DE GRANDE VOIRIE - INJONCTION DE LIBÉRER SANS DÉLAI LE DOMAINE PUBLIC - 1) FACULTÉ DE PRONONCER UNE ASTREINTE - EXISTENCE, AU BESOIN D'OFFICE [RJ1] - 2) DISPOSITIONS DE L'ARTICLE L. 911-8 DU CJA PERMETTANT AU JUGE DE DÉCIDER QU'UNE PART DE L'ASTREINTE QU'IL PRONONCE NE SERA PAS VERSÉE AU REQUÉRANT MAIS SERA AFFECTÉE AU BUDGET DE L'ETAT - APPLICABILITÉ AUX SEULES ASTREINTES PRONONCÉES À L'ENCONTRE D'UNE PERSONNE MORALE DE DROIT PUBLIC OU D'UN ORGANISME PRIVÉ CHARGÉ DE LA GESTION D'UN SERVICE PUBLIC - EXISTENCE - APPLICABILITÉ AUX ASTREINTES PRONONCÉES PAR LE JUGE DE LA CONTRAVENTION DE GRANDE VOIRIE, EN VERTU PRINCIPE GÉNÉRAL SELON LEQUEL LES JUGES ONT LA FACULTÉ DE PRONONCER UNE ASTREINTE EN VUE DE L'EXÉCUTION DE LEURS DÉCISIONS - ABSENCE.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">54-07-04 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. POUVOIRS DU JUGE RÉPRESSIF. - JUGE DE LA CONTRAVENTION DE GRANDE VOIRIE - INJONCTION DE LIBÉRER SANS DÉLAI LE DOMAINE PUBLIC - 1) FACULTÉ DE PRONONCER UNE ASTREINTE - EXISTENCE, AU BESOIN D'OFFICE [RJ1] - 2) DISPOSITIONS DE L'ARTICLE L. 911-8 DU CJA PERMETTANT AU JUGE DE DÉCIDER QU'UNE PART DE L'ASTREINTE QU'IL PRONONCE NE SERA PAS VERSÉE AU REQUÉRANT MAIS SERA AFFECTÉE AU BUDGET DE L'ETAT - APPLICABILITÉ AUX SEULES ASTREINTES PRONONCÉES À L'ENCONTRE D'UNE PERSONNE MORALE DE DROIT PUBLIC OU D'UN ORGANISME PRIVÉ CHARGÉ DE LA GESTION D'UN SERVICE PUBLIC - EXISTENCE - APPLICABILITÉ AUX ASTREINTES PRONONCÉES PAR LE JUGE DE LA CONTRAVENTION DE GRANDE VOIRIE, EN VERTU PRINCIPE GÉNÉRAL SELON LEQUEL LES JUGES ONT LA FACULTÉ DE PRONONCER UNE ASTREINTE EN VUE DE L'EXÉCUTION DE LEURS DÉCISIONS - ABSENCE.
</SCT>
<ANA ID="9A"> 24-01-03-01-04-015 1) Lorsqu'il qualifie de contravention de grande voirie des faits d'occupation irrégulière d'une dépendance du domaine public, il appartient au juge administratif, saisi d'un procès-verbal accompagné ou non de conclusions de l'administration tendant à l'évacuation de cette dépendance, d'enjoindre au contrevenant de libérer sans délai le domaine public et, s'il l'estime nécessaire et au besoin d'office, de prononcer une astreinte.,,,2) Si, en vertu de l'article L. 911-8 du code de justice administrative (CJA), le juge administratif peut décider qu'une part de l'astreinte qu'il prononce ne sera pas versée au requérant mais sera affectée au budget de l'Etat, il résulte de l'ensemble des dispositions du chapitre premier du titre I du livre IX du CJA, dans lequel elle est insérée, que cette disposition ne s'applique qu'aux astreintes que, depuis la loi n° 80-539 du 16 juillet 1980 en ce qui concerne le Conseil d'Etat, et celle du 8 février 1995 en ce qui concerne les tribunaux administratifs et les cours administratives d'appel, ces juridictions peuvent prononcer à l'encontre d'une personne morale de droit public ou d'un organisme privé chargé de la gestion d'un service public. La  possibilité, ouverte par l'article L. 911-8 du CJA, de ne pas verser la totalité de l'astreinte à la victime de l'inexécution ne saurait en revanche s'appliquer lorsque le juge administratif, saisi par l'administration en vue de mettre fin à l'occupation irrégulière d'une dépendance du domaine public, fait application du principe général selon lequel les juges ont la faculté de prononcer une astreinte en vue de l'exécution de leurs décisions.</ANA>
<ANA ID="9B"> 24-01-03-02 1) Lorsqu'il qualifie de contravention de grande voirie des faits d'occupation irrégulière d'une dépendance du domaine public, il appartient au juge administratif, saisi d'un procès-verbal accompagné ou non de conclusions de l'administration tendant à l'évacuation de cette dépendance, d'enjoindre au contrevenant de libérer sans délai le domaine public et, s'il l'estime nécessaire et au besoin d'office, de prononcer une astreinte.,,,2) Si, en vertu de l'article L. 911-8 du code de justice administrative (CJA), le juge administratif peut décider qu'une part de l'astreinte qu'il prononce ne sera pas versée au requérant mais sera affectée au budget de l'Etat, il résulte de l'ensemble des dispositions du chapitre premier du titre I du livre IX du CJA, dans lequel elle est insérée, que cette disposition ne s'applique qu'aux astreintes que, depuis la loi n° 80-539 du 16 juillet 1980 en ce qui concerne le Conseil d'Etat, et celle du 8 février 1995 en ce qui concerne les tribunaux administratifs et les cours administratives d'appel, ces juridictions peuvent prononcer à l'encontre d'une personne morale de droit public ou d'un organisme privé chargé de la gestion d'un service public. La  possibilité, ouverte par l'article L. 911-8 du CJA, de ne pas verser la totalité de l'astreinte à la victime de l'inexécution ne saurait en revanche s'appliquer lorsque le juge administratif, saisi par l'administration en vue de mettre fin à l'occupation irrégulière d'une dépendance du domaine public, fait application du principe général selon lequel les juges ont la faculté de prononcer une astreinte en vue de l'exécution de leurs décisions.</ANA>
<ANA ID="9C"> 54-06-07-01 1) Lorsqu'il qualifie de contravention de grande voirie des faits d'occupation irrégulière d'une dépendance du domaine public, il appartient au juge administratif, saisi d'un procès-verbal accompagné ou non de conclusions de l'administration tendant à l'évacuation de cette dépendance, d'enjoindre au contrevenant de libérer sans délai le domaine public et, s'il l'estime nécessaire et au besoin d'office, de prononcer une astreinte.,,,2) Si, en vertu de l'article L. 911-8 du code de justice administrative (CJA), le juge administratif peut décider qu'une part de l'astreinte qu'il prononce ne sera pas versée au requérant mais sera affectée au budget de l'Etat, il résulte de l'ensemble des dispositions du chapitre premier du titre I du livre IX du CJA, dans lequel elle est insérée, que cette disposition ne s'applique qu'aux astreintes que, depuis la loi n° 80-539 du 16 juillet 1980 en ce qui concerne le Conseil d'Etat, et celle du 8 février 1995 en ce qui concerne les tribunaux administratifs et les cours administratives d'appel, ces juridictions peuvent prononcer à l'encontre d'une personne morale de droit public ou d'un organisme privé chargé de la gestion d'un service public. La  possibilité, ouverte par l'article L. 911-8 du CJA, de ne pas verser la totalité de l'astreinte à la victime de l'inexécution ne saurait en revanche s'appliquer lorsque le juge administratif, saisi par l'administration en vue de mettre fin à l'occupation irrégulière d'une dépendance du domaine public, fait application du principe général selon lequel les juges ont la faculté de prononcer une astreinte en vue de l'exécution de leurs décisions.</ANA>
<ANA ID="9D"> 54-07-04 1) Lorsqu'il qualifie de contravention de grande voirie des faits d'occupation irrégulière d'une dépendance du domaine public, il appartient au juge administratif, saisi d'un procès-verbal accompagné ou non de conclusions de l'administration tendant à l'évacuation de cette dépendance, d'enjoindre au contrevenant de libérer sans délai le domaine public et, s'il l'estime nécessaire et au besoin d'office, de prononcer une astreinte.,,,2) Si, en vertu de l'article L. 911-8 du code de justice administrative (CJA), le juge administratif peut décider qu'une part de l'astreinte qu'il prononce ne sera pas versée au requérant mais sera affectée au budget de l'Etat, il résulte de l'ensemble des dispositions du chapitre premier du titre I du livre IX du CJA, dans lequel elle est insérée, que cette disposition ne s'applique qu'aux astreintes que, depuis la loi n° 80-539 du 16 juillet 1980 en ce qui concerne le Conseil d'Etat, et celle du 8 février 1995 en ce qui concerne les tribunaux administratifs et les cours administratives d'appel, ces juridictions peuvent prononcer à l'encontre d'une personne morale de droit public ou d'un organisme privé chargé de la gestion d'un service public. La  possibilité, ouverte par l'article L. 911-8 du CJA, de ne pas verser la totalité de l'astreinte à la victime de l'inexécution ne saurait en revanche s'appliquer lorsque le juge administratif, saisi par l'administration en vue de mettre fin à l'occupation irrégulière d'une dépendance du domaine public, fait application du principe général selon lequel les juges ont la faculté de prononcer une astreinte en vue de l'exécution de leurs décisions.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 25 septembre 2013, M.,, n° 354677, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
