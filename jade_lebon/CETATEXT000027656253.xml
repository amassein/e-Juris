<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027656253</ID>
<ANCIEN_ID>JG_L_2013_07_000000349496</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/65/62/CETATEXT000027656253.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème et 5ème sous-sections réunies, 03/07/2013, 349496</TITRE>
<DATE_DEC>2013-07-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>349496</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème et 5ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BORE, SALVE DE BRUNETON ; SCP GASCHIGNARD</AVOCATS>
<RAPPORTEUR>Mme Florence Chaltiel-Terral</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Keller</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:349496.20130703</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 23 mai et 19 août 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour la SA Journal du Centre, dont le siège est 3 rue du Chemin de Fer BP 106, à Nevers cedex (58001), représentée par son président directeur général ; la SA Journal du Centre demande au Conseil d'Etat d'annuler l'arrêt n° 11LY00138 du 22 mars 2011 de la cour administrative d'appel de Lyon rejetant sa requête tendant à l'annulation du jugement n° 0503006 du 12 janvier 2007 par lequel le tribunal administratif de Dijon a ordonné une expertise en vue de déterminer si M. A... B...pouvait être tenu pour responsable d'une agression sur son chef de service le 21 mars 2005 et du jugement n° 0509006 du 28 juin 2007 par lequel tribunal administratif de Dijon a annulé la décision du 13 mai 2005 de l'inspecteur du travail de la Nièvre autorisant le licenciement de M. B...et la décision du 28 octobre 2005 du ministre de l'emploi, de la cohésion sociale et du logement confirmant l'autorisation de licenciement ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code du travail ; <br/>
<br/>
              Vu la loi n° 91-647 du 10 juillet 1991 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Florence Chaltiel-Terral, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Rémi Keller, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gaschignard, avocat de la SA Journal du Centre et à la SCP Boré, Salve de Bruneton, avocat de M. B...;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par une décision du 13 mai 2005, l'inspecteur du travail de la Nièvre a autorisé la SA Journal du Centre à licencier pour motif disciplinaire M.B..., délégué syndical Force Ouvrière, en raison du comportement agressif de l'intéressé et des violences commises au cours de la journée du 21 mars 2005 ; que le ministre de l'emploi, de la cohésion sociale et du logement a, par une décision du 28 octobre 2005, rejeté le recours hiérarchique formé contre cette décision par l'Union départementale Force Ouvrière de la Nièvre ; que, saisi par M. B..., le tribunal administratif de Dijon a, par un jugement avant-dire droit du 12 janvier 2007, ordonné une expertise en vue de déterminer si l'intéressé pouvait être tenu pour responsable des faits commis le 21 mars 2005, puis, par un jugement du 28 juin 2007, annulé la décision de l'inspecteur du travail ainsi que la décision du ministre de l'emploi, de la cohésion sociale et du logement ; que le Conseil d'Etat a, par une décision du 23 décembre 2010, annulé l'arrêt du 28 mai 2009 de la cour administrative d'appel de Lyon rejetant l'appel de la SA Journal du Centre contre ces jugements ; que la SA Journal du Centre se pourvoit régulièrement en cassation contre l'arrêt du 22 mars 2011 par lequel la cour administrative d'appel de Lyon, statuant à nouveau sur renvoi du Conseil d'Etat sur les conclusions dont elle était saisie, les a rejetées ; <br/>
<br/>
              Sur la régularité de l'arrêt attaqué :<br/>
<br/>
              2. Considérant que la société requérante soutient que la cour administrative d'appel de Lyon a méconnu le caractère contradictoire de la procédure et dénaturé les pièces du dossier qui lui était soumis en jugeant, pour écarter la fin de non recevoir tirée de la tardiveté de la demande de M. B...devant le tribunal administratif, qu'il ne ressortait pas des pièces du dossier et n'était au demeurant pas allégué que les décisions attaquées auraient été notifiées à l'intéressé ; qu'en jugeant ainsi, la cour, qui n'a pas dénaturé les pièces du dossier et qui s'est bornée à répondre à la fin de non recevoir dont elle était saisie, sans soulever d'office aucun moyen, n'a pas méconnu le caractère contradictoire de la procédure ; <br/>
<br/>
              3. Considérant que si la société soutient que l'arrêt de la cour serait insuffisamment motivé en ce qu'il ne se prononcerait que sur l'un des faits commis par M. B... lors de la journée du 21 mars 2005, il ressort de ses termes mêmes que la cour a fait mention de l'ensemble du comportement de M. B...lors de la journée du 21 mars 2005 ; que, dès lors, le moyen tiré de ce que la cour aurait insuffisamment motivé son arrêt sur ce point doit être écarté ; <br/>
<br/>
              Sur le bien-fondé de l'arrêt attaqué :<br/>
<br/>
              4. Considérant qu'en vertu des dispositions de l'article L. 412-18 du code du travail, devenu l'article L. 2411-3, les salariés légalement investis des fonctions de délégué syndical bénéficient, dans l'intérêt de l'ensemble des salariés qu'ils représentent, d'une protection exceptionnelle ; que, lorsque le licenciement d'un de ces salariés est envisagé, ce licenciement ne doit pas être en rapport avec les fonctions représentatives normalement exercées ou l'appartenance syndicale de l'intéressé ; que, dans le cas où la demande de licenciement est motivée par un comportement fautif, il appartient à l'inspecteur du travail et, le cas échéant, au ministre, de rechercher, sous le contrôle du juge de l'excès de pouvoir, si les faits reprochés au salarié sont d'une gravité suffisante pour justifier son licenciement, compte tenu de l'ensemble des règles applicables au contrat de travail de l'intéressé et des exigences propres à l'exécution normale du mandat dont il est investi ; <br/>
<br/>
              5. Considérant qu'aux termes de l'article L. 122-45 du code du travail, dans sa rédaction alors en vigueur, devenu l'article L. 1132-1 : " (...) aucun salarié ne peut être sanctionné ou licencié (...) en raison de son état de santé ou de son handicap. (...) " ; qu'il résulte de ces dispositions que, lorsque qu'une demande d'autorisation de licenciement d'un salarié protégé est motivée par un comportement jugé fautif, elle ne peut être légalement accordée si les faits reprochés  sont la conséquence d'un état pathologique ou d'un handicap de l'intéressé ; <br/>
<br/>
              6. Considérant que, pour juger que les faits reprochés à l'intéressé étaient en rapport avec son état pathologique, la cour a relevé, en se fondant notamment sur le rapport de l'expert désigné par le tribunal administratif de Dijon, que le comportement agressif de M. B... pendant la journée du 21 mars 2005 était la conséquence des troubles psychiques dont il était atteint et des médicaments qui lui avaient été prescrits pour les traiter, lesquels avaient entrainé une addiction et avaient eu pour effet secondaire une altération de son état de conscience et une désinhibition du comportement ; qu'en en déduisant, par un arrêt suffisamment motivé, que l'administration ne pouvait légalement autoriser son licenciement pour faute, la cour n'a pas commis d'erreur de droit et a exactement qualifié les faits qui lui étaient soumis ; que, par suite, le pourvoi de la SA Journal du Centre doit être rejeté ; <br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              7. Considérant que M. B...a obtenu le bénéfice de l'aide juridictionnelle ; que, par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ; qu'il y a lieu, dans les circonstances de l'espèce, et sous réserve que la SCP Boré et Salve de Bruneton, avocat de M.B..., renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de la SA Journal du Centre la somme de 3 000 euros à verser à la SCP Boré et Salve de Bruneton ;<br/>
<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : Le pourvoi de la SA Journal du Centre est rejeté.<br/>
Article 2 : La SA Journal du Centre versera à la SCP Boré et Salve de Bruneton, avocat de M.  B..., une somme de 3 000 euros en application des dispositions du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
Article 3 : La présente décision sera notifiée à la SA Journal du Centre et à M. A...B....<br/>
Copie en sera adressée pour information au ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">66-07-01-04-02 TRAVAIL ET EMPLOI. LICENCIEMENTS. AUTORISATION ADMINISTRATIVE - SALARIÉS PROTÉGÉS. CONDITIONS DE FOND DE L'AUTORISATION OU DU REFUS D'AUTORISATION. LICENCIEMENT POUR FAUTE. - FACULTÉ D'ACCORDER L'AUTORISATION LORSQUE LES FAITS REPROCHÉS SONT LA CONSÉQUENCE D'UN ÉTAT PATHOLOGIQUE OU D'UN HANDICAP DE L'INTÉRESSÉ - ABSENCE.
</SCT>
<ANA ID="9A"> 66-07-01-04-02 Il résulte des dispositions de l'ancien article L. 122-45 du code du travail (devenu l'article L. 1132-1 de ce code), en vertu desquelles :  (&#133;) aucun salarié ne peut être sanctionné ou licencié (&#133;) en raison de son état de santé ou de son handicap (&#133;) , que lorsqu'une demande d'autorisation de licenciement d'un salarié protégé est motivée par un comportement jugé fautif, elle ne peut être légalement accordée si les faits reprochés sont la conséquence d'un état pathologique ou d'un handicap de l'intéressé.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
