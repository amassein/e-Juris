<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038227997</ID>
<ANCIEN_ID>JG_L_2019_03_000000420514</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/22/79/CETATEXT000038227997.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 13/03/2019, 420514</TITRE>
<DATE_DEC>2019-03-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>420514</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ZRIBI, TEXIER</AVOCATS>
<RAPPORTEUR>M. Christian Fournier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Cytermann</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:420514.20190313</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B...A...a demandé au juge des référés du tribunal administratif de Marseille, d'une part, d'ordonner, sur le fondement des dispositions de l'article L. 521-1 du code de justice administrative, la suspension de l'exécution de la décision en date du 21 février 2018 par laquelle la garde des sceaux, ministre de la justice, lui a infligé la sanction disciplinaire de l'exclusion temporaire de fonctions pour une durée de deux ans et, d'autre part, d'enjoindre à l'Etat de la placer en congé de maladie, de lui verser les rémunérations non perçues depuis la sanction et de procéder à sa mutation. <br/>
<br/>
              Par une ordonnance n° 1803275 du 26 avril 2018, le juge des référés du tribunal administratif de Marseille a rejeté cette demande.<br/>
<br/>
              Par un pourvoi, enregistré le 9 mai 2018 au secrétariat du contentieux du Conseil d'Etat, Mme A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - la loi n° 84-16 du 11 janvier 1984 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Christian Fournier, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Laurent Cytermann, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Zribi, Texier, avocat de Mme A...;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge des référés que, par une décision du 2 octobre 2017, la garde des sceaux, ministre de la justice, a suspendu Mme A..., attachée d'administration de l'Etat, de ses fonctions de chef de cabinet du président du tribunal de grande instance d'Aix-en-Provence à compter du 1er février 2017. Par une ordonnance du 13 octobre 2017, rendue par application de l'article L. 522-3 du code de justice administrative, le juge des référés du tribunal administratif de Marseille a rejeté la demande de Mme A...tendant, sur le fondement de l'article L. 521-2 du même code, à la suspension de l'exécution de cette décision. Par un arrêté ultérieur du 21 février 2018, la garde des sceaux, ministre de la justice, a infligé à Mme A...la sanction disciplinaire d'exclusion temporaire de fonctions pour une durée de deux ans à raison des mêmes faits. Par une ordonnance du 26 avril 2018, le même juge des référés du tribunal administratif de Marseille a rejeté sa demande tendant, sur le fondement de l'article L. 521-1 du même code, à la suspension de l'exécution de cet arrêté. Mme A...se pourvoit en cassation contre cette dernière ordonnance.<br/>
<br/>
              2. En premier lieu, aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". Aux termes de l'article L. 521-2 du même code : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ". Aux termes de l'article L. 522-3 du même code : " Lorsque la demande ne présente pas un caractère d'urgence ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée, le juge des référés peut la rejeter par une ordonnance motivée sans qu'il y ait lieu d'appliquer les deux premiers alinéas de l'article L. 522-1 ".<br/>
<br/>
              3. Eu égard à la nature de l'office respectivement attribué au juge des référés par les articles L. 521-1 et L. 521-2 du code de justice administrative, la circonstance qu'un juge des référés a rejeté comme manifestement mal fondée une demande tendant, sur le fondement de l'article L. 521-2, à ce que soit ordonnée une mesure provisoire afin de faire cesser l'atteinte grave et manifestement illégale qui serait portée par une autorité administrative à une liberté fondamentale ne fait pas, à elle seule, obstacle à ce que le même juge des référés statue ultérieurement sur la demande présentée par le même requérant sur le fondement de l'article L. 521-1, tendant à la suspension de l'exécution d'une décision prise par la même autorité administrative dans le cadre du même différend. Il en va ainsi même lorsque la première demande en référé a été rejetée par application des dispositions de l'article L. 522-3 du code de justice administrative.<br/>
<br/>
              4. Il s'ensuit que la circonstance que le même juge des référés avait rejeté la demande de MmeA..., fondée sur l'article L. 521-2, dirigée contre la décision qui l'avait suspendue de ses fonctions ne faisait pas obstacle, par elle-même, à ce qu'il statue ultérieurement sur la demande, fondée sur l'article L. 521-1, tendant à la suspension de la décision infligeant à l'intéressée une sanction disciplinaire à raison des mêmes faits. En outre, il ne résulte pas des termes de l'ordonnance du 13 octobre 2017 ayant rejeté la demande fondée sur l'article L. 521-2 que le juge des référés du tribunal administratif de Marseille, en jugeant que Mme A...n'apportait pas devant lui, dans le cadre de cette procédure, des éléments suffisamment probants au soutien de ses prétentions selon lesquelles elle avait été victime de harcèlement moral, aurait préjugé de la solution qu'il a ensuite adoptée dans l'ordonnance attaquée. Par suite, le moyen tiré de ce que l'ordonnance attaquée aurait été rendue en méconnaissance du principe d'impartialité doit être écarté.<br/>
<br/>
              5. En deuxième lieu, en jugeant que le moyen tiré de ce que Mme A... avait été victime de faits constitutifs de harcèlement moral n'était pas de nature à créer un doute sérieux sur la légalité de l'arrêté du 21 février 2018, le juge des référés a suffisamment répondu à l'argumentation de la requérante selon laquelle cet arrêté aurait été pris en méconnaissance des dispositions du deuxième alinéa de l'article 6 quinquies de la loi du 13 juillet 1983. Si Mme A... avait, en outre, soutenu devant le juge des référés que l'arrêté du 21 février 2018 ne pouvait prendre effet à une date à laquelle elle se trouvait en congé de maladie, ce moyen était inopérant pour obtenir la suspension de son exécution dès lors qu'en tout état de cause, à la date de sa demande, le congé de maladie avait pris fin. Ainsi, l'absence de mention de ce moyen dans l'ordonnance attaquée ne traduit pas une insuffisance de motivation susceptible d'affecter sa régularité.<br/>
<br/>
              6. En troisième lieu, en jugeant que le moyen tiré de ce que Mme A...aurait été victime de faits de harcèlement moral et aurait été sanctionnée pour les avoir dénoncés n'était pas de nature, en l'état de l'instruction, à faire naître un doute sérieux quant à la légalité de l'arrêté du 21 février 2018, le juge des référés s'est livré, sans les dénaturer, à une appréciation souveraine des faits de l'espèce qui ne peut être discutée devant le juge de cassation. <br/>
<br/>
              7. En quatrième lieu, c'est par une appréciation souveraine exempte de dénaturation que le juge des référés a estimé que le moyen tiré de ce que la sanction infligée par la garde des sceaux, ministre de la justice à Mme A...serait disproportionnée au regard des faits qui lui étaient reprochés n'était pas de nature à faire naître un doute sérieux quant sa légalité.<br/>
<br/>
              8. Il résulte de tout ce qui précède que Mme A...n'est pas fondée à demander l'annulation de l'ordonnance qu'elle attaque. Il y a lieu, en conséquence, de rejeter ses conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de Mme A...est rejeté.<br/>
Article 2 : La présente décision sera notifiée à Mme B...A...et à la garde des sceaux, ministre de la justice.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-035-02-04 PROCÉDURE. PROCÉDURES INSTITUÉES PAR LA LOI DU 30 JUIN 2000. RÉFÉRÉ SUSPENSION (ART. L. 521-1 DU CODE DE JUSTICE ADMINISTRATIVE). POUVOIRS ET DEVOIRS DU JUGE. - MAGISTRAT AYANT STATUÉ EN QUALITÉ DE JUGE DU RÉFÉRÉ-LIBERTÉ SUR UNE DEMANDE TENDANT À CE QUE SOIT ORDONNÉE UNE MESURE PROVISOIRE AFIN DE FAIRE CESSER L'ATTEINTE GRAVE ET MANIFESTEMENT ILLÉGALE QUI SERAIT PORTÉE PAR UNE AUTORITÉ ADMINISTRATIVE À UNE LIBERTÉ FONDAMENTALE - CIRCONSTANCE FAISANT, À ELLE SEULE, OBSTACLE À CE QU'IL SE PRONONCE EN QUALITÉ DE JUGE DU RÉFÉRÉ-SUSPENSION SUR UNE DEMANDE TENDANT À LA SUSPENSION DE L'EXÉCUTION D'UNE DÉCISION PRISE PAR LA MÊME AUTORITÉ ADMINISTRATIVE DANS LE CADRE DU MÊME DIFFÉREND - ABSENCE, Y COMPRIS LORSQUE LA PREMIÈRE DEMANDE EST REJETÉE EN APPLICATION DE L'ARTICLE L. 522-3 DU CJA [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-035-03-04 PROCÉDURE. PROCÉDURES INSTITUÉES PAR LA LOI DU 30 JUIN 2000. RÉFÉRÉ TENDANT AU PRONONCÉ DE MESURES NÉCESSAIRES À LA SAUVEGARDE D'UNE LIBERTÉ FONDAMENTALE (ART. L. 521-2 DU CODE DE JUSTICE ADMINISTRATIVE). POUVOIRS ET DEVOIRS DU JUGE. - MAGISTRAT AYANT STATUÉ EN QUALITÉ DE JUGE DU RÉFÉRÉ-LIBERTÉ SUR UNE DEMANDE TENDANT À CE QUE SOIT ORDONNÉE UNE MESURE PROVISOIRE AFIN DE FAIRE CESSER L'ATTEINTE GRAVE ET MANIFESTEMENT ILLÉGALE QUI SERAIT PORTÉE PAR UNE AUTORITÉ ADMINISTRATIVE À UNE LIBERTÉ FONDAMENTALE - CIRCONSTANCE FAISANT, À ELLE SEULE, OBSTACLE À CE QU'IL SE PRONONCE EN QUALITÉ DE JUGE DU RÉFÉRÉ-SUSPENSION SUR UNE DEMANDE TENDANT À LA SUSPENSION DE L'EXÉCUTION D'UNE DÉCISION PRISE PAR LA MÊME AUTORITÉ ADMINISTRATIVE DANS LE CADRE DU MÊME DIFFÉREND - ABSENCE, Y COMPRIS LORSQUE LA PREMIÈRE DEMANDE EST REJETÉE EN APPLICATION DE L'ARTICLE L. 522-3 DU CJA [RJ1].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-06-03 PROCÉDURE. JUGEMENTS. COMPOSITION DE LA JURIDICTION. - MAGISTRAT AYANT STATUÉ EN QUALITÉ DE JUGE DU RÉFÉRÉ-LIBERTÉ SUR UNE DEMANDE TENDANT À CE QUE SOIT ORDONNÉE UNE MESURE PROVISOIRE AFIN DE FAIRE CESSER L'ATTEINTE GRAVE ET MANIFESTEMENT ILLÉGALE QUI SERAIT PORTÉE PAR UNE AUTORITÉ ADMINISTRATIVE À UNE LIBERTÉ FONDAMENTALE - CIRCONSTANCE FAISANT, À ELLE SEULE, OBSTACLE À CE QU'IL SE PRONONCE EN QUALITÉ DE JUGE DU RÉFÉRÉ-SUSPENSION SUR UNE DEMANDE TENDANT À LA SUSPENSION DE L'EXÉCUTION D'UNE DÉCISION PRISE PAR LA MÊME AUTORITÉ ADMINISTRATIVE DANS LE CADRE DU MÊME DIFFÉREND - ABSENCE, Y COMPRIS LORSQUE LA PREMIÈRE DEMANDE EST REJETÉE EN APPLICATION DE L'ARTICLE L. 522-3 DU CJA [RJ1].
</SCT>
<ANA ID="9A"> 54-035-02-04 Eu égard à la nature de l'office respectivement attribué au juge des référés par les articles L. 521-1 et L. 521-2 du code de justice administrative (CJA), la circonstance qu'un juge des référés a rejeté comme manifestement mal fondée une demande tendant, sur le fondement de l'article L. 521-2, à ce que soit ordonnée une mesure provisoire afin de faire cesser l'atteinte grave et manifestement illégale qui serait portée par une autorité administrative à une liberté fondamentale ne fait pas, à elle seule, obstacle à ce que le même juge des référés statue ultérieurement sur la demande présentée par le même requérant sur le fondement de l'article L. 521-1, tendant à la suspension de l'exécution d'une décision prise par la même autorité administrative dans le cadre du même différend. Il en va ainsi même lorsque la première demande en référé a été rejetée par application des dispositions de l'article L. 522-3 du CJA.</ANA>
<ANA ID="9B"> 54-035-03-04 Eu égard à la nature de l'office respectivement attribué au juge des référés par les articles L. 521-1 et L. 521-2 du code de justice administrative (CJA), la circonstance qu'un juge des référés a rejeté comme manifestement mal fondée une demande tendant, sur le fondement de l'article L. 521-2, à ce que soit ordonnée une mesure provisoire afin de faire cesser l'atteinte grave et manifestement illégale qui serait portée par une autorité administrative à une liberté fondamentale ne fait pas, à elle seule, obstacle à ce que le même juge des référés statue ultérieurement sur la demande présentée par le même requérant sur le fondement de l'article L. 521-1, tendant à la suspension de l'exécution d'une décision prise par la même autorité administrative dans le cadre du même différend. Il en va ainsi même lorsque la première demande en référé a été rejetée par application des dispositions de l'article L. 522-3 du CJA.</ANA>
<ANA ID="9C"> 54-06-03 Eu égard à la nature de l'office respectivement attribué au juge des référés par les articles L. 521-1 et L. 521-2 du code de justice administrative (CJA), la circonstance qu'un juge des référés a rejeté comme manifestement mal fondée une demande tendant, sur le fondement de l'article L. 521-2, à ce que soit ordonnée une mesure provisoire afin de faire cesser l'atteinte grave et manifestement illégale qui serait portée par une autorité administrative à une liberté fondamentale ne fait pas, à elle seule, obstacle à ce que le même juge des référés statue ultérieurement sur la demande présentée par le même requérant sur le fondement de l'article L. 521-1, tendant à la suspension de l'exécution d'une décision prise par la même autorité administrative dans le cadre du même différend. Il en va ainsi même lorsque la première demande en référé a été rejetée par application des dispositions de l'article L. 522-3 du CJA.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr. CE, Section, Avis, 12 mai 2004, Commune de Rogerville, n° 265184, p. 223 ; CE, 18 février 2005,,, n° 268952, T. pp. 1023-1031-1050.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
